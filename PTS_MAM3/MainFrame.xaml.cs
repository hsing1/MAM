﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Net;
using System.IO;
using System.Windows.Resources;
using System.Reflection;
using Telerik.Windows.Controls;
using System.Xml.Linq;
using System.Linq;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Windows.Browser;
using System.Windows.Threading;

namespace PTS_MAM3
{
    public partial class MainFrame : UserControl
    {

        int _PaneSeq = 1;  // 定義開啟幾個Pane 
        PTS_MAM3.WSPROPOSAL.WSPROPOSALSoapClient client = new PTS_MAM3.WSPROPOSAL.WSPROPOSALSoapClient();//產生新的代理類別
        flowWebService.FlowSoapClient myFlowLogin = new flowWebService.FlowSoapClient();        //開啟流程引擎的後台


        private DispatcherTimer _UpdUserAccessTimer;
        public MainFrame()
        {
            // 必須將變數初始化
            InitializeComponent();
            this.Opacity = 0;

            //呼叫文件開啟            
            client.CallREPORT_DOCCompleted += new EventHandler<WSPROPOSAL.CallREPORT_DOCCompletedEventArgs>(client_CallREPORT_DOCCompleted);
            //開啟流程引擎的後台
            myFlowLogin.GetConfirmCodeForBackSiteCompleted += new EventHandler<flowWebService.GetConfirmCodeForBackSiteCompletedEventArgs>(myFlowLogin_GetConfirmCodeForBackSiteCompleted);
        }



        private void chkPermission()
        {

            foreach (WSUserObject.ModuleStruct mod in ModuleClass.moduleList)
            {

                if (!string.IsNullOrEmpty(mod.FSHANDLER))
                {
                    foreach (string ctr in mod.FSHANDLER.Split(','))
                    {
                        object obj = FindName(ctr);

                        if (obj != null)
                        {

                            switch (obj.GetType().FullName)
                            {
                                case "Telerik.Windows.Controls.RadTreeViewItem":
                                    ((Telerik.Windows.Controls.RadTreeViewItem)obj).IsEnabled = true;
                                    ((Telerik.Windows.Controls.RadTreeViewItem)obj).SetValue(ToolTipService.ToolTipProperty, "");
                                    break;
                                case "Telerik.Windows.Controls.RadTreeView":
                                    ((Telerik.Windows.Controls.RadTreeView)obj).IsEnabled = true;
                                    ((Telerik.Windows.Controls.RadTreeView)obj).SetValue(ToolTipService.ToolTipProperty, "");
                                    break;
                                case "Telerik.Windows.Controls.RadOutlookBarItem":
                                    ((Telerik.Windows.Controls.RadOutlookBarItem)obj).IsEnabled = true;
                                    ((Telerik.Windows.Controls.RadOutlookBarItem)obj).SetValue(ToolTipService.ToolTipProperty, "");
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                }

            }
        }


        private void RadPane1_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {



            //// TODO: 在此新增事件處理常式執行項目。
            ////
            //WebClient wc = new WebClient();
            //wc.DownloadProgressChanged += new DownloadProgressChangedEventHandler(wc_DownloadProgressChanged);

            //    // 當設定非同步下載完成並引發 OpenReadCompleted 事件時所要執行的事件處理常式。
            //wc.OpenReadCompleted += new OpenReadCompletedEventHandler(wc_OpenReadCompleted);

            //    // 指定所要下載之壓縮檔的路徑並開始下載。
            //wc.OpenReadAsync(new Uri("Search.xap", UriKind.Relative));



        }


        private void wc_OpenReadCompleted(object sender, OpenReadCompletedEventArgs e)
        {
            if ((e.Error == null) && (e.Cancelled == false))
            {


                // 在此要將所下載的資料流轉換成一個組件並載入至目前的 AppDomain 中。


                // 將所下載的資料流轉換成一個組件。
                Assembly a = LoadAssemblyFromXap("Search.dll", e.Result);

                // 建立 Silverlight 類別庫組件 
                // 中之使用者控制項類別 SimpleVideoPlayer 的執行個體。
                UserControl myPage = (UserControl)a.CreateInstance("Search.UserControl1");



                //PaneGrid1.Children.Add(myPage) ; 
                // RadPane pne = this.PaneGroup.FindName("成案申請" + (_PaneSeq -1 ));
                RadPane pne = (RadPane)this.LayoutRoot.FindName("成案申請" + (_PaneSeq - 1));

                if (pne != null)
                {
                    pne.Content = myPage;
                }
                this.busyLoading1.IsBusy = false;

            }
        }

        public Assembly LoadAssemblyFromXap(string relativeUriString, Stream xapPackageStream)
        {
            Uri uri = new Uri(relativeUriString, UriKind.Relative);
            StreamResourceInfo xapPackageSri = new StreamResourceInfo(xapPackageStream, null);
            StreamResourceInfo assemblySri = Application.GetResourceStream(xapPackageSri, uri);

            AssemblyPart assemblyPart = new AssemblyPart();
            Assembly a = assemblyPart.Load(assemblySri.Stream);
            return a;
        }

        void wc_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            //處理讀取進度

        }







        #region "RadPane generate"

        /// <summary>
        /// 產生新的頁籤頁
        /// </summary>
        /// <param name="PaneName">頁籤頁名稱</param>
        /// <param name="XAMLName">XAML class name</param>
        /// <param name="isNew">是否強制開啟新頁</param>
        public void generateNewPane(string PaneName, string XAMLName, Boolean isNew)
        {
            this.busyLoading1.IsBusy = true;

            RadPane openedPane = (RadPane)PaneGroup.FindName(PaneName);

            if (openedPane == null)
            {
                RadPane radPane1 = new RadPane();






                radPane1.Header = PaneName + "　　";
                // radPane1.Name = PaneName + _PaneSeq;


                radPane1.Name = PaneName;

                setPane(XAMLName, radPane1);


                this.PaneGroup.Items.Add(radPane1);


                _PaneSeq++;

            }
            else if (isNew)
            {
                RadPane radPane1 = new RadPane();
                radPane1.Header = PaneName + "　　";
                // radPane1.Name = PaneName + _PaneSeq;


                radPane1.Name = PaneName + _PaneSeq;

                setPane(XAMLName, radPane1);


                this.PaneGroup.Items.Add(radPane1);


                _PaneSeq++;
            }
            else
            {
                this.PaneGroup.SelectedItem = openedPane;

            }

            this.busyLoading1.IsBusy = false;

        }





        /// <summary>
        /// 產生新的頁籤頁
        /// </summary>
        /// <param name="PaneName">頁籤頁名稱</param>
        /// <param name="XAMLName">XAML class name</param>
        /// <param name="param">額外參數</param>
        /// <param name="isNew">是否強制開啟新頁</param>

        public void generateNewPane(string PaneName, string XAMLName, string param, Boolean isNew)
        {
            this.busyLoading1.IsBusy = true;


            RadPane openedPane = (RadPane)PaneGroup.FindName(PaneName);

            //PreviewPlayer.SLVideoPlayer ctr = new PreviewPlayer.SLVideoPlayer("http://localhost/PTS_WMV9_80min_TC.wmv"); 

            if (openedPane == null)
            {

                RadPane radPane1 = new RadPane();

                radPane1.Header = PaneName + "　　";
                radPane1.Name = PaneName;

                setPane(XAMLName, radPane1, param);


                this.PaneGroup.Items.Add(radPane1);

                _PaneSeq++;

            }
            else if (isNew)
            {
                RadPane radPane1 = new RadPane();

                radPane1.Header = PaneName + "　　";
                radPane1.Name = PaneName + _PaneSeq;

                setPane(XAMLName, radPane1, param);

                this.PaneGroup.Items.Add(radPane1);

                _PaneSeq++;
            }
            else
            {
                this.PaneGroup.SelectedItem = openedPane;
                if (openedPane.Name == "已加入清單項目")
                {
                    ((PTS_MAM3.SRH.SRH126)openedPane.Content).PreBookingListBind();
                }
            }

            this.busyLoading1.IsBusy = false;

        }


        public void generateNewPane(string PaneName, string XAMLName, object param, Boolean isNew)
        {
            this.busyLoading1.IsBusy = true;


            RadPane openedPane = (RadPane)PaneGroup.FindName(PaneName);

            //PreviewPlayer.SLVideoPlayer ctr = new PreviewPlayer.SLVideoPlayer("http://localhost/PTS_WMV9_80min_TC.wmv"); 

            if (openedPane == null)
            {

                RadPane radPane1 = new RadPane();

                radPane1.Header = PaneName + "　　";
                radPane1.Name = PaneName;

                setPane(XAMLName, radPane1, param);


                this.PaneGroup.Items.Add(radPane1);

                _PaneSeq++;

            }
            else if (isNew)
            {
                RadPane radPane1 = new RadPane();

                radPane1.Header = PaneName + "　　";
                radPane1.Name = PaneName + _PaneSeq;

                setPane(XAMLName, radPane1, param);

                this.PaneGroup.Items.Add(radPane1);

                _PaneSeq++;
            }
            else
            {
                this.PaneGroup.SelectedItem = openedPane;
                if (openedPane.Name == "刪除檔案查詢結果")
                {
                    ((PTS_MAM3.MAG.MAG801)openedPane.Content).Refresh();
                }
                else if (openedPane.Name == "查詢刪除檔案記錄結果")
                {
                    ((PTS_MAM3.MAG.MAG803)openedPane.Content).Refresh();
                }

            }
            this.busyLoading1.IsBusy = false;

        }

        private void setPane(string XAMLName, RadPane pane)
        {

            switch (XAMLName)
            {
                case "SystemAdmin.UserAdmin":
                    SystemAdmin.MAG200 ctr = new SystemAdmin.MAG200();
                    pane.Content = ctr;
                    break;

                case "SystemAdmin.DirTreeAdmin":
                    SystemAdmin.DirTreeAdmin dirTreePage = new SystemAdmin.DirTreeAdmin();
                    pane.Content = dirTreePage;
                    break;
                case "SystemAdmin.SystemMonitor":
                    SystemAdmin.SystemMonitor ctr1 = new SystemAdmin.SystemMonitor();
                    pane.Content = ctr1;
                    break;
                case "SystemAdmin.ModuleAdmin":
                    SystemAdmin.ModuleAdmin moduleAdminPage = new SystemAdmin.ModuleAdmin();
                    pane.Content = moduleAdminPage;
                    break;

                case "DFGetTTLPage":
                    DFGetTTL DFGetTTLPage = new DFGetTTL();
                    pane.Content = DFGetTTLPage;
                    break;

                case "DFGetFlowIDPage":
                    FLO.FLO400 FLO400 = new FLO.FLO400();
                    pane.Content = FLO400;
                    break;
                case "FLOAgent_List":
                    PTS_MAM3.FLOAgent_List FLOAgent_ListPage = new FLOAgent_List();
                    pane.Content = FLOAgent_ListPage;
                    break;
                case "PROG_M":
                    //PROG_M.PROG_M ctrPROG_M = new PROG_M.PROG_M();
                    PROG_M.PROG_M ctrPROG_M = new PROG_M.PROG_M(this);
                    pane.Content = ctrPROG_M;
                    break;

                case "PROPOSAL":
                    Proposal.PROPOSAL ctrPROPOSAL = new Proposal.PROPOSAL();
                    pane.Content = ctrPROPOSAL;
                    break;

                case "PROG_D":
                    ProgData.PROG_D ctrPROG_D = new ProgData.PROG_D();
                    pane.Content = ctrPROG_D;
                    break;

                case "PROG_LINK":
                    ProgData.PROGLINK ctrPROG_LINK = new ProgData.PROGLINK();
                    pane.Content = ctrPROG_LINK;
                    break;

                case "PROG_MEN":
                    ProgData.PROGMEN ctrPROG_MEN = new ProgData.PROGMEN();
                    pane.Content = ctrPROG_MEN;
                    break;

                case "PROG_RACE":
                    ProgData.PROGRACE ctrPROG_RACE = new ProgData.PROGRACE();
                    pane.Content = ctrPROG_RACE;
                    break;

                case "PROG_PRODUCER":
                    ProgData.PRO600 ctrPROG_PRODUCER = new ProgData.PRO600();
                    pane.Content = ctrPROG_PRODUCER;
                    break;

                case "PROG_STAFF":
                    ProgData.PRG700 ctrPROG_STAFF = new ProgData.PRG700();
                    pane.Content = ctrPROG_STAFF;
                    break;

                case "PROMO":
                    PRG.PRG800 ctrPROMO = new PRG.PRG800();
                    pane.Content = ctrPROMO;
                    break;

                case "Bro":
                    STT.STT100 ctrBroData = new STT.STT100();
                    pane.Content = ctrBroData;
                    break;

                case "BroChange":
                    STT.STT500 ctrBroChangeData = new STT.STT500();
                    pane.Content = ctrBroChangeData;
                    break;

                case "BroChangeManagement":
                    STT.STT900 ctrBroChangeManagementData = new STT.STT900();
                    pane.Content = ctrBroChangeManagementData;
                    break;

                case "CheckIn":
                    //STT.STT200 ctrTape = new STT.STT200();
                    STT.STT600 ctrTape = new STT.STT600("CheckIn");
                    pane.Content = ctrTape;
                    break;

                case "TransCode":
                    //STT.STT300 ctrTransCode = new STT.STT300();
                    STT.STT600 ctrTransCode = new STT.STT600("TransCode");
                    pane.Content = ctrTransCode;
                    break;

                case "TransCodeProgress":
                    //STT.STT400 ctrTransCodeProgress = new STT.STT400();
                    STT.STT600 ctrTransCodeProgress = new STT.STT600("TransCodeProgress");
                    pane.Content = ctrTransCodeProgress;
                    break;

                case "HistoryTape":
                    STT.STT700 ctrHistoryTape = new STT.STT700();
                    pane.Content = ctrHistoryTape;
                    break;

                case "ArchiveData":
                    ArchiveData.ArchiveData ctrArchiveData = new ArchiveData.ArchiveData();
                    pane.Content = ctrArchiveData;
                    break;
                case "STO102":
                    STO.STO102 STO102PG = new STO.STO102();
                    pane.Content = STO102PG;
                    break;

                case "STO103":
                    STO.STO103 STO103PG = new STO.STO103();
                    pane.Content = STO103PG;
                    break;

                case "STO700":
                    STO.STO700 STO700PG = new STO.STO700();
                    pane.Content = STO700PG;
                    break;

                case "ArchiveExchangeData":
                    STO.STO101 Sto101 = new STO.STO101();
                    pane.Content = Sto101;
                    break;

                //case "Xmal流程範本":
                //    MAMFlowXaml.XamlTestFlow.Start page = new MAMFlowXaml.XamlTestFlow.Start();
                //    pane.Content = page;
                //    break;

                case "MAG320":
                    MAG.MAG320 MAG320Page = new MAG.MAG320();
                    pane.Content = MAG320Page;
                    break;

                case "MAG300":
                    MAG.MAG300 ASCpage = new MAG.MAG300();
                    pane.Content = ASCpage;
                    break;

                case "MAG310":
                    MAG.MAG310 MAG310Page = new MAG.MAG310();
                    pane.Content = MAG310Page;
                    break;

                case "MAG330":
                    MAG.MAG330 MAG330Page = new MAG.MAG330();
                    pane.Content = MAG330Page;
                    break;
                case "MAG340":
                    MAG340 MAG340Page = new MAG340();
                    pane.Content = MAG340Page;
                    break;
                case "MAG400":
                    MAG.MAG400 MAG400page = new MAG.MAG400();
                    pane.Content = MAG400page;
                    break;

                case "MAG500":
                    MAG.MAG500 MAG500page = new MAG.MAG500();
                    pane.Content = MAG500page;
                    break;

                case "MAG510":
                    MAG.MAG510 MAG510page = new MAG.MAG510();
                    pane.Content = MAG510page;
                    break;

                case "MAG520":
                    MAG.MAG520 MAG520page = new MAG.MAG520();
                    pane.Content = MAG520page;
                    break;
                
                case "FlowReDo":    //流程補作
                    FLO.FLO300 FLO300page = new FLO.FLO300();
                    pane.Content = FLO300page;
                    break;

                case "FILE_DELETE":
                    MAG.MAG800 MAG800 = new MAG.MAG800(this);
                    pane.Content = MAG800;
                    break;

                case "SEARCH_FILE_DELETE":
                    MAG.MAG802 MAG802 = new MAG.MAG802(this);
                    pane.Content = MAG802;
                    break;

                case "RPT_LOG_01":
                    RPT.RPT100 RPT100 = new RPT.RPT100();
                    pane.Content = RPT100;
                    break;

                case "RPT_FTP_01":
                    RPT.RPT_FTP_01 RPT_FTP_01 = new RPT.RPT_FTP_01();
                    pane.Content = RPT_FTP_01;
                    break;

                case "RPT_FTP_02":
                    RPT.RPT_FTP_02 RPT_FTP_02 = new RPT.RPT_FTP_02();
                    pane.Content = RPT_FTP_02;
                    break;
                  
                case "RPT_COMBINE_QUEUE_ZONE_PLAY_FILE":
                    RPT.RPT_COMBINE_QUEUE_ZONE_PLAY_FILE RPT_COMBINE_QUEUE_ZONE_PLAY_FILE = new RPT.RPT_COMBINE_QUEUE_ZONE_PLAY_FILE();
                    pane.Content = RPT_COMBINE_QUEUE_ZONE_PLAY_FILE;
                    break;

                case "PRO100_09":   //維護製作目的與前後會部門對照
                    PRO.PRO100_09 PRO100_09 = new PRO.PRO100_09();
                    pane.Content = PRO100_09;
                    break;


                case "RPT_BOOKING_01":
                    RPT.RPT_BOOKING_01 RPT_BOOKING_01 = new RPT.RPT_BOOKING_01();
                    pane.Content = RPT_BOOKING_01;
                    break;

                case "RPT_BOOKING_02":
                    RPT.RPT_BOOKING_02 RPT_BOOKING_02 = new RPT.RPT_BOOKING_02();
                    pane.Content = RPT_BOOKING_02;
                    break;

                case "RPT_BOOKING_03":
                    RPT.RPT_BOOKING_03 RPT_BOOKING_03 = new RPT.RPT_BOOKING_03();
                    pane.Content = RPT_BOOKING_03;
                    break;
                case "RPT_STO_01":
                    RPT.RPT_STO_01 RPT_STO_01 = new RPT.RPT_STO_01();
                    pane.Content = RPT_STO_01;//入庫報表
                    break;
                case "RPT_STO_02":
                    RPT.RPT_STO_02 RPT_STO_02 = new RPT.RPT_STO_02();
                    pane.Content = RPT_STO_02;//節目入庫清單/停製通知單
                    break;


                case "RPT_PROGRAM_VIDEO_DETAIL":
                    RPT.RPT_PROGRAM_VIDEO_DETAIL RPT_PROGRAM_VIDEO_DETAIL = new RPT.RPT_PROGRAM_VIDEO_DETAIL();
                    pane.Content = RPT_PROGRAM_VIDEO_DETAIL;//節目入庫清單/停製通知單
                    break;

                #region 磁帶管理
                case "WAIT_TAPE":
                    MAG.MAG600 MAG600page = new MAG.MAG600();
                    pane.Content = MAG600page;
                    break;
                case "MAG606":
                    MAG.MAG606 MAG606page = new MAG.MAG606();
                    pane.Content = MAG606page;
                    break;
                case "MAG620":
                    MAG.MAG620 MAG620page = new MAG.MAG620();
                    pane.Content = MAG620page;
                    break;
                case "ViewTsmRecall":
                    MAG.MAG611 MAG611page = new MAG.MAG611();
                    pane.Content = MAG611page;
                    break;
                #endregion

                #region 檢索調用
                case "SEARCH":
                    AllSearch.Search searchMainPage = new AllSearch.Search(this);
                    pane.Content = searchMainPage;
                    break;

                case "PROGRAM_BOOKING":
                    PTS_MAM3.SRH.SRH101 programbookingMainPage = new PTS_MAM3.SRH.SRH101(this);
                    pane.Content = programbookingMainPage;
                    break;

                case "HOTKEYWORD":
                    AllSearch.HotKeyWord hotkeyeordMainPage = new AllSearch.HotKeyWord(this);
                    pane.Content = hotkeyeordMainPage;
                    break;
                case "HOTHIT":
                    AllSearch.HotHit hothitMainPage = new AllSearch.HotHit(this);
                    pane.Content = hothitMainPage;
                    break;
                case "SEARCH_HISTORY":
                    AllSearch.SearchHistory search_historyMainPage = new AllSearch.SearchHistory(this);
                    pane.Content = search_historyMainPage;
                    break;
                case "SEARCH_SYNONYM":
                    AllSearch.SearchSynonym search_synonymMainPage = new AllSearch.SearchSynonym(this);
                    pane.Content = search_synonymMainPage;
                    break;
                case "USER_BOOKING_HISTORY":
                    PTS_MAM3.SRH.SRH131 user_booking_history = new SRH.SRH131(this);
                    pane.Content = user_booking_history;
                    break;

                case "BOOKING_HISTORY":
                    PTS_MAM3.SRH.SRH132 booking_history = new SRH.SRH132(this);
                    pane.Content = booking_history;
                    break;
                #endregion
                //case "SEARCHRESULT":
                //    AllSearch.SearchResult searchResultMainPage = new AllSearch.SearchResult();
                //    radPane1.Content = searchMainPage;
                //    break;

                //case "DFQueryActivity":
                //    DFForm.DFQueryActivity DFQueryActivity = new DFForm.DFQueryActivity();
                //    radPane1.Content = DFQueryActivity;
                //    break;
                #region  冠宇的 託播單審核表單、調用表單、入庫置換表單、入庫表單、成案表單、送帶轉檔表單 申請標單
                case "託播單審核表單":
                    PGM.PGM110 myPGM110 = new PGM.PGM110();
                    pane.Content = myPGM110;
                    break;
                case "入庫置換表單":
                    STO.STO101 mySTO101 = new STO.STO101();
                    pane.Content = mySTO101;
                    break;
                case "入庫表單":
                    ArchiveData.ArchiveData MyArchiveData = new ArchiveData.ArchiveData();
                    pane.Content = MyArchiveData;
                    break;
                case "成案表單":
                    Proposal.PROPOSAL myProposal = new Proposal.PROPOSAL();
                    pane.Content = myProposal;
                    break;
                case "送帶轉檔表單":
                    STT.STT100 mySTT100 = new STT.STT100();
                    pane.Content = mySTT100;
                    break;
                case "送帶轉檔置換表單":
                    STT.STT500 mySTT500 = new STT.STT500();
                    pane.Content = mySTT500;
                    break;

                #endregion
                #region PGM表單
                case "節目表維護":
                    PgmQueue myPgmQueue = new PgmQueue();
                    pane.Content = myPgmQueue;
                    break;
                case "破口資源預約":
                    PGM.PGM105 myPGMPromoPlan = new PGM.PGM105();
                    pane.Content = myPGMPromoPlan;
                    break;
                case "頻道預排短帶":
                    PGM.PGM115 myPGMPromoChannelLink = new PGM.PGM115();
                    pane.Content = myPGMPromoChannelLink;
                    break;
                case "節目預排短帶":
                    PGM.PGM120 myPGMPromoProgLink = new PGM.PGM120();
                    pane.Content = myPGMPromoProgLink;
                    break;
                case "短帶託播單":
                    PGM.PGM110_03 myPGMPromoBooking = new PGM.PGM110_03();
                    pane.Content = myPGMPromoBooking;
                    break;
                case "短帶託播單維護":
                    PGM.PGM110_03 myPGMPromoBookingM = new PGM.PGM110_03();
                    pane.Content = myPGMPromoBookingM;
                    break;
                case "插入短帶":
                    //有問題Jimmy 暫時Mark 
                    PGM.PGM125 myPGMInsertPromo = new PGM.PGM125();
                    pane.Content = myPGMInsertPromo;
                    break;
                case "匯入LouthKey":
                    PGM.PGM130 myLouthKey = new PGM.PGM130();
                    pane.Content = myLouthKey;
                    break;
                //送帶轉檔單查詢
                case "送帶轉檔單查詢":
                    STT.STT800 STT800 = new STT.STT800();
                    pane.Content = STT800;
                    break;
                case "轉出節目表":
                    PGM.PGM140 myExportPlayList = new PGM.PGM140();
                    pane.Content = myExportPlayList;
                    break;
                case "查詢節目表檔案狀態":
                    PGM.PGM145 myPGMQueryHDFileStatus = new PGM.PGM145();
                    pane.Content = myPGMQueryHDFileStatus;
                    break;
                case "複製節目表":
                    PGM.PGM150 myPGMCopyPlaylistSDToHD = new PGM.PGM150();
                    pane.Content = myPGMCopyPlaylistSDToHD;
                    break;                
                //case "設定LouthKey":
                //    SetLouthKey myLouthKey = new SetLouthKey();
                //    pane.Content = myLouthKey;
                //    break;
                #endregion
                case "表單狀態查詢":
                    DFForm.DFQueryActivity DFQueryActivity = new DFForm.DFQueryActivity();
                    pane.Content = DFQueryActivity;
                    break;

                #region 冠宇的單檔維護
                case "TBARCHIVE_SET_CLASS":
                    MAG.MAG130 TBARCHIVE_SET_CLASS = new MAG.MAG130("TBARCHIVE_SET_CLASS");
                    pane.Content = TBARCHIVE_SET_CLASS;
                    break;
                case "TBZBOOKING_REASON":
                    MAG.MAG125 TBZBOOKING_REASON = new MAG.MAG125("TBZBOOKING_REASON");
                    pane.Content = TBZBOOKING_REASON;
                    break;
                case "TBZPROGOBJ":
                    MAG.MAG125 TBZPROGOBJ = new MAG.MAG125("TBZPROGOBJ");
                    pane.Content = TBZPROGOBJ;
                    break;
                case "TBZPROGSTATUS":
                    MAG.MAG125 TBZPROGSTATUS = new MAG.MAG125("TBZPROGSTATUS");
                    pane.Content = TBZPROGSTATUS;
                    break;
                case "TBZTITLE":
                    MAG.MAG125 TBZTITLE = new MAG.MAG125("TBZTITLE");
                    pane.Content = TBZTITLE;
                    break;
                case "TBZDEPT":
                    MAG.MAG125 TBZDEPT = new MAG.MAG125("TBZDEPT");
                    pane.Content = TBZDEPT;
                    break;
                case "TBZPROGSRC":
                    MAG.MAG125 TBZPROGSRC = new MAG.MAG125("TBZPROGSRC");
                    pane.Content = TBZPROGSRC;
                    break;
                case "TBZPROGATTR":
                    MAG.MAG125 TBZPROGATTR = new MAG.MAG125("TBZPROGATTR");
                    pane.Content = TBZPROGATTR;
                    break;
                case "TBZPROGAUD":
                    MAG.MAG125 TBZPROGAUD = new MAG.MAG125("TBZPROGAUD");
                    pane.Content = TBZPROGAUD;
                    break;
                case "TBZPROGTYPE":
                    MAG.MAG125 TBZPROGTYPE = new MAG.MAG125("TBZPROGTYPE");
                    pane.Content = TBZPROGTYPE;
                    break;
                case "TBZPROGLANG":
                    MAG.MAG125 TBZPROGLANG = new MAG.MAG125("TBZPROGLANG");
                    pane.Content = TBZPROGLANG;
                    break;
                case "TBZPROGBUY":
                    MAG.MAG125 TBZPROGBUY = new MAG.MAG125("TBZPROGBUY");
                    pane.Content = TBZPROGBUY;
                    break;
                case "TBZPROGBUYD":
                    MAG.MAG101 TBZPROGBUYD = new MAG.MAG101("TBZPROGBUYD");
                    pane.Content = TBZPROGBUYD;
                    break;
                case "TBZPROGGRADE":
                    MAG.MAG125 TBZPROGGRADE = new MAG.MAG125("TBZPROGGRADE");
                    pane.Content = TBZPROGGRADE;
                    break;
                case "TBFILE_TYPE":
                    MAG.MAG100 TBFILE_TYPE = new MAG.MAG100("TBFILE_TYPE");
                    pane.Content = TBFILE_TYPE;
                    break;

                case "TBARCHIVE_SET":
                    MAG.MAG102 TBARCHIVE_SET = new MAG.MAG102("TBARCHIVE_SET");
                    pane.Content = TBARCHIVE_SET;
                    break;
                case "TBZPROMOVER":
                    MAG.MAG125 TBZPROMOVER = new MAG.MAG125("TBZPROMOVER");
                    pane.Content = TBZPROMOVER;
                    break;
                case "TBZSHOOTSPEC":
                    MAG.MAG125 TBZSHOOTSPEC = new MAG.MAG125("TBZSHOOTSPEC");
                    pane.Content = TBZSHOOTSPEC;
                    break;
                case "TBPGM_ACTION":
                    MAG.MAG125 TBPGM_ACTION = new MAG.MAG125("TBPGM_ACTION");
                    pane.Content = TBPGM_ACTION;
                    break;
                case "TBZPROMOTYPE":
                    MAG.MAG125 TBZPROMOTYPE = new MAG.MAG125("TBZPROMOTYPE");
                    pane.Content = TBZPROMOTYPE;
                    break;
                case "TBZPROMOCAT":
                    MAG.MAG125 TBZPROMOCAT = new MAG.MAG125("TBZPROMOCAT");
                    pane.Content = TBZPROMOCAT;
                    break;
                case "TBZPROMOCATD":
                    MAG.MAG101 TBZPROMOCATD = new MAG.MAG101("TBZPROMOCATD");
                    pane.Content = TBZPROMOCATD;
                    break;
                case "TBZSIGNAL":
                    MAG.MAG125 TBZSIGNAL = new MAG.MAG125("TBZSIGNAL");
                    pane.Content = TBZSIGNAL;
                    break;
                case "TBZPROGSPEC":
                    MAG.MAG125 TBZPROGSPEC = new MAG.MAG125("TBZPROGSPEC");
                    pane.Content = TBZPROGSPEC;
                    break;
                case "TBZPROGRACE_AREA":
                    MAG.MAG125 TBZPROGRACE_AREA = new MAG.MAG125("TBZPROGRACE_AREA");
                    pane.Content = TBZPROGRACE_AREA;
                    break;
                case "TBZCODE":
                    MAG.MAG168 TBZCODE = new MAG.MAG168();
                    pane.Content = TBZCODE;
                    break;
                //國家來源代碼檔維護
                case "TBZPROGNATION":
                    MAG.MAG901 MAG901Page = new MAG.MAG901();
                    pane.Content = MAG901Page;
                    break;
                #endregion
                case "SystemAdmin.GroupAdmin":
                    SystemAdmin.GroupAdmin GroupAdminPage = new SystemAdmin.GroupAdmin();
                    pane.Content = GroupAdminPage;
                    break;

                case "STT911":
                    STT.STT911 stt911 = new STT.STT911();
                    pane.Content = stt911;
                    break;

                case "Post_production":
                    STB.STB200 stb200 = new STB.STB200();
                    pane.Content = stb200;
                    break;
            }
        }



        private void setPane(string XAMLName, RadPane pane, string param)
        {

            switch (XAMLName)
            {
                case "Common.HtmlHost":
                    Common.HtmlHost ctr1 = new Common.HtmlHost(param);
                    pane.Content = ctr1;
                    break;

                case "SEARCH":
                    AllSearch.Search searchMainPage = new AllSearch.Search(this);
                    pane.Content = searchMainPage;
                    break;

                case "SEARCHRESULT":
                    PTS_MAM3.SRH.SRH107 searchResultMainPage = new PTS_MAM3.SRH.SRH107(param, this);
                    pane.Content = searchResultMainPage;
                    break;

                case "SEARCH_PREBOOKING":
                    PTS_MAM3.SRH.SRH126 search_prebooking = new PTS_MAM3.SRH.SRH126(param, this);
                    pane.Content = search_prebooking;
                    break;

                case "PROG_D":
                    ProgData.PROG_D ctrPROG_D = new ProgData.PROG_D(param);
                    pane.Content = ctrPROG_D;
                    break;

                case "PROG_LINK":
                    ProgData.PROGLINK ctrPROG_LINK = new ProgData.PROGLINK(param);
                    pane.Content = ctrPROG_LINK;
                    break;

                case "PROG_MEN":
                    ProgData.PROGMEN ctrPROG_MEN = new ProgData.PROGMEN(param);
                    pane.Content = ctrPROG_MEN;
                    break;

                case "PROG_RACE":
                    ProgData.PROGRACE ctrPROG_RACE = new ProgData.PROGRACE(param);
                    pane.Content = ctrPROG_RACE;
                    break;

                case "PROG_PRODUCER":
                    ProgData.PRO600 ctrPROG_PRODUCER = new ProgData.PRO600(param);
                    pane.Content = ctrPROG_PRODUCER;
                    break;
            }
        }


        private void setPane(string XAMLName, RadPane pane, object param)
        {

            switch (XAMLName)
            {
                case "DELETE_SEARCH_RESULT":
                    MAG.MAG801 MAG801 = new MAG.MAG801(this, param);
                    pane.Content = MAG801;
                    break;

                case "SEARCH_DELETE_SEARCH_RESULT":
                    MAG.MAG803 MAG803 = new MAG.MAG803(this, param);
                    pane.Content = MAG803;
                    break;

                
                case "SendBro":
                    STB.STB100 stb100 = new STB.STB100((PTS_MAM3.STB.FormType)param);
                    pane.Content = stb100;
                    break;

            }
        }

        #endregion

        #region deltaFlow GetApplyList
        private void dfGetApplyList(string userName)
        {
            flowWebService.FlowSoapClient myFlow = new flowWebService.FlowSoapClient();
        }


        #endregion






        //public void UpdateSession(object obj)
        //{


        //    WSASPNetSession.WSASPNetSessionSoapClient ASPSession = new WSASPNetSession.WSASPNetSessionSoapClient();


        //    ASPSession.GetUserSessionCompleted += (s, args) =>
        //    {
        //        if (args.Error == null)
        //        {
        //            if (args.Result != null)
        //            {
        //                List<WSASPNetSession.UserStruct> SessionList = args.Result.ToList();
        //                //user = (ASPNetSessionSvc.UserStruct)(SessionList.FirstOrDefault());
        //                ///UserClass u = new UserClass();

        //                //if (UserClass.userData != null)
        //                //{
        //                //    MessageBox.Show(UserClass.userData.FSUSER_ID); 
        //                //}

        //                UserObjectSvc.UserObjectClient UserObj = new UserObjectSvc.UserObjectClient();

        //                UserObj.UpdTBUserSessionCompleted += (s1, args1) =>
        //                {
        //                    // do nothing 
        //                };


        //                UserObj.UpdTBUserSessionAsync(UserClass.userData.FSUSER_ID);

        //                UserClass.userData = (WSASPNetSession.UserStruct)(SessionList.FirstOrDefault());


        //            }
        //            else
        //            {
        //                LogingForm LoginForm1 = new LogingForm();


        //                LoginForm1.Show();
        //            }

        //        }
        //    };

        //    ASPSession.GetUserSessionAsync("USEROBJ");
        //}

        public void _UpdUserAccessTimer_Tick(Object o, EventArgs sender)
        {
            _UpdUserAccessTimer.Stop();
            WSUserObject.WSUserObjectSoapClient UserObj = new WSUserObject.WSUserObjectSoapClient();

            UserObj.UpdTBUserSessionCompleted += (s2, args2) =>
            {
                _UpdUserAccessTimer.Start();
            };


            UserObj.UpdTBUserSessionAsync(UserClass.userData.FSUSER_ID);



        }

        public void moduleTimer_Tick(Object o, EventArgs sender)
        {
            if (ModuleClass.moduleList.Count > 0)
            {
                moduleTimer.Stop();

                chkPermission();

                if (ModuleClass.getModulePermission(""))
                {

                }
            }
        }
        System.Windows.Threading.DispatcherTimer moduleTimer = new System.Windows.Threading.DispatcherTimer();
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

            LogingForm LoginForm1 = new LogingForm();



            //System.IO.FileInfo fin = new FileInfo(u.AbsolutePath); 


            LoginForm1.Show();
            //Login complete
            LoginForm1.Closed += (s, args) =>
            {



                if (UserClass.userData == null)
                { }
                else
                {
                    this.Opacity = 1;
                    moduleTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
                    moduleTimer.Tick += new EventHandler(moduleTimer_Tick);
                    moduleTimer.Start();



                    //_SessionTimer = new DispatcherTimer();
                    //_SessionTimer.Interval = TimeSpan.FromMilliseconds(5000);
                    //_SessionTimer.Tick += _SessionTimer_Tick;
                    //_SessionTimer.Start(); 
                    WSASPNetSession.WSASPNetSessionSoapClient ASPSession = new WSASPNetSession.WSASPNetSessionSoapClient();


                    ASPSession.GetUserSessionCompleted += (s1, args1) =>
                    {
                        try
                        {
                            if (args1.Error == null)
                            {
                                if (args1.Result != null)
                                {
                                    List<WSASPNetSession.UserStruct> SessionList = args1.Result.ToList();


                                    WSUserObject.WSUserObjectSoapClient UserObj = new WSUserObject.WSUserObjectSoapClient();

                                    UserObj.UpdTBUserSessionCompleted += (s2, args2) =>
                                    {
                                        // do nothing 
                                    };


                                    UserObj.UpdTBUserSessionAsync(UserClass.userData.FSUSER_ID);

                                    if ((WSASPNetSession.UserStruct)(SessionList.FirstOrDefault()) != null)
                                    {
                                        UserClass.userData = (WSASPNetSession.UserStruct)(SessionList.FirstOrDefault());
                                    }

                                    _UpdUserAccessTimer = new DispatcherTimer();
                                    _UpdUserAccessTimer.Interval = new TimeSpan(0, 0, 0, 0, 10000);
                                    _UpdUserAccessTimer.Tick += new EventHandler(_UpdUserAccessTimer_Tick);
                                    _UpdUserAccessTimer.Start();



                                }


                            }

                        }
                        catch (Exception ex)
                        {

                            MessageBox.Show("網路連線異常，請通知系統管理員！" + ex.ToString(), "提示", MessageBoxButton.OK);
                            ////登出系統
                            //Uri u = new Uri("index.htm", UriKind.Relative);
                            //HtmlPage.Window.Navigate(u);

                        }


                    };

                    ASPSession.GetUserSessionAsync("USEROBJ");





                    dfGetApplyList(UserClass.userData.FSUSER_ID);
                    // dfGetApplyList(UserClass.userData.FSUSER);
                    generateNewPane("待簽核表單", "DFGetTTLPage", false);
                    StackPanel sp = (StackPanel)this.btnLogout.Content;

                    ((TextBlock)((StackPanel)this.btnLogout.Content).Children[1]).Text = "[" + UserClass.userData.FSUSER_ChtName + "]登出系統";


                    //訊息跑馬
                    this.marquee.Duration = 25;
                }

                chkPermission();



            };

        }




        private void treeViewWaitingSignForm_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("待簽核表單", "DFGetTTLPage", false);
        }

        private void treeViewFlowIDCheck_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("流程單號查詢", "DFGetFlowIDPage", false);
        }

        private void treeViewAgentAssign_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("代理人指派", "FLOAgent_List", false);
            //代理人指派
            //HtmlPage.Window.Navigate(new Uri(PTSMAMResource.ToSetAgentForm + UserClass.userData.FSUSER_ID, UriKind.Absolute), "_blank", "height=800,width=1024,top=100,left=100,toolbar=0,statusbar=0,menubar=0,scrollbar=0,resizable=yes");
        }


        //成案資料維護
        private void treeViewPROPOSAL_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("成案資料", "PROPOSAL", false);
        }

        //節目基本資料維護
        private void treeViewPROG_M_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("基本資料", "PROG_M", false);
        }

        //節目子集資料維護
        private void treeViewPROG_D_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("子集資料", "PROG_D", false);
        }

        //節目衍生子集資料維護
        private void treeViewPROG_LINK_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("衍生節目子集", "PROG_LINK", false);
        }

        //短帶資料維護
        private void treeViewPROMO_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("短帶資料", "PROMO", false);
        }


        //節目相關人員資料維護
        private void treeViewPROG_MEN_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("相關人員", "PROG_MEN", false);
        }

        //節目參展記錄維護
        private void treeViewPROG_RACE_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("參展記錄", "PROG_RACE", false);
        }

        //節目及短帶製作人維護
        private void treeViewPROG_PRODUCER_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("製作人資料", "PROG_PRODUCER", false);
        }

        //工作人員資料維護
        private void treeViewPROG_STAFF_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("工作人員資料", "PROG_STAFF", false);
        }

        //製作目的與前後會部門對照資料維護
        private void treeViewPROGOBJ_DEPT_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("製作目的與前後會部門對照資料", "PRO100_09", false);
        }

        //送帶轉檔資料維護
        private void treeViewBro_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("送帶轉檔資料", "Bro", false);
        }

       

        //送帶轉檔置換資料維護
        private void treeViewBroChange_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("送帶轉檔置換資料", "BroChange", false);
        }

        //送帶轉檔置換資料管理維護
        private void treeViewBroChangeManagement_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("送帶轉檔置換資料管理", "BroChangeManagement", false);
        }

        //轉檔中心線上簽收
        private void treeViewCheckIn_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("轉檔中心線上簽收", "CheckIn", false);
        }

        //轉檔中心轉檔(影帶擷取、數位轉檔)
        private void treeViewTransCode_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("轉檔中心轉檔", "TransCode", false);
        }

        //轉檔中心進度查詢
        private void treeViewTransCodeProgress_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("轉檔進度查詢", "TransCodeProgress", false);
        }

        //影帶回溯
        private void HistoryTape_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("影帶回溯", "HistoryTape", false);
        }

        //入庫資料維護
        private void treeViewArchive_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("入庫資料", "ArchiveData", false);
        }

        private void STO102_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("入庫單管理", "STO102", false);
        }

        private void STO103_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("入庫置換單管理", "STO103", false);
        }

        private void STO700_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("入庫瀏覽管理", "STO700", false);
        }

        //入庫置換維護
        private void treeViewArchive_EXCHANGE_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("入庫資料置換", "ArchiveExchangeData", false);
        }

        //檢索
        private void treeViewCode_SEARCH_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("全文檢索(部分調用)", "SEARCH", false);
        }

        //調用申請單
        private void treeViewCode_PROGRAM_BOOKING_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("整集調用", "PROGRAM_BOOKING", false);
        }

        //已加入預借項目
        private void treeViewCode_SEARCH_PREBOOKING_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("已加入清單項目", "SEARCH_PREBOOKING", "1", false);
        }

        //熱門關鍵字
        private void treeViewCode_HOTKEYWORD_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("熱門關鍵字", "HOTKEYWORD", false);
        }

        //熱門點閱
        private void treeViewCode_HOTHIT_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("熱門點閱", "HOTHIT", false);
        }

        //查詢記錄
        private void treeViewCode_SEARCH_HISTORY_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("查詢記錄", "SEARCH_HISTORY", false);
        }

        //使用者調用清單查詢
        private void treeViewCode_USER_BOOKING_HISTORY_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("查詢調用清單", "USER_BOOKING_HISTORY", false);
        }

        //調用清單查詢
        private void treeViewCode_BOOKING_HISTORY_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("查詢系統調用清單", "BOOKING_HISTORY", false);
        }


        //Log 紀錄查詢
        private void treeViewCode_Log_HISTORY_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            //treeViewCode_BOOKING_HISTORY_MouseLeftButtonUp
            //HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=http://10.13.210.5/Reports/Pages/Report.aspx?ItemPath=%2fPTS_MAM3%2fRPT_TBTRAN_LOG_01", UriKind.Relative), "_blank", "height=700,width=900,top=100,left=100,toolbar=0,statusbar=0,menubar=0,scrollbar=0");
            generateNewPane("LOG記錄檢視", "RPT_LOG_01", false);
        }


        private void treeViewCode_FTP_01_HISTORY_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("主控FTP容量明細表", "RPT_FTP_01", false);
        }


        private void treeViewCode_FTP_02_HISTORY_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("主控FTP容量統計表", "RPT_FTP_02", false);
        }


        private void treeViewCode_BOOKNG_ANALYSIS_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("調用分析", "RPT_BOOKING_01", false);
        }

        private void treeViewCode_BOOKNG_CHANNEL_ANALYSIS_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("調用頻道分析", "RPT_BOOKING_02", false);
        }

        private void treeViewCode_STO_ANALYSIS_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("入庫分析", "RPT_STO_01", false);
        }
        //treeViewCode_RPT_STO_200
        private void treeViewCode_RPT_STO_200(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("節目入庫清單/停製通知單", "RPT_STO_02", false);
        }
        private void treeViewCode_BOOKNG_REASON_CHANNEL_ANALYSIS_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("各頻道調用原因分析", "RPT_BOOKING_03", false);
        }


        private void treeView_PROGRAM_VIDEO_DETAIL_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("節目檔案明細表", "RPT_PROGRAM_VIDEO_DETAIL", false);
        }

        

        //Log 紀錄查詢
        //private void treeViewCode_Logtest_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        //{
        //    generateNewPane("LOG記錄檢視", "RPT_LOG_01", false);
        //}

        //同義詞查詢
        private void treeViewCode_SEARCH_SYNONYM_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("同義詞查詢與維護", "SEARCH_SYNONYM", false);
        }

        //磁帶上架通知檢視
        private void treeViewCode_WAIT_TAPE_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("TS3500磁帶上架通知檢視", "WAIT_TAPE", false);
        }

        //磁帶上下架管理
        private void treeViewCode_MANAGE_TAPE_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("TS3500磁帶上下架管理", "MAG606", false);
        }

        //磁帶異地存放管理
        private void treeViewCode_MANAGE_TAPE_REMOTE_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("TS3310磁帶異地存放管理", "MAG620", false);
        }

        // TSM磁帶回寫檢視作業
        private void treeViewCode_TSMRecallManagement_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("TSM磁帶回寫檢視作業", "ViewTsmRecall", false);
        }

        private void treeViewProcessingForm_Selected(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            generateNewPane("表單狀態查詢", "DFQueryActivity", false);
        }



        //treeViewSTTFormCheck_MouseLeftButtonUp
        private void treeViewSTTFormCheck_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("送帶轉檔單查詢", "送帶轉檔單查詢", false);
        }




        private void C_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("表單狀態查詢", "表單狀態查詢", false);
        }
        private void D_Selected(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            generateNewPane("抽單", "抽單", false);
        }

        #region 系統管理
        //使用者異動管理
        private void UserManage_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("使用者維護", "SystemAdmin.UserAdmin", false);
        }




        private void SystemMonitor_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("硬體效能監控", "SystemAdmin.SystemMonitor", false);
        }

        private void DirAdmin_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("自訂欄位維護", "SystemAdmin.DirTreeAdmin", false);
        }

        // 管理轉檔中心VTR列表
        private void MAG320_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("管理轉檔中心VTR列表", "MAG320", false);
        }

        //檢視轉檔作業流程
        private void MAG300_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("檢視轉檔作業流程", "MAG300", false);
        }

        // 檢視送播媒體檔轉檔狀態
        private void MAG310_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("檢視送播媒體檔轉檔狀態", "MAG310", false);
        }

        // 檢視送播媒體檔轉檔狀態
        private void MAG330_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("入庫媒體檔關鍵影格擷取管理介面", "MAG330", false);
        }

        // 檢視送播媒體檔派送狀態
        private void MAG400_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("檢視送播媒體檔派送狀態", "MAG400", false);
        }

        // 主控播出檔存放空間管理介面
        private void MAG500_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("主控播出檔存放空間管理介面", "MAG500", false);
        }

        // 主控播出檔FTP派送狀態介面
        private void MAG510_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("主控播出檔FTP派送狀態介面", "MAG510", false);
        }

        // 主控播出檔LOUTH寫入狀態介面
        private void MAG520_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("主控播出檔LOUTH寫入狀態介面", "MAG520", false);
        }

        //群組系統權限管理
        private void ModuleAdmin_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("群組系統權限管理", "SystemAdmin.ModuleAdmin", false);
        }

        //手動刪除檔案作業
        private void treeViewCode_FILE_DELETE_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("手動刪除檔案作業", "FILE_DELETE", false);
        }

        //查詢刪除檔案記錄
        private void treeViewCode_SEARCH_FILE_DELETE_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("查詢刪除檔案記錄", "SEARCH_FILE_DELETE", false);
        }
        #endregion

        #region 冠宇的單檔維護按鈕設定

        private void btn_TBZPROMOVER(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("短帶版本代碼檔維護", "TBZPROMOVER", false);
        }
        private void btn_TBZSHOOTSPEC(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("拍攝規格代碼檔維護", "TBZSHOOTSPEC", false);
        }
        private void btn_TBPGM_ACTION(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("活動代碼檔維護", "TBPGM_ACTION", false);
        }
        private void btn_TBZPROMOTYPE(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("短帶類別代碼檔維護", "TBZPROMOTYPE", false);
        }
        private void btn_TBZPROMOCAT(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("短帶類型代碼檔維護", "TBZPROMOCAT", false);
        }
        private void btn_TBZPROMOCATD(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("短帶類型細項代碼檔維護", "TBZPROMOCATD", false);
        }

        private void btn_TBARCHIVE_SET_CLASS(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("節目入庫樣板管理", "TBARCHIVE_SET_CLASS", false);
        }

        private void btn_TBARCHIVE_SET(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("節目入庫項目管理", "TBARCHIVE_SET", false);
        }

        private void btn_TBZPROGOBJ(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("製作目的代碼檔維護", "TBZPROGOBJ", false);
        }
        private void btn_TBZPROGSTATUS(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("參展狀態代碼檔維護", "TBZPROGSTATUS", false);
        }
        private void btn_TBZTITLE(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("頭銜代碼檔維護", "TBZTITLE", false);
        }
        private void btn_TBZDEPT(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("部門代碼檔維護", "TBZDEPT", false);
        }
        private void btn_TBZPROGSRC(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("節目來源代碼檔維護", "TBZPROGSRC", false);
        }
        private void btn_TBZPROGATTR(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("內容屬性代碼檔維護", "TBZPROGATTR", false);
        }
        private void btn_TBZPROGAUD(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("目標觀眾代碼檔維護", "TBZPROGAUD", false);
        }
        private void btn_TBZPROGTYPE(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("表現方式代碼檔維護", "TBZPROGTYPE", false);
        }
        private void btn_TBZPROGLANG(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("語言代碼檔維護", "TBZPROGLANG", false);
        }
        private void btn_TBZPROGBUY(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("外購類別代碼檔維護", "TBZPROGBUY", false);
        }
        private void btn_TBZPROGBUYD(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("外購類別細項代碼檔維護", "TBZPROGBUYD", false);
        }
        private void btn_TBZPROGGRADE(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("節目分級代碼檔維護", "TBZPROGGRADE", false);
        }
        private void btn_TBFILE_TYPE(object sender, MouseButtonEventArgs e)
        { generateNewPane("入庫檔案類型代碼檔維護", "TBFILE_TYPE", false); }

        private void btn_TBZBOOKING_REASON(object sender, MouseButtonEventArgs e)
        { generateNewPane("調用原因代碼檔維護", "TBZBOOKING_REASON", false); }

        private void btn_TBZSIGNAL(object sender, MouseButtonEventArgs e)
        { generateNewPane("訊號來源代碼檔維護", "TBZSIGNAL", false); }

        private void btn_TBZPROGSPEC(object sender, MouseButtonEventArgs e)
        { generateNewPane("節目規格代碼檔維護", "TBZPROGSPEC", false); }

        private void btn_TBZCODE(object sender, MouseButtonEventArgs e)
        { generateNewPane("自訂欄位代碼檔維護", "TBZCODE", false); }

        private void btn_TBZPROGRACE_AREA(object sender, MouseButtonEventArgs e)
        { generateNewPane("參展區域代碼檔維護", "TBZPROGRACE_AREA", false); }

        private void btn_TBZPROGNATION(object sender, MouseButtonEventArgs e)
        { generateNewPane("來源國家代碼檔維護", "TBZPROGNATION", false); }

        #endregion
        private void GroupManage_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("群組維護", "SystemAdmin.GroupAdmin", false);
        }

        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            Common.chWndMessageBox chMsgWnd = new Common.chWndMessageBox("提示", "是否確認登出系統？", 0);
            chMsgWnd.Show();
            chMsgWnd.Closing += (s, args) =>
            {
                if (chMsgWnd.DialogResult == true)
                {
                    //登出系統
                    Uri u = new Uri("", UriKind.Relative);
                    HtmlPage.Window.Navigate(u);
                }

            };
        }

        #region 排表系統畫面產生
        private void treeViewLouthKey_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("匯入LouthKey", "匯入LouthKey", false);
        }
        private void treeViewPGMQUEUE_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("節目表維護", "節目表維護", false);
        }

        private void treeViewPromoPlan_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("破口資源預約", "破口資源預約", false);
        }

        private void treeViewPromoChannelLink_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {

            generateNewPane("頻道預排短帶", "頻道預排短帶", false);
        }

        private void treeViewPromoProgLink_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (ModuleClass.getModulePermission("P2" + "01" + "002") == false)
                MessageBox.Show("您沒有維護節目預排短帶的權限");
            else
                generateNewPane("節目預排短帶", "節目預排短帶", false);
        }

        private void treeViewPromoBooking_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("短帶託播單", "短帶託播單", false);
        }

        private void treeViewInsertPromo_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("插入短帶", "插入短帶", false);
        }

        #endregion


        private void RadTreeView_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {

        }


        private void MainFrame1_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            //STO.STO200 frm1 = new STO.STO200("G201100100010491");
            //frm1.Show();
            STO.STO300 sto300 = new STO.STO300("V", "G201100100010491", "0");
            sto300.Show();
        }

        WSFLOWDLL.WSFLOWDLLSoapClient WSFLOWDLLClient = new WSFLOWDLL.WSFLOWDLLSoapClient();

        /// <summary>
        /// 管理流程後台 流程規劃設計、轉派、抽單
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FlowAdminHandler_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            //開啟流程引擎的後台
            myFlowLogin.GetConfirmCodeForBackSiteAsync(UserClass.userData.FSUSER_ID);
            //等冠宇有空再來查，第一次是正常，但是當呼叫第二次以後，在接收流程的web services就會出錯，我先讓程式看起來沒問題

            //WSFLOWDLLClient.FlowBackOfficeAsync(UserClass.userData.FSUSER_ID);
            //WSFLOWDLLClient.FlowBackOfficeCompleted += (s, args) =>
            //    {
            //    //http://localhost/PTS_MAM3.Web/FLOWBACK.aspx?url=
            //        //HtmlPage.Window.Navigate(new Uri(@"http://localhost/PTS_MAM3.Web/FLOWBACK.aspx?url=http://10.13.220.9/deltaflow/mainlogin.aspx?uid=" + UserClass.userData.FSUSER_ID + @"&confirmcode=" + args.Result, UriKind.Absolute), "_blank", "height=600,width=800,top=100,left=100,toolbar=0,statusbar=0,menubar=0,scrollbar=0");
            //        HtmlPage.Window.Navigate(new Uri(@"FLOWBACK.aspx?url=" + args.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,toolbar=0,statusbar=0,menubar=0,scrollbar=0");
            //        //HtmlPage.Window.Navigate(new Uri( args.Result , UriKind.Absolute), "_blank", "height=600,width=800,top=100,left=100,toolbar=0,statusbar=0,menubar=0,scrollbar=0");
            //    };
        }
        //實作-開啟流程引擎後台
        void myFlowLogin_GetConfirmCodeForBackSiteCompleted(object sender, flowWebService.GetConfirmCodeForBackSiteCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != "" && e.Result != null)
                    HtmlPage.Window.Navigate(new Uri(@"http://10.13.220.35/deltaflow/mainlogin.aspx?uid=" + UserClass.userData.FSUSER_ID + "&confirmcode=" + e.Result, UriKind.Absolute), "_blank", "height=600,width=800,top=100,left=100,toolbar=0,statusbar=0,menubar=0,scrollbar=0,resizable=yes");
            }
        }

        //流程補作
        private void FlowReDoHandler_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("流程補作管理", "FlowReDo", false);
        }

        private void btnDoc_Click(object sender, RoutedEventArgs e)
        {
            //HtmlPage.Window.Navigate(new Uri(@"\\10.1.253.189\MAM_System", UriKind.Absolute), "_blank", "resizable=yes"); //最一開始開啟下載區域
            //HtmlPage.Window.Navigate("http://mam.pts.org.tw/MAM系統操作手冊.doc");  //直接連結下載文件，但是好像會有問題
            //client.CallREPORT_DOCAsync();   //呼叫文件開啟，因為會自己關掉，所以這個方法也不行  

            if (HtmlPage.IsPopupWindowAllowed)
                HtmlPage.PopupWindow(new Uri("http://mam.pts.org.tw/MAM系統操作手冊.doc"), "_blank", null);
            else
                HtmlPage.Window.Navigate(new Uri("http://mam.pts.org.tw/MAM系統操作手冊.doc", UriKind.Absolute), "_blank");
        }
        //實作-呼叫文件開啟
        void client_CallREPORT_DOCCompleted(object sender, WSPROPOSAL.CallREPORT_DOCCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    if (HtmlPage.IsPopupWindowAllowed)
                        HtmlPage.PopupWindow(new Uri("REPORT.aspx?url=" + e.Result), "_blank", null);
                    else
                        HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + e.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,resizable=yes");
                }
                else
                    MessageBox.Show("查詢文件資料異常", "提示訊息", MessageBoxButton.OK);
            }

            //HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + e.Result, UriKind.Relative), "_blank");       
        }

        private void treeViewPromoBookingM_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("短帶託播單維護", "短帶託播單維護", false);
        }

        private void t_COMBINE_QUEUE_ZONE_PLAY_FILE_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("區間節目檔案播出資訊表", "RPT_COMBINE_QUEUE_ZONE_PLAY_FILE", false);
        }

        private void MAG340Handler_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("查詢TC微調紀錄", "MAG340", false);

        }

        private void treeViewExportProgList_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("轉出節目表", "轉出節目表", false);
        }

        private void STT911Handler_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("其他影片", "STT911", false);
        }

        private void treeViewSendBro_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("檔案送播資料", "SendBro", PTS_MAM3.STB.FormType.Normal, false);
        }

        private void SendBroReplaceHandler_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("檔案送播置換資料", "SendBro", PTS_MAM3.STB.FormType.Displacement, false);

        }

        private void Post_productionHandler_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("後製控制中心", "Post_production", false);

        }

        private void treeViewQueryPlayList_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("查詢節目表檔案狀態", "查詢節目表檔案狀態", false);
        }

        private void treeViewCopyPlayListSDToHD_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            generateNewPane("複製節目表", "複製節目表", false);
        }



        



        //關閉視窗前的檢查
        //private void RadDocking_PreviewClose(object sender, Telerik.Windows.Controls.Docking.StateChangeEventArgs e)
        //{


        //    if (MessageBox.Show("是否確定關閉視窗? ", "提示", MessageBoxButton.OKCancel) == MessageBoxResult.Cancel)
        //    {
        //        e.Handled = true;
        //    }
        //    else
        //    {
        //        e.Handled = false; 
        //    }
        //}



    }
}