﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace PTS_MAM3.MAG
{
    public partial class MAG200_06 : ChildWindow
    {
        List<WSUserAdmin.Class_Dept> DeptList = new List<WSUserAdmin.Class_Dept>();
        WSUserAdmin.WSUserAdminSoapClient myService1 = new WSUserAdmin.WSUserAdminSoapClient();
        WSUserAdmin.WSUserAdminSoapClient myService2 = new WSUserAdmin.WSUserAdminSoapClient();
        //WSUserAdminII.WSUserAdminIISoapClient myService3 = new WSUserAdminII.WSUserAdminIISoapClient();
        List<WSUserAdmin.UserStruct> ListBox2_UsersList = new List<WSUserAdmin.UserStruct>();
        List<WSUserAdmin.UserStruct> ListBox1_UsersList = new List<WSUserAdmin.UserStruct>();
        ObservableCollection<WSUserAdmin.UserStruct> FinalList1 = new ObservableCollection<WSUserAdmin.UserStruct>();
        ObservableCollection<WSUserAdmin.UserStruct> FinalList2 = new ObservableCollection<WSUserAdmin.UserStruct>();
        public MAG200_06()
        {
            InitializeComponent();
            //人員部門修改完成事件
            myService1.UpdUser_DEP_MAMCompleted += (M, ARGM) =>
            {
                if (ARGM.Result == true)
                {
                    MessageBox.Show("修改完成");
                    BI.IsBusy = false;
                    this.DialogResult = true;
                }
                else
                    BI.IsBusy = false;
            };
            myService1.GetDeptListAllCompleted += (s, args) =>
            {
                DeptList = args.Result.ToList();
                foreach (var item in DeptList)
                {
                    ComboBoxItem cbi1 = new ComboBoxItem();
                    ComboBoxItem cbi2 = new ComboBoxItem();
                    cbi1.Content = item.FSFullName_ChtName;
                    cbi1.Tag = item;
                    cbi2.Content = item.FSFullName_ChtName;
                    cbi2.Tag = item;
                    
                    comboBox1.Items.Add(cbi1);
                    comboBox2.Items.Add(cbi2);
                }
            };
            myService1.GetDeptListAllAsync();
            myService2.GetUserListByTreeViewDeptCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        ListBox2_UsersList = args.Result.ToList();
                    }
                    foreach (var item in ListBox2_UsersList)
                    {
                        ListBoxItem myLBI = new ListBoxItem();
                        myLBI.Tag = item;
                        myLBI.Content = item.FSUSER_ChtName;
                        if (item.FCACTIVE == "1")
                        {
                            listBox2.Items.Add(myLBI);
                        }
                    }
                }
            };
            myService1.GetUserListByTreeViewDeptCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        ListBox1_UsersList = args.Result.ToList();
                    }
                    foreach (var item in ListBox1_UsersList)
                    {
                        ListBoxItem myLBI = new ListBoxItem();
                        myLBI.Tag = item;
                        myLBI.Content = item.FSUSER_ChtName;
                        if (item.FCACTIVE == "1")
                        {
                            listBox1.Items.Add(myLBI);
                        }
                    }
                }
            };
        }


        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            BI.IsBusy = true;
            foreach (var item in listBox1.Items)
            {
                
                FinalList1.Add(((WSUserAdmin.UserStruct)((ListBoxItem)item).Tag));
            }
            foreach (var item in listBox2.Items)
            {
                FinalList2.Add(((WSUserAdmin.UserStruct)((ListBoxItem)item).Tag));
            }
            //listBox1.Items
            myService1.UpdUser_DEP_MAMAsync(FinalList1, FinalList2, ((WSUserAdmin.Class_Dept)((ComboBoxItem)(comboBox1.SelectedItem)).Tag).FNDEP_ID, ((WSUserAdmin.Class_Dept)((ComboBoxItem)(comboBox2.SelectedItem)).Tag).FNDEP_ID, UserClass.userData.FSUSER_ID.ToString());

            
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void comboBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //preLoadUserListBox1(((WSUserAdmin.Class_Dept)((ComboBoxItem)sender).Tag).FNDEP_ID);
            preLoadUserListBox1(((WSUserAdmin.Class_Dept)(((ComboBoxItem)comboBox1.SelectedItem).Tag)).FNDEP_ID);
            listBox1.Items.Clear();
            comboBox1.IsEnabled = false;
        }

        private void comboBox2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            preLoadUserListBox2(((WSUserAdmin.Class_Dept)(((ComboBoxItem)comboBox2.SelectedItem).Tag)).FNDEP_ID);
            listBox2.Items.Clear();
            comboBox2.IsEnabled = false;
            //preLoadUserListBox1(((WSUserAdmin.Class_Dept)((ComboBoxItem)sender).Tag).FNDEP_ID);
        }
        private void preLoadUserListBox1(int FNDEP_ID)
        {

            ListBox1_UsersList.Clear();
            myService1.GetUserListByTreeViewDeptAsync(FNDEP_ID.ToString());
        }
        private void preLoadUserListBox2(int FNDEP_ID)
        {

            ListBox2_UsersList.Clear();
            myService2.GetUserListByTreeViewDeptAsync(FNDEP_ID.ToString());
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            ListBoxItem LBI=new ListBoxItem();
            LBI = (ListBoxItem)listBox1.SelectedItem;
            if (LBI != null)
            {
                System.Windows.Media.SolidColorBrush mycolor = new SolidColorBrush(Colors.Blue);
                LBI.Foreground = mycolor;
                listBox1.Items.Remove(LBI);
                listBox2.Items.Add(LBI);
            }
            else
                MessageBox.Show("請選擇人員");
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            ListBoxItem LBI = new ListBoxItem();
            LBI = (ListBoxItem)listBox2.SelectedItem;
            if (LBI != null)
            {
                System.Windows.Media.SolidColorBrush mycolor = new SolidColorBrush(Colors.Blue);
                LBI.Foreground = mycolor;
                listBox2.Items.Remove(LBI);
                listBox1.Items.Add(LBI);
            }
            else
                MessageBox.Show("請選擇人員");
            
        }
    }
}

