﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.Web.ENT;

namespace PTS_MAM3.MAG
{
    public partial class MAG100_02 : ChildWindow
    {
        public Object senderObj;
        string tbName;
        public MAG100_02(string InputtbName, Object sender)
        {
            InitializeComponent();
            senderObj = sender;
            tbName = InputtbName;
            switch (tbName)
            {
                case "TBFILE_TYPE":
                    textBox1.Text = ((TBFILE_TYPE)senderObj).FSTYPE_NAME;
                    textBox2.Text = ((TBFILE_TYPE)senderObj).FSSORT;
                    checkBox1.IsChecked = ((TBFILE_TYPE)senderObj).FBISENABLE;
                    for (int i = 0; i < comboBox1.Items.Count; i++)
                    {
                        ComboBoxItem getItem = (ComboBoxItem)comboBox1.Items.ElementAt(i);
                        if (getItem.Content.Equals(((TBFILE_TYPE)senderObj).FSTYPE))
                        { 
                            comboBox1.SelectedIndex = i;
                            break;
                        }
                    }
                break;
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            int intCheck;
            if (int.TryParse(textBox2.Text.Trim(), out intCheck) && intCheck > 0 && textBox1.Text.Count() > 0)
            {
                switch (tbName)
                {
                    case "TBFILE_TYPE":
                        ((TBFILE_TYPE)senderObj).FSTYPE_NAME = textBox1.Text.Trim();
                        ((TBFILE_TYPE)senderObj).FSSORT = textBox2.Text.Trim();
                        ((TBFILE_TYPE)senderObj).FSTYPE = ((ComboBoxItem)comboBox1.SelectedItem).Content.ToString();
                        ((TBFILE_TYPE)senderObj).FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                        ((TBFILE_TYPE)senderObj).FDUPDATED_DATE = DateTime.Now;
                        ((TBFILE_TYPE)senderObj).FBISENABLE = checkBox1.IsChecked;
                        break;
                }
                this.DialogResult = true;
            }
            else
            {
                if (int.TryParse(textBox2.Text.Trim(), out intCheck) != true || textBox2.Text.Trim() == "" || intCheck == 0)
                { MessageBox.Show("顯示排序欄位請填入大於0以及小於或等於999的\"數字\"!"); }
                if (textBox1.Text.Trim().Count() == 0)
                { MessageBox.Show("製作目的名稱必須填入資料!"); }
            }

        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

