﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;

namespace PTS_MAM3.MAG
{
    public class MAG102_Converter:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string strStatus = value.ToString(); //需要轉換的值
            if (!string.IsNullOrEmpty(strStatus))
                if (strStatus.Equals("19999")) //當值等於19999時為預設
                {
                    return "節目預設";//傳回男生圖片路徑
                }
            return strStatus;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }
}
