﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSDELETE;

namespace PTS_MAM3.MAG
{
    public partial class MAG800 : UserControl
    {
        private WSDELETESoapClient wsDeleteClient = new WSDELETESoapClient();
        private Class_DELETED_PARAMETER _class_deleted_parameter = new Class_DELETED_PARAMETER();
        private PTS_MAM3.MainFrame parentFrame;

        public MAG800(PTS_MAM3.MainFrame mainframe)
        {
            InitializeComponent();
            parentFrame = mainframe;

            //取得台別
            wsDeleteClient.GetChannelListCompleted += new EventHandler<GetChannelListCompletedEventArgs>(wsDeleteClient_GetChannelListCompleted);
            wsDeleteClient.GetChannelListAsync();

            //取得檔案狀態
            wsDeleteClient.GetFileStatusListCompleted += new EventHandler<GetFileStatusListCompletedEventArgs>(wsDeleteClient_GetFileStatusListCompleted);
            wsDeleteClient.GetFileStatusListAsync();

            this.RadBusyIndicator.IsBusy = true;
        }

        void wsDeleteClient_GetChannelListCompleted(object sender, GetChannelListCompletedEventArgs e)
        {
            this.RadBusyIndicator.IsBusy = false;

            if (e.Error != null || e.Result == null)
            {
                MessageBox.Show("載入台別錯誤!", "訊息", MessageBoxButton.OK);
                return; 
            }
            this.ListBox_Channel.ItemsSource = null;
            this.ListBox_Channel.ItemsSource = e.Result;
        }

        void wsDeleteClient_GetFileStatusListCompleted(object sender, GetFileStatusListCompletedEventArgs e)
        {
            if (e.Error != null || e.Result == null)
            {
                MessageBox.Show("載入檔案狀態錯誤!", "訊息", MessageBoxButton.OK);
                return;
            }
            this.ComboBox_File_Status.ItemsSource = null;
            this.ComboBox_File_Status.ItemsSource = e.Result;
            this.ComboBox_File_Status.SelectedIndex = 0;
        }

        private void RadioButton_Channel_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
            if ((sender as RadioButton).Tag != null)
            {
                _class_deleted_parameter._fschannel_id = (sender as RadioButton).Tag.ToString();
            }
        }

        private void Button_Search_Prog_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
            MAG800_01 mag800_01 = new MAG800_01(_class_deleted_parameter._fschannel_id,_class_deleted_parameter._fstype);
            mag800_01.Show();

            mag800_01.Closing += (s, args) =>
            {
                this.TextBox_Prog_ID.Text = mag800_01._fsprog_id;
                this.TextBox_Prog_Name.Text = mag800_01._fsprog_name;
            };
        }

        private void RadioButton_G_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
            wsDeleteClient.GetChannelListAsync();
            _class_deleted_parameter._fstype = "G";
            if (this.StackPanel_Channel != null && this.StackPanel_Channel_Title != null)
            {
                this.StackPanel_Channel.Visibility = System.Windows.Visibility.Visible;
                this.StackPanel_Channel_Title.Visibility = System.Windows.Visibility.Visible;
            }
            
        }

        private void RadioButton_P_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
            wsDeleteClient.GetChannelListAsync();
            _class_deleted_parameter._fstype = "P";
            if (this.StackPanel_Channel != null && this.StackPanel_Channel_Title != null)
            {
                this.StackPanel_Channel.Visibility = System.Windows.Visibility.Visible;
                this.StackPanel_Channel_Title.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void RadioButton_D_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
            wsDeleteClient.GetChannelListAsync();
            _class_deleted_parameter._fstype = "D";
            if (this.StackPanel_Channel != null && this.StackPanel_Channel_Title != null)
            {
                this.StackPanel_Channel.Visibility = System.Windows.Visibility.Collapsed;
                this.StackPanel_Channel_Title.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private void Button_Search_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
            if (string.IsNullOrEmpty(_class_deleted_parameter._fschannel_id))
            {
                MessageBox.Show("請選擇台別!", "訊息", MessageBoxButton.OKCancel);
                return;
            }
            if (!string.IsNullOrEmpty(this.TextBox_Prog_ID.Text))
            {
                if (string.IsNullOrEmpty(this.TextBox_Episode_Beg.Text) || string.IsNullOrEmpty(this.TextBox_Episode_Beg.Text))
                {
                    MessageBox.Show("請輸入集數!", "訊息", MessageBoxButton.OKCancel);
                    return;
                }
            }
            if (!this.DatePicker_Tran_Date_Beg.SelectedDate.HasValue || !this.DatePicker_Tran_Date_End.SelectedDate.HasValue)
            {
                MessageBox.Show("請輸入轉檔起迄日期!", "訊息", MessageBoxButton.OKCancel);
                return;
            }
            if (this.DatePicker_Tran_Date_Beg.SelectedDate > this.DatePicker_Tran_Date_End.SelectedDate)
            {
                MessageBox.Show("起迄日期前後錯誤!", "訊息", MessageBoxButton.OKCancel);
                return;
            }
            if (_class_deleted_parameter._fstype == "D")
            {
                _class_deleted_parameter._fschannel_id = "99";
            }
            _class_deleted_parameter._fsprog_id = this.TextBox_Prog_ID.Text;
            _class_deleted_parameter._fnepisode_beg = this.TextBox_Episode_Beg.Text;
            _class_deleted_parameter._fnepisode_end = this.TextBox_Episode_End.Text;
            _class_deleted_parameter._fsfile_no = this.TextBox_FileNo.Text;
            _class_deleted_parameter._fdtran_date_beg = this.DatePicker_Tran_Date_Beg.SelectedDate.Value.ToString("yyyy/MM/dd");
            _class_deleted_parameter._fdtran_date_end = this.DatePicker_Tran_Date_End.SelectedDate.Value.ToString("yyyy/MM/dd");
            if (this.ComboBox_File_Status.SelectedItem.ToString().IndexOf(':') == -1)
            {
                _class_deleted_parameter._fcfile_status = "";
            }
            else
            {
                _class_deleted_parameter._fcfile_status = this.ComboBox_File_Status.SelectedItem.ToString().Split(':')[0];
            }

            parentFrame.generateNewPane("刪除檔案查詢結果", "DELETE_SEARCH_RESULT", _class_deleted_parameter, false);
			//MessageBox.Show(this.ComboBox_File_Status.SelectedItem.ToString().Split(':')[0]);
        }

        private void RadioButton_Video_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
            _class_deleted_parameter._fsfile_type = "V";
        }

        private void RadioButton_Audio_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
            _class_deleted_parameter._fsfile_type = "A";
        }

        private void RadioButton_Photo_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
            _class_deleted_parameter._fsfile_type = "P";
        }

        private void RadioButton_Doc_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
            _class_deleted_parameter._fsfile_type = "D";
        }

    }
}
