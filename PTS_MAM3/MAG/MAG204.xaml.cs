﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using System.Text;
using System.Xml.Linq;
using System.IO;
using System.Collections.ObjectModel;

namespace PTS_MAM3.SystemAdmin
{
    public partial class ModuleAdmin : UserControl
    {
        public ModuleAdmin()
        {
            InitializeComponent();
            preLoadGroupList(-1, null, true);

            preloadModuleCatList("" );

            preloadModuleDetailList("", "");

           
        }


        private void preloadModuleDetailList(string FSMODULE_ID, string FSGROUP_ID)
        {
            if (string.IsNullOrEmpty(FSGROUP_ID))
            {
                lbxModuleDetail.IsEnabled = false;
                FSMODULE_ID = string.Empty; 
            }
            else
            {
                lbxModuleDetail.IsEnabled = true;
            }


            WSGroupAdmin.WSGroupAdminSoapClient GroupObj = new WSGroupAdmin.WSGroupAdminSoapClient();

            ObservableCollection<WSGroupAdmin.Class_Module_Detail> Module_Detail_List;

            GroupObj.fnGetTBMODULE_DETAILCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        lbxModuleDetail.ItemsSource = args.Result; 
                    }
                }

            };
           
            GroupObj.fnGetTBMODULE_DETAILAsync(FSMODULE_ID, FSGROUP_ID); 


        }

        private void preloadModuleCatList(string FSMOUDULE_CAT_ID)
        {

            WSGroupAdmin.WSGroupAdminSoapClient GroupObj = new WSGroupAdmin.WSGroupAdminSoapClient();
            ObservableCollection<WSGroupAdmin.Class_MODULE_CAT> Module_Cat_List;

            GroupObj.fnGetTBMODULE_CATCompleted += (e, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        Module_Cat_List = args.Result;

                        foreach (WSGroupAdmin.Class_MODULE_CAT catObj in Module_Cat_List)
                        {
                            RadTreeViewItem treeItem = new RadTreeViewItem();
                            treeItem.Header = catObj.FSMODULE_CAT_NAME.ToString();
                            treeItem.Tag = catObj;


                            ModuleCatNode.Items.Add(treeItem);  
                        }

                 
                    }
                }
            };

            GroupObj.fnGetTBMODULE_CATAsync(FSMOUDULE_CAT_ID); 

        }


        private void preLoadGroupList(int FNPARENT_ID, RadTreeViewItem treeNode, Boolean isFirst)
        {
            WSGroupAdmin.WSGroupAdminSoapClient GroupObj = new WSGroupAdmin.WSGroupAdminSoapClient();

            WSGroupAdmin.TBGroupStruct t = new WSGroupAdmin.TBGroupStruct();




            t.FSGROUP_ID = string.Empty; 
            
            GroupObj.fnGetTBGROUPSCompleted += (e, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        //StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));

                        var ltox = from s in doc.Elements("Datas").Elements("Data")
                                   select s;

                        
                                                
                       
                        TreeViewGroupList.IsLoadOnDemandEnabled = false;
                        foreach (XElement elem in ltox)
                        {
                            RadTreeViewItem treeItem = new RadTreeViewItem();
                            treeItem.Header = elem.Element("FSGROUP").Value.ToString();
                            treeItem.Tag = elem;
                            //treeItem.IsExpanded = true;
                            treeItem.IsLoadingOnDemand = false;     
                            GroupNode.Items.Add(treeItem);  
                           
                            //Text = elem.Element("FSFSGROUP").Value.ToString();
                        }
                        GroupNode.IsSelected = true;
                        GroupNode.IsExpanded = true;
                        GroupNode.IsLoadingOnDemand = false;
                       
                    }
                }

            };
            GroupObj.fnGetTBGROUPSAsync(t); 

        }
        private void TreeViewGroupList_LoadOnDemand(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {

        }

        private void TreeViewGroupList_Selected(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {

            prloadModCatGroup(); 
        }


        //切換ModuleCat
        private void TreeViewModuleCatList_Selected(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {

            prloadModCatGroup(); 
        }


        private void prloadModCatGroup()
        {
            string module_cat = string.Empty;
            string fsgroup_id = string.Empty;

            if (((RadTreeViewItem)(TreeViewModuleCatList.SelectedItem)).Tag == null)
            {
                module_cat = string.Empty;
            }
            else
            {
                module_cat = ((WSGroupAdmin.Class_MODULE_CAT)(((RadTreeViewItem)(TreeViewModuleCatList.SelectedItem)).Tag)).FSMOUDULE_CAT_ID;
            }


            if (((RadTreeViewItem)(TreeViewGroupList.SelectedItem)).Tag == null)
                fsgroup_id = string.Empty;
            else
                fsgroup_id = ((XElement)(((RadTreeViewItem)(TreeViewGroupList.SelectedItem)).Tag)).Element("FSGROUP_ID").Value.ToString();

            preloadModuleDetailList(module_cat, fsgroup_id); 
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            prloadModCatGroup();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            //string module_cat = string.Empty;
            string fsgroup_id = string.Empty;

            //if (((RadTreeViewItem)(TreeViewModuleCatList.SelectedItem)).Tag == null)
            //{
            //    module_cat = string.Empty;
            //}
            //else
            //{
            //    module_cat = ((WSGroupAdmin.Class_MODULE_CAT)(((RadTreeViewItem)(TreeViewModuleCatList.SelectedItem)).Tag)).FSMOUDULE_CAT_ID;
            //}


            if (((RadTreeViewItem)(TreeViewGroupList.SelectedItem)).Tag == null)
                fsgroup_id = string.Empty;
            else
                fsgroup_id = ((XElement)(((RadTreeViewItem)(TreeViewGroupList.SelectedItem)).Tag)).Element("FSGROUP_ID").Value.ToString();


            //ObservableCollection<WSGroupAdmin.Class_Module_Detail> mod_detail_list = new ObservableCollection<WSGroupAdmin.Class_Module_Detail>(); 

            //for (int i = 0; i < lbxModuleDetail.Items.Count; i++)
            //{

                

                
            //    CheckBox cbx = (CheckBox)lbxModuleDetail.Items.ElementAt(i) ;
            //    if (cbx.IsChecked == true)
            //    {
            //        if (cbx.IsChecked == true && ((WSGroupAdmin.Class_Module_Detail)(cbx.Tag)).FCHasPermission == "False") 
            //        {
            //            ((WSGroupAdmin.Class_Module_Detail)(cbx.Tag)).FCHasPermission = "True";
            //            mod_detail_list.Add((WSGroupAdmin.Class_Module_Detail)cbx.Tag);                 
            //        }                                        
            //    }
            //    else
            //    {
            //        if (cbx.IsChecked == false && ((WSGroupAdmin.Class_Module_Detail)(cbx.Tag)).FCHasPermission == "True") 
            //        {
            //            ((WSGroupAdmin.Class_Module_Detail)(cbx.Tag)).FCHasPermission = "False";
            //            mod_detail_list.Add((WSGroupAdmin.Class_Module_Detail)cbx.Tag);                 
            //        }
            //    }
                
                
            //}


            ObservableCollection<WSGroupAdmin.Class_Module_Detail> mod_detail_list = new ObservableCollection<WSGroupAdmin.Class_Module_Detail>();

            for (int i = 0; i < lbxModuleDetail.Items.Count; i++)
            {
                mod_detail_list.Add((WSGroupAdmin.Class_Module_Detail)lbxModuleDetail.Items[i]); 
            }

            WSGroupAdmin.WSGroupAdminSoapClient wsobj = new WSGroupAdmin.WSGroupAdminSoapClient();

            wsobj.fnUpdateTBMODULE_GROUPCompleted += (s, arg) =>
            {
                
            };

            wsobj.fnUpdateTBMODULE_GROUPAsync(mod_detail_list, fsgroup_id, UserClass.userData.FSUSER_ID); 


         

        }



     
     
    }
}
