﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.Web.ENT;

namespace PTS_MAM3.MAG
{
    public partial class MAG131 : ChildWindow
    {
        private ENT200DomainContext content = new ENT200DomainContext();
        private string tbName;
        public MAG131(string InputtbName)
        {
            InitializeComponent();
            tbName = InputtbName;
        }
        


        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            switch (tbName)
            {
                case "TBARCHIVE_SET_CLASS":
                    var TBARCHIVE_SET_CLASS = content.Load(content.GetTBARCHIVE_SET_CLASSQuery());
                    TBARCHIVE_SET_CLASS.Completed += (s, args) =>
                    {
                        TBARCHIVE_SET_CLASS TBARCHIVE_SET_CL = new TBARCHIVE_SET_CLASS();
                        TBARCHIVE_SET_CL.FSCLASSNAME = textBox1.Text.Trim();
                        TBARCHIVE_SET_CL.FSVIDEO = textBox2.Text.Trim();
                        TBARCHIVE_SET_CL.FSAUDIO = textBox3.Text.Trim();
                        TBARCHIVE_SET_CL.FSPHOTO = textBox4.Text.Trim();
                        TBARCHIVE_SET_CL.FSDOC = textBox5.Text.Trim();
                        content.TBARCHIVE_SET_CLASSes.Add(TBARCHIVE_SET_CL);
                        content.SubmitChanges().Completed += (s2, args2) =>
                        {
                            this.DialogResult = true;
                        };
                    };
                    break;
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

