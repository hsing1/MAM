﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace PTS_MAM3.SystemAdmin
{
    public partial class TemplateDefine : ChildWindow
    {
        public TemplateDefine()
        {
            InitializeComponent();
        }

        public TemplateDefine(string tableName, int FNTEMPLATE_ID)
        {
            InitializeComponent();
            prloadDGTBTemplate(tableName, FNTEMPLATE_ID); 
        }


        private void prloadDGTBTemplate(string tableName, int fntemplate_id)
        {
            WSDirectoryAdmin.WSDirectoryAdminSoapClient DirectoryObj = new WSDirectoryAdmin.WSDirectoryAdminSoapClient();

            DirectoryObj.GetTemplateListByTableCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        this.DGTBTemplate.DataContext = args.Result;
                        int itemIndex = 0 ;
                        foreach (WSDirectoryAdmin.TBTemplateStruct tp in args.Result)
                        {
                            if (tp.FNTEMPLATE_ID == fntemplate_id)
                            {
                                this.DGTBTemplate.SelectedIndex = itemIndex;
                                
                               //((DataGridRow)this.DGTBTemplate.SelectedItem).Focus();
                                break; 
                            }
                            itemIndex += 1; 



                        }                        
                    }
                }

            };

            DirectoryObj.GetTemplateListByTableAsync(tableName);

            
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void DGTBTemplate_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                WSDirectoryAdmin.TBTemplateStruct tp = (WSDirectoryAdmin.TBTemplateStruct)e.AddedItems[0];
                prloadDGTBTemplateField(tp.FNTEMPLATE_ID);
            }
            catch (Exception ex)
            {

            }

            
        }

        private void prloadDGTBTemplateField(int FNTEMPLATE_ID)
        {
            WSDirectoryAdmin.WSDirectoryAdminSoapClient DirectoryObj = new WSDirectoryAdmin.WSDirectoryAdminSoapClient();
            DirectoryObj.GetTemplateFieldsByIdCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        DGTBTemplateField.DataContext = args.Result;

                    }
                }
                
            };

            DirectoryObj.GetTemplateFieldsByIdAsync(FNTEMPLATE_ID);

        }

        //新增自訂代碼欄位
        private void btnAddCodeField_Click(object sender, RoutedEventArgs e) {
            ObservableCollection<WSDirectoryAdmin.TBTemplate_FieldsStruct> ds = (ObservableCollection<WSDirectoryAdmin.TBTemplate_FieldsStruct>)this.DGTBTemplateField.DataContext;

            WSDirectoryAdmin.TBTemplate_FieldsStruct item = ds.FirstOrDefault();
            MAG.MAG202_01_04 TmpField_Add = new MAG.MAG202_01_04("ADD", item.FSTABLE, item.FNTEMPLATE_ID, item.FSFIELD);
            TmpField_Add.Show();

            TmpField_Add.Closing += (s, args) =>
            {
                if (TmpField_Add.DialogResult == true)
                {
                    prloadDGTBTemplateField(item.FNTEMPLATE_ID);
                }
            }; 
        }


        //新增自訂欄位
        private void btnAddNewField_Click(object sender, RoutedEventArgs e)
        {
            ObservableCollection<WSDirectoryAdmin.TBTemplate_FieldsStruct> ds = (ObservableCollection<WSDirectoryAdmin.TBTemplate_FieldsStruct>)this.DGTBTemplateField.DataContext;

            WSDirectoryAdmin.TBTemplate_FieldsStruct item = ds.FirstOrDefault();
            //(string fstable, int fntemplate_id, string fsfield)
            TemplateField_Add TmpField_Add = new TemplateField_Add(ds,item.FSTABLE, item.FNTEMPLATE_ID, item.FSFIELD);
            TmpField_Add.Show();

            TmpField_Add.Closing += (s, args) =>
            {
                if (TmpField_Add.DialogResult == true)
                {
                    prloadDGTBTemplateField(item.FNTEMPLATE_ID);
                }
            }; 
        }


        //修改自訂欄位
        private void btnEditTemplateField_Click(object sender, RoutedEventArgs e)
        {
            ObservableCollection<WSDirectoryAdmin.TBTemplate_FieldsStruct> ds = (ObservableCollection<WSDirectoryAdmin.TBTemplate_FieldsStruct>)this.DGTBTemplateField.DataContext;
            WSDirectoryAdmin.TBTemplate_FieldsStruct item = ds.FirstOrDefault();

            if (this.DGTBTemplateField.SelectedIndex < 0)
            {
                MessageBox.Show("請選取修改欄位", "錯誤", MessageBoxButton.OK); 
            }
            else
            {
                WSDirectoryAdmin.TBTemplate_FieldsStruct tf = (WSDirectoryAdmin.TBTemplate_FieldsStruct)this.DGTBTemplateField.SelectedItem;


                if (tf.FSFIELD_TYPE != "自訂代碼")
                {
                    TemplateField_Edit TmpField_Edit = new TemplateField_Edit(tf);

                    TmpField_Edit.Show();
                    TmpField_Edit.Closing += (s, args) =>
                    {
                        if (TmpField_Edit.DialogResult == true)
                            prloadDGTBTemplateField(tf.FNTEMPLATE_ID);  
                    };
                }else{
                    MAG.MAG202_01_04 TmpField_Edit = new MAG.MAG202_01_04("MOD",tf, item.FSTABLE, item.FNTEMPLATE_ID, item.FSFIELD);

                    TmpField_Edit.Show();
                    TmpField_Edit.Closing += (s, args) =>
                    {
                        if (TmpField_Edit.DialogResult == true)
                            prloadDGTBTemplateField(tf.FNTEMPLATE_ID);
                    };
                } 
            }
        }


        //刪除自訂欄位
        private void btnDeleteTemplateField_Click(object sender, RoutedEventArgs e)
        {
            if (this.DGTBTemplateField.SelectedIndex < 0)
            {
                MessageBox.Show("請選取刪除欄位", "錯誤", MessageBoxButton.OK);
            }
            else
            {
                if (MessageBox.Show("是否確定刪除?", "提示", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                {

                    WSDirectoryAdmin.TBTemplate_FieldsStruct tf = (WSDirectoryAdmin.TBTemplate_FieldsStruct)this.DGTBTemplateField.SelectedItem;
                    WSDirectoryAdmin.WSDirectoryAdminSoapClient dirObj = new WSDirectoryAdmin.WSDirectoryAdminSoapClient();

                    dirObj.fnDeleteTBTEMPLATE_FIELDSCompleted += (s, args) =>
                    {
                        if (args.Error == null)
                        {
                            if (args.Result == false)
                                MessageBox.Show("刪除資料時發生錯誤!", "錯誤", MessageBoxButton.OK);
                            else
                            {
                                prloadDGTBTemplateField(tf.FNTEMPLATE_ID);
                                MessageBox.Show("刪除資料完成", "提示", MessageBoxButton.OK);
                            }
                        }

                    };
                    dirObj.fnDeleteTBTEMPLATE_FIELDSAsync(tf.FNID,  UserClass.userData.FSUSER_ID); 
                }
            }
        }


        //修改Template 
        private void btnEditTemplateName_Click(object sender, RoutedEventArgs e)
        {
            if (this.DGTBTemplate.SelectedIndex < 0)
                MessageBox.Show("未選取欲修改Template", "提示", MessageBoxButton.OK);
            else
            {
                WSDirectoryAdmin.TBTemplateStruct t = (WSDirectoryAdmin.TBTemplateStruct ) this.DGTBTemplate.SelectedItem;

                Template_Edit tmpEdit = new Template_Edit(t.FNTEMPLATE_ID, t.FSTEMPLATE, t.FSDESCRIPTION);
                tmpEdit.Show();

                tmpEdit.Closing += (s, args) =>
                {
                    prloadDGTBTemplate(t.FSTABLE, t.FNTEMPLATE_ID); 

                };
            }
        }

        //刪除Template 
        private void btnDeleteTemplate_Click(object sender, RoutedEventArgs e)
        {
            if (this.DGTBTemplate.SelectedIndex < 0)
                MessageBox.Show("未選取欲刪除的Template", "提示", MessageBoxButton.OK);
            else
            {
                if (((WSDirectoryAdmin.TBTemplateStruct)this.DGTBTemplate.SelectedItem).FCREAD_ONLY == "Y")
                {
                    MessageBox.Show("預設欄位請勿刪除", "提示", MessageBoxButton.OK); 

                }
                else
                {
                    if (MessageBox.Show("請確定是否要刪除Template ?", "提示", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                    {

                        WSDirectoryAdmin.TBTemplateStruct t = (WSDirectoryAdmin.TBTemplateStruct)this.DGTBTemplate.SelectedItem;
                        WSDirectoryAdmin.WSDirectoryAdminSoapClient dirObj = new WSDirectoryAdmin.WSDirectoryAdminSoapClient();
                        dirObj.fnDeleteTBTEMPLATECompleted += (s, args) =>
                        {
                            if (args.Error == null)
                            {
                                if (args.Result == false)
                                {
                                    MessageBox.Show("刪除資料時發生錯誤!", "錯誤", MessageBoxButton.OK);
                                }
                                else
                                {
                                    prloadDGTBTemplate(t.FSTABLE, t.FNTEMPLATE_ID);
                                }
                            }
                        };
                        dirObj.fnDeleteTBTEMPLATEAsync(t, UserClass.userData.FSUSER_ID);

                    }
                }          
            }
        }

        //複製Template
        private void btnCopyTemplate_Click(object sender, RoutedEventArgs e)
        {
            if (this.DGTBTemplate.SelectedIndex < 0)
                MessageBox.Show("未選取欲複製的Template", "提示", MessageBoxButton.OK);
            else
            {
                WSDirectoryAdmin.TBTemplateStruct t = (WSDirectoryAdmin.TBTemplateStruct)this.DGTBTemplate.SelectedItem;
                t.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                t.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                WSDirectoryAdmin.WSDirectoryAdminSoapClient dirObj = new WSDirectoryAdmin.WSDirectoryAdminSoapClient();
                dirObj.fnCopyTBTEMPLATECompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (args.Result == false)
                        {
                            MessageBox.Show("複製資料時發生錯誤!", "錯誤", MessageBoxButton.OK);
                        }
                        else
                        {
                            prloadDGTBTemplate(t.FSTABLE, t.FNTEMPLATE_ID); 
                        }
                    }
                };

                dirObj.fnCopyTBTEMPLATEAsync(t, UserClass.userData.FSUSER_ID); 
            }
        }
    }
}

