﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.MAG
{
    public partial class MAG200_05 : ChildWindow
    {
        public WSUserAdmin.WSUserAdminSoapClient myService = new WSUserAdmin.WSUserAdminSoapClient();
        List<WSUserAdmin.Class_Dept> DeptList = new List<WSUserAdmin.Class_Dept>();
        //WSUserAdmin.DeptStruct myDeptStruct = new WSUserAdmin.DeptStruct();
        WSUserAdmin.Class_Dept myDeptStruct = new WSUserAdmin.Class_Dept();
        public MAG200_05(int iniTreeViewID)
        {
            InitializeComponent();
            myService.GetDeptListAllCompleted += (s, args) =>
            {
                DeptList = args.Result.ToList();
                foreach (var item in DeptList)
                {
                    ComboBoxItem cbi = new ComboBoxItem();
                    cbi.Content = item.FSFullName_ChtName;
                    cbi.Tag = item;
                    comboBox1.Items.Add(cbi);
                }
                foreach (var items in comboBox1.Items)
                {
                    if (((WSUserAdmin.Class_Dept)(((ComboBoxItem)items).Tag)).FNDEP_ID == iniTreeViewID)
                    {
                        comboBox1.SelectedItem = items;
                    }
                }
            };
            myService.GetDeptListAllAsync();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (textBox1.Text.Trim()==string.Empty)//|| ((WSUserAdmin.Class_Dept)(((ComboBoxItem)comboBox1.SelectedItem).Tag)).FNDEP_ID==null)
            {
                MessageBox.Show("請填必填欄位 或部門資料不正確");
                return;
            }
            myDeptStruct.FSDEP = textBox1.Text;
            myDeptStruct.FSDpeName_ChtName = textBox1.Text;
            myDeptStruct.FSFullName_ChtName = ((WSUserAdmin.Class_Dept)(((ComboBoxItem)comboBox1.SelectedItem).Tag)).FSFullName_ChtName + "／" + textBox1.Text;
            myDeptStruct.FSSupervisor_ID = "";
            myDeptStruct.FNPARENT_DEP_ID = ((WSUserAdmin.Class_Dept)(((ComboBoxItem)comboBox1.SelectedItem).Tag)).FNDEP_ID;
            myDeptStruct.FSUpDepName_ChtName = ((WSUserAdmin.Class_Dept)(((ComboBoxItem)comboBox1.SelectedItem).Tag)).FSDEP;
            myDeptStruct.FNDEP_LEVEL = ((WSUserAdmin.Class_Dept)(((ComboBoxItem)comboBox1.SelectedItem).Tag)).FNDEP_LEVEL + 1;
            myDeptStruct.FBIsDepExist = "1";
            myDeptStruct.FSDEP_MEMO = textBox2.Text;
            myService.InsUser_DEP_MAMAsync(myDeptStruct, UserClass.userData.FSUSER_ID);
            myService.InsUser_DEP_MAMCompleted += (s, args) =>
                {
                    if (args.Result)
                    {
                        this.DialogResult = true;
                    }
                    else
                        MessageBox.Show("新增失敗");
                };
            
            
            
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

