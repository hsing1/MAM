﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using PTS_MAM3.Web.ENT;
using System.Windows.Data;

namespace PTS_MAM3.MAG
{
    public partial class MAG101 : Page
    {
        private ENT100DomainContext content = new ENT100DomainContext();
        //private ENTUsersDomainContext contentuser = new ENTUsersDomainContext();
        private string tbName;
        public MAG101(string InputtbName)
        {
            InitializeComponent();
            tbName = InputtbName;
            RefreshDataGrid();
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void RefreshDataGrid()
        {
            switch (tbName)
            {
                case "TBZPROMOCATD":
                    var TBZPROMOCATD = content.Load(content.GetJoinTBZPROMOCATDQueryQuery());
                    TBZPROMOCATD.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        Binding d = new Binding();
                        d.Mode = BindingMode.OneWay;
                        Binding e = new Binding();
                        b.Path = new PropertyPath("FSPROMOCATID");
                        c.Path = new PropertyPath("FSPROMOCATNAME");
                        d.Path = new PropertyPath("FSPROMOCATDID");
                        e.Path = new PropertyPath("FSPROMOCATDNAME");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        ((DataGridTextColumn)dataGrid1.Columns[2]).Binding = d;
                        ((DataGridTextColumn)dataGrid1.Columns[3]).Binding = e;
                        dataGrid1.ItemsSource = TBZPROMOCATD.Entities.Where(s1=>s1.FSPROMOCATID.Trim()!="");
                    };
                    break;
                case "TBZPROGBUYD":
                    var TBZPROGBUYDD = content.Load(content.GetJoinTableBUYIDQueryQuery());
                    TBZPROGBUYDD.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        Binding d = new Binding();
                        d.Mode = BindingMode.OneWay;
                        Binding e = new Binding();
                        b.Path = new PropertyPath("FSPROGBUYID");
                        c.Path = new PropertyPath("FSPROGBUYNAME");
                        d.Path = new PropertyPath("FSPROGBUYDID");
                        e.Path = new PropertyPath("FSPROGBUYDNAME");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        ((DataGridTextColumn)dataGrid1.Columns[2]).Binding = d;
                        ((DataGridTextColumn)dataGrid1.Columns[3]).Binding = e;
                        //var NewTBZPROGBUYD=TBZPROGBUYDD;
                        //foreach (TBJoinClass y in TBZPROGBUYDD.Entities)
                        //{
                        #region 需要修改的部分2011/03/10
                        //var userabout = content.Load(contentuser.GetTBUSERSQuery());
                        //    userabout.Completed += (s1, args1) =>
                        //        {
                        //            foreach (TBJoinClass z in TBZPROGBUYDD.Entities)
                        //            {
                        //                var NewTBZPROGBUYD = from g in userabout.Entities
                        //                                     where g.FSUSER_ID == z.FSCREATED_BY
                        //                                     select g;
                        //                foreach(TBJoinClass a in NewTBZPROGBUYD)
                        //                {
                                            
                        //                }
                        //            }
                        //        };
                        #endregion
                        //}
                        dataGrid1.ItemsSource = TBZPROGBUYDD.Entities.Where(s2=>s2.FSPROGBUYID.Trim()!="");
                    };
                    break;
            }
        }

        private void btnUserDel_Click(object sender, RoutedEventArgs e)
        {
            WSAgentFieldDelete.WSAgentFieldDeleteSoapClient AgentField = new WSAgentFieldDelete.WSAgentFieldDeleteSoapClient();
            if (dataGrid1.SelectedItem == null)
            { MessageBox.Show("請選擇資料行"); }
            else
            {
                switch (tbName)
                {
                    case "TBZPROMOCATD":
                        TBZPROMOCATDJoinClass TBZPROMOCATDJoinClass = (TBZPROMOCATDJoinClass)dataGrid1.SelectedItem;
                        AgentField.DelAgentFieldDetailAsync("TBZPROMOCATD", TBZPROMOCATDJoinClass.FSPROMOCATID, TBZPROMOCATDJoinClass.FSPROMOCATDID, UserClass.userData.FSUSER_ID);
                        break;
                    case "TBZPROGBUYD":
                        TBJoinClass TBJoinClass = (TBJoinClass)dataGrid1.SelectedItem;
                        AgentField.DelAgentFieldDetailAsync("TBZPROGBUYD", TBJoinClass.FSPROGBUYID, TBJoinClass.FSPROGBUYDID, UserClass.userData.FSUSER_ID);
                        break;
                }
                AgentField.DelAgentFieldDetailCompleted += (s, args) =>
                {
                    if (args.Result == "true")
                    {
                        switch (tbName)
                        {
                            case "TBZPROMOCATD":
                                content.TBJoinClasses.Clear();
                                var TBZPROMOCATD = content.Load(content.GetJoinTBZPROMOCATDQueryQuery());
                                TBZPROMOCATD.Completed += (s1, args1) =>
                                {
                                    MessageBox.Show("選取的資料行已經刪除");
                                    dataGrid1.ItemsSource = TBZPROMOCATD.Entities.Where(sa => sa.FSPROMOCATID.Trim() != "");
                                };
                                break;
                            case "TBZPROGBUYD":
                                content.TBZPROGBUYDs.Clear();
                                var TBZPROGBUYD = content.Load(content.GetJoinTableBUYIDQueryQuery());
                                TBZPROGBUYD.Completed += (s1, args1) =>
                                {
                                    MessageBox.Show("選取的資料行已經刪除");
                                    dataGrid1.ItemsSource = TBZPROGBUYD.Entities.Where(sb => sb.FSPROGBUYID.Trim() != "");
                                };
                                break;
                        }
                    }
                    else
                    { MessageBox.Show(args.Result, "資料庫訊息", MessageBoxButton.OK); }
                };
            }
        }
        private void btnClickUserAdd(object sender, RoutedEventArgs e)
        {
            MAG101_01 MAG101_01 = new MAG101_01(tbName);
            MAG101_01.Show();
            MAG101_01.Closing += (s, args) =>
            {
                if (MAG101_01.DialogResult == true)
                {
                    RefreshDataGrid();
                }
                else if (MAG101_01.DialogResult == false)
                {
                    MessageBox.Show("取消新增資料");
                }
            };
        }

        private void btnUserModifyClick(object sender, RoutedEventArgs e)
        {
            if (dataGrid1.SelectedItem == null)
            { MessageBox.Show("請選擇資料行"); }
            else
            {
                MAG101_02 MAG101_02 = new MAG101_02(tbName, dataGrid1.SelectedItem);
                MAG101_02.Show();
                MAG101_02.Closing += (s, args) =>
                {
                    if (MAG101_02.DialogResult == true)
                    {
                        switch (tbName)
                        {
                            case "TBZPROMOCATD":
                                dataGrid1.SelectedItem = (TBZPROMOCATDJoinClass)MAG101_02.senderObj;
                                break;
                            case "TBZPROGBUYD":
                                dataGrid1.SelectedItem = (TBJoinClass)MAG101_02.senderObj;
                                break;
                        }
                        content.SubmitChanges().Completed += (s1, args1) =>
                        {
                            RefreshDataGrid();
                        };
                    }
                    else if (MAG101_02.DialogResult == false)
                    {
                        MessageBox.Show("取消資料修改");
                    }
                };
            }
        }

        private void RefreshDataGridWithCondiction(string strCondiction)
        {
            switch (tbName)
            {
                case "TBZPROMOCATD":
                    var TBZPROMOCATD = content.Load(content.GetJoinTBZPROMOCATDQueryQuery());
                    TBZPROMOCATD.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        Binding d = new Binding();
                        d.Mode = BindingMode.OneWay;
                        Binding e = new Binding();
                        b.Path = new PropertyPath("FSPROMOCATID");
                        c.Path = new PropertyPath("FSPROMOCATNAME");
                        d.Path = new PropertyPath("FSPROMOCATDID");
                        e.Path = new PropertyPath("FSPROMOCATDNAME");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        ((DataGridTextColumn)dataGrid1.Columns[2]).Binding = d;
                        ((DataGridTextColumn)dataGrid1.Columns[3]).Binding = e;
                        dataGrid1.ItemsSource = TBZPROMOCATD.Entities.Where(R => R.FSPROMOCATDNAME.Contains(strCondiction));
                    };
                    break;
                case "TBZPROGBUYD":
                    var TBZPROGBUYDD = content.Load(content.GetJoinTableBUYIDQueryQuery());
                    TBZPROGBUYDD.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        Binding d = new Binding();
                        d.Mode = BindingMode.OneWay;
                        Binding e = new Binding();
                        b.Path = new PropertyPath("FSPROGBUYID");
                        c.Path = new PropertyPath("FSPROGBUYNAME");
                        d.Path = new PropertyPath("FSPROGBUYDID");
                        e.Path = new PropertyPath("FSPROGBUYDNAME");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        ((DataGridTextColumn)dataGrid1.Columns[2]).Binding = d;
                        ((DataGridTextColumn)dataGrid1.Columns[3]).Binding = e;
                        dataGrid1.ItemsSource = TBZPROGBUYDD.Entities.Where(R => R.FSPROGBUYDNAME.Contains(strCondiction));
                    };
                    break;
            }
        }

        private void btnUserDel_Click_1(object sender, RoutedEventArgs e)
        {
            MAG.MAG128 MAG128_Page = new MAG128();
            MAG128_Page.Show();
            MAG128_Page.Closing += (s, args) =>
            {
                RefreshDataGridWithCondiction(MAG128_Page.strReturnCondection.ToString().Trim());
            };
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            switch (tbName)
            {
                case "TBZPROMOCATD":
                    content.TBJoinClasses.Clear();
                    var TBZPROMOCATD = content.Load(content.GetJoinTBZPROMOCATDQueryQuery());
                    TBZPROMOCATD.Completed += (s1, args1) =>
                    {
                        dataGrid1.ItemsSource = TBZPROMOCATD.Entities.Where(sa => sa.FSPROMOCATID.Trim() != "");
                    };
                    break;
                case "TBZPROGBUYD":
                    content.TBZPROGBUYDs.Clear();
                    var TBZPROGBUYD = content.Load(content.GetJoinTableBUYIDQueryQuery());
                    TBZPROGBUYD.Completed += (s1, args1) =>
                    {
                        dataGrid1.ItemsSource = TBZPROGBUYD.Entities.Where(sb => sb.FSPROGBUYID.Trim() != "");
                    };
                    break;
            }
        }

    }
}
