﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.MAG
{
    public partial class MAG330_01 : ChildWindow
    {
        private System.Windows.Threading.DispatcherTimer sysTimer = new System.Windows.Threading.DispatcherTimer();
        public SR_ServiceTranscode.clLOG_WORKER clLOG_WORKER = new SR_ServiceTranscode.clLOG_WORKER();
        public bool _ResetOK = false;
        SR_ServiceTranscode.ServiceTranscodeSoapClient clntST = new SR_ServiceTranscode.ServiceTranscodeSoapClient();

        public MAG330_01(SR_ServiceTranscode.clLOG_WORKER m_clLOG_WORKER)
        {
            clLOG_WORKER = m_clLOG_WORKER;

            InitializeComponent();
            InitializeForm();
        }

        void InitializeForm() //初始化本頁面
        {
            this.LayoutRoot.DataContext = clLOG_WORKER;
            clntST.GetTBLOG_WORKER_ByFSGUIDCompleted += new EventHandler<SR_ServiceTranscode.GetTBLOG_WORKER_ByFSGUIDCompletedEventArgs>(clntST_GetTBLOG_WORKER_ByFSGUIDCompleted);
            clntST.TBLOG_VIDEO_RESETCompleted += new EventHandler<SR_ServiceTranscode.TBLOG_VIDEO_RESETCompletedEventArgs>(clntST_TBLOG_VIDEO_RESETCompleted);

            sysTimer.Interval = new TimeSpan(0, 0, 5);      // 時分秒
            sysTimer.Tick += new EventHandler(sysTimer_Tick);
            sysTimer.Start();
        }

        void clntST_GetTBLOG_WORKER_ByFSGUIDCompleted(object sender, SR_ServiceTranscode.GetTBLOG_WORKER_ByFSGUIDCompletedEventArgs e)
        {
            sysTimer.Stop();

            if (e.Error == null)
            {
                if (e.Result == null)
                    MessageBox.Show("暫時無法取得詳細資料更新資訊","提示訊息",MessageBoxButton.OK);
                else
                {
                    // 取消、存值、更新畫面
                    clLOG_WORKER = null;
                    clLOG_WORKER = e.Result;
                    this.LayoutRoot.DataContext = clLOG_WORKER;
                }
            }
            else
                MessageBox.Show("連結主機服務時發生問題：" + e.Error.ToString());
             
            lblRefresh.Content = "更新時間: " + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
            sysTimer.Start();            
        }

        void sysTimer_Tick(object sender, EventArgs e)
        {
            clntST.GetTBLOG_WORKER_ByFSGUIDAsync(clLOG_WORKER.FSGUID);
        }

        void clntST_TBLOG_VIDEO_RESETCompleted(object sender, SR_ServiceTranscode.TBLOG_VIDEO_RESETCompletedEventArgs e)
        {
            //如果取回的資料沒有錯誤訊息
            if (e.Error == null)
            {
                //如果取回的資料筆數不為0
                if (e.Result < 0)
                {
                    MessageBox.Show("Reset失敗", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
                else
                {
                    _ResetOK = true;
                    MessageBox.Show("重設完畢", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
            else
            {
                //如果取回的資料有錯誤訊息
                MessageBox.Show("發生錯誤: " + e.Error.ToString(), "提示訊息", MessageBoxButton.OK);
            }
        }

#region"按鈕動作"

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            clntST.TBLOG_VIDEO_RESETAsync(clLOG_WORKER.File_ID, clLOG_WORKER.FSGUID);

            //_ResetOK = true;
            //MessageBox.Show("重設完畢", "提示訊息", MessageBoxButton.OK);
            //this.DialogResult = false;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

#endregion"按鈕動作"


    }
}

