﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.ProgData;
using PTS_MAM3.PRG;
using PTS_MAM3.SR_WSPGM_MSSProcess_Management;
using PTS_MAM3.WSPROG_PRODUCER;
using PTS_MAM3.STT;

namespace PTS_MAM3
{   //TC微調紀錄查詢頁面
    public partial class MAG340_01 : ChildWindow
    {
        string FSTYPE = "";
        string FSPROGID = "";
        string FNEPISOD = "";
        string SDATE = "";
        string EDATE = "";
        string FSCREATED_BY = "";
        WSPROG_PRODUCERSoapClient clientPRODUCER = new WSPROG_PRODUCERSoapClient();
        List<UserStruct> ListUserAll = new List<UserStruct>();
        SR_WSPGM_MSSProcess_Management.WSPGM_MSSProcess_ManagementSoapClient
            WSPGM_MSSProcessClient = new WSPGM_MSSProcess_ManagementSoapClient();

        public List<Class_MssProcessDetail> queryResult;

        public MAG340_01()
        {
            InitializeComponent();

            //預設搜尋日期為一個月內
            DPSTART.Text = DateTime.Now.AddMonths(-1).ToShortDateString();
            DPEND.Text = DateTime.Now.ToShortDateString();
            
            //預設選擇"節目"
            this.rdbProg.IsChecked = true;

            //查詢所有使用者資料
            clientPRODUCER.QUERY_TBUSERS_ALLCompleted += new EventHandler<QUERY_TBUSERS_ALLCompletedEventArgs>(clientPRODUCER_QUERY_TBUSERS_ALLCompleted);
            clientPRODUCER.QUERY_TBUSERS_ALLAsync();

            WSPGM_MSSProcessClient.Get_LOG_VIDEO_SEG_CHG_LIST_ADVANCECompleted += new EventHandler<Get_LOG_VIDEO_SEG_CHG_LIST_ADVANCECompletedEventArgs>(WSPGM_MSSProcessClient_Get_LOG_VIDEO_SEG_CHG_LIST_ADVANCECompleted);

        }

        //實作-查詢段落置換記錄
        void WSPGM_MSSProcessClient_Get_LOG_VIDEO_SEG_CHG_LIST_ADVANCECompleted(object sender, Get_LOG_VIDEO_SEG_CHG_LIST_ADVANCECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                queryResult = e.Result;
            }
            this.DialogResult = true;
        }


        //實作-查詢所有使用者資料 
        void clientPRODUCER_QUERY_TBUSERS_ALLCompleted(object sender, QUERY_TBUSERS_ALLCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                ListUserAll = e.Result;
                acbMenID.ItemsSource = ListUserAll; //繫結AutoComplete按鈕
            }
            else
                MessageBox.Show("查詢使用者資料異常！", "提示訊息", MessageBoxButton.OK);

        }

        //選取影片類型時
        void radioButton_Checked(object sender, RoutedEventArgs e)
        {
            FSTYPE = ((RadioButton)sender).Tag.ToString();
            ChangeUIMethod(FSTYPE);
        }

        //按下確定鍵時
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (Convert.ToDateTime(SDATE) > Convert.ToDateTime(EDATE))
            {
                MessageBox.Show("起始日期必須在結束日期之前!");
                return;
            }

            WSPGM_MSSProcessClient.Get_LOG_VIDEO_SEG_CHG_LIST_ADVANCEAsync
                (FSTYPE, FSPROGID, FNEPISOD, SDATE, EDATE, FSCREATED_BY);

        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void btnPROG_Click(object sender, RoutedEventArgs e)
        {
            if (rdbProg.IsChecked == true)
            {
                //PRG100_01 節目查詢頁面
                PROGDATA_VIEW PROGDATA_VIEW_frm = new PROGDATA_VIEW();
                PROGDATA_VIEW_frm.Show();

                PROGDATA_VIEW_frm.Closed += (s, args) =>
                {
                    if (PROGDATA_VIEW_frm.DialogResult == true)
                    {
                        tbxPROG_NAME.Text = PROGDATA_VIEW_frm.strProgName_View;
                        tbxPROG_NAME.Tag = PROGDATA_VIEW_frm.strProgID_View;
                        FSPROGID = PROGDATA_VIEW_frm.strProgID_View;

                        tbxEPISODE.Text = "";
                        lblEPISODE_NAME.Content = "";
                        //m_strEpisode = "0";
                        //m_strCHANNELID = PROGDATA_VIEW_frm.strChannel_Id;
                        //m_strID = PROGDATA_VIEW_frm.strProgID_View;
                    }
                };
            }
            else if (rdbPromo.IsChecked == true)
            {
                //PRG800_07 短帶查詢頁面
                PRG800_07 PROMO_VIEW_frm = new PRG800_07();
                PROMO_VIEW_frm.Show();

                PROMO_VIEW_frm.Closed += (s, args) =>
                {
                    if (PROMO_VIEW_frm.DialogResult == true)
                    {
                        tbxPROG_NAME.Text = PROMO_VIEW_frm.strPromoName_View;
                        tbxPROG_NAME.Tag = PROMO_VIEW_frm.strPromoID_View;
                        //把短帶ID存到FSPROGID是為了使用同一個類別傳遞參數
                        FSPROGID = PROMO_VIEW_frm.strPromoID_View;
                        tbxEPISODE.Text = "";
                        lblEPISODE_NAME.Content = "";
                        //m_strEpisode = "0";
                        //m_strCHANNELID = PROMO_VIEW_frm.strChannel_Id;
                        //m_strID = PROMO_VIEW_frm.strPromoID_View;
                    }
                };
            }
            //else if (rdBData.IsChecked == true)
            //{
            //    //資料帶查詢頁面
            //    STT100_08_01 STT100_08_01_frm = new STT100_08_01();
            //    STT100_08_01_frm.Show();

            //    STT100_08_01_frm.Closed += (s, args) =>
            //    {
            //        if (STT100_08_01_frm.DialogResult == true)
            //        {
            //            if (STT100_08_01_frm.strDATA_ID_View.Trim() == "" || STT100_08_01_frm.strDATA_Name_View == "")
            //            {
            //                MessageBox.Show("資料帶選擇異常，請重新選擇資料帶", "提示訊息", MessageBoxButton.OK);
            //                return;
            //            }

            //            //把資料帶ID存到FSPROGID是為了使用同一個類別傳遞參數
            //            FSPROGID = STT100_08_01_frm.strDATA_ID_View.Trim();
            //            tbxPROG_NAME.Tag = FSPROGID;
            //            tbxPROG_NAME.Text = STT100_08_01_frm.strDATA_Name_View.Trim();
            //            //m_strCHANNELID = STT100_08_01_frm.strDATA_Channel_View.Trim();
            //            // m_strType = "D";      //類型為D，資料帶
            //            // m_strEpisode = "0";   //集別為0，第零集    

            //            //透過節目編號去查詢入庫影像檔      
            //            //ListLogVideo.Clear();
            //            // this.DGLogList.DataContext = null;
            //        }
            //    };

            //}
        }


        private void btnEPISODE_Click(object sender, RoutedEventArgs e)
        {
            if (tbxPROG_NAME.Text.ToString().Trim() != "" && tbxPROG_NAME.Tag.ToString().Trim() != "")
            {//開啟節目子集查詢頁面
                PRG.PRG200_07 PRG200_07_frm = new PRG.PRG200_07(tbxPROG_NAME.Tag.ToString().Trim(), tbxPROG_NAME.Text.ToString().Trim());
                PRG200_07_frm.Show();

                PRG200_07_frm.Closed += (s, args) =>
                {
                    if (PRG200_07_frm.DialogResult == true)
                    {
                        //m_strEpisode = PRG200_07_frm.m_strEpisode_View;
                        tbxEPISODE.Text = PRG200_07_frm.m_strEpisode_View;
                        FNEPISOD = tbxEPISODE.Text;
                        lblEPISODE_NAME.Content = PRG200_07_frm.m_strProgDName_View;
                    }
                };
            }
            else
                MessageBox.Show("請先選擇節目！", "提示訊息", MessageBoxButton.OK);

        }

        private void DPSTART_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {//變更起始日期
            SDATE = this.DPSTART.SelectedDate.Value.ToShortDateString();
        }

        private void DPEND_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {//變更結束日期
            EDATE = this.DPEND.SelectedDate.Value.AddDays(+1).ToShortDateString();
        }

        //自動去比對資料庫，若有人員就顯示資料
        private void acbMenID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (acbMenID.SelectedItem == null)
                tbxNote.Text = "";
            else
            {
                tbxNote.Text = ((UserStruct)(acbMenID.SelectedItem)).FSTITLE_NAME.Trim();
                FSCREATED_BY = ((UserStruct)(acbMenID.SelectedItem)).FSUSER_ID.Trim();
            }
        }

        //更改UI介面的顯示
        void ChangeUIMethod(string fileType)
        {
            if (fileType == "G")
            {
                this.TBName.Text = "節目名稱";
                this.sPEPISOD.Visibility = System.Windows.Visibility.Visible;
            }
            else if (fileType == "P")
            {
                this.TBName.Text = "短帶名稱";
                this.sPEPISOD.Visibility = System.Windows.Visibility.Collapsed;
            }
            else if (fileType == "D")
            {
                this.TBName.Text = "資料帶名稱";
                this.sPEPISOD.Visibility = System.Windows.Visibility.Collapsed;
            }
        }
    }
}

