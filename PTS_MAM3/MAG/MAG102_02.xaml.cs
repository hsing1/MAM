﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;



namespace PTS_MAM3.MAG
{
    public partial class MAG102_02 : ChildWindow
    {
        WSARCHIVE.Class_ARCHIVE_SET myclass;
        
        public MAG102_02(WSARCHIVE.Class_ARCHIVE_SET SET)
        {
            InitializeComponent();
            myclass = SET;
            textBox1.Text = SET.FNVIDEO.Trim();
            textBox2.Text = SET.FNAUDIO.Trim();
            textBox3.Text = SET.FNPHOTO.Trim();
            textBox4.Text = SET.FNDOC;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            bool V, A, P, D;
            short SV,SA,SP,SD;
            WSARCHIVE.WSARCHIVESoapClient client = new WSARCHIVE.WSARCHIVESoapClient();
            V = Int16.TryParse(textBox1.Text, out SV);
            A = Int16.TryParse(textBox2.Text, out SA);
            P = Int16.TryParse(textBox3.Text, out SP);
            D = Int16.TryParse(textBox4.Text, out SD);
            if (V && A && P && D)
            {
                myclass.FNVIDEO = SV.ToString();
                myclass.FNAUDIO = SA.ToString();
                myclass.FNPHOTO = SP.ToString();
                myclass.FNDOC = SD.ToString();
                myclass.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                if (V == A == P == D == true)
                    client.UpdTBARCHIVE_SET_UpdAsync(myclass);
                else
                    MessageBox.Show("請輸入數字");
                client.UpdTBARCHIVE_SET_UpdCompleted += (s, args) =>
                    {
                        if (args.Result == true)
                        {
                            this.DialogResult = true;
                        }
                        else
                        { MessageBox.Show("請檢查網路連線!!"); }
                    };
                //client.UpdTBARCHIVE_SET_UpdAsync(myclass);
                //client.UpdTBARCHIVE_SETAsync(myclass.FSTYPE, myclass.FSID, myclass.FNEPISODE, myclass.FNVIDEO, myclass.FNVIDEO, myclass.FNAUDIO, myclass.FNDOC, myclass.FSUPDATED_BY);
            }
            else
            { MessageBox.Show("請檢查欄位，必須輸入數字!"); }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

