﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.Web.ENT;

namespace PTS_MAM3.MAG
{
    public partial class MAG132 : ChildWindow
    {
        public Object senderObj;
        string tbName;
        public MAG132(string InputtbName,Object sender)
        {
            InitializeComponent();
            senderObj = sender;
            tbName = InputtbName;
            switch (tbName)
            {
                case "TBARCHIVE_SET_CLASS":
                    textBox1.Text = ((TBARCHIVE_SET_CLASS)senderObj).FSCLASSNAME;
                    textBox2.Text = ((TBARCHIVE_SET_CLASS)senderObj).FSVIDEO;
                    textBox3.Text = ((TBARCHIVE_SET_CLASS)senderObj).FSAUDIO;
                    textBox4.Text = ((TBARCHIVE_SET_CLASS)senderObj).FSPHOTO;
                    textBox5.Text = ((TBARCHIVE_SET_CLASS)senderObj).FSDOC;
                    break;
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            switch (tbName)
            {
                case "TBARCHIVE_SET_CLASS":
                    ((TBARCHIVE_SET_CLASS)senderObj).FSCLASSNAME = textBox1.Text.Trim();
                    ((TBARCHIVE_SET_CLASS)senderObj).FSVIDEO = textBox2.Text.Trim();
                    ((TBARCHIVE_SET_CLASS)senderObj).FSAUDIO = textBox3.Text.Trim();
                    ((TBARCHIVE_SET_CLASS)senderObj).FSPHOTO = textBox4.Text.Trim();
                    ((TBARCHIVE_SET_CLASS)senderObj).FSDOC = textBox5.Text.Trim();
                    break;
            }
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

