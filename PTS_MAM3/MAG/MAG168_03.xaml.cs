﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.MAG
{
    public partial class MAG168_03 : ChildWindow
    {
        String m_ID, m_Action;
        public PTS_MAM3.WSLogTemplateField.LogTemplateField_CODE clCODE = new PTS_MAM3.WSLogTemplateField.LogTemplateField_CODE();                  //宣告此類別的一個公用成員 
        WSLogTemplateField.WSLogTemplateFieldSoapClient clntLTF = new WSLogTemplateField.WSLogTemplateFieldSoapClient();
        
        public MAG168_03(String m_ID, String m_Action)
        {
            this.m_ID = m_ID;
            this.m_Action = m_Action;
            InitializeComponent();
            InitializeForm();
        }

        public MAG168_03(String m_ID, String m_Action, PTS_MAM3.WSLogTemplateField.LogTemplateField_CODE clCODE)
        {
            this.m_ID = m_ID;
            this.clCODE = clCODE;
            this.m_Action = m_Action;
            InitializeComponent();
            InitializeForm();
        }

        void InitializeForm() //初始化本頁面
        {
            clntLTF.InsertCodeCompleted += new EventHandler<WSLogTemplateField.InsertCodeCompletedEventArgs>(clntLTF_InsertCodeCompleted);
            clntLTF.UpdateCodeCompleted += new EventHandler<WSLogTemplateField.UpdateCodeCompletedEventArgs>(clntLTF_UpdateCodeCompleted);

            switch (m_Action)
            {
                case "ADD":
                    this.Title = "新增代碼設定";
                    chkISENABLED.IsChecked = true; 

                    break;
                case "MOD":
                    this.Title = "修改代碼設定";
                    txtCODE.IsEnabled = false;

                    txtCODE.Text = clCODE.FSCODE_CODE;
                    txtNAME.Text = clCODE.FSCODE_NAME;
                    txtORDER.Text = clCODE.FSCODE_ORDER;
                    txtNOTE.Text = clCODE.FSCODE_NOTE;
                    chkISENABLED.IsChecked = clCODE.FSCODE_ISENABLED;

                    break;
            }
        }  


#region"觸發事件"

        void clntLTF_InsertCodeCompleted(object sender, WSLogTemplateField.InsertCodeCompletedEventArgs e)
        {
            //如果取回的資料沒有錯誤訊息
            if (e.Error == null)
            {
                String msg = e.Result;
                if (msg  == "")
                {
                    //如果新增成功
                    MessageBox.Show("新增成功", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = true;
                }
                else
                {
                    //如果新增失敗
                    MessageBox.Show("新增時發生錯誤: " + msg.Substring(6), "提示訊息", MessageBoxButton.OK);
                    //this.DialogResult = false;
                }
            }
            else
            {
                //如果取回的資料有錯誤訊息
                MessageBox.Show("呼叫新增函數時發生錯誤: " + e.Error.ToString(), "提示訊息", MessageBoxButton.OK);
                //this.DialogResult = false;
            }
        }

        void clntLTF_UpdateCodeCompleted(object sender, WSLogTemplateField.UpdateCodeCompletedEventArgs e)
        {
            //如果取回的資料沒有錯誤訊息
            if (e.Error == null)
            {
                String msg = e.Result;
                if (msg == "")
                {
                    //如果新增成功
                    MessageBox.Show("修改成功", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = true;
                }
                else
                {
                    //如果新增失敗
                    MessageBox.Show("修改時發生錯誤: " + msg.Substring(6), "提示訊息", MessageBoxButton.OK);
                    //this.DialogResult = false;
                }
            }
            else
            {
                //如果取回的資料有錯誤訊息
                MessageBox.Show("呼叫修改函數時發生錯誤: " + e.Error.ToString(), "提示訊息", MessageBoxButton.OK);
                //this.DialogResult = false;
            }
        }

#endregion


#region"按鈕動作"

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (txtCODE.Text.ToUpper().Trim() == "")
            {
                MessageBox.Show("代碼不可為空", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (txtNAME.Text.ToUpper().Trim() == "")
            {
                MessageBox.Show("顯示名稱不可為空", "提示訊息", MessageBoxButton.OK);
                return;
            }

            switch (m_Action)
            {
                case "ADD":
                    clCODE.FSCODE_ID = m_ID;

                    if (chkISENABLED.IsChecked == true)
                    { clCODE.FSCODE_ISENABLED = true; }
                    else
                    { clCODE.FSCODE_ISENABLED = false; }

                    clCODE.FSCODE_CODE = txtCODE.Text.Trim();
                    clCODE.FSCODE_NAME = txtNAME.Text.Trim();
                    clCODE.FSCODE_ORDER = txtORDER.Text.Trim();
                    clCODE.FSCODE_NOTE = txtNOTE.Text.Trim();

                    clCODE.FSCREATED_BY = UserClass.userData.FSUSER_ID;;

                    clntLTF.InsertCodeAsync(clCODE);
                     
                    break;
                case "MOD":
                    clCODE.FSCODE_ID = m_ID;

                    if (chkISENABLED.IsChecked == true)
                    { clCODE.FSCODE_ISENABLED = true; }
                    else
                    { clCODE.FSCODE_ISENABLED = false; }

                    clCODE.FSCODE_CODE = txtCODE.Text.Trim();
                    clCODE.FSCODE_NAME = txtNAME.Text.Trim();
                    clCODE.FSCODE_ORDER = txtORDER.Text.Trim();
                    clCODE.FSCODE_NOTE = txtNOTE.Text.Trim();

                    clCODE.FSUPDATED_BY = UserClass.userData.FSUSER_ID;;

                    clntLTF.UpdateCodeAsync(clCODE);

                    break;
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

#endregion
    }
}

