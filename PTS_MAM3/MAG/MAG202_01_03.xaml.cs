﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.SystemAdmin
{
    public partial class Template_Edit : ChildWindow
    {

        int _fntemplate_id;
        string _fstemplate;
        string _fsdescription; 

        public Template_Edit(int FNTEMPLATE_ID, string FSTEMPLATE, string FSDESCRIPTION)
        {
            InitializeComponent();
            _fntemplate_id = FNTEMPLATE_ID;
            _fstemplate = FSTEMPLATE;
            _fsdescription = FSDESCRIPTION;
            this.tbxFSTEMPLATE.Text = _fstemplate;
            this.tbxFSDESCRIPTION.Text = _fsdescription; 
        }


        //修改
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            WSDirectoryAdmin.TBTemplateStruct t = new WSDirectoryAdmin.TBTemplateStruct();
            t.FNTEMPLATE_ID = _fntemplate_id;
            t.FSDESCRIPTION = tbxFSDESCRIPTION.Text.Trim() ;
            t.FSTEMPLATE = tbxFSTEMPLATE.Text.Trim() ;
            t.FSUPDATED_BY = UserClass.userData.FSUSER_ID;

            WSDirectoryAdmin.WSDirectoryAdminSoapClient dirObj = new WSDirectoryAdmin.WSDirectoryAdminSoapClient();
            dirObj.fnUpdateTBTEMPLATECompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result == false)
                    {
                        MessageBox.Show("更新資料時發生錯誤!");
                    }
                    else
                        this.DialogResult = true;            
                }                
            };
            dirObj.fnUpdateTBTEMPLATEAsync(t, UserClass.userData.FSUSER_ID);             
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

