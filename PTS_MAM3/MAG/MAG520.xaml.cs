﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;

namespace PTS_MAM3.MAG
{
    public partial class MAG520 : Page
    {
        public MAG520()
        {
            // 
            InitializeComponent();
            // 
            fnGetLouthJobResult();
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //
        }

        private void btnDetail_Click(object sender, RoutedEventArgs e)
        {
            // 判斷是否有選取DataGrid
            if (DG_LouthJobResult.SelectedItem == null)
            {
                MessageBox.Show("請選擇要查詢的項目", "提示訊息", MessageBoxButton.OK);
                return;
            }
            // 開啟子視窗
            MAG520_01 MAG520_01_frm = new MAG520_01(((SR_WSPGM_GetMssStatus.ClassLouthJob)DG_LouthJobResult.SelectedItem));
            MAG520_01_frm.Show();
            MAG520_01_frm.Closing += (s, args) =>
            {
                // 重新讀取主畫面的資料
                fnGetLouthJobResult();
            };
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            fnGetLouthJobResult();
        }

        private void DG_LouthJobResult_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            // 預設正常的前景、背景色
            e.Row.Background = new SolidColorBrush(Colors.White);
            e.Row.Foreground = new SolidColorBrush(Colors.Black);
            // Tricky: 檢查狀態，如果有含"失敗"字樣的，就要變色!
            SR_WSPGM_GetMssStatus.ClassLouthJob obj = e.Row.DataContext as SR_WSPGM_GetMssStatus.ClassLouthJob;
            if (obj.XmlFileStatus.Contains("失敗"))
                e.Row.Background = new SolidColorBrush(Colors.Red);
        }

        private void fnGetLouthJobResult()
        {
            // 因為作業時間比較長，所以要先關掉Button的動作
            fnDisableButtons();
            // 呼叫後端的 WebService 來執行取得檔案清單的動作
            SR_WSPGM_GetMssStatus.ServicePGMSoapClient obj = new SR_WSPGM_GetMssStatus.ServicePGMSoapClient();
            obj.GetMasterControlLouthStatusCompleted += new EventHandler<SR_WSPGM_GetMssStatus.GetMasterControlLouthStatusCompletedEventArgs>(obj_GetMasterControlLouthStatusCompleted);
            obj.GetMasterControlLouthStatusAsync();
        }

        private void fnDisableButtons()
        {
            btnRefresh.IsEnabled = false;
            btnDetail.IsEnabled = false;
        }

        private void fnEnableButtons()
        {
            btnRefresh.IsEnabled = true;
            btnDetail.IsEnabled = true;
        }

        private void obj_GetMasterControlLouthStatusCompleted(object sender, SR_WSPGM_GetMssStatus.GetMasterControlLouthStatusCompletedEventArgs e)
        {
            // 啟用按鍵
            fnEnableButtons();
            // 清除目前 DataGrid 的內容
            DG_LouthJobResult.DataContext = null;
            // 檢視回傳結果
            if (e.Error == null)
            {
                if (e.Result.Count == 0)
                {
                    MessageBox.Show("查不到相關的資料");
                    return;
                }
                // 
                DG_LouthJobResult.DataContext = e.Result;
            }
            else
                MessageBox.Show("連結主機服務時發生問題：" + e.Error.ToString());
        }
    }
}
