﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;

namespace PTS_MAM3.SystemAdmin
{
    public partial class DirTreeAdmin : UserControl
    {
        public DirTreeAdmin()
        {
            InitializeComponent();
            preLoadDirList(-1, null, true);
            this.btnSave.IsEnabled = false;
        }

        private void showShortMessageBox(string title, Object content)
        {
            ChildWindow cw = new ChildWindow();
            cw.Title = title;

            ScrollViewer sv = new ScrollViewer();

            sv.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
            sv.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;

            sv.Content = content;

            cw.Content = sv;

            cw.MaxWidth = 480;
            cw.MaxHeight = 360;
            cw.Show();


            cw.MouseLeftButtonDown += (s, args) =>
            {
                cw.Close();
            };
            cw.KeyDown += (s, args) =>
            {
                cw.Close();
            };
            return;
        }


        private void showShortMessageBox(string title, Object content, Control ctl)
        {
            ChildWindow cw = new ChildWindow();
            cw.Title = title;

            ScrollViewer sv = new ScrollViewer();

            sv.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
            sv.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;

            sv.Content = content;

            cw.Content = sv;

            cw.MaxWidth = 480;
            cw.MaxHeight = 360;
            cw.Show();


            cw.MouseLeftButtonDown += (s, args) =>
            {

                cw.Close();
                ctl.Focus();
            };
            cw.KeyDown += (s, args) =>
            {

                cw.Close();
                ctl.Focus();
            };


            return;
        }


        private void btnDirAdd_Click(object sender, RoutedEventArgs e)
        {

            if (TreeViewDirList.SelectedItem != null)
            {

                RadTreeViewItem treeNode = (RadTreeViewItem)TreeViewDirList.SelectedItem;
                RadTreeViewItem newNode = new RadTreeViewItem();
                newNode.Header = "新建目錄";

                WSDirectoryAdmin.DirectoryStruct node = new WSDirectoryAdmin.DirectoryStruct();
                node.FSDESCIPTION = "";
                node.FSDIR = "";
                node.FNDIR_ID = 99999999;
                node.FCQUEUE = "N";
                node.FDCREATED_DATE = DateTime.Now;
                node.FSUPDATED_BY = "";
                node.FSPARENT = ((WSDirectoryAdmin.DirectoryStruct)treeNode.Tag).FSDIR;
                node.FNPARENT_ID = ((WSDirectoryAdmin.DirectoryStruct)treeNode.Tag).FNDIR_ID;
                newNode.Tag = node;

                treeNode.Items.Add(newNode);

                treeNode.IsExpanded = true;
                newNode.IsSelected = true;
                TreeNodeSelected(newNode);
            }
            else
            {
                showShortMessageBox("提示", "未選取目錄節點");
            }
        }

        private void btnDirDel_Click(object sender, RoutedEventArgs e)
        {

            if (TreeViewDirList.SelectedItem != null)
            {

                if (((RadTreeViewItem)(TreeViewDirList.SelectedItem)).Items.Count > 0)
                {
                    showShortMessageBox("提示", "無法刪除含有子節點項目的節點!");
                }
                else
                {

                    Common.chWndMessageBox chWndMsgbox = new Common.chWndMessageBox("提示", "是否確認要刪除?", 0);

                    chWndMsgbox.Show();

                    chWndMsgbox.Closing += (s, args) =>
                    {
                        if (chWndMsgbox.DialogResult == true)
                        {
                            //進行刪除動作
                            WSDirectoryAdmin.WSDirectoryAdminSoapClient Obj_DirectoryAdmin = new WSDirectoryAdmin.WSDirectoryAdminSoapClient();
                            Obj_DirectoryAdmin.DELETE_TBDIRECTORIES_CHECKAsync(((WSDirectoryAdmin.DirectoryStruct)((RadTreeViewItem)TreeViewDirList.SelectedItem).Tag).FNDIR_ID.ToString(), UserClass.userData.FSUSER_ID.ToString());

                            Obj_DirectoryAdmin.DELETE_TBDIRECTORIES_CHECKCompleted += (objs, objargs) =>
                            {
                                if (objargs.Error == null)
                                {
                                    if (objargs.Result == "")
                                    {
                                        //刪除資料庫後，前端的畫面要先清掉後重新讀取
                                        TreeViewDirList.Items.Clear();
                                        preLoadDirList(-1, null, true);
                                        this.btnSave.IsEnabled = false;
                                        iniField(); //清空畫面上的欄位
                                        showShortMessageBox("訊息", "刪除節點成功");

                                        //畫面不重Load的話，也可以用下面的方法刪除畫面上的節點，但是因為刪除後上層資料夾的圖示也要再判斷，因此先不用這個方法
                                        //RadTreeViewItem treeViewItem = TreeViewDirList.ContainerFromItemRecursive(TreeViewDirList.SelectedItems[0]);
                                        //if (treeViewItem == null)                                       
                                        //    return;

                                        //if (treeViewItem.Parent is RadTreeViewItem)                                       
                                        //    (treeViewItem.Parent as RadTreeViewItem).Items.Remove(treeViewItem);                                       
                                        //else                                     
                                        //    this.TreeViewDirList.Items.Remove(treeViewItem);
                                    }
                                    else
                                    {
                                        if (objargs.Result.Length >= 3)
                                            showShortMessageBox("錯誤", "刪除失敗，原因：" + objargs.Result.Substring(2));
                                        else
                                            showShortMessageBox("錯誤", "刪除失敗");
                                    }
                                }
                            };
                        }

                    };
                }
            }
            else
            {
                showShortMessageBox("提示", "未選取目錄節點");
            }

        }

        //把畫面上的欄位清空
        private void iniField()
        {
            tbxFNDIR_ID.Text = "";
            tbxFSDIR.Text = "";
            tbxFSPARENT.Text = "";
            tbxCreated.Text = "";
            tbxUpdated.Text = "";
            tbxFSDESCIPTION.Text = "";
        }

        private void TreeNodeSelected(RadTreeViewItem item)
        {

            //lboxReadGroup.Items.Clear();
            //lboxOtherGroup.Items.Clear();

            iniField();

            WSDirectoryAdmin.DirectoryStruct dir = (WSDirectoryAdmin.DirectoryStruct)(item).Tag;
            if (dir.FNDIR_ID == 99999999)
            {
                this.tbxFNDIR_ID.Text = "";

            }
            else
            {

                WSDirectoryAdmin.WSDirectoryAdminSoapClient dirObj = new WSDirectoryAdmin.WSDirectoryAdminSoapClient();


                dirObj.GetReadableDirectoryGroupListByIdCompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (args.Result != null)
                        {

                            lboxReadGroup.Items.Clear();
                            foreach (WSDirectoryAdmin.DirectoryGroupStruct obj in args.Result)
                            {
                                System.Windows.Controls.ListBoxItem lItem = new System.Windows.Controls.ListBoxItem();
                                lItem.Content = obj.FSGROUP_ID;
                                lItem.Tag = obj;
                                lboxReadGroup.Items.Add(lItem);
                            }

                        }
                    }
                };

                dirObj.GetReadableDirectoryGroupListByIdAsync(dir.FNDIR_ID);

                dirObj.GetOtherDirectoryGroupListByIdCompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (args.Result != null)
                        {
                            lboxOtherGroup.Items.Clear();
                            foreach (WSDirectoryAdmin.DirectoryGroupStruct obj in args.Result)
                            {
                                System.Windows.Controls.ListBoxItem lItem = new System.Windows.Controls.ListBoxItem();
                                lItem.Content = obj.FSGROUP_ID;
                                lItem.Tag = obj;
                                this.lboxOtherGroup.Items.Add(lItem);
                            }
                        }
                    }
                };
                dirObj.GetOtherDirectoryGroupListByIdAsync(dir.FNDIR_ID);




                //  cbxSubjectTemple.SelectedIndex = -1;
                cbxVideoTemplate.SelectedIndex = -1;
                cbxAudioTemplate.SelectedIndex = -1;
                cbxDocTemplate.SelectedIndex = -1;
                cbxPhotoTemplate.SelectedIndex = -1;


                this.tbxFNDIR_ID.Text = Convert.ToString(dir.FNDIR_ID);
                //foreach (ComboBoxItem cbxItem in cbxSubjectTemple.Items)
                //{

                //    if (dir.FNTEMPLATE_ID_SUBJECT == ((WSDirectoryAdmin.TBTemplateStruct)cbxItem.Tag).FNTEMPLATE_ID)
                //    {
                //        cbxSubjectTemple.SelectedItem = cbxItem;
                //        break;
                //    }

                //}

                foreach (ComboBoxItem cbxItem in this.cbxAudioTemplate.Items)
                {

                    if (dir.FNTEMPLATE_ID_AUDIO == ((WSDirectoryAdmin.TBTemplateStruct)cbxItem.Tag).FNTEMPLATE_ID)
                    {
                        cbxAudioTemplate.SelectedItem = cbxItem;
                        break;
                    }

                }

                foreach (ComboBoxItem cbxItem in this.cbxPhotoTemplate.Items)
                {

                    if (dir.FNTEMPLATE_ID_PHOTO == ((WSDirectoryAdmin.TBTemplateStruct)cbxItem.Tag).FNTEMPLATE_ID)
                    {
                        cbxPhotoTemplate.SelectedItem = cbxItem;
                        break;
                    }

                }

                foreach (ComboBoxItem cbxItem in this.cbxDocTemplate.Items)
                {

                    if (dir.FNTEMPLATE_ID_DOC == ((WSDirectoryAdmin.TBTemplateStruct)cbxItem.Tag).FNTEMPLATE_ID)
                    {
                        cbxDocTemplate.SelectedItem = cbxItem;
                        break;
                    }

                }

                foreach (ComboBoxItem cbxItem in this.cbxVideoTemplate.Items)
                {

                    if (dir.FNTEMPLATE_ID_VIDEO_D == ((WSDirectoryAdmin.TBTemplateStruct)cbxItem.Tag).FNTEMPLATE_ID)
                    {
                        cbxVideoTemplate.SelectedItem = cbxItem;
                        break;
                    }

                }




            }
            this.tbxFSDESCIPTION.Text = dir.FSDESCIPTION;

            if (dir.FCQUEUE == "Y")
            {
                this.cbxFCQUEUE.SelectedIndex = 0;
                btnDirAdd.IsEnabled = false;
            }
            else
            {
                this.cbxFCQUEUE.SelectedIndex = 1;
                btnDirAdd.IsEnabled = true;
            }
            this.tbxFSDIR.Text = dir.FSDIR;
            if (dir.FSPARENT != null)
                this.tbxFSPARENT.Text = dir.FSPARENT;
            this.tbxCreated.Text = dir.FSCREATED_BY + " " + dir.FDCREATED_DATE;
            if (dir.FDUPDATED_DATE != "1900/1/1 上午 12:00:00")
            {
                this.tbxUpdated.Text = dir.FSUPDATED_BY + " " + dir.FDUPDATED_DATE;
            }

            this.btnSave.IsEnabled = true;
        }

        private void TreeViewDirList_Selected(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            TreeNodeSelected((RadTreeViewItem)e.Source);
        }

        private void TreeViewDirList_LoadOnDemand(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            WSDirectoryAdmin.DirectoryStruct dir = (WSDirectoryAdmin.DirectoryStruct)((RadTreeViewItem)e.Source).Tag;
            preLoadDirList(dir.FNDIR_ID, (RadTreeViewItem)e.Source, false);
        }


        private void preLoadDirList(int FNPARENT_ID, RadTreeViewItem treeNode, Boolean isFirst)
        {
            WSDirectoryAdmin.WSDirectoryAdminSoapClient myService = new WSDirectoryAdmin.WSDirectoryAdminSoapClient();

            myService.GetDirListByIdCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        foreach (WSDirectoryAdmin.DirectoryStruct node in args.Result)
                        {
                            RadTreeViewItem newNode = new RadTreeViewItem();
                            newNode.Header = node.FSDIR;
                            newNode.Tag = node;
                            if (node.FCQUEUE == "Y")
                            {
                                newNode.DefaultImageSrc = new Uri("../Images/16_folder_edit.png", UriKind.Relative);
                            }
                            else
                            {
                                newNode.DefaultImageSrc = new Uri("../Images/16_folder.png", UriKind.Relative);
                                newNode.ExpandedImageSrc = new Uri("../Images/16_folderopen.png", UriKind.Relative);
                            }
                            if (treeNode == null)
                            {
                                this.TreeViewDirList.Items.Add(newNode);

                            }
                            else
                            {
                                treeNode.Items.Add(newNode);

                            }
                            if (isFirst)
                            {
                                preLoadDirList(((WSDirectoryAdmin.DirectoryStruct)newNode.Tag).FNDIR_ID, newNode, false);
                                newNode.IsExpanded = true;
                            }
                        }

                        if (treeNode == null)
                        {
                            busyLoading1.IsBusy = false;

                            //TreeViewDeptList.IsLoadOnDemandEnabled = false;
                        }
                        else
                        {
                            treeNode.IsLoadOnDemandEnabled = false;
                        }
                    }

                }
            };

            myService.GetDirListByIdAsync(FNPARENT_ID);

        }

        //儲存
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            int intCheck;

            if (tbxFSDIR.Text.Trim() == string.Empty)
            {
                showShortMessageBox("提示", "請輸入名稱", this.tbxFSDIR);
                return;
            }

            //新建或修改的節點
            WSDirectoryAdmin.DirectoryStruct dirItem = (WSDirectoryAdmin.DirectoryStruct)((RadTreeViewItem)TreeViewDirList.SelectedItem).Tag;

            dirItem.FCQUEUE = (string)cbxFCQUEUE.SelectionBoxItem;

            //dirItem.FDUPDATED_DATE = DateTime.Now.ToString; 
            dirItem.FSDIR = tbxFSDIR.Text.Trim();

            dirItem.FNTEMPLATE_ID_AUDIO = ((WSDirectoryAdmin.TBTemplateStruct)((ComboBoxItem)cbxAudioTemplate.SelectedItem).Tag).FNTEMPLATE_ID;
            dirItem.FNTEMPLATE_ID_DOC = ((WSDirectoryAdmin.TBTemplateStruct)((ComboBoxItem)cbxDocTemplate.SelectedItem).Tag).FNTEMPLATE_ID;
            dirItem.FNTEMPLATE_ID_PHOTO = ((WSDirectoryAdmin.TBTemplateStruct)((ComboBoxItem)cbxPhotoTemplate.SelectedItem).Tag).FNTEMPLATE_ID;
            dirItem.FNTEMPLATE_ID_VIDEO_D = ((WSDirectoryAdmin.TBTemplateStruct)((ComboBoxItem)cbxVideoTemplate.SelectedItem).Tag).FNTEMPLATE_ID;

            dirItem.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
            dirItem.FSDESCIPTION = tbxFSDESCIPTION.Text.Trim();

            WSDirectoryAdmin.WSDirectoryAdminSoapClient dirObj = new WSDirectoryAdmin.WSDirectoryAdminSoapClient();

            if (dirItem.FNDIR_ID == 99999999)  //資料新增
            {
                dirItem.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                dirItem.FDCREATED_DATE = DateTime.Now;

                dirObj.AddDirectoryItemAsync(dirItem);

            }
            else // 資料更新
            {
                dirObj.UpdateDirectoryItemAsync(dirItem);
            }


            dirObj.AddDirectoryItemCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result == string.Empty || args.Result.StartsWith("E"))
                    {
                        if (args.Result.Length >= 3)
                            showShortMessageBox("錯誤", "新增失敗，原因：" + args.Result.Substring(2));
                        else
                            showShortMessageBox("錯誤", "新增失敗");
                    }
                    else
                    {
                        if (int.TryParse(args.Result, out intCheck) == false)
                        {
                            showShortMessageBox("錯誤", "新增失敗，原因：取回來的DIRID轉型錯誤");
                            return;
                        }

                        showShortMessageBox("訊息", "新增完畢");

                        dirItem.FNDIR_ID = intCheck;    //把取回來的DIR_ID寫進去
                        ((RadTreeViewItem)TreeViewDirList.SelectedItem).Tag = dirItem;
                        ((RadTreeViewItem)TreeViewDirList.SelectedItem).Header = dirItem.FSDIR;

                        if (dirItem.FCQUEUE == "Y")
                        {
                            btnDirAdd.IsEnabled = false;
                            ((RadTreeViewItem)TreeViewDirList.SelectedItem).DefaultImageSrc = new Uri("../Images/16_folder_edit.png", UriKind.Relative);
                        }
                        else
                        {
                            btnDirAdd.IsEnabled = true;
                            ((RadTreeViewItem)TreeViewDirList.SelectedItem).DefaultImageSrc = new Uri("../Images/16_folder.png", UriKind.Relative);
                            ((RadTreeViewItem)TreeViewDirList.SelectedItem).ExpandedImageSrc = new Uri("../Images/16_folderopen.png", UriKind.Relative);
                        }

                    }
                }
            };

            dirObj.UpdateDirectoryItemCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != string.Empty)
                        showShortMessageBox("錯誤", args.Result);
                    else
                    {
                        showShortMessageBox("訊息", "更新完畢");
                        ((RadTreeViewItem)TreeViewDirList.SelectedItem).Tag = dirItem;
                        ((RadTreeViewItem)TreeViewDirList.SelectedItem).Header = dirItem.FSDIR;

                        if (dirItem.FCQUEUE == "Y")
                        {
                            btnDirAdd.IsEnabled = false;
                            ((RadTreeViewItem)TreeViewDirList.SelectedItem).DefaultImageSrc = new Uri("../Images/16_folder_edit.png", UriKind.Relative);
                        }
                        else
                        {
                            btnDirAdd.IsEnabled = true;
                            ((RadTreeViewItem)TreeViewDirList.SelectedItem).DefaultImageSrc = new Uri("../Images/16_folder.png", UriKind.Relative);
                            ((RadTreeViewItem)TreeViewDirList.SelectedItem).ExpandedImageSrc = new Uri("../Images/16_folderopen.png", UriKind.Relative);
                        }
                    }
                }
            };





        }



        private void btnApplyToChild_Click(object sender, RoutedEventArgs e)
        {

        }


        private void genCbxTemplate(string FSTABLE, ComboBox cbx)
        {
            WSDirectoryAdmin.WSDirectoryAdminSoapClient DirectoryObj = new WSDirectoryAdmin.WSDirectoryAdminSoapClient();

            DirectoryObj.GetTemplateListByTableCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        foreach (WSDirectoryAdmin.TBTemplateStruct node in args.Result)
                        {
                            ComboBoxItem item = new ComboBoxItem();
                            item.Content = node.FSTEMPLATE;
                            item.Tag = node;
                            cbx.Items.Add(item);
                        }
                    }
                }

            };

            DirectoryObj.GetTemplateListByTableAsync(FSTABLE);
        }

        //
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (lboxOtherGroup.SelectedItems.Count == 0)
            {
                showShortMessageBox("提示", "請選取\"群組清單\"項目");

                return;
            }
            else
            {
                System.Windows.Controls.ListBoxItem lbox = (System.Windows.Controls.ListBoxItem)lboxOtherGroup.SelectedItem;
                System.Windows.Controls.ListBoxItem newBoxItem = new System.Windows.Controls.ListBoxItem();

                newBoxItem.Content = lbox.Content;
                newBoxItem.Tag = lbox.Tag;

                lboxReadGroup.Items.Add(newBoxItem);
                lboxOtherGroup.Items.Remove(lbox);



                WSMAMFunctions.WSMAMFunctionsSoapClient MAMObj = new WSMAMFunctions.WSMAMFunctionsSoapClient();

                string sSql = string.Empty;
                WSDirectoryAdmin.DirectoryStruct dir = (WSDirectoryAdmin.DirectoryStruct)((RadTreeViewItem)TreeViewDirList.SelectedItems[0]).Tag;


                sSql = "insert TBDIRECTORY_GROUP (FNDIR_ID,FSGROUP_ID,FCPRIVILEGE,FSCREATED_BY,FDCREATED_DATE) ";
                sSql += "values (" + dir.FNDIR_ID + ",'" + ((WSDirectoryAdmin.DirectoryGroupStruct)newBoxItem.Tag).FSGROUP_ID + "','R','" + UserClass.userData.FSUSER_ID + "',getdate())";


                MAMObj.fnExecuteSqlCompleted += (s, args) =>
                {
                    if (args.Error != null)
                    {
                        showShortMessageBox("錯誤", args.Error.ToString());
                    }
                    else if (args.Result != string.Empty)
                    {
                        showShortMessageBox("錯誤", args.Result);
                    }

                };

                MAMObj.fnGetValueAsync(sSql);

                //WSDirectoryAdmin.WSDirectoryAdminSoapClient dirObj = new WSDirectoryAdmin.WSDirectoryAdminSoapClient(); 
                //dirObj.InsertDirectGroupCompleted += (s , args ) => {

                //};
                // WSDirectoryAdmin.DirectoryStruct dir = (WSDirectoryAdmin.DirectoryStruct)((RadTreeViewItem) TreeViewDirList.SelectedItems[0]).Tag;


                //dirObj.InsertDirectGroupAsync(dir.FNDIR_ID, 

            }

        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            if (this.lboxReadGroup.SelectedItems.Count == 0)
            {
                showShortMessageBox("提示", "請選取\"可讀群組\"項目");
                return;
            }
            else
            {
                System.Windows.Controls.ListBoxItem lbox = (System.Windows.Controls.ListBoxItem)this.lboxReadGroup.SelectedItem;
                System.Windows.Controls.ListBoxItem newBoxItem = new System.Windows.Controls.ListBoxItem();

                newBoxItem.Content = lbox.Content;
                newBoxItem.Tag = lbox.Tag;



                lboxOtherGroup.Items.Add(newBoxItem);
                lboxReadGroup.Items.Remove(lbox);

                WSMAMFunctions.WSMAMFunctionsSoapClient MAMObj = new WSMAMFunctions.WSMAMFunctionsSoapClient();

                string sSql = string.Empty;
                WSDirectoryAdmin.DirectoryStruct dir = (WSDirectoryAdmin.DirectoryStruct)((RadTreeViewItem)TreeViewDirList.SelectedItems[0]).Tag;


                sSql = "DELETE TBDIRECTORY_GROUP ";
                sSql += "WHERE FNDIR_ID = " + dir.FNDIR_ID + " AND  FSGROUP_ID = '" + ((WSDirectoryAdmin.DirectoryGroupStruct)newBoxItem.Tag).FSGROUP_ID + "' ";


                MAMObj.fnExecuteSqlCompleted += (s, args) =>
                {
                    if (args.Error != null)
                    {
                        showShortMessageBox("錯誤", args.Error.ToString());
                    }
                    else if (args.Result != string.Empty)
                    {
                        showShortMessageBox("錯誤", args.Result);
                    }

                };

                MAMObj.fnGetValueAsync(sSql);


            }
        }

        //private void cbxSubjectTemple_Loaded(object sender, RoutedEventArgs e)
        //{

        //    genCbxTemplate("TBSUBJECT", cbxSubjectTemple);
        //}



        private void cbxPhotoTemplate_Loaded(object sender, RoutedEventArgs e)
        {
            genCbxTemplate("TBLOG_PHOTO", cbxPhotoTemplate);
        }

        private void cbxDocTemplate_Loaded(object sender, RoutedEventArgs e)
        {
            genCbxTemplate("TBLOG_DOC", cbxDocTemplate);
        }

        private void cbxAudioTemplate_Loaded(object sender, RoutedEventArgs e)
        {
            genCbxTemplate("TBLOG_AUDIO", cbxAudioTemplate);
        }

        private void cbxVideoTemplate_Loaded(object sender, RoutedEventArgs e)
        {
            genCbxTemplate("TBLOG_VIDEO_D", cbxVideoTemplate);
        }

        //private void btnSetSubjectTemplate_Click(object sender, RoutedEventArgs e)
        //{
        //    System.Windows.Controls.ComboBoxItem cbxItem = (System.Windows.Controls.ComboBoxItem)cbxSubjectTemple.SelectedItem;
        //    if (cbxItem == null)
        //    {
        //        showShortMessageBox("錯誤", "尚未選取節點項目");
        //    }
        //    else
        //    {
        //        TemplateDefine tplWnd = new TemplateDefine("TBSUBJECT", ((WSDirectoryAdmin.TBTemplateStruct)cbxItem.Tag).FNTEMPLATE_ID);

        //        tplWnd.Show();

        //    }

        //}

        private void btnSetPhotoTemplate_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.ComboBoxItem cbxItem = (System.Windows.Controls.ComboBoxItem)this.cbxPhotoTemplate.SelectedItem;
            if (cbxItem == null)
            {
                showShortMessageBox("錯誤", "尚未選取節點項目");
            }
            else
            {
                TemplateDefine tplWnd = new TemplateDefine("TBLOG_PHOTO", ((WSDirectoryAdmin.TBTemplateStruct)cbxItem.Tag).FNTEMPLATE_ID);

                tplWnd.Show();

            }
        }

        private void btnSetDocTemplate_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.ComboBoxItem cbxItem = (System.Windows.Controls.ComboBoxItem)cbxDocTemplate.SelectedItem;
            if (cbxItem == null)
            {
                showShortMessageBox("錯誤", "尚未選取節點項目");
            }
            else
            {
                TemplateDefine tplWnd = new TemplateDefine("TBLOG_DOC", ((WSDirectoryAdmin.TBTemplateStruct)cbxItem.Tag).FNTEMPLATE_ID);



                tplWnd.Show();

            }
        }

        private void btnSetAudioTemplate_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.ComboBoxItem cbxItem = (System.Windows.Controls.ComboBoxItem)cbxAudioTemplate.SelectedItem;
            if (cbxItem == null)
            {
                showShortMessageBox("錯誤", "尚未選取節點項目");
            }
            else
            {
                TemplateDefine tplWnd = new TemplateDefine("TBLOG_AUDIO", ((WSDirectoryAdmin.TBTemplateStruct)cbxItem.Tag).FNTEMPLATE_ID);

                tplWnd.Show();

            }
        }

        private void btnSetVideoTemplate_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.ComboBoxItem cbxItem = (System.Windows.Controls.ComboBoxItem)cbxVideoTemplate.SelectedItem;
            if (cbxItem == null)
            {
                showShortMessageBox("錯誤", "尚未選取節點項目");
            }
            else
            {
                TemplateDefine tplWnd = new TemplateDefine("TBLOG_VIDEO_D", ((WSDirectoryAdmin.TBTemplateStruct)cbxItem.Tag).FNTEMPLATE_ID);

                tplWnd.Show();

            }
        }

    }
}
