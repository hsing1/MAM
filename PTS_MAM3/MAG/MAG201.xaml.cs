﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using System.Text;
using System.Xml.Linq;

using System.IO;
using PTS_MAM3.WSPROG_PRODUCER;

namespace PTS_MAM3.SystemAdmin
{
    public partial class GroupAdmin : UserControl
    {

        WSPROG_PRODUCERSoapClient client = new WSPROG_PRODUCERSoapClient();   

        private int intCbx1 = 0, intCbx2 = -1, intCbx3 = -1, intCbx4 = -1;
        Boolean refreshok = false;

        List<MyDept> deptlist1 = new List<MyDept>();
        List<MyDept> deptlist2 = new List<MyDept>();
        List<MyDept> deptlist3 = new List<MyDept>();
        List<MyUser> userlist4 = new List<MyUser>();

        List<UserStruct> ListUserAll = new List<UserStruct>();                  //所有的使用者資料
        WSREPORT.WSREPORTSoapClient objReport1 = new WSREPORT.WSREPORTSoapClient();
        WSREPORT.WSREPORTSoapClient objReport2 = new WSREPORT.WSREPORTSoapClient();
        WSREPORT.WSREPORTSoapClient objReport3 = new WSREPORT.WSREPORTSoapClient();
        WSREPORT.WSREPORTSoapClient objReport4 = new WSREPORT.WSREPORTSoapClient();
        public GroupAdmin()
        {
            InitializeComponent();


            cbxDept1.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(cbxDept1_SelectionChanged);
            cbxDept2.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(cbxDept2_SelectionChanged);
            cbxDept3.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(cbxDept3_SelectionChanged);
            cbxUser.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(cbxUser_SelectionChanged);


            objReport1.fnGetDeptListCompleted += new EventHandler<WSREPORT.fnGetDeptListCompletedEventArgs>(objReport1_fnGetDeptListCompleted);
            objReport2.fnGetDeptListCompleted += new EventHandler<WSREPORT.fnGetDeptListCompletedEventArgs>(objReport2_fnGetDeptListCompleted);
            objReport3.fnGetDeptListCompleted += new EventHandler<WSREPORT.fnGetDeptListCompletedEventArgs>(objReport3_fnGetDeptListCompleted);

            objReport4.fnGetTBUSERS_BY_DEPT_IDCompleted += new EventHandler<WSREPORT.fnGetTBUSERS_BY_DEPT_IDCompletedEventArgs>(objReport4_fnGetTBUSERS_BY_DEPT_IDCompleted);

           
            objReport1.fnGetDeptListAsync(intCbx1);

            preLoadGroupList(-1, null, true);


            client.QUERY_TBUSERS_ALLCompleted += new EventHandler<QUERY_TBUSERS_ALLCompletedEventArgs>(client_QUERY_TBUSERS_ALLCompleted);
            client.QUERY_TBUSERS_ALLAsync();

        }




        #region 資料查詢結束

        void objReport1_fnGetDeptListCompleted(object sender, WSREPORT.fnGetDeptListCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                cbxDept2.ItemsSource = null;
                cbxDept3.ItemsSource = null;
                cbxUser.ItemsSource = null;

                if (e.Result.Count == 0)
                {
                    MessageBox.Show("查詢單位時發生錯誤");
                    return;
                }
                else
                {
                    deptlist1.Clear();

                    foreach (WSREPORT.DeptStruct dept in e.Result)
                    {
                        deptlist1.Add(new MyDept(dept.FSDpeName_ChtName, dept.FNDEP_ID));
                    }

                    cbxDept1.ItemsSource = deptlist1;
                }
            }
        }

        void objReport2_fnGetDeptListCompleted(object sender, WSREPORT.fnGetDeptListCompletedEventArgs e)
        {
            deptlist2.Clear();

            cbxDept2.ItemsSource = null;
            cbxDept3.ItemsSource = null;
            cbxUser.ItemsSource = null;

            if (e.Error == null)
            {
                if (e.Result.Count == 0)
                {
                    //MessageBox.Show("查不到子單位");
                    cbxDept3.ItemsSource = null;
                    return;
                }
                else
                {


                    if (intCbx2 != -1)
                    {
                        foreach (WSREPORT.DeptStruct dept in e.Result)
                        {
                            deptlist2.Add(new MyDept(dept.FSDpeName_ChtName, dept.FNDEP_ID));
                        }
                    }

                    cbxDept2.ItemsSource = deptlist2;
                }
            }
        }

        void objReport3_fnGetDeptListCompleted(object sender, WSREPORT.fnGetDeptListCompletedEventArgs e)
        {

            deptlist3.Clear();

            cbxDept3.ItemsSource = null;
            cbxUser.ItemsSource = null;

            if (e.Error == null)
            {
                if (e.Result.Count == 0)
                {
                    //MessageBox.Show("查不到子單位");
                    return;
                }
                else
                {


                    if (intCbx3 != -1)
                    {
                        foreach (WSREPORT.DeptStruct dept in e.Result)
                        {
                            deptlist3.Add(new MyDept(dept.FSDpeName_ChtName, dept.FNDEP_ID));
                        }
                    }

                    cbxDept3.ItemsSource = deptlist3;
                }
            }
        }
        void objReport4_fnGetTBUSERS_BY_DEPT_IDCompleted(object sender, WSREPORT.fnGetTBUSERS_BY_DEPT_IDCompletedEventArgs e)
        {
            if (refreshok)
            {
                return;
            }
            else
            {
                refreshok = true;
            }

            userlist4.Clear();

            cbxUser.ItemsSource = null;
            if (e.Error == null)
            {
                if (e.Result.Count == 0)
                {
                    //MessageBox.Show("此單位查無人員清單");
                    return;
                }
                else
                {


                    //if (intCbx4 != -1)
                    //{
                    foreach (WSREPORT.UserStruct user in e.Result)
                    {
                        String _Name = user.FSUPPERDEPT_CHTNAME + " \\ " +
                                     ((user.FSDEP_CHTNAME.Trim() == "") ? "" : (user.FSDEP_CHTNAME.Trim() + " \\ ")) +
                                     ((user.FSGROUP_CHTNAME.Trim() == "") ? "" : (user.FSGROUP_CHTNAME.Trim() + " \\ ")) +
                                     user.FSUSER_ID + "-" + user.FSUSER_ChtName;

                        userlist4.Add(new MyUser(_Name, user.FSUSER_ID));
                    }
                    //}

                    cbxUser.ItemsSource = userlist4;

                    
                }
            }
        } 

        #endregion


        #region 下拉選單變更


        void cbxDept1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            refreshok = false;
            intCbx2 = ((MyDept)cbxDept1.SelectedItem).id;
            objReport2.fnGetDeptListAsync(intCbx2);
            objReport4.fnGetTBUSERS_BY_DEPT_IDAsync(intCbx2.ToString());
        }

        void cbxDept2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            refreshok = false;
            if (cbxDept2.Items.Count != 0)
            {
                intCbx3 = ((MyDept)cbxDept2.SelectedItem).id;
                objReport3.fnGetDeptListAsync(intCbx3);
            }
            else
            {
                cbxDept3.ItemsSource = null;
            }
            objReport4.fnGetTBUSERS_BY_DEPT_IDAsync(intCbx3.ToString());
        }

        void cbxDept3_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            refreshok = false;
            if (cbxDept3.Items.Count != 0)
            {
                intCbx4 = ((MyDept)cbxDept3.SelectedItem).id;
            }
            //else
            //{
            //    cbxUser.ItemsSource = null;
            //}
            objReport4.fnGetTBUSERS_BY_DEPT_IDAsync(intCbx4.ToString());
        }

        void cbxUser_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //throw new NotImplementedException();
           

        }


        #endregion



        private void preLoadGroupList(int FNPARENT_ID, Telerik.Windows.Controls.RadTreeViewItem treeNode, Boolean isFirst)
        {
            WSGroupAdmin.WSGroupAdminSoapClient GroupObj = new WSGroupAdmin.WSGroupAdminSoapClient();
            
            WSGroupAdmin.TBGroupStruct t = new WSGroupAdmin.TBGroupStruct();

            t.FSGROUP_ID = string.Empty;

            GroupObj.fnGetTBGROUPSCompleted += (e, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        GroupNode.Items.Clear();
                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        //StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));

                        var ltox = from s in doc.Elements("Datas").Elements("Data")
                                   select s;




                        TreeViewGroupList.IsLoadOnDemandEnabled = false;
                        foreach (XElement elem in ltox)
                        {
                            Telerik.Windows.Controls.RadTreeViewItem treeItem = new Telerik.Windows.Controls.RadTreeViewItem();
                            treeItem.Header = elem.Element("FSGROUP").Value.ToString();
                            treeItem.Tag = elem;
                            //treeItem.IsExpanded = true;
                            treeItem.IsLoadingOnDemand = false;
                            GroupNode.Items.Add(treeItem);

                            //Text = elem.Element("FSFSGROUP").Value.ToString();
                        }
                        GroupNode.IsSelected = true;
                        GroupNode.IsExpanded = true;
                        GroupNode.IsLoadingOnDemand = false;

                    }
                }

            };
            GroupObj.fnGetTBGROUPSAsync(t);

        }

        private void TreeViewGroupList_LoadOnDemand(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {

        }

        
        //顯示群組資料
        private void TreeViewGroupList_Selected(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            btnSave.IsEnabled = false;
            string strContent = ""; //顯示的內容
            //WSUserAdmin.DeptStruct Dept = (WSUserAdmin.DeptStruct)((RadTreeViewItem)e.Source).Tag;

           // this.lboxOtherUsers.Items.Clear();
            this.lboxGroupUsers.Items.Clear();

            btnGroupDelete.IsEnabled = false;

            if (((Telerik.Windows.Controls.RadTreeViewItem)e.Source).Tag != null)
            {


                XElement GroupObj = (XElement)((Telerik.Windows.Controls.RadTreeViewItem)e.Source).Tag;



                if (GroupObj != null)
                {
                    btnAdd.IsEnabled = true; 
                    busyLoading1.IsBusy = true;
                    tbxFSGROUP_ID.Text = GroupObj.Element("FSGROUP_ID").Value.ToString();
                    tbxFSGROUP.Text = GroupObj.Element("FSGROUP").Value.ToString();
                    tbxFSDESCRIPTION.Text = GroupObj.Element("FSDESCRIPTION").Value.ToString();
                    tbxCREATED.Text = GroupObj.Element("FSCREATED_BY").Value.ToString() + "/" + GroupObj.Element("FDCREATED_DATE").Value.ToString();
                    tbxUPDATED.Text = GroupObj.Element("FSUPDATED_BY").Value.ToString() + "/" + GroupObj.Element("FDUPDATED_DATE").Value.ToString();

                    WSUserObject.WSUserObjectSoapClient userObj = new WSUserObject.WSUserObjectSoapClient(); 
                   // UserObjectSvc.UserObjectClient userObj = new UserObjectSvc.UserObjectClient();

                    Boolean bisQry1 = false;

                    Boolean bisQry2 = false;

                    userObj.fnGetTBUSERS_BY_GROUP_IDCompleted += (s, args) =>
                    {
                        this.lboxGroupUsers.Items.Clear();
                        if (args.Error == null)
                        {
                            if (args.Result != null)
                            {
                                foreach (WSUserObject.UserStruct user in args.Result)
                                {                                   
                                    if (user.FSDESCRIPTION == "") //顯示內容
                                        strContent = "(" + user.FSUPPERDEPT_CHTNAME + "/" + user.FSDEP_CHTNAME + "/" + user.FSGROUP_CHTNAME + ")" + user.FSUSER_ChtName;
                                    else
                                        strContent ="(" + user.FSUPPERDEPT_CHTNAME + "/" + user.FSDEP_CHTNAME + "/" + user.FSGROUP_CHTNAME + ")" + user.FSUSER_ChtName + "-" + user.FSDESCRIPTION.Trim();

                                    System.Windows.Controls.ListBoxItem lItem = new System.Windows.Controls.ListBoxItem();
                                    lItem.Content = strContent;
                                    lItem.Tag = user;
                                    lboxGroupUsers.Items.Add(lItem);
                                }

                            }
                        }
                        bisQry1 = true;
                        if (bisQry1 && bisQry2)
                        {
                            busyLoading1.IsBusy = false;
                            btnGroupDelete.IsEnabled = true; 
                        }
                    };
                    userObj.fnGetTBUSERS_BY_GROUP_IDAsync(GroupObj.Element("FSGROUP_ID").Value.ToString());


                    userObj.fnGetTBUSERS_NOT_IN_GROUPCompleted += (s, args) =>
                    {

                       // this.lboxOtherUsers.Items.Clear();
                        if (args.Error == null)
                        {
                            if (args.Result != null)
                            {


                                foreach (WSUserObject.UserStruct user in args.Result)
                                {
                                    //lboxGroupUsers.Items.Clea
                                    System.Windows.Controls.ListBoxItem lItem = new System.Windows.Controls.ListBoxItem();
                                    lItem.Content = "(" + user.FSUPPERDEPT_CHTNAME + "/" + user.FSDEP_CHTNAME + "/" + user.FSGROUP_CHTNAME + ")" + user.FSUSER_ChtName;
                                    lItem.Tag = user;
                                //    lboxOtherUsers.Items.Add(lItem);
                                }

                            }
                        }
                        bisQry2 = true;
                        if (bisQry1 && bisQry2)
                        {
                            busyLoading1.IsBusy = false;
                            btnGroupDelete.IsEnabled = true;
                        }

                    };

                    userObj.fnGetTBUSERS_NOT_IN_GROUPAsync(GroupObj.Element("FSGROUP_ID").Value.ToString());

                }
                else
                {
                    btnAdd.IsEnabled = false;
                    tbxFSGROUP_ID.Text = string.Empty;
                    tbxFSGROUP.Text = string.Empty;
                    tbxFSDESCRIPTION.Text = string.Empty;
                    tbxCREATED.Text = string.Empty;
                    tbxUPDATED.Text = string.Empty;
                    btnGroupDelete.IsEnabled = false;

                }
            }
            else
            {
                tbxFSGROUP_ID.Text = string.Empty;
                tbxFSGROUP.Text = string.Empty;
                tbxFSDESCRIPTION.Text = string.Empty;
                tbxCREATED.Text = string.Empty;
                tbxUPDATED.Text = string.Empty;
                btnGroupDelete.IsEnabled = false;
            }
      
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            //if (this.cbxUser.SelectedItem == null)
            //{
            //    MessageBox.Show("請選取欲加入此群組\"人員\"", "提示", MessageBoxButton.OK);
            //    return;
            //}


            if (this.acbMenID.SelectedItem == null)
            {
                MessageBox.Show("請選取欲加入此群組\"人員\"", "提示", MessageBoxButton.OK);
                return;
            }








            foreach (System.Windows.Controls.ListBoxItem lbox in lboxGroupUsers.Items)
            {
                //if (((WSUserObject.UserStruct)lbox.Tag).FSUSER_ID == ((MyUser)cbxUser.SelectedItem).id)
                //{
                //    MessageBox.Show("此使用者已位於此群組內!", "提示", MessageBoxButton.OK);
                //    return;
                //}

                 if (((UserStruct)(acbMenID.SelectedItem)).FSUSER_ID == ((WSUserObject.UserStruct)lbox.Tag).FSUSER_ID ) {
                     MessageBox.Show("此使用者已位於此群組內!", "提示", MessageBoxButton.OK);
                     return;
                }
            }


            btnAdd.IsEnabled = false; 

            WSGroupAdmin.TBUser_GroupStruct t = new WSGroupAdmin.TBUser_GroupStruct();
            XElement xGroupObj = (XElement)((Telerik.Windows.Controls.RadTreeViewItem)TreeViewGroupList.SelectedItem).Tag;
            t.FSUSER_ID = ((UserStruct)(acbMenID.SelectedItem)).FSUSER_ID;
            t.FSGROUP_ID = xGroupObj.Element("FSGROUP_ID").Value.ToString(); 
            t.FSCREATED_BY = UserClass.userData.FSUSER_ID;
            t.FSUPDATED_BY = UserClass.userData.FSUSER_ID;

            WSGroupAdmin.WSGroupAdminSoapClient GroupObj = new WSGroupAdmin.WSGroupAdminSoapClient();
            WSUserObject.WSUserObjectSoapClient userObj = new WSUserObject.WSUserObjectSoapClient(); 

            GroupObj.fnInsertTBUSER_GROUPCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result)
                    {
                        userObj.fnGetTBUSERS_BY_GROUP_IDAsync(xGroupObj.Element("FSGROUP_ID").Value.ToString());
                    }
                    else
                    {
                        MessageBox.Show("異動資料時發生錯誤!", "錯誤", MessageBoxButton.OK);
                        btnAdd.IsEnabled = true;
                    }
                }
                
            };
            GroupObj.fnInsertTBUSER_GROUPAsync(t, UserClass.userData.FSUSER_ID);

        
            userObj.fnGetTBUSERS_BY_GROUP_IDCompleted += (s, args) =>
            {
                this.lboxGroupUsers.Items.Clear();
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {


                        foreach (WSUserObject.UserStruct user in args.Result)
                        {
                            //lboxGroupUsers.Items.Clea
                            System.Windows.Controls.ListBoxItem lItem = new System.Windows.Controls.ListBoxItem();
                            lItem.Content = "(" + user.FSUPPERDEPT_CHTNAME + "/" + user.FSDEP_CHTNAME + "/" + user.FSGROUP_CHTNAME + ")" + user.FSUSER_ChtName;
                            lItem.Tag = user;
                            lboxGroupUsers.Items.Add(lItem);
                        }

                    }
                }
                btnAdd.IsEnabled = true;
                MessageBox.Show("新增資料完成!", "資訊", MessageBoxButton.OK);
            };
         





        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            if (this.lboxGroupUsers.SelectedItems.Count == 0)
            {
                MessageBox.Show("請選取欲移除自此群組\"人員\"", "提示", MessageBoxButton.OK);

                return;
            }
            else
            {
                System.Windows.Controls.ListBoxItem lbox = (System.Windows.Controls.ListBoxItem)lboxGroupUsers.SelectedItem;
                System.Windows.Controls.ListBoxItem newBoxItem = new System.Windows.Controls.ListBoxItem();

                newBoxItem.Content = lbox.Content;
                newBoxItem.Tag = lbox.Tag;

             //   lboxOtherUsers.Items.Add(newBoxItem);
                lboxGroupUsers.Items.Remove(lbox);




                WSGroupAdmin.TBUser_GroupStruct t = new WSGroupAdmin.TBUser_GroupStruct();
                XElement xGroupObj = (XElement)((Telerik.Windows.Controls.RadTreeViewItem)TreeViewGroupList.SelectedItem).Tag;
                t.FSUSER_ID = ((WSUserObject.UserStruct)newBoxItem.Tag).FSUSER_ID;
                t.FSGROUP_ID = xGroupObj.Element("FSGROUP_ID").Value.ToString();
            

                WSGroupAdmin.WSGroupAdminSoapClient GroupObj = new WSGroupAdmin.WSGroupAdminSoapClient();

                GroupObj.fnDeleteTBUSER_GROUPCompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (args.Result)
                        {
                            MessageBox.Show("異動資料完成!", "資訊", MessageBoxButton.OK);
                        }
                        else
                        {
                            MessageBox.Show("異動資料時發生錯誤!", "錯誤", MessageBoxButton.OK);
                        }
                    }

                };
                GroupObj.fnDeleteTBUSER_GROUPAsync(t, UserClass.userData.FSUSER_ID); 




            }
        }

        private void btnGroupAdd_Click(object sender, RoutedEventArgs e)
        {
          
                
                //RadTreeViewItem treeNode = (RadTreeViewItem)TreeViewGroupList.SelectedItem;
                //RadTreeViewItem newNode = new RadTreeViewItem();
                //newNode.Header = "New Group";

              
                //newNode.Tag = null;

                //GroupNode.Items.Add(newNode);

                //treeNode.IsExpanded = true;
                //newNode.IsSelected = true;
                ////TreeNodeSelected(newNode); 
                WSGroupAdmin.WSGroupAdminSoapClient groupObj = new WSGroupAdmin.WSGroupAdminSoapClient();

                groupObj.fnInsertTBGROUPSCompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (!args.Result)
                        {
                            MessageBox.Show("請修改New Group 為新群組名稱!", "錯誤", MessageBoxButton.OK);

                        }
                        else
                        {
                            preLoadGroupList(-1, null, true);
                        }
                    }
                };

                WSGroupAdmin.TBGroupStruct t = new WSGroupAdmin.TBGroupStruct() ; 

                t.FSGROUP_ID = "New Group" ;
                t.FSGROUP = "New Group"; 
                t.FSDESCRIPTION = "" ; 
                t.FSCREATED_BY = UserClass.userData.FSUSER_ID; 
                t.FSUPDATED_BY = UserClass.userData.FSUSER_ID ;

                groupObj.fnInsertTBGROUPSAsync(t, UserClass.userData.FSUSER_ID); 


         
        }

        private void btnGroupDel_Click(object sender, RoutedEventArgs e)
        {
            if (TreeViewGroupList.SelectedItem != null)
            {
                XElement xGroupObj = (XElement)((Telerik.Windows.Controls.RadTreeViewItem)TreeViewGroupList.SelectedItem).Tag;
                WSGroupAdmin.TBGroupStruct t = new WSGroupAdmin.TBGroupStruct(); 
                t.FSGROUP_ID = xGroupObj.Element("FSGROUP_ID").Value.ToString() ; 
               
                WSGroupAdmin.WSGroupAdminSoapClient groupObj = new WSGroupAdmin.WSGroupAdminSoapClient();
                groupObj.fnDeleteTBGROUPSCompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (!args.Result)
                        {
                            MessageBox.Show("請先移除此群組使用者", "提示", MessageBoxButton.OK);

                        }
                        else
                        {
                            preLoadGroupList(-1, null, true);
                        }
                    }
                    
                };
                groupObj.fnDeleteTBGROUPSAsync(t, UserClass.userData.FSUSER_ID); 
            }    
        }

        //儲存群組設定
        private void btnGroupSave_Click(object sender, RoutedEventArgs e)
        {

            XElement xGroupObj = (XElement)((Telerik.Windows.Controls.RadTreeViewItem)TreeViewGroupList.SelectedItem).Tag ; 
            WSGroupAdmin.TBGroupStruct GroupVar = new WSGroupAdmin.TBGroupStruct();

            GroupVar.FSGROUP_ID = this.tbxFSGROUP_ID.Text.Trim();
            GroupVar.FSGROUP = this.tbxFSGROUP.Text.Trim();
            GroupVar.FSDESCRIPTION = this.tbxFSDESCRIPTION.Text.Trim();
            GroupVar.oFSGROUP_ID = xGroupObj.Element("FSGROUP_ID").Value.ToString();
            GroupVar.FSUPDATED_BY = UserClass.userData.FSUSER_ID;

            WSGroupAdmin.WSGroupAdminSoapClient GroupObj = new WSGroupAdmin.WSGroupAdminSoapClient();

            GroupObj.fnUpdateTBGROUPSCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result == true)
                    {

                        preLoadGroupList(-1, null, true);
                        MessageBox.Show("更新資料完成", "資訊", MessageBoxButton.OK);
                    }
                    else
                    {
                        MessageBox.Show("更新資料時發生錯誤!", "錯誤", MessageBoxButton.OK);
                    }
                }
            };
            GroupObj.fnUpdateTBGROUPSAsync(GroupVar, UserClass.userData.FSUSER_ID); 
        }


        #region "修改資料"

        

        private void tbxFSGROUP_ID_TextChanged(object sender, TextChangedEventArgs e)
        {
            
            XElement GroupObj = (XElement)((Telerik.Windows.Controls.RadTreeViewItem)TreeViewGroupList.SelectedItem).Tag;


            if (GroupObj != null)
            {
                btnSave.IsEnabled = true;
            }
            else
                btnSave.IsEnabled = false; 

            
        }

        private void tbxFSGROUP_TextChanged(object sender, TextChangedEventArgs e)
        {
            
            
            XElement GroupObj = (XElement)((Telerik.Windows.Controls.RadTreeViewItem)TreeViewGroupList.SelectedItem).Tag;


            if (GroupObj != null)
            {
                btnSave.IsEnabled = true;
            }
            else
                btnSave.IsEnabled = false; 

            
        }

        private void tbxFSDESCRIPTION_TextChanged(object sender, TextChangedEventArgs e)
        {
            
            XElement GroupObj = (XElement)((Telerik.Windows.Controls.RadTreeViewItem)TreeViewGroupList.SelectedItem).Tag;


            if (GroupObj != null)
            {
                btnSave.IsEnabled = true;
            }
            else
                btnSave.IsEnabled = false; 
            

        }
        #endregion


        //自動去比對資料庫，若有人員就顯示資料
        private void acbMenID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (acbMenID.SelectedItem == null)
                tbxNote.Text = "";
            else
                tbxNote.Text = ((UserStruct)(acbMenID.SelectedItem)).FSTITLE_NAME.Trim();

            if (tbxNote.Text != "")
            {
                this.btnAdd.IsEnabled = true;
            }
            else
            {
                this.btnAdd.IsEnabled = false;
            }
        }

        //實作-查詢所有使用者資料
        void client_QUERY_TBUSERS_ALLCompleted(object sender, QUERY_TBUSERS_ALLCompletedEventArgs e)
        {

            if (e.Error == null)
            {
                ListUserAll = e.Result;
                acbMenID.ItemsSource = ListUserAll; //繫結AutoComplete按鈕
            }
            else
                MessageBox.Show("查詢使用者資料異常！", "提示訊息", MessageBoxButton.OK);
        }


    }
}
