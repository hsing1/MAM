﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.MAG
{
    public partial class MAG168_01 : ChildWindow
    {
        String m_Action;
        public PTS_MAM3.WSLogTemplateField.LogTemplateField_CODESET clCODESET = new PTS_MAM3.WSLogTemplateField.LogTemplateField_CODESET();                  //宣告此類別的一個公用成員 
        WSLogTemplateField.WSLogTemplateFieldSoapClient clntLTF = new WSLogTemplateField.WSLogTemplateFieldSoapClient();

        public String _ID = "", _TITLE = "", _NOTE = "",_ISENABLED =""; 

        public MAG168_01(String m_Action)
        {
            this.m_Action = m_Action;
            InitializeComponent();
            InitializeForm();
        }

        public MAG168_01(String m_Action, PTS_MAM3.WSLogTemplateField.LogTemplateField_CODESET clCODESET)
        {
            this.clCODESET = clCODESET;

            this.m_Action = m_Action;
            InitializeComponent();
            InitializeForm();
        }

        public MAG168_01(String m_Action, String m_ID, String m_TITLE, String m_NOTE, String m_ISENABLED)
        {
            this._ID = m_ID;
            this._TITLE = m_TITLE;
            this._NOTE = m_NOTE;
            this._ISENABLED = m_ISENABLED;


            this.m_Action = m_Action;
            InitializeComponent();
            InitializeForm();
        }

        void InitializeForm() //初始化本頁面
        {
            clntLTF.InsertCodeSetCompleted += new EventHandler<WSLogTemplateField.InsertCodeSetCompletedEventArgs>(clntLTF_InsertCodeSetCompleted);
            clntLTF.UpdateCodeSetCompleted += new EventHandler<WSLogTemplateField.UpdateCodeSetCompletedEventArgs>(clntLTF_UpdateCodeSetCompleted);
            switch (m_Action)
            {
                case "ADD":
                    this.Title = "新增代碼設定";
                    chkISENABLED.IsChecked = true; 

                    break;
                case "MOD":
                    this.Title = "修改代碼設定";
                    txtID.IsEnabled = false;

                    txtID.Text = clCODESET.FSCODE_ID;
                    chkISENABLED.IsChecked = clCODESET.FSCODE_ISENABLED;
                    txtTITLE.Text = clCODESET.FSCODE_TITLE;
                    txtNOTE.Text = clCODESET.FSCODE_NOTE;

                    break;
                case "QRY":
                    this.Title = "查詢代碼設定";
                    OKButton.Content = "查詢";
                    cbxISENABLED.Visibility = Visibility.Visible;
                    chkISENABLED.Visibility = Visibility.Collapsed;

                    txtID.Text = _ID;
                    txtTITLE.Text = _TITLE;
                    txtNOTE.Text = _NOTE;

                    switch (_ISENABLED)
                    {
                        case "01":
                            cbxISENABLED.SelectedIndex = 0;
                            break;
                        case "1":
                            cbxISENABLED.SelectedIndex = 1;
                            break;
                        case "2":
                            cbxISENABLED.SelectedIndex = 2;
                            break;
                    } 
                    break;
            }
        }


#region"觸發事件"

        void clntLTF_InsertCodeSetCompleted(object sender, WSLogTemplateField.InsertCodeSetCompletedEventArgs e)
        {
            //如果取回的資料沒有錯誤訊息
            if (e.Error == null)
            {
                String msg = e.Result;
                if (msg  == "")
                {
                    //如果新增成功
                    MessageBox.Show("新增成功", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = true;
                }
                else
                {
                    //如果新增失敗
                    MessageBox.Show("新增時發生錯誤: " + msg.Substring(6), "提示訊息", MessageBoxButton.OK);
                    //this.DialogResult = false;
                }
            }
            else
            {
                //如果取回的資料有錯誤訊息
                MessageBox.Show("呼叫新增函數時發生錯誤: " + e.Error.ToString(), "提示訊息", MessageBoxButton.OK);
                //this.DialogResult = false;
            }
        }

        void clntLTF_UpdateCodeSetCompleted(object sender, WSLogTemplateField.UpdateCodeSetCompletedEventArgs e)
        {
            //如果取回的資料沒有錯誤訊息
            if (e.Error == null)
            {
                String msg = e.Result;
                if (msg == "")
                {
                    //如果新增成功
                    MessageBox.Show("修改成功", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = true;
                }
                else
                {
                    //如果新增失敗
                    MessageBox.Show("修改時發生錯誤: " + msg.Substring(6), "提示訊息", MessageBoxButton.OK);
                    //this.DialogResult = false;
                }
            }
            else
            {
                //如果取回的資料有錯誤訊息
                MessageBox.Show("呼叫修改函數時發生錯誤: " + e.Error.ToString(), "提示訊息", MessageBoxButton.OK);
                //this.DialogResult = false;
            }
        }

#endregion


#region"按鈕動作"

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            switch (m_Action)
            {
                case "ADD":
                    if (txtID.Text.ToUpper().Trim() == "")
                    {
                        MessageBox.Show("代碼編號不可為空", "提示訊息", MessageBoxButton.OK);
                        return;
                    }
                    else if (txtTITLE.Text.ToUpper().Trim() == "")
                    {
                        MessageBox.Show("代碼名稱不可為空", "提示訊息", MessageBoxButton.OK);
                        return;
                    }

                    clCODESET.FSCODE_ID=txtID.Text.ToUpper().Trim();

                    if (chkISENABLED.IsChecked == true)
                    { clCODESET.FSCODE_ISENABLED = true; }
                    else
                    { clCODESET.FSCODE_ISENABLED = false; }

                    clCODESET.FSCODE_TITLE = txtTITLE.Text.Trim();
                    clCODESET.FSCODE_NOTE = txtNOTE.Text.Trim();

                    clCODESET.FSCREATED_BY = UserClass.userData.FSUSER_ID;;

                    clntLTF.InsertCodeSetAsync(clCODESET);
                     
                    break;
                case "MOD":
                    if (txtID.Text.ToUpper().Trim() == "")
                    {
                        MessageBox.Show("代碼編號不可為空", "提示訊息", MessageBoxButton.OK);
                        return;
                    }
                    else if (txtTITLE.Text.ToUpper().Trim() == "")
                    {
                        MessageBox.Show("代碼名稱不可為空", "提示訊息", MessageBoxButton.OK);
                        return;
                    }

                    clCODESET.FSCODE_ID=txtID.Text.ToUpper().Trim();

                    if (chkISENABLED.IsChecked == true)
                    { clCODESET.FSCODE_ISENABLED = true; }
                    else
                    { clCODESET.FSCODE_ISENABLED = false; }

                    clCODESET.FSCODE_TITLE = txtTITLE.Text.Trim();
                    clCODESET.FSCODE_NOTE = txtNOTE.Text.Trim();

                    clCODESET.FSUPDATED_BY = UserClass.userData.FSUSER_ID;;

                    clntLTF.UpdateCodeSetAsync(clCODESET);

                    break;
                case "QRY":
                    _ID = txtID.Text.Trim();
                    _TITLE = txtTITLE.Text.Trim();
                    _NOTE = txtNOTE.Text.Trim();
                    _ISENABLED = ((ComboBoxItem)cbxISENABLED.SelectedItem).Tag.ToString();
                    this.DialogResult = true;
                    break;
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

#endregion

    }
}

