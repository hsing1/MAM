﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.SystemAdmin
{
    public partial class MAG200_04 : ChildWindow
    {        
        WSUserAdmin.WSUserAdminSoapClient myService = new WSUserAdmin.WSUserAdminSoapClient();
        WSUserAdmin.UserStruct upduser = new WSUserAdmin.UserStruct();
        List<WSUserAdmin.Class_Dept> DeptList = new List<WSUserAdmin.Class_Dept>();
        Random myRnd = new Random();
        bool accountChecked = false;//表示是否檢查過帳號 by Jarvis20130627
        public MAG200_04(int iniTreeViewID)
        {
            InitializeComponent();
            //upduser = iniuser;
            
            
            tbxFSUSER_ID.Text = "PTSMAM" + DateTime.Now.Year + myRnd.Next(1000, 9999);

            this.tbxFSUSER_Account.LostFocus += new RoutedEventHandler(tbxFSUSER_Account_LostFocus);
            myService.GetUserAccountIsExistCompleted += new EventHandler<WSUserAdmin.GetUserAccountIsExistCompletedEventArgs>(myService_GetUserAccountIsExistCompleted);
            myService.GetUserIdIsExistAsync(tbxFSUSER_ID.Text);
            myService.GetUserIdIsExistCompleted+=(s1,args1)=>
                {
                    if (args1.Result != 0)
                    {
                        tbxFSUSER_ID.Text = "PTSMAM" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + myRnd.Next(100);
                        myService.GetUserIdIsExistAsync(tbxFSUSER_ID.Text);
                    }
                    else
                        label1.Content = "*";
                };
            myService.GetDeptListAllCompleted += (s, args) =>
                { 
                    DeptList = args.Result.ToList();
                    foreach (var item in DeptList)
                    {
                        ComboBoxItem cbi = new ComboBoxItem();
                        cbi.Content = item.FSFullName_ChtName;
                        cbi.Tag = item;
                        comboBox1.Items.Add(cbi);
                    }
                    foreach (var items in comboBox1.Items)
                    {
                        if (((WSUserAdmin.Class_Dept)(((ComboBoxItem)items).Tag)).FNDEP_ID == iniTreeViewID)
                        {
                            comboBox1.SelectedItem = items;
                        }
                    }
                    //prLoadUser(upduser);
                };
            myService.GetDeptListAllAsync();
        }
        
        void myService_GetUserAccountIsExistCompleted(object sender, WSUserAdmin.GetUserAccountIsExistCompletedEventArgs e)
        {
            if (e.Result)
            {//回傳true表示有重複的帳號
                accountChecked = false;
            }
            else
            {
                accountChecked = true;
                //showShortMessageBox("請注意","此帳號已有人使用，請重新輸入!");
            }
        }

        void tbxFSUSER_Account_LostFocus(object sender, RoutedEventArgs e)
        {
            if (this.tbxFSUSER_Account.Text != "") 
            {
                myService.GetUserAccountIsExistAsync(this.tbxFSUSER_Account.Text);
            
            }
        }


        private void showShortMessageBox(string title, Object content) 
        {
            ChildWindow cw = new ChildWindow();
            cw.Title = title ;


            cw.Content = content;
            cw.Show();
            
            cw.MouseLeftButtonDown += (s, args) =>
            {
                cw.Close();
            };
            cw.KeyDown += (s, args) =>
            {
                cw.Close();
            };
            return;
        }

        


        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if ((tbxFSUSER_ID.Text.Trim() == string.Empty) || (tbxFSUSER.Text.Trim() == string.Empty) ||
                (this.tbxFSUSER_TITLE.Text.Trim() == string.Empty) || this.tbxFSPASSWD.Password.Trim() == string.Empty || //(this.tbxFSDEPT.Text.Trim() == string.Empty) ||
                (this.tbxFSEMAIL.Text.Trim() == string.Empty) || (this.tbxFSUSER_Account.Text.Trim() == string.Empty)
                ) 
            {              
                showShortMessageBox("請注意",  "請輸入基本必填欄位");
                return;
            }
            if (label1.Content.ToString() != "*")
            {
                MessageBox.Show("取號有問題");
                return;
            }
            if (tbxFSPASSWD.Password != tbxFSPASSWD_Confirm.Password)
            {              
                showShortMessageBox("請注意", "密碼與確認密碼相異，請重新輸入!");
                return;
            }

            if (accountChecked)//檢查帳號是否驗證過 by Jarvis20130627
            {
                upduser.FSUSER = this.tbxFSUSER_Account.Text.Trim();
            }
            else 
            {
                showShortMessageBox("請注意", "此帳號已有人使用，請重新輸入!");
                return;

            }
            upduser.FSUSER_ID = tbxFSUSER_ID.Text.Trim();
           // user.FCACTIVE = "Y";
            if (cbxFCActive.SelectedIndex == 0)//(((ComboBoxItem)cbxFCActive.SelectedItem).Content == "Y")
            {
                upduser.FCACTIVE = "1";
                upduser.FCSECRET = "1";
            }
            else if (cbxFCActive.SelectedIndex == 1)
            {
                upduser.FCACTIVE = "0";
                upduser.FCSECRET = "0";
            }
            upduser.FSUSER_TITLE = tbxFSUSER_TITLE.Text;
            upduser.FSUSER_ChtName = tbxFSUSER.Text.Trim();
            upduser.FNTreeViewDept_ID = ((WSUserAdmin.Class_Dept)(((ComboBoxItem)(comboBox1.SelectedItem)).Tag)).FNDEP_ID;
            upduser.FNTreeViewDept_ChtName = ((ComboBoxItem)(comboBox1.SelectedItem)).Content.ToString().Trim();
            if ((this.tbxFSPASSWD.Password == this.tbxFSPASSWD_Confirm.Password) && (this.tbxFSPASSWD.Password.Trim() != string.Empty))
            {
                upduser.FSPASSWD = this.tbxFSPASSWD.Password.Trim();
            }
            //upduser.FDUPDATED_DATE = DateTime.Now;
            upduser.FSEMAIL = this.tbxFSEMAIL.Text.Trim();
            upduser.FSDESCRIPTION = this.tbxFSDESCRIPTION.Text.Trim();
            WSUserAdmin.WSUserAdminSoapClient userObj = new WSUserAdmin.WSUserAdminSoapClient();
            userObj.InsUserMAMAsync(upduser,UserClass.userData.FSUSER_ID);
            userObj.InsUserMAMCompleted += (S, args) =>
            {

                if (args.Result)
                {
                    this.DialogResult = true;
                }
                else
                { MessageBox.Show("新增MAM內部使用者有問題!"); }
            };
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void tbxUserData_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.OKButton.IsEnabled = true;
        }
    }
}

