﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.MAG
{
    public partial class MAG620 : UserControl
    {
        const int MAX_TS3310_IO_COUNT = 18;                     // 磁帶櫃的I/O最大數量
        const int COLUMN_INDEX_NAME = 2;                        // DataGrid 上面，磁帶名稱的欄位位置
        const int COLUMN_INDEX_STATUS = 3;                      // DataGrid 上面，磁帶使用狀態的欄位位置

        List<string> selectedTapeList = new List<string>();     // 選取的磁帶項目
        DateTime lastExecuteTime;                               // 最後一次執行上下架的時間

        /// <summary>
        /// 
        /// </summary>
        public MAG620()
        {
            // 
            InitializeComponent();
            // 
            lastExecuteTime = DateTime.MinValue;
            // 
            radBusyIndicator1.IsBusy = true;
            //
            fnQueryTs3310TapeDetails();
        }

        // 查詢 3310 的磁帶清單(並綁定到介面)
        void fnQueryTs3310TapeDetails()
        {
            var srTSM = new SR_WSTSM.ServiceTSMSoapClient();
            srTSM.GetTapesDetail_TSM3310Completed += new EventHandler<SR_WSTSM.GetTapesDetail_TSM3310CompletedEventArgs>(srTSM_GetTapesDetail_TSM3310Completed);
            srTSM.GetTapesDetail_TSM3310Async();
        }

        // 查詢 3310 的I/O數量
        void fnQueryTs3310IOCount()
        {
            var srTSM = new SR_WSTSM.ServiceTSMSoapClient();
            srTSM.GetTs3310IOCountCompleted += new EventHandler<SR_WSTSM.GetTs3310IOCountCompletedEventArgs>(srTSM_GetTs3310IOCountCompleted);
            srTSM.GetTs3310IOCountAsync();
        }

        #region ASYNC_COMPLETE
        // 查詢磁帶詳細內容
        void srTSM_GetTapesDetail_TSM3310Completed(object sender, SR_WSTSM.GetTapesDetail_TSM3310CompletedEventArgs e)
        {
            // 清除原有的資料
            DataGrid_TapeList.DataContext = null;

            // 檢查回傳是否有錯誤
            if (e.Error == null)
            {
                if (e.Result.Count == 0)
                    MessageBox.Show("磁帶櫃中目前沒有磁帶。");
                else
                    DataGrid_TapeList.DataContext = e.Result;
            }
            else
                MessageBox.Show("與主機端的連線發生錯誤");

            // 
            radBusyIndicator1.IsBusy = false;
        }

        // 磁帶上架
        void srTSM_CheckinTapes_TSM3310Completed(object sender, SR_WSTSM.CheckinTapes_TSM3310CompletedEventArgs e)
        {
            // 檢查回傳是否有錯誤
            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    MessageBox.Show("開始執行磁帶上架作業。");
                    lastExecuteTime = DateTime.Now;
                }
                else
                    MessageBox.Show("執行磁帶上架作業時發生錯誤，請稍後再試。");
            }
            else
                MessageBox.Show("與主機端的連線發生錯誤");

            //
            radBusyIndicator1.IsBusy = false;
        }

        // 磁帶下架
        void srTSM_CheckoutTapes_TSM3310Completed(object sender, SR_WSTSM.CheckoutTapes_TSM3310CompletedEventArgs e)
        {
            // 檢查回傳是否有錯誤
            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    MessageBox.Show("開始執行磁帶下架作業，請於五分鐘後至磁帶櫃取出下架磁帶。");
                    lastExecuteTime = DateTime.Now;
                }
                else
                    MessageBox.Show("執行磁帶下架作業時發生錯誤，請稍後再試。");
            }
            else
                MessageBox.Show("與主機端的連線發生錯誤");

            //
            radBusyIndicator1.IsBusy = false;
        }

        // 計算 I/O Port 數 + 發送下架命令
        void srTSM_GetTs3310IOCountCompleted(object sender, SR_WSTSM.GetTs3310IOCountCompletedEventArgs e)
        {
            // 檢查回傳是否有錯誤
            if (e.Error == null)
            {
                // 
                if (selectedTapeList.Count + e.Result > MAX_TS3310_IO_COUNT)
                {
                    MessageBox.Show(string.Format("目前I/O裡面有{0}捲磁帶，只能選取{1}捲磁帶", e.Result, MAX_TS3310_IO_COUNT - e.Result));
                    radBusyIndicator1.IsBusy = false;
                    return;
                }

                // 
                var inputArr = new SR_WSTSM.ArrayOfString();
                inputArr.AddRange(selectedTapeList);

                // 送出命令
                var srTSM = new SR_WSTSM.ServiceTSMSoapClient();
                srTSM.CheckoutTapes_TSM3310Completed += new EventHandler<SR_WSTSM.CheckoutTapes_TSM3310CompletedEventArgs>(srTSM_CheckoutTapes_TSM3310Completed);
                srTSM.CheckoutTapes_TSM3310Async(inputArr, UserClass.userData.FSUSER_ID.ToString());

                // 因為還有下一個指令，所以 radBusyIndicator1 不能放開
            }
            else
            {
                MessageBox.Show("與主機端的連線發生錯誤");
                radBusyIndicator1.IsBusy = false;
            }
        }
        #endregion

        #region GUI_Events
        // 功能「磁帶上架」
        void Button_TapeUp_Click(object sender, RoutedEventArgs e)
        {
            var t = (long)((TimeSpan)(DateTime.Now - lastExecuteTime)).TotalSeconds;

            // 檢查目前是不是冷卻中
            if (t < 60)
            {
                MessageBox.Show(string.Format("磁帶上下架指令操作過度頻繁，請於{0}秒後再試。", 60 - t));
                return;
            }

            // 
            radBusyIndicator1.IsBusy = true;

            // 直接送出查詢給WS
            var srTSM = new SR_WSTSM.ServiceTSMSoapClient();
            srTSM.CheckinTapes_TSM3310Completed += new EventHandler<SR_WSTSM.CheckinTapes_TSM3310CompletedEventArgs>(srTSM_CheckinTapes_TSM3310Completed);
            srTSM.CheckinTapes_TSM3310Async(UserClass.userData.FSUSER_ID.ToString());
        }

        // 功能「磁帶下架」
        void Button_TapeDown_Click(object sender, RoutedEventArgs e)
        {
            // 
            if (selectedTapeList.Count == 0)
            {
                MessageBox.Show("沒有選取欲下架的磁帶");
                return;
            }

            // 檢查目前是不是冷卻中
            var t = (long)((TimeSpan)(DateTime.Now - lastExecuteTime)).TotalSeconds;
            if (t < 60)
            {
                MessageBox.Show(string.Format("磁帶上下架指令操作過度頻繁，請於{0}秒後再試。", 60 - t));
                return;
            }

            // 
            radBusyIndicator1.IsBusy = true;

            // 先查詢 I/O 數量
            fnQueryTs3310IOCount();
        }

        // 功能「重新整理」
        void Button_Refresh_Click(object sender, RoutedEventArgs e)
        {
            //
            radBusyIndicator1.IsBusy = true;

            // 清除原有的資料
            selectedTapeList.Clear();
            DataGrid_TapeList.DataContext = null;

            // 查詢
            fnQueryTs3310TapeDetails();
        }

        // 開發測試用
        void Button_Debug_Click(object sender, RoutedEventArgs e)
        {
            var sb = new System.Text.StringBuilder();
            foreach (var s in selectedTapeList)
                sb.Append(s + "、");
            MessageBox.Show(sb.ToString());
        }

        // 
        void DataGrid_TapeList_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            // 在讀入新的資料行時，都要檢查自己的屬性
            string tapeName = ((TextBlock)DataGrid_TapeList.Columns[COLUMN_INDEX_NAME].GetCellContent(e.Row)).Text;
            if (selectedTapeList.Contains(tapeName))
                ((Button)DataGrid_TapeList.Columns[0].GetCellContent(e.Row)).Content = "已選取";
            else
                ((Button)DataGrid_TapeList.Columns[0].GetCellContent(e.Row)).Content = string.Empty;
        }

        // 
        void selectCheckout_Click(object sender, RoutedEventArgs e)
        {
            Button objBtn = sender as Button;

            // 當 ... 時才需要檢查
            if (string.IsNullOrEmpty(objBtn.Content.ToString()))
            {
                if (selectedTapeList.Count >= MAX_TS3310_IO_COUNT)
                {
                    MessageBox.Show(string.Format("一次只能選擇{0}捲磁帶", MAX_TS3310_IO_COUNT));
                    return;
                }
            }

            // 找出目前的 DataGrid 元件
            DataGridRow currentDataGridRow = DataGridRow.GetRowContainingElement(objBtn);

            // 磁帶資訊
            var tapeName = ((TextBlock)DataGrid_TapeList.Columns[COLUMN_INDEX_NAME].GetCellContent(currentDataGridRow)).Text;
            var tapeStatus = ((TextBlock)DataGrid_TapeList.Columns[COLUMN_INDEX_STATUS].GetCellContent(currentDataGridRow)).Text;

            // 雖然有做好 UI 的刷新動作，這邊還是盡量以記憶體來判讀
            if (selectedTapeList.Contains(tapeName))
            {
                selectedTapeList.Remove(tapeName);

                objBtn.Content = string.Empty;    
            }
            else
            {
                selectedTapeList.Add(tapeName);

                objBtn.Content = "已選取";

                // 如果狀態非為 FULL 的，就做警告提示
                if (!tapeStatus.Equals("FULL"))
                    MessageBox.Show(string.Format("請注意：所選取欲下架的磁帶({0})非為FULL狀態", tapeName));
            }
        }
        #endregion
    }
}





//private bool executeCheckinProcess;

//private System.Windows.Threading.DispatcherTimer sysTimer = new System.Windows.Threading.DispatcherTimer();




//private void btnCreateCheckinList_Click(object sender, RoutedEventArgs e)
//{
//    executeCheckinProcess = true;
//    fnChangeProcessButton();

//    DataGrid_TapeList.DataContext = null;

//    fnDisableButtons();

//    //SR_WSTSM.ServiceTSMSoapClient objTSM = new SR_WSTSM.ServiceTSMSoapClient();
//    //objTSM.GetTSM3310RecommandCheckinListCompleted += new EventHandler<SR_WSTSM.GetTSM3310RecommandCheckinListCompletedEventArgs>(objTSM_GetTSM3310RecommandCheckinListCompleted);
//    //objTSM.GetTSM3310RecommandCheckinListAsync();
//}

//private void btnCreateCheckoutList_Click(object sender, RoutedEventArgs e)
//{
//    // 先確認使用者是否真的想要產生磁帶下架清單
//    MessageBoxResult result = MessageBox.Show("產生清單的時間約需3至5分鐘，完成後會有訊息提示，確定是否開始執行？", "提示訊息", MessageBoxButton.OKCancel);

//    if (result != MessageBoxResult.OK)
//        return;

//    // 開始執行整個程序

//    executeCheckinProcess = false;
//    fnChangeProcessButton();

//    DataGrid_TapeList.DataContext = null;

//    fnDisableButtons();

//    //SR_WSTSM.ServiceTSMSoapClient objTSM = new SR_WSTSM.ServiceTSMSoapClient();
//    //objTSM.CreateTSM3310RecommandCheckoutListCompleted += new EventHandler<SR_WSTSM.CreateTSM3310RecommandCheckoutListCompletedEventArgs>(objTSM_CreateTSM3310RecommandCheckoutListCompleted);
//    //objTSM.CreateTSM3310RecommandCheckoutListAsync();
//}

//private void btnProcess_Click(object sender, RoutedEventArgs e)
//{
//    fnDisableButtons();

//    //SR_WSTSM.ServiceTSMSoapClient objTSM = new SR_WSTSM.ServiceTSMSoapClient();
//    //if (executeCheckinProcess)
//    //{
//    //    objTSM.CheckinTapes_TSM3310Completed += new EventHandler<SR_WSTSM.CheckinTapes_TSM3310CompletedEventArgs>(objTSM_CheckinTapes_TSM3310Completed);
//    //    objTSM.CheckinTapes_TSM3310Async();
//    //}
//    //else
//    //{
//    //    objTSM.CheckoutTapes_TSM3310Completed += new EventHandler<SR_WSTSM.CheckoutTapes_TSM3310CompletedEventArgs>(objTSM_CheckoutTapes_TSM3310Completed);
//    //    objTSM.CheckoutTapes_TSM3310Async();
//    //}
//}

//private void sysTimerPolling(object sender, EventArgs e)
//{
//    sysTimer.Stop();

//    //SR_WSTSM.ServiceTSMSoapClient objTSM = new SR_WSTSM.ServiceTSMSoapClient();
//    //objTSM.GetTSM3310RecommandCheckoutListCompleted += new EventHandler<SR_WSTSM.GetTSM3310RecommandCheckoutListCompletedEventArgs>(objTSM_GetTSM3310RecommandCheckoutListCompleted);
//    //objTSM.GetTSM3310RecommandCheckoutListAsync();
//}

//private void fnDisableButtons()
//{
//    btnCreateCheckinList.IsEnabled = false;
//    btnCreateCheckoutList.IsEnabled = false;
//    btnProcess.IsEnabled = false;
//}

//private void fnEnableButtons()
//{
//    btnCreateCheckinList.IsEnabled = true;
//    btnCreateCheckoutList.IsEnabled = true;
//    btnProcess.IsEnabled = true;
//}

//private void fnChangeProcessButton()
//{
//    if (executeCheckinProcess)
//        textBlockProcess.Text = "進行作業＜上架＞";
//    else
//        textBlockProcess.Text = "進行作業＜下架＞";
//}

//private void objTSM_GetTSM3310RecommandCheckinListCompleted(object sender, SR_WSTSM.GetTSM3310RecommandCheckinListCompletedEventArgs e)
//{
//    // 檢查回傳是否有錯誤
//    if (e.Error == null && e.Result != null)
//    {
//        if (e.Result.Count == 0)
//            MessageBox.Show("目前尚無建議上架清單，可直接放入空白磁帶進行上架動作。");
//        else
//            DataGrid_TapeList.DataContext = e.Result;
//    }
//    else
//        MessageBox.Show("網路連接發生錯誤，請稍後再試。");

//    // 將畫面復原
//    fnEnableButtons();
//}

//private void objTSM_CreateTSM3310RecommandCheckoutListCompleted(object sender, SR_WSTSM.CreateTSM3310RecommandCheckoutListCompletedEventArgs e)
//{
//    // 檢查回傳是否有錯誤
//    if (e.Error == null && e.Result == true)
//    {
//        // 打開系統 Timer，開始做掃瞄的動作
//        sysTimer.Start();
//    }
//    else
//    {
//        // 將畫面復原
//        fnEnableButtons();
//        // 提示使用者發生錯誤
//        MessageBox.Show("網路連接發生錯誤，請稍後再試。");
//    }
//}

//private void objTSM_GetTSM3310RecommandCheckoutListCompleted(object sender, SR_WSTSM.GetTSM3310RecommandCheckoutListCompletedEventArgs e)
//{
//    // 檢查回傳是否有錯誤
//    if (e.Error == null)
//    {
//        // 不曉得會不會有真的是 0 筆的情況 =_="
//        if (e.Result == null)
//        {
//            // 目前還沒有收集到清單
//            sysTimer.Start();
//        }
//        else if (e.Result.Count == 0)
//        {
//            // 復原畫面
//            fnEnableButtons();
//            // 提示使用者可以進行下一步
//            MessageBox.Show("目前尚無建議下架清單。");
//        }
//        else
//        {
//            // 呈現結果
//            DataGrid_TapeList.DataContext = e.Result;
//            // 復原畫面
//            fnEnableButtons();
//            // 提示使用者可以進行下一步
//            MessageBox.Show("建議下架清單已產生完畢，可按「執行作業」的按鈕進行磁帶下架的動作。");
//        }
//    }
//    else
//    {
//        // 將時器復原，跑下一輪
//        sysTimer.Start();
//        // 這邊應該要記錄錯了幾次，以免一直出錯
//    }
//}

//private void objTSM_CheckinTapes_TSM3310Completed(object sender, SR_WSTSM.CheckinTapes_TSM3310CompletedEventArgs e)
//{
//    // 檢查回傳是否有錯誤
//    if (e.Error == null && e.Result == true)
//        MessageBox.Show("已發出磁帶上架指令。");
//    else
//        MessageBox.Show("發出磁帶上架指令時發生錯誤，請稍後再試。");
//    // 
//    fnEnableButtons();
//}

//private void objTSM_CheckoutTapes_TSM3310Completed(object sender, SR_WSTSM.CheckoutTapes_TSM3310CompletedEventArgs e)
//{
//    // 檢查回傳是否有錯誤
//    if (e.Error == null && e.Result == true)
//        MessageBox.Show("已發出磁帶下架指令。");
//    else
//        MessageBox.Show("發出磁帶下架指令時發生錯誤，請稍後再試。");
//    // 
//    fnEnableButtons();
//}