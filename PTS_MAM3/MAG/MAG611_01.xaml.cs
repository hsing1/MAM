﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.MAG
{
    public partial class MAG611_01 : ChildWindow
    {
        SR_WSTSM.ClassTSMRecallJob m_FormData;

        // 從母畫面傳來的物件參數
        public MAG611_01(SR_WSTSM.ClassTSMRecallJob FormData)
        {
            InitializeComponent();
            // 將母畫面的資料寫到子畫面的區域變數中
            m_FormData = FormData;
            // 將物件的屬性綁到介面中
            fnBindClassAttributesToForm();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void btnRestartJob_Click(object sender, RoutedEventArgs e)
        {
            SR_WSTSM.ServiceTSMSoapClient objTSM = new SR_WSTSM.ServiceTSMSoapClient();
            objTSM.RetryRecallServiceCompleted += new EventHandler<SR_WSTSM.RetryRecallServiceCompletedEventArgs>(objTSM_RetryRecallServiceCompleted);
            objTSM.RetryRecallServiceAsync(Int64.Parse(m_FormData.JobID));
        }

        private void btnCopyPath_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Clipboard.SetText(m_FormData.ProcessFilePath);
                MessageBox.Show("資料已複製到剪貼簿。");
            }
            catch
            {
                MessageBox.Show("因安全性設定，無法將資料複製到剪貼簿中。");
            }
        }

        // 將物件的屬性綁到介面中
        private void fnBindClassAttributesToForm()
        {
            lbl11.Content = m_FormData.JobID;
            lbl12.Content = m_FormData.JobStatus;
            lbl13.Content = System.IO.Path.GetFileName(m_FormData.ProcessFilePath);
            lbl14.Content = m_FormData.RequestTime;
            lbl15.Content = m_FormData.JobResult;
        }

        private void objTSM_RetryRecallServiceCompleted(object sender, SR_WSTSM.RetryRecallServiceCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    MessageBox.Show("已發出重置作業命令");
                    this.DialogResult = true;       // 完成就自動關掉吧
                }
                else
                    MessageBox.Show("發送重置作業命令時發生錯誤");
            }
            else
                MessageBox.Show("連結主機服務時發生問題：" + e.Error.ToString());
        }
    }
}
