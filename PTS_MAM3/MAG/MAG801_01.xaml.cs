﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSTSM_GetFileInfo;
using System.Windows.Media.Imaging;

namespace PTS_MAM3.MAG
{
    public partial class MAG801_01 : ChildWindow
    {
        private ServiceTSMSoapClient _service_tsm_client = new ServiceTSMSoapClient();

        private PreviewPlayer.SLVideoPlayer player = new PreviewPlayer.SLVideoPlayer("", 0);

        private string _fsfile_type = ""; 

        public MAG801_01(string _fsfile_type , string _fsfile_no)
        {
            InitializeComponent();

            this._fsfile_type = _fsfile_type;

            _service_tsm_client.GetHttpPathByFileIDCompleted += new EventHandler<GetHttpPathByFileIDCompletedEventArgs>(_service_tsm_client_GetHttpPathByFileIDCompleted);
            if (_fsfile_type == "V")
            {
                _service_tsm_client.GetHttpPathByFileIDAsync(_fsfile_no, FileID_MediaType.LowVideo);
            }
            else if (_fsfile_type == "P")
            {
                _service_tsm_client.GetHttpPathByFileIDAsync(_fsfile_no, FileID_MediaType.Photo);
            }
        }

        void _service_tsm_client_GetHttpPathByFileIDCompleted(object sender, GetHttpPathByFileIDCompletedEventArgs e)
        {
            if (e.Error != null || string.IsNullOrEmpty(e.Result))
            {
                MessageBox.Show("取得影片/圖片路徑錯誤!", "訊息", MessageBoxButton.OK);
                return;
            }

            this.Grid_Player.Children.Clear();

            if (this._fsfile_type == "V")
            {
                player.HTML_SetURL(e.Result, 0);
                this.Grid_Player.Children.Add(player);
            }
            else if (this._fsfile_type == "P")
            {
                Image img = new Image();
                img.Source = new BitmapImage(new Uri(e.Result, UriKind.Absolute));
                this.Grid_Player.Children.Add(img);
            }
            
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

