﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;

namespace PTS_MAM3.MAG
{
    public partial class MAG611 : Page
    {
        public MAG611()
        {
            InitializeComponent();
            // 初始化元件
            dpStart.CurrentDateTimeText = DateTime.Now.ToString("yyyy/M/d");
            dpStart.DateTimeText = DateTime.Now.ToString("yyyy/M/d");
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //
        }

        // 按鈕 - 詳細資訊
        private void btnDetail_Click(object sender, RoutedEventArgs e)
        {
            // 判斷是否有選取DataGrid
            if (DGJobList.SelectedItem == null)
            {
                MessageBox.Show("請選擇要查詢的項目", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //// 開啟子視窗
            MAG611_01 MAG611_01_frm = new MAG611_01(((SR_WSTSM.ClassTSMRecallJob)DGJobList.SelectedItem));
            MAG611_01_frm.Show();
            MAG611_01_frm.Closing += (s, args) =>
            {
                if (MAG611_01_frm.DialogResult == true)
                {
                    // 重新讀取主畫面的資料
                    SR_WSTSM.ServiceTSMSoapClient objTSM = new SR_WSTSM.ServiceTSMSoapClient();
                    objTSM.GetTSMRecallJobInformationCompleted += new EventHandler<SR_WSTSM.GetTSMRecallJobInformationCompletedEventArgs>(objTSM_GetTSMRecallJobInformationCompleted);
                    objTSM.GetTSMRecallJobInformationAsync(dpStart.DateTimeText);
                }
            };
        }

        // 按鈕 - 查詢
        private void btnQuery_Click(object sender, RoutedEventArgs e)
        {
            // 檢查輸入的參數
            if (string.IsNullOrEmpty(dpStart.DateTimeText))
            {
                MessageBox.Show("選擇的時間參數有誤");
                return;
            }
            // 畫面處理
            fnDisableButtons();
            //// 呼叫後端 WebService 取得資訊
            SR_WSTSM.ServiceTSMSoapClient objTSM = new SR_WSTSM.ServiceTSMSoapClient();
            objTSM.GetTSMRecallJobInformationCompleted += new EventHandler<SR_WSTSM.GetTSMRecallJobInformationCompletedEventArgs>(objTSM_GetTSMRecallJobInformationCompleted);
            objTSM.GetTSMRecallJobInformationAsync(dpStart.DateTimeText);
        }

        private void fnDisableButtons()
        {
            btnQuery.IsEnabled = false;
            btnDetail.IsEnabled = false;
        }

        private void fnEnableButtons()
        {
            btnQuery.IsEnabled = true;
            btnDetail.IsEnabled = true;
        }

        private void objTSM_GetTSMRecallJobInformationCompleted(object sender, SR_WSTSM.GetTSMRecallJobInformationCompletedEventArgs e)
        {
            //
            fnEnableButtons();
            DGJobList.DataContext = null;
            // 
            if (e.Error == null)
            {
                if (e.Result.Count == 0)
                {
                    MessageBox.Show("查不到該範圍區間的資料");
                    return;
                }
                // 
                DGJobList.DataContext = e.Result;
            }
            else
                MessageBox.Show("連結主機服務時發生問題：" + e.Error.ToString());
        }
    }
}
