﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.SystemAdmin
{
    public partial class UserAdmin_Modify : ChildWindow
    {        
        WSUserAdmin.WSUserAdminSoapClient myService = new WSUserAdmin.WSUserAdminSoapClient();
        WSUserAdmin.UserStruct upduser = new WSUserAdmin.UserStruct();
        List<WSUserAdmin.Class_Dept> DeptList = new List<WSUserAdmin.Class_Dept>();
        
        bool accountChecked = false;//表示是否檢查過帳號 by Jarvis20130627
        bool isChanged = false;//用來標識帳號是否更動過

        public UserAdmin_Modify(WSUserAdmin.UserStruct iniuser)
        {
            InitializeComponent();
            radioButton1.IsChecked = true;
            upduser = iniuser;

            //by Jarvis20130627
            this.tbxFSUSER_Account.LostFocus += new RoutedEventHandler(tbxFSUSER_Account_LostFocus);
            this.tbxFSUSER_Account.TextChanged += new TextChangedEventHandler(tbxFSUSER_Account_TextChanged);
            myService.GetUserAccountIsExistCompleted += new EventHandler<WSUserAdmin.GetUserAccountIsExistCompletedEventArgs>(myService_GetUserAccountIsExistCompleted);

            myService.GetDeptListAllCompleted += (s, args) =>
                { 
                    DeptList = args.Result.ToList();
                    foreach (var item in DeptList)
                    {
                        ComboBoxItem cbi = new ComboBoxItem();
                        cbi.Content = item.FSFullName_ChtName;
                        cbi.Tag = item;
                        comboBox1.Items.Add(cbi);
                    }
                    prLoadUser(upduser);
                };
            myService.GetDeptListAllAsync(iniuser.FNDEP_ID);
        }

        void tbxFSUSER_Account_TextChanged(object sender, TextChangedEventArgs e)
        {
          
                if (this.tbxFSUSER_Account.Text == upduser.FSUSER)
                {
                    isChanged = false;
                }
                else
                {
                    isChanged = true;
                }
           
        }

        void myService_GetUserAccountIsExistCompleted(object sender, WSUserAdmin.GetUserAccountIsExistCompletedEventArgs e)
        {
            if (e.Result)
            {//回傳true表示有重複的帳號
                accountChecked = false;
            }
            else
            {
                accountChecked = true;
                //showShortMessageBox("請注意", "此帳號已有人使用，請重新輸入!");
            }
        }

        void tbxFSUSER_Account_LostFocus(object sender, RoutedEventArgs e)
        {
            if (isChanged)
            {
                if (this.tbxFSUSER_Account.Text != "")
                {
                    myService.GetUserAccountIsExistAsync(this.tbxFSUSER_Account.Text);
                }
            }
            else 
            {
                accountChecked = true;
            }
            
        }


        private void prLoadUser(WSUserAdmin.UserStruct user)
        {
            //this.tbxFSDEPT.Text = user.FSDEPT;
            //this.tbxFSDEPT.Text = user.FSTITLEOFDEPT_CHTNAME;

            foreach (var item in comboBox1.Items)
            {
                if (((WSUserAdmin.Class_Dept)(((ComboBoxItem)item).Tag)).FNDEP_ID == user.FNTreeViewDept_ID)
                {
                    comboBox1.SelectedItem = item;
                }
            }

            this.TBUSER_ID.Text = user.FSUSER_ID;//by Jarvis20130627
            this.tbxFSUSER_Account.Text = user.FSUSER;

            this.tbxFSDESCRIPTION.Text = user.FSDESCRIPTION;
            this.tbxFSEMAIL.Text = user.FSEMAIL;
            this.tbxFSPASSWD.Password = "";
            this.tbxFSPASSWD_Confirm.Password = "";
            this.tbxFSUSER_Account.Text = user.FSUSER;//by Jarvis20130627
            this.tbxFSUSER.Text = user.FSUSER_ChtName;
            this.tbxFSUSER_TITLE.Text = user.FSUSER_TITLE;
            if (user.FCACTIVE == "1")
            {
                this.cbxFCActive.SelectedIndex = 0; 
            }
            else
            {
                this.cbxFCActive.SelectedIndex = 1; 
            }

            
        }

        private void showShortMessageBox(string title, Object content) {
            ChildWindow cw = new ChildWindow();
            cw.Title = title ;


            cw.Content = content;
            cw.Show();
            
            cw.MouseLeftButtonDown += (s, args) =>
            {
                cw.Close();
            };
            cw.KeyDown += (s, args) =>
            {
                cw.Close();
            };
            return;
        }

        


        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if ((tbxFSUSER_Account.Text.Trim() == string.Empty) || (tbxFSUSER.Text.Trim() == string.Empty) ||                
                (this.tbxFSUSER_TITLE.Text.Trim() == string.Empty) || //(this.tbxFSDEPT.Text.Trim() == string.Empty) ||
                (this.tbxFSEMAIL.Text.Trim() == string.Empty)
                ) 
            {              
                showShortMessageBox("請注意",  "請輸入基本必填欄位");
                return;
            }


            if (tbxFSPASSWD.Password != tbxFSPASSWD_Confirm.Password)
            {              
                showShortMessageBox("請注意", "密碼與確認密碼相異，請重新輸入!");
                return;
            }

           // user.FCACTIVE = "Y";
            if (cbxFCActive.SelectedIndex == 0)//(((ComboBoxItem)cbxFCActive.SelectedItem).Content == "Y")
            {
                upduser.FCACTIVE = "1";
                upduser.FCSECRET = "1";
            }
            else if (cbxFCActive.SelectedIndex == 1)
            {
                upduser.FCACTIVE = "0";
                upduser.FCSECRET = "0";
            }
            upduser.FSUSER_TITLE = tbxFSUSER_TITLE.Text;
            upduser.FSUSER_ChtName = tbxFSUSER.Text.Trim();
            upduser.FNTreeViewDept_ID = ((WSUserAdmin.Class_Dept)(((ComboBoxItem)(comboBox1.SelectedItem)).Tag)).FNDEP_ID;
            upduser.FSTITLEOFDEPT_CHTNAME = ((WSUserAdmin.Class_Dept)(((ComboBoxItem)(comboBox1.SelectedItem)).Tag)).FSFullName_ChtName;
            if (radioButton1.IsChecked==true)
            {
                if ((this.tbxFSPASSWD.Password == this.tbxFSPASSWD_Confirm.Password) && (this.tbxFSPASSWD.Password.Trim() != string.Empty))
                {
                    upduser.FSPASSWD = this.tbxFSPASSWD.Password.Trim();
                }
                else if ((this.tbxFSPASSWD.Password == this.tbxFSPASSWD_Confirm.Password) && (this.tbxFSPASSWD.Password.Trim() == string.Empty))
                { upduser.FSPASSWD = ""; }
            }
            else if (radioButton2.IsChecked==true)
            {
                upduser.FSPASSWD = "\\\\";
            }

            //upduser.FDUPDATED_DATE = DateTime.Now;
            upduser.FSEMAIL = this.tbxFSEMAIL.Text.Trim();
            upduser.FSDESCRIPTION = this.tbxFSDESCRIPTION.Text.Trim();

            if (isChanged)//by Jarvis20130627
            {
                if (accountChecked)
                {
                    upduser.FSUSER = this.tbxFSUSER_Account.Text;
                }
                else
                {
                    showShortMessageBox("請注意", "此帳號已有人使用，請重新輸入!");
                    return;
                }
            }
            else 
            {
                upduser.FSUSER = this.tbxFSUSER_Account.Text;
            }
            

            WSUserAdmin.WSUserAdminSoapClient userObj = new WSUserAdmin.WSUserAdminSoapClient();
            userObj.UpdUserInfoAsync(upduser,UserClass.userData.FSUSER_ID);
            userObj.UpdUserInfoCompleted += (S, args) =>
            {

                if (args.Result)
                {
                    this.DialogResult = true;
                }
                else
                { MessageBox.Show("更新資料有問題!"); }
            };
            //this.DialogResult = true;
            //TODO:2012/02/10 做到這裡
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void tbxUserData_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.OKButton.IsEnabled = true;
        }

        private void radioButton2_Checked(object sender, RoutedEventArgs e)
        {
            tbxFSPASSWD.IsEnabled = false;
            tbxFSPASSWD_Confirm.IsEnabled = false;
            upduser.FSPASSWD = "\\\\";
        }

        private void radioButton1_Checked(object sender, RoutedEventArgs e)
        {
            tbxFSPASSWD.IsEnabled = true;
            tbxFSPASSWD_Confirm.IsEnabled = true;
            upduser.FSPASSWD = this.tbxFSPASSWD.Password.Trim();
        }
    }
}

