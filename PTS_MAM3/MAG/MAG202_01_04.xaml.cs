﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using System.Xml.Linq;
using System.IO;
using System.Collections.ObjectModel;

namespace PTS_MAM3.MAG
{
    public partial class MAG202_01_04 : ChildWindow
    {
        string _fstable = "";
        int _fntemplate_id;
        string _fsfield = "";

        String m_Action;
        WSDirectoryAdmin.WSDirectoryAdminSoapClient clntFS = new WSDirectoryAdmin.WSDirectoryAdminSoapClient();
        WSDirectoryAdmin.TBTemplate_FieldsStruct clFieldsStruct = new WSDirectoryAdmin.TBTemplate_FieldsStruct();
        WSLogTemplateField.WSLogTemplateFieldSoapClient clntLTF = new WSLogTemplateField.WSLogTemplateFieldSoapClient();

        public MAG202_01_04(String m_Action, string fstable, int fntemplate_id, string fsfield)
        {
            this.m_Action = m_Action;
            InitializeComponent();
            InitializeForm();

            _fsfield = fsfield;
            _fstable = fstable;
            _fntemplate_id = fntemplate_id;
            initialCbxFSFIELD( _fstable, _fntemplate_id, _fsfield);
        }

        private void initialCbxFSFIELD( string fstable, int fntemplate_id, string fsfield)
        {
            WSDirectoryAdmin.WSDirectoryAdminSoapClient dirObj = new WSDirectoryAdmin.WSDirectoryAdminSoapClient();
            dirObj.fnGetTBTEMPLATE_FIELDS_FSFIELDCompleted += (e, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        // StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        //var ltox = from s in doc.Elements("Datas").Elements("Data")
                        //    select new List<string>(){
                        //        (string)s.Element("FSFIELD")
                        //};

                        int attrCnt = 0;
                        if (fstable == "TBSUBJECT")
                            attrCnt = 50;
                        else
                            attrCnt = 50;

                        for (int i = 1; i <= attrCnt; i++)
                        {
                            cbxField.Items.Add("FSATTRIBUTE" + i);
                        }


                        var ltox = from s in doc.Elements("Datas").Elements("Data")
                                   select
                                   (string)s.Element("FSFIELD");

                        // this.cbxFSFIELD.ItemsSource = ltox;
                         
                        foreach (string s in ltox)
                        {
                            this.cbxField.Items.Remove(s);
                        }
                         
                        if (this.cbxField.Items.Count > 0)
                        {
                            this.cbxField.SelectedIndex = 0;
                        }


                    }
                }
            };
            dirObj.fnGetTBTEMPLATE_FIELDS_FSFIELDAsync(fstable, fntemplate_id, fsfield);
        }

        public MAG202_01_04(String m_Action, WSDirectoryAdmin.TBTemplate_FieldsStruct clFieldsStruct, string fstable, int fntemplate_id, string fsfield)
        {
            this.clFieldsStruct = clFieldsStruct;
            this.m_Action = m_Action;
            InitializeComponent();
            InitializeForm();
        }

        void InitializeForm() //初始化本頁面
        {
            clntLTF.GetCodeSetListCompleted += new EventHandler<WSLogTemplateField.GetCodeSetListCompletedEventArgs>(clntLTF_GetCodeSetListCompleted);
            cbxCode.SelectionChanged += new SelectionChangedEventHandler(cbxCode_SelectionChanged);
            
            switch (m_Action)
            {
                case "ADD":
                    this.Title += "新增";

                    lblTitle.Content = "";
                    rdb1.IsChecked = true;
                    chkNull.IsChecked = true;
                    txtCount.Value = "";
                    txtWidth.Value = "270";

                    txtCount.IsEnabled = false;
                    txtCount.Value = "1";

                    clntLTF.GetCodeSetListAsync("", "", "", "1");
                    //chkISENABLED.IsChecked = true;

                    break;
                case "MOD":
                    this.Title += "修改";

                    cbxField.Items.Add(clFieldsStruct.FSFIELD);
                    cbxField.SelectedIndex = 0;
                    cbxField.IsEnabled = false;
                    rdb1.IsEnabled = false;
                    rdb2.IsEnabled = false;
                      
                    //txtID.Text = clCODESET.FSCODE_ID;
                    //chkISENABLED.IsChecked = clCODESET.FSCODE_ISENABLED;
                    //txtTITLE.Text = clCODESET.FSCODE_TITLE;
                    //txtNOTE.Text = clCODESET.FSCODE_NOTE;
                    clntLTF.GetCodeSetListAsync("", "", "", "01");

                    break;
            }
        }




#region"觸發事件"

        void clntLTF_GetCodeSetListCompleted(object sender, WSLogTemplateField.GetCodeSetListCompletedEventArgs e)
        {
            //如果取回的資料沒有錯誤訊息
            if (e.Error == null)
            {
                //如果取回的資料筆數不為0
                if (e.Result.Count != 0)
                {
                    //把查詢結果丟入公用成員lstNews中, 方便母視窗取用
                    List<PTS_MAM3.WSLogTemplateField.LogTemplateField_CODESET> CodeSetList = new List<PTS_MAM3.WSLogTemplateField.LogTemplateField_CODESET>();

                    cbxCode.DisplayMemberPath = "FSCODE_TITLE";
                    cbxCode.SelectedValuePath = "FSCODE_ID";

                    CodeSetList.Add(new PTS_MAM3.WSLogTemplateField.LogTemplateField_CODESET
                                    {
                                        FSCODE_ID = "",
                                        FSCODE_TITLE = "(請選擇)",
                                        FSCODE_TBCOL = "",
                                        FSCODE_NOTE = "",
                                        FSCODE_ISENABLED = true,
                                        _COUNT = 0,
                                        _ISENABLED = "",
                                 
                                        FSCREATED_BY = "",
                                        FDCREATED_DATE = DateTime.Parse("1900/01/01"),
                                        FSUPDATED_BY ="",
                                        FDUPDATED_DATE = DateTime.Parse("1900/01/01")
                                    });

                   
                    switch (m_Action)
                    {
                        case "ADD":
                            foreach (PTS_MAM3.WSLogTemplateField.LogTemplateField_CODESET ltf in (List<PTS_MAM3.WSLogTemplateField.LogTemplateField_CODESET>)e.Result)
                            {
                                ltf.FSCODE_TITLE = ltf.FSCODE_ID + " - " + ltf.FSCODE_TITLE;
                                if (ltf.FSCODE_ISENABLED == true) //已設定為不允許使用的代碼, 在新增時無法選取
                                    CodeSetList.Add(ltf);
                            }

                            cbxCode.ItemsSource = CodeSetList;

                            cbxCode.SelectedIndex = 0;
                            break;
                        case "MOD":
                            foreach (PTS_MAM3.WSLogTemplateField.LogTemplateField_CODESET ltf in (List<PTS_MAM3.WSLogTemplateField.LogTemplateField_CODESET>)e.Result)
                            {
                                ltf.FSCODE_TITLE = ltf.FSCODE_ID + " - " + ltf.FSCODE_TITLE;
                                CodeSetList.Add(ltf);
                            }

                            cbxCode.ItemsSource = CodeSetList; 

                            for (int i =0; i<cbxCode.Items.Count;i++ )
                            {
                                if (((PTS_MAM3.WSLogTemplateField.LogTemplateField_CODESET)cbxCode.Items[i]).FSCODE_ID == clFieldsStruct.FSCODE_ID)
                                {
                                    cbxCode.SelectedIndex = i;
                                }
                            }
                            cbxCode.IsEnabled = false;

                            switch(clFieldsStruct.FSCODE_CTRL){
                                case "ComboBox":
                                    rdb1.IsChecked = true;

                                    txtCount.Value = "1";
                                    txtCount.IsEnabled = false;
                                break;
                                case "CheckBox":
                                    rdb2.IsChecked = true;

                                    txtCount.Value = clFieldsStruct.FSCODE_CNT;
                                    txtCount.IsEnabled = true;

                                    if (txtCount.Value.ToString() == "0")
                                        txtCount.Value = "";
                                break;
                            }

                            if (clFieldsStruct.FCISNULLABLE == "Y")
                            {
                                chkNull.IsChecked = true;
                            }
                            else
                            {
                                chkNull.IsChecked = false;
                            }

                            txtWidth.Value = clFieldsStruct.FNOBJECT_WIDTH;
                            //cbxCode.SelectedIndex = 0;

                            //    cbxCode.ItemsSource = CodeSetList;
                            //    cbxCode.SelectedIndex = clFieldsStruct.;
                            break;
                    }


                }
                else
                {
                    MessageBox.Show("找不到相關代碼", "提示訊息", MessageBoxButton.OK);
                }
            }
            else
            {
                MessageBox.Show("發生錯誤: " + e.Error.ToString(), "提示訊息", MessageBoxButton.OK);
            }
        }

        void cbxCode_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxCode.SelectedIndex == 0)
            {
                lblTitle.Content = "...";
            }
            else
            {

                string str =((PTS_MAM3.WSLogTemplateField.LogTemplateField_CODESET)cbxCode.SelectedItem).FSCODE_TITLE;

                lblTitle.Content = str.Substring(str.IndexOf("-") + 2);
            }
        }

#endregion


#region"按鈕動作"

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            //this.DialogResult = true;
            if (cbxCode.SelectedIndex <= 0)
            {
                MessageBox.Show("請設定「對應代碼」", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (rdb1.IsChecked == false && rdb2.IsChecked == false)
            {
                MessageBox.Show("請設定「選擇方式」", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (txtWidth.Value.ToString().Trim() == "")
            {
                MessageBox.Show("請設定「元件寬度」", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else
            {
                switch (m_Action)
                {
                    case "ADD":
                        WSDirectoryAdmin.TBTemplate_FieldsStruct tfADD = new WSDirectoryAdmin.TBTemplate_FieldsStruct();

                        tfADD.FCGRID = "N";

                        if (chkNull.IsChecked == true)
                            tfADD.FCISNULLABLE = "Y";
                        else
                            tfADD.FCISNULLABLE = "N";

                        tfADD.FCLIST = "0";
                        tfADD.FCLIST_TYPE = "Y";
                        tfADD.FNFIELD_LENGTH = 100;
                        tfADD.FNOBJECT_WIDTH = Convert.ToInt32(txtWidth.Value);
                        tfADD.FNORDER = 0;
                        tfADD.FNTEMPLATE_ID = _fntemplate_id;
                        tfADD.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                        tfADD.FSDEFAULT = "";
                        tfADD.FSDESCRIPTION = "";
                        tfADD.FSFIELD = (string) this.cbxField.SelectedItem;
                        tfADD.FSFIELD_NAME = lblTitle.Content.ToString();
                        tfADD.FSFIELD_TYPE = "自訂代碼";
                        tfADD.FSLIST_NAME = "";
                        tfADD.FSTABLE = _fstable;
                        tfADD.FSUPDATED_BY = UserClass.userData.FSUSER_ID;

                        tfADD.FSCODE_ID = cbxCode.SelectedValue.ToString();
                        if(txtCount.Value.ToString()=="")
                        {
                            tfADD.FSCODE_CNT = "0";
                        }else{
                            tfADD.FSCODE_CNT =txtCount.Value.ToString();
                        }

                        if (rdb1.IsChecked == true)
                        {
                            tfADD.FSCODE_CTRL = "ComboBox";
                        }else{
                              tfADD.FSCODE_CTRL = "CheckBox";
                        }

                        WSDirectoryAdmin.WSDirectoryAdminSoapClient dirObjADD = new WSDirectoryAdmin.WSDirectoryAdminSoapClient();

                        dirObjADD.fnInsertTBTEMPLATE_FIELDS2Completed += (s, args) =>
                        {
                            if (args.Error == null)
                            {
                                if (args.Result == false)
                                {
                                    MessageBox.Show("資料新增時發生錯誤!", "錯誤", MessageBoxButton.OK); 

                                }
                                else
                                    this.DialogResult = true;
                            }
                    
                        };

                        dirObjADD.fnInsertTBTEMPLATE_FIELDS2Async(tfADD, UserClass.userData.FSUSER_ID); 
                         
                        break;
                    case "MOD":
                        WSDirectoryAdmin.TBTemplate_FieldsStruct tfMOD = new WSDirectoryAdmin.TBTemplate_FieldsStruct();

                        tfMOD.FCGRID = "N";

                        if (chkNull.IsChecked == true)
                            tfMOD.FCISNULLABLE = "Y";
                        else
                            tfMOD.FCISNULLABLE = "N";
                        tfMOD.FNID = clFieldsStruct.FNID;

                        tfMOD.FCLIST = "0";
                        tfMOD.FCLIST_TYPE = "Y";
                        tfMOD.FNFIELD_LENGTH = 100;
                        tfMOD.FNOBJECT_WIDTH = Convert.ToInt32(txtWidth.Value);
                        tfMOD.FNORDER = 0;
                        tfMOD.FNTEMPLATE_ID = clFieldsStruct.FNTEMPLATE_ID;
                        tfMOD.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                        tfMOD.FSDEFAULT = "";
                        tfMOD.FSDESCRIPTION = "";
                        tfMOD.FSFIELD = (string)this.cbxField.SelectedItem;
                        tfMOD.FSFIELD_NAME = lblTitle.Content.ToString();
                        tfMOD.FSFIELD_TYPE = "自訂代碼";
                        tfMOD.FSLIST_NAME = "";
                        tfMOD.FSTABLE = clFieldsStruct.FSTABLE;
                        tfMOD.FSUPDATED_BY = UserClass.userData.FSUSER_ID;

                        tfMOD.FSCODE_ID = cbxCode.SelectedValue.ToString();
                        if(txtCount.Value.ToString()=="")
                        {
                            tfMOD.FSCODE_CNT = "0";
                        }else{
                            tfMOD.FSCODE_CNT = txtCount.Value.ToString();
                        }

                        if (rdb1.IsChecked == true)
                        {
                            tfMOD.FSCODE_CTRL = "ComboBox";
                        }else{
                            tfMOD.FSCODE_CTRL = "CheckBox";
                        }
                         
                        WSDirectoryAdmin.WSDirectoryAdminSoapClient dirObjMOD = new WSDirectoryAdmin.WSDirectoryAdminSoapClient();

                        dirObjMOD.fnUpdateTBTEMPLATE_FIELDS2Completed += (s, args) =>
                        {
                            if (args.Error == null)
                            {
                                if (args.Result == false)
                                {
                                    MessageBox.Show("資料更新時發生錯誤!", "錯誤", MessageBoxButton.OK);

                                }
                                else
                                    this.DialogResult = true;
                            }

                        };

                        dirObjMOD.fnUpdateTBTEMPLATE_FIELDS2Async(tfMOD, UserClass.userData.FSUSER_ID);

                        break;
                }


            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void rdb1_Checked(object sender, RoutedEventArgs e)
        {
            txtCount.IsEnabled = false;
            txtCount.Value = "1";
        } 

        private void rdb2_Checked(object sender, RoutedEventArgs e)
        {
            txtCount.IsEnabled = true;
            txtCount.Value = "";           
        }
         
#endregion



    }
}

