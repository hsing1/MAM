﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;

namespace PTS_MAM3.MAG
{
    public partial class MAG168 : Page
    {
        WSLogTemplateField.WSLogTemplateFieldSoapClient clntLTF = new WSLogTemplateField.WSLogTemplateFieldSoapClient();
        List<PTS_MAM3.WSLogTemplateField.LogTemplateField_CODESET> lstCODESET = new List<PTS_MAM3.WSLogTemplateField.LogTemplateField_CODESET>();
        Boolean isPostBack = false;
        String _ID = "", _TITLE = "", _NOTE = "", _ISENABLED = ""; 

        public MAG168()
        {
            InitializeComponent();
            InitializeForm();
        }

        void InitializeForm() //初始化本頁面
        {
            _ID = "";
            _TITLE = "";
            _NOTE = "";
            _ISENABLED = "01";

            clntLTF.GetCodeSetListCompleted += new EventHandler<WSLogTemplateField.GetCodeSetListCompletedEventArgs>(clntLTF_GetCodeSetListCompleted);
            clntLTF.DeleteCodeSetCompleted += new EventHandler<WSLogTemplateField.DeleteCodeSetCompletedEventArgs>(clntLTF_DeleteCodeSetCompleted);
            clntLTF.GetCodeSetListAsync("","","","01");
            busyLoading.IsBusy = true;
        }


#region"觸發事件"

        void clntLTF_GetCodeSetListCompleted(object sender, WSLogTemplateField.GetCodeSetListCompletedEventArgs e)
        {
            //如果取回的資料沒有錯誤訊息
            if (e.Error == null)
            {
                //如果取回的資料筆數不為0
                if (e.Result.Count != 0)
                {
                    //把查詢結果丟入公用成員lstNews中, 方便母視窗取用
                    lstCODESET = (List<PTS_MAM3.WSLogTemplateField.LogTemplateField_CODESET>)(e.Result);
                    dgResult.ItemsSource = e.Result;

                    if (!isPostBack)
                    {
                        isPostBack = true;
                    }else
                    {
                        MessageBox.Show("符合篩選條件的資料共 " + e.Result.Count + " 筆", "提示訊息", MessageBoxButton.OK);
                    }
                }else{
                    //如果取回的資料筆數=0
                    if (!isPostBack)
                    {
                        isPostBack = true;
                    }else{
                        MessageBox.Show("輸入的篩選條件找不到相關的資料", "提示訊息", MessageBoxButton.OK);

                    }
                }
            }
            else
            {
                //如果取回的資料有錯誤訊息
                if (!isPostBack)
                    {
                        isPostBack = true;
                    }else
                    {
                        MessageBox.Show("發生錯誤: " + e.Error.ToString(), "提示訊息", MessageBoxButton.OK);
                    }
            }
            busyLoading.IsBusy = false;
        }

        void clntLTF_DeleteCodeSetCompleted(object sender, WSLogTemplateField.DeleteCodeSetCompletedEventArgs e)
        {
            //如果取回的資料沒有錯誤訊息
            if (e.Error == null)
            {
                String msg = e.Result;
                if (msg == "")
                {
                    //如果新增成功
                    MessageBox.Show("刪除成功", "提示訊息", MessageBoxButton.OK); 
                }
                else
                {
                    //如果新增失敗
                    MessageBox.Show("刪除時發生錯誤: " + msg.Substring(6), "提示訊息", MessageBoxButton.OK); 
                }
            }
            else
            {
                //如果取回的資料有錯誤訊息
                MessageBox.Show("呼叫刪除函數時發生錯誤: " + e.Error.ToString(), "提示訊息", MessageBoxButton.OK); 
            }
            busyLoading.IsBusy = false;
        }


        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

#endregion


#region"按鈕事件"

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            //宣告出新增子視窗
            MAG168_01 frmADD = new MAG168_01("ADD");
            frmADD.Show();

            frmADD.Closed += (s, args) =>
            {
                if (frmADD.DialogResult == true)
                {
                    //子視窗新增成功後,重新整理清單
                    clntLTF.GetCodeSetListAsync("", "", "", "01");
                    busyLoading.IsBusy = true;
                }
            };            
        }

        private void btnMod_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (dgResult.SelectedItem == null)
            {
                MessageBox.Show("您尚未選擇要修改的資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //宣告出修改子視窗
            MAG168_01 frmMOD = new MAG168_01("MOD", (PTS_MAM3.WSLogTemplateField.LogTemplateField_CODESET)dgResult.SelectedItem);
            frmMOD.Show();

            frmMOD.Closed += (s, args) =>
            {
                if (frmMOD.DialogResult == true)
                {
                    //子視窗修改成功後,重新整理清單
                    clntLTF.GetCodeSetListAsync("", "", "", "01");
                    busyLoading.IsBusy = true;
                }
            };         
        }

        private void btnQry_Click(object sender, RoutedEventArgs e)
        {
            //宣告出查詢子視窗
            MAG168_01 frmQRY = new MAG168_01("QRY", _ID, _TITLE, _NOTE, _ISENABLED);
            frmQRY.Show();

            frmQRY.Closed += (s, args) =>
            {
                if (frmQRY.DialogResult == true)
                {
                    _ID = frmQRY._ID;
                    _TITLE = frmQRY._TITLE;
                    _NOTE = frmQRY._NOTE;
                    _ISENABLED = frmQRY._ISENABLED;

                    //子視窗輸入後,重新整理清單
                    clntLTF.GetCodeSetListAsync( _ID,  _TITLE, _NOTE, _ISENABLED);
                    busyLoading.IsBusy = true;
                }
            };         
        }

        private void btnDel_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (dgResult.SelectedItem == null)
            {
                MessageBox.Show("您尚未選擇要刪除的資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            PTS_MAM3.WSLogTemplateField.LogTemplateField_CODESET cl = (PTS_MAM3.WSLogTemplateField.LogTemplateField_CODESET)dgResult.SelectedItem;

             if(cl._COUNT>0){
                MessageBox.Show("請先刪除所屬代碼項目才能移除代碼設定", "提示訊息", MessageBoxButton.OK);
                return;
             }

            //開始刪除檔案
             busyLoading.IsBusy = true;
             clntLTF.DeleteCodeSetAsync(cl.FSCODE_ID);
        }

#endregion


#region"按鈕事件"

        private void btnDetail_Click(object sender, RoutedEventArgs e)
        {
            var ctl = e.OriginalSource as Button;
            if (ctl != null)
            {
                PTS_MAM3.WSLogTemplateField.LogTemplateField_CODESET cl = (PTS_MAM3.WSLogTemplateField.LogTemplateField_CODESET)(ctl.DataContext);

                //宣告出新增子視窗
                MAG168_02 frmMAG168_02 = new MAG168_02(cl);
                frmMAG168_02.Show();

                frmMAG168_02.Closed += (s, args) =>
                {
                    //if (frmMAG168_02._ResetOK == true)
                    //{
                    //    //子視窗修改成功後,重新整理清單
                    //    clntTranscode.GetTBLOG_WORKER_ByDatesAsync(DateTime.Parse(date1.Text), DateTime.Parse(date2.Text), "MAM_PTS_ANS_SCENECHANGEDETECT", "");
                    //}
                };
            }
        }

#endregion

    }
}
