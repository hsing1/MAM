﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.Web.ENT;

namespace PTS_MAM3.MAG
{
    public partial class MAG101_01 : ChildWindow
    {
        private ENT100DomainContext content = new ENT100DomainContext();
        private string tbName;
        public MAG101_01(string InputtbName)
        {
            InitializeComponent();
            tbName = InputtbName;
            //BiggestSort();
            FillBuyNameIntoCombox1();
        }

        private void FillBuyNameIntoCombox1()
        {
            switch (tbName)
            {
                case "TBZPROMOCATD":
                    var PromocatTBEntity = content.Load(content.GetTBZPROMOCATQuery());
                    PromocatTBEntity.Completed += (s, args) =>
                        {
                            foreach (TBZPROMOCAT x in PromocatTBEntity.Entities)
                            {
                                ComboBoxItem CBI = new ComboBoxItem();
                                if (x.FSPROMOCATID.Trim() != "")
                                {
                                    CBI.Content = x.FSPROMOCATNAME;
                                    CBI.Tag = x.FSPROMOCATID;
                                    comboBox1.Items.Add(CBI);
                                }
                            }
                            comboBox1.SelectedIndex = 0;
                            #region 取得外購類別名稱中單一類別的SORT
                            var fielterTBZPROMOCATD = content.Load(content.GetTBZPROMOCATDQuery());
                            fielterTBZPROMOCATD.Completed += (s1, args1) =>
                                {
                                    int i=0;
                                    var fielterTBZPROMOCATD1 = from y in fielterTBZPROMOCATD.Entities
                                                  where y.FSPROMOCATID == ((ComboBoxItem)comboBox1.SelectedItem).Tag.ToString()
                                                  select y;
                                    foreach(TBZPROMOCATD x in fielterTBZPROMOCATD1)
                                    {
                                        if (x.FSSORT.Trim() != "")
                                        {
                                            if (i <= Convert.ToInt32(x.FSSORT))
                                            { i = Convert.ToInt32(x.FSSORT); }
                                        }
                                    }
                                    i += 1;
                                    if (i <= 9)
                                    { textBox2.Text = "0" + i.ToString(); }
                                    else
                                    { textBox2.Text = i.ToString(); }
                                };
                            #endregion
                        };
                    break;
                case "TBZPROGBUYD":
                    var BuyTBEntity = content.Load(content.GetTBZPROGBUYQuery());
                    BuyTBEntity.Completed += (s, args) =>
                        {
                            foreach (TBZPROGBUY x in BuyTBEntity.Entities)
                            {
                                ComboBoxItem CBI = new ComboBoxItem();
                                if (x.FSPROGBUYID.Trim() != "")
                                {
                                    CBI.Content = x.FSPROGBUYNAME;
                                    CBI.Tag = x.FSPROGBUYID;
                                    comboBox1.Items.Add(CBI);
                                }
                            }
                            comboBox1.SelectedIndex = 0;
                            #region 取得外購類別名稱中單一類別的SORT
                            var fielter = content.Load(content.GetTBZPROGBUYDQuery());
                            fielter.Completed += (s1, args1) =>
                                {
                                    int i=0;
                                    var filter1 = from y in fielter.Entities
                                                  where y.FSPROGBUYID == ((ComboBoxItem)comboBox1.SelectedItem).Tag.ToString()
                                                  select y;
                                    foreach(TBZPROGBUYD y in filter1)
                                    {
                                        if (y.FSSORT.Trim() != "")
                                        {
                                            if (i <= Convert.ToInt32(y.FSSORT))
                                            { i = Convert.ToInt32(y.FSSORT); }
                                        }
                                    }
                                    i += 1;
                                    if (i <= 9)
                                    { textBox2.Text = "0" + i.ToString(); }
                                    else
                                    { textBox2.Text = i.ToString(); }
                                };
                            #endregion
                        };
                    break;
            }

        }

        #region 把最大的Sort抓出來
        private void BiggestSort()
        {
            int numId = 0;
            string strId;
            switch (tbName)
            {
                case "TBZPROMOCATD":
                    var TBJoinClassTBZPROMOCATD = content.Load(content.GetJoinTBZPROMOCATDQueryQuery());
                    TBJoinClassTBZPROMOCATD.Completed += (s, args) =>
                    {
                        foreach (TBZPROMOCATDJoinClass x in TBJoinClassTBZPROMOCATD.Entities)
                        {
                            if (x.FSSORT.Trim() != "")
                            {
                                if (Convert.ToInt32(x.FSSORT) > numId)
                                { numId = Convert.ToInt32(x.FSSORT); }
                            }
                        }
                        numId += 1;
                        if (numId <= 9)
                        {
                            strId = "0" + numId.ToString();
                        }
                        else if (numId >= 99)
                        { strId = "99"; }
                        else
                        { strId = numId.ToString(); }
                        textBox2.Text = strId;
                    };
                    break;
                case "TBZPROGBUYD":
                    var TBJoinClass = content.Load(content.GetJoinTableBUYIDQueryQuery());
                    TBJoinClass.Completed += (s, args) =>
                    {
                        foreach (TBJoinClass x in TBJoinClass.Entities)
                        {
                            if (x.FSSORT.Trim() != "")
                            {
                                if (Convert.ToInt32(x.FSSORT) > numId)
                                { numId = Convert.ToInt32(x.FSSORT); }
                            }
                        }
                        numId += 1;
                        if (numId <= 9)
                        {
                            strId = "0" + numId.ToString();
                        }
                        else if (numId >= 99)
                        { strId = "99"; }
                        else
                        { strId = numId.ToString(); }
                        textBox2.Text = strId;
                    };
                    break;
            }
        }
        #endregion
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            #region 按下確認鍵後 將資料寫入資料庫
            int intCheck;
            if (int.TryParse(textBox2.Text.Trim(), out intCheck) && intCheck > 0 && textBox1.Text.Count() > 0)
            {
                #region 把最大的Id抓出來 並將資料寫回
                int numId = 0;
                string strId;
                switch (tbName)
                {
                    case "TBZPROMOCATD":
                        var TBZPROMOCATD = content.Load(content.GetJoinTBZPROMOCATDQueryQuery());
                        TBZPROMOCATD.Completed += (s, args) =>
                        {
                            #region 取得沒有用到的Id空號由"小到大"
                            int[] array = new int[101];
                            for (int i = 0; i <= 100; i++)
                            { array[i] = 0; }
                            foreach (TBZPROMOCATDJoinClass x in TBZPROMOCATD.Entities)
                            {
                                if (x.FSPROMOCATDID.Trim() != "")
                                {
                                    if (x.FSPROMOCATID == ((ComboBoxItem)(comboBox1.SelectedItem)).Tag.ToString())
                                    { array[Convert.ToInt32(x.FSPROMOCATDID)] = 1; }
                                }
                            }
                            for (int i = 1; i <= 100; i++)
                            {
                                if (array[i] == 0)
                                {
                                    numId = i;
                                    break;
                                }
                                if (i == 100)
                                {
                                    MessageBox.Show("資料庫內編號已使用完畢，請聯絡管理員!");
                                    this.DialogResult = false;
                                }
                            }
                            #endregion
                            if (numId <= 9)
                            { strId = "0" + numId.ToString(); }
                            else
                            { strId = numId.ToString(); }
                            #region 用NC提供的Instert方法
                            TBZPROMOCATD TBZPROMOCATDClass = new Web.ENT.TBZPROMOCATD();
                            //TBZPROMOCATDJoinClass TBZPROMOCATDJoinClass = new TBZPROMOCATDJoinClass();
                            TBZPROMOCATDClass.FDCREATED_DATE = DateTime.Now;
                            TBZPROMOCATDClass.FDUPDATED_DATE = DateTime.Now;
                            TBZPROMOCATDClass.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                            TBZPROMOCATDClass.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                            TBZPROMOCATDClass.FSPROMOCATDID = strId;
                            TBZPROMOCATDClass.FSPROMOCATDNAME = textBox1.Text;
                            TBZPROMOCATDClass.FSPROMOCATID = ((ComboBoxItem)comboBox1.SelectedItem).Tag.ToString();
                            TBZPROMOCATDClass.FSSORT = textBox2.Text;
                            //TBZPROMOCATDClass.FSPROMOCATNAME = ((ComboBoxItem)(comboBox1.SelectedItem)).Content.ToString();
                            TBZPROMOCATDClass.FBISENABLE = true;
                            content.TBZPROMOCATDs.Add(TBZPROMOCATDClass);
                            content.SubmitChanges().Completed += (s2, args2) =>
                            {
                                this.DialogResult = true;
                            };
                            //content.TBZPROMOCATDJoinClasses.Add(TBZPROMOCATDClass);
                            //content.SubmitChanges().Completed += (s2, args2) =>
                            //{
                            //    this.DialogResult = true;
                            //};
                            #endregion
                        };
                        break;
                    case "TBZPROGBUYD":
                        var TBZPROGBUYD = content.Load(content.GetJoinTableBUYIDQueryQuery());
                        TBZPROGBUYD.Completed += (s, args) =>
                        {
                            #region 取得沒有用到的Id空號由"小到大"
                            int[] array = new int[101];
                            for (int i = 0; i <= 100; i++)
                            { array[i] = 0; }
                            foreach (TBJoinClass x in TBZPROGBUYD.Entities)
                            {
                                if (x.FSPROGBUYDID.Trim() != "")
                                {
                                    if (x.FSPROGBUYID == ((ComboBoxItem)(comboBox1.SelectedItem)).Tag.ToString())
                                    { array[Convert.ToInt32(x.FSPROGBUYDID)] = 1; }
                                }
                            }
                            for (int i = 1; i <= 100; i++)
                            {
                                if (array[i] == 0)
                                {
                                    numId = i;
                                    break;
                                }
                                if (i == 100)
                                {
                                    MessageBox.Show("資料庫內編號已使用完畢，請聯絡管理員!");
                                    this.DialogResult = false;
                                }
                            }
                            #endregion
                            if (numId <= 9)
                            { strId = "0" + numId.ToString(); }
                            else
                            { strId = numId.ToString(); }
                            #region 用NC提供的Instert方法
                            TBJoinClass TBJClass = new TBJoinClass();
                            TBJClass.FDCREATED_DATE = DateTime.Now;
                            TBJClass.FDUPDATED_DATE = DateTime.Now;
                            TBJClass.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                            TBJClass.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                            TBJClass.FSPROGBUYDID = strId;
                            TBJClass.FSPROGBUYDNAME = textBox1.Text;
                            TBJClass.FSPROGBUYID = ((ComboBoxItem)comboBox1.SelectedItem).Tag.ToString();
                            TBJClass.FSSORT = textBox2.Text;
                            TBJClass.FSPROGBUYNAME = ((ComboBoxItem)(comboBox1.SelectedItem)).Content.ToString();
                            TBJClass.FBISENABLE = true;
                            content.TBJoinClasses.Add(TBJClass);
                            content.SubmitChanges().Completed += (s2, args2) =>
                                {
                                    this.DialogResult = true;
                                };
                            #endregion

                            //TBZPROGBUYD TBZPROGBUYDClass = new TBZPROGBUYD();
                            //TBZPROGBUYDClass.FDCREATED_DATE = DateTime.Now;
                            //TBZPROGBUYDClass.FDUPDATED_DATE = DateTime.Now;
                            //TBZPROGBUYDClass.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                            //TBZPROGBUYDClass.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                            //TBZPROGBUYDClass.FSPROGBUYDID = strId;
                            //TBZPROGBUYDClass.FSPROGBUYDNAME = textBox1.Text;
                            //TBZPROGBUYDClass.FSPROGBUYID = ((ComboBoxItem)comboBox1.SelectedItem).Tag.ToString();
                            //TBZPROGBUYDClass.FSSORT = textBox2.Text;
                            //content.TBZPROGBUYDs.Add(TBZPROGBUYDClass);
                            //content.SubmitChanges().Completed += (s2, args2) =>
                            //{
                            //    this.DialogResult = true;
                            //};
                        };
                        break;
                }

                #endregion
            }
            else
            {
                if (int.TryParse(textBox2.Text.Trim(), out intCheck) != true || textBox2.Text.Trim() == "" || intCheck == 0)
                { MessageBox.Show("顯示排序欄位請填入大於0以及小於或等於99的\"數字\"!"); }
                if (textBox1.Text.Trim().Count() == 0)
                { MessageBox.Show("製作目的名稱必須填入資料!"); }
            }
            #endregion
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void comboBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (tbName)
            {
                case "TBZPROMOCATD":
                    #region 取得外購類別名稱中單一類別的SORT
                    var fielterTBZPROMOCATD = content.Load(content.GetTBZPROGBUYDQuery());
                    fielterTBZPROMOCATD.Completed += (s1, args1) =>
                    {
                        int i = 0;
                        var filterTBZPROMOCATD1 = from y in fielterTBZPROMOCATD.Entities
                                      where y.FSPROGBUYID == ((ComboBoxItem)comboBox1.SelectedItem).Tag.ToString()
                                      select y;
                        foreach (TBZPROGBUYD y in filterTBZPROMOCATD1)
                        {
                            if (y.FSSORT.Trim() != "")
                            {
                                if (i <= Convert.ToInt32(y.FSSORT))
                                { i = Convert.ToInt32(y.FSSORT); }
                            }
                        }
                        i += 1;
                        if (i <= 9)
                        { textBox2.Text = "0" + i.ToString(); }
                        else
                        { textBox2.Text = i.ToString(); }
                    };
                    #endregion
                    break;
                case "TBZPROGBUYD":
                    #region 取得外購類別名稱中單一類別的SORT
                    var fielter = content.Load(content.GetTBZPROGBUYDQuery());
                    fielter.Completed += (s1, args1) =>
                    {
                        int i = 0;
                        var filter1 = from y in fielter.Entities
                                      where y.FSPROGBUYID == ((ComboBoxItem)comboBox1.SelectedItem).Tag.ToString()
                                      select y;
                        foreach (TBZPROGBUYD y in filter1)
                        {
                            if (y.FSSORT.Trim() != "")
                            {
                                if (i <= Convert.ToInt32(y.FSSORT))
                                { i = Convert.ToInt32(y.FSSORT); }
                            }
                        }
                        i += 1;
                        if (i <= 9)
                        { textBox2.Text = "0" + i.ToString(); }
                        else
                        { textBox2.Text = i.ToString(); }
                    };
                    #endregion
                    break;
            }

        }
    }
}

