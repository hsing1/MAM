﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.MAG
{
    public partial class MAG400_01 : ChildWindow
    {
        SR_PGM_NasFileMoving.ClassNasMovingJob m_FormData;

        // 從母畫面傳來的物件參數
        public MAG400_01(SR_PGM_NasFileMoving.ClassNasMovingJob FormData)
        {
            InitializeComponent();
            // 將母畫面的資料寫到子畫面的區域變數中
            m_FormData = FormData;
            // 將物件的屬性綁到介面中
            fnBindClassAttributesToForm();
        }

        // 將物件的屬性綁到介面中
        private void fnBindClassAttributesToForm()
        {
            lbl6.Content = m_FormData.VideoID;
            lbl7.Content = m_FormData.RequestTime;
            lbl8.Content = m_FormData.StartProcTime;
            lbl9.Content = m_FormData.FinishProcTime;
            lbl10.Content = m_FormData.Status;
        }

        // 功能「確定」
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        // 功能「重啟任務」
        private void btn1_Click(object sender, RoutedEventArgs e)
        {
            SR_PGM_NasFileMoving.ServiceNasFileMovingSoapClient objNasMovingService = new SR_PGM_NasFileMoving.ServiceNasFileMovingSoapClient();
            objNasMovingService.RetryNasFileMovingServiceCompleted += new EventHandler<SR_PGM_NasFileMoving.RetryNasFileMovingServiceCompletedEventArgs>(objNasMovingService_RetryNasFileMovingServiceCompleted);
            objNasMovingService.RetryNasFileMovingServiceAsync(m_FormData);
        }

        private void objNasMovingService_RetryNasFileMovingServiceCompleted(object sender, SR_PGM_NasFileMoving.RetryNasFileMovingServiceCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    MessageBox.Show("已發出重置作業命令");
                    this.DialogResult = true;       // 完成就自動關掉吧
                }
                else
                    MessageBox.Show("發送重置作業命令時發生錯誤");
            }
            else
                MessageBox.Show("連結主機服務時發生問題：" + e.Error.ToString());
        }
    }
}
