﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.MAG
{
    public partial class MAG128 : ChildWindow
    {
        public MAG128()
        {
            InitializeComponent();
        }

        public string strReturnCondection = "";
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (textBox1.Text != "")
            {
                strReturnCondection = textBox1.Text.ToString().Trim();
                this.DialogResult = true;
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

