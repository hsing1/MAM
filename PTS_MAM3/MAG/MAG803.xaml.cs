﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using PTS_MAM3.WSDELETE;
using System.Windows.Data;

namespace PTS_MAM3.MAG
{
    public partial class MAG803 : UserControl
    {
        private WSDELETESoapClient wsDeleteClient = new WSDELETESoapClient();
        private Class_DELETED_SEARCH_PARAMETER _class_deleted_search_parameter = null;
        private PTS_MAM3.MainFrame parentFrame;
        private List<Class_DELETED_SEARCH_RESULT> _class_deleted_search_result_list = new List<Class_DELETED_SEARCH_RESULT>();
        
        public MAG803(PTS_MAM3.MainFrame mainframe, Object _object)
        {
            InitializeComponent();
            this.parentFrame = mainframe;
            this._class_deleted_search_parameter = _object as Class_DELETED_SEARCH_PARAMETER;

            wsDeleteClient.GetAlreadyDeletedFileListCompleted += new EventHandler<GetAlreadyDeletedFileListCompletedEventArgs>(wsDeleteClient_GetAlreadyDeletedFileListCompleted);
            wsDeleteClient.GetAlreadyDeletedFileListAsync(this._class_deleted_search_parameter);
            this.RadBusyIndicator.IsBusy = true;
        }

        public void Refresh()
        {
            this.RadBusyIndicator.IsBusy = true;
            wsDeleteClient.GetAlreadyDeletedFileListAsync(this._class_deleted_search_parameter);
        }

        void wsDeleteClient_GetAlreadyDeletedFileListCompleted(object sender, GetAlreadyDeletedFileListCompletedEventArgs e)
        {
            if (e.Error != null || e.Result == null)
            {
                MessageBox.Show("查詢資料錯誤!", "訊息", MessageBoxButton.OK);
                return;
            }
            else if (e.Result.Count == 0)
            {
                MessageBox.Show("查無資料!", "訊息", MessageBoxButton.OK);
                RadPane openedPane = (RadPane)parentFrame.PaneGroup.SelectedItem;
                this.parentFrame.PaneGroup.RemovePane(openedPane);
                this.parentFrame.PaneGroup.SelectedIndex = this.parentFrame.PaneGroup.Items.Count - 1;
                return;
            }

            this.TextBlock_TotalCount.Text = "有 " + e.Result.Count + " 項結果";

            PagedCollectionView pcb = new PagedCollectionView(e.Result);
            this.DataGrid_Result.ItemsSource = pcb;
            this.DataPager.Source = pcb;

            this.RadBusyIndicator.IsBusy = false;
        }
    }
}
