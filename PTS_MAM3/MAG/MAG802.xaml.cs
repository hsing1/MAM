﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSDELETE;

namespace PTS_MAM3.MAG
{
    public partial class MAG802 : UserControl
    {
        private WSDELETESoapClient wsDeleteClient = new WSDELETESoapClient();
        private Class_DELETED_SEARCH_PARAMETER _class_deleted_search_parameter = new Class_DELETED_SEARCH_PARAMETER();
        private PTS_MAM3.MainFrame parentFrame;
        
        public MAG802(PTS_MAM3.MainFrame mainframe)
        {
            InitializeComponent();

            parentFrame = mainframe;

            _class_deleted_search_parameter._fsfile_type_v = "";
            _class_deleted_search_parameter._fsfile_type_a = "";
            _class_deleted_search_parameter._fsfile_type_p = "";
            _class_deleted_search_parameter._fsfile_type_d = "";
        }

        private void Button_Search_Prog_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            MAG800_01 mag800_01 = new MAG800_01("", _class_deleted_search_parameter._fstype);
            mag800_01.Show();

            mag800_01.Closing += (s, args) =>
            {
                this.TextBox_Prog_ID.Text = mag800_01._fsprog_id;
                this.TextBox_Prog_Name.Text = mag800_01._fsprog_name;
            };
        }

        private void RadioButton_G_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            wsDeleteClient.GetChannelListAsync();
            _class_deleted_search_parameter._fstype = "G";
        }

        private void RadioButton_P_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            wsDeleteClient.GetChannelListAsync();
            _class_deleted_search_parameter._fstype = "P";
        }

        private void RadioButton_D_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            wsDeleteClient.GetChannelListAsync();
            _class_deleted_search_parameter._fstype = "D";
        }

        private void Button_Search_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            if (!string.IsNullOrEmpty(this.TextBox_Prog_ID.Text))
            {
                if (string.IsNullOrEmpty(this.TextBox_Episode_Beg.Text) || string.IsNullOrEmpty(this.TextBox_Episode_Beg.Text))
                {
                    MessageBox.Show("請輸入集數!", "訊息", MessageBoxButton.OKCancel);
                    return;
                }
            }
            if (string.IsNullOrEmpty(_class_deleted_search_parameter._fsfile_type_v) &&
                string.IsNullOrEmpty(_class_deleted_search_parameter._fsfile_type_a) &&
                string.IsNullOrEmpty(_class_deleted_search_parameter._fsfile_type_p) &&
                string.IsNullOrEmpty(_class_deleted_search_parameter._fsfile_type_d))
            {
                MessageBox.Show("請選擇至少一種檔案類型!", "訊息", MessageBoxButton.OKCancel);
                return;
            }
            if (!this.DatePicker_Deleted_Date_Beg.SelectedDate.HasValue || !this.DatePicker_Deleted_Date_End.SelectedDate.HasValue)
            {
                MessageBox.Show("請輸入刪除起迄日期!", "訊息", MessageBoxButton.OKCancel);
                return;
            }
            if (this.DatePicker_Deleted_Date_Beg.SelectedDate > this.DatePicker_Deleted_Date_End.SelectedDate)
            {
                MessageBox.Show("起迄日期前後錯誤!", "訊息", MessageBoxButton.OKCancel);
                return;
            }

            _class_deleted_search_parameter._fsprog_id = this.TextBox_Prog_ID.Text;
            _class_deleted_search_parameter._fnepisode_beg = this.TextBox_Episode_Beg.Text;
            _class_deleted_search_parameter._fnepisode_end = this.TextBox_Episode_End.Text;
            _class_deleted_search_parameter._fdcreated_date_beg = this.DatePicker_Deleted_Date_Beg.SelectedDate.Value.ToString("yyyy/MM/dd");
            _class_deleted_search_parameter._fdcreated_date_end = this.DatePicker_Deleted_Date_End.SelectedDate.Value.ToString("yyyy/MM/dd");
            _class_deleted_search_parameter._fsfile_no = this.TextBox_FileNo.Text;
            parentFrame.generateNewPane("查詢刪除檔案記錄結果", "SEARCH_DELETE_SEARCH_RESULT", _class_deleted_search_parameter, false);
            //MessageBox.Show(this.ComboBox_File_Status.SelectedItem.ToString().Split(':')[0]);
        }

        private void CheckBox_Video_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
            _class_deleted_search_parameter._fsfile_type_v = "";
            if ((bool)this.CheckBox_Video.IsChecked)
            {
                _class_deleted_search_parameter._fsfile_type_v = "V";
            }
            
        }

        private void CheckBox_Audio_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
            _class_deleted_search_parameter._fsfile_type_a = "";
            if ((bool)this.CheckBox_Audio.IsChecked)
            {
                _class_deleted_search_parameter._fsfile_type_a = "A";
            }
        }

        private void CheckBox_Photo_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
            _class_deleted_search_parameter._fsfile_type_p = "";
            if ((bool)this.CheckBox_Photo.IsChecked)
            {
                _class_deleted_search_parameter._fsfile_type_p = "P";
            }
        }

        private void CheckBox_Doc_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
            _class_deleted_search_parameter._fsfile_type_d = "";
            if ((bool)this.CheckBox_Doc.IsChecked)
            {
                _class_deleted_search_parameter._fsfile_type_d = "D";
            }
        }
    }
}
