﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.SystemAdmin
{
    public partial class MAG200_07 : ChildWindow
    {
        WSUserAdmin.UserStruct upduser = new WSUserAdmin.UserStruct();
        List<WSUserAdmin.Class_Dept> DeptList = new List<WSUserAdmin.Class_Dept>();
        List<WSUserAdmin.Class_SIGNATURE_PRIORITY> PriorityList = new List<WSUserAdmin.Class_SIGNATURE_PRIORITY>();
        WSUserAdmin.WSUserAdminSoapClient myService = new WSUserAdmin.WSUserAdminSoapClient();
        public MAG200_07(int iniTreeViewID)
        {
            InitializeComponent();
            

            myService.GetDeptListAllCompleted += (s, args) =>
                { 
                    DeptList = args.Result.ToList();
                    foreach (var item in DeptList)
                    {
                        ComboBoxItem cbi = new ComboBoxItem();
                        cbi.Content = item.FSFullName_ChtName;
                        cbi.Tag = item;
                        comboBox1.Items.Add(cbi);
                    }
                    comboBox1.SelectedIndex = 0;
                    //foreach (var items in comboBox1.Items)
                    //{
                    //    if (((WSUserAdmin.Class_Dept)(((ComboBoxItem)items).Tag)).FNDEP_ID == iniTreeViewID)
                    //    {
                    //        comboBox1.SelectedItem = items;
                    //    }
                    //}
                };
            myService.GetDeptListAllAsync();
            myService.GetSignaturePriorityCompleted += (ss, argss) =>
            { 
                PriorityList = argss.Result.ToList();
                DGPriorityList.ItemsSource = PriorityList;
                DGPriorityList.SelectedIndex = 0;
            };
            myService.InsSignaturePriorityCompleted += (si, argsi) =>
                {
                    if (argsi.Result)
                    {
                        MessageBox.Show("新增成功");
                        textBox1.Text = "";
                        ReFreshSigList();
                    }
                    else
                        MessageBox.Show("新增失敗");
                };
            myService.DelSignaturePriorityCompleted += (sd, argsd) =>
                {
                    if (argsd.Result)
                    {
                        MessageBox.Show("刪除成功");
                        ReFreshSigList();
                    }
                    else
                    { MessageBox.Show("刪除失敗"); }
                };
            ReFreshSigList();
        }

        private void ReFreshSigList()
        { 
            myService.GetSignaturePriorityAsync();
            
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            WSUserAdmin.Class_SIGNATURE_PRIORITY myclaII = new WSUserAdmin.Class_SIGNATURE_PRIORITY();
            bool isSeted = false;

            foreach (var item in PriorityList)
            {
                if (((WSUserAdmin.Class_Dept)(((ComboBoxItem)(comboBox1.SelectedItem)).Tag)).FNDEP_ID.ToString() == item.FNDEPID)
                {
                    MessageBox.Show("此部門已設定簽核者 請刪除後再新增!");
                    isSeted = true;
                }
            }
            if (isSeted == false)
            {

                if (((WSUserAdmin.Class_Dept)(((ComboBoxItem)(comboBox1.SelectedItem)).Tag)).FNDEP_ID.ToString() != "" && textBox1.Text.Trim() != string.Empty)
                {
                    myclaII.FNDEPID = ((WSUserAdmin.Class_Dept)(((ComboBoxItem)(comboBox1.SelectedItem)).Tag)).FNDEP_ID.ToString();
                    myclaII.FSSignatureID = textBox1.Text.Trim();
                    myService.InsSignaturePriorityAsync(myclaII, UserClass.userData.FSUSER_ID);
                }
                else { MessageBox.Show("新增欄位資料請勿空白"); }
                
            }
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            WSUserAdmin.Class_SIGNATURE_PRIORITY myCLS=new WSUserAdmin.Class_SIGNATURE_PRIORITY();
            myCLS = ((WSUserAdmin.Class_SIGNATURE_PRIORITY)(DGPriorityList.SelectedItem));
            myService.DelSignaturePriorityAsync(myCLS, UserClass.userData.FSUSER_ID);
        }
    }
}

