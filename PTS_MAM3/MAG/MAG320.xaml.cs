﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;

namespace PTS_MAM3.MAG
{
    public partial class MAG320 : Page
    {
        public MAG320()
        {
            InitializeComponent();
            // 當讀入本頁面的時候，就自動幫忙撈資料吧
            fnGetVTRList();
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //
        }

        /// <summary>
        /// 顯示詳細資訊的按鈕
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDetail_Click(object sender, RoutedEventArgs e)
        {
            // 判斷是否有選取DataGrid
            if (DGVTRList.SelectedItem == null)
            {
                MessageBox.Show("請選擇要查詢的項目", "提示訊息", MessageBoxButton.OK);
                return;
            }

            // 開啟子視窗
            MAG320_01 MAG320_01_frm = new MAG320_01(((SR_ServiceTranscode.ClassVTR)DGVTRList.SelectedItem));
            MAG320_01_frm.Show();
            MAG320_01_frm.Closing += (s, args) =>
            {
                if (MAG320_01_frm.DialogResult == true)
                {
                    // 重新讀取主畫面的資料
                    fnGetVTRList();
                }
            };
        }

        /// <summary>
        /// 新增VTR的子畫面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAdd_Click(object sender, RoutedEventArgs e)
        {
            // 開啟子視窗
            MAG320_02 MAG320_02_frm = new MAG320_02();
            MAG320_02_frm.Show();
            MAG320_02_frm.Closing += (s, args) =>
            {
                // 關閉子畫面時，將母視窗的資料重新整理過
                fnGetVTRList();
            };
        }

        private void fnDisableButtons()
        {
            btnDetail.IsEnabled = false;
            btnAdd.IsEnabled = false;
        }

        private void fnEnableButtons()
        {
            btnDetail.IsEnabled = true;
            btnAdd.IsEnabled = true;
        }

        /// <summary>
        /// 呼叫 Web Service 取得所有的 VTR 資料
        /// </summary>
        private void fnGetVTRList()
        {
            fnDisableButtons();
            SR_ServiceTranscode.ServiceTranscodeSoapClient obj = new SR_ServiceTranscode.ServiceTranscodeSoapClient();
            obj.GetAllVTRListCompleted += new EventHandler<SR_ServiceTranscode.GetAllVTRListCompletedEventArgs>(obj_GetAllVTRListCompleted);
            obj.GetAllVTRListAsync();
        }

        private void obj_GetAllVTRListCompleted(object sender, SR_ServiceTranscode.GetAllVTRListCompletedEventArgs e)
        {
            //
            fnEnableButtons();
            DGVTRList.DataContext = null;
            //
            if (e.Error == null)
            {
                if (e.Result.Count == 0)
                {
                    MessageBox.Show("查不到VTR的資訊");
                    return;
                }
                // 
                DGVTRList.DataContext = e.Result;
            }
            else
                MessageBox.Show("連結主機服務時發生問題：" + e.Error.ToString());
        }
    }
}
