﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using PTS_MAM3.WSDELETE;
using System.Windows.Data;

namespace PTS_MAM3.MAG
{
    public partial class MAG801 : UserControl
    {
        private WSDELETESoapClient wsDeleteClient = new WSDELETESoapClient();
        private Class_DELETED_PARAMETER _class_deleted_parameter = null;
        private PTS_MAM3.MainFrame parentFrame;
        private List<Class_DELETED_RESULT> _class_deleted_result_list = new List<Class_DELETED_RESULT>();
        private List<string> _deleted_file_no_list = new List<string>();
        private List<Class_DELETED> _class_deleted_list = new List<Class_DELETED>();

        public MAG801(PTS_MAM3.MainFrame mainframe, Object _object)
        {
            InitializeComponent();
            this.parentFrame = mainframe;
            this._class_deleted_parameter = _object as Class_DELETED_PARAMETER;

            wsDeleteClient.GetDeletedFileListCompleted += new EventHandler<GetDeletedFileListCompletedEventArgs>(wsDeleteClient_GetDeletedFileListCompleted);
            wsDeleteClient.GetDeletedFileListAsync(this._class_deleted_parameter);

            wsDeleteClient.DeleteFileCompleted += new EventHandler<DeleteFileCompletedEventArgs>(wsDeleteClient_DeleteFileCompleted);

            this.RadBusyIndicator.IsBusy = true;

            _deleted_file_no_list.Clear();
            _class_deleted_list.Clear();
        }

        public void Refresh()
        {
            this.RadBusyIndicator.IsBusy = true;
            wsDeleteClient.GetDeletedFileListAsync(this._class_deleted_parameter);
        }

        void wsDeleteClient_DeleteFileCompleted(object sender, DeleteFileCompletedEventArgs e)
        {
            if (e.Error != null || e.Result.Count > 0)
            {
                string _fsfile_no = "";
                foreach (string item in e.Result)
                {
                    _fsfile_no += item + ",";
                }
                if (_fsfile_no.Length > 0)
                {
                    _fsfile_no = _fsfile_no.Substring(0, _fsfile_no.Length - 1);
                }
                MessageBox.Show("檔案編號：" + _fsfile_no + "刪除失敗，請洽資訊人員!", "訊息", MessageBoxButton.OK);
            }
            else
            {
                MessageBox.Show("刪除成功!", "訊息", MessageBoxButton.OK); 
            }
            _deleted_file_no_list.Clear();
            _class_deleted_list.Clear();
            wsDeleteClient.GetDeletedFileListAsync(this._class_deleted_parameter);
        }

        void wsDeleteClient_GetDeletedFileListCompleted(object sender, GetDeletedFileListCompletedEventArgs e)
        {
            if (e.Error != null || e.Result == null)
            {
                MessageBox.Show("載入資料錯誤!", "訊息", MessageBoxButton.OK);
                return;
            }
            else if (e.Result.Count == 0)
            {
                MessageBox.Show("查無資料!", "訊息", MessageBoxButton.OK);

                RadPane openedPane = (RadPane)parentFrame.PaneGroup.SelectedItem;
                this.parentFrame.PaneGroup.RemovePane(openedPane);
                this.parentFrame.PaneGroup.SelectedIndex = this.parentFrame.PaneGroup.Items.Count - 1;
                return;
            }

            this.TextBlock_TotalCount.Text = "有 " + e.Result.Count + " 項結果";
            
            PagedCollectionView pcb = new PagedCollectionView(e.Result);
            this.ListBox_Result.ItemsSource = pcb;
            this.DataPager.Source = pcb;

            this.RadBusyIndicator.IsBusy = false;
        }

        private void CheckBox_Delete_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            if ((sender as CheckBox).IsChecked == true)
            {
                if ((sender as CheckBox).Tag != null)
                {
                    if (_deleted_file_no_list.IndexOf((sender as CheckBox).Tag.ToString()) < 0)
                    {
                        _deleted_file_no_list.Add((sender as CheckBox).Tag.ToString());
                    }
                }
            }
            else
            {
                this.CheckBox_All.IsChecked = false;
                _deleted_file_no_list.Remove((sender as CheckBox).Tag.ToString());
            }
        }

        private void CheckBox_All_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            if ((sender as CheckBox).IsChecked == true)
            {
                _deleted_file_no_list.Clear();
                _class_deleted_list.Clear();
                for (int i = 0; i <= (this.ListBox_Result.ItemsSource as PagedCollectionView).Count - 1; i++)
                {
                    _deleted_file_no_list.Add(((this.ListBox_Result.ItemsSource as PagedCollectionView)[i] as Class_DELETED_RESULT)._fsfile_no);
                    ((this.ListBox_Result.ItemsSource as PagedCollectionView)[i] as Class_DELETED_RESULT)._fbis_checked = true;
                }
            }
            else
            {
                _deleted_file_no_list.Clear();
                _class_deleted_list.Clear();
                for (int i = 0; i <= (this.ListBox_Result.ItemsSource as PagedCollectionView).Count - 1; i++)
                {
                    ((this.ListBox_Result.ItemsSource as PagedCollectionView)[i] as Class_DELETED_RESULT)._fbis_checked = false;
                }
            }

        }

        private void Button_Delete_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            //TODO: 在此新增事件處理常式執行項目。
            if (MessageBox.Show("確定要刪除?", "訊息", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {
                if (_deleted_file_no_list.Count > 0)
                {
                    this.RadBusyIndicator.IsBusy = true;
                    for (int i = 0; i <= _deleted_file_no_list.Count - 1; i++)
                    {
                        Class_DELETED _class_deleted = new Class_DELETED();
                        _class_deleted._fsfile_no = _deleted_file_no_list[i];
                        _class_deleted._fsfile_type = this._class_deleted_parameter._fsfile_type;
                        _class_deleted._fcstatus = "N";
                        _class_deleted._fscreated_by = UserClass.userData.FSUSER_ID;
                        _class_deleted._fdcreated_date = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                        _class_deleted._fsmemo = "刪除方式：手動刪除";
                        _class_deleted_list.Add(_class_deleted);
                    }

                    wsDeleteClient.DeleteFileAsync(_class_deleted_list);
                }
                else
                {
                    MessageBox.Show("請選擇要刪除的檔案", "訊息", MessageBoxButton.OK);
                }
            }
        }

        private void DataPager_PageIndexChanged(object sender, System.EventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            this.CheckBox_All.IsChecked = false;
            _deleted_file_no_list.Clear();
            _class_deleted_list.Clear();
            for (int i = 0; i <= (this.ListBox_Result.ItemsSource as PagedCollectionView).Count - 1; i++)
            {
                ((this.ListBox_Result.ItemsSource as PagedCollectionView)[i] as Class_DELETED_RESULT)._fbis_checked = false;
            }
        }

        private void Button_View_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            if ((sender as Button).Tag != null)
            {
                MAG801_01 mag801_01 = new MAG801_01((sender as Button).Tag.ToString().Split(';')[1], (sender as Button).Tag.ToString().Split(';')[0]);
                mag801_01.Show();
            }
        }

    }
}
