﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using PTS_MAM3.Web.ENT;
using System.Windows.Data;


namespace PTS_MAM3.MAG
{
    /// <summary>
    /// 用於新增、修改、刪除 國家來源代碼列表的頁面
    /// </summary>
    public partial class MAG901 : Page
    {
        WSTBZ.WSTBZSoapClient myTBZClient = new WSTBZ.WSTBZSoapClient();
        public MAG901()
        {
            InitializeComponent();
            DelegateCompleteEvent();
            RefreshDataGrid();
        }

        /// <summary>
        /// 設定所有完成處理事件的委派動作
        /// </summary>
        private void DelegateCompleteEvent()
        {
            myTBZClient.Q_TBZPROGNATIONCompleted += (sTBZ, argsTBZ) =>
                { dataGrid1.ItemsSource = argsTBZ.Result; };
            myTBZClient.D_TBZPROGNATIONCompleted += (sD, argsD) =>
                { 
                    RefreshDataGrid();
                    MessageBox.Show("刪除完成");
                };
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void RefreshDataGrid()
        {
            myTBZClient.Q_TBZPROGNATIONAsync();    
        }



        private void btnUserDel_Click(object sender, RoutedEventArgs e)
        {
            if (dataGrid1.SelectedIndex != -1)
            {
                myTBZClient.D_TBZPROGNATIONAsync((WSTBZ.Class_TBZPROGNAION)dataGrid1.SelectedItem, UserClass.userData.FSUSER_ID);
            }
            else
                MessageBox.Show("請選擇欲刪除的代碼!");
            
        }

        private void btnClickUserAdd(object sender, RoutedEventArgs e)
        {
            MAG901_01 MAG901_01 = new MAG901_01();
            MAG901_01.Show();
            MAG901_01.Closing += (s, args) =>
                { RefreshDataGrid(); };
        }

        private void btnUserModifyClick(object sender, RoutedEventArgs e)
        {
            if (dataGrid1.SelectedIndex != -1)
            {
                MAG901_02 MAG901_02 = new MAG901_02((WSTBZ.Class_TBZPROGNAION)dataGrid1.SelectedItem);
                MAG901_02.Show();
                MAG901_02.Closing += (s, args) =>
                { RefreshDataGrid(); };
            }
            else
            { MessageBox.Show("請選擇欲修改的資料!!"); }
            
        }





        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            RefreshDataGrid();
        }

    }
}
