﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSASPNetSession;
using PTS_MAM3.WCFSearchDataBaseFunction;
using System.Xml;
using System.Xml.Linq;
using PTS_MAM3.WSSearchService;
using PTS_MAM3.Common;

namespace AllSearch
{
    public partial class SearchSynonym : UserControl
    {
        WSASPNetSessionSoapClient ASPSession = new WSASPNetSessionSoapClient();

        

        private PTS_MAM3.MainFrame parentFrame;

       
        public SearchSynonym(PTS_MAM3.MainFrame mainframe)
        {
            InitializeComponent();

            parentFrame = mainframe;

            SearchDataBaseFunctionClient searchDb = new SearchDataBaseFunctionClient();

            
            //List同義詞
            List<SynonymClass> synonymList = new List<SynonymClass>();

            //查詢同義詞組筆數
            searchDb.GetSynonymCompleted += (s, args) =>
            {
                if (args.Error != null)
                {
                    return;
                }

                if (args.Result == null || args.Result =="")
                {
                    return;
                }

                XDocument xmlDocResult = XDocument.Parse(args.Result.ToString());

                string fssynonym = xmlDocResult.Element("Datas").Element("Data").Element("FSSYNONYM").Value;

                if (fssynonym != "")
                {
                    string[] fssynonymList = fssynonym.Split(';');

                    if (fssynonymList != null && fssynonymList.Length > 0)
                    {
                        for (int i = 0; i <= fssynonymList.Length - 1; i++)
                        {
                            if (fssynonymList[i].IndexOf(':') != -1)
                            {
                                SynonymClass sClass = new SynonymClass();
                                sClass.fnno = fssynonymList[i].Split(':')[0];
                                sClass.fssynonym = fssynonymList[i].Split(':')[1];

                                synonymList.Add(sClass);
                            }
                        }

                        this.ListBox_Synonym.ItemsSource = synonymList;
                    }
                }
                

            };

            searchDb.GetSynonymAsync("<Data><FSTEST>TEST</FSTEST></Data>");

        }

        private void ListBox_Synonym_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SynonymClass selectitem = (SynonymClass)this.ListBox_Synonym.SelectedItem;
            SearchDataBaseFunctionClient searchDb = new SearchDataBaseFunctionClient();
            //MessageBox.Show(selectitem.fnno);
            searchDb.GetSynonymByNoCompleted += (s, args) =>
            {
                if (args.Error != null)
                {
                    return;
                }

                if (args.Result == null || args.Result == "")
                {
                    return;
                }

                XDocument xmlDocResult = XDocument.Parse(args.Result.ToString());
                var paras = from para in xmlDocResult.Descendants("Data")
                            select new SynonymClass
                            {
                                fnno = para.Element("FNNO").Value,
                                fssynonym = para.Element("FSSYNONYM").Value,
                            };

                this.ListBox_Synonym_Detail.ItemsSource = paras;
            };

            if (selectitem != null)
            {
                searchDb.GetSynonymByNoAsync("<Data><FNNO>" + selectitem.fnno + "</FNNO></Data>");
            }
            
        }

        private void Button_SearchSynonym_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
            if (this.TextBox_Synonym.Text == "")
            {
                MessageBox.Show("請輸入查詢的同義詞!");
                return;
            }

           
            SearchDataBaseFunctionClient searchDb = new SearchDataBaseFunctionClient();
            List<SynonymClass> synonymList = new List<SynonymClass>();

            searchDb.GetSynonymByWordCompleted += (s, args) =>
            {
                if (args.Error != null)
                {
                    return;
                }

                if (args.Result == null || args.Result == "")
                {
                    return;
                }

                XDocument xmlDocResult = XDocument.Parse(args.Result.ToString());

                string fssynonym = xmlDocResult.Element("Datas").Element("Data").Element("FSSYNONYM").Value;

                if (fssynonym.IndexOf(':')==-1)
                {
                    MessageBox.Show("同義詞不存在!");
                    return;
                }

                string[] fssynonymList = fssynonym.Split(';');

                if (fssynonymList != null && fssynonymList.Length > 0)
                {
                    for (int i = 0; i <= fssynonymList.Length - 1; i++)
                    {
                        if (fssynonymList[i].IndexOf(':') != -1)
                        {
                            SynonymClass sClass = new SynonymClass();
                            sClass.fnno = fssynonymList[i].Split(':')[0];
                            sClass.fssynonym = fssynonymList[i].Split(':')[1];

                            synonymList.Add(sClass);

                        }
                        
                    }
                }

                this.ListBox_Synonym.ItemsSource = synonymList;
                this.ListBox_Synonym_Detail.ItemsSource = null;
                
            };

            searchDb.GetSynonymByWordAsync("<Data><FSSYNONYM>" + this.TextBox_Synonym.Text + "</FSSYNONYM></Data>");
        }

        //新增同義詞組
        private void Button_SynonymWordList_Click(object sender, RoutedEventArgs e)
        {
            
            if (this.TextBox_Synonym.Text == "")
            {
                MessageBox.Show("請輸入新增的同義詞!");
                return;
            }

            //if (this.TextBox_Synonym.Text.IndexOf(" ") != -1)
            //{
            //    MessageBox.Show("同義詞不可有空白!");
            //    return;
            //}

            SearchDataBaseFunctionClient searchDb = new SearchDataBaseFunctionClient();
            //檢查是否重複新增
            searchDb.CheckSynonymExistCompleted += (s, args) =>
            {
                if (args.Error != null)
                {
                    return;
                }

                if (args.Result == null || args.Result == "")
                {
                    return;
                }

                XDocument xmlDocResult = XDocument.Parse(args.Result.ToString());

                int fnno_count = Int32.Parse(xmlDocResult.Element("Datas").Element("Data").Element("FNNO_COUNT").Value);
                if (fnno_count > 0)
                {
                    MessageBox.Show("同義詞已存在!");
                    return;
                }
                
                //新增至資料庫
                SearchDataBaseFunctionClient searchDb1 = new SearchDataBaseFunctionClient();
                searchDb1.InsertSynonymListCompleted += (s1, args1) =>
                {
                    if (args1.Error != null)
                    {
                        return;
                    }

                    RefreshListBoxSynonym();
                    this.TextBox_Synonym.Text = "";
                    MessageBox.Show("同義詞組新增成功!");
                };

                string parameter = "<Data><FNNO></FNNO><FSSYNONYM>" + this.TextBox_Synonym.Text + "</FSSYNONYM></Data>";
                searchDb1.InsertSynonymListAsync(parameter,UserClass.userData.FSUSER_ID);

            };

            searchDb.CheckSynonymExistAsync("<Data><FSSYNONYM>" + this.TextBox_Synonym.Text + "</FSSYNONYM></Data>");

        }


        //新增同義詞彙
        private void Button_SynonymWord_Click(object sender, RoutedEventArgs e)
        {
            if (this.TextBox_Synonym.Text == "")
            {
                MessageBox.Show("請輸入新增的同義詞!");
                return;
            }

            //if (this.TextBox_Synonym.Text.IndexOf(" ") != -1)
            //{
            //    MessageBox.Show("同義詞不可有空白!");
            //    return;
            //}

            SearchDataBaseFunctionClient searchDb = new SearchDataBaseFunctionClient();
            //檢查是否重複新增
            searchDb.CheckSynonymExistCompleted += (s, args) =>
            {
                if (args.Error != null)
                {
                    return;
                }

                if (args.Result == null || args.Result == "")
                {
                    return;
                }

                XDocument xmlDocResult = XDocument.Parse(args.Result.ToString());

                int fnno_count = Int32.Parse(xmlDocResult.Element("Datas").Element("Data").Element("FNNO_COUNT").Value);
                if (fnno_count > 0)
                {
                    MessageBox.Show("同義詞已存在!");
                    return;
                }

                
                if (this.ListBox_Synonym_Detail.Items.Count() > 0)
                {
                    SynonymClass synonymClass = new SynonymClass();
                    synonymClass = (SynonymClass)this.ListBox_Synonym_Detail.Items[0];

                    //新增至資料庫
                    SearchDataBaseFunctionClient searchDb1 = new SearchDataBaseFunctionClient();
                    searchDb1.InsertSynonymListCompleted += (s1, args1) =>
                    {
                        if (args1.Error != null)
                        {
                            return;
                        }

                        //取得同義詞組所有詞
                        SearchDataBaseFunctionClient searchDb2 = new SearchDataBaseFunctionClient();
                        searchDb2.GetSynonymByNoCompleted += (s2, args2) =>
                        {
                            if (args2.Error != null)
                            {
                                return;
                            }

                            if (args2.Result == null || args2.Result == "")
                            {
                                return;
                            }

                            XDocument xmlDocResult2 = XDocument.Parse(args2.Result.ToString());
                            var paras = from para in xmlDocResult2.Descendants("Data")
                                        select new SynonymClass
                                        {
                                            fnno = para.Element("FNNO").Value,
                                            fssynonym = para.Element("FSSYNONYM").Value,
                                        };

                            if (paras != null && paras.Count() > 0)
                            {
                                string synonymList = "";
                                foreach (SynonymClass obj in paras)
                                {
                                    synonymList += obj.fssynonym + ";";
                                }

                                if (synonymList != "")
                                {
                                    synonymList = synonymList.Substring(0, synonymList.Length - 1);
                                }
                                //SearchServiceClient searchWS = new SearchServiceClient();
                                SearchServiceSoapClient searchWS = new SearchServiceSoapClient();
                                searchWS.AddSynonymCompleted += (s3,args3) =>
                                {
                                    if (args3.Error != null)
                                    {
                                        return;
                                    }

                                    if (args3.Result == null || args3.Result == "")
                                    {
                                        return;
                                    }


                                    RefreshListBoxSynonym();
                                    MessageBox.Show(args3.Result);
                                    this.TextBox_Synonym.Text = "";
                                    this.ListBox_Synonym_Detail.ItemsSource = null;
                                };

                                searchWS.AddSynonymAsync("<Search><IniPath>C:\\Program Files (x86)\\Tornado\\Tornado Search 5 SDK\\Config\\TS50.ini</IniPath><Synonym>"+synonymList+"</Synonym></Search>");

                            }

                        };


                        searchDb2.GetSynonymByNoAsync("<Data><FNNO>" + synonymClass.fnno+ "</FNNO></Data>");
                    };

                    string insertSynonymParameter = "<Data><FNNO>" + synonymClass .fnno+ "</FNNO><FSSYNONYM>" + this.TextBox_Synonym.Text + "</FSSYNONYM></Data>";
                    searchDb1.InsertSynonymListAsync(insertSynonymParameter, UserClass.userData.FSUSER_ID);

                    //MessageBox.Show(synonymClass.fnno);
                }
                else
                {
                    MessageBox.Show("請先選擇要加入的詞組!");
                }
            };

            searchDb.CheckSynonymExistAsync("<Data><FSSYNONYM>" + this.TextBox_Synonym.Text + "</FSSYNONYM></Data>");
        }



        private void RefreshListBoxSynonym()
        {
            //List同義詞
            List<SynonymClass> synonymList = new List<SynonymClass>();
            //重新整理同義詞組
            SearchDataBaseFunctionClient searchDb = new SearchDataBaseFunctionClient();
            searchDb.GetSynonymCompleted += (s, args) =>
            {
                if (args.Error != null)
                {
                    return;
                }

                if (args.Result == null || args.Result == "")
                {
                    return;
                }

                XDocument xmlDocResult = XDocument.Parse(args.Result.ToString());

                string fssynonym = xmlDocResult.Element("Datas").Element("Data").Element("FSSYNONYM").Value;

                string[] fssynonymList = fssynonym.Split(';');

                if (fssynonymList != null && fssynonymList.Length > 0)
                {
                    for (int i = 0; i <= fssynonymList.Length - 1; i++)
                    {
                        if (fssynonymList[i].IndexOf(':') != -1)
                        {
                            SynonymClass sClass = new SynonymClass();
                            sClass.fnno = fssynonymList[i].Split(':')[0];
                            sClass.fssynonym = fssynonymList[i].Split(':')[1];

                            synonymList.Add(sClass);

                        }
                    }
                }


                this.ListBox_Synonym.ItemsSource = synonymList;
            };

            searchDb.GetSynonymAsync("<Data><FSTEST>TEST</FSTEST></Data>");
        }

        //刪除同義詞組
        private void Button_DeleteSynonymList_Click(object sender, RoutedEventArgs e)
        {
            
            if (this.ListBox_Synonym_Detail.Items.Count() <= 0)
            {
                MessageBox.Show("請先選擇要刪除的詞組!");
                return;
            }

            string synonymList = "";
            for (int i = 0; i <= this.ListBox_Synonym_Detail.Items.Count() - 1; i++)
            {
                SynonymClass synonymClass = new SynonymClass();
                synonymClass = (SynonymClass)this.ListBox_Synonym_Detail.Items[i];
                synonymList += synonymClass.fssynonym + ";";
            }

            if (synonymList != "")
            {
                synonymList = synonymList.Substring(0, synonymList.Length - 1);
            }

            chWndMessageBox confirm = new chWndMessageBox("刪除同義詞組", "確定要刪除此同義詞組?", 0);
            confirm.Show();

            confirm.Closing += (s, args) =>
            {
                if (confirm.DialogResult == true)
                {
                    SynonymClass synonymClass = new SynonymClass();
                    synonymClass = (SynonymClass)this.ListBox_Synonym_Detail.Items[0];
                    //刪除資料庫
                    SearchDataBaseFunctionClient searchDb1 = new SearchDataBaseFunctionClient();
                    searchDb1.DeleteSynonymListCompleted += (s1, args1) =>
                    {
                        if (args1.Error != null)
                        {
                            return;
                        }

                        //刪除同義詞庫
                        //SearchServiceClient searchWS = new SearchServiceClient();
                        SearchServiceSoapClient searchWS = new SearchServiceSoapClient();
                        searchWS.DeleteSynonymCompleted += (s3, args3) =>
                        {
                            if (args3.Error != null)
                            {
                                return;
                            }

                            if (args3.Result == null || args3.Result == "")
                            {
                                return;
                            }

                            RefreshListBoxSynonym();
                            MessageBox.Show(args3.Result);
                            this.TextBox_Synonym.Text = "";
                            this.ListBox_Synonym_Detail.ItemsSource = null;
                        };

                        searchWS.DeleteSynonymAsync("<Search><IniPath>C:\\Program Files (x86)\\Tornado\\Tornado Search 5 SDK\\Config\\TS50.ini</IniPath><Synonym>" + synonymList + "</Synonym></Search>");

                    };


                    searchDb1.DeleteSynonymListAsync("<Data><FNNO>" + synonymClass.fnno + "</FNNO><FSSYNONYM></FSSYNONYM></Data>", UserClass.userData.FSUSER_ID);
                    //MessageBox.Show("確定!");
                }
            };

        }

        //刪除同義詞彙
        private void Button_DeleteSynonym_Click(object sender, RoutedEventArgs e)
        {
            if (this.ListBox_Synonym_Detail.Items.Count() <= 0)
            {
                MessageBox.Show("請先選擇要刪除的詞組!");
                return;
            }

            if (this.ListBox_Synonym_Detail.SelectedItem == null)
            {
                MessageBox.Show("請先選擇要刪除的詞彙!");
                return;
            }

            chWndMessageBox confirm = new chWndMessageBox("刪除同義詞彙", "確定要刪除此同義詞彙?", 0);
            confirm.Show();

            confirm.Closing += (s, args) =>
            {
                if (confirm.DialogResult == true)
                {
                    SynonymClass synonymClass = new SynonymClass();
                    synonymClass = (SynonymClass)this.ListBox_Synonym_Detail.SelectedItem;
                    //刪除資料庫
                    SearchDataBaseFunctionClient searchDb1 = new SearchDataBaseFunctionClient();
                    searchDb1.DeleteSynonymCompleted += (s1, args1) =>
                    {
                        if (args1.Error != null)
                        {
                            return;
                        }

                        //刪除同義詞庫
                        //SearchServiceClient searchWS = new SearchServiceClient();
                        SearchServiceSoapClient searchWS = new SearchServiceSoapClient();
                        searchWS.DeleteSynonymCompleted += (s3, args3) =>
                        {
                            if (args3.Error != null)
                            {
                                return;
                            }

                            if (args3.Result == null || args3.Result == "")
                            {
                                return;
                            }

                            RefreshListBoxSynonym();
                            MessageBox.Show(args3.Result);
                            this.TextBox_Synonym.Text = "";
                            this.ListBox_Synonym_Detail.ItemsSource = null;
                        };

                        searchWS.DeleteSynonymAsync("<Search><IniPath>C:\\Program Files (x86)\\Tornado\\Tornado Search 5 SDK\\Config\\TS50.ini</IniPath><Synonym>" + synonymClass.fssynonym + "</Synonym></Search>");

                    };

                    searchDb1.DeleteSynonymAsync("<Data><FNNO>" + synonymClass.fnno + "</FNNO><FSSYNONYM>" + synonymClass.fssynonym + "</FSSYNONYM></Data>", UserClass.userData.FSUSER_ID);
                }
            };
        }

    }

    public class SynonymClass
    {
        public string fnno { set; get; }
        public string fssynonym { set; get; }
    }
}
