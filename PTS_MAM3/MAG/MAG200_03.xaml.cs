﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace PTS_MAM3.SystemAdmin
{
    public partial class UserAdmin_Query : ChildWindow
    {
        public UserAdmin_Query()
        {
            InitializeComponent();
        }


        public ObservableCollection<WSUserAdmin.UserStruct> _UserList = new ObservableCollection<WSUserAdmin.UserStruct>();
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            WSUserAdmin.UserStruct user = new WSUserAdmin.UserStruct(); 
            
            if ((string)((ComboBoxItem)(cbxFCActive.SelectedItem)).Content == "Y")                        
                user.FCACTIVE = "1";                        
            else
                user.FCACTIVE = "0";


            user.FSUSER_ID = tbxFSUSER_ID.Text.Trim();
            user.FSUSER = tbxFSUSER.Text.Trim();
            user.FSUSER_ChtName = this.tbxFSUSER_ChtName.Text.Trim();
            user.FSGROUP_CHTNAME = tbxFSDEPT.Text.Trim();
            user.FSUSER_TITLE = tbxFSUSER_TITLE.Text.Trim();
            user.FSDESCRIPTION = tbxFSDESCRIPTION.Text.Trim();


            WSUserAdmin.WSUserAdminSoapClient UserQryObj = new WSUserAdmin.WSUserAdminSoapClient();

            //UserQryObj.GetUserListByQueryCompleted += (s, args) =>
            //{
            //    if (args.Error == null)
            //    {
            //        if (args.Result != null)
            //        {
            //            _UserList = (ObservableCollection <WSUserAdmin.UserStruct>) args.Result;
                        
            //        }
            //        else
            //        {
            //            _UserList = null;
                        
            //        }                   
            //    }
            //    this.DialogResult = true;
            //};
            //UserQryObj.GetUserListByQueryAsync(user);

            UserQryObj.GetUserListBySearchCondictionCompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (args.Result != null)
                        {
                            _UserList = (ObservableCollection<WSUserAdmin.UserStruct>)args.Result;
                            //將部門ID加入
                            foreach (var item in _UserList)
                            {
                                if (item.FNGROUP_ID != 0)
                                { 
                                    item.FNTreeViewDept_ID = item.FNGROUP_ID;
                                }
                                else
                                { item.FNTreeViewDept_ID = item.FNDEP_ID; }
                            }
                        }
                        else
                        {
                            _UserList = null;

                        }
                    }
                    this.DialogResult = true;
                };
            UserQryObj.GetUserListBySearchCondictionAsync(user);
            //this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

