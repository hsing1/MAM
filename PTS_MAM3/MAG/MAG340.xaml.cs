﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Windows.Data;

namespace PTS_MAM3
{
    public partial class MAG340 : Page
    {
        SR_WSPGM_MSSProcess_Management.WSPGM_MSSProcess_ManagementSoapClient
    WSPGM_MSSProcessClient = new SR_WSPGM_MSSProcess_Management.WSPGM_MSSProcess_ManagementSoapClient();

        public MAG340()
        {
            InitializeComponent();
            WSPGM_MSSProcessClient.Get_LOG_VIDEO_SEG_CHG_LIST_ADVANCECompleted += new EventHandler<SR_WSPGM_MSSProcess_Management.Get_LOG_VIDEO_SEG_CHG_LIST_ADVANCECompletedEventArgs>(WSPGM_MSSProcessClient_Get_LOG_VIDEO_SEG_CHG_LIST_ADVANCECompleted);

            //獲取段落置換記錄,預設顯示為"節目"、"一個月內"的微調紀錄
            WSPGM_MSSProcessClient.Get_LOG_VIDEO_SEG_CHG_LIST_ADVANCEAsync
            ("G", "", "", DateTime.Now.AddMonths(-1).ToShortDateString(), DateTime.Now.AddDays(+1).ToShortDateString(), "");

            this.BusyIndicator1.IsBusy = true;
        }

        //獲取段落置換記錄結果
        void WSPGM_MSSProcessClient_Get_LOG_VIDEO_SEG_CHG_LIST_ADVANCECompleted(object sender, SR_WSPGM_MSSProcess_Management.Get_LOG_VIDEO_SEG_CHG_LIST_ADVANCECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result.Count > 0)
                {
                    PagedCollectionView pcv = new PagedCollectionView(e.Result);
                    this.DataGrid1.ItemsSource = pcv;
                    this.DataGrid1.SelectedIndex = 0;
                    this.DataPager1.Source = pcv;
                }
                this.BusyIndicator1.IsBusy = false;
            }
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        //按下查詢按鈕後
        private void QueryButton_Click(object sender, RoutedEventArgs e)
        {
            //開啟查詢頁面
            MAG340_01 queryWindow = new MAG340_01();
            queryWindow.Show();
            this.BusyIndicator1.IsBusy = true;
            queryWindow.Closed += (s, args) =>
            {
                if (queryWindow.DialogResult == true)
                {
                    PagedCollectionView pcv = new PagedCollectionView(queryWindow.queryResult);
                    if (pcv.Count == 0)
                    {
                        MessageBox.Show("查無微調Time Code記錄，請重新輸入條件查詢！", "提示訊息", MessageBoxButton.OK);
                    }
                    this.DataGrid1.ItemsSource = pcv;
                    this.DataGrid1.SelectedIndex = 0;
                    this.DataPager1.Source = pcv;
                    
                }
                this.BusyIndicator1.IsBusy = false;
            };
        }

    }
}
