﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.MAG
{
    public partial class MAG320_02 : ChildWindow
    {
        public MAG320_02()
        {
            InitializeComponent();
        }

        private void btnAddVTR_Click(object sender, RoutedEventArgs e)
        {
            char isEnable = 'N';
            char isLock = 'N';

            if (string.IsNullOrEmpty(textBox1.Text))
            {
                MessageBox.Show("請輸入欲新增的VTR編號");
                return;
            }

            if (radioButton1.IsChecked == true || radioButton2.IsChecked == true)
            {
                if (radioButton1.IsChecked == true)
                    isEnable = 'Y';
            }
            else
            {
                MessageBox.Show("請選擇是否啟用");
                return;
            }

            if (radioButton3.IsChecked == true || radioButton4.IsChecked == true)
            {
                if (radioButton3.IsChecked == true)
                    isLock = 'Y';
            }
            else
            {
                MessageBox.Show("請選擇是否鎖定");
                return;
            }

            SR_ServiceTranscode.ServiceTranscodeSoapClient obj = new SR_ServiceTranscode.ServiceTranscodeSoapClient();
            obj.AddVTRCompleted += new EventHandler<SR_ServiceTranscode.AddVTRCompletedEventArgs>(obj_AddVTRCompleted);
            obj.AddVTRAsync(textBox1.Text, isEnable, isLock, textBox2.Text);
        }

        private void obj_AddVTRCompleted(object sender, SR_ServiceTranscode.AddVTRCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    MessageBox.Show("新增作業已完成");
                    textBox1.Text = string.Empty;
                    textBox2.Text = string.Empty;
                }
                else
                    MessageBox.Show("新增作業時發現錯誤，請查詢輸入的參數是否正確");
            }
            else
                MessageBox.Show("連結主機服務時發生問題：" + e.Error.ToString());
        }
    }
}
