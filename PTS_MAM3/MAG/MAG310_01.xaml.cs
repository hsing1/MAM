﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.MAG
{
    public partial class MAG310_01 : ChildWindow
    {
        private SR_WSPGM_MSSProcess_Management.ClassMSSProcessJob m_FormData;


        public MAG310_01(SR_WSPGM_MSSProcess_Management.ClassMSSProcessJob FormData)
        {
            InitializeComponent();

            // 從父視窗傳進來的參數
            this.m_FormData = FormData;

            // 找出對應的 ansID
            if (Int32.Parse(this.m_FormData.TranscodeJobID) > 0)
            {
                // 如果有 TranscodeID，就從 Transcode 服務找出相對應的 ansID (有可能還在 TSMRecall，就會得到 -1)
                SR_ServiceTranscode.ServiceTranscodeSoapClient objServiceTranscode = new SR_ServiceTranscode.ServiceTranscodeSoapClient();
                objServiceTranscode.GetOneTranscodeJobInformationCompleted += new EventHandler<SR_ServiceTranscode.GetOneTranscodeJobInformationCompletedEventArgs>(objServiceTranscode_GetOneTranscodeJobInformationCompleted);
                objServiceTranscode.GetOneTranscodeJobInformationAsync(this.m_FormData.TranscodeJobID);
            }
            else
            {
                // 其他狀態就直接查詢 MSSProcess，用 VideoID 找出最後一筆的對應 ansID
                SR_WSPGM_MSSProcess_Management.WSPGM_MSSProcess_ManagementSoapClient objPGM_MSSProcess_Management = new SR_WSPGM_MSSProcess_Management.WSPGM_MSSProcess_ManagementSoapClient();
                objPGM_MSSProcess_Management.GetLastestAnsJobCompleted += new EventHandler<SR_WSPGM_MSSProcess_Management.GetLastestAnsJobCompletedEventArgs>(objPGM_MSSProcess_Management_GetLastestAnsJobCompleted);
                objPGM_MSSProcess_Management.GetLastestAnsJobAsync(this.m_FormData.VideoID);
            }
        }

        private void objPGM_MSSProcess_Management_GetLastestAnsJobCompleted(object sender, SR_WSPGM_MSSProcess_Management.GetLastestAnsJobCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                // 這邊回傳的資料很有機會是 Null
                if (e.Result == null)
                {
                    fnConvertClassToForm("-1");
                }
                else
                {
                    // 將參數拆解到對應的元件中
                    fnConvertClassToForm(e.Result.AnsJobID);
                }

                // 詢問主機是否是今日，如果回傳 True 的話就將「重啟任務」的按鈕啟用
                WSMAMFunctions.WSMAMFunctionsSoapClient objSR = new WSMAMFunctions.WSMAMFunctionsSoapClient();
                objSR.GetServerDateTimeCompleted += new EventHandler<WSMAMFunctions.GetServerDateTimeCompletedEventArgs>(objSR_GetServerDateTimeCompleted1);
                objSR.GetServerDateTimeAsync();
            }
            else
            {
                MessageBox.Show(string.Concat("查詢轉檔詳細內容發生錯誤：", e.Error.ToString()));
            }
        }

        private void objServiceTranscode_GetOneTranscodeJobInformationCompleted(object sender, SR_ServiceTranscode.GetOneTranscodeJobInformationCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                // 這邊回傳的資料很有可能是 Null
                if (e.Result == null)
                {
                    fnConvertClassToForm("-1");
                }
                else
                {
                    // 將參數拆解到對應的元件中
                    fnConvertClassToForm(e.Result.AnsJobID);
                }

                // 詢問主機是否是今日，如果回傳 True 的話就將「重啟任務」的按鈕啟用
                WSMAMFunctions.WSMAMFunctionsSoapClient objSR = new WSMAMFunctions.WSMAMFunctionsSoapClient();
                objSR.GetServerDateTimeCompleted += new EventHandler<WSMAMFunctions.GetServerDateTimeCompletedEventArgs>(objSR_GetServerDateTimeCompleted1);
                objSR.GetServerDateTimeAsync();
            }
            else
            {
                MessageBox.Show(string.Concat("查詢轉檔詳細內容發生錯誤：", e.Error.ToString()));
            }
        }

        private void btnRestartJob_Click(object sender, RoutedEventArgs e)
        {
            // 先查詢主機時間，如果時間相符才允許進到下一層
            WSMAMFunctions.WSMAMFunctionsSoapClient objSR = new WSMAMFunctions.WSMAMFunctionsSoapClient();
            objSR.GetServerDateTimeCompleted += new EventHandler<WSMAMFunctions.GetServerDateTimeCompletedEventArgs>(objSR_GetServerDateTimeCompleted2);
            objSR.GetServerDateTimeAsync();
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void btnDetailTranscode_Click(object sender, RoutedEventArgs e)
        {
            // 呼叫 Web Service 以取得轉檔進度的物件，並在 Complete 事件中，將該物件放到 Child Window 中做呈現
        }

        // 將傳入的Class屬性帶到GUI裡面
        private void fnConvertClassToForm(string ansJobID)
        {
            lbl11.Content = m_FormData.MSSProcessJobID;
            lbl12.Content = m_FormData.MSSProcessJobStatusC;
            lbl13.Content = m_FormData.VideoID;

            if (m_FormData.TranscodeJobID == "-1")
            {
                m_FormData.TranscodeJobID = "N/A";
                btnDetailTranscode.IsEnabled = false;
            }
            else
            {
                // 不用管
            }

            // 2012-12-05：加入 ANYSTREAM_ID 的顯示
            if (ansJobID == "-1")
                lbl14.Content = string.Format("{0} (ANS#未派送)", m_FormData.TranscodeJobID);
            else if (ansJobID == "-10")
                lbl14.Content = string.Format("{0} (ANS#已清除)", m_FormData.TranscodeJobID);
            else
                lbl14.Content = string.Format("{0} (ANS#{1})", m_FormData.TranscodeJobID, ansJobID);

            if (m_FormData.TsmJobID == "-1")
            {
                lbl15.Content = "N/A";
                btnDetailTsmJob.IsEnabled = false;
            }
            else
                lbl15.Content = m_FormData.TsmJobID;

            if (m_FormData.MSSCopyJobID == "-1")
            {
                lbl16.Content = "N/A";
                btnDetailMsscopy.IsEnabled = false;
            }
            else
                lbl16.Content = m_FormData.MSSCopyJobID;
        }

        // 如果任務編號與主機日期相符，就啟用「重啟任務」的按扭
        private void objSR_GetServerDateTimeCompleted1(object sender, WSMAMFunctions.GetServerDateTimeCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (m_FormData.MSSProcessJobID.StartsWith(string.Concat(e.Result.Year, e.Result.Month.ToString().PadLeft(2, '0'), e.Result.Day.ToString().PadLeft(2, '0'))))
                    btnRestartJob.IsEnabled = true;
            }
            else
                MessageBox.Show(string.Concat("查詢主機時間發生錯誤：", e.Error.ToString()));
        }

        // 如果任務編號與主機日期相符，才允許使用者進到下一層作業
        private void objSR_GetServerDateTimeCompleted2(object sender, WSMAMFunctions.GetServerDateTimeCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (m_FormData.MSSProcessJobID.StartsWith(string.Concat(e.Result.Year, e.Result.Month.ToString().PadLeft(2, '0'), e.Result.Day.ToString().PadLeft(2, '0'))))
                {
                    // 因為還有選項需要讓使用者挑選，所以只好再開一個 Child Window
                    MAG310_01_01 MAG310_01_01_frm = new MAG310_01_01(m_FormData);
                    MAG310_01_01_frm.Show();
                    MAG310_01_01_frm.Closing += (s, args) =>
                    {
                        if (MAG310_01_01_frm.DialogResult == true)
                        {
                            // 如果上層的關掉了，這層的也一起關掉吧
                            this.DialogResult = true;
                        }
                    };
                }
                else
                    MessageBox.Show("不允許操作非當日的任務");
            }
            else
                MessageBox.Show(string.Concat("查詢主機時間發生錯誤：", e.Error.ToString()));
        }
    }
}