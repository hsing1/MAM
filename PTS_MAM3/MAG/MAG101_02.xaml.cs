﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.Web.ENT;

namespace PTS_MAM3.MAG
{
    public partial class MAG101_02 : ChildWindow
    {
        public Object senderObj;
        //public TBZPROGBUYD backObj;
        private ENT100DomainContext content = new ENT100DomainContext();
        string tbName;
        public MAG101_02(string InputtbName, Object sender)
        {
            InitializeComponent();
            senderObj = sender;
            tbName = InputtbName;
            InitializeComponent();
            //FillBuyNameIntoCombox1();
            FillField();
        }

        private void FillField()
        {
            switch (tbName)
            {
                case "TBZPROMOCATD":
                    textBox1.Text = ((TBZPROMOCATDJoinClass)senderObj).FSPROMOCATDNAME;
                    textBox2.Text = ((TBZPROMOCATDJoinClass)senderObj).FSSORT;
                    checkBox1.IsChecked = ((TBZPROMOCATDJoinClass)senderObj).FBISENABLE;
                    break;
                case "TBZPROGBUYD":
                    textBox1.Text = ((TBJoinClass)senderObj).FSPROGBUYDNAME;
                    textBox2.Text = ((TBJoinClass)senderObj).FSSORT;
                    checkBox1.IsChecked = ((TBJoinClass)senderObj).FBISENABLE;
                    break;
            }

            //textBox3.Text = ((TBJoinClass)senderObj).FSPROGBUYNAME;
            //textBox3.IsReadOnly = true;

        }
        private void FillBuyNameIntoCombox1()
        {
            //var BuyTBEntity = content.Load(content.GetTBZPROGBUYQuery());
            //BuyTBEntity.Completed += (s, args) =>
            //{
            //    textBox1.Text = ((TBJoinClass)senderObj).FSPROGBUYDNAME;
            //    textBox2.Text = ((TBJoinClass)senderObj).FSSORT;
            //    foreach (TBZPROGBUY x in BuyTBEntity.Entities)
            //    {
            //        ComboBoxItem CBI = new ComboBoxItem();
            //        CBI.Content = x.FSPROGBUYNAME;
            //        CBI.Tag = x.FSPROGBUYID;
            //        comboBox1.Items.Add(CBI);
            //        for (int i = 0; i < comboBox1.Items.Count; i++)
            //        {
            //            ComboBoxItem getItem = (ComboBoxItem)comboBox1.Items.ElementAt(i);
            //            if (getItem.Tag.Equals(((TBJoinClass)senderObj).FSPROGBUYID))
            //            {
            //                comboBox1.SelectedIndex = i;
            //                break;
            //            }
            //        }
            //    }
            //};
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            switch (tbName)
            {
                case "TBZPROMOCATD":
                    ((TBZPROMOCATDJoinClass)senderObj).FSPROMOCATDNAME = textBox1.Text;
                    ((TBZPROMOCATDJoinClass)senderObj).FSSORT = textBox2.Text;
                    ((TBZPROMOCATDJoinClass)senderObj).FDUPDATED_DATE = DateTime.Now;
                    ((TBZPROMOCATDJoinClass)senderObj).FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                    ((TBZPROMOCATDJoinClass)senderObj).FBISENABLE = (bool)checkBox1.IsChecked;
                    this.DialogResult = true;
                    break;
                case "TBZPROGBUYD":
                    ((TBJoinClass)senderObj).FSPROGBUYDNAME = textBox1.Text;
                    ((TBJoinClass)senderObj).FSSORT = textBox2.Text;
                    ((TBJoinClass)senderObj).FDUPDATED_DATE = DateTime.Now;
                    ((TBJoinClass)senderObj).FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                    ((TBJoinClass)senderObj).FBISENABLE = (bool)checkBox1.IsChecked;
                    this.DialogResult = true;
                    break;
            }
            //((TBJoinClass)senderObj).FSPROGBUYID = ((ComboBoxItem)(comboBox1.SelectedItem)).Tag.ToString();

        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

