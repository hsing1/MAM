﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.Web;

namespace PTS_MAM3.MAG
{
    public partial class MAG300_01 : ChildWindow
    {
        // 2011-09-19：因新增了「主控QC清單」，會與現有的「重新轉檔」功能衝突，而造成系統流程的漏洞，所以把這個功能關掉

        private System.Windows.Threading.DispatcherTimer sysTimer = new System.Windows.Threading.DispatcherTimer();
        private SR_ServiceTranscode.ClassTranscodeJob m_FormData;
        
        // 從母畫面傳來的物件參數
        public MAG300_01(SR_ServiceTranscode.ClassTranscodeJob FormData)
        {
            InitializeComponent();
            // 
            m_FormData = FormData;
            fnConvertClassToForm();
            // 
            sysTimer.Interval = new TimeSpan(0, 0, 30);      // 時分秒
            sysTimer.Tick += new EventHandler(sysTimer_Tick);
            sysTimer.Start();
        }

        private void ChildWindow_Closed(object sender, EventArgs e)
        {
            sysTimer.Stop();
        }

        private void sysTimer_Tick(object sender, EventArgs e)
        {
            // 停用所有按鈕後，重新讀取轉檔進度
            fnDisableButtons();
            SR_ServiceTranscode.ServiceTranscodeSoapClient obj = new SR_ServiceTranscode.ServiceTranscodeSoapClient();
            obj.GetOneTranscodeJobInformationCompleted += new EventHandler<SR_ServiceTranscode.GetOneTranscodeJobInformationCompletedEventArgs>(obj_GetOneTranscodeJobInformationCompleted);
            obj.GetOneTranscodeJobInformationAsync(m_FormData.TranscodeJobID);
        }

        private void btnRestartJob_Click(object sender, RoutedEventArgs e)
        {
            SR_ServiceTranscode.ServiceTranscodeSoapClient objTranscode = new SR_ServiceTranscode.ServiceTranscodeSoapClient();
            objTranscode.RedoTranscodeCompleted += new EventHandler<SR_ServiceTranscode.RedoTranscodeCompletedEventArgs>(objTranscode_RedoTranscodeCompleted);
            objTranscode.RedoTranscodeAsync(Int64.Parse(m_FormData.TranscodeJobID));
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void btnChangePriority_Click(object sender, RoutedEventArgs e)
        {
            // 檢查輸入的參數
            string tmpInputVar = textBox1.Text;
            int priorityValue;
            if (!Int32.TryParse(tmpInputVar, out priorityValue) || priorityValue < 1 || priorityValue > 100)
            {
                MessageBox.Show("優先權參數僅接受 1 - 100 的整數值。");
                textBox1.Text = m_FormData.AnsJobPriority;
                return;
            }
            // 呼叫 WS 
            SR_ServiceTranscode.ServiceTranscodeSoapClient objTranscode = new SR_ServiceTranscode.ServiceTranscodeSoapClient();
            objTranscode.ChangeTranscodeJobPriorityCompleted += new EventHandler<SR_ServiceTranscode.ChangeTranscodeJobPriorityCompletedEventArgs>(objTranscode_ChangeTranscodeJobPriorityCompleted);
            objTranscode.ChangeTranscodeJobPriorityAsync(Int64.Parse(m_FormData.TranscodeJobID), priorityValue);
        }

        private void button_GetAnsReturn_Click(object sender, RoutedEventArgs e)
        {
            // 檢查 Anystream 是否有回傳資料
            if (string.IsNullOrEmpty(m_FormData.AnsErrorMessage))
            {
                MessageBox.Show("目前尚未接收到回傳的訊息。");
                return;
            }
            // 
            sysTimer.Stop();
            // 開啟子視窗
            MAG300_01_01 MAG300_01_01_frm = new MAG300_01_01(m_FormData.AnsErrorMessage);
            MAG300_01_01_frm.Title = "顯示轉檔結果";
            MAG300_01_01_frm.Show();
            MAG300_01_01_frm.Closing += (s, args) =>
            {
                sysTimer.Start();
            };
        }

        private void btnGetAnsXml_Click(object sender, RoutedEventArgs e)
        {
            sysTimer.Stop();
            // 開啟子視窗
            MAG300_01_01 MAG300_01_01_frm = new MAG300_01_01(m_FormData.AnsXml);
            MAG300_01_01_frm.Title = "顯示轉檔內容";
            MAG300_01_01_frm.Show();
            MAG300_01_01_frm.Closing += (s, args) =>
            {
                sysTimer.Start();
            };
        }

        private void obj_GetOneTranscodeJobInformationCompleted(object sender, SR_ServiceTranscode.GetOneTranscodeJobInformationCompletedEventArgs e)
        {
            sysTimer.Stop();
            if (e.Error == null)
            {
                if (e.Result == null)
                    MessageBox.Show(string.Concat("暫時無法取得任務'", m_FormData.TranscodeJobID, "'的相關資訊。"));
                else
                {
                    // 取消、存值、更新畫面
                    m_FormData = null;
                    m_FormData = e.Result;
                    fnConvertClassToForm();
                }
            }
            else
                MessageBox.Show("連結主機服務時發生問題：" + e.Error.ToString());
            sysTimer.Start();
            fnEnableButtons();
        }

        private void objTranscode_RedoTranscodeCompleted(object sender, SR_ServiceTranscode.RedoTranscodeCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    MessageBox.Show("已發出重置作業命令");
                    this.DialogResult = true;       // 完成就自動關掉吧
                }
                else
                    MessageBox.Show("發送重置作業命令時發生錯誤。");
            }
            else
                MessageBox.Show("連結主機服務時發生問題：" + e.Error.ToString());
        }

        private void objTranscode_ChangeTranscodeJobPriorityCompleted(object sender, SR_ServiceTranscode.ChangeTranscodeJobPriorityCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == true)
                    MessageBox.Show("轉檔優先權設定已更新。");
                else
                {
                    textBox1.Text = m_FormData.AnsJobPriority;
                    MessageBox.Show("發送重置作業命令時發生錯誤。");
                }
            }
            else
            {
                textBox1.Text = m_FormData.AnsJobPriority;
                MessageBox.Show("連結主機服務時發生問題：" + e.Error.ToString());
            }
        }

        private void fnDisableButtons()
        {
            btnRestartJob.IsEnabled = false;
            btnChangePriority.IsEnabled = false;
            btnGetAnsReturn.IsEnabled = false;
        }

        private void fnEnableButtons()
        {
            btnRestartJob.IsEnabled = true;
            btnChangePriority.IsEnabled = true;
            btnGetAnsReturn.IsEnabled = true;
        }

        /// <summary>
        /// 將傳入的Class屬性帶到GUI裡面
        /// </summary>
        private void fnConvertClassToForm()
        {
            // 
            lbl11.Content = m_FormData.TranscodeJobID;
            lbl12.Content = m_FormData.TranscodeJobStatus;
            // 
            lbl13.Content = m_FormData.TsmJobID;
            lbl14.Content = m_FormData.TsmJobStatus;
            // 
            lbl15.Content = m_FormData.AnsJobID;
            lbl16.Content = m_FormData.AnsJobStatus;
            textBox1.Text = m_FormData.AnsJobPriority;
            
            if (m_FormData.TranscodeStatus == SR_ServiceTranscode.TRANSCODE_STATUS.TSMERROR ||
                m_FormData.TranscodeStatus == SR_ServiceTranscode.TRANSCODE_STATUS.PARERROR ||
                m_FormData.TranscodeStatus == SR_ServiceTranscode.TRANSCODE_STATUS.ANSERROR ||
                m_FormData.TranscodeStatus == SR_ServiceTranscode.TRANSCODE_STATUS.SYSERROR)
            {
                string[] fsNOTEs = m_FormData.Note.Replace("{", "").Replace("}", "").Split(',');
                if (fsNOTEs != null && fsNOTEs.Count() > 0)
                {
                    if (fsNOTEs.FirstOrDefault(s => s.IndexOf("HD送播") > -1) != null)
                        this.btnRestartJob.Visibility = System.Windows.Visibility.Visible;
                    else
                        this.btnRestartJob.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
        }

    }
}
