﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.SystemAdmin
{
    public partial class UserAdmin_Add : ChildWindow
    {
        WSUserAdmin.UserStruct user = new WSUserAdmin.UserStruct(); 

        public UserAdmin_Add()
        {
            InitializeComponent();
        }

        private void showShortMessageBox(string title, Object content) {
            ChildWindow cw = new ChildWindow();
            cw.Title = title ;


            cw.Content = content;
            cw.Show();
            
            cw.MouseLeftButtonDown += (s, args) =>
            {
                cw.Close();
            };
            cw.KeyDown += (s, args) =>
            {
                cw.Close();
            };
            return;
        }

        


        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if ((tbxFSUSER_ID.Text.Trim() == string.Empty) || (tbxFSUSER.Text.Trim() == string.Empty) || 
                (this.tbxFSPASSWD.Password.Trim() == string.Empty) || (this.tbxFSPASSWD_Confirm.Password.Trim() == string.Empty) ||
                (this.tbxFSUSER_TITLE.Text.Trim() == string.Empty) || (this.tbxFSDEPT.Text.Trim() == string.Empty) ||
                (this.tbxFSEMAIL.Text.Trim() == string.Empty)) 
            {              
                showShortMessageBox("請注意",  "請輸入基本必填欄位");
                return;
            }


            if (tbxFSPASSWD.Password != tbxFSPASSWD_Confirm.Password)
            {              
                showShortMessageBox("請注意", "密碼與確認密碼相異，請重新輸入!");
                return;
            }


          


            user.FCACTIVE = "Y";
            user.FCSECRET = "1";
            user.FDCREATED_DATE = DateTime.Now; 

            
            



            this.DialogResult = true;

        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void tbxUserData_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.OKButton.IsEnabled = true;
        }
    }
}

