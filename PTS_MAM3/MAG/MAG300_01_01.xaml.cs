﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.MAG
{
    public partial class MAG300_01_01 : ChildWindow
    {
        // 從母畫面傳來的物件參數
        public MAG300_01_01(string ansXml)
        {
            InitializeComponent();
            // 
            textBox_Result.Text = ansXml;
            textBox_Result.IsReadOnly = true;
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void btnClipBoard_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Clipboard.SetText(textBox_Result.Text);
                MessageBox.Show("資料已複製到剪貼簿。");
            }
            catch
            {
                MessageBox.Show("因安全性設定，無法將資料複製到剪貼簿中。");
            }
        }
    }
}
