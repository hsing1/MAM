﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;

namespace PTS_MAM3.MAG
{
    public partial class MAG500 : Page
    {
        public MAG500()
        {
            // 
            InitializeComponent();
            // 一開始就先載入清單
            fnGetNasFileList();
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            // 判斷是否有選取DataGrid
            if (DG_NASFileList.SelectedItem == null)
            {
                MessageBox.Show("請選擇要查詢的項目", "提示訊息", MessageBoxButton.OK);
                return;
            }
            // 開啟 Busy
            busyIndicator1.IsBusy = true;
            // 將被選取的欄位，轉為 Class Object
            SR_WSPGM_GetMssStatus.ClassNasFileObject objFile = (SR_WSPGM_GetMssStatus.ClassNasFileObject)DG_NASFileList.SelectedItem;
            // 呼叫後端的 WebService 做檔案處理
            SR_WSPGM_GetMssStatus.ServicePGMSoapClient obj = new SR_WSPGM_GetMssStatus.ServicePGMSoapClient();
            obj.DeleteNasFileCompleted += new EventHandler<SR_WSPGM_GetMssStatus.DeleteNasFileCompletedEventArgs>(obj_DeleteNasFileCompleted);
            obj.DeleteNasFileAsync(objFile.VideoID);
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            fnGetNasFileList();
        }

        private void fnGetNasFileList()
        {
            // 因作業時間較長，需要打開忙碌警示燈
            busyIndicator1.IsBusy = true;
            // 呼叫後端的 WebService 來執行取得檔案清單的動作
            SR_WSPGM_GetMssStatus.ServicePGMSoapClient obj = new SR_WSPGM_GetMssStatus.ServicePGMSoapClient();
            obj.InnerChannel.OperationTimeout = new TimeSpan(0, 5, 0);              // Tricky: 因為端對的WS有很多I/O動作，會讓作業時間拉長
            obj.GetNasFileListCompleted += new EventHandler<SR_WSPGM_GetMssStatus.GetNasFileListCompletedEventArgs>(obj_GetNasFileListCompleted);
            obj.GetNasFileListAsync();
        }

        private void obj_DeleteNasFileCompleted(object sender, SR_WSPGM_GetMssStatus.DeleteNasFileCompletedEventArgs e)
        {
            // 重新整理 DataGrid 的資料
            fnGetNasFileList();
        }

        private void obj_GetNasFileListCompleted(object sender, SR_WSPGM_GetMssStatus.GetNasFileListCompletedEventArgs e)
        {
            // 解除忙碌指示燈
            busyIndicator1.IsBusy = false;
            // 清除目前 DataGrid 的內容
            DG_NASFileList.DataContext = null;
            // 檢視回傳結果
            if (e.Error == null)
            {
                if (e.Result.Count == 0)
                {
                    MessageBox.Show("查不到相關的資料");
                    return;
                }
                // 
                DG_NASFileList.DataContext = e.Result;
            }
            else
                MessageBox.Show("連結主機服務時發生問題：" + e.Error.ToString());
        }
    }
}
