﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSDELETE;

namespace PTS_MAM3.MAG
{
    public partial class MAG800_01 : ChildWindow
    {
        public string _fschannel_id = "";
        public string _fsprog_type = "";
        public string _fsprog_id = "";
        public string _fsprog_name = "";
        private WSDELETESoapClient wsDeleteClient = new WSDELETESoapClient();

        public MAG800_01()
        {
            InitializeComponent();
        }

        public MAG800_01(string _fschannel_id, string _fsprog_type)
        {
            InitializeComponent();
            this._fschannel_id = _fschannel_id;
            this._fsprog_type = _fsprog_type;
            wsDeleteClient.GetProgramListCompleted += new EventHandler<GetProgramListCompletedEventArgs>(wsDeleteClient_GetProgramListCompleted);
        }

        void wsDeleteClient_GetProgramListCompleted(object sender, GetProgramListCompletedEventArgs e)
        {
            this.RadBusyIndicator.IsBusy = false;
            if (e.Error != null || e.Result == null)
            {
                MessageBox.Show("載入資料錯誤!", "訊息", MessageBoxButton.OK);
                return;
            }
            else if (e.Result.Count == 0)
            {
                MessageBox.Show("查無資料!", "訊息", MessageBoxButton.OK);
                return;
            }
            this.DataGrid_Result.ItemsSource = null;
            this.DataGrid_Result.ItemsSource = e.Result;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.DataGrid_Result.SelectedItem == null)
            {
                MessageBox.Show("請選擇一則節目/短帶!", "訊息", MessageBoxButton.OK);
                return;
            }
            _fsprog_id = (this.DataGrid_Result.SelectedItem as Class_DELETED_PROGRAM)._fsprog_id;
            _fsprog_name = (this.DataGrid_Result.SelectedItem as Class_DELETED_PROGRAM)._fsprog_name;
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            _fsprog_id = "";
            _fsprog_name = "";
            this.DialogResult = false;
        }

        private void Button_Search_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
            if (string.IsNullOrEmpty(this.TextBox_ID.Text) && string.IsNullOrEmpty(this.TextBox_Name.Text))
            {
                MessageBox.Show("請輸入編號、名稱!", "訊息", MessageBoxButton.OK);
                return;
            }
            else
            {
                wsDeleteClient.GetProgramListAsync(this._fschannel_id, this._fsprog_type, this.TextBox_ID.Text, this.TextBox_Name.Text);
                this.RadBusyIndicator.IsBusy = true;
            }
        }
    }
}

