﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;

namespace PTS_MAM3.MAG
{
    public partial class MAG510 : Page
    {
        public MAG510()
        {
            // 
            InitializeComponent();
            // 取得清單狀態
            fnGetFtpResult();
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //
        }

        private void btnDetail_Click(object sender, RoutedEventArgs e)
        {
            // 判斷是否有選取DataGrid
            if (DG_FtpResult.SelectedItem == null)
            {
                MessageBox.Show("請選擇要查詢的項目", "提示訊息", MessageBoxButton.OK);
                return;
            }
            // 開啟子視窗
            MAG510_01 MAG510_01_frm = new MAG510_01(((SR_WSPGM_GetMssStatus.ClassFTPJob)DG_FtpResult.SelectedItem).VideoID);
            MAG510_01_frm.Show();
            MAG510_01_frm.Closing += (s, args) =>
            {
                // 重新讀取主畫面的資料
                //fnGetFtpResult();         // 因為重讀的時間過長，讓使用者自行決定重讀的時間
            };
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            fnGetFtpResult();
        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            // 開啟 Busy
            busyIndicator1.IsBusy = true;
            // 呼叫後端的 WebService 做檔案處理
            SR_WSPGM_GetMssStatus.ServicePGMSoapClient obj = new SR_WSPGM_GetMssStatus.ServicePGMSoapClient();
            obj.ResetMasterControlErrorFtpJobCompleted += new EventHandler<SR_WSPGM_GetMssStatus.ResetMasterControlErrorFtpJobCompletedEventArgs>(obj_ResetMasterControlErrorFtpJobCompleted);
            obj.ResetMasterControlErrorFtpJobAsync();
        }

        private void DG_FtpResult_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            // 預設正常的前景、背景色
            e.Row.Background = new SolidColorBrush(Colors.White);
            e.Row.Foreground = new SolidColorBrush(Colors.Black);
            // Tricky: 檢查狀態，如果有含"失敗"字樣的，就要變色!
            SR_WSPGM_GetMssStatus.ClassFTPJob obj = e.Row.DataContext as SR_WSPGM_GetMssStatus.ClassFTPJob;
            if (obj.Ftp1Status.Contains("派送失敗") || obj.Ftp2Status.Contains("派送失敗"))
                e.Row.Background = new SolidColorBrush(Colors.Red);
        }

        private void fnGetFtpResult()
        {
            // 因為作業時間比較長，所以要先開啟忙碌警示燈
            busyIndicator1.IsBusy = true;
            // 呼叫後端的 WebService 來執行取得檔案清單的動作
            SR_WSPGM_GetMssStatus.ServicePGMSoapClient obj = new SR_WSPGM_GetMssStatus.ServicePGMSoapClient();
            obj.GetMasterControlFtpStatusCompleted += new EventHandler<SR_WSPGM_GetMssStatus.GetMasterControlFtpStatusCompletedEventArgs>(obj_GetMasterControlFtpStatusCompleted);
            obj.GetMasterControlFtpStatusAsync();
        }

        private void obj_GetMasterControlFtpStatusCompleted(object sender, SR_WSPGM_GetMssStatus.GetMasterControlFtpStatusCompletedEventArgs e)
        {
            // 關閉忙碌警示燈
            busyIndicator1.IsBusy = false;
            // 清除目前 DataGrid 的內容
            DG_FtpResult.DataContext = null;
            // 檢視回傳結果
            if (e.Error == null)
            {
                if (e.Result.Count == 0)
                {
                    MessageBox.Show("查不到相關的資料");
                    return;
                }
                // 
                DG_FtpResult.DataContext = e.Result;
            }
            else
                MessageBox.Show("連結主機服務時發生問題：" + e.Error.ToString());
        }

        private void obj_ResetMasterControlErrorFtpJobCompleted(object sender, SR_WSPGM_GetMssStatus.ResetMasterControlErrorFtpJobCompletedEventArgs e)
        {
            // 如果回傳結果發生問題，提示使用者
            if (e.Error != null)
                MessageBox.Show("重置作業發生錯誤");
            // 更新檔案清單，讓使用者得到新的資訊
            fnGetFtpResult();
        }
    }
}
