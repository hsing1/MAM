﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.MAG
{
    public partial class MAG168_02 : ChildWindow
    {
        public PTS_MAM3.WSLogTemplateField.LogTemplateField_CODESET clCODESET = new PTS_MAM3.WSLogTemplateField.LogTemplateField_CODESET();  //宣告此類別的一個公用成員 
        PTS_MAM3.WSLogTemplateField.LogTemplateField_CODE clCODE = new PTS_MAM3.WSLogTemplateField.LogTemplateField_CODE();               //宣告此類別的一個公用成員 
        WSLogTemplateField.WSLogTemplateFieldSoapClient clntLTF = new WSLogTemplateField.WSLogTemplateFieldSoapClient();                 //宣告此類別的一個公用成員 
        
        List<PTS_MAM3.WSLogTemplateField.LogTemplateField_CODE> lstCODE = new List<PTS_MAM3.WSLogTemplateField.LogTemplateField_CODE>();

        public MAG168_02(PTS_MAM3.WSLogTemplateField.LogTemplateField_CODESET clCODESET)
        {
            this.clCODESET = clCODESET;

            InitializeComponent();
            InitializeForm();
        }

        void InitializeForm() //初始化本頁面
        {
            Title += " (代碼編號:" + clCODESET.FSCODE_ID + " - " + clCODESET.FSCODE_TITLE + ")";

            clntLTF.GetCodeListCompleted += new EventHandler<WSLogTemplateField.GetCodeListCompletedEventArgs>(clntLTF_GetCodeListCompleted);
            clntLTF.GetCodeListAsync(clCODESET.FSCODE_ID);
            busyLoading.IsBusy = true;
        }
        

#region"觸發事件"

        void clntLTF_GetCodeListCompleted(object sender, WSLogTemplateField.GetCodeListCompletedEventArgs e)
        {
            //如果取回的資料沒有錯誤訊息
            if (e.Error == null)
            {
                //如果取回的資料筆數不為0
                if (e.Result.Count != 0)
                {
                    //把查詢結果丟入公用成員lstNews中, 方便母視窗取用
                    lstCODE = (List<PTS_MAM3.WSLogTemplateField.LogTemplateField_CODE>)(e.Result);
                    dgResult.ItemsSource = e.Result;
                }
                else
                {
                    MessageBox.Show("找不到相關代碼", "提示訊息", MessageBoxButton.OK);
                }
            }
            else
            {
                MessageBox.Show("發生錯誤: " + e.Error.ToString(), "提示訊息", MessageBoxButton.OK);
            }
            busyLoading.IsBusy = false;
        }

#endregion


#region"按鈕動作"

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            //宣告出新增子視窗
            MAG168_03 frmADD = new MAG168_03(clCODESET.FSCODE_ID, "ADD");
            frmADD.Show();

            frmADD.Closed += (s, args) =>
            {
                if (frmADD.DialogResult == true)
                {
                    //子視窗新增成功後,重新整理清單
                    clntLTF.GetCodeListAsync(clCODESET.FSCODE_ID); 

                    busyLoading.IsBusy = true;
                }
            };
        }

        private void btnMod_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (dgResult.SelectedItem == null)
            {
                MessageBox.Show("您尚未選擇要修改的資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //宣告出修改子視窗
            MAG168_03 frmMOD = new MAG168_03(clCODESET.FSCODE_ID, "MOD", (PTS_MAM3.WSLogTemplateField.LogTemplateField_CODE)dgResult.SelectedItem);
            frmMOD.Show();

            frmMOD.Closed += (s, args) =>
            {
                if (frmMOD.DialogResult == true)
                {
                    //子視窗修改成功後,重新整理清單
                    clntLTF.GetCodeListAsync(clCODESET.FSCODE_ID);
                    busyLoading.IsBusy = true;
                }
            };
        } 

        private void btnDel_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (dgResult.SelectedItem == null)
            {
                MessageBox.Show("您尚未選擇要刪除的資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            PTS_MAM3.WSLogTemplateField.LogTemplateField_CODE cl = (PTS_MAM3.WSLogTemplateField.LogTemplateField_CODE)dgResult.SelectedItem;

            //if (cl._COUNT > 0)
            //{
            //    MessageBox.Show("請先刪除所屬代碼項目才能移除代碼設定", "提示訊息", MessageBoxButton.OK);
            //    return;
            //}

            //開始刪除檔案
            busyLoading.IsBusy = true;
            clntLTF.DeleteCodeSetAsync(cl.FSCODE_ID);
        }

#endregion

    }
}

