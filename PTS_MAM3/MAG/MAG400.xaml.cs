﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;

namespace PTS_MAM3.MAG
{
    public partial class MAG400 : Page
    {
        public MAG400()
        {
            InitializeComponent();
            // 初始化元件
            dpStart.CurrentDateTimeText = DateTime.Now.ToString("yyyy/M/d");
            dpEnd.CurrentDateTimeText = DateTime.Now.ToString("yyyy/M/d");
            dpStart.DateTimeText = DateTime.Now.ToString("yyyy/M/d");
            dpEnd.DateTimeText = DateTime.Now.ToString("yyyy/M/d");
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //
        }

        // 按鈕 - 詳細資訊
        private void btnDetail_Click(object sender, RoutedEventArgs e)
        {
            // 判斷是否有選取DataGrid
            if (DGASCList.SelectedItem == null)
            {
                MessageBox.Show("請選擇要查詢的項目", "提示訊息", MessageBoxButton.OK);
                return;
            }

            // 開啟子視窗
            MAG400_01 MAG400_01_frm = new MAG400_01(((SR_PGM_NasFileMoving.ClassNasMovingJob)DGASCList.SelectedItem));
            MAG400_01_frm.Show();
            MAG400_01_frm.Closing += (s, args) =>
            {
                if (MAG400_01_frm.DialogResult == true)
                {
                    // 重新讀取主畫面的資料
                    SR_PGM_NasFileMoving.ServiceNasFileMovingSoapClient objNasMoving = new SR_PGM_NasFileMoving.ServiceNasFileMovingSoapClient();
                    objNasMoving.GetNasFileMovingJobInformationCompleted += new EventHandler<SR_PGM_NasFileMoving.GetNasFileMovingJobInformationCompletedEventArgs>(objNasMoving_GetNasFileMovingJobInformationCompleted);
                    objNasMoving.GetNasFileMovingJobInformationAsync(dpStart.DateTimeText, dpEnd.DateTimeText);
                }
            };
        }

        // 按鈕 - 查詢
        private void btnQuery_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(dpStart.DateTimeText) || string.IsNullOrEmpty(dpEnd.DateTimeText))
            {
                MessageBox.Show("選擇的時間參數有誤");
                return;
            }
            //
            if (DateTime.Compare(DateTime.Parse(dpStart.DateTimeText), DateTime.Parse(dpEnd.DateTimeText)) > 0)
            {
                MessageBox.Show("選擇的起始時間大於結束時間");
                return;
            }
            //
            fnDisableButtons();
            // 呼叫後端 WebService 取得資訊
            SR_PGM_NasFileMoving.ServiceNasFileMovingSoapClient objNasMoving = new SR_PGM_NasFileMoving.ServiceNasFileMovingSoapClient();
            objNasMoving.GetNasFileMovingJobInformationCompleted += new EventHandler<SR_PGM_NasFileMoving.GetNasFileMovingJobInformationCompletedEventArgs>(objNasMoving_GetNasFileMovingJobInformationCompleted);
            objNasMoving.GetNasFileMovingJobInformationAsync(dpStart.DateTimeText, dpEnd.DateTimeText);
        }

        private void fnDisableButtons()
        {
            btnQuery.IsEnabled = false;
            btnDetail.IsEnabled = false;
        }

        private void fnEnableButtons()
        {
            btnQuery.IsEnabled = true;
            btnDetail.IsEnabled = true;
        }

        private void objNasMoving_GetNasFileMovingJobInformationCompleted(object sender, SR_PGM_NasFileMoving.GetNasFileMovingJobInformationCompletedEventArgs e)
        {
            //
            fnEnableButtons();
            DGASCList.DataContext = null;
            // 
            if (e.Error == null)
            {
                if (e.Result.Count == 0)
                {
                    MessageBox.Show("查不到該範圍區間的資料");
                    return;
                }
                // 
                DGASCList.DataContext = e.Result;
            }
            else
                MessageBox.Show("連結主機服務時發生問題：" + e.Error.ToString());
        }
    }
}
