﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using PTS_MAM3.Web.ENT;
using System.Windows.Data;
using System.ServiceModel.DomainServices.Client;

namespace PTS_MAM3.MAG
{
    public partial class MAG100 : Page
    {
        private ENT100DomainContext content = new ENT100DomainContext();
        private string tbName;
        public MAG100(string InputtbName)
        {
            InitializeComponent();
            tbName = InputtbName;
            RefreshDataGrid();
            
            //content.PropertyChanged += (s, args) =>
            //    { 
            //        MessageBox.Show(args.PropertyName);
            //    };
            
        }

        private void RefreshDataGrid()
        {
            switch (tbName)
            {
                case "TBFILE_TYPE":
                    var TBFILE_TYPE = content.Load(content.GetTBFILE_TYPEQuery());
                    TBFILE_TYPE.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        b.Path = new PropertyPath("FSARC_TYPE");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        c.Path = new PropertyPath("FSTYPE_NAME");
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        dataGrid1.ItemsSource = TBFILE_TYPE.Entities;
                    };
                    break;
            }
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void btnUserDel_Click(object sender, RoutedEventArgs e)
        {
            WSAgentFieldDelete.WSAgentFieldDeleteSoapClient AgentField = new WSAgentFieldDelete.WSAgentFieldDeleteSoapClient();
            if (dataGrid1.SelectedItem == null)
            { MessageBox.Show("請選擇資料行"); }
            else
            {

                switch (tbName)
                {
                    case "TBFILE_TYPE":
                        TBFILE_TYPE TBFILE_TYPE = (TBFILE_TYPE)dataGrid1.SelectedItem;
                        AgentField.DelAgentFieldAsync("TBFILE_TYPE", ((TBFILE_TYPE)dataGrid1.SelectedItem).FSARC_TYPE, UserClass.userData.FSUSER_ID);
                        break;
                }
                AgentField.DelAgentFieldCompleted += (s2, args2) =>
                    {
                        if (args2.Result == "true")
                        {
                            switch (tbName)
                            {
                                case "TBFILE_TYPE":
                                    content.TBFILE_TYPEs.Clear();
                                    var TBFILE_TYPE = content.Load(content.GetTBFILE_TYPEQuery());
                                    TBFILE_TYPE.Completed += (s1, args1) =>
                                    {
                                        MessageBox.Show("刪除完成");
                                        dataGrid1.ItemsSource = TBFILE_TYPE.Entities;
                                    };
                                    break;
                            }
                        }
                        else
                        { MessageBox.Show(args2.Result, "資料已被使用過", MessageBoxButton.OK); }
                    };
                    
            }
            
        }


        private void btnClickUserAdd(object sender, RoutedEventArgs e)
        {
            MAG100_01 MAG100_01 = new MAG100_01(tbName);
            MAG100_01.Show();
            MAG100_01.Closing += (s, args) =>
            {
                if (MAG100_01.DialogResult == true)
                {
                    MessageBox.Show("資料已經新增");
                    RefreshDataGrid();
                }
                else if (MAG100_01.DialogResult == false)
                {
                    MessageBox.Show("取消新增資料");
                }
            };
        }

        private void btnUserModifyClick(object sender, RoutedEventArgs e)
        {
            if (dataGrid1.SelectedItem == null)
            { MessageBox.Show("請選擇資料行"); }
            else
            {
                MAG100_02 MAG100_02 = new MAG100_02(tbName, dataGrid1.SelectedItem);
                MAG100_02.Show();
                MAG100_02.Closing += (s, args) =>
                {
                    if (MAG100_02.DialogResult == true)
                    {
                        switch (tbName)
                        {
                            case "TBFILE_TYPE":
                                dataGrid1.SelectedItem = (TBFILE_TYPE)MAG100_02.senderObj;
                                break;
                        }
                        content.SubmitChanges().Completed += (s1, args1) =>
                        {
                            MessageBox.Show("資料已經修改");
                            RefreshDataGrid();
                        };
                    }
                    else if (MAG100_02.DialogResult == false)
                    {
                        MessageBox.Show("取消資料修改");
                    }
                };
            }
        }

        private void RefreshDataGridWithCondiction(string strCondiction)
        {
            switch (tbName)
            {
                case "TBFILE_TYPE":
                    var TBFILE_TYPE = content.Load(content.GetTBFILE_TYPEQuery());
                    TBFILE_TYPE.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        b.Path = new PropertyPath("FSARC_TYPE");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        c.Path = new PropertyPath("FSTYPE_NAME");
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        dataGrid1.ItemsSource = TBFILE_TYPE.Entities.Where(R => R.FSTYPE_NAME.Contains(strCondiction));
                    };
                    break;
            }
        }

        private void btnUserSrh_Click(object sender, RoutedEventArgs e)
        {
            MAG.MAG128 MAG128_Page = new MAG128();
            MAG128_Page.Show();
            MAG128_Page.Closing += (s, args) =>
            {
                RefreshDataGridWithCondiction(MAG128_Page.strReturnCondection.ToString().Trim());
            };
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            RefreshDataGrid();
        }
    }
}
