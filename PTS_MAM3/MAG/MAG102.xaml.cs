﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using PTS_MAM3.Web.ENT;
using PTS_MAM3.ArchiveData;

namespace PTS_MAM3.MAG
{
    public partial class MAG102 : Page
    {
        public MAG102(string TBNAME)
        {
            InitializeComponent();
            RefreshDataGrid();
            client.DelRBARCHIVE_SET_RecordCompleted += new EventHandler<WSARCHIVE.DelRBARCHIVE_SET_RecordCompletedEventArgs>(client_DelRBARCHIVE_SET_RecordCompleted);
        }

        void client_DelRBARCHIVE_SET_RecordCompleted(object sender, WSARCHIVE.DelRBARCHIVE_SET_RecordCompletedEventArgs e)
        {
            MessageBox.Show("資料已刪除");
            RefreshDataGrid();
        }
        //private ENT100DomainContext context = new ENT100DomainContext();
        WSARCHIVE.WSARCHIVESoapClient client = new WSARCHIVE.WSARCHIVESoapClient();
        private void RefreshDataGrid()
        {
            client.GetTBARCHIVE_SETAsync();
            client.GetTBARCHIVE_SETCompleted += (s, args) =>
                { dataGrid1.ItemsSource = args.Result; };
            //PTSMAM系統開發 PTSMAM系統開發
            //SP_Q_TBBROADCAST_ALL 裡面有判斷式 
            //var TBARCHIVE_SET = context.Load(context.GetTBARCHIVE_SETQuery());
            //TBARCHIVE_SET.Completed += (s, args) =>
            //    {
            //        dataGrid1.ItemsSource = TBARCHIVE_SET.Entities;
            //    };
            //throw new NotImplementedException();
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }
        private void btnClickUserAdd(object sender, RoutedEventArgs e)
        {
            MAG102_01 pMAG102_01 = new MAG102_01();
            pMAG102_01.Show();
            pMAG102_01.Closing += (s, args) =>
                { RefreshDataGrid(); };
            
        }

        private void btnUserModifyClick(object sender, RoutedEventArgs e)
        {
            //WSARCHIVE.Class_ARCHIVE_SET set = new WSARCHIVE.Class_ARCHIVE_SET();
            //set.SET_ID = ((WSARCHIVE.Class_ARCHIVE_SET)dataGrid1.SelectedItem).SET_ID;
            //set.FDCREATED_DATE = ((WSARCHIVE.Class_ARCHIVE_SET)dataGrid1.SelectedItem).FDCREATED_DATE;
            //set.FDUPDATED_DATE = ((WSARCHIVE.Class_ARCHIVE_SET)dataGrid1.SelectedItem).FDUPDATED_DATE;
            //set.FNAUDIO = ((WSARCHIVE.Class_ARCHIVE_SET)dataGrid1.SelectedItem).FNAUDIO;
            //set.FNDOC = ((WSARCHIVE.Class_ARCHIVE_SET)dataGrid1.SelectedItem).FNDOC;
            //set.FNEPISODE = ((WSARCHIVE.Class_ARCHIVE_SET)dataGrid1.SelectedItem).FNEPISODE;
            //set.FNPHOTO = ((WSARCHIVE.Class_ARCHIVE_SET)dataGrid1.SelectedItem).FNPHOTO;
            //set.FNVIDEO = ((WSARCHIVE.Class_ARCHIVE_SET)dataGrid1.SelectedItem).FNVIDEO;
            //set.FSCREATED_BY = ((WSARCHIVE.Class_ARCHIVE_SET)dataGrid1.SelectedItem).FSCREATED_BY;
            //set.FSID = ((WSARCHIVE.Class_ARCHIVE_SET)dataGrid1.SelectedItem).FSID;
            //set.FSTYPE = ((WSARCHIVE.Class_ARCHIVE_SET)dataGrid1.SelectedItem).FSTYPE;
            //set.FSUPDATED_BY = UserClass.userData.FSUSER_ID;

            MAG102_02 MAG102 = new MAG102_02((WSARCHIVE.Class_ARCHIVE_SET)dataGrid1.SelectedItem);
            MAG102.Show();
            MAG102.Closing += (s, args) =>
                {
                    if (MAG102.DialogResult == true)
                        MessageBox.Show("修改完成");
                };
        }

        private void btnUserDel_Click(object sender, RoutedEventArgs e)
        {
            
            client.DelTBARCHIVE_SETCompleted += (s, args) =>
                {
                    if (args.Result == true)
                    {
                        RefreshDataGrid();
                        MessageBox.Show("刪除完成");
                    }
                };
            
            client.DelTBARCHIVE_SETAsync((WSARCHIVE.Class_ARCHIVE_SET)dataGrid1.SelectedItem, UserClass.userData.FSUSER_ID);
            //TBARCHIVE_SET TBARCHIVE_SET = (TBARCHIVE_SET)dataGrid1.SelectedItem;
            //context.TBARCHIVE_SETs.Remove(TBARCHIVE_SET);
            //context.SubmitChanges().Completed += (s, args) =>
            //    {
            //        MessageBox.Show("資料已刪除");
            //        RefreshDataGrid();
            //    };
        }
        #region 條件查詢
        private void RefreshDataGridWithCondiction(string strType,string strProgramID,string strEpisode)
        {
            //client.GetTBARCHIVE_SET_SEARCHAsync(strType, strProgramID, strEpisode);
            //client.GetTBARCHIVE_SET_SEARCHCompleted += (s, args) =>
            //{ dataGrid1.ItemsSource = args.Result; };
            client.GetTBARCHIVE_SETAsync();
            client.GetTBARCHIVE_SETCompleted += (s, args) =>
            { dataGrid1.ItemsSource = args.Result.Where(R => R.FSTYPE.ToString() == strType && R.FSID.ToString() == strProgramID && R.FNEPISODE.ToString() == strEpisode); };
            //PTSMAM系統開發 PTSMAM系統開發
            //SP_Q_TBBROADCAST_ALL 裡面有判斷式 
            //var TBARCHIVE_SET = context.Load(context.GetTBARCHIVE_SETQuery());
            //TBARCHIVE_SET.Completed += (s, args) =>
            //    {
            //        dataGrid1.ItemsSource = TBARCHIVE_SET.Entities;
            //    };
            //throw new NotImplementedException();
        }
        #endregion

        private void btnUserDel_Click_1(object sender, RoutedEventArgs e)
        {
            STO.STO100_05 page_STO100_05 = new STO.STO100_05();
            page_STO100_05.Show();

            page_STO100_05.Closed += (s, args) =>
            {
                if (page_STO100_05.public_Type.ToString().Trim() != "" && page_STO100_05.public_ID.ToString().Trim() != "" && page_STO100_05.public_Episode.ToString().Trim() != "")
                {
                    RefreshDataGridWithCondiction(page_STO100_05.public_Type.ToString().Trim(), page_STO100_05.public_ID.ToString().Trim(), page_STO100_05.public_Episode.ToString().Trim());
                }

                //if (ArchiveData_Query_frm.m_ListFormData.Count != 0)
                //{
                //    //this.datagrid1.ItemsSource = null;
                //    //this.datagrid1.ItemsSource = ArchiveData_Query_frm.m_ListFormData;
                //    //this.datagrid1.SelectedIndex = 0;
                //    //MessageBox.Show(ArchiveData_Query_frm.public_Type + "" + " " + "");
                //}
            };
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            RefreshDataGrid();
        }
        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (((WSARCHIVE.Class_ARCHIVE_SET)dataGrid1.SelectedItem) != null)
            { client.DelRBARCHIVE_SET_RecordAsync(((WSARCHIVE.Class_ARCHIVE_SET)dataGrid1.SelectedItem).FSTYPE, ((WSARCHIVE.Class_ARCHIVE_SET)dataGrid1.SelectedItem).FSID, ((WSARCHIVE.Class_ARCHIVE_SET)dataGrid1.SelectedItem).FNEPISODE.ToString(), UserClass.userData.FSUSER_ID); }
        }
    }
}
