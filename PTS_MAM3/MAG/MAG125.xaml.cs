﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using PTS_MAM3.Web.ENT;
using System.Windows.Data;

namespace PTS_MAM3.MAG
{
    public partial class MAG125 : Page
    {
        private ENT100DomainContext content = new ENT100DomainContext();
        private string tbName;
        public MAG125(string InputtbName)
        {
            InitializeComponent();
            tbName = InputtbName;
            RefreshDataGrid();
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void RefreshDataGrid()
        {
            switch(tbName)
            {
                case "TBZPROGRACE_AREA":
                    var TBZPROGRACE_AREA = content.Load(content.GetTBZPROGRACE_AREAQuery());
                    TBZPROGRACE_AREA.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        b.Path = new PropertyPath("FSID");
                        c.Path = new PropertyPath("FSNAME");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "參展區域代碼檔編號";
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "參展區域代碼檔名稱";
                        dataGrid1.ItemsSource = TBZPROGRACE_AREA.Entities.Where(s1 => s1.FSID.Trim() != "");
                    };
                    break;
                case "TBZSIGNAL":
                    var TBZSIGNAL = content.Load(content.GetTBZSIGNALQuery());
                    TBZSIGNAL.Completed += (s, args) =>
                        {
                            Binding b = new Binding();
                            b.Mode = BindingMode.OneWay;
                            Binding c = new Binding();
                            c.Mode = BindingMode.OneWay;
                            b.Path = new PropertyPath("FSSIGNALID");
                            c.Path = new PropertyPath("FSSIGNALNAME");
                            ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                            ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "訊號來源代碼檔編號";
                            ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                            ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "訊號來源代碼檔名稱";
                            dataGrid1.ItemsSource = TBZSIGNAL.Entities.Where(s1=>s1.FSSIGNALID.Trim()!="");
                        };
                    break;
                case "TBZPROGSPEC":
                    var TBZPROGSPEC = content.Load(content.GetTBZPROGSPECQuery());
                    TBZPROGSPEC.Completed += (s, args) =>
                        {
                            Binding b = new Binding();
                            b.Mode = BindingMode.OneWay;
                            Binding c = new Binding();
                            c.Mode = BindingMode.OneWay;
                            b.Path = new PropertyPath("FSPROGSPECID");
                            c.Path = new PropertyPath("FSPROGSPECNAME");
                            ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                            ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "節目規格代碼檔編號";
                            ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                            ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "節目規格代碼檔名稱";
                            dataGrid1.ItemsSource = TBZPROGSPEC.Entities.Where(s2=>s2.FSPROGSPECID.Trim()!="");
                        };
                    break;
                case "TBZPROMOVER":
                    var TBZPROMOVER = content.Load(content.GetTBZPROMOVERQuery());
                    TBZPROMOVER.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        b.Path = new PropertyPath("FSPROMOVERID");
                        c.Path = new PropertyPath("FSPROMOVERNAME");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "短帶版本代碼檔編號";
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "短帶版本代碼檔名稱";
                        dataGrid1.ItemsSource = TBZPROMOVER.Entities.Where(s4 => s4.FSPROMOVERID.Trim() != "");
                    };
                    break;
                case "TBZSHOOTSPEC":
                    var TBZSHOOTSPEC = content.Load(content.GetTBZSHOOTSPECQuery());
                    TBZSHOOTSPEC.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        b.Path = new PropertyPath("FSSHOOTSPECID");
                        c.Path = new PropertyPath("FSSHOOTSPECNAME");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "拍攝規格代碼檔編號";
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "拍攝規格代碼檔名稱";
                        dataGrid1.ItemsSource = TBZSHOOTSPEC.Entities.Where(s5=>s5.FSSHOOTSPECID.Trim()!="");
                    };
                    break;
                case "TBPGM_ACTION":
                    var TBPGM_ACTION = content.Load(content.GetTBPGM_ACTIONQuery());
                    TBPGM_ACTION.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        b.Path = new PropertyPath("FSACTIONID");
                        c.Path = new PropertyPath("FSACTIONNAME");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "活動代碼檔編號";
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "活動代碼檔名稱";
                        dataGrid1.ItemsSource = TBPGM_ACTION.Entities.Where(s6=>s6.FSACTIONID.Trim()!="");
                    };
                    break;
                case "TBZPROMOTYPE":
                    var TBZPROMOTYPE = content.Load(content.GetTBZPROMOTYPEQuery());
                    TBZPROMOTYPE.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        b.Path = new PropertyPath("FSPROMOTYPEID");
                        c.Path = new PropertyPath("FSPROMOTYPENAME");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "短帶類別代碼檔編號";
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "短帶類別代碼檔名稱";
                        dataGrid1.ItemsSource = TBZPROMOTYPE.Entities.Where(s7=>s7.FSPROMOTYPEID.Trim()!="");
                    };
                    break;
                case "TBZPROMOCAT":
                    var TBZPROMOCAT = content.Load(content.GetTBZPROMOCATQuery());
                    TBZPROMOCAT.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        b.Path = new PropertyPath("FSPROMOCATID");
                        c.Path = new PropertyPath("FSPROMOCATNAME");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "短帶類型代碼檔編號";
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "短帶類型代碼檔名稱";
                        dataGrid1.ItemsSource = TBZPROMOCAT.Entities.Where(s8=>s8.FSPROMOCATID.Trim()!="");
                    };
                    break;
                case"TBZBOOKING_REASON":
                    var TBZBOOKING_REASON = content.Load(content.GetTBZBOOKING_REASONQuery());
                    TBZBOOKING_REASON.Completed += (s, args) =>
                    {
                            Binding b = new Binding();
                            b.Mode = BindingMode.OneWay;
                            Binding c = new Binding();
                            c.Mode = BindingMode.OneWay;
                            b.Path = new PropertyPath("FSNO");
                            c.Path = new PropertyPath("FSREASON");
                            ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                            ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "調用原因代碼檔編號";
                            ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                            ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "調用原因代碼檔名稱";
                            dataGrid1.ItemsSource = TBZBOOKING_REASON.Entities.Where(s9=>s9.FSNO.Trim()!="");
                        };
                    break;
                case "TBZPROGSTATUS":
                    var TBZPROGSTATUS = content.Load(content.GetTBZPROGSTATUSQuery());
                    TBZPROGSTATUS.Completed += (s, args) =>
                        {
                            Binding b = new Binding();
                            b.Mode = BindingMode.OneWay;
                            Binding c = new Binding();
                            c.Mode = BindingMode.OneWay;
                            b.Path = new PropertyPath("FSPROGSTATUSID");
                            c.Path = new PropertyPath("FSPROGSTATUSNAME");
                            ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                            ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "參展狀態代碼檔編號";
                            ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                            ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "參展狀態代碼檔名稱";
                            dataGrid1.ItemsSource = TBZPROGSTATUS.Entities.Where(sa=>sa.FSPROGSTATUSID.Trim()!="");
                        };
                    break;
                case "TBZTITLE":
                    var TBZTITLE = content.Load(content.GetTBZTITLEQuery());
                    TBZTITLE.Completed += (s, args) =>
                        {
                            Binding b = new Binding();
                            b.Mode = BindingMode.OneWay;
                            Binding c = new Binding();
                            c.Mode = BindingMode.OneWay;
                            b.Path = new PropertyPath("FSTITLEID");
                            c.Path = new PropertyPath("FSTITLENAME");
                            ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                            ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "頭銜代碼檔編號";
                            ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                            ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "頭銜代碼檔名稱";
                             dataGrid1.ItemsSource = TBZTITLE.Entities.Where(sb=>sb.FSTITLEID.Trim()!="");
                        };
                    break;
                case "TBZDEPT":
                    var TBZDEPT = content.Load(content.GetTBZDEPTQuery());
                    TBZDEPT.Completed += (s, args) =>
                        {
                            Binding b = new Binding();
                            b.Mode = BindingMode.OneWay;
                            Binding c = new Binding();
                            c.Mode = BindingMode.OneWay;
                            b.Path = new PropertyPath("FSDEPTID");
                            c.Path = new PropertyPath("FSDEPTNAME");
                            ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                            ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "部門代碼檔編號";
                            ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                            ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "部門代碼檔名稱";
                            dataGrid1.ItemsSource = TBZDEPT.Entities.Where(sc=>sc.FSDEPTID.Trim()!="");
                        };
                    break;
                case "TBZPROGOBJ":
                    var dataset = content.Load(content.GetTBZPROGOBJQuery());
                    dataset.Completed += (s, args) =>
                        {
                            Binding b = new Binding();
                            b.Mode = BindingMode.OneWay;
                            Binding c = new Binding();
                            c.Mode = BindingMode.OneWay;
                            b.Path = new PropertyPath("FSPROGOBJID");
                            c.Path = new PropertyPath("FSPROGOBJNAME");
                            ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                            ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "製作目的代碼檔編號";
                            ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                            ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "製作目的代碼檔名稱";
                            dataGrid1.ItemsSource = dataset.Entities.Where(sd=>sd.FSPROGOBJID.Trim()!="");
                        };
                    break;
                case "TBZPROGSRC":
                    var TBZPROGSRC = content.Load(content.GetTBZPROGSRCQuery());
                    TBZPROGSRC.Completed += (s, args) =>
                        {
                            Binding b = new Binding();
                            b.Mode = BindingMode.OneWay;
                            Binding c = new Binding();
                            c.Mode = BindingMode.OneWay;
                            b.Path = new PropertyPath("FSPROGSRCID");
                            c.Path = new PropertyPath("FSPROGSRCNAME");
                            ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                            ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "節目來源代碼檔編號";
                            ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                            ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "節目來源代碼檔名稱";
                            dataGrid1.ItemsSource = TBZPROGSRC.Entities.Where(se=>se.FSPROGSRCID.Trim()!="");
                        };
                    break;
                case "TBZPROGATTR":
                    var TBZPROGATTR = content.Load(content.GetTBZPROGATTRQuery());
                    TBZPROGATTR.Completed += (s, args) =>
                        {
                            Binding b = new Binding();
                            b.Mode = BindingMode.OneWay;
                            Binding c = new Binding();
                            c.Mode = BindingMode.OneWay;
                            b.Path = new PropertyPath("FSPROGATTRID");
                            c.Path = new PropertyPath("FSPROGATTRNAME");
                            ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                            ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "內容屬性代碼檔編號";
                            ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                            ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "內容屬性代碼檔名稱";
                            dataGrid1.ItemsSource = TBZPROGATTR.Entities.Where(sf=>sf.FSPROGATTRID.Trim()!="");
                        };
                    break;
                case "TBZPROGAUD":
                    var TBZPROGAUD = content.Load(content.GetTBZPROGAUDQuery());
                    TBZPROGAUD.Completed += (s, args) =>
                        {
                            Binding b = new Binding();
                            b.Mode = BindingMode.OneWay;
                            Binding c = new Binding();
                            c.Mode = BindingMode.OneWay;
                            b.Path = new PropertyPath("FSPROGAUDID");
                            c.Path = new PropertyPath("FSPROGAUDNAME");
                            ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                            ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "目標觀眾代碼檔編號";
                            ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                            ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "目標觀眾代碼檔名稱";
                            dataGrid1.ItemsSource = TBZPROGAUD.Entities.Where(sg=>sg.FSPROGAUDID.Trim()!="");
                        };
                    break;
                case "TBZPROGTYPE":
                    var TBZPROGTYPE = content.Load(content.GetTBZPROGTYPEQuery());
                    TBZPROGTYPE.Completed += (s, args) =>
                        {
                            Binding b = new Binding();
                            b.Mode = BindingMode.OneWay;
                            Binding c = new Binding();
                            c.Mode = BindingMode.OneWay;
                            b.Path = new PropertyPath("FSPROGTYPEID");
                            c.Path = new PropertyPath("FSPROGTYPENAME");
                            ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                            ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "表現方式代碼檔編號";
                            ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                            ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "表現方式代碼檔名稱";
                            dataGrid1.ItemsSource = TBZPROGTYPE.Entities.Where(sh=>sh.FSPROGTYPEID.Trim()!="");
                        };
                    break;
                case "TBZPROGLANG":
                    var TBZPROGLANG = content.Load(content.GetTBZPROGLANGQuery());
                    TBZPROGLANG.Completed += (s, args) =>
                        {
                            Binding b = new Binding();
                            b.Mode = BindingMode.OneWay;
                            Binding c = new Binding();
                            c.Mode = BindingMode.OneWay;
                            b.Path = new PropertyPath("FSPROGLANGID");
                            c.Path = new PropertyPath("FSPROGLANGNAME");
                            ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                            ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "語言代碼檔編號";
                            ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                            ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "語言代碼檔名稱";
                            dataGrid1.ItemsSource = TBZPROGLANG.Entities.Where(si=>si.FSPROGLANGID.Trim()!="");
                        };
                    break;
                case "TBZPROGBUY":
                    var TBZPROGBUY = content.Load(content.GetTBZPROGBUYQuery());
                    TBZPROGBUY.Completed += (s, args) =>
                        {
                            Binding b = new Binding();
                            b.Mode = BindingMode.OneWay;
                            Binding c = new Binding();
                            c.Mode = BindingMode.OneWay;
                            b.Path = new PropertyPath("FSPROGBUYID");
                            c.Path = new PropertyPath("FSPROGBUYNAME");
                            ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                            ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "外購類別代碼檔編號";
                            ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                            ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "外購類別代碼檔名稱";
                            dataGrid1.ItemsSource = TBZPROGBUY.Entities.Where(sj=>sj.FSPROGBUYID.Trim()!="");
                        };
                    break;
                case "TBZPROGGRADE":
                    var TBZPROGGRADE = content.Load(content.GetTBZPROGGRADEQuery());
                    TBZPROGGRADE.Completed += (s, args) =>
                        {
                            Binding b = new Binding();
                            b.Mode = BindingMode.OneWay;
                            Binding c = new Binding();
                            c.Mode = BindingMode.OneWay;
                            b.Path = new PropertyPath("FSPROGGRADEID");
                            c.Path = new PropertyPath("FSPROGGRADENAME");
                            ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                            dataGrid1.Columns[0].Header = "節目分級代碼檔編號";
                            ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                            ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "節目分級代碼檔名稱";
                            dataGrid1.ItemsSource = TBZPROGGRADE.Entities.Where(sk=>sk.FSPROGGRADEID.Trim()!="");
                        };
                    break;
            }
        }



        private void btnUserDel_Click(object sender, RoutedEventArgs e)
        {
            WSAgentFieldDelete.WSAgentFieldDeleteSoapClient AgentField = new WSAgentFieldDelete.WSAgentFieldDeleteSoapClient();
            if (dataGrid1.SelectedItem == null)
            { MessageBox.Show("請選擇資料行"); }
            else
            {
                switch (tbName)
                {

                    case "TBZPROGRACE_AREA":
                        TBZPROGRACE_AREA TBZPROGRACE_AREA = (TBZPROGRACE_AREA)dataGrid1.SelectedItem;
                        AgentField.DelAgentFieldAsync("TBZPROGRACE_AREA", TBZPROGRACE_AREA.FSID, UserClass.userData.FSUSER_ID);
                        content.TBZPROGRACE_AREAs.Remove((TBZPROGRACE_AREA)dataGrid1.SelectedItem);
                        content.SubmitChanges();
                        break;
                    case "TBZSIGNAL":
                        TBZSIGNAL TBZSIGNAL = (TBZSIGNAL)dataGrid1.SelectedItem;
                        AgentField.DelAgentFieldAsync("TBZSIGNAL", TBZSIGNAL.FSSIGNALID, UserClass.userData.FSUSER_ID);
                        break;
                    case "TBZPROGSPEC":
                        TBZPROGSPEC TBZPROGSPEC = (TBZPROGSPEC)dataGrid1.SelectedItem;
                        AgentField.DelAgentFieldAsync("TBZPROGSPEC", TBZPROGSPEC.FSPROGSPECID, UserClass.userData.FSUSER_ID);
                        break;
                    case "TBZPROMOVER":
                        TBZPROMOVER TBZPROMOVER = (TBZPROMOVER)dataGrid1.SelectedItem;
                        AgentField.DelAgentFieldAsync("TBZPROMOVER", TBZPROMOVER.FSPROMOVERID, UserClass.userData.FSUSER_ID);
                        break;
                    case "TBZSHOOTSPEC":
                        TBZSHOOTSPEC TBZSHOOTSPEC = (TBZSHOOTSPEC)dataGrid1.SelectedItem;
                        AgentField.DelAgentFieldAsync("TBZSHOOTSPEC", TBZSHOOTSPEC.FSSHOOTSPECID, UserClass.userData.FSUSER_ID);
                        break;
                    case "TBPGM_ACTION":
                        TBPGM_ACTION TBPGM_ACTION = (TBPGM_ACTION)dataGrid1.SelectedItem;
                        AgentField.DelAgentFieldAsync("TBPGM_ACTION", TBPGM_ACTION.FSACTIONID, UserClass.userData.FSUSER_ID);
                        break;
                    case "TBZBOOKING_REASON":
                        TBZBOOKING_REASON TBZBOOKING_REASON = (TBZBOOKING_REASON)dataGrid1.SelectedItem;
                        AgentField.DelAgentFieldAsync("TBZBOOKING_REASON", TBZBOOKING_REASON.FSNO, UserClass.userData.FSUSER_ID);
                        break;
                    case "TBZPROMOTYPE":
                        TBZPROMOTYPE TBZPROMOTYPE = (TBZPROMOTYPE)dataGrid1.SelectedItem;
                        AgentField.DelAgentFieldAsync("TBZPROMOTYPE", TBZPROMOTYPE.FSPROMOTYPEID, UserClass.userData.FSUSER_ID);
                        break;

                    case "TBZPROMOCAT":
                        TBZPROMOCAT TBZPROMOCAT = (TBZPROMOCAT)dataGrid1.SelectedItem;
                        AgentField.DelAgentFieldAsync("TBZPROMOCAT", TBZPROMOCAT.FSPROMOCATID, UserClass.userData.FSUSER_ID);
                        break;
                    case "TBZPROGSTATUS":
                        TBZPROGSTATUS TBZPROGSTATUS = (TBZPROGSTATUS)dataGrid1.SelectedItem;
                        AgentField.DelAgentFieldAsync("TBZPROGSTATUS", TBZPROGSTATUS.FSPROGSTATUSID, UserClass.userData.FSUSER_ID);
                        break;
                    case "TBZTITLE":
                        TBZTITLE TBZTITLE = (TBZTITLE)dataGrid1.SelectedItem;
                        AgentField.DelAgentFieldAsync("TBZTITLE", TBZTITLE.FSTITLEID, UserClass.userData.FSUSER_ID);
                        break;
                    case "TBZDEPT":
                        TBZDEPT TBZDEPT = (TBZDEPT)dataGrid1.SelectedItem;
                        AgentField.DelAgentFieldAsync("TBZDEPT", TBZDEPT.FSDEPTID, UserClass.userData.FSUSER_ID);
                        break;
                    case "TBZPROGOBJ":
                        TBZPROGOBJ TBZPROGOBJ = (TBZPROGOBJ)dataGrid1.SelectedItem;
                        AgentField.DelAgentFieldAsync("TBZPROGOBJ", TBZPROGOBJ.FSPROGOBJID, UserClass.userData.FSUSER_ID);
                        break;
                    case "TBZPROGSRC":
                        TBZPROGSRC TBZPROGSRC = (TBZPROGSRC)dataGrid1.SelectedItem;
                        AgentField.DelAgentFieldAsync("TBZPROGSRC", TBZPROGSRC.FSPROGSRCID, UserClass.userData.FSUSER_ID);
                        break;
                    case "TBZPROGATTR":
                        TBZPROGATTR TBZPROGATTR = (TBZPROGATTR)dataGrid1.SelectedItem;
                        AgentField.DelAgentFieldAsync("TBZPROGATTR", TBZPROGATTR.FSPROGATTRID, UserClass.userData.FSUSER_ID);
                        break;
                    case "TBZPROGAUD":
                        TBZPROGAUD TBZPROGAUD = (TBZPROGAUD)dataGrid1.SelectedItem;
                        AgentField.DelAgentFieldAsync("TBZPROGAUD", TBZPROGAUD.FSPROGAUDID, UserClass.userData.FSUSER_ID);
                        break;
                    case "TBZPROGTYPE":
                        TBZPROGTYPE TBZPROGTYPE = (TBZPROGTYPE)dataGrid1.SelectedItem;
                        AgentField.DelAgentFieldAsync("TBZPROGTYPE", TBZPROGTYPE.FSPROGTYPEID, UserClass.userData.FSUSER_ID);
                        break;
                    case "TBZPROGLANG":
                        TBZPROGLANG TBZPROGLANG = (TBZPROGLANG)dataGrid1.SelectedItem;
                        AgentField.DelAgentFieldAsync("TBZPROGLANG", TBZPROGLANG.FSPROGLANGID, UserClass.userData.FSUSER_ID);
                        break;
                    case "TBZPROGBUY":
                        TBZPROGBUY TBZPROGBUY = (TBZPROGBUY)dataGrid1.SelectedItem;
                        AgentField.DelAgentFieldAsync("TBZPROGBUY", TBZPROGBUY.FSPROGBUYID, UserClass.userData.FSUSER_ID);
                        break;
                    case "TBZPROGGRADE":
                        TBZPROGGRADE TBZPROGGRADE = (TBZPROGGRADE)dataGrid1.SelectedItem;
                        AgentField.DelAgentFieldAsync("TBZPROGGRADE", TBZPROGGRADE.FSPROGGRADEID, UserClass.userData.FSUSER_ID);
                        break;
                }
                AgentField.DelAgentFieldCompleted+=(s,args)=>
                    {
                        if (args.Result == "true")
                        {
                            RefreshDataGrid();
                            MessageBox.Show("選取的資料行已經刪除");
                        }
                        else { MessageBox.Show(args.Result, "資料庫訊息", MessageBoxButton.OK); }
                    };
            }
        }

        private void btnClickUserAdd(object sender, RoutedEventArgs e)
        {
            MAG126 MAG126 = new MAG126(tbName);
            MAG126.Show();
            MAG126.Closing += (s, args) =>
                {
                    if (MAG126.DialogResult == true)
                    {
                        MessageBox.Show("資料已經新增");
                        RefreshDataGrid();
                    }
                    else if (MAG126.DialogResult == false)
                    {
                        MessageBox.Show("取消新增資料");
                    }
                };
        }

        private void btnUserModifyClick(object sender, RoutedEventArgs e)
        {
            if (dataGrid1.SelectedItem == null)
            { MessageBox.Show("請選擇資料行"); }
            else
            {
                MAG127 MAG127 = new MAG127(tbName,dataGrid1.SelectedItem);
                MAG127.Show();
                MAG127.Closing += (s, args) =>
                {
                    if (MAG127.DialogResult == true)
                    {
                        switch (tbName)
                        {
                            case "TBZPROGRACE_AREA":
                                dataGrid1.SelectedItem = (TBZPROGRACE_AREA)MAG127.senderObj;
                                break;
                            case "TBZSIGNAL":
                                dataGrid1.SelectedItem = (TBZSIGNAL)MAG127.senderObj;
                                break;
                            case "TBZPROGSPEC":
                                dataGrid1.SelectedItem = (TBZPROGSPEC)MAG127.senderObj;
                                break;
                            case "TBZPROMOVER":
                                dataGrid1.SelectedItem = (TBZPROMOVER)MAG127.senderObj;
                                break;
                            case "TBZSHOOTSPEC":
                                dataGrid1.SelectedItem = (TBZSHOOTSPEC)MAG127.senderObj;
                                break;
                            case "TBPGM_ACTION":
                                dataGrid1.SelectedItem = (TBPGM_ACTION)MAG127.senderObj;
                                break;
                            case "TBZPROMOTYPE":
                                dataGrid1.SelectedItem = (TBZPROMOTYPE)MAG127.senderObj;
                                break;
                            case "TBZPROMOCAT":
                                dataGrid1.SelectedItem = (TBZPROMOCAT)MAG127.senderObj;
                                break;
                            case "TBZBOOKING_REASON":
                                dataGrid1.SelectedItem = (TBZBOOKING_REASON)MAG127.senderObj;
                                break;
                            case "TBZPROGSTATUS":
                                dataGrid1.SelectedItem = (TBZPROGSTATUS)MAG127.senderObj;
                                break;
                            case "TBZTITLE":
                                dataGrid1.SelectedItem = (TBZTITLE)MAG127.senderObj;
                                break;
                            case "TBZDEPT":
                                dataGrid1.SelectedItem = (TBZDEPT)MAG127.senderObj;
                                break;
                            case "TBZPROGOBJ":
                                dataGrid1.SelectedItem = (TBZPROGOBJ)MAG127.senderObj;
                                break;
                            case "TBZPROGSRC":
                                dataGrid1.SelectedItem = (TBZPROGSRC)MAG127.senderObj;
                                break;
                            case "TBZPROGATTR":
                                dataGrid1.SelectedItem = (TBZPROGATTR)MAG127.senderObj;
                                break;
                            case "TBZPROGAUD":
                                dataGrid1.SelectedItem = (TBZPROGAUD)MAG127.senderObj;
                                break;
                            case "TBZPROGTYPE":
                                dataGrid1.SelectedItem = (TBZPROGTYPE)MAG127.senderObj;
                                break;
                            case "TBZPROGLANG":
                                dataGrid1.SelectedItem = (TBZPROGLANG)MAG127.senderObj;
                                break;
                            case "TBZPROGBUY":
                                dataGrid1.SelectedItem = (TBZPROGBUY)MAG127.senderObj;
                                break;
                            case "TBZPROGBUYD":
                                dataGrid1.SelectedItem = (TBZPROGBUYD)MAG127.senderObj;
                                break;
                            case "TBZPROGGRADE":
                                dataGrid1.SelectedItem = (TBZPROGGRADE)MAG127.senderObj;
                                break;
                        }
                        content.SubmitChanges().Completed += (s1, args1) =>
                        {
                            MessageBox.Show("資料已經修改");
                            RefreshDataGrid();
                        };
                    }
                    else if (MAG127.DialogResult == false)
                    {
                        MessageBox.Show("取消資料修改");
                    }
                };
            }
        }

        #region 條件查詢所用
        private void RefreshDataGridWithCondiction(string strCondiction)
        {
            switch (tbName)
            {

                case "TBZPROGRACE_AREA":
                    var TBZPROGRACE_AREA = content.Load(content.GetTBZPROGRACE_AREAQuery());
                    TBZPROGRACE_AREA.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        b.Path = new PropertyPath("FSID");
                        c.Path = new PropertyPath("FSNAME");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "參展區域代碼檔編號";
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "參展區域代碼檔名稱";
                        dataGrid1.ItemsSource = TBZPROGRACE_AREA.Entities.Where(R => R.FSNAME.Contains(strCondiction));
                    };
                    break;
                case "TBZSIGNAL":
                    var TBZSIGNAL = content.Load(content.GetTBZSIGNALQuery());
                    TBZSIGNAL.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        b.Path = new PropertyPath("FSSIGNALID");
                        c.Path = new PropertyPath("FSSIGNALNAME");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "訊號來源代碼檔編號";
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "訊號來源代碼檔名稱";
                        dataGrid1.ItemsSource = TBZSIGNAL.Entities.Where(R => R.FSSIGNALNAME.Contains(strCondiction));
                    };
                    break;
                case "TBZPROGSPEC":
                    var TBZPROGSPEC = content.Load(content.GetTBZPROGSPECQuery());
                    TBZPROGSPEC.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        b.Path = new PropertyPath("FSPROGSPECID");
                        c.Path = new PropertyPath("FSPROGSPECNAME");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "節目規格代碼檔編號";
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "節目規格代碼檔名稱";
                        dataGrid1.ItemsSource = TBZPROGSPEC.Entities.Where(R => R.FSPROGSPECNAME.Contains(strCondiction));
                    };
                    break;
                case "TBZPROMOVER":
                    var TBZPROMOVER = content.Load(content.GetTBZPROMOVERQuery());
                    TBZPROMOVER.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        b.Path = new PropertyPath("FSPROMOVERID");
                        c.Path = new PropertyPath("FSPROMOVERNAME");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "短帶版本代碼檔編號";
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "短帶版本代碼檔名稱";
                        dataGrid1.ItemsSource = TBZPROMOVER.Entities.Where(R => R.FSPROMOVERNAME.Contains(strCondiction));
                    };
                    break;
                case "TBZSHOOTSPEC":
                    var TBZSHOOTSPEC = content.Load(content.GetTBZSHOOTSPECQuery());
                    TBZSHOOTSPEC.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        b.Path = new PropertyPath("FSSHOOTSPECID");
                        c.Path = new PropertyPath("FSSHOOTSPECNAME");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "拍攝規格代碼檔編號";
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "拍攝規格代碼檔名稱";
                        dataGrid1.ItemsSource = TBZSHOOTSPEC.Entities.Where(R => R.FSSHOOTSPECNAME.Contains(strCondiction));
                    };
                    break;
                case "TBPGM_ACTION":
                    var TBPGM_ACTION = content.Load(content.GetTBPGM_ACTIONQuery());
                    TBPGM_ACTION.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        b.Path = new PropertyPath("FSACTIONID");
                        c.Path = new PropertyPath("FSACTIONNAME");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "活動代碼檔編號";
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "活動代碼檔名稱";
                        dataGrid1.ItemsSource = TBPGM_ACTION.Entities.Where(R => R.FSACTIONNAME.Contains(strCondiction));
                    };
                    break;
                case "TBZPROMOTYPE":
                    var TBZPROMOTYPE = content.Load(content.GetTBZPROMOTYPEQuery());
                    TBZPROMOTYPE.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        b.Path = new PropertyPath("FSPROMOTYPEID");
                        c.Path = new PropertyPath("FSPROMOTYPENAME");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "短帶類別代碼檔編號";
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "短帶類別代碼檔名稱";
                        dataGrid1.ItemsSource = TBZPROMOTYPE.Entities.Where(R => R.FSPROMOTYPENAME.Contains(strCondiction));
                    };
                    break;
                case "TBZPROMOCAT":
                    var TBZPROMOCAT = content.Load(content.GetTBZPROMOCATQuery());
                    TBZPROMOCAT.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        b.Path = new PropertyPath("FSPROMOCATID");
                        c.Path = new PropertyPath("FSPROMOCATNAME");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "短帶類型代碼檔編號";
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "短帶類型代碼檔名稱";
                        dataGrid1.ItemsSource = TBZPROMOCAT.Entities.Where(R => R.FSPROMOCATNAME.Contains(strCondiction));
                    };
                    break;
                case "TBZBOOKING_REASON":
                    var TBZBOOKING_REASON = content.Load(content.GetTBZBOOKING_REASONQuery());
                    TBZBOOKING_REASON.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        b.Path = new PropertyPath("FSNO");
                        c.Path = new PropertyPath("FSREASON");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "調用原因代碼檔編號";
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "調用原因代碼檔名稱";
                        dataGrid1.ItemsSource = TBZBOOKING_REASON.Entities.Where(R => R.FSREASON.Contains(strCondiction));
                    };
                    break;
                case "TBZPROGSTATUS":
                    var TBZPROGSTATUS = content.Load(content.GetTBZPROGSTATUSQuery());
                    TBZPROGSTATUS.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        b.Path = new PropertyPath("FSPROGSTATUSID");
                        c.Path = new PropertyPath("FSPROGSTATUSNAME");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "參展狀態代碼檔編號";
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "參展狀態代碼檔名稱";
                        dataGrid1.ItemsSource = TBZPROGSTATUS.Entities.Where(R => R.FSPROGSTATUSNAME.Contains(strCondiction));
                    };
                    break;
                case "TBZTITLE":
                    var TBZTITLE = content.Load(content.GetTBZTITLEQuery());
                    TBZTITLE.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        b.Path = new PropertyPath("FSTITLEID");
                        c.Path = new PropertyPath("FSTITLENAME");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "頭銜代碼檔編號";
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "頭銜代碼檔名稱";
                        dataGrid1.ItemsSource = TBZTITLE.Entities.Where(R => R.FSTITLENAME.Contains(strCondiction));
                    };
                    break;
                case "TBZDEPT":
                    var TBZDEPT = content.Load(content.GetTBZDEPTQuery());
                    TBZDEPT.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        b.Path = new PropertyPath("FSDEPTID");
                        c.Path = new PropertyPath("FSDEPTNAME");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "部門代碼檔編號";
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "部門代碼檔名稱";
                        dataGrid1.ItemsSource = TBZDEPT.Entities.Where(R => R.FSDEPTNAME.Contains(strCondiction));
                    };
                    break;
                case "TBZPROGOBJ":
                    var dataset = content.Load(content.GetTBZPROGOBJQuery());
                    dataset.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        b.Path = new PropertyPath("FSPROGOBJID");
                        c.Path = new PropertyPath("FSPROGOBJNAME");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "製作目的代碼檔編號";
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "製作目的代碼檔名稱";
                        dataGrid1.ItemsSource = dataset.Entities.Where(R => R.FSPROGOBJNAME.Contains(strCondiction));
                    };
                    break;
                case "TBZPROGSRC":
                    var TBZPROGSRC = content.Load(content.GetTBZPROGSRCQuery());
                    TBZPROGSRC.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        b.Path = new PropertyPath("FSPROGSRCID");
                        c.Path = new PropertyPath("FSPROGSRCNAME");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "節目來源代碼檔編號";
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "節目來源代碼檔名稱";
                        dataGrid1.ItemsSource = TBZPROGSRC.Entities.Where(R => R.FSPROGSRCNAME.Contains(strCondiction));
                    };
                    break;
                case "TBZPROGATTR":
                    var TBZPROGATTR = content.Load(content.GetTBZPROGATTRQuery());
                    TBZPROGATTR.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        b.Path = new PropertyPath("FSPROGATTRID");
                        c.Path = new PropertyPath("FSPROGATTRNAME");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "內容屬性代碼檔編號";
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "內容屬性代碼檔名稱";
                        dataGrid1.ItemsSource = TBZPROGATTR.Entities.Where(R => R.FSPROGATTRNAME.Contains(strCondiction));
                    };
                    break;
                case "TBZPROGAUD":
                    var TBZPROGAUD = content.Load(content.GetTBZPROGAUDQuery());
                    TBZPROGAUD.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        b.Path = new PropertyPath("FSPROGAUDID");
                        c.Path = new PropertyPath("FSPROGAUDNAME");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "目標觀眾代碼檔編號";
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "目標觀眾代碼檔名稱";
                        dataGrid1.ItemsSource = TBZPROGAUD.Entities.Where(R => R.FSPROGAUDNAME.Contains(strCondiction));
                    };
                    break;
                case "TBZPROGTYPE":
                    var TBZPROGTYPE = content.Load(content.GetTBZPROGTYPEQuery());
                    TBZPROGTYPE.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        b.Path = new PropertyPath("FSPROGTYPEID");
                        c.Path = new PropertyPath("FSPROGTYPENAME");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "表現方式代碼檔編號";
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "表現方式代碼檔名稱";
                        dataGrid1.ItemsSource = TBZPROGTYPE.Entities.Where(R => R.FSPROGTYPENAME.Contains(strCondiction));
                    };
                    break;
                case "TBZPROGLANG":
                    var TBZPROGLANG = content.Load(content.GetTBZPROGLANGQuery());
                    TBZPROGLANG.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        b.Path = new PropertyPath("FSPROGLANGID");
                        c.Path = new PropertyPath("FSPROGLANGNAME");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "語言代碼檔編號";
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "語言代碼檔名稱";
                        dataGrid1.ItemsSource = TBZPROGLANG.Entities.Where(R => R.FSPROGLANGNAME.Contains(strCondiction));
                    };
                    break;
                case "TBZPROGBUY":
                    var TBZPROGBUY = content.Load(content.GetTBZPROGBUYQuery());
                    TBZPROGBUY.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        b.Path = new PropertyPath("FSPROGBUYID");
                        c.Path = new PropertyPath("FSPROGBUYNAME");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "外購類別代碼檔編號";
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "外購類別代碼檔名稱";
                        dataGrid1.ItemsSource = TBZPROGBUY.Entities.Where(R => R.FSPROGBUYNAME.Contains(strCondiction));
                    };
                    break;
                case "TBZPROGGRADE":
                    var TBZPROGGRADE = content.Load(content.GetTBZPROGGRADEQuery());
                    TBZPROGGRADE.Completed += (s, args) =>
                    {
                        Binding b = new Binding();
                        b.Mode = BindingMode.OneWay;
                        Binding c = new Binding();
                        c.Mode = BindingMode.OneWay;
                        b.Path = new PropertyPath("FSPROGGRADEID");
                        c.Path = new PropertyPath("FSPROGGRADENAME");
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Binding = b;
                        ((DataGridTextColumn)dataGrid1.Columns[0]).Header = "節目分級代碼檔編號";
                        //dataGrid1.Columns[0].Header = "節目分級代碼檔編號";
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Binding = c;
                        ((DataGridTextColumn)dataGrid1.Columns[1]).Header = "節目分級代碼檔名稱";
                        dataGrid1.ItemsSource = TBZPROGGRADE.Entities.Where(R => R.FSPROGGRADENAME.Contains(strCondiction));
                    };
                    break;
            }
        }
        #endregion

        private void btnUserDel_Click_1(object sender, RoutedEventArgs e)
        {
            MAG.MAG128 MAG128_Page=new MAG128();
            MAG128_Page.Show();
            MAG128_Page.Closing += (s, args) =>
                {
                    RefreshDataGridWithCondiction(MAG128_Page.strReturnCondection.ToString().Trim());
                };
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            RefreshDataGrid();
        }

    }
}
