﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.Web.ENT;

namespace PTS_MAM3.MAG
{
    public partial class MAG127 : ChildWindow
    {
        public Object senderObj;
        string tbName;
        public MAG127(string InputtbName,Object sender)
        {
            InitializeComponent();
            senderObj = sender;
            tbName = InputtbName;
            switch (tbName)
            {
                case "TBZPROGRACE_AREA":
                    textBox1.Text = ((TBZPROGRACE_AREA)senderObj).FSNAME;
                    textBox2.Text = ((TBZPROGRACE_AREA)senderObj).FSSORT;
                    checkBox1.IsChecked = ((TBZPROGRACE_AREA)senderObj).FBISENABLE;
                    break;
                case "TBZSIGNAL":
                    textBox1.Text = ((TBZSIGNAL)senderObj).FSSIGNALNAME;
                    textBox2.Text = ((TBZSIGNAL)senderObj).FSSORT;
                    checkBox1.IsChecked = ((TBZSIGNAL)senderObj).FBISENABLE;
                    break;
                case "TBZPROGSPEC":
                    textBox1.Text = ((TBZPROGSPEC)senderObj).FSPROGSPECNAME;
                    textBox2.Text = ((TBZPROGSPEC)senderObj).FSSORT;
                    checkBox1.IsChecked = ((TBZPROGSPEC)senderObj).FBISENABLE;
                    break;
                case "TBZPROMOVER":
                    textBox1.Text = ((TBZPROMOVER)senderObj).FSPROMOVERNAME;
                    textBox2.Text = ((TBZPROMOVER)senderObj).FSSORT;
                    checkBox1.IsChecked = ((TBZPROMOVER)senderObj).FBISENABLE;
                    break;
                case "TBZSHOOTSPEC":
                    textBox1.Text = ((TBZSHOOTSPEC)senderObj).FSSHOOTSPECNAME;
                    textBox2.Text = ((TBZSHOOTSPEC)senderObj).FSSORT;
                    checkBox1.IsChecked =((TBZSHOOTSPEC)senderObj).FBISENABLE;
                    break;
                case "TBPGM_ACTION":
                    textBox1.Text = ((TBPGM_ACTION)senderObj).FSACTIONNAME;
                    textBox2.Text = ((TBPGM_ACTION)senderObj).FSSORT;
                    checkBox1.IsChecked = ((TBPGM_ACTION)senderObj).FBISENABLE;
                    break;
                case "TBZBOOKING_REASON":
                    textBox1.Text = ((TBZBOOKING_REASON)senderObj).FSREASON;
                    textBox2.Text = ((TBZBOOKING_REASON)senderObj).FSSORT;
                    checkBox1.IsChecked = ((TBZBOOKING_REASON)senderObj).FBISENABLE;
                    break;
                case "TBZPROMOTYPE":
                    textBox1.Text = ((TBZPROMOTYPE)senderObj).FSPROMOTYPENAME;
                    textBox2.Text = ((TBZPROMOTYPE)senderObj).FSSORT;
                    checkBox1.IsChecked =((TBZPROMOTYPE)senderObj).FBISENABLE;
                    break;
                case "TBZPROMOCAT":
                    textBox1.Text = ((TBZPROMOCAT)senderObj).FSPROMOCATNAME;
                    textBox2.Text = ((TBZPROMOCAT)senderObj).FSSORT;
                    checkBox1.IsChecked =((TBZPROMOCAT)senderObj).FBISENABLE;
                    break;
                case "TBZPROGSTATUS":
                    textBox1.Text = ((TBZPROGSTATUS)senderObj).FSPROGSTATUSNAME;
                    textBox2.Text = ((TBZPROGSTATUS)senderObj).FSSORT;
                    checkBox1.IsChecked =((TBZPROGSTATUS)senderObj).FBISENABLE;
                    break;
                case "TBZTITLE":
                    textBox1.Text=((TBZTITLE)senderObj).FSTITLENAME;
                    textBox2.Text = ((TBZTITLE)senderObj).FSSORT;
                    checkBox1.IsChecked =((TBZTITLE)senderObj).FBISENABLE;
                    break;
                case "TBZDEPT":
                    textBox1.Text = ((TBZDEPT)senderObj).FSDEPTNAME;
                    textBox2.Text = ((TBZDEPT)senderObj).FSSORT;
                    checkBox1.IsChecked =((TBZDEPT)senderObj).FBISENABLE;
                    break;
                case "TBZPROGOBJ":
                    textBox1.Text = ((TBZPROGOBJ)senderObj).FSPROGOBJNAME;
                    textBox2.Text = ((TBZPROGOBJ)senderObj).FSSORT;
                    checkBox1.IsChecked = ((TBZPROGOBJ)senderObj).FBISENABLE;
                    break;
                case "TBZPROGSRC":
                    textBox1.Text = ((TBZPROGSRC)senderObj).FSPROGSRCNAME;
                    textBox2.Text = ((TBZPROGSRC)senderObj).FSSORT;
                    checkBox1.IsChecked =((TBZPROGSRC)senderObj).FBISENABLE;
                    break;
                case "TBZPROGATTR":
                    textBox1.Text = ((TBZPROGATTR)senderObj).FSPROGATTRNAME;
                    textBox2.Text = ((TBZPROGATTR)senderObj).FSSORT;
                    checkBox1.IsChecked =((TBZPROGATTR)senderObj).FBISENABLE;
                    break;
                case "TBZPROGAUD":
                    textBox1.Text = ((TBZPROGAUD)senderObj).FSPROGAUDNAME;
                    textBox2.Text = ((TBZPROGAUD)senderObj).FSSORT;
                    checkBox1.IsChecked = ((TBZPROGAUD)senderObj).FBISENABLE;
                    break;
                case "TBZPROGTYPE":
                    textBox1.Text = ((TBZPROGTYPE)senderObj).FSPROGTYPENAME;
                    textBox2.Text = ((TBZPROGTYPE)senderObj).FSSORT;
                    checkBox1.IsChecked = ((TBZPROGTYPE)senderObj).FBISENABLE;
                    break;
                case "TBZPROGLANG":
                    textBox1.Text = ((TBZPROGLANG)senderObj).FSPROGLANGNAME;
                    textBox2.Text = ((TBZPROGLANG)senderObj).FSSORT;
                    checkBox1.IsChecked = ((TBZPROGLANG)senderObj).FBISENABLE;
                    break;
                case "TBZPROGBUY":
                    textBox1.Text = ((TBZPROGBUY)senderObj).FSPROGBUYNAME;
                    textBox2.Text = ((TBZPROGBUY)senderObj).FSSORT;
                    checkBox1.IsChecked =((TBZPROGBUY)senderObj).FBISENABLE;
                    break;
                case "TBZPROGGRADE":
                    textBox1.Text = ((TBZPROGGRADE)senderObj).FSPROGGRADENAME;
                    textBox2.Text = ((TBZPROGGRADE)senderObj).FSSORT;
                    checkBox1.IsChecked =((TBZPROGGRADE)senderObj).FBISENABLE;
                    break;
            }
            //textBox1.Text = TBZPROGOBJ_ModifiedClass.FSPROGOBJNAME;
            //textBox2.Text = TBZPROGOBJ_ModifiedClass.FSSORT;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            int intCheck;
            if (int.TryParse(textBox2.Text.Trim(), out intCheck) && intCheck > 0 && textBox1.Text.Count() > 0)
            {
                switch (tbName)
                {
                    case "TBZPROGRACE_AREA":
                        ((TBZPROGRACE_AREA)senderObj).FSNAME = textBox1.Text.Trim();
                        ((TBZPROGRACE_AREA)senderObj).FSSORT = textBox2.Text.Trim();
                        ((TBZPROGRACE_AREA)senderObj).FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                        ((TBZPROGRACE_AREA)senderObj).FDUPDATED_DATE = DateTime.Now;
                        ((TBZPROGRACE_AREA)senderObj).FBISENABLE = checkBox1.IsChecked;
                        break;
                    case "TBZSIGNAL":
                        ((TBZSIGNAL)senderObj).FSSIGNALNAME = textBox1.Text.Trim();
                        ((TBZSIGNAL)senderObj).FSSORT = textBox2.Text.Trim();
                        ((TBZSIGNAL)senderObj).FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                        ((TBZSIGNAL)senderObj).FDUPDATED_DATE = DateTime.Now;
                        ((TBZSIGNAL)senderObj).FBISENABLE = checkBox1.IsChecked;
                        break;
                    case "TBZPROGSPEC":
                        ((TBZPROGSPEC)senderObj).FSPROGSPECNAME = textBox1.Text.Trim();
                        ((TBZPROGSPEC)senderObj).FSSORT = textBox2.Text.Trim();
                        ((TBZPROGSPEC)senderObj).FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                        ((TBZPROGSPEC)senderObj).FDUPDATED_DATE = DateTime.Now;
                        ((TBZPROGSPEC)senderObj).FBISENABLE = checkBox1.IsChecked;
                        break;
                    case "TBZPROMOVER":
                        ((TBZPROMOVER)senderObj).FSPROMOVERNAME = textBox1.Text.Trim();
                        ((TBZPROMOVER)senderObj).FSSORT = textBox2.Text.Trim();
                        ((TBZPROMOVER)senderObj).FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                        ((TBZPROMOVER)senderObj).FDUPDATED_DATE = DateTime.Now;
                        ((TBZPROMOVER)senderObj).FBISENABLE = checkBox1.IsChecked;
                        break;
                    case "TBZSHOOTSPEC":
                        ((TBZSHOOTSPEC)senderObj).FSSHOOTSPECNAME = textBox1.Text.Trim();
                        ((TBZSHOOTSPEC)senderObj).FSSORT = textBox2.Text.Trim();
                        ((TBZSHOOTSPEC)senderObj).FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                        ((TBZSHOOTSPEC)senderObj).FDUPDATED_DATE = DateTime.Now;
                        ((TBZSHOOTSPEC)senderObj).FBISENABLE = checkBox1.IsChecked;
                        break;
                    case "TBPGM_ACTION":
                        ((TBPGM_ACTION)senderObj).FSACTIONNAME = textBox1.Text.Trim();
                        ((TBPGM_ACTION)senderObj).FSSORT = textBox2.Text.Trim();
                        ((TBPGM_ACTION)senderObj).FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                        ((TBPGM_ACTION)senderObj).FDUPDATED_DATE = DateTime.Now;
                        ((TBPGM_ACTION)senderObj).FBISENABLE = checkBox1.IsChecked;
                        break;
                    case "TBZPROMOTYPE":
                        ((TBZPROMOTYPE)senderObj).FSPROMOTYPENAME = textBox1.Text.Trim();
                        ((TBZPROMOTYPE)senderObj).FSSORT = textBox2.Text.Trim();
                        ((TBZPROMOTYPE)senderObj).FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                        ((TBZPROMOTYPE)senderObj).FDUPDATED_DATE = DateTime.Now;
                        ((TBZPROMOTYPE)senderObj).FBISENABLE = checkBox1.IsChecked;
                        break;
                    case "TBZPROMOCAT":
                        ((TBZPROMOCAT)senderObj).FSPROMOCATNAME = textBox1.Text.Trim();
                        ((TBZPROMOCAT)senderObj).FSSORT = textBox2.Text.Trim();
                        ((TBZPROMOCAT)senderObj).FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                        ((TBZPROMOCAT)senderObj).FDUPDATED_DATE = DateTime.Now;
                        ((TBZPROMOCAT)senderObj).FBISENABLE = checkBox1.IsChecked;
                        break;
                    case"TBZBOOKING_REASON":
                        ((TBZBOOKING_REASON)senderObj).FSREASON = textBox1.Text.Trim();
                        ((TBZBOOKING_REASON)senderObj).FSSORT = textBox2.Text.Trim();
                        ((TBZBOOKING_REASON)senderObj).FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                        ((TBZBOOKING_REASON)senderObj).FDUPDATED_DATE = DateTime.Now;
                        ((TBZBOOKING_REASON)senderObj).FBISENABLE = checkBox1.IsChecked;
                        break;
                    case "TBZPROGSTATUS":
                        ((TBZPROGSTATUS)senderObj).FSPROGSTATUSNAME = textBox1.Text.Trim();
                        ((TBZPROGSTATUS)senderObj).FSSORT = textBox2.Text.Trim();
                        ((TBZPROGSTATUS)senderObj).FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                        ((TBZPROGSTATUS)senderObj).FDUPDATED_DATE = DateTime.Now;
                        ((TBZPROGSTATUS)senderObj).FBISENABLE = checkBox1.IsChecked;
                        break;
                    case "TBZTITLE":
                        ((TBZTITLE)senderObj).FSTITLENAME = textBox1.Text.Trim();
                        ((TBZTITLE)senderObj).FSSORT = textBox2.Text.Trim();
                        ((TBZTITLE)senderObj).FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                        ((TBZTITLE)senderObj).FDUPDATED_DATE = DateTime.Now;
                        ((TBZTITLE)senderObj).FBISENABLE = checkBox1.IsChecked;
                        break;
                    case "TBZDEPT":
                        ((TBZDEPT)senderObj).FSDEPTNAME = textBox1.Text.Trim();
                        ((TBZDEPT)senderObj).FSSORT = textBox2.Text.Trim();
                        ((TBZDEPT)senderObj).FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                        ((TBZDEPT)senderObj).FDUPDATED_DATE = DateTime.Now;
                        ((TBZDEPT)senderObj).FBISENABLE = checkBox1.IsChecked;
                        break;
                    case "TBZPROGOBJ":
                        ((TBZPROGOBJ)senderObj).FSPROGOBJNAME = textBox1.Text.Trim();
                        ((TBZPROGOBJ)senderObj).FSSORT = textBox2.Text.Trim();
                        ((TBZPROGOBJ)senderObj).FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                        ((TBZPROGOBJ)senderObj).FDUPDATED_DATE = DateTime.Now;
                        ((TBZPROGOBJ)senderObj).FBISENABLE = checkBox1.IsChecked;
                        break;
                    case "TBZPROGSRC":
                        ((TBZPROGSRC)senderObj).FSPROGSRCNAME = textBox1.Text.Trim();
                        ((TBZPROGSRC)senderObj).FSSORT = textBox2.Text.Trim();
                        ((TBZPROGSRC)senderObj).FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                        ((TBZPROGSRC)senderObj).FDUPDATED_DATE = DateTime.Now;
                        ((TBZPROGSRC)senderObj).FBISENABLE = checkBox1.IsChecked;
                        break;
                    case "TBZPROGATTR":
                        ((TBZPROGATTR)senderObj).FSPROGATTRNAME = textBox1.Text.Trim();
                        ((TBZPROGATTR)senderObj).FSSORT = textBox2.Text.Trim();
                        ((TBZPROGATTR)senderObj).FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                        ((TBZPROGATTR)senderObj).FDUPDATED_DATE = DateTime.Now;
                        ((TBZPROGATTR)senderObj).FBISENABLE = checkBox1.IsChecked;
                        break;
                    case "TBZPROGAUD":
                        ((TBZPROGAUD)senderObj).FSPROGAUDNAME = textBox1.Text.Trim();
                        ((TBZPROGAUD)senderObj).FSSORT = textBox2.Text.Trim();
                        ((TBZPROGAUD)senderObj).FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                        ((TBZPROGAUD)senderObj).FDUPDATED_DATE = DateTime.Now;
                        ((TBZPROGAUD)senderObj).FBISENABLE = checkBox1.IsChecked;
                        break;
                    case "TBZPROGTYPE":
                        ((TBZPROGTYPE)senderObj).FSPROGTYPENAME = textBox1.Text.Trim();
                        ((TBZPROGTYPE)senderObj).FSSORT = textBox2.Text.Trim();
                        ((TBZPROGTYPE)senderObj).FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                        ((TBZPROGTYPE)senderObj).FDUPDATED_DATE = DateTime.Now;
                        ((TBZPROGTYPE)senderObj).FBISENABLE = checkBox1.IsChecked;
                        break;
                    case "TBZPROGLANG":
                        ((TBZPROGLANG)senderObj).FSPROGLANGNAME = textBox1.Text.Trim();
                        ((TBZPROGLANG)senderObj).FSSORT = textBox2.Text.Trim();
                        ((TBZPROGLANG)senderObj).FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                        ((TBZPROGLANG)senderObj).FDUPDATED_DATE = DateTime.Now;
                        ((TBZPROGLANG)senderObj).FBISENABLE = checkBox1.IsChecked;
                        break;
                    case "TBZPROGBUY":
                        ((TBZPROGBUY)senderObj).FSPROGBUYNAME = textBox1.Text.Trim();
                        ((TBZPROGBUY)senderObj).FSSORT = textBox2.Text.Trim();
                        ((TBZPROGBUY)senderObj).FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                        ((TBZPROGBUY)senderObj).FDUPDATED_DATE = DateTime.Now;
                        ((TBZPROGBUY)senderObj).FBISENABLE = checkBox1.IsChecked;
                        break;
                    case "TBZPROGGRADE":
                        ((TBZPROGGRADE)senderObj).FSPROGGRADENAME = textBox1.Text.Trim();
                        ((TBZPROGGRADE)senderObj).FSSORT = textBox2.Text.Trim();
                        ((TBZPROGGRADE)senderObj).FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                        ((TBZPROGGRADE)senderObj).FDUPDATED_DATE = DateTime.Now;
                        ((TBZPROGGRADE)senderObj).FBISENABLE = checkBox1.IsChecked;
                        break;
                }
                this.DialogResult = true;
            }
            else
            {
                if (int.TryParse(textBox2.Text.Trim(), out intCheck) != true || textBox2.Text.Trim() == "" || intCheck == 0)
                { MessageBox.Show("顯示排序欄位請填入大於0以及小於或等於99的\"數字\"!"); }
                if (textBox1.Text.Trim().Count() == 0)
                { MessageBox.Show("製作目的名稱必須填入資料!"); }
            }

        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

