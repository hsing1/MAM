﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.MAG
{
    public partial class MAG320_01 : ChildWindow
    {
        SR_ServiceTranscode.ClassVTR m_FormData;

        // 從母畫面傳來的物件參數
        public MAG320_01(SR_ServiceTranscode.ClassVTR formData)
        {
            InitializeComponent();
            // 
            m_FormData = formData;
            fnConvertClassToForm();
        }

        private void btnChangeVTRStatus_Click(object sender, RoutedEventArgs e)
        {
            char isEnable = 'N';
            char isLock = 'N';

            if (radioButton1.IsChecked == true || radioButton2.IsChecked == true)
            {
                if (radioButton1.IsChecked == true)
                    isEnable = 'Y';
            }
            else
            {
                MessageBox.Show("請選擇是否啟用");
                return;
            }

            if (radioButton3.IsChecked == true || radioButton4.IsChecked == true)
            {
                if (radioButton3.IsChecked == true)
                    isLock = 'Y';
            }
            else
            {
                MessageBox.Show("請選擇是否鎖定");
                return;
            }

            SR_ServiceTranscode.ServiceTranscodeSoapClient obj = new SR_ServiceTranscode.ServiceTranscodeSoapClient();
            obj.ChangeVTRStatusCompleted += new EventHandler<SR_ServiceTranscode.ChangeVTRStatusCompletedEventArgs>(obj_ChangeVTRStatusCompleted);
            obj.ChangeVTRStatusAsync(textBox1.Text, isEnable, isLock, textBox2.Text);
        }

        // 將傳入的Class屬性帶到GUI裡面
        private void fnConvertClassToForm()
        {
            // 
            textBox1.Text = m_FormData.Name;
            // 
            textBox2.Text = m_FormData.Comment;
            // 
            if (m_FormData.isEnable == "Y")
            {
                radioButton1.IsChecked = true;
                radioButton2.IsChecked = false;
            }
            else
            {
                radioButton1.IsChecked = false;
                radioButton2.IsChecked = true;
            }
            // 
            if (m_FormData.isLock == "Y")
            {
                radioButton3.IsChecked = true;
                radioButton4.IsChecked = false;
            }
            else
            {
                radioButton3.IsChecked = false;
                radioButton4.IsChecked = true;
            }
        }

        private void obj_ChangeVTRStatusCompleted(object sender, SR_ServiceTranscode.ChangeVTRStatusCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    MessageBox.Show("更新VTR狀態的作業已完成");
                    DialogResult = true;
                }
                else
                    MessageBox.Show("執行更新VTR狀態的作業時發現錯誤，請稍後再試");
            }
            else
                MessageBox.Show("連結主機服務時發生問題：" + e.Error.ToString());
        }
    }
}
