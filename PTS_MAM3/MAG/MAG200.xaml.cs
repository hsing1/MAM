﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Telerik.Windows.Controls;
using PTS_MAM3.MAG;

namespace PTS_MAM3.SystemAdmin
{
    public partial class MAG200 : UserControl
    {
        WSUserAdmin.DeptStruct PublicDept = new WSUserAdmin.DeptStruct();
        public MAG200()
        {
            InitializeComponent();

            busyLoading1.IsBusy = true;

            preLoadDeptList(-1, null);
        }


        private void preLoadUserList(int FNDEP_ID)
        {
            WSUserAdmin.WSUserAdminSoapClient myService = new WSUserAdmin.WSUserAdminSoapClient();
            //阿福原本寫的 冠宇改
            //myService.GetUserListByDeptCompleted += (s, args) =>
            //{
            //    if (args.Error == null)
            //    {
            //        this.DGUserList.DataContext = null;
            //        if (args.Result != null)
            //        {

            //            this.DGUserList.DataContext = args.Result;
            //        }
            //    }

            //};

            //myService.GetUserListByDeptAsync(FNDEP_ID);
            myService.GetUserListByTreeViewDeptCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    this.DGUserList.DataContext = null;
                    if (args.Result != null)
                    {
                        this.DGUserList.DataContext = args.Result;
                    }
                }
            };
            myService.GetUserListByTreeViewDeptAsync(FNDEP_ID.ToString());
        }


        private void preLoadDeptList(int FNPARENT_DEP_ID, RadTreeViewItem treeNode)
        {

            WSUserAdmin.WSUserAdminSoapClient myService = new WSUserAdmin.WSUserAdminSoapClient();

            myService.GetDeptListCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {

                        //ObservableCollection<WSUserAdmin.DeptStruct> DeptStructList = args.Result;

                        foreach (WSUserAdmin.DeptStruct node in args.Result)
                        {
                            RadTreeViewItem newNode = new RadTreeViewItem();
                            newNode.Header = node.FSDEP;
                            newNode.Tag = node;

                            if (treeNode == null)
                            {
                                TreeViewDeptList.Items.Add(newNode);

                            }
                            else
                            {
                                treeNode.Items.Add(newNode);

                            }
                        }

                        if (treeNode == null)
                        {
                            busyLoading1.IsBusy = false;

                            //TreeViewDeptList.IsLoadOnDemandEnabled = false;
                        }
                        else
                        {
                            treeNode.IsLoadOnDemandEnabled = false;
                        }
                    }

                }
            };

            myService.GetDeptListAsync(FNPARENT_DEP_ID);

        }





        private void TreeViewDeptList_LoadOnDemand(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {

            WSUserAdmin.DeptStruct dept = (WSUserAdmin.DeptStruct)((RadTreeViewItem)e.Source).Tag;
            preLoadDeptList(dept.FNDEP_ID, (RadTreeViewItem)e.Source);


        }

        private void TreeViewDeptList_Selected(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {

            WSUserAdmin.DeptStruct Dept = (WSUserAdmin.DeptStruct)((RadTreeViewItem)e.Source).Tag;
            PublicDept = (WSUserAdmin.DeptStruct)((RadTreeViewItem)e.Source).Tag;
            preLoadUserList(Dept.FNDEP_ID);


            if (Dept.FNPARENT_DEP_ID == 9999)
            {
                btnUserModify.IsEnabled = true;
            }
            else
            {
                btnUserModify.IsEnabled = false;

            }

        }

        private void btnUserAdd_Click(object sender, RoutedEventArgs e)
        {
            //  TreeViewDeptList.SelectedItem = TreeViewDeptList.Items[TreeViewDeptList.Items.Count -1];
            //  ((RadTreeViewItem)TreeViewDeptList.SelectedItem).IsExpanded = true; 
            //UserAdmin_Add user_add_frm = new UserAdmin_Add();
            MAG200_04 user_add_frm = new MAG200_04(PublicDept.FNDEP_ID);
            user_add_frm.Show();

            user_add_frm.Closing += (s, args) =>
            {
                preLoadUserList(PublicDept.FNDEP_ID);
                #region 用不到的原本的阿福CODE


                //if (user_add_frm.DialogResult == true)
                //{
                //  //  UserObjectSvc user = new UserObjectSvc.UserObject(); 
                //    WSUserAdmin.UserStruct user = new WSUserAdmin.UserStruct() ; 
                //    user.FCACTIVE = "1";
                //    user.FCSECRET = "1";
                //    user.FDCREATED_DATE = DateTime.Now;
                //    user.FDUPDATED_DATE = DateTime.Now;
                //    user.FNDEP_ID = 9999;
                //    user.FNGROUP_ID = 9999;
                //    user.FNUPPERDEPT_ID = 9999;

                //    user.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                //    user.FSDEP_CHTNAME = "MAM外部使用者";
                //    user.FSDEPT = "MAM外部使用者";
                //    user.FSDESCRIPTION = user_add_frm.tbxFSDESCRIPTION.Text.Trim();
                //    user.FSEMAIL = user_add_frm.tbxFSEMAIL.Text.Trim();
                //    user.FSGROUP_CHTNAME = "MAM外部使用者";
                //    user.FSPASSWD = user_add_frm.tbxFSPASSWD.Password.Trim();
                //    user.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                //    user.FSUPPERDEPT_CHTNAME = "MAM外部使用者";
                //    user.FSUSER = user_add_frm.tbxFSUSER_ID.Text.Trim();
                //    user.FSUSER_ChtName = user_add_frm.tbxFSUSER.Text.Trim();
                //    user.FSUSER_ID = user_add_frm.tbxFSUSER_ID.Text.Trim();
                //    user.FSUSER_TITLE = user_add_frm.tbxFSUSER_TITLE.Text.Trim();



                //    WSUserAdmin.WSUserAdminSoapClient userObj = new WSUserAdmin.WSUserAdminSoapClient();

                //    userObj.AddUserAsync(user);

                //    userObj.AddUserCompleted += (s1, args1) =>
                //    {
                //        if (args1.Error != null)
                //        {
                //            showShortMessageBox("錯誤", args1.Error.ToString());
                //        }
                //        else if (args1.Result.Trim() != string.Empty)
                //        {
                //            showShortMessageBox("錯誤", args1.Result);
                //        }
                //        else
                //        {
                //            showShortMessageBox("訊息", "新增完成");
                //         //   WSUserAdmin.DeptStruct Dept = (WSUserAdmin.DeptStruct)((RadTreeViewItem)TreeViewDeptList.SelectedItem).Tag;
                //         //   preLoadUserList(Dept.FNDEP_ID); 
                //        }

                //    };

                //}
                #endregion
            };


        }

        private void showShortMessageBox(string title, Object content)
        {
            ChildWindow cw = new ChildWindow();
            cw.Title = title;

            ScrollViewer sv = new ScrollViewer();

            sv.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
            sv.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;

            sv.Content = content;

            cw.Content = sv;

            cw.MaxWidth = 480;
            cw.MaxHeight = 360;
            cw.Show();


            cw.MouseLeftButtonDown += (s, args) =>
            {
                cw.Close();
            };
            cw.KeyDown += (s, args) =>
            {
                cw.Close();
            };
            return;
        }



        private void btnUserQuery_Click(object sender, RoutedEventArgs e)
        {
            UserAdmin_Query user_query_frm = new UserAdmin_Query();
            user_query_frm.Show();


            user_query_frm.Closing += (s, args) =>
            {
                if (user_query_frm.DialogResult == true)
                {
                    if (user_query_frm._UserList != null)
                    {
                        DGUserList.DataContext = user_query_frm._UserList;
                    }
                    else
                        showShortMessageBox("訊息", "無資料符合您的查詢條件");
                }
            };

        }

        private void btnUserRefresh_Click(object sender, RoutedEventArgs e)
        {

            if (TreeViewDeptList.SelectedItem != null)
            {
                WSUserAdmin.DeptStruct Dept = (WSUserAdmin.DeptStruct)((RadTreeViewItem)TreeViewDeptList.SelectedItem).Tag;
                preLoadUserList(Dept.FNDEP_ID);
            }




        }

        private void btnUserModify_Click(object sender, RoutedEventArgs e)
        {

            WSUserAdmin.UserStruct user;

            if (DGUserList.DataContext != null)
            {
                if (DGUserList.SelectedItem == null)
                {
                    DGUserList.SelectedIndex = 0;
                }


                user = (WSUserAdmin.UserStruct)DGUserList.SelectedItem;



                UserAdmin_Modify user_modify_frm = new UserAdmin_Modify(user);
                user_modify_frm.Show();

                user_modify_frm.Closing += (s, args) =>
                {
                    //preLoadUserList(PublicDept.FNDEP_ID);//註解 by Jarvis 20141106
                    #region 用不到的阿福原本的Code


                    //if (user_modify_frm.DialogResult == true)
                    //{
                    //    user.FCACTIVE = "1";
                    //    user.FCSECRET = "1";
                    //    user.FDCREATED_DATE = DateTime.Now;
                    //    user.FDUPDATED_DATE = DateTime.Now;
                    //    user.FNDEP_ID = 9999;
                    //    user.FNGROUP_ID = 9999;
                    //    user.FNUPPERDEPT_ID = 9999;

                    //    user.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                    //    user.FSDEP_CHTNAME = "MAM外部使用者";
                    //    user.FSDEPT = "MAM外部使用者";
                    //    user.FSDESCRIPTION = user_modify_frm.tbxFSDESCRIPTION.Text.Trim();
                    //    user.FSEMAIL = user_modify_frm.tbxFSEMAIL.Text.Trim();
                    //    user.FSGROUP_CHTNAME = "MAM外部使用者";
                    //    user.FSPASSWD = user_modify_frm.tbxFSPASSWD.Password.Trim();
                    //    user.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                    //    user.FSUPPERDEPT_CHTNAME = "MAM外部使用者";
                    //    user.FSUSER = user_modify_frm.tbxFSUSER_ID.Text.Trim();
                    //    user.FSUSER_ChtName = user_modify_frm.tbxFSUSER.Text.Trim();
                    //    user.FSUSER_ID = user_modify_frm.tbxFSUSER_ID.Text.Trim();
                    //    user.FSUSER_TITLE = user_modify_frm.tbxFSUSER_TITLE.Text.Trim();

                    //    if ((string)((ComboBoxItem)(user_modify_frm.cbxFCActive.SelectedItem)).Content == "Y")                        
                    //        user.FCACTIVE = "1";                        
                    //    else
                    //        user.FCACTIVE = "0"; 


                    //    WSUserAdmin.WSUserAdminSoapClient userObj = new WSUserAdmin.WSUserAdminSoapClient();

                    //    userObj.ModifyUserAsync(user);

                    //    userObj.ModifyUserCompleted += (s1, args1) =>
                    //    {
                    //        if (args1.Error != null)
                    //        {
                    //            showShortMessageBox("錯誤", args1.Error.ToString());
                    //        }
                    //        else if (args1.Result.Trim() != string.Empty)
                    //        {
                    //            showShortMessageBox("錯誤", args1.Result);
                    //        }
                    //        else
                    //        {
                    //            showShortMessageBox("訊息", "修改完成");
                    //            WSUserAdmin.DeptStruct Dept = (WSUserAdmin.DeptStruct)((RadTreeViewItem)TreeViewDeptList.SelectedItem).Tag;
                    //            preLoadUserList(Dept.FNDEP_ID);
                    //        }

                    //    };


                    //}
                    #endregion
                };
            }



        }

        private void DGUserList_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            WSUserAdmin.UserStruct user;

            if (DGUserList.DataContext != null)
            {
                if (DGUserList.SelectedItem == null)
                {
                    DGUserList.SelectedIndex = 0;
                }

                user = (WSUserAdmin.UserStruct)DGUserList.SelectedItem;


                if (user != null)
                {
                    if (user.FSPASSWD == "//")
                    {
                        btnUserModify.IsEnabled = false;
                        btnUserGroupModify.IsEnabled = false;//By Jarvis 20141106

                    }
                    else
                    {
                        btnUserModify.IsEnabled = true;
                        btnUserGroupModify.IsEnabled = true;//By Jarvis 20141106


                    }

                }
                else
                {
                    btnUserModify.IsEnabled = false;
                }

            }



        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {

        }

        private void button1_Click_1(object sender, RoutedEventArgs e)
        {
            MAG200_05 pMAG200_05 = new MAG200_05(PublicDept.FNDEP_ID);
            pMAG200_05.Show();
            pMAG200_05.Closing += (s, args) =>
            {
                //preLoadUserList(PublicDept.FNDEP_ID);
                if (pMAG200_05.DialogResult == true)
                {
                    MessageBox.Show("新增成功");

                    //TreeViewDeptList.CollapseAll();
                    
                    TreeViewDeptList.Items.Clear();
                    //TreeViewDeptList.IsLoadOnDemandEnabled = true;
                    preLoadDeptList(-1, null);
                }
                else if (pMAG200_05.DialogResult == false)
                {
                    MessageBox.Show("取消新增");
                }
            };
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            MAG200_06 pMAG200_06 = new MAG200_06();
            pMAG200_06.Show();
            pMAG200_06.Closing += (s, args) =>
            { preLoadUserList(1); };
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            MAG200_07 pMAG200_07 = new MAG200_07(PublicDept.FNDEP_ID);
            pMAG200_07.Show();
            pMAG200_07.Closing += (s, args) =>
            {
                if (pMAG200_07.DialogResult == true)
                {
                }
            };
        }

        private void btnUserGroupModify_Click(object sender, RoutedEventArgs e)
        {
            WSUserAdmin.UserStruct user;
            if (DGUserList.DataContext != null)
            {
                if (DGUserList.SelectedItem == null)
                {
                    DGUserList.SelectedIndex = 0;
                }

                user = (WSUserAdmin.UserStruct)DGUserList.SelectedItem;

                MAG200_08 modifyGroup = new MAG200_08(user);
                modifyGroup.Closing += (s, args) => 
                {
                    if (modifyGroup.DialogResult == true) 
                    {
                    
                    }
                };
                modifyGroup.Show();


            }
        }



    }
}
