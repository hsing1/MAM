﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using System.Windows.Threading;
using Telerik.Windows.Controls.Charting;


namespace PTS_MAM3.SystemAdmin
{

    public class SystemLoadInfo
    {
        private DateTime _time;
        private double _cpuLoad;
        private double _memUsage;
        private double _networkSend;
        private double _networkRead;
        private int _webUsers;

        public int WebUsers
        {
            get
            {
                return this._webUsers; 
            }
            set
            {
                this._webUsers = value; 
            }
        }


        public double NetworkSend
        {
            get
            {
                return this._networkSend; 
            }
            set
            {
                this._networkSend = value; 
            }
        }

        public double NetworkRead
        {
            get
            {
                return this._networkRead;
            }
            set
            {
                this._networkRead = value;
            }
        }


        public DateTime Time
        {
            get
            {
                return this._time;
            }
            set
            {
                this._time = value;
            }
        }
        public double CPULoad
        {
            get
            {
                return this._cpuLoad;
            }
            set
            {
                this._cpuLoad = value;
            }
        }
        public double MemUsage
        {
            get
            {
                return this._memUsage;
            }
            set
            {
                this._memUsage = value;
            }
        }
    }



    public partial class SystemMonitor : UserControl
    {

        private const int queueCapacity = 30;
        
        private Queue<SystemLoadInfo> cpuData = new Queue<SystemLoadInfo>(queueCapacity);
        private DateTime nowTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
        private DispatcherTimer timer1, timer2;


        public SystemMonitor()
        {
            InitializeComponent();
            this.SetUpRadChart();

            this.SetUpPrimaryYAxis();
            this.SetUpSecondaryYAxis();
       
            this.SetUpXAxis();
            this.SetUpNetworkMapping();
            this.SetUpCpuLoadMapping();
            this.SetUpMemoryUsageMapping();
            
            
        }

        private void SetUpRadChart()
        {
            RadChart1.DefaultView.ChartTitle.Content = "CPU & Memory 監控";
            RadChart1.DefaultView.ChartArea.NoDataString = "Waiting for data...";
            RadChart1.DefaultView.ChartArea.EnableAnimations = false;

            RadChart2.DefaultView.ChartTitle.Content = "Network 監控";
            RadChart2.DefaultView.ChartArea.NoDataString = "Waiting for data...";
            RadChart2.DefaultView.ChartArea.EnableAnimations = false;            

        }

        private void SetUpXAxis()
        {
            //RadChart1.DefaultView.ChartArea.AxisX.DefaultLabelFormat = "#VAL{mm:ss}";
            RadChart1.DefaultView.ChartArea.AxisX.DefaultLabelFormat = "#VAL{mm:ss}";
            RadChart1.DefaultView.ChartArea.AxisX.LabelRotationAngle = 270;
            RadChart1.DefaultView.ChartArea.AxisX.LabelStep = 2;
            RadChart1.DefaultView.ChartArea.AxisX.Title = "Time{分:秒}";
            RadChart1.DefaultView.ChartArea.AxisX.LayoutMode = AxisLayoutMode.Normal;
            RadChart1.DefaultView.ChartArea.AxisX.AutoRange = false;


            RadChart2.DefaultView.ChartArea.AxisX.DefaultLabelFormat = "#VAL{mm:ss}";
            RadChart2.DefaultView.ChartArea.AxisX.LabelRotationAngle = 270;
            RadChart2.DefaultView.ChartArea.AxisX.LabelStep = 2;
            RadChart2.DefaultView.ChartArea.AxisX.Title = "Time{分:秒}";
            RadChart2.DefaultView.ChartArea.AxisX.LayoutMode = AxisLayoutMode.Normal;
            RadChart2.DefaultView.ChartArea.AxisX.AutoRange = false;

        }

        private void SetUpPrimaryYAxis()
        {

            RadChart1.DefaultView.ChartArea.AxisY.AutoRange = false;
            RadChart1.DefaultView.ChartArea.AxisY.MinValue = 0;
            RadChart1.DefaultView.ChartArea.AxisY.MaxValue = 1;
            RadChart1.DefaultView.ChartArea.AxisY.Step = 0.2;            
            RadChart1.DefaultView.ChartArea.AxisY.DefaultLabelFormat = "#VAL{p2}";
            RadChart1.DefaultView.ChartArea.AxisY.Title = "CPU %";


            RadChart2.DefaultView.ChartArea.AxisY.AutoRange = true;
            RadChart2.DefaultView.ChartArea.AxisY.MinValue = 0;
            RadChart2.DefaultView.ChartArea.AxisY.MaxValue = 100000;
            RadChart2.DefaultView.ChartArea.AxisY.Step = 20000;
            RadChart2.DefaultView.ChartArea.AxisY.DefaultLabelFormat = "#VAL KB/S";
            RadChart2.DefaultView.ChartArea.AxisY.Title = "Send KB/S";

              
        }


   

        private void SetUpSecondaryYAxis()
        {
            AxisY ramAxis = new AxisY();
            ramAxis.AxisName = "RAM";
            ramAxis.Title = "Free memory";
            ramAxis.AutoRange = true;
            //ramAxis.MinValue = 1000;
            //ramAxis.MaxValue = 10240;
            //ramAxis.Step = 200;
            
            ramAxis.DefaultLabelFormat = "#VAL MB";
            RadChart1.DefaultView.ChartArea.AdditionalYAxes.Add(ramAxis);



            AxisY nwRead = new AxisY();
            nwRead.AxisName = "Network Read";
            nwRead.Title = "Read KB/S";
          
            nwRead.AutoRange = true; 
            nwRead.MinValue = 0;
            nwRead.MaxValue = 100000;
            nwRead.Step = 20000;

            nwRead.DefaultLabelFormat = "#VAL KB/S";
            RadChart2.DefaultView.ChartArea.AdditionalYAxes.Add(nwRead);

        }

        
        private void SetUpAxisXRange(DateTime now)
        {
            RadChart1.DefaultView.ChartArea.AxisX.MinValue = now.AddSeconds(-14.5).ToOADate();
            RadChart1.DefaultView.ChartArea.AxisX.MaxValue = now.ToOADate();
            RadChart1.DefaultView.ChartArea.AxisX.Step = 1.0 / 24.0 / 3600.0 / 2;

            RadChart2.DefaultView.ChartArea.AxisX.MinValue = now.AddSeconds(-14.5).ToOADate();
            RadChart2.DefaultView.ChartArea.AxisX.MaxValue = now.ToOADate();
            RadChart2.DefaultView.ChartArea.AxisX.Step = 1.0 / 24.0 / 3600.0 / 2;
        }


        private void SetUpTimer()
        {
            this.UpdateGridViewDate(); 
            timer1 = new DispatcherTimer();
            timer1.Interval = TimeSpan.FromMilliseconds(500);
            timer1.Tick += timer1_Tick;

            timer2 = new DispatcherTimer();
            timer2.Interval = TimeSpan.FromMilliseconds(30000);
            timer2.Tick += timer2_Tick;  
        }



        void timer1_Tick(object sender, EventArgs e)
        {
           // timer1.Stop();
           // this.nowTime = this.nowTime.AddMilliseconds(500);
            this.nowTime = DateTime.Now; 
            this.UpdateData(this.nowTime);

            this.SetUpAxisXRange(this.nowTime);

            RadChart1.ItemsSource = null;
            RadChart1.ItemsSource = this.cpuData;

            RadChart2.ItemsSource = null;
            RadChart2.ItemsSource = this.cpuData; 
        }

        void timer2_Tick(object sender, EventArgs e)
        {
            timer2.Stop();

            this.UpdateGridViewDate(); 


            //timer2.Start();
        }

        private void UpdateGridViewDate()
        {
            if (_fsserver_name != string.Empty)
            {
                WSServerMonitor.WSServerMonitorSoapClient MonitorSrv = new WSServerMonitor.WSServerMonitorSoapClient();
                MonitorSrv.GetHDWebUserCompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (args.Result != null)
                        {
                            DGHDWebUser.DataContext = args.Result; 
                        }
                    }

                    timer2.Start();
                };

                MonitorSrv.GetHDWebUserAsync(_fsserver_name);

            }
        }

        private void UpdateData(DateTime now)
        {
            if (_fsserver_name != string.Empty)
            {
                WSServerMonitor.WSServerMonitorSoapClient MonitorSrv = new WSServerMonitor.WSServerMonitorSoapClient();
               

                MonitorSrv.GetServerDetailByNameCompleted += (s, args) =>
                {

                    if (args.Error == null ) {
                        if (args.Result != null ) {
                            WSServerMonitor.ServerDetailStruct sysDetail = (WSServerMonitor.ServerDetailStruct)args.Result; 
                            if (this.cpuData.Count >= queueCapacity)
                                this.cpuData.Dequeue();
    
                            SystemLoadInfo systemInfo = new SystemLoadInfo();
    
                            systemInfo.CPULoad = Convert.ToDouble(sysDetail.FSCPU)  / 100 ;
                            systemInfo.Time = now;
                            systemInfo.MemUsage = Convert.ToDouble(sysDetail.FSMEMORY);
                            systemInfo.NetworkRead = Convert.ToDouble(sysDetail.FSREAD_NETWORK) ;
                            systemInfo.NetworkSend = Convert.ToDouble(sysDetail.FSSEND_NETWORK) ;
                            systemInfo.WebUsers = Convert.ToInt32(sysDetail.FSWEB_USERS); 

                            this.cpuData.Enqueue(systemInfo);
                        }

                    }

                    timer1.Start();
                    
                };
                MonitorSrv.GetServerDetailByNameAsync(_fsserver_name);
            
            }

        }


        private void SetUpNetworkMapping()
        {

            SeriesMapping NetworkDataMapping = new SeriesMapping();
            NetworkDataMapping.LegendLabel = "Network Send";
            NetworkDataMapping.SeriesDefinition = new LineSeriesDefinition();
            NetworkDataMapping.SeriesDefinition.ItemLabelFormat = "Time: #X{hh:mm:ss}";
            NetworkDataMapping.SeriesDefinition.ShowItemLabels = false;
            NetworkDataMapping.SeriesDefinition.Appearance.Fill = new SolidColorBrush(Color.FromArgb(128, 0, 128, 255));
            NetworkDataMapping.SeriesDefinition.Appearance.Stroke = new SolidColorBrush(Color.FromArgb(255, 0, 128, 255));
            NetworkDataMapping.SeriesDefinition.Appearance.StrokeThickness = 1;
            (NetworkDataMapping.SeriesDefinition as LineSeriesDefinition).ShowPointMarks = false;
            (NetworkDataMapping.SeriesDefinition as LineSeriesDefinition).ShowItemLabels = false;
            NetworkDataMapping.ItemMappings.Add(new ItemMapping("Time", DataPointMember.XValue));
            NetworkDataMapping.ItemMappings.Add(new ItemMapping("NetworkSend", DataPointMember.YValue));
            RadChart2.SeriesMappings.Add(NetworkDataMapping);


        


            SeriesMapping NetworkDataMapping2 = new SeriesMapping();
            NetworkDataMapping2.LegendLabel = "Network Read";
            NetworkDataMapping2.SeriesDefinition = new LineSeriesDefinition();
            NetworkDataMapping2.SeriesDefinition.ItemLabelFormat = "Time: #X{hh:mm:ss}";
            NetworkDataMapping2.SeriesDefinition.ShowItemLabels = false; 
            (NetworkDataMapping2.SeriesDefinition as LineSeriesDefinition).ShowPointMarks = false;
            (NetworkDataMapping2.SeriesDefinition as LineSeriesDefinition).ShowItemLabels = false;
            NetworkDataMapping2.SeriesDefinition.AxisName = "Network Read";
            //NetworkDataMapping2.SeriesDefinition.AxisName = "Network read";
            NetworkDataMapping2.ItemMappings.Add(new ItemMapping("Time", DataPointMember.XValue));
            NetworkDataMapping2.ItemMappings.Add(new ItemMapping("NetworkRead", DataPointMember.YValue));
            RadChart2.SeriesMappings.Add(NetworkDataMapping2);
            
            
        }


        private void SetUpMemoryUsageMapping()
        {
            SeriesMapping memoryDataMapping = new SeriesMapping();
            memoryDataMapping.LegendLabel = "Free Mem";
            memoryDataMapping.SeriesDefinition = new LineSeriesDefinition();
            (memoryDataMapping.SeriesDefinition as LineSeriesDefinition).ShowPointMarks = false;
            (memoryDataMapping.SeriesDefinition as LineSeriesDefinition).ShowItemLabels = false;
            memoryDataMapping.SeriesDefinition.AxisName = "RAM";
            memoryDataMapping.ItemMappings.Add(new ItemMapping("Time", DataPointMember.XValue));
            memoryDataMapping.ItemMappings.Add(new ItemMapping("MemUsage", DataPointMember.YValue));
            RadChart1.SeriesMappings.Add(memoryDataMapping);
            
        }

        private void SetUpCpuLoadMapping()
        {
            SeriesMapping cpuDataMapping = new SeriesMapping();
            cpuDataMapping.LegendLabel = "CPU Utilization";
            cpuDataMapping.SeriesDefinition = new LineSeriesDefinition();
            cpuDataMapping.SeriesDefinition.ItemLabelFormat = "Time: #X{hh:mm:ss}";
            cpuDataMapping.SeriesDefinition.ShowItemLabels = false;
            cpuDataMapping.SeriesDefinition.Appearance.Fill = new SolidColorBrush(Color.FromArgb(128, 0, 128, 255));
            cpuDataMapping.SeriesDefinition.Appearance.Stroke = new SolidColorBrush(Color.FromArgb(255, 0, 128, 255));
            cpuDataMapping.SeriesDefinition.Appearance.StrokeThickness = 1;
            (cpuDataMapping.SeriesDefinition as LineSeriesDefinition).ShowPointMarks = false;
            (cpuDataMapping.SeriesDefinition as LineSeriesDefinition).ShowItemLabels = false;
            cpuDataMapping.ItemMappings.Add(new ItemMapping("Time", DataPointMember.XValue));
            cpuDataMapping.ItemMappings.Add(new ItemMapping("CPULoad", DataPointMember.YValue));
            RadChart1.SeriesMappings.Add(cpuDataMapping);
        }



#region "TreeViewServerList" 
        private void TreeViewServerList_LoadOnDemand(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            WSServerMonitor.WSServerMonitorSoapClient Srv1 = new WSServerMonitor.WSServerMonitorSoapClient();


            string fsgroup = string.Empty;

            if (((RadTreeViewItem)(e.Source)).Tag != null)
            {
                try
                {
                    fsgroup = ((RadTreeViewItem)(e.Source)).Tag.ToString();
                }
                catch (Exception ex)
                {
                    fsgroup = string.Empty; 
                }

            }


            if (fsgroup != string.Empty)
            {
                Srv1.GetServerListByGroupAsync(fsgroup);
            }

            Srv1.GetServerListByGroupCompleted += (s, args) =>
            {

                RadTreeViewItem CurrentNode = (RadTreeViewItem)e.Source;
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        foreach (WSServerMonitor.ServerStruct sysObj in args.Result)
                        {
                            RadTreeViewItem newNode = new RadTreeViewItem();
                            newNode.Header = sysObj.FSSERVER_NAME + " (" + sysObj.FSSERVER_IP + ")";

                            if (sysObj.FCAVAILABLE == "Y")                            
                                newNode.DefaultImageSrc = new Uri("../Images/16_server_lightning.png", UriKind.Relative);
                            else
                                newNode.DefaultImageSrc = new Uri("../Images/16_server_delete.png", UriKind.Relative);
                            
                            newNode.Tag = sysObj;
                            CurrentNode.Items.Add(newNode);
                        }
                    }
                }
                CurrentNode.IsLoadOnDemandEnabled = false;
            };


        }


        private void TreeViewServerList_Selected(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            RadTreeViewItem node = (RadTreeViewItem)e.Source;
            WSServerMonitor.ServerStruct sysObj;

            if (node.Tag != null)
            {
                if (node.Tag.GetType() != Type.GetType("System.String"))
                {
                    try
                    {
                       
                        sysObj = (WSServerMonitor.ServerStruct)node.Tag;


                        

                        _fsserver_name = sysObj.FSSERVER_NAME;
                                           

                      
                        ShowPerformanceCounter((WSServerMonitor.ServerStruct)node.Tag);
                       
                      
                    }
                    catch (Exception ex)
                    {
                        return;
                    }
                }
            }
        }
     
#endregion 

        string _fsserver_name = string.Empty;
        private void ShowPerformanceCounter(WSServerMonitor.ServerStruct sysObj)
        {
            this.cpuData.Clear();

            DGHDWebUser.DataContext = null;


            if (timer1 != null)
            {
                timer1.Stop();
            }

            if (timer2 != null)
            {
                timer2.Stop();
            }


        

            this.SetUpTimer();

            if (timer1 != null)
                timer1.Start();

            //if (timer2 != null)
            //    timer2.Start();
       
        }

    }
}
