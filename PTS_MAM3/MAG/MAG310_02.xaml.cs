﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.SR_WSPGM_MSSProcess_Management;


namespace PTS_MAM3
{    //段落置換修改頁面
    public partial class MAG310_02 : ChildWindow
    {
        SR_WSPGM_MSSProcess_Management.WSPGM_MSSProcess_ManagementSoapClient
            client = new SR_WSPGM_MSSProcess_Management.WSPGM_MSSProcess_ManagementSoapClient();

        WSBROADCAST.WSBROADCASTSoapClient broadCastClient = new WSBROADCAST.WSBROADCASTSoapClient();

        Class_MssProcessDetail currentData;
        SR_WSPGM_MSSProcess_Management.ClassMSSProcessJob selectedJob;
        int timeCodeStartFrame = 0;//新的TimeCode轉成Frame的值
        string timeCodeStart = "";//紀錄新的TimeCode
        int timeCodeEndFrame = 0;
        string timeCodeEnd = "";
        int timeDuration = 0;//新的TimeCode的起訖區間(Frame)

        int oldTimeCodeFrame_Start = 0;//舊的TimeCode轉成Frame的值
        int oldTimeCodeFrame_End = 0;

        int segCount = 0;//段落計數、如果是0就是只有自己本身1段
        int segIndex = -1;//檔案有多個段落時，選取段落的index
        string fileTimeCodeTop = "";//檔案有多個段落時,第一筆的起
        string fileTimeCodeBottom = "";//檔案有多個段落時,最後一筆的迄

       public string errorText = "";

       public MAG310_02(SR_WSPGM_MSSProcess_Management.ClassMSSProcessJob job)
        {
            InitializeComponent();

            broadCastClient.GetTBLOG_VIDEO_SEG_COUNTCompleted += new EventHandler<WSBROADCAST.GetTBLOG_VIDEO_SEG_COUNTCompletedEventArgs>(broadCastClient_GetTBLOG_VIDEO_SEG_COUNTCompleted);
            selectedJob = job;
            client.UPDATE_TBLOG_VIDEO_SEG_ReplacementCompleted += new EventHandler<UPDATE_TBLOG_VIDEO_SEG_ReplacementCompletedEventArgs>(client_UPDATE_TBLOG_VIDEO_SEG_ReplacementCompleted);

            client.INSERT_LOG_VIDEO_SEG_CHGCompleted += new EventHandler<INSERT_LOG_VIDEO_SEG_CHGCompletedEventArgs>(client_INSERT_LOG_VIDEO_SEG_CHGCompleted);

            client.Update_MSSPROCCESS_TunningCompleted += new EventHandler<Update_MSSPROCCESS_TunningCompletedEventArgs>(client_Update_MSSPROCCESS_TunningCompleted);

            client.GetMssProcessDetailCompleted += new EventHandler<SR_WSPGM_MSSProcess_Management.GetMssProcessDetailCompletedEventArgs>(client_GetMssProcessDetailCompleted);
            client.GetMssProcessDetailAsync(job.VideoID);
        }

       void client_Update_MSSPROCCESS_TunningCompleted(object sender, Update_MSSPROCCESS_TunningCompletedEventArgs e)
       {
           if(e.Error==null)
               this.DialogResult = e.Result;
       }

        //新增資料進TBLOG_VIDEO_SEG_CHG的結果 by Jarvis20130605
        void client_INSERT_LOG_VIDEO_SEG_CHGCompleted(object sender, INSERT_LOG_VIDEO_SEG_CHGCompletedEventArgs e)
        {
            if (e.Error == null)
            {

                client.Update_MSSPROCCESS_TunningAsync(selectedJob.MSSProcessJobID, selectedJob.VideoID, UserClass.userData.FSUSER_ID);
            }
            else 
            {
                errorText += e.Error.Message + Environment.NewLine;
            }
        }
        //更新TBLOG_VIDEO_SEG，確定完成後才INSERT進_LOG_VIDEO_SEG_CHG
        void client_UPDATE_TBLOG_VIDEO_SEG_ReplacementCompleted(object sender, UPDATE_TBLOG_VIDEO_SEG_ReplacementCompletedEventArgs e)
        {
           
            if (e.Error == null)
            {
                if (e.Result)
                {
                    client.INSERT_LOG_VIDEO_SEG_CHGAsync(currentData, UserClass.userData.FSUSER_ID);
                }
                else
                {//開發提示用
                    MessageBox.Show("檢查SP");
                }
            }
            else
            {
                errorText += e.Error.Message + Environment.NewLine;
            }
        }

        //取得該筆段落的詳細資料
        void client_GetMssProcessDetailCompleted(object sender, SR_WSPGM_MSSProcess_Management.GetMssProcessDetailCompletedEventArgs e)
        {

            if (e.Error == null)
            {
                if (e.Result.Count == 1)
                {
                    this.DataContext = e.Result[0];
                    currentData = e.Result[0];
                    currentData.FDCREATED_DATE =selectedJob.MSSProcessJobID.Substring(0, 8);
                    //將原本的TimeCode轉成Frame，用來計算修改幅度
                    //因為舊的Frame的格式一定是對的才會儲存起來
                    //所以可以不用理會檢查格式的結果，直接丟一個字串給方法即可
                    string temp = "";
                    oldTimeCodeFrame_Start = TimeCodeToFrameMethod(e.Result[0].FSBEG_TIMECODE, out temp);
                    oldTimeCodeFrame_End = TimeCodeToFrameMethod(e.Result[0].FSEND_TIMECODE, out temp);
                    UpdateTBValue(currentData.FSBEG_TIMECODE, false);
                    UpdateTBValue(currentData.FSEND_TIMECODE, true);

                    textBox1.Focus();
                    textBox1.SelectAll();
                    
                    broadCastClient.GetTBLOG_VIDEO_SEG_COUNTAsync(e.Result[0].FSFILE_NO);
                }
                else
                {//開發提示用
                 //   MessageBox.Show("傳回" + e.Result.Count + "筆");
                }
            }
            else
            {
                errorText += e.Error.Message + Environment.NewLine;
            }
        }

        //尋找該筆段落為第幾段，並記錄所屬檔案的起迄
        void broadCastClient_GetTBLOG_VIDEO_SEG_COUNTCompleted(object sender, WSBROADCAST.GetTBLOG_VIDEO_SEG_COUNTCompletedEventArgs e)
        {   //該檔案的段落數
            segCount = e.Result.Count;

            if (segCount > 0)
            {   //第一段TimeCode起
                fileTimeCodeTop = e.Result[0].FSBEG_TIMECODE;
                //最後一段的TimeCode的迄
                fileTimeCodeBottom = e.Result[e.Result.Count - 1].FSEND_TIMECODE;

                //尋找選取段落是該檔案的第幾段
                for (int i = 0; i < segCount; i++)
                {
                    if (e.Result[i].FSVIDEO_ID == currentData.FSVIDEO_ID)
                    {
                        segIndex = i;
                    }
                }
            }
        }

     
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            //檢查輸入的值
            if (!checkFormate())
            {
                return;
            }

            string timeCode = "";
            bool flage = true;
            foreach (UIElement ui in this.NewTimeCodeGrid.Children)
            {
                if (ui is TextBox)
                    timeCode += ui.GetValue(TextBox.TextProperty).ToString();
                else if (ui is TextBlock)
                    timeCode += ui.GetValue(TextBlock.TextProperty).ToString();

                if (timeCode.Length == 11)
                {
                    if (flage)
                    {//獲取輸入的TimeCode「起」
                        timeCodeStart = timeCode;
                        string outresult = "";
                        timeCodeStartFrame = TimeCodeToFrameMethod(timeCode, out outresult);

                        //如果outresult不是空字串,表示TimeCode格式有錯並自動修改過
                        //需要把更改過的值在Update回UI上
                        if (outresult != "")
                        {
                            string temp = "";
                            timeCodeStart=outresult;
                            timeCodeStartFrame = TimeCodeToFrameMethod(outresult, out temp);

                            UpdateTBValue(outresult, false);
                        }
                        flage = false;
                    }
                    else
                    {//獲取輸入的TimeCode「迄」
                        timeCodeEnd = timeCode;
                        string outresult = "";
                        timeCodeEndFrame = TimeCodeToFrameMethod(timeCode, out outresult);

                        //如果outresult不是空字串,表示TimeCode格式有錯並自動修改過
                        //需要把更改過的值在Update回UI上
                        if (outresult != "")
                        {
                            string temp = "";
                            timeCodeEnd=outresult;
                            timeCodeEndFrame = TimeCodeToFrameMethod(outresult, out temp);

                            UpdateTBValue(outresult, true);
                        }

                        //資料都沒變動直接檔掉
                        if (timeCodeStart == currentData.FSBEG_TIMECODE && timeCodeEnd == currentData.FSEND_TIMECODE)
                        {
                            MessageBox.Show("沒有修改任何資料!");
                            this.DialogResult = true;
                            return;
                        }

                        //計算起訖間隔
                        if (timeCodeEndFrame != -1 && timeCodeStartFrame != -1)
                            timeDuration = timeCodeEndFrame - timeCodeStartFrame;
                        
                        if (timeDuration < 0)
                        {
                            MessageBox.Show("Time Code (迄) 必須大於Time Code (起)");
                            return;
                        }
                        
                        //檢查修改的間距與位置
                        if (CheckModififyAmount())
                        { 
                            //檢查都通過，準備寫入資料庫
                            currentData.FSBEG_TIMECODE_NEW = timeCodeStart;
                            currentData.FSEND_TIMECODE_NEW = timeCodeEnd;
                           
                            client.UPDATE_TBLOG_VIDEO_SEG_ReplacementAsync
                                (currentData,UserClass.userData.FSUSER_ID.ToString());
                        }
                    }
                    //因為是用同一個變數重複計算，所以一定要清空,變數timeCode
                    //免得起迄會連在同一個字串中
                    timeCode = "";
                }
            }
        }

        //檢查修改的間距與位置
        bool CheckModififyAmount()
        {
            string temp = "";
            int topFrame = TimeCodeToFrameMethod(fileTimeCodeTop, out temp);

            int bottomFrame = TimeCodeToFrameMethod(fileTimeCodeBottom, out temp);

            //修改第一段TimeCode的起不可以在原段落之前(會吃到其他節目)
            if (timeCodeStartFrame < topFrame &&segIndex==0) 
            {
              MessageBox.Show("TimeCode「起」不得在原檔案段落之前", "提示訊息", MessageBoxButton.OK);
              return false;
            }
            else if (timeCodeEndFrame > bottomFrame&&(segIndex==segCount-1))
            {//修改最後一段TimeCode的迄不可以在原段落之後(會吃到其他節目)
                MessageBox.Show("TimeCode「迄」不得在原檔案段落之後", "提示訊息", MessageBoxButton.OK);
                return false;
            }

            int startAmount = Math.Abs(timeCodeStartFrame - oldTimeCodeFrame_Start);
            int endAmount = Math.Abs(timeCodeEndFrame - oldTimeCodeFrame_End);

            if ((startAmount + endAmount) > 5)
            {
                MessageBox.Show("修改間距不得超過5個Frame!", "提示訊息", MessageBoxButton.OK);
                return false;
            }


            return true;
        }

        //檢查輸入的值
        bool checkFormate()
        {
            string errorTextS = "";
            string errorTextE = "";
            bool flag = false;

            //檢查每一個欄位輸入的值
            foreach (UIElement ui in this.NewTimeCodeGrid.Children)
            {
                if (ui is TextBox)
                {
                    int validate = 0;
                    string text = ui.GetValue(TextBox.TextProperty).ToString();
                    if (!int.TryParse(text, out validate) || text.Length < 2)
                    {
                        int index = this.NewTimeCodeGrid.Children.IndexOf(ui);
                        if (index < 7)
                        {
                            errorTextS += (index / 2 + 1).ToString() + ",";

                        }
                        else
                        {
                            errorTextE += (index / 2 - 2).ToString() + ",";
                        }
                        flag = true;//標記有發生過錯誤
                    }
                }
            }

            if (flag)
            {
                if (errorTextS != "")
                {
                    errorTextS = "TimeCode欄位「起」第" + errorTextS.Remove(errorTextS.Length - 1) + "格式不符";
                }

                if (errorTextE != "")
                {
                    errorTextE = "TimeCode欄位「迄」第" + errorTextE.Remove(errorTextE.Length - 1) + "格式不符";
                }

                MessageBox.Show(errorTextS + Environment.NewLine + errorTextE);
                return false;
            }
            return true;
        }

        //檢查TimeCode格式，並轉成Frame
        public static int TimeCodeToFrameMethod(string Timecode, out string NewTimecode)
        {
            NewTimecode = "";
            string errorText = "";
            string[] firstArr = Timecode.Split(new char[] { ':', ';' });
            int HH = Int32.Parse(firstArr[0]);
            int MM = Int32.Parse(firstArr[1]);
            int SS = Int32.Parse(firstArr[2]);
            int FF = Int32.Parse(firstArr[3]);
            int totalFrame = 0;

          
           if (!TransferTimecode.check_timecode_format(firstArr[0], firstArr[1], firstArr[2], firstArr[3], out errorText)) 
           {
               if (errorText == "所輸入的時間不符合DropFrame的格式")
               {
                   FF = 02;
                   totalFrame = TransferTimecode.timecodetoframe(String.Format("{0:00}", HH) + ":" + String.Format("{0:00}", MM) + ":" + String.Format("{0:00}", SS) + ";" + String.Format("{0:00}", FF));
                   NewTimecode = TransferTimecode.frame2timecode(totalFrame);

                   if (errorText != "")
                   {
                       MessageBox.Show(errorText);
                       //MessageBox.Show(Timecode + Environment.NewLine + "自動改成" + Environment.NewLine + String.Format("{0:00}", HH) + ":" + String.Format("{0:00}", MM) + ":" + String.Format("{0:00}", SS) + ";" + String.Format("{0:00}", FF));
                       MessageBox.Show(Timecode + Environment.NewLine + "自動改成" + Environment.NewLine + NewTimecode);

                       //NewTimecode = String.Format("{0:00}", HH) + ":" + String.Format("{0:00}", MM) + ":" + String.Format("{0:00}", SS) + ":" + String.Format("{0:00}", FF);
                       // totalFrame = Convert.ToInt32((HH * 3600 + MM * 60 + SS) * 29.97) + FF;

                       return -1;
                   }
               }
           }
            return totalFrame;
        }

        //將TimeCode更新回UI上
        void UpdateTBValue(string timeCode, bool isEnd)
        {
            int charIndex = 0;//計算每次要切的字元index
            int ControlIndex = 0;//計算控制項在容器裡的index 
            string[] timeCodeArr = new string[timeCode.Length / 2 - 1];
            for (int i = 0; i < timeCode.Length / 2 - 1; i++)
            {
                timeCodeArr[i] = timeCode.Substring(charIndex, 2);
                charIndex += 3;
            }

            if (isEnd)
            {//如果是迄,index從7開始算
                ControlIndex = 7;
            }

            for (int i = 0; i < (this.NewTimeCodeGrid.Children.Count / 2) - 3; i++)
            {
                this.NewTimeCodeGrid.Children[ControlIndex].SetValue(TextBox.TextProperty, timeCodeArr[i]);
                ControlIndex += 2;
            }
        }

        //自動移動Focus
        private void textBox1_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = ((TextBox)sender);
            int length = tb.Text.Length;

            if (length == tb.MaxLength)
            {
                int index = this.NewTimeCodeGrid.Children.IndexOf(tb);
                int childrenCount = this.NewTimeCodeGrid.Children.Count;
                if (index == 6)
                {
                    ((TextBox)this.NewTimeCodeGrid.Children[index + 1]).Focus();
                    ((TextBox)this.NewTimeCodeGrid.Children[index + 1]).SelectAll();
                }
                else if (index <= childrenCount - 3)
                {
                    ((TextBox)this.NewTimeCodeGrid.Children[index + 2]).Focus();
                    ((TextBox)this.NewTimeCodeGrid.Children[index + 2]).SelectAll();
                }
                else
                    this.OKButton.Focus();
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

