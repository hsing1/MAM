﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;

namespace PTS_MAM3.MAG
{
    public partial class MAG330 : Page
    {
        SR_ServiceTranscode.ServiceTranscodeSoapClient clntTranscode = new SR_ServiceTranscode.ServiceTranscodeSoapClient();
        List<SR_ServiceTranscode.clLOG_WORKER> lstLOG_WORKER = new List<SR_ServiceTranscode.clLOG_WORKER>();

        public MAG330()
        {
            InitializeComponent();
            InitializeForm();
        }

        // 初始化本頁面
        private void InitializeForm()
        {
            date1.Text = DateTime.Now.AddMonths(-1).ToString("yyyy/M/d");
            date2.Text = DateTime.Now.ToString("yyyy/M/d");

            clntTranscode.GetTBLOG_WORKER_ByDatesCompleted += new EventHandler<SR_ServiceTranscode.GetTBLOG_WORKER_ByDatesCompletedEventArgs>(clntTranscode_GetTBLOG_WORKER_ByDatesCompleted);
            clntTranscode.GetTBLOG_WORKER_ByDatesAsync(DateTime.Parse(date1.Text), DateTime.Parse(date2.Text), "MAMSYSTEM", "");
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }



        private void btnDetail_Click(object sender, RoutedEventArgs e)
        {
            var ctl = e.OriginalSource as Button;
            if (ctl != null)
            {
                SR_ServiceTranscode.clLOG_WORKER clLOG_WORKER = (SR_ServiceTranscode.clLOG_WORKER)(ctl.DataContext);

                //宣告出新增子視窗
                MAG330_01 frmMAG330_01 = new MAG330_01(clLOG_WORKER);
                frmMAG330_01.Show();

                frmMAG330_01.Closed += (s, args) =>
                {
                    if (frmMAG330_01._ResetOK == true)
                    {
                        //子視窗修改成功後,重新整理清單
                        clntTranscode.GetTBLOG_WORKER_ByDatesAsync(DateTime.Parse(date1.Text), DateTime.Parse(date2.Text), "MAMSYSTEM", "");
                    }
                };
            }
        }

        private void btnQryAll_Click(object sender, RoutedEventArgs e)
        {
            if (date1.Text.Trim() == "" || date2.Text.Trim() == "")
            {
                MessageBox.Show("請設定要篩選的起迄日期", "提示訊息", MessageBoxButton.OK);
                return;
            }

            clntTranscode.GetTBLOG_WORKER_ByDatesAsync(DateTime.Parse(date1.Text), DateTime.Parse(date2.Text), "MAMSYSTEM", "");
        }

        private void btnQry1_Click(object sender, RoutedEventArgs e)
        {
            if (date1.Text.Trim() == "" || date2.Text.Trim() == "")
            {
                MessageBox.Show("請設定要篩選的起迄日期", "提示訊息", MessageBoxButton.OK);
                return;
            }

            clntTranscode.GetTBLOG_WORKER_ByDatesAsync(DateTime.Parse(date1.Text), DateTime.Parse(date2.Text), "MAMSYSTEM", "1");
        }

        private void btnQry2_Click(object sender, RoutedEventArgs e)
        {
            if (date1.Text.Trim() == "" || date2.Text.Trim() == "")
            {
                MessageBox.Show("請設定要篩選的起迄日期", "提示訊息", MessageBoxButton.OK);
                return;
            }

            clntTranscode.GetTBLOG_WORKER_ByDatesAsync(DateTime.Parse(date1.Text), DateTime.Parse(date2.Text), "MAMSYSTEM", "2");
        }

        private void btnQry3_Click(object sender, RoutedEventArgs e)
        {
            if (date1.Text.Trim() == "" || date2.Text.Trim() == "")
            {
                MessageBox.Show("請設定要篩選的起迄日期", "提示訊息", MessageBoxButton.OK);
                return;
            }

            clntTranscode.GetTBLOG_WORKER_ByDatesAsync(DateTime.Parse(date1.Text), DateTime.Parse(date2.Text), "MAMSYSTEM", "3");
        }



        private void dgResult_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            SR_ServiceTranscode.clLOG_WORKER obj = e.Row.DataContext as SR_ServiceTranscode.clLOG_WORKER;
            TextBlock txt = (TextBlock)dgResult.Columns[4].GetCellContent(e.Row);

            switch (obj.FSRESULT)
            {
                case "Fail":
                    txt.Foreground = new SolidColorBrush(Colors.Red);
                    break;
                case "處理中":
                    txt.Foreground = new SolidColorBrush(Colors.Green);
                    break;
                case "Success":
                    txt.Foreground = new SolidColorBrush(Colors.Blue);
                    break;
                default:
                    txt.Foreground = new SolidColorBrush(Colors.Orange);
                    break;
            }

        }



        private void clntTranscode_GetTBLOG_WORKER_ByDatesCompleted(object sender, SR_ServiceTranscode.GetTBLOG_WORKER_ByDatesCompletedEventArgs e)
        {
            //如果取回的資料沒有錯誤訊息
            if (e.Error == null)
            {
                //如果取回的資料筆數不為0
                if (e.Result.Count != 0)
                {
                    //把查詢結果丟入公用成員lstNews中, 方便母視窗取用
                    lstLOG_WORKER = new List<SR_ServiceTranscode.clLOG_WORKER>(e.Result);
                    dgResult.ItemsSource = e.Result;
                    MessageBox.Show("符合篩選條件的資料共 " + e.Result.Count + " 筆", "提示訊息", MessageBoxButton.OK);
                }
                else
                {
                    //如果取回的資料筆數=0
                    MessageBox.Show("輸入的篩選條件找不到相關的資料", "提示訊息", MessageBoxButton.OK);
                }
            }
            else
            {
                //如果取回的資料有錯誤訊息
                MessageBox.Show("發生錯誤: " + e.Error.ToString(), "提示訊息", MessageBoxButton.OK);
            }
        }
    }
}
