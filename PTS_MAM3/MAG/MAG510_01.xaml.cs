﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.MAG
{
    public partial class MAG510_01 : ChildWindow
    {
        SR_WSPGM_GetMssStatus.ClassFTPJob m_FormData;

        // 從母畫面傳來的物件參數
        public MAG510_01(string videoID)
        {
            InitializeComponent();
            // 查詢資料
            fnRegetFtpStatus(videoID);
        }

        private void button_RestartFTP1_Click(object sender, RoutedEventArgs e)
        {
            fnRetryFtpJob(m_FormData.VideoID, "1");
        }

        private void button_RestartFTP2_Click(object sender, RoutedEventArgs e)
        {
            fnRetryFtpJob(m_FormData.VideoID, "2");
        }

        private void button_Close_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void button_Refresh_Click(object sender, RoutedEventArgs e)
        {
            fnRegetFtpStatus(m_FormData.VideoID);
        }

        private void fnDisableButtons()
        {
            button_Refresh.IsEnabled = false;
            button_Close.IsEnabled = false;
            button_RestartFTP1.IsEnabled = false;
            button_RestartFTP2.IsEnabled = false;
        }

        private void fnEnableButtons()
        {
            button_Refresh.IsEnabled = true;
            button_Close.IsEnabled = true;
            button_RestartFTP1.IsEnabled = true;
            button_RestartFTP2.IsEnabled = true;
        }

        private void fnRetryFtpJob(string videoID, string ftpNo)
        {
            fnDisableButtons();
            SR_WSPGM_GetMssStatus.ServicePGMSoapClient obj = new SR_WSPGM_GetMssStatus.ServicePGMSoapClient();
            obj.ResetMasterControlFtpJobCompleted += new EventHandler<SR_WSPGM_GetMssStatus.ResetMasterControlFtpJobCompletedEventArgs>(obj_ResetMasterControlFtpJobCompleted);
            obj.ResetMasterControlFtpJobAsync(videoID, ftpNo);
        }

        private void fnRegetFtpStatus(string videoID)
        {
            fnDisableButtons();
            SR_WSPGM_GetMssStatus.ServicePGMSoapClient obj = new SR_WSPGM_GetMssStatus.ServicePGMSoapClient();
            obj.GetMasterControlFtpDetailStatusCompleted += new EventHandler<SR_WSPGM_GetMssStatus.GetMasterControlFtpDetailStatusCompletedEventArgs>(obj_GetMasterControlFtpDetailStatusCompleted);
            obj.GetMasterControlFtpDetailStatusAsync(videoID);
        }

        // 將物件的屬性綁到介面中
        private void fnBindClassAttributesToForm()
        {
            if (m_FormData != null)
            {
                // Global
                textBox1.Text = m_FormData.VideoID;
                // Ftp1 Info
                textBox2.Text = m_FormData.Ftp1Status;
                textBox3.Text = m_FormData.Ftp1ErrMsg;
                textBox4.Text = m_FormData.Ftp1StartExecTime;
                textBox5.Text = m_FormData.Ftp1FinishExecTime;
                textBox6.Text = string.Concat(m_FormData.Ftp1FileSizeFTP, " / ", m_FormData.Ftp1FileSizeNAS);
                // Ftp2 Info
                textBox7.Text = m_FormData.Ftp2Status;
                textBox8.Text = m_FormData.Ftp2ErrMsg;
                textBox9.Text = m_FormData.Ftp2StartExecTime;
                textBox10.Text = m_FormData.Ftp2FinishExecTime;
                textBox11.Text = string.Concat(m_FormData.Ftp2FileSizeFTP, " / ", m_FormData.Ftp2FileSizeNAS);
                textBox12.Text = m_FormData.Note;
            }
        }

        private void obj_ResetMasterControlFtpJobCompleted(object sender, SR_WSPGM_GetMssStatus.ResetMasterControlFtpJobCompletedEventArgs e)
        {
            // 
            fnEnableButtons();
            // 
            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    MessageBox.Show("重置FTP作業已完成");
                    fnRegetFtpStatus(m_FormData.VideoID);
                }
                else
                    MessageBox.Show("重置FTP作業時發生錯誤，請重試");
            }
            else
                MessageBox.Show("連結主機服務時發生問題：" + e.Error.ToString());
        }

        private void obj_GetMasterControlFtpDetailStatusCompleted(object sender, SR_WSPGM_GetMssStatus.GetMasterControlFtpDetailStatusCompletedEventArgs e)
        {
            // 
            fnEnableButtons();
            // 
            if (e.Error == null)
            {
                if (e.Result == null)
                {
                    // 理論上會到這邊，只有 VideoID 輸入錯誤，但 VideoID 其實是由母視窗帶來的，不太可能會錯
                    MessageBox.Show("輸入錯誤的VideoID，請重新查詢。");
                    this.DialogResult = true;
                }
                else
                {
                    // 將現有的值重新配置
                    m_FormData = e.Result;
                    fnBindClassAttributesToForm();
                }
            }
            else
                MessageBox.Show("連結主機服務時發生問題：" + e.Error.ToString());
        }
    }
}
