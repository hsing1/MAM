﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using System.Xml.Linq;
using System.IO;
using System.Collections.ObjectModel;

namespace PTS_MAM3.SystemAdmin
{
    public partial class TemplateField_Add : ChildWindow
    {
        string _fstable = "";
        int _fntemplate_id ;
        string _fsfield = "";

        public TemplateField_Add(ObservableCollection<WSDirectoryAdmin.TBTemplate_FieldsStruct> ds, string fstable, int fntemplate_id, string fsfield)
        {
            InitializeComponent();
            _fsfield = fsfield;
            _fstable = fstable;
            _fntemplate_id = fntemplate_id; 
            initialCbxFSFIELD(ds,_fstable, _fntemplate_id, _fsfield);


        }

        private void initialCbxFSFIELD(ObservableCollection<WSDirectoryAdmin.TBTemplate_FieldsStruct> ds, string fstable, int fntemplate_id, string fsfield)
        {
            WSDirectoryAdmin.WSDirectoryAdminSoapClient dirObj = new WSDirectoryAdmin.WSDirectoryAdminSoapClient();
            dirObj.fnGetTBTEMPLATE_FIELDS_FSFIELDCompleted += (e, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                       // StringBuilder output = new StringBuilder();
                    // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        //var ltox = from s in doc.Elements("Datas").Elements("Data")
                        //    select new List<string>(){
                        //        (string)s.Element("FSFIELD")
                        //};

                        int attrCnt = 0 ;
                        if (fstable == "TBSUBJECT")
                            attrCnt = 50;                        
                        else
                            attrCnt = 50;

                        for (int i = 1; i <= attrCnt; i++)
                        {
                            cbxFSFIELD.Items.Add("FSATTRIBUTE" + i );
                        }


                        var ltox = from s in doc.Elements("Datas").Elements("Data")
                                   select
                                   (string)s.Element("FSFIELD");

                        // this.cbxFSFIELD.ItemsSource = ltox;

                        

                        foreach (string s in ltox)
                        {
                            this.cbxFSFIELD.Items.Remove(s) ; 
                        }





                        if (this.cbxFSFIELD.Items.Count > 0)
                        {
                            this.cbxFSFIELD.SelectedIndex = 0;
                        }
                      

                    }
                }
            };
            dirObj.fnGetTBTEMPLATE_FIELDS_FSFIELDAsync(fstable, fntemplate_id, fsfield); 
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (tbxFIELD_NAME.Text.Trim() == string.Empty){
                MessageBox.Show("請輸入名稱","錯誤!", MessageBoxButton.OK);
                tbxFIELD_NAME.Focus() ; 
            }
            else if ((tbxFNFIELD_LENGTH.Value.ToString() == string.Empty) && ((string) ((System.Windows.Controls.ContentControl)((cbxFSFIELD_TYPE.SelectedItem))).Content == "nvarchar") || ((string)((System.Windows.Controls.ContentControl)((cbxFSFIELD_TYPE.SelectedItem))).Content == "nchar"))
            {              
                 MessageBox.Show("請輸入欄位長度", "錯誤!", MessageBoxButton.OK);
                 tbxFNFIELD_LENGTH.Focus();                
            }
            else if (tbxFNOBJECT_WIDTH.Value.ToString() == string.Empty)
            {

                MessageBox.Show("請輸入元件寬度", "錯誤!", MessageBoxButton.OK);
                tbxFNOBJECT_WIDTH.Focus();
            }
            else
            {
                WSDirectoryAdmin.TBTemplate_FieldsStruct tf = new WSDirectoryAdmin.TBTemplate_FieldsStruct();

                tf.FCGRID = "N";

                if (this.cbFCISNULLABLE.IsChecked == true)
                    tf.FCISNULLABLE = "Y";
                else
                    tf.FCISNULLABLE = "N";

                tf.FCLIST = "0";
                tf.FCLIST_TYPE = "N";
                tf.FNFIELD_LENGTH = Convert.ToInt32(tbxFNFIELD_LENGTH.Value);
                tf.FNOBJECT_WIDTH = Convert.ToInt32(tbxFNOBJECT_WIDTH.Value);
                tf.FNORDER = 0;
                tf.FNTEMPLATE_ID = _fntemplate_id;
                tf.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                tf.FSDEFAULT = this.tbxFSDEFAULT.Text;
                tf.FSDESCRIPTION = "";
                tf.FSFIELD = (string) this.cbxFSFIELD.SelectedItem;
                tf.FSFIELD_NAME = tbxFIELD_NAME.Text;
                tf.FSFIELD_TYPE = (string)((System.Windows.Controls.ContentControl)((cbxFSFIELD_TYPE.SelectedItem))).Content;
                tf.FSLIST_NAME = "";
                tf.FSTABLE = _fstable;
                tf.FSUPDATED_BY = UserClass.userData.FSUSER_ID;




                WSDirectoryAdmin.WSDirectoryAdminSoapClient dirObj = new WSDirectoryAdmin.WSDirectoryAdminSoapClient();

                dirObj.fnInsertTBTEMPLATE_FIELDSCompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (args.Result == false)
                        {
                            MessageBox.Show("資料新增時發生錯誤!", "錯誤", MessageBoxButton.OK); 

                        }
                        else
                            this.DialogResult = true;
                    }
                    
                };

                
                dirObj.fnInsertTBTEMPLATE_FIELDSAsync(tf, UserClass.userData.FSUSER_ID); 
                
                
            }
             
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

