﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;

namespace PTS_MAM3.MAG
{
    public partial class MAG310 : Page
    {
          SR_WSPGM_MSSProcess_Management.WSPGM_MSSProcess_ManagementSoapClient objMSSProcess = new SR_WSPGM_MSSProcess_Management.WSPGM_MSSProcess_ManagementSoapClient();
       
        public MAG310()
        {
            InitializeComponent();

            objMSSProcess.GetOneDayMssProcessJobCompleted += new EventHandler<SR_WSPGM_MSSProcess_Management.GetOneDayMssProcessJobCompletedEventArgs>(objMSSProcess_GetOneDayMssProcessJobCompleted);

            // 初始化 DatePicker 的值
            dpQuery.Text = DateTime.Now.ToString("yyyy/M/d");
        
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //
        }

        private void DGJobList_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            // 預設正常的前景、背景色
            e.Row.Background = new SolidColorBrush(Colors.White);
            e.Row.Foreground = new SolidColorBrush(Colors.Black);

            // Tricky: 檢查狀態，如果狀態有含"ERR"字樣的，就要變色!
            SR_WSPGM_MSSProcess_Management.ClassMSSProcessJob obj = e.Row.DataContext as SR_WSPGM_MSSProcess_Management.ClassMSSProcessJob;
            if (!string.IsNullOrEmpty(obj.MSSProcessJobStatus) && obj.MSSProcessJobStatus.Contains("ERR"))
                e.Row.Background = new SolidColorBrush(Colors.Red);
        }

        /// <summary>
        /// 顯示詳細資訊的按鈕
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDetail_Click(object sender, RoutedEventArgs e)
        {
            // 判斷是否有選取DataGrid
            if (DGJobList.SelectedItem == null)
            {
                MessageBox.Show("請選擇要查詢的項目", "提示訊息", MessageBoxButton.OK);
                return;
            }

            // 開啟子視窗
            MAG310_01 MAG310_01_frm = new MAG310_01(((SR_WSPGM_MSSProcess_Management.ClassMSSProcessJob)DGJobList.SelectedItem));
            MAG310_01_frm.Show();
            MAG310_01_frm.Closing += (s, args) =>
            {
                if (MAG310_01_frm.DialogResult == true)
                {
                    // 重新讀取主畫面的資料
                    objMSSProcess.GetOneDayMssProcessJobAsync(dpQuery.Text, 1);
                }
            };
        }

        /// <summary>
        /// 查詢的按鈕
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnQuery_Click(object sender, RoutedEventArgs e)
        {
            // 檢查輸入的時間參數
            if (string.IsNullOrEmpty(dpQuery.Text))
            {
                MessageBox.Show("選擇的時間參數有誤");
                return;
            }

            // 處理畫面
            fnDisableButtons();

            // 呼叫後端的 WS 服務，以取得要呈現的資料
            SR_WSPGM_MSSProcess_Management.WSPGM_MSSProcess_ManagementSoapClient objMSSProcess = new SR_WSPGM_MSSProcess_Management.WSPGM_MSSProcess_ManagementSoapClient();
            objMSSProcess.GetOneDayMssProcessJobCompleted += new EventHandler<SR_WSPGM_MSSProcess_Management.GetOneDayMssProcessJobCompletedEventArgs>(objMSSProcess_GetOneDayMssProcessJobCompleted);
            objMSSProcess.GetOneDayMssProcessJobAsync(dpQuery.Text);
        }

        /// <summary>
        /// 匯出檔案
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            // 檢查輸入的時間參數
            if (string.IsNullOrEmpty(dpQuery.Text))
            {
                MessageBox.Show("選擇的時間參數有誤");
                return;
            }

            // 處理畫面
            fnDisableButtons();
        }

        private void fnDisableButtons()
        {
            btnQuery.IsEnabled = false;
            btnDetail.IsEnabled = false;
        }

        private void fnEnableButtons()
        {
            btnQuery.IsEnabled = true;
            btnDetail.IsEnabled = true;
        }

        private void objMSSProcess_GetOneDayMssProcessJobCompleted(object sender, SR_WSPGM_MSSProcess_Management.GetOneDayMssProcessJobCompletedEventArgs e)
        {
            //
            fnEnableButtons();
            DGJobList.DataContext = null;
            //
            if (e.Error == null)
            {
                if (e.Result.Count == 0)
                {
                    MessageBox.Show("查不到該範圍區間的資料");
                    return;
                }
                // 
                DGJobList.DataContext = e.Result;
            }
            else
                MessageBox.Show("連結主機服務時發生問題：" + e.Error.ToString());
        }

        //2013/06/03 調整Frame按鈕按下時
        private void TuningBtn_Click(object sender, RoutedEventArgs e)
        {
            // 判斷是否有選取DataGrid
            if (DGJobList.SelectedItem == null)
            {
                MessageBox.Show("請選擇要修改的項目", "提示訊息", MessageBoxButton.OK);
                return;
            }
       
          SR_WSPGM_MSSProcess_Management.ClassMSSProcessJob job=(SR_WSPGM_MSSProcess_Management.ClassMSSProcessJob)DGJobList.SelectedItem;
          if (job.MSSProcessJobStatus =="ERR_TRANS")//MSSProcessJobStatusC = "執行轉主控播出檔時發生錯誤"; //修改 by Jarvis20130816
          {
              MAG310_02 mag310_2 = new MAG310_02(job);
              mag310_2.Show();

              mag310_2.Closing += (s, args) => 
              {
                  if (mag310_2.DialogResult == false)
                  {
                      MessageBox.Show("修改失敗:" + mag310_2.errorText);
                  }

                  // 重新讀取主畫面的資料
                  objMSSProcess.GetOneDayMssProcessJobAsync(dpQuery.Text, 1);

              };

          }
          else 
          {
              MessageBox.Show("狀態為「"+job.MSSProcessJobStatusC+"」的項目無法修改", "提示訊息", MessageBoxButton.OK);
              return;          
          }


        }
    }
}
