﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.MAG
{
    public partial class MAG901_01 : ChildWindow
    {
        WSTBZ.WSTBZSoapClient TBZClient = new WSTBZ.WSTBZSoapClient();
        WSTBZ.Class_TBZPROGNAION myClass = new WSTBZ.Class_TBZPROGNAION();
        public MAG901_01()
        {
            InitializeComponent();
            DelegateCompleteEvent();
        }

        private void DelegateCompleteEvent()
        {
            TBZClient.Q_TBZPROGNATIONCompleted += (sTBZ, argsTBZ) =>
                {
                    if (argsTBZ.Result.Where(S => S.FSNATION_ID == tbFSNATION_ID.Text.Trim()).Count() > 0)
                        MessageBox.Show("代碼重複請重新輸入不重複代碼!!");
                    else
                        TBZClient.I_TBZPROGNATIONAsync(myClass);//如果代碼ID與資料庫內ID不重複 則新增 國家來源
                };
            TBZClient.I_TBZPROGNATIONCompleted += (sI, argsI) =>
                {
                    if (argsI.Result == true)
                        this.DialogResult = true;
                    else
                        MessageBox.Show("新增失敗請聯絡管理員!");
                };
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (checkFieldFill())   //檢查欄位是否都填妥
            {
                TBZClient.Q_TBZPROGNATIONAsync();   //檢查代碼檔是否重複
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private bool checkFieldFill()
        {
            bool rtnbool = false;
            if (tbFSNATION_ID.Text != "" && tbFSNATION_NAME.Text != "" && tbFSSORT.Text != "")
            {
                myClass.FSNATION_ID = tbFSNATION_ID.Text.Trim();
                myClass.FSNATION_NAME = tbFSNATION_NAME.Text.Trim();
                myClass.FSSORT = tbFSSORT.Text.Trim();
                myClass.FBISENABLE = "1";
                myClass.FSCREATE_BY = UserClass.userData.FSUSER_ID;
                myClass.FSUPDATE_BY = UserClass.userData.FSUSER_ID;
                rtnbool = true;
            }
            else
                MessageBox.Show("請填入所有欄位");
            return rtnbool;
        }
    }
}

