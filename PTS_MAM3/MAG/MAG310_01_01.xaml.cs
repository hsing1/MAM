﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.MAG
{
    public partial class MAG310_01_01 : ChildWindow
    {
        SR_WSPGM_MSSProcess_Management.ClassMSSProcessJob m_FormData;

        // 傳入的物件參數
        public MAG310_01_01(SR_WSPGM_MSSProcess_Management.ClassMSSProcessJob FormData)
        {
            InitializeComponent();
            // 
            m_FormData = FormData;
            // 初始化介面

        }

        private void btnRestart_Click(object sender, RoutedEventArgs e)
        {
            bool isForceTranscode = false;
            bool isForceCopyFile = false;

            // 檢查 checkBox 上的鉤選項目
            if (chkBox_ForceTranscode.IsChecked == true)
                isForceTranscode = true;
            if (chkBox_ForceCopy.IsChecked == true)
                isForceCopyFile = true;

            // 發送 Web Request 到後端做處理
            SR_WSPGM_MSSProcess_Management.WSPGM_MSSProcess_ManagementSoapClient obj = new SR_WSPGM_MSSProcess_Management.WSPGM_MSSProcess_ManagementSoapClient();
            obj.RetryMssProcessJobCompleted += new EventHandler<SR_WSPGM_MSSProcess_Management.RetryMssProcessJobCompletedEventArgs>(objMSSProcess_RetryMssProcessJobCompleted);
            obj.RetryMssProcessJobAsync(m_FormData.MSSProcessJobID, isForceTranscode, isForceCopyFile);
        }

        private void objMSSProcess_RetryMssProcessJobCompleted(object sender, SR_WSPGM_MSSProcess_Management.RetryMssProcessJobCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    MessageBox.Show("重啟任務的命令已被執行");
                    // 完成就自動關掉本視窗
                    this.DialogResult = true;
                }
                else
                    MessageBox.Show("發送重啟任務的命令時發生錯誤");
            }
            else
                MessageBox.Show("連結主機服務時發生問題：" + e.Error.ToString());
        }

        // 只要有一個 CheckBox 被選取，就顯示提示訊息
        private void chkBox_Checked(object sender, RoutedEventArgs e)
        {
            if (chkBox_ForceCopy.IsChecked == true || chkBox_ForceTranscode.IsChecked == true)
                label1.Visibility = System.Windows.Visibility.Visible;
            else
                label1.Visibility = System.Windows.Visibility.Collapsed;
        }
    }
}
