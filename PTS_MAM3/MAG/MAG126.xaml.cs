﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.Web.ENT;

namespace PTS_MAM3.MAG
{
    public partial class MAG126 : ChildWindow
    {
        private ENT100DomainContext content = new ENT100DomainContext();
        private string tbName;
        public MAG126(string InputtbName)
        {
            InitializeComponent();
            tbName = InputtbName;
            BiggestSort();
        }
        
        #region 把最大的Sort抓出來
        private void BiggestSort()
        {
            int numId = 0;
            string strId;
            switch (tbName)
            {
                case "TBZPROGRACE_AREA":
                    var TBZPROGRACE_AREA = content.Load(content.GetTBZPROGRACE_AREAQuery());
                    TBZPROGRACE_AREA.Completed += (s, args) =>
                    {
                        foreach (TBZPROGRACE_AREA x in TBZPROGRACE_AREA.Entities)
                        {
                            if (x.FSSORT.Trim() != "")
                            {
                                if (Convert.ToInt32(x.FSSORT) > numId)
                                { numId = Convert.ToInt32(x.FSSORT); }
                            }
                        }
                        numId += 1;
                        if (numId <= 9)
                        {
                            strId = "000" + numId.ToString();
                        }                 
                        else if (numId>9 && numId <= 99)
                        {
                            strId = "00" + numId.ToString();
                        }
                        else if (numId > 99 && numId <= 999)
                        { strId = "0" + numId.ToString(); }
                        else if (numId > 999 && numId <= 9999)
                        { strId = numId.ToString(); }
                        else //(numId > 9999)
                        { strId = "9999"; }
                        textBox2.Text = strId;
                    };
                    break;
                case "TBZSIGNAL":
                    var TBZSIGNAL = content.Load(content.GetTBZSIGNALQuery());
                    TBZSIGNAL.Completed += (s, args) =>
                    {
                        foreach (TBZSIGNAL x in TBZSIGNAL.Entities)
                        {
                            if (x.FSSORT.Trim() != "")
                            {
                                if (Convert.ToInt32(x.FSSORT) > numId)
                                { numId = Convert.ToInt32(x.FSSORT); }
                            }
                        }
                        numId += 1;
                        if (numId <= 9)
                        {
                            strId = "0" + numId.ToString();
                        }
                        else if (numId >= 99)
                        { strId = "99"; }
                        else
                        { strId = numId.ToString(); }
                        textBox2.Text = strId;
                    };
                    break;
                case "TBZPROGSPEC":
                    var TBZPROGSPEC = content.Load(content.GetTBZPROGSPECQuery());
                    TBZPROGSPEC.Completed += (s, args) =>
                    {
                        foreach (TBZPROGSPEC x in TBZPROGSPEC.Entities)
                        {
                            if (x.FSSORT.Trim() != "")
                            {
                                if (Convert.ToInt32(x.FSSORT) > numId)
                                { numId = Convert.ToInt32(x.FSSORT); }
                            }
                        }
                        numId += 1;
                        if (numId <= 9)
                        {
                            strId = "0" + numId.ToString();
                        }
                        else if (numId >= 99)
                        { strId = "99"; }
                        else
                        { strId = numId.ToString(); }
                        textBox2.Text = strId;
                    };
                    break;
                case "TBZPROMOVER":
                    var TBZPROMOVER = content.Load(content.GetTBZPROMOVERQuery());
                    TBZPROMOVER.Completed += (s, args) =>
                    {
                        foreach (TBZPROMOVER x in TBZPROMOVER.Entities)
                        {
                            if (x.FSSORT.Trim() != "")
                            {
                                if (Convert.ToInt32(x.FSSORT) > numId)
                                { numId = Convert.ToInt32(x.FSSORT); }
                            }
                        }
                        numId += 1;
                        if (numId <= 9)
                        {
                            strId = "0" + numId.ToString();
                        }
                        else if (numId >= 99)
                        { strId = "99"; }
                        else
                        { strId = numId.ToString(); }
                        textBox2.Text = strId;
                    };
                    break;
                case "TBZSHOOTSPEC":
                    var TBZSHOOTSPEC = content.Load(content.GetTBZSHOOTSPECQuery());
                    TBZSHOOTSPEC.Completed += (s, args) =>
                    {
                        foreach (TBZSHOOTSPEC x in TBZSHOOTSPEC.Entities)
                        {
                            if (x.FSSORT.Trim() != "")
                            {
                                if (Convert.ToInt32(x.FSSORT) > numId)
                                { numId = Convert.ToInt32(x.FSSORT); }
                            }
                        }
                        numId += 1;
                        if (numId <= 9)
                        {
                            strId = "0" + numId.ToString();
                        }
                        else if (numId >= 99)
                        { strId = "99"; }
                        else
                        { strId = numId.ToString(); }
                        textBox2.Text = strId;
                    };
                    break;
                case "TBPGM_ACTION":
                    var TBPGM_ACTION = content.Load(content.GetTBPGM_ACTIONQuery());
                    TBPGM_ACTION.Completed += (s, args) =>
                    {
                        foreach (TBPGM_ACTION x in TBPGM_ACTION.Entities)
                        {
                            if (x.FSSORT.Trim() != "")
                            {
                                if (Convert.ToInt32(x.FSSORT) > numId)
                                { numId = Convert.ToInt32(x.FSSORT); }
                            }
                        }
                        numId += 1;
                        if (numId <= 9)
                        {
                            strId = "0" + numId.ToString();
                        }
                        else if (numId >= 99)
                        { strId = "99"; }
                        else
                        { strId = numId.ToString(); }
                        textBox2.Text = strId;
                    };
                    break;
                case "TBZPROMOTYPE":
                    var TBZPROMOTYPE = content.Load(content.GetTBZPROMOTYPEQuery());
                    TBZPROMOTYPE.Completed += (s, args) =>
                    {
                        foreach (TBZPROMOTYPE x in TBZPROMOTYPE.Entities)
                        {
                            if (x.FSSORT.Trim() != "")
                            {
                                if (Convert.ToInt32(x.FSSORT) > numId)
                                { numId = Convert.ToInt32(x.FSSORT); }
                            }
                        }
                        numId += 1;
                        if (numId <= 9)
                        {
                            strId = "0" + numId.ToString();
                        }
                        else if (numId >= 99)
                        { strId = "99"; }
                        else
                        { strId = numId.ToString(); }
                        textBox2.Text = strId;
                    };
                    break;
                case "TBZPROMOCAT":
                    var TBZPROMOCAT = content.Load(content.GetTBZPROMOCATQuery());
                    TBZPROMOCAT.Completed += (s, args) =>
                    {
                        foreach (TBZPROMOCAT x in TBZPROMOCAT.Entities)
                        {
                            if (x.FSSORT.Trim() != "")
                            {
                                if (Convert.ToInt32(x.FSSORT) > numId)
                                { numId = Convert.ToInt32(x.FSSORT); }
                            }
                        }
                        numId += 1;
                        if (numId <= 9)
                        {
                            strId = "0" + numId.ToString();
                        }
                        else if (numId >= 99)
                        { strId = "99"; }
                        else
                        { strId = numId.ToString(); }
                        textBox2.Text = strId;
                    };
                    break;
                case"TBZBOOKING_REASON":
                    var TBZBOOKING_REASON = content.Load(content.GetTBZBOOKING_REASONQuery());
                    TBZBOOKING_REASON.Completed += (s, args) =>
                    {
                        foreach (TBZBOOKING_REASON x in TBZBOOKING_REASON.Entities)
                        {
                            if (x.FSSORT.Trim() != "")
                            {
                                if (Convert.ToInt32(x.FSSORT) > numId)
                                { numId = Convert.ToInt32(x.FSSORT); }
                            }
                        }
                        numId += 1;
                        if (numId <= 9)
                        {
                            strId = "0" + numId.ToString();
                        }
                        else if (numId >= 99)
                        { strId = "99"; }
                        else
                        { strId = numId.ToString(); }
                        textBox2.Text = strId;
                    };
                    break;
                case "TBZPROGSTATUS":
                    var TBZPROGSTATUS = content.Load(content.GetTBZPROGSTATUSQuery());
                    TBZPROGSTATUS.Completed += (s, args) =>
                    {
                        foreach (TBZPROGSTATUS x in TBZPROGSTATUS.Entities)
                        {
                            if (x.FSSORT.Trim() != "")
                            {
                                if (Convert.ToInt32(x.FSSORT) > numId)
                                { numId = Convert.ToInt32(x.FSSORT); }
                            }
                        }
                        numId += 1;
                        if (numId <= 9)
                        {
                            strId = "0" + numId.ToString();
                        }
                        else if (numId >= 99)
                        { strId = "99"; }
                        else
                        { strId = numId.ToString(); }
                        textBox2.Text = strId;
                    };
                    break;
                case "TBZTITLE":
                    var TBZTITLE = content.Load(content.GetTBZTITLEQuery());
                    TBZTITLE.Completed += (s, args) =>
                    {
                        foreach (TBZTITLE x in TBZTITLE.Entities)
                        {
                            if (x.FSSORT.Trim() != "")
                            {
                                if (Convert.ToInt32(x.FSSORT) > numId)
                                { numId = Convert.ToInt32(x.FSSORT); }
                            }
                        }
                        numId += 1;
                        if (numId <= 9)
                        {
                            strId = "0" + numId.ToString();
                        }
                        else if (numId >= 99)
                        { strId = "99"; }
                        else
                        { strId = numId.ToString(); }
                        textBox2.Text = strId;
                    };
                    break;
                case "TBZDEPT":
                    var TBZDEPT = content.Load(content.GetTBZDEPTQuery());
                    TBZDEPT.Completed += (s, args) =>
                    {
                        foreach (TBZDEPT x in TBZDEPT.Entities)
                        {
                            if (x.FSSORT.Trim() != "")
                            {
                                if (Convert.ToInt32(x.FSSORT) > numId)
                                { numId = Convert.ToInt32(x.FSSORT); }
                            }
                        }
                        numId += 1;
                        if (numId <= 9)
                        {
                            strId = "0" + numId.ToString();
                        }
                        else if (numId >= 99)
                        { strId = "99"; }
                        else
                        { strId = numId.ToString(); }
                        textBox2.Text = strId;
                    };
                    break;
                case "TBZPROGOBJ":
                    var dts = content.Load(content.GetTBZPROGOBJQuery());
                    dts.Completed += (s, args) =>
                    {
                        //var dts1 = from b in dts.Entities
                        //           select b;
                        foreach (TBZPROGOBJ x in dts.Entities)
                        {
                            if (x.FSSORT.Trim() != "")
                            {
                                if (Convert.ToInt32(x.FSSORT) > numId)
                                { numId = Convert.ToInt32(x.FSSORT); }
                            }
                        }
                        numId += 1;
                        if (numId <= 9)
                        {
                            strId = "0" + numId.ToString();
                        }
                        else if (numId >= 99)
                        { strId = "99"; }
                        else
                        { strId = numId.ToString(); }
                        textBox2.Text = strId;
                    };
                    break;
                case "TBZPROGSRC":
                    var TBZPROGSRC = content.Load(content.GetTBZPROGSRCQuery());
                    TBZPROGSRC.Completed += (s, args) =>
                    {
                        foreach (TBZPROGSRC x in TBZPROGSRC.Entities)
                        {
                            if (x.FSSORT.Trim() != "")
                            {
                                if (Convert.ToInt32(x.FSSORT) > numId)
                                { numId = Convert.ToInt32(x.FSSORT); }
                            }
                        }
                        numId += 1;
                        if (numId <= 9)
                        {
                            strId = "0" + numId.ToString();
                        }
                        else if (numId >= 99)
                        { strId = "99"; }
                        else
                        { strId = numId.ToString(); }
                        textBox2.Text = strId;
                    };
                    break;
                case "TBZPROGATTR":
                    var TBZPROGATTR = content.Load(content.GetTBZPROGATTRQuery());
                    TBZPROGATTR.Completed += (s, args) =>
                    {
                        foreach (TBZPROGATTR x in TBZPROGATTR.Entities)
                        {
                            if (x.FSSORT.Trim() != "")
                            {
                                if (Convert.ToInt32(x.FSSORT) > numId)
                                { numId = Convert.ToInt32(x.FSSORT); }
                            }
                        }
                        numId += 1;
                        if (numId <= 9)
                        {
                            strId = "0" + numId.ToString();
                        }
                        else if (numId >= 99)
                        { strId = "99"; }
                        else
                        { strId = numId.ToString(); }
                        textBox2.Text = strId;
                    };
                    break;
                case "TBZPROGAUD":
                    var TBZPROGAUD = content.Load(content.GetTBZPROGAUDQuery());
                    TBZPROGAUD.Completed += (s, args) =>
                    {
                        foreach (TBZPROGAUD x in TBZPROGAUD.Entities)
                        {
                            if (x.FSSORT.Trim() != "")
                            {
                                if (Convert.ToInt32(x.FSSORT) > numId)
                                { numId = Convert.ToInt32(x.FSSORT); }
                            }
                        }
                        numId += 1;
                        if (numId <= 9)
                        {
                            strId = "0" + numId.ToString();
                        }
                        else if (numId >= 99)
                        { strId = "99"; }
                        else
                        { strId = numId.ToString(); }
                        textBox2.Text = strId;
                    };
                    break;
                case "TBZPROGTYPE":
                    var TBZPROGTYPE = content.Load(content.GetTBZPROGTYPEQuery());
                    TBZPROGTYPE.Completed += (s, args) =>
                    {
                        foreach (TBZPROGTYPE x in TBZPROGTYPE.Entities)
                        {
                            if (x.FSSORT.Trim() != "")
                            {
                                if (Convert.ToInt32(x.FSSORT) > numId)
                                { numId = Convert.ToInt32(x.FSSORT); }
                            }
                        }
                        numId += 1;
                        if (numId <= 9)
                        {
                            strId = "0" + numId.ToString();
                        }
                        else if (numId >= 99)
                        { strId = "99"; }
                        else
                        { strId = numId.ToString(); }
                        textBox2.Text = strId;
                    };
                    break;
                case "TBZPROGLANG":
                    var TBZPROGLANG = content.Load(content.GetTBZPROGLANGQuery());
                    TBZPROGLANG.Completed += (s, args) =>
                    {
                        foreach (TBZPROGLANG x in TBZPROGLANG.Entities)
                        {
                            if (x.FSSORT.Trim() != "")
                            {
                                if (Convert.ToInt32(x.FSSORT) > numId)
                                { numId = Convert.ToInt32(x.FSSORT); }
                            }
                        }
                        numId += 1;
                        if (numId <= 9)
                        {
                            strId = "0" + numId.ToString();
                        }
                        else if (numId >= 99)
                        { strId = "99"; }
                        else
                        { strId = numId.ToString(); }
                        textBox2.Text = strId;
                    };
                    break;
                case "TBZPROGBUY":
                    var TBZPROGBUY = content.Load(content.GetTBZPROGBUYQuery());
                    TBZPROGBUY.Completed += (s, args) =>
                    {
                        foreach (TBZPROGBUY x in TBZPROGBUY.Entities)
                        {
                            if (x.FSSORT.Trim() != "")
                            {
                                if (Convert.ToInt32(x.FSSORT) > numId)
                                { numId = Convert.ToInt32(x.FSSORT); }
                            }
                        }
                        numId += 1;
                        if (numId <= 9)
                        {
                            strId = "0" + numId.ToString();
                        }
                        else if (numId >= 99)
                        { strId = "99"; }
                        else
                        { strId = numId.ToString(); }
                        textBox2.Text = strId;
                    };
                    break;
                case "TBZPROGGRADE":
                    var TBZPROGGRADE = content.Load(content.GetTBZPROGGRADEQuery());
                    TBZPROGGRADE.Completed += (s, args) =>
                    {
                        foreach (TBZPROGGRADE x in TBZPROGGRADE.Entities)
                        {
                            if (x.FSSORT.Trim() != "")
                            {
                                if (Convert.ToInt32(x.FSSORT) > numId)
                                { numId = Convert.ToInt32(x.FSSORT); }
                            }
                        }
                        numId += 1;
                        if (numId <= 9)
                        {
                            strId = "0" + numId.ToString();
                        }
                        else if (numId >= 99)
                        { strId = "99"; }
                        else
                        { strId = numId.ToString(); }
                        textBox2.Text = strId;
                    };
                    break;
            }
        }
        #endregion

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            #region 按下確認鍵後 將資料寫入資料庫
            int intCheck;
            if (int.TryParse(textBox2.Text.Trim(), out intCheck) && intCheck > 0 && textBox1.Text.Count() > 0)
            {
                #region 把最大的Id抓出來 並將資料寫回
                int numId = 0;
                string strId;
                switch (tbName)
                {
                    case "TBZPROGRACE_AREA":
                        var TBZPROGRACE_AREA = content.Load(content.GetTBZPROGRACE_AREAQuery());
                        TBZPROGRACE_AREA.Completed += (s, args) =>
                        {
                            #region 取得沒有用到的Id空號由"小到大"
                            int[] array = new int[10001];
                            for (int i = 0; i <= 10000; i++)
                            { array[i] = 0; }
                            foreach (TBZPROGRACE_AREA x in TBZPROGRACE_AREA.Entities)
                            {
                                if (x.FSID.Trim() != "")
                                    array[Convert.ToInt32(x.FSID)] = 1;
                            }
                            for (int i = 1; i <= 10000; i++)
                            {
                                if (array[i] == 0)
                                {
                                    numId = i;
                                    break;
                                }
                                if (i == 10000)
                                {
                                    MessageBox.Show("資料庫內編號已使用完畢，請聯絡管理員!");
                                    this.DialogResult = false;
                                }
                            }
                            #endregion
                            if (numId <= 9)
                            { strId = "000" + numId.ToString(); }
                            else if (numId > 9 && numId <= 99)
                            { strId = "00" + numId.ToString(); }
                            else if (numId > 99 && numId <= 999)
                            { strId = "0" + numId.ToString(); }
                            else if (numId > 999 && numId <= 9999)
                            { strId = numId.ToString(); }
                            else
                            { strId = numId.ToString(); }
                            TBZPROGRACE_AREA TBZPROGRACE_AREAClass = new TBZPROGRACE_AREA();
                            TBZPROGRACE_AREAClass.FDCREATED_DATE = DateTime.Now;
                            TBZPROGRACE_AREAClass.FDUPDATED_DATE = DateTime.Now;
                            TBZPROGRACE_AREAClass.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                            TBZPROGRACE_AREAClass.FSID = strId;
                            TBZPROGRACE_AREAClass.FSNAME = textBox1.Text.Trim();
                            TBZPROGRACE_AREAClass.FSSORT = textBox2.Text;
                            TBZPROGRACE_AREAClass.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                            TBZPROGRACE_AREAClass.FBISENABLE = true;
                            content.TBZPROGRACE_AREAs.Add(TBZPROGRACE_AREAClass);
                            content.SubmitChanges().Completed += (s2, args2) =>
                            {
                                this.DialogResult = true;
                            };
                        };
                        break;
                    case "TBZSIGNAL":
                        var TBZSIGNAL = content.Load(content.GetTBZSIGNALQuery());
                        TBZSIGNAL.Completed += (s, args) =>
                        {
                            #region 取得沒有用到的Id空號由"小到大"
                            int[] array = new int[101];
                            for (int i = 0; i <= 100; i++)
                            { array[i] = 0; }
                            foreach (TBZSIGNAL x in TBZSIGNAL.Entities)
                            {
                                if(x.FSSIGNALID.Trim()!="")
                                    array[Convert.ToInt32(x.FSSIGNALID)] = 1;
                            }
                            for (int i = 1; i <= 100; i++)
                            {
                                if (array[i] == 0)
                                {
                                    numId = i;
                                    break;
                                }
                                if (i == 100)
                                {
                                    MessageBox.Show("資料庫內編號已使用完畢，請聯絡管理員!");
                                    this.DialogResult = false;
                                }
                            }
                            #endregion
                            if (numId <= 9)
                            { strId = "0" + numId.ToString(); }
                            else
                            { strId = numId.ToString(); }
                            TBZSIGNAL TBZSIGNALClass = new TBZSIGNAL();
                            TBZSIGNALClass.FDCREATED_DATE = DateTime.Now;
                            TBZSIGNALClass.FDUPDATED_DATE = DateTime.Now;
                            TBZSIGNALClass.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                            TBZSIGNALClass.FSSIGNALID = strId;
                            TBZSIGNALClass.FSSIGNALNAME = textBox1.Text.Trim();
                            TBZSIGNALClass.FSSORT = textBox2.Text;
                            TBZSIGNALClass.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                            TBZSIGNALClass.FBISENABLE = true;
                            content.TBZSIGNALs.Add(TBZSIGNALClass);
                            content.SubmitChanges().Completed += (s2, args2) =>
                            {
                                this.DialogResult = true;
                            };
                        };
                        break;
                    case "TBZPROGSPEC":
                        var TBZPROGSPEC = content.Load(content.GetTBZPROGSPECQuery());
                        TBZPROGSPEC.Completed += (s, args) =>
                        {
                            #region 取得沒有用到的Id空號由"小到大"
                            int[] array = new int[101];
                            for (int i = 0; i <= 100; i++)
                            { array[i] = 0; }
                            foreach (TBZPROGSPEC x in TBZPROGSPEC.Entities)
                            {
                                if(x.FSPROGSPECID.Trim()!="")
                                array[Convert.ToInt32(x.FSPROGSPECID)] = 1;
                            }
                            for (int i = 1; i <= 100; i++)
                            {
                                if (array[i] == 0)
                                {
                                    numId = i;
                                    break;
                                }
                                if (i == 100)
                                {
                                    MessageBox.Show("資料庫內編號已使用完畢，請聯絡管理員!");
                                    this.DialogResult = false;
                                }
                            }
                            #endregion
                            if (numId <= 9)
                            { strId = "0" + numId.ToString(); }
                            else
                            { strId = numId.ToString(); }
                            TBZPROGSPEC TBZPROGSPECClass = new TBZPROGSPEC();
                            TBZPROGSPECClass.FDCREATED_DATE = DateTime.Now;
                            TBZPROGSPECClass.FDUPDATED_DATE = DateTime.Now;
                            TBZPROGSPECClass.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                            TBZPROGSPECClass.FSPROGSPECID = strId;
                            TBZPROGSPECClass.FSPROGSPECNAME = textBox1.Text.Trim();
                            TBZPROGSPECClass.FSSORT = textBox2.Text;
                            TBZPROGSPECClass.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                            TBZPROGSPECClass.FBISENABLE = true;
                            content.TBZPROGSPECs.Add(TBZPROGSPECClass);
                            content.SubmitChanges().Completed += (s2, args2) =>
                            {
                                this.DialogResult = true;
                            };
                        };
                        break;
                    case "TBZPROMOVER":
                        var TBZPROMOVER = content.Load(content.GetTBZPROMOVERQuery());
                        TBZPROMOVER.Completed += (s, args) =>
                        {
                            #region 取得沒有用到的Id空號由"小到大"
                            int[] array = new int[101];
                            for (int i = 0; i <= 100; i++)
                            { array[i] = 0; }
                            foreach (TBZPROMOVER x in TBZPROMOVER.Entities)
                            {
                                if(x.FSPROMOVERID.Trim()!="")
                                array[Convert.ToInt32(x.FSPROMOVERID)] = 1;
                            }
                            for (int i = 1; i <= 100; i++)
                            {
                                if (array[i] == 0)
                                {
                                    numId = i;
                                    break;
                                }
                                if (i == 100)
                                {
                                    MessageBox.Show("資料庫內編號已使用完畢，請聯絡管理員!");
                                    this.DialogResult = false;
                                }
                            }
                            #endregion
                            if (numId <= 9)
                            { strId = "0" + numId.ToString(); }
                            else
                            { strId = numId.ToString(); }
                            TBZPROMOVER TBZPROMOVERClass = new TBZPROMOVER();
                            TBZPROMOVERClass.FDCREATED_DATE = DateTime.Now;
                            TBZPROMOVERClass.FDUPDATED_DATE = DateTime.Now;
                            TBZPROMOVERClass.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                            TBZPROMOVERClass.FSPROMOVERID = strId;
                            TBZPROMOVERClass.FSPROMOVERNAME = textBox1.Text.Trim();
                            TBZPROMOVERClass.FSSORT = textBox2.Text;
                            TBZPROMOVERClass.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                            TBZPROMOVERClass.FBISENABLE = true;
                            content.TBZPROMOVERs.Add(TBZPROMOVERClass);
                            content.SubmitChanges().Completed += (s2, args2) =>
                            {
                                this.DialogResult = true;
                            };
                        };
                        break;

                    case "TBZSHOOTSPEC":
                        var TBZSHOOTSPEC = content.Load(content.GetTBZSHOOTSPECQuery());
                        TBZSHOOTSPEC.Completed += (s, args) =>
                        {
                            #region 取得沒有用到的Id空號由"小到大"
                            int[] array = new int[101];
                            for (int i = 0; i <= 100; i++)
                            { array[i] = 0; }
                            foreach (TBZSHOOTSPEC x in TBZSHOOTSPEC.Entities)
                            {
                                if(x.FSSHOOTSPECID.Trim()!="")
                                array[Convert.ToInt32(x.FSSHOOTSPECID)] = 1;
                            }
                            for (int i = 1; i <= 100; i++)
                            {
                                if (array[i] == 0)
                                {
                                    numId = i;
                                    break;
                                }
                                if (i == 100)
                                {
                                    MessageBox.Show("資料庫內編號已使用完畢，請聯絡管理員!");
                                    this.DialogResult = false;
                                }
                            }
                            #endregion
                            if (numId <= 9)
                            { strId = "0" + numId.ToString(); }
                            else
                            { strId = numId.ToString(); }
                            TBZSHOOTSPEC TBZSHOOTSPECClass = new TBZSHOOTSPEC();
                            TBZSHOOTSPECClass.FDCREATED_DATE = DateTime.Now;
                            TBZSHOOTSPECClass.FDUPDATED_DATE = DateTime.Now;
                            TBZSHOOTSPECClass.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                            TBZSHOOTSPECClass.FSSHOOTSPECID = strId;
                            TBZSHOOTSPECClass.FSSHOOTSPECNAME = textBox1.Text.Trim();
                            TBZSHOOTSPECClass.FSSORT = textBox2.Text;
                            TBZSHOOTSPECClass.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                            TBZSHOOTSPECClass.FBISENABLE = true;
                            content.TBZSHOOTSPECs.Add(TBZSHOOTSPECClass);
                            content.SubmitChanges().Completed += (s2, args2) =>
                            {
                                this.DialogResult = true;
                            };
                        };
                        break;

                    case "TBPGM_ACTION":
                        var TBPGM_ACTION = content.Load(content.GetTBPGM_ACTIONQuery());
                        TBPGM_ACTION.Completed += (s, args) =>
                        {
                            #region 取得沒有用到的Id空號由"小到大"
                            int[] array = new int[101];
                            for (int i = 0; i <= 100; i++)
                            { array[i] = 0; }
                            foreach (TBPGM_ACTION x in TBPGM_ACTION.Entities)
                            {
                                if (x.FSACTIONID.Trim() != "")
                                {
                                    array[Convert.ToInt32(x.FSACTIONID)] = 1;
                                }
                            }
                            for (int i = 1; i <= 100; i++)
                            {
                                if (array[i] == 0)
                                {
                                    numId = i;
                                    break;
                                }
                                if (i == 100)
                                {
                                    MessageBox.Show("資料庫內編號已使用完畢，請聯絡管理員!");
                                    this.DialogResult = false;
                                }
                            }
                            #endregion
                            if (numId <= 9)
                            { strId = "0" + numId.ToString(); }
                            else
                            { strId = numId.ToString(); }
                            TBPGM_ACTION TBPGM_ACTIONClass = new TBPGM_ACTION();
                            TBPGM_ACTIONClass.FDCREATED_DATE = DateTime.Now;
                            TBPGM_ACTIONClass.FDUPDATED_DATE = DateTime.Now;
                            TBPGM_ACTIONClass.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                            TBPGM_ACTIONClass.FSACTIONID = strId;
                            TBPGM_ACTIONClass.FSACTIONNAME = textBox1.Text.Trim();
                            TBPGM_ACTIONClass.FSSORT = textBox2.Text;
                            TBPGM_ACTIONClass.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                            TBPGM_ACTIONClass.FBISENABLE = true;
                            content.TBPGM_ACTIONs.Add(TBPGM_ACTIONClass);
                            content.SubmitChanges().Completed += (s2, args2) =>
                            {
                                this.DialogResult = true;
                            };
                        };
                        break;
                    
                    case "TBZPROMOTYPE":
                        var TBZPROMOTYPE = content.Load(content.GetTBZPROMOTYPEQuery());
                        TBZPROMOTYPE.Completed += (s, args) =>
                        {
                            #region 取得沒有用到的Id空號由"小到大"
                            int[] array = new int[101];
                            for (int i = 0; i <= 100; i++)
                            { array[i] = 0; }
                            foreach (TBZPROMOTYPE x in TBZPROMOTYPE.Entities)
                            {
                                if(x.FSPROMOTYPEID.Trim()!="")
                                array[Convert.ToInt32(x.FSPROMOTYPEID)] = 1;
                            }
                            for (int i = 1; i <= 100; i++)
                            {
                                if (array[i] == 0)
                                {
                                    numId = i;
                                    break;
                                }
                                if (i == 100)
                                {
                                    MessageBox.Show("資料庫內編號已使用完畢，請聯絡管理員!");
                                    this.DialogResult = false;
                                }
                            }
                            #endregion
                            if (numId <= 9)
                            { strId = "0" + numId.ToString(); }
                            else
                            { strId = numId.ToString(); }
                            TBZPROMOTYPE TBZPROMOTYPEClass = new TBZPROMOTYPE();
                            TBZPROMOTYPEClass.FDCREATED_DATE = DateTime.Now;
                            TBZPROMOTYPEClass.FDUPDATED_DATE = DateTime.Now;
                            TBZPROMOTYPEClass.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                            TBZPROMOTYPEClass.FSPROMOTYPEID = strId;
                            TBZPROMOTYPEClass.FSPROMOTYPENAME = textBox1.Text.Trim();
                            TBZPROMOTYPEClass.FSSORT = textBox2.Text;
                            TBZPROMOTYPEClass.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                            TBZPROMOTYPEClass.FBISENABLE = true;
                            content.TBZPROMOTYPEs.Add(TBZPROMOTYPEClass);
                            content.SubmitChanges().Completed += (s2, args2) =>
                            {
                                this.DialogResult = true;
                            };
                        };
                        break;

                    case "TBZPROMOCAT":
                        var TBZPROMOCAT = content.Load(content.GetTBZPROMOCATQuery());
                        TBZPROMOCAT.Completed += (s, args) =>
                        {
                            #region 取得沒有用到的Id空號由"小到大"
                            int[] array = new int[101];
                            for (int i = 0; i <= 100; i++)
                            { array[i] = 0; }
                            foreach (TBZPROMOCAT x in TBZPROMOCAT.Entities)
                            {
                                if(x.FSPROMOCATID.Trim()!="")
                                array[Convert.ToInt32(x.FSPROMOCATID)] = 1;
                            }
                            for (int i = 1; i <= 100; i++)
                            {
                                if (array[i] == 0)
                                {
                                    numId = i;
                                    break;
                                }
                                if (i == 100)
                                {
                                    MessageBox.Show("資料庫內編號已使用完畢，請聯絡管理員!");
                                    this.DialogResult = false;
                                }
                            }
                            #endregion
                            if (numId <= 9)
                            { strId = "0" + numId.ToString(); }
                            else
                            { strId = numId.ToString(); }
                            TBZPROMOCAT TBZPROMOCATClass = new TBZPROMOCAT();
                            TBZPROMOCATClass.FDCREATED_DATE = DateTime.Now;
                            TBZPROMOCATClass.FDUPDATED_DATE = DateTime.Now;
                            TBZPROMOCATClass.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                            TBZPROMOCATClass.FSPROMOCATID = strId;
                            TBZPROMOCATClass.FSPROMOCATNAME = textBox1.Text.Trim();
                            TBZPROMOCATClass.FSSORT = textBox2.Text;
                            TBZPROMOCATClass.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                            TBZPROMOCATClass.FBISENABLE = true;
                            content.TBZPROMOCATs.Add(TBZPROMOCATClass);
                            content.SubmitChanges().Completed += (s2, args2) =>
                            {
                                this.DialogResult = true;
                            };
                        };
                        break;

                    case"TBZBOOKING_REASON":
                        var TBZBOOKING_REASON = content.Load(content.GetTBZBOOKING_REASONQuery());
                        TBZBOOKING_REASON.Completed += (s, args) =>
                        {
                            #region 取得沒有用到的Id空號由"小到大"
                            int[] array = new int[101];
                            for (int i = 0; i <= 100; i++)
                            { array[i] = 0; }
                            foreach (TBZBOOKING_REASON x in TBZBOOKING_REASON.Entities)
                            {
                                if(x.FSNO.Trim()!="")
                                array[Convert.ToInt32(x.FSNO)] = 1;
                            }
                            for (int i = 1; i <= 100; i++)
                            {
                                if (array[i] == 0)
                                {
                                    numId = i;
                                    break;
                                }
                                if (i == 100)
                                {
                                    MessageBox.Show("資料庫內編號已使用完畢，請聯絡管理員!");
                                    this.DialogResult = false;
                                }
                            }
                            #endregion
                            if (numId <= 9)
                            { strId = "0" + numId.ToString(); }
                            else
                            { strId = numId.ToString(); }
                            TBZBOOKING_REASON TBZBOOKING_REASONClass = new TBZBOOKING_REASON();
                            TBZBOOKING_REASONClass.FDCREATED_DATE = DateTime.Now;
                            TBZBOOKING_REASONClass.FDUPDATED_DATE = DateTime.Now;
                            TBZBOOKING_REASONClass.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                            TBZBOOKING_REASONClass.FSNO = strId;
                            TBZBOOKING_REASONClass.FSREASON = textBox1.Text.Trim();
                            TBZBOOKING_REASONClass.FSSORT = textBox2.Text;
                            TBZBOOKING_REASONClass.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                            TBZBOOKING_REASONClass.FBISENABLE = true;
                            content.TBZBOOKING_REASONs.Add(TBZBOOKING_REASONClass);
                            content.SubmitChanges().Completed += (s2, args2) =>
                            {
                                this.DialogResult = true;
                            };
                        };
                        break;
                    case "TBZPROGSTATUS":
                        var TBZPROGSTATUS = content.Load(content.GetTBZPROGSTATUSQuery());
                        TBZPROGSTATUS.Completed += (s, args) =>
                        {
                            #region 取得沒有用到的Id空號由"小到大"
                            int[] array = new int[101];
                            for (int i = 0; i <= 100; i++)
                            { array[i] = 0; }
                            foreach (TBZPROGSTATUS x in TBZPROGSTATUS.Entities)
                            {
                                if(x.FSPROGSTATUSID.Trim()!="")
                                array[Convert.ToInt32(x.FSPROGSTATUSID)] = 1;
                            }
                            for (int i = 1; i <= 100; i++)
                            {
                                if (array[i] == 0)
                                {
                                    numId = i;
                                    break;
                                }
                                if (i == 100)
                                {
                                    MessageBox.Show("資料庫內編號已使用完畢，請聯絡管理員!");
                                    this.DialogResult = false;
                                }
                            }
                            #endregion
                            if (numId <= 9)
                            { strId = "0" + numId.ToString(); }
                            else
                            { strId = numId.ToString(); }
                            TBZPROGSTATUS TBZPROGSTATUSClass = new TBZPROGSTATUS();
                            TBZPROGSTATUSClass.FDCREATED_DATE = DateTime.Now;
                            TBZPROGSTATUSClass.FDUPDATED_DATE = DateTime.Now;
                            TBZPROGSTATUSClass.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                            TBZPROGSTATUSClass.FSPROGSTATUSID = strId;
                            TBZPROGSTATUSClass.FSPROGSTATUSNAME = textBox1.Text.Trim();
                            TBZPROGSTATUSClass.FSSORT = textBox2.Text;
                            TBZPROGSTATUSClass.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                            TBZPROGSTATUSClass.FBISENABLE = true;
                            content.TBZPROGSTATUS.Add(TBZPROGSTATUSClass);
                            content.SubmitChanges().Completed += (s2, args2) =>
                            {
                                this.DialogResult = true;
                            };
                        };
                        break;
                    case "TBZTITLE":
                        var TBZTITLE = content.Load(content.GetTBZTITLEQuery());
                        TBZTITLE.Completed += (s, args) =>
                        {
                            #region 取得沒有用到的Id空號由"小到大"
                            int[] array = new int[101];
                            for (int i = 0; i <= 100; i++)
                            { array[i] = 0; }
                            foreach (TBZTITLE x in TBZTITLE.Entities)
                            {
                                if(x.FSTITLEID.Trim()!="")
                                array[Convert.ToInt32(x.FSTITLEID)] = 1;
                            }
                            for (int i = 1; i <= 100; i++)
                            {
                                if (array[i] == 0)
                                {
                                    numId = i;
                                    break;
                                }
                                if (i == 100)
                                {
                                    MessageBox.Show("資料庫內編號已使用完畢，請聯絡管理員!");
                                    this.DialogResult = false;
                                }
                            }
                            #endregion
                            if (numId <= 9)
                            { strId = "0" + numId.ToString(); }
                            else
                            { strId = numId.ToString(); }
                            TBZTITLE TBZTITLEClass = new TBZTITLE();
                            TBZTITLEClass.FDCREATED_DATE = DateTime.Now;
                            TBZTITLEClass.FDUPDATED_DATE = DateTime.Now;
                            TBZTITLEClass.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                            TBZTITLEClass.FSTITLEID = strId;
                            TBZTITLEClass.FSTITLENAME = textBox1.Text.Trim();
                            TBZTITLEClass.FSSORT = textBox2.Text;
                            TBZTITLEClass.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                            TBZTITLEClass.FBISENABLE = true;
                            content.TBZTITLEs.Add(TBZTITLEClass);
                            content.SubmitChanges().Completed += (s2, args2) =>
                            {
                                this.DialogResult = true;
                            };
                        };
                        break;
                    case "TBZDEPT":
                        var TBZDEPT = content.Load(content.GetTBZDEPTQuery());
                        TBZDEPT.Completed += (s, args) =>
                        {
                            #region 取得沒有用到的Id空號由"小到大"
                            int[] array = new int[101];
                            for (int i = 0; i <= 100; i++)
                            { array[i] = 0; }
                            foreach (TBZDEPT x in TBZDEPT.Entities)
                            {
                                if(x.FSDEPTID.Trim()!="")
                                array[Convert.ToInt32(x.FSDEPTID)] = 1;
                            }
                            for (int i = 1; i <= 100; i++)
                            {
                                if (array[i] == 0)
                                {
                                    numId = i;
                                    break;
                                }
                                if (i == 100)
                                {
                                    MessageBox.Show("資料庫內編號已使用完畢，請聯絡管理員!");
                                    this.DialogResult = false;
                                }
                            }
                            #endregion
                            //foreach (TBZDEPT x in TBZDEPT.Entities)
                            //{
                            //    if (Convert.ToInt32(x.FSDEPTID) > numId)
                            //    { numId = Convert.ToInt32(x.FSDEPTID); }
                            //}
                            //numId += 1;
                            if (numId <= 9)
                            {
                                strId = "0" + numId.ToString();
                            }
                            else
                            {
                                strId = numId.ToString();
                            }
                            TBZDEPT TBZDEPTClass = new TBZDEPT();
                            TBZDEPTClass.FDCREATED_DATE = DateTime.Now;
                            TBZDEPTClass.FDUPDATED_DATE = DateTime.Now;
                            TBZDEPTClass.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                            TBZDEPTClass.FSDEPTID = strId;
                            TBZDEPTClass.FSDEPTNAME = textBox1.Text.Trim();
                            TBZDEPTClass.FSSORT = textBox2.Text;
                            TBZDEPTClass.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                            TBZDEPTClass.FBISENABLE = true;
                            content.TBZDEPTs.Add(TBZDEPTClass);
                            content.SubmitChanges().Completed += (s2, args2) =>
                            {
                                this.DialogResult = true;
                            };
                        };
                        break;
                    case "TBZPROGOBJ":
                        var TBZPROGOBJ = content.Load(content.GetTBZPROGOBJQuery());
                        TBZPROGOBJ.Completed += (s, args) =>
                        {
                            #region 取得沒有用到的Id空號由"小到大"
                            int[] array = new int[101];
                            for (int i = 0; i <= 100; i++)
                            { array[i] = 0; }
                            foreach (TBZPROGOBJ x in TBZPROGOBJ.Entities)
                            {
                                if(x.FSPROGOBJID.Trim()!="")
                                array[Convert.ToInt32(x.FSPROGOBJID)] = 1;
                            }
                            for (int i = 1; i <= 100; i++)
                            {
                                if (array[i] == 0)
                                {
                                    numId = i;
                                    break;
                                }
                                if (i == 100)
                                {
                                    MessageBox.Show("資料庫內編號已使用完畢，請聯絡管理員!");
                                    this.DialogResult = false;
                                }
                            }
                            #endregion
                            if (numId <= 9)
                            {
                                strId = "0" + numId.ToString();
                            }
                            else
                            {
                                strId = numId.ToString();
                            }
                            TBZPROGOBJ TBZPROGOBJClass = new TBZPROGOBJ();
                            TBZPROGOBJClass.FDCREATED_DATE = DateTime.Now;
                            TBZPROGOBJClass.FDUPDATED_DATE = DateTime.Now;
                            TBZPROGOBJClass.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                            TBZPROGOBJClass.FSPROGOBJID = strId;
                            TBZPROGOBJClass.FSPROGOBJNAME = textBox1.Text.Trim();
                            TBZPROGOBJClass.FSSORT = textBox2.Text;
                            TBZPROGOBJClass.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                            TBZPROGOBJClass.FBISENABLE = true;
                            content.TBZPROGOBJs.Add(TBZPROGOBJClass);
                            content.SubmitChanges().Completed += (s2, args2) =>
                            {
                                this.DialogResult = true;
                            };
                        };
                        break;
                    case "TBZPROGSRC":
                        var TBZPROGSRC = content.Load(content.GetTBZPROGSRCQuery());
                        TBZPROGSRC.Completed += (s, args) =>
                        {
                            #region 取得沒有用到的Id空號由"小到大"
                            int[] array = new int[101];
                            for (int i = 0; i <= 100; i++)
                            { array[i] = 0; }
                            foreach (TBZPROGSRC x in TBZPROGSRC.Entities)
                            {
                                if(x.FSPROGSRCID.Trim()!="")
                                array[Convert.ToInt32(x.FSPROGSRCID)] = 1;
                            }
                            for (int i = 1; i <= 100; i++)
                            {
                                if (array[i] == 0)
                                {
                                    numId = i;
                                    break;
                                }
                                if (i == 100)
                                {
                                    MessageBox.Show("資料庫內編號已使用完畢，請聯絡管理員!");
                                    this.DialogResult = false;
                                }
                            }
                            #endregion
                            if (numId <= 9)
                            { strId = "0" + numId.ToString(); }
                            else
                            { strId = numId.ToString(); }
                            TBZPROGSRC TBZPROGSRCClass = new TBZPROGSRC();
                            TBZPROGSRCClass.FDCREATED_DATE = DateTime.Now;
                            TBZPROGSRCClass.FDUPDATED_DATE = DateTime.Now;
                            TBZPROGSRCClass.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                            TBZPROGSRCClass.FSPROGSRCID = strId;
                            TBZPROGSRCClass.FSPROGSRCNAME = textBox1.Text.Trim();
                            TBZPROGSRCClass.FSSORT = textBox2.Text;
                            TBZPROGSRCClass.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                            TBZPROGSRCClass.FBISENABLE = true;
                            content.TBZPROGSRCs.Add(TBZPROGSRCClass);
                            content.SubmitChanges().Completed += (s2, args2) =>
                            {
                                this.DialogResult = true;
                            };
                        };
                        break;
                    case "TBZPROGATTR":
                        var TBZPROGATTR = content.Load(content.GetTBZPROGATTRQuery());
                        TBZPROGATTR.Completed += (s, args) =>
                        {
                            #region 取得沒有用到的Id空號由"小到大"
                            int[] array = new int[101];
                            for (int i = 0; i <= 100; i++)
                            { array[i] = 0; }
                            foreach (TBZPROGATTR x in TBZPROGATTR.Entities)
                            {
                                if(x.FSPROGATTRID.Trim()!="")
                                array[Convert.ToInt32(x.FSPROGATTRID)] = 1;
                            }
                            for (int i = 1; i <= 100; i++)
                            {
                                if (array[i] == 0)
                                {
                                    numId = i;
                                    break;
                                }
                                if (i == 100)
                                {
                                    MessageBox.Show("資料庫內編號已使用完畢，請聯絡管理員!");
                                    this.DialogResult = false;
                                }
                            }
                            #endregion
                            if (numId <= 9)
                            { strId = "0" + numId.ToString(); }
                            else
                            { strId = numId.ToString(); }
                            TBZPROGATTR TBZPROGATTRClass = new TBZPROGATTR();
                            TBZPROGATTRClass.FDCREATED_DATE = DateTime.Now;
                            TBZPROGATTRClass.FDUPDATED_DATE = DateTime.Now;
                            TBZPROGATTRClass.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                            TBZPROGATTRClass.FSPROGATTRID = strId;
                            TBZPROGATTRClass.FSPROGATTRNAME = textBox1.Text.Trim();
                            TBZPROGATTRClass.FSSORT = textBox2.Text;
                            TBZPROGATTRClass.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                            TBZPROGATTRClass.FBISENABLE = true;
                            content.TBZPROGATTRs.Add(TBZPROGATTRClass);
                            content.SubmitChanges().Completed += (s2, args2) =>
                            {
                                this.DialogResult = true;
                            };
                        };
                        break;
                    case "TBZPROGAUD":
                        var TBZPROGAUD = content.Load(content.GetTBZPROGAUDQuery());
                        TBZPROGAUD.Completed += (s, args) =>
                        {
                            #region 取得沒有用到的Id空號由"小到大"
                            int[] array = new int[101];
                            for (int i = 0; i <= 100; i++)
                            { array[i] = 0; }
                            foreach (TBZPROGAUD x in TBZPROGAUD.Entities)
                            {
                                if(x.FSPROGAUDID.Trim()!="")
                                array[Convert.ToInt32(x.FSPROGAUDID)] = 1;
                            }
                            for (int i = 1; i <= 100; i++)
                            {
                                if (array[i] == 0)
                                {
                                    numId = i;
                                    break;
                                }
                                if (i == 100)
                                {
                                    MessageBox.Show("資料庫內編號已使用完畢，請聯絡管理員!");
                                    this.DialogResult = false;
                                }
                            }
                            #endregion
                            if (numId <= 9)
                            { strId = "0" + numId.ToString(); }
                            else
                            { strId = numId.ToString(); }
                            TBZPROGAUD TBZPROGAUDClass = new TBZPROGAUD();
                            TBZPROGAUDClass.FDCREATED_DATE = DateTime.Now;
                            TBZPROGAUDClass.FDUPDATED_DATE = DateTime.Now;
                            TBZPROGAUDClass.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                            TBZPROGAUDClass.FSPROGAUDID = strId;
                            TBZPROGAUDClass.FSPROGAUDNAME = textBox1.Text.Trim();
                            TBZPROGAUDClass.FSSORT = textBox2.Text;
                            TBZPROGAUDClass.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                            TBZPROGAUDClass.FBISENABLE = true;
                            content.TBZPROGAUDs.Add(TBZPROGAUDClass);
                            content.SubmitChanges().Completed += (s2, args2) =>
                            {
                                this.DialogResult = true;
                            };
                        };
                        break;
                    case "TBZPROGTYPE":
                        var TBZPROGTYPE = content.Load(content.GetTBZPROGTYPEQuery());
                        TBZPROGTYPE.Completed += (s, args) =>
                        {
                            #region 取得沒有用到的Id空號由"小到大"
                            int[] array = new int[101];
                            for (int i = 0; i <= 100; i++)
                            { array[i] = 0; }
                            foreach (TBZPROGTYPE x in TBZPROGTYPE.Entities)
                            {
                                if(x.FSPROGTYPEID.Trim()!="")
                                array[Convert.ToInt32(x.FSPROGTYPEID)] = 1;
                            }
                            for (int i = 1; i <= 100; i++)
                            {
                                if (array[i] == 0)
                                {
                                    numId = i;
                                    break;
                                }
                                if (i == 100)
                                {
                                    MessageBox.Show("資料庫內編號已使用完畢，請聯絡管理員!");
                                    this.DialogResult = false;
                                }
                            }
                            #endregion
                            if (numId <= 9)
                            { strId = "0" + numId.ToString(); }
                            else
                            { strId = numId.ToString(); }
                            TBZPROGTYPE TBZPROGTYPEClass = new TBZPROGTYPE();
                            TBZPROGTYPEClass.FDCREATED_DATE = DateTime.Now;
                            TBZPROGTYPEClass.FDUPDATED_DATE = DateTime.Now;
                            TBZPROGTYPEClass.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                            TBZPROGTYPEClass.FSPROGTYPEID = strId;
                            TBZPROGTYPEClass.FSPROGTYPENAME = textBox1.Text.Trim();
                            TBZPROGTYPEClass.FSSORT = textBox2.Text;
                            TBZPROGTYPEClass.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                            TBZPROGTYPEClass.FBISENABLE = true;
                            content.TBZPROGTYPEs.Add(TBZPROGTYPEClass);
                            content.SubmitChanges().Completed += (s2, args2) =>
                            {
                                this.DialogResult = true;
                            };
                        };
                        break;
                    case "TBZPROGLANG":
                        var TBZPROGLANG = content.Load(content.GetTBZPROGLANGQuery());
                        TBZPROGLANG.Completed += (s, args) =>
                        {
                            #region 取得沒有用到的Id空號由"小到大"
                            int[] array = new int[101];
                            for (int i = 0; i <= 100; i++)
                            { array[i] = 0; }
                            foreach (TBZPROGLANG x in TBZPROGLANG.Entities)
                            {
                                if(x.FSPROGLANGID.Trim()!="")
                                array[Convert.ToInt32(x.FSPROGLANGID)] = 1;
                            }
                            for (int i = 1; i <= 100; i++)
                            {
                                if (array[i] == 0)
                                {
                                    numId = i;
                                    break;
                                }
                                if (i == 100)
                                {
                                    MessageBox.Show("資料庫內編號已使用完畢，請聯絡管理員!");
                                    this.DialogResult = false;
                                }
                            }
                            #endregion
                            if (numId <= 9)
                            { strId = "0" + numId.ToString(); }
                            else
                            { strId = numId.ToString(); }
                            TBZPROGLANG TBZPROGLANGClass = new TBZPROGLANG();
                            TBZPROGLANGClass.FDCREATED_DATE = DateTime.Now;
                            TBZPROGLANGClass.FDUPDATED_DATE = DateTime.Now;
                            TBZPROGLANGClass.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                            TBZPROGLANGClass.FSPROGLANGID = strId;
                            TBZPROGLANGClass.FSPROGLANGNAME = textBox1.Text.Trim();
                            TBZPROGLANGClass.FSSORT = textBox2.Text;
                            TBZPROGLANGClass.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                            TBZPROGLANGClass.FBISENABLE = true;
                            content.TBZPROGLANGs.Add(TBZPROGLANGClass);
                            content.SubmitChanges().Completed += (s2, args2) =>
                            {
                                this.DialogResult = true;
                            };
                        };
                        break;
                    case "TBZPROGBUY":
                        var TBZPROGBUY = content.Load(content.GetTBZPROGBUYQuery());
                        TBZPROGBUY.Completed += (s, args) =>
                        {
                            #region 取得沒有用到的Id空號由"小到大"
                            int[] array = new int[101];
                            for (int i = 0; i <= 100; i++)
                            { array[i] = 0; }
                            foreach (TBZPROGBUY x in TBZPROGBUY.Entities)
                            {
                                if(x.FSPROGBUYID.Trim()!="")
                                array[Convert.ToInt32(x.FSPROGBUYID)] = 1;
                            }
                            for (int i = 1; i <= 100; i++)
                            {
                                if (array[i] == 0)
                                {
                                    numId = i;
                                    break;
                                }
                                if (i == 100)
                                {
                                    MessageBox.Show("資料庫內編號已使用完畢，請聯絡管理員!");
                                    this.DialogResult = false;
                                }
                            }
                            #endregion
                            if (numId <= 9)
                            { strId = "0" + numId.ToString(); }
                            else
                            { strId = numId.ToString(); }
                            TBZPROGBUY TBZPROGBUYClass = new TBZPROGBUY();
                            TBZPROGBUYClass.FDCREATED_DATE = DateTime.Now;
                            TBZPROGBUYClass.FDUPDATED_DATE = DateTime.Now;
                            TBZPROGBUYClass.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                            TBZPROGBUYClass.FSPROGBUYID = strId;
                            TBZPROGBUYClass.FSPROGBUYNAME = textBox1.Text.Trim();
                            TBZPROGBUYClass.FSSORT = textBox2.Text;
                            TBZPROGBUYClass.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                            TBZPROGBUYClass.FBISENABLE = true;
                            content.TBZPROGBUYs.Add(TBZPROGBUYClass);
                            content.SubmitChanges().Completed += (s2, args2) =>
                            {
                                this.DialogResult = true;
                            };
                        };
                        break;
                    case "TBZPROGGRADE":
                        var TBZPROGGRADE = content.Load(content.GetTBZPROGGRADEQuery());
                        TBZPROGGRADE.Completed += (s, args) =>
                        {
                            #region 取得沒有用到的Id空號由"小到大"
                            int[] array = new int[101];
                            for (int i = 0; i <= 100; i++)
                            { array[i] = 0; }
                            foreach (TBZPROGGRADE x in TBZPROGGRADE.Entities)
                            {
                                if(x.FSPROGGRADEID.Trim()!="")
                                array[Convert.ToInt32(x.FSPROGGRADEID)] = 1;
                            }
                            for (int i = 1; i <= 100; i++)
                            {
                                if (array[i] == 0)
                                {
                                    numId = i;
                                    break;
                                }
                                if (i == 100)
                                {
                                    MessageBox.Show("資料庫內編號已使用完畢，請聯絡管理員!");
                                    this.DialogResult = false;
                                }
                            }
                            #endregion
                            if (numId <= 9)
                            { strId = "0" + numId.ToString(); }
                            else
                            { strId = numId.ToString(); }
                            TBZPROGGRADE TBZPROGGRADEClass = new TBZPROGGRADE();
                            TBZPROGGRADEClass.FDCREATED_DATE = DateTime.Now;
                            TBZPROGGRADEClass.FDUPDATED_DATE = DateTime.Now;
                            TBZPROGGRADEClass.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                            TBZPROGGRADEClass.FSPROGGRADEID = strId;
                            TBZPROGGRADEClass.FSPROGGRADENAME = textBox1.Text.Trim();
                            TBZPROGGRADEClass.FSSORT = textBox2.Text;
                            TBZPROGGRADEClass.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                            TBZPROGGRADEClass.FBISENABLE = true;
                            content.TBZPROGGRADEs.Add(TBZPROGGRADEClass);
                            content.SubmitChanges().Completed += (s2, args2) =>
                            {
                                this.DialogResult = true;
                            };
                        };
                        break;
                }

                #endregion
            }
            else
            {
                if (int.TryParse(textBox2.Text.Trim(), out intCheck)!=true || textBox2.Text.Trim()=="" || intCheck==0)
                {
                    if (tbName == "TBZPROGRACE_AREA")
                    { MessageBox.Show("顯示排序欄位請填入大於0以及小於或等於9999的\"數字\"!"); }
                    else
                    MessageBox.Show("顯示排序欄位請填入大於0以及小於或等於99的\"數字\"!"); 
                }
                if (textBox1.Text.Trim().Count() == 0)
                { MessageBox.Show("製作目的名稱必須填入資料!"); }
            }
            #endregion
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

