﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.ProgData;
using PTS_MAM3.Web.ENT;

namespace PTS_MAM3.MAG
{
    public partial class MAG102_01 : ChildWindow
    {
        
        string GPType = "G", PGM_Id, Episode = "0";
        short v_number, a_number, p_number, d_number;
        bool isnumber = true;
        ENT200DomainContext Context = new ENT200DomainContext();
        List<TBARCHIVE_SET_CLASS> cbxList = new List<TBARCHIVE_SET_CLASS>();
        public MAG102_01()
        {
            InitializeComponent();
            var x = Context.Load(Context.GetTBARCHIVE_SET_CLASSQuery());
            x.Completed += (s, args) =>
                {
                    foreach (TBARCHIVE_SET_CLASS y in x.Entities)
                    { 
                        ComboBoxItem cbi = new ComboBoxItem();
                        cbi.Content = y.FSCLASSNAME;
                        cbi.Tag = y;
                        comboBox1.Items.Add(cbi);
                    }
                };
            comboBox1.SelectionChanged += new SelectionChangedEventHandler(comboBox1_SelectionChanged);
            rdbProg.IsChecked = true;
        }

        void comboBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            textBox1.Text = ((TBARCHIVE_SET_CLASS)(((ComboBoxItem)comboBox1.SelectedItem).Tag)).FSVIDEO.Trim();
            textBox2.Text = ((TBARCHIVE_SET_CLASS)(((ComboBoxItem)comboBox1.SelectedItem).Tag)).FSAUDIO.Trim();
            textBox3.Text = ((TBARCHIVE_SET_CLASS)(((ComboBoxItem)comboBox1.SelectedItem).Tag)).FSPHOTO.Trim();
            textBox4.Text = ((TBARCHIVE_SET_CLASS)(((ComboBoxItem)comboBox1.SelectedItem).Tag)).FSDOC.Trim();
        }
        //int number;
        //bool result = Int32.TryParse(value, out number);
        WSARCHIVE.WSARCHIVESoapClient client = new WSARCHIVE.WSARCHIVESoapClient();
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (tbxNAME.Text != "")
            {
                if (rdbPromo.IsChecked == true)
                { GPType = "P"; }
                PGM_Id = tbxNAME.Tag.ToString().Trim();
                //Episode = tbxEPISODE.ToString().Trim();
                isnumber = Int16.TryParse(textBox1.Text, out v_number);
                isnumber = Int16.TryParse(textBox2.Text, out a_number);
                isnumber = Int16.TryParse(textBox3.Text, out p_number);
                isnumber = Int16.TryParse(textBox4.Text, out d_number);
                client.InsTBARCHIVE_SETCompleted += (s, args) =>
                    {
                        if (args.Result == "HaveAddaNew")
                        {
                            this.DialogResult = true;
                            MessageBox.Show("資料新增完成");
                        }
                        else if (args.Result == "SameId")
                        { MessageBox.Show("資料庫中已有相同設定，請用修改資料修改原本設定"); }
                    };
                if (PGMDefault.IsChecked == true)   //節目預設
                    Episode = "19999";
                client.InsTBARCHIVE_SETAsync(GPType, PGM_Id, Episode, v_number, a_number, p_number, d_number, UserClass.userData.FSUSER_ID);
            }
            else
            { MessageBox.Show("請選擇節目名稱!"); }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void btnID_Click(object sender, RoutedEventArgs e)
        {
            if (GPType == "G")
            {
                PROGDATA_VIEW PROGDATA_VIEW_frm = new PROGDATA_VIEW();
                PROGDATA_VIEW_frm.Show();
                PROGDATA_VIEW_frm.Closed += (s, args) =>
                {
                    if (PROGDATA_VIEW_frm.DialogResult == true)
                    {
                        tbxNAME.Text = PROGDATA_VIEW_frm.strProgName_View;
                        tbxNAME.Tag = PROGDATA_VIEW_frm.strProgID_View;
                        PGM_Id = PROGDATA_VIEW_frm.strProgID_View;
                        tbxEPISODE.Text = "";
                        lblEPISODE_NAME.Content = "";
                    }
                };
            }
            else if (GPType == "P")
            {
                //PRG800_07
                PRG.PRG800_07 PROMO_VIEW_frm = new PRG.PRG800_07();
                PROMO_VIEW_frm.Show();

                PROMO_VIEW_frm.Closed += (s, args) =>
                {
                    if (PROMO_VIEW_frm.DialogResult == true)
                    {
                        tbxNAME.Text = PROMO_VIEW_frm.strPromoName_View;
                        tbxNAME.Tag = PROMO_VIEW_frm.strPromoID_View;
                        PGM_Id = PROMO_VIEW_frm.strPromoID_View;
                        tbxEPISODE.Text = "";
                        lblEPISODE_NAME.Content = "";
                        Episode = "0";
                    }
                };
            }
        }

        private void btnEPISODE_Click(object sender, RoutedEventArgs e)
        {
            if (tbxNAME.Text.ToString().Trim() != "")
            {
                PRG.PRG200_07 PRG200_07_frm = new PRG.PRG200_07(PGM_Id, tbxNAME.Text.ToString().Trim());
                PRG200_07_frm.Show();

                PRG200_07_frm.Closed += (s, args) =>
                {
                    if (PRG200_07_frm.DialogResult == true)
                    {
                        Episode = PRG200_07_frm.m_strEpisode_View;
                        tbxEPISODE.Text = PRG200_07_frm.m_strEpisode_View;
                        lblEPISODE_NAME.Content = PRG200_07_frm.m_strProgDName_View;
                    }
                };
            }
            else
                MessageBox.Show("請先選擇節目！");
        }

        private void rdbProg_Checked(object sender, RoutedEventArgs e)
        {
            if (rdbProg.IsChecked == true)
            {
                GPType = "G";
                tbNANE.Text = "節目名稱";
                tbFNEPISODE.Visibility = Visibility.Visible;
                tbxEPISODE.Visibility = Visibility.Visible;
                btnEPISODE.Visibility = Visibility.Visible;
                tbEPISODE_NAME.Visibility = Visibility.Visible;
                lblEPISODE_NAME.Visibility = Visibility.Visible;
            }
        }

        private void rdbPromo_Checked(object sender, RoutedEventArgs e)
        {
            if (rdbPromo.IsChecked == true)
            {
                GPType = "P";
                Episode = "0";
                tbNANE.Text = "短帶名稱";
                tbFNEPISODE.Visibility = Visibility.Collapsed;
                tbxEPISODE.Visibility = Visibility.Collapsed;
                btnEPISODE.Visibility = Visibility.Collapsed;
                tbEPISODE_NAME.Visibility = Visibility.Collapsed;
                lblEPISODE_NAME.Visibility = Visibility.Collapsed;
            }
        }
    }
}

