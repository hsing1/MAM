﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.MAG
{
    public partial class MAG606 : UserControl
    {
        const int OPERATION_INTERVAL = 30;                  // 上下架指令的操作最短間隔
        const int MAX_TS3500_IO_COUNT = 16;                 // 磁帶櫃的I/O最大數量
        const int COLUMN_INDEX_NAME = 1;                    // DataGrid 上面，磁帶名稱的欄位位置

        List<SR_WSTSM.ClassTapeInfo> Tapes = new List<SR_WSTSM.ClassTapeInfo>();
        List<SR_WSTSM.ClassTapeInfo> BindingTapeList = new List<SR_WSTSM.ClassTapeInfo>();
        List<string> SelectedTapeList = new List<string>(); // 選取的磁帶項目
        DateTime lastExecuteTime;                           // 最後一次執行上下架的時間

        /// <summary>
        /// 
        /// </summary>
        public MAG606()
        {
            // 
            InitializeComponent();
            // 最後執行查詢的時間
            lastExecuteTime = DateTime.MinValue;
            // 
            radBusyIndicator1.IsBusy = true;
            // 資料綁定
            DataGrid_TapeList.DataContext = null;
            // 進行查詢動作
            fnQueryTs3500TapeDetails();
        }

        // 查詢 3500 的磁帶清單(並綁定到介面)
        void fnQueryTs3500TapeDetails()
        {
            var srTSM = new SR_WSTSM.ServiceTSMSoapClient();
            srTSM.GetTapesDetail_TSM3500Completed += new EventHandler<SR_WSTSM.GetTapesDetail_TSM3500CompletedEventArgs>(objTSM_GetTapesDetail_TSM3500Completed);
            srTSM.GetTapesDetail_TSM3500Async();
        }

        // 查詢 3500 的I/O數量
        void fnQueryTs3500IOCount()
        {
            var srTSM = new SR_WSTSM.ServiceTSMSoapClient();
            srTSM.GetTs3500IOCountCompleted += new EventHandler<SR_WSTSM.GetTs3500IOCountCompletedEventArgs>(srTSM_GetTs3500IOCountCompleted);
            srTSM.GetTs3500IOCountAsync();
        }

        #region ASYNC_COMPLETE
        // 查詢磁帶詳細內容
        void objTSM_GetTapesDetail_TSM3500Completed(object sender, SR_WSTSM.GetTapesDetail_TSM3500CompletedEventArgs e)
        {
            // 檢查回傳是否有錯誤
            if (e.Error == null)
            {
                if (e.Result.Count > 0)
                {
                    //
                    BindingTapeList = new List<SR_WSTSM.ClassTapeInfo>();
                    //
                    Tapes = e.Result;
                    foreach (var t in Tapes)
                        BindingTapeList.Add(t);
                    //
                    RebindTapes();
                }
                else
                {
                    MessageBox.Show("磁帶櫃中目前沒有磁帶。");
                }
            }
            else
                MessageBox.Show("與主機端的連線發生錯誤");

            // 
            radBusyIndicator1.IsBusy = false;
        }

        // 磁帶上架
        void objTSM_CheckinTapes_TSM3500Completed(object sender, SR_WSTSM.CheckinTapes_TSM3500CompletedEventArgs e)
        {
            // 檢查回傳是否有錯誤
            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    MessageBox.Show("開始執行磁帶上架作業。");
                    lastExecuteTime = DateTime.Now;
                }
                else
                    MessageBox.Show("執行磁帶上架作業時發生錯誤，請稍後再試。");
            }
            else
                MessageBox.Show("與主機端的連線發生錯誤");

            //
            radBusyIndicator1.IsBusy = false;
        }

        // 磁帶下架
        void objTSM_CheckoutTapes_TSM3500Completed(object sender, SR_WSTSM.CheckoutTapes_TSM3500CompletedEventArgs e)
        {
            // 檢查回傳是否有錯誤
            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    MessageBox.Show("開始執行磁帶下架作業，請於五分鐘後至磁帶櫃取出下架磁帶。");
                    lastExecuteTime = DateTime.Now;
                }
                else
                    MessageBox.Show("執行磁帶下架作業時發生錯誤，請稍後再試。");
            }
            else
                MessageBox.Show("與主機端的連線發生錯誤");

            //
            radBusyIndicator1.IsBusy = false;
        }

        // 計算 I/O Port 數 + 發送下架命令
        void srTSM_GetTs3500IOCountCompleted(object sender, SR_WSTSM.GetTs3500IOCountCompletedEventArgs e)
        {
            // 檢查回傳是否有錯誤
            if (e.Error == null)
            {
                // 
                if (SelectedTapeList.Count + e.Result > MAX_TS3500_IO_COUNT)
                {
                    MessageBox.Show(string.Format("目前I/O裡面有{0}捲磁帶，只能選取{1}捲磁帶", e.Result, MAX_TS3500_IO_COUNT - e.Result));
                    radBusyIndicator1.IsBusy = false;
                    return;
                }

                // 
                var inputArr = new SR_WSTSM.ArrayOfString();
                inputArr.AddRange(SelectedTapeList);

                // 送出命令
                var srTSM = new SR_WSTSM.ServiceTSMSoapClient();
                srTSM.CheckoutTapes_TSM3500Async(inputArr, UserClass.userData.FSUSER_ID.ToString());
                srTSM.CheckoutTapes_TSM3500Completed += new EventHandler<SR_WSTSM.CheckoutTapes_TSM3500CompletedEventArgs>(objTSM_CheckoutTapes_TSM3500Completed);

                // 因為還有下一個指令，所以 radBusyIndicator1 不能放開
            }
            else
            {
                MessageBox.Show("與主機端的連線發生錯誤");
                radBusyIndicator1.IsBusy = false;
            }
        }
        #endregion

        #region GUI_Events
        // 功能「磁帶上架」
        void Button_TapeUp_Click(object sender, RoutedEventArgs e)
        {
            var t = (long)((TimeSpan)(DateTime.Now - lastExecuteTime)).TotalSeconds;

            // 檢查目前是不是冷卻中
            if (t < OPERATION_INTERVAL)
            {
                MessageBox.Show(string.Format("磁帶上下架指令操作過度頻繁，請於{0}秒後再試。", OPERATION_INTERVAL - t));
                return;
            }

            // 
            radBusyIndicator1.IsBusy = true;

            // 直接送出查詢給WS
            var srTSM = new SR_WSTSM.ServiceTSMSoapClient();
            srTSM.CheckinTapes_TSM3500Completed += new EventHandler<SR_WSTSM.CheckinTapes_TSM3500CompletedEventArgs>(objTSM_CheckinTapes_TSM3500Completed);
            srTSM.CheckinTapes_TSM3500Async(UserClass.userData.FSUSER_ID.ToString());
        }

        // 功能「磁帶下架」
        void Button_TapeDown_Click(object sender, RoutedEventArgs e)
        {
            // 
            if (SelectedTapeList.Count == 0)
            {
                MessageBox.Show("沒有選取欲下架的磁帶");
                return;
            }

            // 檢查目前是不是冷卻中
            var t = (long)((TimeSpan)(DateTime.Now - lastExecuteTime)).TotalSeconds;
            if (t < OPERATION_INTERVAL)
            {
                MessageBox.Show(string.Format("磁帶上下架指令操作過度頻繁，請於{0}秒後再試。", OPERATION_INTERVAL - t));
                return;
            }

            // 
            radBusyIndicator1.IsBusy = true;

            // 先查詢 I/O 數量
            fnQueryTs3500IOCount();
        }

        // 功能「重新整理」
        void Button_Refresh_Click(object sender, RoutedEventArgs e)
        {
            //
            radBusyIndicator1.IsBusy = true;

            // 清除原有的資料
            SelectedTapeList.Clear();

            // 查詢
            fnQueryTs3500TapeDetails();
        }

        // 功能「」
        void Button_ShowAll_Click(object sender, RoutedEventArgs e)
        {
            //
            radBusyIndicator1.IsBusy = true;

            // 清除原有的資料
            SelectedTapeList.Clear();
            BindingTapeList = new List<SR_WSTSM.ClassTapeInfo>();

            // 符合條件的項目
            foreach (var t in Tapes)
                BindingTapeList.Add(t);
            RebindTapes();

            // 
            radBusyIndicator1.IsBusy = false;
        }

        // 功能「」
        void Button_ShowA_Click(object sender, RoutedEventArgs e)
        {
            //
            radBusyIndicator1.IsBusy = true;

            // 清除原有的資料
            SelectedTapeList.Clear();
            BindingTapeList = new List<SR_WSTSM.ClassTapeInfo>();

            // 符合條件的項目
            foreach (var t in Tapes)
                if (t.ContainerPool.Contains("HSM") && t.SpaceStatus.Equals("FULL", StringComparison.OrdinalIgnoreCase))
                    BindingTapeList.Add(t);
            RebindTapes();

            // 
            radBusyIndicator1.IsBusy = false;
        }

        // 功能「」
        void Button_ShowB_Click(object sender, RoutedEventArgs e)
        {
            //
            radBusyIndicator1.IsBusy = true;

            // 清除原有的資料
            SelectedTapeList.Clear();
            BindingTapeList = new List<SR_WSTSM.ClassTapeInfo>();

            // 符合條件的項目
            foreach (var t in Tapes)
                if (t.ContainerPool.Contains("BAK") && t.SpaceStatus.Equals("FULL", StringComparison.OrdinalIgnoreCase))
                    BindingTapeList.Add(t);
            RebindTapes();

            // 
            radBusyIndicator1.IsBusy = false;
        }

        // 開發測試用
        void Button_Debug_Click(object sender, RoutedEventArgs e)
        {
            var sb = new System.Text.StringBuilder();
            foreach (var s in SelectedTapeList)
                sb.Append(s + "、");
            MessageBox.Show(sb.ToString());
        }

        // 
        void DataGrid_TapeList_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            // 在讀入新的資料行時，都要檢查自己的屬性
            var tapeName = ((TextBlock)DataGrid_TapeList.Columns[COLUMN_INDEX_NAME].GetCellContent(e.Row)).Text;
            if (SelectedTapeList.Contains(tapeName))
                ((Button)DataGrid_TapeList.Columns[0].GetCellContent(e.Row)).Content = "已選取";
            else
                ((Button)DataGrid_TapeList.Columns[0].GetCellContent(e.Row)).Content = string.Empty;
        }

        // 
        void selectCheckout_Click(object sender, RoutedEventArgs e)
        {
            Button objBtn = sender as Button;
            
            // 當 ... 時才需要檢查
            if (string.IsNullOrEmpty(objBtn.Content.ToString()))
            {
                if (SelectedTapeList.Count >= MAX_TS3500_IO_COUNT)
                {
                    MessageBox.Show(string.Format("一次只能選擇{0}捲磁帶", MAX_TS3500_IO_COUNT));
                    return;
                }
            }
            
            // 找出目前的 DataGrid 元件
            DataGridRow currentDataGridRow = DataGridRow.GetRowContainingElement(objBtn);

            // 找出目前的磁帶編號
            string tapeName = ((TextBlock)DataGrid_TapeList.Columns[COLUMN_INDEX_NAME].GetCellContent(currentDataGridRow)).Text;

            // 雖然有做好 UI 的刷新動作，這邊還是盡量以記憶體來判讀
            if (SelectedTapeList.Contains(tapeName))
            {
                SelectedTapeList.Remove(tapeName);

                objBtn.Content = string.Empty;
            }
            else
            {
                SelectedTapeList.Add(tapeName);

                objBtn.Content = "已選取";
            }
        }

        // 重新綁定 Tape 的資料，並更新「顯示筆數」
        void RebindTapes()
        {
            DataGrid_TapeList.DataContext = null;
            System.Threading.Thread.Sleep(100);
            DataGrid_TapeList.DataContext = BindingTapeList;
            txtTapeCount.Text = string.Format(" 顯示筆數：{0}/{1}", BindingTapeList.Count, Tapes.Count);
        }
        #endregion
    }
}