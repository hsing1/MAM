﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSUserAdmin;
using PTS_MAM3.WSGroupAdmin;
using System.Text;
using System.Xml.Linq;
using System.IO;
using System.Collections.ObjectModel;
using System.ComponentModel;


namespace PTS_MAM3
{
    public partial class MAG200_08 : ChildWindow, INotifyPropertyChanged
    {

        WSGroupAdminSoapClient GroupClient = new WSGroupAdminSoapClient();

        UserStruct CurrentEditUser;

        public MAG200_08(UserStruct iniuser)
        {
            InitializeComponent();

            this.DataContext = this;
            CurrentEditUser = iniuser;
            this.SPTitle.DataContext = CurrentEditUser;

            GroupClient.fnGetTBGROUPSCompleted += (s, e) =>
            {
                if (e.Error == null)
                {
                    byte[] byteArray = Encoding.Unicode.GetBytes(e.Result);
                    //StringBuilder output = new StringBuilder();
                    // Create an XmlReader
                    XDocument doc = XDocument.Load(new MemoryStream(byteArray));

                    var ltox = from data in doc.Elements("Datas").Elements("Data")
                               select data;

                    foreach (XElement elem in ltox)
                    {
                        AllGroups.Add(new TBGroupStruct()
                        {
                            FSGROUP = elem.Element("FSGROUP").Value.ToString(),
                            FSGROUP_ID = elem.Element("FSGROUP_ID").Value.ToString()
                        });
                        //Telerik.Windows.Controls.RadTreeViewItem treeItem = new Telerik.Windows.Controls.RadTreeViewItem();
                        //treeItem.Header = elem.Element("FSGROUP").Value.ToString();
                        //treeItem.Tag = elem;
                        ////treeItem.IsExpanded = true;
                        //treeItem.IsLoadingOnDemand = false;
                        //GroupNode.Items.Add(treeItem);

                        //Text = elem.Element("FSFSGROUP").Value.ToString();
                    }

                    GroupClient.fnGetTBGROUPS_By_USERIDAsync(iniuser.FSUSER_ID);
                }
            };

            GroupClient.fnGetTBGROUPS_By_USERIDCompleted += (s, e) =>
            {
                if (e.Error == null)
                {
                    UserGroups = e.Result;

                    // IEnumerable<TBGroupStruct> groupTemp = AllGroups.Except(UserGroups);

                    foreach (TBGroupStruct group in UserGroups)
                    {
                        AllGroups.Remove(AllGroups.Where(g => g.FSGROUP_ID == group.FSGROUP_ID).FirstOrDefault());
                    }
                }
            };

            GroupClient.fnInsertTBUSER_GROUP_BatchCompleted += (s, e) =>
            {
                if (e.Error == null)
                {
                    if (e.Result)
                    {
                        if (this.RemoveGroupsSanbox.Count != 0)
                        {
                            DeleteUserGroup_Batch();
                        }
                        else
                        {
                            this.DialogResult = true;
                        }
                    }
                    else
                    {
                        MessageBox.Show("新增時發生錯誤");
                    }
                }

            };

            GroupClient.fnDeleteTBUSER_GROUP_BatchCompleted += (s, e) =>
            {
                if (e.Error == null)
                {
                    if (e.Result)
                    {
                        this.DialogResult = true;
                    }
                    else
                    {
                        MessageBox.Show("刪除時發生錯誤");
                    }
                }
            };
            GroupClient.fnGetTBGROUPSAsync(new TBGroupStruct() { FSGROUP_ID = "" });

        }

        ObservableCollection<TBGroupStruct> ReSortTBGroupStructCollection(ObservableCollection<TBGroupStruct> collection)
        {
            var orderedCollection = collection.OrderBy(a => a.FSGROUP_ID);
            ObservableCollection<TBGroupStruct> newCollection = new ObservableCollection<TBGroupStruct>();
            foreach (TBGroupStruct group in orderedCollection)
            {
                newCollection.Add(group);
            }
            return newCollection;
        }


        ObservableCollection<TBGroupStruct> userGroups = new ObservableCollection<TBGroupStruct>();
        public ObservableCollection<TBGroupStruct> UserGroups
        {
            get { return userGroups; }
            set
            {
                userGroups = value;

                RaisePropertyChanged("UserGroups");
            }
        }


        ObservableCollection<TBGroupStruct> allGroups = new ObservableCollection<TBGroupStruct>();
        public ObservableCollection<TBGroupStruct> AllGroups
        {
            get { return allGroups; }
            set { allGroups = value; RaisePropertyChanged("AllGroups"); }
        }

        ObservableCollection<TBGroupStruct> addGroupsSanbox = new ObservableCollection<TBGroupStruct>();

        public ObservableCollection<TBGroupStruct> AddGroupsSanbox
        {
            get { return addGroupsSanbox; }
            set { addGroupsSanbox = value; RaisePropertyChanged("AddGroupsSanbox"); }
        }

        ObservableCollection<TBGroupStruct> removeGroupsSanbox = new ObservableCollection<TBGroupStruct>();

        public ObservableCollection<TBGroupStruct> RemoveGroupsSanbox
        {
            get { return removeGroupsSanbox; }
            set { removeGroupsSanbox = value; RaisePropertyChanged("RemoveGroupsSanbox"); }
        }




        private void AddBtn_Click(object sender, RoutedEventArgs e)
        {
            if (this.AllGroupListBox.SelectedItem != null)
            {
                AddGroupMethod((TBGroupStruct)this.AllGroupListBox.SelectedItem);

            }
            else if (this.RemoveTempListBox.SelectedItem != null)
            {
                AddGroupMethod((TBGroupStruct)this.RemoveTempListBox.SelectedItem);
            }
            else
            {
                MessageBox.Show("請選擇要新增的項目!");
            }
        }


        void AddGroupMethod(TBGroupStruct selectedGroup)
        {
            this.AllGroups.Remove(selectedGroup);
            this.UserGroups.Add(selectedGroup);

            if (this.RemoveGroupsSanbox.Contains(selectedGroup))
            {
                this.RemoveGroupsSanbox.Remove(selectedGroup);
            }
            else
            {
                this.AddGroupsSanbox.Add(selectedGroup);
            }

            UserGroups = ReSortTBGroupStructCollection(UserGroups);
        }

        void RemoveGroupMethod(TBGroupStruct selectedGroup)
        {
            this.AllGroups.Add(selectedGroup);
            this.UserGroups.Remove(selectedGroup);

            if (this.AddGroupsSanbox.Contains(selectedGroup))
            {
                this.AddGroupsSanbox.Remove(selectedGroup);
            }
            else
            {
                this.RemoveGroupsSanbox.Add(selectedGroup);
            }
            AllGroups = ReSortTBGroupStructCollection(AllGroups);
        }
        private void RemoveBtn_Click(object sender, RoutedEventArgs e)
        {
            if (this.UserGroupListBox.SelectedItem != null)
            {
                RemoveGroupMethod((TBGroupStruct)this.UserGroupListBox.SelectedItem);

            }
            else if (this.AddTempListBox.SelectedItem != null)
            {
                RemoveGroupMethod((TBGroupStruct)this.AddTempListBox.SelectedItem);
            }
            else
            {
                MessageBox.Show("請選擇要移除的項目!");
            }
        }

        void DeleteUserGroup_Batch()
        {
            ObservableCollection<TBUser_GroupStruct> userGroup = new ObservableCollection<TBUser_GroupStruct>();

            foreach (TBGroupStruct group in this.RemoveGroupsSanbox)
            {
                userGroup.Add(new TBUser_GroupStruct()
                {
                    FSGROUP_ID = group.FSGROUP_ID,
                    FSUSER_ID = CurrentEditUser.FSUSER_ID,
                    FSCREATED_BY = UserClass.userData.FSUSER_ID,
                    FSUPDATED_BY = UserClass.userData.FSUSER_ID
                });
            }
            GroupClient.fnDeleteTBUSER_GROUP_BatchAsync(userGroup, UserClass.userData.FSUSER_ID);

        }

        void AddUserGroup_Batch()
        {
            ObservableCollection<TBUser_GroupStruct> userGroup = new ObservableCollection<TBUser_GroupStruct>();

            foreach (TBGroupStruct group in this.AddGroupsSanbox)
            {
                userGroup.Add(new TBUser_GroupStruct()
                {
                    FSGROUP_ID = group.FSGROUP_ID,
                    FSUSER_ID = CurrentEditUser.FSUSER_ID,
                    FSCREATED_BY = UserClass.userData.FSUSER_ID,
                    FSUPDATED_BY = UserClass.userData.FSUSER_ID
                });
            }

            GroupClient.fnInsertTBUSER_GROUP_BatchAsync(userGroup, UserClass.userData.FSUSER_ID);
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.AddGroupsSanbox.Count + this.RemoveGroupsSanbox.Count == 0)
            {
                this.DialogResult = true;
            }
            else
            {
                if (this.AddGroupsSanbox.Count != 0)
                {
                    AddUserGroup_Batch();
                }
                else
                {
                    DeleteUserGroup_Batch();
                }
            }


        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void UserGroupListBox_GotFocus(object sender, RoutedEventArgs e)
        {
            ListBox lb = (ListBox)sender;
            int index = lb.SelectedIndex;
            ClearSelectedItem();
            lb.SelectedIndex = index;

        }

        void ClearSelectedItem() 
        {
            this.UserGroupListBox.SelectedItem = null;
            this.AddTempListBox.SelectedItem = null;
            this.AllGroupListBox.SelectedItem = null;
            this.RemoveTempListBox.SelectedItem = null;
        }
    }
}

