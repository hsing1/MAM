﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.Web.ENT;

namespace PTS_MAM3.MAG
{
    public partial class MAG100_01 : ChildWindow
    {
        private ENT100DomainContext content = new ENT100DomainContext();
        private string tbName;
        public MAG100_01(string InputtbName)
        {
            InitializeComponent();
            tbName = InputtbName;
            BiggestSort();
        }
        #region 把最大的Sort抓出來
        private void BiggestSort()
        {
            int numId = 0;
            string strId;
            switch (tbName)
            {
                case "TBFILE_TYPE":
                    var TBFILE_TYPE = content.Load(content.GetTBFILE_TYPEQuery());
                    TBFILE_TYPE.Completed += (s, args) =>
                    {
                        foreach (TBFILE_TYPE x in TBFILE_TYPE.Entities)
                        {
                            if (x.FSSORT.Trim() != "")
                            {
                                if (Convert.ToInt32(x.FSSORT) > numId)
                                { numId = Convert.ToInt32(x.FSSORT); }
                            }
                        }
                        numId += 1;
                        if (numId <= 9)
                        {
                            strId = "00" + numId.ToString();
                        }
                        else if(numId>=10 && numId<=99)
                        { strId = "0" + numId.ToString(); }
                        else if (numId >= 999)
                        { strId = "999"; }
                        else
                        { strId = numId.ToString(); }
                        textBox2.Text = strId;
                    };
                    break;
            }
        }
        #endregion
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            #region 按下確認鍵後 將資料寫入資料庫
            int intCheck;
            if (int.TryParse(textBox2.Text.Trim(), out intCheck) && intCheck > 0 && textBox1.Text.Count() > 0)
            {
                #region 把最大的Id抓出來 並將資料寫回
                int numId = 0;
                string strId;
                switch (tbName)
                {
                    case "TBFILE_TYPE":
                        var TBFILE_TYPE = content.Load(content.GetTBFILE_TYPEQuery());
                        TBFILE_TYPE.Completed += (s, args) =>
                        {
                            #region 取得沒有用到的Id空號由"小到大"
                            int[] array = new int[1001];
                            for (int i = 0; i <= 1000; i++)
                            { array[i] = 0; }
                            foreach (TBFILE_TYPE x in TBFILE_TYPE.Entities)
                            {
                                if(x.FSARC_TYPE.Trim()!="")
                                array[Convert.ToInt32(x.FSARC_TYPE)] = 1;
                            }
                            for (int i = 1; i <= 1000; i++)
                            {
                                if (array[i] == 0)
                                {
                                    numId = i;
                                    break;
                                }
                                if (i == 1000)
                                {
                                    MessageBox.Show("資料庫內編號已使用完畢，請聯絡管理員!");
                                    this.DialogResult = false;
                                }
                            }
                            #endregion
                            if (numId <= 9)
                            { strId = "00" + numId.ToString(); }
                            else if (numId <= 99 && numId >= 10)
                            { strId = "0" + numId.ToString(); }
                            else
                            { strId = numId.ToString(); }
                            TBFILE_TYPE TBFILE_TYPEClass = new TBFILE_TYPE();
                            TBFILE_TYPEClass.FDCREATED_DATE = DateTime.Now;
                            TBFILE_TYPEClass.FDUPDATED_DATE = DateTime.Now;
                            TBFILE_TYPEClass.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                            TBFILE_TYPEClass.FSTYPE = ((ComboBoxItem)comboBox1.SelectedItem).Content.ToString();
                            TBFILE_TYPEClass.FSARC_TYPE = strId;
                            TBFILE_TYPEClass.FSTYPE_NAME = textBox1.Text.Trim();
                            TBFILE_TYPEClass.FSSORT = textBox2.Text;
                            TBFILE_TYPEClass.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                            TBFILE_TYPEClass.FBISENABLE = true;
                            content.TBFILE_TYPEs.Add(TBFILE_TYPEClass);
                            content.SubmitChanges().Completed += (s2, args2) =>
                            {
                                this.DialogResult = true;
                            };
                        };
                        break;
                }

                #endregion
            }
            else
            {
                if (int.TryParse(textBox2.Text.Trim(), out intCheck) != true || textBox2.Text.Trim() == "" || intCheck == 0)
                { MessageBox.Show("顯示排序欄位請填入大於0以及小於或等於99的\"數字\"!"); }
                if (textBox1.Text.Trim().Count() == 0)
                { MessageBox.Show("製作目的名稱必須填入資料!"); }
            }
            #endregion
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

