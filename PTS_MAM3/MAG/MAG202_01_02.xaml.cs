﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.SystemAdmin
{
    public partial class TemplateField_Edit : ChildWindow
    {
        WSDirectoryAdmin.TBTemplate_FieldsStruct _tf;
        public TemplateField_Edit(WSDirectoryAdmin.TBTemplate_FieldsStruct tf)
        {
            InitializeComponent();
            _tf = tf;
            this.tbxFIELD_NAME.Text = _tf.FSFIELD_NAME;
            this.tbxFNFIELD_LENGTH.Value = _tf.FNFIELD_LENGTH;
            this.tbxFNOBJECT_WIDTH.Value = _tf.FNOBJECT_WIDTH;
            this.tbxFSDEFAULT.Text = _tf.FSDEFAULT;
            if (_tf.FCISNULLABLE == "Y")
                this.cbFCISNULLABLE.IsChecked = true;
            else
                this.cbFCISNULLABLE.IsChecked = false;
            this.cbxFSFIELD.Items.Add(_tf.FSFIELD);
            this.cbxFSFIELD.SelectedIndex = 0;
            this.cbxFSFIELD.IsEnabled = false;


            
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {

            if (tbxFIELD_NAME.Text.Trim() == string.Empty)
            {
                MessageBox.Show("請輸入名稱", "錯誤!", MessageBoxButton.OK);
                tbxFIELD_NAME.Focus();
            }
            else if ((tbxFNFIELD_LENGTH.Value.ToString() == string.Empty) && ((string)((System.Windows.Controls.ContentControl)((cbxFSFIELD_TYPE.SelectedItem))).Content == "nvarchar") || ((string)((System.Windows.Controls.ContentControl)((cbxFSFIELD_TYPE.SelectedItem))).Content == "nchar"))
            {
                MessageBox.Show("請輸入欄位長度", "錯誤!", MessageBoxButton.OK);
                tbxFNFIELD_LENGTH.Focus();
            }
            else if (tbxFNOBJECT_WIDTH.Value.ToString() == string.Empty)
            {

                MessageBox.Show("請輸入元件寬度", "錯誤!", MessageBoxButton.OK);
                tbxFNOBJECT_WIDTH.Focus();
            }
            else
            {
                WSDirectoryAdmin.TBTemplate_FieldsStruct tf = new WSDirectoryAdmin.TBTemplate_FieldsStruct();

                tf.FCGRID = "N";

                if (this.cbFCISNULLABLE.IsChecked == true)
                    tf.FCISNULLABLE = "Y";
                else
                    tf.FCISNULLABLE = "N";
                tf.FNID = _tf.FNID; 
                
                tf.FCLIST = "0";
                tf.FCLIST_TYPE = "N";
                tf.FNFIELD_LENGTH = Convert.ToInt32(tbxFNFIELD_LENGTH.Value);
                tf.FNOBJECT_WIDTH = Convert.ToInt32(tbxFNOBJECT_WIDTH.Value);
                tf.FNORDER = 0;
                tf.FNTEMPLATE_ID = _tf.FNTEMPLATE_ID;
                tf.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                tf.FSDEFAULT = this.tbxFSDEFAULT.Text;
                tf.FSDESCRIPTION = "";
                tf.FSFIELD = (string)this.cbxFSFIELD.SelectedItem;
                tf.FSFIELD_NAME = tbxFIELD_NAME.Text;
                tf.FSFIELD_TYPE = (string)((System.Windows.Controls.ContentControl)((cbxFSFIELD_TYPE.SelectedItem))).Content;
                tf.FSLIST_NAME = "";
                tf.FSTABLE = _tf.FSTABLE;
                tf.FSUPDATED_BY = UserClass.userData.FSUSER_ID;




                WSDirectoryAdmin.WSDirectoryAdminSoapClient dirObj = new WSDirectoryAdmin.WSDirectoryAdminSoapClient();

                dirObj.fnUpdateTBTEMPLATE_FIELDSCompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (args.Result == false)
                        {
                            MessageBox.Show("資料更新時發生錯誤!", "錯誤", MessageBoxButton.OK);

                        }
                        else
                            this.DialogResult = true;
                    }

                };

                dirObj.fnUpdateTBTEMPLATE_FIELDSAsync(tf, UserClass.userData.FSUSER_ID);


            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

