﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.MAG
{
    public partial class MAG520_01 : ChildWindow
    {
        // 雖然我在 SR_WSPGM_GetMssStatus.ClassLouthJob 有埋了 full file path 的屬性
        // 但是怕被別人用來移花接木，而導致一些重要檔案被刪除
        // 所以我在傳遞參數時還是以檔名為主，讓後端自行去判斷真正的路徑為何

        SR_WSPGM_GetMssStatus.ClassLouthJob m_FormData;

        // 從母畫面傳來的物件參數
        public MAG520_01(SR_WSPGM_GetMssStatus.ClassLouthJob FormData)
        {
            InitializeComponent();
            // 將母畫面的資料寫到子畫面的區域變數中
            //m_FormData = FormData;
            // 將物件現有的屬性綁到介面中
            //fnBindClassAttributesToForm();
            // 查詢更詳細的資料
            fnGetLouthJobDetailStatus(FormData.XmlFileName);
        }

        private void button_Close_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void button_Refresh_Click(object sender, RoutedEventArgs e)
        {
            fnGetLouthJobDetailStatus(m_FormData.XmlFileName);
        }

        private void button_RestartLouthJob_Click(object sender, RoutedEventArgs e)
        {
            fnDisableButtons();
            SR_WSPGM_GetMssStatus.ServicePGMSoapClient obj = new SR_WSPGM_GetMssStatus.ServicePGMSoapClient();
            obj.ResetMasterControlLouthJobCompleted += new EventHandler<SR_WSPGM_GetMssStatus.ResetMasterControlLouthJobCompletedEventArgs>(obj_ResetMasterControlLouthJobCompleted);
            obj.ResetMasterControlLouthJobAsync(m_FormData.XmlFileName);
        }

        private void button_DeleteLouthJob_Click(object sender, RoutedEventArgs e)
        {
            fnDisableButtons();
            SR_WSPGM_GetMssStatus.ServicePGMSoapClient obj = new SR_WSPGM_GetMssStatus.ServicePGMSoapClient();
            obj.DeleteMasterControlLouthJobCompleted += new EventHandler<SR_WSPGM_GetMssStatus.DeleteMasterControlLouthJobCompletedEventArgs>(obj_DeleteMasterControlLouthJobCompleted);
            obj.DeleteMasterControlLouthJobAsync(m_FormData.XmlFileName);
        }

        private void fnDisableButtons()
        {
            button_Refresh.IsEnabled = false;
            button_Close.IsEnabled = false;
            button_RestartLouthJob.IsEnabled = false;
            button_DeleteLouthJob.IsEnabled = false;
        }

        private void fnEnableButtons()
        {
            button_Refresh.IsEnabled = true;
            button_Close.IsEnabled = true;
            button_RestartLouthJob.IsEnabled = true;
            button_DeleteLouthJob.IsEnabled = true;
        }

        private void fnGetLouthJobDetailStatus(string fileName)
        {
            fnDisableButtons();
            SR_WSPGM_GetMssStatus.ServicePGMSoapClient obj = new SR_WSPGM_GetMssStatus.ServicePGMSoapClient();
            obj.GetMasterControlLouthDetailStatusCompleted += new EventHandler<SR_WSPGM_GetMssStatus.GetMasterControlLouthDetailStatusCompletedEventArgs>(obj_GetMasterControlLouthDetailStatusCompleted);
            obj.GetMasterControlLouthDetailStatusAsync(fileName);
        }

        // 將物件的屬性綁到介面中
        private void fnBindClassAttributesToForm()
        {
            textBlock_11.Text = m_FormData.XmlFileName;
            textBlock_12.Text = m_FormData.XmlFileStatus;
            DG_LouthJobResult.DataContext = null;
            // 試試看 List<Struct> 是不是也可以放到 DataGrid 中
            if (m_FormData.XmlProcessErrors != null && m_FormData.XmlProcessErrors.Count > 0)
                DG_LouthJobResult.DataContext = m_FormData.XmlProcessErrors;
        }

        private void obj_GetMasterControlLouthDetailStatusCompleted(object sender, SR_WSPGM_GetMssStatus.GetMasterControlLouthDetailStatusCompletedEventArgs e)
        {
            // 
            fnEnableButtons();
            // 
            if (e.Error == null)
            {
                if (e.Result == null)
                {
                    MessageBox.Show("取得Louth寫入任務的資訊時發生IO錯誤，請重新查詢");
                    DialogResult = true;
                }
                else
                {
                    m_FormData = e.Result;
                    fnBindClassAttributesToForm();
                }
            }
            else
                MessageBox.Show("連結主機服務時發生問題：" + e.Error.ToString());
        }

        private void obj_ResetMasterControlLouthJobCompleted(object sender, SR_WSPGM_GetMssStatus.ResetMasterControlLouthJobCompletedEventArgs e)
        {
            // 
            fnEnableButtons();
            // 
            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    MessageBox.Show("重置Louth寫入作業已完成");
                    DialogResult = true;
                }
                else
                {
                    MessageBox.Show("重置Louth寫入作業時發生錯誤，請重試");
                }
            }
            else
                MessageBox.Show("連結主機服務時發生問題：" + e.Error.ToString());
        }

        private void obj_DeleteMasterControlLouthJobCompleted(object sender, SR_WSPGM_GetMssStatus.DeleteMasterControlLouthJobCompletedEventArgs e)
        {
            // 
            fnEnableButtons();
            // 
            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    MessageBox.Show("刪除Louth寫入作業已完成");
                    DialogResult = true;
                }
                else
                {
                    MessageBox.Show("刪除Louth寫入作業時發生錯誤，請重試");
                }
            }
            else
                MessageBox.Show("連結主機服務時發生問題：" + e.Error.ToString());
        }
    }
}
