﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.MAG
{
    public partial class MAG600 : UserControl
    {
        public MAG600()
        {
            InitializeComponent();
            //
            fnQueryTapeNeedOnlineList();
        }


        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            btnRefresh.IsEnabled = false;
            // 發出查詢的要求
            fnQueryTapeNeedOnlineList();
        }

        private void fnQueryTapeNeedOnlineList()
        {
            // 直接送出查詢給 WS
            SR_WSTSM.ServiceTSMSoapClient objTSMGetTapeInfo = new SR_WSTSM.ServiceTSMSoapClient();
            objTSMGetTapeInfo.GetVolumeNeedOnlineCompleted += new EventHandler<SR_WSTSM.GetVolumeNeedOnlineCompletedEventArgs>(objTSMGetTapeInfo_GetVolumeNeedOnlineCompleted);
            objTSMGetTapeInfo.GetVolumeNeedOnlineAsync();
        }

        private void objTSMGetTapeInfo_GetVolumeNeedOnlineCompleted(object sender, SR_WSTSM.GetVolumeNeedOnlineCompletedEventArgs e)
        {
            // 
            btnRefresh.IsEnabled = true;
            // 清除原有的資料
            DataGrid_TapeList.DataContext = null;

            // 檢查是否有錯誤
            if (e.Error == null)
            {
                if (e.Result.Count == 0)
                {
                    MessageBox.Show("目前沒有磁帶上架的需求。");
                    return;
                }
                // 
                DataGrid_TapeList.DataContext = e.Result;
            }
            else
                MessageBox.Show("網路連接發生錯誤。" + e.Error.ToString());
        }
    }
}
