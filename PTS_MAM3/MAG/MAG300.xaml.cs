﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;

namespace PTS_MAM3.MAG
{
    public partial class MAG300 : Page
    {
        private System.Windows.Threading.DispatcherTimer sysTimer = new System.Windows.Threading.DispatcherTimer();

        public MAG300()
        {
            InitializeComponent();
            // 初始化元件
            dpStart.Text = DateTime.Now.ToString("yyyy/M/d");
            dpEnd.Text = DateTime.Now.ToString("yyyy/M/d");
            // Timer Setting
            sysTimer.Stop();
            sysTimer.Interval = new TimeSpan(0, 1, 0);      // 時分秒
            sysTimer.Tick += new EventHandler(sysTimer_Tick);
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //
        }

        private void sysTimer_Tick(object sender, EventArgs e)
        {
            // 自動查詢
            if ((bool)this.chkREFRESH.IsChecked)
            {
                fnGetTranscodeInformation(false);
            }
        }

        /// <summary>
        /// 顯示詳細資訊的按鈕
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDetail_Click(object sender, RoutedEventArgs e)
        {
            // 判斷是否有選取DataGrid
            if (DGASCList.SelectedItem == null)
            {
                MessageBox.Show("請選擇要查詢的項目", "提示訊息", MessageBoxButton.OK);
                return;
            }

            // 暫時停止自動更新
            sysTimer.Stop();

            // 開啟子視窗
            MAG300_01 MAG300_01_frm = new MAG300_01(((SR_ServiceTranscode.ClassTranscodeJob)DGASCList.SelectedItem));
            MAG300_01_frm.Show();

            //20150925 david.sin取消關掉畫面reload
            //MAG300_01_frm.Closing += (s, args) =>
            //{
            //    // 重新讀取主畫面的資料
            //    fnGetTranscodeInformation(false);
            //};
        }

        /// <summary>
        /// 查詢的按鈕
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnQuery_Click(object sender, RoutedEventArgs e)
        {
            fnGetTranscodeInformation(true);
        }

        /// <summary>
        /// 處理DataGrid在載入每一行資料列時的事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DGASCList_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            // 預設正常的前景、背景色
            e.Row.Background = new SolidColorBrush(Colors.White);
            e.Row.Foreground = new SolidColorBrush(Colors.Black);
            // Tricky: 檢查狀態，如果有含"錯誤"字樣的，就要變色!
            SR_ServiceTranscode.ClassTranscodeJob obj = e.Row.DataContext as SR_ServiceTranscode.ClassTranscodeJob;
            if (!string.IsNullOrEmpty(obj.AnsJobStatus) && obj.AnsJobStatus.Contains("錯誤"))
                e.Row.Background = new SolidColorBrush(Colors.Red);
        }

        private void fnDisableButtons()
        {
            btnQuery.IsEnabled = false;
            btnDetail.IsEnabled = false;
        }

        private void fnEnableButtons()
        {
            btnQuery.IsEnabled = true;
            btnDetail.IsEnabled = true;
        }

        private void fnGetTranscodeInformation(bool showMsgBox)
        {
            // 如果在這邊設定，代表，會讓現有的 sysTimer 停下來
            sysTimer.Stop();
            // 
            if (string.IsNullOrEmpty(dpStart.Text) || string.IsNullOrEmpty(dpEnd.Text))
            {
                if (showMsgBox)
                    MessageBox.Show("選擇的時間參數有誤");
                return;
            }
            //
            if (DateTime.Compare(DateTime.Parse(dpStart.Text), DateTime.Parse(dpEnd.Text)) > 0)
            {
                if (showMsgBox)
                    MessageBox.Show("選擇的起始時間大於結束時間");
                return;
            }
            //
            fnDisableButtons();
            // Call Web Service to Get All Information
            SR_ServiceTranscode.ServiceTranscodeSoapClient objTranscode = new SR_ServiceTranscode.ServiceTranscodeSoapClient();
            objTranscode.GetTranscodeJobsInformationCompleted += new EventHandler<SR_ServiceTranscode.GetTranscodeJobsInformationCompletedEventArgs>(objTranscode_GetTranscodeJobInformationCompleted);
            objTranscode.GetTranscodeJobsInformationAsync(dpStart.Text, dpEnd.Text);
        }

        private void objTranscode_GetTranscodeJobInformationCompleted(object sender, SR_ServiceTranscode.GetTranscodeJobsInformationCompletedEventArgs e)
        {
            //
            fnEnableButtons();
            DGASCList.DataContext = null;
            //
            if (e.Error == null)
            {
                if (e.Result.Count == 0)
                {
                    MessageBox.Show("查不到該範圍區間的資料");
                    return;
                }
                // 
                DGASCList.DataContext = e.Result;
                
                // 2012-08-15：停用計時器做刷新的動作
                //sysTimer.Start();
            }
            else
                MessageBox.Show("連結主機服務時發生問題：" + e.Error.ToString());
        }
    }
}
