﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSPROG_M;
using PTS_MAM3.WSPROMO;
using PTS_MAM3.WSREPORT;
using System.Text;

namespace PTS_MAM3.PRG
{
    public partial class PRG100_13 : ChildWindow
    {
        WSPROG_MSoapClient client = new WSPROG_MSoapClient();      //產生新的代理類別
        WSPROMOSoapClient clientP = new WSPROMOSoapClient();

        string m_strType = "";            //類型,節目或短帶
        string m_strProgID = "";          //節目編號
        string m_strProgName = "";        //節目名稱
        string m_CHANNEL_ID = "";         //頻道ID
        string m_Dep_ID = "";             //成案單位
        int m_FNUpperDept_ID;             //上層組織 
        int m_FNDep_ID;                   //組織
        int m_FNGroup_ID;                 //組別
        public string m_CHANNEL_ID_LAST="";//最後選擇的頻道別
        public int m_FNDept_ID_LAST;      //最後選擇的部門別 

        public Boolean bolcheck = false;  //判斷是否已認定完基本資料  

        //成案單位
        private int intCbx1 = 0, intCbx2 = -1, intCbx3 = -1;

        List<MyDept> deptlist1 = new List<MyDept>();
        List<MyDept> deptlist2 = new List<MyDept>();
        List<MyDept> deptlist3 = new List<MyDept>();

        WSREPORTSoapClient objReport1 = new WSREPORTSoapClient();
        WSREPORTSoapClient objReport2 = new WSREPORTSoapClient();
        WSREPORTSoapClient objReport3 = new WSREPORTSoapClient();

        public PRG100_13(string strType,string strProgID, string strProgName,string strChannelID,string strDepID)
        {
            InitializeComponent();

            m_strProgID = strProgID;
            m_strProgName = strProgName;
            m_CHANNEL_ID = strChannelID;
            m_Dep_ID = strDepID;
            m_strType = strType;

            tbxtProgID.Text = m_strProgID;
            tbxPGMNAME.Text = m_strProgName;

            InitializeForm();   //初始化本頁面
        }

        void InitializeForm() //初始化本頁面
        {
            //下載代碼檔
            client.fnGetTBPROG_M_CODECompleted += new EventHandler<fnGetTBPROG_M_CODECompletedEventArgs>(client_fnGetTBPROG_M_CODECompleted);
            client.fnGetTBPROG_M_CODEAsync();

            //修改節目主檔_節目資料認定_修改頻道別及成案單位
            client.UPDATE_TBPROGM_IDENTIFIEDCompleted += new EventHandler<UPDATE_TBPROGM_IDENTIFIEDCompletedEventArgs>(client_UPDATE_TBPROGM_IDENTIFIEDCompleted);
            
            //修改短帶資料檔_宣傳帶資料認定_修改頻道別及成案單位
            clientP.UPDATE_TBPGM_PROMO_IDENTIFIEDCompleted += new EventHandler<UPDATE_TBPGM_PROMO_IDENTIFIEDCompletedEventArgs>(clientP_UPDATE_TBPGM_PROMO_IDENTIFIEDCompleted);

            //修改所有子集的頻道別以及影音圖文的頻道別
            client.UPDATE_TBPROGD_TBLOG_CHANNELCompleted += new EventHandler<UPDATE_TBPROGD_TBLOG_CHANNELCompletedEventArgs>(client_UPDATE_TBPROGD_TBLOG_CHANNELCompleted);

            //節目資料認定_比對頻道別類型
            client.fnCheck_ChannelTypeCompleted += new EventHandler<fnCheck_ChannelTypeCompletedEventArgs>(client_fnCheck_ChannelTypeCompleted);

            //查詢該部門分類裡的部門
            objReport1.fnGetDeptListCompleted += new EventHandler<WSREPORT.fnGetDeptListCompletedEventArgs>(objReport1_fnGetDeptListCompleted);
            objReport2.fnGetDeptListCompleted += new EventHandler<WSREPORT.fnGetDeptListCompletedEventArgs>(objReport2_fnGetDeptListCompleted);
            objReport3.fnGetDeptListCompleted += new EventHandler<WSREPORT.fnGetDeptListCompletedEventArgs>(objReport3_fnGetDeptListCompleted);

            //查詢部門
            objReport1.QUERY_TBUSER_DEP_BYIDCompleted += new EventHandler<QUERY_TBUSER_DEP_BYIDCompletedEventArgs>(objReport1_QUERY_TBUSER_DEP_BYIDCompleted);

            if (m_Dep_ID != "" && m_Dep_ID != "0")      //查詢成案單位，若是為0表示是初始的值，就不用查詢成案單位
                objReport1.QUERY_TBUSER_DEP_BYIDAsync(m_Dep_ID);
            else
                objReport1.fnGetDeptListAsync(intCbx1); //下載單位代碼及比對成案單位

            BusyMsg.IsBusy = true;
        }     

        #region 實作

        //實作-查詢部門
        void objReport1_QUERY_TBUSER_DEP_BYIDCompleted(object sender, QUERY_TBUSER_DEP_BYIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                DeptStruct obj = new DeptStruct();
                obj = e.Result;

                if (obj.FNDEP_LEVEL == 1)           //層級2，表示第一層為FNDEP_ID，不用再查
                {
                    m_FNUpperDept_ID = obj.FNDEP_ID;                     
                }
                else if (obj.FNDEP_LEVEL == 2)      //層級2，表示第一層為FNPARENT_DEP_ID，第二層為FNDEP_ID，不用再查
                {
                    int.TryParse(obj.FNPARENT_DEP_ID.ToString(), out m_FNUpperDept_ID) ; 
                    m_FNDep_ID = obj.FNDEP_ID;                   
                }
                else if (obj.FNDEP_LEVEL == 3 || obj.FNDEP_LEVEL == 4)  //層級3，表示第二層為FNPARENT_DEP_ID，第三層為FNDEP_ID，需要再查第一層
                {                    
                    m_FNGroup_ID = obj.FNDEP_ID;
                    int.TryParse(obj.FNPARENT_DEP_ID.ToString(), out m_FNDep_ID);
                    objReport1.QUERY_TBUSER_DEP_BYIDAsync(obj.FNPARENT_DEP_ID.ToString());  //再查一層
                    return;
                }                
            }

            objReport1.fnGetDeptListAsync(intCbx1); //下載單位代碼及比對成案單位
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }

        //實作-查詢該部門分類裡的部門
        void objReport1_fnGetDeptListCompleted(object sender, WSREPORT.fnGetDeptListCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                cbxDept2.ItemsSource = null;
                cbxDept3.ItemsSource = null;      

                if (e.Result.Count == 0)
                {
                    MessageBox.Show("查詢單位時發生錯誤");
                    return;
                }
                else
                {
                    deptlist1.Clear();

                    foreach (WSREPORT.DeptStruct dept in e.Result)
                    {
                        deptlist1.Add(new MyDept(dept.FSDpeName_ChtName, dept.FNDEP_ID));
                    }

                    cbxDept1.ItemsSource = deptlist1;

                    if (m_FNUpperDept_ID != -1)
                    {
                        for (int i = 0; i < cbxDept1.Items.Count; i++)
                        {
                            if (((MyDept)cbxDept1.Items[i]).id.ToString().Trim().Equals(m_FNUpperDept_ID.ToString().Trim()))
                            {
                                cbxDept1.SelectedIndex = i;
                                break;
                            }
                        }
                    }
                }
            }
        }

        void objReport2_fnGetDeptListCompleted(object sender, WSREPORT.fnGetDeptListCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            deptlist2.Clear();
            cbxDept2.ItemsSource = null;
            cbxDept3.ItemsSource = null;    

            if (e.Error == null)
            {
                if (e.Result.Count == 0)
                {
                    //MessageBox.Show("查不到子單位");
                    cbxDept3.ItemsSource = null;
                    return;
                }
                else
                {
                    if (intCbx2 != -1)
                    {
                        foreach (WSREPORT.DeptStruct dept in e.Result)
                        {
                            deptlist2.Add(new MyDept(dept.FSDpeName_ChtName, dept.FNDEP_ID));
                        }
                    }

                    cbxDept2.ItemsSource = deptlist2;

                    if (m_FNUpperDept_ID != -1)
                    {
                        for (int i = 0; i < cbxDept2.Items.Count; i++)
                        {
                            if (((MyDept)cbxDept2.Items[i]).id.ToString().Trim().Equals(m_FNDep_ID.ToString().Trim()))
                            {
                                cbxDept2.SelectedIndex = i;
                                m_FNUpperDept_ID = -1;
                                break;
                            }
                        }
                    }

                }
            }
        }

        void objReport3_fnGetDeptListCompleted(object sender, WSREPORT.fnGetDeptListCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            deptlist3.Clear();
            cbxDept3.ItemsSource = null;

            if (e.Error == null)
            {
                if (e.Result.Count == 0)
                {
                    //MessageBox.Show("查不到子單位");
                    return;
                }
                else
                {
                    if (intCbx3 != -1)
                    {
                        foreach (WSREPORT.DeptStruct dept in e.Result)
                        {
                            deptlist3.Add(new MyDept(dept.FSDpeName_ChtName, dept.FNDEP_ID));
                        }
                    }

                    cbxDept3.ItemsSource = deptlist3;

                    if (m_FNDep_ID != -1)
                    {
                        for (int i = 0; i < cbxDept3.Items.Count; i++)
                        {
                            if (((MyDept)cbxDept3.Items[i]).id.ToString().Trim().Equals(m_FNGroup_ID.ToString().Trim()))
                            {
                                cbxDept3.SelectedIndex = i;
                                m_FNDep_ID = -1;
                                break;
                            }
                        }
                    }
                }
            }
        }

        //實作-修改節目主檔_節目資料認定_修改頻道別及成案單位
        void client_UPDATE_TBPROGM_IDENTIFIEDCompleted(object sender, UPDATE_TBPROGM_IDENTIFIEDCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    //檢查要不要更新頻道
                    if (((ComboBoxItem)cbCHANNEL_ID.SelectedItem).Tag.ToString() != m_CHANNEL_ID)
                    {
                        //1.修改directory的function，移動該節目及所有的子集節點
                        //2.修改所有子集的頻道別以及影音圖文的頻道別
                        client.UPDATE_TBPROGD_TBLOG_CHANNELAsync("G",m_strProgID,((ComboBoxItem)cbCHANNEL_ID.SelectedItem).Tag.ToString(),UserClass.userData.FSUSER_ID.ToString());
                        BusyMsg.IsBusy = true;
                    }
                    else
                    {
                        MessageBox.Show("修改節目資料「" + m_strProgName.ToString() + "」成功！", "提示訊息", MessageBoxButton.OK);
                        bolcheck = true;
                        this.DialogResult = true;
                    } 
                }
                else
                {
                    MessageBox.Show("修改節目資料「" + m_strProgName.ToString() + "」失敗！", "提示訊息", MessageBoxButton.OK);
                    //this.DialogResult = false;
                }
            }
        }

        //實作-修改短帶資料檔_宣傳帶資料認定_修改頻道別及成案單位
        void clientP_UPDATE_TBPGM_PROMO_IDENTIFIEDCompleted(object sender, UPDATE_TBPGM_PROMO_IDENTIFIEDCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    //檢查要不要更新頻道
                    if (((ComboBoxItem)cbCHANNEL_ID.SelectedItem).Tag.ToString() != m_CHANNEL_ID)
                    {
                        //1.修改directory的function，移動該節目及所有的子集節點
                        //2.修改所有子集的頻道別以及影音圖文的頻道別
                        client.UPDATE_TBPROGD_TBLOG_CHANNELAsync("P",m_strProgID,((ComboBoxItem)cbCHANNEL_ID.SelectedItem).Tag.ToString(),UserClass.userData.FSUSER_ID.ToString());
                        BusyMsg.IsBusy = true;
                    }
                    else
                    {
                        MessageBox.Show("修改短帶資料「" + m_strProgName.ToString() + "」成功！", "提示訊息", MessageBoxButton.OK);
                        bolcheck = true;
                        this.DialogResult = true;
                    } 
                }
                else
                {
                    MessageBox.Show("修改短帶資料「" + m_strProgName.ToString() + "」失敗！", "提示訊息", MessageBoxButton.OK);                    
                }
            }
        }
        
        //實作-修改所有子集的頻道別以及影音圖文的頻道別
        void client_UPDATE_TBPROGD_TBLOG_CHANNELCompleted(object sender, UPDATE_TBPROGD_TBLOG_CHANNELCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    MessageBox.Show("修改資料「" + m_strProgName.ToString() + "」成功！", "提示訊息", MessageBoxButton.OK);
                    bolcheck = true;
                    this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("修改資料「" + m_strProgName.ToString() + "」失敗！", "提示訊息", MessageBoxButton.OK);
                }
            }
        }

        //實作-節目資料認定_比對頻道別類型
        void client_fnCheck_ChannelTypeCompleted(object sender, fnCheck_ChannelTypeCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result == true)
                    UPDATE_TBPROGM_IDENTIFIED();    //進行節目資料認定        
                else
                    MessageBox.Show("請注意頻道類型，SD頻道只能換SD頻道、HD頻道只能換HD頻道！", "提示訊息", MessageBoxButton.OK);                
            }
        }  

        #endregion

        #region ComboBox載入代碼檔

        //實作-下載所有的代碼檔
        void client_fnGetTBPROG_M_CODECompleted(object sender, fnGetTBPROG_M_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        WSPROG_M.Class_CODE CodeData = new WSPROG_M.Class_CODE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            case "TBZCHANNEL":       //頻道別代碼
                                ComboBoxItem cbiCHANNEL = new ComboBoxItem();
                                cbiCHANNEL.Content = CodeData.NAME;
                                cbiCHANNEL.Tag = CodeData.ID;
                                this.cbCHANNEL_ID.Items.Add(cbiCHANNEL);
                                break;

                            default:
                                break;
                        }
                    }

                    //比對代碼檔                    
                    compareCode_FSCHANNEL_ID(m_CHANNEL_ID.ToString().Trim());             
                }
            }
        }

        #endregion

        #region 比對代碼檔

        //比對代碼檔_頻道別
        void compareCode_FSCHANNEL_ID(string strID)
        {
            for (int i = 0; i < cbCHANNEL_ID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbCHANNEL_ID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbCHANNEL_ID.SelectedIndex = i;
                    break;
                }
            }
        }

        #endregion

        #region 檢查畫面資料及搬值

        private Class_PROGM FormToClass_PROGM()
        {
            Class_PROGM obj = new Class_PROGM();

            obj.FSPROG_ID = m_strProgID;                            //節目編號
            obj.FSPGMNAME = m_strProgName;                          //節目名稱

            if (cbCHANNEL_ID.SelectedIndex != -1)                   //頻道別
                obj.FSCHANNEL_ID = ((ComboBoxItem)cbCHANNEL_ID.SelectedItem).Tag.ToString();
            else
                obj.FSCHANNEL_ID = "";

            obj.FNDEP_ID = checkDepID();
            obj.FSUPDUSER = UserClass.userData.FSUSER_ID.ToString();//修改者

            m_CHANNEL_ID_LAST = obj.FSCHANNEL_ID;                   //最後選到的頻道別及部門別
            m_FNDept_ID_LAST = obj.FNDEP_ID;

            return obj;
        }

        private Class_PROMO FormToClass_PROMO()
        {
            Class_PROMO obj = new Class_PROMO();

            obj.FSPROMO_ID = m_strProgID;                               //短帶編號
            obj.FSPROMO_NAME = m_strProgName;                           //短帶名稱

            if (cbCHANNEL_ID.SelectedIndex != -1)                       //頻道別
                obj.FSMAIN_CHANNEL_ID = ((ComboBoxItem)cbCHANNEL_ID.SelectedItem).Tag.ToString();
            else
                obj.FSMAIN_CHANNEL_ID = "";

            obj.FNDEP_ID = checkDepID();
            obj.FSUPDATED_BY = UserClass.userData.FSUSER_ID.ToString(); //修改者

            m_CHANNEL_ID_LAST = obj.FSMAIN_CHANNEL_ID;                  //最後選到的頻道別及部門別
            m_FNDept_ID_LAST = obj.FNDEP_ID;

            return obj;
        }

        //檢查成案單位選到哪一個
        private int checkDepID()
        {
            int strReturn = 0 ;

            if (cbxDept3.SelectedIndex != -1)
            {
                strReturn = Convert.ToInt32(((MyDept)cbxDept3.SelectedItem).id.ToString().Trim());
                return strReturn;
            }

            if (cbxDept2.SelectedIndex != -1)
            {
                strReturn = Convert.ToInt32(((MyDept)cbxDept2.SelectedItem).id.ToString().Trim());
                return strReturn;
            }                 

            if (cbxDept1.SelectedIndex != -1)
                strReturn = Convert.ToInt32(((MyDept)cbxDept1.SelectedItem).id.ToString().Trim()); 

            return strReturn;
        }

        private Boolean Check_Data()    //檢查畫面上的欄位是否都填妥
        {
            StringBuilder strMessage = new StringBuilder();
            
            if (cbCHANNEL_ID.SelectedIndex == -1)
                strMessage.AppendLine("請選擇「頻道」欄位");

            if (cbxDept1.SelectedIndex == -1 && cbxDept2.SelectedIndex == -1 && cbxDept3.SelectedIndex == -1)
                strMessage.AppendLine("請選擇「成案單位」欄位");

            if (strMessage.ToString().Trim() != "")
            {
                MessageBoxResult result = MessageBox.Show(strMessage.ToString(), "提示訊息", MessageBoxButton.OK);
                return false;
            }           

            return true;
        }

        private Boolean Check_Data_Change()    //檢查是否有重新選擇資料
        {
           //檢查是不是有改變成案單位及頻道別，若是沒有改就不要更新資料庫 
           string strNewChannel = ((ComboBoxItem)cbCHANNEL_ID.SelectedItem).Tag.ToString();     
           string strNewDep = checkDepID().ToString();     
                
           if (strNewChannel != "" && strNewDep != "0" && m_CHANNEL_ID == strNewChannel && strNewDep == m_Dep_ID)
           {     
               MessageBox.Show("無重新設定頻道別及成案部門，因此不更新資料庫", "提示訊息", MessageBoxButton.OK);
               return false;
           }     
           else     
               return true;
        }

        private Boolean Check_Data_ChannelType()    //檢查頻道別的SD、HD，SD頻道只能換SD頻道、HD頻道只能換HD頻道
        {
            if (m_CHANNEL_ID != "" && m_CHANNEL_ID.Trim() != ((ComboBoxItem)cbCHANNEL_ID.SelectedItem).Tag.ToString())
            {
                client.fnCheck_ChannelTypeAsync(m_CHANNEL_ID, ((ComboBoxItem)cbCHANNEL_ID.SelectedItem).Tag.ToString());
                BusyMsg.IsBusy = true;
                return false;
            }
            else
                 return true;
        }

        #endregion

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (Check_Data() == false)              //檢查畫面上的欄位是否都填妥
                return;

            if (Check_Data_Change() == false)       //檢查是否有重新選擇資料
            {
                bolcheck = true;
                this.DialogResult = true;
                return;
            }

            if (Check_Data_ChannelType() == false)  //檢查頻道別類型
                return;            

            UPDATE_TBPROGM_IDENTIFIED();    //節目資料認定
        }

        //節目資料認定
        private void UPDATE_TBPROGM_IDENTIFIED()
        {
            if (m_strType == "G")
            {
                //修改節目主檔_節目資料認定_修改頻道別及成案單位
                client.UPDATE_TBPROGM_IDENTIFIEDAsync(FormToClass_PROGM(), m_CHANNEL_ID);
            }
            else if (m_strType == "P")
            {
                //修改短帶資料檔_宣傳帶資料認定_修改頻道別及成案單位
                clientP.UPDATE_TBPGM_PROMO_IDENTIFIEDAsync(FormToClass_PROMO(),m_CHANNEL_ID);
            }
        }

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {           
            this.DialogResult = false;
        }

        private void cbxDept1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            intCbx2 = ((MyDept)cbxDept1.SelectedItem).id;    
            objReport2.fnGetDeptListAsync(intCbx2);    
            BusyMsg.IsBusy = true;    
        }

        private void cbxDept2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxDept2.Items.Count != 0)    
            {    
                intCbx3 = ((MyDept)cbxDept2.SelectedItem).id;    
                objReport3.fnGetDeptListAsync(intCbx3);    
                BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息    
            }    
            else                   
                cbxDept3.ItemsSource = null;    
        }
        
    }
}

