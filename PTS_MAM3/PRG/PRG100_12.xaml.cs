﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSPROG_M;
using PTS_MAM3.WSPROG_MYSQL_IMPORT;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace PTS_MAM3.PRG
{
    public partial class PRG100_12 : ChildWindow
    {
        WSPROG_M.WSPROG_MSoapClient client = new WSPROG_MSoapClient();
             
        string m_strProgName = "";       //節目名稱                          
        string m_strProgID = "";         //節目編號    
        int m_BEG_EPISODE ;
        int m_END_EPISODE ;
        private PTS_MAM3.MainFrame parentFrame;  //批次新增子集完成後跳到子集畫面
        Class_PROGM ProgMobj = new Class_PROGM();
        Boolean bolContinue = false;

        public PRG100_12(Class_PROGM FormData, PTS_MAM3.MainFrame mainframe)
        {
            InitializeComponent();
                        
            //批次新增子集資料
            client.INSERT_TBPROGD_TBPROGCompleted += new EventHandler<INSERT_TBPROGD_TBPROGCompletedEventArgs>(client_INSERT_TBPROGD_TBPROGCompleted);
           
            ProgMobj = FormData;
            m_strProgName = ProgMobj.FSPGMNAME.Trim();
            m_strProgID = ProgMobj.FSPROG_ID.Trim();

            tbxBEG_EPISODE.Text = "1";
            tbxEND_EPISODE.Text = ProgMobj.FNTOTEPISODE.ToString().Trim();

            tbxtProgID.Text = m_strProgID;
            tbxPGMNAME.Text = m_strProgName;
            parentFrame = mainframe;
        }
               
        //實作-批次新增子集資料
        void client_INSERT_TBPROGD_TBPROGCompleted(object sender, INSERT_TBPROGD_TBPROGCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息 

            if (e.Error == null)
            {
                if (e.Result.bolResult == true)
                {
                    if (bolContinue == true)
                    {
                        MessageBox.Show("批次新增子集資料成功！", "提示訊息", MessageBoxButton.OK);
                    }
                    else
                    {
                        MessageBox.Show("批次新增子集資料成功！", "提示訊息", MessageBoxButton.OK);
                        parentFrame.generateNewPane("子集資料", "PROG_D", m_strProgID.Trim(), true);
                        this.DialogResult = true;
                    }
                }
                else
                    MessageBox.Show("批次新增子集資料失敗！\n失敗原因：" + e.Result.strErrorMsg, "提示訊息", MessageBoxButton.OK);
            }
        }

        //確定
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {            
            if (CheckData() == true)
            {             
                bolContinue = false;
                client.INSERT_TBPROGD_TBPROGAsync(ProgMobj, tbxBEG_EPISODE.Text.Trim(), tbxEND_EPISODE.Text.Trim(), UserClass.userData.FSUSER_ID.ToString());
                BusyMsg.IsBusy = true;  //關閉忙碌的Loading訊息                          
            }             
        }

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //繼續新增
        private void Continuebutton_Click(object sender, RoutedEventArgs e)
        {
            if (CheckData() == true)
            {
                bolContinue = true;
                client.INSERT_TBPROGD_TBPROGAsync(ProgMobj, tbxBEG_EPISODE.Text.Trim(), tbxEND_EPISODE.Text.Trim(), UserClass.userData.FSUSER_ID.ToString());
                BusyMsg.IsBusy = true;  //關閉忙碌的Loading訊息                          
            }                
        }

        private Boolean CheckData()
        {
            short shortCheck1 = -1, shortCheck2 = -1;
            StringBuilder strMessage = new StringBuilder();

            if (tbxBEG_EPISODE.Text.Trim() != "")
            {
                if (short.TryParse(tbxBEG_EPISODE.Text.Trim(), out shortCheck1) == false)
                {
                    strMessage.AppendLine("請檢查「集數(起)」欄位必須為整數數值型態");
                }
                else if (shortCheck1 <= 0)
                {
                    strMessage.AppendLine("請檢查「集數(起)」欄位必須>0");
                }
            }
            else
            {
                strMessage.AppendLine("請檢查「集數(起)」欄位必須輸入值");
            }

            if (tbxEND_EPISODE.Text.Trim() != "")
            {
                if (short.TryParse(tbxEND_EPISODE.Text.Trim(), out shortCheck2) == false)
                {
                    strMessage.AppendLine("請檢查「集數(迄)」欄位必須為整數數值型態");
                }
                else if (shortCheck2 <= 0)
                {
                    strMessage.AppendLine("請檢查「集數(迄)」欄位必須>0");
                }
            }
            else
            {
                strMessage.AppendLine("請檢查「集數(迄)」欄位必須輸入值");
            }

            if ((shortCheck1 >= 0) && (shortCheck2 >= 0) && (shortCheck1 > shortCheck2))
            {
                strMessage.AppendLine("請檢查「集數(起)」欄位必須小於或等於「集數(迄)」");
            }

            if (shortCheck2 > ProgMobj.FNTOTEPISODE)
            {
                strMessage.AppendLine("請檢查「集數(迄)」欄位必須小於「總集數」，該節目總共「" + ProgMobj.FNTOTEPISODE.ToString() +　"」集");
            }

            if (strMessage.ToString().Trim() == "")
                return true;
            else
            {
                MessageBoxResult result = MessageBox.Show(strMessage.ToString(), "提示訊息", MessageBoxButton.OK);
                return false;
            }
        }

    }
}

