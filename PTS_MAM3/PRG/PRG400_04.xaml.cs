﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;
using System.IO;
using System.Text;
using PTS_MAM3.WSPROGMEN;
using PTS_MAM3.PROG_M;

namespace PTS_MAM3.ProgData
{
    public partial class PROGMEN_Query : ChildWindow
    {
        WSPROGMENSoapClient client = new WSPROGMENSoapClient();                     //產生新的代理類別
        List<ClassCode> m_CodeData = new List<ClassCode>();                         //代碼檔集合
        List<Class_PROGSTAFF> m_StaffList = new List<Class_PROGSTAFF>();            //工作人員檔集合
        public List<Class_PROGMEN> m_ListFormData = new List<Class_PROGMEN>();      //記錄查詢到的資料
        public Class_PROGMEN m_Queryobj = new Class_PROGMEN();                      //查詢條件的obj
        List<string> m_StaffName = new List<string>();                              //工作人員姓名集合
        string m_strStaffID = "";                                                   //工作人員ID

        public PROGMEN_Query()
        {
            InitializeComponent();
            InitializeForm();          //初始化本頁面
        }

        void InitializeForm() //初始化本頁面
        {
            //下載代碼檔           
            client.fnGetTBPROGMEN_CODECompleted += new EventHandler<fnGetTBPROGMEN_CODECompletedEventArgs>(client_fnGetTBPROGMEN_CODECompleted);
            client.fnGetTBPROGMEN_CODEAsync();
            
            //查詢相關人員資料
            client.fnQUERY_TBPROGMENCompleted += new EventHandler<fnQUERY_TBPROGMENCompletedEventArgs>(client_fnQUERY_TBPROGMENCompleted);

            //查詢節目管理系統的工作人員資料
            client.fnGetTBPROGSTAFFCompleted += new EventHandler<fnGetTBPROGSTAFFCompletedEventArgs>(client_fnGetTBPROGSTAFFCompleted);
            client.fnGetTBPROGSTAFFAsync();

            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }

        //實作-查詢相關人員資料
        void client_fnQUERY_TBPROGMENCompleted(object sender, fnQUERY_TBPROGMENCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

             if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)  //若是有查詢結果，則顯示，不然就請使用者重新查詢
                {
                    m_ListFormData = new List<Class_PROGMEN>(e.Result);  //把取回來的結果，加上型別定義，才可以塞回去    
                    this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("查無相關人員資料，請重新輸入條件查詢！", "提示訊息", MessageBoxButton.OK);
                   return;
                }
            }
        }

        //實作-查詢節目管理系統的工作人員資料
        void client_fnGetTBPROGSTAFFCompleted(object sender, fnGetTBPROGSTAFFCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        m_StaffList.Add(e.Result[i]);
                        m_StaffName.Add(e.Result[i].FSNAME);

                        ComboBoxItem cbiStaff = new ComboBoxItem();
                        cbiStaff.Content = e.Result[i].FSNAME;
                        cbiStaff.Tag = e.Result[i].FSSTAFFID;
                        this.cbStaff.Items.Add(cbiStaff);   //取得的工作人員資料塞到ComboBox裡
                    }

                    acbStaff.ItemsSource = m_StaffName;    //AutoComplete資料繫結
                }
            }
        }

        #region 比對及轉換       

        void compareSTAFFID(string strID)      //工作人員代碼
        {
            for (int i = 0; i < cbStaff.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbStaff.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbStaff.SelectedIndex = i;
                    break;
                }
            }
        }

        #endregion       

        #region ComboBox載入代碼檔

        //實作-下載所有的代碼檔
        void client_fnGetTBPROGMEN_CODECompleted(object sender, fnGetTBPROGMEN_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            case "TBZTITLE":      //頭銜代碼
                                ComboBoxItem cbiTITLE = new ComboBoxItem();
                                cbiTITLE.Content = CodeData.NAME;
                                cbiTITLE.Tag = CodeData.ID;
                                this.cbTITLE_ID.Items.Add(cbiTITLE);
                                break;
                            default:
                                break;
                        }
                    }                 
                }
            }
        }

        #endregion

        #region 檢查畫面資料及搬值

        private Class_PROGMEN FormToClass_PROGMEN()
        {
            Class_PROGMEN obj = new Class_PROGMEN();

            if (tbxPROG_NAME.Text.Trim() == "")
                obj.FSPROG_ID = "";
            else
                obj.FSPROG_ID = tbxPROG_NAME.Tag.ToString().Trim(); //節目編碼
           
            if (tbxEPISODE.Text.ToString().Trim() == "")            //集別           
                obj.FNEPISODE = 0;  
            else
                obj.FNEPISODE = Convert.ToInt16(tbxEPISODE.Text.ToString().Trim());

            if (cbTITLE_ID.SelectedIndex != -1)                     //頭銜
                obj.FSTITLEID = ((ComboBoxItem)cbTITLE_ID.SelectedItem).Tag.ToString();
            else
                obj.FSTITLEID = "";

            obj.FSEMAIL = tbxEMAIL.Text.ToString().Trim();          //EMAIL
            obj.FSNAME = tbxNAME.Text.ToString().Trim();            //公司/姓名
            obj.FSMEMO = tbxMEMO.Text.ToString().Trim();            //備註

            //if (cbStaff.SelectedIndex != -1)
            //    obj.FSSTAFFID = ((ComboBoxItem)cbStaff.SelectedItem).Tag.ToString().Trim();    //工作人員代碼
            //else
            //    obj.FSSTAFFID = "";

            if (m_strStaffID.Trim() != "")                          //工作人員代碼
                obj.FSSTAFFID = m_strStaffID.Trim();
            else
                obj.FSSTAFFID = "";

            m_Queryobj = obj;       //紀錄查詢條件

            return obj;
        }

        private Boolean Check_PROGMENData()    //檢查畫面上的欄位是否都填妥
        {
            StringBuilder strMessage = new StringBuilder();
            string strStaffName = acbStaff.Text.ToString().Trim();
            Boolean bolStaff = false;

            //if (tbxPROG_NAME.Text.Trim() == "")
            //    strMessage.AppendLine("請選擇「節目」");

            //先不檔，沒有輸入頭銜也可以查比較人性化
            //if (cbTITLE_ID.SelectedIndex == -1)
            //    strMessage.AppendLine("請選擇「頭銜」欄位");

            if (strStaffName != "") //檢查輸入的工作人員姓名是不是有在工作人員檔裡
            {
                for (int i = 0; i < m_StaffList.Count; i++)
                {
                    if (m_StaffList[i].FSNAME.ToString().Trim().Equals(strStaffName))
                    {
                        bolStaff = true;
                        break;
                    }
                }

                if (bolStaff == false)
                    strMessage.AppendLine("工作人員-" + strStaffName + "不在工作人員檔裡，請重新輸入");
            }

            if (tbxPROG_NAME.Text.Trim() == "" && tbxEPISODE.Text.ToString().Trim() == "" && tbxNAME.Text.ToString().Trim()=="" && cbTITLE_ID.SelectedIndex == -1 && m_strStaffID.Trim() == "")
                strMessage.AppendLine("節目、子集、頭銜、公司姓名、工作人員姓名，請至少輸入一種查詢條件！");

            if (strMessage.ToString().Trim() == "")
                return true;
            else
            {
                MessageBoxResult result = MessageBox.Show(strMessage.ToString(), "提示訊息", MessageBoxButton.OK);
                return false;
            }
        }

        //選擇工作人員時，要秀工作人員的詳細內容
        private void cbStaff_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string strStaffID = ((ComboBoxItem)cbStaff.SelectedItem).Tag.ToString().Trim();

            for (int i = 0; i < m_StaffList.Count; i++)
            {
                if (m_StaffList[i].FSSTAFFID.ToString().Trim().Equals(strStaffID))
                {
                    tbxENAME.Text = m_StaffList[i].FSENAME.ToString().Trim();       //外文姓名
                    tbxWEBSITE.Text = m_StaffList[i].FSWEBSITE.ToString().Trim();  //個人網站
                    tbxINTRO.Text = m_StaffList[i].FSINTRO.ToString().Trim();       //簡介
                    tbxEINTRO.Text = m_StaffList[i].FSEINTRO.ToString().Trim();     //外文簡介
                    break;
                }
            }
        }

        #endregion    

        //開啟節目資料查詢畫面
        private void btnPROG_Click(object sender, RoutedEventArgs e)
        {
            PROGDATA_VIEW PROGDATA_VIEW_frm = new PROGDATA_VIEW();
            PROGDATA_VIEW_frm.Show();

            PROGDATA_VIEW_frm.Closed += (s, args) =>
            {
                if (PROGDATA_VIEW_frm.DialogResult == true)
                {
                    tbxPROG_NAME.Text = PROGDATA_VIEW_frm.strProgName_View;
                    tbxPROG_NAME.Tag = PROGDATA_VIEW_frm.strProgID_View;

                    tbxEPISODE.Text = "";
                    lblEPISODE_NAME.Content = "";
                }
            };
        }

        //開啟節目子集資料查詢畫面
        private void btnEPISODE_Click(object sender, RoutedEventArgs e)
        {
            if (tbxPROG_NAME.Text.ToString().Trim() != "" && tbxPROG_NAME.Tag.ToString().Trim() != "")
            {
                PRG.PRG200_07 PRG200_07_frm = new PRG.PRG200_07(tbxPROG_NAME.Tag.ToString().Trim(), tbxPROG_NAME.Text.ToString().Trim());
                PRG200_07_frm.Show();

                PRG200_07_frm.Closed += (s, args) =>
                {
                    if (PRG200_07_frm.DialogResult == true)
                    {
                        tbxEPISODE.Text = PRG200_07_frm.m_strEpisode_View;
                        lblEPISODE_NAME.Content = PRG200_07_frm.m_strProgDName_View;
                    }
                };
            }
            else
                MessageBox.Show("請先選擇節目！", "提示訊息", MessageBoxButton.OK);
        }

        //確定
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (Check_PROGMENData() == false)      //檢查畫面上的欄位是否都填妥
                return;

            //查詢相關人員資料
            client.fnQUERY_TBPROGMENAsync(FormToClass_PROGMEN());
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //自動去比對資料庫，若有相同姓名的人員就顯示資料
        private void acbStaff_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0)
            {
                tbxENAME.Text = "";
                tbxWEBSITE.Text = "";
                tbxINTRO.Text = "";
                tbxEINTRO.Text = "";
                m_strStaffID = "";
                return;
            }

            string strStaffName = ((object[])(e.AddedItems))[0].ToString();

            for (int i = 0; i < m_StaffList.Count; i++)
            {
                if (m_StaffList[i].FSNAME.ToString().Trim().Equals(strStaffName))
                {
                    tbxENAME.Text = m_StaffList[i].FSENAME.ToString().Trim();       //外文姓名
                    tbxWEBSITE.Text = m_StaffList[i].FSWEBSITE.ToString().Trim();   //個人網站
                    tbxINTRO.Text = m_StaffList[i].FSINTRO.ToString().Trim();       //簡介
                    tbxEINTRO.Text = m_StaffList[i].FSEINTRO.ToString().Trim();     //外文簡介
                    m_strStaffID = m_StaffList[i].FSSTAFFID.ToString().Trim();      //工作人員ID
                    break;
                }
            }
        }
    }
}

