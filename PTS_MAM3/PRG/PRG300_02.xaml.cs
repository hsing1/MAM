﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using PTS_MAM3.WSPROGLINK;

namespace PTS_MAM3.ProgData
{
    public partial class PROGLINK_Edit : ChildWindow
    {
        WSPROGLINKSoapClient client = new WSPROGLINKSoapClient();     //產生新的代理類別  
        Class_PROGLINK m_FormData = new Class_PROGLINK();
        string m_strProgName = "";                                    //暫存衍生節目名稱 
        string m_strEpisode = "";                                     //暫存衍生集別
        public Class_PROGLINK m_Queryobj = new Class_PROGLINK();      //查詢修改的obj

        public PROGLINK_Edit( Class_PROGLINK FormData)
        {
            InitializeComponent();
            InitializeForm();          //初始化本頁面
            m_FormData = FormData;
            ClassToForm();
        }

        void InitializeForm() //初始化本頁面
        {
            //修改扣播出次數節目子集資料
            client.UPDATE_TBPROGLINKCompleted += new EventHandler<UPDATE_TBPROGLINKCompletedEventArgs>(client_UPDATE_TBPROGLINKCompleted);
        }

        private void ClassToForm()  //將前一頁點選的節目資料繫集到畫面上
        {
            this.gdPROG_LINK.DataContext = m_FormData;

            //將class裡的資料放到畫面上

            //衍生
            tbxPROG_NAME.Text = m_FormData.FSPROG_ID_NAME.ToString();
            tbxPROG_NAME.Tag =  m_FormData.FSPROG_ID.ToString();     
            tbxEPISODE.Text =  m_FormData.SHOW_FNEPISODE.ToString();
            lblEPISODE_NAME.Content = m_FormData.FNEPISODE_NAME.ToString();

            //扣播出次數
            tbxORGPROG_NAME.Text =  m_FormData.FSORGPROG_ID_NAME.ToString();
            tbxORGPROG_NAME.Tag =  m_FormData.FSORGPROG_ID.ToString();
            tbxORGEPISODE.Text =  m_FormData.SHOW_ORGFNEPISODE.ToString();;
            lblORGEPISODE_NAME.Content = m_FormData.FNORGEPISODE_NAME.ToString();

            tbxMEMO.Text = m_FormData.FSMEMO.ToString();    //備註            
        }

        #region 實作

        //實作-修改扣播出次數節目子集資料
        void client_UPDATE_TBPROGLINKCompleted(object sender, UPDATE_TBPROGLINKCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result.bolResult == true)
                {
                    MessageBox.Show("修改衍生子集資料「" + m_strProgName.ToString() + "-" + m_strEpisode + "集」成功！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("修改衍生子集資料「" + m_strProgName.ToString() + "-" + m_strEpisode + "集」失敗！」失敗！\n失敗原因：" + e.Result.strErrorMsg, "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
        }

        #endregion
        
        #region 檢查畫面資料及搬值

        private Class_PROGLINK FormToClass_PROGLINK()
        {
            Class_PROGLINK obj = new Class_PROGLINK();

            obj.FSPROG_ID = tbxPROG_NAME.Tag.ToString().Trim();     //節目編碼
            m_strProgName = tbxPROG_NAME.Text.ToString().Trim();    //暫存本次新增的節目名稱，為了秀給使用者看結果用

            if (tbxEPISODE.Text.ToString().Trim() == "")            //集別
            {
                obj.FNEPISODE = 32767;
                m_strEpisode = "";
            }
            else
            {
                obj.FNEPISODE = Convert.ToInt16(tbxEPISODE.Text.ToString().Trim());
                m_strEpisode = tbxEPISODE.Text.ToString().Trim();       //暫存本次新增的集別，為了秀給使用者看結果用
            }

            obj.FSORGPROG_ID = tbxORGPROG_NAME.Tag.ToString().Trim();     //節目編碼

            if (tbxORGEPISODE.Text.ToString().Trim() == "")            //集別          
                obj.FNORGEPISODE = 32767;
            else
                obj.FNORGEPISODE = Convert.ToInt16(tbxORGEPISODE.Text.ToString().Trim());

            obj.FSMEMO = tbxMEMO.Text.ToString().Trim();

            obj.FSCREATED_BY = UserClass.userData.FSUSER_ID.ToString();//建檔者
            obj.FSUPDATED_BY = UserClass.userData.FSUSER_ID.ToString();//修改者                     
            m_Queryobj = obj;
            return obj;
        }        

        #endregion   

        //確定
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {           
            //修改衍生節目子集資料
            client.UPDATE_TBPROGLINKAsync(FormToClass_PROGLINK());
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

