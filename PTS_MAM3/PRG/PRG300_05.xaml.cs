﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSPROGLINK;
using System.Text;

namespace PTS_MAM3.ProgData
{
    public partial class PRG300_05 : ChildWindow
    {
        WSPROGLINKSoapClient client = new WSPROGLINKSoapClient() ;
        Boolean m_Continue = false;
        string m_strEpisode = "";                                       //暫存衍生集別
        public Class_PROGLINK m_Queryobj = new Class_PROGLINK();        //查詢新增的obj
        List<Class_PROGLINK> m_ListProgLink = new List<Class_PROGLINK>();//批次新增的衍生子集集合

        public PRG300_05()
        {
            InitializeComponent();
            InitializeForm();          //初始化本頁面
        }

        void InitializeForm() //初始化本頁面
        {
            //批次新增扣播出次數節目子集資料
            client.INSERT_TBPROLINK_BATCHCompleted += new EventHandler<INSERT_TBPROLINK_BATCHCompletedEventArgs>(client_INSERT_TBPROLINK_BATCHCompleted);

            //查詢扣播出次數節目子集資料(避免重複)
            client.QUERY_TBPROGLINK_CHECKCompleted += new EventHandler<QUERY_TBPROGLINK_CHECKCompletedEventArgs>(client_QUERY_TBPROGLINK_CHECKCompleted);
        }

        #region 實作       

        //實作-批次新增扣播出次數節目子集資料
        void client_INSERT_TBPROLINK_BATCHCompleted(object sender, INSERT_TBPROLINK_BATCHCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                List<ReturnMsg> ListMsg = e.Result;
                StringBuilder strMessage = new StringBuilder();

                for (int i = 0; i < ListMsg.Count; i++)
                {        
                    if (ListMsg[i].bolResult == false)
                        strMessage.Append("衍生節目:" + m_ListProgLink[i].FSPROG_ID_NAME + ",衍生集別:" + m_ListProgLink[i].FNEPISODE + ",扣播出次數節目:" + m_ListProgLink[i].FSORGPROG_ID_NAME + ",扣播出次數集別:" + m_ListProgLink[i].FNORGEPISODE + ",錯誤訊息:" + ListMsg[i].strErrorMsg + "\n");                    
                }

                if (strMessage.ToString().Trim() != "")
                {
                    MessageBox.Show("批次新增衍生子集資料「" + tbxPROG_NAME.Text.ToString() + "-" + tbxEPISODE.Text.ToString() + "集」失敗！", "提示訊息", MessageBoxButton.OK);
                    MessageBox.Show(strMessage.ToString(), "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = true;
                }                    
                else
                {
                    MessageBox.Show("批次新增衍生子集資料「" + tbxPROG_NAME.Text.ToString() + "-" + tbxEPISODE.Text.ToString() + "集」成功！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = true;
                }
            }            
        }

        //實作-查詢扣播出次數節目子集資料(避免重複)
        void client_QUERY_TBPROGLINK_CHECKCompleted(object sender, QUERY_TBPROGLINK_CHECKCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    if (e.Result == "0")
                    {
                        //新增扣播出次數節目子集資料
                        client.INSERT_TBPROLINKAsync(FormToClass_PROGLINK());
                    }
                    else
                    {
                        MessageBox.Show("資料庫已有重複資料，請檢查後重新輸入！", "提示訊息", MessageBoxButton.OK);
                    }
                }
            }
        }

        #endregion

        #region 檢查畫面資料及搬值

        private Class_PROGLINK FormToClass_PROGLINK()
        {
            Class_PROGLINK obj = new Class_PROGLINK();

            obj.FSPROG_ID = tbxPROG_NAME.Tag.ToString().Trim();     //節目編碼           
            if (tbxEPISODE.Text.ToString().Trim() == "")            //集別
            {
                obj.FNEPISODE = 32767;
                m_strEpisode = "";
            }
            else
            {
                obj.FNEPISODE = Convert.ToInt16(tbxEPISODE.Text.ToString().Trim());
                m_strEpisode = tbxEPISODE.Text.ToString().Trim();       //暫存本次新增的集別，為了秀給使用者看結果用
            }

            obj.FSORGPROG_ID = "" ;
            obj.FNORGEPISODE = 32767;
            obj.FSMEMO = "";

            m_Queryobj = obj;

            return obj;
        }

        private Boolean Check_PROGLINKData()    //檢查畫面上的欄位是否都填妥
        {
            StringBuilder strMessage = new StringBuilder();

            if (m_ListProgLink.Count == 0)
                strMessage.AppendLine("請至少輸入一筆資料");

            if (strMessage.ToString().Trim() == "")
                return true;
            else
            {
                MessageBoxResult result = MessageBox.Show(strMessage.ToString(), "提示訊息", MessageBoxButton.OK);
                return false;
            }
        }

        #endregion   

        //確定
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (Check_PROGLINKData() == false)      //檢查畫面上的欄位是否都填妥
                return;

            m_Continue = false;

            FormToClass_PROGLINK(); //記錄新增的衍生節目及集別 

            if (tbxMEMO.Text.ToString().Trim() != "")
            {
                for (int i = 0; i < m_ListProgLink.Count; i++)
                {
                    m_ListProgLink[i].FSMEMO = tbxMEMO.Text.ToString().Trim();
                }
            }

            //批次新增衍生節目子集資料
            client.INSERT_TBPROLINK_BATCHAsync(m_ListProgLink);
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //開啟節目資料查詢畫面(衍生)
        private void btnPROG_Click(object sender, RoutedEventArgs e)
        {
            PROGDATA_VIEW PROGDATA_VIEW_frm = new PROGDATA_VIEW();
            PROGDATA_VIEW_frm.Show();

            PROGDATA_VIEW_frm.Closed += (s, args) =>
            {
                if (PROGDATA_VIEW_frm.DialogResult == true)
                {
                    tbxPROG_NAME.Text = PROGDATA_VIEW_frm.strProgName_View;
                    tbxPROG_NAME.Tag = PROGDATA_VIEW_frm.strProgID_View;

                    tbxEPISODE.Text = "";
                    lblEPISODE_NAME.Content = "";
                }
            };
        }

        //開啟節目子集資料查詢畫面(衍生)
        private void btnEPISODE_Click(object sender, RoutedEventArgs e)
        {
            if (tbxPROG_NAME.Text.ToString().Trim() != "" && tbxPROG_NAME.Tag.ToString().Trim() != "")
            {
                PRG.PRG200_07 PRG200_07_frm = new PRG.PRG200_07(tbxPROG_NAME.Tag.ToString().Trim(), tbxPROG_NAME.Text.ToString().Trim());
                PRG200_07_frm.Show();

                PRG200_07_frm.Closed += (s, args) =>
                {
                    if (PRG200_07_frm.DialogResult == true)
                    {
                        tbxEPISODE.Text = PRG200_07_frm.m_strEpisode_View;
                        lblEPISODE_NAME.Content = PRG200_07_frm.m_strProgDName_View;
                    }
                };
            }
            else
                MessageBox.Show("請先選擇節目！", "提示訊息", MessageBoxButton.OK);
        }

        //開啟節目資料查詢畫面(扣播出次數)
        private void btnORGPROG_Click(object sender, RoutedEventArgs e)
        {
            PROGDATA_VIEW PROGDATA_VIEW_frm = new PROGDATA_VIEW();
            PROGDATA_VIEW_frm.Show();

            PROGDATA_VIEW_frm.Closed += (s, args) =>
            {
                if (PROGDATA_VIEW_frm.DialogResult == true)
                {
                    tbxORGPROG_NAME.Text = PROGDATA_VIEW_frm.strProgName_View;
                    tbxORGPROG_NAME.Tag = PROGDATA_VIEW_frm.strProgID_View;
                }
            };
        }

        //開啟節目子集資料查詢畫面(扣播出次數)
        private void btnORGEPISODE_Click(object sender, RoutedEventArgs e)
        {
            if (tbxPROG_NAME.Text.Trim() == "")
            {
                MessageBox.Show("請選擇「衍生節目」", "提示訊息", MessageBoxButton.OK);
                return;
            }

            if (tbxEPISODE.Text.Trim() == "")
            {
                MessageBox.Show("請選擇「衍生子集」", "提示訊息", MessageBoxButton.OK);
                return;
            }
           

            if (tbxORGPROG_NAME.Text.ToString().Trim() != "" && tbxORGPROG_NAME.Tag.ToString().Trim() != "")
            {
                PRG.PRG200_07 PRG200_07_frm = new PRG.PRG200_07(tbxORGPROG_NAME.Tag.ToString().Trim(), tbxORGPROG_NAME.Text.ToString().Trim());
                PRG200_07_frm.Show();

                PRG200_07_frm.Closed += (s, args) =>
                {
                    if (PRG200_07_frm.DialogResult == true)
                    {                            
                         Class_PROGLINK obj = new Class_PROGLINK(); 
                         obj.FSPROG_ID = tbxPROG_NAME.Tag.ToString().Trim();                        //節目編碼
                         obj.FSPROG_ID_NAME = tbxPROG_NAME.Text.ToString().Trim();                  //節目名稱
                         obj.FNEPISODE = Convert.ToInt16(tbxEPISODE.Text.ToString().Trim());        //集別
                         obj.FSORGPROG_ID = tbxORGPROG_NAME.Tag.ToString().Trim();                  //扣播出次數節目編號
                         obj.FSORGPROG_ID_NAME = tbxORGPROG_NAME.Text.ToString().Trim();            //扣播出次數節目名稱
                         obj.FNORGEPISODE = Convert.ToInt16(PRG200_07_frm.m_strEpisode_View.ToString().Trim());  //扣播出次數集別   
                         obj.SHOW_ORGFNEPISODE = PRG200_07_frm.m_strEpisode_View.ToString().Trim();  //扣播出次數集別  
                         obj.SHOW_FNORGEPISODE_PLAY = PRG200_07_frm.m_strPlayEpisode_View.ToString().Trim();  //扣播出次數播出集別   
                         obj.FNORGEPISODE_NAME = PRG200_07_frm.m_strProgDName_View.ToString().Trim();
                         obj.FSMEMO = "";                                           ///備註欄位資料，最後按下送出時一併給
                         obj.FSCREATED_BY = UserClass.userData.FSUSER_ID.ToString();//建檔者
                         obj.FSUPDATED_BY = UserClass.userData.FSUSER_ID.ToString();//修改者

                         if (checkRepeat(obj)==true)
                             MessageBox.Show("選取的扣播出次數子集重複", "提示訊息", MessageBoxButton.OK);
                         else
                         {
                          btnPROG.IsEnabled = false;        //將衍生節目子集設為不可修改
                          btnEPISODE.IsEnabled = false;

                          m_ListProgLink.Add(obj);
                          DGPROG_LinkList.ItemsSource = null;
                          DGPROG_LinkList.ItemsSource = m_ListProgLink;

                          if (m_ListProgLink.Count > 0)
                              DGPROG_LinkList.SelectedIndex = 0;
                         }
                    }
                };
            }
            else
                MessageBox.Show("請先選擇扣播出次數節目！", "提示訊息", MessageBoxButton.OK);
        }

        //刪除
        private void btnDel_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGPROG_LinkList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預刪除的扣播出次數節目子集資料", "提示訊息", MessageBoxButton.OK);
                return;
            }
           
            m_ListProgLink.Remove((Class_PROGLINK)DGPROG_LinkList.SelectedItem);
            DGPROG_LinkList.ItemsSource = null;
            DGPROG_LinkList.ItemsSource = m_ListProgLink;

            if (m_ListProgLink.Count > 0)
                DGPROG_LinkList.SelectedIndex = 0;
        }

        //判斷新增的是否有重複
        private Boolean checkRepeat(Class_PROGLINK obj)    
        {
            for (int i = 0; i < m_ListProgLink.Count; i++)
            {
                if (m_ListProgLink[i].FSPROG_ID == obj.FSPROG_ID && m_ListProgLink[i].FNEPISODE == obj.FNEPISODE && m_ListProgLink[i].FSORGPROG_ID == obj.FSORGPROG_ID && m_ListProgLink[i].FNORGEPISODE == obj.FNORGEPISODE)
                    return true;
            }

            return false;
        }
       
    }
}

