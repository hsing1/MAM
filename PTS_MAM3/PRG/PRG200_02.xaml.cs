﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.PROG_M;
using PTS_MAM3.WSPROG_D;
using PTS_MAM3.WSPROG_M;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace PTS_MAM3.ProgData
{
    public partial class PROGDDATA_Edit : ChildWindow
    {
        WSPROG_DSoapClient client = new WSPROG_DSoapClient();      //產生新的代理類別
        WSPROG_MSoapClient clientPROGM = new WSPROG_MSoapClient();
        Class_PROGD m_FormData = new Class_PROGD();
        List<Class_CODE> m_CodeDataBuyD = new List<Class_CODE>();  //代碼檔集合(外購類別細項) 
        string m_strProgName = "";                                 //暫存節目名稱
        string m_strProgID = "";                                   //暫存節目編號
        string m_strEpisode = "";                                  //暫存集別
        string m_2ndMemo = "";                                     //2nd備註

        public PROGDDATA_Edit(Class_PROGD FormData)
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面
            m_FormData = FormData;
            ClassToForm();
            m_strProgName = FormData.FSPROG_ID_NAME.Trim();     //暫存節目名稱
            m_strProgID = FormData.FSPROG_ID.Trim();            //暫存節目編號
            m_strEpisode = FormData.SHOW_FNEPISODE.Trim();      //暫存集別

            tbxTAPELENGTH.Text = Convert.ToString(FormData.FNTAPELENGTH / 60); //實際帶長
            tbxTAPELENGTH_S.Text = Convert.ToString(FormData.FNTAPELENGTH % 60);//實際帶長
            checkPermission();                                  //檢查修改權限
        }

        void InitializeForm() //初始化本頁面
        {
            //下載代碼檔
            clientPROGM.fnGetTBPROG_M_CODECompleted += new EventHandler<fnGetTBPROG_M_CODECompletedEventArgs>(clientPROGM_fnGetTBPROG_M_CODECompleted);           
            clientPROGM.fnGetTBPROG_M_CODEAsync();

            //下載外購類別細項代碼檔
            clientPROGM.fnGetTBPROG_M_PROGBUYD_CODECompleted += new EventHandler<fnGetTBPROG_M_PROGBUYD_CODECompletedEventArgs>(client_fnGetTBPROG_M_PROGBUYD_CODECompleted);
            

            //修改節目子集資料
            client.UPDATE_TBPROGDCompleted += new EventHandler<UPDATE_TBPROGDCompletedEventArgs>(client_UPDATE_TBPROGDCompleted);

            //查詢節目子集是否有2ND
            client.Query_TBPROG_D_NDCompleted += new EventHandler<Query_TBPROG_D_NDCompletedEventArgs>(client_Query_TBPROG_D_NDCompleted);
        }
      
        private void ClassToForm()  //將前一頁點選的節目子資料繫集到畫面上
        {
            this.gdPROG_D.DataContext = m_FormData;
        }

        #region 實作

        //實作-修改節目子集資料
        void client_UPDATE_TBPROGDCompleted(object sender, UPDATE_TBPROGDCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result.bolResult == true)
                {
                    MessageBox.Show("修改節目子集資料「" + m_strProgName.ToString() + "-" + m_strEpisode + "集」成功！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("修改節目子集資料「" + m_strProgName.ToString() + "-" + m_strEpisode + "集」失敗！\n失敗原因：" + e.Result.strErrorMsg, "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
        }        

        //實作-下載外購類別細項代碼檔
        void client_fnGetTBPROG_M_PROGBUYD_CODECompleted(object sender, fnGetTBPROG_M_PROGBUYD_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {                  
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        ComboBoxItem cbiPROGBUYD = new ComboBoxItem();
                        cbiPROGBUYD.Content = CodeData.NAME;
                        cbiPROGBUYD.Tag = CodeData.ID;
                        cbiPROGBUYD.DataContext = CodeData.IDD;
                        this.cbPROGBUYDID.Items.Add(cbiPROGBUYD);   //先都產生在隱藏起來的細項代碼combobox裡                   
                        m_CodeDataBuyD.Add(CodeData);
                    }
                }

                CompareCode();

               // compareCode_FSPROGBUYDID(m_FormData.FSPROGBUYDID.ToString().Trim());

            }
        }

        void CompareCode() 
        {
            //比對代碼檔
            compareRACE(m_FormData.FCRACE.ToString().Trim());         //可排檔
            compareDel(m_FormData.FSDEL.ToString().Trim());           //刪除註記
            compareCode_FSPROGGRADEID(m_FormData.FSPROGGRADEID.ToString().Trim());
            compareCode_FSPROGATTRID(m_FormData.FSPROGATTRID.ToString().Trim());
            compareCode_FSPROGSRCID(m_FormData.FSPROGSRCID.ToString().Trim());
            compareCode_FSPROGTYPEID(m_FormData.FSPROGTYPEID.ToString().Trim());
            compareCode_FSPROGAUDID(m_FormData.FSPROGAUDID.ToString().Trim());
            compareCode_FSPROGLANGID1(m_FormData.FSPROGLANGID1.ToString().Trim());
            compareCode_FSPROGLANGID2(m_FormData.FSPROGLANGID2.ToString().Trim());
            compareCode_FSPROGBUYID(m_FormData.FSPROGBUYID.ToString().Trim());
            compareCode_FSPROGBUYDID(m_FormData.FSPROGBUYDID.ToString().Trim());
            compareCode_FSCHANNEL_ID(m_FormData.FSCHANNEL_ID.ToString().Trim());
            compareCode_FSSHOOTSPEC(CheckList(m_FormData.FSSHOOTSPEC.ToString().Trim()));
            compareCode_FSPROGSALEID(m_FormData.FSPROGSALEID.ToString().Trim());
            compareCode_FSPROGSPEC(CheckList(m_FormData.FSPROGSPEC.ToString().Trim()));
            compareCode_CR_TYPE(m_FormData.FSCR_TYPE.ToString().Trim());
            cbNATION_ID.SelectedItem = cbNATION_ID.Items.Where(S => ((ComboBoxItem)S).Tag.ToString() == m_FormData.FSPROGNATIONID).FirstOrDefault();
            //if (m_FormData.FDEXPIRE_DATE.ToShortDateString() != "1900/1/1")
            if (m_FormData.FDEXPIRE_DATE > Convert.ToDateTime("1900/1/5"))
            {
                dpEXPIRE.Text = m_FormData.FDEXPIRE_DATE.ToShortDateString();
            }
            if (m_FormData.FSEXPIRE_DATE_ACTION == "Y")
                cbDELETE.IsChecked = true;
            else
                cbDELETE.IsChecked = false;
        }
        //實作-下載所有的代碼檔
        void clientPROGM_fnGetTBPROG_M_CODECompleted(object sender, fnGetTBPROG_M_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    //外購類別代碼，空白要自己帶入
                    ComboBoxItem cbiPROGBUYNull = new ComboBoxItem();
                    cbiPROGBUYNull.Content = " ";
                    cbiPROGBUYNull.Tag = "";
                    this.cbPROGBUYID.Items.Add(cbiPROGBUYNull);                    

                    //因為行銷類別是節目管理系統的資料庫因此空白要自己帶入
                    ComboBoxItem cbiPROGSALENull = new ComboBoxItem();
                    cbiPROGSALENull.Content = " ";
                    cbiPROGSALENull.Tag = "";
                    this.cbPROGSALEID.Items.Add(cbiPROGSALENull);
                    
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            case "TBZCHANNEL":       //頻道別代碼
                                ComboBoxItem cbiCHANNEL = new ComboBoxItem();
                                cbiCHANNEL.Content = CodeData.NAME;
                                cbiCHANNEL.Tag = CodeData.ID;
                                this.cbCHANNEL_ID.Items.Add(cbiCHANNEL);
                                break;
                            case "TBZSHOOTSPEC":       //拍攝規格代碼
                                CheckBox cbSHOOTSPEC = new CheckBox();
                                cbSHOOTSPEC.Content = CodeData.NAME;
                                cbSHOOTSPEC.Tag = CodeData.ID;
                                listSHOOTSPEC.Items.Add(cbSHOOTSPEC);
                                break;
                            case "TBZPROGGRADE":      //節目分級代碼
                                ComboBoxItem cbiPROGGRADE = new ComboBoxItem();
                                cbiPROGGRADE.Content = CodeData.NAME;
                                cbiPROGGRADE.Tag = CodeData.ID;
                                this.cbPROGGRADEID.Items.Add(cbiPROGGRADE);
                                break;
                            case "TBZPROGSRC":      //節目來源代碼
                                ComboBoxItem cbiPROGSRC = new ComboBoxItem();
                                cbiPROGSRC.Content = CodeData.NAME;
                                cbiPROGSRC.Tag = CodeData.ID;
                                this.cbPROGSRCID.Items.Add(cbiPROGSRC);
                                break;
                            case "TBZPROGATTR":      //內容屬性代碼
                                ComboBoxItem cbiPROGATTR = new ComboBoxItem();
                                cbiPROGATTR.Content = CodeData.NAME;
                                cbiPROGATTR.Tag = CodeData.ID;
                                this.cbPROGATTRID.Items.Add(cbiPROGATTR);
                                break;
                            case "TBZPROGAUD":      //目標觀眾代碼
                                ComboBoxItem cbiPROGAUD = new ComboBoxItem();
                                cbiPROGAUD.Content = CodeData.NAME;
                                cbiPROGAUD.Tag = CodeData.ID;
                                this.cbPROGAUDID.Items.Add(cbiPROGAUD);
                                break;
                            case "TBZPROGTYPE":      //表現方式代碼
                                ComboBoxItem cbiPROGTYPE = new ComboBoxItem();
                                cbiPROGTYPE.Content = CodeData.NAME;
                                cbiPROGTYPE.Tag = CodeData.ID;
                                this.cbPROGTYPEID.Items.Add(cbiPROGTYPE);
                                break;
                            case "TBZPROGLANG":      //語言代碼
                                ComboBoxItem cbiPROGLANG1 = new ComboBoxItem();
                                cbiPROGLANG1.Content = CodeData.NAME;
                                cbiPROGLANG1.Tag = CodeData.ID;
                                this.cbPROGLANGID1.Items.Add(cbiPROGLANG1);  //主聲道

                                ComboBoxItem cbiPROGLANG2 = new ComboBoxItem();
                                cbiPROGLANG2.Content = CodeData.NAME;
                                cbiPROGLANG2.Tag = CodeData.ID;
                                this.cbPROGLANGID2.Items.Add(cbiPROGLANG2);  //副聲道
                                break;
                            case "TBZPROGBUY":       //外購類別代碼
                                ComboBoxItem cbiPROGBUY = new ComboBoxItem();
                                cbiPROGBUY.Content = CodeData.NAME;
                                cbiPROGBUY.Tag = CodeData.ID;
                                this.cbPROGBUYID.Items.Add(cbiPROGBUY);
                                break;
                            case "TBZPROGSALE":       //行銷類別代碼
                                ComboBoxItem cbiPROGSALE = new ComboBoxItem();
                                cbiPROGSALE.Content = CodeData.NAME;
                                cbiPROGSALE.Tag = CodeData.ID;
                                this.cbPROGSALEID.Items.Add(cbiPROGSALE);
                                break;
                            case "TBZPROGSPEC":       //節目規格代碼
                                CheckBox cbPROGSPEC = new CheckBox();
                                cbPROGSPEC.Content = CodeData.NAME;
                                cbPROGSPEC.Tag = CodeData.ID;
                                listPROGSPEC.Items.Add(cbPROGSPEC);
                                break;
                            case "TBZPROGNATION":       //來源國家    2012/11/22 kyle                            
                                ComboBoxItem cbNATION = new ComboBoxItem();
                                cbNATION.Content = CodeData.NAME;
                                cbNATION.Tag = CodeData.ID;
                                cbNATION_ID.Items.Add(cbNATION);
                                break;
                            default:
                                break;
                        }
                    }
                    clientPROGM.fnGetTBPROG_M_PROGBUYD_CODEAsync();
                    
                }
            }
        }

        //實作-查詢節目子集是否有2ND
        void client_Query_TBPROG_D_NDCompleted(object sender, Query_TBPROG_D_NDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    call2nd(e.Result.Trim());    //呼叫2ND
                }
            }
        }
              
        private string[] CheckList(string strChannel)
        {
            char[] delimiterChars = { ';' };
            string[] words = strChannel.Split(delimiterChars);
            return words;
        }

        private void compareRACE(string strRACE)   //是否為可排檔
        {
            if (strRACE == "N")
                 cbRACE.SelectedIndex = 0;
            else if (strRACE == "Y")
                cbRACE.SelectedIndex = 1;
        }

        private void compareDel(string strDel)   //是否為刪除
        {
            if (strDel == "N")
                cbDel.SelectedIndex = 0;
            else if (strDel == "Y")
                cbDel.SelectedIndex = 1;
        }

        //選擇外購類別，要判斷顯示哪些外購類別細項
        private void cbBUY_ID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.cbPROGBUYDIDS.Items.Clear();     //使用前先清空
            string strBUYID = ""; //外購類別大項
            ComboBox cb = sender as ComboBox;
            ComboBoxItem cbm = (ComboBoxItem)cb.SelectedItem;
            strBUYID = cbm.Tag.ToString();

            for (int i = 0; i < cbPROGBUYDID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGBUYDID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strBUYID))
                {
                    ComboBoxItem cbiPROGBUYDS = new ComboBoxItem();
                    cbiPROGBUYDS.Content = getcbi.Content;
                    cbiPROGBUYDS.Tag = getcbi.DataContext;
                    this.cbPROGBUYDIDS.Items.Add(cbiPROGBUYDS);
                }
            }
        }

        #endregion

        #region 比對代碼檔
        
        //比對代碼檔_節目型態(內容屬性)
        void compareCode_FSPROGATTRID(string strID)
        {
            for (int i = 0; i < cbPROGATTRID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGATTRID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGATTRID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_節目分級
        void compareCode_FSPROGGRADEID(string strID)
        {
            for (int i = 0; i < cbPROGGRADEID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGGRADEID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGGRADEID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_節目來源
        void compareCode_FSPROGSRCID(string strID)
        {
            for (int i = 0; i < cbPROGSRCID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGSRCID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGSRCID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_表現方式
        void compareCode_FSPROGTYPEID(string strID)
        {
            for (int i = 0; i < cbPROGTYPEID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGTYPEID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGTYPEID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_目標觀眾
        void compareCode_FSPROGAUDID(string strID)
        {
            for (int i = 0; i < cbPROGAUDID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGAUDID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGAUDID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_主聲道
        void compareCode_FSPROGLANGID1(string strID)
        {
            for (int i = 0; i < cbPROGLANGID1.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGLANGID1.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGLANGID1.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_副聲道
        void compareCode_FSPROGLANGID2(string strID)
        {
            for (int i = 0; i < cbPROGLANGID2.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGLANGID2.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGLANGID2.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_外購類別大類
        void compareCode_FSPROGBUYID(string strID)
        {
            for (int i = 0; i < cbPROGBUYID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGBUYID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGBUYID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_外購類別細類
        void compareCode_FSPROGBUYDID(string strID)
        {
            for (int i = 0; i < cbPROGBUYDIDS.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGBUYDIDS.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGBUYDIDS.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_拍攝規格
        void compareCode_FSSHOOTSPEC(string[] ListTEST_Send)
        {
            for (int i = 0; i < ListTEST_Send.Length - 1; i++)
            {
                for (int j = 0; j < listSHOOTSPEC.Items.Count; j++)
                {
                    CheckBox getcki = (CheckBox)listSHOOTSPEC.Items.ElementAt(j);

                    if (getcki.Tag.ToString().Trim().Equals(ListTEST_Send[i]))
                    {
                        getcki.IsChecked = true;
                        break;
                    }
                }
            }
        }

        //比對代碼檔_節目規格
        void compareCode_FSPROGSPEC(string[] ListTEST_Send)
        {
            for (int i = 0; i < ListTEST_Send.Length - 1; i++)
            {
                for (int j = 0; j < listPROGSPEC.Items.Count; j++)
                {
                    CheckBox getcki = (CheckBox)listPROGSPEC.Items.ElementAt(j);

                    if (getcki.Tag.ToString().Trim().Equals(ListTEST_Send[i]))
                    {
                        getcki.IsChecked = true;
                        break;
                    }
                }
            }
        }

        //比對代碼檔_頻道別
        void compareCode_FSCHANNEL_ID(string strID)
        {
            for (int i = 0; i < cbCHANNEL_ID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbCHANNEL_ID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbCHANNEL_ID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_行銷類別
        void compareCode_FSPROGSALEID(string strID)
        {
            for (int i = 0; i < cbPROGSALEID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGSALEID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGSALEID.SelectedIndex = i;
                    break;
                }
            }
        }

        //選擇外購類別，要判斷顯示哪些外購類別細項
        private void cbPROGBUYID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.cbPROGBUYDIDS.Items.Clear();     //使用前先清空
            string strBUYID = ""; //外購類別大項
            ComboBox cb = sender as ComboBox;
            ComboBoxItem cbm = (ComboBoxItem)cb.SelectedItem;
            strBUYID = cbm.Tag.ToString();

            for (int i = 0; i < cbPROGBUYDID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGBUYDID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strBUYID))
                {
                    ComboBoxItem cbiPROGBUYDS = new ComboBoxItem();
                    cbiPROGBUYDS.Content = getcbi.Content;
                    cbiPROGBUYDS.Tag = getcbi.DataContext;
                    this.cbPROGBUYDIDS.Items.Add(cbiPROGBUYDS);
                }
            }
        }


        //比對代碼檔_著作權類型
        void compareCode_CR_TYPE(string strID)
        {
            for (int i = 0; i < cbCR_TYPE.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbCR_TYPE.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbCR_TYPE.SelectedIndex = i;
                    break;
                }
            }
        }

        #endregion

        #region 檢查畫面資料及搬值

        private Class_PROGD FormToClass_PROGD()
        {
            Class_PROGD obj = new Class_PROGD();

            obj.FSPROG_ID = tbxtProgID.Text.ToString();                                 //節目編號
            obj.FNEPISODE = Convert.ToInt16(tbxEPISODE.Text.ToString().Trim());         //集別

            if (tbxSHOWEPISODE.Text.ToString().Trim() == "")                            //播出集別            
                obj.FNSHOWEPISODE = 0;   
            else
                obj.FNSHOWEPISODE = Convert.ToInt16(tbxSHOWEPISODE.Text.ToString().Trim());           
                      
            obj.FSPGDNAME = tbxPGDNAME.Text.ToString().Trim();                          //子集名稱
            obj.FSPGDENAME = tbxPGDENAME.Text.ToString().Trim();                        //子集外文名稱

            if (tbxLENGTH.Text.ToString().Trim() == "")                                 //時長            
                obj.FNLENGTH = 0;
            else
                obj.FNLENGTH = Convert.ToInt16(tbxLENGTH.Text.ToString().Trim());

            if (tbxTAPELENGTH.Text.ToString().Trim() == "" && tbxTAPELENGTH_S.Text.ToString().Trim() == "")//實際帶長           
                obj.FNTAPELENGTH = 0;
            else if (tbxTAPELENGTH.Text.ToString().Trim() != "" && tbxTAPELENGTH_S.Text.ToString().Trim() == "")
                obj.FNTAPELENGTH = Convert.ToInt32(tbxTAPELENGTH.Text.ToString().Trim()) * 60;
            else if (tbxTAPELENGTH.Text.ToString().Trim() == "" && tbxTAPELENGTH_S.Text.ToString().Trim() != "")
                obj.FNTAPELENGTH = Convert.ToInt32(tbxTAPELENGTH_S.Text.ToString().Trim());
            else if (tbxTAPELENGTH.Text.ToString().Trim() != "" && tbxTAPELENGTH_S.Text.ToString().Trim() != "")
                obj.FNTAPELENGTH = Convert.ToInt32(tbxTAPELENGTH.Text.ToString().Trim()) * 60 + Convert.ToInt32(tbxTAPELENGTH_S.Text.ToString().Trim());

            if (cbCHANNEL_ID.SelectedIndex != -1)                                       //頻道別
                obj.FSCHANNEL_ID = ((ComboBoxItem)cbCHANNEL_ID.SelectedItem).Tag.ToString();
            else
                obj.FSCHANNEL_ID = "";

            if (cbRACE.SelectedIndex != -1)                                             //可排檔
                obj.FCRACE = ((ComboBoxItem)cbRACE.SelectedItem).Tag.ToString();
            else
                obj.FCRACE = "";

            obj.FSPRDYEAR = tbxPRDYEAR.Text.ToString().Trim();                          //完成年度
            obj.FSSHOOTSPEC = Check_SHOOTSPECList();                                    //拍攝規格
            obj.FSPROGGRADE = tbxPROGGRADE.Text.ToString().Trim();                      //非正常分級的說明
            obj.FSCONTENT = tbxCONTENT.Text.ToString().Trim();                          //內容簡述
            obj.FSMEMO = tbxMEMO.Text.ToString().Trim();                                //備註
            obj.FSNDMEMO = m_2ndMemo.Trim();                                            //2nd備註

            if (cbPROGGRADEID.SelectedIndex != -1)                //節目分級
                obj.FSPROGGRADEID = ((ComboBoxItem)cbPROGGRADEID.SelectedItem).Tag.ToString();
            else
                obj.FSPROGGRADEID = "";

            if (cbPROGSRCID.SelectedIndex != -1)                 //節目來源
                obj.FSPROGSRCID = ((ComboBoxItem)cbPROGSRCID.SelectedItem).Tag.ToString();
            else
                obj.FSPROGSRCID = "" ;

            if (cbPROGATTRID.SelectedIndex != -1)                //節目型態
                obj.FSPROGATTRID = ((ComboBoxItem)cbPROGATTRID.SelectedItem).Tag.ToString();
            else
                obj.FSPROGATTRID = "" ;

            if (cbPROGAUDID.SelectedIndex != -1)                 //目標觀眾
                obj.FSPROGAUDID = ((ComboBoxItem)cbPROGAUDID.SelectedItem).Tag.ToString();
            else
                obj.FSPROGAUDID = "";

            if (cbPROGTYPEID.SelectedIndex != -1)                //表現方式
                obj.FSPROGTYPEID = ((ComboBoxItem)cbPROGTYPEID.SelectedItem).Tag.ToString();
            else
                obj.FSPROGTYPEID = "" ;

            if (cbPROGLANGID1.SelectedIndex != -1)               //主聲道
                obj.FSPROGLANGID1 = ((ComboBoxItem)cbPROGLANGID1.SelectedItem).Tag.ToString();
            else
                obj.FSPROGLANGID1 = "";

            if (cbPROGLANGID2.SelectedIndex != -1)               //副聲道
                obj.FSPROGLANGID2 = ((ComboBoxItem)cbPROGLANGID2.SelectedItem).Tag.ToString();
            else
                obj.FSPROGLANGID2 = "";

            if (cbPROGBUYID.SelectedIndex != -1)                 //外購類別大類
                obj.FSPROGBUYID = ((ComboBoxItem)cbPROGBUYID.SelectedItem).Tag.ToString();
            else
                obj.FSPROGBUYID = "";

            if (cbPROGBUYDIDS.SelectedIndex != -1)               //外購類別細類
                obj.FSPROGBUYDID = ((ComboBoxItem)cbPROGBUYDIDS.SelectedItem).Tag.ToString();
            else
                obj.FSPROGBUYDID = "" ;

            if (cbPROGSALEID.SelectedIndex != -1)                //行銷類別
                obj.FSPROGSALEID = ((ComboBoxItem)cbPROGSALEID.SelectedItem).Tag.ToString();          
            else
                obj.FSPROGSALEID = "";

            if (cbDel.SelectedIndex == 1)
            {
                obj.FSDEL = "Y";         //刪除註記
                obj.FSDELUSER = UserClass.userData.FSUSER_ID.ToString();    //刪除者
            }
            else
            {
                obj.FSDEL = "N";        //刪除註記
                obj.FSDELUSER = "";     //刪除者
            }

            if (cbCR_TYPE.SelectedIndex != -1)                //著作權類型
                obj.FSCR_TYPE = ((ComboBoxItem)cbCR_TYPE.SelectedItem).Tag.ToString();
            else
                obj.FSCR_TYPE = "";

            obj.FSCR_NOTE = tbxCR_NOTE.Text.ToString().Trim();          //著作權備註

            obj.FSPROGSPEC = Check_PROGSPECList();                      //節目規格   
            obj.FSUPDUSER = UserClass.userData.FSUSER_ID.ToString();    //修改者

            if (cbDELETE.IsChecked == false)                          //到期日期後是否刪除
                obj.FSEXPIRE_DATE_ACTION = "N";
            else
                obj.FSEXPIRE_DATE_ACTION = "Y";


            if (dpEXPIRE.Text == "")
                obj.FDEXPIRE_DATE = Convert.ToDateTime("1900/1/1");
            else
                obj.FDEXPIRE_DATE = Convert.ToDateTime(dpEXPIRE.Text).AddHours(23).AddMinutes(59);  //到期日期

            obj.FSPROGNATIONID = ((ComboBoxItem)cbNATION_ID.SelectedItem).Tag.ToString();       //國家代碼
            obj.Origin_FDEXPIRE_DATE = m_FormData.Origin_FDEXPIRE_DATE;
            obj.Origin_FSEXPIRE_DATE_ACTION = m_FormData.Origin_FSEXPIRE_DATE_ACTION;

            return obj;
        }

        private string Check_SHOOTSPECList()    //檢查拍攝規格有哪些
        {
            string strReturn = "";            
            for (int i = 0; i < listSHOOTSPEC.Items.Count; i++)
            {
                CheckBox getckb = (CheckBox)listSHOOTSPEC.Items.ElementAt(i);
                if (getckb.IsChecked == true)
                {
                    strReturn += getckb.Tag.ToString().Trim() + ";";
                }
            }
            return strReturn;
        }

        private string Check_PROGSPECList()    //檢查節目規格有哪些
        {
            string strReturn = "";            
            for (int i = 0; i < listPROGSPEC.Items.Count; i++)
            {
                CheckBox getckb = (CheckBox)listPROGSPEC.Items.ElementAt(i);
                if (getckb.IsChecked == true)
                {
                    strReturn += getckb.Tag.ToString().Trim() + ";";
                }
            }
            return strReturn;
        }

        private Boolean Check_PROGDData()    //檢查畫面上的欄位是否都填妥
        {
            StringBuilder strMessage = new StringBuilder();
            short shortCheck;   //純粹為了判斷是否為數字型態之用
            double intCheck;    //純粹為了判斷是否為數字型態之用

            if (cbCHANNEL_ID.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「頻道」欄位");    

            if (tbxPGDNAME.Text.Trim() == "")
                strMessage.AppendLine("請輸入「子集名稱」欄位");

            if (tbxSHOWEPISODE.Text.Trim() != "")
            {
                if (short.TryParse(tbxSHOWEPISODE.Text.Trim(), out shortCheck) == false)
                    strMessage.AppendLine("請檢查「播出集別」欄位必須為數值型態");
            }

            if (tbxLENGTH.Text.Trim() != "")
            {
                if (short.TryParse(tbxLENGTH.Text.Trim(), out shortCheck) == false)
                    strMessage.AppendLine("請檢查「時長」欄位必須為數值型態");
            }
            
            if (tbxPRDYEAR.Text.Trim() != "")
            {
                if (short.TryParse(tbxPRDYEAR.Text.Trim(), out shortCheck) == false)
                    strMessage.AppendLine("請檢查「完成年度」欄位必須為數值型態");
                else
                {
                    if (tbxPRDYEAR.Text.Trim().Length != 4)
                        strMessage.AppendLine("請檢查「完成年度」欄位必須為四碼的西元年\n" + "範例：2011(2011年)");
                }
            }

            //先不要求為必填
            //if (Check_SHOOTSPECList() == "")
            //{
            //    strMessage.AppendLine("請勾選「拍攝規格」欄位");
            //}

            //除了節目分級為必填以外，若子集的節目分級選是非普遍級，則要求使用者填寫「非正常分級說明」欄位，不填要提醒
            if (cbPROGGRADEID.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「節目分級」欄位");
            else
            {
                 if (((ComboBoxItem)cbPROGGRADEID.SelectedItem).Content.ToString().IndexOf("普") != 0 && tbxPROGGRADE.Text.Trim() == "")
                     strMessage.AppendLine("節目分級非普級，請輸入「非正常分級的說明」欄位");
            }

            if (cbPROGSRCID.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「節目來源代碼」欄位");

            if (cbPROGATTRID.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「內容屬性代碼」欄位");

            if (cbPROGAUDID.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「目標觀眾代碼」欄位");

            if (cbPROGTYPEID.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「表現方式代碼」欄位");

            if (cbPROGLANGID1.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「主聲道代碼」欄位");

            if (cbPROGLANGID2.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「副聲道代碼」欄位");

            if (Check_PROGSPECList().Trim() == "")
                strMessage.AppendLine("請勾選「節目規格」欄位");

            if (cbPROGBUYID.SelectedIndex != -1 && ((ComboBoxItem)(cbPROGBUYID.SelectedItem)).Content.ToString().Trim() != "" && cbPROGBUYDIDS.SelectedIndex == -1) //因為設定外鍵，因此選擇了大項就要設細項
                strMessage.AppendLine("請輸入「外購類別細項」欄位");

            if (tbxTAPELENGTH.Text.Trim() != "")
            {
                if (double.TryParse(tbxTAPELENGTH.Text.Trim(), out intCheck) == false)
                    strMessage.AppendLine("請檢查「實際帶長-分」欄位必須為數值型態");
            }

            if (tbxTAPELENGTH_S.Text.Trim() != "")
            {
                if (double.TryParse(tbxTAPELENGTH_S.Text.Trim(), out intCheck) == false)
                    strMessage.AppendLine("請檢查「實際帶長-秒」欄位必須為數值型態");
                else
                {
                    if (intCheck>=60)
                        strMessage.AppendLine("請檢查「實際帶長-秒」欄位必須小於60");
                }
            }

            if (cbNATION_ID.SelectedIndex == -1)
            {
                strMessage.AppendLine("請補入「來源國家」欄位");
            }

            if (strMessage.ToString().Trim() == "")
            {
                if (cbDel.SelectedIndex == 1)
                {
                    MessageBoxResult resultMsg = MessageBox.Show("確定要刪除節目子集資料「" + m_strProgName.ToString() + "-" + m_strEpisode + "集」資料嗎?", "提示訊息", MessageBoxButton.OKCancel);
                    if (resultMsg == MessageBoxResult.Cancel)
                        return false;
                    else
                        return true;
                }
                else
                    return true;
            }
            else
            {
                MessageBoxResult result = MessageBox.Show(strMessage.ToString(), "提示訊息", MessageBoxButton.OK);
                return false;
            }
        }

        #endregion
        
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (Check_PROGDData() == false)      //檢查畫面上的欄位是否都填妥
                return;
           
            //修改節目子集資料
            client.UPDATE_TBPROGDAsync(FormToClass_PROGD());
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //設定 Secondary Event
        private void btnEvent_Click(object sender, RoutedEventArgs e)
        {
            if (m_2ndMemo.Trim() == "")
                client.Query_TBPROG_D_NDAsync(m_strProgID, m_strEpisode);   //先查詢出該子集是否有2nd資料，若有要帶入修改
            else
                call2nd(m_2ndMemo.Trim());           //呼叫2ND 
        }

        //呼叫2ND
        private void call2nd(string str2nd)
        {
            if (m_FormData.FSCHANNEL_ID.Trim() == "")
            {
                MessageBox.Show("請先設定頻道別資料，才能設定Secondary Event！", "提示訊息", MessageBoxButton.OK);
                return;
            }      

            PGM.PGM100_02 SetLouthKey_Frm = new PGM.PGM100_02();

            //修改時傳入組好的備註字串
            SetLouthKey_Frm.InLouthKeyString = str2nd;
            SetLouthKey_Frm.WorkChannelID = m_FormData.FSCHANNEL_ID.Trim();

            SetLouthKey_Frm.Show();
            SetLouthKey_Frm.Closed += (s, args) =>
            {
                if (SetLouthKey_Frm.OutLouthKeyString != null)
                {
                    //傳回選擇好的備註字串                  
                    m_2ndMemo = SetLouthKey_Frm.OutLouthKeyString.Trim();
                }
            };
        }

        //設定主控播出提示資料
        private void btnSet_Click(object sender, RoutedEventArgs e)
        {
            PTS_MAM3.PGM.PGM100_05 PGM100_05_Frm = new PTS_MAM3.PGM.PGM100_05();

            PGM100_05_Frm.textBoxProgID.Text = m_FormData.FSPROG_ID;
            PGM100_05_Frm.textBoxProgName.Text = m_FormData.FSPROG_ID_NAME.Trim();
            PGM100_05_Frm.textBoxEpisode.Text = m_FormData.SHOW_FNEPISODE;
            PGM100_05_Frm.Show();
            PGM100_05_Frm.Closed += (s, args) =>
            {
            };
        }
      
        //修改子集權限再分成六大屬性、外購、行銷
        private void checkPermission()
        {
            if (ModuleClass.getModulePermissionByName("02", "節目子集-四大屬性維護") == false)
            {
                cbPROGSRCID.IsEnabled = false;
                cbPROGATTRID.IsEnabled = false;
                cbPROGAUDID.IsEnabled = false;
                cbPROGTYPEID.IsEnabled = false;
            }

            if (ModuleClass.getModulePermissionByName("02", "節目子集-外購類別維護") == false)
            {
                cbPROGBUYID.IsEnabled = false;
                cbPROGBUYDIDS.IsEnabled = false;
            }

            if (ModuleClass.getModulePermissionByName("02", "節目子集-行銷類別維護") == false)
            {
                cbPROGSALEID.IsEnabled = false;
            }
        }

    }
}

