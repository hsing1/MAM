﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Collections.ObjectModel;
using PTS_MAM3.WSPROMO;
using PTS_MAM3.PRG;
using PTS_MAM3.ProgData;
using ExpressionMediaPlayer;
using System.ComponentModel;
using System.Text;



namespace PTS_MAM3
{
    public partial class PRG900 : ChildWindow, INotifyPropertyChanged
    {
        #region Property

        #region Code
        List<Class_CODE> channel_Code = new List<Class_CODE>();
        public List<Class_CODE> Channel_Code
        {
            get { return channel_Code; }
            set { channel_Code = value; }
        }

        List<Class_CODE> type_Code = new List<Class_CODE>();
        public List<Class_CODE> Type_Code
        {
            get { return type_Code; }
            set { type_Code = value; }
        }


        List<Class_CODE> cat_Code = new List<Class_CODE>();
        public List<Class_CODE> Cat_Code
        {
            get { return cat_Code; }
            set { cat_Code = value; }
        }

        List<Class_CODE> cat_Detail_Code = new List<Class_CODE>();
        public List<Class_CODE> Cat_Detail_Code
        {
            get { return cat_Detail_Code; }
            set { cat_Detail_Code = value; }
        }

        List<Class_CODE> version_Code = new List<Class_CODE>();
        public List<Class_CODE> Version_Code
        {
            get { return version_Code; }
            set { version_Code = value; }
        }

        List<Class_CODE> purpose_Code = new List<Class_CODE>();
        public List<Class_CODE> Purpose_Code
        {
            get { return purpose_Code; }
            set { purpose_Code = value; }
        }
        #endregion

        #region Data

        ObservableCollection<Promo_EFFECTIVE_ZONE> promo_Zone_List = new ObservableCollection<Promo_EFFECTIVE_ZONE>();
        public ObservableCollection<Promo_EFFECTIVE_ZONE> Promo_Zone_List
        {
            get { return promo_Zone_List; }
            set { promo_Zone_List = value; }
        }

        public string Promo_Name { get; set; }
        public string Promo_ENG_Name { get; set; }
        public string Promo_Length { get; set; }

        public string Promo_Type { get; set; }
        public string Promo_Cat { get; set; }
        public string Promo_Cat_Detail { get; set; }
        public string Promo_Charity { get; set; }
        public string Promo_Version { get; set; }
        public string Promo_Activity { get; set; }
        public string Promo_Prog_ID { get; set; }
        public string Promo_Prog_Episode { get; set; }
        public string Promo_Create_YYYYMM { get; set; }
        public string Promo_Create_Purpose { get; set; }
        public string Promo_Create_Unit { get; set; }
        public string Promo_ExpireDate { get; set; }
        public bool Promo_ExpireDelete { get; set; }
        public string Promo_Memo { get; set; }



        public string Broadcast_Arc_Type { get; set; }
        public string Broadcast_BeginTimeCode { get; set; }
        public string Broadcast_EndTimeCode { get; set; }
        public string Broadcast_Memo { get; set; }

        string newFileNO = "";
        string newVideoID = "";
        string newDirID = "";

        #endregion

        #region Object

        public Class_PROMO Promo = new Class_PROMO();
        public WSBROADCAST.Class_BROADCAST Broadcast = new WSBROADCAST.Class_BROADCAST();

        #endregion
        #endregion
        WSPROMOSoapClient promoClient = new WSPROMOSoapClient();
        WSBROADCAST.WSBROADCASTSoapClient broadcastClient = new WSBROADCAST.WSBROADCASTSoapClient();
        PTS_MAM3.WSMAMFunctions.WSMAMFunctionsSoapClient MAM_Client = new PTS_MAM3.WSMAMFunctions.WSMAMFunctionsSoapClient();

        public Class_PROMO newPromo;

        public PRG900()
        {
            InitializeComponent();

            this.Loaded += new RoutedEventHandler(PRG900_Loaded);

            this.DataContext = this;
        }

        void PRG900_Loaded(object sender, RoutedEventArgs e)
        {

            //下載代碼檔
            promoClient.GetTBPGM_PROMO_CODECompleted += new EventHandler<GetTBPGM_PROMO_CODECompletedEventArgs>(promoClient_GetTBPGM_PROMO_CODECompleted);

            //下載短帶類型細項代碼檔
            promoClient.GetTBPGM_PROMO_CATD_CODECompleted += new EventHandler<GetTBPGM_PROMO_CATD_CODECompletedEventArgs>(promoClient_GetTBPGM_PROMO_CATD_CODECompleted);

            promoClient.QUERY_TBPROMO_CHECKCompleted += new EventHandler<QUERY_TBPROMO_CHECKCompletedEventArgs>(promoClient_QUERY_TBPROMO_CHECKCompleted);

            promoClient.GetTBPGM_PROMO_BYPROMONAMECompleted += new EventHandler<GetTBPGM_PROMO_BYPROMONAMECompletedEventArgs>(promoClient_GetTBPGM_PROMO_BYPROMONAMECompleted);

            promoClient.INSERT_TBPGM_PROMOCompleted += new EventHandler<INSERT_TBPGM_PROMOCompletedEventArgs>(promoClient_INSERT_TBPGM_PROMOCompleted);

            //取單號
            MAM_Client.GetNoRecordCompleted += new EventHandler<WSMAMFunctions.GetNoRecordCompletedEventArgs>(MAM_Client_GetNoRecordCompleted);

            broadcastClient.fnGetFileIDCompleted += new EventHandler<WSBROADCAST.fnGetFileIDCompletedEventArgs>(broadcastClient_fnGetFileIDCompleted);

            broadcastClient.QueryVideoID_PROG_STTCompleted += new EventHandler<WSBROADCAST.QueryVideoID_PROG_STTCompletedEventArgs>(broadcastClient_QueryVideoID_PROG_STTCompleted);
            //一次性新增送播單
            broadcastClient.INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLISTCompleted += new EventHandler<WSBROADCAST.INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLISTCompletedEventArgs>(broadcastClient_INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLISTCompleted);

            BusyMsg.IsBusy = true;
            promoClient.GetTBPGM_PROMO_CODEAsync();

        }


        void broadcastClient_QueryVideoID_PROG_STTCompleted(object sender, WSBROADCAST.QueryVideoID_PROG_STTCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result.Trim() != "")
                {
                    newVideoID = e.Result.Trim();

                    PTS_MAM3.WSBROADCAST.Class_LOG_VIDEO Log_Video = new WSBROADCAST.Class_LOG_VIDEO();
                    Log_Video.FSFILE_NO = newFileNO;
                    Log_Video.FSVIDEO_PROG = newVideoID;
                    Log_Video.FCFILE_STATUS = "B";
                    Log_Video.FSBRO_ID = Broadcast.FSBRO_ID;
                    Log_Video.FSFILE_TYPE_HV = "mxf";
                    Log_Video.FSFILE_TYPE_LV = Broadcast_Arc_Type == "001" ? "wmv" : "mp4";
                    Log_Video.FSSUBJECT_ID = "P" + Promo.FSPROMO_ID;
                    Log_Video.FSSUPERVISOR = "N";
                    Log_Video.FSPLAY = "N";
                    Log_Video.FSCHANNEL_ID = Promo.FSMAIN_CHANNEL_ID;
                    Log_Video.FNDIR_ID = Convert.ToInt64(newDirID);
                    Log_Video.FSARC_TYPE = Broadcast_Arc_Type;
                    Log_Video.FSID = Promo.FSPROMO_ID;
                    Log_Video.FNEPISODE = 0;
                    Log_Video.FSBEG_TIMECODE = Broadcast_BeginTimeCode;
                    Log_Video.FSEND_TIMECODE = Broadcast_EndTimeCode;
                    Log_Video.FSTRACK = "MMMMMMMM";
                    Log_Video.FSTYPE = "P";
                    Log_Video.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                    Log_Video.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                    Log_Video.FSTITLE = "";
                    Log_Video.FSDESCRIPTION = "";
                    Log_Video.FSFILE_SIZE = "";
                    Log_Video.FSOLD_FILE_NAME = "";
                    Log_Video.FSARC_ID = "";
                    Log_Video.FSCHANGE_FILE_NO = "";

                    WSBROADCAST.Class_LOG_VIDEO_SEG ClassSeg = new WSBROADCAST.Class_LOG_VIDEO_SEG();            //段落
                    ClassSeg.FSFILE_NO = newFileNO;

                    ClassSeg.FNSEG_ID = "1";
                    ClassSeg.FSVIDEO_ID = newVideoID + "01";

                    ClassSeg.FSBEG_TIMECODE = Broadcast_BeginTimeCode;
                    ClassSeg.FSEND_TIMECODE = Broadcast_EndTimeCode;
                    ClassSeg.FSSUB_TIMECODE = TransferTimecode.Subtract_timecode(ClassSeg.FSBEG_TIMECODE, ClassSeg.FSEND_TIMECODE);
                    ClassSeg.FSCREATED_BY = UserClass.userData.FSUSER_ID.ToString();     //建檔者  
                    ClassSeg.FSUPDATED_BY = UserClass.userData.FSUSER_ID.ToString();     //建檔者
                    ClassSeg.FNBEG_TIMECODE_FRAME = TransferTimecode.timecodetoframe(ClassSeg.FSBEG_TIMECODE);
                    ClassSeg.FNEND_TIMECODE_FRAME = TransferTimecode.timecodetoframe(ClassSeg.FSEND_TIMECODE);

                    Log_Video.VideoListSeg = new List<WSBROADCAST.Class_LOG_VIDEO_SEG>() { ClassSeg };

                    broadcastClient.INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLISTAsync(Broadcast, new List<PTS_MAM3.WSBROADCAST.Class_LOG_VIDEO>() { Log_Video });
                }
            }
        }



        void broadcastClient_fnGetFileIDCompleted(object sender, WSBROADCAST.fnGetFileIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    string strGet = e.Result;  //接收回來的參數，前者是檔案編號，後者是DIRID                  

                    char[] delimiterChars = { ';' };
                    string[] words = strGet.Split(delimiterChars);

                    if (words.Length == 2)
                    {
                        newFileNO = words[0];
                        //Log_video.FSFILE_NO = words[0];
                        newDirID = words[1];
                    }
                    else if (words.Length == 1)
                    {
                        newFileNO = words[0];
                        //Log_video.FSFILE_NO = words[0];
                        newDirID = "";
                    }
                    else
                    {
                        newFileNO = "";
                        //Log_video.FSFILE_NO = "";
                        newDirID = "";
                    }

                    broadcastClient.QueryVideoID_PROG_STTAsync(Broadcast.FSID, Broadcast.FNEPISODE.ToString(), Broadcast_Arc_Type, true);

                }
            }
        }

        string _ReMaxBookingID = "";

        void broadcastClient_INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLISTCompleted(object sender, WSBROADCAST.INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLISTCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {
                    //下一步 新增託播單

                    WSPGMSendSQL.SendSQLSoapClient SP_I_Promo_Booking = new WSPGMSendSQL.SendSQLSoapClient();
                    StringBuilder sbBooking = new StringBuilder();
                    StringBuilder sbPromoZone = new StringBuilder();
                    StringBuilder sbBookingSETTING = new StringBuilder();


                    sbBooking.AppendLine("<Data>");
                    sbBooking.AppendLine("<FSPROMO_ID>" + Promo.FSPROMO_ID + "</FSPROMO_ID>");
                    sbBooking.AppendLine("<FCSTATUS>" + "Y" + "</FCSTATUS>");
                    sbBooking.AppendLine("<FSCREATED_BY>" + UserClass.userData.FSUSER_ID + "</FSCREATED_BY>");
                    sbBooking.AppendLine("<FSUPDATED_BY>" + "" + "</FSUPDATED_BY>");
                    sbBooking.AppendLine("<FSMEMO>" + tbMemo.Text + "</FSMEMO>");
                    sbBooking.AppendLine("</Data>");

                    sbBookingSETTING.AppendLine("<Data>");
                    sbBookingSETTING.AppendLine("<FSTIME1>" + textBoxTime1.Text + "</FSTIME1>");
                    sbBookingSETTING.AppendLine("<FSTIME2>" + textBoxTime2.Text + "</FSTIME2>");
                    sbBookingSETTING.AppendLine("<FSTIME3>" + textBoxTime3.Text + "</FSTIME3>");
                    sbBookingSETTING.AppendLine("<FSTIME4>" + textBoxTime4.Text + "</FSTIME4>");
                    sbBookingSETTING.AppendLine("<FSTIME5>" + textBoxTime5.Text + "</FSTIME5>");
                    sbBookingSETTING.AppendLine("<FSTIME6>" + textBoxTime6.Text + "</FSTIME6>");
                    sbBookingSETTING.AppendLine("<FSTIME7>" + textBoxTime7.Text + "</FSTIME7>");
                    sbBookingSETTING.AppendLine("<FSTIME8>" + textBoxTime8.Text + "</FSTIME8>");
                    sbBookingSETTING.AppendLine("<FSCMNO>" + "" + "</FSCMNO>");
                    sbBookingSETTING.AppendLine("</Data>");

                    int ZoneNO = 1;
                    sbPromoZone.AppendLine("<Datas>");
                    foreach (Promo_EFFECTIVE_ZONE TempZone in Promo_Zone_List)
                    {
                        sbPromoZone.AppendLine("<Data>");
                        sbPromoZone.AppendLine("<FSPROMO_ID>" + Promo.FSPROMO_ID + "</FSPROMO_ID>");
                        sbPromoZone.AppendLine("<FNNO>" + ZoneNO.ToString() + "</FNNO>");
                        ZoneNO = ZoneNO + 1;
                        sbPromoZone.AppendLine("<FDBEG_DATE>" + TempZone.FDBEG_DATE + "</FDBEG_DATE>");
                        sbPromoZone.AppendLine("<FDEND_DATE>" + TempZone.FDEND_DATE + "</FDEND_DATE>");
                        sbPromoZone.AppendLine("<FSBEG_TIME>" + TempZone.FSBEG_TIME + "</FSBEG_TIME>");
                        sbPromoZone.AppendLine("<FSEND_TIME>" + TempZone.FSEND_TIME + "</FSEND_TIME>");
                        sbPromoZone.AppendLine("<FCDATE_TIME_TYPE>" + TempZone.FCDATE_TIME_TYPE + "</FCDATE_TIME_TYPE>");
                        sbPromoZone.AppendLine("<FSCHANNEL_ID>" + TempZone.FSCHANNEL_ID + "</FSCHANNEL_ID>");
                        sbPromoZone.AppendLine("<FSWEEK>" + TempZone.FSWEEK + "</FSWEEK>");
                        sbPromoZone.AppendLine("<FSCREATED_BY>" + UserClass.userData.FSUSER_ID.ToString() + "</FSCREATED_BY>");
                        sbPromoZone.AppendLine("<FSUPDATED_BY>" + "" + "</FSUPDATED_BY>");
                        sbPromoZone.AppendLine("</Data>");
                    }
                    sbPromoZone.AppendLine("</Datas>");



                    //sb=sb.Replace("\r\n", "");

                    SP_I_Promo_Booking.Do_Insert_Promo_BookingAsync("", "", sbBooking.ToString(), "", sbPromoZone.ToString(), sbBookingSETTING.ToString());
                    SP_I_Promo_Booking.Do_Insert_Promo_BookingCompleted += (s, args) =>
                    {
                        if (args.Error == null)
                        {

                            if (args.Result != "")
                            {
                                _ReMaxBookingID = args.Result;
                                MessageBox.Show("新增成功");
                                this.DialogResult = true;
                                //try
                                //    {
                                //        newAFlow();
                                //        if (_ReMaxBookingID != "")
                                //        {
                                //            MessageBox.Show("建立完成");
                                //        }
                                //    }
                                //    catch (Exception ex)
                                //    {
                                //        MessageBox.Show("表單送出失敗,原因:"+ ex.Message);
                                //    }
                            }
                            else
                            {
                                MessageBox.Show("新增托播單資料失敗,須另行單獨新增短帶" + Promo.FSPROG_ID_NAME + "的托播單");
                                this.DialogResult = false;

                            }
                        }
                    };

                }
                else
                {
                    MessageBox.Show("新增送播單資料失敗,須另行單獨新增短帶" + Promo.FSPROG_ID_NAME + "的送播單與托播單");
                    this.DialogResult = false;

                }
            }
        }

        void promoClient_GetTBPGM_PROMO_BYPROMONAMECompleted(object sender, GetTBPGM_PROMO_BYPROMONAMECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result.Count > 0)
                {
                    Promo = e.Result[0];//短帶名稱不會重複，理論上都只會有一筆
                    //下一步 新增送播單
                    MAM_Client.GetNoRecordAsync("07", "", "", UserClass.userData.FSUSER_ID);

                }
                else
                {
                    //找不到Promo
                }
            }
        }

        void MAM_Client_GetNoRecordCompleted(object sender, WSMAMFunctions.GetNoRecordCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (!string.IsNullOrEmpty(e.Result))
                {
                    Broadcast.FSBRO_ID = e.Result;
                    Broadcast.FSID = Promo.FSPROMO_ID;
                    Broadcast.FSID_NAME = Promo.FSPROMO_NAME;
                    Broadcast.FSBRO_TYPE = "P";
                    Broadcast.FNEPISODE = 0;
                    Broadcast.FCCHECK_STATUS = "N";
                    Broadcast.FCCHANGE = "N";
                    Broadcast.FSMEMO = Broadcast_Memo;

                    Broadcast.FDBRO_DATE = DateTime.Now;
                    Broadcast.FSBRO_BY = UserClass.userData.FSUSER_ID;
                    Broadcast.FSSIGNED_BY = UserClass.userData.FSUSER_ID;
                    Broadcast.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                    Broadcast.FSCHECK_BY = UserClass.userData.FSUSER_ID;
                    Broadcast.FSCREATED_BY = UserClass.userData.FSUSER_ID;

                    //下一步 先取file_no跟videoid

                    broadcastClient.fnGetFileIDAsync("P", Broadcast.FSID, Broadcast.FNEPISODE.ToString(), UserClass.userData.FSUSER_ID, Promo.FSMAIN_CHANNEL_ID, Promo.FSPROMO_NAME, Promo.FSPROMO_NAME);


                }
                else
                {

                }
            }
        }

        void promoClient_INSERT_TBPGM_PROMOCompleted(object sender, INSERT_TBPGM_PROMOCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    //再Query一次短帶，把剛剛新增的資料取回來(為了PromoID)
                    promoClient.GetTBPGM_PROMO_BYPROMONAMEAsync(Promo_Name);


                }
                else
                {
                    //新增短帶資料時發生錯誤


                }
            }
        }

        void promoClient_QUERY_TBPROMO_CHECKCompleted(object sender, QUERY_TBPROMO_CHECKCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    if (e.Result == "0")
                    {
                        //新增短帶資料
                        newPromo = FormToClass_PROMO();
                        promoClient.INSERT_TBPGM_PROMOAsync(newPromo);
                    }
                    else
                    {
                        MessageBox.Show("資料庫已有重複的「短帶名稱」資料，請檢查後重新輸入！", "提示訊息", MessageBoxButton.OK);
                        this.BusyMsg.IsBusy = false;
                    }
                }
            }
        }

        bool CheckData()
        {
            if (String.IsNullOrEmpty(Promo_Name))
            {
                MessageBox.Show("請輸入短帶名稱");
                return false;
            }
            else if (String.IsNullOrEmpty(Promo_Type))
            {
                MessageBox.Show("請選擇類別");
                return false;
            }

            else if (String.IsNullOrEmpty(Promo_Memo))
            {
                Promo_Memo = "";
            }
            else if (String.IsNullOrEmpty(Broadcast_Arc_Type))
            {
                MessageBox.Show("請選擇檔案類型");
                return false;
            }

            string begStr = this.tbBeg_H.Text.Trim() + this.tbBeg_m.Text.Trim() + this.tbBeg_s.Text.Trim() + this.tbBeg_f.Text.Trim();
            string endStr = this.tbEnd_H.Text.Trim() + this.tbEnd_m.Text.Trim() + this.tbEnd_s.Text.Trim() + this.tbEnd_f.Text.Trim();

            if (begStr.Length + endStr.Length < 16)
            {
                MessageBox.Show("TimeCode輸入不正確");
                return false;
            }
            else 
            {
                if (CheckTimeCode() == false)
                {
                    MessageBox.Show("TimeCode輸入不正確");
                    return false;
                }
            }


            if (!String.IsNullOrEmpty(Promo_Length))
            {
                Promo_Length = Promo_Length.Trim();
                int tempInt = -1;
                if (!int.TryParse(Promo_Length, out tempInt))
                {
                    MessageBox.Show("請輸入正整數數字");

                    return false;
                }
                else if (tempInt == 0)
                {
                    MessageBox.Show("短帶長度不可為0");
                    return false;
                }
            }
            else
            {
                MessageBox.Show("請輸入播出長度");
                return false;
            }

            if (String.IsNullOrEmpty(Promo_ENG_Name))
            {
                Promo_ENG_Name = "";
            }
            else
            {
                Promo_ENG_Name = Promo_ENG_Name.Trim();
            }

            if (String.IsNullOrEmpty(Promo_Cat))
            {
                Promo_Cat = "";
                //MessageBox.Show("請選擇類型(大項)");
                //return false;
            }

            if (String.IsNullOrEmpty(Promo_Cat_Detail))
            {
                Promo_Cat_Detail = "";
                //MessageBox.Show("請選擇類型(細項)");
                //return false;
            }

            if (String.IsNullOrEmpty(Promo_Charity))
            {
                Promo_Charity = "";
            }
            else
            {
                Promo_Charity = Promo_Charity.Trim();
            }


            if (String.IsNullOrEmpty(Promo_Version))
            {
                Promo_Version = "";
            }

            if (String.IsNullOrEmpty(Promo_Activity))
            {
                Promo_Activity = "";
            }

            if (String.IsNullOrEmpty(Promo_Prog_ID))
            {
                Promo_Prog_ID = "";
            }

            if (String.IsNullOrEmpty(Promo_Prog_Episode))
            {
                Promo_Prog_Episode = "0";
            }

            if (string.IsNullOrEmpty(Promo_Create_YYYYMM))
            {
                Promo_Create_YYYYMM = "";
            }
            else
            {
                Promo_Create_YYYYMM = Promo_Create_YYYYMM.Trim();
            }

            if (String.IsNullOrEmpty(Promo_Create_Purpose))
            {
                Promo_Create_Purpose = "";
            }

            if (String.IsNullOrEmpty(Promo_Create_Unit))
            {
                Promo_Create_Unit = "";
            }

            if (String.IsNullOrEmpty(Promo_ExpireDate))
            {
                Promo_ExpireDate = "1900-01-01";
            }

            //if (!CheckTimeCode()) 
            //{
            //    MessageBox.Show("TimeCode輸入不正確");
            //    return false;
            //}

            if (String.IsNullOrEmpty(Broadcast_Memo))
            {
                Broadcast_Memo = "";
            }

            if (Promo_Zone_List.Count == 0)
            {
                MessageBox.Show("至少要有一筆短帶託播設定");
                return false;
            }



            if (Promo_Zone_List.Count <= 0)
            {
                MessageBox.Show("請先加入有效區間");
                return false;
            }



            return true;
        }

        private Class_PROMO FormToClass_PROMO()
        {
            Class_PROMO obj = new Class_PROMO();


            obj.FSPROMO_ID = "";//短帶編號，從系統取號
            obj.FSPROMO_NAME = Promo_Name; //短帶名稱
            obj.FSPROMO_NAME_ENG = Promo_ENG_Name; //短帶外文名稱
            obj.FSDURATION = Promo_Length; //播出長度(秒)
            obj.FSPROG_ID = Promo_Prog_ID; //節目編號
            obj.FNEPISODE = Convert.ToInt16(Promo_Prog_Episode);            //集別
            obj.FSPROGOBJID = Promo_Create_Purpose;//製作目的
            obj.FSPRDCENID = Promo_Create_Unit;//製作單位
            obj.FSPRDYYMM = Promo_Create_YYYYMM;//製作年月                     

            //類別
            obj.FSPROMOTYPEID = ((Class_CODE)this.cbPromoTypeID.SelectedValue).ID; //Promo_Type;


            obj.FSACTIONID = Promo_Activity;                                                           //活動

            obj.FSPROMOCATID = this.cbPromoCatID.SelectedValue != null ? ((Class_CODE)this.cbPromoCatID.SelectedValue).ID : ""; //Promo_Cat;

            obj.FSPROMOCATDID = this.cbPromoCatIDDS.SelectedValue != null ? ((Class_CODE)this.cbPromoCatIDDS.SelectedValue).IDD : ""; //Promo_Cat_Detail;

            //版本
            obj.FSPROMOVERID = Promo_Version;


            obj.FSMEMO = Promo_Memo;                                          //備註
            obj.FSMAIN_CHANNEL_ID = UserClass.userData.FSCHANNEL_ID.ToString();                   //頻道別
            obj.FSWELFARE = Promo_Charity;                                    //公益託播編號

            obj.FSCREATED_BY = UserClass.userData.FSUSER_ID.ToString();                           //建檔者
            obj.FSUPDATED_BY = UserClass.userData.FSUSER_ID.ToString();                           //修改者

            obj.FDEXPIRE_DATE = Convert.ToDateTime(Promo_ExpireDate);

            obj.FSEXPIRE_DATE_ACTION = Promo_ExpireDelete ? "Y" : "N";


            return obj;
        }



        void promoClient_GetTBPGM_PROMO_CATD_CODECompleted(object sender, GetTBPGM_PROMO_CATD_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    Cat_Detail_Code = e.Result;
                }
            }

            InitialDatas();

            BusyMsg.IsBusy = false;

        }

        void InitialDatas()
        {
            Promo_ExpireDate = DateTime.Now.AddYears(1).ToString("yyyy/MM/dd");
        }

        void promoClient_GetTBPGM_PROMO_CODECompleted(object sender, GetTBPGM_PROMO_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    //Version_Code.Add(new Class_CODE() {  NAME="",ID=""});
                    //Purpose_Code.Add(new Class_CODE() { NAME = "", ID = "" });
                    //Cat_Code.Add(new Class_CODE() { NAME = "", ID = "" });

                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            case "TBZCHANNEL":
                                Channel_Code.Add(CodeData);
                                break;

                            case "TBZPROGOBJ":       //製作目的代碼
                                Purpose_Code.Add(CodeData);
                                break;

                            case "TBZPROMOVER":      //版本代碼
                                Version_Code.Add(CodeData);
                                break;
                            case "TBZPROMOTYPE":      //類別
                                Type_Code.Add(CodeData);
                                break;
                            case "TBZPROMOCAT":       //類型代碼
                                Cat_Code.Add(CodeData);

                                break;
                            default:
                                break;
                        }
                    }
                    promoClient.GetTBPGM_PROMO_CATD_CODEAsync(); //2015/10/02 by Jarvis

                    if (cbPromoTypeID.Items.Count > 0)      //類別預設為第一個：宣傳帶
                        cbPromoTypeID.SelectedIndex = 0;
                }
            }
        }

        private void cbPromoCatID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Cat_Detail_Code

            if (this.cbPromoCatID.SelectedItem != null)
            {
                Class_CODE code = (Class_CODE)this.cbPromoCatID.SelectedItem;

                this.cbPromoCatIDDS.ItemsSource = Cat_Detail_Code.Where(a => a.ID == code.ID);
            }
        }

        private void OkBtn_Click(object sender, RoutedEventArgs e)
        {
            if (CheckData())
            {
                BusyMsg.IsBusy = true;
                promoClient.QUERY_TBPROMO_CHECKAsync(Promo_Name);
            }
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("確定取消表單?", "提示", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                this.DialogResult = false;
        }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            PRG100_11 PRG100_11_frm = new PRG100_11();
            PRG100_11_frm.Show();

            PRG100_11_frm.Closed += (s, args) =>
            {
                if (PRG100_11_frm.DialogResult == true)
                {
                    tbxPRDCEN.Text = PRG100_11_frm.strPRDCENName_View;
                    Promo_Create_Unit = PRG100_11_frm.strPRDCENID_View;
                }
            };
        }

        private void cbArcType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.cbArcType.SelectedIndex >= 0)
            {
                ComboBoxItem cbi = (ComboBoxItem)this.cbArcType.SelectedItem;

                Broadcast_Arc_Type = cbi.Tag.ToString();
            }
        }

        private void btnPROG_Click(object sender, RoutedEventArgs e)
        {
            PROGDATA_VIEW PROGDATA_VIEW_frm = new PROGDATA_VIEW();
            PROGDATA_VIEW_frm.Show();

            PROGDATA_VIEW_frm.Closed += (s, args) =>
            {
                if (PROGDATA_VIEW_frm.DialogResult == true)
                {
                    tbProg.Text = PROGDATA_VIEW_frm.strProgName_View;
                    Promo_Prog_ID = PROGDATA_VIEW_frm.strProgID_View;


                    tbEpisode.Text = "";
                    tbEpisodeName.Text = "";
                    Promo_Prog_Episode = "0";

                }
            };
        }

        private void btnEPISODE_Click(object sender, RoutedEventArgs e)
        {
            if (tbProg.Text.ToString().Trim() != "")
            {
                PRG.PRG200_07 PRG200_07_frm = new PRG.PRG200_07(Promo_Prog_ID, tbProg.Text.ToString().Trim());
                PRG200_07_frm.Show();

                PRG200_07_frm.Closed += (s, args) =>
                {
                    if (PRG200_07_frm.DialogResult == true)
                    {
                        Promo_Prog_Episode = PRG200_07_frm.m_strEpisode_View;
                        RaisePropertyChanged("Promo_Prog_Episode");
                        tbEpisodeName.Text = PRG200_07_frm.m_strProgDName_View;
                    }
                };
            }
            else
                MessageBox.Show("請先選擇節目！", "提示訊息", MessageBoxButton.OK);
        }

        private void btnAct_Click(object sender, RoutedEventArgs e)
        {
            PRG800_08 PRG800_08_frm = new PRG800_08();
            PRG800_08_frm.Show();

            PRG800_08_frm.Closed += (s, args) =>
            {
                if (PRG800_08_frm.DialogResult == true)
                {
                    tbActive.Text = PRG800_08_frm.strActIDName_View;
                    Promo_Activity = PRG800_08_frm.strActID_View;

                }
            };
        }

        private void tbEnd_f_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text.Length == tb.MaxLength)
            {
                StackPanel sp = (StackPanel)tb.Parent;

                int index = sp.Children.IndexOf(tb);

                //檢查並計算長度
                //if (index == sp.Children.Count - 1)
                // {
                if (CheckTimeCode() == false)
                {
                    MessageBox.Show("TimeCode輸入不正確");
                    return;
                }
                //}


                while (index < sp.Children.Count - 1)
                {
                    Type type = sp.Children[index + 1].GetType();
                    if (type != typeof(TextBox))
                        index += 1;
                    else
                    {
                        ((TextBox)sp.Children[index + 1]).Focus();

                        break;
                    }
                }


            }
        }

        bool? CheckTimeCode()
        {

            try
            {
                string begStr=this.tbBeg_H.Text.Trim() + this.tbBeg_m.Text.Trim() + this.tbBeg_s.Text.Trim() + this.tbBeg_f.Text.Trim ();

                if (begStr.Length < 8)
                    return null;

                TimeCode tcBegin = new TimeCode(Convert.ToInt32(this.tbBeg_H.Text), Convert.ToInt32(this.tbBeg_m.Text), Convert.ToInt32(this.tbBeg_s.Text), Convert.ToInt32(this.tbBeg_f.Text), SmpteFrameRate.Smpte2997Drop);

                Broadcast_BeginTimeCode = tcBegin.ToString();

                string endStr = this.tbEnd_H.Text.Trim() + this.tbEnd_m.Text.Trim() + this.tbEnd_s.Text.Trim() + this.tbEnd_f.Text.Trim();

                if (endStr.Length < 8)
                    return null;

                TimeCode tcEnd = new TimeCode(Convert.ToInt32(this.tbEnd_H.Text), Convert.ToInt32(this.tbEnd_m.Text), Convert.ToInt32(this.tbEnd_s.Text), Convert.ToInt32(this.tbEnd_f.Text), SmpteFrameRate.Smpte2997Drop);

                Broadcast_EndTimeCode = tcEnd.ToString();
            }
            catch (Exception ex)
            {               
                if (ex.Message == "輸入字串格式不正確。")
                {
                    return null;
                }

                return false;
            }

            int frameCount = TransferTimecode.Subtract_timecode_toframe(Broadcast_BeginTimeCode, Broadcast_EndTimeCode);
            this.tbLength.Text = TransferTimecode.frame2timecode(frameCount);

            return true;
        }

        private void addZoneBtn_Click(object sender, RoutedEventArgs e)
        {
            if (cbChannel.SelectedIndex < 0)
            {
                MessageBox.Show("請選擇頻道");
                return;
            }

            if (WeekCheckBoxToString() == "0000000")
            {
                MessageBox.Show("請至少要勾選一個星期選項");
                return;
            }

            string _TempFCDATE_TIME_TYPE;

            if (this.rbDateRange.IsChecked == true)
            {
                _TempFCDATE_TIME_TYPE = this.rbDateRange.Tag.ToString();
                if (this.dpDateBegin.SelectedDate == null)
                {
                    MessageBox.Show("請選擇起始日期");
                    return;
                }
                else if (this.dpDateEnd.SelectedDate == null)
                {
                    MessageBox.Show("請選擇結束日期");
                    return;
                }
                else if (this.dpDateBegin.SelectedDate > this.dpDateEnd.SelectedDate)
                {
                    MessageBox.Show("結束日期必須大於起始日期");
                    return;
                }
                else if (this.wtDateBegin.Text == "")
                {
                    MessageBox.Show("請輸入起始時間");
                    return;
                }
                else if (this.wtDateEnd.Text == "")
                {
                    MessageBox.Show("請輸入結束時間");
                    return;
                }
                else
                {
                    if (CheckTime(this.wtDateBegin.Text) == false || CheckTime(this.wtDateEnd.Text) == false)
                    {
                        MessageBox.Show("起迄時間格式有誤,請輸入4碼起始時間,EX 0600");
                        return;
                    }
                }
            }
            else
            {
                _TempFCDATE_TIME_TYPE = this.rbTimeRange.Tag.ToString();

                if (this.dpTimeBegin.SelectedDate == null)
                {
                    MessageBox.Show("請選擇起始日期");
                    return;
                }
                else if (this.dpTimeEnd.SelectedDate == null)
                {
                    MessageBox.Show("請選擇結束日期");
                    return;
                }
                else if (this.dpTimeBegin.SelectedDate > this.dpTimeEnd.SelectedDate)
                {
                    MessageBox.Show("結束日期必須大於起始日期");
                    return;
                }
                else if (this.wtTimeBegin.Text == "")
                {
                    MessageBox.Show("請輸入起始時間");
                    return;
                }
                else if (this.wtTimeEnd.Text == "")
                {
                    MessageBox.Show("請輸入結束時間");
                    return;
                }
                else
                {
                    if (CheckTime(this.wtTimeBegin.Text) == false || CheckTime(this.wtTimeEnd.Text) == false)
                    {
                        MessageBox.Show("起迄時間格式有誤,請輸入4碼起始時間,EX 0600");
                        return;
                    }
                }
            }



            foreach (Promo_EFFECTIVE_ZONE Temp_Promo_Zone_List in Promo_Zone_List)
            {
                bool channelSame = Temp_Promo_Zone_List.FSCHANNEL_ID == ((Class_CODE)cbChannel.SelectedItem).ID;
                bool DateSame = false;

                if (this.rbDateRange.IsChecked == true)
                {
                    DateSame = (Convert.ToDateTime(this.dpDateBegin.Text) <= Convert.ToDateTime(Temp_Promo_Zone_List.FDEND_DATE) && Convert.ToDateTime(dpDateEnd.Text) >= Convert.ToDateTime(Temp_Promo_Zone_List.FDBEG_DATE));
                }
                else
                {
                    DateSame = (this.dpTimeBegin.SelectedDate <= Convert.ToDateTime(Temp_Promo_Zone_List.FDEND_DATE) && this.dpTimeEnd.SelectedDate >= Convert.ToDateTime(Temp_Promo_Zone_List.FDBEG_DATE));
                }

                if (channelSame && DateSame)
                {
                    MessageBox.Show("此有效區間與目前已經加入過的有效區間重複,不可重複加入");
                    return;
                }
            }


            Promo_Zone_List.Add(new Promo_EFFECTIVE_ZONE()
            {
                FNPROMO_BOOKING_NO = "",
                FSPROMO_ID = Promo.FSPROMO_ID,
                FNNO = (Promo_Zone_List.Count + 1).ToString(),
                FDBEG_DATE = this.rbDateRange.IsChecked == true ? dpDateBegin.SelectedDate.Value.ToString("yyyy-MM-dd").ToString() : dpTimeBegin.SelectedDate.Value.ToString("yyyy-MM-dd"),
                FDEND_DATE = this.rbDateRange.IsChecked == true ? dpDateEnd.SelectedDate.Value.ToString("yyyy-MM-dd") : dpTimeEnd.SelectedDate.Value.ToString("yyyy-MM-dd"),
                FSBEG_TIME = this.rbDateRange.IsChecked == true ? wtDateBegin.Text : wtTimeBegin.Text,
                FSEND_TIME = this.rbDateRange.IsChecked == true ? wtDateEnd.Text : wtTimeEnd.Text,
                FCDATE_TIME_TYPE = _TempFCDATE_TIME_TYPE,
                FSCHANNEL_ID = ((Class_CODE)cbChannel.SelectedItem).ID,
                FSCHANNEL_NAME = ((Class_CODE)cbChannel.SelectedItem).NAME,
                FSWEEK = WeekCheckBoxToString(),
            });
            //RaisePropertyChanged("Promo_Zone_List");
        }

        public string WeekCheckBoxToString()
        {
            string sweek = "";
            foreach (UIElement ui in spWeek.Children)
            {
                if (ui is CheckBox)
                {
                    CheckBox cb = (CheckBox)ui;
                    if (cb.Tag != null && cb.Tag.ToString() == "ALL")//&& cb.IsChecked == true
                    {
                        continue;
                        //return "1111111";
                    }
                    sweek += (cb.IsChecked == true ? "1" : "0");
                }
            }
            return sweek;
        }

        public bool CheckTime(string Instring)
        {
            int TempInt;
            if (int.TryParse(Instring, out TempInt) == false)
                return false;
            if (Instring.Length < 4)
                return false;

            if (int.Parse(Instring.Substring(2, 2)) > 60)
                return false;
            return true;
        }

        private void removeZoneBtn_Click(object sender, RoutedEventArgs e)
        {
            if (this.dataGridZone.SelectedIndex >= 0)
            {
                Promo_Zone_List.RemoveAt(dataGridZone.SelectedIndex);

            }
            else
            {
                MessageBox.Show("請選擇要移除的項目");
            }
        }

        void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            foreach (UIElement ui in spWeek.Children)
            {
                if (ui is CheckBox)
                {
                    CheckBox cb = (CheckBox)ui;
                    //if (cb.Tag != null && cb.Tag.ToString() == "ALL")
                    //{
                    //    continue;
                    //}
                    cb.IsChecked = true;
                }
            }
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            foreach (UIElement ui in spWeek.Children)
            {
                if (ui is CheckBox)
                {
                    CheckBox cb = (CheckBox)ui;
                    //if (cb.Tag != null && cb.Tag.ToString() == "ALL")//&& cb.IsChecked == true
                    //{
                    //    continue;
                    //}
                    cb.IsChecked = false;
                }
            }
        }
    }


    public class Class_Promo_SendBroadcast_Booking
    {
        #region Promo



        #endregion


    }
}
