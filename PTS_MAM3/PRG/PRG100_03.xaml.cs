﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.PROG_M;
using PTS_MAM3.WSPROG_M;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace PTS_MAM3.ProgData
{
    public partial class PRG100_03 : ChildWindow
    {
        WSPROG_MSoapClient client = new WSPROG_MSoapClient();      //產生新的代理類別
        Class_PROGM m_FormData = new Class_PROGM();
        List<Class_CODE> m_CodeDataBuyD = new List<Class_CODE>();  //代碼檔集合(外購類別細項) 

        public PRG100_03(Class_PROGM FormData)
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面
            m_FormData = FormData;
            ClassToForm();
        }

        public PRG100_03(string strProgID)
        {
            InitializeComponent();

            //查詢節目主檔資料
            client.fnGetProg_MCompleted += new EventHandler<fnGetProg_MCompletedEventArgs>(client_fnGetProg_MCompleted);
            client.fnGetProg_MAsync(strProgID);      
        }
        
        void InitializeForm() //初始化本頁面
        {
            //下載代碼檔
            client.fnGetTBPROG_M_CODECompleted += new EventHandler<fnGetTBPROG_M_CODECompletedEventArgs>(client_fnGetTBPROG_M_CODECompleted);
            client.fnGetTBPROG_M_CODEAsync();

            //下載外購類別細項代碼檔
            client.fnGetTBPROG_M_PROGBUYD_CODECompleted += new EventHandler<fnGetTBPROG_M_PROGBUYD_CODECompletedEventArgs>(client_fnGetTBPROG_M_PROGBUYD_CODECompleted);
            client.fnGetTBPROG_M_PROGBUYD_CODEAsync();
        }

        private void ClassToForm()  //將前一頁點選的節目資料繫集到畫面上
        {
            this.gdPROG_M.DataContext = m_FormData;
        }

        #region ComboBox載入代碼檔

        //實作-下載外購類別細項代碼檔
        void client_fnGetTBPROG_M_PROGBUYD_CODECompleted(object sender, fnGetTBPROG_M_PROGBUYD_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    //m_CodeDataBuyD = new List<Class_CODE>(e.Result);

                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        ComboBoxItem cbiPROGBUYD = new ComboBoxItem();
                        cbiPROGBUYD.Content = CodeData.NAME;
                        cbiPROGBUYD.Tag = CodeData.ID;
                        cbiPROGBUYD.DataContext = CodeData.IDD;
                        this.cbPROGBUYDID.Items.Add(cbiPROGBUYD);   //先都產生在隱藏起來的細項代碼combobox裡                   
                        m_CodeDataBuyD.Add(CodeData);
                    }
                }
            }
        }

        //實作-下載所有的代碼檔
        void client_fnGetTBPROG_M_CODECompleted(object sender, fnGetTBPROG_M_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            case "TBZCHANNEL":       //頻道別代碼
                                ComboBoxItem cbiCHANNEL = new ComboBoxItem();
                                cbiCHANNEL.Content = CodeData.NAME;
                                cbiCHANNEL.Tag = CodeData.ID;
                                this.cbCHANNEL_ID.Items.Add(cbiCHANNEL);

                                CheckBox cbCHANNEL = new CheckBox();
                                cbCHANNEL.Content = CodeData.NAME;
                                cbCHANNEL.Tag = CodeData.ID;
                                cbCHANNEL.IsEnabled = false;
                                //combobox的點選及取消點選的事件處理常式
                                //cbCHANNEL.Checked += new RoutedEventHandler(cbCHANNEL_Checked);
                                //cbCHANNEL.Unchecked += new RoutedEventHandler(cbCHANNEL_Unchecked);
                                listCHANNEL.Items.Add(cbCHANNEL);
                                break;
                            case "TBZDEPT":         //部門別代碼
                                ComboBoxItem cbiDEPT = new ComboBoxItem();
                                cbiDEPT.Content = CodeData.NAME;
                                cbiDEPT.Tag = CodeData.ID;
                                this.cbPRDDEPTID.Items.Add(cbiDEPT);
                                break;
                            case "TBZPROGOBJ":      //製作目的代碼
                                ComboBoxItem cbiPROGOBJ = new ComboBoxItem();
                                cbiPROGOBJ.Content = CodeData.NAME;
                                cbiPROGOBJ.Tag = CodeData.ID;
                                this.cbPROGOBJID.Items.Add(cbiPROGOBJ);
                                break;
                            case "TBZPRDCENTER":      //製作單位代碼
                                ComboBoxItem cbiPRDCENTER = new ComboBoxItem();
                                cbiPRDCENTER.Content = CodeData.NAME;
                                cbiPRDCENTER.Tag = CodeData.ID;
                                this.cbPRDCENID.Items.Add(cbiPRDCENTER);
                                break;
                            case "TBZPROGGRADE":      //節目分級代碼
                                ComboBoxItem cbiPROGGRADE = new ComboBoxItem();
                                cbiPROGGRADE.Content = CodeData.NAME;
                                cbiPROGGRADE.Tag = CodeData.ID;
                                this.cbPROGGRADEID.Items.Add(cbiPROGGRADE);
                                break;
                            case "TBZPROGSRC":      //節目來源代碼
                                ComboBoxItem cbiPROGSRC = new ComboBoxItem();
                                cbiPROGSRC.Content = CodeData.NAME;
                                cbiPROGSRC.Tag = CodeData.ID;
                                this.cbPROGSRCID.Items.Add(cbiPROGSRC);
                                break;
                            case "TBZPROGATTR":      //內容屬性代碼
                                ComboBoxItem cbiPROGATTR = new ComboBoxItem();
                                cbiPROGATTR.Content = CodeData.NAME;
                                cbiPROGATTR.Tag = CodeData.ID;
                                this.cbPROGATTRID.Items.Add(cbiPROGATTR);
                                break;
                            case "TBZPROGAUD":      //目標觀眾代碼
                                ComboBoxItem cbiPROGAUD = new ComboBoxItem();
                                cbiPROGAUD.Content = CodeData.NAME;
                                cbiPROGAUD.Tag = CodeData.ID;
                                this.cbPROGAUDID.Items.Add(cbiPROGAUD);
                                break;
                            case "TBZPROGTYPE":      //表現方式代碼
                                ComboBoxItem cbiPROGTYPE = new ComboBoxItem();
                                cbiPROGTYPE.Content = CodeData.NAME;
                                cbiPROGTYPE.Tag = CodeData.ID;
                                this.cbPROGTYPEID.Items.Add(cbiPROGTYPE);
                                break;
                            case "TBZPROGLANG":      //語言代碼
                                ComboBoxItem cbiPROGLANG1 = new ComboBoxItem();
                                cbiPROGLANG1.Content = CodeData.NAME;
                                cbiPROGLANG1.Tag = CodeData.ID;
                                this.cbPROGLANGID1.Items.Add(cbiPROGLANG1);  //主聲道

                                ComboBoxItem cbiPROGLANG2 = new ComboBoxItem();
                                cbiPROGLANG2.Content = CodeData.NAME;
                                cbiPROGLANG2.Tag = CodeData.ID;
                                this.cbPROGLANGID2.Items.Add(cbiPROGLANG2);  //副聲道
                                break;
                            case "TBZPROGBUY":       //外購類別代碼
                                ComboBoxItem cbiPROGBUY = new ComboBoxItem();
                                cbiPROGBUY.Content = CodeData.NAME;
                                cbiPROGBUY.Tag = CodeData.ID;
                                this.cbPROGBUYID.Items.Add(cbiPROGBUY);
                                break;
                            case "TBZPROGSALE":       //行銷類別代碼
                                ComboBoxItem cbiPROGSALE = new ComboBoxItem();
                                cbiPROGSALE.Content = CodeData.NAME;
                                cbiPROGSALE.Tag = CodeData.ID;
                                this.cbPROGSALEID.Items.Add(cbiPROGSALE);
                                break;
                            case "TBZPROGSPEC":       //節目規格代碼
                                CheckBox cbPROGSPEC = new CheckBox();
                                cbPROGSPEC.Content = CodeData.NAME;
                                cbPROGSPEC.Tag = CodeData.ID;
                                cbPROGSPEC.IsEnabled = false;
                                listPROGSPEC.Items.Add(cbPROGSPEC);
                                break;
                            case "TBZPROGNATION":       //來源國家    2012/11/22 kyle                            
                                ComboBoxItem cbNATION = new ComboBoxItem();
                                cbNATION.Content = CodeData.NAME;
                                cbNATION.Tag = CodeData.ID;
                                cbNATION_ID.Items.Add(cbNATION);
                                break;
                            default:
                                break;
                        }
                    }

                    //比對代碼檔 塞入選擇取回的代碼
                    compareDERIVE(m_FormData.FCDERIVE.ToString().Trim());     //衍生
                    compareOutBuy(m_FormData.FCOUT_BUY.ToString().Trim());    //外購
                    compareDel(m_FormData.FSDEL.ToString().Trim());           //刪除註記
                    compareCode_FSPRDDEPTID(m_FormData.FSPRDDEPTID.ToString().Trim());
                    compareCode_FSPROGOBJID(m_FormData.FSPROGOBJID.ToString().Trim());
                    compareCode_FSPROGGRADEID(m_FormData.FSPROGGRADEID.ToString().Trim());
                    compareCode_FSPROGATTRID(m_FormData.FSPROGATTRID.ToString().Trim());               
                    compareCode_FSPROGSRCID(m_FormData.FSPROGSRCID.ToString().Trim());
                    compareCode_FSPROGTYPEID(m_FormData.FSPROGTYPEID.ToString().Trim());
                    compareCode_FSPROGAUDID(m_FormData.FSPROGAUDID.ToString().Trim());
                    compareCode_FSPROGLANGID1(m_FormData.FSPROGLANGID1.ToString().Trim());
                    compareCode_FSPROGLANGID2(m_FormData.FSPROGLANGID2.ToString().Trim());
                    compareCode_FSPROGBUYID(m_FormData.FSPROGBUYID.ToString().Trim());
                    compareCode_FSPROGBUYDID(m_FormData.FSPROGBUYDID.ToString().Trim());
                    compareCode_FSCHANNEL_ID(m_FormData.FSCHANNEL_ID.ToString().Trim());
                    compareCode_FSCHANNEL(CheckList(m_FormData.FSCHANNEL.ToString().Trim()));
                    compareCode_FSPROGSALEID(m_FormData.FSPROGSALEID.ToString().Trim());
                    compareCode_FSPRDCENID(m_FormData.FSPRDCENID.ToString().Trim());
                    compareCode_FSPROGSPEC(CheckList(m_FormData.FSPROGSPEC.ToString()));
                    cbNATION_ID.SelectedItem = cbNATION_ID.Items.Where(S => ((ComboBoxItem)S).Tag.ToString() == m_FormData.FSPROGNATIONID).FirstOrDefault();
                    //if (m_FormData.FDEXPIRE_DATE.ToShortDateString() != "1900/1/1")
                    if (m_FormData.FDEXPIRE_DATE > Convert.ToDateTime("1900/1/5"))
                    {
                        dpEXPIRE.Text = m_FormData.FDEXPIRE_DATE.ToShortDateString();
                    }
                    if (m_FormData.FSEXPIRE_DATE_ACTION == "Y")
                        cbDELETE.IsChecked = true;
                    else
                        cbDELETE.IsChecked = false;
                }
            }
        }

        //實作-查詢節目主檔資料
        void client_fnGetProg_MCompleted(object sender, fnGetProg_MCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)  //若是有查詢結果，則顯示
                {
                    m_FormData = e.Result[0];
                    ClassToForm();
                    InitializeForm();
                }
                else
                {
                    MessageBox.Show("查無此節目基本資料", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false; ;
                }
            }
        } 

        private string[] CheckList(string strChannel)
        {  
            char[] delimiterChars = { ';' };
            string[] words = strChannel.Split(delimiterChars);
            return words;
        }

        private void compareDERIVE(string strDERIVE)   //是否為衍生
        {      
            if (strDERIVE=="N")
                cbDERIVE.SelectedIndex=0;
            else if (strDERIVE=="Y")
                cbDERIVE.SelectedIndex=1;       
        }

        private void compareOutBuy(string strOutBuy)   //是否為外購
        {
            if (strOutBuy == "N")
                cbOUT_BUY.SelectedIndex = 0;
            else if (strOutBuy == "Y")
                cbOUT_BUY.SelectedIndex = 1;       
        }    
                
        private void compareDel(string strDel)   //是否為刪除
        {
            if (strDel == "N")
                cbDel.SelectedIndex = 0;
            else if (strDel == "Y")
                cbDel.SelectedIndex = 1;       
        } 

        //選擇外購類別，要判斷顯示哪些外購類別細項
        private void cbBUY_ID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.cbPROGBUYDIDS.Items.Clear();     //使用前先清空
            string strBUYID = ""; //外購類別大項
            ComboBox cb = sender as ComboBox;
            ComboBoxItem cbm = (ComboBoxItem)cb.SelectedItem;
            strBUYID = cbm.Tag.ToString();

            for (int i = 0; i < cbPROGBUYDID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGBUYDID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strBUYID))
                {
                    ComboBoxItem cbiPROGBUYDS = new ComboBoxItem();
                    cbiPROGBUYDS.Content = getcbi.Content;
                    cbiPROGBUYDS.Tag = getcbi.DataContext;
                    this.cbPROGBUYDIDS.Items.Add(cbiPROGBUYDS);
                }
            }
        }

        #endregion

        #region 比對代碼檔

        //比對代碼檔_製作部門
        void compareCode_FSPRDDEPTID(string strID)
        {
            for (int i = 0; i < cbPRDDEPTID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPRDDEPTID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPRDDEPTID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_製作單位
        void compareCode_FSPRDCENID(string strID)
        {
            for (int i = 0; i < cbPRDCENID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPRDCENID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPRDCENID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_節目型態(內容屬性)
        void compareCode_FSPROGATTRID(string strID)
        {
            for (int i = 0; i < cbPROGATTRID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGATTRID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGATTRID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_製作目的
        void compareCode_FSPROGOBJID(string strID)
        {
            for (int i = 0; i < cbPROGOBJID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGOBJID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGOBJID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_節目分級
        void compareCode_FSPROGGRADEID(string strID)
        {
            for (int i = 0; i < cbPROGGRADEID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGGRADEID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGGRADEID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_節目來源
        void compareCode_FSPROGSRCID(string strID)
        {
            for (int i = 0; i < cbPROGSRCID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGSRCID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGSRCID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_表現方式
        void compareCode_FSPROGTYPEID(string strID)
        {
            for (int i = 0; i < cbPROGTYPEID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGTYPEID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGTYPEID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_目標觀眾
        void compareCode_FSPROGAUDID(string strID)
        {
            for (int i = 0; i < cbPROGAUDID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGAUDID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGAUDID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_主聲道
        void compareCode_FSPROGLANGID1(string strID)
        {
            for (int i = 0; i < cbPROGLANGID1.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGLANGID1.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGLANGID1.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_副聲道
        void compareCode_FSPROGLANGID2(string strID)
        {
            for (int i = 0; i < cbPROGLANGID2.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGLANGID2.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGLANGID2.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_外購類別大類
        void compareCode_FSPROGBUYID(string strID)
        {
            for (int i = 0; i < cbPROGBUYID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGBUYID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGBUYID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_外購類別細類
        void compareCode_FSPROGBUYDID(string strID)
        {
            for (int i = 0; i < cbPROGBUYDIDS.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGBUYDIDS.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGBUYDIDS.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_可播映頻道
        void compareCode_FSCHANNEL(string[] ListTEST_Send)
        {
            for (int i = 0; i < ListTEST_Send.Length - 1; i++)
            {
                for (int j = 0; j < listCHANNEL.Items.Count; j++)
                {
                    CheckBox getcki = (CheckBox)listCHANNEL.Items.ElementAt(j);

                    if (getcki.Tag.ToString().Trim().Equals(ListTEST_Send[i]))
                    {
                        getcki.IsChecked = true;
                        break;
                    }
                }
            }
        }

        //比對代碼檔_節目規格
        void compareCode_FSPROGSPEC(string[] ListTEST_Send)
        {
            for (int i = 0; i < ListTEST_Send.Length - 1; i++)
            {
                for (int j = 0; j < listPROGSPEC.Items.Count; j++)
                {
                    CheckBox getcki = (CheckBox)listPROGSPEC.Items.ElementAt(j);

                    if (getcki.Tag.ToString().Trim().Equals(ListTEST_Send[i]))
                    {
                        getcki.IsChecked = true;
                        break;
                    }
                }
            }
        }

        //比對代碼檔_頻道別
        void compareCode_FSCHANNEL_ID(string strID)
        {
            for (int i = 0; i < cbCHANNEL_ID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbCHANNEL_ID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbCHANNEL_ID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_行銷類別
        void compareCode_FSPROGSALEID(string strID)
        {
            for (int i = 0; i < cbPROGSALEID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGSALEID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGSALEID.SelectedIndex = i;
                    break;
                }
            }
        }

        //選擇外購類別，要判斷顯示哪些外購類別細項
        private void cbPROGBUYID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.cbPROGBUYDIDS.Items.Clear();     //使用前先清空
            string strBUYID = ""; //外購類別大項
            ComboBox cb = sender as ComboBox;
            ComboBoxItem cbm = (ComboBoxItem)cb.SelectedItem;
            strBUYID = cbm.Tag.ToString();

            for (int i = 0; i < cbPROGBUYDID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGBUYDID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strBUYID))
                {
                    ComboBoxItem cbiPROGBUYDS = new ComboBoxItem();
                    cbiPROGBUYDS.Content = getcbi.Content;
                    cbiPROGBUYDS.Tag = getcbi.DataContext;
                    this.cbPROGBUYDIDS.Items.Add(cbiPROGBUYDS);
                }
            }
        }

        #endregion     

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }


    }
}

