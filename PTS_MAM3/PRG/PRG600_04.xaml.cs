﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSPROG_PRODUCER;
using System.Text;
using PTS_MAM3.PRG;

namespace PTS_MAM3.ProgData
{
    public partial class PRG600_04 : ChildWindow
    {
        WSPROG_PRODUCERSoapClient client = new WSPROG_PRODUCERSoapClient();     //產生新的代理類別
        string m_IDName = "";                                                   //暫存名稱 
        string m_strType = "";                                                  //類型
        string m_strID = "";                                                    //編碼
        public List<Class_PROGPRODUCER> m_ListFormData = new List<Class_PROGPRODUCER>();  //記錄查詢到的資料
        public Class_PROGPRODUCER m_Queryobj = new Class_PROGPRODUCER();        //查詢條件的obj
        List<UserStruct> ListUserAll = new List<UserStruct>();                  //所有的使用者資料

        public PRG600_04()
        {
            InitializeComponent();
            InitializeForm();          //初始化本頁面            
        }

        void InitializeForm() //初始化本頁面
        {          
            //查詢節目或短帶製作人資料
            client.QUERY_TBPROG_PRODUCERCompleted += new EventHandler<QUERY_TBPROG_PRODUCERCompletedEventArgs>(client_QUERY_TBPROG_PRODUCERCompleted);

            //查詢所有使用者資料
            client.QUERY_TBUSERS_ALLCompleted += new EventHandler<QUERY_TBUSERS_ALLCompletedEventArgs>(client_QUERY_TBUSERS_ALLCompleted);
            client.QUERY_TBUSERS_ALLAsync();
        }       
               
        #region 實作

        //實作-查詢節目或短帶製作人資料
        void client_QUERY_TBPROG_PRODUCERCompleted(object sender, QUERY_TBPROG_PRODUCERCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)  //若是有查詢結果，則顯示，不然就請使用者重新查詢
                {
                    m_ListFormData = new List<Class_PROGPRODUCER>(e.Result);  //把取回來的結果，加上型別定義，才可以塞回去    
                    this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("查無製作人資料，請重新輸入條件查詢！", "提示訊息", MessageBoxButton.OK);
                    return;
                }
            }
        }

        //實作-查詢所有使用者資料
        void client_QUERY_TBUSERS_ALLCompleted(object sender, QUERY_TBUSERS_ALLCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                ListUserAll = e.Result;
                acbMenID.ItemsSource = ListUserAll; //繫結AutoComplete按鈕
            }
            else
                MessageBox.Show("查詢使用者資料異常！", "提示訊息", MessageBoxButton.OK);
        }

        #endregion
        
        #region 檢查畫面資料及搬值

        private Class_PROGPRODUCER FormToClass_PROGPRODUCER()
        {
            Class_PROGPRODUCER obj = new Class_PROGPRODUCER();

            obj.FSPRO_TYPE = m_strType.Trim();                  //類型
            obj.FSID = m_strID.Trim();                          //編碼

            if (acbMenID.SelectedItem == null)                  //製作人
                obj.FSPRODUCER = "";  
            else
                obj.FSPRODUCER = ((UserStruct)(acbMenID.SelectedItem)).FSUSER_ID.Trim();
                                          
            m_IDName = tbxPROG_NAME.Text.ToString().Trim();     //暫存本次新增的名稱，為了秀給使用者看結果用
            m_Queryobj = obj;                                   //紀錄查詢條件   

            return obj;
        }

        private Boolean Check_PROGPRODUCERData()    //檢查畫面上的欄位是否都填妥
        {
            StringBuilder strMessage = new StringBuilder();

            //if (m_strType.Trim() == "")
            //    strMessage.AppendLine("請選擇「類型」");

            //if (m_strID.Trim() == "")
            //    strMessage.AppendLine("請選擇「節目名稱或短帶名稱」");

            if (acbMenID.Text.Trim() != "" && acbMenID.SelectedItem == null)
                strMessage.AppendLine("請輸入存在的人員資料");
            
            if (strMessage.ToString().Trim() == "")
                return true;
            else
            {
                MessageBoxResult result = MessageBox.Show(strMessage.ToString(), "提示訊息", MessageBoxButton.OK);
                return false;
            }
        }

        #endregion   

        //確定
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (Check_PROGPRODUCERData() == false)      //檢查畫面上的欄位是否都填妥
                return;

            //查詢
            client.QUERY_TBPROG_PRODUCERAsync(FormToClass_PROGPRODUCER());
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
      
        //選取節目時
        private void rdbProg_Checked(object sender, RoutedEventArgs e)
        {
            if (rdbProg.IsChecked == true)
            {
                m_strType = "G";
                tbPROG_NANE.Text = "節目名稱";
                tbxPROG_NAME.Text = "";
                tbxPROG_NAME.Tag = "";
                m_strID = "";              
            }
        }

        //選取短帶時
        private void rdbPromo_Checked(object sender, RoutedEventArgs e)
        {
            if (rdbPromo.IsChecked == true)
            {
                m_strType = "P";
                tbPROG_NANE.Text = "短帶名稱";
                tbxPROG_NAME.Text = "";
                tbxPROG_NAME.Tag = "";
            }
        }

        //開啟資料查詢畫面
        private void btnPROG_Click(object sender, RoutedEventArgs e)
        {
            if (rdbProg.IsChecked == true)
            {
                m_strType = "G";
                //PRG100_01
                PROGDATA_VIEW PROGDATA_VIEW_frm = new PROGDATA_VIEW();
                PROGDATA_VIEW_frm.Show();

                PROGDATA_VIEW_frm.Closed += (s, args) =>
                {
                    if (PROGDATA_VIEW_frm.DialogResult == true)
                    {
                        tbxPROG_NAME.Text = PROGDATA_VIEW_frm.strProgName_View;
                        tbxPROG_NAME.Tag = PROGDATA_VIEW_frm.strProgID_View;
                        m_strID = PROGDATA_VIEW_frm.strProgID_View;
                    }
                };
            }
            else if (rdbPromo.IsChecked == true)
            {
                 m_strType = "P" ;
                //PRG800_07
                PRG800_07 PROMO_VIEW_frm = new PRG800_07();
                PROMO_VIEW_frm.Show();

                PROMO_VIEW_frm.Closed += (s, args) =>
                {
                    if (PROMO_VIEW_frm.DialogResult == true)
                    {
                        tbxPROG_NAME.Text = PROMO_VIEW_frm.strPromoName_View;
                        tbxPROG_NAME.Tag = PROMO_VIEW_frm.strPromoID_View;
                        m_strID = PROMO_VIEW_frm.strPromoID_View;
                    }
                };
            }
            else
                MessageBox.Show("請先選擇「類型」，節目或短帶", "提示訊息", MessageBoxButton.OK);
        }

        //自動去比對資料庫，若有人員就顯示資料
        private void acbMenID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (acbMenID.SelectedItem == null)
                tbxNote.Text = "";
            else
                tbxNote.Text = ((UserStruct)(acbMenID.SelectedItem)).FSTITLE_NAME.Trim();
        }

    }
}

