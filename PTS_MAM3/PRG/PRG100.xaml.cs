﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Xml.Linq;
using System.IO;
using System.Text;
using PTS_MAM3.WSPROG_M;
using PTS_MAM3.PRG;
using PTS_MAM3.ProgData;
using System.Windows.Data;

namespace PTS_MAM3.PROG_M
{
    public partial class PROG_M : Page 
    {
        WSPROG_MSoapClient client = new WSPROG_MSoapClient();       //產生新的代理類別
        Class_PROGM m_ReuseQueryobj = new Class_PROGM();            //取得查詢頁面的查詢條件obj
        Int32 m_intPageCount = -1;                                  //紀錄PageIndex
        string m_strProgName = "";                                  //暫存節目名稱
        Class_PROGM m_Deleteobj = new Class_PROGM();                //刪除的暫存obj
        private PTS_MAM3.MainFrame parentFrame;                     //批次新增子集完成後跳到子集畫面

        public PROG_M(PTS_MAM3.MainFrame mainframe)
        {
            InitializeComponent();
            InitializeForm();           //初始化本頁面    
            parentFrame = mainframe;    //批次新增子集完成後要帶到子集的主畫面
        }
        
        void InitializeForm() //初始化本頁面
        {      
            //取得全部節目資料
            client.fnGetTBPROG_M_ALLCompleted += new EventHandler<fnGetTBPROG_M_ALLCompletedEventArgs>(client_fnGetTBPROG_M_ALLCompleted);
            //client.fnGetTBPROG_M_ALLAsync();    //一開始不Load全部資料

            //批次新增子集資料
            client.INSERT_TBPROGD_TBPROGCompleted += new EventHandler<INSERT_TBPROGD_TBPROGCompletedEventArgs>(client_INSERT_TBPROGD_TBPROGCompleted);

            //查詢節目主檔資料
            client.fnQUERYTBPROG_MCompleted += new EventHandler<fnQUERYTBPROG_MCompletedEventArgs>(client_fnQUERYTBPROG_MCompleted);  
    
            //刪除節目主檔資料
            client.DELETE_TBPROGMCompleted += new EventHandler<DELETE_TBPROGMCompletedEventArgs>(client_DELETE_TBPROGMCompleted);

            //檢查刪除節目資料
            client.DELETE_TBPROGM_CheckCompleted += new EventHandler<DELETE_TBPROGM_CheckCompletedEventArgs>(client_DELETE_TBPROGM_CheckCompleted);                      
        }

        #region 實作

        //實作-取得全部節目資料
        void client_fnGetTBPROG_M_ALLCompleted(object sender, fnGetTBPROG_M_ALLCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息 

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    //將資料bind到DataPager，先註解
                    //this.DGPROG_MList.DataContext = e.Result;

                    //if (this.DGPROG_MList.DataContext != null)
                    //{
                    //    DGPROG_MList.SelectedIndex = 0;
                    //}

                    //宣告PagedCollectionView,將撈取的資料放入
                    PagedCollectionView pcv = new PagedCollectionView(e.Result);
                    //指定DataGrid以及DataPager的內容
                    DGPROG_MList.ItemsSource = pcv;
                    DataPager.Source = pcv;                   
                }
            }             
        }

        //實作-批次新增子集資料
        void client_INSERT_TBPROGD_TBPROGCompleted(object sender, INSERT_TBPROGD_TBPROGCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息 

            if (e.Error == null)
            {
                if (e.Result.bolResult == true)
                {
                    MessageBox.Show("批次新增子集資料成功！", "提示訊息", MessageBoxButton.OK);
                    parentFrame.generateNewPane("子集資料", "PROG_D", ((Class_PROGM)DGPROG_MList.SelectedItem).FSPROG_ID.Trim(), true);
                }   
                else
                    MessageBox.Show("批次新增子集資料失敗！\n失敗原因：" + e.Result.strErrorMsg, "提示訊息", MessageBoxButton.OK);
            }
        }

        //實作-查詢節目主檔資料
        void client_fnQUERYTBPROG_MCompleted(object sender, fnQUERYTBPROG_MCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息 

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)  //若是有查詢結果，則顯示
                {                    
                    PagedCollectionView pcv = new PagedCollectionView(e.Result);                    
                    DGPROG_MList.ItemsSource = pcv;
                    DataPager.Source = pcv;

                    if (m_intPageCount!= -1)        //設定修改後查詢完要回到哪一頁
                        DataPager.PageIndex = m_intPageCount;
                }
                else
                {
                    PagedCollectionView pcv = new PagedCollectionView(new List<Class_PROGM>());
                    //指定DataGrid以及DataPager的內容
                    DGPROG_MList.ItemsSource = pcv;
                    DataPager.Source = pcv;
                }
            }
        }

        //實作-刪除節目主檔資料
        void client_DELETE_TBPROGMCompleted(object sender, DELETE_TBPROGMCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息 

            if (e.Error == null)
            {
                if (e.Result.bolResult == true)
                {
                    MessageBox.Show("刪除基本資料「" + m_strProgName + "」成功！", "提示訊息", MessageBoxButton.OK);
                    client.fnQUERYTBPROG_MAsync(m_ReuseQueryobj);    //如果有刪除節目資料，要用查詢條件去查詢
                }
                else
                {
                    MessageBox.Show("刪除基本資料「" + m_strProgName + "」失敗！\n失敗原因：" + e.Result.strErrorMsg, "提示訊息", MessageBoxButton.OK);
                }
            }
        }

        //實作-檢查刪除節目資料
        void client_DELETE_TBPROGM_CheckCompleted(object sender, DELETE_TBPROGM_CheckCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息 

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)  //若是有查詢結果，則顯示
                {
                  PRG.PRG100_09 PRG100_09_frm = new PRG.PRG100_09(e.Result);  //,"PROGM"
                  PRG100_09_frm.Show();                  
                }
                else
                {
                    //刪除基本資料
                    client.DELETE_TBPROGMAsync(m_Deleteobj, UserClass.userData.FSUSER_ID.ToString());
                }
            }
        }

        #endregion

        //查詢節目資料
        private void btnPROGQuery_Click(object sender, RoutedEventArgs e)
        {
            PROGMDATA_Query PROGMDATA_Query_frm = new PROGMDATA_Query();
            PROGMDATA_Query_frm.Show();

            PROGMDATA_Query_frm.Closed += (s, args) =>
            {
                if (PROGMDATA_Query_frm.m_ListFormData.Count != 0)
                {
                    //將資料bind到DataPager，先註解
                    //this.DGPROG_MList.DataContext = null;
                    //this.DGPROG_MList.DataContext = PROGMDATA_Query_frm.m_ListFormData;
                    //this.DGPROG_MList.SelectedIndex = 0;

                    //宣告PagedCollectionView,將撈取的資料放入
                    PagedCollectionView pcv = new PagedCollectionView(PROGMDATA_Query_frm.m_ListFormData);
                    //指定DataGrid以及DataPager的內容
                    DGPROG_MList.ItemsSource = pcv;
                    DataPager.Source = pcv;

                    m_ReuseQueryobj = PROGMDATA_Query_frm.m_Queryobj;  //紀錄查詢的條件
                }
            };
        }

        //修改節目資料
        private void btnPROGModify_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGPROG_MList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預修改的節目資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            if (checkPermission() == false) //檢查製作目的權限
                return;

            if (checkIdentified() == false) //檢查是否需要進行認定基本資料
                return;            

            m_intPageCount = DataPager.PageIndex;       //取得PageIndex

            PROGMDATA_Add PROG_M_Add_frm = new PROGMDATA_Add(((Class_PROGM)DGPROG_MList.SelectedItem));
            PROG_M_Add_frm.Show();

            PROG_M_Add_frm.Closing += (s, args) =>
            {
                if (PROG_M_Add_frm.DialogResult == true)
                {
                    client.fnQUERYTBPROG_MAsync(m_ReuseQueryobj);    //如果有修改節目資料，要用查詢條件去查詢
                    BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息 
                }
                    
            };
        }

        //新增子集資料
        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGPROG_MList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預批次新增子集的節目資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            if (checkPermission() == false) //檢查製作目的權限
                return;

            if (((Class_PROGM)DGPROG_MList.SelectedItem).FNTOTEPISODE == 0)
            {
                MessageBox.Show("選擇的節目資料總集數為「0」，無法新增子集資料", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (((Class_PROGM)DGPROG_MList.SelectedItem).FSPROGSPEC.Trim() == "")
            {
                MessageBox.Show("無設定節目子集的規格(SD、HD)，無法新增子集資料，請先設定再轉入", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else
            {
                //原先的作法，無法選擇集別，產生全部的子集
                //client.INSERT_TBPROGD_TBPROGAsync(((Class_PROGM)DGPROG_MList.SelectedItem),"","");
                //BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息

                //開啟批次新增子集
                PRG.PRG100_12 PRG100_12_frm = new PRG.PRG100_12(((Class_PROGM)DGPROG_MList.SelectedItem), parentFrame);
                PRG100_12_frm.Show();
            }                
        }

        //刪除
        private void btnPROGDelete_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGPROG_MList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預刪除的基本資料", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else
            {
                if (checkPermission() == false) //檢查製作目的權限
                    return;

                string strPROG_ID = ((Class_PROGM)DGPROG_MList.SelectedItem).FSPROG_ID.ToString().Trim();
                m_strProgName = ((Class_PROGM)DGPROG_MList.SelectedItem).FSPGMNAME.ToString().Trim();               

                MessageBoxResult result = MessageBox.Show("確定要刪除「" + m_strProgName.ToString() +  "」?", "提示訊息", MessageBoxButton.OKCancel);

                if (result == MessageBoxResult.OK)
                {
                    m_intPageCount = DataPager.PageIndex;       //取得PageIndex

                    m_Deleteobj = ((Class_PROGM)DGPROG_MList.SelectedItem);

                    //判斷刪除基本資料要依序檢查各資料表
                    client.DELETE_TBPROGM_CheckAsync(m_Deleteobj, UserClass.userData.FSUSER_ID.ToString());
                    BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                }
            }
        }

        //批次轉入
        private void btnInto_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGPROG_MList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預批次轉入的節目資料", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else
            {
                if (ModuleClass.getModulePermissionByName("02", "批次轉入子集資料") == false) //檢查批次轉入子集資料權限
                {
                    MessageBox.Show("無批次轉入子集資料權限，無法轉入", "提示訊息", MessageBoxButton.OK);
                    return;
                }                    

                PRG.PRG100_10 PRG100_10_frm = new PRG.PRG100_10(((Class_PROGM)DGPROG_MList.SelectedItem));
                PRG100_10_frm.Show();
            }
        }

        //檢視
        private void btnPROGView_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGPROG_MList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預檢視的節目資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            PRG100_03 PRG100_03_frm = new PRG100_03(((Class_PROGM)DGPROG_MList.SelectedItem));
            PRG100_03_frm.Show();
        }

        //檢查製作目的權限
        private Boolean checkPermission()
        {
            //判斷有無修改此製作目的的權限，若是無，就讓使用者無法修改
            if (ModuleClass.getModulePermissionByName("00", ((Class_PROGM)DGPROG_MList.SelectedItem).FSPROGOBJID_NAME.Trim()) == false)
            {
                MessageBox.Show("無法使用，因為沒有製作目的「" + ((Class_PROGM)DGPROG_MList.SelectedItem).FSPROGOBJID_NAME.Trim() + "」的權限", "提示訊息", MessageBoxButton.OK);
                return false;
            }
            return true;
        }

        //檢查是否需要進行認定基本資料
        private Boolean checkIdentified()
        {
            //判斷有無修改此製作目的的權限，若是無，就讓使用者無法修改
            if (((Class_PROGM)DGPROG_MList.SelectedItem).FSCHANNEL_ID.Trim()=="")
            {
                MessageBox.Show("無法修改，因為該節目尚未進行認定基本資料，請先認定基本資料", "提示訊息", MessageBoxButton.OK);
                return false;
            }
            return true;
        }

        //認定基本資料
        private void btnPROG_IDENTIFIED_Click(object sender, RoutedEventArgs e)
        {
            if (ModuleClass.getModulePermissionByName("01", "節目資料認定") == true)
            {
                //判斷是否有選取DataGrid
                if (DGPROG_MList.SelectedItem == null)
                {
                    MessageBox.Show("請選擇預認定的節目資料", "提示訊息", MessageBoxButton.OK);
                    return;
                }

                Class_PROGM obj = new Class_PROGM();
                obj = (Class_PROGM)DGPROG_MList.SelectedItem;

                PRG100_13 PRG100_13_frm = new PRG100_13("G", obj.FSPROG_ID.Trim(), obj.FSPGMNAME.Trim(), obj.FSCHANNEL_ID.Trim(), obj.FNDEP_ID.ToString().Trim());
                PRG100_13_frm.Show();

                PRG100_13_frm.Closing += (s, args) =>
                {
                    if (PRG100_13_frm.DialogResult == true)
                    {
                        client.fnQUERYTBPROG_MAsync(m_ReuseQueryobj);    //如果有修改節目資料，要用查詢條件去查詢
                        BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息 
                    }

                };
            }
            else
                MessageBox.Show("無節目資料認定權限，無法修改", "提示訊息", MessageBoxButton.OK);
        }

        //子集資料捷徑
        private void btnEpisode_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGPROG_MList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預開啟子集的節目資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            parentFrame.generateNewPane("子集資料", "PROG_D", ((Class_PROGM)DGPROG_MList.SelectedItem).FSPROG_ID.Trim(), true);
        }

        //節目衍生子集捷徑
        private void btnProgLink_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGPROG_MList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預開啟衍生子集的節目資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            parentFrame.generateNewPane("衍生節目子集", "PROG_LINK", ((Class_PROGM)DGPROG_MList.SelectedItem).FSPROG_ID.Trim(), true);
        }

        //相關人員捷徑
        private void btnProgMen_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGPROG_MList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預開啟相關人員的節目資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            parentFrame.generateNewPane("相關人員", "PROG_MEN", ((Class_PROGM)DGPROG_MList.SelectedItem).FSPROG_ID.Trim(), true);
        }

        //參展得獎記錄捷徑
        private void btnProgRace_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGPROG_MList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預開啟參展記錄的節目資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            parentFrame.generateNewPane("參展記錄", "PROG_RACE", ((Class_PROGM)DGPROG_MList.SelectedItem).FSPROG_ID.Trim(), true);
        }

        //製作人捷徑
        private void btnProducer_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGPROG_MList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預開啟製作人的節目資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            parentFrame.generateNewPane("製作人資料", "PROG_PRODUCER", ((Class_PROGM)DGPROG_MList.SelectedItem).FSPROG_ID.Trim(), true);
        }

    }
}
