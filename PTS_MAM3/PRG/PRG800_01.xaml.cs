﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using PTS_MAM3.PROG_M;
using PTS_MAM3.WSPROMO;
using PTS_MAM3.ProgData;

namespace PTS_MAM3.PRG
{
    public partial class PRG800_01 : ChildWindow
    {
        WSPROMOSoapClient client = new WSPROMOSoapClient();
        List<Class_CODE> m_CodeDataCateD = new List<Class_CODE>();  //代碼檔集合(短帶類型細項) 
        string m_strPromoName = "";        //暫存短帶名稱 
        string m_strID = "";              //編碼
        string m_strEpisode = "";         //集別
        string m_ActID = "";              //活動代碼
        Boolean m_bolContinue = false;    //判斷繼續新增
        Boolean m_bolSTT = false;         //判斷送帶轉檔
        Boolean m_bolPrint = false;       //判斷列印編號
        string m_strPrintID = "";         //列印編號
        string m_strPromoID = ""; //PROMOID 複製表單時用的

        public Class_PROMO m_Queryobj = new Class_PROMO();    //查詢新增的obj

        public PRG800_01()
        {
            InitializeComponent();
            InitializeForm();           //初始化本頁面
            dpEXPIRE.Text = DateTime.Today.AddYears(1).ToShortDateString(); //設定一年後自動刪除
            //cbDELETE.IsChecked = true;  //到其後自動刪除設定//預設不勾選 2016-03-23  by Jarvis
        }

        /// <summary>
        /// 用來複製短帶資料的建構子
        /// </summary>
        /// <param name="iniClass_PROMO">選取的要複製的類別</param>
        public PRG800_01(Class_PROMO iniClass_PROMO)
        {
            InitializeComponent();
            m_strPromoID = iniClass_PROMO.FSPROMO_ID.Trim();
            InitializeForm();          //初始化本頁面
            dpEXPIRE.Text = DateTime.Today.AddYears(1).ToShortDateString(); //設定一年後自動刪除
            cbDELETE.IsChecked = true;  //到其後自動刪除設定


        }

        void InitializeForm() //初始化本頁面
        {
            //新增短帶資料檔資料
            client.INSERT_TBPGM_PROMOCompleted += new EventHandler<INSERT_TBPGM_PROMOCompletedEventArgs>(client_INSERT_TBPGM_PROMOCompleted);

            //下載代碼檔
            client.GetTBPGM_PROMO_CODECompleted += new EventHandler<GetTBPGM_PROMO_CODECompletedEventArgs>(client_GetTBPGM_PROMO_CODECompleted);
            client.GetTBPGM_PROMO_CODEAsync();

            //下載短帶類型細項代碼檔
            client.GetTBPGM_PROMO_CATD_CODECompleted += new EventHandler<GetTBPGM_PROMO_CATD_CODECompletedEventArgs>(client_GetTBPGM_PROMO_CATD_CODECompleted);
            //client.GetTBPGM_PROMO_CATD_CODEAsync(); 2015/10/02 by Jarvis

            //查詢是否有相同名稱的短帶
            client.QUERY_TBPROMO_CHECKCompleted += new EventHandler<QUERY_TBPROMO_CHECKCompletedEventArgs>(client_QUERY_TBPROMO_CHECKCompleted);

            //查詢宣傳帶主檔透過宣傳帶編號 
            client.QUERY_TBPGM_PROMO_BYIDCompleted += new EventHandler<QUERY_TBPGM_PROMO_BYIDCompletedEventArgs>(client_QUERY_TBPGM_PROMO_BYIDCompleted);

            
            //BusyMsg.IsBusy = true;  
        }
   
        #region 實作

        //實作-下載短帶類型細項代碼檔
        void client_GetTBPGM_PROMO_CATD_CODECompleted(object sender, GetTBPGM_PROMO_CATD_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    //m_CodeDataBuyD = new List<Class_CODE>(e.Result);

                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        ComboBoxItem cbiPROMOCATD = new ComboBoxItem();
                        cbiPROMOCATD.Content = CodeData.NAME;
                        cbiPROMOCATD.Tag = CodeData.ID;
                        cbiPROMOCATD.DataContext = CodeData.IDD;
                        this.cbPROMOCATIDD.Items.Add(cbiPROMOCATD);   //先都產生在隱藏起來的細項代碼combobox裡                   
                        m_CodeDataCateD.Add(CodeData);
                    }
                }
            }
        }

        //實作-下載代碼檔
        void client_GetTBPGM_PROMO_CODECompleted(object sender, GetTBPGM_PROMO_CODECompletedEventArgs e)
        {
            //BusyMsg.IsBusy = false;  

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    //版本代碼，空白要自己帶入
                    ComboBoxItem cbiPROMOVERNull = new ComboBoxItem();
                    cbiPROMOVERNull.Content = " ";
                    cbiPROMOVERNull.Tag = "";
                    this.cbPROMOVERID.Items.Add(cbiPROMOVERNull);

                    //製作目的代碼，空白要自己帶入
                    ComboBoxItem cbiPROGOBJNull = new ComboBoxItem();
                    cbiPROGOBJNull.Content = " ";
                    cbiPROGOBJNull.Tag = "";
                    this.cbPROGOBJID.Items.Add(cbiPROGOBJNull);

                    //製作單位代碼，空白要自己帶入
                    ComboBoxItem cbiPRDCENTERNull = new ComboBoxItem();
                    cbiPRDCENTERNull.Content = " ";
                    cbiPRDCENTERNull.Tag = "";
                    this.cbPRDCENID.Items.Add(cbiPRDCENTERNull);

                    ////類型大項代碼，空白要自己帶入//後來設定為必填，所以不用給空白
                    //ComboBoxItem cbiPROMOCATNull = new ComboBoxItem();
                    //cbiPROMOCATNull.Content = " ";
                    //cbiPROMOCATNull.Tag = "";
                    //this.cbPROMOCATID.Items.Add(cbiPROMOCATNull);

                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            case "TBZCHANNEL":        //頻道別代碼
                                ComboBoxItem cbiCHANNEL = new ComboBoxItem();
                                cbiCHANNEL.Content = CodeData.NAME;
                                cbiCHANNEL.Tag = CodeData.ID;
                                this.cbCHANNEL_ID.Items.Add(cbiCHANNEL);
                                break;
                            case "TBZPROGOBJ":       //製作目的代碼
                                ComboBoxItem cbiPROGOBJ = new ComboBoxItem();
                                cbiPROGOBJ.Content = CodeData.NAME;
                                cbiPROGOBJ.Tag = CodeData.ID;
                                this.cbPROGOBJID.Items.Add(cbiPROGOBJ);
                                break;
                            case "TBZPRDCENTER":      //製作單位代碼
                                if (CodeData.ID.Trim() != "00000")  //舊資料的代碼要略過
                                {
                                    ComboBoxItem cbiPRDCENTER = new ComboBoxItem();
                                    cbiPRDCENTER.Content = CodeData.NAME;
                                    cbiPRDCENTER.Tag = CodeData.ID;
                                    this.cbPRDCENID.Items.Add(cbiPRDCENTER);
                                }
                                break;
                            case "TBZPROMOVER":      //版本代碼
                                ComboBoxItem cbiPROMOVER = new ComboBoxItem();
                                cbiPROMOVER.Content = CodeData.NAME;
                                cbiPROMOVER.Tag = CodeData.ID;
                                this.cbPROMOVERID.Items.Add(cbiPROMOVER);
                                break;
                            case "TBZPROMOTYPE":      //類別
                                ComboBoxItem cbiPROMOTYPE = new ComboBoxItem();
                                cbiPROMOTYPE.Content = CodeData.NAME;
                                cbiPROMOTYPE.Tag = CodeData.ID;
                                this.cbPROMOTYPEID.Items.Add(cbiPROMOTYPE);
                                break;
                            case "TBZPROMOCAT":       //類型代碼
                                ComboBoxItem cbiPROMOCAT = new ComboBoxItem();
                                cbiPROMOCAT.Content = CodeData.NAME;
                                cbiPROMOCAT.Tag = CodeData.ID;
                                this.cbPROMOCATID.Items.Add(cbiPROMOCAT);
                                
                                break;
                            default:
                                break;
                        }
                    }
                    client.GetTBPGM_PROMO_CATD_CODEAsync(); //2015/10/02 by Jarvis

                    if (cbPROMOTYPEID.Items.Count > 0)      //類別預設為第一個：宣傳帶
                        cbPROMOTYPEID.SelectedIndex = 0;
                    client.QUERY_TBPGM_PROMO_BYIDAsync(m_strPromoID);

                    BusyMsg.IsBusy = true;
                }
            }
        }

        //實作-查詢是否有相同名稱的短帶
        void client_QUERY_TBPROMO_CHECKCompleted(object sender, QUERY_TBPROMO_CHECKCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    if (e.Result == "0")
                    {
                        //新增短帶資料
                        client.INSERT_TBPGM_PROMOAsync(FormToClass_PROMO());
                    }
                    else
                    {
                        MessageBox.Show("資料庫已有重複的「短帶名稱」資料，請檢查後重新輸入！", "提示訊息", MessageBoxButton.OK);
                        this.BusyMsg.IsBusy = false;
                    }
                }
            }
        }

        //實作-新增短帶資料檔資料
        void client_INSERT_TBPGM_PROMOCompleted(object sender, INSERT_TBPGM_PROMOCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    MessageBox.Show("新增短帶基本資料「" + m_strPromoName.ToString() + "」成功！", "提示訊息", MessageBoxButton.OK);
                    this.BusyMsg.IsBusy = false;
                    if (m_bolContinue==false)          //讓使用者可以一直新增同樣內容的資料
                        this.DialogResult = true; 
 
                    if (m_bolSTT == true)              //繼續送帶轉檔
                    {
                        if (m_bolPrint == false)       //不印在同支帶子，就把列印編號清掉
                            m_strPrintID = "";

                        STT.STT100_07 STT100_07_frm = new STT.STT100_07(tbxPROMO_NAME.Text.Trim(), m_strPrintID);
                        STT100_07_frm.Show();

                        STT100_07_frm.Closing += (s, args) =>
                        {
                            //取回列印編號
                            m_strPrintID = STT100_07_frm.tbxPRINTID.Text.Trim();
                        };                       
                    }
                }
                else
                {
                    MessageBox.Show("新增短帶基本資料「" + m_strPromoName.ToString() + "」失敗！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
        }

        //實作-查詢宣傳帶主檔透過宣傳帶編號 
        void client_QUERY_TBPGM_PROMO_BYIDCompleted(object sender, QUERY_TBPGM_PROMO_BYIDCompletedEventArgs e)
        {
            

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    Class_PROMO obj = new Class_PROMO();

                    obj = e.Result[0];

                    tbxPROMO_NAME.Text = obj.FSPROMO_NAME.Trim();
                    tbxFROMO_NAME_ENG.Text = obj.FSPROMO_NAME_ENG.Trim();
                    tbxDURATION.Text = obj.SHOW_FSDURATION.Trim();
                    tbxPROG_NAME.Text =obj.FSPROG_ID_NAME.Trim();
                    tbxPROG_NAME.Tag = obj.FSPROG_ID.Trim();
                    m_strID = obj.FSPROG_ID.Trim();;

                    if (obj.FNEPISODE != 0)
                    {
                        tbxEPISODE.Text = obj.FNEPISODE.ToString().Trim();
                        lblEPISODE_NAME.Content = obj.FNEPISODE_NAME.Trim();
                        m_strEpisode = obj.FNEPISODE.ToString().Trim();
                    }
                    else
                    {
                        tbxEPISODE.Text = "";
                        lblEPISODE_NAME.Content = "";
                        m_strEpisode = "0";
                    }
                    
                    tbxPRDYYMM.Text = obj.FSPRDYYMM.Trim();
                    tbxMEMO.Text = obj.FSMEMO.Trim();
                    tbxACTIONID.Text = obj.FSACTION_NAME.Trim();
                    tbxACTIONID.Tag = obj.FSACTIONID.Trim();
                    m_ActID = obj.FSACTIONID.Trim();
                    tbxWelfare.Text = obj.FSWELFARE.Trim();

                    compareCode_FSPROGOBJID(obj.FSPROGOBJID.Trim());
                    compareCode_FSPRDCENID(obj.FSPRDCENID.Trim());
                    compareCode_FSPROMOVERID(obj.FSPROMOVERID.Trim());
                    compareCode_FSPROMOTYPEID(obj.FSPROMOTYPEID.Trim());
                    compareCode_FSPROMOCATID(obj.FSPROMOCATID.Trim());
                    compareCode_FSPROMOCATDID(obj.FSPROMOCATDID.Trim());                   
                }            
            }
            BusyMsg.IsBusy = false;
        }

        #endregion

        #region 檢查畫面資料及搬值

        //比對代碼檔_製作單位
        void compareCode_FSPRDCENID(string strID)
        {
            for (int i = 0; i < cbPRDCENID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPRDCENID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPRDCENID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_製作目的
        void compareCode_FSPROGOBJID(string strID)
        {
            for (int i = 0; i < cbPROGOBJID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGOBJID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGOBJID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_短帶版本
        void compareCode_FSPROMOVERID(string strID)
        {
            for (int i = 0; i < cbPROMOVERID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROMOVERID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROMOVERID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_類別
        void compareCode_FSPROMOTYPEID(string strID)
        {
            for (int i = 0; i < cbPROMOTYPEID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROMOTYPEID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROMOTYPEID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_類型大項
        void compareCode_FSPROMOCATID(string strID)
        {
            for (int i = 0; i < cbPROMOCATID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROMOCATID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROMOCATID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_類型細項
        void compareCode_FSPROMOCATDID(string strID)
        {
            for (int i = 0; i < cbPROMOCATIDDS.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROMOCATIDDS.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROMOCATIDDS.SelectedIndex = i;
                    break;
                }
            }
        }

        private Class_PROMO FormToClass_PROMO()
        {
            Class_PROMO obj = new Class_PROMO();

            m_strPromoName = tbxPROMO_NAME.Text.ToString().Trim();                              //短帶名稱
            obj.FSPROMO_ID = "" ;                                                               //短帶編號，從系統取號
            obj.FSPROMO_NAME = tbxPROMO_NAME.Text.ToString().Trim();                            //短帶名稱
            obj.FSPROMO_NAME_ENG = tbxFROMO_NAME_ENG.Text.ToString().Trim();                    //短帶外文名稱
            obj.FSDURATION = tbxDURATION.Text.ToString().Trim() ;                               //播出長度(秒)
            obj.FSPROG_ID = m_strID;                                                            //節目編號

           if (m_strEpisode.Trim() == "")                                                       //集別
               obj.FNEPISODE = 0;
            else
               obj.FNEPISODE = Convert.ToInt16(m_strEpisode.Trim());
          
           if (cbPROGOBJID.SelectedIndex != -1)                                                 //製作目的
               obj.FSPROGOBJID = ((ComboBoxItem)cbPROGOBJID.SelectedItem).Tag.ToString();
           else
               obj.FSPROGOBJID =""; 

           if (tbxPRDCEN.Text.Trim() != "")                                                     //製作單位
               obj.FSPRDCENID = tbxPRDCEN.Tag.ToString();
           else
               obj.FSPRDCENID = "";

           obj.FSPRDYYMM = tbxPRDYYMM.Text.ToString().Trim() ;                                  //製作年月
         
           if (cbPROMOTYPEID.SelectedIndex != -1)                                               //類別
               obj.FSPROMOTYPEID =((ComboBoxItem)cbPROMOTYPEID.SelectedItem).Tag.ToString(); 
           else
               obj.FSPROMOTYPEID = "";

            obj.FSACTIONID = m_ActID;                                                           //活動
            
          if (cbPROMOCATID.SelectedIndex != -1)                                                 //類型大類
             obj.FSPROMOCATID = ((ComboBoxItem)cbPROMOCATID.SelectedItem).Tag.ToString();
           else
             obj.FSPROMOCATID = "" ;

          if (cbPROMOCATIDDS.SelectedIndex != -1)                                               //類型細類
              obj.FSPROMOCATDID = ((ComboBoxItem)cbPROMOCATIDDS.SelectedItem).Tag.ToString();
          else
              obj.FSPROMOCATDID ="";

          if (cbPROMOVERID.SelectedIndex != -1)                                                 //版本
              obj.FSPROMOVERID = ((ComboBoxItem)cbPROMOVERID.SelectedItem).Tag.ToString();
          else
              obj.FSPROMOVERID ="";

          obj.FSMEMO = tbxMEMO.Text.ToString().Trim();                                          //備註
          obj.FSMAIN_CHANNEL_ID = UserClass.userData.FSCHANNEL_ID.ToString();                   //頻道別
          obj.FSWELFARE = tbxWelfare.Text.ToString().Trim();                                    //公益託播編號

          obj.FSCREATED_BY = UserClass.userData.FSUSER_ID.ToString();                           //建檔者
          obj.FSUPDATED_BY = UserClass.userData.FSUSER_ID.ToString();                           //修改者

          if (dpEXPIRE.Text != "")
              obj.FDEXPIRE_DATE = Convert.ToDateTime(dpEXPIRE.Text).AddHours(23).AddMinutes(59);
          else
              obj.FDEXPIRE_DATE = Convert.ToDateTime("1900-01-01");

          if (cbDELETE.IsChecked==true)
            obj.FSEXPIRE_DATE_ACTION = "Y";
          else
            obj.FSEXPIRE_DATE_ACTION = "N";

          m_Queryobj = obj;

            return obj;
        }

        private Boolean Check_PROMO()    //檢查畫面上的欄位是否都填妥
        {
            StringBuilder strMessage = new StringBuilder();
            double douCheck;   //純粹為了判斷是否為數字型態之用   

            if (tbxPROMO_NAME.Text.Trim() == "")
                strMessage.AppendLine("請輸入「短帶名稱」欄位");

            if (tbxDURATION.Text.Trim() == "")
                strMessage.AppendLine("請輸入「播出長度」欄位");
            else
            {
                if (double.TryParse(tbxDURATION.Text.Trim(), out douCheck) == false)
                {
                    strMessage.AppendLine("請檢查「播出長度」欄位必須為數值型態");
                }
                else if (tbxDURATION.Text.Trim().StartsWith("-"))
                {
                    strMessage.AppendLine("請檢查「播出長度」欄位必須為正整數");
                }
            }

            if (cbPROMOTYPEID.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「類別」欄位");

            if (cbPROMOCATID.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「類型大項」及「類型細項」欄位");
            else
            {
                if (cbPROMOCATID.SelectedIndex != -1 && ((ComboBoxItem)(cbPROMOCATID.SelectedItem)).Content.ToString().Trim() != "" && cbPROMOCATIDDS.SelectedIndex == -1) //因為設定外鍵，因此選擇了大項就要設細項
                    strMessage.AppendLine("請輸入「類型細項」欄位");
            }

            if (strMessage.ToString().Trim() == "")
                return true;
            else
            {
                MessageBoxResult result = MessageBox.Show(strMessage.ToString(), "提示訊息", MessageBoxButton.OK);
                this.BusyMsg.IsBusy = false;

                return false;
            }
        }

        #endregion


        //確定
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.BusyMsg.IsBusy = true;

            m_bolContinue = false;             //不繼續新增
            m_bolSTT = false;                  //不送帶轉檔
            m_bolPrint = false;                //不同支帶子
            checkPromo();   
        }

        //繼續新增
        private void ContinueButton_Click(object sender, RoutedEventArgs e)
        {
            this.BusyMsg.IsBusy = true;

            m_bolContinue = true;              //繼續新增
            m_bolSTT = false;                  //不送帶轉檔
            m_bolPrint = false;                //不同支帶子
            checkPromo();
        }

        //送帶轉檔(短帶都在同支帶子)
        private void btnSTT_Click(object sender, RoutedEventArgs e)
        {
            this.BusyMsg.IsBusy = true;
            m_bolContinue = true;             //繼續新增
            m_bolSTT = true;                  //送帶轉檔
            m_bolPrint = true;                //同支帶子
            checkPromo();
        }

        //送帶轉檔(短帶在不同支帶子)
        private void btnSTT_ONE_Click(object sender, RoutedEventArgs e)
        {
            this.BusyMsg.IsBusy = true;

            m_bolContinue = true;             //繼續新增
            m_bolSTT = true;                  //送帶轉檔
            m_bolPrint = false;               //不同支帶子
            checkPromo();
        }

        //確定後先檢查
        private void checkPromo()
        {
            if (Check_PROMO() == false)      //檢查畫面上的欄位是否都填妥
                return;

            //查詢是否有相同短帶名稱的資料
            client.QUERY_TBPROMO_CHECKAsync(tbxPROMO_NAME.Text.ToString().Trim());
        }

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //選擇短帶類型，要判斷顯示哪些短帶類型細項
        private void cbPROMOCATID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.cbPROMOCATIDDS.Items.Clear();     //使用前先清空
            string strCATID = ""; //外購類別大項
            ComboBox cb = sender as ComboBox;
            ComboBoxItem cbm = (ComboBoxItem)cb.SelectedItem;
            strCATID = cbm.Tag.ToString();

            for (int i = 0; i < cbPROMOCATIDD.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROMOCATIDD.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strCATID))
                {
                    ComboBoxItem cbiPROMOCATIDS = new ComboBoxItem();
                    cbiPROMOCATIDS.Content = getcbi.Content;
                    cbiPROMOCATIDS.Tag = getcbi.DataContext;
                    this.cbPROMOCATIDDS.Items.Add(cbiPROMOCATIDS);
                }
            }
        }

        //選取活動
        private void btnAct_Click(object sender, RoutedEventArgs e)
        {
            PRG800_08 PRG800_08_frm = new PRG800_08();
            PRG800_08_frm.Show();

            PRG800_08_frm.Closed += (s, args) =>
            {
                if (PRG800_08_frm.DialogResult == true)
                {
                    tbxACTIONID.Text = PRG800_08_frm.strActIDName_View;
                    tbxACTIONID.Tag = PRG800_08_frm.strActID_View;
                    m_ActID = PRG800_08_frm.strActID_View; 
                }
            };
        }

        //選取節目名稱
        private void btnPROG_Click(object sender, RoutedEventArgs e)
        {
            //PRG100_01
            PROGDATA_VIEW PROGDATA_VIEW_frm = new PROGDATA_VIEW();
            PROGDATA_VIEW_frm.Show();

            PROGDATA_VIEW_frm.Closed += (s, args) =>
            {
                if (PROGDATA_VIEW_frm.DialogResult == true)
                {
                    tbxPROG_NAME.Text = PROGDATA_VIEW_frm.strProgName_View;
                    tbxPROG_NAME.Tag = PROGDATA_VIEW_frm.strProgID_View;

                    tbxEPISODE.Text = "";
                    lblEPISODE_NAME.Content = "";
                    m_strEpisode = "0";                
                    m_strID = PROGDATA_VIEW_frm.strProgID_View;
                }
            };
        }

        //選取子集名稱
        private void btnEPISODE_Click(object sender, RoutedEventArgs e)
        {
            if (tbxPROG_NAME.Text.ToString().Trim() != "" && tbxPROG_NAME.Tag.ToString().Trim() != "")
            {
                PRG.PRG200_07 PRG200_07_frm = new PRG.PRG200_07(tbxPROG_NAME.Tag.ToString().Trim(), tbxPROG_NAME.Text.ToString().Trim());
                PRG200_07_frm.Show();

                PRG200_07_frm.Closed += (s, args) =>
                {
                    if (PRG200_07_frm.DialogResult == true)
                    {
                        m_strEpisode = PRG200_07_frm.m_strEpisode_View;
                        tbxEPISODE.Text = PRG200_07_frm.m_strEpisode_View;
                        lblEPISODE_NAME.Content = PRG200_07_frm.m_strProgDName_View;
                    }
                };
            }
            else
                MessageBox.Show("請先選擇節目！", "提示訊息", MessageBoxButton.OK);
        }

        //預帶短帶資料
        private void btnQuery_Click(object sender, RoutedEventArgs e)
        {
            PRG800_07 PROMO_VIEW_frm = new PRG800_07();
            PROMO_VIEW_frm.Show();

            PROMO_VIEW_frm.Closed += (s, args) =>
            {
                if (PROMO_VIEW_frm.DialogResult == true)
                {
                    client.QUERY_TBPGM_PROMO_BYIDAsync(PROMO_VIEW_frm.strPromoID_View.Trim());
                    BusyMsg.IsBusy = true;
                }
            };
        }

        //選取製作單位
        private void btnPRDCEN_Click(object sender, RoutedEventArgs e)
        {
            PRG100_11 PRG100_11_frm = new PRG100_11();
            PRG100_11_frm.Show();

            PRG100_11_frm.Closed += (s, args) =>
            {
                if (PRG100_11_frm.DialogResult == true)
                {
                    tbxPRDCEN.Text = PRG100_11_frm.strPRDCENName_View;
                    tbxPRDCEN.Tag = PRG100_11_frm.strPRDCENID_View;
                }
            };
        }

    }
}

