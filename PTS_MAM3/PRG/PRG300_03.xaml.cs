﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using PTS_MAM3.WSPROGLINK;

namespace PTS_MAM3.ProgData
{
    public partial class PRG300_03 : ChildWindow
    {
        WSPROGLINKSoapClient client = new WSPROGLINKSoapClient();     //產生新的代理類別  
        Class_PROGLINK m_FormData = new Class_PROGLINK();
        string m_strProgName = "";                                    //暫存衍生節目名稱 
        string m_strEpisode = "";                                     //暫存衍生集別
        public Class_PROGLINK m_Queryobj = new Class_PROGLINK();      //查詢修改的obj

        public PRG300_03(Class_PROGLINK FormData)
        {
            InitializeComponent();        
            m_FormData = FormData;
            ClassToForm();
        }

        private void ClassToForm()  //將前一頁點選的節目資料繫集到畫面上
        {
            this.gdPROG_LINK.DataContext = m_FormData;

            //將class裡的資料放到畫面上

            //衍生
            tbxPROG_NAME.Text = m_FormData.FSPROG_ID_NAME.ToString();
            tbxPROG_NAME.Tag =  m_FormData.FSPROG_ID.ToString();     
            tbxEPISODE.Text =  m_FormData.SHOW_FNEPISODE.ToString();
            lblEPISODE_NAME.Content = m_FormData.FNEPISODE_NAME.ToString();

            //扣播出次數
            tbxORGPROG_NAME.Text =  m_FormData.FSORGPROG_ID_NAME.ToString();
            tbxORGPROG_NAME.Tag =  m_FormData.FSORGPROG_ID.ToString();
            tbxORGEPISODE.Text =  m_FormData.SHOW_ORGFNEPISODE.ToString();;
            lblORGEPISODE_NAME.Content = m_FormData.FNORGEPISODE_NAME.ToString();

            tbxMEMO.Text = m_FormData.FSMEMO.ToString();    //備註
        }    
        
        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

