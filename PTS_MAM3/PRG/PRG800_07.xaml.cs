﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSPROMO;
using PTS_MAM3.ProgData;
using DataGridDCTest;
using PTS_MAM3.WSBROADCAST;

namespace PTS_MAM3.PRG
{
    public partial class PRG800_07 : ChildWindow
    {
        WSPROMOSoapClient client = new WSPROMOSoapClient();  //產生新的代理類別
        WSBROADCASTSoapClient clientBro = new WSBROADCASTSoapClient(); 

        public string strPromoID_View = "";     //選取的短帶編號
        public string strPromoName_View = "";   //選取的短帶名稱
        public string strChannel_Id = "";       //頻道別
        public string strProducer = "";         //製作人
        public string strDEP_ID = "";           //成案單位

        public PRG800_07()
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面           
        }

        void InitializeForm() //初始化本頁面
        {            
            //查詢短帶資料
            client.QUERY_TBPGM_PROMOCompleted += new EventHandler<QUERY_TBPGM_PROMOCompletedEventArgs>(client_QUERY_TBPGM_PROMOCompleted);

            //查詢節目製作人
            clientBro.GetPRODUCERCompleted += new EventHandler<GetPRODUCERCompletedEventArgs>(clientBro_GetPRODUCERCompleted);
        }

        void LoadBusy()
        {
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
            btnSearch.IsEnabled = false;
            btnMy.IsEnabled = false;
            btnChannel.IsEnabled = false;
            tbxPROMONAME.IsEnabled = false;
        }

        void LoadBusyUnLock()
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息
            btnSearch.IsEnabled = true;
            btnMy.IsEnabled = true;
            btnChannel.IsEnabled = true;
            tbxPROMONAME.IsEnabled = true;
            tbxPROMONAME.Focus();
        }
                
        //實作-查詢節目製作人
        void clientBro_GetPRODUCERCompleted(object sender, GetPRODUCERCompletedEventArgs e)
        {
            LoadBusyUnLock();

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Trim() != "")
                {
                    strProducer = e.Result;
                }
            }
            this.DialogResult = true;
        }

        //實作-查詢短帶資料
        void client_QUERY_TBPGM_PROMOCompleted(object sender, QUERY_TBPGM_PROMOCompletedEventArgs e)
        {
            LoadBusyUnLock();

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    this.DGPROMOList.DataContext = e.Result;

                    if (this.DGPROMOList.DataContext != null)
                    {
                        DGPROMOList.SelectedIndex = 0;
                    }
                }
                else
                {
                    MessageBox.Show("查無短帶資料，請重新輸入條件查詢！", "提示訊息", MessageBoxButton.OK);
                    return;
                }
            }   
        }

        //確定
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGPROMOList.SelectedItem == null)
            {
                MessageBox.Show("請選擇節目資料", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else
            {  
                strPromoID_View = ((Class_PROMO)DGPROMOList.SelectedItem).FSPROMO_ID.ToString();
                strPromoName_View = ((Class_PROMO)DGPROMOList.SelectedItem).FSPROMO_NAME.ToString();
                strChannel_Id = ((Class_PROMO)DGPROMOList.SelectedItem).FSMAIN_CHANNEL_ID.ToString();
                strDEP_ID = ((Class_PROMO)DGPROMOList.SelectedItem).FNDEP_ID.ToString();
                //this.DialogResult = true;

                clientBro.GetPRODUCERAsync("P", strPromoID_View);    //選定短帶後，再去查詢短帶製作人
                LoadBusy();
            }
        }

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
       
        //查詢全部
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            Class_PROMO obj = new Class_PROMO();

            obj.FSPROMO_NAME = tbxPROMONAME.Text.Trim();
            obj.FSDEL = "N";
            client.QUERY_TBPGM_PROMOAsync(obj);
            
            LoadBusy();
        }
	
		//本人建立
        private void btnMy_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Class_PROMO obj = new Class_PROMO();

            obj.FSPROMO_NAME = tbxPROMONAME.Text.Trim();
            obj.FSCREATED_BY = UserClass.userData.FSUSER_ID.ToString();
            obj.FSDEL = "N";
            client.QUERY_TBPGM_PROMOAsync(obj);
            LoadBusy();
        }

		//本人頻道
        private void btnChannel_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Class_PROMO obj = new Class_PROMO();

            obj.FSPROMO_NAME = tbxPROMONAME.Text.Trim();
            obj.FSMAIN_CHANNEL_ID = UserClass.userData.FSCHANNEL_ID.ToString();
            obj.FSDEL = "N";
            client.QUERY_TBPGM_PROMOAsync(obj);
            LoadBusy();
        } 

        //連點事件
        private void RowDoubleClick(object sender, DataGridDCTest.DataGridRowClickedArgs e)
        {
            DoubleClickDataGrid dcdg = sender as DoubleClickDataGrid;

            strPromoID_View = ((Class_PROMO)DGPROMOList.SelectedItem).FSPROMO_ID.ToString();
            strPromoName_View = ((Class_PROMO)DGPROMOList.SelectedItem).FSPROMO_NAME.ToString();
            strChannel_Id = ((Class_PROMO)DGPROMOList.SelectedItem).FSMAIN_CHANNEL_ID.ToString();
            strDEP_ID = ((Class_PROMO)DGPROMOList.SelectedItem).FNDEP_ID.ToString();
            //this.DialogResult = true;

            clientBro.GetPRODUCERAsync("P", strPromoID_View);    //選定短帶後，再去查詢短帶製作人
            LoadBusy();
        }

        //輸入完查詢內容後按下enter即可查詢
        private void tbxPROMONAME_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                btnSearch_Click(sender, e);
            }
        }		
       
    }
}

