﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSPROG_M;
using PTS_MAM3.ProgData;
using DataGridDCTest;

namespace PTS_MAM3.PRG
{
    public partial class PRG100_11 : ChildWindow
    {
        WSPROG_MSoapClient client = new WSPROG_MSoapClient(); //產生新的代理類別

        public string strPRDCENID_View = "";                //選取的製作單位編號
        public string strPRDCENName_View = "";              //選取的製作單位名稱

        public PRG100_11()
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面
        }

        void InitializeForm() //初始化本頁面
        {
            //查詢製作單位資料By製作單位名稱
            client.GetTBPGM_PRDCENTER_CODE_BYNAMECompleted += new EventHandler<GetTBPGM_PRDCENTER_CODE_BYNAMECompletedEventArgs>(client_GetTBPGM_PRDCENTER_CODE_BYNAMECompleted);
            client.GetTBPGM_PRDCENTER_CODE_BYNAMEAsync(""); //第一次傳空值查全部
            LoadBusy();
        }

        void LoadBusy()
        {
            BusyMsg.IsBusy = true;  //關閉忙碌的Loading訊息
            btnSearch.IsEnabled = false;
            btnAll.IsEnabled = false;
            tbxPRDCENNAME.IsEnabled = false;
        }

        void LoadBusyUnLock()
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息
            btnSearch.IsEnabled = true;
            btnAll.IsEnabled = true;
            tbxPRDCENNAME.IsEnabled = true;
            tbxPRDCENNAME.Focus();
        }

        //實作-製作單位資料By製作單位名稱
        void client_GetTBPGM_PRDCENTER_CODE_BYNAMECompleted(object sender, GetTBPGM_PRDCENTER_CODE_BYNAMECompletedEventArgs e)
        {
            LoadBusyUnLock();

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    this.DGPRDCENList.DataContext = e.Result;

                    if (this.DGPRDCENList.DataContext != null)
                    {
                        DGPRDCENList.SelectedIndex = 0;
                    }
                }
                else
                {
                    MessageBox.Show("查無製作單位資料，請重新輸入條件查詢！", "提示訊息", MessageBoxButton.OK);
                    return;
                }
            }  
        }        
           
        //確定
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGPRDCENList.SelectedItem == null)
            {
                MessageBox.Show("請選擇製作單位資料", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else
            {
                strPRDCENID_View = ((Class_CODE)DGPRDCENList.SelectedItem).ID.ToString();
                strPRDCENName_View = ((Class_CODE)DGPRDCENList.SelectedItem).NAME.ToString();            
                this.DialogResult = true;
            }
        }

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
       
        //查詢
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            if (tbxPRDCENNAME.Text.Trim() != "")
            {
                //查詢節目主檔資料
                client.GetTBPGM_PRDCENTER_CODE_BYNAMEAsync(tbxPRDCENNAME.Text.Trim());
                LoadBusy();
            }
            else
                MessageBox.Show("請輸入條件查詢！", "提示訊息", MessageBoxButton.OK);        
        }

        //重新查詢
        private void btnAll_Click(object sender, RoutedEventArgs e)
        {
            tbxPRDCENNAME.Text = "";

            //取得全部節目資料
            client.GetTBPGM_PRDCENTER_CODE_BYNAMEAsync("");
            LoadBusy();
        }

        //連點事件
        private void RowDoubleClick(object sender, DataGridDCTest.DataGridRowClickedArgs e)
        {
            DoubleClickDataGrid dcdg = sender as DoubleClickDataGrid;

            strPRDCENID_View = ((Class_CODE)DGPRDCENList.SelectedItem).ID.ToString();
            strPRDCENName_View = ((Class_CODE)DGPRDCENList.SelectedItem).NAME.ToString();
            this.DialogResult = true;
        }

        //輸入完查詢內容後按下enter即可查詢
        private void tbxPROMONAME_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                btnSearch_Click(sender, e);
            }
        }

    }
}

