﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Xml.Linq;
using System.IO;
using System.Text;
using PTS_MAM3.WSPROGMEN;
using PTS_MAM3.PROG_M;
using System.Windows.Data;

namespace PTS_MAM3.ProgData
{
    public partial class PROGMEN : Page
    {
        WSPROGMENSoapClient client = new WSPROGMENSoapClient();     //產生新的代理類別
        List<Class_CODE> m_CodeData = new List<Class_CODE>();       //代碼檔集合 
        string m_strProgName = "";                                  //暫存節目名稱 
        string m_strEpisode = "";                                   //暫存集別
        Class_PROGMEN m_ReuseQueryobj = new Class_PROGMEN();        //取得查詢頁面的查詢條件obj
        Int32 m_intPageCount = -1;                                  //紀錄PageIndex

        public PROGMEN()
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面
        }

        public PROGMEN(string strProgID)
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面

            m_ReuseQueryobj.FSPROG_ID = strProgID;
            m_ReuseQueryobj.FNEPISODE = 0;
            client.fnQUERY_TBPROGMENAsync(m_ReuseQueryobj);    //透過節目編號，取得全部節目子集資料(批次新增子集時呼叫)
            BusyMsg.IsBusy = true;
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        void InitializeForm() //初始化本頁面
        {
            //下載代碼檔
            client.fnGetTBPROGMEN_CODECompleted += new EventHandler<fnGetTBPROGMEN_CODECompletedEventArgs>(client_fnGetTBPROGMEN_CODECompleted);
            client.fnGetTBPROGMEN_CODEAsync();

            //取得全部相關人員記錄資料
            client.fnGetTBPROGMEN_ALLCompleted += new EventHandler<fnGetTBPROGMEN_ALLCompletedEventArgs>(client_fnGetTBPROGMEN_ALLCompleted);

            //刪除相關人員記錄資料
            client.fnDELETE_TBPROGMENCompleted += new EventHandler<fnDELETE_TBPROGMENCompletedEventArgs>(client_fnDELETE_TBPROGMENCompleted);

            //查詢相關人員資料
            client.fnQUERY_TBPROGMENCompleted += new EventHandler<fnQUERY_TBPROGMENCompletedEventArgs>(client_fnQUERY_TBPROGMENCompleted);
        }

        #region 實作

        //實作-取得全部相關人員資料
        void client_fnGetTBPROGMEN_ALLCompleted(object sender, fnGetTBPROGMEN_ALLCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    //將資料bind到DataPager，先註解
                    //this.DGPROG_MenList.DataContext = e.Result;

                    //if (this.DGPROG_MenList.DataContext != null)
                    //{
                    //    DGPROG_MenList.SelectedIndex = 0;
                    //}

                    //宣告PagedCollectionView,將撈取的資料放入
                    PagedCollectionView pcv = new PagedCollectionView(e.Result);
                    //指定DataGrid以及DataPager的內容
                    DGPROG_MenList.ItemsSource = pcv;
                    DataPager.Source = pcv;
                }
            } 
        }

        //實作-下載代碼檔
        void client_fnGetTBPROGMEN_CODECompleted(object sender, fnGetTBPROGMEN_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        m_CodeData.Add(e.Result[i]);
                    }
                }
            }

            //client.fnGetTBPROGMEN_ALLAsync();   //實作-取得全部相關人員資料      //一開始不Load全部資料  
        }

        //實作-刪除相關人員記錄資料
        void client_fnDELETE_TBPROGMENCompleted(object sender, fnDELETE_TBPROGMENCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result.bolResult == true)
                {
                    MessageBox.Show("刪除相關人員資料「" + m_strProgName.ToString() + "-" + m_strEpisode + "集」成功！", "提示訊息", MessageBoxButton.OK);
                    //client.fnGetTBPROGMEN_ALLAsync();  //更新相關人員  //先註解，因為不要Load全部 
                    client.fnQUERY_TBPROGMENAsync(m_ReuseQueryobj);    //如果有刪除資料，要用查詢條件去查詢
                }
                else
                {
                    MessageBox.Show("刪除相關人員資料「" + m_strProgName.ToString() + "-" + m_strEpisode + "集」失敗！\n失敗原因：" + e.Result.strErrorMsg, "提示訊息", MessageBoxButton.OK);
                }
            }
        }

        //實作-查詢相關人員資料
        void client_fnQUERY_TBPROGMENCompleted(object sender, fnQUERY_TBPROGMENCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)  //若是有查詢結果，則顯示，不然就請使用者重新查詢
                {
                    PagedCollectionView pcv = new PagedCollectionView(e.Result);
                    DGPROG_MenList.ItemsSource = pcv;
                    DataPager.Source = pcv;

                    if (m_intPageCount != -1)        //設定修改後查詢完要回到哪一頁
                        DataPager.PageIndex = m_intPageCount;
                }
                else
                {
                    PagedCollectionView pcv = new PagedCollectionView(new List<Class_PROGMEN>());
                    //指定DataGrid以及DataPager的內容
                    DGPROG_MenList.ItemsSource = pcv;
                    DataPager.Source = pcv;
                }
            }
        }

        #endregion

        //新增
        private void btnPROGAdd_Click(object sender, RoutedEventArgs e)
        {
            if (checkPermission() == false)
            {
                MessageBox.Show("無相關人員資料維護權限，無法新增", "提示訊息", MessageBoxButton.OK);
                return;
            }

            PROGMEN_Add PROGMEN_Add_frm = new PROGMEN_Add();
            PROGMEN_Add_frm.Show();

            PROGMEN_Add_frm.Closing += (s, args) =>
            {
                if (PROGMEN_Add_frm.DialogResult == true)
                {
                    m_ReuseQueryobj = new Class_PROGMEN();     //新增後，把查詢條件清空
                    m_ReuseQueryobj = PROGMEN_Add_frm.m_Queryobj;
                    client.fnQUERY_TBPROGMENAsync(m_ReuseQueryobj);
                    BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                }                                          
            };
        }  

        //修改
        private void btnPROGModify_Click(object sender, RoutedEventArgs e)
        {
            if (checkPermission() == false)
            {
                MessageBox.Show("無相關人員資料維護權限，無法修改", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //判斷是否有選取DataGrid
            if (DGPROG_MenList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預修改的相關人員資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            m_intPageCount = DataPager.PageIndex;       //取得PageIndex

            PROGMEN_Edit PROGMEN_Edit_frm = new PROGMEN_Edit(((Class_PROGMEN)DGPROG_MenList.SelectedItem));
            PROGMEN_Edit_frm.Show();

            PROGMEN_Edit_frm.Closing += (s, args) =>
            {
                if (PROGMEN_Edit_frm.DialogResult == true)
                {
                    if (m_ReuseQueryobj.FSPROG_ID == null)
                    {
                        client.fnQUERY_TBPROGMENAsync(PROGMEN_Edit_frm.m_Queryobj);    //如果不是就要用要用修改去查詢
                        BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                    }                        
                    else
                    {
                        client.fnQUERY_TBPROGMENAsync(m_ReuseQueryobj);                //如果先查詢再修改就要保留查詢的條件 
                        BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                    }                        
                }               
            };
        }

        //查詢
        private void btnPROGQuery_Click(object sender, RoutedEventArgs e)
        {
            PROGMEN_Query PROGMEN_Query_frm = new PROGMEN_Query();
            PROGMEN_Query_frm.Show();

            PROGMEN_Query_frm.Closed += (s, args) =>
            {
                if (PROGMEN_Query_frm.m_ListFormData.Count != 0)
                {
                    //將資料bind到DataPager，先註解
                    //this.DGPROG_MenList.DataContext = null;
                    //this.DGPROG_MenList.DataContext = PROGMEN_Query_frm.m_ListFormData;
                    //this.DGPROG_MenList.SelectedIndex = 0;

                    //宣告PagedCollectionView,將撈取的資料放入
                    PagedCollectionView pcv = new PagedCollectionView(PROGMEN_Query_frm.m_ListFormData);
                    //指定DataGrid以及DataPager的內容
                    DGPROG_MenList.ItemsSource = pcv;
                    DataPager.Source = pcv;

                    m_ReuseQueryobj = PROGMEN_Query_frm.m_Queryobj;  //紀錄查詢的條件
                }
            };
        }

        //刪除
        private void btnPROGDel_Click(object sender, RoutedEventArgs e)
        {
            string strMsg = "";

            if (checkPermission() == false)
            {
                MessageBox.Show("無相關人員資料維護權限，無法刪除", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //判斷是否有選取DataGrid
            if (DGPROG_MenList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預刪除的相關人員資料", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else
            {
                m_strProgName = ((Class_PROGMEN)DGPROG_MenList.SelectedItem).FSPROG_ID_NAME.ToString().Trim();
                m_strEpisode = ((Class_PROGMEN)DGPROG_MenList.SelectedItem).FNEPISODE.ToString().Trim();

                if (((Class_PROGMEN)DGPROG_MenList.SelectedItem).FSSTAFFID.Trim() == "")
                    strMsg = "確定要刪除「" + m_strProgName.ToString() + "-" + m_strEpisode + "集」?\n此筆相關人員資料刪除時間較久，請稍候";
                else
                    strMsg = "確定要刪除「" + m_strProgName.ToString() + "-" + m_strEpisode + "集」?";

                MessageBoxResult result = MessageBox.Show(strMsg, "提示訊息", MessageBoxButton.OKCancel);
                
                if (result == MessageBoxResult.OK)
                {
                    m_intPageCount = DataPager.PageIndex;       //取得PageIndex
                    client.fnDELETE_TBPROGMENAsync((Class_PROGMEN)DGPROG_MenList.SelectedItem, UserClass.userData.FSUSER_ID.ToString());
                    BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                }                    
            }
        }

        //檢視
        private void btnPROGView_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGPROG_MenList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預檢視的相關人員資料", "提示訊息", MessageBoxButton.OK);
                return;
            }           

            PRG400_03 PRG400_03_frm = new PRG400_03(((Class_PROGMEN)DGPROG_MenList.SelectedItem));
            PRG400_03_frm.Show();
        }

        //檢查權限
        private Boolean checkPermission()
        {
            if (ModuleClass.getModulePermissionByName("02", "相關人員資料維護") == false)
                return false;
            else
                return true;
        }


    }
}
