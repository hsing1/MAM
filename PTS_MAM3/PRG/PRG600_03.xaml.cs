﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSPROG_PRODUCER;
using System.Text;
using PTS_MAM3.PRG;

namespace PTS_MAM3.ProgData
{
    public partial class PRG600_03 : ChildWindow
    {
        WSPROG_PRODUCERSoapClient client = new WSPROG_PRODUCERSoapClient();     //產生新的代理類別
        string m_IDName = "";                                                   //暫存名稱 
        string m_strType = "";                                                  //類型
        string m_strID = "";                                                    //編碼
        string m_PRODUCERID = "";                                               //要修改的製作人
        int m_FNUpperDept_ID;                                                   //上層組織 
        int m_FNDep_ID;                        　                               //組織
        int m_FNGroup_ID;                                                       //組別
        String m_User;
        int IsPostback = 0;
        int SetUser = 0;
        Boolean refreshok = false;
        public Class_PROGPRODUCER m_Queryobj = new Class_PROGPRODUCER();        //查詢修改的obj
        Class_PROGPRODUCER m_FormData = new Class_PROGPRODUCER();
        List<UserStruct> ListUserAll = new List<UserStruct>();                  //所有的使用者資料

        private int intCbx1 = 0, intCbx2 = -1, intCbx3 = -1, intCbx4 = -1;

        List<MyDept> deptlist1 = new List<MyDept>();
        List<MyDept> deptlist2 = new List<MyDept>();
        List<MyDept> deptlist3 = new List<MyDept>();
        List<MyUser> userlist4 = new List<MyUser>();

        WSREPORT.WSREPORTSoapClient objReport1 = new WSREPORT.WSREPORTSoapClient();
        WSREPORT.WSREPORTSoapClient objReport2 = new WSREPORT.WSREPORTSoapClient();
        WSREPORT.WSREPORTSoapClient objReport3 = new WSREPORT.WSREPORTSoapClient();
        WSREPORT.WSREPORTSoapClient objReport4 = new WSREPORT.WSREPORTSoapClient();
        WSREPORT.WSREPORTSoapClient objRpt = new WSREPORT.WSREPORTSoapClient();

        public PRG600_03(Class_PROGPRODUCER FormData)
        {
            InitializeComponent();
            InitializeForm();          //初始化本頁面
            m_FormData = FormData;
            ClassToForm();
        }

        void InitializeForm() //初始化本頁面
        {           
            //將下拉式選單改成AutoComplete作法
            //objReport1.fnGetDeptListCompleted += new EventHandler<WSREPORT.fnGetDeptListCompletedEventArgs>(objReport1_fnGetDeptListCompleted);
            //objReport2.fnGetDeptListCompleted += new EventHandler<WSREPORT.fnGetDeptListCompletedEventArgs>(objReport2_fnGetDeptListCompleted);
            //objReport3.fnGetDeptListCompleted += new EventHandler<WSREPORT.fnGetDeptListCompletedEventArgs>(objReport3_fnGetDeptListCompleted);
            //objReport4.fnGetTBUSERS_BY_DEPT_IDCompleted += new EventHandler<WSREPORT.fnGetTBUSERS_BY_DEPT_IDCompletedEventArgs>(objReport4_fnGetTBUSERS_BY_DEPT_IDCompleted);
      
            //cbxDept1.SelectionChanged += new SelectionChangedEventHandler(cbxDept1_SelectionChanged);
            //cbxDept2.SelectionChanged += new SelectionChangedEventHandler(cbxDept2_SelectionChanged);
            //cbxDept3.SelectionChanged += new SelectionChangedEventHandler(cbxDept3_SelectionChanged);
            ////cbxUser.SelectionChanged += new SelectionChangedEventHandler(cbxUser_SelectionChanged);

            //objReport1.fnGetDeptListAsync(intCbx1);

            //查詢所有使用者資料
            client.QUERY_TBUSERS_ALLCompleted += new EventHandler<QUERY_TBUSERS_ALLCompletedEventArgs>(client_QUERY_TBUSERS_ALLCompleted);
            client.QUERY_TBUSERS_ALLAsync();
        }      

        private void ClassToForm()  //將前一頁點選的節目資料繫集到畫面上
        {        
            if (m_FormData.FSPRO_TYPE.Trim()=="G")
            {
                rdbProg.IsChecked = true ;
                m_strType = "G" ;
            }
            else if (m_FormData.FSPRO_TYPE.Trim()=="P")
            {
                rdbPromo.IsChecked = true;
                m_strType = "P" ;
            }

            m_strID = m_FormData.FSID.Trim();
            tbxPROG_NAME.Text = m_FormData.FSID_NAME.Trim() ;
            tbxPROG_NAME.Tag = m_FormData.FSID.Trim();

            //將下拉式選單改成AutoComplete作法
            //m_PRODUCERID = m_FormData.FSPRODUCER.Trim();
            //m_FNUpperDept_ID = m_FormData.FNUpperDept_ID;           //上層組織 
            //m_FNDep_ID = m_FormData.FNDep_ID;                       //組織
            //m_FNGroup_ID = m_FormData.FNGroup_ID;                   //組別
            //m_User = m_FormData.FSPRODUCER.Trim();

            //if (m_FNUpperDept_ID != 0) { SetUser += 1; }
            //if (m_FNDep_ID != 0 && m_FNDep_ID != 9999) { SetUser += 1; }
            //if (m_FNGroup_ID != 0 && m_FNDep_ID != 9999) { SetUser += 1; }
        }

        #region 實作                

        //實作-查詢所有使用者資料
        void client_QUERY_TBUSERS_ALLCompleted(object sender, QUERY_TBUSERS_ALLCompletedEventArgs e)
        {            

            if (e.Error == null)
            {
                ListUserAll = e.Result;
                //acbMenID.ItemsSource = ListUserAll; //繫結AutoComplete按鈕，因為是檢視，所以不用繫結

                for (int i = 0; i < ListUserAll.Count; i++)
                {
                    if (ListUserAll[i].FSUSER_ID.ToString().Trim().Equals(m_FormData.FSPRODUCER.Trim()))
                    {
                        acbMenID.Text = ListUserAll[i].FSUSER_ChtName.Trim();
                        tbxNote.Text = ListUserAll[i].FSTITLE_NAME.Trim();
                        break;
                    }
                }
            }
            else
                MessageBox.Show("查詢使用者資料異常！", "提示訊息", MessageBoxButton.OK);
        }

        #endregion

        #region 資料查詢結束

        void objReport1_fnGetDeptListCompleted(object sender, WSREPORT.fnGetDeptListCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                    cbxDept2.ItemsSource = null;
                    cbxDept3.ItemsSource = null;
                    cbxUser.ItemsSource = null;

                if (e.Result.Count == 0)
                {
                    MessageBox.Show("查詢單位時發生錯誤");
                    return;
                }
                else
                {
                    deptlist1.Clear();

                    foreach (WSREPORT.DeptStruct dept in e.Result)
                    {
                        deptlist1.Add(new MyDept(dept.FSDpeName_ChtName, dept.FNDEP_ID));
                    }

                    cbxDept1.ItemsSource = deptlist1;

                    if (m_FNUpperDept_ID != -1)
                    {
                        for (int i = 0; i < cbxDept1.Items.Count; i++)
                        {
                            if (((MyDept)cbxDept1.Items[i]).id.ToString().Trim().Equals(m_FNUpperDept_ID.ToString().Trim()))
                            {
                                cbxDept1.SelectedIndex = i;                           
                                break;
                            }
                        }                   
                    }
                }
            }
        }

        void objReport2_fnGetDeptListCompleted(object sender, WSREPORT.fnGetDeptListCompletedEventArgs e)
        {
            deptlist2.Clear();

            cbxDept2.ItemsSource = null;
            cbxDept3.ItemsSource = null;
            cbxUser.ItemsSource = null;

            if (e.Error == null)
            {
                if (e.Result.Count == 0)
                {
                    //MessageBox.Show("查不到子單位");
                    cbxDept3.ItemsSource = null;
                    return;
                }
                else
                {
                    if (intCbx2 != -1)
                    {
                        foreach (WSREPORT.DeptStruct dept in e.Result)
                        {
                            deptlist2.Add(new MyDept(dept.FSDpeName_ChtName, dept.FNDEP_ID));
                        }
                    }

                    cbxDept2.ItemsSource = deptlist2;

                    if (m_FNUpperDept_ID != -1)
                    {
                        for (int i = 0; i < cbxDept2.Items.Count; i++)
                        {
                            if (((MyDept)cbxDept2.Items[i]).id.ToString().Trim().Equals(m_FNDep_ID.ToString().Trim()))
                            {
                                cbxDept2.SelectedIndex = i;
                                m_FNUpperDept_ID = -1;                               
                                break;
                            }
                        }                       
                    }   
                
                }
            }
        }

        void objReport3_fnGetDeptListCompleted(object sender, WSREPORT.fnGetDeptListCompletedEventArgs e)
        {

            deptlist3.Clear();

            cbxDept3.ItemsSource = null;
            cbxUser.ItemsSource = null;

            if (e.Error == null)
            {
                if (e.Result.Count == 0)
                {
                    //MessageBox.Show("查不到子單位");
                    return;
                }
                else
                {
                    if (intCbx3 != -1)
                    {
                        foreach (WSREPORT.DeptStruct dept in e.Result)
                        {
                            deptlist3.Add(new MyDept(dept.FSDpeName_ChtName, dept.FNDEP_ID));
                        }
                    }

                    cbxDept3.ItemsSource = deptlist3;

                    if (m_FNDep_ID != -1)
                    {
                        for (int i = 0; i < cbxDept3.Items.Count; i++)
                        {
                            if (((MyDept)cbxDept3.Items[i]).id.ToString().Trim().Equals(m_FNGroup_ID.ToString().Trim()))
                            {
                                cbxDept3.SelectedIndex = i;
                                m_FNDep_ID = -1;                                
                                break;
                            }
                        }                      
                    } 
                }
            }
        }

        void objReport4_fnGetTBUSERS_BY_DEPT_IDCompleted(object sender, WSREPORT.fnGetTBUSERS_BY_DEPT_IDCompletedEventArgs e)
        {
            if (cbxDept2.SelectedIndex <= 0)
            {
                return;
            }
            else if (refreshok)
            {
                return;
            }
            else
            {
                refreshok = true;
            }

            userlist4.Clear();

            cbxUser.ItemsSource = null;
            if (e.Error == null)
            {
                if (e.Result.Count == 0)
                {
                    //MessageBox.Show("此單位查無人員清單");
                    return;
                }
                else
                {


                    //if (intCbx4 != -1)
                    //{
                        foreach (WSREPORT.UserStruct user in e.Result)
                        {
                            String _Name = user.FSUPPERDEPT_CHTNAME + " \\ " +
                                         ((user.FSDEP_CHTNAME.Trim() == "") ? "" : (user.FSDEP_CHTNAME.Trim() + " \\ ")) +
                                         ((user.FSGROUP_CHTNAME.Trim() == "") ? "" : (user.FSGROUP_CHTNAME.Trim() + " \\ ")) +
                                         user.FSUSER_ID + "-" + user.FSUSER_ChtName;

                            userlist4.Add(new MyUser(_Name, user.FSUSER_ID));
                        }
                    //}

                    cbxUser.ItemsSource = userlist4;

                    if (m_FNGroup_ID != -1)
                    {
                        for (int i = 0; i < cbxUser.Items.Count; i++)
                        {
                            if (((MyUser)cbxUser.Items[i]).id.ToString().Trim().Equals(m_User.Trim()))
                            {
                                cbxUser.SelectedIndex = i;
                                m_FNGroup_ID = -1;                               
                                break;
                            }
                        }                       
                    } 
                   
                }
            }
        }

        #endregion

        #region 下拉選單變更

        void cbxDept1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            refreshok = false;
                intCbx2 = ((MyDept)cbxDept1.SelectedItem).id;
                objReport2.fnGetDeptListAsync(intCbx2);
                if ((IsPostback <= 0 && SetUser ==1)|| IsPostback > 0)
                {
                    objReport4.fnGetTBUSERS_BY_DEPT_IDAsync(intCbx2.ToString());
                    IsPostback = 1;
                }           
        }

        void cbxDept2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            refreshok = false;   
                if (cbxDept2.Items.Count != 0)
                {
                    intCbx3 = ((MyDept)cbxDept2.SelectedItem).id;
                    objReport3.fnGetDeptListAsync(intCbx3);
                }
                else               
                    cbxDept3.ItemsSource = null;

                if ((IsPostback <= 0 && SetUser == 2) || IsPostback > 0)
                 {
                     objReport4.fnGetTBUSERS_BY_DEPT_IDAsync(intCbx3.ToString());
                     IsPostback = 1;
                 }            
        }

        void cbxDept3_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            refreshok = false;
                if (cbxDept3.Items.Count != 0)              
                    intCbx4 = ((MyDept)cbxDept3.SelectedItem).id;

                if ((IsPostback <= 0 && SetUser == 3) || IsPostback > 0)
                {
                    objReport4.fnGetTBUSERS_BY_DEPT_IDAsync(intCbx4.ToString());
                    IsPostback = 1;
                }
        }

        #endregion
               
        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        } 

    }
}

