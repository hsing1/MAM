﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using PTS_MAM3.PROG_M;
using PTS_MAM3.WSPROMO;
using PTS_MAM3.ProgData;

namespace PTS_MAM3.PRG
{
    public partial class PRG800_04 : ChildWindow
    {
        WSPROMOSoapClient client = new WSPROMOSoapClient();
        List<Class_CODE> m_CodeDataCateD = new List<Class_CODE>();          //代碼檔集合(短帶類型細項) 
        public List<Class_PROMO> m_ListFormData = new List<Class_PROMO>();  //記錄查詢到的短帶資料
        string m_strID = "";               //編碼
        string m_strEpisode = "";          //集別
        string m_ActID = "";               //活動代碼
        public Class_PROMO m_Queryobj = new Class_PROMO();                  //查詢條件的obj

        public PRG800_04()
        {
            InitializeComponent();
            InitializeForm();          //初始化本頁面
        }

        void InitializeForm() //初始化本頁面
        {          
            //下載代碼檔
            client.GetTBPGM_PROMO_CODECompleted += new EventHandler<GetTBPGM_PROMO_CODECompletedEventArgs>(client_GetTBPGM_PROMO_CODECompleted);
            client.GetTBPGM_PROMO_CODEAsync();

            //下載短帶類型細項代碼檔
            client.GetTBPGM_PROMO_CATD_CODECompleted += new EventHandler<GetTBPGM_PROMO_CATD_CODECompletedEventArgs>(client_GetTBPGM_PROMO_CATD_CODECompleted);
            client.GetTBPGM_PROMO_CATD_CODEAsync();

           //查詢短帶資料
            client.QUERY_TBPGM_PROMOCompleted += new EventHandler<QUERY_TBPGM_PROMOCompletedEventArgs>(client_QUERY_TBPGM_PROMOCompleted);

            BusyMsg.IsBusy = true;  
        }

        #region 實作

        //實作-下載短帶類型細項代碼檔
        void client_GetTBPGM_PROMO_CATD_CODECompleted(object sender, GetTBPGM_PROMO_CATD_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    //m_CodeDataBuyD = new List<Class_CODE>(e.Result);

                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        ComboBoxItem cbiPROMOCATD = new ComboBoxItem();
                        cbiPROMOCATD.Content = CodeData.NAME;
                        cbiPROMOCATD.Tag = CodeData.ID;
                        cbiPROMOCATD.DataContext = CodeData.IDD;
                        this.cbPROMOCATIDD.Items.Add(cbiPROMOCATD);   //先都產生在隱藏起來的細項代碼combobox裡                   
                        m_CodeDataCateD.Add(CodeData);
                    }
                }
            }
        }

        //實作-下載代碼檔
        void client_GetTBPGM_PROMO_CODECompleted(object sender, GetTBPGM_PROMO_CODECompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            case "TBZCHANNEL":        //頻道別代碼
                                ComboBoxItem cbiCHANNEL = new ComboBoxItem();
                                cbiCHANNEL.Content = CodeData.NAME;
                                cbiCHANNEL.Tag = CodeData.ID;
                                this.cbCHANNEL_ID.Items.Add(cbiCHANNEL);
                                break;
                            case "TBZPROGOBJ":       //製作目的代碼
                                ComboBoxItem cbiPROGOBJ = new ComboBoxItem();
                                cbiPROGOBJ.Content = CodeData.NAME;
                                cbiPROGOBJ.Tag = CodeData.ID;
                                this.cbPROGOBJID.Items.Add(cbiPROGOBJ);
                                break;
                            case "TBZPRDCENTER":      //製作單位代碼
                                ComboBoxItem cbiPRDCENTER = new ComboBoxItem();
                                cbiPRDCENTER.Content = CodeData.NAME;
                                cbiPRDCENTER.Tag = CodeData.ID;
                                this.cbPRDCENID.Items.Add(cbiPRDCENTER);
                                break;
                            case "TBZPROMOVER":      //版本代碼
                                ComboBoxItem cbiPROMOVER = new ComboBoxItem();
                                cbiPROMOVER.Content = CodeData.NAME;
                                cbiPROMOVER.Tag = CodeData.ID;
                                this.cbPROMOVERID.Items.Add(cbiPROMOVER);
                                break;
                            case "TBZPROMOTYPE":      //類別
                                ComboBoxItem cbiPROMOTYPE = new ComboBoxItem();
                                cbiPROMOTYPE.Content = CodeData.NAME;
                                cbiPROMOTYPE.Tag = CodeData.ID;
                                this.cbPROMOTYPEID.Items.Add(cbiPROMOTYPE);
                                break;
                            case "TBZPROMOCAT":       //類型代碼
                                ComboBoxItem cbiPROMOCAT = new ComboBoxItem();
                                cbiPROMOCAT.Content = CodeData.NAME;
                                cbiPROMOCAT.Tag = CodeData.ID;
                                this.cbPROMOCATID.Items.Add(cbiPROMOCAT);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
        
        //實作-查詢短帶資料
        void client_QUERY_TBPGM_PROMOCompleted(object sender, QUERY_TBPGM_PROMOCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)  //若是有查詢結果，則顯示，不然就請使用者重新查詢
                {
                    m_ListFormData = new List<Class_PROMO>(e.Result);  //把取回來的結果，加上型別定義，才可以塞回去    
                    this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("查無短帶資料，請重新輸入條件查詢！", "提示訊息", MessageBoxButton.OK);
                    return;
                }
            }
        }       

        #endregion

        #region 搬值

        private Class_PROMO FormToClass_PROMO()
        {
            Class_PROMO obj = new Class_PROMO();                   
          
            obj.FSPROMO_NAME = tbxPROMO_NAME.Text.ToString().Trim();                            //短帶名稱
            obj.FSPROMO_NAME_ENG = tbxFROMO_NAME_ENG.Text.ToString().Trim();                    //短帶外文名稱
            obj.FSDURATION = tbxDURATION.Text.ToString().Trim() ;                               //播出長度(秒)
            obj.FSPROG_ID = m_strID;                                                            //節目編號

           if (m_strEpisode.Trim() == "")                                                       //集別
               obj.FNEPISODE = 0;
            else
               obj.FNEPISODE = Convert.ToInt16(m_strEpisode.Trim());
          
           if (cbPROGOBJID.SelectedIndex != -1)                                                 //製作目的
               obj.FSPROGOBJID = ((ComboBoxItem)cbPROGOBJID.SelectedItem).Tag.ToString();
           else
               obj.FSPROGOBJID ="";

           if (tbxPRDCEN.Text.Trim() != "")                       //製作單位
               obj.FSPRDCENID = tbxPRDCEN.Tag.ToString();
           else
               obj.FSPRDCENID = "";

           obj.FSPRDYYMM = tbxPRDYYMM.Text.ToString().Trim() ;                                  //製作年月
         
           if (cbPROMOTYPEID.SelectedIndex != -1)                                               //類別
               obj.FSPROMOTYPEID =((ComboBoxItem)cbPROMOTYPEID.SelectedItem).Tag.ToString(); 
           else
               obj.FSPROMOTYPEID = "";

            obj.FSACTIONID = m_ActID;                                                           //活動
            
          if (cbPROMOCATID.SelectedIndex != -1)                                                 //類型大類
             obj.FSPROMOCATID = ((ComboBoxItem)cbPROMOCATID.SelectedItem).Tag.ToString();
           else
             obj.FSPROMOCATID = "" ;

          if (cbPROMOCATIDDS.SelectedIndex != -1)                                               //類型細類
              obj.FSPROMOCATDID = ((ComboBoxItem)cbPROMOCATIDDS.SelectedItem).Tag.ToString();
          else
              obj.FSPROMOCATDID ="";

          if (cbPROMOVERID.SelectedIndex != -1)                                                 //版本
              obj.FSPROMOVERID = ((ComboBoxItem)cbPROMOVERID.SelectedItem).Tag.ToString();
          else
              obj.FSPROMOVERID ="";

          obj.FSMEMO = tbxMEMO.Text.ToString().Trim();                                          //備註
          obj.FSMAIN_CHANNEL_ID = "";                                                           //頻道別(先不給查，塞空的)
          obj.FSWELFARE = tbxWelfare.Text.ToString().Trim();                                    //公益託播編號

          m_Queryobj = obj;

            return obj;
        }
        
        #endregion
        
        //確定
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            double douCheck;   //純粹為了判斷是否為數字型態之用  

            if (tbxDURATION.Text.Trim() != "")
            {
                if (double.TryParse(tbxDURATION.Text.Trim(), out douCheck) == false)
                {
                   MessageBox.Show("請檢查「播出長度」欄位必須為數值型態");
                }
                else if (tbxDURATION.Text.Trim().StartsWith("-"))
                {
                    MessageBox.Show("請檢查「播出長度」欄位必須為正整數");
                }
            }

            //查詢節目主檔資料
            client.QUERY_TBPGM_PROMOAsync(FormToClass_PROMO());   
        }

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //選擇短帶類型，要判斷顯示哪些短帶類型細項
        private void cbPROMOCATID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.cbPROMOCATIDDS.Items.Clear();     //使用前先清空
            string strCATID = ""; //外購類別大項
            ComboBox cb = sender as ComboBox;
            ComboBoxItem cbm = (ComboBoxItem)cb.SelectedItem;
            strCATID = cbm.Tag.ToString();

            for (int i = 0; i < cbPROMOCATIDD.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROMOCATIDD.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strCATID))
                {
                    ComboBoxItem cbiPROMOCATIDS = new ComboBoxItem();
                    cbiPROMOCATIDS.Content = getcbi.Content;
                    cbiPROMOCATIDS.Tag = getcbi.DataContext;
                    this.cbPROMOCATIDDS.Items.Add(cbiPROMOCATIDS);
                }
            }
        }

        //選取活動
        private void btnAct_Click(object sender, RoutedEventArgs e)
        {
            PRG800_08 PRG800_08_frm = new PRG800_08();
            PRG800_08_frm.Show();

            PRG800_08_frm.Closed += (s, args) =>
            {
                if (PRG800_08_frm.DialogResult == true)
                {
                    tbxACTIONID.Text = PRG800_08_frm.strActIDName_View;
                    tbxACTIONID.Tag = PRG800_08_frm.strActID_View;
                    m_ActID = PRG800_08_frm.strActID_View; 
                }
            };
        }

        //選取節目名稱
        private void btnPROG_Click(object sender, RoutedEventArgs e)
        {
            //PRG100_01
            PROGDATA_VIEW PROGDATA_VIEW_frm = new PROGDATA_VIEW();
            PROGDATA_VIEW_frm.Show();

            PROGDATA_VIEW_frm.Closed += (s, args) =>
            {
                if (PROGDATA_VIEW_frm.DialogResult == true)
                {
                    tbxPROG_NAME.Text = PROGDATA_VIEW_frm.strProgName_View;
                    tbxPROG_NAME.Tag = PROGDATA_VIEW_frm.strProgID_View;

                    tbxEPISODE.Text = "";
                    lblEPISODE_NAME.Content = "";
                    m_strEpisode = "0";                
                    m_strID = PROGDATA_VIEW_frm.strProgID_View;
                }
            };
        }

        //選取子集名稱
        private void btnEPISODE_Click(object sender, RoutedEventArgs e)
        {
            if (tbxPROG_NAME.Text.ToString().Trim() != "" && tbxPROG_NAME.Tag.ToString().Trim() != "")
            {
                PRG.PRG200_07 PRG200_07_frm = new PRG.PRG200_07(tbxPROG_NAME.Tag.ToString().Trim(), tbxPROG_NAME.Text.ToString().Trim());
                PRG200_07_frm.Show();

                PRG200_07_frm.Closed += (s, args) =>
                {
                    if (PRG200_07_frm.DialogResult == true)
                    {
                        m_strEpisode = PRG200_07_frm.m_strEpisode_View;
                        tbxEPISODE.Text = PRG200_07_frm.m_strEpisode_View;
                        lblEPISODE_NAME.Content = PRG200_07_frm.m_strProgDName_View;
                    }
                };
            }
            else
                MessageBox.Show("請先選擇節目！", "提示訊息", MessageBoxButton.OK);
        }

        //選取製作單位
        private void btnPRDCEN_Click(object sender, RoutedEventArgs e)
        {
            PRG100_11 PRG100_11_frm = new PRG100_11();
            PRG100_11_frm.Show();

            PRG100_11_frm.Closed += (s, args) =>
            {
                if (PRG100_11_frm.DialogResult == true)
                {
                    tbxPRDCEN.Text = PRG100_11_frm.strPRDCENName_View;
                    tbxPRDCEN.Tag = PRG100_11_frm.strPRDCENID_View;
                }
            };
        }

    }
}

