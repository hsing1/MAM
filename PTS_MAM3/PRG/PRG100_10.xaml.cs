﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSPROG_M;
using PTS_MAM3.WSPROG_MYSQL_IMPORT;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace PTS_MAM3.PRG
{
    public partial class PRG100_10 : ChildWindow
    {
        WSPROG_M.WSPROG_MSoapClient client = new WSPROG_MSoapClient();
        WSPROG_MYSQL_IMPORTSoapClient clientMySql = new WSPROG_MYSQL_IMPORTSoapClient();

        string m_ResultString = "";            
        string m_strProgName = "";          //節目名稱                          
        string m_strProgID = "";            //節目編號    
        int m_BEG_EPISODE ;
        int m_END_EPISODE ;
        int m_iforceUpdate = 0;
        Int64 MySQLProgID;
        string MySQLWhosInCharge = "";

        bool m_bolDoProgD = false;            //節目子集資料(判斷是否已執行過)
        bool m_bolDoProgMen = false;          //節目人員資料
        bool m_bolDoProgMusic = false;        //節目音樂資料

        public PRG100_10(Class_PROGM FormData)
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面
            
            m_strProgName = FormData.FSPGMNAME.Trim();     
            m_strProgID = FormData.FSPROG_ID.Trim();

            tbxtProgID.Text = m_strProgID;
            tbxPGMNAME.Text = m_strProgName;
        }

        void InitializeForm() //初始化本頁面
        {
            //檢查事件，原本的作法註解掉
            //client.chkAndImportFromMySQLCompleted += new EventHandler<WSPROG_M.chkAndImportFromMySQLCompletedEventArgs>(client_chkAndImportFromMySQLCompleted);

            //檢查MySql的資料主檔裡是否只有一筆相同的節目資料，以及子集資料
            clientMySql.checkProgName_ProgDCompleted += new EventHandler<checkProgName_ProgDCompletedEventArgs>(clientMySql_checkProgName_ProgDCompleted);

            //更新節目子集資料
            clientMySql.updateProgD_DATACompleted += new EventHandler<updateProgD_DATACompletedEventArgs>(clientMySql_updateProgD_DATACompleted);

            //更新子集工作人員資料
            clientMySql.DoProgMenCompleted += new EventHandler<DoProgMenCompletedEventArgs>(clientMySql_DoProgMenCompleted);

            //更新音樂資料
            clientMySql.DoProgMusicCompleted += new EventHandler<DoProgMusicCompletedEventArgs>(clientMySql_DoProgMusicCompleted);
        }       

        #region 實作

        //原本的作法    
        void client_chkAndImportFromMySQLCompleted(object sender, WSPROG_M.chkAndImportFromMySQLCompletedEventArgs e)
        {
            

            m_ResultString = e.Result;
            if (m_ResultString == "")
            {
                //回傳空字串者,表示處理完畢並成功.
                MessageBox.Show("資料批次轉入完畢!!");
                this.DialogResult = true;
            }
            else if (m_ResultString.StartsWith("ERROR:"))
            {
                //回傳開頭為"ERROR:"字串者,表示處理過程發生錯誤
                MessageBox.Show("轉入過程發生錯誤: " + m_ResultString.Substring(6), "匯入失敗", MessageBoxButton.OK);
                this.DialogResult = false;
            }
            else
            {
                //回傳開頭非"ERROR:"字串者,表示要求使用者進一步確認後再往下一步go
                MessageBoxResult result = MessageBox.Show(m_ResultString, "提示訊息", MessageBoxButton.OKCancel);

                if (result == MessageBoxResult.OK)
                {
                    m_iforceUpdate += 1;
                    client.chkAndImportFromMySQLAsync(m_strProgID, m_strProgName, m_BEG_EPISODE, m_END_EPISODE, m_iforceUpdate,UserClass.userData.FSUSER_ID);
                    BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                    return;
                }
                else
                {
                    MessageBox.Show("您已取消資料批次轉入動作。", "取消匯入", MessageBoxButton.OK);
                    this.DialogResult = true;
                }
                BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息
            }
        }

        //實作-檢查MySql的資料主檔裡是否只有一筆相同的節目資料，以及子集資料
        void clientMySql_checkProgName_ProgDCompleted(object sender, checkProgName_ProgDCompletedEventArgs e)
        {
            

            if (e.Error == null)
            {
                ReturnMySqlMsg obj = new ReturnMySqlMsg();
                obj = e.Result;

                if (obj.bolResult == false)
                    MessageBox.Show("批次轉入失敗，原因：" + e.Result.strMsg.ToString().Trim(), "提示訊息", MessageBoxButton.OK);
                else
                {
                    MySQLProgID = obj.intMySQLProgID  ;                    
                    checkDo();       //檢查資料正確，繼續往下作
                    return;
                }
            }
            else
                MessageBox.Show("批次轉入失敗，原因：檢查資料主檔裡是否只有一筆相同的節目資料，以及子集資料異常", "提示訊息", MessageBoxButton.OK);
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息
        }

        //實作-更新節目子集資料
        void clientMySql_updateProgD_DATACompleted(object sender, updateProgD_DATACompletedEventArgs e)
        {
           

            if (e.Error == null)
            {
                if (e.Result == false)
                    MessageBox.Show("批次轉入子集失敗", "提示訊息", MessageBoxButton.OK);
                else
                {
                    m_bolDoProgD = true;    //更新節目子集資料成功，繼續往下作
                    checkDo();
                    return;
                }
            }
            else
                MessageBox.Show("批次轉入子集失敗", "提示訊息", MessageBoxButton.OK);

            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息
        }

        //更新子集工作人員資料
        void clientMySql_DoProgMenCompleted(object sender, DoProgMenCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息


            if (e.Error == null)
            {
                ReturnMySqlMsg obj = new ReturnMySqlMsg();
                obj = e.Result;

                if (obj.bolResult == false && obj.bolQuery == false)
                {
                    MessageBox.Show("批次轉入工作人員資料失敗，原因：" + e.Result.strMsg.ToString().Trim(), "提示訊息", MessageBoxButton.OK);
                    return;
                }
                else if (obj.bolResult == false && obj.bolQuery == true)
                {
                    //要求使用者進一步確認後再往下一步
                    MessageBoxResult result = MessageBox.Show(obj.strMsg, "提示訊息", MessageBoxButton.OKCancel);

                    if (result == MessageBoxResult.OK)
                    {
                        //因為已確認，所以最後一個參數要給True
                        clientMySql.DoProgMenAsync(MySQLProgID, m_strProgID, m_strProgName, m_BEG_EPISODE, m_END_EPISODE, MySQLWhosInCharge, true,UserClass.userData.FSUSER_ID);
                        BusyMsg.IsBusy = true;

                    }
                    else
                    {
                        MessageBox.Show("您已取消資料批次轉入動作。", "取消匯入", MessageBoxButton.OK);                        
                    }
                }
                else if (obj.bolResult == true)
                {
                    m_bolDoProgMen = true;    //更新工作人員資料成功，繼續往下作
                    checkDo();
                  
                }
                else
                    MessageBox.Show("批次轉入工作人員資料失敗", "提示訊息", MessageBoxButton.OK);
            }
            else
                MessageBox.Show("批次轉入工作人員資料失敗", "提示訊息", MessageBoxButton.OK);

        }

        //實作-更新音樂資料
        void clientMySql_DoProgMusicCompleted(object sender, DoProgMusicCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息


            if (e.Error == null)
            {
                ReturnMySqlMsg obj = new ReturnMySqlMsg();
                obj = e.Result;

                if (obj.bolResult == false && obj.bolQuery == false)
                {
                    MessageBox.Show("批次轉入音樂資料失敗，原因：" + e.Result.strMsg.ToString().Trim(), "提示訊息", MessageBoxButton.OK);
                    return;
                }
                else if (obj.bolResult == false && obj.bolQuery == true)
                {
                    //要求使用者進一步確認後再往下一步
                    MessageBoxResult result = MessageBox.Show(obj.strMsg, "提示訊息", MessageBoxButton.OKCancel);

                    if (result == MessageBoxResult.OK)
                    {
                        //因為已確認，所以最後一個參數要給True
                        clientMySql.DoProgMusicAsync(MySQLProgID, m_strProgID, m_strProgName, m_BEG_EPISODE, m_END_EPISODE, true);
                        BusyMsg.IsBusy = true;
                        
                    }
                    else
                    {
                        MessageBox.Show("您已取消資料批次轉入動作。", "取消匯入", MessageBoxButton.OK);
                    }
                }
                else if (obj.bolResult == true)
                {
                    m_bolDoProgMusic = true;    //更新音樂資料成功，繼續往下作
                    checkDo();
                   
                }
                else
                    MessageBox.Show("批次轉入音樂資料失敗", "提示訊息", MessageBoxButton.OK);
            }
            else
                MessageBox.Show("批次轉入音樂資料失敗", "提示訊息", MessageBoxButton.OK);
            
        }

        #endregion

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {            
            if (CheckData() == true)
            {
                m_ResultString = "";
                m_strProgID = tbxtProgID.Text.Trim();
                m_strProgName = tbxPGMNAME.Text.Trim(); 
                m_BEG_EPISODE = int.Parse(tbxBEG_EPISODE.Text.Trim());
                m_END_EPISODE = int.Parse(tbxEND_EPISODE.Text.Trim());
                m_iforceUpdate = 0;
                
                //判斷是否已執行過該步驟
                m_bolDoProgD = false;
                m_bolDoProgMen = false;
                m_bolDoProgMusic = false;

                MessageBoxResult result = MessageBox.Show("確定要轉入「第" + tbxBEG_EPISODE.Text.Trim() + "集~第" + tbxEND_EPISODE.Text.Trim() + "集」資料嗎?\n若是集數較多，轉入需要的時間會較久！", "提示訊息", MessageBoxButton.OKCancel);

                if (result == MessageBoxResult.OK)
                {
                    clientMySql.checkProgName_ProgDAsync(m_strProgID, m_strProgName, m_BEG_EPISODE, m_END_EPISODE);
                    BusyMsg.IsBusy = true;  //關閉忙碌的Loading訊息 
                }
                else
                    MessageBox.Show("您已取消資料批次轉入動作。", "取消匯入", MessageBoxButton.OK);

                ////回傳開頭非"ERROR:"字串者,表示要求使用者進一步確認後再往下一步go
                //MessageBoxResult result = MessageBox.Show("確定要轉入「第" + tbxBEG_EPISODE.Text.Trim() + "集~第" + tbxEND_EPISODE.Text.Trim() + "集」資料嗎?\n若是集數較多，轉入需要的時間會較久！", "提示訊息", MessageBoxButton.OKCancel);

                //if (result == MessageBoxResult.OK)
                //{
                //    client.chkAndImportFromMySQLAsync(m_strProgID, m_strProgName, m_BEG_EPISODE, m_END_EPISODE, m_iforceUpdate);
                //    BusyMsg.IsBusy = true;  //關閉忙碌的Loading訊息 
                //}
                //else
                //    MessageBox.Show("您已取消資料批次轉入動作。", "取消匯入", MessageBoxButton.OK);
            }             
        }

        private Boolean CheckData()
        {
            short shortCheck1 = -1, shortCheck2 = -1;
            StringBuilder strMessage = new StringBuilder();

            if (tbxBEG_EPISODE.Text.Trim() != "")
            {
                if (short.TryParse(tbxBEG_EPISODE.Text.Trim(), out shortCheck1) == false)
                {
                    strMessage.AppendLine("請檢查「集數(起)」欄位必須為整數數值型態");
                }
                else if (shortCheck1 <= 0)
                {
                    strMessage.AppendLine("請檢查「集數(起)」欄位必須>0");
                }
            }else{
                strMessage.AppendLine("請檢查「集數(起)」欄位必須輸入值");
            }

            if (tbxEND_EPISODE.Text.Trim() != "")
            {
                if (short.TryParse(tbxEND_EPISODE.Text.Trim(), out shortCheck2) == false)
                {
                    strMessage.AppendLine("請檢查「集數(迄)」欄位必須為整數數值型態");
                }
                else if (shortCheck2 <= 0)
                {
                    strMessage.AppendLine("請檢查「集數(迄)」欄位必須>0");
                }
            }
            else
            {
                strMessage.AppendLine("請檢查「集數(迄)」欄位必須輸入值");
            }

            if ((shortCheck1 >= 0) && (shortCheck2 >= 0) && (shortCheck1 > shortCheck2))
            {
                strMessage.AppendLine("請檢查「集數(起)」欄位必須小於或等於「集數(迄)」");
            }

            if (cbProgD.IsChecked == false && cbMen.IsChecked == false && cbMusic.IsChecked == false)
                strMessage.AppendLine("請勾選「類型」");

            if (strMessage.ToString().Trim() == "")
                return true;
            else
            {
                MessageBoxResult result = MessageBox.Show(strMessage.ToString(), "提示訊息", MessageBoxButton.OK);
                return false;
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //判斷接下來要進行哪一個類型
        private void checkDo()
        {
            BusyMsg.IsBusy = true;

            if (cbProgD.IsChecked == true && m_bolDoProgD == false)
                clientMySql.updateProgD_DATAAsync(MySQLProgID, m_strProgID, m_BEG_EPISODE, m_END_EPISODE,UserClass.userData.FSUSER_ID);
            else if (cbMen.IsChecked == true && m_bolDoProgMen == false)
                clientMySql.DoProgMenAsync(MySQLProgID, m_strProgID, m_strProgName, m_BEG_EPISODE, m_END_EPISODE, MySQLWhosInCharge, false, UserClass.userData.FSUSER_ID);
            else if (cbMusic.IsChecked == true && m_bolDoProgMusic == false)
                clientMySql.DoProgMusicAsync(MySQLProgID, m_strProgID, m_strProgName, m_BEG_EPISODE, m_END_EPISODE, false);
            else
            {
                tbxBEG_EPISODE.Text = "";
                tbxEND_EPISODE.Text = "";
                cbProgD.IsChecked = false;
                cbMen.IsChecked = false;
                cbMusic.IsChecked = false;
                MessageBox.Show("批次轉入資料成功", "提示訊息", MessageBoxButton.OK);
                this.DialogResult = true;
                BusyMsg.IsBusy = false;
            }
        }

    }
}

