﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using PTS_MAM3.WSPROG_M;
using PTS_MAM3.WSPROG_D;
using PTS_MAM3.ProgData;
using System.Windows.Data;

namespace PTS_MAM3.ProgData
{
    public partial class PROG_D : Page
    {
        WSPROG_DSoapClient client = new WSPROG_DSoapClient();       //產生新的代理類別
        Class_PROGD m_ReuseQueryobj = new Class_PROGD();            //取得查詢頁面的查詢條件obj
        Int32 m_intPageCount = -1;                                  //紀錄PageIndex
        string m_strProgName = "";                                  //暫存節目名稱
        string m_strProgID = "";                                    //暫存節目編號
        string m_strEpisode = "";                                   //暫存集別
        Class_PROGD m_Deleteobj = new Class_PROGD();                //刪除的暫存obj

        public PROG_D()
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面
        }

        public PROG_D(string strProgID)
        {
            InitializeComponent();
            InitializeForm();                                   //初始化本頁面
            client.GetTBPROG_D_BYPROGID_ALLAsync(strProgID);    //透過節目編號，取得全部節目子集資料(批次新增子集時呼叫)
            BusyMsg.IsBusy = true;
            m_ReuseQueryobj.FSPROG_ID = strProgID;
        }

        void InitializeForm() //初始化本頁面
        {
            //取得全部子集資料
            client.GetTBPROG_D_ALLCompleted += new EventHandler<GetTBPROG_D_ALLCompletedEventArgs>(client_GetTBPROG_D_ALLCompleted);
            //client.GetTBPROG_D_ALLAsync();  //一開始不Load全部資料

            //查詢節目子集資料
            client.QUERY_TBPROG_DCompleted += new EventHandler<QUERY_TBPROG_DCompletedEventArgs>(client_QUERY_TBPROG_DCompleted);  
   
            //刪除節目子集資料
            client.DELETE_TBPROGDCompleted += new EventHandler<DELETE_TBPROGDCompletedEventArgs>(client_DELETE_TBPROGDCompleted);

            //檢查刪除節目子集資料
            client.DELETE_TBPROGD_CheckCompleted += new EventHandler<DELETE_TBPROGD_CheckCompletedEventArgs>(client_DELETE_TBPROGD_CheckCompleted);

            //透過節目編號，取得全部節目子集資料
            client.GetTBPROG_D_BYPROGID_ALLCompleted += new EventHandler<GetTBPROG_D_BYPROGID_ALLCompletedEventArgs>(client_GetTBPROG_D_BYPROGID_ALLCompleted);            
        }

        #region 實作

        //實作-取得全部子集資料
        void client_GetTBPROG_D_ALLCompleted(object sender, GetTBPROG_D_ALLCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    //將資料bind到DataPager，先註解
                    //this.DGPROG_DList.DataContext = e.Result;

                    //if (this.DGPROG_DList.DataContext != null)
                    //{
                    //    DGPROG_DList.SelectedIndex = 0;
                    //}

                    //宣告PagedCollectionView,將撈取的資料放入
                    PagedCollectionView pcv = new PagedCollectionView(e.Result);
                    //指定DataGrid以及DataPager的內容
                    DGPROG_DList.ItemsSource = pcv;
                    DataPager.Source = pcv;
                }
            }             
        }

        //實作-查詢節目子集資料
        void client_QUERY_TBPROG_DCompleted(object sender, QUERY_TBPROG_DCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)  //若是有查詢結果，則顯示
                {
                    PagedCollectionView pcv = new PagedCollectionView(e.Result);
                    DGPROG_DList.ItemsSource = pcv;
                    DataPager.Source = pcv;

                    if (m_intPageCount != -1)        //設定修改後查詢完要回到哪一頁
                        DataPager.PageIndex = m_intPageCount;
                }
                else
                {
                    PagedCollectionView pcv = new PagedCollectionView(new List<Class_PROGD>());
                    //指定DataGrid以及DataPager的內容
                    DGPROG_DList.ItemsSource = pcv;
                    DataPager.Source = pcv;
                }
            }
        }

        //實作-刪除節目子集資料
        void client_DELETE_TBPROGDCompleted(object sender, DELETE_TBPROGDCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result.bolResult == true)
                {
                    MessageBox.Show("刪除子集資料「" + m_strProgName + "-" + m_strEpisode + "集」成功！", "提示訊息", MessageBoxButton.OK);
                    client.QUERY_TBPROG_DAsync(m_ReuseQueryobj);    //如果有刪除節目資料，要用查詢條件去查詢
                }
                else
                {
                    MessageBox.Show("刪除子集資料「" + m_strProgName + "-" + m_strEpisode + "集」失敗！\n失敗原因：" + e.Result.strErrorMsg, "提示訊息", MessageBoxButton.OK);
                }
            }
        }

        //實作-檢查刪除節目子集資料
        void client_DELETE_TBPROGD_CheckCompleted(object sender, DELETE_TBPROGD_CheckCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)  //若是有查詢結果，則顯示
                {
                    PRG.PRG100_09 PRG100_09_frm = new PRG.PRG100_09(e.Result);  //, "PROGD"
                    PRG100_09_frm.Show();
                }
                else
                {
                    //刪除子集資料
                    client.DELETE_TBPROGDAsync(((Class_PROGD)DGPROG_DList.SelectedItem), UserClass.userData.FSUSER_ID.ToString());
                }
            }
        }

        //透過節目編號，取得全部節目子集資料
        void client_GetTBPROG_D_BYPROGID_ALLCompleted(object sender, GetTBPROG_D_BYPROGID_ALLCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null )
                {                   
                    //宣告PagedCollectionView,將撈取的資料放入
                    PagedCollectionView pcv = new PagedCollectionView(e.Result);
                    //指定DataGrid以及DataPager的內容
                    DGPROG_DList.ItemsSource = pcv;
                    DataPager.Source = pcv;
                }
            }   
        }

        #endregion

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }
               
        //修改
        private void btnPROGModify_Click(object sender, RoutedEventArgs e)
        {
            if (checkPermission() == false)
            {
                MessageBox.Show("無節目子集資料維護權限，無法修改", "提示訊息", MessageBoxButton.OK);
                return;
            }
            
            //判斷是否有選取DataGrid
            if (DGPROG_DList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預修改的子集資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //判斷是否有選取DataGrid
            if (((Class_PROGD)DGPROG_DList.SelectedItem).FSCHANNEL_ID.Trim() == "")
            {
                MessageBox.Show("無法修改，因為該節目尚未進行認定基本資料，請先認定基本資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            m_intPageCount = DataPager.PageIndex;       //取得PageIndex

            PROGDDATA_Edit PROGDDATA_Edit_frm = new PROGDDATA_Edit(((Class_PROGD)DGPROG_DList.SelectedItem));
            PROGDDATA_Edit_frm.Show();

            PROGDDATA_Edit_frm.Closing += (s, args) =>
            {
                if (PROGDDATA_Edit_frm.DialogResult == true)
                {
                    //    client.GetTBPROG_D_ALLAsync();    //先註解，因為不要Load全部
                    client.QUERY_TBPROG_DAsync(m_ReuseQueryobj);
                    BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                }
            };
        }

        //整批修改
        private void btnPROGModify_All_Click(object sender, RoutedEventArgs e)
        {
            if (checkPermission() == false)
            {
                MessageBox.Show("無節目子集資料維護權限，無法修改", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //判斷是否有選取DataGrid
            if (DGPROG_DList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預修改的子集資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //判斷是否有選取DataGrid
            if (((Class_PROGD)DGPROG_DList.SelectedItem).FSCHANNEL_ID.Trim() == "")
            {
                MessageBox.Show("無法修改，因為該節目尚未進行認定基本資料，請先認定基本資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            m_intPageCount = DataPager.PageIndex;       //取得PageIndex

            PRG200_09 PRG200_09_frm = new PRG200_09(((Class_PROGD)DGPROG_DList.SelectedItem));
            PRG200_09_frm.Show();

            PRG200_09_frm.Closing += (s, args) =>
            {
                if (PRG200_09_frm.DialogResult == true)
                {                   
                    client.QUERY_TBPROG_DAsync(m_ReuseQueryobj);
                    BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                }
            };
        }

        //查詢
        private void btnPROGQuery_Click(object sender, RoutedEventArgs e)
        {
            PROGDDATA_Query PROGDDATA_Query_frm = new PROGDDATA_Query();
            PROGDDATA_Query_frm.Show();

            PROGDDATA_Query_frm.Closed += (s, args) =>
            {
                if (PROGDDATA_Query_frm.m_ListFormData.Count != 0)
                {
                    //將資料bind到DataPager，先註解
                    //this.DGPROG_DList.DataContext = null;
                    //this.DGPROG_DList.DataContext = PROGDDATA_Query_frm.m_ListFormData;
                    //this.DGPROG_DList.SelectedIndex = 0;

                    //宣告PagedCollectionView,將撈取的資料放入
                    PagedCollectionView pcv = new PagedCollectionView(PROGDDATA_Query_frm.m_ListFormData);
                    //指定DataGrid以及DataPager的內容
                    DGPROG_DList.ItemsSource = pcv;                    
                    DataPager.Source = pcv;

                    m_ReuseQueryobj = PROGDDATA_Query_frm.m_Queryobj;  //紀錄查詢的條件
                }
            };
        }
        
        //刪除
        private void btnPROGDelete_Click(object sender, RoutedEventArgs e)
        {
            if (checkPermission() == false)
            {
                MessageBox.Show("無節目子集資料維護權限，無法刪除", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //判斷是否有選取DataGrid
            if (DGPROG_DList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預刪除的子集資料", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else
            {                
                m_strProgName = ((Class_PROGD)DGPROG_DList.SelectedItem).FSPROG_ID_NAME.ToString().Trim();
                m_strProgID = ((Class_PROGD)DGPROG_DList.SelectedItem).FSPROG_ID.ToString().Trim();
                m_strEpisode = ((Class_PROGD)DGPROG_DList.SelectedItem).FNEPISODE.ToString().Trim();

                MessageBoxResult result = MessageBox.Show("確定要刪除「" +  m_strProgName + "-" + m_strEpisode + "集」?", "提示訊息", MessageBoxButton.OKCancel);

                if (result == MessageBoxResult.OK)
                {
                    m_intPageCount = DataPager.PageIndex;       //取得PageIndex

                    //判斷是否可刪除子集資料
                    m_Deleteobj = ((Class_PROGD)DGPROG_DList.SelectedItem);

                    //判斷刪除基本資料要依序檢查各資料表
                    client.DELETE_TBPROGD_CheckAsync(m_Deleteobj, UserClass.userData.FSUSER_ID.ToString());
                    BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                }
            }
        }

        //檢視子集資料
        private void btnView_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGPROG_DList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預檢視的子集資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            PRG200_03 PRG200_03_frm = new PRG200_03(((Class_PROGD)DGPROG_DList.SelectedItem));
            PRG200_03_frm.Show();
        }

        //檢查權限
        private Boolean checkPermission()
        {
            if (ModuleClass.getModulePermissionByName("02", "節目子集資料維護") == false)
                return false;
            else
                return true;            
        }

    }
}
