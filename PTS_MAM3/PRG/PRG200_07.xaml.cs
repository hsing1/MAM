﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSPROG_D;
using System.Xml.Linq;
using System.IO;
using System.Text;
using DataGridDCTest;

namespace PTS_MAM3.PRG
{
    public partial class PRG200_07 : ChildWindow  //查詢節目子集資料
    {
        WSPROG_DSoapClient client = new WSPROG_DSoapClient();  //產生新的代理類別

        public string m_strProgID = "";            //帶入的節目編號
        public string m_strProgName = "";          //帶入的節目名稱
        public string m_strEpisode_View = "";       //選取的集別
        public string m_strProgDName_View = "";     //選取的子集名稱
        public string m_strPlayEpisode_View = "";   //選取的播出集別
        public List<string> ListCheck = new List<string>(); 

        public PRG200_07(string ProgID ,string ProgName)
        {
            InitializeComponent();
            m_strProgID=ProgID.ToString().Trim();
            m_strProgName = ProgName.ToString().Trim();

            InitializeForm();        //初始化本頁面
        }

        void InitializeForm() //初始化本頁面
        {
            //取得全部節目子集資料
            client.GetTBPROG_D_BYPROGID_ALLCompleted += new EventHandler<GetTBPROG_D_BYPROGID_ALLCompletedEventArgs>(client_GetTBPROG_D_BYPROGID_ALLCompleted);
            //client.GetTBPROG_D_BYPROGID_ALLAsync(m_strProgID); //一開始不要LOAD全部太多了8/27
            //BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息

            //查詢子集資料By集別和節目名稱 
            client.QProg_D_BYEPISODE_PGDNAMECompleted += new EventHandler<QProg_D_BYEPISODE_PGDNAMECompletedEventArgs>(client_fnQProg_D_BYEPISODE_PGDNAMECompleted);

            //修改子集資料 有含 區間判別
            client.QProg_D_BYEPISODE_PGDNAME_WITH_INTERVALCompleted += new EventHandler<QProg_D_BYEPISODE_PGDNAME_WITH_INTERVALCompletedEventArgs>(client_QProg_D_BYEPISODE_PGDNAME_WITH_INTERVALCompleted);
        }
        //實作-查詢子集資料By集別和節目名稱 包含區間判別 完成處理常式 
        void client_QProg_D_BYEPISODE_PGDNAME_WITH_INTERVALCompleted(object sender, QProg_D_BYEPISODE_PGDNAME_WITH_INTERVALCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    this.DGPROG_DListD.DataContext = e.Result;

                    if (this.DGPROG_DListD.DataContext != null)
                    {
                        DGPROG_DListD.SelectedIndex = 0;
                    }
                }
                else
                {
                    MessageBox.Show("該節目查無節目子集資料，請重新輸入查詢條件！", "提示訊息", MessageBoxButton.OK);
                    return;
                }
            }
        }

        //實作-查詢子集資料By集別和節目名稱 
        void client_fnQProg_D_BYEPISODE_PGDNAMECompleted(object sender, QProg_D_BYEPISODE_PGDNAMECompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    this.DGPROG_DListD.DataContext = e.Result;

                    if (this.DGPROG_DListD.DataContext != null)
                    {
                        DGPROG_DListD.SelectedIndex = 0;
                    }
                }
                else
                {
                    MessageBox.Show("該節目查無節目子集資料，請重新輸入查詢條件！", "提示訊息", MessageBoxButton.OK);
                    return;
                }
            }
        }

        //實作-取得全部節目子集資料
        void client_GetTBPROG_D_BYPROGID_ALLCompleted(object sender, GetTBPROG_D_BYPROGID_ALLCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    this.DGPROG_DListD.DataContext = e.Result;

                    if (this.DGPROG_DListD.DataContext != null)
                    {
                        DGPROG_DListD.SelectedIndex = 0;
                    }
                }
                else
                {
                    MessageBox.Show("該節目查無節目子集資料，請重新選擇節目名稱！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
        }
        
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            ////判斷是否有選取DataGrid
            //if (DGPROG_DList.SelectedItem == null)
            //{
            //    MessageBox.Show("請選擇節目子集資料", "提示訊息", MessageBoxButton.OK);
            //    return;
            //}
            //else
            //{
            //    m_strEpisode_View = ((Class_PROGD)DGPROG_DList.SelectedItem).FNEPISODE.ToString();
            //    m_strProgDName_View = ((Class_PROGD)DGPROG_DList.SelectedItem).FSPGDNAME.ToString();

            //    this.DialogResult = true;
            //}

            //判斷是否有選取DataGrid
            if (DGPROG_DListD.SelectedItem == null)
            {
                MessageBox.Show("請選擇節目子集資料", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else
            {
                m_strEpisode_View = ((Class_PROGD)DGPROG_DListD.SelectedItem).FNEPISODE.ToString();
                m_strProgDName_View = ((Class_PROGD)DGPROG_DListD.SelectedItem).FSPGDNAME.ToString();
                m_strPlayEpisode_View = ((Class_PROGD)DGPROG_DListD.SelectedItem).FNSHOWEPISODE.ToString();
                this.DialogResult = true;
            }   
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //條件查詢
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            short shortCheck;   //純粹為了判斷是否為數字型態之用
            string strEpisode = tbxEPISODE.Text.ToString().Trim() ;
            string strProgDName=tbxEPISODENAME.Text.ToString().Trim();

            if (radioButton1.IsChecked == true)
            {
                if (short.TryParse(strEpisode, out shortCheck) == false)
                    MessageBox.Show("請檢查「集別」欄位必須為數值型態", "提示訊息", MessageBoxButton.OK);
                else
                {
                    client.QProg_D_BYEPISODE_PGDNAMEAsync(m_strProgID, strEpisode, strProgDName);
                    BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                }
            }
            else if (radioButton2.IsChecked == true)
            {
                if (short.TryParse(tbStart.Text.Trim(), out shortCheck) == false || short.TryParse(tbEnd.Text.Trim(), out shortCheck) == false )
                    MessageBox.Show("請檢查「集別區間」欄位必須為數值型態", "提示訊息", MessageBoxButton.OK);
                else
                {
                    client.QProg_D_BYEPISODE_PGDNAME_WITH_INTERVALAsync(m_strProgID, strEpisode, strProgDName, tbStart.Text.Trim(), tbEnd.Text.Trim());
                    BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                }
            }
        }

        //重新查詢
        private void btnAll_Click(object sender, RoutedEventArgs e)
        {
            tbxEPISODE.Text="";
            tbxEPISODENAME.Text = "";
            client.GetTBPROG_D_BYPROGID_ALLAsync(m_strProgID);
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }

        //連點事件
        private void RowDoubleClick(object sender, DataGridDCTest.DataGridRowClickedArgs e)
        {
            DoubleClickDataGrid dcdg = sender as DoubleClickDataGrid;
            //Class_PROGM a = (dcdg.SelectedItem) as Class_PROGM;
            //MessageBox.Show(a.FSPGMNAME, "提示訊息", MessageBoxButton.OK);
            m_strEpisode_View = ((Class_PROGD)dcdg.SelectedItem).FNEPISODE.ToString();
            m_strProgDName_View = ((Class_PROGD)dcdg.SelectedItem).FSPGDNAME.ToString();
            m_strPlayEpisode_View = ((Class_PROGD)DGPROG_DListD.SelectedItem).FNSHOWEPISODE.ToString();
            this.DialogResult = true;
        }

        //集別輸完後按下enter直接查詢
        private void tbxEPISODE_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                btnSearch_Click(sender, e);
            }
        }

        //子集名稱輸完後按下enter直接查詢
        private void tbxEPISODENAME_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                btnSearch_Click(sender, e);
            }
        }


    }
}

