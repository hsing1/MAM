﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSPROG_D;
using System.Xml.Linq;
using System.IO;
using System.Text;

namespace PTS_MAM3.PRG
{
    public partial class PRG200_08 : ChildWindow  //查詢節目子集資料
    {
        WSPROG_DSoapClient client = new WSPROG_DSoapClient();   //產生新的代理類別

        public string m_strProgID = "";                         //帶入的節目編號
        public string m_strProgName = "";                       //帶入的節目名稱
        public string m_strEpisodeList_View = "";               //選取的集別string
        public List<string> ListCheck = new List<string>();     //選取的集別List

        public PRG200_08(string ProgID ,string ProgName)
        {
            InitializeComponent();
            m_strProgID=ProgID.ToString().Trim();
            m_strProgName = ProgName.ToString().Trim();

            InitializeForm();        //初始化本頁面
        }

        void InitializeForm() //初始化本頁面
        {
            //取得全部節目子集資料
            client.GetTBPROG_D_BYPROGID_ALLCompleted += new EventHandler<GetTBPROG_D_BYPROGID_ALLCompletedEventArgs>(client_GetTBPROG_D_BYPROGID_ALLCompleted);
            //client.GetTBPROG_D_BYPROGID_ALLAsync(m_strProgID);//一開始不要LOAD全部太多了

            //查詢子集資料By集別和節目名稱 
            client.QProg_D_BYEPISODE_PGDNAMECompleted += new EventHandler<QProg_D_BYEPISODE_PGDNAMECompletedEventArgs>(client_fnQProg_D_BYEPISODE_PGDNAMECompleted);

            client.QProg_D_BYEPISODE_PGDNAME_WITH_INTERVALCompleted += new EventHandler<QProg_D_BYEPISODE_PGDNAME_WITH_INTERVALCompletedEventArgs>(client_QProg_D_BYEPISODE_PGDNAME_WITH_INTERVALCompleted);
        }

        void client_QProg_D_BYEPISODE_PGDNAME_WITH_INTERVALCompleted(object sender, QProg_D_BYEPISODE_PGDNAME_WITH_INTERVALCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    this.DGPROG_DList.DataContext = e.Result;

                    if (this.DGPROG_DList.DataContext != null)
                    {
                        DGPROG_DList.SelectedIndex = 0;
                    }
                }
                else
                {
                    MessageBox.Show("該節目查無節目子集資料，請重新輸入查詢條件！", "提示訊息", MessageBoxButton.OK);
                    return;
                }
            }
        }

        //實作-查詢子集資料By集別和節目名稱 
        void client_fnQProg_D_BYEPISODE_PGDNAMECompleted(object sender, QProg_D_BYEPISODE_PGDNAMECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    this.DGPROG_DList.DataContext = e.Result;

                    if (this.DGPROG_DList.DataContext != null)
                    {
                        DGPROG_DList.SelectedIndex = 0;
                    }
                }
                else
                {
                    MessageBox.Show("該節目查無節目子集資料，請重新輸入查詢條件！", "提示訊息", MessageBoxButton.OK);
                    return;
                }
            }
        }

        //實作-取得全部節目子集資料
        void client_GetTBPROG_D_BYPROGID_ALLCompleted(object sender, GetTBPROG_D_BYPROGID_ALLCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    this.DGPROG_DList.DataContext = e.Result;

                    if (this.DGPROG_DList.DataContext != null)
                    {
                        DGPROG_DList.SelectedIndex = 0;
                    }
                }
                else
                {
                    MessageBox.Show("該節目查無節目子集資料，請重新選擇節目名稱！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
        }
   
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {           
            string strCheck="";  

           if (ListCheck.Count == 0)     //判斷是否有選取集別
           {
               MessageBox.Show("請選擇節目子集資料", "提示訊息", MessageBoxButton.OK);
               return;
           }
           else
           {
               for (int i = 0; i < ListCheck.Count; i++)   
                   strCheck += ListCheck[i].Trim() + ";";             

               m_strEpisodeList_View = strCheck;
              this.DialogResult = true;
           }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //條件查詢
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            short shortCheck;   //純粹為了判斷是否為數字型態之用
            string strEpisode = tbxEPISODE.Text.ToString().Trim() ;
            string strProgDName=tbxEPISODENAME.Text.ToString().Trim();

            if (radioButton1.IsChecked==true)
            {
                if (short.TryParse(strEpisode, out shortCheck) == false)
                    MessageBox.Show("請檢查「集別」欄位必須為數值型態", "提示訊息", MessageBoxButton.OK);
                else
                    client.QProg_D_BYEPISODE_PGDNAMEAsync(m_strProgID, strEpisode, strProgDName);
            }
            else if (radioButton2.IsChecked==true)
            {
                if (short.TryParse(tbStart.Text.Trim(), out shortCheck) == false || short.TryParse(tbEnd.Text.Trim(), out shortCheck) == false)
                    MessageBox.Show("請檢查「集別區間」欄位必須為數值型態", "提示訊息", MessageBoxButton.OK);
                else
                    client.QProg_D_BYEPISODE_PGDNAME_WITH_INTERVALAsync(m_strProgID, strEpisode, strProgDName, tbStart.Text.Trim(), tbEnd.Text.Trim());
            }

        }

        //重新查詢
        private void btnAll_Click(object sender, RoutedEventArgs e)
        {
            tbxEPISODE.Text="";
            tbxEPISODENAME.Text = "";
            client.GetTBPROG_D_ALLAsync(m_strProgID);
        }

        //勾選的時候選到的集別加到List
        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            ListCheck.Add(cb.Tag.ToString());
        }

        //取消勾選的時候選到的集別從到List拿掉
        private void cbEpisode_Unchecked(object sender, RoutedEventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            ListCheck.Remove(cb.Tag.ToString());
        }

      



    }
}

