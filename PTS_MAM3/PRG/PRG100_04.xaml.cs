﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.PROG_M;
using PTS_MAM3.WSPROG_M;
using System.Text;
using System.Xml.Linq;
using System.IO;
using PTS_MAM3.PRG;

namespace PTS_MAM3.ProgData
{
    public partial class PROGMDATA_Query : ChildWindow
    {
        WSPROG_MSoapClient client = new WSPROG_MSoapClient();               //產生新的代理類別
        public List<Class_PROGM> m_ListFormData = new List<Class_PROGM>();  //記錄查詢到的節目主檔資料
        List<Class_CODE> m_CodeData = new List<Class_CODE>();               //代碼檔集合 
        List<Class_CODE> m_CodeDataBuyD = new List<Class_CODE>();           //代碼檔集合(外購類別細項) 
        public Class_PROGM m_Queryobj = new Class_PROGM();                  //查詢條件的obj

        public PROGMDATA_Query()
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面
        }

        void InitializeForm() //初始化本頁面
        {
            //下載代碼檔
            client.fnGetTBPROG_M_CODECompleted += new EventHandler<fnGetTBPROG_M_CODECompletedEventArgs>(client_fnGetTBPROG_M_CODECompleted);
            client.fnGetTBPROG_M_CODEAsync();

            //下載外購類別細項代碼檔
            client.fnGetTBPROG_M_PROGBUYD_CODECompleted += new EventHandler<fnGetTBPROG_M_PROGBUYD_CODECompletedEventArgs>(client_fnGetTBPROG_M_PROGBUYD_CODECompleted);
            client.fnGetTBPROG_M_PROGBUYD_CODEAsync();

            //查詢節目主檔資料
            client.fnQUERYTBPROG_MCompleted += new EventHandler<fnQUERYTBPROG_MCompletedEventArgs>(client_fnQUERYTBPROG_MCompleted);
        }

        //實作-查詢節目主檔資料
        void client_fnQUERYTBPROG_MCompleted(object sender, fnQUERYTBPROG_MCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)  //若是有查詢結果，則顯示，不然就請使用者重新查詢
                {
                    m_ListFormData = new List<Class_PROGM>(e.Result);  //把取回來的結果，加上型別定義，才可以塞回去    
                    this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("查無節目基本資料，請重新輸入條件查詢！", "提示訊息", MessageBoxButton.OK);
                    return;
                }
            }
        }
        
        //比對代碼檔
        string compareCode_Code(string strID, string strTable)
        {
            for (int i = 0; i < m_CodeData.Count; i++)
            {
                if (m_CodeData[i].ID.ToString().Trim().Equals(strID) && m_CodeData[i].TABLENAME.ToString().Trim().Equals(strTable))
                {
                    return m_CodeData[i].NAME.ToString().Trim();
                }
            }
            return "";
        }

        //比對代碼檔_外購類別細項
        string compareCode_CodeBuyD(string strID, string strDID)
        {
            for (int i = 0; i < m_CodeDataBuyD.Count; i++)
            {
                if (m_CodeDataBuyD[i].ID.ToString().Trim().Equals(strID) && m_CodeDataBuyD[i].IDD.ToString().Trim().Equals(strDID))
                {
                    return m_CodeDataBuyD[i].NAME.ToString().Trim();
                }
            }
            return "";
        }   
        
        #region ComboBox載入代碼檔

        //實作-下載外購類別細項代碼檔
        void client_fnGetTBPROG_M_PROGBUYD_CODECompleted(object sender, fnGetTBPROG_M_PROGBUYD_CODECompletedEventArgs e)
        {        
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    //m_CodeDataBuyD = new List<Class_CODE>(e.Result);
      
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        ComboBoxItem cbiPROGBUYD = new ComboBoxItem();
                        cbiPROGBUYD.Content = CodeData.NAME;
                        cbiPROGBUYD.Tag = CodeData.ID;
                        cbiPROGBUYD.DataContext = CodeData.IDD;
                        this.cbPROGBUYDID.Items.Add(cbiPROGBUYD);   //先都產生在隱藏起來的細項代碼combobox裡                   
                        m_CodeDataBuyD.Add(CodeData);  
                    }
                }
            }
        }

        //實作-下載所有的代碼檔
        void client_fnGetTBPROG_M_CODECompleted(object sender, fnGetTBPROG_M_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            case "TBZCHANNEL":       //頻道別代碼
                                ComboBoxItem cbiCHANNEL = new ComboBoxItem();
                                cbiCHANNEL.Content = CodeData.NAME;
                                cbiCHANNEL.Tag = CodeData.ID;
                                this.cbCHANNEL_ID.Items.Add(cbiCHANNEL);

                                CheckBox cbCHANNEL = new CheckBox();
                                cbCHANNEL.Content = CodeData.NAME;
                                cbCHANNEL.Tag = CodeData.ID;
                                //combobox的點選及取消點選的事件處理常式
                                //cbCHANNEL.Checked += new RoutedEventHandler(cbCHANNEL_Checked);
                                //cbCHANNEL.Unchecked += new RoutedEventHandler(cbCHANNEL_Unchecked);
                                listCHANNEL.Items.Add(cbCHANNEL);
                                break;
                            case "TBZDEPT":         //部門別代碼
                                if (CodeData.NAME.Trim() != "")//要濾掉空白
                                {
                                    ComboBoxItem cbiDEPT = new ComboBoxItem();
                                    cbiDEPT.Content = CodeData.NAME;
                                    cbiDEPT.Tag = CodeData.ID;
                                    this.cbPRDDEPTID.Items.Add(cbiDEPT);
                                }
                                break;
                            case "TBZPROGOBJ":      //製作目的代碼
                                ComboBoxItem cbiPROGOBJ = new ComboBoxItem();
                                cbiPROGOBJ.Content = CodeData.NAME;
                                cbiPROGOBJ.Tag = CodeData.ID;
                                this.cbPROGOBJID.Items.Add(cbiPROGOBJ);
                                break;
                            case "TBZPRDCENTER":      //製作單位代碼
                                if (CodeData.ID.Trim() != "00000")  //舊資料的代碼要略過
                                {
                                    ComboBoxItem cbiPRDCENTER = new ComboBoxItem();
                                    cbiPRDCENTER.Content = CodeData.NAME;
                                    cbiPRDCENTER.Tag = CodeData.ID;
                                    this.cbPRDCENID.Items.Add(cbiPRDCENTER);
                                }
                                break;
                            case "TBZPROGGRADE":      //節目分級代碼
                                ComboBoxItem cbiPROGGRADE = new ComboBoxItem();
                                cbiPROGGRADE.Content = CodeData.NAME;
                                cbiPROGGRADE.Tag = CodeData.ID;
                                this.cbPROGGRADEID.Items.Add(cbiPROGGRADE);
                                break;
                            case "TBZPROGSRC":      //節目來源代碼
                                ComboBoxItem cbiPROGSRC = new ComboBoxItem();
                                cbiPROGSRC.Content = CodeData.NAME;
                                cbiPROGSRC.Tag = CodeData.ID;
                                this.cbPROGSRCID.Items.Add(cbiPROGSRC);
                                break;
                            case "TBZPROGATTR":      //內容屬性代碼
                                ComboBoxItem cbiPROGATTR = new ComboBoxItem();
                                cbiPROGATTR.Content = CodeData.NAME;
                                cbiPROGATTR.Tag = CodeData.ID;
                                this.cbPROGATTRID.Items.Add(cbiPROGATTR);
                                break;
                            case "TBZPROGAUD":      //目標觀眾代碼
                                ComboBoxItem cbiPROGAUD = new ComboBoxItem();
                                cbiPROGAUD.Content = CodeData.NAME;
                                cbiPROGAUD.Tag = CodeData.ID;
                                this.cbPROGAUDID.Items.Add(cbiPROGAUD);
                                break;
                            case "TBZPROGTYPE":      //表現方式代碼
                                ComboBoxItem cbiPROGTYPE = new ComboBoxItem();
                                cbiPROGTYPE.Content = CodeData.NAME;
                                cbiPROGTYPE.Tag = CodeData.ID;
                                this.cbPROGTYPEID.Items.Add(cbiPROGTYPE);
                                break;
                            case "TBZPROGLANG":      //語言代碼
                                ComboBoxItem cbiPROGLANG1 = new ComboBoxItem();
                                cbiPROGLANG1.Content = CodeData.NAME;
                                cbiPROGLANG1.Tag = CodeData.ID;
                                this.cbPROGLANGID1.Items.Add(cbiPROGLANG1);  //主聲道

                                ComboBoxItem cbiPROGLANG2 = new ComboBoxItem();
                                cbiPROGLANG2.Content = CodeData.NAME;
                                cbiPROGLANG2.Tag = CodeData.ID;
                                this.cbPROGLANGID2.Items.Add(cbiPROGLANG2);  //副聲道
                                break;
                            case "TBZPROGBUY":       //外購類別代碼
                                if (CodeData.NAME.Trim() != "")//要濾掉空白
                                {
                                    ComboBoxItem cbiPROGBUY = new ComboBoxItem();
                                    cbiPROGBUY.Content = CodeData.NAME;
                                    cbiPROGBUY.Tag = CodeData.ID;
                                    this.cbPROGBUYID.Items.Add(cbiPROGBUY);
                                }
                                break;
                            case "TBZPROGSALE":       //行銷類別代碼
                                ComboBoxItem cbiPROGSALE = new ComboBoxItem();
                                cbiPROGSALE.Content = CodeData.NAME;
                                cbiPROGSALE.Tag = CodeData.ID;
                                this.cbPROGSALEID.Items.Add(cbiPROGSALE);
                                break;
                            case "TBZPROGSPEC":       //節目規格代碼
                                CheckBox cbPROGSPEC = new CheckBox();
                                cbPROGSPEC.Content = CodeData.NAME;
                                cbPROGSPEC.Tag = CodeData.ID;
                                listPROGSPEC.Items.Add(cbPROGSPEC);
                                break;
                            default:
                                break;
                        }
                    }

                    //比對代碼檔
                    //compareDERIVE(m_FormData.FCDERIVE.ToString().Trim());     //衍生
                    //compareOutBuy(m_FormData.FCOUT_BUY.ToString().Trim());    //外購
                    //compareDel(m_FormData.FSDEL.ToString().Trim());           //刪除註記
                    //compareCode_FSPRDDEPTID(m_FormData.FSPRDDEPTID.ToString().Trim());
                    //compareCode_FSPROGOBJID(m_FormData.FSPROGOBJID.ToString().Trim());
                    //compareCode_FSPROGGRADEID(m_FormData.FSPROGGRADEID.ToString().Trim());
                    //compareCode_FSPROGATTRID(m_FormData.FSPROGATTRID.ToString().Trim());
                    //compareCode_FSPROGSRCID(m_FormData.FSPROGSRCID.ToString().Trim());
                    //compareCode_FSPROGTYPEID(m_FormData.FSPROGTYPEID.ToString().Trim());
                    //compareCode_FSPROGAUDID(m_FormData.FSPROGAUDID.ToString().Trim());
                    //compareCode_FSPROGLANGID1(m_FormData.FSPROGLANGID1.ToString().Trim());
                    //compareCode_FSPROGLANGID2(m_FormData.FSPROGLANGID2.ToString().Trim());
                    //compareCode_FSPROGBUYID(m_FormData.FSPROGBUYID.ToString().Trim());
                    //compareCode_FSPROGBUYDID(m_FormData.FSPROGBUYDID.ToString().Trim());
                    //compareCode_FSCHANNEL_ID(m_FormData.FSCHANNEL_ID.ToString().Trim());
                    //compareCode_FSCHANNEL(CheckList(m_FormData.FSCHANNEL.ToString().Trim()));
                    //compareCode_FSPROGSALEID(m_FormData.FSPROGSALEID.ToString().Trim());
                    //compareCode_FSPRDCENID(m_FormData.FSPRDCENID.ToString().Trim());
                }
            }
        }

        private string[] CheckList(string strChannel)
        {
            char[] delimiterChars = { ';' };
            string[] words = strChannel.Split(delimiterChars);
            return words;
        }

        private void compareDERIVE(string strDERIVE)   //是否為衍生
        {
            if (strDERIVE == "N")
                cbDERIVE.SelectedIndex = 0;
            else if (strDERIVE == "Y")
                cbDERIVE.SelectedIndex = 1;
        }

        private void compareOutBuy(string strOutBuy)   //是否為外購
        {
            if (strOutBuy == "N")
                cbOUT_BUY.SelectedIndex = 0;
            else if (strOutBuy == "Y")
                cbOUT_BUY.SelectedIndex = 1;
        }

        //選擇外購類別，要判斷顯示哪些外購類別細項
        private void cbPROGBUYID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.cbPROGBUYDIDS.Items.Clear();     //使用前先清空
            string strBUYID = ""; //外購類別大項
            ComboBox cb = sender as ComboBox;
            ComboBoxItem cbm = (ComboBoxItem)cb.SelectedItem;
            strBUYID = cbm.Tag.ToString();

            for (int i = 0; i < cbPROGBUYDID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGBUYDID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strBUYID))
                {
                    ComboBoxItem cbiPROGBUYDS = new ComboBoxItem();
                    cbiPROGBUYDS.Content = getcbi.Content;
                    cbiPROGBUYDS.Tag = getcbi.DataContext;
                    this.cbPROGBUYDIDS.Items.Add(cbiPROGBUYDS);
                }
            }
        }

        #endregion

        #region 檢查畫面資料及搬值

        private Class_PROGM FormToClass_PROGM()
        {
            Class_PROGM obj = new Class_PROGM();

            obj.FSPROG_ID = tbxtProgID.Text.ToString();              //節目編號

            if (cbCHANNEL_ID.SelectedIndex != -1)                    //頻道別
                obj.FSCHANNEL_ID = ((ComboBoxItem)cbCHANNEL_ID.SelectedItem).Tag.ToString();
            else
                obj.FSCHANNEL_ID = "";

            if (cbPROGOBJID.SelectedIndex != -1)                     //製作目的
                obj.FSPROGOBJID = ((ComboBoxItem)cbPROGOBJID.SelectedItem).Tag.ToString();
            else
                obj.FSPROGOBJID = "";

            if (cbPRDDEPTID.SelectedIndex != -1)                    //製作部門
                obj.FSPRDDEPTID = ((ComboBoxItem)cbPRDDEPTID.SelectedItem).Tag.ToString();
            else
                obj.FSPRDDEPTID = "";

            if (cbDERIVE.SelectedIndex != -1)                       //衍生
                obj.FCDERIVE = ((ComboBoxItem)cbDERIVE.SelectedItem).Tag.ToString();
            else
                obj.FCDERIVE = "";

            obj.FSCHANNEL = Check_ChannelList();                   //預訂播出頻道

            obj.FSPGMNAME = tbxPGMNAME.Text.ToString().Trim();     //節目名稱
            obj.FSWEBNAME = tbxWEBNAME.Text.ToString().Trim();     //播出名稱 
            obj.FSPGMENAME = tbxPGMENAME.Text.ToString().Trim();   //外文名稱

            //if (cbPRDCENID.SelectedIndex != -1)                    //製作單位
            //    obj.FSPRDCENID = ((ComboBoxItem)cbPRDCENID.SelectedItem).Tag.ToString();
            //else
            //    obj.FSPRDCENID = "";

            if (tbxPRDCEN.Text.Trim() != "")                       //製作單位
                obj.FSPRDCENID = tbxPRDCEN.Tag.ToString();
            else
                obj.FSPRDCENID = "";

            if (tbxTOTEPISODE.Text.ToString().Trim()=="")
                obj.FNTOTEPISODE = 0; //總集數
            else
                obj.FNTOTEPISODE = Convert.ToInt16(tbxTOTEPISODE.Text.ToString().Trim()); //總集數

            if (tbxLENGTH.Text.ToString().Trim()=="")
                obj.FNLENGTH = 0;      //每集時長            
            else
                obj.FNLENGTH = Convert.ToInt16(tbxLENGTH.Text.ToString().Trim());         //每集時長
            
            obj.FSPRDYYMM = tbxPRDYYMM.Text.ToString().Trim();    //製作年月

            if (cbPROGGRADEID.SelectedIndex != -1)                //節目分級
                obj.FSPROGGRADEID = ((ComboBoxItem)cbPROGGRADEID.SelectedItem).Tag.ToString();
            else
                obj.FSPROGGRADEID = "";

            if (dpShowDate.Text.ToString().Trim() == "")        //預計上檔日期
                obj.FDSHOWDATE = Convert.ToDateTime("1900/1/1");
            else
                obj.FDSHOWDATE = Convert.ToDateTime(dpShowDate.Text);

            obj.FSCONTENT = tbxCONTENT.Text.ToString().Trim();   //節目內容簡述
            obj.FSMEMO = tbxMEMO.Text.ToString().Trim();         //備註      

            if (cbPROGSRCID.SelectedIndex != -1)                 //節目來源
                obj.FSPROGSRCID = ((ComboBoxItem)cbPROGSRCID.SelectedItem).Tag.ToString();
            else
                obj.FSPROGSRCID = "";

            if (cbPROGATTRID.SelectedIndex != -1)                //節目型態
                obj.FSPROGATTRID = ((ComboBoxItem)cbPROGATTRID.SelectedItem).Tag.ToString();
            else
                obj.FSPROGATTRID = "";

            if (cbPROGAUDID.SelectedIndex != -1)                 //目標觀眾
                obj.FSPROGAUDID = ((ComboBoxItem)cbPROGAUDID.SelectedItem).Tag.ToString();
            else
                obj.FSPROGAUDID = "";

            if (cbPROGTYPEID.SelectedIndex != -1)                //表現方式
                obj.FSPROGTYPEID = ((ComboBoxItem)cbPROGTYPEID.SelectedItem).Tag.ToString();
            else
                obj.FSPROGTYPEID = "";

            if (cbPROGLANGID1.SelectedIndex != -1)               //主聲道
                obj.FSPROGLANGID1 = ((ComboBoxItem)cbPROGLANGID1.SelectedItem).Tag.ToString();
            else
                obj.FSPROGLANGID1 = "";

            if (cbPROGLANGID2.SelectedIndex != -1)               //副聲道
                obj.FSPROGLANGID2 = ((ComboBoxItem)cbPROGLANGID2.SelectedItem).Tag.ToString();
            else
                obj.FSPROGLANGID2 = "";

            if (cbPROGBUYID.SelectedIndex != -1)                 //外購類別大類
                obj.FSPROGBUYID = ((ComboBoxItem)cbPROGBUYID.SelectedItem).Tag.ToString();
            else
                obj.FSPROGBUYID = "";

            if (cbPROGBUYDIDS.SelectedIndex != -1)               //外購類別細類
                obj.FSPROGBUYDID = ((ComboBoxItem)cbPROGBUYDIDS.SelectedItem).Tag.ToString();
            else
                obj.FSPROGBUYDID = "";

            if (cbPROGSALEID.SelectedIndex != -1)                //行銷類別
                obj.FSPROGSALEID = ((ComboBoxItem)cbPROGSALEID.SelectedItem).Tag.ToString();
            else
                obj.FSPROGSALEID = "";

            obj.FSEWEBPAGE = tbxEWEBPAGE.Text.ToString().Trim(); //英文網頁資料

            if (cbOUT_BUY.SelectedIndex != -1)                   //外購
                obj.FCOUT_BUY = ((ComboBoxItem)cbOUT_BUY.SelectedItem).Tag.ToString();
            else
                obj.FCOUT_BUY = "";

            obj.FSDEL = "N";        //刪除註記
            obj.FSDELUSER = "";     //刪除者                 

            obj.FSPROGSPEC = Check_PROGSPECList();              //節目規格   

            m_Queryobj = obj;       //紀錄查詢條件

            return obj;            
        }

        private string Check_ChannelList()    //檢查可播映頻道有哪些
        {
            string strReturn = "";
            List<string> ListCHANNEL = new List<string>();  //TEST
            for (int i = 0; i < listCHANNEL.Items.Count; i++)
            {
                CheckBox getckb = (CheckBox)listCHANNEL.Items.ElementAt(i);
                if (getckb.IsChecked == true)
                {
                    strReturn += getckb.Tag.ToString().Trim() + ";";
                }
            }
            return strReturn;
        }

        private string Check_PROGSPECList()    //檢查節目規格有哪些
        {
            string strReturn = "";
            for (int i = 0; i < listPROGSPEC.Items.Count; i++)
            {
                CheckBox getckb = (CheckBox)listPROGSPEC.Items.ElementAt(i);
                if (getckb.IsChecked == true)
                {
                    strReturn += getckb.Tag.ToString().Trim() + ";";
                }
            }
            return strReturn;
        }

        private Boolean Check_PROGMData()    //檢查畫面上的欄位是否都填妥
        {
            StringBuilder strMessage = new StringBuilder();
            int intCheck;   //純粹為了判斷是否為數字型態之用
            short shortCheck;   //純粹為了判斷是否為數字型態之用
            
            if (tbxLENGTH.Text.Trim() != "")
            {
                if (short.TryParse(tbxLENGTH.Text.Trim(), out shortCheck) == false)
                    strMessage.AppendLine("請檢查「每集時長」欄位必須為數值型態");
            }

            if (tbxTOTEPISODE.Text.Trim() != "")
            {
                if (short.TryParse(tbxTOTEPISODE.Text.Trim(), out shortCheck) == false)
                    strMessage.AppendLine("請檢查「總集數」欄位必須為數值型態");
            }

            if (tbxPRDYYMM.Text.Trim() != "")
            {
                if (int.TryParse(tbxPRDYYMM.Text.Trim(), out intCheck) == false)
                    strMessage.AppendLine("請檢查「製作年月」欄位必須為數值型態");
                else
                {
                    if (tbxPRDYYMM.Text.Trim().Length != 6)
                        strMessage.AppendLine("請檢查「製作年月」欄位必須為四碼西元年+兩碼月份\n" + "範例：201101(2011年1月)");
                }
            }

            if (strMessage.ToString().Trim() == "")
                return true;
            else
            {
                MessageBoxResult result = MessageBox.Show(strMessage.ToString(), "提示訊息", MessageBoxButton.OK);
                return false;
            }
        }

        #endregion       

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (Check_PROGMData() == false)      //檢查畫面上的欄位是否都填妥
                return;

            //查詢節目主檔資料
            client.fnQUERYTBPROG_MAsync(FormToClass_PROGM());
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //選取製作單位
        private void btnPRDCEN_Click(object sender, RoutedEventArgs e)
        {
            PRG100_11 PRG100_11_frm = new PRG100_11();
            PRG100_11_frm.Show();

            PRG100_11_frm.Closed += (s, args) =>
            {
                if (PRG100_11_frm.DialogResult == true)
                {
                    tbxPRDCEN.Text = PRG100_11_frm.strPRDCENName_View;
                    tbxPRDCEN.Tag = PRG100_11_frm.strPRDCENID_View;
                }
            };
        }   
    }
}

