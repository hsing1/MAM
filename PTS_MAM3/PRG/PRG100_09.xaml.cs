﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.PRG
{
    public partial class PRG100_09 : ChildWindow
    {
        public PRG100_09(List<WSPROG_M.Class_DELETE_MSG> ListDELETE_MSG)  //, string strSW
        {
            InitializeComponent();
            
            this.Title = "刪除節目基本資料提示訊息";

            if (ListDELETE_MSG.Count>0)
                DGPROG_MList.ItemsSource = ListDELETE_MSG;
        }

        public PRG100_09(List<WSPROG_D.Class_DELETE_MSG> ListDELETE_MSG) //, string strSW
        { 
            InitializeComponent();

            this.Title = "刪除節目子集資料提示訊息";
           
            if (ListDELETE_MSG.Count > 0)
                DGPROG_MList.ItemsSource = ListDELETE_MSG;
        }

        public PRG100_09(List<WSPROMO.Class_DELETE_MSG> ListDELETE_MSG) //, string strSW
        {
            InitializeComponent();

            this.Title = "刪除宣傳帶基本資料提示訊息";

            if (ListDELETE_MSG.Count > 0)
                DGPROG_MList.ItemsSource = ListDELETE_MSG;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }       
    }
}

