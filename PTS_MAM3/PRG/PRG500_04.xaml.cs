﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;
using System.IO;
using System.Text;
using PTS_MAM3.WSPROGRACE;
using PTS_MAM3.WSPROG_M;
using PTS_MAM3.PROG_M;
using PTS_MAM3.PRG;

namespace PTS_MAM3.ProgData
{
    public partial class PROGRACE_Query : ChildWindow
    {
        WSPROGRACESoapClient client = new WSPROGRACESoapClient();   //產生新的代理類別
        WSPROG_MSoapClient clientM = new WSPROG_MSoapClient();

        List<string> m_ListRace = new List<string>();               //參展名稱集合 
        List<string> m_ListWin = new List<string>();                //得獎名稱集合
        public List<Class_PROGRACE> m_ListFormData = new List<Class_PROGRACE>();    //記錄查詢到的資料
        List<Class_PROGRACE> m_ListFormDataWin = new List<Class_PROGRACE>(); //記錄查詢到的資料_得獎記錄，為了比對用
        public Class_PROGRACE m_Queryobj = new Class_PROGRACE();    //查詢條件的obj
        string m_strRaceType = "";                                  //查詢參展的狀態

        public PROGRACE_Query()
        {
            InitializeComponent();
            InitializeForm();          //初始化本頁面
        }

        void InitializeForm() //初始化本頁面
        {
            //下載代碼檔//原本參展狀態是用代碼檔，後來改成只有三種：入選、入圍、得獎
            client.fnGetTBPROGRACE_CODECompleted += new EventHandler<fnGetTBPROGRACE_CODECompletedEventArgs>(client_fnGetTBPROGRACE_CODECompleted);
            client.fnGetTBPROGRACE_CODEAsync();

            //下載代碼檔
            clientM.fnGetTBPROG_M_CODECompleted += new EventHandler<fnGetTBPROG_M_CODECompletedEventArgs>(clientM_fnGetTBPROG_M_CODECompleted);
            clientM.fnGetTBPROG_M_CODEAsync();

            //查詢參展得獎資料
            client.fnQUERY_TBPROGRACECompleted += new EventHandler<fnQUERY_TBPROGRACECompletedEventArgs>(client_fnQUERY_TBPROGRACECompleted);

            //參展名稱 AUTO COMPLETE
            client.fnGetRACECompleted += new EventHandler<fnGetRACECompletedEventArgs>(client_fnGetRACECompleted);
            client.fnGetRACEAsync();

            //得獎名稱 AUTO COMPLETE
            client.fnGetWINCompleted += new EventHandler<fnGetWINCompletedEventArgs>(client_fnGetWINCompleted);
            client.fnGetWINAsync();

            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }        

        #region 實作

        //實作-查詢參展得獎資料
        void client_fnQUERY_TBPROGRACECompleted(object sender, fnQUERY_TBPROGRACECompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)  //若是有查詢結果，則顯示，不然就請使用者重新查詢
                {
                    if (m_strRaceType == "")
                    {
                        m_ListFormData = new List<Class_PROGRACE>(e.Result);  //把取回來的結果，加上型別定義，才可以塞回去    
                        this.DialogResult = true;
                    }
                    else if (m_strRaceType == "in")
                    {
                        m_ListFormData = new List<Class_PROGRACE>(e.Result);
                        m_strRaceType = "win";
                        client.fnQUERY_TBPROGRACEAsync(FormToClass_PROGRACE("win"));
                    }
                    else if (m_strRaceType == "win")
                    {
                        m_ListFormDataWin = new List<Class_PROGRACE>(e.Result);
                        m_ListFormData = checkQuery();  //比對

                        if (m_ListFormData.Count == 0)
                        {
                            MessageBox.Show("查無參展得獎資料，請重新輸入條件查詢！", "提示訊息", MessageBoxButton.OK);
                            return;
                        }
                        else
                            this.DialogResult = true;
                    }   
                }
                else
                {
                    if (m_strRaceType == "" )
                    {
                        MessageBox.Show("查無參展得獎資料，請重新輸入條件查詢！", "提示訊息", MessageBoxButton.OK);
                        return;
                    }
                    else if (m_strRaceType == "in")     //入圍就沒有查到的話，就都不需要比對
                    {
                        MessageBox.Show("查無參展得獎資料，請重新輸入條件查詢！", "提示訊息", MessageBoxButton.OK);
                        return;
                    }
                    else if (m_strRaceType == "win")
                    {
                        if (cbMix.IsChecked == true)    //有入圍沒得獎：如果沒有得獎可以比對，那就不需要比對，直接回傳有入圍的結果
                        {
                            this.DialogResult = true;
                        }
                        else if (cbUnion.IsChecked == true) //有入圍有得獎：如果沒有得獎可以比對，表示沒有資料是同時入圍且得獎                       
                        {
                            MessageBox.Show("查無參展得獎資料，請重新輸入條件查詢！", "提示訊息", MessageBoxButton.OK);
                            return;
                        }   
                    }   
                }
            }
        } 

        //實作-得獎名稱
        void client_fnGetWINCompleted(object sender, fnGetWINCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    tbxPROGWIN.ItemsSource = e.Result;
                }
            }
        }

        //實作-參展名稱
        void client_fnGetRACECompleted(object sender, fnGetRACECompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    tbxPROGRACE.ItemsSource = e.Result;
                }
            } 
        }

        #endregion              

        #region ComboBox載入代碼檔

        //實作-下載所有的代碼檔
        void client_fnGetTBPROGRACE_CODECompleted(object sender, fnGetTBPROGRACE_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        WSPROGRACE.Class_CODE CodeData = new WSPROGRACE.Class_CODE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            //case "TBZPROGSTATUS":      //參展狀態代碼
                            //    ComboBoxItem cbiPROGSTATUS = new ComboBoxItem();
                            //    cbiPROGSTATUS.Content = CodeData.NAME;
                            //    cbiPROGSTATUS.Tag = CodeData.ID;
                            //    this.cbPROGSTATUS.Items.Add(cbiPROGSTATUS);
                            //    break;
                            case "TBZPROGRACE_AREA":    //參展區域                              
                                ComboBoxItem cbiPRGAREA = new ComboBoxItem();
                                cbiPRGAREA.Content = CodeData.NAME;
                                cbiPRGAREA.Tag = CodeData.ID;
                                this.cbAREA.Items.Add(cbiPRGAREA);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

        //實作-下載代碼檔
        void clientM_fnGetTBPROG_M_CODECompleted(object sender, fnGetTBPROG_M_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        WSPROG_M.Class_CODE CodeData = new WSPROG_M.Class_CODE();
                        CodeData = e.Result[i];                        

                        switch (CodeData.TABLENAME)
                        {
                            case "TBZPROGSRC":      //節目來源代碼
                                ComboBoxItem cbiPROGSRC = new ComboBoxItem();
                                cbiPROGSRC.Content = CodeData.NAME;
                                cbiPROGSRC.Tag = CodeData.ID;
                                this.cbPROGSRCID.Items.Add(cbiPROGSRC);
                                break;

                            case "TBZCHANNEL":       //頻道別代碼
                                ComboBoxItem cbiCHANNEL = new ComboBoxItem();
                                cbiCHANNEL.Content = CodeData.NAME;
                                cbiCHANNEL.Tag = CodeData.ID;
                                this.cbCHANNEL_ID.Items.Add(cbiCHANNEL);                               
                                break;

                            default:
                                break;
                        }
                    }

                }
            }
        }

        #endregion

        #region 檢查畫面資料及搬值

        private Class_PROGRACE FormToClass_PROGRACE(string strRaceType)
        {       
            Class_PROGRACE obj = new Class_PROGRACE();

            if (tbxPROG_NAME.Text.Trim() == "")
                obj.FSPROG_ID = "";
            else
                obj.FSPROG_ID = tbxPROG_NAME.Tag.ToString().Trim(); //節目編碼
           
            if (tbxEPISODE.Text.ToString().Trim() == "")            //集別
                obj.FNEPISODE = 0;              
            else            
                obj.FNEPISODE = Convert.ToInt16(tbxEPISODE.Text.ToString().Trim());

            if (tbxAREA.Text.Trim() != "")                           //參展區域
                obj.FSAREA = tbxAREA.Tag.ToString().Trim();
            else
                obj.FSAREA = "";

            if (cbForeign.IsChecked == true)                        //參展區域(非台灣)
                obj.FSAREA_NOT_TAIWAN = compareArea();
            else
                obj.FSAREA_NOT_TAIWAN = "";

            obj.FSPROGRACE = tbxPROGRACE.Text.ToString().Trim();    //參展名稱

            if (dpRACEDATE.Text.Trim() == "")                       //參展日期(開始)
                obj.FDRACEDATE = new DateTime(1900, 1, 1);
            else
                obj.FDRACEDATE = Convert.ToDateTime(dpRACEDATE.Text);
            
            if (dpRACEDATE_END.Text.Trim() == "")                   //參展日期(結束)
                obj.FDRACEDATE_END = DateTime.Now;
            else
                obj.FDRACEDATE_END = Convert.ToDateTime(dpRACEDATE_END.Text);

            if (strRaceType == "")
            {
                if (cbPROGSTATUS.SelectedIndex != -1)                  //參展狀態              
                    obj.FSPROGSTATUS = ((ComboBoxItem)cbPROGSTATUS.SelectedItem).Tag.ToString();                  
                else          
                    obj.FSPROGSTATUS = "";
            }
            else if (strRaceType == "in")
                obj.FSPROGSTATUS = "入圍";
            else if (strRaceType == "win")
                obj.FSPROGSTATUS = "得獎";          

            if (tbPERIOD.Text.ToString().Trim() == "")             //屆數
                obj.FNPERIOD = 0;
            else
                obj.FNPERIOD = Convert.ToInt16(tbPERIOD.Text.ToString().Trim());

            obj.FSPROGWIN = tbxPROGWIN.Text.ToString().Trim();      //得獎名稱
            obj.FSPROGNAME = tbxPROGNAME.Text.ToString().Trim();    //參展節目名稱
            obj.FSMEMO = tbxMEMO.Text.ToString().Trim();            //備註   

            if (cbPROGSRCID.SelectedIndex != -1)                    //節目來源
                obj.FSPROGSRCID = ((ComboBoxItem)cbPROGSRCID.SelectedItem).Tag.ToString();
            else
                obj.FSPROGSRCID = "";

            if (cbCHANNEL_ID.SelectedIndex != -1)                    //節目來源
                obj.FSCHANNEL_ID = ((ComboBoxItem)cbCHANNEL_ID.SelectedItem).Tag.ToString();
            else
                obj.FSCHANNEL_ID = "";

            m_Queryobj = obj;       //紀錄查詢條件

            return obj;
        }

        private Boolean Check_PROGRACEData()    //檢查畫面上的欄位是否都填妥
        {
            StringBuilder strMessage = new StringBuilder();
            short shortCheck;   //純粹為了判斷是否為數字型態之用

            //if (tbxPROG_NAME.Text.Trim() == "")
            //    strMessage.AppendLine("請選擇「節目」");

            if (tbPERIOD.Text.Trim() != "")
            {
                if (short.TryParse(tbPERIOD.Text.Trim(), out shortCheck) == false)               
                    strMessage.AppendLine("請檢查「屆數」欄位必須為數值型態");   
            }

            if (dpRACEDATE.Text.Trim() != "" && dpRACEDATE_END.Text.Trim() == "" || dpRACEDATE.Text.Trim() == "" && dpRACEDATE_END.Text.Trim() != "")
                strMessage.AppendLine("查詢日期區間必須開始及結束日期都要選擇");

            if (cbPROGSTATUS.SelectedIndex != -1 && cbMix.IsChecked == true || cbPROGSTATUS.SelectedIndex != -1 && cbUnion.IsChecked == true || cbMix.IsChecked == true && cbUnion.IsChecked == true)
                strMessage.AppendLine("參展狀態的查詢條件，請選擇其中一項");

            if (tbxAREA.Text.Trim() != "" && cbForeign.IsChecked == true)
                strMessage.AppendLine("參展區域的查詢條件，沒有辦法指定參展區域又選擇「非台灣」");

            if (cbForeign.IsChecked == true  && compareArea() == "")
                strMessage.AppendLine("參展區域為台灣的代碼異常，請檢查參展區域代碼檔");

            if (strMessage.ToString().Trim() == "")
                return true;
            else
            {
                MessageBoxResult result = MessageBox.Show(strMessage.ToString(), "提示訊息", MessageBoxButton.OK);
                return false;
            }
        }

        #endregion    
        
        //開啟節目資料查詢畫面
        private void btnPROG_Click(object sender, RoutedEventArgs e)
        {
            PRG500_07 PRG500_07_frm = new PRG500_07();
            PRG500_07_frm.Show();

            PRG500_07_frm.Closed += (s, args) =>
            {
                if (PRG500_07_frm.DialogResult == true)
                {
                    if (PRG500_07_frm.strProgName_View.Trim() != "")
                    {
                        tbxPROG_NAME.Text = PRG500_07_frm.strProgName_View;
                        tbxPROG_NAME.Tag = PRG500_07_frm.strProgID_View;
                    }

                    if (PRG500_07_frm.strEpisodeName_View.Trim() != "")
                    {
                        tbxEPISODE.Text = PRG500_07_frm.strEpisode_View;
                        lblEPISODE_NAME.Content = PRG500_07_frm.strEpisodeName_View;
                    }
                    else
                    {
                        tbxEPISODE.Text = "";
                        lblEPISODE_NAME.Content = "";
                    }
                }
            };
        }

        //開啟節目子集資料查詢畫面
        private void btnEPISODE_Click(object sender, RoutedEventArgs e)
        {
            if (tbxPROG_NAME.Text.ToString().Trim() != "" && tbxPROG_NAME.Tag.ToString().Trim() != "")
            {
                PRG.PRG200_07 PRG200_07_frm = new PRG.PRG200_07(tbxPROG_NAME.Tag.ToString().Trim(), tbxPROG_NAME.Text.ToString().Trim());
                PRG200_07_frm.Show();

                PRG200_07_frm.Closed += (s, args) =>
                {
                    if (PRG200_07_frm.DialogResult == true)
                    {
                        tbxEPISODE.Text = PRG200_07_frm.m_strEpisode_View;
                        lblEPISODE_NAME.Content = PRG200_07_frm.m_strProgDName_View;                       
                    }
                };
            }
            else
                MessageBox.Show("請先選擇節目！", "提示訊息", MessageBoxButton.OK);
        }

        //確定
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (Check_PROGRACEData() == false)      //檢查畫面上的欄位是否都填妥
                return;

            //一般查詢
            if (cbMix.IsChecked == false && cbUnion.IsChecked == false)                          
            {
                client.fnQUERY_TBPROGRACEAsync(FormToClass_PROGRACE(""));
                BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                return;
            }

            //若是勾選了入圍且得獎或是入圍但沒得獎就要繼續查
            m_strRaceType = "in";
            client.fnQUERY_TBPROGRACEAsync(FormToClass_PROGRACE("in"));          
        }

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //選取參展區域
        private void btnAREA_Click(object sender, RoutedEventArgs e)
        {
            PRG500_05 PRG500_05_frm = new PRG500_05();
            PRG500_05_frm.Show();

            PRG500_05_frm.Closed += (s, args) =>
            {
                if (PRG500_05_frm.DialogResult == true)
                {
                    tbxAREA.Text = PRG500_05_frm.strName_View;
                    tbxAREA.Tag = PRG500_05_frm.strID_View;
                }
            };
        }

        //查詢台灣的參展區域代碼
        private string compareArea()   
        {
            for (int i = 0; i < cbAREA.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbAREA.Items.ElementAt(i);
                if (getcbi.Content.ToString().Trim().Equals("台灣"))
                {
                    return getcbi.Tag.ToString().Trim();                  
                }
            }
            return "";
        }

        //比對「有入圍沒得獎」以及「有入圍有得獎」
        private List<Class_PROGRACE> checkQuery()
        {
            List<Class_PROGRACE> ListFormData = new List<Class_PROGRACE>();    //記錄查詢到的資料
            Boolean bolCheck = false;

            if (cbMix.IsChecked == true)        //有入圍沒得獎，兩個迴圈去跑，若是查到有入圍裡也有得獎的就跳掉，最後都沒比對到，就是我們要的
            {
                for (int i = 0; i < m_ListFormData.Count; i++)
                {
                    for (int j = 0; j < m_ListFormDataWin.Count; j++)
                    {
                        bolCheck = false;
                        
                        if (m_ListFormData[i].FSPROG_ID.Trim() == m_ListFormDataWin[j].FSPROG_ID.Trim() && m_ListFormData[i].FNEPISODE == m_ListFormDataWin[j].FNEPISODE && m_ListFormData[i].FSAREA.Trim() == m_ListFormDataWin[j].FSAREA.Trim() && m_ListFormData[i].FSPROGRACE.Trim() == m_ListFormDataWin[j].FSPROGRACE.Trim() && m_ListFormData[i].FDRACEDATE == m_ListFormDataWin[j].FDRACEDATE && m_ListFormData[i].FNPERIOD == m_ListFormDataWin[j].FNPERIOD)
                        {
                            bolCheck = true;
                            break;
                        }                            
                    }

                    if (bolCheck == false)
                        ListFormData.Add(m_ListFormData[i]);    //回傳以有入圍為主
                }               
            }
            else if (cbUnion.IsChecked == true) //有入圍有得獎                       
            {
               for (int i = 0; i < m_ListFormData.Count; i++)
                {
                    for (int j = 0; j < m_ListFormDataWin.Count; j++)
                    {
                        if (m_ListFormData[i].FSPROG_ID.Trim() == m_ListFormDataWin[j].FSPROG_ID.Trim() && m_ListFormData[i].FNEPISODE == m_ListFormDataWin[j].FNEPISODE && m_ListFormData[i].FSAREA.Trim() == m_ListFormDataWin[j].FSAREA.Trim() && m_ListFormData[i].FSPROGRACE.Trim() == m_ListFormDataWin[j].FSPROGRACE.Trim() && m_ListFormData[i].FDRACEDATE == m_ListFormDataWin[j].FDRACEDATE && m_ListFormData[i].FNPERIOD == m_ListFormDataWin[j].FNPERIOD)
                        {
                             ListFormData.Add(m_ListFormData[i]);    //回傳以有入圍為主
                            break;
                        }                            
                    }  
                }
            }         

            return ListFormData;
        }


    }
}

