﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using PTS_MAM3.PROG_M;
using PTS_MAM3.WSPROMO;
using PTS_MAM3.ProgData;

namespace PTS_MAM3.PRG
{
    public partial class PRG800_02 : ChildWindow
    {
        WSPROMOSoapClient client = new WSPROMOSoapClient();
        List<Class_CODE> m_CodeDataCateD = new List<Class_CODE>();  //代碼檔集合(短帶類型細項) 
        string m_strPromoName = "";        //暫存短帶名稱 
        string m_strID = "";              //編碼
        string m_strEpisode = "";         //集別
        string m_ActID = "";              //活動代碼
        Class_PROMO m_FormData = new Class_PROMO();
        public Class_PROMO m_Queryobj = new Class_PROMO();    //查詢修改的obj

        public PRG800_02(Class_PROMO FormData)
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面
            m_FormData = FormData;
            ClassToForm();
        }

        void InitializeForm() //初始化本頁面
        {
            //修改短帶資料檔資料   
            client.UPDATE_TBPGM_PROMOCompleted += new EventHandler<UPDATE_TBPGM_PROMOCompletedEventArgs>(client_UPDATE_TBPGM_PROMOCompleted);
            
            //下載代碼檔
            client.GetTBPGM_PROMO_CODECompleted += new EventHandler<GetTBPGM_PROMO_CODECompletedEventArgs>(client_GetTBPGM_PROMO_CODECompleted);
            client.GetTBPGM_PROMO_CODEAsync();

            //下載短帶類型細項代碼檔
            client.GetTBPGM_PROMO_CATD_CODECompleted += new EventHandler<GetTBPGM_PROMO_CATD_CODECompletedEventArgs>(client_GetTBPGM_PROMO_CATD_CODECompleted);
            client.GetTBPGM_PROMO_CATD_CODEAsync();

            //查詢是否有相同名稱的短帶
            client.QUERY_TBPROMO_BYNAME_CHECKCompleted += new EventHandler<QUERY_TBPROMO_BYNAME_CHECKCompletedEventArgs>(client_QUERY_TBPROMO_BYNAME_CHECKCompleted);

            BusyMsg.IsBusy = true;  
        }

        private void ClassToForm()  //將前一頁點選的短帶資料繫集到畫面上
        {
            this.gdPROMO.DataContext = m_FormData;
            if (m_FormData.FSEXPIRE_DATE_ACTION == "Y")
                cbDELETE.IsChecked = true;
            else
                cbDELETE.IsChecked = false;
            if (m_FormData.FDEXPIRE_DATE > Convert.ToDateTime("1911-01-01"))
            {
                dpEXPIRE.Text = m_FormData.FDEXPIRE_DATE.ToShortDateString();
            }
            else
            {
                dpEXPIRE.Text = "";
            }
            
        }

        #region 實作

        //實作-下載短帶類型細項代碼檔
        void client_GetTBPGM_PROMO_CATD_CODECompleted(object sender, GetTBPGM_PROMO_CATD_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    //m_CodeDataBuyD = new List<Class_CODE>(e.Result);

                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        ComboBoxItem cbiPROMOCATD = new ComboBoxItem();
                        cbiPROMOCATD.Content = CodeData.NAME;
                        cbiPROMOCATD.Tag = CodeData.ID;
                        cbiPROMOCATD.DataContext = CodeData.IDD;
                        this.cbPROMOCATIDD.Items.Add(cbiPROMOCATD);   //先都產生在隱藏起來的細項代碼combobox裡                   
                        m_CodeDataCateD.Add(CodeData);
                    }
                }
            }
        }

        //實作-下載代碼檔
        void client_GetTBPGM_PROMO_CODECompleted(object sender, GetTBPGM_PROMO_CODECompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    //版本代碼，空白要自己帶入
                    ComboBoxItem cbiPROMOVERNull = new ComboBoxItem();
                    cbiPROMOVERNull.Content = " ";
                    cbiPROMOVERNull.Tag = "";
                    this.cbPROMOVERID.Items.Add(cbiPROMOVERNull);

                    //製作目的代碼，空白要自己帶入
                    ComboBoxItem cbiPROGOBJNull = new ComboBoxItem();
                    cbiPROGOBJNull.Content = " ";
                    cbiPROGOBJNull.Tag = "";
                    this.cbPROGOBJID.Items.Add(cbiPROGOBJNull);

                    //製作單位代碼，空白要自己帶入
                    ComboBoxItem cbiPRDCENTERNull = new ComboBoxItem();
                    cbiPRDCENTERNull.Content = " ";
                    cbiPRDCENTERNull.Tag = "";
                    this.cbPRDCENID.Items.Add(cbiPRDCENTERNull);

                    ////類型大項代碼，空白要自己帶入//後來設定為必填，所以不用給空白
                    //ComboBoxItem cbiPROMOCATNull = new ComboBoxItem();
                    //cbiPROMOCATNull.Content = " ";
                    //cbiPROMOCATNull.Tag = "";
                    //this.cbPROMOCATID.Items.Add(cbiPROMOCATNull);

                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            case "TBZCHANNEL":        //頻道別代碼
                                ComboBoxItem cbiCHANNEL = new ComboBoxItem();
                                cbiCHANNEL.Content = CodeData.NAME;
                                cbiCHANNEL.Tag = CodeData.ID;
                                this.cbCHANNEL_ID.Items.Add(cbiCHANNEL);
                                break;
                            case "TBZPROGOBJ":       //製作目的代碼
                                ComboBoxItem cbiPROGOBJ = new ComboBoxItem();
                                cbiPROGOBJ.Content = CodeData.NAME;
                                cbiPROGOBJ.Tag = CodeData.ID;
                                this.cbPROGOBJID.Items.Add(cbiPROGOBJ);
                                break;
                            case "TBZPRDCENTER":      //製作單位代碼
                                if (CodeData.ID.Trim() != "00000")  //舊資料的代碼要略過
                                {
                                    ComboBoxItem cbiPRDCENTER = new ComboBoxItem();
                                    cbiPRDCENTER.Content = CodeData.NAME;
                                    cbiPRDCENTER.Tag = CodeData.ID;
                                    this.cbPRDCENID.Items.Add(cbiPRDCENTER);
                                }                               
                                break;
                            case "TBZPROMOVER":      //版本代碼
                                ComboBoxItem cbiPROMOVER = new ComboBoxItem();
                                cbiPROMOVER.Content = CodeData.NAME;
                                cbiPROMOVER.Tag = CodeData.ID;
                                this.cbPROMOVERID.Items.Add(cbiPROMOVER);
                                break;
                            case "TBZPROMOTYPE":      //類別
                                ComboBoxItem cbiPROMOTYPE = new ComboBoxItem();
                                cbiPROMOTYPE.Content = CodeData.NAME;
                                cbiPROMOTYPE.Tag = CodeData.ID;
                                this.cbPROMOTYPEID.Items.Add(cbiPROMOTYPE);
                                break;
                            case "TBZPROMOCAT":       //類型代碼
                                ComboBoxItem cbiPROMOCAT = new ComboBoxItem();
                                cbiPROMOCAT.Content = CodeData.NAME;
                                cbiPROMOCAT.Tag = CodeData.ID;
                                this.cbPROMOCATID.Items.Add(cbiPROMOCAT);
                                break;
                            default:
                                break;
                        }
                    }

                    //比對代碼檔   
                    compareCode_PROGID();
                    compareCode_EPISODE();
                    compareCode_ACT();    
                    compareCode_FSPROGOBJID(m_FormData.FSPROGOBJID.ToString().Trim());
                    compareCode_FSPRDCENID(m_FormData.FSPRDCENID.ToString().Trim());
                    compareCode_FSPROMOVERID(m_FormData.FSPROMOVERID.ToString().Trim());
                    compareCode_FSPROMOTYPEID(m_FormData.FSPROMOTYPEID.ToString().Trim());
                    compareCode_FSPROMOCATID(m_FormData.FSPROMOCATID.ToString().Trim());
                    compareCode_FSPROMOCATDID(m_FormData.FSPROMOCATDID.ToString().Trim());
                    compareCode_FSCHANNEL_ID(m_FormData.FSMAIN_CHANNEL_ID.ToString().Trim());
                    //註解by Jarvis 20131202    
                    //compareDel(m_FormData.FSDEL.ToString().Trim());           //刪除註記              
                }
            }
        }

        //實作-修改短帶資料檔資料   
        void client_UPDATE_TBPGM_PROMOCompleted(object sender, UPDATE_TBPGM_PROMOCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    MessageBox.Show("修改短帶基本資料「" + m_strPromoName.ToString() + "」成功！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("修改短帶基本資料「" + m_strPromoName.ToString() + "」失敗！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
        }
        
        //實作-查詢是否有相同名稱的短帶
        void client_QUERY_TBPROMO_BYNAME_CHECKCompleted(object sender, QUERY_TBPROMO_BYNAME_CHECKCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result == false)
                {
                    //修改短帶主檔
                    client.UPDATE_TBPGM_PROMOAsync(FormToClass_PROMO());

                    BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                }
                else
                    MessageBox.Show("已有重複的「短帶名稱」資料，請檢查後重新輸入！", "提示訊息", MessageBoxButton.OK);
            }
            else
                MessageBox.Show("查詢「短帶名稱」資料失敗！", "提示訊息", MessageBoxButton.OK);
        }

        #endregion

        #region 比對代碼檔

        //節目編號
        void compareCode_PROGID()
        {
            tbxPROG_NAME.Text = m_FormData.FSPROG_ID_NAME.Trim();
            tbxPROG_NAME.Tag = m_FormData.FSPROG_ID.Trim();
            m_strID = m_FormData.FSPROG_ID.Trim();
        }

        //集別
        void compareCode_EPISODE()
        {
           tbxEPISODE.Text = m_FormData.SHOW_FNEPISODE.Trim() ;
           lblEPISODE_NAME.Content =  m_FormData.FNEPISODE_NAME.Trim() ;
           m_strEpisode =m_FormData.SHOW_FNEPISODE.Trim() ;
        }

        //活動
        void compareCode_ACT()
        {
            tbxACTIONID.Text = m_FormData.FSACTION_NAME.Trim();
            tbxACTIONID.Tag = m_FormData.FSACTIONID.Trim();
            m_ActID = m_FormData.FSACTIONID.Trim();
        }

        //比對代碼檔_製作單位
        void compareCode_FSPRDCENID(string strID)
        {
            for (int i = 0; i < cbPRDCENID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPRDCENID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPRDCENID.SelectedIndex = i;
                    tbxPRDCEN.Text = getcbi.Content.ToString().Trim();
                    tbxPRDCEN.Tag = getcbi.Tag.ToString().Trim();
                    break;
                }
            }
        }

        //比對代碼檔_製作目的
        void compareCode_FSPROGOBJID(string strID)
        {
            for (int i = 0; i < cbPROGOBJID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGOBJID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGOBJID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_短帶版本
        void compareCode_FSPROMOVERID(string strID)
        {
            for (int i = 0; i < cbPROMOVERID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROMOVERID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROMOVERID.SelectedIndex = i;
                    break;
                }
            }
        }
        
        //比對代碼檔_類別
        void compareCode_FSPROMOTYPEID(string strID)
        {
            for (int i = 0; i < cbPROMOTYPEID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROMOTYPEID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROMOTYPEID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_類型大項
        void compareCode_FSPROMOCATID(string strID)
        {
            for (int i = 0; i < cbPROMOCATID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROMOCATID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROMOCATID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_類型細項
        void compareCode_FSPROMOCATDID(string strID)
        {
            for (int i = 0; i < cbPROMOCATIDDS.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROMOCATIDDS.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROMOCATIDDS.SelectedIndex = i;
                    break;
                }
            }
        }
        
        //比對代碼檔_頻道別
        void compareCode_FSCHANNEL_ID(string strID)
        {
            for (int i = 0; i < cbCHANNEL_ID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbCHANNEL_ID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbCHANNEL_ID.SelectedIndex = i;
                    break;
                }
            }
        }

        //註解by Jarvis 20131202
        //是否為刪除
        //private void compareDel(string strDel)   
        //{
        //    if (strDel == "N")
        //        cbDel.SelectedIndex = 0;
        //    else if (strDel == "Y")
        //        cbDel.SelectedIndex = 1;
        //} 

        //選擇短帶類型，要判斷顯示哪些短帶類型細項
        private void cbPROMOCATID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.cbPROMOCATIDDS.Items.Clear();     //使用前先清空
            string strCATID = ""; //類型大項
            ComboBox cb = sender as ComboBox;
            ComboBoxItem cbm = (ComboBoxItem)cb.SelectedItem;
            strCATID = cbm.Tag.ToString();

            for (int i = 0; i < cbPROMOCATIDD.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROMOCATIDD.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strCATID))
                {
                    ComboBoxItem cbiPROMOCATIDS = new ComboBoxItem();
                    cbiPROMOCATIDS.Content = getcbi.Content;
                    cbiPROMOCATIDS.Tag = getcbi.DataContext;
                    this.cbPROMOCATIDDS.Items.Add(cbiPROMOCATIDS);
                }
            }
        }
            
        #endregion

        #region 檢查畫面資料及搬值

        private Class_PROMO FormToClass_PROMO()
        {
            Class_PROMO obj = new Class_PROMO();

            m_strPromoName = tbxPROMO_NAME.Text.ToString().Trim();                              //短帶名稱
            obj.FSPROMO_ID = tbxPROMO_ID.Text.ToString().Trim();                                //短帶編號，從系統取號
            obj.FSPROMO_NAME = tbxPROMO_NAME.Text.ToString().Trim();                            //短帶名稱
            obj.FSPROMO_NAME_ENG = tbxFROMO_NAME_ENG.Text.ToString().Trim();                    //短帶外文名稱
            obj.FSDURATION = tbxDURATION.Text.ToString().Trim() ;                               //播出長度(秒)
            obj.FSPROG_ID = m_strID;                                                            //節目編號
            obj.Origin_FDEXPIRE_DATE = m_FormData.Origin_FDEXPIRE_DATE;
            obj.Origin_FSEXPIRE_DATE_ACTION = m_FormData.Origin_FSEXPIRE_DATE_ACTION;

           if (m_strEpisode.Trim() == "")                                                       //集別
               obj.FNEPISODE = 0;
            else
               obj.FNEPISODE = Convert.ToInt16(m_strEpisode.Trim());
          
           if (cbPROGOBJID.SelectedIndex != -1)                                                 //製作目的
               obj.FSPROGOBJID = ((ComboBoxItem)cbPROGOBJID.SelectedItem).Tag.ToString();
           else
               obj.FSPROGOBJID ="";

           if (tbxPRDCEN.Text.Trim() != "")                       //製作單位
               obj.FSPRDCENID = tbxPRDCEN.Tag.ToString();
           else
               obj.FSPRDCENID = "";

           obj.FSPRDYYMM = tbxPRDYYMM.Text.ToString().Trim() ;                                  //製作年月
         
           if (cbPROMOTYPEID.SelectedIndex != -1)                                               //類別
               obj.FSPROMOTYPEID =((ComboBoxItem)cbPROMOTYPEID.SelectedItem).Tag.ToString(); 
           else
               obj.FSPROMOTYPEID = "";

            obj.FSACTIONID = m_ActID;                                                           //活動
            
          if (cbPROMOCATID.SelectedIndex != -1)                                                 //類型大類
             obj.FSPROMOCATID = ((ComboBoxItem)cbPROMOCATID.SelectedItem).Tag.ToString();
           else
             obj.FSPROMOCATID = "" ;

          if (cbPROMOCATIDDS.SelectedIndex != -1)                                               //類型細類
              obj.FSPROMOCATDID = ((ComboBoxItem)cbPROMOCATIDDS.SelectedItem).Tag.ToString();
          else
              obj.FSPROMOCATDID ="";

          if (cbPROMOVERID.SelectedIndex != -1)                                                 //版本
              obj.FSPROMOVERID = ((ComboBoxItem)cbPROMOVERID.SelectedItem).Tag.ToString();
          else
              obj.FSPROMOVERID ="";

          obj.FSMEMO = tbxMEMO.Text.ToString().Trim();                                          //備註
          obj.FSMAIN_CHANNEL_ID = UserClass.userData.FSCHANNEL_ID.ToString();                   //頻道別

          //修改by Jarvis 20131202
          //if (cbDel.SelectedIndex != -1)
          //{                                                    //刪除註記
          //    obj.FSDEL = ((ComboBoxItem)cbDel.SelectedItem).Tag.ToString();
          //    obj.FSDELUSER = UserClass.userData.FSUSER_ID.ToString(); //刪除者 
          //}
          //else
          //{
          
            
              obj.FSDEL = "N";
              obj.FSDELUSER = "";         
          //}

          obj.FSWELFARE = tbxWelfare.Text.ToString().Trim();                                    //公益託播編號

                             
          obj.FSUPDATED_BY = UserClass.userData.FSUSER_ID.ToString();                           //修改者
          
          if (dpEXPIRE.Text != "")
              obj.FDEXPIRE_DATE = Convert.ToDateTime(dpEXPIRE.Text).AddHours(23).AddMinutes(59);
          else
              obj.FDEXPIRE_DATE = Convert.ToDateTime("1900-01-01");

          if (cbDELETE.IsChecked == true)
              obj.FSEXPIRE_DATE_ACTION = "Y";
          else
              obj.FSEXPIRE_DATE_ACTION = "N"; 

          m_Queryobj = obj;
          return obj;
        }

        private Boolean Check_PROMO()    //檢查畫面上的欄位是否都填妥
        {
            StringBuilder strMessage = new StringBuilder();
            double douCheck;   //純粹為了判斷是否為數字型態之用   

            if (tbxPROMO_NAME.Text.Trim() == "")
                strMessage.AppendLine("請輸入「短帶名稱」欄位");

            if (tbxDURATION.Text.Trim() == "")
                strMessage.AppendLine("請輸入「播出長度」欄位");
            else
            {
                if (double.TryParse(tbxDURATION.Text.Trim(), out douCheck) == false)
                {
                    strMessage.AppendLine("請檢查「播出長度」欄位必須為數值型態");
                }
                else if (tbxDURATION.Text.Trim().StartsWith("-"))
                {
                    strMessage.AppendLine("請檢查「播出長度」欄位必須為正整數");
                }
            }

            if (cbPROMOTYPEID.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「類別」欄位");

            if (cbPROMOCATID.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「類型大項」及「類型細項」欄位");
            else
            {
                if (cbPROMOCATID.SelectedIndex != -1 && ((ComboBoxItem)(cbPROMOCATID.SelectedItem)).Content.ToString().Trim() != "" && cbPROMOCATIDDS.SelectedIndex == -1) //因為設定外鍵，因此選擇了大項就要設細項
                    strMessage.AppendLine("請輸入「類型細項」欄位");
            }

            if (strMessage.ToString().Trim() == "")
            {
                //註解By Jarvis 20131202
                //if (cbDel.SelectedIndex == 1)
                //{
                //    MessageBoxResult resultMsg = MessageBox.Show("確定要刪除「" + tbxPROMO_NAME.Text.Trim() + "」短帶資料嗎?", "提示訊息", MessageBoxButton.OKCancel);
                //    if (resultMsg == MessageBoxResult.Cancel)
                //        return false;
                //    else
                //        return true;
                //}
                //else
                    return true;
            }
            else
            {
                MessageBoxResult result = MessageBox.Show(strMessage.ToString(), "提示訊息", MessageBoxButton.OK);
                return false;
            }
        }

        #endregion

        //確定
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (Check_PROMO() == false)      //檢查畫面上的欄位是否都填妥
                return;

            //查詢是否有相同短帶名稱的資料
            client.QUERY_TBPROMO_BYNAME_CHECKAsync(tbxPROMO_NAME.Text.ToString().Trim(),tbxPROMO_ID.Text.Trim());       
        }

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
        
        //選取活動
        private void btnAct_Click(object sender, RoutedEventArgs e)
        {
            PRG800_08 PRG800_08_frm = new PRG800_08();
            PRG800_08_frm.Show();

            PRG800_08_frm.Closed += (s, args) =>
            {
                if (PRG800_08_frm.DialogResult == true)
                {
                    tbxACTIONID.Text = PRG800_08_frm.strActIDName_View;
                    tbxACTIONID.Tag = PRG800_08_frm.strActID_View;
                    m_ActID = PRG800_08_frm.strActID_View; 
                }
            };
        }

        //選取節目名稱
        private void btnPROG_Click(object sender, RoutedEventArgs e)
        {
            //PRG100_01
            PROGDATA_VIEW PROGDATA_VIEW_frm = new PROGDATA_VIEW();
            PROGDATA_VIEW_frm.Show();

            PROGDATA_VIEW_frm.Closed += (s, args) =>
            {
                if (PROGDATA_VIEW_frm.DialogResult == true)
                {
                    tbxPROG_NAME.Text = PROGDATA_VIEW_frm.strProgName_View;
                    tbxPROG_NAME.Tag = PROGDATA_VIEW_frm.strProgID_View;

                    tbxEPISODE.Text = "";
                    lblEPISODE_NAME.Content = "";
                    m_strEpisode = "0";                
                    m_strID = PROGDATA_VIEW_frm.strProgID_View;
                }
            };
        }

        //選取子集名稱
        private void btnEPISODE_Click(object sender, RoutedEventArgs e)
        {
            if (tbxPROG_NAME.Text.ToString().Trim() != "" && tbxPROG_NAME.Tag.ToString().Trim() != "")
            {
                PRG.PRG200_07 PRG200_07_frm = new PRG.PRG200_07(tbxPROG_NAME.Tag.ToString().Trim(), tbxPROG_NAME.Text.ToString().Trim());
                PRG200_07_frm.Show();

                PRG200_07_frm.Closed += (s, args) =>
                {
                    if (PRG200_07_frm.DialogResult == true)
                    {
                        m_strEpisode = PRG200_07_frm.m_strEpisode_View;
                        tbxEPISODE.Text = PRG200_07_frm.m_strEpisode_View;
                        lblEPISODE_NAME.Content = PRG200_07_frm.m_strProgDName_View;
                    }
                };
            }
            else
                MessageBox.Show("請先選擇節目！", "提示訊息", MessageBoxButton.OK);
        }

        //選取製作單位
        private void btnPRDCEN_Click(object sender, RoutedEventArgs e)
        {
            PRG100_11 PRG100_11_frm = new PRG100_11();
            PRG100_11_frm.Show();

            PRG100_11_frm.Closed += (s, args) =>
            {
                if (PRG100_11_frm.DialogResult == true)
                {
                    tbxPRDCEN.Text = PRG100_11_frm.strPRDCENName_View;
                    tbxPRDCEN.Tag = PRG100_11_frm.strPRDCENID_View;
                }
            };
        }

    }
}

