﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Xml.Linq;
using System.IO;
using System.Text;
using PTS_MAM3.WSPROGRACE;
using PTS_MAM3.PROG_M;
using System.Windows.Data;
using System.Windows.Browser;
using System.Threading;

namespace PTS_MAM3.ProgData
{
    public partial class PROGRACE : Page
    {

        WSPROGRACESoapClient client = new WSPROGRACESoapClient();   //產生新的代理類別
        //List<Class_CODE> m_CodeData = new List<Class_CODE>();     //代碼檔集合 
        string m_strProgName = "";                                  //暫存節目名稱 
        string m_strEpisode = "";                                   //暫存集別
        Class_PROGRACE m_ReuseQueryobj = new Class_PROGRACE();      //取得查詢頁面的查詢條件obj
        Int32 m_intPageCount = -1;                                  //紀錄PageIndex       
        List<Class_PROGRACE> ListExportRace = new List<Class_PROGRACE>();//匯出Excel

        public PROGRACE()
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面
        }

        public PROGRACE(string strProgID)
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面
            m_ReuseQueryobj.FSPROG_ID = strProgID;
            client.fnQUERY_TBPROGRACEAsync(m_ReuseQueryobj);
            BusyMsg.IsBusy = true;
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        void InitializeForm() //初始化本頁面
        {           
            //取得全部參展得獎記錄資料
            client.fnGetTBPROGRACE_ALLCompleted += new EventHandler<fnGetTBPROGRACE_ALLCompletedEventArgs>(client_fnGetTBPROGRACE_ALLCompleted);
            //client.fnGetTBPROGRACE_ALLAsync();  //實作-取得全部參展得獎記錄資料  //先註解，因為不要Load全部 
            
            //刪除參展得獎記錄資料
            client.fnDELETE_TBPROGRACECompleted += new EventHandler<fnDELETE_TBPROGRACECompletedEventArgs>(client_fnDELETE_TBPROGRACECompleted);

            //查詢參展得獎資料
            client.fnQUERY_TBPROGRACECompleted += new EventHandler<fnQUERY_TBPROGRACECompletedEventArgs>(client_fnQUERY_TBPROGRACECompleted);
            
            //匯出Excel
            //client.ExportToExcelCompleted += new EventHandler<ExportToExcelCompletedEventArgs>(client_ExportToExcelCompleted);
            
            //匯出文字檔
            //client.ExportToTxtCompleted += new EventHandler<ExportToTxtCompletedEventArgs>(client_ExportToTxtCompleted);
        
        }
             
        #region 實作

        //實作-取得全部參展得獎記錄資料
        void client_fnGetTBPROGRACE_ALLCompleted(object sender, fnGetTBPROGRACE_ALLCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    //宣告PagedCollectionView,將撈取的資料放入
                    PagedCollectionView pcv = new PagedCollectionView(e.Result);
                    //指定DataGrid以及DataPager的內容
                    DGPROG_RaceList.ItemsSource = pcv;
                    DataPager.Source = pcv;
                    ListExportRace = e.Result;
                }                
            } 
        }

        ////實作-下載代碼檔
        //void client_fnGetTBPROGRACE_CODECompleted(object sender, fnGetTBPROGRACE_CODECompletedEventArgs e)
        //{
        //    if (e.Error == null)
        //    {
        //        if (e.Result != null && e.Result.Count > 0)               
        //        {
        //            for (int i = 0; i < e.Result.Count; i++)
        //            {
        //                m_CodeData.Add(e.Result[i]);
        //            }                   
        //        }               
        //    }             
        //}

        //實作-刪除參展得獎記錄資料

        void client_fnDELETE_TBPROGRACECompleted(object sender, fnDELETE_TBPROGRACECompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    MessageBox.Show("刪除參展得獎資料「" + m_strProgName.ToString() + "-" + m_strEpisode + "集」成功！", "提示訊息", MessageBoxButton.OK);
                    client.fnQUERY_TBPROGRACEAsync(m_ReuseQueryobj);        //如果有刪除資料，要用查詢條件去查詢
                    BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                }
                else
                {
                    MessageBox.Show("刪除參展得獎資料「" + m_strProgName.ToString() + "-" + m_strEpisode + "集」失敗！", "提示訊息", MessageBoxButton.OK);
                }
            }
        }

        //實作-查詢參展得獎資料
        void client_fnQUERY_TBPROGRACECompleted(object sender, fnQUERY_TBPROGRACECompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)  //若是有查詢結果，則顯示，不然就請使用者重新查詢
                {
                    PagedCollectionView pcv = new PagedCollectionView(e.Result);
                    DGPROG_RaceList.ItemsSource = pcv;
                    DataPager.Source = pcv;
                    ListExportRace = e.Result;

                    if (m_intPageCount != -1)        //設定修改後查詢完要回到哪一頁
                        DataPager.PageIndex = m_intPageCount;
                }
                else
                {
                    PagedCollectionView pcv = new PagedCollectionView(new List<Class_PROGRACE>());
                    //指定DataGrid以及DataPager的內容
                    DGPROG_RaceList.ItemsSource = pcv;
                    DataPager.Source = pcv;                    
                }
            }
        }

        ////實作-匯出Excel
        //void client_ExportToExcelCompleted(object sender, ExportToExcelCompletedEventArgs e)
        //{
        //    BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

        //    if (e.Error == null && e.Result != "")
        //    {
        //        if (e.Result.StartsWith("Error") == true)
        //            MessageBox.Show("參展記錄資料匯出異常，無法匯出，" + e.Result, "提示訊息", MessageBoxButton.OK);
        //        else
        //            MessageBox.Show("參展記錄資料匯出成功，請檢查C磁碟底下的檔案", "提示訊息", MessageBoxButton.OK);
        //    }
        //    else
        //        MessageBox.Show("參展記錄資料匯出異常，無法匯出", "提示訊息", MessageBoxButton.OK);           
        //}

        ////實作-匯出文字檔
        //void client_ExportToTxtCompleted(object sender, ExportToTxtCompletedEventArgs e)
        //{
        //    string strPath = "";    //檔案位置
        //    BusyMsg.IsBusy = false; //關閉忙碌的Loading訊息

        //    if (e.Error == null && e.Result != "")
        //    {
        //        if (e.Result.StartsWith("Error") == true)
        //            MessageBox.Show("參展記錄資料匯出異常，無法匯出，" + e.Result, "提示訊息", MessageBoxButton.OK);
        //        else
        //        {
        //            strPath = e.Result;
        //            //Thread.Sleep(2000);//原本的作法透過文件下載
        //            //if (HtmlPage.IsPopupWindowAllowed)
        //            //    HtmlPage.PopupWindow(new Uri("REPORT.aspx?url=" + e.Result), "_blank", null);
        //            //else
        //            //    HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + e.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,resizable=yes");

        //            WCF100DL.WCF100Client WCF100client = new WCF100DL.WCF100Client();                    
        //            SaveFileDialog dialog = new SaveFileDialog();
        //            dialog.Filter = "csv|*.csv";
        //            dialog.FilterIndex = 1;
        //            dialog.ShowDialog();
        //            try
        //            {    
        //                WCF100client.DownloadAsync(strPath);    
        //                WCF100client.DownloadCompleted += (s1, args1) =>    
        //                {    
        //                    if (args1.Error != null)    
        //                    {    
        //                        MessageBox.Show(args1.Error.ToString());    
        //                        return;    
        //                    }    

        //                    if (args1.Result != null)    
        //                    {    
        //                        try    
        //                        {    
        //                            Stream fileobj = dialog.OpenFile();    
        //                            if (fileobj == null)    
        //                            {    
        //                            }    
        //                            else    
        //                            {    
        //                                fileobj.Write(args1.Result.File, 0, args1.Result.File.Length);    
        //                                fileobj.Close();    
        //                            }    
        //                        }    
        //                        catch (Exception ex)    
        //                        {    
        //                            return;    
        //                        }    
        //                    }    
        //                };    
        //            }
        //            catch (Exception ex)
        //            { MessageBox.Show(ex.ToString().Trim()); }                   
        //        }
        //            //MessageBox.Show("參展記錄資料匯出成功，請檢查C磁碟底下的檔案", "提示訊息", MessageBoxButton.OK);           
        //    }
        //    else
        //        MessageBox.Show("參展記錄資料匯出異常，無法匯出", "提示訊息", MessageBoxButton.OK);      
        //}

        #endregion       
        
        //新增
        private void btnPROGAdd_Click(object sender, RoutedEventArgs e)
        {
            if (checkPermission() == false)
            {
                MessageBox.Show("無參展記錄資料維護權限，無法新增", "提示訊息", MessageBoxButton.OK);
                return;
            }

            PROGRACE_Add PROGRACE_Add_frm = new PROGRACE_Add();
            PROGRACE_Add_frm.Show();

            PROGRACE_Add_frm.Closing += (s, args) =>
            {
                if (PROGRACE_Add_frm.DialogResult == true)
                {
                    m_ReuseQueryobj = new Class_PROGRACE();     //新增後，把查詢條件清空
                    client.fnQUERY_TBPROGRACEAsync(PROGRACE_Add_frm.m_Queryobj);
                    BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                }                    
            };
        }

        //修改
        private void btnPROGModify_Click(object sender, RoutedEventArgs e)
        {
            if (checkPermission() == false)
            {
                MessageBox.Show("無參展記錄資料維護權限，無法修改", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //判斷是否有選取DataGrid
            if (DGPROG_RaceList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預修改的參展記錄資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            m_intPageCount = DataPager.PageIndex;       //取得PageIndex

            PROGRACE_Edit PROGRACE_Edit_frm = new PROGRACE_Edit(((Class_PROGRACE)DGPROG_RaceList.SelectedItem));
            PROGRACE_Edit_frm.Show();

            PROGRACE_Edit_frm.Closing += (s, args) =>
            {
                if (PROGRACE_Edit_frm.DialogResult == true)
                {
                    if (m_ReuseQueryobj.FSPROG_ID == null)
                    {
                        client.fnQUERY_TBPROGRACEAsync(PROGRACE_Edit_frm.m_Queryobj);    //如果不是就要用要用修改去查詢
                        BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                    }                        
                    else
                    {
                        client.fnQUERY_TBPROGRACEAsync(m_ReuseQueryobj);                //如果先查詢再修改就要保留查詢的條件                        
                        BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                    }                        
                }
            };
        }

        //查詢
        private void btnPROGQuery_Click(object sender, RoutedEventArgs e)
        {
            PROGRACE_Query PROGRACE_Query_frm = new PROGRACE_Query();
            PROGRACE_Query_frm.Show();

            PROGRACE_Query_frm.Closed += (s, args) =>
            {
                if (PROGRACE_Query_frm.m_ListFormData.Count != 0)
                {
                    //將資料bind到DataPager，先註解
                    //this.DGPROG_RaceList.DataContext = null;
                    //this.DGPROG_RaceList.DataContext = PROGRACE_Query_frm.m_ListFormData;
                    //this.DGPROG_RaceList.SelectedIndex = 0;

                    //宣告PagedCollectionView,將撈取的資料放入
                    PagedCollectionView pcv = new PagedCollectionView(PROGRACE_Query_frm.m_ListFormData);
                    //指定DataGrid以及DataPager的內容
                    DGPROG_RaceList.ItemsSource = pcv;
                    DataPager.Source = pcv;
                    ListExportRace = PROGRACE_Query_frm.m_ListFormData;

                    m_ReuseQueryobj = PROGRACE_Query_frm.m_Queryobj;  //紀錄查詢的條件
                }
            };
        }

        //刪除
        private void btnPROGDelete_Click(object sender, RoutedEventArgs e)
        {
            if (checkPermission() == false)
            {
                MessageBox.Show("無參展記錄資料維護權限，無法刪除", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //判斷是否有選取DataGrid
            if (DGPROG_RaceList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預刪除的參展記錄資料", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else
            {
                m_strProgName = ((Class_PROGRACE)DGPROG_RaceList.SelectedItem).FSPROG_ID_NAME.ToString().Trim();
                m_strEpisode = ((Class_PROGRACE)DGPROG_RaceList.SelectedItem).FNEPISODE.ToString().Trim();

                MessageBoxResult result = MessageBox.Show("確定要刪除「" + m_strProgName.ToString() + "-" + m_strEpisode + "集」?", "提示訊息", MessageBoxButton.OKCancel);
                if (result == MessageBoxResult.OK)
                {
                    m_intPageCount = DataPager.PageIndex;       //取得PageIndex
                    client.fnDELETE_TBPROGRACEAsync(((Class_PROGRACE)DGPROG_RaceList.SelectedItem).AUTO.ToString().Trim(), UserClass.userData.FSUSER_ID.ToString());
                    BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                }
            }
        }



        //檢視
        private void btnView_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGPROG_RaceList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預檢視的參展記錄資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            PRG500_03 PRG500_03_frm = new PRG500_03(((Class_PROGRACE)DGPROG_RaceList.SelectedItem));
            PRG500_03_frm.Show();
        }

        //檢查權限
        private Boolean checkPermission()
        {
            if (ModuleClass.getModulePermissionByName("02", "參展記錄資料維護") == false)
                return false;
            else
                return true;
        }

        //匯出參展記錄
        private void btnExcel_Click(object sender, RoutedEventArgs e)
        {   
            try
            {
                if (ListExportRace.Count == 0)
                {
                    MessageBox.Show("請先查詢要匯出的參展記錄資料再匯出", "提示訊息", MessageBoxButton.OK);
                    return;
                }

                //this.DGPROG_RaceList.ToCSV(); //另一個方法，把整個DataGrid印出來，但是如果有設分頁只會印同一頁

                string strExport = "";
                strExport = GenerateCsv();

                SaveFileDialog sfd = new SaveFileDialog()
                {
                    DefaultExt = "csv",
                    Filter = "CSV Files (*.csv)|*.csv|All files (*.*)|*.*",
                    FilterIndex = 1
                };

                if (sfd.ShowDialog() == true)
                {
                    using (Stream stream = sfd.OpenFile())
                    {
                        using (StreamWriter writer = new StreamWriter(stream, System.Text.UnicodeEncoding.Unicode))
                        {
                            writer.Write(strExport);
                            writer.Close();
                        }
                        stream.Close();
                        MessageBox.Show("參展記錄資料匯出成功", "提示訊息", MessageBoxButton.OK);
                    }
                }    
            }
            catch (Exception ex)
            {
                MessageBox.Show("參展記錄資料匯出異常，無法匯出，錯誤原因" + ex.Message, "提示訊息", MessageBoxButton.OK);                
            }
        }

        //產生匯出檔案內容
        private string GenerateCsv()
        {            
            StringBuilder content = new StringBuilder("");
            int intCount = 1;
            try
            {
                content.Append("\t編號");
                content.Append("\t節目編號");
                content.Append("\t節目名稱");
                content.Append("\t子集名稱");
                content.Append("\t集別");
                content.Append("\t參展狀態");
                content.Append("\t參展區域");
                content.Append("\t屆數");
                content.Append("\t影展名稱");
                content.Append("\t入圍或得獎日期");
                content.Append("\t獎項名稱");
                content.Append("\t參展節目名稱");
                content.Append("\t節目來源");
                content.Append("\t所屬頻道");
                content.Append("\r\n");//換行

                foreach (Class_PROGRACE objRace in ListExportRace)
                {
                    content.Append(intCount + "\t");

                    if (objRace.FSPROG_ID.StartsWith("0"))
                        content.Append("'" + objRace.FSPROG_ID + "\t");
                    else
                        content.Append(objRace.FSPROG_ID + "\t");

                    content.Append(objRace.FSPROG_ID_NAME + "\t");
                    content.Append(objRace.FNEPISODE_NAME + "\t");
                    content.Append(objRace.FNEPISODE + "\t");
                    content.Append(objRace.FSPROGSTATUS + "\t");
                    content.Append(objRace.FSAREA_NAME + "\t");

                    if (objRace.FNPERIOD == 0)
                        content.Append("\t");
                    else
                        content.Append(objRace.FNPERIOD + "\t");

                    content.Append(objRace.FSPROGRACE.Replace("\n", " ") + "\t");
                    content.Append(objRace.SHOW_FDRACEDATE + "\t");
                    content.Append(objRace.FSPROGWIN.Replace("\n", " ") + "\t");
                    content.Append(objRace.FSPROGNAME.Replace("\n", " ") + "\t");
                    content.Append(objRace.FSPROGSRC_NAME + "\t");
                    content.Append(objRace.FSCHANNEL_ID_NAME + "\t");
                    content.Append("\r\n"); //換行
                    intCount++;
                }

                return content.ToString().Trim();
            }
            catch (Exception ex)
            {
                MessageBox.Show("參展記錄資料匯出異常，無法匯出，錯誤原因" + ex.Message, "提示訊息", MessageBoxButton.OK);
                return "";
            }  
        }
             
    }
}
