﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSPROGRACE;
using PTS_MAM3.ProgData;
using DataGridDCTest;

namespace PTS_MAM3.PRG
{
    public partial class PRG500_05 : ChildWindow
    {
        WSPROGRACESoapClient client = new WSPROGRACESoapClient(); //產生新的代理類別

        public string strID_View = "";                //選取的參展區域編號
        public string strName_View = "";              //選取的參展區域名稱

        public PRG500_05()
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面
        }

        void InitializeForm() //初始化本頁面
        {

            //查詢參展區域資料By參展區域名稱
            client.GetTBPROGRACE_AREA_CODE_BYNAMECompleted += new EventHandler<GetTBPROGRACE_AREA_CODE_BYNAMECompletedEventArgs>(client_GetTBPROGRACE_AREA_CODE_BYNAMECompleted);
            client.GetTBPROGRACE_AREA_CODE_BYNAMEAsync("");//第一次傳空值查全部
            LoadBusy();
        }
        
        void LoadBusy()
        {
            BusyMsg.IsBusy = true;  //關閉忙碌的Loading訊息
            btnSearch.IsEnabled = false;
            btnAll.IsEnabled = false;
            tbxPROGRACENAME.IsEnabled = false;
        }

        void LoadBusyUnLock()
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息
            btnSearch.IsEnabled = true;
            btnAll.IsEnabled = true;
            tbxPROGRACENAME.IsEnabled = true;
            tbxPROGRACENAME.Focus();
        }

        //實作-查詢參展區域資料By參展區域名稱
        void client_GetTBPROGRACE_AREA_CODE_BYNAMECompleted(object sender, GetTBPROGRACE_AREA_CODE_BYNAMECompletedEventArgs e)
        {
            LoadBusyUnLock();

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    this.DGPROGRACEList.DataContext = e.Result;

                    if (this.DGPROGRACEList.DataContext != null)
                    {
                        DGPROGRACEList.SelectedIndex = 0;
                    }
                }
                else
                {
                    MessageBox.Show("查無參展區域資料，請重新輸入條件查詢！", "提示訊息", MessageBoxButton.OK);
                    return;
                }
            } 
        }

        //確定
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGPROGRACEList.SelectedItem == null)
            {
                MessageBox.Show("請選擇製作單位資料", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else
            {
                strID_View = ((Class_CODE)DGPROGRACEList.SelectedItem).ID.ToString();
                strName_View = ((Class_CODE)DGPROGRACEList.SelectedItem).NAME.ToString();            
                this.DialogResult = true;
            }
        }

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
       
        //查詢
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            if (tbxPROGRACENAME.Text.Trim() != "")
            {                
                client.GetTBPROGRACE_AREA_CODE_BYNAMEAsync(tbxPROGRACENAME.Text.Trim());
                LoadBusy();
            }
            else
                MessageBox.Show("請輸入條件查詢！", "提示訊息", MessageBoxButton.OK);        
        }

        //重新查詢
        private void btnAll_Click(object sender, RoutedEventArgs e)
        {
            tbxPROGRACENAME.Text = "";

            //取得全部節目資料
            client.GetTBPROGRACE_AREA_CODE_BYNAMEAsync("");
            LoadBusy();
        }

        //連點事件
        private void RowDoubleClick(object sender, DataGridDCTest.DataGridRowClickedArgs e)
        {
            DoubleClickDataGrid dcdg = sender as DoubleClickDataGrid;

            strID_View = ((Class_CODE)DGPROGRACEList.SelectedItem).ID.ToString();
            strName_View = ((Class_CODE)DGPROGRACEList.SelectedItem).NAME.ToString();
            this.DialogResult = true;
        }

        //輸入完查詢內容後按下enter即可查詢
        private void tbxPROMONAME_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                btnSearch_Click(sender, e);
            }
        }

    }
}

