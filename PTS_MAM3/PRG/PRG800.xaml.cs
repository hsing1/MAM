﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using PTS_MAM3.WSPROMO;
using PTS_MAM3.ProgData;
using System.Windows.Data;

namespace PTS_MAM3.PRG
{
    public partial class PRG800 : Page
    {
        WSPROMOSoapClient client = new WSPROMOSoapClient();         //產生新的代理類別
        Class_PROMO m_ReuseQueryobj = new Class_PROMO();            //取得查詢頁面的查詢條件obj
        Int32 m_intPageCount = -1;                                  //紀錄PageIndex
        string m_strPromoName = "";                                 //暫存Promo名稱
        string m_strPromoID = "";                                   //暫存PromoID
        Class_PROMO m_Deleteobj = new Class_PROMO();                //刪除的暫存obj

        public PRG800()
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面
        }

        void InitializeForm() //初始化本頁面
        {
            //取得全部短帶資料
            client.GetTBPGM_PROMO_ALLCompleted += new EventHandler<GetTBPGM_PROMO_ALLCompletedEventArgs>(client_GetTBPGM_PROMO_ALLCompleted);
            //client.GetTBPGM_PROMO_ALLAsync();   //一開始不Load全部資料    

            //查詢短帶資料
            client.QUERY_TBPGM_PROMOCompleted += new EventHandler<QUERY_TBPGM_PROMOCompletedEventArgs>(client_QUERY_TBPGM_PROMOCompleted);

            //刪除短帶資料
            client.DELETE_TBPGM_PROMOCompleted += new EventHandler<DELETE_TBPGM_PROMOCompletedEventArgs>(client_DELETE_TBPGM_PROMOCompleted);

            //檢查刪除短帶資料
            client.DELETE_TBPROMO_CheckCompleted += new EventHandler<DELETE_TBPROMO_CheckCompletedEventArgs>(client_DELETE_TBPROMO_CheckCompleted);
        }

        //實作-取得全部短帶資料
        void client_GetTBPGM_PROMO_ALLCompleted(object sender, GetTBPGM_PROMO_ALLCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    //宣告PagedCollectionView,將撈取的資料放入
                    PagedCollectionView pcv = new PagedCollectionView(e.Result);
                    //指定DataGrid以及DataPager的內容
                    DGPROMOList.ItemsSource = pcv;
                    DataPager.Source = pcv;
                }
            }
        }

        //實作-查詢短帶資料
        void client_QUERY_TBPGM_PROMOCompleted(object sender, QUERY_TBPGM_PROMOCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)  //若是有查詢結果，則顯示，不然就請使用者重新查詢
                {
                    PagedCollectionView pcv = new PagedCollectionView(e.Result);
                    DGPROMOList.ItemsSource = pcv;
                    DataPager.Source = pcv;

                    if (m_intPageCount != -1)        //設定修改後查詢完要回到哪一頁
                        DataPager.PageIndex = m_intPageCount;
                }
                else
                {
                    PagedCollectionView pcv = new PagedCollectionView(new List<Class_PROMO>());
                    //指定DataGrid以及DataPager的內容
                    DGPROMOList.ItemsSource = pcv;
                    DataPager.Source = pcv;
                }
            }
        }

        //實作-刪除短帶資料
        void client_DELETE_TBPGM_PROMOCompleted(object sender, DELETE_TBPGM_PROMOCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;

            if (e.Error == null)
            {
                if (e.Result.bolResult == true)
                {
                    MessageBox.Show("刪除資料「" + m_strPromoName + "」成功！", "提示訊息", MessageBoxButton.OK);
                    client.QUERY_TBPGM_PROMOAsync(m_ReuseQueryobj);        //如果有刪除資料，要用查詢條件去查詢
                    BusyMsg.IsBusy = true;
                }
                else
                {
                    MessageBox.Show("刪除資料「" + m_strPromoName + "」失敗！\n失敗原因：" + e.Result.strErrorMsg, "提示訊息", MessageBoxButton.OK);
                }
            }
        }

        //實作-檢查刪除短帶資料
        void client_DELETE_TBPROMO_CheckCompleted(object sender, DELETE_TBPROMO_CheckCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)  //若是有查詢結果，則顯示
                {
                    PRG.PRG100_09 PRG100_09_frm = new PRG.PRG100_09(e.Result);
                    PRG100_09_frm.Show();
                }
                else
                {
                    //刪除短帶資料
                    client.DELETE_TBPGM_PROMOAsync(m_strPromoID, UserClass.userData.FSUSER_ID.ToString());
                }
            }
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        //修改
        private void btnPROGModify_Click(object sender, RoutedEventArgs e)
        {
            if (checkPermission() == false)
            {
                MessageBox.Show("無短帶資料維護權限，無法修改", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //判斷是否有選取DataGrid
            if (DGPROMOList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預修改的短帶資料", "提示訊息", MessageBoxButton.OK);
                return;
            }
            Class_PROMO selectedPromo = ((Class_PROMO)DGPROMOList.SelectedItem);

            if (selectedPromo.FSDEL == "Y")
            {
                MessageBox.Show("無法修改已刪除的短帶資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            PRG800_02 PRG800_02_frm = new PRG800_02(selectedPromo);
            PRG800_02_frm.Show();

            PRG800_02_frm.Closing += (s, args) =>
            {
                if (PRG800_02_frm.DialogResult == true)
                {
                    if (m_ReuseQueryobj.FSPROG_ID == null)
                        client.QUERY_TBPGM_PROMOAsync(PRG800_02_frm.m_Queryobj);        //如果不是就要用要用修改去查詢
                    else
                        client.QUERY_TBPGM_PROMOAsync(m_ReuseQueryobj);                 //如果先查詢再修改就要保留查詢的條件 

                    BusyMsg.IsBusy = true;
                }
            };
        }

        //查詢
        private void btnPROGQuery_Click(object sender, RoutedEventArgs e)
        {
            PRG800_04 PRG800_04_frm = new PRG800_04();
            PRG800_04_frm.Show();

            PRG800_04_frm.Closed += (s, args) =>
            {
                if (PRG800_04_frm.m_ListFormData.Count != 0)
                {
                    //宣告PagedCollectionView,將撈取的資料放入
                    PagedCollectionView pcv = new PagedCollectionView(PRG800_04_frm.m_ListFormData);

                    //指定DataGrid以及DataPager的內容
                    DGPROMOList.ItemsSource = pcv;
                    DataPager.Source = pcv;

                    m_ReuseQueryobj = PRG800_04_frm.m_Queryobj;  //紀錄查詢的條件
                }
            };
        }

        //新增
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (checkPermission() == false)
            {
                MessageBox.Show("無短帶資料維護權限，無法新增", "提示訊息", MessageBoxButton.OK);
                return;
            }

            PRG800_01 PRG800_01_Add_frm = new PRG800_01();
            PRG800_01_Add_frm.Show();

            PRG800_01_Add_frm.Closing += (s, args) =>
            {
                if (PRG800_01_Add_frm.DialogResult == true)
                {
                    m_ReuseQueryobj = new Class_PROMO();     //新增後，把查詢條件清空
                    client.QUERY_TBPGM_PROMOAsync(PRG800_01_Add_frm.m_Queryobj);   //更新  
                    BusyMsg.IsBusy = true;
                }
            };
        }

        //複製
        private void btnCopy_Click(object sender, RoutedEventArgs e)
        {
            if ((Class_PROMO)DGPROMOList.SelectedItem != null)
            {
                PRG800_01 PRG800_01_Add_frm = new PRG800_01((Class_PROMO)DGPROMOList.SelectedItem);
                PRG800_01_Add_frm.Show();

                PRG800_01_Add_frm.Closing += (s, args) =>
                {
                    if (PRG800_01_Add_frm.DialogResult == true)
                    {
                        m_ReuseQueryobj = new Class_PROMO();     //新增後，把查詢條件清空
                        client.QUERY_TBPGM_PROMOAsync(PRG800_01_Add_frm.m_Queryobj);   //更新  
                        BusyMsg.IsBusy = true;
                    }
                };
            }
            else { MessageBox.Show("請選擇要複製的短帶資料!!"); }
        }

        //刪除
        private void btnPROGDelete_Click(object sender, RoutedEventArgs e)
        {
            if (checkPermission() == false)
            {
                MessageBox.Show("無短帶資料維護權限，無法刪除", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //判斷是否有選取DataGrid
            if (DGPROMOList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預刪除的短帶資料", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else
            {
                Class_PROMO selectedPromo = ((Class_PROMO)DGPROMOList.SelectedItem);
                if (selectedPromo.FSDEL == "Y")
                {
                    MessageBox.Show("無法刪除已刪除的短帶資料", "提示訊息", MessageBoxButton.OK);
                    return;

                }
                m_strPromoName = selectedPromo.FSPROMO_NAME.ToString().Trim();
                m_strPromoID = selectedPromo.FSPROMO_ID.ToString().Trim();

                MessageBoxResult result = MessageBox.Show("確定要刪除「" + m_strPromoName + "」?", "提示訊息", MessageBoxButton.OKCancel);
                if (result == MessageBoxResult.OK)
                {
                    m_intPageCount = DataPager.PageIndex;       //取得PageIndex

                    //判斷刪除宣傳帶基本資料要依序檢查各資料表
                    client.DELETE_TBPROMO_CheckAsync(m_strPromoID, UserClass.userData.FSUSER_ID.ToString());
                    BusyMsg.IsBusy = true;
                }
            }
        }

        //檢視
        private void btnPROGView_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGPROMOList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預檢視的短帶資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            PRG800_03 PRG800_03_frm = new PRG800_03(((Class_PROMO)DGPROMOList.SelectedItem));
            PRG800_03_frm.Show();
        }

        //認定基本資料
        private void btnPROMO_IDENTIFIED_Click(object sender, RoutedEventArgs e)
        {
            if (ModuleClass.getModulePermissionByName("01", "節目資料認定") == true)
            {
                //判斷是否有選取DataGrid
                if (DGPROMOList.SelectedItem == null)
                {
                    MessageBox.Show("請選擇預認定的短帶資料", "提示訊息", MessageBoxButton.OK);
                    return;
                }


                Class_PROMO obj = new Class_PROMO();
                obj = (Class_PROMO)DGPROMOList.SelectedItem;

                if (obj.FSDEL == "Y")
                {
                    MessageBox.Show("無法認定已刪除的短帶資料", "提示訊息", MessageBoxButton.OK);
                    return;
                }

                PRG100_13 PRG100_13_frm = new PRG100_13("P", obj.FSPROMO_ID.Trim(), obj.FSPROMO_NAME.Trim(), obj.FSMAIN_CHANNEL_ID.Trim(), obj.FNDEP_ID.ToString().Trim());
                PRG100_13_frm.Show();

                PRG100_13_frm.Closing += (s, args) =>
                {
                    if (PRG100_13_frm.DialogResult == true)
                    {
                        client.QUERY_TBPGM_PROMOAsync(m_ReuseQueryobj);
                        BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息 
                    }

                };
            }
            else
                MessageBox.Show("無短帶資料認定權限，無法修改", "提示訊息", MessageBoxButton.OK);
        }

        //檢查權限
        private Boolean checkPermission()
        {
            if (ModuleClass.getModulePermissionByName("A5", "短帶資料維護") == false)
                return false;
            else
                return true;
        }

        private void btnDragon_Click(object sender, RoutedEventArgs e)
        {
            PRG900 p900 = new PRG900();
            p900.Closed += (s, args) =>
            {
                if (p900.DialogResult == true)
                {
                    client.QUERY_TBPGM_PROMOAsync(p900.newPromo);
                }
            };
            p900.Show();

        }

        void p900_Closed(object sender, EventArgs e)
        {


        }



    }

    public class FSDELTOChinessConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string oriValue = value.ToString();

            if (oriValue == "Y")
            {
                return "是";
            }
            else
            {
                return "否";
            }

        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }
}
