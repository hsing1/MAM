﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSPROGLINK;
using System.Text;

namespace PTS_MAM3.ProgData
{
    public partial class PROGLINK_Query : ChildWindow
    {
        WSPROGLINKSoapClient client = new WSPROGLINKSoapClient();
        public List<Class_PROGLINK> m_ListFormData = new List<Class_PROGLINK>();  //記錄查詢到的資料
        string m_strProgID = "";                                      //衍生節目編號 
        string m_strEpisode = "";                                     //衍生集別
        string m_strORGProgID = "";                                   //扣播出次數節目編號  
        string m_strORGEpisode = "";                                  //扣播出次數集別
        public Class_PROGLINK m_Queryobj = new Class_PROGLINK();      //查詢條件的obj

        public PROGLINK_Query()
        {
            InitializeComponent();
            InitializeComponent();
            InitializeForm();          //初始化本頁面
        }

        void InitializeForm() //初始化本頁面
        {          
            //查詢扣播出次數節目子集資料
            client.QUERY_TBPROGLINKCompleted += new EventHandler<QUERY_TBPROGLINKCompletedEventArgs>(client_QUERY_TBPROGLINKCompleted);
        }

        #region 實作

        //實作-查詢扣播出次數節目子集資料
        void client_QUERY_TBPROGLINKCompleted(object sender, QUERY_TBPROGLINKCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)  //若是有查詢結果，則顯示，不然就請使用者重新查詢
                {
                    m_ListFormData = new List<Class_PROGLINK>(e.Result);  //把取回來的結果，加上型別定義，才可以塞回去    
                    this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("查無衍生子集資料，請重新輸入條件查詢！", "提示訊息", MessageBoxButton.OK);
                    return;
                }
            }
        }

        #endregion

        #region 檢查畫面資料及搬值

        private Class_PROGLINK FormToClass_PROGLINK()
        {
            Class_PROGLINK obj = new Class_PROGLINK();

            obj.FSPROG_ID = m_strProgID.Trim();     //節目編碼

            if (m_strEpisode.Trim() == "")            //集別         
                obj.FNEPISODE = 32767;
            else
                obj.FNEPISODE = Convert.ToInt16(tbxEPISODE.Text.ToString().Trim());

            obj.FSORGPROG_ID = m_strORGProgID.Trim();     //節目編碼

            if (m_strORGEpisode.Trim() == "")            //集別          
                obj.FNORGEPISODE = 32767;
            else
                obj.FNORGEPISODE = Convert.ToInt16(tbxORGEPISODE.Text.ToString().Trim());

            m_Queryobj = obj;       //紀錄查詢條件

            return obj;
        }

        #endregion  

        //確定
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            //查詢參展記錄資料
            client.QUERY_TBPROGLINKAsync(FormToClass_PROGLINK());
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //開啟節目資料查詢畫面(衍生)
        private void btnPROG_Click(object sender, RoutedEventArgs e)
        {
            PROGDATA_VIEW PROGDATA_VIEW_frm = new PROGDATA_VIEW();
            PROGDATA_VIEW_frm.Show();

            PROGDATA_VIEW_frm.Closed += (s, args) =>
            {
                if (PROGDATA_VIEW_frm.DialogResult == true)
                {
                    tbxPROG_NAME.Text = PROGDATA_VIEW_frm.strProgName_View;
                    tbxPROG_NAME.Tag = PROGDATA_VIEW_frm.strProgID_View;
                    m_strProgID = PROGDATA_VIEW_frm.strProgID_View;

                    tbxEPISODE.Text = "";
                    lblEPISODE_NAME.Content = "";
                    m_strEpisode = "";
                }
            };
        }

        //開啟節目子集資料查詢畫面(衍生)
        private void btnEPISODE_Click(object sender, RoutedEventArgs e)
        {
            if (tbxPROG_NAME.Text.ToString().Trim() != "" && tbxPROG_NAME.Tag.ToString().Trim() != "")
            {
                PRG.PRG200_07 PRG200_07_frm = new PRG.PRG200_07(tbxPROG_NAME.Tag.ToString().Trim(), tbxPROG_NAME.Text.ToString().Trim());
                PRG200_07_frm.Show();

                PRG200_07_frm.Closed += (s, args) =>
                {
                    if (PRG200_07_frm.DialogResult == true)
                    {
                        tbxEPISODE.Text = PRG200_07_frm.m_strEpisode_View;
                        lblEPISODE_NAME.Content = PRG200_07_frm.m_strProgDName_View;
                        m_strEpisode = PRG200_07_frm.m_strEpisode_View;
                    }
                };
            }
            else
                MessageBox.Show("請先選擇衍生節目！", "提示訊息", MessageBoxButton.OK);
        }

        //開啟節目資料查詢畫面(扣播出次數)
        private void btnORGPROG_Click(object sender, RoutedEventArgs e)
        {
            PROGDATA_VIEW PROGDATA_VIEW_frm = new PROGDATA_VIEW();
            PROGDATA_VIEW_frm.Show();

            PROGDATA_VIEW_frm.Closed += (s, args) =>
            {
                if (PROGDATA_VIEW_frm.DialogResult == true)
                {
                    tbxORGPROG_NAME.Text = PROGDATA_VIEW_frm.strProgName_View;
                    tbxORGPROG_NAME.Tag = PROGDATA_VIEW_frm.strProgID_View;
                    m_strORGProgID = PROGDATA_VIEW_frm.strProgID_View;

                    tbxORGEPISODE.Text = "";
                    lblORGEPISODE_NAME.Content = "";
                    m_strORGEpisode = "";
                }
            };
        }

        //開啟節目子集資料查詢畫面(扣播出次數)
        private void btnORGEPISODE_Click(object sender, RoutedEventArgs e)
        {
            if (tbxORGPROG_NAME.Text.ToString().Trim() != "" && tbxORGPROG_NAME.Tag.ToString().Trim() != "")
            {
                PRG.PRG200_07 PRG200_07_frm = new PRG.PRG200_07(tbxORGPROG_NAME.Tag.ToString().Trim(), tbxORGPROG_NAME.Text.ToString().Trim());
                PRG200_07_frm.Show();

                PRG200_07_frm.Closed += (s, args) =>
                {
                    if (PRG200_07_frm.DialogResult == true)
                    {
                        tbxORGEPISODE.Text = PRG200_07_frm.m_strEpisode_View;
                        lblORGEPISODE_NAME.Content = PRG200_07_frm.m_strProgDName_View;
                        m_strORGEpisode = PRG200_07_frm.m_strEpisode_View;
                    }
                };
            }
            else
                MessageBox.Show("請先選擇扣播出次數節目！", "提示訊息", MessageBoxButton.OK);
        }


    }
}

