﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.PROG_M;
using PTS_MAM3.WSPROG_D;
using PTS_MAM3.WSPROG_M;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace PTS_MAM3.ProgData
{
    public partial class PRG200_03 : ChildWindow
    {
        WSPROG_DSoapClient client = new WSPROG_DSoapClient();      //產生新的代理類別
        WSPROG_MSoapClient clientPROGM = new WSPROG_MSoapClient();
        Class_PROGD m_FormData = new Class_PROGD();
        List<Class_CODE> m_CodeDataBuyD = new List<Class_CODE>();  //代碼檔集合(外購類別細項) 
        string m_strProgName = "";                                 //暫存節目名稱
        string m_strProgID = "";                                   //暫存節目編號
        string m_strEpisode = "";                                  //暫存集別
        string m_2ndMemo = "";                                     //2nd備註
        short m_shortEpisode;                                      //暫存集別

        public PRG200_03(Class_PROGD FormData)
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面
            m_FormData = FormData;
            ClassToForm();
            m_strProgName = FormData.FSPROG_ID_NAME.Trim();     //暫存節目名稱
            m_strProgID = FormData.FSPROG_ID.Trim();            //暫存節目編號
            m_strEpisode = FormData.SHOW_FNEPISODE.Trim();      //暫存集別
            m_shortEpisode = FormData.FNEPISODE;                //暫存集別
            this.btnEvent.Content = this.btnEvent.Content.ToString().Replace("設定", "檢視");
            this.btnSet.Content = this.btnSet.Content.ToString().Replace("設定", "檢視");
        }

        public PRG200_03(string strProgID, string strEpisode)
        {
            InitializeComponent();

            //查詢節目子集檔
            client.GetProg_DCompleted += new EventHandler<GetProg_DCompletedEventArgs>(client_GetProg_DCompleted);
            client.GetProg_DAsync(strProgID, strEpisode);
        }

        void InitializeForm() //初始化本頁面
        {
            //下載代碼檔
            clientPROGM.fnGetTBPROG_M_CODECompleted += new EventHandler<fnGetTBPROG_M_CODECompletedEventArgs>(clientPROGM_fnGetTBPROG_M_CODECompleted);
            clientPROGM.fnGetTBPROG_M_CODEAsync();

            //下載外購類別細項代碼檔
            clientPROGM.fnGetTBPROG_M_PROGBUYD_CODECompleted += new EventHandler<fnGetTBPROG_M_PROGBUYD_CODECompletedEventArgs>(client_fnGetTBPROG_M_PROGBUYD_CODECompleted);
            clientPROGM.fnGetTBPROG_M_PROGBUYD_CODEAsync();

            //查詢節目子集是否有2ND
            client.Query_TBPROG_D_NDCompleted += new EventHandler<Query_TBPROG_D_NDCompletedEventArgs>(client_Query_TBPROG_D_NDCompleted);
        }

        private void ClassToForm()  //將前一頁點選的節目子資料繫集到畫面上
        {
            this.gdPROG_D.DataContext = m_FormData;
            tbxTAPELENGTH.Text = Convert.ToString(m_FormData.FNTAPELENGTH / 60); //實際帶長
            tbxTAPELENGTH_S.Text = Convert.ToString(m_FormData.FNTAPELENGTH % 60);//實際帶長
        }

        #region ComboBox載入代碼檔

        //實作-下載外購類別細項代碼檔
        void client_fnGetTBPROG_M_PROGBUYD_CODECompleted(object sender, fnGetTBPROG_M_PROGBUYD_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        ComboBoxItem cbiPROGBUYD = new ComboBoxItem();
                        cbiPROGBUYD.Content = CodeData.NAME;
                        cbiPROGBUYD.Tag = CodeData.ID;
                        cbiPROGBUYD.DataContext = CodeData.IDD;
                        this.cbPROGBUYDID.Items.Add(cbiPROGBUYD);   //先都產生在隱藏起來的細項代碼combobox裡                   
                        m_CodeDataBuyD.Add(CodeData);
                    }
                }
            }
        }

        //實作-下載所有的代碼檔
        void clientPROGM_fnGetTBPROG_M_CODECompleted(object sender, fnGetTBPROG_M_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            case "TBZCHANNEL":       //頻道別代碼
                                ComboBoxItem cbiCHANNEL = new ComboBoxItem();
                                cbiCHANNEL.Content = CodeData.NAME;
                                cbiCHANNEL.Tag = CodeData.ID;
                                this.cbCHANNEL_ID.Items.Add(cbiCHANNEL);
                                break;
                            case "TBZSHOOTSPEC":       //拍攝規格代碼
                                CheckBox cbSHOOTSPEC = new CheckBox();
                                cbSHOOTSPEC.Content = CodeData.NAME;
                                cbSHOOTSPEC.Tag = CodeData.ID;
                                cbSHOOTSPEC.IsEnabled = false;
                                listSHOOTSPEC.Items.Add(cbSHOOTSPEC);
                                break;
                            case "TBZPROGGRADE":      //節目分級代碼
                                ComboBoxItem cbiPROGGRADE = new ComboBoxItem();
                                cbiPROGGRADE.Content = CodeData.NAME;
                                cbiPROGGRADE.Tag = CodeData.ID;
                                this.cbPROGGRADEID.Items.Add(cbiPROGGRADE);
                                break;
                            case "TBZPROGSRC":      //節目來源代碼
                                ComboBoxItem cbiPROGSRC = new ComboBoxItem();
                                cbiPROGSRC.Content = CodeData.NAME;
                                cbiPROGSRC.Tag = CodeData.ID;
                                this.cbPROGSRCID.Items.Add(cbiPROGSRC);
                                break;
                            case "TBZPROGATTR":      //內容屬性代碼
                                ComboBoxItem cbiPROGATTR = new ComboBoxItem();
                                cbiPROGATTR.Content = CodeData.NAME;
                                cbiPROGATTR.Tag = CodeData.ID;
                                this.cbPROGATTRID.Items.Add(cbiPROGATTR);
                                break;
                            case "TBZPROGAUD":      //目標觀眾代碼
                                ComboBoxItem cbiPROGAUD = new ComboBoxItem();
                                cbiPROGAUD.Content = CodeData.NAME;
                                cbiPROGAUD.Tag = CodeData.ID;
                                this.cbPROGAUDID.Items.Add(cbiPROGAUD);
                                break;
                            case "TBZPROGTYPE":      //表現方式代碼
                                ComboBoxItem cbiPROGTYPE = new ComboBoxItem();
                                cbiPROGTYPE.Content = CodeData.NAME;
                                cbiPROGTYPE.Tag = CodeData.ID;
                                this.cbPROGTYPEID.Items.Add(cbiPROGTYPE);
                                break;
                            case "TBZPROGLANG":      //語言代碼
                                ComboBoxItem cbiPROGLANG1 = new ComboBoxItem();
                                cbiPROGLANG1.Content = CodeData.NAME;
                                cbiPROGLANG1.Tag = CodeData.ID;
                                this.cbPROGLANGID1.Items.Add(cbiPROGLANG1);  //主聲道

                                ComboBoxItem cbiPROGLANG2 = new ComboBoxItem();
                                cbiPROGLANG2.Content = CodeData.NAME;
                                cbiPROGLANG2.Tag = CodeData.ID;
                                this.cbPROGLANGID2.Items.Add(cbiPROGLANG2);  //副聲道
                                break;
                            case "TBZPROGBUY":       //外購類別代碼
                                ComboBoxItem cbiPROGBUY = new ComboBoxItem();
                                cbiPROGBUY.Content = CodeData.NAME;
                                cbiPROGBUY.Tag = CodeData.ID;
                                this.cbPROGBUYID.Items.Add(cbiPROGBUY);
                                break;
                            case "TBZPROGSALE":       //行銷類別代碼
                                ComboBoxItem cbiPROGSALE = new ComboBoxItem();
                                cbiPROGSALE.Content = CodeData.NAME;
                                cbiPROGSALE.Tag = CodeData.ID;
                                this.cbPROGSALEID.Items.Add(cbiPROGSALE);
                                break;
                            case "TBZPROGSPEC":       //節目規格代碼
                                CheckBox cbPROGSPEC = new CheckBox();
                                cbPROGSPEC.Content = CodeData.NAME;
                                cbPROGSPEC.Tag = CodeData.ID;
                                cbPROGSPEC.IsEnabled = false;
                                listPROGSPEC.Items.Add(cbPROGSPEC);
                                break;
                            case "TBZPROGNATION":       //來源國家    2012/11/22 kyle                            
                                ComboBoxItem cbNATION = new ComboBoxItem();
                                cbNATION.Content = CodeData.NAME;
                                cbNATION.Tag = CodeData.ID;
                                cbNATION_ID.Items.Add(cbNATION);
                                break;
                            default:
                                break;
                        }
                    }

                    //比對代碼檔
                    compareRACE(m_FormData.FCRACE.ToString().Trim());         //可排檔
                    compareDel(m_FormData.FSDEL.ToString().Trim());           //刪除註記
                    compareCode_FSPROGGRADEID(m_FormData.FSPROGGRADEID.ToString().Trim());
                    compareCode_FSPROGATTRID(m_FormData.FSPROGATTRID.ToString().Trim());
                    compareCode_FSPROGSRCID(m_FormData.FSPROGSRCID.ToString().Trim());
                    compareCode_FSPROGTYPEID(m_FormData.FSPROGTYPEID.ToString().Trim());
                    compareCode_FSPROGAUDID(m_FormData.FSPROGAUDID.ToString().Trim());
                    compareCode_FSPROGLANGID1(m_FormData.FSPROGLANGID1.ToString().Trim());
                    compareCode_FSPROGLANGID2(m_FormData.FSPROGLANGID2.ToString().Trim());
                    compareCode_FSPROGBUYID(m_FormData.FSPROGBUYID.ToString().Trim());
                    compareCode_FSPROGBUYDID(m_FormData.FSPROGBUYDID.ToString().Trim());
                    compareCode_FSCHANNEL_ID(m_FormData.FSCHANNEL_ID.ToString().Trim());
                    compareCode_FSSHOOTSPEC(CheckList(m_FormData.FSSHOOTSPEC.ToString().Trim()));
                    compareCode_FSPROGSALEID(m_FormData.FSPROGSALEID.ToString().Trim());
                    compareCode_FSPROGSPEC(CheckList(m_FormData.FSPROGSPEC.ToString().Trim()));
                    compareCode_CR_TYPE(m_FormData.FSCR_TYPE.ToString().Trim());
                    cbNATION_ID.SelectedItem = cbNATION_ID.Items.Where(S => ((ComboBoxItem)S).Tag.ToString() == m_FormData.FSPROGNATIONID).FirstOrDefault();
                    //if (m_FormData.FDEXPIRE_DATE.ToShortDateString() != "1900/1/1")
                    if (m_FormData.FDEXPIRE_DATE > Convert.ToDateTime("1900/1/5"))
                    {
                        dpEXPIRE.Text = m_FormData.FDEXPIRE_DATE.ToShortDateString();
                    }
                    if (m_FormData.FSEXPIRE_DATE_ACTION == "Y")
                        cbDELETE.IsChecked = true;
                    else
                        cbDELETE.IsChecked = false;
                }
            }
        }

        //實作-查詢節目子集是否有2ND
        void client_Query_TBPROG_D_NDCompleted(object sender, Query_TBPROG_D_NDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    call2nd(e.Result.Trim());    //呼叫2ND
                }
            }
        }

        //實作-查詢節目子集檔
        void client_GetProg_DCompleted(object sender, GetProg_DCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)  //若是有查詢結果，則顯示
                {
                    m_FormData = e.Result[0];
                    ClassToForm();
                    InitializeForm();
                }
                else
                {
                    MessageBox.Show("查無此節目子集資料", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false; ;
                }
            }
        }

        private string[] CheckList(string strChannel)
        {
            char[] delimiterChars = { ';' };
            string[] words = strChannel.Split(delimiterChars);
            return words;
        }

        private void compareRACE(string strRACE)   //是否為可排檔
        {
            if (strRACE == "N")
                cbRACE.SelectedIndex = 0;
            else if (strRACE == "Y")
                cbRACE.SelectedIndex = 1;
        }

        private void compareDel(string strDel)   //是否為刪除
        {
            if (strDel == "N")
                cbDel.SelectedIndex = 0;
            else if (strDel == "Y")
                cbDel.SelectedIndex = 1;
        }

        //選擇外購類別，要判斷顯示哪些外購類別細項
        private void cbBUY_ID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.cbPROGBUYDIDS.Items.Clear();     //使用前先清空
            string strBUYID = ""; //外購類別大項
            ComboBox cb = sender as ComboBox;
            ComboBoxItem cbm = (ComboBoxItem)cb.SelectedItem;
            strBUYID = cbm.Tag.ToString();

            for (int i = 0; i < cbPROGBUYDID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGBUYDID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strBUYID))
                {
                    ComboBoxItem cbiPROGBUYDS = new ComboBoxItem();
                    cbiPROGBUYDS.Content = getcbi.Content;
                    cbiPROGBUYDS.Tag = getcbi.DataContext;
                    this.cbPROGBUYDIDS.Items.Add(cbiPROGBUYDS);
                }
            }
        }

        #endregion

        #region 比對代碼檔

        //比對代碼檔_節目型態(內容屬性)
        void compareCode_FSPROGATTRID(string strID)
        {
            for (int i = 0; i < cbPROGATTRID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGATTRID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGATTRID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_節目分級
        void compareCode_FSPROGGRADEID(string strID)
        {
            for (int i = 0; i < cbPROGGRADEID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGGRADEID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGGRADEID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_節目來源
        void compareCode_FSPROGSRCID(string strID)
        {
            for (int i = 0; i < cbPROGSRCID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGSRCID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGSRCID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_表現方式
        void compareCode_FSPROGTYPEID(string strID)
        {
            for (int i = 0; i < cbPROGTYPEID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGTYPEID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGTYPEID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_目標觀眾
        void compareCode_FSPROGAUDID(string strID)
        {
            for (int i = 0; i < cbPROGAUDID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGAUDID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGAUDID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_主聲道
        void compareCode_FSPROGLANGID1(string strID)
        {
            for (int i = 0; i < cbPROGLANGID1.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGLANGID1.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGLANGID1.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_副聲道
        void compareCode_FSPROGLANGID2(string strID)
        {
            for (int i = 0; i < cbPROGLANGID2.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGLANGID2.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGLANGID2.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_外購類別大類
        void compareCode_FSPROGBUYID(string strID)
        {
            for (int i = 0; i < cbPROGBUYID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGBUYID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGBUYID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_外購類別細類
        void compareCode_FSPROGBUYDID(string strID)
        {
            for (int i = 0; i < cbPROGBUYDIDS.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGBUYDIDS.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGBUYDIDS.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_拍攝規格
        void compareCode_FSSHOOTSPEC(string[] ListTEST_Send)
        {
            for (int i = 0; i < ListTEST_Send.Length - 1; i++)
            {
                for (int j = 0; j < listSHOOTSPEC.Items.Count; j++)
                {
                    CheckBox getcki = (CheckBox)listSHOOTSPEC.Items.ElementAt(j);

                    if (getcki.Tag.ToString().Trim().Equals(ListTEST_Send[i]))
                    {
                        getcki.IsChecked = true;
                        break;
                    }
                }
            }
        }

        //比對代碼檔_節目規格
        void compareCode_FSPROGSPEC(string[] ListTEST_Send)
        {
            for (int i = 0; i < ListTEST_Send.Length - 1; i++)
            {
                for (int j = 0; j < listPROGSPEC.Items.Count; j++)
                {
                    CheckBox getcki = (CheckBox)listPROGSPEC.Items.ElementAt(j);

                    if (getcki.Tag.ToString().Trim().Equals(ListTEST_Send[i]))
                    {
                        getcki.IsChecked = true;
                        break;
                    }
                }
            }
        }

        //比對代碼檔_頻道別
        void compareCode_FSCHANNEL_ID(string strID)
        {
            for (int i = 0; i < cbCHANNEL_ID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbCHANNEL_ID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbCHANNEL_ID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_行銷類別
        void compareCode_FSPROGSALEID(string strID)
        {
            for (int i = 0; i < cbPROGSALEID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGSALEID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGSALEID.SelectedIndex = i;
                    break;
                }
            }
        }

        //選擇外購類別，要判斷顯示哪些外購類別細項
        private void cbPROGBUYID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.cbPROGBUYDIDS.Items.Clear();     //使用前先清空
            string strBUYID = ""; //外購類別大項
            ComboBox cb = sender as ComboBox;
            ComboBoxItem cbm = (ComboBoxItem)cb.SelectedItem;
            strBUYID = cbm.Tag.ToString();

            for (int i = 0; i < cbPROGBUYDID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGBUYDID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strBUYID))
                {
                    ComboBoxItem cbiPROGBUYDS = new ComboBoxItem();
                    cbiPROGBUYDS.Content = getcbi.Content;
                    cbiPROGBUYDS.Tag = getcbi.DataContext;
                    this.cbPROGBUYDIDS.Items.Add(cbiPROGBUYDS);
                }
            }
        }

        //比對代碼檔_著作權類型
        void compareCode_CR_TYPE(string strID)
        {
            for (int i = 0; i < cbCR_TYPE.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbCR_TYPE.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbCR_TYPE.SelectedIndex = i;
                    break;
                }
            }
        }

        #endregion

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //設定 Secondary Event
        private void btnEvent_Click(object sender, RoutedEventArgs e)
        {
            if (m_2ndMemo.Trim() == "")
                client.Query_TBPROG_D_NDAsync(m_strProgID, m_strEpisode);   //先查詢出該子集是否有2nd資料，若有要帶入修改
            else
                call2nd(m_2ndMemo.Trim());           //呼叫2ND 
        }

        //呼叫2ND
        private void call2nd(string str2nd)
        {
            PGM.PGM100_02 SetLouthKey_Frm = new PGM.PGM100_02();

            //修改時傳入組好的備註字串
            SetLouthKey_Frm.InLouthKeyString = str2nd;
            SetLouthKey_Frm.WorkChannelID = m_FormData.FSCHANNEL_ID.Trim();

            #region 20140903 by Jarvis disable所有控制項,只留cancel按鈕
            foreach (UIElement ui in SetLouthKey_Frm.grid1.Children)
            {
                try
                {
                    ((Control)ui).IsEnabled = false;
                }
                catch
                {

                }
            }

            SetLouthKey_Frm.OKButton.IsEnabled = false;
            #endregion

            SetLouthKey_Frm.Show();
            SetLouthKey_Frm.Closed += (s, args) =>
            {
                if (SetLouthKey_Frm.OutLouthKeyString != null)
                {
                    //傳回選擇好的備註字串                  
                    m_2ndMemo = SetLouthKey_Frm.OutLouthKeyString.Trim();
                }
            };
        }

        //設定主控播出提示資料
        private void btnSet_Click(object sender, RoutedEventArgs e)
        {
            PTS_MAM3.PGM.PGM100_05 PGM100_05_Frm = new PTS_MAM3.PGM.PGM100_05();

            PGM100_05_Frm.textBoxProgID.Text = m_FormData.FSPROG_ID;
            PGM100_05_Frm.textBoxProgName.Text = m_FormData.FSPROG_ID_NAME.Trim();
            PGM100_05_Frm.textBoxEpisode.Text = m_FormData.SHOW_FNEPISODE;

            #region 20140903 by Jarvis disable所有控制項,只留cancel按鈕
            //PGM100_05_Frm._ARC_TYPE =string.Format("{0:000}",Convert.ToInt32(m_FormData.FSPROGSPEC.Replace(";","")));
            foreach (UIElement ui in PGM100_05_Frm.LayoutRoot.Children)
            {
                try
                {
                    if (ui is RadioButton) 
                    {
                        RadioButton rbBtn=(RadioButton)ui;
                        if (rbBtn.Name == "radioButtonSD" || rbBtn.Name == "radioButtonHD")
                        {
                            continue;
                        }
                    }

                    ((Control)ui).IsEnabled = false;
                }
                catch
                {

                }
            }
            PGM100_05_Frm.CancelButton.IsEnabled = true;
            #endregion

            PGM100_05_Frm.Show();
            PGM100_05_Frm.Closed += (s, args) =>
            {
            };
        }

        //顯示實際帶長
        private void btnLength_Click(object sender, RoutedEventArgs e)
        {
            PRG.PRG200_10 PRG200_10_frm = new PRG.PRG200_10(m_strProgID, m_shortEpisode);
            PRG200_10_frm.Show();
        }

    }
}

