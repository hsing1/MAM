﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;
using System.IO;
using System.Text;
using PTS_MAM3.WSPROGMEN;
using PTS_MAM3.PROG_M;
using System.Text.RegularExpressions;

namespace PTS_MAM3.ProgData
{
    public partial class PROGMEN_Add : ChildWindow
    {
        WSPROGMENSoapClient client = new WSPROGMENSoapClient();          //產生新的代理類別
        List<ClassCode> m_CodeData = new List<ClassCode>();              //代碼檔集合
        List<Class_PROGSTAFF> m_StaffList = new List<Class_PROGSTAFF>(); //工作人員檔集合
        string m_strProgName = "";                                       //暫存節目名稱 
        string m_strEpisode= "";                                         //暫存集別
        Boolean m_Continue = false;
        public Class_PROGMEN m_Queryobj = new Class_PROGMEN();           //查詢新增的obj
        List<string> m_StaffName = new List<string>();                   //工作人員姓名集合
        string m_strStaffID = "";                                        //工作人員ID
        string m_strTempStaffID = "";                                    //工作人員ID暫存，避免問兩次
        string m_strTotalEpisode = "";                                   //總集數

        public PROGMEN_Add()
        {
            InitializeComponent();
            InitializeForm();          //初始化本頁面
        }

        void InitializeForm() //初始化本頁面
        {
            ToolTipService.SetToolTip(this.tbxNAME, "提示 ：欄位內連續多個空白只會存入單一空白");
            //下載代碼檔           
            client.fnGetTBPROGMEN_CODECompleted+=new EventHandler<fnGetTBPROGMEN_CODECompletedEventArgs>(client_fnGetTBPROGMEN_CODECompleted);
            client.fnGetTBPROGMEN_CODEAsync();

            //新增相關人員資料
            client.fnINSERT_TBPROMENCompleted += new EventHandler<fnINSERT_TBPROMENCompletedEventArgs>(client_fnINSERT_TBPROMENCompleted);

            //查詢相關人員資料(避免重複)
            client.fnQUERY_TBPROGMEN_CHECKCompleted += new EventHandler<fnQUERY_TBPROGMEN_CHECKCompletedEventArgs>(client_fnQUERY_TBPROGMEN_CHECKCompleted);

            //查詢節目管理系統的工作人員資料
            client.fnGetTBPROGSTAFFCompleted+=new EventHandler<fnGetTBPROGSTAFFCompletedEventArgs>(client_fnGetTBPROGSTAFFCompleted);
            client.fnGetTBPROGSTAFFAsync();

            //整批轉入新增相關人員資料
            client.fnINSERT_TBPROMEN_ALLCompleted += new EventHandler<fnINSERT_TBPROMEN_ALLCompletedEventArgs>(client_fnINSERT_TBPROMEN_ALLCompleted);

            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }
        
        #region 實作

        //實作-新增相關人員資料
        void client_fnINSERT_TBPROMENCompleted(object sender, fnINSERT_TBPROMENCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;

            if (e.Error == null)
            {
                if (e.Result.bolResult == true)
                {
                    MessageBox.Show("新增相關人員資料「" + m_strProgName.ToString() + "-" + m_strEpisode + "集」成功！", "提示訊息", MessageBoxButton.OK);

                    if (m_Continue == false)          //讓使用者可以一直新增同樣內容的資料
                        this.DialogResult = true;  
                }
                else
                {
                    MessageBox.Show("新增相關人員資料「" + m_strProgName.ToString() + "-" + m_strEpisode + "集」失敗！\n失敗原因：" + e.Result.strErrorMsg, "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
        }

        //實作-整批轉入新增相關人員資料
        void client_fnINSERT_TBPROMEN_ALLCompleted(object sender, fnINSERT_TBPROMEN_ALLCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;
            Boolean bolCheck = false;
            string strMsg = "";         

            if (e.Error == null)
            {
                for (int i = 0; i < e.Result.Count; i++)
                {
                    if (e.Result[i].bolResult == false)
                    {
                        strMsg = strMsg + e.Result[i].strErrorMsg.Trim() + "\n";
                        bolCheck = true;    //有失敗的訊息
                    }                
                }

                if (bolCheck == false)
                {
                    if (cbBooking.IsChecked == true)
                        MessageBox.Show("整批新增相關人員資料「" + tbxSEpisode.Text.Trim() + "集~" + tbxEEpisode.Text.Trim() + "集」成功！", "提示訊息", MessageBoxButton.OK);
                    else if (cbJump.IsChecked == true)
                        MessageBox.Show("整批跳集新增相關人員資料成功！", "提示訊息", MessageBoxButton.OK);

                    if (m_Continue == false)          //讓使用者可以一直新增同樣內容的資料
                        this.DialogResult = true;
                }
                else
                {
                    if (cbBooking.IsChecked == true)
                        MessageBox.Show("整批新增相關人員資料「" + tbxSEpisode.Text.Trim() + "集~" + tbxEEpisode.Text.Trim() + "集」失敗！\n" + strMsg, "提示訊息", MessageBoxButton.OK);
                    else if (cbJump.IsChecked == true)
                        MessageBox.Show("整批跳集新增相關人員資料失敗！\n" + strMsg, "提示訊息", MessageBoxButton.OK);
                    
                    this.DialogResult = false;
                }
            }
        }

        //實作-查詢相關人員資料(避免重複)
        void client_fnQUERY_TBPROGMEN_CHECKCompleted(object sender, fnQUERY_TBPROGMEN_CHECKCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;

            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    if (e.Result == "0")
                    {
                        //新增相關人員資料
                        client.fnINSERT_TBPROMENAsync(FormToClass_PROGMEN());
                        BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                    }
                    else
                    {
                        MessageBox.Show("資料庫已有重複資料，請檢查後重新輸入！", "提示訊息", MessageBoxButton.OK);
                    }
                }
            }
        }


        //實作-查詢節目管理系統的工作人員資料
        void client_fnGetTBPROGSTAFFCompleted(object sender, fnGetTBPROGSTAFFCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;

            if (e.Error == null)
            {
                if (e.Result != null)
                {           
                       for (int i = 0; i < e.Result.Count; i++)
                          {
                            m_StaffList.Add(e.Result[i]);
                            m_StaffName.Add(e.Result[i].FSNAME);

                            ComboBoxItem cbiStaff = new ComboBoxItem();
                            cbiStaff.Content = e.Result[i].FSNAME;
                            cbiStaff.Tag = e.Result[i].FSSTAFFID;
                            this.cbStaff.Items.Add(cbiStaff);   //取得的工作人員資料塞到ComboBox裡                            
                         }

                       acbStaff.ItemsSource = m_StaffName ;    //AutoComplete資料繫結
                    }
                }
            } 
        

        #endregion

        #region ComboBox載入代碼檔

        //實作-下載所有的代碼檔
        void client_fnGetTBPROGMEN_CODECompleted(object sender, fnGetTBPROGMEN_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            case "TBZTITLE":      //頭銜代碼
                                ComboBoxItem cbiTITLE = new ComboBoxItem();
                                cbiTITLE.Content = CodeData.NAME;
                                cbiTITLE.Tag = CodeData.ID;
                                this.cbTITLE_ID.Items.Add(cbiTITLE);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

        #endregion

        #region 檢查畫面資料及搬值

        private Class_PROGMEN FormToClass_PROGMEN()
        {
            Class_PROGMEN obj = new Class_PROGMEN();

            obj.FSPROG_ID = tbxPROG_NAME.Tag.ToString().Trim();     //節目編碼
            m_strProgName = tbxPROG_NAME.Text.ToString().Trim();    //暫存本次新增的節目名稱，為了秀給使用者看結果用

            if (tbxEPISODE.Text.ToString().Trim() == "")            //集別
            {
                obj.FNEPISODE = 0;
                m_strEpisode = "";
            }
            else
            {
                obj.FNEPISODE = Convert.ToInt16(tbxEPISODE.Text.ToString().Trim());
                m_strEpisode = tbxEPISODE.Text.ToString().Trim();  //暫存本次新增的集別，為了秀給使用者看結果用
            }

            if (cbTITLE_ID.SelectedIndex != -1)                     //頭銜
                obj.FSTITLEID = ((ComboBoxItem)cbTITLE_ID.SelectedItem).Tag.ToString();
            else
                obj.FSTITLEID = "";

            obj.FSEMAIL = tbxEMAIL.Text.ToString().Trim();          //EMAIL

            //var tbxNAMEArr = Regex.Split(tbxNAME.Text, @"[\W_]+");
            tbxNAME.Text = tbxNAME.Text.Replace("\t", " ");
            string[] tbxNAMEArr=tbxNAME.Text.Split(new string[] { " "}, StringSplitOptions.RemoveEmptyEntries);
            tbxNAME.Text = "";

            foreach (string str in tbxNAMEArr)
            {
                tbxNAME.Text += str + " ";
            }
            tbxNAME.Text = tbxNAME.Text.Trim();
            obj.FSNAME = tbxNAME.Text;            //公司/姓名
            obj.FSMEMO = tbxMEMO.Text.ToString().Trim();            //備註

            //if (cbStaff.SelectedIndex != -1)                      //工作人員從下拉式選單變成
            //    obj.FSSTAFFID = ((ComboBoxItem)cbStaff.SelectedItem).Tag.ToString().Trim();    //工作人員代碼
            //else
            //    obj.FSSTAFFID = "";

            if (m_strTempStaffID == "ini")
            {
                if (m_strStaffID.Trim() != "")                          //工作人員代碼，要判斷是否要連結            
                    obj.FSSTAFFID = m_strStaffID.Trim();
                else
                {
                    obj.FSSTAFFID = checkName(obj.FSNAME);

                    if (obj.FSSTAFFID != "")
                    {
                        MessageBoxResult result = MessageBox.Show("確定要連結工作人員資料「" + obj.FSNAME + "」嗎?", "提示訊息", MessageBoxButton.OKCancel);

                        if (result != MessageBoxResult.OK)
                            obj.FSSTAFFID = "";
                    }
                }

                m_strTempStaffID = obj.FSSTAFFID;
            }
            else      
                obj.FSSTAFFID = m_strTempStaffID;      
            
            obj.FSCREATED_BY = UserClass.userData.FSUSER_ID.ToString();//建檔者
            obj.FSUPDATED_BY = UserClass.userData.FSUSER_ID.ToString();//修改者
                     
            m_Queryobj = obj;

            return obj;
        }

        private Boolean Check_PROGRACEData()    //檢查畫面上的欄位是否都填妥
        {
            StringBuilder strMessage = new StringBuilder();
            string strStaffName = acbStaff.Text.ToString().Trim();
            Boolean bolStaff = false;
            short shortCheck1 = -1, shortCheck2 = -1, shortCheck3 = -1;

            if (tbxPROG_NAME.Text.Trim() == "")
                strMessage.AppendLine("請選擇「節目」");

            if (cbTITLE_ID.SelectedIndex == -1)
                strMessage.AppendLine("請選擇「頭銜」欄位");
            
            if (tbxNAME.Text.Trim() == "") //20140722 by Jarvis
            {
                tbxNAME.Text = "";
                strMessage.AppendLine("請輸入「公司/姓名」欄位");
            }

            //整批新增的判斷
            if (cbBooking.IsChecked == true)
            {
                if (tbxSEpisode.Text.Trim() != "")
                {
                    if (short.TryParse(tbxSEpisode.Text.Trim(), out shortCheck1) == false)
                    {
                        strMessage.AppendLine("請檢查「整批新增集數(起)」欄位必須為整數數值型態");
                    }
                    else if (shortCheck1 <= 0)
                    {
                        strMessage.AppendLine("請檢查「整批新增集數(起)」欄位必須大於0");
                    }
                }
                else
                    strMessage.AppendLine("請填寫「整批新增集數(起)」欄位");

                if (tbxEEpisode.Text.Trim() != "")
                {
                    if (short.TryParse(tbxEEpisode.Text.Trim(), out shortCheck2) == false)
                    {
                        strMessage.AppendLine("請檢查「整批新增集數(迄)」欄位必須為整數數值型態");
                    }
                    else if (shortCheck2 <= 0)
                    {
                        strMessage.AppendLine("請檢查「整批新增集數(迄)」欄位必須大於0");
                    }
                }
                else
                    strMessage.AppendLine("請填寫「整批新增集數(迄)」欄位");

                if (tbxSEpisode.Text.Trim() != "" && tbxEEpisode.Text.Trim() != "")
                {
                    if ((shortCheck1 >= 0) && (shortCheck2 >= 0) && (shortCheck1 > shortCheck2))
                    {
                        strMessage.AppendLine("請檢查「整批新增集數(起)」欄位必須小於或等於「整批新增集數(迄)」");
                    }

                    if (short.TryParse(m_strTotalEpisode, out shortCheck3) == false)
                        strMessage.AppendLine("請檢查節目的總集數！");
                    else
                    {
                        if (shortCheck2 > shortCheck3)
                        {
                            strMessage.AppendLine("請檢查「整批新增集數(迄)」欄位必須小於「總集數」，該節目總共「" + m_strTotalEpisode + "」集");
                        }
                    }
                }
            }

            //整批新增的判斷
            if (cbJump.IsChecked == true)
            {
                if (tbxJump.Text.Trim() == "")
                    strMessage.AppendLine("請填寫整批跳集新增的集數");                
            }
            
            if (strStaffName != "") //檢查輸入的工作人員姓名是不是有在工作人員檔裡
            {
                for (int i = 0; i < m_StaffList.Count; i++)
                    {
                        if (m_StaffList[i].FSNAME.ToString().Trim().Equals(strStaffName))
                        {
                            bolStaff = true;
                            break;
                        }
                    }

                if (bolStaff == false)
                    strMessage.AppendLine("工作人員-" + strStaffName + "不在工作人員檔裡，請重新輸入");
            }        

            if (strMessage.ToString().Trim() == "")
                return true;
            else
            {
                MessageBoxResult result = MessageBox.Show(strMessage.ToString(), "提示訊息", MessageBoxButton.OK);
                return false;
            }
        }

        //選擇工作人員時，要秀工作人員的詳細內容
        private void cbStaff_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string strStaffID = ((ComboBoxItem)cbStaff.SelectedItem).Tag.ToString().Trim();

            for (int i = 0; i < m_StaffList.Count; i++)
            {
                if (m_StaffList[i].FSSTAFFID.ToString().Trim().Equals(strStaffID))
                {
                    tbxENAME.Text = m_StaffList[i].FSENAME.ToString().Trim();       //外文姓名
                    tbxWEBSITE.Text =  m_StaffList[i].FSWEBSITE.ToString().Trim();  //個人網站
                    tbxINTRO.Text = m_StaffList[i].FSINTRO.ToString().Trim();       //簡介
                    tbxEINTRO.Text = m_StaffList[i].FSEINTRO.ToString().Trim();     //外文簡介
                    break;
                }
            }
        }

        #endregion  
  
        //確定
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (Check_PROGRACEData() == false)      //檢查畫面上的欄位是否都填妥
                return;

            m_strTempStaffID = "ini";
            m_Continue = false;

            if (cbBooking.IsChecked == true)        //判斷是不是整批新增
                InsertAll();                        //整批新增   
            else if (cbJump.IsChecked == true)
                InsertAll_Jump();                   //整批跳集新增    
            else
            {
                client.fnQUERY_TBPROGMEN_CHECKAsync(FormToClass_PROGMEN()); //檢查是否有相同的一集、同頭銜且同名，若有就無法新增
                BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
            }
        }

        //繼續新增
        private void ContinueButton_Click(object sender, RoutedEventArgs e)
        {
            if (Check_PROGRACEData() == false)      //檢查畫面上的欄位是否都填妥
                return;

            m_strTempStaffID = "ini";
            m_Continue = true;

            if (cbBooking.IsChecked == true)        //判斷是不是整批新增
                InsertAll();                        //整批新增
            else if (cbJump.IsChecked == true)
                InsertAll_Jump();                   //整批跳集新增    
            else
            {
                client.fnQUERY_TBPROGMEN_CHECKAsync(FormToClass_PROGMEN());//檢查是否有相同的一集、同頭銜且同名，若有就無法新增
                BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
            }                          
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //開啟節目資料查詢畫面
        private void btnPROG_Click(object sender, RoutedEventArgs e)
        {
            PROGDATA_VIEW PROGDATA_VIEW_frm = new PROGDATA_VIEW();
            PROGDATA_VIEW_frm.Show();

            PROGDATA_VIEW_frm.Closed += (s, args) =>
            {
                if (PROGDATA_VIEW_frm.DialogResult == true)
                {
                    tbxPROG_NAME.Text = PROGDATA_VIEW_frm.strProgName_View;
                    tbxPROG_NAME.Tag = PROGDATA_VIEW_frm.strProgID_View;
                    m_strTotalEpisode = PROGDATA_VIEW_frm.strTOTLEEPISODE;

                    tbxEPISODE.Text = "";
                    lblEPISODE_NAME.Content = "";
                }
            };
        }

        //開啟節目子集資料查詢畫面
        private void btnEPISODE_Click(object sender, RoutedEventArgs e)
        {
            if (tbxPROG_NAME.Text.ToString().Trim() != "" && tbxPROG_NAME.Tag.ToString().Trim() != "")
            {
                PRG.PRG200_07 PRG200_07_frm = new PRG.PRG200_07(tbxPROG_NAME.Tag.ToString().Trim(), tbxPROG_NAME.Text.ToString().Trim());
                PRG200_07_frm.Show();

                PRG200_07_frm.Closed += (s, args) =>
                {
                    if (PRG200_07_frm.DialogResult == true)
                    {
                        tbxEPISODE.Text = PRG200_07_frm.m_strEpisode_View;
                        lblEPISODE_NAME.Content = PRG200_07_frm.m_strProgDName_View;
                    }
                };
            }
            else
                MessageBox.Show("請先選擇節目！", "提示訊息", MessageBoxButton.OK);
        }

        //自動去比對資料庫，若有相同姓名的人員就顯示資料
        private void acbStaff_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0)
            {
                 tbxENAME.Text ="";
                 tbxWEBSITE.Text ="";   
                 tbxINTRO.Text ="";   
                 tbxEINTRO.Text = "";
                 m_strStaffID = "";
                 return;
            }                

            string strStaffName = ((object[])(e.AddedItems))[0].ToString();

            for (int i = 0; i < m_StaffList.Count; i++)
            {
                if (m_StaffList[i].FSNAME.ToString().Trim().Equals(strStaffName))
                {
                    tbxENAME.Text = m_StaffList[i].FSENAME.ToString().Trim();       //外文姓名
                    tbxWEBSITE.Text = m_StaffList[i].FSWEBSITE.ToString().Trim();   //個人網站
                    tbxINTRO.Text = m_StaffList[i].FSINTRO.ToString().Trim();       //簡介
                    tbxEINTRO.Text = m_StaffList[i].FSEINTRO.ToString().Trim();     //外文簡介
                    m_strStaffID = m_StaffList[i].FSSTAFFID.ToString().Trim();      //工作人員ID
                    break;
                }
            }
        }

        //檢查姓名/名字 有沒有一樣的名稱可以連結關係
        private string checkName(string strName)
        {
            for (int i = 0; i < m_StaffList.Count; i++)
            {
                if (m_StaffList[i].FSNAME.ToString().Trim().Equals(strName.Trim()))             
                    return m_StaffList[i].FSSTAFFID.ToString().Trim();      
            }            
            return "";
        }

        //整批新增，連續集數
        private void InsertAll()
        {            
            short shortCheckS = -1, shortCheckE = -1;
            short.TryParse(tbxSEpisode.Text.Trim(), out shortCheckS);
            short.TryParse(tbxEEpisode.Text.Trim(), out shortCheckE);

            if (shortCheckS == -1 && shortCheckE == -1)
            {
                MessageBox.Show("請檢查「整批新增集數」欄位異常", "提示訊息", MessageBoxButton.OK);
                return;
            }                
               
            MessageBoxResult result = MessageBox.Show("確定要整批新增「" + tbxSEpisode.Text.Trim() + "集~" + tbxEEpisode.Text.Trim() + "集」?", "提示訊息", MessageBoxButton.OKCancel);

            if (result == MessageBoxResult.OK)
            {
                List<Class_PROGMEN> ListobjMen = new List<Class_PROGMEN>();

                 for (short i = shortCheckS; i <= shortCheckE; i++)
                {
                    Class_PROGMEN objMen = new Class_PROGMEN();                   
                    objMen = FormToClass_PROGMEN();
                    objMen.FNEPISODE = i;

                    ListobjMen.Add(objMen);
                }

                 if (ListobjMen.Count > 0)
                 {
                     client.fnINSERT_TBPROMEN_ALLAsync(ListobjMen);
                     BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                     m_Queryobj.FNEPISODE = 0;
                 }
            }   
        }

        //整批跳集新增
        private void InsertAll_Jump()
        {
            List<short> ListshortEpisode = new List<short>();   //要新增的集別
            char[] delimiterChars = { ',' };
            short shortCheckS = -1,shortCheckT = -1;
            string strMsg = "";

            string[] words = tbxJump.Text.Trim().Split(delimiterChars);

            foreach (string wordsobj in words)
            {
               shortCheckS = -1;
               short.TryParse(wordsobj.Trim(), out shortCheckS);

               if (shortCheckS != -1 && shortCheckS != 0)
               {
                   ListshortEpisode.Add(shortCheckS);
                   strMsg= strMsg+wordsobj.Trim()+"、";
               }                   
            }

            if (strMsg.Trim() == "")
            {
                MessageBox.Show("請檢查「整批跳集新增集數」欄位，查無合適的集數！", "提示訊息", MessageBoxButton.OK);
                return;
            }

            if (short.TryParse(m_strTotalEpisode, out shortCheckT) == false)
            {
                MessageBox.Show("請檢查節目的總集數！", "提示訊息", MessageBoxButton.OK);
                return;
            }

            MessageBoxResult result = MessageBox.Show("確定要整批跳集新增「" + strMsg.Substring(0,strMsg.Length-1) + "」?", "提示訊息", MessageBoxButton.OKCancel);

            if (result == MessageBoxResult.OK)
            {
                List<Class_PROGMEN> ListobjMen = new List<Class_PROGMEN>();

                foreach(short shortEpisode in ListshortEpisode)
                {
                    if (shortEpisode <= shortCheckT)
                    {
                        Class_PROGMEN objMen = new Class_PROGMEN();
                        objMen = FormToClass_PROGMEN();
                        objMen.FNEPISODE = shortEpisode;
                        ListobjMen.Add(objMen);
                    }
                }

                if (ListobjMen.Count > 0)
                {
                    client.fnINSERT_TBPROMEN_ALLAsync(ListobjMen);
                    BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                    m_Queryobj.FNEPISODE = 0;
                }
            }
        }


    }
}

