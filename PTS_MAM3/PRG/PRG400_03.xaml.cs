﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;
using System.IO;
using System.Text;
using PTS_MAM3.WSPROGMEN;
using PTS_MAM3.PROG_M;

namespace PTS_MAM3.ProgData
{
    public partial class PRG400_03 : ChildWindow
    {
        WSPROGMENSoapClient client = new WSPROGMENSoapClient();          //產生新的代理類別
        List<ClassCode> m_CodeData = new List<ClassCode>();              //代碼檔集合
        List<Class_PROGSTAFF> m_StaffList = new List<Class_PROGSTAFF>(); //工作人員檔集合
        string m_strProgName = "";                                       //暫存節目名稱 
        string m_strEpisode = "";                                        //暫存集別
        Class_PROGMEN m_FormData = new Class_PROGMEN();
        string m_strAUTO = "";                                           //相關人員主Key
        public Class_PROGMEN m_Queryobj = new Class_PROGMEN();           //查詢修改的obj

        public PRG400_03(Class_PROGMEN FormData)
        {
            InitializeComponent();
            InitializeForm();          //初始化本頁面
            m_FormData = FormData;
            ClassToForm();
        }

        void InitializeForm() //初始化本頁面
        {
            //下載代碼檔           
            client.fnGetTBPROGMEN_CODECompleted += new EventHandler<fnGetTBPROGMEN_CODECompletedEventArgs>(client_fnGetTBPROGMEN_CODECompleted);
            client.fnGetTBPROGMEN_CODEAsync();
          
            //查詢節目管理系統的工作人員資料
            client.fnGetTBPROGSTAFFCompleted += new EventHandler<fnGetTBPROGSTAFFCompletedEventArgs>(client_fnGetTBPROGSTAFFCompleted);
            client.fnGetTBPROGSTAFFAsync();
        }

        private void ClassToForm()  //將前一頁點選的節目資料繫集到畫面上
        {
            this.gdPROG_MEN.DataContext = m_FormData;
        }

        #region 實作
        
        //實作-查詢節目管理系統的工作人員資料
        void client_fnGetTBPROGSTAFFCompleted(object sender, fnGetTBPROGSTAFFCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        m_StaffList.Add(e.Result[i]);
                        ComboBoxItem cbiStaff = new ComboBoxItem();
                        cbiStaff.Content = e.Result[i].FSNAME;
                        cbiStaff.Tag = e.Result[i].FSSTAFFID;
                        this.cbStaff.Items.Add(cbiStaff);   //取得的工作人員資料塞到ComboBox裡
                    }

                    //必須在代碼都下載完後才能比對
                    compareSTAFFID(m_FormData.FSSTAFFID.ToString().Trim());           //工作人員代碼
                }
            }
        }

        void compareTITLEID(string strID)      //比對代碼檔_頭銜代碼
        {
            for (int i = 0; i < cbTITLE_ID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbTITLE_ID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbTITLE_ID.SelectedIndex = i;
                    break;
                }
            }
        }

        void compareSTAFFID(string strID)      //工作人員代碼
        {
            for (int i = 0; i < cbStaff.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbStaff.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbStaff.SelectedIndex = i;
                    break;
                }
            }
        }
        
        //選擇工作人員時，要秀工作人員的詳細內容
        private void cbStaff_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string strStaffID = ((ComboBoxItem)cbStaff.SelectedItem).Tag.ToString().Trim();

            for (int i = 0; i < m_StaffList.Count; i++)
            {
                if (m_StaffList[i].FSSTAFFID.ToString().Trim().Equals(strStaffID))
                {
                    tbxENAME.Text = m_StaffList[i].FSENAME.ToString().Trim();       //外文姓名
                    tbxWEBSITE.Text = m_StaffList[i].FSWEBSITE.ToString().Trim();  //個人網站
                    tbxINTRO.Text = m_StaffList[i].FSINTRO.ToString().Trim();       //簡介
                    tbxEINTRO.Text = m_StaffList[i].FSEINTRO.ToString().Trim();     //外文簡介
                    break;
                }
            }
        }

        #endregion

        #region ComboBox載入代碼檔

        //實作-下載所有的代碼檔
        void client_fnGetTBPROGMEN_CODECompleted(object sender, fnGetTBPROGMEN_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            case "TBZTITLE":      //頭銜代碼
                                ComboBoxItem cbiTITLE = new ComboBoxItem();
                                cbiTITLE.Content = CodeData.NAME;
                                cbiTITLE.Tag = CodeData.ID;
                                this.cbTITLE_ID.Items.Add(cbiTITLE);
                                break;
                            default:
                                break;
                        }
                    }

                    //將class裡的資料放到畫面上，一定要在代碼檔之後
                    m_strAUTO = m_FormData.AUTO.ToString().Trim();                    //主Key
                    tbxPROG_NAME.Tag = m_FormData.FSPROG_ID.ToString().Trim();        //節目編號
                    compareTITLEID(m_FormData.FSTITLEID.ToString().Trim());           //頭銜代碼                   
                }
            }
        }

        #endregion

        #region 檢查畫面資料及搬值  

        #endregion

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //開啟節目資料查詢畫面
        private void btnPROG_Click(object sender, RoutedEventArgs e)
        {
            PROGDATA_VIEW PROGDATA_VIEW_frm = new PROGDATA_VIEW();
            PROGDATA_VIEW_frm.Show();

            PROGDATA_VIEW_frm.Closed += (s, args) =>
            {
                if (PROGDATA_VIEW_frm.DialogResult == true)
                {
                    tbxPROG_NAME.Text = PROGDATA_VIEW_frm.strProgName_View;
                    tbxPROG_NAME.Tag = PROGDATA_VIEW_frm.strProgID_View;

                    tbxEPISODE.Text = "";
                    lblEPISODE_NAME.Content = "";
                }
            };
        }

        //開啟節目子集資料查詢畫面
        private void btnEPISODE_Click(object sender, RoutedEventArgs e)
        {
            if (tbxPROG_NAME.Text.ToString().Trim() != "" && tbxPROG_NAME.Tag.ToString().Trim() != "")
            {
                PRG.PRG200_07 PRG200_07_frm = new PRG.PRG200_07(tbxPROG_NAME.Tag.ToString().Trim(), tbxPROG_NAME.Text.ToString().Trim());
                PRG200_07_frm.Show();

                PRG200_07_frm.Closed += (s, args) =>
                {
                    if (PRG200_07_frm.DialogResult == true)
                    {
                        tbxEPISODE.Text = PRG200_07_frm.m_strEpisode_View;
                        lblEPISODE_NAME.Content = PRG200_07_frm.m_strProgDName_View;
                    }
                };
            }
            else
                MessageBox.Show("請先選擇節目！", "提示訊息", MessageBoxButton.OK);
        }
    }
}

