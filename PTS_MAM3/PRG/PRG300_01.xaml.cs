﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSPROGLINK;
using PTS_MAM3.WSPROG_M;
using System.Text;

namespace PTS_MAM3.ProgData
{
    public partial class PROGLINK_Add : ChildWindow
    {
        WSPROGLINKSoapClient client = new WSPROGLINKSoapClient() ;
        WSPROG_MSoapClient clientProgM = new WSPROG_MSoapClient();
        string m_strProgName = "";                                      //暫存衍生節目名稱 
        string m_strEpisode = "";                                       //暫存衍生集別
        Boolean m_Continue = false;
        public Class_PROGLINK m_Queryobj = new Class_PROGLINK();        //查詢新增的obj
        string m_strSW = "";                                            //判斷要檢查的是衍生或是扣播出

        public PROGLINK_Add()
        {
            InitializeComponent();
            InitializeForm();          //初始化本頁面
        }

        void InitializeForm() //初始化本頁面
        {
            //新增扣播出次數節目子集資料
            client.INSERT_TBPROLINKCompleted += new EventHandler<INSERT_TBPROLINKCompletedEventArgs>(client_INSERT_TBPROLINKCompleted);

            //查詢扣播出次數節目子集資料(避免重複)
            client.QUERY_TBPROGLINK_CHECKCompleted += new EventHandler<QUERY_TBPROGLINK_CHECKCompletedEventArgs>(client_QUERY_TBPROGLINK_CHECKCompleted);

            //查詢節目是否為衍生節目
            clientProgM.fnGetProg_M_LINKCompleted += new EventHandler<fnGetProg_M_LINKCompletedEventArgs>(clientProgM_fnGetProg_M_LINKCompleted);                      
        }
       
        #region 實作

        //實作-新增扣播出次數資料
        void client_INSERT_TBPROLINKCompleted(object sender, INSERT_TBPROLINKCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result.bolResult == true)
                {
                    MessageBox.Show("新增衍生子集資料「" + m_strProgName.ToString() + "-" + m_strEpisode + "集」成功！", "提示訊息", MessageBoxButton.OK);

                    if (m_Continue == false)          //讓使用者可以一直新增同樣內容的資料
                        this.DialogResult = true;  
                }
                else
                {
                    MessageBox.Show("新增衍生子集資料「" + m_strProgName.ToString() + "-" + m_strEpisode + "集」失敗！\n失敗原因：" + e.Result.strErrorMsg, "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }            
        }

        //實作-查詢扣播出次數節目子集資料(避免重複)
        void client_QUERY_TBPROGLINK_CHECKCompleted(object sender, QUERY_TBPROGLINK_CHECKCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    if (e.Result == "0")
                    {
                        //新增扣播出次數節目子集資料
                        client.INSERT_TBPROLINKAsync(FormToClass_PROGLINK());
                        BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                    }
                    else
                    {
                        MessageBox.Show("資料庫已有重複資料，請檢查後重新輸入！", "提示訊息", MessageBoxButton.OK);
                    }
                }
            }
        }       

        //實作-查詢節目是否為衍生節目
        void clientProgM_fnGetProg_M_LINKCompleted(object sender, fnGetProg_M_LINKCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result == "Y")
                {
                    if (m_strSW == "LINK")                    
                        checkProgLink_PLAY();   //繼續判斷節目資料是否為扣播出                    
                    else
                        MessageBox.Show("「" + tbxORGPROG_NAME.Text.Trim() + "」在節目基本資料裡已設定為衍生節目，\n因此無法新增為扣播出次數子集！", "提示訊息", MessageBoxButton.OK);                      
                }
                else if (e.Result == "N")
                {
                    if (m_strSW == "LINK")                   
                        MessageBox.Show("「" + tbxPROG_NAME.Text.Trim() + "」在節目基本資料裡已設定為非衍生節目，\n因此無法新增為衍生子集！", "提示訊息", MessageBoxButton.OK);                   
                    else
                    {
                        //檢查是否有相同的資料，若有就無法新增
                        client.QUERY_TBPROGLINK_CHECKAsync(FormToClass_PROGLINK()); //新增資料
                        BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                    }
                }
                else
                    MessageBox.Show("讀取節目基本資料失敗，請重新查詢！", "提示訊息", MessageBoxButton.OK);
            }
        }

        #endregion

        #region 檢查畫面資料及搬值

        private Class_PROGLINK FormToClass_PROGLINK()
        {
            Class_PROGLINK obj = new Class_PROGLINK();

            obj.FSPROG_ID = tbxPROG_NAME.Tag.ToString().Trim();     //節目編碼
            m_strProgName = tbxPROG_NAME.Text.ToString().Trim();    //暫存本次新增的節目名稱，為了秀給使用者看結果用

            if (tbxEPISODE.Text.ToString().Trim() == "")            //集別
            {
                obj.FNEPISODE = 32767;
                m_strEpisode = "";
            }
            else
            {
                obj.FNEPISODE = Convert.ToInt16(tbxEPISODE.Text.ToString().Trim());
                m_strEpisode = tbxEPISODE.Text.ToString().Trim();       //暫存本次新增的集別，為了秀給使用者看結果用
            }

            obj.FSORGPROG_ID = tbxORGPROG_NAME.Tag.ToString().Trim();     //節目編碼

            if (tbxORGEPISODE.Text.ToString().Trim() == "")            //集別          
                obj.FNORGEPISODE = 32767;        
            else
                obj.FNORGEPISODE = Convert.ToInt16(tbxORGEPISODE.Text.ToString().Trim());

            obj.FSMEMO = tbxMEMO.Text.ToString().Trim();

            obj.FSCREATED_BY = UserClass.userData.FSUSER_ID.ToString();//建檔者
            obj.FSUPDATED_BY = UserClass.userData.FSUSER_ID.ToString();//修改者  
                   
            m_Queryobj = obj;

            return obj;
        }

        private Boolean Check_PROGLINKData()    //檢查畫面上的欄位是否都填妥
        {
            StringBuilder strMessage = new StringBuilder();

            if (tbxPROG_NAME.Text.Trim() == "")
                strMessage.AppendLine("請選擇「衍生節目」");

            if (tbxEPISODE.Text.Trim() == "")
                strMessage.AppendLine("請選擇「衍生子集」");

            if (tbxORGPROG_NAME.Text.Trim() == "")
                strMessage.AppendLine("請選擇「扣播出次數節目」");

            if (tbxORGEPISODE.Text.Trim() == "")
                strMessage.AppendLine("請選擇「扣播出次數子集」");    

            if (tbxPROG_NAME.Text.Trim() == tbxORGPROG_NAME.Text.Trim() && tbxEPISODE.Text.Trim() == tbxORGEPISODE.Text.Trim())
                strMessage.AppendLine("衍生節目、集別與扣播出次數節目、集別相同無法新增，請重新選擇");

            if (strMessage.ToString().Trim() == "")
                return true;
            else
            {
                MessageBoxResult result = MessageBox.Show(strMessage.ToString(), "提示訊息", MessageBoxButton.OK);
                return false;
            }
        }

        #endregion   

        //確定
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (Check_PROGLINKData() == false)      //檢查畫面上的欄位是否都填妥
                return;

            m_Continue = false;
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
            checkProgLink();        //查詢衍生節目是否為衍生節目
        }

        //繼續新增
        private void ContinueButton_Click(object sender, RoutedEventArgs e)
        {
            if (Check_PROGLINKData() == false)      //檢查畫面上的欄位是否都填妥
                return;

            m_Continue = true;
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
            checkProgLink();        //查詢衍生節目是否為衍生節目
        }

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //查詢衍生節目是否為衍生節目
        private void checkProgLink()    
        {
            string strProgID = tbxPROG_NAME.Tag.ToString().Trim();          //節目編號
            m_strSW = "LINK";   //檢查衍生

            clientProgM.fnGetProg_M_LINKAsync(strProgID);
        }

        //查詢扣播出節目是否為非衍生節目
        private void checkProgLink_PLAY()
        {
            string strProgID = tbxORGPROG_NAME.Tag.ToString().Trim();       //節目編號
            m_strSW = "PLAY";   //檢查扣播出

            clientProgM.fnGetProg_M_LINKAsync(strProgID);
        }  

        //開啟節目資料查詢畫面(衍生)
        private void btnPROG_Click(object sender, RoutedEventArgs e)
        {
            PROGDATA_VIEW PROGDATA_VIEW_frm = new PROGDATA_VIEW();
            PROGDATA_VIEW_frm.Show();

            PROGDATA_VIEW_frm.Closed += (s, args) =>
            {
                if (PROGDATA_VIEW_frm.DialogResult == true)
                {
                    tbxPROG_NAME.Text = PROGDATA_VIEW_frm.strProgName_View;
                    tbxPROG_NAME.Tag = PROGDATA_VIEW_frm.strProgID_View;

                    tbxEPISODE.Text = "";
                    lblEPISODE_NAME.Content = "";
                }
            };
        }

        //開啟節目子集資料查詢畫面(衍生)
        private void btnEPISODE_Click(object sender, RoutedEventArgs e)
        {
            if (tbxPROG_NAME.Text.ToString().Trim() != "" && tbxPROG_NAME.Tag.ToString().Trim() != "")
            {
                PRG.PRG200_07 PRG200_07_frm = new PRG.PRG200_07(tbxPROG_NAME.Tag.ToString().Trim(), tbxPROG_NAME.Text.ToString().Trim());
                PRG200_07_frm.Show();

                PRG200_07_frm.Closed += (s, args) =>
                {
                    if (PRG200_07_frm.DialogResult == true)
                    {
                        tbxEPISODE.Text = PRG200_07_frm.m_strEpisode_View;
                        lblEPISODE_NAME.Content = PRG200_07_frm.m_strProgDName_View;
                    }
                };
            }
            else
                MessageBox.Show("請先選擇節目！", "提示訊息", MessageBoxButton.OK);
        }

        //開啟節目資料查詢畫面(扣播出次數)
        private void btnORGPROG_Click(object sender, RoutedEventArgs e)
        {
            PROGDATA_VIEW PROGDATA_VIEW_frm = new PROGDATA_VIEW();
            PROGDATA_VIEW_frm.Show();

            PROGDATA_VIEW_frm.Closed += (s, args) =>
            {
                if (PROGDATA_VIEW_frm.DialogResult == true)
                {
                    tbxORGPROG_NAME.Text = PROGDATA_VIEW_frm.strProgName_View;
                    tbxORGPROG_NAME.Tag = PROGDATA_VIEW_frm.strProgID_View;

                    tbxORGEPISODE.Text = "";
                    lblORGEPISODE_NAME.Content = "";
                }
            };
        }

        //開啟節目子集資料查詢畫面(扣播出次數)
        private void btnORGEPISODE_Click(object sender, RoutedEventArgs e)
        {
            if (tbxORGPROG_NAME.Text.ToString().Trim() != "" && tbxORGPROG_NAME.Tag.ToString().Trim() != "")
            {
                PRG.PRG200_07 PRG200_07_frm = new PRG.PRG200_07(tbxORGPROG_NAME.Tag.ToString().Trim(), tbxORGPROG_NAME.Text.ToString().Trim());
                PRG200_07_frm.Show();

                PRG200_07_frm.Closed += (s, args) =>
                {
                    if (PRG200_07_frm.DialogResult == true)
                    {
                        tbxORGEPISODE.Text = PRG200_07_frm.m_strEpisode_View;
                        lblORGEPISODE_NAME.Content = PRG200_07_frm.m_strProgDName_View;
                    }
                };
            }
            else
                MessageBox.Show("請先選擇節目！", "提示訊息", MessageBoxButton.OK);
        }
       
    }
}

