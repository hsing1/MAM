﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;
using System.IO;
using System.Text;
using PTS_MAM3.WSPROG_M;
using PTS_MAM3.ProgData;
using DataGridDCTest;
using PTS_MAM3.WSBROADCAST;
using System.Windows.Browser;   //使用HtmlPage要先引用這行 

namespace PTS_MAM3.ProgData
{
    public partial class PROGDATA_VIEW : ChildWindow
    {
        WSPROG_MSoapClient client = new WSPROG_MSoapClient();  //產生新的代理類別
        WSBROADCASTSoapClient clientBro = new WSBROADCASTSoapClient();                

        public string strProgID_View = "";      //選取的節目編號
        public string strProgName_View = "";    //選取的節目名稱
        public string strTOTLEEPISODE = "";     //全部集數
        public string strFNLENGTH = "";         //節目總長
        public string strCHANNEL = "";          //可播映頻道
        public string strChannel_Id = "";       //頻道別
        public string strProducer = "";         //製作人
        public string strDEP_ID = "";           //成案單位

        public PROGDATA_VIEW()
        {
            InitializeComponent();             
            InitializeForm();        //初始化本頁面
        }    

        void InitializeForm() //初始化本頁面
        {
            //取得全部節目資料
            client.fnGetTBPROG_M_ALLCompleted += new EventHandler<fnGetTBPROG_M_ALLCompletedEventArgs>(client_fnGetTBPROG_M_ALLCompleted);
            //client.fnGetTBPROG_M_ALLAsync();  //預設不Load全部的節目資料，必須輸入查詢

            //查詢節目資料By節目名稱        
            client.fnQProg_M_BYPGDNAMECompleted += new EventHandler<fnQProg_M_BYPGDNAMECompletedEventArgs>(client_fnQProg_M_BYPGDNAMECompleted);

            //查詢節目資料
            client.fnQUERYTBPROG_MCompleted += new EventHandler<fnQUERYTBPROG_MCompletedEventArgs>(client_fnQUERYTBPROG_MCompleted);

            //查詢節目製作人
            clientBro.GetPRODUCERCompleted += new EventHandler<GetPRODUCERCompletedEventArgs>(clientBro_GetPRODUCERCompleted);
        }

        void LoadBusy()
        {
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
            btnSearch.IsEnabled = false;
            btnMy.IsEnabled = false;
            btnChannel.IsEnabled = false;
            tbxPGMNAME.IsEnabled = false;
        }

        void LoadBusyUnLock()
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息
            btnSearch.IsEnabled = true;
            btnMy.IsEnabled = true;
            btnChannel.IsEnabled = true;
            tbxPGMNAME.IsEnabled = true;
            tbxPGMNAME.Focus();
        }

        //實作-查詢節目資料By節目名稱
        void client_fnQProg_M_BYPGDNAMECompleted(object sender, fnQProg_M_BYPGDNAMECompletedEventArgs e)
        {
            LoadBusyUnLock();

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    this.DGPROG_MListD.DataContext = e.Result;

                    if (this.DGPROG_MListD.DataContext != null)
                    {
                        DGPROG_MListD.SelectedIndex = 0;
                    }
                }
                else
                {
                    MessageBox.Show("查無節目主檔資料，請重新輸入條件查詢！", "提示訊息", MessageBoxButton.OK);
                    return;
                }
            }
        }

        //實作-取得全部節目資料
        void client_fnGetTBPROG_M_ALLCompleted(object sender, fnGetTBPROG_M_ALLCompletedEventArgs e)
        {
            LoadBusyUnLock();

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    this.DGPROG_MListD.DataContext = e.Result;

                    if (this.DGPROG_MListD.DataContext != null)
                    {
                        DGPROG_MListD.SelectedIndex = 0;
                    }
                }
            } 
        }

        //實作-查詢節目製作人
        void clientBro_GetPRODUCERCompleted(object sender, GetPRODUCERCompletedEventArgs e)
        {
            LoadBusyUnLock();

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Trim() != "")
                {
                    strProducer = e.Result;
                }
            }
            this.DialogResult = true;
        }

        //實作-查詢節目資料
        void client_fnQUERYTBPROG_MCompleted(object sender, fnQUERYTBPROG_MCompletedEventArgs e)
        {
            LoadBusyUnLock();

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    this.DGPROG_MListD.DataContext = e.Result;

                    if (this.DGPROG_MListD.DataContext != null)
                    {
                        DGPROG_MListD.SelectedIndex = 0;
                    }
                }
                else
                {
                    MessageBox.Show("查無節目基本資料，請重新輸入條件查詢！", "提示訊息", MessageBoxButton.OK);
                    return;
                }
            }
        }

        //確定
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            ////判斷是否有選取DataGrid
            //if (DGPROG_MList.SelectedItem == null)
            //{
            //    MessageBox.Show("請選擇節目資料", "提示訊息", MessageBoxButton.OK);
            //    return;
            //}
            //else
            //{
            //    strProgID_View = ((Class_PROGM)DGPROG_MList.SelectedItem).FSPROG_ID.ToString();     
            //    strProgName_View = ((Class_PROGM)DGPROG_MList.SelectedItem).FSPGMNAME.ToString(); 
            //    this.DialogResult = true;
            //}

            //判斷是否有選取DataGrid
            if (DGPROG_MListD.SelectedItem == null)
            {
                MessageBox.Show("請選擇節目資料", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else
            {
                strProgID_View = ((Class_PROGM)DGPROG_MListD.SelectedItem).FSPROG_ID.ToString();
                strProgName_View = ((Class_PROGM)DGPROG_MListD.SelectedItem).FSPGMNAME.ToString();
                strTOTLEEPISODE = ((Class_PROGM)DGPROG_MListD.SelectedItem).FNTOTEPISODE.ToString();
                strFNLENGTH = ((Class_PROGM)DGPROG_MListD.SelectedItem).FNLENGTH.ToString();
                strCHANNEL = ((Class_PROGM)DGPROG_MListD.SelectedItem).FSCHANNEL.ToString();
                strChannel_Id = ((Class_PROGM)DGPROG_MListD.SelectedItem).FSCHANNEL_ID.ToString();
                strDEP_ID = ((Class_PROGM)DGPROG_MListD.SelectedItem).FNDEP_ID.ToString();
                //this.DialogResult = true;

                //將查詢製作人拉到流程引擎處理，因此程式先註解
                clientBro.GetPRODUCERAsync("G", strProgID_View);    //選定節目後，再去查詢節目製作人
                LoadBusy();
            }
        }

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //查詢
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            Class_PROGM obj = new Class_PROGM();

            obj.FSPGMNAME = tbxPGMNAME.Text.Trim();

            client.fnQUERYTBPROG_MAsync(obj);
            LoadBusy();         
        }

        //本人頻道
        private void btnChannel_Click(object sender, RoutedEventArgs e)
        {
            Class_PROGM obj = new Class_PROGM();

            obj.FSPGMNAME = tbxPGMNAME.Text.Trim();
            obj.FSCHANNEL_ID = UserClass.userData.FSCHANNEL_ID.ToString();

            client.fnQUERYTBPROG_MAsync(obj);
            LoadBusy();
        }

        //本人建立
        private void btnMy_Click(object sender, RoutedEventArgs e)
        {
            Class_PROGM obj = new Class_PROGM();

            obj.FSPGMNAME = tbxPGMNAME.Text.Trim();
            obj.FSCRTUSER = UserClass.userData.FSUSER_ID.ToString();

            client.fnQUERYTBPROG_MAsync(obj);
            LoadBusy();
        }

        //連點事件
        private void RowDoubleClick(object sender, DataGridRowClickedArgs e)
        {
            DoubleClickDataGrid dcdg = sender as DoubleClickDataGrid;
            //Class_PROGM a = (dcdg.SelectedItem) as Class_PROGM;
            //MessageBox.Show(a.FSPGMNAME, "提示訊息", MessageBoxButton.OK);

            strProgID_View = ((Class_PROGM)dcdg.SelectedItem).FSPROG_ID.ToString();
            strProgName_View = ((Class_PROGM)dcdg.SelectedItem).FSPGMNAME.ToString();
            strTOTLEEPISODE = ((Class_PROGM)DGPROG_MListD.SelectedItem).FNTOTEPISODE.ToString();
            strFNLENGTH = ((Class_PROGM)DGPROG_MListD.SelectedItem).FNLENGTH.ToString();
            strCHANNEL = ((Class_PROGM)DGPROG_MListD.SelectedItem).FSCHANNEL.ToString();
            strChannel_Id = ((Class_PROGM)DGPROG_MListD.SelectedItem).FSCHANNEL_ID.ToString();
            strDEP_ID = ((Class_PROGM)DGPROG_MListD.SelectedItem).FNDEP_ID.ToString();
            //this.DialogResult = true;

            //將查詢製作人拉到流程引擎處理，因此程式先註解
            clientBro.GetPRODUCERAsync("G", strProgID_View);    //選定節目後，再去查詢節目製作人
            LoadBusy();
        }

        //輸入完查詢內容後按下enter即可查詢
        private void tbxPGMNAME_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                btnSearch_Click(sender,e);
            }
        }
        
    }
}

