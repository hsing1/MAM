﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;
using System.IO;
using System.Text;
using PTS_MAM3.WSPROGRACE;
using PTS_MAM3.PROG_M;

namespace PTS_MAM3.ProgData
{
    public partial class PRG500_03 : ChildWindow
    {
        WSPROGRACESoapClient client = new WSPROGRACESoapClient();   //產生新的代理類別
        List<string> m_ListRace = new List<string>();               //參展名稱集合 
        List<string> m_ListWin = new List<string>();                //得獎名稱集合
        Class_PROGRACE m_FormData = new Class_PROGRACE();
        string m_strAUTO = "";                                      //參展得獎主Key
        string m_strProgName = "";                                  //暫存節目名稱 
        string m_strEpisode = "";                                   //暫存集別
        public Class_PROGRACE m_Queryobj = new Class_PROGRACE();    //查詢修改的obj

        public PRG500_03(Class_PROGRACE FormData)
        {
            InitializeComponent();
            InitializeForm();          //初始化本頁面
            m_FormData = FormData;
            ClassToForm();
        }

        void InitializeForm() //初始化本頁面
        {
            //下載代碼檔//原本參展狀態是用代碼檔，後來改成只有三種：入選、入圍、得獎
            client.fnGetTBPROGRACE_CODECompleted += new EventHandler<fnGetTBPROGRACE_CODECompletedEventArgs>(client_fnGetTBPROGRACE_CODECompleted);
            client.fnGetTBPROGRACE_CODEAsync();            
   
            //參展名稱 AUTO COMPLETE
            client.fnGetRACECompleted += new EventHandler<fnGetRACECompletedEventArgs>(client_fnGetRACECompleted);
            client.fnGetRACEAsync();

            //得獎名稱 AUTO COMPLETE
            client.fnGetWINCompleted += new EventHandler<fnGetWINCompletedEventArgs>(client_fnGetWINCompleted);
            client.fnGetWINAsync();
        }
           
        private void ClassToForm()  //將前一頁點選的節目資料繫集到畫面上
        {
            this.gdPROG_RACE.DataContext = m_FormData;
            tbxPROGRACE.Text = m_FormData.FSPROGRACE.ToString().Trim();       //這兩個欄位是bind到AutoComplete，因此要特別指定
            tbxPROGWIN.Text = m_FormData.FSPROGWIN.ToString().Trim();

            m_strAUTO = m_FormData.AUTO.ToString().Trim();                    //主Key
            tbxPROG_NAME.Tag = m_FormData.FSPROG_ID.ToString().Trim();        //節目編號
            compareStatus(m_FormData.FSPROGSTATUS.ToString().Trim());         //參展狀態        
        }

        #region 實作      

        //實作-得獎名稱
        void client_fnGetWINCompleted(object sender, fnGetWINCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    tbxPROGWIN.ItemsSource = e.Result;
                }
            }
        }

        //實作-參展名稱
        void client_fnGetRACECompleted(object sender, fnGetRACECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    tbxPROGRACE.ItemsSource = e.Result;
                }
            }
        }

        #endregion

        #region ComboBox載入代碼檔

        //實作-下載所有的代碼檔
        void client_fnGetTBPROGRACE_CODECompleted(object sender, fnGetTBPROGRACE_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            //case "TBZPROGSTATUS":      //參展狀態代碼
                            //    ComboBoxItem cbiPROGSTATUS = new ComboBoxItem();
                            //    cbiPROGSTATUS.Content = CodeData.NAME;
                            //    cbiPROGSTATUS.Tag = CodeData.ID;
                            //    this.cbPROGSTATUS.Items.Add(cbiPROGSTATUS);
                            //    break;
                            case "TBZPROGRACE_AREA":    //參展區域                              
                                ComboBoxItem cbiPRGAREA = new ComboBoxItem();
                                cbiPRGAREA.Content = CodeData.NAME;
                                cbiPRGAREA.Tag = CodeData.ID;
                                this.cbAREA.Items.Add(cbiPRGAREA);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            compareArea(m_FormData.FSAREA.ToString().Trim());                   //參展區域，必須要再查詢出代碼之後  
        }

        void compareStatus(string strNAME)      //比對代碼檔_參展狀態
        {
            for (int i = 0; i < cbPROGSTATUS.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGSTATUS.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strNAME))
                {
                    cbPROGSTATUS.SelectedIndex = i;
                    break;
                }
            }
        }

        private void compareArea(string strArea)   //參展區域
        {
            for (int i = 0; i < cbAREA.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbAREA.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strArea))
                {
                    cbAREA.SelectedIndex = i;
                    tbxAREA.Text = getcbi.Content.ToString().Trim();
                    tbxAREA.Tag = getcbi.Tag.ToString().Trim();
                    break;
                }
            }
        }
        
        #endregion        

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

