﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;
using System.IO;
using System.Text;
using PTS_MAM3.WSPROGMEN;
using PTS_MAM3.PROG_M;
using DataGridDCTest;

namespace PTS_MAM3.ProgData
{
    public partial class PRG400_05 : ChildWindow
    {
        WSPROGMENSoapClient client = new WSPROGMENSoapClient();          //產生新的代理類別
        List<ClassCode> m_CodeData = new List<ClassCode>();              //代碼檔集合
        List<Class_PROGSTAFF> m_StaffList = new List<Class_PROGSTAFF>(); //工作人員檔集合
        Class_PROGMEN m_FormData = new Class_PROGMEN();
        string m_strAUTO = "";                                           //相關人員主Key
        public Class_PROGMEN m_Queryobj = new Class_PROGMEN();           //查詢修改的obj

        public PRG400_05(string strProgID , string strEpisode)
        {
            InitializeComponent();
            //查詢相關人員資料
            client.fnQUERY_TBPROGMENCompleted += new EventHandler<fnQUERY_TBPROGMENCompletedEventArgs>(client_fnQUERY_TBPROGMENCompleted);

            QUERY_TBPROGMEN(strProgID, strEpisode);
        }

        void InitializeForm() //初始化本頁面
        {
            //下載代碼檔           
            client.fnGetTBPROGMEN_CODECompleted += new EventHandler<fnGetTBPROGMEN_CODECompletedEventArgs>(client_fnGetTBPROGMEN_CODECompleted);
            client.fnGetTBPROGMEN_CODEAsync();
            
            //查詢節目管理系統的工作人員資料
            client.fnGetTBPROGSTAFFCompleted += new EventHandler<fnGetTBPROGSTAFFCompletedEventArgs>(client_fnGetTBPROGSTAFFCompleted);
            client.fnGetTBPROGSTAFFAsync();            
        }    

        private void QUERY_TBPROGMEN(string strProgID, string strEpisode)
        {
            Class_PROGMEN obj = new Class_PROGMEN();

            obj.FSPROG_ID = strProgID;
            obj.FNEPISODE = Convert.ToInt16(strEpisode);

            obj.FSTITLEID = "";
            obj.FSEMAIL = "";
            obj.FSSTAFFID = "";
            obj.FSNAME = "";
            obj.FSMEMO = "";
           client.fnQUERY_TBPROGMENAsync(obj);           
        }

        private void ClassToForm()  //將前一頁點選的節目資料繫集到畫面上
        {
            this.gdPROG_MEN.DataContext = m_FormData;
        }

        #region 實作

        //實作-查詢相關人員資料
        void client_fnQUERY_TBPROGMENCompleted(object sender, fnQUERY_TBPROGMENCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)  //若是有查詢結果，則顯示，不然就請使用者重新查詢
                {
                    this.DGPROG_MenList.DataContext = e.Result;

                    if (this.DGPROG_MenList.DataContext != null)
                    {
                        DGPROG_MenList.SelectedIndex = 0;
                        m_FormData = e.Result[0];
                        ClassToForm();
                        InitializeForm();            //初始化本頁面
                    }
                }
                else
                {
                    MessageBox.Show("查無相關人員資料，請重新輸入條件查詢！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
        }
        
        //實作-查詢節目管理系統的工作人員資料
        void client_fnGetTBPROGSTAFFCompleted(object sender, fnGetTBPROGSTAFFCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        m_StaffList.Add(e.Result[i]);
                        ComboBoxItem cbiStaff = new ComboBoxItem();
                        cbiStaff.Content = e.Result[i].FSNAME;
                        cbiStaff.Tag = e.Result[i].FSSTAFFID;
                        this.cbStaff.Items.Add(cbiStaff);   //取得的工作人員資料塞到ComboBox裡
                    }

                    //必須在代碼都下載完後才能比對
                    compareSTAFFID(m_FormData.FSSTAFFID.ToString().Trim());           //工作人員代碼
                }
            }
        }

        void compareTITLEID(string strID)      //比對代碼檔_頭銜代碼
        {
            for (int i = 0; i < cbTITLE_ID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbTITLE_ID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbTITLE_ID.SelectedIndex = i;
                    break;
                }
            }
        }

        void compareSTAFFID(string strID)      //工作人員代碼
        {
            for (int i = 0; i < cbStaff.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbStaff.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbStaff.SelectedIndex = i;
                    break;
                }
            }
        }

        //選擇工作人員時，要秀工作人員的詳細內容
        private void cbStaff_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string strStaffID = ((ComboBoxItem)cbStaff.SelectedItem).Tag.ToString().Trim();

            for (int i = 0; i < m_StaffList.Count; i++)
            {
                if (m_StaffList[i].FSSTAFFID.ToString().Trim().Equals(strStaffID))
                {
                    tbxENAME.Text = m_StaffList[i].FSENAME.ToString().Trim();       //外文姓名
                    tbxWEBSITE.Text = m_StaffList[i].FSWEBSITE.ToString().Trim();  //個人網站
                    tbxINTRO.Text = m_StaffList[i].FSINTRO.ToString().Trim();       //簡介
                    tbxEINTRO.Text = m_StaffList[i].FSEINTRO.ToString().Trim();     //外文簡介
                    break;
                }
            }
        }

        #endregion

        #region ComboBox載入代碼檔

        //實作-下載所有的代碼檔
        void client_fnGetTBPROGMEN_CODECompleted(object sender, fnGetTBPROGMEN_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            case "TBZTITLE":      //頭銜代碼
                                ComboBoxItem cbiTITLE = new ComboBoxItem();
                                cbiTITLE.Content = CodeData.NAME;
                                cbiTITLE.Tag = CodeData.ID;
                                this.cbTITLE_ID.Items.Add(cbiTITLE);
                                break;
                            default:
                                break;
                        }
                    }

                    //將class裡的資料放到畫面上，一定要在代碼檔之後
                    m_strAUTO = m_FormData.AUTO.ToString().Trim();                    //主Key
                    tbxPROG_NAME.Tag = m_FormData.FSPROG_ID.ToString().Trim();        //節目編號
                    compareTITLEID(m_FormData.FSTITLEID.ToString().Trim());           //頭銜代碼                   
                }
            }
        }

        #endregion

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //檢視
        private void ViewButton_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGPROG_MenList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預檢視的相關人員資料", "提示訊息", MessageBoxButton.OK);
                return;
            }
            
            m_FormData = (Class_PROGMEN)DGPROG_MenList.SelectedItem;
            ClassToForm();
            
            m_strAUTO = m_FormData.AUTO.ToString().Trim();                    //主Key
            tbxPROG_NAME.Tag = m_FormData.FSPROG_ID.ToString().Trim();        //節目編號
            compareTITLEID(m_FormData.FSTITLEID.ToString().Trim());           //頭銜代碼 
            compareSTAFFID(m_FormData.FSSTAFFID.ToString().Trim());           //工作人員代碼
        }    
    }
}

