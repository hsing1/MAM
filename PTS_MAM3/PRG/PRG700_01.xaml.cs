﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.PRG
{
    public partial class PRG700_01 : ChildWindow
    {

#region"宣告變數"

        String m_Action;
        public PTS_MAM3.WSSTAFF.Class_STAFF clSTAFF = new WSSTAFF.Class_STAFF();
        PTS_MAM3.WSSTAFF.WSSTAFFSoapClient clntSTAFF = new WSSTAFF.WSSTAFFSoapClient(); 
            
#endregion


#region"初始物件"

        public PRG700_01(String m_Action)
        {
            this.m_Action = m_Action;

            InitializeComponent();
            InitializeForm();
        }

        public PRG700_01(String m_Action, PTS_MAM3.WSSTAFF.Class_STAFF clSTAFF)
        {
            this.m_Action = m_Action;

            if (clSTAFF.FSSTAFFID == null)
            {
                this.clSTAFF.FSSTAFFID = "";
                this.clSTAFF.FSNAME = "";
                this.clSTAFF.FSENAME = "";
                this.clSTAFF.FSTEL = "";
                this.clSTAFF.FSFAX = "";
                this.clSTAFF.FSEMAIL = "";
                this.clSTAFF.FSADDRESS = "";
                this.clSTAFF.FSWEBSITE = "";
                this.clSTAFF.FSINTRO = "";
                this.clSTAFF.FSEINTRO = "";
                this.clSTAFF.FSMEMO = "";
            }
            else
            {
                this.clSTAFF = clSTAFF;
            }

            InitializeComponent();
            InitializeForm();
        }

        void InitializeForm() //初始化本頁面
        {
            clntSTAFF.INSERT_TBSTAFF_PGMCompleted += new EventHandler<WSSTAFF.INSERT_TBSTAFF_PGMCompletedEventArgs>(clntSTAFF_INSERT_TBSTAFF_PGMCompleted);
            clntSTAFF.UPDATE_TBSTAFF_PGMCompleted += new EventHandler<WSSTAFF.UPDATE_TBSTAFF_PGMCompletedEventArgs>(clntSTAFF_UPDATE_TBSTAFF_PGMCompleted);
           
            switch (m_Action)
            {
                case "ADD":
                    Title += "新增資料";
                    lblSTAFFID.Content = "(新增完畢自動取號)";
                    txtSTAFFID.Visibility = System.Windows.Visibility.Collapsed;

                    break;
                case "MOD":
                    Title += "修改資料";
                    ShowContent();

                    break;
                case "VEW":
                    Title += "檢視資料";
                    DisableItems();
                    ShowContent();

                    lblNAME.Content = "中文姓名";

                    OKButton.Visibility = System.Windows.Visibility.Collapsed;
                    CancelButton.Content = "確定";

                    break;
                case "QRY":
                    Title += "查詢資料";
                    ShowContent();

                    lblNAME.Content = "中文姓名";
                    lblSTAFFID.Visibility = System.Windows.Visibility.Collapsed;
                    txtSTAFFID.Visibility = System.Windows.Visibility.Visible;

                    OKButton.Margin = CancelButton.Margin;
                    CancelButton.Visibility = System.Windows.Visibility.Collapsed;

                    break;
            }
        }



#endregion


#region"觸發事件"

        void clntSTAFF_INSERT_TBSTAFF_PGMCompleted(object sender, WSSTAFF.INSERT_TBSTAFF_PGMCompletedEventArgs e)
        {
            //如果取回的資料沒有錯誤訊息
            if (e.Error == null)
            {
                String msg = e.Result;

                if (msg.Contains("ERROR"))
                {
                    //如果新增失敗
                    MessageBox.Show("新增時發生錯誤: " + msg.Substring(6), "提示訊息", MessageBoxButton.OK);
                    //this.DialogResult = false;
                }
                else
                {
                    //如果新增成功
                    clSTAFF.FSSTAFFID = msg;
                    MessageBox.Show("新增成功", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = true;
                }
            }
            else
            {
                //如果取回的資料有錯誤訊息
                MessageBox.Show("呼叫新增函數時發生錯誤: " + e.Error.ToString(), "提示訊息", MessageBoxButton.OK);
                //this.DialogResult = false;
            }
        }

        void clntSTAFF_UPDATE_TBSTAFF_PGMCompleted(object sender, WSSTAFF.UPDATE_TBSTAFF_PGMCompletedEventArgs e)
        {
            //如果取回的資料沒有錯誤訊息
            if (e.Error == null)
            {
                String msg = e.Result;
                if (msg.Contains("ERROR"))
                {
                    //如果修改失敗
                    MessageBox.Show("修改時發生錯誤: " + msg.Substring(6), "提示訊息", MessageBoxButton.OK);
                    //this.DialogResult = false;
                }
                else
                {
                    //如果修改成功
                    MessageBox.Show("修改成功", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = true;
                }
            }
            else
            {
                //如果取回的資料有錯誤訊息
                MessageBox.Show("呼叫修改函數時發生錯誤: " + e.Error.ToString(), "提示訊息", MessageBoxButton.OK);
                //this.DialogResult = false;
            }
        }  

#endregion


#region"按鈕動作"

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            //中文姓名不可以重複

            SetClassItems();

            if(m_Action == "ADD")
                clSTAFF.FSSTAFFID = "";

            switch (m_Action)
            {
                case "ADD":
                    if (txtNAME.Text.Trim() == "")
                    {
                        MessageBox.Show("「中文姓名」欄不可為空", "提示訊息", MessageBoxButton.OK);
                        return;
                    }

                    clntSTAFF.INSERT_TBSTAFF_PGMAsync(clSTAFF);
                    break;
                case "MOD":
                    if (txtNAME.Text.Trim() == "")
                    {
                        MessageBox.Show("「中文姓名」欄不可為空", "提示訊息", MessageBoxButton.OK);
                        return;
                    }

                    clntSTAFF.UPDATE_TBSTAFF_PGMAsync(clSTAFF);
                    break;
                case "QRY":
                    this.DialogResult = true;
                    break;
            }

            //this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

#endregion


#region"其他函數"

        void SetClassItems()
        {
            switch (m_Action)
            {
                case "ADD":
                    clSTAFF.FSSTAFFID = lblSTAFFID.Content.ToString();
                    break;
                case "MOD":
                    clSTAFF.FSSTAFFID = lblSTAFFID.Content.ToString();
                    break;
                case "QRY":
                    clSTAFF.FSSTAFFID = txtSTAFFID.Text.Trim();
                    break;
            }
            clSTAFF.FSNAME = txtNAME.Text.Trim();
            clSTAFF.FSENAME = txtENAME.Text.Trim();
            clSTAFF.FSTEL = txtTEL.Text.Trim();
            clSTAFF.FSFAX = txtFAX.Text.Trim();
            clSTAFF.FSEMAIL = txtEMAIL.Text.Trim();
            clSTAFF.FSADDRESS = txtADDRESS.Text.Trim();
            clSTAFF.FSWEBSITE = txtWEBSITE.Text.Trim();
            clSTAFF.FSINTRO = txtINTRO.Text.Trim();
            clSTAFF.FSEINTRO = txtEINTRO.Text.Trim();
            clSTAFF.FSMEMO = txtMEMO.Text.Trim();
            clSTAFF.FSCRTUSER = UserClass.userData.FSUSER_ID;
            clSTAFF.FDCRTDATE = DateTime.Now;
            clSTAFF.FSUPDUSER = UserClass.userData.FSUSER_ID;
            clSTAFF.FDUPDDATE  =  DateTime.Now;
        }

        void DisableItems()
        {
            lblSTAFFID.IsEnabled = false;
            txtNAME.IsReadOnly = true;
            txtENAME.IsReadOnly = true;
            txtTEL.IsReadOnly = true;
            txtFAX.IsReadOnly = true;
            txtEMAIL.IsReadOnly = true;
            txtADDRESS.IsReadOnly = true;
            txtWEBSITE.IsReadOnly = true;
            txtINTRO.IsReadOnly = true;
            txtEINTRO.IsReadOnly = true;
            txtMEMO.IsReadOnly = true;
        }

        void ShowContent()
        {
            lblSTAFFID.Content = clSTAFF.FSSTAFFID;
            txtSTAFFID.Text = clSTAFF.FSSTAFFID;
            txtNAME.Text = clSTAFF.FSNAME;
            txtENAME.Text = clSTAFF.FSENAME;
            txtTEL.Text = clSTAFF.FSTEL;
            txtFAX.Text = clSTAFF.FSFAX;
            txtEMAIL.Text = clSTAFF.FSEMAIL;
            txtADDRESS.Text = clSTAFF.FSADDRESS;
            txtWEBSITE.Text = clSTAFF.FSWEBSITE;
            txtINTRO.Text = clSTAFF.FSINTRO;
            txtEINTRO.Text = clSTAFF.FSEINTRO;
            txtMEMO.Text = clSTAFF.FSMEMO; 
        }

#endregion


    }
}

