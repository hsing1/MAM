﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;
using System.IO;
using System.Text;
using PTS_MAM3.WSPROGMEN;
using PTS_MAM3.PROG_M;

namespace PTS_MAM3.ProgData
{
    public partial class PROGMEN_Edit : ChildWindow
    {
        WSPROGMENSoapClient client = new WSPROGMENSoapClient();          //產生新的代理類別
        List<ClassCode> m_CodeData = new List<ClassCode>();              //代碼檔集合
        List<Class_PROGSTAFF> m_StaffList = new List<Class_PROGSTAFF>(); //工作人員檔集合
        string m_strProgName = "";                                       //暫存節目名稱 
        string m_strEpisode = "";                                        //暫存集別
        Class_PROGMEN m_FormData = new Class_PROGMEN();
        string m_strAUTO = "";                                           //相關人員主Key
        public Class_PROGMEN m_Queryobj = new Class_PROGMEN();           //查詢修改的obj
        List<string> m_StaffName = new List<string>();                   //工作人員姓名集合
        string m_strStaffID = "";                                        //工作人員ID

        public PROGMEN_Edit(Class_PROGMEN FormData)
        {
            InitializeComponent();
            InitializeForm();          //初始化本頁面
            m_FormData = FormData;
            ClassToForm();
        }

        void InitializeForm() //初始化本頁面
        {
            //下載代碼檔           
            client.fnGetTBPROGMEN_CODECompleted += new EventHandler<fnGetTBPROGMEN_CODECompletedEventArgs>(client_fnGetTBPROGMEN_CODECompleted);
            client.fnGetTBPROGMEN_CODEAsync();

            //修改相關人員資料
            client.fnUPDATE_TBPROGRACECompleted += new EventHandler<fnUPDATE_TBPROGRACECompletedEventArgs>(client_fnUPDATE_TBPROGRACECompleted);

            //查詢相關人員資料(避免重複)
            client.fnQUERY_TBPROGMEN_CHECKCompleted += new EventHandler<fnQUERY_TBPROGMEN_CHECKCompletedEventArgs>(client_fnQUERY_TBPROGMEN_CHECKCompleted);

            //查詢節目管理系統的工作人員資料
            client.fnGetTBPROGSTAFFCompleted += new EventHandler<fnGetTBPROGSTAFFCompletedEventArgs>(client_fnGetTBPROGSTAFFCompleted);
            client.fnGetTBPROGSTAFFAsync();

            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }

        private void ClassToForm()  //將前一頁點選的節目資料繫集到畫面上
        {
            this.gdPROG_MEN.DataContext = m_FormData;
        }

        #region 實作

        //實作-查詢相關人員資料(避免重複)
        void client_fnQUERY_TBPROGMEN_CHECKCompleted(object sender, fnQUERY_TBPROGMEN_CHECKCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;

            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    if (e.Result == "0")
                    {
                        //修改工作人員資料
                        client.fnUPDATE_TBPROGRACEAsync(FormToClass_PROGMEN());
                        BusyMsg.IsBusy = true;
                    }
                    else
                    {
                        MessageBox.Show("資料庫已有重複資料，請檢查後重新輸入！", "提示訊息", MessageBoxButton.OK);
                    }
                }
            }
        }

        //實作-修改相關人員資料
        void client_fnUPDATE_TBPROGRACECompleted(object sender, fnUPDATE_TBPROGRACECompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result.bolResult == true)
                {
                    MessageBox.Show("修改相關人員資料「" + m_strProgName.ToString() + "-" + m_strEpisode + "集」成功！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("修改相關人員資料「" + m_strProgName.ToString() + "-" + m_strEpisode + "集」失敗！\n失敗原因：" + e.Result.strErrorMsg, "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
        }

        //實作-查詢節目管理系統的工作人員資料
        void client_fnGetTBPROGSTAFFCompleted(object sender, fnGetTBPROGSTAFFCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null )
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        m_StaffList.Add(e.Result[i]);
                        m_StaffName.Add(e.Result[i].FSNAME);

                        ComboBoxItem cbiStaff = new ComboBoxItem();
                        cbiStaff.Content = e.Result[i].FSNAME;
                        cbiStaff.Tag = e.Result[i].FSSTAFFID;
                        this.cbStaff.Items.Add(cbiStaff);   //取得的工作人員資料塞到ComboBox裡
                    }

                    acbStaff.ItemsSource = m_StaffName;    //AutoComplete資料繫結

                    //必須在代碼都下載完後才能比對
                    compareSTAFFID(m_FormData.FSSTAFFID.ToString().Trim());           //工作人員代碼
                }
            }
        }

        void compareTITLEID(string strID)      //比對代碼檔_頭銜代碼
        {
            for (int i = 0; i < cbTITLE_ID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbTITLE_ID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbTITLE_ID.SelectedIndex = i;
                    break;
                }
            }
        }

        void compareSTAFFID(string strID)      //工作人員代碼
        {
            for (int i = 0; i < cbStaff.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbStaff.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbStaff.SelectedIndex = i;
                    break;
                }
            }
  
            for (int i = 0; i < m_StaffList.Count; i++)
            {
                if (m_StaffList[i].FSSTAFFID.ToString().Trim().Equals(strID))
                {
                    acbStaff.Text = m_StaffList[i].FSNAME.ToString().Trim();
                    m_strStaffID = m_StaffList[i].FSSTAFFID.ToString().Trim();
                    tbxENAME.Text = m_StaffList[i].FSENAME.ToString().Trim();       //外文姓名
                    tbxWEBSITE.Text = m_StaffList[i].FSWEBSITE.ToString().Trim();   //個人網站
                    tbxINTRO.Text = m_StaffList[i].FSINTRO.ToString().Trim();       //簡介
                    tbxEINTRO.Text = m_StaffList[i].FSEINTRO.ToString().Trim();     //外文簡介
                    break;
                }
            }
        }

        #endregion

        #region ComboBox載入代碼檔

        //實作-下載所有的代碼檔
        void client_fnGetTBPROGMEN_CODECompleted(object sender, fnGetTBPROGMEN_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            case "TBZTITLE":      //頭銜代碼
                                ComboBoxItem cbiTITLE = new ComboBoxItem();
                                cbiTITLE.Content = CodeData.NAME;
                                cbiTITLE.Tag = CodeData.ID;
                                this.cbTITLE_ID.Items.Add(cbiTITLE);
                                break;
                            default:
                                break;
                        }
                    }

                    //將class裡的資料放到畫面上，一定要在代碼檔之後
                    m_strAUTO = m_FormData.AUTO.ToString().Trim();                    //主Key
                    tbxPROG_NAME.Tag = m_FormData.FSPROG_ID.ToString().Trim();        //節目編號
                    compareTITLEID(m_FormData.FSTITLEID.ToString().Trim());           //頭銜代碼                   
                }
            }
        }

        #endregion

        #region 檢查畫面資料及搬值

        private Class_PROGMEN FormToClass_PROGMEN()
        {
            Class_PROGMEN obj = new Class_PROGMEN();

            obj.AUTO = m_strAUTO;
            obj.FSPROG_ID = tbxPROG_NAME.Tag.ToString().Trim();     //節目編碼
            m_strProgName = tbxPROG_NAME.Text.ToString().Trim();    //暫存本次新增的節目名稱，為了秀給使用者看結果用

            if (tbxEPISODE.Text.ToString().Trim() == "")            //集別
            {
                obj.FNEPISODE = 0;
                m_strEpisode = "";
            }
            else
            {
                obj.FNEPISODE = Convert.ToInt16(tbxEPISODE.Text.ToString().Trim());
                m_strEpisode = tbxEPISODE.Text.ToString().Trim();  //暫存本次新增的集別，為了秀給使用者看結果用
            }

            if (cbTITLE_ID.SelectedIndex != -1)                     //頭銜
                obj.FSTITLEID = ((ComboBoxItem)cbTITLE_ID.SelectedItem).Tag.ToString();

            obj.FSEMAIL = tbxEMAIL.Text.ToString().Trim();          //EMAIL
            obj.FSNAME = tbxNAME.Text.ToString().Trim();            //公司/姓名
            obj.FSMEMO = tbxMEMO.Text.ToString().Trim();            //備註

            //if (cbStaff.SelectedIndex != -1)
            //    obj.FSSTAFFID = ((ComboBoxItem)cbStaff.SelectedItem).Tag.ToString().Trim();    //工作人員代碼
            //else
            //    obj.FSSTAFFID = "";

            if (m_strStaffID.Trim() != "")                          //工作人員代碼
                obj.FSSTAFFID = m_strStaffID.Trim();
            else
                obj.FSSTAFFID = "";

            obj.FSUPDATED_BY = UserClass.userData.FSUSER_ID.ToString();//修改者                     
            m_Queryobj = obj;
            return obj;
        }

        private Boolean Check_PROGMENData()    //檢查畫面上的欄位是否都填妥
        {
            StringBuilder strMessage = new StringBuilder();
            string strStaffName = acbStaff.Text.ToString().Trim();
            Boolean bolStaff = false;            

            if (tbxPROG_NAME.Text.Trim() == "")
                strMessage.AppendLine("請選擇「節目」");

            if (cbTITLE_ID.SelectedIndex == -1)
                strMessage.AppendLine("請選擇「頭銜」欄位");

            if (strStaffName != "") //檢查輸入的工作人員姓名是不是有在工作人員檔裡
            {
                for (int i = 0; i < m_StaffList.Count; i++)
                {
                    if (m_StaffList[i].FSNAME.ToString().Trim().Equals(strStaffName))
                    {
                        bolStaff = true;
                        break;
                    }
                }

                if (bolStaff == false)
                    strMessage.AppendLine("工作人員-" + strStaffName + "不在工作人員檔裡，請重新輸入");
            }        

            if (strMessage.ToString().Trim() == "")
                return true;
            else
            {
                MessageBoxResult result = MessageBox.Show(strMessage.ToString(), "提示訊息", MessageBoxButton.OK);
                return false;
            }
        }

        //選擇工作人員時，要秀工作人員的詳細內容
        private void cbStaff_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string strStaffID = ((ComboBoxItem)cbStaff.SelectedItem).Tag.ToString().Trim();

            for (int i = 0; i < m_StaffList.Count; i++)
            {
                if (m_StaffList[i].FSSTAFFID.ToString().Trim().Equals(strStaffID))
                {
                    tbxENAME.Text = m_StaffList[i].FSENAME.ToString().Trim();       //外文姓名
                    tbxWEBSITE.Text = m_StaffList[i].FSWEBSITE.ToString().Trim();  //個人網站
                    tbxINTRO.Text = m_StaffList[i].FSINTRO.ToString().Trim();       //簡介
                    tbxEINTRO.Text = m_StaffList[i].FSEINTRO.ToString().Trim();     //外文簡介
                    break;
                }
            }
        }

        #endregion

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (Check_PROGMENData() == false)      //檢查畫面上的欄位是否都填妥
                return;

            //檢查是否有相同的一集、同頭銜且同名，若有就無法修改
            //client.fnQUERY_TBPROGMEN_CHECKAsync(FormToClass_PROGMEN()); 
            client.fnUPDATE_TBPROGRACEAsync(FormToClass_PROGMEN());//辨識的欄位都無法修改,所以不執行check直接Update 20140821 by Jarvis
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //開啟節目資料查詢畫面
        private void btnPROG_Click(object sender, RoutedEventArgs e)
        {
            PROGDATA_VIEW PROGDATA_VIEW_frm = new PROGDATA_VIEW();
            PROGDATA_VIEW_frm.Show();

            PROGDATA_VIEW_frm.Closed += (s, args) =>
            {
                if (PROGDATA_VIEW_frm.DialogResult == true)
                {
                    tbxPROG_NAME.Text = PROGDATA_VIEW_frm.strProgName_View;
                    tbxPROG_NAME.Tag = PROGDATA_VIEW_frm.strProgID_View;

                    tbxEPISODE.Text = "";
                    lblEPISODE_NAME.Content = "";
                }
            };
        }

        //開啟節目子集資料查詢畫面
        private void btnEPISODE_Click(object sender, RoutedEventArgs e)
        {
            if (tbxPROG_NAME.Text.ToString().Trim() != "" && tbxPROG_NAME.Tag.ToString().Trim() != "")
            {
                PRG.PRG200_07 PRG200_07_frm = new PRG.PRG200_07(tbxPROG_NAME.Tag.ToString().Trim(), tbxPROG_NAME.Text.ToString().Trim());
                PRG200_07_frm.Show();

                PRG200_07_frm.Closed += (s, args) =>
                {
                    if (PRG200_07_frm.DialogResult == true)
                    {
                        tbxEPISODE.Text = PRG200_07_frm.m_strEpisode_View;
                        lblEPISODE_NAME.Content = PRG200_07_frm.m_strProgDName_View;
                    }
                };
            }
            else
                MessageBox.Show("請先選擇節目！", "提示訊息", MessageBoxButton.OK);
        }

        //自動去比對資料庫，若有相同姓名的人員就顯示資料
        private void acbStaff_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0)
            {
                tbxENAME.Text = "";
                tbxWEBSITE.Text = "";
                tbxINTRO.Text = "";
                tbxEINTRO.Text = "";
                m_strStaffID = "";
                return;
            }

            string strStaffName = ((object[])(e.AddedItems))[0].ToString();

            for (int i = 0; i < m_StaffList.Count; i++)
            {
                if (m_StaffList[i].FSNAME.ToString().Trim().Equals(strStaffName))
                {
                    tbxENAME.Text = m_StaffList[i].FSENAME.ToString().Trim();       //外文姓名
                    tbxWEBSITE.Text = m_StaffList[i].FSWEBSITE.ToString().Trim();   //個人網站
                    tbxINTRO.Text = m_StaffList[i].FSINTRO.ToString().Trim();       //簡介
                    tbxEINTRO.Text = m_StaffList[i].FSEINTRO.ToString().Trim();     //外文簡介
                    m_strStaffID = m_StaffList[i].FSSTAFFID.ToString().Trim();
                    break;
                }
            }
        }


    }
}

