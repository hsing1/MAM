﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Xml.Linq;
using System.IO;
using System.Text;
using PTS_MAM3.WSSTAFF;
using System.Windows.Data;

namespace PTS_MAM3.ProgData
{
    public partial class PRG700 : Page
    {
        PTS_MAM3.WSSTAFF.WSSTAFFSoapClient clntSTAFF = new WSSTAFF.WSSTAFFSoapClient();

        Class_STAFF clSTAFF = new Class_STAFF();
        Class_STAFF qrySTAFF = new Class_STAFF();

        String m_Act = "";

        //Int32 m_intPageCount = -1; 

        public PRG700()
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面
        }
         
        void InitializeForm() //初始化本頁面
        {           
            //Dennis.Wen:刪除所選取的資料
            clntSTAFF.DELETE_TBSTAFF_PGMCompleted += new EventHandler<DELETE_TBSTAFF_PGMCompletedEventArgs>(clntSTAFF_DELETE_TBSTAFF_PGMCompleted);
            //Dennis.Wen:依照子視窗的篩選條件查詢工作人員
            clntSTAFF.QUERY_TBSTAFF_PGM_BYFILTERCompleted += new EventHandler<QUERY_TBSTAFF_PGM_BYFILTERCompletedEventArgs>(clntSTAFF_QUERY_TBSTAFF_PGM_BYFILTERCompleted);
            //取得全部工作人員資料
            clntSTAFF.GetTBSTAFF_ALLCompleted += new EventHandler<GetTBSTAFF_ALLCompletedEventArgs>(client_GetTBSTAFF_ALLCompleted);
            ////Dennis.Wen:移除此段
            //查詢工作人員資料
            //clntSTAFF.QUERY_TBSTAFFCompleted += new EventHandler<QUERY_TBSTAFFCompletedEventArgs>(client_QUERY_TBSTAFFCompleted);           
    
            clntSTAFF.GetTBSTAFF_ALLAsync();
            BusyMsg.IsBusy = true;  //關閉忙碌的Loading訊息
        }

        void clntSTAFF_QUERY_TBSTAFF_PGM_BYFILTERCompleted(object sender, QUERY_TBSTAFF_PGM_BYFILTERCompletedEventArgs e)
        {
            //如果取回的資料沒有錯誤訊息
            if (e.Error == null)
            {
                //如果取回的資料筆數不為0
                if (e.Result == null)
                {
                    if (m_Act == "QRY")
                    {
                        MessageBox.Show("查無相關資料", "提示訊息", MessageBoxButton.OK);
                        m_Act = "";
                    }
                }
                else if (e.Result.Count != 0)
                {
                    PagedCollectionView pcv = new PagedCollectionView(e.Result);
                    //指定DataGrid以及DataPager的內容
                    DGPROG_STAFFList.ItemsSource = pcv;
                    DataPager.Source = pcv;
                    DataPager.PageIndex = 0;

                    if (m_Act == "QRY")
                    {
                        MessageBox.Show("共" + e.Result.Count + "筆查詢結果", "提示訊息", MessageBoxButton.OK);
                        m_Act = "";
                    }
                }
            }
            else
            {
                MessageBox.Show("發生錯誤: " + e.Error.ToString(), "提示訊息", MessageBoxButton.OK);
            }
            BusyMsg.IsBusy = false;
        }


     
#region "事件處理"

        //實作-取得全部工作人員資料
        void client_GetTBSTAFF_ALLCompleted(object sender, GetTBSTAFF_ALLCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    //宣告PagedCollectionView,將撈取的資料放入
                    PagedCollectionView pcv = new PagedCollectionView(e.Result);
                    //指定DataGrid以及DataPager的內容
                    DGPROG_STAFFList.ItemsSource = pcv;
                    DataPager.Source = pcv;
                    DataPager.PageIndex = 0;
                }
            }
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息
        }

        //Dennis.Wen:依照子視窗的篩選條件查詢工作人員
        void clntSTAFF_DELETE_TBSTAFF_PGMCompleted(object sender, DELETE_TBSTAFF_PGMCompletedEventArgs e)
        {
            //如果取回的資料沒有錯誤訊息
            if (e.Error == null)
            {
                String msg = e.Result;
                if (msg == "")
                {
                    //如果新增成功
                    //DGPROG_STAFFList.SelectedItem

                    MessageBox.Show("刪除成功", "提示訊息", MessageBoxButton.OK);
                }
                else
                {
                    //如果新增失敗
                    MessageBox.Show("刪除時發生錯誤: " + msg.Substring(6), "提示訊息", MessageBoxButton.OK);
                }
            }
            else
            {
                //如果取回的資料有錯誤訊息
                MessageBox.Show("呼叫刪除函數時發生錯誤: " + e.Error.ToString(), "提示訊息", MessageBoxButton.OK);
            }

            clntSTAFF.GetTBSTAFF_ALLAsync();
        }    

        ////Dennis.Wen:移除此段
        //void client_QUERY_TBSTAFFCompleted(object sender, QUERY_TBSTAFFCompletedEventArgs e)
        //{
        //    BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

        //    if (e.Error == null)
        //    {
        //        if (e.Result != null && e.Result.Count > 0)  //若是有查詢結果，則顯示，不然就請使用者重新查詢
        //        {
        //            PagedCollectionView pcv = new PagedCollectionView(e.Result);
        //            DGPROG_STAFFList.ItemsSource = pcv;
        //            DataPager.Source = pcv;

        //            if (m_intPageCount != -1)        //設定修改後查詢完要回到哪一頁
        //                DataPager.PageIndex = m_intPageCount;
        //        }
        //        else
        //        {
        //            PagedCollectionView pcv = new PagedCollectionView(new List<Class_STAFF>());
        //            //指定DataGrid以及DataPager的內容
        //            DGPROG_STAFFList.ItemsSource = pcv;
        //            DataPager.Source = pcv;
        //        }
        //    }
        //}

#endregion     
  

#region "元件動作"

        //Dennis.Wen:新增
        private void btnPROGAdd_Click(object sender, RoutedEventArgs e)
        {
            if (checkPermission() == false)
            {
                MessageBox.Show("無工作人員資料維護權限，無法新增", "提示訊息", MessageBoxButton.OK);
                return;
            }
            
            //宣告出新增子視窗
            PRG.PRG700_01 frmADD = new PRG.PRG700_01("ADD");
            frmADD.Show();

            frmADD.Closed += (s, args) =>
            {
                if (frmADD.DialogResult == true)
                {
                    //查詢清單顯示新增的該筆資料
                    List<Class_STAFF> listSTAFF = new List<Class_STAFF>();

                    frmADD.clSTAFF.FSCRTUSER = "MAM系統使用者";    //不特別寫會是帳號ID        
                    listSTAFF.Add(frmADD.clSTAFF);

                    PagedCollectionView pcv = new PagedCollectionView(listSTAFF);

                    DGPROG_STAFFList.ItemsSource = pcv;
                    DataPager.Source = pcv;
                }
            };    
        }

        //Dennis.Wen:修改
        private void btnPROGModify_Click(object sender, RoutedEventArgs e)
        {
            if (checkPermission() == false)
            {
                MessageBox.Show("無工作人員資料維護權限，無法修改", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //判斷是否有選取DataGrid
            if (DGPROG_STAFFList.SelectedItem == null)
            {
                MessageBox.Show("您尚未選擇要修改的資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //宣告出修改子視窗
            PRG.PRG700_01 frmMOD = new PRG.PRG700_01("MOD", (WSSTAFF.Class_STAFF)DGPROG_STAFFList.SelectedItem);
            frmMOD.Show();

            frmMOD.Closed += (s, args) =>
            {
                if (frmMOD.DialogResult == true)    
                {
                    //子視窗修改成功後,重新update data
                    DGPROG_STAFFList.SelectedItem = (Object)frmMOD.clSTAFF;
                }
            }; 
        }

        //Dennis.Wen:檢視
        private void btnView_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGPROG_STAFFList.SelectedItem == null)
            {
                MessageBox.Show("您尚未選擇要檢視的資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //宣告出修改子視窗
            PRG.PRG700_01 frmVEW = new PRG.PRG700_01("VEW", (WSSTAFF.Class_STAFF)DGPROG_STAFFList.SelectedItem);
            frmVEW.Show();
        }

        //Dennis.Wen:查詢
        private void btnPROGQuery_Click(object sender, RoutedEventArgs e)
        {
            //宣告出查詢子視窗
            PRG.PRG700_01 frmQRY = new PRG.PRG700_01("QRY", qrySTAFF);
            frmQRY.Show();

            frmQRY.Closed += (s, args) =>
            {
                if (frmQRY.DialogResult == true)
                {
                    qrySTAFF = frmQRY.clSTAFF;
                    ////子視窗輸入後,重新整理清單
                    BusyMsg.IsBusy = true;
                    m_Act = "QRY";
                    clntSTAFF.QUERY_TBSTAFF_PGM_BYFILTERAsync(qrySTAFF);
                }
            };
        }

        //Dennis.Wen:刪除
        private void btnPROGDelete_Click(object sender, RoutedEventArgs e)
        {
            if (checkPermission() == false)
            {
                MessageBox.Show("無工作人員資料維護權限，無法刪除", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //判斷是否有選取DataGrid
            if (DGPROG_STAFFList.SelectedItem == null)
            {
                MessageBox.Show("您尚未選擇要刪除的資料", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else
            {
                if (MessageBox.Show("是否要刪除資料", "提示訊息", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                {
                    WSSTAFF.Class_STAFF cl = (WSSTAFF.Class_STAFF)DGPROG_STAFFList.SelectedItem;

                    //開始刪除檔案
                    BusyMsg.IsBusy = true;
                    clntSTAFF.DELETE_TBSTAFF_PGMAsync(cl.FSSTAFFID);                    
                }
            }
        }

        //檢查權限
        private Boolean checkPermission()
        {
            if (ModuleClass.getModulePermissionByName("02", "工作人員資料維護") == false)
                return false;
            else
                return true;
        }

    #endregion
         
    }
}
