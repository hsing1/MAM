﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;
using System.IO;
using System.Text;
using PTS_MAM3.WSPROGRACE;
using PTS_MAM3.PROG_M;
using PTS_MAM3.PRG;

namespace PTS_MAM3.ProgData
{
    public partial class PROGRACE_Edit : ChildWindow
    {
        WSPROGRACESoapClient client = new WSPROGRACESoapClient();   //產生新的代理類別
        List<string> m_ListRace = new List<string>();               //參展名稱集合 
        List<string> m_ListWin = new List<string>();                //得獎名稱集合
        Class_PROGRACE m_FormData = new Class_PROGRACE();
        string m_strAUTO = "";                                      //參展得獎主Key
        string m_strProgName = "";                                  //暫存節目名稱 
        string m_strEpisode = "";                                   //暫存集別
        public Class_PROGRACE m_Queryobj = new Class_PROGRACE();    //查詢修改的obj

        public PROGRACE_Edit(Class_PROGRACE FormData)
        {
            InitializeComponent();
            InitializeForm();          //初始化本頁面
            m_FormData = FormData;
            ClassToForm();
        }

        void InitializeForm() //初始化本頁面
        {
            //下載代碼檔//原本參展狀態是用代碼檔，後來改成只有三種：入選、入圍、得獎
            client.fnGetTBPROGRACE_CODECompleted += new EventHandler<fnGetTBPROGRACE_CODECompletedEventArgs>(client_fnGetTBPROGRACE_CODECompleted);
            client.fnGetTBPROGRACE_CODEAsync();
            
            //修改參展得獎資料
            client.fnUPDATE_TBPROGRACECompleted += new EventHandler<fnUPDATE_TBPROGRACECompletedEventArgs>(client_fnUPDATE_TBPROGRACECompleted);

            //參展名稱 AUTO COMPLETE
            client.fnGetRACECompleted += new EventHandler<fnGetRACECompletedEventArgs>(client_fnGetRACECompleted);
            client.fnGetRACEAsync();

            //得獎名稱 AUTO COMPLETE
            client.fnGetWINCompleted += new EventHandler<fnGetWINCompletedEventArgs>(client_fnGetWINCompleted);
            client.fnGetWINAsync();

            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }
           
        private void ClassToForm()  //將前一頁點選的節目資料繫集到畫面上
        {
            this.gdPROG_RACE.DataContext = m_FormData;
            tbxPROGRACE.Text = m_FormData.FSPROGRACE.ToString().Trim();         //這兩個欄位是bind到AutoComplete，因此要特別指定
            tbxPROGWIN.Text = m_FormData.FSPROGWIN.ToString().Trim();
           
            m_strAUTO = m_FormData.AUTO.ToString().Trim();                      //主Key
            tbxPROG_NAME.Tag = m_FormData.FSPROG_ID.ToString().Trim();          //節目編號
            compareStatusID(m_FormData.FSPROGSTATUS.ToString().Trim());         //參展狀態
        }

        #region 實作

        //實作-修改參展得獎資料
        void client_fnUPDATE_TBPROGRACECompleted(object sender, fnUPDATE_TBPROGRACECompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    MessageBox.Show("修改參展得獎資料「" + m_strProgName.ToString() + "-" + m_strEpisode + "集」成功！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("修改參展得獎資料「" + m_strProgName.ToString() + "-" + m_strEpisode + "集」失敗！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
        }       

        //實作-得獎名稱
        void client_fnGetWINCompleted(object sender, fnGetWINCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    tbxPROGWIN.ItemsSource = e.Result;
                }
            }
        }

        //實作-參展名稱
        void client_fnGetRACECompleted(object sender, fnGetRACECompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    tbxPROGRACE.ItemsSource = e.Result;
                }
            }
        }

        #endregion

        #region ComboBox載入代碼檔

        //實作-下載所有的代碼檔
        void client_fnGetTBPROGRACE_CODECompleted(object sender, fnGetTBPROGRACE_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            //case "TBZPROGSTATUS":      //參展狀態代碼
                            //    ComboBoxItem cbiPROGSTATUS = new ComboBoxItem();
                            //    cbiPROGSTATUS.Content = CodeData.NAME;
                            //    cbiPROGSTATUS.Tag = CodeData.ID;
                            //    this.cbPROGSTATUS.Items.Add(cbiPROGSTATUS);
                            //    break;
                            case "TBZPROGRACE_AREA":    //參展區域                              
                                    ComboBoxItem cbiPRGAREA = new ComboBoxItem();
                                    cbiPRGAREA.Content = CodeData.NAME;
                                    cbiPRGAREA.Tag = CodeData.ID;
                                    this.cbAREA.Items.Add(cbiPRGAREA);                               
                                break;
                            default:
                                break;
                        }
                    }
                }
            }

            compareArea(m_FormData.FSAREA.ToString().Trim());                   //參展區域，必須要再查詢出代碼之後          
        }

        //void compareStatusID(string strID)      //比對代碼檔_參展狀態
        //{
        //    for (int i = 0; i < cbPROGSTATUS.Items.Count; i++)
        //    {
        //        ComboBoxItem getcbi = (ComboBoxItem)cbPROGSTATUS.Items.ElementAt(i);
        //        if (getcbi.Tag.ToString().Trim().Equals(strID))
        //        {
        //            cbPROGSTATUS.SelectedIndex = i;
        //            break;
        //        }
        //    }
        //}

        void compareStatusID(string strNAME)      //比對代碼檔_參展狀態
        {
            for (int i = 0; i < cbPROGSTATUS.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGSTATUS.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strNAME))
                {
                    cbPROGSTATUS.SelectedIndex = i;
                    break;
                }
            }
        }

        private void compareArea(string strArea)   //參展區域
        {
            for (int i = 0; i < cbAREA.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbAREA.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strArea))
                {
                    cbAREA.SelectedIndex = i;
                    tbxAREA.Text = getcbi.Content.ToString().Trim();
                    tbxAREA.Tag = getcbi.Tag.ToString().Trim();
                    break;
                }
            }
        }

       #endregion

        #region 檢查畫面資料及搬值

        private Class_PROGRACE FormToClass_PROGRACE()
        {
            Class_PROGRACE obj = new Class_PROGRACE();

            obj.AUTO = m_strAUTO;
            obj.FSPROG_ID = tbxPROG_NAME.Tag.ToString().Trim();     //節目編碼
            m_strProgName = tbxPROG_NAME.Text.ToString().Trim();    //暫存本次新增的節目名稱，為了秀給使用者看結果用

            if (tbxEPISODE.Text.ToString().Trim() == "")            //集別
            {
                obj.FNEPISODE = 0;
                m_strEpisode = "";
            }
            else
            {
                obj.FNEPISODE = Convert.ToInt16(tbxEPISODE.Text.ToString().Trim());
                m_strEpisode = tbxEPISODE.Text.ToString().Trim();   //暫存本次新增的集別，為了秀給使用者看結果用
            }

            if (tbxAREA.Text.Trim() != "")                          //參展區域
                obj.FSAREA = tbxAREA.Tag.ToString();
            else
                obj.FSAREA = "";

            obj.FSPROGRACE = tbxPROGRACE.Text.ToString().Trim();    //參展名稱

            if (dpRACEDATE.Text.Trim() == "")                       //參展日期
                obj.FDRACEDATE = new DateTime(1900, 1, 1);
            else
                obj.FDRACEDATE = Convert.ToDateTime(dpRACEDATE.Text);

            if (cbPROGSTATUS.SelectedIndex != -1)                  //參展狀態代碼
                obj.FSPROGSTATUS = ((ComboBoxItem)cbPROGSTATUS.SelectedItem).Tag.ToString();
            else
                obj.FSPROGSTATUS = "";

            if (tbPERIOD.Text.ToString().Trim() == "")             //屆數
                obj.FNPERIOD = 0;
            else
                obj.FNPERIOD = Convert.ToInt16(tbPERIOD.Text.ToString().Trim());

            obj.FSPROGWIN = tbxPROGWIN.Text.ToString().Trim();      //得獎名稱
            obj.FSPROGNAME = tbxPROGNAME.Text.ToString().Trim();    //參展節目名稱
            obj.FSMEMO = tbxMEMO.Text.ToString().Trim();            //備註

            obj.FSUPDATED_BY = UserClass.userData.FSUSER_ID.ToString();    //修改者

            m_Queryobj = obj;

            return obj;
        }

        private Boolean Check_PROGRACEData()    //檢查畫面上的欄位是否都填妥
        {
            StringBuilder strMessage = new StringBuilder();
            short shortCheck;   //純粹為了判斷是否為數字型態之用

            if (tbxPROG_NAME.Text.Trim() == "")
                strMessage.AppendLine("請選擇「節目」");

            if (tbPERIOD.Text.Trim() != "")
            {
                if (short.TryParse(tbPERIOD.Text.Trim(), out shortCheck) == false)               
                    strMessage.AppendLine("請檢查「屆數」欄位必須為數值型態");  
            }

            if (cbPROGSTATUS.SelectedIndex == -1)
                strMessage.AppendLine("請選擇「參展狀態」欄位");

            if (tbxAREA.Text.Trim() == "")
                strMessage.AppendLine("請選擇「參展區域」欄位");

            if (tbxPROGRACE.Text.Trim() == "")
                strMessage.AppendLine("請填寫「影展名稱」欄位");

            if (tbxPROGRACE.Text.Trim().Length > 100)
                strMessage.AppendLine("「參展名稱」長度大於100，請檢查");

            if (tbxPROGWIN.Text.Trim().Length > 100)
                strMessage.AppendLine("「得獎名稱」長度大於100，請檢查");

            if (strMessage.ToString().Trim() == "")
                return true;
            else
            {
                MessageBoxResult result = MessageBox.Show(strMessage.ToString(), "提示訊息", MessageBoxButton.OK);
                return false;
            }
        }

        #endregion    

        //開啟節目資料查詢畫面
        private void btnPROG_Click(object sender, RoutedEventArgs e)
        {
            PROGDATA_VIEW PROGDATA_VIEW_frm = new PROGDATA_VIEW();
            PROGDATA_VIEW_frm.Show();

            PROGDATA_VIEW_frm.Closed += (s, args) =>
            {
                if (PROGDATA_VIEW_frm.DialogResult == true)
                {
                    tbxPROG_NAME.Text = PROGDATA_VIEW_frm.strProgName_View;
                    tbxPROG_NAME.Tag = PROGDATA_VIEW_frm.strProgID_View;
                }
            };
        }

        //開啟節目子集資料查詢畫面
        private void btnEPISODE_Click(object sender, RoutedEventArgs e)
        {
            if (tbxPROG_NAME.Text.ToString().Trim() != "" && tbxPROG_NAME.Tag.ToString().Trim() != "")
            {
                PRG.PRG200_07 PRG200_07_frm = new PRG.PRG200_07(tbxPROG_NAME.Tag.ToString().Trim(), tbxPROG_NAME.Text.ToString().Trim());
                PRG200_07_frm.Show();

                PRG200_07_frm.Closed += (s, args) =>
                {
                    if (PRG200_07_frm.DialogResult == true)
                    {
                        tbxEPISODE.Text = PRG200_07_frm.m_strEpisode_View;
                        lblEPISODE_NAME.Content = PRG200_07_frm.m_strProgDName_View;
                    }
                };
            }
            else
                MessageBox.Show("請先選擇節目！", "提示訊息", MessageBoxButton.OK);
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (Check_PROGRACEData() == false)      //檢查畫面上的欄位是否都填妥
                return;
                       
            //修改參展紀錄資料
            client.fnUPDATE_TBPROGRACEAsync(FormToClass_PROGRACE());
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //選取參展區域
        private void btnAREA_Click(object sender, RoutedEventArgs e)
        {
            PRG500_05 PRG500_05_frm = new PRG500_05();
            PRG500_05_frm.Show();

            PRG500_05_frm.Closed += (s, args) =>
            {
                if (PRG500_05_frm.DialogResult == true)
                {
                    tbxAREA.Text = PRG500_05_frm.strName_View;
                    tbxAREA.Tag = PRG500_05_frm.strID_View;
                }
            };
        }

    }
}

