﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.PROG_M;
using PTS_MAM3.WSPROG_M;
using PTS_MAM3.WSPROGLINK;
using System.Text;
using System.Xml.Linq;
using System.IO;
using PTS_MAM3.PRG;
using PTS_MAM3.WSPROG_D;

namespace PTS_MAM3.ProgData
{
    public partial class PROGMDATA_Add : ChildWindow
    {
        WSPROG_MSoapClient client = new WSPROG_MSoapClient();      //產生新的代理類別
        WSPROGLINKSoapClient clientLink = new WSPROGLINKSoapClient();
        WSPROG_DSoapClient clientD = new WSPROG_DSoapClient();

        Class_PROGM m_FormData = new Class_PROGM();
        List<Class_CODE> m_CodeDataBuyD = new List<Class_CODE>();  //代碼檔集合(外購類別細項) 
        string strProgName = "";                                   //暫存節目名稱
        string strExceptionMsg = "";                               //寫入資料庫錯誤訊息

        public PROGMDATA_Add(Class_PROGM FormData)
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面
            m_FormData = FormData;
            m_FormData.Origin_FDEXPIRE_DATE = m_FormData.FDEXPIRE_DATE;
            m_FormData.Origin_FSEXPIRE_DATE_ACTION = m_FormData.FSEXPIRE_DATE_ACTION;
            ClassToForm();
            checkPermission();      //檢查修改權限
        }

        void InitializeForm() //初始化本頁面
        {
            //下載代碼檔
            client.fnGetTBPROG_M_CODECompleted += new EventHandler<fnGetTBPROG_M_CODECompletedEventArgs>(client_fnGetTBPROG_M_CODECompleted);
            client.fnGetTBPROG_M_CODEAsync();

            //下載外購類別細項代碼檔
            client.fnGetTBPROG_M_PROGBUYD_CODECompleted += new EventHandler<fnGetTBPROG_M_PROGBUYD_CODECompletedEventArgs>(client_fnGetTBPROG_M_PROGBUYD_CODECompleted);

            //修改節目主檔資料
            client.fnUPDATETBPROGMCompleted += new EventHandler<fnUPDATETBPROGMCompletedEventArgs>(client_fnUPDATETBPROGMCompleted);

            //查詢衍生節目是否有衍生節目資料
            clientLink.QUERY_TBPROGLINK_BYPROGID_BYCHECK_LINKCompleted += new EventHandler<QUERY_TBPROGLINK_BYPROGID_BYCHECK_LINKCompletedEventArgs>(clientLink_QUERY_TBPROGLINK_BYPROGID_BYCHECK_LINKCompleted);

            //查詢衍生節目是否有扣播出節目資料
            clientLink.QUERY_TBPROGLINK_BYPROGID_BYCHECK_LINK_PLAYCompleted += new EventHandler<QUERY_TBPROGLINK_BYPROGID_BYCHECK_LINK_PLAYCompletedEventArgs>(clientLink_QUERY_TBPROGLINK_BYPROGID_BYCHECK_LINK_PLAYCompleted);

            //查詢是否有相同名稱的節目資料
            client.QUERY_TBPROGM_BYNAME_CHECKCompleted += new EventHandler<QUERY_TBPROGM_BYNAME_CHECKCompletedEventArgs>(client_QUERY_TBPROGM_BYNAME_CHECKCompleted);

            //整批修改九大選項
            clientD.UPDATE_TBPROGD_ALL_NINECompleted += new EventHandler<UPDATE_TBPROGD_ALL_NINECompletedEventArgs>(clientD_UPDATE_TBPROGD_ALL_NINECompleted);
        }

        //實作-修改節目主檔資料
        void client_fnUPDATETBPROGMCompleted(object sender, fnUPDATETBPROGMCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result.bolResult == true)
                {
                    Class_PROGD obj;
                    Boolean bolPROGSRCID = false;
                    Boolean bolPROGATTRID = false;
                    Boolean bolPROGAUDID = false;
                    Boolean bolPROGTYPEID = false;
                    Boolean bolPROGLANGID1 = false;
                    Boolean bolPROGLANGID2 = false;
                    Boolean bolPROGBUYID = false;
                    Boolean bolPROGBUYDIDS = false;
                    Boolean bolPROGSALEID = false;
                    Boolean bolPROGNATIONID = false;

                    string msg = checkSix(out obj, out  bolPROGSRCID, out  bolPROGATTRID,
            out  bolPROGAUDID,
            out  bolPROGTYPEID,
            out  bolPROGLANGID1,
            out  bolPROGLANGID2,
            out  bolPROGBUYID,
            out  bolPROGBUYDIDS,
            out  bolPROGSALEID,
            out  bolPROGNATIONID);

                    bool changeMain = false;
                    bool changeSub = false;
                    if (msg == "")
                    {
                        MessageBox.Show("修改節目資料「" + strProgName.ToString() + "」成功！", "提示訊息", MessageBoxButton.OK);
                        this.DialogResult = true;
                    }

                    else
                    {

                        WSPROG_DSoapClient clientD_Temp = new WSPROG_DSoapClient();
                        clientD_Temp.GetTBPROG_D_BYPROGID_ALLCompleted += (s, args) =>
                        {
                            if (changeMain)
                            {
                                var mainChannelKinds = args.Result.GroupBy(a => a.FSPROGLANGID1).Select(a => a.First()).ToList();

                                //   msg += "\n主聲道：" + m_FormData.FSPROGLANGID1_NAME + "->" + ((ComboBoxItem)cbPROGLANGID1.SelectedItem).Content.ToString().Trim();

                                if (mainChannelKinds.Count > 1)//主聲道多於一種情況
                                {
                                    Dictionary<string, string> kindsDic = new Dictionary<string, string>();

                                    foreach (Class_PROGD prog_d in mainChannelKinds)
                                    {

                                        var queryResult = args.Result.Where(a => a.FSPROGLANGID1 == prog_d.FSPROGLANGID1).ToList();

                                        string episodesStr = "子集:";
                                        foreach (Class_PROGD d in queryResult)
                                        {
                                            episodesStr += d.FNEPISODE.ToString() + ",";
                                        }

                                        if (episodesStr[episodesStr.Length - 1] == ',')
                                        {
                                            episodesStr = episodesStr.Remove(episodesStr.Length - 1);
                                        }
                                        kindsDic.Add(prog_d.FSPROGLANGID1_NAME, episodesStr);
                                    }
                                    int lineIndex = 0;
                                    string[] linesArr = msg.Split(new[] { '\r', '\n' });
                                    for (int i = 0; i < linesArr.Length; i++)
                                    {
                                        if (linesArr[i].Contains("主聲道"))
                                        {
                                            lineIndex = i;
                                            break;
                                        }
                                    }

                                    string changeLineStr = "\n";
                                    string spaceStr = "\t";

                                    string newMainChaneelMessage = changeLineStr + "主聲道：";
                                    foreach (KeyValuePair<string, string> keyParir in kindsDic)
                                    {
                                        newMainChaneelMessage += keyParir.Value + ":" + keyParir.Key + "->" + ((ComboBoxItem)cbPROGLANGID1.SelectedItem).Content.ToString().Trim() + changeLineStr + spaceStr;
                                    }
                                    //for (int i = 0; i < mainChannelKinds.Count; i++)
                                    //{

                                    //    newMainChaneelMessage += mainChannelKinds[i].FSPROGLANGID1_NAME + "->" + ((ComboBoxItem)cbPROGLANGID1.SelectedItem).Content.ToString().Trim() + changeLineStr + spaceStr;
                                    //}

                                    newMainChaneelMessage = newMainChaneelMessage.Remove(newMainChaneelMessage.LastIndexOf(spaceStr));

                                    linesArr[lineIndex] = newMainChaneelMessage;
                                    msg = "";
                                    foreach (string str in linesArr)
                                    {
                                        msg += str;
                                    }



                                }
                            }

                            if (changeSub)
                            {
                                var subChannelKinds = args.Result.GroupBy(a => a.FSPROGLANGID2).Select(a => a.First()).ToList();

                                if (subChannelKinds.Count > 1)//副聲道多於一種情況
                                {
                                    Dictionary<string, string> kindsDic = new Dictionary<string, string>();

                                    foreach (Class_PROGD prog_d in subChannelKinds)
                                    {

                                        var queryResult = args.Result.Where(a => a.FSPROGLANGID2 == prog_d.FSPROGLANGID2).ToList();

                                        string episodesStr = "子集:";
                                        foreach (Class_PROGD d in queryResult)
                                        {
                                            episodesStr += d.FNEPISODE.ToString() + ",";
                                        }

                                        if (episodesStr[episodesStr.Length - 1] == ',')
                                        {
                                            episodesStr = episodesStr.Remove(episodesStr.Length - 1);
                                        }
                                        kindsDic.Add(prog_d.FSPROGLANGID2_NAME, episodesStr);
                                    }
                                    int lineIndex = 0;
                                    string[] linesArr = msg.Split(new[] { '\r', '\n' });

                                    for (int i = 0; i < linesArr.Length; i++)
                                    {
                                        if (linesArr[i].Contains("副聲道"))
                                        {
                                            lineIndex = i;
                                            break;
                                        }
                                    }

                                    string changeLineStr = "\n";
                                    string spaceStr = "\t";

                                    string newMainChaneelMessage = changeLineStr + "副聲道：";
                                    foreach (KeyValuePair<string, string> keyParir in kindsDic)
                                    {
                                        newMainChaneelMessage += keyParir.Value + ":" + keyParir.Key + "->" + ((ComboBoxItem)cbPROGLANGID2.SelectedItem).Content.ToString().Trim() + changeLineStr + spaceStr;
                                    }

                                    newMainChaneelMessage = newMainChaneelMessage.Remove(newMainChaneelMessage.LastIndexOf(spaceStr));

                                    linesArr[lineIndex] = newMainChaneelMessage;
                                    msg = "";
                                    foreach (string str in linesArr)
                                    {
                                        msg += str + changeLineStr;
                                    }


                                }


                               

                            };
                            if (changeMain || changeSub)
                            {
                                 
                                MessageBoxResult resultMsg = MessageBox.Show(msg, "提示訊息", MessageBoxButton.OKCancel);
                                if (resultMsg == MessageBoxResult.OK)
                                    clientD.UPDATE_TBPROGD_ALL_NINEAsync(obj, bolPROGSRCID, bolPROGATTRID, bolPROGAUDID, bolPROGTYPEID, bolPROGLANGID1, bolPROGLANGID2, bolPROGBUYID, bolPROGBUYDIDS, bolPROGSALEID, bolPROGNATIONID);
                                else
                                {
                                    MessageBox.Show("修改節目資料「" + strProgName.ToString() + "」成功！", "提示訊息", MessageBoxButton.OK);
                                    this.DialogResult = true;
                                }
                            }
                        };

                        if (msg.Contains("主聲道"))
                        {
                            changeMain = true;
                        }

                        if (msg.Contains("副聲道"))
                        {
                            changeSub = true;
                        }

                        if (changeMain || changeSub)
                        {
                            clientD_Temp.GetTBPROG_D_BYPROGID_ALLAsync(m_FormData.FSPROG_ID);
                        }
                    }



                    //MessageBox.Show("修改節目資料「" + strProgName.ToString() + "」成功！", "提示訊息", MessageBoxButton.OK);
                    //this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("修改節目資料「" + strProgName.ToString() + "」失敗！\n失敗原因：" + e.Result.strErrorMsg, "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
            else
                MessageBox.Show("修改節目資料「" + strProgName.ToString() + "」失敗！", "提示訊息", MessageBoxButton.OK);
        }

        //實作-查詢衍生節目是否有衍生節目資料
        void clientLink_QUERY_TBPROGLINK_BYPROGID_BYCHECK_LINKCompleted(object sender, QUERY_TBPROGLINK_BYPROGID_BYCHECK_LINKCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result == false)
                {
                    //先檢查節目名稱有沒有重複
                    client.QUERY_TBPROGM_BYNAME_CHECKAsync(tbxPGMNAME.Text.ToString().Trim(), tbxtProgID.Text.ToString().Trim());
                }
                else
                    MessageBox.Show("「" + tbxPGMNAME.Text.Trim() + "」在衍生節目子集資料裡已設定為衍生子集，\n因此無法修改為非衍生！", "提示訊息", MessageBoxButton.OK);
            }
            else
                MessageBox.Show("查詢「衍生節目子集」資料失敗！", "提示訊息", MessageBoxButton.OK);
        }

        //實作-查詢衍生節目是否有扣播出節目資料
        void clientLink_QUERY_TBPROGLINK_BYPROGID_BYCHECK_LINK_PLAYCompleted(object sender, QUERY_TBPROGLINK_BYPROGID_BYCHECK_LINK_PLAYCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result == false)
                {
                    //先檢查節目名稱有沒有重複
                    client.QUERY_TBPROGM_BYNAME_CHECKAsync(tbxPGMNAME.Text.ToString().Trim(), tbxtProgID.Text.ToString().Trim());
                }
                else
                    MessageBox.Show("「" + tbxPGMNAME.Text.Trim() + "」在衍生節目子集資料裡已設定為扣播出節目，\n因此無法修改為衍生！", "提示訊息", MessageBoxButton.OK);
            }
            else
                MessageBox.Show("查詢「衍生節目子集」資料失敗！", "提示訊息", MessageBoxButton.OK);
        }

        //實作-查詢是否有相同名稱的節目資料
        void client_QUERY_TBPROGM_BYNAME_CHECKCompleted(object sender, QUERY_TBPROGM_BYNAME_CHECKCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result == false)
                {
                    //修改節目主檔
                    client.fnUPDATETBPROGMAsync(FormToClass_PROGM(), strExceptionMsg);

                    BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                }
                else
                    MessageBox.Show("已有重複的「節目名稱」資料，請檢查後重新輸入！", "提示訊息", MessageBoxButton.OK);
            }
            else
                MessageBox.Show("查詢「節目名稱」資料失敗！", "提示訊息", MessageBoxButton.OK);
        }

        //實作-整批修改九大選項
        void clientD_UPDATE_TBPROGD_ALL_NINECompleted(object sender, UPDATE_TBPROGD_ALL_NINECompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result.bolResult == true)
                {
                    MessageBox.Show("修改節目資料「" + strProgName.ToString() + "」成功！\n", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("修改節目資料「" + strProgName.ToString() + "」成功！\n修改子集資料失敗！\n失敗原因：" + e.Result.strErrorMsg, "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = true;
                }
            }
            else
            {
                MessageBox.Show("修改節目資料「" + strProgName.ToString() + "」成功！\n修改子集資料失敗！", "提示訊息", MessageBoxButton.OK);
                this.DialogResult = true;
            }
        }

        private void ClassToForm()  //將前一頁點選的節目資料繫集到畫面上
        {
            this.gdPROG_M.DataContext = m_FormData;
        }

        #region ComboBox載入代碼檔

        //實作-下載外購類別細項代碼檔
        void client_fnGetTBPROG_M_PROGBUYD_CODECompleted(object sender, fnGetTBPROG_M_PROGBUYD_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    //m_CodeDataBuyD = new List<Class_CODE>(e.Result);

                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        ComboBoxItem cbiPROGBUYD = new ComboBoxItem();
                        cbiPROGBUYD.Content = CodeData.NAME;
                        cbiPROGBUYD.Tag = CodeData.ID;
                        cbiPROGBUYD.DataContext = CodeData.IDD;
                        this.cbPROGBUYDID.Items.Add(cbiPROGBUYD);   //先都產生在隱藏起來的細項代碼combobox裡                   
                        m_CodeDataBuyD.Add(CodeData);
                    }
                }
                CompareCode();
            }
        }


        //實作-下載所有的代碼檔
        void client_fnGetTBPROG_M_CODECompleted(object sender, fnGetTBPROG_M_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    //外購類別代碼，空白要自己帶入
                    ComboBoxItem cbiPROGBUYNull = new ComboBoxItem();
                    cbiPROGBUYNull.Content = " ";
                    cbiPROGBUYNull.Tag = "";
                    this.cbPROGBUYID.Items.Add(cbiPROGBUYNull);

                    //因為行銷類別是節目管理系統的資料庫因此空白要自己帶入
                    ComboBoxItem cbiPROGSALENull = new ComboBoxItem();
                    cbiPROGSALENull.Content = " ";
                    cbiPROGSALENull.Tag = "";
                    this.cbPROGSALEID.Items.Add(cbiPROGSALENull);

                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            case "TBZCHANNEL":       //頻道別代碼
                                ComboBoxItem cbiCHANNEL = new ComboBoxItem();
                                cbiCHANNEL.Content = CodeData.NAME;
                                cbiCHANNEL.Tag = CodeData.ID;
                                this.cbCHANNEL_ID.Items.Add(cbiCHANNEL);

                                CheckBox cbCHANNEL = new CheckBox();
                                cbCHANNEL.Content = CodeData.NAME;
                                cbCHANNEL.Tag = CodeData.ID;
                                //combobox的點選及取消點選的事件處理常式
                                //cbCHANNEL.Checked += new RoutedEventHandler(cbCHANNEL_Checked);
                                //cbCHANNEL.Unchecked += new RoutedEventHandler(cbCHANNEL_Unchecked);
                                listCHANNEL.Items.Add(cbCHANNEL);
                                break;
                            case "TBZDEPT":         //部門別代碼
                                if (CodeData.NAME.Trim() != "")//要濾掉空白
                                {
                                    ComboBoxItem cbiDEPT = new ComboBoxItem();
                                    cbiDEPT.Content = CodeData.NAME;
                                    cbiDEPT.Tag = CodeData.ID;
                                    this.cbPRDDEPTID.Items.Add(cbiDEPT);
                                }
                                break;
                            case "TBZPROGOBJ":      //製作目的代碼
                                ComboBoxItem cbiPROGOBJ = new ComboBoxItem();
                                cbiPROGOBJ.Content = CodeData.NAME;
                                cbiPROGOBJ.Tag = CodeData.ID;

                                //判斷有無修改此製作目的的權限，若是無，就要把選項反白讓使用者無法選擇
                                if (ModuleClass.getModulePermissionByName("00", CodeData.NAME) == false)
                                    cbiPROGOBJ.IsEnabled = false;

                                this.cbPROGOBJID.Items.Add(cbiPROGOBJ);
                                break;
                            case "TBZPRDCENTER":      //製作單位代碼
                                if (CodeData.ID.Trim() != "00000")  //舊資料的代碼要略過
                                {
                                    ComboBoxItem cbiPRDCENTER = new ComboBoxItem();
                                    cbiPRDCENTER.Content = CodeData.NAME;
                                    cbiPRDCENTER.Tag = CodeData.ID;
                                    this.cbPRDCENID.Items.Add(cbiPRDCENTER);
                                }
                                break;
                            case "TBZPROGGRADE":      //節目分級代碼
                                ComboBoxItem cbiPROGGRADE = new ComboBoxItem();
                                cbiPROGGRADE.Content = CodeData.NAME;
                                cbiPROGGRADE.Tag = CodeData.ID;
                                this.cbPROGGRADEID.Items.Add(cbiPROGGRADE);
                                break;
                            case "TBZPROGSRC":      //節目來源代碼
                                ComboBoxItem cbiPROGSRC = new ComboBoxItem();
                                cbiPROGSRC.Content = CodeData.NAME;
                                cbiPROGSRC.Tag = CodeData.ID;
                                this.cbPROGSRCID.Items.Add(cbiPROGSRC);
                                break;
                            case "TBZPROGATTR":      //內容屬性代碼
                                ComboBoxItem cbiPROGATTR = new ComboBoxItem();
                                cbiPROGATTR.Content = CodeData.NAME;
                                cbiPROGATTR.Tag = CodeData.ID;
                                this.cbPROGATTRID.Items.Add(cbiPROGATTR);
                                break;
                            case "TBZPROGAUD":      //目標觀眾代碼
                                ComboBoxItem cbiPROGAUD = new ComboBoxItem();
                                cbiPROGAUD.Content = CodeData.NAME;
                                cbiPROGAUD.Tag = CodeData.ID;
                                this.cbPROGAUDID.Items.Add(cbiPROGAUD);
                                break;
                            case "TBZPROGTYPE":      //表現方式代碼
                                ComboBoxItem cbiPROGTYPE = new ComboBoxItem();
                                cbiPROGTYPE.Content = CodeData.NAME;
                                cbiPROGTYPE.Tag = CodeData.ID;
                                this.cbPROGTYPEID.Items.Add(cbiPROGTYPE);
                                break;
                            case "TBZPROGLANG":      //語言代碼
                                ComboBoxItem cbiPROGLANG1 = new ComboBoxItem();
                                cbiPROGLANG1.Content = CodeData.NAME;
                                cbiPROGLANG1.Tag = CodeData.ID;
                                this.cbPROGLANGID1.Items.Add(cbiPROGLANG1);  //主聲道

                                ComboBoxItem cbiPROGLANG2 = new ComboBoxItem();
                                cbiPROGLANG2.Content = CodeData.NAME;
                                cbiPROGLANG2.Tag = CodeData.ID;
                                this.cbPROGLANGID2.Items.Add(cbiPROGLANG2);  //副聲道
                                break;
                            case "TBZPROGBUY":       //外購類別代碼
                                ComboBoxItem cbiPROGBUY = new ComboBoxItem();
                                cbiPROGBUY.Content = CodeData.NAME;
                                cbiPROGBUY.Tag = CodeData.ID;
                                this.cbPROGBUYID.Items.Add(cbiPROGBUY);
                                break;
                            case "TBZPROGSALE":       //行銷類別代碼
                                ComboBoxItem cbiPROGSALE = new ComboBoxItem();
                                cbiPROGSALE.Content = CodeData.NAME;
                                cbiPROGSALE.Tag = CodeData.ID;
                                this.cbPROGSALEID.Items.Add(cbiPROGSALE);
                                break;
                            case "TBZPROGSPEC":       //節目規格代碼
                                CheckBox cbPROGSPEC = new CheckBox();
                                cbPROGSPEC.Content = CodeData.NAME;
                                cbPROGSPEC.Tag = CodeData.ID;
                                listPROGSPEC.Items.Add(cbPROGSPEC);
                                break;
                            case "TBZPROGNATION":       //來源國家    2012/11/22 kyle                            
                                ComboBoxItem cbNATION = new ComboBoxItem();
                                cbNATION.Content = CodeData.NAME;
                                cbNATION.Tag = CodeData.ID;
                                cbNATION_ID.Items.Add(cbNATION);
                                break;
                            default:
                                break;
                        }
                    }

                    client.fnGetTBPROG_M_PROGBUYD_CODEAsync();

                }
            }
        }

        void CompareCode()
        {
            //比對代碼檔 塞入選擇取回的代碼
            compareDERIVE(m_FormData.FCDERIVE.ToString().Trim());     //衍生
            compareOutBuy(m_FormData.FCOUT_BUY.ToString().Trim());    //外購
            compareDel(m_FormData.FSDEL.ToString().Trim());           //刪除註記
            compareCode_FSPRDDEPTID(m_FormData.FSPRDDEPTID.ToString().Trim());
            compareCode_FSPROGOBJID(m_FormData.FSPROGOBJID.ToString().Trim());
            compareCode_FSPROGGRADEID(m_FormData.FSPROGGRADEID.ToString().Trim());
            compareCode_FSPROGATTRID(m_FormData.FSPROGATTRID.ToString().Trim());
            compareCode_FSPROGSRCID(m_FormData.FSPROGSRCID.ToString().Trim());
            compareCode_FSPROGTYPEID(m_FormData.FSPROGTYPEID.ToString().Trim());
            compareCode_FSPROGAUDID(m_FormData.FSPROGAUDID.ToString().Trim());
            compareCode_FSPROGLANGID1(m_FormData.FSPROGLANGID1.ToString().Trim());
            compareCode_FSPROGLANGID2(m_FormData.FSPROGLANGID2.ToString().Trim());
            compareCode_FSPROGBUYID(m_FormData.FSPROGBUYID.ToString().Trim());
            compareCode_FSPROGBUYDID(m_FormData.FSPROGBUYDID.ToString().Trim());
            compareCode_FSCHANNEL_ID(m_FormData.FSCHANNEL_ID.ToString().Trim());
            compareCode_FSCHANNEL(CheckList(m_FormData.FSCHANNEL.ToString().Trim()));
            compareCode_FSPROGSALEID(m_FormData.FSPROGSALEID.ToString().Trim());
            compareCode_FSPRDCENID(m_FormData.FSPRDCENID.ToString().Trim());
            compareCode_FSPROGSPEC(CheckList(m_FormData.FSPROGSPEC.ToString()));
            cbNATION_ID.SelectedItem = cbNATION_ID.Items.Where(S => ((ComboBoxItem)S).Tag.ToString() == m_FormData.FSPROGNATIONID).FirstOrDefault();
            //if (m_FormData.FDEXPIRE_DATE.ToShortDateString() != "1900/1/1")
            if (m_FormData.FDEXPIRE_DATE > Convert.ToDateTime("1900/1/5"))
            {
                dpEXPIRE.Text = m_FormData.FDEXPIRE_DATE.ToShortDateString();
            }
            if (m_FormData.FSEXPIRE_DATE_ACTION == "Y")
                cbDELETE.IsChecked = true;
            else
                cbDELETE.IsChecked = false;
        }

        private string[] CheckList(string strChannel)
        {
            char[] delimiterChars = { ';' };
            string[] words = strChannel.Split(delimiterChars);
            return words;
        }

        private void compareDERIVE(string strDERIVE)   //是否為衍生
        {
            if (strDERIVE == "N")
                cbDERIVE.SelectedIndex = 0;
            else if (strDERIVE == "Y")
                cbDERIVE.SelectedIndex = 1;
        }

        private void compareOutBuy(string strOutBuy)   //是否為外購
        {
            if (strOutBuy == "N")
                cbOUT_BUY.SelectedIndex = 0;
            else if (strOutBuy == "Y")
                cbOUT_BUY.SelectedIndex = 1;
        }

        private void compareDel(string strDel)   //是否為刪除
        {
            if (strDel == "N")
                cbDel.SelectedIndex = 0;
            else if (strDel == "Y")
                cbDel.SelectedIndex = 1;
        }

        //選擇外購類別，要判斷顯示哪些外購類別細項
        private void cbBUY_ID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.cbPROGBUYDIDS.Items.Clear();     //使用前先清空
            string strBUYID = ""; //外購類別大項
            ComboBox cb = sender as ComboBox;
            ComboBoxItem cbm = (ComboBoxItem)cb.SelectedItem;
            strBUYID = cbm.Tag.ToString();

            for (int i = 0; i < cbPROGBUYDID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGBUYDID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strBUYID))
                {
                    ComboBoxItem cbiPROGBUYDS = new ComboBoxItem();
                    cbiPROGBUYDS.Content = getcbi.Content;
                    cbiPROGBUYDS.Tag = getcbi.DataContext;
                    this.cbPROGBUYDIDS.Items.Add(cbiPROGBUYDS);
                }
            }
        }

        #endregion

        #region 比對代碼檔

        //比對代碼檔_製作部門
        void compareCode_FSPRDDEPTID(string strID)
        {
            for (int i = 0; i < cbPRDDEPTID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPRDDEPTID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPRDDEPTID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_製作單位
        void compareCode_FSPRDCENID(string strID)
        {
            for (int i = 0; i < cbPRDCENID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPRDCENID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPRDCENID.SelectedIndex = i;
                    tbxPRDCEN.Text = getcbi.Content.ToString().Trim();
                    tbxPRDCEN.Tag = getcbi.Tag.ToString().Trim();
                    break;
                }
            }
        }

        //比對代碼檔_節目型態(內容屬性)
        void compareCode_FSPROGATTRID(string strID)
        {
            for (int i = 0; i < cbPROGATTRID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGATTRID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGATTRID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_製作目的
        void compareCode_FSPROGOBJID(string strID)
        {
            for (int i = 0; i < cbPROGOBJID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGOBJID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGOBJID.SelectedIndex = i;

                    //判斷有無修改此製作目的的權限
                    if (ModuleClass.getModulePermissionByName("00", getcbi.Content.ToString().Trim()) == false)
                        cbPROGOBJID.IsEnabled = false;

                    break;
                }
            }
        }

        //比對代碼檔_節目分級
        void compareCode_FSPROGGRADEID(string strID)
        {
            for (int i = 0; i < cbPROGGRADEID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGGRADEID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGGRADEID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_節目來源
        void compareCode_FSPROGSRCID(string strID)
        {
            for (int i = 0; i < cbPROGSRCID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGSRCID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGSRCID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_表現方式
        void compareCode_FSPROGTYPEID(string strID)
        {
            for (int i = 0; i < cbPROGTYPEID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGTYPEID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGTYPEID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_目標觀眾
        void compareCode_FSPROGAUDID(string strID)
        {
            for (int i = 0; i < cbPROGAUDID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGAUDID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGAUDID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_主聲道
        void compareCode_FSPROGLANGID1(string strID)
        {
            for (int i = 0; i < cbPROGLANGID1.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGLANGID1.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGLANGID1.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_副聲道
        void compareCode_FSPROGLANGID2(string strID)
        {
            for (int i = 0; i < cbPROGLANGID2.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGLANGID2.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGLANGID2.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_外購類別大類
        void compareCode_FSPROGBUYID(string strID)
        {
            for (int i = 0; i < cbPROGBUYID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGBUYID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGBUYID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_外購類別細類
        void compareCode_FSPROGBUYDID(string strID)
        {
            for (int i = 0; i < cbPROGBUYDIDS.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGBUYDIDS.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGBUYDIDS.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_可播映頻道
        void compareCode_FSCHANNEL(string[] ListTEST_Send)
        {
            for (int i = 0; i < ListTEST_Send.Length - 1; i++)
            {
                for (int j = 0; j < listCHANNEL.Items.Count; j++)
                {
                    CheckBox getcki = (CheckBox)listCHANNEL.Items.ElementAt(j);

                    if (getcki.Tag.ToString().Trim().Equals(ListTEST_Send[i]))
                    {
                        getcki.IsChecked = true;
                        break;
                    }
                }
            }
        }

        //比對代碼檔_節目規格
        void compareCode_FSPROGSPEC(string[] ListTEST_Send)
        {
            for (int i = 0; i < ListTEST_Send.Length - 1; i++)
            {
                for (int j = 0; j < listPROGSPEC.Items.Count; j++)
                {
                    CheckBox getcki = (CheckBox)listPROGSPEC.Items.ElementAt(j);

                    if (getcki.Tag.ToString().Trim().Equals(ListTEST_Send[i]))
                    {
                        getcki.IsChecked = true;
                        break;
                    }
                }
            }
        }

        //比對代碼檔_頻道別
        void compareCode_FSCHANNEL_ID(string strID)
        {
            for (int i = 0; i < cbCHANNEL_ID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbCHANNEL_ID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbCHANNEL_ID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_行銷類別
        void compareCode_FSPROGSALEID(string strID)
        {
            for (int i = 0; i < cbPROGSALEID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGSALEID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGSALEID.SelectedIndex = i;
                    break;
                }
            }
        }

        //選擇外購類別，要判斷顯示哪些外購類別細項
        private void cbPROGBUYID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            this.cbPROGBUYDIDS.Items.Clear();     //使用前先清空
            string strBUYID = ""; //外購類別大項
            ComboBox cb = sender as ComboBox;
            ComboBoxItem cbm = (ComboBoxItem)cb.SelectedItem;
            strBUYID = cbm.Tag.ToString();

            for (int i = 0; i < cbPROGBUYDID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGBUYDID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strBUYID))
                {
                    ComboBoxItem cbiPROGBUYDS = new ComboBoxItem();
                    cbiPROGBUYDS.Content = getcbi.Content;
                    cbiPROGBUYDS.Tag = getcbi.DataContext;
                    this.cbPROGBUYDIDS.Items.Add(cbiPROGBUYDS);
                }
            }
        }

        #endregion

        #region 檢查畫面資料及搬值

        private Class_PROGM FormToClass_PROGM()
        {
            Class_PROGM obj = new Class_PROGM();

            obj.FSPROG_ID = tbxtProgID.Text.ToString();              //節目編號

            if (cbCHANNEL_ID.SelectedIndex != -1)                    //頻道別
                obj.FSCHANNEL_ID = ((ComboBoxItem)cbCHANNEL_ID.SelectedItem).Tag.ToString();
            else
                obj.FSCHANNEL_ID = "";

            if (cbPROGOBJID.SelectedIndex != -1)                     //製作目的
                obj.FSPROGOBJID = ((ComboBoxItem)cbPROGOBJID.SelectedItem).Tag.ToString();
            else
                obj.FSPROGOBJID = "";

            if (cbPRDDEPTID.SelectedIndex != -1)                    //製作部門
                obj.FSPRDDEPTID = ((ComboBoxItem)cbPRDDEPTID.SelectedItem).Tag.ToString();
            else
                obj.FSPRDDEPTID = "";

            if (cbDERIVE.SelectedIndex != -1)                       //衍生
                obj.FCDERIVE = ((ComboBoxItem)cbDERIVE.SelectedItem).Tag.ToString();
            else
                obj.FCDERIVE = "";

            obj.FSCHANNEL = Check_ChannelList();                   //預訂播出頻道

            obj.FSPGMNAME = tbxPGMNAME.Text.ToString().Trim();     //節目名稱
            obj.FSWEBNAME = tbxWEBNAME.Text.ToString().Trim();     //播出名稱 
            obj.FSPGMENAME = tbxPGMENAME.Text.ToString().Trim();   //外文名稱

            //if (cbPRDCENID.SelectedIndex != -1)                    //製作單位
            //    obj.FSPRDCENID = ((ComboBoxItem)cbPRDCENID.SelectedItem).Tag.ToString();
            //else
            //    obj.FSPRDCENID = "";

            if (tbxPRDCEN.Text.Trim() != "")                       //製作單位
                obj.FSPRDCENID = tbxPRDCEN.Tag.ToString();
            else
                obj.FSPRDCENID = "";

            if (tbxTOTEPISODE.Text.ToString().Trim() != "")
                obj.FNTOTEPISODE = Convert.ToInt16(tbxTOTEPISODE.Text.ToString().Trim()); //總集數
            else
                obj.FNTOTEPISODE = 0;

            if (tbxLENGTH.Text.ToString().Trim() != "")
                obj.FNLENGTH = Convert.ToInt16(tbxLENGTH.Text.ToString().Trim());         //每集時長
            else
                obj.FNLENGTH = 0;

            obj.FSPRDYYMM = tbxPRDYYMM.Text.ToString().Trim();    //製作年月

            if (cbPROGGRADEID.SelectedIndex != -1)                //節目分級
                obj.FSPROGGRADEID = ((ComboBoxItem)cbPROGGRADEID.SelectedItem).Tag.ToString();
            else
                obj.FSPROGGRADEID = "";

            if (dpShowDate.Text.Trim() == "")
                obj.FDSHOWDATE = Convert.ToDateTime("1900/1/1");
            else
                obj.FDSHOWDATE = Convert.ToDateTime(dpShowDate.Text);//預計上檔日期

            obj.FSCONTENT = tbxCONTENT.Text.ToString().Trim();   //節目內容簡述
            obj.FSMEMO = tbxMEMO.Text.ToString().Trim();         //備註      

            if (cbPROGSRCID.SelectedIndex != -1)                 //節目來源
                obj.FSPROGSRCID = ((ComboBoxItem)cbPROGSRCID.SelectedItem).Tag.ToString();
            else
                obj.FSPROGSRCID = "";

            if (cbPROGATTRID.SelectedIndex != -1)                //節目型態
                obj.FSPROGATTRID = ((ComboBoxItem)cbPROGATTRID.SelectedItem).Tag.ToString();
            else
                obj.FSPROGATTRID = "";

            if (cbPROGAUDID.SelectedIndex != -1)                 //目標觀眾
                obj.FSPROGAUDID = ((ComboBoxItem)cbPROGAUDID.SelectedItem).Tag.ToString();
            else
                obj.FSPROGAUDID = "";

            if (cbPROGTYPEID.SelectedIndex != -1)                //表現方式
                obj.FSPROGTYPEID = ((ComboBoxItem)cbPROGTYPEID.SelectedItem).Tag.ToString();
            else
                obj.FSPROGTYPEID = "";

            if (cbPROGLANGID1.SelectedIndex != -1)               //主聲道
                obj.FSPROGLANGID1 = ((ComboBoxItem)cbPROGLANGID1.SelectedItem).Tag.ToString();
            else
                obj.FSPROGLANGID1 = "";

            if (cbPROGLANGID2.SelectedIndex != -1)               //副聲道
                obj.FSPROGLANGID2 = ((ComboBoxItem)cbPROGLANGID2.SelectedItem).Tag.ToString();
            else
                obj.FSPROGLANGID2 = "";

            if (cbPROGBUYID.SelectedIndex != -1)                 //外購類別大類
                obj.FSPROGBUYID = ((ComboBoxItem)cbPROGBUYID.SelectedItem).Tag.ToString();
            else
                obj.FSPROGBUYID = "";

            if (cbPROGBUYDIDS.SelectedIndex != -1)               //外購類別細類
                obj.FSPROGBUYDID = ((ComboBoxItem)cbPROGBUYDIDS.SelectedItem).Tag.ToString();
            else
                obj.FSPROGBUYDID = "";

            if (cbPROGSALEID.SelectedIndex != -1)                //行銷類別
                obj.FSPROGSALEID = ((ComboBoxItem)cbPROGSALEID.SelectedItem).Tag.ToString();
            else
                obj.FSPROGSALEID = "";

            obj.FSEWEBPAGE = tbxEWEBPAGE.Text.ToString().Trim(); //英文網頁資料

            if (cbOUT_BUY.SelectedIndex != -1)                   //外購
                obj.FCOUT_BUY = ((ComboBoxItem)cbOUT_BUY.SelectedItem).Tag.ToString();
            else
                obj.FCOUT_BUY = "";

            if (cbDel.SelectedIndex == 1)
            {
                obj.FSDEL = "Y";         //刪除註記
                obj.FSDELUSER = UserClass.userData.FSUSER_ID.ToString();    //刪除者
            }
            else
            {
                obj.FSDEL = "N";        //刪除註記
                obj.FSDELUSER = "";     //刪除者
            }

            obj.FSPROGSPEC = Check_PROGSPECList();                          //節目規格   

            obj.FSUPDUSER = UserClass.userData.FSUSER_ID.ToString();        //修改者

            if (cbDELETE.IsChecked == false)                          //到期日期後是否刪除
                obj.FSEXPIRE_DATE_ACTION = "N";
            else
                obj.FSEXPIRE_DATE_ACTION = "Y";


            if (dpEXPIRE.Text == "")
                obj.FDEXPIRE_DATE = Convert.ToDateTime("1900/1/1");
            else
                obj.FDEXPIRE_DATE = Convert.ToDateTime(dpEXPIRE.Text).AddHours(23).AddMinutes(59);  //到期日期

            obj.FSPROGNATIONID = ((ComboBoxItem)cbNATION_ID.SelectedItem).Tag.ToString();       //國家代碼

            obj.Origin_FDEXPIRE_DATE = m_FormData.Origin_FDEXPIRE_DATE;
            obj.Origin_FSEXPIRE_DATE_ACTION = m_FormData.Origin_FSEXPIRE_DATE_ACTION;
            return obj;
        }

        private string Check_ChannelList()    //檢查可播映頻道有哪些
        {
            string strReturn = "";
            List<string> ListCHANNEL = new List<string>();  //TEST
            for (int i = 0; i < listCHANNEL.Items.Count; i++)
            {
                CheckBox getckb = (CheckBox)listCHANNEL.Items.ElementAt(i);
                if (getckb.IsChecked == true)
                {
                    strReturn += getckb.Tag.ToString().Trim() + ";";
                }
            }
            return strReturn;
        }

        private string Check_PROGSPECList()    //檢查節目規格有哪些
        {
            string strReturn = "";
            for (int i = 0; i < listPROGSPEC.Items.Count; i++)
            {
                CheckBox getckb = (CheckBox)listPROGSPEC.Items.ElementAt(i);
                if (getckb.IsChecked == true)
                {
                    strReturn += getckb.Tag.ToString().Trim() + ";";
                }
            }
            return strReturn;
        }

        private Boolean Check_PROGMData()    //檢查畫面上的欄位是否都填妥
        {
            StringBuilder strMessage = new StringBuilder();
            double intCheck;   //純粹為了判斷是否為數字型態之用  
            short shortCheck;           //純粹為了判斷是否為數字型態之用(smallint)

            if (tbxPGMNAME.Text.Trim() == "")
                strMessage.AppendLine("請輸入「節目名稱」欄位");
            else
                strProgName = tbxPGMNAME.Text.ToString().Trim();  //暫存本次新增的節目名稱，為了秀給使用者看結果用

            if (tbxWEBNAME.Text.Trim() == "")
                strMessage.AppendLine("請輸入「網路播出名稱」欄位");

            if (cbCHANNEL_ID.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「頻道」欄位");

            if (tbxLENGTH.Text.Trim() != "")
            {
                if (short.TryParse(tbxLENGTH.Text.Trim(), out shortCheck) == false)
                    strMessage.AppendLine("請檢查「每集時長」欄位必須為數值型態");
            }

            if (tbxTOTEPISODE.Text.Trim() != "")
            {
                if (short.TryParse(tbxTOTEPISODE.Text.Trim(), out shortCheck) == false)
                    strMessage.AppendLine("請檢查「總集數」欄位必須為數值型態");
                else if (tbxTOTEPISODE.Text.Trim() == "0")
                    strMessage.AppendLine("請檢查「總集數」欄位不能為零");
            }
            else
                strMessage.AppendLine("請輸入「總集數」欄位");

            if (tbxPRDYYMM.Text.Trim() != "")
            {
                if (double.TryParse(tbxPRDYYMM.Text.Trim(), out intCheck) == false)
                    strMessage.AppendLine("請檢查「製作年月」欄位必須為數值型態");
                else
                {
                    if (tbxPRDYYMM.Text.Trim().Length != 6)
                        strMessage.AppendLine("請檢查「製作年月」欄位必須為四碼西元年+兩碼月份\n" + "範例：201101(2011年1月)");
                }
            }

            if (Check_ChannelList() == "")
                strMessage.AppendLine("請勾選「可播映頻道」欄位");

            if (cbPROGOBJID.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「製作目的」欄位");

            if (cbDERIVE.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「衍生節目」欄位");

            //if (cbPRDCENID.SelectedIndex == -1)
            //    strMessage.AppendLine("請輸入「製作單位」欄位");

            if (tbxPRDCEN.Text.Trim() == "")
                strMessage.AppendLine("請輸入「製作單位」欄位");

            if (cbPROGGRADEID.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「節目分級」欄位");

            if (cbPROGSRCID.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「節目來源代碼」欄位");

            if (cbPROGATTRID.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「內容屬性代碼」欄位");

            if (cbPROGAUDID.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「目標觀眾代碼」欄位");

            if (cbPROGTYPEID.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「表現方式代碼」欄位");

            if (cbPROGLANGID1.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「主聲道代碼」欄位");

            if (cbPROGLANGID2.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「副聲道代碼」欄位");

            if (cbPROGBUYID.SelectedIndex != -1 && ((ComboBoxItem)(cbPROGBUYID.SelectedItem)).Content.ToString().Trim() != "" && cbPROGBUYDIDS.SelectedIndex == -1) //因為設定外鍵，因此選擇了大項就要設細項
                strMessage.AppendLine("請輸入「外購類別細項」欄位");

            if (cbNATION_ID.SelectedIndex == -1)
            {
                strMessage.AppendLine("請補入「來源國家」欄位");
            }
            //if (Check_PROGSPECList().Trim() == "")    //節目規格是在子集上的，因此節目主檔修改為不是必填欄位
            //    strMessage.AppendLine("請勾選「節目規格」欄位");

            if (strMessage.ToString().Trim() == "")
            {
                if (cbDel.SelectedIndex == 1)
                {
                    MessageBoxResult resultMsg = MessageBox.Show("確定要刪除「" + strProgName + "」節目主檔資料嗎?", "提示訊息", MessageBoxButton.OKCancel);
                    if (resultMsg == MessageBoxResult.Cancel)
                        return false;
                    else
                        return true;
                }
                else
                    return true;
            }
            else
            {
                MessageBoxResult result = MessageBox.Show(strMessage.ToString(), "提示訊息", MessageBoxButton.OK);
                return false;
            }
        }

        #endregion

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {

            if (Check_PROGMData() == false)      //檢查畫面上的欄位是否都填妥
                return;

            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
            checkProgLink();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void checkProgLink()    //修改節目「衍生節目與否」要檢查
        {
            string strProgID = tbxtProgID.Text.ToString();     //節目編號

            if (cbDERIVE.SelectedIndex == 1)
            {
                //N → Y 檢查該節目是否有衍生節目子集資料，並且扣播出子集若是有資料就要彈出訊息通知
                clientLink.QUERY_TBPROGLINK_BYPROGID_BYCHECK_LINK_PLAYAsync(strProgID);
            }
            else
            {
                //Y → N 檢查該節目是否有衍生節目子集資料，並且衍生節目子集若是有資料就要彈出訊息通知
                clientLink.QUERY_TBPROGLINK_BYPROGID_BYCHECK_LINKAsync(strProgID);
            }
        }

        //選取製作單位
        private void btnPRDCEN_Click(object sender, RoutedEventArgs e)
        {
            PRG100_11 PRG100_11_frm = new PRG100_11();
            PRG100_11_frm.Show();

            PRG100_11_frm.Closed += (s, args) =>
            {
                if (PRG100_11_frm.DialogResult == true)
                {
                    tbxPRDCEN.Text = PRG100_11_frm.strPRDCENName_View;
                    tbxPRDCEN.Tag = PRG100_11_frm.strPRDCENID_View;
                }
            };
        }

        //查詢九大選項是否有改變
        private string checkSix(out Class_PROGD obj, out bool bolPROGSRCID, out bool bolPROGATTRID,
            out bool bolPROGAUDID,
            out bool bolPROGTYPEID,
            out bool bolPROGLANGID1,
            out bool bolPROGLANGID2,
            out bool bolPROGBUYID,
            out bool bolPROGBUYDIDS,
            out bool bolPROGSALEID,
            out bool bolPROGNATIONID)
        {
            string msg = "是否要一併修改所有子集的資料?";
            string strPROGSRCID = ((ComboBoxItem)cbPROGSRCID.SelectedItem).Tag.ToString().Trim();       //節目來源
            string strPROGATTRID = ((ComboBoxItem)cbPROGATTRID.SelectedItem).Tag.ToString().Trim();     //節目型態
            string strPROGAUDID = ((ComboBoxItem)cbPROGAUDID.SelectedItem).Tag.ToString().Trim();       //目標觀眾
            string strPROGTYPEID = ((ComboBoxItem)cbPROGTYPEID.SelectedItem).Tag.ToString().Trim();     //表現方式
            string strPROGLANGID1 = ((ComboBoxItem)cbPROGLANGID1.SelectedItem).Tag.ToString().Trim();   //主聲道
            string strPROGLANGID2 = ((ComboBoxItem)cbPROGLANGID2.SelectedItem).Tag.ToString().Trim();   //副聲道

            string strPROGBUYID = "";      //外購大項
            string strPROGBUYDIDS = "";  //外購細項
            string strPROGSALEID = ""; //行銷
            string strPROGNATIONID = "";//來源國家

            if (cbPROGBUYID.SelectedItem == null)
                strPROGBUYID = "";
            else
                strPROGBUYID = ((ComboBoxItem)cbPROGBUYID.SelectedItem).Tag.ToString().Trim();

            if (cbPROGBUYDIDS.SelectedItem == null)
                strPROGBUYDIDS = "";
            else
                strPROGBUYDIDS = ((ComboBoxItem)cbPROGBUYDIDS.SelectedItem).Tag.ToString().Trim();

            if (cbPROGSALEID.SelectedItem == null)
                strPROGSALEID = "";
            else
                strPROGSALEID = ((ComboBoxItem)cbPROGSALEID.SelectedItem).Tag.ToString().Trim();

            if (cbNATION_ID.SelectedItem != null)
            {
                strPROGNATIONID = ((ComboBoxItem)cbNATION_ID.SelectedItem).Tag.ToString().Trim();
            }

            bolPROGSRCID = false;
            bolPROGATTRID = false;
            bolPROGAUDID = false;
            bolPROGTYPEID = false;
            bolPROGLANGID1 = false;
            bolPROGLANGID2 = false;
            bolPROGBUYID = false;
            bolPROGBUYDIDS = false;
            bolPROGSALEID = false;
            bolPROGNATIONID = false;

            //檢查有沒有權限，若是沒有權限也就沒有檢查改變及更新的必要
            if (ModuleClass.getModulePermissionByName("01", "節目主檔-六大屬性維護") == false &&
                ModuleClass.getModulePermissionByName("01", "節目主檔-外購類別維護") == false &&
                ModuleClass.getModulePermissionByName("01", "節目主檔-行銷類別維護") == false)
            {
                MessageBox.Show("修改節目資料「" + strProgName.ToString() + "」成功！", "提示訊息", MessageBoxButton.OK);
                this.DialogResult = true;
            }

            obj = new Class_PROGD();
            obj.FSPROG_ID = tbxtProgID.Text.ToString();     //節目編號
            obj.FNEPISODE = 0;
            obj.FSPGDNAME = "";
            obj.FSPROGSRCID = "";
            obj.FSPROGATTRID = "";
            obj.FSPROGAUDID = "";
            obj.FSPROGTYPEID = "";
            obj.FSPROGLANGID1 = "";
            obj.FSPROGLANGID2 = "";
            obj.FSPROGBUYID = "";
            obj.FSPROGBUYDID = "";
            obj.FSPROGSALEID = "";
            obj.FSCRTUSER = UserClass.userData.FSUSER_ID.ToString();
            obj.FSUPDUSER = UserClass.userData.FSUSER_ID.ToString();
            obj.FSPROGNATIONID = "";
            obj.FSPROGSALEID = "";


            //檢查是否也有子集六大屬性的修改權限
            if (ModuleClass.getModulePermissionByName("01", "節目主檔-六大屬性維護") == true && ModuleClass.getModulePermissionByName("02", "節目子集-四大屬性維護") == true)
            {
                if (m_FormData.FSPROGSRCID.Trim() != strPROGSRCID)
                {
                    obj.FSPROGSRCID = strPROGSRCID;
                    bolPROGSRCID = true;
                    msg += "\n節目來源：" + m_FormData.FSPROGSRCID_NAME + "->" + ((ComboBoxItem)cbPROGSRCID.SelectedItem).Content.ToString().Trim();
                }
                else
                    obj.FSPROGSRCID = "";

                if (m_FormData.FSPROGATTRID.Trim() != strPROGATTRID)
                {
                    obj.FSPROGATTRID = strPROGATTRID;
                    bolPROGATTRID = true;
                    msg += "\n節目型態：" + m_FormData.FSPROGATTRID_NAME + "->" + ((ComboBoxItem)cbPROGATTRID.SelectedItem).Content.ToString().Trim();
                }
                else
                    obj.FSPROGATTRID = "";

                if (m_FormData.FSPROGAUDID.Trim() != strPROGAUDID)
                {
                    obj.FSPROGAUDID = strPROGAUDID;
                    bolPROGAUDID = true;
                    msg += "\n目標觀眾：" + m_FormData.FSPROGAUDID_NAME + "->" + ((ComboBoxItem)cbPROGAUDID.SelectedItem).Content.ToString().Trim();
                }
                else
                    obj.FSPROGAUDID = "";

                if (m_FormData.FSPROGTYPEID.Trim() != strPROGTYPEID)
                {
                    obj.FSPROGTYPEID = strPROGTYPEID;
                    bolPROGTYPEID = true;
                    msg += "\n表現方式：" + m_FormData.FSPROGTYPEID_NAME + "->" + ((ComboBoxItem)cbPROGTYPEID.SelectedItem).Content.ToString().Trim();
                }
                else
                    obj.FSPROGTYPEID = "";

                //原本也要更新主、副聲道，但是改成不更新
                //2014-09-19又改回要更新 by Jarvis
                if (m_FormData.FSPROGLANGID1.Trim() != strPROGLANGID1)
                {
                    obj.FSPROGLANGID1 = strPROGLANGID1;
                    bolPROGLANGID1 = true;
                    msg += "\n主聲道：" + m_FormData.FSPROGLANGID1_NAME + "->" + ((ComboBoxItem)cbPROGLANGID1.SelectedItem).Content.ToString().Trim();
                }
                else
                    obj.FSPROGLANGID1 = "";

                if (m_FormData.FSPROGLANGID2.Trim() != strPROGLANGID2)
                {
                    obj.FSPROGLANGID2 = strPROGLANGID2;
                    bolPROGLANGID2 = true;
                    msg += "\n副聲道：" + m_FormData.FSPROGLANGID2_NAME + "->" + ((ComboBoxItem)cbPROGLANGID2.SelectedItem).Content.ToString().Trim();
                }
                else
                    obj.FSPROGLANGID2 = "";


                //if (m_FormData.FSPROGBUYID.Trim() != strPROGBUYID) 
                //{
                //    obj.FSPROGBUYDID = strPROGBUYID;
                //    bolPROGBUYID = true;
                //    msg+="\n外購類別(大項):"+m_FormData.FSPROGBUYID_NAME+"->"+ ((ComboBoxItem)cbPROGBUYID.SelectedItem).Content.ToString().Trim();
                //}

                //if (m_FormData.FSPROGBUYDID.Trim() != strPROGBUYDIDS)
                //{
                //    obj.FSPROGBUYDID = strPROGBUYDIDS;
                //    bolPROGBUYDIDS = true;
                //    msg += "\n外購類別(細項):" + m_FormData.FSPROGBUYDID_NAME + "->" + ((ComboBoxItem)cbPROGBUYDIDS.SelectedItem).Content.ToString().Trim();
                //}

                //if (m_FormData.FSPROGSALEID.Trim() != strPROGSALEID)
                //{
                //    obj.FSPROGSALEID = strPROGSALEID;
                //    bolPROGSALEID = true;
                //    msg += "\n行銷類別:" + m_FormData.FSPROGSALEID_NAME + "->" + ((ComboBoxItem)cbPROGSALEID.SelectedItem).Content.ToString().Trim();
                //}

                if (m_FormData.FSPROGNATIONID.Trim() != strPROGNATIONID)
                {
                    obj.FSPROGNATIONID = strPROGNATIONID;
                    bolPROGNATIONID = true;
                    msg += "\n來源國家:" + m_FormData.FSPROGNATIONID_NAME + "->" + ((ComboBoxItem)cbNATION_ID.SelectedItem).Content.ToString().Trim();
                }
                /*
                             string strPROGBUYID = ((ComboBoxItem)cbPROGBUYID.SelectedItem).Tag.ToString().Trim();      //外購大項
            string strPROGBUYDIDS = ((ComboBoxItem)cbPROGBUYDIDS.SelectedItem).Tag.ToString().Trim();  //外購細項
            string strPROGSALEID = ((ComboBoxItem)cbPROGSALEID.SelectedItem).Tag.ToString().Trim(); ; //行銷
            string strPROGNATIONID = ((ComboBoxItem)cbNATION_ID.SelectedItem).Tag.ToString().Trim();   //來源國家

                 */
            }

            //外購改成不要更新到子集
            //2014-09-19 註解by Jarvis
            //-------------------------------
            //obj.FSPROGBUYID = "";
            //obj.FSPROGBUYDID = "";
            //---------------------------------

            //檢查是否也有子集外購的修改權限
            if (ModuleClass.getModulePermissionByName("01", "節目主檔-外購類別維護") == true && ModuleClass.getModulePermissionByName("02", "節目子集-外購類別維護") == true)
            {
                if (m_FormData.FSPROGBUYID.Trim() != strPROGBUYID)
                {
                    obj.FSPROGBUYID = strPROGBUYID;
                    bolPROGBUYID = true;
                    string newPROGBUYID_Name = "";

                    if (cbPROGBUYID.SelectedItem == null)
                        newPROGBUYID_Name = "";
                    else
                        newPROGBUYID_Name = ((ComboBoxItem)cbPROGBUYID.SelectedItem).Content.ToString().Trim();

                    msg += "\n外購類別(大項)：" + m_FormData.FSPROGBUYID_NAME + "->" + newPROGBUYID_Name;
                }
                else
                    obj.FSPROGBUYID = "";

                if (m_FormData.FSPROGBUYDID.Trim() != strPROGBUYDIDS)
                {
                    obj.FSPROGBUYDID = strPROGBUYDIDS;
                    bolPROGBUYDIDS = true;
                    string newPROGBUYDIDS_Name = "";

                    if (cbPROGBUYDIDS.SelectedItem == null)
                        newPROGBUYDIDS_Name = "";
                    else
                        newPROGBUYDIDS_Name = ((ComboBoxItem)cbPROGBUYDIDS.SelectedItem).Content.ToString().Trim();

                    msg += "\n外購(細項)：" + m_FormData.FSPROGBUYDID_NAME + "->" + newPROGBUYDIDS_Name;
                }
                else
                    obj.FSPROGBUYDID = "";
            }

            //子集行銷改成不要更新到子集
            //2014-09-19 註解by Jarvis
            //------------------------------
            // obj.FSPROGSALEID = "";
            //-------------------------------


            //檢查是否也有子集行銷的修改權限
            if (ModuleClass.getModulePermissionByName("01", "節目主檔-行銷類別維護") == true && ModuleClass.getModulePermissionByName("02", "節目子集-行銷類別維護") == true)
            {
                if (m_FormData.FSPROGSALEID.Trim() != strPROGSALEID)
                {
                    obj.FSPROGSALEID = strPROGSALEID;
                    bolPROGSALEID = true;
                    string newPROGSALEID_Name = "";

                    if (cbPROGSALEID.SelectedItem == null)
                        newPROGSALEID_Name = "";
                    else
                        newPROGSALEID_Name = ((ComboBoxItem)cbPROGSALEID.SelectedItem).Content.ToString().Trim();

                    msg += "\n行銷類別：" + m_FormData.FSPROGSALEID_NAME + "->" + newPROGSALEID_Name;
                }
                else
                    obj.FSPROGSALEID = "";
            }

            if (bolPROGSRCID == true || bolPROGATTRID == true || bolPROGAUDID == true || bolPROGTYPEID == true || bolPROGLANGID1 == true || bolPROGLANGID2 == true || bolPROGBUYID == true || bolPROGBUYDIDS == true || bolPROGSALEID == true || bolPROGBUYID == true || bolPROGBUYDIDS == true || bolPROGSALEID == true || bolPROGNATIONID == true)
            {
                return msg;
                //修改前要詢問

            }

            return "";
        }

        //Original Backup
        //private void checkSix()
        //{
        //    string msg = "是否要一併修改所有子集的資料?";            
        //    string strPROGSRCID = ((ComboBoxItem)cbPROGSRCID.SelectedItem).Tag.ToString().Trim();       //節目來源
        //    string strPROGATTRID = ((ComboBoxItem)cbPROGATTRID.SelectedItem).Tag.ToString().Trim();     //節目型態
        //    string strPROGAUDID = ((ComboBoxItem)cbPROGAUDID.SelectedItem).Tag.ToString().Trim();       //目標觀眾
        //    string strPROGTYPEID = ((ComboBoxItem)cbPROGTYPEID.SelectedItem).Tag.ToString().Trim();     //表現方式
        //    string strPROGLANGID1 = ((ComboBoxItem)cbPROGLANGID1.SelectedItem).Tag.ToString().Trim();   //主聲道
        //    string strPROGLANGID2 = ((ComboBoxItem)cbPROGLANGID2.SelectedItem).Tag.ToString().Trim();   //副聲道

        //    string strPROGBUYID = "";      //外購大項
        //    string strPROGBUYDIDS = "";  //外購細項
        //    string strPROGSALEID = ""; //行銷
        //    string strPROGNATIONID = "";//來源國家

        //    if (cbPROGBUYID.SelectedItem == null)
        //        strPROGBUYID = "";
        //    else
        //        strPROGBUYID = ((ComboBoxItem)cbPROGBUYID.SelectedItem).Tag.ToString().Trim();

        //    if (cbPROGBUYDIDS.SelectedItem == null)
        //        strPROGBUYDIDS = "";
        //    else
        //        strPROGBUYDIDS = ((ComboBoxItem)cbPROGBUYDIDS.SelectedItem).Tag.ToString().Trim();

        //    if (cbPROGSALEID.SelectedItem == null)
        //        strPROGSALEID = "";
        //    else
        //        strPROGSALEID = ((ComboBoxItem)cbPROGSALEID.SelectedItem).Tag.ToString().Trim();

        //    if (cbNATION_ID.SelectedItem != null)
        //    {
        //        strPROGNATIONID=((ComboBoxItem)cbNATION_ID.SelectedItem).Tag.ToString().Trim();
        //    }

        //    Boolean bolPROGSRCID = false;
        //    Boolean bolPROGATTRID = false;
        //    Boolean bolPROGAUDID = false;
        //    Boolean bolPROGTYPEID = false;
        //    Boolean bolPROGLANGID1 = false;
        //    Boolean bolPROGLANGID2 = false;
        //    Boolean bolPROGBUYID = false;
        //    Boolean bolPROGBUYDIDS = false;
        //    Boolean bolPROGSALEID = false;
        //    Boolean bolPROGNATIONID = false;

        //    //檢查有沒有權限，若是沒有權限也就沒有檢查改變及更新的必要
        //    if (ModuleClass.getModulePermissionByName("01", "節目主檔-六大屬性維護") == false &&
        //        ModuleClass.getModulePermissionByName("01", "節目主檔-外購類別維護") == false &&
        //        ModuleClass.getModulePermissionByName("01", "節目主檔-行銷類別維護") == false)
        //    {
        //        MessageBox.Show("修改節目資料「" + strProgName.ToString() + "」成功！", "提示訊息", MessageBoxButton.OK);
        //        this.DialogResult = true;
        //    }

        //    Class_PROGD obj = new Class_PROGD();
        //    obj.FSPROG_ID = tbxtProgID.Text.ToString();     //節目編號
        //    obj.FNEPISODE = 0;
        //    obj.FSPGDNAME = "";
        //    obj.FSPROGSRCID = "";
        //    obj.FSPROGATTRID = "";
        //    obj.FSPROGAUDID = "";
        //    obj.FSPROGTYPEID = "";
        //    obj.FSPROGLANGID1 = "";
        //    obj.FSPROGLANGID2 = "";
        //    obj.FSPROGBUYID = "";
        //    obj.FSPROGBUYDID = "";
        //    obj.FSPROGSALEID = "";
        //    obj.FSCRTUSER = UserClass.userData.FSUSER_ID.ToString();
        //    obj.FSUPDUSER = UserClass.userData.FSUSER_ID.ToString();
        //    obj.FSPROGNATIONID = "";
        //    obj.FSPROGSALEID = "";


        //    //檢查是否也有子集六大屬性的修改權限
        //    if (ModuleClass.getModulePermissionByName("01", "節目主檔-六大屬性維護") == true && ModuleClass.getModulePermissionByName("02", "節目子集-四大屬性維護") == true)
        //    {
        //        if (m_FormData.FSPROGSRCID.Trim() != strPROGSRCID)
        //        {
        //            obj.FSPROGSRCID = strPROGSRCID;
        //            bolPROGSRCID = true;
        //            msg += "\n節目來源：" + m_FormData.FSPROGSRCID_NAME + "->" + ((ComboBoxItem)cbPROGSRCID.SelectedItem).Content.ToString().Trim();
        //        }
        //        else
        //            obj.FSPROGSRCID = "";

        //        if (m_FormData.FSPROGATTRID.Trim() != strPROGATTRID)
        //        {
        //            obj.FSPROGATTRID = strPROGATTRID;
        //            bolPROGATTRID = true;
        //            msg += "\n節目型態：" + m_FormData.FSPROGATTRID_NAME + "->" + ((ComboBoxItem)cbPROGATTRID.SelectedItem).Content.ToString().Trim();
        //        }
        //        else
        //            obj.FSPROGATTRID = "";

        //        if (m_FormData.FSPROGAUDID.Trim() != strPROGAUDID)
        //        {
        //            obj.FSPROGAUDID = strPROGAUDID;
        //            bolPROGAUDID = true;
        //            msg += "\n目標觀眾：" + m_FormData.FSPROGAUDID_NAME + "->" + ((ComboBoxItem)cbPROGAUDID.SelectedItem).Content.ToString().Trim();
        //        }
        //        else
        //            obj.FSPROGAUDID = "";

        //        if (m_FormData.FSPROGTYPEID.Trim() != strPROGTYPEID)
        //        {
        //            obj.FSPROGTYPEID = strPROGTYPEID;
        //            bolPROGTYPEID = true;
        //            msg += "\n表現方式：" + m_FormData.FSPROGTYPEID_NAME + "->" + ((ComboBoxItem)cbPROGTYPEID.SelectedItem).Content.ToString().Trim();
        //        }
        //        else
        //            obj.FSPROGTYPEID = "";

        //        //原本也要更新主、副聲道，但是改成不更新
        //        //2014-09-19又改回要更新 by Jarvis
        //        if (m_FormData.FSPROGLANGID1.Trim() != strPROGLANGID1)
        //        {
        //            obj.FSPROGLANGID1 = strPROGLANGID1;
        //            bolPROGLANGID1 = true;
        //            msg += "\n主聲道：" + m_FormData.FSPROGLANGID1_NAME + "->" + ((ComboBoxItem)cbPROGLANGID1.SelectedItem).Content.ToString().Trim();
        //        }
        //        else
        //        obj.FSPROGLANGID1 = "";

        //        if (m_FormData.FSPROGLANGID2.Trim() != strPROGLANGID2)
        //        {
        //            obj.FSPROGLANGID2 = strPROGLANGID2;
        //            bolPROGLANGID2 = true;
        //            msg += "\n副聲道：" + m_FormData.FSPROGLANGID2_NAME + "->" + ((ComboBoxItem)cbPROGLANGID2.SelectedItem).Content.ToString().Trim();
        //        }
        //        else
        //        obj.FSPROGLANGID2 = "";


        //        //if (m_FormData.FSPROGBUYID.Trim() != strPROGBUYID) 
        //        //{
        //        //    obj.FSPROGBUYDID = strPROGBUYID;
        //        //    bolPROGBUYID = true;
        //        //    msg+="\n外購類別(大項):"+m_FormData.FSPROGBUYID_NAME+"->"+ ((ComboBoxItem)cbPROGBUYID.SelectedItem).Content.ToString().Trim();
        //        //}

        //        //if (m_FormData.FSPROGBUYDID.Trim() != strPROGBUYDIDS)
        //        //{
        //        //    obj.FSPROGBUYDID = strPROGBUYDIDS;
        //        //    bolPROGBUYDIDS = true;
        //        //    msg += "\n外購類別(細項):" + m_FormData.FSPROGBUYDID_NAME + "->" + ((ComboBoxItem)cbPROGBUYDIDS.SelectedItem).Content.ToString().Trim();
        //        //}

        //        //if (m_FormData.FSPROGSALEID.Trim() != strPROGSALEID)
        //        //{
        //        //    obj.FSPROGSALEID = strPROGSALEID;
        //        //    bolPROGSALEID = true;
        //        //    msg += "\n行銷類別:" + m_FormData.FSPROGSALEID_NAME + "->" + ((ComboBoxItem)cbPROGSALEID.SelectedItem).Content.ToString().Trim();
        //        //}

        //        if (m_FormData.FSPROGNATIONID.Trim() != strPROGNATIONID)
        //        {
        //            obj.FSPROGNATIONID = strPROGNATIONID;
        //            bolPROGNATIONID = true;
        //            msg += "\n來源國家:" + m_FormData.FSPROGNATIONID_NAME + "->" + ((ComboBoxItem)cbNATION_ID.SelectedItem).Content.ToString().Trim();
        //        }
        //        /*
        //                     string strPROGBUYID = ((ComboBoxItem)cbPROGBUYID.SelectedItem).Tag.ToString().Trim();      //外購大項
        //    string strPROGBUYDIDS = ((ComboBoxItem)cbPROGBUYDIDS.SelectedItem).Tag.ToString().Trim();  //外購細項
        //    string strPROGSALEID = ((ComboBoxItem)cbPROGSALEID.SelectedItem).Tag.ToString().Trim(); ; //行銷
        //    string strPROGNATIONID = ((ComboBoxItem)cbNATION_ID.SelectedItem).Tag.ToString().Trim();   //來源國家

        //         */
        //    }

        //    //外購改成不要更新到子集
        //    //2014-09-19 註解by Jarvis
        //    //-------------------------------
        //    //obj.FSPROGBUYID = "";
        //    //obj.FSPROGBUYDID = "";
        //    //---------------------------------

        //    //檢查是否也有子集外購的修改權限
        //    if (ModuleClass.getModulePermissionByName("01", "節目主檔-外購類別維護") == true && ModuleClass.getModulePermissionByName("02", "節目子集-外購類別維護") == true)
        //    {
        //        if (m_FormData.FSPROGBUYID.Trim() != strPROGBUYID)
        //        {
        //            obj.FSPROGBUYID = strPROGBUYID;
        //            bolPROGBUYID = true;
        //            string newPROGBUYID_Name = "";

        //            if (cbPROGBUYID.SelectedItem == null)
        //                newPROGBUYID_Name = "";
        //            else
        //                newPROGBUYID_Name = ((ComboBoxItem)cbPROGBUYID.SelectedItem).Content.ToString().Trim();

        //            msg += "\n外購類別(大項)：" + m_FormData.FSPROGBUYID_NAME + "->" + newPROGBUYID_Name;
        //        }
        //        else
        //            obj.FSPROGBUYID = "";

        //        if (m_FormData.FSPROGBUYDID.Trim() != strPROGBUYDIDS)
        //        {
        //            obj.FSPROGBUYDID = strPROGBUYDIDS;
        //            bolPROGBUYDIDS = true;
        //            string newPROGBUYDIDS_Name = "";

        //            if (cbPROGBUYDIDS.SelectedItem == null)
        //                newPROGBUYDIDS_Name = "";
        //            else
        //                newPROGBUYDIDS_Name = ((ComboBoxItem)cbPROGBUYDIDS.SelectedItem).Content.ToString().Trim();

        //            msg += "\n外購(細項)：" + m_FormData.FSPROGBUYDID_NAME + "->" + newPROGBUYDIDS_Name;
        //        }
        //        else
        //            obj.FSPROGBUYDID = "";
        //    }

        //    //子集行銷改成不要更新到子集
        //    //2014-09-19 註解by Jarvis
        //    //------------------------------
        //    // obj.FSPROGSALEID = "";
        //    //-------------------------------


        //    //檢查是否也有子集行銷的修改權限
        //    if (ModuleClass.getModulePermissionByName("01", "節目主檔-行銷類別維護") == true && ModuleClass.getModulePermissionByName("02", "節目子集-行銷類別維護") == true)
        //    {
        //        if (m_FormData.FSPROGSALEID.Trim() != strPROGSALEID)
        //        {
        //            obj.FSPROGSALEID = strPROGSALEID;
        //            bolPROGSALEID = true;
        //            string newPROGSALEID_Name = "";

        //            if (cbPROGSALEID.SelectedItem == null)
        //                newPROGSALEID_Name = "";
        //            else
        //                newPROGSALEID_Name = ((ComboBoxItem)cbPROGSALEID.SelectedItem).Content.ToString().Trim();

        //            msg += "\n行銷類別：" + m_FormData.FSPROGSALEID_NAME + "->" + newPROGSALEID_Name;
        //        }
        //        else
        //            obj.FSPROGSALEID = "";
        //    }

        //    if (bolPROGSRCID == true || bolPROGATTRID == true || bolPROGAUDID == true || bolPROGTYPEID == true || bolPROGLANGID1 == true || bolPROGLANGID2 == true || bolPROGBUYID == true || bolPROGBUYDIDS == true || bolPROGSALEID == true || bolPROGBUYID == true || bolPROGBUYDIDS == true || bolPROGSALEID == true || bolPROGNATIONID==true)
        //    {
        //        //修改前要詢問
        //        MessageBoxResult resultMsg = MessageBox.Show(msg, "提示訊息", MessageBoxButton.OKCancel);
        //        if (resultMsg == MessageBoxResult.OK)
        //            clientD.UPDATE_TBPROGD_ALL_NINEAsync(obj, bolPROGSRCID, bolPROGATTRID, bolPROGAUDID, bolPROGTYPEID, bolPROGLANGID1, bolPROGLANGID2, bolPROGBUYID, bolPROGBUYDIDS, bolPROGSALEID, bolPROGNATIONID);
        //        else
        //        {
        //            MessageBox.Show("修改節目資料「" + strProgName.ToString() + "」成功！", "提示訊息", MessageBoxButton.OK);
        //            this.DialogResult = true;
        //        }
        //    }
        //    else
        //    {
        //        MessageBox.Show("修改節目資料「" + strProgName.ToString() + "」成功！", "提示訊息", MessageBoxButton.OK);
        //        this.DialogResult = true;
        //    }
        //}

        //修改子集權限再分成六大屬性、外購、行銷
        private void checkPermission()
        {
            if (ModuleClass.getModulePermissionByName("01", "節目主檔-六大屬性維護") == false)
            {
                cbPROGSRCID.IsEnabled = false;
                cbPROGATTRID.IsEnabled = false;
                cbPROGAUDID.IsEnabled = false;
                cbPROGTYPEID.IsEnabled = false;
                cbPROGLANGID1.IsEnabled = false;
                cbPROGLANGID2.IsEnabled = false;
            }

            if (ModuleClass.getModulePermissionByName("01", "節目主檔-外購類別維護") == false)
            {
                cbPROGBUYID.IsEnabled = false;
                cbPROGBUYDIDS.IsEnabled = false;
            }

            if (ModuleClass.getModulePermissionByName("01", "節目主檔-行銷類別維護") == false)
            {
                cbPROGSALEID.IsEnabled = false;
            }
        }

    }
}

