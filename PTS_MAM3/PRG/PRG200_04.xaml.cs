﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.PROG_M;
using PTS_MAM3.WSPROG_D;
using PTS_MAM3.WSPROG_M;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace PTS_MAM3.ProgData
{
    public partial class PROGDDATA_Query : ChildWindow
    {

        WSPROG_DSoapClient client = new WSPROG_DSoapClient();               //產生新的代理類別
        WSPROG_MSoapClient clientPROGM = new WSPROG_MSoapClient();  
        List<Class_CODE> m_CodeDataBuyD = new List<Class_CODE>();           //代碼檔集合(外購類別細項) 
        public List<Class_PROGD> m_ListFormData = new List<Class_PROGD>();  //記錄查詢到的節目子集資料
        public Class_PROGD m_Queryobj = new Class_PROGD();                  //查詢條件的obj

        public PROGDDATA_Query()
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面  
        }

        void InitializeForm() //初始化本頁面
        {
            //下載代碼檔
            clientPROGM.fnGetTBPROG_M_CODECompleted += new EventHandler<fnGetTBPROG_M_CODECompletedEventArgs>(clientPROGM_fnGetTBPROG_M_CODECompleted);           
            clientPROGM.fnGetTBPROG_M_CODEAsync();

            //下載外購類別細項代碼檔
            clientPROGM.fnGetTBPROG_M_PROGBUYD_CODECompleted += new EventHandler<fnGetTBPROG_M_PROGBUYD_CODECompletedEventArgs>(client_fnGetTBPROG_M_PROGBUYD_CODECompleted);
            clientPROGM.fnGetTBPROG_M_PROGBUYD_CODEAsync();

            //查詢節目子集資料
            client.QUERY_TBPROG_DCompleted += new EventHandler<QUERY_TBPROG_DCompletedEventArgs>(client_QUERY_TBPROG_DCompleted);            
        }

        //實作-查詢節目子集資料
        void client_QUERY_TBPROG_DCompleted(object sender, QUERY_TBPROG_DCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)  //若是有查詢結果，則顯示，不然就請使用者重新查詢
                {
                    m_ListFormData = new List<Class_PROGD>(e.Result);  //把取回來的結果，加上型別定義，才可以塞回去    
                    this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("查無節目子集資料，請重新輸入條件查詢！", "提示訊息", MessageBoxButton.OK);
                    return;
                }
            }
        }
        
        #region ComboBox載入代碼檔

        //實作-下載外購類別細項代碼檔
        void client_fnGetTBPROG_M_PROGBUYD_CODECompleted(object sender, fnGetTBPROG_M_PROGBUYD_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {                  
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        ComboBoxItem cbiPROGBUYD = new ComboBoxItem();
                        cbiPROGBUYD.Content = CodeData.NAME;
                        cbiPROGBUYD.Tag = CodeData.ID;
                        cbiPROGBUYD.DataContext = CodeData.IDD;
                        this.cbPROGBUYDID.Items.Add(cbiPROGBUYD);   //先都產生在隱藏起來的細項代碼combobox裡                   
                        m_CodeDataBuyD.Add(CodeData);
                    }
                }
            }
        }

        //實作-下載所有的代碼檔
        void clientPROGM_fnGetTBPROG_M_CODECompleted(object sender, fnGetTBPROG_M_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            case "TBZCHANNEL":       //頻道別代碼
                                ComboBoxItem cbiCHANNEL = new ComboBoxItem();
                                cbiCHANNEL.Content = CodeData.NAME;
                                cbiCHANNEL.Tag = CodeData.ID;
                                this.cbCHANNEL_ID.Items.Add(cbiCHANNEL);
                                break;
                            case "TBZSHOOTSPEC":       //拍攝規格代碼
                                CheckBox cbSHOOTSPEC = new CheckBox();
                                cbSHOOTSPEC.Content = CodeData.NAME;
                                cbSHOOTSPEC.Tag = CodeData.ID;
                                listSHOOTSPEC.Items.Add(cbSHOOTSPEC);
                                break;
                            case "TBZPROGGRADE":      //節目分級代碼
                                ComboBoxItem cbiPROGGRADE = new ComboBoxItem();
                                cbiPROGGRADE.Content = CodeData.NAME;
                                cbiPROGGRADE.Tag = CodeData.ID;
                                this.cbPROGGRADEID.Items.Add(cbiPROGGRADE);
                                break;
                            case "TBZPROGSRC":      //節目來源代碼
                                ComboBoxItem cbiPROGSRC = new ComboBoxItem();
                                cbiPROGSRC.Content = CodeData.NAME;
                                cbiPROGSRC.Tag = CodeData.ID;
                                this.cbPROGSRCID.Items.Add(cbiPROGSRC);
                                break;
                            case "TBZPROGATTR":      //內容屬性代碼
                                ComboBoxItem cbiPROGATTR = new ComboBoxItem();
                                cbiPROGATTR.Content = CodeData.NAME;
                                cbiPROGATTR.Tag = CodeData.ID;
                                this.cbPROGATTRID.Items.Add(cbiPROGATTR);
                                break;
                            case "TBZPROGAUD":      //目標觀眾代碼
                                ComboBoxItem cbiPROGAUD = new ComboBoxItem();
                                cbiPROGAUD.Content = CodeData.NAME;
                                cbiPROGAUD.Tag = CodeData.ID;
                                this.cbPROGAUDID.Items.Add(cbiPROGAUD);
                                break;
                            case "TBZPROGTYPE":      //表現方式代碼
                                ComboBoxItem cbiPROGTYPE = new ComboBoxItem();
                                cbiPROGTYPE.Content = CodeData.NAME;
                                cbiPROGTYPE.Tag = CodeData.ID;
                                this.cbPROGTYPEID.Items.Add(cbiPROGTYPE);
                                break;
                            case "TBZPROGLANG":      //語言代碼
                                ComboBoxItem cbiPROGLANG1 = new ComboBoxItem();
                                cbiPROGLANG1.Content = CodeData.NAME;
                                cbiPROGLANG1.Tag = CodeData.ID;
                                this.cbPROGLANGID1.Items.Add(cbiPROGLANG1);  //主聲道

                                ComboBoxItem cbiPROGLANG2 = new ComboBoxItem();
                                cbiPROGLANG2.Content = CodeData.NAME;
                                cbiPROGLANG2.Tag = CodeData.ID;
                                this.cbPROGLANGID2.Items.Add(cbiPROGLANG2);  //副聲道
                                break;
                            case "TBZPROGBUY":       //外購類別代碼
                                if (CodeData.NAME.Trim() != "")//要濾掉空白
                                {
                                    ComboBoxItem cbiPROGBUY = new ComboBoxItem();
                                    cbiPROGBUY.Content = CodeData.NAME;
                                    cbiPROGBUY.Tag = CodeData.ID;
                                    this.cbPROGBUYID.Items.Add(cbiPROGBUY);
                                }
                                break;
                            case "TBZPROGSALE":       //行銷類別代碼
                                ComboBoxItem cbiPROGSALE = new ComboBoxItem();
                                cbiPROGSALE.Content = CodeData.NAME;
                                cbiPROGSALE.Tag = CodeData.ID;
                                this.cbPROGSALEID.Items.Add(cbiPROGSALE);
                                break;
                            case "TBZPROGSPEC":       //節目規格代碼
                                CheckBox cbPROGSPEC = new CheckBox();
                                cbPROGSPEC.Content = CodeData.NAME;
                                cbPROGSPEC.Tag = CodeData.ID;
                                listPROGSPEC.Items.Add(cbPROGSPEC);
                                break;
                            default:
                                break;
                        }
                    }                                       
                }
            }
        }
              
        private string[] CheckList(string strChannel)
        {
            char[] delimiterChars = { ';' };
            string[] words = strChannel.Split(delimiterChars);
            return words;
        }

        //選擇外購類別，要判斷顯示哪些外購類別細項
        private void cbBUY_ID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.cbPROGBUYDIDS.Items.Clear();     //使用前先清空
            string strBUYID = ""; //外購類別大項
            ComboBox cb = sender as ComboBox;
            ComboBoxItem cbm = (ComboBoxItem)cb.SelectedItem;
            strBUYID = cbm.Tag.ToString();

            for (int i = 0; i < cbPROGBUYDID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGBUYDID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strBUYID))
                {
                    ComboBoxItem cbiPROGBUYDS = new ComboBoxItem();
                    cbiPROGBUYDS.Content = getcbi.Content;
                    cbiPROGBUYDS.Tag = getcbi.DataContext;
                    this.cbPROGBUYDIDS.Items.Add(cbiPROGBUYDS);
                }
            }
        }

        #endregion

        #region 比對代碼檔     

        //選擇外購類別，要判斷顯示哪些外購類別細項
        private void cbPROGBUYID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.cbPROGBUYDIDS.Items.Clear();     //使用前先清空
            string strBUYID = ""; //外購類別大項
            ComboBox cb = sender as ComboBox;
            ComboBoxItem cbm = (ComboBoxItem)cb.SelectedItem;
            strBUYID = cbm.Tag.ToString();

            for (int i = 0; i < cbPROGBUYDID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGBUYDID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strBUYID))
                {
                    ComboBoxItem cbiPROGBUYDS = new ComboBoxItem();
                    cbiPROGBUYDS.Content = getcbi.Content;
                    cbiPROGBUYDS.Tag = getcbi.DataContext;
                    this.cbPROGBUYDIDS.Items.Add(cbiPROGBUYDS);
                }
            }
        }

        #endregion

        #region 檢查畫面資料及搬值

        private Class_PROGD FormToClass_PROGD()
        {
            Class_PROGD obj = new Class_PROGD();

            if (tbxPROG_NAME.Text != "")
                obj.FSPROG_ID = tbxPROG_NAME.Tag.ToString().Trim();                     //節目編號
            else
                obj.FSPROG_ID = "";

            if (tbxEPISODE.Text != "")
                obj.FNEPISODE = Convert.ToInt16(tbxEPISODE.Text.ToString().Trim());     //集別
            else
                obj.FNEPISODE = 0;            

            if (tbxSHOWEPISODE.Text.ToString().Trim() == "")                            //播出集別            
                obj.FNSHOWEPISODE = 0;   
            else
                obj.FNSHOWEPISODE = Convert.ToInt16(tbxSHOWEPISODE.Text.ToString().Trim());           
                      
            obj.FSPGDNAME = tbxPGDNAME.Text.ToString().Trim();                          //子集名稱
            obj.FSPGDENAME = tbxPGDENAME.Text.ToString().Trim();                        //子集外文名稱

            if (tbxLENGTH.Text.ToString().Trim() == "")                                 //時長            
                obj.FNLENGTH = 0;
            else
                obj.FNLENGTH = Convert.ToInt16(tbxLENGTH.Text.ToString().Trim());

            if (tbxTAPELENGTH.Text.ToString().Trim() == "" && tbxTAPELENGTH_S.Text.ToString().Trim() == "")//實際帶長           
                obj.FNTAPELENGTH = 0;
            else if (tbxTAPELENGTH.Text.ToString().Trim() != "" && tbxTAPELENGTH_S.Text.ToString().Trim() == "")         
                obj.FNTAPELENGTH = Convert.ToInt32(tbxTAPELENGTH.Text.ToString().Trim())*60;
            else if (tbxTAPELENGTH.Text.ToString().Trim() == "" && tbxTAPELENGTH_S.Text.ToString().Trim() != "")
                obj.FNTAPELENGTH = Convert.ToInt32(tbxTAPELENGTH_S.Text.ToString().Trim());
            else if (tbxTAPELENGTH.Text.ToString().Trim() != "" && tbxTAPELENGTH_S.Text.ToString().Trim() != "")
                obj.FNTAPELENGTH = Convert.ToInt32(tbxTAPELENGTH.Text.ToString().Trim()) * 60 + Convert.ToInt32(tbxTAPELENGTH_S.Text.ToString().Trim());

            if (cbCHANNEL_ID.SelectedIndex != -1)                                       //頻道別
                obj.FSCHANNEL_ID = ((ComboBoxItem)cbCHANNEL_ID.SelectedItem).Tag.ToString();
            else
                obj.FSCHANNEL_ID = "";

            if (cbRACE.SelectedIndex != -1)                                             //可排檔
                obj.FCRACE = ((ComboBoxItem)cbRACE.SelectedItem).Tag.ToString();
            else
                obj.FCRACE = "" ;

            obj.FSPRDYEAR = tbxPRDYEAR.Text.ToString().Trim();                          //完成年度
            obj.FSSHOOTSPEC = Check_SHOOTSPECList();                                    //拍攝規格
            obj.FSPROGGRADE = tbxPROGGRADE.Text.ToString().Trim();                      //非正常分級的說明
            obj.FSCONTENT = tbxCONTENT.Text.ToString().Trim();                          //內容簡述
            obj.FSMEMO = tbxMEMO.Text.ToString().Trim();                                //備註 

            if (cbPROGGRADEID.SelectedIndex != -1)                //節目分級
                obj.FSPROGGRADEID = ((ComboBoxItem)cbPROGGRADEID.SelectedItem).Tag.ToString();
            else
                obj.FSPROGGRADEID = "";

            if (cbPROGSRCID.SelectedIndex != -1)                 //節目來源
                obj.FSPROGSRCID = ((ComboBoxItem)cbPROGSRCID.SelectedItem).Tag.ToString();
            else
                obj.FSPROGSRCID = "";

            if (cbPROGATTRID.SelectedIndex != -1)                //節目型態
                obj.FSPROGATTRID = ((ComboBoxItem)cbPROGATTRID.SelectedItem).Tag.ToString();
            else
                obj.FSPROGATTRID = "";

            if (cbPROGAUDID.SelectedIndex != -1)                 //目標觀眾
                obj.FSPROGAUDID = ((ComboBoxItem)cbPROGAUDID.SelectedItem).Tag.ToString();
            else
                obj.FSPROGAUDID = "";

            if (cbPROGTYPEID.SelectedIndex != -1)                //表現方式
                obj.FSPROGTYPEID = ((ComboBoxItem)cbPROGTYPEID.SelectedItem).Tag.ToString();
            else
                obj.FSPROGTYPEID = "";

            if (cbPROGLANGID1.SelectedIndex != -1)               //主聲道
                obj.FSPROGLANGID1 = ((ComboBoxItem)cbPROGLANGID1.SelectedItem).Tag.ToString();
            else
                obj.FSPROGLANGID1 = "";

            if (cbPROGLANGID2.SelectedIndex != -1)               //副聲道
                obj.FSPROGLANGID2 = ((ComboBoxItem)cbPROGLANGID2.SelectedItem).Tag.ToString();
            else
                obj.FSPROGLANGID2 = "";

            if (cbPROGBUYID.SelectedIndex != -1)                 //外購類別大類
                obj.FSPROGBUYID = ((ComboBoxItem)cbPROGBUYID.SelectedItem).Tag.ToString();
            else
                obj.FSPROGBUYID = "";

            if (cbPROGBUYDIDS.SelectedIndex != -1)               //外購類別細類
                obj.FSPROGBUYDID = ((ComboBoxItem)cbPROGBUYDIDS.SelectedItem).Tag.ToString();
            else
                obj.FSPROGBUYDID = "";

            if (cbPROGSALEID.SelectedIndex != -1)                //行銷類別
                obj.FSPROGSALEID = ((ComboBoxItem)cbPROGSALEID.SelectedItem).Tag.ToString();
            else
                obj.FSPROGSALEID = "";

            if (cbCR_TYPE.SelectedIndex != -1)                //著作權類型
                obj.FSCR_TYPE = ((ComboBoxItem)cbCR_TYPE.SelectedItem).Tag.ToString();
            else
                obj.FSCR_TYPE = "";

            obj.FSCR_NOTE = tbxCR_NOTE.Text.ToString().Trim();          //著作權備註

            //刪除註記及刪除者先不給查
            obj.FSDEL = "";             //刪除註記
            obj.FSDELUSER = "";         //刪除者

            obj.FSPROGSPEC = Check_PROGSPECList();             //節目規格

            m_Queryobj = obj;           //紀錄查詢條件

            return obj;
        }

        private string Check_SHOOTSPECList()    //檢查拍攝規格有哪些
        {
            string strReturn = "";
            List<string> ListCHANNEL = new List<string>();  //TEST
            for (int i = 0; i < listSHOOTSPEC.Items.Count; i++)
            {
                CheckBox getckb = (CheckBox)listSHOOTSPEC.Items.ElementAt(i);
                if (getckb.IsChecked == true)
                {
                    strReturn += getckb.Tag.ToString().Trim() + ";";
                }
            }
            return strReturn;
        }

        private string Check_PROGSPECList()    //檢查節目規格有哪些
        {
            string strReturn = "";
            for (int i = 0; i < listPROGSPEC.Items.Count; i++)
            {
                CheckBox getckb = (CheckBox)listPROGSPEC.Items.ElementAt(i);
                if (getckb.IsChecked == true)
                {
                    strReturn += getckb.Tag.ToString().Trim() + ";";
                }
            }
            return strReturn;
        }

        private Boolean Check_PROGDData()    //檢查畫面上的欄位是否都填妥
        {
            StringBuilder strMessage = new StringBuilder();
            double intCheck;   //純粹為了判斷是否為數字型態之用   
                    
            if (tbxSHOWEPISODE.Text.Trim() != "")
            {
                if (double.TryParse(tbxSHOWEPISODE.Text.Trim(), out intCheck) == false)
                    strMessage.AppendLine("請檢查「播出集別」欄位必須為數值型態");
            }

            if (tbxLENGTH.Text.Trim() != "")
            {
                if (double.TryParse(tbxLENGTH.Text.Trim(), out intCheck) == false)
                    strMessage.AppendLine("請檢查「時長」欄位必須為數值型態");
            }

            if (tbxTAPELENGTH.Text.Trim() != "")
            {
                if (double.TryParse(tbxTAPELENGTH.Text.Trim(), out intCheck) == false)
                    strMessage.AppendLine("請檢查「實際帶長-分」欄位必須為數值型態");
            }

            if (tbxTAPELENGTH_S.Text.Trim() != "")
            {
                if (double.TryParse(tbxTAPELENGTH_S.Text.Trim(), out intCheck) == false)
                    strMessage.AppendLine("請檢查「實際帶長-秒」欄位必須為數值型態");
                else
                {
                    if (intCheck >= 60)
                        strMessage.AppendLine("請檢查「實際帶長-秒」欄位必須小於60");
                }
            }
            
            if (tbxPRDYEAR.Text.Trim() != "")
            {
                if (double.TryParse(tbxPRDYEAR.Text.Trim(), out intCheck) == false)
                    strMessage.AppendLine("請檢查「完成年度」欄位必須為數值型態");
                else
                {
                    if (tbxPRDYEAR.Text.Trim().Length != 4)
                        strMessage.AppendLine("請檢查「完成年度」欄位必須為四碼的西元年\n" + "範例：2011(2011年)");
                }
            }
            
            if (strMessage.ToString().Trim() == "")
                return true;
            else
            {
                MessageBoxResult result = MessageBox.Show(strMessage.ToString(), "提示訊息", MessageBoxButton.OK);
                return false;
            }
        }

        #endregion

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (Check_PROGDData() == false)      //檢查畫面上的欄位是否都填妥
                return;

            //查詢節目子集資料
            client.QUERY_TBPROG_DAsync(FormToClass_PROGD());
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //開啟節目資料查詢畫面
        private void btnPROG_Click(object sender, RoutedEventArgs e)
        {
            //PRG100_01
            PROGDATA_VIEW PROGDATA_VIEW_frm = new PROGDATA_VIEW();
            PROGDATA_VIEW_frm.Show();

            PROGDATA_VIEW_frm.Closed += (s, args) =>
            {
                if (PROGDATA_VIEW_frm.DialogResult == true)
                {
                    tbxPROG_NAME.Text = PROGDATA_VIEW_frm.strProgName_View;
                    tbxPROG_NAME.Tag = PROGDATA_VIEW_frm.strProgID_View;

                    tbxEPISODE.Text = "";
                    lblEPISODE_NAME.Content = "";
                }
            };            
        }

        //開啟節目子集資料查詢畫面
        private void btnEPISODE_Click(object sender, RoutedEventArgs e)
        {
            if (tbxPROG_NAME.Tag == null)
            {
                MessageBox.Show("請先選擇節目", "提示訊息", MessageBoxButton.OK);
                return;
            }

             PRG.PRG200_07 PRG200_07_frm = new PRG.PRG200_07(tbxPROG_NAME.Tag.ToString().Trim(), tbxPROG_NAME.Text.ToString().Trim());
             PRG200_07_frm.Show();

             PRG200_07_frm.Closed += (s, args) =>
             {
                 if (PRG200_07_frm.DialogResult == true)
                 {
                     tbxEPISODE.Text = PRG200_07_frm.m_strEpisode_View;
                     lblEPISODE_NAME.Content = PRG200_07_frm.m_strProgDName_View;
                 }
             };    
        }
                       
    }
}

