﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;
using System.IO;
using System.Text;
using PTS_MAM3.WSPROGRACE;
using PTS_MAM3.PROG_M;
using PTS_MAM3.PRG;

namespace PTS_MAM3.ProgData
{
    public partial class PROGRACE_Add : ChildWindow
    {
        WSPROGRACESoapClient client = new WSPROGRACESoapClient();   //產生新的代理類別
        List<string> m_ListRace = new List<string>();               //參展名稱集合 
        List<string> m_ListWin = new List<string>();                //得獎名稱集合
        string m_strProgName = "";                                  //暫存節目名稱 
        string m_strEpisode= "";                                    //暫存集別
        Boolean m_Continue = false;
        public Class_PROGRACE m_Queryobj = new Class_PROGRACE();    //查詢新增的obj

        public PROGRACE_Add()
        {
            InitializeComponent();
            InitializeForm();          //初始化本頁面
        }

        void InitializeForm() //初始化本頁面
        {
            //下載代碼檔//原本參展狀態是用代碼檔，後來改成只有三種：入選、入圍、得獎
            //client.fnGetTBPROGRACE_CODECompleted += new EventHandler<fnGetTBPROGRACE_CODECompletedEventArgs>(client_fnGetTBPROGRACE_CODECompleted);
            //client.fnGetTBPROGRACE_CODEAsync();

            //新增參展得獎資料
            client.fnINSERT_TBPROGRACECompleted += new EventHandler<fnINSERT_TBPROGRACECompletedEventArgs>(client_fnINSERT_TBPROGRACECompleted);

            //參展名稱 AUTO COMPLETE
            client.fnGetRACECompleted += new EventHandler<fnGetRACECompletedEventArgs>(client_fnGetRACECompleted);
            client.fnGetRACEAsync();

            //得獎名稱 AUTO COMPLETE
            client.fnGetWINCompleted += new EventHandler<fnGetWINCompletedEventArgs>(client_fnGetWINCompleted);
            client.fnGetWINAsync();

            //查詢參展得獎資料(避免重複)
            client.fnQUERY_TBPROGRACE_CHECKCompleted += new EventHandler<fnQUERY_TBPROGRACE_CHECKCompletedEventArgs>(client_fnQUERY_TBPROGRACE_CHECKCompleted);
        }

        #region 實作

        //實作-新增參展得獎資料
        void client_fnINSERT_TBPROGRACECompleted(object sender, fnINSERT_TBPROGRACECompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    MessageBox.Show("新增參展得獎資料「" + m_strProgName.ToString() + "-" + m_strEpisode + "集」成功！", "提示訊息", MessageBoxButton.OK);

                    if (m_Continue == false)          //讓使用者可以一直新增同樣內容的資料
                        this.DialogResult = true;  
                }
                else
                {
                    MessageBox.Show("新增參展得獎資料「" + m_strProgName.ToString() + "-" + m_strEpisode + "集」失敗！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
        }

        //實作-查詢參展得獎資料(避免重複)
        void client_fnQUERY_TBPROGRACE_CHECKCompleted(object sender, fnQUERY_TBPROGRACE_CHECKCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    if (e.Result == "0")
                    {
                        //新增參展記錄資料
                        client.fnINSERT_TBPROGRACEAsync(FormToClass_PROGRACE());
                        BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                    }
                    else
                    {
                        MessageBox.Show("資料庫已有重複資料，請檢查後重新輸入！", "提示訊息", MessageBoxButton.OK);
                    }
                }
            }
        }

        //實作-獎項名稱
        void client_fnGetWINCompleted(object sender, fnGetWINCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0 )
                {         
                    tbxPROGWIN.ItemsSource = e.Result;
                }
            }
        }

        //實作-影展名稱
        void client_fnGetRACECompleted(object sender, fnGetRACECompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0 )
                {         
                    tbxPROGRACE.ItemsSource = e.Result;
                }
            }
        }

        #endregion

        #region ComboBox載入代碼檔

        //實作-下載所有的代碼檔
        //void client_fnGetTBPROGRACE_CODECompleted(object sender, fnGetTBPROGRACE_CODECompletedEventArgs e)
        //{
        //    if (e.Error == null)
        //    {
        //        if (e.Result != null && e.Result.Count > 0)
        //        {
        //            for (int i = 0; i < e.Result.Count; i++)
        //            {
        //                Class_CODE CodeData = new Class_CODE() ;
        //                CodeData = e.Result[i];

        //                    switch (CodeData.TABLENAME)
        //                    {
        //                        case "TBZPROGSTATUS":      //參展狀態代碼
        //                            ComboBoxItem cbiPROGSTATUS = new ComboBoxItem();
        //                            cbiPROGSTATUS.Content = CodeData.NAME;
        //                            cbiPROGSTATUS.Tag = CodeData.ID;
        //                            this.cbPROGSTATUS.Items.Add(cbiPROGSTATUS);
        //                            break;
        //                        default:
        //                            break;
        //                    }
        //            }
        //        }    
        //    }
        //}

        #endregion

        #region 檢查畫面資料及搬值

        private Class_PROGRACE FormToClass_PROGRACE()
        {
            Class_PROGRACE obj = new Class_PROGRACE();

            obj.FSPROG_ID = tbxPROG_NAME.Tag.ToString().Trim();     //節目編碼
            m_strProgName = tbxPROG_NAME.Text.ToString().Trim();    //暫存本次新增的節目名稱，為了秀給使用者看結果用

            if (tbxEPISODE.Text.ToString().Trim() == "")            //集別
            {
                obj.FNEPISODE = 0;
                m_strEpisode = "";
            }
            else
            {
            obj.FNEPISODE = Convert.ToInt16(tbxEPISODE.Text.ToString().Trim());
            m_strEpisode = tbxEPISODE.Text.ToString().Trim();       //暫存本次新增的集別，為了秀給使用者看結果用
            }
                  
            obj.FSAREA = tbxAREA.Tag.ToString().Trim() ;            //參展區域
            obj.FSPROGRACE = tbxPROGRACE.Text.ToString().Trim();    //影展名稱

            if (dpRACEDATE.Text.Trim() == "")                       //參展日期
                obj.FDRACEDATE = new DateTime(1900, 1, 1);
            else
                obj.FDRACEDATE = Convert.ToDateTime(dpRACEDATE.Text);

            if (cbPROGSTATUS.SelectedIndex != -1)                  //參展狀態
                obj.FSPROGSTATUS = ((ComboBoxItem)cbPROGSTATUS.SelectedItem).Tag.ToString();
            else
                obj.FSPROGSTATUS = "";

            if (tbPERIOD.Text.ToString().Trim() == "")             //屆數
                obj.FNPERIOD = 0;
            else
                obj.FNPERIOD = Convert.ToInt16(tbPERIOD.Text.ToString().Trim());

            obj.FSPROGWIN = tbxPROGWIN.Text.ToString().Trim();      //獎項名稱
            obj.FSPROGNAME = tbxPROGNAME.Text.ToString().Trim();    //參展節目名稱
            obj.FSMEMO = tbxMEMO.Text.ToString().Trim() ;           //備註
            obj.FSCREATED_BY = UserClass.userData.FSUSER_ID.ToString();//建檔者
            obj.FSUPDATED_BY = UserClass.userData.FSUSER_ID.ToString();//修改者                     
            m_Queryobj = obj;

            return obj;
        }

        private Boolean Check_PROGRACEData()    //檢查畫面上的欄位是否都填妥
        {
            StringBuilder strMessage = new StringBuilder();
            short shortCheck;   //純粹為了判斷是否為數字型態之用

            if (tbxPROG_NAME.Text.Trim() == "")
                strMessage.AppendLine("請選擇「節目」");

            if (tbPERIOD.Text.Trim() != "")            
            {
                if (short.TryParse(tbPERIOD.Text.Trim(), out shortCheck) == false)              
                    strMessage.AppendLine("請檢查「屆數」欄位必須為數值型態");
            }

            if (cbPROGSTATUS.SelectedIndex == -1)
                strMessage.AppendLine("請選擇「參展狀態」欄位");

            if (tbxAREA.Tag == null)
                strMessage.AppendLine("請選擇「參展區域」欄位");

            if (tbxPROGRACE.Text.Trim() == "")
                strMessage.AppendLine("請填寫「影展名稱」欄位");

            if (tbxPROGRACE.Text.Trim().Length > 100)
                strMessage.AppendLine("「影展名稱」長度大於100，請檢查");

            if (tbxPROGWIN.Text.Trim().Length > 100)
                strMessage.AppendLine("「獎項名稱」長度大於100，請檢查");

            if (strMessage.ToString().Trim() == "")
                return true;
            else
            {
                MessageBoxResult result = MessageBox.Show(strMessage.ToString(), "提示訊息", MessageBoxButton.OK);
                return false;
            }
        }

        #endregion    

        //開啟節目資料查詢畫面
        private void btnPROG_Click(object sender, RoutedEventArgs e)
        {
            PRG500_07 PRG500_07_frm = new PRG500_07();
            PRG500_07_frm.Show();

            PRG500_07_frm.Closed += (s, args) =>
            {
                if (PRG500_07_frm.DialogResult == true)
                {
                    if (PRG500_07_frm.strProgName_View.Trim() != "")
                    {
                        tbxPROG_NAME.Text = PRG500_07_frm.strProgName_View;
                        tbxPROG_NAME.Tag = PRG500_07_frm.strProgID_View;   
                    }

                    if (PRG500_07_frm.strEpisodeName_View.Trim() != "")
                    {
                        tbxEPISODE.Text = PRG500_07_frm.strEpisode_View;
                        lblEPISODE_NAME.Content = PRG500_07_frm.strEpisodeName_View;
                    }
                    else
                    {
                        tbxEPISODE.Text = "";
                        lblEPISODE_NAME.Content = "";
                    }
                }              
            };
        }

        //開啟節目子集資料查詢畫面
        private void btnEPISODE_Click(object sender, RoutedEventArgs e)
        {
            if (tbxPROG_NAME.Text.ToString().Trim() != "" && tbxPROG_NAME.Tag.ToString().Trim() != "")
            {
                PRG.PRG200_07 PRG200_07_frm = new PRG.PRG200_07(tbxPROG_NAME.Tag.ToString().Trim(), tbxPROG_NAME.Text.ToString().Trim());
                PRG200_07_frm.Show();

                PRG200_07_frm.Closed += (s, args) =>
                {
                    if (PRG200_07_frm.DialogResult == true)
                    {
                        tbxEPISODE.Text = PRG200_07_frm.m_strEpisode_View;
                        lblEPISODE_NAME.Content = PRG200_07_frm.m_strProgDName_View;
                    }
                };
            }
            else
                MessageBox.Show("請先選擇節目！", "提示訊息", MessageBoxButton.OK);
        }

        //確定
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (Check_PROGRACEData() == false)      //檢查畫面上的欄位是否都填妥
                return;

            m_Continue = false;
            //檢查是否有相同的一集、同參展狀態、同參展區域、同屆、同參展名稱、且同得獎名稱，若有就無法新增
            client.fnQUERY_TBPROGRACE_CHECKAsync(FormToClass_PROGRACE());
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }

        //繼續新增
        private void ContinueButton_Click(object sender, RoutedEventArgs e)
        {
            if (Check_PROGRACEData() == false)      //檢查畫面上的欄位是否都填妥
                return;

            m_Continue = true;
            //檢查是否有相同的一集、同參展狀態、同參展區域、同屆、同參展名稱、且同得獎名稱，若有就無法新增
            client.fnQUERY_TBPROGRACE_CHECKAsync(FormToClass_PROGRACE());
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //選取參展區域
        private void btnAREA_Click(object sender, RoutedEventArgs e)
        {
            PRG500_05 PRG500_05_frm = new PRG500_05();
            PRG500_05_frm.Show();

            PRG500_05_frm.Closed += (s, args) =>
            {
                if (PRG500_05_frm.DialogResult == true)
                {
                    tbxAREA.Text = PRG500_05_frm.strName_View;
                    tbxAREA.Tag = PRG500_05_frm.strID_View;
                }
            };
        }

    }
}

