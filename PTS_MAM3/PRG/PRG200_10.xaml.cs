﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSPROG_D;
using PTS_MAM3.ProgData;

namespace PTS_MAM3.PRG
{
    public partial class PRG200_10 : ChildWindow
    {
        WSPROG_DSoapClient client = new WSPROG_DSoapClient(); //產生新的代理類別

        public string strPRDCENID_View = "";                //選取的製作單位編號
        public string strPRDCENName_View = "";              //選取的製作單位名稱

        public PRG200_10(string strProg_ID,short shortEpisode)
        {
           InitializeComponent();
         
           Class_PROGD obj = new Class_PROGD();
           obj.FSPROG_ID = strProg_ID;
           obj.FNEPISODE = shortEpisode;

           //查詢實際帶長
           client.GET_EPISODE_LENGTHCompleted += new EventHandler<GET_EPISODE_LENGTHCompletedEventArgs>(client_GET_EPISODE_LENGTHCompleted);
           client.GET_EPISODE_LENGTHAsync(obj);
           BusyMsg.IsBusy = true;  //關閉忙碌的Loading訊息
        }

        //實作-查詢實際帶長
        void client_GET_EPISODE_LENGTHCompleted(object sender, GET_EPISODE_LENGTHCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    this.DGTAPELENGTHList.DataContext = e.Result;                  
                }
                else
                {
                    MessageBox.Show("查無實際帶長資訊！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }  
        }
                  
        //離開
        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
       
     
 

    }
}

