﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSPROG_PRODUCER;
using System.Text;
using PTS_MAM3.PRG;

namespace PTS_MAM3.ProgData
{
    public partial class PRG600_01 : ChildWindow
    {
        WSPROG_PRODUCERSoapClient client = new WSPROG_PRODUCERSoapClient();     //產生新的代理類別
        string m_IDName = "";                                                   //暫存名稱 
        string m_strType = "";                                                  //類型
        string m_strID = "";                                                    //編碼
        Boolean m_Continue = false;
        Boolean refreshok = false;
        public Class_PROGPRODUCER m_Queryobj = new Class_PROGPRODUCER();        //查詢新增的obj
        List<UserStruct> ListUserAll = new List<UserStruct>();                  //所有的使用者資料

        private int intCbx1 = 0, intCbx2 = -1, intCbx3 = -1, intCbx4 = -1;

        List<MyDept> deptlist1 = new List<MyDept>();
        List<MyDept> deptlist2 = new List<MyDept>();
        List<MyDept> deptlist3 = new List<MyDept>();
        List<MyUser> userlist4 = new List<MyUser>();

        WSREPORT.WSREPORTSoapClient objReport1 = new WSREPORT.WSREPORTSoapClient();
        WSREPORT.WSREPORTSoapClient objReport2 = new WSREPORT.WSREPORTSoapClient();
        WSREPORT.WSREPORTSoapClient objReport3 = new WSREPORT.WSREPORTSoapClient();
        WSREPORT.WSREPORTSoapClient objReport4 = new WSREPORT.WSREPORTSoapClient();
        WSREPORT.WSREPORTSoapClient objRpt = new WSREPORT.WSREPORTSoapClient();

        public PRG600_01()
        {
            InitializeComponent();
            InitializeForm();          //初始化本頁面

            rdbProg.IsChecked = true;
        }

        void InitializeForm() //初始化本頁面
        {
            //新增節目或短帶製作人資料
            client.INSERT_TBPROG_PRODUCERCompleted += new EventHandler<INSERT_TBPROG_PRODUCERCompletedEventArgs>(client_INSERT_TBPROG_PRODUCERCompleted);

            //查詢節目或短帶製作人資料(避免重複)
            client.QUERY_TBPROG_PRODUCER_CHECKCompleted += new EventHandler<QUERY_TBPROG_PRODUCER_CHECKCompletedEventArgs>(client_QUERY_TBPROG_PRODUCER_CHECKCompleted);

            //將下拉式選單改成AutoComplete作法
            //objReport1.fnGetDeptListCompleted += new EventHandler<WSREPORT.fnGetDeptListCompletedEventArgs>(objReport1_fnGetDeptListCompleted);
            //objReport2.fnGetDeptListCompleted += new EventHandler<WSREPORT.fnGetDeptListCompletedEventArgs>(objReport2_fnGetDeptListCompleted);
            //objReport3.fnGetDeptListCompleted += new EventHandler<WSREPORT.fnGetDeptListCompletedEventArgs>(objReport3_fnGetDeptListCompleted);
            //objReport4.fnGetTBUSERS_BY_DEPT_IDCompleted += new EventHandler<WSREPORT.fnGetTBUSERS_BY_DEPT_IDCompletedEventArgs>(objReport4_fnGetTBUSERS_BY_DEPT_IDCompleted);
      
            //cbxDept1.SelectionChanged += new SelectionChangedEventHandler(cbxDept1_SelectionChanged);
            //cbxDept2.SelectionChanged += new SelectionChangedEventHandler(cbxDept2_SelectionChanged);
            //cbxDept3.SelectionChanged += new SelectionChangedEventHandler(cbxDept3_SelectionChanged);
            //cbxUser.SelectionChanged += new SelectionChangedEventHandler(cbxUser_SelectionChanged);

            //objReport1.fnGetDeptListAsync(intCbx1);
            //objReport2.fnGetDeptListAsync(intCbx2);
            //objReport3.fnGetDeptListAsync(intCbx3);
            //objReport4.fnGetTBUSERS_BY_DEPT_IDAsync(intCbx4.ToString());

            //查詢所有使用者資料
            client.QUERY_TBUSERS_ALLCompleted += new EventHandler<QUERY_TBUSERS_ALLCompletedEventArgs>(client_QUERY_TBUSERS_ALLCompleted);
            client.QUERY_TBUSERS_ALLAsync();
            BusyMsg.IsBusy = true;  //關閉忙碌的Loading訊息
        }
               
        #region 實作

        //實作-查詢節目或短帶製作人資料(避免重複)
        void client_QUERY_TBPROG_PRODUCER_CHECKCompleted(object sender, QUERY_TBPROG_PRODUCER_CHECKCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    if (e.Result == "0")
                    {
                        //新增節目或短帶製作人資料
                        client.INSERT_TBPROG_PRODUCERAsync(FormToClass_PROGPRODUCER());
                    }
                    else
                    {
                        MessageBox.Show("資料庫已有重複資料，請檢查後重新輸入！", "提示訊息", MessageBoxButton.OK);
                    }
                }
            }
        }

        //實作-新增節目或短帶製作人資料
        void client_INSERT_TBPROG_PRODUCERCompleted(object sender, INSERT_TBPROG_PRODUCERCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    MessageBox.Show("新增製作人資料「" + m_IDName + "」成功！", "提示訊息", MessageBoxButton.OK);

                    if (m_Continue == false)          //讓使用者可以一直新增同樣內容的資料
                        this.DialogResult = true; 
                }
                else
                {
                    MessageBox.Show("新增製作人資料「" + m_IDName + "」失敗！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
        }

        //實作-查詢所有使用者資料
        void client_QUERY_TBUSERS_ALLCompleted(object sender, QUERY_TBUSERS_ALLCompletedEventArgs e)
        {
             BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                ListUserAll = e.Result;
                acbMenID.ItemsSource = ListUserAll; //繫結AutoComplete按鈕
            }
            else
                MessageBox.Show("查詢使用者資料異常！", "提示訊息", MessageBoxButton.OK);            
        }

        #endregion

        #region 資料查詢結束

        void objReport1_fnGetDeptListCompleted(object sender, WSREPORT.fnGetDeptListCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result.Count == 0)
                {
                    MessageBox.Show("查詢單位時發生錯誤");
                    cbxDept2.ItemsSource = null;
                    cbxDept3.ItemsSource = null;
                    return;
                }
                else
                {
                    deptlist1.Clear();

                    foreach (WSREPORT.DeptStruct dept in e.Result)
                    {
                        deptlist1.Add(new MyDept(dept.FSDpeName_ChtName, dept.FNDEP_ID));
                    }

                    cbxDept1.ItemsSource = deptlist1;
                }
            }
        }

        void objReport2_fnGetDeptListCompleted(object sender, WSREPORT.fnGetDeptListCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            deptlist2.Clear();

            cbxDept2.ItemsSource = null;
            cbxDept3.ItemsSource = null;
            cbxUser.ItemsSource = null;

            if (e.Error == null)
            {
                if (e.Result.Count == 0)
                {
                    //MessageBox.Show("查不到子單位");
                    cbxDept3.ItemsSource = null;
                    return;
                }
                else
                {
                    if (intCbx2 != -1)
                    {
                        foreach (WSREPORT.DeptStruct dept in e.Result)
                        {
                            deptlist2.Add(new MyDept(dept.FSDpeName_ChtName, dept.FNDEP_ID));
                        }
                    }

                    cbxDept2.ItemsSource = deptlist2;
                }
            }
        }

        void objReport3_fnGetDeptListCompleted(object sender, WSREPORT.fnGetDeptListCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            deptlist3.Clear();

            cbxDept3.ItemsSource = null;
            cbxUser.ItemsSource = null;

            if (e.Error == null)
            {
                if (e.Result.Count == 0)
                {
                    //MessageBox.Show("查不到子單位");
                    return;
                }
                else
                {
                    if (intCbx3 != -1)
                    {
                        foreach (WSREPORT.DeptStruct dept in e.Result)
                        {
                            deptlist3.Add(new MyDept(dept.FSDpeName_ChtName, dept.FNDEP_ID));
                        }
                    }

                    cbxDept3.ItemsSource = deptlist3;
                }
            }
        }

        void objReport4_fnGetTBUSERS_BY_DEPT_IDCompleted(object sender, WSREPORT.fnGetTBUSERS_BY_DEPT_IDCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (cbxDept2.SelectedIndex <= 0)
            {
                return;
            }
            else if (refreshok)
            {
                return;
            }
            else
            {
                refreshok = true;
            }

            userlist4.Clear();

            cbxUser.ItemsSource = null;
            if (e.Error == null)
            {
                if (e.Result.Count == 0)
                {
                    //MessageBox.Show("此單位查無人員清單");
                    return;
                }
                else
                {  
                        foreach (WSREPORT.UserStruct user in e.Result)
                        {
                            String _Name = user.FSUPPERDEPT_CHTNAME + " \\ " +
                                         ((user.FSDEP_CHTNAME.Trim() == "") ? "" : (user.FSDEP_CHTNAME.Trim() + " \\ ")) +
                                         ((user.FSGROUP_CHTNAME.Trim() == "") ? "" : (user.FSGROUP_CHTNAME.Trim() + " \\ ")) +
                                         user.FSUSER_ID + "-" + user.FSUSER_ChtName;

                            userlist4.Add(new MyUser(_Name, user.FSUSER_ID));
                        } 

                    cbxUser.ItemsSource = userlist4;
                }
            }
        }

        #endregion

        #region 下拉選單變更

        void cbxDept1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            refreshok = false;
            intCbx2 = ((MyDept)cbxDept1.SelectedItem).id;
            objReport2.fnGetDeptListAsync(intCbx2);
            objReport4.fnGetTBUSERS_BY_DEPT_IDAsync(intCbx2.ToString());
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }

        void cbxDept2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            refreshok = false;   

            if (cbxDept2.Items.Count != 0)
            {
                intCbx3 = ((MyDept)cbxDept2.SelectedItem).id;
                objReport3.fnGetDeptListAsync(intCbx3);
            }
            else
            {
                cbxDept3.ItemsSource = null;
            }
            objReport4.fnGetTBUSERS_BY_DEPT_IDAsync(intCbx3.ToString());
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }

        void cbxDept3_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            refreshok = false;

            if (cbxDept3.Items.Count != 0)
            {
                intCbx4 = ((MyDept)cbxDept3.SelectedItem).id;
            }
            //else
            //{
            //    cbxUser.ItemsSource = null;
            //}
            objReport4.fnGetTBUSERS_BY_DEPT_IDAsync(intCbx4.ToString());
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }

        void cbxUser_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //throw new NotImplementedException();
        }

        #endregion

        #region 檢查畫面資料及搬值

        private Class_PROGPRODUCER FormToClass_PROGPRODUCER()
        {
            Class_PROGPRODUCER obj = new Class_PROGPRODUCER();

            obj.FSPRO_TYPE = m_strType.Trim();                                      //類型
            obj.FSID = m_strID.Trim();                                              //編碼
            m_IDName = tbxPROG_NAME.Text.ToString().Trim();                         //暫存本次新增的名稱，為了秀給使用者看結果用
   
            //obj.FSPRODUCER = ((MyUser)cbxUser.SelectedItem).id.ToString();        //將下拉式選單改成AutoComplete作法
            obj.FSPRODUCER = ((UserStruct)(acbMenID.SelectedItem)).FSUSER_ID.Trim();

            obj.FSCREATED_BY = UserClass.userData.FSUSER_ID.ToString();             //建檔者
            obj.FSUPDATED_BY = UserClass.userData.FSUSER_ID.ToString();             //修改者

            m_Queryobj = obj; 
            return obj;
        }

        private Boolean Check_PROGPRODUCERData()    //檢查畫面上的欄位是否都填妥
        {
            StringBuilder strMessage = new StringBuilder();

            if (m_strType.Trim() == "")
                strMessage.AppendLine("請選擇「類型」");

            if (m_strID.Trim() == "")
                strMessage.AppendLine("請選擇「節目名稱或短帶名稱」");

            //將下拉式選單改成AutoComplete作法
            //if (cbxUser.SelectedIndex == -1)
            //    strMessage.AppendLine("請選擇「製作人」欄位");

            if (acbMenID.Text.Trim() == "")
                strMessage.AppendLine("請輸入「製作人」欄位");
            else if (acbMenID.Text.Trim() != "" && acbMenID.SelectedItem == null)
                strMessage.AppendLine("請輸入存在的人員資料");

            if (strMessage.ToString().Trim() == "")
                return true;
            else
            {
                MessageBoxResult result = MessageBox.Show(strMessage.ToString(), "提示訊息", MessageBoxButton.OK);
                return false;
            }
        }

        #endregion   

        //確定
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (Check_PROGPRODUCERData() == false)      //檢查畫面上的欄位是否都填妥
                return;

            m_Continue = false;

            //檢查是否有相同的資料，若有就無法新增
            client.QUERY_TBPROG_PRODUCER_CHECKAsync(FormToClass_PROGPRODUCER());
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }

        //繼續新增
        private void ContinueButton_Click(object sender, RoutedEventArgs e)
        {
            if (Check_PROGPRODUCERData() == false)      //檢查畫面上的欄位是否都填妥
                return;

            m_Continue = true;
            //檢查是否有相同的資料，若有就無法新增
            client.QUERY_TBPROG_PRODUCER_CHECKAsync(FormToClass_PROGPRODUCER());
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
      
        //選取節目時
        private void rdbProg_Checked(object sender, RoutedEventArgs e)
        {
            if (rdbProg.IsChecked == true)
            {
                m_strType = "G";
                tbPROG_NANE.Text = "節目名稱";
                tbxPROG_NAME.Text = "";
                tbxPROG_NAME.Tag = "";
                m_strID = "";              
            }
        }

        //選取短帶時
        private void rdbPromo_Checked(object sender, RoutedEventArgs e)
        {
            if (rdbPromo.IsChecked == true)
            {
                m_strType = "P";
                tbPROG_NANE.Text = "短帶名稱";
                tbxPROG_NAME.Text = "";
                tbxPROG_NAME.Tag = "";
            }
        }

        //開啟資料查詢畫面
        private void btnPROG_Click(object sender, RoutedEventArgs e)
        {
            if (rdbProg.IsChecked == true)
            {
                m_strType = "G";
                //PRG100_01
                PROGDATA_VIEW PROGDATA_VIEW_frm = new PROGDATA_VIEW();
                PROGDATA_VIEW_frm.Show();

                PROGDATA_VIEW_frm.Closed += (s, args) =>
                {
                    if (PROGDATA_VIEW_frm.DialogResult == true)
                    {
                        tbxPROG_NAME.Text = PROGDATA_VIEW_frm.strProgName_View;
                        tbxPROG_NAME.Tag = PROGDATA_VIEW_frm.strProgID_View;
                        m_strID = PROGDATA_VIEW_frm.strProgID_View;
                    }
                };
            }
            else if (rdbPromo.IsChecked == true)
            {
                 m_strType = "P" ;
                //PRG800_07
                PRG800_07 PROMO_VIEW_frm = new PRG800_07();
                PROMO_VIEW_frm.Show();

                PROMO_VIEW_frm.Closed += (s, args) =>
                {
                    if (PROMO_VIEW_frm.DialogResult == true)
                    {
                        tbxPROG_NAME.Text = PROMO_VIEW_frm.strPromoName_View;
                        tbxPROG_NAME.Tag = PROMO_VIEW_frm.strPromoID_View;
                        m_strID = PROMO_VIEW_frm.strPromoID_View;
                    }
                };
            }
        }

        //自動去比對資料庫，若有人員就顯示資料
        private void acbMenID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (acbMenID.SelectedItem == null)         
                tbxNote.Text = "";
            else
                tbxNote.Text = ((UserStruct)(acbMenID.SelectedItem)).FSTITLE_NAME.Trim();          
        }

    }
}

