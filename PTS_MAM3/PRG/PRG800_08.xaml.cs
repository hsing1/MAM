﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSPROMO;
using PTS_MAM3.ProgData;
using DataGridDCTest;

namespace PTS_MAM3.PRG
{
    public partial class PRG800_08 : ChildWindow
    {
        WSPROMOSoapClient client = new WSPROMOSoapClient();  //產生新的代理類別

        public string strActID_View = "";     //選取的活動編號
        public string strActIDName_View = "";   //選取的活動名稱

        public PRG800_08()
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面
        }

        void InitializeForm() //初始化本頁面
        {
            //查詢活動資料By活動名稱
            client.GetTBPGM_ACTION_CODE_BYNAMECompleted += new EventHandler<GetTBPGM_ACTION_CODE_BYNAMECompletedEventArgs>(client_GetTBPGM_ACTION_CODE_BYNAMECompleted);
            client.GetTBPGM_ACTION_CODE_BYNAMEAsync(""); //第一次傳空值查全部
            LoadBusy();
        }

        void LoadBusy()
        {
            BusyMsg.IsBusy = true;  //關閉忙碌的Loading訊息
            btnSearch.IsEnabled = false;
            btnAll.IsEnabled = false;
            tbxPROMONAME.IsEnabled = false;
        }

        void LoadBusyUnLock()
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息
            btnSearch.IsEnabled = true;
            btnAll.IsEnabled = true;
            tbxPROMONAME.IsEnabled = true;
            tbxPROMONAME.Focus();
        }

        //實作-活動資料By活動名稱
        void client_GetTBPGM_ACTION_CODE_BYNAMECompleted(object sender, GetTBPGM_ACTION_CODE_BYNAMECompletedEventArgs e)
        {
            LoadBusyUnLock();

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    this.DGActList.DataContext = e.Result;

                    if (this.DGActList.DataContext != null)
                    {
                        DGActList.SelectedIndex = 0;
                    }
                }
                else
                {
                    MessageBox.Show("查無活動資料，請重新輸入條件查詢！", "提示訊息", MessageBoxButton.OK);
                    return;
                }
            }   
        }      
           
        //確定
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGActList.SelectedItem == null)
            {
                MessageBox.Show("請選擇活動資料", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else
            {
                strActID_View = ((Class_CODE)DGActList.SelectedItem).ID.ToString();
                strActIDName_View = ((Class_CODE)DGActList.SelectedItem).NAME.ToString();            
                this.DialogResult = true;
            }
        }

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
       
        //查詢
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            if (tbxPROMONAME.Text.Trim() != "")
            {
                //查詢節目主檔資料
                client.GetTBPGM_ACTION_CODE_BYNAMEAsync(tbxPROMONAME.Text.Trim());
                LoadBusy();
            }
            else
                MessageBox.Show("請輸入條件查詢！", "提示訊息", MessageBoxButton.OK);        
        }

        //重新查詢
        private void btnAll_Click(object sender, RoutedEventArgs e)
        {
            tbxPROMONAME.Text = "";

            //取得全部節目資料
            client.GetTBPGM_ACTION_CODE_BYNAMEAsync("");
            LoadBusy();
        }

        //連點事件
        private void RowDoubleClick(object sender, DataGridDCTest.DataGridRowClickedArgs e)
        {
            DoubleClickDataGrid dcdg = sender as DoubleClickDataGrid;

            strActID_View = ((Class_CODE)DGActList.SelectedItem).ID.ToString();
            strActIDName_View = ((Class_CODE)DGActList.SelectedItem).NAME.ToString();
            this.DialogResult = true;
        }

        //輸入完查詢內容後按下enter即可查詢
        private void tbxPROMONAME_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                btnSearch_Click(sender, e);
            }
        }

    }
}

