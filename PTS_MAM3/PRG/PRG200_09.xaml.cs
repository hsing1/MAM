﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.PROG_M;
using PTS_MAM3.WSPROG_D;
using PTS_MAM3.WSPROG_M;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace PTS_MAM3.ProgData
{
    public partial class PRG200_09 : ChildWindow
    {
        WSPROG_DSoapClient client = new WSPROG_DSoapClient();      //產生新的代理類別
        WSPROG_MSoapClient clientPROGM = new WSPROG_MSoapClient();
        Class_PROGD m_FormData = new Class_PROGD();
        List<Class_CODE> m_CodeDataBuyD = new List<Class_CODE>();  //代碼檔集合(外購類別細項) 
        string m_strProgName = "";                                 //暫存節目名稱
        string m_strProgID = "";                                   //暫存節目編號
        string m_strEpisode = "";                                  //暫存集別

        public PRG200_09(Class_PROGD FormData)
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面
            m_FormData = FormData;
            ClassToForm();
            m_strProgName = FormData.FSPROG_ID_NAME.Trim();     //暫存節目名稱
            m_strProgID = FormData.FSPROG_ID.Trim();            //暫存節目編號
            m_strEpisode = FormData.SHOW_FNEPISODE.Trim();      //暫存集別
            checkPermission();                                  //檢查修改權限
        }

        void InitializeForm() //初始化本頁面
        {
            //下載代碼檔
            clientPROGM.fnGetTBPROG_M_CODECompleted += new EventHandler<fnGetTBPROG_M_CODECompletedEventArgs>(clientPROGM_fnGetTBPROG_M_CODECompleted);           
            clientPROGM.fnGetTBPROG_M_CODEAsync();

            //下載外購類別細項代碼檔
            clientPROGM.fnGetTBPROG_M_PROGBUYD_CODECompleted += new EventHandler<fnGetTBPROG_M_PROGBUYD_CODECompletedEventArgs>(client_fnGetTBPROG_M_PROGBUYD_CODECompleted);
            clientPROGM.fnGetTBPROG_M_PROGBUYD_CODEAsync();

            //整批修改節目子集資料
            client.UPDATE_TBPROGD_ALLCompleted += new EventHandler<UPDATE_TBPROGD_ALLCompletedEventArgs>(client_UPDATE_TBPROGD_ALLCompleted);
        }
              
        private void ClassToForm()  //將前一頁點選的節目子資料繫集到畫面上
        {
            this.gdPROG_D.DataContext = m_FormData;
        }

        #region 實作

        //實作-整批修改節目子集資料
        void client_UPDATE_TBPROGD_ALLCompleted(object sender, UPDATE_TBPROGD_ALLCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result.bolResult == true)
                {
                    MessageBox.Show("整批修改節目子集資料「" + m_strProgName.ToString() + "-" + m_strEpisode + "集~" + tbxEPISODE_END.Text.Trim() + "集」成功！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("整批修改節目子集資料「" + m_strProgName.ToString() + "-" + m_strEpisode + "集~" + tbxEPISODE_END.Text.Trim() + "集」失敗！\n失敗原因：" + e.Result.strErrorMsg, "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
        }     

        //實作-下載外購類別細項代碼檔
        void client_fnGetTBPROG_M_PROGBUYD_CODECompleted(object sender, fnGetTBPROG_M_PROGBUYD_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {                  
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        ComboBoxItem cbiPROGBUYD = new ComboBoxItem();
                        cbiPROGBUYD.Content = CodeData.NAME;
                        cbiPROGBUYD.Tag = CodeData.ID;
                        cbiPROGBUYD.DataContext = CodeData.IDD;
                        this.cbPROGBUYDID.Items.Add(cbiPROGBUYD);   //先都產生在隱藏起來的細項代碼combobox裡                   
                        m_CodeDataBuyD.Add(CodeData);
                    }
                }
            }
        }

        //實作-下載所有的代碼檔
        void clientPROGM_fnGetTBPROG_M_CODECompleted(object sender, fnGetTBPROG_M_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    //外購類別代碼，空白要自己帶入
                    ComboBoxItem cbiPROGBUYNull = new ComboBoxItem();
                    cbiPROGBUYNull.Content = " ";
                    cbiPROGBUYNull.Tag = "";
                    this.cbPROGBUYID.Items.Add(cbiPROGBUYNull);                    

                    //因為行銷類別是節目管理系統的資料庫因此空白要自己帶入
                    ComboBoxItem cbiPROGSALENull = new ComboBoxItem();
                    cbiPROGSALENull.Content = " ";
                    cbiPROGSALENull.Tag = "";
                    this.cbPROGSALEID.Items.Add(cbiPROGSALENull);
                    
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            case "TBZCHANNEL":       //頻道別代碼
                                ComboBoxItem cbiCHANNEL = new ComboBoxItem();
                                cbiCHANNEL.Content = CodeData.NAME;
                                cbiCHANNEL.Tag = CodeData.ID;
                                this.cbCHANNEL_ID.Items.Add(cbiCHANNEL);
                                break;
                            case "TBZSHOOTSPEC":       //拍攝規格代碼
                                CheckBox cbSHOOTSPEC = new CheckBox();
                                cbSHOOTSPEC.Content = CodeData.NAME;
                                cbSHOOTSPEC.Tag = CodeData.ID;
                                listSHOOTSPEC.Items.Add(cbSHOOTSPEC);
                                break;
                            case "TBZPROGGRADE":      //節目分級代碼
                                ComboBoxItem cbiPROGGRADE = new ComboBoxItem();
                                cbiPROGGRADE.Content = CodeData.NAME;
                                cbiPROGGRADE.Tag = CodeData.ID;
                                this.cbPROGGRADEID.Items.Add(cbiPROGGRADE);
                                break;
                            case "TBZPROGSRC":      //節目來源代碼
                                ComboBoxItem cbiPROGSRC = new ComboBoxItem();
                                cbiPROGSRC.Content = CodeData.NAME;
                                cbiPROGSRC.Tag = CodeData.ID;
                                this.cbPROGSRCID.Items.Add(cbiPROGSRC);
                                break;
                            case "TBZPROGATTR":      //內容屬性代碼
                                ComboBoxItem cbiPROGATTR = new ComboBoxItem();
                                cbiPROGATTR.Content = CodeData.NAME;
                                cbiPROGATTR.Tag = CodeData.ID;
                                this.cbPROGATTRID.Items.Add(cbiPROGATTR);
                                break;
                            case "TBZPROGAUD":      //目標觀眾代碼
                                ComboBoxItem cbiPROGAUD = new ComboBoxItem();
                                cbiPROGAUD.Content = CodeData.NAME;
                                cbiPROGAUD.Tag = CodeData.ID;
                                this.cbPROGAUDID.Items.Add(cbiPROGAUD);
                                break;
                            case "TBZPROGTYPE":      //表現方式代碼
                                ComboBoxItem cbiPROGTYPE = new ComboBoxItem();
                                cbiPROGTYPE.Content = CodeData.NAME;
                                cbiPROGTYPE.Tag = CodeData.ID;
                                this.cbPROGTYPEID.Items.Add(cbiPROGTYPE);
                                break;
                            case "TBZPROGLANG":      //語言代碼
                                ComboBoxItem cbiPROGLANG1 = new ComboBoxItem();
                                cbiPROGLANG1.Content = CodeData.NAME;
                                cbiPROGLANG1.Tag = CodeData.ID;
                                this.cbPROGLANGID1.Items.Add(cbiPROGLANG1);  //主聲道

                                ComboBoxItem cbiPROGLANG2 = new ComboBoxItem();
                                cbiPROGLANG2.Content = CodeData.NAME;
                                cbiPROGLANG2.Tag = CodeData.ID;
                                this.cbPROGLANGID2.Items.Add(cbiPROGLANG2);  //副聲道
                                break;
                            case "TBZPROGBUY":       //外購類別代碼
                                ComboBoxItem cbiPROGBUY = new ComboBoxItem();
                                cbiPROGBUY.Content = CodeData.NAME;
                                cbiPROGBUY.Tag = CodeData.ID;
                                this.cbPROGBUYID.Items.Add(cbiPROGBUY);
                                break;
                            case "TBZPROGSALE":       //行銷類別代碼
                                ComboBoxItem cbiPROGSALE = new ComboBoxItem();
                                cbiPROGSALE.Content = CodeData.NAME;
                                cbiPROGSALE.Tag = CodeData.ID;
                                this.cbPROGSALEID.Items.Add(cbiPROGSALE);
                                break;
                            case "TBZPROGSPEC":       //節目規格代碼
                                CheckBox cbPROGSPEC = new CheckBox();
                                cbPROGSPEC.Content = CodeData.NAME;
                                cbPROGSPEC.Tag = CodeData.ID;
                                listPROGSPEC.Items.Add(cbPROGSPEC);
                                break;
                            case "TBZPROGNATION":       //來源國家    2012/11/22 kyle                            
                                ComboBoxItem cbNATION = new ComboBoxItem();
                                cbNATION.Content = CodeData.NAME;
                                cbNATION.Tag = CodeData.ID;
                                cbNATION_ID.Items.Add(cbNATION);
                                break;
                            default:
                                break;
                        }
                    }

                    //比對代碼檔
                    compareCode_FSPROGGRADEID(m_FormData.FSPROGGRADEID.ToString().Trim());
                    compareCode_FSPROGATTRID(m_FormData.FSPROGATTRID.ToString().Trim());
                    compareCode_FSPROGSRCID(m_FormData.FSPROGSRCID.ToString().Trim());
                    compareCode_FSPROGTYPEID(m_FormData.FSPROGTYPEID.ToString().Trim());
                    compareCode_FSPROGAUDID(m_FormData.FSPROGAUDID.ToString().Trim());
                    compareCode_FSPROGLANGID1(m_FormData.FSPROGLANGID1.ToString().Trim());
                    compareCode_FSPROGLANGID2(m_FormData.FSPROGLANGID2.ToString().Trim());
                    compareCode_FSPROGBUYID(m_FormData.FSPROGBUYID.ToString().Trim());
                    compareCode_FSPROGBUYDID(m_FormData.FSPROGBUYDID.ToString().Trim());
                    compareCode_FSCHANNEL_ID(m_FormData.FSCHANNEL_ID.ToString().Trim());
                    compareCode_FSSHOOTSPEC(CheckList(m_FormData.FSSHOOTSPEC.ToString().Trim()));
                    compareCode_FSPROGSALEID(m_FormData.FSPROGSALEID.ToString().Trim());
                    compareCode_FSPROGSPEC(CheckList(m_FormData.FSPROGSPEC.ToString().Trim()));
                    compareCode_CR_TYPE(m_FormData.FSCR_TYPE.ToString().Trim());
                    cbNATION_ID.SelectedItem = cbNATION_ID.Items.Where(S => ((ComboBoxItem)S).Tag.ToString() == m_FormData.FSPROGNATIONID).FirstOrDefault();
                    //if (m_FormData.FDEXPIRE_DATE.ToShortDateString() != "1900/1/1")
                    if (m_FormData.FDEXPIRE_DATE > Convert.ToDateTime("1900/1/5"))
                    {
                        dpEXPIRE.Text = m_FormData.FDEXPIRE_DATE.ToShortDateString();
                    }
                    if (m_FormData.FSEXPIRE_DATE_ACTION == "Y")
                        cbDELETE.IsChecked = true;
                    else
                        cbDELETE.IsChecked = false;
                }
            }
        }        
              
        private string[] CheckList(string strChannel)
        {
            char[] delimiterChars = { ';' };
            string[] words = strChannel.Split(delimiterChars);
            return words;
        }       

        //選擇外購類別，要判斷顯示哪些外購類別細項
        private void cbBUY_ID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.cbPROGBUYDIDS.Items.Clear();     //使用前先清空
            string strBUYID = ""; //外購類別大項
            ComboBox cb = sender as ComboBox;
            ComboBoxItem cbm = (ComboBoxItem)cb.SelectedItem;
            strBUYID = cbm.Tag.ToString();

            for (int i = 0; i < cbPROGBUYDID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGBUYDID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strBUYID))
                {
                    ComboBoxItem cbiPROGBUYDS = new ComboBoxItem();
                    cbiPROGBUYDS.Content = getcbi.Content;
                    cbiPROGBUYDS.Tag = getcbi.DataContext;
                    this.cbPROGBUYDIDS.Items.Add(cbiPROGBUYDS);
                }
            }
        }

        #endregion

        #region 比對代碼檔
        
        //比對代碼檔_節目型態(內容屬性)
        void compareCode_FSPROGATTRID(string strID)
        {
            for (int i = 0; i < cbPROGATTRID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGATTRID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGATTRID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_節目分級
        void compareCode_FSPROGGRADEID(string strID)
        {
            for (int i = 0; i < cbPROGGRADEID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGGRADEID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGGRADEID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_節目來源
        void compareCode_FSPROGSRCID(string strID)
        {
            for (int i = 0; i < cbPROGSRCID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGSRCID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGSRCID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_表現方式
        void compareCode_FSPROGTYPEID(string strID)
        {
            for (int i = 0; i < cbPROGTYPEID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGTYPEID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGTYPEID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_目標觀眾
        void compareCode_FSPROGAUDID(string strID)
        {
            for (int i = 0; i < cbPROGAUDID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGAUDID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGAUDID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_主聲道
        void compareCode_FSPROGLANGID1(string strID)
        {
            for (int i = 0; i < cbPROGLANGID1.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGLANGID1.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGLANGID1.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_副聲道
        void compareCode_FSPROGLANGID2(string strID)
        {
            for (int i = 0; i < cbPROGLANGID2.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGLANGID2.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGLANGID2.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_外購類別大類
        void compareCode_FSPROGBUYID(string strID)
        {
            for (int i = 0; i < cbPROGBUYID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGBUYID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGBUYID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_外購類別細類
        void compareCode_FSPROGBUYDID(string strID)
        {
            for (int i = 0; i < cbPROGBUYDIDS.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGBUYDIDS.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGBUYDIDS.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_拍攝規格
        void compareCode_FSSHOOTSPEC(string[] ListTEST_Send)
        {
            for (int i = 0; i < ListTEST_Send.Length - 1; i++)
            {
                for (int j = 0; j < listSHOOTSPEC.Items.Count; j++)
                {
                    CheckBox getcki = (CheckBox)listSHOOTSPEC.Items.ElementAt(j);

                    if (getcki.Tag.ToString().Trim().Equals(ListTEST_Send[i]))
                    {
                        getcki.IsChecked = true;
                        break;
                    }
                }
            }
        }

        //比對代碼檔_節目規格
        void compareCode_FSPROGSPEC(string[] ListTEST_Send)
        {
            for (int i = 0; i < ListTEST_Send.Length - 1; i++)
            {
                for (int j = 0; j < listPROGSPEC.Items.Count; j++)
                {
                    CheckBox getcki = (CheckBox)listPROGSPEC.Items.ElementAt(j);

                    if (getcki.Tag.ToString().Trim().Equals(ListTEST_Send[i]))
                    {
                        getcki.IsChecked = true;
                        break;
                    }
                }
            }
        }

        //比對代碼檔_頻道別
        void compareCode_FSCHANNEL_ID(string strID)
        {
            for (int i = 0; i < cbCHANNEL_ID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbCHANNEL_ID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbCHANNEL_ID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_行銷類別
        void compareCode_FSPROGSALEID(string strID)
        {
            for (int i = 0; i < cbPROGSALEID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGSALEID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGSALEID.SelectedIndex = i;
                    break;
                }
            }
        }

        //選擇外購類別，要判斷顯示哪些外購類別細項
        private void cbPROGBUYID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.cbPROGBUYDIDS.Items.Clear();     //使用前先清空
            string strBUYID = ""; //外購類別大項
            ComboBox cb = sender as ComboBox;
            ComboBoxItem cbm = (ComboBoxItem)cb.SelectedItem;
            strBUYID = cbm.Tag.ToString();

            for (int i = 0; i < cbPROGBUYDID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGBUYDID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strBUYID))
                {
                    ComboBoxItem cbiPROGBUYDS = new ComboBoxItem();
                    cbiPROGBUYDS.Content = getcbi.Content;
                    cbiPROGBUYDS.Tag = getcbi.DataContext;
                    this.cbPROGBUYDIDS.Items.Add(cbiPROGBUYDS);
                }
            }
        }

        //比對代碼檔_著作權類型
        void compareCode_CR_TYPE(string strID)
        {
            for (int i = 0; i < cbCR_TYPE.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbCR_TYPE.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbCR_TYPE.SelectedIndex = i;
                    break;
                }
            }
        }

        #endregion

        #region 檢查畫面資料及搬值

        //整批修改節目子集資料
        private void FormToClass_PROGD()
        {
            Boolean bolPGDNAME = false;
            Boolean bolPGDENAME = false;
            Boolean bolLENGTH = false;
            Boolean bolPROGGRADEID = false;
            Boolean bolPROGGRADE = false;
            Boolean bolCONTENT = false;
            Boolean bolcbMEMO = false;
            Boolean bolCR_TYPE = false;
            Boolean bolCR_NOTE = false;

            Boolean bolPROGSRCID = false;
            Boolean bolPROGATTRID = false;
            Boolean bolPROGAUDID = false;
            Boolean bolPROGTYPEID = false;
            Boolean bolPROGLANGID1 = false;
            Boolean bolPROGLANGID2 = false;
            Boolean bolPROGBUYID = false;
            Boolean bolPROGSALEID = false;

            Boolean bolPROGSPEC = false;
            Boolean bolSHOOTSPEC = false;
            Boolean bolPROGNATIONID = false;
            Boolean bolEXPIRE_DATE = false;
            Boolean bolEXPIRE_DATE_ACTION = false;
            Boolean bolPRDYEAR = false;

            short FNEPISODE_END = 0;
            Class_PROGD obj = new Class_PROGD();

            obj.Origin_FDEXPIRE_DATE = m_FormData.FDEXPIRE_DATE;
            obj.Origin_FSEXPIRE_DATE_ACTION = m_FormData.FSEXPIRE_DATE_ACTION;

            obj.FSPROG_ID = tbxtProgID.Text.ToString();                                     //節目編號
            obj.FNEPISODE = Convert.ToInt16(tbxEPISODE.Text.ToString().Trim());             //開始集別
            FNEPISODE_END = Convert.ToInt16(tbxEPISODE_END.Text.ToString().Trim());         //結束集別

            if (cbPGDNAME.IsChecked == true)
            {
                obj.FSPGDNAME = tbxPGDNAME.Text.ToString().Trim();                          //子集名稱
                bolPGDNAME = true;
            }
            else
                obj.FSPGDNAME = "";

            if (cbPGDENAME.IsChecked == true)
            {
                obj.FSPGDENAME = tbxPGDENAME.Text.ToString().Trim();                        //子集外文名稱
                bolPGDENAME = true;
            }
            else
                obj.FSPGDENAME = "";

            if (cbLENGTH.IsChecked == true)
            {
                if (tbxLENGTH.Text.ToString().Trim() == "")                                 //時長            
                    obj.FNLENGTH = 0;
                else
                    obj.FNLENGTH = Convert.ToInt16(tbxLENGTH.Text.ToString().Trim());

                bolLENGTH = true;
            }
            else
                obj.FNLENGTH = 0;

            if (cbkPROGGRADEID.IsChecked == true)      //節目分級
            {
                bolPROGGRADEID = true;
                obj.FSPROGGRADEID = ((ComboBoxItem)cbPROGGRADEID.SelectedItem).Tag.ToString();
            }
            else
                obj.FSPROGGRADEID = "";

            if (cbPROGGRADE.IsChecked == true)
            {
                obj.FSPROGGRADE = tbxPROGGRADE.Text.ToString().Trim();                      //非正常分級的說明
                bolPROGGRADE = true;
            }
            else
                obj.FSPROGGRADE = "";

            if (cbCONTENT.IsChecked == true)
            {
                obj.FSCONTENT = tbxCONTENT.Text.ToString().Trim();                          //內容簡述
                bolCONTENT = true;
            }
            else
                obj.FSCONTENT = "";

            if (cbMemo.IsChecked == true)
            {
                obj.FSMEMO = tbxMEMO.Text.ToString().Trim();                                //備註
                bolcbMEMO = true;
            }
            else
                obj.FSMEMO = "";

            if (cbkCR_TYPE.IsChecked == true)              //著作權類型
            {
                bolCR_TYPE = true;

                if (cbCR_TYPE.SelectedIndex != -1)
                    obj.FSCR_TYPE = ((ComboBoxItem)cbCR_TYPE.SelectedItem).Tag.ToString();
                else
                    obj.FSCR_TYPE = "";
            }
            else
                obj.FSCR_TYPE = "";

            if (cbkCR_TYPE.IsChecked == true)
            {
                obj.FSCR_NOTE = tbxCR_NOTE.Text.ToString().Trim();                          //著作權備註
                bolCR_NOTE = true;
            }
            else
                obj.FSCR_NOTE = "";

            if (cbxPROGSRCID.IsChecked == true)          //節目來源
            {
                obj.FSPROGSRCID = ((ComboBoxItem)cbPROGSRCID.SelectedItem).Tag.ToString();
                bolPROGSRCID = true;
            }
            else
                obj.FSPROGSRCID = "";

            if (cbxPROGATTRID.IsChecked == true)        //節目型態
            {
                obj.FSPROGATTRID = ((ComboBoxItem)cbPROGATTRID.SelectedItem).Tag.ToString();
                bolPROGATTRID = true;
            }
            else
                obj.FSPROGATTRID = "";

            if (cbxPROGAUDID.IsChecked == true)          //目標觀眾
            {
                obj.FSPROGAUDID = ((ComboBoxItem)cbPROGAUDID.SelectedItem).Tag.ToString();
                bolPROGAUDID = true;
            }
            else
                obj.FSPROGAUDID = "";

            if (cbxPROGTYPEID.IsChecked == true)        //表現方式
            {
                obj.FSPROGTYPEID = ((ComboBoxItem)cbPROGTYPEID.SelectedItem).Tag.ToString();
                bolPROGTYPEID = true;
            }
            else
                obj.FSPROGTYPEID = "";

            if (cbxPROGLANGID1.IsChecked == true)      //主聲道
            {
                obj.FSPROGLANGID1 = ((ComboBoxItem)cbPROGLANGID1.SelectedItem).Tag.ToString();
                bolPROGLANGID1 = true;
            }
            else
                obj.FSPROGLANGID1 = "";

            if (cbxPROGLANGID2.IsChecked == true)      //副聲道
            {
                obj.FSPROGLANGID2 = ((ComboBoxItem)cbPROGLANGID2.SelectedItem).Tag.ToString();
                bolPROGLANGID2 = true;
            }
            else
                obj.FSPROGLANGID2 = "";

            if (cbxPROGBUYID.IsChecked == true)                                             //外購類別
            {
                obj.FSPROGBUYID = ((ComboBoxItem)cbPROGBUYID.SelectedItem).Tag.ToString();

                if (cbPROGBUYDIDS.SelectedItem == null)
                    obj.FSPROGBUYDID = "";
                else
                    obj.FSPROGBUYDID = ((ComboBoxItem)cbPROGBUYDIDS.SelectedItem).Tag.ToString();
                
                bolPROGBUYID = true;
            }
            else
            {
                obj.FSPROGBUYID = "";
                obj.FSPROGBUYDID = "";
            }

            if (cbxPROGSALEID.IsChecked == true)        //行銷類別
            {
                bolPROGSALEID = true;

                if (cbPROGSALEID.SelectedIndex != -1)
                    obj.FSPROGSALEID = ((ComboBoxItem)cbPROGSALEID.SelectedItem).Tag.ToString();
                else
                    obj.FSPROGSALEID = "";
            }
            else
                obj.FSPROGSALEID = "";

            if (cbPROGSPEC.IsChecked == true)
            {
                obj.FSPROGSPEC = Check_PROGSPECList();                                      //節目規格   
                bolPROGSPEC = true;
            }
            else
                obj.FSPROGSPEC = "";

            if (cbSHOOTSPEC.IsChecked == true)
            {
                obj.FSSHOOTSPEC = Check_SHOOTSPECList();                                    //拍攝規格
                bolSHOOTSPEC = true;
            }
            else
                obj.FSSHOOTSPEC = "";

            if (cb_Modify_NATION_ID.IsChecked == true)
            {
                obj.FSPROGNATIONID = ((ComboBoxItem)(cbNATION_ID.SelectedItem)).Tag.ToString();
                bolPROGNATIONID = true;
            }
            else
                obj.FSPROGNATIONID = "";

            if (cb_Modify_EXPIRE_DATE.IsChecked == true)
            {
                if (dpEXPIRE.Text == "")
                    obj.FDEXPIRE_DATE = Convert.ToDateTime("1900/1/1");
                else
                    obj.FDEXPIRE_DATE = Convert.ToDateTime(dpEXPIRE.Text).AddHours(23).AddMinutes(59);  //到期日期

                bolEXPIRE_DATE = true;
            }

            if (cb_Modify_EXPIRE_DELETE.IsChecked == true)
            {
                if (cbDELETE.IsChecked == true)
                    obj.FSEXPIRE_DATE_ACTION = "Y";
                else
                    obj.FSEXPIRE_DATE_ACTION = "N";

                bolEXPIRE_DATE_ACTION = true;
            }
            else
                obj.FSEXPIRE_DATE_ACTION = "";

            if (cbPRDYEAR.IsChecked == true)
            {
                obj.FSPRDYEAR = tbxPRDYEAR.Text.Trim();
                bolPRDYEAR = true;
            }
            else
                obj.FSPRDYEAR = "";

            obj.FSUPDUSER = UserClass.userData.FSUSER_ID.ToString();                        //修改者

            client.UPDATE_TBPROGD_ALLAsync(obj, FNEPISODE_END, bolPGDNAME, bolPGDENAME, bolLENGTH, bolPROGGRADEID, bolPROGGRADE, bolCONTENT, bolcbMEMO, bolCR_TYPE, bolCR_NOTE,
            bolPROGSRCID, bolPROGATTRID, bolPROGAUDID, bolPROGTYPEID, bolPROGLANGID1, bolPROGLANGID2, bolPROGBUYID, bolPROGSALEID, bolPROGSPEC, bolSHOOTSPEC, bolPROGNATIONID, bolEXPIRE_DATE, bolEXPIRE_DATE_ACTION, bolPRDYEAR);
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }

        private string Check_SHOOTSPECList()    //檢查拍攝規格有哪些
        {
            string strReturn = "";            
            for (int i = 0; i < listSHOOTSPEC.Items.Count; i++)
            {
                CheckBox getckb = (CheckBox)listSHOOTSPEC.Items.ElementAt(i);
                if (getckb.IsChecked == true)
                {
                    strReturn += getckb.Tag.ToString().Trim() + ";";
                }
            }
            return strReturn;
        }

        private string Check_PROGSPECList()    //檢查節目規格有哪些
        {
            string strReturn = "";            
            for (int i = 0; i < listPROGSPEC.Items.Count; i++)
            {
                CheckBox getckb = (CheckBox)listPROGSPEC.Items.ElementAt(i);
                if (getckb.IsChecked == true)
                {
                    strReturn += getckb.Tag.ToString().Trim() + ";";
                }
            }
            return strReturn;
        }

        private Boolean Check_PROGDData()    //檢查畫面上的欄位是否都填妥
        {
            StringBuilder strMessage = new StringBuilder();
            short shortCheck;   //純粹為了判斷是否為數字型態之用
            double intCheck;    //純粹為了判斷是否為數字型態之用
            short shortCheck1 = -1, shortCheck2 = -1;

            if (tbxEPISODE.Text.Trim() != "")
            {
                if (short.TryParse(tbxEPISODE.Text.Trim(), out shortCheck1) == false)
                {
                    strMessage.AppendLine("請檢查「集數(起)」欄位必須為整數數值型態");
                }
                else if (shortCheck1 <= 0)
                {
                    strMessage.AppendLine("請檢查「集數(起)」欄位必須>0");
                }
            }
            else
            {
                strMessage.AppendLine("請檢查「集數(起)」欄位必須輸入值");
            }

            if (tbxEPISODE_END.Text.Trim() != "")
            {
                if (short.TryParse(tbxEPISODE_END.Text.Trim(), out shortCheck2) == false)
                {
                    strMessage.AppendLine("請檢查「集數(迄)」欄位必須為整數數值型態");
                }
                else if (shortCheck2 <= 0)
                {
                    strMessage.AppendLine("請檢查「集數(迄)」欄位必須>0");
                }
            }
            else
            {
                strMessage.AppendLine("請檢查「集數(迄)」欄位必須輸入值");
            }

            if ((shortCheck1 >= 0) && (shortCheck2 >= 0) && (shortCheck1 > shortCheck2))
            {
                strMessage.AppendLine("請檢查「集數(起)」欄位必須小於或等於「集數(迄)」");
            }

            //因為不好讀取該節目的總集數，因此就先不管總集數
            //if (shortCheck2 > m_FormData.FNTOTEPISODE)
            //{
            //    strMessage.AppendLine("請檢查「集數(迄)」欄位必須小於「總集數」，該節目總共「" + ProgMobj.FNTOTEPISODE.ToString() + "」集");
            //}

            if (cbPGDNAME.IsChecked == true && tbxPGDNAME.Text.Trim() == "")
                strMessage.AppendLine("請輸入「子集名稱」欄位");

            //非必填
            //if (cbPGDENAME.IsChecked == true && tbxPGDENAME.Text.Trim() == "")
            //    strMessage.AppendLine("請輸入「子集外文名稱」欄位");

            if (cbLENGTH.IsChecked == true)
            {
                if (tbxLENGTH.Text.Trim() != "")
                {
                    if (short.TryParse(tbxLENGTH.Text.Trim(), out shortCheck) == false)
                        strMessage.AppendLine("請檢查「時長」欄位必須為數值型態");
                }
                //else //非必填
                //    strMessage.AppendLine("請輸入「時長」欄位");              
            }

            if (cbPROGGRADEID.SelectedIndex == -1 && cbkPROGGRADEID.IsChecked == true)
                strMessage.AppendLine("請輸入「節目分級」欄位");

            //非必填
            //if (cbPROGGRADE.IsChecked == true && tbxPROGGRADE.Text.ToString().Trim()=="" )
            //    strMessage.AppendLine("請輸入「非正常分級的說明」欄位");

            //非必填
            //if (cbCONTENT.IsChecked == true && tbxCONTENT.Text.ToString().Trim()=="")
            //    strMessage.AppendLine("請輸入「內容」欄位");

            //非必填
            //if (cbMemo.IsChecked == true && tbxMEMO.Text.ToString().Trim()=="")
            //    strMessage.AppendLine("請輸入「備註」欄位");

            //非必填
            //if (cbCR_TYPE.SelectedIndex == -1 && cbkCR_TYPE.IsChecked == true)
            //    strMessage.AppendLine("請輸入「著作權類型」欄位");

            //非必填
            //if (cbkCR_TYPE.IsChecked == true && tbxCR_NOTE.Text.ToString().Trim() == "")
            //    strMessage.AppendLine("請輸入「著作權備註」欄位");

            if (cbPROGSRCID.SelectedIndex == -1 && cbxPROGSRCID.IsChecked == true)                
                strMessage.AppendLine("請輸入「節目來源」欄位");

            if (cbPROGATTRID.SelectedIndex == -1 && cbxPROGATTRID.IsChecked == true)               
                strMessage.AppendLine("請輸入「節目型態」欄位");

            if (cbPROGAUDID.SelectedIndex == -1 && cbxPROGAUDID.IsChecked == true)                 
                strMessage.AppendLine("請輸入「目標觀眾」欄位");

            if (cbPROGTYPEID.SelectedIndex == -1 && cbxPROGTYPEID.IsChecked == true)               
                strMessage.AppendLine("請輸入「表現方式」欄位");

            if (cbPROGLANGID1.SelectedIndex == -1 && cbxPROGLANGID1.IsChecked == true)              
                strMessage.AppendLine("請輸入「主聲道」欄位");

            if (cbPROGLANGID2.SelectedIndex == -1 && cbxPROGLANGID2.IsChecked == true)               
                strMessage.AppendLine("請輸入「副聲道」欄位");

            if (cbNATION_ID.SelectedIndex == -1 && cb_Modify_NATION_ID.IsChecked == true)
            {
                strMessage.AppendLine("請輸入「來源國家」欄位");
            }

            if (cbxPROGBUYID.IsChecked == true)
            {
                //非必填
                //if (cbPROGBUYID.SelectedIndex == -1 || cbPROGBUYID.SelectedIndex == 0 )
                //    strMessage.AppendLine("請輸入「外購類別」欄位");

                //if (cbPROGBUYDIDS.SelectedIndex == -1)
                //    strMessage.AppendLine("請輸入「外購(細項)」欄位");

                if (cbPROGBUYID.SelectedIndex > 0 && cbPROGBUYDIDS.SelectedIndex == -1)
                    strMessage.AppendLine("請輸入「外購(細項)」欄位");
            }

            //非必填
            //if (cbxPROGSALEID.IsChecked == true)
            //{
            //    if (cbPROGSALEID.SelectedIndex == -1 || cbPROGSALEID.SelectedIndex == 0)
            //        strMessage.AppendLine("請輸入「行銷類別」欄位");
            //}  

            if (cbPROGSPEC.IsChecked == true && Check_PROGSPECList()=="")
                strMessage.AppendLine("請輸入「節目規格」欄位");

            //非必填
            //if (cbSHOOTSPEC.IsChecked == true && Check_SHOOTSPECList() == "")
            //    strMessage.AppendLine("請輸入「拍攝規格」欄位");

            if (cbPRDYEAR.IsChecked==true)
            {
                if (tbxPRDYEAR.Text.Trim().Length != 4)
                    strMessage.AppendLine("請檢查「完成年度」欄位必須為四碼的西元年\n" + "範例：2011(2011年)");
            }


        
            if (strMessage.ToString().Trim() == "") 
            {
                MessageBoxResult resultMsg = MessageBox.Show("確定要批次修改節目子集資料「" + tbxEPISODE.Text.Trim() + "集-" + tbxEPISODE_END.Text.Trim() + "集」資料嗎?", "提示訊息", MessageBoxButton.OKCancel);
                if (resultMsg == MessageBoxResult.Cancel)
                    return false;
                else
                    return true; 
            }                        
            else
            {
                MessageBoxResult result = MessageBox.Show(strMessage.ToString(), "提示訊息", MessageBoxButton.OK);
                return false;
            }
        }

        #endregion
        
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (Check_PROGDData() == false)      //檢查畫面上的欄位是否都填妥
                return;
            
            FormToClass_PROGD();                 //整批修改節目子集資料
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //修改子集權限再分成六大屬性、外購、行銷
        private void checkPermission()
        {
            if (ModuleClass.getModulePermissionByName("02", "節目子集-四大屬性維護") == false)
            {
                cbPROGSRCID.IsEnabled = false;
                cbPROGATTRID.IsEnabled = false;
                cbPROGAUDID.IsEnabled = false;
                cbPROGTYPEID.IsEnabled = false;
                cbxPROGSRCID.IsEnabled = false;
                cbxPROGATTRID.IsEnabled = false;
                cbxPROGAUDID.IsEnabled = false;
                cbxPROGTYPEID.IsEnabled = false;
            }

            if (ModuleClass.getModulePermissionByName("02", "節目子集-外購類別維護") == false)
            {
                cbPROGBUYID.IsEnabled = false;
                cbPROGBUYDIDS.IsEnabled = false;
                cbxPROGBUYID.IsEnabled = false;
            }

            if (ModuleClass.getModulePermissionByName("02", "節目子集-行銷類別維護") == false)
            {
                cbPROGSALEID.IsEnabled = false;
                cbxPROGSALEID.IsEnabled = false;
            }
        }

    }
}

