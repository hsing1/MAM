﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Xml.Linq;
using System.IO;
using System.Text;
using PTS_MAM3.WSPROG_PRODUCER;
using PTS_MAM3.PROG_M;
using System.Windows.Data;

namespace PTS_MAM3.ProgData
{
    public partial class PRO600 : Page
    {
        WSPROG_PRODUCERSoapClient client = new WSPROG_PRODUCERSoapClient();     //產生新的代理類別
        string m_strIDName = "";                                                //暫存名稱 
        Class_PROGPRODUCER m_ReuseQueryobj = new Class_PROGPRODUCER();          //取得查詢頁面的查詢條件obj
        Int32 m_intPageCount = -1;                                              //紀錄PageIndex
        
        public PRO600()
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面
        }

        public PRO600(string strProgID)
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面
            m_ReuseQueryobj.FSID = strProgID;
            client.QUERY_TBPROG_PRODUCER_BYPRODUCERAsync(m_ReuseQueryobj);
            BusyMsg.IsBusy = true;
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        void InitializeForm() //初始化本頁面
        {
            //刪除節目製作人資料
            client.DELETE_TBPROG_PRODUCERCompleted += new EventHandler<DELETE_TBPROG_PRODUCERCompletedEventArgs>(client_DELETE_TBPROG_PRODUCERCompleted);

            //查詢節目製作人資料
            client.QUERY_TBPROG_PRODUCER_BYPRODUCERCompleted += new EventHandler<QUERY_TBPROG_PRODUCER_BYPRODUCERCompletedEventArgs>(client_QUERY_TBPROG_PRODUCER_BYPRODUCERCompleted);                      
        }
            
        #region 實作      

        //實作-查詢節目或短帶製作人資料 
        void client_QUERY_TBPROG_PRODUCER_BYPRODUCERCompleted(object sender, QUERY_TBPROG_PRODUCER_BYPRODUCERCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)  //若是有查詢結果，則顯示，不然就請使用者重新查詢
                {
                    PagedCollectionView pcv = new PagedCollectionView(e.Result);
                    DGPROG_PRODUCERList.ItemsSource = pcv;
                    DataPager.Source = pcv;

                    if (m_intPageCount != -1)        //設定修改後查詢完要回到哪一頁
                        DataPager.PageIndex = m_intPageCount;
                }
                else
                {
                    PagedCollectionView pcv = new PagedCollectionView(new List<Class_PROGPRODUCER>());
                    //指定DataGrid以及DataPager的內容
                    DGPROG_PRODUCERList.ItemsSource = pcv;
                    DataPager.Source = pcv;
                }
            }
        }

        //實作-刪除節目或短帶製作人資料
        void client_DELETE_TBPROG_PRODUCERCompleted(object sender, DELETE_TBPROG_PRODUCERCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    MessageBox.Show("刪除製作人資料「" + m_strIDName + "」成功！", "提示訊息", MessageBoxButton.OK);
                    client.QUERY_TBPROG_PRODUCER_BYPRODUCERAsync(m_ReuseQueryobj);        //如果有刪除資料，要用查詢條件去查詢
                    BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                }
                else
                {
                    MessageBox.Show("刪除製作人資料「" + m_strIDName + "」失敗！", "提示訊息", MessageBoxButton.OK);
                }
            }
        }        

        #endregion       
        
        //新增
        private void btnPROGAdd_Click(object sender, RoutedEventArgs e)
        {
            if (checkPermission() == false)
            {
                MessageBox.Show("無節目製作人資料維護權限，無法新增", "提示訊息", MessageBoxButton.OK);
                return;
            }

            PRG600_01 PRG600_01_frm = new PRG600_01();
            PRG600_01_frm.Show();

            PRG600_01_frm.Closing += (s, args) =>
            {
                if (PRG600_01_frm.DialogResult == true)
                {
                    m_ReuseQueryobj = new Class_PROGPRODUCER();     //新增後，把查詢條件清空
                    client.QUERY_TBPROG_PRODUCER_BYPRODUCERAsync(PRG600_01_frm.m_Queryobj);
                    BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                }                   
            };
        }

        //修改
        private void btnPROGModify_Click(object sender, RoutedEventArgs e)
        {
            if (checkPermission() == false)
            {
                MessageBox.Show("無節目製作人資料維護權限，無法修改", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //判斷是否有選取DataGrid
            if (DGPROG_PRODUCERList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預修改的製作人資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            m_intPageCount = DataPager.PageIndex;       //取得PageIndex

            PRG600_02 PRG600_02_frm = new PRG600_02(((Class_PROGPRODUCER)DGPROG_PRODUCERList.SelectedItem));
            PRG600_02_frm.Show();

            PRG600_02_frm.Closing += (s, args) =>
            {
                if (PRG600_02_frm.DialogResult == true)
                {
                    if (m_ReuseQueryobj.FSID == null)
                    {
                        client.QUERY_TBPROG_PRODUCER_BYPRODUCERAsync(PRG600_02_frm.m_Queryobj);    //如果不是就要用要用修改去查詢
                        BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                    }                        
                    else
                    {
                        client.QUERY_TBPROG_PRODUCER_BYPRODUCERAsync(m_ReuseQueryobj);                //如果先查詢再修改就要保留查詢的條件
                        BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                    }                       
                }                  
            };
        }

        //查詢
        private void btnPROGQuery_Click(object sender, RoutedEventArgs e)
        {
            PRG600_04 PRG600_04_frm = new PRG600_04();
            PRG600_04_frm.Show();

            PRG600_04_frm.Closed += (s, args) =>
            {
                if (PRG600_04_frm.m_ListFormData.Count != 0)
                {
                    //宣告PagedCollectionView,將撈取的資料放入
                    PagedCollectionView pcv = new PagedCollectionView(PRG600_04_frm.m_ListFormData);
                    //指定DataGrid以及DataPager的內容
                    DGPROG_PRODUCERList.ItemsSource = pcv;
                    DataPager.Source = pcv;

                    m_ReuseQueryobj = PRG600_04_frm.m_Queryobj;  //紀錄查詢的條件
                }
            };
        }

        //刪除
        private void btnPROGDelete_Click(object sender, RoutedEventArgs e)
        {
            if (checkPermission() == false)
            {
                MessageBox.Show("無節目製作人資料維護權限，無法刪除", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //判斷是否有選取DataGrid
            if (DGPROG_PRODUCERList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預刪除的製作人資料", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else
            {
                m_strIDName = ((Class_PROGPRODUCER)DGPROG_PRODUCERList.SelectedItem).FSID_NAME.ToString().Trim();                

                string strFSPRO_TYPE = ((Class_PROGPRODUCER)DGPROG_PRODUCERList.SelectedItem).FSPRO_TYPE.ToString().Trim();
                string strFSID = ((Class_PROGPRODUCER)DGPROG_PRODUCERList.SelectedItem).FSID.ToString().Trim();
                string strFSPRODUCER = ((Class_PROGPRODUCER)DGPROG_PRODUCERList.SelectedItem).FSPRODUCER.ToString().Trim();
                              
                MessageBoxResult result = MessageBox.Show("確定要刪除「" + m_strIDName + "」?", "提示訊息", MessageBoxButton.OKCancel);
                if (result == MessageBoxResult.OK)
                {
                    m_intPageCount = DataPager.PageIndex;       //取得PageIndex
                    client.DELETE_TBPROG_PRODUCERAsync(strFSPRO_TYPE, strFSID, strFSPRODUCER, UserClass.userData.FSUSER_ID.ToString());
                    BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                }                   
            }
        }

        //檢視
        private void btnPROGView_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGPROG_PRODUCERList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預檢視的製作人資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            PRG600_03 PRG600_03_frm = new PRG600_03(((Class_PROGPRODUCER)DGPROG_PRODUCERList.SelectedItem));
            PRG600_03_frm.Show();
        }

        //檢查權限
        private Boolean checkPermission()
        {
            if (ModuleClass.getModulePermissionByName("02", "節目製作人資料維護") == false)
                return false;
            else
                return true;
        }

    }
}
