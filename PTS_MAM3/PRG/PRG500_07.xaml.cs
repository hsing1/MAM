﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;
using System.IO;
using System.Text;
using PTS_MAM3.WSPROG_M;
using PTS_MAM3.WSPROG_D;
using PTS_MAM3.ProgData;
using DataGridDCTest;
using PTS_MAM3.WSBROADCAST;
using System.Windows.Browser;   //使用HtmlPage要先引用這行 

namespace PTS_MAM3.ProgData
{
    public partial class PRG500_07 : ChildWindow
    {
        WSPROG_MSoapClient client = new WSPROG_MSoapClient();  //產生新的代理類別
        WSPROG_DSoapClient clientD = new WSPROG_DSoapClient();
        WSBROADCASTSoapClient clientBro = new WSBROADCASTSoapClient();                

        public string strProgID_View = "";      //選取的節目編號
        public string strProgName_View = "";    //選取的節目名稱
        public string strEpisode_View = "";     //選取的集別
        public string strEpisodeName_View = ""; //選取的子集名稱
        public string strTOTLEEPISODE = "";     //全部集數
        public string strFNLENGTH = "";         //節目總長
        public string strCHANNEL = "";          //可播映頻道
        public string strChannel_Id = "";       //頻道別
        public string strProducer = "";         //製作人

        public PRG500_07()
        {
            InitializeComponent();             
            InitializeForm();        //初始化本頁面
        }    

        void InitializeForm() //初始化本頁面
        {  
            //查詢節目資料
            client.fnQProg_M_BYPGDNAME_RACECompleted += new EventHandler<fnQProg_M_BYPGDNAME_RACECompletedEventArgs>(client_fnQProg_M_BYPGDNAME_RACECompleted);

            //查詢子集資料
            clientD.QProg_D_BYPGDNAME_RACECompleted += new EventHandler<QProg_D_BYPGDNAME_RACECompletedEventArgs>(clientD_QProg_D_BYPGDNAME_RACECompleted);
        }

        void LoadBusy()
        {
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
            btnSearch.IsEnabled = false;
            tbxPGMNAME.IsEnabled = false;
        }

        void LoadBusyUnLock()
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息
            btnSearch.IsEnabled = true;
            tbxPGMNAME.IsEnabled = true;
            tbxPGMNAME.Focus();
        }
              
        //實作-查詢節目資料
        void client_fnQProg_M_BYPGDNAME_RACECompleted(object sender, fnQProg_M_BYPGDNAME_RACECompletedEventArgs e)
        {      
            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    this.DGPROG_MListD.DataContext = e.Result;

                    if (this.DGPROG_MListD.DataContext != null)
                    {
                        DGPROG_MListD.SelectedIndex = 0;
                    }
                }            
            }
            clientD.QProg_D_BYPGDNAME_RACEAsync(tbxPGMNAME.Text.Trim());
        }

        //實作-查詢子集資料
        void clientD_QProg_D_BYPGDNAME_RACECompleted(object sender, QProg_D_BYPGDNAME_RACECompletedEventArgs e)
        {
            LoadBusyUnLock();

            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    this.DGPROG_DListD.DataContext = e.Result;

                    if (this.DGPROG_DListD.DataContext != null)
                    {
                        DGPROG_DListD.SelectedIndex = 0;
                    }
                }          
            }
        }   

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //查詢
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            if (tbxPGMNAME.Text.Trim() == "")
            {
                MessageBox.Show("請輸入待查詢的名稱", "提示訊息", MessageBoxButton.OK);
                return;
            }

            client.fnQProg_M_BYPGDNAME_RACEAsync(tbxPGMNAME.Text.Trim());
            LoadBusy();         
        }     

        //連點事件
        private void RowDoubleClick(object sender, DataGridRowClickedArgs e)
        {
            DoubleClickDataGrid dcdg = sender as DoubleClickDataGrid;  
            strProgID_View = ((Class_PROGM)dcdg.SelectedItem).FSPROG_ID.ToString();
            strProgName_View = ((Class_PROGM)dcdg.SelectedItem).FSPGMNAME.ToString();
            strTOTLEEPISODE = ((Class_PROGM)DGPROG_MListD.SelectedItem).FNTOTEPISODE.ToString();
            strFNLENGTH = ((Class_PROGM)DGPROG_MListD.SelectedItem).FNLENGTH.ToString();
            strCHANNEL = ((Class_PROGM)DGPROG_MListD.SelectedItem).FSCHANNEL.ToString();
            strChannel_Id = ((Class_PROGM)DGPROG_MListD.SelectedItem).FSCHANNEL_ID.ToString();
            this.DialogResult = true;
        }

        //連點事件
        private void RowDoubleClickD(object sender, DataGridRowClickedArgs e)
        {
            DoubleClickDataGrid dcdg = sender as DoubleClickDataGrid;
            strProgID_View = ((Class_PROGD)dcdg.SelectedItem).FSPROG_ID.ToString();
            strProgName_View = ((Class_PROGD)dcdg.SelectedItem).FSPROG_ID_NAME.ToString();
            strEpisode_View = ((Class_PROGD)dcdg.SelectedItem).FNEPISODE.ToString();
            strEpisodeName_View = ((Class_PROGD)dcdg.SelectedItem).FSPGDNAME.ToString();   
            this.DialogResult = true;
        }

        //輸入完查詢內容後按下enter即可查詢
        private void tbxPGMNAME_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                btnSearch_Click(sender,e);
            }
        }
        
    }
}

