﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using PTS_MAM3.PROG_M;
using PTS_MAM3.WSPROMO;
using PTS_MAM3.ProgData;

namespace PTS_MAM3.PRG
{
    public partial class PRG800_03 : ChildWindow
    {
        WSPROMOSoapClient client = new WSPROMOSoapClient();
        List<Class_CODE> m_CodeDataCateD = new List<Class_CODE>();  //代碼檔集合(短帶類型細項) 
        string m_strPromoName = "";        //暫存短帶名稱 
        string m_strID = "";              //編碼
        string m_strEpisode = "";         //集別
        string m_ActID = "";              //活動代碼
        Class_PROMO m_FormData = new Class_PROMO();
        public Class_PROMO m_Queryobj = new Class_PROMO();    //查詢修改的obj

        public PRG800_03(Class_PROMO FormData)
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面
            m_FormData = FormData;
            ClassToForm();
        }

        void InitializeForm() //初始化本頁面
        {           
            //下載代碼檔
            client.GetTBPGM_PROMO_CODECompleted += new EventHandler<GetTBPGM_PROMO_CODECompletedEventArgs>(client_GetTBPGM_PROMO_CODECompleted);
            client.GetTBPGM_PROMO_CODEAsync();

            //下載短帶類型細項代碼檔
            client.GetTBPGM_PROMO_CATD_CODECompleted += new EventHandler<GetTBPGM_PROMO_CATD_CODECompletedEventArgs>(client_GetTBPGM_PROMO_CATD_CODECompleted);
            client.GetTBPGM_PROMO_CATD_CODEAsync();
        }

        private void ClassToForm()  //將前一頁點選的短帶資料繫集到畫面上
        {
            this.gdPROMO.DataContext = m_FormData;
            if (m_FormData.FSEXPIRE_DATE_ACTION == "Y")
                cbDELETE.IsChecked = true;
            else
                cbDELETE.IsChecked = false;
            dpEXPIRE.Text = m_FormData.FDEXPIRE_DATE.ToShortDateString();
        }

        #region 實作

        //實作-下載短帶類型細項代碼檔
        void client_GetTBPGM_PROMO_CATD_CODECompleted(object sender, GetTBPGM_PROMO_CATD_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    //m_CodeDataBuyD = new List<Class_CODE>(e.Result);

                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        ComboBoxItem cbiPROMOCATD = new ComboBoxItem();
                        cbiPROMOCATD.Content = CodeData.NAME;
                        cbiPROMOCATD.Tag = CodeData.ID;
                        cbiPROMOCATD.DataContext = CodeData.IDD;
                        this.cbPROMOCATIDD.Items.Add(cbiPROMOCATD);   //先都產生在隱藏起來的細項代碼combobox裡                   
                        m_CodeDataCateD.Add(CodeData);
                    }
                }
            }
        }

        //實作-下載代碼檔
        void client_GetTBPGM_PROMO_CODECompleted(object sender, GetTBPGM_PROMO_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            case "TBZCHANNEL":        //頻道別代碼
                                ComboBoxItem cbiCHANNEL = new ComboBoxItem();
                                cbiCHANNEL.Content = CodeData.NAME;
                                cbiCHANNEL.Tag = CodeData.ID;
                                this.cbCHANNEL_ID.Items.Add(cbiCHANNEL);
                                break;
                            case "TBZPROGOBJ":       //製作目的代碼
                                ComboBoxItem cbiPROGOBJ = new ComboBoxItem();
                                cbiPROGOBJ.Content = CodeData.NAME;
                                cbiPROGOBJ.Tag = CodeData.ID;
                                this.cbPROGOBJID.Items.Add(cbiPROGOBJ);
                                break;
                            case "TBZPRDCENTER":      //製作單位代碼
                                ComboBoxItem cbiPRDCENTER = new ComboBoxItem();
                                cbiPRDCENTER.Content = CodeData.NAME;
                                cbiPRDCENTER.Tag = CodeData.ID;
                                this.cbPRDCENID.Items.Add(cbiPRDCENTER);
                                break;
                            case "TBZPROMOVER":      //版本代碼
                                ComboBoxItem cbiPROMOVER = new ComboBoxItem();
                                cbiPROMOVER.Content = CodeData.NAME;
                                cbiPROMOVER.Tag = CodeData.ID;
                                this.cbPROMOVERID.Items.Add(cbiPROMOVER);
                                break;
                            case "TBZPROMOTYPE":      //類別
                                ComboBoxItem cbiPROMOTYPE = new ComboBoxItem();
                                cbiPROMOTYPE.Content = CodeData.NAME;
                                cbiPROMOTYPE.Tag = CodeData.ID;
                                this.cbPROMOTYPEID.Items.Add(cbiPROMOTYPE);
                                break;
                            case "TBZPROMOCAT":       //類型代碼
                                ComboBoxItem cbiPROMOCAT = new ComboBoxItem();
                                cbiPROMOCAT.Content = CodeData.NAME;
                                cbiPROMOCAT.Tag = CodeData.ID;
                                this.cbPROMOCATID.Items.Add(cbiPROMOCAT);
                                break;
                            default:
                                break;
                        }
                    }

                    //比對代碼檔   
                    compareCode_PROGID();
                    compareCode_EPISODE();
                    compareCode_ACT();    
                    compareCode_FSPROGOBJID(m_FormData.FSPROGOBJID.ToString().Trim());
                    compareCode_FSPRDCENID(m_FormData.FSPRDCENID.ToString().Trim());
                    compareCode_FSPROMOVERID(m_FormData.FSPROMOVERID.ToString().Trim());
                    compareCode_FSPROMOTYPEID(m_FormData.FSPROMOTYPEID.ToString().Trim());
                    compareCode_FSPROMOCATID(m_FormData.FSPROMOCATID.ToString().Trim());
                    compareCode_FSPROMOCATDID(m_FormData.FSPROMOCATDID.ToString().Trim());
                    compareCode_FSCHANNEL_ID(m_FormData.FSMAIN_CHANNEL_ID.ToString().Trim());
                    compareDel(m_FormData.FSDEL.ToString().Trim());           //刪除註記
                }
            }
        }

        #endregion

        #region 比對代碼檔

        //節目編號
        void compareCode_PROGID()
        {
            tbxPROG_NAME.Text = m_FormData.FSPROG_ID_NAME.Trim();
            tbxPROG_NAME.Tag = m_FormData.FSPROG_ID.Trim();
            m_strID = m_FormData.FSPROG_ID.Trim();
        }

        //集別
        void compareCode_EPISODE()
        {
           tbxEPISODE.Text = m_FormData.SHOW_FNEPISODE.Trim() ;
           lblEPISODE_NAME.Content =  m_FormData.FNEPISODE_NAME.Trim() ;
           m_strEpisode =m_FormData.SHOW_FNEPISODE.Trim() ;
        }

        //活動
        void compareCode_ACT()
        {
            tbxACTIONID.Text = m_FormData.FSACTION_NAME.Trim();
            tbxACTIONID.Tag = m_FormData.FSACTIONID.Trim();
            m_ActID = m_FormData.FSACTIONID.Trim();
        }

        //比對代碼檔_製作單位
        void compareCode_FSPRDCENID(string strID)
        {
            for (int i = 0; i < cbPRDCENID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPRDCENID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPRDCENID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_製作目的
        void compareCode_FSPROGOBJID(string strID)
        {
            for (int i = 0; i < cbPROGOBJID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGOBJID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROGOBJID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_短帶版本
        void compareCode_FSPROMOVERID(string strID)
        {
            for (int i = 0; i < cbPROMOVERID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROMOVERID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROMOVERID.SelectedIndex = i;
                    break;
                }
            }
        }
        
        //比對代碼檔_類別
        void compareCode_FSPROMOTYPEID(string strID)
        {
            for (int i = 0; i < cbPROMOTYPEID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROMOTYPEID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROMOTYPEID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_類型大項
        void compareCode_FSPROMOCATID(string strID)
        {
            for (int i = 0; i < cbPROMOCATID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROMOCATID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROMOCATID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_類型細項
        void compareCode_FSPROMOCATDID(string strID)
        {
            for (int i = 0; i < cbPROMOCATIDDS.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROMOCATIDDS.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbPROMOCATIDDS.SelectedIndex = i;
                    break;
                }
            }
        }
        
        //比對代碼檔_頻道別
        void compareCode_FSCHANNEL_ID(string strID)
        {
            for (int i = 0; i < cbCHANNEL_ID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbCHANNEL_ID.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID))
                {
                    cbCHANNEL_ID.SelectedIndex = i;
                    break;
                }
            }
        }

        //是否為刪除
        private void compareDel(string strDel)   
        {
            if (strDel == "N")
                cbDel.SelectedIndex = 0;
            else if (strDel == "Y")
                cbDel.SelectedIndex = 1;
        } 

        //選擇短帶類型，要判斷顯示哪些短帶類型細項
        private void cbPROMOCATID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.cbPROMOCATIDDS.Items.Clear();     //使用前先清空
            string strCATID = ""; //類型大項
            ComboBox cb = sender as ComboBox;
            ComboBoxItem cbm = (ComboBoxItem)cb.SelectedItem;
            strCATID = cbm.Tag.ToString();

            for (int i = 0; i < cbPROMOCATIDD.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROMOCATIDD.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strCATID))
                {
                    ComboBoxItem cbiPROMOCATIDS = new ComboBoxItem();
                    cbiPROMOCATIDS.Content = getcbi.Content;
                    cbiPROMOCATIDS.Tag = getcbi.DataContext;
                    this.cbPROMOCATIDDS.Items.Add(cbiPROMOCATIDS);
                }
            }
        }
            
        #endregion

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }     
    }
}

