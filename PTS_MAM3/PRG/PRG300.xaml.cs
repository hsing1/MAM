﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Xml.Linq;
using System.IO;
using System.Text;
using PTS_MAM3.WSPROGLINK;
using PTS_MAM3.PROG_M;
using System.Windows.Data;

namespace PTS_MAM3.ProgData
{
    public partial class PROGLINK : Page
    {
        WSPROGLINKSoapClient client = new WSPROGLINKSoapClient();   //產生新的代理類別
        List<ClassCode> m_CodeData = new List<ClassCode>();         //代碼檔集合 
        string m_strProgName = "";                                  //暫存節目名稱 
        string m_strEpisode = "";                                   //暫存集別
        Class_PROGLINK m_ReuseQueryobj = new Class_PROGLINK();      //取得查詢頁面的查詢條件obj
        Int32 m_intPageCount = -1;                                  //紀錄PageIndex

        public PROGLINK()
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面
        }

        public PROGLINK(string strProgID)
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面
            m_ReuseQueryobj.FSPROG_ID = strProgID;
            m_ReuseQueryobj.FSORGPROG_ID = "";
            m_ReuseQueryobj.FNEPISODE = 32767;
            m_ReuseQueryobj.FNORGEPISODE = 32767;
            m_ReuseQueryobj.FSMEMO = "";
            client.QUERY_TBPROGLINKAsync(m_ReuseQueryobj);
            BusyMsg.IsBusy = true;
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        void InitializeForm() //初始化本頁面
        {
            //取得全部扣播出次數節目子集資料
            client.GetTBPROGLINK_ALLCompleted += new EventHandler<GetTBPROGLINK_ALLCompletedEventArgs>(client_GetTBPROGLINK_ALLCompleted);
            //client.GetTBPROGLINK_ALLAsync();  //一開始不Load全部資料

            //刪除扣播出次數節目子集資料
            client.DELETE_TBPROGLINKCompleted += new EventHandler<DELETE_TBPROGLINKCompletedEventArgs>(client_DELETE_TBPROGLINKCompleted);

            //查詢扣播出次數節目子集資料
            client.QUERY_TBPROGLINKCompleted += new EventHandler<QUERY_TBPROGLINKCompletedEventArgs>(client_QUERY_TBPROGLINKCompleted);
        }       

        #region 實作       

        //實作-取得全部扣播出次數節目子集資料
        void client_GetTBPROGLINK_ALLCompleted(object sender, GetTBPROGLINK_ALLCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {                  
                    PagedCollectionView pcv = new PagedCollectionView(e.Result);
                    DGPROG_LinkList.ItemsSource = pcv;
                    DataPager.Source = pcv;
                }
            }
        }

        //實作-刪除扣播出次數節目子集資料
        void client_DELETE_TBPROGLINKCompleted(object sender, DELETE_TBPROGLINKCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result.bolResult == true)
                {
                    MessageBox.Show("刪除扣播出次數節目子集資料「" + m_strProgName.ToString() + "-" + m_strEpisode + "集」成功！", "提示訊息", MessageBoxButton.OK); 
                    client.QUERY_TBPROGLINKAsync(m_ReuseQueryobj);    //如果有刪除節目資料，要用查詢條件去查詢
                }
                else
                {
                    MessageBox.Show("刪除扣播出次數節目子集資料「" + m_strProgName.ToString() + "-" + m_strEpisode + "集」失敗！\n失敗原因：" + e.Result.strErrorMsg, "提示訊息", MessageBoxButton.OK);
                }
            }
        }

        //實作-查詢扣播出次數節目子集資料
        void client_QUERY_TBPROGLINKCompleted(object sender, QUERY_TBPROGLINKCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)  //若是有查詢結果，則顯示，不然就請使用者重新查詢
                {
                    PagedCollectionView pcv = new PagedCollectionView(e.Result);
                    DGPROG_LinkList.ItemsSource = pcv;
                    DataPager.Source = pcv;

                    if (m_intPageCount != -1)        //設定修改後查詢完要回到哪一頁
                        DataPager.PageIndex = m_intPageCount;
                }
                else
                {
                    PagedCollectionView pcv = new PagedCollectionView(new List<Class_PROGLINK>());
                    //指定DataGrid以及DataPager的內容
                    DGPROG_LinkList.ItemsSource = pcv;
                    DataPager.Source = pcv;
                }
            }
        }

        #endregion
        
        //新增
        private void btnPROGAdd_Click(object sender, RoutedEventArgs e)
        {
            if (checkPermission() == false)
            {
                MessageBox.Show("無衍生子集資料維護權限，無法新增", "提示訊息", MessageBoxButton.OK);
                return;
            }

            PROGLINK_Add PROGLINK_Add_frm = new PROGLINK_Add();
            PROGLINK_Add_frm.Show();

            PROGLINK_Add_frm.Closing += (s, args) =>
            {
                if (PROGLINK_Add_frm.DialogResult == true)
                {
                    m_ReuseQueryobj = new Class_PROGLINK();     //新增後，把查詢條件清空
                    client.QUERY_TBPROGLINKAsync(PROGLINK_Add_frm.m_Queryobj);
                    BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                }                    
            };
        }

        //修改
        private void btnPROGModify_Click(object sender, RoutedEventArgs e)
        {
            if (checkPermission() == false)
            {
                MessageBox.Show("無衍生子集資料維護權限，無法修改", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //判斷是否有選取DataGrid
            if (DGPROG_LinkList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預修改的扣播出次數節目子集資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            m_intPageCount = DataPager.PageIndex;       //取得PageIndex

            PROGLINK_Edit PROGLINK_Edit_frm = new PROGLINK_Edit(((Class_PROGLINK)DGPROG_LinkList.SelectedItem));
            PROGLINK_Edit_frm.Show();

            PROGLINK_Edit_frm.Closing += (s, args) =>
            {
                if (PROGLINK_Edit_frm.DialogResult == true)
                {
                    if (m_ReuseQueryobj.FSPROG_ID == null)
                    {
                        client.QUERY_TBPROGLINKAsync(PROGLINK_Edit_frm.m_Queryobj);    //如果不是就要用要用修改去查詢
                        BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                    }                        
                    else
                    {
                        client.QUERY_TBPROGLINKAsync(m_ReuseQueryobj);                 //如果先查詢再修改就要保留查詢的條件 
                        BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                    }                        
                }                
            };
        }

        //查詢
        private void btnPROGQuery_Click(object sender, RoutedEventArgs e)
        {
            PROGLINK_Query PROGLINK_Query_frm = new PROGLINK_Query();
            PROGLINK_Query_frm.Show();

            PROGLINK_Query_frm.Closed += (s, args) =>
            {
                if (PROGLINK_Query_frm.m_ListFormData.Count != 0)
                {
                    //宣告PagedCollectionView,將撈取的資料放入
                    PagedCollectionView pcv = new PagedCollectionView(PROGLINK_Query_frm.m_ListFormData);
                    //指定DataGrid以及DataPager的內容
                    DGPROG_LinkList.ItemsSource = pcv;
                    DataPager.Source = pcv;

                    m_ReuseQueryobj = PROGLINK_Query_frm.m_Queryobj;  //紀錄查詢的條件
                }
            };
        }

        //刪除
        private void btnPROGDel_Click(object sender, RoutedEventArgs e)
        {
            if (checkPermission() == false)
            {
                MessageBox.Show("無衍生子集資料維護權限，無法刪除", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //判斷是否有選取DataGrid
            if (DGPROG_LinkList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預刪除的扣播出次數節目子集資料", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else
            {
                string strPROG_ID = ((Class_PROGLINK)DGPROG_LinkList.SelectedItem).FSPROG_ID.ToString().Trim();
                string strEPISODE = ((Class_PROGLINK)DGPROG_LinkList.SelectedItem).FNEPISODE.ToString().Trim();
                string strORGPROG_ID = ((Class_PROGLINK)DGPROG_LinkList.SelectedItem).FSORGPROG_ID.ToString().Trim();
                string strORGEPISODE = ((Class_PROGLINK)DGPROG_LinkList.SelectedItem).FNORGEPISODE.ToString().Trim();

                m_strProgName = ((Class_PROGLINK)DGPROG_LinkList.SelectedItem).FSPROG_ID_NAME.ToString().Trim();
                m_strEpisode = ((Class_PROGLINK)DGPROG_LinkList.SelectedItem).FNEPISODE.ToString().Trim();

                MessageBoxResult result = MessageBox.Show("確定要刪除「" + m_strProgName.ToString() + "-" + m_strEpisode + "集」?", "提示訊息", MessageBoxButton.OKCancel);

                if (result == MessageBoxResult.OK)
                {
                    m_intPageCount = DataPager.PageIndex;       //取得PageIndex
                    client.DELETE_TBPROGLINKAsync(((Class_PROGLINK)DGPROG_LinkList.SelectedItem), UserClass.userData.FSUSER_ID.ToString());
                    BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                }                    
            }
        }

        //檢視
        private void btnPROGView_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGPROG_LinkList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預檢視的扣播出次數節目子集資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            PRG300_03 PRG300_03_frm = new PRG300_03(((Class_PROGLINK)DGPROG_LinkList.SelectedItem));
            PRG300_03_frm.Show();
        }

        //批次新增
        private void btnPROGAddBatch_Click(object sender, RoutedEventArgs e)
        {
            if (checkPermission() == false)
            {
                MessageBox.Show("無衍生子集資料維護權限，無法新增", "提示訊息", MessageBoxButton.OK);
                return;
            }

            PRG300_05 PRG300_05_frm = new PRG300_05();
            PRG300_05_frm.Show();

            PRG300_05_frm.Closing += (s, args) =>
            {
                if (PRG300_05_frm.DialogResult == true)
                {                    
                    m_ReuseQueryobj = PRG300_05_frm.m_Queryobj;
                    client.QUERY_TBPROGLINKAsync(PRG300_05_frm.m_Queryobj);
                    BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                }
            };
        }


        //檢查權限
        private Boolean checkPermission()
        {
            if (ModuleClass.getModulePermissionByName("02", "衍生節目子集維護") == false)
                return false;
            else
                return true;
        }
    }
}
