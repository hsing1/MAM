﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSPROPOSAL;
using PTS_MAM3.ProgData;

namespace PTS_MAM3.Proposal
{
    public partial class PRO100_10 : ChildWindow
    {        
        WSPROPOSALSoapClient client = new WSPROPOSALSoapClient();//產生新的代理類別
        public string strProID = "";

        public PRO100_10()
        {
            InitializeComponent();
          
            //查詢成案單檔
            client.fnGetTBPROPOSAL_ALL_BYEMERGENCYCompleted += new EventHandler<fnGetTBPROPOSAL_ALL_BYEMERGENCYCompletedEventArgs>(client_fnGetTBPROPOSAL_ALL_BYEMERGENCYCompleted);

            //載入送帶轉檔資料
            Load_TBBROADCAST_ALL();
        }

        //實作-查詢成案單檔
        void client_fnGetTBPROPOSAL_ALL_BYEMERGENCYCompleted(object sender, fnGetTBPROPOSAL_ALL_BYEMERGENCYCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null)
                { 
                    //指定DataGrid的內容
                    DGPROPOSALList.ItemsSource = e.Result;

                    if (e.Result.Count > 0)
                        DGPROPOSALList.SelectedIndex = 0;
                }
            }

            BusyMsg.IsBusy = false; 
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (DGPROPOSALList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預列印的節目成案資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //載入成案資料
        private void Load_TBBROADCAST_ALL()
        {
            dpStart.Text = DateTime.Now.ToShortDateString();
            dpEnd.Text = DateTime.Now.ToShortDateString();

            //要加一天才能帶出今天的
            client.fnGetTBPROPOSAL_ALL_BYEMERGENCYAsync(DateTime.Now.AddYears(-1), DateTime.Now.AddDays(1),"");
            BusyMsg.IsBusy = true; 
        }

        //查詢成案資料
        private void btnQuery_Click(object sender, RoutedEventArgs e)
        {
            //節目名稱查詢或是不要
            if (tbxPROG_NAME.Tag != null)
                client.fnGetTBPROPOSAL_ALL_BYEMERGENCYAsync(Convert.ToDateTime(dpStart.Text), Convert.ToDateTime(dpEnd.Text).AddDays(1), tbxPROG_NAME.Tag.ToString().Trim()); 
            else
                client.fnGetTBPROPOSAL_ALL_BYEMERGENCYAsync(Convert.ToDateTime(dpStart.Text), Convert.ToDateTime(dpEnd.Text).AddDays(1),""); 
                        
            BusyMsg.IsBusy = true; 
        }

        //開啟節目資料查詢畫面
        private void btnPROG_Click(object sender, RoutedEventArgs e)
        {
            PROGDATA_VIEW PROGDATA_VIEW_frm = new PROGDATA_VIEW();
            PROGDATA_VIEW_frm.Show();

            PROGDATA_VIEW_frm.Closed += (s, args) =>
            {
                if (PROGDATA_VIEW_frm.DialogResult == true)
                {
                    tbxPROG_NAME.Text = PROGDATA_VIEW_frm.strProgName_View;
                    tbxPROG_NAME.Tag = PROGDATA_VIEW_frm.strProgID_View;  
                }
            };
        }

    }
}

