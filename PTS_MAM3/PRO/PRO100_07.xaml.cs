﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSPROG_M;

namespace PTS_MAM3.PRO
{
    public partial class PRO100_07 : ChildWindow
    {
        WSPROG_MSoapClient client = new WSPROG_MSoapClient();                 //產生新的代理類別
        public string m_Dept = "";
        public string m_DeptName = "";

        public PRO100_07(string strDep,string strSW)
        {
            InitializeComponent();

            if (strSW.Trim() == "BEF")
                this.Title = "設定前會部門";
            else
                this.Title = "設定後會部門";

                m_Dept = strDep;
            
            //下載代碼檔
            client.fnGetTBPROG_M_CODECompleted += new EventHandler<fnGetTBPROG_M_CODECompletedEventArgs>(client_fnGetTBPROG_M_CODECompleted);
            client.fnGetTBPROG_M_CODEAsync();
        }

        //實作-下載所有的代碼檔
        void client_fnGetTBPROG_M_CODECompleted(object sender, fnGetTBPROG_M_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {        
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {        
                            case "TBZDEPT":
                                //前後會部門別代碼，因為部門檔有些是必填，有些不是必填，因此這裡代表必填要把空白濾掉
                                if (CodeData.NAME.Trim() != "")
                                {
                                    CheckBox cbiBEF = new CheckBox();
                                    cbiBEF.Content = CodeData.NAME;
                                    cbiBEF.Tag = CodeData.ID;
                                    //ScaleTransform st = new ScaleTransform();
                                    //st.ScaleX = 1.1;
                                    //st.ScaleY = 1.1;
                                    //cbiBEF.RenderTransform = st;
                                    this.listDEPT.Items.Add(cbiBEF);
                                }

                                break;

                            default:
                                break;
                        }
                    }

                    //將「其他」欄位加入，ID為99
                    if (listDEPT.Items.Count > 0)
                    {
                         CheckBox cbiBEF = new CheckBox();
                         cbiBEF.Content = "其他";           
                         cbiBEF.Tag = "99";           
                         this.listDEPT.Items.Add(cbiBEF); 
                    }

                }
            }

            compareCode(CheckList(m_Dept));
        }

        private string Check_DEPTList()    
        {
            string strReturn = "";
            
            for (int i = 0; i < listDEPT.Items.Count; i++)
            {
                CheckBox getckb = (CheckBox)listDEPT.Items.ElementAt(i);
                if (getckb.IsChecked == true)
                {
                    strReturn += getckb.Tag.ToString().Trim() + ";";
                }
            }
            return strReturn;
        }

        public string Check_DEPT_NAME_List()
        {
            string strReturn = "";
            
            for (int i = 0; i < listDEPT.Items.Count; i++)
            {
                CheckBox getckb = (CheckBox)listDEPT.Items.ElementAt(i);
                if (getckb.IsChecked == true)
                {
                    strReturn += getckb.Content.ToString().Trim() + ";";
                }
            }
            return strReturn;
        }

        private string[] CheckList(string strChannel)
        {
            char[] delimiterChars = { ';' };
            string[] words = strChannel.Split(delimiterChars);
            return words;
        }

        //比對代碼檔_可播映頻道
        void compareCode(string[] ListSend)
        {           
            for (int i = 0; i < ListSend.Length - 1; i++)
            {
                for (int j = 0; j < listDEPT.Items.Count; j++)
                {
                    CheckBox getcki = (CheckBox)listDEPT.Items.ElementAt(j);

                    if (getcki.Tag.ToString().Trim().Equals(ListSend[i]))
                    {
                        getcki.IsChecked = true;
                        break;
                    }
                }
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            m_Dept = Check_DEPTList();
            m_DeptName = Check_DEPT_NAME_List();
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

