﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using System.Xml.Linq;
using System.IO;
using PTS_MAM3.PROG_M;
using PTS_MAM3.WSPROG_M;
using PTS_MAM3.WSPROPOSAL;

namespace PTS_MAM3.Proposal
{
    public partial class PRO100_03 : ChildWindow
    {
        WSPROG_MSoapClient client = new WSPROG_MSoapClient();     //產生新的代理類別
        WSPROPOSALSoapClient clientPRO = new WSPROPOSALSoapClient();
        Class_PROPOSAL m_FormData = new Class_PROPOSAL();  
        string m_strBEF_DEPT = "";
        string m_strAFT_DEPT = "";
        public string strFromID = "";    //Flow專用(成案單編號)
        public int intFlowID;            //Flow專用
        public int intProcessID;         //Flow專用

        public PRO100_03(Class_PROPOSAL FormData)
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面
            m_FormData = FormData;
            ClassToForm();
         }

        public PRO100_03(string strFromID)
        {
            if (strFromID.ToString().Trim() == "")
                return;
            InitializeComponent();

            //下載代碼檔
            client.fnGetTBPROG_M_CODECompleted += new EventHandler<fnGetTBPROG_M_CODECompletedEventArgs>(client_fnGetTBPROG_M_CODECompleted);

            //下載外購類別細項代碼檔
            client.fnGetTBPROG_M_PROGBUYD_CODECompleted += new EventHandler<fnGetTBPROG_M_PROGBUYD_CODECompletedEventArgs>(client_fnGetTBPROG_M_PROGBUYD_CODECompleted);

            //查詢成案資料
            clientPRO.fnQUERYTBPROPOSAL_BYIDCompleted += new EventHandler<fnQUERYTBPROPOSAL_BYIDCompletedEventArgs>(clientPRO_fnQUERYTBPROPOSAL_BYIDCompleted);
            clientPRO.fnQUERYTBPROPOSAL_BYIDAsync(strFromID.ToString().Trim());
        }

        void InitializeForm() //初始化本頁面
        {
            //下載代碼檔
            client.fnGetTBPROG_M_CODECompleted += new EventHandler<fnGetTBPROG_M_CODECompletedEventArgs>(client_fnGetTBPROG_M_CODECompleted);
            client.fnGetTBPROG_M_CODEAsync();

            //下載外購類別細項代碼檔
            client.fnGetTBPROG_M_PROGBUYD_CODECompleted += new EventHandler<fnGetTBPROG_M_PROGBUYD_CODECompletedEventArgs>(client_fnGetTBPROG_M_PROGBUYD_CODECompleted);
            client.fnGetTBPROG_M_PROGBUYD_CODEAsync();          
        }

        //實作-查詢成案資料
        void clientPRO_fnQUERYTBPROPOSAL_BYIDCompleted(object sender, fnQUERYTBPROPOSAL_BYIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result.Count== 0)
                {
                    MessageBox.Show("傳入參數異常，請檢查！", "提示訊息", MessageBoxButton.OK);
                    return;
                }

                if (e.Result != null && e.Result.Count > 0)
                {
                    //指定繫集的class及下拉式選單要繫集的class
                    this.gdPROPOSAL_Edit.DataContext = e.Result[0];
             
                    m_FormData = (Class_PROPOSAL)(e.Result[0]);
                    dpSHOW_DATE.Text = m_FormData.FDSHOW_DATE.ToString();  //日期格式比較特別，用指定的方式

                    //把資料BIND進去 到期日期 到期日期動作 來源資料 三個欄位
                    if (m_FormData.FDEXPIRE_DATE.ToShortDateString()=="1900/1/1")
                    {
                        dpEXPIRE.Text = "";
                    }else
                        dpEXPIRE.Text = m_FormData.FDEXPIRE_DATE.ToShortDateString();
                    if (m_FormData.FSEXPIRE_DATE_ACTION == "Y")
                    {
                        cbDELETE.IsChecked = true;
                    }
                    else 
                    {
                        cbDELETE.IsChecked = false;
                    }
                    cbNATION_ID.SelectedItem = cbNATION_ID.Items.Select(S => ((ComboBoxItem)S).Tag = m_FormData.FSPROGNATIONID).FirstOrDefault();   //國家代碼

                    client.fnGetTBPROG_M_CODEAsync();
                    client.fnGetTBPROG_M_PROGBUYD_CODEAsync();
                }
            }
        }

        private void ClassToForm()  //將前一頁點選的節目資料繫集到畫面上
        {
            this.gdPROPOSAL_Edit.DataContext = m_FormData;
        }

        #region ComboBox載入代碼檔

        //實作-下載外購類別細項代碼檔
        void client_fnGetTBPROG_M_PROGBUYD_CODECompleted(object sender, fnGetTBPROG_M_PROGBUYD_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        ComboBoxItem cbiPROGBUYD = new ComboBoxItem();
                        cbiPROGBUYD.Content = CodeData.NAME;
                        cbiPROGBUYD.Tag = CodeData.ID;
                        cbiPROGBUYD.DataContext = CodeData.IDD;
                        this.cbBUYD_ID.Items.Add(cbiPROGBUYD);   //先都產生在隱藏起來的細項代碼combobox裡         
                        //m_CodeDataBuyD.Add(CodeData);
                    }
                    
                }
            }
        }

        //實作-下載所有的代碼檔
        void client_fnGetTBPROG_M_CODECompleted(object sender, fnGetTBPROG_M_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            case "TBZPROGOBJ":      //製作目的代碼
                                ComboBoxItem cbiPROGOBJ = new ComboBoxItem();
                                cbiPROGOBJ.Content = CodeData.NAME;
                                cbiPROGOBJ.Tag = CodeData.ID;
                                this.cbOBJ_ID.Items.Add(cbiPROGOBJ);
                                break;

                            case "TBZDEPT":         //製作部門別代碼
                                ComboBoxItem cbiPRD = new ComboBoxItem();
                                cbiPRD.Content = CodeData.NAME;
                                cbiPRD.Tag = CodeData.ID;
                                this.cbPRD_DEPT_ID.Items.Add(cbiPRD);

                                //付款部門別代碼
                                ComboBoxItem cbiPAY = new ComboBoxItem();
                                cbiPAY.Content = CodeData.NAME;
                                cbiPAY.Tag = CodeData.ID;
                                this.cbPAY_DEPT_ID.Items.Add(cbiPAY);

                                //前後會部門別代碼
                                CheckBox cbiBEF = new CheckBox();                               
                                cbiBEF.Content = CodeData.NAME;
                                cbiBEF.Tag = CodeData.ID;
                                this.listDEPT.Items.Add(cbiBEF);                                
                                break;

                            case "TBZPROGGRADE":      //節目分級代碼
                                ComboBoxItem cbiPROGGRADE = new ComboBoxItem();
                                cbiPROGGRADE.Content = CodeData.NAME;
                                cbiPROGGRADE.Tag = CodeData.ID;
                                this.cbPROGGRADEID.Items.Add(cbiPROGGRADE);
                                break;

                            case "TBZPROGSRC":      //節目來源代碼
                                ComboBoxItem cbiSRC = new ComboBoxItem();
                                cbiSRC.Content = CodeData.NAME;
                                cbiSRC.Tag = CodeData.ID;
                                this.cbSRCID.Items.Add(cbiSRC);
                                break;

                            case "TBZPROGATTR":      //內容屬性代碼
                                ComboBoxItem cbiTYPE = new ComboBoxItem();
                                cbiTYPE.Content = CodeData.NAME;
                                cbiTYPE.Tag = CodeData.ID;
                                this.cbTYPE.Items.Add(cbiTYPE);
                                break;

                            case "TBZPROGAUD":      //目標觀眾代碼
                                ComboBoxItem cbiAUD = new ComboBoxItem();
                                cbiAUD.Content = CodeData.NAME;
                                cbiAUD.Tag = CodeData.ID;
                                this.cbAUD_ID.Items.Add(cbiAUD);
                                break;

                            case "TBZPROGTYPE":      //表現方式代碼
                                ComboBoxItem cbiSHOW_TYPE = new ComboBoxItem();
                                cbiSHOW_TYPE.Content = CodeData.NAME;
                                cbiSHOW_TYPE.Tag = CodeData.ID;
                                this.cbSHOW_TYPE.Items.Add(cbiSHOW_TYPE);
                                break;

                            case "TBZPROGLANG":      //語言代碼
                                ComboBoxItem cbiLANG_ID_MAIN = new ComboBoxItem();
                                cbiLANG_ID_MAIN.Content = CodeData.NAME;
                                cbiLANG_ID_MAIN.Tag = CodeData.ID;
                                this.cbLANG_ID_MAIN.Items.Add(cbiLANG_ID_MAIN);  //主聲道

                                ComboBoxItem cbiLANG_ID_SUB = new ComboBoxItem();
                                cbiLANG_ID_SUB.Content = CodeData.NAME;
                                cbiLANG_ID_SUB.Tag = CodeData.ID;
                                this.cbLANG_ID_SUB.Items.Add(cbiLANG_ID_SUB);  //副聲道
                                break;

                            case "TBZPROGBUY":       //外購類別代碼
                                ComboBoxItem cbiBUY_ID = new ComboBoxItem();
                                cbiBUY_ID.Content = CodeData.NAME;
                                cbiBUY_ID.Tag = CodeData.ID;
                                this.cbBUY_ID.Items.Add(cbiBUY_ID);
                                break;

                            case "TBZCHANNEL_PRO":       //頻道別代碼
                                CheckBox cbCHANNEL = new CheckBox();
                                cbCHANNEL.Content = CodeData.NAME;
                                cbCHANNEL.Tag = CodeData.ID;
                                cbCHANNEL.IsEnabled = false;
                                listCHANNEL.Items.Add(cbCHANNEL);
                                break;

                            case "TBZPRDCENTER":      //製作單位代碼
                                if (CodeData.ID.Trim() != "00000")  //舊資料的代碼要略過
                                {
                                    ComboBoxItem cbiPRDCENTER = new ComboBoxItem();
                                    cbiPRDCENTER.Content = CodeData.NAME;
                                    cbiPRDCENTER.Tag = CodeData.ID;
                                    this.cbPRDCENID.Items.Add(cbiPRDCENTER);
                                }
                                break;

                            case "TBZPROGSPEC":       //節目規格代碼
                                CheckBox cbPROGSPEC = new CheckBox();
                                cbPROGSPEC.Content = CodeData.NAME;
                                cbPROGSPEC.Tag = CodeData.ID;
                                cbPROGSPEC.IsEnabled = false;
                                listPROGSPEC.Items.Add(cbPROGSPEC);
                                break;

                            case "TBZPROGNATION":       //來源國家    2012/11/22 kyle                            
                                ComboBoxItem cbNATION = new ComboBoxItem();
                                cbNATION.Content = CodeData.NAME;
                                cbNATION.Tag = CodeData.ID;
                                cbNATION_ID.Items.Add(cbNATION);
                                break;

                            default:
                                break;
                        }
                        
                    }

                    //將「其他」欄位加入前後會部門檔，ID為99
                    if (listDEPT.Items.Count > 0)
                    {
                        CheckBox cbiBEF = new CheckBox();
                        cbiBEF.Content = "其他";
                        cbiBEF.Tag = "99";
                        this.listDEPT.Items.Add(cbiBEF);
                    }

                    //比對代碼檔
                    compareCode_FSPRDDEPTID(m_FormData.FSPRD_DEPT_ID.ToString());
                    compareCode_FSPAYDEPTID(m_FormData.FSPAY_DEPT_ID.ToString());  
                    compareCode_FSOBJID(m_FormData.FSOBJ_ID.ToString());
                    compareCode_FSGRADEID(m_FormData.FSGRADE_ID.ToString());
                    compareCode_FSSRCID(m_FormData.FSSRC_ID.ToString());
                    compareCode_FSTYPEID(m_FormData.FSTYPE.ToString());
                    compareCode_FSAUDID(m_FormData.FSAUD_ID.ToString());
                    compareCode_FSSHOW_TYPE(m_FormData.FSSHOW_TYPE.ToString());
                    compareCode_FSLANG_ID_MAIN(m_FormData.FSLANG_ID_MAIN.ToString());
                    compareCode_FSLANG_ID_SUB(m_FormData.FSLANG_ID_SUB.ToString());
                    compareCode_FSBUY_ID(m_FormData.FSBUY_ID.ToString());
                    compareCode_FSBUYD_ID(m_FormData.FSBUYD_ID.ToString());
                    compareCode_FSCHANNEL(CheckList(m_FormData.FSCHANNEL.ToString()));
                    comparePRO_TYPE(m_FormData.FCPRO_TYPE.ToString());
                    compareCode_FSPRDCENID(m_FormData.FSPRDCENID.ToString());
                    m_strBEF_DEPT = m_FormData.FSBEF_DEPT_ID.ToString();
                    m_strAFT_DEPT = m_FormData.FSAFT_DEPT_ID.ToString();
                    tbBEF_DEPT_ID.Text = compareCode_DEPT(CheckList(m_FormData.FSBEF_DEPT_ID.ToString()));
                    tbAFT_DEPT_ID.Text = compareCode_DEPT(CheckList(m_FormData.FSAFT_DEPT_ID.ToString()));
                    compareCode_FSPROGSPEC(CheckList(m_FormData.FSPROGSPEC.ToString()));
                    compareProgD(m_FormData.FCPROGD.ToString());

                    //加入新的三個欄位
                    if (m_FormData.FDEXPIRE_DATE.ToShortDateString() == "1900/1/1")
                    {
                        dpEXPIRE.Text = "";
                    }
                    else
                        dpEXPIRE.Text = m_FormData.FDEXPIRE_DATE.ToShortDateString();
                    if (m_FormData.FSEXPIRE_DATE_ACTION == "Y")
                    {
                        cbDELETE.IsChecked = true;
                    }
                    else
                    {
                        cbDELETE.IsChecked = false;
                    }
                    cbNATION_ID.SelectedItem = cbNATION_ID.Items.Where(C=>((ComboBoxItem)C).Tag.ToString() == m_FormData.FSPROGNATIONID).FirstOrDefault();   //國家代碼

                    //clientPRO.fnQUERYTBPROPOSAL_BYIDAsync(strFromID.ToString().Trim());
                    
                    if (m_FormData.FCEMERGENCY.Trim() == "Y")    //緊急成案
                        cbEmergency.IsChecked = true;
                }
            }
        }


        private string[] CheckList(string strChannel)
        {
            char[] delimiterChars = { ';' };
            string[] words = strChannel.Split(delimiterChars);
            return words;
        }

        //選擇外購類別，要判斷顯示哪些外購類別細項
        private void cbBUY_ID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.cbBUYD_IDS.Items.Clear();     //使用前先清空
            string strBUYID = ""; //外購類別大項
            ComboBox cb = sender as ComboBox;
            ComboBoxItem cbm = (ComboBoxItem)cb.SelectedItem;
            strBUYID = cbm.Tag.ToString();

            for (int i = 0; i < cbBUYD_ID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbBUYD_ID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strBUYID))
                {
                    ComboBoxItem cbiPROGBUYDS = new ComboBoxItem();
                    cbiPROGBUYDS.Content = getcbi.Content;
                    cbiPROGBUYDS.Tag = getcbi.DataContext;
                    this.cbBUYD_IDS.Items.Add(cbiPROGBUYDS);
                }
            }
        }

        #endregion

        #region 比對代碼檔

        //比對代碼檔_製作部門
        void compareCode_FSPRDDEPTID(string strID)
        {
            for (int i = 0; i < cbPRD_DEPT_ID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPRD_DEPT_ID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbPRD_DEPT_ID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_付款部門
        void compareCode_FSPAYDEPTID(string strID)
        {
            for (int i = 0; i < cbPAY_DEPT_ID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPAY_DEPT_ID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbPAY_DEPT_ID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_製作目的
        void compareCode_FSOBJID(string strID)
        {
            for (int i = 0; i < cbOBJ_ID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbOBJ_ID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbOBJ_ID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_節目分級
        void compareCode_FSGRADEID(string strID)
        {
            for (int i = 0; i < cbPROGGRADEID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGGRADEID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbPROGGRADEID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_節目來源
        void compareCode_FSSRCID(string strID)
        {
            for (int i = 0; i < cbSRCID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbSRCID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbSRCID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_節目型態
        void compareCode_FSTYPEID(string strID)
        {
            for (int i = 0; i < cbTYPE.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbTYPE.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbTYPE.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_目標觀眾
        void compareCode_FSAUDID(string strID)
        {
            for (int i = 0; i < cbAUD_ID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbAUD_ID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbAUD_ID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_表現方式
        void compareCode_FSSHOW_TYPE(string strID)
        {
            for (int i = 0; i < cbSHOW_TYPE.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbSHOW_TYPE.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbSHOW_TYPE.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_主聲道
        void compareCode_FSLANG_ID_MAIN(string strID)
        {
            for (int i = 0; i < cbLANG_ID_MAIN.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbLANG_ID_MAIN.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbLANG_ID_MAIN.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_副聲道
        void compareCode_FSLANG_ID_SUB(string strID)
        {
            for (int i = 0; i < cbLANG_ID_SUB.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbLANG_ID_SUB.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbLANG_ID_SUB.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_外購類別大類
        void compareCode_FSBUY_ID(string strID)
        {
            for (int i = 0; i < cbBUY_ID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbBUY_ID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbBUY_ID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_外購類別細類
        void compareCode_FSBUYD_ID(string strID)
        {
            for (int i = 0; i < cbBUYD_IDS.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbBUYD_IDS.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbBUYD_IDS.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_可播映頻道
        void compareCode_FSCHANNEL(string[] ListTEST_Send)
        {
            //MessageBox.Show(
            //listCHANNEL.Items.ElementAt(0).GetType().ToString());

            for (int i = 0; i < ListTEST_Send.Length - 1; i++)
            {
                for (int j = 0; j < listCHANNEL.Items.Count; j++)
                {
                    CheckBox getcki = (CheckBox)listCHANNEL.Items.ElementAt(j);

                    if (getcki.Tag.ToString().Trim().Equals(ListTEST_Send[i]))
                    {
                        getcki.IsChecked = true;
                        break;
                    }
                }
            }
        }

        //比對代碼檔_節目規格
        void compareCode_FSPROGSPEC(string[] ListTEST_Send)
        {
            for (int i = 0; i < ListTEST_Send.Length - 1; i++)
            {
                for (int j = 0; j < listPROGSPEC.Items.Count; j++)
                {
                    CheckBox getcki = (CheckBox)listPROGSPEC.Items.ElementAt(j);

                    if (getcki.Tag.ToString().Trim().Equals(ListTEST_Send[i]))
                    {
                        getcki.IsChecked = true;
                        break;
                    }
                }
            }
        }

        //比對代碼檔_前後會部門
        string compareCode_DEPT(string[] ListTEST_Send)
        {
            string strDept = "";
            for (int i = 0; i < ListTEST_Send.Length - 1; i++)
            {
                for (int j = 0; j < listDEPT.Items.Count; j++)
                {
                    CheckBox getcki = (CheckBox)listDEPT.Items.ElementAt(j);

                    if (getcki.Tag.ToString().Trim().Equals(ListTEST_Send[i]))
                    {
                        getcki.IsChecked = true;
                        strDept += getcki.Content.ToString().Trim() + ";";
                    }
                }
            }
            return strDept;
        }

        //比對節目種類
        void comparePRO_TYPE(string strID)
        {
            for (int i = 0; i < cbPRO_TYPE.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPRO_TYPE.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbPRO_TYPE.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_製作單位代碼
        void compareCode_FSPRDCENID(string strID)
        {
            for (int i = 0; i < cbPRDCENID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPRDCENID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbPRDCENID.SelectedIndex = i;
                    break;
                }
            }
        }

        //是否為產生子集
        private void compareProgD(string strProgD)
        {
            if (strProgD == "N")
                cbPROGD.SelectedIndex = 0;
            else if (strProgD == "Y")
                cbPROGD.SelectedIndex = 1;
        } 

        #endregion
               
        //前會部門
        private void btnBEF_DEPT_ID_Click(object sender, RoutedEventArgs e)
        {
            PRO.PRO100_07 PRO100_07_frm = new PRO.PRO100_07(m_strBEF_DEPT, "BEF");
            PRO100_07_frm.Show();

            PRO100_07_frm.Closing += (s, args) =>
            {
                if (PRO100_07_frm.DialogResult == true)
                {
                    m_strBEF_DEPT = PRO100_07_frm.m_Dept;
                    tbBEF_DEPT_ID.Text = PRO100_07_frm.m_DeptName;
                }
            };
        }

        //後會部門
        private void btnAFT_DEPT_ID_Click(object sender, RoutedEventArgs e)
        {
            PRO.PRO100_07 PRO100_07_frm = new PRO.PRO100_07(m_strAFT_DEPT, "AFT");
            PRO100_07_frm.Show();

            PRO100_07_frm.Closing += (s, args) =>
            {
                if (PRO100_07_frm.DialogResult == true)
                {
                    m_strAFT_DEPT = PRO100_07_frm.m_Dept;
                    tbAFT_DEPT_ID.Text = PRO100_07_frm.m_DeptName;
                }
            };
        }
    
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
          
    }
}

