﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Xml.Linq;
using System.IO;
using System.Text;
using PTS_MAM3.WSPROPOSAL;
using System.Windows.Browser;
using System.Windows.Data;

namespace PTS_MAM3.Proposal
{
    public partial class PROPOSAL : Page
    {
        WSPROPOSALSoapClient client = new WSPROPOSALSoapClient();//產生新的代理類別
        List<Class_PROPOSAL> m_ListFormData = new List<Class_PROPOSAL>();  //記錄查詢到的成案資料

        public PROPOSAL()
        {
            InitializeComponent();

            //取得所有成案檔
            client.fnGetTBPROPOSAL_ALLCompleted += new EventHandler<fnGetTBPROPOSAL_ALLCompletedEventArgs>(client_fnGetTBPROPOSAL_ALLCompleted);
            //client.fnGetTBPROPOSAL_ALLAsync();

            //取得所有成案檔_BY新增者
            client.fnGetTBPROPOSAL_ALL_BYUSERIDCompleted += new EventHandler<fnGetTBPROPOSAL_ALL_BYUSERIDCompletedEventArgs>(client_fnGetTBPROPOSAL_ALL_BYUSERIDCompleted);
           
            //列印成案單
            client.CallREPORT_PROCompleted += new EventHandler<CallREPORT_PROCompletedEventArgs>(client_CallREPORT_PROCompleted);

            Load_TBPROPOSAL_ALL_BYUSERID();

            //緊急成案的按鈕要卡權限
            if (ModuleClass.getModulePermissionByName("01", "緊急成案") == false)
                btnEmergency.IsEnabled = false;
        }

        //載入成案資料_BY新增者
        private void Load_TBPROPOSAL_ALL_BYUSERID()
        {            
            client.fnGetTBPROPOSAL_ALL_BYUSERIDAsync(UserClass.userData.FSUSER_ID.ToString().Trim());
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }

        //實作-列印成案單
        void client_CallREPORT_PROCompleted(object sender, CallREPORT_PROCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    if (e.Result.ToString().StartsWith("錯誤"))
                        MessageBox.Show(e.Result.ToString(), "提示訊息", MessageBoxButton.OK);
                    else
                        HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + e.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,resizable=yes");
                        //HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + e.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,toolbar=0,statusbar=0,menubar=0,scrollbar=0");
                }
                else
                    MessageBox.Show("查無相關資料，或該筆資料已不存在資料庫中。", "提示訊息", MessageBoxButton.OK);
            }             
        }

        //實作-取得所有成案檔
        void client_fnGetTBPROPOSAL_ALLCompleted(object sender, fnGetTBPROPOSAL_ALLCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null )
                {
                    //宣告PagedCollectionView,將撈取的資料放入
                    PagedCollectionView pcv = new PagedCollectionView(e.Result);
                    //指定DataGrid以及DataPager的內容
                    DGPROPOSALListDouble.ItemsSource = pcv; //DoubleClick的DataGrid
                    DataPager.Source = pcv;
                }
            }            
        }

        //實作-取得所有成案檔_BY新增者
        void client_fnGetTBPROPOSAL_ALL_BYUSERIDCompleted(object sender, fnGetTBPROPOSAL_ALL_BYUSERIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null )
                {
                    //宣告PagedCollectionView,將撈取的資料放入
                    PagedCollectionView pcv = new PagedCollectionView(e.Result);
                  
                    //指定DataGrid以及DataPager的內容
                    DGPROPOSALListDouble.ItemsSource = pcv; //DoubleClick的DataGrid
                    DataPager.Source = pcv;
                }
            }

            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        //新增成案
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            PROPOSAL_Add PROPOSAL_Add_frm = new PROPOSAL_Add();
            PROPOSAL_Add_frm.Show();

            PROPOSAL_Add_frm.Closing += (s, args) =>
           {
               if (PROPOSAL_Add_frm.DialogResult == true)
                   Load_TBPROPOSAL_ALL_BYUSERID();         
            };
        }

        //修改成案
        private void btnModify_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGPROPOSALListDouble.SelectedItem == null)
            {
                MessageBox.Show("請選擇預修改的節目成案資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            if (((Class_PROPOSAL)DGPROPOSALListDouble.SelectedItem).FCCHECK_STATUS != "N")
            {
                MessageBox.Show("該筆成案資料不是「審核中」，無法修改", "提示訊息", MessageBoxButton.OK);
                return;
            }


            //檢查製作目的權限者皆可修改
            //if (checkPermission() == false)
            //    return;
            

            //原本修改成案單是限制本人，現在開放有製作目的權限者皆可修改
            //if (((Class_PROPOSAL)DGPROPOSALListDouble.SelectedItem).FSCREATED_BY != UserClass.userData.FSUSER_ID.ToString().Trim())
            //{
            //    MessageBox.Show("該筆成案資料不是「本帳號申請」，無法修改", "提示訊息", MessageBoxButton.OK);
            //    return;
            //}

            //2013/04/27 by Jarvis 成案單開放原作者與製播權限者修改
            //(合併上面兩段註解原條件判斷與提示訊息),使用公視節目部員工帳號登入測試
            bool checkResault=checkPermission();
            bool isNotMy=((Class_PROPOSAL)DGPROPOSALListDouble.SelectedItem).FSCREATED_BY != UserClass.userData.FSUSER_ID.ToString().Trim();
            if (!checkResault && isNotMy)
            {
                //MessageBox.Show("沒有該筆成案資料「" + ((Class_PROPOSAL)DGPROPOSALListDouble.SelectedItem).FSOBJ_ID_NAME.Trim() + "」製作目的的權限，或不是「本帳號申請」，無法修改", "提示訊息", MessageBoxButton.OK);
                MessageBox.Show("無法修改此筆資料。"+Environment.NewLine+"(此登入帳號無此資料之製作目的權限且非申請帳號)","提示訊息",MessageBoxButton.OK);
                return;
            }

            PROPOSAL_Edit PROPOSAL_Edit_frm = new PROPOSAL_Edit(((Class_PROPOSAL)DGPROPOSALListDouble.SelectedItem));
            PROPOSAL_Edit_frm.Show();

            PROPOSAL_Edit_frm.Closing += (s, args) =>
            {
                if (PROPOSAL_Edit_frm.DialogResult == true)
                    Load_TBPROPOSAL_ALL_BYUSERID();             
            };
        }

        //查詢成案
        private void btnQuery_Click(object sender, RoutedEventArgs e)
        {
            PROPOSAL_Query PROPOSAL_Query_frm = new PROPOSAL_Query();
            PROPOSAL_Query_frm.Show();

            PROPOSAL_Query_frm.Closed += (s, args) =>
                {
                    if (PROPOSAL_Query_frm.m_ListFormData.Count!=0)
                    {
                        PagedCollectionView pcv = new PagedCollectionView(PROPOSAL_Query_frm.m_ListFormData);
                        //指定DataGrid以及DataPager的內容
                        DGPROPOSALListDouble.ItemsSource = pcv; //DoubleClick的DataGrid
                        DataPager.Source = pcv;
                    }
                };
        }
        
        //更新成案
        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            Load_TBPROPOSAL_ALL_BYUSERID();  
        }

        //列印成案單
        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGPROPOSALListDouble.SelectedItem == null)
            {
                MessageBox.Show("請選擇預列印的節目成案資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //列印成案單(丟入參數：成案單編號、查詢者、SessionID)
            client.CallREPORT_PROAsync(((Class_PROPOSAL)DGPROPOSALListDouble.SelectedItem).FSPRO_ID.ToString().Trim(),UserClass.userData.FSUSER_ID.ToString().Trim(),UserClass.userData.FSSESSION_ID.ToString().Trim());
        }

        //緊急成案
        private void btnEmergency_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGPROPOSALListDouble.SelectedItem == null)
            {
                MessageBox.Show("請選擇預修改的節目成案資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            if (((Class_PROPOSAL)DGPROPOSALListDouble.SelectedItem).FCEMERGENCY != "Y")
            {
                MessageBox.Show("該筆成案資料不是緊急流程，無法修改", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //檢查製作目的權限者皆可修改
            if (checkPermission() == false)
                return;

            //原本修改成案單是限制本人，現在開放有製作目的權限者皆可修改
            //if (((Class_PROPOSAL)DGPROPOSALListDouble.SelectedItem).FSCREATED_BY != UserClass.userData.FSUSER_ID.ToString().Trim())
            //{
            //    MessageBox.Show("該筆成案資料不是「本帳號申請」，無法修改", "提示訊息", MessageBoxButton.OK);
            //    return;
            //}

            PRO100_08 PRO100_08_frm = new PRO100_08(((Class_PROPOSAL)DGPROPOSALListDouble.SelectedItem));
            PRO100_08_frm.Show();

            PRO100_08_frm.Closing += (s, args) =>
            {
                if (PRO100_08_frm.DialogResult == true)
                    Load_TBPROPOSAL_ALL_BYUSERID();
            };
        }
        
        //檢查製作目的權限
        private Boolean checkPermission()
        {
            //判斷有無修改此製作目的的權限，若是無，就讓使用者無法修改
            if (ModuleClass.getModulePermissionByName("00", ((Class_PROPOSAL)DGPROPOSALListDouble.SelectedItem).FSOBJ_ID_NAME.Trim()) == false)
            {
                //MessageBox.Show("無法使用，因為沒有製作目的「" + ((Class_PROPOSAL)DGPROPOSALListDouble.SelectedItem).FSOBJ_ID_NAME.Trim() + "」的權限", "提示訊息", MessageBoxButton.OK);
                return false;
            }
            return true;
        }

        //連續點擊-檢視成案檔 
        private void DGPROPOSALListDouble_RowDoubleClicked(object sender, DataGridDCTest.DataGridRowClickedArgs e)
        {
            DataGridDCTest.DoubleClickDataGrid dcdg = sender as DataGridDCTest.DoubleClickDataGrid;       

            //判斷是否有選取DataGrid
            if (dcdg.SelectedItem == null)
            {
                MessageBox.Show("請選擇預檢視的節目成案資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            PRO100_03 PRO100_03_frm = new PRO100_03(((Class_PROPOSAL)DGPROPOSALListDouble.SelectedItem));
            PRO100_03_frm.Show();
        }

        //列印緊急成案
        private void btnEmergencyPrint_Click(object sender, RoutedEventArgs e)
        {
            PRO100_10 PRO100_10_frm = new PRO100_10();
            PRO100_10_frm.Show();

            PRO100_10_frm.Closing += (s, args) =>
            {
                if (PRO100_10_frm.DialogResult == true)
                {
                    //列印成案單(丟入參數：成案單編號、查詢者、SessionID)
                    client.CallREPORT_PROAsync(((Class_PROPOSAL)PRO100_10_frm.DGPROPOSALList.SelectedItem).FSPRO_ID.ToString().Trim(), UserClass.userData.FSUSER_ID.ToString().Trim(), UserClass.userData.FSSESSION_ID.ToString().Trim()); 
                }                   
            };
        }        

        //取得DataGeid的內容
        //private void button1_Click(object sender, RoutedEventArgs e)
        //{MessageBox.Show (((Class_PROPOSAL)DGPROPOSALList.SelectedItem).FSPROG_NAME.ToString() );
        //}

        //取得DataGeid的內容
        //private void DGPROPOSALList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{           
        //    //MessageBox.Show((e.AddedItems[0] as Class_PROPOSAL).FSPROG_NAME);
            //MessageBox.Show(e.AddedItems[0].GetType().ToString()); 
            //MessageBox.Show (((Class_PROPOSAL) e.AddedItems[0]).FSPROG_NAME );
        //}


    }
}
