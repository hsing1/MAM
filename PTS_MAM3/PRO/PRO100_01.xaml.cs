﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using System.Xml.Linq;
using System.IO;
using PTS_MAM3.PROG_M;
using PTS_MAM3.WSPROG_M;
using PTS_MAM3.WSMAMFunctions;
using PTS_MAM3.WSPROPOSAL;
using PTS_MAM3.PRG;
using PTS_MAM3.FlowMAM;
using PTS_MAM3.WSPROG_PRODUCER;

namespace PTS_MAM3.Proposal
{
    public partial class PROPOSAL_Add : ChildWindow
    { //產生新的代理類別 by Jarvis20130429
        WSPROG_PRODUCERSoapClient clientPRODUCER = new WSPROG_PRODUCERSoapClient();
        List<UserStruct> ListUserAll = new List<UserStruct>();
        Class_PROGPRODUCER ProducerObj = null;


        WSPROG_MSoapClient client = new WSPROG_MSoapClient();                   //產生新的代理類別
        WSMAMFunctionsSoapClient clientMAM = new WSMAMFunctionsSoapClient();
        WSPROPOSALSoapClient clientPRO = new WSPROPOSALSoapClient();
        //flowWebService.FlowSoapClient FlowClinet = new flowWebService.FlowSoapClient(); //流程引擎
        FlowMAMSoapClient clientFlow = new FlowMAMSoapClient();                         //後端的流程引擎

        string m_strProgID = "";                                                //暫存節目編號  
        string m_strProgName = "";                                              //暫存節目名稱
        string m_strBEF_DEPT = "";
        string m_strAFT_DEPT = "";
        public Class_PROPOSAL m_Queryobj = new Class_PROPOSAL();                //查詢新增的obj
        Boolean m_bolPRO_TYPE = false;                                          //判斷是不是第一次進來
        List<Class_PROGOBJ_DEPT> PROGOBJ_DEPT_obj = new List<Class_PROGOBJ_DEPT>();
        List<Class_CODE> m_CodeDataObj = new List<Class_CODE>();
        List<Class_CODE> m_CodeDataDept = new List<Class_CODE>();
        int intCountFlow = 0;                                                   //判斷流程若是失敗重複呼叫

        public PROPOSAL_Add()
        {
            InitializeComponent();
            InitializeForm();      //初始化本頁面

            this.Loaded += new RoutedEventHandler(PROPOSAL_Add_Loaded);
        }

        //by Jarvis20130527
        void PROPOSAL_Add_Loaded(object sender, RoutedEventArgs e)
        {
            this.cbPRO_TYPE.Focus();

            #region 匯出TabIndex設定給其他頁面用 //Jarvis20130527
            //record字串的值,複製貼上到其他頁面的.cs檔即可
            //Dictionary<string, int> controlTabIndex = new Dictionary<string, int>();
            //string record = "";
            //foreach (UIElement control in this.gdPROPOSAL.Children)
            //{
            //    if (control is TextBlock)
            //    {
            //    }
            //    else
            //    {
            //        int index = Convert.ToInt32(control.GetValue(TabIndexProperty));
            //        if (index < 100)
            //        {
            //            record +="this."+ control.GetValue(Control.NameProperty).ToString()+".TabIndex=" + index+";";
            //            //controlTabIndex.Add(control.GetValue(Control.NameProperty).ToString(), index); ;
            //            // controlTabIndex.Add(control.Name, control.TabIndex);
            //        }
            //    }
            //}
            #endregion
        }

        //初始化本頁面
        void InitializeForm()
        {
            //下載代碼檔
            client.fnGetTBPROG_M_CODECompleted += new EventHandler<fnGetTBPROG_M_CODECompletedEventArgs>(client_fnGetTBPROG_M_CODECompleted);

            //下載來源國家代碼檔

            //下載外購類別細項代碼檔
            client.fnGetTBPROG_M_PROGBUYD_CODECompleted += new EventHandler<fnGetTBPROG_M_PROGBUYD_CODECompletedEventArgs>(client_fnGetTBPROG_M_PROGBUYD_CODECompleted);

            //抓取成案單編號
            clientMAM.GetNoRecordCompleted += new EventHandler<GetNoRecordCompletedEventArgs>(clientMAM_GetNoRecordCompleted);
            clientMAM.GetNoRecordAsync("07", "", "", UserClass.userData.FSUSER_ID);

            //新增成案資料
            clientPRO.fnINSERTTBPROPOSALCompleted += new EventHandler<WSPROPOSAL.fnINSERTTBPROPOSALCompletedEventArgs>(clientPRO_fnINSERTTBPROPOSALCompleted);

            //查詢是否有相同名稱的節目資料
            clientPRO.QUERY_TBPROGNAME_CHECKCompleted += new EventHandler<QUERY_TBPROGNAME_CHECKCompletedEventArgs>(clientPRO_QUERY_TBPROGNAME_CHECKCompleted);

            //查詢成案主檔透過節目編號
            clientPRO.QUERYTBPROPOSAL_BYPROGIDCompleted += new EventHandler<QUERYTBPROPOSAL_BYPROGIDCompletedEventArgs>(clientPRO_QUERYTBPROPOSAL_BYPROGIDCompleted);

            //查詢製作目的與前後會部門對照檔列表           
            clientPRO.QueryTBPROGOBJ_DEPT_LISTCompleted += new EventHandler<QueryTBPROGOBJ_DEPT_LISTCompletedEventArgs>(clientPRO_QueryTBPROGOBJ_DEPT_LISTCompleted);

            //透過節目編號查詢節目基本資料
            client.fnGetProg_MCompleted += new EventHandler<fnGetProg_MCompletedEventArgs>(client_fnGetProg_MCompleted);

            //流程引擎           
            //FlowClinet.NewFlowWithFieldCompleted += new EventHandler<flowWebService.NewFlowWithFieldCompletedEventArgs>(FlowClinet_NewFlowWithFieldCompleted);

            //後端的流程引擎-流程起始
            clientFlow.CallFlow_NewFlowWithFieldCompleted += new EventHandler<CallFlow_NewFlowWithFieldCompletedEventArgs>(clientFlow_CallFlow_NewFlowWithFieldCompleted);


            //查詢所有使用者資料  by Jarvis20130429
            clientPRODUCER.QUERY_TBUSERS_ALLCompleted += new EventHandler<QUERY_TBUSERS_ALLCompletedEventArgs>(clientPRODUCER_QUERY_TBUSERS_ALLCompleted);
            clientPRODUCER.QUERY_TBUSERS_ALLAsync();

            //新增製作人資料進TBPROG_PRODUCER  by Jarvis20130429
            clientPRODUCER.INSERT_TBPROG_PRODUCERCompleted += new EventHandler<INSERT_TBPROG_PRODUCERCompletedEventArgs>(clientPRODUCER_INSERT_TBPROG_PRODUCERCompleted);

            clientPRODUCER.QUERY_TBPROG_PRODUCERCompleted += new EventHandler<QUERY_TBPROG_PRODUCERCompletedEventArgs>(clientPRODUCER_QUERY_TBPROG_PRODUCERCompleted);

            clientPRO.QueryNoRecordPROG_IDCompleted += new EventHandler<QueryNoRecordPROG_IDCompletedEventArgs>(clientPRO_QueryNoRecordPROG_IDCompleted);

            BusyMsg.IsBusy = true;

            //緊急成案的按鈕要卡權限
            if (ModuleClass.getModulePermissionByName("01", "緊急成案") == false)
                cbEmergency.IsEnabled = false;
        }



        #region 實作
        //實作-查詢製作人  by Jarvis20130614
        void clientPRODUCER_QUERY_TBPROG_PRODUCERCompleted(object sender, QUERY_TBPROG_PRODUCERCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result.Count > 0)//結果數大於0表示是續製的節目
                {
                    m_Queryobj.FSPRODUCER = e.Result[0].FSPRODUCER;
                    UserStruct us = new UserStruct();
                    us.FSUSER_ID = e.Result[0].FSPRODUCER;
                    us.FSUSER_ChtName = e.Result[0].FSPRODUCER_NAME;
                    us.FSTITLE_NAME = "";
                    acbMenID.SelectedItem=us;
                    //ListUserAll[0].FSUPPERDEPT_CHTNAME = e.Result[0].FSPRODUCER_NAME;
                }
            }
        }

        //實作-查詢所有使用者資料  by Jarvis20130429
        void clientPRODUCER_QUERY_TBUSERS_ALLCompleted(object sender, QUERY_TBUSERS_ALLCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                ListUserAll = e.Result;
                acbMenID.ItemsSource = ListUserAll; //繫結AutoComplete按鈕
            }
            else
                MessageBox.Show("查詢使用者資料異常！", "提示訊息", MessageBoxButton.OK);

        }

        //實作-抓取成案單編號
        void clientMAM_GetNoRecordCompleted(object sender, GetNoRecordCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                    tbxPRO_ID.Text = e.Result;
                else
                {
                    MessageBox.Show("成案單編號讀取異常，請聯絡系統管理員！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
            else
            {
                MessageBox.Show("成案單編號讀取異常，請聯絡系統管理員！", "提示訊息", MessageBoxButton.OK);
                this.DialogResult = false;
            }

            client.fnGetTBPROG_M_CODEAsync();   //下載代碼檔
        }

        //實作-讀取新增成案結果
        void clientPRO_fnINSERTTBPROPOSALCompleted(object sender, WSPROPOSAL.fnINSERTTBPROPOSALCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    //clientPRO.QueryNoRecordPROG_IDAsync(tbxPROG_NAME.Text.ToString().Trim(), UserClass.userData.FSUSER_ID.ToString());

                    newAFlow();
                    //必須等待流程也成功才彈出成功訊息
                    //MessageBox.Show("新增成案資料「" + m_strProgName.ToString() + "」成功！", "提示訊息", MessageBoxButton.OK);
                    //this.DialogResult = true; 
                }
                else
                {
                    MessageBox.Show("新增成案資料「" + m_strProgName.ToString() + "」失敗！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
        }

        void clientPRO_QueryNoRecordPROG_IDCompleted(object sender, QueryNoRecordPROG_IDCompletedEventArgs e)
        {
            if(e.Error == null)
            {
                //if (e.Result != "")
                //{
                    m_strProgID = e.Result;
                    clientPRODUCER.INSERT_TBPROG_PRODUCERAsync(FormToClass_PROGPRODUCER());
                //}
            }
        }

        void clientPRODUCER_INSERT_TBPROG_PRODUCERCompleted(object sender, INSERT_TBPROG_PRODUCERCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {
                    
                }    //確定成案資料與製作人資料已建立後，才去啟動流程  修改 By Jarvis20130531
            }
        }

        //實作-查詢是否有相同名稱的節目資料
        void clientPRO_QUERY_TBPROGNAME_CHECKCompleted(object sender, QUERY_TBPROGNAME_CHECKCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result == false)
                {
                    //新增成案單資料(儲存時才去取節目編號)                          
                    clientPRO.fnINSERTTBPROPOSALAsync(FormToClass_Proposal());
                    BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息      
                }
                else
                    MessageBox.Show("已有重複的「節目名稱」資料，請檢查後重新輸入！", "提示訊息", MessageBoxButton.OK);
            }
        }

        //實作-查詢成案主檔透過節目編號
        void clientPRO_QUERYTBPROPOSAL_BYPROGIDCompleted(object sender, QUERYTBPROPOSAL_BYPROGIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    Class_PROPOSAL FormData = new Class_PROPOSAL();
                    FormData = (Class_PROPOSAL)(e.Result[0]);

                    if (FormData.SHOW_FDSHOW_DATE.ToString() == "1900/1/1" || FormData.SHOW_FDSHOW_DATE.ToString() == "")
                        dpSHOW_DATE.Text = "";
                    else
                        dpSHOW_DATE.Text = FormData.FDSHOW_DATE.ToString();  //日期格式比較特別，用指定的方式

                    tbxLENGTH.Text = FormData.FNLENGTH.ToString();

                    if (cbPRO_TYPE.SelectedIndex != 1)  //續製的開始及結束及別要往後算
                    {
                        tbxBEG_EPISODE.Text = FormData.FNBEG_EPISODE.ToString();
                        tbxEND_EPISODE.Text = FormData.FNEND_EPISODE.ToString();
                    }
                    else
                    {
                        client.fnGetProg_MAsync(FormData.FSPROG_ID.Trim()); //開始以及結束集數要參考節目主檔
                        BusyMsg.IsBusy = true;
                    }
                    //Class_PROGPRODUCER class_producer = new Class_PROGPRODUCER();
                    //class_producer.FSID = FormData.FSPRO_ID;
                    //class_producer.FSPRO_TYPE= 
                    //clientPRODUCER.QUERY_TBPROG_PRODUCERAsync();

                    tbxCONTENT.Text = FormData.FSCONTENT.ToString();
                    tbxPGMENAME.Text = FormData.FSNAME_FOR;
                    tbxWEBNAME.Text = FormData.FSPLAY_NAME;
                    tbxENOTE.Text = FormData.FSENOTE;

                    //比對代碼檔
                    compareCode_FSPRDDEPTID(FormData.FSPRD_DEPT_ID.ToString());
                    compareCode_FSPAYDEPTID(FormData.FSPAY_DEPT_ID.ToString());
                    compareCode_FSOBJID(FormData.FSOBJ_ID.ToString());
                    compareCode_FSGRADEID(FormData.FSGRADE_ID.ToString());
                    compareCode_FSSRCID(FormData.FSSRC_ID.ToString());
                    compareCode_FSTYPEID(FormData.FSTYPE.ToString());
                    compareCode_FSAUDID(FormData.FSAUD_ID.ToString());
                    compareCode_FSSHOW_TYPE(FormData.FSSHOW_TYPE.ToString());
                    compareCode_FSLANG_ID_MAIN(FormData.FSLANG_ID_MAIN.ToString());
                    compareCode_FSLANG_ID_SUB(FormData.FSLANG_ID_SUB.ToString());
                    compareCode_FSBUY_ID(FormData.FSBUY_ID.ToString());
                    compareCode_FSBUYD_ID(FormData.FSBUYD_ID.ToString());
                    compareCode_FSCHANNEL(CheckList(FormData.FSCHANNEL.ToString()));
                    tbBEF_DEPT_ID.Text = compareCode_DEPT(CheckList(FormData.FSBEF_DEPT_ID.ToString()));
                    tbAFT_DEPT_ID.Text = compareCode_DEPT(CheckList(FormData.FSAFT_DEPT_ID.ToString()));
                    m_strBEF_DEPT = FormData.FSBEF_DEPT_ID.ToString();
                    m_strAFT_DEPT = FormData.FSAFT_DEPT_ID.ToString();
                    compareCode_FSPROGSPEC(CheckList(FormData.FSPROGSPEC.ToString()));
                    compareProgD(FormData.FCPROGD.ToString());
                    compareCode_FSPRDCENID(FormData.FSPRDCENID.ToString());
                }
            }
        }

        //實作-查詢製作目的與前後會部門對照檔列表 
        void clientPRO_QueryTBPROGOBJ_DEPT_LISTCompleted(object sender, QueryTBPROGOBJ_DEPT_LISTCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                PROGOBJ_DEPT_obj = e.Result;

                for (int i = 0; i < PROGOBJ_DEPT_obj.Count; i++)
                {
                    PROGOBJ_DEPT_obj[i].SHOW_FSOBJ_ID = Check_OBJ_NAME(PROGOBJ_DEPT_obj[i].FSOBJ_ID);
                    PROGOBJ_DEPT_obj[i].SHOW_FSBEF_DEPT_ID = Check_DEPT_NAME_List(CheckList(PROGOBJ_DEPT_obj[i].FSBEF_DEPT_ID));
                    PROGOBJ_DEPT_obj[i].SHOW_FSAFT_DEPT_ID = Check_DEPT_NAME_List(CheckList(PROGOBJ_DEPT_obj[i].FSAFT_DEPT_ID));
                }

            }
        }

        //實作-透過節目編號查詢節目基本資料(續製的節目集數要抓節目主檔的總集數)
        void client_fnGetProg_MCompleted(object sender, fnGetProg_MCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;

            if (e.Error == null)
            {
                List<Class_PROGM> Listobj = new List<Class_PROGM>();
                Class_PROGM obj = new Class_PROGM();

                Listobj = e.Result;

                if (Listobj.Count > 0)
                {
                    obj = Listobj[0];
                }
                else
                    return;

                if (cbPRO_TYPE.SelectedIndex == 1)  //續製的開始及結束集別，透過節目主檔往後算
                {
                    tbxBEG_EPISODE.Text = (obj.FNTOTEPISODE + 1).ToString();
                    tbxEND_EPISODE.Text = "";
                }
                else
                {   //新製及衍生要把集數清掉
                    tbxBEG_EPISODE.Text = "1";
                    tbxEND_EPISODE.Text = "";
                }

                Class_PROGM FormData = new Class_PROGM();
                FormData = (Class_PROGM)(e.Result[0]);

                Class_PROGPRODUCER producer = new Class_PROGPRODUCER();
                producer.FSID = FormData.FSPROG_ID;
                producer.FSPRO_TYPE = "G";
                producer.FSPRODUCER = "";

                clientPRODUCER.QUERY_TBPROG_PRODUCERAsync(producer);
                if (FormData.SHOW_FDSHOWDATE == "")
                    dpSHOW_DATE.Text = "";
                else
                    dpSHOW_DATE.Text = FormData.FDSHOWDATE.ToString();      //日期格式比較特別，用指定的方式    

                tbxLENGTH.Text = FormData.FNLENGTH.ToString();

                if (cbPRO_TYPE.SelectedIndex == 2)
                    tbxCONTENT.Text = FormData.FSCONTENT.ToString();        //類型為衍生時，內容才要預帶  

                tbxPGMENAME.Text = FormData.FSPGMENAME;
                tbxWEBNAME.Text = FormData.FSWEBNAME;

                //比對代碼檔    
                compareCode_FSPRDDEPTID(FormData.FSPRDDEPTID.ToString());   //製作部門                   
                compareCode_FSOBJID(FormData.FSPROGOBJID.ToString());       //製作目的    
                compareCode_FSGRADEID(FormData.FSPROGGRADEID.ToString());   //分級    
                compareCode_FSSRCID(FormData.FSPROGSRCID.ToString());       //來源    
                compareCode_FSTYPEID(FormData.FSPROGATTRID.ToString());     //內容屬性(節目型態)    
                compareCode_FSAUDID(FormData.FSPROGAUDID.ToString());       //目標觀眾    
                compareCode_FSSHOW_TYPE(FormData.FSPROGTYPEID.ToString());  //表現方式    
                compareCode_FSLANG_ID_MAIN(FormData.FSPROGLANGID1.ToString());//主聲道    
                compareCode_FSLANG_ID_SUB(FormData.FSPROGLANGID2.ToString());//副聲道    
                compareCode_FSBUY_ID(FormData.FSPROGBUYID.ToString());      //外購大項    
                compareCode_FSBUYD_ID(FormData.FSPROGBUYDID.ToString());    //外購細項    
                compareCode_FSCHANNEL(CheckList(FormData.FSCHANNEL.ToString()));//可播頻道                        
                compareCode_FSPRDCENID(FormData.FSPRDCENID.ToString());     //製作單位  
                cbNATION_ID.SelectedItem = cbNATION_ID.Items.Where(S => ((ComboBoxItem)S).Tag.ToString() == FormData.FSPROGNATIONID).FirstOrDefault();
                if (FormData.FDEXPIRE_DATE >= Convert.ToDateTime("1911/1/1"))
                {
                    dpEXPIRE.Text = FormData.FDEXPIRE_DATE.ToShortDateString();
                }
                if (FormData.FSEXPIRE_DATE_ACTION == "Y")
                    cbDELETE.IsChecked = true;
                else
                    cbDELETE.IsChecked = false;
            }
        }

        //實作--流程起始
        void clientFlow_CallFlow_NewFlowWithFieldCompleted(object sender, CallFlow_NewFlowWithFieldCompletedEventArgs e)
        {
            if (e.Error == null && e.Result != "")
            {
                MessageBox.Show("新增成案資料「" + m_strProgName.ToString() + "」成功！", "提示訊息", MessageBoxButton.OK);
                this.DialogResult = true;
            }
            else
            {
                MessageBox.Show("呼叫流程引擎異常，新增成案資料「" + m_strProgName.ToString() + "」失敗！", "提示訊息", MessageBoxButton.OK);
                this.DialogResult = true;  //雖然流程失敗，但是因為已寫入還是要畫面重新Load 
            }
        }

        #endregion

        #region 比對代碼檔

        private string[] CheckList(string strChannel)
        {
            char[] delimiterChars = { ';' };
            string[] words = strChannel.Split(delimiterChars);
            return words;
        }

        //比對代碼檔_製作部門
        void compareCode_FSPRDDEPTID(string strID)
        {
            for (int i = 0; i < cbPRD_DEPT_ID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPRD_DEPT_ID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbPRD_DEPT_ID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_付款部門
        void compareCode_FSPAYDEPTID(string strID)
        {
            for (int i = 0; i < cbPAY_DEPT_ID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPAY_DEPT_ID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbPAY_DEPT_ID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_製作目的
        void compareCode_FSOBJID(string strID)
        {
            for (int i = 0; i < cbOBJ_ID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbOBJ_ID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbOBJ_ID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_節目分級
        void compareCode_FSGRADEID(string strID)
        {
            for (int i = 0; i < cbPROGGRADEID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGGRADEID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbPROGGRADEID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_節目來源
        void compareCode_FSSRCID(string strID)
        {
            for (int i = 0; i < cbSRCID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbSRCID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbSRCID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_節目型態
        void compareCode_FSTYPEID(string strID)
        {
            for (int i = 0; i < cbTYPE.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbTYPE.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbTYPE.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_目標觀眾
        void compareCode_FSAUDID(string strID)
        {
            for (int i = 0; i < cbAUD_ID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbAUD_ID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbAUD_ID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_表現方式
        void compareCode_FSSHOW_TYPE(string strID)
        {
            for (int i = 0; i < cbSHOW_TYPE.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbSHOW_TYPE.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbSHOW_TYPE.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_主聲道
        void compareCode_FSLANG_ID_MAIN(string strID)
        {
            for (int i = 0; i < cbLANG_ID_MAIN.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbLANG_ID_MAIN.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbLANG_ID_MAIN.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_副聲道
        void compareCode_FSLANG_ID_SUB(string strID)
        {
            for (int i = 0; i < cbLANG_ID_SUB.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbLANG_ID_SUB.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbLANG_ID_SUB.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_外購類別大類
        void compareCode_FSBUY_ID(string strID)
        {
            for (int i = 0; i < cbBUY_ID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbBUY_ID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbBUY_ID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_外購類別細類
        void compareCode_FSBUYD_ID(string strID)
        {
            for (int i = 0; i < cbBUYD_IDS.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbBUYD_IDS.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbBUYD_IDS.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_可播映頻道
        void compareCode_FSCHANNEL(string[] ListTEST_Send)
        {
            //MessageBox.Show(
            //listCHANNEL.Items.ElementAt(0).GetType().ToString());

            for (int i = 0; i < ListTEST_Send.Length - 1; i++)
            {
                for (int j = 0; j < listCHANNEL.Items.Count; j++)
                {
                    CheckBox getcki = (CheckBox)listCHANNEL.Items.ElementAt(j);

                    if (getcki.Tag.ToString().Trim().Equals(ListTEST_Send[i]))
                    {
                        getcki.IsChecked = true;
                        break;
                    }
                }
            }
        }

        //比對代碼檔_節目規格
        void compareCode_FSPROGSPEC(string[] ListTEST_Send)
        {
            for (int i = 0; i < ListTEST_Send.Length - 1; i++)
            {
                for (int j = 0; j < listPROGSPEC.Items.Count; j++)
                {
                    CheckBox getcki = (CheckBox)listPROGSPEC.Items.ElementAt(j);

                    if (getcki.Tag.ToString().Trim().Equals(ListTEST_Send[i]))
                    {
                        getcki.IsChecked = true;
                        break;
                    }
                }
            }
        }

        //比對代碼檔_前後會部門
        string compareCode_DEPT(string[] ListTEST_Send)
        {
            string strDept = "";
            for (int i = 0; i < ListTEST_Send.Length - 1; i++)
            {
                for (int j = 0; j < listDEPT.Items.Count; j++)
                {
                    CheckBox getcki = (CheckBox)listDEPT.Items.ElementAt(j);

                    if (getcki.Tag.ToString().Trim().Equals(ListTEST_Send[i]))
                    {
                        getcki.IsChecked = true;
                        strDept += getcki.Content.ToString().Trim() + ";";
                    }
                }
            }
            return strDept;
        }

        //比對代碼檔_製作目的名稱
        string Check_OBJ_NAME(string strID)
        {
            string strReturn = "";

            for (int i = 0; i < m_CodeDataObj.Count; i++)
            {
                if (m_CodeDataObj[i].ID.ToString().Trim().Equals(strID))
                {
                    strReturn = m_CodeDataObj[i].NAME.Trim();
                    break;
                }
            }

            return strReturn;
        }

        //比對代碼檔_部門名稱
        string Check_DEPT_NAME_List(string[] ListSend)
        {
            string strReturn = "";

            for (int i = 0; i < ListSend.Length - 1; i++)
            {
                for (int j = 0; j < m_CodeDataDept.Count; j++)
                {
                    if (m_CodeDataDept[j].ID.ToString().Trim().Equals(ListSend[i]))
                    {
                        strReturn = strReturn + m_CodeDataDept[j].NAME.Trim() + ";";
                        break;
                    }
                }
            }

            return strReturn;
        }

        //是否為產生子集
        private void compareProgD(string strProgD)
        {
            if (strProgD == "N")
                cbPROGD.SelectedIndex = 0;
            else if (strProgD == "Y")
                cbPROGD.SelectedIndex = 1;
        }

        //比對代碼檔_製作單位代碼
        void compareCode_FSPRDCENID(string strID)
        {
            for (int i = 0; i < cbPRDCENID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPRDCENID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbPRDCENID.SelectedIndex = i;
                    tbxPRDCEN.Text = getcbi.Content.ToString().Trim();
                    tbxPRDCEN.Tag = getcbi.Tag.ToString().Trim();
                    break;
                }
            }
        }

        #endregion

        #region ComboBox載入代碼檔

        //實作-下載外購類別細項代碼檔
        void client_fnGetTBPROG_M_PROGBUYD_CODECompleted(object sender, fnGetTBPROG_M_PROGBUYD_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    this.cbBUYD_ID.Items.Clear();
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        ComboBoxItem cbiPROGBUYD = new ComboBoxItem();
                        cbiPROGBUYD.Content = CodeData.NAME;
                        cbiPROGBUYD.Tag = CodeData.ID;
                        cbiPROGBUYD.DataContext = CodeData.IDD;
                        this.cbBUYD_ID.Items.Add(cbiPROGBUYD);   //先都產生在隱藏起來的細項代碼combobox裡                   
                        //m_CodeDataBuyD.Add(CodeData);
                    }
                }
            }
            clientPRO.QueryTBPROGOBJ_DEPT_LISTAsync();  //查詢製作目的與前後會部門對照檔列表 
        }

        //實作-下載所有的代碼檔
        void client_fnGetTBPROG_M_CODECompleted(object sender, fnGetTBPROG_M_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    this.cbOBJ_ID.Items.Clear();
                    this.cbPRD_DEPT_ID.Items.Clear();
                    this.cbPAY_DEPT_ID.Items.Clear();
                    this.cbPROGGRADEID.Items.Clear();
                    this.cbSRCID.Items.Clear();
                    this.cbTYPE.Items.Clear();
                    this.cbAUD_ID.Items.Clear();
                    this.cbSHOW_TYPE.Items.Clear();
                    this.cbLANG_ID_MAIN.Items.Clear();
                    this.cbLANG_ID_SUB.Items.Clear();
                    this.cbBUY_ID.Items.Clear();
                    this.cbPRDCENID.Items.Clear();      //製作單位
                    tbxPRDCEN.Text = "";
                    tbxPRDCEN.Tag = "";
                    this.listCHANNEL.Items.Clear();
                    this.listDEPT.Items.Clear();
                    m_CodeDataObj.Clear();
                    m_CodeDataDept.Clear();
                    this.listPROGSPEC.Items.Clear();
                    this.cbNATION_ID.Items.Clear(); //來源國家

                    //付款部門代碼，空白要自己帶入
                    ComboBoxItem cbiPAYNull = new ComboBoxItem();
                    cbiPAYNull.Content = " ";
                    cbiPAYNull.Tag = "";
                    this.cbPAY_DEPT_ID.Items.Add(cbiPAYNull);

                    //外購類別代碼，空白要自己帶入
                    ComboBoxItem cbiBUY_IDNull = new ComboBoxItem();
                    cbiBUY_IDNull.Content = " ";
                    cbiBUY_IDNull.Tag = "";
                    this.cbBUY_ID.Items.Add(cbiBUY_IDNull);

                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            case "TBZPROGOBJ":      //製作目的代碼                               
                                ComboBoxItem cbiPROGOBJ = new ComboBoxItem();
                                cbiPROGOBJ.Content = CodeData.NAME;
                                cbiPROGOBJ.Tag = CodeData.ID;
                                this.cbOBJ_ID.Items.Add(cbiPROGOBJ);    //每個使用者皆可以成案，不依據製作目的綁權限

                                //判斷前後會部門
                                if (CodeData.NAME.Trim() != "")
                                    m_CodeDataObj.Add(CodeData);

                                break;

                            case "TBZDEPT":         //製作部門別代碼，因為部門檔有些是必填，有些不是必填，因此這裡代表必填要把空白濾掉  
                                if (CodeData.NAME.Trim() != "")
                                {
                                    ComboBoxItem cbiPRD = new ComboBoxItem();
                                    cbiPRD.Content = CodeData.NAME;
                                    cbiPRD.Tag = CodeData.ID;
                                    this.cbPRD_DEPT_ID.Items.Add(cbiPRD);

                                    if (CodeData.NAME.Trim() != "")
                                        m_CodeDataDept.Add(CodeData);
                                }

                                //付款部門別代碼                                
                                ComboBoxItem cbiPAY = new ComboBoxItem();
                                cbiPAY.Content = CodeData.NAME;
                                cbiPAY.Tag = CodeData.ID;
                                this.cbPAY_DEPT_ID.Items.Add(cbiPAY);

                                //前後會部門別代碼
                                if (CodeData.NAME.Trim() != "")
                                {
                                    CheckBox cbiBEF = new CheckBox();
                                    cbiBEF.Content = CodeData.NAME;
                                    cbiBEF.Tag = CodeData.ID;
                                    this.listDEPT.Items.Add(cbiBEF);
                                }

                                break;

                            case "TBZPROGGRADE":      //節目分級代碼                                
                                ComboBoxItem cbiPROGGRADE = new ComboBoxItem();
                                cbiPROGGRADE.Content = CodeData.NAME;
                                cbiPROGGRADE.Tag = CodeData.ID;
                                this.cbPROGGRADEID.Items.Add(cbiPROGGRADE);
                                break;

                            case "TBZPROGSRC":      //節目來源代碼                                
                                ComboBoxItem cbiSRC = new ComboBoxItem();
                                cbiSRC.Content = CodeData.NAME;
                                cbiSRC.Tag = CodeData.ID;
                                this.cbSRCID.Items.Add(cbiSRC);
                                break;

                            case "TBZPROGATTR":      //內容屬性代碼                                
                                ComboBoxItem cbiTYPE = new ComboBoxItem();
                                cbiTYPE.Content = CodeData.NAME;
                                cbiTYPE.Tag = CodeData.ID;
                                this.cbTYPE.Items.Add(cbiTYPE);
                                break;

                            case "TBZPROGAUD":      //目標觀眾代碼                                
                                ComboBoxItem cbiAUD = new ComboBoxItem();
                                cbiAUD.Content = CodeData.NAME;
                                cbiAUD.Tag = CodeData.ID;
                                this.cbAUD_ID.Items.Add(cbiAUD);
                                break;

                            case "TBZPROGTYPE":      //表現方式代碼                                
                                ComboBoxItem cbiSHOW_TYPE = new ComboBoxItem();
                                cbiSHOW_TYPE.Content = CodeData.NAME;
                                cbiSHOW_TYPE.Tag = CodeData.ID;
                                this.cbSHOW_TYPE.Items.Add(cbiSHOW_TYPE);
                                break;

                            case "TBZPROGLANG":      //語言代碼                                
                                ComboBoxItem cbiLANG_ID_MAIN = new ComboBoxItem();
                                cbiLANG_ID_MAIN.Content = CodeData.NAME;
                                cbiLANG_ID_MAIN.Tag = CodeData.ID;
                                this.cbLANG_ID_MAIN.Items.Add(cbiLANG_ID_MAIN);  //主聲道

                                ComboBoxItem cbiLANG_ID_SUB = new ComboBoxItem();
                                cbiLANG_ID_SUB.Content = CodeData.NAME;
                                cbiLANG_ID_SUB.Tag = CodeData.ID;
                                this.cbLANG_ID_SUB.Items.Add(cbiLANG_ID_SUB);  //副聲道
                                break;

                            case "TBZPROGBUY":       //外購類別代碼                                
                                ComboBoxItem cbiBUY_ID = new ComboBoxItem();
                                cbiBUY_ID.Content = CodeData.NAME;
                                cbiBUY_ID.Tag = CodeData.ID;
                                this.cbBUY_ID.Items.Add(cbiBUY_ID);
                                break;

                            case "TBZCHANNEL_PRO":       //頻道別代碼                                
                                CheckBox cbCHANNEL = new CheckBox();
                                cbCHANNEL.Content = CodeData.NAME;
                                cbCHANNEL.Tag = CodeData.ID;
                                listCHANNEL.Items.Add(cbCHANNEL);
                                break;

                            case "TBZPRDCENTER":      //製作單位代碼
                                if (CodeData.ID.Trim() != "00000")  //舊資料的代碼要略過
                                {
                                    ComboBoxItem cbiPRDCENTER = new ComboBoxItem();
                                    cbiPRDCENTER.Content = CodeData.NAME;
                                    cbiPRDCENTER.Tag = CodeData.ID;
                                    this.cbPRDCENID.Items.Add(cbiPRDCENTER);
                                }
                                break;

                            case "TBZPROGSPEC":       //節目規格代碼
                                CheckBox cbPROGSPEC = new CheckBox();
                                cbPROGSPEC.Content = CodeData.NAME;
                                cbPROGSPEC.Tag = CodeData.ID;
                                cbPROGSPEC.Checked += new RoutedEventHandler(cbPROGSPEC_Checked);
                                listPROGSPEC.Items.Add(cbPROGSPEC);
                                break;

                            case "TBZPROGNATION":       //來源國家    2012/11/22 kyle                            
                                ComboBoxItem cbNATION = new ComboBoxItem();
                                cbNATION.Content = CodeData.NAME;
                                cbNATION.Tag = CodeData.ID;
                                cbNATION_ID.Items.Add(cbNATION);
                                break;

                            default:
                                break;
                        }
                    }
                }
            }

            client.fnGetTBPROG_M_PROGBUYD_CODEAsync();  //下載外購類別細項代碼檔
        }

        //判斷節目規格點選超過兩個以上，要提醒及自動帶出分集備註
        void cbPROGSPEC_Checked(object sender, RoutedEventArgs e)
        {
            int intCount = 0;
            string strReturn = "";

            for (int i = 0; i < listPROGSPEC.Items.Count; i++)
            {
                CheckBox getckb = (CheckBox)listPROGSPEC.Items.ElementAt(i);
                if (getckb.IsChecked == true)
                {
                    strReturn += getckb.Content.ToString().Trim() + ":~ ";
                    intCount++;
                }
            }

            if (intCount > 1)
            {
                MessageBox.Show("請記得填寫分集備註");
                tbxENOTE.Text = strReturn;
                tbxENOTE.Focus();
            }
        }

        //選擇外購類別，要判斷顯示哪些外購類別細項
        private void cbBUY_ID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.cbBUYD_IDS.Items.Clear();     //使用前先清空
            string strBUYID = ""; //外購類別大項
            ComboBox cb = sender as ComboBox;

            if (cb.SelectedIndex == -1)
                return;

            ComboBoxItem cbm = (ComboBoxItem)cb.SelectedItem;
            strBUYID = cbm.Tag.ToString();

            for (int i = 0; i < cbBUYD_ID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbBUYD_ID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strBUYID))
                {
                    ComboBoxItem cbiPROGBUYDS = new ComboBoxItem();
                    cbiPROGBUYDS.Content = getcbi.Content;
                    cbiPROGBUYDS.Tag = getcbi.DataContext;
                    this.cbBUYD_IDS.Items.Add(cbiPROGBUYDS);
                }
            }
        }

        #endregion

        #region 檢查畫面資料及搬值

        private Class_PROPOSAL FormToClass_Proposal()
        {
            Class_PROPOSAL obj = new Class_PROPOSAL();

            obj.FSPRO_ID = tbxPRO_ID.Text.ToString();               //成案單編號
            obj.FSPROG_ID = m_strProgID;                            //節目編號(從webservice取得)
            obj.FSPRODUCER = ((UserStruct)(acbMenID.SelectedItem)).FSUSER_ID.Trim(); //製作人ID  by Jarvis20130429
            if (cbPRO_TYPE.SelectedIndex != -1)                     //成案種類
                obj.FCPRO_TYPE = ((ComboBoxItem)cbPRO_TYPE.SelectedItem).Tag.ToString();

            obj.FSPROG_NAME = tbxPROG_NAME.Text.ToString().Trim(); //節目名稱
            obj.FSPLAY_NAME = tbxWEBNAME.Text.ToString().Trim();   //播出名稱 
            obj.FSNAME_FOR = tbxPGMENAME.Text.ToString().Trim();   //外文名稱
            obj.FSCONTENT = tbxCONTENT.Text.ToString().Trim();     //節目內容簡述
            obj.FNLENGTH = Convert.ToInt32(tbxLENGTH.Text.ToString().Trim());              //每集時長
            obj.FNBEG_EPISODE = Convert.ToInt16(tbxBEG_EPISODE.Text.ToString().Trim());    //集次(起)
            obj.FNEND_EPISODE = Convert.ToInt16(tbxEND_EPISODE.Text.ToString().Trim());    //集次(迄)
            obj.FSCHANNEL = Check_ChannelList();                   //預訂播出頻道
            if (cbDELETE.IsChecked == false)                          //到期日期後是否刪除
                obj.FSEXPIRE_DATE_ACTION = "N";
            else
                obj.FSEXPIRE_DATE_ACTION = "Y";


            if (dpEXPIRE.Text == "")
                obj.FDEXPIRE_DATE = Convert.ToDateTime("1900/1/1");
            else
                obj.FDEXPIRE_DATE = Convert.ToDateTime(dpEXPIRE.Text).AddHours(23).AddMinutes(59);  //到期日期

            obj.FSPROGNATIONID = ((ComboBoxItem)cbNATION_ID.SelectedItem).Tag.ToString();       //國家代碼
            //if (dpEXPIRE.Text=="")
            //    obj.FDEXPIRE_DATE = Convert.ToDateTime("1900/1/1");
            //else
            //    obj.FDEXPIRE_DATE = Convert.ToDateTime(dpEXPIRE.Text).AddHours(23).AddMinutes(59);  //到期日期



            if (dpSHOW_DATE.Text == "")
                obj.FDSHOW_DATE = Convert.ToDateTime("1900/1/1");
            else
                obj.FDSHOW_DATE = Convert.ToDateTime(dpSHOW_DATE.Text);//預訂上檔日期

            //if (cbPRDCENID.SelectedIndex != -1)                    //製作單位
            //    obj.FSPRDCENID = ((ComboBoxItem)cbPRDCENID.SelectedItem).Tag.ToString();
            //else
            //    obj.FSPRDCENID = "";

            if (tbxPRDCEN.Text.Trim() != "")                       //製作單位
                obj.FSPRDCENID = tbxPRDCEN.Tag.ToString();
            else
                obj.FSPRDCENID = "";

            if (cbPRD_DEPT_ID.SelectedIndex != -1)                 //製作部門
                obj.FSPRD_DEPT_ID = ((ComboBoxItem)cbPRD_DEPT_ID.SelectedItem).Tag.ToString();
            else
                obj.FSPRD_DEPT_ID = "";

            if (cbPAY_DEPT_ID.SelectedIndex != -1)                //付款部門
                obj.FSPAY_DEPT_ID = ((ComboBoxItem)cbPAY_DEPT_ID.SelectedItem).Tag.ToString();
            else
                obj.FSPAY_DEPT_ID = "";

            obj.FSBEF_DEPT_ID = m_strBEF_DEPT;                    //前會部門
            obj.FSAFT_DEPT_ID = m_strAFT_DEPT;                    //後會部門

            if (cbOBJ_ID.SelectedIndex != -1)                     //製作目的
                obj.FSOBJ_ID = ((ComboBoxItem)cbOBJ_ID.SelectedItem).Tag.ToString();
            else
                obj.FSOBJ_ID = "";

            if (cbPROGGRADEID.SelectedIndex != -1)                //節目分級
                obj.FSGRADE_ID = ((ComboBoxItem)cbPROGGRADEID.SelectedItem).Tag.ToString();
            else
                obj.FSGRADE_ID = "";

            if (cbSRCID.SelectedIndex != -1)                     //節目來源
                obj.FSSRC_ID = ((ComboBoxItem)cbSRCID.SelectedItem).Tag.ToString();
            else
                obj.FSSRC_ID = "";

            if (cbTYPE.SelectedIndex != -1)                     //節目型態
                obj.FSTYPE = ((ComboBoxItem)cbTYPE.SelectedItem).Tag.ToString();
            else
                obj.FSTYPE = "";

            if (cbAUD_ID.SelectedIndex != -1)                   //目標觀眾
                obj.FSAUD_ID = ((ComboBoxItem)cbAUD_ID.SelectedItem).Tag.ToString();
            else
                obj.FSAUD_ID = "";

            if (cbSHOW_TYPE.SelectedIndex != -1)                //表現方式
                obj.FSSHOW_TYPE = ((ComboBoxItem)cbSHOW_TYPE.SelectedItem).Tag.ToString();
            else
                obj.FSSHOW_TYPE = "";

            if (cbLANG_ID_MAIN.SelectedIndex != -1)             //主聲道
                obj.FSLANG_ID_MAIN = ((ComboBoxItem)cbLANG_ID_MAIN.SelectedItem).Tag.ToString();
            else
                obj.FSLANG_ID_MAIN = "";

            if (cbLANG_ID_SUB.SelectedIndex != -1)              //副聲道
                obj.FSLANG_ID_SUB = ((ComboBoxItem)cbLANG_ID_SUB.SelectedItem).Tag.ToString();
            else
                obj.FSLANG_ID_SUB = "";

            if (cbBUY_ID.SelectedIndex != -1)                   //外購類別大類
                obj.FSBUY_ID = ((ComboBoxItem)cbBUY_ID.SelectedItem).Tag.ToString();
            else
                obj.FSBUY_ID = "";

            if (cbBUYD_IDS.SelectedIndex != -1 && ((ComboBoxItem)(cbBUY_ID.SelectedItem)).Content.ToString().Trim() != "")                 //外購類別細類
                obj.FSBUYD_ID = ((ComboBoxItem)cbBUYD_IDS.SelectedItem).Tag.ToString();
            else
                obj.FSBUYD_ID = "";

            obj.FSSIGN_DEPT_ID = "";      //簽核單位，先放空
            obj.FCCHECK_STATUS = "N";     //狀態

            //宏觀是非常特別的部門，有些成案是國際部買來的=>要歸類在宏觀
            //有些成案是送到企劃部審核=>要歸類在公視
            if (UserClass.userData.FSCHANNEL_ID == "08" && ((ComboBoxItem)cbOBJ_ID.SelectedItem).Content.ToString().Trim() == "公視製播")
                obj.FSCHANNEL_ID = "01";
            else
                obj.FSCHANNEL_ID = UserClass.userData.FSCHANNEL_ID.ToString();    //頻道別

            if (cbEmergency.IsChecked == true)  //緊急成案
                obj.FCEMERGENCY = "Y";
            else
                obj.FCEMERGENCY = "N";

            obj.FSPROGSPEC = Check_PROGSPECList();                                //節目規格   

            if (cbPROGD.SelectedItem == null)
                obj.FCPROGD = "N";
            else
                obj.FCPROGD = ((ComboBoxItem)cbPROGD.SelectedItem).Tag.ToString(); //產生子集

            obj.FSENOTE = tbxENOTE.Text.ToString().Trim();                        //分集備註

            obj.FSCREATED_BY = UserClass.userData.FSUSER_ID.ToString();           //建檔者
            obj.FSUPDATED_BY = UserClass.userData.FSUSER_ID.ToString();           //修改者

            m_strProgName = obj.FSPROG_NAME;  //暫存本次新增的節目名稱，為了秀給使用者看結果用

            m_Queryobj = obj;
            return obj;
        }

        private Class_PROGPRODUCER FormToClass_PROGPRODUCER()
        {
            Class_PROGPRODUCER obj = new Class_PROGPRODUCER();

            obj.FSPRO_TYPE = "G";//節目類型 因為是成案中新增所以統一為G
            obj.FSID = m_strProgID;     //查詢檔案編號                                      //節目編號
            //m_IDName = tbxPROG_NAME.Text.ToString().Trim();                     //暫存本次新增的名稱，為了秀給使用者看結果用

            //obj.FSPRODUCER = ((MyUser)cbxUser.SelectedItem).id.ToString();        //將下拉式選單改成AutoComplete作法
            obj.FSPRODUCER = ((UserStruct)(acbMenID.SelectedItem)).FSUSER_ID.Trim();

            obj.FSCREATED_BY = UserClass.userData.FSUSER_ID.ToString();             //建檔者
            obj.FSUPDATED_BY = UserClass.userData.FSUSER_ID.ToString();             //修改者

            // m_Queryobj = obj;
            return obj;
        }

        private string Check_ChannelList()    //檢查可播映頻道有哪些
        {
            string strReturn = "";
            List<string> ListCHANNEL = new List<string>();  //TEST
            for (int i = 0; i < listCHANNEL.Items.Count; i++)
            {
                CheckBox getckb = (CheckBox)listCHANNEL.Items.ElementAt(i);
                if (getckb.IsChecked == true)
                {
                    strReturn += getckb.Tag.ToString().Trim() + ";";
                }
            }
            return strReturn;
        }

        private string Check_PROGSPECList()    //檢查節目規格有哪些
        {
            string strReturn = "";
            for (int i = 0; i < listPROGSPEC.Items.Count; i++)
            {
                CheckBox getckb = (CheckBox)listPROGSPEC.Items.ElementAt(i);
                if (getckb.IsChecked == true)
                {
                    strReturn += getckb.Tag.ToString().Trim() + ";";
                }
            }
            return strReturn;
        }

        private Boolean Check_ProposalData()    //檢查畫面上的欄位是否都填妥
        {
            StringBuilder strMessage = new StringBuilder();
            int intCheck;               //純粹為了判斷是否為數字型態之用(integer)
            short shortCheck;           //純粹為了判斷是否為數字型態之用(smallint)

            short intBegEpisode = 0; //集數(起)
            short intEndEpisode = 0; //集數(迄)

            // by Jarvis20130501
            if (acbMenID.SelectedItem == null)
            {
                strMessage.AppendLine("請輸入「製作人」欄位");

            }


            if (cbPRO_TYPE.SelectedIndex == 1)  //續製節目要判斷有沒有選節目，反之要判斷有沒有輸入節目名稱
            {
                if (m_strProgID == "")
                    strMessage.AppendLine("續製節目，請選擇「節目名稱」欄位");
            }
            else
            {
                if (tbxPROG_NAME.Text.Trim() == "")
                    strMessage.AppendLine("請輸入「節目名稱」欄位");
            }

            if (tbxWEBNAME.Text.Trim() == "")
                strMessage.AppendLine("請輸入「播出名稱」欄位");

            if (tbxLENGTH.Text.Trim() == "")
                strMessage.AppendLine("請輸入「節目長度」欄位");
            else
            {
                if (int.TryParse(tbxLENGTH.Text.Trim(), out intCheck) == false)
                    strMessage.AppendLine("請檢查「節目長度」欄位必須為數值型態");
            }

            if (tbxBEG_EPISODE.Text.Trim() == "")
                strMessage.AppendLine("請輸入「集數(起)」欄位");
            else
            {
                if (short.TryParse(tbxBEG_EPISODE.Text.Trim(), out shortCheck) == false)
                    strMessage.AppendLine("請檢查「集數(起)」欄位必須為數值型態");
                else
                    intBegEpisode = Convert.ToInt16(tbxBEG_EPISODE.Text.Trim());
            }

            if (tbxEND_EPISODE.Text.Trim() == "")
                strMessage.AppendLine("請輸入「集數(迄)」欄位");
            else
            {
                if (short.TryParse(tbxEND_EPISODE.Text.Trim(), out shortCheck) == false)
                    strMessage.AppendLine("請檢查「集數(迄)」欄位必須為數值型態");
                else
                    intEndEpisode = Convert.ToInt16(tbxEND_EPISODE.Text.Trim());
            }

            if (intBegEpisode != 0 & intEndEpisode != 0 & intBegEpisode > intEndEpisode)   //檢查起不能大於迄
                strMessage.AppendLine("請檢查「集數(迄)」欄位必須大於或等於「集數(起)」");

            if (cbPRO_TYPE.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「種類」欄位");

            if (cbOBJ_ID.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「製作目的」欄位");

            //if (dpSHOW_DATE.Text.Trim() == "")    //改成可不填
            //    strMessage.AppendLine("請輸入「預訂上檔日期」欄位");

            if (cbPRD_DEPT_ID.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「製作部門」欄位");

            if (cbPROGGRADEID.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「節目分級」欄位");

            if (cbSRCID.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「節目來源」欄位");

            if (cbTYPE.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「節目型態」欄位");

            if (cbAUD_ID.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「目標觀眾」欄位");

            if (cbSHOW_TYPE.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「表現方式」欄位");

            if (cbLANG_ID_MAIN.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「主聲道」欄位");

            if (cbLANG_ID_SUB.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「副聲道」欄位");

            if (cbBUY_ID.SelectedIndex != -1 && ((ComboBoxItem)(cbBUY_ID.SelectedItem)).Content.ToString().Trim() != "" && cbBUYD_IDS.SelectedIndex == -1) //因為設定外鍵，因此選擇了大項就要設細項
                strMessage.AppendLine("請輸入「外購類別細項」欄位");

            if (cbNATION_ID.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「來源國家」欄位");
            //if (cbPRDCENID.SelectedIndex == -1)
            //    strMessage.AppendLine("請輸入「製作單位」欄位");

            if (tbxPRDCEN.Text.Trim() == "")
                strMessage.AppendLine("請輸入「製作單位」欄位");

            if (Check_PROGSPECList().Trim() == "")
                strMessage.AppendLine("請勾選「節目規格」欄位");

            try
            {
                if (dpEXPIRE.Text == "")
                {
                }
                else
                {
                    DateTime testField = Convert.ToDateTime(dpEXPIRE.Text).AddHours(23).AddMinutes(59);  //到期日期
                }
            }
            catch (Exception)
            {
                strMessage.AppendLine("請注意到期時間格式");
            }

            //若是勾選節目類型為新製或衍生，並且開始集數填寫的不是第0集，就要彈出訊息通知
            if (cbPRO_TYPE.SelectedIndex == 0 || cbPRO_TYPE.SelectedIndex == 2)
            {
                if (intBegEpisode != 1)
                    strMessage.AppendLine("節目種類為新製或衍生時，請檢查「集數(起)」欄位必須從第1集開始");
            }

            if (strMessage.ToString().Trim() == "")
                return true;
            else
            {
                MessageBoxResult result = MessageBox.Show(strMessage.ToString(), "提示訊息", MessageBoxButton.OK);
                return false;
            }
        }

        //前會部門
        private void btnBEF_DEPT_ID_Click(object sender, RoutedEventArgs e)
        {
            PRO.PRO100_07 PRO100_07_frm = new PRO.PRO100_07(m_strBEF_DEPT, "BEF");
            PRO100_07_frm.Show();

            PRO100_07_frm.Closing += (s, args) =>
            {
                if (PRO100_07_frm.DialogResult == true)
                {
                    m_strBEF_DEPT = PRO100_07_frm.m_Dept;
                    tbBEF_DEPT_ID.Text = PRO100_07_frm.m_DeptName;
                }
            };
        }

        //後會部門
        private void btnAFT_DEPT_ID_Click(object sender, RoutedEventArgs e)
        {
            PRO.PRO100_07 PRO100_07_frm = new PRO.PRO100_07(m_strAFT_DEPT, "AFT");
            PRO100_07_frm.Show();

            PRO100_07_frm.Closing += (s, args) =>
            {
                if (PRO100_07_frm.DialogResult == true)
                {
                    m_strAFT_DEPT = PRO100_07_frm.m_Dept;
                    tbAFT_DEPT_ID.Text = PRO100_07_frm.m_DeptName;
                }
            };
        }

        #endregion


        //自動去比對資料庫，若有人員就顯示資料 by Jarvis20130429
        private void acbMenID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (acbMenID.SelectedItem == null)
                tbxNote.Text = "";
            else
                tbxNote.Text = ((UserStruct)(acbMenID.SelectedItem)).FSTITLE_NAME.Trim();
        }

        //確定
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (Check_ProposalData() == false)      //檢查畫面上的欄位是否都填妥
                return;
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
            if (cbPRO_TYPE.SelectedIndex == 1)  //續製節目不用去比對是否有相同節目名稱的資料
                clientPRO.fnINSERTTBPROPOSALAsync(FormToClass_Proposal());
            else
                clientPRO.QUERY_TBPROGNAME_CHECKAsync(tbxPROG_NAME.Text.ToString().Trim());   //查詢是否有相同節目名稱的資料 
        }

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //節目種類(若是續製節目則要挑選節目)
        private void cbPRO_TYPE_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbPRO_TYPE.SelectedIndex == 1)
            {
                tbxPROG_NAME.IsEnabled = false;
                EnabledControl();
            }
            else
            {
                tbxPROG_NAME.IsEnabled = true;
                DisEnabledControl();
            }
        }

        //若是為續製，要鎖住不給修改
        private void EnabledControl()
        {
            //續製的話，企劃部決議只留製作目的及開始集數要鎖住
            cbOBJ_ID.IsEnabled = false;
            tbxBEG_EPISODE.IsEnabled = false;

            iniForm();   //清空畫面上資料
        }

        //若是不為續製，要開放修改
        private void DisEnabledControl()
        {
            //續製的話，企劃部決議只留製作目的及開始集數要鎖住
            cbOBJ_ID.IsEnabled = true;
            tbxBEG_EPISODE.IsEnabled = true;

            if (m_bolPRO_TYPE == true)
            {
                if (tbxPROG_NAME.Text.Trim() != "")
                {
                    MessageBoxResult resultMsg = MessageBox.Show("確定要保留畫面上資料?", "提示訊息", MessageBoxButton.OKCancel);
                    if (resultMsg == MessageBoxResult.Cancel)
                    {
                        iniForm();   //清空畫面上資料                        
                    }
                }
            }
            m_bolPRO_TYPE = true;
            tbxBEG_EPISODE.Text = "1"; //因為新製或衍生都從第一集開始，所以就先預帶 1
        }

        private void iniForm()  //清空畫面上資料
        {
            dpSHOW_DATE.Text = "";
            tbxLENGTH.Text = "";
            tbxBEG_EPISODE.Text = "";
            tbxEND_EPISODE.Text = "";
            tbxCONTENT.Text = "";
            tbxPGMENAME.Text = "";
            tbxWEBNAME.Text = "";
            tbBEF_DEPT_ID.Text = "";
            tbAFT_DEPT_ID.Text = "";
            m_strBEF_DEPT = "";
            m_strAFT_DEPT = "";
            tbxPROG_NAME.Text = "";
            tbxPROG_NAME.Tag = "";
            tbxENOTE.Text = "";
            cbPROGD.SelectedIndex = 1;

            client.fnGetTBPROG_M_CODEAsync();   //下載代碼檔
            BusyMsg.IsBusy = true;
        }

        //開啟節目資料查詢畫面
        private void btnPROG_Click(object sender, RoutedEventArgs e)
        {
            if (cbPRO_TYPE.SelectedIndex == -1)   //要預帶節目資料時要先選擇種類，因為要鎖住開始結束集別
            {
                MessageBox.Show("請先選擇「種類」", "提示訊息", MessageBoxButton.OK);
                return;
            }

            ProgData.PROGDATA_VIEW PROGDATA_VIEW_frm = new ProgData.PROGDATA_VIEW();
            PROGDATA_VIEW_frm.Show();

            PROGDATA_VIEW_frm.Closed += (s, args) =>
            {
                if (PROGDATA_VIEW_frm.DialogResult == true)
                {
                    tbxPROG_NAME.Text = PROGDATA_VIEW_frm.strProgName_View;
                    tbxPROG_NAME.Tag = PROGDATA_VIEW_frm.strProgID_View;
                    m_strProgID = PROGDATA_VIEW_frm.strProgID_View;
                    //clientPRO.QUERYTBPROPOSAL_BYPROGIDAsync(m_strProgID);//原本查詢是查成案單檔，現在改成查節目主檔，資料才會是目前資料庫的資料
                    client.fnGetProg_MAsync(m_strProgID); //預帶查詢節目主檔                   
                    BusyMsg.IsBusy = true;
                }
            };
        }

        //若是沒有輸入播出名稱，點選時預帶節目名稱
        private void tbxWEBNAME_GotFocus(object sender, RoutedEventArgs e)
        {
            if (tbxWEBNAME.Text.ToString().Trim() == "" && tbxPROG_NAME.Text.ToString().Trim() != "")
                tbxWEBNAME.Text = tbxPROG_NAME.Text.ToString().Trim();
        }

        //選取製作單位
        private void btnPRDCEN_Click(object sender, RoutedEventArgs e)
        {
            PRG100_11 PRG100_11_frm = new PRG100_11();
            PRG100_11_frm.Show();

            PRG100_11_frm.Closed += (s, args) =>
            {
                if (PRG100_11_frm.DialogResult == true)
                {
                    tbxPRDCEN.Text = PRG100_11_frm.strPRDCENName_View;
                    tbxPRDCEN.Tag = PRG100_11_frm.strPRDCENID_View;
                }
            };
        }

        //挑選製作目的時要自動帶前會部門及後會部門
        private void cbOBJ_ID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbOBJ_ID.SelectedItem == null)
                return;

            for (int i = 0; i < PROGOBJ_DEPT_obj.Count; i++)
            {
                if (((ComboBoxItem)cbOBJ_ID.SelectedItem).Tag.ToString().Trim().Equals(PROGOBJ_DEPT_obj[i].FSOBJ_ID.Trim()))
                {
                    m_strBEF_DEPT = PROGOBJ_DEPT_obj[i].FSBEF_DEPT_ID.Trim();
                    m_strAFT_DEPT = PROGOBJ_DEPT_obj[i].FSAFT_DEPT_ID.Trim();
                    tbBEF_DEPT_ID.Text = PROGOBJ_DEPT_obj[i].SHOW_FSBEF_DEPT_ID.Trim();
                    tbAFT_DEPT_ID.Text = PROGOBJ_DEPT_obj[i].SHOW_FSAFT_DEPT_ID.Trim();
                    break;
                }
            }
        }

        #region 流程引擎

        //以下程式將Flow放到Flow引擎中
        private void newAFlow()
        {
            StringBuilder sb = new StringBuilder();
            string strParameter02 = "01";               //頻道別
            string strParameter03 = "N";                //緊急成案

            //宏觀是非常特別的部門，有些成案會國際部自己送出就結束，有些成案要送到企劃部審核
            //若是宏觀提出，並且製作目的是「公視製播」
            if (UserClass.userData.FSCHANNEL_ID.Trim() == "08" && ((ComboBoxItem)cbOBJ_ID.SelectedItem).Content.ToString().Trim() == "公視製播")
                strParameter02 = "01";
            else
                strParameter02 = UserClass.userData.FSCHANNEL_ID.Trim();

            if (cbEmergency.IsChecked == true)  //緊急成案
                strParameter03 = "Y";

            sb.Append(@"<VariableCollection>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>FormId</name>");
            sb.Append(@"    <value>" + tbxPRO_ID.Text + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>NextXamlName</name>");
            sb.Append(@"    <value>/Proposal/PROPOSAL_Approve.xml</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>hid_SendTo</name>");      //預設是給1，不用審核的單子最後顯示才會是通過
            sb.Append(@"    <value>1</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>Function01</name>");
            sb.Append(@"    <value>0</value>");             //預設是給0，表單不修改，改成 1 此表單就可以修改
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>parameter02</name>");
            sb.Append(@"    <value>" + strParameter02 + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>parameter03</name>");
            sb.Append(@"    <value>" + strParameter03 + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>USERID</name>");
            sb.Append(@"    <value>" + UserClass.userData.FSUSER_ID.ToString().Trim() + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>FormInfo</name>");
            sb.Append(@"    <value><![CDATA[" + "節目名稱-" + m_strProgName.ToString() + "]]></value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>CancelForm</name>");
            sb.Append(@"    <value>CanCancel</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"</VariableCollection>");

            //後端呼叫流程引擎
            clientFlow.CallFlow_NewFlowWithFieldAsync(3, UserClass.userData.FSUSER_ID.ToString().Trim(), sb.ToString());
            //FlowClinet.NewFlowWithFieldAsync(3, UserClass.userData.FSUSER_ID.ToString().Trim(), sb.ToString());
        }

        //原本的前端呼叫流程引擎
        void FlowClinet_NewFlowWithFieldCompleted(object sender, flowWebService.NewFlowWithFieldCompletedEventArgs e)
        {
            if (e.Error == null && e.Result != "")
            {
                MessageBox.Show("新增成案資料「" + m_strProgName.ToString() + "」成功！", "提示訊息", MessageBoxButton.OK);
                this.DialogResult = true;
            }
            else
            {
                if (intCountFlow >= 3)  //不成功就呼叫流程三次，三次都不成功就顯示失敗
                {
                    MessageBox.Show("呼叫流程引擎異常，新增成案資料「" + m_strProgName.ToString() + "」失敗！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
                else
                {
                    intCountFlow++;
                    newAFlow();
                }
            }
        }

        #endregion



    }

}