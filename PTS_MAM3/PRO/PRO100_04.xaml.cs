﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using System.Xml.Linq;
using System.IO;
using PTS_MAM3.WSPROG_M;
using PTS_MAM3.WSPROPOSAL;
using PTS_MAM3.PROG_M;
using PTS_MAM3.MAMFunctions;
using PTS_MAM3.PRG;
using PTS_MAM3.WSPROG_PRODUCER;

namespace PTS_MAM3.Proposal
{
    public partial class PROPOSAL_Query : ChildWindow
    {
        WSPROG_MSoapClient client = new WSPROG_MSoapClient();     //產生新的代理類別
        WSPROPOSALSoapClient clientPRO = new WSPROPOSALSoapClient();
        
        WSPROG_PRODUCERSoapClient clientPRODUCER = new WSPROG_PRODUCERSoapClient();
        List<UserStruct> ListUserAll = new List<UserStruct>();
        //Class_PROGPRODUCER ProducerObj = null;

        public List<Class_PROPOSAL> m_ListFormData = new List<Class_PROPOSAL>();  //記錄查詢到的成案資料
        string m_strBEF_DEPT = "";
        string m_strAFT_DEPT = "";
        string m_strStatus = "";     //狀態

        public PROPOSAL_Query()
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面
        }

        void InitializeForm()       //初始化本頁面
        {
            //下載代碼檔
            client.fnGetTBPROG_M_CODECompleted += new EventHandler<fnGetTBPROG_M_CODECompletedEventArgs>(client_fnGetTBPROG_M_CODECompleted);
            client.fnGetTBPROG_M_CODEAsync();

            //下載外購類別細項代碼檔
            client.fnGetTBPROG_M_PROGBUYD_CODECompleted += new EventHandler<fnGetTBPROG_M_PROGBUYD_CODECompletedEventArgs>(client_fnGetTBPROG_M_PROGBUYD_CODECompleted);
            client.fnGetTBPROG_M_PROGBUYD_CODEAsync();

            //查詢成案資料
            clientPRO.fnQUERYTBPROPOSALCompleted += new EventHandler<fnQUERYTBPROPOSALCompletedEventArgs>(clientPRO_fnQUERYTBPROPOSALCompleted);

            //查詢成案資料_BY建檔者
            clientPRO.fnQUERYTBPROPOSAL_BYUSERIDCompleted += new EventHandler<fnQUERYTBPROPOSAL_BYUSERIDCompletedEventArgs>(clientPRO_fnQUERYTBPROPOSAL_BYUSERIDCompleted);

            //查詢所有使用者資料 by Jarvis20130523
            clientPRODUCER.QUERY_TBUSERS_ALLCompleted += new EventHandler<QUERY_TBUSERS_ALLCompletedEventArgs>(clientPRODUCER_QUERY_TBUSERS_ALLCompleted);
            clientPRODUCER.QUERY_TBUSERS_ALLAsync();


            //緊急成案的按鈕要卡權限
            if (ModuleClass.getModulePermissionByName("01", "緊急成案") == false)
            {
                cbEmergency.IsEnabled = false;
                cbCompleteN.IsEnabled = false;
            }

            //日期區間預帶一年
            dpStart.Text = DateTime.Now.AddYears(-1).ToShortDateString();
            dpEnd.Text = DateTime.Now.ToShortDateString();

            SetControlsTabIndex();
        }

        //實作-查詢所有使用者資料 by Jarvis20130523
        void clientPRODUCER_QUERY_TBUSERS_ALLCompleted(object sender, QUERY_TBUSERS_ALLCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                ListUserAll = e.Result;
                acbMenID.ItemsSource = ListUserAll; //繫結AutoComplete按鈕
            }
            else
                MessageBox.Show("查詢使用者資料異常！", "提示訊息", MessageBoxButton.OK);

        }

        //自動去比對資料庫，若有人員就顯示資料 by Jarvis20130523
        private void acbMenID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (acbMenID.SelectedItem == null)
                tbxNote.Text = "";
            else
                tbxNote.Text = ((UserStruct)(acbMenID.SelectedItem)).FSTITLE_NAME.Trim();
        }

        //實作-查詢成案資料
        void clientPRO_fnQUERYTBPROPOSALCompleted(object sender, fnQUERYTBPROPOSALCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)  //若是有查詢結果，則顯示，不然就請使用者重新查詢
                {
                    m_ListFormData = new List<Class_PROPOSAL>(e.Result);  //把取回來的結果，加上型別定義，才可以塞回去    
                    this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("查無成案資料，請重新輸入條件查詢！", "提示訊息", MessageBoxButton.OK);
                    return;
                }
            }
        }

        //實作查詢成案資料_BY建檔者
        void clientPRO_fnQUERYTBPROPOSAL_BYUSERIDCompleted(object sender, fnQUERYTBPROPOSAL_BYUSERIDCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)  //若是有查詢結果，則顯示，不然就請使用者重新查詢
                {
                    m_ListFormData = new List<Class_PROPOSAL>(e.Result);  //把取回來的結果，加上型別定義，才可以塞回去    
                    this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("查無成案資料，請重新輸入條件查詢！", "提示訊息", MessageBoxButton.OK);
                    return;
                }
            }            
        }

        #region ComboBox載入代碼檔

        //實作-下載外購類別細項代碼檔
        void client_fnGetTBPROG_M_PROGBUYD_CODECompleted(object sender, fnGetTBPROG_M_PROGBUYD_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        ComboBoxItem cbiPROGBUYD = new ComboBoxItem();
                        cbiPROGBUYD.Content = CodeData.NAME;
                        cbiPROGBUYD.Tag = CodeData.ID;
                        cbiPROGBUYD.DataContext = CodeData.IDD;
                        this.cbBUYD_ID.Items.Add(cbiPROGBUYD);   //先都產生在隱藏起來的細項代碼combobox裡                   
                    }
                }
            }
        }

        //實作-下載所有的代碼檔
        void client_fnGetTBPROG_M_CODECompleted(object sender, fnGetTBPROG_M_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            case "TBZPROGOBJ":      //製作目的代碼
                                ComboBoxItem cbiPROGOBJ = new ComboBoxItem();
                                cbiPROGOBJ.Content = CodeData.NAME;
                                cbiPROGOBJ.Tag = CodeData.ID;
                                this.cbOBJ_ID.Items.Add(cbiPROGOBJ);
                                break;

                            case "TBZDEPT":         //製作部門別代碼
                                if (CodeData.NAME.Trim() != "")//要濾掉空白
                                {
                                    ComboBoxItem cbiPRD = new ComboBoxItem();
                                    cbiPRD.Content = CodeData.NAME;
                                    cbiPRD.Tag = CodeData.ID;
                                    this.cbPRD_DEPT_ID.Items.Add(cbiPRD);
                                }

                                //付款部門別代碼
                                if (CodeData.NAME.Trim() != "")//要濾掉空白
                                {
                                    ComboBoxItem cbiPAY = new ComboBoxItem();
                                    cbiPAY.Content = CodeData.NAME;
                                    cbiPAY.Tag = CodeData.ID;
                                    this.cbPAY_DEPT_ID.Items.Add(cbiPAY);
                                }

                                //前後會部門別代碼
                                if (CodeData.NAME.Trim() != "")//要濾掉空白
                                {
                                    CheckBox cbiBEF = new CheckBox();
                                    cbiBEF.Content = CodeData.NAME;
                                    cbiBEF.Tag = CodeData.ID;
                                    this.listDEPT.Items.Add(cbiBEF);
                                }
                                break;

                            case "TBZPROGGRADE":      //節目分級代碼
                                ComboBoxItem cbiPROGGRADE = new ComboBoxItem();
                                cbiPROGGRADE.Content = CodeData.NAME;
                                cbiPROGGRADE.Tag = CodeData.ID;
                                this.cbPROGGRADEID.Items.Add(cbiPROGGRADE);
                                break;

                            case "TBZPROGSRC":      //節目來源代碼
                                ComboBoxItem cbiSRC = new ComboBoxItem();
                                cbiSRC.Content = CodeData.NAME;
                                cbiSRC.Tag = CodeData.ID;
                                this.cbSRCID.Items.Add(cbiSRC);
                                break;

                            case "TBZPROGATTR":      //內容屬性代碼
                                ComboBoxItem cbiTYPE = new ComboBoxItem();
                                cbiTYPE.Content = CodeData.NAME;
                                cbiTYPE.Tag = CodeData.ID;
                                this.cbTYPE.Items.Add(cbiTYPE);
                                break;

                            case "TBZPROGAUD":      //目標觀眾代碼
                                ComboBoxItem cbiAUD = new ComboBoxItem();
                                cbiAUD.Content = CodeData.NAME;
                                cbiAUD.Tag = CodeData.ID;
                                this.cbAUD_ID.Items.Add(cbiAUD);
                                break;

                            case "TBZPROGTYPE":      //表現方式代碼
                                ComboBoxItem cbiSHOW_TYPE = new ComboBoxItem();
                                cbiSHOW_TYPE.Content = CodeData.NAME;
                                cbiSHOW_TYPE.Tag = CodeData.ID;
                                this.cbSHOW_TYPE.Items.Add(cbiSHOW_TYPE);
                                break;

                            case "TBZPROGLANG":      //語言代碼
                                ComboBoxItem cbiLANG_ID_MAIN = new ComboBoxItem();
                                cbiLANG_ID_MAIN.Content = CodeData.NAME;
                                cbiLANG_ID_MAIN.Tag = CodeData.ID;
                                this.cbLANG_ID_MAIN.Items.Add(cbiLANG_ID_MAIN);  //主聲道

                                ComboBoxItem cbiLANG_ID_SUB = new ComboBoxItem();
                                cbiLANG_ID_SUB.Content = CodeData.NAME;
                                cbiLANG_ID_SUB.Tag = CodeData.ID;
                                this.cbLANG_ID_SUB.Items.Add(cbiLANG_ID_SUB);  //副聲道
                                break;

                            case "TBZPROGBUY":       //外購類別代碼
                                if (CodeData.NAME.Trim() != "")//要濾掉空白
                                {
                                    ComboBoxItem cbiBUY_ID = new ComboBoxItem();
                                    cbiBUY_ID.Content = CodeData.NAME;
                                    cbiBUY_ID.Tag = CodeData.ID;
                                    this.cbBUY_ID.Items.Add(cbiBUY_ID);
                                }
                                break;

                            case "TBZCHANNEL_PRO":       //頻道別代碼
                                CheckBox cbCHANNEL = new CheckBox();
                                cbCHANNEL.Content = CodeData.NAME;
                                cbCHANNEL.Tag = CodeData.ID;
                                listCHANNEL.Items.Add(cbCHANNEL);
                                break;

                            case "TBZPRDCENTER":      //製作單位代碼
                                if (CodeData.ID.Trim() != "00000")  //舊資料的代碼要略過
                                {
                                    ComboBoxItem cbiPRDCENTER = new ComboBoxItem();
                                    cbiPRDCENTER.Content = CodeData.NAME;
                                    cbiPRDCENTER.Tag = CodeData.ID;
                                    this.cbPRDCENID.Items.Add(cbiPRDCENTER);
                                }
                                break;

                            case "TBZPROGSPEC":       //節目規格代碼
                                CheckBox cbPROGSPEC = new CheckBox();
                                cbPROGSPEC.Content = CodeData.NAME;
                                cbPROGSPEC.Tag = CodeData.ID;
                                listPROGSPEC.Items.Add(cbPROGSPEC);
                                break;

                            default:
                                break;
                        }

                    }
                }
            }
        }

        private string[] CheckList(string strChannel)
        {
            char[] delimiterChars = { ';' };
            string[] words = strChannel.Split(delimiterChars);
            return words;
        }

        //選擇外購類別，要判斷顯示哪些外購類別細項
        private void cbBUY_ID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.cbBUYD_IDS.Items.Clear();     //使用前先清空
            string strBUYID = ""; //外購類別大項
            ComboBox cb = sender as ComboBox;
            ComboBoxItem cbm = (ComboBoxItem)cb.SelectedItem;
            strBUYID = cbm.Tag.ToString();

            for (int i = 0; i < cbBUYD_ID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbBUYD_ID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strBUYID))
                {
                    ComboBoxItem cbiPROGBUYDS = new ComboBoxItem();
                    cbiPROGBUYDS.Content = getcbi.Content;
                    cbiPROGBUYDS.Tag = getcbi.DataContext;
                    this.cbBUYD_IDS.Items.Add(cbiPROGBUYDS);
                }
            }
        }

        #endregion

        #region 檢查畫面資料及搬值

        private Class_PROPOSAL FormToClass_Proposal()
        {
            Class_PROPOSAL obj = new Class_PROPOSAL();

            obj.FSPRO_ID = this.tbxPRO_ID.Text;//成案單編號，2015/10/02 by Jarvis

            if (cbPRO_TYPE.SelectedIndex != -1)                     //成案種類
                obj.FCPRO_TYPE = ((ComboBoxItem)cbPRO_TYPE.SelectedItem).Tag.ToString();
            else
                obj.FCPRO_TYPE = "";

            if (cbPro_Status.SelectedIndex != -1)                   //成案狀態
                m_strStatus = ((ComboBoxItem)cbPro_Status.SelectedItem).Tag.ToString();
            else
                m_strStatus = "";

            obj.FSPROG_NAME = tbxPGMNAME.Text.ToString().Trim();   //節目名稱
            obj.FSPLAY_NAME = tbxWEBNAME.Text.ToString().Trim();   //播出名稱 
            obj.FSNAME_FOR = tbxPGMENAME.Text.ToString().Trim();   //外文名稱
            obj.FSCONTENT = tbxCONTENT.Text.ToString().Trim();     //節目內容簡述

            if (tbxPRDCEN.Text.Trim() != "")                       //製作單位
                obj.FSPRDCENID = tbxPRDCEN.Tag.ToString();
            else
                obj.FSPRDCENID = "";

            if (this.acbMenID.SelectedItem != null)              //製作人 by Jarvis20130523
            {
                obj.FSPRODUCER = ((UserStruct)this.acbMenID.SelectedItem).FSUSER_ID;
            }
            else
                obj.FSPRODUCER = "";

            if (tbxLENGTH.Text.ToString().Trim() != "")            //每集時長
                obj.FNLENGTH = Convert.ToInt32(tbxLENGTH.Text.ToString().Trim());
            else
                obj.FNLENGTH = 0;       //若是沒有填就給初始值0

            if (tbxBEG_EPISODE.Text.ToString().Trim() != "")       //集次(起)
                obj.FNBEG_EPISODE = Convert.ToInt16(tbxBEG_EPISODE.Text.ToString().Trim());
            else
                obj.FNBEG_EPISODE = 0;  //若是沒有填就給初始值0

            if (tbxEND_EPISODE.Text.ToString().Trim() != "")         //集次(迄)
                obj.FNEND_EPISODE = Convert.ToInt16(tbxEND_EPISODE.Text.ToString().Trim());
            else
                obj.FNEND_EPISODE = 0; //若是沒有填就給初始值0

            if (Check_ChannelList().Length == 0)   //預訂播出頻道
                obj.FSCHANNEL = "";
            else
                obj.FSCHANNEL = Check_ChannelList();

            if (dpSHOW_DATE.Text.ToString().Trim() == "")        //預訂上檔日期
                obj.FDSHOW_DATE = Convert.ToDateTime("1900/1/1");
            else
                obj.FDSHOW_DATE = Convert.ToDateTime(dpSHOW_DATE.Text);

            if (cbPRD_DEPT_ID.SelectedIndex != -1)                 //製作部門
                obj.FSPRD_DEPT_ID = ((ComboBoxItem)cbPRD_DEPT_ID.SelectedItem).Tag.ToString();
            else
                obj.FSPRD_DEPT_ID = "";

            if (cbPAY_DEPT_ID.SelectedIndex != -1)                //付款部門
                obj.FSPAY_DEPT_ID = ((ComboBoxItem)cbPAY_DEPT_ID.SelectedItem).Tag.ToString();
            else
                obj.FSPAY_DEPT_ID = "";

            obj.FSBEF_DEPT_ID = m_strBEF_DEPT;                    //前會部門
            obj.FSAFT_DEPT_ID = m_strAFT_DEPT;                    //後會部門

            if (cbOBJ_ID.SelectedIndex != -1)                     //製作目的
                obj.FSOBJ_ID = ((ComboBoxItem)cbOBJ_ID.SelectedItem).Tag.ToString();
            else
                obj.FSOBJ_ID = "";

            if (cbPROGGRADEID.SelectedIndex != -1)                //節目分級
                obj.FSGRADE_ID = ((ComboBoxItem)cbPROGGRADEID.SelectedItem).Tag.ToString();
            else
                obj.FSGRADE_ID = "";

            if (cbSRCID.SelectedIndex != -1)                     //節目來源
                obj.FSSRC_ID = ((ComboBoxItem)cbSRCID.SelectedItem).Tag.ToString();
            else
                obj.FSSRC_ID = "";

            if (cbTYPE.SelectedIndex != -1)                     //節目型態
                obj.FSTYPE = ((ComboBoxItem)cbTYPE.SelectedItem).Tag.ToString();
            else
                obj.FSTYPE = "";

            if (cbAUD_ID.SelectedIndex != -1)                   //目標觀眾
                obj.FSAUD_ID = ((ComboBoxItem)cbAUD_ID.SelectedItem).Tag.ToString();
            else
                obj.FSAUD_ID = "";

            if (cbSHOW_TYPE.SelectedIndex != -1)                //表現方式
                obj.FSSHOW_TYPE = ((ComboBoxItem)cbSHOW_TYPE.SelectedItem).Tag.ToString();
            else
                obj.FSSHOW_TYPE = "";

            if (cbLANG_ID_MAIN.SelectedIndex != -1)             //主聲道
                obj.FSLANG_ID_MAIN = ((ComboBoxItem)cbLANG_ID_MAIN.SelectedItem).Tag.ToString();
            else
                obj.FSLANG_ID_MAIN = "";

            if (cbLANG_ID_SUB.SelectedIndex != -1)              //副聲道
                obj.FSLANG_ID_SUB = ((ComboBoxItem)cbLANG_ID_SUB.SelectedItem).Tag.ToString();
            else
                obj.FSLANG_ID_SUB = "";

            if (cbBUY_ID.SelectedIndex != -1)                   //外購類別大類
                obj.FSBUY_ID = ((ComboBoxItem)cbBUY_ID.SelectedItem).Tag.ToString();
            else
                obj.FSBUY_ID = "";

            if (cbBUYD_IDS.SelectedIndex != -1)                 //外購類別細類
                obj.FSBUYD_ID = ((ComboBoxItem)cbBUYD_IDS.SelectedItem).Tag.ToString();
            else
                obj.FSBUYD_ID = "";

            obj.FSSIGN_DEPT_ID = "";    //簽核單位，先放空
            obj.FCCHECK_STATUS = "";
            obj.FSCHANNEL_ID = "";

            if (cbEmergency.IsChecked == true)    //緊急成案
                obj.FCEMERGENCY = "Y";     
            else
                obj.FCEMERGENCY = "";

            if (cbCompleteN.IsChecked == true)    //資料補齊
            {
                obj.FCEMERGENCY = "Y";  
                obj.FCCOMPLETE = "N"; 
            }                    
            else
                obj.FCCOMPLETE = "";

            obj.FSPROGSPEC = Check_PROGSPECList();                                //節目規格   

            if (cbPROGD.SelectedItem == null)
                obj.FCPROGD = "";
            else
                obj.FCPROGD = ((ComboBoxItem)cbPROGD.SelectedItem).Tag.ToString();//產生子集

            obj.FSENOTE = tbxENOTE.Text.ToString().Trim();                        //分集備註

            return obj;
        }

        private string Check_PROGSPECList()    //檢查節目規格有哪些
        {
            string strReturn = "";
            for (int i = 0; i < listPROGSPEC.Items.Count; i++)
            {
                CheckBox getckb = (CheckBox)listPROGSPEC.Items.ElementAt(i);
                if (getckb.IsChecked == true)
                {
                    strReturn += getckb.Tag.ToString().Trim() + ";";
                }
            }
            return strReturn;
        }

        private string Check_ChannelList()    //檢查可播映頻道有哪些
        {
            string strReturn = "";
            List<string> ListCHANNEL = new List<string>();  //TEST
            for (int i = 0; i < listCHANNEL.Items.Count; i++)
            {
                CheckBox getckb = (CheckBox)listCHANNEL.Items.ElementAt(i);
                if (getckb.IsChecked == true)
                {
                    strReturn += getckb.Tag.ToString().Trim() + ";";
                }
            }
            return strReturn;
        }

        private Boolean Check_ProposalData()    //檢查畫面上的欄位是否都填妥
        {
            StringBuilder strMessage = new StringBuilder();
            double intCheck;   //純粹為了判斷是否為數字型態之用

            if (tbxLENGTH.Text.Trim() != "")
            {
                if (double.TryParse(tbxLENGTH.Text.Trim(), out intCheck) == false)
                    strMessage.AppendLine("請檢查「節目長度」欄位必須為數值型態");
            }

            if (tbxBEG_EPISODE.Text.Trim() != "")
            {
                if (double.TryParse(tbxBEG_EPISODE.Text.Trim(), out intCheck) == false)
                    strMessage.AppendLine("請檢查「集數(起)」欄位必須為數值型態");
            }

            if (tbxEND_EPISODE.Text.Trim() != "")
            {
                if (double.TryParse(tbxEND_EPISODE.Text.Trim(), out intCheck) == false)
                    strMessage.AppendLine("請檢查「集數(迄)」欄位必須為數值型態");
            }

            if (strMessage.ToString().Trim() == "")
                return true;
            else
            {
                MessageBoxResult result = MessageBox.Show(strMessage.ToString(), "提示訊息", MessageBoxButton.OK);
                return false;
            }
        }

        //前會部門
        private void btnBEF_DEPT_ID_Click(object sender, RoutedEventArgs e)
        {
            PRO.PRO100_07 PRO100_07_frm = new PRO.PRO100_07(m_strBEF_DEPT, "BEF");
            PRO100_07_frm.Show();

            PRO100_07_frm.Closing += (s, args) =>
            {
                if (PRO100_07_frm.DialogResult == true)
                {
                    m_strBEF_DEPT = PRO100_07_frm.m_Dept;
                    tbBEF_DEPT_ID.Text = PRO100_07_frm.m_DeptName;
                }
            };
        }

        //後會部門
        private void btnAFT_DEPT_ID_Click(object sender, RoutedEventArgs e)
        {
            PRO.PRO100_07 PRO100_07_frm = new PRO.PRO100_07(m_strAFT_DEPT, "AFT");
            PRO100_07_frm.Show();

            PRO100_07_frm.Closing += (s, args) =>
            {
                if (PRO100_07_frm.DialogResult == true)
                {
                    m_strAFT_DEPT = PRO100_07_frm.m_Dept;
                    tbAFT_DEPT_ID.Text = PRO100_07_frm.m_DeptName;
                }
            };
        }

        #endregion

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (Check_ProposalData() == false)      //檢查畫面上的欄位是否都填妥
                return;

            //查詢成案資料
            clientPRO.fnQUERYTBPROPOSALAsync(FormToClass_Proposal(), (dpStart.Text == "" ? DateTime.MinValue : Convert.ToDateTime(dpStart.Text)), (dpEnd.Text==""?DateTime.MaxValue:Convert.ToDateTime(dpEnd.Text).AddDays(1)), m_strStatus);
            //clientPRO.fnQUERYTBPROPOSAL_BYUSERIDAsync(FormToClass_Proposal(), UserClass.userData.FSUSER_ID.ToString().Trim());//文嘉提說成案資料安全性還好，因此查詢時皆可查詢

            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //選取製作單位
        private void btnPRDCEN_Click(object sender, RoutedEventArgs e)
        {
            PRG100_11 PRG100_11_frm = new PRG100_11();
            PRG100_11_frm.Show();

            PRG100_11_frm.Closed += (s, args) =>
            {
                if (PRG100_11_frm.DialogResult == true)
                {
                    tbxPRDCEN.Text = PRG100_11_frm.strPRDCENName_View;
                    tbxPRDCEN.Tag = PRG100_11_frm.strPRDCENID_View;
                }
            };
        }

        void SetControlsTabIndex()
        {
            this.cbPRO_TYPE.TabIndex = 0;
            this.cbEmergency.TabIndex = 1;
            this.cbCompleteN.TabIndex = 2;
            this.dpStart.TabIndex = 3;
            this.dpEnd.TabIndex = 4;
            this.cbPro_Status.TabIndex = 5;
            this.cbOBJ_ID.TabIndex = 6;
            this.cbPRD_DEPT_ID.TabIndex = 7;
            this.btnPRDCEN.TabIndex = 8;
            this.cbPAY_DEPT_ID.TabIndex = 9;
            this.acbMenID.TabIndex = 10;
            this.listCHANNEL.TabIndex = 11;
            this.tbxPGMNAME.TabIndex = 12;
            this.tbxPGMENAME.TabIndex = 13;
            this.tbxWEBNAME.TabIndex = 14;
            this.tbxLENGTH.TabIndex = 15;
            this.tbxBEG_EPISODE.TabIndex = 16;
            this.tbxEND_EPISODE.TabIndex = 17;
            this.listPROGSPEC.TabIndex = 18;
            this.cbPROGD.TabIndex = 19;
            this.tbxENOTE.TabIndex = 20;
            this.tbxCONTENT.TabIndex = 21;
            this.btnBEF_DEPT_ID.TabIndex = 22;
            this.btnAFT_DEPT_ID.TabIndex = 23;
            this.cbPROGGRADEID.TabIndex = 24;
            this.dpSHOW_DATE.TabIndex = 25;
            this.cbSRCID.TabIndex = 26;
            this.cbTYPE.TabIndex = 27;
            this.cbAUD_ID.TabIndex = 28;
            this.cbSHOW_TYPE.TabIndex = 29;
            this.cbLANG_ID_MAIN.TabIndex = 30;
            this.cbLANG_ID_SUB.TabIndex = 31;
            this.cbBUY_ID.TabIndex = 32;
            this.cbBUYD_IDS.TabIndex = 33;
            this.listDEPT.TabIndex = 34;
            this.cbPRDCENID.TabIndex = 35;
            this.cbBUYD_ID.TabIndex = 36;
            this.OKButton.TabIndex = 37;
            this.CancelButton.TabIndex = 38;
        }

    }
}

