﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using System.Xml.Linq;
using System.IO;
using PTS_MAM3.PROG_M;
using PTS_MAM3.WSPROG_M;
using PTS_MAM3.WSPROPOSAL;
using PTS_MAM3.PRG;
using PTS_MAM3.FlowMAM;
using PTS_MAM3.WSPROG_PRODUCER;

namespace PTS_MAM3.Proposal
{
    public partial class PROPOSAL_Edit : ChildWindow
    {
        //產生新的代理類別 2013/04/29 by Jarvis
        WSPROG_PRODUCERSoapClient clientPRODUCER = new WSPROG_PRODUCERSoapClient();
        List<UserStruct> ListUserAll = new List<UserStruct>();
        Class_PROGPRODUCER ProducerObj = null;

        WSPROG_MSoapClient client = new WSPROG_MSoapClient();   //產生新的代理類別
        WSPROPOSALSoapClient clientPRO = new WSPROPOSALSoapClient();
        FlowMAMSoapClient clientFlow = new FlowMAMSoapClient(); //後端的流程引擎
        Class_PROPOSAL m_FormData = new Class_PROPOSAL();
        Boolean CheckOK = false;                                //檢查更新是否成功(上一層要判斷用的)
        string strProgName = "";                                //暫存節目名稱 
        string m_strChannelID = "";                             //頻道別不會異動 
        string m_strBEF_DEPT = "";
        string m_strAFT_DEPT = "";
        public int intProcessID;                                //Flow專用
        string strPstatus = "";                                 //Flow專用-審核結果

        public PROPOSAL_Edit(Class_PROPOSAL FormData)
        { 
            InitializeComponent();
            InitializeForm();        //初始化本頁面
            m_FormData = FormData;
            ClassToForm();

            //緊急成案的按鈕要卡權限
            if (ModuleClass.getModulePermissionByName("01", "緊急成案") == false)
                cbEmergency.IsEnabled = false;
         }

        public PROPOSAL_Edit(string strFromID)  //簽核頁面
        {
            if (strFromID.ToString().Trim() == "")
                return;

            InitializeComponent();

            //下載代碼檔
            client.fnGetTBPROG_M_CODECompleted += new EventHandler<fnGetTBPROG_M_CODECompletedEventArgs>(client_fnGetTBPROG_M_CODECompleted);

            //下載外購類別細項代碼檔
            client.fnGetTBPROG_M_PROGBUYD_CODECompleted += new EventHandler<fnGetTBPROG_M_PROGBUYD_CODECompletedEventArgs>(client_fnGetTBPROG_M_PROGBUYD_CODECompleted);

            //查詢成案資料
            clientPRO.fnQUERYTBPROPOSAL_BYIDCompleted += new EventHandler<fnQUERYTBPROPOSAL_BYIDCompletedEventArgs>(clientPRO_fnQUERYTBPROPOSAL_BYIDCompleted);
            clientPRO.fnQUERYTBPROPOSAL_BYIDAsync(strFromID.ToString().Trim());

            //修改成案資料
            clientPRO.fnUPDATETBPROPOSALCompleted += new EventHandler<fnUPDATETBPROPOSALCompletedEventArgs>(clientPRO_fnUPDATETBPROPOSALCompleted);

            //查詢是否有相同名稱的節目資料
            clientPRO.QUERY_TBPROGNAME_CHECKCompleted += new EventHandler<QUERY_TBPROGNAME_CHECKCompletedEventArgs>(clientPRO_QUERY_TBPROGNAME_CHECKCompleted);

            //透過節目編號查詢節目基本資料(目前在web services補上內容欄位)
            //client.fnGetProg_MCompleted += new EventHandler<fnGetProg_MCompletedEventArgs>(client_fnGetProg_MCompleted);

            this.Title = "通過/不通過 成案資料";
            this.OKButton.Content = "修改";
            btnApprove.Visibility = Visibility.Visible;
            btnReject.Visibility = Visibility.Visible;

            //緊急成案的按鈕要卡權限
            if (ModuleClass.getModulePermissionByName("01", "緊急成案") == false)
                cbEmergency.IsEnabled = false;

            //後端的流程引擎-審核流程
            clientFlow.CallFlow_WorkWithFieldCompleted += new EventHandler<CallFlow_WorkWithFieldCompletedEventArgs>(clientFlow_CallFlow_WorkWithFieldCompleted);

            
        }
        
        void InitializeForm() //初始化本頁面
        {
            SetControlsTabIndex();

            //下載代碼檔
            client.fnGetTBPROG_M_CODECompleted += new EventHandler<fnGetTBPROG_M_CODECompletedEventArgs>(client_fnGetTBPROG_M_CODECompleted);
            client.fnGetTBPROG_M_CODEAsync();

            //下載外購類別細項代碼檔
            client.fnGetTBPROG_M_PROGBUYD_CODECompleted += new EventHandler<fnGetTBPROG_M_PROGBUYD_CODECompletedEventArgs>(client_fnGetTBPROG_M_PROGBUYD_CODECompleted);
            client.fnGetTBPROG_M_PROGBUYD_CODEAsync();

            //修改成案資料
            clientPRO.fnUPDATETBPROPOSALCompleted += new EventHandler<fnUPDATETBPROPOSALCompletedEventArgs>(clientPRO_fnUPDATETBPROPOSALCompleted);

            //查詢是否有相同名稱的節目資料
            clientPRO.QUERY_TBPROGNAME_CHECKCompleted += new EventHandler<QUERY_TBPROGNAME_CHECKCompletedEventArgs>(clientPRO_QUERY_TBPROGNAME_CHECKCompleted);

            //查詢所有使用者資料 2013/04/29 by Jarvis
            clientPRODUCER.QUERY_TBUSERS_ALLCompleted += new EventHandler<QUERY_TBUSERS_ALLCompletedEventArgs>(clientPRODUCER_QUERY_TBUSERS_ALLCompleted);
            clientPRODUCER.QUERY_TBUSERS_ALLAsync();

        }
        //設定控制量的TabIndex,由PRO100_01.xaml匯出設定
       

        //實作-查詢所有使用者資料 2013/04/29 by Jarvis
        void clientPRODUCER_QUERY_TBUSERS_ALLCompleted(object sender, QUERY_TBUSERS_ALLCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                acbMenID.Text = m_FormData.FSPRODUSER_NAME;
                //ListUserAll = e.Result;
                acbMenID.ItemsSource = e.Result; //繫結AutoComplete按鈕
            }
            else
                MessageBox.Show("查詢使用者資料異常！", "提示訊息", MessageBoxButton.OK);

        }


        //實作-修改成案資料
        void clientPRO_fnUPDATETBPROPOSALCompleted(object sender, fnUPDATETBPROPOSALCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    CheckOK = true;

                    if (strPstatus != "")
                        ContinueFlow(strPstatus);     //送至流程處理                    
                    else
                    {
                        MessageBox.Show("修改成案資料「" + strProgName.ToString() + "」成功！", "提示訊息", MessageBoxButton.OK);
                        this.DialogResult = true;
                    }                 
                }
                else
                {
                    CheckOK = false;
                    MessageBox.Show("修改成案資料「" + strProgName.ToString() + "」失敗！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
        }

        //實作-查詢成案資料
        void clientPRO_fnQUERYTBPROPOSAL_BYIDCompleted(object sender, fnQUERYTBPROPOSAL_BYIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result.Count== 0)
                {
                    MessageBox.Show("傳入參數異常，請檢查！", "提示訊息", MessageBoxButton.OK);
                    return;
                }

                if (e.Result != null && e.Result.Count > 0)
                {
                    //指定繫集的class及下拉式選單要繫集的class
                    this.gdPROPOSAL_Edit.DataContext = e.Result[0];             
                    m_FormData = (Class_PROPOSAL)(e.Result[0]);
                    acbMenID.Text = m_FormData.FSPRODUSER_NAME;
                    if (m_FormData.SHOW_FDSHOW_DATE == "")
                        dpSHOW_DATE.Text = "";
                    else
                        dpSHOW_DATE.Text = m_FormData.FDSHOW_DATE.ToString();  //日期格式比較特別，用指定的方式                    

                    //if (m_FormData.FCPRO_TYPE.Trim() == "2")               //續製要去找到目前節目主檔的內容，目前在web services補上內容欄位
                    //    client.fnGetProg_MAsync(m_FormData.FSPROG_ID.Trim());

                    client.fnGetTBPROG_M_CODEAsync();
                    client.fnGetTBPROG_M_PROGBUYD_CODEAsync();
                }
            }
        }

        private void ClassToForm()  //將前一頁點選的節目資料繫集到畫面上
        {
            this.gdPROPOSAL_Edit.DataContext = m_FormData;

            if (m_FormData.SHOW_FDSHOW_DATE == "")
                dpSHOW_DATE.Text = "";
            else
                dpSHOW_DATE.Text = m_FormData.FDSHOW_DATE.ToString();  //日期格式比較特別，用指定的方式
        }

        //實作-查詢是否有相同名稱的節目資料
        void clientPRO_QUERY_TBPROGNAME_CHECKCompleted(object sender, QUERY_TBPROGNAME_CHECKCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result == false)
                {
                    //修改節目成案資料  
                    clientPRO.fnUPDATETBPROPOSALAsync(FormToClass_Proposal());

                    BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                }
                else
                {
                    btnApprove.IsEnabled = true;
                    btnReject.IsEnabled = true;
                    CancelButton.IsEnabled = true;
                    OKButton.IsEnabled = true;
                    MessageBox.Show("已有重複的「節目名稱」資料，請檢查後重新輸入！", "提示訊息", MessageBoxButton.OK);
                }
                    
            }
        }

        //實作-透過節目編號查詢節目基本資料
        void client_fnGetProg_MCompleted(object sender, fnGetProg_MCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                tbxCONTENT.Text = (e.Result)[0].FSCONTENT.Trim() + "\n" + "以下是本次續製新增:" + "\n" + m_FormData.FSCONTENT.Trim();               
            }            
        }

        //實作-審核流程
        void clientFlow_CallFlow_WorkWithFieldCompleted(object sender, CallFlow_WorkWithFieldCompletedEventArgs e)
        {
            if (e.Error == null && e.Result != "")
                MessageBox.Show("節目「" + tbxPGMNAME.Text + "」審核完成！", "提示訊息", MessageBoxButton.OK);
            else
                MessageBox.Show("呼叫流程引擎異常，節目「" + tbxPGMNAME.Text + "」審核失敗！", "提示訊息", MessageBoxButton.OK);

            this.DialogResult = true;
        }

        #region ComboBox載入代碼檔

        //實作-下載外購類別細項代碼檔
        void client_fnGetTBPROG_M_PROGBUYD_CODECompleted(object sender, fnGetTBPROG_M_PROGBUYD_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        ComboBoxItem cbiPROGBUYD = new ComboBoxItem();
                        cbiPROGBUYD.Content = CodeData.NAME;
                        cbiPROGBUYD.Tag = CodeData.ID;
                        cbiPROGBUYD.DataContext = CodeData.IDD;
                        this.cbBUYD_ID.Items.Add(cbiPROGBUYD);   //先都產生在隱藏起來的細項代碼combobox裡                   
                        //m_CodeDataBuyD.Add(CodeData);
                    }
                }
            }
        }

        //實作-下載所有的代碼檔
        void client_fnGetTBPROG_M_CODECompleted(object sender, fnGetTBPROG_M_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    //付款部門代碼，空白要自己帶入
                    ComboBoxItem cbiPAYNull = new ComboBoxItem();
                    cbiPAYNull.Content = " ";
                    cbiPAYNull.Tag = "";
                    this.cbPAY_DEPT_ID.Items.Add(cbiPAYNull);

                    //外購類別代碼，空白要自己帶入
                    ComboBoxItem cbiBUY_IDNull = new ComboBoxItem();
                    cbiBUY_IDNull.Content = " ";
                    cbiBUY_IDNull.Tag = "";
                    this.cbBUY_ID.Items.Add(cbiBUY_IDNull);

                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            case "TBZPROGOBJ":      //製作目的代碼
                                ComboBoxItem cbiPROGOBJ = new ComboBoxItem();
                                cbiPROGOBJ.Content = CodeData.NAME;
                                cbiPROGOBJ.Tag = CodeData.ID;

                                //判斷有無修改此製作目的的權限，若是無，就要把選項反白讓使用者無法選擇
                                //特別注意，續製要鎖開始集數及製作目的

                                //若是審核成案表單，就要依照節目目的鎖住權限，若是修改成案表單，則不鎖權限
                                if (this.Title.ToString().IndexOf("通過") > -1)
                                {
                                    if (ModuleClass.getModulePermissionByName("00", CodeData.NAME) == false)
                                        cbiPROGOBJ.IsEnabled = false;
                                }

                                this.cbOBJ_ID.Items.Add(cbiPROGOBJ);
                                break;

                            case "TBZDEPT":         //製作部門別代碼，因為部門檔有些是必填，有些不是必填，因此這裡代表必填要把空白濾掉
                                if (CodeData.NAME.Trim() != "") 
                                {
                                    ComboBoxItem cbiPRD = new ComboBoxItem();
                                    cbiPRD.Content = CodeData.NAME;
                                    cbiPRD.Tag = CodeData.ID;
                                    this.cbPRD_DEPT_ID.Items.Add(cbiPRD);
                                }

                                //付款部門別代碼
                                ComboBoxItem cbiPAY = new ComboBoxItem();
                                cbiPAY.Content = CodeData.NAME;
                                cbiPAY.Tag = CodeData.ID;
                                this.cbPAY_DEPT_ID.Items.Add(cbiPAY);

                                //前後會部門別代碼
                                if (CodeData.NAME.Trim() != "")
                                {
                                    CheckBox cbiBEF = new CheckBox();
                                    cbiBEF.Content = CodeData.NAME;
                                    cbiBEF.Tag = CodeData.ID;
                                    this.listDEPT.Items.Add(cbiBEF);
                                }
                                 
                                break;

                            case "TBZPROGGRADE":      //節目分級代碼
                                ComboBoxItem cbiPROGGRADE = new ComboBoxItem();
                                cbiPROGGRADE.Content = CodeData.NAME;
                                cbiPROGGRADE.Tag = CodeData.ID;
                                this.cbPROGGRADEID.Items.Add(cbiPROGGRADE);
                                break;

                            case "TBZPROGSRC":      //節目來源代碼
                                ComboBoxItem cbiSRC = new ComboBoxItem();
                                cbiSRC.Content = CodeData.NAME;
                                cbiSRC.Tag = CodeData.ID;
                                this.cbSRCID.Items.Add(cbiSRC);
                                break;

                            case "TBZPROGATTR":      //內容屬性代碼
                                ComboBoxItem cbiTYPE = new ComboBoxItem();
                                cbiTYPE.Content = CodeData.NAME;
                                cbiTYPE.Tag = CodeData.ID;
                                this.cbTYPE.Items.Add(cbiTYPE);
                                break;

                            case "TBZPROGAUD":      //目標觀眾代碼
                                ComboBoxItem cbiAUD = new ComboBoxItem();
                                cbiAUD.Content = CodeData.NAME;
                                cbiAUD.Tag = CodeData.ID;
                                this.cbAUD_ID.Items.Add(cbiAUD);
                                break;

                            case "TBZPROGTYPE":      //表現方式代碼
                                ComboBoxItem cbiSHOW_TYPE = new ComboBoxItem();
                                cbiSHOW_TYPE.Content = CodeData.NAME;
                                cbiSHOW_TYPE.Tag = CodeData.ID;
                                this.cbSHOW_TYPE.Items.Add(cbiSHOW_TYPE);
                                break;

                            case "TBZPROGLANG":      //語言代碼
                                ComboBoxItem cbiLANG_ID_MAIN = new ComboBoxItem();
                                cbiLANG_ID_MAIN.Content = CodeData.NAME;
                                cbiLANG_ID_MAIN.Tag = CodeData.ID;
                                this.cbLANG_ID_MAIN.Items.Add(cbiLANG_ID_MAIN);  //主聲道

                                ComboBoxItem cbiLANG_ID_SUB = new ComboBoxItem();
                                cbiLANG_ID_SUB.Content = CodeData.NAME;
                                cbiLANG_ID_SUB.Tag = CodeData.ID;
                                this.cbLANG_ID_SUB.Items.Add(cbiLANG_ID_SUB);  //副聲道
                                break;

                            case "TBZPROGBUY":       //外購類別代碼
                                ComboBoxItem cbiBUY_ID = new ComboBoxItem();
                                cbiBUY_ID.Content = CodeData.NAME;
                                cbiBUY_ID.Tag = CodeData.ID;
                                this.cbBUY_ID.Items.Add(cbiBUY_ID);
                                break;

                            case "TBZCHANNEL_PRO":       //頻道別代碼
                                CheckBox cbCHANNEL = new CheckBox();
                                cbCHANNEL.Content = CodeData.NAME;
                                cbCHANNEL.Tag = CodeData.ID;
                                listCHANNEL.Items.Add(cbCHANNEL);
                                break;

                            case "TBZPRDCENTER":      //製作單位代碼
                                if (CodeData.ID.Trim() != "00000")  //舊資料的代碼要略過
                                {
                                    ComboBoxItem cbiPRDCENTER = new ComboBoxItem();
                                    cbiPRDCENTER.Content = CodeData.NAME;
                                    cbiPRDCENTER.Tag = CodeData.ID;
                                    this.cbPRDCENID.Items.Add(cbiPRDCENTER);
                                }
                                break;

                            case "TBZPROGSPEC":       //節目規格代碼
                                CheckBox cbPROGSPEC = new CheckBox();
                                cbPROGSPEC.Content = CodeData.NAME;
                                cbPROGSPEC.Tag = CodeData.ID;
                                listPROGSPEC.Items.Add(cbPROGSPEC);
                                break;

                            case "TBZPROGNATION":       //來源國家    2012/11/22 kyle                            
                                ComboBoxItem cbNATION = new ComboBoxItem();
                                cbNATION.Content = CodeData.NAME;
                                cbNATION.Tag = CodeData.ID;
                                cbNATION_ID.Items.Add(cbNATION);
                                break;

                            default:
                                break;
                        }
                    }

                    //將「其他」欄位加入前後會部門檔，ID為99
                    if (listDEPT.Items.Count > 0)
                    {
                        CheckBox cbiBEF = new CheckBox();
                        cbiBEF.Content = "其他";
                        cbiBEF.Tag = "99";
                        this.listDEPT.Items.Add(cbiBEF);
                    }

                    //比對代碼檔
                    compareCode_FSPRDDEPTID(m_FormData.FSPRD_DEPT_ID.ToString());
                    compareCode_FSPAYDEPTID(m_FormData.FSPAY_DEPT_ID.ToString());  
                    compareCode_FSOBJID(m_FormData.FSOBJ_ID.ToString());
                    compareCode_FSGRADEID(m_FormData.FSGRADE_ID.ToString());
                    compareCode_FSSRCID(m_FormData.FSSRC_ID.ToString());
                    compareCode_FSTYPEID(m_FormData.FSTYPE.ToString());
                    compareCode_FSAUDID(m_FormData.FSAUD_ID.ToString());
                    compareCode_FSSHOW_TYPE(m_FormData.FSSHOW_TYPE.ToString());
                    compareCode_FSLANG_ID_MAIN(m_FormData.FSLANG_ID_MAIN.ToString());
                    compareCode_FSLANG_ID_SUB(m_FormData.FSLANG_ID_SUB.ToString());
                    compareCode_FSBUY_ID(m_FormData.FSBUY_ID.ToString());
                    compareCode_FSBUYD_ID(m_FormData.FSBUYD_ID.ToString());
                    compareCode_FSCHANNEL(CheckList(m_FormData.FSCHANNEL.ToString()));
                    comparePRO_TYPE(m_FormData.FCPRO_TYPE.ToString());
                    compareCode_FSPRDCENID(m_FormData.FSPRDCENID.ToString());
                    m_strBEF_DEPT = m_FormData.FSBEF_DEPT_ID.ToString();
                    m_strAFT_DEPT = m_FormData.FSAFT_DEPT_ID.ToString();
                    tbBEF_DEPT_ID.Text = compareCode_DEPT(CheckList(m_FormData.FSBEF_DEPT_ID.ToString()));
                    tbAFT_DEPT_ID.Text = compareCode_DEPT(CheckList(m_FormData.FSAFT_DEPT_ID.ToString()));
                    compareCode_FSPROGSPEC(CheckList(m_FormData.FSPROGSPEC.ToString()));
                    compareProgD(m_FormData.FCPROGD.ToString());
                    m_strChannelID = m_FormData.FSCHANNEL_ID.Trim();

                    //加入新的三個欄位
                    if (m_FormData.FDEXPIRE_DATE.ToShortDateString() == "1900/1/1")
                    {
                        dpEXPIRE.Text = "";
                    }
                    else
                        dpEXPIRE.Text = m_FormData.FDEXPIRE_DATE.ToShortDateString();
                    if (m_FormData.FSEXPIRE_DATE_ACTION == "Y")
                    {
                        cbDELETE.IsChecked = true;
                    }
                    else
                    {
                        cbDELETE.IsChecked = false;
                    }
                    cbNATION_ID.SelectedItem = cbNATION_ID.Items.Where(C => ((ComboBoxItem)C).Tag.ToString() == m_FormData.FSPROGNATIONID).FirstOrDefault();   //國家代碼

                    if (m_FormData.FCEMERGENCY.Trim() == "Y")    //緊急成案
                        cbEmergency.IsChecked = true;
                }
            }
        }

        private string[] CheckList(string strChannel)
        {
            char[] delimiterChars = { ';' };
            string[] words = strChannel.Split(delimiterChars);
            return words;
        }

        //選擇外購類別，要判斷顯示哪些外購類別細項
        private void cbBUY_ID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.cbBUYD_IDS.Items.Clear();     //使用前先清空
            string strBUYID = ""; //外購類別大項
            ComboBox cb = sender as ComboBox;
            ComboBoxItem cbm = (ComboBoxItem)cb.SelectedItem;
            strBUYID = cbm.Tag.ToString();

            for (int i = 0; i < cbBUYD_ID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbBUYD_ID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strBUYID))
                {
                    ComboBoxItem cbiPROGBUYDS = new ComboBoxItem();
                    cbiPROGBUYDS.Content = getcbi.Content;
                    cbiPROGBUYDS.Tag = getcbi.DataContext;
                    this.cbBUYD_IDS.Items.Add(cbiPROGBUYDS);
                }
            }
        }

        #endregion

        #region 比對代碼檔

        //比對代碼檔_製作部門
        void compareCode_FSPRDDEPTID(string strID)
        {
            for (int i = 0; i < cbPRD_DEPT_ID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPRD_DEPT_ID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbPRD_DEPT_ID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_付款部門
        void compareCode_FSPAYDEPTID(string strID)
        {
            for (int i = 0; i < cbPAY_DEPT_ID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPAY_DEPT_ID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbPAY_DEPT_ID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_製作目的
        void compareCode_FSOBJID(string strID)
        {
            for (int i = 0; i < cbOBJ_ID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbOBJ_ID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbOBJ_ID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_節目分級
        void compareCode_FSGRADEID(string strID)
        {
            for (int i = 0; i < cbPROGGRADEID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPROGGRADEID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbPROGGRADEID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_節目來源
        void compareCode_FSSRCID(string strID)
        {
            for (int i = 0; i < cbSRCID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbSRCID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbSRCID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_節目型態
        void compareCode_FSTYPEID(string strID)
        {
            for (int i = 0; i < cbTYPE.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbTYPE.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbTYPE.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_目標觀眾
        void compareCode_FSAUDID(string strID)
        {
            for (int i = 0; i < cbAUD_ID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbAUD_ID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbAUD_ID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_表現方式
        void compareCode_FSSHOW_TYPE(string strID)
        {
            for (int i = 0; i < cbSHOW_TYPE.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbSHOW_TYPE.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbSHOW_TYPE.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_主聲道
        void compareCode_FSLANG_ID_MAIN(string strID)
        {
            for (int i = 0; i < cbLANG_ID_MAIN.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbLANG_ID_MAIN.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbLANG_ID_MAIN.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_副聲道
        void compareCode_FSLANG_ID_SUB(string strID)
        {
            for (int i = 0; i < cbLANG_ID_SUB.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbLANG_ID_SUB.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbLANG_ID_SUB.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_外購類別大類
        void compareCode_FSBUY_ID(string strID)
        {
            for (int i = 0; i < cbBUY_ID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbBUY_ID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbBUY_ID.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_外購類別細類
        void compareCode_FSBUYD_ID(string strID)
        {
            for (int i = 0; i < cbBUYD_IDS.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbBUYD_IDS.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbBUYD_IDS.SelectedIndex = i;
                    break;
                }
            }
        }

        //比對代碼檔_製作單位代碼
        void compareCode_FSPRDCENID(string strID)
        {
            for (int i = 0; i < cbPRDCENID.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPRDCENID.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbPRDCENID.SelectedIndex = i;
                    tbxPRDCEN.Text = getcbi.Content.ToString().Trim();
                    tbxPRDCEN.Tag = getcbi.Tag.ToString().Trim();
                    break;
                }
            }
        }

        //比對代碼檔_可播映頻道
        void compareCode_FSCHANNEL(string[] ListTEST_Send)
        {
            //MessageBox.Show(
            //listCHANNEL.Items.ElementAt(0).GetType().ToString());

            for (int i = 0; i < ListTEST_Send.Length - 1; i++)
            {
                for (int j = 0; j < listCHANNEL.Items.Count; j++)
                {
                    CheckBox getcki = (CheckBox)listCHANNEL.Items.ElementAt(j);

                    if (getcki.Tag.ToString().Trim().Equals(ListTEST_Send[i]))
                    {
                        getcki.IsChecked = true;
                        break;
                    }
                }
            }
        }

        //比對代碼檔_節目規格
        void compareCode_FSPROGSPEC(string[] ListTEST_Send)
        {
            for (int i = 0; i < ListTEST_Send.Length - 1; i++)
            {
                for (int j = 0; j < listPROGSPEC.Items.Count; j++)
                {
                    CheckBox getcki = (CheckBox)listPROGSPEC.Items.ElementAt(j);

                    if (getcki.Tag.ToString().Trim().Equals(ListTEST_Send[i]))
                    {
                        getcki.IsChecked = true;
                        break;
                    }
                }
            }
        }

        //比對代碼檔_前後會部門
        string compareCode_DEPT(string[] ListTEST_Send)
        {
            string strDept = "";
            for (int i = 0; i < ListTEST_Send.Length - 1; i++)
            {
                for (int j = 0; j < listDEPT.Items.Count; j++)
                {
                    CheckBox getcki = (CheckBox)listDEPT.Items.ElementAt(j);

                    if (getcki.Tag.ToString().Trim().Equals(ListTEST_Send[i]))
                    {
                        getcki.IsChecked = true;
                        strDept += getcki.Content.ToString().Trim() + ";";
                    }
                }
            }
            return strDept;
        }

        //比對節目種類
        void comparePRO_TYPE(string strID)
        {
            if (strID.Trim() == "2")    //續製只能鎖開始集數及製作目的
                EnabledControl();

            for (int i = 0; i < cbPRO_TYPE.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPRO_TYPE.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {              
                    cbPRO_TYPE.SelectedIndex = i;
                    break;
                }
            }
        }

        //是否為產生子集
        private void compareProgD(string strProgD)   
        {
            if (strProgD == "N")
                cbPROGD.SelectedIndex = 0;
            else if (strProgD == "Y")
                cbPROGD.SelectedIndex = 1;
        }

        //若是為續製，要鎖住不給修改，續製只能鎖開始集數及製作目的
        private void EnabledControl()
        {
            cbOBJ_ID.IsEnabled = false;
            tbxBEG_EPISODE.IsEnabled = false;
        }

        #endregion

        #region 檢查畫面資料及搬值

        private Class_PROPOSAL FormToClass_Proposal()
        {
            Class_PROPOSAL obj = new Class_PROPOSAL();

            obj.FSPRO_ID = tbxPRO_ID.Text.ToString();   //成案單編號
            //obj.FSPROG_ID = "";                         //節目編號(從webservice取得)

            if (acbMenID.SelectedItem != null)
            { obj.FSPRODUCER = ((UserStruct)(acbMenID.SelectedItem)).FSUSER_ID.Trim(); } //製作人ID 2013/04/29 by Jarvis 
            else
            { obj.FSPRODUCER = m_FormData.FSPRODUCER; }

            if (cbPRO_TYPE.SelectedIndex != -1)        //成案種類
                obj.FCPRO_TYPE = ((ComboBoxItem)cbPRO_TYPE.SelectedItem).Tag.ToString();

            obj.FSPROG_NAME = tbxPGMNAME.Text.ToString().Trim();   //節目名稱
            obj.FSPLAY_NAME = tbxWEBNAME.Text.ToString().Trim();   //播出名稱 
            obj.FSNAME_FOR = tbxPGMENAME.Text.ToString().Trim();   //外文名稱
            obj.FSCONTENT = tbxCONTENT.Text.ToString().Trim();     //節目內容簡述
            obj.FNLENGTH = Convert.ToInt32(tbxLENGTH.Text.ToString().Trim());              //每集時長
            obj.FNBEG_EPISODE = Convert.ToInt16(tbxBEG_EPISODE.Text.ToString().Trim());    //集次(起)
            obj.FNEND_EPISODE = Convert.ToInt16(tbxEND_EPISODE.Text.ToString().Trim());    //集次(迄)
            obj.FSCHANNEL = Check_ChannelList();                   //預訂播出頻道

            if (dpSHOW_DATE.Text == "")
                obj.FDSHOW_DATE = Convert.ToDateTime("1900/1/1");
            else
                obj.FDSHOW_DATE = Convert.ToDateTime(dpSHOW_DATE.Text);//預訂上檔日期

            if (tbxPRDCEN.Text.Trim() != "")                       //製作單位
                obj.FSPRDCENID = tbxPRDCEN.Tag.ToString();
            else
                obj.FSPRDCENID = "";

            if (cbPRD_DEPT_ID.SelectedIndex != -1)                 //製作部門
                obj.FSPRD_DEPT_ID = ((ComboBoxItem)cbPRD_DEPT_ID.SelectedItem).Tag.ToString();
            else
                obj.FSPRD_DEPT_ID = "";

            if (cbPAY_DEPT_ID.SelectedIndex != -1)                //付款部門
                obj.FSPAY_DEPT_ID = ((ComboBoxItem)cbPAY_DEPT_ID.SelectedItem).Tag.ToString();
            else
                obj.FSPAY_DEPT_ID = "";

            obj.FSBEF_DEPT_ID = m_strBEF_DEPT;                    //前會部門
            obj.FSAFT_DEPT_ID = m_strAFT_DEPT;                    //後會部門

            if (cbOBJ_ID.SelectedIndex != -1)                     //製作目的
                obj.FSOBJ_ID = ((ComboBoxItem)cbOBJ_ID.SelectedItem).Tag.ToString();
            else
                obj.FSOBJ_ID = "";

            if (cbPROGGRADEID.SelectedIndex != -1)                //節目分級
                obj.FSGRADE_ID = ((ComboBoxItem)cbPROGGRADEID.SelectedItem).Tag.ToString();
            else
                obj.FSGRADE_ID = "";

            if (cbSRCID.SelectedIndex != -1)                     //節目來源
                obj.FSSRC_ID = ((ComboBoxItem)cbSRCID.SelectedItem).Tag.ToString();
            else
                obj.FSSRC_ID = "";

            if (cbTYPE.SelectedIndex != -1)                     //節目型態
                obj.FSTYPE = ((ComboBoxItem)cbTYPE.SelectedItem).Tag.ToString();
            else
                obj.FSTYPE = "";

            if (cbAUD_ID.SelectedIndex != -1)                   //目標觀眾
                obj.FSAUD_ID = ((ComboBoxItem)cbAUD_ID.SelectedItem).Tag.ToString();
            else
                obj.FSAUD_ID = "";

            if (cbSHOW_TYPE.SelectedIndex != -1)                //表現方式
                obj.FSSHOW_TYPE = ((ComboBoxItem)cbSHOW_TYPE.SelectedItem).Tag.ToString();
            else
                obj.FSSHOW_TYPE = "";

            if (cbLANG_ID_MAIN.SelectedIndex != -1)             //主聲道
                obj.FSLANG_ID_MAIN = ((ComboBoxItem)cbLANG_ID_MAIN.SelectedItem).Tag.ToString();
            else
                obj.FSLANG_ID_MAIN = "";

            if (cbLANG_ID_SUB.SelectedIndex != -1)              //副聲道
                obj.FSLANG_ID_SUB = ((ComboBoxItem)cbLANG_ID_SUB.SelectedItem).Tag.ToString();
            else
                obj.FSLANG_ID_SUB = "";

            if (cbBUY_ID.SelectedIndex != -1)                   //外購類別大類
                obj.FSBUY_ID = ((ComboBoxItem)cbBUY_ID.SelectedItem).Tag.ToString();
            else
                obj.FSBUY_ID = "";

            if (cbBUYD_IDS.SelectedIndex != -1)                 //外購類別細類
                obj.FSBUYD_ID = ((ComboBoxItem)cbBUYD_IDS.SelectedItem).Tag.ToString();
            else
                obj.FSBUYD_ID = "";

            obj.FSSIGN_DEPT_ID = "";                            //簽核單位，先放空
            obj.FCCHECK_STATUS = "N";                           //狀態
            obj.FSCHANNEL_ID = m_strChannelID;                  //修改成案單時，頻道別不會改變

            if (cbEmergency.IsChecked == true)                  //緊急成案
                obj.FCEMERGENCY = "Y";
            else
                obj.FCEMERGENCY = "N";

            obj.FSPROGSPEC = Check_PROGSPECList();                                //節目規格   
            obj.FCPROGD = ((ComboBoxItem)cbPROGD.SelectedItem).Tag.ToString();    //產生子集
            obj.FSENOTE = tbxENOTE.Text.ToString().Trim();                        //分集備註

            obj.FSCREATED_BY = UserClass.userData.FSUSER_ID.ToString();           //建檔者
            obj.FSUPDATED_BY = UserClass.userData.FSUSER_ID.ToString();           //修改者

            //新加的三個欄位

            if (cbDELETE.IsChecked == false)                          //到期日期後是否刪除
                obj.FSEXPIRE_DATE_ACTION = "N";
            else
                obj.FSEXPIRE_DATE_ACTION = "Y";


            if (dpEXPIRE.Text == "")
                obj.FDEXPIRE_DATE = Convert.ToDateTime("1900/1/1");
            else
                obj.FDEXPIRE_DATE = Convert.ToDateTime(dpEXPIRE.Text).AddHours(23).AddMinutes(59);  //到期日期

            obj.FSPROGNATIONID = ((ComboBoxItem)cbNATION_ID.SelectedItem).Tag.ToString();       //國家代碼

            strProgName = obj.FSPROG_NAME;  //暫存本次新增的節目名稱，為了秀給使用者看結果用

            obj.Origin_FDEXPIRE_DATE = m_FormData.Origin_FDEXPIRE_DATE;
            obj.Origin_FSEXPIRE_DATE_ACTION = m_FormData.Origin_FSEXPIRE_DATE_ACTION;

            return obj;
        }


        private string Check_ChannelList()    //檢查可播映頻道有哪些
        {
            string strReturn = "";
            List<string> ListCHANNEL = new List<string>();  //TEST
            for (int i = 0; i < listCHANNEL.Items.Count; i++)
            {
                CheckBox getckb = (CheckBox)listCHANNEL.Items.ElementAt(i);
                if (getckb.IsChecked == true)
                {
                    strReturn += getckb.Tag.ToString().Trim() + ";";
                }
            }
            return strReturn;
        }

        private string Check_PROGSPECList()    //檢查節目規格有哪些
        {
            string strReturn = "";
            for (int i = 0; i < listPROGSPEC.Items.Count; i++)
            {
                CheckBox getckb = (CheckBox)listPROGSPEC.Items.ElementAt(i);
                if (getckb.IsChecked == true)
                {
                    strReturn += getckb.Tag.ToString().Trim() + ";";
                }
            }
            return strReturn;
        }

        private Boolean Check_ProposalData()    //檢查畫面上的欄位是否都填妥
        {
            StringBuilder strMessage = new StringBuilder();
            int intCheck;               //純粹為了判斷是否為數字型態之用(integer)
            short shortCheck;           //純粹為了判斷是否為數字型態之用(smallint)

            short intBegEpisode = 0; //集數(起)
            short intEndEpisode = 0; //集數(迄)

            // 2013/05/01 by Jarvis
            if (m_FormData.FSPRODUCER == null)
            {
                strMessage.AppendLine("請輸入「製作人」欄位");

            }

            if (tbxPGMNAME.Text.Trim() == "")
                strMessage.AppendLine("請輸入「節目名稱」欄位");

            if (tbxWEBNAME.Text.Trim() == "")
                strMessage.AppendLine("請輸入「播出名稱」欄位");

            if (tbxLENGTH.Text.Trim() == "")
                strMessage.AppendLine("請輸入「節目長度」欄位");
            else
            {
                if (int.TryParse(tbxLENGTH.Text.Trim(), out intCheck) == false)
                    strMessage.AppendLine("請檢查「節目長度」欄位必須為數值型態");
            }

            if (tbxBEG_EPISODE.Text.Trim() == "")
                strMessage.AppendLine("請輸入「集數(起)」欄位");
            else
            {
                if (short.TryParse(tbxBEG_EPISODE.Text.Trim(), out shortCheck) == false)
                    strMessage.AppendLine("請檢查「集數(起)」欄位必須為數值型態");
                else
                    intBegEpisode = Convert.ToInt16(tbxBEG_EPISODE.Text.Trim());
            }

            if (tbxEND_EPISODE.Text.Trim() == "")
                strMessage.AppendLine("請輸入「集數(迄)」欄位");
            else
            {
                if (short.TryParse(tbxEND_EPISODE.Text.Trim(), out shortCheck) == false)
                    strMessage.AppendLine("請檢查「集數(迄)」欄位必須為數值型態");
                else
                    intEndEpisode = Convert.ToInt16(tbxEND_EPISODE.Text.Trim());
            }

            if (intBegEpisode != 0 & intEndEpisode!=0 & intBegEpisode > intEndEpisode)   //檢查起不能大於迄
                strMessage.AppendLine("請檢查「集數(迄)」欄位必須大於或等於「集數(起)」");

            if (cbPRO_TYPE.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「種類」欄位");

            if (cbOBJ_ID.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「製作目的」欄位");

            //if (dpSHOW_DATE.Text.Trim() == "")        //改成可不填
            //    strMessage.AppendLine("請輸入「預訂上檔日期」欄位");

            if (cbPRD_DEPT_ID.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「製作部門」欄位");

            if (cbPROGGRADEID.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「節目分級」欄位");

            if (cbSRCID.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「節目來源」欄位");

            if (cbTYPE.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「節目型態」欄位");

            if (cbAUD_ID.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「目標觀眾」欄位");

            if (cbSHOW_TYPE.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「表現方式」欄位");

            if (cbLANG_ID_MAIN.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「主聲道」欄位");

            if (cbLANG_ID_SUB.SelectedIndex == -1)
                strMessage.AppendLine("請輸入「副聲道」欄位");

            if (cbBUY_ID.SelectedIndex != -1　&&　((ComboBoxItem) (cbBUY_ID.SelectedItem)).Content.ToString().Trim() !="" && cbBUYD_IDS.SelectedIndex == -1) //因為設定外鍵，因此選擇了大項就要設細項
                strMessage.AppendLine("請輸入「外購類別細項」欄位");

            if (tbxPRDCEN.Text.Trim() == "")
                strMessage.AppendLine("請輸入「製作單位」欄位");

            if (Check_PROGSPECList().Trim() == "")
                strMessage.AppendLine("請勾選「節目規格」欄位");

            //若是勾選節目類型為新製或衍生，並且開始集數填寫的不是第0集，就要彈出訊息通知
            if (cbPRO_TYPE.SelectedIndex == 0 || cbPRO_TYPE.SelectedIndex == 2)
            {
                if (intBegEpisode != 1)
                    strMessage.AppendLine("節目種類為新製或衍生時，請檢查「集數(起)」欄位必須從第1集開始");
            }   

            //若是點選核可，還要判斷有無此製作目的的權限
            if (strPstatus == "Approve" && ModuleClass.getModulePermissionByName("00", ((ComboBoxItem)(cbOBJ_ID.SelectedItem)).Content.ToString().Trim()) == false)
                strMessage.AppendLine("無" + ((ComboBoxItem)(cbOBJ_ID.SelectedItem)).Content.ToString().Trim() + "的權限，請檢查製作目的權限");
           
            if (strMessage.ToString().Trim() == "")
                return true;
            else
            {
                MessageBoxResult result = MessageBox.Show(strMessage.ToString(), "提示訊息", MessageBoxButton.OK);
                return false;
            }
        }

        //前會部門
        private void btnBEF_DEPT_ID_Click(object sender, RoutedEventArgs e)
        {
            PRO.PRO100_07 PRO100_07_frm = new PRO.PRO100_07(m_strBEF_DEPT, "BEF");
            PRO100_07_frm.Show();

            PRO100_07_frm.Closing += (s, args) =>
            {
                if (PRO100_07_frm.DialogResult == true)
                {
                    m_strBEF_DEPT = PRO100_07_frm.m_Dept;
                    tbBEF_DEPT_ID.Text = PRO100_07_frm.m_DeptName;
                }
            };
        }

        //後會部門
        private void btnAFT_DEPT_ID_Click(object sender, RoutedEventArgs e)
        {
            PRO.PRO100_07 PRO100_07_frm = new PRO.PRO100_07(m_strAFT_DEPT, "AFT");
            PRO100_07_frm.Show();

            PRO100_07_frm.Closing += (s, args) =>
            {
                if (PRO100_07_frm.DialogResult == true)
                {
                    m_strAFT_DEPT = PRO100_07_frm.m_Dept;
                    tbAFT_DEPT_ID.Text = PRO100_07_frm.m_DeptName;
                }
            };
        }

        #endregion

        //更新成案主程式
        private void UpdatePro()    
        {
            if (Check_ProposalData() == false)      //檢查畫面上的欄位是否都填妥
                return;

            btnApprove.IsEnabled = false;           //一送出後就把按鈕鎖起來，避免使用者連續按
            btnReject.IsEnabled = false;
            CancelButton.IsEnabled = false;
            OKButton.IsEnabled = false;

            //續製節目以及不通過，不用去比對是否有相同節目名稱的資料
            if (cbPRO_TYPE.SelectedIndex == 1 || strPstatus == "Reject")  
            {
                 //修改節目成案資料  
                clientPRO.fnUPDATETBPROPOSALAsync(FormToClass_Proposal());
            }               
            else
                clientPRO.QUERY_TBPROGNAME_CHECKAsync(tbxPGMNAME.Text.ToString().Trim());   //查詢是否有相同節目名稱的資料 

            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }

        //確定
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            UpdatePro();
        }

        //核可
        private void btnApprove_Click(object sender, RoutedEventArgs e)
        {
            strPstatus = "Approve";
            UpdatePro(); 
        }

        //駁回
        private void btnReject_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult resultMsg = MessageBox.Show("確定要不通過「" + tbxPGMNAME.Text.Trim() + "」節目資料嗎?", "提示訊息", MessageBoxButton.OKCancel);
            if (resultMsg == MessageBoxResult.Cancel)
                return;

            strPstatus = "Reject";
            UpdatePro();
        }

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
        
        //若是沒有輸入播出名稱，點選時預帶節目名稱
        private void tbxWEBNAME_GotFocus(object sender, RoutedEventArgs e)
        {
            if (tbxWEBNAME.Text.ToString().Trim() == "" && tbxPGMNAME.Text.ToString().Trim() != "")
                tbxWEBNAME.Text = tbxPGMNAME.Text.ToString().Trim();
        }

        //選取製作單位
        private void btnPRDCEN_Click(object sender, RoutedEventArgs e)
        {
            PRG100_11 PRG100_11_frm = new PRG100_11();
            PRG100_11_frm.Show();

            PRG100_11_frm.Closed += (s, args) =>
            {
                if (PRG100_11_frm.DialogResult == true)
                {
                    tbxPRDCEN.Text = PRG100_11_frm.strPRDCENName_View;
                    tbxPRDCEN.Tag = PRG100_11_frm.strPRDCENID_View;                    
                }
            };
        }

        //自動去比對資料庫，若有人員就顯示資料 2013/04/29 by Jarvis
        private void acbMenID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (acbMenID.SelectedItem == null)
                tbxNote.Text = "";
            else
                tbxNote.Text = ((UserStruct)(acbMenID.SelectedItem)).FSTITLE_NAME.Trim();
        }
        void SetControlsTabIndex()
        {
            this.cbPRO_TYPE.TabIndex = 0;
            this.cbEmergency.TabIndex = 1;
            this.cbOBJ_ID.TabIndex = 2;
            this.cbPRD_DEPT_ID.TabIndex = 3;
            this.btnPRDCEN.TabIndex = 4;
            this.cbPAY_DEPT_ID.TabIndex = 5;
            this.acbMenID.TabIndex = 6;
            this.listCHANNEL.TabIndex = 7;
            this.tbxPGMNAME.TabIndex = 8;
            //this.btnPROG.TabIndex=9;
            this.tbxPGMENAME.TabIndex = 10;
            this.tbxWEBNAME.TabIndex = 11;
            this.tbxLENGTH.TabIndex = 12;
            this.tbxBEG_EPISODE.TabIndex = 13;
            this.tbxEND_EPISODE.TabIndex = 14;
            this.listPROGSPEC.TabIndex = 15;
            this.cbPROGD.TabIndex = 16;
            this.tbxENOTE.TabIndex = 17;
            this.tbxCONTENT.TabIndex = 18;
            this.btnBEF_DEPT_ID.TabIndex = 19;
            this.btnAFT_DEPT_ID.TabIndex = 20;
            this.cbPROGGRADEID.TabIndex = 21;
            this.dpSHOW_DATE.TabIndex = 22;
            this.cbSRCID.TabIndex = 23;
            this.cbTYPE.TabIndex = 24;
            this.cbAUD_ID.TabIndex = 25;
            this.cbSHOW_TYPE.TabIndex = 26;
            this.cbLANG_ID_MAIN.TabIndex = 27;
            this.cbLANG_ID_SUB.TabIndex = 28;
            this.cbBUY_ID.TabIndex = 29;
            this.cbBUYD_IDS.TabIndex = 30;
            this.dpEXPIRE.TabIndex = 31;
            this.cbDELETE.TabIndex = 32;
            this.cbNATION_ID.TabIndex = 33;
            this.listDEPT.TabIndex = 34;
            this.cbPRDCENID.TabIndex = 35;
            this.cbBUYD_ID.TabIndex = 36;
            this.OKButton.TabIndex = 37;
            this.CancelButton.TabIndex = 38;
        }
        

        #region 流程引擎

        void ContinueFlow(string strSW)
        {
            string strFlowStatus = "";
            string hid_SendTo = "";

            if (strSW == "Approve")         //核可
            {
                hid_SendTo = "1";
                strFlowStatus = "Y";
            }
            else if (strSW == "Reject")     //駁回
            {
                hid_SendTo = "2";
                strFlowStatus = "F";
            }

            StringBuilder sb = new StringBuilder();
            sb.Append(@"<VariableCollection>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>parameter01</name>");
            sb.Append(@"    <value>" + strFlowStatus + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>USERID</name>");
            sb.Append(@"    <value>" + UserClass.userData.FSUSER_ID + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>CancelForm</name>");
            sb.Append(@"    <value>1</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>hid_SendTo</name>");
            sb.Append(@"    <value>" + hid_SendTo + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"</VariableCollection>");//hid_SendTo

            //流程引擎放到後端處理
            clientFlow.CallFlow_WorkWithFieldAsync(intProcessID.ToString(), sb.ToString());

            //flowWebService.FlowSoapClient client = new flowWebService.FlowSoapClient();
            //client.WorkWithFieldAsync(intProcessID.ToString(), sb.ToString());
            //client.WorkWithFieldCompleted += (s, args) =>
            //{
            //    if (args.Error == null && args.Result !="")
            //        MessageBox.Show("節目「" + tbxPGMNAME.Text + "」審核完成！", "提示訊息", MessageBoxButton.OK);
            //    else
            //        MessageBox.Show("呼叫流程引擎異常，節目「" + tbxPGMNAME.Text + "」審核失敗！", "提示訊息", MessageBoxButton.OK);
                
            //    this.DialogResult = true;
            //};
        }

        #endregion
    }
}

