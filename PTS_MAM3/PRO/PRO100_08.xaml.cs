﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using System.Xml.Linq;
using System.IO;
using PTS_MAM3.PROG_M;
using PTS_MAM3.WSPROG_M;
using PTS_MAM3.WSPROPOSAL;

namespace PTS_MAM3.Proposal
{
    public partial class PRO100_08 : ChildWindow
    {
        WSPROPOSALSoapClient clientPRO = new WSPROPOSALSoapClient();
        Class_PROPOSAL m_FormData = new Class_PROPOSAL();
        string strProgName = "";                        //暫存節目名稱    

        public PRO100_08(Class_PROPOSAL FormData)
        { 
            InitializeComponent();
            InitializeForm();        //初始化本頁面
            m_FormData = FormData;
            ClassToForm(); 
         }

        void InitializeForm() //初始化本頁面
        {
            //修改成案資料_緊急流程           
            clientPRO.fnUPDATETBPROPOSAL_EMERGENCYCompleted += new EventHandler<fnUPDATETBPROPOSAL_EMERGENCYCompletedEventArgs>(clientPRO_fnUPDATETBPROPOSAL_EMERGENCYCompleted);
        }

        //實作-修改成案資料_緊急流程 
        void clientPRO_fnUPDATETBPROPOSAL_EMERGENCYCompleted(object sender, fnUPDATETBPROPOSAL_EMERGENCYCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result == true)
                {                  
                    MessageBox.Show("修改成案資料「" + strProgName.ToString() + "」成功！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = true;
                }
                else
                {                   
                    MessageBox.Show("修改成案資料「" + strProgName.ToString() + "」失敗！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
        }     

        private void ClassToForm()  //將前一頁點選的節目資料繫集到畫面上
        {
            this.gdPROPOSAL_Edit.DataContext = m_FormData;

            if (m_FormData.FCEMERGENCY.Trim() == "Y")
                cbEmergency.IsChecked = true;

            if (m_FormData.FCCOMPLETE.Trim() == "Y")
                cbComplete.IsChecked = true;

            comparePRO_TYPE(m_FormData.FCPRO_TYPE.ToString());
        }

        //比對節目種類
        void comparePRO_TYPE(string strID)
        {
            for (int i = 0; i < cbPRO_TYPE.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbPRO_TYPE.Items.ElementAt(i);
                if (getcbi.Tag.Equals(strID))
                {
                    cbPRO_TYPE.SelectedIndex = i;
                    break;
                }
            }
        }

        #region 檢查畫面資料及搬值

        private Class_PROPOSAL FormToClass_Proposal()
        {
            Class_PROPOSAL obj = new Class_PROPOSAL();

            obj.FSPRO_ID = tbxPRO_ID.Text.ToString();               //成案單編號
                        
            if (cbComplete.IsChecked == true)                       //資料補齊
            {
                obj.FCCOMPLETE = "Y";

                if (dpComplete.Text.Trim() == "")                       //預訂資料補齊日期
                    obj.FDCOMPLETE = new DateTime(1900, 1, 1);
                else
                    obj.FDCOMPLETE = Convert.ToDateTime(dpComplete.Text);
            }                
            else
            {
                obj.FCCOMPLETE = "N";
                obj.FDCOMPLETE = new DateTime(1900, 1, 1);
            }    
                        
            obj.FSUPDATED_BY = UserClass.userData.FSUSER_ID.ToString();           //修改者

            strProgName = tbxPGMNAME.Text.Trim();  //暫存本次新增的節目名稱，為了秀給使用者看結果用

            return obj;
        }       
      
        #endregion

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            clientPRO.fnUPDATETBPROPOSAL_EMERGENCYAsync(FormToClass_Proposal());//修改節目成案資料_緊急成案
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
          
    }
}

