﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.IO;
using PTS_MAM3.PROG_M;
using PTS_MAM3.WSPROG_M;
using PTS_MAM3.WSPROPOSAL;

namespace PTS_MAM3.PRO
{
    public partial class PRO100_09 : Page
    {
        WSPROPOSALSoapClient clientPRO = new WSPROPOSALSoapClient();
        WSPROG_MSoapClient client = new WSPROG_MSoapClient();

        List<Class_PROGOBJ_DEPT> PROGOBJ_DEPT_obj = new List<Class_PROGOBJ_DEPT>();
        List<Class_CODE> m_CodeDataDept = new List<Class_CODE>();
        List<Class_CODE> m_CodeDataObj = new List<Class_CODE>();

        public PRO100_09()
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面
        }

        void InitializeForm() //初始化本頁面
        {
            //查詢製作目的與前後會部門對照檔列表           
            clientPRO.QueryTBPROGOBJ_DEPT_LISTCompleted += new EventHandler<QueryTBPROGOBJ_DEPT_LISTCompletedEventArgs>(clientPRO_QueryTBPROGOBJ_DEPT_LISTCompleted);
            
            //下載代碼檔
            client.fnGetTBPROG_M_CODECompleted += new EventHandler<fnGetTBPROG_M_CODECompletedEventArgs>(client_fnGetTBPROG_M_CODECompleted);
            client.fnGetTBPROG_M_CODEAsync();

            //修改製作目的與前後會部門對照檔
            clientPRO.UPDATETBPROGOBJ_DEPT_LISTCompleted += new EventHandler<UPDATETBPROGOBJ_DEPT_LISTCompletedEventArgs>(clientPRO_UPDATETBPROGOBJ_DEPT_LISTCompleted);
            BusyMsg.IsBusy = true;
        }        

        //實作-查詢製作目的與前後會部門對照檔列表 
        void clientPRO_QueryTBPROGOBJ_DEPT_LISTCompleted(object sender, QueryTBPROGOBJ_DEPT_LISTCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                PROGOBJ_DEPT_obj = e.Result;

                for (int i = 0; i < PROGOBJ_DEPT_obj.Count; i++)
                {
                    PROGOBJ_DEPT_obj[i].SHOW_FSOBJ_ID = Check_OBJ_NAME(PROGOBJ_DEPT_obj[i].FSOBJ_ID);
                    PROGOBJ_DEPT_obj[i].SHOW_FSBEF_DEPT_ID = Check_DEPT_NAME_List(CheckList(PROGOBJ_DEPT_obj[i].FSBEF_DEPT_ID));
                    PROGOBJ_DEPT_obj[i].SHOW_FSAFT_DEPT_ID = Check_DEPT_NAME_List(CheckList(PROGOBJ_DEPT_obj[i].FSAFT_DEPT_ID));
                }

                DGOBJ.ItemsSource = PROGOBJ_DEPT_obj;                

               tbxOBJ.Text = "";
               tbxBEF.Text = "";
               tbxAFT.Text = "";
            }
        }

        //實作-下載所有的代碼檔
        void client_fnGetTBPROG_M_CODECompleted(object sender, fnGetTBPROG_M_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            case "TBZDEPT":
                                //前後會部門別代碼，因為部門檔有些是必填，有些不是必填，因此這裡代表必填要把空白濾掉
                                if (CodeData.NAME.Trim() != "")                               
                                    m_CodeDataDept.Add(CodeData);
                                break;

                            case "TBZPROGOBJ":
                                //製作目的代碼
                                if (CodeData.NAME.Trim() != "")                              
                                    m_CodeDataObj.Add(CodeData);
                                break;

                            default:
                                break;
                        }
                    }                 
                }
            }

            clientPRO.QueryTBPROGOBJ_DEPT_LISTAsync();
        }

        //實作-修改製作目的與前後會部門對照檔
        void clientPRO_UPDATETBPROGOBJ_DEPT_LISTCompleted(object sender, UPDATETBPROGOBJ_DEPT_LISTCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    MessageBox.Show("修改製作目的與前後會部門對照檔資料成功！", "提示訊息", MessageBoxButton.OK); 
                    clientPRO.QueryTBPROGOBJ_DEPT_LISTAsync();
                    BusyMsg.IsBusy = true; 
                }                   
                else
                    MessageBox.Show("修改製作目的與前後會部門對照檔資料失敗！", "提示訊息", MessageBoxButton.OK); 
            }
        }

        private string[] CheckList(string strIDList)
        {
            char[] delimiterChars = { ';' };
            string[] words = strIDList.Split(delimiterChars);
            return words;
        }

        //比對代碼檔_部門名稱
        string Check_DEPT_NAME_List(string[] ListSend)
        {
            string strReturn = "";

            for (int i = 0; i < ListSend.Length - 1; i++)
            {
                for (int j = 0; j < m_CodeDataDept.Count; j++)
                {
                    if (m_CodeDataDept[j].ID.ToString().Trim().Equals(ListSend[i]))
                    {
                        strReturn = strReturn + m_CodeDataDept[j].NAME.Trim() + ";";
                        break;
                    }
                }
            }

            return strReturn;
        }

        //比對代碼檔_製作目的名稱
        string Check_OBJ_NAME(string strID)
        {
            string strReturn = "";

            for (int i = 0; i < m_CodeDataObj.Count; i++)    
            {
                if (m_CodeDataObj[i].ID.ToString().Trim().Equals(strID))    
                {
                    strReturn = m_CodeDataObj[i].NAME.Trim();    
                    break;    
                }    
            }              

            return strReturn;
        }

        //DataGrid Click
        private void DGOBJ_RowClicked(object sender, DataGridDCTest.DataGridRowClickedArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGOBJ.SelectedItem == null)
            {
                MessageBox.Show("請選擇預修改的製作目的資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            tbxOBJ.Text = ((Class_PROGOBJ_DEPT)(DGOBJ.SelectedItem)).SHOW_FSOBJ_ID;
            tbxBEF.Text = ((Class_PROGOBJ_DEPT)(DGOBJ.SelectedItem)).SHOW_FSBEF_DEPT_ID;
            tbxAFT.Text = ((Class_PROGOBJ_DEPT)(DGOBJ.SelectedItem)).SHOW_FSAFT_DEPT_ID;
        }

        //重新繫集DataGrid
        private void LoadDataGrid()
        {
            DGOBJ.ItemsSource = null;
            DGOBJ.ItemsSource = PROGOBJ_DEPT_obj;
        }

        //確定
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            clientPRO.UPDATETBPROGOBJ_DEPT_LISTAsync(PROGOBJ_DEPT_obj,UserClass.userData.FSUSER_ID.ToString());//修改製作目的與前後會部門對照檔
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }

        //前會部門
        private void btnBEF_DEPT_ID_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGOBJ.SelectedItem == null)
            {
                MessageBox.Show("請選擇預修改的製作目的資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            PRO.PRO100_07 PRO100_07_frm = new PRO.PRO100_07(((Class_PROGOBJ_DEPT)(DGOBJ.SelectedItem)).FSBEF_DEPT_ID, "BEF");
            PRO100_07_frm.Show();

            PRO100_07_frm.Closing += (s, args) =>
            {
                if (PRO100_07_frm.DialogResult == true)
                {
                    ((Class_PROGOBJ_DEPT)(DGOBJ.SelectedItem)).FSBEF_DEPT_ID = PRO100_07_frm.m_Dept;
                    ((Class_PROGOBJ_DEPT)(DGOBJ.SelectedItem)).SHOW_FSBEF_DEPT_ID = PRO100_07_frm.m_DeptName;
                    tbxBEF.Text = PRO100_07_frm.m_DeptName;                    
                }
            };
        }

        //後會部門
        private void btnAFT_DEPT_ID_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGOBJ.SelectedItem == null)
            {
                MessageBox.Show("請選擇預修改的製作目的資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            PRO.PRO100_07 PRO100_07_frm = new PRO.PRO100_07(((Class_PROGOBJ_DEPT)(DGOBJ.SelectedItem)).FSAFT_DEPT_ID, "BEF");
            PRO100_07_frm.Show();

            PRO100_07_frm.Closing += (s, args) =>
            {
                if (PRO100_07_frm.DialogResult == true)
                {
                    ((Class_PROGOBJ_DEPT)(DGOBJ.SelectedItem)).FSAFT_DEPT_ID = PRO100_07_frm.m_Dept;
                    ((Class_PROGOBJ_DEPT)(DGOBJ.SelectedItem)).SHOW_FSAFT_DEPT_ID = PRO100_07_frm.m_DeptName;
                    tbxAFT.Text = PRO100_07_frm.m_DeptName;                    
                }
            };
        }

    }
}
