﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSBROADCAST;
using PTS_MAM3.PRG;
using PTS_MAM3.ProgData;
using System.ComponentModel;
using PTS_MAM3.STT;
using System.Reflection;
using System.Windows.Browser;
using System.Windows.Data;

namespace PTS_MAM3.STB
{
    public enum ActionType { Unknow, Create, View, Edit, Copy, Delete }

    public partial class STB100_01 : ChildWindow, INotifyPropertyChanged
    {
        PTS_MAM3.WSBROADCAST.WSBROADCASTSoapClient Brocast_Client = new PTS_MAM3.WSBROADCAST.WSBROADCASTSoapClient();
        PTS_MAM3.WSMAMFunctions.WSMAMFunctionsSoapClient MAM_Client = new PTS_MAM3.WSMAMFunctions.WSMAMFunctionsSoapClient();
        WSPROG_M.WSPROG_MSoapClient programClient = new PTS_MAM3.WSPROG_M.WSPROG_MSoapClient();
        WSPROMO.WSPROMOSoapClient promoClient = new WSPROMO.WSPROMOSoapClient();
        WSPROG_D.WSPROG_DSoapClient episodeClient = new WSPROG_D.WSPROG_DSoapClient();


        WSHD_MC.WSHD_MCSoapClient hdMC_CLient = new WSHD_MC.WSHD_MCSoapClient();

        string Arc_type_ID = "";

        Class_BROADCAST brocast = null;
        public Class_BROADCAST Brocast
        {
            get { return brocast; }
            set { brocast = value; RaisePropertyChanged("Brocast"); }
        }

        Class_LOG_VIDEO log_video = new Class_LOG_VIDEO();

        public Class_LOG_VIDEO Log_video
        {
            get { return log_video; }
            set { log_video = value; RaisePropertyChanged("Log_video"); }
        }

        List<Class_LOG_VIDEO> videoList = new List<Class_LOG_VIDEO>();

        public List<Class_LOG_VIDEO> VideoList
        {
            get { return videoList; }
            set { videoList = value; RaisePropertyChanged("VideoList"); }
        }



        FormType form_Type = FormType.Normal;//視窗類型 送播單 置換送播單 後製控制中心
        ActionType Bro_ActionType = ActionType.Unknow;//單子的類型 送播單、送播置換單、後製審核
        ActionType seg_ActionType = ActionType.Unknow;//對段落檔的動作類型 新 修 刪

        //bool IsCopy_Segment = false;       //是否是複製的段落

        /// <summary>
        /// true=Reject;false=Upload
        /// </summary>
        bool? isRejectOrUpload = null;//是退單還是標記上傳

        //List<Class_LOG_VIDEO> ListLogVideo = new List<Class_LOG_VIDEO>();
        string m_DIRID = ""; //要長tree用的資料夾編號
        //string m_strFileNONew = "";

        string oldBroadcast_ID = "";//舊的單號，複製表單時用的

        //複製表單的流程 取原單的Log_Video→取新的VIDEOID→取新的FileNo→取得新的單號 完成
        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="_100Type">頁面的類型(送播單/送播置換單/後製)</param>
        public STB100_01(FormType _100Type)//新增
        {
            InitializeComponent();

            Bro_ActionType = ActionType.Create;

            Brocast = new Class_BROADCAST();
            Brocast.FSBRO_TYPE = "G";
            form_Type = _100Type;
            if (form_Type == FormType.Displacement)
            {
                this.tbBro_ID.Text = "送播置換單編號";
            }
            InitialChildWindow();
        }

        /// <summary>
        /// 修改或檢視
        /// </summary>
        /// <param name="sourceBroadcast"></param>
        /// <param name="action">要執行的動作類型</param>
        /// <param name="_100Type">頁面的類型(送播單/送播置換單/後製)</param>
        public STB100_01(Class_BROADCAST sourceBroadcast, ActionType action, FormType _100Type)
        {
            InitializeComponent();

            if (sourceBroadcast.FSBRO_TYPE == "G")
            {
                this.rdbProg.IsChecked = true;
            }
            else if (sourceBroadcast.FSBRO_TYPE == "P")
            {
                this.rdbPromo.IsChecked = true;
            }

            Bro_ActionType = action;

            Brocast = CopyNewBroadcast(sourceBroadcast);
            form_Type = _100Type;

            InitialChildWindow();
        }

        Class_BROADCAST CopyNewBroadcast(Class_BROADCAST sourceBroadcast)
        {
            Class_BROADCAST newBroadcast = new Class_BROADCAST();

            PropertyInfo[] properties = newBroadcast.GetType().GetProperties();

            foreach (PropertyInfo pi in properties)
            {
                pi.SetValue(newBroadcast, pi.GetValue(sourceBroadcast, null), null);
            }

            return newBroadcast;
        }


        void ChangeArcType_Show(string arcTypeID)
        {
            this.cbArcType.SelectedIndex = Convert.ToInt32(arcTypeID) - 1;
        }

        /// <summary>
        /// 複製表單
        /// </summary>
        /// <param name="sourceBroadcast"></param>
        /// <param name="video_list">原單的LogVideo</param>
        /// <param name="_100Type">頁面的類型(送播單/送播置換單/後製)</param>
        public STB100_01(Class_BROADCAST sourceBroadcast, List<Class_LOG_VIDEO> video_list, FormType _100Type)//複製
        {
            InitializeComponent();

            Bro_ActionType = ActionType.Copy;

            Class_BROADCAST newBroadcast = new Class_BROADCAST();
            PropertyInfo[] properArr = newBroadcast.GetType().GetProperties();
            foreach (PropertyInfo pi in properArr)
            {
                pi.SetValue(newBroadcast, pi.GetValue(sourceBroadcast, null), null);
            }

            newBroadcast.FNEPISODE = 0;
            newBroadcast.FNEPISODE_NAME = "";

            if (newBroadcast.FSBRO_TYPE == "P")
            {
                newBroadcast.FSID = "";
                newBroadcast.FSID_NAME = "";
            }

            newBroadcast.FSMEMO = "";
            Brocast = newBroadcast;



            VideoList = video_list;
            foreach (Class_LOG_VIDEO video in VideoList)
            {
                video.FCFILE_STATUS = "B";
                video.FCFILE_STATUS_NAME = "";
            }

            form_Type = _100Type;
            this.HasCloseButton = false;


            InitialChildWindow();

            if (Brocast.FSBRO_TYPE == "G")
            {
                this.rdbProg.IsChecked = true;
                programClient.fnGetProg_MAsync(Brocast.FSID);
            }
            else if (Brocast.FSBRO_TYPE == "P")
            {
                this.rdbPromo.IsChecked = true;
                //promoClient.GetTBPGM_PROMO_BYPROMONAMEAsync(Brocast.FSID_NAME);
            }

        }

        void InitialChildWindow()
        {
            #region webservice 完成事件
            this.Loaded += new RoutedEventHandler(STB100_01_Loaded);
            //取一個新的單號
            MAM_Client.GetNoRecordCompleted += new EventHandler<WSMAMFunctions.GetNoRecordCompletedEventArgs>(MAM_Client_GetNoRecordCompleted);

            //取得新的VideoID
            Brocast_Client.QueryVideoID_PROG_STTCompleted += new EventHandler<WSBROADCAST.QueryVideoID_PROG_STTCompletedEventArgs>(Brocast_Client_QueryVideoID_PROG_STTCompleted);

            //取得新的檔案編號
            Brocast_Client.fnGetFileIDCompleted += new EventHandler<WSBROADCAST.fnGetFileIDCompletedEventArgs>(Brocast_Client_fnGetFileIDCompleted);

            //取得代碼檔
            Brocast_Client.fnGetTBBROADCAST_CODECompleted += new EventHandler<fnGetTBBROADCAST_CODECompletedEventArgs>(Brocast_Client_fnGetTBBROADCAST_CODECompleted);

            //新增單子
            Brocast_Client.INSERT_BROADCAST_WITHOUT_FLOWCompleted += new EventHandler<INSERT_BROADCAST_WITHOUT_FLOWCompletedEventArgs>(Brocast_Client_INSERT_BROADCAST_WITHOUT_FLOWCompleted);
            //查詢48小時內有無排播(短帶)
            Brocast_Client.GetPROMO_PLAY_NOTECompleted += new EventHandler<GetPROMO_PLAY_NOTECompletedEventArgs>(Brocast_Client_GetPROMO_PLAY_NOTECompleted);

            //查詢48小時內有無排播(節目)
            Brocast_Client.GetPROG_PLAY_NOTECompleted += new EventHandler<GetPROG_PLAY_NOTECompletedEventArgs>(Brocast_Client_GetPROG_PLAY_NOTECompleted);

            Brocast_Client.GetTBLOG_VIDEO_BY_Type_EPISODECompleted += new EventHandler<GetTBLOG_VIDEO_BY_Type_EPISODECompletedEventArgs>(Brocast_Client_GetTBLOG_VIDEO_BY_Type_EPISODECompleted);

            Brocast_Client.CallREPORT_BRO_DetailCompleted += new EventHandler<CallREPORT_BRO_DetailCompletedEventArgs>(Brocast_Client_CallREPORT_BRO_DetailCompleted);

            Brocast_Client.CallReport_Archive_BigCompleted += new EventHandler<CallReport_Archive_BigCompletedEventArgs>(Brocast_Client_CallReport_Archive_BigCompleted);
            Brocast_Client.CallReport_Archive_SmallCompleted += new EventHandler<CallReport_Archive_SmallCompletedEventArgs>(Brocast_Client_CallReport_Archive_SmallCompleted);
            Brocast_Client.CallReport_Archive_StickCompleted += new EventHandler<CallReport_Archive_StickCompletedEventArgs>(Brocast_Client_CallReport_Archive_StickCompleted);
            Brocast_Client.fnGetTBBROADCAST_CODEAsync();

            Brocast_Client.UPDATE_TBBROADCAST_StatusCompleted += new EventHandler<UPDATE_TBBROADCAST_StatusCompletedEventArgs>(Brocast_Client_UPDATE_TBBROADCAST_StausCompleted);

            Brocast_Client.GetTBBROADCAST_BYBROIDCompleted += new EventHandler<GetTBBROADCAST_BYBROIDCompletedEventArgs>(Brocast_Client_GetTBBROADCAST_BYBROIDCompleted);

            Brocast_Client.Query_CheckIsAbleToCreateNewTransformFormCompleted += new EventHandler<Query_CheckIsAbleToCreateNewTransformFormCompletedEventArgs>(Brocast_Client_Query_CheckIsAbleToCreateNewTransformFormCompleted);
            //Brocast_Client.INSERT_LOG_VIDEOCompleted += new EventHandler<INSERT_LOG_VIDEOCompletedEventArgs>(Brocast_Client_INSERT_LOG_VIDEOCompleted);

            //Brocast_Client.INSERT_TBLOG_VIDEO_SEGCompleted += new EventHandler<INSERT_TBLOG_VIDEO_SEGCompletedEventArgs>(Brocast_Client_INSERT_TBLOG_VIDEO_SEGCompleted);

            Brocast_Client.INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLISTCompleted += new EventHandler<INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLISTCompletedEventArgs>(Brocast_Client_INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLISTCompleted);

            Brocast_Client.DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONECompleted += new EventHandler<DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONECompletedEventArgs>(Brocast_Client_DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONECompleted);

            Brocast_Client.Delete_BROADCAST_AT_ONCECompleted += new EventHandler<Delete_BROADCAST_AT_ONCECompletedEventArgs>(Brocast_Client_Delete_BROADCAST_AT_ONCECompleted);

            hdMC_CLient.Add_HD_MCCompleted += new EventHandler<WSHD_MC.Add_HD_MCCompletedEventArgs>(hdMC_CLient_Add_HD_MCCompleted);
            Brocast_Client.Set_Class_LOG_VIDEO_AND_SEGCompleted += new EventHandler<Set_Class_LOG_VIDEO_AND_SEGCompletedEventArgs>(Brocast_Client_Set_Class_LOG_VIDEO_AND_SEGCompleted);
            Brocast_Client.Set_Class_LOG_VIDEO_AND_SEG_NewVideoIDCompleted += new EventHandler<Set_Class_LOG_VIDEO_AND_SEG_NewVideoIDCompletedEventArgs>(Brocast_Client_Set_Class_LOG_VIDEO_AND_SEG_NewVideoIDCompleted);


            programClient.fnGetProg_MCompleted += new EventHandler<WSPROG_M.fnGetProg_MCompletedEventArgs>(programClient_fnGetProg_MCompleted);
            episodeClient.GetProg_DCompleted += new EventHandler<WSPROG_D.GetProg_DCompletedEventArgs>(episodeClient_GetProg_DCompleted);
            promoClient.GetTBPGM_PROMO_BYPROMONAMECompleted += new EventHandler<WSPROMO.GetTBPGM_PROMO_BYPROMONAMECompletedEventArgs>(promoClient_GetTBPGM_PROMO_BYPROMONAMECompleted);


            Brocast_Client.UPDATE_TBLOG_VIDEO_REVIEWCompleted += new EventHandler<UPDATE_TBLOG_VIDEO_REVIEWCompletedEventArgs>(Brocast_Client_UPDATE_TBLOG_VIDEO_REVIEWCompleted);

            Brocast_Client.Insert_VideoID_MapCompleted += new EventHandler<Insert_VideoID_MapCompletedEventArgs>(Brocast_Client_Insert_VideoID_MapCompleted);

            Brocast_Client.QueryVideoID_PROG_OnceCompleted += new EventHandler<QueryVideoID_PROG_OnceCompletedEventArgs>(Brocast_Client_QueryVideoID_PROG_OnceCompleted);

            Brocast_Client.CallREPORT_BRO_PROMOCompleted += new EventHandler<CallREPORT_BRO_PROMOCompletedEventArgs>(Brocast_Client_CallREPORT_BRO_PROMOCompleted);

            Brocast_Client.UPDATE_TBLOG_VIDEO_TRACKCompleted += new EventHandler<UPDATE_TBLOG_VIDEO_TRACKCompletedEventArgs>(Brocast_Client_UPDATE_TBLOG_VIDEO_TRACKCompleted);
            #endregion

            bool isDisplacement = form_Type == FormType.Displacement;
            Brocast.FCCHANGE = isDisplacement ? "Y" : "N";

            if (Bro_ActionType == ActionType.Create || Bro_ActionType == ActionType.Copy)
                this.Title = "新增送播" + (isDisplacement ? "置換" : "") + "單";
            else if (Bro_ActionType == ActionType.View)
                this.Title = "檢視送播" + (isDisplacement ? "置換" : "") + "單";
            else if (Bro_ActionType == ActionType.Edit)
                this.Title = "修改送播" + (isDisplacement ? "置換" : "") + "單";

            if (isDisplacement)
            {
                this.btnSeg.Content = "新增置換段落";
                this.btnEdit.Content = "修改置換段落";
                this.btnDel.Content = "刪除置換段落";
                this.btnCopy.Content = "複製置換段落";
                this.btnCopy.Visibility = System.Windows.Visibility.Visible;
                this.btnCopy_Bro.Visibility = System.Windows.Visibility.Collapsed;
                bottomGrid.ColumnDefinitions[1].Width = new GridLength();

            }
            else
            {
                bottomGrid.ColumnDefinitions[1].Width = new GridLength(1, GridUnitType.Star);
            }

            //Brocast.FSBRO_TYPE = this.rdbProg.IsChecked == true ? "G" : "P";

            if (Bro_ActionType == ActionType.Create || Bro_ActionType == ActionType.Copy)
            {
                this.btnViewSeg.Visibility = System.Windows.Visibility.Collapsed;

                if (Bro_ActionType == ActionType.Copy)
                {
                    this.DGLogList.Columns[1].Visibility = System.Windows.Visibility.Collapsed;
                    this.DGLogList.Columns[2].Visibility = System.Windows.Visibility.Visible;

                    this.tbVideo_ARC_TYPE.Visibility = System.Windows.Visibility.Collapsed;
                    this.cbArcType.Visibility = System.Windows.Visibility.Collapsed;
                }
                MAM_Client.GetNoRecordAsync("07", "", "", UserClass.userData.FSUSER_ID);
            }
            else
            {
                LoadTBLOG_VIDEO();
            }

            if (Bro_ActionType == ActionType.Edit && Brocast.FCCHECK_STATUS != "R")
            {
                LockView_All();
                this.OKButton.IsEnabled = true;
                this.CancelButton.IsEnabled = true;
                this.btnSeg.Visibility = System.Windows.Visibility.Collapsed;
                this.btnDel.Visibility = System.Windows.Visibility.Collapsed;
                this.btnCopy.Visibility = System.Windows.Visibility.Collapsed;
            }
            if (Bro_ActionType == ActionType.Edit && Brocast.FCCHECK_STATUS == "R")
            {
                this.rdbProg.IsEnabled = false;
                this.rdbPromo.IsEnabled = false;
                this.btnPROG.IsEnabled = false;
                this.btnEPISODE.IsEnabled = false;
                this.cbArcType.IsEnabled = false;
                //this.AudioTrackSetControl.IsEnabled = false;

                this.gdPROG_M.RowDefinitions[3].Height = new GridLength(0);
                this.gdPROG_M.RowDefinitions[4].Height = new GridLength(0);
                this.gdPROG_M.RowDefinitions[5].Height = new GridLength(0);
                this.btnSeg.Visibility = System.Windows.Visibility.Collapsed;
                this.btnDel.Visibility = System.Windows.Visibility.Collapsed;
                this.btnCopy.Visibility = System.Windows.Visibility.Collapsed;
                this.btnViewSeg.Visibility = System.Windows.Visibility.Collapsed;
            }
            else if (Bro_ActionType == ActionType.View && form_Type != FormType.Post_Production)
            {
                if (Brocast.FSBRO_TYPE == "P")
                {
                    this.gdPROG_M.RowDefinitions[2].Height = new GridLength(0);
                    this.gdPROG_M.RowDefinitions[6].Height = new GridLength(0);
                }
                this.gdPROG_M.RowDefinitions[3].Height = new GridLength(0);
                this.gdPROG_M.RowDefinitions[4].Height = new GridLength(0);
                this.gdPROG_M.RowDefinitions[5].Height = new GridLength(0);
                this.gdPROG_M.RowDefinitions[6].Height = new GridLength(0);
                this.gdPROG_M.RowDefinitions[7].Height = new GridLength(0);
                this.gdPROG_M.RowDefinitions[18].Height = new GridLength(0);
                this.gdPROG_M.RowDefinitions[19].Height = new GridLength();//Auto的意思
                this.spSegControl.Visibility = System.Windows.Visibility.Collapsed;

                LockView_All();
            }
            else if (form_Type == FormType.Post_Production)
            {
                this.gdPROG_M.RowDefinitions[3].Height = new GridLength(0);
                this.gdPROG_M.RowDefinitions[4].Height = new GridLength(0);
                this.gdPROG_M.RowDefinitions[5].Height = new GridLength(0);
                this.gdPROG_M.RowDefinitions[18].Height = new GridLength(0);
                this.gdPROG_M.RowDefinitions[20].Height = new GridLength();//Auto的意思
                this.gdPROG_M.RowDefinitions[21].Height = new GridLength();//Auto的意思

                this.btnSeg.Visibility = System.Windows.Visibility.Collapsed;
                this.btnDel.Visibility = System.Windows.Visibility.Collapsed;
                this.btnCopy.Visibility = System.Windows.Visibility.Collapsed;

                if (Brocast.FCCHECK_STATUS == "R")//退單
                {
                    this.btnUpload.IsEnabled = false;
                }

                if (Brocast.FCCHECK_STATUS != "N")
                {
                    this.btnEdit.IsEnabled = false;
                }
                LockView_All();
            }
            else if (Bro_ActionType == ActionType.Copy)
            {
                Brocast.FCCHECK_STATUS = "G";
                Brocast.FCCHECK_STATUS_NAME = "";
                this.btnSeg.Visibility = System.Windows.Visibility.Collapsed;
                this.btnDel.Visibility = System.Windows.Visibility.Collapsed;
                this.btnCopy.Visibility = System.Windows.Visibility.Collapsed;
                this.rdbProg.IsEnabled = false;
                this.rdbPromo.IsEnabled = false;
            }

            this.gdPROG_M.DataContext = Brocast;
        }

        void Brocast_Client_UPDATE_TBLOG_VIDEO_TRACKCompleted(object sender, UPDATE_TBLOG_VIDEO_TRACKCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {
                    MessageBox.Show("送播單修改成功");
                    this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("修改送播單時發生錯誤");
                }

            }
        }

        void Brocast_Client_CallREPORT_BRO_PROMOCompleted(object sender, CallREPORT_BRO_PROMOCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    if (e.Result.ToString().StartsWith("錯誤"))
                        MessageBox.Show(e.Result.ToString(), "提示訊息", MessageBoxButton.OK);
                    else
                        HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + e.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,resizable=yes");
                }
                else
                    MessageBox.Show("查無相關資料，或該筆資料已不存在資料庫中。", "提示訊息", MessageBoxButton.OK);
            };
        }

        void Brocast_Client_QueryVideoID_PROG_OnceCompleted(object sender, QueryVideoID_PROG_OnceCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Error == null)
                {
                    if (e.Result.Trim() != "")
                    {
                        if (form_Type == FormType.Displacement && Bro_ActionType != ActionType.Copy && seg_ActionType != ActionType.Copy)
                        {
                            Class_LOG_VIDEO logVideo = (Class_LOG_VIDEO)DGLogList.SelectedItem;

                            STT100_10_01 _STT100_10_01 = new STT100_10_01(logVideo.FSARC_TYPE_NAME, Arc_type_ID, Brocast.FSBRO_TYPE, Brocast.FSID, Brocast.FNEPISODE.ToString(), Brocast.FSCHANNELID, seg_ActionType == ActionType.Edit ? logVideo.FSFILE_NO.Trim() : "", Brocast.FSBRO_ID, Brocast.FSID_NAME, Brocast.FNEPISODE_NAME, Brocast.FCCHANGE == "Y" ? logVideo.FSFILE_NO : "", Bro_ActionType == ActionType.Copy, e.Result, new Class_LOG_VIDEO(), trackTemp == "" ? "MMMMMMMM" : trackTemp);
                            _STT100_10_01.Show();
                            _STT100_10_01.Closed += (s, args) =>
                            {
                                if (_STT100_10_01.DialogResult == true)
                                {
                                    _STT100_10_01.myCLV.FCFILE_STATUS_NAME = Translate_Status_Name.TransStatusName_Video_HDMC(_STT100_10_01.myCLV.FCFILE_STATUS, "");

                                    VideoList.Add(_STT100_10_01.myCLV);
                                    this.DGLogList.ItemsSource = null;
                                    this.DGLogList.ItemsSource = VideoList;
                                }
                            };
                        }
                        else if (form_Type == FormType.Displacement && seg_ActionType == ActionType.Copy)
                        {
                            Log_video.FSVIDEO_PROG = e.Result;
                            Brocast_Client.fnGetFileIDAsync(Brocast.FSBRO_TYPE, Brocast.FSID, Brocast.FNEPISODE.ToString(), UserClass.userData.FSUSER_ID.ToString(), Brocast.FSCHANNELID, Brocast.FSID_NAME, Brocast.FNEPISODE_NAME);

                        }
                        else
                        {
                            Log_video.FSVIDEO_PROG = e.Result;
                            foreach (Class_LOG_VIDEO video in VideoList)
                            {
                                video.FSVIDEO_PROG = e.Result;
                                foreach (Class_LOG_VIDEO_SEG seg in video.VideoListSeg)
                                {
                                    seg.FSVIDEO_ID = video.FSVIDEO_PROG + String.Format("{0:00}", Convert.ToInt32(seg.FNSEG_ID));
                                }
                            }

                            if (form_Type == FormType.Normal && Bro_ActionType == ActionType.Copy)
                            {

                                Brocast_Client.fnGetFileIDAsync(Brocast.FSBRO_TYPE, Brocast.FSID, Brocast.FNEPISODE.ToString(), UserClass.userData.FSUSER_ID, Brocast.FSCHANNELID, Brocast.FSID_NAME, Brocast.FNEPISODE_NAME);
                            }
                        }
                        //if (bro_ActionType == ActionType.Copy || Bro_Type == STBType.Displacement)
                        //{
                        //    Brocast_Client.fnGetFileIDAsync(Brocast.FSBRO_TYPE, Brocast.FSID, Brocast.FNEPISODE.ToString(), UserClass.userData.FSUSER_ID.ToString(), Brocast.FSCHANNELID, Brocast.FSID_NAME, Brocast.FNEPISODE_NAME);
                        //}
                        //else
                        //{
                        if (Bro_ActionType != ActionType.Copy && form_Type != FormType.Displacement)
                            Load_Video_Seg();

                        // }
                    }
                }
                else
                {
                    MessageBox.Show("取得新的VideoID時發生錯誤", "提示", MessageBoxButton.OK);
                }

            }
        }

        void Brocast_Client_Set_Class_LOG_VIDEO_AND_SEGCompleted(object sender, Set_Class_LOG_VIDEO_AND_SEGCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                Brocast.FNEPISODE = null;
                Brocast.FNEPISODE_NAME = "";
                VideoList = e.Result;
                this.DGLogList.ItemsSource = null;
                this.DGLogList.ItemsSource = VideoList;
                this.DGLogList.SelectedIndex = 0;
            }
        }

        void Brocast_Client_Insert_VideoID_MapCompleted(object sender, Insert_VideoID_MapCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                MessageBox.Show("已完成標記");
                this.DialogResult = true;
            }
        }

        void Brocast_Client_Set_Class_LOG_VIDEO_AND_SEG_NewVideoIDCompleted(object sender, Set_Class_LOG_VIDEO_AND_SEG_NewVideoIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                Brocast.FNEPISODE = null;
                Brocast.FNEPISODE_NAME = "";
                VideoList = e.Result;
                this.DGLogList.ItemsSource = null;
                this.DGLogList.ItemsSource = VideoList;
                this.DGLogList.SelectedIndex = 0;
            }
        }

        void Brocast_Client_UPDATE_TBLOG_VIDEO_REVIEWCompleted(object sender, UPDATE_TBLOG_VIDEO_REVIEWCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {
                    hdMC_CLient.Add_HD_MCAsync(Brocast.FSBRO_ID, VideoList[0].FSFILE_NO, VideoList[0].FSVIDEO_PROG, UserClass.userData.FSUSER_ID);
                    return;
                }
            }

            MessageBox.Show("新增送播" + (form_Type == FormType.Displacement ? "置換" : "") + "單時發生錯誤!");
        }



        void promoClient_GetTBPGM_PROMO_BYPROMONAMECompleted(object sender, WSPROMO.GetTBPGM_PROMO_BYPROMONAMECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result.Count > 0)
                {
                    WSPROMO.Class_PROMO promoData = e.Result[0];

                    checkPromo(promoData.FSPROMO_ID, promoData.FSPROMO_NAME, Brocast.FSCHANNELID, promoData.FNDEP_ID.ToString(), "");

                }
            }
        }

        void episodeClient_GetProg_DCompleted(object sender, WSPROG_D.GetProg_DCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result.Count > 0)
                {
                    PTS_MAM3.WSPROG_D.Class_PROGD episodeData = e.Result[0];
                    Brocast.FNEPISODE = episodeData.FNEPISODE;
                    Brocast.SHOW_FNEPISODE = episodeData.SHOW_FNEPISODE;
                    tbxEPISODE.Text = episodeData.SHOW_FNEPISODE;
                    lblEPISODE_NAME.Content = episodeData.FNEPISODE_NAME;

                    //載入節目資料
                    checkProg(programData.FSPROG_ID, programData.FSPGMNAME, programData.FSCHANNEL_ID, programData.FNDEP_ID.ToString(), "", programData.FNTOTEPISODE.ToString(), programData.FSCHANNEL, programData.FNLENGTH.ToString());


                    Brocast_Client.GetPROG_PLAY_NOTEAsync(Brocast.FSID, Brocast.SHOW_FNEPISODE);
                }
            }
        }

        WSPROG_M.Class_PROGM programData;
        void programClient_fnGetProg_MCompleted(object sender, WSPROG_M.fnGetProg_MCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result.Count > 0)
                {
                    programData = e.Result[0];
                    Brocast.FSID = programData.FSPROG_ID;
                    Brocast.FSID_NAME = programData.FSPGMNAME;
                    Brocast.FSCHANNELID = programData.FSCHANNEL_ID;

                    checkProg(programData.FSPROG_ID, programData.FSPGMNAME, programData.FSCHANNEL_ID, programData.FNDEP_ID.ToString(), "", programData.FNTOTEPISODE.ToString(), programData.FSCHANNEL, programData.FNLENGTH.ToString());

                    if (Brocast.FNEPISODE != null)
                        episodeClient.GetProg_DAsync(programData.FSPROG_ID, Brocast.FNEPISODE.ToString());
                }
            }
        }

        void STB100_01_Loaded(object sender, RoutedEventArgs e)
        {
            if (form_Type == FormType.Displacement)
            {
                this.DGLogList.Columns[this.DGLogList.Columns.Count - 1].Visibility = System.Windows.Visibility.Visible;
            }
        }

        void hdMC_CLient_Add_HD_MCCompleted(object sender, WSHD_MC.Add_HD_MCCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {
                    string message = "";
                    if (form_Type != FormType.Post_Production)
                        message = "送播單新增成功!";
                    else if (form_Type == FormType.Post_Production)
                    {
                        if (UpdatedStatus == "R")
                        {
                            message = "退單成功";
                        }
                        else if (UpdatedStatus == "U")
                        {

                            Brocast_Client.Insert_VideoID_MapAsync(Brocast.FSID, Brocast.FNEPISODE.ToString(), Arc_type_ID, VideoList[0].FSVIDEO_PROG);
                            return;
                            //message = "已完成標記";
                        }
                    }
                    MessageBox.Show(message);
                    this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("新增送播單時發生錯誤!");
                }
            }
        }

        void Brocast_Client_Query_CheckIsAbleToCreateNewTransformFormCompleted(object sender, Query_CheckIsAbleToCreateNewTransformFormCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == "True")
                {
                    STB100_01 copyBro = new STB100_01(Brocast, ActionType.Copy, form_Type);
                    copyBro.Closed += (s, args) =>
                    {
                        this.DialogResult = copyBro.DialogResult;
                    };
                    copyBro.Show();
                    return;
                }
                else
                {
                    MessageBox.Show(e.Result);

                }
            }
            BusyMsg.IsBusy = false;
        }

        void Brocast_Client_Delete_BROADCAST_AT_ONCECompleted(object sender, Delete_BROADCAST_AT_ONCECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                    this.DialogResult = false;
                else
                {
                    //刪除失敗看要怎麼處理。
                }
            }
        }

        void Brocast_Client_DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONECompleted(object sender, DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {
                    MessageBox.Show("成功刪除段落檔");
                    LoadTBLOG_VIDEO();
                }
                else
                {
                    MessageBox.Show("刪除段落檔時發生錯誤!");
                }
            }
        }

        void Brocast_Client_INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLISTCompleted(object sender, INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLISTCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {
                    Brocast_Client.UPDATE_TBLOG_VIDEO_REVIEWAsync(Brocast.FSBRO_ID, UserClass.userData.FSUSER_ID);
                    //Brocast_Client.UPDATE_TBLOG_VIDEO_FILE_STATUSAsync(VideoList[0].FSCHANGE_FILE_NO, "D", UserClass.userData.FSUSER_ID);

                }
                else
                {
                    MessageBox.Show("新增送播" + (form_Type == FormType.Displacement ? "置換" : "") + "單時發生錯誤!");
                }
            }
        }



        string UpdatedStatus = "";//紀錄Update的狀態，給完成事件用
        void Brocast_Client_GetTBBROADCAST_BYBROIDCompleted(object sender, GetTBBROADCAST_BYBROIDCompletedEventArgs e)
        {
            if (isRejectOrUpload != null)
            {
                Brocast = e.Result.FirstOrDefault();
                if (Brocast.FCCHECK_STATUS != "N")
                {
                    if (isRejectOrUpload == true)
                    {
                        MessageBox.Show("狀態為「" + Brocast.FCCHECK_STATUS_NAME + "」的單子無法退單!");
                        return;
                    }
                    else
                    {
                        if (Brocast.HD_MC_Status != "E")
                        {
                            MessageBox.Show("狀態為「" + Brocast.FCCHECK_STATUS_NAME + "」的單子無法標記為已上傳!");
                            return;
                        }
                    }

                }

                if (isRejectOrUpload == true)
                {
                    if (MessageBox.Show("確定退單?", "提示訊息", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                    {
                        UpdatedStatus = "R";
                        Brocast_Client.UPDATE_TBBROADCAST_StatusAsync(Brocast.FSBRO_ID, UpdatedStatus, UserClass.userData.FSUSER_ID);
                    }
                }
                else if (isRejectOrUpload == false)
                {
                   
                    string MessageStr = Brocast.HD_MC_Status == "E" ? "重新" : "";
                    MessageStr += "標記此筆資料已開始上傳?";
                    if (MessageBox.Show(MessageStr, "提示訊息", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                    {
                        //2018.01.18標記上傳pass BenWu寫的傳檔程式，直接請USER將檔案放置MkUpload後狀態強制直接改為M(標記上傳成功)

                        UpdatedStatus = "M";
                       
                        Brocast_Client.UPDATE_TBBROADCAST_StatusAsync(Brocast.FSBRO_ID, UpdatedStatus, UserClass.userData.FSUSER_ID);
                                               

                    }
                }
            }
        }

        void Brocast_Client_UPDATE_TBBROADCAST_StausCompleted(object sender, UPDATE_TBBROADCAST_StatusCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {
                    if (form_Type == FormType.Post_Production && UpdatedStatus == "U")
                    {
                        hdMC_CLient.Add_HD_MCAsync(Brocast.FSBRO_ID, VideoList[0].FSFILE_NO, VideoList[0].FSVIDEO_PROG, UserClass.userData.FSUSER_ID);
                    }
                    else
                    {
                        string message = "";
                        if (form_Type == FormType.Post_Production)
                        {
                            if (UpdatedStatus == "R")
                            {
                                message = "退單成功";
                                Brocast.FCCHECK_STATUS = UpdatedStatus;
                                Brocast_Client.PrepareSendBroadcastNotifyAsync(Brocast, VideoList[0]);
                            }
                            else if (UpdatedStatus == "U")
                            {
                                message = "已完成標記";
                            }
                        }
                        else
                        {
                            if (Bro_ActionType == ActionType.Edit)
                            {
                                Brocast_Client.UPDATE_TBLOG_VIDEO_TRACKAsync(VideoList[0].FSFILE_NO, VideoList[0].FSTRACK, UserClass.userData.FSUSER_ID);
                                return;
                                //message = "送播單修改成功";
                            }
                        }


                        MessageBox.Show(message);
                        this.DialogResult = true;
                    }
                }
                else
                {
                    MessageBox.Show("修改送播單狀態時發生錯誤!");
                }
            }
        }

        void LockView_All()
        {
            foreach (UIElement ui in this.gdPROG_M.Children)
            {
                if (ui is TextBox)
                {
                    ((TextBox)ui).IsReadOnly = true;
                }
                else if (ui is Button)
                {
                    ((Button)ui).IsEnabled = false;
                }
                else if (ui is StackPanel)
                {
                    foreach (UIElement ui2 in ((StackPanel)ui).Children)
                    {
                        if (ui2 is RadioButton)
                        {
                            ((RadioButton)ui2).IsEnabled = false;
                        }
                    }
                }
                else if (ui is ComboBox)
                {
                    ((ComboBox)ui).IsEnabled = false;
                }
            }

            AudioTrackSetControl.DiableControl();
        }

        void Brocast_Client_CallREPORT_BRO_DetailCompleted(object sender, CallREPORT_BRO_DetailCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    if (e.Result.ToString().StartsWith("錯誤"))
                        MessageBox.Show(e.Result.ToString(), "提示訊息", MessageBoxButton.OK);
                    else
                        System.Windows.Browser.HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + e.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,resizable=yes");
                }
                else
                    MessageBox.Show("查無相關資料，或該筆資料已不存在資料庫中。", "提示訊息", MessageBoxButton.OK);
            };
        }

        void Brocast_Client_CallReport_Archive_BigCompleted(object sender, CallReport_Archive_BigCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    if (e.Result.ToString().StartsWith("錯誤"))
                        MessageBox.Show(e.Result.ToString(), "提示訊息", MessageBoxButton.OK);
                    else
                        System.Windows.Browser.HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + e.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,resizable=yes");
                }
                else
                    MessageBox.Show("查無相關資料，或該筆資料已不存在資料庫中。", "提示訊息", MessageBoxButton.OK);
            };
        }

        void Brocast_Client_CallReport_Archive_SmallCompleted(object sender, CallReport_Archive_SmallCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    if (e.Result.ToString().StartsWith("錯誤"))
                        MessageBox.Show(e.Result.ToString(), "提示訊息", MessageBoxButton.OK);
                    else
                        System.Windows.Browser.HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + e.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,resizable=yes");
                }
                else
                    MessageBox.Show("查無相關資料，或該筆資料已不存在資料庫中。", "提示訊息", MessageBoxButton.OK);
            };
        }

        void Brocast_Client_CallReport_Archive_StickCompleted(object sender, CallReport_Archive_StickCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    if (e.Result.ToString().StartsWith("錯誤"))
                        MessageBox.Show(e.Result.ToString(), "提示訊息", MessageBoxButton.OK);
                    else
                        System.Windows.Browser.HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + e.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,resizable=yes");
                }
                else
                    MessageBox.Show("查無相關資料，或該筆資料已不存在資料庫中。", "提示訊息", MessageBoxButton.OK);
            };
        }

        void Brocast_Client_INSERT_BROADCAST_WITHOUT_FLOWCompleted(object sender, INSERT_BROADCAST_WITHOUT_FLOWCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {
                    //if (bro_ActionType == ActionType.Copy)
                    //{
                    //    //Brocast_Client.INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLISTAsync(Brocast, VideoList);
                    //    //Brocast_Client.INSERT_LOG_VIDEOAsync(VideoList[0]);
                    //}
                    //else
                    //{
                    //if (form_Type == FormType.Displacement)
                    //{
                    //    var sdsf = VideoList;
                    //    //Brocast_Client.UPDATE_TBLOG_VIDEO_AND_D_WHEN_EXCHANGE_FILEAsync(
                    //}
                    //else
                    //{
                    hdMC_CLient.Add_HD_MCAsync(Brocast.FSBRO_ID, VideoList[0].FSFILE_NO, VideoList[0].FSVIDEO_PROG, UserClass.userData.FSUSER_ID);
                    //}
                    //}
                }
                else
                {
                    MessageBox.Show("新增送播單時發生錯誤!");
                }
            }
        }

        void Brocast_Client_GetTBLOG_VIDEO_BY_Type_EPISODECompleted(object sender, GetTBLOG_VIDEO_BY_Type_EPISODECompletedEventArgs e)
        {
            Brocast.FNEPISODE = Brocast.FNEPISODE == null ? 0 : Brocast.FNEPISODE;

            if (Brocast.FCCHANGE == "Y")
            {
                if (e.Result.Count == 0)
                {
                    MessageBox.Show(Brocast.FSID_NAME + " 第" + Brocast.FNEPISODE.ToString() + "集，沒有可置換的檔案");

                }
                else if (Bro_ActionType == ActionType.Copy)
                {
                    if (form_Type == FormType.Displacement)
                    {
                        var queryResult = e.Result.Where(a => a.FSARC_TYPE == Arc_type_ID && a.FSID == Brocast.FSID && a.FNEPISODE == Brocast.FNEPISODE && a.Brocast_Status == "R" && a.FCFILE_STATUS == "X");

                        if (queryResult.Count() > 0)
                        {
                            MessageBox.Show("無法新增第二筆" + Brocast.FSID_NAME + " 第" + Brocast.FNEPISODE.ToString() + "集的" + (Arc_type_ID == "001" ? "SD送播帶" : "HD送播帶") + "。請修改已被後製退回之表單，表單編號:" + queryResult.FirstOrDefault().FSBRO_ID);
                            BusyMsg.IsBusy = false;
                            return;
                        }
                        else if (e.Result.Where(a => a.FSARC_TYPE == Arc_type_ID && a.FSID == Brocast.FSID && a.FNEPISODE == Brocast.FNEPISODE && a.FCFILE_STATUS != "X" && a.FCFILE_STATUS != "D").Count() > 0)
                        {
                            MessageBox.Show("無法新增第二筆" + Brocast.FSID_NAME + " 第" + Brocast.FNEPISODE.ToString() + "集的" + (Arc_type_ID == "001" ? "SD送播帶。" : "HD送播帶。"));
                            BusyMsg.IsBusy = false;
                            return;
                        }
                    }
                    else
                    {
                        Brocast_Client.QueryVideoID_PROG_OnceAsync(Brocast.FSID, Brocast.FNEPISODE.ToString(), Arc_type_ID);
                        // Brocast_Client.QueryVideoID_PROG_STTAsync(Brocast.FSID, Brocast.FNEPISODE.ToString(), Arc_type_ID, true);
                    }
                }

            }
            else
            {
                if (Bro_ActionType == ActionType.Create && seg_ActionType == ActionType.Create)
                {
                    var queryResult = e.Result.Where(a => a.FSARC_TYPE == Arc_type_ID && a.FSID == Brocast.FSID && a.FNEPISODE == Brocast.FNEPISODE && a.Brocast_Status == "R" && a.FCFILE_STATUS == "X");

                    if (queryResult.Count() > 0)
                    {
                        MessageBox.Show("無法新增第二筆" + Brocast.FSID_NAME + " 第" + Brocast.FNEPISODE.ToString() + "集的" + (Arc_type_ID == "001" ? "SD送播帶" : "HD送播帶") + "。請修改已被後製退回之表單，表單編號:" + queryResult.FirstOrDefault().FSBRO_ID);
                        BusyMsg.IsBusy = false;
                        return;
                    }
                    else if (e.Result.Where(a => a.FSARC_TYPE == Arc_type_ID && a.FSID == Brocast.FSID && a.FNEPISODE == Brocast.FNEPISODE && a.FCFILE_STATUS != "X" && a.FCFILE_STATUS != "D").Count() > 0)
                    {
                        MessageBox.Show("無法新增第二筆" + Brocast.FSID_NAME + " 第" + Brocast.FNEPISODE.ToString() + "集的" + (Arc_type_ID == "001" ? "SD送播帶。" : "HD送播帶。"));
                        BusyMsg.IsBusy = false;
                        return;
                    }

                    //取得新的VideoID false
                    Brocast_Client.QueryVideoID_PROG_STTAsync(Brocast.FSID, Brocast.FNEPISODE.ToString(), Arc_type_ID, true);
                }
                else if (Bro_ActionType == ActionType.Copy)
                {
                    if (seg_ActionType != ActionType.Delete)
                    {
                        var queryResult = e.Result.Where(a => a.FSARC_TYPE == Arc_type_ID && a.FSID == Brocast.FSID && a.FNEPISODE == Brocast.FNEPISODE && a.Brocast_Status == "R" && a.FCFILE_STATUS == "X");

                        if (queryResult.Count() > 0)
                        {
                            MessageBox.Show("無法新增第二筆" + Brocast.FSID_NAME + " 第" + Brocast.FNEPISODE.ToString() + "集的" + (Arc_type_ID == "001" ? "SD送播帶" : "HD送播帶") + "。請修改已被後製退回之表單，表單編號:" + queryResult.FirstOrDefault().FSBRO_ID);
                            BusyMsg.IsBusy = false;
                            return;
                        }
                        else if (e.Result.Where(a => a.FSARC_TYPE == Arc_type_ID && a.FSID == Brocast.FSID && a.FNEPISODE == Brocast.FNEPISODE && a.FCFILE_STATUS != "X" && a.FCFILE_STATUS != "D").Count() > 0)
                        {
                            MessageBox.Show("無法新增第二筆" + Brocast.FSID_NAME + " 第" + Brocast.FNEPISODE.ToString() + "集的" + (Arc_type_ID == "001" ? "SD送播帶" : "HD送播帶"));
                            BusyMsg.IsBusy = false;
                            return;
                        }
                        else
                        {
                            Brocast_Client.INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLISTAsync(FormToClass_Bro(), VideoList);
                            return;
                        }
                    }
                    //取得新的VideoID
                    //Brocast_Client.QueryVideoID_PROG_STTAsync(Brocast.FSID, Brocast.FNEPISODE.ToString(), Arc_type_ID, false);
                    //}

                    //else
                    //{
                    //    //取得新的VideoID
                    //    Brocast_Client.QueryVideoID_PROG_STTAsync(Brocast.FSID, Brocast.FNEPISODE.ToString(), Arc_type_ID, false);
                    //}
                }
            }

            if (Bro_ActionType != ActionType.Create)
            {
                VideoList = e.Result;
                foreach (Class_LOG_VIDEO logvideo in VideoList)
                {
                    logvideo.FCFILE_STATUS_NAME = Translate_Status_Name.TransStatusName_Video_HDMC(logvideo.FCFILE_STATUS, Brocast.HD_MC_Status);
                }

                //增加給USER複製的資訊  2016-06-15 by Jarvis
                if (Bro_ActionType != ActionType.Edit && VideoList.Count > 0)
                {
                    Brocast.FSMEMO = Brocast.FSMEMO.Insert(0, Brocast.FSBRO_ID + "_" + Brocast.FSID_NAME.Substring(4, Brocast.FSID_NAME.Length - 4) + "_" + Brocast.FNEPISODE.ToString() + "_" + VideoList[0].FSVIDEO_PROG.Substring(2, 6) + ".mxf" + Environment.NewLine + "---------------------------------------------------------" + Environment.NewLine);
                }
            }

            if (form_Type == FormType.Post_Production)
            {
                if (Brocast.FCCHECK_STATUS != "N")
                {
                    if (Brocast.FCCHECK_STATUS == "U" || Brocast.FCCHECK_STATUS == "E")
                    {
                        this.btnUpload.IsEnabled = false;
                    }
                    this.btnReject.IsEnabled = false;
                }

                if (Brocast.FCCHECK_STATUS == "U" && !string.IsNullOrEmpty(Brocast.HD_MC_Status) && Brocast.HD_MC_Status == "E")
                {
                    this.btnUpload.IsEnabled = true;
                    this.btnUpload.Content = "重新標記已上傳";
                }
                else if (!string.IsNullOrEmpty(Brocast.HD_MC_Status))
                {
                    this.btnUpload.IsEnabled = false;
                }

                if (VideoList.Where(a => a.FCFILE_STATUS != "B").Count() > 0)
                {
                    this.btnUpload.IsEnabled = false;
                    this.btnReject.IsEnabled = false;
                }
            }

            //if (bro_ActionType == ActionType.Copy)
            //{
            //    foreach (Class_LOG_VIDEO video in VideoList)
            //    {
            //        video.FCFILE_STATUS = "B";
            //        video.FCFILE_STATUS_NAME = "待轉檔";
            //        video.FSFILE_NO = "";
            //        video.FSVIDEO_PROG = "";
            //    }
            //    //取得新的VideoID
            //    //Brocast_Client.QueryVideoID_PROG_STTAsync(Brocast.FSID, Brocast.FNEPISODE.ToString(), Arc_type_ID, false);
            //}

            if (Bro_ActionType == ActionType.Create && form_Type != FormType.Displacement)
            {
                VideoList = e.Result.Where(a => a.FSBRO_ID == Brocast.FSBRO_ID).ToList();
            }
            else
            {
                VideoList = e.Result;
            }

            this.DGLogList.ItemsSource = null;
            this.DGLogList.ItemsSource = VideoList;
            this.DGLogList.SelectedIndex = 0;

            BusyMsg.IsBusy = false;
        }



        void Brocast_Client_GetPROG_PLAY_NOTECompleted(object sender, GetPROG_PLAY_NOTECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.FBSW == true)
                    MessageBox.Show("播出日期:" + e.Result.FDDATE + " 時間:" + e.Result.FSPLAYTIME + " 僅剩:" + e.Result.FSNOTE, "提示訊息", MessageBoxButton.OK);
            }

            //置換才要先查詢LogVideo
            if (Brocast.FCCHANGE == "Y")
                LoadTBLOG_VIDEO();
        }

        void Brocast_Client_GetPROMO_PLAY_NOTECompleted(object sender, GetPROMO_PLAY_NOTECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.FBSW == true)
                    MessageBox.Show("播出日期:" + e.Result.FDDATE + " 時間:" + e.Result.FSPLAYTIME + " 僅剩:" + e.Result.FSNOTE, "提示訊息", MessageBoxButton.OK);
            }

            //if (Bro_ActionType == ActionType.Copy && Brocast.FSBRO_TYPE == "P")
            //{
            //    Brocast_Client.QueryVideoID_PROG_STTAsync(Brocast.FSID, Brocast.FNEPISODE.ToString(), Arc_type_ID, true);
            //}

            //置換才要先查詢LogVideo
            if (Brocast.FCCHANGE == "Y")
                LoadTBLOG_VIDEO();
        }

        void Brocast_Client_fnGetTBBROADCAST_CODECompleted(object sender, fnGetTBBROADCAST_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            case "TBZCHANNEL":       //頻道別代碼
                                CheckBox cbCHANNEL = new CheckBox();
                                cbCHANNEL.Content = CodeData.NAME;
                                cbCHANNEL.Tag = CodeData.ID;
                                cbCHANNEL.IsEnabled = false;
                                listCHANNEL.Items.Add(cbCHANNEL);
                                break;

                            default:
                                break;
                        }
                    }
                }
            }
        }

        void MAM_Client_GetNoRecordCompleted(object sender, WSMAMFunctions.GetNoRecordCompletedEventArgs e)
        {
            oldBroadcast_ID = Brocast.FSBRO_ID;
            Brocast.FSBRO_ID = e.Result;

            if (Bro_ActionType == ActionType.Copy && form_Type != FormType.Post_Production)
            {
                List<Class_LOG_VIDEO> tempVideoList = new List<Class_LOG_VIDEO>();
                foreach (Class_LOG_VIDEO video in VideoList)
                {
                    video.FSBRO_ID = Brocast.FSBRO_ID;

                    Class_LOG_VIDEO newLogVideo = new Class_LOG_VIDEO();
                    PropertyInfo[] propArr = video.GetType().GetProperties();
                    foreach (PropertyInfo pi in propArr)
                    {
                        pi.SetValue(newLogVideo, pi.GetValue(video, null), null);
                    }

                    tempVideoList.Add(newLogVideo);
                }

                VideoList.Clear();
                VideoList = tempVideoList;
                this.DGLogList.ItemsSource = null;
                this.DGLogList.ItemsSource = VideoList;
                //Brocast_Client.Set_Class_LOG_VIDEO_AND_SEGAsync(Brocast.FSBRO_TYPE, Brocast.FSID, Brocast.FNEPISODE.ToString(), Brocast.FSUPDATED_BY, Brocast.FSCHANNELID, Brocast.FSID_NAME, Brocast.FNEPISODE_NAME, Brocast.FSBRO_ID, VideoList);
                //Brocast_Client.Set_Class_LOG_VIDEO_AND_SEGAsync(Brocast.FSBRO_TYPE, Brocast.FSID, Brocast.FNEPISODE.ToString(), Brocast.FSUPDATED_BY, Brocast.FSCHANNELID, Brocast.FSID_NAME, Brocast.FNEPISODE_NAME, Brocast.FSBRO_ID, VideoList);
            }
        }



        private Class_BROADCAST FormToClass_Bro()
        {
            Brocast.FDBRO_DATE = DateTime.Now;                                          //送帶轉檔申請日期
            Brocast.FSBRO_BY = UserClass.userData.FSUSER_ID.ToString();                 //送帶轉檔申請者

            Brocast.FCCHECK_STATUS = "N";
            Brocast.FSSIGNED_BY = UserClass.userData.FSUSER_ID;

            Brocast.FSCHECK_BY = "";                                                    //審核者
            Brocast.FSCREATED_BY = UserClass.userData.FSUSER_ID.ToString();//建檔者
            Brocast.FSUPDATED_BY = UserClass.userData.FSUSER_ID.ToString();//修改者                     

            if (Bro_ActionType == ActionType.Copy || Bro_ActionType == ActionType.Edit)
            {
                foreach (Class_LOG_VIDEO video in VideoList)
                {
                    video.FSTRACK = this.AudioTrackSetControl.GetAudioTrackSetting();
                }
            }
            return Brocast;
        }


        private void btnPROG_Click(object sender, RoutedEventArgs e)
        {
            if (rdbProg.IsChecked == true)
            {
                //PRG100_01
                PROGDATA_VIEW PROGDATA_VIEW_frm = new PROGDATA_VIEW();


                PROGDATA_VIEW_frm.Closed += (s, args) =>
                {
                    if (PROGDATA_VIEW_frm.DialogResult == true)
                    {
                        //檢查是否要作資料認定
                        if (PROGDATA_VIEW_frm.strChannel_Id.Trim() == "" || PROGDATA_VIEW_frm.strDEP_ID.Trim() == "0")
                        {
                            checkProgM_Channel_Dep(PROGDATA_VIEW_frm.strProgID_View.Trim(), PROGDATA_VIEW_frm.strProgName_View.Trim(), PROGDATA_VIEW_frm.strChannel_Id.Trim(), PROGDATA_VIEW_frm.strDEP_ID.Trim(), PROGDATA_VIEW_frm.strProducer.Trim(), PROGDATA_VIEW_frm.strTOTLEEPISODE.Trim(), PROGDATA_VIEW_frm.strCHANNEL.Trim(), PROGDATA_VIEW_frm.strFNLENGTH.Trim());
                            return;
                        }

                        //載入節目資料
                        checkProg(PROGDATA_VIEW_frm.strProgID_View.Trim(), PROGDATA_VIEW_frm.strProgName_View.Trim(), PROGDATA_VIEW_frm.strChannel_Id.Trim(), PROGDATA_VIEW_frm.strDEP_ID.Trim(), PROGDATA_VIEW_frm.strProducer.Trim(), PROGDATA_VIEW_frm.strTOTLEEPISODE.Trim(), PROGDATA_VIEW_frm.strCHANNEL.Trim(), PROGDATA_VIEW_frm.strFNLENGTH.Trim());

                    }
                };

                PROGDATA_VIEW_frm.Show();
            }
            else if (rdbPromo.IsChecked == true)
            {
                //PRG800_07
                PRG800_07 PROMO_VIEW_frm = new PRG800_07();
                PROMO_VIEW_frm.Show();

                PROMO_VIEW_frm.Closed += (s, args) =>
                {
                    if (PROMO_VIEW_frm.DialogResult == true)
                    {
                        //檢查是否要作資料認定
                        if (PROMO_VIEW_frm.strChannel_Id.Trim() == "" || PROMO_VIEW_frm.strDEP_ID.Trim() == "0")
                        {
                            checkProgM_Channel_Dep(PROMO_VIEW_frm.strPromoID_View.Trim(), PROMO_VIEW_frm.strPromoName_View.Trim(), PROMO_VIEW_frm.strChannel_Id.Trim(), PROMO_VIEW_frm.strDEP_ID.Trim(), PROMO_VIEW_frm.strProducer.Trim(), "", "", "");
                            return;
                        }

                        //載入短帶資料
                        checkPromo(PROMO_VIEW_frm.strPromoID_View.Trim(), PROMO_VIEW_frm.strPromoName_View.Trim(), PROMO_VIEW_frm.strChannel_Id.Trim(), PROMO_VIEW_frm.strDEP_ID.Trim(), PROMO_VIEW_frm.strProducer.Trim());

                        if (Bro_ActionType == ActionType.Copy)
                        {
                            Brocast.FNEPISODE = Brocast.FNEPISODE == null ? 0 : Brocast.FNEPISODE;
                            foreach (Class_LOG_VIDEO video in VideoList)
                            {
                                video.FSID = Brocast.FSID;
                                video.FNEPISODE = Brocast.FNEPISODE.Value;
                            }

                            //false
                            Brocast_Client.QueryVideoID_PROG_STTAsync(Brocast.FSID, Brocast.FNEPISODE.ToString(), Arc_type_ID, true);
                        }

                    }
                };
            }
        }

        //檢查是否要作節目資料認定
        private void checkProgM_Channel_Dep(string strProgID, string strProgName, string strChannelID, string strDepID, string strProducer, string strTEpidode, string strPlayCHANNEL, string strLength)
        {
            string strCheck = "";   //判斷基本資料缺少訊息
            //string strType = "";    //類型

            //if (rdbProg.IsChecked == true)
            //    strType = "G";
            //else if (rdbPromo.IsChecked == true)
            //    strType = "P";

            //無頻道別要檔掉     
            if (strChannelID.Trim() == "")
            {
                strCheck = "無頻道別資料";
            }

            //無成案單位要檔掉     
            if (strDepID.Trim() == "0")
            {
                if (strCheck.Trim() == "")
                    strCheck = "無成案單位資料";
                else
                    strCheck = strCheck + "、無成案單位資料";
            }

            if (strCheck.Trim() != "")
            {
                MessageBox.Show(strCheck + "，請先進行節目資料認定", "提示訊息", MessageBoxButton.OK);

                //判斷有無修改「節目資料認定」的權限     
                if (ModuleClass.getModulePermissionByName("01", "節目資料認定") == true)
                {
                    MessageBoxResult resultMsg = MessageBox.Show("確定要修改「" + strProgName.Trim() + "」的頻道別資料嗎?", "提示訊息", MessageBoxButton.OKCancel);
                    if (resultMsg == MessageBoxResult.OK)
                    {
                        PRG100_13 PRG100_13_frm = new PRG100_13(Brocast.FSBRO_TYPE, strProgID, strProgName, strChannelID, strDepID);
                        PRG100_13_frm.Show();

                        PRG100_13_frm.Closed += (s, args) =>
                        {
                            if (rdbProg.IsChecked == true)
                            {
                                //如果資料補齊，即可載入節目資料
                                if (PRG100_13_frm.bolcheck == true)
                                    checkProg(strProgID, strProgName, strChannelID, strDepID, strProducer, strTEpidode, strPlayCHANNEL, strLength);
                            }
                            else if (rdbPromo.IsChecked == true)
                            {
                                //如果資料補齊，即可載入短帶資料
                                if (PRG100_13_frm.bolcheck == true)
                                    checkPromo(strProgID, strProgName, strChannelID, strDepID, strProducer);
                            }
                        };
                    }
                }
            }
        }

        //載入節目資料
        private void checkProg(string strProgID, string strProgName, string strChannelID, string strDepID, string strProducer, string strTEpidode, string strPlayCHANNEL, string strLength)
        {

            Brocast.FSID_NAME = strProgName.Trim();
            Brocast.FSID = strProgID.Trim();
            Brocast.FNEPISODE = 0;
            Brocast.FNEPISODE_NAME = "";
            //tbxPROG_NAME.Text = strProgName;
            //tbxPROG_NAME.Tag = strProgID;

            //m_strProducer = strProducer;   //製作人

            tbxEPISODE.Text = "";
            //lblEPISODE_NAME.Content = "";


            tbxTOTEPISODE.Text = strTEpidode;
            tbxLENGTH.Text = strLength;
            Brocast.FSCHANNELID = strChannelID;

            //m_strCHANNELID = strChannelID;
            //m_strDep_ID = strDepID; //原Code有，但似乎沒用

            Clear_listCHANNEL();                             //清空-可播映頻道
            compareCode_FSCHANNEL(CheckList(strPlayCHANNEL));//可播映頻道

            //透過節目編號去查詢入庫影像檔
            if (Bro_ActionType != ActionType.Copy)
            {
                VideoList.Clear();
                this.DGLogList.DataContext = null;
            }
        }


        private string[] CheckList(string strChannel)
        {
            char[] delimiterChars = { ';' };
            string[] words = strChannel.Split(delimiterChars);
            return words;
        }

        void Clear_listCHANNEL()
        {
            for (int j = 0; j < listCHANNEL.Items.Count; j++)
            {
                CheckBox getcki = (CheckBox)listCHANNEL.Items.ElementAt(j);
                getcki.IsChecked = false;
            }
        }

        //比對代碼檔_可播映頻道
        void compareCode_FSCHANNEL(string[] ListTEST_Send)
        {
            for (int i = 0; i < ListTEST_Send.Length - 1; i++)
            {
                for (int j = 0; j < listCHANNEL.Items.Count; j++)
                {
                    CheckBox getcki = (CheckBox)listCHANNEL.Items.ElementAt(j);
                    if (getcki.Tag.ToString().Trim().Equals(ListTEST_Send[i]))
                    {
                        getcki.IsChecked = true;
                        break;
                    }
                }
            }
        }

        //載入短帶資料
        private void checkPromo(string strPromoID, string strPromoName, string strChannelID, string strDepID, string strProducer)
        {
            Brocast.FSID_NAME = strPromoName;
            Brocast.FSID = strPromoID;
            //tbxPROG_NAME.Text = strPromoName;
            //tbxPROG_NAME.Tag = strPromoID;

            //m_strProducer = strProducer;                         //製作人


            //tbxEPISODE.Text = "";
            //lblEPISODE_NAME.Content = "";
            Brocast.FNEPISODE = 0;
            Brocast.FSCHANNELID = strChannelID;

            Clear_listCHANNEL();                                 //清空-可播映頻道                  
            if (Bro_ActionType != ActionType.Copy)
            {
                VideoList.Clear();
                this.DGLogList.DataContext = null;
            }
            //找48小時有無排播記錄
            Brocast_Client.GetPROMO_PLAY_NOTEAsync(Brocast.FSID);
        }

        private void btnEPISODE_Click(object sender, RoutedEventArgs e)
        {
            if (Brocast.FSID != "")
            {
                PRG.PRG200_07 PRG200_07_frm = new PRG.PRG200_07(Brocast.FSID, Brocast.FSID_NAME);


                PRG200_07_frm.Closed += (s, args) =>
                {
                    if (PRG200_07_frm.DialogResult == true)
                    {
                        Brocast.FNEPISODE = Convert.ToInt16(PRG200_07_frm.m_strEpisode_View);
                        Brocast.FNEPISODE_NAME = PRG200_07_frm.m_strProgDName_View;
                        tbxEPISODE.Text = Brocast.FNEPISODE.ToString();

                        if (Bro_ActionType == ActionType.Copy)
                        {
                            foreach (Class_LOG_VIDEO video in VideoList)
                            {
                                video.FSID = Brocast.FSID;
                                video.FNEPISODE = Brocast.FNEPISODE.Value;
                            }

                            //false
                            Brocast_Client.QueryVideoID_PROG_STTAsync(Brocast.FSID, Brocast.FNEPISODE.ToString(), Arc_type_ID, true);
                        }
                        else
                        {

                            //lblEPISODE_NAME.Content = PRG200_07_frm.m_strProgDName_View;

                            //透過節目編號去查詢入庫影像檔
                            if (form_Type == FormType.Displacement)
                            {
                                VideoList.Clear();
                                this.DGLogList.DataContext = null;

                                Brocast_Client.GetPROG_PLAY_NOTEAsync(brocast.FSID, brocast.FNEPISODE.ToString());
                            }
                        }
                    }
                };

                PRG200_07_frm.Show();
            }
            else
                MessageBox.Show("請先選擇節目！", "提示訊息", MessageBoxButton.OK);
        }

        private void btnSeg_Click(object sender, RoutedEventArgs e)
        {

            Button btn = (Button)sender;

            if (btn.Name == "btnSeg")
            {
                seg_ActionType = ActionType.Create;

                if (form_Type != FormType.Displacement)
                {
                    if (this.VideoList.Count < 1)
                    {
                    }
                    else
                    {
                        MessageBox.Show("一張送播單僅限一個檔案");
                        return;
                    }
                }
            }
            else if (btn.Name == "btnEdit")
            {
                seg_ActionType = ActionType.Edit;
            }
            else if (btn.Name == "btnDel")
            {
                seg_ActionType = ActionType.Delete;
            }
            else if (btn.Name == "btnCopy")
            {
                seg_ActionType = ActionType.Copy;

                //if (this.VideoList.Count < 1)
                //{
                //}
                //else
                //{
                //    MessageBox.Show("一張送播單僅限一個檔案");
                //    return;
                //}

                //IsCopy_Segment = true;
            }

            if (Brocast.FSBRO_TYPE == "G" && string.IsNullOrEmpty(Brocast.FSID))
            {
                MessageBox.Show("請先選擇節目", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (Brocast.FSBRO_TYPE == "G" && (Brocast.FNEPISODE == null || Brocast.FNEPISODE == 0))
            {
                MessageBox.Show("請先選擇子集", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (Brocast.FSBRO_TYPE == "P" && string.IsNullOrEmpty(Brocast.FSID))
            {
                MessageBox.Show("請先選擇短帶", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (cbArcType.SelectedIndex < 0)
            {
                MessageBox.Show("請選擇影片類型");
            }


            if (Brocast.FCCHANGE == "Y" && DGLogList.SelectedItem == null)//置換時，不管對段落做什麼動作都要先選
            {
                MessageBox.Show("請選擇欲置換的檔案", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (Brocast.FCCHANGE == "N" && DGLogList.SelectedItem == null)
            {
                if (seg_ActionType != ActionType.Create) //非置換時，只有在不是新增時需要選
                {
                    MessageBox.Show("請選擇欲修改的檔案", "提示訊息", MessageBoxButton.OK);
                    return;
                }
            }

            if (this.rdbProg.IsChecked == true && this.AudioTrackSetControl.GetAudioTrackSetting() == "")
            {
                MessageBox.Show("音軌設定為必填");
                return;
            }

            trackTemp = AudioTrackSetControl.GetAudioTrackSetting();

            Class_LOG_VIDEO logVideo = (Class_LOG_VIDEO)this.DGLogList.SelectedItem;

            if (logVideo != null)
                logVideo.FSTRACK = trackTemp;

            if (btn.Name == "btnSeg")
            {
                Brocast_Client.RecordStepIntoTrackinglogAsync("STB100_01 新增段落檔", Brocast.FSBRO_ID, UserClass.userData.FSUSER_ID);
            }
            else if (btn.Name == "btnEdit")
            {
                Brocast_Client.RecordStepIntoTrackinglogAsync("STB100_01 修改段落檔", STB100.GetValuesToString(logVideo), UserClass.userData.FSUSER_ID);

            }
            else if (btn.Name == "btnDel")
            {
                Brocast_Client.RecordStepIntoTrackinglogAsync("STB100_01 刪除段落檔", logVideo.FSFILE_NO, UserClass.userData.FSUSER_ID);
            }
            else if (btn.Name == "btnCopy")
            {
                Brocast_Client.RecordStepIntoTrackinglogAsync("STB100_01 複製段落檔", STB100.GetValuesToString(logVideo), UserClass.userData.FSUSER_ID);
            }


            if ((seg_ActionType == ActionType.Edit || seg_ActionType == ActionType.Delete) && logVideo.FSBRO_ID.Trim() != Brocast.FSBRO_ID.Trim())
            {
                MessageBox.Show("此筆並非為當次申請的段落資料，因此無法異動", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (seg_ActionType == ActionType.Delete && (logVideo.FCFILE_STATUS.Trim() != "B"))
            {
                MessageBox.Show("此筆段落資料狀態不是「待轉檔」，因此無法異動", "提示訊息", MessageBoxButton.OK);
                return;
            }


            if (seg_ActionType == ActionType.Delete)
            {
                if (MessageBox.Show("確定要刪除「" + logVideo.FSARC_TYPE_NAME + "」的段落資料嗎?", "提示訊息", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                {
                    if (form_Type == FormType.Displacement)
                    {
                        if (VideoList.Remove(logVideo))
                        {
                            this.DGLogList.ItemsSource = null;
                            this.DGLogList.ItemsSource = VideoList;
                        }
                    }
                    else
                    {
                        //刪除段落檔
                        Brocast_Client.DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONEAsync(logVideo.FSFILE_NO, UserClass.userData.FSUSER_ID.ToString());
                    }
                }
                else
                {
                    return;
                }
            }
            else if (seg_ActionType == ActionType.Edit)
            {
                if (form_Type == FormType.Displacement && Bro_ActionType == ActionType.Create)
                {

                    Log_Video_Seg_Modify(logVideo);
                }
                else
                {
                    Load_Video_Seg();
                }
            }
            else if (form_Type == FormType.Displacement)
            {
                if (seg_ActionType == ActionType.Copy || seg_ActionType == ActionType.Create)
                {
                    // if (logVideo.Brocast_Status != null && logVideo.Brocast_Status != "U")
                    //{
                    //    MessageBox.Show("送播單(" + logVideo.FSBRO_ID + ")狀態為「" + CheckStatus_SendBro(logVideo.Brocast_Status) + "」，無法置換此檔案", "提示訊息", MessageBoxButton.OK);
                    //    return;
                    //}//
                    // else 
                    //logVideo.FCFILE_STATUS != "B" &&
                    if ((logVideo.FCFILE_STATUS != "F" && logVideo.FCFILE_STATUS != "T" && logVideo.FCFILE_STATUS != "Y") && Bro_ActionType == ActionType.Create)
                    {
                        MessageBox.Show("檔案狀態為「" + logVideo.FCFILE_STATUS_NAME + "」，無法置換此檔案", "提示訊息", MessageBoxButton.OK);
                        return;
                    }


                    string ileagalStatus = "BTYS";
                    for (int i = 0; i < VideoList.Count; i++)
                    {
                        if (VideoList.Where(a => a.FSBRO_ID == Brocast.FSBRO_ID).Count() > 0)
                        {
                            MessageBox.Show("每張送播置換單每次只能新增一筆置換資料！", "提示訊息", MessageBoxButton.OK);
                            return;
                        }
                        ////擋同樣的檔案，不能新增一筆以上的檔案置換資料
                        //if (VideoList[i].FSCHANGE_FILE_NO.Trim() == logVideo.FSFILE_NO.Trim() && ileagalStatus.Contains(VideoList[i].FCFILE_STATUS))
                        //{
                        //    MessageBox.Show("檔案編號為：" + ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSFILE_NO.Trim() + "，只能新增一筆置換資料！", "提示訊息", MessageBoxButton.OK);
                        //    return;
                        //}
                    }

                    if (seg_ActionType == ActionType.Copy)
                    {
                        Load_Video_Seg();
                    }
                    else
                    {
                        //置換前強制取一個VideoID
                        Brocast_Client.QueryVideoID_PROG_STTAsync(Brocast.FSID, Brocast.FNEPISODE.ToString(), Arc_type_ID, true);
                    }


                }
            }
            else if (Bro_ActionType == ActionType.Create && form_Type == FormType.Normal)
            {
                LoadTBLOG_VIDEO();
            }
            else
            {

                Load_Video_Seg();
            }
        }


        /// <summary>
        /// 修改時呼叫的函式
        /// 會直接傳入被選擇的Class_LOG_VIDEO
        /// </summary>
        /// <param name="log_video">在DGLogList中被選擇的Class_LOG_VIDEO</param>
        private void Log_Video_Seg_Modify(Class_LOG_VIDEO log_video)
        {
            string strProgName = tbxPROG_NAME.Text.Trim();
            string strEpisodeName = "";

            if (Brocast.FSBRO_TYPE == "G")
            {
                if (Brocast.FNEPISODE == 0)
                    strEpisodeName = strProgName;
                else
                    strEpisodeName = lblEPISODE_NAME.Content.ToString();
            }
            else
                strEpisodeName = strProgName;
            string changeFileno = log_video.FSCHANGE_FILE_NO;
            STT100_10_01 STT100_10_01_frm = new STT100_10_01(log_video.FSARC_TYPE_NAME, Arc_type_ID, Brocast.FSBRO_TYPE, Brocast.FSID, Brocast.FNEPISODE.ToString(), Brocast.FSCHANNELID, log_video.FSFILE_NO, Brocast.FSBRO_ID, Brocast.FSID_NAME, Brocast.FNEPISODE_NAME, "", true, log_video.FSVIDEO_PROG, log_video, log_video.FSTRACK);
            STT100_10_01_frm.Show();

            //若是有填入段落檔，就要去讀取該節目集別的段落資料，並且秀在畫面上
            STT100_10_01_frm.Closing += (s, args) =>
            {
                //if (STT100_10_01_frm.DialogResult == true)
                //    ListLogVideo.Add(STT100_10_01_frm.myCLV);
                //DGLogList.ItemsSource = null;

                for (int i = 0; i < VideoList.Count; i++)
                {
                    if (VideoList[i].FSFILE_NO == STT100_10_01_frm.myCLV.FSFILE_NO)
                    {
                        VideoList[i].VideoListSeg = STT100_10_01_frm.ListSeg;
                        VideoList[i].FSCHANGE_FILE_NO = changeFileno;

                    }
                }
                DGLogList.ItemsSource = null;
                DGLogList.ItemsSource = VideoList;
            };
        }



        void Brocast_Client_QueryVideoID_PROG_STTCompleted(object sender, WSBROADCAST.QueryVideoID_PROG_STTCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Error == null)
                {
                    if (e.Result.Trim() != "")
                    {
                        if (form_Type == FormType.Displacement && Bro_ActionType != ActionType.Copy && seg_ActionType != ActionType.Copy)
                        {
                            Class_LOG_VIDEO logVideo = (Class_LOG_VIDEO)DGLogList.SelectedItem;

                            STT100_10_01 _STT100_10_01 = new STT100_10_01(logVideo.FSARC_TYPE_NAME, Arc_type_ID, Brocast.FSBRO_TYPE, Brocast.FSID, Brocast.FNEPISODE.ToString(), Brocast.FSCHANNELID, seg_ActionType == ActionType.Edit ? logVideo.FSFILE_NO.Trim() : "", Brocast.FSBRO_ID, Brocast.FSID_NAME, Brocast.FNEPISODE_NAME, Brocast.FCCHANGE == "Y" ? logVideo.FSFILE_NO : "", Bro_ActionType == ActionType.Copy, e.Result, new Class_LOG_VIDEO(), trackTemp == "" ? "MMMMMMMM" : trackTemp);
                            _STT100_10_01.Show();
                            _STT100_10_01.Closed += (s, args) =>
                            {
                                if (_STT100_10_01.DialogResult == true)
                                {
                                    _STT100_10_01.myCLV.FCFILE_STATUS_NAME = Translate_Status_Name.TransStatusName_Video_HDMC(_STT100_10_01.myCLV.FCFILE_STATUS, "");

                                    VideoList.Add(_STT100_10_01.myCLV);
                                    this.DGLogList.ItemsSource = null;
                                    this.DGLogList.ItemsSource = VideoList;
                                }
                            };
                        }
                        else if (form_Type == FormType.Displacement && seg_ActionType == ActionType.Copy)
                        {
                            Log_video.FSVIDEO_PROG = e.Result;
                            Brocast_Client.fnGetFileIDAsync(Brocast.FSBRO_TYPE, Brocast.FSID, Brocast.FNEPISODE.ToString(), UserClass.userData.FSUSER_ID.ToString(), Brocast.FSCHANNELID, Brocast.FSID_NAME, Brocast.FNEPISODE_NAME);

                        }
                        else
                        {
                            Log_video.FSVIDEO_PROG = e.Result;
                            foreach (Class_LOG_VIDEO video in VideoList)
                            {
                                video.FSVIDEO_PROG = e.Result;
                                foreach (Class_LOG_VIDEO_SEG seg in video.VideoListSeg)
                                {
                                    seg.FSVIDEO_ID = video.FSVIDEO_PROG + String.Format("{0:00}", Convert.ToInt32(seg.FNSEG_ID));
                                }
                            }

                            if (form_Type == FormType.Normal && Bro_ActionType == ActionType.Copy)
                            {

                                Brocast_Client.fnGetFileIDAsync(Brocast.FSBRO_TYPE, Brocast.FSID, Brocast.FNEPISODE.ToString(), UserClass.userData.FSUSER_ID, Brocast.FSCHANNELID, Brocast.FSID_NAME, Brocast.FNEPISODE_NAME);
                            }
                        }
                        //if (bro_ActionType == ActionType.Copy || Bro_Type == STBType.Displacement)
                        //{
                        //    Brocast_Client.fnGetFileIDAsync(Brocast.FSBRO_TYPE, Brocast.FSID, Brocast.FNEPISODE.ToString(), UserClass.userData.FSUSER_ID.ToString(), Brocast.FSCHANNELID, Brocast.FSID_NAME, Brocast.FNEPISODE_NAME);
                        //}
                        //else
                        //{
                        if (Bro_ActionType != ActionType.Copy && form_Type != FormType.Displacement)
                            Load_Video_Seg();

                        // }
                    }
                }
                else
                {
                    MessageBox.Show("取得新的VideoID時發生錯誤", "提示", MessageBoxButton.OK);
                }

            }
        }

        void Brocast_Client_fnGetFileIDCompleted(object sender, WSBROADCAST.fnGetFileIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    string strGet = e.Result;  //接收回來的參數，前者是檔案編號，後者是DIRID                  

                    char[] delimiterChars = { ';' };
                    string[] words = strGet.Split(delimiterChars);

                    string newFileNO = "";
                    string newDirID = "";
                    if (words.Length == 2)
                    {
                        newFileNO = words[0];
                        //Log_video.FSFILE_NO = words[0];
                        newDirID = words[1];
                    }
                    else if (words.Length == 1)
                    {
                        newFileNO = words[0];
                        //Log_video.FSFILE_NO = words[0];
                        newDirID = "";
                    }
                    else
                    {
                        newFileNO = "";
                        //Log_video.FSFILE_NO = "";
                        newDirID = "";
                    }

                    if (form_Type == FormType.Displacement && seg_ActionType == ActionType.Copy)
                    {
                        Class_LOG_VIDEO oldLogVideo = (Class_LOG_VIDEO)this.DGLogList.SelectedItem;

                        if (oldLogVideo != null)
                        {

                            Class_LOG_VIDEO newLogVideo = new Class_LOG_VIDEO();
                            PropertyInfo[] propArr = oldLogVideo.GetType().GetProperties();
                            foreach (PropertyInfo pi in propArr)
                            {
                                pi.SetValue(newLogVideo, pi.GetValue(oldLogVideo, null), null);
                            }

                            newLogVideo.FSBRO_ID = Brocast.FSBRO_ID;
                            newLogVideo.FCFILE_STATUS = "B";
                            newLogVideo.FCFILE_STATUS_NAME = Translate_Status_Name.TransStatusName_Video_HDMC(newLogVideo.FCFILE_STATUS, "");
                            newLogVideo.FSTRACK = this.AudioTrackSetControl.GetAudioTrackSetting();
                            newLogVideo.FSVIDEO_PROG = Log_video.FSVIDEO_PROG;
                            newLogVideo.FSFILE_NO = newFileNO;
                            newLogVideo.FNDIR_ID = newDirID != "" ? Convert.ToInt64(newDirID) : 0;
                            newLogVideo.FSCHANGE_FILE_NO = oldLogVideo.FSFILE_NO;
                            foreach (Class_LOG_VIDEO_SEG seg in newLogVideo.VideoListSeg)
                            {
                                seg.FSFILE_NO = newLogVideo.FSFILE_NO;
                                seg.FSVIDEO_ID = newLogVideo.FSVIDEO_PROG + seg.FNSEG_ID;
                            }
                            VideoList.Add(newLogVideo);
                            this.DGLogList.ItemsSource = null;
                            this.DGLogList.ItemsSource = VideoList;
                        }


                        ////for (int i = 0; i < ListSeg.Count; i++)
                        ////{
                        ////    ListSeg[i].FSFILE_NO = m_strFileNONew.Trim();  //取得檔案編號(FSFILE_NO)塞進段落List裡
                        ////}

                        ////List<Class_LOG_VIDEO_SEG> listSeg = new List<Class_LOG_VIDEO_SEG>();

                        ////listSeg.Add(new PTS_MAM3.WSBROADCAST.Class_LOG_VIDEO_SEG() { FSVIDEO_ID = Log_video.FSVIDEO_PROG, FSFILE_NO = Log_video.FSFILE_NO.Trim(), FSBEG_TIMECODE = "01:00:00;00", FSEND_TIMECODE = "01:01:10;02", FNSEG_ID = "01", FSCREATED_BY = UserClass.userData.FSUSER_ID, FSUPDATED_BY = UserClass.userData.FSUSER_ID });
                        ////Brocast_Client.INSERT_TBLOG_VIDEO_SEGAsync(listSeg);  //新增段落

                        ////if (bro_ActionType != ActionType.Copy)
                        //Load_Video_Seg();
                    }
                    else if (form_Type == FormType.Normal && Bro_ActionType == ActionType.Copy)
                    {
                        VideoList[0].FSFILE_NO = newFileNO;
                        foreach (Class_LOG_VIDEO_SEG seg in VideoList[0].VideoListSeg)
                        {
                            seg.FSFILE_NO = newFileNO;
                        }
                        long tempLong = 0;
                        Int64.TryParse(newDirID, out tempLong);
                        VideoList[0].FNDIR_ID = tempLong;
                    }

                }
            }
        }

        string trackTemp = "";
        //新增、修改段落資料
        private void Load_Video_Seg()
        {
            Class_LOG_VIDEO logVideo = (Class_LOG_VIDEO)DGLogList.SelectedItem;

            if (Bro_ActionType == ActionType.Copy && seg_ActionType == ActionType.Unknow)
            {
                foreach (Class_LOG_VIDEO video in VideoList)
                {
                    video.FSFILE_NO = Log_video.FSFILE_NO;
                    video.FSVIDEO_PROG = Log_video.FSVIDEO_PROG;
                    video.FNDIR_ID = Convert.ToInt64(m_DIRID);
                    foreach (Class_LOG_VIDEO_SEG seg in video.VideoListSeg)
                    {
                        seg.FSFILE_NO = Log_video.FSFILE_NO;
                        seg.FSVIDEO_ID = Log_video.FSVIDEO_PROG + seg.FNSEG_ID;
                    }
                }

                this.DGLogList.ItemsSource = VideoList;
                this.DGLogList.SelectedIndex = 0;

                //Copy的最後取一個新的單號
                MAM_Client.GetNoRecordAsync("07", "", "", UserClass.userData.FSUSER_ID);
            }
            else //if (bro_ActionType != ActionType.Copy && seg_ACtionType != ActionType.Copy)
            {
                //要串給長結點用的
                string strProgName = tbxPROG_NAME.Text.Trim();
                string strEpisodeName = "";
                trackTemp = AudioTrackSetControl.GetAudioTrackSetting();
                if (Brocast.FSBRO_TYPE == "G")
                {
                    if (Brocast.FNEPISODE == 0)
                        strEpisodeName = strProgName;
                    else
                        strEpisodeName = lblEPISODE_NAME.Content.ToString();

                    if (trackTemp == "")
                    {
                        MessageBox.Show("音軌設定為必填!");
                        return;
                    }
                    else if (this.cbArcType.SelectedIndex < 0)
                    {
                        MessageBox.Show("請先選擇影片類型");
                        return;
                    }
                }
                else
                {
                    trackTemp = "MMMMMMMM";
                    strEpisodeName = strProgName;
                }

                if (Bro_ActionType == ActionType.Copy)
                {
                    //if (log_video != null)
                    //{
                    if (logVideo.FSFILE_NO != "")
                    {
                        ComboBoxItem cbi = (ComboBoxItem)this.cbArcType.SelectedItem;
                        logVideo.FSTRACK = trackTemp;
                        logVideo.FSARC_TYPE = cbi.Tag.ToString();
                        logVideo.FSARC_TYPE_NAME = cbi.Content.ToString();
                        STT911_01_01 _911_01_01 = new STT911_01_01(logVideo, Brocast.FSID_NAME, Brocast.FNEPISODE.ToString(), true);

                        _911_01_01.Closed += (s, args) =>
                        {
                            if (_911_01_01.DialogResult == true)
                            {
                                logVideo = _911_01_01.Log_Video;
                                if (VideoList.Count == 0)
                                    VideoList.Add(logVideo);

                                this.DGLogList.ItemsSource = VideoList;
                            }
                        };
                        _911_01_01.Show();
                    }
                    //}
                }
                else
                {

                    if (form_Type == FormType.Displacement)
                    {
                        if (logVideo != null)
                        {
                            if (seg_ActionType == ActionType.Create || seg_ActionType == ActionType.Copy)
                            {
                                if ((logVideo.FCFILE_STATUS != "B" && logVideo.FCFILE_STATUS != "F" && logVideo.FCFILE_STATUS != "T" && logVideo.FCFILE_STATUS != "Y") && Bro_ActionType == ActionType.Create)
                                {
                                    MessageBox.Show("檔案狀態為「" + logVideo.FCFILE_STATUS_NAME + "」，無法置換此檔案", "提示訊息", MessageBoxButton.OK);
                                    return;
                                }
                                //2016/04/14 暫時開放做測試
                                else if (logVideo.Brocast_Status != "U")
                                {
                                    MessageBox.Show("送播單(" + logVideo.FSBRO_ID + ")狀態為「" + CheckStatus_SendBro(logVideo.Brocast_Status) + "」，無法置換此檔案", "提示訊息", MessageBoxButton.OK);
                                    return;
                                }

                                string ileagalStatus = "BTYS";
                                for (int i = 0; i < VideoList.Count; i++)
                                {
                                    //擋同樣的檔案，不能新增一筆以上的檔案置換資料
                                    if (VideoList[i].FSCHANGE_FILE_NO.Trim().Equals(logVideo.FSFILE_NO.Trim()) && ileagalStatus.Contains(VideoList[i].FCFILE_STATUS))
                                    {
                                        MessageBox.Show("檔案編號為：" + logVideo.FSFILE_NO.Trim() + "，只能新增一筆置換資料！", "提示訊息", MessageBoxButton.OK);
                                        return;
                                    }
                                }

                                Brocast_Client.QueryVideoID_PROG_STTAsync(Brocast.FSID, Brocast.FNEPISODE.ToString(), Arc_type_ID, true);
                                return;
                            }

                        }
                    }
                    //else
                    //{
                    STT100_01_01 STT100_01_01_frm = new STT100_01_01(((ComboBoxItem)cbArcType.SelectedItem).Content.ToString(), Arc_type_ID, Brocast.FSBRO_TYPE, Brocast.FSID, Brocast.FNEPISODE.ToString(), Brocast.FSCHANNELID, seg_ActionType == ActionType.Edit ? logVideo.FSFILE_NO.Trim() : "", Brocast.FSBRO_ID, Brocast.FSID_NAME, Brocast.FNEPISODE_NAME, Brocast.FCCHANGE == "Y" ? logVideo.FSFILE_NO : "", seg_ActionType == ActionType.Copy,
    (Log_video.FSVIDEO_PROG == null ?
    (VideoList.Count > 0 ? VideoList[0].FSVIDEO_PROG : "") : Log_video.FSVIDEO_PROG), trackTemp);
                    STT100_01_01_frm.Show();

                    //若是有填入段落檔，就要去讀取該節目集別的段落資料，並且秀在畫面上
                    STT100_01_01_frm.Closing += (s, args) =>
                    {
                        if (STT100_01_01_frm.DialogResult == true)
                        {
                            rdbProg.IsEnabled = false;
                            rdbPromo.IsEnabled = false;
                            btnPROG.IsEnabled = false;
                            btnEPISODE.IsEnabled = false;

                            seg_ActionType = ActionType.Unknow;
                            LoadTBLOG_VIDEO();
                        }


                    };
                    //}
                }
            }
        }


        //載入入庫影像檔
        private void LoadTBLOG_VIDEO()
        {
            BusyMsg.IsBusy = true;

            string searchBroId = "";
            if ((Brocast.FCCHANGE == "Y" && Bro_ActionType == ActionType.Create) || Bro_ActionType == ActionType.Create || Bro_ActionType == ActionType.Copy)
            {
                searchBroId = "";
            }
            //else if (Brocast.FCCHANGE == "Y" && bro_ActionType == ActionType.View)
            //{
            //    searchBroId = oldBroadcast_ID;
            //}
            else
            {
                searchBroId = Brocast.FSBRO_ID;
            }

            Brocast_Client.GetTBLOG_VIDEO_BY_Type_EPISODEAsync(Brocast.FSBRO_TYPE, Arc_type_ID, Brocast.FSID, Brocast.FNEPISODE.ToString(), searchBroId);
        }

        private void btnEvent_Click(object sender, RoutedEventArgs e)
        {

        }



        private void DGLogList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.DGLogList.SelectedItem != null)
            {
                Class_LOG_VIDEO logVideo = (Class_LOG_VIDEO)this.DGLogList.SelectedItem;
                ChangeArcType_Show(logVideo.FSARC_TYPE);
                this.tbTrack.Text = logVideo.FSTRACK;
                this.AudioTrackSetControl.SetAudioTrackSetting(logVideo.FSTRACK);

                if (Bro_ActionType == ActionType.View)
                {
                    this.tbTrack.Visibility = System.Windows.Visibility.Visible;
                    this.AudioTrackSetControl.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
        }

        private void rdbProg_Checked(object sender, RoutedEventArgs e)
        {
            if (Brocast != null && Bro_ActionType != ActionType.Copy)
            {
                Brocast.FSBRO_TYPE = "G";

                if (gdPROG_M != null)
                {
                    this.gdPROG_M.RowDefinitions[2].Height = new GridLength(29, GridUnitType.Star);
                    this.gdPROG_M.RowDefinitions[3].Height = new GridLength(29, GridUnitType.Star);
                    this.gdPROG_M.RowDefinitions[4].Height = new GridLength(29, GridUnitType.Star);
                    this.gdPROG_M.RowDefinitions[5].Height = new GridLength(29, GridUnitType.Star);
                    this.gdPROG_M.RowDefinitions[6].Height = new GridLength();
                    if (Bro_ActionType == ActionType.Create)
                    {
                        this.btnViewSeg.Visibility = System.Windows.Visibility.Collapsed;
                    }

                    if (Bro_ActionType == ActionType.Copy)
                    {
                        if (CopyFlag)
                        {
                            Brocast.FSID = "";
                            Brocast.FSID_NAME = "";
                        }
                        CopyFlag = true;
                    }
                    else
                    {
                        Brocast.FSID = "";
                        Brocast.FSID_NAME = "";
                    }
                    this.tbPROG_NANE.Text = "節目名稱";
                }
            }
        }

        bool CopyFlag = false;
        private void rdbPromo_Checked(object sender, RoutedEventArgs e)
        {
            if (Brocast != null)
            {
                Brocast.FSBRO_TYPE = "P";

                this.gdPROG_M.RowDefinitions[2].Height = new GridLength(0);
                this.gdPROG_M.RowDefinitions[3].Height = new GridLength(0);
                this.gdPROG_M.RowDefinitions[4].Height = new GridLength(0);
                this.gdPROG_M.RowDefinitions[5].Height = new GridLength(0);
                this.gdPROG_M.RowDefinitions[6].Height = new GridLength(0);
                if (Bro_ActionType == ActionType.Create)
                {
                    this.btnSet.Visibility = System.Windows.Visibility.Collapsed;
                    this.btnViewSeg.Visibility = System.Windows.Visibility.Collapsed;
                }

                if (Bro_ActionType == ActionType.Copy)
                {
                    if (CopyFlag)
                    {
                        Brocast.FSID = "";
                        Brocast.FSID_NAME = "";
                    }
                    CopyFlag = true;
                }
                else
                {
                    Brocast.FSID = "";
                    Brocast.FSID_NAME = "";
                }
                this.tbPROG_NANE.Text = "短帶名稱";
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (Bro_ActionType != ActionType.View || (Bro_ActionType == ActionType.Edit && Brocast.FCCHECK_STATUS != "R"))
            {
                if (string.IsNullOrEmpty(Brocast.FSID))
                {
                    if (Brocast.FSBRO_TYPE == "G")
                        MessageBox.Show("請選擇節目");
                    else if (Brocast.FSBRO_TYPE == "P")
                        MessageBox.Show("請選擇短帶");

                    return;
                }
                else if (rdbProg.IsChecked == true && (Brocast.FNEPISODE == null || Brocast.FNEPISODE == 0))
                {
                    MessageBox.Show("請選擇子集");
                    return;
                }
                else if (cbArcType.SelectedIndex < 0)
                {
                    MessageBox.Show("請選擇影片類型");
                    return;
                }
                else if (DGLogList.GetRows().Count == 0)
                {
                    MessageBox.Show("尚未新增任何檔案");
                    return;
                }



                BusyMsg.IsBusy = true;
                if (Bro_ActionType == ActionType.Create)
                {
                    Brocast_Client.RecordStepIntoTrackinglogAsync("STB100_01 新增送播單_Bro", STB100.GetValuesToString(FormToClass_Bro()), UserClass.userData.FSUSER_ID);
                    if (Brocast.FCCHANGE == "Y")
                    {
                        List<Class_LOG_VIDEO> tempList = new List<Class_LOG_VIDEO>();
                        foreach (Class_LOG_VIDEO video in VideoList)
                        {
                            if (video.FSBRO_ID == Brocast.FSBRO_ID)
                            {
                                //Brocast_Client.RecordStepIntoTrackinglogAsync("STB100_01 新增送播單_Video", STB100.GetValuesToString(video), UserClass.userData.FSUSER_ID);
                                tempList.Add(video);
                            }
                        }

                        VideoList = tempList;
                        if (tempList.Count > 0)
                        {
                            Brocast_Client.INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLISTAsync(FormToClass_Bro(), VideoList);
                            //Brocast_Client.INSERT_BROADCAST_WITHOUT_FLOWAsync(FormToClass_Bro());
                        }
                        else
                        {
                            MessageBox.Show("尚未新增要置換的段落");
                            BusyMsg.IsBusy = false;
                        }
                    }
                    else
                    {
                        Brocast_Client.RecordStepIntoTrackinglogAsync("STB100_01 新增送播單", STB100.GetValuesToString(FormToClass_Bro()), UserClass.userData.FSUSER_ID);
                        Brocast_Client.INSERT_BROADCAST_WITHOUT_FLOWAsync(FormToClass_Bro());
                    }
                }
                else if (Bro_ActionType == ActionType.Edit)
                {
                    Brocast_Client.RecordStepIntoTrackinglogAsync("STB100_01 修改送播單", STB100.GetValuesToString(FormToClass_Bro()), UserClass.userData.FSUSER_ID);

                    Brocast_Client.UPDATE_TBBROADCAST_StatusAsync(Brocast.FSBRO_ID, "N", UserClass.userData.FSUSER_ID);
                }
                else if (Bro_ActionType == ActionType.Copy)
                {
                    //if (VideoList[0].FSFILE_NO == "")
                    //{
                    //    MessageBox.Show("請修改節目資料或段落資料");
                    //    BusyMsg.IsBusy = false;
                    //    return;
                    //}
                    Brocast_Client.RecordStepIntoTrackinglogAsync("STB100_01 複製送播單", STB100.GetValuesToString(FormToClass_Bro()), UserClass.userData.FSUSER_ID);
                    LoadTBLOG_VIDEO();
                    // this.DialogResult = true;
                }

            }
            else
            {
                this.DialogResult = false;
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("確定取消表單?", "提示", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {
                if (Bro_ActionType == ActionType.Copy || Bro_ActionType == ActionType.Create)
                {
                    seg_ActionType = ActionType.Copy;
                    Brocast_Client.RecordStepIntoTrackinglogAsync("STB100_01 取消送播單", STB100.GetValuesToString(Brocast), UserClass.userData.FSUSER_ID);
                    Brocast_Client.Delete_BROADCAST_AT_ONCEAsync(Brocast.FSBRO_ID, UserClass.userData.FSUSER_ID);
                }
                else
                { this.DialogResult = false; }

            }
        }


        private void btnSet_Click(object sender, RoutedEventArgs e)
        {
            if (Brocast.FSBRO_TYPE == "G" && string.IsNullOrEmpty(Brocast.FSID))
            {
                MessageBox.Show("請先選擇節目名稱", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (Brocast.FSBRO_TYPE == "G" && Brocast.FSID != "" && (Brocast.FNEPISODE == null || Brocast.FNEPISODE == 0))
            {
                MessageBox.Show("請先選擇子集", "提示訊息", MessageBoxButton.OK);
                return;
            }

            ////選定好節目或是短帶後，要鎖定
            //rdbProg.IsEnabled = false;
            //rdbPromo.IsEnabled = false;
            //btnPROG.IsEnabled = false;
            //btnEPISODE.IsEnabled = false;

            PTS_MAM3.PGM.PGM100_05 PGM100_05_Frm = new PTS_MAM3.PGM.PGM100_05();

            PGM100_05_Frm.textBoxProgID.Text = Brocast.FSID;
            PGM100_05_Frm.textBoxProgName.Text = Brocast.FSID_NAME;
            PGM100_05_Frm.textBoxEpisode.Text = Brocast.FNEPISODE.ToString();
            PGM100_05_Frm.Show();
            PGM100_05_Frm.Closed += (s, args) =>
            {
            };
        }

        void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void btnCopy_Bro_Click(object sender, RoutedEventArgs e)
        {
            //BusyMsg.IsBusy = true;

            STB100_01 copyBro = new STB100_01(Brocast, VideoList, form_Type);
            copyBro.Closed += (s, args) =>
            {
                this.DialogResult = copyBro.DialogResult;
            };
            copyBro.Show();


            //要可以改節目、集數，所以這邊就不檢查了!
            //Brocast_Client.Query_CheckIsAbleToCreateNewTransformFormAsync(Brocast.FSID, Brocast.FNEPISODE.ToString(), VideoList);



            //string availibleStatus = "X;F;R";

            //if (availibleStatus.Contains(VideoList[0].FCFILE_STATUS))
            //{
            //    STB100_01 copyBro = new STB100_01(Brocast, ActionType.Copy, Bro_Type);
            //    copyBro.Closed += (s, args) =>
            //    {
            //        this.DialogResult = copyBro.DialogResult;
            //    };
            //    copyBro.Show();
            //}
            //else
            //{
            //    MessageBox.Show("無法新增第二筆 " + Brocast.FSID_NAME + " 第" + Brocast.FNEPISODE.ToString() + "集的HD送播帶。");
            //    return;
            //}
        }

        private void btnView_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGLogList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預檢視的資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            Class_LOG_VIDEO log_video = (Class_LOG_VIDEO)DGLogList.SelectedItem;

            string strFileTypeName = log_video.FSARC_TYPE_NAME.Trim();
            string strFileTypeID = log_video.FSARC_TYPE.Trim();

            if (Bro_ActionType != ActionType.Copy)
            {
                STT300_03 STT300_03_frm = new STT300_03(strFileTypeName, strFileTypeID, Brocast.FSBRO_TYPE, Brocast.FSID, Brocast.FNEPISODE.ToString(), (Class_LOG_VIDEO)DGLogList.SelectedItem, (Bro_ActionType == ActionType.Copy ? oldBroadcast_ID : Brocast.FSBRO_ID));
                STT300_03_frm.Show();
            }
            else
            {
                STT300_03 STT300_03_frm = new STT300_03(strFileTypeName, strFileTypeID, Brocast.FSBRO_TYPE, Brocast.FSID, Brocast.FNEPISODE.ToString(), (Class_LOG_VIDEO)DGLogList.SelectedItem, (Bro_ActionType == ActionType.Copy ? oldBroadcast_ID : Brocast.FSBRO_ID), false);
                STT300_03_frm.Show();
            }
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;

        }

        private void btnReportStick_Click(object sender, RoutedEventArgs e)
        {
            if (DGLogList.SelectedItem == null)
            {
                MessageBox.Show("請選擇欲列印的送帶轉檔資料", "提示訊息", MessageBoxButton.OK);
                return;
            }
            Brocast_Client.CallReport_Archive_StickAsync(((Class_LOG_VIDEO)DGLogList.SelectedItem).FSFILE_NO.ToString().Trim(), UserClass.userData.FSSESSION_ID.ToString().Trim());
        }

        private void btnReportSmall_Click(object sender, RoutedEventArgs e)
        {

            if (DGLogList.SelectedItem == null)
            {
                MessageBox.Show("請選擇欲列印的送帶轉檔資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            Brocast_Client.CallReport_Archive_SmallAsync(((Class_LOG_VIDEO)DGLogList.SelectedItem).FSFILE_NO.ToString().Trim(), UserClass.userData.FSSESSION_ID.ToString().Trim());
        }

        private void btnReportBig_Click(object sender, RoutedEventArgs e)
        {

            if (DGLogList.SelectedItem == null)
            {
                MessageBox.Show("請選擇欲列印的送帶轉檔資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //列印成案單(丟入參數：檔案編號、查詢者、SessionID)
            Brocast_Client.CallReport_Archive_BigAsync(((Class_LOG_VIDEO)DGLogList.SelectedItem).FSFILE_NO.ToString().Trim(), UserClass.userData.FSSESSION_ID.ToString().Trim());
        }

        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGLogList.SelectedItem == null)
            {
                MessageBox.Show("請選擇欲列印的送帶轉檔資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            if (Brocast.FSBRO_TYPE == "G")
            {
                Brocast_Client.CallREPORT_BRO_DetailAsync(((Class_LOG_VIDEO)DGLogList.SelectedItem).FSFILE_NO, UserClass.userData.FSUSER_ID, UserClass.userData.FSSESSION_ID);
            }
            else
            {
                PrintSTT_PROMO();
            }
        }

        //列印送帶轉檔單_多筆短帶資料
        private void PrintSTT_PROMO()
        {
            //列印成案單(丟入參數：成案單編號、查詢者、SessionID)
            Brocast_Client.CallREPORT_BRO_PROMOAsync(Brocast.FSPRINTID.ToString().Trim(), UserClass.userData.FSUSER_ID.ToString().Trim(), UserClass.userData.FSSESSION_ID.ToString().Trim());
        }

        private void btnReject_Click(object sender, RoutedEventArgs e)
        {
            // this.DGLogList.SelectedIndex = 0;
            //Class_LOG_VIDEO log_video=(Class_LOG_VIDEO)this.DGLogList.SelectedItem;
            //if (log_video.FCFILE_STATUS != "B") 
            //{

            //} 
            isRejectOrUpload = true;

            Brocast_Client.GetTBBROADCAST_BYBROIDAsync(Brocast.FSBRO_ID);


        }

        private void btnUpload_Click(object sender, RoutedEventArgs e)
        {
            if (Brocast.FCCHECK_STATUS == "N" || Brocast.HD_MC_Status == "E")
            {
                isRejectOrUpload = false;
                Brocast_Client.GetTBBROADCAST_BYBROIDAsync(Brocast.FSBRO_ID);
            }
            else
            {
                MessageBox.Show("「" + CheckStatus_SendBro(Brocast.FCCHECK_STATUS) + "」狀態無法上傳檔案!");
            }
        }



        //狀態名稱轉換
        public string CheckStatus_SendBro(string strStatusID)
        {
            switch (strStatusID)
            {
                case "N"://表單初始狀態
                    return "未完成";
                case "R":
                    return "退單";
                case "U":
                    return "開始上傳";
                case "E":
                    return "抽單";
                default:
                    return "";
            }
        }


        private void DGLogList_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Background = new SolidColorBrush(Colors.White);
            e.Row.Foreground = new SolidColorBrush(Colors.Black);

            Class_LOG_VIDEO LOG_VIDEO = e.Row.DataContext as Class_LOG_VIDEO;

            if (LOG_VIDEO.FSBRO_ID.Trim() != Brocast.FSBRO_ID)
                e.Row.Background = new SolidColorBrush(Colors.LightGray);

            if (LOG_VIDEO.FSCHANGE_FILE_NO != "")
                e.Row.Foreground = new SolidColorBrush(Colors.Red);
        }

        private void cbArcType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbArcType.SelectedIndex >= 0)
            {
                ComboBoxItem selectedItem = ((ComboBoxItem)cbArcType.SelectedItem);
                if (selectedItem != null)
                    Arc_type_ID = selectedItem.Tag.ToString();
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cb = ((ComboBox)sender);
            ComboBoxItem cbi = (ComboBoxItem)cb.SelectedItem;
            if (cbi != null)
            {
                DataGridCell parent = (DataGridCell)cb.Parent;
                Class_LOG_VIDEO d = (Class_LOG_VIDEO)parent.DataContext;
                d.FSARC_TYPE_NAME = cbi.Content.ToString();
                d.FSARC_TYPE = cbi.Tag.ToString();
                Arc_type_ID = d.FSARC_TYPE;
                d.FSARC_SPEC_NAME = d.FSARC_TYPE == "001" ? "SD" : "HD";

                d.FSFILE_TYPE_LV = d.FSARC_TYPE == "001" ? "wmv" : "mp4";
            }
        }
    }

    public class Class_ArcTypeToIntConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                string arctype = value.ToString();
                if (arctype == "001")
                    return 0;
                else
                    return 1;
            }

            return 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }

}

