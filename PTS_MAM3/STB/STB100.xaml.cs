﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSBROADCAST;
using System.Windows.Data;


namespace PTS_MAM3.STB
{
    public enum FormType { Normal, Displacement, Post_Production }

    public partial class STB100 : Page
    {
        PTS_MAM3.WSBROADCAST.WSBROADCASTSoapClient Broadcast_Client = new PTS_MAM3.WSBROADCAST.WSBROADCASTSoapClient();

        PTS_MAM3.WSDELETE.WSDELETESoapClient wsDeleteClient = new WSDELETE.WSDELETESoapClient();

        WSHD_MC.WSHD_MCSoapClient hd_mcClient = new WSHD_MC.WSHD_MCSoapClient();
        //抽單用的兩個參數
        Class_BROADCAST class_Broadcast = null;//選中的單
        PTS_MAM3.WSDELETE.Class_DELETED _class_deleted = null;//刪除檔案的資訊

        FormType Broadcast_Type = FormType.Normal;

        public STB100(FormType _100type)
        {
            InitializeComponent();

            Broadcast_Type = _100type;

            if (Broadcast_Type == FormType.Displacement)
            {
                this.tbQuery.Text = "查詢送播置換單";
                this.tbCreate.Text = "新增送播置換單";
                this.tbEdit.Text = "修改送播置換單";
                this.DGBroListDouble.Columns[0].Header = "送播置換單編號";
            }
            Initial();
        }

        public void Initial()
        {


            //依時間範圍與使用者帳號查詢送播單
            Broadcast_Client.fnGetTBBROADCAST_BROADCASTCompleted += new EventHandler<fnGetTBBROADCAST_BROADCASTCompletedEventArgs>(Brocast_Client_fnGetTBBROADCAST_BROADCASTCompleted);

            Broadcast_Client.UPDATE_TBBROADCAST_Staus_DirectlyCompleted += new EventHandler<UPDATE_TBBROADCAST_Staus_DirectlyCompletedEventArgs>(Brocast_Client_UPDATE_TBBROADCAST_Staus_DirectlyCompleted);
            Broadcast_Client.GetTBLOG_VIDEO_SEG_COUNTCompleted += new EventHandler<GetTBLOG_VIDEO_SEG_COUNTCompletedEventArgs>(Brocast_Client_GetTBLOG_VIDEO_SEG_COUNTCompleted);
            Broadcast_Client.GetTBLOG_VIDEO_FileInfoList_BYBROIDCompleted += new EventHandler<GetTBLOG_VIDEO_FileInfoList_BYBROIDCompletedEventArgs>(Brocast_Client_GetTBLOG_VIDEO_FileInfoList_BYBROIDCompleted);

            wsDeleteClient.DeleteFile_No_NewVideoIDCompleted += new EventHandler<WSDELETE.DeleteFile_No_NewVideoIDCompletedEventArgs>(wsDeleteClient_DeleteFile_No_NewVideoIDCompleted);

            wsDeleteClient.Reply_Detached_ErrorCompleted += new EventHandler<WSDELETE.Reply_Detached_ErrorCompletedEventArgs>(wsDeleteClient_Reply_Detached_ErrorCompleted);

            hd_mcClient.Delete_HD_MC_BY_BROIDCompleted += new EventHandler<WSHD_MC.Delete_HD_MC_BY_BROIDCompletedEventArgs>(hd_mcClient_Delete_HD_MC_BY_BROIDCompleted);

            Broadcast_Client.fnGetTBBROADCAST_ADVANCE_FOR_SendBroadcastCompleted += new EventHandler<fnGetTBBROADCAST_ADVANCE_FOR_SendBroadcastCompletedEventArgs>(Broadcast_Client_fnGetTBBROADCAST_ADVANCE_FOR_SendBroadcastCompleted);

            Broadcast_Client.TestMethodCompleted += new EventHandler<TestMethodCompletedEventArgs>(Broadcast_Client_TestMethodCompleted);

            Load_Broadcast();

        }

        void wsDeleteClient_DeleteFile_No_NewVideoIDCompleted(object sender, WSDELETE.DeleteFile_No_NewVideoIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result.Count == 0)
                {
                    Broadcast_Client.UPDATE_TBBROADCAST_Staus_DirectlyAsync(class_Broadcast.FSBRO_ID, "E", UserClass.userData.FSUSER_ID);//改變該筆資料在TBBROADCAST中的狀態
                }
                else
                {//有回傳結果代表有發生錯誤，要復原資料

                    wsDeleteClient.Reply_Detached_ErrorAsync(_class_deleted._fsfile_no, class_Broadcast.FCCHECK_STATUS, UserClass.userData.FSUSER_ID);
                }
            }
        }

        void Broadcast_Client_fnGetTBBROADCAST_ADVANCE_FOR_SendBroadcastCompleted(object sender, fnGetTBBROADCAST_ADVANCE_FOR_SendBroadcastCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                PagedCollectionView pcv = new PagedCollectionView(e.Result);
                //指定DataGrid以及DataPager的內容
                DGBroListDouble.ItemsSource = pcv;
                DataPager.Source = pcv;
                BusyMsg.IsBusy = false;
            }
        }

        void hd_mcClient_Delete_HD_MC_BY_BROIDCompleted(object sender, WSHD_MC.Delete_HD_MC_BY_BROIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                CancelMessage(e.Result);
                _class_deleted = null;
                class_Broadcast = null;
            }
        }


        string m_strType = "";           //類型
        string m_strID = "";             //編碼
        string m_strEpisode = "";        //集別
        string m_strCHANNELID = "";      //頻道別   
        string strBroID = "";
        string strUserID = "";
        DateTime strSDate = DateTime.Now.AddMonths(-2);
        DateTime strEDate = DateTime.Now.AddDays(1);
        string strStatus = "";
        void Load_Broadcast()
        {
            //DateTime dtStart = DateTime.Now.AddMonths(-2);
            //DateTime dtEnd = DateTime.Now.AddDays(1);
            //BusyMsg.IsBusy = true;

            //Brocast_Client.fnGetTBBROADCAST_BROADCASTAsync(UserClass.userData.FSUSER_ID, Broadcast_Type == FormType.Normal ? "N" : "Y", dtStart, dtEnd);

            Broadcast_Client.fnGetTBBROADCAST_ADVANCE_FOR_SendBroadcastAsync(strStatus, strBroID, m_strType, m_strID, m_strEpisode, Broadcast_Type == FormType.Normal ? "N" : "Y", strUserID, strSDate, strEDate);
        }

        void Brocast_Client_fnGetTBBROADCAST_BROADCASTCompleted(object sender, fnGetTBBROADCAST_BROADCASTCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                PagedCollectionView pcv = new PagedCollectionView(e.Result);
                //指定DataGrid以及DataPager的內容
                DGBroListDouble.ItemsSource = pcv;
                DataPager.Source = pcv;
                BusyMsg.IsBusy = false;
            }
        }

        private void DGBroListDouble_RowDoubleClicked(object sender, DataGridDCTest.DataGridRowClickedArgs e)
        {
            STB100_01 _100_01 = new STB100_01((Class_BROADCAST)this.DGBroListDouble.SelectedItem, ActionType.View, Broadcast_Type);
            _100_01.Closed += (q, w) =>
            {
                if (_100_01.DialogResult == true)
                    Load_Broadcast();
            };
            _100_01.Show();
        }


        private void DetachedBtn_Click(object sender, RoutedEventArgs e)
        {
            this.DetachedBtn.IsEnabled = false;
            class_Broadcast = ((Class_BROADCAST)DGBroListDouble.SelectedItem);

            if (class_Broadcast != null)
            {
                //判斷是否為本人建立
                if (class_Broadcast.FSCREATED_BY != UserClass.userData.FSUSER_ID.ToString().Trim())
                {
                    MessageBox.Show("此登入帳號非此資料之申請帳號，無法抽單");
                    class_Broadcast = null;
                    return;
                }

                if (class_Broadcast.FCCHECK_STATUS == "N" || class_Broadcast.FCCHECK_STATUS == "R")
                {
                    Broadcast_Client.GetTBLOG_VIDEO_FileInfoList_BYBROIDAsync(class_Broadcast.FSBRO_ID);
                }
                else//轉檔、抽單
                {
                    MessageBox.Show("表單狀態為「" + class_Broadcast.FCCHECK_STATUS_NAME + "」無法抽單!!");
                    class_Broadcast = null;
                }
            }
            else
            {
                MessageBox.Show("請先選擇要抽單的送播單");
            }
            this.DetachedBtn.IsEnabled = true;

        }

        void Brocast_Client_GetTBLOG_VIDEO_FileInfoList_BYBROIDCompleted(object sender, GetTBLOG_VIDEO_FileInfoList_BYBROIDCompletedEventArgs e)
        {
            if (e.Result.Count > 0)
            {
                string[] fileArr = e.Result[0].Split(new char[] { ';' });
                if (fileArr[1] == "待轉檔")//FCFILE_STATUS=B
                {
                    Broadcast_Client.GetTBLOG_VIDEO_SEG_COUNTAsync(fileArr[0]);
                }
                else
                {
                    MessageBox.Show("檔案狀態為「" + fileArr[1] + "」無法抽單", "提示", MessageBoxButton.OK);
                    class_Broadcast = null;
                    DetachedBtn.IsEnabled = true;
                }
            }
        }

        void Brocast_Client_GetTBLOG_VIDEO_SEG_COUNTCompleted(object sender, GetTBLOG_VIDEO_SEG_COUNTCompletedEventArgs e)
        {
            List<Class_LOG_VIDEO_SEG> SEG_Result = e.Result;
            //bool flag = true;
            foreach (Class_LOG_VIDEO_SEG clvs in SEG_Result)
            {
                if (clvs.FCLOW_RES != "N")
                {
                    MessageBox.Show("此筆送帶轉檔單的檔案不為「待轉擋」，無法抽單", "提示", MessageBoxButton.OK);
                    this.DetachedBtn.IsEnabled = true;
                    class_Broadcast = null;
                    //flag = false;
                    return;
                }
            }

            //if (flag)
            //{
            if (ASK())
            {
                DeleteVideoDataMethod(SEG_Result[0].FSFILE_NO);
            }
            else
            {
                this.DetachedBtn.IsEnabled = true;
                class_Broadcast = null;
            }
            //}
        }

        bool ASK()
        {
            bool result = false;

            MessageBoxResult dialogResult = MessageBox.Show("確定抽單?", "確認視窗", MessageBoxButton.OKCancel);
            if (dialogResult == MessageBoxResult.OK)
            {
                result = true;
            }

            return result;
        }


        void DeleteVideoDataMethod(string _fsfile_no)
        {
            List<PTS_MAM3.WSDELETE.Class_DELETED> _class_deleted_list = new List<WSDELETE.Class_DELETED>();

            _class_deleted = new WSDELETE.Class_DELETED();
            _class_deleted._fsfile_no = _fsfile_no;//_deleted_file_no_list[i];
            _class_deleted._fsfile_type = "V";// this._class_deleted_parameter._fsfile_type;
            _class_deleted._fcstatus = "N";
            _class_deleted._fscreated_by = UserClass.userData.FSUSER_ID;
            _class_deleted._fdcreated_date = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
            _class_deleted._fsmemo = "刪除方式：抽單";
            _class_deleted_list.Add(_class_deleted);
            wsDeleteClient.DeleteFile_No_NewVideoIDAsync(_class_deleted_list);
        }

        void wsDeleteClient_Reply_Detached_ErrorCompleted(object sender, WSDELETE.Reply_Detached_ErrorCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {
                    MessageBox.Show("抽單過程發生錯誤，資料已回復!", "提示訊息", MessageBoxButton.OK);
                    _class_deleted = null;
                    class_Broadcast = null;
                    Load_Broadcast();
                    DetachedBtn.IsEnabled = true;
                }
            }
        }

        private void btnBroRefresh_Click(object sender, RoutedEventArgs e)
        {
            Load_Broadcast();
        }

        private void btnBroAdd_Click(object sender, RoutedEventArgs e)
        {
            STB100_01 _100_01 = new STB100_01(Broadcast_Type);
            _100_01.Closed += (s, args) =>
            {
                if (_100_01.DialogResult == true)
                {
                    Load_Broadcast();
                }
            };
            _100_01.Show();

        }

        void Brocast_Client_UPDATE_TBBROADCAST_Staus_DirectlyCompleted(object sender, UPDATE_TBBROADCAST_Staus_DirectlyCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                hd_mcClient.Delete_HD_MC_BY_BROIDAsync(class_Broadcast.FSBRO_ID);

            }
        }

        private void CancelMessage(bool Result)
        {
            this.DetachedBtn.IsEnabled = true;
            if (Result == true)
            {
                MessageBox.Show("抽單成功");
                Load_Broadcast();
            }
            else
            { MessageBox.Show("抽單失敗!"); }

        }

        private void btnBroQuery_Click(object sender, RoutedEventArgs e)
        {
            STB100_02 _100_02 = new STB100_02(false, Broadcast_Type == FormType.Normal ? "N" : "Y");
            _100_02.Closed += (s, args) =>
            {
                if (_100_02.DialogResult == true)
                {
                    m_strType = _100_02.m_strType;           //類型
                    m_strID = _100_02.m_strID;             //編碼
                    m_strEpisode = _100_02.m_strEpisode;        //集別
                    m_strCHANNELID = _100_02.m_strCHANNELID;      //頻道別   
                    strBroID = _100_02.strBroID;
                    strUserID = _100_02.strUserID;
                    strSDate = Convert.ToDateTime(_100_02.strSDate);
                    strEDate = Convert.ToDateTime(_100_02.strEDate);
                    strStatus = _100_02.strStatus;
                    Load_Broadcast();
                    //this.DGBroListDouble.ItemsSource = _100_02.m_ListFormData;
                }
            };
            _100_02.Show();
        }

        private void btBroModify_Click(object sender, RoutedEventArgs e)
        {
            Class_BROADCAST boradcast = (Class_BROADCAST)this.DGBroListDouble.SelectedItem;
            if (boradcast != null)
            {
                if (boradcast.FCCHECK_STATUS == "R")
                {
                    STB100_01 _100_01 = new STB100_01(boradcast, ActionType.Edit, Broadcast_Type);
                    _100_01.Closed += (s, args) =>
                    {
                        if (_100_01.DialogResult == true)
                        {
                            Load_Broadcast();
                        }
                    };
                    _100_01.Show();
                }
                else
                {
                    MessageBox.Show("送播單狀態為「" + boradcast.FCCHECK_STATUS_NAME + "」不可修改");
                }
            }
            else
            {
                MessageBox.Show("請先選擇要修改的送播單");

            }
        }

        private void TestBtn_Click(object sender, RoutedEventArgs e)
        {
           
            WSSTT.WSSTTSoapClient WSSTT_Client = new WSSTT.WSSTTSoapClient();

            WSSTT_Client.CallIngest_HDCompleted += new EventHandler<WSSTT.CallIngest_HDCompletedEventArgs>(WSSTT_Client_CallIngest_HDCompleted);

            WSSTT_Client.CallIngest_HDAsync("G201602600020003", "FILE", @"\\10.13.200.6\HDUpload\Test\00ATAK.mxf", "01:00:00;00", "01:00:20;00", @"\\HSM1\ptstest\Media\Lv\ptstv\2016\2016002\G201602600020003.mp4", "{Jarvis測試}", "HD");

        }

        void Broadcast_Client_TestMethodCompleted(object sender, TestMethodCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                MessageBox.Show(e.Result);
            }
            else 
            {
                MessageBox.Show(e.Error.ToString());
            }
        }

        string temp_JobID = "";
        void WSSTT_Client_CallIngest_HDCompleted(object sender, WSSTT.CallIngest_HDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    if (e.Result.StartsWith("E"))
                    {
                        MessageBox.Show("啟動轉檔失敗，原因：" + e.Result.Substring(2), "提示訊息", MessageBoxButton.OK);
                        // this.DialogResult = false;
                    }
                    else
                    {
                        //更新轉檔的JobID  
                        Broadcast_Client.UPDATE_TBLOG_VIDEO_JobIDCompleted += new EventHandler<UPDATE_TBLOG_VIDEO_JobIDCompletedEventArgs>(Broadcast_Client_UPDATE_TBLOG_VIDEO_JobIDCompleted);

                        temp_JobID = e.Result;
                        Broadcast_Client.UPDATE_TBLOG_VIDEO_JobIDAsync("G201602600020003", temp_JobID, "來源檔案名稱：Sequence 1_4CH.mov ", UserClass.userData.FSUSER_ID.ToString());
                    }
                }
                else
                {
                    MessageBox.Show("啟動轉檔失敗", "提示訊息", MessageBoxButton.OK);
                    //this.DialogResult = false;
                }
            }
        }

        void Broadcast_Client_UPDATE_TBLOG_VIDEO_JobIDCompleted(object sender, UPDATE_TBLOG_VIDEO_JobIDCompletedEventArgs e)
        {
           // Broadcast_Client.TestMethodAsync(temp_JobID);
        }


        public static string GetValuesToString(object ori_class) 
        {
            string strMessage = "";

            System.Reflection.PropertyInfo[] properties = ori_class.GetType().GetProperties();
            foreach (System.Reflection.PropertyInfo pi in properties)
            {
                object obj = pi.GetValue(ori_class, null);
                if (obj != null)
                    strMessage += pi.Name + ":" + obj.ToString() + Environment.NewLine;
            }

            return strMessage;
        }
    }
}
