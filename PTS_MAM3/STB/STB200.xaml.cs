﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSBROADCAST;
using System.Windows.Data;

namespace PTS_MAM3.STB
{
    public partial class STB200 : UserControl
    {
        WSBROADCAST.WSBROADCASTSoapClient Broadcast_Client = new WSBROADCAST.WSBROADCASTSoapClient();
        string status_filter = "";
        List<Class_BROADCAST> Broadcasts = null;

        public STB200()
        {
            InitializeComponent();

            Broadcast_Client.fnGetTBBROADCAST_ADVANCE_FOR_SendBroadcastCompleted += new EventHandler<WSBROADCAST.fnGetTBBROADCAST_ADVANCE_FOR_SendBroadcastCompletedEventArgs>(Broadcast_Client_fnGetTBBROADCAST_ADVANCE_FOR_SendBroadcastCompleted);

            this.dpBeg.SelectedDate = DateTime.Now.AddYears(-1);
            this.dpEnd.SelectedDate = DateTime.Now;
            Load_Broadcast();
        }

        void Broadcast_Client_fnGetTBBROADCAST_ADVANCE_FOR_SendBroadcastCompleted(object sender, WSBROADCAST.fnGetTBBROADCAST_ADVANCE_FOR_SendBroadcastCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                Broadcasts = e.Result;
                //if (status_filter != "")
                //   Broadcasts= Broadcasts.Where(a => status_filter.Contains(a.FCCHECK_STATUS)).ToList();
                PagedCollectionView pcv = new PagedCollectionView(Broadcasts);
                //指定DataGrid以及DataPager的內容
                DGBroListDouble.ItemsSource = pcv;
                DataPager.Source = pcv;
                BusyMsg.IsBusy = false;
            }
        }

        //void SimpleLoad()
        //{
        //    Broadcast_Client.fnGetTBBROADCAST_ADVANCE_FOR_SendBroadcastAsync(status_filter, "", "", "", "","", "", this.dpBeg.SelectedDate.Value, this.dpEnd.SelectedDate.Value.AddDays(1));
        //}

        private void DGBroListDouble_RowDoubleClicked(object sender, DataGridDCTest.DataGridRowClickedArgs e)
        {
            STB100_01 _100_01 = new STB100_01((Class_BROADCAST)this.DGBroListDouble.SelectedItem, ActionType.View, FormType.Post_Production);
            _100_01.Closed += (s, args) =>
            {
                if (_100_01.DialogResult == true)
                    Load_Broadcast();
            };
            _100_01.Show();
        }

        string m_strType = "";           //類型
        string m_strID = "";             //編碼
        string m_strEpisode = "";        //集別
        string m_strCHANNELID = "";      //頻道別   
        string strBroID = "";
        string strUserID = "";
        DateTime strSDate = DateTime.Now.AddMonths(-2);
        DateTime strEDate = DateTime.Now.AddDays(1);
        //string strStatus = "";

        private void btnQuery_Advance_Click(object sender, RoutedEventArgs e)
        {
            STB100_02 _100_02 = new STB100_02(true,"N");
            _100_02.Closed += (s, args) =>
            {
                if (_100_02.DialogResult == true)
                {
                    m_strType = _100_02.m_strType;           //類型
                    m_strID = _100_02.m_strID;             //編碼
                    m_strEpisode = _100_02.m_strEpisode;        //集別
                    m_strCHANNELID = _100_02.m_strCHANNELID;      //頻道別   
                    strBroID = _100_02.strBroID;
                    strUserID = "";
                    strSDate = Convert.ToDateTime(_100_02.strSDate);
                    strEDate = Convert.ToDateTime(_100_02.strEDate);
                    status_filter = _100_02.strStatus;
                    Load_Broadcast();
                    //Broadcasts = _100_02.m_ListFormData;
                    //this.DGBroListDouble.ItemsSource = Broadcasts;
                }
            };
            _100_02.Show();
        }

        void Load_Broadcast()
        {
            Broadcast_Client.fnGetTBBROADCAST_ADVANCE_FOR_SendBroadcastAsync(status_filter, strBroID, m_strType, m_strID, m_strEpisode, "", strUserID, strSDate, strEDate);
        }

        void Brocast_Client_fnGetTBBROADCAST_BROADCASTCompleted(object sender, fnGetTBBROADCAST_BROADCASTCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                Broadcasts = e.Result;
                PagedCollectionView pcv = new PagedCollectionView(Broadcasts);
                //指定DataGrid以及DataPager的內容
                DGBroListDouble.ItemsSource = pcv;
                DataPager.Source = pcv;
                BusyMsg.IsBusy = false;
            }
        }

        private void btnQuery_Fail_Click(object sender, RoutedEventArgs e)
        {
            status_filter = "R";
            Load_Broadcast();

        }

        private void btnQuery_NONINGEST_Click(object sender, RoutedEventArgs e)
        {
            status_filter = "N";
            Load_Broadcast();
        }

        private void btnQuery_Click(object sender, RoutedEventArgs e)
        {
            status_filter = "";
            m_strType = "";
            strBroID = "";
            m_strID = "";
            m_strEpisode = "";
            strUserID = "";
            strSDate = this.dpBeg.SelectedDate.Value;
            strEDate = this.dpEnd.SelectedDate.Value.AddDays(1);
            //Broadcast_Client.fnGetTBBROADCAST_ADVANCE_FOR_SendBroadcastAsync(status_filter, strBroID, m_strType, m_strID, m_strEpisode, "", strUserID, strSDate, strEDate);
            Load_Broadcast();
        }

        private void btnDone_Click(object sender, RoutedEventArgs e)
        {
            status_filter = "T;Y";
            Load_Broadcast();

        }

    }
}
