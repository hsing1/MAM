﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSCODE;
using System.Xml.Linq;
using System.IO;
using System.Text;
using PTS_MAM3.Code.PROGOBJ; 

namespace PTS_MAM3.Code.PROGOBJ
{
    public partial class Code_PROGOBJID_Query : ChildWindow
    {
        WSCODESoapClient client = new WSCODESoapClient();  //產生新的代理類別

        public Code_PROGOBJID_Query()
        {
            InitializeComponent();
            client.fnGetTBZPROGOBJ_BYNAMECompleted += new EventHandler<fnGetTBZPROGOBJ_BYNAMECompletedEventArgs>(client_fnGetTBZPROGOBJ_BYNAMECompleted);
        }

        void client_fnGetTBZPROGOBJ_BYNAMECompleted(object sender, fnGetTBZPROGOBJ_BYNAMECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    List<Class_PROGOBJ> FormData = new List<Class_PROGOBJ>();
                    byte[] byteArray = Encoding.Unicode.GetBytes(e.Result);

                    StringBuilder output = new StringBuilder();
                    // Create an XmlReader
                    XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                    var ltox = from s in doc.Elements("Datas").Elements("Data")
                               select new Class_PROGOBJ
                               {
                                   ID = (string)s.Element("FSPROGOBJID"),                           //製作目的代碼
                                   NAME = (string)s.Element("FSPROGOBJNAME"),                       //製作目的名稱
                                   SORT = (string)s.Element("FSSORT"),                              //排序號碼
                                   CREATED_BY = (string)s.Element("FSCREATED_BY"),                  //建檔者
                                   CREATED_BY_NAME = (string)s.Element("FSCREATED_BY_NAME"),        //建檔者名稱
                                   SHOW_CREATED_DATE = (string)s.Element("CONVERT_FDCREATED_DATE"),    //建檔日期
                                   UPDATED_BY = (string)s.Element("FSUPDATED_BY"),                  //修改者
                                   UPDATED_BY_NAME = (string)s.Element("FSUPDATED_BY_NAME"),        //修改者名稱
                                   SHOW_UPDATED_DATE = (string)s.Element("CONVERT_FDUPDATED_DATE"),    //修改日期        
                               };

                    foreach (Class_PROGOBJ obj in ltox)
                    {
                        FormData.Add(obj);
                    }

                    if (FormData.Count > 0)
                    {
                        for (int i = 0; i < FormData.Count; i++)
                        {
                            MessageBox.Show(FormData[i].NAME.ToString());
                        }
                    }

                    //關掉此頁
                    //this.DialogResult = true;


                }
            }
        }

      

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            client.fnGetTBZPROGOBJ_BYNAMEAsync(tbxPGMNAME.Text.ToString());
             




        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

