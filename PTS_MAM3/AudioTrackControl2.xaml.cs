﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3
{
    public partial class AudioTrackControl2 : UserControl
    {
        public AudioTrackControl2()
        {
            InitializeComponent();
        }

        public string GetAudioTrackSetting()
        {
            string track = "";
            for (int i = 0; i < this.LayoutRoot.Children.Count; i++)
            {
                int childrenCount = ((Grid)this.LayoutRoot.Children[i]).Children.Count;
                for (int j = 0; j < childrenCount; j++)
                {
                    IEnumerable<UIElement> result = ((StackPanel)((Grid)this.LayoutRoot.Children[i]).Children[j]).Children.Where(ui => ui is RadioButton && ((RadioButton)ui).IsChecked == true);
                    //IEnumerable<UIElement> result = ((StackPanel)this.LayoutRoot.Children[i]).Children.Where(ui => ui is RadioButton && ((RadioButton)ui).IsChecked == true);
                    if (result.Count() == 0)
                    {
                        return "";
                    }
                    else
                    {
                        track += ((RadioButton)result.FirstOrDefault()).Content.ToString();
                    }
                }
            }

            return track;
        }

        public void SetAudioTrackSetting(string trackSettin)
        {
            char[] tracks = trackSettin.ToArray();

            for (int i = 1; i <= tracks.Length; i++)
            {
                StackPanel SP = (StackPanel)this.FindName("SPTrack" + i);
                string track = tracks[i-1].ToString();
                if (SP != null)
                {
                    var reslut = SP.Children.Where(ui => ui is RadioButton && ((RadioButton)ui).Content.ToString() == track);

                    int count = reslut.Count();
                    if (count > 0)
                        ((RadioButton)reslut.FirstOrDefault()).IsChecked = true;
                }
            }

            //for (int i = 0; i < this.LayoutRoot.Children.Count; i++)
            //{
            //    Grid LRGrid = (Grid)this.LayoutRoot.Children[i];
            //    int childrenCount = LRGrid.Children.Count;

            //    for (int j = 0; j < childrenCount; j++)
            //    {

            //        StackPanel sp = (StackPanel)LRGrid.Children[j];
            //        IEnumerable<UIElement> result = sp.Children.Where(ui => ui is RadioButton && ((RadioButton)ui).Content.ToString() == tracks[i].ToString());

            //        //IEnumerable<UIElement> result = ((StackPanel)this.LayoutRoot.Children[i]).Children.Where(ui => ui is RadioButton && ((RadioButton)ui).Content.ToString() == tracks[i].ToString());
            //        if (result.Count() == 0)
            //        {
            //            continue;
            //        }
            //        else
            //        {
            //            ((RadioButton)result.FirstOrDefault()).IsChecked = true;
            //        }
            //    }
            //}

            this.UpdateLayout();
        }

        public void DiableControl()
        {
            for (int i = 0; i < this.LayoutRoot.Children.Count; i++)
            {
                int childrenCount = ((Grid)this.LayoutRoot.Children[i]).Children.Count;

                for (int j = 0; j < childrenCount; j++)
                {
                    List<UIElement> result = ((StackPanel)((Grid)this.LayoutRoot.Children[i]).Children[j]).Children.Where(ui => ui is RadioButton).ToList();

                    foreach (RadioButton rb in result)
                    {
                        rb.IsEnabled = false;
                    }
                }
            }
        }


    }
}
