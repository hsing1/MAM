﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSASPNetSession; 
//using PTS_MAM3.DFLoginSvc;

namespace PTS_MAM3
{
    public partial class LogingForm : ChildWindow
    {
        public LogingForm()
        {
            InitializeComponent();
            tbxFSUser_ID.Focus();     //焦點設在帳號欄位 
            
            Thickness m = new Thickness(0,180,0,0);
            this.Margin = m; 
            
        }


        private void showShortMessageBox(string title, Object content)
        {
            ChildWindow cw = new ChildWindow();
            cw.Title = title;

            ScrollViewer sv = new ScrollViewer();

            sv.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
            sv.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;

            sv.Content = content; 

            cw.Content = sv;
            
            cw.MaxWidth = 480; 
            cw.MaxHeight = 360; 
            cw.Show();


            cw.MouseLeftButtonDown += (s, args) =>
            {
                cw.Close();
               
            };
            cw.KeyDown += (s, args) =>
            {
                cw.Close();
                
            };
            return;
        }


        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            Login();
            //this.DialogResult = true;
        }

        private void Login()
        {
            
            //UserObjectSvc.UserObjectClient userObj = new UserObjectSvc.UserObjectClient();


            WSUserObject.WSUserObjectSoapClient userObj = new WSUserObject.WSUserObjectSoapClient(); 

            userObj.loginAsync(tbxFSUser_ID.Text, tbxFSPASSWD.Password);
            userObj.loginCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        if (args.Result == string.Empty)
                        {
                            //

                            WSASPNetSession.WSASPNetSessionSoapClient ASPSession = new WSASPNetSession.WSASPNetSessionSoapClient();


                            ASPSession.GetUserSessionCompleted += (s1, args1) =>
                            {
                                if (args1.Error == null)
                                {
                                    if (args1.Result != null)
                                    {
                                        List<WSASPNetSession.UserStruct> SessionList = args1.Result.ToList();
                                        //user = (ASPNetSessionSvc.UserStruct)(SessionList.FirstOrDefault());
                                        ///UserClass u = new UserClass();

                                        //if (UserClass.userData != null)
                                        //{
                                        //    MessageBox.Show(UserClass.userData.FSUSER_ID); 
                                        //}



                                      
                                        UserClass.userData = (WSASPNetSession.UserStruct)(SessionList.FirstOrDefault());

                                        ModuleClass.getModulePermissionByUserId(UserClass.userData.FSUSER_ID); 

                                    }
                                    //DeltaFlow的Login
                                    //FDLoginWSSoapClient DFLogin = new FDLoginWSSoapClient();
                                    //DFLogin.DFLoginAsync(tbxFSUser_ID.Text, tbxFSPASSWD.Password);

                                }
                                this.DialogResult = true;
                            };

                            ASPSession.GetUserSessionAsync("USEROBJ");

                            //帳號與密碼正確
                           
                        }
                        else
                        {
                            //登入時發生錯誤
                            tbxFSPASSWD.Password = ""; 
                            showShortMessageBox("錯誤", args.Result);
                           
                            //this.DialogResult = true;
                        }

                    }
                }

            };
            
        }



        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.tbxFSPASSWD.Password = string.Empty;
            this.tbxFSUser_ID.Text = string.Empty; 
        }

        private void tbxFSPASSWD_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Login();
            }

        }
    }
}