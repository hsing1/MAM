﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3
{
    public partial class AudioTrackControl : UserControl
    {
        public AudioTrackControl()
        {
            InitializeComponent();
        }

        public string GetAudioTrackSetting()
        {
            string track = "";
            for (int i = 0; i < this.LayoutRoot.Children.Count; i++)
            {
                IEnumerable<UIElement> result = ((StackPanel)this.LayoutRoot.Children[i]).Children.Where(ui => ui is RadioButton && ((RadioButton)ui).IsChecked == true);
                if (result.Count() == 0)
                {
                    return "";
                }
                else
                {
                    track += ((RadioButton)result.FirstOrDefault()).Content.ToString();
                }
            }
            //foreach (UIElement ui in this.SPTrack1.Children)
            //{
            //    if (ui is RadioButton)
            //    {
            //        RadioButton rb = (RadioButton)ui;
            //        if (rb.IsChecked == true)
            //        {
            //            track += rb.Content.ToString();
            //        }
            //    }
            //}

            //foreach (UIElement ui in this.SPTrack2.Children)
            //{
            //    if (ui is RadioButton)
            //    {
            //        RadioButton rb = (RadioButton)ui;
            //        if (rb.IsChecked == true)
            //        {
            //            track += rb.Content.ToString();
            //        }
            //    }
            //}
            //foreach (UIElement ui in this.SPTrack3.Children)
            //{
            //    if (ui is RadioButton)
            //    {
            //        RadioButton rb = (RadioButton)ui;
            //        if (rb.IsChecked == true)
            //        {
            //            track += rb.Content.ToString();
            //        }
            //    }
            //}

            //foreach (UIElement ui in this.SPTrack4.Children)
            //{
            //    if (ui is RadioButton)
            //    {
            //        RadioButton rb = (RadioButton)ui;
            //        if (rb.IsChecked == true)
            //        {
            //            track += rb.Content.ToString();
            //        }
            //    }
            //}
            return track;
        }


        public void SetAudioTrackSetting(string trackSettin)
        {
            char[] tracks = trackSettin.ToArray();

            for (int i = 0; i < this.LayoutRoot.Children.Count; i++)
            {
                IEnumerable<UIElement> result = ((StackPanel)this.LayoutRoot.Children[i]).Children.Where(ui => ui is RadioButton && ((RadioButton)ui).Content.ToString() == tracks[i].ToString());
                if (result.Count() == 0)
                {
                    continue;
                }
                else
                {
                    ((RadioButton)result.FirstOrDefault()).IsChecked = true;
                }
            }

        }

    }
}
