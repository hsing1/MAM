﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace PTS_MAM3.PGM
{

    public partial class PGM125_03 : ChildWindow
    {
        List<Promo_Plan_TIME_SEG_SET> Promo_Plan_TIME_SEG_SET_List = new List<Promo_Plan_TIME_SEG_SET>();

        List<Promo_Plan_Zone> Promo_Plan_Zone_List = new List<Promo_Plan_Zone>();
        public List<Date_Arr_Promo> Date_Arr_Promo_List = new List<Date_Arr_Promo>();
        public List<Date_Arr_Promo_Count> Date_Arr_Promo_Count_List = new List<Date_Arr_Promo_Count>();
        //宣傳帶可排播次數
        //public class Promo_Plan_Zone
        //{
        //    public string FSPROMO_ID { get; set; }  //宣傳帶編碼
        //    public string FSPROMO_NAME { get; set; }  //宣傳帶名稱
        //    public string FSPROG_ID { get; set; }   //節目編碼
        //    public string FSPROG_NAME { get; set; }   //節目名稱
        //    public string FNPROMO_TYPEA { get; set; }   //A時段有效次數
        //    public string FNPROMO_TYPEB { get; set; }   //B時段有效次數
        //    public string FNPROMO_TYPEC { get; set; }   //C時段有效次數
        //    public string FNPROMO_TYPED { get; set; }   //D時段有效次數
        //    public string FNPROMO_TYPEE { get; set; }   //E時段有效次數
        //    public string FSPLAY_COUNT_MAX { get; set; }   //最大次數
        //    public string FSPLAY_COUNT_MIN { get; set; }   //最小次數
        //    public string FNPROMO_INSERT_COUNT { get; set; }   //插入次數
        //}
        ////節目表插入的Promo
        //public class Date_Arr_Promo
        //{
        //    public string FSPROMO_ID { get; set; }  //宣傳帶編碼
        //    public string FSPROMO_NAME { get; set; }  //宣傳帶名稱
        //    public string FSPLAYTIME { get; set; }   //播出時間
        //}
        ////節目表插入的Promo
        //public class Date_Arr_Promo_Count
        //{
        //    public string FSPROMO_ID { get; set; }  //宣傳帶編碼
        //    public string FSPROMO_NAME { get; set; }  //宣傳帶名稱
        //    public string INSERT_COUNT { get; set; }   //插入次數
        //}

        public string _Date = "";
        public string _channelid = "";
        public string _week = "";
        public PGM125_03()
        {
            InitializeComponent();

        }

        public void QueryPlan()
        {

            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_TIME_SEG_SET = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";
            StringBuilder sb1 = new StringBuilder();
            sb1.AppendLine("<Data>");
            sb1.AppendLine("</Data>");
            SP_Q_TBPGM_TIME_SEG_SET.Do_QueryAsync("[SP_Q_TBPGM_TIME_SEG_SET]", sb1.ToString());
            SP_Q_TBPGM_TIME_SEG_SET.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {
                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);

                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            Promo_Plan_TIME_SEG_SET_List.Add(new Promo_Plan_TIME_SEG_SET()
                            {
                                FSTIME_SEG_TYPE = ShareFunction.XElementToString(elem.Element("FSTIME_SEG_TYPE")),
                                FSBEG_TIME = ShareFunction.XElementToString(elem.Element("FSBEG_TIME")),
                                FSEND_TIME = ShareFunction.XElementToString(elem.Element("FSEND_TIME")),
                            });

                        }

                    }


                    WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_PROMO_PLAN_BY_ID_DATE_CHANNEL = new WSPGMSendSQL.SendSQLSoapClient();

                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("<Datas>");

                    for (int j = 0; j <= Date_Arr_Promo_Count_List.Count - 1; j++)
                    {
                        sb.AppendLine("<Data>");
                        sb.AppendLine("<FSPROMO_ID>" + Date_Arr_Promo_Count_List[j].FSPROMO_ID + "</FSPROMO_ID>");
                        sb.AppendLine("<FDDATE>" + _Date + "</FDDATE>");
                        sb.AppendLine("<WEEK>" + _week + "</WEEK>");
                        sb.AppendLine("</Data>");
                    }

                    sb.AppendLine("</Datas>");
                    SP_Q_TBPGM_PROMO_PLAN_BY_ID_DATE_CHANNEL.Do_Query_Promo_Plan_SetAsync("SP_Q_TBPGM_PROMO_PLAN_BY_ID_DATE_CHANNEL", sb.ToString());
                    //SP_Q_TBPGM_QUEUE.Do_QueryCompleted += new EventHandler<SendSQL.Do_QueryCompletedEventArgs>(SP_Q_TBPGM_QUEUE_Do_QueryCompleted);
                    //AAA.Do_InsertAsync("", "", ReturnStr);
                    SP_Q_TBPGM_PROMO_PLAN_BY_ID_DATE_CHANNEL.Do_Query_Promo_Plan_SetCompleted += (s1, args1) =>
                    {
                        if (args1.Error == null)
                        {
                            if (args1.Result != null && args1.Result != "")
                            {

                                byte[] byteArray = Encoding.Unicode.GetBytes(args1.Result);
                                Promo_Plan_Zone_List.Clear();
                                StringBuilder output = new StringBuilder();
                                // Create an XmlReader
                                XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                                var ltox = from str in doc.Elements("Datas").Elements("Data")
                                           select str;
                                //foreach (XElement elem in ltox)
                                //{
                                //    int TypeACount = 0;
                                //    int TypeBCount = 0;
                                //    int TypeCCount = 0;
                                //    int TypeDCount = 0;
                                //    int TypeECount = 0;

                                //    for (int j = 0; j <= Date_Arr_Promo_List.Count - 1; j++)
                                //    {
                                //        if (Date_Arr_Promo_List[j].FSPROMO_ID == ShareFunction.XElementToString(elem.Element("FSPROMO_ID")))
                                //        {
                                //            for (int i = 0; i <= Promo_Plan_TIME_SEG_SET_List.Count - 1; i++)
                                //            {
                                //                int PlayTimeFrame = TransferTimecode.timecodetoframe(Date_Arr_Promo_List[j].FSPLAYTIME);
                                //                int BegTimeFrame = TransferTimecode.timecodetoframe(Promo_Plan_TIME_SEG_SET_List[i].FSBEG_TIME.Substring(0, 2) + ":" + Promo_Plan_TIME_SEG_SET_List[i].FSBEG_TIME.Substring(2, 2) + ":00;00");
                                //                int EndTimeFrame = TransferTimecode.timecodetoframe(Promo_Plan_TIME_SEG_SET_List[i].FSEND_TIME.Substring(0, 2) + ":" + Promo_Plan_TIME_SEG_SET_List[i].FSEND_TIME.Substring(2, 2) + ":00;00");

                                //                if (PlayTimeFrame >= BegTimeFrame && PlayTimeFrame < EndTimeFrame)
                                //                {
                                //                    if (Promo_Plan_TIME_SEG_SET_List[i].FSTIME_SEG_TYPE == "時段A")
                                //                        TypeACount = TypeACount + 1;
                                //                    else if (Promo_Plan_TIME_SEG_SET_List[i].FSTIME_SEG_TYPE == "時段B")
                                //                        TypeBCount = TypeBCount + 1;
                                //                    else if (Promo_Plan_TIME_SEG_SET_List[i].FSTIME_SEG_TYPE == "時段C")
                                //                        TypeCCount = TypeCCount + 1;
                                //                    else if (Promo_Plan_TIME_SEG_SET_List[i].FSTIME_SEG_TYPE == "時段D")
                                //                        TypeDCount = TypeDCount + 1;
                                //                    else if (Promo_Plan_TIME_SEG_SET_List[i].FSTIME_SEG_TYPE == "時段E")
                                //                        TypeECount = TypeECount + 1;
                                //                }
                                //            }
                                //        }
                                //    }

                                //    string INSERT_COUNT = "";
                                //    if (TypeACount != 0)
                                //        INSERT_COUNT = INSERT_COUNT + TypeACount.ToString() + "A";
                                //    if (TypeBCount != 0)
                                //        INSERT_COUNT = INSERT_COUNT + TypeBCount.ToString() + "B";
                                //    if (TypeCCount != 0)
                                //        INSERT_COUNT = INSERT_COUNT + TypeCCount.ToString() + "C";
                                //    if (TypeDCount != 0)
                                //        INSERT_COUNT = INSERT_COUNT + TypeDCount.ToString() + "D";
                                //    if (TypeECount != 0)
                                //        INSERT_COUNT = INSERT_COUNT + TypeECount.ToString() + "E";

                                //    Promo_Plan_Zone_List.Add(new Promo_Plan_Zone()
                                //    {
                                //        FSPROMO_ID = ShareFunction.XElementToString(elem.Element("FSPROMO_ID")),
                                //        FSPROMO_NAME = ShareFunction.XElementToString(elem.Element("FSPROMO_NAME")),
                                //        FSPROG_ID = ShareFunction.XElementToString(elem.Element("FSPROG_ID")),
                                //        FSPROG_NAME = ShareFunction.XElementToString(elem.Element("FSPROG_NAME")),
                                //        FNPROMO_TYPEA = ShareFunction.XElementToString(elem.Element("FNPROMO_TYPEA")),
                                //        FNPROMO_TYPEB = ShareFunction.XElementToString(elem.Element("FNPROMO_TYPEB")),
                                //        FNPROMO_TYPEC = ShareFunction.XElementToString(elem.Element("FNPROMO_TYPEC")),
                                //        FNPROMO_TYPED = ShareFunction.XElementToString(elem.Element("FNPROMO_TYPED")),
                                //        FNPROMO_TYPEE = ShareFunction.XElementToString(elem.Element("FNPROMO_TYPEE")),
                                //        FSPLAY_COUNT_MAX = ShareFunction.XElementToString(elem.Element("FSPLAY_COUNT_MAX")),
                                //        FSPLAY_COUNT_MIN = ShareFunction.XElementToString(elem.Element("FSPLAY_COUNT_MIN")),
                                //        FNPROMO_INSERT_COUNT = INSERT_COUNT

                                //    });
                                //}

                                for (int k = 0; k <= Date_Arr_Promo_Count_List.Count() - 1; k++)
                                {
                                    int TypeACount = 0;
                                    int TypeBCount = 0;
                                    int TypeCCount = 0;
                                    int TypeDCount = 0;
                                    int TypeECount = 0;
                                    int AllCount = 0;
                                    #region //計算時段使用次數
                                    for (int j = 0; j <= Date_Arr_Promo_List.Count - 1; j++)
                                    {
                                        if (Date_Arr_Promo_List[j].FSPROMO_ID == Date_Arr_Promo_Count_List[k].FSPROMO_ID)
                                        {
                                            AllCount = AllCount + 1;
                                            for (int i = 0; i <= Promo_Plan_TIME_SEG_SET_List.Count - 1; i++)
                                            {
                                                int PlayTimeFrame = TransferTimecode.timecodetoframe(Date_Arr_Promo_List[j].FSPLAYTIME);
                                                int BegTimeFrame = TransferTimecode.timecodetoframe(Promo_Plan_TIME_SEG_SET_List[i].FSBEG_TIME.Substring(0, 2) + ":" + Promo_Plan_TIME_SEG_SET_List[i].FSBEG_TIME.Substring(2, 2) + ":00;00");
                                                int EndTimeFrame = TransferTimecode.timecodetoframe(Promo_Plan_TIME_SEG_SET_List[i].FSEND_TIME.Substring(0, 2) + ":" + Promo_Plan_TIME_SEG_SET_List[i].FSEND_TIME.Substring(2, 2) + ":00;00");

                                                if (PlayTimeFrame >= BegTimeFrame && PlayTimeFrame < EndTimeFrame)
                                                {
                                                    if (Promo_Plan_TIME_SEG_SET_List[i].FSTIME_SEG_TYPE == "時段A")
                                                        TypeACount = TypeACount + 1;
                                                    else if (Promo_Plan_TIME_SEG_SET_List[i].FSTIME_SEG_TYPE == "時段B")
                                                        TypeBCount = TypeBCount + 1;
                                                    else if (Promo_Plan_TIME_SEG_SET_List[i].FSTIME_SEG_TYPE == "時段C")
                                                        TypeCCount = TypeCCount + 1;
                                                    else if (Promo_Plan_TIME_SEG_SET_List[i].FSTIME_SEG_TYPE == "時段D")
                                                        TypeDCount = TypeDCount + 1;
                                                    else if (Promo_Plan_TIME_SEG_SET_List[i].FSTIME_SEG_TYPE == "時段E")
                                                        TypeECount = TypeECount + 1;
                                                }
                                            }
                                        }
                                    }

                                    string INSERT_COUNT = "";
                                    if (TypeACount != 0)
                                        INSERT_COUNT = INSERT_COUNT + TypeACount.ToString() + "A";
                                    if (TypeBCount != 0)
                                        INSERT_COUNT = INSERT_COUNT + TypeBCount.ToString() + "B";
                                    if (TypeCCount != 0)
                                        INSERT_COUNT = INSERT_COUNT + TypeCCount.ToString() + "C";
                                    if (TypeDCount != 0)
                                        INSERT_COUNT = INSERT_COUNT + TypeDCount.ToString() + "D";
                                    if (TypeECount != 0)
                                        INSERT_COUNT = INSERT_COUNT + TypeECount.ToString() + "E";
                                    INSERT_COUNT = INSERT_COUNT + "共:" + AllCount.ToString();

                                    //計算次數完畢
                                    #endregion


                                    if (ltox.Count() > 0)
                                    {
                                        bool needadd = true;
                                        foreach (XElement elem in ltox)
                                        {
                                            if (ShareFunction.XElementToString(elem.Element("FSPROMO_ID")) == Date_Arr_Promo_Count_List[k].FSPROMO_ID)
                                            {
                                                Promo_Plan_Zone_List.Add(new Promo_Plan_Zone()
                                                {
                                                    FSPROMO_ID = ShareFunction.XElementToString(elem.Element("FSPROMO_ID")),
                                                    FSPROMO_NAME = ShareFunction.XElementToString(elem.Element("FSPROMO_NAME")),
                                                    FSPROG_ID = ShareFunction.XElementToString(elem.Element("FSPROG_ID")),
                                                    FSPROG_NAME = ShareFunction.XElementToString(elem.Element("FSPROG_NAME")),
                                                    FNPROMO_TYPEA = ShareFunction.XElementToString(elem.Element("FNPROMO_TYPEA")),
                                                    FNPROMO_TYPEB = ShareFunction.XElementToString(elem.Element("FNPROMO_TYPEB")),
                                                    FNPROMO_TYPEC = ShareFunction.XElementToString(elem.Element("FNPROMO_TYPEC")),
                                                    FNPROMO_TYPED = ShareFunction.XElementToString(elem.Element("FNPROMO_TYPED")),
                                                    FNPROMO_TYPEE = ShareFunction.XElementToString(elem.Element("FNPROMO_TYPEE")),
                                                    FSPLAY_COUNT_MAX = ShareFunction.XElementToString(elem.Element("FSPLAY_COUNT_MAX")),
                                                    FSPLAY_COUNT_MIN = ShareFunction.XElementToString(elem.Element("FSPLAY_COUNT_MIN")),
                                                    FNPROMO_INSERT_COUNT = INSERT_COUNT,
                                                    FNINSERT_TYPEA = TypeACount.ToString(),
                                                    FNINSERT_TYPEB = TypeBCount.ToString(),
                                                    FNINSERT_TYPEC = TypeCCount.ToString(),
                                                    FNINSERT_TYPED = TypeDCount.ToString(),
                                                    FNINSERT_TYPEE = TypeECount.ToString(),
                                                    FNINSERT_ALL = AllCount.ToString()
                                                });
                                                needadd = false;
                                            }
                                        }
                                        if (needadd == true)
                                        {
                                            Promo_Plan_Zone_List.Add(new Promo_Plan_Zone()
                                            {
                                                FSPROMO_ID = Date_Arr_Promo_Count_List[k].FSPROMO_ID,
                                                FSPROMO_NAME = Date_Arr_Promo_Count_List[k].FSPROMO_NAME,
                                                FSPROG_ID = "",
                                                FSPROG_NAME = "",
                                                FNPROMO_TYPEA = "",
                                                FNPROMO_TYPEB = "",
                                                FNPROMO_TYPEC = "",
                                                FNPROMO_TYPED = "",
                                                FNPROMO_TYPEE = "",
                                                FSPLAY_COUNT_MAX = "",
                                                FSPLAY_COUNT_MIN = "",
                                                FNPROMO_INSERT_COUNT = INSERT_COUNT,
                                                FNINSERT_TYPEA = TypeACount.ToString(),
                                                FNINSERT_TYPEB = TypeBCount.ToString(),
                                                FNINSERT_TYPEC = TypeCCount.ToString(),
                                                FNINSERT_TYPED = TypeDCount.ToString(),
                                                FNINSERT_TYPEE = TypeECount.ToString(),
                                                FNINSERT_ALL = AllCount.ToString()

                                            });
                                        }
                                    }
                                    else
                                    {
                                        Promo_Plan_Zone_List.Add(new Promo_Plan_Zone()
                                        {
                                            FSPROMO_ID = Date_Arr_Promo_Count_List[k].FSPROMO_ID,
                                            FSPROMO_NAME = Date_Arr_Promo_Count_List[k].FSPROMO_NAME,
                                            FSPROG_ID = "",
                                            FSPROG_NAME = "",
                                            FNPROMO_TYPEA = "",
                                            FNPROMO_TYPEB = "",
                                            FNPROMO_TYPEC = "",
                                            FNPROMO_TYPED = "",
                                            FNPROMO_TYPEE = "",
                                            FSPLAY_COUNT_MAX = "",
                                            FSPLAY_COUNT_MIN = "",
                                            FNPROMO_INSERT_COUNT = INSERT_COUNT,
                                            FNINSERT_TYPEA = TypeACount.ToString(),
                                            FNINSERT_TYPEB = TypeBCount.ToString(),
                                            FNINSERT_TYPEC = TypeCCount.ToString(),
                                            FNINSERT_TYPED = TypeDCount.ToString(),
                                            FNINSERT_TYPEE = TypeECount.ToString(),
                                            FNINSERT_ALL = AllCount.ToString()

                                        });
                                    }
                                }

                            }
                            DCdataGridPromoList.ItemsSource = null;
                            DCdataGridPromoList.ItemsSource = Promo_Plan_Zone_List;
                            //dataGridProgList.ItemsSource = PGM_ARR_PROMO;
                        }
                    };
                };


            };



        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        public List<Date_Arr_Promo> Date_Arr_Promo_List_Detail = new List<Date_Arr_Promo>();

        private void DCdataGridPromoList_RowClicked(object sender, DataGridDCTest.DataGridRowClickedArgs e)
        {
            int ProgInsertCount = 0;

            int TypeACount = 0;
            int TypeBCount = 0;
            int TypeCCount = 0;
            int TypeDCount = 0;
            int TypeECount = 0;
            //if (Promo_Plan_Zone_List[DCdataGridPromoList.SelectedIndex].FSPROG_ID!="")
            //  labelProgMessage.Content= "[" + Promo_Plan_Zone_List[DCdataGridPromoList.SelectedIndex].FSPROG_NAME + "]";

            Date_Arr_Promo_List_Detail.Clear();
            for (int i = 0; i <= Date_Arr_Promo_List.Count - 1; i++)
            {
                if (Promo_Plan_Zone_List[DCdataGridPromoList.SelectedIndex].FSPROG_ID != "")
                {
                }

                if (Date_Arr_Promo_List[i].FSPROMO_ID == ((Promo_Plan_Zone)DCdataGridPromoList.SelectedItems[0]).FSPROMO_ID)
                {
                    Date_Arr_Promo_List_Detail.Add(Date_Arr_Promo_List[i]);
                }
                //if (Date_Arr_Promo_List[i].FSPROMO_ID == Promo_Plan_Zone_List[DCdataGridPromoList.SelectedIndex].FSPROMO_ID)
                //{
                //    Date_Arr_Promo_List_Detail.Add(Date_Arr_Promo_List[i]);
                //}
            }

            if (Promo_Plan_Zone_List[DCdataGridPromoList.SelectedIndex].FSPROG_ID != "")
            {
                labelProgMessage.Content = "節目[" + Promo_Plan_Zone_List[DCdataGridPromoList.SelectedIndex].FSPROG_NAME + "]";
                labelProgMessage.Content = labelProgMessage.Content + "預約破口資源[";
                if (Promo_Plan_Zone_List[DCdataGridPromoList.SelectedIndex].FNPROMO_TYPEA != "0")
                    labelProgMessage.Content = labelProgMessage.Content + Promo_Plan_Zone_List[DCdataGridPromoList.SelectedIndex].FNPROMO_TYPEA + "A";
                if (Promo_Plan_Zone_List[DCdataGridPromoList.SelectedIndex].FNPROMO_TYPEB != "0")
                    labelProgMessage.Content = labelProgMessage.Content + Promo_Plan_Zone_List[DCdataGridPromoList.SelectedIndex].FNPROMO_TYPEB + "B";
                if (Promo_Plan_Zone_List[DCdataGridPromoList.SelectedIndex].FNPROMO_TYPEC != "0")
                    labelProgMessage.Content = labelProgMessage.Content + Promo_Plan_Zone_List[DCdataGridPromoList.SelectedIndex].FNPROMO_TYPEC + "C";
                if (Promo_Plan_Zone_List[DCdataGridPromoList.SelectedIndex].FNPROMO_TYPED != "0")
                    labelProgMessage.Content = labelProgMessage.Content + Promo_Plan_Zone_List[DCdataGridPromoList.SelectedIndex].FNPROMO_TYPED + "D";
                if (Promo_Plan_Zone_List[DCdataGridPromoList.SelectedIndex].FNPROMO_TYPEE != "0")
                    labelProgMessage.Content = labelProgMessage.Content + Promo_Plan_Zone_List[DCdataGridPromoList.SelectedIndex].FNPROMO_TYPEE + "E";
                if (Promo_Plan_Zone_List[DCdataGridPromoList.SelectedIndex].FSPLAY_COUNT_MAX != "0")
                    labelProgMessage.Content = labelProgMessage.Content + "最大:" + Promo_Plan_Zone_List[DCdataGridPromoList.SelectedIndex].FSPLAY_COUNT_MAX;
                if (Promo_Plan_Zone_List[DCdataGridPromoList.SelectedIndex].FSPLAY_COUNT_MIN != "0")
                    labelProgMessage.Content = labelProgMessage.Content + "最小:" + Promo_Plan_Zone_List[DCdataGridPromoList.SelectedIndex].FSPLAY_COUNT_MIN;


                labelProgMessage.Content = labelProgMessage.Content + "]";


                //開始計算所有屬於此節目的Promo已經排入次數
                for (int i = 0; i <= Promo_Plan_Zone_List.Count - 1; i++)
                {
                    if (Promo_Plan_Zone_List[i].FSPROG_ID == Promo_Plan_Zone_List[DCdataGridPromoList.SelectedIndex].FSPROG_ID)
                    {
                        TypeACount = TypeACount + int.Parse(Promo_Plan_Zone_List[i].FNINSERT_TYPEA);
                        TypeBCount = TypeBCount + int.Parse(Promo_Plan_Zone_List[i].FNINSERT_TYPEB);
                        TypeCCount = TypeCCount + int.Parse(Promo_Plan_Zone_List[i].FNINSERT_TYPEC);
                        TypeDCount = TypeDCount + int.Parse(Promo_Plan_Zone_List[i].FNINSERT_TYPED);
                        TypeECount = TypeECount + int.Parse(Promo_Plan_Zone_List[i].FNINSERT_TYPEE);
                        ProgInsertCount = ProgInsertCount + int.Parse(Promo_Plan_Zone_List[i].FNINSERT_ALL);

                    }
                }

                string INSERT_COUNT = "";
                if (TypeACount != 0)
                    INSERT_COUNT = INSERT_COUNT + TypeACount.ToString() + "A";
                if (TypeBCount != 0)
                    INSERT_COUNT = INSERT_COUNT + TypeBCount.ToString() + "B";
                if (TypeCCount != 0)
                    INSERT_COUNT = INSERT_COUNT + TypeCCount.ToString() + "C";
                if (TypeDCount != 0)
                    INSERT_COUNT = INSERT_COUNT + TypeDCount.ToString() + "D";
                if (TypeECount != 0)
                    INSERT_COUNT = INSERT_COUNT + TypeECount.ToString() + "E";
                INSERT_COUNT = INSERT_COUNT + "共:" + ProgInsertCount.ToString();


                labelProgMessage.Content = labelProgMessage.Content + "已插入狀態:" + INSERT_COUNT;
            }
            else
                labelProgMessage.Content = "";
            DCdataGridPromoListDetail.ItemsSource = null;
            DCdataGridPromoListDetail.ItemsSource = Date_Arr_Promo_List_Detail;
        }
    }
}

