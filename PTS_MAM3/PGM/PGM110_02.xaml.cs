﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Globalization;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace PTS_MAM3.PGM
{
    public partial class PGM110_02 : ChildWindow
    {
        public PGM110_02()
        {
            InitializeComponent();
        }

        public string _PromoBookingNO;
        public string ProcessId;
        public void QueryData()
        {
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_PROMO_BOOKING = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            //sb.AppendLine("<FDDATE>" + this.textBoxDate.Text + "</FDDATE>");
            sb.AppendLine("<FSPROMO_ID>" + "" + "</FSPROMO_ID>");
            sb.AppendLine("<FCSTATUS>" + "" + "</FCSTATUS>");
            sb.AppendLine("<FNPROMO_BOOKING_NO>" + _PromoBookingNO + "</FNPROMO_BOOKING_NO>");

            sb.AppendLine("</Data>");
            SP_Q_TBPGM_PROMO_BOOKING.Do_QueryAsync("SP_Q_TBPGM_PROMO_BOOKING", sb.ToString(), ReturnXML);

            SP_Q_TBPGM_PROMO_BOOKING.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {
                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            textBoxPromoID.Text = elem.Element("FSPROMO_ID").Value.ToString();
                            textBoxNewPromoName.Text = elem.Element("FSPROMO_NAME").Value.ToString();
                            textBoxPromoLength.Text = elem.Element("FSDURATION").Value.ToString();
                            textBoxMemo.Text = elem.Element("FSMEMO").Value.ToString();
                        }

                        //循序查詢Booking,Zone,SCH

                        WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_PROMO_EFFECTIVE_ZONE = new WSPGMSendSQL.SendSQLSoapClient();
                        StringBuilder sbZone = new StringBuilder();
                        sbZone.AppendLine("<Data>");
                        sbZone.AppendLine("<FNPROMO_BOOKING_NO>" + _PromoBookingNO + "</FNPROMO_BOOKING_NO>");
                        sbZone.AppendLine("</Data>");
                        SP_Q_TBPGM_PROMO_EFFECTIVE_ZONE.Do_QueryAsync("SP_Q_TBPGM_PROMO_EFFECTIVE_ZONE", sbZone.ToString(), ReturnXML);
                        SP_Q_TBPGM_PROMO_EFFECTIVE_ZONE.Do_QueryCompleted += (s1, args1) =>
                        {
                            if (args1.Error == null)
                            {
                                if (args1.Result != null && args1.Result != "")
                                {
                                    byte[] byteArray1 = Encoding.Unicode.GetBytes(args1.Result);
                                    StringBuilder output1 = new StringBuilder();
                                    // Create an XmlReader
                                    Promo_Zone_List.Clear();
                                    XDocument doc1 = XDocument.Load(new MemoryStream(byteArray1));
                                    var ltox1 = from str in doc1.Elements("Datas").Elements("Data")
                                                select str;
                                    foreach (XElement elem in ltox1)
                                    {

                                        //List<Promo_EFFECTIVE_ZONE> Promo_Zone_List = new List<Promo_EFFECTIVE_ZONE>();
                                        //List<Promo_SCHEDULED> Promo_Scheduled_List = new List<Promo_SCHEDULED>();
                                        Promo_Zone_List.Add(new Promo_EFFECTIVE_ZONE()
                                        {
                                            FNPROMO_BOOKING_NO = "",
                                            FSPROMO_ID = "",
                                            FNNO = elem.Element("FNNO").Value.ToString(),
                                            FDBEG_DATE = (Convert.ToDateTime(elem.Element("FDBEG_DATE").Value)).ToString("yyyy/MM/dd"),
                                            FDEND_DATE = (Convert.ToDateTime(elem.Element("FDEND_DATE").Value)).ToString("yyyy/MM/dd"),
                                            FSBEG_TIME = elem.Element("FSBEG_TIME").Value.ToString(),
                                            FSEND_TIME = elem.Element("FSEND_TIME").Value.ToString(),
                                            FCDATE_TIME_TYPE = elem.Element("FCDATE_TIME_TYPE").Value.ToString(),
                                            FSCHANNEL_ID = elem.Element("FSCHANNEL_ID").Value.ToString(),
                                            FSCHANNEL_NAME = elem.Element("FSCHANNEL_NAME").Value.ToString(),
                                            FSWEEK = elem.Element("FSWEEK").Value.ToString(),
                                        });
                                    }
                                    dataGridZone.ItemsSource = null;
                                    dataGridZone.ItemsSource = Promo_Zone_List;

                                    //循序查詢Booking,Zone,SCH
                                    WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_PROMO_BOOKING_SETTING = new WSPGMSendSQL.SendSQLSoapClient();
                                    StringBuilder sbSch = new StringBuilder();
                                    sbSch.AppendLine("<Data>");
                                    sbSch.AppendLine("<FNPROMO_BOOKING_NO>" + _PromoBookingNO + "</FNPROMO_BOOKING_NO>");
                                    sbSch.AppendLine("<FSPROMO_ID>" + "" + "</FSPROMO_ID>");
                                    sbSch.AppendLine("<FCSTATUS>" + "" + "</FCSTATUS>");
                                    sbSch.AppendLine("</Data>");
                                    SP_Q_TBPGM_PROMO_BOOKING_SETTING.Do_QueryAsync("SP_Q_TBPGM_PROMO_BOOKING_SETTING", sbSch.ToString(), ReturnXML);
                                    SP_Q_TBPGM_PROMO_BOOKING_SETTING.Do_QueryCompleted += (s2, args2) =>
                                    {
                                        if (args2.Error == null)
                                        {
                                            if (args2.Result != null && args2.Result != "")
                                            {
                                                byte[] byteArray2 = Encoding.Unicode.GetBytes(args2.Result);
                                                StringBuilder output2 = new StringBuilder();
                                                // Create an XmlReader
                                                Promo_Scheduled_List.Clear();
                                                XDocument doc2 = XDocument.Load(new MemoryStream(byteArray2));
                                                var ltox2 = from str in doc2.Elements("Datas").Elements("Data")
                                                            select str;
                                                foreach (XElement elem in ltox2)
                                                {
                                                    if (elem.Element("FSTIME1").Value.ToString() != "")
                                                    {
                                                        checkBoxTime1.IsChecked = true;
                                                        textBoxTime1.Text = elem.Element("FSTIME1").Value.ToString();
                                                    }
                                                    else
                                                    {
                                                        checkBoxTime1.IsChecked = false;
                                                        textBoxTime1.Text = "";
                                                    }
                                                    if (elem.Element("FSTIME2").Value.ToString() != "")
                                                    {
                                                        checkBoxTime2.IsChecked = true;
                                                        textBoxTime2.Text = elem.Element("FSTIME2").Value.ToString();
                                                    }
                                                    else
                                                    {
                                                        checkBoxTime2.IsChecked = false;
                                                        textBoxTime2.Text = "";
                                                    }
                                                    if (elem.Element("FSTIME3").Value.ToString() != "")
                                                    {
                                                        checkBoxTime3.IsChecked = true;
                                                        textBoxTime3.Text = elem.Element("FSTIME3").Value.ToString();
                                                    }
                                                    else
                                                    {
                                                        checkBoxTime3.IsChecked = false;
                                                        textBoxTime3.Text = "";
                                                    }
                                                    if (elem.Element("FSTIME4").Value.ToString() != "")
                                                    {
                                                        checkBoxTime4.IsChecked = true;
                                                        textBoxTime4.Text = elem.Element("FSTIME4").Value.ToString();
                                                    }
                                                    else
                                                    {
                                                        checkBoxTime4.IsChecked = false;
                                                        textBoxTime4.Text = "";
                                                    }
                                                    if (elem.Element("FSTIME5").Value.ToString() != "")
                                                    {
                                                        checkBoxTime5.IsChecked = true;
                                                        textBoxTime5.Text = elem.Element("FSTIME5").Value.ToString();
                                                    }
                                                    else
                                                    {
                                                        checkBoxTime5.IsChecked = false;
                                                        textBoxTime5.Text = "";
                                                    }
                                                    if (elem.Element("FSTIME6").Value.ToString() != "")
                                                    {
                                                        checkBoxTime6.IsChecked = true;
                                                        textBoxTime6.Text = elem.Element("FSTIME6").Value.ToString();
                                                    }
                                                    else
                                                    {
                                                        checkBoxTime6.IsChecked = false;
                                                        textBoxTime6.Text = "";
                                                    }
                                                    if (elem.Element("FSTIME7").Value.ToString() != "")
                                                    {
                                                        checkBoxTime7.IsChecked = true;
                                                        textBoxTime7.Text = elem.Element("FSTIME7").Value.ToString();
                                                    }
                                                    else
                                                    {
                                                        checkBoxTime7.IsChecked = false;
                                                        textBoxTime7.Text = "";
                                                    }
                                                    if (elem.Element("FSTIME8").Value.ToString() != "")
                                                    {
                                                        checkBoxTime8.IsChecked = true;
                                                        textBoxTime8.Text = elem.Element("FSTIME8").Value.ToString();
                                                    }
                                                    else
                                                    {
                                                        checkBoxTime8.IsChecked = false;
                                                        textBoxTime8.Text = "";
                                                    }

                                                }

                                            }
                                        }
                                    };
                                }
                            }
                        };


                    }
                }
            };

        }



        List<Promo_EFFECTIVE_ZONE> Promo_Zone_List = new List<Promo_EFFECTIVE_ZONE>();
        List<Promo_SCHEDULED> Promo_Scheduled_List = new List<Promo_SCHEDULED>();

        public bool CheckTime(string Instring)
        {
            int TempInt;
            if (int.TryParse(Instring, out TempInt) == false)
                return false;
            if (Instring.Length < 4)
                return false;

            if (int.Parse(Instring.Substring(2, 2)) > 60)
                return false;
            return true;
        }

        public DateTime TransDate(string InDateString)
        {
            DateTime parsed;
            DateTime.TryParseExact(InDateString, "yyyy/mm/dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out parsed);
            return parsed;
        }

 
        private void buttonReject_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void buttonClose_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
 
    }
}
