﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace PTS_MAM3
{
    public partial class PgmQueueModify : ChildWindow
    {
        public string _InSIgnal;
        public PgmQueueModify()
        {
            InitializeComponent();
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBZSIGNAL_ALL = new WSPGMSendSQL.SendSQLSoapClient();

            //string ReturnStr = "";

            StringBuilder sb1 = new StringBuilder();
            object ReturnXML1 = "";
            sb1.AppendLine("<Data>");
            sb1.AppendLine("</Data>");
            object ReturnXML = "";
            SP_Q_TBZSIGNAL_ALL.Do_QueryAsync("[SP_Q_TBZSIGNAL_ALL]", sb1.ToString(), ReturnXML);
            SP_Q_TBZSIGNAL_ALL.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {

                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);

                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            ComboBoxItem TempComboBoxItem = new ComboBoxItem();
                            TempComboBoxItem.Content = elem.Element("FSSIGNALNAME").Value.ToString();
                            //TempComboBoxItem.Tag = elem.Element("FSSIGNALNAME").Value.ToString();
                            comboBoxSignal1.Items.Add(TempComboBoxItem);

                            //TempSignalList.Add(elem.Element("FSSIGNALNAME").Value.ToString());
                        }

                        for (int Combocount = 0; Combocount < comboBoxSignal1.Items.Count; Combocount++)
                        {
                            if (((ComboBoxItem)comboBoxSignal1.Items[Combocount]).Content.ToString() == _InSIgnal)
                            {
                                comboBoxSignal1.SelectedIndex = Combocount;
                            }
                        }
                        //SignalList = TempSignalList;
                    }
                    else
                    {
                    }
                }

            };
        }
        public PgmQueueDate ModifyQueue=new PgmQueueDate();
        public string WorkChannelID;
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (textBoxDefSeg.Text == "")
            {
                MessageBox.Show("請輸入預設段數");
                return;
            }
            if (textBoxDefMin.Text == "")
            {
                MessageBox.Show("請輸入預設分鐘");
                return;
            }
            if (textBoxDefSec.Text == "")
            {
                MessageBox.Show("請輸入預設秒數");
                return;
            }
            if (comboBoxLive.SelectedIndex == -1)
            {
                MessageBox.Show("請選擇Live種類");
                return;
            }

            
            try
            {
                int.Parse(textBoxDefSeg.Text);
                int.Parse(textBoxDefMin.Text);
                int.Parse(textBoxDefSec.Text);
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("預設段數或預設長度格式不正確");
                return;
            }
            ModifyQueue.ProgID = textBoxProgID.Text;
            ModifyQueue.Seqno = textBoxProgSeqno.Text;
            ModifyQueue.ProgName = textBoxProgName.Text;
            ModifyQueue.ProgDate = textBoxDate.Text;
            ModifyQueue.CHANNEL_ID = textBoxChannelID.Text;
            ModifyQueue.CHANNEL_NAME = textBoxChannelName.Text;
            ModifyQueue.BEG_TIME = textBoxBTime.Text;
            ModifyQueue.END_TIME = textBoxETime.Text;
            ModifyQueue.REPLAY = TextBoxReplay.Text;
            ModifyQueue.EPISODE = textBoxEpisode.Text;
            ModifyQueue.LIVE = ((ComboBoxItem)comboBoxLive.SelectedItem).Tag.ToString();
            if (comboBoxSignal1.SelectedIndex == -1)
                ModifyQueue.SIGNAL = "";
            else
                ModifyQueue.SIGNAL = ((ComboBoxItem)comboBoxSignal1.SelectedItem).Content.ToString();
            ModifyQueue.DEP = textBoxDep.Text;
            ModifyQueue.ONOUT = textBoxONOUT.Text;
            ModifyQueue.MEMO = textBoxMemo.Text;
            ModifyQueue.MEMO1 = textBoxMemo1.Text;
            ModifyQueue.DEF_MIN = textBoxDefMin.Text;
            ModifyQueue.DEF_SEC = textBoxDefSec.Text;


            ModifyQueue.ProgDefSeg = textBoxDefSeg.Text;
            this.DialogResult = true;
            //MessageBox.Show("修改完成");

            //WSPGMSendSQL.SendSQLSoapClient SP_U_TBPGM_BANK_DETAIL_SEG_DUR = new WSPGMSendSQL.SendSQLSoapClient();
            //StringBuilder sb = new StringBuilder();
            //object ReturnXML = "";

            //sb.AppendLine("<Data>");

            //    sb.AppendLine("<FSPROG_ID>" + this.textBoxProgID.Text + "</FSPROG_ID>");
            //    sb.AppendLine("<FNSEQNO>" + this.textBoxProgSeqno.Text + "</FNSEQNO>");
            //    sb.AppendLine("<FSPROG_NAME>" +  TransferTimecode.ReplaceXML(this.textBoxProgName.Text) + "</FSPROG_NAME>");
            //    sb.AppendLine("<FNSEG>" + this.textBoxDefSeg.Text + "</FNSEG>");
            //    sb.AppendLine("<FNDUR>" + this.textBoxDefMin.Text + "</FNDUR>");
            //    sb.AppendLine("<FSMEMO>" +  TransferTimecode.ReplaceXML(this.textBoxMemo.Text) + "</FSMEMO>");
            //    sb.AppendLine("<FSMEMO1>" + TransferTimecode.ReplaceXML(this.textBoxMemo1.Text) + "</FSMEMO1>");
            //    sb.AppendLine("<FSPLAY_TYPE>" + ((ComboBoxItem)comboBoxLive.SelectedItem).Tag.ToString() + "</FSPLAY_TYPE>");
            //    //sb.AppendLine("<FNLIVE>" + ((ComboBoxItem)comboBoxLive.SelectedItem).Tag.ToString() + "</FNLIVE>");

            //    sb.AppendLine("<FSUPDATED_BY>" + UserClass.userData.FSUSER_ID + "</FSUPDATED_BY>");
            //    sb.AppendLine("</Data>");

            ////sb=sb.Replace("\r\n", "");
            //SP_U_TBPGM_BANK_DETAIL_SEG_DUR.Do_InsertAsync("SP_U_TBPGM_BANK_DETAIL_SEG_DUR", sb.ToString());
            //SP_U_TBPGM_BANK_DETAIL_SEG_DUR.Do_InsertCompleted += (s, args) =>
            //{
            //    if (args.Error == null)
            //    {

            //        if (args.Result == true)
            //        {
            //            ModifyQueue.ProgID = textBoxProgID.Text;
            //            ModifyQueue.Seqno = textBoxProgSeqno.Text;
            //            ModifyQueue.ProgName = textBoxProgName.Text;
            //            ModifyQueue.ProgDate = textBoxDate.Text;
            //            ModifyQueue.CHANNEL_ID = textBoxChannelID.Text;
            //            ModifyQueue.CHANNEL_NAME = textBoxChannelName.Text;
            //            ModifyQueue.BEG_TIME = textBoxBTime.Text;
            //            ModifyQueue.END_TIME = textBoxETime.Text;
            //            ModifyQueue.REPLAY = TextBoxReplay.Text;
            //            ModifyQueue.EPISODE = textBoxEpisode.Text;
            //            ModifyQueue.LIVE = ((ComboBoxItem)comboBoxLive.SelectedItem).Tag.ToString();
            //            if (comboBoxSignal1.SelectedIndex == -1)
            //                ModifyQueue.SIGNAL = "";
            //            else
            //                ModifyQueue.SIGNAL = ((ComboBoxItem)comboBoxSignal1.SelectedItem).Content.ToString();
            //            ModifyQueue.DEP = textBoxDep.Text;
            //            ModifyQueue.ONOUT = textBoxONOUT.Text;
            //            ModifyQueue.MEMO = textBoxMemo.Text;
            //            ModifyQueue.MEMO1 = textBoxMemo1.Text;
            //            ModifyQueue.DEF_MIN = textBoxDefMin.Text;
            //            ModifyQueue.DEF_SEC = textBoxDefSec.Text;


            //            ModifyQueue.ProgDefSeg = textBoxDefSeg.Text;
            //            this.DialogResult = true;
            //            MessageBox.Show("修改完成");
            //        }
            //        else
            //            MessageBox.Show("修改失敗");
            //    }
            //};           
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void buttonSetLouthKey_Click(object sender, RoutedEventArgs e)
        {
            PGM.PGM100_02 SetLouthKey_Frm = new PGM.PGM100_02();

            SetLouthKey_Frm.InLouthKeyString = textBoxMemo.Text;
            //SetLouthKey_Frm.AnalyLouthKeyString(textBoxMemo.Text);
            SetLouthKey_Frm.WorkChannelID = WorkChannelID;
            SetLouthKey_Frm.Show();
            SetLouthKey_Frm.Closed += (s, args) =>
            {
                if (SetLouthKey_Frm.OutLouthKeyString != null)
                {
                    textBoxMemo.Text = SetLouthKey_Frm.OutLouthKeyString;
                }

            };
        }

        private void buttonUpdateBankDetailSEG_Click(object sender, RoutedEventArgs e)
        {
            if (textBoxDefSeg.Text == "")
            {
                MessageBox.Show("請輸入預設段數");
                return;
            }
            if (textBoxDefMin.Text == "")
            {
                MessageBox.Show("請輸入預設分鐘");
                return;
            }
            if (textBoxDefSec.Text == "")
            {
                MessageBox.Show("請輸入預設秒數");
                return;
            }
            

            try
            {
                int.Parse(textBoxDefSeg.Text);
                int.Parse(textBoxDefMin.Text);
                int.Parse(textBoxDefSec.Text);

            }
            catch (Exception ex)
            {
                MessageBox.Show("預設段數或預設長度格式不正確");
                return;
            }


            WSPGMSendSQL.SendSQLSoapClient SP_U_TBPGM_BANK_DETAIL_SEG_DUR = new WSPGMSendSQL.SendSQLSoapClient();
            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";

            sb.AppendLine("<Data>");

            sb.AppendLine("<FSPROG_ID>" + this.textBoxProgID.Text + "</FSPROG_ID>");
            sb.AppendLine("<FNSEQNO>" + this.textBoxProgSeqno.Text + "</FNSEQNO>");
            sb.AppendLine("<FSPROG_NAME>" + TransferTimecode.ReplaceXML(this.textBoxProgName.Text) + "</FSPROG_NAME>");
            sb.AppendLine("<FNSEG>" + this.textBoxDefSeg.Text + "</FNSEG>");
            sb.AppendLine("<FNDUR>" + this.textBoxDefMin.Text + "</FNDUR>");
            sb.AppendLine("<FNSEC>" + this.textBoxDefSec.Text + "</FNSEC>");
            sb.AppendLine("<FSSIGNAL>" + ((ComboBoxItem)comboBoxSignal1.SelectedItem).Content.ToString() + "</FSSIGNAL>");
            sb.AppendLine("<FSMEMO>" + TransferTimecode.ReplaceXML(this.textBoxMemo.Text) + "</FSMEMO>");
            sb.AppendLine("<FSMEMO1>" + TransferTimecode.ReplaceXML(this.textBoxMemo1.Text) + "</FSMEMO1>");
            sb.AppendLine("<FSPLAY_TYPE>" + ((ComboBoxItem)comboBoxLive.SelectedItem).Tag.ToString() + "</FSPLAY_TYPE>");
            //sb.AppendLine("<FNLIVE>" + ((ComboBoxItem)comboBoxLive.SelectedItem).Tag.ToString() + "</FNLIVE>");

            sb.AppendLine("<FSUPDATED_BY>" + UserClass.userData.FSUSER_ID + "</FSUPDATED_BY>");
            sb.AppendLine("</Data>");

            //sb=sb.Replace("\r\n", "");
            SP_U_TBPGM_BANK_DETAIL_SEG_DUR.Do_InsertAsync("SP_U_TBPGM_BANK_DETAIL_SEG_DUR", sb.ToString());
            SP_U_TBPGM_BANK_DETAIL_SEG_DUR.Do_InsertCompleted += (s, args) =>
            {
                if (args.Error == null)
                {

                    if (args.Result == true)
                    {
                        
                        MessageBox.Show("修改完成");
                    }
                    else
                        MessageBox.Show("修改失敗");
                }
            };          
        }
    }
}

