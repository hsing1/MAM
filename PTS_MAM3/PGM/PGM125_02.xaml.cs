﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace PTS_MAM3.PGM
{
    public partial class PGM125_02 : ChildWindow
    {
        public PGM125_02()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
        List<Promo_SCHEDULED_List> Promo_Scheduled_List = new List<Promo_SCHEDULED_List>();
        string _Date;
        string _channel;
        public void QueryDateSchPromo()
        {

            //循序查詢SCH
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_PROMO_SCHEDULED = new WSPGMSendSQL.SendSQLSoapClient();
            StringBuilder sbSch = new StringBuilder();
            sbSch.AppendLine("<Data>");
            sbSch.AppendLine("<FDDATE>" + _Date + "</FDDATE>");
            sbSch.AppendLine("<FSCHANNEL_ID>" + _channel + "</FSCHANNEL_ID>");

            sbSch.AppendLine("</Data>");
            SP_Q_TBPGM_PROMO_SCHEDULED.Do_QueryAsync("SP_Q_TBPGM_PROMO_SCHEDULED_BY_CHANNEL_DATE", sbSch.ToString());
            SP_Q_TBPGM_PROMO_SCHEDULED.Do_QueryCompleted += (s2, args2) =>
            {
                if (args2.Error == null)
                {
                    if (args2.Result != null)
                    {
                        byte[] byteArray2 = Encoding.Unicode.GetBytes(args2.Result);
                        StringBuilder output2 = new StringBuilder();
                        // Create an XmlReader
                        Promo_Scheduled_List.Clear();
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray2));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            Promo_Scheduled_List.Add(new Promo_SCHEDULED_List()
                            {
                                FNPROMO_BOOKING_NO = elem.Element("FNPROMO_BOOKING_NO").Value.ToString(),
                                FSPROMO_ID = elem.Element("FSPROMO_ID").Value.ToString(),
                                FNNO = elem.Element("FNNO").Value.ToString(),
                                FDDATE = elem.Element("FDDATE").Value.ToString(),
                                FSTIME = elem.Element("FSTIME").Value.ToString(),
                                FCSTATUS = elem.Element("FCSTATUS").Value.ToString(),
                                FSCHANNEL_ID = elem.Element("FSCHANNEL_ID").Value.ToString(),
                                FSCHANNEL_NAME = elem.Element("FSCHANNEL_NAME").Value.ToString(),
                                FSPROMO_NAME = elem.Element("FSPROMO_NAME").Value.ToString(),
                                FSDURATION = elem.Element("FSDURATION").Value.ToString(),

                            });
                        }
                        DCdataGridPromoList.ItemsSource = null;
                        DCdataGridPromoList.ItemsSource = Promo_Scheduled_List;
                        //循序查詢Booking,Zone,SCH

                    }
                }
            };


            //        @FSCHANNEL_ID	varchar(2),
            //@FDDATE datetime
            //As
            //select A.FNPROMO_BOOKING_NO,A.FSPROMO_ID,A.FNNO,
            //A.FDDATE,A.FSCHANNEL_ID,A.FSTIME,A.FCSTATUS,B.FSCHANNEL_NAME ,C.FSPROMO_NAME,C.FSDURATION
        }

        private void ButtonChooseImport_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ButtonAllImport_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}

