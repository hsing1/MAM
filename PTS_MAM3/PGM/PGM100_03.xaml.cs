﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace PTS_MAM3
{
    public partial class PGM100_03 : ChildWindow
    {
        public PGM100_03()
        {
            InitializeComponent();
        }
        public string _TapeNO;
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            //Do_Insert_List
            if (GETQueryProgFile.Count == 0)
            {
                MessageBox.Show("請加入救命帶檔案");
                return;
            }
            WSPGMSendSQL.SendSQLSoapClient SP_I_TBPGM_SAVE_TAPE_LIST = new WSPGMSendSQL.SendSQLSoapClient();
            StringBuilder sb = new StringBuilder();
            StringBuilder sbDel = new StringBuilder();
            sbDel.AppendLine("<Data>");
            sbDel.AppendLine("<FSPROG_ID>" + textBoxProgID.Text + "</FSPROG_ID>");
            sbDel.AppendLine("<FNSEQNO>" + textBoxProgSeqno.Text + "</FNSEQNO>");
            sbDel.AppendLine("<FSCHANNEL_ID>" + textBoxChannelID.Text + "</FSCHANNEL_ID>");
            sbDel.AppendLine("<FDDATE>" + textBoxDate.Text + "</FDDATE>");
            sbDel.AppendLine("</Data>");


            int Save_NO=1;
            object ReturnXML = "";
            sb.AppendLine("<Datas>");
            foreach (QueryProgFile TempQueue in GETQueryProgFile)
            {
                sb.AppendLine("<Data>");
                sb.AppendLine("<FSPROG_ID>" + textBoxProgID.Text + "</FSPROG_ID>");
                sb.AppendLine("<FNSEQNO>" + textBoxProgSeqno.Text + "</FNSEQNO>");
                sb.AppendLine("<FSCHANNEL_ID>" + textBoxChannelID.Text + "</FSCHANNEL_ID>");
                sb.AppendLine("<FDDATE>" + textBoxDate.Text + "</FDDATE>");
                sb.AppendLine("<FSBEG_TIME>" + textBoxBTime.Text + "</FSBEG_TIME>");
                sb.AppendLine("<FSEND_TIME>" + textBoxETime.Text + "</FSEND_TIME>");
                sb.AppendLine("<FSPROG_NAME>" + textBoxProgName.Text + "</FSPROG_NAME>");
                sb.AppendLine("<FNEPISODE>" + textBoxEpisode.Text + "</FNEPISODE>");
                sb.AppendLine("<FSTAPENO>" + _TapeNO + "</FSTAPENO>");

                sb.AppendLine("<FNSAVE_NO>" + Save_NO + "</FNSAVE_NO>");
                Save_NO = Save_NO + 1;
                sb.AppendLine("<FSSAVE_PROG_ID>" + TempQueue.FSPROG_ID + "</FSSAVE_PROG_ID>");
                sb.AppendLine("<FNSAVE_EPISODE>" + TempQueue.FNEPISODE + "</FNSAVE_EPISODE>");
                sb.AppendLine("<FSSAVE_TAPENO>" + TempQueue.FSFILE_NO + "</FSSAVE_TAPENO>");
                sb.AppendLine("<FSSAVE_LENGTH>" + TempQueue.FSLENGTH + "</FSSAVE_LENGTH>");
                sb.AppendLine("<FSCREATED_BY>" + "MIKE" + "</FSCREATED_BY>");
                sb.AppendLine("</Data>");

            }
            sb.AppendLine("</Datas>");
            //sb=sb.Replace("\r\n", "");
            SP_I_TBPGM_SAVE_TAPE_LIST.Do_Insert_ListAsync("SP_D_TBPGM_SAVE_TAPE_LIST", sbDel.ToString(), "SP_I_TBPGM_SAVE_TAPE_LIST", sb.ToString());
            SP_I_TBPGM_SAVE_TAPE_LIST.Do_Insert_ListCompleted += (s, args) =>
            {
                if (args.Error == null)
                {

                    if (args.Result == true)
                        MessageBox.Show("新增成功");
                    else
                        MessageBox.Show("新增失敗");
                }
            };           
            
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        public void QueryData()
        {
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_SAVE_TAPE_LIST = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");

            sb.AppendLine("<FSPROG_ID>" + textBoxProgID.Text + "</FSPROG_ID>");
            sb.AppendLine("<FNSEQNO>" + textBoxProgSeqno.Text + "</FNSEQNO>");
            sb.AppendLine("<FSCHANNEL_ID>" + textBoxChannelID.Text + "</FSCHANNEL_ID>");
            sb.AppendLine("<FDDATE>" + textBoxDate.Text + "</FDDATE>");

            sb.AppendLine("</Data>");
            SP_Q_TBPGM_SAVE_TAPE_LIST.Do_QueryAsync("SP_Q_TBPGM_SAVE_TAPE_LIST", sb.ToString(), ReturnXML);
            SP_Q_TBPGM_SAVE_TAPE_LIST.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {

                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        GETQueryProgFile.Clear();
                        StringBuilder output = new StringBuilder();
                        if (byteArray.Count() == 0)
                        {
                            return;
                        }
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {

                            GETQueryProgFile.Add(new QueryProgFile()
                            {

                                FSPROG_ID = elem.Element("FSSAVE_PROG_ID").Value.ToString(),
                                FNEPISODE = elem.Element("FNSAVE_EPISODE").Value.ToString(),
                                FSPGDNAME = elem.Element("FSPGDNAME").Value.ToString(),
                                FSFILE_NO = elem.Element("FSSAVE_TAPENO").Value.ToString(),
                                FSLENGTH = elem.Element("FSSAVE_LENGTH").Value.ToString(),
                                FCLOW_RES = ""
                            });
                        }
                        dataGridSaveList.ItemsSource = GETQueryProgFile;



                    }
                }
            };           
        }



        public string _ChannelFileType;
        List<QueryProgFile> GETQueryProgFile = new List<QueryProgFile>();
        private void buttonAdd_Click(object sender, RoutedEventArgs e)
        {
            PGM100_04 PGM100_04_Frm = new PGM100_04();
            PGM100_04_Frm._ChannelFileType = _ChannelFileType;
            PGM100_04_Frm.Show();
            PGM100_04_Frm.Closed += (s, args) =>
            {
                GETQueryProgFile.Add(new QueryProgFile()
                {
                    FSPROG_ID = PGM100_04_Frm._FSPROG_ID,
                    FNEPISODE = PGM100_04_Frm._FNEPISODE,
                    FSPGDNAME = PGM100_04_Frm._FSPGDNAME,
                    FSFILE_NO = PGM100_04_Frm._FSFILE_NO,
                    FSLENGTH = PGM100_04_Frm._FSLENGTH,
                    //FSBEG_TIMECODE = elem.Element("FSBEG_TIMECODE").Value.ToString(),
                    //FSEND_TIMECODE = elem.Element("FSEND_TIMECODE").Value.ToString(),
                    FCLOW_RES = PGM100_04_Frm._FCLOW_RES
                });
                dataGridSaveList.ItemsSource = null;
                dataGridSaveList.ItemsSource = GETQueryProgFile;
            };
        }

        private void buttonRemove_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridSaveList.SelectedIndex < 0)
            {
                MessageBox.Show("請先選取節目資料");
                return;
            }
            GETQueryProgFile.RemoveAt(dataGridSaveList.SelectedIndex);
            //GETQueryProgFile[dataGridSaveList.SelectedIndex].ProgID = PGMQUEUEMODIFY_Frm.ModifyQueue.ProgID;
        }
    }
}

