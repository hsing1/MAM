﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Globalization;
using System.Text;
using System.Xml.Linq;
using System.IO;
using PTS_MAM3.FlowMAM;

namespace PTS_MAM3.PGM
{
    public partial class PGM110_01 : ChildWindow
    {
        public PGM110_01()
        {
            InitializeComponent();
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBZCHANNEL_ALL = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            sb.AppendLine("</Data>");
            SP_Q_TBZCHANNEL_ALL.Do_QueryAsync("[SP_Q_TBZCHANNEL_ALL]", sb.ToString(), ReturnXML);
            //SP_Q_TBPGM_QUEUE.Do_QueryCompleted += new EventHandler<SendSQL.Do_QueryCompletedEventArgs>(SP_Q_TBPGM_QUEUE_Do_QueryCompleted);
            //AAA.Do_InsertAsync("", "", ReturnStr);
            SP_Q_TBZCHANNEL_ALL.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {


                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);

                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            //if (ModuleClass.getModulePermission("P3" + elem.Element("ID").Value.ToString() + "001") == true)
                            //{
                                ComboBoxItem TempComboBoxItem = new ComboBoxItem();
                                TempComboBoxItem.Content = elem.Element("NAME").Value.ToString();
                                TempComboBoxItem.Tag = elem.Element("ID").Value.ToString();

                                comboBoxChannel.Items.Add(TempComboBoxItem);


                            //}
                        }
                    }
                    QueryData();
                }
            };

        }

        public string _PromoBookingNO;
        public int intProcessID;
        public int intFlowID;

        public void QueryData()
        {
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_PROMO_BOOKING = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            //sb.AppendLine("<FDDATE>" + this.textBoxDate.Text + "</FDDATE>");
            sb.AppendLine("<FSPROMO_ID>" + "" + "</FSPROMO_ID>");
            sb.AppendLine("<FCSTATUS>" + "" + "</FCSTATUS>");
            sb.AppendLine("<FNPROMO_BOOKING_NO>" + _PromoBookingNO + "</FNPROMO_BOOKING_NO>");

            sb.AppendLine("</Data>");
            SP_Q_TBPGM_PROMO_BOOKING.Do_QueryAsync("SP_Q_TBPGM_PROMO_BOOKING", sb.ToString(), ReturnXML);

            SP_Q_TBPGM_PROMO_BOOKING.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {
                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            textBoxPromoID.Text = elem.Element("FSPROMO_ID").Value.ToString();
                            textBoxNewPromoName.Text = elem.Element("FSPROMO_NAME").Value.ToString();
                            textBoxPromoLength.Text = elem.Element("FSDURATION").Value.ToString();
                            textBoxMemo.Text = elem.Element("FSMEMO").Value.ToString();
                        }

                        //循序查詢Booking,Zone,SCH

                        WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_PROMO_EFFECTIVE_ZONE = new WSPGMSendSQL.SendSQLSoapClient();
                        StringBuilder sbZone = new StringBuilder();
                        sbZone.AppendLine("<Data>");
                        sbZone.AppendLine("<FNPROMO_BOOKING_NO>" + _PromoBookingNO + "</FNPROMO_BOOKING_NO>");
                        sbZone.AppendLine("</Data>");
                        SP_Q_TBPGM_PROMO_EFFECTIVE_ZONE.Do_QueryAsync("SP_Q_TBPGM_PROMO_EFFECTIVE_ZONE", sbZone.ToString(), ReturnXML);
                        SP_Q_TBPGM_PROMO_EFFECTIVE_ZONE.Do_QueryCompleted += (s1, args1) =>
                        {
                            if (args1.Error == null)
                            {
                                if (args1.Result != null && args1.Result != "")
                                {
                                    byte[] byteArray1 = Encoding.Unicode.GetBytes(args1.Result);
                                    StringBuilder output1 = new StringBuilder();
                                    // Create an XmlReader
                                    Promo_Zone_List.Clear();
                                    XDocument doc1 = XDocument.Load(new MemoryStream(byteArray1));
                                    var ltox1 = from str in doc1.Elements("Datas").Elements("Data")
                                                select str;
                                    foreach (XElement elem in ltox1)
                                    {

                                        //List<Promo_EFFECTIVE_ZONE> Promo_Zone_List = new List<Promo_EFFECTIVE_ZONE>();
                                        //List<Promo_SCHEDULED> Promo_Scheduled_List = new List<Promo_SCHEDULED>();
                                        Promo_Zone_List.Add(new Promo_EFFECTIVE_ZONE()
                                        {
                                            FNPROMO_BOOKING_NO = "",
                                            FSPROMO_ID = "",
                                            FNNO = elem.Element("FNNO").Value.ToString(),
                                            FDBEG_DATE = (Convert.ToDateTime(elem.Element("FDBEG_DATE").Value)).ToString("yyyy/MM/dd"),
                                            FDEND_DATE = (Convert.ToDateTime(elem.Element("FDEND_DATE").Value)).ToString("yyyy/MM/dd"),
                                            FSBEG_TIME = elem.Element("FSBEG_TIME").Value.ToString(),
                                            FSEND_TIME = elem.Element("FSEND_TIME").Value.ToString(),
                                            FCDATE_TIME_TYPE = elem.Element("FCDATE_TIME_TYPE").Value.ToString(),
                                            FSCHANNEL_ID = elem.Element("FSCHANNEL_ID").Value.ToString(),
                                            FSCHANNEL_NAME = elem.Element("FSCHANNEL_NAME").Value.ToString(),
                                            FSWEEK = elem.Element("FSWEEK").Value.ToString(),
                                        });
                                    }
                                    dataGridZone.ItemsSource = null;
                                    dataGridZone.ItemsSource = Promo_Zone_List;

                                    //循序查詢Booking,Zone,SCH
                                    WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_PROMO_BOOKING_SETTING = new WSPGMSendSQL.SendSQLSoapClient();
                                    StringBuilder sbSch = new StringBuilder();
                                    sbSch.AppendLine("<Data>");
                                    sbSch.AppendLine("<FNPROMO_BOOKING_NO>" + _PromoBookingNO + "</FNPROMO_BOOKING_NO>");
                                    sbSch.AppendLine("<FSPROMO_ID>" + "" + "</FSPROMO_ID>");
                                    sbSch.AppendLine("<FCSTATUS>" + "" + "</FCSTATUS>");
                                    
                                    sbSch.AppendLine("</Data>");
                                    SP_Q_TBPGM_PROMO_BOOKING_SETTING.Do_QueryAsync("SP_Q_TBPGM_PROMO_BOOKING_SETTING", sbSch.ToString(), ReturnXML);
                                    SP_Q_TBPGM_PROMO_BOOKING_SETTING.Do_QueryCompleted += (s2, args2) =>
                                    {
                                        if (args2.Error == null)
                                        {
                                            if (args2.Result != null && args2.Result != "")
                                            {
                                                byte[] byteArray2 = Encoding.Unicode.GetBytes(args2.Result);
                                                StringBuilder output2 = new StringBuilder();
                                                // Create an XmlReader
                                                Promo_Scheduled_List.Clear();
                                                XDocument doc2 = XDocument.Load(new MemoryStream(byteArray2));
                                                var ltox2 = from str in doc2.Elements("Datas").Elements("Data")
                                                            select str;
                                                foreach (XElement elem in ltox2)
                                                {
                                                    if (elem.Element("FSTIME1").Value.ToString() != "")
                                                    {
                                                        checkBoxTime1.IsChecked = true;
                                                        textBoxTime1.Text = elem.Element("FSTIME1").Value.ToString();
                                                    }
                                                    else
                                                    {
                                                        checkBoxTime1.IsChecked = false;
                                                        textBoxTime1.Text ="";
                                                    }
                                                    if (elem.Element("FSTIME2").Value.ToString() != "")
                                                    {
                                                        checkBoxTime2.IsChecked = true;
                                                        textBoxTime2.Text = elem.Element("FSTIME2").Value.ToString();
                                                    }
                                                    else
                                                    {
                                                        checkBoxTime2.IsChecked = false;
                                                        textBoxTime2.Text = "";
                                                    }
                                                    if (elem.Element("FSTIME3").Value.ToString() != "")
                                                    {
                                                        checkBoxTime3.IsChecked = true;
                                                        textBoxTime3.Text = elem.Element("FSTIME3").Value.ToString();
                                                    }
                                                    else
                                                    {
                                                        checkBoxTime3.IsChecked = false;
                                                        textBoxTime3.Text = "";
                                                    }
                                                    if (elem.Element("FSTIME4").Value.ToString() != "")
                                                    {
                                                        checkBoxTime4.IsChecked = true;
                                                        textBoxTime4.Text = elem.Element("FSTIME4").Value.ToString();
                                                    }
                                                    else
                                                    {
                                                        checkBoxTime4.IsChecked = false;
                                                        textBoxTime4.Text = "";
                                                    }
                                                    if (elem.Element("FSTIME5").Value.ToString() != "")
                                                    {
                                                        checkBoxTime5.IsChecked = true;
                                                        textBoxTime5.Text = elem.Element("FSTIME5").Value.ToString();
                                                    }
                                                    else
                                                    {
                                                        checkBoxTime5.IsChecked = false;
                                                        textBoxTime5.Text = "";
                                                    }
                                                    if (elem.Element("FSTIME6").Value.ToString() != "")
                                                    {
                                                        checkBoxTime6.IsChecked = true;
                                                        textBoxTime6.Text = elem.Element("FSTIME6").Value.ToString();
                                                    }
                                                    else
                                                    {
                                                        checkBoxTime6.IsChecked = false;
                                                        textBoxTime6.Text = "";
                                                    }
                                                    if (elem.Element("FSTIME7").Value.ToString() != "")
                                                    {
                                                        checkBoxTime7.IsChecked = true;
                                                        textBoxTime7.Text = elem.Element("FSTIME7").Value.ToString();
                                                    }
                                                    else
                                                    {
                                                        checkBoxTime7.IsChecked = false;
                                                        textBoxTime7.Text = "";
                                                    }
                                                    if (elem.Element("FSTIME8").Value.ToString() != "")
                                                    {
                                                        checkBoxTime8.IsChecked = true;
                                                        textBoxTime8.Text = elem.Element("FSTIME8").Value.ToString();
                                                    }
                                                    else
                                                    {
                                                        checkBoxTime8.IsChecked = false;
                                                        textBoxTime8.Text = "";
                                                    }
                                                    
                                                }

                                            }
                                        }
                                    };
                                }
                            }
                        };


                    }
                }
            };

        }


        List<Promo_EFFECTIVE_ZONE> Promo_Zone_List = new List<Promo_EFFECTIVE_ZONE>();
        List<Promo_SCHEDULED> Promo_Scheduled_List = new List<Promo_SCHEDULED>();



        public void TransWeek(string InWeekString)
        {
            if (InWeekString.Substring(0, 1) == "1")
                checkBoxWeek1.IsChecked = true;
            else
                checkBoxWeek1.IsChecked = false;
            if (InWeekString.Substring(1, 1) == "1")
                checkBoxWeek2.IsChecked = true;
            else
                checkBoxWeek2.IsChecked = false;
            if (InWeekString.Substring(2, 1) == "1")
                checkBoxWeek3.IsChecked = true;
            else
                checkBoxWeek3.IsChecked = false;
            if (InWeekString.Substring(3, 1) == "1")
                checkBoxWeek4.IsChecked = true;
            else
                checkBoxWeek4.IsChecked = false;
            if (InWeekString.Substring(4, 1) == "1")
                checkBoxWeek5.IsChecked = true;
            else
                checkBoxWeek5.IsChecked = false;
            if (InWeekString.Substring(5, 1) == "1")
                checkBoxWeek6.IsChecked = true;
            else
                checkBoxWeek6.IsChecked = false;
            if (InWeekString.Substring(6, 1) == "1")
                checkBoxWeek7.IsChecked = true;
            else
                checkBoxWeek7.IsChecked = false;
        }

        public string WeekCheckBoxToString()
        {
            string sweek = "";
            if (checkBoxWeek1.IsChecked == true)
                sweek = sweek + "1";
            else
                sweek = sweek + "0";

            if (checkBoxWeek2.IsChecked == true)
                sweek = sweek + "1";
            else
                sweek = sweek + "0";

            if (checkBoxWeek3.IsChecked == true)
                sweek = sweek + "1";
            else
                sweek = sweek + "0";

            if (checkBoxWeek4.IsChecked == true)
                sweek = sweek + "1";
            else
                sweek = sweek + "0";

            if (checkBoxWeek5.IsChecked == true)
                sweek = sweek + "1";
            else
                sweek = sweek + "0";

            if (checkBoxWeek6.IsChecked == true)
                sweek = sweek + "1";
            else
                sweek = sweek + "0";

            if (checkBoxWeek7.IsChecked == true)
                sweek = sweek + "1";
            else
                sweek = sweek + "0";
            return sweek;
        }

        public bool CheckTime(string Instring)
        {
            int TempInt;
            if (int.TryParse(Instring, out TempInt) == false)
                return false;
            if (Instring.Length < 4)
                return false;

            if (int.Parse(Instring.Substring(2, 2)) > 60)
                return false;
            return true;
        }

        private void checkBoxAllCheck_Click(object sender, RoutedEventArgs e)
        {
            checkBoxWeek1.IsChecked = checkBoxAllCheck.IsChecked;
            checkBoxWeek2.IsChecked = checkBoxAllCheck.IsChecked;
            checkBoxWeek3.IsChecked = checkBoxAllCheck.IsChecked;
            checkBoxWeek4.IsChecked = checkBoxAllCheck.IsChecked;
            checkBoxWeek5.IsChecked = checkBoxAllCheck.IsChecked;
            checkBoxWeek6.IsChecked = checkBoxAllCheck.IsChecked;
            checkBoxWeek7.IsChecked = checkBoxAllCheck.IsChecked;

        }

        private void buttonQueryPromo_Click(object sender, RoutedEventArgs e)
        {

        }

        private void buttonAdd_Click(object sender, RoutedEventArgs e)
        {
            if (comboBoxChannel.SelectedIndex < 0)
            {
                MessageBox.Show("請選擇頻道");
                return;
            }
            if (this.datePickerBDate.Text == "")
            {
                MessageBox.Show("請選擇起始日期");
                return;
            }
            if (this.datePickerEDate.Text == "")
            {
                MessageBox.Show("請選擇結束日期");
                return;
            }
            if (WeekCheckBoxToString() == "0000000")
            {
                MessageBox.Show("請至少要勾選一個星期選項");
                return;
            }
            if (this.textBoxBTime.Text != "" && this.textBoxETime.Text != "")
            {
                if (comboBoxTimeType.SelectedIndex < 0)
                {
                    MessageBox.Show("請選擇時間類型");
                    return;
                }
            }
            if ((this.textBoxBTime.Text == "" || this.textBoxETime.Text == ""))
            {
                if (this.textBoxBTime.Text == "" && this.textBoxETime.Text == "")
                { }
                else
                {
                    MessageBox.Show("請輸入起迄時間");
                    return;
                }
            }
            if (this.textBoxBTime.Text != "")
            {
                textBoxBTime.Text = "0000";
                //if (CheckTime(this.textBoxBTime.Text.Trim().ToString()) == false)
                //{
                //    MessageBox.Show("起迄時間格式有誤,請輸入4碼起始時間,EX 0600");
                //    return;
                //}
            }
            if (this.textBoxETime.Text != "")
            {
                if (CheckTime(this.textBoxETime.Text.Trim().ToString()) == false)
                {
                    MessageBox.Show("起迄時間格式有誤,請輸入4碼起始時間,EX 0600");
                    return;
                }
            }
            if (Convert.ToDateTime(datePickerEDate.Text) < Convert.ToDateTime(datePickerBDate.Text))
            {
                MessageBox.Show("結束日期必須大於起始日期");
                return;
            }
            if (textBoxETime.Text != "" && textBoxBTime.Text != "")
            {
                if (int.Parse(textBoxETime.Text) < int.Parse(textBoxBTime.Text))
                {
                    MessageBox.Show("結束時間必須大於起始時間");
                    return;
                }
            }
            foreach (Promo_EFFECTIVE_ZONE Temp_Promo_Zone_List in Promo_Zone_List)
            {
                if (Temp_Promo_Zone_List.FSPROMO_ID == this.textBoxPromoID.Text && Temp_Promo_Zone_List.FSCHANNEL_ID == ((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString()
                    && Temp_Promo_Zone_List.FDBEG_DATE == datePickerBDate.Text && Temp_Promo_Zone_List.FDEND_DATE == datePickerEDate.Text
                     && Temp_Promo_Zone_List.FSBEG_TIME == textBoxBTime.Text && Temp_Promo_Zone_List.FSEND_TIME == textBoxETime.Text)
                {
                    MessageBox.Show("此有效區間已經加入過,不可重複加入");
                    return;
                }
            }
            string _TempFCDATE_TIME_TYPE;
            if (comboBoxTimeType.SelectedIndex == -1)
                _TempFCDATE_TIME_TYPE = "";
            else
                _TempFCDATE_TIME_TYPE = ((ComboBoxItem)comboBoxTimeType.SelectedItem).Tag.ToString();
            Promo_Zone_List.Add(new Promo_EFFECTIVE_ZONE()
            {
                FNPROMO_BOOKING_NO = "",
                FSPROMO_ID = "",
                FNNO = (Promo_Zone_List.Count + 1).ToString(),
                FDBEG_DATE = datePickerBDate.Text,
                FDEND_DATE = datePickerEDate.Text,
                FSBEG_TIME = textBoxBTime.Text,
                FSEND_TIME = textBoxETime.Text,
                FCDATE_TIME_TYPE = _TempFCDATE_TIME_TYPE,
                FSCHANNEL_ID = ((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString(),
                FSCHANNEL_NAME = ((ComboBoxItem)comboBoxChannel.SelectedItem).Content.ToString(),
                FSWEEK = WeekCheckBoxToString(),
            });
            dataGridZone.ItemsSource = null;
            dataGridZone.ItemsSource = Promo_Zone_List;
        }

        private void buttonRemove_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridZone.SelectedIndex < 0)
            {
                MessageBox.Show("請先選擇要移除的短帶");
                return;
            }
            Promo_Zone_List.RemoveAt(dataGridZone.SelectedIndex);
            dataGridZone.ItemsSource = null;
            dataGridZone.ItemsSource = Promo_Zone_List;

        }

        public DateTime TransDate(string InDateString)
        {
            DateTime parsed;
            DateTime.TryParseExact(InDateString, "yyyy/mm/dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out parsed);
            return parsed;
        }

        


        private void buttonReject_Click(object sender, RoutedEventArgs e)
        {
            WorkWithField("F", "1", "不通過");
            ////要改流程變數的話要呼叫的
            //StringBuilder sb=new StringBuilder();
            //sb.Append(@"<VariableCollection>");
            //sb.Append(@"  <variable>");
            //sb.Append(@"    <name>hid_SendTo</name>");
            //sb.Append(@"    <value>" + "2" + "</value>");
            //sb.Append(@"  </variable>");
            //sb.Append(@"</VariableCollection>");//hid_SendTo
            ////處理流程到下一個階段

            //flowWebService.FlowSoapClient client = new flowWebService.FlowSoapClient();
            //client.UpdateFieldValueAsync(intFlowID, sb.ToString());
            //client.UpdateFieldValueCompleted += (s, args) =>
            //{
            //    client.WorkWithFieldAsync(intProcessID.ToString(), "");
            //};
        }

        private void buttonAgree_Click(object sender, RoutedEventArgs e)
        {
            if (Promo_Zone_List.Count <= 0)
            {
                MessageBox.Show("請先加入有效區間");
                return;
            }

            if (this.textBoxPromoID.Text == "")
            {
                MessageBox.Show("請先查詢短帶");
                return;
            }

            WSPGMSendSQL.SendSQLSoapClient SP_U_Promo_Booking = new WSPGMSendSQL.SendSQLSoapClient();
            StringBuilder sbBookingSETTING = new StringBuilder();
            sbBookingSETTING.AppendLine("<Data>");
            sbBookingSETTING.AppendLine("<FSTIME1>" + textBoxTime1.Text + "</FSTIME1>");
            sbBookingSETTING.AppendLine("<FSTIME2>" + textBoxTime2.Text + "</FSTIME2>");
            sbBookingSETTING.AppendLine("<FSTIME3>" + textBoxTime3.Text + "</FSTIME3>");
            sbBookingSETTING.AppendLine("<FSTIME4>" + textBoxTime4.Text + "</FSTIME4>");
            sbBookingSETTING.AppendLine("<FSTIME5>" + textBoxTime5.Text + "</FSTIME5>");
            sbBookingSETTING.AppendLine("<FSTIME6>" + textBoxTime6.Text + "</FSTIME6>");
            sbBookingSETTING.AppendLine("<FSTIME7>" + textBoxTime7.Text + "</FSTIME7>");
            sbBookingSETTING.AppendLine("<FSTIME8>" + textBoxTime8.Text + "</FSTIME8>");
            sbBookingSETTING.AppendLine("<FSCMNO>" + "" + "</FSCMNO>");
            sbBookingSETTING.AppendLine("</Data>");

            StringBuilder sbPromoZone = new StringBuilder();
            int ZoneNO = 1;
            sbPromoZone.AppendLine("<Datas>");
            foreach (Promo_EFFECTIVE_ZONE TempZone in Promo_Zone_List)
            {
                sbPromoZone.AppendLine("<Data>");
                sbPromoZone.AppendLine("<FSPROMO_ID>" + textBoxPromoID.Text + "</FSPROMO_ID>");
                sbPromoZone.AppendLine("<FNNO>" + ZoneNO.ToString() + "</FNNO>");
                ZoneNO = ZoneNO + 1;
                sbPromoZone.AppendLine("<FDBEG_DATE>" + TempZone.FDBEG_DATE + "</FDBEG_DATE>");
                sbPromoZone.AppendLine("<FDEND_DATE>" + TempZone.FDEND_DATE + "</FDEND_DATE>");
                sbPromoZone.AppendLine("<FSBEG_TIME>" + TempZone.FSBEG_TIME + "</FSBEG_TIME>");
                sbPromoZone.AppendLine("<FSEND_TIME>" + TempZone.FSEND_TIME + "</FSEND_TIME>");
                sbPromoZone.AppendLine("<FCDATE_TIME_TYPE>" + TempZone.FCDATE_TIME_TYPE + "</FCDATE_TIME_TYPE>");
                sbPromoZone.AppendLine("<FSCHANNEL_ID>" + TempZone.FSCHANNEL_ID + "</FSCHANNEL_ID>");
                sbPromoZone.AppendLine("<FSWEEK>" + TempZone.FSWEEK + "</FSWEEK>");
                sbPromoZone.AppendLine("<FSCREATED_BY>" + UserClass.userData.FSUSER_ID.ToString() + "</FSCREATED_BY>");
                sbPromoZone.AppendLine("<FSUPDATED_BY>" + "" + "</FSUPDATED_BY>");
                sbPromoZone.AppendLine("</Data>");
            }
            sbPromoZone.AppendLine("</Datas>");

            
            //sb=sb.Replace("\r\n", "");

            SP_U_Promo_Booking.Do_Agree_Promo_BookingAsync(_PromoBookingNO, sbPromoZone.ToString(), sbBookingSETTING.ToString(), textBoxMemo.Text);
            SP_U_Promo_Booking.Do_Agree_Promo_BookingCompleted += (s, args) =>
            {
                if (args.Error == null)
                {

                    if (args.Result == true)
                    {
                        //WorkWithField("Y", "1", "同意");
                        MessageBox.Show("修改完成");
                    }
                    else
                        MessageBox.Show("託播單資料異動失敗");
                }
            };
        }

        private void WorkWithField(string FCCHECK_STATUS, string hid_SendTo, string FlowResult)
        {
            FlowMAMSoapClient client = new FlowMAMSoapClient();
            
            //flowWebService.FlowSoapClient client = new flowWebService.FlowSoapClient();
            StringBuilder sb = new StringBuilder();
            sb.Append(@"<VariableCollection>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>parameter01</name>");
            sb.Append(@"    <value>" + FCCHECK_STATUS + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>hid_SendTo</name>");
            sb.Append(@"    <value>" + hid_SendTo + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>USERID</name>");
            sb.Append(@"    <value>" + UserClass.userData.FSUSER_ID + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>USERNAME</name>");
            sb.Append(@"    <value>" + UserClass.userData.FSUSER_ChtName + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>FlowResult</name>");
            sb.Append(@"    <value>" + FlowResult + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>CancelForm</name>");
            sb.Append(@"    <value>1</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"</VariableCollection>");//hid_SendTo

            client.CallFlow_WorkWithFieldAsync(intProcessID.ToString(), sb.ToString());

            client.CallFlow_WorkWithFieldCompleted += (s1, args1) =>
            {
                //MessageBox.Show("審核成功!");
                if (args1.Error == null)
                {
                    if (args1.Result != null && args1.Result != "")
                    {
                        MessageBox.Show("審核成功!");
                    }
                    else
                    {
                        MessageBox.Show("審核失敗!");
                    }
                }
                this.DialogResult = true;
            };
        }


        private void AgreeFlow()
        {
            //處理流程到下一個階段
            flowWebService.FlowSoapClient client = new flowWebService.FlowSoapClient();
            client.WorkWithFieldAsync(intProcessID.ToString(), "");
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        void FlowClinet_NewFlowWithFieldCompleted(object sender, flowWebService.NewFlowWithFieldCompletedEventArgs e)
        {
            MessageBox.Show("表單已送出");
        }



    }
}
