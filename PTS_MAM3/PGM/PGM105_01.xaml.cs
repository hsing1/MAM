﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace PTS_MAM3.PGM
{
    public partial class PGM105_01 : ChildWindow
    {
        public string XElementToString(XElement INXElement)
        {
            string ReturnStr;
            try
            {
                ReturnStr = INXElement.Value.ToString();
                return ReturnStr;

            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public PGM105_01()
        {
            InitializeComponent();
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBZDEPT_ALL = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            sb.AppendLine("</Data>");
            SP_Q_TBZDEPT_ALL.Do_QueryAsync("[SP_Q_TBZDEPT_ALL]", sb.ToString(), ReturnXML);
            SP_Q_TBZDEPT_ALL.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {
                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        StringBuilder output = new StringBuilder();
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            ComboBoxItem TempComboBoxItem = new ComboBoxItem();
                            TempComboBoxItem.Content = elem.Element("NAME").Value.ToString();
                            TempComboBoxItem.Tag = elem.Element("ID").Value.ToString();

                            comboBoxSource.Items.Add(TempComboBoxItem);
                        }
                        if (_source != "")
                        {
                            ComboBoxCheckSelectItem("來源", comboBoxSource, _source);
                        }
                    }
                }
            };

            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_TIME_SEG_SET = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";
            StringBuilder sb1 = new StringBuilder();
            sb1.AppendLine("<Data>");
            sb1.AppendLine("</Data>");
            SP_Q_TBPGM_TIME_SEG_SET.Do_QueryAsync("[SP_Q_TBPGM_TIME_SEG_SET]", sb1.ToString());
            SP_Q_TBPGM_TIME_SEG_SET.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {
                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);

                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            if (XElementToString(elem.Element("FSBEG_TIME")) != "")
                            {
                                listBoxTimeSET.Items.Add(XElementToString(elem.Element("FSTIME_SEG_TYPE")) + ":" + XElementToString(elem.Element("FSBEG_TIME")) + "--" + XElementToString(elem.Element("FSEND_TIME")));
                            }
                        }

                    }

                }
            };
        }
        public string _WorkStatus;
        public string _source = "";
        public bool ParseInteger(string InString)
        {
            try
            {
                int.Parse(InString);
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("最小次數格式有誤");
                return false;
            }
        }

        public void SetSource()
        {
            if (_source != "")
            {
                ComboBoxCheckSelectItem("來源", comboBoxSource, _source);
            }
        }

        public void GetSource()
        {
            InitializeComponent();
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBZDEPT_ALL = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            sb.AppendLine("</Data>");
            SP_Q_TBZDEPT_ALL.Do_QueryAsync("[SP_Q_TBZDEPT_ALL]", sb.ToString(), ReturnXML);
            SP_Q_TBZDEPT_ALL.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            ComboBoxItem TempComboBoxItem = new ComboBoxItem();
                            TempComboBoxItem.Content = elem.Element("NAME").Value.ToString();
                            TempComboBoxItem.Tag = elem.Element("ID").Value.ToString();
                            comboBoxSource.Items.Add(TempComboBoxItem);
                        }
                    }
                }
                if (_source != "")
                {
                    ComboBoxCheckSelectItem("來源", comboBoxSource, _source);

                }
            };

        }

        public int GetMAXFNNO()
        {
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_PROMO_PLAN_MAX_FNNO = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<Data>");
            sb.AppendLine("</Data>");
            string maxNO = "";
            SP_Q_TBPGM_PROMO_PLAN_MAX_FNNO.Do_QueryAsync("SP_Q_TBPGM_PROMO_PLAN_MAX_FNNO", sb.ToString());
            SP_Q_TBPGM_PROMO_PLAN_MAX_FNNO.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        //StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            maxNO = elem.Element("MAXFNNO").Value.ToString();

                        }
                    }
                }
            };
            return int.Parse(maxNO);
        }
        //檢查資料輸入格式
        public bool CheckInput()
        {
            if (((ComboBoxItem)comboBoxSource.SelectedItem).Tag.ToString()=="")
            {
                MessageBox.Show("請先選擇來源");
                comboBoxSource.Focus();
                return false;
            }
            if (textBoxProgID.Text == "")
            {
                MessageBox.Show("請先選擇節目");
                textBoxProgName.Focus();
                return false;
            }
            if (textBoxProgName.Text == "")
            {
                MessageBox.Show("請先輸入節目");
                return false;
            }
            if (datePickerBDate.Text == "")
            {
                MessageBox.Show("請先選擇起始日期");
                return false;
            }
            if (datePickerEDate.Text == "")
            {
                MessageBox.Show("請先選擇結束日期");
                return false;
            }
            if (textBoxMinCount.Text == "")
            {
                MessageBox.Show("請先輸入最小次數");
                return false;
            }
            else
            {
                if (ParseInteger(textBoxMinCount.Text) == false)
                {
                    MessageBox.Show("最小次數格式有誤");
                    return false;
                }
            }
            if (textBoxMaxCount.Text == "")
            {
                MessageBox.Show("請先輸入最大次數");
                return false;
            }
            else
            {
                if (ParseInteger(textBoxMaxCount.Text) == false)
                {
                    MessageBox.Show("最大次數格式有誤");
                    return false;
                }
            }

            if (int.Parse(textBoxMaxCount.Text) < int.Parse(textBoxMinCount.Text))
            {
                    MessageBox.Show("最大次數必須大於最小次數");
                    return false;
            }

            if (textBoxTypeACount.Text == "")
                textBoxTypeACount.Text = "0";
            if (textBoxTypeBCount.Text == "")
                textBoxTypeBCount.Text = "0";
            if (textBoxTypeCCount.Text == "")
                textBoxTypeCCount.Text = "0";
            if (textBoxTypeDCount.Text == "")
                textBoxTypeDCount.Text = "0";
            if (textBoxTypeECount.Text == "")
                textBoxTypeECount.Text = "0";
            if (ParseInteger(textBoxTypeACount.Text) == false)
            {
                MessageBox.Show("時段次數A格式有誤");
                return false;
            }
            if (ParseInteger(textBoxTypeBCount.Text) == false)
            {
                MessageBox.Show("時段次數B格式有誤");
                return false;
            }
            if (ParseInteger(textBoxTypeCCount.Text) == false)
            {
                MessageBox.Show("時段次數C格式有誤");
                return false;
            }
            if (ParseInteger(textBoxTypeDCount.Text) == false)
            {
                MessageBox.Show("時段次數D格式有誤");
                return false;
            }
            if (ParseInteger(textBoxTypeECount.Text) == false)
            {
                MessageBox.Show("時段次數E格式有誤");
                return false;
            }
            if ((checkBoxWeek1.IsChecked == false) && (checkBoxWeek2.IsChecked == false) && (checkBoxWeek3.IsChecked == false) && (checkBoxWeek4.IsChecked == false) && (checkBoxWeek5.IsChecked == false) && (checkBoxWeek6.IsChecked == false) && (checkBoxWeek7.IsChecked == false))
            {
                MessageBox.Show("請至少勾選一個星期");
                return false;
            }
            return true;
        }

        List<PromoPlanList> PlanList = new List<PromoPlanList>();
        //檢查資料是否重複
        public bool CheckDataExist()
        {
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_PROMO_PLAN = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            sb.AppendLine("<FSPROG_ID>" + textBoxProgID.Text + "</FSPROG_ID>");
            sb.AppendLine("<FSPROG_NAME>" + textBoxProgName.Text + "</FSPROG_NAME>");
            //sb.AppendLine("<FCSOURCETYPE>" + ((ComboBoxItem)comboBoxSource.SelectedItem).Tag.ToString() + "</FCSOURCETYPE>");
            sb.AppendLine("<FCSOURCETYPE>" + "" + "</FCSOURCETYPE>");

            sb.AppendLine("<FSPROMO_BEG_DATE>" + datePickerBDate.Text + "</FSPROMO_BEG_DATE>");
            sb.AppendLine("<FSPROMO_END_DATE>" + datePickerEDate.Text + "</FSPROMO_END_DATE>");
            sb.AppendLine("</Data>");
            bool hasSeting = true;
            SP_Q_TBPGM_PROMO_PLAN.Do_QueryAsync("SP_Q_TBPGM_PROMO_PLAN", sb.ToString(), ReturnXML);
            SP_Q_TBPGM_PROMO_PLAN.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {
                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        PlanList.Clear();
                        //StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            //PlanList.Add(new PromoPlanList()
                            //{
                            //    FNNO = elem.Element("FNNO").Value.ToString(),
                            //    FSPROG_ID = elem.Element("FSPROG_ID").Value.ToString(),
                            //    FSPROG_NAME = elem.Element("FSPROG_NAME").Value.ToString(),
                            //    FCSOURCETYPE = elem.Element("FCSOURCETYPE").Value.ToString(),
                            //    FCSOURCE = elem.Element("FCSOURCE").Value.ToString(),
                            //    FSPLAY_COUNT_MAX =elem.Element("FSPLAY_COUNT_MAX").Value.ToString(),
                            //    FSPLAY_COUNT_MIN = elem.Element("FSPLAY_COUNT_MIN").Value.ToString(),
                            //    FNPROMO_TYPEA =elem.Element("FNPROMO_TYPEA").Value.ToString(),
                            //    FNPROMO_TYPEB =elem.Element("FNPROMO_TYPEB").Value.ToString(),
                            //    FNPROMO_TYPEC =elem.Element("FNPROMO_TYPEC").Value.ToString(),
                            //    FNPROMO_TYPED = elem.Element("FNPROMO_TYPED").Value.ToString(),
                            //    FNPROMO_TYPEE = elem.Element("FNPROMO_TYPEE").Value.ToString(),
                            //    FSPROMO_BEG_DATE = elem.Element("FSPROMO_BEG_DATE").Value.ToString(),
                            //    FSPROMO_END_DATE = elem.Element("FSPROMO_END_DATE").Value.ToString(),
                            //    FSWEEK = elem.Element("FSWEEK").Value.ToString(),
                            //    FSMEMO = elem.Element("FSMEMO").Value.ToString()
                            //});
                            string TempWeek = elem.Element("FSWEEK").Value.ToString();
                            if (_WorkStatus == "I")
                            {
                                if (((checkBoxWeek1.IsChecked == true) && (TempWeek.Substring(0, 1) == "1")) ||
                                    ((checkBoxWeek2.IsChecked == true) && (TempWeek.Substring(1, 1) == "1")) ||
                                    ((checkBoxWeek3.IsChecked == true) && (TempWeek.Substring(2, 1) == "1")) ||
                                    ((checkBoxWeek4.IsChecked == true) && (TempWeek.Substring(3, 1) == "1")) ||
                                    ((checkBoxWeek5.IsChecked == true) && (TempWeek.Substring(4, 1) == "1")) ||
                                    ((checkBoxWeek6.IsChecked == true) && (TempWeek.Substring(5, 1) == "1")) ||
                                    ((checkBoxWeek7.IsChecked == true) && (TempWeek.Substring(6, 1) == "1")))
                                {
                                    MessageBox.Show("此節目此日期區段中有設定過預約破口資源,不可重複建立");
                                    hasSeting = false;
                                }
                            }
                            else if (_WorkStatus == "U")
                            {
                                if ((((checkBoxWeek1.IsChecked == true) && (TempWeek.Substring(0, 1) == "1")) ||
                                    ((checkBoxWeek2.IsChecked == true) && (TempWeek.Substring(1, 1) == "1")) ||
                                    ((checkBoxWeek3.IsChecked == true) && (TempWeek.Substring(2, 1) == "1")) ||
                                    ((checkBoxWeek4.IsChecked == true) && (TempWeek.Substring(3, 1) == "1")) ||
                                    ((checkBoxWeek5.IsChecked == true) && (TempWeek.Substring(4, 1) == "1")) ||
                                    ((checkBoxWeek6.IsChecked == true) && (TempWeek.Substring(5, 1) == "1")) ||
                                    ((checkBoxWeek7.IsChecked == true) && (TempWeek.Substring(6, 1) == "1")))
                                && (elem.Element("FNNO").Value.ToString() != textBoxFNNO.Text))
                                {
                                    MessageBox.Show("此節目此日期區段中有設定過預約破口資源,不可重複建立");
                                    hasSeting = false;
                                }
                            }
                        }
                        //dataGridSaveList.ItemsSource = null;
                        //dataGridSaveList.ItemsSource = QueryProg;
                    }
                }

                if (hasSeting == true)
                {
                    _week = WeekCheckBoxToString();

                    if (_WorkStatus == "I")
                    {
                        //textBoxFNNO.Text=GetMAXFNNO().ToString();
                        InsertData();
                        
                    }
                    else if (_WorkStatus == "U")
                    {
                        UpdateData();
                    }

                    
                }
            };

            return hasSeting;
        }

        public void ComboBoxCheckSelectItem(string Instring, ComboBox SelectComboBox, String SelectText)
        {
            SelectComboBox.SelectedIndex = -1;
            for (int Combocount = 0; Combocount < SelectComboBox.Items.Count; Combocount++)
            {
                if (((ComboBoxItem)SelectComboBox.Items[Combocount]).Content.ToString() == SelectText)
                {
                    SelectComboBox.SelectedIndex = Combocount;
                }
            }
            if (SelectComboBox.SelectedIndex == -1)
                MessageBox.Show("找不到對應的" + Instring + "設定");
        }

        public void TransWeek(string InWeekString)
        {
            if (InWeekString.Substring(0, 1) == "1")
                checkBoxWeek1.IsChecked = true;
            else
                checkBoxWeek1.IsChecked = false;
            if (InWeekString.Substring(1, 1) == "1")
                checkBoxWeek2.IsChecked = true;
            else
                checkBoxWeek2.IsChecked = false;
            if (InWeekString.Substring(2, 1) == "1")
                checkBoxWeek3.IsChecked = true;
            else
                checkBoxWeek3.IsChecked = false;
            if (InWeekString.Substring(3, 1) == "1")
                checkBoxWeek4.IsChecked = true;
            else
                checkBoxWeek4.IsChecked = false;
            if (InWeekString.Substring(4, 1) == "1")
                checkBoxWeek5.IsChecked = true;
            else
                checkBoxWeek5.IsChecked = false;
            if (InWeekString.Substring(5, 1) == "1")
                checkBoxWeek6.IsChecked = true;
            else
                checkBoxWeek6.IsChecked = false;
            if (InWeekString.Substring(6, 1) == "1")
                checkBoxWeek7.IsChecked = true;
            else
                checkBoxWeek7.IsChecked = false;
        }

        public string WeekCheckBoxToString()
        {
            string sweek = "";
            if (checkBoxWeek1.IsChecked == true)
                sweek = sweek + "1";
            else
                sweek = sweek + "0";

            if (checkBoxWeek2.IsChecked == true)
                sweek = sweek + "1";
            else
                sweek = sweek + "0";

            if (checkBoxWeek3.IsChecked == true)
                sweek = sweek + "1";
            else
                sweek = sweek + "0";

            if (checkBoxWeek4.IsChecked == true)
                sweek = sweek + "1";
            else
                sweek = sweek + "0";

            if (checkBoxWeek5.IsChecked == true)
                sweek = sweek + "1";
            else
                sweek = sweek + "0";

            if (checkBoxWeek6.IsChecked == true)
                sweek = sweek + "1";
            else
                sweek = sweek + "0";

            if (checkBoxWeek7.IsChecked == true)
                sweek = sweek + "1";
            else
                sweek = sweek + "0";
            return sweek;
        }

        public void InsertData()
        {
            WSPGMSendSQL.SendSQLSoapClient SP_I_TBPGM_PROMO_PLAN = new WSPGMSendSQL.SendSQLSoapClient();
            StringBuilder sbMaxno = new StringBuilder();

            sbMaxno.AppendLine("<Data>");
            sbMaxno.AppendLine("</Data>");

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<Data>");
            //sb.AppendLine("<FNNO>" + textBoxFNNO.Text + "</FNNO>");
            sb.AppendLine("<FSPROG_ID>" + textBoxProgID.Text + "</FSPROG_ID>");
            sb.AppendLine("<FSPROG_NAME>" + textBoxProgName.Text + "</FSPROG_NAME>");
            sb.AppendLine("<FCSOURCETYPE>" + ((ComboBoxItem)comboBoxSource.SelectedItem).Tag.ToString() + "</FCSOURCETYPE>");
            sb.AppendLine("<FCSOURCE>" + ((ComboBoxItem)comboBoxSource.SelectedItem).Content.ToString() + "</FCSOURCE>");
            sb.AppendLine("<FSPLAY_COUNT_MAX>" + textBoxMaxCount.Text + "</FSPLAY_COUNT_MAX>");
            sb.AppendLine("<FSPLAY_COUNT_MIN>" + textBoxMinCount.Text + "</FSPLAY_COUNT_MIN>");
            sb.AppendLine("<FNPROMO_TYPEA>" + textBoxTypeACount.Text + "</FNPROMO_TYPEA>");
            sb.AppendLine("<FNPROMO_TYPEB>" + textBoxTypeBCount.Text + "</FNPROMO_TYPEB>");
            sb.AppendLine("<FNPROMO_TYPEC>" + textBoxTypeCCount.Text + "</FNPROMO_TYPEC>");
            sb.AppendLine("<FNPROMO_TYPED>" + textBoxTypeDCount.Text + "</FNPROMO_TYPED>");
            sb.AppendLine("<FNPROMO_TYPEE>" + textBoxTypeECount.Text + "</FNPROMO_TYPEE>");
            sb.AppendLine("<FSPROMO_BEG_DATE>" + datePickerBDate.Text + "</FSPROMO_BEG_DATE>");
            sb.AppendLine("<FSPROMO_END_DATE>" + datePickerEDate.Text + "</FSPROMO_END_DATE>");
            sb.AppendLine("<FSWEEK>" + WeekCheckBoxToString() + "</FSWEEK>");
            sb.AppendLine("<FSMEMO>" + textBoxMemo.Text + "</FSMEMO>");
            sb.AppendLine("<FSCREATED_BY>" + UserClass.userData.FSUSER_ID.ToString() + "</FSCREATED_BY>");
            sb.AppendLine("<FSUPDATED_BY>" + UserClass.userData.FSUSER_ID.ToString() + "</FSUPDATED_BY>");
            sb.AppendLine("</Data>");

            SP_I_TBPGM_PROMO_PLAN.Do_Insert_TBPGM_PROMO_PLANAsync("SP_Q_TBPGM_PROMO_PLAN_MAX_FNNO", sbMaxno.ToString(), "SP_I_TBPGM_PROMO_PLAN", sb.ToString());
            SP_I_TBPGM_PROMO_PLAN.Do_Insert_TBPGM_PROMO_PLANCompleted += (s, args) =>
        {
            if (args.Error == null)
            {
                if (args.Result == "Error")
                    MessageBox.Show("新增失敗");
                else
                {
                    textBoxFNNO.Text = args.Result;
                    MessageBox.Show("新增完成");
                    this.DialogResult = true;
                }
            }
        };
        }

        public void UpdateData()
        {
            WSPGMSendSQL.SendSQLSoapClient SP_U_TBPGM_PROMO_PLAN = new WSPGMSendSQL.SendSQLSoapClient();
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<Data>");
            sb.AppendLine("<FNNO>" + textBoxFNNO.Text + "</FNNO>");
            sb.AppendLine("<FSPROG_ID>" + textBoxProgID.Text + "</FSPROG_ID>");
            sb.AppendLine("<FSPROG_NAME>" +  TransferTimecode.ReplaceXML(textBoxProgName.Text) + "</FSPROG_NAME>");
            sb.AppendLine("<FCSOURCETYPE>" + ((ComboBoxItem)comboBoxSource.SelectedItem).Tag.ToString() + "</FCSOURCETYPE>");
            sb.AppendLine("<FCSOURCE>" + ((ComboBoxItem)comboBoxSource.SelectedItem).Content.ToString() + "</FCSOURCE>");
            sb.AppendLine("<FSPLAY_COUNT_MAX>" + textBoxMaxCount.Text + "</FSPLAY_COUNT_MAX>");
            sb.AppendLine("<FSPLAY_COUNT_MIN>" + textBoxMinCount.Text + "</FSPLAY_COUNT_MIN>");
            sb.AppendLine("<FNPROMO_TYPEA>" + textBoxTypeACount.Text + "</FNPROMO_TYPEA>");
            sb.AppendLine("<FNPROMO_TYPEB>" + textBoxTypeBCount.Text + "</FNPROMO_TYPEB>");
            sb.AppendLine("<FNPROMO_TYPEC>" + textBoxTypeCCount.Text + "</FNPROMO_TYPEC>");
            sb.AppendLine("<FNPROMO_TYPED>" + textBoxTypeDCount.Text + "</FNPROMO_TYPED>");
            sb.AppendLine("<FNPROMO_TYPEE>" + textBoxTypeECount.Text + "</FNPROMO_TYPEE>");
            sb.AppendLine("<FSPROMO_BEG_DATE>" + datePickerBDate.Text + "</FSPROMO_BEG_DATE>");
            sb.AppendLine("<FSPROMO_END_DATE>" + datePickerEDate.Text + "</FSPROMO_END_DATE>");
            sb.AppendLine("<FSWEEK>" + WeekCheckBoxToString() + "</FSWEEK>");
            sb.AppendLine("<FSMEMO>" + TransferTimecode.ReplaceXML(textBoxMemo.Text) + "</FSMEMO>");
            sb.AppendLine("<FSCREATED_BY>" + UserClass.userData.FSUSER_ID.ToString() + "</FSCREATED_BY>");
            sb.AppendLine("<FSUPDATED_BY>" + UserClass.userData.FSUSER_ID.ToString() + "</FSUPDATED_BY>");

            sb.AppendLine("</Data>");

            SP_U_TBPGM_PROMO_PLAN.Do_InsertAsync("SP_U_TBPGM_PROMO_PLAN", sb.ToString());
            SP_U_TBPGM_PROMO_PLAN.Do_InsertCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result == true)
                    {
                        MessageBox.Show("修改成功");
                        this.DialogResult = true;
                    }
                    else
                        MessageBox.Show("修改失敗");
                }
            };
        }

        public bool _hasInsOrModify = false;
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            _hasInsOrModify = true;
            if (CheckInput() == false) //檢查資料輸入格式
            {
                return;
            }

            if (CheckDataExist() == false) //檢查資料是否重複
            {
                return;
            }

        }
        public string _week = "";
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void buttonQueryProg_Click(object sender, RoutedEventArgs e)
        {
            PGM105_02 QueryProg_Frm = new PGM105_02();
            QueryProg_Frm.textBoxNewProgName.Text = textBoxProgName.Text;
            QueryProg_Frm.SendQuery();
            QueryProg_Frm.Show();
            QueryProg_Frm.Closed += (s, args) =>
            {
                if (QueryProg_Frm._FSPROG_ID != "")
                {
                    textBoxProgID.Text = QueryProg_Frm._FSPROG_ID;
                    textBoxProgName.Text = QueryProg_Frm._FSPGDNAME;
                }
            };

        }

        private void textBoxMaxCount_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}

