﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Text;

namespace PTS_MAM3.PGM
{
    public partial class PGM105 : Page
    {
        public PGM105()
        {
            InitializeComponent();
            if (ModuleClass.getModulePermission("P400001") == false)
            {
                MessageBox.Show("您沒有維護預約破口的權限");
                return;
            }
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }
        List<PromoPlanList> PlanList = new List<PromoPlanList>();
        private void btnPROGAdd_Click(object sender, RoutedEventArgs e)
        {


            PGM105_01 PromoPlanIns_Frm = new PGM105_01();
            //PromoPlanIns_Frm.GetSource();
            PromoPlanIns_Frm._WorkStatus = "I";
            PromoPlanIns_Frm._source = "";
            PromoPlanIns_Frm.Show();
            PromoPlanIns_Frm.Closed += (s, args) =>
            {
                if (PromoPlanIns_Frm._hasInsOrModify == true)
                {
                    PlanList.Add(new PromoPlanList()
                    {
                        FNNO = PromoPlanIns_Frm.textBoxFNNO.Text,
                        FSPROG_ID = PromoPlanIns_Frm.textBoxProgID.Text,
                        FSPROG_NAME = PromoPlanIns_Frm.textBoxProgName.Text,
                        FCSOURCETYPE = ((ComboBoxItem)PromoPlanIns_Frm.comboBoxSource.SelectedItem).Tag.ToString(),
                        FCSOURCE = ((ComboBoxItem)PromoPlanIns_Frm.comboBoxSource.SelectedItem).Content.ToString(),
                        FSPLAY_COUNT_MAX = PromoPlanIns_Frm.textBoxMinCount.Text,
                        FSPLAY_COUNT_MIN = PromoPlanIns_Frm.textBoxMaxCount.Text,
                        FNPROMO_TYPEA = PromoPlanIns_Frm.textBoxTypeACount.Text,
                        FNPROMO_TYPEB = PromoPlanIns_Frm.textBoxTypeBCount.Text,
                        FNPROMO_TYPEC = PromoPlanIns_Frm.textBoxTypeCCount.Text,
                        FNPROMO_TYPED = PromoPlanIns_Frm.textBoxTypeDCount.Text,
                        FNPROMO_TYPEE = PromoPlanIns_Frm.textBoxTypeECount.Text,
                        FSPROMO_BEG_DATE = PromoPlanIns_Frm.datePickerBDate.Text,
                        FSPROMO_END_DATE = PromoPlanIns_Frm.datePickerEDate.Text,
                        FSWEEK = PromoPlanIns_Frm._week,
                        FSMEMO = PromoPlanIns_Frm.textBoxMemo.Text
                    });

                    dataGridPromoPlan.ItemsSource = null;
                    dataGridPromoPlan.ItemsSource = PlanList;
                }
            };
        }

        private void btnPROGModify_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridPromoPlan.SelectedIndex < 0)
            {
                MessageBox.Show("請先選擇一筆資料,再進行修改");
                return;
            }
            PGM105_01 PromoPlanIns_Frm = new PGM105_01();
            PromoPlanIns_Frm._source = PlanList[dataGridPromoPlan.SelectedIndex].FCSOURCE;
            //PromoPlanIns_Frm.SetSource();
            //PromoPlanIns_Frm.GetSource();
            PromoPlanIns_Frm.textBoxFNNO.Text = PlanList[dataGridPromoPlan.SelectedIndex].FNNO;
            PromoPlanIns_Frm.textBoxProgID.Text=PlanList[dataGridPromoPlan.SelectedIndex].FSPROG_ID;
            PromoPlanIns_Frm.textBoxProgName.Text=PlanList[dataGridPromoPlan.SelectedIndex].FSPROG_NAME;
            //PromoPlanIns_Frm.ComboBoxCheckSelectItem("來源", PromoPlanIns_Frm.comboBoxSource, PlanList[dataGridPromoPlan.SelectedIndex].FCSOURCE);
            PromoPlanIns_Frm.textBoxMinCount.Text = PlanList[dataGridPromoPlan.SelectedIndex].FSPLAY_COUNT_MAX;
            PromoPlanIns_Frm.textBoxMaxCount.Text = PlanList[dataGridPromoPlan.SelectedIndex].FSPLAY_COUNT_MIN;
            PromoPlanIns_Frm.textBoxTypeACount.Text = PlanList[dataGridPromoPlan.SelectedIndex].FNPROMO_TYPEA;
            PromoPlanIns_Frm.textBoxTypeBCount.Text = PlanList[dataGridPromoPlan.SelectedIndex].FNPROMO_TYPEB;
            PromoPlanIns_Frm.textBoxTypeCCount.Text = PlanList[dataGridPromoPlan.SelectedIndex].FNPROMO_TYPEC;
            PromoPlanIns_Frm.textBoxTypeDCount.Text = PlanList[dataGridPromoPlan.SelectedIndex].FNPROMO_TYPED;
            PromoPlanIns_Frm.textBoxTypeECount.Text = PlanList[dataGridPromoPlan.SelectedIndex].FNPROMO_TYPEE;
            PromoPlanIns_Frm.datePickerBDate.Text = PlanList[dataGridPromoPlan.SelectedIndex].FSPROMO_BEG_DATE;
            PromoPlanIns_Frm.datePickerEDate.Text = PlanList[dataGridPromoPlan.SelectedIndex].FSPROMO_END_DATE;
            PromoPlanIns_Frm._week = PlanList[dataGridPromoPlan.SelectedIndex].FSWEEK;
            PromoPlanIns_Frm.TransWeek(PlanList[dataGridPromoPlan.SelectedIndex].FSWEEK);
            PromoPlanIns_Frm.textBoxMemo.Text = PlanList[dataGridPromoPlan.SelectedIndex].FSMEMO;
            PromoPlanIns_Frm._WorkStatus = "U";
            PromoPlanIns_Frm.Show();
            PromoPlanIns_Frm.Closed += (s, args) =>
            {
                if (PromoPlanIns_Frm._hasInsOrModify == true)
                {

                    PlanList[dataGridPromoPlan.SelectedIndex].FNNO = PromoPlanIns_Frm.textBoxFNNO.Text;
                    PlanList[dataGridPromoPlan.SelectedIndex].FSPROG_ID = PromoPlanIns_Frm.textBoxProgID.Text;
                    PlanList[dataGridPromoPlan.SelectedIndex].FSPROG_NAME = PromoPlanIns_Frm.textBoxProgName.Text;
                    PlanList[dataGridPromoPlan.SelectedIndex].FCSOURCETYPE = ((ComboBoxItem)PromoPlanIns_Frm.comboBoxSource.SelectedItem).Tag.ToString();
                    PlanList[dataGridPromoPlan.SelectedIndex].FCSOURCE = ((ComboBoxItem)PromoPlanIns_Frm.comboBoxSource.SelectedItem).Content.ToString();
                    PlanList[dataGridPromoPlan.SelectedIndex].FSPLAY_COUNT_MAX = PromoPlanIns_Frm.textBoxMinCount.Text;
                    PlanList[dataGridPromoPlan.SelectedIndex].FSPLAY_COUNT_MIN = PromoPlanIns_Frm.textBoxMaxCount.Text;
                    PlanList[dataGridPromoPlan.SelectedIndex].FNPROMO_TYPEA = PromoPlanIns_Frm.textBoxTypeACount.Text;
                    PlanList[dataGridPromoPlan.SelectedIndex].FNPROMO_TYPEB = PromoPlanIns_Frm.textBoxTypeBCount.Text;
                    PlanList[dataGridPromoPlan.SelectedIndex].FNPROMO_TYPEC = PromoPlanIns_Frm.textBoxTypeCCount.Text;
                    PlanList[dataGridPromoPlan.SelectedIndex].FNPROMO_TYPED = PromoPlanIns_Frm.textBoxTypeDCount.Text;
                    PlanList[dataGridPromoPlan.SelectedIndex].FNPROMO_TYPEE = PromoPlanIns_Frm.textBoxTypeECount.Text;
                    PlanList[dataGridPromoPlan.SelectedIndex].FSPROMO_BEG_DATE = PromoPlanIns_Frm.datePickerBDate.Text;
                    PlanList[dataGridPromoPlan.SelectedIndex].FSPROMO_END_DATE = PromoPlanIns_Frm.datePickerEDate.Text;
                    PlanList[dataGridPromoPlan.SelectedIndex].FSWEEK = PromoPlanIns_Frm._week;
                    PlanList[dataGridPromoPlan.SelectedIndex].FSMEMO = PromoPlanIns_Frm.textBoxMemo.Text;
                    dataGridPromoPlan.ItemsSource = null;
                    dataGridPromoPlan.ItemsSource = PlanList;
                }
            };
        }

        private void btnPROGDelete_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridPromoPlan.SelectedIndex < 0)
            {
                MessageBox.Show("請先選擇一筆資料,再進行刪除");
                return;
            }
            WSPGMSendSQL.SendSQLSoapClient SP_D_TBPGM_PROMO_PLAN = new WSPGMSendSQL.SendSQLSoapClient();
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<Data>");
            sb.AppendLine("<FNNO>" + PlanList[dataGridPromoPlan.SelectedIndex].FNNO + "</FNNO>");
            sb.AppendLine("</Data>");

            SP_D_TBPGM_PROMO_PLAN.Do_InsertAsync("SP_D_TBPGM_PROMO_PLAN", sb.ToString());
            SP_D_TBPGM_PROMO_PLAN.Do_InsertCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result == true)
                        MessageBox.Show("刪除成功");
                    else
                        MessageBox.Show("刪除失敗");
                }
            };
            PlanList.RemoveAt(dataGridPromoPlan.SelectedIndex);
            dataGridPromoPlan.ItemsSource = null;
            dataGridPromoPlan.ItemsSource = PlanList;
        }

        private void btnPROGQuery_Click(object sender, RoutedEventArgs e)
        {
            PGM105_03 PromoPlanQuery_Frm = new PGM105_03();

            PromoPlanQuery_Frm.Show();
            PromoPlanQuery_Frm.Closed += (s, args) =>
            {
                if (PromoPlanQuery_Frm.PlanList.Count != 0)
                {
                    PlanList= PromoPlanQuery_Frm.PlanList ;
                   
                    dataGridPromoPlan.ItemsSource = null;
                    dataGridPromoPlan.ItemsSource = PlanList;
                }
            };
        }

    }
}
