﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace PTS_MAM3.PGM
{
    public partial class PGM100_05 : ChildWindow
    {
        //string _Status = "";
        public string _ARC_TYPE = "";
        public PGM100_05()
        {
            InitializeComponent();


            WSPGMSendSQL.SendSQLSoapClient SP_Q_Combo = new WSPGMSendSQL.SendSQLSoapClient();

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";

            sb.Clear();
            sb.AppendLine("<Data>");
            //sb.AppendLine("<FSPROMOTYPEID>02</FSPROMOTYPEID>");
            sb.AppendLine("</Data>");

            WSPGMSendSQL.SendSQLSoapClient SP_Q_Audio = new WSPGMSendSQL.SendSQLSoapClient();
            ComboBoxItem TempComboBoxItem1 = new ComboBoxItem();
            TempComboBoxItem1.Content = "";
            TempComboBoxItem1.Tag = "";

            ComboBoxItem TempComboBoxItem2 = new ComboBoxItem();
            TempComboBoxItem2.Content = "";
            TempComboBoxItem2.Tag = "";

            ComboBoxItem TempComboBoxItem3 = new ComboBoxItem();
            TempComboBoxItem3.Content = "";
            TempComboBoxItem3.Tag = "";

            ComboBoxItem TempComboBoxItem4 = new ComboBoxItem();
            TempComboBoxItem4.Content = "";
            TempComboBoxItem4.Tag = "";

            comboBoxCH1.Items.Add(TempComboBoxItem1);
            comboBoxCH2.Items.Add(TempComboBoxItem2);
            comboBoxCH3.Items.Add(TempComboBoxItem3);
            comboBoxCH4.Items.Add(TempComboBoxItem4);
            StringBuilder sbA = new StringBuilder();
            sbA.AppendLine("<Data>");
            sbA.AppendLine("</Data>");
            SP_Q_Audio.Do_QueryAsync("SP_Q_TBZPROGLANG_ALL", sbA.ToString(), ReturnXML);
            SP_Q_Audio.Do_QueryCompleted += (s1, args1) =>
            {
                if (args1.Error == null)
                {
                    if (args1.Result != null && args1.Result != "")
                    {
                        byte[] byteArray = Encoding.Unicode.GetBytes(args1.Result);
                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            ComboBoxItem TempComboBoxItem11 = new ComboBoxItem();
                            TempComboBoxItem11.Content = elem.Element("NAME").Value.ToString();
                            TempComboBoxItem11.Tag = elem.Element("ID").Value.ToString();
                            comboBoxCH1.Items.Add(TempComboBoxItem11);

                            ComboBoxItem TempComboBoxItem12 = new ComboBoxItem();
                            TempComboBoxItem12.Content = elem.Element("NAME").Value.ToString();
                            TempComboBoxItem12.Tag = elem.Element("ID").Value.ToString();
                            comboBoxCH2.Items.Add(TempComboBoxItem12);

                            ComboBoxItem TempComboBoxItem13 = new ComboBoxItem();
                            TempComboBoxItem13.Content = elem.Element("NAME").Value.ToString();
                            TempComboBoxItem13.Tag = elem.Element("ID").Value.ToString();
                            comboBoxCH3.Items.Add(TempComboBoxItem13);

                            ComboBoxItem TempComboBoxItem14 = new ComboBoxItem();
                            TempComboBoxItem14.Content = elem.Element("NAME").Value.ToString();
                            TempComboBoxItem14.Tag = elem.Element("ID").Value.ToString();
                            comboBoxCH4.Items.Add(TempComboBoxItem14);
                        }
                    }


                    WSPGMSendSQL.SendSQLSoapClient SP_Q_Grade = new WSPGMSendSQL.SendSQLSoapClient();
                    //ComboBoxItem TempComboBoxItemGrade = new ComboBoxItem();
                    //TempComboBoxItemGrade.Content = "";
                    //TempComboBoxItemGrade.Tag = "";
                    //comboBoxGrade.Items.Add(TempComboBoxItemGrade);

                    StringBuilder sbB = new StringBuilder();
                    string [] tmpStr;
                    sbB.AppendLine("<Data>");
                    sbB.AppendLine("</Data>");
                    SP_Q_Grade.Do_QueryAsync("SP_Q_TBZPROGGRADE", sbB.ToString(), ReturnXML);
                    SP_Q_Grade.Do_QueryCompleted += (s2, args2) =>
                    {
                        if (args2.Error == null)
                        {
                            if (args2.Result != null && args2.Result != "")
                            {

                                byte[] byteArray2 = Encoding.Unicode.GetBytes(args2.Result);
                                StringBuilder output2 = new StringBuilder();

                                XDocument doc2 = XDocument.Load(new MemoryStream(byteArray2));
                                var ltox2 = from str in doc2.Elements("Datas").Elements("Data")
                                            select str;
                                foreach (XElement elem in ltox2)
                                {
                                    ComboBoxItem TempComboBoxItemGrade1 = new ComboBoxItem();
                                    if (elem.Element("NAME").Value.ToString() != "")
                                    {
                                        //TempComboBoxItemGrade1.Content = elem.Element("NAME").Value.ToString().Substring(0, 3);
                                        tmpStr = elem.Element("NAME").Value.ToString().Split('：');
                                        TempComboBoxItemGrade1.Content = tmpStr[0];
                                    }
                                    else
                                        TempComboBoxItemGrade1.Content = elem.Element("NAME").Value.ToString();
                                    TempComboBoxItemGrade1.Tag = elem.Element("ID").Value.ToString();
                                    comboBoxGrade.Items.Add(TempComboBoxItemGrade1);


                                }



                                WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPROG_D = new WSPGMSendSQL.SendSQLSoapClient();
                                StringBuilder sbC = new StringBuilder();
                                sbC.AppendLine("<Data>");
                                sbC.AppendLine("<FSPROG_ID>" + textBoxProgID.Text + "</FSPROG_ID>");
                                sbC.AppendLine("<FNEPISODE>" + textBoxEpisode.Text + "</FNEPISODE>");
                                sbC.AppendLine("</Data>");
                                SP_Q_TBPROG_D.Do_QueryAsync("SP_Q_TBPROG_D", sbC.ToString(), ReturnXML);
                                SP_Q_TBPROG_D.Do_QueryCompleted += (s3, args3) =>
                                {
                                    if (args3.Error == null)
                                    {
                                        if (args3.Result != null && args3.Result != "")
                                        {

                                            byte[] byteArray3 = Encoding.Unicode.GetBytes(args3.Result);
                                            StringBuilder output3 = new StringBuilder();

                                            XDocument doc3 = XDocument.Load(new MemoryStream(byteArray3));
                                            var ltox3 = from str in doc3.Elements("Datas").Elements("Data")
                                                        select str;
                                            foreach (XElement elem in ltox3)
                                            {
                                                if (elem.Element("FSPROGGRADEID").Value.ToString() != "")
                                                {
                                                    comboBoxGrade.SelectedIndex = -1;
                                                    for (int Combocount = 0; Combocount < comboBoxGrade.Items.Count; Combocount++)
                                                    {
                                                        if (((ComboBoxItem)comboBoxGrade.Items[Combocount]).Tag.ToString() == elem.Element("FSPROGGRADEID").Value.ToString())
                                                        {
                                                            comboBoxGrade.SelectedIndex = Combocount;
                                                        }
                                                    }
                                                    if (comboBoxGrade.SelectedIndex == -1)
                                                        MessageBox.Show("找不到對應的級別設定");

                                                }
                                                else
                                                    comboBoxGrade.SelectedIndex = -1;

                                            }

                                            if (_ARC_TYPE != "")
                                            {
                                                if (_ARC_TYPE == "001")
                                                {
                                                    radioButtonSD.IsChecked = true;
                                                }
                                                else if (_ARC_TYPE == "002")
                                                {
                                                    radioButtonHD.IsChecked = true;
                                                }

                                            }
                                            else
                                            {
                                                radioButtonSD.IsChecked = true;
                                                //MessageBox.Show("請先選擇規格以便取得設定資料");
                                            }
                                        }
                                    }
                                };


                            }
                        }
                    };

                }
            };

            this.Loaded += new RoutedEventHandler(PGM100_05_Loaded);
        }

        void PGM100_05_Loaded(object sender, RoutedEventArgs e)
        {
            foreach (Control ui in alertGrid.Children)
            {
                if (ui is RadioButton)
                {
                    RadioButton rb = (RadioButton)ui;
                    rb.IsChecked = false;
                }
                else if (ui is TextBox)
                {
                    TextBox tb = (TextBox)ui;
                    tb.Text = "";
                }

                ui.IsEnabled = false;
            }
        }

        void ResetToDefaultValue()
        {
            foreach (UIElement ui in this.LayoutRoot.Children)
            {

                if (ui is RadioButton)
                {
                    RadioButton RB = (RadioButton)ui;
                    string rbContent = RB.Content.ToString();
                    if (rbContent == "不要")
                    {
                        RB.IsChecked = true;
                    }
                    else if (rbContent == "SD" || rbContent == "HD")
                    {
                        continue;
                    }
                    else
                    {
                        RB.IsChecked = false;
                    }
                }
            }
        }

        public void QueryData()
        {
            WSPGMSendSQL.SendSQLSoapClient SP_Q_INSERT_TAPE = new WSPGMSendSQL.SendSQLSoapClient();

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");

            sb.AppendLine("<FSPROG_ID>" + textBoxProgID.Text + "</FSPROG_ID>");
            sb.AppendLine("<FNEPISODE>" + textBoxEpisode.Text + "</FNEPISODE>");
            sb.AppendLine("<FSARC_TYPE>" + _ARC_TYPE + "</FSARC_TYPE>");
            sb.AppendLine("</Data>");

            SP_Q_INSERT_TAPE.Do_QueryAsync("SP_Q_TBPGM_INSERT_TAPE", sb.ToString(), ReturnXML);

            SP_Q_INSERT_TAPE.Do_QueryCompleted += (s, args) =>
            {
                #region 查詢主控播出提示結果
                if (args.Error == null)
                {
                    #region 主控播出提示
                    if (args.Result != null && args.Result != "")
                    {

                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            #region 主控插播帶

                            if (elem.Element("FCSMOKE").Value.ToString() == "Y")
                                radioButtonSmokeYes.IsChecked = true;
                            else
                                radioButtonSmokeNO.IsChecked = true;

                            if (elem.Element("FCALERTS").Value.ToString() == "Y")
                                radioButtonAlertYes.IsChecked = true;
                            else
                                radioButtonAlertNO.IsChecked = true;

                            if (elem.Element("FSINSERT_TAPE").Value.ToString() == "1")
                                radioButtonAlert1.IsChecked = true;
                            else if (elem.Element("FSINSERT_TAPE").Value.ToString() == "2")
                                radioButtonAlert2.IsChecked = true;
                            else if (elem.Element("FSINSERT_TAPE").Value.ToString() == "3")
                                radioButtonAlert3.IsChecked = true;
                            else if (elem.Element("FSINSERT_TAPE").Value.ToString() == "4")
                                radioButtonAlert4.IsChecked = true;
                            else if (elem.Element("FSINSERT_TAPE").Value.ToString() == "5")
                                radioButtonAlert5.IsChecked = true;
                            else if (elem.Element("FSINSERT_TAPE").Value.ToString() == "6")
                                radioButtonAlert6.IsChecked = true;
                            else if (elem.Element("FSINSERT_TAPE").Value.ToString() == "7")
                                radioButtonAlert7.IsChecked = true;
                            else
                            {
                                radioButtonAlert1.IsChecked = false;
                                radioButtonAlert2.IsChecked = false;
                                radioButtonAlert3.IsChecked = false;
                                radioButtonAlert4.IsChecked = false;
                                radioButtonAlert5.IsChecked = false;
                                radioButtonAlert6.IsChecked = false;
                                radioButtonAlert7.IsChecked = false;
                            }
                            textBoxAlert7Name.Text = elem.Element("FSINSERT_TAPE_NAME").Value.ToString();
                            textBoxAlert7Dur.Text = elem.Element("FSINSERT_TAPE_DUR").Value.ToString();


                            if (elem.Element("FSTITLE").Value.ToString() == "Y")
                                radioButtonTitleYes.IsChecked = true;
                            else
                                radioButtonTitleNO.IsChecked = true;
                            textBoxTitleName.Text = elem.Element("FSTITLE_NAME").Value.ToString();
                            textBoxTitleDur.Text = elem.Element("FSTITLE_DUR").Value.ToString();

                            #endregion
                            #region 主控鏡面


                            if (elem.Element("FCPROG_SIDE_LABEL").Value.ToString() == "Y")
                                radioButtonProgSideLabelYes.IsChecked = true;
                            else
                                radioButtonProgSideLabelNO.IsChecked = true;

                            if (elem.Element("FCLIVE_SIDE_LABEL").Value.ToString() == "Y")
                                radioButtonLiveSideLabelYes.IsChecked = true;
                            else
                                radioButtonLiveSideLabelNO.IsChecked = true;

                            if (elem.Element("FCTIME_SIDE_LABEL").Value.ToString() == "Y")
                                radioButtonTimeSideLabelYes.IsChecked = true;
                            else
                                radioButtonTimeSideLabelNO.IsChecked = true;

                            if (elem.Element("FCREPLAY").Value.ToString() == "Y")
                                radioButtonReplayYes.IsChecked = true;
                            else
                                radioButtonReplayNO.IsChecked = true;

                            if (elem.Element("FCRECORD_PLAY").Value.ToString() == "Y")
                                radioButtonRecordPlayYes.IsChecked = true;
                            else
                                radioButtoRecordPlayNO.IsChecked = true;

                            if (elem.Element("FCSTEREO").Value.ToString() == "Y")
                                radioButtonStereoYes.IsChecked = true;
                            else
                                radioButtonStereoNO.IsChecked = true;

                            if (elem.Element("FCLOGO").Value.ToString() == "L")
                                radioButtonLogoLeft.IsChecked = true;
                            else if (elem.Element("FCLOGO").Value.ToString() == "R")
                                radioButtonLogoRight.IsChecked = true;
                            else
                                radioButtonLogoNO.IsChecked = true;

                            if (elem.Element("FSCHNAME1").Value.ToString() != "")
                                ComboBoxCheckSelectItem(elem.Element("FSCHNAME1").Value.ToString(), comboBoxCH1, "CH1");
                            else
                                comboBoxCH1.SelectedIndex = -1;

                            if (elem.Element("FSCHNAME2").Value.ToString() != "")
                                ComboBoxCheckSelectItem(elem.Element("FSCHNAME2").Value.ToString(), comboBoxCH2, "CH2");
                            else
                                comboBoxCH2.SelectedIndex = -1;

                            if (elem.Element("FSCHNAME3").Value.ToString() != "")
                                ComboBoxCheckSelectItem(elem.Element("FSCHNAME3").Value.ToString(), comboBoxCH3, "CH3");
                            else
                                comboBoxCH3.SelectedIndex = -1;

                            if (elem.Element("FSCHNAME4").Value.ToString() != "")
                                ComboBoxCheckSelectItem(elem.Element("FSCHNAME4").Value.ToString(), comboBoxCH4, "CH4");
                            else
                                comboBoxCH4.SelectedIndex = -1;
                            #endregion

                        }
                    }
                    else  //如果沒有查到資料則不處理
                    {
                        MessageBox.Show("此資料尚未設定播出提示");
                        ResetToDefaultValue();
                    }
                    #endregion

                }

                #endregion
            };
        }

        public void ComboBoxCheckSelectItem(string Instring, ComboBox SelectComboBox, String SelectText)
        {
            SelectComboBox.SelectedIndex = -1;
            for (int Combocount = 0; Combocount < SelectComboBox.Items.Count; Combocount++)
            {
                if (((ComboBoxItem)SelectComboBox.Items[Combocount]).Content.ToString() == Instring)
                {
                    SelectComboBox.SelectedIndex = Combocount;
                }
            }
            if (SelectComboBox.SelectedIndex == -1)
                MessageBox.Show("找不到對應的" + SelectText + "設定");
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (CheckData() == false)
            {
                return;
            }
            SaveData();
            this.DialogResult = true;
        }

        public bool CheckData()
        {
            if (radioButtonSD.IsChecked == false && radioButtonHD.IsChecked == false)
            {
                MessageBox.Show("請先選擇規格");
                return false;
            }

            if (radioButtonSmokeYes.IsChecked == false && radioButtonSmokeNO.IsChecked == false)
            {
                MessageBox.Show("請先選擇吸菸卡狀態");
                return false;
            }
            if (radioButtonAlertYes.IsChecked == true)
            {
                //MessageBox.Show("請先選擇警示卡狀態");
                bool checkVersion = false;
                foreach (UIElement ui in alertGrid.Children)
                {
                    if (ui is RadioButton)
                    {
                        RadioButton rb = (RadioButton)ui;
                        if (rb.IsChecked == true)
                        {
                            checkVersion = true;
                            break;
                        }
                    }
                }

                if (!checkVersion)
                {
                    MessageBox.Show("請先選擇警示卡版本");
                    return false;
                }
                //return false;
            }

            

            if (radioButtonAlert7.IsChecked == true)
            {
                if (textBoxAlert7Name.Text == "")
                {
                    MessageBox.Show("請先輸入客製版名稱");
                    return false;
                }
                if (textBoxAlert7Dur.Text == "")
                {
                    MessageBox.Show("請先輸入客製版長度");
                    return false;
                }
                try
                {
                    int.Parse(textBoxAlert7Dur.Text);
                }
                catch
                {
                    MessageBox.Show("客製版長度格式有誤");
                    return false;
                }
            }
            else
            {
                if (textBoxAlert7Name.Text != "" || (textBoxAlert7Dur.Text != "" && textBoxAlert7Dur.Text != "0"))
                {
                    MessageBox.Show("您並未選擇客製版,輸入的名稱和長度並無效用");
                    return false;
                }
            }

            if (radioButtonTitleYes.IsChecked == true)
            {
                if (textBoxTitleName.Text == "")
                {
                    MessageBox.Show("請先輸入檔名總片頭名稱");
                    return false;
                }
                if (textBoxTitleDur.Text == "")
                {
                    MessageBox.Show("請先輸入檔名總片頭長度");
                    return false;
                }
                try
                {
                    int.Parse(textBoxTitleDur.Text);
                }
                catch
                {
                    MessageBox.Show("檔名總片頭長度格式有誤");
                    return false;
                }
            }
            else
            {
                if (textBoxTitleName.Text != "" || (textBoxTitleDur.Text != "" && textBoxTitleDur.Text != "0"))
                {
                    MessageBox.Show("您並未選擇檔名總片頭,輸入的名稱和長度並無效用");
                    return false;
                }
            }

            //if (radioButtonAlertNO.IsChecked.Value)
            //{
            //    bool isChecked = radioButtonAlert1.IsChecked.Value && radioButtonAlert2.IsChecked.Value && radioButtonAlert3.IsChecked.Value && radioButtonAlert4.IsChecked.Value && radioButtonAlert5.IsChecked.Value && radioButtonAlert6.IsChecked.Value;

            //    if (!isChecked)
            //    {
            //        MessageBox.Show("您並未選擇檔名總片頭,輸入的名稱和長度並無效用");
            //        return false;
            //    }
            //}



            return true;
        }

        public void SaveData()
        {

            WSPGMSendSQL.SendSQLSoapClient SP_I_INSERT_TAPE = new WSPGMSendSQL.SendSQLSoapClient();
            StringBuilder sb = new StringBuilder();
            StringBuilder sbDel = new StringBuilder();


            if (radioButtonSD.IsChecked == true)
                _ARC_TYPE = "001";
            else
                _ARC_TYPE = "002";
            sbDel.AppendLine("<Data>");
            sbDel.AppendLine("<FSPROG_ID>" + textBoxProgID.Text + "</FSPROG_ID>");
            sbDel.AppendLine("<FNEPISODE>" + textBoxEpisode.Text + "</FNEPISODE>");
            sbDel.AppendLine("<FSARC_TYPE>" + _ARC_TYPE + "</FSARC_TYPE>");

            sbDel.AppendLine("</Data>");

            object ReturnXML = "";
            sb.AppendLine("<Datas>");
            sb.AppendLine("<Data>");

            sb.AppendLine("<FSPROG_ID>" + textBoxProgID.Text + "</FSPROG_ID>");
            sb.AppendLine("<FNEPISODE>" + textBoxEpisode.Text + "</FNEPISODE>");
            if (radioButtonSD.IsChecked == true)
                sb.AppendLine("<FSARC_TYPE>" + "001" + "</FSARC_TYPE>");
            else
                sb.AppendLine("<FSARC_TYPE>" + "002" + "</FSARC_TYPE>");
            if (radioButtonSmokeYes.IsChecked == true)
                sb.AppendLine("<FCSMOKE>" + "Y" + "</FCSMOKE>");
            else
                sb.AppendLine("<FCSMOKE>" + "N" + "</FCSMOKE>");
            if (radioButtonAlertYes.IsChecked == true)
                sb.AppendLine("<FCALERTS>" + "Y" + "</FCALERTS>");
            else
                sb.AppendLine("<FCALERTS>" + "N" + "</FCALERTS>");

            if (radioButtonAlert1.IsChecked == true)
                sb.AppendLine("<FSINSERT_TAPE>" + "1" + "</FSINSERT_TAPE>");
            else if (radioButtonAlert2.IsChecked == true)
                sb.AppendLine("<FSINSERT_TAPE>" + "2" + "</FSINSERT_TAPE>");
            else if (radioButtonAlert3.IsChecked == true)
                sb.AppendLine("<FSINSERT_TAPE>" + "3" + "</FSINSERT_TAPE>");
            else if (radioButtonAlert4.IsChecked == true)
                sb.AppendLine("<FSINSERT_TAPE>" + "4" + "</FSINSERT_TAPE>");
            else if (radioButtonAlert5.IsChecked == true)
                sb.AppendLine("<FSINSERT_TAPE>" + "5" + "</FSINSERT_TAPE>");
            else if (radioButtonAlert6.IsChecked == true)
                sb.AppendLine("<FSINSERT_TAPE>" + "6" + "</FSINSERT_TAPE>");
            else if (radioButtonAlert7.IsChecked == true)
                sb.AppendLine("<FSINSERT_TAPE>" + "7" + "</FSINSERT_TAPE>");
            else
                sb.AppendLine("<FSINSERT_TAPE>" + "" + "</FSINSERT_TAPE>");

            sb.AppendLine("<FSINSERT_TAPE_NAME>" + textBoxAlert7Name.Text + "</FSINSERT_TAPE_NAME>");
            sb.AppendLine("<FSINSERT_TAPE_DUR>" + textBoxAlert7Dur.Text + "</FSINSERT_TAPE_DUR>");
            sb.AppendLine("<FSTITLE_NAME>" + textBoxTitleName.Text + "</FSTITLE_NAME>");
            sb.AppendLine("<FSTITLE_DUR>" + textBoxTitleDur.Text + "</FSTITLE_DUR>");
            if (radioButtonTitleYes.IsChecked == true)
                sb.AppendLine("<FSTITLE>" + "Y" + "</FSTITLE>");
            else
                sb.AppendLine("<FSTITLE>" + "N" + "</FSTITLE>");

            if (radioButtonProgSideLabelYes.IsChecked == true)
                sb.AppendLine("<FCPROG_SIDE_LABEL>" + "Y" + "</FCPROG_SIDE_LABEL>");
            else
                sb.AppendLine("<FCPROG_SIDE_LABEL>" + "N" + "</FCPROG_SIDE_LABEL>");

            if (radioButtonLiveSideLabelYes.IsChecked == true)
                sb.AppendLine("<FCLIVE_SIDE_LABEL>" + "Y" + "</FCLIVE_SIDE_LABEL>");
            else
                sb.AppendLine("<FCLIVE_SIDE_LABEL>" + "N" + "</FCLIVE_SIDE_LABEL>");

            if (radioButtonTimeSideLabelYes.IsChecked == true)
                sb.AppendLine("<FCTIME_SIDE_LABEL>" + "Y" + "</FCTIME_SIDE_LABEL>");
            else
                sb.AppendLine("<FCTIME_SIDE_LABEL>" + "N" + "</FCTIME_SIDE_LABEL>");

            if (radioButtonReplayYes.IsChecked == true)
                sb.AppendLine("<FCREPLAY>" + "Y" + "</FCREPLAY>");
            else
                sb.AppendLine("<FCREPLAY>" + "N" + "</FCREPLAY>");

            if (radioButtonRecordPlayYes.IsChecked == true)
                sb.AppendLine("<FCRECORD_PLAY>" + "Y" + "</FCRECORD_PLAY>");
            else
                sb.AppendLine("<FCRECORD_PLAY>" + "N" + "</FCRECORD_PLAY>");

            if (radioButtonStereoYes.IsChecked == true)
                sb.AppendLine("<FCSTEREO>" + "Y" + "</FCSTEREO>");
            else
                sb.AppendLine("<FCSTEREO>" + "N" + "</FCSTEREO>");

            if (radioButtonLogoLeft.IsChecked == true)
                sb.AppendLine("<FCLOGO>" + "L" + "</FCLOGO>");
            else if (radioButtonLogoRight.IsChecked == true)
                sb.AppendLine("<FCLOGO>" + "R" + "</FCLOGO>");
            else
                sb.AppendLine("<FCLOGO>" + "N" + "</FCLOGO>");

            if (comboBoxCH1.SelectedIndex > 0)
                sb.AppendLine("<FSCH1>" + ((ComboBoxItem)comboBoxCH1.SelectedItem).Tag.ToString() + "</FSCH1>");
            else
                sb.AppendLine("<FSCH1>" + "" + "</FSCH1>");

            if (comboBoxCH2.SelectedIndex > 0)
                sb.AppendLine("<FSCH2>" + ((ComboBoxItem)comboBoxCH2.SelectedItem).Tag.ToString() + "</FSCH2>");
            else
                sb.AppendLine("<FSCH2>" + "" + "</FSCH2>");

            if (comboBoxCH3.SelectedIndex > 0)
                sb.AppendLine("<FSCH3>" + ((ComboBoxItem)comboBoxCH3.SelectedItem).Tag.ToString() + "</FSCH3>");
            else
                sb.AppendLine("<FSCH3>" + "" + "</FSCH3>");

            if (comboBoxCH4.SelectedIndex > 0)
                sb.AppendLine("<FSCH4>" + ((ComboBoxItem)comboBoxCH4.SelectedItem).Tag.ToString() + "</FSCH4>");
            else
                sb.AppendLine("<FSCH4>" + "" + "</FSCH4>");

            sb.AppendLine("<FSCREATED_BY>" + UserClass.userData.FSUSER_ID.ToString() + "</FSCREATED_BY>");

            sb.AppendLine("</Data>");
            sb.AppendLine("</Datas>");




            //sb=sb.Replace("\r\n", "");

            SP_I_INSERT_TAPE.Do_Insert_ListAsync("SP_D_TBPGM_INSERT_TAPE", sbDel.ToString(), "SP_I_TBPGM_INSERT_TAPE", sb.ToString());
            SP_I_INSERT_TAPE.Do_Insert_ListCompleted += (s, args) =>
            {
                if (args.Error == null)
                {

                    if (args.Result == true)
                    {
                        MessageBox.Show("新增成功");
                    }
                    else
                        MessageBox.Show("新增失敗");
                }
            };




        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
        string _PromoTypeID = "";
        string _PromoTypeName = "";

        private void radioButtonSD_Checked(object sender, RoutedEventArgs e)
        {

            if (radioButtonHD.IsChecked == true)
            {

                _ARC_TYPE = "002";
                QueryData();
            }
            else if (radioButtonSD.IsChecked == true)
            {

                _ARC_TYPE = "001";
                QueryData();
            }

        }

        private void radioButtonHD_Checked(object sender, RoutedEventArgs e)
        {
            if (radioButtonHD.IsChecked == true)
            {
                _ARC_TYPE = "002";
                QueryData();
            }
            else if (radioButtonSD.IsChecked == true)
            {
                _ARC_TYPE = "001";
                QueryData();
            }

        }

        private void buttonUpdateGrade_Click(object sender, RoutedEventArgs e)
        {


            if (((ComboBoxItem)this.comboBoxGrade.SelectedItem).Tag.ToString() == "")
            {
                MessageBox.Show("請先選擇級別");
                return;
            }

            WSPGMSendSQL.SendSQLSoapClient SP_U_TBPROG_D_GRADE_ID = new WSPGMSendSQL.SendSQLSoapClient();
            StringBuilder sbUpdateGrade = new StringBuilder();
            sbUpdateGrade.AppendLine("<Data>");
            sbUpdateGrade.AppendLine("<FSPROG_ID>" + this.textBoxProgID.Text + "</FSPROG_ID>");
            sbUpdateGrade.AppendLine("<FNEPISODE>" + this.textBoxEpisode.Text + "</FNEPISODE>");
            sbUpdateGrade.AppendLine("<FSPROGGRADEID>" + ((ComboBoxItem)this.comboBoxGrade.SelectedItem).Tag.ToString() + "</FSPROGGRADEID>");
            sbUpdateGrade.AppendLine("<FSUPDUSER>" + UserClass.userData.FSUSER_ID + "</FSUPDUSER>");

            sbUpdateGrade.AppendLine("</Data>");

            SP_U_TBPROG_D_GRADE_ID.Do_InsertAsync("SP_U_TBPROG_D_GRADE_ID", sbUpdateGrade.ToString());
            SP_U_TBPROG_D_GRADE_ID.Do_InsertCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result == true)
                    {

                        MessageBox.Show("修改節目集數級別完成");
                    }
                    else
                        MessageBox.Show("修改節目集數級別失敗");
                }
            };
        }

        private void radioButtonAlertNO_Checked(object sender, RoutedEventArgs e)
        {
            if (this.radioButtonAlertNO != null)
            {
                foreach (Control ui in alertGrid.Children)
                {
                    if (ui is RadioButton)
                    {
                        RadioButton rb = (RadioButton)ui;
                        rb.IsChecked = false;
                    }
                    else if (ui is TextBox)
                    {
                        TextBox tb = (TextBox)ui;
                        tb.Text = "";
                    }

                    ui.IsEnabled = false;
                }

            }
        }

        private void radioButtonAlertYes_Checked(object sender, RoutedEventArgs e)
        {
            if (this.radioButtonAlertYes != null)
            {
                foreach (Control ui in alertGrid.Children)
                {
                    ui.IsEnabled = true;
                }

                radioButtonAlert1.IsChecked = true;
            }
        }
    }
}

