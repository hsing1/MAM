﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace PTS_MAM3.PGM
{
    public partial class PGM120 : Page
    {
        public PGM120()
        {
            InitializeComponent();
            if (ModuleClass.getModulePermission("P2" + "01" + "002") == false)
            {
                MessageBox.Show("您沒有維護節目預排短帶的權限");

               
            }
            
            
        }



        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }
        
        List<PromoProgLinkList> Promo_Prog_Link = new List<PromoProgLinkList>();
        List<PromoProgLinkDetail> Promo_Prog_Link_Detail = new List<PromoProgLinkDetail>();
        
        private void buttonQueryProg_Click(object sender, RoutedEventArgs e)
        {
            
            PGM120_01 ProgQuery_Frm = new PGM120_01();

            ProgQuery_Frm.Show();
            ProgQuery_Frm.Closed += (s, args) =>
            {
                if (ProgQuery_Frm._progid != "")
                {
                    textBoxProgID.Text = ProgQuery_Frm._progid;
                    textBoxProgSeqno.Text = ProgQuery_Frm._progseqno;
                    textBoxProgName.Text = ProgQuery_Frm._progname;
                    textBoxChannelID.Text = ProgQuery_Frm._ChannelID;
                    textBoxChannelName.Text = ProgQuery_Frm._ChannelName;
                    
                    WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_PROG_PROMO_LINK = new WSPGMSendSQL.SendSQLSoapClient();
                    //string ReturnStr = "";

                    StringBuilder sb = new StringBuilder();
                    object ReturnXML = "";
                    sb.AppendLine("<Data>");

                    sb.AppendLine("<FSPROG_ID>" + textBoxProgID.Text + "</FSPROG_ID>");
                    sb.AppendLine("<FNSEQNO>" + textBoxProgSeqno.Text + "</FNSEQNO>");
                    sb.AppendLine("<FSCHANNEL_ID>" + textBoxChannelID.Text + "</FSCHANNEL_ID>");
                    sb.AppendLine("</Data>");
                    SP_Q_TBPGM_PROG_PROMO_LINK.Do_QueryAsync("SP_Q_TBPGM_PROG_PROMO_LINK", sb.ToString(), ReturnXML);
                    //SP_Q_TBPGM_QUEUE.Do_QueryCompleted += new EventHandler<SendSQL.Do_QueryCompletedEventArgs>(SP_Q_TBPGM_QUEUE_Do_QueryCompleted);
                    //AAA.Do_InsertAsync("", "", ReturnStr);
                    SP_Q_TBPGM_PROG_PROMO_LINK.Do_QueryCompleted += (s1, args1) =>
                    {
                        if (args1.Error == null)
                        {
                            if ((args1.Result != null) && (args1.Result != ""))
                            {
                                byte[] byteArray = Encoding.Unicode.GetBytes(args1.Result);
                                Promo_Prog_Link.Clear();
                                StringBuilder output = new StringBuilder();
                                // Create an XmlReader
                                XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                                var ltox = from str in doc.Elements("Datas").Elements("Data")
                                           select str;
                                foreach (XElement elem in ltox)
                                {
                                    Promo_Prog_Link.Add(new PromoProgLinkList()
                                    {
                                        FSPROMO_ID = elem.Element("FSPROMO_ID").Value.ToString(),
                                        FSPROMO_NAME = elem.Element("FSPROMO_NAME").Value.ToString(),
                                        //FSCHANNEL_ID = elem.Element("FSCHANNEL_ID").Value.ToString(),
                                        //FSCHANNEL_NAME = elem.Element("FSCHANNEL_NAME").Value.ToString(),
                                        FNNO = elem.Element("FNNO").Value.ToString(),
                                        //FCLINK_TYPE = elem.Element("FCLINK_TYPE").Value.ToString(),
                                        //FCTIME_TYPE = elem.Element("FCTIME_TYPE").Value.ToString(),
                                        //FCINSERT_TYPE = elem.Element("FCINSERT_TYPE").Value.ToString(),
                                    });
                                }
                                dataGridQueue.ItemsSource = null;
                                dataGridQueue.ItemsSource = Promo_Prog_Link;
                            }
                            else
                            {
                                dataGridQueue.ItemsSource = null;
                            }
                        }
                        #region 開始查詢所有此節目的連結資料
                        WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_PROG_PROMO_LINK_DETAIL = new WSPGMSendSQL.SendSQLSoapClient();
                        //string ReturnStr = "";

                        StringBuilder sb1 = new StringBuilder();
                        object ReturnXML1 = "";
                        sb1.AppendLine("<Data>");

                        sb1.AppendLine("<FSPROG_ID>" + textBoxProgID.Text + "</FSPROG_ID>");
                        sb1.AppendLine("</Data>");
                        SP_Q_TBPGM_PROG_PROMO_LINK_DETAIL.Do_QueryAsync("SP_Q_TBPGM_PROG_PROMO_LINK_DETAIL", sb1.ToString(), ReturnXML1);

                        SP_Q_TBPGM_PROG_PROMO_LINK_DETAIL.Do_QueryCompleted += (s2, args2) =>
                        {
                            if (args2.Error == null)
                            {
                                if ((args2.Result != null) && (args2.Result != ""))
                                {
                                    byte[] byteArray = Encoding.Unicode.GetBytes(args2.Result);
                                    Promo_Prog_Link_Detail.Clear();
                                    StringBuilder output = new StringBuilder();
                                    // Create an XmlReader
                                    XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                                    var ltox = from str in doc.Elements("Datas").Elements("Data")
                                               select str;
                                    foreach (XElement elem in ltox)
                                    {
                                        string Kind="";
                                        if (elem.Element("FSCHANNEL_NAME").Value.ToString() != "")
                                            Kind = "節目頻道連結(只有在節目排播在此頻道時才會連結此宣傳帶)";
                                        else if (elem.Element("FNSEQNO").Value.ToString() != "-1")
                                            Kind = "節目時段規劃連結(只有在節目排播在此時段規劃資料時才會連結此宣傳帶)";
                                        else
                                            Kind = "節目連結(只要有排播此節目都會連結此宣傳帶)";

                                        Promo_Prog_Link_Detail.Add(new PromoProgLinkDetail()
                                        {
                                            FSPROMO_ID = elem.Element("FSPROMO_ID").Value.ToString(),
                                            FSPROMO_NAME = elem.Element("FSPROMO_NAME").Value.ToString(),
                                            //FSCHANNEL_ID = elem.Element("FSCHANNEL_ID").Value.ToString(),
                                            //FSCHANNEL_NAME = elem.Element("FSCHANNEL_NAME").Value.ToString(),
                                            FNNO = elem.Element("FNNO").Value.ToString(),
                                            FNSEQNO = elem.Element("FNSEQNO").Value.ToString(),
                                            //FCLINK_TYPE = elem.Element("FCLINK_TYPE").Value.ToString(),
                                            //FCTIME_TYPE = elem.Element("FCTIME_TYPE").Value.ToString(),
                                            //FCINSERT_TYPE = elem.Element("FCINSERT_TYPE").Value.ToString(),
                                            FSCHANNEL = elem.Element("FSCHANNEL_NAME").Value.ToString(),
                                            FSKIND=Kind,

                                        });
                                    }
                                    dataGridAll.ItemsSource = null;
                                    dataGridAll.ItemsSource = Promo_Prog_Link_Detail;
                                }
                                else
                                {
                                    dataGridAll.ItemsSource = null;
                                }
                            }


                        };
                        #endregion


                    };


                }
            };
        }

        private void buttonAdd_Click(object sender, RoutedEventArgs e)
        {
            PGM115_01 PromoQuery_Frm = new PGM115_01();
            PromoQuery_Frm.OKButton.Content = "加入";
            PromoQuery_Frm.Show();
            PromoQuery_Frm.Closed += (s, args) =>
            {
                if ((PromoQuery_Frm._Promo_ID != "")  && (PromoQuery_Frm._Promo_ID != null))
                {
                    //string _TempFCTIME_TYPE = "";
                    //string _TempFCINSERT_TYPE = "";
                    //if (radioButtonAll.IsChecked == true)
                    //    _TempFCTIME_TYPE = "A";
                    //else if (radioButtonONE.IsChecked == true)
                    //    _TempFCTIME_TYPE = "1";
                    //else if (radioButtonTWO.IsChecked == true)
                    //    _TempFCTIME_TYPE = "2";
                    //else if (radioButtonTHREE.IsChecked == true)
                    //    _TempFCTIME_TYPE = "3";

                    //if (radioButtonInsetAll.IsChecked == true)
                    //    _TempFCINSERT_TYPE = "A";
                    //else
                    //    _TempFCINSERT_TYPE = "C";
                    Promo_Prog_Link.Add(new PromoProgLinkList()
                    {
                        FSPROMO_ID = PromoQuery_Frm._Promo_ID,
                        FSPROMO_NAME = PromoQuery_Frm._Promo_Name,
                        //FSCHANNEL_ID = ((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString(),
                        //FSCHANNEL_NAME = ((ComboBoxItem)comboBoxChannel.SelectedItem).Content.ToString(),
                        FNNO = (Promo_Prog_Link.Count + 1).ToString(),
                        //FCLINK_TYPE = ((ComboBoxItem)comboBoxLinkType.SelectedItem).Content.ToString(),
                        //FCTIME_TYPE = _TempFCTIME_TYPE,
                        //FCINSERT_TYPE = _TempFCINSERT_TYPE,
                    });
                    dataGridQueue.ItemsSource = null;
                    dataGridQueue.ItemsSource = Promo_Prog_Link;

                }
            };
        }

        private void buttonRemove_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridQueue.SelectedIndex < 0)
            {
                MessageBox.Show("請先選擇一筆資料,再進行移除");
                return;
            }
            Promo_Prog_Link.RemoveAt(dataGridQueue.SelectedIndex);
            dataGridQueue.ItemsSource = null;
            dataGridQueue.ItemsSource = Promo_Prog_Link;
        }


        public void Write_Promo_Prog_Link()
        { 
            if (Promo_Prog_Link.Count <= 0)
            {
                ////MessageBox.Show("請先加入短帶");
                ////return;
                var res = MessageBox.Show("您沒有加入短帶,請問是否要刪除這個節目連結的宣傳帶?", "", MessageBoxButton.OKCancel);
                if (res.ToString() == "Cancel")
                {
                    return;
                }
            }

            WSPGMSendSQL.SendSQLSoapClient SP_I_TBPGM_PROG_PROMO_LINK = new WSPGMSendSQL.SendSQLSoapClient();
            StringBuilder sb = new StringBuilder();
            StringBuilder sbDel = new StringBuilder();
            sbDel.AppendLine("<Data>");
            sbDel.AppendLine("<FSPROG_ID>" + textBoxProgID.Text + "</FSPROG_ID>");
            sbDel.AppendLine("<FNSEQNO>" + textBoxProgSeqno.Text + "</FNSEQNO>");
            sbDel.AppendLine("<FSCHANNEL_ID>" + textBoxChannelID.Text + "</FSCHANNEL_ID>");
            sbDel.AppendLine("</Data>");


            int Save_NO = 1;
            object ReturnXML = "";
            sb.AppendLine("<Datas>");
            foreach (PromoProgLinkList Temp_Promo_Prog_Link in Promo_Prog_Link)
            {
                sb.AppendLine("<Data>");
                sb.AppendLine("<FSPROG_ID>" + textBoxProgID.Text + "</FSPROG_ID>");
                sb.AppendLine("<FNSEQNO>" + textBoxProgSeqno.Text + "</FNSEQNO>");
                sb.AppendLine("<FSPROMO_ID>" + Temp_Promo_Prog_Link.FSPROMO_ID + "</FSPROMO_ID>");
                sb.AppendLine("<FNNO>" + Save_NO + "</FNNO>");
                Save_NO = Save_NO + 1;
                sb.AppendLine("<FSCREATED_BY>" + UserClass.userData.FSUSER_ID.ToString() + "</FSCREATED_BY>");
                sb.AppendLine("<FSUPDATED_BY>" + UserClass.userData.FSUSER_ID.ToString() + "</FSUPDATED_BY>");
                sb.AppendLine("<FSCHANNEL_ID>" + textBoxChannelID.Text + "</FSCHANNEL_ID>");
                sb.AppendLine("</Data>");

            }
            sb.AppendLine("</Datas>");
            //sb=sb.Replace("\r\n", "");
            SP_I_TBPGM_PROG_PROMO_LINK.Do_Insert_ListAsync("SP_D_TBPGM_PROG_PROMO_LINK", sbDel.ToString(), "SP_I_TBPGM_PROG_PROMO_LINK", sb.ToString());
            SP_I_TBPGM_PROG_PROMO_LINK.Do_Insert_ListCompleted += (s, args) =>
            {
                if (args.Error == null)
                {

                    if (args.Result == true)
                    {
                        MessageBox.Show("新增成功");
                        Promo_Prog_Link.Clear();
                        textBoxProgID.Text = "";
                        textBoxProgSeqno.Text = "";
                        textBoxProgName.Text = "";
                        Save_NO = 1;
                    }
                    else
                        MessageBox.Show("新增失敗");
                }
            };

        }

        private void buttonWrite_Click(object sender, RoutedEventArgs e)
        {
            Write_Promo_Prog_Link();
        }
    }
}
