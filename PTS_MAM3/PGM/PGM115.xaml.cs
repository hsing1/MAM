﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace PTS_MAM3.PGM
{
    public partial class PGM115 : Page
    {
        bool _loadForm = false;
        public PGM115()
        {
            InitializeComponent();
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBZCHANNEL_ALL = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            sb.AppendLine("</Data>");
            SP_Q_TBZCHANNEL_ALL.Do_QueryAsync("[SP_Q_TBZCHANNEL_ALL]", sb.ToString(), ReturnXML);
            //SP_Q_TBPGM_QUEUE.Do_QueryCompleted += new EventHandler<SendSQL.Do_QueryCompletedEventArgs>(SP_Q_TBPGM_QUEUE_Do_QueryCompleted);
            //AAA.Do_InsertAsync("", "", ReturnStr);
            SP_Q_TBZCHANNEL_ALL.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {


                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);

                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            if (ModuleClass.getModulePermission("P2" + elem.Element("ID").Value.ToString() + "001") == true)
                            {

                                ComboBoxItem TempComboBoxItem = new ComboBoxItem();
                                TempComboBoxItem.Content = elem.Element("NAME").Value.ToString();
                                TempComboBoxItem.Tag = elem.Element("ID").Value.ToString();

                                comboBoxChannel.Items.Add(TempComboBoxItem);
                            }
                        }

                    }
                }
            };
            _loadForm = true;
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        List<PromoChannelLinkList> Promo_Channel_Link = new List<PromoChannelLinkList>();
        public void QueryLink()
        {
            if (comboBoxChannel.SelectedIndex == -1)
            {
                //MessageBox.Show("請先選擇要查詢之頻道");
                return;
            }
            if (comboBoxLinkType.SelectedIndex == -1)
            {
                //MessageBox.Show("請先選擇要查詢之頻道");
                return;
            }
            
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_CHANNEL_PROMO_LINK = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            //sb.AppendLine("<FDDATE>" + this.textBoxDate.Text + "</FDDATE>");

            sb.AppendLine("<FSCHANNEL_ID>" + ((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString() +"</FSCHANNEL_ID>");
            sb.AppendLine("<FCLINK_TYPE>" + ((ComboBoxItem)comboBoxLinkType.SelectedItem).Tag.ToString() +"</FCLINK_TYPE>");
            if (radioButtonAll.IsChecked==true)
                sb.AppendLine("<FCTIME_TYPE>" + "A" +"</FCTIME_TYPE>");
            else if (radioButtonONE.IsChecked == true)
                sb.AppendLine("<FCTIME_TYPE>" + "1" + "</FCTIME_TYPE>");
            else if (radioButtonTWO.IsChecked == true)
                sb.AppendLine("<FCTIME_TYPE>" + "2" + "</FCTIME_TYPE>");
            else if (radioButtonTHREE.IsChecked == true)
                sb.AppendLine("<FCTIME_TYPE>" + "3" + "</FCTIME_TYPE>");

            if (radioButtonInsetAll.IsChecked==true)
                sb.AppendLine("<FCINSERT_TYPE>" + "A" + "</FCINSERT_TYPE>");
            else
                sb.AppendLine("<FCINSERT_TYPE>" + "C" +"</FCINSERT_TYPE>");
            sb.AppendLine("</Data>");
            SP_Q_TBPGM_CHANNEL_PROMO_LINK.Do_QueryAsync("SP_Q_TBPGM_CHANNEL_PROMO_LINK", sb.ToString(), ReturnXML);
            //SP_Q_TBPGM_QUEUE.Do_QueryCompleted += new EventHandler<SendSQL.Do_QueryCompletedEventArgs>(SP_Q_TBPGM_QUEUE_Do_QueryCompleted);
            //AAA.Do_InsertAsync("", "", ReturnStr);
            SP_Q_TBPGM_CHANNEL_PROMO_LINK.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    Promo_Channel_Link.Clear();
                    if ((args.Result != null) && (args.Result != ""))
                    {

                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        
                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {

                            //comboBoxChannel.SelectedIndex = -1;
                            //for (int Combocount = 0; Combocount < comboBoxChannel.Items.Count; Combocount++)
                            //{
                            //    if (((ComboBoxItem)comboBoxChannel.Items[Combocount]).Tag.ToString() == elem.Element("FSCHANNEL_ID").Value.ToString())
                            //    {
                            //        comboBoxChannel.SelectedIndex = Combocount;
                            //    }
                            //}
                            //if (comboBoxChannel.SelectedIndex == -1)
                            //    MessageBox.Show("找不到頻道[" + elem.Element("FSCHANNEL_NAME").Value.ToString() + "]");


                            //comboBoxLinkType.SelectedIndex = -1;
                            //for (int Combocount = 0; Combocount < comboBoxLinkType.Items.Count; Combocount++)
                            //{
                            //    if (((ComboBoxItem)comboBoxLinkType.Items[Combocount]).Tag.ToString() == elem.Element("FCLINK_TYPE").Value.ToString())
                            //    {
                            //        comboBoxLinkType.SelectedIndex = Combocount;
                            //    }
                            //}
                            //if (comboBoxLinkType.SelectedIndex == -1)
                            //    MessageBox.Show("找不到連結類型");


                            //if (elem.Element("FCTIME_TYPE").Value.ToString() == "A")
                            //    radioButtonAll.IsChecked = true;
                            //else if (elem.Element("FCTIME_TYPE").Value.ToString() == "1") 
                            //    radioButtonONE.IsChecked = true;
                            //else if (elem.Element("FCTIME_TYPE").Value.ToString() == "2")
                            //    radioButtonTWO.IsChecked = true;
                            //else if (elem.Element("FCTIME_TYPE").Value.ToString() == "3")
                            //    radioButtonTHREE.IsChecked = true;

                            //if (elem.Element("FCINSERT_TYPE").Value.ToString() == "A")
                            //    radioButtonInsetAll.IsChecked = true;
                            //else
                            //    radioButtonInsertcircle.IsChecked = true;

                            Promo_Channel_Link.Add(new PromoChannelLinkList()
                            {
                                FSPROMO_ID = elem.Element("FSPROMO_ID").Value.ToString(),
                                FSPROMO_NAME = elem.Element("FSPROMO_NAME").Value.ToString(),
                                //FSCHANNEL_ID = elem.Element("FSCHANNEL_ID").Value.ToString(),
                                //FSCHANNEL_NAME = elem.Element("FSCHANNEL_NAME").Value.ToString(),
                                FNNO = elem.Element("FNNO").Value.ToString(),
                                //FCLINK_TYPE = elem.Element("FCLINK_TYPE").Value.ToString(),
                                //FCTIME_TYPE = elem.Element("FCTIME_TYPE").Value.ToString(),
                                //FCINSERT_TYPE = elem.Element("FCINSERT_TYPE").Value.ToString(),
                            });
                        }
                        dataGridQueue.ItemsSource = null;
                        dataGridQueue.ItemsSource = Promo_Channel_Link;



                    }
                    else
                    {
                        dataGridQueue.ItemsSource = null;
                    }
                }
            };           
        }

        private void buttonAdd_Click(object sender, RoutedEventArgs e)
        {
            PGM115_01 PromoQuery_Frm = new PGM115_01();
            PromoQuery_Frm.OKButton.Content = "加入";
            PromoQuery_Frm.Show();
            PromoQuery_Frm.Closed += (s, args) =>
            {
                if (PromoQuery_Frm._Promo_ID != "")
                {
                    //string _TempFCTIME_TYPE = "";
                    //string _TempFCINSERT_TYPE = "";
                    //if (radioButtonAll.IsChecked == true)
                    //    _TempFCTIME_TYPE = "A";
                    //else if (radioButtonONE.IsChecked == true)
                    //    _TempFCTIME_TYPE = "1";
                    //else if (radioButtonTWO.IsChecked == true)
                    //    _TempFCTIME_TYPE = "2";
                    //else if (radioButtonTHREE.IsChecked == true)
                    //    _TempFCTIME_TYPE = "3";

                    //if (radioButtonInsetAll.IsChecked == true)
                    //    _TempFCINSERT_TYPE = "A";
                    //else
                    //    _TempFCINSERT_TYPE = "C";
                    Promo_Channel_Link.Add(new PromoChannelLinkList()
                    {
                        FSPROMO_ID = PromoQuery_Frm._Promo_ID,
                        FSPROMO_NAME = PromoQuery_Frm._Promo_Name,
                        //FSCHANNEL_ID = ((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString(),
                        //FSCHANNEL_NAME = ((ComboBoxItem)comboBoxChannel.SelectedItem).Content.ToString(),
                        FNNO = (Promo_Channel_Link.Count+1).ToString(),
                        //FCLINK_TYPE = ((ComboBoxItem)comboBoxLinkType.SelectedItem).Content.ToString(),
                        //FCTIME_TYPE = _TempFCTIME_TYPE,
                        //FCINSERT_TYPE = _TempFCINSERT_TYPE,
                    });
                    dataGridQueue.ItemsSource = null;
                    dataGridQueue.ItemsSource = Promo_Channel_Link;

                }
            };
        }

        private void buttonRemove_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridQueue.SelectedIndex < 0)
            {
                MessageBox.Show("請先選擇一筆資料,再進行移除");
                return;
            }
            Promo_Channel_Link.RemoveAt(dataGridQueue.SelectedIndex);
            dataGridQueue.ItemsSource = null;
            dataGridQueue.ItemsSource = Promo_Channel_Link;
        }

        private void comboBoxChannel_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            QueryLink();
        }

        private void comboBoxLinkType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            QueryLink();
        }

        private void radioButtonAll_Click(object sender, RoutedEventArgs e)
        {
            if (_loadForm==true)
                QueryLink();
        }
        //List<PromoChannelLinkList> Promo_Channel_Link = new List<PromoChannelLinkList>();
        private void buttonWrite_Click(object sender, RoutedEventArgs e)
        {
            if (Promo_Channel_Link.Count <= 0)
            {
                //MessageBox.Show("請先加入短帶");
                //return;
                var res = MessageBox.Show("您沒有加入短帶,請問是否要刪除這個類型的頻道連結宣傳帶?", "", MessageBoxButton.OKCancel);
                if (res.ToString() == "Cancel")
                {
                    return;
                }
            }

            WSPGMSendSQL.SendSQLSoapClient SP_I_TBPGM_CHANNEL_PROMO_LINK = new WSPGMSendSQL.SendSQLSoapClient();
            StringBuilder sb = new StringBuilder();
            StringBuilder sbDel = new StringBuilder();
            sbDel.AppendLine("<Data>");

            sbDel.AppendLine("<FSCHANNEL_ID>" + ((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString() + "</FSCHANNEL_ID>");
            sbDel.AppendLine("<FCLINK_TYPE>" + ((ComboBoxItem)comboBoxLinkType.SelectedItem).Tag.ToString() + "</FCLINK_TYPE>");
            if (radioButtonAll.IsChecked == true)
                sbDel.AppendLine("<FCTIME_TYPE>" + "A" + "</FCTIME_TYPE>");
            else if (radioButtonONE.IsChecked == true)
                sbDel.AppendLine("<FCTIME_TYPE>" + "1" + "</FCTIME_TYPE>");
            else if (radioButtonTWO.IsChecked == true)
                sbDel.AppendLine("<FCTIME_TYPE>" + "2" + "</FCTIME_TYPE>");
            else if (radioButtonTHREE.IsChecked == true)
                sbDel.AppendLine("<FCTIME_TYPE>" + "3" + "</FCTIME_TYPE>");

            if (radioButtonInsetAll.IsChecked == true)
                sbDel.AppendLine("<FCINSERT_TYPE>" + "A" + "</FCINSERT_TYPE>");
            else
                sbDel.AppendLine("<FCINSERT_TYPE>" + "C" + "</FCINSERT_TYPE>");
            sbDel.AppendLine("</Data>");


            int Save_NO = 1;
            object ReturnXML = "";
            sb.AppendLine("<Datas>");
            foreach (PromoChannelLinkList Temp_Promo_Channel_Link in Promo_Channel_Link)
            {
                sb.AppendLine("<Data>");
                sb.AppendLine("<FSPROMO_ID>" + Temp_Promo_Channel_Link.FSPROMO_ID + "</FSPROMO_ID>");
                sb.AppendLine("<FSCHANNEL_ID>" + ((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString() + "</FSCHANNEL_ID>");
                sb.AppendLine("<FNNO>" + Save_NO + "</FNNO>");
                Save_NO = Save_NO + 1;
                sb.AppendLine("<FCLINK_TYPE>" + ((ComboBoxItem)comboBoxLinkType.SelectedItem).Tag.ToString() + "</FCLINK_TYPE>");
                if (radioButtonAll.IsChecked == true)
                    sb.AppendLine("<FCTIME_TYPE>" + "A" + "</FCTIME_TYPE>");
                else if (radioButtonONE.IsChecked == true)
                    sb.AppendLine("<FCTIME_TYPE>" + "1" + "</FCTIME_TYPE>");
                else if (radioButtonTWO.IsChecked == true)
                    sb.AppendLine("<FCTIME_TYPE>" + "2" + "</FCTIME_TYPE>");
                else if (radioButtonTHREE.IsChecked == true)
                    sb.AppendLine("<FCTIME_TYPE>" + "3" + "</FCTIME_TYPE>");

                if (radioButtonInsetAll.IsChecked == true)
                    sb.AppendLine("<FCINSERT_TYPE>" + "A" + "</FCINSERT_TYPE>");
                else
                    sb.AppendLine("<FCINSERT_TYPE>" + "C" + "</FCINSERT_TYPE>");
                sb.AppendLine("<FSCREATED_BY>" + UserClass.userData.FSUSER_ID.ToString() + "</FSCREATED_BY>");
                sb.AppendLine("<FSUPDATED_BY>" + UserClass.userData.FSUSER_ID.ToString() + "</FSUPDATED_BY>");
                sb.AppendLine("</Data>");

            }
            sb.AppendLine("</Datas>");
            //sb=sb.Replace("\r\n", "");
            SP_I_TBPGM_CHANNEL_PROMO_LINK.Do_Insert_ListAsync("SP_D_TBPGM_CHANNEL_PROMO_LINK", sbDel.ToString(), "SP_I_TBPGM_CHANNEL_PROMO_LINK", sb.ToString());
            SP_I_TBPGM_CHANNEL_PROMO_LINK.Do_Insert_ListCompleted += (s, args) =>
            {
                if (args.Error == null)
                {

                    if (args.Result == true)
                        MessageBox.Show("新增成功");
                    else
                        MessageBox.Show("新增失敗");
                }
            };
        }

    }
}
