﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace PTS_MAM3
{
    public partial class PGM100_06 : ChildWindow
    {
        string _OnLoad = "";
        public PGM100_06()
        {
            InitializeComponent();

            InitComboBox("SP_Q_TBZCHANNEL_ALL", cbMainChannelID, "ID", "NAME");
            InitComboBox("SP_Q_TBZPROGOBJ_ALL", cbOBJ_ID, "FSPROGOBJID", "FSPROGOBJNAME");
            InitComboBox("SP_Q_TBZDEPT_ALL", cbPRD_DEPT_ID, "ID", "NAME");
            InitComboBox("SP_Q_TBZPROMOTYPE", cbPRO_TYPE, "ID", "NAME");
            InitComboBox("SP_Q_TBZPROMOVER", cbVER_ID, "ID", "NAME");
            InitComboBox("SP_Q_TBZPROMOCAT", cbCAT_ID, "ID", "NAME");
           
        }

        public void InitComboBox(string sp_name,ComboBox TempCombobox,string ReturnID,string ReturnName)
        {
            WSPGMSendSQL.SendSQLSoapClient SP_Q_Combo = new WSPGMSendSQL.SendSQLSoapClient();

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            sb.AppendLine("</Data>");
            SP_Q_Combo.Do_QueryAsync(sp_name, sb.ToString(), ReturnXML);
            SP_Q_Combo.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {
                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            ComboBoxItem TempComboBoxItem = new ComboBoxItem();
                            TempComboBoxItem.Content = elem.Element(ReturnName).Value.ToString();
                            TempComboBoxItem.Tag = elem.Element(ReturnID).Value.ToString();

                            TempCombobox.Items.Add(TempComboBoxItem);
                        }
                        if (sp_name=="SP_Q_TBZPROMOCAT")
                        {
                            _OnLoad = "END";
                        }
                    }
                }
            };      
        }
        public bool BeChooseData = false;
        public PromoList ReturnPromoData;
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (DataGridPromoList.SelectedIndex < 0)
            {
                MessageBox.Show("請先選擇一筆宣傳帶資料");
                return;
            }
            BeChooseData = true;
            ReturnPromoData = ((PromoList)DataGridPromoList.SelectedItems[0]);
            //ReturnPromoData = QueryPromoList[DataGridPromoList.SelectedIndex];
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            
            this.DialogResult = false;
        }

        private void ChildWindow_Closed(object sender, EventArgs e)
        {

        }
        List<PromoList> QueryPromoList = new List<PromoList>();
        private void btnQueryPromo_Click(object sender, RoutedEventArgs e)
        {
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_PROMO = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            //sb.AppendLine("<FDDATE>" + this.textBoxDate.Text + "</FDDATE>");
            sb.AppendLine("<FSPROMO_ID>" + this.tbxPRO_ID.Text + "</FSPROMO_ID>");
            sb.AppendLine("<FSPROMO_NAME>" + TransferTimecode.ReplaceXML(this.tbxPromoName.Text) + "</FSPROMO_NAME>");
            sb.AppendLine("<FSPROMO_NAME_ENG>" + TransferTimecode.ReplaceXML(this.tbxENG_NAME.Text) + "</FSPROMO_NAME_ENG>");
            sb.AppendLine("<FSDURATION>" + this.tbxPromo_Length.Text + "</FSDURATION>");
            sb.AppendLine("<FSPROG_ID>" + this.tbxPROG_ID.Text + "</FSPROG_ID>");
            sb.AppendLine("<FNEPISODE>" + this.tbxPROG_EPISODE.Text + "</FNEPISODE>");
            if (cbPRO_TYPE.SelectedIndex != -1)
                sb.AppendLine("<FSPROMOTYPEID>" + ((ComboBoxItem)cbPRO_TYPE.SelectedItem).Tag.ToString() + "</FSPROMOTYPEID>");
            else
                sb.AppendLine("<FSPROMOTYPEID>" + "" + "</FSPROMOTYPEID>");
            if (cbOBJ_ID.SelectedIndex != -1)
                sb.AppendLine("<FSPROGOBJID>" + ((ComboBoxItem)cbOBJ_ID.SelectedItem).Tag.ToString() + "</FSPROGOBJID>");
            else
                sb.AppendLine("<FSPROGOBJID>" + "" + "</FSPROGOBJID>");
            if (cbPRD_DEPT_ID.SelectedIndex != -1)
                sb.AppendLine("<FSPRDCENID>" + ((ComboBoxItem)cbPRD_DEPT_ID.SelectedItem).Tag.ToString() + "</FSPRDCENID>");
            else
                sb.AppendLine("<FSPRDCENID>" + "" + "</FSPRDCENID>");
            sb.AppendLine("<FSPRDYYMM>" + this.tbxPRD_YYMM.Text + "</FSPRDYYMM>");
            sb.AppendLine("<FSACTIONID>" + this.tbxACT_ID.Text + "</FSACTIONID>");
            if (cbCAT_ID.SelectedIndex != -1)
                sb.AppendLine("<FSPROMOCATID>" + ((ComboBoxItem)cbCAT_ID.SelectedItem).Tag.ToString() + "</FSPROMOCATID>");
            else
                sb.AppendLine("<FSPROMOCATID>" + "" + "</FSPROMOCATID>");
            if (cbPromoCATD.SelectedIndex != -1)
                sb.AppendLine("<FSPROMOCATDID>" + ((ComboBoxItem)cbPromoCATD.SelectedItem).Tag.ToString() + "</FSPROMOCATDID>");
            else
                sb.AppendLine("<FSPROMOCATDID>" + "" + "</FSPROMOCATDID>");
            if (cbVER_ID.SelectedIndex != -1)
                sb.AppendLine("<FSPROMOVERID>" + ((ComboBoxItem)cbVER_ID.SelectedItem).Tag.ToString() + "</FSPROMOVERID>");
            else
                sb.AppendLine("<FSPROMOVERID>" + "" + "</FSPROMOVERID>");
            if (cbMainChannelID.SelectedIndex != -1)
                sb.AppendLine("<FSMAIN_CHANNEL_ID>" + ((ComboBoxItem)cbMainChannelID.SelectedItem).Tag.ToString() + "</FSMAIN_CHANNEL_ID>");
            else
                sb.AppendLine("<FSMAIN_CHANNEL_ID>" + "" + "</FSMAIN_CHANNEL_ID>");
            sb.AppendLine("<FSMEMO>" + "" + "</FSMEMO>");


            sb.AppendLine("</Data>");
            SP_Q_TBPGM_PROMO.Do_QueryAsync("SP_Q_TBPGM_PROMO", sb.ToString(), ReturnXML);

            SP_Q_TBPGM_PROMO.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {


                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        QueryPromoList.Clear();
                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            QueryPromoList.Add(new PromoList()
                            {
                                FSPROMO_ID = elem.Element("FSPROMO_ID").Value.ToString(),
                                FSPROMO_NAME = elem.Element("FSPROMO_NAME").Value.ToString(),
                                FSPROMO_NAME_ENG = elem.Element("FSPROMO_NAME_ENG").Value.ToString(),

                                //ProgDate = (Convert.ToDateTime(elem.Element("FDDATE").Value)).ToString("yyyy/MM/dd"),
                                //ProgDate = elem.Element("FDDATE").Value.ToString(),
                                FSDURATION = elem.Element("FSDURATION").Value.ToString(),
                                FSPROG_ID = elem.Element("FSPROG_ID").Value.ToString(),
                                FNEPISODE = elem.Element("FNEPISODE").Value.ToString(),
                                FSPROG_NAME = elem.Element("FSPROG_NAME").Value.ToString(),
                                FSPROMOTYPEID = elem.Element("FSPROMOTYPEID").Value.ToString(),
                                FSPROMOTYPENAME = elem.Element("FSPROMOTYPENAME").Value.ToString(),
                                FSPROGOBJID = elem.Element("FSPROGOBJID").Value.ToString(),
                                FSPROGOBJNAME = elem.Element("FSPROGOBJNAME").Value.ToString(),
                                FSPRDCENID = elem.Element("FSPRDCENID").Value.ToString(),
                                FSDEPTNAME = elem.Element("FSDEPTNAME").Value.ToString(),
                                FSACTIONID = elem.Element("FSACTIONID").Value.ToString(),
                                FSACTIONNAME = elem.Element("FSACTIONNAME").Value.ToString(),
                                FSPRDYYMM = elem.Element("FSPRDYYMM").Value.ToString(),
                                FSPROMOCATID = elem.Element("FSPROMOCATID").Value.ToString(),
                                FSPROMOCATNAME = elem.Element("FSPROMOCATNAME").Value.ToString(),
                                FSPROMOCATDID = elem.Element("FSPROMOCATDID").Value.ToString(),
                                FSPROMOCATDNAME = elem.Element("FSPROMOCATDNAME").Value.ToString(),
                                FSPROMOVERID = elem.Element("FSPROMOVERID").Value.ToString(),
                                FSPROMOVERNAME = elem.Element("FSPROMOVERNAME").Value.ToString(),
                                FSMAIN_CHANNEL_ID = elem.Element("FSMAIN_CHANNEL_ID").Value.ToString(),
                                FSCHANNEL_NAME = elem.Element("FSCHANNEL_NAME").Value.ToString(),
                                FSMEMO = elem.Element("FSMEMO").Value.ToString(),
                            });
                        }
                        DataGridPromoList.ItemsSource = QueryPromoList;
                    }
                }
            };            
        }

        private void cbCAT_ID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_OnLoad == "")
                return;
            cbPromoCATD.Items.Clear();
            if (cbCAT_ID.SelectedIndex == -1)
            {
                return;
            }


            WSPGMSendSQL.SendSQLSoapClient SP_Q_Combo = new WSPGMSendSQL.SendSQLSoapClient();

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            sb.AppendLine("<FSPROMOCATID>" + ((ComboBoxItem)cbCAT_ID.SelectedItem).Tag.ToString() + "</FSPROMOCATID>");
            sb.AppendLine("</Data>");
            SP_Q_Combo.Do_QueryAsync("SP_Q_TBZPROMOCATD", sb.ToString(), ReturnXML);
            SP_Q_Combo.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            ComboBoxItem TempComboBoxItem = new ComboBoxItem();
                            TempComboBoxItem.Content = elem.Element("NAME").Value.ToString();
                            TempComboBoxItem.Tag = elem.Element("ID").Value.ToString();

                            cbPromoCATD.Items.Add(TempComboBoxItem);
                        }
                    }
                }
            };      
        }
    }
}

