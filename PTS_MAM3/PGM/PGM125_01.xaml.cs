﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace PTS_MAM3.PGM
{
    public partial class PGM125_01 : ChildWindow
    {
        public PGM125_01()
        {
            InitializeComponent();
        }

        public string _channelid="";
        public string _date="";
        public string _Promo_ID;
        public string _Promo_Name;

        public string XElementToString(XElement INXElement)
        {
            string ReturnStr;
            try 
            {
                ReturnStr=INXElement.Value.ToString();
                return ReturnStr;
                
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (DCdataGridPromoList.SelectedIndex < 0)
            {
                MessageBox.Show("請先選取資料");
                return;
            }
            for (int i = DCdataGridPromoList.SelectedItems.Count - 1; i >= 0; i--)
            {
                ReturnPromo = ((InsPromoQueryPromo)DCdataGridPromoList.SelectedItems[i]);
            }

            //ReturnPromo = GetPromoList[DCdataGridPromoList.SelectedIndex];
            //_Promo_ID = ReturnPromo.FSPROMO_ID;
            //this.DialogResult = true;
            _Promo_ID = ReturnPromo.FSPROMO_ID;


            if (ReturnPromo.FSVIDEO_ID == "") //要去取VideoID
            {
                TransProgList.WebService1SoapClient MapProg_Episode_VideoID = new TransProgList.WebService1SoapClient();
                //if (AAA.TransProglistAsync("N", "2010/12/16") = "ok")

                MapProg_Episode_VideoID.MapVideoIDAsync(_channelid, ReturnPromo.FSPROMO_ID, "0");
                MapProg_Episode_VideoID.MapVideoIDCompleted += (s1, args1) =>
                {
                    if (args1.Error == null)
                    {
                        if (args1.Result == "取得Video ID有誤")
                        {
                            MessageBox.Show("對應宣傳帶與VideoID發生錯誤,不可以插入此宣傳帶!");
                            return;
                        }
                        else //要將VideoID 寫入
                        {
                            ReturnPromo.FSVIDEO_ID = args1.Result;
                        }
                    }
                    ReturnPromo = GetPromoList[DCdataGridPromoList.SelectedIndex];

                    this.DialogResult = true;
                };

            }
            else
            {
                this.DialogResult = true;
            }
           
            
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        public InsPromoQueryPromo ReturnPromo = new InsPromoQueryPromo();
        List<InsPromoQueryPromo> GetPromoList = new List<InsPromoQueryPromo>();
        private void buttonQuery_Click(object sender, RoutedEventArgs e)
        {
            if (textBoxNewPromoName.Text == "")
            {
                //MessageBox.Show("請輸入要查的短帶名稱");
                //return;
                textBoxNewPromoName.Text = "%";
            }
            if (_date == "")
            {
                MessageBox.Show("必須要有日期");
                return;
            }
            if (_channelid == "")
            {
                MessageBox.Show("必須要有頻道");
                return;
            }
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_PROMO = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            sb.AppendLine("<FSPROMO_NAME>" + TransferTimecode.ReplaceXML(textBoxNewPromoName.Text) + "</FSPROMO_NAME>");

            sb.AppendLine("<FSPROMO_ID>" + "" + "</FSPROMO_ID>");
            sb.AppendLine("<FSPROMO_NAME_ENG>" + "" + "</FSPROMO_NAME_ENG>");
            sb.AppendLine("<FSDURATION>" + "" + "</FSDURATION>");
            sb.AppendLine("<FSPROMOTYPEID>" + "" + "</FSPROMOTYPEID>");
            sb.AppendLine("<FSCHANNEL_ID>" + _channelid + "</FSCHANNEL_ID>");
            sb.AppendLine("<FDDATE>" + _date + "</FDDATE>");
            if ((Convert.ToDateTime(_date)).DayOfWeek.ToString()=="Monday")
                sb.AppendLine("<FSWEEK>" + "1______" + "</FSWEEK>");
            else if ((Convert.ToDateTime(_date)).DayOfWeek.ToString() == "Tuesday")
                sb.AppendLine("<FSWEEK>" + "_1_____" + "</FSWEEK>");
            else if ((Convert.ToDateTime(_date)).DayOfWeek.ToString() == "Wednesday")
                sb.AppendLine("<FSWEEK>" + "__1____" + "</FSWEEK>");
            else if ((Convert.ToDateTime(_date)).DayOfWeek.ToString() == "Thursday")
                sb.AppendLine("<FSWEEK>" + "___1___" + "</FSWEEK>");
            else if ((Convert.ToDateTime(_date)).DayOfWeek.ToString() == "Friday")
                sb.AppendLine("<FSWEEK>" + "____1__" + "</FSWEEK>");
            else if ((Convert.ToDateTime(_date)).DayOfWeek.ToString() == "Saturday")
                sb.AppendLine("<FSWEEK>" + "_____1_" + "</FSWEEK>");
            else if ((Convert.ToDateTime(_date)).DayOfWeek.ToString() == "Sunday")
                sb.AppendLine("<FSWEEK>" + "______1" + "</FSWEEK>");
            //sb.AppendLine("<FSWEEK>" + "" + "</FSWEEK>");





            //FSWEEK like '__1____'

//            @FSPROMO_ID	varchar(11),
//@FSPROMO_NAME	nvarchar(50),
//@FSPROMO_NAME_ENG	varchar(50),
//@FSDURATION	char(11),
//@FSPROMOTYPEID	char(2),
//@FSCHANNEL_ID	char(2),
//@FDDATE	datetime,
//@FSWEEK	char(7)
            sb.AppendLine("</Data>");

            SP_Q_TBPGM_PROMO.Do_QueryAsync("SP_Q_TBPGM_PROMO_BY_EFF_ZONE", sb.ToString(), ReturnXML);
            SP_Q_TBPGM_PROMO.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {

                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        GetPromoList.Clear();
                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            

                            //if (XElementToString(elem.Element("FSPROMO_NAME")).Substring(0, 2) != "RM" || (XElementToString(elem.Element("FSPROMO_NAME")).Substring(0, 2) == "RM" && checkBoxRM.IsChecked == true)) //不要列出REMain
                            if (XElementToString(elem.Element("FSPROMO_NAME")).IndexOf("RM") == -1 || (XElementToString(elem.Element("FSPROMO_NAME")).IndexOf("RM") != -1 && checkBoxRM.IsChecked == true)) //不要列出REMain
                            {
                                string File_status = "";
                                if (XElementToString(elem.Element("FCLOW_RES")) == "")
                                    File_status = "尚未送帶轉檔";
                                else if (XElementToString(elem.Element("FCLOW_RES")) == "Y")
                                    File_status = "轉檔完成";
                                else if (XElementToString(elem.Element("FCLOW_RES")) == "N")
                                    File_status = "尚未轉檔";
                                else if (XElementToString(elem.Element("FCLOW_RES")) == "R")
                                    File_status = "轉檔中";
                                else if (XElementToString(elem.Element("FCLOW_RES")) == "F")
                                    File_status = "轉檔失敗";
                                else if (XElementToString(elem.Element("FCFILE_STATUS")) == "")
                                    File_status = "尚未入庫";
                                else if (XElementToString(elem.Element("FCFILE_STATUS")) == "Y")
                                    File_status = "轉檔完成";
                                else if (XElementToString(elem.Element("FCFILE_STATUS")) == "Y")
                                    File_status = "已經入庫";
                                else
                                    File_status = "未知狀態";
                                GetPromoList.Add(new InsPromoQueryPromo()
                                {

                                    FSPROMO_ID = XElementToString(elem.Element("FSPROMO_ID")),
                                    FSPROMO_NAME = XElementToString(elem.Element("FSPROMO_NAME")),
                                    FSPROMO_NAME_ENG = XElementToString(elem.Element("FSPROMO_NAME_ENG")),
                                    FSDURATION = XElementToString(elem.Element("FSDURATION")),
                                    FSPROG_ID = XElementToString(elem.Element("FSPROG_ID")),
                                    FNEPISODE = XElementToString(elem.Element("FNEPISODE")),
                                    FSPROMOTYPEID = XElementToString(elem.Element("FSPROMOTYPEID")),
                                    FSPROGOBJID = XElementToString(elem.Element("FSPROGOBJID")),
                                    FSPRDCENID = XElementToString(elem.Element("FSPRDCENID")),
                                    FSPRDYYMM = XElementToString(elem.Element("FSPRDYYMM")),
                                    FSPROMOCATID = XElementToString(elem.Element("FSPROMOCATID")),
                                    FSPROMOCATDID = XElementToString(elem.Element("FSPROMOCATDID")),
                                    FSPROMOVERID = XElementToString(elem.Element("FSPROMOVERID")),
                                    FSMAIN_CHANNEL_ID = XElementToString(elem.Element("FSMAIN_CHANNEL_ID")),
                                    FSBEG_TIME = XElementToString(elem.Element("FSBEG_TIME")),
                                    FSEND_TIME = XElementToString(elem.Element("FSEND_TIME")),
                                    FCDATE_TIME_TYPE = XElementToString(elem.Element("FCDATE_TIME_TYPE")),
                                    FSFILE_NO = XElementToString(elem.Element("FSFILE_NO")),
                                    FSVIDEO_ID = XElementToString(elem.Element("FSVIDEO_ID")),
                                    FDBEG_DATE = XElementToString(elem.Element("FDBEG_DATE")),
                                    FDEND_DATE = XElementToString(elem.Element("FDEND_DATE")),
                                    FSMEMO = "",
                                    FSMEMO1 = XElementToString(elem.Element("FSMEMO")),
                                    FSSIGNAL = "COMM",
                                    FCFILE_STATUS = File_status,
                                    FSTIME1 = XElementToString(elem.Element("FSTIME1")),
                                    FSTIME2 = XElementToString(elem.Element("FSTIME2")),
                                    FSTIME3 = XElementToString(elem.Element("FSTIME3")),
                                    FSTIME4 = XElementToString(elem.Element("FSTIME4")),
                                    FSTIME5 = XElementToString(elem.Element("FSTIME5")),
                                    FSTIME6 = XElementToString(elem.Element("FSTIME6")),
                                    FSTIME7 = XElementToString(elem.Element("FSTIME7")),
                                    FSTIME8 = XElementToString(elem.Element("FSTIME8")),
                                    FSCMNO = XElementToString(elem.Element("FSCMNO")),
                                });
                            }
                        }
                        DCdataGridPromoList.ItemsSource = null;
                        DCdataGridPromoList.ItemsSource = GetPromoList;
                        MessageBox.Show("查詢完畢");
                    }
                    else
                    {
                        MessageBox.Show("查無資料");
                    }
                }

            };           
        }

        private void DCdataGridPromoList_RowDoubleClicked(object sender, DataGridDCTest.DataGridRowClickedArgs e)
        {
            if (DCdataGridPromoList.SelectedIndex < 0)
            {
                MessageBox.Show("請先選取資料");
                return;
            }
            for (int i = DCdataGridPromoList.SelectedItems.Count - 1; i >= 0; i--)
            {
                ReturnPromo = ((InsPromoQueryPromo)DCdataGridPromoList.SelectedItems[i]);
            }

            //ReturnPromo = GetPromoList[DCdataGridPromoList.SelectedIndex];
            //_Promo_ID = ReturnPromo.FSPROMO_ID;
            //this.DialogResult = true;
            _Promo_ID = ReturnPromo.FSPROMO_ID;


            if (ReturnPromo.FSVIDEO_ID == "") //要去取VideoID
            {
                TransProgList.WebService1SoapClient MapProg_Episode_VideoID = new TransProgList.WebService1SoapClient();
                //if (AAA.TransProglistAsync("N", "2010/12/16") = "ok")

                MapProg_Episode_VideoID.MapVideoIDAsync(_channelid, ReturnPromo.FSPROMO_ID, "0");
                MapProg_Episode_VideoID.MapVideoIDCompleted += (s1, args1) =>
                {
                    if (args1.Error == null)
                    {
                        if (args1.Result == "取得Video ID有誤")
                        {
                            MessageBox.Show("對應宣傳帶與VideoID發生錯誤,不可以插入此宣傳帶!");
                            return;
                        }
                        else //要將VideoID 寫入
                        {
                            ReturnPromo.FSVIDEO_ID = args1.Result;
                        }
                    }
                    ReturnPromo = GetPromoList[DCdataGridPromoList.SelectedIndex];

                    this.DialogResult = true;
                };

            }
            else
            {
                this.DialogResult = true;
            }
        }
    }
}

