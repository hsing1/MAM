﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Xml.Linq;
using System.Text;
using System.IO;
using System.IO.IsolatedStorage;

namespace PTS_MAM3.PGM
{
    public partial class PGM135 : Page
    {
        public PGM135()
        {
            InitializeComponent();
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }


        public string XElementToString(XElement INXElement)
        {
            string ReturnStr;
            try
            {
                ReturnStr = INXElement.Value.ToString();
                return ReturnStr;

            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public string GetTimecode(int InHH, int InMM, int InSS, int InFF)
        {
            string RTime;

            int TempTen;
            int TempOne;
            TempTen = (int)(Math.Floor((double)InHH / (double)16));
            TempOne = InHH - 16 * TempTen;
            RTime = String.Format("{0:00}", TempTen * 10 + TempOne);

            TempTen = (int)(Math.Floor((double)InMM / (double)16));
            TempOne = InMM - 16 * TempTen;
            RTime = RTime + ":" + String.Format("{0:00}", TempTen * 10 + TempOne);

            TempTen = (int)(Math.Floor((double)InSS / (double)16));
            TempOne = InSS - 16 * TempTen;
            RTime = RTime + ":" + String.Format("{0:00}", TempTen * 10 + TempOne);

            TempTen = (int)(Math.Floor((double)InFF / (double)16));
            TempOne = InFF - 16 * TempTen;
            RTime = RTime + ":" + String.Format("{0:00}", TempTen * 10 + TempOne);

            return RTime;
        }

        bool _NeedSave = false;

        List<LSTFileColData> LSTFile = new List<LSTFileColData>();

        private void buttonOpenFile_Click(object sender, RoutedEventArgs e)
        {
            LSTFile.Clear();
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "All files (*.*)|*.*";

            byte[] bTitle1 = new byte[16];

            //List<byte[]> InList=new List<byte[]>();
            string SendString = "";
            if (dlg.ShowDialog() == true)
            {
                WSPGMSendSQL.SendSQLSoapClient TransLstProgName = new WSPGMSendSQL.SendSQLSoapClient();
                System.Collections.ObjectModel.ObservableCollection<WSPGMSendSQL.LSTProgName> InList1 = new System.Collections.ObjectModel.ObservableCollection<WSPGMSendSQL.LSTProgName>();

                SendString = "<Datas>";
                IsolatedStorageFile iso = IsolatedStorageFile.GetUserStoreForApplication();
                using (Stream fileStream = dlg.File.OpenRead())
                {
                    using (IsolatedStorageFileStream isoStream =
                        new IsolatedStorageFileStream(dlg.File.Name, FileMode.Create, iso))
                    {
                        int SNO = 0;
                        // Read and write the data block by block until finish
                        while (true)
                        {
                            byte[] buffer = new byte[104];
                            int count = fileStream.Read(buffer, 0, buffer.Length);
                            if (count > 0)
                            {
                                isoStream.Write(buffer, 0, count);
                                string TempStr;
                                TempStr = buffer[0].ToString();
                                char C = Convert.ToChar(buffer[16]); //轉換Ascii為字元 EX:48-->0
                                TempStr = TempStr + C;

                                byte[] b = new byte[16];

                                for (int i = 24; i < 40; i++)
                                {
                                    b[i - 24] = buffer[i];
                                }
                                InList1.Add(new WSPGMSendSQL.LSTProgName()
                                {
                                    Name = b,
                                });

                                string progname = UTF8Encoding.UTF8.GetString(b, 0, b.Length);
                                //Encoding.GetEncoding("big5");
                                //progname = System.Text.Encoding.GetEncoding("950").GetString(b, 0, b.Length);
                                progname = System.Text.Encoding.GetEncoding("utf-16").GetString(b, 0, b.Length);
                                //string strModified = System.Text.Encoding.Unicode.GetString(b);
                                //b.ToString();
                                TempStr = TempStr + progname;
                                //MessageBox.Show(TempStr);
                                string SType, SReconcile, SEffect1, SEffect2, SEffect3, SOnAirTimecode, SID, STitle, SSOMTimecode;
                                string SDurTimecode, SChannel, SQualifier4, SSegNum, SDeviceMaj, SDeviceMin, SBinHigh, SBinLow;
                                string SQualifier1, SQualifier2, SQualifier3, SDateToAir, SEventControl, SEventStatus;
                                string SCompileID, SCompileSOM, SBoxAID, SBoxASOM, SBoxBID, SBoxBSOM;
                                string SReserved, SBackupDeviceMaj, SBackupDeviceMin, SExtendedEventControl;

                                if (buffer[0].ToString() == "0")
                                    SType = "S";
                                else
                                    SType = "K";
                                SReconcile = "";

                                string SFadeIN;
                                string SFast;
                                if (buffer[9].ToString() == "0" && buffer[10].ToString() == "0" && buffer[11].ToString() == "0")
                                {
                                    SFadeIN = "V";
                                    SFast = "F.";
                                    SEffect1 = "0";
                                    SEffect2 = "0";
                                    SEffect3 = "0";
                                }
                                else
                                {
                                    SFadeIN = "v";
                                    SFast = "S.";
                                    SEffect1 = buffer[9].ToString();
                                    SEffect2 = buffer[10].ToString();
                                    SEffect3 = buffer[11].ToString();
                                }

                                SOnAirTimecode = GetTimecode(int.Parse(buffer[15].ToString()), int.Parse(buffer[14].ToString()), int.Parse(buffer[13].ToString()), int.Parse(buffer[12].ToString()));

                                byte[] bID = new byte[8];

                                for (int i = 16; i < 24; i++)
                                {
                                    bID[i - 16] = buffer[i];
                                }
                                SID = UTF8Encoding.UTF8.GetString(bID, 0, bID.Length);

                                byte[] bTitle = new byte[16];

                                for (int i = 24; i < 40; i++)
                                {
                                    bTitle[i - 24] = buffer[i];
                                }
                                STitle = UTF8Encoding.UTF8.GetString(bTitle, 0, bTitle.Length);
                                bTitle1 = bTitle;
                                //InList1.Add(bTitle);


                                SendString = SendString + "<Data>" + STitle + "</Data>";
                                SSOMTimecode = GetTimecode(int.Parse(buffer[43].ToString()), int.Parse(buffer[42].ToString()), int.Parse(buffer[41].ToString()), int.Parse(buffer[40].ToString()));
                                SDurTimecode = GetTimecode(int.Parse(buffer[47].ToString()), int.Parse(buffer[46].ToString()), int.Parse(buffer[45].ToString()), int.Parse(buffer[44].ToString()));

                                SChannel = buffer[48].ToString();
                                SQualifier4 = buffer[49].ToString();
                                SSegNum = buffer[50].ToString();
                                SDeviceMaj = buffer[51].ToString();
                                SDeviceMin = buffer[52].ToString();
                                SBinHigh = buffer[53].ToString();
                                SBinLow = buffer[54].ToString();
                                SQualifier1 = buffer[55].ToString();
                                SQualifier2 = buffer[56].ToString();
                                SQualifier3 = buffer[57].ToString();
                                SDateToAir = "255";//buffer[58].ToString();

                                SEventControl = buffer[60].ToString();
                                if (buffer[60].ToString() == "00" && buffer[61].ToString() == "00")
                                    SEventControl = "";
                                else if (buffer[60].ToString() == "17" && buffer[61].ToString() == "00")
                                    SEventControl = "AO";
                                else if (buffer[60].ToString() == "07" && buffer[61].ToString() == "00")
                                    SEventControl = "A";
                                else if (buffer[60].ToString() == "47" && buffer[61].ToString() == "02")
                                    SEventControl = "AUN";
                                else if (buffer[60].ToString() == "07" && buffer[61].ToString() == "02")
                                    SEventControl = "AN";
                                else if (buffer[60].ToString() == "57" && buffer[61].ToString() == "02")
                                    SEventControl = "AUNO";
                                else if (buffer[60].ToString() == "17" && buffer[61].ToString() == "02")
                                    SEventControl = "ANO";
                                else
                                    SEventControl = "";

                                SEventStatus = "";//buffer[54].ToString();
                                SCompileID = "";//buffer[54].ToString();
                                SCompileSOM = "";//buffer[54].ToString();
                                SBoxAID = "";//buffer[54].ToString();
                                SBoxASOM = "";//buffer[54].ToString();
                                SBoxBID = "";//buffer[54].ToString();
                                SBoxBSOM = "";//buffer[54].ToString();
                                SReserved = "";//buffer[54].ToString();
                                SBackupDeviceMaj = "";//buffer[54].ToString();
                                SBackupDeviceMin = "";//buffer[54].ToString();
                                SExtendedEventControl = "";//buffer[54].ToString();
                                SNO = SNO + 1;
                                LSTFile.Add(new LSTFileColData()
                                {
                                    NO = SNO.ToString(),
                                    Type = SType,
                                    Reconcile = SReconcile,
                                    Effect1 = SEffect1,
                                    Effect2 = SEffect2,
                                    Effect3 = SEffect3,
                                    OnAirTimecode = SOnAirTimecode,
                                    ID = SID,
                                    Title = STitle,
                                    SOMTimecode = SSOMTimecode,
                                    DurTimecode = SDurTimecode,
                                    Channel = SChannel,
                                    Qualifier4 = SQualifier4,
                                    SegNum = SSegNum,
                                    DeviceMaj = SDeviceMaj,
                                    DeviceMin = SDeviceMin,
                                    BinHigh = SBinHigh,
                                    BinLow = SBinLow,
                                    Qualifier1 = SQualifier1,
                                    Qualifier2 = SQualifier2,
                                    Qualifier3 = SQualifier3,
                                    DateToAir = SDateToAir,
                                    EventControl = SEventControl,
                                    EventStatus = SEventStatus,
                                    CompileID = SCompileID,
                                    CompileSOM = SCompileSOM,
                                    BoxAID = SBoxAID,
                                    BoxASOM = SBoxASOM,
                                    BoxBID = SBoxBID,
                                    BoxBSOM = SBoxBSOM,
                                    Reserved = SReserved,
                                    BackupDeviceMaj = SBackupDeviceMaj,
                                    BackupDeviceMin = SBackupDeviceMin,
                                    ExtendedEventControl = SExtendedEventControl,
                                    FadeIN = SFadeIN,
                                    Fast = SFast,


                                });
                            }
                            else
                            {
                                break;
                            }
                        }
                    }

                }

                SendString = SendString + "</Datas>";
                dataGridQueue.ItemsSource = LSTFile;

                TransLstProgName.TransLstProgNameAsync(InList1);
                TransLstProgName.TransLstProgNameCompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (args.Result != null && args.Result != "")
                        {




                            byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);

                            XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                            var ltox = from str in doc.Elements("Datas").Elements("Data")
                                       select str;
                            int _TempComNo = 0;
                            foreach (XElement elem in ltox)
                            {

                                string ProgName = "";
                                if (XElementToString(elem.Element("ProgName")) == "")
                                    ProgName = "";
                                else
                                    ProgName = elem.Element("ProgName").Value;

                                LSTFile[_TempComNo].Title = ProgName;

                                _TempComNo = _TempComNo + 1;
                            }

                            dataGridQueue.ItemsSource = null;
                            dataGridQueue.ItemsSource = LSTFile;

                        }
                    }

                };
            }
        }

    }
}
