﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace PTS_MAM3
{
    public partial class PGM125_05 : ChildWindow
    {
        public PGM125_05()
        {
            InitializeComponent();
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBZSIGNAL_ALL = new WSPGMSendSQL.SendSQLSoapClient();
            StringBuilder sb1 = new StringBuilder();
            object ReturnXML1 = "";
            sb1.AppendLine("<Data>");
            sb1.AppendLine("</Data>");
            object ReturnXML = "";
            SP_Q_TBZSIGNAL_ALL.Do_QueryAsync("[SP_Q_TBZSIGNAL_ALL]", sb1.ToString(), ReturnXML);
            SP_Q_TBZSIGNAL_ALL.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {
                        


                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);

                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {

                            ComboBoxItem TempComboBoxItem = new ComboBoxItem();
                            TempComboBoxItem.Content = elem.Element("FSSIGNALNAME").Value.ToString();

                            comboBoxSignal1.Items.Add(TempComboBoxItem);
                           
                         
                        }
                        //SignalList = TempSignalList;
                    }
                    else
                    {
                    }
                }

                
            };

        }

        public string XElementToString(XElement INXElement)
        {
            string ReturnStr;
            try
            {
                ReturnStr = INXElement.Value.ToString();
                return ReturnStr;

            }
            catch (Exception ex)
            {
                return "";
            }
        }


        private void GetDefSetting()
        {
           

            if (textBoxProgID.Text == "")
            {
                return;
            }
            if (textBoxEpisode.Text == "")
            {
                return;
            }
            
            WSPGMSendSQL.SendSQLSoapClient SP_Q_WaitReplaceList = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";
            StringBuilder sb1 = new StringBuilder();
            object ReturnXML1 = "";
            sb1.AppendLine("<Data>");
            sb1.AppendLine("<FSPROG_ID>" + textBoxProgID.Text + "</FSPROG_ID>");
            sb1.AppendLine("<FNEPISODE>" + textBoxEpisode.Text + "</FNEPISODE>");
            sb1.AppendLine("<FDDATE>" + _date + "</FDDATE>");

            sb1.AppendLine("</Data>");
            SP_Q_WaitReplaceList.Do_QueryAsync("[SP_Q_GET_QUEUE_DEF_SEC_SIGNAL]", sb1.ToString());
            SP_Q_WaitReplaceList.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {
                        byte[] byteArray2 = Encoding.Unicode.GetBytes(args.Result);
                        StringBuilder output2 = new StringBuilder();
                        // Create an XmlReader

                        XDocument doc = XDocument.Load(new MemoryStream(byteArray2));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            string RFDDATE = XElementToString(elem.Element("FDDATE"));
                            string RFNDEF_MIN = XElementToString(elem.Element("FNDEF_MIN"));
                            string RFNDEF_SEC = XElementToString(elem.Element("FNDEF_SEC"));
                            string RFSSIGNAL = XElementToString(elem.Element("FSSIGNAL"));
                            string FSBEG_TIME = XElementToString(elem.Element("FSBEG_TIME"));
                            string FSEND_TIME = XElementToString(elem.Element("FSEND_TIME"));
                            textBoxDefMin.Text = RFNDEF_MIN;
                            textBoxDefSec.Text=RFNDEF_SEC;
                            //textBoxBTime.Text = FSBEG_TIME;
                            //textBoxETime.Text = FSEND_TIME;

                            comboBoxSignal1.SelectedIndex = -1;
                            for (int Combocount = 0; Combocount < comboBoxSignal1.Items.Count; Combocount++)
                            {
                                if (((ComboBoxItem)comboBoxSignal1.Items[Combocount]).Content.ToString() == RFSSIGNAL)
                                {
                                    comboBoxSignal1.SelectedIndex = Combocount;
                                }
                            }

                            return;
                           

                        }
                    }      
                }

            };  
        }


        public PgmQueueDate _PgmQueueProg=new PgmQueueDate();
        public string _date;
        public string _channelid;
        public string _ChannelFileType;
        public string _channelName;
        public string _FromForm;
        public bool _HasReplace = false;
        public string SourceBTime;
        public string SourceETime;

        public List<ReplaceProg> ReplaceProgList = new List<ReplaceProg>();
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (textBoxProgID.Text =="")
            {
                MessageBox.Show("請查詢節目");
                return;
            }
            if (textBoxEpisode.Text == "")
            {
                MessageBox.Show("請查詢節目集數資料");
                return;
            }
            if (textBoxProgName.Text == "")
            {
                MessageBox.Show("請查詢節目名稱資料");
                return;
            }
            if (textBoxDefSeg.Text == "")
            {
                MessageBox.Show("請輸入預設段數");
                return;
            }
            if (textBoxDefMin.Text == "")
            {
                MessageBox.Show("請輸入預設分鐘");
                return;
            }
            if (textBoxDefSec.Text == "")
            {
                MessageBox.Show("請輸入預設秒數");
                return;
            }
            if (comboBoxLive.SelectedIndex == -1)
            {
                MessageBox.Show("請選擇Live種類");
                return;
            }
            if (this.textBoxBTime.Text == "")
            {
                MessageBox.Show("請輸入起始時間");
                return;
            }
            if (this.textBoxETime.Text == "")
            {
                MessageBox.Show("請輸入結束時間");
                return;
            }
            if (this.textBoxBTime.Text.Length < 4)
            {
                MessageBox.Show("起始時間格式有誤");
                return;
            }
            if (this.textBoxETime.Text.Length < 4)
            {
                MessageBox.Show("結束時間格式有誤");
                return;
            }
            try
            {
                int.Parse(textBoxDefSeg.Text);
                int.Parse(textBoxDefMin.Text);
                int.Parse(textBoxDefSec.Text);
                int.Parse(textBoxBTime.Text);
                int.Parse(textBoxETime.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("預設段數,預設長度,起始時間,結束時間都必須為數字格式");
                return;
            }


            if (int.Parse(textBoxETime.Text) < int.Parse(textBoxBTime.Text))
            {
                MessageBox.Show("結束時間必須大於起始時間");
                return;
                
            }

            if (SourceBTime != null && SourceETime != null)
            {
                if (int.Parse(textBoxBTime.Text) < int.Parse(SourceBTime) || int.Parse(textBoxETime.Text) > int.Parse(SourceETime))
                {
                    if (MessageBox.Show("您輸入的起訖時間超過原節目的起訖時間" + SourceBTime + "--" + SourceETime + ",您確定要置換節目?", "置換節目", MessageBoxButton.OKCancel) == MessageBoxResult.Cancel)
                    {
                        return;
                    }
                }
            }

                
           
            GetProgEpisodeSEG(textBoxProgID.Text, textBoxEpisode.Text, _ChannelFileType, "", textBoxDefSeg.Text);
                //先建立或取得播映序號
                WSPGMSendSQL.SendSQLSoapClient ReplaceQueueProg = new WSPGMSendSQL.SendSQLSoapClient();
                StringBuilder sb = new StringBuilder();
                object ReturnXML = "";

                sb.AppendLine("<Data>");
                sb.AppendLine("<FSPROG_ID>" + this.textBoxProgID.Text + "</FSPROG_ID>");
                sb.AppendLine("<FNEPISODE>" + this.textBoxEpisode.Text + "</FNEPISODE>");
                sb.AppendLine("<FDDATE>" + _date + "</FDDATE>");
                sb.AppendLine("<FSBTIME>" + this.textBoxBTime.Text + "</FSBTIME>");
                sb.AppendLine("<FSETIME>" + this.textBoxETime.Text + "</FSETIME>");
                sb.AppendLine("<FSPROGNAME>" + TransferTimecode.ReplaceXML(this.textBoxProgName.Text) + "</FSPROGNAME>");
                sb.AppendLine("<FNDUR>" + this.textBoxDefMin.Text + "</FNDUR>");
                sb.AppendLine("<FNSEG>" + this.textBoxDefSeg.Text + "</FNSEG>");
                sb.AppendLine("<FSPLAY_TYPE>" + ((ComboBoxItem)comboBoxLive.SelectedItem).Tag.ToString() + "</FSPLAY_TYPE>");
                sb.AppendLine("<FSCHANNEL_ID>" + _channelid + "</FSCHANNEL_ID>");
                sb.AppendLine("<FSMEMO>" + TransferTimecode.ReplaceXML(textBoxMemo.Text) + "</FSMEMO>");
                sb.AppendLine("<FSMEMO1>" + TransferTimecode.ReplaceXML(textBoxMemo1.Text) + "</FSMEMO1>");
                sb.AppendLine("<FSSIGNAL>" + ((ComboBoxItem)comboBoxSignal1.SelectedItem).Content.ToString() + "</FSSIGNAL>");
                sb.AppendLine("<FNDEF_MIN>" + textBoxDefMin.Text + "</FNDEF_MIN>");
                sb.AppendLine("<FNDEF_SEC>" + textBoxDefSec.Text + "</FNDEF_SEC>");
                sb.AppendLine("<SFSPROGID>" + ReplaceProgList[0].SFSPROG_ID + "</SFSPROGID>");
                sb.AppendLine("<SFNSEQNO>" + ReplaceProgList[0].SFNSEQNO + "</SFNSEQNO>");
                sb.AppendLine("</Data>");
                    
                //sb=sb.Replace("\r\n", "");
                ReplaceQueueProg.ReplaceQueueProgAsync("", sb.ToString());
                ReplaceQueueProg.ReplaceQueueProgCompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (args.Result != "" && args.Result != null)
                        {
                            if (args.Result.Substring(0, 5) == "Error")
                            {
                                MessageBox.Show(args.Result);
                                return;
                            }
                            byte[] byteArray2 = Encoding.Unicode.GetBytes(args.Result);
                            StringBuilder output2 = new StringBuilder();
                            // Create an XmlReader

                            XDocument doc = XDocument.Load(new MemoryStream(byteArray2));
                            var ltox = from str in doc.Elements("Datas").Elements("Data")
                                       select str;
                            string DFNSEQNO = "";
                            string DVIDEO_ID = "";
                            foreach (XElement elem in ltox)
                            {
                                 DFNSEQNO = XElementToString(elem.Element("FNSEQNO"));
                                 DVIDEO_ID = XElementToString(elem.Element("FSVIDEO_ID"));
                            }
                            for (int i = ReplaceProgList.Count - 1; i >= 0; i--)
                            {
                                ReplaceProgList[i].DFNSEQNO = DFNSEQNO;
                                ReplaceProgList[i].FSVIDEO_ID = DVIDEO_ID;
                                ReplaceProgList[i].FNLIVE = ((ComboBoxItem)comboBoxLive.SelectedItem).Tag.ToString();
                                ReplaceProgList[i].FSMEMO = textBoxMemo.Text;
                                ReplaceProgList[i].FSMEMO1 = textBoxMemo.Text;
                                ReplaceProgList[i].FSSIGNAL = ((ComboBoxItem)comboBoxSignal1.SelectedItem).Content.ToString();
                            }

                            ////將新加入的節目回傳到節目表維護介面
                            //_PgmQueueProg.ProgID = this.textBoxProgID.Text;
                            //_PgmQueueProg.Seqno = args.Result;
                            //_PgmQueueProg.ProgName = this.textBoxProgName.Text;
                            //_PgmQueueProg.ProgDate = _date;
                            ////ProgDate = elem.Element("FDDATE").Value.ToString(),
                            //_PgmQueueProg.CHANNEL_ID = _channelid;
                            //_PgmQueueProg.BEG_TIME = this.textBoxBTime.Text;
                            //_PgmQueueProg.END_TIME = this.textBoxETime.Text;
                            //_PgmQueueProg.REPLAY = "0";
                            //_PgmQueueProg.EPISODE = this.textBoxEpisode.Text;
                            ////_PgmQueueProg.TAPENO = QueryProg.FSFILE_NO;
                            //_PgmQueueProg.PROG_NAME = this.textBoxProgName.Text;
                            //_PgmQueueProg.LIVE = ((ComboBoxItem)comboBoxLive.SelectedItem).Tag.ToString();
                            //_PgmQueueProg.SIGNAL = ((ComboBoxItem)comboBoxSignal1.SelectedItem).Content.ToString();
                            //_PgmQueueProg.DEP = "0";
                            //_PgmQueueProg.ONOUT = "0";
                            //_PgmQueueProg.MEMO = this.textBoxMemo.Text;
                            //_PgmQueueProg.MEMO1 = this.textBoxMemo1.Text;
                            //_PgmQueueProg.CHANNEL_NAME = _channelName;
                            //_PgmQueueProg.FILETYPE = "";
                            //_PgmQueueProg.DEF_MIN = this.textBoxDefMin.Text;
                            //_PgmQueueProg.DEF_SEC = this.textBoxDefSec.Text;
                            //_PgmQueueProg.ProgDefSeg = this.textBoxDefSeg.Text;

                            //if (int.Parse(this.textBoxBTime.Text) >= 2400)
                            //    _PgmQueueProg.RealBEG_TIME = (int.Parse(this.textBoxBTime.Text) - 2400).ToString("0000");
                            //else
                            //    _PgmQueueProg.RealBEG_TIME = this.textBoxBTime.Text;

                            //if (int.Parse(this.textBoxETime.Text) >= 2400)
                            //    _PgmQueueProg.RealEND_TIME = (int.Parse(this.textBoxETime.Text) - 2400).ToString("0000");
                            //else
                            //    _PgmQueueProg.RealEND_TIME = this.textBoxETime.Text;
                            //_PgmQueueProg.BEG_TIME = this.textBoxBTime.Text;
                            //_PgmQueueProg.END_TIME = this.textBoxETime.Text;

                            //播映資料建立完成開始寫入TBPGMQUEUE與刪除被置換的的節目資料
                            _HasReplace = true;
                            this.DialogResult = true;
                            //MessageBox.Show("修改完成");
                        }
                        else
                        {
                            MessageBox.Show("置換節目失敗,建立播映資料時發生錯誤");
                            return;
                        }

                    }
                };
               
            //this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
        QueryProgFile QueryProg = new QueryProgFile();
        //List<QueryProgFile> GETQueryProgFile = new List<QueryProgFile>();
        private void buttonQueryProg_Click(object sender, RoutedEventArgs e)
        {
            PGM100_04 PGM100_04_Frm = new PGM100_04();
            PGM100_04_Frm._ChannelFileType = _ChannelFileType;
            PGM100_04_Frm.Show();
            PGM100_04_Frm.Closed += (s, args) =>
            {
                if (PGM100_04_Frm._FSPROG_ID != null)
                {
                    textBoxProgID.Text = PGM100_04_Frm._FSPROG_ID;
                    textBoxEpisode.Text = PGM100_04_Frm._FNEPISODE;
                    textBoxProgName.Text = PGM100_04_Frm._FSPGDNAME;
                    QueryProg.FSPROG_ID = PGM100_04_Frm._FSPROG_ID;
                    QueryProg.FNEPISODE = PGM100_04_Frm._FNEPISODE;
                    QueryProg.FSPGDNAME = PGM100_04_Frm._FSPGDNAME;
                    QueryProg.FSFILE_NO = PGM100_04_Frm._FSFILE_NO;
                    QueryProg.FSLENGTH = PGM100_04_Frm._FSLENGTH;
                    QueryProg.FCLOW_RES = PGM100_04_Frm._FCLOW_RES;
                    GetDefSetting();
                }
             };
        }

        private void buttonSetLouthKey_Click(object sender, RoutedEventArgs e)
        {
            PGM.PGM100_02 SetLouthKey_Frm = new PGM.PGM100_02();

            SetLouthKey_Frm.InLouthKeyString = textBoxMemo.Text;
            //SetLouthKey_Frm.AnalyLouthKeyString(textBoxMemo.Text);
            SetLouthKey_Frm.WorkChannelID = _channelid;
            SetLouthKey_Frm.Show();
            SetLouthKey_Frm.Closed += (s, args) =>
            {
                if (SetLouthKey_Frm.OutLouthKeyString != null)
                {
                    textBoxMemo.Text = SetLouthKey_Frm.OutLouthKeyString;
                }

            };
        }

        private void textBoxEpisode_LostFocus(object sender, RoutedEventArgs e)
        {
            GetDefSetting();
        }

        private void GetProgEpisodeSEG(string PROGID,string EPISODE,string CHANNELTYPE,string DEFDUR,string DEFSEQMENT)
        {
            if (textBoxProgID.Text == "")
            {
                MessageBox.Show("請查詢節目");
                return;
            }
            if (textBoxEpisode.Text == "")
            {
                MessageBox.Show("請查詢節目集數資料");
                return;
            }
            if (textBoxProgName.Text == "")
            {
                MessageBox.Show("請查詢節目名稱資料");
                return;
            }
            if (textBoxDefSeg.Text == "")
            {
                MessageBox.Show("請輸入預設段數");
                return;
            }
            if (textBoxDefMin.Text == "")
            {
                MessageBox.Show("請輸入預設分鐘");
                return;
            }
            if (textBoxDefSec.Text == "")
            {
                MessageBox.Show("請輸入預設秒數");
                return;
            }

            if (this.textBoxBTime.Text == "")
            {
                MessageBox.Show("請輸入起始時間");
                return;
            }
            if (this.textBoxETime.Text == "")
            {
                MessageBox.Show("請輸入結束時間");
                return;
            }
            if (this.textBoxBTime.Text.Length < 4)
            {
                MessageBox.Show("起始時間格式有誤");
                return;
            }
            if (this.textBoxETime.Text.Length < 4)
            {
                MessageBox.Show("結束時間格式有誤");
                return;
            }
            //先建立或取得播映序號
                WSPGMSendSQL.SendSQLSoapClient GetProgSEGMENT = new WSPGMSendSQL.SendSQLSoapClient();
                StringBuilder sb = new StringBuilder();
                object ReturnXML = "";
       

                sb.AppendLine("<Data>");
                sb.AppendLine("<FSProgID>" + this.textBoxProgID.Text + "</FSProgID>");
                sb.AppendLine("<FNEpisode>" + this.textBoxEpisode.Text + "</FNEpisode>");
               sb.AppendLine("<FSChannel_ID>" + _channelid + "</FSChannel_ID>");

               int defdur;
               defdur = int.Parse(textBoxETime.Text.Substring(0, 2)) * 60 + int.Parse(textBoxETime.Text.Substring(2, 2))
                      - int.Parse(textBoxBTime.Text.Substring(0, 2)) * 60 - int.Parse(textBoxBTime.Text.Substring(2, 2));

               sb.AppendLine("<FNDUR>" + defdur.ToString() + "</FNDUR>");
                sb.AppendLine("<FNDEF_MIN>" + this.textBoxDefMin.Text + "</FNDEF_MIN>");
                sb.AppendLine("<FNDEF_SEC>" + this.textBoxDefSec.Text + "</FNDEF_SEC>");
                sb.AppendLine("<DEFSEG>" + this.textBoxDefSeg.Text + "</DEFSEG>");
                sb.AppendLine("</Data>");
                    
                //sb=sb.Replace("\r\n", "");
                GetProgSEGMENT.GetProgDefAsync("", sb.ToString());
                GetProgSEGMENT.GetProgDefCompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (args.Result != "" && args.Result != null)
                        {
                            byte[] byteArray2 = Encoding.Unicode.GetBytes(args.Result);
                            StringBuilder output2 = new StringBuilder();
                            // Create an XmlReader

                            XDocument doc = XDocument.Load(new MemoryStream(byteArray2));
                            var ltox = from str in doc.Elements("Datas").Elements("Data")
                                       select str;
        
                            string FSProgID = "";
                            string FNEpisode = "";
                            string FSBEG_TIMECODE = "";
                            string FSEND_TIMECODE = "";
                            string FNSEG_ID = "";
                            string FNDUR_ID = "";
                            string FSVIDEO_ID = "";
                            string FSFILE_NO = "";
                            int maxReturnSeg = 0;

                            foreach (XElement elem in ltox)
                            {

                                FSProgID = textBoxProgID.Text;
                                FNEpisode = this.textBoxEpisode.Text;
                                FSBEG_TIMECODE = XElementToString(elem.Element("FSBEG_TIMECODE"));
                                FSEND_TIMECODE = XElementToString(elem.Element("FSEND_TIMECODE"));
                                FNSEG_ID = XElementToString(elem.Element("FNSEG_ID"));
                                FNDUR_ID = XElementToString(elem.Element("FNDUR_ID"));
                                FSVIDEO_ID = XElementToString(elem.Element("FSVIDEO_ID"));
                                FSFILE_NO = XElementToString(elem.Element("FSFILE_NO"));

                                if (int.Parse(FNSEG_ID) > maxReturnSeg)
                                    maxReturnSeg = int.Parse(FNSEG_ID);
                                if (int.Parse(FNSEG_ID) > ReplaceProgList.Count)
                                {

                                    ReplaceProg TempReplaceProg = new ReplaceProg();
                                   TempReplaceProg.SFSPROG_ID = "";
                                   TempReplaceProg.SFNSEQNO = "";
                                   TempReplaceProg.SFSPROG_NAME = "";
                                   TempReplaceProg.SFNBREAK_NO = "";
                                   TempReplaceProg.SFSPLAY_TIME = "";
                                   TempReplaceProg.SFSDURATION = "";
                                   TempReplaceProg.SFNEPISODE = "";
                                   TempReplaceProg.DFNSEQNO = "";
                                   TempReplaceProg.DFSPROG_ID = FSProgID;
                                   TempReplaceProg.DFNEPISODE = FNEpisode;
                                   TempReplaceProg.DFSDURATION = FNDUR_ID;
                                   TempReplaceProg.DFNBREAK_NO = FNSEG_ID;
                                   TempReplaceProg.DFSPROG_NAME = textBoxProgName.Text;
                                   TempReplaceProg.FSFILE_NO = FSFILE_NO;
                                   ReplaceProgList.Add(TempReplaceProg);
                                }
                                else
                                { 
                                    ReplaceProgList[int.Parse(FNSEG_ID)-1].DFSPROG_ID=FSProgID;
                                    ReplaceProgList[int.Parse(FNSEG_ID)-1].DFNEPISODE=FNEpisode;
                                    ReplaceProgList[int.Parse(FNSEG_ID)-1].DFSDURATION=FNDUR_ID;
                                    ReplaceProgList[int.Parse(FNSEG_ID)-1].DFNBREAK_NO=FNSEG_ID;
                                    ReplaceProgList[int.Parse(FNSEG_ID)-1].DFSPROG_NAME=textBoxProgName.Text;
                                    ReplaceProgList[int.Parse(FNSEG_ID) - 1].FSFILE_NO = FSFILE_NO;
                                    

                                }

                            }

                            for (int i = maxReturnSeg+1; i <= ReplaceProgList.Count; i++)
                            {
                                ReplaceProgList[i - 1].DFSPROG_ID = "";
                                ReplaceProgList[i - 1].DFNEPISODE = "";
                                ReplaceProgList[i - 1].DFSDURATION = "";
                                ReplaceProgList[i - 1].DFNBREAK_NO = "";
                                ReplaceProgList[i - 1].DFSPROG_NAME = "";
                            }

                            for (int i = ReplaceProgList.Count-1; i >=0; i--)
                            {
                                if (ReplaceProgList[i].DFSPROG_ID == "" && ReplaceProgList[i].SFSPROG_ID=="")
                                {
                                    ReplaceProgList.RemoveAt(i);
                                }
                                
                            }
                            dataGridReplaceProgList.ItemsSource = null;
                            dataGridReplaceProgList.ItemsSource = ReplaceProgList;
                        }
                        else
                        {
                            MessageBox.Show("取得資料");
                            return;
                        }

                    }
                };
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            GetProgEpisodeSEG(textBoxProgID.Text,textBoxEpisode.Text, _ChannelFileType,"",textBoxDefSeg.Text);
        }

         public void QueryDefProg(string FSPROG_ID,string FNSEQNO,string FDDATE,string FSCHANNEL_ID)
        {
           
            //先建立或取得播映序號
            WSPGMSendSQL.SendSQLSoapClient Get_QUEUE_Prog = new WSPGMSendSQL.SendSQLSoapClient();
               
                //sb=sb.Replace("\r\n", "");
            Get_QUEUE_Prog.Get_QUEUE_ProgAsync(FSPROG_ID, FNSEQNO, FDDATE, FSCHANNEL_ID);
            Get_QUEUE_Prog.Get_QUEUE_ProgCompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (args.Result != "" && args.Result != null)
                        {
                            byte[] byteArray2 = Encoding.Unicode.GetBytes(args.Result);
                            StringBuilder output2 = new StringBuilder();
                            // Create an XmlReader

                            XDocument doc = XDocument.Load(new MemoryStream(byteArray2));
                            var ltox = from str in doc.Elements("Datas").Elements("Data")
                                       select str;

                            string FSBEG_TIME = "";
                            string FSEND_TIME = "";
                            string FNREPLAY = "";
                            string FNEPISODE = "";
                            string FSTAPENO = "";
                            string FSPROG_NAME = "";
                            string FNLIVE = "";
                            string FSSIGNAL = "";
                            string FNDEP = "";
                            string FNONOUT = "";
                            string FSMEMO = "";
                            string FSMEMO1 = "";
                            string FSCHANNEL_NAME = "";
                            string FNSEG = "";
                            string FNDUR = "";
                            string FNDEF_MIN = "";
                            string FNDEF_SEC = "";
                            foreach (XElement elem in ltox)
                            {
                                FSBEG_TIME = XElementToString(elem.Element("FSBEG_TIME"));
                                FSEND_TIME = XElementToString(elem.Element("FSEND_TIME"));
                                FNREPLAY = XElementToString(elem.Element("FNREPLAY"));
                                FNEPISODE = XElementToString(elem.Element("FNEPISODE"));
                                FSTAPENO = XElementToString(elem.Element("FSTAPENO"));
                                FSPROG_NAME = XElementToString(elem.Element("FSPROG_NAME"));
                                FNLIVE = XElementToString(elem.Element("FNLIVE"));
                                FSSIGNAL = XElementToString(elem.Element("FSSIGNAL"));
                                FNDEP = XElementToString(elem.Element("FNDEP"));
                                FNONOUT = XElementToString(elem.Element("FNONOUT"));
                                FSMEMO = XElementToString(elem.Element("FSMEMO"));
                                FSMEMO1 = XElementToString(elem.Element("FSMEMO1"));
                                FSCHANNEL_NAME = XElementToString(elem.Element("FSCHANNEL_NAME"));
                                FNSEG = XElementToString(elem.Element("FNSEG"));
                                FNDUR = XElementToString(elem.Element("FNDUR"));
                                FNDEF_MIN = XElementToString(elem.Element("FNDEF_MIN"));
                                FNDEF_SEC = XElementToString(elem.Element("FNDEF_SEC"));


                                textBoxProgID.Text = FSPROG_ID;
                                textBoxProgName.Text = FSPROG_NAME;
                                
                                textBoxBTime.Text = FSBEG_TIME;
                                textBoxETime.Text = FSEND_TIME;
                                textBoxEpisode.Text = FNEPISODE;

                                SourceBTime = FSBEG_TIME;
                                SourceETime = FSEND_TIME;
                               
                                comboBoxLive.SelectedIndex = -1;
                                
                                for (int Combocount = 0; Combocount < comboBoxLive.Items.Count; Combocount++)
                                {
                                    if (((ComboBoxItem)comboBoxLive.Items[Combocount]).Tag.ToString() == FNLIVE)
                                    {
                                        comboBoxLive.SelectedIndex = Combocount;
                                    }
                                }

                                //PGMQUEUEMODIFY_Frm._InSIgnal = PgmQueueSource[dataGridQueue.SelectedIndex].SIGNAL;
                                comboBoxSignal1.SelectedIndex = -1;
                                for (int Combocount = 0; Combocount < comboBoxSignal1.Items.Count; Combocount++)
                                {
                                    if (((ComboBoxItem)comboBoxSignal1.Items[Combocount]).Content.ToString() == FSSIGNAL)
                                    {
                                        comboBoxSignal1.SelectedIndex = Combocount;
                                    }
                                }


                                textBoxMemo.Text = FSMEMO;
                                textBoxMemo1.Text = FSMEMO1;
                                textBoxDefSeg.Text = FNSEG;
                                textBoxDefMin.Text = FNDEF_MIN;
                                textBoxDefSec.Text = FNDEF_SEC;
                             
                            }

                         
                        }
                     
                    }
                };
        }

              
    }
}

