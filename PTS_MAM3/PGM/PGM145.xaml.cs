﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Text;
using System.Xml.Linq;
using System.IO;
using PTS_MAM3.WSPROPOSAL;
using System.Windows.Browser;
using PTS_MAM3.PGM;

namespace PTS_MAM3.PGM
{
    public partial class PGM145 : Page
    {
        public PGM145()
        {
            InitializeComponent();
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBZCHANNEL_ALL = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";
            ModuleClass.getModulePermission("");

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            sb.AppendLine("</Data>");
            SP_Q_TBZCHANNEL_ALL.Do_QueryAsync("[SP_Q_TBZCHANNEL_ALL]", sb.ToString(), ReturnXML);
            //SP_Q_TBPGM_QUEUE.Do_QueryCompleted += new EventHandler<SendSQL.Do_QueryCompletedEventArgs>(SP_Q_TBPGM_QUEUE_Do_QueryCompleted);
            //AAA.Do_InsertAsync("", "", ReturnStr);
            SP_Q_TBZCHANNEL_ALL.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            //if (ModuleClass.getModulePermission("P1" + elem.Element("ID").Value.ToString() + "001") == true || ModuleClass.getModulePermission("P1" + elem.Element("ID").Value.ToString() + "002") == true)
                            //{
                            if (ModuleClass.getModulePermission("P700001") == true)
                            {
                                if (elem.Element("TYPE").Value.ToString() == "002") //只呈現HD的頻道
                                {
                                    ComboBoxItem TempComboBoxItem = new ComboBoxItem();
                                    TempComboBoxItem.Content = elem.Element("NAME").Value.ToString();
                                    TempComboBoxItem.Tag = elem.Element("ID").Value.ToString() + "-" + elem.Element("TYPE").Value.ToString();
                                    comboBoxChannel.Items.Add(TempComboBoxItem);
                                }
                            }
                            //}

                        }
                    }
                }
            };
        }


        List<PgmDateFileStatus> PGM_PLAYLIST_STATUS = new List<PgmDateFileStatus>();
        List<PgmDateFileStatus> PGM_FILE_LIST = new List<PgmDateFileStatus>();
             
        string _ChannelType;
        string _ChannelID;
        string _Date;
        string _ChannelName;

        private void buttonQuery_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                //RR.Header="Progid";
                //radGridView1.Columns.Add(RR);
                //RR.Header="ProgNAME";
                //radGridView1.Columns.Add(RR);
                if (comboBoxChannel.SelectedIndex == -1)
                {
                    MessageBox.Show("請先選擇要查詢之頻道");
                    return;
                }
                //if (ModuleClass.getModulePermission("P1" + ((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString().Split(new Char[] { '-' })[0] + "002") == false)
                //{
                //    MessageBox.Show("您沒有查詢此頻道節目表的權限");
                //    return;
                //}

                BusyMsg.IsBusy = true;

                WSPGMSendSQL.SendSQLSoapClient SP_Q_HD_PlayList_File_Status = new WSPGMSendSQL.SendSQLSoapClient();
                //string ReturnStr = "";

                StringBuilder sb = new StringBuilder();
                object ReturnXML = "";
                sb.AppendLine("<Data>");
                //sb.AppendLine("<FDDATE>" + this.textBoxDate.Text + "</FDDATE>");
                if (comboBoxChannel.SelectedIndex != -1)
                {
                    string[] ChannelType = ((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString().Split(new Char[] { '-' });
                    //sb.AppendLine("<FSCHANNEL_ID>" + ChannelType[0] + "</FSCHANNEL_ID>");
                    _ChannelType = ChannelType[1];
                    _ChannelID = ChannelType[0];
                    _Date = this.textBoxDate.Text;
                    _ChannelName = ((ComboBoxItem)comboBoxChannel.SelectedItem).Content.ToString();
                }
                //_Date="2010/12/16";
                //_ChannelID="06";
                sb.AppendLine("<FDDATE>" + _Date + "</FDDATE>");
                sb.AppendLine("<FSCHANNEL_ID>" + _ChannelID + "</FSCHANNEL_ID>");
                sb.AppendLine("</Data>");
                SP_Q_HD_PlayList_File_Status.Do_Get_HD_PlayList_File_StatusAsync("SP_Q_GET_HD_PLAYLIST_FILE_STATUS", sb.ToString(), ReturnXML);
                //SP_Q_TBPGM_QUEUE.Do_QueryCompleted += new EventHandler<SendSQL.Do_QueryCompletedEventArgs>(SP_Q_TBPGM_QUEUE_Do_QueryCompleted);
                //AAA.Do_InsertAsync("", "", ReturnStr);
                int _NO = 0;
                int _Uno = 0;
                SP_Q_HD_PlayList_File_Status.Do_Get_HD_PlayList_File_StatusCompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (args.Result != null && args.Result != "")
                        {

                            byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                            PGM_PLAYLIST_STATUS.Clear();
                            PGM_FILE_LIST.Clear();
                            StringBuilder output = new StringBuilder();
                            // Create an XmlReader
                            XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                            var ltox = from str in doc.Elements("Datas").Elements("Data")
                                       select str;
                            foreach (XElement elem in ltox)
                            {
                               
        //                                public string NO { get; set; }
        //public string FSPROG_ID { get; set; }
        //public string FNEPISODE { get; set; }
        //public string FSPROG_NAME { get; set; }
        //public string FNBREAK_NO { get; set; }
        //public string FCPROPERTY { get; set; }
        //public string fsplay_time { get; set; }
        //public string FSDUR { get; set; }
        //public string FNLIVE { get; set; }
        //public string FSFILE_NO { get; set; }
        //public string FSVIDEO_ID { get; set; }
        //public string FSMEMO { get; set; }
        //public string FCCHECK_STATUS { get; set; }
        //public string FCFILE_STATUS { get; set; }
        //public string FCSTATUS { get; set; }
        //public string FSBEG_TIMECODE { get; set; }
        //public string FSEND_TIMECODE { get; set; }
        //public string Real_File_Status { get; set; }
                                _NO=_NO+1;
                                string NO = _NO.ToString();
                                string FSPROG_ID = elem.Element("FSPROG_ID").Value.ToString();

                                //string RealBTime;
                                //string RealETime;
                                //if (int.Parse(elem.Element("FSBEG_TIMECODE").Value.ToString()) >= 2400)
                                //    RealBTime = (int.Parse(elem.Element("FSBEG_TIMECODE").Value.ToString()) - 2400).ToString("0000");
                                //else
                                //    RealBTime = elem.Element("FSBEG_TIMECODE").Value.ToString();

                                //if (int.Parse(elem.Element("FSEND_TIMECODE").Value.ToString()) >= 2400)
                                //    RealETime = (int.Parse(elem.Element("FSEND_TIMECODE").Value.ToString()) - 2400).ToString("0000");
                                //else
                                //    RealETime = elem.Element("FSEND_TIMECODE").Value.ToString();

                                //A.FSPROG_ID,A.FNEPISODE,A.FSPROG_NAME,A.FNBREAK_NO,A.FCPROPERTY,A.fsplay_time,A.FSDUR,A.FNLIVE,A.FSFILE_NO,A.FSVIDEO_ID,A.FSMEMO,G.FCCHECK_STATUS,C.FCFILE_STATUS,D.FCSTATUS,F.FSBEG_TIMECODE,F.FSEND_TIMECODE 
                                string RealStatus="";
                                if (elem.Element("Real_File_Status").Value.ToString() != "")
                                    RealStatus = elem.Element("Real_File_Status").Value.ToString();
                                else
                                    RealStatus=Translate_Status_Name.TransStatusName(elem.Element("FCCHECK_STATUS").Value.ToString(), elem.Element("FCFILE_STATUS").Value.ToString(), elem.Element("FCSTATUS").Value.ToString());

                                //先檢查是否有加入過清單
                                bool HasAdd = false;
                                foreach (PgmDateFileStatus TempUniqueList in PGM_FILE_LIST)
                                {
                                    if (TempUniqueList.FSVIDEO_ID == elem.Element("FSVIDEO_ID").Value.ToString()) 
                                    {
                                            HasAdd = true;
                                          
                                    }
                                }
                                if (HasAdd==false)
                                {
                                    _Uno = _Uno + 1;
                                    PGM_FILE_LIST.Add(new PgmDateFileStatus()
                                    {
                                        NO = _Uno.ToString(),
                                        FSPROG_ID = elem.Element("FSPROG_ID").Value.ToString(),
                                        FNEPISODE = elem.Element("FNEPISODE").Value.ToString(),
                                        FSPROG_NAME = elem.Element("FSPROG_NAME").Value.ToString(),
                                        FNBREAK_NO = "",
                                        FCPROPERTY = elem.Element("FCPROPERTY").Value.ToString(),
                                        fsplay_time = "",
                                        FSDUR = elem.Element("FSDUR").Value.ToString(),
                                        FNLIVE = elem.Element("FNLIVE").Value.ToString(),
                                        FSFILE_NO = elem.Element("FSFILE_NO").Value.ToString(),
                                        FSVIDEO_ID = elem.Element("FSVIDEO_ID").Value.ToString(),
                                        FSMEMO = "",
                                        FCCHECK_STATUS = elem.Element("FCCHECK_STATUS").Value.ToString(),
                                        FCFILE_STATUS = elem.Element("FCFILE_STATUS").Value.ToString(),
                                        FCSTATUS = elem.Element("FCSTATUS").Value.ToString(),
                                        FSBEG_TIMECODE = elem.Element("FSBEG_TIMECODE").Value.ToString(),
                                        FSEND_TIMECODE = elem.Element("FSEND_TIMECODE").Value.ToString(),
                                        Real_File_Status = RealStatus,
                                    });
                                }
                            

                              

                                PGM_PLAYLIST_STATUS.Add(new PgmDateFileStatus()
                                {
                                    //    public string NO { get; set; }
                                    //    public string FSPROG_ID { get; set; }
                                    //    public string FNEPISODE { get; set; }
                                    //    public string FSPROG_NAME { get; set; }
                                    //    public string FNBREAK_NO { get; set; }
                                    //    public string FCPROPERTY { get; set; }
                                    //    public string fsplay_time { get; set; }
                                    //    public string FSDUR { get; set; }
                                    //    public string FNLIVE { get; set; }
                                    //    public string FSFILE_NO { get; set; }
                                    //    public string FSVIDEO_ID { get; set; }
                                    //    public string FSMEMO { get; set; }
                                    //    public string FCFILE_STATUS { get; set; }
                                    //    public string FCSTATUS { get; set; }
                                    //    public string FSBEG_TIMECODE { get; set; }
                                    //    public string FSEND_TIMECODE { get; set; }
                                    NO =  _NO.ToString(),
                                    FSPROG_ID = elem.Element("FSPROG_ID").Value.ToString(),
                                    FNEPISODE = elem.Element("FNEPISODE").Value.ToString(),
                                    FSPROG_NAME = elem.Element("FSPROG_NAME").Value.ToString(),
                                    //ProgDate = (Convert.ToDateTime(elem.Element("FDDATE").Value)).ToString("yyyy/MM/dd"),

                                    FNBREAK_NO = elem.Element("FNBREAK_NO").Value.ToString(),
                                    FCPROPERTY = elem.Element("FCPROPERTY").Value.ToString(),
                                    fsplay_time = elem.Element("fsplay_time").Value.ToString(),
                                    FSDUR = elem.Element("FSDUR").Value.ToString(),
                                    FNLIVE = elem.Element("FNLIVE").Value.ToString(),
                                    FSFILE_NO = elem.Element("FSFILE_NO").Value.ToString(),
                                    FSVIDEO_ID = elem.Element("FSVIDEO_ID").Value.ToString(),
                                    FSMEMO = elem.Element("FSMEMO").Value.ToString(),
                                    FCCHECK_STATUS = elem.Element("FCCHECK_STATUS").Value.ToString(),
                                    FCFILE_STATUS = elem.Element("FCFILE_STATUS").Value.ToString(),
                                    FCSTATUS = elem.Element("FCSTATUS").Value.ToString(),
                                    FSBEG_TIMECODE = elem.Element("FSBEG_TIMECODE").Value.ToString(),
                                    FSEND_TIMECODE = elem.Element("FSEND_TIMECODE").Value.ToString(),
                                    Real_File_Status = RealStatus,

                                });
                            }
                            dataGridQueue.ItemsSource = null;
                            if (radioButtonPlayList.IsChecked==true)
                                dataGridQueue.ItemsSource = PGM_PLAYLIST_STATUS;
                            else
                                dataGridQueue.ItemsSource = PGM_FILE_LIST;
                            BusyMsg.IsBusy = false;

                            MessageBox.Show("查詢完畢");
                        } //if (args.Result != null && args.Result !="")
                        else
                        {
                            BusyMsg.IsBusy = false;
                            MessageBox.Show("節目表不存在");
                        }
                    }
                };
            }
            catch
            {
                BusyMsg.IsBusy = false;
                MessageBox.Show("查詢節目表失敗!");
            }
        }

      

        private void buttonGetDate_Click(object sender, RoutedEventArgs e)
        {
            if (calendar1.Visibility == System.Windows.Visibility.Visible)
                calendar1.Visibility = System.Windows.Visibility.Collapsed;
            else
                calendar1.Visibility = System.Windows.Visibility.Visible;
        }

        private void calendar1_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
        {
            //this.textBoxDate.Text = this.calendar1.SelectedDate.ToString();
            DateTime Dt1 = calendar1.SelectedDate.Value;
            this.textBoxDate.Text = Dt1.ToString("yyyy/MM/dd");
            calendar1.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void radioButtonPlayList_Checked(object sender, RoutedEventArgs e)
        {
            //if (radioButtonPlayList.IsChecked == true)
            //    dataGridQueue.ItemsSource = PGM_PLAYLIST_STATUS;
            //else
            //    dataGridQueue.ItemsSource = PGM_FILE_LIST;
        }

        private void radioButtonFile_Checked(object sender, RoutedEventArgs e)
        {
            //if (radioButtonPlayList.IsChecked == true)
            //    dataGridQueue.ItemsSource = PGM_PLAYLIST_STATUS;
            //else
            //    dataGridQueue.ItemsSource = PGM_FILE_LIST;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            dataGridQueue.LoadingRow += (s1, args1) =>
            {
                PgmQueueDate PGMQueue = args1.Row.DataContext as PgmQueueDate;
                args1.Row.Background = new SolidColorBrush(Colors.White);
                args1.Row.Foreground = new SolidColorBrush(Colors.Black);


                if (radioButtonPlayList.IsChecked == true)
                {
                    if (PGM_PLAYLIST_STATUS[args1.Row.GetIndex()].FCPROPERTY == "P")
                    {
                        args1.Row.Background = new SolidColorBrush(Colors.Cyan);
                    }
                    if (PGM_PLAYLIST_STATUS[args1.Row.GetIndex()].FCPROPERTY == "G")
                    {
                        args1.Row.Background = new SolidColorBrush(Colors.Yellow);
                    }
                    if (PGM_PLAYLIST_STATUS[args1.Row.GetIndex()].FNLIVE == "1")
                    {
                        args1.Row.Background = new SolidColorBrush(Colors.Red);
                        args1.Row.Foreground = new SolidColorBrush(Colors.White);
                    }
                }
                else
                {
                    if (PGM_FILE_LIST[args1.Row.GetIndex()].FCPROPERTY == "P")
                    {
                        args1.Row.Background = new SolidColorBrush(Colors.Cyan);
                    }
                    if (PGM_FILE_LIST[args1.Row.GetIndex()].FCPROPERTY == "G")
                    {
                        args1.Row.Background = new SolidColorBrush(Colors.Yellow);
                    }
                    if (PGM_FILE_LIST[args1.Row.GetIndex()].FNLIVE == "1")
                    {
                        args1.Row.Background = new SolidColorBrush(Colors.Red);
                        args1.Row.Foreground = new SolidColorBrush(Colors.White);
                    }
                }
                   
                
            };
        }

        private void radioButtonPlayList_Click(object sender, RoutedEventArgs e)
        {
            if (radioButtonPlayList.IsChecked == true)
                dataGridQueue.ItemsSource = PGM_PLAYLIST_STATUS;
            else
                dataGridQueue.ItemsSource = PGM_FILE_LIST;
        }

        private void radioButtonFile_Click(object sender, RoutedEventArgs e)
        {
            if (radioButtonPlayList.IsChecked == true)
                dataGridQueue.ItemsSource = PGM_PLAYLIST_STATUS;
            else
                dataGridQueue.ItemsSource = PGM_FILE_LIST;
        }

    }
}
