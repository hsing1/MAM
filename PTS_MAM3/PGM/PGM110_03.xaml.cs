﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using System.Xml.Linq;
using System.IO;
using System.Globalization;

namespace PTS_MAM3.PGM
{
    public partial class PGM110_03 : Page
    {
        public PGM110_03()
        {
            InitializeComponent();

            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBZCHANNEL_ALL = new WSPGMSendSQL.SendSQLSoapClient();
            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            sb.AppendLine("</Data>");
            SP_Q_TBZCHANNEL_ALL.Do_QueryAsync("[SP_Q_TBZCHANNEL_ALL]", sb.ToString(), ReturnXML);
            //SP_Q_TBPGM_QUEUE.Do_QueryCompleted += new EventHandler<SendSQL.Do_QueryCompletedEventArgs>(SP_Q_TBPGM_QUEUE_Do_QueryCompleted);
            //AAA.Do_InsertAsync("", "", ReturnStr);
            SP_Q_TBZCHANNEL_ALL.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {


                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);

                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {

                            //if (ModuleClass.getModulePermission("P3" + elem.Element("ID").Value.ToString() + "001") == true)
                            //{
                            ComboBoxItem TempComboBoxItem = new ComboBoxItem();
                            TempComboBoxItem.Content = elem.Element("NAME").Value.ToString();
                            TempComboBoxItem.Tag = elem.Element("ID").Value.ToString();

                            comboBoxChannel.Items.Add(TempComboBoxItem);

                            //}

                        }
                    }
                }
            };           
        }

        private void buttonQueryPromo_Click(object sender, RoutedEventArgs e)
        {
            PGM100_06 PGM100_06_Frm = new PGM100_06();
            PGM100_06_Frm.Show();
            PGM100_06_Frm.Closed += (s, args) =>
            {
                if (PGM100_06_Frm.DataGridPromoList.SelectedIndex >= 0)
                {
                    textBoxPromoID.Text = PGM100_06_Frm.ReturnPromoData.FSPROMO_ID;
                    textBoxNewPromoName.Text = PGM100_06_Frm.ReturnPromoData.FSPROMO_NAME;
                    textBoxPromoLength.Text = PGM100_06_Frm.ReturnPromoData.FSDURATION;
                    //_PromoTypeName = PGM100_06_Frm.ReturnPromoData.FSPROMOTYPENAME;
                }
            };
        }

        private void buttonQueryPromoBooking_Click(object sender, RoutedEventArgs e)
        {

        }

        List<PromoBooking> Promo_Booking_List = new List<PromoBooking>();
        public void QueryData()
        {
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_PROMO_BOOKING_BY_NAME = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");


            //sb.AppendLine("<FDDATE>" + this.textBoxDate.Text + "</FDDATE>");
            sb.AppendLine("<FNPROMO_BOOKING_NO>" + "" + "</FNPROMO_BOOKING_NO>");
            sb.AppendLine("<FSPROMO_ID>" + textBoxPromoID.Text + "</FSPROMO_ID>");
            //if (comboBoxStatus.SelectedIndex == -1)
                sb.AppendLine("<FCSTATUS>" + "" + "</FCSTATUS>");
            //else
            //    sb.AppendLine("<FCSTATUS>" + ((ComboBoxItem)comboBoxStatus.SelectedItem).Tag.ToString() + "</FCSTATUS>");
                sb.AppendLine("<FSPROMO_NAME>" + TransferTimecode.ReplaceXML(textBoxNewPromoName.Text) + "</FSPROMO_NAME>");
            sb.AppendLine("<FSUSER_ChtName>" + TransferTimecode.ReplaceXML(textBoxChtUserName.Text) + "</FSUSER_ChtName>");
            sb.AppendLine("<FDBDATE>" + datePickerBDate.Text + "</FDBDATE>");
            sb.AppendLine("<FDEDATE>" + datePickerEDate.Text + "</FDEDATE>");


            string _TempChannel;
            if (comboBoxChannel.SelectedIndex == -1)
                _TempChannel = "";
            else
                _TempChannel = ((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString();

            sb.AppendLine("<FSCHANNEL_ID>" + _TempChannel + "</FSCHANNEL_ID>");
            
            sb.AppendLine("</Data>");
            SP_Q_TBPGM_PROMO_BOOKING_BY_NAME.Do_QueryAsync("SP_Q_TBPGM_PROMO_BOOKING_BY_NAME_CHANNEL", sb.ToString(), ReturnXML);

            SP_Q_TBPGM_PROMO_BOOKING_BY_NAME.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {
                        Promo_Booking_List.Clear();
                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            string Status="";
                            if ((elem.Element("FCSTATUS").Value.ToString() == "Y") || (elem.Element("FCSTATUS").Value.ToString() == "R"))
                                Status = "審核通過";
                            else
                                Status = "建立失敗";

                            Promo_Booking_List.Add(new PromoBooking()
                            {
                                FNPROMO_BOOKING_NO = int.Parse(elem.Element("FNPROMO_BOOKING_NO").Value.ToString()),
                                FSPROMO_ID = elem.Element("FSPROMO_ID").Value.ToString(),
                                FCSTATUS = Status,
                                FSPROMO_NAME = elem.Element("FSPROMO_NAME").Value.ToString(),
                                FSDURATION = elem.Element("FSDURATION").Value.ToString(),
                                FSMEMO = elem.Element("FSMEMO").Value.ToString(),
                                FSCREATED_BY = elem.Element("FSCREATED_BY").Value.ToString(),
                                FSUSER_ChtName = elem.Element("FSUSER_ChtName").Value.ToString(),
                                FDCREATED_DATE = DateTime.Parse(elem.Element("FDCREATED_DATE").Value.ToString()).ToString("yyyy/MM/dd HH:mm:ss"),
                                FSCHANNEL_NAME = elem.Element("FSCHANNEL_NAME").Value.ToString(),
                            });


                        };
                        DCdataGridPromoBookingList.ItemsSource = null;
                        DCdataGridPromoBookingList.ItemsSource = Promo_Booking_List;

                        MessageBox.Show("查詢完畢");
                    }
                    else
                    {
                        Promo_Booking_List.Clear();
                        MessageBox.Show("沒有查到託播單資料");
                    }
                }
            };

        }

        private void DCdataGridPromoBookingList_RowDoubleClicked(object sender, DataGridDCTest.DataGridRowClickedArgs e)
        {
            if (DCdataGridPromoBookingList.SelectedIndex < 0)
            {
                MessageBox.Show("請先選取託播單資料");
                return;
            }

            PGM.PGM110_02 PGM110_02_View = new PGM.PGM110_02();
            PGM110_02_View._PromoBookingNO = ((PromoBooking)DCdataGridPromoBookingList.SelectedItems[0]).FNPROMO_BOOKING_NO.ToString();
            PGM110_02_View.QueryData();
            PGM110_02_View.Show();
            
        }


        private void btnPromoQuery_Click(object sender, RoutedEventArgs e)
        {
            QueryData();
        }

        private void btnPromoAdd_Click(object sender, RoutedEventArgs e)
        {

            //if (ModuleClass.getModulePermission("P3" + "00" + "004") == false)
            //{
            //    MessageBox.Show("您沒有新增託播單的權限");
            //    return;
            //}

            PGM.PGM110 PGM110_View = new PGM.PGM110();
            PGM110_View.Show();
        }

        private void btnPromoModify_Click(object sender, RoutedEventArgs e)
        {
            if (DCdataGridPromoBookingList.SelectedIndex < 0)
            {
                MessageBox.Show("請先選取託播單資料");
                return;
            }

            //if (((PromoBooking)DCdataGridPromoBookingList.SelectedItems[0]).FSCREATED_BY != UserClass.userData.FSUSER_ID)
            //{
            //    if (ModuleClass.getModulePermission("P3" + "00" + "002") == false)
            //    {
            //        MessageBox.Show("您沒有修改託播單的權限");
            //        return;
            //    }
            //}
            PGM.PGM110_01 PGM110_01_View = new PGM.PGM110_01();
            PGM110_01_View._PromoBookingNO = ((PromoBooking)DCdataGridPromoBookingList.SelectedItems[0]).FNPROMO_BOOKING_NO.ToString(); 
            //Promo_Booking_List[DCdataGridPromoBookingList.SelectedIndex].FNPROMO_BOOKING_NO;
            PGM110_01_View.QueryData();
            PGM110_01_View.Show();
        }

        private void btnPromoDelete_Click(object sender, RoutedEventArgs e)
        {
            int TempIndex = DCdataGridPromoBookingList.SelectedIndex;
            if (((PromoBooking)DCdataGridPromoBookingList.SelectedItems[0]).FNPROMO_BOOKING_NO.ToString() == "")
            {
                MessageBox.Show("請先選取要刪除的託播單");
                return;
            }
            if (MessageBox.Show("您確定要刪除[" + ((PromoBooking)DCdataGridPromoBookingList.SelectedItems[0]).FSPROMO_NAME + "]託播單?", "刪除托缽單", MessageBoxButton.OKCancel) == MessageBoxResult.Cancel)
            {
                return;
            }
            //if (((PromoBooking)DCdataGridPromoBookingList.SelectedItems[0]).FSCREATED_BY != UserClass.userData.FSUSER_ID)
            //{
            //    if (ModuleClass.getModulePermission("P3" + "00" + "003") == false)
            //    {
            //        MessageBox.Show("您沒有刪除託播單的權限");
            //        return;
            //    }
            //}

            WSPGMSendSQL.SendSQLSoapClient SP_D_PGM_PROMO_BOOKING = new WSPGMSendSQL.SendSQLSoapClient();
            SP_D_PGM_PROMO_BOOKING.Do_Delete_Promo_BookingAsync(((PromoBooking)DCdataGridPromoBookingList.SelectedItems[0]).FNPROMO_BOOKING_NO.ToString());

            SP_D_PGM_PROMO_BOOKING.Do_Delete_Promo_BookingCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != true)
                    {
                        MessageBox.Show("刪除失敗");
                        return;
                    }
                    else
                    {
                        Promo_Booking_List.Remove((PromoBooking)DCdataGridPromoBookingList.SelectedItems[0]);
                        //Promo_Booking_List.RemoveAt(DCdataGridPromoBookingList.SelectedIndex);
                        DCdataGridPromoBookingList.ItemsSource = null;
                        DCdataGridPromoBookingList.ItemsSource = Promo_Booking_List;

                        if (TempIndex < Promo_Booking_List.Count - 1)
                            DCdataGridPromoBookingList.SelectedIndex = TempIndex;
                        else
                            DCdataGridPromoBookingList.SelectedIndex = Promo_Booking_List.Count - 1;
                        DCdataGridPromoBookingList.UpdateLayout();
                        DCdataGridPromoBookingList.ScrollIntoView(DCdataGridPromoBookingList.SelectedItem, DCdataGridPromoBookingList.Columns[0]);


                        MessageBox.Show("刪除完成");
                        return;
                    }
                }
            };
        }



        

    }
}

