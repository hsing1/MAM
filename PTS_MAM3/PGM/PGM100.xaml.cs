﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Text;
using System.Xml.Linq;
using System.IO;
using PTS_MAM3.WSPROPOSAL;
using System.Windows.Browser;
using PTS_MAM3.PGM;

namespace PTS_MAM3
{
    public partial class PgmQueue : Page
    {
        //Boolean NeedWrite = false;

        public PgmQueue()
        {
            InitializeComponent();
            

            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBZCHANNEL_ALL = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";
            ModuleClass.getModulePermission("");

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            sb.AppendLine("</Data>");
            SP_Q_TBZCHANNEL_ALL.Do_QueryAsync("[SP_Q_TBZCHANNEL_ALL]", sb.ToString(), ReturnXML);
            //SP_Q_TBPGM_QUEUE.Do_QueryCompleted += new EventHandler<SendSQL.Do_QueryCompletedEventArgs>(SP_Q_TBPGM_QUEUE_Do_QueryCompleted);
            //AAA.Do_InsertAsync("", "", ReturnStr);
            SP_Q_TBZCHANNEL_ALL.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            if (ModuleClass.getModulePermission("P1" + elem.Element("ID").Value.ToString() + "001") == true || ModuleClass.getModulePermission("P1" + elem.Element("ID").Value.ToString() + "002") == true)
                            {
                                ComboBoxItem TempComboBoxItem = new ComboBoxItem();
                                TempComboBoxItem.Content = elem.Element("NAME").Value.ToString();
                                TempComboBoxItem.Tag = elem.Element("ID").Value.ToString() + "-" + elem.Element("TYPE").Value.ToString();
                                comboBoxChannel.Items.Add(TempComboBoxItem);                               
                            }
                            
                        }
                    }
                }
            };

            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBZSIGNAL_ALL = new WSPGMSendSQL.SendSQLSoapClient();

            //string ReturnStr = "";

            StringBuilder sb1 = new StringBuilder();
            object ReturnXML1 = "";
            sb1.AppendLine("<Data>");
            sb1.AppendLine("</Data>");
            SP_Q_TBZSIGNAL_ALL.Do_QueryAsync("[SP_Q_TBZSIGNAL_ALL]", sb1.ToString(), ReturnXML);
            SP_Q_TBZSIGNAL_ALL.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {

                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);

                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            ComboBoxItem TempComboBoxItem = new ComboBoxItem();
                            TempComboBoxItem.Content = elem.Element("FSSIGNALNAME").Value.ToString();

                            comboBoxSignal1.Items.Add(TempComboBoxItem);
                           

                            //TempSignalList.Add(elem.Element("FSSIGNALNAME").Value.ToString());
                        }
                        //SignalList = TempSignalList;
                    }
                    else
                    {
                    }
                }

            };

        }



        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void ComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i <= comboBoxSignal1.Items.Count - 1; i++)
            {
                string TestI = ((ComboBoxItem)comboBoxSignal1.Items[i]).Content.ToString();
                (sender as ComboBox).Items.Add(TestI);
            }
            (sender as ComboBox).IsDropDownOpen = true;
        }
        List<PgmQueueDate> PgmQueueSource = new List<PgmQueueDate>();
        bool _NeedSave = false;
        private void button1_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                //RR.Header="Progid";
                //radGridView1.Columns.Add(RR);
                //RR.Header="ProgNAME";
                //radGridView1.Columns.Add(RR);
                if (comboBoxChannel.SelectedIndex == -1)
                {
                    MessageBox.Show("請先選擇要查詢之頻道");
                    return;
                }
                if (ModuleClass.getModulePermission("P1" + ((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString().Split(new Char[] { '-' })[0] + "002") == false)
                {
                    MessageBox.Show("您沒有查詢此頻道節目表的權限");
                    return;
                }

                BusyMsg.IsBusy = true; 

                WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_QUEUE = new WSPGMSendSQL.SendSQLSoapClient();
                //string ReturnStr = "";

                StringBuilder sb = new StringBuilder();
                object ReturnXML = "";
                sb.AppendLine("<Data>");
                //sb.AppendLine("<FDDATE>" + this.textBoxDate.Text + "</FDDATE>");
                if (comboBoxChannel.SelectedIndex != -1)
                {
                    string[] ChannelType = ((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString().Split(new Char[] { '-' });
                    //sb.AppendLine("<FSCHANNEL_ID>" + ChannelType[0] + "</FSCHANNEL_ID>");
                    _ChannelType = ChannelType[1];
                    _ChannelID = ChannelType[0];
                    _Date = this.textBoxDate.Text;
                    _ChannelName = ((ComboBoxItem)comboBoxChannel.SelectedItem).Content.ToString();
                }
                //_Date="2010/12/16";
                //_ChannelID="06";
                sb.AppendLine("<FDDATE>" + _Date + "</FDDATE>");
                sb.AppendLine("<FSCHANNEL_ID>" + _ChannelID + "</FSCHANNEL_ID>");
                sb.AppendLine("</Data>");
                SP_Q_TBPGM_QUEUE.Do_QueryAsync("SP_Q_TBPGM_QUEUE", sb.ToString(), ReturnXML);
                //SP_Q_TBPGM_QUEUE.Do_QueryCompleted += new EventHandler<SendSQL.Do_QueryCompletedEventArgs>(SP_Q_TBPGM_QUEUE_Do_QueryCompleted);
                //AAA.Do_InsertAsync("", "", ReturnStr);
                SP_Q_TBPGM_QUEUE.Do_QueryCompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (args.Result != null && args.Result != "")
                        {

                            byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                            PgmQueueSource.Clear();
                            StringBuilder output = new StringBuilder();
                            // Create an XmlReader
                            XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                            var ltox = from str in doc.Elements("Datas").Elements("Data")
                                       select str;
                            foreach (XElement elem in ltox)
                            {

                                string RealBTime;
                                string RealETime;
                                if (int.Parse(elem.Element("FSBEG_TIME").Value.ToString())>=2400)
                                    RealBTime= (int.Parse(elem.Element("FSBEG_TIME").Value.ToString())-2400).ToString("0000");
                                else
                                    RealBTime=elem.Element("FSBEG_TIME").Value.ToString();

                                if (int.Parse(elem.Element("FSEND_TIME").Value.ToString()) >= 2400)
                                    RealETime = (int.Parse(elem.Element("FSEND_TIME").Value.ToString()) - 2400).ToString("0000");
                                else
                                    RealETime = elem.Element("FSEND_TIME").Value.ToString();

                                string SDEF_MIN;
                                string SDEF_SEC;

                                string SDEF_SEG;
                                if (elem.Element("FNDEF_MIN").Value.ToString() == "")
                                    SDEF_MIN = "0";
                                else
                                    SDEF_MIN = elem.Element("FNDEF_MIN").Value.ToString();

                                if (elem.Element("FNDEF_SEC").Value.ToString() == "")
                                    SDEF_SEC = "0";
                                else
                                    SDEF_SEC = elem.Element("FNDEF_SEC").Value.ToString();
                                if (elem.Element("FNDEF_SEG").Value.ToString() == "")
                                    SDEF_SEG = elem.Element("FNSEG").Value.ToString();
                                else
                                    SDEF_SEG = elem.Element("FNDEF_SEG").Value.ToString(); 
                                
                                PgmQueueSource.Add(new PgmQueueDate()
                                {
                                    ProgID = elem.Element("FSPROG_ID").Value.ToString(),
                                    Seqno = elem.Element("FNSEQNO").Value.ToString(),
                                    ProgName = elem.Element("FSPROG_NAME").Value.ToString(),

                                    ProgDate = (Convert.ToDateTime(elem.Element("FDDATE").Value)).ToString("yyyy/MM/dd"),
                                    //ProgDate = elem.Element("FDDATE").Value.ToString(),
                                    CHANNEL_ID = elem.Element("FSCHANNEL_ID").Value.ToString(),
                                    BEG_TIME = elem.Element("FSBEG_TIME").Value.ToString(),
                                    END_TIME = elem.Element("FSEND_TIME").Value.ToString(),
                                    REPLAY = elem.Element("FNREPLAY").Value.ToString(),
                                    EPISODE = elem.Element("FNEPISODE").Value.ToString(),
                                    TAPENO = elem.Element("FSTAPENO").Value.ToString(),
                                    PROG_NAME = elem.Element("FSPROG_NAME").Value.ToString(),
                                    LIVE = elem.Element("FNLIVE").Value.ToString(),
                                    SIGNAL = elem.Element("FSSIGNAL").Value.ToString(),
                                    DEP = elem.Element("FNDEP").Value.ToString(),
                                    ONOUT = elem.Element("FNONOUT").Value.ToString(),
                                    MEMO = elem.Element("FSMEMO").Value.ToString(),
                                    MEMO1 = elem.Element("FSMEMO1").Value.ToString(),
                                    CHANNEL_NAME = elem.Element("FSCHANNEL_NAME").Value.ToString(),
                                    FILETYPE = "",
                                    ProgDefSeg = SDEF_SEG,
                                    RealBEG_TIME = RealBTime,
                                    RealEND_TIME = RealETime,
                                    DEF_MIN = SDEF_MIN,
                                    DEF_SEC = SDEF_SEC,

                                });
                            }
                            dataGridQueue.ItemsSource = null;
                            dataGridQueue.ItemsSource = PgmQueueSource;
                            BusyMsg.IsBusy = false;
                            _NeedSave = false;
                            MessageBox.Show("查詢完畢");
                        } //if (args.Result != null && args.Result !="")
                        else
                        {
                            BusyMsg.IsBusy = false; 
                            MessageBox.Show("節目表不存在");
                        }
                    }
                };
            }
            catch
            {
                BusyMsg.IsBusy = false; 
                MessageBox.Show("查詢節目表失敗!");
            }
        }

        void dataGridQueue_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void calendar1_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
        {
            
            //this.textBoxDate.Text = this.calendar1.SelectedDate.ToString();
            DateTime Dt1 = calendar1.SelectedDate.Value;
            this.textBoxDate.Text = Dt1.ToString("yyyy/MM/dd");
            calendar1.Visibility = System.Windows.Visibility.Collapsed;
        }

            

        private void buttonGetDate_Click(object sender, RoutedEventArgs e)
        {
            if (calendar1.Visibility == System.Windows.Visibility.Visible)
                calendar1.Visibility = System.Windows.Visibility.Collapsed;
            else
                calendar1.Visibility = System.Windows.Visibility.Visible;

        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            dataGridQueue.LoadingRow += (s1, args1) =>
            {
                PgmQueueDate PGMQueue = args1.Row.DataContext as PgmQueueDate;
                args1.Row.Background = new SolidColorBrush(Colors.White);
                args1.Row.Foreground = new SolidColorBrush(Colors.Black);

                int i = args1.Row.GetIndex() - 1;
                if (args1.Row.GetIndex() != 0) //之前找不到節目或Promo的資料
                {
                    if (int.Parse(PgmQueueSource[i].END_TIME) < int.Parse(PgmQueueSource[args1.Row.GetIndex()].BEG_TIME))
                    {
                        args1.Row.Background = new SolidColorBrush(Colors.Yellow);
                    }
                    if (int.Parse(PgmQueueSource[i].END_TIME) > int.Parse(PgmQueueSource[args1.Row.GetIndex()].BEG_TIME))
                    {
                        args1.Row.Foreground = new SolidColorBrush(Colors.Red);
                    }
                }
                i = args1.Row.GetIndex() + 1;
                if (args1.Row.GetIndex() != PgmQueueSource.Count - 1) //之後找不到節目或Promo的資料
                {
                    if (int.Parse(PgmQueueSource[i].BEG_TIME) < int.Parse(PgmQueueSource[args1.Row.GetIndex()].END_TIME))
                    {
                        args1.Row.Foreground = new SolidColorBrush(Colors.Red);
                    }
                    if (int.Parse(PgmQueueSource[i].BEG_TIME) > int.Parse(PgmQueueSource[args1.Row.GetIndex()].END_TIME))
                    {
                        args1.Row.Background = new SolidColorBrush(Colors.Yellow);
                    }
                }
            };
  
        }

        private void buttonUpdate_Click(object sender, RoutedEventArgs e)
        {

            if (dataGridQueue.SelectedIndex < 0)
            {
                MessageBox.Show("請先選擇一筆節目資料");
                return;
            }
            if (ModuleClass.getModulePermission("P1" + ((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString().Split(new Char[] { '-' })[0] + "002") == false)
            {
                MessageBox.Show("您沒有修改此頻道節目表的權限");
                return;
            }
            PgmQueueModify PGMQUEUEMODIFY_Frm = new PgmQueueModify();
            PGMQUEUEMODIFY_Frm.textBoxProgID.Text = PgmQueueSource[dataGridQueue.SelectedIndex].ProgID;
            PGMQUEUEMODIFY_Frm.textBoxProgSeqno.Text = PgmQueueSource[dataGridQueue.SelectedIndex].Seqno;
            PGMQUEUEMODIFY_Frm.textBoxProgName.Text = PgmQueueSource[dataGridQueue.SelectedIndex].ProgName;
            PGMQUEUEMODIFY_Frm.textBoxDate.Text = PgmQueueSource[dataGridQueue.SelectedIndex].ProgDate;
            PGMQUEUEMODIFY_Frm.textBoxChannelID.Text = PgmQueueSource[dataGridQueue.SelectedIndex].CHANNEL_ID;
            PGMQUEUEMODIFY_Frm.textBoxChannelName.Text = PgmQueueSource[dataGridQueue.SelectedIndex].CHANNEL_NAME;
            PGMQUEUEMODIFY_Frm.textBoxBTime.Text = PgmQueueSource[dataGridQueue.SelectedIndex].BEG_TIME;
            PGMQUEUEMODIFY_Frm.textBoxETime.Text = PgmQueueSource[dataGridQueue.SelectedIndex].END_TIME;
            PGMQUEUEMODIFY_Frm.TextBoxReplay.Text = PgmQueueSource[dataGridQueue.SelectedIndex].REPLAY;
            PGMQUEUEMODIFY_Frm.textBoxEpisode.Text = PgmQueueSource[dataGridQueue.SelectedIndex].EPISODE;

            PGMQUEUEMODIFY_Frm.comboBoxLive.SelectedIndex = -1;
            for (int Combocount = 0; Combocount < PGMQUEUEMODIFY_Frm.comboBoxLive.Items.Count; Combocount++)
            {
                if (((ComboBoxItem)PGMQUEUEMODIFY_Frm.comboBoxLive.Items[Combocount]).Tag.ToString() == PgmQueueSource[dataGridQueue.SelectedIndex].LIVE)
                {
                    PGMQUEUEMODIFY_Frm.comboBoxLive.SelectedIndex = Combocount;
                }
            }

            PGMQUEUEMODIFY_Frm._InSIgnal = PgmQueueSource[dataGridQueue.SelectedIndex].SIGNAL;
            PGMQUEUEMODIFY_Frm.comboBoxSignal1.SelectedIndex = -1;
            //for (int Combocount = 0; Combocount < PGMQUEUEMODIFY_Frm.comboBoxSignal1.Items.Count; Combocount++)
            //{
            //    if (((ComboBoxItem)PGMQUEUEMODIFY_Frm.comboBoxSignal1.Items[Combocount]).Tag.ToString() == PgmQueueSource[dataGridQueue.SelectedIndex].SIGNAL)
            //    {
            //        PGMQUEUEMODIFY_Frm.comboBoxSignal1.SelectedIndex = Combocount;
            //    }
            //}

            //PGMQUEUEMODIFY_Frm.textBoxLive.Text = PgmQueueSource[dataGridQueue.SelectedIndex].LIVE;
            PGMQUEUEMODIFY_Frm.textBoxDep.Text = PgmQueueSource[dataGridQueue.SelectedIndex].DEP;
            PGMQUEUEMODIFY_Frm.textBoxONOUT.Text = PgmQueueSource[dataGridQueue.SelectedIndex].ONOUT;
            PGMQUEUEMODIFY_Frm.textBoxMemo.Text = PgmQueueSource[dataGridQueue.SelectedIndex].MEMO;
            PGMQUEUEMODIFY_Frm.textBoxMemo1.Text = PgmQueueSource[dataGridQueue.SelectedIndex].MEMO1;
            PGMQUEUEMODIFY_Frm.textBoxDefSeg.Text = PgmQueueSource[dataGridQueue.SelectedIndex].ProgDefSeg;
            PGMQUEUEMODIFY_Frm.textBoxDefMin.Text = PgmQueueSource[dataGridQueue.SelectedIndex].DEF_MIN;
            PGMQUEUEMODIFY_Frm.textBoxDefSec.Text = PgmQueueSource[dataGridQueue.SelectedIndex].DEF_SEC;
            PGMQUEUEMODIFY_Frm.WorkChannelID = _ChannelID;
            
            PGMQUEUEMODIFY_Frm.Show();
            PGMQUEUEMODIFY_Frm.Closed += (s, args) =>
            {
                if (PGMQUEUEMODIFY_Frm.ModifyQueue.ProgID != null)
                {

                    string RealBTime;
                    string RealETime;
                    if (int.Parse(PGMQUEUEMODIFY_Frm.ModifyQueue.BEG_TIME) >= 2400)
                        RealBTime = (int.Parse(PGMQUEUEMODIFY_Frm.ModifyQueue.BEG_TIME) - 2400).ToString("0000");
                    else
                        RealBTime = PGMQUEUEMODIFY_Frm.ModifyQueue.BEG_TIME;

                    if (int.Parse(PGMQUEUEMODIFY_Frm.ModifyQueue.END_TIME) >= 2400)
                        RealETime = (int.Parse(PGMQUEUEMODIFY_Frm.ModifyQueue.END_TIME) - 2400).ToString("0000");
                    else
                        RealETime = PGMQUEUEMODIFY_Frm.ModifyQueue.END_TIME; 
                                
                    PgmQueueSource[dataGridQueue.SelectedIndex].ProgID = PGMQUEUEMODIFY_Frm.ModifyQueue.ProgID;
                    PgmQueueSource[dataGridQueue.SelectedIndex].Seqno = PGMQUEUEMODIFY_Frm.ModifyQueue.Seqno;
                    PgmQueueSource[dataGridQueue.SelectedIndex].ProgName = PGMQUEUEMODIFY_Frm.ModifyQueue.ProgName;
                    PgmQueueSource[dataGridQueue.SelectedIndex].ProgDate = PGMQUEUEMODIFY_Frm.ModifyQueue.ProgDate;
                    PgmQueueSource[dataGridQueue.SelectedIndex].CHANNEL_ID = PGMQUEUEMODIFY_Frm.ModifyQueue.CHANNEL_ID;
                    PgmQueueSource[dataGridQueue.SelectedIndex].CHANNEL_NAME = PGMQUEUEMODIFY_Frm.ModifyQueue.CHANNEL_NAME;
                    PgmQueueSource[dataGridQueue.SelectedIndex].BEG_TIME = PGMQUEUEMODIFY_Frm.ModifyQueue.BEG_TIME;
                    PgmQueueSource[dataGridQueue.SelectedIndex].END_TIME = PGMQUEUEMODIFY_Frm.ModifyQueue.END_TIME;
                    PgmQueueSource[dataGridQueue.SelectedIndex].REPLAY = PGMQUEUEMODIFY_Frm.ModifyQueue.REPLAY;
                    PgmQueueSource[dataGridQueue.SelectedIndex].EPISODE = PGMQUEUEMODIFY_Frm.ModifyQueue.EPISODE;
                    PgmQueueSource[dataGridQueue.SelectedIndex].LIVE = PGMQUEUEMODIFY_Frm.ModifyQueue.LIVE;
                    PgmQueueSource[dataGridQueue.SelectedIndex].SIGNAL = PGMQUEUEMODIFY_Frm.ModifyQueue.SIGNAL;
                    PgmQueueSource[dataGridQueue.SelectedIndex].DEP = PGMQUEUEMODIFY_Frm.ModifyQueue.DEP;
                    PgmQueueSource[dataGridQueue.SelectedIndex].ONOUT = PGMQUEUEMODIFY_Frm.ModifyQueue.ONOUT;
                    PgmQueueSource[dataGridQueue.SelectedIndex].MEMO = PGMQUEUEMODIFY_Frm.ModifyQueue.MEMO;
                    PgmQueueSource[dataGridQueue.SelectedIndex].MEMO1 = PGMQUEUEMODIFY_Frm.ModifyQueue.MEMO1;
                    PgmQueueSource[dataGridQueue.SelectedIndex].ProgDefSeg = PGMQUEUEMODIFY_Frm.textBoxDefSeg.Text;
                    PgmQueueSource[dataGridQueue.SelectedIndex].DEF_MIN = PGMQUEUEMODIFY_Frm.textBoxDefMin.Text;
                    PgmQueueSource[dataGridQueue.SelectedIndex].DEF_SEC = PGMQUEUEMODIFY_Frm.textBoxDefSec.Text;
                    PgmQueueSource[dataGridQueue.SelectedIndex].RealBEG_TIME = RealBTime;
                    PgmQueueSource[dataGridQueue.SelectedIndex].RealEND_TIME = RealETime;
                    _NeedSave = true;
                }
                dataGridQueue.ItemsSource = null;
                dataGridQueue.ItemsSource = PgmQueueSource;
            };
        }

        string _ChannelType;
        string _ChannelID;
        string _Date;
        string _ChannelName;

        private void buttonQueryTape_Click(object sender, RoutedEventArgs e)
        {
            if (_ChannelID=="")
            {
                MessageBox.Show("請先查詢節目表");
                return;
            }
            if (ModuleClass.getModulePermission("P1" + _ChannelID + "002") == false)
            {
                MessageBox.Show("您沒有查詢此頻道節目表檔案狀態的權限");
                return;
            }
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBLOG_VIDEO_BY_ID_EPISODE = new WSPGMSendSQL.SendSQLSoapClient();
            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Datas>");
            
            foreach (PgmQueueDate TempQueue in PgmQueueSource)
            {
                sb.AppendLine("<Data>");
                sb.AppendLine("<FSID>" + TempQueue.ProgID + "</FSID>");
                sb.AppendLine("<FNEPISODE>" + TempQueue.EPISODE + "</FNEPISODE>");
                sb.AppendLine("<FSARC_TYPE>" + _ChannelType + "</FSARC_TYPE>");
                sb.AppendLine("</Data>");            
            }
            sb.AppendLine("</Datas>");

            SP_Q_TBLOG_VIDEO_BY_ID_EPISODE.Do_Query_TBLOG_VIDEO_BY_ID_EPISODEAsync("[SP_Q_TBLOG_VIDEO_BY_ID_EPISODE_ARCTPYE]", sb.ToString(), ReturnXML);
            //SP_Q_TBPGM_QUEUE.Do_QueryCompleted += new EventHandler<SendSQL.Do_QueryCompletedEventArgs>(SP_Q_TBPGM_QUEUE_Do_QueryCompleted);
            //AAA.Do_InsertAsync("", "", ReturnStr);
            SP_Q_TBLOG_VIDEO_BY_ID_EPISODE.Do_Query_TBLOG_VIDEO_BY_ID_EPISODECompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {

                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            foreach (PgmQueueDate TempQueue in PgmQueueSource)
                            {
                                if ((elem.Element("FSID").Value.ToString() == TempQueue.ProgID) && (elem.Element("FNEPISODE").Value.ToString() == TempQueue.EPISODE) && (elem.Element("FSARC_TYPE").Value.ToString() == _ChannelType))
                                {
                                    if (TempQueue.LIVE != "1")
                                    {
                                        TempQueue.TAPENO = elem.Element("FSFILE_NO").Value.ToString();
                                        if (elem.Element("FCLOW_RES").Value.ToString() == "Y")
                                            TempQueue.FILETYPE = "已有資料";
                                        else if (elem.Element("FCLOW_RES").Value.ToString() == "N")
                                            TempQueue.FILETYPE = "尚未轉檔";
                                        else if (elem.Element("FCLOW_RES").Value.ToString() == "R")
                                            TempQueue.FILETYPE = "轉檔中";
                                        else if (elem.Element("FCLOW_RES").Value.ToString() == "")
                                            TempQueue.FILETYPE = "檔案尚未入庫";
                                        else if (elem.Element("FCLOW_RES").Value.ToString() == "F")
                                            TempQueue.FILETYPE = "轉檔失敗";
                                        else
                                            TempQueue.FILETYPE =elem.Element("FCLOW_RES").Value.ToString();
                                    }
                                    else
                                    {
                                        TempQueue.TAPENO = elem.Element("FSFILE_NO").Value.ToString();
                                        TempQueue.FILETYPE = "此為Live節目";
                                    }
                                }
                            }
                        }
                        dataGridQueue.ItemsSource = null;
                        dataGridQueue.ItemsSource = PgmQueueSource;
                        MessageBox.Show("更新檔案狀態完成");
                    }
                }
            };
        }

        private void buttonNoTapeList_Click(object sender, RoutedEventArgs e)
        {
            if (PgmQueueSource.Count <= 0)
            {
                MessageBox.Show("請先查詢節目表");
                return;
            }
            if (_ChannelID == "")
            {
                MessageBox.Show("請先查詢節目表");
                return;
            }

            if (ModuleClass.getModulePermission("P1" + _ChannelID + "002") == false)
            {
                MessageBox.Show("您沒有列印此頻道未到帶清單的權限");
                return;
            }
            int NO_TAPE_COUNT=0;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<Datas>");
            Guid g;
            g = Guid.NewGuid();

            WSPGMSendSQL.SendSQLSoapClient SP_I_RPT_TBPGM_NO_TAPE_LIST_01 = new WSPGMSendSQL.SendSQLSoapClient();

            foreach (PgmQueueDate TempQueue in PgmQueueSource)
            {
                if (TempQueue.FILETYPE == "")
                {
                    MessageBox.Show("請先取得檔案狀態");
                    return;
                }
                if ((TempQueue.LIVE != "1") && (TempQueue.FILETYPE != "已有資料"))
                {
                    NO_TAPE_COUNT = NO_TAPE_COUNT + 1;
                    Guid g1;
                    g1 = Guid.NewGuid();
                
                    sb.AppendLine("<Data>");
                    sb.AppendLine("<QUERY_KEY>" + g.ToString() + "</QUERY_KEY>");
                    sb.AppendLine("<QUERY_KEY_SUBID>" + g1.ToString() + "</QUERY_KEY_SUBID>");
                    sb.AppendLine("<QUERY_BY>" + UserClass.userData.FSUSER_ChtName.ToString() + "</QUERY_BY>");
                    //sb.AppendLine("<QUERY_DATE>" + TempQueue.ProgID + "</QUERY_DATE>");
                    sb.AppendLine("<播出日期>" + TempQueue.ProgDate + "</播出日期>");
                    sb.AppendLine("<頻道>" + TempQueue.CHANNEL_NAME + "</頻道>");
                    sb.AppendLine("<節目名稱>" + TransferTimecode.ReplaceXML(TempQueue.PROG_NAME) + "</節目名稱>");
                    sb.AppendLine("<集數>" + TempQueue.EPISODE + "</集數>");
                    sb.AppendLine("<檔案編號>" + TempQueue.TAPENO + "</檔案編號>");
                    sb.AppendLine("<起始時間>" + TempQueue.BEG_TIME + "</起始時間>");
                    sb.AppendLine("<結束時間>" + TempQueue.END_TIME + "</結束時間>");
                    sb.AppendLine("<檔案狀態>" + TempQueue.FILETYPE + "</檔案狀態>");
                    

                    sb.AppendLine("</Data>");
                }
            }
            sb.AppendLine("</Datas>");

            if (NO_TAPE_COUNT == 0)
            {
                MessageBox.Show("沒有未到帶清單");
                return;
            }
            SP_I_RPT_TBPGM_NO_TAPE_LIST_01.Do_Insert_ListAsync("", "", "SP_I_RPT_TBPGM_QUEUE_NO_TAPE_LIST_01", sb.ToString());
            SP_I_RPT_TBPGM_NO_TAPE_LIST_01.Do_Insert_ListCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result == true)
                    {
                        WSPGMSendSQL.SendSQLSoapClient GETURL = new WSPGMSendSQL.SendSQLSoapClient();
                        GETURL.GETReportURLAsync("RPT_TBPGM_QUEUE_NO_TAPE_LIST_01", g.ToString());
                        GETURL.GETReportURLCompleted += (s1, args1) =>
                        {
                            if (args1.Error == null)
                            {
                                if (args1.Error == null)
                                {
                                    if (args1.Result != null && args1.Result != "")
                                    {
                                        if (args1.Result.ToString().StartsWith("錯誤"))
                                            MessageBox.Show(args1.Result.ToString(), "提示訊息", MessageBoxButton.OK);
                                        else
                                            HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + args1.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,toolbar=0,statusbar=0,menubar=0,scrollbar=0,resizable=yes");
                                    }
                                    else
                                        MessageBox.Show("查無相關資料，或該筆資料已不存在資料庫中。", "提示訊息", MessageBoxButton.OK);
                                }             
                            }
                        };
                    }
                    else
                        MessageBox.Show("新增失敗");
                }
            };       
        }

        private void buttonSave_Click(object sender, RoutedEventArgs e)
        {
            if (PgmQueueSource.Count <=0)
            {
                MessageBox.Show("請先查詢節目表");
                return;
            }
            if (_ChannelID == "")
            {
                MessageBox.Show("請先查詢節目表");
                return;
            }

            if (ModuleClass.getModulePermission("P1" + _ChannelID + "002") == false)
            {
                MessageBox.Show("您沒有儲存此頻道節目表的權限");
                return;
            }
            WSPGMSendSQL.SendSQLSoapClient SP_I_TBPGM_QUEUE = new WSPGMSendSQL.SendSQLSoapClient();
            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Datas>");
            foreach (PgmQueueDate TempQueue in PgmQueueSource)
            {
                    sb.AppendLine("<Data>");
                    sb.AppendLine("<FSPROG_ID>" + TempQueue.ProgID + "</FSPROG_ID>");
                    sb.AppendLine("<FNSEQNO>" + TempQueue.Seqno + "</FNSEQNO>");
                    sb.AppendLine("<FDDATE>" + TempQueue.ProgDate + "</FDDATE>");
                    sb.AppendLine("<FSCHANNEL_ID>" + TempQueue.CHANNEL_ID + "</FSCHANNEL_ID>");
                    sb.AppendLine("<FSBEG_TIME>" + TempQueue.BEG_TIME + "</FSBEG_TIME>");
                    sb.AppendLine("<FSEND_TIME>" + TempQueue.END_TIME + "</FSEND_TIME>");
                    sb.AppendLine("<FNREPLAY>" + TempQueue.REPLAY + "</FNREPLAY>");
                    sb.AppendLine("<FNEPISODE>" + TempQueue.EPISODE + "</FNEPISODE>");
                    sb.AppendLine("<FSTAPENO>" + TempQueue.TAPENO + "</FSTAPENO>");
                    //sb.AppendLine("<FSTAPENO>" + "11" + "</FSTAPENO>");
                    sb.AppendLine("<FSPROG_NAME>" + TransferTimecode.ReplaceXML(TempQueue.ProgName) + "</FSPROG_NAME>");
                    sb.AppendLine("<FNLIVE>" + TempQueue.LIVE + "</FNLIVE>");
                    sb.AppendLine("<FSSIGNAL>" + TempQueue.SIGNAL + "</FSSIGNAL>");
                    sb.AppendLine("<FNDEP>" + TempQueue.DEP + "</FNDEP>");
                    sb.AppendLine("<FNONOUT>" + TempQueue.ONOUT + "</FNONOUT>");
                    sb.AppendLine("<FSMEMO>" +  TransferTimecode.ReplaceXML(TempQueue.MEMO) + "</FSMEMO>");
                    sb.AppendLine("<FSMEMO1>" +  TransferTimecode.ReplaceXML(TempQueue.MEMO1) + "</FSMEMO1>");
                    //sb.AppendLine("<FSMEMO>" + "+LOGO" + "</FSMEMO>");
                    sb.AppendLine("<FSCREATED_BY>" + UserClass.userData.FSUSER_ID + "</FSCREATED_BY>");
                    sb.AppendLine("<FNDEF_MIN>" + TempQueue.DEF_MIN + "</FNDEF_MIN>");
                    sb.AppendLine("<FNDEF_SEC>" + TempQueue.DEF_SEC + "</FNDEF_SEC>");
                    sb.AppendLine("<FNDEF_SEG>" + TempQueue.ProgDefSeg + "</FNDEF_SEG>");
                    sb.AppendLine("</Data>");


                    //sb.AppendLine("<FSPROG_ID>2010001</FSPROG_ID>");
                    //sb.AppendLine("<FNSEQNO>1</FNSEQNO>");
                    //sb.AppendLine("<FDDATE>2011/01/02</FDDATE>");
                    //sb.AppendLine("<FSCHANNEL_ID>06</FSCHANNEL_ID>");
                    //sb.AppendLine("<FSBEG_TIME>0600</FSBEG_TIME>");
                    //sb.AppendLine("<FSEND_TIME>0700</FSEND_TIME>");
                    //sb.AppendLine("<FNREPLAY>0</FNREPLAY>");
                    //sb.AppendLine("<FNEPISODE>1</FNEPISODE>");
                    //sb.AppendLine("<FSTAPENO>001100</FSTAPENO>");
                    //sb.AppendLine("<FSPROG_NAME>播溪小園丁</FSPROG_NAME>");
                    //sb.AppendLine("<FNLIVE>1</FNLIVE>");
                    //sb.AppendLine("<FNDEP>1</FNDEP>");
                    //sb.AppendLine("<FNONOUT>1</FNONOUT>");
                    //sb.AppendLine("<FSMEMO>24片名</FSMEMO>");
                    //sb.AppendLine("<FSCREATED_BY>Mike</FSCREATED_BY>");
            }
            sb.AppendLine("</Datas>");
            //sb=sb.Replace("\r\n", "");
            SP_I_TBPGM_QUEUE.Do_Insert_TBPGMQUEUEAsync("SP_I_TBPGM_QUEUE",_Date,_ChannelID, sb.ToString());
            SP_I_TBPGM_QUEUE.Do_Insert_TBPGMQUEUECompleted += (s, args) =>
            {
                if (args.Error == null)
                {

                    if (args.Result == true)
                    {
                        MessageBox.Show("新增成功");
                        _NeedSave = false;
                    }
                    else
                        MessageBox.Show("新增失敗");
                }
            };           
        }

        //救命帶清單
        private void buttonSaveLiftTape_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridQueue.SelectedIndex < 0)
            {
                MessageBox.Show("請先選擇一筆節目資料");
                return;
            }
            if (_ChannelID == "")
            {
                MessageBox.Show("請先查詢節目表");
                return;
            }
            
            if (ModuleClass.getModulePermission("P1" + _ChannelID + "002") == false)
            {
                MessageBox.Show("您沒有維護此頻道救命帶清單的權限");
                return;
            }
            PGM100_03 PGM100_03_Frm = new PGM100_03();
           
            PGM100_03_Frm.textBoxProgID.Text = PgmQueueSource[dataGridQueue.SelectedIndex].ProgID;
            PGM100_03_Frm.textBoxProgSeqno.Text = PgmQueueSource[dataGridQueue.SelectedIndex].Seqno;
            PGM100_03_Frm.textBoxProgName.Text = PgmQueueSource[dataGridQueue.SelectedIndex].ProgName;
            PGM100_03_Frm.textBoxDate.Text = PgmQueueSource[dataGridQueue.SelectedIndex].ProgDate;
            PGM100_03_Frm.textBoxChannelID.Text = PgmQueueSource[dataGridQueue.SelectedIndex].CHANNEL_ID;
            PGM100_03_Frm.textBoxChannelName.Text = PgmQueueSource[dataGridQueue.SelectedIndex].CHANNEL_NAME;
            PGM100_03_Frm.textBoxBTime.Text = PgmQueueSource[dataGridQueue.SelectedIndex].BEG_TIME;
            PGM100_03_Frm.textBoxETime.Text = PgmQueueSource[dataGridQueue.SelectedIndex].END_TIME;
            PGM100_03_Frm.textBoxEpisode.Text = PgmQueueSource[dataGridQueue.SelectedIndex].EPISODE;
            PGM100_03_Frm.textBoxMemo.Text = PgmQueueSource[dataGridQueue.SelectedIndex].MEMO;
            PGM100_03_Frm._ChannelFileType = _ChannelType;
            PGM100_03_Frm.QueryData();
            PGM100_03_Frm.Show();
            PGM100_03_Frm.Closed += (s, args) =>
            {
            };
        }

        private void buttonInsertTape_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridQueue.SelectedIndex < 0)
            {
                MessageBox.Show("請先選擇一筆節目資料");
                return;
            }
            if (_ChannelID == "")
            {
                MessageBox.Show("請先查詢節目表");
                return;
            }

            if (ModuleClass.getModulePermission("P1" + _ChannelID + "002") == false)
            {
                MessageBox.Show("您沒有維護此頻道播出提示的權限");
                return;
            }
            PTS_MAM3.PGM.PGM100_05 PGM100_05_Frm = new PTS_MAM3.PGM.PGM100_05();

            PGM100_05_Frm.textBoxProgID.Text = PgmQueueSource[dataGridQueue.SelectedIndex].ProgID;
            PGM100_05_Frm._ARC_TYPE = _ChannelType;
            PGM100_05_Frm.textBoxProgName.Text = PgmQueueSource[dataGridQueue.SelectedIndex].ProgName;
            //PGM100_05_Frm.textBoxChannelName.Text = PgmQueueSource[dataGridQueue.SelectedIndex].CHANNEL_NAME;
            PGM100_05_Frm.textBoxEpisode.Text = PgmQueueSource[dataGridQueue.SelectedIndex].EPISODE;
            //PGM100_05_Frm.QueryData();
            PGM100_05_Frm.Show();
            PGM100_05_Frm.Closed += (s, args) =>
            {
            };
        }

        private void buttonPrint_Click(object sender, RoutedEventArgs e)
        {
            if (PgmQueueSource.Count <= 0)
            {
                MessageBox.Show("請先查詢節目表");
                return;
            }
            if (_ChannelID == "")
            {
                MessageBox.Show("請先查詢節目表");
                return;
            }

            if (ModuleClass.getModulePermission("P1" + _ChannelID + "002") == false)
            {
                MessageBox.Show("您沒有列印此頻道節目表的權限");
                return;
            }
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<Datas>");
            Guid g;
            g = Guid.NewGuid();

            WSPGMSendSQL.SendSQLSoapClient SP_I_RPT_TBPGM_QUEUE_01 = new WSPGMSendSQL.SendSQLSoapClient();

            foreach (PgmQueueDate TempQueue in PgmQueueSource)
            {
                Guid g1;
                g1 = Guid.NewGuid();
                
                sb.AppendLine("<Data>");
                sb.AppendLine("<QUERY_KEY>" + g.ToString() + "</QUERY_KEY>");
                sb.AppendLine("<QUERY_KEY_SUBID>" + g1.ToString() + "</QUERY_KEY_SUBID>");
                sb.AppendLine("<QUERY_BY>" + UserClass.userData.FSUSER_ChtName.ToString() + "</QUERY_BY>");
                //sb.AppendLine("<QUERY_DATE>" + TempQueue.ProgID + "</QUERY_DATE>");
                sb.AppendLine("<播出日期>" + TempQueue.ProgDate + "</播出日期>");
                sb.AppendLine("<頻道>" + TempQueue.CHANNEL_NAME + "</頻道>");
                sb.AppendLine("<節目名稱>" +  TransferTimecode.ReplaceXML(TempQueue.PROG_NAME) + "</節目名稱>");
                sb.AppendLine("<集數>" + TempQueue.EPISODE + "</集數>");
                sb.AppendLine("<影像編號>" + TempQueue.TAPENO + "</影像編號>");
                sb.AppendLine("<起始時間>" + TempQueue.BEG_TIME + "</起始時間>");
                sb.AppendLine("<結束時間>" + TempQueue.END_TIME + "</結束時間>");
                if (TempQueue.LIVE=="0")
                    sb.AppendLine("<LIVE播出>" + "檔案" + "</LIVE播出>");
                else if  (TempQueue.LIVE=="1")
                    sb.AppendLine("<LIVE播出>" + "LIVE" + "</LIVE播出>");
                else
                    sb.AppendLine("<LIVE播出>" + "檔案" + "</LIVE播出>");

                if (TempQueue.ONOUT=="0")
                    sb.AppendLine("<上檔下檔>" + "上檔" + "</上檔下檔>");
                else if  (TempQueue.ONOUT=="1")
                        sb.AppendLine("<上檔下檔>" + "一般" + "</上檔下檔>");
                else if  (TempQueue.ONOUT=="2")
                    sb.AppendLine("<上檔下檔>" + "下檔" + "</上檔下檔>");
                else 
                    sb.AppendLine("<上檔下檔>" + "一般" + "</上檔下檔>");

                sb.AppendLine("</Data>");
                
            }
            sb.AppendLine("</Datas>");

            SP_I_RPT_TBPGM_QUEUE_01.Do_Insert_ListAsync("", "", "SP_I_RPT_TBPGM_QUEUE_01", sb.ToString());
            SP_I_RPT_TBPGM_QUEUE_01.Do_Insert_ListCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result == true)
                    {
                        WSPGMSendSQL.SendSQLSoapClient GETURL = new WSPGMSendSQL.SendSQLSoapClient();
                        GETURL.GETReportURLAsync("RPT_TBPGM_QUEUE_01", g.ToString());
                        GETURL.GETReportURLCompleted += (s1, args1) =>
                        {
                            if (args1.Error == null)
                            {
                                if (args1.Result != null && args1.Result != "")
                                {
                                    if (args1.Result.ToString().StartsWith("錯誤"))
                                        MessageBox.Show(args1.Result.ToString(), "提示訊息", MessageBoxButton.OK);
                                    else
                                        HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + args1.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,toolbar=0,statusbar=0,menubar=0,scrollbar=0,resizable=yes");
                                }
                                else
                                    MessageBox.Show("查無相關資料，或該筆資料已不存在資料庫中。", "提示訊息", MessageBoxButton.OK);
                            }             
                        };
                    }
                    else
                        MessageBox.Show("列印失敗");
                }
            };       

        }

        private void buttonPrintSaveTapeList_Click(object sender, RoutedEventArgs e)
        {
            if (_ChannelID == "")
            {
                MessageBox.Show("請先查詢節目表");
                return;
            }

            if (ModuleClass.getModulePermission("P1" + _ChannelID + "002") == false)
            {
                MessageBox.Show("您沒有列印此頻道救命帶清單的權限");
                return;
            }
            WSPGMSendSQL.SendSQLSoapClient SP_PrintSaveTapeList = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";

            StringBuilder sb = new StringBuilder();

            SP_PrintSaveTapeList.PrintSaveTapeListAsync(_Date, _ChannelID, UserClass.userData.FSUSER_ChtName);
            //SP_Q_TBPGM_QUEUE.Do_QueryCompleted += new EventHandler<SendSQL.Do_QueryCompletedEventArgs>(SP_Q_TBPGM_QUEUE_Do_QueryCompleted);
            //AAA.Do_InsertAsync("", "", ReturnStr);
            SP_PrintSaveTapeList.PrintSaveTapeListCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {
                        if (args.Result.ToString().StartsWith("錯誤"))
                            MessageBox.Show(args.Result.ToString(), "提示訊息", MessageBoxButton.OK);
                        else
                            HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + args.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,toolbar=0,statusbar=0,menubar=0,scrollbar=0,resizable=yes");
                    }
                    else
                        MessageBox.Show("查無相關資料，或該筆資料已不存在資料庫中。", "提示訊息", MessageBoxButton.OK);
                }             

 
            };           
        }

        private void buttonImport_Click(object sender, RoutedEventArgs e)
        {
            //RR.Header="Progid";
            //radGridView1.Columns.Add(RR);
            //RR.Header="ProgNAME";
            //radGridView1.Columns.Add(RR);
            if (comboBoxChannel.SelectedIndex == -1)
            {
                MessageBox.Show("請先選擇要匯入的頻道");
                return;
            }

            if (ModuleClass.getModulePermission("P1" + ((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString().Split(new Char[] { '-' })[0] + "001") == false)
            {
                MessageBox.Show("您沒有匯入此頻道節目表的權限");
                return;
            }

            if (textBoxDate.Text == "")
            {
                MessageBox.Show("請先選擇要匯入的日期");
                return;
            }

            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_QUEUE = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            //sb.AppendLine("<FDDATE>" + this.textBoxDate.Text + "</FDDATE>");
            if (comboBoxChannel.SelectedIndex != -1)
            {
                string[] ChannelType = ((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString().Split(new Char[] { '-' });
                //sb.AppendLine("<FSCHANNEL_ID>" + ChannelType[0] + "</FSCHANNEL_ID>");
                _ChannelType = ChannelType[1];
                _ChannelID = ChannelType[0];
                _Date = this.textBoxDate.Text;
            }

            TransProgList.WebService1SoapClient ImportPlayList = new TransProgList.WebService1SoapClient();
            //if (AAA.TransProglistAsync("N", "2010/12/16") = "ok")

            ImportPlayList.TransProglistAsync(_ChannelID, _Date);

            ImportPlayList.TransProglistCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {
                        if (args.Result == "ok")
                            MessageBox.Show("匯入節目表成功");
                        else
                            MessageBox.Show("匯入節目表發生錯誤,原因為:" + args.Result);
                    }
                }
            };
        }

        private void buttonDeleteProg_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridQueue.SelectedIndex < 0)
            {
                MessageBox.Show("請先選擇一筆節目資料");
                return;
            }
            if (_ChannelID == "")
            {
                MessageBox.Show("請先查詢節目表");
                return;
            }

            if (ModuleClass.getModulePermission("P1" + _ChannelID + "002") == false)
            {
                MessageBox.Show("您沒有刪除此頻道節目的權限");
                return;
            }
            PgmQueueSource.RemoveAt(dataGridQueue.SelectedIndex);
            dataGridQueue.ItemsSource = null;
            dataGridQueue.ItemsSource = PgmQueueSource;
            _NeedSave = true;
            MessageBox.Show("刪除完成");
        }

        private void buttonInsertProg_Click(object sender, RoutedEventArgs e)
        {
            if (PgmQueueSource.Count <= 0)
            {
                MessageBox.Show("請先查詢資料");
                return;
            }
            if (_ChannelID == "")
            {
                MessageBox.Show("請先查詢節目表");
                return;
            }

            if (ModuleClass.getModulePermission("P1" + _ChannelID + "002") == false)
            {
                MessageBox.Show("您沒有於此頻道插入節目的權限");
                return;
            }
            PGM100_07 PGMQUEUEINSERT_Frm = new PGM100_07();
            PGMQUEUEINSERT_Frm._date = _Date;
            PGMQUEUEINSERT_Frm._channelid = _ChannelID;
            PGMQUEUEINSERT_Frm._ChannelFileType = _ChannelType;
            PGMQUEUEINSERT_Frm._channelName = _ChannelName;
            PGMQUEUEINSERT_Frm._FromForm = "QUEUE";
            PGMQUEUEINSERT_Frm.Show();
            PGMQUEUEINSERT_Frm.Closed += (s, args) =>
            {
                if (PGMQUEUEINSERT_Frm._PgmQueueProg != null)
                {
                    if (PGMQUEUEINSERT_Frm._PgmQueueProg.ProgID != "" && PGMQUEUEINSERT_Frm._PgmQueueProg.ProgID != null)
                    {
                        foreach (PgmQueueDate TempQueue in PgmQueueSource)
                        {
                            if (TempQueue.ProgID == PGMQUEUEINSERT_Frm._PgmQueueProg.ProgID && TempQueue.Seqno== PGMQUEUEINSERT_Frm._PgmQueueProg.Seqno)
                            {
                                MessageBox.Show("節目表中已經有此節目,不可插入!");
                                return;
                            }
                           
                        }
                        int InsertIndex=-1;
                        int TempIndex=0;
                        foreach (PgmQueueDate TempQueue in PgmQueueSource)
                        {
                            if (int.Parse(TempQueue.BEG_TIME) > int.Parse(PGMQUEUEINSERT_Frm._PgmQueueProg.BEG_TIME) && InsertIndex==-1) 
                            {
                                InsertIndex = TempIndex;
                            }
                            TempIndex=TempIndex +1;
                        }
                        if (InsertIndex == -1)
                        {
                            InsertIndex = PgmQueueSource.Count();
                        }

                        //PgmQueueSource.Insert(dataGridQueue.SelectedIndex, PGMQUEUEINSERT_Frm._PgmQueueProg);
                        PgmQueueSource.Insert(InsertIndex, PGMQUEUEINSERT_Frm._PgmQueueProg);
                        dataGridQueue.ItemsSource = null;
                        dataGridQueue.ItemsSource = PgmQueueSource;
                        _NeedSave = true;
                    }
                }
              };
        }

        private void LayoutRoot_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void buttonProntPlayMemo_Click(object sender, RoutedEventArgs e)
        {
            if (_ChannelID == "")
            {
                MessageBox.Show("請先查詢節目表");
                return;
            }

            if (ModuleClass.getModulePermission("P1" + _ChannelID + "002") == false)
            {
                MessageBox.Show("您沒有列印此頻道播出提示資料的權限");
                return;
            }

            WSPGMSendSQL.SendSQLSoapClient SP_PrintPlayMemo = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";
             StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Datas>");

            foreach (PgmQueueDate TempQueue in PgmQueueSource)
            {
                sb.AppendLine("<Data>");
                sb.AppendLine("<FSPROG_ID>" + TempQueue.ProgID + "</FSPROG_ID>");
                sb.AppendLine("<FNSEQNO>" + TempQueue.Seqno + "</FNSEQNO>");
                sb.AppendLine("<FDDATE>" + TempQueue.ProgDate + "</FDDATE>");
                sb.AppendLine("<FSCHANNEL_ID>" + TempQueue.CHANNEL_ID + "</FSCHANNEL_ID>");
                sb.AppendLine("<FNEPISODE>" + TempQueue.EPISODE + "</FNEPISODE>");
                sb.AppendLine("<FSCHANNEL_NAME>" + TempQueue.CHANNEL_NAME + "</FSCHANNEL_NAME>");
                sb.AppendLine("<FSPROG_ANME>" + TransferTimecode.ReplaceXML(TempQueue.PROG_NAME) + "</FSPROG_ANME>");
                sb.AppendLine("</Data>");
            }
            sb.AppendLine("</Datas>");
            SP_PrintPlayMemo.Do_Query_Queue_Insert_TapeAsync(_Date, _ChannelID, sb.ToString(), UserClass.userData.FSUSER_ChtName);
            //SP_Q_TBPGM_QUEUE.Do_QueryCompleted += new EventHandler<SendSQL.Do_QueryCompletedEventArgs>(SP_Q_TBPGM_QUEUE_Do_QueryCompleted);
            //AAA.Do_InsertAsync("", "", ReturnStr);
            SP_PrintPlayMemo.Do_Query_Queue_Insert_TapeCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {
                        if (args.Result.ToString().StartsWith("錯誤"))
                            MessageBox.Show(args.Result.ToString(), "提示訊息", MessageBoxButton.OK);
                        else
                            HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + args.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,toolbar=0,statusbar=0,menubar=0,scrollbar=0,resizable=yes");
                    }
                    else
                        MessageBox.Show("查無相關資料，或該筆資料已不存在資料庫中。", "提示訊息", MessageBoxButton.OK);
                }


            };           
        }

        private void radToolBarBookingList_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void radToolBarBookingList1_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void dataGridQueue_KeyUp(object sender, KeyEventArgs e)
        {
        }

        private void Grid_KeyUp(object sender, KeyEventArgs e)
        {
           
        }

        private void Page_KeyUp(object sender, KeyEventArgs e)
        {
            if ((Keyboard.Modifiers & ModifierKeys.Shift) == ModifierKeys.Shift)
            {
                if (e.Key == Key.Q)
                {
                    button1_Click(sender, e);
                }
                if (e.Key == Key.M)
                {
                    buttonImport_Click(sender, e);
                }
                if (e.Key == Key.U)
                {
                    buttonUpdate_Click(sender, e);
                }
                if (e.Key == Key.R)
                {
                    buttonQueryTape_Click(sender, e);
                }
                if (e.Key == Key.S)
                {
                    buttonSaveLiftTape_Click(sender, e);
                }
                if (e.Key == Key.T)
                {
                    buttonInsertTape_Click(sender, e);
                }
                if (e.Key == Key.W)
                {
                    buttonSave_Click(sender, e);
                }
                if (e.Key == Key.N)
                {
                    buttonNoTapeList_Click(sender, e);
                }
                if (e.Key == Key.P)
                {
                    buttonPrint_Click(sender, e);
                }
                if (e.Key == Key.A)
                {
                    buttonPrintSaveTapeList_Click(sender, e);
                }
                if (e.Key == Key.D)
                {
                    buttonDeleteProg_Click(sender, e);
                }
                if (e.Key == Key.I)
                {
                    buttonInsertProg_Click(sender, e);
                }


            }
        }

        private void Page_MouseEnter(object sender, MouseEventArgs e)
        {

        }

        private void Page_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (PgmQueueSource.Count == 0)
                    comboBoxChannel.Focus();
            }
            catch
            {
                comboBoxChannel.Focus();
            }
        }

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {
            //NeedWrite = true;
            if (_NeedSave == true)
            {
                if (MessageBox.Show("節目表有異動,請問是否需要在離開前儲存節目表?", "關閉頁面", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                {
                    buttonSave_Click(sender,e);
                }
                //if (MessageBox.Show("尚未存檔,您將要關閉頁面?", "關閉頁面", MessageBoxButton.OKCancel) == MessageBoxResult.Cancel)
                //{
                    
                //}
                //else
                //if (MessageBox.Show("尚未存檔,您將要關閉頁面?", "關閉頁面", MessageBoxButton.OKCancel) == MessageBoxResult.Cancel)
                //{
                //    Application.Current.MainWindow.Close();
                //}
            } 
        }

        private void Page_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //if (MessageBox.Show("尚未存檔,您將要關閉頁面?", "關閉頁面", MessageBoxButton.OKCancel) == MessageBoxResult.Cancel)
            //{
            //    Application.Current.MainWindow.Close();
            //}
           

        }

        private void comboBoxSignal1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _NeedSave = true;
        }

        private void buttonNeedTape_Click(object sender, RoutedEventArgs e)
        {
            PGM100_09 PGMNeedTape_Frm = new PGM100_09();

            PGMNeedTape_Frm.Show();
            PGMNeedTape_Frm.Closed += (s, args) =>
            {
                
            };
        }

        private void buttonTest_Click(object sender, RoutedEventArgs e)
        {
            //WSTSM_GetFileInfo.GetFileStatusByTsmPath
            WSTSM_GetFileInfo.ServiceTSMSoapClient GET_TSM_STATUS = new WSTSM_GetFileInfo.ServiceTSMSoapClient();
            GET_TSM_STATUS.GetFileStatusByTsmPathAsync("/ptstest/ptstv/HVideo/2015/2015003/G201500300010001.mxf");
                GET_TSM_STATUS.GetFileStatusByTsmPathCompleted +=  (s, args) =>
                    {
                    };

            
        }



    }

  
}
