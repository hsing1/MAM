﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace PTS_MAM3.PGM
{
    public partial class PGM100_08 : ChildWindow
    {
        //string _Status = "";
        public string _Seqno = "";
        public string _Date = "";
        public string _channel = "";
        public PGM100_08()
        {
            InitializeComponent();
            WSPGMSendSQL.SendSQLSoapClient SP_Q_Combo = new WSPGMSendSQL.SendSQLSoapClient();

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            sb.AppendLine("</Data>");
            SP_Q_Combo.Do_QueryAsync("SP_Q_TBZCHANNEL_ALL", sb.ToString(), ReturnXML);
            SP_Q_Combo.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            ComboBoxItem TempComboBoxItem = new ComboBoxItem();
                            TempComboBoxItem.Content = elem.Element("NAME").Value.ToString();
                            TempComboBoxItem.Tag = elem.Element("ID").Value.ToString();

                            comboBoxChannel.Items.Add(TempComboBoxItem);
                        }
                    }
                }
            };

            WSPGMSendSQL.SendSQLSoapClient SP_Q_Audio = new WSPGMSendSQL.SendSQLSoapClient();
            ComboBoxItem TempComboBoxItem1 = new ComboBoxItem();
            TempComboBoxItem1.Content = "";
            TempComboBoxItem1.Tag = "";

            ComboBoxItem TempComboBoxItem2 = new ComboBoxItem();
            TempComboBoxItem2.Content = "";
            TempComboBoxItem2.Tag = "";

            ComboBoxItem TempComboBoxItem3 = new ComboBoxItem();
            TempComboBoxItem3.Content = "";
            TempComboBoxItem3.Tag = "";

            ComboBoxItem TempComboBoxItem4 = new ComboBoxItem();
            TempComboBoxItem4.Content = "";
            TempComboBoxItem4.Tag = "";

            comboBoxCH1.Items.Add(TempComboBoxItem1);
            comboBoxCH2.Items.Add(TempComboBoxItem2);
            comboBoxCH3.Items.Add(TempComboBoxItem3);
            comboBoxCH4.Items.Add(TempComboBoxItem4);
            StringBuilder sbA = new StringBuilder();
            sbA.AppendLine("<Data>");
            sbA.AppendLine("</Data>");
            SP_Q_Audio.Do_QueryAsync("SP_Q_TBZPROGLANG_ALL", sbA.ToString(), ReturnXML);
            SP_Q_Audio.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            ComboBoxItem TempComboBoxItem11 = new ComboBoxItem();
                            TempComboBoxItem11.Content = elem.Element("NAME").Value.ToString();
                            TempComboBoxItem11.Tag = elem.Element("ID").Value.ToString();
                            comboBoxCH1.Items.Add(TempComboBoxItem11);

                            ComboBoxItem TempComboBoxItem12 = new ComboBoxItem();
                            TempComboBoxItem12.Content = elem.Element("NAME").Value.ToString();
                            TempComboBoxItem12.Tag = elem.Element("ID").Value.ToString();
                            comboBoxCH2.Items.Add(TempComboBoxItem12);

                            ComboBoxItem TempComboBoxItem13 = new ComboBoxItem();
                            TempComboBoxItem13.Content = elem.Element("NAME").Value.ToString();
                            TempComboBoxItem13.Tag = elem.Element("ID").Value.ToString();
                            comboBoxCH3.Items.Add(TempComboBoxItem13);

                            ComboBoxItem TempComboBoxItem14 = new ComboBoxItem();
                            TempComboBoxItem14.Content = elem.Element("NAME").Value.ToString();
                            TempComboBoxItem14.Tag = elem.Element("ID").Value.ToString();
                            comboBoxCH4.Items.Add(TempComboBoxItem14);
                        }
                    }
                    QueryData();
                }
            };
        }

        List<CenterControlInsertTape> CCInsertTape = new List<CenterControlInsertTape>();
        public void QueryData()
        {
            WSPGMSendSQL.SendSQLSoapClient SP_Q_INSERT_TAPE = new WSPGMSendSQL.SendSQLSoapClient();
            WSPGMSendSQL.SendSQLSoapClient SP_Q_MONITER = new WSPGMSendSQL.SendSQLSoapClient();

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            if (_Seqno != "")
            {
                sb.AppendLine("<FSPROG_ID>" + textBoxProgID.Text + "</FSPROG_ID>");
                sb.AppendLine("<FNSEQNO>" + _Seqno + "</FNSEQNO>");
                sb.AppendLine("<FDDATE>" + _Date + "</FDDATE>");
                sb.AppendLine("<FSCHANNEL_ID>" + _channel + "</FSCHANNEL_ID>");
            }
            else
            {
                sb.AppendLine("<FSPROG_ID>" + textBoxProgID.Text + "</FSPROG_ID>");
                sb.AppendLine("<FNEPISODE>" + textBoxEpisode.Text + "</FNEPISODE>");
            }
            sb.AppendLine("</Data>");
            if (_Seqno != "")
            {
                SP_Q_INSERT_TAPE.Do_QueryAsync("SP_Q_TBPGM_QUEUE_INSERT_TAPE", sb.ToString(), ReturnXML);
            }
            else
            {
                SP_Q_INSERT_TAPE.Do_QueryAsync("SP_Q_TBPROG_D_INSERT_TAPE", sb.ToString(), ReturnXML);
            }

            SP_Q_INSERT_TAPE.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {
                        CCInsertTape.Clear();
                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            CCInsertTape.Add(new CenterControlInsertTape()
                            {
                                FNNO = elem.Element("FNNO").Value.ToString(),
                                FSPROMO_ID = elem.Element("FSPROMO_ID").Value.ToString(),
                                FSINSERT_CHANNEL_ID = elem.Element("FSINSERT_CHANNEL_ID").Value.ToString(),

                                //ProgDate = (Convert.ToDateTime(elem.Element("FDDATE").Value)).ToString("yyyy/MM/dd"),
                                //ProgDate = elem.Element("FDDATE").Value.ToString(),
                                FSCHANNEL_NAME = elem.Element("FSCHANNEL_NAME").Value.ToString(),
                                FSPROMO_NAME = elem.Element("FSPROMO_NAME").Value.ToString(),
                                FSPROMOTYPEID = elem.Element("FSPROMOTYPEID").Value.ToString(),
                                FSPROMOTYPENAME = elem.Element("FSPROMOTYPENAME").Value.ToString(),
                            });
                        }
                        dataGridInsertList.ItemsSource = null;
                        dataGridInsertList.ItemsSource = CCInsertTape;
                    }
                }

                if (_Seqno != "")
                {
                    SP_Q_MONITER.Do_QueryAsync("SP_Q_TBPGM_QUEUE_MONITER", sb.ToString(), ReturnXML);
                }
                else
                {
                    SP_Q_MONITER.Do_QueryAsync("SP_Q_TBPROG_D_MONITER", sb.ToString(), ReturnXML);
                }

                SP_Q_MONITER.Do_QueryCompleted += (s1, args1) =>
                {
                    if (args1.Error == null)
                    {
                        if (args1.Result != null && args1.Result != "")
                        {
                            byte[] byteArray = Encoding.Unicode.GetBytes(args1.Result);
                            StringBuilder output = new StringBuilder();
                            // Create an XmlReader
                            XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                            var ltox = from str in doc.Elements("Datas").Elements("Data")
                                       select str;
                            foreach (XElement elem in ltox)
                            {
                                if (elem.Element("FCPROG_SIDE_LABEL").Value.ToString() == "Y")
                                    radioButtonProgSideLabelYes.IsChecked = true;
                                else
                                    radioButtonProgSideLabelNO.IsChecked = true;

                                if (elem.Element("FCLIVE_SIDE_LABEL").Value.ToString() == "Y")
                                    radioButtonLiveSideLabelYes.IsChecked = true;
                                else
                                    radioButtonLiveSideLabelNO.IsChecked = true;

                                if (elem.Element("FCTIME_SIDE_LABEL").Value.ToString() == "Y")
                                    radioButtonTimeSideLabelYes.IsChecked = true;
                                else
                                    radioButtonTimeSideLabelNO.IsChecked = true;

                                if (elem.Element("FCREPLAY").Value.ToString() == "Y")
                                    radioButtonReplayYes.IsChecked = true;
                                else
                                    radioButtonReplayNO.IsChecked = true;

                                if (elem.Element("FCRECORD_PLAY").Value.ToString() == "Y")
                                    radioButtonRecordPlayYes.IsChecked = true;
                                else
                                    radioButtoRecordPlayNO.IsChecked = true;

                                if (elem.Element("FCSTEREO").Value.ToString() == "Y")
                                    radioButtonStereoYes.IsChecked = true;
                                else
                                    radioButtonStereoNO.IsChecked = true;

                                if (elem.Element("FCLOGO").Value.ToString() == "L")
                                    radioButtonLogoLeft.IsChecked = true;
                                else if (elem.Element("FCLOGO").Value.ToString() == "R")
                                    radioButtonLogoRight.IsChecked = true;
                                else
                                    radioButtonLogoNO.IsChecked = true;

                                if (elem.Element("FSCHNAME1").Value.ToString() != "")
                                    ComboBoxCheckSelectItem(elem.Element("FSCHNAME1").Value.ToString(), comboBoxCH1, "CH1");
                                else
                                    comboBoxCH1.SelectedIndex = -1;

                                if (elem.Element("FSCHNAME2").Value.ToString() != "")
                                    ComboBoxCheckSelectItem(elem.Element("FSCHNAME2").Value.ToString(), comboBoxCH2, "CH2");
                                else
                                    comboBoxCH2.SelectedIndex = -1;

                                if (elem.Element("FSCHNAME3").Value.ToString() != "")
                                    ComboBoxCheckSelectItem(elem.Element("FSCHNAME3").Value.ToString(), comboBoxCH3, "CH3");
                                else
                                    comboBoxCH3.SelectedIndex = -1;

                                if (elem.Element("FSCHNAME4").Value.ToString() != "")
                                    ComboBoxCheckSelectItem(elem.Element("FSCHNAME4").Value.ToString(), comboBoxCH4, "CH4");
                                else
                                    comboBoxCH4.SelectedIndex = -1;
                            }
                        } //if (args1.Result != null && args1.Result != "")
                        else
                        {

                        }
                    }
                };
            };


        }

        public void ComboBoxCheckSelectItem(string Instring, ComboBox SelectComboBox, String SelectText)
        {
            SelectComboBox.SelectedIndex = -1;
            for (int Combocount = 0; Combocount < SelectComboBox.Items.Count; Combocount++)
            {
                if (((ComboBoxItem)SelectComboBox.Items[Combocount]).Content.ToString() == Instring)
                {
                    SelectComboBox.SelectedIndex = Combocount;
                }
            }
            if (SelectComboBox.SelectedIndex == -1)
                MessageBox.Show("找不到對應的" + SelectText + "設定");
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            SaveData();
            this.DialogResult = true;
        }

        public void SaveData()
        {
            if (CCInsertTape.Count <= 0)
            {
                //MessageBox.Show("請先加入宣傳帶");
                //return;
            }

            WSPGMSendSQL.SendSQLSoapClient SP_I_INSERT_TAPE = new WSPGMSendSQL.SendSQLSoapClient();
            WSPGMSendSQL.SendSQLSoapClient SP_I_MONITER = new WSPGMSendSQL.SendSQLSoapClient();
            StringBuilder sb = new StringBuilder();
            StringBuilder sbDel = new StringBuilder();
            StringBuilder sbMoniter = new StringBuilder();
            StringBuilder sbMoniterDel = new StringBuilder();

            sbDel.AppendLine("<Data>");


            if (_Seqno != "")
            {
                sbDel.AppendLine("<FSPROG_ID>" + textBoxProgID.Text + "</FSPROG_ID>");
                sbDel.AppendLine("<FNSEQNO>" + _Seqno + "</FNSEQNO>");
                sbDel.AppendLine("<FDDATE>" + _Date + "</FDDATE>");
                sbDel.AppendLine("<FSCHANNEL_ID>" + _channel + "</FSCHANNEL_ID>");
            }
            else
            {
                sbDel.AppendLine("<FSPROG_ID>" + textBoxProgID.Text + "</FSPROG_ID>");
                sbDel.AppendLine("<FNEPISODE>" + textBoxEpisode.Text + "</FNEPISODE>");
            }
            sbDel.AppendLine("</Data>");

            int Save_NO = 1;
            object ReturnXML = "";
            sb.AppendLine("<Datas>");
            foreach (CenterControlInsertTape Temp_CCInsertTape in CCInsertTape)
            {
                sb.AppendLine("<Data>");
                sb.AppendLine("<FSPROG_ID>" + this.textBoxProgID.Text + "</FSPROG_ID>");
                if (_Seqno != "")
                {
                    sb.AppendLine("<FNSEQNO>" + _Seqno + "</FNSEQNO>");
                    sb.AppendLine("<FDDATE>" + _Date + "</FDDATE>");
                    sb.AppendLine("<FSCHANNEL_ID>" + _channel + "</FSCHANNEL_ID>");
                }
                sb.AppendLine("<FNEPISODE>" + this.textBoxEpisode.Text + "</FNEPISODE>");
                sb.AppendLine("<FNNO>" + Save_NO + "</FNNO>");
                Save_NO = Save_NO + 1;
                sb.AppendLine("<FSPROMO_ID>" + Temp_CCInsertTape.FSPROMO_ID + "</FSPROMO_ID>");
                sb.AppendLine("<FSINSERT_CHANNEL_ID>" + Temp_CCInsertTape.FSINSERT_CHANNEL_ID + "</FSINSERT_CHANNEL_ID>");
                sb.AppendLine("<FSCREATED_BY>" + UserClass.userData.FSUSER_ID.ToString() + "</FSCREATED_BY>");
                sb.AppendLine("</Data>");
            }
            sb.AppendLine("</Datas>");
            sbMoniter.AppendLine("<Datas>");
            sbMoniter.AppendLine("<Data>");
            sbMoniter.AppendLine("<FSPROG_ID>" + this.textBoxProgID.Text + "</FSPROG_ID>");
            if (_Seqno != "")
            {
                sbMoniter.AppendLine("<FNSEQNO>" + _Seqno + "</FNSEQNO>");
                sbMoniter.AppendLine("<FDDATE>" + _Date + "</FDDATE>");
                sbMoniter.AppendLine("<FSCHANNEL_ID>" + _channel + "</FSCHANNEL_ID>");
            }
            sbMoniter.AppendLine("<FNEPISODE>" + this.textBoxEpisode.Text + "</FNEPISODE>");
            if (radioButtonProgSideLabelYes.IsChecked == true)
                sbMoniter.AppendLine("<FCPROG_SIDE_LABEL>" + "Y" + "</FCPROG_SIDE_LABEL>");
            else
                sbMoniter.AppendLine("<FCPROG_SIDE_LABEL>" + "N" + "</FCPROG_SIDE_LABEL>");

            if (radioButtonLiveSideLabelYes.IsChecked == true)
                sbMoniter.AppendLine("<FCLIVE_SIDE_LABEL>" + "Y" + "</FCLIVE_SIDE_LABEL>");
            else
                sbMoniter.AppendLine("<FCLIVE_SIDE_LABEL>" + "N" + "</FCLIVE_SIDE_LABEL>");

            if (radioButtonTimeSideLabelYes.IsChecked == true)
                sbMoniter.AppendLine("<FCTIME_SIDE_LABEL>" + "Y" + "</FCTIME_SIDE_LABEL>");
            else
                sbMoniter.AppendLine("<FCTIME_SIDE_LABEL>" + "N" + "</FCTIME_SIDE_LABEL>");

            if (radioButtonReplayYes.IsChecked == true)
                sbMoniter.AppendLine("<FCREPLAY>" + "Y" + "</FCREPLAY>");
            else
                sbMoniter.AppendLine("<FCREPLAY>" + "N" + "</FCREPLAY>");

            if (radioButtonRecordPlayYes.IsChecked == true)
                sbMoniter.AppendLine("<FCRECORD_PLAY>" + "Y" + "</FCRECORD_PLAY>");
            else
                sbMoniter.AppendLine("<FCRECORD_PLAY>" + "N" + "</FCRECORD_PLAY>");

            if (radioButtonStereoYes.IsChecked == true)
                sbMoniter.AppendLine("<FCSTEREO>" + "Y" + "</FCSTEREO>");
            else
                sbMoniter.AppendLine("<FCSTEREO>" + "N" + "</FCSTEREO>");

            if (radioButtonLogoLeft.IsChecked == true)
                sbMoniter.AppendLine("<FCLOGO>" + "L" + "</FCLOGO>");
            else if (radioButtonLogoRight.IsChecked == true)
                sbMoniter.AppendLine("<FCLOGO>" + "R" + "</FCLOGO>");
            else
                sbMoniter.AppendLine("<FCLOGO>" + "N" + "</FCLOGO>");

            if (comboBoxCH1.SelectedIndex > 0)
                sbMoniter.AppendLine("<FSCH1>" + ((ComboBoxItem)comboBoxCH1.SelectedItem).Tag.ToString() + "</FSCH1>");
            else
                sbMoniter.AppendLine("<FSCH1>" + "" + "</FSCH1>");

            if (comboBoxCH2.SelectedIndex > 0)
                sbMoniter.AppendLine("<FSCH2>" + ((ComboBoxItem)comboBoxCH2.SelectedItem).Tag.ToString() + "</FSCH2>");
            else
                sbMoniter.AppendLine("<FSCH2>" + "" + "</FSCH2>");

            if (comboBoxCH3.SelectedIndex > 0)
                sbMoniter.AppendLine("<FSCH3>" + ((ComboBoxItem)comboBoxCH3.SelectedItem).Tag.ToString() + "</FSCH3>");
            else
                sbMoniter.AppendLine("<FSCH3>" + "" + "</FSCH3>");

            if (comboBoxCH4.SelectedIndex > 0)
                sbMoniter.AppendLine("<FSCH4>" + ((ComboBoxItem)comboBoxCH4.SelectedItem).Tag.ToString() + "</FSCH4>");
            else
                sbMoniter.AppendLine("<FSCH4>" + "" + "</FSCH4>");

            sbMoniter.AppendLine("<FSCREATED_BY>" + UserClass.userData.FSUSER_ID.ToString() + "</FSCREATED_BY>");
            sbMoniter.AppendLine("</Data>");
            sbMoniter.AppendLine("</Datas>");




            sbMoniterDel.AppendLine("<Data>");
            if (_Seqno != "")
            {
                sbMoniterDel.AppendLine("<FSPROG_ID>" + textBoxProgID.Text + "</FSPROG_ID>");
                sbMoniterDel.AppendLine("<FNSEQNO>" + _Seqno + "</FNSEQNO>");
                sbMoniterDel.AppendLine("<FDDATE>" + _Date + "</FDDATE>");
                sbMoniterDel.AppendLine("<FSCHANNEL_ID>" + _channel + "</FSCHANNEL_ID>");
            }
            else
            {
                sbMoniterDel.AppendLine("<FSPROG_ID>" + textBoxProgID.Text + "</FSPROG_ID>");
                sbMoniterDel.AppendLine("<FNEPISODE>" + textBoxEpisode.Text + "</FNEPISODE>");
            }
            sbMoniterDel.AppendLine("</Data>");


            //sb=sb.Replace("\r\n", "");
            if (_Seqno != "")
                SP_I_INSERT_TAPE.Do_Insert_ListAsync("SP_D_TBPGM_QUEUE_INSERT_TAPE", sbDel.ToString(), "SP_I_TBPGM_QUEUE_INSERT_TAPE", sb.ToString());
            else
                SP_I_INSERT_TAPE.Do_Insert_ListAsync("SP_D_TBPROG_D_INSERT_TAPE", sbDel.ToString(), "SP_I_TBPROG_D_INSERT_TAPE", sb.ToString());
            SP_I_INSERT_TAPE.Do_Insert_ListCompleted += (s, args) =>
            {
                if (args.Error == null)
                {

                    if (args.Result == true)
                    {
                        if (_Seqno != "")
                            SP_I_MONITER.Do_Insert_ListAsync("SP_D_TBPGM_QUEUE_MONITER", sbMoniterDel.ToString(), "SP_I_TBPGM_QUEUE_MONITER", sbMoniter.ToString());
                        else
                            SP_I_MONITER.Do_Insert_ListAsync("SP_D_TBPROG_D_MONITER", sbMoniterDel.ToString(), "SP_I_TBPROG_D_MONITER", sbMoniter.ToString());
                        SP_I_MONITER.Do_Insert_ListCompleted += (s1, args1) =>
                        {
                            if (args1.Error == null)
                            {

                                if (args1.Result == true)
                                    MessageBox.Show("新增成功");
                                else
                                    MessageBox.Show("新增失敗");
                            }
                        };
                    }
                    else
                        MessageBox.Show("新增失敗");
                }
            };




        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
        string _PromoTypeID = "";
        string _PromoTypeName = "";

        private void buttonQueryPromo_Click(object sender, RoutedEventArgs e)
        {
            PGM100_06 PGM100_06_Frm = new PGM100_06();

            //PGM100_06_Frm.textBoxProgID.Text = PgmQueueSource[dataGridQueue.SelectedIndex].ProgID;
            //PGM100_06_Frm.textBoxProgSeqno.Text = PgmQueueSource[dataGridQueue.SelectedIndex].Seqno;
            //PGM100_06_Frm.textBoxProgName.Text = PgmQueueSource[dataGridQueue.SelectedIndex].ProgName;
            //PGM100_06_Frm.textBoxDate.Text = PgmQueueSource[dataGridQueue.SelectedIndex].ProgDate;
            //PGM100_06_Frm.textBoxChannelID.Text = PgmQueueSource[dataGridQueue.SelectedIndex].CHANNEL_ID;
            //PGM100_06_Frm.textBoxChannelName.Text = PgmQueueSource[dataGridQueue.SelectedIndex].CHANNEL_NAME;
            //PGM100_06_Frm.textBoxBTime.Text = PgmQueueSource[dataGridQueue.SelectedIndex].BEG_TIME;
            //PGM100_06_Frm.textBoxETime.Text = PgmQueueSource[dataGridQueue.SelectedIndex].END_TIME;
            //PGM100_06_Frm.textBoxEpisode.Text = PgmQueueSource[dataGridQueue.SelectedIndex].EPISODE;
            //PGM100_06_Frm.textBoxMemo.Text = PgmQueueSource[dataGridQueue.SelectedIndex].MEMO;
            //PGM100_06_Frm._ChannelFileType = _ChannelType;
            //PGM100_06_Frm.QueryData();
            PGM100_06_Frm.Show();
            PGM100_06_Frm.Closed += (s, args) =>
            {
                if (PGM100_06_Frm.BeChooseData == true)
                {
                    textBoxPromoID.Text = PGM100_06_Frm.ReturnPromoData.FSPROMO_ID;
                    textBoxPromoName.Text = PGM100_06_Frm.ReturnPromoData.FSPROMO_NAME;
                    _PromoTypeID = PGM100_06_Frm.ReturnPromoData.FSPROMOTYPEID;
                    _PromoTypeName = PGM100_06_Frm.ReturnPromoData.FSPROMOTYPENAME;
                }
            };
        }

        private void buttonAdd_Click(object sender, RoutedEventArgs e)
        {
            if (comboBoxChannel.SelectedIndex < 0)
            {
                MessageBox.Show("請選擇頻道");
                return;
            }
            if (this.textBoxPromoID.Text == "")
            {
                MessageBox.Show("請選擇要加入的宣傳帶");
                return;
            }

            foreach (CenterControlInsertTape Temp_CCInsertTape in CCInsertTape)
            {
                if (Temp_CCInsertTape.FSPROMO_ID == this.textBoxPromoID.Text && Temp_CCInsertTape.FSINSERT_CHANNEL_ID == ((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString())
                {
                    MessageBox.Show("此宣傳帶已經加入過,不可重複加入");
                    return;
                }
            }

            CCInsertTape.Add(new CenterControlInsertTape()
            {
                FNNO = (CCInsertTape.Count + 1).ToString(),
                FSPROMO_ID = this.textBoxPromoID.Text,
                FSPROMO_NAME = this.textBoxPromoName.Text,
                FSINSERT_CHANNEL_ID = ((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString(),
                FSCHANNEL_NAME = ((ComboBoxItem)comboBoxChannel.SelectedItem).Content.ToString(),
                FSPROMOTYPEID = _PromoTypeID,
                FSPROMOTYPENAME = _PromoTypeName,
            });
            dataGridInsertList.ItemsSource = null;
            dataGridInsertList.ItemsSource = CCInsertTape;
        }

        private void buttonRemove_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridInsertList.SelectedIndex < 0)
            {
                MessageBox.Show("請先選擇要移除的宣傳帶");
                return;
            }
            CCInsertTape.RemoveAt(dataGridInsertList.SelectedIndex);
            dataGridInsertList.ItemsSource = null;
            dataGridInsertList.ItemsSource = CCInsertTape;

        }
    }
}

