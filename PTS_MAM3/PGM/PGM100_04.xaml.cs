﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace PTS_MAM3
{
    public partial class PGM100_04 : ChildWindow
    {
        public PGM100_04()
        {
            InitializeComponent();
        }
        QueryProgFile ReturnProg = new QueryProgFile();
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridSaveList.SelectedIndex < 0)
            {
                MessageBox.Show("請先選取節目資料");
                return;
            }

            for (int i = dataGridSaveList.SelectedItems.Count - 1; i >= 0; i--)
            {
                ReturnProg = ((QueryProgFile)dataGridSaveList.SelectedItems[i]);
            }
            _FSPROG_ID = ReturnProg.FSPROG_ID;
            _FNEPISODE = ReturnProg.FNEPISODE;
            _FSPGDNAME = ReturnProg.FSPGDNAME;
            _FSLENGTH = ReturnProg.FSLENGTH;
            _FSFILE_NO = ReturnProg.FSFILE_NO;
            _FCLOW_RES = ReturnProg.FCLOW_RES;

            //_FSPROG_ID = GETQueryProgFile[dataGridSaveList.SelectedIndex].FSPROG_ID;
            //_FNEPISODE = GETQueryProgFile[dataGridSaveList.SelectedIndex].FNEPISODE;
            //_FSPGDNAME = GETQueryProgFile[dataGridSaveList.SelectedIndex].FSPGDNAME;
            //_FSLENGTH = GETQueryProgFile[dataGridSaveList.SelectedIndex].FSLENGTH;
            //_FSFILE_NO = GETQueryProgFile[dataGridSaveList.SelectedIndex].FSFILE_NO;
            //_FCLOW_RES = GETQueryProgFile[dataGridSaveList.SelectedIndex].FCLOW_RES;
            ;

            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
        public string _ChannelFileType;
        public string _FSPROG_ID;
        public string _FNEPISODE;
        public string _FSPGDNAME;
        public string _FSFILE_NO;
        public string _FSLENGTH ;
        public string _FCLOW_RES;
        
        List<QueryProgFile> GETQueryProgFile = new List<QueryProgFile>();
        private void buttonQuery_Click(object sender, RoutedEventArgs e)
        {
            if (textBoxNewProgName.Text == "")
            {
                //MessageBox.Show("請輸入要查的節目");
                //return;
                textBoxNewProgName.Text = "%";
            }
            WSPGMSendSQL.SendSQLSoapClient SP_Q_PGM_PROG_FILE_STATUS = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            sb.AppendLine("<FNEPISODE>" + textBoxNewEpisode.Text + "</FNEPISODE>");

            sb.AppendLine("<FSARC_TYPE>" + _ChannelFileType + "</FSARC_TYPE>");
            //sb.AppendLine("<FSARC_TYPE>" + "002" + "</FSARC_TYPE>"); HD頻道
            sb.AppendLine("<FSPGDNAME>" + TransferTimecode.ReplaceXML(textBoxNewProgName.Text) + "</FSPGDNAME>");
            sb.AppendLine("</Data>");

            SP_Q_PGM_PROG_FILE_STATUS.Do_QueryAsync("SP_Q_PGM_PROG_FILE_STATUS", sb.ToString(), ReturnXML);
            SP_Q_PGM_PROG_FILE_STATUS.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {

                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        GETQueryProgFile.Clear();
                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            string LowStatus;

                            if (elem.Element("FCLOW_RES").Value.ToString() == "Y")
                                LowStatus = "檔案已存在";
                            else if (elem.Element("FCLOW_RES").Value.ToString() == "N")
                                LowStatus = "等待入庫";
                            else if (elem.Element("FCLOW_RES").Value.ToString() == "")
                                LowStatus = "未送帶轉檔";
                            else if (elem.Element("FCLOW_RES").Value.ToString() == "R")
                                LowStatus = "擷取中";
                            else
                                LowStatus = "不明狀態";
                            string Dur;
                            if ((elem.Element("FSBEG_TIMECODE").Value.ToString() != "") && (elem.Element("FSEND_TIMECODE").Value.ToString() != ""))
                                Dur = TransferTimecode.frame2timecode(TransferTimecode.timecodetoframe(elem.Element("FSEND_TIMECODE").Value.ToString()) - TransferTimecode.timecodetoframe(elem.Element("FSBEG_TIMECODE").Value.ToString()));
                            else
                                Dur = "";

                            GETQueryProgFile.Add(new QueryProgFile()
                            {
                                FSPROG_ID = elem.Element("FSID").Value.ToString(),
                                FNEPISODE = elem.Element("FNEPISODE").Value.ToString(),
                                FSPGDNAME = elem.Element("FSPGDNAME").Value.ToString(),

                                FSFILE_NO = elem.Element("FSFILE_NO").Value.ToString(),
                                FSLENGTH = Dur,
                                FCLOW_RES = LowStatus
                            });
                        }
                        dataGridSaveList.ItemsSource = null;
                        dataGridSaveList.ItemsSource = GETQueryProgFile;



                    }
                }
            };           
        }
    }
}

