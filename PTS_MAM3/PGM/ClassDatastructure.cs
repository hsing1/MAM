﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;
using System.ComponentModel;

namespace PTS_MAM3
{
    public static class ShareFunction
    {
        public static string XElementToString(XElement INXElement)
        {
            string ReturnStr;
            try
            {
                ReturnStr = INXElement.Value.ToString();
                return ReturnStr;

            }
            catch (Exception ex)
            {
                return "";
            }
        }
    }

    public class DateFormat
    {
        public object FormatDate(String indate)
        {
            // 取得格式化字串並且使用它來格式化日期資料。
            return (Convert.ToDateTime(indate)).ToString("yyyy/MM/dd");
        }

    }


    public class LouthKeyList
    {
        public string FSGROUP { get; set; }
        public string FSNAME { get; set; }
        public string FSNO { get; set; }
        public string FSMEMO { get; set; }
        public string FSLAYEL { get; set; }
        public string FSRULE { get; set; }
    }
    //待置換清單
    public class Wait_Replace_List
    {
        public string FSPROG_ID { get; set; }
        public string FNEPISODE { get; set; }
        public string FSPROG_NAME { get; set; }
        public string FSFILE_NO { get; set; }
        public string FSVIDEO_ID { get; set; }
        public string FSBRO_ID { get; set; }
        public string FCCHECK_STATUS { get; set; }
    }

    

    //建立救命帶清單時,查詢節目回傳值
    public class QueryProgFile
    {
        public string FSPROG_ID { get; set; }
        public string FNEPISODE { get; set; }
        public string FSPGDNAME { get; set; }
        public string FSFILE_NO { get; set; }
        public string FSLENGTH { get; set; }
        public string FCLOW_RES { get; set; }
    }

    //查詢節目回傳值
    public class ReturnSaveTapeList
    {
        public string FSPROG_ID { get; set; }
        public string FNEPISODE { get; set; }
        public string FSPGDNAME { get; set; }
        public string FSTAPENO { get; set; }
        public string FSBEG_TIMECODE { get; set; }
        public string FSEND_TIMECODE { get; set; }
        public string FCLOW_RES { get; set; }
    }

    public class ProgList
    {
        public string ProgID { get; set; }
        public string ProgName { get; set; }
    }

   
    public class PgmDateFileStatus
    {
        public string NO { get; set; }
        public string FSPROG_ID { get; set; }
        public string FNEPISODE { get; set; }
        public string FSPROG_NAME { get; set; }
        public string FNBREAK_NO { get; set; }
        public string FCPROPERTY { get; set; }
        public string fsplay_time { get; set; }
        public string FSDUR { get; set; }
        public string FNLIVE { get; set; }
        public string FSFILE_NO { get; set; }
        public string FSVIDEO_ID { get; set; }
        public string FSMEMO { get; set; }
        public string FCCHECK_STATUS { get; set; }
        public string FCFILE_STATUS { get; set; }
        public string FCSTATUS { get; set; }
        public string FSBEG_TIMECODE { get; set; }
        public string FSEND_TIMECODE { get; set; }
        public string Real_File_Status { get; set; }
    }

    public class PgmQueueDate
    {
        public string ProgID { get; set; }
        public string Seqno { get; set; }
        public string ProgName { get; set; }
        public string ProgDate { get; set; }
        public string CHANNEL_ID { get; set; }
        public string BEG_TIME { get; set; }
        public string END_TIME { get; set; }
        public string REPLAY { get; set; }
        public string EPISODE { get; set; }
        public string TAPENO { get; set; }
        public string PROG_NAME { get; set; }
        public string LIVE { get; set; }
        public string SIGNAL { get; set; }
        public string DEP { get; set; }
        public string ONOUT { get; set; }
        public string MEMO { get; set; }
        public string MEMO1 { get; set; }
        public string CHANNEL_NAME { get; set; }
        public string FILETYPE { get; set; }
        public string ProgDefSeg { get; set; }
        public string RealBEG_TIME { get; set; }
        public string RealEND_TIME { get; set; }
        public string DEF_MIN { get; set; }
        public string DEF_SEC { get; set; }
    }

    public class ActionList
    {
        public string ActionID { get; set; }
        public string ActionName { get; set; }
    }
    public class SimplePromoList
    {
        public string Promo_ID { get; set; }
        public string Promo_Name { get; set; }
    }

    public class PromoList
    {
        public string FSPROMO_ID { get; set; }
        public string FSPROMO_NAME { get; set; }
        public string FSPROMO_NAME_ENG { get; set; }
        public string FSDURATION { get; set; }
        public string FSPROG_ID { get; set; }
        public string FNEPISODE { get; set; }
        public string FSPROG_NAME { get; set; }
        public string FSPROMOTYPEID { get; set; }
        public string FSPROMOTYPENAME { get; set; }
        public string FSPROGOBJID { get; set; }
        public string FSPROGOBJNAME { get; set; }
        public string FSPRDCENID { get; set; }
        public string FSDEPTNAME { get; set; }
        public string FSACTIONID { get; set; }
        public string FSACTIONNAME { get; set; }
        public string FSPRDYYMM { get; set; }
        public string FSPROMOCATID { get; set; }
        public string FSPROMOCATNAME { get; set; }
        public string FSPROMOCATDID { get; set; }
        public string FSPROMOCATDNAME { get; set; }
        public string FSPROMOVERID { get; set; }
        public string FSPROMOVERNAME { get; set; }
        public string FSMAIN_CHANNEL_ID { get; set; }
        public string FSCHANNEL_NAME { get; set; }
        public string FSMEMO { get; set; }
    }

    public class Promo_EFFECTIVE_ZONE
    {
        public string FNPROMO_BOOKING_NO { get; set; }
        public string FSPROMO_ID { get; set; }
        public string FNNO { get; set; }
        public string FDBEG_DATE { get; set; }
        public string FDEND_DATE { get; set; }
        public string FSBEG_TIME { get; set; }
        public string FSEND_TIME { get; set; }
        public string FCDATE_TIME_TYPE { get; set; }
        public string FSCHANNEL_ID { get; set; }
        public string FSCHANNEL_NAME { get; set; }
        public string FSWEEK { get; set; }
    }

    public class Promo_SCHEDULED
    {
        public string FNPROMO_BOOKING_NO { get; set; }
        public string FSPROMO_ID { get; set; }
        public string FNNO { get; set; }
        public string FDDATE { get; set; }
        public string FSTIME { get; set; }
        public string FCSTATUS { get; set; }
        public string FSCHANNEL_ID { get; set; }
        public string FSCHANNEL_NAME { get; set; }
    }

    public class PromoBooking
    {
        public int FNPROMO_BOOKING_NO { get; set; }
        public string FSPROMO_ID { get; set; }
        public string FCSTATUS { get; set; }
        public string FSPROMO_NAME { get; set; }
        public string FSDURATION { get; set; }
        public string FSMEMO { get; set; }
        public string FSCREATED_BY { get; set; }
        public string FSUSER_ChtName { get; set; }
        public string FDCREATED_DATE { get; set; }
        public string FSCHANNEL_NAME { get; set; }
    }

    public class Promo_SCHEDULED_List
    {
        public bool ISCHECK { get; set; }
        public string FNPROMO_BOOKING_NO { get; set; }
        public string FSPROMO_ID { get; set; }
        public string FNNO { get; set; }
        public string FDDATE { get; set; }
        public string FSTIME { get; set; }
        public string FCSTATUS { get; set; }
        public string FSCHANNEL_ID { get; set; }
        public string FSCHANNEL_NAME { get; set; }
        public string FSPROMO_NAME { get; set; }
        public string FSDURATION { get; set; }
        public string FSFILE_NO { get; set; }
        public string FSVIDEO_ID { get; set; }

    }

    public class CenterControlInsertTape
    {
        public string FNNO { get; set; }
        public string FSPROMO_ID { get; set; }
        public string FSINSERT_CHANNEL_ID { get; set; }
        public string FSCHANNEL_NAME { get; set; }
        public string FSPROMO_NAME { get; set; }
        public string FSPROMOTYPEID { get; set; }
        public string FSPROMOTYPENAME { get; set; }
    }

    public class SimpleProgList
    {
        public string Prog_ID { get; set; }
        public string Prog_Name { get; set; }
    }

    public class PromoProgLinkList
    {
        public string FSPROG_ID { get; set; }
        public string FSPROG_NAME { get; set; }
        public string FNSEQNO { get; set; }
        public string FSPROMO_ID { get; set; }
        public string FSPROMO_NAME { get; set; }
        public string FNNO { get; set; }
    }

    public class PromoProgLinkDetail
    {
        public string FSPROG_ID { get; set; }
        public string FSPROG_NAME { get; set; }
        public string FNSEQNO { get; set; }
        public string FSPROMO_ID { get; set; }
        public string FSPROMO_NAME { get; set; }
        public string FNNO { get; set; }
        public string FSCHANNEL { get; set; }
        public string FSKIND { get; set; }

    }

    public class PromoChannelLinkList
    {
        public string FSPROMO_ID { get; set; }
        public string FSPROMO_NAME { get; set; }
        public string FSCHANNEL_ID { get; set; }
        public string FSCHANNEL_NAME { get; set; }
        public string FNNO { get; set; }
        public string FCLINK_TYPE { get; set; }
        public string FCTIME_TYPE { get; set; }
        public string FCINSERT_TYPE { get; set; }
    }

    public class PromoPlanList
    {
        public string FNNO { get; set; }
        public string FSPROG_ID { get; set; }
        public string FSPROG_NAME { get; set; }
        public string FCSOURCETYPE { get; set; }
        public string FCSOURCE { get; set; }
        public string FSPLAY_COUNT_MAX { get; set; }
        public string FSPLAY_COUNT_MIN { get; set; }
        public string FNPROMO_TYPEA { get; set; }
        public string FNPROMO_TYPEB { get; set; }
        public string FNPROMO_TYPEC { get; set; }
        public string FNPROMO_TYPED { get; set; }
        public string FNPROMO_TYPEE { get; set; }
        public string FSPROMO_BEG_DATE { get; set; }
        public string FSPROMO_END_DATE { get; set; }
        public string FSWEEK { get; set; }
        public string FSMEMO { get; set; }
    }

    public class ProgBankDetail
    {
        public string FSPROG_ID { get; set; }
        public string FNSEQNO { get; set; }
        public string FSPROG_NAME { get; set; }
        public string FDBEG_DATE { get; set; }
        public string FDEND_DATE { get; set; }
        public string FSBEG_TIME { get; set; }
        public string FSEND_TIME { get; set; }
        public string FSCHANNEL_ID { get; set; }
        public string FSCHANNEL_NAME { get; set; }
        public string FNREPLAY { get; set; }
        public string FSWEEK { get; set; }
        public string FSPLAY_TYPE { get; set; }
        public string FNSEG { get; set; }
        public string FNDUR { get; set; }
    }

    public class ArrPromoList : System.ComponentModel.INotifyPropertyChanged
    {
        private string _COMBINENO;
        //public string COMBINENO { get; set; }  //序號
        public string COMBINENO  //序號
        {
            get { return this._COMBINENO; }
            set
            {
                if (value != this._COMBINENO)
                {
                    this._COMBINENO = value;
                    NotifyPropertyChanged("COMBINENO");
                }

            }
        }
        //public string FCTIME_TYPE { get; set; }  //時間類型 O:定時 A:順時
        private string _FCTIME_TYPE;
        public string FCTIME_TYPE  //時間類型 O:定時 A:順時
        {
            get { return this._FCTIME_TYPE; }
            set
            {
                if (value != this._FCTIME_TYPE)
                {
                    this._FCTIME_TYPE = value;
                    NotifyPropertyChanged("FCTIME_TYPE");
                }

            }
        }
        //public string FSPROG_ID { get; set; }   //節目編碼,如果為Promo則為所屬節目編碼
        private string _FSPROG_ID;
        public string FSPROG_ID  //節目編碼,如果為Promo則為所屬節目編碼
        {
            get { return this._FSPROG_ID; }
            set
            {
                if (value != this._FSPROG_ID)
                {
                    this._FSPROG_ID = value;
                    NotifyPropertyChanged("FSPROG_ID");
                }

            }
        }
        //public string FNSEQNO { get; set; } //節目播映序號
        private string _FNSEQNO;
        public string FNSEQNO  //節目播映序號
        {
            get { return this._FNSEQNO; }
            set
            {
                if (value != this._FNSEQNO)
                {
                    this._FNSEQNO = value;
                    NotifyPropertyChanged("FNSEQNO");
                }

            }
        }
        //public string FSNAME { get; set; }  //節目或Promo名稱
        private string _FSNAME;
        public string FSNAME  //節目或Promo名稱
        {
            get { return this._FSNAME; }
            set
            {
                if (value != this._FSNAME)
                {
                    this._FSNAME = value;
                    NotifyPropertyChanged("FSNAME");
                }

            }
        }
        //public string FSCHANNEL_ID { get; set; } //頻道編碼
        private string _FSCHANNEL_ID;
        public string FSCHANNEL_ID  //頻道編碼
        {
            get { return this._FSCHANNEL_ID; }
            set
            {
                if (value != this._FSCHANNEL_ID)
                {
                    this._FSCHANNEL_ID = value;
                    NotifyPropertyChanged("FSCHANNEL_ID");
                }

            }
        }
        //public string FSCHANNEL_NAME { get; set; }  //頻道名稱
        private string _FSCHANNEL_NAME;
        public string FSCHANNEL_NAME   //頻道名稱
        {
            get { return this._FSCHANNEL_NAME; }
            set
            {
                if (value != this._FSCHANNEL_NAME)
                {
                    this._FSCHANNEL_NAME = value;
                    NotifyPropertyChanged("FSCHANNEL_NAME");
                }

            }
        }
        //public string FSPLAY_TIME { get; set; } //播出時間
        private string _FSPLAY_TIME;
        public string FSPLAY_TIME  //播出時間
        {
            get { return this._FSPLAY_TIME; }
            set
            {
                if (value != this._FSPLAY_TIME)
                {
                    this._FSPLAY_TIME = value;
                    NotifyPropertyChanged("FSPLAY_TIME");
                }

            }
        }
        private string _RealFSPLAY_TIME;
        public string RealFSPLAY_TIME  //播出時間
        {
            get { return this._RealFSPLAY_TIME; }
            set
            {
                if (value != this._RealFSPLAY_TIME)
                {
                    this._RealFSPLAY_TIME = value;
                    NotifyPropertyChanged("RealFSPLAY_TIME");
                }

            }
        }
        //public string FSDURATION { get; set; }  //長度
        private string _FSDURATION;
        public string FSDURATION  //長度
        {
            get { return this._FSDURATION; }
            set
            {
                if (value != this._FSDURATION)
                {
                    this._FSDURATION = value;
                    NotifyPropertyChanged("FSDURATION");
                }

            }
        }
        //public string FNEPISODE { get; set; }   //節目集數
        private string _FNEPISODE;
        public string FNEPISODE   //節目集數
        {
            get { return this._FNEPISODE; }
            set
            {
                if (value != this._FNEPISODE)
                {
                    this._FNEPISODE = value;
                    NotifyPropertyChanged("FNEPISODE");
                }

            }
        }

        //public string FNBREAK_NO { get; set; }  //段數
        private string _FNBREAK_NO;
        public string FNBREAK_NO   //段數
        {
            get { return this._FNBREAK_NO; }
            set
            {
                if (value != this._FNBREAK_NO)
                {
                    this._FNBREAK_NO = value;
                    NotifyPropertyChanged("FNBREAK_NO");
                }

            }
        }
        //public string FNSUB_NO { get; set; }    //支數
        private string _FNSUB_NO;
        public string FNSUB_NO  //支數
        {
            get { return this._FNSUB_NO; }
            set
            {
                if (value != this._FNSUB_NO)
                {
                    this._FNSUB_NO = value;
                    NotifyPropertyChanged("FNSUB_NO");
                }

            }
        }
        //public string FSFILE_NO { get; set; }   //節目或Promo檔名
        private string _FSFILE_NO;
        public string FSFILE_NO  //節目或Promo檔名
        {
            get { return this._FSFILE_NO; }
            set
            {
                if (value != this._FSFILE_NO)
                {
                    this._FSFILE_NO = value;
                    NotifyPropertyChanged("FSFILE_NO");
                }

            }
        }
        //public string FSVIDEO_ID { get; set; }  //節目段落和宣傳帶主控檔案編號
        private string _FSVIDEO_ID;
        public string FSVIDEO_ID  //節目段落和宣傳帶主控檔案編號
        {
            get { return this._FSVIDEO_ID; }
            set
            {
                if (value != this._FSVIDEO_ID)
                {
                    this._FSVIDEO_ID = value;
                    NotifyPropertyChanged("FSVIDEO_ID");
                }

            }
        }
        //public string FSPROMO_ID { get; set; }  //宣傳帶編碼,節目的話為空
        private string _FSPROMO_ID;
        public string FSPROMO_ID  //宣傳帶編碼,節目的話為空
        {
            get { return this._FSPROMO_ID; }
            set
            {
                if (value != this._FSPROMO_ID)
                {
                    this._FSPROMO_ID = value;
                    NotifyPropertyChanged("FSPROMO_ID");
                }

            }
        }
        //public string FDDATE { get; set; }  //日期
        private string _FDDATE;
        public string FDDATE  //日期
        {
            get { return this._FDDATE; }
            set
            {
                if (value != this._FDDATE)
                {
                    this._FDDATE = value;
                    NotifyPropertyChanged("FDDATE");
                }

            }
        }
        //public string FNLIVE { get; set; }  //是否為Live節目,1為Live,0為一般
        private string _FNLIVE;
        public string FNLIVE  //是否為Live節目,1為Live,0為一般
        {
            get { return this._FNLIVE; }
            set
            {
                if (value != this._FNLIVE)
                {
                    this._FNLIVE = value;
                    NotifyPropertyChanged("FNLIVE");
                }

            }
        }
        //public string FCTYPE { get; set; }  //節目或Promo,G為節目,P為宣傳帶
        private string _FCTYPE;
        public string FCTYPE  //節目或Promo,G為節目,P為宣傳帶
        {
            get { return this._FCTYPE; }
            set
            {
                if (value != this._FCTYPE)
                {
                    this._FCTYPE = value;
                    NotifyPropertyChanged("FCTYPE");
                }

            }
        }
        //public string FSSTATUS { get; set; }    //檔案狀態,呼叫維智的service取得檔案派送到主控檔案的狀態(其實是NASB)
        private string _FSSTATUS;
        public string FSSTATUS   //檔案狀態,呼叫維智的service取得檔案派送到主控檔案的狀態(其實是NASB)
        {
            get { return this._FSSTATUS; }
            set
            {
                if (value != this._FSSTATUS)
                {
                    this._FSSTATUS = value;
                    NotifyPropertyChanged("FSSTATUS");
                }

            }
        }
        private string _FSSIGNAL;
        public string FSSIGNAL   //檔案狀態,呼叫維智的service取得檔案派送到主控檔案的狀態(其實是NASB)
        {
            get { return this._FSSIGNAL; }
            set
            {
                if (value != this._FSSIGNAL)
                {
                    this._FSSIGNAL = value;
                    NotifyPropertyChanged("FSSIGNAL");
                }

            }
        }
        //public string FSMEMO { get; set; }  //備註,包含second event
        private string _FSMEMO;
        public string FSMEMO  //備註,包含second event
        {
            get { return this._FSMEMO; }
            set
            {
                if (value != this._FSMEMO)
                {
                    this._FSMEMO = value;
                    NotifyPropertyChanged("FSMEMO");
                }

            }
        }
        private string _FSMEMO1;
        public string FSMEMO1  //備註,包含second event
        {
            get { return this._FSMEMO1; }
            set
            {
                if (value != this._FSMEMO1)
                {
                    this._FSMEMO1 = value;
                    NotifyPropertyChanged("FSMEMO1");
                }

            }
        }
        //public string FSPROG_NAME { get; set; }  //節目名稱,宣傳帶則要要加入所屬節目名稱
        private string _FSPROG_NAME;
        public string FSPROG_NAME   //節目名稱,宣傳帶則要要加入所屬節目名稱
        {
            get { return this._FSPROG_NAME; }
            set
            {
                if (value != this._FSPROG_NAME)
                {
                    this._FSPROG_NAME = value;
                    NotifyPropertyChanged("FSPROG_NAME");
                }

            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }

    public class ArrPromo_LOST_List
    {
        public bool ISCHECK { get; set; }
        public string FNCREATE_LIST_NO { get; set; }  //建立清單序號
        public string COMBINENO { get; set; }  //序號
        public string FCTIME_TYPE { get; set; }  //時間類型 O:定時 A:順時
        public string FSPROG_ID { get; set; }   //節目編碼,如果為Promo則為所屬節目編碼
        public string FNSEQNO { get; set; } //節目播映序號
        public string FSNAME { get; set; }  //節目或Promo名稱
        public string FSCHANNEL_ID { get; set; } //頻道編碼
        
        public string FSCHANNEL_NAME { get; set; }  //頻道名稱
        public string FSPLAY_TIME { get; set; } //播出時間
        public string RealFSPLAY_TIME { get; set; }  //播出時間
        public string FSDURATION { get; set; }  //長度
        public string FNEPISODE { get; set; }   //節目集數
        public string FNBREAK_NO { get; set; }  //段數
        public string FNSUB_NO { get; set; }    //支數
        public string FSFILE_NO { get; set; }   //節目或Promo檔名
        public string FSVIDEO_ID { get; set; }  //節目段落和宣傳帶主控檔案編號
        public string FSPROMO_ID { get; set; }  //宣傳帶編碼,節目的話為空
        public string FDDATE { get; set; }  //日期
        public string FNLIVE { get; set; }  //是否為Live節目,1為Live,0為一般
        public string FCTYPE { get; set; }  //節目或Promo,G為節目,P為宣傳帶
        public string FSSTATUS { get; set; }    //檔案狀態,呼叫維智的service取得檔案派送到主控檔案的狀態(其實是NASB)
        public string FSSIGNAL{ get; set; }   //檔案狀態,呼叫維智的service取得檔案派送到主控檔案的狀態(其實是NASB)
        
        public string FSMEMO { get; set; }  //備註,包含second event
        public string FSMEMO1{ get; set; }  //備註,包含second event
        
        public string FSPROG_NAME { get; set; }  //節目名稱,宣傳帶則要要加入所屬節目名稱
        
        
    }

    public class InsPromoQueryPromo
    {

        public string FSPROMO_ID { get; set; }  //宣傳帶編碼
        public string FSPROMO_NAME { get; set; }  //Promo名稱
        public string FSPROMO_NAME_ENG { get; set; }   //Promo英文名稱
        public string FSDURATION { get; set; } //宣傳帶長度
        public string FSPROG_ID { get; set; }  //宣傳帶所屬節目編碼
        public string FNEPISODE { get; set; } //宣傳帶所屬節目頻道
        public string FSPROMOTYPEID { get; set; }  //宣傳帶類別代碼
        public string FSPROGOBJID { get; set; } //
        public string FSPRDCENID { get; set; }  //
        public string FSPRDYYMM { get; set; }   //
        public string FSPROMOCATID { get; set; }  //
        public string FSPROMOCATDID { get; set; }    //
        public string FSPROMOVERID { get; set; }   //
        public string FSMAIN_CHANNEL_ID { get; set; }  //宣傳帶所屬頻道
        public string FSBEG_TIME { get; set; }  //有效起始時間
        public string FSEND_TIME { get; set; }  //有效結束時間
        public string FCDATE_TIME_TYPE { get; set; }  //時間類型 1:日期時間區間 2:時間區間
        public string FSFILE_NO { get; set; }  //宣傳帶檔名
        public string FSVIDEO_ID { get; set; }    //宣傳帶主控播出檔案編號
        public string FDBEG_DATE { get; set; }    //有效起始日期
        public string FDEND_DATE { get; set; }    //有效結束日期
        public string FSMEMO { get; set; }    //2nd Event
        public string FSMEMO1 { get; set; }    //備註
        public string FSSIGNAL { get; set; }    //訊號來源
        public string FCFILE_STATUS { get; set; }    //檔案狀態
        public string FSTIME1 { get; set; }    //晨間
        public string FSTIME2 { get; set; }    //午間
        public string FSTIME3 { get; set; }    //晚間
        public string FSTIME4 { get; set; }    //夜間
        public string FSTIME5 { get; set; }    //隨節目
        public string FSTIME6 { get; set; }    //一日
        public string FSTIME7 { get; set; }    //一周
        public string FSTIME8 { get; set; }    //其他
        public string FSCMNO { get; set; }    //備用
        
    }

    public class Promo_Plan_TIME_SEG_SET
    {
        public string FSTIME_SEG_TYPE { get; set; }  //時段名稱
        public string FSBEG_TIME { get; set; }  //起始時間
        public string FSEND_TIME { get; set; }   //結束時間
    }

    //宣傳帶可排播次數
    public class Promo_Plan_Zone
    {
        public string FSPROMO_ID { get; set; }  //宣傳帶編碼
        public string FSPROMO_NAME { get; set; }  //宣傳帶名稱
        public string FSPROG_ID { get; set; }   //節目編碼
        public string FSPROG_NAME { get; set; }   //節目名稱
        public string FNPROMO_TYPEA { get; set; }   //A時段有效次數
        public string FNPROMO_TYPEB { get; set; }   //B時段有效次數
        public string FNPROMO_TYPEC { get; set; }   //C時段有效次數
        public string FNPROMO_TYPED { get; set; }   //D時段有效次數
        public string FNPROMO_TYPEE { get; set; }   //E時段有效次數
        public string FSPLAY_COUNT_MAX { get; set; }   //最大次數
        public string FSPLAY_COUNT_MIN { get; set; }   //最小次數
        public string FNPROMO_INSERT_COUNT { get; set; }   //插入次數
        public string FNINSERT_TYPEA { get; set; }   //A時段已插入有效次數
        public string FNINSERT_TYPEB { get; set; }   //B時段已插入有效次數
        public string FNINSERT_TYPEC { get; set; }   //C時段已插入有效次數
        public string FNINSERT_TYPED { get; set; }   //D時段已插入有效次數
        public string FNINSERT_TYPEE { get; set; }   //E時段已插入有效次數
        public string FNINSERT_ALL { get; set; }   //已插入有效次數
    }
    //節目表插入的Promo
    public class Date_Arr_Promo
    {
        public string FSPROMO_ID { get; set; }  //宣傳帶編碼
        public string FSPROMO_NAME { get; set; }  //宣傳帶名稱
        public string FSPROG_ID { get; set; }   //節目編碼
        public string FSPROG_NAME { get; set; }   //節目名稱
        public string FSPLAYTIME { get; set; }   //播出時間
    }
    //節目表插入的Promo
    public class Date_Arr_Promo_Count
    {
        public string FSPROMO_ID { get; set; }  //宣傳帶編碼
        public string FSPROMO_NAME { get; set; }  //宣傳帶名稱
        public string INSERT_COUNT { get; set; }   //插入次數
    }

    //播出運行表比對不同的節目
    public class CheckDiffProg
    {
        public string NowProgID { get; set; }  //重新合併節目ID
        public string NowProgName { get; set; }  //重新合併節目名稱
        public string NowProgEpisode { get; set; }   //重新合併節目集數
        public string NowProgSeg { get; set; }   //重新合併節目段落
        public string NowProgDur { get; set; }   //重新合併節目長度
        public string NowProgVIdeoID { get; set; }   //重新合併節目VideoID
        public string NowProgFileNO { get; set; }   //重新合併節目FileID
        public string QueueProgID { get; set; }  //目前節目ID
        public string QueueProgName { get; set; }  //目前節目名稱
        public string QueueProgEpisode { get; set; }   //目前節目集數
        public string QueueProgSeg { get; set; }   //目前節目段落
        public string QueueProgDur { get; set; }   //目前節目長度
        public string QueueProgVIdeoID { get; set; }   //目前節目VideoID
        public string QueueProgFileNO { get; set; }   //目前節目FIleNO
        public string FSSTATUS { get; set; }   //狀態
    }    
    //播出運行表比對長度不同的Promo
    public class CheckDiffPromo
    {
        public string fspromo_id { get; set; }  //短帶編碼
        public string FSPROMO_NAME { get; set; }  //短帶名稱
        public string FSBEG_TIMECODE { get; set; }   //起始時間
        public string FSEND_TIMECODE { get; set; }   //結束時間
        public string fsdur { get; set; }   //長度
        public string fsduration { get; set; }   //修正後長度
        public string FSDiffDur { get; set; }   //長度相差
        public string FSVIDEO_ID { get; set; }   //VIDEOID
        public string FSFILE_NO { get; set; }   //FILENO
        public string OLD_VIDEO_ID { get; set; }   //VIDEOID
        public string OLD_FILE_NO { get; set; }   //FILENO
        public string FSSTATUS { get; set; }  //狀態
    }


    public class LSTFileColData
    {
        public string NO { get; set; }

        public string Type { get; set; }
        public string Reconcile { get; set; }
        public string Effect1 { get; set; }
        public string Effect2 { get; set; }
        public string Effect3 { get; set; }
        public string OnAirTimecode { get; set; }
        public string ID { get; set; }
        public string Title { get; set; }
        public string SOMTimecode { get; set; }
        public string DurTimecode { get; set; }
        public string Channel { get; set; }
        public string Qualifier4 { get; set; }
        public string SegNum { get; set; }
        public string DeviceMaj { get; set; }
        public string DeviceMin { get; set; }
        public string BinHigh { get; set; }
        public string BinLow { get; set; }
        public string Qualifier1 { get; set; }
        public string Qualifier2 { get; set; }
        public string Qualifier3 { get; set; }
        public string DateToAir { get; set; }
        public string EventControl { get; set; }
        public string EventStatus { get; set; }
        public string CompileID { get; set; }
        public string CompileSOM { get; set; }
        public string BoxAID { get; set; }
        public string BoxASOM { get; set; }
        public string BoxBID { get; set; }
        public string BoxBSOM { get; set; }
        public string Reserved { get; set; }
        public string BackupDeviceMaj { get; set; }
        public string BackupDeviceMin { get; set; }
        public string ExtendedEventControl { get; set; }
        public string FadeIN { get; set; }
        public string Fast { get; set; }
    }


    //節目或宣傳帶對應的VideoID
    public class Map_Video_ID
    {
        public string FSID { get; set; }  //節目或宣傳帶編碼
        public string FNEPISODE { get; set; }  //集數,如果為宣傳帶則為0
        public string FSCHANNEL_ID { get; set; }   //頻道
        public string FSVIDEO_ID { get; set; }   //主控編號
    }
    
    //置換節目
    public class ReplaceProg
    {

        public string SFSPROG_ID { get; set; }  //來源節目編碼
        public string SFNSEQNO { get; set; }  //來源節目序號
        public string SFSPROG_NAME { get; set; }  //來源節目名稱
        public string SFNBREAK_NO { get; set; }   //來源節目段落
        public string SFSPLAY_TIME { get; set; } //來源節目播出時間
        public string SFSDURATION { get; set; } //來源節目長度
        public string SFNEPISODE { get; set; } //來源節目集數
        public string DFSPROG_ID { get; set; }  //目的節目編碼
        public string DFNSEQNO { get; set; }  //目的節目序號
        public string DFSPROG_NAME { get; set; }  //目的節目名稱
        public string DFNBREAK_NO { get; set; }   //目的節目段落
        public string DFSDURATION { get; set; } //目的節目長度
        public string DFNEPISODE { get; set; } //目的節目集數

        public string FSFILE_NO { get; set; } //目的節目檔名
        public string FSVIDEO_ID { get; set; } //目的節目VIDOEOID
        public string FNLIVE { get; set; } //目的節目LIVE
        public string FSMEMO { get; set; } //目的節目2ndEvent
        public string FSMEMO1 { get; set; } //目的節目MEMO
        public string FSSIGNAL { get; set; } //目的節目訊號源
                           
    }

}
