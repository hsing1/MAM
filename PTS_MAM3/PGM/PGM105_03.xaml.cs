﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace PTS_MAM3.PGM
{
    public partial class PGM105_03 : ChildWindow
    {
        public PGM105_03()
        {
            InitializeComponent();

            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBZDEPT_ALL = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            sb.AppendLine("</Data>");
            SP_Q_TBZDEPT_ALL.Do_QueryAsync("[SP_Q_TBZDEPT_ALL]", sb.ToString(), ReturnXML);
            SP_Q_TBZDEPT_ALL.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            ComboBoxItem TempComboBoxItem = new ComboBoxItem();
                            TempComboBoxItem.Content = elem.Element("NAME").Value.ToString();
                            TempComboBoxItem.Tag = elem.Element("ID").Value.ToString();
                            comboBoxSource.Items.Add(TempComboBoxItem);
                        }
                    }
                }

            };           

        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_PROMO_PLAN = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            sb.AppendLine("<FSPROG_ID>" + textBoxProgID.Text + "</FSPROG_ID>");
            sb.AppendLine("<FSPROG_NAME>" + TransferTimecode.ReplaceXML(textBoxProgName.Text) + "</FSPROG_NAME>");
            if (comboBoxSource.SelectedIndex < 0)
                sb.AppendLine("<FCSOURCETYPE>" + "" + "</FCSOURCETYPE>");
            else
              sb.AppendLine("<FCSOURCETYPE>" + ((ComboBoxItem)comboBoxSource.SelectedItem).Tag.ToString() + "</FCSOURCETYPE>");
           

            sb.AppendLine("<FSPROMO_BEG_DATE>" + datePickerBDate.Text + "</FSPROMO_BEG_DATE>");
            sb.AppendLine("<FSPROMO_END_DATE>" + datePickerEDate.Text + "</FSPROMO_END_DATE>");
            sb.AppendLine("</Data>");
            bool hasSeting = true;
            SP_Q_TBPGM_PROMO_PLAN.Do_QueryAsync("SP_Q_TBPGM_PROMO_PLAN", sb.ToString(), ReturnXML);
            SP_Q_TBPGM_PROMO_PLAN.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {
                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        PlanList.Clear();
                        //StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            PlanList.Add(new PromoPlanList()
                            {
                                FNNO = ShareFunction.XElementToString(elem.Element("FNNO")),
                                FSPROG_ID = ShareFunction.XElementToString(elem.Element("FSPROG_ID")),
                                FSPROG_NAME = ShareFunction.XElementToString(elem.Element("FSPROG_NAME")),
                                FCSOURCETYPE = ShareFunction.XElementToString(elem.Element("FCSOURCETYPE")),
                                FCSOURCE = ShareFunction.XElementToString(elem.Element("FCSOURCE")),
                                FSPLAY_COUNT_MAX = ShareFunction.XElementToString(elem.Element("FSPLAY_COUNT_MAX")),
                                FSPLAY_COUNT_MIN = ShareFunction.XElementToString(elem.Element("FSPLAY_COUNT_MIN")),
                                FNPROMO_TYPEA = ShareFunction.XElementToString(elem.Element("FNPROMO_TYPEA")),
                                FNPROMO_TYPEB = ShareFunction.XElementToString(elem.Element("FNPROMO_TYPEB")),
                                FNPROMO_TYPEC = ShareFunction.XElementToString(elem.Element("FNPROMO_TYPEC")),
                                FNPROMO_TYPED = ShareFunction.XElementToString(elem.Element("FNPROMO_TYPED")),
                                FNPROMO_TYPEE = ShareFunction.XElementToString(elem.Element("FNPROMO_TYPEE")),
                                FSPROMO_BEG_DATE = (Convert.ToDateTime(ShareFunction.XElementToString(elem.Element("FSPROMO_BEG_DATE")))).ToString("yyyy/MM/dd"),
                                FSPROMO_END_DATE = (Convert.ToDateTime(ShareFunction.XElementToString(elem.Element("FSPROMO_END_DATE")))).ToString("yyyy/MM/dd"),
                                FSWEEK = ShareFunction.XElementToString(elem.Element("FSWEEK")),
                                FSMEMO = ShareFunction.XElementToString(elem.Element("FSMEMO"))
                            });
                        }
                    }
                    else
                    {
                        MessageBox.Show("查無資料");
                    }
                }

                this.DialogResult = true;
            };



        }

        public List<PromoPlanList> PlanList = new List<PromoPlanList>();

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void buttonQueryProg_Click(object sender, RoutedEventArgs e)
        {
            PGM105_02 QueryProg_Frm = new PGM105_02();
            QueryProg_Frm.textBoxNewProgName.Text = textBoxProgName.Text;
            QueryProg_Frm.SendQuery();
            QueryProg_Frm.Show();
            QueryProg_Frm.Closed += (s, args) =>
            {
                if (QueryProg_Frm._FSPROG_ID != "")
                {
                    textBoxProgID.Text = QueryProg_Frm._FSPROG_ID;
                    textBoxProgName.Text = QueryProg_Frm._FSPGDNAME;
                }
            };
            
        }
    }
}

