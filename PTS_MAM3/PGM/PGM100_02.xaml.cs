﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;
using System.Text;
using System.IO;
using Telerik.Windows.Controls;

namespace PTS_MAM3.PGM
{
    public partial class PGM100_02 : ChildWindow
    {
        public PGM100_02()
        {
            InitializeComponent();
        }

        List<LouthKeyList> LouthKey = new List<LouthKeyList>();

        public string InLouthKeyString;
        public string OutLouthKeyString;

        public string WorkChannelID;

        List<string> WaitList=new List<string>();
        List<string> ContinueList = new List<string>();
        List<string> NameList = new List<string>();


        public bool CheckTime(string InTime)
        {
            if (InTime == "__:__:__")
            {
                return true;
            }
            else
            {
                try
                {
                    if (int.Parse(InTime.Substring(0, 2)) > 1)
                        return false;
                    if (int.Parse(InTime.Substring(3, 2)) >= 60)
                        return false;
                    if (int.Parse(InTime.Substring(6, 2)) >= 60)
                        return false;

                    return true;
                }
                catch
                {
                    return false;
                }

            }
        }

        public bool CheckINT(string InTime)
        {
            if (InTime.Trim() == "")
            {
                return true;
            }
            else
            {
                try
                {
                    int.Parse(InTime.Trim());
                    return true;
                }
                catch
                {
                    return false;
                }

            }
        }

        public string TransString(XElement Instring)
        {
            try
            {
                Instring.Value.ToString();
                return Instring.Value.ToString();
            }
            catch
            {
                return "";
            }
        }

        public void AddComboboxItem(ComboBox AddComboBox, String SelectText,string SelectLayal)
        {
            ComboBoxItem TempComboboxItem = new ComboBoxItem();
            TempComboboxItem.Content = SelectText;
            TempComboboxItem.Tag = SelectLayal;
            AddComboBox.Items.Add(TempComboboxItem);
        }

        public void AnalyLouthKeyString(string InString)
        {
            //InString = "+LOGO";

            string[] LouthKeys = InString.Split(new Char[] { '+' });

            foreach (string LouthKey in LouthKeys)
            {
                if (LouthKey.Trim() != "")
                {
                    string[] LouthKeyType = LouthKey.Split(new Char[] { '#' });
                    if (LouthKeyType[0] != "") //如果有設定LouthKey Ex:中/英#00:02:00,10,30
                    {
                        if (LouthKeyType[0].IndexOf("/") > -1) //有斜線代表為雙語
                        {
                            checkBoxDoubleLang.IsChecked = true;
                            ComboBoxCheckSelectItem("雙語", comboBoxDoubleLang, LouthKeyType[0]);

                            if (LouthKey.IndexOf("#") > -1)  //#00:02:00,10,30 //表示有設定起始時間,間隔,長度
                                Trans(LouthKeyType[1], tbxDoubleLangST, textBoxDoubleLangInterval, textBoxDoubleLangDur, textBoxDoubleLangSec);
                        }

                        if (LouthKeyType[0].IndexOf("片名") > -1) //代表為片名
                        {
                            checkBoxName.IsChecked = true;
                            if (LouthKeyType[0].IndexOf("(整段)") > -1) //代表為片名(整段)
                                this.checkBoxNameAll.IsChecked = true;
                            else if (LouthKeyType[0].IndexOf("(全程避片頭)") > -1)
                                this.checkBoxNameOne.IsChecked = true;
                            else
                                this.checkBoxNameAll.IsChecked = false;
                            if (LouthKey.IndexOf("#") > -1)  //#00:02:00,10,30 //表示有設定起始時間,間隔,長度
                                Trans(LouthKeyType[1], tbxNameST, textBoxNameInterval, textBoxNameDur, textBoxNameSec);
                            if (LouthKeyType[0].IndexOf("(整段)") > -1)
                                autoCompleteBoxName.Text = LouthKeyType[0].Substring(3,(LouthKeyType[0].Length)-7);
                            else if (LouthKeyType[0].IndexOf("(全程避片頭)") > -1)
                                autoCompleteBoxName.Text = LouthKeyType[0].Substring(3, (LouthKeyType[0].Length) - 10);
                            else
                                autoCompleteBoxName.Text = LouthKeyType[0].Substring(3, (LouthKeyType[0].Length) - 3);
                        }

                        if (LouthKeyType[0].IndexOf("立體聲") > -1)  //代表為立體聲
                        {
                            checkBoxStereo.IsChecked = true;
                            ComboBoxCheckSelectItem("立體聲", comboBoxStereo, LouthKeyType[0]);

                            if (LouthKey.IndexOf("#") > -1)  //#00:02:00,10,30 //表示有設定起始時間,間隔,長度
                                Trans(LouthKeyType[1], tbxStereoST, textBoxStereoInterval, textBoxStereoDur, textBoxStereoSec);
                        }

                        if (LouthKeyType[0].IndexOf("口述影像版") > -1)  //代表為數位同步播出
                        {
                            checkBoxDigital.IsChecked = true;
                            ComboBoxCheckSelectItem("口述影像版", comboBoxDigital, LouthKeyType[0]);
                        }
                        if (LouthKeyType[0].IndexOf("接著") > -1)   //代表為接著
                        {
                            checkBoxWait.IsChecked = true;
                            autoCompleteBoxWait.Text = LouthKeyType[0];
                            //ComboBoxCheckSelectItem("接著", comboBoxWait, LouthKeyType[0]);

                            if (LouthKey.IndexOf("#") > -1)  //#90,30 //表示有設定片尾前幾秒,長度
                                Trans1(LouthKeyType[1], textBoxWaitEndPoint, textBoxWaitDur);
                        }
                        //代表為繼續收看
                        if ((LouthKeyType[0].IndexOf("下週") > -1) || (LouthKeyType[0].IndexOf("明天") > -1) || (LouthKeyType[0].IndexOf("下次") > -1))
                        {
                            checkBoxContinue.IsChecked = true;
                            autoCompleteBoxContinue.Text = LouthKeyType[0];
                            //ComboBoxCheckSelectItem("繼續收看", comboBoxContinue, LouthKeyType[0]);

                            if (LouthKey.IndexOf("#") > -1)  //#90,30 //表示有設定片尾前幾秒,長度
                                Trans1(LouthKeyType[1], textBoxContinueEndPoint, textBoxContinueDur);
                        }

                        if (LouthKeyType[0].IndexOf("重播") > -1)  //代表為重播
                        {
                            checkBoxReplay.IsChecked = true;
                            ComboBoxCheckSelectItem("重播", comboBoxReplay, LouthKeyType[0]);
                        }
                        if (LouthKeyType[0].IndexOf("直播") > -1)  //代表為重播
                        {
                            checkBoxLivePlay.IsChecked = true;
                            ComboBoxCheckSelectItem("直播", comboBoxLivePlay, LouthKeyType[0]);
                        }
                        if (LouthKeyType[0].ToUpper().IndexOf("LIVE") > -1)  //代表為Live
                        {
                            checkBoxLive.IsChecked = true;
                            ComboBoxCheckSelectItem("LIVE", comboBoxLive, LouthKeyType[0]);
                        }
                        if (LouthKeyType[0].IndexOf("錄影轉播") > -1)  //代表為錄影轉播
                        {
                            checkBoxRecordPlay.IsChecked = true;
                            ComboBoxCheckSelectItem("錄影轉播", comboBoxRecordPlay, LouthKeyType[0]);
                        }
                        if (LouthKeyType[0].IndexOf("時間") > -1)  //代表為時間
                        {
                            checkBoxTime.IsChecked = true;
                            ComboBoxCheckSelectItem("時間", comboBoxTime, LouthKeyType[0]);
                        }
                        if (LouthKeyType[0].ToUpper().IndexOf("LOGO") > -1)  //代表為Logo
                        {
                            checkBoxLogo.IsChecked = true;
                            ComboBoxCheckSelectItem("LOGO", comboBoxLogo, LouthKeyType[0]);
                        }
                        if (LouthKeyType[0].ToUpper().IndexOf("LOSE") > -1)  //代表為Lose
                        {
                            checkBoxLose.IsChecked = true;
                        }
                    }

                }
            }
        }


        public bool CheckLouthKeyLayel(string InString)
        {
            //InString = "+LOGO";

            string[] LouthKeys = InString.Split(new Char[] { '+' });
            comboBoxNowKey.Items.Clear();
            bool HasSameLayel = false;

            
            string LayelSameMessage = "";
            foreach (string LouthKeyString in LouthKeys)
            {
                if (LouthKeyString.Trim() != "")
                {
                    string[] LouthKeyType = LouthKeyString.Split(new Char[] { '#' });

                    if (LouthKeyType[0].IndexOf("(整段)") > -1) //代表為片名(整段)
                    {
                        LouthKeyType[0] = LouthKeyType[0].Substring(3, (LouthKeyType[0].Length) - 7);
                    }
                    else if (LouthKeyType[0].IndexOf("全程避片頭") > -1)
                        LouthKeyType[0] = LouthKeyType[0].Substring(3, (LouthKeyType[0].Length) - 10);
                    else if (LouthKeyType[0].IndexOf("片名") > -1)
                        LouthKeyType[0] = LouthKeyType[0].Substring(3, (LouthKeyType[0].Length) - 3);
                    else
                        LouthKeyType[0] = LouthKeyType[0];

                    if (LouthKeyType[0] != "") //如果有設定LouthKey Ex:中/英#00:02:00,10,30
                    {
                        //LouthKeyType[0] //表示每個FSMEMO中每個+號的字串,要跟TempList.FSNAME做比對
                        foreach (LouthKeyList TempList in LouthKey)
                        {
                            if (TempList.FSNAME == LouthKeyType[0]) 
                            {
                                Boolean flag =false;
                                for (int nowComboCount = 0; nowComboCount < comboBoxNowKey.Items.Count; nowComboCount++)
                                {
                                    if (((ComboBoxItem)comboBoxNowKey.Items[nowComboCount]).Content.ToString() == TempList.FSNAME)
                                        flag = true;
                                }
                                if (flag==false)
                                    AddComboboxItem(comboBoxNowKey, TempList.FSNAME, TempList.FSLAYEL);
                            }
                        }
                    }
                }
            }
            //開始分析comboBoxNowKey 中的是否有在相同Layel
            for (int nowComboCount = 0; nowComboCount < comboBoxNowKey.Items.Count; nowComboCount++)
            {
                string nowLayel = ((ComboBoxItem)comboBoxNowKey.Items[nowComboCount]).Tag.ToString();
                for (int Combocount = nowComboCount + 1; Combocount < comboBoxNowKey.Items.Count; Combocount++)
                {
                    if (((ComboBoxItem)comboBoxNowKey.Items[Combocount]).Tag.ToString() == nowLayel)
                    {
                        LayelSameMessage = LayelSameMessage + ((ComboBoxItem)comboBoxNowKey.Items[nowComboCount]).Content.ToString()
                            + "與" + ((ComboBoxItem)comboBoxNowKey.Items[Combocount]).Content.ToString() + "在相同階層  ";
                    }
                }
            }

            if (LayelSameMessage != "")
            {
                MessageBox.Show(LayelSameMessage);
                HasSameLayel = true;
            }
                    
            return HasSameLayel;
        }

        string[] TimeStatus;
        public void Trans(string Instring, RadMaskedTextBox TextBoxST, TextBox TextBoxInterval, TextBox TextBoxDur, TextBox TextBoxSec)
        {
            TimeStatus = Instring.Split(new Char[] { ',' });
            if (Instring.IndexOf(",") > -1)//代表有 間隔,長度
            {
                TextBoxST.MaskedText = TimeStatus[0];
                TextBoxInterval.Text = TimeStatus[1];
                TextBoxDur.Text = TimeStatus[2];

                try
                {
                    TextBoxSec.Text = TimeStatus[3];
                }
                catch (Exception ex)
                {
                    TextBoxSec.Text = "0";
                }
                finally
                {

                }
              
            }
            else
            {
                TextBoxST.MaskedText = TimeStatus[0];
                TextBoxInterval.Text = "";
                TextBoxDur.Text = "";
                TextBoxSec.Text = "0";
            }
        }

        public void Trans1(string Instring, TextBox TextBoxEndPoint, TextBox TextBoxDur)
        {
            TimeStatus = Instring.Split(new Char[] { ',' });
            if (Instring.IndexOf(",") > -1)//代表有 間隔,長度
            {
                TextBoxEndPoint.Text = TimeStatus[0];
                TextBoxDur.Text = TimeStatus[1];
            }
            else
            {
                TextBoxEndPoint.Text = TimeStatus[0];
                TextBoxDur.Text = "";
            }
        }

        public void ComboBoxCheckSelectItem(string Instring, ComboBox SelectComboBox, String SelectText)
        {
            SelectComboBox.SelectedIndex = -1;
            for (int Combocount = 0; Combocount < SelectComboBox.Items.Count; Combocount++)
            {
                if (((ComboBoxItem)SelectComboBox.Items[Combocount]).Content.ToString() == SelectText)
                {
                    SelectComboBox.SelectedIndex = Combocount;
                }
            }
            if (SelectComboBox.SelectedIndex == -1)
                MessageBox.Show("找不到對應的" + Instring + "設定");
        }



        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool Tempbool = SetLouthKey();
                if (SetLouthKey() == false)
                {
                    return;
                }
                else
                {
                    OutLouthKeyString = textBox1.Text;

                    if (CheckLouthKeyLayel(textBox1.Text) == true)
                    {
                        if (MessageBox.Show("您確定要設定此LouthKey?", "確認", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                            this.DialogResult = true;
                    }
                    else
                        this.DialogResult = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("設定Louth Key 發生錯誤,訊息:" + ex.Message + ":請重新設定");
                return;
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        public bool SetLouthKey()
        {
            int i = 0;

            #region 檢查是否有不可同時勾選的資料

            if (checkBoxLivePlay.IsChecked == true)
            {
                i = i + 1;
            }
            if (checkBoxLive.IsChecked == true)
            {
                i = i + 1;
            }
            if (checkBoxRecordPlay.IsChecked == true)
            {
                i = i + 1;
            }
            if (i > 1)
            {
                MessageBox.Show("直播,Live,錄影直播不可同時勾選");

            }
            //i = 0;
            //if (checkBoxWait.IsChecked == true)
            //{
            //    i = i + 1;
            //}
            //if (checkBoxContinue.IsChecked == true)
            //{
            //    i = i + 1;
            //}
            ////if (checkBoxNextWeek.IsChecked == true)
            ////{
            ////    i = i + 1;
            ////}

            //if (i > 1)
            //{
            //    MessageBox.Show("稍後,繼續收看不可同時勾選");
            //}
            #endregion

            if (checkBoxDoubleLang.IsChecked == true)
            {
                if (comboBoxDoubleLang.SelectedIndex != -1)
                {
                    if (((ComboBoxItem)comboBoxDoubleLang.SelectedItem).Content.ToString() == "")
                    {
                        MessageBox.Show("請選擇雙語種類");
                        return false;
                    }
                }
                if (CheckTime(tbxDoubleLangST.MaskedText) == false)
                {
                    MessageBox.Show("雙語起始時間格式有誤");
                    return false;
                }
                if (CheckINT(textBoxDoubleLangInterval.Text) == false)
                {
                    MessageBox.Show("雙語間隔格式有誤");
                    return false;
                }
                if (CheckINT(textBoxDoubleLangDur.Text) == false)
                {
                    MessageBox.Show("雙語長度格式有誤");
                    return false;
                }

                if (textBoxDoubleLangDur.Text != "" && textBoxDoubleLangInterval.Text != "")
                {
                    if (int.Parse(textBoxDoubleLangDur.Text) > int.Parse(textBoxDoubleLangInterval.Text) * 60)
                    {
                        MessageBox.Show("雙語長度不可大於間隔");
                        return false;
                    }
                }
                if (CheckINT(textBoxDoubleLangSec.Text) == false)
                {
                    MessageBox.Show("雙語第二段延後秒數格式有誤");
                    return false;
                }
                
            }

            if (checkBoxName.IsChecked == true)
            {
                if (autoCompleteBoxName.SelectedItem == null)
                {
                    MessageBox.Show("您輸入的片名不存在");
                    return false;
                }
                if (checkBoxNameAll.IsChecked == true && checkBoxNameOne.IsChecked == true)
                {
                    MessageBox.Show("片名不可同時勾選整段與全程避片頭");
                    return false;
                }

                if ((checkBoxNameAll.IsChecked == true || checkBoxNameOne.IsChecked == true) && (tbxNameST.MaskedText != "__:__:__"))
                {
                    MessageBox.Show("片名勾選整段或全程避片頭時不可設定起始時間,間隔,秒數等資訊");
                    return false;
                }


                if (CheckTime(tbxNameST.MaskedText) == false)
                {
                    MessageBox.Show("片名起始時間格式有誤");
                    return false;
                }
                if (CheckINT(textBoxNameInterval.Text) == false)
                {
                    MessageBox.Show("片名間隔格式有誤");
                    return false;
                }
                if (CheckINT(textBoxNameDur.Text) == false)
                {
                    MessageBox.Show("片名長度格式有誤");
                    return false;
                }

                if (textBoxNameDur.Text != "" && textBoxNameInterval.Text != "")
                {
                    if (int.Parse(textBoxNameDur.Text) > int.Parse(textBoxNameInterval.Text) * 60)
                    {
                        MessageBox.Show("片名長度不可大於間隔");
                        return false;
                    }
                }
                if (CheckINT(textBoxNameSec.Text) == false)
                {
                    MessageBox.Show("片名第二段延後秒數格式有誤");
                    return false;
                }
            }

            if (checkBoxStereo.IsChecked == true)
            {
                if (comboBoxStereo.SelectedIndex == -1)
                {
                    MessageBox.Show("請選擇立體聲種類");
                    return false;
                }
                if (CheckTime(tbxStereoST.MaskedText) == false)
                {
                    MessageBox.Show("立體聲起始時間格式有誤");
                    return false;
                }
                if (CheckINT(textBoxStereoInterval.Text) == false)
                {
                    MessageBox.Show("立體聲間隔格式有誤");
                    return false;
                }
                if (CheckINT(textBoxStereoDur.Text) == false)
                {
                    MessageBox.Show("立體聲長度格式有誤");
                    return false;
                }
                if (textBoxStereoDur.Text != "" && textBoxStereoInterval.Text != "")
                {
                    if (int.Parse(textBoxStereoDur.Text) > int.Parse(textBoxStereoInterval.Text) * 60)
                    {
                        MessageBox.Show("立體聲長度不可大於間隔");
                        return false;
                    }
                }
                if (CheckINT(textBoxStereoSec.Text) == false)
                {
                    MessageBox.Show("立體聲第二段延後秒數格式有誤");
                    return false;
                }
            }

            if (checkBoxReplay.IsChecked == true)
            {
                if (comboBoxReplay.SelectedIndex == -1)
                {
                    MessageBox.Show("請選擇重播種類");
                    return false;
                }
            }

            if (checkBoxDigital.IsChecked == true)
            {
                if (comboBoxDigital.SelectedIndex == -1)
                {
                    MessageBox.Show("請選擇口述影像版種類");
                    return false;
                }

               
            }

            if (checkBoxLivePlay.IsChecked == true)
            {
                if (comboBoxLivePlay.SelectedIndex == -1)
                {
                    MessageBox.Show("請選擇直播種類");
                    return false;
                }
            }

            if (checkBoxLive.IsChecked == true)
            {
                if (comboBoxLive.SelectedIndex == -1)
                {
                    MessageBox.Show("請選擇Live種類");
                    return false;
                }
            }

            if (checkBoxRecordPlay.IsChecked == true)
            {
                if (comboBoxRecordPlay.SelectedIndex == -1)
                {
                    MessageBox.Show("請選擇錄影轉播種類");
                    return false;
                }
            }

            if (checkBoxTime.IsChecked == true)
            {
                if (comboBoxTime.SelectedIndex == -1)
                {
                    MessageBox.Show("請選擇時間種類");
                    return false;
                }
            }

            if (checkBoxLogo.IsChecked == true)
            {
                if (comboBoxLogo.SelectedIndex == -1)
                {
                    MessageBox.Show("請選擇Logo種類");
                    return false;
                }
            }
            if (checkBoxWait.IsChecked == true)
            {
                if (autoCompleteBoxWait.Text != "")
                {
                    if (autoCompleteBoxWait.SelectedItem == null)
                    {
                        MessageBox.Show("您輸入的接著種類不存在");
                        return false;
                    }

                    if (CheckINT(textBoxWaitEndPoint.Text) == false)
                    {
                        MessageBox.Show("接著 片尾前幾秒 格式有誤");
                        return false;
                    }
                    if (CheckINT(textBoxWaitDur.Text) == false)
                    {
                        MessageBox.Show("接著 長度 格式有誤");
                        return false;
                    }
                }
            }
            if (checkBoxContinue.IsChecked == true)
            {
                if (autoCompleteBoxContinue.Text != "")
                {
                    if (autoCompleteBoxContinue.SelectedItem == null)
                    {
                        MessageBox.Show("您輸入的繼續收看種類不存在");
                        return false;
                    }
                    if (CheckINT(textBoxContinueEndPoint.Text) == false)
                    {
                        MessageBox.Show("繼續收看 片尾前幾秒 格式有誤");
                        return false;
                    }
                    if (CheckINT(textBoxContinueDur.Text) == false)
                    {
                        MessageBox.Show("繼續收看 長度 格式有誤");
                        return false;
                    }
                
                }
            }

            this.textBox1.Text = "";
            if (this.checkBoxDoubleLang.IsChecked == true)
            {
                this.textBox1.Text = this.textBox1.Text + "+" + ((ComboBoxItem)comboBoxDoubleLang.SelectedItem).Content.ToString();
                if (tbxDoubleLangST.MaskedText != "__:__:__")
                {
                    this.textBox1.Text = this.textBox1.Text + "#" + tbxDoubleLangST.MaskedText;
                    if ((textBoxDoubleLangInterval.Text != "") && (textBoxDoubleLangDur.Text != "") && (textBoxDoubleLangSec.Text != ""))
                        this.textBox1.Text = this.textBox1.Text + "," + textBoxDoubleLangInterval.Text + "," + textBoxDoubleLangDur.Text + "," + textBoxDoubleLangSec.Text;
                }
            }


            if (this.checkBoxName.IsChecked == true)
            {
                if (checkBoxNameAll.IsChecked == true)
                    this.textBox1.Text = this.textBox1.Text + "+片名-" + autoCompleteBoxName.Text + "(整段)";
                else if (checkBoxNameOne.IsChecked == true)
                    this.textBox1.Text = this.textBox1.Text + "+片名-" + autoCompleteBoxName.Text + "(全程避片頭)";
                else
                    this.textBox1.Text = this.textBox1.Text + "+片名-" + autoCompleteBoxName.Text;

                if (tbxNameST.MaskedText != "__:__:__")
                {
                    this.textBox1.Text = this.textBox1.Text + "#" + tbxNameST.MaskedText;
                    if ((textBoxNameInterval.Text != "") && (textBoxNameDur.Text != "") && (textBoxNameSec.Text != ""))
                        this.textBox1.Text = this.textBox1.Text + "," + textBoxNameInterval.Text + "," + textBoxNameDur.Text + "," + textBoxNameSec.Text;
                }
            }

            if (this.checkBoxStereo.IsChecked == true)
            {
                this.textBox1.Text = this.textBox1.Text + "+" + ((ComboBoxItem)comboBoxStereo.SelectedItem).Content.ToString();
                if (tbxStereoST.MaskedText != "__:__:__")
                {
                    this.textBox1.Text = this.textBox1.Text + "#" + tbxStereoST.MaskedText;
                    if ((textBoxStereoInterval.Text != "") && (textBoxStereoDur.Text != "") && (textBoxStereoSec.Text != ""))
                        this.textBox1.Text = this.textBox1.Text + "," + textBoxStereoInterval.Text + "," + textBoxStereoDur.Text + "," + textBoxStereoSec.Text;
                }
            }

            if (this.checkBoxDigital.IsChecked == true)
            {
                this.textBox1.Text = this.textBox1.Text + "+" + ((ComboBoxItem)comboBoxDigital.SelectedItem).Content.ToString();
                
            }
            if (this.checkBoxWait.IsChecked == true)
            {
                //this.textBox1.Text = this.textBox1.Text + "+" + ((ComboBoxItem)comboBoxWait.SelectedItem).Content.ToString();
                this.textBox1.Text = this.textBox1.Text + "+" + autoCompleteBoxWait.Text;
                if ((textBoxWaitEndPoint.Text != "") && (textBoxWaitDur.Text != ""))
                    this.textBox1.Text = this.textBox1.Text + "#" + textBoxWaitEndPoint.Text + "," + textBoxWaitDur.Text;
            }
            if (this.checkBoxContinue.IsChecked == true)
            {
                //this.textBox1.Text = this.textBox1.Text + "+" + ((ComboBoxItem)comboBoxContinue.SelectedItem).Content.ToString();
                this.textBox1.Text = this.textBox1.Text + "+" + autoCompleteBoxContinue.Text;
                if ((textBoxContinueEndPoint.Text != "") && (textBoxContinueDur.Text != ""))
                    this.textBox1.Text = this.textBox1.Text + "#" + textBoxContinueEndPoint.Text + "," + textBoxContinueDur.Text;
            }


            if (this.checkBoxReplay.IsChecked == true)
            {
                this.textBox1.Text = this.textBox1.Text + "+" + ((ComboBoxItem)comboBoxReplay.SelectedItem).Content.ToString();
            }
            if (this.checkBoxLivePlay.IsChecked == true)
            {
                this.textBox1.Text = this.textBox1.Text + "+" + ((ComboBoxItem)comboBoxLivePlay.SelectedItem).Content.ToString();
            }

            if (this.checkBoxLive.IsChecked == true)
            {
                this.textBox1.Text = this.textBox1.Text + "+" + ((ComboBoxItem)comboBoxLive.SelectedItem).Content.ToString();
            }

            if (this.checkBoxRecordPlay.IsChecked == true)
            {
                this.textBox1.Text = this.textBox1.Text + "+" + ((ComboBoxItem)comboBoxRecordPlay.SelectedItem).Content.ToString();
            }

            if (this.checkBoxTime.IsChecked == true)
            {
                this.textBox1.Text = this.textBox1.Text + "+" + ((ComboBoxItem)comboBoxTime.SelectedItem).Content.ToString();
            }
            if (this.checkBoxLogo.IsChecked == true)
            {
                this.textBox1.Text = this.textBox1.Text + "+" + ((ComboBoxItem)comboBoxLogo.SelectedItem).Content.ToString();
            }

            OutLouthKeyString = this.textBox1.Text;
            return true;
        }
     

        private void LayoutRoot_Loaded(object sender, RoutedEventArgs e)
        {
                        //InLouthKeyString = "";
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_LOUTH_KEY = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            sb.AppendLine("<FSCHANNEL_ID>" + WorkChannelID + "</FSCHANNEL_ID>");
            sb.AppendLine("</Data>");
            SP_Q_TBPGM_LOUTH_KEY.Do_QueryAsync("SP_Q_TBPGM_LOUTH_KEY", sb.ToString(), ReturnXML);
            //SP_Q_TBPGM_QUEUE.Do_QueryCompleted += new EventHandler<SendSQL.Do_QueryCompletedEventArgs>(SP_Q_TBPGM_QUEUE_Do_QueryCompleted);
            //AAA.Do_InsertAsync("", "", ReturnStr);
            SP_Q_TBPGM_LOUTH_KEY.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        LouthKey.Clear();
                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            LouthKey.Add(new LouthKeyList()
                            {
                                FSGROUP = TransString(elem.Element("FSGROUP")),
                                FSNAME = TransString(elem.Element("FSNAME")),
                                FSNO = TransString(elem.Element("FSNO")),

                                FSMEMO = TransString(elem.Element("FSMEMO")),
                                FSLAYEL = TransString(elem.Element("FSLAYEL")),
                                FSRULE = TransString(elem.Element("FSRULE"))
                            });
                        }
                        //要將資料寫入Combobox

                        foreach (LouthKeyList TempList in LouthKey)
                        {
                            if (TempList.FSNAME.IndexOf("/") > -1) //有斜線代表為雙語
                            {
                                AddComboboxItem(comboBoxDoubleLang, TempList.FSNAME, TempList.FSLAYEL);
                                //ComboBoxItem TempComboboxItem = new ComboBoxItem();
                                //TempComboboxItem.Content = TempList.FSNAME;
                                //comboBoxDoubleLang.Items.Add(TempComboboxItem);
                            }
                            if (TempList.FSGROUP == "片名")
                            {
                                NameList.Add(TempList.FSNAME);
                            }
                            //if (TempList.FSNAME.IndexOf("片名") > -1) //代表為片名
                            //{
                            //    //AddComboboxItem(comboBoxDoubleLang, TempList.FSNAME);
                            //    NameList.Add(TempList.FSNAME);
                            //}
                            if (TempList.FSNAME.IndexOf("立體聲") ==0) //代表為立體聲
                            {
                                AddComboboxItem(comboBoxStereo, TempList.FSNAME, TempList.FSLAYEL);
                            }
                            //if (TempList.FSNAME.IndexOf("數位") > -1) //代表為數位
                            //{
                            //    AddComboboxItem(comboBoxDigital, TempList.FSNAME, TempList.FSLAYEL);
                            //}
                            if (TempList.FSNAME.IndexOf("口述影像版") > -1) //代表為口述影像版
                            {
                                AddComboboxItem(comboBoxDigital, TempList.FSNAME, TempList.FSLAYEL);
                            }
                            if (TempList.FSNAME.IndexOf("接著") ==0) //代表為接著
                            {
                                //AddComboboxItem(comboBoxWait, TempList.FSNAME);
                                WaitList.Add(TempList.FSNAME);
                            }
                            if ((TempList.FSNAME.IndexOf("下週") ==0) || (TempList.FSNAME.IndexOf("明天") ==0) || (TempList.FSNAME.IndexOf("下次") ==0)) //代表為繼續收看
                            {
                                //AddComboboxItem(comboBoxContinue, TempList.FSNAME);
                                ContinueList.Add(TempList.FSNAME);
                            }
                            if (TempList.FSNAME.IndexOf("重播") == 0) //代表為重播
                            {
                                AddComboboxItem(comboBoxReplay, TempList.FSNAME, TempList.FSLAYEL);
                            }
                            if (TempList.FSNAME.IndexOf("直播") == 0) //代表為直播
                            {
                                AddComboboxItem(comboBoxLivePlay, TempList.FSNAME, TempList.FSLAYEL);
                            }
                            if (TempList.FSNAME.ToUpper().IndexOf("LIVE") == 0)  //代表為Live
                            {
                                AddComboboxItem(comboBoxLive, TempList.FSNAME, TempList.FSLAYEL);
                            }
                            if (TempList.FSNAME.IndexOf("錄影轉播") == 0) //代表為錄影轉播
                            {
                                AddComboboxItem(comboBoxRecordPlay, TempList.FSNAME, TempList.FSLAYEL);
                            }
                            if (TempList.FSNAME.IndexOf("時間") == 0) //代表為時間
                            {
                                AddComboboxItem(comboBoxTime, TempList.FSNAME, TempList.FSLAYEL);
                            }
                            if ((TempList.FSNAME.ToUpper().IndexOf("LOGO") == 0) ) //代表為LOGO
                            {
                                AddComboboxItem(comboBoxLogo, TempList.FSNAME, TempList.FSLAYEL);
                            }
                        }
                        autoCompleteBoxWait.ItemsSource = WaitList;
                        autoCompleteBoxContinue.ItemsSource = ContinueList;
                        autoCompleteBoxName.ItemsSource = NameList;
                        if (InLouthKeyString != "")
                        {
                            AnalyLouthKeyString(InLouthKeyString);
                        }
                    }
                }
            };
        }

        private void checkBoxDoubleLang_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void buttonSet_Click(object sender, RoutedEventArgs e)
        {
            SetLouthKey();
        }

    }
}

