﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.IO;
using System.Xml.Linq;
using System.Text;

namespace PTS_MAM3.PGM
{
    public partial class PGM140 : Page
    {
        public PGM140()
        {
            InitializeComponent();
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBZSIGNAL_ALL = new WSPGMSendSQL.SendSQLSoapClient();

            //string ReturnStr = "";

            StringBuilder sb1 = new StringBuilder();
            object ReturnXML = "";
            sb1.AppendLine("<Data>");
            sb1.AppendLine("</Data>");
            SP_Q_TBZSIGNAL_ALL.Do_QueryAsync("[SP_Q_TBPGM_PROG_EPG_ALL]", sb1.ToString(), ReturnXML);
            SP_Q_TBZSIGNAL_ALL.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {

                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);

                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            ComboBoxItem TempComboBoxItem = new ComboBoxItem();
                            TempComboBoxItem.Content = elem.Element("FSCHANNELNAME").Value.ToString();
                            TempComboBoxItem.Tag = elem.Element("FSCHANNEL_ID").Value.ToString() + " " + elem.Element("FS_OUTPUT_FILE_NAME").Value.ToString();
                            comboBoxChannel.Items.Add(TempComboBoxItem);

                            //TempSignalList.Add(elem.Element("FSSIGNALNAME").Value.ToString());
                        }
                        //SignalList = TempSignalList;
                    }
                    else
                    {
                    }
                }

            };
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void buttonPTSMOD_Click(object sender, RoutedEventArgs e)
        {
            if (ModuleClass.getModulePermission("P651001") == false)
            {
                MessageBox.Show("您沒有轉出主頻MOD節目表的權限");
                return;
            }

            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "txt" + "|*.txt";
            dialog.FilterIndex = 1;
            //dialog.GetType().GetMethod("set_DefaultFileName").Invoke(dialog, new object[] { "13.txt" });
            
            dialog.ShowDialog();
            BusyMsg.IsBusy = true;
            TransProgList.WebService1SoapClient ExportMODPlayList = new TransProgList.WebService1SoapClient();
            //if (AAA.TransProglistAsync("N", "2010/12/16") = "ok")

            ExportMODPlayList.ExportModProglistAsync("51");

            ExportMODPlayList.ExportModProglistCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {
                        WCF100DL.WCF100Client WCF100client = new WCF100DL.WCF100Client();


                        string MODFileID = "13.txt";

                        WCF100client.DownloadAsync(MODFileID);
                        //WCF100client.DownloadAsync(args.Result);
                        WCF100client.DownloadCompleted += (s1, args1) =>
                        {
                            if (args1.Error != null)
                            {
                                MessageBox.Show(args1.Error.ToString());
                                return;
                            }

                            if (args1.Result != null)
                            {
                                try
                                {

                                    Stream fileobj = dialog.OpenFile();
                                    if (fileobj == null)
                                    {
                                    }
                                    else
                                    {
                                        fileobj.Write(args1.Result.File, 0, args1.Result.File.Length);
                                        fileobj.Close();
                                        BusyMsg.IsBusy = false;
                                        MessageBox.Show("匯出節目表成功");

                                    }
                                }
                                catch (Exception ex)
                                {
                                    BusyMsg.IsBusy = false;
                                    return;
                                }
                            }
                        };

                      
                    }
                }
                BusyMsg.IsBusy = false;
            };
          
        }

        private void buttonPTSHDMOD_Click(object sender, RoutedEventArgs e)
        {
            if (ModuleClass.getModulePermission("P661001") == false)
            {
                MessageBox.Show("您沒有轉出HD-MOD節目表的權限");
                return;
            }
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "txt" + "|*.txt";
            dialog.FilterIndex = 1;
            //dialog.GetType().GetMethod("set_DefaultFileName").Invoke(dialog, new object[] { "14.txt" });

            dialog.ShowDialog();
            BusyMsg.IsBusy = true;
            TransProgList.WebService1SoapClient ExportMODPlayList = new TransProgList.WebService1SoapClient();
            //if (AAA.TransProglistAsync("N", "2010/12/16") = "ok")

            ExportMODPlayList.ExportModProglistAsync("61");

            ExportMODPlayList.ExportModProglistCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {
                        WCF100DL.WCF100Client WCF100client = new WCF100DL.WCF100Client();


                        string MODFileID = "14.txt";

                        WCF100client.DownloadAsync(MODFileID);
                        //WCF100client.DownloadAsync(args.Result);
                        WCF100client.DownloadCompleted += (s1, args1) =>
                        {
                            if (args1.Error != null)
                            {
                                MessageBox.Show(args1.Error.ToString());
                                return;
                            }

                            if (args1.Result != null)
                            {
                                try
                                {

                                    Stream fileobj = dialog.OpenFile();
                                    if (fileobj == null)
                                    {
                                    }
                                    else
                                    {
                                        fileobj.Write(args1.Result.File, 0, args1.Result.File.Length);
                                        fileobj.Close();
                                        BusyMsg.IsBusy = false;
                                        MessageBox.Show("匯出節目表成功");

                                    }
                                }
                                catch (Exception ex)
                                {
                                    BusyMsg.IsBusy = false;
                                    return;
                                }
                            }
                        };

                      
                    }
                }
                BusyMsg.IsBusy = false;
            };
           
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
           
            BusyMsg.IsBusy = true;
            TransProgList.WebService1SoapClient ExportMODPlayList = new TransProgList.WebService1SoapClient();
            //if (AAA.TransProglistAsync("N", "2010/12/16") = "ok")

            ExportMODPlayList.ExportModProglistAsync("51Y");

            ExportMODPlayList.ExportModProglistCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {
                        if (args.Result == "ok")
                            MessageBox.Show("匯出節目表成功");
                        else
                            MessageBox.Show("匯出節目表發生錯誤,原因為:" + args.Result);
                    }
                }
                BusyMsg.IsBusy = false;
            };
        }

        private void buttonExportEPG_Click(object sender, RoutedEventArgs e)
        {
            string _ChannelID;
            string _ChannelName;
            string _ChannelFileName;
            if (comboBoxChannel.SelectedIndex != -1)
            {
                string[] ChannelType = ((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString().Split(new Char[] { ' ' });
                _ChannelFileName = ChannelType[1];
                _ChannelID = ChannelType[0];
                _ChannelName = ((ComboBoxItem)comboBoxChannel.SelectedItem).Content.ToString();

                if (ModuleClass.getModulePermission("P6" + _ChannelID + "001") == false)
                {
                    MessageBox.Show("您沒有轉出此頻道EPG節目表的權限");
                    return;
                }

                SaveFileDialog dialog = new SaveFileDialog();
                dialog.Filter = _ChannelFileName + "|*.txt";
                dialog.FilterIndex = 1;
                //dialog.GetType().GetMethod("set_DefaultFileName").Invoke(dialog, new object[] { "13.txt" });
              
                dialog.ShowDialog();
                BusyMsg.IsBusy = true;
                TransProgList.WebService1SoapClient ExportMODPlayList = new TransProgList.WebService1SoapClient();
                //if (AAA.TransProglistAsync("N", "2010/12/16") = "ok")

                ExportMODPlayList.ExportModProglistAsync(_ChannelID);

                ExportMODPlayList.ExportModProglistCompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (args.Result != null && args.Result != "")
                        {
                            WCF100DL.WCF100Client WCF100client = new WCF100DL.WCF100Client();


                            string MODFileID = _ChannelFileName;

                            WCF100client.DownloadAsync(MODFileID);
                            //WCF100client.DownloadAsync(args.Result);
                            WCF100client.DownloadCompleted += (s1, args1) =>
                            {
                                if (args1.Error != null)
                                {
                                    MessageBox.Show(args1.Error.ToString());
                                    return;
                                }

                                if (args1.Result != null)
                                {
                                    try
                                    {

                                        Stream fileobj = dialog.OpenFile();
                                        if (fileobj == null)
                                        {
                                        }
                                        else
                                        {
                                            fileobj.Write(args1.Result.File, 0, args1.Result.File.Length);
                                            fileobj.Close();
                                            BusyMsg.IsBusy = false;
                                            MessageBox.Show("匯出節目表成功");

                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        BusyMsg.IsBusy = false;
                                        return;
                                    }
                                }
                            };


                        }
                    }
                    BusyMsg.IsBusy = false;
                };
            }

          
        }

    }
}
