﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.InteropServices.Automation;
using System.Windows.Browser;
using System.Text;
using System.Xml.Linq;

namespace PTS_MAM3
{
    public partial class SetLouthKey : Page
    {
        public SetLouthKey()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {


            int i = 0;
            if (checkBoxLivePlay.IsChecked == true)
            {
                i = i + 1;
            }
            if (checkBoxLive.IsChecked == true)
            {
                i = i + 1;
            }
            if (checkBoxRecordPlay.IsChecked == true)
            {
                i = i + 1;
            }
            if (i > 1)
            {
                MessageBox.Show("直播,Live,錄影直播不可同時勾選");

            }
            i = 0;
            if (checkBoxWait.IsChecked == true)
            {
                i = i + 1;
            }
            if (checkBoxContinue.IsChecked == true)
            {
                i = i + 1;
            }
            //if (checkBoxNextWeek.IsChecked == true)
            //{
            //    i = i + 1;
            //}

            if (i > 1)
            {
                MessageBox.Show("稍後,繼續收看不可同時勾選");
            }
            if (checkBoxDoubleLang.IsChecked == true)
            {
                if (comboBoxDoubleLang.SelectedIndex != -1)
                {
                    if (((ComboBoxItem)comboBoxDoubleLang.SelectedItem).Content.ToString() == "")
                    {
                        MessageBox.Show("請選擇雙語種類");
                        return;
                    }
                }
            }

            if (checkBoxStereo.IsChecked == true)
            {
                if (comboBoxStereo.SelectedIndex == -1)
                {
                    MessageBox.Show("請選擇立體聲種類");
                    return;
                }
            }

            if (checkBoxReplay.IsChecked == true)
            {
                if (comboBoxReplay.SelectedIndex == -1)
                {
                    MessageBox.Show("請選擇重播種類");
                    return;
                }
            }

            if (checkBoxDigital.IsChecked == true)
            {
                if (comboBoxDigital.SelectedIndex == -1)
                {
                    MessageBox.Show("請選擇數位同步播出種類");
                    return;
                }
            }

            if (checkBoxLivePlay.IsChecked == true)
            {
                if (comboBoxLivePlay.SelectedIndex == -1)
                {
                    MessageBox.Show("請選擇直播種類");
                    return;
                }
            }

            if (checkBoxLive.IsChecked == true)
            {
                if (comboBoxLive.SelectedIndex == -1)
                {
                    MessageBox.Show("請選擇Live種類");
                    return;
                }
            }

            if (checkBoxRecordPlay.IsChecked == true)
            {
                if (comboBoxRecordPlay.SelectedIndex == -1)
                {
                    MessageBox.Show("請選擇錄影轉播種類");
                    return;
                }
            }

            if (checkBoxTime.IsChecked == true)
            {
                if (comboBoxTime.SelectedIndex == -1)
                {
                    MessageBox.Show("請選擇時間種類");
                    return;
                }
            }

            if (checkBoxLogo.IsChecked == true)
            {
                if (comboBoxLogo.SelectedIndex == -1)
                {
                    MessageBox.Show("請選擇Logo種類");
                    return;
                }
            }
            if (checkBoxWait.IsChecked == true)
            {
                if (comboBoxWait.SelectedIndex == -1)
                {
                    MessageBox.Show("請選擇接著種類");
                    return;
                }
            }
            if (checkBoxContinue.IsChecked == true)
            {
                if (comboBoxContinue.SelectedIndex == -1)
                {
                    MessageBox.Show("請選擇繼續收看種類");
                    return;
                }
            }

            this.textBox1.Text = "";
            if (this.checkBoxDoubleLang.IsChecked == true)
            {
                this.textBox1.Text = this.textBox1.Text + "+" + ((ComboBoxItem)comboBoxDoubleLang.SelectedItem).Content.ToString();
                if (textBoxDoubleLangST.Text != "")
                {
                    this.textBox1.Text = this.textBox1.Text + "#" + textBoxDoubleLangST.Text;
                    if ((textBoxDoubleLangInterval.Text != "") && (textBoxDoubleLangDur.Text != ""))
                        this.textBox1.Text = this.textBox1.Text + "," + textBoxDoubleLangInterval.Text + "," + textBoxDoubleLangDur.Text;
                }
            }


            if (this.checkBoxName.IsChecked == true)
            {
                if (checkBoxNameAll.IsChecked == true)
                    this.textBox1.Text = this.textBox1.Text + "+片名(整段)";
                else
                    this.textBox1.Text = this.textBox1.Text + "+片名";

                if (textBoxNameST.Text != "")
                {
                    this.textBox1.Text = this.textBox1.Text + "#" + textBoxNameST.Text;
                    if ((textBoxNameInterval.Text != "") && (textBoxNameDur.Text != ""))
                        this.textBox1.Text = this.textBox1.Text + "," + textBoxNameInterval.Text + "," + textBoxNameDur.Text;
                }
            }

            if (this.checkBoxStereo.IsChecked == true)
            {
                this.textBox1.Text = this.textBox1.Text + "+" + ((ComboBoxItem)comboBoxStereo.SelectedItem).Content.ToString();
                if (textBoxStereoST.Text != "")
                {
                    this.textBox1.Text = this.textBox1.Text + "#" + textBoxStereoST.Text;
                    if ((textBoxStereoInterval.Text != "") && (textBoxStereoDur.Text != ""))
                        this.textBox1.Text = this.textBox1.Text + "," + textBoxStereoInterval.Text + "," + textBoxStereoDur.Text;
                }
            }

            if (this.checkBoxDigital.IsChecked == true)
            {
                this.textBox1.Text = this.textBox1.Text + "+" + ((ComboBoxItem)comboBoxDigital.SelectedItem).Content.ToString();
                if (textBoxDigitalST.Text != "")
                {
                    this.textBox1.Text = this.textBox1.Text + "#" + textBoxDigitalST.Text;
                    if ((textBoxDigitalInterval.Text != "") && (textBoxDigitalDur.Text != ""))
                        this.textBox1.Text = this.textBox1.Text + "," + textBoxDigitalInterval.Text + "," + textBoxDigitalDur.Text;
                }
            }
            if (this.checkBoxWait.IsChecked == true)
            {
                this.textBox1.Text = this.textBox1.Text + "+" + ((ComboBoxItem)comboBoxWait.SelectedItem).Content.ToString();
                if ((textBoxWaitEndPoint.Text != "") && (textBoxWaitDur.Text != ""))
                    this.textBox1.Text = this.textBox1.Text + "#" + textBoxWaitEndPoint.Text + "," + textBoxWaitDur.Text;
            }
            if (this.checkBoxContinue.IsChecked == true)
            {
                this.textBox1.Text = this.textBox1.Text + "+" + ((ComboBoxItem)comboBoxContinue.SelectedItem).Content.ToString();
                if ((textBoxContinueEndPoint.Text != "") && (textBoxContinueDur.Text != ""))
                    this.textBox1.Text = this.textBox1.Text + "#" + textBoxContinueEndPoint.Text + "," + textBoxContinueDur.Text;
            }


            if (this.checkBoxReplay.IsChecked == true)
            {
                this.textBox1.Text = this.textBox1.Text + "+" + ((ComboBoxItem)comboBoxReplay.SelectedItem).Content.ToString();
            }
            if (this.checkBoxLivePlay.IsChecked == true)
            {
                this.textBox1.Text = this.textBox1.Text + "+" + ((ComboBoxItem)comboBoxLivePlay.SelectedItem).Content.ToString();
            }

            if (this.checkBoxLive.IsChecked == true)
            {
                this.textBox1.Text = this.textBox1.Text + "+" + ((ComboBoxItem)comboBoxLive.SelectedItem).Content.ToString();
            }

            if (this.checkBoxRecordPlay.IsChecked == true)
            {
                this.textBox1.Text = this.textBox1.Text + "+" + ((ComboBoxItem)comboBoxRecordPlay.SelectedItem).Content.ToString();
            }

            if (this.checkBoxTime.IsChecked == true)
            {
                this.textBox1.Text = this.textBox1.Text + "+" + ((ComboBoxItem)comboBoxTime.SelectedItem).Content.ToString();
            }
            if (this.checkBoxLogo.IsChecked == true)
            {
                this.textBox1.Text = this.textBox1.Text + "+" + ((ComboBoxItem)comboBoxLogo.SelectedItem).Content.ToString();
            }


        }


        private void button2_Click_1(object sender, RoutedEventArgs e)
        {
            OpenFileDialog flDialog = new OpenFileDialog();
            flDialog.Filter = "Excel Files(*.xlsx)|*.xlsx|Excel Files(*.xls)|*.xls";

            bool res = (bool)flDialog.ShowDialog();
            List<LouthKeyList> source = new List<LouthKeyList>();
           
            //if (res)
            //{
                FileInfo fs = flDialog.File;

                string fileName = fs.Name;
                //MessageBox.Show(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\" + fs.Name);

                #region Reading Data From Excel File

                dynamic objExcel = AutomationFactory.CreateObject("Excel.Application");
                //MessageBox.Show(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\" + fileName);
                //Open the Workbook Here
                //dynamic objExcelWorkBook = objExcel.Workbooks.Open(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\" + fileName);
                dynamic objExcelWorkBook = objExcel.Workbooks.Open("D:\\PTS-片名編號表20110214.xls");
                //Read the Worksheet
                dynamic objActiveWorkSheet = objExcelWorkBook.ActiveSheet();
                //Cells to Read
                dynamic objCell_1, objCell_2;

                //Iterate through Cells
                for (int count = 2; count < 17; count++)
                {
                    MessageBox.Show(count.ToString());
                    objCell_1 = objActiveWorkSheet.Cells[count, 1];
                    objCell_2 = objActiveWorkSheet.Cells[count, 2];
                    source.Add(new LouthKeyList()
                    {
                        FSGROUP = objActiveWorkSheet.Cells[count, 1],
                        FSNAME = objActiveWorkSheet.Cells[count, 2],
                        FSNO = objActiveWorkSheet.Cells[count, 3],
                        FSMEMO = objActiveWorkSheet.Cells[count, 4],
                        FSLAYEL = objActiveWorkSheet.Cells[count, 5],
                        FSRULE = objActiveWorkSheet.Cells[count, 6]
                    });
                }

                this.dataGrid1.ItemsSource = source;

                #endregion
           // }

        }

        string[] TimeStatus;
        public void Trans(string Instring,TextBox TextBoxST,TextBox TextBoxInterval,TextBox TextBoxDur) 
        {
            TimeStatus = Instring.Split(new Char[] { ',' });
            if (Instring.IndexOf(",") > -1)//代表有 間隔,長度
            {
                TextBoxST.Text = TimeStatus[0];
                TextBoxInterval.Text = TimeStatus[1];
                TextBoxDur.Text = TimeStatus[2];
            }
            else
            {
                TextBoxST.Text = TimeStatus[0];
                TextBoxInterval.Text = "";
                TextBoxDur.Text = "";
            } 
        }

        public void Trans1(string Instring, TextBox TextBoxEndPoint,  TextBox TextBoxDur)
        {
            TimeStatus = Instring.Split(new Char[] { ',' });
            if (Instring.IndexOf(",") > -1)//代表有 間隔,長度
            {
                TextBoxEndPoint.Text = TimeStatus[0];
                TextBoxDur.Text = TimeStatus[1];
            }
            else
            {
                TextBoxEndPoint.Text = TimeStatus[0];
                TextBoxDur.Text = "";
            }
        }        
        public void ComboBoxCheckSelectItem(string Instring,ComboBox SelectComboBox,String SelectText) 
        {
            SelectComboBox.SelectedIndex = -1;
            for (int Combocount = 0; Combocount < SelectComboBox.Items.Count; Combocount++)
            {
                if (((ComboBoxItem)SelectComboBox.Items[Combocount]).Content.ToString() == SelectText)
                { 
                    SelectComboBox.SelectedIndex = Combocount;
                }
            }
            if (SelectComboBox.SelectedIndex == -1)
                MessageBox.Show("找不到對應的" + Instring + "設定");
        }

        
        private void button3_Click(object sender, RoutedEventArgs e)
        {
            AnalyLouthKeyString(textBox1.Text);
        }

        string myFileName = string.Empty;
        private void button4_Click(object sender, RoutedEventArgs e)
        {
            // 建立開啟檔案對話方塊的執行個體。
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.Filter = "Excel(.xls, .xlsx)|*.xls;*.xlsx|所有的檔案(*.*)|*.*";
            openFileDialog1.FilterIndex = 2;

            // 只能選取一個檔案
            openFileDialog1.Multiselect = false;

            // 呼叫 ShowDialog 方法來顯示出開啟檔案對話方塊。
            bool? userClickedOK = openFileDialog1.ShowDialog();

            // 如果使用者按下『確定』按鈕則繼續處理。
            if (userClickedOK.GetValueOrDefault() == true)
            {

                myFileName = openFileDialog1.File.Name;

                // 開啟使用者所選取的檔案以便加以讀取。
                using (System.IO.Stream fileStream = openFileDialog1.File.OpenRead())
                {

                    // 以下的程式碼會將所要上傳之檔案的資料流轉換成一個字串。
                    string myFileString = string.Empty;
                    byte[] myFileArray = new byte[System.Convert.ToInt32(fileStream.Length)];
                    fileStream.Read(myFileArray, 0, myFileArray.Length);

                    // Converts the value of an array of 8-bit unsigned integers to its equivalent string representation that is encoded with base-64 digits.
                    myFileString = Convert.ToBase64String(myFileArray);

                    // 取得泛型處理常式的絕對 URL 。
                    // GetUrl 是一個使用者自訂類別。
                    UriBuilder ub = new UriBuilder(GetUrl.GetAbsoluteUrl("TakeUploadString.ashx"));

                    // 替泛型處理常式的絕對 URL 加上查詢字串。
                    ub.Query = string.Format("FileName={0}", HttpUtility.UrlEncode(openFileDialog1.File.Name));

                    // 建立 WebClient 物件。
                    WebClient wc = new WebClient();
                    
                    wc.UploadStringCompleted += (s, args) =>
                    {
                        //WriteExcel.WriteExcelSoapClient WExcel = new WriteExcel.WriteExcelSoapClient();
                        ////'WExcel.ImportExcelAsync(HttpUtility.UrlEncode(openFileDialog1.File.Name));
                        //WExcel.ImportExcelAsync(openFileDialog1.File.Name);
                        
                        //WExcel.ImportExcelCompleted += (s1, args1) =>
                        //     {
                        //         if (args1.Error == null)
                        //         {

                        //             if (args1.Result == "ok")
                        //                 MessageBox.Show("新增成功");
                        //             else
                        //                 MessageBox.Show("新增失敗");


                        //         }
                        //     };           

                        //MessageBox.Show("檔案 " + myFileName + " 已經上傳完畢");
                    };

                    // 呼叫 UploadStringAsync 方法來上傳檔案。
                    wc.UploadStringAsync(ub.Uri, myFileString);

                }
            }
        }

        public string TransString(XElement Instring) 
        {
            try
            {
                Instring.Value.ToString();
                return Instring.Value.ToString();
            }
            catch
            {
                return "";   
            }
        }

        public void AddComboboxItem(ComboBox AddComboBox, String SelectText) 
        {
            ComboBoxItem TempComboboxItem = new ComboBoxItem();
            TempComboboxItem.Content = SelectText;
            AddComboBox.Items.Add(TempComboboxItem);
        }

        public void AnalyLouthKeyString(string InString)
        {

            string[] LouthKeys = InString.Split(new Char[] { '+' });

            foreach (string LouthKey in LouthKeys)
            {
                if (LouthKey.Trim() != "")
                {
                    string[] LouthKeyType = LouthKey.Split(new Char[] { '#' });
                    if (LouthKeyType[0] != "") //如果有設定LouthKey Ex:中/英#00:02:00,10,30
                    {
                        if (LouthKeyType[0].IndexOf("/") > -1) //有斜線代表為雙語
                        {
                            checkBoxDoubleLang.IsChecked = true;
                            ComboBoxCheckSelectItem("雙語", comboBoxDoubleLang, LouthKeyType[0]);

                            if (LouthKey.IndexOf("#") > -1)  //#00:02:00,10,30 //表示有設定起始時間,間隔,長度
                                Trans(LouthKeyType[1], textBoxDoubleLangST, textBoxDoubleLangInterval, textBoxDoubleLangDur);
                        }

                        if (LouthKeyType[0].IndexOf("片名") > -1) //代表為片名
                        {
                            checkBoxName.IsChecked = true;
                            if (LouthKeyType[0].IndexOf("片名(整段)") > -1) //代表為片名(整段)
                                this.checkBoxNameAll.IsChecked = true;
                            else
                                this.checkBoxNameAll.IsChecked = false;
                            if (LouthKey.IndexOf("#") > -1)  //#00:02:00,10,30 //表示有設定起始時間,間隔,長度
                                Trans(LouthKeyType[1], textBoxNameST, textBoxNameInterval, textBoxNameDur);
                        }

                        if (LouthKeyType[0].IndexOf("立體聲") > -1)  //代表為立體聲
                        {
                            checkBoxStereo.IsChecked = true;
                            ComboBoxCheckSelectItem("立體聲", comboBoxStereo, LouthKeyType[0]);

                            if (LouthKey.IndexOf("#") > -1)  //#00:02:00,10,30 //表示有設定起始時間,間隔,長度
                                Trans(LouthKeyType[1], textBoxStereoST, textBoxStereoInterval, textBoxStereoDur);
                        }

                        if (LouthKeyType[0].IndexOf("數位") > -1)  //代表為數位同步播出
                        {
                            checkBoxDigital.IsChecked = true;
                            ComboBoxCheckSelectItem("數位同步播出", comboBoxDigital, LouthKeyType[0]);

                            if (LouthKey.IndexOf("#") > -1)  //#00:02:00,10,30 //表示有設定起始時間,間隔,長度
                                Trans(LouthKeyType[1], textBoxDigitalST, textBoxDigitalInterval, textBoxDigitalDur);
                        }
                        if (LouthKeyType[0].IndexOf("接著") > -1)   //代表為接著
                        {
                            checkBoxWait.IsChecked = true;
                            ComboBoxCheckSelectItem("接著", comboBoxWait, LouthKeyType[0]);

                            if (LouthKey.IndexOf("#") > -1)  //#90,30 //表示有設定片尾前幾秒,長度
                                Trans1(LouthKeyType[1], textBoxWaitEndPoint, textBoxWaitDur);
                        }
                        //代表為繼續收看
                        if ((LouthKeyType[0].IndexOf("下週") > -1) || (LouthKeyType[0].IndexOf("明天") > -1) || (LouthKeyType[0].IndexOf("下次") > -1))
                        {
                            checkBoxContinue.IsChecked = true;
                            ComboBoxCheckSelectItem("繼續收看", comboBoxContinue, LouthKeyType[0]);

                            if (LouthKey.IndexOf("#") > -1)  //#90,30 //表示有設定片尾前幾秒,長度
                                Trans1(LouthKeyType[1], textBoxContinueEndPoint, textBoxContinueDur);
                        }

                        if (LouthKeyType[0].IndexOf("重播") > -1)  //代表為重播
                        {
                            checkBoxReplay.IsChecked = true;
                            ComboBoxCheckSelectItem("重播", comboBoxReplay, LouthKeyType[0]);
                        }
                        if (LouthKeyType[0].IndexOf("直播") > -1)  //代表為重播
                        {
                            checkBoxLivePlay.IsChecked = true;
                            ComboBoxCheckSelectItem("直播", comboBoxLivePlay, LouthKeyType[0]);
                        }
                        if (LouthKeyType[0].IndexOf("Live") > -1)  //代表為Live
                        {
                            checkBoxLive.IsChecked = true;
                            ComboBoxCheckSelectItem("Live", comboBoxLive, LouthKeyType[0]);
                        }
                        if (LouthKeyType[0].IndexOf("錄影轉播") > -1)  //代表為錄影轉播
                        {
                            checkBoxRecordPlay.IsChecked = true;
                            ComboBoxCheckSelectItem("錄影轉播", comboBoxRecordPlay, LouthKeyType[0]);
                        }
                        if (LouthKeyType[0].IndexOf("時間") > -1)  //代表為時間
                        {
                            checkBoxTime.IsChecked = true;
                            ComboBoxCheckSelectItem("時間", comboBoxTime, LouthKeyType[0]);
                        }
                        if (LouthKeyType[0].IndexOf("LOGO") > -1)  //代表為Logo
                        {
                            checkBoxLogo.IsChecked = true;
                            ComboBoxCheckSelectItem("LOGO", comboBoxLogo, LouthKeyType[0]);
                        }
                        if (LouthKeyType[0].IndexOf("LOSE") > -1)  //代表為Lose
                        {
                            checkBoxLose.IsChecked = true;
                        }
                    }

                }
            }
        }

        List<LouthKeyList> LouthKey = new List<LouthKeyList>();
        private void LayoutRoot_Loaded(object sender, RoutedEventArgs e)
        {
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_LOUTH_KEY = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            sb.AppendLine("</Data>");
            SP_Q_TBPGM_LOUTH_KEY.Do_QueryAsync("SP_Q_TBPGM_LOUTH_KEY", sb.ToString(), ReturnXML);
            //SP_Q_TBPGM_QUEUE.Do_QueryCompleted += new EventHandler<SendSQL.Do_QueryCompletedEventArgs>(SP_Q_TBPGM_QUEUE_Do_QueryCompleted);
            //AAA.Do_InsertAsync("", "", ReturnStr);
            SP_Q_TBPGM_LOUTH_KEY.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        LouthKey.Clear();
                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            LouthKey.Add(new LouthKeyList()
                            {
                                FSGROUP = TransString(elem.Element("FSGROUP")),
                                FSNAME = TransString(elem.Element("FSNAME")),
                                FSNO = TransString(elem.Element("FSNO")),

                                FSMEMO = TransString(elem.Element("FSMEMO")),
                                FSLAYEL = TransString(elem.Element("FSLAYEL")),
                                FSRULE = TransString(elem.Element("FSRULE"))
                            });
                        }
                        //要將資料寫入Combobox

                        foreach (LouthKeyList TempList in LouthKey)
                        {
                            if (TempList.FSNAME.IndexOf("/") > -1) //有斜線代表為雙語
                            {
                                AddComboboxItem(comboBoxDoubleLang, TempList.FSNAME);
                                //ComboBoxItem TempComboboxItem = new ComboBoxItem();
                                //TempComboboxItem.Content = TempList.FSNAME;
                                //comboBoxDoubleLang.Items.Add(TempComboboxItem);
                            }
                            if (TempList.FSNAME.IndexOf("片名") > -1) //代表為片名
                            {
                                //AddComboboxItem(comboBoxDoubleLang, TempList.FSNAME);
                            }
                            if (TempList.FSNAME.IndexOf("立體聲") > -1) //代表為立體聲
                            {
                                AddComboboxItem(comboBoxStereo, TempList.FSNAME);
                            }
                            if (TempList.FSNAME.IndexOf("數位") > -1) //代表為數位
                            {
                                AddComboboxItem(comboBoxDigital, TempList.FSNAME);
                            }
                            if (TempList.FSNAME.IndexOf("接著") > -1) //代表為接著
                            {
                                AddComboboxItem(comboBoxWait, TempList.FSNAME);
                            }
                            if ((TempList.FSNAME.IndexOf("下週") > -1) || (TempList.FSNAME.IndexOf("明天") > -1) || (TempList.FSNAME.IndexOf("下次") > -1)) //代表為繼續收看
                            {
                                AddComboboxItem(comboBoxContinue, TempList.FSNAME);
                            }
                            if (TempList.FSNAME.IndexOf("重播") > -1) //代表為重播
                            {
                                AddComboboxItem(comboBoxReplay, TempList.FSNAME);
                            }
                            if (TempList.FSNAME.IndexOf("直播") > -1) //代表為直播
                            {
                                AddComboboxItem(comboBoxLivePlay, TempList.FSNAME);
                            }
                            if ((TempList.FSNAME.IndexOf("Live") > -1) ||(TempList.FSNAME.IndexOf("LIVE") > -1)) //代表為Live
                            {
                                AddComboboxItem(comboBoxLive, TempList.FSNAME);
                            }
                            if (TempList.FSNAME.IndexOf("錄影轉播") > -1) //代表為錄影轉播
                            {
                                AddComboboxItem(comboBoxRecordPlay, TempList.FSNAME);
                            }
                            if (TempList.FSNAME.IndexOf("時間") > -1) //代表為時間
                            {
                                AddComboboxItem(comboBoxTime, TempList.FSNAME);
                            }
                            if ((TempList.FSNAME.IndexOf("LOGO") > -1) || (TempList.FSNAME.IndexOf("Logo") > -1)) //代表為LOGO
                            {
                                AddComboboxItem(comboBoxLogo, TempList.FSNAME);
                            }
                        }

                        //if (LouthKeyType[0].IndexOf("/") > -1) //有斜線代表為雙語
                        //{
                        //    checkBoxDoubleLang.IsChecked = true;
                        //    ComboBoxCheckSelectItem("雙語", comboBoxDoubleLang, LouthKeyType[0]);

                        //    if (LouthKey.IndexOf("#") > -1)  //#00:02:00,10,30 //表示有設定起始時間,間隔,長度
                        //        Trans(LouthKeyType[1], textBoxDoubleLangST, textBoxDoubleLangInterval, textBoxDoubleLangDur);
                        //}

                    }
                }
            };           
        }
         
    }

    public class GetUrl
    {
        public static string GetAbsoluteUrl(string strRelativePath)
        {
            if (string.IsNullOrEmpty(strRelativePath))
                return strRelativePath;

            string strFullUrl;
            if (strRelativePath.StartsWith("http:", StringComparison.OrdinalIgnoreCase)
              || strRelativePath.StartsWith("https:", StringComparison.OrdinalIgnoreCase)
              || strRelativePath.StartsWith("file:", StringComparison.OrdinalIgnoreCase)
              )
            {
                // 已經是絕對路徑。
                strFullUrl = strRelativePath;
            }
            else
            {
                // 是相對路徑，必須轉換成絕對路徑。
                strFullUrl = System.Windows.Application.Current.Host.Source.AbsoluteUri;
                if (strFullUrl.IndexOf("ClientBin") > 0)
                    strFullUrl = strFullUrl.Substring(0, strFullUrl.IndexOf("ClientBin")) + "DataSource/" + strRelativePath;
                else
                    strFullUrl = strFullUrl.Substring(0, strFullUrl.LastIndexOf("/") + 1) + "DataSource/" + strRelativePath;

                //strFullUrl = "http://localhost/PTS_MAM3.Web/DataSource/TakeUploadString.ashx";

            }

            return strFullUrl;
        }
    }
        /// <returns></returns>
       
 

}
    

