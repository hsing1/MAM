﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;

using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Globalization;
using System.Text;
using System.Xml.Linq;
using System.IO;
using PTS_MAM3.flowWebService;
using PTS_MAM3.FlowMAM;

namespace PTS_MAM3.PGM
{
    public partial class PGM110 : ChildWindow
    {
        public PGM110()
        {
            InitializeComponent();
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBZCHANNEL_ALL = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            sb.AppendLine("</Data>");
            SP_Q_TBZCHANNEL_ALL.Do_QueryAsync("[SP_Q_TBZCHANNEL_ALL]", sb.ToString(), ReturnXML);
            //SP_Q_TBPGM_QUEUE.Do_QueryCompleted += new EventHandler<SendSQL.Do_QueryCompletedEventArgs>(SP_Q_TBPGM_QUEUE_Do_QueryCompleted);
            //AAA.Do_InsertAsync("", "", ReturnStr);
            SP_Q_TBZCHANNEL_ALL.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {


                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);

                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {

                            //if (ModuleClass.getModulePermission("P3" + elem.Element("ID").Value.ToString() + "001") == true)
                            //{
                                ComboBoxItem TempComboBoxItem = new ComboBoxItem();
                                TempComboBoxItem.Content = elem.Element("NAME").Value.ToString();
                                TempComboBoxItem.Tag = elem.Element("ID").Value.ToString();

                                comboBoxChannel.Items.Add(TempComboBoxItem);

                            //}

                        }
                    }
                }
            };           

        }

        public PGM110(string strFSPROMO_ID, string strFSPROMO_NAME, string strFSDURATION)
        {
            #region 叔叔原本的行為
            InitializeComponent();
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBZCHANNEL_ALL = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            sb.AppendLine("</Data>");
            SP_Q_TBZCHANNEL_ALL.Do_QueryAsync("[SP_Q_TBZCHANNEL_ALL]", sb.ToString(), ReturnXML);
            //SP_Q_TBPGM_QUEUE.Do_QueryCompleted += new EventHandler<SendSQL.Do_QueryCompletedEventArgs>(SP_Q_TBPGM_QUEUE_Do_QueryCompleted);
            //AAA.Do_InsertAsync("", "", ReturnStr);
            SP_Q_TBZCHANNEL_ALL.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {


                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);

                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {

                            //if (ModuleClass.getModulePermission("P3" + elem.Element("ID").Value.ToString() + "001") == true)
                            //{
                            ComboBoxItem TempComboBoxItem = new ComboBoxItem();
                            TempComboBoxItem.Content = elem.Element("NAME").Value.ToString();
                            TempComboBoxItem.Tag = elem.Element("ID").Value.ToString();

                            comboBoxChannel.Items.Add(TempComboBoxItem);

                            //}

                        }
                    }
                }
            };
            #endregion
            textBoxPromoID.Text = strFSPROMO_ID;
            textBoxNewPromoName.Text = strFSPROMO_NAME;
            textBoxPromoLength.Text = strFSDURATION;
        }

        public string XElementToString(XElement INXElement)
        {
            string ReturnStr;
            try 
            {
                ReturnStr=INXElement.Value.ToString();
                return ReturnStr;
                
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        List<Promo_EFFECTIVE_ZONE> Promo_Zone_List = new List<Promo_EFFECTIVE_ZONE>();
        List<Promo_SCHEDULED> Promo_Scheduled_List = new List<Promo_SCHEDULED>();


        public void TransWeek(string InWeekString)
        { 
            if (InWeekString.Substring(0,1)=="1")
                checkBoxWeek1.IsChecked=true;
            else
                checkBoxWeek1.IsChecked=false;
            if (InWeekString.Substring(1, 1) == "1")
                checkBoxWeek2.IsChecked = true;
            else
                checkBoxWeek2.IsChecked = false;
            if (InWeekString.Substring(2, 1) == "1")
                checkBoxWeek3.IsChecked = true;
            else
                checkBoxWeek3.IsChecked = false;
            if (InWeekString.Substring(3, 1) == "1")
                checkBoxWeek4.IsChecked = true;
            else
                checkBoxWeek4.IsChecked = false;
            if (InWeekString.Substring(4, 1) == "1")
                checkBoxWeek5.IsChecked = true;
            else
                checkBoxWeek5.IsChecked = false;
            if (InWeekString.Substring(5, 1) == "1")
                checkBoxWeek6.IsChecked = true;
            else
                checkBoxWeek6.IsChecked = false;
            if (InWeekString.Substring(6, 1) == "1")
                checkBoxWeek7.IsChecked = true;
            else
                checkBoxWeek7.IsChecked = false;
        }

        public string WeekCheckBoxToString()
        {
            string sweek = "";
            if (checkBoxWeek1.IsChecked == true)
                sweek = sweek + "1";
            else
                sweek = sweek + "0";

            if (checkBoxWeek2.IsChecked == true)
                sweek = sweek + "1";
            else
                sweek = sweek + "0";

            if (checkBoxWeek3.IsChecked == true)
                sweek = sweek + "1";
            else
                sweek = sweek + "0";

            if (checkBoxWeek4.IsChecked == true)
                sweek = sweek + "1";
            else
                sweek = sweek + "0";

            if (checkBoxWeek5.IsChecked == true)
                sweek = sweek + "1";
            else
                sweek = sweek + "0";

            if (checkBoxWeek6.IsChecked == true)
                sweek = sweek + "1";
            else
                sweek = sweek + "0";

            if (checkBoxWeek7.IsChecked == true)
                sweek = sweek + "1";
            else
                sweek = sweek + "0";
            return sweek;
        }

        public bool CheckTime(string Instring)
        {
            int TempInt;
            if (int.TryParse(Instring, out TempInt) == false)
                return false;
            if (Instring.Length < 4)
                return false;
            
            if (int.Parse(Instring.Substring(2,2))>60)
                return false;
            return true;
        }

        private void checkBoxAllCheck_Click(object sender, RoutedEventArgs e)
        {
            checkBoxWeek1.IsChecked = checkBoxAllCheck.IsChecked;
            checkBoxWeek2.IsChecked = checkBoxAllCheck.IsChecked;
            checkBoxWeek3.IsChecked = checkBoxAllCheck.IsChecked;
            checkBoxWeek4.IsChecked = checkBoxAllCheck.IsChecked;
            checkBoxWeek5.IsChecked = checkBoxAllCheck.IsChecked;
            checkBoxWeek6.IsChecked = checkBoxAllCheck.IsChecked;
            checkBoxWeek7.IsChecked = checkBoxAllCheck.IsChecked;

        }

        private void buttonQueryPromo_Click(object sender, RoutedEventArgs e)
        {
            PGM100_06 PGM100_06_Frm = new PGM100_06();
            PGM100_06_Frm.Show();
            PGM100_06_Frm.Closed += (s, args) =>
            {
                if (PGM100_06_Frm.DataGridPromoList.SelectedIndex >= 0)
                {
                    textBoxPromoID.Text = PGM100_06_Frm.ReturnPromoData.FSPROMO_ID;
                    textBoxNewPromoName.Text = PGM100_06_Frm.ReturnPromoData.FSPROMO_NAME;
                    textBoxPromoLength.Text = PGM100_06_Frm.ReturnPromoData.FSDURATION;
                    //_PromoTypeName = PGM100_06_Frm.ReturnPromoData.FSPROMOTYPENAME;
                }
            };
        }
        public string TransTimeString()
        {
            string ReturnString="";
            if (comboBoxTimeType.SelectedValue == null)
            {
                return "";
            }
            if ( ((ComboBoxItem)comboBoxTimeType.SelectedItem).Tag.ToString() == "1")
            { //日期時間區間
                if ((this.datePickerBDate.Text != "") && (this.datePickerEDate.Text != ""))
                {
                    //if (this.textBoxBTime.Text != "" && this.textBoxETime.Text != "")
                    //{
                        ReturnString = this.datePickerBDate.Text + " " + this.textBoxBTime.Text + "-->" + this.datePickerEDate.Text + " " + this.textBoxETime.Text;
                    //}
                    //else
                    //{
                    //    ReturnString = this.datePickerBDate.Text + " 0000"+ "-->" + this.datePickerEDate.Text + " 2400"  ;
                    //}

                }
             
              
             
            }
            else if (((ComboBoxItem)comboBoxTimeType.SelectedItem).Tag.ToString() == "2")
            { //時間區間
                if (this.textBoxBTime.Text != "" && this.textBoxETime.Text != "")
                {
                    ReturnString = this.datePickerBDate.Text + "-->" + this.datePickerEDate.Text + " 的 " + this.textBoxBTime.Text + "-->" + this.textBoxETime.Text;
                }
                else
                {
                    ReturnString = this.datePickerBDate.Text + "-->" + this.datePickerEDate.Text ;
                }

            }
            return ReturnString;
        }

        private void buttonAdd_Click(object sender, RoutedEventArgs e)
        {
            if (comboBoxChannel.SelectedIndex < 0)
            {
                MessageBox.Show("請選擇頻道");
                return;
            }
            if (this.datePickerBDate.Text == "")
            {
                MessageBox.Show("請選擇起始日期");
                return;
            }
            if (this.datePickerEDate.Text == "")
            {
                MessageBox.Show("請選擇結束日期");
                return;
            }
            if (WeekCheckBoxToString() == "0000000")
            {
                MessageBox.Show("請至少要勾選一個星期選項");
                return;
            }
            if (this.textBoxBTime.Text!="" && this.textBoxETime.Text!="")
            {
                if (comboBoxTimeType.SelectedIndex < 0)
                {
                    MessageBox.Show("請選擇時間類型");
                    return;
                }
            }


            if ((this.textBoxBTime.Text == "" || this.textBoxETime.Text == ""))
            {
                if (this.textBoxBTime.Text == "" && this.textBoxETime.Text == "")
                { }
                else
                {
                    MessageBox.Show("請輸入起迄時間");
                    return;
                }
            }
            if (this.textBoxBTime.Text != "")
            {
                textBoxBTime.Text = "0000";
                //if (CheckTime(this.textBoxBTime.Text.Trim().ToString()) == false)
                //{
                //    MessageBox.Show("起迄時間格式有誤,請輸入4碼起始時間,EX 0600");
                //    return;
                //}
            }
            if (this.textBoxETime.Text != "")
            {
                if (CheckTime(this.textBoxETime.Text.Trim().ToString()) == false)
                {
                    MessageBox.Show("起迄時間格式有誤,請輸入4碼起始時間,EX 0600");
                    return;
                }
            }

            if (Convert.ToDateTime(datePickerEDate.Text) < Convert.ToDateTime(datePickerBDate.Text))
            {
                    MessageBox.Show("結束日期必須大於起始日期");
                    return;
            }




            if (textBoxETime.Text != "" && textBoxBTime.Text != "")
            {
                if (int.Parse(textBoxETime.Text) < int.Parse(textBoxBTime.Text))
                {
                    MessageBox.Show("結束時間必須大於起始時間");
                    return;
                }
            }
            foreach (Promo_EFFECTIVE_ZONE Temp_Promo_Zone_List in Promo_Zone_List)
            {
                //if (Temp_Promo_Zone_List.FSPROMO_ID == this.textBoxPromoID.Text && Temp_Promo_Zone_List.FSCHANNEL_ID == ((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString()
                //    && Temp_Promo_Zone_List.FDBEG_DATE == datePickerBDate.Text && Temp_Promo_Zone_List.FDEND_DATE == datePickerEDate.Text
                //     && Temp_Promo_Zone_List.FSBEG_TIME == textBoxBTime.Text && Temp_Promo_Zone_List.FSEND_TIME == textBoxETime.Text)
                //{
                if (Temp_Promo_Zone_List.FSCHANNEL_ID == ((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString()
                    && (Convert.ToDateTime(datePickerBDate.Text) <= Convert.ToDateTime(Temp_Promo_Zone_List.FDEND_DATE) && Convert.ToDateTime(datePickerEDate.Text) >= Convert.ToDateTime(Temp_Promo_Zone_List.FDBEG_DATE)))
                       {
                        MessageBox.Show("此有效區間與目前已經加入過的有效區間重複,不可重複加入");
                    return;
                }
            }

            string _TempFCDATE_TIME_TYPE;
            if (comboBoxTimeType.SelectedIndex == -1)
                _TempFCDATE_TIME_TYPE = "";
            else
                _TempFCDATE_TIME_TYPE = ((ComboBoxItem)comboBoxTimeType.SelectedItem).Tag.ToString();
            
            Promo_Zone_List.Add(new Promo_EFFECTIVE_ZONE()
                {
                    FNPROMO_BOOKING_NO = "",
                    FSPROMO_ID = "",
                    FNNO = (Promo_Zone_List.Count + 1).ToString(),
                    FDBEG_DATE = datePickerBDate.Text,
                    FDEND_DATE = datePickerEDate.Text,
                    FSBEG_TIME = textBoxBTime.Text,
                    FSEND_TIME = textBoxETime.Text,
                    FCDATE_TIME_TYPE = _TempFCDATE_TIME_TYPE,
                    FSCHANNEL_ID = ((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString(),
                    FSCHANNEL_NAME = ((ComboBoxItem)comboBoxChannel.SelectedItem).Content.ToString(),
                    FSWEEK = WeekCheckBoxToString(),
                });
            dataGridZone.ItemsSource = null;
            dataGridZone.ItemsSource = Promo_Zone_List;
        }

        private void buttonRemove_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridZone.SelectedIndex < 0)
            {
                MessageBox.Show("請先選擇要移除的有效區間");
                return;
            }
            Promo_Zone_List.RemoveAt(dataGridZone.SelectedIndex);
            dataGridZone.ItemsSource = null;
            dataGridZone.ItemsSource = Promo_Zone_List;

        }

        public DateTime TransDate(string InDateString)
        { 
            DateTime parsed;
            IFormatProvider ifp = new CultureInfo("zh-TW");

            return DateTime.ParseExact(InDateString, "yyyy/MM/dd", ifp); 

            //DateTime.TryParseExact(InDateString, "yyyy/mm/dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out parsed);
            //return parsed;
        }

        private void buttonAddCheck_Click(object sender, RoutedEventArgs e)
        {
            if (Promo_Zone_List.Count <= 0)
            {
                MessageBox.Show("請先加入有效區間");
                return;
            }

            if (this.textBoxPromoID.Text == "")
            {
                MessageBox.Show("請先查詢短帶");
                return;
            }
            buttonAddCheck.IsEnabled = false;
            WSPGMSendSQL.SendSQLSoapClient SP_I_Promo_Booking = new WSPGMSendSQL.SendSQLSoapClient();
            StringBuilder sbBooking = new StringBuilder();
            StringBuilder sbPromoZone = new StringBuilder();
            StringBuilder sbBookingSETTING = new StringBuilder();


            sbBooking.AppendLine("<Data>");
            sbBooking.AppendLine("<FSPROMO_ID>" + textBoxPromoID.Text + "</FSPROMO_ID>");
            sbBooking.AppendLine("<FCSTATUS>" + "Y" + "</FCSTATUS>");
            sbBooking.AppendLine("<FSCREATED_BY>" + UserClass.userData.FSUSER_ID.ToString() + "</FSCREATED_BY>");
            sbBooking.AppendLine("<FSUPDATED_BY>" + "" + "</FSUPDATED_BY>");
            sbBooking.AppendLine("<FSMEMO>" + textBoxMemo.Text + "</FSMEMO>");
            sbBooking.AppendLine("</Data>");

            sbBookingSETTING.AppendLine("<Data>");
            sbBookingSETTING.AppendLine("<FSTIME1>" + textBoxTime1.Text + "</FSTIME1>");
            sbBookingSETTING.AppendLine("<FSTIME2>" + textBoxTime2.Text + "</FSTIME2>");
            sbBookingSETTING.AppendLine("<FSTIME3>" + textBoxTime3.Text + "</FSTIME3>");
            sbBookingSETTING.AppendLine("<FSTIME4>" + textBoxTime4.Text + "</FSTIME4>");
            sbBookingSETTING.AppendLine("<FSTIME5>" + textBoxTime5.Text + "</FSTIME5>");
            sbBookingSETTING.AppendLine("<FSTIME6>" + textBoxTime6.Text + "</FSTIME6>");
            sbBookingSETTING.AppendLine("<FSTIME7>" + textBoxTime7.Text + "</FSTIME7>");
            sbBookingSETTING.AppendLine("<FSTIME8>" + textBoxTime8.Text + "</FSTIME8>");
            sbBookingSETTING.AppendLine("<FSCMNO>" + "" + "</FSCMNO>");
            sbBookingSETTING.AppendLine("</Data>");

            int ZoneNO=1;
            sbPromoZone.AppendLine("<Datas>");
            foreach (Promo_EFFECTIVE_ZONE TempZone in Promo_Zone_List)
            {
                sbPromoZone.AppendLine("<Data>");
                sbPromoZone.AppendLine("<FSPROMO_ID>" + textBoxPromoID.Text + "</FSPROMO_ID>");
                sbPromoZone.AppendLine("<FNNO>" + ZoneNO.ToString() + "</FNNO>");
                ZoneNO=ZoneNO+1;
                sbPromoZone.AppendLine("<FDBEG_DATE>" + TempZone.FDBEG_DATE + "</FDBEG_DATE>");
                sbPromoZone.AppendLine("<FDEND_DATE>" + TempZone.FDEND_DATE + "</FDEND_DATE>");
                sbPromoZone.AppendLine("<FSBEG_TIME>" + TempZone.FSBEG_TIME + "</FSBEG_TIME>");
                sbPromoZone.AppendLine("<FSEND_TIME>" + TempZone.FSEND_TIME + "</FSEND_TIME>");
                sbPromoZone.AppendLine("<FCDATE_TIME_TYPE>" + TempZone.FCDATE_TIME_TYPE + "</FCDATE_TIME_TYPE>");
                sbPromoZone.AppendLine("<FSCHANNEL_ID>" + TempZone.FSCHANNEL_ID + "</FSCHANNEL_ID>");
                sbPromoZone.AppendLine("<FSWEEK>" + TempZone.FSWEEK + "</FSWEEK>");
                sbPromoZone.AppendLine("<FSCREATED_BY>" +  UserClass.userData.FSUSER_ID.ToString() + "</FSCREATED_BY>");
                sbPromoZone.AppendLine("<FSUPDATED_BY>" + "" + "</FSUPDATED_BY>");
                sbPromoZone.AppendLine("</Data>");
            }
            sbPromoZone.AppendLine("</Datas>");

            
            
            //sb=sb.Replace("\r\n", "");

            SP_I_Promo_Booking.Do_Insert_Promo_BookingAsync("", "", sbBooking.ToString(), "", sbPromoZone.ToString(), sbBookingSETTING.ToString());
            SP_I_Promo_Booking.Do_Insert_Promo_BookingCompleted += (s, args) =>
            {
                if (args.Error == null)
                {

                    if (args.Result != "")
                    {
                        _ReMaxBookingID = args.Result;
                        MessageBox.Show("新增成功");
                        //try
                        //    {
                        //        newAFlow();
                        //        if (_ReMaxBookingID != "")
                        //        {
                        //            MessageBox.Show("建立完成");
                        //        }
                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        MessageBox.Show("表單送出失敗,原因:"+ ex.Message);
                        //    }
                    }
                    else
                        MessageBox.Show("新增資料失敗");
                }
            };

            Promo_Zone_List.Clear();
            //Promo_Scheduled_List.Clear();
            textBoxPromoID.Text = "";
            textBoxPromoLength.Text = "";
            textBoxMemo.Text = "";
            textBoxNewPromoName.Text="";

            //dataGridZone.ItemsSource = null;

            //dataGridZone.ItemsSource = Promo_Zone_List;

            buttonAddCheck.IsEnabled = true;

        }

        string _ReMaxBookingID="";
        

        private void newAFlow()
        {
            FlowMAMSoapClient FlowClinet = new FlowMAMSoapClient();
            //FlowClinet.CallFlow_NewFlowWithFieldCompleted +=new EventHandler<CallFlow_NewFlowWithFieldCompletedEventArgs>(FlowClinet_CallFlow_NewFlowWithFieldCompleted);
            //flowWebService.FlowSoapClient FlowClinet = new flowWebService.FlowSoapClient();
            //FlowClinet.NewFlowWithFieldCompleted += new EventHandler<flowWebService.NewFlowWithFieldCompletedEventArgs>(FlowClinet_NewFlowWithFieldCompleted);
            string hid_SendTo_value = "1";
            StringBuilder sb = new StringBuilder();

            sb.Append(@"<VariableCollection>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>FormId</name>");
            sb.Append(@"    <value>" + _ReMaxBookingID + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>NextXamlName</name>");
            sb.Append(@"    <value>/PGM/PGM110_01.xaml</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>hid_SendTo</name>");
            sb.Append(@"    <value>" + "1" + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>FormInfo</name>");
            sb.Append(@"    <value>" + "[" + textBoxNewPromoName.Text + "]託播單" + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>CancelForm</name>");
            sb.Append(@"    <value>CanCancel</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>Function01</name>");
            sb.Append(@"    <value>" + "0" + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"</VariableCollection>");//hid_SendTo
             FlowClinet.CallFlow_NewFlowWithFieldAsync(9, UserClass.userData.FSUSER_ID, sb.ToString());
             FlowClinet.CallFlow_NewFlowWithFieldCompleted += (s1, args1) =>
             {
                 //MessageBox.Show("審核成功!");
                 if (args1.Error == null)
                 {
                     //MessageBox.Show("審核成功!");
                 }
                 else
                 {
                     MessageBox.Show("審核失敗!");
                 }
                 this.DialogResult = true;
             };             
            
        }


        void FlowClinet_NewFlowWithFieldCompleted(object sender, flowWebService.NewFlowWithFieldCompletedEventArgs e)
        {
            MessageBox.Show("表單已送出");
            
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            
            WSPGMSendSQL.SendSQLSoapClient SP_I_TBPGM_TEMP = new WSPGMSendSQL.SendSQLSoapClient();
            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";

                    sb.AppendLine("<Data>");
                    sb.AppendLine("<FSID>" + "1" + "</FSID>");
                    sb.AppendLine("<FSNAME>" + TransferTimecode.ReplaceXML(@"<>""'&") + "</FSNAME>");
                    sb.AppendLine("</Data>");


            //sb=sb.Replace("\r\n", "");
                    SP_I_TBPGM_TEMP.Do_InsertAsync("SP_I_TBPGM_TEMP", sb.ToString());
                    SP_I_TBPGM_TEMP.Do_InsertCompleted += (s, args) =>
            {
                if (args.Error == null)
                {

                    if (args.Result == true)
                        MessageBox.Show("新增成功");
                    else
                        MessageBox.Show("新增失敗");
                }
            };           
            

           
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_QUEUE = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            sb.AppendLine("</Data>");
            SP_Q_TBPGM_QUEUE.Do_QueryAsync("SP_Q_TBPGM_TEMP", sb.ToString(), ReturnXML);
            //SP_Q_TBPGM_QUEUE.Do_QueryCompleted += new EventHandler<SendSQL.Do_QueryCompletedEventArgs>(SP_Q_TBPGM_QUEUE_Do_QueryCompleted);
            //AAA.Do_InsertAsync("", "", ReturnStr);
            SP_Q_TBPGM_QUEUE.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {

                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            this.textBoxBTime.Text = elem.Element("FSID").Value.ToString();
                            this.textBoxETime.Text= elem.Element("FSNAME").Value.ToString();
                        }



                    }
                }
            };           
        }

        private void buttonInsertPromo_Click(object sender, RoutedEventArgs e)
        {
            PTS_MAM3.PRG.PRG800_01 PRG800_01_Add_frm = new PTS_MAM3.PRG.PRG800_01();
            PRG800_01_Add_frm.Show();

            PRG800_01_Add_frm.Closing += (s, args) =>
            {
                if (PRG800_01_Add_frm.DialogResult == true)
                {
                    //MessageBox.Show(PRG800_01_Add_frm.m_Queryobj.FSPROMO_NAME + PRG800_01_Add_frm.m_Queryobj.FSPROMO_NAME);
                    WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_PROMO = new WSPGMSendSQL.SendSQLSoapClient();
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("<Data>");
                    sb.AppendLine("<FSPROMO_NAME>" + TransferTimecode.ReplaceXML(PRG800_01_Add_frm.m_Queryobj.FSPROMO_NAME) + "</FSPROMO_NAME>");
                    sb.AppendLine("</Data>");

                    SP_Q_TBPGM_PROMO.Do_QueryAsync("SP_Q_TBPGM_PROMO_ALLNAME", sb.ToString());
                    SP_Q_TBPGM_PROMO.Do_QueryCompleted += (s1, args1) =>
                    {
                        if (args1.Error == null)
                        {
                            if (args1.Result != null && args1.Result != "")
                            {

                                byte[] byteArray = Encoding.Unicode.GetBytes(args1.Result);
                                StringBuilder output = new StringBuilder();
                                // Create an XmlReader
                                XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                                var ltox = from str in doc.Elements("Datas").Elements("Data")
                                           select str;
                                foreach (XElement elem in ltox)
                                {
                                    textBoxPromoID.Text = XElementToString(elem.Element("FSPROMO_ID"));
                                    textBoxNewPromoName.Text = XElementToString(elem.Element("FSPROMO_NAME"));
                                    textBoxPromoLength.Text = XElementToString(elem.Element("FSDURATION"));
                                }
                            }
                        }


                    };


                }
            };
        }

        private void textBox3_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void comboBoxTimeType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            labelTimeMessage.Content =TransTimeString();
        }

        private void datePickerBDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            labelTimeMessage.Content = TransTimeString();
        }

        private void datePickerEDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            labelTimeMessage.Content = TransTimeString();
        }

        private void textBoxBTime_TextChanged(object sender, TextChangedEventArgs e)
        {
            labelTimeMessage.Content = TransTimeString();
        }

        private void textBoxETime_TextChanged(object sender, TextChangedEventArgs e)
        {
            labelTimeMessage.Content = TransTimeString();
        }
    }
}
