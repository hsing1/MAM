﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using System.Xml.Linq;
using System.IO;
using System.Windows.Browser;


namespace PTS_MAM3.PGM
{
    public partial class PGM100_09 : ChildWindow
    {
        public PGM100_09()
        {
            InitializeComponent();
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBZCHANNEL_ALL = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            sb.AppendLine("</Data>");
            SP_Q_TBZCHANNEL_ALL.Do_QueryAsync("[SP_Q_TBZCHANNEL_ALL]", sb.ToString(), ReturnXML);
            //SP_Q_TBPGM_QUEUE.Do_QueryCompleted += new EventHandler<SendSQL.Do_QueryCompletedEventArgs>(SP_Q_TBPGM_QUEUE_Do_QueryCompleted);
            //AAA.Do_InsertAsync("", "", ReturnStr);
            SP_Q_TBZCHANNEL_ALL.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {


                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);

                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {

                            //if (ModuleClass.getModulePermission("P3" + elem.Element("ID").Value.ToString() + "001") == true)
                            //{
                            ComboBoxItem TempComboBoxItem = new ComboBoxItem();
                            TempComboBoxItem.Content = elem.Element("NAME").Value.ToString();
                            TempComboBoxItem.Tag = elem.Element("ID").Value.ToString();

                            comboBoxChannel.Items.Add(TempComboBoxItem);

                            //}

                        }
                    }
                }
            };      
        }


        public bool CheckData()
        {
            if (this.datePickerBDate.Text == "")
            {
                MessageBox.Show("請選擇起始日期");
                return false;
            }
            if (this.datePickerEDate.Text == "")
            {
                MessageBox.Show("請選擇結束日期");
                return false;
            }
            if (comboBoxChannel.SelectedIndex < 0)
            {
                MessageBox.Show("請選擇頻道");
                return false;
            }
            return true;
        }

        public void QueryData()
        {
            WSPGMSendSQL.SendSQLSoapClient SP_Q_GET_QUEUE_ZONE_NO_TAPE_LIST = new WSPGMSendSQL.SendSQLSoapClient();

            SP_Q_GET_QUEUE_ZONE_NO_TAPE_LIST.GET_Queue_Zone_NO_Tape_ListAsync(datePickerBDate.Text, datePickerEDate.Text, ((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString(), UserClass.userData.FSUSER_ChtName, ((ComboBoxItem)comboBoxChannel.SelectedItem).Content.ToString());

            SP_Q_GET_QUEUE_ZONE_NO_TAPE_LIST.GET_Queue_Zone_NO_Tape_ListCompleted += (s, args) =>
            {
                #region 查詢片庫所需調帶清單
                if (args.Error == null)
                {
                    #region 片庫所需調帶清單
                    if (args.Result != null && args.Result != "")
                    {
                        if (args.Result.ToString().StartsWith("錯誤") || args.Result.ToString().StartsWith("Error"))
                            MessageBox.Show(args.Result.ToString(), "提示訊息", MessageBoxButton.OK);
                        else
                            HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + args.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,toolbar=0,statusbar=0,menubar=0,scrollbar=0,resizable=yes");

                    }
                    else  //如果沒有查到資料則不處理
                    {
                        MessageBox.Show("沒有查到片庫所需調帶清單");
                    }
                    #endregion

                }

                #endregion
            };
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (CheckData() == false)
            {
                return;
            }
            QueryData();
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }



    }
}

