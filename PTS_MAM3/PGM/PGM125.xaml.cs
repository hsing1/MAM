﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Text;
using System.Xml.Linq;
using System.IO;
using System.Windows.Browser;
using System.Windows.Resources;
using Telerik.Windows.Controls;
using PTS_MAM3.PreviewPlayer;
using PTS_MAM3.SR_WSTSM_GetFileInfo;
using System.Windows.Data;

namespace PTS_MAM3.PGM
{


    public partial class PGM125 : Page
    {
        //public List<string> TSignalList=new List<string>();
        public PGM125()
        {
            InitializeComponent();
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBZCHANNEL_ALL = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";
            this.canvasPromo.MouseLeftButtonDown += new MouseButtonEventHandler(canvasPromo_MouseLeftButtonDown);
            this.canvasPromo.MouseLeftButtonUp += new MouseButtonEventHandler(canvasPromo_MouseLeftButtonUp);
            this.canvasPromo.MouseMove += new MouseEventHandler(canvasPromo_MouseMove);


            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            sb.AppendLine("</Data>");
            SP_Q_TBZCHANNEL_ALL.Do_QueryAsync("[SP_Q_TBZCHANNEL_ALL]", sb.ToString(), ReturnXML);
            //SP_Q_TBPGM_QUEUE.Do_QueryCompleted += new EventHandler<SendSQL.Do_QueryCompletedEventArgs>(SP_Q_TBPGM_QUEUE_Do_QueryCompleted);
            //AAA.Do_InsertAsync("", "", ReturnStr);
            SP_Q_TBZCHANNEL_ALL.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {


                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);

                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            if (ModuleClass.getModulePermission("P5" + elem.Element("ID").Value.ToString() + "003") == true)
                            {
                                ComboBoxItem TempComboBoxItem = new ComboBoxItem();
                                TempComboBoxItem.Content = elem.Element("NAME").Value.ToString();
                                TempComboBoxItem.Tag = elem.Element("ID").Value.ToString() + "-" + elem.Element("TYPE").Value.ToString();

                                comboBoxChannel.Items.Add(TempComboBoxItem);
                            }

                        }
                    }
                }
            };

            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBZSIGNAL_ALL = new WSPGMSendSQL.SendSQLSoapClient();

            //string ReturnStr = "";

            StringBuilder sb1 = new StringBuilder();
            object ReturnXML1 = "";
            sb1.AppendLine("<Data>");
            sb1.AppendLine("</Data>");
            SP_Q_TBZSIGNAL_ALL.Do_QueryAsync("[SP_Q_TBZSIGNAL_ALL]", sb1.ToString(), ReturnXML);
            SP_Q_TBZSIGNAL_ALL.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {

                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);

                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            ComboBoxItem TempComboBoxItem = new ComboBoxItem();
                            TempComboBoxItem.Content = elem.Element("FSSIGNALNAME").Value.ToString();

                            comboBoxSignal1.Items.Add(TempComboBoxItem);

                            //TempSignalList.Add(elem.Element("FSSIGNALNAME").Value.ToString());
                        }
                        //SignalList = TempSignalList;
                    }
                    else
                    {
                    }
                }

            };

        }

        void canvasPromo_MouseMove(object sender, MouseEventArgs e)
        {
            if (isRectMouseCapture == true)
            {
                //this.CanvasTranslateTransform1.X = e.GetPosition(this).X - (ax + UC1.Width - canvasPromo.Width);
                double wwidth = Application.Current.Host.Content.ActualWidth;

                //this.CanvasTranslateTransform1.X = e.GetPosition(LayoutRoot).X - ax - wwidth + canvasPromo.Width;
                //this.CanvasTranslateTransform1.Y = e.GetPosition(LayoutRoot).Y - ay;

                this.CanvasTranslateTransform1.X = e.GetPosition(this).X - ax - wwidth + canvasPromo.Width;
                this.CanvasTranslateTransform1.Y = e.GetPosition(LayoutRoot).Y - ay;
            }
        }

        void canvasPromo_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.canvasPromo.ReleaseMouseCapture();
            isRectMouseCapture = false;
        }
        double ax = 0;
        double ay = 0;
        private Boolean isRectMouseCapture = false;
        void canvasPromo_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.canvasPromo.CaptureMouse();
            isRectMouseCapture = true;
            ax = e.GetPosition(canvasPromo).X;
            ay = e.GetPosition(canvasPromo).Y;
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void ComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i <= comboBoxSignal1.Items.Count - 1; i++)
            {
                string TestI = ((ComboBoxItem)comboBoxSignal1.Items[i]).Content.ToString();
                (sender as ComboBox).Items.Add(TestI);
            }
            (sender as ComboBox).IsDropDownOpen = true;
        }


        private void DataGrid_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
        }

        System.Windows.Threading.DispatcherTimer myDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
        string _ChannelType = "";
        string _ChannelID = "";
        string _ChannelName = "";
        string _Date = "";
        public InsPromoQueryPromo WorkPromo = new InsPromoQueryPromo();
        //public InsPromoQueryPromo CopyPromo = new InsPromoQueryPromo();
        List<InsPromoQueryPromo> CopyPromoList = new List<InsPromoQueryPromo>();

        List<ArrPromoList> PGM_ARR_PROMO = new List<ArrPromoList>();
        List<ArrPromoList> PGM_ARR_PROMO1 = new List<ArrPromoList>();
        List<ArrPromoList> PGM_ARR_PROMO2 = new List<ArrPromoList>();
        List<ArrPromoList> PGM_ARR_PROMO3 = new List<ArrPromoList>();
        List<ArrPromoList> PGM_ARR_PROMO4 = new List<ArrPromoList>();
        List<ArrPromoList> PGM_ARR_PROMO5 = new List<ArrPromoList>();
        List<ArrPromoList> PGM_ARR_PROMO6 = new List<ArrPromoList>();
        List<ArrPromoList> PGM_ARR_PROMO7 = new List<ArrPromoList>();
        List<ArrPromoList> PGM_ARR_PROMO8 = new List<ArrPromoList>();
        List<ArrPromoList> PGM_ARR_PROMO9 = new List<ArrPromoList>();
        List<ArrPromoList> PGM_ARR_PROMO10 = new List<ArrPromoList>();


        public ArrPromoList TransList(ArrPromoList inArrPromoList)
        {
            ArrPromoList RArrPromoList = new ArrPromoList();
            RArrPromoList.COMBINENO = inArrPromoList.COMBINENO;
            RArrPromoList.FCTIME_TYPE = inArrPromoList.FCTIME_TYPE;
            RArrPromoList.FSPROG_ID = inArrPromoList.FSPROG_ID;
            RArrPromoList.FNSEQNO = inArrPromoList.FNSEQNO;
            RArrPromoList.FSNAME = inArrPromoList.FSNAME;
            RArrPromoList.FSCHANNEL_ID = inArrPromoList.FSCHANNEL_ID;
            RArrPromoList.FSCHANNEL_NAME = inArrPromoList.FSCHANNEL_NAME;
            RArrPromoList.FSPLAY_TIME = inArrPromoList.FSPLAY_TIME;
            RArrPromoList.FSDURATION = inArrPromoList.FSDURATION;
            RArrPromoList.FNEPISODE = inArrPromoList.FNEPISODE;
            RArrPromoList.FNBREAK_NO = inArrPromoList.FNBREAK_NO;
            RArrPromoList.FNSUB_NO = inArrPromoList.FNSUB_NO;
            RArrPromoList.FSFILE_NO = inArrPromoList.FSFILE_NO;
            RArrPromoList.FSVIDEO_ID = inArrPromoList.FSVIDEO_ID;
            RArrPromoList.FSPROMO_ID = inArrPromoList.FSPROMO_ID;
            RArrPromoList.FDDATE = inArrPromoList.FDDATE;
            RArrPromoList.FNLIVE = inArrPromoList.FNLIVE;
            RArrPromoList.FCTYPE = inArrPromoList.FCTYPE;
            RArrPromoList.FSSTATUS = inArrPromoList.FSSTATUS;
            RArrPromoList.FSSIGNAL = inArrPromoList.FSSIGNAL;
            RArrPromoList.FSMEMO = inArrPromoList.FSMEMO;
            RArrPromoList.FSMEMO1 = inArrPromoList.FSMEMO1;
            RArrPromoList.FSPROG_NAME = inArrPromoList.FSPROG_NAME;
            RArrPromoList.RealFSPLAY_TIME = GetRealTImeCode(inArrPromoList.FSPLAY_TIME);

            return RArrPromoList;
        }

        private void Add_Copy_Promo()
        {
            CopyPromoList.Clear();
            
            //先將SelectItem加入CopyPromoList

            for (int i = 0;i<=dataGridProgList.SelectedItems.Count - 1; i++)
            {
                if (((ArrPromoList)dataGridProgList.SelectedItems[i]).FCTYPE == "P")
                {
                    InsPromoQueryPromo TempCopyPromo = new InsPromoQueryPromo();
                    TempCopyPromo.FSPROMO_ID = ((ArrPromoList)dataGridProgList.SelectedItems[i]).FSPROMO_ID;
                    TempCopyPromo.FSPROMO_NAME = ((ArrPromoList)dataGridProgList.SelectedItems[i]).FSNAME;
                    TempCopyPromo.FSPROMO_NAME_ENG = "";
                    TempCopyPromo.FSDURATION = ((ArrPromoList)dataGridProgList.SelectedItems[i]).FSDURATION;
                    TempCopyPromo.FSPROG_ID = "";
                    TempCopyPromo.FNEPISODE = "";
                    TempCopyPromo.FSPROMOTYPEID = "";
                    TempCopyPromo.FSPROGOBJID = "";
                    TempCopyPromo.FSPRDCENID = "";
                    TempCopyPromo.FSPRDYYMM = "";
                    TempCopyPromo.FSPROMOCATID = "";
                    TempCopyPromo.FSPROMOCATDID = "";
                    TempCopyPromo.FSPROMOVERID = "";
                    TempCopyPromo.FSMAIN_CHANNEL_ID = "";
                    TempCopyPromo.FSBEG_TIME = "";
                    TempCopyPromo.FSEND_TIME = "";
                    TempCopyPromo.FSFILE_NO = ((ArrPromoList)dataGridProgList.SelectedItems[i]).FSFILE_NO;
                    TempCopyPromo.FSVIDEO_ID = ((ArrPromoList)dataGridProgList.SelectedItems[i]).FSVIDEO_ID;
                    TempCopyPromo.FDBEG_DATE = "";
                    TempCopyPromo.FDEND_DATE = "";
                    TempCopyPromo.FSMEMO = ((ArrPromoList)dataGridProgList.SelectedItems[i]).FSMEMO;
                    TempCopyPromo.FSMEMO1 = ((ArrPromoList)dataGridProgList.SelectedItems[i]).FSMEMO1;
                    TempCopyPromo.FSSIGNAL = ((ArrPromoList)dataGridProgList.SelectedItems[i]).FSSIGNAL;
                    CopyPromoList.Add(TempCopyPromo);
                }
            }
            //for (int i = dataGridProgList.SelectedItems.Count - 1; i >= 0; i--)
            //{
            //     PGM_ARR_PROMO.Remove((ArrPromoList)dataGrid1.SelectedItems[i]);
               
            //}

            //CopyPromo.FSPROMO_ID = PGM_ARR_PROMO[this.dataGridProgList.SelectedIndex].FSPROMO_ID;
            //CopyPromo.FSPROMO_NAME = PGM_ARR_PROMO[this.dataGridProgList.SelectedIndex].FSNAME;
            //CopyPromo.FSPROMO_NAME_ENG = "";
            //CopyPromo.FSDURATION = PGM_ARR_PROMO[this.dataGridProgList.SelectedIndex].FSDURATION;
            //CopyPromo.FSPROG_ID = "";
            //CopyPromo.FNEPISODE = "";
            //CopyPromo.FSPROMOTYPEID = "";
            //CopyPromo.FSPROGOBJID = "";
            //CopyPromo.FSPRDCENID = "";
            //CopyPromo.FSPRDYYMM = "";
            //CopyPromo.FSPROMOCATID = "";
            //CopyPromo.FSPROMOCATDID = "";
            //CopyPromo.FSPROMOVERID = "";
            //CopyPromo.FSMAIN_CHANNEL_ID = "";
            //CopyPromo.FSBEG_TIME = "";
            //CopyPromo.FSEND_TIME = "";
            //CopyPromo.FSFILE_NO = PGM_ARR_PROMO[this.dataGridProgList.SelectedIndex].FSFILE_NO;
            //CopyPromo.FSVIDEO_ID = PGM_ARR_PROMO[this.dataGridProgList.SelectedIndex].FSVIDEO_ID;
            //CopyPromo.FDBEG_DATE = "";
            //CopyPromo.FDEND_DATE = "";
            //CopyPromo.FSMEMO = PGM_ARR_PROMO[this.dataGridProgList.SelectedIndex].FSMEMO;
            //CopyPromo.FSMEMO1 = PGM_ARR_PROMO[this.dataGridProgList.SelectedIndex].FSMEMO1;
            //CopyPromo.FSSIGNAL = PGM_ARR_PROMO[this.dataGridProgList.SelectedIndex].FSSIGNAL;
        }

        List<int> deleteindex = new List<int>();
        private void Del_Select_Promo()
        {
            //由下到上一筆一筆刪除
            deleteindex.Clear();
            Add_Work_Change();
            for (int i = dataGridProgList.SelectedItems.Count - 1; i >= 0; i--) //將要刪除的列加入deleteindex中
            {
                if (((ArrPromoList)dataGridProgList.SelectedItems[i]).FCTYPE == "P") //只有Promo要刪除
                {
                    int bbb = PGM_ARR_PROMO.IndexOf((ArrPromoList)dataGridProgList.SelectedItems[i]);
                    deleteindex.Add(bbb);


                   // PGM_ARR_PROMO.Remove((ArrPromoList)dataGridProgList.SelectedItems[i]);
                }

            }

            //依照deleteindex中的資料一一將資料移除
            for (int i = 0; i <= deleteindex.Count - 1; i++)
            {
                //PGM_ARR_PROMO.RemoveAt(deleteindex[i]);


                string DelProgID = PGM_ARR_PROMO[deleteindex[i]].FSPROG_ID;
                string DelSeqno = PGM_ARR_PROMO[deleteindex[i]].FNSEQNO;
                string DelBreakNO = PGM_ARR_PROMO[deleteindex[i]].FNBREAK_NO;
                string DelSubNO = PGM_ARR_PROMO[deleteindex[i]].FNSUB_NO;

                PGM_ARR_PROMO.RemoveAt(deleteindex[i]);
                int j = 0;
                int NowIndex = j;
                while (j < PGM_ARR_PROMO.Count - 1 && PGM_ARR_PROMO[j].FSPROG_ID == DelProgID && PGM_ARR_PROMO[j].FNSEQNO == DelSeqno && PGM_ARR_PROMO[j].FNBREAK_NO == DelBreakNO && int.Parse(PGM_ARR_PROMO[j].FNSUB_NO) > int.Parse(DelSubNO) && PGM_ARR_PROMO[j].FCTYPE == "P")
                {
                    //PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FNSUB_NO = (int.Parse(PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FNSUB_NO) - 1).ToString();
                    PGM_ARR_PROMO[j].FNSUB_NO = (int.Parse(PGM_ARR_PROMO[j].FNSUB_NO) - 1).ToString();
                    j = j + 1;
                }

                //string DelProgID = ((ArrPromoList)dataGridProgList.SelectedItems[i]).FSPROG_ID;
                //string DelSeqno = ((ArrPromoList)dataGridProgList.SelectedItems[i]).FNSEQNO;
                //string DelBreakNO = ((ArrPromoList)dataGridProgList.SelectedItems[i]).FNBREAK_NO;
                //string DelSubNO = ((ArrPromoList)dataGridProgList.SelectedItems[i]).FNSUB_NO;

                //PGM_ARR_PROMO.Remove((ArrPromoList)dataGridProgList.SelectedItems[i]);
                //int j = 0;
                //int NowIndex = j;
                //while (j < PGM_ARR_PROMO.Count - 1 && PGM_ARR_PROMO[j].FSPROG_ID == DelProgID && PGM_ARR_PROMO[j].FNSEQNO == DelSeqno && PGM_ARR_PROMO[j].FNBREAK_NO == DelBreakNO && int.Parse(PGM_ARR_PROMO[j].FNSUB_NO) > int.Parse(DelSubNO) && PGM_ARR_PROMO[j].FCTYPE == "P")
                //{
                //    //PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FNSUB_NO = (int.Parse(PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FNSUB_NO) - 1).ToString();
                //    PGM_ARR_PROMO[j].FNSUB_NO = (int.Parse(PGM_ARR_PROMO[j].FNSUB_NO) - 1).ToString();
                //    i = i + 1;
                //}
            }

            ReCalTimecode();
           
        }

        private void CopyPastePromo()
        {
            if (dataGridProgList.SelectedIndex < 0)
            {
                MessageBox.Show("必須先選擇要插入Promo的位置");
                return;
            }
            Add_Work_Change();
            
            int i = dataGridProgList.SelectedIndex;
            Datagridindex = dataGridProgList.SelectedIndex;

            foreach (InsPromoQueryPromo CopyPromo1 in CopyPromoList)
            {
                if (PGM_ARR_PROMO[Datagridindex].FCTYPE == "P") //如果所選位置是Promo
                {
                    ArrPromoList Temp_ArrPromo = new ArrPromoList();
                    Temp_ArrPromo.COMBINENO = "";
                    Temp_ArrPromo.FCTIME_TYPE = "A";
                    Temp_ArrPromo.FSPROG_ID = PGM_ARR_PROMO[Datagridindex].FSPROG_ID;
                    Temp_ArrPromo.FNSEQNO = PGM_ARR_PROMO[Datagridindex].FNSEQNO;
                    Temp_ArrPromo.FSNAME = CopyPromo1.FSPROMO_NAME;
                    Temp_ArrPromo.FSCHANNEL_ID = PGM_ARR_PROMO[Datagridindex].FSCHANNEL_ID;
                    Temp_ArrPromo.FSCHANNEL_NAME = PGM_ARR_PROMO[Datagridindex].FSCHANNEL_NAME;

                    Temp_ArrPromo.FSPLAY_TIME = PGM_ARR_PROMO[Datagridindex].FSPLAY_TIME;
                    Temp_ArrPromo.FSDURATION = CopyPromo1.FSDURATION;
                    Temp_ArrPromo.FNEPISODE = PGM_ARR_PROMO[Datagridindex].FNEPISODE;
                    Temp_ArrPromo.FNBREAK_NO = PGM_ARR_PROMO[Datagridindex].FNBREAK_NO;
                    //由於是promo,所以插入promo的支數是相同的,之後的PromosubNO要加一
                    Temp_ArrPromo.FNSUB_NO = PGM_ARR_PROMO[Datagridindex].FNSUB_NO;

                    Temp_ArrPromo.FSFILE_NO = CopyPromo1.FSFILE_NO;
                    Temp_ArrPromo.FSVIDEO_ID = CopyPromo1.FSVIDEO_ID;
                    Temp_ArrPromo.FSPROMO_ID = CopyPromo1.FSPROMO_ID;
                    Temp_ArrPromo.FDDATE = PGM_ARR_PROMO[Datagridindex].FDDATE;
                    Temp_ArrPromo.FNLIVE = "0"; //表示LIVE
                    Temp_ArrPromo.FCTYPE = "P"; //表示宣傳帶
                    Temp_ArrPromo.FSMEMO = CopyPromo1.FSMEMO;
                    Temp_ArrPromo.FSPROG_NAME = PGM_ARR_PROMO[Datagridindex].FSPROG_NAME;
                    Temp_ArrPromo.RealFSPLAY_TIME = GetRealTImeCode(Temp_ArrPromo.FSPLAY_TIME);
                    Temp_ArrPromo.FSSIGNAL = CopyPromo1.FSSIGNAL;
                    Temp_ArrPromo.FSMEMO1 = CopyPromo1.FSMEMO1;
                    PGM_ARR_PROMO.Insert(Datagridindex, Temp_ArrPromo);
                    //之後的PromosubNO要加一
                    i = Datagridindex + 1; //從下一筆開始
                    while (i < PGM_ARR_PROMO.Count - 1 && PGM_ARR_PROMO[i].FCTYPE == "P")
                    {
                        PGM_ARR_PROMO[i].FNSUB_NO = (int.Parse(PGM_ARR_PROMO[i].FNSUB_NO) + 1).ToString();
                        i = i + 1;
                    }
                }
                else if (PGM_ARR_PROMO[Datagridindex].FCTYPE == "G")//如果所選位置是節目則要看前一筆資料的訊息
                {
                    int j;
                    if (i == 0) //如果為第一筆資料,前面沒資料
                    {
                        MessageBox.Show("不能在此插入短帶");
                    }
                    else //如果不是第一段
                    {
                        j = Datagridindex - 1;
                        if (PGM_ARR_PROMO[j].FSPROG_ID != "")
                        {
                            ArrPromoList Temp_ArrPromo = new ArrPromoList();
                            Temp_ArrPromo.COMBINENO = "";
                            Temp_ArrPromo.FCTIME_TYPE = "A";
                            Temp_ArrPromo.FSPROG_ID = PGM_ARR_PROMO[j].FSPROG_ID;
                            Temp_ArrPromo.FNSEQNO = PGM_ARR_PROMO[j].FNSEQNO;
                            Temp_ArrPromo.FSNAME = CopyPromo1.FSPROMO_NAME;
                            Temp_ArrPromo.FSCHANNEL_ID = PGM_ARR_PROMO[j].FSCHANNEL_ID;
                            Temp_ArrPromo.FSCHANNEL_NAME = PGM_ARR_PROMO[j].FSCHANNEL_NAME;

                            if (PGM_ARR_PROMO[j].FSPLAY_TIME=="")
                                Temp_ArrPromo.FSPLAY_TIME = PGM_ARR_PROMO[Datagridindex].FSPLAY_TIME;
                            else
                                Temp_ArrPromo.FSPLAY_TIME = PGM_ARR_PROMO[j].FSPLAY_TIME;

                            Temp_ArrPromo.FSDURATION = CopyPromo1.FSDURATION;
                            Temp_ArrPromo.FNEPISODE = PGM_ARR_PROMO[j].FNEPISODE;
                            Temp_ArrPromo.FNBREAK_NO = PGM_ARR_PROMO[j].FNBREAK_NO;
                            //由於是promo,所以插入promo的支數是相同的,之後的PromosubNO要加一
                            if (PGM_ARR_PROMO[j].FCTYPE == "P") //如果前一筆為Promo則採用前一筆的支數+1
                                Temp_ArrPromo.FNSUB_NO = (int.Parse(PGM_ARR_PROMO[j].FNSUB_NO) + 1).ToString();
                            else //不然為第一支
                                Temp_ArrPromo.FNSUB_NO = "1";

                            Temp_ArrPromo.FSFILE_NO = CopyPromo1.FSFILE_NO;
                            Temp_ArrPromo.FSVIDEO_ID = CopyPromo1.FSVIDEO_ID;
                            Temp_ArrPromo.FSPROMO_ID = CopyPromo1.FSPROMO_ID;
                            Temp_ArrPromo.FDDATE = PGM_ARR_PROMO[j].FDDATE;
                            Temp_ArrPromo.FNLIVE = "0"; //表示非LIVE
                            Temp_ArrPromo.FCTYPE = "P"; //表示宣傳帶
                            Temp_ArrPromo.FSMEMO = CopyPromo1.FSMEMO;
                            Temp_ArrPromo.FSMEMO1 = CopyPromo1.FSMEMO1;
                            Temp_ArrPromo.FSSIGNAL = CopyPromo1.FSSIGNAL;
                            Temp_ArrPromo.FSPROG_NAME = PGM_ARR_PROMO[j].FSPROG_NAME;
                            PGM_ARR_PROMO.Insert(Datagridindex, Temp_ArrPromo);
                        }
                        else //如果插在節目標題上,那就是此節目第一支
                        {
                            ArrPromoList Temp_ArrPromo = new ArrPromoList();
                            j = Datagridindex;
                            Temp_ArrPromo.COMBINENO = "";
                            Temp_ArrPromo.FCTIME_TYPE = "A";
                            Temp_ArrPromo.FSPROG_ID = PGM_ARR_PROMO[j].FSPROG_ID;
                            Temp_ArrPromo.FNSEQNO = PGM_ARR_PROMO[j].FNSEQNO;
                            Temp_ArrPromo.FSNAME = CopyPromo1.FSPROMO_NAME;
                            Temp_ArrPromo.FSCHANNEL_ID = PGM_ARR_PROMO[j].FSCHANNEL_ID;
                            Temp_ArrPromo.FSCHANNEL_NAME = PGM_ARR_PROMO[j].FSCHANNEL_NAME;

                            Temp_ArrPromo.FSPLAY_TIME = PGM_ARR_PROMO[j].FSPLAY_TIME;
                            Temp_ArrPromo.FSDURATION = CopyPromo1.FSDURATION;
                            Temp_ArrPromo.FNEPISODE = PGM_ARR_PROMO[j].FNEPISODE;
                            Temp_ArrPromo.FNBREAK_NO = (int.Parse(PGM_ARR_PROMO[j].FNBREAK_NO) - 1).ToString();
                            //由於是promo,所以插入promo的支數是相同的,之後的PromosubNO要加一
                            Temp_ArrPromo.FNSUB_NO = "1";

                            Temp_ArrPromo.FSFILE_NO = CopyPromo1.FSFILE_NO;
                            Temp_ArrPromo.FSVIDEO_ID = CopyPromo1.FSVIDEO_ID;
                            Temp_ArrPromo.FSPROMO_ID = CopyPromo1.FSPROMO_ID;
                            Temp_ArrPromo.FDDATE = PGM_ARR_PROMO[j].FDDATE;
                            Temp_ArrPromo.FNLIVE = "0"; //表示非LIVE
                            Temp_ArrPromo.FCTYPE = "P"; //表示宣傳帶
                            Temp_ArrPromo.FSMEMO = CopyPromo1.FSMEMO;
                            Temp_ArrPromo.FSMEMO1 = CopyPromo1.FSMEMO1;
                            Temp_ArrPromo.FSSIGNAL = CopyPromo1.FSSIGNAL;
                            Temp_ArrPromo.FSPROG_NAME = PGM_ARR_PROMO[j].FSPROG_NAME;
                            PGM_ARR_PROMO.Insert(Datagridindex, Temp_ArrPromo);
                        }
                    }
                }
                else if (PGM_ARR_PROMO[Datagridindex].FCTYPE == "")//如果所選位置是節目抬頭則要看前一筆資料的訊息
                {
                    int j = Datagridindex - 1;
                    while (j > 0 && PGM_ARR_PROMO[j].FSPROG_ID == "") //先找到之前一筆的資料
                    {
                        j = j - 1;
                    }
                    if (j > 0)
                    {
                        ArrPromoList Temp_ArrPromo = new ArrPromoList();
                        Temp_ArrPromo.COMBINENO = "";
                        Temp_ArrPromo.FCTIME_TYPE = "A";
                        Temp_ArrPromo.FSPROG_ID = PGM_ARR_PROMO[j].FSPROG_ID;
                        Temp_ArrPromo.FNSEQNO = PGM_ARR_PROMO[j].FNSEQNO;
                        Temp_ArrPromo.FSNAME = CopyPromo1.FSPROMO_NAME;
                        Temp_ArrPromo.FSCHANNEL_ID = PGM_ARR_PROMO[j].FSCHANNEL_ID;
                        Temp_ArrPromo.FSCHANNEL_NAME = PGM_ARR_PROMO[j].FSCHANNEL_NAME;

                        Temp_ArrPromo.FSPLAY_TIME = PGM_ARR_PROMO[j].FSPLAY_TIME;
                        //Temp_ArrPromo.RealFSPLAY_TIME = g PGM_ARR_PROMO[j].FSPLAY_TIME;

                        Temp_ArrPromo.FSDURATION = CopyPromo1.FSDURATION;
                        Temp_ArrPromo.FNEPISODE = PGM_ARR_PROMO[j].FNEPISODE;
                        Temp_ArrPromo.FNBREAK_NO = PGM_ARR_PROMO[j].FNBREAK_NO;
                        //由於是promo,所以插入promo的支數是相同的,之後的PromosubNO要加一
                        if (PGM_ARR_PROMO[j].FCTYPE == "P")
                            Temp_ArrPromo.FNSUB_NO = (int.Parse(PGM_ARR_PROMO[j].FNSUB_NO) + 1).ToString();
                        else
                            Temp_ArrPromo.FNSUB_NO = "1";

                        Temp_ArrPromo.FSFILE_NO = CopyPromo1.FSFILE_NO;
                        Temp_ArrPromo.FSVIDEO_ID = CopyPromo1.FSVIDEO_ID;
                        Temp_ArrPromo.FSPROMO_ID = CopyPromo1.FSPROMO_ID;
                        Temp_ArrPromo.FDDATE = PGM_ARR_PROMO[j].FDDATE;
                        Temp_ArrPromo.FNLIVE = "0"; //表示非LIVE
                        Temp_ArrPromo.FCTYPE = "P"; //表示宣傳帶
                        Temp_ArrPromo.FSMEMO = CopyPromo1.FSMEMO;
                        Temp_ArrPromo.FSMEMO1 = CopyPromo1.FSMEMO1;
                        Temp_ArrPromo.FSSIGNAL = CopyPromo1.FSSIGNAL;
                        Temp_ArrPromo.FSPROG_NAME = PGM_ARR_PROMO[j].FSPROG_NAME;
                        PGM_ARR_PROMO.Insert(Datagridindex, Temp_ArrPromo);
                        //因為一定是最後一支所以不用修改之後的支數
                    }
                }
                Datagridindex = Datagridindex +1;
            }


            ReCalTimecode();
            dataGridProgList.SelectedIndex = Datagridindex + 1;
        }


        private void Add_Work_Change()
        {

            PGM_ARR_PROMO10.Clear();
            foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO9)
            {
                PGM_ARR_PROMO10.Add(TransList(TemArrPromoList));
            }
            PGM_ARR_PROMO9.Clear();
            foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO8)
            {
                PGM_ARR_PROMO9.Add(TransList(TemArrPromoList));
            }
            PGM_ARR_PROMO8.Clear();
            foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO7)
            {
                PGM_ARR_PROMO8.Add(TransList(TemArrPromoList));
            }
            PGM_ARR_PROMO7.Clear();
            foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO6)
            {
                PGM_ARR_PROMO7.Add(TransList(TemArrPromoList));
            }
            PGM_ARR_PROMO6.Clear();
            foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO5)
            {
                PGM_ARR_PROMO6.Add(TransList(TemArrPromoList));
            }
            PGM_ARR_PROMO5.Clear();
            foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO4)
            {
                PGM_ARR_PROMO5.Add(TransList(TemArrPromoList));
            }
            PGM_ARR_PROMO4.Clear();
            foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO3)
            {
                PGM_ARR_PROMO4.Add(TransList(TemArrPromoList));
            }
            PGM_ARR_PROMO3.Clear();
            foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO2)
            {
                PGM_ARR_PROMO3.Add(TransList(TemArrPromoList));
            }
            PGM_ARR_PROMO2.Clear();
            foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO1)
            {
                PGM_ARR_PROMO2.Add(TransList(TemArrPromoList));
            }
            PGM_ARR_PROMO1.Clear();
            foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO)
            {
                PGM_ARR_PROMO1.Add(TransList(TemArrPromoList));
            }

            if (myDispatcherTimer.IsEnabled == false)
                myDispatcherTimer.Start();

            //PGM_ARR_PROMO10 = PGM_ARR_PROMO9;
            //PGM_ARR_PROMO9 = PGM_ARR_PROMO8;
            //PGM_ARR_PROMO8 = PGM_ARR_PROMO7;
            //PGM_ARR_PROMO7 = PGM_ARR_PROMO6;
            //PGM_ARR_PROMO6 = PGM_ARR_PROMO5;
            //PGM_ARR_PROMO5 = PGM_ARR_PROMO4;
            //PGM_ARR_PROMO4 = PGM_ARR_PROMO3;
            //PGM_ARR_PROMO3 = PGM_ARR_PROMO2;
            //PGM_ARR_PROMO2 = PGM_ARR_PROMO1;
            //PGM_ARR_PROMO1 = PGM_ARR_PROMO;
        }

        private void Back_Work_Change()
        {
            PGM_ARR_PROMO.Clear();
            foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO1)
            {
                PGM_ARR_PROMO.Add(TransList(TemArrPromoList));
            }
            PGM_ARR_PROMO1.Clear();
            foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO2)
            {
                PGM_ARR_PROMO1.Add(TransList(TemArrPromoList));
            }
            PGM_ARR_PROMO2.Clear();
            foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO3)
            {
                PGM_ARR_PROMO2.Add(TransList(TemArrPromoList));
            }
            PGM_ARR_PROMO3.Clear();
            foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO4)
            {
                PGM_ARR_PROMO3.Add(TransList(TemArrPromoList));
            }
            PGM_ARR_PROMO4.Clear();
            foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO5)
            {
                PGM_ARR_PROMO4.Add(TransList(TemArrPromoList));
            }
            PGM_ARR_PROMO5.Clear();
            foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO6)
            {
                PGM_ARR_PROMO5.Add(TransList(TemArrPromoList));
            }
            PGM_ARR_PROMO6.Clear();
            foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO7)
            {
                PGM_ARR_PROMO6.Add(TransList(TemArrPromoList));
            }
            PGM_ARR_PROMO7.Clear();
            foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO8)
            {
                PGM_ARR_PROMO7.Add(TransList(TemArrPromoList));
            }
            PGM_ARR_PROMO8.Clear();
            foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO9)
            {
                PGM_ARR_PROMO8.Add(TransList(TemArrPromoList));
            }
            PGM_ARR_PROMO9.Clear();
            foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO10)
            {
                PGM_ARR_PROMO9.Add(TransList(TemArrPromoList));
            }

            PGM_ARR_PROMO10.Clear();
            if (myDispatcherTimer.IsEnabled == false)
                myDispatcherTimer.Start();
        }

        private void Clear_Back_Change()
        {
            //PGM_ARR_PROMO = null;
            PGM_ARR_PROMO1.Clear();
            PGM_ARR_PROMO2.Clear();
            PGM_ARR_PROMO3.Clear();
            PGM_ARR_PROMO4.Clear();
            PGM_ARR_PROMO5.Clear();
            PGM_ARR_PROMO6.Clear();
            PGM_ARR_PROMO7.Clear();
            PGM_ARR_PROMO8.Clear();
            PGM_ARR_PROMO9.Clear();
            PGM_ARR_PROMO10.Clear();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (comboBoxChannel.SelectedIndex == -1)
            {
                MessageBox.Show("請先選擇要查詢之頻道");
                return;
            }
            BusyMsg.IsBusy = true; 
            labelInsPromoID.Content = "";
            WSPGMSendSQL.SendSQLSoapClient SP_Q_Combin_Arr_Promo = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";

            //StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            //sb.AppendLine("<Data>");
            //sb.AppendLine("<FDDATE>" + this.textBoxDate.Text + "</FDDATE>");
            if (comboBoxChannel.SelectedIndex != -1)
            {
                string[] ChannelType = ((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString().Split(new Char[] { '-' });
                _ChannelType = ChannelType[1];
                _ChannelID = ChannelType[0];
                _ChannelName = ((ComboBoxItem)comboBoxChannel.SelectedItem).Content.ToString();
                _Date = this.DatePickerDate.Text;
            }
            //_Date = "2010/12/16";
            //_ChannelID = "06";
            //sb.AppendLine("<FDDATE>2010/12/16</FDDATE>");
            //sb.AppendLine("<FSCHANNEL_ID>06</FSCHANNEL_ID>");
            //sb.AppendLine("</Data>");
            SP_Q_Combin_Arr_Promo.Do_Query_Combin_Arr_PromoAsync(_Date, _ChannelID, ReturnXML);
            //SP_Q_TBPGM_QUEUE.Do_QueryCompleted += new EventHandler<SendSQL.Do_QueryCompletedEventArgs>(SP_Q_TBPGM_QUEUE_Do_QueryCompleted);
            //AAA.Do_InsertAsync("", "", ReturnStr);
            SP_Q_Combin_Arr_Promo.Do_Query_Combin_Arr_PromoCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "" && args.Result.Substring(0, 6) != "<Error")
                    {

                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        PGM_ARR_PROMO.Clear();
                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        int _TempComNo = 1;
                        foreach (XElement elem in ltox)
                        {

                            string _fddate = "";
                            if (XElementToString(elem.Element("FDDATE")) == "")
                                _fddate = "";
                            else
                                _fddate = (Convert.ToDateTime(elem.Element("FDDATE").Value)).ToString("yyyy/MM/dd");

                            PGM_ARR_PROMO.Add(new ArrPromoList()
                            {
                                //COMBINENO = XElementToString(elem.Element("COMBINENO")),
                                COMBINENO = _TempComNo.ToString(),
                                FCTIME_TYPE = XElementToString(elem.Element("FCTIME_TYPE")),
                                FSPROG_ID = XElementToString(elem.Element("FSPROG_ID")),
                                FNSEQNO = XElementToString(elem.Element("FNSEQNO")),
                                FSNAME = XElementToString(elem.Element("FSNAME")),
                                FSCHANNEL_ID = XElementToString(elem.Element("FSCHANNEL_ID")),
                                FSCHANNEL_NAME = XElementToString(elem.Element("FSCHANNEL_NAME")),
                                FSPLAY_TIME = XElementToString(elem.Element("FSPLAY_TIME")),
                                RealFSPLAY_TIME = GetRealTImeCode(XElementToString(elem.Element("FSPLAY_TIME"))),
                                FSDURATION = XElementToString(elem.Element("FSDURATION")),
                                FNEPISODE = XElementToString(elem.Element("FNEPISODE")),
                                FNBREAK_NO = XElementToString(elem.Element("FNBREAK_NO")),
                                FNSUB_NO = XElementToString(elem.Element("FNSUB_NO")),
                                FSFILE_NO = XElementToString(elem.Element("FSFILE_NO")),
                                FSVIDEO_ID = XElementToString(elem.Element("FSVIDEO_ID")),
                                FSPROMO_ID = XElementToString(elem.Element("FSPROMO_ID")),
                                FDDATE = _fddate,
                                FNLIVE = XElementToString(elem.Element("FNLIVE")),
                                FSSTATUS = XElementToString(elem.Element("FSSTATUS")),
                                FSSIGNAL = XElementToString(elem.Element("FSSIGNAL")),
                                FCTYPE = XElementToString(elem.Element("FCTYPE")),
                                FSMEMO = XElementToString(elem.Element("FSMEMO")),
                                FSMEMO1 = XElementToString(elem.Element("FSMEMO1")),
                                FSPROG_NAME = XElementToString(elem.Element("FSPROG_NAME"))
                            });
                            _TempComNo = _TempComNo + 1;
                        }

                        dataGridProgList.ItemsSource = null;
                        dataGridProgList.ItemsSource = PGM_ARR_PROMO;

                        QueryLostPromo();
                        Clear_Back_Change();
                        BusyMsg.IsBusy = false;
                        MessageBox.Show("查詢播出運行表完成");
                    }
                    else
                    {
                        BusyMsg.IsBusy = false;
                        MessageBox.Show("訊息:" + args.Result);
                    }
                }
            };
        }

        public string XElementToString(XElement INXElement)
        {
            string ReturnStr;
            try
            {
                ReturnStr = INXElement.Value.ToString();
                return ReturnStr;

            }
            catch (Exception ex)
            {
                return "";
            }
        }

        private void ButtonReLoad_Click(object sender, RoutedEventArgs e)
        {

            if (this.DatePickerDate.Text == "")
            {
                MessageBox.Show("請先選擇日期");
                return;
            }
            if (comboBoxChannel.SelectedIndex == -1)
            {
                MessageBox.Show("請先選擇要查詢之頻道");
                return;
            }
            BusyMsg.IsBusy = true;
            labelInsPromoID.Content = "";

            //string ReturnStr = "";

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            //sb.AppendLine("<FDDATE>" + this.textBoxDate.Text + "</FDDATE>");
            if (comboBoxChannel.SelectedIndex != -1)
            {
                string[] ChannelType = ((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString().Split(new Char[] { '-' });
                _ChannelType = ChannelType[1];
                _ChannelID = ChannelType[0];
                _ChannelName = ((ComboBoxItem)comboBoxChannel.SelectedItem).Content.ToString();
                
                _Date = this.DatePickerDate.Text;
            }

            sb.AppendLine("<FDDATE>" + _Date + "</FDDATE>");
            sb.AppendLine("<FSCHANNEL_ID>" + _ChannelID + "</FSCHANNEL_ID>");
            sb.AppendLine("</Data>");
            if (_ChannelType == "001")
            {
                WSPGMCreatePlayList.WSPGMCreatePlayListSoapClient Q_PGM_PLAY_LIST = new WSPGMCreatePlayList.WSPGMCreatePlayListSoapClient();
                Q_PGM_PLAY_LIST.Do_Query_Prog_Queue_SegAsync("", sb.ToString());
                Q_PGM_PLAY_LIST.Do_Query_Prog_Queue_SegCompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (args.Result != null && args.Result != "" && args.Result.Substring(0, 6) != "<Error")
                        {

                            byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                            PGM_ARR_PROMO.Clear();
                            StringBuilder output = new StringBuilder();
                            // Create an XmlReader
                            XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                            var ltox = from str in doc.Elements("Datas").Elements("Data")
                                       select str;
                            foreach (XElement elem in ltox)
                            {
                                PGM_ARR_PROMO.Add(new ArrPromoList()
                                {
                                    COMBINENO = XElementToString(elem.Element("COMBINENO")),
                                    FCTIME_TYPE = XElementToString(elem.Element("FCTIME_TYPE")),
                                    FSPROG_ID = XElementToString(elem.Element("FSPROG_ID")),
                                    FNSEQNO = XElementToString(elem.Element("FNSEQNO")),
                                    FSNAME = elem.Element("FSNAME").Value.ToString(),
                                    FSCHANNEL_ID = XElementToString(elem.Element("FSCHANNEL_ID")),
                                    FSCHANNEL_NAME = XElementToString(elem.Element("FSCHANNEL_NAME")),
                                    FSPLAY_TIME = XElementToString(elem.Element("FSPLAY_TIME")),
                                    RealFSPLAY_TIME = GetRealTImeCode(XElementToString(elem.Element("FSPLAY_TIME"))),
                                    FSDURATION = XElementToString(elem.Element("FSDURATION")),
                                    FNEPISODE = XElementToString(elem.Element("FNEPISODE")),
                                    FNBREAK_NO = XElementToString(elem.Element("FNBREAK_NO")),
                                    FNSUB_NO = XElementToString(elem.Element("FNSUB_NO")),
                                    FSFILE_NO = XElementToString(elem.Element("FSFILE_NO")),
                                    FSVIDEO_ID = XElementToString(elem.Element("FSVIDEO_ID")),
                                    FSPROMO_ID = XElementToString(elem.Element("FSPROMO_ID")),
                                    FDDATE = (Convert.ToDateTime(elem.Element("FDDATE").Value)).ToString("yyyy/MM/dd"),
                                    FNLIVE = XElementToString(elem.Element("FNLIVE")),
                                    FSSTATUS = XElementToString(elem.Element("FSSTATUS")),
                                    FSSIGNAL = XElementToString(elem.Element("FSSIGNAL")),
                                    FCTYPE = XElementToString(elem.Element("FCTYPE")),
                                    FSMEMO = XElementToString(elem.Element("FSMEMO")),
                                    FSMEMO1 = XElementToString(elem.Element("FSMEMO1")),
                                    FSPROG_NAME = elem.Element("FSPROG_NAME").Value.ToString()
                                });
                            }
                            dataGridProgList.ItemsSource = null;
                            dataGridProgList.ItemsSource = PGM_ARR_PROMO;
                            BusyMsg.IsBusy = false;
                            if (ReloadMessage == true)
                                MessageBox.Show("重新載入完成");
                            ReloadMessage = true;

                        }
                        else
                        {
                            BusyMsg.IsBusy = false;
                            MessageBox.Show("訊息:" + args.Result);
                        }
                    }
                };
            }
            else
            {
                WSPGMCreatePlayList.WSPGMCreatePlayListSoapClient Q_PGM_PLAY_LIST = new WSPGMCreatePlayList.WSPGMCreatePlayListSoapClient();
                Q_PGM_PLAY_LIST.Do_Query_Prog_Queue_Seg_MIXAsync("", sb.ToString());
                Q_PGM_PLAY_LIST.Do_Query_Prog_Queue_Seg_MIXCompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (args.Result != null && args.Result != "" && args.Result.Substring(0, 6) != "<Error")
                        {

                            byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                            PGM_ARR_PROMO.Clear();
                            StringBuilder output = new StringBuilder();
                            // Create an XmlReader
                            XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                            var ltox = from str in doc.Elements("Datas").Elements("Data")
                                       select str;
                            foreach (XElement elem in ltox)
                            {
                                PGM_ARR_PROMO.Add(new ArrPromoList()
                                {
                                    COMBINENO = XElementToString(elem.Element("COMBINENO")),
                                    FCTIME_TYPE = XElementToString(elem.Element("FCTIME_TYPE")),
                                    FSPROG_ID = XElementToString(elem.Element("FSPROG_ID")),
                                    FNSEQNO = XElementToString(elem.Element("FNSEQNO")),
                                    FSNAME = elem.Element("FSNAME").Value.ToString(),
                                    FSCHANNEL_ID = XElementToString(elem.Element("FSCHANNEL_ID")),
                                    FSCHANNEL_NAME = XElementToString(elem.Element("FSCHANNEL_NAME")),
                                    FSPLAY_TIME = XElementToString(elem.Element("FSPLAY_TIME")),
                                    RealFSPLAY_TIME = GetRealTImeCode(XElementToString(elem.Element("FSPLAY_TIME"))),
                                    FSDURATION = XElementToString(elem.Element("FSDURATION")),
                                    FNEPISODE = XElementToString(elem.Element("FNEPISODE")),
                                    FNBREAK_NO = XElementToString(elem.Element("FNBREAK_NO")),
                                    FNSUB_NO = XElementToString(elem.Element("FNSUB_NO")),
                                    FSFILE_NO = XElementToString(elem.Element("FSFILE_NO")),
                                    FSVIDEO_ID = XElementToString(elem.Element("FSVIDEO_ID")),
                                    FSPROMO_ID = XElementToString(elem.Element("FSPROMO_ID")),
                                    FDDATE = (Convert.ToDateTime(elem.Element("FDDATE").Value)).ToString("yyyy/MM/dd"),
                                    FNLIVE = XElementToString(elem.Element("FNLIVE")),
                                    FSSTATUS = XElementToString(elem.Element("FSSTATUS")),
                                    FSSIGNAL = XElementToString(elem.Element("FSSIGNAL")),
                                    FCTYPE = XElementToString(elem.Element("FCTYPE")),
                                    FSMEMO = XElementToString(elem.Element("FSMEMO")),
                                    FSMEMO1 = XElementToString(elem.Element("FSMEMO1")),
                                    FSPROG_NAME = elem.Element("FSPROG_NAME").Value.ToString()
                                });
                            }
                            dataGridProgList.ItemsSource = null;
                            dataGridProgList.ItemsSource = PGM_ARR_PROMO;
                            BusyMsg.IsBusy = false;
                            if (ReloadMessage == true)
                                MessageBox.Show("重新載入完成");
                            ReloadMessage = true;

                        }
                        else
                        {
                            BusyMsg.IsBusy = false;
                            MessageBox.Show("訊息:" + args.Result);
                        }
                    }
                };
            }
        }

        private void ButtonQueryPromo_Click(object sender, RoutedEventArgs e)
        {
            if (_Date == "")
            {
                MessageBox.Show("請先選擇日期");
                return;
            }
            if (_ChannelID == "")
            {
                MessageBox.Show("請先選擇頻道");
                return;
            }
            PGM125_01 PGM125_01_Frm = new PGM125_01();
            PGM125_01_Frm._date = _Date;
            PGM125_01_Frm._channelid = _ChannelID;

            PGM125_01_Frm.Show();
            PGM125_01_Frm.Closed += (s, args) =>
            {
                if (PGM125_01_Frm._Promo_ID != null && PGM125_01_Frm._Promo_ID != "")
                {
                    WorkPromo = PGM125_01_Frm.ReturnPromo;
                    labelInsPromoID.Content = WorkPromo.FSPROMO_ID;
                    labelInsPromoName.Content = WorkPromo.FSPROMO_NAME;

                    canvasPromo.Visibility = Visibility.Visible;

                }

            };
        }

        private void buttonClosePromo_Click(object sender, RoutedEventArgs e)
        {
            canvasPromo.Visibility = Visibility.Collapsed;
        }

        private void buttonReCalTime_Click(object sender, RoutedEventArgs e)
        {
            BusyMsg.IsBusy = true;
            ReCalTimecode();
            BusyMsg.IsBusy = false;
        }

        public string GetRealTImeCode(string InTimeCode)
        {
            string RealPlayTime;
            if (InTimeCode == "")
                RealPlayTime = "";
            else
            {
                if (int.Parse(InTimeCode.Substring(0, 2)) >= 24)
                    RealPlayTime = (int.Parse(InTimeCode.Substring(0, 2)) - 24).ToString("00") + InTimeCode.Substring(2, 9);
                else
                    RealPlayTime = InTimeCode;
            }
            return RealPlayTime;
        }

        public void ReCalTimecode()
        {
            string NextPlayTime = "";
            int combineNo = 1;
            foreach (ArrPromoList Temp_ArrPromo in PGM_ARR_PROMO)
            {
                Temp_ArrPromo.COMBINENO = combineNo.ToString();
                combineNo = combineNo + 1;
                if (Temp_ArrPromo.FSPROG_ID != "")
                {
                    if (Temp_ArrPromo.FCTIME_TYPE == "O") //固定時間
                    {
                        NextPlayTime = TransferTimecode.frame2timecode(TransferTimecode.timecodetoframe(Temp_ArrPromo.FSPLAY_TIME) + (TransferTimecode.timecodetoframe(Temp_ArrPromo.FSDURATION)));
                    }
                    else if (Temp_ArrPromo.FCTIME_TYPE == "A") //順時間
                    {
                        if (NextPlayTime == "")
                        {
                            NextPlayTime = TransferTimecode.frame2timecode(TransferTimecode.timecodetoframe(Temp_ArrPromo.FSPLAY_TIME) + (TransferTimecode.timecodetoframe(Temp_ArrPromo.FSDURATION)));
                        }
                        else
                        {
                            Temp_ArrPromo.FSPLAY_TIME = NextPlayTime;
                            Temp_ArrPromo.RealFSPLAY_TIME = GetRealTImeCode(NextPlayTime);
                            NextPlayTime = TransferTimecode.frame2timecode(TransferTimecode.timecodetoframe(Temp_ArrPromo.FSPLAY_TIME) + (TransferTimecode.timecodetoframe(Temp_ArrPromo.FSDURATION)));
                        }
                    }
                }
            }
            dataGridProgList.SelectedIndex = Datagridindex;
            //dataGridProgList.ItemsSource = null;
            //dataGridProgList.ItemsSource = PGM_ARR_PROMO;
        }

        public void ReCalSUBNO()
        {
            string NextPlayTime = "";
            int combineNo = 1;
            int subno = 1;
            foreach (ArrPromoList Temp_ArrPromo in PGM_ARR_PROMO)
            {
                Temp_ArrPromo.COMBINENO = combineNo.ToString();
                combineNo = combineNo + 1;
                if (Temp_ArrPromo.FCTYPE == "G" || Temp_ArrPromo.FCTYPE == "") //節目或分行
                {
                    subno = 1;
                    Temp_ArrPromo.FNSUB_NO = "";
                }
                else
                {
                    Temp_ArrPromo.FNSUB_NO = subno.ToString();
                    subno = subno + 1;
                }
            }

            dataGridProgList.SelectedIndex = Datagridindex;
            //dataGridProgList.ItemsSource = null;
            //dataGridProgList.ItemsSource = PGM_ARR_PROMO;
        }


        int Datagridindex;
        private void buttonInsertPromo_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridProgList.SelectedIndex < 0)
            {
                MessageBox.Show("必須先選擇要插入Promo的位置");
                return;
            }

            if (labelInsPromoID.Content.ToString() == "")
            {
                MessageBox.Show("請先查詢要作業的短帶");
                return;
            }

            ArrPromoList Temp_ArrPromo = new ArrPromoList();
            int i = dataGridProgList.SelectedIndex;
            Datagridindex = dataGridProgList.SelectedIndex;

            //if (CheckPromoInsertTime(PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSPLAY_TIME) == false)
            //{
            //    //MessageBox.Show("不在有效時間內");
            //    return;
            //}
            Add_Work_Change();


            if (PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FCTYPE == "P") //如果所選位置是Promo
            {
                Temp_ArrPromo.COMBINENO = "";
                Temp_ArrPromo.FCTIME_TYPE = "A";
                Temp_ArrPromo.FSPROG_ID = PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSPROG_ID;
                Temp_ArrPromo.FNSEQNO = PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FNSEQNO;
                Temp_ArrPromo.FSNAME = WorkPromo.FSPROMO_NAME;
                Temp_ArrPromo.FSCHANNEL_ID = PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSCHANNEL_ID;
                Temp_ArrPromo.FSCHANNEL_NAME = PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSCHANNEL_NAME;

                Temp_ArrPromo.FSPLAY_TIME = PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSPLAY_TIME;
                Temp_ArrPromo.FSDURATION = WorkPromo.FSDURATION;
                Temp_ArrPromo.FNEPISODE = PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FNEPISODE;
                Temp_ArrPromo.FNBREAK_NO = PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FNBREAK_NO;
                //由於是promo,所以插入promo的支數是相同的,之後的PromosubNO要加一
                Temp_ArrPromo.FNSUB_NO = PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FNSUB_NO;

                Temp_ArrPromo.FSFILE_NO = WorkPromo.FSFILE_NO;
                Temp_ArrPromo.FSVIDEO_ID = WorkPromo.FSVIDEO_ID;
                Temp_ArrPromo.FSPROMO_ID = WorkPromo.FSPROMO_ID;
                Temp_ArrPromo.FDDATE = PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FDDATE;
                Temp_ArrPromo.FNLIVE = "0"; //表示LIVE
                Temp_ArrPromo.FCTYPE = "P"; //表示宣傳帶
                Temp_ArrPromo.FSMEMO = WorkPromo.FSMEMO;
                Temp_ArrPromo.FSPROG_NAME = PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSPROG_NAME;
                if (CheckPromoInsertTime(Temp_ArrPromo.FSPLAY_TIME) == false)
                {
                    //MessageBox.Show("不在有效時間內");
                    return;
                }

                Temp_ArrPromo.RealFSPLAY_TIME = GetRealTImeCode(Temp_ArrPromo.FSPLAY_TIME);
                Temp_ArrPromo.FSSIGNAL = WorkPromo.FSSIGNAL;
                Temp_ArrPromo.FSMEMO1 = WorkPromo.FSMEMO1;
                PGM_ARR_PROMO.Insert(dataGridProgList.SelectedIndex, Temp_ArrPromo);
                //之後的PromosubNO要加一
                i = i + 1; //從下一筆開始
                while (i < PGM_ARR_PROMO.Count - 1 && PGM_ARR_PROMO[i].FCTYPE == "P")
                {
                    PGM_ARR_PROMO[i].FNSUB_NO = (int.Parse(PGM_ARR_PROMO[i].FNSUB_NO) + 1).ToString();
                    i = i + 1;
                }
            }
            else if (PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FCTYPE == "G")//如果所選位置是節目則要看前一筆資料的訊息
            {
                int j;
                if (i == 0) //如果為第一筆資料,前面沒資料
                {
                    //第一筆一定是節目說明所以不能插入
                    //j = dataGridProgList.SelectedIndex;
                    //Temp_ArrPromo.COMBINENO = "";
                    //Temp_ArrPromo.FCTIME_TYPE = "A";
                    //Temp_ArrPromo.FSPROG_ID = PGM_ARR_PROMO[j].FSPROG_ID;
                    //Temp_ArrPromo.FNSEQNO = PGM_ARR_PROMO[j].FNSEQNO;
                    //Temp_ArrPromo.FSNAME = WorkPromo.FSPROMO_NAME;
                    //Temp_ArrPromo.FSCHANNEL_ID = PGM_ARR_PROMO[j].FSCHANNEL_ID;
                    //Temp_ArrPromo.FSCHANNEL_NAME = PGM_ARR_PROMO[j].FSCHANNEL_NAME;

                    //Temp_ArrPromo.FSPLAY_TIME = PGM_ARR_PROMO[j].FSPLAY_TIME;
                    //Temp_ArrPromo.FSDURATION = WorkPromo.FSDURATION;
                    //Temp_ArrPromo.FNEPISODE = PGM_ARR_PROMO[j].FNEPISODE;
                    //Temp_ArrPromo.FNBREAK_NO = "0";
                    ////由於是promo,所以插入promo的支數是相同的,之後的PromosubNO要加一
                    //Temp_ArrPromo.FNSUB_NO = "1";

                    //Temp_ArrPromo.FSFILE_NO = WorkPromo.FSFILE_NO;
                    //Temp_ArrPromo.FSVIDEO_ID = WorkPromo.FSVIDEO_ID;
                    //Temp_ArrPromo.FSPROMO_ID = WorkPromo.FSPROMO_ID;
                    //Temp_ArrPromo.FDDATE = PGM_ARR_PROMO[j].FDDATE;
                    //Temp_ArrPromo.FNLIVE = "1"; //表示LIVE
                    //Temp_ArrPromo.FCTYPE = "P"; //表示宣傳帶
                    //Temp_ArrPromo.FSMEMO = "";
                    //PGM_ARR_PROMO.Insert(dataGridProgList.SelectedIndex, Temp_ArrPromo);
                    MessageBox.Show("不能在此插入短帶");
                }
                else //如果不是第一段
                {

                    j = dataGridProgList.SelectedIndex - 1;
                    if (PGM_ARR_PROMO[j].FSPROG_ID != "")
                    {

                        Temp_ArrPromo.COMBINENO = "";
                        Temp_ArrPromo.FCTIME_TYPE = "A";
                        Temp_ArrPromo.FSPROG_ID = PGM_ARR_PROMO[j].FSPROG_ID;
                        Temp_ArrPromo.FNSEQNO = PGM_ARR_PROMO[j].FNSEQNO;
                        Temp_ArrPromo.FSNAME = WorkPromo.FSPROMO_NAME;
                        Temp_ArrPromo.FSCHANNEL_ID = PGM_ARR_PROMO[j].FSCHANNEL_ID;
                        Temp_ArrPromo.FSCHANNEL_NAME = PGM_ARR_PROMO[j].FSCHANNEL_NAME;

                        Temp_ArrPromo.FSPLAY_TIME = PGM_ARR_PROMO[j].FSPLAY_TIME;
                        Temp_ArrPromo.FSDURATION = WorkPromo.FSDURATION;
                        Temp_ArrPromo.FNEPISODE = PGM_ARR_PROMO[j].FNEPISODE;
                        Temp_ArrPromo.FNBREAK_NO = PGM_ARR_PROMO[j].FNBREAK_NO;
                        //由於是promo,所以插入promo的支數是相同的,之後的PromosubNO要加一
                        if (PGM_ARR_PROMO[j].FCTYPE == "P") //如果前一筆為Promo則採用前一筆的支數+1
                            Temp_ArrPromo.FNSUB_NO = (int.Parse(PGM_ARR_PROMO[j].FNSUB_NO) + 1).ToString();
                        else //不然為第一支
                            Temp_ArrPromo.FNSUB_NO = "1";

                        Temp_ArrPromo.FSFILE_NO = WorkPromo.FSFILE_NO;
                        Temp_ArrPromo.FSVIDEO_ID = WorkPromo.FSVIDEO_ID;
                        Temp_ArrPromo.FSPROMO_ID = WorkPromo.FSPROMO_ID;
                        Temp_ArrPromo.FDDATE = PGM_ARR_PROMO[j].FDDATE;
                        Temp_ArrPromo.FNLIVE = "0"; //表示非LIVE
                        Temp_ArrPromo.FCTYPE = "P"; //表示宣傳帶
                        Temp_ArrPromo.FSMEMO = WorkPromo.FSMEMO;
                        Temp_ArrPromo.FSMEMO1 = WorkPromo.FSMEMO1;
                        Temp_ArrPromo.FSSIGNAL = WorkPromo.FSSIGNAL;
                        Temp_ArrPromo.FSPROG_NAME = PGM_ARR_PROMO[j].FSPROG_NAME;
                        if (CheckPromoInsertTime(Temp_ArrPromo.FSPLAY_TIME) == false)
                        {
                            //MessageBox.Show("不在有效時間內");
                            return;
                        }

                        Temp_ArrPromo.RealFSPLAY_TIME = GetRealTImeCode(Temp_ArrPromo.FSPLAY_TIME);
                        PGM_ARR_PROMO.Insert(dataGridProgList.SelectedIndex, Temp_ArrPromo);
                    }
                    else //如果插在節目標題上,那就是此節目第一支
                    {
                        j = dataGridProgList.SelectedIndex;
                        Temp_ArrPromo.COMBINENO = "";
                        Temp_ArrPromo.FCTIME_TYPE = "A";
                        Temp_ArrPromo.FSPROG_ID = PGM_ARR_PROMO[j].FSPROG_ID;
                        Temp_ArrPromo.FNSEQNO = PGM_ARR_PROMO[j].FNSEQNO;
                        Temp_ArrPromo.FSNAME = WorkPromo.FSPROMO_NAME;
                        Temp_ArrPromo.FSCHANNEL_ID = PGM_ARR_PROMO[j].FSCHANNEL_ID;
                        Temp_ArrPromo.FSCHANNEL_NAME = PGM_ARR_PROMO[j].FSCHANNEL_NAME;

                        Temp_ArrPromo.FSPLAY_TIME = PGM_ARR_PROMO[j].FSPLAY_TIME;
                        Temp_ArrPromo.FSDURATION = WorkPromo.FSDURATION;
                        Temp_ArrPromo.FNEPISODE = PGM_ARR_PROMO[j].FNEPISODE;
                        Temp_ArrPromo.FNBREAK_NO = (int.Parse(PGM_ARR_PROMO[j].FNBREAK_NO) - 1).ToString();
                        //由於是promo,所以插入promo的支數是相同的,之後的PromosubNO要加一
                        Temp_ArrPromo.FNSUB_NO = "1";

                        Temp_ArrPromo.FSFILE_NO = WorkPromo.FSFILE_NO;
                        Temp_ArrPromo.FSVIDEO_ID = WorkPromo.FSVIDEO_ID;
                        Temp_ArrPromo.FSPROMO_ID = WorkPromo.FSPROMO_ID;
                        Temp_ArrPromo.FDDATE = PGM_ARR_PROMO[j].FDDATE;
                        Temp_ArrPromo.FNLIVE = "0"; //表示非LIVE
                        Temp_ArrPromo.FCTYPE = "P"; //表示宣傳帶
                        Temp_ArrPromo.FSMEMO = WorkPromo.FSMEMO;
                        Temp_ArrPromo.FSMEMO1 = WorkPromo.FSMEMO1;
                        Temp_ArrPromo.FSSIGNAL = WorkPromo.FSSIGNAL;
                        Temp_ArrPromo.FSPROG_NAME = PGM_ARR_PROMO[j].FSPROG_NAME;
                        if (CheckPromoInsertTime(Temp_ArrPromo.FSPLAY_TIME) == false)
                        {
                            //MessageBox.Show("不在有效時間內");
                            return;
                        }
                        Temp_ArrPromo.RealFSPLAY_TIME = GetRealTImeCode(Temp_ArrPromo.FSPLAY_TIME);
                        PGM_ARR_PROMO.Insert(dataGridProgList.SelectedIndex, Temp_ArrPromo);
                    }
                }
            }
            else if (PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FCTYPE == "")//如果所選位置是節目抬頭則要看前一筆資料的訊息
            {
                int j = dataGridProgList.SelectedIndex - 1;
                while (j > 0 && PGM_ARR_PROMO[j].FSPROG_ID == "") //先找到之前一筆的資料
                {
                    j = j - 1;
                }
                if (j > 0)
                {

                    Temp_ArrPromo.COMBINENO = "";
                    Temp_ArrPromo.FCTIME_TYPE = "A";
                    Temp_ArrPromo.FSPROG_ID = PGM_ARR_PROMO[j].FSPROG_ID;
                    Temp_ArrPromo.FNSEQNO = PGM_ARR_PROMO[j].FNSEQNO;
                    Temp_ArrPromo.FSNAME = WorkPromo.FSPROMO_NAME;
                    Temp_ArrPromo.FSCHANNEL_ID = PGM_ARR_PROMO[j].FSCHANNEL_ID;
                    Temp_ArrPromo.FSCHANNEL_NAME = PGM_ARR_PROMO[j].FSCHANNEL_NAME;

                    Temp_ArrPromo.FSPLAY_TIME = PGM_ARR_PROMO[j].FSPLAY_TIME;
                    Temp_ArrPromo.FSDURATION = WorkPromo.FSDURATION;
                    Temp_ArrPromo.FNEPISODE = PGM_ARR_PROMO[j].FNEPISODE;
                    Temp_ArrPromo.FNBREAK_NO = PGM_ARR_PROMO[j].FNBREAK_NO;
                    //由於是promo,所以插入promo的支數是相同的,之後的PromosubNO要加一
                    if (PGM_ARR_PROMO[j].FCTYPE == "P")
                        Temp_ArrPromo.FNSUB_NO = (int.Parse(PGM_ARR_PROMO[j].FNSUB_NO) + 1).ToString();
                    else
                        Temp_ArrPromo.FNSUB_NO = "1";

                    Temp_ArrPromo.FSFILE_NO = WorkPromo.FSFILE_NO;
                    Temp_ArrPromo.FSVIDEO_ID = WorkPromo.FSVIDEO_ID;
                    Temp_ArrPromo.FSPROMO_ID = WorkPromo.FSPROMO_ID;
                    Temp_ArrPromo.FDDATE = PGM_ARR_PROMO[j].FDDATE;
                    Temp_ArrPromo.FNLIVE = "0"; //表示非LIVE
                    Temp_ArrPromo.FCTYPE = "P"; //表示宣傳帶
                    Temp_ArrPromo.FSMEMO = WorkPromo.FSMEMO;
                    Temp_ArrPromo.FSMEMO1 = WorkPromo.FSMEMO1;
                    Temp_ArrPromo.FSSIGNAL = WorkPromo.FSSIGNAL;
                    Temp_ArrPromo.FSPROG_NAME = PGM_ARR_PROMO[j].FSPROG_NAME;
                    if (CheckPromoInsertTime(Temp_ArrPromo.FSPLAY_TIME) == false)
                    {
                        //MessageBox.Show("不在有效時間內");
                        return;
                    }
                    Temp_ArrPromo.RealFSPLAY_TIME = GetRealTImeCode(Temp_ArrPromo.FSPLAY_TIME);
                    //Temp_ArrPromo.FSSIGNAL = "";
                    //Temp_ArrPromo.FSMEMO1 = "";
                    PGM_ARR_PROMO.Insert(dataGridProgList.SelectedIndex, Temp_ArrPromo);
                    //因為一定是最後一支所以不用修改之後的支數
                }

            }
            ReCalTimecode();
            dataGridProgList.SelectedIndex = Datagridindex + 1;
        }

        private void buttonReplacePromo_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridProgList.SelectedIndex < 0)
            {
                MessageBox.Show("必須先選擇要置換短帶的位置");
                return;
            }

            if (labelInsPromoID.Content.ToString() == "")
            {
                MessageBox.Show("請先查詢要作業的短帶");
                return;
            }
            if (PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FCTYPE != "P") //如果所選位置是Promo
            {
                MessageBox.Show("此筆資料不是短帶,不能置換");
                return;
            }
            //ArrPromoList Temp_ArrPromo = new ArrPromoList();
            int i = dataGridProgList.SelectedIndex;

            if (CheckPromoInsertTime(PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSPLAY_TIME) == false)
            {
                //MessageBox.Show("不在有效時間內");
                return;
            }
            Add_Work_Change();
            if (PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FCTYPE == "P") //如果所選位置是Promo
            {
                PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSNAME = WorkPromo.FSPROMO_NAME;
                PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSDURATION = WorkPromo.FSDURATION;
                PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSFILE_NO = WorkPromo.FSFILE_NO;
                PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSVIDEO_ID = WorkPromo.FSVIDEO_ID;
                PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSPROMO_ID = WorkPromo.FSPROMO_ID;
            }
            ReCalTimecode();
        }

        private void buttonDeletePromo_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridProgList.SelectedIndex < 0)
            {
                MessageBox.Show("必須先選擇要刪除短帶的位置");
                return;
            }
            if (PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FCTYPE != "P") //如果所選位置是Promo
            {
                MessageBox.Show("此筆資料不是短帶,不能刪除");
                return;
            }
            Add_Work_Change();
            string DelProgID = PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSPROG_ID;
            string DelSeqno = PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FNSEQNO;
            string DelBreakNO = PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FNBREAK_NO;

            PGM_ARR_PROMO.RemoveAt(dataGridProgList.SelectedIndex);
            int i = dataGridProgList.SelectedIndex;
            int NowIndex = i;
            while (i < PGM_ARR_PROMO.Count - 1 && PGM_ARR_PROMO[i].FSPROG_ID == DelProgID && PGM_ARR_PROMO[i].FNSEQNO == DelSeqno && PGM_ARR_PROMO[i].FNBREAK_NO == DelBreakNO && PGM_ARR_PROMO[i].FCTYPE == "P")
            {
                //PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FNSUB_NO = (int.Parse(PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FNSUB_NO) - 1).ToString();
                PGM_ARR_PROMO[i].FNSUB_NO = (int.Parse(PGM_ARR_PROMO[i].FNSUB_NO) - 1).ToString();
                i = i + 1;
            }

            ReCalTimecode();
            dataGridProgList.SelectedIndex = NowIndex - 1;
        }

        private void buttonReplaceAllPromo_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridProgList.SelectedIndex < 0)
            {
                MessageBox.Show("必須先選擇要置換短帶的位置");
                return;
            }
            if (labelInsPromoID.Content.ToString() == "")
            {
                MessageBox.Show("請先查詢要作業的短帶");
                return;
            }
            if (PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FCTYPE != "P") //如果所選位置是Promo
            {
                MessageBox.Show("此筆資料不是短帶,不能置換");
                return;
            }
            Add_Work_Change();
            //ArrPromoList Temp_ArrPromo = new ArrPromoList();
            //int i = dataGridProgList.SelectedIndex;
            string Replace_Promo_ID = PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSPROMO_ID;
            for (int i = 0; i <= PGM_ARR_PROMO.Count - 1; i++)
            {

                if (PGM_ARR_PROMO[i].FSPROMO_ID == Replace_Promo_ID)
                {
                    if (CheckPromoInsertTime(PGM_ARR_PROMO[i].FSPLAY_TIME) == false)
                    {
                        //MessageBox.Show(PGM_ARR_PROMO[i].FSPLAY_TIME +"不在有效時間內");
                    }
                    else
                    {
                        PGM_ARR_PROMO[i].FSNAME = WorkPromo.FSPROMO_NAME;
                        PGM_ARR_PROMO[i].FSDURATION = WorkPromo.FSDURATION;
                        PGM_ARR_PROMO[i].FSFILE_NO = WorkPromo.FSFILE_NO;
                        PGM_ARR_PROMO[i].FSVIDEO_ID = WorkPromo.FSVIDEO_ID;
                        PGM_ARR_PROMO[i].FSPROMO_ID = WorkPromo.FSPROMO_ID;
                    }
                }
            }
            ReCalTimecode();
            MessageBox.Show("全部置換完成");
        }

        //public void Each_Tick(object o, EventArgs sender)
        public void Each_Tick(object sender, EventArgs e)
        {
            myDispatcherTimer.Stop();
            RoutedEventArgs AAA = new RoutedEventArgs();
            buttonSaveArrList_Click(sender, AAA);
        }


        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            //myDispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0,5000); // 100 Milliseconds 
            myDispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 300000); // 100 Milliseconds 
            myDispatcherTimer.Tick += new EventHandler(Each_Tick);

            //myDispatcherTimer.Start();
            dataGridProgList.LoadingRow += (s1, args1) =>
            {
                PgmQueueDate PGMQueue = args1.Row.DataContext as PgmQueueDate;
                args1.Row.Background = new SolidColorBrush(Colors.White);
                args1.Row.Foreground = new SolidColorBrush(Colors.Black);
                if (PGM_ARR_PROMO[args1.Row.GetIndex()].FSPROG_ID != "")
                {
                    int j;
                    j = args1.Row.GetIndex() - 1;
                    int i = -1;
                    if (PGM_ARR_PROMO[j].FSPROG_ID != "" && j!=-1)
                    {
                        i = j;
                    }
                    else
                    {
                        while (j > 0 && PGM_ARR_PROMO[j].FSPROG_ID == "")
                        {
                            j = j - 1;
                            i = j;
                        }
                    }
                    if (i != -1) //之前找不到節目或Promo的資料
                    {
                        if (args1.Row.GetIndex() != 0)
                        {
                            if ((TransferTimecode.timecodetoframe(PGM_ARR_PROMO[i].FSPLAY_TIME) + TransferTimecode.timecodetoframe(PGM_ARR_PROMO[i].FSDURATION)) != TransferTimecode.timecodetoframe(PGM_ARR_PROMO[args1.Row.GetIndex()].FSPLAY_TIME))
                            {
                                args1.Row.Foreground = new SolidColorBrush(Colors.Red);
                            }
                        }
                    }
                    i = -1; //往後找直到找到節目或Promo
                    j = args1.Row.GetIndex() + 1;
                    if (PGM_ARR_PROMO[j].FSPROG_ID != "" && j <= PGM_ARR_PROMO.Count - 1)
                    {
                        i = j;
                    }
                    else
                    {
                        while (j < PGM_ARR_PROMO.Count - 1 && PGM_ARR_PROMO[j].FSPROG_ID == "")
                        {
                            j = j + 1;
                            i = j;
                        }
                    }
                    if (i != -1) //之後找不到節目或Promo的資料
                    {
                        if (args1.Row.GetIndex() != PGM_ARR_PROMO.Count - 1)
                        {
                            if (TransferTimecode.timecodetoframe(PGM_ARR_PROMO[args1.Row.GetIndex()].FSPLAY_TIME) + TransferTimecode.timecodetoframe(PGM_ARR_PROMO[args1.Row.GetIndex()].FSDURATION) != TransferTimecode.timecodetoframe(PGM_ARR_PROMO[j].FSPLAY_TIME))
                            {
                                args1.Row.Foreground = new SolidColorBrush(Colors.Red);
                                if (PGM_ARR_PROMO[args1.Row.GetIndex()].FNLIVE == "1")
                                {
                                    args1.Row.Foreground = new SolidColorBrush(Colors.White);
                                }
                            }
                        }
                    }


                    if (PGM_ARR_PROMO[args1.Row.GetIndex()].FCTYPE == "P")
                    {
                        args1.Row.Background = new SolidColorBrush(Colors.Cyan);
                    }
                    if (PGM_ARR_PROMO[args1.Row.GetIndex()].FCTYPE == "G")
                    {
                        args1.Row.Background = new SolidColorBrush(Colors.Yellow);
                    }
                    if (PGM_ARR_PROMO[args1.Row.GetIndex()].FNLIVE == "1")
                    {
                        args1.Row.Background = new SolidColorBrush(Colors.Red);
                        args1.Row.Foreground = new SolidColorBrush(Colors.White);
                    }

                }
            };
        }


        void Canvas_KeyUp(object sender, KeyEventArgs e)
        {
            //check for the specific 'v' key, then check modifiers
            if (e.Key == Key.V)
            {
                if ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
                {
                    //specific Ctrl+V action here
                    buttonDeletePromo_Click(sender, e);
                }
            } // else ignore the keystroke
        }


        private bool CheckPromoInsertTime(string inTimecode)
        {
            if (WorkPromo.FSBEG_TIME == "" || WorkPromo.FSEND_TIME == "")
            {
                return true;
            }
            if (WorkPromo.FCDATE_TIME_TYPE == "1") //日期時間區間
            {
                if (_Date == WorkPromo.FDBEG_DATE)
                {
                    if (TransferTimecode.timecodetoframe(inTimecode) < TransferTimecode.timecodetoframe(WorkPromo.FSBEG_TIME.Substring(0, 2) + ":" + WorkPromo.FSBEG_TIME.Substring(2, 2) + ":00:00"))
                    {
                        MessageBox.Show("此短帶有效區間為" + WorkPromo.FSBEG_TIME + "--" + WorkPromo.FSEND_TIME + "不可於此處插入");
                        return false; //回傳不可插入
                    }
                    //WorkPromo.FSBEG_TIME
                }

                if (_Date == WorkPromo.FDEND_DATE)
                {
                    if (TransferTimecode.timecodetoframe(inTimecode) > TransferTimecode.timecodetoframe(WorkPromo.FSEND_TIME.Substring(0, 2) + ":" + WorkPromo.FSEND_TIME.Substring(2, 2) + ":00:00"))
                    {
                        MessageBox.Show("此短帶有效區間為" + WorkPromo.FSBEG_TIME + "--" + WorkPromo.FSEND_TIME + "不可於此處插入");
                        return false; //回傳不可插入
                    }
                    //WorkPromo.FSBEG_TIME
                }
                //if (_Date == WorkPromo.FDBEG_DATE || _Date == WorkPromo.FDEND_DATE)
                //{
                //    if (TransferTimecode.timecodetoframe(inTimecode) > TransferTimecode.timecodetoframe(WorkPromo.FSEND_TIME) || TransferTimecode.timecodetoframe(inTimecode) < TransferTimecode.timecodetoframe(WorkPromo.FSBEG_TIME))
                //    {
                //        MessageBox.Show("此短帶有效區間為" + WorkPromo.FSBEG_TIME + "--" + WorkPromo.FSEND_TIME + "不可於此處插入");
                //        return false; //回傳不可插入
                //    }
                //    //WorkPromo.FSBEG_TIME
                //}
            }
            else if (WorkPromo.FCDATE_TIME_TYPE == "2")//時間區間
            {
                if (TransferTimecode.timecodetoframe(inTimecode) > TransferTimecode.timecodetoframe(WorkPromo.FSEND_TIME.Substring(0, 2) + ":" + WorkPromo.FSEND_TIME.Substring(2, 2) + ":00:00") || TransferTimecode.timecodetoframe(inTimecode) < TransferTimecode.timecodetoframe(WorkPromo.FSBEG_TIME.Substring(0, 2) + ":" + WorkPromo.FSBEG_TIME.Substring(2, 2) + ":00:00"))
                {
                    MessageBox.Show("此短帶有效區間為" + WorkPromo.FSBEG_TIME + "--" + WorkPromo.FSEND_TIME + "不可於此處插入");
                    return false; //回傳不可插入
                }
            }
            return true;
        }

        List<Promo_SCHEDULED_List> Promo_Scheduled_List = new List<Promo_SCHEDULED_List>();
        public void QueryDateSchPromo()
        {

            //循序查詢SCH
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_PROMO_SCHEDULED = new WSPGMSendSQL.SendSQLSoapClient();
            StringBuilder sbSch = new StringBuilder();
            sbSch.AppendLine("<Data>");
            sbSch.AppendLine("<FDDATE>" + _Date + "</FDDATE>");
            sbSch.AppendLine("<FSCHANNEL_ID>" + _ChannelID + "</FSCHANNEL_ID>");

            sbSch.AppendLine("</Data>");
            SP_Q_TBPGM_PROMO_SCHEDULED.Do_QueryAsync("SP_Q_TBPGM_PROMO_SCHEDULED_BY_CHANNEL_DATE", sbSch.ToString());
            SP_Q_TBPGM_PROMO_SCHEDULED.Do_QueryCompleted += (s2, args2) =>
            {
                if (args2.Error == null)
                {
                    if (args2.Result != null && args2.Result != "")
                    {
                        List<Map_Video_ID> List_Map_video_ID = new List<Map_Video_ID>();

                        byte[] byteArray2 = Encoding.Unicode.GetBytes(args2.Result);
                        StringBuilder output2 = new StringBuilder();
                        // Create an XmlReader
                        Promo_Scheduled_List.Clear();
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray2));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            string QVideoID = XElementToString(elem.Element("FSVIDEO_ID"));
                            if (QVideoID == "")
                            {
                                //先看List_Map_video_ID 中是否已經加入過此筆資料,沒有則要加入
                                bool needAdd = true;
                                for (int i = 0; i <= List_Map_video_ID.Count - 1; i++)
                                {
                                    if (List_Map_video_ID[i].FSID == XElementToString(elem.Element("FSPROMO_ID")))  //因為為promo所以只要ID相同即可
                                    {
                                        needAdd = false;
                                        
                                    }
                                }
                                if (needAdd == true)
                                {
                                List_Map_video_ID.Add(new Map_Video_ID()
                                {
                                    FSID = XElementToString(elem.Element("FSPROMO_ID")),
                                    FNEPISODE = "0",
                                    FSCHANNEL_ID = XElementToString(elem.Element("FSCHANNEL_ID")),
                                    FSVIDEO_ID="",
                                });
                                }

                                
                            }
                            Promo_Scheduled_List.Add(new Promo_SCHEDULED_List()
                            {
                                ISCHECK = false,
                                FNPROMO_BOOKING_NO = XElementToString(elem.Element("FNPROMO_BOOKING_NO")),
                                FSPROMO_ID = XElementToString(elem.Element("FSPROMO_ID")),
                                FNNO = XElementToString(elem.Element("FNNO")),
                                FDDATE = XElementToString(elem.Element("FDDATE")),
                                FSTIME = XElementToString(elem.Element("FSTIME")),
                                FCSTATUS = XElementToString(elem.Element("FCSTATUS")),
                                FSCHANNEL_ID = XElementToString(elem.Element("FSCHANNEL_ID")),
                                FSCHANNEL_NAME = XElementToString(elem.Element("FSCHANNEL_NAME")),
                                FSPROMO_NAME = XElementToString(elem.Element("FSPROMO_NAME")),
                                FSDURATION = XElementToString(elem.Element("FSDURATION")),
                                FSFILE_NO = XElementToString(elem.Element("FSFILE_NO")),
                                FSVIDEO_ID = QVideoID,

                            });
                        }

                       
                        if (List_Map_video_ID.Count != 0)
                        {
                            StringBuilder sbMap = new StringBuilder();
                            sbMap.AppendLine("<Datas>");
                            for (int i = 0; i <= List_Map_video_ID.Count - 1; i++)
                            {
                                sbMap.AppendLine("<Data>");
                                sbMap.AppendLine("<prog_id>" + List_Map_video_ID[i].FSID + "</prog_id>");
                                sbMap.AppendLine("<Episode>" + List_Map_video_ID[i].FNEPISODE + "</Episode>");
                                sbMap.AppendLine("<channelid>" + List_Map_video_ID[i].FSCHANNEL_ID + "</channelid>");

                                sbMap.AppendLine("</Data>");
                            }
                            sbMap.AppendLine("</Datas>");

                            //開始將List_Map_video_ID資料送到後端去取得video_id
                            TransProgList.WebService1SoapClient MapProg_Episode_VideoID = new TransProgList.WebService1SoapClient();

                            MapProg_Episode_VideoID.MapVideoIDListAsync(sbMap.ToString());
                            MapProg_Episode_VideoID.MapVideoIDListCompleted += (s1, args1) =>
                            {
                                if (args1.Error == null)
                                {
                                    if (args1.Result == "<Datas></Datas>")
                                    {
                                    }
                                    else //要將VideoID 寫入
                                    {

                                        byte[] byteArray = Encoding.Unicode.GetBytes(args1.Result);
                                        //PGM_ARR_PROMO.Clear();
                                        StringBuilder output = new StringBuilder();
                                        // Create an XmlReader
                                        XDocument doc1 = XDocument.Load(new MemoryStream(byteArray));
                                        var ltox1 = from str in doc1.Elements("Datas").Elements("Data")
                                                   select str;
                                        
                                        foreach (XElement elem in ltox1)
                                        {
                                            string prog_id = "";
                                            string Episode = "";
                                            string channelid = "";
                                            string video_id = "";
                                            prog_id = XElementToString(elem.Element("prog_id"));
                                            Episode = XElementToString(elem.Element("Episode"));
                                            channelid = XElementToString(elem.Element("channelid"));
                                            video_id = XElementToString(elem.Element("video_id"));

                                            for (int i = 0; i <= Promo_Scheduled_List.Count - 1; i++)
                                            {
                                                if (Promo_Scheduled_List[i].FSPROMO_ID == prog_id)  //因為為promo所以只要ID相同即可
                                                {
                                                    Promo_Scheduled_List[i].FSVIDEO_ID = video_id;

                                                }
                                            }
                                           
                                        }

                                     }
                                    DCdataGridPromoList.ItemsSource = null;
                                    DCdataGridPromoList.ItemsSource = Promo_Scheduled_List;
                                }
                            };
                        }

                        DCdataGridPromoList.ItemsSource = null;
                        DCdataGridPromoList.ItemsSource = Promo_Scheduled_List;
                    }
                }
            };
        }

        List<ArrPromo_LOST_List> Arr_Promo_Lost_List = new List<ArrPromo_LOST_List>();
        public void QueryLostPromo()
        {
            DCdataGridLostPromo.ItemsSource = null;
            //循序查詢SCH
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_ARR_PROMO_LOST = new WSPGMSendSQL.SendSQLSoapClient();
            StringBuilder sbSch = new StringBuilder();
            sbSch.AppendLine("<Data>");
            sbSch.AppendLine("<FDDATE>" + _Date + "</FDDATE>");
            sbSch.AppendLine("<FSCHANNEL_ID>" + _ChannelID + "</FSCHANNEL_ID>");

            sbSch.AppendLine("</Data>");
            SP_Q_TBPGM_ARR_PROMO_LOST.Do_QueryAsync("SP_Q_TBPGM_ARR_PROMO_LOST", sbSch.ToString());
            SP_Q_TBPGM_ARR_PROMO_LOST.Do_QueryCompleted += (s2, args2) =>
            {
                if (args2.Error == null)
                {
                    if (args2.Result != null && args2.Result != "")
                    {
                        byte[] byteArray2 = Encoding.Unicode.GetBytes(args2.Result);
                        StringBuilder output2 = new StringBuilder();
                        // Create an XmlReader
                        Arr_Promo_Lost_List.Clear();
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray2));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            string QVideoID = XElementToString(elem.Element("FSVIDEO_ID"));
                            //if (QVideoID == "")
                            //{
                            //    TransProgList.WebService1SoapClient MapProg_Episode_VideoID = new TransProgList.WebService1SoapClient();

                            //    MapProg_Episode_VideoID.MapVideoIDAsync(_ChannelID, XElementToString(elem.Element("FSPROMO_ID")), "0");
                            //    MapProg_Episode_VideoID.MapVideoIDCompleted += (s1, args1) =>
                            //    {
                            //        if (args1.Error == null)
                            //        {
                            //            if (args1.Result == "取得Video ID有誤")
                            //            {
                            //                MessageBox.Show("對應宣傳帶與VideoID發生錯誤,不可以插入此宣傳帶!");
                            //                return;
                            //            }
                            //            else //要將VideoID 寫入
                            //            {
                            //                QVideoID = args1.Result;
                            //            }
                            //        }
                            //    };
                            //}

                            Arr_Promo_Lost_List.Add(new ArrPromo_LOST_List()
                            {

                                ISCHECK = false,
                                FNCREATE_LIST_NO = XElementToString(elem.Element("FNCREATE_LIST_NO")),
                                COMBINENO = XElementToString(elem.Element("FNARR_PROMO_NO")),
                                FCTIME_TYPE = "",
                                FSPROG_ID = XElementToString(elem.Element("FSPROG_ID")),
                                FNSEQNO = XElementToString(elem.Element("FNSEQNO")),
                                FSNAME = XElementToString(elem.Element("FSPROMO_NAME")),
                                FSCHANNEL_ID = XElementToString(elem.Element("FSCHANNEL_ID")),
                                FSCHANNEL_NAME = XElementToString(elem.Element("FSCHANNEL_NAME")),
                                FSPLAY_TIME = XElementToString(elem.Element("FSPLAYTIME")),
                                RealFSPLAY_TIME = GetRealTImeCode(XElementToString(elem.Element("FSPLAYTIME"))),
                                FSDURATION = XElementToString(elem.Element("FSDUR")),
                                FNEPISODE = XElementToString(elem.Element("FNEPISODE")),
                                FNBREAK_NO = XElementToString(elem.Element("FNBREAK_NO")),
                                FNSUB_NO = XElementToString(elem.Element("FNSUB_NO")),
                                FSFILE_NO = XElementToString(elem.Element("FSFILE_NO")),
                                FSVIDEO_ID = QVideoID,
                                FSPROMO_ID = XElementToString(elem.Element("FSPROMO_ID")),
                                FDDATE = XElementToString(elem.Element("FDDATE")),
                                FNLIVE = XElementToString(elem.Element("FNLIVE")),
                                FCTYPE = XElementToString(elem.Element("FCTYPE")),
                                FSSTATUS = XElementToString(elem.Element("FSSTATUS")),
                                FSSIGNAL = XElementToString(elem.Element("FSSIGNAL")),
                                FSMEMO = XElementToString(elem.Element("FSMEMO")),
                                FSMEMO1 = XElementToString(elem.Element("FSMEMO1")),
                                FSPROG_NAME = XElementToString(elem.Element("FSPROG_NAME")),

                            });
                        }

                        DCdataGridLostPromo.ItemsSource = Arr_Promo_Lost_List;

                    }
                }
            };
        }

        private void buttonBookingList_Click(object sender, RoutedEventArgs e)
        {
            if (_Date == "")
            {
                MessageBox.Show("請先選擇日期");
                return;
            }
            if (_ChannelID == "")
            {
                MessageBox.Show("請先選擇頻道");
                return;
            }
            QueryDateSchPromo();
            canvasBooking.Visibility = Visibility.Visible;

        }

        private void buttonPromoInfo_Click(object sender, RoutedEventArgs e)
        {
            if (_Date == "")
            {
                MessageBox.Show("請先選擇日期");
                return;
            }
            if (_ChannelID == "")
            {
                MessageBox.Show("請先選擇頻道");
                return;
            }
            PGM125_03 PGM125_03_Frm = new PGM125_03();
            PGM125_03_Frm._Date = _Date;

            if ((Convert.ToDateTime(_Date)).DayOfWeek.ToString() == "Monday")
                PGM125_03_Frm._week = "1______";
            else if ((Convert.ToDateTime(_Date)).DayOfWeek.ToString() == "Tuesday")
                PGM125_03_Frm._week = "_1_____";
            else if ((Convert.ToDateTime(_Date)).DayOfWeek.ToString() == "Wednesday")
                PGM125_03_Frm._week = "__1____";
            else if ((Convert.ToDateTime(_Date)).DayOfWeek.ToString() == "Thursday")
                PGM125_03_Frm._week = "___1___";
            else if ((Convert.ToDateTime(_Date)).DayOfWeek.ToString() == "Friday")
                PGM125_03_Frm._week = "____1__";
            else if ((Convert.ToDateTime(_Date)).DayOfWeek.ToString() == "Saturday")
                PGM125_03_Frm._week = "_____1_";
            else if ((Convert.ToDateTime(_Date)).DayOfWeek.ToString() == "Sunday")
                PGM125_03_Frm._week = "______1";
            PGM125_03_Frm._channelid = _ChannelID;



            if (PGM_ARR_PROMO.Count < 0)
            {
                MessageBox.Show("請先查詢播出運行表");
                return;
            }

            for (int i = 0; i <= PGM_ARR_PROMO.Count - 1; i++)
            {
                if (PGM_ARR_PROMO[i].FCTYPE == "P" && PGM_ARR_PROMO[i].FSNAME.Substring(0, 2) != "RM")   //如果是宣傳帶才要做,而且不為RM
                {
                    PGM125_03_Frm.Date_Arr_Promo_List.Add(new Date_Arr_Promo()
                    {
                        FSPROMO_ID = PGM_ARR_PROMO[i].FSPROMO_ID,
                        FSPROMO_NAME = PGM_ARR_PROMO[i].FSNAME,
                        FSPROG_ID = PGM_ARR_PROMO[i].FSPROG_ID,
                        FSPROG_NAME = PGM_ARR_PROMO[i].FSPROG_NAME,
                        FSPLAYTIME = PGM_ARR_PROMO[i].RealFSPLAY_TIME,
                    });
                    bool needAdd = true;
                    for (int j = 0; j <= PGM125_03_Frm.Date_Arr_Promo_Count_List.Count - 1; j++)
                    {
                        if (PGM125_03_Frm.Date_Arr_Promo_Count_List[j].FSPROMO_ID == PGM_ARR_PROMO[i].FSPROMO_ID)
                        { //當已經有此之宣傳帶時,數量要加一
                            PGM125_03_Frm.Date_Arr_Promo_Count_List[j].INSERT_COUNT = (int.Parse(PGM125_03_Frm.Date_Arr_Promo_Count_List[j].INSERT_COUNT) + 1).ToString();
                            needAdd = false;
                        }

                    }
                    if (needAdd == true)
                    {
                        PGM125_03_Frm.Date_Arr_Promo_Count_List.Add(new Date_Arr_Promo_Count()
                        {
                            FSPROMO_ID = PGM_ARR_PROMO[i].FSPROMO_ID,
                            FSPROMO_NAME = PGM_ARR_PROMO[i].FSNAME,
                            INSERT_COUNT = "1",
                        });
                    }
                }
            }

            PGM125_03_Frm.QueryPlan();
            PGM125_03_Frm.Show();
            PGM125_03_Frm.Closed += (s, args) =>
            {
            };
        }

        private void ButtonChooseImport_Click(object sender, RoutedEventArgs e)
        {

            StringBuilder sb = new StringBuilder();
            Add_Work_Change();
            sb.AppendLine("<Datas>");
            for (int i = 0; i <= Promo_Scheduled_List.Count - 1; i++)
            {
                if (Promo_Scheduled_List[i].ISCHECK == true)
                {
                    bool CheckTimeOver = false;
                    for (int j = 0; j <= PGM_ARR_PROMO.Count - 1; j++)
                    {
                        if (PGM_ARR_PROMO[j].FSPLAY_TIME != "")
                        {
                            string InTimecode;
                            InTimecode = Promo_Scheduled_List[i].FSTIME.Substring(0, 2) + ":" + Promo_Scheduled_List[i].FSTIME.Substring(2, 2) + ":00;00";
                            if (TransferTimecode.timecodetoframe(InTimecode) <= TransferTimecode.timecodetoframe(PGM_ARR_PROMO[j].FSPLAY_TIME) && CheckTimeOver == false)
                            { //當要插入的時間大於播出時間時
                                if (InsertPromo(j, Promo_Scheduled_List[i].FSPROMO_ID, Promo_Scheduled_List[i].FSPROMO_NAME, Promo_Scheduled_List[i].FSDURATION, Promo_Scheduled_List[i].FSVIDEO_ID, Promo_Scheduled_List[i].FSFILE_NO,"COMM") == true)
                                {
                                    Promo_Scheduled_List[i].FCSTATUS = "Y";
                                    sb.AppendLine("<Data>");
                                    sb.AppendLine("<FNPROMO_BOOKING_NO>" + Promo_Scheduled_List[i].FNPROMO_BOOKING_NO + "</FNPROMO_BOOKING_NO>");
                                    sb.AppendLine("<FNNO>" + Promo_Scheduled_List[i].FNNO + "</FNNO>");
                                    sb.AppendLine("<FCSTATUS>" + "Y" + "</FCSTATUS>");
                                    sb.AppendLine("</Data>");
                                    CheckTimeOver = true;
                                    ReCalTimecode();
                                }
                            }
                        }

                    }

                }
            }

            sb.AppendLine("</Datas>");


            WSPGMSendSQL.SendSQLSoapClient SP_U_TBPGM_PROMO_SCHEDULED = new WSPGMSendSQL.SendSQLSoapClient();


            SP_U_TBPGM_PROMO_SCHEDULED.Do_Insert_ListAsync("", "", "SP_U_TBPGM_PROMO_SCHEDULED", sb.ToString());
            SP_U_TBPGM_PROMO_SCHEDULED.Do_Insert_ListCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result == true)
                    {
                        dataGridProgList.ItemsSource = null;
                        dataGridProgList.ItemsSource = PGM_ARR_PROMO;
                        DCdataGridPromoList.ItemsSource = null;
                        DCdataGridPromoList.ItemsSource = Promo_Scheduled_List;
                        MessageBox.Show("依選擇移入作業完成");
                    }
                    else
                        MessageBox.Show("無法修改資料庫狀態");
                }
            };
        }

        private void ButtonAllImport_Click(object sender, RoutedEventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            Add_Work_Change();
            sb.AppendLine("<Datas>");
            for (int i = 0; i <= Promo_Scheduled_List.Count - 1; i++)
            {
                if (Promo_Scheduled_List[i].FCSTATUS == "N")
                {
                    bool CheckTimeOver = false;
                    for (int j = 0; j <= PGM_ARR_PROMO.Count - 1; j++)
                    {
                        if (PGM_ARR_PROMO[j].FSPLAY_TIME != "")
                        {
                            string InTimecode;
                            InTimecode = Promo_Scheduled_List[i].FSTIME.Substring(0, 2) + ":" + Promo_Scheduled_List[i].FSTIME.Substring(2, 2) + ":00;00";
                            if (TransferTimecode.timecodetoframe(InTimecode) <= TransferTimecode.timecodetoframe(PGM_ARR_PROMO[j].FSPLAY_TIME) && CheckTimeOver == false)
                            { //當要插入的時間大於播出時間時
                                InsertPromo(j, Promo_Scheduled_List[i].FSPROMO_ID, Promo_Scheduled_List[i].FSPROMO_NAME, Promo_Scheduled_List[i].FSDURATION, Promo_Scheduled_List[i].FSVIDEO_ID, Promo_Scheduled_List[i].FSFILE_NO, "COMM");
                                Promo_Scheduled_List[i].FCSTATUS = "Y";
                                sb.AppendLine("<Data>");
                                sb.AppendLine("<FNPROMO_BOOKING_NO>" + Promo_Scheduled_List[i].FNPROMO_BOOKING_NO + "</FNPROMO_BOOKING_NO>");
                                sb.AppendLine("<FNNO>" + Promo_Scheduled_List[i].FNNO + "</FNNO>");
                                sb.AppendLine("<FCSTATUS>" + "Y" + "</FCSTATUS>");
                                sb.AppendLine("</Data>");
                                CheckTimeOver = true;
                                ReCalTimecode();
                            }
                        }

                    }

                }
            }
            sb.AppendLine("</Datas>");
            WSPGMSendSQL.SendSQLSoapClient SP_U_TBPGM_PROMO_SCHEDULED = new WSPGMSendSQL.SendSQLSoapClient();
            SP_U_TBPGM_PROMO_SCHEDULED.Do_Insert_ListAsync("", "", "SP_U_TBPGM_PROMO_SCHEDULED", sb.ToString());
            SP_U_TBPGM_PROMO_SCHEDULED.Do_Insert_ListCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result == true)
                    {
                        dataGridProgList.ItemsSource = null;
                        dataGridProgList.ItemsSource = PGM_ARR_PROMO;
                        DCdataGridPromoList.ItemsSource = null;
                        DCdataGridPromoList.ItemsSource = Promo_Scheduled_List;
                        MessageBox.Show("全部移入作業完成");
                    }
                    else
                        MessageBox.Show("無法修改資料庫狀態");
                }
            };
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            canvasBooking.Visibility = Visibility.Collapsed;
        }

        public bool InsertPromo(int InsertRow, string InsPromoID, string InsPromoName, string InsPromoDuration, string InsPromoVideoID, string InsPromoFileNO, string InsPromoSIGNAL)
        {
            ArrPromoList Temp_ArrPromo = new ArrPromoList();
            int i = InsertRow;
            if (PGM_ARR_PROMO[InsertRow].FCTYPE == "P") //如果所選位置是Promo
            {
                Temp_ArrPromo.COMBINENO = "";
                Temp_ArrPromo.FCTIME_TYPE = "A";
                Temp_ArrPromo.FSPROG_ID = PGM_ARR_PROMO[InsertRow].FSPROG_ID;
                Temp_ArrPromo.FNSEQNO = PGM_ARR_PROMO[InsertRow].FNSEQNO;
                Temp_ArrPromo.FSNAME = InsPromoName;
                Temp_ArrPromo.FSCHANNEL_ID = PGM_ARR_PROMO[InsertRow].FSCHANNEL_ID;
                Temp_ArrPromo.FSCHANNEL_NAME = PGM_ARR_PROMO[InsertRow].FSCHANNEL_NAME;

                Temp_ArrPromo.FSPLAY_TIME = PGM_ARR_PROMO[InsertRow].FSPLAY_TIME;
                Temp_ArrPromo.FSDURATION = InsPromoDuration;
                Temp_ArrPromo.FNEPISODE = PGM_ARR_PROMO[InsertRow].FNEPISODE;
                Temp_ArrPromo.FNBREAK_NO = PGM_ARR_PROMO[InsertRow].FNBREAK_NO;
                //由於是promo,所以插入promo的支數是相同的,之後的PromosubNO要加一
                Temp_ArrPromo.FNSUB_NO = PGM_ARR_PROMO[InsertRow].FNSUB_NO;

                Temp_ArrPromo.FSFILE_NO = InsPromoFileNO;
                Temp_ArrPromo.FSVIDEO_ID = InsPromoVideoID;
                Temp_ArrPromo.FSPROMO_ID = InsPromoID;
                Temp_ArrPromo.FDDATE = PGM_ARR_PROMO[InsertRow].FDDATE;
                Temp_ArrPromo.FNLIVE = "0"; //表示LIVE
                Temp_ArrPromo.FCTYPE = "P"; //表示宣傳帶
                Temp_ArrPromo.FSSIGNAL = InsPromoSIGNAL;
                Temp_ArrPromo.FSMEMO = "";
                Temp_ArrPromo.FSMEMO1 = "";
                Temp_ArrPromo.FSPROG_NAME = PGM_ARR_PROMO[InsertRow].FSPROG_NAME;
                //if (CheckPromoInsertTime(Temp_ArrPromo.FSPLAY_TIME) == false)
                //{
                //    //MessageBox.Show("不在有效時間內");
                //    return false;
                //}

                Temp_ArrPromo.RealFSPLAY_TIME = GetRealTImeCode(Temp_ArrPromo.FSPLAY_TIME);
                //Temp_ArrPromo.FSSIGNAL = "";
                //Temp_ArrPromo.FSMEMO1 = "";
                PGM_ARR_PROMO.Insert(InsertRow, Temp_ArrPromo);
                //之後的PromosubNO要加一
                i = i + 1; //從下一筆開始
                while (i < PGM_ARR_PROMO.Count - 1 && PGM_ARR_PROMO[i].FCTYPE == "P")
                {
                    PGM_ARR_PROMO[i].FNSUB_NO = (int.Parse(PGM_ARR_PROMO[i].FNSUB_NO) + 1).ToString();
                    i = i + 1;
                }
            }
            else if (PGM_ARR_PROMO[InsertRow].FCTYPE == "G")//如果所選位置是節目則要看前一筆資料的訊息
            {
                int j;
                if (i == 0) //如果為第一筆資料,前面沒資料
                {
                    MessageBox.Show("不能在此插入短帶");
                }
                else //如果不是第一段
                {
                    j = InsertRow - 1;
                    if (PGM_ARR_PROMO[j].FSPROG_ID != "")
                    {

                        Temp_ArrPromo.COMBINENO = "";
                        Temp_ArrPromo.FCTIME_TYPE = "A";
                        Temp_ArrPromo.FSPROG_ID = PGM_ARR_PROMO[j].FSPROG_ID;
                        Temp_ArrPromo.FNSEQNO = PGM_ARR_PROMO[j].FNSEQNO;
                        Temp_ArrPromo.FSNAME = InsPromoName;
                        Temp_ArrPromo.FSCHANNEL_ID = PGM_ARR_PROMO[j].FSCHANNEL_ID;
                        Temp_ArrPromo.FSCHANNEL_NAME = PGM_ARR_PROMO[j].FSCHANNEL_NAME;

                        Temp_ArrPromo.FSPLAY_TIME = PGM_ARR_PROMO[j].FSPLAY_TIME;
                        Temp_ArrPromo.FSDURATION = InsPromoDuration;
                        Temp_ArrPromo.FNEPISODE = PGM_ARR_PROMO[j].FNEPISODE;
                        Temp_ArrPromo.FNBREAK_NO = PGM_ARR_PROMO[j].FNBREAK_NO;
                        //由於是promo,所以插入promo的支數是相同的,之後的PromosubNO要加一
                        if (PGM_ARR_PROMO[j].FCTYPE == "P") //如果前一筆為Promo則採用前一筆的支數+1
                            Temp_ArrPromo.FNSUB_NO = (int.Parse(PGM_ARR_PROMO[j].FNSUB_NO) + 1).ToString();
                        else //不然為第一支
                            Temp_ArrPromo.FNSUB_NO = "1";

                        Temp_ArrPromo.FSFILE_NO = InsPromoFileNO;
                        Temp_ArrPromo.FSVIDEO_ID = InsPromoVideoID;
                        Temp_ArrPromo.FSPROMO_ID = InsPromoID;
                        Temp_ArrPromo.FDDATE = PGM_ARR_PROMO[j].FDDATE;
                        Temp_ArrPromo.FNLIVE = "0"; //表示非LIVE
                        Temp_ArrPromo.FSSIGNAL = InsPromoSIGNAL; 
                        Temp_ArrPromo.FCTYPE = "P"; //表示宣傳帶
                        Temp_ArrPromo.FSMEMO = "";
                        Temp_ArrPromo.FSPROG_NAME = PGM_ARR_PROMO[j].FSPROG_NAME;
                        //if (CheckPromoInsertTime(Temp_ArrPromo.FSPLAY_TIME) == false)
                        //{
                        //    //MessageBox.Show("不在有效時間內");
                        //    return false;
                        //}
                        PGM_ARR_PROMO.Insert(InsertRow, Temp_ArrPromo);
                    }
                    else //如果插在節目標題上,那就是此節目第一支
                    {
                        j = InsertRow;
                        Temp_ArrPromo.COMBINENO = "";
                        Temp_ArrPromo.FCTIME_TYPE = "A";
                        Temp_ArrPromo.FSPROG_ID = PGM_ARR_PROMO[j].FSPROG_ID;
                        Temp_ArrPromo.FNSEQNO = PGM_ARR_PROMO[j].FNSEQNO;
                        Temp_ArrPromo.FSNAME = InsPromoName;
                        Temp_ArrPromo.FSCHANNEL_ID = PGM_ARR_PROMO[j].FSCHANNEL_ID;
                        Temp_ArrPromo.FSCHANNEL_NAME = PGM_ARR_PROMO[j].FSCHANNEL_NAME;

                        Temp_ArrPromo.FSPLAY_TIME = PGM_ARR_PROMO[j].FSPLAY_TIME;
                        Temp_ArrPromo.FSDURATION = InsPromoDuration;
                        Temp_ArrPromo.FNEPISODE = PGM_ARR_PROMO[j].FNEPISODE;
                        Temp_ArrPromo.FNBREAK_NO = (int.Parse(PGM_ARR_PROMO[j].FNBREAK_NO) - 1).ToString();
                        //由於是promo,所以插入promo的支數是相同的,之後的PromosubNO要加一
                        Temp_ArrPromo.FNSUB_NO = "1";

                        Temp_ArrPromo.FSFILE_NO = InsPromoFileNO;
                        Temp_ArrPromo.FSVIDEO_ID = InsPromoVideoID;
                        Temp_ArrPromo.FSPROMO_ID = InsPromoID;
                        Temp_ArrPromo.FDDATE = PGM_ARR_PROMO[j].FDDATE;
                        Temp_ArrPromo.FNLIVE = "0"; //表示非LIVE
                        Temp_ArrPromo.FCTYPE = "P"; //表示宣傳帶
                        Temp_ArrPromo.FSSIGNAL = InsPromoSIGNAL; 
                        Temp_ArrPromo.FSMEMO = "";
                        Temp_ArrPromo.FSPROG_NAME = PGM_ARR_PROMO[j].FSPROG_NAME;
                        //if (CheckPromoInsertTime(Temp_ArrPromo.FSPLAY_TIME) == false)
                        //{
                        //    //MessageBox.Show("不在有效時間內");
                        //    return false;
                        //}
                        PGM_ARR_PROMO.Insert(InsertRow, Temp_ArrPromo);
                    }
                }
            }
            else if (PGM_ARR_PROMO[InsertRow].FCTYPE == "")//如果所選位置是節目抬頭則要看前一筆資料的訊息
            {
                int j = dataGridProgList.SelectedIndex - 1;
                while (j > 0 && PGM_ARR_PROMO[j].FSPROG_ID == "") //先找到之前一筆的資料
                {
                    j = j - 1;
                }
                if (j > 0)
                {

                    Temp_ArrPromo.COMBINENO = "";
                    Temp_ArrPromo.FCTIME_TYPE = "A";
                    Temp_ArrPromo.FSPROG_ID = PGM_ARR_PROMO[j].FSPROG_ID;
                    Temp_ArrPromo.FNSEQNO = PGM_ARR_PROMO[j].FNSEQNO;
                    Temp_ArrPromo.FSNAME = InsPromoName;
                    Temp_ArrPromo.FSCHANNEL_ID = PGM_ARR_PROMO[j].FSCHANNEL_ID;
                    Temp_ArrPromo.FSCHANNEL_NAME = PGM_ARR_PROMO[j].FSCHANNEL_NAME;

                    Temp_ArrPromo.FSPLAY_TIME = PGM_ARR_PROMO[j].FSPLAY_TIME;
                    Temp_ArrPromo.FSDURATION = InsPromoDuration;
                    Temp_ArrPromo.FNEPISODE = PGM_ARR_PROMO[j].FNEPISODE;
                    Temp_ArrPromo.FNBREAK_NO = PGM_ARR_PROMO[j].FNBREAK_NO;
                    //由於是promo,所以插入promo的支數是相同的,之後的PromosubNO要加一
                    if (PGM_ARR_PROMO[j].FCTYPE == "P")
                        Temp_ArrPromo.FNSUB_NO = (int.Parse(PGM_ARR_PROMO[j].FNSUB_NO) + 1).ToString();
                    else
                        Temp_ArrPromo.FNSUB_NO = "1";

                    Temp_ArrPromo.FSFILE_NO = InsPromoFileNO;
                    Temp_ArrPromo.FSVIDEO_ID = InsPromoVideoID;
                    Temp_ArrPromo.FSPROMO_ID = InsPromoID;
                    Temp_ArrPromo.FDDATE = PGM_ARR_PROMO[j].FDDATE;
                    Temp_ArrPromo.FNLIVE = "0"; //表示非LIVE
                    Temp_ArrPromo.FCTYPE = "P"; //表示宣傳帶
                    Temp_ArrPromo.FSSIGNAL = InsPromoSIGNAL; 
                    Temp_ArrPromo.FSMEMO = "";
                    Temp_ArrPromo.FSPROG_NAME = PGM_ARR_PROMO[j].FSPROG_NAME;
                    //if (CheckPromoInsertTime(Temp_ArrPromo.FSPLAY_TIME) == false)
                    //{
                    //    //MessageBox.Show("不在有效時間內");
                    //    return false;
                    //}
                    PGM_ARR_PROMO.Insert(InsertRow, Temp_ArrPromo);
                    //因為一定是最後一支所以不用修改之後的支數
                }

            }
            return true;
        }

        private void buttonPrintList_Click(object sender, RoutedEventArgs e)
        {
            //CommonHelper.OpenURL("http://10.13.220.35/ReportServer/Pages/ReportViewer.aspx?/PTS_MAM3/RPT_TBPGM_ARR_PROMO_01&QUERY_KEY=de9839b2-2e6c-4353-8206-a592b927ac8a&rc:parameters=false&rs:Command=Render");
            //return;
            //List<ArrPromoList> PGM_ARR_PROMO = new List<ArrPromoList>();
            if (PGM_ARR_PROMO.Count <= 0)
            {
                MessageBox.Show("請先查詢節目表");
                return;
            }
            if (_ChannelID == "")
            {
                MessageBox.Show("請先查詢節目表");
                return;
            }
            if (ModuleClass.getModulePermission("P5" + _ChannelID + "007") == false)
            {
                MessageBox.Show("您沒有列印此頻道播出運行表的權限");
                return;
            }
            BusyMsg.IsBusy = true;
            int NO_TAPE_COUNT = 0;
            StringBuilder sb = new StringBuilder();
            int ListNo = 1;
            sb.AppendLine("<Datas>");
            Guid g;
            g = Guid.NewGuid();

            WSPGMSendSQL.SendSQLSoapClient SP_I_RPT_TBPGM_ARR_PROMO_01 = new WSPGMSendSQL.SendSQLSoapClient();

            foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO)
            {
                if ((TemArrPromoList.FCTYPE == "P") || (TemArrPromoList.FCTYPE == "G"))
                {
                    NO_TAPE_COUNT = NO_TAPE_COUNT + 1;
                    Guid g1;
                    g1 = Guid.NewGuid();

                    sb.AppendLine("<Data>");
                    sb.AppendLine("<QUERY_KEY>" + g.ToString() + "</QUERY_KEY>");
                    sb.AppendLine("<QUERY_KEY_SUBID>" + g1.ToString() + "</QUERY_KEY_SUBID>");
                    sb.AppendLine("<QUERY_BY>" + UserClass.userData.FSUSER_ChtName.ToString() + "</QUERY_BY>");
                    //sb.AppendLine("<QUERY_DATE>" + _Date + "</QUERY_DATE>"); //getdate()
                    
                    //sb.AppendLine("<序號>" + TemArrPromoList.COMBINENO + "</序號>");
                    sb.AppendLine("<序號>" + ListNo.ToString() + "</序號>");
                    ListNo = ListNo + 1;
                    sb.AppendLine("<時間類型>" + TemArrPromoList.FCTIME_TYPE + "</時間類型>");
                    sb.AppendLine("<節目編碼>" + TemArrPromoList.FSPROG_ID.Trim() + "</節目編碼>");
                    sb.AppendLine("<播映序號>" + TemArrPromoList.FNSEQNO + "</播映序號>");

                    if (TemArrPromoList.FCTYPE == "G")
                    {
                        sb.AppendLine("<名稱>" + TransferTimecode.ReplaceXML(TemArrPromoList.FSNAME) + "#" + TemArrPromoList.FNEPISODE + "</名稱>");
                        //sb.AppendLine("<節目名稱>" + TemArrPromoList.FSNAME + "</節目名稱>");
                        sb.AppendLine("<短帶編碼>" + "" + "</短帶編碼>");
                        //sb.AppendLine("<宣傳帶名稱>" + "" + "</宣傳帶名稱>");
                    }
                    else
                    {
                        sb.AppendLine("<名稱>" + TransferTimecode.ReplaceXML(TemArrPromoList.FSNAME) + "</名稱>");
                        //sb.AppendLine("<節目名稱>" + TemArrPromoList.FSPROG_NAME + "</節目名稱>");
                        sb.AppendLine("<短帶編碼>" + TemArrPromoList.FSPROMO_ID + "</短帶編碼>");
                        //sb.AppendLine("<宣傳帶名稱>" + TemArrPromoList.FSNAME + "</宣傳帶名稱>");
                    }
                    sb.AppendLine("<頻道>" + TemArrPromoList.FSCHANNEL_NAME + "</頻道>");
                    sb.AppendLine("<節目集數>" + TemArrPromoList.FNEPISODE + "</節目集數>");
                    sb.AppendLine("<段數>" + TemArrPromoList.FNBREAK_NO + "</段數>");
                    sb.AppendLine("<支數>" + TemArrPromoList.FNSUB_NO + "</支數>");
                    sb.AppendLine("<播出日期>" + TemArrPromoList.FDDATE + "</播出日期>");
                    sb.AppendLine("<播出時間>" + TemArrPromoList.RealFSPLAY_TIME + "</播出時間>");
                    sb.AppendLine("<長度>" + TemArrPromoList.FSDURATION + "</長度>");
                    sb.AppendLine("<影像編號>" + TemArrPromoList.FSFILE_NO + "</影像編號>");
                    sb.AppendLine("<主控編號>" + TemArrPromoList.FSVIDEO_ID + "</主控編號>");
                    sb.AppendLine("<資料類型>" + TemArrPromoList.FCTYPE + "</資料類型>");
                    sb.AppendLine("<備註>" + TransferTimecode.ReplaceXML(TemArrPromoList.FSMEMO) + "</備註>");
                    sb.AppendLine("<FSSIGNAL>" + TemArrPromoList.FSSIGNAL + "</FSSIGNAL>");
                    if (TemArrPromoList.FCTYPE=="G")
                        sb.AppendLine("<FSRAN>" + "Y" + "</FSRAN>");
                    else
                        sb.AppendLine("<FSRAN>" + "" + "</FSRAN>");
                    sb.AppendLine("<FNLIVE>" + TemArrPromoList.FNLIVE + "</FNLIVE>");
                    sb.AppendLine("</Data>");
                }
            }
            sb.AppendLine("</Datas>");

            if (NO_TAPE_COUNT == 0)
            {
                BusyMsg.IsBusy = false;
                MessageBox.Show("沒有節目表資料");
                return;
            }
            SP_I_RPT_TBPGM_ARR_PROMO_01.Do_Insert_ListAsync("", "", "SP_I_RPT_TBPGM_ARR_PROMO_01", sb.ToString());
            SP_I_RPT_TBPGM_ARR_PROMO_01.Do_Insert_ListCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result == true)
                    {
                        WSPGMSendSQL.SendSQLSoapClient GETURL = new WSPGMSendSQL.SendSQLSoapClient();
                        GETURL.GETReportURLAsync("RPT_TBPGM_ARR_PROMO_01", g.ToString());
                        GETURL.GETReportURLCompleted += (s1, args1) =>
                        {
                            if (args1.Error == null)
                            {
                                if (args1.Result != null && args1.Result != "")
                                {
                                    BusyMsg.IsBusy = false;
                                    if (args1.Result.ToString().StartsWith("錯誤"))
                                        MessageBox.Show(args1.Result.ToString(), "提示訊息", MessageBoxButton.OK);
                                    else
                                    {
                                        HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + args1.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,toolbar=0,statusbar=0,menubar=0,scrollbar=0");
                                        //CommonHelper.OpenURL(@"REPORT.aspx?url=" + args1.Result);
                                        
                                        //base.NavigationService.Source =new Uri("REPORT.aspx?url=" + args1.Result);
                                        //HyperlinkButton Hbutton = new HyperlinkButton();
                                        //Hbutton.NavigateUri=  new Uri("REPORT.aspx?url=" + args1.Result, UriKind.Relative); 
                                        //Hbutton.Click();
                                        //"http://www.bing.com");
                                        //button.ClickMe(); 
             //TargetName = "_blank";   
                                        //var URL = @"http://SiteSelect.aspx";
                                        //SecureString secure = new SecureString();
                                        //char[] passwordChars = Properties.Settings.Default.Password.ToCharArray();
                                        ////converting string to securestring...found from internet
                                        //foreach (char c in passwordChars)
                                        //{
                                        //    secure.AppendChar(c);
                                        //}
                                        //Process.Start(URL, "", Properties.Settings.Default.User, secure, "agent");
                                    }
                                }
                                else
                                {
                                    BusyMsg.IsBusy = false;
                                    MessageBox.Show("啟動報表功能異常，請通知系統管理員", "提示訊息", MessageBoxButton.OK);
                                }
                                //if (args1.Result != null && args1.Result != "")
                                //{
                                //    HtmlPage.Window.Navigate(new Uri("http://localhost/PTS_MAM3.Web/REPORT.aspx?url=" + args1.Result, UriKind.Absolute), "_blank", "height=600,width=800,top=100,left=100,toolbar=0,statusbar=0,menubar=0,scrollbar=0");

                                //}
                            }
                        };
                    }
                    else
                    {
                        BusyMsg.IsBusy = false;
                        MessageBox.Show("列印失敗");
                    }
                }
            };
        }

        public static class CommonHelper
        {
            private class HyperlinkButtonWrapper : HyperlinkButton
            {
                public void OpenURL(string navigateUri)
                {
                    OpenURL(new Uri(navigateUri, UriKind.Absolute));
                }

                public void OpenURL(Uri navigateUri)
                {
                    base.NavigateUri = navigateUri;
                    base.TargetName = "_blank";
                    base.OnClick();
                }
            }

            public static void OpenURL(string navigateUri)
            {
                new HyperlinkButtonWrapper().OpenURL(navigateUri);
            }
        }

        private void buttonSaveArrList_Click(object sender, RoutedEventArgs e)
        {
            BusyMsg.IsBusy = true;
            WSPGMSendSQL.SendSQLSoapClient SP_I_TBPGM_Combinequeue = new WSPGMSendSQL.SendSQLSoapClient();
            StringBuilder sbCombine = new StringBuilder();
            object ReturnXML = "";
            sbCombine.AppendLine("<Combine>");


            StringBuilder sbArrPromo = new StringBuilder();
            sbArrPromo.AppendLine("<ArrPromo>");


            foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO)
            {
                if (TemArrPromoList.FCTYPE == "G") //節目
                {
                    sbCombine.AppendLine("<Data>");
                    sbCombine.AppendLine("<FNCONBINE_NO>" + TemArrPromoList.COMBINENO + "</FNCONBINE_NO>");
                    sbCombine.AppendLine("<FCTIMETYPE>" + TemArrPromoList.FCTIME_TYPE + "</FCTIMETYPE>");
                    sbCombine.AppendLine("<FSPROG_ID>" + TemArrPromoList.FSPROG_ID + "</FSPROG_ID>");
                    sbCombine.AppendLine("<FNSEQNO>" + TemArrPromoList.FNSEQNO + "</FNSEQNO>");
                    sbCombine.AppendLine("<FDDATE>" + _Date + "</FDDATE>");
                    sbCombine.AppendLine("<FSCHANNEL_ID>" + _ChannelID + "</FSCHANNEL_ID>");
                    sbCombine.AppendLine("<FNEPISODE>" + TemArrPromoList.FNEPISODE + "</FNEPISODE>");
                    sbCombine.AppendLine("<FNBREAK_NO>" + TemArrPromoList.FNBREAK_NO + "</FNBREAK_NO>");
                    sbCombine.AppendLine("<FNSUB_NO>" + TemArrPromoList.FNSUB_NO + "</FNSUB_NO>");
                    sbCombine.AppendLine("<FCPROPERTY>" + TemArrPromoList.FCTYPE + "</FCPROPERTY>");
                    sbCombine.AppendLine("<FSPLAY_TIME>" + TemArrPromoList.FSPLAY_TIME + "</FSPLAY_TIME>");
                    sbCombine.AppendLine("<FSDUR>" + TemArrPromoList.FSDURATION + "</FSDUR>");
                    sbCombine.AppendLine("<FSSIGNAL>" + TemArrPromoList.FSSIGNAL + "</FSSIGNAL>");
                    sbCombine.AppendLine("<FNLIVE>" + TemArrPromoList.FNLIVE + "</FNLIVE>");
                    sbCombine.AppendLine("<FSPROMO_ID>" + "" + "</FSPROMO_ID>");
                    if (TemArrPromoList.FSMEMO != null)
                        sbCombine.AppendLine("<FSMEMO>" + TransferTimecode.ReplaceXML(TemArrPromoList.FSMEMO) + "</FSMEMO>");
                    else
                        sbCombine.AppendLine("<FSMEMO>" + "" + "</FSMEMO>");
                    if (TemArrPromoList.FSMEMO1 != null)
                        sbCombine.AppendLine("<FSMEMO1>" + TransferTimecode.ReplaceXML(TemArrPromoList.FSMEMO1) + "</FSMEMO1>");
                    else
                        sbCombine.AppendLine("<FSMEMO1>" + "" + "</FSMEMO1>");
                    sbCombine.AppendLine("<FSCREATED_BY>" + UserClass.userData.FSUSER_ChtName + "</FSCREATED_BY>");

                    if (TemArrPromoList.FSPROG_NAME != null)
                        sbCombine.AppendLine("<FSPROG_NAME>" + TransferTimecode.ReplaceXML(TemArrPromoList.FSPROG_NAME) + "</FSPROG_NAME>");
                    else
                        sbCombine.AppendLine("<FSPROG_NAME>" + "" + "</FSPROG_NAME>");

                    sbCombine.AppendLine("<FSVIDEO_ID>" + TemArrPromoList.FSVIDEO_ID + "</FSVIDEO_ID>");
                    sbCombine.AppendLine("<FSFILE_NO>" + TemArrPromoList.FSFILE_NO + "</FSFILE_NO>");
                    sbCombine.AppendLine("</Data>");
                }
                else if (TemArrPromoList.FCTYPE == "P")  //宣傳帶
                {
                    sbArrPromo.AppendLine("<Data>");
                    sbArrPromo.AppendLine("<FNARR_PROMO_NO>" + TemArrPromoList.COMBINENO + "</FNARR_PROMO_NO>");
                    sbArrPromo.AppendLine("<FSPROG_ID>" + TemArrPromoList.FSPROG_ID + "</FSPROG_ID>");
                    sbArrPromo.AppendLine("<FNSEQNO>" + TemArrPromoList.FNSEQNO + "</FNSEQNO>");
                    sbArrPromo.AppendLine("<FDDATE>" + _Date + "</FDDATE>");
                    sbArrPromo.AppendLine("<FSCHANNEL_ID>" + _ChannelID + "</FSCHANNEL_ID>");
                    sbArrPromo.AppendLine("<FNBREAK_NO>" + TemArrPromoList.FNBREAK_NO + "</FNBREAK_NO>");
                    sbArrPromo.AppendLine("<FNSUB_NO>" + TemArrPromoList.FNSUB_NO + "</FNSUB_NO>");
                    sbArrPromo.AppendLine("<FSPROMO_ID>" + TemArrPromoList.FSPROMO_ID + "</FSPROMO_ID>");
                    sbArrPromo.AppendLine("<FSPLAYTIME>" + TemArrPromoList.FSPLAY_TIME + "</FSPLAYTIME>");
                    sbArrPromo.AppendLine("<FSDUR>" + TemArrPromoList.FSDURATION + "</FSDUR>");
                    sbArrPromo.AppendLine("<FSSIGNAL>" + TemArrPromoList.FSSIGNAL + "</FSSIGNAL>");
                    if (TemArrPromoList.FSMEMO != null)
                        sbArrPromo.AppendLine("<FSMEMO>" + TransferTimecode.ReplaceXML(TemArrPromoList.FSMEMO) + "</FSMEMO>");
                    else
                        sbCombine.AppendLine("<FSMEMO>" + "" + "</FSMEMO>");
                    if (TemArrPromoList.FSMEMO1 != null)
                        sbArrPromo.AppendLine("<FSMEMO1>" + TransferTimecode.ReplaceXML(TemArrPromoList.FSMEMO1) + "</FSMEMO1>");
                    else
                        sbArrPromo.AppendLine("<FSMEMO1>" + "" + "</FSMEMO1>");
                    sbArrPromo.AppendLine("<FSCREATED_BY>" + UserClass.userData.FSUSER_ChtName + "</FSCREATED_BY>");
                    sbArrPromo.AppendLine("<FCTIMETYPE>" + TemArrPromoList.FCTIME_TYPE + "</FCTIMETYPE>");
                    sbArrPromo.AppendLine("<FSFILE_NO>" + TemArrPromoList.FSFILE_NO + "</FSFILE_NO>");
                    sbArrPromo.AppendLine("<FSVIDEO_ID>" + TemArrPromoList.FSVIDEO_ID + "</FSVIDEO_ID>");
                    sbArrPromo.AppendLine("</Data>");
                }
            }
            sbCombine.AppendLine("</Combine>");
            sbArrPromo.AppendLine("</ArrPromo>");

            //sb=sb.Replace("\r\n", "");
            SP_I_TBPGM_Combinequeue.Do_Insert_TBPGM_Combine_queueAsync("", _Date, _ChannelID, sbCombine.ToString(), sbArrPromo.ToString());
            SP_I_TBPGM_Combinequeue.Do_Insert_TBPGM_Combine_queueCompleted += (s, args) =>
            {
                if (args.Error == null)
                {


                    if (myDispatcherTimer.IsEnabled == true)
                        myDispatcherTimer.Stop();
                    BusyMsg.IsBusy = false;
                    if (args.Result == true)
                        MessageBox.Show("儲存播出運行表完成");
                    else
                        MessageBox.Show("儲存播出運行表失敗!!!!");

                }
            };
        }

        List<string> FileStatusList = new List<string>();

        public string GetFileStatusXML()
        {
            if (PGM_ARR_PROMO.Count <= 0)
            {
                //MessageBox.Show("請先查詢播出運行表");
                return "";
            }
            //Boolean hasGetStatus = false;

            //for (int i = 0; i <= PGM_ARR_PROMO.Count - 1; i++)
            //{
            //    if (PGM_ARR_PROMO[i].FSSTATUS != "")
            //    {
            //        hasGetStatus = true;
            //    }
            //}
            //if (hasGetStatus==false)
            //{
            //    //MessageBox.Show("請先取得檔案狀態");
            //    return "尚未取得檔案狀態";
            //}

            FileStatusList.Clear();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<Datas>");
            for (int i = 0; i <= PGM_ARR_PROMO.Count - 1; i++)
            {
               
                    
                //if (PGM_ARR_PROMO[i].FSVIDEO_ID != "")  //要有主控編號才可以
                //只有這三種狀態才要派送,且已經有檔案編號, && (PGM_ARR_PROMO[i].FSSTATUS == "派送失敗" || PGM_ARR_PROMO[i].FSSTATUS == "尚未開始派送" || PGM_ARR_PROMO[i].FSSTATUS == "處理派送程序發生失敗"
                if (PGM_ARR_PROMO[i].FNLIVE != "1" && PGM_ARR_PROMO[i].FSFILE_NO != "" && (PGM_ARR_PROMO[i].FSSTATUS == "派送失敗" || PGM_ARR_PROMO[i].FSSTATUS == "尚未開始派送" || PGM_ARR_PROMO[i].FSSTATUS == "處理派送程序發生失敗"))
                {
                    bool needAdd = true;
                    for (int j = 0; j <= FileStatusList.Count - 1; j++)
                    {
                        if (PGM_ARR_PROMO[i].FCTYPE == "P") //代表Promo
                        {
                            if (FileStatusList[j] == PGM_ARR_PROMO[i].FSVIDEO_ID) //只要檢查Video_ID
                            { //當已經有此VIDEO_ID ,不要新增
                                needAdd = false;
                            }
                        }
                        else //節目要加入段落資料做Video_ID
                        {
                            if (FileStatusList[j] == PGM_ARR_PROMO[i].FSVIDEO_ID + int.Parse(PGM_ARR_PROMO[i].FNBREAK_NO).ToString("00"))
                            { //當已經有此VIDEO_ID ,不要新增
                                needAdd = false;
                            }
                        }
                    }
                    if (needAdd == true)
                    {
                        FileStatusList.Add(PGM_ARR_PROMO[i].FSVIDEO_ID);
                        sb.AppendLine("<Data>");
                        if (PGM_ARR_PROMO[i].FCTYPE == "G")
                            sb.AppendLine("<FileStatus>" + PGM_ARR_PROMO[i].FSVIDEO_ID + int.Parse(PGM_ARR_PROMO[i].FNBREAK_NO).ToString("00") + "</FileStatus>");
                        else
                            sb.AppendLine("<FileStatus>" + PGM_ARR_PROMO[i].FSVIDEO_ID + "</FileStatus>");
                        sb.AppendLine("</Data>");
                    }

                }
            }
            sb.AppendLine("</Datas>");
            return sb.ToString();
        }

        public string GetVideoIDXML()
        {
            if (PGM_ARR_PROMO.Count <= 0)
            {
                return "";
            }
            
            FileStatusList.Clear();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<Datas>");
            for (int i = 0; i <= PGM_ARR_PROMO.Count - 1; i++)
            {
                if (PGM_ARR_PROMO[i].FSVIDEO_ID != "")  //要有主控編號就可以加入清單,以取得檔案送狀態
                {
                    bool needAdd = true;
                    for (int j = 0; j <= FileStatusList.Count - 1; j++)
                    {
                        if (PGM_ARR_PROMO[i].FCTYPE == "P") //代表Promo
                        {
                            if (FileStatusList[j] == PGM_ARR_PROMO[i].FSVIDEO_ID) //只要檢查Video_ID
                            { //當已經有此VIDEO_ID ,不要新增
                                needAdd = false;
                            }
                        }
                        else //節目要加入段落資料做Video_ID
                        {
                            if (FileStatusList[j] == PGM_ARR_PROMO[i].FSVIDEO_ID + int.Parse(PGM_ARR_PROMO[i].FNBREAK_NO).ToString("00"))
                            { //當已經有此VIDEO_ID ,不要新增
                                needAdd = false;
                            }
                        }
                    }
                    if (needAdd == true)
                    {
                        if (PGM_ARR_PROMO[i].FCTYPE == "G")
                            FileStatusList.Add(PGM_ARR_PROMO[i].FSVIDEO_ID + int.Parse(PGM_ARR_PROMO[i].FNBREAK_NO).ToString("00"));
                        else
                            FileStatusList.Add(PGM_ARR_PROMO[i].FSVIDEO_ID);
                        sb.AppendLine("<Data>");
                        if (PGM_ARR_PROMO[i].FCTYPE == "G")
                            sb.AppendLine("<FileStatus>" + PGM_ARR_PROMO[i].FSVIDEO_ID + int.Parse(PGM_ARR_PROMO[i].FNBREAK_NO).ToString("00") + "</FileStatus>");
                        else
                            sb.AppendLine("<FileStatus>" + PGM_ARR_PROMO[i].FSVIDEO_ID + "</FileStatus>");
                        sb.AppendLine("</Data>");
                    }

                }
            }
            sb.AppendLine("</Datas>");
            return sb.ToString();
        }


        private void buttonSendFile_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("您確定要派送檔案?", "派送檔案", MessageBoxButton.OKCancel) == MessageBoxResult.Cancel)
            {
                return;
            }

            if (_ChannelType == "002")
            {
                MessageBox.Show("HD頻道不可派送檔案!");
                return;
            }

            string FIleStatusXML = GetVideoIDXML();

            if (FIleStatusXML == "" || FIleStatusXML == "<Datas></Datas>")
            {
                MessageBox.Show("沒有需要取得狀態的資料");
                return;
            }

            WSPGMSendSQL.SendSQLSoapClient GetVideoISStatus = new WSPGMSendSQL.SendSQLSoapClient();

            System.Collections.ObjectModel.ObservableCollection<WSPGMSendSQL.ArrPromoList> InList = new System.Collections.ObjectModel.ObservableCollection<WSPGMSendSQL.ArrPromoList>();

            foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO)
            {
                if (TemArrPromoList.FSPROG_ID != "")
                {

                    InList.Add(new WSPGMSendSQL.ArrPromoList()
                    {
                        COMBINENO = TemArrPromoList.COMBINENO,
                        FCTIME_TYPE = TemArrPromoList.FCTIME_TYPE,
                        FSPROG_ID = TemArrPromoList.FSPROG_ID,
                        FNSEQNO = TemArrPromoList.FNSEQNO,
                        FSNAME = TemArrPromoList.FSNAME,
                        FSCHANNEL_ID = TemArrPromoList.FSCHANNEL_ID,
                        FSCHANNEL_NAME = TemArrPromoList.FSCHANNEL_NAME,
                        FSPLAY_TIME = TemArrPromoList.FSPLAY_TIME,
                        FSDURATION = TemArrPromoList.FSDURATION,
                        FNEPISODE = TemArrPromoList.FNEPISODE,
                        FNBREAK_NO = TemArrPromoList.FNBREAK_NO,
                        FNSUB_NO = TemArrPromoList.FNSUB_NO,
                        FSFILE_NO = TemArrPromoList.FSFILE_NO,
                        FSVIDEO_ID = TemArrPromoList.FSVIDEO_ID,
                        FSPROMO_ID = TemArrPromoList.FSPROMO_ID,
                        FDDATE = TemArrPromoList.FDDATE,
                        FNLIVE = TemArrPromoList.FNLIVE,
                        FSSTATUS = TemArrPromoList.FSSTATUS,
                        FCTYPE = TemArrPromoList.FCTYPE,
                        FSMEMO = TemArrPromoList.FSMEMO,
                        FSPROG_NAME = TemArrPromoList.FSPROG_NAME,
                        FSSIGNAL = TemArrPromoList.FSSIGNAL
                    });
                }
            }

            GetVideoISStatus.GetVideoISStatusAsync(FIleStatusXML, InList,false);
            GetVideoISStatus.GetVideoISStatusCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "" && args.Result.Substring(0, 5) != "Error")
                    {
                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);

                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;

                        string ErrMessage = "";
                        foreach (XElement elem in ltox)
                        {
                            for (int i = 0; i <= PGM_ARR_PROMO.Count - 1; i++)
                            {
                                if (PGM_ARR_PROMO[i].FCTYPE == "G")
                                {
                                    if (PGM_ARR_PROMO[i].FNLIVE == "0")
                                    {
                                        if (PGM_ARR_PROMO[i].FSVIDEO_ID + int.Parse(PGM_ARR_PROMO[i].FNBREAK_NO).ToString("00") == XElementToString(elem.Element("VideoID")))
                                        {//當VideoID相同,要更新狀態
                                            if (PGM_ARR_PROMO[i].FSFILE_NO != "")
                                            {
                                                PGM_ARR_PROMO[i].FSSTATUS = TransFileStatus(XElementToString(elem.Element("Status")));
                                                if (PGM_ARR_PROMO[i].FSSTATUS != "FTP1傳送完成")
                                                    ErrMessage = ErrMessage + "播出時間:" + PGM_ARR_PROMO[i].FSPLAY_TIME + " 名稱:" + PGM_ARR_PROMO[i].FSNAME + ":檔案派送狀態未完成:訊息:" + PGM_ARR_PROMO[i].FSSTATUS + Convert.ToChar(10) + Convert.ToChar(13);
                                                //if (PGM_ARR_PROMO[i].FSSTATUS != "FTP1與FTP2傳送完成")
                                                //    ErrMessage = ErrMessage + "播出時間:" + PGM_ARR_PROMO[i].FSPLAY_TIME + " 名稱:" + PGM_ARR_PROMO[i].FSNAME + ":檔案派送狀態未完成:訊息:" + PGM_ARR_PROMO[i].FSSTATUS + Convert.ToChar(10) + Convert.ToChar(13);
                                            }
                                            else
                                            {
                                                PGM_ARR_PROMO[i].FSSTATUS = "尚未有檔案編號";
                                                ErrMessage = ErrMessage + "播出時間:" + PGM_ARR_PROMO[i].FSPLAY_TIME + " 名稱:" + PGM_ARR_PROMO[i].FSNAME + ":尚未有檔案編號" + Convert.ToChar(10) + Convert.ToChar(13);

                                            }

                                        }
                                    }
                                }
                                else
                                {
                                    if (PGM_ARR_PROMO[i].FSVIDEO_ID == XElementToString(elem.Element("VideoID")))
                                    {//當VideoID相同,要更新狀態
                                        if (PGM_ARR_PROMO[i].FSFILE_NO != "")
                                        {
                                            PGM_ARR_PROMO[i].FSSTATUS = TransFileStatus(XElementToString(elem.Element("Status")));
                                            if (PGM_ARR_PROMO[i].FSSTATUS != "FTP1傳送完成")
                                                ErrMessage = ErrMessage + "播出時間:" + PGM_ARR_PROMO[i].FSPLAY_TIME + " 名稱:" + PGM_ARR_PROMO[i].FSNAME + ":檔案派送狀態未完成:訊息:" + PGM_ARR_PROMO[i].FSSTATUS + Convert.ToChar(10) + Convert.ToChar(13);
                                        }
                                        else
                                        {
                                            PGM_ARR_PROMO[i].FSSTATUS = "尚未有檔案編號";
                                            ErrMessage = ErrMessage + "播出時間:" + PGM_ARR_PROMO[i].FSPLAY_TIME + " 名稱:" + PGM_ARR_PROMO[i].FSNAME + ":尚未有檔案編號" + Convert.ToChar(10) + Convert.ToChar(13);

                                        }
                                    }
                                }
                            }
                        }
                        if (ErrMessage != "")
                            MessageBox.Show(ErrMessage);
                        dataGridProgList.ItemsSource = null;
                        dataGridProgList.ItemsSource = PGM_ARR_PROMO;
                    }
                    else if (args.Result.Substring(0, 5) == "Error")
                    {
                        MessageBox.Show(args.Result);
                        return;
                    }
                }
                //20150904 by Mike
                //先取得檔案狀態再做派送檔案
                //string FIleStatusXML1 = GetFileStatusXML();

                //if (FIleStatusXML1 == "" || FIleStatusXML1 == "<Datas></Datas>")
                //{
                //    MessageBox.Show("沒有需要派送的檔案");
                //    return;
                //}
                //WSPGMSendSQL.SendSQLSoapClient SendMovingFile = new WSPGMSendSQL.SendSQLSoapClient();
                //SendMovingFile.SendMovingFileAsync(FIleStatusXML1);
                //SendMovingFile.SendMovingFileCompleted += (s1, args1) =>
                //{
                //    if (args1.Error == null)
                //    {
                //        MessageBox.Show(args1.Result);
                //    }
                //};
            };

            
            //string FIleStatusXML = GetFileStatusXML();

            //if (FIleStatusXML == "尚未取得檔案狀態")
            //{
            //    MessageBox.Show("尚未取得檔案狀態");
            //    return;
            //}

            //if (FIleStatusXML == "" || FIleStatusXML == "<Datas></Datas>")
            //{
            //    MessageBox.Show("沒有需要派送的檔案");
            //    return;
            //}

           
        }

        private void buttonGetFileStatus_Click(object sender, RoutedEventArgs e)
        {
            

            string FIleStatusXML = GetVideoIDXML();

            if (FIleStatusXML == "" || FIleStatusXML == "<Datas></Datas>")
            {
                MessageBox.Show("沒有需要取得狀態的資料");
                return;
            }
            if (_ChannelType == "002")
            {
                //WSPGMSendSQL.SendSQLSoapClient TransList = new WSPGMSendSQL.SendSQLSoapClient();
                System.Collections.ObjectModel.ObservableCollection<WSPGMSendSQL.ArrPromoList> InList = new System.Collections.ObjectModel.ObservableCollection<WSPGMSendSQL.ArrPromoList>();

                foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO)
                {
                    if (TemArrPromoList.FSPROG_ID != "")
                    {

                        InList.Add(new WSPGMSendSQL.ArrPromoList()
                        {
                            COMBINENO = TemArrPromoList.COMBINENO,
                            FCTIME_TYPE = TemArrPromoList.FCTIME_TYPE,
                            FSPROG_ID = TemArrPromoList.FSPROG_ID,
                            FNSEQNO = TemArrPromoList.FNSEQNO,
                            FSNAME = TemArrPromoList.FSNAME,
                            FSCHANNEL_ID = TemArrPromoList.FSCHANNEL_ID,
                            FSCHANNEL_NAME = TemArrPromoList.FSCHANNEL_NAME,
                            FSPLAY_TIME = TemArrPromoList.FSPLAY_TIME,
                            FSDURATION = TemArrPromoList.FSDURATION,
                            FNEPISODE = TemArrPromoList.FNEPISODE,
                            FNBREAK_NO = TemArrPromoList.FNBREAK_NO,
                            FNSUB_NO = TemArrPromoList.FNSUB_NO,
                            FSFILE_NO = TemArrPromoList.FSFILE_NO,
                            FSVIDEO_ID = TemArrPromoList.FSVIDEO_ID,
                            FSPROMO_ID = TemArrPromoList.FSPROMO_ID,
                            FDDATE = TemArrPromoList.FDDATE,
                            FNLIVE = TemArrPromoList.FNLIVE,
                            FSSTATUS = TemArrPromoList.FSSTATUS,
                            FCTYPE = TemArrPromoList.FCTYPE,
                            FSMEMO = TemArrPromoList.FSMEMO,
                            FSPROG_NAME = TemArrPromoList.FSPROG_NAME,
                            FSSIGNAL = TemArrPromoList.FSSIGNAL
                        });
                    }
                }
                WSPGMSendSQL.SendSQLSoapClient GetHD_Video_Status = new WSPGMSendSQL.SendSQLSoapClient();
                GetHD_Video_Status.GetHD_List_VideoID_StatusAsync(InList);
                GetHD_Video_Status.GetHD_List_VideoID_StatusCompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (args.Result != null && args.Result != "")
                        {
                            byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);

                            StringBuilder output = new StringBuilder();
                            // Create an XmlReader
                            XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                            var ltox = from str in doc.Elements("Datas").Elements("Data")
                                       select str;

                            string ErrMessage="";
                            foreach (XElement elem in ltox)
                            {
                                for (int i = 0; i <= PGM_ARR_PROMO.Count - 1; i++)
                                {
                                    if (PGM_ARR_PROMO[i].FCTYPE == "G")
                                    {
                                        if (PGM_ARR_PROMO[i].FNLIVE == "0")
                                        {
                                            if (PGM_ARR_PROMO[i].FSVIDEO_ID == XElementToString(elem.Element("FSVIDEO_ID")))
                                            {//當VideoID相同,要更新狀態
                                                if (XElementToString(elem.Element("FSSTATUS")).Length == 3)
                                                    PGM_ARR_PROMO[i].FSSTATUS = Translate_Status_Name.TransStatusName(XElementToString(elem.Element("FSSTATUS")).Substring(0, 1), XElementToString(elem.Element("FSSTATUS")).Substring(1, 1), XElementToString(elem.Element("FSSTATUS")).Substring(2, 1));
                                                else
                                                    PGM_ARR_PROMO[i].FSSTATUS = XElementToString(elem.Element("FSSTATUS"));
                                                
                                                if (PGM_ARR_PROMO[i].FSFILE_NO != "")
                                                {
                                                    if (PGM_ARR_PROMO[i].FSSTATUS != "檔案已到待播區")
                                                        ErrMessage = ErrMessage + "播出時間:" + PGM_ARR_PROMO[i].FSPLAY_TIME + " 名稱:" + PGM_ARR_PROMO[i].FSNAME + ":檔案派送狀態未完成:訊息:" + PGM_ARR_PROMO[i].FSSTATUS + Convert.ToChar(10) + Convert.ToChar(13);
                                                }
                                                else
                                                {
                                                    PGM_ARR_PROMO[i].FSSTATUS = "尚未有檔案編號";
                                                    ErrMessage = ErrMessage + "播出時間:" + PGM_ARR_PROMO[i].FSPLAY_TIME + " 名稱:" + PGM_ARR_PROMO[i].FSNAME + ":尚未有檔案編號" + Convert.ToChar(10) + Convert.ToChar(13);

                                                }

                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (PGM_ARR_PROMO[i].FSVIDEO_ID == XElementToString(elem.Element("FSVIDEO_ID")))
                                        {//當VideoID相同,要更新狀態
                                            if (PGM_ARR_PROMO[i].FSFILE_NO != "")
                                            {
                                                if (XElementToString(elem.Element("FSSTATUS")).Length == 3)
                                                    PGM_ARR_PROMO[i].FSSTATUS = Translate_Status_Name.TransStatusName(XElementToString(elem.Element("FSSTATUS")).Substring(0, 1), XElementToString(elem.Element("FSSTATUS")).Substring(1, 1), XElementToString(elem.Element("FSSTATUS")).Substring(2, 1));
                                                else
                                                    PGM_ARR_PROMO[i].FSSTATUS = XElementToString(elem.Element("FSSTATUS"));
                                                if (PGM_ARR_PROMO[i].FSSTATUS != "檔案已到待播區")
                                                    ErrMessage = ErrMessage + "播出時間:" + PGM_ARR_PROMO[i].FSPLAY_TIME + " 名稱:" + PGM_ARR_PROMO[i].FSNAME + ":檔案派送狀態未完成:訊息:" + PGM_ARR_PROMO[i].FSSTATUS + Convert.ToChar(10) + Convert.ToChar(13);
                                            }
                                            else
                                            {
                                                PGM_ARR_PROMO[i].FSSTATUS = "尚未有檔案編號";
                                                ErrMessage = ErrMessage + "播出時間:" + PGM_ARR_PROMO[i].FSPLAY_TIME + " 名稱:" + PGM_ARR_PROMO[i].FSNAME + ":尚未有檔案編號" + Convert.ToChar(10) + Convert.ToChar(13);

                                            }
                                        }
                                    }
                                }
                            }
                            if (ErrMessage != "")
                                MessageBox.Show(ErrMessage);
                            dataGridProgList.ItemsSource = null;
                            dataGridProgList.ItemsSource = PGM_ARR_PROMO;
                        }
                    }
                };

            }
            else
            {
            

                WSPGMSendSQL.SendSQLSoapClient GetVideoISStatus = new WSPGMSendSQL.SendSQLSoapClient();
                GetVideoISStatus.GetVideoISStatusAsync(FIleStatusXML,null,false);
                GetVideoISStatus.GetVideoISStatusCompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (args.Result != null && args.Result != "")
                        {
                            byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);

                            StringBuilder output = new StringBuilder();
                            // Create an XmlReader
                            XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                            var ltox = from str in doc.Elements("Datas").Elements("Data")
                                       select str;

                            string ErrMessage="";
                            foreach (XElement elem in ltox)
                            {
                                for (int i = 0; i <= PGM_ARR_PROMO.Count - 1; i++)
                                {
                                    if (PGM_ARR_PROMO[i].FCTYPE == "G")
                                    {
                                        if (PGM_ARR_PROMO[i].FNLIVE == "0")
                                        {
                                            if (PGM_ARR_PROMO[i].FSVIDEO_ID + int.Parse(PGM_ARR_PROMO[i].FNBREAK_NO).ToString("00") == XElementToString(elem.Element("VideoID")))
                                            {//當VideoID相同,要更新狀態
                                                if (PGM_ARR_PROMO[i].FSFILE_NO != "")
                                                {
                                                    PGM_ARR_PROMO[i].FSSTATUS = TransFileStatus(XElementToString(elem.Element("Status")));

                                                    if (PGM_ARR_PROMO[i].FSSTATUS != "FTP1傳送完成")
                                                        ErrMessage = ErrMessage + "播出時間:" + PGM_ARR_PROMO[i].FSPLAY_TIME + " 名稱:" + PGM_ARR_PROMO[i].FSNAME + ":檔案派送狀態未完成:訊息:" + PGM_ARR_PROMO[i].FSSTATUS + Convert.ToChar(10) + Convert.ToChar(13);
                                                }
                                                else
                                                {
                                                    PGM_ARR_PROMO[i].FSSTATUS = "尚未有檔案編號";
                                                    ErrMessage = ErrMessage + "播出時間:" + PGM_ARR_PROMO[i].FSPLAY_TIME + " 名稱:" + PGM_ARR_PROMO[i].FSNAME + ":尚未有檔案編號" + Convert.ToChar(10) + Convert.ToChar(13);

                                                }

                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (PGM_ARR_PROMO[i].FSVIDEO_ID == XElementToString(elem.Element("VideoID")))
                                        {//當VideoID相同,要更新狀態
                                            if (PGM_ARR_PROMO[i].FSFILE_NO != "")
                                            {
                                                PGM_ARR_PROMO[i].FSSTATUS = TransFileStatus(XElementToString(elem.Element("Status")));
                                                if (PGM_ARR_PROMO[i].FSSTATUS != "FTP1傳送完成")
                                                    ErrMessage = ErrMessage + "播出時間:" + PGM_ARR_PROMO[i].FSPLAY_TIME + " 名稱:" + PGM_ARR_PROMO[i].FSNAME + ":檔案派送狀態未完成:訊息:" + PGM_ARR_PROMO[i].FSSTATUS + Convert.ToChar(10) + Convert.ToChar(13);
                                            }
                                            else
                                            {
                                                PGM_ARR_PROMO[i].FSSTATUS = "尚未有檔案編號";
                                                ErrMessage = ErrMessage + "播出時間:" + PGM_ARR_PROMO[i].FSPLAY_TIME + " 名稱:" + PGM_ARR_PROMO[i].FSNAME + ":尚未有檔案編號" + Convert.ToChar(10) + Convert.ToChar(13);

                                            }
                                        }
                                    }
                                }
                            }
                            if (ErrMessage != "")
                                MessageBox.Show(ErrMessage);
                            dataGridProgList.ItemsSource = null;
                            dataGridProgList.ItemsSource = PGM_ARR_PROMO;
                        }
                    }
                };
            }
        }

        public string TransFileStatus(string INEngFIleStatus)
        {
            string OutChtFileStatus;
            if (INEngFIleStatus == "FTP1OK_FTP2OK" || INEngFIleStatus == "FTP1EXIST_FTP2EXIST"
                            || INEngFIleStatus == "FTP1OK_FTP2EXIST" || INEngFIleStatus == "FTP1EXIST_FTP2OK")
                OutChtFileStatus = "FTP1傳送完成";
            else if (INEngFIleStatus == "FTP1OK_FTP2PROC" || INEngFIleStatus == "FTP1EXIST_FTP2PROC")
                OutChtFileStatus = "FTP1傳送完成";
            else if (INEngFIleStatus == "FTP1OK_FTP2ERR" || INEngFIleStatus == "FTP1EXIST_FTP2ERR")
                OutChtFileStatus = "FTP1傳送完成";
            else if (INEngFIleStatus == "FTP1PROC_FTP2PROC")
                OutChtFileStatus = "FTP1傳送中";
            else if (INEngFIleStatus == "FTP1PROC_FTP2OK" || INEngFIleStatus == "FTP1PROC_FTP2EXIST")
                OutChtFileStatus = "FTP1傳送中";
            else if (INEngFIleStatus == "FTP1PROC_FTP2ERR")
                OutChtFileStatus = "FTP1傳送中";
            else if (INEngFIleStatus == "FTP1ERR_FTP2PROC")
                OutChtFileStatus = "FTP1傳送失敗";
            else if (INEngFIleStatus == "FTP1ERR_FTP2OK" || INEngFIleStatus == "FTP1ERR_FTP2EXIST")
                OutChtFileStatus = "FTP1傳送失敗";
            else if (INEngFIleStatus == "FTP1ERR_FTP2ERR")
                OutChtFileStatus = "FTP1傳送失敗";
            else if (INEngFIleStatus == "FTP1OK_FTP2NOTSTART" || INEngFIleStatus == "FTP1EXIST_FTP2NOTSTART" || INEngFIleStatus == "FTP1OK_FTP2NOSTART" || INEngFIleStatus == "FTP1EXIST_FTP2NOSTART")
                OutChtFileStatus = "FTP1傳送完成";
            else if (INEngFIleStatus == "FTP1PROC_FTP2NOTSTART" || INEngFIleStatus == "FTP1PROC_FTP2NOSTART")
                OutChtFileStatus = "FTP1傳送中";
            else if (INEngFIleStatus == "FTP1ERR_FTP2NOTSTART" || INEngFIleStatus == "FTP1ERR_FTP2NOSTART")
                OutChtFileStatus = "FTP1傳送失敗";
            else if (INEngFIleStatus == "FTP1NOTSTART_FTP2PROC" || INEngFIleStatus == "FTP1NOSTART_FTP2PROC")
                OutChtFileStatus = "FTP1尚未傳送";
            else if (INEngFIleStatus == "FTP1NOTSTART_FTP2OK" || INEngFIleStatus == "FTP1NOTSTART_FTP2EXIST" || INEngFIleStatus == "FTP1NOSTART_FTP2OK" || INEngFIleStatus == "FTP1NOSTART_FTP2EXIST")
                OutChtFileStatus = "FTP1尚未傳送";
            else if (INEngFIleStatus == "FTP1NOTSTART_FTP2ERR" || INEngFIleStatus == "FTP1NOSTART_FTP2ERR")
                OutChtFileStatus = "FTP1尚未傳送";
            else if (INEngFIleStatus == "FTP1NOTSTART_FTP2NOTSTART" || INEngFIleStatus == "FTP1NOSTART_FTP2NOSTART")
                OutChtFileStatus = "FTP1尚未傳送";
            else if (INEngFIleStatus == "MSSCOPY_OK")
                OutChtFileStatus = "派送完成,但尚未FTP到主控";
            else if (INEngFIleStatus == "MSSCOPY_PROC")
                OutChtFileStatus = "派送中";
            else if (INEngFIleStatus == "MSSCOPY_ERR")
                OutChtFileStatus = "派送失敗";
            else if (INEngFIleStatus == "MSSPROCESS_OK")
                OutChtFileStatus = "尚未開始派送";
            else if (INEngFIleStatus == "IMX50_NOTEXIST")
                OutChtFileStatus = "來源檔案不存在";
            else if (INEngFIleStatus == "IMX50_PROC")
                OutChtFileStatus = "來源檔案轉檔中";
            else if (INEngFIleStatus == "IMX50_ERR")
                OutChtFileStatus = "來源檔案轉檔失敗";
            else if (INEngFIleStatus == "MSSPROCESS_PROC")
                OutChtFileStatus = "正在將檔案轉為主控格式";
            else if (INEngFIleStatus == "MSSPROCESS_ERR")
                OutChtFileStatus = "將檔案轉為主控格式失敗";
            else if (INEngFIleStatus == "MSSPROCESS_NOTSTART")
                OutChtFileStatus = "尚未將檔案轉為主控格式";
            else if (INEngFIleStatus == "IMX50_OK")
                OutChtFileStatus = "檔案入庫完成,尚未開始轉成主控格式";
            else if (INEngFIleStatus == "SYSERROR")
                OutChtFileStatus = "處理派送程序發生失敗";
            else
                OutChtFileStatus = INEngFIleStatus;
            //if (INEngFIleStatus == "FTP1OK_FTP2OK" || INEngFIleStatus == "FTP1EXIST_FTP2EXIST"
            //    || INEngFIleStatus == "FTP1OK_FTP2EXIST" || INEngFIleStatus == "FTP1EXIST_FTP2OK")
            //    OutChtFileStatus = "FTP1與FTP2傳送完成";
            //else if (INEngFIleStatus == "FTP1OK_FTP2PROC" || INEngFIleStatus == "FTP1EXIST_FTP2PROC")
            //    OutChtFileStatus = "FTP1傳送完成,FTP2傳送中";
            //else if (INEngFIleStatus == "FTP1OK_FTP2ERR" || INEngFIleStatus == "FTP1EXIST_FTP2ERR")
            //    OutChtFileStatus = "FTP1傳送完成,FTP2傳送失敗";
            //else if (INEngFIleStatus == "FTP1PROC_FTP2PROC")
            //    OutChtFileStatus = "FTP1傳送中,FTP2傳送中";
            //else if (INEngFIleStatus == "FTP1PROC_FTP2OK" || INEngFIleStatus == "FTP1PROC_FTP2EXIST")
            //    OutChtFileStatus = "FTP1傳送中,FTP2傳送完成";
            //else if (INEngFIleStatus == "FTP1PROC_FTP2ERR")
            //    OutChtFileStatus = "FTP1傳送中,FTP2傳送失敗";
            //else if (INEngFIleStatus == "FTP1ERR_FTP2PROC")
            //    OutChtFileStatus = "FTP1傳送失敗,FTP2傳送中";
            //else if (INEngFIleStatus == "FTP1ERR_FTP2OK" || INEngFIleStatus == "FTP1ERR_FTP2EXIST")
            //    OutChtFileStatus = "FTP1傳送失敗,FTP2傳送完成";
            //else if (INEngFIleStatus == "FTP1ERR_FTP2ERR")
            //    OutChtFileStatus = "FTP1傳送失敗,FTP2傳送失敗";
            //else if (INEngFIleStatus == "FTP1OK_FTP2NOTSTART" || INEngFIleStatus == "FTP1EXIST_FTP2NOTSTART")
            //    OutChtFileStatus = "FTP1傳送完成,FTP2尚未傳送";
            //else if (INEngFIleStatus == "FTP1PROC_FTP2NOTSTART")
            //    OutChtFileStatus = "FTP1傳送中,FTP2尚未傳送";
            //else if (INEngFIleStatus == "FTP1ERR_FTP2NOTSTART")
            //    OutChtFileStatus = "FTP1傳送失敗,FTP2尚未傳送";
            //else if (INEngFIleStatus == "FTP1NOTSTART_FTP2PROC")
            //    OutChtFileStatus = "FTP1尚未傳送,FTP2傳送中";
            //else if (INEngFIleStatus == "FTP1NOTSTART_FTP2OK" || INEngFIleStatus == "FTP1NOTSTART_FTP2EXIST")
            //    OutChtFileStatus = "FTP1尚未傳送,FTP2傳送完成";
            //else if (INEngFIleStatus == "FTP1NOTSTART_FTP2ERR")
            //    OutChtFileStatus = "FTP1尚未傳送,FTP2傳送失敗";
            //else if (INEngFIleStatus == "FTP1NOTSTART_FTP2NOTSTART")
            //    OutChtFileStatus = "FTP1尚未傳送,FTP2尚未傳送";
            //else if (INEngFIleStatus == "MSSCOPY_OK")
            //    OutChtFileStatus = "派送完成,但尚未FTP到主控";
            //else if (INEngFIleStatus == "MSSCOPY_PROC")
            //    OutChtFileStatus = "派送中";
            //else if (INEngFIleStatus == "MSSCOPY_ERR")
            //    OutChtFileStatus = "派送失敗";
            //else if (INEngFIleStatus == "MSSPROCESS_OK")
            //    OutChtFileStatus = "尚未開始派送";
            //else if (INEngFIleStatus == "IMX50_NOTEXIST")
            //    OutChtFileStatus = "來源檔案不存在";
            //else if (INEngFIleStatus == "IMX50_PROC")
            //    OutChtFileStatus = "來源檔案轉檔中";
            //else if (INEngFIleStatus == "IMX50_ERR")
            //    OutChtFileStatus = "來源檔案轉檔失敗";
            //else if (INEngFIleStatus == "MSSPROCESS_PROC")
            //    OutChtFileStatus = "正在將檔案轉為主控格式";
            //else if (INEngFIleStatus == "MSSPROCESS_ERR")
            //    OutChtFileStatus = "將檔案轉為主控格式失敗";
            //else if (INEngFIleStatus == "MSSPROCESS_NOTSTART")
            //    OutChtFileStatus = "尚未將檔案轉為主控格式";
            //else if (INEngFIleStatus == "IMX50_OK")
            //    OutChtFileStatus = "檔案入庫完成,尚未開始轉成主控格式";
            //else if (INEngFIleStatus == "SYSERROR")
            //    OutChtFileStatus = "處理派送程序發生失敗";
            //else
            //    OutChtFileStatus = INEngFIleStatus;

            return OutChtFileStatus;
        }

        //sw1.Write((char)i);  //將數字轉換為ASCII
        private void buttonLST_Click(object sender, RoutedEventArgs e)
        {
            if (_ChannelID == "")
            {
                MessageBox.Show("請先查詢節目表");
                return;
            }
            if (ModuleClass.getModulePermission("P5" + _ChannelID + "006") == false)
            {
                MessageBox.Show("您沒有轉出此頻道LST檔案的權限");
                return;
            }

            SaveFileDialog dialog = new SaveFileDialog();
            //dialog.Filter = "lst File" + "|*.lst";
            dialog.Filter = "Zip File" + "|*.Zip";
            dialog.FilterIndex = 1;
            dialog.ShowDialog();
            //檢查資料,如果有沒有輸入訊號的則提出警示

            string ErrMessage = "";
            foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO)
            {
                if (TemArrPromoList.FSPROG_ID != "")
                {
                    if (TemArrPromoList.FSSIGNAL == "")
                    {
                        ErrMessage=ErrMessage + "播出時間" + TemArrPromoList.FSPLAY_TIME + " 名稱:" + TemArrPromoList.FSNAME + ":沒有選擇訊號源"+  Convert.ToChar(10) +Convert.ToChar(13);
                        
                    }
                }
            }
            if (ErrMessage != "")
            {
                if (MessageBox.Show(ErrMessage + "要繼續嗎?", "提示訊息", MessageBoxButton.OKCancel) == MessageBoxResult.Cancel) 
                  return;
            }
            BusyMsg.IsBusy = true;
            WSPGMSendSQL.SendSQLSoapClient TransList = new WSPGMSendSQL.SendSQLSoapClient();
            System.Collections.ObjectModel.ObservableCollection<WSPGMSendSQL.ArrPromoList> InList = new System.Collections.ObjectModel.ObservableCollection<WSPGMSendSQL.ArrPromoList>();

            foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO)
            {
                if (TemArrPromoList.FSPROG_ID != "")
                {
                    
                    InList.Add(new WSPGMSendSQL.ArrPromoList()
                    {
                        COMBINENO = TemArrPromoList.COMBINENO,
                        FCTIME_TYPE = TemArrPromoList.FCTIME_TYPE,
                        FSPROG_ID = TemArrPromoList.FSPROG_ID,
                        FNSEQNO = TemArrPromoList.FNSEQNO,
                        FSNAME = TemArrPromoList.FSNAME,
                        FSCHANNEL_ID = TemArrPromoList.FSCHANNEL_ID,
                        FSCHANNEL_NAME = TemArrPromoList.FSCHANNEL_NAME,
                        FSPLAY_TIME = TemArrPromoList.FSPLAY_TIME,
                        FSDURATION = TemArrPromoList.FSDURATION,
                        FNEPISODE = TemArrPromoList.FNEPISODE,
                        FNBREAK_NO = TemArrPromoList.FNBREAK_NO,
                        FNSUB_NO = TemArrPromoList.FNSUB_NO,
                        FSFILE_NO = TemArrPromoList.FSFILE_NO,
                        FSVIDEO_ID = TemArrPromoList.FSVIDEO_ID,
                        FSPROMO_ID = TemArrPromoList.FSPROMO_ID,
                        FDDATE = TemArrPromoList.FDDATE,
                        FNLIVE = TemArrPromoList.FNLIVE,
                        FSSTATUS = TemArrPromoList.FSSTATUS,
                        FCTYPE = TemArrPromoList.FCTYPE,
                        FSMEMO = TemArrPromoList.FSMEMO,
                        FSPROG_NAME = TemArrPromoList.FSPROG_NAME,
                        FSSIGNAL = TemArrPromoList.FSSIGNAL
                    });
                }
            }

            TransList.TransLstAsync(InList);
            TransList.TransLstCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {


                        //args.Result	"http://localhost/PTS_MAM3.Web/UploadFolder/0120110410.lst"	string


                        WCF100DL.WCF100Client WCF100client = new WCF100DL.WCF100Client();

                        string LSTFileID = _ChannelID + String.Format("{0:00}", Convert.ToDateTime(_Date).Year) + String.Format("{0:00}", Convert.ToDateTime(_Date).Month) + String.Format("{0:00}", Convert.ToDateTime(_Date).Day) + ".zip";

                        WCF100client.DownloadAsync(LSTFileID);
                        //WCF100client.DownloadAsync(args.Result);
                        WCF100client.DownloadCompleted += (s1, args1) =>
                        {
                            if (args1.Error != null)
                            {
                                MessageBox.Show(args1.Error.ToString());
                                return;
                            }

                            if (args1.Result != null)
                            {
                                try
                                {

                                    Stream fileobj = dialog.OpenFile();
                                    if (fileobj == null)
                                    {
                                    }
                                    else
                                    {
                                        fileobj.Write(args1.Result.File, 0, args1.Result.File.Length);
                                        fileobj.Close();
                                        BusyMsg.IsBusy = false;
                                        MessageBox.Show("轉檔完成");

                                        #region //詢問是否需要儲存首播清單,已經產生完畢要下載 "P" + Channel_ID+ YYYY+MM+DD + ".lst"
                                        //WCF100DL.WCF100Client WCF100client1 = new WCF100DL.WCF100Client();

                                        //string FirstPlayProg = "G"+ _ChannelID + String.Format("{0:00}", Convert.ToDateTime(_Date).Year) + String.Format("{0:00}", Convert.ToDateTime(_Date).Month) + String.Format("{0:00}", Convert.ToDateTime(_Date).Day) + ".lst";
                                        //SaveFileDialog dialog1 = new SaveFileDialog();
                                        //dialog1.Filter = "lst File" + "|*.lst";
                                        //dialog1.FilterIndex = 1;
                                        //dialog1.ShowDialog();
                                        //WCF100client1.DownloadAsync(FirstPlayProg);
                                        ////WCF100client.DownloadAsync(args.Result);
                                        //WCF100client1.DownloadCompleted += (s2, args2) =>
                                        //    {
                                        //        if (args2.Error != null)
                                        //        {
                                        //            MessageBox.Show(args2.Error.ToString());
                                        //            return;
                                        //        }

                                        //        if (args1.Result != null)
                                        //        {
                                        //            try
                                        //            {
                                        //                Stream fileobj1 = dialog1.OpenFile();
                                        //                if (fileobj1 != null)
                                        //                {
                                        //                    fileobj1.Write(args2.Result.File, 0, args2.Result.File.Length);
                                        //                    fileobj1.Close();
                                                           
                                        //                }
                                                        
                                        //            }
                                        //            catch (Exception ex)
                                        //            {
                                        //                return;
                                        //            }
                                        //        }

                                        //    };
                                        #endregion

                                    }
                                }
                                catch (Exception ex)
                                {
                                    BusyMsg.IsBusy = false;
                                    return;
                                }
                            }
                        };



                        //System.Windows.Browser.HtmlPage.Window.Navigate(new Uri(args.Result, UriKind.Absolute));
                    }
                    else
                        MessageBox.Show("產生檔案有問題");
                }

            };
        }

        private void buttonSetTimeType_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridProgList.SelectedIndex < 0)
            {
                MessageBox.Show("必須先選擇資料");
                return;
            }
            //if (PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FCTIME_TYPE == "") //時間設定不為固定
            //{
            //    MessageBox.Show("不能更改此種資料類型");
            //    return;
            //}
            //ArrPromoList Temp_ArrPromo = new ArrPromoList();
            int i = dataGridProgList.SelectedIndex;
            //if (PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FCTYPE != "P") //如果所選位置是Promo
            //{
            PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FCTIME_TYPE = "A";
            //}
            ReCalTimecode();
        }

        private void LayoutRoot_KeyUp(object sender, KeyEventArgs e)
        {
            //if ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)

        }

        private void buttonSetTimeTypeO_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridProgList.SelectedIndex < 0)
            {
                MessageBox.Show("必須先選擇資料");
                return;
            }
            //if (PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FCTIME_TYPE == "") //時間設定不為固定
            //{
            //    MessageBox.Show("不能更改此種資料類型");
            //    return;
            //}
            //tbxTimecode.MaskedText;
            try
            {
                if (tbxTimecode.MaskedText != "__:__:__;__")
                {
                    if (int.Parse(tbxTimecode.MaskedText.Substring(0, 2)) >= 30)
                    {
                        MessageBox.Show("時間輸入格式有誤");
                        return;
                    }
                    if (int.Parse(tbxTimecode.MaskedText.Substring(3, 2)) >= 60)
                    {
                        MessageBox.Show("時間輸入格式有誤");
                        return;
                    }
                    if (int.Parse(tbxTimecode.MaskedText.Substring(6, 2)) >= 60)
                    {
                        MessageBox.Show("時間輸入格式有誤");
                        return;
                    }
                    if (int.Parse(tbxTimecode.MaskedText.Substring(9, 2)) >= 30)
                    {
                        MessageBox.Show("時間輸入格式有誤");
                        return;
                    }
                }
            }
            catch
            {
                MessageBox.Show("時間輸入格式有誤");
                return;
            }
            //ArrPromoList Temp_ArrPromo = new ArrPromoList();
            int i = dataGridProgList.SelectedIndex;
            //if (PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FCTYPE != "P") //如果所選位置是Promo
            //{
            PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FCTIME_TYPE = "O";
            if (tbxTimecode.MaskedText != "__:__:__;__")
            {
                PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSPLAY_TIME = tbxTimecode.MaskedText;
                PGM_ARR_PROMO[dataGridProgList.SelectedIndex].RealFSPLAY_TIME = GetRealTImeCode(tbxTimecode.MaskedText);
            }
            //}
            ReCalTimecode();
        }

        private void dataGridProgList_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (dataGridProgList.SelectedIndex < 0)
            {
                return;
            }
            if (PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FCTYPE == "P") //宣傳帶才需做次數計算
            {
                int InsertCount = 0;
                for (int i = 0; i <= PGM_ARR_PROMO.Count - 1; i++)
                {
                    if (PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSPROMO_ID == PGM_ARR_PROMO[i].FSPROMO_ID)
                    {
                        InsertCount = InsertCount + 1;
                    }
                }
                textBlockStatus.Text = "短帶[" + PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSNAME + "]插入次數:" + InsertCount.ToString();

            }

            PromoInsertStatus.Clear();


            foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO)
            {
                if (TemArrPromoList.FSPROMO_ID == PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSPROMO_ID)
                {
                    PromoInsertStatus.Add(new Date_Arr_Promo()
                    {
                        FSPROMO_ID = TemArrPromoList.FSPROMO_ID,
                        FSPROMO_NAME = TemArrPromoList.FSNAME,
                        FSPROG_ID = TemArrPromoList.FSPROG_ID,
                        FSPROG_NAME = TemArrPromoList.FSPROG_NAME,
                        FSPLAYTIME = TemArrPromoList.RealFSPLAY_TIME

                    });
                }
            }
            if (_PromoInsertStatusForm == null)
            {
                _PromoInsertStatusForm = new Telerik.Windows.Controls.RadWindow();
                _PromoInsertStatusForm.Header = PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSNAME + "--排播狀態";
                _PromoInsertStatusForm.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
                _PromoInsertStatusForm.TopOffset = -100;
                _PromoInsertStatusForm.Width = 300;
                _PromoInsertStatusForm.Height = 300;


                DataGrid D1 = new DataGrid();

                _PromoInsertStatusForm.Content = D1;
                StyleManager.SetTheme(_PromoInsertStatusForm, new Office_SilverTheme());


                DataGridTextColumn textColumn = new DataGridTextColumn();
                textColumn.Header = "短帶編號";
                textColumn.Binding = new Binding("FSPROMO_ID");
                textColumn.Visibility = Visibility.Collapsed;

                DataGridTextColumn textColumn1 = new DataGridTextColumn();
                textColumn1.Header = "短帶名稱";
                textColumn1.Binding = new Binding("FSPROMO_NAME");
                textColumn1.Visibility = Visibility.Collapsed;

                DataGridTextColumn textColumn2 = new DataGridTextColumn();
                textColumn2.Header = "節目編碼";
                textColumn2.Binding = new Binding("FSPROG_ID");
                textColumn2.Visibility = Visibility.Collapsed;

                DataGridTextColumn textColumn3 = new DataGridTextColumn();
                textColumn3.Header = "所屬節目名稱";
                textColumn3.Binding = new Binding("FSPROG_NAME");

                DataGridTextColumn textColumn4 = new DataGridTextColumn();
                textColumn4.Header = "播出時間";
                textColumn4.Binding = new Binding("FSPLAYTIME");


                D1.Columns.Add(textColumn);
                D1.Columns.Add(textColumn1);
                D1.Columns.Add(textColumn2);
                D1.Columns.Add(textColumn3);
                D1.Columns.Add(textColumn4);
                D1.AutoGenerateColumns = false;
                D1.ItemsSource = null;
                D1.ItemsSource = PromoInsertStatus;

                //_PromoInsertStatusForm.Show();

            }
            else
            {
                _PromoInsertStatusForm.Header = PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSNAME + "--排播狀態";
                DataGrid D1 = new DataGrid();

                _PromoInsertStatusForm.Content = D1;
                StyleManager.SetTheme(_PromoInsertStatusForm, new Office_SilverTheme());


                DataGridTextColumn textColumn = new DataGridTextColumn();
                textColumn.Header = "短帶編號";
                textColumn.Binding = new Binding("FSPROMO_ID");
                textColumn.Visibility = Visibility.Collapsed;

                DataGridTextColumn textColumn1 = new DataGridTextColumn();
                textColumn1.Header = "短帶名稱";
                textColumn1.Binding = new Binding("FSPROMO_NAME");
                textColumn1.Visibility = Visibility.Collapsed;

                DataGridTextColumn textColumn2 = new DataGridTextColumn();
                textColumn2.Header = "節目編碼";
                textColumn2.Binding = new Binding("FSPROG_ID");
                textColumn2.Visibility = Visibility.Collapsed;

                DataGridTextColumn textColumn3 = new DataGridTextColumn();
                textColumn3.Header = "所屬節目名稱";
                textColumn3.Binding = new Binding("FSPROG_NAME");

                DataGridTextColumn textColumn4 = new DataGridTextColumn();
                textColumn4.Header = "播出時間";
                textColumn4.Binding = new Binding("FSPLAYTIME");


                D1.Columns.Add(textColumn);
                D1.Columns.Add(textColumn1);
                D1.Columns.Add(textColumn2);
                D1.Columns.Add(textColumn3);
                D1.Columns.Add(textColumn4);
                D1.AutoGenerateColumns = false;
                D1.ItemsSource = null;
                D1.ItemsSource = PromoInsertStatus;
                //_PromoInsertStatusForm.Show();
            }

        }

        private void buttonSetLouthKey_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                if (PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FCTYPE == "G" || PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FCTYPE == "P") //節目與Promo才可以設定Louth Key
                {
                    PGM.PGM100_02 SetLouthKey_Frm = new PGM.PGM100_02();

                    SetLouthKey_Frm.InLouthKeyString = PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSMEMO;
                    //SetLouthKey_Frm.AnalyLouthKeyString(textBoxMemo.Text);
                    SetLouthKey_Frm.WorkChannelID = _ChannelID;
                    SetLouthKey_Frm.Show();
                    SetLouthKey_Frm.Closed += (s, args) =>
                    {
                        if (SetLouthKey_Frm.OutLouthKeyString != null)
                        {
                            PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSMEMO = SetLouthKey_Frm.OutLouthKeyString;
                        }

                    };
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("設定Louth Key 發生錯誤,訊息:"+ ex.Message +":請重新設定");
                return ;
            }
            
        }

        private void buttonLST1_Click(object sender, RoutedEventArgs e)
        {



            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "lst File" + "|*.lst";
            dialog.FilterIndex = 1;
            dialog.ShowDialog();


            WCF100DL.WCF100Client WCF100client = new WCF100DL.WCF100Client();
            //WCF100client.DownloadAsync("http://localhost/PTS_MAM3.Web/UploadFolder/0120041203.lst");
            //WCF100client.DownloadAsync("/UploadFolder/0120041203.lst");
            WCF100client.DownloadAsync(TextBoxTest.Text);

            WCF100client.DownloadCompleted += (s1, args1) =>
            {
                if (args1.Error != null)
                {
                    MessageBox.Show(args1.Error.ToString());
                    return;
                }

                if (args1.Result != null)
                {
                    try
                    {
                        Stream fileobj = dialog.OpenFile();
                        if (fileobj == null)
                        {
                        }
                        else
                        {
                            fileobj.Write(args1.Result.File, 0, args1.Result.File.Length);
                            fileobj.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        return;
                    }
                }
            };

            // Disable download button to avoid clicking again while downloading the file


            //System.Windows.Browser.HtmlPage.Window.Navigate(new Uri(args.Result, UriKind.Absolute));
        }

        private void Page_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void Page_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            //comboBoxChannel.Focus();
            try
            {
                if (PGM_ARR_PROMO.Count == 0)
                    comboBoxChannel.Focus();
            }
            catch
            {
                comboBoxChannel.Focus();
            }
        }

        private void DCdataGridPromoList_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            //DCdataGridPromoList.Focus();
        }

        private long _startFrame { get; set; }
        private string _mediaSource { get; set; }
        Telerik.Windows.Controls.RadWindow _wnd;

        private void buttonPreview_Click(object sender, RoutedEventArgs e)
        {

            if (PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSFILE_NO != "")
            {
                //WSPGMSendSQL.SendSQLSoapClient GetLVFilePath = new WSPGMSendSQL.SendSQLSoapClient();

                //如果是Promo的話直接拉檔案的位置play,節目的話需要產生ASX檔案再去播放ASX
                if (PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FCTYPE == "P")
                {
                    ServiceTSMSoapClient tsm = new ServiceTSMSoapClient();
                    tsm.GetHttpPathByFileIDAsync(PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSFILE_NO, FileID_MediaType.LowVideo);
                    tsm.GetHttpPathByFileIDCompleted += (s, args) =>
                    {

                        if (args.Result != null && args.Result != "")
                        {
                            _mediaSource = args.Result;
                            _startFrame = 0;
                            if (_wnd == null)
                            {
                                _wnd = new Telerik.Windows.Controls.RadWindow();
                                _wnd.Header = "檔案預覽";
                                _wnd.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
                                _wnd.TopOffset = -100;
                                _wnd.Width = 640;
                                //_wnd.Height = 506;

                                StyleManager.SetTheme(_wnd, new Office_SilverTheme());

                                SLVideoPlayer sl = new SLVideoPlayer(_mediaSource, _startFrame);
                                _wnd.Content = sl;
                                _wnd.Show();

                            }
                            else
                            {
                                SLVideoPlayer sl = new SLVideoPlayer(_mediaSource, _startFrame);
                                _wnd.Content = sl;
                                _wnd.Show();
                            }
                        }
                    };
                }
                else
                {
                    WSPGMSendSQL.SendSQLSoapClient PlayLVFilePath = new WSPGMSendSQL.SendSQLSoapClient();
                    PlayLVFilePath.GetLVFilePathAsync(PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSFILE_NO, int.Parse(PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FNBREAK_NO));

                    PlayLVFilePath.GetLVFilePathCompleted += (s, args) =>
                        {
                            if (args.Result != null && args.Result != "")
                            {
                                _mediaSource = args.Result;
                                _startFrame = 0;
                                if (_wnd == null)
                                {
                                    _wnd = new Telerik.Windows.Controls.RadWindow();
                                    _wnd.Header = "檔案預覽";
                                    _wnd.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
                                    _wnd.TopOffset = -100;
                                    _wnd.Width = 640;
                                    //_wnd.Height = 506;

                                    StyleManager.SetTheme(_wnd, new Office_SilverTheme());

                                    SLVideoPlayer sl = new SLVideoPlayer(_mediaSource, _startFrame);
                                    _wnd.Content = sl;
                                    _wnd.Show();

                                }
                                else
                                {
                                    SLVideoPlayer sl = new SLVideoPlayer(_mediaSource, _startFrame);
                                    _wnd.Content = sl;
                                    _wnd.Show();
                                }
                            }
                        };
                }


            }
            else
            {
                MessageBox.Show("此檔案尚未有編號,請檢察此檔案是否已經送帶轉檔!");
            }
        }

        private void LayoutRoot_KeyDown(object sender, KeyEventArgs e)
        {
            if ((Keyboard.Modifiers & ModifierKeys.Shift) == ModifierKeys.Shift)
            {
                if (e.Key == Key.I)
                {
                    buttonInsertPromo_Click(sender, e);
                }
                if (e.Key == Key.R)
                {
                    buttonReplacePromo_Click(sender, e);
                }
                if (e.Key == Key.D)
                {
                    buttonDeletePromo_Click(sender, e);
                }
                if (e.Key == Key.E)
                {
                    buttonReplaceAllPromo_Click(sender, e);
                }
                if (e.Key == Key.Q)
                {
                    Button_Click(sender, e);
                }
                if (e.Key == Key.L)
                {
                    ButtonReLoad_Click(sender, e);
                }
                if (e.Key == Key.M)
                {
                    ButtonQueryPromo_Click(sender, e);
                }
                if (e.Key == Key.T)
                {
                    buttonReCalTime_Click(sender, e);
                }
                if (e.Key == Key.B)
                {
                    buttonBookingList_Click(sender, e);
                }
                if (e.Key == Key.P)
                {
                    buttonPromoInfo_Click(sender, e);
                }
                if (e.Key == Key.S)
                {
                    buttonSendFile_Click(sender, e);
                }
                if (e.Key == Key.G)
                {
                    buttonGetFileStatus_Click(sender, e);
                }
                if (e.Key == Key.A)
                {
                    buttonSetTimeType_Click(sender, e);
                }
                if (e.Key == Key.O)
                {
                    buttonSetTimeTypeO_Click(sender, e);
                }
                if (e.Key == Key.F)
                {
                    buttonPrintList_Click(sender, e);
                }
                if (e.Key == Key.W)
                {
                    buttonSaveArrList_Click(sender, e);
                }
                if (e.Key == Key.Y)
                {
                    buttonLST_Click(sender, e);
                }
                if (e.Key == Key.U)
                {
                    buttonClosePromo_Click(sender, e);
                }

                if (e.Key == Key.H)
                {
                    ButtonChooseImport_Click(sender, e);
                }
                if (e.Key == Key.K)
                {
                    ButtonAllImport_Click(sender, e);
                }
                if (e.Key == Key.N)
                {
                    ButtonCancel_Click(sender, e);
                }
                if (e.Key == Key.C) //複製一行Promo
                {
                    if (dataGridProgList.SelectedIndex < 0)
                    {
                        MessageBox.Show("必須先選擇資料");
                        return;
                    }
                    if (PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FCTYPE != "P") //時間設定不為固定
                    {
                        MessageBox.Show("只能複製為短帶類型");
                        return;
                    }
                    Add_Copy_Promo();
                }
                if (e.Key == Key.X) //剪下一行Promo
                {
                    if (dataGridProgList.SelectedIndex < 0)
                    {
                        MessageBox.Show("必須先選擇資料");
                        return;
                    }
                    if (PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FCTYPE != "P") //時間設定不為固定
                    {
                        MessageBox.Show("只能剪下為短帶類型");
                        return;
                    }

                    int TempIndex = dataGridProgList.SelectedIndex;
                    Add_Copy_Promo();
                    //Add_Work_Change();
                    Del_Select_Promo();
                    if (TempIndex < PGM_ARR_PROMO.Count - 1)
                        dataGridProgList.SelectedIndex = TempIndex;
                    else
                        dataGridProgList.SelectedIndex = PGM_ARR_PROMO.Count - 1;
                    dataGridProgList.UpdateLayout();
                    dataGridProgList.ScrollIntoView(dataGridProgList.SelectedItem, dataGridProgList.Columns[0]);
                    //buttonDeletePromo_Click(sender, e);
                }
                if (e.Key == Key.V) //貼上一行Promo
                {
                   
                    if (CopyPromoList.Count == 0)  //if (CopyPromo.FSPROG_ID == null)
                    {
                        MessageBox.Show("沒有資料可以貼上");
                        return;
                    }
                    if (dataGridProgList.SelectedIndex < 0)
                    {
                        MessageBox.Show("必須先選擇資料");
                        return;
                    }

                    //Add_Work_Change();
                    CopyPastePromo();

                }
                if (e.Key == Key.Z) //回復
                {
                    if (PGM_ARR_PROMO1.Count != 0)
                    {
                        int TempIndex = dataGridProgList.SelectedIndex;
                        Back_Work_Change();
                        dataGridProgList.ItemsSource = null;
                        dataGridProgList.ItemsSource = PGM_ARR_PROMO;
                        dataGridProgList.SelectedIndex = TempIndex;
                        dataGridProgList.UpdateLayout();
                        dataGridProgList.ScrollIntoView(dataGridProgList.SelectedItem, dataGridProgList.Columns[0]); 
                    }
                    else
                    {
                        MessageBox.Show("沒有可以回覆的資訊");
                    }
                }

            }

            if ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
            {
                if (e.Key == Key.C) //複製一行Promo
                {
                    if (dataGridProgList.SelectedIndex < 0)
                    {
                        MessageBox.Show("必須先選擇資料");
                        return;
                    }
                    if (PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FCTYPE != "P") //時間設定不為固定
                    {
                        MessageBox.Show("只能複製為短帶類型");
                        return;
                    }
                    Add_Copy_Promo();
                }
                if (e.Key == Key.X) //剪下一行Promo
                {
                    if (dataGridProgList.SelectedIndex < 0)
                    {
                        MessageBox.Show("必須先選擇資料");
                        return;
                    }
                    if (PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FCTYPE != "P") //時間設定不為固定
                    {
                        MessageBox.Show("只能剪下為短帶類型");
                        return;
                    }
                    int TempIndex = dataGridProgList.SelectedIndex;
                    Add_Copy_Promo();
                    //Add_Work_Change();
                    Del_Select_Promo();
                    if (TempIndex < PGM_ARR_PROMO.Count - 1)
                        dataGridProgList.SelectedIndex = TempIndex;
                    else
                        dataGridProgList.SelectedIndex = PGM_ARR_PROMO.Count - 1;
                    dataGridProgList.UpdateLayout();
                    dataGridProgList.ScrollIntoView(dataGridProgList.SelectedItem, dataGridProgList.Columns[0]); 
                    //buttonDeletePromo_Click(sender, e);
                }
                if (e.Key == Key.V) //貼上一行Promo
                {
                    if (CopyPromoList.Count == 0)  //if (CopyPromo.FSPROG_ID == null)
                    {
                        MessageBox.Show("沒有資料可以貼上");
                        return;
                    }
                    if (dataGridProgList.SelectedIndex < 0)
                    {
                        MessageBox.Show("必須先選擇資料");
                        return;
                    }

                    //Add_Work_Change();
                    CopyPastePromo();

                }
                if (e.Key == Key.Z) //回復
                {
                    if (PGM_ARR_PROMO1.Count != 0)
                    {
                        int TempIndex = dataGridProgList.SelectedIndex;
                        Back_Work_Change();
                        dataGridProgList.ItemsSource = null;
                        dataGridProgList.ItemsSource = PGM_ARR_PROMO;
                        dataGridProgList.SelectedIndex = TempIndex;
                        dataGridProgList.UpdateLayout();
                        dataGridProgList.ScrollIntoView(dataGridProgList.SelectedItem, dataGridProgList.Columns[0]); 
                    }
                    else
                    {
                        MessageBox.Show("沒有可以回覆的資訊");
                    }
                }
            }
        }

        private void buttonSetTimeTypeA_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i <= PGM_ARR_PROMO.Count - 1; i++)
            {
                //if (PGM_ARR_PROMO[i].FCTIME_TYPE == "O" && PGM_ARR_PROMO[i].FNLIVE == "0")
                //    PGM_ARR_PROMO[i].FCTIME_TYPE = "A";
                
                //if (PGM_ARR_PROMO[i].FCTIME_TYPE == "O")
                    PGM_ARR_PROMO[i].FCTIME_TYPE = "A";

            }
            ReCalTimecode();
        }


        //取得輸入字串前幾個字元byte數,中文字算2byte
        public string GetSubString(string str, int getStrLength)
        {
            int StrLength = 0;

            //string tempStr = "";
            int StrSubLength = 0;
            foreach (char c in str)
            {
                if ((int)c >= 33 && (int)c <= 126)
                {
                    if ((StrLength + 1) > getStrLength)
                        return str.Substring(0, StrSubLength);
                    StrLength = StrLength + 1;
                    StrSubLength = StrSubLength + 1;
                }
                else
                {
                    int aa = (int)c;
                    //MessageBox.Show(aa.ToString());
                    if ((StrLength + 2) > getStrLength)
                        return str.Substring(0, StrSubLength);
                    StrLength = StrLength + 2;
                    StrSubLength = StrSubLength + 1;
                }


            }
            return str.Substring(0, StrSubLength);
        }



        Telerik.Windows.Controls.RadWindow _PromoInsertStatusForm;
        List<Date_Arr_Promo> PromoInsertStatus = new List<Date_Arr_Promo>();

        private void buttonPromoInsertStatus_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridProgList.SelectedIndex == -1)
            {
                MessageBox.Show("請先挑選播出運行表中的宣傳帶");
                return;
            }
            PromoInsertStatus.Clear();

            foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO)
            {
                if (TemArrPromoList.FSPROMO_ID == PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSPROMO_ID)
                {
                    PromoInsertStatus.Add(new Date_Arr_Promo()
                    {
                        FSPROMO_ID = TemArrPromoList.FSPROMO_ID,
                        FSPROMO_NAME = TemArrPromoList.FSNAME,
                        FSPROG_ID = TemArrPromoList.FSPROG_ID,
                        FSPROG_NAME = TemArrPromoList.FSPROG_NAME,
                        FSPLAYTIME = TemArrPromoList.RealFSPLAY_TIME

                    });
                }
            }

            if (_PromoInsertStatusForm == null)
            {
                _PromoInsertStatusForm = new Telerik.Windows.Controls.RadWindow();
                _PromoInsertStatusForm.Header = PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSNAME + "--排播狀態";
                _PromoInsertStatusForm.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
                _PromoInsertStatusForm.TopOffset = -100;
                _PromoInsertStatusForm.Width = 300;
                _PromoInsertStatusForm.Height = 300;


                DataGrid D1 = new DataGrid();

                _PromoInsertStatusForm.Content = D1;
                StyleManager.SetTheme(_PromoInsertStatusForm, new Office_SilverTheme());


                DataGridTextColumn textColumn = new DataGridTextColumn();
                textColumn.Header = "短帶編號";
                textColumn.Binding = new Binding("FSPROMO_ID");
                textColumn.Visibility = Visibility.Collapsed;

                DataGridTextColumn textColumn1 = new DataGridTextColumn();
                textColumn1.Header = "短帶名稱";
                textColumn1.Binding = new Binding("FSPROMO_NAME");
                textColumn1.Visibility = Visibility.Collapsed;

                DataGridTextColumn textColumn2 = new DataGridTextColumn();
                textColumn2.Header = "節目編碼";
                textColumn2.Binding = new Binding("FSPROG_ID");
                textColumn2.Visibility = Visibility.Collapsed;

                DataGridTextColumn textColumn3 = new DataGridTextColumn();
                textColumn3.Header = "所屬節目名稱";
                textColumn3.Binding = new Binding("FSPROG_NAME");

                DataGridTextColumn textColumn4 = new DataGridTextColumn();
                textColumn4.Header = "播出時間";
                textColumn4.Binding = new Binding("FSPLAYTIME");


                D1.Columns.Add(textColumn);
                D1.Columns.Add(textColumn1);
                D1.Columns.Add(textColumn2);
                D1.Columns.Add(textColumn3);
                D1.Columns.Add(textColumn4);
                D1.AutoGenerateColumns = false;
                D1.ItemsSource = null;
                D1.ItemsSource = PromoInsertStatus;

                _PromoInsertStatusForm.Show();

            }
            else
            {
                _PromoInsertStatusForm.Header = PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSNAME + "--排播狀態";
                DataGrid D1 = new DataGrid();

                _PromoInsertStatusForm.Content = D1;
                StyleManager.SetTheme(_PromoInsertStatusForm, new Office_SilverTheme());


                DataGridTextColumn textColumn = new DataGridTextColumn();
                textColumn.Header = "短帶編號";
                textColumn.Binding = new Binding("FSPROMO_ID");
                textColumn.Visibility = Visibility.Collapsed;

                DataGridTextColumn textColumn1 = new DataGridTextColumn();
                textColumn1.Header = "短帶名稱";
                textColumn1.Binding = new Binding("FSPROMO_NAME");
                textColumn1.Visibility = Visibility.Collapsed;

                DataGridTextColumn textColumn2 = new DataGridTextColumn();
                textColumn2.Header = "節目編碼";
                textColumn2.Binding = new Binding("FSPROG_ID");
                textColumn2.Visibility = Visibility.Collapsed;

                DataGridTextColumn textColumn3 = new DataGridTextColumn();
                textColumn3.Header = "所屬節目名稱";
                textColumn3.Binding = new Binding("FSPROG_NAME");

                DataGridTextColumn textColumn4 = new DataGridTextColumn();
                textColumn4.Header = "播出時間";
                textColumn4.Binding = new Binding("FSPLAYTIME");


                D1.Columns.Add(textColumn);
                D1.Columns.Add(textColumn1);
                D1.Columns.Add(textColumn2);
                D1.Columns.Add(textColumn3);
                D1.Columns.Add(textColumn4);
                D1.AutoGenerateColumns = false;
                D1.ItemsSource = null;
                D1.ItemsSource = PromoInsertStatus;
                _PromoInsertStatusForm.Show();
            }
        }


        List<PgmQueueDate> PgmQueueSource = new List<PgmQueueDate>();
        //自動插入空白行
        private void buttonAutoInsNullPromo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //RR.Header="Progid";
                //radGridView1.Columns.Add(RR);
                //RR.Header="ProgNAME";
                //radGridView1.Columns.Add(RR);
                if (_Date == "")
                {
                    MessageBox.Show("請先查詢播出運行表");
                    return;
                }


                WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_QUEUE = new WSPGMSendSQL.SendSQLSoapClient();
                //string ReturnStr = "";

                StringBuilder sb = new StringBuilder();
                object ReturnXML = "";
                sb.AppendLine("<Data>");
                //sb.AppendLine("<FDDATE>" + this.textBoxDate.Text + "</FDDATE>");

                //_Date="2010/12/16";
                //_ChannelID="06";
                sb.AppendLine("<FDDATE>" + _Date + "</FDDATE>");
                sb.AppendLine("<FSCHANNEL_ID>" + _ChannelID + "</FSCHANNEL_ID>");
                sb.AppendLine("</Data>");
                SP_Q_TBPGM_QUEUE.Do_QueryAsync("SP_Q_TBPGM_QUEUE", sb.ToString(), ReturnXML);
                //SP_Q_TBPGM_QUEUE.Do_QueryCompleted += new EventHandler<SendSQL.Do_QueryCompletedEventArgs>(SP_Q_TBPGM_QUEUE_Do_QueryCompleted);
                //AAA.Do_InsertAsync("", "", ReturnStr);
                SP_Q_TBPGM_QUEUE.Do_QueryCompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (args.Result != null && args.Result != "")
                        {

                            byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                            PgmQueueSource.Clear();
                            StringBuilder output = new StringBuilder();
                            // Create an XmlReader
                            XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                            var ltox = from str in doc.Elements("Datas").Elements("Data")
                                       select str;
                            foreach (XElement elem in ltox)
                            {

                                string RealBTime;
                                string RealETime;
                                if (int.Parse(elem.Element("FSBEG_TIME").Value.ToString()) >= 2400)
                                    RealBTime = (int.Parse(elem.Element("FSBEG_TIME").Value.ToString()) - 2400).ToString("0000");
                                else
                                    RealBTime = elem.Element("FSBEG_TIME").Value.ToString();

                                if (int.Parse(elem.Element("FSEND_TIME").Value.ToString()) >= 2400)
                                    RealETime = (int.Parse(elem.Element("FSEND_TIME").Value.ToString()) - 2400).ToString("0000");
                                else
                                    RealETime = elem.Element("FSEND_TIME").Value.ToString();

                                PgmQueueSource.Add(new PgmQueueDate()
                                {
                                    ProgID = elem.Element("FSPROG_ID").Value.ToString(),
                                    Seqno = elem.Element("FNSEQNO").Value.ToString(),
                                    ProgName = elem.Element("FSPROG_NAME").Value.ToString(),

                                    ProgDate = (Convert.ToDateTime(elem.Element("FDDATE").Value)).ToString("yyyy/MM/dd"),
                                    //ProgDate = elem.Element("FDDATE").Value.ToString(),
                                    CHANNEL_ID = elem.Element("FSCHANNEL_ID").Value.ToString(),
                                    BEG_TIME = elem.Element("FSBEG_TIME").Value.ToString(),
                                    END_TIME = elem.Element("FSEND_TIME").Value.ToString(),
                                    REPLAY = elem.Element("FNREPLAY").Value.ToString(),
                                    EPISODE = elem.Element("FNEPISODE").Value.ToString(),
                                    TAPENO = elem.Element("FSTAPENO").Value.ToString(),
                                    PROG_NAME = elem.Element("FSPROG_NAME").Value.ToString(),
                                    LIVE = elem.Element("FNLIVE").Value.ToString(),
                                    DEP = elem.Element("FNDEP").Value.ToString(),
                                    ONOUT = elem.Element("FNONOUT").Value.ToString(),
                                    MEMO = elem.Element("FSMEMO").Value.ToString(),
                                    MEMO1 = elem.Element("FSMEMO1").Value.ToString(),
                                    CHANNEL_NAME = elem.Element("FSCHANNEL_NAME").Value.ToString(),
                                    FILETYPE = "",
                                    ProgDefSeg = elem.Element("FNSEG").Value.ToString(),
                                    RealBEG_TIME = RealBTime,
                                    RealEND_TIME = RealETime,
                                    DEF_MIN = elem.Element("FNDEF_MIN").Value.ToString(),
                                    DEF_SEC = elem.Element("FNDEF_SEC").Value.ToString(),
                                });
                            }

                            foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO)
                            {
                                string Btime = "";
                                string Etime = "";
                                int ProgDur = 0; //此節目長度(分鐘)
                                int nowProgDur = 0; //目前已經有多長的影像(Frame)
                                if (TemArrPromoList.FSNAME.Substring(0, 10) == "----------")
                                {
                                    foreach (PgmQueueDate TempQueue in PgmQueueSource)
                                    {
                                        if (TemArrPromoList.FSPROG_ID == TempQueue.ProgID & TemArrPromoList.FNSEQNO == TempQueue.Seqno)
                                        {
                                            Btime = TempQueue.BEG_TIME;
                                            Etime = TempQueue.END_TIME;
                                            ProgDur = int.Parse(Etime.Substring(0, 2)) * 60 + int.Parse(Etime.Substring(2, 2)) - int.Parse(Btime.Substring(0, 2)) - int.Parse(Btime.Substring(2, 2));


                                            foreach (ArrPromoList TemArrPromoList1 in PGM_ARR_PROMO)
                                            { //取得此節目已經插入多少長度
                                                if (TemArrPromoList1.FSPROG_ID == TempQueue.ProgID & TemArrPromoList1.FNSEQNO == TempQueue.Seqno)
                                                {
                                                    if (TemArrPromoList1.FSDURATION != "")
                                                    {
                                                        nowProgDur = nowProgDur + TransferTimecode.timecodetoframe(TemArrPromoList1.FSDURATION);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }


                            }

                        } //if (args.Result != null && args.Result !="")
                        else
                        {
                            return;
                            //MessageBox.Show("節目表不存在");
                        }
                    }
                };
            }
            catch
            {

                //MessageBox.Show("查詢節目表失敗!");
            }


        }

        private void ButtonChooseImportLostPromo_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridProgList.SelectedIndex < 0)
            {
                MessageBox.Show("必須先選擇要插入Promo的位置");
                return;
            }

            int GridIndex = dataGridProgList.SelectedIndex;
            StringBuilder sb = new StringBuilder();
            Add_Work_Change();
            sb.AppendLine("<Datas>");
            for (int i = 0; i <= Arr_Promo_Lost_List.Count - 1; i++)
            {
                if (Arr_Promo_Lost_List[i].ISCHECK == true)
                {
                    if (InsertPromo(GridIndex, Arr_Promo_Lost_List[i].FSPROMO_ID, Arr_Promo_Lost_List[i].FSNAME, Arr_Promo_Lost_List[i].FSDURATION, Arr_Promo_Lost_List[i].FSVIDEO_ID, Arr_Promo_Lost_List[i].FSFILE_NO,Arr_Promo_Lost_List[i].FSSIGNAL) == true)
                    {
                        GridIndex = GridIndex + 1;
                        Arr_Promo_Lost_List[i].FSSTATUS = "Y";
                        sb.AppendLine("<Data>");
                        sb.AppendLine("<FNCREATE_LIST_NO>" + Arr_Promo_Lost_List[i].FNCREATE_LIST_NO + "</FNCREATE_LIST_NO>");
                        sb.AppendLine("<FNARR_PROMO_NO>" + Arr_Promo_Lost_List[i].COMBINENO + "</FNARR_PROMO_NO>");
                        sb.AppendLine("<FDDATE>" + Arr_Promo_Lost_List[i].FDDATE.Split(' ')[0] + "</FDDATE>");
                        sb.AppendLine("<FSCHANNEL_ID>" + Arr_Promo_Lost_List[i].FSCHANNEL_ID + "</FSCHANNEL_ID>");
                        sb.AppendLine("<FSSTATUS>" + "Y" + "</FSSTATUS>");
                        sb.AppendLine("</Data>");
                    }

                }
            }
            ReCalTimecode();

            sb.AppendLine("</Datas>");


            WSPGMSendSQL.SendSQLSoapClient SP_U_TBPGM_ARR_PROMO_LOST = new WSPGMSendSQL.SendSQLSoapClient();


            SP_U_TBPGM_ARR_PROMO_LOST.Do_Insert_ListAsync("", "", "SP_U_TBPGM_ARR_PROMO_LOST", sb.ToString());
            SP_U_TBPGM_ARR_PROMO_LOST.Do_Insert_ListCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result == true)
                    {
                        dataGridProgList.ItemsSource = null;
                        dataGridProgList.ItemsSource = PGM_ARR_PROMO;
                        DCdataGridLostPromo.ItemsSource = null;
                        DCdataGridLostPromo.ItemsSource = Arr_Promo_Lost_List;
                        MessageBox.Show("依選擇移入作業完成");
                    }
                    else
                        MessageBox.Show("無法修改資料庫狀態");
                }
            };
        }

        private void ButtonAllImportLostPromo_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridProgList.SelectedIndex < 0)
            {
                MessageBox.Show("必須先選擇要插入Promo的位置");
                return;
            }

            int GridIndex = dataGridProgList.SelectedIndex;
            StringBuilder sb = new StringBuilder();
            Add_Work_Change();
            sb.AppendLine("<Datas>");
            for (int i = 0; i <= Arr_Promo_Lost_List.Count - 1; i++)
            {
                if (Arr_Promo_Lost_List[i].FSSTATUS == "N" || Arr_Promo_Lost_List[i].FSSTATUS == "")
                {

                    if (InsertPromo(GridIndex, Arr_Promo_Lost_List[i].FSPROMO_ID, Arr_Promo_Lost_List[i].FSNAME, Arr_Promo_Lost_List[i].FSDURATION, Arr_Promo_Lost_List[i].FSVIDEO_ID, Arr_Promo_Lost_List[i].FSFILE_NO, Arr_Promo_Lost_List[i].FSSIGNAL) == true)
                    {
                        GridIndex = GridIndex + 1;
                        Arr_Promo_Lost_List[i].FSSTATUS = "Y";
                        sb.AppendLine("<Data>");
                        sb.AppendLine("<FNCREATE_LIST_NO>" + Arr_Promo_Lost_List[i].FNCREATE_LIST_NO + "</FNCREATE_LIST_NO>");
                        sb.AppendLine("<FNARR_PROMO_NO>" + Arr_Promo_Lost_List[i].COMBINENO + "</FNARR_PROMO_NO>");
                        sb.AppendLine("<FDDATE>" + Arr_Promo_Lost_List[i].FDDATE.Split(' ')[0] + "</FDDATE>");
                        sb.AppendLine("<FSCHANNEL_ID>" + Arr_Promo_Lost_List[i].FSCHANNEL_ID + "</FSCHANNEL_ID>");
                        sb.AppendLine("<FSSTATUS>" + "Y" + "</FSSTATUS>");
                        sb.AppendLine("</Data>");
                    }

                }
            }
            ReCalTimecode();
            sb.AppendLine("</Datas>");
            WSPGMSendSQL.SendSQLSoapClient SP_U_TBPGM_ARR_PROMO_LOST = new WSPGMSendSQL.SendSQLSoapClient();
            SP_U_TBPGM_ARR_PROMO_LOST.Do_Insert_ListAsync("", "", "SP_U_TBPGM_ARR_PROMO_LOST", sb.ToString());
            SP_U_TBPGM_ARR_PROMO_LOST.Do_Insert_ListCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result == true)
                    {
                        dataGridProgList.ItemsSource = null;
                        dataGridProgList.ItemsSource = PGM_ARR_PROMO;
                        DCdataGridLostPromo.ItemsSource = null;
                        DCdataGridLostPromo.ItemsSource = Arr_Promo_Lost_List;
                        MessageBox.Show("全部移入作業完成");
                    }
                    else
                        MessageBox.Show("無法修改資料庫狀態");
                }
            };
        }

        private void ButtonCancelLostPromo_Click(object sender, RoutedEventArgs e)
        {
            canvasLostPromo.Visibility = Visibility.Collapsed;
        }

        private void buttonLostPromoList_Click(object sender, RoutedEventArgs e)
        {
            if (_Date == "")
            {
                MessageBox.Show("請先選擇日期");
                return;
            }
            if (_ChannelID == "")
            {
                MessageBox.Show("請先選擇頻道");
                return;
            }
            QueryLostPromo();
            canvasLostPromo.Visibility = Visibility.Visible;
        }

        private void buttonCheckSeg_Click(object sender, RoutedEventArgs e)
        {
            if (_ChannelID == "")
            {
                MessageBox.Show("請先查詢節目表");
                return;
            }

            WSPGMSendSQL.SendSQLSoapClient TransList = new WSPGMSendSQL.SendSQLSoapClient();
            System.Collections.ObjectModel.ObservableCollection<WSPGMSendSQL.ArrPromoList> InList = new System.Collections.ObjectModel.ObservableCollection<WSPGMSendSQL.ArrPromoList>();

            foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO)
            {
                if (TemArrPromoList.FSPROG_ID != "")
                {
                    InList.Add(new WSPGMSendSQL.ArrPromoList()
                    {
                        COMBINENO = TemArrPromoList.COMBINENO,
                        FCTIME_TYPE = TemArrPromoList.FCTIME_TYPE,
                        FSPROG_ID = TemArrPromoList.FSPROG_ID,
                        FNSEQNO = TemArrPromoList.FNSEQNO,
                        FSNAME = TemArrPromoList.FSNAME,
                        FSCHANNEL_ID = TemArrPromoList.FSCHANNEL_ID,
                        FSCHANNEL_NAME = TemArrPromoList.FSCHANNEL_NAME,
                        FSPLAY_TIME = TemArrPromoList.FSPLAY_TIME,
                        FSDURATION = TemArrPromoList.FSDURATION,
                        FNEPISODE = TemArrPromoList.FNEPISODE,
                        FNBREAK_NO = TemArrPromoList.FNBREAK_NO,
                        FNSUB_NO = TemArrPromoList.FNSUB_NO,
                        FSFILE_NO = TemArrPromoList.FSFILE_NO,
                        FSVIDEO_ID = TemArrPromoList.FSVIDEO_ID,
                        FSPROMO_ID = TemArrPromoList.FSPROMO_ID,
                        FDDATE = TemArrPromoList.FDDATE,
                        FNLIVE = TemArrPromoList.FNLIVE,
                        FSSTATUS = TemArrPromoList.FSSTATUS,
                        FCTYPE = TemArrPromoList.FCTYPE,
                        FSMEMO = TemArrPromoList.FSMEMO,
                        FSPROG_NAME = TemArrPromoList.FSPROG_NAME
                    });
                }
            }
            
            TransList.CheckArrListFileSegAsync(InList);
            TransList.CheckArrListFileSegCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "ok" && args.Result != "")
                    {

                        MessageBox.Show("此架構中的檔案與段落資料中的不符,請重新載入"+ Convert.ToChar(10) +Convert.ToChar(13) + args.Result);
                         //System.Windows.Browser.HtmlPage.Window.Navigate(new Uri(args.Result, UriKind.Absolute));
                    }
                    else
                        MessageBox.Show("檢查完成,並未發現不符狀態");
                }

            };
        }

        private void buttonPrintNoTape_Click(object sender, RoutedEventArgs e)
        {


            if (this.DatePickerDate.Text == "")
            {
                MessageBox.Show("請先選擇日期");
                return;
            }
            string[] ChannelType = ((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString().Split(new Char[] { '-' });
            _ChannelType = ChannelType[1];
            if (ChannelType[0] == "")
            {
                MessageBox.Show("請先選擇頻道");
                return;
            }
            if (myDispatcherTimer.IsEnabled == true)
            {
                MessageBox.Show("您需要先儲存播出運行表!");
                return;
            }

            
            WSPGMSendSQL.SendSQLSoapClient SP_I_RPT_TBPGM_ARR_PROMO_NO_TAPE_LIST= new WSPGMSendSQL.SendSQLSoapClient();
            SP_I_RPT_TBPGM_ARR_PROMO_NO_TAPE_LIST.Get_Arr_Promo_NO_Tape_ListAsync(this.DatePickerDate.Text, ChannelType[0], UserClass.userData.FSUSER_ChtName);
            SP_I_RPT_TBPGM_ARR_PROMO_NO_TAPE_LIST.Get_Arr_Promo_NO_Tape_ListCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result == "沒有未到帶清單")
                    {
                        MessageBox.Show("沒有未到帶清單");
                    }
                    else
                    {
                        HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + args.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,toolbar=0,statusbar=0,menubar=0,scrollbar=0,resizable=yes");

                    }

                }
                else
                    MessageBox.Show("產生未到帶清單失敗");
                
            };       
        }


        //打開比對後的資料表單
        public void OpenDiffForm()
        {
            #region 顯示不同段落與Promo的Form
            if (_CheckDiffProgFom == null)
            {
                _CheckDiffProgFom = new Telerik.Windows.Controls.RadWindow();
                _CheckDiffProgFom.Header = "比對目前架構與重新合併作業架構的不同";
                _CheckDiffProgFom.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
                _CheckDiffProgFom.TopOffset = -100;
                _CheckDiffProgFom.Width = 1000;
                _CheckDiffProgFom.Height = 500;
 
                StyleManager.SetTheme(_CheckDiffProgFom, new Office_SilverTheme());
                PGM125_04 CheckDiffForm=new PGM125_04(DiffProgList,DiffPromoList,_Date,_ChannelID,this);

                _CheckDiffProgFom.Content=CheckDiffForm;
                
                _CheckDiffProgFom.Show();
            }
            else
            {
                PGM125_04 CheckDiffForm = new PGM125_04(DiffProgList, DiffPromoList,_Date,_ChannelID,this);

                _CheckDiffProgFom.Content = CheckDiffForm;

                _CheckDiffProgFom.Show();
            }
            #endregion
            #region 舊的顯示不同段落與Promo的Form
            //if (_CheckDiffProgFom == null)
            //{
            //    _CheckDiffProgFom = new Telerik.Windows.Controls.RadWindow();
            //    _CheckDiffProgFom.Header = "比對目前架構與重新合併作業架構的不同";
            //    _CheckDiffProgFom.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            //    _CheckDiffProgFom.TopOffset = -100;
            //    _CheckDiffProgFom.Width = 300;
            //    _CheckDiffProgFom.Height = 300;

            //    DataGrid D1 = new DataGrid();

            //    _CheckDiffProgFom.Content = D1;
            //    StyleManager.SetTheme(_CheckDiffProgFom, new Office_SilverTheme());

            //    DataGridTextColumn textColumn1 = new DataGridTextColumn();
            //    textColumn1.Header = "重新合併節目ID";
            //    textColumn1.Binding = new Binding("NowProgID");
                
            //    DataGridTextColumn textColumn2 = new DataGridTextColumn();
            //    textColumn2.Header = "節目名稱";
            //    textColumn2.Binding = new Binding("NowProgName");
                
            //    DataGridTextColumn textColumn3 = new DataGridTextColumn();
            //    textColumn3.Header = "集數";
            //    textColumn3.Binding = new Binding("NowProgEpisode");

            //    DataGridTextColumn textColumn4 = new DataGridTextColumn();
            //    textColumn4.Header = "段落";
            //    textColumn4.Binding = new Binding("NowProgSeg");

            //    DataGridTextColumn textColumn5 = new DataGridTextColumn();
            //    textColumn5.Header = "長度";
            //    textColumn5.Binding = new Binding("NowProgDur");
                
            //    DataGridTextColumn textColumn6 = new DataGridTextColumn();
            //    textColumn6.Header = "VideoID";
            //    textColumn6.Binding = new Binding("NowProgVIdeoID");
                
            //    DataGridTextColumn textColumn7 = new DataGridTextColumn();
            //    textColumn7.Header = "目前架構中節目編碼";
            //    textColumn7.Binding = new Binding("QueueProgID");
                
            //    DataGridTextColumn textColumn8 = new DataGridTextColumn();
            //    textColumn8.Header = "節目名稱";
            //    textColumn8.Binding = new Binding("QueueProgName");

            //    DataGridTextColumn textColumn9 = new DataGridTextColumn();
            //    textColumn9.Header = "集數";
            //    textColumn9.Binding = new Binding("QueueProgEpisode");

            //    DataGridTextColumn textColumn10 = new DataGridTextColumn();
            //    textColumn10.Header = "段落";
            //    textColumn10.Binding = new Binding("QueueProgSeg");

            //    DataGridTextColumn textColumn11 = new DataGridTextColumn();
            //    textColumn11.Header = "長度";
            //    textColumn11.Binding = new Binding("QueueProgDur");

            //    DataGridTextColumn textColumn12 = new DataGridTextColumn();
            //    textColumn12.Header = "VideoID";
            //    textColumn12.Binding = new Binding("QueueProgVIdeoID");

            //    DataGridTextColumn textColumn13 = new DataGridTextColumn();
            //    textColumn13.Header = "狀態";
            //    textColumn13.Binding = new Binding("FSSTATUS");

             
               
            //    D1.Columns.Add(textColumn1);
            //    D1.Columns.Add(textColumn2);
            //    D1.Columns.Add(textColumn3);
            //    D1.Columns.Add(textColumn4);
            //    D1.Columns.Add(textColumn5);
            //    D1.Columns.Add(textColumn6);
            //    D1.Columns.Add(textColumn7);
            //    D1.Columns.Add(textColumn8);
            //    D1.Columns.Add(textColumn9);
            //    D1.Columns.Add(textColumn10);
            //    D1.Columns.Add(textColumn11);
            //    D1.Columns.Add(textColumn12);
            //    D1.Columns.Add(textColumn13);
            //    D1.AutoGenerateColumns = false;
            //    D1.ItemsSource = null;
            //    D1.ItemsSource = DiffProgList;

            //    _CheckDiffProgFom.Show();
            //}
            //else
            //{
            //    _CheckDiffProgFom.Header = "比對目前架構與重新合併作業架構的不同";
            //    DataGrid D1 = new DataGrid();

            //    _CheckDiffProgFom.Content = D1;
            //    StyleManager.SetTheme(_CheckDiffProgFom, new Office_SilverTheme());

            //    DataGridTextColumn textColumn1 = new DataGridTextColumn();
            //    textColumn1.Header = "重新合併";
            //    textColumn1.Binding = new Binding("NowProgID");
               
            //    DataGridTextColumn textColumn2 = new DataGridTextColumn();
            //    textColumn2.Header = "節目名稱";
            //    textColumn2.Binding = new Binding("NowProgName");
                
            //    DataGridTextColumn textColumn3 = new DataGridTextColumn();
            //    textColumn3.Header = "集數";
            //    textColumn3.Binding = new Binding("NowProgEpisode");

            //    DataGridTextColumn textColumn4 = new DataGridTextColumn();
            //    textColumn4.Header = "段落";
            //    textColumn4.Binding = new Binding("NowProgSeg");

            //    DataGridTextColumn textColumn5 = new DataGridTextColumn();
            //    textColumn5.Header = "長度";
            //    textColumn5.Binding = new Binding("NowProgDur");

            //    DataGridTextColumn textColumn6 = new DataGridTextColumn();
            //    textColumn6.Header = "VideoID";
            //    textColumn6.Binding = new Binding("NowProgVIdeoID");

            //    DataGridTextColumn textColumn7 = new DataGridTextColumn();
            //    textColumn7.Header = "目前架構";
            //    textColumn7.Binding = new Binding("QueueProgID");

            //    DataGridTextColumn textColumn8 = new DataGridTextColumn();
            //    textColumn8.Header = "節目名稱";
            //    textColumn8.Binding = new Binding("QueueProgName");

            //    DataGridTextColumn textColumn9 = new DataGridTextColumn();
            //    textColumn9.Header = "集數";
            //    textColumn9.Binding = new Binding("QueueProgEpisode");

            //    DataGridTextColumn textColumn10 = new DataGridTextColumn();
            //    textColumn10.Header = "段落";
            //    textColumn10.Binding = new Binding("QueueProgSeg");

            //    DataGridTextColumn textColumn11 = new DataGridTextColumn();
            //    textColumn11.Header = "長度";
            //    textColumn11.Binding = new Binding("QueueProgDur");

            //    DataGridTextColumn textColumn12 = new DataGridTextColumn();
            //    textColumn12.Header = "VideoID";
            //    textColumn12.Binding = new Binding("QueueProgVIdeoID");

            //    DataGridTextColumn textColumn13 = new DataGridTextColumn();
            //    textColumn13.Header = "狀態";
            //    textColumn13.Binding = new Binding("FSSTATUS");

            //    D1.Columns.Add(textColumn1);
            //    D1.Columns.Add(textColumn2);
            //    D1.Columns.Add(textColumn3);
            //    D1.Columns.Add(textColumn4);
            //    D1.Columns.Add(textColumn5);
            //    D1.Columns.Add(textColumn6);
            //    D1.Columns.Add(textColumn7);
            //    D1.Columns.Add(textColumn8);
            //    D1.Columns.Add(textColumn9);
            //    D1.Columns.Add(textColumn10);
            //    D1.Columns.Add(textColumn11);
            //    D1.Columns.Add(textColumn12);
            //    D1.Columns.Add(textColumn13);
            //    D1.AutoGenerateColumns = false;
            //    D1.ItemsSource = null;
            //    D1.ItemsSource = DiffProgList;
            //    _CheckDiffProgFom.Show();
            //}
            #endregion
        }


        Telerik.Windows.Controls.RadWindow _CheckDiffProgFom;
        List<CheckDiffProg> DiffProgList = new List<CheckDiffProg>();
        List<CheckDiffPromo> DiffPromoList = new List<CheckDiffPromo>();

        private void buttonCheckDiffSeg_Click(object sender, RoutedEventArgs e)
        {
            if (_ChannelID == "")
            {
                MessageBox.Show("請先查詢節目表");
                return;
            }
            BusyMsg.IsBusy = true;
            WSPGMSendSQL.SendSQLSoapClient TransList = new WSPGMSendSQL.SendSQLSoapClient();
            if (_ChannelType == "001")
            {
                TransList.CheckDiffProgListAsync(_ChannelID, _Date, _ChannelType);
                TransList.CheckDiffProgListCompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (args.Result != null && args.Result != "ok" && args.Result != "" && args.Result != "<Datas></Datas>")
                        {

                            byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                            DiffProgList.Clear();
                            DiffPromoList.Clear();
                            StringBuilder output = new StringBuilder();
                            // Create an XmlReader
                            XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                            var ltox = from str in doc.Elements("Datas").Elements("Data")
                                       select str;

                            foreach (XElement elem in ltox)
                            {
                                if (XElementToString(elem.Element("QueueProgID")) != "PROMO")
                                {
                                    DiffProgList.Add(new CheckDiffProg()
                                    {
                                        NowProgID = XElementToString(elem.Element("NowProgID")),
                                        NowProgName = XElementToString(elem.Element("NowProgName")),
                                        NowProgEpisode = XElementToString(elem.Element("NowProgEpisode")),
                                        NowProgSeg = XElementToString(elem.Element("NowProgSeg")),
                                        NowProgDur = XElementToString(elem.Element("NowProgDur")),
                                        NowProgVIdeoID = XElementToString(elem.Element("NowProgVIdeoID")),
                                        NowProgFileNO = XElementToString(elem.Element("NowProgFileNO")),
                                        QueueProgID = XElementToString(elem.Element("QueueProgID")),
                                        QueueProgName = XElementToString(elem.Element("QueueProgName")),
                                        QueueProgEpisode = XElementToString(elem.Element("QueueProgEpisode")),
                                        QueueProgSeg = XElementToString(elem.Element("QueueProgSeg")),
                                        QueueProgDur = XElementToString(elem.Element("QueueProgDur")),
                                        QueueProgVIdeoID = XElementToString(elem.Element("QueueProgVIdeoID")),
                                        QueueProgFileNO = XElementToString(elem.Element("QueueProgFileNO")),
                                        FSSTATUS = XElementToString(elem.Element("FSSTATUS"))
                                    });
                                }
                                else
                                {

                                    string AfterDur = "";  //修正後長度
                                    string DiffDur = "";  //相差長度
                                    if (XElementToString(elem.Element("NowProgEpisode")) == "")
                                    {
                                        AfterDur = "";
                                        DiffDur = "";
                                    }
                                    else
                                    {
                                        int PromoDurFrame = TransferTimecode.timecodetoframe(XElementToString(elem.Element("NowProgDur")));
                                        int RealDurFrame = TransferTimecode.timecodetoframe(XElementToString(elem.Element("NowProgSeg"))) - TransferTimecode.timecodetoframe(XElementToString(elem.Element("NowProgEpisode")));
                                        AfterDur = TransferTimecode.frame2timecode(RealDurFrame);

                                        if (PromoDurFrame >= RealDurFrame)
                                            DiffDur = TransferTimecode.frame2timecode(PromoDurFrame - RealDurFrame);
                                        else
                                            DiffDur = "-" + TransferTimecode.frame2timecode(RealDurFrame - PromoDurFrame);
                                    }
                                    DiffPromoList.Add(new CheckDiffPromo()
                                    {
                                        fspromo_id = XElementToString(elem.Element("NowProgID")),
                                        FSPROMO_NAME = XElementToString(elem.Element("NowProgName")),
                                        FSBEG_TIMECODE = XElementToString(elem.Element("NowProgEpisode")),
                                        FSEND_TIMECODE = XElementToString(elem.Element("NowProgSeg")),
                                        fsdur = XElementToString(elem.Element("NowProgDur")),
                                        fsduration = AfterDur,
                                        FSDiffDur = DiffDur,
                                        FSVIDEO_ID = XElementToString(elem.Element("NowProgVIdeoID")),
                                        FSFILE_NO = XElementToString(elem.Element("NowProgFileNO")),
                                        OLD_VIDEO_ID = XElementToString(elem.Element("QueueProgVIdeoID")),
                                        OLD_FILE_NO = XElementToString(elem.Element("QueueProgFileNO")),

                                        FSSTATUS = XElementToString(elem.Element("FSSTATUS"))
                                    });
                                }
                            }
                            //呼叫產生清單畫面
                            BusyMsg.IsBusy = false;
                            if (DiffProgList.Count == 0 && DiffPromoList.Count == 0)
                                MessageBox.Show("比對完畢,並無發現異動資料");
                            else
                                OpenDiffForm();
                            //MessageBox.Show("此架構中的檔案與段落資料中的不符,請重新載入" + Convert.ToChar(10) + Convert.ToChar(13) + args.Result);
                            //System.Windows.Browser.HtmlPage.Window.Navigate(new Uri(args.Result, UriKind.Absolute));
                        }
                        else
                        {
                            BusyMsg.IsBusy = false;
                            MessageBox.Show("比對完畢,並無發現異動資料");
                        }
                    }

                };
            }
            else
            {
                TransList.CheckDiffProgList_MIXAsync(_ChannelID, _Date, _ChannelType);
                TransList.CheckDiffProgList_MIXCompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (args.Result != null && args.Result != "ok" && args.Result != "" && args.Result != "<Datas></Datas>")
                        {

                            byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                            DiffProgList.Clear();
                            DiffPromoList.Clear();
                            StringBuilder output = new StringBuilder();
                            // Create an XmlReader
                            XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                            var ltox = from str in doc.Elements("Datas").Elements("Data")
                                       select str;

                            foreach (XElement elem in ltox)
                            {
                                if (XElementToString(elem.Element("QueueProgID")) != "PROMO")
                                {
                                    DiffProgList.Add(new CheckDiffProg()
                                    {
                                        NowProgID = XElementToString(elem.Element("NowProgID")),
                                        NowProgName = XElementToString(elem.Element("NowProgName")),
                                        NowProgEpisode = XElementToString(elem.Element("NowProgEpisode")),
                                        NowProgSeg = XElementToString(elem.Element("NowProgSeg")),
                                        NowProgDur = XElementToString(elem.Element("NowProgDur")),
                                        NowProgVIdeoID = XElementToString(elem.Element("NowProgVIdeoID")),
                                        NowProgFileNO = XElementToString(elem.Element("NowProgFileNO")),
                                        QueueProgID = XElementToString(elem.Element("QueueProgID")),
                                        QueueProgName = XElementToString(elem.Element("QueueProgName")),
                                        QueueProgEpisode = XElementToString(elem.Element("QueueProgEpisode")),
                                        QueueProgSeg = XElementToString(elem.Element("QueueProgSeg")),
                                        QueueProgDur = XElementToString(elem.Element("QueueProgDur")),
                                        QueueProgVIdeoID = XElementToString(elem.Element("QueueProgVIdeoID")),
                                        QueueProgFileNO = XElementToString(elem.Element("QueueProgFileNO")),
                                        FSSTATUS = XElementToString(elem.Element("FSSTATUS"))
                                    });
                                }
                                else
                                {

                                    string AfterDur = "";  //修正後長度
                                    string DiffDur = "";  //相差長度
                                    if (XElementToString(elem.Element("NowProgEpisode")) == "")
                                    {
                                        AfterDur = "";
                                        DiffDur = "";
                                    }
                                    else
                                    {
                                        int PromoDurFrame = TransferTimecode.timecodetoframe(XElementToString(elem.Element("NowProgDur")));
                                        int RealDurFrame = TransferTimecode.timecodetoframe(XElementToString(elem.Element("NowProgSeg"))) - TransferTimecode.timecodetoframe(XElementToString(elem.Element("NowProgEpisode")));
                                        AfterDur = TransferTimecode.frame2timecode(RealDurFrame);

                                        if (PromoDurFrame >= RealDurFrame)
                                            DiffDur = TransferTimecode.frame2timecode(PromoDurFrame - RealDurFrame);
                                        else
                                            DiffDur = "-" + TransferTimecode.frame2timecode(RealDurFrame - PromoDurFrame);
                                    }
                                    DiffPromoList.Add(new CheckDiffPromo()
                                    {
                                        fspromo_id = XElementToString(elem.Element("NowProgID")),
                                        FSPROMO_NAME = XElementToString(elem.Element("NowProgName")),
                                        FSBEG_TIMECODE = XElementToString(elem.Element("NowProgEpisode")),
                                        FSEND_TIMECODE = XElementToString(elem.Element("NowProgSeg")),
                                        fsdur = XElementToString(elem.Element("NowProgDur")),
                                        fsduration = AfterDur,
                                        FSDiffDur = DiffDur,
                                        FSVIDEO_ID = XElementToString(elem.Element("NowProgVIdeoID")),
                                        FSFILE_NO = XElementToString(elem.Element("NowProgFileNO")),
                                        OLD_VIDEO_ID = XElementToString(elem.Element("QueueProgVIdeoID")),
                                        OLD_FILE_NO = XElementToString(elem.Element("QueueProgFileNO")),

                                        FSSTATUS = XElementToString(elem.Element("FSSTATUS"))
                                    });
                                }
                            }
                            //呼叫產生清單畫面
                            BusyMsg.IsBusy = false;
                            if (DiffProgList.Count == 0 && DiffPromoList.Count == 0)
                                MessageBox.Show("比對完畢,並無發現異動資料");
                            else
                                OpenDiffForm();
                            //MessageBox.Show("此架構中的檔案與段落資料中的不符,請重新載入" + Convert.ToChar(10) + Convert.ToChar(13) + args.Result);
                            //System.Windows.Browser.HtmlPage.Window.Navigate(new Uri(args.Result, UriKind.Absolute));
                        }
                        else
                        {
                            BusyMsg.IsBusy = false;
                            MessageBox.Show("比對完畢,並無發現異動資料");
                        }
                    }

                };
            }
        }

        //列印KeyList
        private void buttonPrintKeyList_Click(object sender, RoutedEventArgs e)
        {

            WSPGMSendSQL.SendSQLSoapClient SP_Get_Key_List = new WSPGMSendSQL.SendSQLSoapClient();
            StringBuilder sbKeyList = new StringBuilder();
            object ReturnXML = "";
            sbKeyList.AppendLine("<Datas>");


            foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO)
            {
                    if (TemArrPromoList.FSMEMO != null && TemArrPromoList.FSMEMO != "")
                        sbKeyList.AppendLine("<FSMEMO>" + TransferTimecode.ReplaceXML(TemArrPromoList.FSMEMO) + "</FSMEMO>");

            }
            sbKeyList.AppendLine("</Datas>");
            
            //sb=sb.Replace("\r\n", "");
            SP_Get_Key_List.Get_Key_ListAsync(sbKeyList.ToString(),_ChannelID, _Date, UserClass.userData.FSUSER_ChtName.ToString(),_ChannelName);
            SP_Get_Key_List.Get_Key_ListCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result == "" || args.Result == null)
                    {
                        MessageBox.Show("沒有KeyList");
                    }
                    else
                    {
                        if (args.Result.Substring(0, 5) == "ERROR")
                            MessageBox.Show("有錯誤發生:訊息: " + args.Result);
                        else
                            HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + args.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,toolbar=0,statusbar=0,menubar=0,scrollbar=0,resizable=yes");
                    }
                }
            };

            
        }

        private void PageInsertPromo_Unloaded(object sender, RoutedEventArgs e)
        {

            if (myDispatcherTimer.IsEnabled == true)
            {
                if (MessageBox.Show("播出運行表有異動,請問是否需要在離開前儲存播出運行表?", "關閉頁面", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                {
                    buttonSaveArrList_Click(sender, e);
                }
            } 
        }

        private void comboBoxSignal1_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            myDispatcherTimer.Start();
        }


        public void UpdateGridPromoDur(List<CheckDiffPromo> ListDiffPromo)
        {
            Add_Work_Change();

            foreach (CheckDiffPromo TempDiffPromo in ListDiffPromo)
            {
                if (TempDiffPromo.FSDiffDur != "")
                {
                    #region ForEach ArrPromoList
                    foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO)
                    {
                        if (TemArrPromoList.FCTYPE == "P")  //宣傳帶
                        {
                            if (TempDiffPromo.fspromo_id == TemArrPromoList.FSPROMO_ID)
                            {
                                TemArrPromoList.FSDURATION = TempDiffPromo.fsduration;
                            }
                            
                        }
                    }
                    #endregion

                }

            }


            ReCalTimecode();
           
        }

        private void buttonProposeDeleteList_Click(object sender, RoutedEventArgs e)
        {

            WSPGMSendSQL.SendSQLSoapClient SP_GET_PROPOSE_DELETE_LIST = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";

            StringBuilder sb = new StringBuilder();

            SP_GET_PROPOSE_DELETE_LIST.Print_Propose_Delete_ListAsync(UserClass.userData.FSUSER_ChtName,_ChannelType);
            //SP_Q_TBPGM_QUEUE.Do_QueryCompleted += new EventHandler<SendSQL.Do_QueryCompletedEventArgs>(SP_Q_TBPGM_QUEUE_Do_QueryCompleted);
            //AAA.Do_InsertAsync("", "", ReturnStr);
            SP_GET_PROPOSE_DELETE_LIST.Print_Propose_Delete_ListCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {
                        if (args.Result.ToString().StartsWith("ERROR:"))
                            MessageBox.Show(args.Result.ToString(), "錯誤訊息", MessageBoxButton.OK);
                        else
                            HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + args.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,toolbar=0,statusbar=0,menubar=0,scrollbar=0,resizable=yes");
                    }
                    else
                        MessageBox.Show("沒有查到建議刪除清單!", "提示訊息", MessageBoxButton.OK);
                }


            };          
        }

       private void buttonAlwaysSendFile_Click(object sender, RoutedEventArgs e)
        {
            if (_ChannelType == "002")
            {
                MessageBox.Show("HD頻道不可派送檔案!");
                return;
            }

           if (dataGridProgList.SelectedIndex >= 0)
            {
                if (PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FNLIVE != "1" && PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSFILE_NO != "")
                {

                    FileStatusList.Clear();

                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("<Datas>");
                    sb.AppendLine("<Data>");
                    if (PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FCTYPE == "G")
                    {
                        foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO)
                        {
                            if (TemArrPromoList.FSPROG_ID == PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSPROG_ID && TemArrPromoList.FNSEQNO == PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FNSEQNO && TemArrPromoList.FCTYPE == "G")
                            {
                                FileStatusList.Add(TemArrPromoList.FSVIDEO_ID + int.Parse(TemArrPromoList.FNBREAK_NO).ToString("00"));
                                sb.AppendLine("<FileStatus>" + TemArrPromoList.FSVIDEO_ID + int.Parse(TemArrPromoList.FNBREAK_NO).ToString("00") + "</FileStatus>");
                            }
                        }
                    }
                    else
                    {
                        FileStatusList.Add(PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSVIDEO_ID);
                        sb.AppendLine("<FileStatus>" + PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSVIDEO_ID + "</FileStatus>");
                    }
                   
                    sb.AppendLine("</Data>");
                    sb.AppendLine("</Datas>");

                    string FIleStatusXML;
                    FIleStatusXML = sb.ToString();
                  
                    WSPGMSendSQL.SendSQLSoapClient GetVideoISStatus = new WSPGMSendSQL.SendSQLSoapClient();

                    System.Collections.ObjectModel.ObservableCollection<WSPGMSendSQL.ArrPromoList> InList = new System.Collections.ObjectModel.ObservableCollection<WSPGMSendSQL.ArrPromoList>();
                    foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO)
                    {
                        if (PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FCTYPE == "G")
                        {
                            if (TemArrPromoList.FSPROG_ID == PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSPROG_ID && TemArrPromoList.FNSEQNO == PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FNSEQNO && TemArrPromoList.FCTYPE == "G")
                            {
                                InList.Add(new WSPGMSendSQL.ArrPromoList()
                                {
                                    COMBINENO = TemArrPromoList.COMBINENO,
                                    FCTIME_TYPE = TemArrPromoList.FCTIME_TYPE,
                                    FSPROG_ID = TemArrPromoList.FSPROG_ID,
                                    FNSEQNO = TemArrPromoList.FNSEQNO,
                                    FSNAME = TemArrPromoList.FSNAME,
                                    FSCHANNEL_ID = TemArrPromoList.FSCHANNEL_ID,
                                    FSCHANNEL_NAME = TemArrPromoList.FSCHANNEL_NAME,
                                    FSPLAY_TIME = TemArrPromoList.FSPLAY_TIME,
                                    FSDURATION = TemArrPromoList.FSDURATION,
                                    FNEPISODE = TemArrPromoList.FNEPISODE,
                                    FNBREAK_NO = TemArrPromoList.FNBREAK_NO,
                                    FNSUB_NO = TemArrPromoList.FNSUB_NO,
                                    FSFILE_NO = TemArrPromoList.FSFILE_NO,
                                    FSVIDEO_ID = TemArrPromoList.FSVIDEO_ID,
                                    FSPROMO_ID = TemArrPromoList.FSPROMO_ID,
                                    FDDATE = TemArrPromoList.FDDATE,
                                    FNLIVE = TemArrPromoList.FNLIVE,
                                    FSSTATUS = TemArrPromoList.FSSTATUS,
                                    FCTYPE = TemArrPromoList.FCTYPE,
                                    FSMEMO = TemArrPromoList.FSMEMO,
                                    FSPROG_NAME = TemArrPromoList.FSPROG_NAME,
                                    FSSIGNAL = TemArrPromoList.FSSIGNAL
                                });
                            }
                        }
                        else
                        {
                            if (TemArrPromoList.FSPROMO_ID == PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSPROMO_ID)
                            {
                                InList.Add(new WSPGMSendSQL.ArrPromoList()
                                {
                                    COMBINENO = TemArrPromoList.COMBINENO,
                                    FCTIME_TYPE = TemArrPromoList.FCTIME_TYPE,
                                    FSPROG_ID = TemArrPromoList.FSPROG_ID,
                                    FNSEQNO = TemArrPromoList.FNSEQNO,
                                    FSNAME = TemArrPromoList.FSNAME,
                                    FSCHANNEL_ID = TemArrPromoList.FSCHANNEL_ID,
                                    FSCHANNEL_NAME = TemArrPromoList.FSCHANNEL_NAME,
                                    FSPLAY_TIME = TemArrPromoList.FSPLAY_TIME,
                                    FSDURATION = TemArrPromoList.FSDURATION,
                                    FNEPISODE = TemArrPromoList.FNEPISODE,
                                    FNBREAK_NO = TemArrPromoList.FNBREAK_NO,
                                    FNSUB_NO = TemArrPromoList.FNSUB_NO,
                                    FSFILE_NO = TemArrPromoList.FSFILE_NO,
                                    FSVIDEO_ID = TemArrPromoList.FSVIDEO_ID,
                                    FSPROMO_ID = TemArrPromoList.FSPROMO_ID,
                                    FDDATE = TemArrPromoList.FDDATE,
                                    FNLIVE = TemArrPromoList.FNLIVE,
                                    FSSTATUS = TemArrPromoList.FSSTATUS,
                                    FCTYPE = TemArrPromoList.FCTYPE,
                                    FSMEMO = TemArrPromoList.FSMEMO,
                                    FSPROG_NAME = TemArrPromoList.FSPROG_NAME,
                                    FSSIGNAL = TemArrPromoList.FSSIGNAL
                                });
                            }                            
                        }
                        

                    }
                    
                    

                    GetVideoISStatus.GetVideoISStatusAsync(FIleStatusXML, InList, true);
                    GetVideoISStatus.GetVideoISStatusCompleted += (s, args) =>
                    {
                        if (args.Error == null)
                        {
                            MessageBox.Show("作業完成");
                        }
                        //先取得檔案狀態再做派送檔案

                        //if (PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FCTYPE == "G")
                        //{
                        //    sb.Clear();
                        
                        //    sb.AppendLine("<Datas>");
                        //    sb.AppendLine("<Data>");
                        //    sb.AppendLine("<FileStatus>" + PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSVIDEO_ID + int.Parse(PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FNBREAK_NO).ToString("00") + "</FileStatus>");
                        //    sb.AppendLine("</Data>");
                        //    sb.AppendLine("</Datas>");
                        //}




                        //WSPGMSendSQL.SendSQLSoapClient SendMovingFile = new WSPGMSendSQL.SendSQLSoapClient();
                        //SendMovingFile.SendMovingFileAsync(sb.ToString());
                        //SendMovingFile.SendMovingFileCompleted += (s1, args1) =>
                        //{
                        //    if (args1.Error == null)
                        //    {
                        //        MessageBox.Show(args1.Result);
                        //    }
                        //};
                        //MessageBox.Show("OK");
                    };
                   
                }
                else
                {
                    MessageBox.Show("此資料無法派送");
                    return;
                }
            }
            else
                MessageBox.Show("請先點選資料");
          

        }

       private void buttonPrint_Combine_Queue_Key_List_Click(object sender, RoutedEventArgs e)
       {
           if (_ChannelID == "")
           {
               MessageBox.Show("請先查詢節目表");
               return;
           }
           if (ModuleClass.getModulePermission("P5" + _ChannelID + "006") == false)
           {
               MessageBox.Show("您沒有轉出此頻道LST檔案的權限");
               return;
           }

           string ErrMessage = "";
           foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO)
           {
               if (TemArrPromoList.FSPROG_ID != "")
               {
                   if (TemArrPromoList.FSSIGNAL == "")
                   {
                       ErrMessage = ErrMessage + "播出時間" + TemArrPromoList.FSPLAY_TIME + " 名稱:" + TemArrPromoList.FSNAME + ":沒有選擇訊號源" + Convert.ToChar(10) + Convert.ToChar(13);

                   }
               }
           }
           if (ErrMessage != "")
           {
               if (MessageBox.Show(ErrMessage + "要繼續嗎?", "提示訊息", MessageBoxButton.OKCancel) == MessageBoxResult.Cancel)
                   return;
           }
           BusyMsg.IsBusy = true;
           WSPGMSendSQL.SendSQLSoapClient TransList = new WSPGMSendSQL.SendSQLSoapClient();
           System.Collections.ObjectModel.ObservableCollection<WSPGMSendSQL.ArrPromoList> InList = new System.Collections.ObjectModel.ObservableCollection<WSPGMSendSQL.ArrPromoList>();

           foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO)
           {
               if (TemArrPromoList.FSPROG_ID != "")
               {

                   InList.Add(new WSPGMSendSQL.ArrPromoList()
                   {
                       COMBINENO = TemArrPromoList.COMBINENO,
                       FCTIME_TYPE = TemArrPromoList.FCTIME_TYPE,
                       FSPROG_ID = TemArrPromoList.FSPROG_ID,
                       FNSEQNO = TemArrPromoList.FNSEQNO,
                       FSNAME = TemArrPromoList.FSNAME,
                       FSCHANNEL_ID = TemArrPromoList.FSCHANNEL_ID,
                       FSCHANNEL_NAME = TemArrPromoList.FSCHANNEL_NAME,
                       FSPLAY_TIME = TemArrPromoList.FSPLAY_TIME,
                       FSDURATION = TemArrPromoList.FSDURATION,
                       FNEPISODE = TemArrPromoList.FNEPISODE,
                       FNBREAK_NO = TemArrPromoList.FNBREAK_NO,
                       FNSUB_NO = TemArrPromoList.FNSUB_NO,
                       FSFILE_NO = TemArrPromoList.FSFILE_NO,
                       FSVIDEO_ID = TemArrPromoList.FSVIDEO_ID,
                       FSPROMO_ID = TemArrPromoList.FSPROMO_ID,
                       FDDATE = TemArrPromoList.FDDATE,
                       FNLIVE = TemArrPromoList.FNLIVE,
                       FSSTATUS = TemArrPromoList.FSSTATUS,
                       FCTYPE = TemArrPromoList.FCTYPE,
                       FSMEMO = TemArrPromoList.FSMEMO,
                       FSPROG_NAME = TemArrPromoList.FSPROG_NAME,
                       FSSIGNAL = TemArrPromoList.FSSIGNAL
                   });
               }
           }

         
            TransList.PrintLstAsync(InList, UserClass.userData.FSUSER_ChtName.ToString());
            TransList.PrintLstCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {
                        WSPGMSendSQL.SendSQLSoapClient GETURL = new WSPGMSendSQL.SendSQLSoapClient();
                        GETURL.GETReportURLAsync("RPT_DATE_LST", args.Result);
                        GETURL.GETReportURLCompleted += (s1, args1) =>
                        {
                            if (args1.Error == null)
                            {
                                if (args1.Result != null && args1.Result != "")
                                {
                                    BusyMsg.IsBusy = false;
                                    if (args1.Result.ToString().StartsWith("錯誤"))
                                        MessageBox.Show(args1.Result.ToString(), "提示訊息", MessageBoxButton.OK);
                                    else
                                        HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + args1.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,toolbar=0,statusbar=0,menubar=0,scrollbar=0");
                                }
                                else
                                {
                                    BusyMsg.IsBusy = false;
                                    MessageBox.Show("啟動報表功能異常，請通知系統管理員", "提示訊息", MessageBoxButton.OK);

                                }

                            }
                        };


                    }
                    else
                    {
                        BusyMsg.IsBusy = false;
                        MessageBox.Show("產生檔案有問題");
                    }
                }
            };
        }
       
       Telerik.Windows.Controls.RadWindow _Wait_Replace_List;
       List<Wait_Replace_List> Now_Wait_Replace_List = new List<Wait_Replace_List>();
       
       private void buttonWaitReplaceList_Click(object sender, RoutedEventArgs e)
       {


           WSPGMSendSQL.SendSQLSoapClient SP_Q_WaitReplaceList = new WSPGMSendSQL.SendSQLSoapClient();

           //string ReturnStr = "";

           StringBuilder sb1 = new StringBuilder();
           object ReturnXML1 = "";
           sb1.AppendLine("<Data>");
           sb1.AppendLine("<FDDATE>" + _Date + "</FDDATE>");
           sb1.AppendLine("<FSCHANNEL_ID>" + _ChannelID + "</FSCHANNEL_ID>");
           sb1.AppendLine("</Data>");
           SP_Q_WaitReplaceList.Do_QueryAsync("[SP_Q_WAIT_REPLACE_LIST]", sb1.ToString());
           SP_Q_WaitReplaceList.Do_QueryCompleted += (s, args) =>
           {
               if (args.Error == null)
               {
                   if (args.Result != null && args.Result != "")
                   {
                       byte[] byteArray2 = Encoding.Unicode.GetBytes(args.Result);
                       StringBuilder output2 = new StringBuilder();
                       // Create an XmlReader
                       Now_Wait_Replace_List.Clear();
                       XDocument doc = XDocument.Load(new MemoryStream(byteArray2));
                       var ltox = from str in doc.Elements("Datas").Elements("Data")
                                  select str;
                       foreach (XElement elem in ltox)
                       {
                        
                            string RFSPROG_ID =XElementToString(elem.Element("FSPROG_ID"));
                            string RFNEPISODE =XElementToString(elem.Element("FNEPISODE"));
                            string RFSPROG_NAME =XElementToString(elem.Element("FSPROG_NAME"));
                            string RFSFILE_NO =XElementToString(elem.Element("FSFILE_NO"));
                            string RFSVIDEO_ID =XElementToString(elem.Element("FSVIDEO_ID"));
                           string RFSBRO_ID =XElementToString(elem.Element("FSBRO_ID"));
                           string RFCCHECK_STATUS =XElementToString(elem.Element("FCCHECK_STATUS"));
                           string ReplaceStatus;
                           if (RFCCHECK_STATUS == "N")
                               ReplaceStatus = "已填單";
                           else if (RFCCHECK_STATUS == "Y")
                               ReplaceStatus = "已審核";
                           else if (RFCCHECK_STATUS == "I")
                               ReplaceStatus = "已轉檔";
                           else if (RFCCHECK_STATUS == "F")
                               ReplaceStatus = "已被退回";
                           else if (RFCCHECK_STATUS == "G")
                               ReplaceStatus = "已線上簽收";
                           else if (RFCCHECK_STATUS == "C")
                               ReplaceStatus = "置換";
                           else if (RFCCHECK_STATUS == "X")
                               ReplaceStatus = "抽單";
                           else if (RFCCHECK_STATUS == "T")
                               ReplaceStatus = "影帶回溯";
                           else
                               ReplaceStatus = "";

                           if (RFCCHECK_STATUS != "F" && RFCCHECK_STATUS != "C" && RFCCHECK_STATUS != "X")
                           {
                               Now_Wait_Replace_List.Add(new Wait_Replace_List()
                               {
                                   FSPROG_ID = RFSPROG_ID,
                                   FNEPISODE = RFNEPISODE,
                                   FSPROG_NAME = RFSPROG_NAME,
                                   FSFILE_NO = RFSFILE_NO,
                                   FSVIDEO_ID = RFSVIDEO_ID,
                                   FSBRO_ID = RFSBRO_ID,
                                   FCCHECK_STATUS = ReplaceStatus


                               });
                           }
                          
                           
                       }
                       if (_Wait_Replace_List == null)
                       {
                           _Wait_Replace_List = new Telerik.Windows.Controls.RadWindow();
                           _Wait_Replace_List.Header = "待置換清單";
                           _Wait_Replace_List.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
                           _Wait_Replace_List.TopOffset = -100;
                           _Wait_Replace_List.Width = 600;
                           _Wait_Replace_List.Height = 400;


                           DataGrid D1 = new DataGrid();

                           _Wait_Replace_List.Content = D1;
                           StyleManager.SetTheme(_Wait_Replace_List, new Office_SilverTheme());

                           DataGridTextColumn textColumn = new DataGridTextColumn();
                           textColumn.Header = "節目編號";
                           textColumn.Binding = new Binding("FSPROG_ID");
                       

                           DataGridTextColumn textColumn1 = new DataGridTextColumn();
                           textColumn1.Header = "集數";
                           textColumn1.Binding = new Binding("FNEPISODE");
                           //textColumn1.Visibility = Visibility.Collapsed;

                           DataGridTextColumn textColumn2 = new DataGridTextColumn();
                           textColumn2.Header = "節目名稱";
                           textColumn2.Binding = new Binding("FSPROG_NAME");
                           
                           DataGridTextColumn textColumn3 = new DataGridTextColumn();
                           textColumn3.Header = "檔名";
                           textColumn3.Binding = new Binding("FSFILE_NO");

                           DataGridTextColumn textColumn4 = new DataGridTextColumn();
                           textColumn4.Header = "主控編號";
                           textColumn4.Binding = new Binding("FSVIDEO_ID");

                           DataGridTextColumn textColumn5 = new DataGridTextColumn();
                           textColumn5.Header = "置換單號";
                           textColumn5.Binding = new Binding("FSBRO_ID");

                           DataGridTextColumn textColumn6 = new DataGridTextColumn();
                           textColumn6.Header = "狀態";
                           textColumn6.Binding = new Binding("FCCHECK_STATUS");

                           D1.Columns.Add(textColumn);
                           D1.Columns.Add(textColumn1);
                           D1.Columns.Add(textColumn2);
                           D1.Columns.Add(textColumn3);
                           D1.Columns.Add(textColumn4);
                           D1.Columns.Add(textColumn5);
                           D1.Columns.Add(textColumn6);
                           D1.AutoGenerateColumns = false;
                           D1.ItemsSource = null;
                           D1.ItemsSource = Now_Wait_Replace_List;

                           _Wait_Replace_List.Show();

                       }
                       else
                       {
                           _Wait_Replace_List = new Telerik.Windows.Controls.RadWindow();
                           _Wait_Replace_List.Header = "待置換清單";
                           _Wait_Replace_List.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
                           _Wait_Replace_List.TopOffset = -100;
                           _Wait_Replace_List.Width = 300;
                           _Wait_Replace_List.Height = 300;


                           DataGrid D1 = new DataGrid();

                           _Wait_Replace_List.Content = D1;
                           StyleManager.SetTheme(_Wait_Replace_List, new Office_SilverTheme());

                           DataGridTextColumn textColumn = new DataGridTextColumn();
                           textColumn.Header = "節目編號";
                           textColumn.Binding = new Binding("FSPROG_ID");


                           DataGridTextColumn textColumn1 = new DataGridTextColumn();
                           textColumn1.Header = "集數";
                           textColumn1.Binding = new Binding("FNEPISODE");
                           //textColumn1.Visibility = Visibility.Collapsed;

                           DataGridTextColumn textColumn2 = new DataGridTextColumn();
                           textColumn2.Header = "節目名稱";
                           textColumn2.Binding = new Binding("FSPROG_NAME");

                           DataGridTextColumn textColumn3 = new DataGridTextColumn();
                           textColumn3.Header = "檔名";
                           textColumn3.Binding = new Binding("FSFILE_NO");

                           DataGridTextColumn textColumn4 = new DataGridTextColumn();
                           textColumn4.Header = "主控編號";
                           textColumn4.Binding = new Binding("FSVIDEO_ID");

                           DataGridTextColumn textColumn5 = new DataGridTextColumn();
                           textColumn5.Header = "置換單號";
                           textColumn5.Binding = new Binding("FSBRO_ID");

                           DataGridTextColumn textColumn6 = new DataGridTextColumn();
                           textColumn6.Header = "狀態";
                           textColumn6.Binding = new Binding("FCCHECK_STATUS");

                           D1.Columns.Add(textColumn);
                           D1.Columns.Add(textColumn1);
                           D1.Columns.Add(textColumn2);
                           D1.Columns.Add(textColumn3);
                           D1.Columns.Add(textColumn4);
                           D1.Columns.Add(textColumn5);
                           D1.Columns.Add(textColumn6);
                           D1.AutoGenerateColumns = false;
                           D1.ItemsSource = null;
                           D1.ItemsSource = Now_Wait_Replace_List;

                           _Wait_Replace_List.Show();
                       }
                       
                   }
                   else
                   {
                       MessageBox.Show("沒有查到資料");
                   }
               }

           };
          
       }

       private void buttonCopyPlayList_Click(object sender, RoutedEventArgs e)
       {
           if (comboBoxChannel.SelectedIndex == -1)
           {
               MessageBox.Show("請先選擇要來源頻道");
               return;
           }
           BusyMsg.IsBusy = true;
           labelInsPromoID.Content = "";
           WSPGMSendSQL.SendSQLSoapClient SP_Copy_Play_List = new WSPGMSendSQL.SendSQLSoapClient();
           //string ReturnStr = "";

           //StringBuilder sb = new StringBuilder();
           object ReturnXML = "";
           //sb.AppendLine("<Data>");
           //sb.AppendLine("<FDDATE>" + this.textBoxDate.Text + "</FDDATE>");
           if (comboBoxChannel.SelectedIndex != -1)
           {
               string[] ChannelType = ((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString().Split(new Char[] { '-' });
               _ChannelType = ChannelType[1];
               _ChannelID = ChannelType[0];
               _ChannelName = ((ComboBoxItem)comboBoxChannel.SelectedItem).Content.ToString();
               _Date = this.DatePickerDate.Text;
           }
           //_Date = "2010/12/16";
           //_ChannelID = "06";
           //sb.AppendLine("<FDDATE>2010/12/16</FDDATE>");
           //sb.AppendLine("<FSCHANNEL_ID>06</FSCHANNEL_ID>");
           //sb.AppendLine("</Data>");
           SP_Copy_Play_List.Do_Copy_Play_ListAsync(_Date, _ChannelID, ReturnXML);
           //SP_Q_TBPGM_QUEUE.Do_QueryCompleted += new EventHandler<SendSQL.Do_QueryCompletedEventArgs>(SP_Q_TBPGM_QUEUE_Do_QueryCompleted);
           //AAA.Do_InsertAsync("", "", ReturnStr);
           SP_Copy_Play_List.Do_Copy_Play_ListCompleted += (s, args) =>
           {
               if (args.Error == null)
               {
                   if (args.Result == "")
                   {
                       BusyMsg.IsBusy = false;
                       MessageBox.Show("拷貝節目表完成");
                   }
                   else
                   {
                       BusyMsg.IsBusy = false;
                       MessageBox.Show("訊息:" + args.Result);
                   }
               }
           };
       }

       bool ReloadMessage = true;
       private void buttonInsertProg_Click(object sender, RoutedEventArgs e)
       {
           if (_Date =="")
           {
               MessageBox.Show("請先查詢節目表");
               return;
           }
           if (_ChannelID == "")
           {
               MessageBox.Show("請先查詢節目表");
               return;
           }

           if (myDispatcherTimer.IsEnabled == true)
           {
               MessageBox.Show("您需要先儲存播出運行表!");
               return;
           }

           if (ModuleClass.getModulePermission("P1" + _ChannelID + "002") == false)
           {
               MessageBox.Show("您沒有於此頻道插入節目的權限");
               return;
           }
           PGM100_07 PGMQUEUEINSERT_Frm = new PGM100_07();
           PGMQUEUEINSERT_Frm._date = _Date;
           PGMQUEUEINSERT_Frm._channelid = _ChannelID;
           PGMQUEUEINSERT_Frm._ChannelFileType = _ChannelType;
           PGMQUEUEINSERT_Frm._channelName = _ChannelName;
           PGMQUEUEINSERT_Frm._FromForm = "COMBINEQUEUE";
           PGMQUEUEINSERT_Frm.Show();
           PGMQUEUEINSERT_Frm.Closed += (s, args) =>
           {
               if (PGMQUEUEINSERT_Frm._PgmQueueProg != null)
               {
                   if (PGMQUEUEINSERT_Frm._HasInsert == true)
                   {
                       ReloadMessage = false;
                       ButtonReLoad_Click(sender, e);
                       if (myDispatcherTimer.IsEnabled == false)
                           myDispatcherTimer.Start();
                   }
               }
           };
       }

       private void buttonDeleteProg_Click(object sender, RoutedEventArgs e)
       {
           if (_Date == "")
           {
               MessageBox.Show("請先查詢節目表");
               return;
           }
           if (_ChannelID == "")
           {
               MessageBox.Show("請先查詢節目表");
               return;
           }

           if (myDispatcherTimer.IsEnabled == true)
           {
               MessageBox.Show("您需要先儲存播出運行表!");
               return;
           }

           if (ModuleClass.getModulePermission("P1" + _ChannelID + "002") == false)
           {
               MessageBox.Show("您沒有刪除此頻道節目的權限");
               return;
           }

           if (dataGridProgList.SelectedIndex < 0)
           {
               MessageBox.Show("必須先選擇資料");
               return;
           }

           string progname="";
           
           if (PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FCTYPE != "P") 
           {
               for (int i = 0; i <= PGM_ARR_PROMO.Count - 1; i++)
               {
                   if ((PGM_ARR_PROMO[i].FCTYPE=="G") && (PGM_ARR_PROMO[i].FSPROG_ID == PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSPROG_ID) && (PGM_ARR_PROMO[i].FNSEQNO == PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FNSEQNO) && (PGM_ARR_PROMO[i].FNBREAK_NO == "1"))
                   {
                       progname = PGM_ARR_PROMO[i].FSPROG_NAME;
                   }
               }
               
           }
           else
             progname = PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSPROG_NAME;

           if (MessageBox.Show("您卻定要將節目[" + progname + "]自節目表移除?", "刪除節目", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
           {
               string DelProgID = PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSPROG_ID;
               string DelProgSeqno = PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FNSEQNO;
               //先刪除TBPGM_QUEUE中的節目資料
               WSPGMSendSQL.SendSQLSoapClient Delete_Prog = new WSPGMSendSQL.SendSQLSoapClient();
               Delete_Prog.Delete_Queue_ProgAsync(_ChannelID,_Date,DelProgID,DelProgSeqno,PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FNEPISODE);
               Delete_Prog.Delete_Queue_ProgCompleted += (s, args) =>
               {
                   if (args.Error == null)
                   {
                       if (args.Result == "OK")
                       {
                           for (int i = PGM_ARR_PROMO.Count - 1; i >= 0; i--)
                           {
                               if ((PGM_ARR_PROMO[i].FSPROG_ID == DelProgID) && (PGM_ARR_PROMO[i].FNSEQNO == DelProgSeqno))
                               {
                                   PGM_ARR_PROMO.RemoveAt(i);

                                   if (PGM_ARR_PROMO[i - 1].FCTYPE == "")
                                       PGM_ARR_PROMO.RemoveAt(i - 1);

                               }
                           }
                           dataGridProgList.ItemsSource = null;
                           dataGridProgList.ItemsSource = PGM_ARR_PROMO;

                           if (myDispatcherTimer.IsEnabled == false)
                               myDispatcherTimer.Start();

                           MessageBox.Show("刪除完成");

                         
                           return;
                       }
                       else
                           MessageBox.Show(args.Result);
                   }
                   else
                       MessageBox.Show("刪除節目表的節目失敗");

               };     
              
           }

          
          
       }


       List<ReplaceProg> ReplaceProgList = new List<ReplaceProg>();
       private void buttonReplaceProg_Click(object sender, RoutedEventArgs e)
       {
           if (_Date == "")
           {
               MessageBox.Show("請先查詢節目表");
               return;
           }
           if (_ChannelID == "")
           {
               MessageBox.Show("請先查詢節目表");
               return;
           }

           if (myDispatcherTimer.IsEnabled == true)
           {
               MessageBox.Show("您需要先儲存播出運行表!");
               return;
           }

           if (ModuleClass.getModulePermission("P1" + _ChannelID + "002") == false)
           {
               MessageBox.Show("您沒有於此頻道置換節目的權限");
               return;
           }
           if (dataGridProgList.SelectedIndex <= 0)
           {
               MessageBox.Show("請先選擇要置換的節目");
               return;
           }
           ReplaceProgList.Clear();
           string ReplaceProgID = PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FSPROG_ID;
           string ReplaceProgSeqno = PGM_ARR_PROMO[dataGridProgList.SelectedIndex].FNSEQNO;
          
           #region  將要置換的節目打包傳送至PGM125_05
           for (int i = 0; i <= PGM_ARR_PROMO.Count - 1; i++)
           {
               if ((PGM_ARR_PROMO[i].FSPROG_ID == ReplaceProgID) && (PGM_ARR_PROMO[i].FNSEQNO == ReplaceProgSeqno) && (PGM_ARR_PROMO[i].FCTYPE=="G"))
               {
                   ReplaceProg ReplaceProgData = new ReplaceProg();
                   ReplaceProgData.SFSPROG_ID= PGM_ARR_PROMO[i].FSPROG_ID;
                   ReplaceProgData.SFNSEQNO=PGM_ARR_PROMO[i].FNSEQNO;
                   ReplaceProgData.SFSPROG_NAME = PGM_ARR_PROMO[i].FSPROG_NAME;
                   ReplaceProgData.SFNBREAK_NO = PGM_ARR_PROMO[i].FNBREAK_NO;
                   ReplaceProgData.SFSPLAY_TIME = PGM_ARR_PROMO[i].FSPLAY_TIME;
                   ReplaceProgData.SFSDURATION = PGM_ARR_PROMO[i].FSDURATION;
                   ReplaceProgData.SFNEPISODE = PGM_ARR_PROMO[i].FNEPISODE;
                   ReplaceProgData.DFSPROG_ID = "";
                   ReplaceProgData.DFNSEQNO = "";
                   ReplaceProgData.DFSPROG_NAME = "";
                   ReplaceProgData.DFNBREAK_NO = "";
                   ReplaceProgData.DFSDURATION = "";
                   ReplaceProgData.DFNEPISODE = "";
                   ReplaceProgList.Add(ReplaceProgData);
               }
           }
           #endregion  將要置換的節目打包傳送至PGM125_05
           PGM125_05 PGMQUEUEREPLACE_Frm = new PGM125_05();
           PGMQUEUEREPLACE_Frm._date = _Date;
           PGMQUEUEREPLACE_Frm._channelid = _ChannelID;
           PGMQUEUEREPLACE_Frm._ChannelFileType = _ChannelType;
           PGMQUEUEREPLACE_Frm._channelName = _ChannelName;
           PGMQUEUEREPLACE_Frm.ReplaceProgList = ReplaceProgList;
           PGMQUEUEREPLACE_Frm._FromForm = "COMBINEQUEUE";
           #region 查詢要置換的節目帶入預設值
           PGMQUEUEREPLACE_Frm.QueryDefProg(ReplaceProgID, ReplaceProgSeqno, _Date, _ChannelID);

           #endregion
           PGMQUEUEREPLACE_Frm.dataGridReplaceProgList.ItemsSource = null;
           PGMQUEUEREPLACE_Frm.dataGridReplaceProgList.ItemsSource = PGMQUEUEREPLACE_Frm.ReplaceProgList;
           PGMQUEUEREPLACE_Frm.Show();
           BusyMsg.IsBusy = true;
           #region 開始處理回傳資料
           PGMQUEUEREPLACE_Frm.Closed += (s, args) =>
           {
               if (PGMQUEUEREPLACE_Frm.ReplaceProgList[0].DFSPROG_ID != "")
               {
                   if (PGMQUEUEREPLACE_Frm._HasReplace == true)
                   {
                       int maxDSEG = 0;
                       int maxSSEG = 0;
                       int NowSub = 1;
                       string RProg_id = "";
                       string RSeqno = "";

                       for (int j = 0; j <= PGMQUEUEREPLACE_Frm.ReplaceProgList.Count - 1; j++)
                       {
                           if (PGMQUEUEREPLACE_Frm.ReplaceProgList[j].DFNBREAK_NO != "")
                               maxDSEG = int.Parse(PGMQUEUEREPLACE_Frm.ReplaceProgList[j].DFNBREAK_NO);
                           if (PGMQUEUEREPLACE_Frm.ReplaceProgList[j].SFNBREAK_NO != "")
                               maxSSEG = int.Parse(PGMQUEUEREPLACE_Frm.ReplaceProgList[j].SFNBREAK_NO);
                           if (PGMQUEUEREPLACE_Frm.ReplaceProgList[j].DFSPROG_ID != "")
                               RProg_id = PGMQUEUEREPLACE_Frm.ReplaceProgList[j].DFSPROG_ID;
                           if (PGMQUEUEREPLACE_Frm.ReplaceProgList[j].DFSPROG_ID != "")
                               RSeqno = PGMQUEUEREPLACE_Frm.ReplaceProgList[j].DFSPROG_ID;
                       }
                       bool Flag = false;
                       string nowChannelID = "";
                       string nowChannelName = "";
                       string nowPlayTime = "";
                       string nowDate = "";
                       int StartRow = 0;
                       int EndRow = 0;
                       #region 開始取得要置換的節目起訖位置
                       for (int i = 0; i <= PGM_ARR_PROMO.Count - 1; i++)
                       {
                           if (PGM_ARR_PROMO[i].FSPROG_ID == PGMQUEUEREPLACE_Frm.ReplaceProgList[0].SFSPROG_ID && PGM_ARR_PROMO[i].FNSEQNO == PGMQUEUEREPLACE_Frm.ReplaceProgList[0].SFNSEQNO && Flag == false)
                           {
                               Flag = true; //找到第一筆
                               StartRow = i;
                               nowChannelID = PGM_ARR_PROMO[i].FSCHANNEL_ID;
                               nowChannelName = PGM_ARR_PROMO[i].FSCHANNEL_NAME;
                               nowPlayTime = PGM_ARR_PROMO[i].FSPLAY_TIME;
                               nowDate = PGM_ARR_PROMO[i].FDDATE;


                           }
                           if (Flag == true)
                           {
                               if ((PGM_ARR_PROMO[i].FSPROG_ID != PGMQUEUEREPLACE_Frm.ReplaceProgList[0].SFSPROG_ID || PGM_ARR_PROMO[i].FNSEQNO != PGMQUEUEREPLACE_Frm.ReplaceProgList[0].SFNSEQNO) && (EndRow == 0))
                                   EndRow = i;
                           }

                       }
                       #endregion
                       #region 開始取得要置換的節目起訖位置
                       for (int i = 0; i <= PGM_ARR_PROMO.Count - 1; i++)
                       {
                           if (PGM_ARR_PROMO[i].FSPROG_ID == PGMQUEUEREPLACE_Frm.ReplaceProgList[0].SFSPROG_ID && PGM_ARR_PROMO[i].FNSEQNO == PGMQUEUEREPLACE_Frm.ReplaceProgList[0].SFNSEQNO && Flag == false)
                           {
                               Flag = true; //找到第一筆
                               StartRow = i;
                               nowChannelID = PGM_ARR_PROMO[i].FSCHANNEL_ID;
                               nowChannelName = PGM_ARR_PROMO[i].FSCHANNEL_NAME;
                               nowPlayTime = PGM_ARR_PROMO[i].FSPLAY_TIME;
                               nowDate = PGM_ARR_PROMO[i].FDDATE;


                           }
                           if (Flag == true)
                           {
                               if ((PGM_ARR_PROMO[i].FSPROG_ID != PGMQUEUEREPLACE_Frm.ReplaceProgList[0].SFSPROG_ID || PGM_ARR_PROMO[i].FNSEQNO != PGMQUEUEREPLACE_Frm.ReplaceProgList[0].SFNSEQNO) && (EndRow == 0))
                                   EndRow = i;
                           }

                       }
                       #endregion
                       if (EndRow == 0)
                           EndRow = PGM_ARR_PROMO.Count - 1;
                       #region 置換目前的節目與Promo
                       for (int j = StartRow; j <= EndRow; j++)
                       {
                           if (PGM_ARR_PROMO[j].FCTYPE == "G")
                           {
                               //檢查段數是否大於目的節目的最大段數,如果大於則要刪除此節目段(先不處理最後再刪除),如果小於等於則要將其節目改為目的節目
                               if (int.Parse(PGM_ARR_PROMO[j].FNBREAK_NO) <= maxDSEG)
                               {
                                   PGM_ARR_PROMO[j].COMBINENO = "";
                                   PGM_ARR_PROMO[j].FCTIME_TYPE = "A";
                                   PGM_ARR_PROMO[j].FSPROG_ID = PGMQUEUEREPLACE_Frm.ReplaceProgList[int.Parse(PGM_ARR_PROMO[j].FNBREAK_NO) - 1].DFSPROG_ID;
                                   PGM_ARR_PROMO[j].FNSEQNO = PGMQUEUEREPLACE_Frm.ReplaceProgList[int.Parse(PGM_ARR_PROMO[j].FNBREAK_NO) - 1].DFNSEQNO;
                                   PGM_ARR_PROMO[j].FSNAME = PGMQUEUEREPLACE_Frm.ReplaceProgList[int.Parse(PGM_ARR_PROMO[j].FNBREAK_NO) - 1].DFSPROG_NAME;
                                   PGM_ARR_PROMO[j].FSCHANNEL_ID = nowChannelID;
                                   PGM_ARR_PROMO[j].FSCHANNEL_NAME = nowChannelName;
                                   PGM_ARR_PROMO[j].FSPLAY_TIME = nowPlayTime;
                                   PGM_ARR_PROMO[j].FSDURATION = PGMQUEUEREPLACE_Frm.ReplaceProgList[int.Parse(PGM_ARR_PROMO[j].FNBREAK_NO) - 1].DFSDURATION;
                                   PGM_ARR_PROMO[j].FNEPISODE = PGMQUEUEREPLACE_Frm.ReplaceProgList[int.Parse(PGM_ARR_PROMO[j].FNBREAK_NO) - 1].DFNEPISODE;
                                   PGM_ARR_PROMO[j].FNBREAK_NO = PGMQUEUEREPLACE_Frm.ReplaceProgList[int.Parse(PGM_ARR_PROMO[j].FNBREAK_NO) - 1].DFNBREAK_NO;
                                   PGM_ARR_PROMO[j].FCTYPE = "G";
                                   PGM_ARR_PROMO[j].FNSUB_NO = "";
                                   PGM_ARR_PROMO[j].FSFILE_NO = PGMQUEUEREPLACE_Frm.ReplaceProgList[int.Parse(PGM_ARR_PROMO[j].FNBREAK_NO) - 1].FSFILE_NO;
                                   PGM_ARR_PROMO[j].FSVIDEO_ID = PGMQUEUEREPLACE_Frm.ReplaceProgList[int.Parse(PGM_ARR_PROMO[j].FNBREAK_NO) - 1].FSVIDEO_ID;
                                   PGM_ARR_PROMO[j].FSPROMO_ID = "";
                                   PGM_ARR_PROMO[j].FDDATE = nowDate;
                                   PGM_ARR_PROMO[j].FNLIVE = PGMQUEUEREPLACE_Frm.ReplaceProgList[int.Parse(PGM_ARR_PROMO[j].FNBREAK_NO) - 1].FNLIVE;

                                   PGM_ARR_PROMO[j].FSMEMO = PGMQUEUEREPLACE_Frm.ReplaceProgList[int.Parse(PGM_ARR_PROMO[j].FNBREAK_NO) - 1].FSMEMO;
                                   PGM_ARR_PROMO[j].FSMEMO1 = PGMQUEUEREPLACE_Frm.ReplaceProgList[int.Parse(PGM_ARR_PROMO[j].FNBREAK_NO) - 1].FSMEMO1;
                                   PGM_ARR_PROMO[j].FSSIGNAL = PGMQUEUEREPLACE_Frm.ReplaceProgList[int.Parse(PGM_ARR_PROMO[j].FNBREAK_NO) - 1].FSSIGNAL;
                                   PGM_ARR_PROMO[j].FSPROG_NAME = PGMQUEUEREPLACE_Frm.ReplaceProgList[int.Parse(PGM_ARR_PROMO[j].FNBREAK_NO) - 1].DFSPROG_NAME;
                               }
                           }
                           else if (PGM_ARR_PROMO[j].FCTYPE == "P")
                           {
                               if (int.Parse(PGM_ARR_PROMO[j].FNBREAK_NO) <= maxDSEG) //如果小於目的節目段數則只需修改屬於哪個節目
                               {
                                   PGM_ARR_PROMO[j].FSPROG_ID = PGMQUEUEREPLACE_Frm.ReplaceProgList[0].DFSPROG_ID;
                                   PGM_ARR_PROMO[j].FNSEQNO = PGMQUEUEREPLACE_Frm.ReplaceProgList[0].DFNSEQNO;
                                   if (int.Parse(PGM_ARR_PROMO[j].FNBREAK_NO) == maxDSEG)
                                   {
                                       //取得目前的支數
                                       NowSub = int.Parse(PGM_ARR_PROMO[j].FNSUB_NO);
                                   }
                               }
                               else
                               {
                                   PGM_ARR_PROMO[j].FSPROG_ID = PGMQUEUEREPLACE_Frm.ReplaceProgList[0].DFSPROG_ID;
                                   PGM_ARR_PROMO[j].FNSEQNO = PGMQUEUEREPLACE_Frm.ReplaceProgList[0].DFNSEQNO;
                                   PGM_ARR_PROMO[j].FNBREAK_NO = maxDSEG.ToString();
                                   NowSub = NowSub + 1;
                                   PGM_ARR_PROMO[j].FNSUB_NO = NowSub.ToString();
                               }
                           }

                       } // for (int j=StartRow ; j <= EndRow; j++)
                       #endregion
                       #region 開始將大於來源節目段的插入節目表
                       for (int j = maxSSEG; j <= maxDSEG - 1; j++)
                       {
                           ArrPromoList Temp_ArrPromo = new ArrPromoList();
                           Temp_ArrPromo.COMBINENO = "";
                           Temp_ArrPromo.FCTIME_TYPE = "A";
                           Temp_ArrPromo.FSPROG_ID = PGMQUEUEREPLACE_Frm.ReplaceProgList[j].DFSPROG_ID;
                           Temp_ArrPromo.FNSEQNO = PGMQUEUEREPLACE_Frm.ReplaceProgList[j].DFNSEQNO;
                           Temp_ArrPromo.FSNAME = PGMQUEUEREPLACE_Frm.ReplaceProgList[j].DFSPROG_NAME;
                           Temp_ArrPromo.FSCHANNEL_ID = nowChannelID;
                           Temp_ArrPromo.FSCHANNEL_NAME = nowChannelName;
                           Temp_ArrPromo.FSPLAY_TIME = nowPlayTime;
                           Temp_ArrPromo.FSDURATION = PGMQUEUEREPLACE_Frm.ReplaceProgList[j].DFSDURATION;
                           Temp_ArrPromo.FNEPISODE = PGMQUEUEREPLACE_Frm.ReplaceProgList[j].DFNEPISODE;
                           Temp_ArrPromo.FNBREAK_NO = PGMQUEUEREPLACE_Frm.ReplaceProgList[j].DFNBREAK_NO;
                           Temp_ArrPromo.FCTYPE = "G";
                           Temp_ArrPromo.FNSUB_NO = "";
                           Temp_ArrPromo.FSFILE_NO = PGMQUEUEREPLACE_Frm.ReplaceProgList[j].FSFILE_NO;
                           Temp_ArrPromo.FSVIDEO_ID = PGMQUEUEREPLACE_Frm.ReplaceProgList[j].FSVIDEO_ID;
                           Temp_ArrPromo.FSPROMO_ID = "";
                           Temp_ArrPromo.FDDATE = nowDate;
                           Temp_ArrPromo.FNLIVE = PGMQUEUEREPLACE_Frm.ReplaceProgList[j].FNLIVE;

                           Temp_ArrPromo.FSMEMO = PGMQUEUEREPLACE_Frm.ReplaceProgList[j].FSMEMO;
                           Temp_ArrPromo.FSMEMO1 = PGMQUEUEREPLACE_Frm.ReplaceProgList[j].FSMEMO1;
                           Temp_ArrPromo.FSSIGNAL = PGMQUEUEREPLACE_Frm.ReplaceProgList[j].FSSIGNAL;
                           Temp_ArrPromo.FSPROG_NAME = PGMQUEUEREPLACE_Frm.ReplaceProgList[j].DFSPROG_NAME;
                           PGM_ARR_PROMO.Insert(EndRow, Temp_ArrPromo);
                           EndRow = EndRow + 1;
                       }
                       #endregion
                       #region 開始刪除大於目的節目段數的節目
                       for (int i = PGM_ARR_PROMO.Count - 1; i >= 0; i--)
                       {
                           if ((PGM_ARR_PROMO[i].FSPROG_ID == PGMQUEUEREPLACE_Frm.ReplaceProgList[0].SFSPROG_ID) && (PGM_ARR_PROMO[i].FNSEQNO == PGMQUEUEREPLACE_Frm.ReplaceProgList[0].SFNSEQNO) && (int.Parse(PGM_ARR_PROMO[i].FNBREAK_NO) > maxDSEG))
                               PGM_ARR_PROMO.RemoveAt(i);

                       }
                       #endregion

                       ReCalTimecode();
                       dataGridProgList.ItemsSource = null;
                       dataGridProgList.ItemsSource = PGM_ARR_PROMO;
                       if (myDispatcherTimer.IsEnabled == false)
                           myDispatcherTimer.Start();
                   }
               } //if (PGMQUEUEREPLACE_Frm.ReplaceProgList[0].DFSPROG_ID != "")
           }; //PGMQUEUEREPLACE_Frm.Closed += (s, args)
           #endregion 開始處理回傳資料

           
          
           BusyMsg.IsBusy = false;


       }

       private void buttonCheckPromoZone_Click(object sender, RoutedEventArgs e)
       {

           if (PGM_ARR_PROMO.Count <= 0)
           {
               return;
           }
          
           StringBuilder sb = new StringBuilder();
           sb.AppendLine("<Datas>");
           for (int i = 0; i <= PGM_ARR_PROMO.Count - 1; i++)
           {
               if (PGM_ARR_PROMO[i].FCTYPE == "P")  //是宣傳帶就要加入
               {
                   sb.AppendLine("<Data>");
                   sb.AppendLine("<FSPROMO_ID>" + PGM_ARR_PROMO[i].FSPROMO_ID + "</FSPROMO_ID>");
                   sb.AppendLine("<FSPROMO_NAME>" + TransferTimecode.ReplaceXML(PGM_ARR_PROMO[i].FSNAME) + "</FSPROMO_NAME>");
                   sb.AppendLine("<FSPLAY_TIME>" + PGM_ARR_PROMO[i].RealFSPLAY_TIME + "</FSPLAY_TIME>");
                   sb.AppendLine("<FSCHANNEL_ID>" + PGM_ARR_PROMO[i].FSCHANNEL_ID + "</FSCHANNEL_ID>");
                   sb.AppendLine("<FDDATE>" + PGM_ARR_PROMO[i].FDDATE + "</FDDATE>");
                   sb.AppendLine("</Data>");
               }
           }
           sb.AppendLine("</Datas>");

           if (sb.ToString() == "" || sb.ToString() == "<Datas></Datas>")
           {
               MessageBox.Show("沒有需要檢查的短帶");
               return;
           }
           BusyMsg.IsBusy = true;
           WSPGMSendSQL.SendSQLSoapClient Do_Check_Promo_Zone = new WSPGMSendSQL.SendSQLSoapClient();
           Do_Check_Promo_Zone.Do_Check_Promo_ZoneAsync(null, sb.ToString());
           Do_Check_Promo_Zone.Do_Check_Promo_ZoneCompleted += (s, args) =>
           {
               if (args.Error == null)
               {
                   if (args.Result != null && args.Result != "")
                   {
                       MessageBox.Show(args.Result);
                   }
                   else
                       MessageBox.Show("沒有檢查到不在有效區間的短帶");
               }
               else
                   MessageBox.Show("檢查發生錯誤");
           };
           BusyMsg.IsBusy = false;
       }

       public string TransChannelIDToAliasName(string FSCHANNEL_ID)
       {
           string ChannelAliasName = "";
           if (FSCHANNEL_ID == "01") //公視主頻
               ChannelAliasName = "P";
           else if (FSCHANNEL_ID == "02") //DImo
               ChannelAliasName = "D";
           else if (FSCHANNEL_ID == "07") //客家
               ChannelAliasName = "H";
           else if (FSCHANNEL_ID == "08") //宏觀
               ChannelAliasName = "M";
           else if (FSCHANNEL_ID == "09") //原民
               ChannelAliasName = "I";
           else if (FSCHANNEL_ID == "11") //HD頻道
               ChannelAliasName = "N";
           else if (FSCHANNEL_ID == "51") //主頻MOD
               ChannelAliasName = "O";
           else if (FSCHANNEL_ID == "61") //HD MOD
               ChannelAliasName = "A";
           return ChannelAliasName;
       }

       private void buttonExport1_Click(object sender, RoutedEventArgs e)
       {
           //轉出電子檔轉檔清單

           if (_ChannelID == "")
           {
               MessageBox.Show("請先查詢節目表");
               return;
           }
           if (ModuleClass.getModulePermission("P5" + _ChannelID + "006") == false)
           {
               MessageBox.Show("您沒有轉出此頻道檔案的權限");
               return;
           }

           SaveFileDialog dialog = new SaveFileDialog();
           //dialog.Filter = "lst File" + "|*.lst";
           dialog.Filter = "Zip File" + "|*.Zip";
           dialog.FilterIndex = 1;
           dialog.ShowDialog();
           //檢查資料,如果有沒有輸入訊號的則提出警示

           string ErrMessage = "";
          
           BusyMsg.IsBusy = true;
           WSPGMSendSQL.SendSQLSoapClient ExportFilingList = new WSPGMSendSQL.SendSQLSoapClient();

           ExportFilingList.ExportFilingListAsync(_Date, _ChannelID);
           ExportFilingList.ExportFilingListCompleted += (s, args) =>
           {
               if (args.Error == null)
               {
                   if (args.Result != null && args.Result != "")
                   {


                       //args.Result	"http://localhost/PTS_MAM3.Web/UploadFolder/0120110410.lst"	string


                       WCF100DL.WCF100Client WCF100client = new WCF100DL.WCF100Client();

                       string FilingFileID = String.Format("{0:00}", Convert.ToDateTime(_Date).Year) + String.Format("{0:00}", Convert.ToDateTime(_Date).Month) + String.Format("{0:00}", Convert.ToDateTime(_Date).Day) + ".zip";
                       FilingFileID = "OF" + TransChannelIDToAliasName(_ChannelID) + FilingFileID;
                       WCF100client.DownloadAsync(FilingFileID);
                       //WCF100client.DownloadAsync(args.Result);
                       WCF100client.DownloadCompleted += (s1, args1) =>
                       {
                           if (args1.Error != null)
                           {
                               MessageBox.Show(args1.Error.ToString());
                               return;
                           }
                           if (args1.Result != null)
                           {
                               try
                               {
                                   Stream fileobj = dialog.OpenFile();
                                   if (fileobj == null)
                                   {
                                   }
                                   else
                                   {
                                       fileobj.Write(args1.Result.File, 0, args1.Result.File.Length);
                                       fileobj.Close();
                                       BusyMsg.IsBusy = false;
                                       MessageBox.Show("轉檔完成");
                                   }
                               }
                               catch (Exception ex)
                               {
                                   BusyMsg.IsBusy = false;
                                   return;
                               }
                           }
                       };
                       //System.Windows.Browser.HtmlPage.Window.Navigate(new Uri(args.Result, UriKind.Absolute));
                   }
                   else
                       MessageBox.Show("沒有查到資料");
               }
               else
                   MessageBox.Show("產生檔案有問題,"+ args.Error);

           };
           BusyMsg.IsBusy = false;
       }

       private void buttonPrintMainControlTapeList_Click(object sender, RoutedEventArgs e)
       {
           if (_ChannelID == "")
           {
               MessageBox.Show("請先查詢節目表");
               return;
           }


           //檢查資料,如果有沒有輸入訊號的則提出警示

           string ErrMessage = "";

           BusyMsg.IsBusy = true;
           WSPGMSendSQL.SendSQLSoapClient DAY_TAPE_LIST = new WSPGMSendSQL.SendSQLSoapClient();
           System.Collections.ObjectModel.ObservableCollection<WSPGMSendSQL.ArrPromoList> InList = new System.Collections.ObjectModel.ObservableCollection<WSPGMSendSQL.ArrPromoList>();

           foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO)
           {
               if (TemArrPromoList.FSPROG_ID != "")
               {

                   InList.Add(new WSPGMSendSQL.ArrPromoList()
                   {
                       COMBINENO = TemArrPromoList.COMBINENO,
                       FCTIME_TYPE = TemArrPromoList.FCTIME_TYPE,
                       FSPROG_ID = TemArrPromoList.FSPROG_ID,
                       FNSEQNO = TemArrPromoList.FNSEQNO,
                       FSNAME = TemArrPromoList.FSNAME,
                       FSCHANNEL_ID = TemArrPromoList.FSCHANNEL_ID,
                       FSCHANNEL_NAME = TemArrPromoList.FSCHANNEL_NAME,
                       FSPLAY_TIME = TemArrPromoList.FSPLAY_TIME,
                       FSDURATION = TemArrPromoList.FSDURATION,
                       FNEPISODE = TemArrPromoList.FNEPISODE,
                       FNBREAK_NO = TemArrPromoList.FNBREAK_NO,
                       FNSUB_NO = TemArrPromoList.FNSUB_NO,
                       FSFILE_NO = TemArrPromoList.FSFILE_NO,
                       FSVIDEO_ID = TemArrPromoList.FSVIDEO_ID,
                       FSPROMO_ID = TemArrPromoList.FSPROMO_ID,
                       FDDATE = TemArrPromoList.FDDATE,
                       FNLIVE = TemArrPromoList.FNLIVE,
                       FSSTATUS = TemArrPromoList.FSSTATUS,
                       FCTYPE = TemArrPromoList.FCTYPE,
                       FSMEMO = TemArrPromoList.FSMEMO,
                       FSPROG_NAME = TemArrPromoList.FSPROG_NAME,
                       FSSIGNAL = TemArrPromoList.FSSIGNAL
                   });
               }
           }

           DAY_TAPE_LIST.PrintTapeListAsync(InList, UserClass.userData.FSUSER_ChtName);
           DAY_TAPE_LIST.PrintTapeListCompleted += (s, args) =>
           {
               BusyMsg.IsBusy = false;
               if (args.Error == null)
               {
                   if (args.Result == "")
                   {
                       MessageBox.Show("沒有資料");
                   }
                   else
                   {
                       HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + args.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,toolbar=0,statusbar=0,menubar=0,scrollbar=0,resizable=yes");

                   }

               }
               else
                   MessageBox.Show("產生主控播出排帶表失敗");
           };
       }

       private void buttonPrintMainControlTapeListSIGN_Click(object sender, RoutedEventArgs e)
       {
           if (_ChannelID == "")
           {
               MessageBox.Show("請先查詢節目表");
               return;
           }


           //檢查資料,如果有沒有輸入訊號的則提出警示

           string ErrMessage = "";

           BusyMsg.IsBusy = true;
           WSPGMSendSQL.SendSQLSoapClient DAY_TAPE_LIST_SIGN = new WSPGMSendSQL.SendSQLSoapClient();
           System.Collections.ObjectModel.ObservableCollection<WSPGMSendSQL.ArrPromoList> InList = new System.Collections.ObjectModel.ObservableCollection<WSPGMSendSQL.ArrPromoList>();

           foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO)
           {
               if (TemArrPromoList.FSPROG_ID != "")
               {

                   InList.Add(new WSPGMSendSQL.ArrPromoList()
                   {
                       COMBINENO = TemArrPromoList.COMBINENO,
                       FCTIME_TYPE = TemArrPromoList.FCTIME_TYPE,
                       FSPROG_ID = TemArrPromoList.FSPROG_ID,
                       FNSEQNO = TemArrPromoList.FNSEQNO,
                       FSNAME = TemArrPromoList.FSNAME,
                       FSCHANNEL_ID = TemArrPromoList.FSCHANNEL_ID,
                       FSCHANNEL_NAME = TemArrPromoList.FSCHANNEL_NAME,
                       FSPLAY_TIME = TemArrPromoList.FSPLAY_TIME,
                       FSDURATION = TemArrPromoList.FSDURATION,
                       FNEPISODE = TemArrPromoList.FNEPISODE,
                       FNBREAK_NO = TemArrPromoList.FNBREAK_NO,
                       FNSUB_NO = TemArrPromoList.FNSUB_NO,
                       FSFILE_NO = TemArrPromoList.FSFILE_NO,
                       FSVIDEO_ID = TemArrPromoList.FSVIDEO_ID,
                       FSPROMO_ID = TemArrPromoList.FSPROMO_ID,
                       FDDATE = TemArrPromoList.FDDATE,
                       FNLIVE = TemArrPromoList.FNLIVE,
                       FSSTATUS = TemArrPromoList.FSSTATUS,
                       FCTYPE = TemArrPromoList.FCTYPE,
                       FSMEMO = TemArrPromoList.FSMEMO,
                       FSPROG_NAME = TemArrPromoList.FSPROG_NAME,
                       FSSIGNAL = TemArrPromoList.FSSIGNAL
                   });
               }
           }

           DAY_TAPE_LIST_SIGN.PrintTapeListSignAsync(InList, UserClass.userData.FSUSER_ChtName);
           DAY_TAPE_LIST_SIGN.PrintTapeListSignCompleted += (s, args) =>
           {
               BusyMsg.IsBusy = false;
               if (args.Error == null)
               {
                   if (args.Result == "")
                   {
                       MessageBox.Show("沒有資料");
                   }
                   else
                   {
                       HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + args.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,toolbar=0,statusbar=0,menubar=0,scrollbar=0,resizable=yes");

                   }

               }
               else
                   MessageBox.Show("產生主控播出錄影帶簽收單失敗");
           };
       }

       private void buttonPrintFilingList_Click(object sender, RoutedEventArgs e)
       {
           if (_ChannelID == "")
           {
               MessageBox.Show("請先查詢節目表");
               return;
           }


           //檢查資料,如果有沒有輸入訊號的則提出警示

           string ErrMessage = "";

           BusyMsg.IsBusy = true;
           WSPGMSendSQL.SendSQLSoapClient FILING_LIST = new WSPGMSendSQL.SendSQLSoapClient();



           FILING_LIST.PrintFilingListAsync(_Date,_ChannelID, UserClass.userData.FSUSER_ChtName);
           FILING_LIST.PrintFilingListCompleted += (s, args) =>
           {
               BusyMsg.IsBusy = false;
               if (args.Error == null)
               {
                   if (args.Result == "")
                   {
                       MessageBox.Show("沒有資料");
                   }
                   else
                   {
                       HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + args.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,toolbar=0,statusbar=0,menubar=0,scrollbar=0,resizable=yes");

                   }

               }
               else
                   MessageBox.Show("產生Filing表失敗,訊息" + args.Error.ToString());
           };
       }

       private void buttonHDLouthProposeDeleteList_Click(object sender, RoutedEventArgs e)
       {

           WSPGMSendSQL.SendSQLSoapClient SP_GET_PROPOSE_DELETE_LIST = new WSPGMSendSQL.SendSQLSoapClient();
           //string ReturnStr = "";

           StringBuilder sb = new StringBuilder();

           SP_GET_PROPOSE_DELETE_LIST.Print_Propose_Delete_ListAsync(UserClass.userData.FSUSER_ChtName, "49");
           //SP_Q_TBPGM_QUEUE.Do_QueryCompleted += new EventHandler<SendSQL.Do_QueryCompletedEventArgs>(SP_Q_TBPGM_QUEUE_Do_QueryCompleted);
           //AAA.Do_InsertAsync("", "", ReturnStr);
           SP_GET_PROPOSE_DELETE_LIST.Print_Propose_Delete_ListCompleted += (s, args) =>
           {
               if (args.Error == null)
               {
                   if (args.Result != null && args.Result != "")
                   {
                       if (args.Result.ToString().StartsWith("ERROR:"))
                           MessageBox.Show(args.Result.ToString(), "錯誤訊息", MessageBoxButton.OK);
                       else
                           HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + args.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,toolbar=0,statusbar=0,menubar=0,scrollbar=0,resizable=yes");
                   }
                   else
                       MessageBox.Show("沒有查到建議刪除清單!", "提示訊息", MessageBoxButton.OK);
               }


           };          
       }

       private void buttonPrint_Combine_Queue_Key_List_E_Click(object sender, RoutedEventArgs e)
       {
           if (_ChannelID == "")
           {
               MessageBox.Show("請先查詢節目表");
               return;
           }
           if (ModuleClass.getModulePermission("P5" + _ChannelID + "006") == false)
           {
               MessageBox.Show("您沒有轉出此頻道LST檔案的權限");
               return;
           }

           string ErrMessage = "";
           foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO)
           {
               if (TemArrPromoList.FSPROG_ID != "")
               {
                   if (TemArrPromoList.FSSIGNAL == "")
                   {
                       ErrMessage = ErrMessage + "播出時間" + TemArrPromoList.FSPLAY_TIME + " 名稱:" + TemArrPromoList.FSNAME + ":沒有選擇訊號源" + Convert.ToChar(10) + Convert.ToChar(13);

                   }
               }
           }
           if (ErrMessage != "")
           {
               if (MessageBox.Show(ErrMessage + "要繼續嗎?", "提示訊息", MessageBoxButton.OKCancel) == MessageBoxResult.Cancel)
                   return;
           }
           BusyMsg.IsBusy = true;
           WSPGMSendSQL.SendSQLSoapClient TransList = new WSPGMSendSQL.SendSQLSoapClient();
           System.Collections.ObjectModel.ObservableCollection<WSPGMSendSQL.ArrPromoList> InList = new System.Collections.ObjectModel.ObservableCollection<WSPGMSendSQL.ArrPromoList>();

           foreach (ArrPromoList TemArrPromoList in PGM_ARR_PROMO)
           {
               if (TemArrPromoList.FSPROG_ID != "")
               {

                   InList.Add(new WSPGMSendSQL.ArrPromoList()
                   {
                       COMBINENO = TemArrPromoList.COMBINENO,
                       FCTIME_TYPE = TemArrPromoList.FCTIME_TYPE,
                       FSPROG_ID = TemArrPromoList.FSPROG_ID,
                       FNSEQNO = TemArrPromoList.FNSEQNO,
                       FSNAME = TemArrPromoList.FSNAME,
                       FSCHANNEL_ID = TemArrPromoList.FSCHANNEL_ID,
                       FSCHANNEL_NAME = TemArrPromoList.FSCHANNEL_NAME,
                       FSPLAY_TIME = TemArrPromoList.FSPLAY_TIME,
                       FSDURATION = TemArrPromoList.FSDURATION,
                       FNEPISODE = TemArrPromoList.FNEPISODE,
                       FNBREAK_NO = TemArrPromoList.FNBREAK_NO,
                       FNSUB_NO = TemArrPromoList.FNSUB_NO,
                       FSFILE_NO = TemArrPromoList.FSFILE_NO,
                       FSVIDEO_ID = TemArrPromoList.FSVIDEO_ID,
                       FSPROMO_ID = TemArrPromoList.FSPROMO_ID,
                       FDDATE = TemArrPromoList.FDDATE,
                       FNLIVE = TemArrPromoList.FNLIVE,
                       FSSTATUS = TemArrPromoList.FSSTATUS,
                       FCTYPE = TemArrPromoList.FCTYPE,
                       FSMEMO = TemArrPromoList.FSMEMO,
                       FSPROG_NAME = TemArrPromoList.FSPROG_NAME,
                       FSSIGNAL = TemArrPromoList.FSSIGNAL
                   });
               }
           }

            TransList.PrintHDLstAsync(InList, UserClass.userData.FSUSER_ChtName.ToString());
            TransList.PrintHDLstCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {
                        WSPGMSendSQL.SendSQLSoapClient GETURL = new WSPGMSendSQL.SendSQLSoapClient();
                        GETURL.GETReportURLAsync("RPT_HD_DATE_LST", args.Result);
                        GETURL.GETReportURLCompleted += (s1, args1) =>
                        {
                            if (args1.Error == null)
                            {
                                if (args1.Result != null && args1.Result != "")
                                {
                                    BusyMsg.IsBusy = false;
                                    if (args1.Result.ToString().StartsWith("錯誤"))
                                        MessageBox.Show(args1.Result.ToString(), "提示訊息", MessageBoxButton.OK);
                                    else
                                        HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + args1.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,toolbar=0,statusbar=0,menubar=0,scrollbar=0");
                                }
                                else
                                {
                                    BusyMsg.IsBusy = false;
                                    MessageBox.Show("啟動報表功能異常，請通知系統管理員", "提示訊息", MessageBoxButton.OK);

                                }

                            }
                        };


                    }
                    else
                    {
                        BusyMsg.IsBusy = false;
                        MessageBox.Show("產生檔案有問題");
                    }
                }
            };
           
           
       }
   
    }
}
