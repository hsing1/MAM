﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace PTS_MAM3.PGM
{
    public partial class PGM115_01 : ChildWindow
    {
        public PGM115_01()
        {
            InitializeComponent();
        }
        public string _Promo_ID;
        public string _Promo_Name;

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (DCdataGridPromoList.SelectedIndex < 0)
            {
                MessageBox.Show("請先選取節目資料");
                return;
            }

            for (int i = DCdataGridPromoList.SelectedItems.Count - 1; i >= 0; i--)
            {
                ReturnPromo = ((SimplePromoList)DCdataGridPromoList.SelectedItems[i]);
            }


            _Promo_ID = ReturnPromo.Promo_ID;
            _Promo_Name = ReturnPromo.Promo_Name;

            //_Promo_ID = GetPromoList[DCdataGridPromoList.SelectedIndex].Promo_ID;
            //_Promo_Name = GetPromoList[DCdataGridPromoList.SelectedIndex].Promo_Name;
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            _Promo_ID = "";
            this.DialogResult = false;
        }
        List<SimplePromoList> GetPromoList = new List<SimplePromoList>();
        private void buttonQuery_Click(object sender, RoutedEventArgs e)
        {
            if (textBoxNewPromoName.Text == "")
            {
                //MessageBox.Show("請輸入要查的短帶名稱");
                //return;
                textBoxNewPromoName.Text = "%";
            }
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_PROMO_SIMPLE = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            sb.AppendLine("<FSPROMO_NAME>" + TransferTimecode.ReplaceXML(textBoxNewPromoName.Text) + "</FSPROMO_NAME>");

            sb.AppendLine("</Data>");

            SP_Q_TBPGM_PROMO_SIMPLE.Do_QueryAsync("SP_Q_TBPGM_PROMO_SIMPLE", sb.ToString(), ReturnXML);
            SP_Q_TBPGM_PROMO_SIMPLE.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {

                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        GetPromoList.Clear();
                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {

                            GetPromoList.Add(new SimplePromoList()
                            {
                                Promo_ID = elem.Element("FSPROMO_ID").Value.ToString(),
                                Promo_Name = elem.Element("FSPROMO_NAME").Value.ToString(),
                            });
                        }
                        DCdataGridPromoList.ItemsSource = null;
                        DCdataGridPromoList.ItemsSource = GetPromoList;
                    }
                }
            };           
        
        }

        public SimplePromoList ReturnPromo = new SimplePromoList();

        private void DCdataGridPromoList_RowDoubleClicked(object sender, DataGridDCTest.DataGridRowClickedArgs e)
        {
            if (DCdataGridPromoList.SelectedIndex < 0)
            {
                MessageBox.Show("請先選取節目資料");
                return;
            }
            
            for (int i = DCdataGridPromoList.SelectedItems.Count - 1; i >= 0; i--)
            {
                ReturnPromo = ((SimplePromoList)DCdataGridPromoList.SelectedItems[i]);
            }


            _Promo_ID = ReturnPromo.Promo_ID;
            _Promo_Name = ReturnPromo.Promo_Name;

            //_Promo_ID = GetPromoList[DCdataGridPromoList.SelectedIndex].Promo_ID;
            //_Promo_Name = GetPromoList[DCdataGridPromoList.SelectedIndex].Promo_Name;
            this.DialogResult = true;
        }

        private void DCdataGridPromoList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}

