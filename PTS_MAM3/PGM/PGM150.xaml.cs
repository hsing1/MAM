﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace PTS_MAM3.PGM
{
    public partial class PGM150 : Page
    {
        public PGM150()
        {
            InitializeComponent();
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBZCHANNEL_ALL = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";
            ModuleClass.getModulePermission("");

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            sb.AppendLine("</Data>");
            SP_Q_TBZCHANNEL_ALL.Do_QueryAsync("[SP_Q_TBZCHANNEL_ALL]", sb.ToString(), ReturnXML);
            //SP_Q_TBPGM_QUEUE.Do_QueryCompleted += new EventHandler<SendSQL.Do_QueryCompletedEventArgs>(SP_Q_TBPGM_QUEUE_Do_QueryCompleted);
            //AAA.Do_InsertAsync("", "", ReturnStr);
            SP_Q_TBZCHANNEL_ALL.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            if (ModuleClass.getModulePermission("P1" + elem.Element("ID").Value.ToString() + "001") == true || ModuleClass.getModulePermission("P1" + elem.Element("ID").Value.ToString() + "002") == true)
                            {
                                ComboBoxItem TempComboBoxItem = new ComboBoxItem();
                                TempComboBoxItem.Content = elem.Element("NAME").Value.ToString();
                                TempComboBoxItem.Tag = elem.Element("ID").Value.ToString() + "-" + elem.Element("TYPE").Value.ToString();
                            if (elem.Element("TYPE").Value.ToString()=="001")
                                comboBoxSourceChannel.Items.Add(TempComboBoxItem);
                            else
                                comboBoxDescChannel.Items.Add(TempComboBoxItem);
                            }

                        }
                    }
                }
            };

        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void buttonGetDate_Click(object sender, RoutedEventArgs e)
        {
            if (calendar1.Visibility == System.Windows.Visibility.Visible)
                calendar1.Visibility = System.Windows.Visibility.Collapsed;
            else
                calendar1.Visibility = System.Windows.Visibility.Visible;
        }

        private void calendar1_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
        {
            //this.textBoxDate.Text = this.calendar1.SelectedDate.ToString();
            DateTime Dt1 = calendar1.SelectedDate.Value;
            this.textBoxDate.Text = Dt1.ToString("yyyy/MM/dd");
            calendar1.Visibility = System.Windows.Visibility.Collapsed;
        }

        string _Date = "";
        string _ChannelSourceType = "";
        string _ChannelSourceID = "";
        string _ChannelSourceName = "";
        string _ChannelDescType = "";
        string _ChannelDescID = "";
        string _ChannelDescName = "";

        private void buttonCopy_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (comboBoxSourceChannel.SelectedIndex == -1)
                {
                    MessageBox.Show("請先選擇來源頻道");
                    return;
                }
                if (comboBoxDescChannel.SelectedIndex == -1)
                {
                    MessageBox.Show("請先選擇目的頻道");
                    return;
                }

                if (this.textBoxDate.Text == "")
                {
                    MessageBox.Show("請先選擇要複製的日期");
                    return;
                }
                WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_QUEUE = new WSPGMSendSQL.SendSQLSoapClient();
               
                _Date = this.textBoxDate.Text;
                if (comboBoxSourceChannel.SelectedIndex != -1)
                {
                    string[] ChannelType = ((ComboBoxItem)comboBoxSourceChannel.SelectedItem).Tag.ToString().Split(new Char[] { '-' });
                    _ChannelSourceType = ChannelType[1];
                    _ChannelSourceID = ChannelType[0];
                    _ChannelSourceName = ((ComboBoxItem)comboBoxSourceChannel.SelectedItem).Content.ToString();
                }

                if (comboBoxDescChannel.SelectedIndex != -1)
                {
                    string[] ChannelType = ((ComboBoxItem)comboBoxDescChannel.SelectedItem).Tag.ToString().Split(new Char[] { '-' });
                    _ChannelDescType = ChannelType[1];
                    _ChannelDescID = ChannelType[0];
                    _ChannelDescName = ((ComboBoxItem)comboBoxDescChannel.SelectedItem).Content.ToString();
                }
                SP_Q_TBPGM_QUEUE.Do_Copy_Play_List_SD_TO_HDAsync(_ChannelSourceID, _ChannelDescID, _Date);
                SP_Q_TBPGM_QUEUE.Do_Copy_Play_List_SD_TO_HDCompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (args.Result == "")
                        {
                            MessageBox.Show("拷貝節目表完成");
                        }
                        else
                        {
                            MessageBox.Show("訊息:" + args.Result);
                        }

                    }
                };
            }
            catch
            {

                MessageBox.Show("拷貝節目表失敗!");
            }
        }

    }
}
