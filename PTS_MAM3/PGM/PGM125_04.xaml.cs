﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;

namespace PTS_MAM3.PGM
{
    public partial class PGM125_04 : UserControl
    {
        public PGM125_04()
        {
            InitializeComponent();
        }
        List<CheckDiffPromo> ListDiffPromo=new List<CheckDiffPromo>();
        string _date = "";
        string _channel_id="";
        private PTS_MAM3.PGM.PGM125 parentFrame = new PTS_MAM3.PGM.PGM125();
        public PGM125_04(List<CheckDiffProg> ListProg, List<CheckDiffPromo> ListPromo, string indate, string inchannel_id, PTS_MAM3.PGM.PGM125 UPForm)
        {
            InitializeComponent();
            parentFrame = UPForm;
            dataGridQueue.ItemsSource = null;
            dataGridQueue.ItemsSource = ListProg;
            dataGridPromo.ItemsSource = null;
            dataGridPromo.ItemsSource = ListPromo;
            textBlockProg.Text = "節目段落資料:一共: " + ListProg.Count.ToString() + " 筆";
            textBlockPromo.Text = "短帶資料:一共: " + ListPromo.Count.ToString() + " 筆";
            ListDiffPromo=ListPromo;
            _date = indate;
            _channel_id = inchannel_id;
        }
        private void buttonUpdateDur_Click(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show( ListDiffPromo.Count.ToString());
            WSPGMSendSQL.SendSQLSoapClient SP_U_TBPGM_ARRPROMO_DUR = new WSPGMSendSQL.SendSQLSoapClient();
            StringBuilder sbDiffPromo = new StringBuilder();
            sbDiffPromo.AppendLine("<Datas>");
            int ModifyCount = 0;
            foreach (CheckDiffPromo TempDiffPromo in ListDiffPromo)
            {
                if (TempDiffPromo.FSDiffDur != "")
                {

                    ModifyCount = ModifyCount + 1;
                    sbDiffPromo.AppendLine("<Data>");
                    //sbDiffPromo.AppendLine("<fspromo_id>" + TempDiffPromo.fspromo_id + "</fspromo_id>");
                    //sbDiffPromo.AppendLine("<FSPROMO_NAME>" + TempDiffPromo.FSPROMO_NAME + "</FSPROMO_NAME>");
                    //sbDiffPromo.AppendLine("<FSBEG_TIMECODE>" + TempDiffPromo.FSBEG_TIMECODE + "</FSBEG_TIMECODE>");
                    //sbDiffPromo.AppendLine("<FSEND_TIMECODE>" + TempDiffPromo.FSEND_TIMECODE + "</FSEND_TIMECODE>");
                    //sbDiffPromo.AppendLine("<fsdur>" + TempDiffPromo.fsdur + "</fsdur>");
                    //sbDiffPromo.AppendLine("<fsduration>" + TempDiffPromo.fsduration + "</fsduration>");
                    //sbDiffPromo.AppendLine("<FSDiffDur>" + TempDiffPromo.FSDiffDur + "</FSDiffDur>");
                    //sbDiffPromo.AppendLine("<FSVIDEO_ID>" + TempDiffPromo.FSVIDEO_ID + "</FSVIDEO_ID>");
                    //sbDiffPromo.AppendLine("<FSSTATUS>" + TempDiffPromo.FSSTATUS + "</FSSTATUS>");

                    sbDiffPromo.AppendLine("<fspromo_id>" + TempDiffPromo.fspromo_id + "</fspromo_id>");
                    sbDiffPromo.AppendLine("<fsduration>" + TempDiffPromo.fsduration + "</fsduration>");
                    //sbDiffPromo.AppendLine("<FDDATE>" + _date + "</FDDATE>");
                    //sbDiffPromo.AppendLine("<FSCHANNEL_ID>" + _channel_id + "</FSCHANNEL_ID>");
                    sbDiffPromo.AppendLine("</Data>");
                }
                
            }
            sbDiffPromo.AppendLine("</Datas>");
            if (ModifyCount==0)
                MessageBox.Show("沒有資料可以修改");

            SP_U_TBPGM_ARRPROMO_DUR.Do_Update_ARR_PROMO_DURAsync(sbDiffPromo.ToString(), _date, _channel_id);
            SP_U_TBPGM_ARRPROMO_DUR.Do_Update_ARR_PROMO_DURCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result == true)
                    {
                        parentFrame.UpdateGridPromoDur(ListDiffPromo);
                        MessageBox.Show("修改完成");
                    }
                    else
                        MessageBox.Show("修改失敗!!!!");

                }
            };
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            parentFrame.UpdateGridPromoDur(ListDiffPromo);
        }
    }
}

