﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace PTS_MAM3.PGM
{
    public partial class PGM120_01 : ChildWindow
    {
        public PGM120_01()
        {
            InitializeComponent();
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBZCHANNEL_ALL = new WSPGMSendSQL.SendSQLSoapClient();
            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            sb.AppendLine("</Data>");
            SP_Q_TBZCHANNEL_ALL.Do_QueryAsync("[SP_Q_TBZCHANNEL_ALL]", sb.ToString(), ReturnXML);
            //SP_Q_TBPGM_QUEUE.Do_QueryCompleted += new EventHandler<SendSQL.Do_QueryCompletedEventArgs>(SP_Q_TBPGM_QUEUE_Do_QueryCompleted);
            //AAA.Do_InsertAsync("", "", ReturnStr);
            SP_Q_TBZCHANNEL_ALL.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {


                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);

                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                                ComboBoxItem TempComboBoxItem = new ComboBoxItem();
                                TempComboBoxItem.Content = elem.Element("NAME").Value.ToString();
                                TempComboBoxItem.Tag = elem.Element("ID").Value.ToString();
                                //TempComboBoxItem.Tag = elem.Element("ID").Value.ToString() + "-" + elem.Element("TYPE").Value.ToString();

                                comboBoxChannel.Items.Add(TempComboBoxItem);

                        }
                    }
                }
            };
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            ReturnData();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
        public string _progid="";
        public string _progname="";
        public string _progseqno="";
        public string _ChannelName = "";
        public string _ChannelID = "";

        List<SimpleProgList> GetSimpleProgList = new List<SimpleProgList>();
        List<ProgBankDetail> GetProgBankDetailList = new List<ProgBankDetail>();


        private void ReturnData()
        {
            if (radioButtonProg.IsChecked == true)
            {
                if (DCdataGridProgList.SelectedIndex < 0)
                {
                    MessageBox.Show("請先選取節目資料");
                    return;
                }
                _progid = ((SimpleProgList)DCdataGridProgList.SelectedItems[0]).Prog_ID;
                _progseqno = "-1";
                _progname = ((SimpleProgList)DCdataGridProgList.SelectedItems[0]).Prog_Name;
                _ChannelName = "";
                _ChannelID = "";
                //_progid = GetSimpleProgList[DCdataGridProgList.SelectedIndex].Prog_ID;
                //_progseqno = "-1";
                //_progname = GetSimpleProgList[DCdataGridProgList.SelectedIndex].Prog_Name;
            }
            else if (radioButtonBankDetail.IsChecked == true)
            {
                if (DCdataGridBankDetailList.SelectedIndex < 0)
                {
                    MessageBox.Show("請先選取節目資料");
                    return;
                }

                _progid = ((ProgBankDetail)DCdataGridBankDetailList.SelectedItems[0]).FSPROG_ID;
                _progseqno = ((ProgBankDetail)DCdataGridBankDetailList.SelectedItems[0]).FNSEQNO;
                _progname = ((ProgBankDetail)DCdataGridBankDetailList.SelectedItems[0]).FSPROG_NAME;
                _ChannelName = "";
                _ChannelID = "";
                //_progid = GetProgBankDetailList[DCdataGridBankDetailList.SelectedIndex].FSPROG_ID;
                //_progseqno = GetProgBankDetailList[DCdataGridBankDetailList.SelectedIndex].FNSEQNO;
                //_progname = GetProgBankDetailList[DCdataGridBankDetailList.SelectedIndex].FSPROG_NAME;
            }
            else if (radioButtonProgChannel.IsChecked == true)
            {
                if (DCdataGridProgList.SelectedIndex < 0)
                {
                    MessageBox.Show("請先選取節目資料");
                    return;
                }

                //string[] ChannelType = ((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString().Split(new Char[] { '-' });
                //_ChannelType = ChannelType[1];
                //_ChannelID = ChannelType[0];
                //_ChannelName = ((ComboBoxItem)comboBoxChannel.SelectedItem).Content.ToString();

                _progid = ((SimpleProgList)DCdataGridProgList.SelectedItems[0]).Prog_ID;
                _progseqno = "-1";
                _progname = ((SimpleProgList)DCdataGridProgList.SelectedItems[0]).Prog_Name;
                _ChannelName = ((ComboBoxItem)comboBoxChannel.SelectedItem).Content.ToString();
                _ChannelID = ((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString();
                //_progid = GetProgBankDetailList[DCdataGridBankDetailList.SelectedIndex].FSPROG_ID;
                //_progseqno = GetProgBankDetailList[DCdataGridBankDetailList.SelectedIndex].FNSEQNO;
                //_progname = GetProgBankDetailList[DCdataGridBankDetailList.SelectedIndex].FSPROG_NAME;

            }

            this.DialogResult = true;
        }

        private void DCdataGridPromoList_RowDoubleClicked(object sender, DataGridDCTest.DataGridRowClickedArgs e)
        {
            ReturnData();
        }

        private void buttonQuery_Click(object sender, RoutedEventArgs e)
        {
            if (textBoxNewProgName.Text == "")
            {
                //MessageBox.Show("請輸入節目名稱條件");
                //return;
                textBoxNewProgName.Text = "%";
            }
 
            if ((radioButtonProg.IsChecked == true) || (radioButtonProgChannel.IsChecked==true))
            { 
                
                WSPGMSendSQL.SendSQLSoapClient SP_Q_PROG = new WSPGMSendSQL.SendSQLSoapClient();
                //string ReturnStr = "";

                StringBuilder sb = new StringBuilder();

                object ReturnXML = "";
                sb.AppendLine("<Data>");
                //sb.AppendLine("<FSPROG_NAME>" + textBoxNewProgName.Text + "</FSPROG_NAME>");
                sb.AppendLine("<FSPGMNAME>" + textBoxNewProgName.Text + "</FSPGMNAME>");
                sb.AppendLine("</Data>");

                SP_Q_PROG.Do_QueryAsync("SP_Q_TBPROG_M_BY_NAME", sb.ToString(), ReturnXML);
                SP_Q_PROG.Do_QueryCompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (args.Result != null)
                        {

                            byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                            GetSimpleProgList.Clear();
                            StringBuilder output = new StringBuilder();
                            // Create an XmlReader
                            XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                            var ltox = from str in doc.Elements("Datas").Elements("Data")
                                       select str;
                            foreach (XElement elem in ltox)
                            {

                                GetSimpleProgList.Add(new SimpleProgList()
                                {
                                    Prog_ID = elem.Element("FSPROG_ID").Value.ToString(),
                                    Prog_Name = elem.Element("FSPGMNAME").Value.ToString(),
                                });
                            }
                            DCdataGridProgList.ItemsSource = null;
                            DCdataGridProgList.ItemsSource = GetSimpleProgList;
                        }
                    }
                };           



            }
            else
            { 
                 WSPGMSendSQL.SendSQLSoapClient SP_Q_PROG = new WSPGMSendSQL.SendSQLSoapClient();
                //string ReturnStr = "";

                StringBuilder sb = new StringBuilder();
                object ReturnXML = "";
                sb.AppendLine("<Data>");
                sb.AppendLine("<FSPROG_NAME>" + textBoxNewProgName.Text + "</FSPROG_NAME>");
                sb.AppendLine("<FSPROG_ID>" + "" + "</FSPROG_ID>");
                sb.AppendLine("<FNSEQNO>" + "" + "</FNSEQNO>");
                sb.AppendLine("</Data>");

                SP_Q_PROG.Do_QueryAsync("SP_Q_TBPGM_BANK_DETAIL", sb.ToString(), ReturnXML);
                SP_Q_PROG.Do_QueryCompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (args.Result != null && args.Result != "")
                        {

                            byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                            GetProgBankDetailList.Clear();
                            StringBuilder output = new StringBuilder();
                            // Create an XmlReader
                            XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                            var ltox = from str in doc.Elements("Datas").Elements("Data")
                                       select str;
                            foreach (XElement elem in ltox)
                            {
                                GetProgBankDetailList.Add(new ProgBankDetail()
                                {
                                    FSPROG_ID = elem.Element("FSPROG_ID").Value.ToString(),
                                    FNSEQNO = elem.Element("FNSEQNO").Value.ToString(),
                                    FSPROG_NAME = elem.Element("FSPROG_NAME").Value.ToString(),
                                    FDBEG_DATE = elem.Element("FDBEG_DATE").Value.ToString(),
                                    FDEND_DATE = elem.Element("FDEND_DATE").Value.ToString(),
                                    FSBEG_TIME = elem.Element("FSBEG_TIME").Value.ToString(),
                                    FSEND_TIME = elem.Element("FSEND_TIME").Value.ToString(),
                                    FSCHANNEL_ID = elem.Element("FSCHANNEL_ID").Value.ToString(),
                                    FSCHANNEL_NAME = elem.Element("FSCHANNEL_NAME").Value.ToString(),
                                    FNREPLAY = elem.Element("FNREPLAY").Value.ToString(),
                                    FSWEEK = elem.Element("FSWEEK").Value.ToString(),
                                    FSPLAY_TYPE = elem.Element("FSPLAY_TYPE").Value.ToString(),
                                    FNSEG = elem.Element("FNSEG").Value.ToString(),
                                    FNDUR = elem.Element("FNDUR").Value.ToString(),
                                });
                            }
                            DCdataGridBankDetailList.ItemsSource = null;
                            DCdataGridBankDetailList.ItemsSource = GetProgBankDetailList;
                        }
                    }
                };           
            }
        }

        private void radioButtonProg_Click(object sender, RoutedEventArgs e)
        {
            //DCdataGridPromoList.ItemsSource = null;
            if (radioButtonBankDetail.IsChecked == true)
            {
                DCdataGridProgList.Visibility = Visibility.Collapsed;
                DCdataGridBankDetailList.Visibility = Visibility.Visible;
            }
            else
            {
                DCdataGridProgList.Visibility = Visibility.Visible;
                DCdataGridBankDetailList.Visibility = Visibility.Collapsed;
            }
        }

        private void radioButtonBankDetail_Click(object sender, RoutedEventArgs e)
        {
            if (radioButtonBankDetail.IsChecked == true)
            {
                DCdataGridProgList.Visibility = Visibility.Collapsed;
                DCdataGridBankDetailList.Visibility = Visibility.Visible;
            }
            else
            {
                DCdataGridProgList.Visibility = Visibility.Visible;
                DCdataGridBankDetailList.Visibility = Visibility.Collapsed;
            }
        }

        private void DCdataGridProgList_RowDoubleClicked(object sender, DataGridDCTest.DataGridRowClickedArgs e)
        {
            ReturnData();
        }

        private void radioButtonProgChannel_Click(object sender, RoutedEventArgs e)
        {
            if (radioButtonBankDetail.IsChecked == true)
            {
                DCdataGridProgList.Visibility = Visibility.Collapsed;
                DCdataGridBankDetailList.Visibility = Visibility.Visible;
            }
            else
            {
                DCdataGridProgList.Visibility = Visibility.Visible;
                DCdataGridBankDetailList.Visibility = Visibility.Collapsed;
            }
        }
    }
}

