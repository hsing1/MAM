﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace PTS_MAM3
{
    public partial class PGM100_07 : ChildWindow
    {
        public PGM100_07()
        {
            InitializeComponent();
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBZSIGNAL_ALL = new WSPGMSendSQL.SendSQLSoapClient();
            StringBuilder sb1 = new StringBuilder();
            object ReturnXML1 = "";
            sb1.AppendLine("<Data>");
            sb1.AppendLine("</Data>");
            object ReturnXML = "";
            SP_Q_TBZSIGNAL_ALL.Do_QueryAsync("[SP_Q_TBZSIGNAL_ALL]", sb1.ToString(), ReturnXML);
            SP_Q_TBZSIGNAL_ALL.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {

                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);

                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            ComboBoxItem TempComboBoxItem = new ComboBoxItem();
                            TempComboBoxItem.Content = elem.Element("FSSIGNALNAME").Value.ToString();

                            comboBoxSignal1.Items.Add(TempComboBoxItem);

                            //TempSignalList.Add(elem.Element("FSSIGNALNAME").Value.ToString());
                        }
                        //SignalList = TempSignalList;
                    }
                    else
                    {
                    }
                }

            };
        }

        public string XElementToString(XElement INXElement)
        {
            string ReturnStr;
            try
            {
                ReturnStr = INXElement.Value.ToString();
                return ReturnStr;

            }
            catch (Exception ex)
            {
                return "";
            }
        }


        private void GetDefSetting()
        {
           

            if (textBoxProgID.Text == "")
            {
                return;
            }
            if (textBoxEpisode.Text == "")
            {
                return;
            }
            
            WSPGMSendSQL.SendSQLSoapClient SP_Q_WaitReplaceList = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";
            StringBuilder sb1 = new StringBuilder();
            object ReturnXML1 = "";
            sb1.AppendLine("<Data>");
            sb1.AppendLine("<FSPROG_ID>" + textBoxProgID.Text + "</FSPROG_ID>");
            sb1.AppendLine("<FNEPISODE>" + textBoxEpisode.Text + "</FNEPISODE>");
            sb1.AppendLine("<FDDATE>" + _date + "</FDDATE>");

            sb1.AppendLine("</Data>");
            SP_Q_WaitReplaceList.Do_QueryAsync("[SP_Q_GET_QUEUE_DEF_SEC_SIGNAL]", sb1.ToString());
            SP_Q_WaitReplaceList.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {
                        byte[] byteArray2 = Encoding.Unicode.GetBytes(args.Result);
                        StringBuilder output2 = new StringBuilder();
                        // Create an XmlReader

                        XDocument doc = XDocument.Load(new MemoryStream(byteArray2));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            string RFDDATE = XElementToString(elem.Element("FDDATE"));
                            string RFNDEF_MIN = XElementToString(elem.Element("FNDEF_MIN"));
                            string RFNDEF_SEC = XElementToString(elem.Element("FNDEF_SEC"));
                            string RFSSIGNAL = XElementToString(elem.Element("FSSIGNAL"));
                            string FSBEG_TIME = XElementToString(elem.Element("FSBEG_TIME"));
                            string FSEND_TIME = XElementToString(elem.Element("FSEND_TIME"));
                            textBoxDefMin.Text = RFNDEF_MIN;
                            textBoxDefSec.Text=RFNDEF_SEC;
                            textBoxBTime.Text = FSBEG_TIME;
                            textBoxETime.Text = FSEND_TIME;

                            comboBoxSignal1.SelectedIndex = -1;
                            for (int Combocount = 0; Combocount < comboBoxSignal1.Items.Count; Combocount++)
                            {
                                if (((ComboBoxItem)comboBoxSignal1.Items[Combocount]).Content.ToString() == RFSSIGNAL)
                                {
                                    comboBoxSignal1.SelectedIndex = Combocount;
                                }
                            }

                            return;
                           

                        }
                    }      
                }

            };  
        }


        public PgmQueueDate _PgmQueueProg=new PgmQueueDate();
        public string _date;
        public string _channelid;
        public string _ChannelFileType;
        public string _channelName;
        public string _FromForm;
        public bool _HasInsert = false;
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (textBoxProgID.Text =="")
            {
                MessageBox.Show("請查詢節目");
                return;
            }
            if (textBoxEpisode.Text == "")
            {
                MessageBox.Show("請查詢節目集數資料");
                return;
            }
            if (textBoxProgName.Text == "")
            {
                MessageBox.Show("請查詢節目名稱資料");
                return;
            }
            if (textBoxDefSeg.Text == "")
            {
                MessageBox.Show("請輸入預設段數");
                return;
            }
            if (textBoxDefMin.Text == "")
            {
                MessageBox.Show("請輸入預設分鐘");
                return;
            }
            if (textBoxDefSec.Text == "")
            {
                MessageBox.Show("請輸入預設秒數");
                return;
            }
            if (comboBoxLive.SelectedIndex == -1)
            {
                MessageBox.Show("請選擇Live種類");
                return;
            }
            if (this.textBoxBTime.Text == "")
            {
                MessageBox.Show("請輸入起始時間");
                return;
            }
            if (this.textBoxETime.Text == "")
            {
                MessageBox.Show("請輸入結束時間");
                return;
            }
            if (this.textBoxBTime.Text.Length < 4)
            {
                MessageBox.Show("起始時間格式有誤");
                return;
            }
            if (this.textBoxETime.Text.Length < 4)
            {
                MessageBox.Show("結束時間格式有誤");
                return;
            }
            try
            {
                int.Parse(textBoxDefSeg.Text);
                int.Parse(textBoxDefMin.Text);
                int.Parse(textBoxDefSec.Text);
                int.Parse(textBoxBTime.Text);
                int.Parse(textBoxETime.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("預設段數,預設長度,起始時間,結束時間都必須為數字格式");
                return;
            }


            if (int.Parse(textBoxETime.Text) < int.Parse(textBoxBTime.Text))
            {
                MessageBox.Show("結束時間必須大於起始時間");
                return;
                
            }

           TransProgList.WebService1SoapClient MapProg_Episode_VideoID = new TransProgList.WebService1SoapClient();
            //if (AAA.TransProglistAsync("N", "2010/12/16") = "ok")
            
           MapProg_Episode_VideoID.MapVideoIDAsync(_channelid, textBoxProgID.Text, textBoxEpisode.Text);

            MapProg_Episode_VideoID.MapVideoIDCompleted += (s1, args1) =>
                {
                    if (args1.Error == null)
                    {
                        if (args1.Result == "取得Video ID有誤")
                        {
                            MessageBox.Show("對應節目集數與VideoID發生錯誤,不可以插入節目!");
                            return;
                        }
                    }
                    //先建立或取得播映序號
                    WSPGMSendSQL.SendSQLSoapClient TBPGMBank_Deail_Modify = new WSPGMSendSQL.SendSQLSoapClient();
                    StringBuilder sb = new StringBuilder();
                    object ReturnXML = "";

                    sb.AppendLine("<Data>");
                    sb.AppendLine("<FSPROG_ID>" + this.textBoxProgID.Text + "</FSPROG_ID>");
                    sb.AppendLine("<FNEPISODE>" + this.textBoxEpisode.Text + "</FNEPISODE>");
                    sb.AppendLine("<FDDATE>" + _date + "</FDDATE>");
                    sb.AppendLine("<FSBTIME>" + this.textBoxBTime.Text + "</FSBTIME>");
                    sb.AppendLine("<FSETIME>" + this.textBoxETime.Text + "</FSETIME>");
                    sb.AppendLine("<FSPROGNAME>" + TransferTimecode.ReplaceXML(this.textBoxProgName.Text) + "</FSPROGNAME>");
                    sb.AppendLine("<FNDUR>" + this.textBoxDefMin.Text + "</FNDUR>");
                    sb.AppendLine("<FNSEG>" + this.textBoxDefSeg.Text + "</FNSEG>");
                    sb.AppendLine("<FSPLAY_TYPE>" + ((ComboBoxItem)comboBoxLive.SelectedItem).Tag.ToString() + "</FSPLAY_TYPE>");
                    sb.AppendLine("<FSCHANNEL_ID>" + _channelid + "</FSCHANNEL_ID>");
                    sb.AppendLine("<FSMEMO>" + textBoxMemo.Text + "</FSMEMO>");
                    sb.AppendLine("<FSMEMO1>" + textBoxMemo1.Text + "</FSMEMO1>");
                    sb.AppendLine("<FSSIGNAL>" + ((ComboBoxItem)comboBoxSignal1.SelectedItem).Content.ToString() + "</FSSIGNAL>");
                    sb.AppendLine("<FNDEF_MIN>" + textBoxDefMin.Text + "</FNDEF_MIN>");
                    sb.AppendLine("<FNDEF_SEC>" + textBoxDefSec.Text + "</FNDEF_SEC>");
                    sb.AppendLine("</Data>");
                    if (_FromForm == "QUEUE")
                    {
                        //sb=sb.Replace("\r\n", "");
                        TBPGMBank_Deail_Modify.Do_Bank_Deail_ModifyAsync("", sb.ToString());
                        TBPGMBank_Deail_Modify.Do_Bank_Deail_ModifyCompleted += (s, args) =>
                        {
                            if (args.Error == null)
                            {
                                if (args.Result != "" && args.Result != null)
                                {
                                    //將新加入的節目回傳到節目表維護介面
                                    _PgmQueueProg.ProgID = this.textBoxProgID.Text;
                                    _PgmQueueProg.Seqno = args.Result;
                                    _PgmQueueProg.ProgName = this.textBoxProgName.Text;

                                    _PgmQueueProg.ProgDate = _date;
                                    //ProgDate = elem.Element("FDDATE").Value.ToString(),
                                    _PgmQueueProg.CHANNEL_ID = _channelid;
                                    _PgmQueueProg.BEG_TIME = this.textBoxBTime.Text;
                                    _PgmQueueProg.END_TIME = this.textBoxETime.Text;
                                    _PgmQueueProg.REPLAY = "0";
                                    _PgmQueueProg.EPISODE = this.textBoxEpisode.Text;
                                    _PgmQueueProg.TAPENO = QueryProg.FSFILE_NO;
                                    _PgmQueueProg.PROG_NAME = this.textBoxProgName.Text;
                                    _PgmQueueProg.LIVE = ((ComboBoxItem)comboBoxLive.SelectedItem).Tag.ToString();
                                    _PgmQueueProg.SIGNAL = ((ComboBoxItem)comboBoxSignal1.SelectedItem).Content.ToString();
                                    _PgmQueueProg.DEP = "0";
                                    _PgmQueueProg.ONOUT = "0";
                                    _PgmQueueProg.MEMO = this.textBoxMemo.Text;
                                    _PgmQueueProg.MEMO1 = this.textBoxMemo1.Text;
                                    _PgmQueueProg.CHANNEL_NAME = _channelName;
                                    _PgmQueueProg.FILETYPE = "";
                                    _PgmQueueProg.DEF_MIN = this.textBoxDefMin.Text;
                                    _PgmQueueProg.DEF_SEC = this.textBoxDefSec.Text;
                                    _PgmQueueProg.ProgDefSeg = this.textBoxDefSeg.Text;

                                    if (int.Parse(this.textBoxBTime.Text) >= 2400)
                                        _PgmQueueProg.RealBEG_TIME = (int.Parse(this.textBoxBTime.Text) - 2400).ToString("0000");
                                    else
                                        _PgmQueueProg.RealBEG_TIME = this.textBoxBTime.Text;

                                    if (int.Parse(this.textBoxETime.Text) >= 2400)
                                        _PgmQueueProg.RealEND_TIME = (int.Parse(this.textBoxETime.Text) - 2400).ToString("0000");
                                    else
                                        _PgmQueueProg.RealEND_TIME = this.textBoxETime.Text;
                                    _PgmQueueProg.BEG_TIME = this.textBoxBTime.Text;
                                    _PgmQueueProg.END_TIME = this.textBoxETime.Text;
                                    _HasInsert = true;
                                    this.DialogResult = true;
                                    //MessageBox.Show("修改完成");
                                }
                                else
                                {
                                    MessageBox.Show("插入節目失敗");
                                    return;
                                }
                            }
                        };
                    }
                    else // if (_FromForm == "COMBINEQUEUE")
                    {
                        //sb=sb.Replace("\r\n", "");
                        TBPGMBank_Deail_Modify.Do_Bank_Deail_ModifyAsync("", sb.ToString());
                        TBPGMBank_Deail_Modify.Do_Bank_Deail_ModifyCompleted += (s, args) =>
                        {
                            if (args.Error == null)
                            {
                                if (args.Result != "" && args.Result != null)
                                {
                                    //將新加入的節目回傳到節目表維護介面
                                    _PgmQueueProg.ProgID = this.textBoxProgID.Text;
                                    _PgmQueueProg.Seqno = args.Result;
                                    _PgmQueueProg.ProgName = this.textBoxProgName.Text;

                                    _PgmQueueProg.ProgDate = _date;
                                    //ProgDate = elem.Element("FDDATE").Value.ToString(),
                                    _PgmQueueProg.CHANNEL_ID = _channelid;
                                    _PgmQueueProg.BEG_TIME = this.textBoxBTime.Text;
                                    _PgmQueueProg.END_TIME = this.textBoxETime.Text;
                                    _PgmQueueProg.REPLAY = "0";
                                    _PgmQueueProg.EPISODE = this.textBoxEpisode.Text;
                                    _PgmQueueProg.TAPENO = QueryProg.FSFILE_NO;
                                    _PgmQueueProg.PROG_NAME = this.textBoxProgName.Text;
                                    _PgmQueueProg.LIVE = ((ComboBoxItem)comboBoxLive.SelectedItem).Tag.ToString();
                                    _PgmQueueProg.SIGNAL = ((ComboBoxItem)comboBoxSignal1.SelectedItem).Content.ToString();
                                    _PgmQueueProg.DEP = "0";
                                    _PgmQueueProg.ONOUT = "0";
                                    _PgmQueueProg.MEMO = this.textBoxMemo.Text;
                                    _PgmQueueProg.MEMO1 = this.textBoxMemo1.Text;
                                    _PgmQueueProg.CHANNEL_NAME = _channelName;
                                    _PgmQueueProg.FILETYPE = "";
                                    _PgmQueueProg.DEF_MIN = this.textBoxDefMin.Text;
                                    _PgmQueueProg.DEF_SEC = this.textBoxDefSec.Text;
                                    _PgmQueueProg.ProgDefSeg = this.textBoxDefSeg.Text;

                                    if (int.Parse(this.textBoxBTime.Text) >= 2400)
                                        _PgmQueueProg.RealBEG_TIME = (int.Parse(this.textBoxBTime.Text) - 2400).ToString("0000");
                                    else
                                        _PgmQueueProg.RealBEG_TIME = this.textBoxBTime.Text;

                                    if (int.Parse(this.textBoxETime.Text) >= 2400)
                                        _PgmQueueProg.RealEND_TIME = (int.Parse(this.textBoxETime.Text) - 2400).ToString("0000");
                                    else
                                        _PgmQueueProg.RealEND_TIME = this.textBoxETime.Text;
                                    _PgmQueueProg.BEG_TIME = this.textBoxBTime.Text;
                                    _PgmQueueProg.END_TIME = this.textBoxETime.Text;
                                    _HasInsert = true;
                                    this.DialogResult = true;
                                    //MessageBox.Show("修改完成");
                                }
                                else
                                {
                                    MessageBox.Show("插入節目失敗");
                                    return;
                                }
                            }
                        };
                    }
                    
                };
            //this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
        QueryProgFile QueryProg = new QueryProgFile();
        //List<QueryProgFile> GETQueryProgFile = new List<QueryProgFile>();
        private void buttonQueryProg_Click(object sender, RoutedEventArgs e)
        {
            PGM100_04 PGM100_04_Frm = new PGM100_04();
            PGM100_04_Frm._ChannelFileType = _ChannelFileType;
            PGM100_04_Frm.Show();
            PGM100_04_Frm.Closed += (s, args) =>
            {
                if (PGM100_04_Frm._FSPROG_ID != null)
                {
                    textBoxProgID.Text = PGM100_04_Frm._FSPROG_ID;
                    textBoxEpisode.Text = PGM100_04_Frm._FNEPISODE;
                    textBoxProgName.Text = PGM100_04_Frm._FSPGDNAME;
                    QueryProg.FSPROG_ID = PGM100_04_Frm._FSPROG_ID;
                    QueryProg.FNEPISODE = PGM100_04_Frm._FNEPISODE;
                    QueryProg.FSPGDNAME = PGM100_04_Frm._FSPGDNAME;
                    QueryProg.FSFILE_NO = PGM100_04_Frm._FSFILE_NO;
                    QueryProg.FSLENGTH = PGM100_04_Frm._FSLENGTH;
                    QueryProg.FCLOW_RES = PGM100_04_Frm._FCLOW_RES;
                    GetDefSetting();
                }
             };
        }

        private void buttonSetLouthKey_Click(object sender, RoutedEventArgs e)
        {
            PGM.PGM100_02 SetLouthKey_Frm = new PGM.PGM100_02();

            SetLouthKey_Frm.InLouthKeyString = textBoxMemo.Text;
            //SetLouthKey_Frm.AnalyLouthKeyString(textBoxMemo.Text);
            SetLouthKey_Frm.WorkChannelID = _channelid;
            SetLouthKey_Frm.Show();
            SetLouthKey_Frm.Closed += (s, args) =>
            {
                if (SetLouthKey_Frm.OutLouthKeyString != null)
                {
                    textBoxMemo.Text = SetLouthKey_Frm.OutLouthKeyString;
                }

            };
        }

        private void textBoxEpisode_LostFocus(object sender, RoutedEventArgs e)
        {
            GetDefSetting();
        }
    }
}

