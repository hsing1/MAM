﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.IO;
using System.Windows.Browser;
using System.Text;
using System.Xml.Linq;
using Telerik.Windows.Controls;

namespace PTS_MAM3.PGM
{
    public partial class PGM130 : Page
    {
        public PGM130()
        {
            InitializeComponent();
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBZCHANNEL_ALL = new WSPGMSendSQL.SendSQLSoapClient();

            ModuleClass.getModulePermission("");
            RadUpload1.UploadServiceUrl = @"../RadUploadHandler.ashx";
            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            sb.AppendLine("</Data>");
            SP_Q_TBZCHANNEL_ALL.Do_QueryAsync("[SP_Q_TBZCHANNEL_ALL]", sb.ToString(), ReturnXML);
            SP_Q_TBZCHANNEL_ALL.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {



                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);

                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {

                            if (ModuleClass.getModulePermission("P5" + elem.Element("ID").Value.ToString() + "008") == true)
                            {
                                ComboBoxItem TempComboBoxItem = new ComboBoxItem();
                                TempComboBoxItem.Content = elem.Element("NAME").Value.ToString();
                                TempComboBoxItem.Tag = elem.Element("ID").Value.ToString();

                                comboBoxChannel.Items.Add(TempComboBoxItem);
                            }

                        }
                    }
                }
            };      
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }
        string myFileName = string.Empty;
        private void ButtonImportLouthExcelFile_Click(object sender, RoutedEventArgs e)
        {
            // 建立開啟檔案對話方塊的執行個體。
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.Filter = "Excel(.xls, .xlsx)|*.xls;*.xlsx|所有的檔案(*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            ButtonExitChooseFile.Visibility = Visibility.Visible;
            // 只能選取一個檔案
            openFileDialog1.Multiselect = false;

            // 呼叫 ShowDialog 方法來顯示出開啟檔案對話方塊。
            bool? userClickedOK = openFileDialog1.ShowDialog();

            // 如果使用者按下『確定』按鈕則繼續處理。
            if (userClickedOK.GetValueOrDefault() == true)
            {

                myFileName = openFileDialog1.File.Name;

                // 開啟使用者所選取的檔案以便加以讀取。
                using (System.IO.Stream fileStream = openFileDialog1.File.OpenRead())
                {

                    // 以下的程式碼會將所要上傳之檔案的資料流轉換成一個字串。
                    string myFileString = string.Empty;
                    byte[] myFileArray = new byte[System.Convert.ToInt32(fileStream.Length)];
                    fileStream.Read(myFileArray, 0, myFileArray.Length);

                    // Converts the value of an array of 8-bit unsigned integers to its equivalent string representation that is encoded with base-64 digits.
                    myFileString = Convert.ToBase64String(myFileArray);

                    // 取得泛型處理常式的絕對 URL 。
                    // GetUrl 是一個使用者自訂類別。
                    UriBuilder ub = new UriBuilder(GetUrl.GetAbsoluteUrl("TakeUploadString.ashx"));

                    // 替泛型處理常式的絕對 URL 加上查詢字串。
                    ub.Query = string.Format("FileName={0}", HttpUtility.UrlEncode(openFileDialog1.File.Name));

                    // 建立 WebClient 物件。
                    WebClient wc = new WebClient();

                    // 呼叫 UploadStringAsync 方法來上傳檔案。
                    wc.UploadStringAsync(ub.Uri, myFileString);

                    wc.UploadStringCompleted += (s, args) =>
                    {
                        if (args.Error == null)
                        {
                            MessageBox.Show(args.Result);
                        }
                        else
                        {
                            MessageBox.Show("上傳檔案失敗");
                        }
                        //WriteExcel.WriteExcelSoapClient WExcel = new WriteExcel.WriteExcelSoapClient();
                        ////'WExcel.ImportExcelAsync(HttpUtility.UrlEncode(openFileDialog1.File.Name));
                        //WExcel.ImportExcelAsync(openFileDialog1.File.Name);

                        //WExcel.ImportExcelCompleted += (s1, args1) =>
                        //     {
                        //         if (args1.Error == null)
                        //         {

                        //             if (args1.Result == "ok")
                        //                 MessageBox.Show("新增成功");
                        //             else
                        //                 MessageBox.Show("新增失敗");


                        //         }
                        //     };           

                        //MessageBox.Show("檔案 " + myFileName + " 已經上傳完畢");
                    };



                    //wc.UploadStringCompleted += (s, args) =>
                    //{
                    //    if (args.Error == null)
                    //    {
                    //        if (args.Result != null && args.Result !="")
                    //        {
                    //            //if (args.Result == true)
                    //            //    QueryLouthList();
                    //            //else
                    //            //    MessageBox.Show("匯入資料錯誤!");

                    //        }
                    //    }
                    //};  

                }
            }
        }


        public string TransString(XElement Instring)
        {
            try
            {
                Instring.Value.ToString();
                return Instring.Value.ToString();
            }
            catch
            {
                return "";
            }
        }


        List<LouthKeyList> LouthKey = new List<LouthKeyList>();
        public void QueryLouthList()
        {
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPGM_LOUTH_KEY = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            sb.AppendLine("<FSCHANNEL_ID>" + ((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString() + "</FSCHANNEL_ID>");
            sb.AppendLine("</Data>");
            SP_Q_TBPGM_LOUTH_KEY.Do_QueryAsync("SP_Q_TBPGM_LOUTH_KEY", sb.ToString(), ReturnXML);
            //SP_Q_TBPGM_QUEUE.Do_QueryCompleted += new EventHandler<SendSQL.Do_QueryCompletedEventArgs>(SP_Q_TBPGM_QUEUE_Do_QueryCompleted);
            //AAA.Do_InsertAsync("", "", ReturnStr);
            SP_Q_TBPGM_LOUTH_KEY.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {
                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        LouthKey.Clear();
                        StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            LouthKey.Add(new LouthKeyList()
                            {
                                FSGROUP = TransString(elem.Element("FSGROUP")),
                                FSNAME = TransString(elem.Element("FSNAME")),
                                FSNO = TransString(elem.Element("FSNO")),

                                FSMEMO = TransString(elem.Element("FSMEMO")),
                                FSLAYEL = TransString(elem.Element("FSLAYEL")),
                                FSRULE = TransString(elem.Element("FSRULE"))
                            });
                        }

                        DataGridLouth.ItemsSource = null;
                        DataGridLouth.ItemsSource = LouthKey;

                    }
                    else
                    {
                        MessageBox.Show("沒有此頻道的Louth Key資料");
                    }
                }
            };    
        }

        private void ButtonQueryLouthExcelFile_Click(object sender, RoutedEventArgs e)
        {
            if (((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString() == "")
            {
                MessageBox.Show("請先選要查詢的頻道");
                return;
            }
            QueryLouthList();
        }

        public class GetUrl
        {
            public static string GetAbsoluteUrl(string strRelativePath)
            {
                if (string.IsNullOrEmpty(strRelativePath))
                    return strRelativePath;

                string strFullUrl;
                if (strRelativePath.StartsWith("http:", StringComparison.OrdinalIgnoreCase)
                  || strRelativePath.StartsWith("https:", StringComparison.OrdinalIgnoreCase)
                  || strRelativePath.StartsWith("file:", StringComparison.OrdinalIgnoreCase)
                  )
                {
                    // 已經是絕對路徑。
                    strFullUrl = strRelativePath;
                }
                else
                {
                    // 是相對路徑，必須轉換成絕對路徑。
                    strFullUrl = System.Windows.Application.Current.Host.Source.AbsoluteUri;
                    if (strFullUrl.IndexOf("ClientBin") > 0)
                        strFullUrl = strFullUrl.Substring(0, strFullUrl.IndexOf("ClientBin")) + "DataSource/" + strRelativePath;
                    else
                        strFullUrl = strFullUrl.Substring(0, strFullUrl.LastIndexOf("/") + 1) + "DataSource/" + strRelativePath;

                    //strFullUrl = "http://10.13.220.2/DataSource/TakeUploadString.ashx";

                }

                return strFullUrl;
            }
        }

        private void ButtonUploadFile_Click(object sender, RoutedEventArgs e)
        {
            if (((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString() == "")
            {
                MessageBox.Show("請先選要匯入的頻道");
                return;
            }
            RadUpload1.Visibility = Visibility.Visible;
            ButtonExitChooseFile.Visibility = Visibility.Visible;
        }

        private void RadUpload1_UploadFinished(object sender, RoutedEventArgs e)
        {
            try
            {
                string Uploadfilename="";
                foreach (RadUploadItem item in RadUpload1.Items)
                {
                    Uploadfilename = item.FileName;
                }

                WSPGMImportExcel.WriteExcelSoapClient ImportExcel = new WSPGMImportExcel.WriteExcelSoapClient();
                //string ReturnStr = "";

                ImportExcel.ImportExcelAsync(Uploadfilename, ((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString());
                //SP_Q_TBPGM_QUEUE.Do_QueryCompleted += new EventHandler<SendSQL.Do_QueryCompletedEventArgs>(SP_Q_TBPGM_QUEUE_Do_QueryCompleted);
                //AAA.Do_InsertAsync("", "", ReturnStr);
                ImportExcel.ImportExcelCompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (args.Result != null && args.Result != "")
                        {
                            if (args.Result != "匯入失敗" && args.Result != "Error")
                            {
                                MessageBox.Show(args.Result);
                                RadUpload1.Visibility = Visibility.Collapsed;
                                ButtonExitChooseFile.Visibility = Visibility.Collapsed;
                            }
                            else
                                MessageBox.Show("匯入失敗");
                        } //if (args.Result != null && args.Result !="")
                        else
                        {
                            MessageBox.Show("匯入失敗");
                        }
                    }
                };
            }
            catch
            {
                MessageBox.Show("匯入失敗!");
            }
        }

        private void RadUpload1_LostFocus(object sender, RoutedEventArgs e)
        {
            //RadUpload1.Visibility = Visibility.Collapsed;
        }

        private void ButtonExitChooseFile_Click(object sender, RoutedEventArgs e)
        {
            RadUpload1.Visibility = Visibility.Collapsed;
            ButtonExitChooseFile.Visibility = Visibility.Collapsed;
            
        }

        private void ButtonANalyLST_Click(object sender, RoutedEventArgs e)
        {
            // 建立開啟檔案對話方塊的執行個體。
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.Filter = "LST(.lst)|*.lst";
            openFileDialog1.FilterIndex = 2;
            ButtonExitChooseFile.Visibility = Visibility.Visible;
            // 只能選取一個檔案
            openFileDialog1.Multiselect = false;

            // 呼叫 ShowDialog 方法來顯示出開啟檔案對話方塊。
            bool? userClickedOK = openFileDialog1.ShowDialog();

            // 如果使用者按下『確定』按鈕則繼續處理。
            if (userClickedOK.GetValueOrDefault() == true)
            {

                myFileName = openFileDialog1.File.Name;

                // 開啟使用者所選取的檔案以便加以讀取。
                using (System.IO.Stream fileStream = openFileDialog1.File.OpenRead())
                {

                    // 以下的程式碼會將所要上傳之檔案的資料流轉換成一個字串。
                    string myFileString = string.Empty;
                    byte[] myFileArray = new byte[System.Convert.ToInt32(fileStream.Length)];
                    fileStream.Read(myFileArray, 0, myFileArray.Length);

                    // Converts the value of an array of 8-bit unsigned integers to its equivalent string representation that is encoded with base-64 digits.
                    myFileString = Convert.ToBase64String(myFileArray);

                    // 取得泛型處理常式的絕對 URL 。
                    // GetUrl 是一個使用者自訂類別。
                    UriBuilder ub = new UriBuilder(GetUrl.GetAbsoluteUrl("TakeUploadString.ashx"));

                    // 替泛型處理常式的絕對 URL 加上查詢字串。
                    ub.Query = string.Format("FileName={0}", HttpUtility.UrlEncode(openFileDialog1.File.Name));

                    // 建立 WebClient 物件。
                    WebClient wc = new WebClient();

                    // 呼叫 UploadStringAsync 方法來上傳檔案。
                    wc.UploadStringAsync(ub.Uri, myFileString);

                    wc.UploadStringCompleted += (s, args) =>
                    {
                        if (args.Error == null)
                        {
                            MessageBox.Show("匯入資料完成!");
                        }
                        else
                        {
                            MessageBox.Show("上傳檔案失敗");
                        }

                    };

                }
            }
        }

        private void ButtonDeleteLouthKey_Click(object sender, RoutedEventArgs e)
        {

            try
            {

                if (comboBoxChannel.SelectedIndex == -1)
                {
                    MessageBox.Show("請先選擇要刪除哪個頻道的Louth Key資料");
                    return;
                }

                //BusyMsg.IsBusy = true;

                WSPGMSendSQL.SendSQLSoapClient SP_D_TBPGM_LOUTH_KEY = new WSPGMSendSQL.SendSQLSoapClient();
                //string ReturnStr = "";

                StringBuilder sb = new StringBuilder();
                object ReturnXML = "";
                sb.AppendLine("<Data>");
                sb.AppendLine("<FSCHANNEL_ID>" + ((ComboBoxItem)comboBoxChannel.SelectedItem).Tag.ToString() + "</FSCHANNEL_ID>");
                sb.AppendLine("</Data>");
                SP_D_TBPGM_LOUTH_KEY.Do_InsertAsync("SP_D_TBPGM_LOUTH_KEY", sb.ToString(), ReturnXML);
                SP_D_TBPGM_LOUTH_KEY.Do_InsertCompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (args.Result == true)
                            MessageBox.Show("刪除成功");
                        else
                            MessageBox.Show("刪除失敗");
                    }
                };
            }
            catch
            {

                MessageBox.Show("刪除失敗!");
            }
        }

    }
}
