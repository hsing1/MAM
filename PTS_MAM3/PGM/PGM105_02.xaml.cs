﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace PTS_MAM3.PGM
{
    public partial class PGM105_02 : ChildWindow
    {
        public PGM105_02()
        {
            InitializeComponent();
        }
        public string _FSPROG_ID="";
        public string _FSPGDNAME="";
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridProgList.SelectedIndex < 0)
            {
                MessageBox.Show("請先選取節目資料");
                return;
            }

            _FSPROG_ID = QueryProg[dataGridProgList.SelectedIndex].ProgID;
            _FSPGDNAME = QueryProg[dataGridProgList.SelectedIndex].ProgName;
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        public void SendQuery()
        { 
            WSPGMSendSQL.SendSQLSoapClient SP_Q_TBPROG_M_BY_NAME = new WSPGMSendSQL.SendSQLSoapClient();
            //string ReturnStr = "";

            StringBuilder sb = new StringBuilder();
            object ReturnXML = "";
            sb.AppendLine("<Data>");
            sb.AppendLine("<FSPGMNAME>" + TransferTimecode.ReplaceXML(textBoxNewProgName.Text) + "</FSPGMNAME>");
            sb.AppendLine("</Data>");

            SP_Q_TBPROG_M_BY_NAME.Do_QueryAsync("SP_Q_TBPROG_M_BY_NAME", sb.ToString(), ReturnXML);
            SP_Q_TBPROG_M_BY_NAME.Do_QueryCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        byte[] byteArray = Encoding.Unicode.GetBytes(args.Result);
                        QueryProg.Clear();
                        //StringBuilder output = new StringBuilder();
                        // Create an XmlReader
                        XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                        var ltox = from str in doc.Elements("Datas").Elements("Data")
                                   select str;
                        foreach (XElement elem in ltox)
                        {
                            QueryProg.Add(new ProgList()
                            {
                                ProgID = elem.Element("FSPROG_ID").Value.ToString(),
                                ProgName = elem.Element("FSPGMNAME").Value.ToString()
                            });
                        }
                        dataGridProgList.ItemsSource = null;
                        dataGridProgList.ItemsSource = QueryProg;

                        //dataGridSaveList.ItemsSource = null;
                        //dataGridSaveList.ItemsSource = QueryProg;
                    }
                }
            };           
        }

        List<ProgList> QueryProg = new List<ProgList>();
        private void buttonQuery_Click(object sender, RoutedEventArgs e)
        {
            if (textBoxNewProgName.Text == "")
            {
                //MessageBox.Show("請輸入要查的節目");
                //return;
                textBoxNewProgName.Text = "%";
            }
            SendQuery();
        }

        private void dataGridProgList_RowDoubleClicked(object sender, DataGridDCTest.DataGridRowClickedArgs e)
        {
            //OKButton_Click(sender,e);
            if (dataGridProgList.SelectedIndex < 0)
            {
                MessageBox.Show("請先選取節目資料");
                return;
            }

            _FSPROG_ID = QueryProg[dataGridProgList.SelectedIndex].ProgID;
            _FSPGDNAME = QueryProg[dataGridProgList.SelectedIndex].ProgName;
            this.DialogResult = true;
        }
    }
}

