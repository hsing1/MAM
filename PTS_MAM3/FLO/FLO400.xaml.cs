﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;

namespace PTS_MAM3.FLO
{
    public partial class FLO400 : Page
    {
        FlowFieldCom.FlowFieldComSoapClient FFC = new FlowFieldCom.FlowFieldComSoapClient();
        public FLO400()
        {
            InitializeComponent();
            FFC.GetFlowIDfromFormIDCompleted += new EventHandler<FlowFieldCom.GetFlowIDfromFormIDCompletedEventArgs>(FFC_GetFlowIDfromFormIDCompleted);
        }

        void FFC_GetFlowIDfromFormIDCompleted(object sender, FlowFieldCom.GetFlowIDfromFormIDCompletedEventArgs e)
        {
            List<FlowFieldCom.Class_FlowIDfromFormID> CFList = new List<FlowFieldCom.Class_FlowIDfromFormID>();
            CFList = e.Result;
            if (CFList.Count > 0)
            {
                foreach (var item in CFList)
                {
                    textBox2.Text += item.displayname + ":" + item.ActiveID + "\n";
                }
            }
            else
                textBox2.Text = "查無單號";
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            textBox2.Text = "";
            FFC.GetFlowIDfromFormIDAsync(textBox1.Text.Trim());       
        }

    }
}
