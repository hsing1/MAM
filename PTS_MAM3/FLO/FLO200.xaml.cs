﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.FLO
{
    public partial class FLO200 : ChildWindow
    {
        flowWebService.FlowSoapClient client = new flowWebService.FlowSoapClient();
        int actorid;
        public FLO200(int actorid)
        {
            InitializeComponent();
            this.actorid = actorid;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (textBox1.Text.Trim() != "")
            {
                client.UpdateCommentByActoridAsync(actorid, UserClass.userData.FSUSER_ChtName, "不通過-" + textBox1.Text.Trim());
            }
            else
            { MessageBox.Show("請輸入意見!"); }
            client.UpdateCommentByActoridCompleted += (s, args) =>
                {
                    this.DialogResult = true;
                };
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

