﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Text;
using PTS_MAM3.FlowMAM;

namespace PTS_MAM3.FLO
{
    public partial class FLO300 : Page
    {
        //直接呼叫flow，無LOG 註解/修改 by Jarvis 20140207
       //flowWebService.FlowSoapClient FlowClinet = new flowWebService.FlowSoapClient();
        FlowMAMSoapClient clientFlow = new FlowMAMSoapClient();

        public FLO300()
        {
            InitializeComponent();
            
           // FlowClinet.NewFlowWithFieldCompleted += new EventHandler<flowWebService.NewFlowWithFieldCompletedEventArgs>(FlowClinet_NewFlowWithFieldCompleted);
            clientFlow.CallFlow_NewFlowWithFieldCompleted += new EventHandler<CallFlow_NewFlowWithFieldCompletedEventArgs>(clientFlow_CallFlow_NewFlowWithFieldCompleted);
        }

        void clientFlow_CallFlow_NewFlowWithFieldCompleted(object sender, CallFlow_NewFlowWithFieldCompletedEventArgs e)
        {
            if (e.Error == null && e.Result != "")
            {
                if (rdbPro.IsChecked == true)
                    MessageBox.Show("寫入成案流程引擎成功，案號：" + e.Result);
                else if (rdbStt.IsChecked == true)
                    MessageBox.Show("寫入送帶轉檔流程引擎成功，案號：" + e.Result);
                else if (rdbSttc.IsChecked == true)
                    MessageBox.Show("寫入送帶轉檔置換流程引擎成功，案號：" + e.Result);
            }
            else
                MessageBox.Show("寫入流程引擎失敗");   
        }

        //void FlowClinet_NewFlowWithFieldCompleted(object sender, flowWebService.NewFlowWithFieldCompletedEventArgs e)
        //{
        //    if (e.Error == null && e.Result!="")
        //    {
        //        if (rdbPro.IsChecked == true)
        //            MessageBox.Show("寫入成案流程引擎成功，案號：" + e.Result);
        //        else if (rdbStt.IsChecked == true)
        //            MessageBox.Show("寫入送帶轉檔流程引擎成功，案號：" + e.Result);
        //        else if (rdbSttc.IsChecked == true)
        //            MessageBox.Show("寫入送帶轉檔置換流程引擎成功，案號：" + e.Result);
        //    }
        //    else         
        //        MessageBox.Show("寫入流程引擎失敗");          
        //}   

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void btnYes_Click(object sender, RoutedEventArgs e)
        {
            if (Check_PROGMData() == false)      //檢查畫面上的欄位是否都填妥
                return;

            if (rdbPro.IsChecked == true)
                FlowPro();
            else if (rdbStt.IsChecked == true)
                FlowStt();
            else if (rdbSttc.IsChecked == true)
                FlowSttC();
            else
                MessageBox.Show("請選擇流程類型");  
        }

        //檢查必填欄位
        private Boolean Check_PROGMData()    //檢查畫面上的欄位是否都填妥
        {
            StringBuilder strMessage = new StringBuilder();

            if (tbxID.Text.Trim() == "")
                    strMessage.AppendLine("請輸入「單號」欄位");

            if (rdbProg.IsChecked == false && rdbPromo.IsChecked == false)
                    strMessage.AppendLine("請選擇「類型」欄位");

            if (tbxName.Text.Trim() == "")
                    strMessage.AppendLine("請輸入「節目或短帶名稱」欄位");

            if (tbxUserID.Text.Trim() == "")
                    strMessage.AppendLine("請輸入「建檔者ID」欄位");


            if (rdbPro.IsChecked == true && cbChannel.SelectedIndex == -1) 
                    strMessage.AppendLine("請選擇「建檔者頻道」欄位");   
             
            if (strMessage.ToString().Trim() != "")
            {
                MessageBoxResult result = MessageBox.Show(strMessage.ToString(), "提示訊息", MessageBoxButton.OK);
                return false;
            }
            else
                return true;
        }


        //送帶轉檔
        private void FlowStt()
        {          
            string FlowNote = "";

            if (rdbProg.IsChecked == true)
                FlowNote = "節目名稱-" + tbxName.Text.ToString().Trim();
            else if (rdbPromo.IsChecked == true)
                FlowNote = "短帶名稱-" + tbxName.Text.ToString().Trim();

            StringBuilder sb = new StringBuilder();
            sb.Append(@"<VariableCollection>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>FormId</name>");
            sb.Append(@"    <value>" + tbxID.Text.Trim() + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>NextXamlName</name>");
            sb.Append(@"    <value>/STT/STT100_05.xaml</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>Function01</name>");
            sb.Append(@"    <value>0</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>hid_SendTo</name>");      //預設是給1，不用審核的單子最後顯示才會是通過
            sb.Append(@"    <value>1</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>parameter01</name>");     //預設通過的狀態-Y(提出者是審核人)
            sb.Append(@"    <value>Y</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>USERID</name>");
            sb.Append(@"    <value>" + tbxUserID.Text.Trim() + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>FormInfo</name>");
            sb.Append(@"    <value>" + FlowNote + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>CancelForm</name>");
            sb.Append(@"    <value>CanCancel</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"</VariableCollection>");

            clientFlow.CallFlow_NewFlowWithFieldAsync(6, tbxUserID.Text.Trim(), sb.ToString());
           // FlowClinet.NewFlowWithFieldAsync(6, tbxUserID.Text.Trim(), sb.ToString());        
        }

        //成案
        private void FlowPro()
        {
            StringBuilder sb = new StringBuilder();
            string strParameter02 = "01";               //頻道別
            string strParameter03 = "N";                //緊急成案

            if (cbChannel.SelectedIndex != -1)          //頻道別              
                strParameter02 = ((ComboBoxItem)cbChannel.SelectedItem).Tag.ToString();

            if (rdbEY.IsChecked == true)                //緊急成案
                strParameter03 = "Y";

            sb.Append(@"<VariableCollection>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>FormId</name>");
            sb.Append(@"    <value>" + tbxID.Text.Trim() + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>NextXamlName</name>");
            sb.Append(@"    <value>/Proposal/PROPOSAL_Approve.xml</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>hid_SendTo</name>");      //預設是給1，不用審核的單子最後顯示才會是通過
            sb.Append(@"    <value>1</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>Function01</name>");
            sb.Append(@"    <value>0</value>");             //預設是給0，表單不修改，改成 1 此表單就可以修改
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>parameter02</name>");
            sb.Append(@"    <value>" + strParameter02 + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>parameter03</name>");
            sb.Append(@"    <value>" + strParameter03 + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>USERID</name>");
            sb.Append(@"    <value>" + tbxUserID.Text.Trim() + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>FormInfo</name>");
            sb.Append(@"    <value>" + "節目名稱-" + tbxName.Text.ToString().Trim() + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>CancelForm</name>");
            sb.Append(@"    <value>CanCancel</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"</VariableCollection>");

            clientFlow.CallFlow_NewFlowWithFieldAsync(3, tbxUserID.Text.Trim(), sb.ToString());
            //FlowClinet.NewFlowWithFieldAsync(3, tbxUserID.Text.Trim(), sb.ToString());
        }

        //送帶轉檔置換
        private void FlowSttC()
        {
            string FlowNote = "";

            if (rdbProg.IsChecked == true)
                FlowNote = "節目名稱-" + tbxName.Text.ToString().Trim();
            else if (rdbPromo.IsChecked == true)
                FlowNote = "短帶名稱-" + tbxName.Text.ToString().Trim();

            StringBuilder sb = new StringBuilder();

            sb.Append(@"<VariableCollection>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>FormId</name>");
            sb.Append(@"    <value>" + tbxID.Text.Trim() + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>NextXamlName</name>");
            sb.Append(@"    <value>/STT/STT500_05.xaml</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>hid_SendTo</name>");      //預設是給1，不用審核的單子最後顯示才會是通過
            sb.Append(@"    <value>1</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>Function01</name>");
            sb.Append(@"    <value>0</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>parameter01</name>");     //預設通過的狀態-Y(提出者是審核人)
            sb.Append(@"    <value>C</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>USERID</name>");
            sb.Append(@"    <value>" + tbxUserID.Text.Trim() + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>FormInfo</name>");
            sb.Append(@"    <value>" + FlowNote + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>WasBroadcasted</name>");
            sb.Append(@"    <value>" + false + "</value>"); //因為要判斷是否有排播過太麻煩，所以這裡就直接給false
            sb.Append(@"  </variable>");    
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>CancelForm</name>");
            sb.Append(@"    <value>CanCancel</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"</VariableCollection>");
            
            clientFlow.CallFlow_NewFlowWithFieldAsync(11, tbxUserID.Text.Trim(), sb.ToString());
            //FlowClinet.NewFlowWithFieldAsync(11, tbxUserID.Text.Trim(), sb.ToString());  
        }

        //成案
        private void rdbPro_Checked(object sender, RoutedEventArgs e)
        {
            if (rdbPro.IsChecked == true)
            {
                tbID.Text = "成案單單號";
                rdbProg.IsChecked = true;
                rdbPromo.IsChecked = false;
                tbChannel.Visibility = Visibility.Visible;
                cbChannel.Visibility = Visibility.Visible;
                tbEmergency.Visibility = Visibility.Visible;
                rdbEY.Visibility = Visibility.Visible;
            }
        }

        //送帶轉檔
        private void rdbStt_Checked(object sender, RoutedEventArgs e)
        {
            if (rdbStt.IsChecked == true)
            {
                tbID.Text = "送帶轉檔單單號";
                tbChannel.Visibility = Visibility.Collapsed;
                cbChannel.Visibility = Visibility.Collapsed;
                tbEmergency.Visibility = Visibility.Collapsed;
                rdbEY.Visibility = Visibility.Collapsed;
            }
        }

        //送帶轉檔置換
        private void rdbSttc_Checked(object sender, RoutedEventArgs e)
        {
            if (rdbSttc.IsChecked == true)
            {
                tbID.Text = "送帶轉檔置換單單號";
                tbChannel.Visibility = Visibility.Collapsed;
                cbChannel.Visibility = Visibility.Collapsed;
                tbEmergency.Visibility = Visibility.Collapsed;
                rdbEY.Visibility = Visibility.Collapsed;
            }
        }
    }
}
