﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.IO;
using System.Xml.Linq;
using PTS_MAM3.ArchiveData;
using System.Windows.Data;

namespace PTS_MAM3.DFForm
{
    public partial class DFQueryActivity : Page
    {
        private string CurrentGridSelectedId = "", CurrentGridSelectedClass = "";
        private string dataGrid1SelectedId;
        private FlowFieldCom.FlowFieldComSoapClient ComClient = new FlowFieldCom.FlowFieldComSoapClient();
        public DFQueryActivity()
        {
            InitializeComponent();
            dataGrid1.SelectionChanged += new SelectionChangedEventHandler(dataGrid1_SelectionChanged);
            ShowMainData("P", DateTime.Now.AddYears(-1).ToShortDateString(), DateTime.Now.ToShortDateString(), 0, 0);
            dpk_SDate.DateTimeText = DateTime.Now.AddYears(-1).ToShortDateString();
            dpk_EDate.DateTimeText = DateTime.Now.ToShortDateString();
        }
        #region 呼叫SVC然後Bind到dataGrid1上 void ShowMainData(string FlowStatus,string SDate,string EDate,int Flowid,int ProecssType)
        void ShowMainData(string FlowStatus,string SDate,string EDate,int Flowid,int ProecssType)
        {
            RadBusyIndicator1.IsBusy = true;
            ComClient.DFWaitToCheck_MainCompleted += (s, args) =>
                {
                    List<FlowFieldCom.DFWaitToCheck_Main_Class> mm = new List<FlowFieldCom.DFWaitToCheck_Main_Class>();
                    foreach (FlowFieldCom.DFWaitToCheck_Main_Class x in args.Result)
                    {
                        if (x.DF_QueryActivity_Status.Trim() == "已結案")
                        {
                            if (x.DF_hid_SendTo == "1")
                            { x.DF_QueryActivity_Status = "已結案-通過"; }
                            if (x.DF_hid_SendTo == "2")
                            { x.DF_QueryActivity_Status = "已結案-不通過"; }
                        }
                        if (x.DF_QueryActivity_Status.Trim() == "等待審核")
                        { x.DF_QueryActivity_Status = "等待審核"; }
                        mm.Add(x);
                    }
                    //宣告PagedCollectionView,將撈取的資料放入
                    PagedCollectionView pcv = new PagedCollectionView(mm.OrderByDescending(R => R.DF_QueryActivity_Enterdate));
                    //指定DataGrid以及DataPager的內容
                    dataGrid1.ItemsSource = pcv;
                    DataPager.Source = pcv;
                    //dataGrid1.ItemsSource = mm.OrderByDescending(R => R.DF_QueryActivity_Enterdate);//.Take(100); 
                    RadBusyIndicator1.IsBusy = false;
                };
            ComClient.DFWaitToCheck_MainAsync(UserClass.userData.FSUSER_ID, FlowStatus, SDate, EDate, 0, ProecssType);
        }
        #endregion


        void dataGrid1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                FlowFieldCom.DFWaitToCheck_Main_Class myobj = new FlowFieldCom.DFWaitToCheck_Main_Class();
                myobj = e.AddedItems[0] as FlowFieldCom.DFWaitToCheck_Main_Class;
                dataGrid1SelectedId = myobj.DF_QueryActivity_Activeid;
                //抽單 要 測試 先拿掉
                if ((myobj.DF_CancleForm == "CanCancel") && (((FlowFieldCom.DFWaitToCheck_Main_Class)dataGrid1.SelectedItem).DF_QueryActivity_Status == "等待審核"))
                { BT3.Visibility = Visibility.Visible; }
                else
                { BT3.Visibility = Visibility.Collapsed; }
                CurrentGridSelectedClass = myobj.DF_QueryActivity_Displayname;
                CurrentGridSelectedId = myobj.DF_Form_FormId;
                QueryActivityListBindingDeatail();
                ExtentTo.Value = dataGrid1.ActualWidth;
                dataGrid2.Height = LayoutRoot.ActualHeight / 2;
                dataGrid1.Height = LayoutRoot.ActualHeight / 2 - radToolBar1.ActualHeight;
                dataGrid2.Margin = new Thickness(0, LayoutRoot.ActualHeight / 2, 0, 0);
                ExtentDetail.Begin();
            }
            catch
            {
                dataGrid2.ItemsSource = null;
                ExtentDetail_Reverse.Begin();
            }
        }
           
        void QueryActivityListBindingMaster(string selectConditcion)
        {
            flowWebService.FlowSoapClient client = new flowWebService.FlowSoapClient();
            client.QueryActivityAsync(UserClass.userData.FSUSER_ID, selectConditcion , "", "", 0, 0, 0, 1);
            client.QueryActivityCompleted += (s, args) =>
            {
                
                string BackXML = args.Result;
                TextReader txr = new StringReader(BackXML);
                XDocument doc = XDocument.Load(txr);
                var ltox = from sel in doc.Elements("ActivityCollection").Elements("Activity")
                           select sel;
                foreach (XElement elem in ltox)
                {
                    MAMFunctions.DFQueryActivityXML QueryActivityClass = new MAMFunctions.DFQueryActivityXML();
                    QueryActivityClass.DF_Form_FormId ="";
                    QueryActivityClass.DF_QueryActivity_Activeid = elem.Element("activeid").Value;
                    QueryActivityClass.DF_QueryActivity_Applyname = elem.Element("Applyname").Value;
                    QueryActivityClass.DF_QueryActivity_Displayname = elem.Element("displayname").Value;
                    QueryActivityClass.DF_QueryActivity_Enterdate = elem.Element("enterDate").Value;
                    QueryActivityClass.DF_QueryActivity_Finishdate = elem.Element("finishDate").Value;
                    QueryActivityClass.DF_QueryActivity_Status = elem.Element("status").Value;
                    QueryActivityClass.DF_QueryActivity_Vesion = elem.Element("version").Value;
                }
            };
        }
        
        void QueryActivityListBindingDeatail()
        {
            ComClient.GetApprovalListDetailCompleted += (s, args) =>
                {
                    dataGrid2.ItemsSource = args.Result;
                };
            ComClient.GetApprovalListDetailAsync(Convert.ToInt32(dataGrid1SelectedId));
        }
        
        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string FlowStatus = "P", SDate = "", EDate = "";
            int Flowid = 0, ProcessType = 0;
            FlowStatus = ((ComboBoxItem)(cbo_FlowStatus.SelectedItem)).Tag.ToString();
            ProcessType = Convert.ToInt32(((ComboBoxItem)(cbo_ProcessType.SelectedItem)).Tag.ToString());
            SDate = dpk_SDate.DateTimeText;
            EDate = dpk_EDate.DateTimeText;
            ShowMainData(FlowStatus, SDate, EDate, Flowid, ProcessType);
        }

        private void Button_ViewForm_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentGridSelectedId == "" || CurrentGridSelectedId == null)
            { MessageBox.Show("請選擇表單"); }
            else
            { CheckWhatPage(CurrentGridSelectedClass, CurrentGridSelectedId); }
        }

        #region 開啟查詢表單
        private void CheckWhatPage(string Page, string FormId)
        {
            switch (Page)
            {
                case "成案表單":
                    Proposal.PRO100_03 PRO_View = new Proposal.PRO100_03(FormId);
                    PRO_View.Show();
                    break;
                case "入庫表單":
                    STO.STO100_03 STO100_03page = new STO.STO100_03(FormId,"Check");
                    STO100_03page.Show();
                    break;
                case "入庫置換表單":
                    STO.STO101_03 STO101_03page = new STO.STO101_03(FormId, "Check");
                    STO101_03page.Show();
                    break;
                case "託播單審核表單":
                    PGM.PGM110_02 PGM110_02_View = new PGM.PGM110_02();
                    PGM110_02_View._PromoBookingNO = FormId;
                    PGM110_02_View.QueryData();
                    PGM110_02_View.Show();
                    break;
                case "調用表單":
                    SRH.SRH138 SRH138 = new SRH.SRH138(FormId);
                    SRH138.Show();
                    //MessageBox.Show("功能尚未開放，請稍待系統更新!");
                    break;
                case "送帶轉檔表單":
                    STT.STT100_06 STT100_06page = new STT.STT100_06(FormId);
                    STT100_06page.Show();
                    break;
                case "送帶轉檔置換表單":
                    STT.STT500_06 STT500_06page = new STT.STT500_06(FormId);
                    STT500_06page.Show();
                    break;
            }
        }
        #endregion 結束查詢表單開啟
        //抽單按鈕處發事件
        private void BT3_Click(object sender, RoutedEventArgs e)
        {
            var result = MessageBox.Show("確定抽單?", "確認視窗", MessageBoxButton.OKCancel);
            if (result == MessageBoxResult.OK)
            {
                WSFlowCancel.WSFlowCancelSoapClient cancelClient = new WSFlowCancel.WSFlowCancelSoapClient();
                switch (((FlowFieldCom.DFWaitToCheck_Main_Class)(dataGrid1.SelectedItem)).DF_QueryActivity_Displayname)
                {
                    case "成案表單":
                        cancelClient.PRO100CancelCompleted += (s, args) =>
                            { CancelMessage(args.Result); };
                        cancelClient.PRO100CancelAsync(((FlowFieldCom.DFWaitToCheck_Main_Class)(dataGrid1.SelectedItem)).DF_QueryActivity_Activeid, ((FlowFieldCom.DFWaitToCheck_Main_Class)(dataGrid1.SelectedItem)).DF_Form_FormId, UserClass.userData.FSUSER_ID);
                        break;
                    case "入庫表單":
                        cancelClient.STO100CancelCompleted += (s, args) =>
                            { CancelMessage(args.Result); };
                        cancelClient.STO100CancelAsync(((FlowFieldCom.DFWaitToCheck_Main_Class)(dataGrid1.SelectedItem)).DF_QueryActivity_Activeid, ((FlowFieldCom.DFWaitToCheck_Main_Class)(dataGrid1.SelectedItem)).DF_Form_FormId, UserClass.userData.FSUSER_ID);
                        break;
                    case "入庫置換表單":
                        cancelClient.STO101CancelCompleted += (s, args) =>
                            { CancelMessage(args.Result); };
                        cancelClient.STO101CancelAsync(((FlowFieldCom.DFWaitToCheck_Main_Class)(dataGrid1.SelectedItem)).DF_QueryActivity_Activeid, ((FlowFieldCom.DFWaitToCheck_Main_Class)(dataGrid1.SelectedItem)).DF_Form_FormId, UserClass.userData.FSUSER_ID);
                        break;
                    case "託播單審核表單":
                        cancelClient.PGM110CancelCompleted += (s, args) =>
                            { CancelMessage(args.Result); };
                        cancelClient.PGM110CancelAsync(((FlowFieldCom.DFWaitToCheck_Main_Class)(dataGrid1.SelectedItem)).DF_QueryActivity_Activeid, ((FlowFieldCom.DFWaitToCheck_Main_Class)(dataGrid1.SelectedItem)).DF_Form_FormId, UserClass.userData.FSUSER_ID);
                        break;
                    case "調用表單":
                        cancelClient.BOK100CancelCompleted += (s, args) =>
                            { CancelMessage(args.Result); };
                        cancelClient.BOK100CancelAsync(((FlowFieldCom.DFWaitToCheck_Main_Class)(dataGrid1.SelectedItem)).DF_QueryActivity_Activeid, ((FlowFieldCom.DFWaitToCheck_Main_Class)(dataGrid1.SelectedItem)).DF_Form_FormId, UserClass.userData.FSUSER_ID);
                        break;
                    case "送帶轉檔表單":
                        cancelClient.BRO100CancelCompleted += (s, args) =>
                            { CancelMessage(args.Result); };
                        cancelClient.BRO100CancelAsync(((FlowFieldCom.DFWaitToCheck_Main_Class)(dataGrid1.SelectedItem)).DF_QueryActivity_Activeid, ((FlowFieldCom.DFWaitToCheck_Main_Class)(dataGrid1.SelectedItem)).DF_Form_FormId, UserClass.userData.FSUSER_ID);
                        break;
                    case "送帶轉檔置換表單":
                        cancelClient.BRO200CancelCompleted += (s, args) =>
                            { CancelMessage(args.Result); };
                        cancelClient.BRO200CancelAsync(((FlowFieldCom.DFWaitToCheck_Main_Class)(dataGrid1.SelectedItem)).DF_QueryActivity_Activeid, ((FlowFieldCom.DFWaitToCheck_Main_Class)(dataGrid1.SelectedItem)).DF_Form_FormId, UserClass.userData.FSUSER_ID);
                        break;
                }
            }
        }
        private void CancelMessage(bool Result)
        {
            if (Result == true)
            {
                ShowMainData("P", "", "", 0, 0);
                MessageBox.Show("抽單成功");
                BT3.Visibility = Visibility.Collapsed;
            }
            else
            { MessageBox.Show("抽單失敗!"); }
        }
    }
}
