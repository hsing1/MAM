﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Xml.Linq;
using System.IO;
using Telerik.Windows.Controls;
using PTS_MAM3.MAMFunctions;
using System.Windows.Data;
using System.Windows.Browser;

namespace PTS_MAM3
{
    public partial class DFGetTTL : Page
    {
        public FlowFieldCom.FlowFieldComSoapClient client = new FlowFieldCom.FlowFieldComSoapClient();
        public List<FlowFieldCom.DFWaitForCheck_Class> GetTTLList = new List<FlowFieldCom.DFWaitForCheck_Class>();
        public DFGetTTL()
        {
            InitializeComponent();
            client.ReturnIPAddressCompleted += (s, args) =>
                { MessageBox.Show(args.Result.ToString()); };
            if (UserClass.userData.FSUSER_ID != null)
            {
                DoDFWaitYourCheck();
            }
            client.DFWaitYourCheckCompleted += (s, args) =>
            {
                if (args.Error != null)
                {
                    MessageBox.Show(args.Error.Message);
                }
                else
                {
                    GetTTLList = args.Result;
                    dataGrid1.ItemsSource = GetTTLList;
                    busyIndicator1.IsBusy = false;

                    NextXamlName = "";
                    PageFormId = "";
                }
            };
        }

        void DoDFWaitYourCheck()
        {
            busyIndicator1.IsBusy = true;
            client.DFWaitYourCheckAsync(UserClass.userData.FSUSER_ID);
        }

        #region 開啟查詢表單
        private void CheckWhatPage(string Page,string FormId)
        {
            switch (Page)
            {
                case "/SRH/SRH137.xaml_Sign":
                    SRH.SRH137 SRH137_Page = new SRH.SRH137(FormId);
                    SRH137_Page.intFlowID = intFlowID;
                    SRH137_Page.intProcessID = ProcessId;
                    SRH137_Page.Show();
                    SRH137_Page.Closing += (s, args) =>
                    {
                        if (SRH137_Page.DialogResult == true)
                        {
                            DoDFWaitYourCheck();
                        }
                    };
                    break;
                case "/SRH/SRH136.xaml_Sign":
                    SRH.SRH136 SRH136_Page = new SRH.SRH136(FormId);
                    SRH136_Page.intFlowID = intFlowID;
                    SRH136_Page.intProcessID = ProcessId;
                    SRH136_Page.Show();
                    SRH136_Page.Closing += (s, args) =>
                    {
                        if (SRH136_Page.DialogResult == true)
                        {
                            DoDFWaitYourCheck();
                        }
                    };
                    break;
                case "/STT/STT500_05.xaml_Sign":
                    PTS_MAM3.STT.STT500_05 STT500_05_Page_sign = new STT.STT500_05(FormId);
                    STT500_05_Page_sign.intFlowID = intFlowID;
                    STT500_05_Page_sign.intProcessID = ProcessId;
                    STT500_05_Page_sign.Show();
                    STT500_05_Page_sign.Closing += (s, args) =>
                    {
                        if (STT500_05_Page_sign.DialogResult == true)
                        {
                            DoDFWaitYourCheck();
                        }
                    };
                    break;
                case "/STT/STT100_05.xaml_Sign":
                    PTS_MAM3.STT.STT100_05 STT100_05_Page = new STT.STT100_05(FormId);
                    STT100_05_Page.intFlowID = intFlowID;
                    STT100_05_Page.intProcessID = ProcessId;
                    STT100_05_Page.Show();
                    STT100_05_Page.Closing += (s, args) =>
                    {
                        if (STT100_05_Page.DialogResult == true)
                        {
                            DoDFWaitYourCheck();
                        }
                    };
                    break;
                case "/STO/STO101_01.xaml_Sign":
                    STO.STO101_06 STO101_06_Page = new STO.STO101_06(FormId);
                    STO101_06_Page.intFlowID = intFlowID;
                    STO101_06_Page.intProcessID = ProcessId;
                    STO101_06_Page.Show();
                    STO101_06_Page.Closing += (s, args) =>
                        {
                            if (STO101_06_Page.DialogResult == true)
                            {
                                DoDFWaitYourCheck();
                            }
                        };
                    break;
                case "/STO/STO100_01.xaml_Sign":
                    STO.STO100_06 STO100_06page = new STO.STO100_06(FormId);
                    STO100_06page.intFlowID = intFlowID;
                    STO100_06page.intProcessID = ProcessId;
                    STO100_06page.Closing += (s, args) =>
                        {
                            if (STO100_06page.DialogResult == true)
                            {
                                DoDFWaitYourCheck();
                            }
                        };
                    STO100_06page.Show();
                    break;
                //case "/SRH/SRH136.xaml_Sign":
                //    SRH.SRH136 srh136=new SRH.SRH136(FormId);
                //    srh136.intFlowID = intFlowID;
                //    srh136.intProcessID = ProcessId;
                //    srh136.Closing += (s, args) =>
                //    {
                //        if (srh136.DialogResult == true)
                //        {
                //            DoDFWaitYourCheck();
                //        }
                //    };
                //    srh136.Show();
                //    break;
                case "/Proposal/PROPOSAL_Approve.xml_Sign":
                    Proposal.PROPOSAL_Edit PRO_Approve = new Proposal.PROPOSAL_Edit(FormId);
                    PRO_Approve.intProcessID = ProcessId;
                    PRO_Approve.Show();
                    PRO_Approve.Closing += (s, args) =>
                        {
                            if (PRO_Approve.DialogResult == true)
                            { 
                                DoDFWaitYourCheck();
                            }
                        };
                    break;
                case "/PGM/PGM110_01.xaml_Sign":
                    PGM.PGM110_01 PGM110_01 =new PGM.PGM110_01();
                    PGM110_01.intProcessID =ProcessId;
                    PGM110_01.intFlowID = intFlowID;
                    PGM110_01._PromoBookingNO = FormId;
                    PGM110_01.Show();
                    PGM110_01.Closing += (s, args) =>
                    {
                        if (PGM110_01.DialogResult == true)
                        {
                            DoDFWaitYourCheck();
                        }
                    };
                    break;
                case "/SRH/SRH136.xaml_View":
                    SRH.SRH138 SRH138_View = new SRH.SRH138(FormId);
                    SRH138_View.Show();
                    break;
                case "/STT/STT500_05.xaml_View":
                    PTS_MAM3.STT.STT500_06 STT500_05_Page_View = new STT.STT500_06(FormId);
                    STT500_05_Page_View.Show();
                    break;
                case "/STT/STT100_05.xaml_View":
                    PTS_MAM3.STT.STT100_06 STT100_06_Page_View = new STT.STT100_06(FormId);
                    STT100_06_Page_View.Show();
                    break;
                case "/Proposal/PROPOSAL_Approve.xml_View":
                    Proposal.PRO100_03 PRO_View = new Proposal.PRO100_03(FormId);
                    //PRO_View.intProcessID = ProcessId;
                    PRO_View.Show();
                    break;
                case "/STO/STO100_01.xaml_View":
                    STO.STO100_03 STO100_03_View = new STO.STO100_03(FormId,"");
                    STO100_03_View.Show();                    
                    break;
                case "/STO/STO101_01.xaml_View":
                    STO.STO101_03 STO101_03_View = new STO.STO101_03(FormId, "");
                    STO101_03_View.Show();
                    break;
                case "/PGM/PGM110_01.xaml_View":
                    PGM.PGM110_02 PGM110_02_View = new PGM.PGM110_02();
                    PGM110_02_View._PromoBookingNO = FormId;
                    PGM110_02_View.QueryData();
                    PGM110_02_View.Show();
                    break;
                case "/Proposal/PROPOSAL_Approve.xml_Modify":
                    Proposal.PROPOSAL_Edit PRO_Edit = new Proposal.PROPOSAL_Edit(FormId);
                    PRO_Edit.Show();
                    break;

                case "GetAgentForm_Sign":
                    HtmlPage.Window.Navigate(new Uri(PTSMAMResource.GetAgentForm_Sign + FormId), "_blank", "height=300,width=400,top=100,left=100,toolbar=0,statusbar=0,menubar=0,scrollbar=0,resizable=yes");
                    break;
            }
                
        }
        #endregion 結束查詢表單開啟
        private void button1_Click(object sender, RoutedEventArgs e)
        {
        }
        private string NextXamlName = "", PageFormId = "", strFunction01 = "";
        private int ProcessId = 0;
        private int intFlowID;
        private void dataGrid1_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (dataGrid1.SelectedIndex >= 0)
            {
                if ((((FlowFieldCom.DFWaitForCheck_Class)dataGrid1.SelectedItem).flowid) != "")
                {
                    intFlowID = Convert.ToInt32((((FlowFieldCom.DFWaitForCheck_Class)dataGrid1.SelectedItem).flowid));
                }
                else
                { intFlowID = 0; }

                
                if (((FlowFieldCom.DFWaitForCheck_Class)dataGrid1.SelectedItem).Ttlid == "" || ((FlowFieldCom.DFWaitForCheck_Class)dataGrid1.SelectedItem).Ttlid == null)
                {
                    //設定Function01按鈕不可作用
                    strFunction01 = "0";
                    CheckFunctionIsWorkOrNot(strFunction01);
                    NextXamlName = "GetAgentForm";
                    PageFormId = ((FlowFieldCom.DFWaitForCheck_Class)dataGrid1.SelectedItem).FormUrl;
                    
                }
                else
                {
                    CheckFunctionIsWorkOrNot(((FlowFieldCom.DFWaitForCheck_Class)dataGrid1.SelectedItem).Function01);
                    NextXamlName = ((FlowFieldCom.DFWaitForCheck_Class)dataGrid1.SelectedItem).NextXamlName;
                    PageFormId = ((FlowFieldCom.DFWaitForCheck_Class)dataGrid1.SelectedItem).FormId;
                    ProcessId = Convert.ToInt32(((FlowFieldCom.DFWaitForCheck_Class)dataGrid1.SelectedItem).Receiveid);
                }
            }
        }

        private void button1_Click_1(object sender, RoutedEventArgs e)
        {
            if (NextXamlName == "" || PageFormId == "")
            { MessageBox.Show("請選擇欲簽核的表單"); }
            else
            { 
                CheckWhatPage(NextXamlName + "_Sign", PageFormId);
            }
        }

        private void ViewDetail_Click(object sender, RoutedEventArgs e)
        {
            if (NextXamlName == "" || PageFormId == "")
            { MessageBox.Show("請選取要查看的表單"); }
            else
            { CheckWhatPage(NextXamlName + "_View", PageFormId); }
        }

        private void Modify_Function01_Click(object sender, RoutedEventArgs e)
        {
            if (NextXamlName == "" || PageFormId == "")
            { MessageBox.Show("請選取要修改的表單"); }
            else
            { CheckWhatPage(NextXamlName + "_Modify", PageFormId); }
        }

        void CheckFunctionIsWorkOrNot(string Function01)
        {
            if (Function01 == "1")
            { btnFunction01.Visibility = System.Windows.Visibility.Visible; }
            else
            { btnFunction01.Visibility = System.Windows.Visibility.Collapsed; }
        }

        private void ReFresh_Click(object sender, RoutedEventArgs e)
        {
            DoDFWaitYourCheck();
        }

        private void btnBatch_Click(object sender, RoutedEventArgs e)
        {
        }




        List<string> BatchList = new List<string>();
        private void checkbox1_Click(object sender, RoutedEventArgs e)
        {
            
            if (((CheckBox)sender).IsChecked == true)
            {
                bool existStatus = false;
                foreach (string x in BatchList)
                {
                    if(x==((CheckBox)sender).Content.ToString())
                    {existStatus=true;}
                }
                if (existStatus == false)
                { BatchList.Add(((CheckBox)sender).Content.ToString()); }
            }
            else
            {
                bool existStatus = false;
                foreach (string x in BatchList)
                {
                    if (x == ((CheckBox)sender).Content.ToString())
                    { existStatus = true; }
                }
                if (existStatus == true)
                { BatchList.Remove(((CheckBox)sender).Content.ToString()); }
            }
        }
    }
}
