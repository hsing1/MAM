﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Xml.Linq;
using System.IO;
using Telerik.Windows.Controls;
using PTS_MAM3.MAMFunctions;
using System.Windows.Data;
using System.Windows.Browser;


namespace PTS_MAM3
{
    public partial class FLOAgent_List : Page
    {

        WSFlowAgent.FlowAgentSoapClient myFlowAgentClient = new WSFlowAgent.FlowAgentSoapClient();
        public List<WSFlowAgent.Class_Flow_Agent_Flow> myList = new List<WSFlowAgent.Class_Flow_Agent_Flow>();
        PTS_MAM3.FLO.FLOAgent_Add addPage = new FLO.FLOAgent_Add();
        string selected_aid = "";
        public FLOAgent_List()
        {
            InitializeComponent();
            myFlowAgentClient.GetFlowAgentListCompleted += new EventHandler<WSFlowAgent.GetFlowAgentListCompletedEventArgs>(myFlowAgentClient_GetFlowAgentListCompleted);
            addPage.Closed += new EventHandler(addPage_Closed);
            myFlowAgentClient.DelFlowAgentCompleted += new EventHandler<WSFlowAgent.DelFlowAgentCompletedEventArgs>(myFlowAgentClient_DelFlowAgentCompleted);
            myFlowAgentClient.GetFlowAgentListAsync(UserClass.userData.FSUSER_ID);
        }

        void myFlowAgentClient_DelFlowAgentCompleted(object sender, WSFlowAgent.DelFlowAgentCompletedEventArgs e)
        {
            myFlowAgentClient.GetFlowAgentListAsync(UserClass.userData.FSUSER_ID);
        }

        void addPage_Closed(object sender, EventArgs e)
        {
            if (addPage.DialogResult == true)
                myFlowAgentClient.GetFlowAgentListAsync(UserClass.userData.FSUSER_ID);
        }

        void myFlowAgentClient_GetFlowAgentListCompleted(object sender, WSFlowAgent.GetFlowAgentListCompletedEventArgs e)
        {
            dataGrid1.ItemsSource = e.Result;
        }

        private void dataGrid1_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            //if (((WSFlowAgent.Class_Flow_Agent_Flow)dataGrid1.SelectedItem).aid != null)
            //    selected_aid = ((WSFlowAgent.Class_Flow_Agent_Flow)dataGrid1.SelectedItem).aid;
            //else
            //    selected_aid = "";
        }

        
        private void newAgent_Click(object sender, RoutedEventArgs e)
        {
            addPage.Show();
        }


        private void delAgent_Click(object sender, RoutedEventArgs e)
        {
            if (dataGrid1.SelectedItem != null)
                selected_aid = ((WSFlowAgent.Class_Flow_Agent_Flow)dataGrid1.SelectedItem).aid;
            else
                selected_aid = "";
            if (selected_aid != "")
            {
                //MessageBox.Show(selected_aid);
                myFlowAgentClient.DelFlowAgentAsync(selected_aid, UserClass.userData.FSUSER_ID);
            }
            else
            { MessageBox.Show("請選擇欲刪除的代理人"); }
        }







    }
}
