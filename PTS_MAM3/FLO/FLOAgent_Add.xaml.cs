﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.FLO
{
    public partial class FLOAgent_Add : ChildWindow
    {
        WSFlowAgent.FlowAgentSoapClient myAgnet = new WSFlowAgent.FlowAgentSoapClient();
        WSUserAdmin.WSUserAdminSoapClient clientUserAdmin = new WSUserAdmin.WSUserAdminSoapClient();
        List<WSUserAdmin.Class_Dept> DeptList = new List<WSUserAdmin.Class_Dept>();
        List<WSUserAdmin.UserStruct> ListBox1_UsersList = new List<WSUserAdmin.UserStruct>();
        string selAgentUser = "";
        public FLOAgent_Add()
        {
            InitializeComponent();
            sDP.DateTimeText = DateTime.Today.ToShortDateString();
            eDP.DateTimeText = DateTime.Today.AddDays(1).ToShortDateString();
            myAgnet.InsFlowAgentCompleted += new EventHandler<WSFlowAgent.InsFlowAgentCompletedEventArgs>(myAgnet_InsFlowAgentCompleted);
            clientUserAdmin.GetDeptListAllCompleted += new EventHandler<WSUserAdmin.GetDeptListAllCompletedEventArgs>(clientUserAdmin_GetDeptListAllCompleted);
            clientUserAdmin.GetUserListByTreeViewDeptCompleted += new EventHandler<WSUserAdmin.GetUserListByTreeViewDeptCompletedEventArgs>(clientUserAdmin_GetUserListByTreeViewDeptCompleted);
            clientUserAdmin.GetDeptListAllAsync();
        }

        private void comboBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            preLoadUserListBox1(((WSUserAdmin.Class_Dept)(((ComboBoxItem)comboBox1.SelectedItem).Tag)).FNDEP_ID);
            listBox1.Items.Clear();
        }

        private void listBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }

        private void preLoadUserListBox1(int FNDEP_ID)
        {
            ListBox1_UsersList.Clear();
            clientUserAdmin.GetUserListByTreeViewDeptAsync(FNDEP_ID.ToString());
        }

        void clientUserAdmin_GetUserListByTreeViewDeptCompleted(object sender, WSUserAdmin.GetUserListByTreeViewDeptCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    ListBox1_UsersList = e.Result.ToList();
                }
                foreach (var item in ListBox1_UsersList)
                {
                    ListBoxItem myLBI = new ListBoxItem();
                    myLBI.Tag = item;
                    myLBI.Content = item.FSUSER_ChtName;
                    if (item.FCACTIVE == "1")
                    {
                        listBox1.Items.Add(myLBI);
                    }
                }
            }
        }

        void clientUserAdmin_GetDeptListAllCompleted(object sender, WSUserAdmin.GetDeptListAllCompletedEventArgs e)
        {
            DeptList = e.Result.ToList();
            foreach (var item in DeptList)
            {
                ComboBoxItem cbi1 = new ComboBoxItem();
                cbi1.Content = item.FSFullName_ChtName;
                cbi1.Tag = item;
                comboBox1.Items.Add(cbi1);
            }
        }

        void myAgnet_InsFlowAgentCompleted(object sender, WSFlowAgent.InsFlowAgentCompletedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            DateTime dtSDate,dtEDate;
            bool bDataCorrect = true;
            #region 填入資訊
            string strHandleName = "";
            string strAgentName = "";
            string strSdate = "";
            string strEdate = "";
            string strEFlowid = "";
            #region 串部門的
            if (CB3.IsChecked == true)
            {
                if (strEFlowid == "")
                    strEFlowid += "3";
                else
                    strEFlowid += ",3";
            }
            if (CB6.IsChecked == true)
            {
                if (strEFlowid == "")
                    strEFlowid += "6";
                else
                    strEFlowid += ",6";
            }
            if (CB7.IsChecked == true)
            {
                if (strEFlowid == "")
                    strEFlowid += "7";
                else
                    strEFlowid += ",7";
            }
            if (CB8.IsChecked == true)
            {
                if (strEFlowid == "")
                    strEFlowid += "8";
                else
                    strEFlowid += ",8";
            }
            if (CB9.IsChecked == true)
            {
                if (strEFlowid == "")
                    strEFlowid += "9";
                else
                    strEFlowid += ",9";
            }
            if (CB10.IsChecked == true)
            {
                if (strEFlowid == "")
                    strEFlowid += "10";
                else
                    strEFlowid += ",10";
            }
            if (CB11.IsChecked == true)
            {
                if (strEFlowid == "")
                    strEFlowid += "11";
                else
                    strEFlowid += ",11";
            }
            if (CB0.IsChecked == true)
                strEFlowid = "0";
            #endregion

            strSdate = sDP.DateTimeText + " 00:00";
            strEdate = eDP.DateTimeText + " 23:59";
            dtSDate = Convert.ToDateTime(strSdate);
            dtEDate = Convert.ToDateTime(strEdate);
            strHandleName = UserClass.userData.FSUSER_ID;
            ListBoxItem LBI = new ListBoxItem();
            LBI = (ListBoxItem)listBox1.SelectedItem;
            if (LBI != null)
            { strAgentName = ((WSUserAdmin.UserStruct)LBI.Tag).FSUSER_ID; }
            else
            { strAgentName = ""; }
            #endregion
            if (dtEDate < dtSDate)
            { MessageBox.Show("日期區間不正確"); bDataCorrect = false; }
            if (strAgentName == "")
            { MessageBox.Show("請選擇代理人"); bDataCorrect = false; }
            if (strEFlowid == "")
            { MessageBox.Show("請選擇代理表單"); bDataCorrect = false; }

            if (bDataCorrect==true)
            {
                myAgnet.InsFlowAgentAsync(strHandleName, strAgentName, strSdate, strEdate, strEFlowid);    
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }


    }
}

