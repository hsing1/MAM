﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using PTS_MAM3.WSTSM_GetFileInfo;
using PTS_MAM3.WSBookingFunction;
using System.Xml.Linq;

namespace PTS_MAM3.STO
{
    public partial class STO200 : ChildWindow
    {
        public STO200()
        {
            InitializeComponent();
        }

        public string _fsfile_no { get; set; }

        //建立Video物件
        PreviewPlayer.SLVideoPlayer player = new PreviewPlayer.SLVideoPlayer("", 0);

        public STO200(string fsfile_no)
        {

           
            InitializeComponent();

            
            _fsfile_no = fsfile_no;
           initVideoPlayer();
           // player.HTML_SetURL("http://10.13.220.2/Media/Lv/ptstv/2011/2011089/G201108900040001.wmv", 0);

            this.GrdSLVideoPlayer.Child = player; 
            initKeyframe(); 
            preLoadDGTBLOG_VIDEO_D(); 
        }

        /// <summary>
        /// 初始化video player 
        /// </summary>
        private void initVideoPlayer()
        {
            string startTimeCode = string.Empty; 
            //取得start timecode
            WSBookingFunctionSoapClient booking = new WSBookingFunctionSoapClient();
            ServiceTSMSoapClient tsm = new ServiceTSMSoapClient();

            booking.GetFileStartTimeCodeCompleted += (s, args) =>
            {
                if ((args.Error != null) || String.IsNullOrEmpty(args.Result))
                {
                }
                else
                {
                    XDocument xDoc = XDocument.Parse(args.Result);
                    try
                    {
                        startTimeCode = xDoc.Element("Datas").Element("Data").Element("FSBEG_TIMECODE").Value;
                        tsm.GetHttpPathByFileIDAsync(_fsfile_no, FileID_MediaType.LowVideo);
                    }
                    catch (Exception ex)
                    {
                        // not complete yet.
                    }
               
                }
            };


            booking.GetFileStartTimeCodeAsync("<Data><FSFILE_NO>" + _fsfile_no + "</FSFILE_NO></Data>"); 


            tsm.GetHttpPathByFileIDCompleted += (s, args) =>
            {
                if ((args.Error != null) || String.IsNullOrEmpty(args.Result))
                {
                }
                else
                {                

                    player.HTML_SetURL(args.Result, TransferTimecode.timecodetoframe(startTimeCode));                                         
                }

            };

            
        }



        /// <summary>
        /// 初始化Keyframe List
        /// </summary>
        private void initKeyframe() {
            WSVideoMetaEdit.WSVideoMetaEditSoapClient WSVideoObj = new WSVideoMetaEdit.WSVideoMetaEditSoapClient();
            WSVideoObj.fnGetTBLOG_VIDEO_KEYFRAMECompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        this.lbxKeframe.ItemsSource = args.Result; 
                    }
                }                
            };

            WSVideoObj.fnGetTBLOG_VIDEO_KEYFRAMEAsync(_fsfile_no); 

        }


        /// <summary>
        /// 載入影像段落資料至DataGrid
        /// </summary>
        private void preLoadDGTBLOG_VIDEO_D()
        {
            WSVideoMetaEdit.WSVideoMetaEditSoapClient video_d_obj = new WSVideoMetaEdit.WSVideoMetaEditSoapClient();

            video_d_obj.fnGetTBLOG_VIDEO_DCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        this.DGTBLOG_VIDEO_D.DataContext = args.Result; 
                    }
                }
            };

            video_d_obj.fnGetTBLOG_VIDEO_DAsync(_fsfile_no, -1);

        }

        /// <summary>
        /// 設定影像段落
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSetVideoSeg_Click(object sender, RoutedEventArgs e)
        {
            if (tbxMarkIn.Text.Trim() == string.Empty)
            {
                MessageBox.Show("請選取影像起點", "提示", MessageBoxButton.OK);
                return; 
            }
            else if (tbxMarkOut.Text.Trim() == string.Empty)
            {
                MessageBox.Show("請選取影像迄點", "提示", MessageBoxButton.OK);
                return;
            }
            else if( (tbxMarkIn.Text.Length != 11) ||  (tbxMarkOut.Text.Length != 11) ||
                      (tbxMarkIn.Text.Replace(';', ':').Split(':').Length != 4) ||
                      (tbxMarkOut.Text.Replace(';', ':').Split(':').Length != 4)                       
                ) {
                MessageBox.Show("起迄格式段落有誤", "提示", MessageBoxButton.OK);
                return;
            }
            else 
            {
                foreach (string val in tbxMarkIn.Text.Replace(';', ':').Split(':'))
                {
                    if(val.Length != 2) 
                    {
                        MessageBox.Show("起迄格式段落有誤", "提示", MessageBoxButton.OK);
                        return;
                    }
                    int t;
                    if (!int.TryParse(val, out t))
                    {
                        MessageBox.Show("起迄格式段落有誤", "提示", MessageBoxButton.OK);
                        return;
                    }
                }


                foreach (string val in tbxMarkOut.Text.Replace(';', ':').Split(':'))
                {
                    if (val.Length != 2)
                    {
                        MessageBox.Show("起迄格式段落有誤", "提示", MessageBoxButton.OK);
                        return;
                    }
                    int t;
                    if (!int.TryParse(val, out t))
                    {
                        MessageBox.Show("起迄格式段落有誤", "提示", MessageBoxButton.OK);
                        return;
                    }
                }

                if (TransferTimecode.timecodetoframe(tbxMarkIn.Text) >= TransferTimecode.timecodetoframe(tbxMarkOut.Text))
                {
                    MessageBox.Show("起迄段落範圍有誤", "提示", MessageBoxButton.OK);
                    return;
                }


                //檢查通過
                STO200_01 frm = new STO200_01(_fsfile_no, tbxMarkIn.Text, tbxMarkOut.Text, ((ObservableCollection<WSVideoMetaEdit.Class_Video_D>) this.DGTBLOG_VIDEO_D.DataContext).FirstOrDefault()) ;

                frm.Show();


                frm.Closed += (s, args) =>
                {
                    preLoadDGTBLOG_VIDEO_D();
                };

            }

        }

        //編輯影像段落資料
        private void btnVideoSegEdit_Click(object sender, RoutedEventArgs e)
        {
            if(this.DGTBLOG_VIDEO_D.SelectedItem != null ) {
                WSVideoMetaEdit.Class_Video_D VideoObj = (WSVideoMetaEdit.Class_Video_D)DGTBLOG_VIDEO_D.SelectedItem;
                STO200_02 videoEditForm = new STO200_02(VideoObj);
                videoEditForm.Show();

                videoEditForm.Closed += (s, args) =>
                {
                    preLoadDGTBLOG_VIDEO_D();
                };


            }
            else {
                MessageBox.Show("未選取欲編輯項目", "提示", MessageBoxButton.OK);
                return; 
            }
            
        }


        //儲存Keyframe 描述
        private void btnSaveKeyframeDescription_Click(object sender, RoutedEventArgs e)
        {
            WSVideoMetaEdit.Class_Video_Keyframe KeyframeObj;

            KeyframeObj = (WSVideoMetaEdit.Class_Video_Keyframe ) this.lbxKeframe.SelectedItem;


            if (this.lbxKeframe.SelectedItem != null)
            {
                KeyframeObj.FSDESCRIPTION = tbxKeyframeDescription.Text;

                WSVideoMetaEdit.WSVideoMetaEditSoapClient WSVideoObj = new WSVideoMetaEdit.WSVideoMetaEditSoapClient();

                WSVideoObj.fnUpdateTBLOG_VIDEO_KEYFRAMECompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (!args.Result)
                        {
                            MessageBox.Show("儲存Keyframe 描述資料時發生錯誤!", "提示", MessageBoxButton.OK);
                            return;
                        }
                        else
                        {
                            MessageBox.Show("儲存Keyframe 描述資料完畢!");
                        }
                    }

                };
                WSVideoObj.fnUpdateTBLOG_VIDEO_KEYFRAMEAsync(KeyframeObj, UserClass.userData.FSUSER_ID); 
            }
          


        }


        //刪除影像段落
        private void btnVideoSegDelete_Click(object sender, RoutedEventArgs e)
        {

            
            if (this.DGTBLOG_VIDEO_D.SelectedItem != null)
            {
                if (((WSVideoMetaEdit.Class_Video_D)DGTBLOG_VIDEO_D.SelectedItem).FNSEQ_NO == "0")
                {
                    MessageBox.Show("預設段落, 請勿刪除!", "提示", MessageBoxButton.OK);
                    return; 
                }
                Common.chWndMessageBox chMsgWnd = new Common.chWndMessageBox("提示", "是否確認刪除影像段落資料？", 0);
                chMsgWnd.Show();
                chMsgWnd.Closed += (s, args) =>
                {
                    if (chMsgWnd.DialogResult == true)
                    {


                        WSVideoMetaEdit.Class_Video_D VideoObj = (WSVideoMetaEdit.Class_Video_D)DGTBLOG_VIDEO_D.SelectedItem;
                       
                        WSVideoMetaEdit.WSVideoMetaEditSoapClient WSVideoObj = new WSVideoMetaEdit.WSVideoMetaEditSoapClient();
                        WSVideoObj.fnDeleteTBLOG_VIDEO_DCompleted += (s1, args1) =>
                        {
                            if (args1.Error == null)
                            {

                                if (args1.Result)
                                {
                                    preLoadDGTBLOG_VIDEO_D();
                                    MessageBox.Show("刪除影像段落資料成功!", "提示", MessageBoxButton.OK);

                                }
                                else
                                {
                                    MessageBox.Show("刪除影像段落資料時發生錯誤!", "提示", MessageBoxButton.OK);
                                    return;
                                }


                            }


                        };

                        WSVideoObj.fnDeleteTBLOG_VIDEO_DAsync(VideoObj.FSFILE_NO, Convert.ToInt32(VideoObj.FNSEQ_NO), UserClass.userData.FSUSER_ID);
                    }

                };



            }
            else
            {
                MessageBox.Show("未選取欲刪除項目", "提示", MessageBoxButton.OK);
                return;
            }
            
        }

       
        //set markin
        private void btnMarkIn_Click(object sender, RoutedEventArgs e)
        {
            this.tbxMarkIn.Text = TransferTimecode.frame2timecode(Convert.ToInt32(this.player.HTML_GetCurrentFrame())); 

        }


        //set markout 
        private void btnMarkOut_Click(object sender, RoutedEventArgs e)
        {
            this.tbxMarkOut.Text = TransferTimecode.frame2timecode(Convert.ToInt32(this.player.HTML_GetCurrentFrame())); 
        }


   
    }
}

