﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Media.Imaging;
using System.Collections.ObjectModel;
using PTS_MAM3.WSBookingFunction;
using System.Xml.Linq;

namespace PTS_MAM3.STO
{
    public partial class STO200_01 : ChildWindow
    {
        public STO200_01()
        {
            InitializeComponent();
        }


        private WSVideoMetaEdit.Class_Video_D _firstVideoD ;
        private string _fsfile_no { get; set; } 
        public STO200_01(string fsfile_no, string markin, string markout, WSVideoMetaEdit.Class_Video_D videoDObj)
        {
            InitializeComponent();
            this.tbxFSBEG_TIMECODE.Text = markin;
            this.tbxFSEND_TIMECODE.Text = markout;
            _firstVideoD = videoDObj;
            _fsfile_no = fsfile_no; 
            WSTSM_GetFileInfo.ServiceTSMSoapClient objTSM = new WSTSM_GetFileInfo.ServiceTSMSoapClient();




            string startTimeCode = string.Empty;

            WSBookingFunctionSoapClient booking = new WSBookingFunctionSoapClient();

            booking.GetFileStartTimeCodeCompleted += (s, args) =>
            {
                if ((args.Error != null) || String.IsNullOrEmpty(args.Result))
                {
                }
                else
                {
                    XDocument xDoc = XDocument.Parse(args.Result);
                    startTimeCode = xDoc.Element("Datas").Element("Data").Element("FSBEG_TIMECODE").Value;
                    int iStartTime = TransferTimecode.timecodetoframe(startTimeCode);

                    int begTime = TransferTimecode.timecodetoframe(tbxFSBEG_TIMECODE.Text);
                    int endTime = TransferTimecode.timecodetoframe(tbxFSEND_TIMECODE.Text); 

                    //取得range 內的keyframe                    
                    string rangeStart = _fsfile_no + "_" + TransferTimecode.frame2timecode(begTime - iStartTime).Replace(";", "").Replace(":", "");
                    string rangeEnd = _fsfile_no + "_" + TransferTimecode.frame2timecode(endTime - iStartTime).Replace(";", "").Replace(":", "");
                    WSVideoMetaEdit.WSVideoMetaEditSoapClient WSVideoObj = new WSVideoMetaEdit.WSVideoMetaEditSoapClient();
                    ObservableCollection<WSVideoMetaEdit.Class_Video_Keyframe> keyframeList;
                    WSVideoObj.fnGetTBLOG_VIDEO_KEYFRAME_IN_RANGECompleted += (s1, args2) =>
                    {
                        if (args2.Error == null)
                        {
                            if (args2.Result != null)
                            {
                                keyframeList = args2.Result;

                                if (keyframeList.Count > 0)
                                {
                                    imgHeadFrame.Source = new BitmapImage(new Uri(((WSVideoMetaEdit.Class_Video_Keyframe)keyframeList.FirstOrDefault()).path, UriKind.Absolute));
                                    imgHeadFrame.Tag = keyframeList.FirstOrDefault();
                                }
                            }
                        }
                    };
                    WSVideoObj.fnGetTBLOG_VIDEO_KEYFRAME_IN_RANGEAsync(fsfile_no, rangeStart, rangeEnd); 




                }
            };

            booking.GetFileStartTimeCodeAsync("<Data><FSFILE_NO>" + _fsfile_no + "</FSFILE_NO></Data>"); 
         

         


            
        }
        
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.tbxFSTITLE.Text == string.Empty)
            {
                MessageBox.Show("請輸入段落標題!", "提示", MessageBoxButton.OK);
                return; 
            }
            else if (this.tbxFSDESCRIPTION.Text == string.Empty)
            {
                MessageBox.Show("請輸入段落描述!", "提示", MessageBoxButton.OK);
                return;
            }
            else
            {
                WSVideoMetaEdit.Class_Video_D videoObj = new WSVideoMetaEdit.Class_Video_D(); 
                //videoObj.

                videoObj.FCFILE_STATUS = _firstVideoD.FCFILE_STATUS;
                videoObj.FNDIR_ID = _firstVideoD.FNDIR_ID;
                videoObj.FNEPISODE = _firstVideoD.FNEPISODE;
                videoObj.FSBEG_TIMECODE = tbxFSBEG_TIMECODE.Text.Trim() ;
                videoObj.FSEND_TIMECODE = tbxFSEND_TIMECODE.Text.Trim();
                videoObj.FSFILE_NO = _firstVideoD.FSFILE_NO;
                videoObj.FSID = _firstVideoD.FSID; 
                //暫時先這樣寫, 稍後修正
                if (imgHeadFrame.Tag != null)
                {
                    videoObj.FSKEYFRAME_NO = ((WSVideoMetaEdit.Class_Video_Keyframe)imgHeadFrame.Tag).FSFILE_NO; 
                }
                else
                    videoObj.FSKEYFRAME_NO = _firstVideoD.FSKEYFRAME_NO;


                
                videoObj.FSOLD_FILE_NAME = _firstVideoD.FSOLD_FILE_NAME;
                videoObj.FSSUBJECT_ID = _firstVideoD.FSSUBJECT_ID;
                videoObj.FSTITLE = tbxFSTITLE.Text.Trim();
                videoObj.FSTYPE = _firstVideoD.FSTYPE;
                videoObj.FSSUPERVISOR = _firstVideoD.FSSUPERVISOR; 
                videoObj.FSUPDATED_BY = UserClass.userData.FSUSER_ID;
                videoObj.FSCREATED_BY = UserClass.userData.FSCREATED_BY;
                videoObj.FSCHANNEL_ID = _firstVideoD.FSCHANNEL_ID;
                videoObj.FSCHANGE_FILE_NO = _firstVideoD.FSCHANGE_FILE_NO;
                videoObj.FSDESCRIPTION = tbxFSDESCRIPTION.Text.Trim();

                videoObj.FCFROM = _firstVideoD.FCFROM; 
                

                WSVideoMetaEdit.WSVideoMetaEditSoapClient WSVideoDObj = new WSVideoMetaEdit.WSVideoMetaEditSoapClient();

                WSVideoDObj.fnInsertTBLOG_VIDEO_DCompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (!args.Result)
                        {
                            MessageBox.Show("新增影像段落時發生錯誤!", "錯誤", MessageBoxButton.OK);
                        }
                        else
                        {
                            MessageBox.Show("新增影像段落完成!", "提示", MessageBoxButton.OK);
                            this.DialogResult = true;
                        }

                    }

                };
                WSVideoDObj.fnInsertTBLOG_VIDEO_DAsync(videoObj, UserClass.userData.FSUSER); 
                
            }

              
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //更換Keyframe 
        private void imgHeadFrame_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            string rangeStart = _fsfile_no + "_" + tbxFSBEG_TIMECODE.Text.Replace(";", "").Replace(":", "") ; 
            string rangeEnd = _fsfile_no + "_" + tbxFSEND_TIMECODE.Text.Replace(";", "").Replace(":", "")  ; 

            STO200_01_01 keyframeListFrm = new STO200_01_01(_fsfile_no, rangeStart, rangeEnd);

            keyframeListFrm.Show();

            keyframeListFrm.Closing += (s, args) =>
            {
                if (keyframeListFrm._selectKeyframe != null)
                {

                    this.imgHeadFrame.Source = new BitmapImage(new Uri(keyframeListFrm._selectKeyframe.path, UriKind.Absolute));
                    imgHeadFrame.Tag = keyframeListFrm._selectKeyframe;

                }



                    //  imgHeadFrame.Source =  new BitmapImage(new Uri(((WSVideoMetaEdit.Class_Video_Keyframe) keyframeList.FirstOrDefault()).path, UriKind.Absolute));
//                            imgHeadFrame.Tag = keyframeList.FirstOrDefault(); 
               //
            };
        }

      
    }
}

