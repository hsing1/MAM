﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Media.Imaging;
using System.Collections.ObjectModel;
using PTS_MAM3.WSBookingFunction;
using System.Xml.Linq;


namespace PTS_MAM3.STO
{
    public partial class STO200_02 : ChildWindow
    {

        private WSVideoMetaEdit.Class_Video_D _VideoDObj { get; set; }

        public STO200_02(WSVideoMetaEdit.Class_Video_D _firstVideoD)
        {
            InitializeComponent();

            _VideoDObj = _firstVideoD;

            this.tbxFSBEG_TIMECODE.Text = _VideoDObj.FSBEG_TIMECODE;
            this.tbxFSEND_TIMECODE.Text = _VideoDObj.FSEND_TIMECODE;
            
            this.tbxFSTITLE.Text = _VideoDObj.FSTITLE;
            this.tbxFSDESCRIPTION.Text = _VideoDObj.FSDESCRIPTION; 
            //this.imgHeadFrame.Source =
                //imgHeadFrame.Source =  new BitmapImage(new Uri(((WSVideoMetaEdit.Class_Video_Keyframe) keyframeList.FirstOrDefault()).path, UriKind.Absolute));
//                            imgHeadFrame.Tag = keyframeList.FirstOrDefault(); 

            initKeyframe(); 

        }

        private void initKeyframe() 
        {
            ObservableCollection<WSVideoMetaEdit.Class_Video_Keyframe> keyframeList;
            WSVideoMetaEdit.WSVideoMetaEditSoapClient WSKeyframeObj  = new WSVideoMetaEdit.WSVideoMetaEditSoapClient();

            WSKeyframeObj.fnGetTBLOG_VIDEO_KEYFRAME_IN_RANGECompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        keyframeList = args.Result;

                        if (keyframeList.Count > 0)
                        {
                           imgHeadFrame.Source = new BitmapImage(new Uri(((WSVideoMetaEdit.Class_Video_Keyframe)keyframeList.FirstOrDefault()).path, UriKind.Absolute));
                           imgHeadFrame.Tag = keyframeList.FirstOrDefault(); 
                        }
                    }

                }
                
            };
            WSKeyframeObj.fnGetTBLOG_VIDEO_KEYFRAME_IN_RANGEAsync(_VideoDObj.FSFILE_NO, _VideoDObj.FSKEYFRAME_NO, _VideoDObj.FSKEYFRAME_NO); 
            
        }



        //修改資料
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.tbxFSTITLE.Text == string.Empty)
            {
                MessageBox.Show("請輸入段落標題!", "提示", MessageBoxButton.OK);
                return; 
            }
            else if (this.tbxFSDESCRIPTION.Text == string.Empty)
            {
                MessageBox.Show("請輸入段落描述!", "提示", MessageBoxButton.OK);
                return;

            }
            else
            {
                _VideoDObj.FSTITLE = this.tbxFSTITLE.Text.Trim();
                _VideoDObj.FSDESCRIPTION = this.tbxFSDESCRIPTION.Text.Trim();




                _VideoDObj.FSBEG_TIMECODE = this.tbxFSBEG_TIMECODE.Text.Trim();
                _VideoDObj.FSEND_TIMECODE = this.tbxFSEND_TIMECODE.Text.Trim();

                if ((WSVideoMetaEdit.Class_Video_Keyframe)imgHeadFrame.Tag != null)
                {
                    _VideoDObj.FSKEYFRAME_NO = ((WSVideoMetaEdit.Class_Video_Keyframe)imgHeadFrame.Tag).FSFILE_NO;
                }
                else
                {
                    //
                }
               



                WSVideoMetaEdit.WSVideoMetaEditSoapClient WSVideoDObj = new WSVideoMetaEdit.WSVideoMetaEditSoapClient();

                WSVideoDObj.fnUpdateTBLOG_VIDEO_DCompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (!args.Result)
                        {
                            MessageBox.Show("更新影像段落資料時發生錯誤!", "錯誤", MessageBoxButton.OK);
                        }
                        else
                        {
                            MessageBox.Show("更新影像影像段落資料完成!", "提示", MessageBoxButton.OK);
                            this.DialogResult = true;
                        }

                    }                
                };

                //WSVideoDObj.fnInsertTBLOG_VIDEO_DAsync(videoObj, UserClass.userData.FSUSER); 
                WSVideoDObj.fnUpdateTBLOG_VIDEO_DAsync(_VideoDObj, UserClass.userData.FSUSER); 


            }

        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void imgHeadFrame_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            WSTSM_GetFileInfo.ServiceTSMSoapClient objTSM = new WSTSM_GetFileInfo.ServiceTSMSoapClient();




            string startTimeCode = string.Empty;

            WSBookingFunctionSoapClient booking = new WSBookingFunctionSoapClient();


            booking.GetFileStartTimeCodeCompleted += (s, args) =>
            {
                if ((args.Error != null) || String.IsNullOrEmpty(args.Result))
                {
                }
                else
                {
                    XDocument xDoc = XDocument.Parse(args.Result);
                    startTimeCode = xDoc.Element("Datas").Element("Data").Element("FSBEG_TIMECODE").Value;
                    int iStartTime = TransferTimecode.timecodetoframe(startTimeCode);

                    int begTime = TransferTimecode.timecodetoframe(tbxFSBEG_TIMECODE.Text);
                    int endTime = TransferTimecode.timecodetoframe(tbxFSEND_TIMECODE.Text);

                    string rangeStart = _VideoDObj.FSFILE_NO + "_" + TransferTimecode.frame2timecode(begTime - iStartTime).Replace(";", "").Replace(":", "");
                    string rangeEnd = _VideoDObj.FSFILE_NO + "_" + TransferTimecode.frame2timecode(endTime - iStartTime).Replace(";", "").Replace(":", "");

                    STO200_01_01 keyframeListFrm = new STO200_01_01(_VideoDObj.FSFILE_NO, rangeStart, rangeEnd);

                    keyframeListFrm.Show();

                    keyframeListFrm.Closing += (s1, args1) =>
                    {
                        if (keyframeListFrm._selectKeyframe != null)
                        {

                            this.imgHeadFrame.Source = new BitmapImage(new Uri(keyframeListFrm._selectKeyframe.path, UriKind.Absolute));
                            imgHeadFrame.Tag = keyframeListFrm._selectKeyframe;

                        }



                        //  imgHeadFrame.Source =  new BitmapImage(new Uri(((WSVideoMetaEdit.Class_Video_Keyframe) keyframeList.FirstOrDefault()).path, UriKind.Absolute));
                        //                            imgHeadFrame.Tag = keyframeList.FirstOrDefault(); 
                        //
                    };

                }
            };



          
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            STO.STO300 sto300 = new STO300("V", _VideoDObj.FSFILE_NO, _VideoDObj.FNSEQ_NO);
            sto300.Show();
        }
    }
}

