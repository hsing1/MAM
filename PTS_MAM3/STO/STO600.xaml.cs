﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using PTS_MAM3.WSSTT;
using System.IO;
namespace PTS_MAM3.STO
{
    public partial class STO600 : ChildWindow
    {
        WSSTTSoapClient client = new WSSTTSoapClient();

        public string strSelect_File_Path = "";                         //選取的檔案路徑
        public string strSelect_File_Name = "";                         //選取的檔案名稱

        string m_strUpLoadPath = "";                                    //送帶轉檔暫存區的路徑
        List<string> DirectoriesList = new List<string>();              //目錄集合
        List<string> FileList = new List<string>();                     //檔案集合
        List<Class_File_Info> FileInformationList = new List<Class_File_Info>();   //檔案內容集合       
        string m_strFileDirectory = "";                                 //記錄目錄資料夾
        string m_strUpLoadType = "";                                    //數位檔案上傳位置(Network或是Fiber)

        public STO600()
        {
            //m_strUpLoadType = strUpLoadType;
            InitializeComponent();
            //InitializeForm();               //初始化本頁面      
            iniTree();
            //busyLoadingLeft.IsBusy = true;  //等待讀取server資料夾名稱
        }

        void InitializeForm() //初始化本頁面
        {
            //取得送帶轉檔暫存區的路徑
            //client.GetSTTFilePathCompleted += new EventHandler<GetSTTFilePathCompletedEventArgs>(client_GetSTTFilePathCompleted);
            //client.GetSTTFilePathAsync(m_strUpLoadType);

            //取得最主要目錄
            //client.GetTopDirectoriesCompleted += new EventHandler<GetTopDirectoriesCompletedEventArgs>(client_GetTopDirectoriesCompleted);  

            //取得目錄下的檔案
            //client.GetDirectorieFilesCompleted += new EventHandler<GetDirectorieFilesCompletedEventArgs>(client_GetDirectorieFilesCompleted);
        }

        

        void iniTree()
        {
            RadTreeViewItem TVitem01 = new RadTreeViewItem();
            RadTreeViewItem TVitem07 = new RadTreeViewItem();
            RadTreeViewItem TVitem08 = new RadTreeViewItem();
            RadTreeViewItem TVitem09 = new RadTreeViewItem();
            TVitem01.Header = "公視"; TVitem01.IsLoadOnDemandEnabled = true;
            TVitem07.Header = "客台"; TVitem07.IsLoadOnDemandEnabled = true;
            TVitem08.Header = "宏觀"; TVitem08.IsLoadOnDemandEnabled = true;
            TVitem09.Header = "原民"; TVitem09.IsLoadOnDemandEnabled = true;
            WSReview.Class_ReviewTree station01class = new WSReview.Class_ReviewTree();
            WSReview.Class_ReviewTree station07class = new WSReview.Class_ReviewTree();
            WSReview.Class_ReviewTree station08class = new WSReview.Class_ReviewTree();
            WSReview.Class_ReviewTree station09class = new WSReview.Class_ReviewTree();
            station01class.StationID = "01"; station01class.FSID = ""; station01class.FSNAME = ""; station01class.CatalogID = "";
            station07class.StationID = "07"; station07class.FSID = ""; station07class.FSNAME = ""; station07class.CatalogID = "";
            station08class.StationID = "08"; station08class.FSID = ""; station08class.FSNAME = ""; station08class.CatalogID = "";
            station09class.StationID = "09"; station09class.FSID = ""; station09class.FSNAME = ""; station09class.CatalogID = "";
            TVitem01.Tag = station01class;
            TVitem07.Tag = station07class;
            TVitem08.Tag = station08class;
            TVitem09.Tag = station09class;
            TreeViewFileList.Items.Add(TVitem01);
            TreeViewFileList.Items.Add(TVitem07);
            TreeViewFileList.Items.Add(TVitem08);
            TreeViewFileList.Items.Add(TVitem09);
            //TreeViewFileList.AddHandler
        }
        #region 實作
        //實作-取得最主要目錄
        //void client_GetTopDirectoriesCompleted(object sender, GetTopDirectoriesCompletedEventArgs e)
        //{
        //    busyLoadingLeft.IsBusy = false;

        //    if (e.Error == null)
        //    {
        //        if (e.Result != null && e.Result.Count > 0)
        //        {
        //            //最主要的目錄節點
        //            RadTreeViewItem newNode = new RadTreeViewItem();
        //            newNode.Header = "轉檔中心數位檔案上傳區";
        //            newNode.Tag = m_strUpLoadPath;
        //            TreeViewFileList.Items.Add(newNode);

        //            for (int i = 0; i < e.Result.Count; i++)
        //            {
        //                RadTreeViewItem newNodeChild = new RadTreeViewItem();
        //                newNodeChild.Header = e.Result[i].Replace(m_strUpLoadPath, "");
        //                newNodeChild.Tag = e.Result[i];
        //                newNode.Items.Add(newNodeChild);
        //            }
        //        }
        //    }
        //}

        //實作-取得目錄下的檔案
        //void client_GetDirectorieFilesCompleted(object sender, GetDirectorieFilesCompletedEventArgs e)
        //{
        //    if (e.Error == null)
        //    {
        //        FileInformationList.Clear();

        //        if (e.Result != null)
        //        {                    
        //            lbFileList.ItemsSource = e.Result;                    
        //            acbFileName.ItemsSource = e.Result; 

        //            //原本的作法是程式塞TextBlock到ListBox裡，現在改用Bind的方式
        //            //for (int i = 0; i < e.Result.Count; i++)
        //            //{
        //            //    FileList.Add(e.Result[i]);

        //            //    TextBlock tbFile = new TextBlock();
        //            //    tbFile.Text = e.Result[i];
        //            //    tbFile.Tag = e.Result[i];
        //            //    lbFileList.Items.Add(tbFile);
        //            //}
        //        }

        //    }
        //}

        //實作-取得送帶轉檔暫存區的路徑
        //void client_GetSTTFilePathCompleted(object sender, GetSTTFilePathCompletedEventArgs e)
        //{
        //    busyLoadingLeft.IsBusy = false;

        //    if (e.Error == null)
        //    {
        //        if (e.Result != "")
        //        {
        //            m_strUpLoadPath = e.Result;
        //            m_strFileDirectory = m_strUpLoadPath;
        //            client.GetTopDirectoriesAsync(m_strUpLoadType);    //取得最主要目錄
        //            client.GetDirectorieFilesAsync(m_strUpLoadPath);
        //            busyLoadingLeft.IsBusy = true;
        //        }
        //    }
        //}

        # endregion
       
        
        //選取時觸發
        private void TreeViewFileList_Selected(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            //RadTreeViewItem treeNode = (RadTreeViewItem)e.Source;

            ////取得目錄列表，取得前先刪除ViewItem            
            //preLoadDirectoryList(treeNode.Tag.ToString(), treeNode);

            ////取得檔案列表，取得前先刪除ListBox
            ////lbFileList.Items.Clear();  //現在用Binding的方式，不用清除ListBox
            //m_strFileDirectory = treeNode.Tag.ToString();
            //client.GetDirectorieFilesAsync(treeNode.Tag.ToString());
        }

        //第一次點選時觸發
        private void TreeViewFileList_LoadOnDemand(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            WSReview.Class_ReviewTree myCRT = new WSReview.Class_ReviewTree();
            myCRT = (WSReview.Class_ReviewTree)((RadTreeViewItem)(e.Source)).Tag;
            //因為要求即時，因此每次點選都重新去找，LoadOnDemand就不作事          
            //((RadTreeViewItem)(e.Source)).IsLoadOnDemandEnabled = false;
            #region 加入分類 G節目 P短帶 D資料帶
            if (myCRT.StationID != "" && myCRT.CatalogID == "")
            {
                RadTreeViewItem tvi01 = new RadTreeViewItem();
                RadTreeViewItem tvi02 = new RadTreeViewItem();
                RadTreeViewItem tvi03 = new RadTreeViewItem();
                
                WSReview.Class_ReviewTree cr01 = new WSReview.Class_ReviewTree();
                WSReview.Class_ReviewTree cr02 = new WSReview.Class_ReviewTree();
                WSReview.Class_ReviewTree cr03 = new WSReview.Class_ReviewTree();
                
                cr01.StationID = myCRT.StationID; cr01.CatalogID = "G";
                cr02.StationID = myCRT.StationID; cr02.CatalogID = "P";
                cr03.StationID = myCRT.StationID; cr03.CatalogID = "D";

                tvi01.Header = "節目"; tvi01.IsLoadOnDemandEnabled = true; tvi01.Tag = cr01;
                tvi02.Header = "短帶"; tvi02.IsLoadOnDemandEnabled = true; tvi02.Tag = cr02;
                tvi03.Header = "資料帶"; tvi03.IsLoadOnDemandEnabled = true; tvi03.Tag = cr03;

                ((RadTreeViewItem)(e.Source)).Items.Add(tvi01);
                ((RadTreeViewItem)(e.Source)).Items.Add(tvi02);
                ((RadTreeViewItem)(e.Source)).Items.Add(tvi03);
            }
            #endregion
            ((RadTreeViewItem)(e.Source)).IsLoadOnDemandEnabled = false;
        }

        ////取得目錄列表
        //private void preLoadDirectoryList(string strPath, RadTreeViewItem treeNode)
        //{
        //    WSSTTSoapClient clientDirectory = new WSSTTSoapClient();         //必須在裡面宣告，complete事件會重覆跑
        //    clientDirectory.GetDirectoriesAsync(treeNode.Tag.ToString());    //透過路徑，取得該路徑底下的資料夾

        //    clientDirectory.GetDirectoriesCompleted += (s, args) =>
        //    {
        //        if (args.Error == null)
        //        {
        //            if (args.Result != null && args.Result.Count > 0)
        //            {
        //                treeNode.Items.Clear();

        //                for (int i = 0; i < args.Result.Count; i++)
        //                {                            
        //                    RadTreeViewItem newNode = new RadTreeViewItem();

        //                    if (args.Result[i].Replace(treeNode.Tag.ToString().Trim(), "").Trim().StartsWith("\\") == true)
        //                        newNode.Header = args.Result[i].Replace(treeNode.Tag.ToString().Trim(), "").Substring(1);  
        //                    else
        //                        newNode.Header = args.Result[i].Replace(treeNode.Tag.ToString().Trim(), ""); 
                            
        //                    newNode.Tag = args.Result[i];
        //                    treeNode.Items.Add(newNode);
        //                }                      
        //            }                    
        //        }
        //        treeNode.IsLoadOnDemandEnabled = false;
        //    };
        //}

        //private void OKButton_Click(object sender, RoutedEventArgs e)
        //{
        //    if (lbFileList.SelectedItem == null)
        //    {
        //        MessageBox.Show("請先選擇欲轉檔的檔案", "提示訊息", MessageBoxButton.OK);
        //        return;
        //    }     

        //    strSelect_File_Path = ((Class_File_Info)lbFileList.SelectedItem).FSFile_Path.ToString().Trim();
        //    strSelect_File_Name = ((Class_File_Info)lbFileList.SelectedItem).FSFile_Name.ToString().Trim();

        //    this.DialogResult = true;
        //}

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        ////選取確認
        //private void btnEnter_Click(object sender, RoutedEventArgs e)
        //{
        //    if (acbFileName.Text == "")
        //    {
        //        MessageBox.Show("請輸入影片名稱", "提示訊息", MessageBoxButton.OK);
        //        return;
        //    }
        //    else if(acbFileName.SelectedItem == null)
        //    {
        //        MessageBox.Show("查無該檔案名稱，請檢查", "提示訊息", MessageBoxButton.OK);
        //        return;
        //    }

        //    strSelect_File_Path = ((Class_File_Info)acbFileName.SelectedItem).FSFile_Path.ToString().Trim();
        //    strSelect_File_Name = ((Class_File_Info)acbFileName.SelectedItem).FSFile_Name.ToString().Trim();
        //    this.DialogResult = true;
        //}


    }

}

