﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.ProgData;
using PTS_MAM3.PROG_M;
using System.Text;
using System.Xml.Linq;
using System.IO;
using PTS_MAM3.WSPROG_M;
using Telerik.Windows.Controls;
using PTS_MAM3.FlowMAM;



namespace PTS_MAM3.STO
{
    /// <summary>
    /// 入庫置換單新增
    /// </summary>
    public partial class STO101_01 : ChildWindow
    {
        WSPROG_MSoapClient client1 = new WSPROG_MSoapClient();     //產生新的代理類別
        WSMAMFunctions.WSMAMFunctionsSoapClient Client2 = new WSMAMFunctions.WSMAMFunctionsSoapClient();
        WSARCHIVE.WSARCHIVESoapClient client_ARCHIVE = new WSARCHIVE.WSARCHIVESoapClient();
        WSLogTemplateField_CheckIsNullable.WSLogTemplateField_CheckIsNullableSoapClient client_CheckField = new WSLogTemplateField_CheckIsNullable.WSLogTemplateField_CheckIsNullableSoapClient();
        List<WSARCHIVE.Class_ARCHIVE_VAPD> VAPDList = new List<WSARCHIVE.Class_ARCHIVE_VAPD>();
        //流程用
        FlowMAMSoapClient clientFlow = new FlowMAMSoapClient();
        public int intProcessID;
        public int intFlowID;
        //要傳到子視窗去的編號
        string FSID = "", ARCFSTYPE = "G", FSARCID = "", ARCFNEPISODE = "0", ARCCHANNEL = "", CHANNEL_IDs = "", NewOrModify = "New";
        //FSID:節目ID ARCFSTYPE:G節目帶 P宣傳帶 FSARCID:入庫單號 ARCFNEPISODE:節目集數 ARCCHANNEL:節目所屬頻道 CHANNEL_IDs:節目可播頻道 NewOrModify:新增或修改
        WSARCHIVE.Class_ARCHIVE pageClass = new WSARCHIVE.Class_ARCHIVE();
        public List<Object> UploadItems = new List<object>();

        /// <summary>
        /// 新增表單及流程時所使用的類別
        /// </summary>
        public STO101_01()
        {
            InitializeComponent();
            //LOAD進初始資料 "播出頻道"顯示
            SetCheckFieldCompleted();    //設定必填欄位完成事件
            SetInsSuccessCompleted();   //設定送出流程單完成事件
            client1.fnGetTBPROG_M_CODECompleted += new EventHandler<fnGetTBPROG_M_CODECompletedEventArgs>(client_fnGetTBPROG_M_CODECompleted);
            client1.fnGetTBPROG_M_CODEAsync();
            //取得入庫單號
            Client2.GetNoRecordAsync("07", "", "", UserClass.userData.FSUSER_ID);
            Client2.GetNoRecordCompleted += new EventHandler<WSMAMFunctions.GetNoRecordCompletedEventArgs>(Client2_GetNoRecordCompleted);

            rdbProg.IsChecked = true;

        }

        void RefreshModify(WSARCHIVE.Class_ARCHIVE myClass)
        {
            if (myClass.FSTYPE == "G")
            { rdbProg.IsChecked = true; }
            else if (myClass.FSTYPE == "P")
            { rdbPromo.IsChecked = true; }
            tbxARC_ID.Text = FSARCID.ToString().Trim();
            tbxEPISODE.Text = ARCFNEPISODE;
            client_ARCHIVE.GetARCHIVE_RefreshModifyCompleted += (s, args) =>
                {
                    if (myClass.FNEPISODE != 0)
                    {
                        tbxNAME.Text = args.Result.FSPROG_ID_NAME;
                        lblEPISODE_NAME.Content = args.Result.FSEPISODE_NAME;
                        tbxTOTEPISODE.Text = args.Result.FNTOTEPISODE;
                        tbxLENGTH.Text = args.Result.FNLENGTH;
                        CHANNEL_IDs = args.Result.FSCHANNEL_ID;
                        ARCCHANNEL = myClass.FSCHANNEL_ID.Trim();
                    }
                    else
                    {
                        tbxNAME.Text = args.Result.FSPROG_ID_NAME;
                        tbxTOTEPISODE.Text = args.Result.FNTOTEPISODE;
                        tbxLENGTH.Text = args.Result.FNLENGTH;
                        CHANNEL_IDs = args.Result.FSCHANNEL_ID;
                        ARCCHANNEL = myClass.FSCHANNEL_ID.Trim();
                    }
                    Refresh(FSARCID);
                };
            client_ARCHIVE.GetARCHIVE_RefreshModify_PCompleted += (s, args) =>
            {
                tbxNAME.Text = args.Result.FSPROG_ID_NAME;
                lblEPISODE_NAME.Content = args.Result.FSEPISODE_NAME;
                CHANNEL_IDs = args.Result.FSCHANNEL_ID;
                ARCCHANNEL = myClass.FSCHANNEL_ID.Trim();
                Refresh(FSARCID);
            };
            if (myClass.FSTYPE == "G")
            { client_ARCHIVE.GetARCHIVE_RefreshModifyAsync(myClass); }
            else if (myClass.FSTYPE == "P")
            { client_ARCHIVE.GetARCHIVE_RefreshModify_PAsync(myClass); }
        }
        List<WSARCHIVE.Class_ARCHIVE_VAPD> calCountList = new List<WSARCHIVE.Class_ARCHIVE_VAPD>();
        void Refresh(string FSARCID)
        {
            client_ARCHIVE.RefreshVAPDCompleted += (s, args) =>
                {
                    dataGrid1.ItemsSource = null;
                    dataGrid1.ItemsSource = args.Result;
                    calCountList = args.Result;
                    Clear_listCHANNEL();                                                //清空-可播映頻道
                    compareCode_FSCHANNEL(CheckList(CHANNEL_IDs));     //可播映頻道
                    //更新已上傳文件 的計算
                    CountVAPD();
                };
            client_ARCHIVE.RefreshVAPDAsync(FSARCID,FSID,ARCFNEPISODE);
        }
        int UploadedV, UploadedA, UploadedP, UploadedD;
        public void CountVAPD()
        {
            var TVList = from x in calCountList
                         where x.FSTableName == "TBLOG_VIDEO"
                         select x;
            var TAList = from x in calCountList
                         where x.FSTableName == "TBLOG_AUDIO"
                         select x;
            var TPList = from x in calCountList
                         where x.FSTableName == "TBLOG_PHOTO"
                         select x;
            var TDList = from x in calCountList
                         where x.FSTableName == "TBLOG_DOC"
                         select x;
            UploadedV = TVList.Count();
            UploadedA = TAList.Count();
            UploadedP = TPList.Count();
            UploadedD = TDList.Count();
            lblLack.Content = "影像" + UploadedV + "個、聲音" + UploadedA + "個、圖片" + UploadedP + "張、其他附檔" + UploadedD + "個";
        }

        void Client2_GetNoRecordCompleted(object sender, WSMAMFunctions.GetNoRecordCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    tbxARC_ID.Text = e.Result;
                    FSARCID = e.Result;
                    Refresh(e.Result);
                }
                else
                {
                    MessageBox.Show("成案功能尚未開啟，請聯絡系統管理員！");
                    this.DialogResult = false;
                }
            }
            else
            {
                MessageBox.Show("成案功能尚未開啟，請聯絡系統管理員！");
                this.DialogResult = false;
            }
        }

        List<WSLogTemplateField_CheckIsNullable.CheckFileList> SendCheckFieldList = new List<WSLogTemplateField_CheckIsNullable.CheckFileList>();
        List<WSLogTemplateField_CheckIsNullable.CheckFileList> returnCheckFieldList = new List<WSLogTemplateField_CheckIsNullable.CheckFileList>();
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            SendCheckFieldList.Clear();
            foreach (WSARCHIVE.Class_ARCHIVE_VAPD x in calCountList)
            {
                WSLogTemplateField_CheckIsNullable.CheckFileList SP = new WSLogTemplateField_CheckIsNullable.CheckFileList();
                SP.FSFileName = x.FSOldFileName;
                SP.FSFileNo = x.FSFile_No;
                if (x.FSTableName == "TBLOG_VIDEO")
                { SP.FSTableType = "V"; }
                else if (x.FSTableName == "TBLOG_AUDIO")
                { SP.FSTableType = "A"; }
                else if (x.FSTableName == "TBLOG_PHOTO")
                { SP.FSTableType = "P"; }
                else if (x.FSTableName == "TBLOG_DOC")
                { SP.FSTableType = "D"; }
                SendCheckFieldList.Add(SP);
            }
            client_CheckField.TemplateField_CheckIsNullAbleAsync(SendCheckFieldList);

        }
        
        private void SetInsSuccessCompleted()
        {
            client_ARCHIVE.InsTBLOG_ARCHIVE_EXCHANGE_ARECARDCompleted += (s, args) =>
            {
                if (args.Result == true)
                {
                    newAFlow(FSARCID);
                    MessageBox.Show("入庫置換流程已啟動");
                    this.DialogResult = true;
                }
                else
                { MessageBox.Show("請檢查網路狀態"); }
            };
        }
        private void SetCheckFieldCompleted()
        {
            client_CheckField.TemplateField_CheckIsNullAbleCompleted += (sf, argsf) =>
                {
                    returnCheckFieldList.Clear();
                    returnCheckFieldList = argsf.Result;
                    if (returnCheckFieldList.Count <= 0)
                    {

                        int V, A, P, D;
                        V = UploadedV - Vcount;
                        A = UploadedA - Acount;
                        P = UploadedP - Pcount;
                        D = UploadedD - Dcount;
                        if (tbxMemo.Text == "")
                        {
                            MessageBox.Show("請填寫資置換檔案說明欄位!!");
                        }
                        else
                        {
                            if (NewOrModify == "New")
                            {
                                if (FSID == "" || FSARCID == "" || ARCFSTYPE == "")
                                { MessageBox.Show("請確認輸入的資料正確"); }
                                else
                                {
                                    WSARCHIVE.Class_ARCHIVE ARCHIVEClass = new WSARCHIVE.Class_ARCHIVE();
                                    ARCHIVEClass.FSARC_ID = FSARCID;
                                    ARCHIVEClass.FSTYPE = ARCFSTYPE;
                                    ARCHIVEClass.FSID = FSID;
                                    ARCHIVEClass.FSCHANNEL_ID = ARCCHANNEL;
                                    ARCHIVEClass.FSLESSCLAM = tbxMemo.Text.Trim();
                                    if (cbBooking.IsChecked == true)
                                        ARCHIVEClass.FSSUPERVISOR = "Y";
                                    else
                                        ARCHIVEClass.FSSUPERVISOR = "N";
                                    ARCHIVEClass.FNEPISODE = Convert.ToInt16(ARCFNEPISODE);
                                    ARCHIVEClass.FSCREATED_BY = UserClass.userData.FSUSER_ID;
                                    client_ARCHIVE.InsTBLOG_ARCHIVE_EXCHANGE_ARECARDAsync(ARCHIVEClass);

                                }
                            }
                            else if (NewOrModify == "Modify")
                            {
                                client_ARCHIVE.UpdTBARCHIVE_EXCHANGECompleted += (s, args) =>
                                {
                                    if (args.Result == true)
                                    {
                                        MessageBox.Show("修改完成");
                                        this.DialogResult = true;
                                    }
                                    else
                                    {
                                        MessageBox.Show("請檢查網路連線");
                                        this.DialogResult = false;
                                    }
                                };
                                client_ARCHIVE.UpdTBARCHIVE_EXCHANGEAsync(FSARCID, ARCFSTYPE, FSID, ARCFNEPISODE, "W", ARCCHANNEL, UserClass.userData.FSUSER_ID, tbxMemo.Text.Trim(), pageClass.FSSUPERVISOR);
                            }
                            else if (NewOrModify == "Flow")
                            {
                                newAFlow(FSARCID);
                            }
                        }
                    }
                    else if (returnCheckFieldList.Count > 0)
                    {
                        string FileName = "下列檔案有必填欄位未填：\n";
                        foreach (WSLogTemplateField_CheckIsNullable.CheckFileList Y in returnCheckFieldList)
                        { FileName += Y.FSFileName + " \n"; }
                        FileName += "請點選檔案 並選取功能列上\"修改資料\"進入必填欄位修改";
                        MessageBox.Show(FileName);
                    }
                 
                };
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;

        }

        //選取節目時
        private void rdbProg_Checked(object sender, RoutedEventArgs e)
        {
            tbxNAME.Text = "";
            tbxNAME.Tag = "";
            tbxTOTEPISODE.Text = "";
            tbxLENGTH.Text = "";
            if (rdbProg.IsChecked == true)
            {
                ARCFSTYPE = "G";
                tbNANE.Text = "*節目名稱";
                tbFNEPISODE.Visibility = Visibility.Visible;
                tbxEPISODE.Visibility = Visibility.Visible;
                btnEPISODE.Visibility = Visibility.Visible;
                tbEPISODE_NAME.Visibility = Visibility.Visible;
                lblEPISODE_NAME.Visibility = Visibility.Visible;
                tbCHANNEL.Visibility = Visibility.Visible;
                listCHANNEL.Visibility = Visibility.Visible;
                tbTOTEPISODE.Visibility = Visibility.Visible;
                tbxTOTEPISODE.Visibility = Visibility.Visible;
                tbLENGTH.Visibility = Visibility.Visible;
                tbxLENGTH.Visibility = Visibility.Visible;
            }
            else
            {

            }
        }

        //選取宣傳帶時
        private void rdbPromo_Checked(object sender, RoutedEventArgs e)
        {
            tbxNAME.Text = "";
            tbxNAME.Tag = "";
            tbxTOTEPISODE.Text = "";
            tbxLENGTH.Text = "";
            if (rdbPromo.IsChecked == true)
            {
                ARCFSTYPE = "P";
                ARCFNEPISODE = "0";
                tbNANE.Text = "*宣傳帶名稱";
                tbFNEPISODE.Visibility = Visibility.Collapsed;
                tbxEPISODE.Visibility = Visibility.Collapsed;
                btnEPISODE.Visibility = Visibility.Collapsed;
                tbEPISODE_NAME.Visibility = Visibility.Collapsed;
                lblEPISODE_NAME.Visibility = Visibility.Collapsed;
                tbCHANNEL.Visibility = Visibility.Collapsed;
                listCHANNEL.Visibility = Visibility.Collapsed;
                tbTOTEPISODE.Visibility = Visibility.Collapsed;
                tbxTOTEPISODE.Visibility = Visibility.Collapsed;
                tbLENGTH.Visibility = Visibility.Collapsed;
                tbxLENGTH.Visibility = Visibility.Collapsed;
            }
            else
            {
            }

        }

        private void btnID_Click(object sender, RoutedEventArgs e)
        {
            if (ARCFSTYPE == "G")
            {
                //PRG100_07
                STO100_10 PROGDATA_VIEW_frm = new STO100_10();
                PROGDATA_VIEW_frm.Show();

                PROGDATA_VIEW_frm.Closed += (s, args) =>
                {
                    if (PROGDATA_VIEW_frm.DialogResult == true)
                    {
                        tbxNAME.Text = PROGDATA_VIEW_frm.strProgName_View;
                        tbxNAME.Tag = PROGDATA_VIEW_frm.strProgID_View;

                        tbxEPISODE.Text = "";
                        lblEPISODE_NAME.Content = "";
                        tbxTOTEPISODE.Text = PROGDATA_VIEW_frm.strTOTLEEPISODE;
                        tbxLENGTH.Text = PROGDATA_VIEW_frm.strFNLENGTH;
                        ARCCHANNEL = PROGDATA_VIEW_frm.strChannel_Id;
                        FSID = PROGDATA_VIEW_frm.strProgID_View;
                        CHANNEL_IDs = PROGDATA_VIEW_frm.strCHANNEL;
                        Clear_listCHANNEL();                                                //清空-可播映頻道
                        compareCode_FSCHANNEL(CheckList(PROGDATA_VIEW_frm.strCHANNEL));     //可播映頻道
                    }
                };
            }
            else if (ARCFSTYPE == "P")
            {
                //PRG800_07
                STO100_20 PROMO_VIEW_frm = new STO100_20();
                PROMO_VIEW_frm.Show();

                PROMO_VIEW_frm.Closed += (s, args) =>
                {
                    if (PROMO_VIEW_frm.DialogResult == true)
                    {
                        tbxNAME.Text = PROMO_VIEW_frm.strPromoName_View;
                        tbxNAME.Tag = PROMO_VIEW_frm.strPromoID_View;
                        FSID = PROMO_VIEW_frm.strPromoID_View;
                        ARCCHANNEL = PROMO_VIEW_frm.strChannel_Id;
                        tbxEPISODE.Text = "";
                        lblEPISODE_NAME.Content = "";
                        ARCFNEPISODE = "0";
                    }
                };
            }

        }

        private void btnEPISODE_Click(object sender, RoutedEventArgs e)
        {
            if (tbxNAME.Text.ToString().Trim() != "")
            {
                STO.STO100_30 PRG200_07_frm = new STO100_30(FSID, tbxNAME.Text.ToString().Trim());//new PRG.PRG200_07(FSID, tbxNAME.Text.ToString().Trim());
                PRG200_07_frm.Show();

                PRG200_07_frm.Closed += (s, args) =>
                {
                    if (PRG200_07_frm.DialogResult == true)
                    {
                        ARCFNEPISODE = PRG200_07_frm.m_strEpisode_View;
                        tbxEPISODE.Text = PRG200_07_frm.m_strEpisode_View;
                        lblEPISODE_NAME.Content = PRG200_07_frm.m_strProgDName_View;
                    }
                };
            }
            else
                MessageBox.Show("請先選擇節目！");
        }

        #region 比對

        private string[] CheckList(string strChannel)
        {
            char[] delimiterChars = { ';' };
            string[] words = strChannel.Split(delimiterChars);
            return words;
        }

        //比對代碼檔_可播映頻道
        void compareCode_FSCHANNEL(string[] ListTEST_Send)
        {
            for (int i = 0; i < ListTEST_Send.Length - 1; i++)
            {
                for (int j = 0; j < listCHANNEL.Items.Count; j++)
                {
                    CheckBox getcki = (CheckBox)listCHANNEL.Items.ElementAt(j);

                    if (getcki.Tag.ToString().Trim().Equals(ListTEST_Send[i]))
                    {
                        getcki.IsChecked = true;
                        break;
                    }
                }
            }
        }

        //清空_可播映頻道
        void Clear_listCHANNEL()
        {
            for (int j = 0; j < listCHANNEL.Items.Count; j++)
            {
                CheckBox getcki = (CheckBox)listCHANNEL.Items.ElementAt(j);
                getcki.IsChecked = false;
            }
        }

        #endregion

        #region 實作-下載所有的代碼檔
        void client_fnGetTBPROG_M_CODECompleted(object sender, fnGetTBPROG_M_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE CodeData = new Class_CODE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            case "TBZCHANNEL":       //頻道別代碼
                                CheckBox cbCHANNEL = new CheckBox();
                                cbCHANNEL.Content = CodeData.NAME;
                                cbCHANNEL.Tag = CodeData.ID;
                                listCHANNEL.Items.Add(cbCHANNEL);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }


        #endregion

        #region 呼叫新增影音圖文子視窗
        private void btnAddVedio_Click(object sender, RoutedEventArgs e)
        {
            if (EnterUploadProcess() == true)
            {
                STO.STO101_01_01 stoA = new STO.STO101_01_01(FSARCID, ARCFSTYPE, FSID, ARCFNEPISODE, ARCCHANNEL);
                stoA.Show();
                stoA.Closing += (s, args) =>
                {
                    Refresh(FSARCID);
                };
            }
        }



        private void btnAddAudio_Click(object sender, RoutedEventArgs e)
        {
            if (EnterUploadProcess() == true)
            {
                STO.STO101_01_02 stoA = new STO.STO101_01_02(FSARCID, ARCFSTYPE, FSID, ARCFNEPISODE, ARCCHANNEL, tbxNAME.Text, lblEPISODE_NAME.Content.ToString());
                stoA.Show();
                stoA.Closing += (s, args) =>
                {
                    Refresh(FSARCID);
                };
            }
        }

        private void btnAddPicture_Click(object sender, RoutedEventArgs e)
        {
            if (EnterUploadProcess() == true)
            {
                STO.STO101_01_03 stoP = new STO.STO101_01_03(FSARCID, ARCFSTYPE, FSID, ARCFNEPISODE, ARCCHANNEL, tbxNAME.Text, lblEPISODE_NAME.Content.ToString());
                stoP.Show();
                stoP.Closing += (s, args) =>
                    {
                        Refresh(FSARCID);
                    };
            }
        }

        private void btnAddDocument_Click(object sender, RoutedEventArgs e)
        {
            if (EnterUploadProcess() == true)
            {
                STO.STO101_01_04 stoD = new STO.STO101_01_04(FSARCID, ARCFSTYPE, FSID, ARCFNEPISODE, ARCCHANNEL, tbxNAME.Text, lblEPISODE_NAME.Content.ToString());
                stoD.Show();
                stoD.Closing += (s, args) =>
                {
                    Refresh(FSARCID);
                };
            }
        }
        #endregion
        #region 修改及刪除資料
        private void btnDeleteClick(object sender, RoutedEventArgs e)
        {
            if (dataGrid1.SelectedItem == null)
            { MessageBox.Show("請選擇欲刪除的檔案"); }
            else if (((WSARCHIVE.Class_ARCHIVE_VAPD)dataGrid1.SelectedItem).FSIsOldUploadedFile == "Y")
            { MessageBox.Show("本檔案已入庫，無法刪除!!"); }
            else
            {
                switch (((WSARCHIVE.Class_ARCHIVE_VAPD)dataGrid1.SelectedItem).FSTableName)
                {
                    case ("TBLOG_VIDEO"):
                        client_ARCHIVE.DelTBLOG_VIDEOCompleted += (s, args) =>
                            {
                                Refresh(FSARCID);
                            };
                        client_ARCHIVE.DelTBLOG_VIDEOAsync(((WSARCHIVE.Class_ARCHIVE_VAPD)dataGrid1.SelectedItem), UserClass.userData.FSUSER_ID);
                        break;
                    case ("TBLOG_AUDIO"):
                        client_ARCHIVE.DelTBLOG_AUDIOCompleted += (s, args) =>
                            {
                                Refresh(FSARCID);
                            };
                        client_ARCHIVE.DelTBLOG_AUDIOAsync(((WSARCHIVE.Class_ARCHIVE_VAPD)dataGrid1.SelectedItem), UserClass.userData.FSUSER_ID);
                        break;
                    case ("TBLOG_PHOTO"):
                        client_ARCHIVE.DelTBLOG_PHOTOCompleted += (s, args) =>
                            {
                                Refresh(FSARCID);
                            };
                        client_ARCHIVE.DelTBLOG_PHOTOAsync(((WSARCHIVE.Class_ARCHIVE_VAPD)dataGrid1.SelectedItem), UserClass.userData.FSUSER_ID);
                        break;
                    case ("TBLOG_DOC"):
                        client_ARCHIVE.DelTBLOG_DOCCompleted += (s, args) =>
                            {
                                Refresh(FSARCID);
                            };
                        client_ARCHIVE.DelTBLOG_DOCAsync(((WSARCHIVE.Class_ARCHIVE_VAPD)dataGrid1.SelectedItem), UserClass.userData.FSUSER_ID);
                        break;
                }
                MessageBox.Show("刪除完成");
            }
        }
        //修改資料
        private void btnModifyClick(object sender, RoutedEventArgs e)
        {
            if (dataGrid1.SelectedItem == null)
            { MessageBox.Show("請選擇欲修改的資料行"); }
            else
            {
                //STO.STO101_01_01_02 sto101010102 = new STO.STO101_01_01_02(((WSARCHIVE.Class_ARCHIVE_VAPD)dataGrid1.SelectedItem));
                //sto101010102.Show();
                //sto101010102.Closing += (s, args) =>
                //{ Refresh(FSARCID); };

                STO.STO100_01_01_02 sto100010102 = new STO.STO100_01_01_02(((WSARCHIVE.Class_ARCHIVE_VAPD)dataGrid1.SelectedItem));
                sto100010102.Show();
                sto100010102.Closing += (s, args) =>
                { Refresh(FSARCID); };
            }
        }
        #endregion
        int Vcount, Acount, Pcount, Dcount;
        /// <summary>
        /// 取回TBARCHIVE_SET資料
        /// </summary>
        /// <param name="FSType">G或P</param>
        /// <param name="FSID">節目代號</param>
        /// <param name="Episode">節目集數</param>
        /// <returns></returns>
        private void ShowUploadNumber(string FSType, string FSID, string Episode)
        {

            client_ARCHIVE.GetRBARCHIVE_SET_BY_TIECondictionCompleted += (s, args) =>
                {
                    if (args.Result.Count > 0)
                    {
                        foreach (WSARCHIVE.Class_ARCHIVE_SET x in args.Result)
                        {
                            Vcount = Convert.ToInt32(x.FNVIDEO.ToString().Trim());
                            Acount = Convert.ToInt32(x.FNAUDIO.ToString().Trim());
                            Pcount = Convert.ToInt32(x.FNPHOTO.ToString().Trim());
                            Dcount = Convert.ToInt32(x.FNDOC.ToString().Trim());
                        }
                    }
                    else
                    { 
                    }
                };
            client_ARCHIVE.GetRBARCHIVE_SET_BY_TIECondictionAsync(FSType, FSID, Episode);
        }

        //是否已經從TBARCHIVE_SET取回了上傳檔案數量 true 是 false還未取過
        bool ShowUploadNumbered = false;
        bool returnBool = false;
        private bool EnterUploadProcess()
        {
            if (tbxNAME.Text == "")
            { MessageBox.Show("請填*必填欄位"); }
            else
            { returnBool = true; }
            if (returnBool == true)
            {
                if (ShowUploadNumbered == false)
                {
                    ShowUploadNumber(ARCFSTYPE, FSID, ARCFNEPISODE);
                    ShowUploadNumbered = true;
                }
                btnID.IsEnabled = false;
                btnEPISODE.IsEnabled = false;
                rdbProg.IsEnabled = false;
                rdbPromo.IsEnabled = false;
            }
            return returnBool;
        }

        private void newAFlow(string ARCHIVE_ID)
        {
            clientFlow.CallFlow_NewFlowWithFieldCompleted+= (s, args) =>
            {
                if (args.Error == null && args.Result != "")
                {
                    MessageBox.Show("表單已重新送出，請等待簽核!");
                    this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("呼叫流程作業失敗!");
                    this.DialogResult = false;
                }
            };

            //flowWebService.FlowSoapClient FlowClinet = new flowWebService.FlowSoapClient();
            //FlowClinet.NewFlowWithFieldCompleted += (s, args) =>
            //{
            //    this.DialogResult = true;
            //};

            StringBuilder sb = new StringBuilder();
            sb.Append(@"<VariableCollection>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>FormId</name>");
            sb.Append(@"    <value>" + ARCHIVE_ID + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>NextXamlName</name>");
            sb.Append(@"    <value>/STO/STO101_01.xaml</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>hid_SendTo</name>");
            sb.Append(@"    <value>" + "1" + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>Function01</name>");
            sb.Append(@"    <value>0</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>parameter01</name>");
            sb.Append(@"    <value>" + "W" + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>CancelForm</name>");
            sb.Append(@"    <value>CanCancel</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"</VariableCollection>");//hid_SendTo

            clientFlow.CallFlow_NewFlowWithFieldAsync(10, UserClass.userData.FSUSER_ID, sb.ToString());
            //FlowClinet.NewFlowWithFieldAsync(10, UserClass.userData.FSUSER_ID, sb.ToString());
        }
        private void WorkWithField(string FCCHECK_STATUS, string hid_SendTo, string FlowResult)
        {
            //flowWebService.FlowSoapClient client = new flowWebService.FlowSoapClient();
            StringBuilder sb = new StringBuilder();
            sb.Append(@"<VariableCollection>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>parameter01</name>");
            sb.Append(@"    <value>" + FCCHECK_STATUS + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>hid_SendTo</name>");
            sb.Append(@"    <value>" + hid_SendTo + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>USERID</name>");
            sb.Append(@"    <value>" + UserClass.userData.FSUSER_ID + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>USERNAME</name>");
            sb.Append(@"    <value>" + UserClass.userData.FSUSER_ChtName + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>FlowResult</name>");
            sb.Append(@"    <value>" + FlowResult + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"</VariableCollection>");//hid_SendTo

            clientFlow.CallFlow_WorkWithFieldAsync(intProcessID.ToString(), sb.ToString());
            clientFlow.CallFlow_WorkWithFieldCompleted += (s1, args1) =>
            {
                if (args1.Error == null && args1.Result != "")
                {
                    MessageBox.Show("審核成功!");
                    this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("審核失敗!");
                    this.DialogResult = false;
                }
            };
            //client.WorkWithFieldAsync(intProcessID.ToString(), sb.ToString());

            //client.WorkWithFieldCompleted += (s1, args1) =>
            //{
            //    this.DialogResult = true;
            //};
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            WorkWithField("Y", "1", "審核通過");
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            WorkWithField("F", "2","審核駁回");
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void CW_Loaded(object sender, RoutedEventArgs e)
        {
            dataGrid1.LoadingRow += (s1, args1) =>
            {
                PgmQueueDate PGMQueue = args1.Row.DataContext as PgmQueueDate;
                args1.Row.Background = new SolidColorBrush(Colors.White);
                args1.Row.Foreground = new SolidColorBrush(Colors.Black);
                int i = args1.Row.GetIndex();

                if (calCountList[i].FSIsOldUploadedFile == "Y")
                {
                    args1.Row.Background = new SolidColorBrush(Colors.Yellow);
                    args1.Row.Foreground = new SolidColorBrush(Colors.Black);
                }
            };
        }
    }
}

