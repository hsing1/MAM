﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSARCHIVE;
using System.Collections.ObjectModel;

namespace PTS_MAM3.STO
{
    public partial class STO101_01_01_01 : ChildWindow
    {
        WSARCHIVE.WSARCHIVESoapClient client = new WSARCHIVE.WSARCHIVESoapClient();
        List<Class_ARCHIVE_TBLOG_VIDEO> mylist = new List<Class_ARCHIVE_TBLOG_VIDEO>();
        public Class_ARCHIVE_TBLOG_VIDEO returnClass = new Class_ARCHIVE_TBLOG_VIDEO();
        /// <summary>
        /// 選擇已轉檔檔案頁面
        /// </summary>
        /// <param name="FSID">節目ID</param>
        /// <param name="EPIDOED">節目集數</param>
        public STO101_01_01_01(string FSID,string EPIDOED)
        {
            InitializeComponent();
            client.GetTBLOG_VIDEO_EXCHANGE_ALLCompleted += (s, args) =>
                {
                    mylist = args.Result;
                    datagrid1.ItemsSource = mylist;
                };
            client.GetTBLOG_VIDEO_EXCHANGE_ALLAsync(FSID, EPIDOED);
        }



        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            returnClass = (Class_ARCHIVE_TBLOG_VIDEO)datagrid1.SelectedItem;
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void datagrid1_RowDoubleClicked(object sender, DataGridDCTest.DataGridRowClickedArgs e)
        {
            returnClass = (Class_ARCHIVE_TBLOG_VIDEO)datagrid1.SelectedItem;
            this.DialogResult = true;
        }
    }
}

