﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.STO
{
    public partial class STO101_01_06 : ChildWindow
    {
        string FSTABLENAME, FSTYPE, FSID, FNEPISODE;
        WSARCHIVE.WSARCHIVESoapClient ARCHIVEClient = new WSARCHIVE.WSARCHIVESoapClient();
        public WSARCHIVE.Class_ARCHIVE_VAPD_METADATA ExchangeClass = new WSARCHIVE.Class_ARCHIVE_VAPD_METADATA();
        public STO101_01_06(string tableName,string FSTYPE,string FSID,string FNEPISODE)
        {
            InitializeComponent();
            this.FSTABLENAME = tableName;
            this.FSTYPE = FSTYPE;
            this.FSID = FSID;
            this.FNEPISODE = FNEPISODE;
            Refresh();
        }

        private void Refresh()
        {
            ARCHIVEClient.GetTBLOG_VAPD_EXCHANGE_METADATACompleted += (s, args) =>
            { dataGrid1.ItemsSource = args.Result; };
            ARCHIVEClient.GetTBLOG_VAPD_EXCHANGE_METADATAAsync(FSTABLENAME, FSTYPE, FSID, FNEPISODE);
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (((WSARCHIVE.Class_ARCHIVE_VAPD_METADATA)dataGrid1.SelectedItem) != null)
            {
                ExchangeClass = ((WSARCHIVE.Class_ARCHIVE_VAPD_METADATA)dataGrid1.SelectedItem);
                this.DialogResult = true;
            }
            else
            { MessageBox.Show("請選擇所要置換的檔案"); }
            
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        
    }
}

