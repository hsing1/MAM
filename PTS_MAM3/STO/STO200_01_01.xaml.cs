﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.STO
{
    public partial class STO200_01_01 : ChildWindow
    {

        public string _fsfile_no { get; set; }
        public string _rangeStart { get; set; }
        public string _rangeEnd { get; set; }

        public WSVideoMetaEdit.Class_Video_Keyframe _selectKeyframe {get; set; } 

        public STO200_01_01(string fsfile_no, string rangeStart, string rangeEnd )
        {
            InitializeComponent();

            _fsfile_no = fsfile_no;
            _rangeStart = rangeStart;
            _rangeEnd = rangeEnd;
            initKeyframe();
        }

       

        /// <summary>
        /// 初始化Keyframe List
        /// </summary>
        private void initKeyframe()
        {

         
            WSVideoMetaEdit.WSVideoMetaEditSoapClient WSVideoObj = new WSVideoMetaEdit.WSVideoMetaEditSoapClient();
            
            WSVideoObj.fnGetTBLOG_VIDEO_KEYFRAME_IN_RANGECompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        this.lbxKeyframe.ItemsSource = args.Result;
                    }
                }
            };
            

            
            WSVideoObj.fnGetTBLOG_VIDEO_KEYFRAME_IN_RANGEAsync(_fsfile_no, _rangeStart, _rangeEnd);

        }

        private void lbxKeyframe_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (lbxKeyframe.SelectedItem == null)
            {
                MessageBox.Show("請選取Keyframe!","提示" , MessageBoxButton.OK);
                return; 
            }
            else
            {
                _selectKeyframe = (WSVideoMetaEdit.Class_Video_Keyframe)lbxKeyframe.SelectedItem;
                this.DialogResult = true; 
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }


        

      
    }
}

