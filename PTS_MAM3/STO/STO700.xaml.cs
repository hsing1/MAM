﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using Telerik.Windows.Controls;

using System.Collections.ObjectModel;
using PTS_MAM3.WSReview;
using PTS_MAM3.WSDirectoryAdmin;

namespace PTS_MAM3.STO
{
    public partial class STO700 : Page
    {
        WSReview.WSReviewSoapClient myReviewSW = new WSReview.WSReviewSoapClient();
        RadTreeViewItem pubTVI = new RadTreeViewItem();
        WSReview.Class_ReviewTree myCRT = new WSReview.Class_ReviewTree();
        WSDirectoryAdmin.WSDirectoryAdminSoapClient WSDirectoryAdminClient = new WSDirectoryAdmin.WSDirectoryAdminSoapClient();

        public STO700()
        {
            InitializeComponent();

            iniTree();

            //從TBDIRECTORIES查詢資料 by Jarvis20130711
            WSDirectoryAdminClient.GetDirListByIdCompleted += new EventHandler<WSDirectoryAdmin.GetDirListByIdCompletedEventArgs>(myService_GetDirListByIdCompleted);
            //根據頻道、類型尋找所有年分 by Jarvis20130711
            myReviewSW.Get_Channel_YEARSCompleted += new EventHandler<Get_Channel_YEARSCompletedEventArgs>(myReviewSW_Get_Channel_YEARSCompleted);
            //根據頻道、類型、年分尋找入庫清單 by Jarvis20130711
            myReviewSW.GetFileList_NEWCompleted += new EventHandler<GetFileList_NEWCompletedEventArgs>(myReviewSW_GetFileList_NEWCompleted);

            myReviewSW.GetCatalog_YearsCompleted += (s1, args1) =>
                {
                    List<WSReview.Class_ReviewTree> myInsList = new List<WSReview.Class_ReviewTree>();
                    myInsList = args1.Result;
                    foreach (var item in myInsList)
                    {
                        RadTreeViewItem tvi = new RadTreeViewItem();
                        tvi.Header = item.YearID;
                        tvi.Tag = item;
                        pubTVI.Items.Add(tvi);
                    }
                    pubTVI.IsExpanded = true;
                    busyLoadingLeft.IsBusy = false;
                };
            myReviewSW.GetFSID_FSNAMECompleted += (s2, args2) =>
                {
                    List<WSReview.Class_ReviewTree> myInsList = new List<WSReview.Class_ReviewTree>();
                    myInsList = args2.Result;
                    foreach (var item in myInsList)
                    {
                        RadTreeViewItem tvi = new RadTreeViewItem();
                        tvi.Header = item.FSNAME;
                        tvi.Tag = item;
                        tvi.IsLoadOnDemandEnabled = false;
                        pubTVI.Items.Add(tvi);
                    }
                    pubTVI.IsExpanded = true;
                    busyLoadingLeft.IsBusy = false;
                };

            myReviewSW.GetVAPDCountListCompleted += (s3, args3) =>
                {
                    List<WSReview.Class_ReviewDetailList> myDetailList = new List<WSReview.Class_ReviewDetailList>();
                    myDetailList = args3.Result;
                    datagrid1.ItemsSource = myDetailList;
                    datagrid1.SelectedIndex = 0;
                    radBusyIndicator2.IsBusy = false;
                };

            //實作--尋找ID(如節目ID)帶入原本的GetVAPDCountList方法獲得詳細入庫資料 by Jarvis20130711
            myReviewSW.GetVAPDCountList_NEWCompleted += (s4, arg4) =>
            {
                List<Class_ReviewDetailList> myDetailList = new List<Class_ReviewDetailList>();
                myDetailList = arg4.Result;
                datagrid1.ItemsSource = myDetailList;
                datagrid1.SelectedIndex = 0;
                radBusyIndicator2.IsBusy = false;
            };
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

        }

        //實作-解析傳回的入庫清單 by Jarvis20130711
        void myReviewSW_GetFileList_NEWCompleted(object sender, GetFileList_NEWCompletedEventArgs e)
        {
            List<WSReview.Class_ReviewTree> myInsList = new List<WSReview.Class_ReviewTree>();
            myInsList = e.Result;
            pubTVI.Items.Clear();
            foreach (var item in myInsList)
            {
                RadTreeViewItem tvi = new RadTreeViewItem();
                tvi.Header = item.FSNAME;
                tvi.Tag = item;
                tvi.IsLoadOnDemandEnabled = false;
                pubTVI.Items.Add(tvi);
            }

            busyLoadingLeft.IsBusy = false;
            pubTVI.IsExpanded = true;
        }


        //實作-解析獲取的年份的資料 by Jarvis20130710
        void myReviewSW_Get_Channel_YEARSCompleted(object sender, Get_Channel_YEARSCompletedEventArgs e)
        {
            var tempResult = from q in e.Result
                             group q by q.YearID into g //以防萬一再group一次，順便練習LINQ
                             select g;

            if (tempResult.Count() > 0)
            {
                pubTVI.Items.Clear();
                foreach (IGrouping<string, Class_ReviewTree> crt in tempResult)
                {
                    RadTreeViewItem promoyear = new RadTreeViewItem();
                    promoyear.Header = crt.FirstOrDefault().YearID;
                    promoyear.Tag = crt.FirstOrDefault().CatalogID;
                    promoyear.IsLoadOnDemandEnabled = true;
                   
                    pubTVI.Items.Add(promoyear);
                }
            }
            this.busyLoadingLeft.IsBusy = false;
            pubTVI.IsExpanded = true;
        }

        //實作--解析從TBDIRECTORIES傳回的第一層目錄 by Jarvis20130710
        void myService_GetDirListByIdCompleted(object sender, WSDirectoryAdmin.GetDirListByIdCompletedEventArgs e)
        {
            foreach (var item in e.Result)
            {
                RadTreeViewItem channelNode = new RadTreeViewItem();
                channelNode.Header = item.FSDIR;
                channelNode.Visibility = System.Windows.Visibility.Visible;
                channelNode.Tag = item;
                channelNode.IsLoadOnDemandEnabled = true;//設成true會觸發LoadOnDemand事件

                if (item.FSDIR != "捐贈")
                {
                    RadTreeViewItem programNode = new RadTreeViewItem();
                    RadTreeViewItem promoNode = new RadTreeViewItem();
                    programNode.Header = "節目";
                    programNode.Tag = item;
                    promoNode.Header = "短帶";
                    promoNode.Tag = item;
                    programNode.IsLoadOnDemandEnabled = true;
                    promoNode.IsLoadOnDemandEnabled = true;
                    channelNode.Items.Add(programNode);
                    channelNode.Items.Add(promoNode);
                }
                TreeViewFileList.Items.Add(channelNode);
            }
            pubTVI.IsExpanded = true;
            this.busyLoadingLeft.IsBusy = false;
        }

        void iniTree()
        {
            this.busyLoadingLeft.IsBusy = true;
            //獲取第一層的目錄
            WSDirectoryAdminClient.GetDirListByIdAsync(0);

            #region 原Code 註解by Jarvis20130708
            /*
            RadTreeViewItem TVitem01 = new RadTreeViewItem();
            RadTreeViewItem TVitem07 = new RadTreeViewItem();
            RadTreeViewItem TVitem08 = new RadTreeViewItem();
            RadTreeViewItem TVitem09 = new RadTreeViewItem();
            RadTreeViewItem TVitem99 = new RadTreeViewItem();
            TVitem01.Header = "公視"; TVitem01.IsLoadOnDemandEnabled = true;
            TVitem07.Header = "客台"; TVitem07.IsLoadOnDemandEnabled = true;
            TVitem08.Header = "宏觀"; TVitem08.IsLoadOnDemandEnabled = true;
            TVitem09.Header = "原民"; TVitem09.IsLoadOnDemandEnabled = true;
            TVitem99.Header = "捐贈"; TVitem99.IsLoadOnDemandEnabled = true;
            WSReview.Class_ReviewTree station01class = new WSReview.Class_ReviewTree();
            WSReview.Class_ReviewTree station07class = new WSReview.Class_ReviewTree();
            WSReview.Class_ReviewTree station08class = new WSReview.Class_ReviewTree();
            WSReview.Class_ReviewTree station09class = new WSReview.Class_ReviewTree();
            WSReview.Class_ReviewTree station99class = new WSReview.Class_ReviewTree();
            station01class.StationID = "01"; station01class.FSID = ""; station01class.FSNAME = ""; station01class.CatalogID = ""; station01class.YearID = "";
            station07class.StationID = "07"; station07class.FSID = ""; station07class.FSNAME = ""; station07class.CatalogID = ""; station07class.YearID = "";
            station08class.StationID = "08"; station08class.FSID = ""; station08class.FSNAME = ""; station08class.CatalogID = ""; station08class.YearID = "";
            station09class.StationID = "09"; station09class.FSID = ""; station09class.FSNAME = ""; station09class.CatalogID = ""; station09class.YearID = "";
            station99class.StationID = "99"; station99class.FSID = ""; station99class.FSNAME = ""; station99class.CatalogID = ""; station99class.YearID = "";
            TVitem01.Tag = station01class;
            TVitem07.Tag = station07class;
            TVitem08.Tag = station08class;
            TVitem09.Tag = station09class;
            TVitem99.Tag = station99class;
            TreeViewFileList.Items.Add(TVitem01);
            TreeViewFileList.Items.Add(TVitem07);
            TreeViewFileList.Items.Add(TVitem08);
            TreeViewFileList.Items.Add(TVitem09);
            TreeViewFileList.Items.Add(TVitem99);
            //TreeViewFileList.AddHandler*/
            #endregion
        }

        //建樹的主要邏輯，依照FullPath分層做事 by Jarvis20130710
        void BuildTreeMethod(Telerik.Windows.RadRoutedEventArgs e)
        {
            pubTVI = ((RadTreeViewItem)(e.Source));
            if (pubTVI.IsExpanded)
            {
                //如果已經展開代表已經選取過,就收起來不做其他事
                pubTVI.IsExpanded = false;
                return;
            }
            this.busyLoadingLeft.IsBusy = true;

            string path = pubTVI.FullPath.Replace("\\", ";");
            string[] pathArr = path.Split(new char[] { ';' });

            switch (pathArr.Length)
            {
                case 1://第一層,資料帶抓年分
                    if (pathArr[0] == "捐贈")
                    {
                        myReviewSW.Get_Channel_YEARSAsync(pathArr[0], "D");
                    }
                    this.busyLoadingLeft.IsBusy = false;
                    break;

                case 2://第二層,資料帶是年分要抓檔案,節目與短帶要抓年分
                    if (pathArr[1] == "節目")
                    {
                        myReviewSW.Get_Channel_YEARSAsync(pathArr[0], "G");
                    }
                    else if (pathArr[1] == "短帶")
                    {
                        myReviewSW.Get_Channel_YEARSAsync(pathArr[0], "P");
                    }
                    else //資料帶抓入庫清單
                    {
                        myReviewSW.GetFileList_NEWAsync("D", pathArr[0], pathArr[1]);
                    }
                    break;

                case 3://第三層,資料帶取入庫詳細資料,節目與短帶取入庫清單
                    if (pathArr[1] == "節目")
                    {
                        myReviewSW.GetFileList_NEWAsync("G", pathArr[0], pathArr[2]);
                    }
                    else if (pathArr[1] == "短帶")
                    {
                        myReviewSW.GetFileList_NEWAsync("P", pathArr[0], pathArr[2]);
                    }
                    else
                    {
                        this.busyLoadingLeft.IsBusy = false;
                        radBusyIndicator2.IsBusy = true;

                        myReviewSW.GetVAPDCountList_NEWAsync("D", pathArr[2]);
                    }
                    break;

                case 4://節目與短帶取入庫詳細資料。
                    this.busyLoadingLeft.IsBusy = false;
                    radBusyIndicator2.IsBusy = true;
                    if (pathArr[1] == "節目")
                    {
                        myReviewSW.GetVAPDCountList_NEWAsync("G", pathArr[pathArr.Length - 1]);
                    }
                    else if (pathArr[1] == "短帶")
                    {
                        myReviewSW.GetVAPDCountList_NEWAsync("P", pathArr[pathArr.Length - 1]);
                    }
                    break;
            }
        }

        //第一次點選時觸發
        private void TreeViewFileList_LoadOnDemand(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            if (((RadTreeViewItem)(e.Source)).IsLoadOnDemandEnabled)
            {
                BuildTreeMethod(e);
                ((RadTreeViewItem)(e.Source)).IsLoadOnDemandEnabled = false;
            }
            #region 原Code 註解 by Jarvis20130708
            /*
             * pubTVI = ((RadTreeViewItem)(e.Source));
            WSReview.Class_ReviewTree myCRT = new WSReview.Class_ReviewTree();
            myCRT = (WSReview.Class_ReviewTree)((RadTreeViewItem)(e.Source)).Tag;
            if (myCRT.StationID == "99")
                myCRT.CatalogID = "D";
            //因為要求即時，因此每次點選都重新去找，LoadOnDemand就不作事          
            //((RadTreeViewItem)(e.Source)).IsLoadOnDemandEnabled = false;
            #region 加入分類 G節目 P短帶 D資料帶
            if (myCRT.StationID != "" && myCRT.StationID != "99" && myCRT.CatalogID == "")
            {
                RadTreeViewItem tvi01 = new RadTreeViewItem();
                RadTreeViewItem tvi02 = new RadTreeViewItem();

                WSReview.Class_ReviewTree cr01 = new WSReview.Class_ReviewTree();
                WSReview.Class_ReviewTree cr02 = new WSReview.Class_ReviewTree();

                cr01.StationID = myCRT.StationID; cr01.CatalogID = "G"; cr01.YearID = "";
                cr02.StationID = myCRT.StationID; cr02.CatalogID = "P"; cr02.YearID = "";

                tvi01.Header = "節目"; tvi01.IsLoadOnDemandEnabled = true; tvi01.Tag = cr01;
                tvi02.Header = "短帶"; tvi02.IsLoadOnDemandEnabled = true; tvi02.Tag = cr02;

                ((RadTreeViewItem)(e.Source)).Items.Add(tvi01);
                ((RadTreeViewItem)(e.Source)).Items.Add(tvi02);
            }
            #endregion
            #region 取得年分
            if ((myCRT.StationID!="" && myCRT.CatalogID!="" && myCRT.YearID=="") )
            {//取得年分類
                busyLoadingLeft.IsBusy = true;
                myReviewSW.GetCatalog_YearsAsync(myCRT);
            }
            #endregion
            #region 取得FSID 與FSNAME
            if (myCRT.YearID != "" && myCRT.FSID == "" && myCRT.FSNAME == "")
            {
                busyLoadingLeft.IsBusy = true;
                myReviewSW.GetFSID_FSNAMEAsync(myCRT);
            }
            #endregion
            ((RadTreeViewItem)(e.Source)).IsLoadOnDemandEnabled = false;
            */
            #endregion
        }

        //選取時觸發
        private void TreeViewFileList_Selected(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            BuildTreeMethod(e);
          
            #region 原Code 註解by Jarvis20130708
            /*
              
            pubTVI = ((RadTreeViewItem)(e.Source));
            myCRT = (WSReview.Class_ReviewTree)((RadTreeViewItem)(e.Source)).Tag;
            if (myCRT.FSID!="" && myCRT.FSNAME!="")
            {
                radBusyIndicator2.IsBusy = true;
                myReviewSW.GetVAPDCountListAsync(myCRT);
            }

            //RadTreeViewItem treeNode = (RadTreeViewItem)e.Source;

            ////取得目錄列表，取得前先刪除ViewItem            
            //preLoadDirectoryList(treeNode.Tag.ToString(), treeNode);

            ////取得檔案列表，取得前先刪除ListBox
            ////lbFileList.Items.Clear();  //現在用Binding的方式，不用清除ListBox
            //m_strFileDirectory = treeNode.Tag.ToString();
            //client.GetDirectorieFilesAsync(treeNode.Tag.ToString());
            */
            #endregion
        }


        private void FileListButton_Click(object sender, RoutedEventArgs e)
        {
            //註解 by Jarvis20130709
            //if ((WSReview.Class_ReviewDetailList)(datagrid1.SelectedItem) != null)
            //{
            //    STO.STO700_01 mySTO700_01 = new STO700_01((WSReview.Class_ReviewDetailList)(datagrid1.SelectedItem), this.myCRT.CatalogID);
            //    mySTO700_01.Show();
            //}
            //else
            //{ MessageBox.Show("請選擇節目子集"); }

            if ((WSReview.Class_ReviewDetailList)(datagrid1.SelectedItem) != null)
            {
                STO.STO700_01 mySTO700_01 = new STO700_01((WSReview.Class_ReviewDetailList)(datagrid1.SelectedItem));
                mySTO700_01.Show();
            }
            else
            { MessageBox.Show("請選擇節目子集"); }

        }

        private void buttonSrhName_Click(object sender, RoutedEventArgs e)
        {
            if (textBox1.Text != "")
            {
                STO700_02 pgSTO700_02 = new STO700_02(textBox1.Text.Trim());
                pgSTO700_02.Show();
                pgSTO700_02.Closing += new EventHandler<System.ComponentModel.CancelEventArgs>(pgSTO700_02_Closing);
            }
        }

        void pgSTO700_02_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (((STO700_02)sender).DialogResult == true)
            {
                WSReview.Class_ReviewTree myCRT = new WSReview.Class_ReviewTree();
                myCRT.CatalogID = ((STO700_02)sender).sel_GPD_Class.GPD;
                myCRT.FSID = ((STO700_02)sender).sel_GPD_Class.FSID;
                myCRT.FSNAME = ((STO700_02)sender).sel_GPD_Class.FSNAME;
                if (myCRT.FSID != "" && myCRT.FSNAME != "")
                {
                    radBusyIndicator2.IsBusy = true;
                    myReviewSW.GetVAPDCountListAsync(myCRT);
                }
                //((STO700_02)sender).sel_GPD_Class
            }
        }
    }
}
