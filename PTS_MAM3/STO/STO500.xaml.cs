﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.SR_WSTSM;

namespace PTS_MAM3.STO
{
    public partial class STO500 : ChildWindow
    {
        string aaFileName;
        public STO500(string FileName)
        {
            InitializeComponent();
            //sLVideoPlayer1.
            PreviewPlayer.SLVideoPlayer player = new PreviewPlayer.SLVideoPlayer("", 0);
            ServiceTSMSoapClient tsm = new ServiceTSMSoapClient();
            tsm.GetHttpPathByFileIDCompleted += (s, args) =>
            {
                if ((args.Error != null) || String.IsNullOrEmpty(args.Result))
                {
                    MessageBox.Show("找不到檔案!");
                    //RadBusyIndicator_Check.IsBusy = false;
                    return;
                }

                //player.HTML_SetURL(args.Result, _start_timecode);
                //將Video加置Grid
                //this.Grid_Show.Children.Add(player);
                LayoutRoot.Children.Add(player);
                //aaFileName = @"http://10.13.220.2/Media/Lv/G2011099000002.wmv";
                aaFileName = args.Result;

                player.HTML_SetURL(aaFileName, 0);
                //player.HTML_SetFrame(0);
                
                //this.RadBusyIndicator_Check.IsBusy = false;
            };

            tsm.GetHttpPathByFileIDAsync(FileName, FileID_MediaType.LowVideo);
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

