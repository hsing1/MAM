﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO;

namespace PTS_MAM3.STO
{
    public partial class STO700_01 : ChildWindow
    {
        WSReview.WSReviewSoapClient myReviewSW = new WSReview.WSReviewSoapClient();
        WSReview.Class_ReviewDetailList pubReviewDetail=new WSReview.Class_ReviewDetailList();
        List<WSReview.ClassReviewFile> VList = new List<WSReview.ClassReviewFile>();
        List<WSReview.ClassReviewFile> AList = new List<WSReview.ClassReviewFile>();
        List<WSReview.ClassReviewFile> PList = new List<WSReview.ClassReviewFile>();
        List<WSReview.ClassReviewFile> DList = new List<WSReview.ClassReviewFile>();
       //註解 by Jarvis20130709
        //public STO700_01(WSReview.Class_ReviewDetailList iniClass,string categroyID)
        //{
        //    InitializeComponent();
        //    pubReviewDetail=iniClass;
        //    myReviewSW.GetVAPDFILESCompleted += new EventHandler<WSReview.GetVAPDFILESCompletedEventArgs>(myReviewSW_GetVAPDFILESCompleted);
        //    VRefresh(categroyID);
        //    ARefresh(categroyID);
        //    PRefresh(categroyID);
        //    DRefresh(categroyID);
        //}
        public STO700_01(WSReview.Class_ReviewDetailList iniClass)
        {
            InitializeComponent();
            pubReviewDetail = iniClass;
            myReviewSW.GetVAPDFILESCompleted += new EventHandler<WSReview.GetVAPDFILESCompletedEventArgs>(myReviewSW_GetVAPDFILESCompleted);
            VRefresh(iniClass.CatalogID);
            ARefresh(iniClass.CatalogID);
            PRefresh(iniClass.CatalogID);
            DRefresh(iniClass.CatalogID);
        }
        void myReviewSW_GetVAPDFILESCompleted(object sender, WSReview.GetVAPDFILESCompletedEventArgs e)
        {
            List<WSReview.ClassReviewFile> tempList = new List<WSReview.ClassReviewFile>();
            tempList = e.Result;
            string myItem = "";
            
            
            
            foreach (var item in tempList)
            {
                if (item.FSTB == "V")
                { VList.Add(item); }
                if (item.FSTB == "A")
                    AList.Add(item);
                if (item.FSTB == "P")
                    PList.Add(item);
                if (item.FSTB == "D")
                    DList.Add(item);
                myItem = item.FSTB;
            }

            //VRefresh();
            //ARefresh();
            //PRefresh();
            //DRefresh();
            if (myItem=="V")
                dataGridV.ItemsSource = VList;
            else if(myItem=="A")
                dataGridA.ItemsSource = AList;
            else if(myItem=="P")
                dataGridP.ItemsSource = PList;
            else if(myItem=="D")
                dataGridD.ItemsSource = DList;

            dataGridV.SelectedIndex = 0;
            dataGridA.SelectedIndex = 0;
            dataGridP.SelectedIndex = 0;
            dataGridD.SelectedIndex = 0;

            
        }

        private void VRefresh(string _categoryID)
        {
            VList = new List<WSReview.ClassReviewFile>();
            myReviewSW.GetVAPDFILESAsync(pubReviewDetail, "V", _categoryID);
        }

        private void ARefresh(string _categoryID)
        {
            AList = new List<WSReview.ClassReviewFile>();
            myReviewSW.GetVAPDFILESAsync(pubReviewDetail, "A", _categoryID);
        }

        private void PRefresh(string _categoryID)
        {
            PList = new List<WSReview.ClassReviewFile>();
            myReviewSW.GetVAPDFILESAsync(pubReviewDetail, "P", _categoryID);
        }

        private void DRefresh(string _categoryID)
        {
            DList = new List<WSReview.ClassReviewFile>();
            myReviewSW.GetVAPDFILESAsync(pubReviewDetail, "D", _categoryID);
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void tabControl1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //if (((TabItem)(((TabControl)sender).SelectedItem)).Name == "tabItemV")
            //{
            //    STO200BTN.Visibility = Visibility.Visible;
            //}
            //else
            //{ STO200BTN.Visibility = Visibility.Collapsed; }
            //if (((TabItem)(((TabControl)sender).SelectedItem)).Name == "tabItemV")
            //{ VRefresh(); }
            //if (((TabItem)(((TabControl)sender).SelectedItem)).Name == "tabItemA")
            //{ ARefresh(); }
            //if (((TabItem)(((TabControl)sender).SelectedItem)).Name == "tabItemP")
            //{ PRefresh(); }
            //if (((TabItem)(((TabControl)sender).SelectedItem)).Name == "tabItemD")
            //{ DRefresh(); }
            //if (tabItemV.IsSelected==true)
            //{
            //    VRefresh();
            //}
            //if (tabItemA.IsSelected == true)
            //{
            //    ARefresh();
            //}
            //if (tabItemP.IsSelected == true)
            //{
            //    PRefresh();
            //}
            //if (tabItemD.IsSelected == true)
            //{
            //    DRefresh();
            //}
            //if (((TabItem)(tabControl1.SelectedItem)).Name=="tabItemV")
            //{
            //    VRefresh();
            //}
            //if (((TabItem)(tabControl1.SelectedItem)).Name=="tabItemA")
            //{
            //    ARefresh();
            //}
            //if (((TabItem)(tabControl1.SelectedItem)).Name=="tabItemP")
            //{
            //    PRefresh();
            //}
            //if (((TabItem)(tabControl1.SelectedItem)).Name=="tabItemD")
            //{
            //    DRefresh();
            //}
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            if (tabItemV.IsSelected == true && ((WSReview.ClassReviewFile)(dataGridV.SelectedItem)) != null)
            {
                STO.STO700_01_02 pSTO700_01_02V = new STO700_01_02((WSReview.ClassReviewFile)(dataGridV.SelectedItem));
                pSTO700_01_02V.Show();
                //(WSReview.ClassReviewFile)(dataGridV.SelectedItem) 
            }
            else if (tabItemA.IsSelected == true && ((WSReview.ClassReviewFile)(dataGridA.SelectedItem)) != null)
            {
                STO.STO700_01_02 pSTO700_01_02A = new STO700_01_02((WSReview.ClassReviewFile)(dataGridA.SelectedItem));
                pSTO700_01_02A.Show();
            }
            else if (tabItemP.IsSelected == true && ((WSReview.ClassReviewFile)(dataGridP.SelectedItem)) != null)
            {
                STO.STO700_01_02 pSTO700_01_02P = new STO700_01_02((WSReview.ClassReviewFile)(dataGridP.SelectedItem));
                pSTO700_01_02P.Show();
            }
            else if (tabItemD.IsSelected == true && ((WSReview.ClassReviewFile)(dataGridD.SelectedItem)) != null)
            {
                STO.STO700_01_02 pSTO700_01_02D = new STO700_01_02((WSReview.ClassReviewFile)(dataGridD.SelectedItem));
                pSTO700_01_02D.Show();
            }

        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            WCF100DL.WCF100Client WCF100client = new WCF100DL.WCF100Client();

            if (tabItemV.IsSelected == true && ((WSReview.ClassReviewFile)dataGridV.SelectedItem) != null)
            {
                STO.STO500 STO500Page = new STO500(((WSReview.ClassReviewFile)dataGridV.SelectedItem).FSFILE_NO);
                STO500Page.Show();
                //(WSReview.ClassReviewFile)(dataGridV.SelectedItem) 
            }
            else if (tabItemA.IsSelected==true)
            {
                dialog.Filter = ((WSReview.ClassReviewFile)dataGridA.SelectedItem).FSFILE_TYPE + "|*." + ((WSReview.ClassReviewFile)dataGridA.SelectedItem).FSFILE_TYPE;
                dialog.FilterIndex = 1;
                dialog.ShowDialog();
                try
                {
                    WCF100client.DownloadAsync(((WSReview.ClassReviewFile)dataGridA.SelectedItem).FSFILE_PATH);
                    WCF100client.DownloadCompleted += (s1, args1) =>
                    {
                        if (args1.Error != null)
                        {
                            MessageBox.Show(args1.Error.ToString());
                            return;
                        }

                        if (args1.Result != null)
                        {
                            try
                            {
                                Stream fileobj = dialog.OpenFile();
                                if (fileobj == null)
                                {
                                }
                                else
                                {
                                    fileobj.Write(args1.Result.File, 0, args1.Result.File.Length);
                                    fileobj.Close();
                                }
                            }
                            catch (Exception ex)
                            {
                                string myex = ex.Message;
                                return;
                            }
                        }
                    };
                }
                catch (Exception ex)
                { MessageBox.Show(ex.ToString().Trim()); }
            }
            else if (tabItemP.IsSelected==true)
            {
                dialog.Filter = ((WSReview.ClassReviewFile)dataGridP.SelectedItem).FSFILE_TYPE + "|*." + ((WSReview.ClassReviewFile)dataGridP.SelectedItem).FSFILE_TYPE;
                dialog.FilterIndex = 1;
                    //.jpeg .bmp .tiff .png .gif
                if (((WSReview.ClassReviewFile)dataGridP.SelectedItem).FSFILE_TYPE == "jpeg" || ((WSReview.ClassReviewFile)dataGridP.SelectedItem).FSFILE_TYPE == "jpg" || ((WSReview.ClassReviewFile)dataGridP.SelectedItem).FSFILE_TYPE == "bmp" || ((WSReview.ClassReviewFile)dataGridP.SelectedItem).FSFILE_TYPE == "tiff" || ((WSReview.ClassReviewFile)dataGridP.SelectedItem).FSFILE_TYPE == "png" || ((WSReview.ClassReviewFile)dataGridP.SelectedItem).FSFILE_TYPE == "gif")
                {
                    try
                    {
                        STO100_08 STO100_08 = new STO100_08(((WSReview.ClassReviewFile)dataGridP.SelectedItem).FSFILE_NO, ((WSReview.ClassReviewFile)dataGridP.SelectedItem).FSFILE_PATH);
                        STO100_08.Show();
                        //HtmlPage.Window.Navigate(new Uri(selectClass.FSTSMPath, UriKind.RelativeOrAbsolute), "_blank", "height=700,width=900,top=100,left=100,toolbar=0,statusbar=0,menubar=0,scrollbar=0");
                    }
                    catch (Exception ex)
                    { MessageBox.Show(ex.ToString()); }
                }
                else
                {
                    dialog.ShowDialog();
                    try
                    {
                        WCF100client.DownloadAsync(((WSReview.ClassReviewFile)dataGridP.SelectedItem).FSFILE_PATH);
                        WCF100client.DownloadCompleted += (s1, args1) =>
                        {
                            if (args1.Error != null)
                            {
                                MessageBox.Show(args1.Error.ToString());
                                return;
                            }

                            if (args1.Result != null)
                            {
                                try
                                {
                                    Stream fileobj = dialog.OpenFile();
                                    if (fileobj == null)
                                    {
                                    }
                                    else
                                    {
                                        fileobj.Write(args1.Result.File, 0, args1.Result.File.Length);
                                        fileobj.Close();
                                    }
                                }
                                catch (Exception ex)
                                {
                                    return;
                                }
                            }
                        };
                    }
                    catch (Exception ex)
                    { MessageBox.Show(ex.ToString().Trim()); }
                }
            }
            else if (tabItemD.IsSelected==true)
            {
                dialog.Filter = ((WSReview.ClassReviewFile)dataGridD.SelectedItem).FSFILE_TYPE + "|*." + ((WSReview.ClassReviewFile)dataGridD.SelectedItem).FSFILE_TYPE;
                dialog.FilterIndex = 1;
                if ((((WSReview.ClassReviewFile)dataGridD.SelectedItem).FSFILE_TYPE == "txt") || ((WSReview.ClassReviewFile)dataGridD.SelectedItem).FSFILE_TYPE == "rtf")
                {
                    STO100_09 STO100_09 = new STO100_09(((WSReview.ClassReviewFile)dataGridD.SelectedItem).FSFILE_NO);//(selectClass.FSFile_No, selectClass.FSTSMPath);
                    STO100_09.Show();
                }
                else
                {
                    dialog.ShowDialog();
                    try
                    {
                        WCF100client.DownloadAsync(((WSReview.ClassReviewFile)dataGridD.SelectedItem).FSFILE_PATH);
                        WCF100client.DownloadCompleted += (s1, args1) =>
                        {
                            if (args1.Error != null)
                            {
                                MessageBox.Show(args1.Error.ToString());
                                return;
                            }

                            if (args1.Result != null)
                            {
                                try
                                {
                                    Stream fileobj = dialog.OpenFile();
                                    if (fileobj == null)
                                    {
                                    }
                                    else
                                    {
                                        fileobj.Write(args1.Result.File, 0, args1.Result.File.Length);
                                        fileobj.Close();
                                    }
                                }
                                catch (Exception ex)
                                {
                                    return;
                                }
                            }
                        };
                    }
                    catch (Exception ex)
                    { MessageBox.Show(ex.ToString().Trim()); }
                }
            }
            #region 瀏覽檔案
            //if (((WSARCHIVE.Class_ARCHIVE_VAPD)dataGrid1.SelectedItem) != null)
            //{
            //    WCF100DL.WCF100Client WCF100client = new WCF100DL.WCF100Client();
            //    WSARCHIVE.Class_ARCHIVE_VAPD selectClass = ((WSARCHIVE.Class_ARCHIVE_VAPD)dataGrid1.SelectedItem);
            //    SaveFileDialog dialog = new SaveFileDialog();
            //    dialog.Filter = selectClass.FSFileType + "|*." + selectClass.FSFileType;
            //    dialog.FilterIndex = 1;
            //    string fileNo = ((WSARCHIVE.Class_ARCHIVE_VAPD)dataGrid1.SelectedItem).FSFile_No;
            //    switch (((WSARCHIVE.Class_ARCHIVE_VAPD)dataGrid1.SelectedItem).FSTableName)
            //    {
            //        case ("TBLOG_VIDEO"):
            //            STO.STO500 STO500Page = new STO500(selectClass.FSFile_No);
            //            STO500Page.Show();
            //            break;
            //        case ("TBLOG_AUDIO"):
            //            dialog.ShowDialog();
            //            try
            //            {
            //                WCF100client.DownloadAsync(selectClass.FSTSMPath);
            //                WCF100client.DownloadCompleted += (s1, args1) =>
            //                {
            //                    if (args1.Error != null)
            //                    {
            //                        MessageBox.Show(args1.Error.ToString());
            //                        return;
            //                    }

            //                    if (args1.Result != null)
            //                    {
            //                        try
            //                        {
            //                            Stream fileobj = dialog.OpenFile();
            //                            if (fileobj == null)
            //                            {
            //                            }
            //                            else
            //                            {
            //                                fileobj.Write(args1.Result.File, 0, args1.Result.File.Length);
            //                                fileobj.Close();
            //                            }
            //                        }
            //                        catch (Exception ex)
            //                        {
            //                            return;
            //                        }
            //                    }
            //                };
            //            }
            //            catch (Exception ex)
            //            { MessageBox.Show(ex.ToString().Trim()); }
            #endregion
        }

        private void STO200BTN_Click(object sender, RoutedEventArgs e)
        {
            if (((WSReview.ClassReviewFile)(dataGridV.SelectedItem)) != null)
            {
                STO200 pSTO200 = new STO200(((WSReview.ClassReviewFile)(dataGridV.SelectedItem)).FSFILE_NO);
                pSTO200.Show();
            }
            else
            { MessageBox.Show("請選擇影片檔案"); }
        }


    }
}

