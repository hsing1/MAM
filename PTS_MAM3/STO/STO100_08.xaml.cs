﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Media.Imaging;
using PTS_MAM3.WSTSM_GetFileInfo;


namespace PTS_MAM3.STO
{
    public partial class STO100_08 : ChildWindow
    {
        ServiceTSMSoapClient tsm_photo = new ServiceTSMSoapClient();
        public STO100_08(string FileNo,string strImageSource)
        {
            InitializeComponent();
            string IS;
            
            string[] result;
            char[] charSeparators = { '.' };
            string[] stringSeparator = { "." };

            tsm_photo.GetHttpPathByFileIDAsync(FileNo, FileID_MediaType.Photo);
            tsm_photo.GetHttpPathByFileIDCompleted += (s, args) =>
            {
                if ((args.Error != null) || String.IsNullOrEmpty(args.Result))
                {
                    //this.RadBusyIndicator_AllSearchResult.IsBusy = false;
                    return;
                }
                string strPath = "";    
                Image img = new Image();
                img.Width = 320;
                img.Height = 320;
                IS = args.Result;
                result = IS.Split(stringSeparator, StringSplitOptions.None);
                for (int i = 0; i < result.Count()-1; i++)
                {
                    if (i == result.Count() - 2)
                        strPath = strPath + result[i];
                    else
                        strPath = strPath + result[i] + ".";
                }
                    //img.Source = new BitmapImage(new Uri(strPath + "_M.jpg", UriKind.Absolute));
                img.Source = new BitmapImage(new Uri(strPath + "_M.jpg", UriKind.Absolute));
                //this.Grid_Video.Children.Add(img);
                this.image1.Source = img.Source;
                //this.RadBusyIndicator_AllSearchResult.IsBusy = false;

            };
           // image1.Source = new BitmapImage(new Uri(@"\\\\mamstream\\MAMDFS\\Media\\Photo\\ptstv\\2011\\2011108\\G201110800010051.jpg", UriKind.RelativeOrAbsolute));

        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

