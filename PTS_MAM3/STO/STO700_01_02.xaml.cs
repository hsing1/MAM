﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSPROG_M;
using Telerik.Windows.Controls;
using PTS_MAM3.WSARCHIVE;

namespace PTS_MAM3.STO
{
    public partial class STO700_01_02 : ChildWindow
    {
        public Class_ARCHIVE_VAPD pageclass = new Class_ARCHIVE_VAPD();
        WSARCHIVE.WSARCHIVESoapClient client = new WSARCHIVESoapClient();
        WSLogTemplateField.WSLogTemplateFieldSoapClient TFclient = new WSLogTemplateField.WSLogTemplateFieldSoapClient();
        Class_ARCHIVE_VAPD_METADATA VAPD_METADATA = new Class_ARCHIVE_VAPD_METADATA();
        public string VAPD="", FSFILE_NO, FNSEQ_NO="0";

        public List<WSLogTemplateField.LogTemplateField> LogList = new List<WSLogTemplateField.LogTemplateField>();
        public List<WSLogTemplateField.LogTemplateField> LogListInsert = new List<WSLogTemplateField.LogTemplateField>();
        public STO700_01_02(WSReview.ClassReviewFile myclass)
        {
            InitializeComponent();
            
            pageclass.FSFile_No = myclass.FSFILE_NO;
            pageclass.FSTSMPath = myclass.FSFILE_PATH;
            if (myclass.FSTB=="V")
                pageclass.FSTableName = "TBLOG_VIDEO";
            else if (myclass.FSTB=="A")
                pageclass.FSTableName = "TBLOG_AUDIO";
            else if (myclass.FSTB == "P")
                pageclass.FSTableName = "TBLOG_PHOTO";
            else if (myclass.FSTB == "D")
                pageclass.FSTableName = "TBLOG_DOC";
            if (pageclass.FSTableName == "TBLOG_VIDEO")
            { 
                button1.Visibility = Visibility.Visible;
                //listBox1.Visibility = Visibility.Collapsed;
            }
            client.GetTBLOG_VAPD_METADATACompleted += (s, args) =>
                {
                    VAPD_METADATA = args.Result;
                    if (VAPD_METADATA.MTTitle == null)
                    { textBox1.Text = ""; }
                    else
                    { textBox1.Text = VAPD_METADATA.MTTitle; }
                    if (VAPD_METADATA.MTDescription == null)
                    { textBox2.Text = ""; }
                    else
                    { textBox2.Text = VAPD_METADATA.MTDescription; }
                    if (VAPD_METADATA.FSSUPERVISOR == null)
                    { this.DialogResult = false; }
                    else
                    {
                        if (VAPD_METADATA.FSSUPERVISOR=="Y")
                        {
                            fileCheckBox.IsChecked = true;
                        }
                        else if (VAPD_METADATA.FSSUPERVISOR=="N")
                        { fileCheckBox.IsChecked = false; }
                    }
                    DFieldInit();
                };
            client.GetTBLOG_VAPD_METADATAAsync(pageclass);
            BusyIndicator1.IsBusy = true;
            client.UpdTBLOG_VAPD_METADATACompleted += new EventHandler<UpdTBLOG_VAPD_METADATACompletedEventArgs>(client_UpdTBLOG_VAPD_METADATACompleted);
        }

        void client_UpdTBLOG_VAPD_METADATACompleted(object sender, UpdTBLOG_VAPD_METADATACompletedEventArgs e)
        {
            if (e.Result == true)
            {
                DFieldUPD();
            }
            else
            {
                MessageBox.Show("請檢查網路狀態");
                this.DialogResult = false;
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            VAPD_METADATA.MTTitle = textBox1.Text;
            VAPD_METADATA.MTDescription = textBox2.Text;
            VAPD_METADATA.FSUpdatedBy = UserClass.userData.FSUSER_ID;
            if (fileCheckBox.IsChecked==true)
            { VAPD_METADATA.FSSUPERVISOR = "Y"; }
            else if (fileCheckBox.IsChecked==false)
            { VAPD_METADATA.FSSUPERVISOR = "N"; }
            client.UpdTBLOG_VAPD_METADATAAsync(VAPD_METADATA);
        }
        private void DFieldUPD()
        {
            /*Dennis.Wen: 因應自訂欄位增加列舉型態, 本段程式已大改, 依照不同型態產生不同控制項.*/

            for (int i = 0; i < listBox1.Items.Count(); i++)
            {
                listBox1.SelectedIndex = i;
                StackPanel boxitem = (StackPanel)listBox1.SelectedItem;

                WSLogTemplateField.LogTemplateField x = (WSLogTemplateField.LogTemplateField)LogList[i]; 

                if (x.FSCODE_CTRL == "")
                { 
                    ((WSLogTemplateField.LogTemplateField)boxitem.Tag).FSFIELD_VALUE = ((TextBox)boxitem.Children[2]).Text;
                }
                else
                {
                    switch (x.FSCODE_CTRL)
                    {
                        case "ComboBox":
                            if (((PTS_MAM3.WSLogTemplateField.LogTemplateField_CODE)(((ComboBox)(boxitem.Children[2])).SelectedItem)).FSCODE_NAME.StartsWith("(已停用)"))
                            {
                                MessageBox.Show("不可選擇已停用的代碼項目.", "提示訊息", MessageBoxButton.OK);
                                return;
                            }
                            ((WSLogTemplateField.LogTemplateField)boxitem.Tag).FSFIELD_VALUE = ((ComboBox)(boxitem.Children[2])).SelectedValue.ToString();
                            break;
                        case "CheckBox":
                            String _Value = ""; 
                            int _Cnt = 0;
                            WrapPanel wrap = (WrapPanel)(boxitem.Children[2]);

                            for (int k = 0; k < wrap.Children.Count(); k++)
                            { 
                                CheckBox chk = ((CheckBox)wrap.Children[k]);
                                if (chk.IsChecked == true) 
                                {
                                    _Cnt += 1;
                                    _Value += chk.Tag + ";";
                                }
                            }

                            if (int.Parse(x.FSCODE_CNT) > 0 && _Cnt > int.Parse(x.FSCODE_CNT))
                            {
                                MessageBox.Show("欄位「" + x.FSFIELD_NAME + "」的勾選限制設定為" + int.Parse(x.FSCODE_CNT) + "個項目.","提示訊息", MessageBoxButton.OK);
                                return;
                            }
                            
                            ((WSLogTemplateField.LogTemplateField)boxitem.Tag).FSFIELD_VALUE = _Value;
                            break;
                    }
                }  

                if (x.FCISNULLABLE =="N" && ((WSLogTemplateField.LogTemplateField)boxitem.Tag).FSFIELD_VALUE.Trim() == "")
                {
                    MessageBox.Show("欄位「" + x.FSFIELD_NAME + "」必填欄位.", "提示訊息", MessageBoxButton.OK);
                    return;
                }
                LogListInsert.Add(((WSLogTemplateField.LogTemplateField)boxitem.Tag));
            }
            
            ////Dennis.Wen: 以下是原來寫法
            //for (int x = 0; x < listBox1.Items.Count(); x++)
            //{
            //    listBox1.SelectedIndex = x;
            //    StackPanel boxitem = (StackPanel)listBox1.SelectedItem;
            //    ((WSLogTemplateField.LogTemplateField)boxitem.Tag).FSFIELD_VALUE = ((TextBox)boxitem.Children[1]).Text;
            //    LogListInsert.Add(((WSLogTemplateField.LogTemplateField)boxitem.Tag)); 
            //}

            if (LogListInsert.Count > 0)
            {
                TFclient.SetTemplateFieldsAsync(VAPD, FSFILE_NO, Convert.ToInt32(FNSEQ_NO), LogListInsert, UserClass.userData.FSUSER_ID);
            }
            else
            {
                MessageBox.Show("資料修改成功");
                this.DialogResult = true;
            }
            //{ };
            TFclient.SetTemplateFieldsCompleted += (s, args) =>
            {
                if (args.Result == true)
                {
                    MessageBox.Show("資料修改成功");
                    this.DialogResult = true;
                }
                else
                { MessageBox.Show("請檢查輸入欄位"); }
            };
        }
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            STO200 sto200 = new STO200(pageclass.FSFile_No.Trim());
            sto200.Show();
        }

        /*Dennis.Wen: 因應自訂欄位增加列舉型態, 本段程式已大改, 依照不同型態產生不同控制項.*/
        private void DFieldInit()
        {
            //string VAPD = "";
            if (VAPD_METADATA.FSTableName == "TBLOG_VIDEO")
                VAPD = "V";
            else if (VAPD_METADATA.FSTableName == "TBLOG_AUDIO")
                VAPD = "A";
            else if (VAPD_METADATA.FSTableName == "TBLOG_PHOTO")
                VAPD = "P";
            else if (VAPD_METADATA.FSTableName == "TBLOG_DOC")
                VAPD = "D";
            //STO.STO300 sto300 = new STO300(VAPD, VAPD_METADATA.FSFile_No, "0");
            //sto300.Show();
            FSFILE_NO = VAPD_METADATA.FSFile_No;
            TFclient.GetTemplateFieldsAsync(VAPD, FSFILE_NO, FNSEQ_NO);
            TFclient.GetTemplateFieldsCompleted += (s, args) =>
            {
                LogList = args.Result;
                int i = 0;
                foreach (WSLogTemplateField.LogTemplateField x in LogList)
                {
                    StackPanel SP = new StackPanel();
                    Label lbl = new Label();
                    Label lbl2 = new Label();
                    Control ctrl; 

                    lbl.Content = x.FSFIELD_NAME;
                    lbl.Width = 100;

                    if (x.FCISNULLABLE == "N")
                    {
                        lbl2.Content = "*";
                    }
                    else
                    {
                        lbl2.Content = " ";
                    }
                    lbl2.Foreground = new SolidColorBrush(Colors.Red);
                    lbl2.Width = 10;

                    if (x.FSCODE_CTRL == "")//此欄位為空表示用一般的文字框控制項即可
                    {
                        ctrl = new TextBox();

                        ctrl.Name = "txtBox" + i.ToString();
                        ctrl.Width = Convert.ToInt32(x.FNOBJECT_WIDTH);
                        ((TextBox)ctrl).MaxLength = Convert.ToInt32(x.FNFIELD_LENGTH);
                        
                        ((TextBox)ctrl).TextWrapping = TextWrapping.Wrap;

                        ((TextBox)ctrl).Text = x.FSFIELD_VALUE;

                        SP.Tag = x;
                        SP.Orientation = Orientation.Horizontal;
                        SP.Children.Insert(0, ctrl);
                        SP.Children.Insert(0, lbl2);
                        SP.Children.Insert(0, lbl);
                    }else{ 
                        switch (x.FSCODE_CTRL)
                        {
                            case "ComboBox":
                                ctrl = new ComboBox();
                                ctrl.Name = "cbxBox" + i.ToString();
                                ctrl.Width = Convert.ToInt32(x.FNOBJECT_WIDTH);

                                ((ComboBox)ctrl).DisplayMemberPath = "FSCODE_NAME";
                                ((ComboBox)ctrl).SelectedValuePath = "FSCODE_CODE";

                                List<WSLogTemplateField.LogTemplateField_CODE> CodeList = new List<WSLogTemplateField.LogTemplateField_CODE>();

                                if (x.CODELIST.Count > 0)
                                {
                                    CodeList.Add(
                                        new WSLogTemplateField.LogTemplateField_CODE
                                        {
                                            FSCODE_ID = "",
                                            FSCODE_CODE = "",
                                            FSCODE_NAME = "(未選擇)",
                                            FSCODE_ENAME = "",
                                            FSCODE_ORDER = "",

                                            FSCODE_SET = "",
                                            FSCODE_NOTE = "",
                                            FSCODE_CHANNEL_ID = "",

                                            FSCREATED_BY = "",
                                            FDCREATED_DATE = DateTime.Parse("1900/01/01"),
                                            FSUPDATED_BY = "",
                                            FDUPDATED_DATE = DateTime.Parse("1900/01/01")
                                        }
                                    );


                                    foreach (WSLogTemplateField.LogTemplateField_CODE code in x.CODELIST)
                                    {
                                        CodeList.Add(code);
                                    }

                                    ((ComboBox)ctrl).ItemsSource = CodeList; 

                                    if (x.FSFIELD_VALUE != "")
                                    {
                                        ((ComboBox)ctrl).SelectedValue = x.FSFIELD_VALUE;
                                    }
                                    else
                                    {
                                        ((ComboBox)ctrl).SelectedIndex = 0;
                                    }
                                }

                                SP.Tag = x;
                                SP.Orientation = Orientation.Horizontal;
                                SP.Children.Insert(0, ctrl);
                                SP.Children.Insert(0, lbl2);
                                SP.Children.Insert(0, lbl);


                                break;

                            case "CheckBox":
                                WrapPanel wrap = new WrapPanel();
                                wrap.Orientation = Orientation.Horizontal;
                                wrap.Width = Convert.ToInt32(x.FNOBJECT_WIDTH);
                                wrap.Name = "wrapPnl" + i.ToString();
                
                                if (x.CODELIST.Count > 0)
                                {
                                    int j = 0;
                                    foreach (WSLogTemplateField.LogTemplateField_CODE code in x.CODELIST)
                                    {
                                        CheckBox chk = new CheckBox();
                                        chk.Name = "chkBox" + i.ToString() + "_" + j.ToString();

                                        chk.Content = code.FSCODE_NAME + "　";
                                        chk.Tag = code.FSCODE_CODE;

                                        if (code.FSCODE_ISENABLED != true) chk.IsEnabled = false; //<=====

                                        if (x.FSFIELD_VALUE.Contains(code.FSCODE_CODE + ";")) chk.IsChecked = true;

                                        wrap.Children.Insert(wrap.Children.Count, chk);

                                        j++;
                                    }

                                }

                                SP.Tag = x;
                                SP.Orientation = Orientation.Horizontal;
                                SP.Children.Insert(0, wrap);
                                SP.Children.Insert(0, lbl2);
                                SP.Children.Insert(0, lbl);

                                break;
                        }                       
                    }
                     
                    listBox1.Items.Add(SP);
                    i++;
                } 
            };

            this.BusyIndicator1.IsBusy = false;
        }

        private void listBox1_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {

        }

        
        //private void InitField()
        //{
        //    //Telerik.Windows.Controls.RadToolBar i = new RadToolBar();
        //    TFclient.GetTemplateFieldsAsync(VAPD, FSFILE_NO);
        //    TFclient.GetTemplateFieldsCompleted += (s, args) =>
        //    {
        //        LogList = args.Result;
        //        int i = 0;
        //        foreach (WSLogTemplateField.LogTemplateField x in LogList)
        //        {
        //            StackPanel SP = new StackPanel();
        //            Label lbl = new Label();
        //            TextBox txtBox = new TextBox();
        //            lbl.Content = x.FSFIELD_NAME;
        //            txtBox.Name = txtBox + i.ToString();
        //            lbl.Width = 100;
        //            txtBox.MaxLength = Convert.ToInt32(x.FNFIELD_LENGTH);
        //            if (x.FSFIELD_VALUE == "")
        //            { }//txtBox.Text = x.FSDEFAULT; }
        //            else
        //                txtBox.Text = x.FSFIELD_VALUE;
        //            txtBox.Width = Convert.ToInt32(x.FNOBJECT_WIDTH);
        //            SP.Tag = x;
        //            SP.Orientation = Orientation.Horizontal;
        //            SP.Children.Insert(0, txtBox);
        //            SP.Children.Insert(0, lbl);
        //            listBox1.Items.Add(SP);
        //            i++;
        //        }
        //        //StackPanel newSP = new StackPanel();
        //        //TextBox txtbox = new TextBox();
        //        //txtbox.Text = "THIS IS TXTBOX";
        //        //txtbox.Width = 100;
        //        //newSP.Orientation = Orientation.Horizontal;
        //        ////Label lbl = new Label();
        //        //lbl.Content = "XXXX";
        //        //newSP.Children.Insert(0, txtbox);
        //        //newSP.Children.Insert(0, lbl);
        //        ////stackPanel1.Children.Insert(1, newSP);
        //        //listBox1.Items.Add(newSP);
        //    };

        //    //listBox1.Items.Add(lbl);
        //}
    }
}

