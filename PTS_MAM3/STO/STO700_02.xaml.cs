﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.STO
{
    public partial class STO700_02 : ChildWindow
    {
        WSReview.WSReviewSoapClient myclient = new WSReview.WSReviewSoapClient();
        List<WSReview.Class_GPD_Name> DGList = new List<WSReview.Class_GPD_Name>();
        public WSReview.Class_GPD_Name sel_GPD_Class = new WSReview.Class_GPD_Name();
        public STO700_02(string iniCondiction)
        {
            InitializeComponent();
            myclient.GetGPDNameListCompleted += new EventHandler<WSReview.GetGPDNameListCompletedEventArgs>(myclient_GetGPDNameListCompleted);
            myclient.GetGPDNameListAsync(iniCondiction);
        }

        void myclient_GetGPDNameListCompleted(object sender, WSReview.GetGPDNameListCompletedEventArgs e)
        {
            DGList = e.Result;
            datagrid1.ItemsSource = DGList;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (((WSReview.Class_GPD_Name)(datagrid1.SelectedItem)) !=null)
	        {
                sel_GPD_Class = ((WSReview.Class_GPD_Name)(datagrid1.SelectedItem));
                this.DialogResult = true;
	        }
            
            
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

