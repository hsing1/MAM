﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSPROG_D;
using System.Xml.Linq;
using System.IO;
using System.Text;
using DataGridDCTest;
using PTS_MAM3.WSPROG_M;

namespace PTS_MAM3.STO
{
    public partial class STO100_30 : ChildWindow  //查詢節目子集資料
    {
        WSPROG_DSoapClient client = new WSPROG_DSoapClient();  //產生新的代理類別

        public string m_strProgID = "";            //帶入的節目編號
        public string m_strProgName = "";          //帶入的節目名稱
        public string m_strEpisode_View = "";       //選取的集別
        public string m_strProgDName_View = "";     //選取的子集名稱
        public string m_strPlayEpisode_View = "";   //選取的播出集別
        public List<string> ListCheck = new List<string>();
        public List<Class_PROGD> allList = new List<Class_PROGD>();
        public List<Class_PROGD> allCheckedList = new List<Class_PROGD>();

        public STO100_30(string ProgID ,string ProgName)
        {
            InitializeComponent();
            m_strProgID=ProgID.ToString().Trim();
            m_strProgName = ProgName.ToString().Trim();

            InitializeForm();        //初始化本頁面
        }

        void InitializeForm() //初始化本頁面
        {
            //取得全部節目子集資料
            client.GetTBPROG_D_BYPROGID_ALLCompleted += new EventHandler<GetTBPROG_D_BYPROGID_ALLCompletedEventArgs>(client_GetTBPROG_D_BYPROGID_ALLCompleted);
            client.GetTBPROG_D_BYPROGID_ALLAsync(m_strProgID);
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息

            //查詢子集資料By集別和節目名稱 
            client.QProg_D_BYEPISODE_PGDNAMECompleted += new EventHandler<QProg_D_BYEPISODE_PGDNAMECompletedEventArgs>(client_fnQProg_D_BYEPISODE_PGDNAMECompleted);
        }
        
        //實作-查詢子集資料By集別和節目名稱 
        void client_fnQProg_D_BYEPISODE_PGDNAMECompleted(object sender, QProg_D_BYEPISODE_PGDNAMECompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    //this.DGPROG_DListD.DataContext = e.Result;
                    allList = e.Result;
                    WasThereEverStored();
                    //if (this.DGPROG_DListD.DataContext != null)
                    //{
                    //    DGPROG_DListD.SelectedIndex = 0;
                    //}
                }
                else
                {
                    MessageBox.Show("該節目查無節目子集資料，請重新輸入查詢條件！", "提示訊息", MessageBoxButton.OK);
                    return;
                }
            }
        }

        //實作-取得全部節目子集資料
        void client_GetTBPROG_D_BYPROGID_ALLCompleted(object sender, GetTBPROG_D_BYPROGID_ALLCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    //this.DGPROG_DListD.DataContext = e.Result;
                    allList = e.Result;
                    WasThereEverStored();
                    //if (this.DGPROG_DListD.DataContext != null)
                    //{
                    //    DGPROG_DListD.SelectedIndex = 0;
                    //}
                }
                else
                {
                    MessageBox.Show("該節目查無節目子集資料，請重新選擇節目名稱！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
        }
        
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            ////判斷是否有選取DataGrid
            //if (DGPROG_DList.SelectedItem == null)
            //{
            //    MessageBox.Show("請選擇節目子集資料", "提示訊息", MessageBoxButton.OK);
            //    return;
            //}
            //else
            //{
            //    m_strEpisode_View = ((Class_PROGD)DGPROG_DList.SelectedItem).FNEPISODE.ToString();
            //    m_strProgDName_View = ((Class_PROGD)DGPROG_DList.SelectedItem).FSPGDNAME.ToString();

            //    this.DialogResult = true;
            //}

            //判斷是否有選取DataGrid
            if (DGPROG_DListD.SelectedItem == null)
            {
                MessageBox.Show("請選擇節目子集資料", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else
            {
                m_strEpisode_View = ((Class_PROGD)DGPROG_DListD.SelectedItem).FNEPISODE.ToString();
                m_strProgDName_View = ((Class_PROGD)DGPROG_DListD.SelectedItem).FSPGDNAME.ToString();
                m_strPlayEpisode_View = ((Class_PROGD)DGPROG_DListD.SelectedItem).FNSHOWEPISODE.ToString();
                this.DialogResult = true;
            }   
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //條件查詢
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            short shortCheck;   //純粹為了判斷是否為數字型態之用
            string strEpisode = tbxEPISODE.Text.ToString().Trim() ;
            string strProgDName=tbxEPISODENAME.Text.ToString().Trim();

            if (short.TryParse(strEpisode, out shortCheck) == false && strEpisode != "")
                MessageBox.Show("請檢查「集別」欄位必須為數值型態", "提示訊息", MessageBoxButton.OK);
            else
            {
                client.QProg_D_BYEPISODE_PGDNAMEAsync(m_strProgID, strEpisode, strProgDName);
                BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
            }                 
        }

        //重新查詢
        private void btnAll_Click(object sender, RoutedEventArgs e)
        {
            tbxEPISODE.Text="";
            tbxEPISODENAME.Text = "";
            //client.GetTBPROG_D_ALLAsync(m_strProgID);
            client.GetTBPROG_D_BYPROGID_ALLAsync(m_strProgID);
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }

        //連點事件
        private void RowDoubleClick(object sender, DataGridDCTest.DataGridRowClickedArgs e)
        {
            DoubleClickDataGrid dcdg = sender as DoubleClickDataGrid;
            //Class_PROGM a = (dcdg.SelectedItem) as Class_PROGM;
            //MessageBox.Show(a.FSPGMNAME, "提示訊息", MessageBoxButton.OK);
            m_strEpisode_View = ((Class_PROGD)dcdg.SelectedItem).FNEPISODE.ToString();
            m_strProgDName_View = ((Class_PROGD)dcdg.SelectedItem).FSPGDNAME.ToString();
            m_strPlayEpisode_View = ((Class_PROGD)DGPROG_DListD.SelectedItem).FNSHOWEPISODE.ToString();
            this.DialogResult = true;
        }

        WSWasThereEverStored.WSWasThereEverStoredSoapClient storedClient = new WSWasThereEverStored.WSWasThereEverStoredSoapClient();
        List<WSWasThereEverStored.Class_ARCHIVE> checkStoredList = new List<WSWasThereEverStored.Class_ARCHIVE>();
        private void WasThereEverStored()
        {
            storedClient.CheckStored_GAsync();
            storedClient.CheckStored_GCompleted += (s, args) =>
            {
                allCheckedList = new List<Class_PROGD>();
                checkStoredList = args.Result;
                bool everStored = false;
                foreach (Class_PROGD y in allList)
                {
                    foreach (WSWasThereEverStored.Class_ARCHIVE z in checkStoredList)
                    {
                        if ((y.FSPROG_ID == z.FSID)&&(y.FNEPISODE.ToString()==z.FNEPISODE_NAME))
                        {
                            everStored = true;
                            allCheckedList.Add(y);

                        }
                    }
                }
                if (allCheckedList.Count > 0)
                {
                    this.DGPROG_DListD.DataContext = allCheckedList;
                    if (this.DGPROG_DListD.DataContext != null)
                    {
                        DGPROG_DListD.SelectedIndex = 0;
                    }
                }
                else
                { MessageBox.Show("子集尚未有入庫紀錄!"); }
            };
        }
    }
}

