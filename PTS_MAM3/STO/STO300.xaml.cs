﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;


namespace PTS_MAM3.STO
{
    public partial class STO300 : ChildWindow
    {
        WSLogTemplateField.WSLogTemplateFieldSoapClient client=new WSLogTemplateField.WSLogTemplateFieldSoapClient();
        public string VAPD, FSFILE_NO, FNSEQ_NO;
        public List<WSLogTemplateField.LogTemplateField> LogList = new List<WSLogTemplateField.LogTemplateField>();
        public List<WSLogTemplateField.LogTemplateField> LogListInsert = new List<WSLogTemplateField.LogTemplateField>();
        public STO300(string fstable_type, string fsfile_no,string fsseq_no)
        {
            VAPD = fstable_type;
            FSFILE_NO = fsfile_no;
            FNSEQ_NO = fsseq_no;
            InitializeComponent();
            InitField();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            for (int x = 0; x < listBox1.Items.Count(); x++)
            { 
                listBox1.SelectedIndex = x;
                StackPanel boxitem = (StackPanel)listBox1.SelectedItem;
                ((WSLogTemplateField.LogTemplateField)boxitem.Tag).FSFIELD_VALUE = ((TextBox)boxitem.Children[1]).Text;
                LogListInsert.Add(((WSLogTemplateField.LogTemplateField)boxitem.Tag));
                //((WSLogTemplateField.LogTemplateField)boxitem.Tag).FSFIELD_VALUE = boxitem.Children.FindChildByType<TextBox>().Text.ToString();
               // ((WSLogTemplateField.LogTemplateField)boxitem.Tag).FSFIELD_VALUE = //boxitem.Children.ElementAt<TextBox>(0).Text;
              //  LogListInsert.Add(((WSLogTemplateField.LogTemplateField)boxitem.Tag));
            }

            client.SetTemplateFieldsAsync(VAPD, FSFILE_NO, Convert.ToInt32(FNSEQ_NO), LogListInsert, UserClass.userData.FSUSER_ID);
                //{ };
            client.SetTemplateFieldsCompleted += (s, args) =>
                {
                    if (args.Result == true)
                    {
                        this.DialogResult = true;
                    }
                    else
                    { MessageBox.Show("請檢查輸入欄位"); }
                };
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void InitField()
        {
            //Telerik.Windows.Controls.RadToolBar i = new RadToolBar();
            client.GetTemplateFieldsAsync(VAPD, FSFILE_NO, FNSEQ_NO);
            client.GetTemplateFieldsCompleted += (s, args) =>
                {
                    LogList = args.Result;
                    int i = 0;
                    foreach (WSLogTemplateField.LogTemplateField x in LogList)
                    { 
                        StackPanel SP = new StackPanel();
                        Label lbl = new Label();
                        TextBox txtBox = new TextBox();
                        lbl.Content = x.FSFIELD_NAME;
                        txtBox.Name = txtBox + i.ToString();
                        lbl.Width=100;
                        txtBox.MaxLength = Convert.ToInt32(x.FNFIELD_LENGTH);
                        if (x.FSFIELD_VALUE == "")
                        { }//txtBox.Text = x.FSDEFAULT; }
                        else
                            txtBox.Text = x.FSFIELD_VALUE;
                        txtBox.Width = Convert.ToInt32(x.FNOBJECT_WIDTH);
                        SP.Tag = x;
                        SP.Orientation = Orientation.Horizontal;
                        SP.Children.Insert(0, txtBox);
                        SP.Children.Insert(0, lbl);
                        listBox1.Items.Add(SP);
                        i++;
                    }
                    //StackPanel newSP = new StackPanel();
                    //TextBox txtbox = new TextBox();
                    //txtbox.Text = "THIS IS TXTBOX";
                    //txtbox.Width = 100;
                    //newSP.Orientation = Orientation.Horizontal;
                    ////Label lbl = new Label();
                    //lbl.Content = "XXXX";
                    //newSP.Children.Insert(0, txtbox);
                    //newSP.Children.Insert(0, lbl);
                    ////stackPanel1.Children.Insert(1, newSP);
                    //listBox1.Items.Add(newSP);
                };
        
            //listBox1.Items.Add(lbl);
        }
            //<telerik:RadToolBar telerik:StyleManager.Theme="Windows7" OverflowButtonVisibility="Collapsed" GripVisibility="Collapsed" Grid.RowSpan="1"  Margin="0,0,0,0" Name="radToolBar1">
            //<Button x:Name="ViewDetail" Click="ViewDetail_Click">
            //    <StackPanel Orientation="Horizontal">
            //        <Image Source="../Images/16_Done Square.png" Width="16" Height="16" />
            //        <TextBlock Text="查看選取流程" Margin="2,0,0,0" HorizontalAlignment="Right" VerticalAlignment="Center" />
            //    </StackPanel>
            //</Button>

         //CheckBox cbCHANNEL = new CheckBox();
         //                       cbCHANNEL.Content = CodeData.NAME;
         //                       cbCHANNEL.Tag = CodeData.ID;
         //                       listCHANNEL.Items.Add(cbCHANNEL);
         //   for (int i = 0; i < listCHANNEL.Items.Count; i++)
         //   {
         //       CheckBox getckb = (CheckBox)listCHANNEL.Items.ElementAt ;
         //       if (getckb.IsChecked == true)
         //       {
         //           strReturn += getckb.Tag.ToString().Trim() + ";";
         //       }
         //   }
    }
}

