﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using PTS_MAM3.WSMAMFunctions;
using PTS_MAM3.WSARCHIVE;
using System.Collections.ObjectModel;

namespace PTS_MAM3.STO
{
    /// <summary>
    /// 入庫置換新增"影"TBLOG_VIDEO
    /// </summary>
    public partial class STO101_01_01 : ChildWindow
    {
        public List<ClassUploadFileClass> ClassUploadFileClassList = new List<ClassUploadFileClass>();
        public List<Class_CODE_TBFILE_TYPE> AllClassCodeList = new List<Class_CODE_TBFILE_TYPE>();
        WSARCHIVESoapClient ARCHIVEclient = new WSARCHIVESoapClient();
        Class_ARCHIVE_VAPD_METADATA FileExchangeClass = new Class_ARCHIVE_VAPD_METADATA();
        //接回來來自於"選擇已上傳頁面"的CLASS
        List<Class_ARCHIVE_TBLOG_VIDEO> returnedClassList = new List<Class_ARCHIVE_TBLOG_VIDEO>();

        //來自於main window property
        string mwpARC_ID = "", mwpFSTYPE = "", mwpFSID = "", mwpEPISODE = "", mwpChannel = "";
        /// <summary>
        /// 起始頁面
        /// </summary>
        /// <param name="ARC_ID">來自於新增</param>
        /// <param name="FSTYPE">辨別節目帶(G)或是宣傳帶的代碼(P)</param>
        /// <param name="FSID">節目的Id</param>
        /// <param name="FNEPISODE">節目集數</param>
        /// <param name="Channel">節目所屬頻道Id ex:公視 "01"</param>
        public STO101_01_01(string ARC_ID, string FSTYPE, string FSID, string FNEPISODE, string Channel)
        {
            InitializeComponent();
            //將來自於main window property的值放到公用變數裡去
            mwpARC_ID = ARC_ID;
            mwpEPISODE = FNEPISODE;
            mwpFSID = FSID;
            mwpFSTYPE = FSTYPE;//G:節目帶 P:宣傳帶
            mwpChannel = Channel;
        }

        /// <summary>
        /// 按下畫面中的確認鍵
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (returnedClassList != null && FileExchangeClass.FSFile_No != null)
            {
                OKButton.IsEnabled = false;
                CancelButton.IsEnabled = false;
                foreach (Class_ARCHIVE_TBLOG_VIDEO x in returnedClassList)
                {
                    x.strFSCHANGE_FILE_NO = FileExchangeClass.FSFile_No;
                }
                ARCHIVEclient.UpdTBLOG_Video_STTCompleted += (s, args) =>
                {
                    if (args.Result == true)
                    {
                        MessageBox.Show("新增已完成");
                        this.DialogResult = true;
                    }
                    else
                    {
                        MessageBox.Show("請重新選取檔案");
                        this.DialogResult = false;
                    }
                };
                ARCHIVEclient.UpdTBLOG_Video_STTAsync(returnedClassList);
            }
            else
            { MessageBox.Show("請選擇置換的檔案及被置換的檔案!!"); }
        }

        /// <summary>
        /// 按下畫面中的取消鍵
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }



        private void willExchange(object sender, RoutedEventArgs e)
        {
            STO101_01_01_01 sto = new STO101_01_01_01(mwpFSID, mwpEPISODE);
            sto.Show();
            sto.Closing += (s, args) =>
                {
                    if (sto.DialogResult == true)
                    {
                        sto.returnClass.strARC_ID = mwpARC_ID;
                        sto.returnClass.strUserId = UserClass.userData.FSUSER_ID;
                        returnedClassList.Clear();
                        returnedClassList.Add(sto.returnClass);
                    }
                };
            sto.Closed += (s, args) =>
            { 
                dataGrid1.ItemsSource = null;
                dataGrid1.ItemsSource = returnedClassList;
            };
        }
        private void button1_Click(object sender, RoutedEventArgs e)
        {
            STO.STO101_01_06 SelectPage = new STO.STO101_01_06("TBLOG_VIDEO", mwpFSTYPE, mwpFSID, mwpEPISODE);
            SelectPage.Show();
            SelectPage.Closing += (s, args) =>
            {
                if (SelectPage.DialogResult == true)
                {
                    FileExchangeClass = SelectPage.ExchangeClass;
                    label4.Content = FileExchangeClass.FSFile_No.Trim() + " 將被置換!!";
                }
            };
        }
        private void btnUserDel_Click(object sender, RoutedEventArgs e)
        {

            if (dataGrid1.SelectedItem == null)
            { MessageBox.Show("請選擇檔案"); }
            returnedClassList.Remove((Class_ARCHIVE_TBLOG_VIDEO)dataGrid1.SelectedItem);
            dataGrid1.ItemsSource = null;
            dataGrid1.ItemsSource = returnedClassList;

        }
        private void button2_Click(object sender, RoutedEventArgs e)
        {
            STO.STO101_01_06 SelectPage = new STO.STO101_01_06("TBLOG_VIDEO", mwpFSTYPE, mwpFSID, mwpEPISODE);
            SelectPage.Show();
            SelectPage.Closing += (s, args) =>
            {
                if (SelectPage.DialogResult == true)
                {
                    FileExchangeClass = SelectPage.ExchangeClass;
                    label4.Content = FileExchangeClass.FSOldFileName.Trim() + " 將被置換!!";
                }
            };
        }
    }
}

