﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;

namespace PTS_MAM3.STO
{
    public class STO400:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string strStatus = value.ToString(); //需要轉換的值
            if (!string.IsNullOrEmpty(strStatus))
                if (strStatus.Equals("Y")) //當值等於Male時
                {
                    return "通過";//傳回男生圖片路徑
                }
                else if (strStatus.Equals("F")) //當值等於Female時
                {
                    return "不通過";//傳回女生圖片路徑
                }
                else if (strStatus.Equals("N"))
                { return "審核中"; }
                else if (strStatus.Equals("W"))
                { return "審核中"; }
                else if (strStatus.Equals("X"))
                { return "抽單"; }

            return "";
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }
}
