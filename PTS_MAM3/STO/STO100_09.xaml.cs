﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.STO
{
    public partial class STO100_09 : ChildWindow
    {
        public WSARCHIVE.WSARCHIVESoapClient WSClient = new WSARCHIVE.WSARCHIVESoapClient();
        public STO100_09(string FileNo)
        {
            WSClient.PreViewDocContentAsync(FileNo);
            WSClient.PreViewDocContentCompleted += (s, args) =>
                { textBlock1.Text = args.Result; };
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

