﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSPROG_M;
using Telerik.Windows.Controls;
using PTS_MAM3.WSARCHIVE;

namespace PTS_MAM3.STO
{
    public partial class STO101_01_01_02 : ChildWindow
    {
        public Class_ARCHIVE_VAPD pageclass;
        WSARCHIVE.WSARCHIVESoapClient client = new WSARCHIVESoapClient();
        WSLogTemplateField.WSLogTemplateFieldSoapClient TFclient = new WSLogTemplateField.WSLogTemplateFieldSoapClient();
        Class_ARCHIVE_VAPD_METADATA VAPD_METADATA = new Class_ARCHIVE_VAPD_METADATA();
        public string VAPD = "", FSFILE_NO, FNSEQ_NO = "0";

        public List<WSLogTemplateField.LogTemplateField> LogList = new List<WSLogTemplateField.LogTemplateField>();
        public List<WSLogTemplateField.LogTemplateField> LogListInsert = new List<WSLogTemplateField.LogTemplateField>();
        public STO101_01_01_02(Class_ARCHIVE_VAPD myclass)
        {
            InitializeComponent();
            pageclass = myclass;
            if (myclass.FSTableName == "TBLOG_VIDEO")
            { button1.Visibility = Visibility.Visible; }
            client.GetTBLOG_VAPD_METADATACompleted += (s, args) =>
                { 
                    VAPD_METADATA = args.Result;
                    if (VAPD_METADATA.MTTitle == null)
                    { textBox1.Text = ""; }
                    else
                    { textBox1.Text = VAPD_METADATA.MTTitle; }
                    if (VAPD_METADATA.MTDescription == null)
                    { textBox2.Text = ""; }
                    else
                    { textBox2.Text = VAPD_METADATA.MTDescription; }
                    DFieldInit();
                };
            client.GetTBLOG_VAPD_METADATAAsync(myclass);
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            VAPD_METADATA.MTTitle = textBox1.Text;
            VAPD_METADATA.MTDescription = textBox2.Text;
            client.UpdTBLOG_VAPD_METADATACompleted += (s, args) =>
                {
                    if (args.Result == true)
                    {
                        DFieldUPD();
                    }
                    else
                    { 
                        MessageBox.Show("請檢查網路狀態");
                        this.DialogResult = false;
                    }
                };
            client.UpdTBLOG_VAPD_METADATAAsync(VAPD_METADATA);
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            STO200 sto200 = new STO200(pageclass.FSFile_No.Trim());
            sto200.Show();
        }

        private void DFieldUPD()
        {
            for (int x = 0; x < listBox1.Items.Count(); x++)
            {
                listBox1.SelectedIndex = x;
                StackPanel boxitem = (StackPanel)listBox1.SelectedItem;
                ((WSLogTemplateField.LogTemplateField)boxitem.Tag).FSFIELD_VALUE = ((TextBox)boxitem.Children[1]).Text;
                LogListInsert.Add(((WSLogTemplateField.LogTemplateField)boxitem.Tag));
                //((WSLogTemplateField.LogTemplateField)boxitem.Tag).FSFIELD_VALUE = boxitem.Children.FindChildByType<TextBox>().Text.ToString();
                // ((WSLogTemplateField.LogTemplateField)boxitem.Tag).FSFIELD_VALUE = //boxitem.Children.ElementAt<TextBox>(0).Text;
                //  LogListInsert.Add(((WSLogTemplateField.LogTemplateField)boxitem.Tag));
            }

            if (LogListInsert.Count > 0)
            {
                TFclient.SetTemplateFieldsAsync(VAPD, FSFILE_NO, Convert.ToInt32(FNSEQ_NO), LogListInsert, UserClass.userData.FSUSER_ID);
            }
            else
            {
                MessageBox.Show("資料修改成功");
                this.DialogResult = true;
            }
            //{ };
            TFclient.SetTemplateFieldsCompleted += (s, args) =>
            {
                if (args.Result == true)
                {
                    MessageBox.Show("資料修改成功");
                    this.DialogResult = true;
                }
                else
                { MessageBox.Show("請檢查輸入欄位"); }
            };
        }
        private void DFieldInit()
        {
            //string VAPD = "";
            if (VAPD_METADATA.FSTableName == "TBLOG_VIDEO")
                VAPD = "V";
            else if (VAPD_METADATA.FSTableName == "TBLOG_AUDIO")
                VAPD = "A";
            else if (VAPD_METADATA.FSTableName == "TBLOG_PHOTO")
                VAPD = "P";
            else if (VAPD_METADATA.FSTableName == "TBLOG_DOC")
                VAPD = "D";
            //STO.STO300 sto300 = new STO300(VAPD, VAPD_METADATA.FSFile_No, "0");
            //sto300.Show();
            FSFILE_NO = VAPD_METADATA.FSFile_No;
            TFclient.GetTemplateFieldsAsync(VAPD, FSFILE_NO, FNSEQ_NO);
            TFclient.GetTemplateFieldsCompleted += (s, args) =>
            {
                LogList = args.Result;
                int i = 0;
                foreach (WSLogTemplateField.LogTemplateField x in LogList)
                {
                    StackPanel SP = new StackPanel();
                    Label lbl = new Label();
                    TextBox txtBox = new TextBox();
                    lbl.Content = x.FSFIELD_NAME;
                    txtBox.Name = txtBox + i.ToString();
                    lbl.Width = 100;
                    txtBox.MaxLength = Convert.ToInt32(x.FNFIELD_LENGTH);
                    if (x.FSFIELD_VALUE == "")
                    { }//txtBox.Text = x.FSDEFAULT; }
                    else
                        txtBox.Text = x.FSFIELD_VALUE;
                    txtBox.Width = Convert.ToInt32(x.FNOBJECT_WIDTH);
                    SP.Tag = x;
                    SP.Orientation = Orientation.Horizontal;
                    SP.Children.Insert(0, txtBox);
                    SP.Children.Insert(0, lbl);
                    listBox1.Items.Add(SP);
                    i++;
                }
                //StackPanel newSP = new StackPanel();
                //TextBox txtbox = new TextBox();
                //txtbox.Text = "THIS IS TXTBOX";
                //txtbox.Width = 100;
                //newSP.Orientation = Orientation.Horizontal;
                ////Label lbl = new Label();
                //lbl.Content = "XXXX";
                //newSP.Children.Insert(0, txtbox);
                //newSP.Children.Insert(0, lbl);
                ////stackPanel1.Children.Insert(1, newSP);
                //listBox1.Items.Add(newSP);
            };
        }
    }
}

