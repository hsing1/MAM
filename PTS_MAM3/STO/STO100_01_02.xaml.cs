﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using PTS_MAM3.WSMAMFunctions;
using PTS_MAM3.WSARCHIVE;
using System.Collections.ObjectModel;

namespace PTS_MAM3.STO
{
    /// <summary>
    /// 入庫表單新增，上傳"音"TBLOG_AUDIO
    /// </summary>
    public partial class STO100_01_02 : ChildWindow
    {
        public List<ClassUploadFileClass> ClassUploadFileClassList = new List<ClassUploadFileClass>();
        public List<Class_CODE_TBFILE_TYPE> AllClassCodeList = new List<Class_CODE_TBFILE_TYPE>();
        WSARCHIVESoapClient ARCHIVEclient = new WSARCHIVESoapClient();
        string UploadFolderURL = "";// PTSMAMResource.RS_ARCHIVE_UPloadFolder;//@"\\10.13.220.2\uploadfolder\Audios";
        //來自於main window property
        string mwpARC_ID = "", mwpFSTYPE = "", mwpFSID = "", mwpEPISODE = "", mwpChannel = "", mwpFSID_NAME = "", mwpFNEPISODE_NAME = "";
        /// <summary>
        /// 起始頁面
        /// </summary>
        /// <param name="ARC_ID">來自於新增</param>
        /// <param name="FSTYPE">辨別節目帶(G)或是宣傳帶的代碼(P)</param>
        /// <param name="FSID">節目的Id</param>
        /// <param name="FNEPISODE">節目集數</param>
        /// <param name="Channel">節目所屬頻道Id ex:公視 "01"</param>
        public STO100_01_02(string ARC_ID, string FSTYPE, string FSID, string FNEPISODE, string Channel,string FSID_NAME,string FNEPISODE_NAME)
        {
            InitializeComponent();
            //將來自於main window property的值放到公用變數裡去
            mwpARC_ID = ARC_ID;
            mwpEPISODE = FNEPISODE;
            mwpFSID = FSID;
            mwpFSTYPE = FSTYPE;//G:節目帶 P:宣傳帶
            mwpChannel = Channel;
            mwpFSID_NAME = FSID_NAME;
            mwpFNEPISODE_NAME = FNEPISODE_NAME;
            radUpload1.UploadServiceUrl = @"../RadUploadHandler.ashx";
            radUpload1.MaxFileSize = Convert.ToInt64(PTSMAMResource.MaxUploadFileSize);
            ARCHIVEclient.UploadFolderPATHCompleted += (s, path) =>
                {
                    UploadFolderURL = path.Result;
                    radUpload1.TargetPhysicalFolder = UploadFolderURL;
                };
            ARCHIVEclient.UploadFolderPATHAsync();
            //把資料填入combox1
            GetCode_TBFILE_TYPE();
        }

        /// <summary>
        /// 呼叫WSARCHIVE中的"GetTBARCHIVE_CODE_TBFILE_TYPE"方法填入代碼檔
        /// </summary>
        private void GetCode_TBFILE_TYPE()
        {
            ARCHIVEclient.GetTBARCHIVE_CODE_TBFILE_TYPECompleted += (s, args) =>
            {
                AllClassCodeList = args.Result;
                var g = from t in AllClassCodeList
                        where t.TABLENAME == "TBFILE_TYPE" && t.FSTYPE == "音"
                        orderby t.SORT
                        select t;
                foreach (Class_CODE x in g)
                {
                    ComboBoxItem cbi = new ComboBoxItem();
                    cbi.Content = x.NAME;
                    cbi.Tag = x.ID;
                    comboBox1.Items.Add(cbi);
                }
                comboBox1.SelectedIndex = 0;
            };
            ARCHIVEclient.GetTBARCHIVE_CODE_TBFILE_TYPEAsync();
        }

        /// <summary>
        /// 按下畫面中的確認鍵
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            radUpload1.IsDeleteEnabled = false;
            OKButton.IsEnabled = false;
            CancelButton.IsEnabled = false;
            //radUpload1.IsDeleteEnabled = false;
            foreach (RadUploadItem item in radUpload1.Items)
            {
               
                PTS_MAM3.WSARCHIVE.ClassUploadFileClass picClass = new PTS_MAM3.WSARCHIVE.ClassUploadFileClass();
                picClass.strARC_ID = mwpARC_ID;
                picClass.strARC_TYPE = ((ComboBoxItem)comboBox1.SelectedItem).Tag.ToString();
                picClass.strFSID = mwpFSID;
                picClass.strEpisodeId = mwpEPISODE;
                picClass.strType = mwpFSTYPE;
                //至軒取號要用的節目中文名稱及子集中文名稱
                picClass.strFSID_NAME = mwpFSID_NAME;
                picClass.strEpisodeId_Name = mwpFNEPISODE_NAME;

                picClass.strChannelId = mwpChannel;
                picClass.strOFileName = item.FileName;
                picClass.srtFileSize = item.FileSize;
                picClass.strOExtensionName = System.IO.Path.GetExtension(item.FileName).Substring(1).ToLower();
                picClass.strUserId = UserClass.userData.FSUSER_ID;
                picClass.strUploadFileType = "Audio";
                if (textBox1.Text.Trim() != "")
                {
                    picClass.strMetadataFSTITLE = textBox1.Text;
                }
                else
                {
                    picClass.strMetadataFSTITLE = item.FileName;
                }
                picClass.strMetadataFSFSDESCRIPTION = textBox2.Text;
                ClassUploadFileClassList.Add(picClass);
            }
            ARCHIVEclient.GetUploadInfoCompleted += (s, args) =>
            {
                if (ChekFilePath(args.Result))
                {
                    ClassUploadFileClassList = args.Result;
                    radUpload1.StartUpload();
                }
            };
            ARCHIVEclient.GetUploadInfoAsync(ClassUploadFileClassList);
        }

        //檢查檔案上傳路徑(靜態方法，與其他頁面共用) by Jarvis 20130820
       public static bool ChekFilePath(List<ClassUploadFileClass> uploadInfoCollection)
        {

            foreach (ClassUploadFileClass cuf in uploadInfoCollection)
            {
                if (cuf.strDirectory == "")
                {
                    MessageBox.Show("獲取檔案上傳路徑異常");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 按下畫面中的取消鍵
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        /// <summary>
        /// 當上傳開始時 所觸發的事件
        /// 目前是將確定與取消按鈕隱藏
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radUpload1_UploadStarted(object sender, Telerik.Windows.Controls.UploadStartedEventArgs e)
        {
            OKButton.Visibility = Visibility.Collapsed;
            CancelButton.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// 檔案上傳結束後所執行的事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radUpload1_UploadFinished(object sender, RoutedEventArgs e)
        {
            ARCHIVEclient.InsTBLOG_AUDIOCompleted += (s, args) =>
            {
                if (args.Result == "true")
                {
                    //radBusyIndicator1.IsBusy = false;
                    MessageBox.Show("資料庫寫入完成");
                    this.DialogResult = true;
                }
                else
                { this.DialogResult = false; }
            };
            ARCHIVEclient.InsTBLOG_AUDIOAsync(ClassUploadFileClassList);
        }

        /// <summary>
        /// 檔案起始上傳前所執行的事件
        /// 將新檔名給上傳檔案
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radUpload1_FileUploadStarting(object sender, Telerik.Windows.Controls.FileUploadStartingEventArgs e)
        {
            foreach (ClassUploadFileClass x in ClassUploadFileClassList)
            {
                if (x.strOFileName == e.SelectedFile.File.Name)
                {
                    e.UploadData.FileName = x.strFileId + "." + x.strOExtensionName;
                    //e.UploadData.TargetPhysicalFolder = x.strDirectory;
                }
            }
        }

        /// <summary>
        /// 隱藏按鈕
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radUpload1_FilesSelected(object sender, Telerik.Windows.Controls.FilesSelectedEventArgs e)
        {
            Button uploadButton = this.radUpload1.ChildrenOfType<Button>().Where(b => b.Name.Equals("UploadButton")).FirstOrDefault();
            Button cancelButton = this.radUpload1.ChildrenOfType<Button>().Where(b => b.Name.Equals("CancelButton")).FirstOrDefault();
            if (uploadButton != null)
            {
                uploadButton.Opacity = 0;
                uploadButton.IsHitTestVisible = false;
            }
            if (cancelButton != null)
            {
                cancelButton.Opacity = 0;
                cancelButton.IsHitTestVisible = false;
            }
        }


        private void radUpload1_FileTooLarge(object sender, FileEventArgs e)
        {
            MessageBox.Show("單一檔案超過上傳大小上限", "警示訊息", MessageBoxButton.OK);
        }
    }
}

