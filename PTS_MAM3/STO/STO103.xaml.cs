﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using PTS_MAM3.ArchiveData;
using PTS_MAM3.WSARCHIVE;
using System.Windows.Data;


namespace PTS_MAM3.STO
{
    /// <summary>
    /// 入庫置換單管理頁面
    /// </summary>
    public partial class STO103 : Page
    {
        List<Class_ARCHIVE> ArchiveClassList = new List<Class_ARCHIVE>();
        List<Class_ARCHIVE> ArchiveClassListSearch = new List<Class_ARCHIVE>();
        WSARCHIVESoapClient ARCHIVEClient = new WSARCHIVESoapClient();
        DateTime dtStart = DateTime.Now.AddYears(-1);
        DateTime dtEnd = DateTime.Now;

        private FlowFieldCom.FlowFieldComSoapClient ComClient = new FlowFieldCom.FlowFieldComSoapClient();

        public STO103()
        {
            InitializeComponent();
            Refresh();
            comboBox1.SelectionChanged += new SelectionChangedEventHandler(comboBox1_SelectionChanged);
            ARCHIVEClient.GetTBARCHIVE_EXCHANEG_FILE_ALLCompleted += (s, args) =>
            {
                ArchiveClassList = args.Result.OrderByDescending(c => c.FSARC_ID).ToList();
                //datagrid1.ItemsSource = ArchiveClassList;
                //宣告PagedCollectionView,將撈取的資料放入
                PagedCollectionView pcv = new PagedCollectionView(ArchiveClassList);
                //指定DataGrid以及DataPager的內容
                datagrid1.ItemsSource = pcv;
                DataPager.Source = pcv;

                if (args.Result == null)
                { }
                else
                { datagrid1.SelectedIndex = 0; }
            };
            ARCHIVEClient.GetTBARCHIVE_EXCHANEG_FILE_ALLCompleted += (s, args) =>
            {
                ArchiveClassList = args.Result.OrderByDescending(c => c.FSARC_ID).ToList();
                //datagrid1.ItemsSource = ArchiveClassList;


                //宣告PagedCollectionView,將撈取的資料放入
                PagedCollectionView pcv = new PagedCollectionView(ArchiveClassList);
                //指定DataGrid以及DataPager的內容
                datagrid1.ItemsSource = pcv;
                DataPager.Source = pcv;
            };

            ComClient.GetApprovalListDetailByFormIDCompleted += new EventHandler<FlowFieldCom.GetApprovalListDetailByFormIDCompletedEventArgs>(ComClient_GetApprovalListDetailByFormIDCompleted);

        }

        void comboBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (((ComboBoxItem)comboBox1.SelectedItem).Tag.ToString() != "ALL")
            {
                ArchiveClassListSearch = ArchiveClassList.Where(ss => ss.FCCHECK_STATUS.ToString() == ((ComboBoxItem)comboBox1.SelectedItem).Tag.ToString()).ToList();
                //datagrid1.ItemsSource = ArchiveClassListSearch;
                //宣告PagedCollectionView,將撈取的資料放入
                PagedCollectionView pcv = new PagedCollectionView(ArchiveClassListSearch);
                //指定DataGrid以及DataPager的內容
                datagrid1.ItemsSource = pcv;
                DataPager.Source = pcv;
            }
            else
            {
                //宣告PagedCollectionView,將撈取的資料放入
                PagedCollectionView pcv = new PagedCollectionView(ArchiveClassList);
                //指定DataGrid以及DataPager的內容
                datagrid1.ItemsSource = pcv;
                DataPager.Source = pcv;
            }
            //datagrid1.ItemsSource = ArchiveClassList;
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        //新增
        private void btnArchiveAdd_Click(object sender, RoutedEventArgs e)
        {
            STO.STO101_01 STO101_01 = new STO.STO101_01();
            STO101_01.Show();
            STO101_01.Closing += (s, args) =>
                { Refresh(); };
        }

        //修改
        private void btnArchiveModify_Click(object sender, RoutedEventArgs e)
        {
            if (datagrid1.SelectedItem != null)
            {
                if (((Class_ARCHIVE)datagrid1.SelectedItem).FCCHECK_STATUS == "W")
                {
                    STO.STO101_02 STO101_02 = new STO.STO101_02(((Class_ARCHIVE)datagrid1.SelectedItem));
                    STO101_02.Show();
                    STO101_02.Closing += (s, args) =>
                    { Refresh(); };
                }
                else
                { MessageBox.Show("審核中的表單才能夠更改!"); }
            }
        }

        //查詢
        private void btnArchiveQuery_Click(object sender, RoutedEventArgs e)
        {
            string FSTYPE, FSID, FSEPISODE;
            STO.STO100_05 pageSTO100_05 = new STO.STO100_05();
            pageSTO100_05.Show();
            pageSTO100_05.Closing += (s, args) =>
            {
                if (pageSTO100_05.DialogResult == true)
                {
                    FSTYPE = pageSTO100_05.public_Type;
                    FSID = pageSTO100_05.public_ID;
                    FSEPISODE = pageSTO100_05.public_Episode;
                    string sD = pageSTO100_05.selsDate;
                    string eD = pageSTO100_05.seleDate;
                    RefreshCondiction(FSTYPE, FSID, FSEPISODE, sD, eD);
                }
                //ArchiveClassList = pageSTO100_05.ArchiveClassSearchList.OrderByDescending(c => c.FSARC_ID).ToList();
                //datagrid1.ItemsSource = ArchiveClassList;
                //宣告PagedCollectionView,將撈取的資料放入
                //PagedCollectionView pcv = new PagedCollectionView(ArchiveClassList);
                //指定DataGrid以及DataPager的內容
                // datagrid1.ItemsSource = pcv;
                // DataPager.Source = pcv;
            };



            //pageSTO100_05.Closing += (s, args) =>
            //{

            //    FSTYPE = pageSTO100_05.public_Type;
            //    FSID = pageSTO100_05.public_ID;
            //    FSEPISODE = pageSTO100_05.public_Episode;
            //    string sD = pageSTO100_05.selsDate;
            //    string eD = pageSTO100_05.seleDate;
            //    RefreshCondiction(FSTYPE, FSID, FSEPISODE, sD, eD);

            //    //ArchiveClassList = pageSTO100_05.ArchiveClassSearchList.OrderByDescending(c => c.FSARC_ID).ToList();
            //    //datagrid1.ItemsSource = ArchiveClassList;
            //    //宣告PagedCollectionView,將撈取的資料放入
            //    //PagedCollectionView pcv = new PagedCollectionView(ArchiveClassList);
            //    //指定DataGrid以及DataPager的內容
            //   // datagrid1.ItemsSource = pcv;
            //   // DataPager.Source = pcv;
            //};
        }

        //更新
        private void btnArchiveGRefresh_Click(object sender, RoutedEventArgs e)
        {
            Refresh();
        }

        private void Refresh()
        {
            ARCHIVEClient.GetTBARCHIVE_EXCHANEG_FILE_ALLAsync(UserClass.userData.FSUSER_ID, dtStart.ToShortDateString(), dtEnd.ToShortDateString(), "", "", "");

        }

        private void RefreshCondiction(string FSTYPE, string FSID, string FSEPISODE, string sDate, string eDate)
        {

            ARCHIVEClient.GetTBARCHIVE_EXCHANEG_FILE_ALLAsync(UserClass.userData.FSUSER_ID, sDate, eDate, FSTYPE, FSID, FSEPISODE);
            //WSARCHIVESoapClient ARCHIVEClient = new WSARCHIVESoapClient();
            //ArchiveClassListSearch = ArchiveClassList;
            //datagrid1.ItemsSource = ArchiveClassListSearch.Where(R => R.FSTYPE == FSTYPE && R.FSID == FSID && R.FNEPISODE.ToString() == FSEPISODE); 
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            if (datagrid1.SelectedItem != null)
            {
                STO.STO101_03 STO101_03Page = new STO.STO101_03(((Class_ARCHIVE)datagrid1.SelectedItem).FSARC_ID, "");
                STO101_03Page.Show();
            }
        }

        private void datagrid1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.datagrid1.SelectedItem != null)
            {
                ComClient.GetApprovalListDetailByFormIDAsync(((Class_ARCHIVE)this.datagrid1.SelectedItem).FSARC_ID);
            }
        }

        void ComClient_GetApprovalListDetailByFormIDCompleted(object sender, FlowFieldCom.GetApprovalListDetailByFormIDCompletedEventArgs e)
        {
            dataGrid2.ItemsSource = e.Result;
        }


    }
}
