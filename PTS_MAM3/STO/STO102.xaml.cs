﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using PTS_MAM3.ArchiveData;
using PTS_MAM3.WSARCHIVE;
using System.Windows.Browser;
using System.Windows.Data;


namespace PTS_MAM3.STO
{
    /// <summary>
    /// 入庫單管理頁面
    /// </summary>
    public partial class STO102 : Page
    {
        List<Class_ARCHIVE> ArchiveClassList = new List<Class_ARCHIVE>();
        List<Class_ARCHIVE> ArchiveClassListSearch = new List<Class_ARCHIVE>();
        DateTime dtStart = DateTime.Now.AddYears(-1);
        DateTime dtEnd = DateTime.Now;
        private FlowFieldCom.FlowFieldComSoapClient ComClient = new FlowFieldCom.FlowFieldComSoapClient();

        public STO102()
        {
            InitializeComponent();
            Refresh();
            comboBox1.SelectionChanged += new SelectionChangedEventHandler(comboBox1_SelectionChanged);
            ARCHIVEClient.GetTBARCHIVE_ALLCompleted += (s, args) =>
            {
                ArchiveClassList = args.Result.OrderByDescending(c => c.FSARC_ID).ToList();
                //datagrid1.ItemsSource = ArchiveClassList;
                //宣告PagedCollectionView,將撈取的資料放入
                PagedCollectionView pcv = new PagedCollectionView(ArchiveClassList);
                //指定DataGrid以及DataPager的內容
                datagrid1.ItemsSource = pcv;
                DataPager.Source = pcv;
            };

            ComClient.GetApprovalListDetailByFormIDCompleted += new EventHandler<FlowFieldCom.GetApprovalListDetailByFormIDCompletedEventArgs>(ComClient_GetApprovalListDetailByFormIDCompleted);

        }

        void comboBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (((ComboBoxItem)comboBox1.SelectedItem).Tag.ToString() != "ALL")
            {
                ArchiveClassListSearch = ArchiveClassList.Where(ss => ss.FCCHECK_STATUS.ToString() == ((ComboBoxItem)comboBox1.SelectedItem).Tag.ToString()).ToList();
                //datagrid1.ItemsSource = ArchiveClassListSearch;
                //宣告PagedCollectionView,將撈取的資料放入
                PagedCollectionView pcv = new PagedCollectionView(ArchiveClassListSearch);
                //指定DataGrid以及DataPager的內容
                datagrid1.ItemsSource = pcv;
                DataPager.Source = pcv;
            }
            else
            {
                //宣告PagedCollectionView,將撈取的資料放入
                PagedCollectionView pcv = new PagedCollectionView(ArchiveClassList);
                //指定DataGrid以及DataPager的內容
                datagrid1.ItemsSource = pcv;
                DataPager.Source = pcv;
            }
            //datagrid1.ItemsSource = ArchiveClassList;
        }
        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        //新增
        private void btnArchiveAdd_Click(object sender, RoutedEventArgs e)
        {
            ArchiveData_Add ArchiveData_Add_frm = new ArchiveData_Add();
            ArchiveData_Add_frm.Show();
            ArchiveData_Add_frm.Closing += (s, args) =>
                { Refresh(); };
        }

        //修改
        private void btnArchiveModify_Click(object sender, RoutedEventArgs e)
        {
            if (datagrid1.SelectedItem != null)
            {

                if (((Class_ARCHIVE)datagrid1.SelectedItem).FCCHECK_STATUS == "N")
                {
                    STO.STO100_02 STO100_02page = new STO.STO100_02(((Class_ARCHIVE)datagrid1.SelectedItem));
                    STO100_02page.Show();
                    STO100_02page.Closing += (s, args) =>
                    { Refresh(); };
                }
                else
                { MessageBox.Show("審核中的表單才能夠被修改!"); }
            }
        }

        //查詢
        private void btnArchiveQuery_Click(object sender, RoutedEventArgs e)
        {
            string FSTYPE,FSID,FSEPISODE;
            STO.STO100_05 pageSTO100_05 = new STO.STO100_05();
            pageSTO100_05.Show();
            pageSTO100_05.Closing += (s, args) =>
                {
                    if (pageSTO100_05.DialogResult == true)
                    {
                        FSTYPE = pageSTO100_05.public_Type;
                        FSID = pageSTO100_05.public_ID;
                        FSEPISODE = pageSTO100_05.public_Episode;
                        string sD = pageSTO100_05.selsDate;
                        string eD = pageSTO100_05.seleDate;
                        //if (FSID != "")
                        //{
                        RefreshCondiction(FSTYPE, FSID, FSEPISODE, sD, eD);
                        //}
                    }
                };
        }
        private void RefreshCondiction(string FSTYPE, string FSID, string FSEPISODE, string sDate, string eDate)
        {
            ARCHIVEClient.GetTBARCHIVE_ALLAsync("", sDate, eDate, FSTYPE, FSID, FSEPISODE);
        }

        //更新
        private void btnArchiveGRefresh_Click(object sender, RoutedEventArgs e)
        {
            Refresh();
        }

        WSARCHIVESoapClient ARCHIVEClient = new WSARCHIVESoapClient();

        private void Refresh()
        {
            ARCHIVEClient.GetTBARCHIVE_ALLCompleted += (s, args) =>
            {
                ArchiveClassList = args.Result.OrderByDescending(c => c.FSARC_ID).ToList();
                //datagrid1.ItemsSource = ArchiveClassList;


                //宣告PagedCollectionView,將撈取的資料放入
                PagedCollectionView pcv = new PagedCollectionView(ArchiveClassList);
                //指定DataGrid以及DataPager的內容
                datagrid1.ItemsSource = pcv;
                DataPager.Source = pcv;

            };

            ARCHIVEClient.GetTBARCHIVE_ALLAsync("", dtStart.ToShortDateString(), dtEnd.ToShortDateString(), "", "", "");
            
            //ARCHIVEClient.GetTBARCHIVE_ALL_NONCONDICTIONCompleted += (s, args) =>
            //    {
            //        ArchiveClassList = args.Result.OrderByDescending(c=>c.FSARC_ID).ToList();
            //        //datagrid1.ItemsSource = ArchiveClassList;
            //        //宣告PagedCollectionView,將撈取的資料放入
            //        PagedCollectionView pcv = new PagedCollectionView(ArchiveClassList);
            //        //指定DataGrid以及DataPager的內容
            //        datagrid1.ItemsSource = pcv;
            //        DataPager.Source = pcv;
            //    };
            //ARCHIVEClient.GetTBARCHIVE_ALL_NONCONDICTIONAsync(UserClass.userData.FSUSER_ID);
            
        }



        private void button1_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (datagrid1.SelectedItem == null)
            {
                MessageBox.Show("請選擇預列印的入庫資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            WSARCHIVESoapClient ARCHIVEClient = new WSARCHIVESoapClient();

            ARCHIVEClient.CallREPORT_STOCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {
                        if (args.Result.ToString().StartsWith("錯誤"))
                            MessageBox.Show(args.Result.ToString(), "提示訊息", MessageBoxButton.OK);
                        else
                            HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + args.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,resizable=yes");
                    }
                    else
                        MessageBox.Show("查無相關資料。", "提示訊息", MessageBoxButton.OK);
                };            
            };

            //列印成案單(丟入參數：入庫單編號、查詢者、SessionID)
            ARCHIVEClient.CallREPORT_STOAsync(((Class_ARCHIVE)datagrid1.SelectedItem).FSARC_ID.ToString().Trim(), UserClass.userData.FSUSER_ID.ToString().Trim(), UserClass.userData.FSSESSION_ID.ToString().Trim());
        }

        private void btnArchiveRESEND_Click(object sender, RoutedEventArgs e)
        {
            if (datagrid1.SelectedItem != null)
            {
                if (((Class_ARCHIVE)datagrid1.SelectedItem).FCCHECK_STATUS.Trim() == "F" || ((Class_ARCHIVE)datagrid1.SelectedItem).FCCHECK_STATUS.Trim() == "X")
                {
                    STO.STO100_07 STO100_07page = new STO.STO100_07(((Class_ARCHIVE)datagrid1.SelectedItem));
                    STO100_07page.Show();
                    STO100_07page.Closing += (s, args) =>
                    { Refresh(); };
                }
                else
                { MessageBox.Show("主管審核為\"不通過\"或\"使用者抽單\"的單據才能執行\"重送審核\"!"); }
            }
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            if (datagrid1.SelectedItem != null)
            {
                STO.STO100_03 STO100_03page = new STO.STO100_03(((Class_ARCHIVE)datagrid1.SelectedItem).FSARC_ID,"");
                STO100_03page.Show();
                STO100_03page.Closing += (s, args) =>
                { 
                //    Refresh(); 
                };
            }
        }

        private void datagrid1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.datagrid1.SelectedItem != null)
            {
                ComClient.GetApprovalListDetailByFormIDAsync(((Class_ARCHIVE)this.datagrid1.SelectedItem).FSARC_ID);
            }
        }

        void ComClient_GetApprovalListDetailByFormIDCompleted(object sender, FlowFieldCom.GetApprovalListDetailByFormIDCompletedEventArgs e)
        {
            dataGrid2.ItemsSource = e.Result;
        }

    }
}
