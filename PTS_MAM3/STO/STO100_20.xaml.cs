﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSPROMO;
using PTS_MAM3.ProgData;
using DataGridDCTest;
using PTS_MAM3.WSBROADCAST;

namespace PTS_MAM3.STO
{
    public partial class STO100_20 : ChildWindow
    {
        WSPROMOSoapClient client = new WSPROMOSoapClient();  //產生新的代理類別
        WSBROADCASTSoapClient clientBro = new WSBROADCASTSoapClient(); 

        public string strPromoID_View = "";     //選取的短帶編號
        public string strPromoName_View = "";   //選取的短帶名稱
        public string strChannel_Id = "";       //頻道別
        public string strProducer = "";         //製作人
        public List<Class_PROMO> allList = new List<Class_PROMO>();
        public List<Class_PROMO> allCheckedList = new List<Class_PROMO>();
        public STO100_20()
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面           
        }

        void InitializeForm() //初始化本頁面
        {
            //取得全部節目資料
            client.GetTBPGM_PROMO_ALLCompleted += new EventHandler<GetTBPGM_PROMO_ALLCompletedEventArgs>(client_GetTBPGM_PROMO_ALLCompleted);
            //client.GetTBPGM_PROMO_ALLAsync();     //預設不Load全部的節目資料，必須輸入查詢

            //查詢節目資料By節目名稱        
            client.GetTBPGM_PROMO_BYPROMONAMECompleted += new EventHandler<GetTBPGM_PROMO_BYPROMONAMECompletedEventArgs>(client_GetTBPGM_PROMO_BYPROMONAMECompleted);

            //查詢節目製作人
            clientBro.GetPRODUCERCompleted += new EventHandler<GetPRODUCERCompletedEventArgs>(clientBro_GetPRODUCERCompleted);
        }

        void LoadBusy()
        {
            BusyMsg.IsBusy = true;  //關閉忙碌的Loading訊息
            btnSearch.IsEnabled = false;
            btnAll.IsEnabled = false;
            tbxPROMONAME.IsEnabled = false;
        }

        void LoadBusyUnLock()
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息
            btnSearch.IsEnabled = true;
            btnAll.IsEnabled = true;
            tbxPROMONAME.IsEnabled = true;
            tbxPROMONAME.Focus();
        }

        //實作-查詢節目資料By節目名稱
        void client_GetTBPGM_PROMO_BYPROMONAMECompleted(object sender, GetTBPGM_PROMO_BYPROMONAMECompletedEventArgs e)
        {
            LoadBusyUnLock();

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    //this.DGPROMOList.DataContext = e.Result;
                    allList = e.Result;
                    WasThereEverStored();
                    //if (this.DGPROMOList.DataContext != null)
                    //{
                    //    DGPROMOList.SelectedIndex = 0;
                    //}
                }
                else
                {
                    MessageBox.Show("查無短帶資料，請重新輸入條件查詢！", "提示訊息", MessageBoxButton.OK);
                    return;
                }
            }   
        }

        //實作-取得全部節目資料
        void client_GetTBPGM_PROMO_ALLCompleted(object sender, GetTBPGM_PROMO_ALLCompletedEventArgs e)
        {
            LoadBusyUnLock();

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    //this.DGPROMOList.DataContext = e.Result;
                    allList = e.Result;
                    WasThereEverStored();
                    //if (this.DGPROMOList.DataContext != null)
                    //{
                    //    DGPROMOList.SelectedIndex = 0;
                    //}
                }
            } 
        }

        //實作-查詢節目製作人
        void clientBro_GetPRODUCERCompleted(object sender, GetPRODUCERCompletedEventArgs e)
        {
            LoadBusyUnLock();

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Trim() != "")
                {
                    strProducer = e.Result;
                }
            }
            this.DialogResult = true;
        }

        //確定
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGPROMOList.SelectedItem == null)
            {
                MessageBox.Show("請選擇節目資料", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else
            {  
                strPromoID_View = ((Class_PROMO)DGPROMOList.SelectedItem).FSPROMO_ID.ToString();
                strPromoName_View = ((Class_PROMO)DGPROMOList.SelectedItem).FSPROMO_NAME.ToString();
                strChannel_Id = ((Class_PROMO)DGPROMOList.SelectedItem).FSMAIN_CHANNEL_ID.ToString();
                //this.DialogResult = true;

                clientBro.GetPRODUCERAsync("P", strPromoID_View);    //選定短帶後，再去查詢短帶製作人
                LoadBusy();
            }
        }

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
       
        //查詢
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            if (tbxPROMONAME.Text.Trim() != "")
            {
                //查詢節目主檔資料
                client.GetTBPGM_PROMO_BYPROMONAMEAsync(tbxPROMONAME.Text.Trim());
                LoadBusy();
            }
            else
                MessageBox.Show("請輸入條件查詢！", "提示訊息", MessageBoxButton.OK);      
        }

        //重新查詢
        private void btnAll_Click(object sender, RoutedEventArgs e)
        {
            tbxPROMONAME.Text = "";

            //取得全部節目資料
            client.GetTBPGM_PROMO_ALLAsync();
            LoadBusy();
        }

        //連點事件
        private void RowDoubleClick(object sender, DataGridDCTest.DataGridRowClickedArgs e)
        {
            DoubleClickDataGrid dcdg = sender as DoubleClickDataGrid;

            strPromoID_View = ((Class_PROMO)DGPROMOList.SelectedItem).FSPROMO_ID.ToString();
            strPromoName_View = ((Class_PROMO)DGPROMOList.SelectedItem).FSPROMO_NAME.ToString();
            strChannel_Id = ((Class_PROMO)DGPROMOList.SelectedItem).FSMAIN_CHANNEL_ID.ToString();
            //this.DialogResult = true;

            clientBro.GetPRODUCERAsync("P", strPromoID_View);    //選定短帶後，再去查詢短帶製作人
            LoadBusy();
        }

        //輸入完查詢內容後按下enter即可查詢
        private void tbxPROMONAME_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                btnSearch_Click(sender, e);
            }
        }

        WSWasThereEverStored.WSWasThereEverStoredSoapClient storedClient = new WSWasThereEverStored.WSWasThereEverStoredSoapClient();
        List<WSWasThereEverStored.Class_ARCHIVE> checkStoredList = new List<WSWasThereEverStored.Class_ARCHIVE>();
        private void WasThereEverStored()
        {
            storedClient.CheckStored_PAsync();
            storedClient.CheckStored_PCompleted += (s, args) =>
            {
                checkStoredList = args.Result;
                bool everStored = false;
                foreach (Class_PROMO y in allList)
                {
                    foreach (WSWasThereEverStored.Class_ARCHIVE z in checkStoredList)
                    {
                        if (y.FSPROG_ID == z.FSID)
                        {
                            everStored = true;
                            allCheckedList.Add(y);

                        }
                    }
                }
                if (allCheckedList.Count > 0)
                {
                    this.DGPROMOList.DataContext = allCheckedList;
                    if (this.DGPROMOList.DataContext != null)
                    {
                        DGPROMOList.SelectedIndex = 0;
                    }
                }
                else
                { MessageBox.Show("本節目節目尚未有入庫紀錄!"); }
            };
        }
    }
}

