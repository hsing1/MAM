﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSARCHIVE;
using PTS_MAM3.PRG;
using PTS_MAM3.ProgData;

namespace PTS_MAM3.STO
{
    public partial class STO100_05 : ChildWindow
    {
        WSARCHIVESoapClient client = new WSARCHIVESoapClient();

        string m_strFileTypeName = "";   //入庫檔案類型名稱
        string m_strFileTypeID = "";     //入庫檔案類型編號
        string m_strType = "";           //類型
        string m_strID = "";             //編碼
        string m_strEpisode = "0";        //集別
        string m_strCHANNELID = "";      //頻道別   
        string m_strFileNO = "";         //檔案編號(修改時用的)
        string m_strBroID = "";          //送帶轉檔單單號    
        string m_TBNAME = "";
        public List<Class_ARCHIVE> m_ListFormData = new List<Class_ARCHIVE>();  //記錄查詢到的資料

        public string public_Type = "";
        public string public_ID = "";
        public string public_Episode = "0";

        public STO100_05()
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面
            //m_TBNAME = TBNAME;
            rdbProg.IsChecked = true;
        }

        void InitializeForm() //初始化本頁面
        {
            //用類型、編碼、集別取得入庫單檔
            datePicker1.SelectedDate = DateTime.Now.AddYears(-1);
            datePicker2.SelectedDate = DateTime.Now.AddDays(1);
            client.GetTBARCHIVE_BYIDEPISODECompleted += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        if (args.Result != null && args.Result.Count > 0)  //若是有查詢結果，則顯示，不然就請使用者重新查詢
                        {
                            m_ListFormData = new List<Class_ARCHIVE>(args.Result);  //把取回來的結果，加上型別定義，才可以塞回去   

                            public_Type = m_strType;
                            public_ID = m_strID ;
                            public_Episode = m_strEpisode;

                            this.DialogResult = true;
                        }
                        else
                        {
                            MessageBox.Show("查無資料，請重新輸入條件查詢！");
                            return;
                        }
                    }
                };
          
        }
        public string selsDate;
        public string seleDate;
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            public_Type = m_strType;
            public_ID = m_strID;
            public_Episode = m_strEpisode;
            selsDate = datePicker1.Text;
            seleDate = ((Convert.ToDateTime(datePicker2.Text)).AddDays(1)).ToShortDateString();

            this.DialogResult = true;

            //if (public_Type != "" && public_ID != "")
            //{ this.DialogResult = true; }
            //else
            //{ MessageBox.Show("請選擇類型及節目!!"); }
            //用類型、編碼、集別取得送帶轉檔檔
            //client.GetTBARCHIVE_BYIDEPISODEAsync(m_strType, m_strID, m_strEpisode,m_TBNAME,UserClass.userData.FSUSER_ID);       
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //查詢節目
        private void btnPROG_Click(object sender, RoutedEventArgs e)
        {
            if (rdbProg.IsChecked == true)
            {
                //PRG100_01
                PROGDATA_VIEW PROGDATA_VIEW_frm = new PROGDATA_VIEW();
                PROGDATA_VIEW_frm.Show();

                PROGDATA_VIEW_frm.Closed += (s, args) =>
                {
                    if (PROGDATA_VIEW_frm.DialogResult == true)
                    {
                        tbxPROG_NAME.Text = PROGDATA_VIEW_frm.strProgName_View;
                        tbxPROG_NAME.Tag = PROGDATA_VIEW_frm.strProgID_View;

                        tbxEPISODE.Text = "";
                        lblEPISODE_NAME.Content = "";
                        m_strEpisode = "0";
                        m_strCHANNELID = PROGDATA_VIEW_frm.strChannel_Id;
                        m_strID = PROGDATA_VIEW_frm.strProgID_View;
                    }
                };
            }
            else if (rdbPromo.IsChecked == true)
            {
                //PRG800_07
                PRG800_07 PROMO_VIEW_frm = new PRG800_07();
                PROMO_VIEW_frm.Show();

                PROMO_VIEW_frm.Closed += (s, args) =>
                {
                    if (PROMO_VIEW_frm.DialogResult == true)
                    {
                        tbxPROG_NAME.Text = PROMO_VIEW_frm.strPromoName_View;
                        tbxPROG_NAME.Tag = PROMO_VIEW_frm.strPromoID_View;

                        tbxEPISODE.Text = "";
                        lblEPISODE_NAME.Content = "";
                        m_strEpisode = "0";
                        m_strCHANNELID = PROMO_VIEW_frm.strChannel_Id;
                        m_strID = PROMO_VIEW_frm.strPromoID_View;
                    }
                };
            }
        }

        //查詢子集
        private void btnEPISODE_Click(object sender, RoutedEventArgs e)
        {
            if (tbxPROG_NAME.Text.ToString().Trim() != "" && tbxPROG_NAME.Tag.ToString().Trim() != "")
            {
                PRG.PRG200_07 PRG200_07_frm = new PRG.PRG200_07(tbxPROG_NAME.Tag.ToString().Trim(), tbxPROG_NAME.Text.ToString().Trim());
                PRG200_07_frm.Show();

                PRG200_07_frm.Closed += (s, args) =>
                {
                    if (PRG200_07_frm.DialogResult == true)
                    {
                        m_strEpisode = PRG200_07_frm.m_strEpisode_View;
                        tbxEPISODE.Text = PRG200_07_frm.m_strEpisode_View;
                        lblEPISODE_NAME.Content = PRG200_07_frm.m_strProgDName_View;
                    }
                };
            }
            else
                MessageBox.Show("請先選擇節目！");
        }

        //選取節目
        private void rdbProg_Checked(object sender, RoutedEventArgs e)
        {
            if (rdbProg.IsChecked == true)
            {
                m_strType = "G";
                tbPROG_NANE.Text = "節目名稱";
                tbxPROG_NAME.Text = "";
                tbxPROG_NAME.Tag = "";
                m_strID = "";
                m_strEpisode = "0";

                tbFNEPISODE.Visibility = Visibility.Visible;
                tbxEPISODE.Visibility = Visibility.Visible;
                btnEPISODE.Visibility = Visibility.Visible;
                tbEPISODE_NAME.Visibility = Visibility.Visible;
                lblEPISODE_NAME.Visibility = Visibility.Visible;
            }
        }

        //選取宣傳帶
        private void rdbPromo_Checked(object sender, RoutedEventArgs e)
        {
            if (rdbPromo.IsChecked == true)
            {
                m_strType = "P";
                tbPROG_NANE.Text = "宣傳帶名稱";
                tbxPROG_NAME.Text = "";
                tbxPROG_NAME.Tag = "";
                tbxEPISODE.Text = "";
                lblEPISODE_NAME.Content = "";
                m_strID = "";
                m_strEpisode = "0";

                tbFNEPISODE.Visibility = Visibility.Collapsed;
                tbxEPISODE.Visibility = Visibility.Collapsed;
                btnEPISODE.Visibility = Visibility.Collapsed;
                tbEPISODE_NAME.Visibility = Visibility.Collapsed;
                lblEPISODE_NAME.Visibility = Visibility.Collapsed;
            }
        }

    }
}

