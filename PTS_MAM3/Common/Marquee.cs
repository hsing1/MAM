﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Media.Animation;

namespace PTS_MAM3
{
    public class Marquee : ContentControl
    {
        bool IsLoaded;
        Storyboard sb = new Storyboard();
        DoubleAnimation da = new DoubleAnimation();  

        public Marquee()
        {
            DefaultStyleKey = typeof(Marquee);
        }

        FrameworkElement cp;
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            cp = this.GetTemplateChild("PART_Content") as FrameworkElement;
            Loaded += new RoutedEventHandler(Marquee_Loaded);
            this.SizeChanged += new SizeChangedEventHandler(Marquee_SizeChanged);
        }

        void Marquee_SizeChanged(object sender, SizeChangedEventArgs e)
        {
             RestartAnimation();
        }

        void Marquee_Loaded(object sender, RoutedEventArgs e)
        {
            IsLoaded = true;
            RestartAnimation();
            this.MouseEnter += new MouseEventHandler(Marquee_MouseEnter);
            this.MouseLeave += new MouseEventHandler(Marquee_MouseLeave);
        }

        void Marquee_MouseLeave(object sender, MouseEventArgs e)
        {
            sb.Resume();
        }

        void Marquee_MouseEnter(object sender, MouseEventArgs e)
        {
            sb.Pause();
        }

        public double Duration
        {
            get { return (double)GetValue(DurationProperty); }
            set { SetValue(DurationProperty, value); }
        }

        public static readonly DependencyProperty DurationProperty =
            DependencyProperty.Register("Duration", typeof(double), typeof(Marquee), new PropertyMetadata(ValueChanged));

        static void ValueChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Marquee mar = sender as Marquee;
            mar.RestartAnimation();
        }

        private void RestartAnimation()
        {
            if (cp !=null && IsLoaded)
            {              
                da.From = this.ActualWidth;
                da.To = -cp.ActualWidth;
                da.RepeatBehavior = RepeatBehavior.Forever;
                da.Duration = new Duration( TimeSpan.FromSeconds( this.Duration));

                if (sb.Children.Count == 0)
                {
                    sb.Children.Add(da);
                    Storyboard.SetTargetProperty(da, new PropertyPath("(Canvas.Left)"));
                    Storyboard.SetTarget(da, cp);
                    sb.Begin();
                }
            }
        }    

    }
}
