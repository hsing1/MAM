﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace FTVNEWS.Common
{
    public class HighlightingRichTextBox : Control
    {

        private string RichTextBoxName = "RichTextBox";

        private RichTextBox RichTextBox { set; get; }

        public HighlightingRichTextBox()
        {
            this.DefaultStyleKey = typeof(HighlightingRichTextBox);

        }

        

        public Brush HighlightBrush
        {
            get { return GetValue(HighlightBrushProperty) as Brush; }
            set { SetValue(HighlightBrushProperty, value); }
        }

        public string Text
        {
            get { return GetValue(TextProperty) as string; }
            set { SetValue(TextProperty, value); }
        }

        public string HighlightText
        {
            get { return GetValue(HighlightTextProperty) as string; }
            set { SetValue(HighlightTextProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
        DependencyProperty.Register(
        "Text",
        typeof(string),
        typeof(HighlightingRichTextBox),
        new PropertyMetadata(OnTextPropertyChanged));

        private static void OnTextPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            HighlightingRichTextBox source = d as HighlightingRichTextBox;
            if (source.RichTextBox != null)
            {
                source.Highlight();
            }
        }

        public static readonly DependencyProperty HighlightTextProperty =
        DependencyProperty.Register(
        "HighlightText",
        typeof(string),
        typeof(HighlightingRichTextBox),
        new PropertyMetadata(OnHighlightTextPropertyChanged));

        private static void OnHighlightTextPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            HighlightingRichTextBox source = d as HighlightingRichTextBox;
            source.Highlight();
        }

        public static readonly DependencyProperty HighlightBrushProperty =
        DependencyProperty.Register(
        "HighlightBrush",
        typeof(Brush),
        typeof(HighlightingRichTextBox),
        new PropertyMetadata(null, OnHighlightBrushPropertyChanged));

        private static void OnHighlightBrushPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            HighlightingRichTextBox source = d as HighlightingRichTextBox;
            source.Highlight();
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            OnApplyTemplate();
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            // Grab the template part
            RichTextBox = GetTemplateChild(RichTextBoxName) as RichTextBox;

            // Re-apply the text value
            string text = Text;
            Text = null;
            Text = text;
        }

        private void Highlight()
        {
            
            if (RichTextBox != null)
            {
                RichTextBox.Blocks.Clear();

                RichTextBox.TextWrapping = TextWrapping.Wrap;

                Paragraph myParagraph = new Paragraph();
                List<string> stringList = new List<string>();
                int cur = 0;


                if (!string.IsNullOrEmpty(Text) && !string.IsNullOrEmpty(HighlightText) && Text.IndexOf(HighlightText) > -1)
                {
                    while (cur < Text.Length)
                    {
                        int show = Text.IndexOf(HighlightText, cur);
                        if (show > -1)
                        {
                            cur = 0;
                            stringList.Add(Text.Substring(cur, show - cur));
                            stringList.Add(Text.Substring(show, HighlightText.Length));
                            cur = show + HighlightText.Length;
                            Text = Text.Substring(cur, Text.Length - cur);
                        }
                        else
                        {
                            //cur = Text.Length;
                            break;
                        }
                    }
                    stringList.Add(Text);
                }
                else
                {
                    stringList.Add(Text);
                }

                if (stringList != null && stringList.Count > 0)
                {
                    for (int i = 0; i <= stringList.Count - 1; i++)
                    {
                        Run r = new Run();
                        if (stringList[i] == HighlightText)
                        {
                            r.Text = stringList[i];
                            r.Foreground = HighlightBrush;
                        }
                        else
                        {
                            r.Text = stringList[i];
                        }
                        myParagraph.Inlines.Add(r);
                    }
                }

                RichTextBox.Blocks.Add(myParagraph);
            }
        }
    }
}
