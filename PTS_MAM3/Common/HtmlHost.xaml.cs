﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.Common
{
    public partial class HtmlHost : UserControl
    {

        public HtmlHost(string url)
        {

            InitializeComponent();
            this.radHtmlPlaceholder1.SourceUrl = new Uri(url);
        }

        public HtmlHost()
        {
            InitializeComponent();
        }

        public HtmlHost(string url, int w, int h)
        {
            InitializeComponent();
            this.radHtmlPlaceholder1.SourceUrl = new Uri(url,UriKind.Absolute);
            //this.radHtmlPlaceholder1.Width = w;
            //this.radHtmlPlaceholder1.Height = h;
        }
    }
}
