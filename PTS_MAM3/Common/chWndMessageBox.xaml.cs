﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Media.Imaging;

namespace PTS_MAM3.Common
{
    public partial class chWndMessageBox : ChildWindow
    {
        public chWndMessageBox(string title, string message,int iType)
        {
            InitializeComponent();

            tbMessage.Text = message;
            this.Title = title;
            BitmapImage bmi ; 
            switch (iType)
            {
                case 0 :  //warning 
                    bmi = new BitmapImage(new Uri("../Images/48_Messagebox_Help.png", UriKind.Relative));
                    this.imgIcon.Source = bmi; 
                    break;
                case 1 :
                    bmi = new BitmapImage(new Uri("../Images/48_messagebox_warning.png", UriKind.Relative));
                    this.imgIcon.Source = bmi; 
                    break;                   
            }
            
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

