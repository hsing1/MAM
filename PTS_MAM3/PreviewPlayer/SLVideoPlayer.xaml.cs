﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Browser;
using System.Windows.Threading;
using ExpressionMediaPlayer;
using Telerik;
using Telerik.Windows.Controls;

#if SILVERLIGHT
using WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation;
#endif

namespace PTS_MAM3.PreviewPlayer
{
    public partial class SLVideoPlayer : UserControl
    {     
        private System.Windows.Threading.DispatcherTimer dt = new System.Windows.Threading.DispatcherTimer();
		private System.Windows.Threading.DispatcherTimer dt2 = new System.Windows.Threading.DispatcherTimer();

        private long _startFrame { get; set; }
        private string _mediaSource { get; set; }

        private long _mediaStartFrame { set; get; }

        public SLVideoPlayer()
        {
            InitializeComponent();
        }


        bool DoubleClick = false;

        public SLVideoPlayer(string mediaSource, long startFrame)
        {
            InitializeComponent();


            _startFrame = startFrame ;
            _mediaSource = mediaSource; 
            if (mediaSource != "")
            {
                //this.MediaElement1.Source = new Uri(_mediaSource, UriKind.Absolute); 
                if (_mediaSource.IndexOf("mms://", 0) > -1)
                {
                    MediaElement1.SetValue(MediaElement.SourceProperty, new Uri(_mediaSource, UriKind.Absolute));
                }
                else
                {
                    MediaElement1.Source = new Uri(_mediaSource, UriKind.Absolute);
                }
            }

            

            dt.Tick += dt_Tick;
            dt.Interval = TimeSpan.FromMilliseconds(50);
            dt.Start();
			
			
			dt2.Tick += dt2_Tick;
            dt2.Interval = TimeSpan.FromMilliseconds(300);
            dt2.Start();
			
			

            //模擬double click
            System.Windows.Threading.DispatcherTimer double_timer = new System.Windows.Threading.DispatcherTimer();
            double_timer.Interval = TimeSpan.FromSeconds(0.5);
            double_timer.Tick += new EventHandler(double_timer_Tick);
            double_timer.Start();


            //this.smpte_BitMap.Visibility = Visibility.Collapsed;

            //this.MediaElement1.Source = new Uri(@"mms://mam.ftv.com.tw/video/2010/12/20/2010C20P05M1.wmv", UriKind.Absolute);
            //this.MediaElement1.Opacity = 1;
            //this.MediaElement1.Play();

        }
        void double_timer_Tick(object sender, EventArgs e)
        {
            DoubleClick = false;
        }





        private Boolean _bFlagUpdateSlider = false;
        private Boolean _bDragSlider = false;
		private void dt2_Tick(object sender, EventArgs e)
        {
			try {
				Uri uri ; 
				
				if(MediaElement1.CurrentState == MediaElementState.Buffering ) 
				{
						BufferingGrid.Visibility = Visibility.Visible; 				
				}
				else {
					BufferingGrid.Visibility = Visibility.Collapsed; 
				}
												
				if ((MediaElement1.CurrentState.ToString() == "Playing"))
                {
				    uri = new Uri("../Images/media-playback-pause.png", UriKind.Relative) ;
					this.btnMediaPlay.SetValue( ToolTipService.ToolTipProperty ,"暫停");
					
				}
				else {
					uri = new Uri("../Images/Play1Hot.png", UriKind.Relative) ;	
					this.btnMediaPlay.SetValue( ToolTipService.ToolTipProperty ,"播放");	
				}
				
				this.imgMediaPlay.Source = new System.Windows.Media.Imaging.BitmapImage(uri) ;
                
				//


                if (!_bDragSlider){ 
                    _bFlagUpdateSlider = true;
                    double duration = MediaElement1.NaturalDuration.TimeSpan.TotalSeconds;
                    double current = (MediaElement1.Position.TotalSeconds);


                    if (duration > 0)
                    {
                        this.SliderTimeline.Value = current / duration;
                    }
                    _bFlagUpdateSlider = false;
                }
				//
				
				this.textBoxTotalDuration.Text = TimeSpan_To_Timecode(MediaElement1.NaturalDuration.TimeSpan, SmpteFrameRate.Smpte2997Drop); 
			}
			catch (Exception ex) { 
            
            }       
		}

        private void dt_Tick(object sender, EventArgs e)
        {
            HtmlDocument doc; 
            HtmlElement obj;

            try {
                
                if ((MediaElement1.CurrentState.ToString() == "Playing"))
                {
					//smpte_BitMap
                   
                }

                if ((MediaElement1.CurrentState.ToString() != "Closed"))
                {
                    string timeString = TimeSpan_To_Timecode(MediaElement1.Position, SmpteFrameRate.Smpte2997Drop); 
                    textBoxTimeCode.Text = timeString;


                    //string currentFrame = HTML_GetCurrentFrame();                                     

                }
                else
                {
                    textBoxTimeCode.Text = "00:00:00;00";
                }
				
				

            }
            catch (Exception ex) { 
                //
            }       
        }
        private string TimeSpan_To_Timecode(TimeSpan value, SmpteFrameRate rate)
        {
            TimeSpan startT = TimeSpan.FromSeconds(_startFrame / 29.97);

            return TimeCode.ConvertToString(startT.Add(value).TotalSeconds, rate);
        }

        Telerik.Windows.Controls.RadWindow _wnd;


        private void Grd_Video_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (DoubleClick)
            {

                //Application.Current.Host.Content.IsFullScreen = !Application.Current.Host.Content.IsFullScreen;
                //VisualStateManager.GoToState(this, "Fullscreen", true);
                //Telerik.Windows.Controls.RadWindow wnd = (Telerik.Windows.Controls.RadWindow) ((MainFrame)System.Windows.Application.Current.RootVisual).FindName("WinCtrl");
                //wnd.Show(); 




                if (Parent.GetType().FullName != "Telerik.Windows.Controls.RadWindow")
                {
                    this.HTML_Pause_Click();
                    if (_wnd == null)
                    {
                        _wnd = new Telerik.Windows.Controls.RadWindow();
                        _wnd.Header = "檔案預覽";
                        _wnd.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
                        _wnd.TopOffset = -100;
                        _wnd.Width = 640;

                        _wnd.IsRestricted = true;
                        //_wnd.Height = 506;
                        //StyleManager.SetTheme( _wnd , new Office_SilverTheme() );
                        ////_mediaSource = "mms://10.13.220.3:8088/MAMMMS/G201109900050002.wmv";
                        //SLVideoPlayer sl = new SLVideoPlayer(_mediaSource, _startFrame);
                        //_wnd.Content = sl;
                        LargePlayer lp = new LargePlayer(_mediaSource, _startFrame, _mediaStartFrame);
                        _wnd.Content = lp;
                        _wnd.Show();

                    }
                    else
                    {
                        //_wnd.Close();
                        //_wnd = null;

                        //_wnd = new RadWindow();
                        //_wnd.Header = "檔案預覽";
                        //_wnd.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
                        //_wnd.Width = 640;
                        //_wnd.Height = 480;

                        _wnd.IsRestricted = true;
                        //_mediaSource = "mms://10.13.220.3:8088/MAMMMS/G201109900050002.wmv";
                        //SLVideoPlayer sl = new SLVideoPlayer(_mediaSource, _startFrame);
                        //_wnd.Content = sl;
                        //_wnd.Show();
                        LargePlayer lp = new LargePlayer(_mediaSource, _startFrame, _mediaStartFrame);
                        _wnd.Content = lp;
                        _wnd.Show();
                    }



                }







            }
            else
            {

                DoubleClick = true;
                this.HTML_Play_Click();
            }
        }



        private void MediaElement1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
           

        }
       

    
        private void btnMediaPlay_Click(object sender, System.Windows.RoutedEventArgs e)
        {

        	
			Uri uri ;
			if ((MediaElement1.CurrentState.ToString() == "Playing"))
            {					
				MediaElement1.Pause() ; 
				uri = new Uri("../Images/media-playback-pause.png", UriKind.Relative) ;
				this.btnMediaPlay.SetValue( ToolTipService.ToolTipProperty ,"播放");
			}
			else {
				MediaElement1.Play() ; 
				uri = new Uri("../Images/Play1Hot.png", UriKind.Relative) ;	
				this.btnMediaPlay.SetValue( ToolTipService.ToolTipProperty ,"暫停");
			}
			this.imgMediaPlay.Source = new System.Windows.Media.Imaging.BitmapImage(uri) ;
        }

        private void btnMediaStop_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	MediaElement1.Stop() ;
        }

        private void btnMediaStepReward_Click(object sender, RoutedEventArgs e)
        {
            HTML_Step_Click(-1);
        }

        private void btnMediaStepForward_Click(object sender, RoutedEventArgs e)
        {
            HTML_Step_Click(1); 
        }




        private void SliderVolume_ValueChanged_1(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                if (MediaElement1 != null)
                {
                    this.MediaElement1.Volume = e.NewValue;
                    this.SliderVolume.SetValue(ToolTipService.ToolTipProperty, "音量" + (int)(e.NewValue * 100) + "%");
                }
            }              
            catch (Exception ex) { 
                //
            }       
            
        }


        
        private void radSlider1_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
			try 
			{

            if (!_bFlagUpdateSlider)
            {
                _bDragSlider = true; 
                double duration = MediaElement1.NaturalDuration.TimeSpan.TotalSeconds;
                
                TimeCode newPosition = TimeCode.FromAbsoluteTime(duration * SliderTimeline.Value, SmpteFrameRate.Smpte2997Drop);
                MediaElement1.Position = TimeSpan.FromSeconds(newPosition.TotalSeconds);
                _bDragSlider = false;
            }}
			catch (Exception ex) { 
                _bDragSlider = false ; 
            }       
        }


        private void Media_Time_Backward(string t)
        {
            TimeCode oneFrame = new TimeCode(t, SmpteFrameRate.Smpte2997Drop);
            TimeCode current = TimeCode.FromAbsoluteTime(MediaElement1.Position.TotalSeconds, SmpteFrameRate.Smpte2997Drop);

            try
            {
                TimeCode newPosition = current.Subtract(oneFrame);
                MediaElement1.Position = TimeSpan.FromSeconds(newPosition.TotalSeconds);
            }
            catch (Exception ex)
            {

            }

            
        }

        private void Media_Time_Forward(string t)
        {
            TimeCode oneFrame = new TimeCode(t, SmpteFrameRate.Smpte2997Drop);

            TimeCode current = TimeCode.FromAbsoluteTime(MediaElement1.Position.TotalSeconds, SmpteFrameRate.Smpte2997Drop);

            try
            {
                TimeCode newPosition = current.Add(oneFrame);
                MediaElement1.Position = TimeSpan.FromSeconds(newPosition.TotalSeconds);
            }
            catch (Exception ex)
            {

            }
        }

	
				
		[ScriptableMember]
		public void HTML_Play_Click()
		{
			MediaElement1.Play();			
		}
		
		
		[ScriptableMember]
		public void HTML_Pause_Click()
		{
			MediaElement1.Pause();			
		}
		
		[ScriptableMember]
		public void HTML_Stop_Click()
		{
			MediaElement1.Stop();			
		}
		
	
		[ScriptableMember]
		public void HTML_Step_Click(int frame)
		{
			if (frame > 0) 
			{
				Media_Time_Forward("00:00:00:01") ;
			}
			else if (frame < 0) 
            {
                Media_Time_Backward("00:00:00:01");	
			}				
		}
		
	
		[ScriptableMember]
		public void HTML_Step_Second_Click(int second)
		{
			if (second > 0) 
			{
				Media_Time_Forward("00:00:" + String.Format("{0:00}", second)  + ":00") ;
			}
			else if (second < 0) {
                Media_Time_Backward("00:00:" + String.Format("{0:00}", (-1) * second) + ":00");	
			}			
		}
		
		[ScriptableMember]
		public string HTML_GetTotalFrame() 
		{
            long frames = 0;

            double totalDuration = MediaElement1.NaturalDuration.TimeSpan.TotalMilliseconds;
            frames = (long) Math.Floor(totalDuration / 33.3667); 
	        return Convert.ToString(frames);
		}

        [ScriptableMember]
        public string HTML_GetCurrentFrame()
        {
            long frames = 0;

            double totalDuration = MediaElement1.Position.TotalMilliseconds;
            frames = (long)Math.Floor(totalDuration / 33.3667);

            
            return Convert.ToString(frames + _startFrame);
        }


        [ScriptableMember]
        public void HTML_SetURL(string url, long startFrame )
        {

            MediaElement1.Stop();
			this.sb_MediaElementSourceLoad.Begin() ;

            if (url.IndexOf("mms://", 0) > -1 )
            {
                MediaElement1.SetValue(MediaElement.SourceProperty, new Uri(url, UriKind.Absolute));
            }
            else
            {
                MediaElement1.Source = new Uri(url, UriKind.Absolute);
            }
            _mediaSource = url; 
            _startFrame = startFrame;
            //MediaElement1.Play();
			this.textBoxTotalDuration.Text = TimeSpan_To_Timecode(MediaElement1.NaturalDuration.TimeSpan, SmpteFrameRate.Smpte2997Drop); 
        }

        [ScriptableMember]
        public void HTML_SetStartFrame(long frame)
        {
            this._mediaStartFrame = frame;
        }

		[ScriptableMember]
        public void HTML_SetFrame(long frame)
        {
			try 
			{				
            	MediaElement1.Stop();
            	//MediaElement1.Source = new Uri(url, UriKind.Absolute);
				MediaElement1.Position = TimeSpan.FromMilliseconds(frame * 33.3667) ; 
	            //MediaElement1.Play();
			}
			catch (Exception ex) {
				
			}
        }

		

		private void MuteButton_Click(object sender, RoutedEventArgs e)
        {
									
            MediaElement1.IsMuted = (bool)muteButton.IsChecked;
        }

		



		private void MediaElement1_BufferingProgressChanged(object sender, System.Windows.RoutedEventArgs e)
		{
			// TODO: 在此新增事件處理常式執行項目。
			
			if (MediaElement1.BufferingProgress == 1) 
			{
				TextBlockBuffering.Text = "100%";
				//BufferingGrid.Visibility = Visibility.Collapsed; 
			}
			else {			
				TextBlockBuffering.Text = (int) (MediaElement1.BufferingProgress * 100 ) + "%";
				BufferingGrid.Visibility = Visibility.Visible;
			}
			
		}

      
       

  
		

	    
		
		
	
    }
}
