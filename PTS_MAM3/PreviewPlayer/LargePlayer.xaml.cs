﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.Class;

namespace PTS_MAM3.PreviewPlayer
{
    public partial class LargePlayer : UserControl
    {
        private int _HH = 0;
        private int _MM = 0;
        private int _SS = 0;
        private int _FF = 0;

        private SLVideoPlayer player = null;
        private long _flmedia_start_frame = 0;

        public LargePlayer(string _fsmedia_path, long _flstart_frame,long _flmedia_start_frame)
        {
            InitializeComponent();

            this._flmedia_start_frame = _flmedia_start_frame;

            player = new SLVideoPlayer(_fsmedia_path, _flstart_frame);
            this.Grid_Player.Children.Add(player);

        }

        private void TextBox_HH_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            if (!Int32.TryParse(this.TextBox_HH.Text, out _HH))
            {
                MessageBox.Show("請輸入數字!", "訊息", MessageBoxButton.OK);
                return;
            }
            if (this.TextBox_HH.Text.Length == 2)
            {
                this.TextBox_MM.Focus();
            }
        }

        private void TextBox_MM_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            if (!Int32.TryParse(this.TextBox_MM.Text, out _MM))
            {
                MessageBox.Show("請輸入數字!", "訊息", MessageBoxButton.OK);
                return;
            }
            if (this.TextBox_MM.Text.Length == 2)
            {
                this.TextBox_SS.Focus();
            }
        }

        private void TextBox_SS_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            if (!Int32.TryParse(this.TextBox_SS.Text, out _SS))
            {
                MessageBox.Show("請輸入數字!", "訊息", MessageBoxButton.OK);
                return;
            }
            if (this.TextBox_SS.Text.Length == 2)
            {
                this.TextBox_FF.Focus();
            }
        }

        private void TextBox_FF_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            if (!Int32.TryParse(this.TextBox_FF.Text, out _FF))
            {
                MessageBox.Show("請輸入數字!", "訊息", MessageBoxButton.OK);
                return;
            }
            
        }

        private void Button_Goto_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            player.HTML_SetFrame(long.Parse(TransferTimecode.timecodetoframe(this.TextBox_HH.Text + ":" + this.TextBox_MM.Text + ":" + this.TextBox_SS.Text + ":" + this.TextBox_FF.Text).ToString()) - this._flmedia_start_frame);
            //player.HTML_Play_Click();
        }
    }
}
