﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PTS_MAM3
{
    public static class Translate_Status_Name
    {
        //Log_Video+HDMC兩張表合在一起看
        //HDMC 的狀態為(空白 S C E) 四種其中之一時 在審核QC AP為"檔案未到")
        public static string TransStatusName_Video_HDMC(string file_Status, string HDMC_Status)
        {
            //只有HDMC_Status才會有可能是空白的情況
            string combinStatus = (file_Status == "" ? "_" : file_Status) +
                ((HDMC_Status == null ? "" : HDMC_Status) == "" ? "_" : HDMC_Status);
            combinStatus = combinStatus.Trim();

            //要回傳的狀態名稱
            string statusName = "";
            switch (combinStatus)
            {
                case "B_":
                    statusName = "等待上傳";
                    break;
                case "BS":
                    statusName = "上傳中";
                    break;
                case "BC":
                    statusName = "上傳完成";
                    break;
                case "BE":
                    statusName = "上傳失敗";
                    break;
            }

            if (statusName == "")
            {
                switch (combinStatus[0]) //TBLOG_VIDEO.fcfile_Status
                {
                    case 'D':
                        statusName = "置換刪除";
                        break;
                    case 'X':
                        statusName = "刪除";
                        break;
                }
            }

            if (statusName == "")
            {
                switch (combinStatus[1])//HDMC_Status
                {
                    case 'N':
                        statusName = "待審核";
                        break;
                    case 'O':
                        statusName = "審核通過";
                        break;
                    case 'X':
                        statusName = "審核不過";
                        break;
                }
            }

            if (statusName == "")
            {
                statusName = combinStatus;
            }
            return statusName;
        }

        //三張表合在一起看
        public static string TransStatusName(string brocast_Status, string file_Status, string HDMC_Status)
        {
            string combinStatus =
                (brocast_Status == "" ? "_" : brocast_Status) +
                (file_Status == "" ? "_" : file_Status) +
                (HDMC_Status == "" ? "_" : HDMC_Status);

            combinStatus = combinStatus.Trim();

            //要回傳的狀態名稱
            string statusName = "";
            switch (combinStatus)
            {
                //-----------------回溯-------------------
                case "GB_":
                    statusName = "待轉檔";
                    break;
                case "TB_":
                    statusName = "待轉檔";
                    break;
                case "XX_":
                    statusName = "抽單";
                    break;
                case "GS_":
                    statusName = "轉檔中";
                    break;
                case "TS_":
                    statusName = "轉檔中";
                    break;
                case "GR_":
                    statusName = "轉檔失敗";
                    break;
                case "TR_":
                    statusName = "轉檔失敗";
                    break;
                case "GT_":
                    statusName = "轉檔完成";
                    break;
                case "TT_":
                    statusName = "轉檔完成";
                    break;
                case "GY_":
                    statusName = "入庫完成";
                    break;
                case "TY_":
                    statusName = "入庫完成";
                    break;

                case "GTN":
                    statusName = "轉檔完成/待審核";
                    break;
                case "GTO":
                    statusName = "轉檔完成/審核通過";
                    break;
                case "GTX":
                    statusName = "轉檔完成/審核不過";
                    break;
                case "GYN":
                    statusName = "入庫完成/待審核";
                    break;
                case "GYO":
                    statusName = "入庫完成/審核通過";
                    break;
                case "GYX":
                    statusName = "入庫完成/審核不過";
                    break;

                case "TTN":
                    statusName = "轉檔完成/待審核";
                    break;
                case "TTO":
                    statusName = "轉檔完成/審核通過";
                    break;
                case "TTX":
                    statusName = "轉檔完成/審核不過";
                    break;
                case "TYN":
                    statusName = "入庫完成/待審核";
                    break;
                case "TYO":
                    statusName = "入庫完成/審核通過";
                    break;
                case "TYX":
                    statusName = "入庫完成/審核不過";
                    break;
                //---------------HD新流程---------------------
                case "NB_":
                    statusName = "等待上傳";
                    break;
                case "UB_":
                    statusName = "標記上傳";
                    break;
                case "RX_":
                    statusName = "退單";
                    break;
                case "EX_":
                    statusName = "抽單";
                    break;
                case "UBS":
                    statusName = "上傳中";
                    break;
                case "UBC":
                    statusName = "上傳完成";
                    break;
                case "UBE":
                    statusName = "上傳失敗";
                    break;
                case "UDO":
                    statusName = "刪除";
                    break;
                case "UBN":
                    statusName = "待審核";
                    break;
                case "UBO":
                    statusName = "審核通過";
                    break;
                case "UFX":
                    statusName = "審核不過";
                    break;
                case "UDX":
                    statusName = "刪除";
                    break;
                case "USO":
                    statusName = "審核通過/轉檔中";
                    break;
                case "URO":
                    statusName = "審核通過/轉檔失敗";
                    break;
                case "UTO":
                    statusName = "審核通過/轉檔完成";
                    break;
                case "UYO":
                    statusName = "審核通過/入庫完成";
                    break;
                case "___":
                    statusName = "未填寫送播單";
                    break;
                default:
                    if (combinStatus[1] == 'X')//可能是手動刪除造成的狀態
                    {
                        statusName = "刪除";
                    }
                    else
                    {
                        statusName = combinStatus;
                    }
                    break;
            }
            return statusName;
        }
    }
}
