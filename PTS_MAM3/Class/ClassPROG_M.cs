﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.PROG_M
{
    public class ClassPROG_M
    {
        public string FSPROG_ID { get; set; }
        public string FSPGMNAME { get; set; }     
        public string FSPGMENAME { get; set; }
        public string FSPRDDEPTID { get; set; }
        public string FSCHANNEL { get; set; }
        public Int16 FNTOTEPISODE { get; set; }
        public Int16 FNLENGTH { get; set; }
        public string FSPROGOBJID { get; set; }
        public string FSPROGSRCID { get; set; }
        public string FSPROGATTRID { get; set; }
        public string FSPROGAUDID { get; set; }
        public string FSPROGTYPEID { get; set; }
        public string FSPROGGRADEID { get; set; }
        public string FSPROGLANGID1 { get; set; }
        public string FSPROGLANGID2 { get; set; }
        public string FSPROGSALEID;
        public string FSPROGBUYID { get; set; }
        public string FSPROGBUYDID { get; set; }
        public string FSPRDCENID { get; set; }
        public string FSPRDYYMM { get; set; }
        public DateTime FDSHOWDATE { get; set; }
        public string FSCONTENT { get; set; }
        public string FSMEMO { get; set; }
        public string FSEWEBPAGE { get; set; }
        public string FCDERIVE { get; set; }
        public string FSWEBNAME { get; set; }
        public string FSCHANNEL_ID { get; set; }
        public string FSDEL { get; set; }
        public string FSDELUSER { get; set; }
        public string FCOUT_BUY { get; set; }
        public string FSCRTUSER { get; set; }
        public DateTime FDCRTDATE { get; set; }
        public string FSUPDUSER { get; set; }
        public DateTime FDUPDDATE { get; set; }

    }
}
