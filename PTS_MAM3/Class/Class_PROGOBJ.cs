﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3
{
    public class Class_PROGOBJ
    {
        public string ID { get; set; }
        public string NAME { get; set; }
        public string SORT { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATED_BY_NAME { get; set; }
        public string UPDATED_BY { get; set; }
        public string UPDATED_BY_NAME { get; set; }
        private DateTime CREATED_DATE;
        //顯示的新增的日期
        public string SHOW_CREATED_DATE { get; set; }  
        private DateTime UPDATED_DATE;
        //顯示的修改的日期
        public string SHOW_UPDATED_DATE { get; set; }
               
    }
}


        //原本想說在Set時將字串轉型，把值塞到其它欄位
        //DateTime.TryParse(value, out UPDATED_DATE);
              
        //要判斷顯示資料為空時，顯示空白，重點就是不能用自己變數，而是要用該class裡的其他變數
        //private string AAA;

        ////顯示的修改日期
        //public string SHOW_UPDATED_DATE 
        //{
        //    get 
        //    {
        //        if (AAA == string.Empty)
        //            return "BBB";
        //        else
        //            return AAA; 
        //    }
        //    set
        //    {
        //        AAA = value;
        //    } 
        //}