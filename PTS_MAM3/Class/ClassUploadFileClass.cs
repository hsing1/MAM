﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.Class
{
    /// <summary>
    /// strARC_ID:入庫單單號
    /// strProgramId:
    /// strFSID:節目代號
    /// strEpisodeId:集數
    /// strType: G:節目帶 P:宣傳帶
    /// strDirectory:TSM儲存路徑(維智)
    /// strFileId:取檔號碼(至軒)
    /// strOFileName:原始檔案名稱
    /// strOExtensionName:原始副檔名
    /// strChannelId:頻道Id
    /// strUserId:使用者代碼
    /// strUploadFileType
    /// </summary>
    public class ClassUploadFileClass
    {
        public string strARC_ID { get; set; }
        public string strProgramId { get; set; }
        public string strFSID { get; set; }
        public string strEpisodeId { get; set; }
        public string strType { get; set; }
        public string strDirectory { get; set; }
        public string strFileId { get; set; }
        public string strOFileName { get; set; }
        public string strOExtensionName { get; set; }
        public string strChannelId { get; set; }
        public string srtFileSize { get; set; }
        public string strUserId { get; set; }
        public string strUploadFileType { get; set; }
        //input metadata
        public string strMetadataFSTITLE { get; set; }
        public string strMetadataFSFSDESCRIPTION { get; set; }
        public void clear()
        {
            strARC_ID = null;
            strProgramId =null;
            strFSID =null;
            strEpisodeId =null;
            strType=null;
            strDirectory =null;
            strFileId=null;
            strOFileName=null;
            strOExtensionName=null;
            strChannelId=null;
            srtFileSize=null;
            strUserId = null;
            strUploadFileType = null;
            strMetadataFSTITLE = null;
            strMetadataFSFSDESCRIPTION = null;
        }
    }
}
