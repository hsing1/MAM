﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Browser;

namespace PTS_MAM3.RPT
{
    public partial class RPT_FTP_01 : UserControl
    {
        WSCHANNEL.WSCHANNELSoapClient objChannel = new WSCHANNEL.WSCHANNELSoapClient();
        WSREPORT.WSREPORTSoapClient objReport = new WSREPORT.WSREPORTSoapClient();
        List<MyChannel> chalist = new List<MyChannel>();

        public RPT_FTP_01()
        {
            InitializeComponent();
            objChannel.GetChannelListCompleted += new EventHandler<WSCHANNEL.GetChannelListCompletedEventArgs>(objChannel_GetChannelListCompleted);
            objChannel.GetChannelListAsync();
            
            objReport.GetFTP_01StingCompleted += new EventHandler<WSREPORT.GetFTP_01StingCompletedEventArgs>(objReport_GetFTP_01StingCompleted);
            qDate.SelectedDate = DateTime.Now.Date;
        }

        void objReport_GetFTP_01StingCompleted(object sender, WSREPORT.GetFTP_01StingCompletedEventArgs e)
        {
            //throw new NotImplementedException();
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    if (e.Result.ToString().StartsWith("錯誤"))
                        MessageBox.Show(e.Result.ToString(), "提示訊息", MessageBoxButton.OK);
                    else
                        HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + e.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,toolbar=0,statusbar=0,menubar=0,scrollbar=0,resizable=yes");
                }
                else
                    MessageBox.Show("查無相關資料，或該筆資料已不存在資料庫中。", "提示訊息", MessageBoxButton.OK);
            }  
        }

        void objChannel_GetChannelListCompleted(object sender, WSCHANNEL.GetChannelListCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result.Count == 0)
                {
                    MessageBox.Show("查詢頻道清單時發生錯誤");
                    cbxCha.ItemsSource = null;
                    return;
                }
                else
                {
                    chalist.Clear();

                    chalist.Add(new MyChannel("(請選擇)", ""));

                    foreach (WSCHANNEL.Class_CHANNEL cha in e.Result)
                    {
                        chalist.Add(new MyChannel(cha.FSCHANNEL_NAME, cha.FSCHANNEL_ID));
                    }

                    cbxCha.ItemsSource = chalist;
                    cbxCha.SelectedIndex = 0;
                }
            }
        }

        private void btnQuery_Click(object sender, RoutedEventArgs e)
        {
            if (cbxCha.SelectedIndex <= 0)
            {
                MessageBox.Show("請選擇頻道別。");
                return;
            }

            if (qDate.SelectedDate.ToString() == "")
            {
                MessageBox.Show("請輸入要篩選的記錄日期。", "提示訊息", MessageBoxButton.OK);
                return;
            }

            String _FCHANNEL_ID = ((MyChannel)cbxCha.SelectedItem).id;
            String _DATE = ((DateTime)qDate.SelectedDate).ToString("yyyy/MM/dd"); 
            String _QUERY_BY = UserClass.userData.FSUSER_ID.ToString().Trim();

            objReport.GetFTP_01StingAsync(_FCHANNEL_ID, _DATE, _QUERY_BY);
        }
    }
}

public class MyChannel
{
    public String Name { get; set; }
    public String id { get; set; }

    public MyChannel(String Name, String id)
    {
        this.Name = Name;
        this.id = id;
    }
}