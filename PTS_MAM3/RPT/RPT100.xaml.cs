﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Browser;

namespace PTS_MAM3.RPT
{
    public partial class RPT100 : UserControl
    {

        private int intCbx1 = 0, intCbx2 = -1, intCbx3 = -1, intCbx4 = -1;
        Boolean refreshok = false;

        List<MyDept> deptlist1 = new List<MyDept>();
        List<MyDept> deptlist2 = new List<MyDept>();
        List<MyDept> deptlist3 = new List<MyDept>();
        List<MyUser> userlist4 = new List<MyUser>();

        WSREPORT.WSREPORTSoapClient objReport1 = new WSREPORT.WSREPORTSoapClient();
        WSREPORT.WSREPORTSoapClient objReport2 = new WSREPORT.WSREPORTSoapClient();
        WSREPORT.WSREPORTSoapClient objReport3 = new WSREPORT.WSREPORTSoapClient();
        WSREPORT.WSREPORTSoapClient objReport4 = new WSREPORT.WSREPORTSoapClient();
        WSREPORT.WSREPORTSoapClient objRpt = new WSREPORT.WSREPORTSoapClient();

        public RPT100()
        {
            InitializeComponent();

            objReport1.fnGetDeptListCompleted += new EventHandler<WSREPORT.fnGetDeptListCompletedEventArgs>(objReport1_fnGetDeptListCompleted);
            objReport2.fnGetDeptListCompleted += new EventHandler<WSREPORT.fnGetDeptListCompletedEventArgs>(objReport2_fnGetDeptListCompleted);
            objReport3.fnGetDeptListCompleted += new EventHandler<WSREPORT.fnGetDeptListCompletedEventArgs>(objReport3_fnGetDeptListCompleted);
            objReport4.fnGetTBUSERS_BY_DEPT_IDCompleted+=new EventHandler<WSREPORT.fnGetTBUSERS_BY_DEPT_IDCompletedEventArgs>(objReport4_fnGetTBUSERS_BY_DEPT_IDCompleted);
            objRpt.GetRPT100StingCompleted += new EventHandler<WSREPORT.GetRPT100StingCompletedEventArgs>(objRpt_GetRPT100StingCompleted);

            cbxDept1.SelectionChanged += new SelectionChangedEventHandler(cbxDept1_SelectionChanged);
            cbxDept2.SelectionChanged += new SelectionChangedEventHandler(cbxDept2_SelectionChanged);
            cbxDept3.SelectionChanged += new SelectionChangedEventHandler(cbxDept3_SelectionChanged);
            cbxUser.SelectionChanged += new SelectionChangedEventHandler(cbxUser_SelectionChanged);
 
            objReport1.fnGetDeptListAsync(intCbx1);
            //objReport2.fnGetDeptListAsync(intCbx2);
            //objReport3.fnGetDeptListAsync(intCbx3);
            //objReport4.fnGetTBUSERS_BY_DEPT_IDAsync(intCbx4.ToString());

            //初始元件
            date1.SelectedDate = DateTime.Now.Date;
            date2.SelectedDate = DateTime.Now.Date;
        }  

        void objRpt_GetRPT100StingCompleted(object sender, WSREPORT.GetRPT100StingCompletedEventArgs e)
        {
            //throw new NotImplementedException();
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    if (e.Result.ToString().StartsWith("錯誤"))
                        MessageBox.Show(e.Result.ToString(), "提示訊息", MessageBoxButton.OK);
                    else
                        //HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + e.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,toolbar=0,statusbar=0,menubar=0,scrollbar=0");
                        HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + e.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,resizable=yes,statusbar=0,menubar=0");
                }
                else
                    MessageBox.Show("查無相關資料，或該筆資料已不存在資料庫中。", "提示訊息", MessageBoxButton.OK);
            }      
        } 


#region 資料查詢結束 
 
        void objReport1_fnGetDeptListCompleted(object sender, WSREPORT.fnGetDeptListCompletedEventArgs e)
            {
                if (e.Error == null)
                {
                    cbxDept2.ItemsSource = null;
                    cbxDept3.ItemsSource = null;
                    cbxUser.ItemsSource = null;

                    if (e.Result.Count == 0)
                    {
                        MessageBox.Show("查詢單位時發生錯誤");
                        return;
                    }
                    else
                    {
                        deptlist1.Clear();
                         
                        foreach (WSREPORT.DeptStruct dept in e.Result)
                        {
                            deptlist1.Add(new MyDept(dept.FSDpeName_ChtName, dept.FNDEP_ID));
                        }

                        cbxDept1.ItemsSource = deptlist1; 
                    }
                }
            }

        void objReport2_fnGetDeptListCompleted(object sender, WSREPORT.fnGetDeptListCompletedEventArgs e)
        {
                    deptlist2.Clear();

                    cbxDept2.ItemsSource = null;
                    cbxDept3.ItemsSource = null;
                    cbxUser.ItemsSource = null;

            if (e.Error == null)
            {
                if (e.Result.Count == 0)
                {
                    //MessageBox.Show("查不到子單位");
                    cbxDept3.ItemsSource = null;
                    return;
                }
                else
                {


                    if (intCbx2 != -1)
                    { 
                        foreach (WSREPORT.DeptStruct dept in e.Result)
                        {
                            deptlist2.Add(new MyDept(dept.FSDpeName_ChtName, dept.FNDEP_ID));
                        } 
                    } 

                    cbxDept2.ItemsSource = deptlist2; 
                }
            }
        }

        void objReport3_fnGetDeptListCompleted(object sender, WSREPORT.fnGetDeptListCompletedEventArgs e)
        {

                    deptlist3.Clear();

                    cbxDept3.ItemsSource = null;
                    cbxUser.ItemsSource = null;

            if (e.Error == null)
            {
                if (e.Result.Count == 0)
                {
                    //MessageBox.Show("查不到子單位");
                    return;
                }
                else
                {


                    if (intCbx3 != -1)
                    {
                        foreach (WSREPORT.DeptStruct dept in e.Result)
                        {
                            deptlist3.Add(new MyDept(dept.FSDpeName_ChtName, dept.FNDEP_ID));
                        }
                    }

                    cbxDept3.ItemsSource = deptlist3; 
                }
            }
        }

        void objReport4_fnGetTBUSERS_BY_DEPT_IDCompleted(object sender, WSREPORT.fnGetTBUSERS_BY_DEPT_IDCompletedEventArgs e)
        {
            if (cbxDept2.SelectedIndex <= 0)
            {
                return;
            }else if (refreshok)
            {
                return;
            }
            else
            {
                refreshok = true;
            }

                    userlist4.Clear();

                    cbxUser.ItemsSource = null;
            if (e.Error == null)
            {
                if (e.Result.Count == 0)
                {
                    //MessageBox.Show("此單位查無人員清單");
                    return;
                }
                else
                {


                    //if (intCbx4 != -1)
                    //{
                        foreach (WSREPORT.UserStruct user in e.Result)
                        {
                            String _Name = user.FSUPPERDEPT_CHTNAME + " \\ " +
                                         ((user.FSDEP_CHTNAME.Trim() == "") ? "" : (user.FSDEP_CHTNAME.Trim() + " \\ ")) +
                                         ((user.FSGROUP_CHTNAME.Trim() == "") ? "" : (user.FSGROUP_CHTNAME.Trim() + " \\ ")) + 
                                         user.FSUSER_ID + "-" + user.FSUSER_ChtName;

                            userlist4.Add(new MyUser(_Name, user.FSUSER_ID));
                        }
                    //}

                    cbxUser.ItemsSource = userlist4;
                }
            }
        } 

#endregion


#region 下拉選單變更

        void cbxDept1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            refreshok = false;
            intCbx2 = ((MyDept)cbxDept1.SelectedItem).id;
            objReport2.fnGetDeptListAsync(intCbx2);
            objReport4.fnGetTBUSERS_BY_DEPT_IDAsync(intCbx2.ToString());
        }

        void cbxDept2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            refreshok = false;
            if (cbxDept2.Items.Count != 0)
            {
                intCbx3 = ((MyDept)cbxDept2.SelectedItem).id;
                objReport3.fnGetDeptListAsync(intCbx3);
            }else{
                cbxDept3.ItemsSource = null;
            }   
                objReport4.fnGetTBUSERS_BY_DEPT_IDAsync(intCbx3.ToString());
        }

        void cbxDept3_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            refreshok = false;
            if (cbxDept3.Items.Count != 0)
            {
                intCbx4 = ((MyDept)cbxDept3.SelectedItem).id;
            }
            //else
            //{
            //    cbxUser.ItemsSource = null;
            //}
                objReport4.fnGetTBUSERS_BY_DEPT_IDAsync(intCbx4.ToString());
        }

        void cbxUser_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
                //throw new NotImplementedException();
        }

#endregion


#region 其他元件處理
        
    private void OKButton_Click(object sender, RoutedEventArgs e)
    {
        if ((date1.SelectedDate.ToString() == "") && (date2.SelectedDate.ToString() == ""))
        {
            MessageBox.Show("為避免資料過多，請至少輸入起始日期或結束日期。", "提示訊息", MessageBoxButton.OK);
            return;
        }

        String _FSUSER_ID ="";
        String _FDDATE_S ="1900/01/01";
        String _FDDATE_E = "1900/01/01";
        String _FSSP_NAME = "";
        String _QUERY_BY = UserClass.userData.FSUSER_ID.ToString().Trim();

        if (chkByUser.IsChecked == true)
        {
            if (cbxUser.SelectedIndex < 0 || cbxUser.Items.Count==0)
            {
                MessageBox.Show("請選擇要篩選的使用者。", "提示訊息", MessageBoxButton.OK);
                return;
            }
            _FSUSER_ID = ((MyUser)cbxUser.SelectedItem).id;
        }
        if (date1.SelectedDate.ToString() != "")
        {
            DateTime dt = (DateTime)date1.SelectedDate;
            _FDDATE_S = dt.ToString("yyyy/MM/dd");
        }
        if (date2.SelectedDate.ToString() != "")
        {
            DateTime dt = (DateTime)date2.SelectedDate;
            _FDDATE_E = dt.ToString("yyyy/MM/dd");
        }

        objRpt.GetRPT100StingAsync(_FSUSER_ID, _FDDATE_S, _FDDATE_E, _FSSP_NAME, _QUERY_BY);
    }
        
    private void chkByUser_Unchecked(object sender, RoutedEventArgs e)
    {
        grdByUser.Visibility = Visibility.Collapsed;
    }

    private void chkByUser_Checked(object sender, RoutedEventArgs e)
    {
        grdByUser.Visibility = Visibility.Visible;
    }

#endregion

         

    }
}


public class MyDept 
{ 
   public String Name { get; set; } 
   public int id { get; set; }

   public MyDept(String Name, int id)
   {
       this.Name = Name;
       this.id = id;
   }
 }


public class MyUser
{
    public String Name { get; set; }
    public String id { get; set; }

    public MyUser(String Name, String id)
    {
        this.Name = Name;
        this.id = id;
    }
}