﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSBookingFunction;
using System.Windows.Browser;

namespace PTS_MAM3.RPT
{
    public partial class RPT_BOOKING_03 : UserControl
    {
        public RPT_BOOKING_03()
        {
            InitializeComponent();
        }

        private void Button_Search_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            string _sDate = "";
            string _eDate = "";
            if (this.DatePicker_Begin.SelectedDate.HasValue)
            {
                _sDate = this.DatePicker_Begin.SelectedDate.Value.ToString("yyyy/MM/dd");
            }
            if (this.DatePicker_End.SelectedDate.HasValue)
            {
                _eDate = this.DatePicker_End.SelectedDate.Value.ToString("yyyy/MM/dd");
            }
            WSBookingFunctionSoapClient booking = new WSBookingFunctionSoapClient();
            booking.GetBookingReasonChannelAnalysisReportCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {
                        if (args.Result.ToString().StartsWith("錯誤"))
                            MessageBox.Show(args.Result.ToString(), "提示訊息", MessageBoxButton.OK);
                        else
                            HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + args.Result, UriKind.Relative), "_blank", "height=600,width=1100,top=100,left=100,toolbar=0,statusbar=0,menubar=0,scrollbar=0,resizable=yes");
                    }
                    else
                        MessageBox.Show("查無相關資料，或該筆資料已不存在資料庫中。", "提示訊息", MessageBoxButton.OK);
                }
            };

            booking.GetBookingReasonChannelAnalysisReportAsync(_sDate, _eDate, UserClass.userData.FSUSER_ID, UserClass.userData.FSSESSION_ID);

        }
    }
}
