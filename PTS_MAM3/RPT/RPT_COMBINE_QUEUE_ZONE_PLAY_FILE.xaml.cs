﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Browser;

namespace PTS_MAM3.RPT
{
    public partial class RPT_COMBINE_QUEUE_ZONE_PLAY_FILE : UserControl
    {
        public RPT_COMBINE_QUEUE_ZONE_PLAY_FILE()
        {
            InitializeComponent();
        }

        private void Button_Search_Click(object sender, RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            string _sDate = "";
            string _eDate = "";
            if (this.DatePicker_Begin.SelectedDate.HasValue)
            {
                _sDate = this.DatePicker_Begin.SelectedDate.Value.ToString("yyyy/MM/dd");
            }
            if (this.DatePicker_End.SelectedDate.HasValue)
            {
                _eDate = this.DatePicker_End.SelectedDate.Value.ToString("yyyy/MM/dd");
            }
            WSPGMSendSQL.SendSQLSoapClient RPT_TBPGM_COMBINE_QUEUE_ZONE_FILE = new WSPGMSendSQL.SendSQLSoapClient();
            RPT_TBPGM_COMBINE_QUEUE_ZONE_FILE.Get_Combine_queue_Prog_ListAsync(_sDate, _eDate, UserClass.userData.FSUSER_ChtName);
            RPT_TBPGM_COMBINE_QUEUE_ZONE_FILE.Get_Combine_queue_Prog_ListCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result == "沒有節目資料")
                    {
                        MessageBox.Show("沒有節目資料");
                    }
                    else
                    {
                        HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + args.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,toolbar=0,statusbar=0,menubar=0,scrollbar=0,resizable=yes");

                    }

                }
                else
                    MessageBox.Show("產生節目播出清單失敗");

            };       

            
        }
    }
}
