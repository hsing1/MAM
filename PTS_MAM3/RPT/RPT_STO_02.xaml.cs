﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using PTS_MAM3.ProgData;
using System.Windows.Browser;

namespace PTS_MAM3.RPT
{
    public partial class RPT_STO_02 : Page
    {
        PROGDATA_VIEW PROGDATA_VIEW_frm = new PROGDATA_VIEW();
        WSREPORT.WSREPORTSoapClient objReport = new WSREPORT.WSREPORTSoapClient();
        private string FSID = "";
        private string FSNAME = "";
        public RPT_STO_02()
        {
            InitializeComponent();
            PROGDATA_VIEW_frm.Closed += (s, args) =>
            {
                if (PROGDATA_VIEW_frm.DialogResult == true)
                {
                    FSNAME = PROGDATA_VIEW_frm.strProgName_View;
                    FSID = PROGDATA_VIEW_frm.strProgID_View;
                    label2.Content = FSNAME;
                }
            };
            objReport.GetRPT_STO_02Completed += new EventHandler<WSREPORT.GetRPT_STO_02CompletedEventArgs>(objReport_GetRPT_STO_02Completed);
        }

        void objReport_GetRPT_STO_02Completed(object sender, WSREPORT.GetRPT_STO_02CompletedEventArgs e)
        {
            string toUrl = e.Result.ToString();
            HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + e.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,resizable=yes,statusbar=0,menubar=0");
        }



        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void textBox1_TextChanged(object sender, TextChangedEventArgs e)
        {
        }



        private void button1_Click(object sender, RoutedEventArgs e)
        {
            PROGDATA_VIEW_frm.Show();
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            if(FSID!="" && textBox1.Text!="")
            {
                objReport.GetRPT_STO_02Async(FSID, textBox1.Text.ToString());
            }
        }

    }
}
