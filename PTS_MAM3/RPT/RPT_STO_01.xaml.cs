﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Windows.Browser;

namespace PTS_MAM3.RPT
{
    public partial class RPT_STO_01 : Page
    {
        public RPT_STO_01()
        {
            InitializeComponent();
            grid1.Visibility = Visibility.Collapsed;
            objReport.GetRPT_STO_01Completed += (s, args) =>
                {
                    if (args.Error == null)
                    {
                        string toUrl = args.Result.ToString();
                        HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + args.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,resizable=yes,statusbar=0,menubar=0");
                    }
                };
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }
        WSREPORT.WSREPORTSoapClient objReport = new WSREPORT.WSREPORTSoapClient();
        private void comboBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (((ComboBoxItem)comboBox1.SelectedItem).Content.ToString() == "頻道轉檔內容分析")
                grid1.Visibility = Visibility.Visible;
            else if (((ComboBoxItem)comboBox1.SelectedItem).Content.ToString() == "入庫量分析")
                grid1.Visibility = Visibility.Visible;
            else if (((ComboBoxItem)comboBox1.SelectedItem).Content.ToString() == "頻道入庫分析")
                grid1.Visibility = Visibility.Visible;
            else if (((ComboBoxItem)comboBox1.SelectedItem).Content.ToString() == "頻道轉檔分析")
                grid1.Visibility = Visibility.Visible;
            else
                grid1.Visibility = Visibility.Collapsed;
        }

        private void button7_Click(object sender, RoutedEventArgs e)
        {
            string SelectTag = ((ComboBoxItem)comboBox1.SelectedItem).Tag.ToString();
            if (Convert.ToInt32(((ComboBoxItem)S_Year.SelectedItem).Content.ToString()) <= Convert.ToInt32(((ComboBoxItem)E_Year.SelectedItem).Content.ToString()))
                objReport.GetRPT_STO_01Async(((ComboBoxItem)S_Year.SelectedItem).Content.ToString(), ((ComboBoxItem)S_Month.SelectedItem).Content.ToString(), ((ComboBoxItem)E_Year.SelectedItem).Content.ToString(), ((ComboBoxItem)E_Month.SelectedItem).Content.ToString(), UserClass.userData.FSUSER_ID, SelectTag);
            else
            { MessageBox.Show("請確認日期區間"); }
        }

    }
}
