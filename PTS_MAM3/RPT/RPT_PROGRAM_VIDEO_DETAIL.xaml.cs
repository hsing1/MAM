﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.PRG;
using PTS_MAM3.WSPROG_M;
using PTS_MAM3.ProgData;
using System.Windows.Browser;

namespace PTS_MAM3.RPT
{
    public partial class RPT_PROGRAM_VIDEO_DETAIL : UserControl
    {
        private WSPROG_MSoapClient wsPROG_M_CLIENT = new WSPROG_MSoapClient();

        private string fsPROG_ID = string.Empty;

        public RPT_PROGRAM_VIDEO_DETAIL()
        {
            InitializeComponent();

            this.RadBusyIndicator.BusyContent = "報表產生中，請稍後...";
            this.RadBusyIndicator.FontSize = 16;

            wsPROG_M_CLIENT.fnRPT_PROGRAM_VIDEO_DETAILCompleted += new EventHandler<fnRPT_PROGRAM_VIDEO_DETAILCompletedEventArgs>(wsPROG_M_CLIENT_fnRPT_PROGRAM_VIDEO_DETAILCompleted);
        }

        void wsPROG_M_CLIENT_fnRPT_PROGRAM_VIDEO_DETAILCompleted(object sender, fnRPT_PROGRAM_VIDEO_DETAILCompletedEventArgs e)
        {
            if (e.Error != null || string.IsNullOrEmpty(e.Result))
            {
                MessageBox.Show("產生報表錯誤!", "訊息", MessageBoxButton.OK);
            }
            else
            {
                HtmlPage.Window.Navigate(new Uri(e.Result, UriKind.Absolute), "_blank");
            }
            this.RadBusyIndicator.IsBusy = false;
        }

        private void Button_PROG_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            PROGDATA_VIEW PROGDATA_VIEW = new PROGDATA_VIEW();
            PROGDATA_VIEW.Show();

            PROGDATA_VIEW.Closed += new EventHandler(PROGDATA_VIEW_Closed);
        }

        void PROGDATA_VIEW_Closed(object sender, EventArgs e)
        {
            if ((sender as PROGDATA_VIEW).DialogResult == true)
            {
                this.TextBox_PROGRAM_NAME.Text = (sender as PROGDATA_VIEW).strProgName_View;
                fsPROG_ID = (sender as PROGDATA_VIEW).strProgID_View;
            }
        }

        private void Button_EXPORT_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            int fnEPISODE_BEG = 0;
            int fnEPISODE_END = 0;
            string fsCHANNEL = string.Empty;
            string fsSOURCE = string.Empty;

            if (string.IsNullOrEmpty(fsPROG_ID) == true)
            {
                MessageBox.Show("請選擇節目!", "訊息", MessageBoxButton.OK);
            }
            else if (string.IsNullOrEmpty(this.TextBox_EPISODE_BEG.Text) == true || string.IsNullOrEmpty(this.TextBox_EPISODE_END.Text) == true)
            {
                MessageBox.Show("請輸入集數!", "訊息", MessageBoxButton.OK);
            }
            else if (int.TryParse(this.TextBox_EPISODE_BEG.Text, out fnEPISODE_BEG) == false || int.TryParse(this.TextBox_EPISODE_END.Text, out fnEPISODE_END) == false)
            {
                MessageBox.Show("集數需為數字!", "訊息", MessageBoxButton.OK);
            }
            else
            {
                if (this.RadioButton_CHANNEL_01.IsChecked == true) fsSOURCE = "01";
                if (this.RadioButton_CHANNEL_02.IsChecked == true) fsSOURCE = "02";
                if (this.RadioButton_CHANNEL_07.IsChecked == true) fsSOURCE = "07";
                if (this.RadioButton_CHANNEL_08.IsChecked == true) fsSOURCE = "08";

                if (this.RadioButton_SOURCE_01.IsChecked == true) fsSOURCE = "MAM-VIDEO";

                wsPROG_M_CLIENT.fnRPT_PROGRAM_VIDEO_DETAILAsync("G", fsPROG_ID, fnEPISODE_BEG.ToString(), fnEPISODE_END.ToString(), fsCHANNEL, fsSOURCE, UserClass.userData.FSUSER_ID);
                this.RadBusyIndicator.IsBusy = true;
            }
        }
    }
}
