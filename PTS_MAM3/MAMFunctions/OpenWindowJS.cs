﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.MAMFunctions
{
    public class OpenWindowJS
    {
        #region Open Full Screen Window
        /// <summary>Open Browser Window: Full Screen</summary>
        /// <param name="URL">string</param>
        /// <param name="Name">string</param>
        /// <param name="FullScreen">bool</param>
        /// <param name="MenuBar">bool</param>
        /// <param name="Resizable">bool</param>
        /// <param name="Toolbar">bool</param>
        /// <param name="Location">bool</param>
        /// <param name="StatusBar">bool</param>
        /// <param name="Copyhistory">bool</param>
        /// <param name="ScrollBalr">bool</param>
        /// <returns>string</returns>
        public static string GetScript
           (string URL,
            string Name,
            bool FullScreen,
            bool MenuBar,
            bool Resizable,
            bool Toolbar,
            bool Location,
            bool StatusBar,
            bool Copyhistory,
            bool ScrollBar)
        {
            string ret = "javascript:void window.open ('";
            ret = ret + URL + "', '" + Name + "', '";
            ret = ret + "fullScreen=" + ((FullScreen) ? "1" : "0") + ",";
            ret = ret + "resizable=" + ((Resizable) ? "1" : "0") + ",";
            ret = ret + "menuBar=" + ((MenuBar) ? "1" : "0") + ",";
            ret = ret + "toolbar=" + ((Toolbar) ? "1" : "0") + ",";
            ret = ret + "location=" + ((Location) ? "1" : "0") + ",";
            ret = ret + "statusBar=" + ((StatusBar) ? "1" : "0") + ",";
            ret = ret + "copyhistory=" + ((Copyhistory) ? "1" : "0") + ",";
            ret = ret + "scrollBar=" + ((ScrollBar) ? "1" : "0") + ",";
            ret = ret + "')";
            return ret;
        }
        #endregion

        #region Open Window with adjustable Width/Height
        /// <summary>Open Browser Window, set Width/Height</summary>
        /// <param name="URL">string</param>
        /// <param name="Name">string</param>
        /// <param name="Width">string</param>
        /// <param name="Height">string</param>
        /// <param name="MenuBar">bool</param>
        /// <param name="Toolbar">bool</param>
        /// <param name="Location">bool</param>
        /// <param name="StatusBar">bool</param>
        /// <param name="Copyhistory">bool</param>
        /// <param name="ScrollBar">bool</param>
        /// <returns>string</returns>
        public static string GetScript
           (string URL,
            string Name,
            string Width,
            string Height,
            bool MenuBar,
            bool Toolbar,
            bool Location,
            bool StatusBar,
            bool Copyhistory,
            bool ScrollBar)
        {
            string ret = "javascript:void window.open ('";
            ret = ret + URL + "', '" + Name + "', '";

            ret = ret + "resizable=1,";
            ret = ret + "width=" + Width + ",";
            ret = ret + "height=" + Height + ",";

            ret = ret + "menuBar=" + ((MenuBar) ? "1" : "0") + ",";
            ret = ret + "toolbar=" + ((Toolbar) ? "1" : "0") + ",";
            ret = ret + "location=" + ((Location) ? "1" : "0") + ",";
            ret = ret + "statusBar=" + ((StatusBar) ? "1" : "0") + ",";
            ret = ret + "copyhistory=" + ((Copyhistory) ? "1" : "0") + ",";
            ret = ret + "scrollBar=" + ((ScrollBar) ? "1" : "0") + ",";
            ret = ret + "')";
            return ret;
        }
        #endregion
    }
}
