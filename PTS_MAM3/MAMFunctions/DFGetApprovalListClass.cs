﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.MAMFunctions
{
    public class DFGetApprovalListClass
    {
        private string DF_GetApprovalList_MemberNameField;

        public string DF_GetApprovalList_MemberName
        {
            get { return DF_GetApprovalList_MemberNameField; }
            set { DF_GetApprovalList_MemberNameField = value; }
        }
        private string DF_GEtApprovalList_EnterdateField;

        public string DF_GEtApprovalList_Enterdate
        {
            get { return DF_GEtApprovalList_EnterdateField; }
            set { DF_GEtApprovalList_EnterdateField = value; }
        }
        private string DF_GEtApprovalList_MemberDisplayNameField;

        public string DF_GEtApprovalList_MemberDisplayName
        {
            get { return DF_GEtApprovalList_MemberDisplayNameField; }
            set { DF_GEtApprovalList_MemberDisplayNameField = value; }
        }

        private string DF_GEtApprovalList_FinishdateField;

        public string DF_GEtApprovalList_Finishdate
        {
            get { return DF_GEtApprovalList_FinishdateField; }
            set { DF_GEtApprovalList_FinishdateField = value; }
        }

        private string DF_GetApprovalList_NodeNameField;

        public string DF_GetApprovalList_NodeName
        {
            get { return DF_GetApprovalList_NodeNameField; }
            set { DF_GetApprovalList_NodeNameField = value; }
        }

        private string DF_GetApprovalList_StatusField;

        public string DF_GetApprovalList_Status
        {
            get { return DF_GetApprovalList_StatusField; }
            set { DF_GetApprovalList_StatusField = value; }
        }

        private string DF_GetApprovalList_FormUrlField;

        public string DF_GetApprovalList_FormUrl
        {
            get { return DF_GetApprovalList_FormUrlField; }
            set { DF_GetApprovalList_FormUrlField = value; }
        }

        private string DF_GetApprovalList_TTLidField;

        public string DF_GetApprovalList_TTLid
        {
            get { return DF_GetApprovalList_TTLidField; }
            set { DF_GetApprovalList_TTLidField = value; }
        }

        private string DF_GetApprovalList_NodeidField;

        public string DF_GetApprovalList_Nodeid
        {
            get { return DF_GetApprovalList_NodeidField; }
            set { DF_GetApprovalList_NodeidField = value; }
        }

        private string DF_GetApprovalList_CommentField;

        public string DF_GetApprovalList_Comment
        {
            get { return DF_GetApprovalList_CommentField; }
            set { DF_GetApprovalList_CommentField = value; }
        }

        private string DF_GetApprovalList_CommentPositionField;

        public string DF_GetApprovalList_CommentPosition
        {
            get { return DF_GetApprovalList_CommentPositionField; }
            set { DF_GetApprovalList_CommentPositionField = value; }
        }
    }
}
