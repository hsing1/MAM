﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.MAMFunctions
{
    public class FlowFrameTagObj
    {
        private string DF_FormIdField;

        public string DF_FormId
        {
            get { return DF_FormIdField; }
            set { DF_FormIdField = value; }
        }

        private string DF_GetTTL_ReceiveidField;

        public string DF_GetTTL_Receiveid
        {
            get { return DF_GetTTL_ReceiveidField; }
            set { DF_GetTTL_ReceiveidField = value; }
        }

        private string DF_GetTTL_TtlidField;

        public string DF_GetTTL_Ttlid
        {
            get { return DF_GetTTL_TtlidField; }
            set { DF_GetTTL_TtlidField = value; }
        }

        private string DF_GetVariableValue_NextXamlNameField;

        public string DF_GetVariableValue_NextXamlName
        {
            get { return DF_GetVariableValue_NextXamlNameField; }
            set { DF_GetVariableValue_NextXamlNameField = value; }
        }

        private string DF_GetFlowChartUrl_ValueField;
        public string DF_GetFlowChartUrl_Value
        {
            get { return DF_GetFlowChartUrl_ValueField; }
            set { DF_GetFlowChartUrl_ValueField = value; }
        }
    }
}
