﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.MAMFunctions
{
    public class TBJoinClass
    {
        public string FSPROGBUYID { get; set; }
        public string FSPROGBUYNAME { get; set; }
        public string FSPROGBUYDID { get; set; }
        public string FSPROGBUYDNAME { get; set; }
        public string FSSORT { get; set; }
        public string FSCREATED_BY { get; set; }
        public DateTime FDCREATED_DATE { get; set; }
        public string FSUPDATED_BY { get; set; }
        public Nullable<DateTime> FDUPDATED_DATE { get; set; }
    }
}
