﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.MAMFunctions
{
    public class ProposalClass
    {
        //存放PROPOSAL Session 資料
        private static PTS_MAM3.WSPROPOSAL.Class_PROPOSAL m_PROPOSALData;
        public static PTS_MAM3.WSPROPOSAL.Class_PROPOSAL PROPOSALData
        {
            get
            {
                return m_PROPOSALData;
            }
            set
            {
                m_PROPOSALData = value;
            }
        }
    }
}
