﻿using System.Collections.Generic;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace PTS_MAM3.MAMFunctions
{
    public static class MAMFunctions
    {
        public static ObservableCollection<string> fnGetValueList(string SQLstr)
        {

            ObservableCollection <string> rtnList  = new ObservableCollection <string>() ;

            WSMAMFunctions.WSMAMFunctionsSoapClient MAMFunctionObj = new WSMAMFunctions.WSMAMFunctionsSoapClient();

            

            MAMFunctionObj.fnGetValueCompleted += (s, args) =>
            {
                if (args.Error != null)
                {
                    if (args.Result != null)
                    {

                        rtnList = (ObservableCollection<string>)args.Result;                         
                    }

                }
                
            };

            MAMFunctionObj.fnGetValueAsync(SQLstr);

            return rtnList; 
        }
    }
}
