﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.MAMFunctions
{
    public class DFQueryActivityXML
    {
        private string DF_Form_FormIdField;

        public string DF_Form_FormId
        {
            get { return DF_Form_FormIdField; }
            set { DF_Form_FormIdField = value; }
        }

        private string DF_QueryActivity_ApplynameField;

        public string DF_QueryActivity_Applyname
        {
            get { return DF_QueryActivity_ApplynameField; }
            set { DF_QueryActivity_ApplynameField = value; }
        }
        private string DF_QueryActivity_ActiveidField;

        public string DF_QueryActivity_Activeid
        {
            get { return DF_QueryActivity_ActiveidField; }
            set { DF_QueryActivity_ActiveidField = value; }
        }
        private string DF_QueryActivity_EnterdateField;

        public string DF_QueryActivity_Enterdate
        {
            get { return DF_QueryActivity_EnterdateField; }
            set { DF_QueryActivity_EnterdateField = value; }
        }
        private string DF_QueryActivity_VesionField;

        public string DF_QueryActivity_Vesion
        {
            get { return DF_QueryActivity_VesionField; }
            set { DF_QueryActivity_VesionField = value; }
        }
        private string DF_QueryActivity_StatusField;

        public string DF_QueryActivity_Status
        {
            get { return DF_QueryActivity_StatusField; }
            set { DF_QueryActivity_StatusField = value; }
        }
        private string DF_QueryActivity_DisplaynameField;

        public string DF_QueryActivity_Displayname
        {
            get { return DF_QueryActivity_DisplaynameField; }
            set { DF_QueryActivity_DisplaynameField = value; }
        }
        private string DF_QueryActivity_FinishdateField;

        public string DF_QueryActivity_Finishdate
        {
            get { return DF_QueryActivity_FinishdateField; }
            set { DF_QueryActivity_FinishdateField = value; }
        }

    }
}
