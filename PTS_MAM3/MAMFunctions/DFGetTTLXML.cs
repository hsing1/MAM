﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PTS_MAM3.MAMFunctions
{
    public class Tag
    {
        private string FormIdField;
        public string FormId
        {
            get { return FormIdField; }
            set { FormIdField = value; }
        }
        private string TtlidField;
        public string Ttlid
        {
            get { return TtlidField; }
            set { TtlidField = value; }
        }

        private string DF_Receiveid;
        public string Receiveid
        {
            get { return DF_Receiveid; }
            set { DF_Receiveid = value; }
        }
    }

    public class DFGetTTLXML
    {
        private Tag button;
        public Tag ButtonTag
        {
            get { return button; }
            set { button = value; }
        }

        private string FunctionField01;

        public string Function01
        {
            get { return FunctionField01; }
            set { FunctionField01 = value; }
        }

        private string FunctionField02;

        public string Function02
        {
            get { return FunctionField02; }
            set { FunctionField02 = value; }
        }

        private string FunctionField03;

        public string Function03
        {
            get { return FunctionField03; }
            set { FunctionField03 = value; }
        }

        private string FunctionField04;

        public string Function04
        {
            get { return FunctionField04; }
            set { FunctionField04 = value; }
        }

        private string FunctionField05;

        public string Function05
        {
            get { return FunctionField05; }
            set { FunctionField01 = value; }
        }
        
        private string DF_FormId;

        public string FormId
        {
            get { return DF_FormId; }
            set { DF_FormId = value; }
        }
        private string DF_FormUrl;
        
        public string FormUrl
        {
            get { return DF_FormUrl; }
            set { DF_FormUrl = value; }
        }
        private string DF_Applyname;
        [Display(Name = "申請者帳號", Description = "申請者的帳號")]
        [Required(ErrorMessage = "帳號是必須資料")]
        public string Applyname
        {
            get { return DF_Applyname; }
            set { DF_Applyname = value; }
        }
        private string DF_ReceiveDate;

        public string ReceiveDate
        {
            get { return DF_ReceiveDate; }
            set { DF_ReceiveDate = value; }
        }
        private string DF_Priority;

        public string Priority
        {
            get { return DF_Priority; }
            set { DF_Priority = value; }
        }
        private string DF_Title;

        public string Title
        {
            get { return DF_Title; }
            set { DF_Title = value; }
        }

        //用於儲存表單Id的
        //[Display(Name = "在Flow中表單的Id", Description = "在Flow中表單的Id")]
        //[Required(ErrorMessage = "表單Id是必須資料")]
        private string DF_Ttlid;

        public string Ttlid
        {
            get { return DF_Ttlid; }
            set { DF_Ttlid = value; }
        }

        private string DF_Version;

        public string Version
        {
            get { return DF_Version; }
            set { DF_Version = value; }
        }
        private string DF_HaveAttach;

        public string HaveAttach
        {
            get { return DF_HaveAttach; }
            set { DF_HaveAttach = value; }
        }
        private string DF_Remark;

        public string Remark
        {
            get { return DF_Remark; }
            set { DF_Remark = value; }
        }
        private string DF_Receiveid;

        public string Receiveid
        {
            get { return DF_Receiveid; }
            set { DF_Receiveid = value; }
        }
        private string DF_Batch;

        public string Batch
        {
            get { return DF_Batch; }
            set { DF_Batch = value; }
        }
        private string DF_OriginalName;

        public string OriginalName
        {
            get { return DF_OriginalName; }
            set { DF_OriginalName = value; }
        }
        private string DF_FlowChartUrl;

        public string FlowChartUrl
        {
            get { return DF_FlowChartUrl; }
            set { DF_FlowChartUrl = value; }
        }
        private string DF_flowid;

        public string flowid
        {
            get { return DF_flowid; }
            set { DF_flowid = value; }
        }
        private string DF_OverDueDate;

        public string OverDueDate
        {
            get { return DF_OverDueDate; }
            set { DF_OverDueDate = value; }
        }
        private string DF_Addsign;

        public string Addsign
        {
            get { return DF_Addsign; }
            set { DF_Addsign = value; }
        }
        private string DF_Nodeid;

        public string Nodeid
        {
            get { return DF_Nodeid; }
            set { DF_Nodeid = value; }
        }
        private string DF_Roleid;

        public string Roleid
        {
            get { return DF_Roleid; }
            set { DF_Roleid = value; }
        }
    }
}
