﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace PTS_MAM3
{
    public static class ModuleClass
    {

        public static void getModulePermissionByUserId(string fsuser_id) {

            WSUserObject.WSUserObjectSoapClient UserObj = new WSUserObject.WSUserObjectSoapClient(); 

           

           // UserObjectSvc.UserObjectClient UserObj = new UserObjectSvc.UserObjectClient();
            //UserObj.fnGetModuleListCompleted += (s, args) =>
            UserObj.fnGetModuleListCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {

                        moduleList = args.Result;
                        //                                                
                    }
                }
            };

            UserObj.fnGetModuleListAsync(fsuser_id); 



        }



        //存放User Session 資料
        private static ObservableCollection<WSUserObject.ModuleStruct> m_moduleList = new ObservableCollection<WSUserObject.ModuleStruct>();

        public static ObservableCollection<WSUserObject.ModuleStruct> moduleList
        {
            get
            {
                return m_moduleList; 
            }
            set
            {
                m_moduleList = value; 
            }
        }

        public static Boolean getModulePermission(string fsmodule_id)
        {
            Boolean brtn = false;

            foreach (WSUserObject.ModuleStruct item in moduleList)
            {
                if (item.FSMODULE_ID == fsmodule_id)
                {
                    brtn = true;
                    break; 
                }
            }


            return brtn; 
        }

        public static Boolean getModulePermissionByName(string fsmoudule_cat_id,string fsunc_name)
        {
            Boolean brtn = false;

            foreach (WSUserObject.ModuleStruct item in moduleList)
            {
                if ((item.FSFUNC_NAME == fsunc_name) && (item.FSMODULE_ID.Substring(0,2) == fsmoudule_cat_id) )
                {
                    brtn = true;
                    break;
                }
            }


            return brtn;
        }


    }
}
