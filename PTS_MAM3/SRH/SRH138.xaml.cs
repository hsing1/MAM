﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SearchClassLibrary;
using PTS_MAM3.WSBookingFunction;
using System.Xml;
using System.Xml.Linq;
using System.Text;
using PTS_MAM3.WSTSM_GetFileInfo;
using System.Windows.Media.Imaging;
using PTS_MAM3.WCFSearchDataBaseFunction;


namespace PTS_MAM3.SRH
{
    public partial class SRH138 : ChildWindow
    {
        private string _bookingNo;
        private string _fsfileno;
        private string _fscategory;
        private long _start_timecode;
        private long _begin_timecode;
        private string _fsguid;

        WSBookingFunctionSoapClient booking = new WSBookingFunctionSoapClient();
        //建立Video物件
        PreviewPlayer.SLVideoPlayer player = new PreviewPlayer.SLVideoPlayer("", 0);

        List<CheckBookingDetailClass> checkBookingDetailList = new List<CheckBookingDetailClass>();

        public SRH138(string _fsbooking_no)
        {
            InitializeComponent();

            _bookingNo = _fsbooking_no;

            //取得Master
            GetBookingMasterData();
            //取得Detail
            GetBookingDetailData();
        }


        private void GetBookingMasterData()
        {
            booking.GetBookingMasterDataCompleted += (s, args) =>
            {
                if (args.Error != null || string.IsNullOrEmpty(args.Result))
                {
                    return;
                }

                XDocument xmlDocResult = XDocument.Parse(args.Result.ToString());

                if (string.IsNullOrEmpty(xmlDocResult.Element("Datas").Element("Data").Element("FSBOOKING_NO").Value))
                {
                    return;
                }
                this.TextBlock_BookingNo.Text = xmlDocResult.Element("Datas").Element("Data").Element("FSBOOKING_NO").Value;
                this.TextBlock_UserId.Text = xmlDocResult.Element("Datas").Element("Data").Element("FSUSER_ID").Value;
                this.TextBlock_BookingDate.Text = xmlDocResult.Element("Datas").Element("Data").Element("FDBOOKING_DATE").Value;
                this.TextBlock_BookingReason.Text = xmlDocResult.Element("Datas").Element("Data").Element("FSREASON").Value;
                this.TextBlock_BookingReason_Desc.Text = xmlDocResult.Element("Datas").Element("Data").Element("FSBOOKING_REASON_DESC").Value;

                if (xmlDocResult.Element("Datas").Element("Data").Element("FCCHECK_STATUS").Value == "03")
                {
                    this.OKButton.Visibility = System.Windows.Visibility.Collapsed;
                }

            };

            booking.GetBookingMasterDataAsync("<Data><FSBOOKING_NO>" + _bookingNo + "</FSBOOKING_NO></Data>");
        }

        private void GetBookingDetailData()
        {
            booking.GetBookingDetailDataCompleted += (s, args) =>
            {
                if (args.Error != null || string.IsNullOrEmpty(args.Result))
                {
                    return;
                }

                XDocument xmlDocResult = XDocument.Parse(args.Result.ToString());
                for (int i = 0; i <= xmlDocResult.Descendants("Data").Count() - 1; i++)
                {
                    //MessageBox.Show(xmlDocResult.Descendants("Data").ElementAt(i).Element("FSSEQ").Value);
                    CheckBookingDetailClass c = new CheckBookingDetailClass();
                    c.FSBOOKING_NO = xmlDocResult.Descendants("Data").ElementAt(i).Element("FSBOOKING_NO").Value;
                    c.FSSEQ = xmlDocResult.Descendants("Data").ElementAt(i).Element("FSSEQ").Value;
                    c.FSFILE_NO = xmlDocResult.Descendants("Data").ElementAt(i).Element("FSFILE_NO").Value;
                    c.FSTITLE = xmlDocResult.Descendants("Data").ElementAt(i).Element("FSTITLE").Value;
                    c.FSBEG_TIMECODE = xmlDocResult.Descendants("Data").ElementAt(i).Element("FSBEG_TIMECODE").Value;
                    c.FSEND_TIMECODE = xmlDocResult.Descendants("Data").ElementAt(i).Element("FSEND_TIMECODE").Value;
                    c.FSFILE_CATEGORY = xmlDocResult.Descendants("Data").ElementAt(i).Element("FSFILE_CATEGORY").Value;
                    c.FCCHECK = xmlDocResult.Descendants("Data").ElementAt(i).Element("FCCHECK").Value;
                    c.FCCHECK_NAME = xmlDocResult.Descendants("Data").ElementAt(i).Element("FCCHECK_NAME").Value;
                    c.FSCHECK_ID = xmlDocResult.Descendants("Data").ElementAt(i).Element("FSCHECK_ID").Value;
                    c.FDCHECK_DATE = xmlDocResult.Descendants("Data").ElementAt(i).Element("FDCHECK_DATE").Value;
                    //c.FCOUT_BUY = xmlDocResult.Descendants("Data").ElementAt(i).Element("FCOUT_BUY").Value;
                    //c.FCCHECK_OUT_BUY = xmlDocResult.Descendants("Data").ElementAt(i).Element("FCCHECK_OUT_BUY").Value;
                    //c.FCCHECK_OUT_BUY_NAME = xmlDocResult.Descendants("Data").ElementAt(i).Element("FCCHECK_OUT_BUY_NAME").Value;
                    //c.FSCHECK_OUT_BUY_ID = xmlDocResult.Descendants("Data").ElementAt(i).Element("FSCHECK_OUT_BUY_ID").Value;
                    //c.FDCHECK_OUT_BUY_DATE = xmlDocResult.Descendants("Data").ElementAt(i).Element("FDCHECK_OUT_BUY_DATE").Value;
                    c.FSIMAGEPATH = GetFileCategoryImage(xmlDocResult.Descendants("Data").ElementAt(i).Element("FSFILE_CATEGORY").Value);
                    c.FSFILENO_CATEGORY = xmlDocResult.Descendants("Data").ElementAt(i).Element("FSFILE_NO").Value + ";" +
                                            xmlDocResult.Descendants("Data").ElementAt(i).Element("FSFILE_CATEGORY").Value + ";" +
                                            xmlDocResult.Descendants("Data").ElementAt(i).Element("FSSTART_TIMECODE").Value + ";" +
                                            xmlDocResult.Descendants("Data").ElementAt(i).Element("FSBEG_TIMECODE").Value + ";" +
                                            xmlDocResult.Descendants("Data").ElementAt(i).Element("FSGUID").Value;
                    c.FCFILE_STATUS = xmlDocResult.Descendants("Data").ElementAt(i).Element("FCFILE_STATUS").Value;
                    c.FCFILE_TRANSCODE_STATUS = xmlDocResult.Descendants("Data").ElementAt(i).Element("FCFILE_TRANSCODE_STATUS").Value;
                    c.FSFILE_OUTPUT_INFO = (GetFileOutputInfo(xmlDocResult.Descendants("Data").ElementAt(i).Element("FSFILE_OUTPUT_INFO").Value));
                    checkBookingDetailList.Add(c);
                }

                this.DataGrid_BookingDetail.SelectedIndex = -1;
                this.DataGrid_BookingDetail.ItemsSource = null;
                this.DataGrid_BookingDetail.ItemsSource = checkBookingDetailList;

            };

            booking.GetBookingDetailDataAsync("<Data><FSBOOKING_NO>" + _bookingNo + "</FSBOOKING_NO></Data>");
        }

        //取出轉出檔案格式
        private string GetFileOutputInfo(string profilePath)
        {
            if (string.IsNullOrEmpty(profilePath))
            {
                return "原始格式";
            }
            else
            {
                return profilePath.Substring(profilePath.LastIndexOf('\\') + 1, profilePath.Length - profilePath.LastIndexOf('\\') - 5);
            }
        }

        //依照不同類型，顯示不同圖片
        private string GetFileCategoryImage(string category)
        {
            if (category == "1")
            {
                return "../Images/video16_16.png";
            }
            else if (category == "2")
            {
                return "../Images/audio16_16.png";
            }
            else if (category == "3")
            {
                return "../Images/photo16_16.png";
            }
            else if (category == "4")
            {
                return "../Images/txt16_16.png";
            }
            else if (category == "5")
            {
                return "../Images/news16_16.png";
            }
            else
            {
                return "";
            }
        }

        private void PreViewContent(string category, string fileno)
        {
            this.RadBusyIndicator_Check.IsBusy = true;
            this.Grid_Show.Children.Clear();

            if (category == "1")
            {
                //影
                //取得路徑
                WSBookingFunctionSoapClient booking = new WSBookingFunctionSoapClient();
                booking.GetAsxFileCompleted += (s, args) =>
                {
                    if ((args.Error != null) || String.IsNullOrEmpty(args.Result))
                    {
                        this.RadBusyIndicator_Check.IsBusy = false;
                        return;
                    }

                    player.HTML_SetURL(args.Result, _begin_timecode);
                    this.Grid_Show.Children.Add(player);
                    this.RadBusyIndicator_Check.IsBusy = false;
                };
                booking.GetAsxFileAsync(_fsguid);
                //ServiceTSMSoapClient tsm = new ServiceTSMSoapClient();
                //tsm.GetHttpPathByFileIDCompleted += (s, args) =>
                //{
                //    if ((args.Error != null) || String.IsNullOrEmpty(args.Result))
                //    {
                //        MessageBox.Show("找不到檔案!");
                //        this.RadBusyIndicator_Check.IsBusy = false;
                //        return;
                //    }

                //    player.HTML_SetURL(args.Result, _start_timecode);
                //    //將Video加置Grid
                //    this.Grid_Show.Children.Add(player);
                //    player.HTML_SetFrame(_begin_timecode - _start_timecode > 0 ? _begin_timecode - _start_timecode : 0);
                //    this.RadBusyIndicator_Check.IsBusy = false;
                //};

                //tsm.GetHttpPathByFileIDAsync(fileno, FileID_MediaType.LowVideo);

            }
            else if (category == "2")
            {
                //音
                this.RadBusyIndicator_Check.IsBusy = false;
            }
            else if (category == "3")
            {
                //圖
                ServiceTSMSoapClient tsm_photo = new ServiceTSMSoapClient();
                tsm_photo.GetHttpPathByFileIDCompleted += (s, args) =>
                {
                    if ((args.Error != null) || String.IsNullOrEmpty(args.Result))
                    {
                        this.RadBusyIndicator_Check.IsBusy = false;
                        return;
                    }
                    else
                    {
                        Image img = new Image();
                        img.Width = 320;
                        img.Height = 320;
                        img.Source = new BitmapImage(new Uri(args.Result, UriKind.Absolute));
                        this.Grid_Show.Children.Add(img);
                        this.RadBusyIndicator_Check.IsBusy = false;
                    }
                };

                tsm_photo.GetHttpPathByFileIDAsync(fileno, FileID_MediaType.Photo);
            }
            else if (category == "4")
            {
                //文
                SearchDataBaseFunctionClient searchDb = new SearchDataBaseFunctionClient();
                searchDb.GetSearchDetailDataCompleted += (s, args) =>
                {
                    if ((args.Error != null) || String.IsNullOrEmpty(args.Result))
                    {
                        this.RadBusyIndicator_Check.IsBusy = false;
                        return;
                    }
                    else
                    {
                        XDocument xmlDocResult = XDocument.Parse(args.Result.Replace("", ""));
                        if (xmlDocResult.Element("Datas").Value != "")
                        {
                            TextBlock textblock = new TextBlock();
                            textblock.Text = xmlDocResult.Element("Datas").Element("Data").Element("FSCONTENT").Value;
                            textblock.FontSize = 12;
                            this.Grid_Show.Children.Add(textblock);
                            this.RadBusyIndicator_Check.IsBusy = false;
                        }
                    }
                };

                searchDb.GetSearchDetailDataAsync("<Data><FSVW_NAME>" + FileCategoryToType(category) + "</FSVW_NAME><FSSYS_ID>" + fileno + "</FSSYS_ID></Data>");
            }
            else if (category == "5")
            {
                //新聞
                WSBookingFunctionSoapClient booking = new WSBookingFunctionSoapClient();
                booking.GetNewsLVPathCompleted += (s, args) =>
                {
                    if ((args.Error != null) || String.IsNullOrEmpty(args.Result))
                    {
                        this.RadBusyIndicator_Check.IsBusy = false;
                        return;
                    }

                    //取得開始frame
                    booking.GetNewsStartFrameCompleted += (s1, args1) =>
                    {
                        if ((args1.Error != null) || String.IsNullOrEmpty(args1.Result))
                        {
                            this.RadBusyIndicator_Check.IsBusy = false;
                            return;
                        }

                        XDocument xDoc = XDocument.Parse(args1.Result);

                        player.HTML_SetURL(args.Result, long.Parse(xDoc.Element("Datas").Element("Data").Element("FSTIMECODE1").Value));
                        //將Video加置Grid
                        this.Grid_Show.Children.Add(player);
                        player.HTML_SetFrame(_begin_timecode - _start_timecode > 0 ? _begin_timecode - _start_timecode : 0);
                        this.RadBusyIndicator_Check.IsBusy = false;
                    };

                    booking.GetNewsStartFrameAsync("<Data><FSFILE_NO>" + fileno + "</FSFILE_NO><FNSEQ_NO>1</FNSEQ_NO></Data>");

                };

                booking.GetNewsLVPathAsync(fileno);
            }
        }

        private void Button_Image_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            _fsfileno = ((Button)sender).Tag.ToString().Split(';')[0];
            _fscategory = ((Button)sender).Tag.ToString().Split(';')[1];
            if (!long.TryParse(((Button)sender).Tag.ToString().Split(';')[2], out _start_timecode))
            {
                _start_timecode = 0;
            }
            if (((Button)sender).Tag.ToString().Split(';')[3] != "")
            {
                _begin_timecode = TransferTimecode.timecodetoframe(((Button)sender).Tag.ToString().Split(';')[3]);
            }
            else
            {
                _begin_timecode = 0;
            }
            _fsguid = ((Button)sender).Tag.ToString().Split(';')[4];
            PreViewContent(_fscategory, _fsfileno);
        }

        private string FileCategoryToType(string category)
        {
            if (category == "1")
            {
                return "VW_SEARCH_VIDEO";
            }
            else if (category == "2")
            {
                return "VW_SEARCH_AUDIO";
            }
            else if (category == "3")
            {
                return "VW_SEARCH_PHOTO";
            }
            else if (category == "4")
            {
                return "VW_SEARCH_DOC";
            }
            else if (category == "5")
            {
                return "VW_NEWS";
            }
            else
            {
                return "";
            }
        }

        private void HyperlinkButton_Memo_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            HyperlinkButton hyperlinkbutton = sender as HyperlinkButton;
            DataGridRow currentDataGridRow = DataGridRow.GetRowContainingElement(hyperlinkbutton);
            if (currentDataGridRow == null)
            {
                return;
            }
            string textblock_seq = (this.DataGrid_BookingDetail.Columns[2].GetCellContent(currentDataGridRow) as TextBlock).Text;

            SRH136_01 srh136_01 = new SRH136_01(_bookingNo, textblock_seq, false);
            srh136_01.Show();

        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

