﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SearchClassLibrary;
using PTS_MAM3.WSBookingFunction;
using System.Xml;
using System.Xml.Linq;
using System.Text;
using PTS_MAM3.WSTSM_GetFileInfo;
using System.Windows.Media.Imaging;
using PTS_MAM3.WCFSearchDataBaseFunction;

namespace PTS_MAM3.SRH
{
    public partial class SRH136 : ChildWindow
    {
        private string _bookingNo;
        private string _fsfileno;
        private string _fscategory;
        private long _start_timecode;
        private long _begin_timecode;
        private string _fsguid;
        private string _fsFILE_PATH = string.Empty;

        public int intProcessID;
        public int intFlowID;
        private string parameter = string.Empty;
        private WSBookingFunctionSoapClient booking = new WSBookingFunctionSoapClient();
        private ServiceTSMSoapClient tsm_photo = new ServiceTSMSoapClient();
        private SearchDataBaseFunctionClient searchDb = new SearchDataBaseFunctionClient();
        //DeltaFlow的用於簽核流程用
        private flowWebService.FlowSoapClient client = new flowWebService.FlowSoapClient();

        //建立Video物件
        PreviewPlayer.SLVideoPlayer player = new PreviewPlayer.SLVideoPlayer("", 0);
        StringBuilder sb = new StringBuilder();
        

        List<CheckBookingDetailClass> checkBookingDetailList = new List<CheckBookingDetailClass>();

        //直屬主管審核頁面

        public SRH136(string bookingNo)
        {
            InitializeComponent();
            this.RadBusyIndicator.IsBusy = true;
            Register();

            _bookingNo = bookingNo;

            //取得Master
            GetBookingMasterDataCheckBySelfSupervisor();
            
        }

        //註冊事件
        private void Register()
        {
            //FLOW
            client.WorkWithFieldCompleted += new EventHandler<flowWebService.WorkWithFieldCompletedEventArgs>(client_WorkWithFieldCompleted);
            
            //取得需審核項目(Booking Master)
            booking.GetBookingMasterDataCheckBySelfSupervisorCompleted += new EventHandler<GetBookingMasterDataCheckBySelfSupervisorCompletedEventArgs>(booking_GetBookingMasterDataCheckBySelfSupervisorCompleted);
            //取得需審核項目(Booking Detail)
            booking.GetBookingDetailDataCheckBySelfSupervisorCompleted += new EventHandler<GetBookingDetailDataCheckBySelfSupervisorCompletedEventArgs>(booking_GetBookingDetailDataCheckBySelfSupervisorCompleted);
            
            //更新直屬主管審核狀態
            booking.UpdateBookingStatusCompleted += new EventHandler<UpdateBookingStatusCompletedEventArgs>(booking_UpdateBookingStatusCompleted);
            //送轉檔
            booking.SendToTranscodeCompleted += new EventHandler<SendToTranscodeCompletedEventArgs>(booking_SendToTranscodeCompleted);


            //取得部分調用影片ASX
            booking.GetAsxFileCompleted += new EventHandler<GetAsxFileCompletedEventArgs>(booking_GetAsxFileCompleted);
            //取得圖片路徑
            tsm_photo.GetHttpPathByFileIDCompleted += new EventHandler<GetHttpPathByFileIDCompletedEventArgs>(tsm_photo_GetHttpPathByFileIDCompleted);

            //取得檢索詳細資料
            searchDb.GetSearchDetailDataCompleted += new EventHandler<GetSearchDetailDataCompletedEventArgs>(searchDb_GetSearchDetailDataCompleted);

            //取得新聞低解
            booking.GetNewsLVPathCompleted += new EventHandler<GetNewsLVPathCompletedEventArgs>(booking_GetNewsLVPathCompleted);
            //取得新聞開始Frame
            booking.GetNewsStartFrameCompleted += new EventHandler<GetNewsStartFrameCompletedEventArgs>(booking_GetNewsStartFrameCompleted);

            //取得要修改審核的成案單位主管
            booking.GetBookingSupervisorUpdateCompleted += new EventHandler<GetBookingSupervisorUpdateCompletedEventArgs>(booking_GetBookingSupervisorUpdateCompleted);
            client.UpdateFieldValueCompleted += new EventHandler<flowWebService.UpdateFieldValueCompletedEventArgs>(client_UpdateFieldValueCompleted);
        }

        

#region 完成事件

        //取得需審核項目(Booking Master)
        void booking_GetBookingMasterDataCheckBySelfSupervisorCompleted(object sender, GetBookingMasterDataCheckBySelfSupervisorCompletedEventArgs e)
        {
            if (e.Error != null || string.IsNullOrEmpty(e.Result))
            {
                MessageBox.Show("取得調用主表審核資料錯誤!" + e.Error.Message, "訊息", MessageBoxButton.OK);
            }
            else
            {
                XDocument xmlDocResult = XDocument.Parse(e.Result.ToString());
                if (string.IsNullOrEmpty(xmlDocResult.Element("Datas").Element("Data").Element("FSBOOKING_NO").Value))
                {
                    MessageBox.Show("調用主表審核資料XML錯誤!", "訊息", MessageBoxButton.OK);
                }
                else
                {
                    this.TextBlock_BookingNo.Text = xmlDocResult.Element("Datas").Element("Data").Element("FSBOOKING_NO").Value;
                    this.TextBlock_UserId.Text = xmlDocResult.Element("Datas").Element("Data").Element("FSUSER_ID").Value;
                    this.TextBlock_BookingDate.Text = xmlDocResult.Element("Datas").Element("Data").Element("FDBOOKING_DATE").Value;
                    this.TextBlock_BookingReason.Text = xmlDocResult.Element("Datas").Element("Data").Element("FSREASON").Value;
                    this.TextBlock_BookingReason_Desc.Text = xmlDocResult.Element("Datas").Element("Data").Element("FSBOOKING_REASON_DESC").Value;

                    if (xmlDocResult.Element("Datas").Element("Data").Element("FCCHECK_STATUS").Value == "03")
                    {
                        this.OKButton.Visibility = System.Windows.Visibility.Collapsed;
                    }

                    //取得Detail
                    GetBookingDetailDataCheckBySelfSupervisor();
                }
            }
        }

        //取得需審核項目(Booking Detail)
        void booking_GetBookingDetailDataCheckBySelfSupervisorCompleted(object sender, GetBookingDetailDataCheckBySelfSupervisorCompletedEventArgs e)
        {
            if (e.Error != null || string.IsNullOrEmpty(e.Result))
            {
                MessageBox.Show("取得調用明細表審核資料錯誤!" + e.Error.Message, "訊息", MessageBoxButton.OK);
            }
            else
            {
                XDocument xmlDocResult = XDocument.Parse(e.Result.ToString());
                for (int i = 0; i <= xmlDocResult.Descendants("Data").Count() - 1; i++)
                {
                    //MessageBox.Show(xmlDocResult.Descendants("Data").ElementAt(i).Element("FSSEQ").Value);
                    CheckBookingDetailClass c = new CheckBookingDetailClass();
                    c.FSBOOKING_NO = xmlDocResult.Descendants("Data").ElementAt(i).Element("FSBOOKING_NO").Value;
                    c.FSSEQ = xmlDocResult.Descendants("Data").ElementAt(i).Element("FSSEQ").Value;
                    c.FSFILE_NO = xmlDocResult.Descendants("Data").ElementAt(i).Element("FSFILE_NO").Value;
                    c.FSTITLE = xmlDocResult.Descendants("Data").ElementAt(i).Element("FSTITLE").Value;
                    c.FSBEG_TIMECODE = xmlDocResult.Descendants("Data").ElementAt(i).Element("FSBEG_TIMECODE").Value;
                    c.FSEND_TIMECODE = xmlDocResult.Descendants("Data").ElementAt(i).Element("FSEND_TIMECODE").Value;
                    c.FSFILE_CATEGORY = xmlDocResult.Descendants("Data").ElementAt(i).Element("FSFILE_CATEGORY").Value;
                    c.FCCHECK = xmlDocResult.Descendants("Data").ElementAt(i).Element("FCCHECK").Value;
                    c.FCCHECK_NAME = xmlDocResult.Descendants("Data").ElementAt(i).Element("FCCHECK_NAME").Value;
                    c.FSCHECK_ID = xmlDocResult.Descendants("Data").ElementAt(i).Element("FSCHECK_ID").Value;
                    c.FDCHECK_DATE = xmlDocResult.Descendants("Data").ElementAt(i).Element("FDCHECK_DATE").Value;
                    c.FSIMAGEPATH = GetFileCategoryImage(xmlDocResult.Descendants("Data").ElementAt(i).Element("FSFILE_CATEGORY").Value);
                    c.FSFILENO_CATEGORY = xmlDocResult.Descendants("Data").ElementAt(i).Element("FSFILE_NO").Value + ";" +
                                            xmlDocResult.Descendants("Data").ElementAt(i).Element("FSFILE_CATEGORY").Value + ";" +
                                            xmlDocResult.Descendants("Data").ElementAt(i).Element("FSSTART_TIMECODE").Value + ";" +
                                            xmlDocResult.Descendants("Data").ElementAt(i).Element("FSBEG_TIMECODE").Value + ";" +
                                            xmlDocResult.Descendants("Data").ElementAt(i).Element("FSGUID").Value;
                    c.FCFILE_STATUS = xmlDocResult.Descendants("Data").ElementAt(i).Element("FCFILE_STATUS").Value;
                    c.FCFILE_TRANSCODE_STATUS = xmlDocResult.Descendants("Data").ElementAt(i).Element("FCFILE_TRANSCODE_STATUS").Value;
                    c.FSFILE_OUTPUT_INFO = (GetFileOutputInfo(xmlDocResult.Descendants("Data").ElementAt(i).Element("FSFILE_OUTPUT_INFO").Value));
                    checkBookingDetailList.Add(c);
                }

                this.DataGrid_BookingDetail.SelectedIndex = -1;
                this.DataGrid_BookingDetail.ItemsSource = null;
                this.DataGrid_BookingDetail.ItemsSource = checkBookingDetailList;

            }

            if (checkBookingDetailList == null || checkBookingDetailList.Count == 0)
            {
                this.OKButton.Visibility = System.Windows.Visibility.Collapsed;
            }

            this.RadBusyIndicator.IsBusy = false;

        }


        //更新直屬主管審核狀態
        void booking_UpdateBookingStatusCompleted(object sender, UpdateBookingStatusCompletedEventArgs e)
        {
            if (e.Error != null || !e.Result)
            {
                MessageBox.Show("審核失敗!" + e.Error.Message, "訊息", MessageBoxButton.OK);
                this.RadBusyIndicator.IsBusy = false;
            }
            else
            {
                //送FLOW
                //查詢直屬主管審核檔案狀態，若已被拒絕，則不需要傳給該成案單位主管審核，需修改FLOW變數DeptSigner
                booking.GetBookingSupervisorUpdateAsync("<Data><FSBOOKING_NO>"+ _bookingNo +"</FSBOOKING_NO></Data>");
            }

        }

        //取得要修改的成案主管審核狀態
        void booking_GetBookingSupervisorUpdateCompleted(object sender, GetBookingSupervisorUpdateCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show("審核失敗!" + e.Error.Message, "訊息", MessageBoxButton.OK);
                this.RadBusyIndicator.IsBusy = false;
            }
            else
            {
                XDocument xmlDocResult = XDocument.Parse(e.Result.ToString());

                string DeptSigner = xmlDocResult.Descendants("Data").ElementAt(0).Element("FSSUPERVISOR").Value.Split(';')[0];
                string active_id = xmlDocResult.Descendants("Data").ElementAt(0).Element("FSSUPERVISOR").Value.Split(';')[1];
                client.UpdateFieldValueAsync(int.Parse(active_id), "<VariableCollection><variable><name>DeptSigner</name><value>" + DeptSigner + "</value></variable></VariableCollection>");
            }
        }

        void client_UpdateFieldValueCompleted(object sender, flowWebService.UpdateFieldValueCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show("審核失敗!" + e.Error.Message, "訊息", MessageBoxButton.OK);
                this.RadBusyIndicator.IsBusy = false;
            }
            else
            {
                sb.Append(@"<VariableCollection>");
                sb.Append(@"  <variable>");
                sb.Append(@"    <name>parameter01</name>");
                sb.Append(@"    <value>" + TransferTimecode.ReplaceXML(parameter) + "</value>");
                sb.Append(@"  </variable>");
                sb.Append(@"  <variable>");
                sb.Append(@"    <name>USERID</name>");
                sb.Append(@"    <value>" + UserClass.userData.FSUSER_ID + "</value>");
                sb.Append(@"  </variable>");
                sb.Append(@"  <variable>");
                sb.Append(@"    <name>NextXamlName</name>");
                sb.Append(@"    <value>/SRH/SRH137.xaml</value>");
                sb.Append(@"  </variable>");
                sb.Append(@"  <variable>");
                sb.Append(@"    <name>USERNAME</name>");
                sb.Append(@"    <value>" + UserClass.userData.FSUSER_ChtName + "</value>");
                sb.Append(@"  </variable>");
                sb.Append(@"  <variable>");
                sb.Append(@"    <name>FlowResult</name>");
                sb.Append(@"    <value>調用清單直屬主管審核完成!</value>");
                sb.Append(@"  </variable>");
                sb.Append(@"  <variable>");
                sb.Append(@"    <name>CancelForm</name>");
                sb.Append(@"    <value>1</value>");
                sb.Append(@"  </variable>");
                sb.Append(@"</VariableCollection>");//hid_SendTo

                booking.fnINSERT_FLOW_WITHFIELD_LOGAsync(8, "0", UserClass.userData.FSUSER_ID, "開始呼叫FLOW!");
                client.WorkWithFieldAsync(intProcessID.ToString(), sb.ToString());
            }
           
        }

        //送轉檔
        void booking_SendToTranscodeCompleted(object sender, SendToTranscodeCompletedEventArgs e)
        {
            this.RadBusyIndicator.IsBusy = false;
            if (e.Error != null)
            {
                MessageBox.Show("送轉檔失敗!" + e.Error.Message, "訊息", MessageBoxButton.OK);
            }
            else
            {
                MessageBox.Show("審核成功!", "訊息", MessageBoxButton.OK);
                this.DialogResult = true;
            }
        }


        //送FLOW
        void client_WorkWithFieldCompleted(object sender, flowWebService.WorkWithFieldCompletedEventArgs e)
        {
            //加入FLOW失敗不要執行此非同步,並顯示錯誤訊息
            booking.fnINSERT_FLOW_WITHFIELD_LOGAsync(8, e.Result, UserClass.userData.FSUSER_ID, sb.ToString());
            if (Convert.ToInt32(e.Result) > 0)
            {
                //booking.UpdateBookingStatusAsync(parameter, UserClass.userData.FSUSER_ID);
                //送轉檔
                booking.SendToTranscodeAsync(_bookingNo, UserClass.userData.FSUSER_ID);
            }
            else
            { 
                MessageBox.Show("流程呼叫有問,請通知管理者!","訊息",MessageBoxButton.OK); 
            }
        }

        //取的ASX路徑
        void booking_GetAsxFileCompleted(object sender, GetAsxFileCompletedEventArgs e)
        {
            
            if ((e.Error != null) || String.IsNullOrEmpty(e.Result))
            {
                MessageBox.Show("取得檔案預覽錯誤!" + e.Error.Message, "訊息", MessageBoxButton.OK);
            }
            else
            {
                player.HTML_SetURL(e.Result, _begin_timecode);
                this.Grid_Show.Children.Add(player);
            }
            this.RadBusyIndicator.IsBusy = false;
        }

        //取得圖片路徑
        void tsm_photo_GetHttpPathByFileIDCompleted(object sender, GetHttpPathByFileIDCompletedEventArgs e)
        {
            
            if ((e.Error != null) || String.IsNullOrEmpty(e.Result))
            {
                MessageBox.Show("取得圖片路徑錯誤!" + e.Error.Message, "訊息", MessageBoxButton.OK);
            }
            else
            {
                Image img = new Image();
                img.Width = 320;
                img.Height = 320;
                img.Source = new BitmapImage(new Uri(e.Result, UriKind.Absolute));
                this.Grid_Show.Children.Add(img);
            }
            this.RadBusyIndicator.IsBusy = false;
        }

        //取得詳細資料
        void searchDb_GetSearchDetailDataCompleted(object sender, GetSearchDetailDataCompletedEventArgs e)
        {
            if ((e.Error != null) || String.IsNullOrEmpty(e.Result))
            {
                MessageBox.Show("取得詳細資料錯誤!" + e.Error.Message, "訊息", MessageBoxButton.OK);
            }
            else
            {
                XDocument xmlDocResult = XDocument.Parse(e.Result.Replace("", ""));
                if (xmlDocResult.Element("Datas").Value != "")
                {
                    TextBlock textblock = new TextBlock();
                    textblock.Text = xmlDocResult.Element("Datas").Element("Data").Element("FSCONTENT").Value;
                    textblock.FontSize = 12;
                    this.Grid_Show.Children.Add(textblock);
                }
            }
            this.RadBusyIndicator.IsBusy = false;
        }

        //取得新聞低解
        void booking_GetNewsLVPathCompleted(object sender, GetNewsLVPathCompletedEventArgs e)
        {
            if ((e.Error != null) || String.IsNullOrEmpty(e.Result))
            {
                MessageBox.Show("取得新聞低解路徑錯誤!" + e.Error.Message, "訊息", MessageBoxButton.OK);
                this.RadBusyIndicator.IsBusy = false;
            }
            else
            {
                _fsFILE_PATH = e.Result;
                
                //取得開始frame
                booking.GetNewsStartFrameAsync("<Data><FSFILE_NO>" + _fsfileno + "</FSFILE_NO><FNSEQ_NO>1</FNSEQ_NO></Data>");
            }
        }

        //取得新聞開始Frmae
        void booking_GetNewsStartFrameCompleted(object sender, GetNewsStartFrameCompletedEventArgs e)
        {
            if ((e.Error != null) || String.IsNullOrEmpty(e.Result))
            {
                this.RadBusyIndicator.IsBusy = false;
                return;
            }
            else
            {
                XDocument xDoc = XDocument.Parse(e.Result);
                player.HTML_SetURL(_fsFILE_PATH, long.Parse(xDoc.Element("Datas").Element("Data").Element("FSTIMECODE1").Value));
                //將Video加置Grid
                this.Grid_Show.Children.Add(player);
                player.HTML_SetFrame(_begin_timecode - _start_timecode > 0 ? _begin_timecode - _start_timecode : 0);
            }
            this.RadBusyIndicator.IsBusy = false;
        }

#endregion


        private void GetBookingMasterDataCheckBySelfSupervisor()
        {
            booking.GetBookingMasterDataCheckBySelfSupervisorAsync("<Data><FSBOOKING_NO>" + _bookingNo + "</FSBOOKING_NO></Data>");
        }

        private void GetBookingDetailDataCheckBySelfSupervisor()
        {
            booking.GetBookingDetailDataCheckBySelfSupervisorAsync("<Data><FSBOOKING_NO>" + _bookingNo + "</FSBOOKING_NO></Data>");
        }

        //取出轉出檔案格式
        private string GetFileOutputInfo(string profilePath)
        {
            if (string.IsNullOrEmpty(profilePath))
            {
                return "原始格式";
            }
            else
            {
                return profilePath.Substring(profilePath.LastIndexOf('\\') + 1, profilePath.Length - profilePath.LastIndexOf('\\') - 5);
            }
        }

        //依照不同類型，顯示不同圖片
        private string GetFileCategoryImage(string category)
        {
            if (category == "1")
            {
                return "../Images/video16_16.png";
            }
            else if (category == "2")
            {
                return "../Images/audio16_16.png";
            }
            else if (category == "3")
            {
                return "../Images/photo16_16.png";
            }
            else if (category == "4")
            {
                return "../Images/txt16_16.png";
            }
            else if (category == "5")
            {
                return "../Images/news16_16.png";
            }
            else
            {
                return "";
            }
        }

        private void PreViewContent(string category)
        {
            this.RadBusyIndicator.IsBusy = true;
            this.Grid_Show.Children.Clear();

            if (category == "1")
            {
                //影
                booking.GetAsxFileAsync(_fsguid);
            }
            else if (category == "2")
            {
                //音
                this.RadBusyIndicator.IsBusy = false;
            }
            else if (category == "3")
            {
                //圖

                tsm_photo.GetHttpPathByFileIDAsync(_fsfileno, FileID_MediaType.Photo);
            }
            else if (category == "4")
            {
                //文

                searchDb.GetSearchDetailDataAsync("<Data><FSVW_NAME>" + FileCategoryToType(category) + "</FSVW_NAME><FSSYS_ID>" + _fsfileno + "</FSSYS_ID></Data>");
            }
            else if (category == "5")
            {
                //新聞
                booking.GetNewsLVPathAsync(_fsfileno);
            }
        }

        private void Button_Image_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            _fsfileno = ((Button)sender).Tag.ToString().Split(';')[0];
            _fscategory = ((Button)sender).Tag.ToString().Split(';')[1];
            if (!long.TryParse(((Button)sender).Tag.ToString().Split(';')[2], out _start_timecode))
            {
                _start_timecode = 0;
            }
            if (((Button)sender).Tag.ToString().Split(';')[3] != "")
            {
                _begin_timecode = TransferTimecode.timecodetoframe(((Button)sender).Tag.ToString().Split(';')[3]);
            }
            else
            {
                _begin_timecode = 0;
            }
            _fsguid = ((Button)sender).Tag.ToString().Split(';')[4];
            PreViewContent(_fscategory);
        }

        private string FileCategoryToType(string category)
        {
            if (category == "1")
            {
                return "VW_SEARCH_VIDEO";
            }
            else if (category == "2")
            {
                return "VW_SEARCH_AUDIO";
            }
            else if (category == "3")
            {
                return "VW_SEARCH_PHOTO";
            }
            else if (category == "4")
            {
                return "VW_SEARCH_DOC";
            }
            else if (category == "5")
            {
                return "VW_NEWS";
            }
            else
            {
                return "";
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {

            //若有未審則不能送出
            this.RadBusyIndicator.IsBusy = true;
            bool flag = false;
            if (checkBookingDetailList != null && checkBookingDetailList.Count() > 0)
            {
                flag = true;
                
                for (int i = 0; i <= checkBookingDetailList.Count() - 1; i++)
                {
                    if (checkBookingDetailList[i].FCCHECK == "R")
                    {
                        flag = false;
                        MessageBox.Show("尚未審核完畢!", "訊息", MessageBoxButton.OK);
                        this.RadBusyIndicator.IsBusy = false;
                        return;
                    }
                }


                if (flag && checkBookingDetailList != null && checkBookingDetailList.Count() > 0)
                {
                    parameter += "<Datas>";
                    for (int i = 0; i <= checkBookingDetailList.Count() - 1; i++)
                    {
                        //更新審核情況
                        parameter += "<Data>";
                        parameter += "<FSBOOKING_NO>" + this.TextBlock_BookingNo.Text + "</FSBOOKING_NO>";
                        parameter += "<FSSEQ>" + checkBookingDetailList[i].FSSEQ + "</FSSEQ>";
                        parameter += "<FCCHECK>" + checkBookingDetailList[i].FCCHECK + "</FCCHECK>";
                        parameter += "<FSCHECK_ID>" + UserClass.userData.FSUSER_ID + "</FSCHECK_ID>";
                        parameter += "<FDCHECK_DATE>" + DateTime.Now.ToString("yyyy/MM/dd") + "</FDCHECK_DATE>";
                        parameter += "</Data>";
                    }
                    parameter += "</Datas>";
                }

                booking.UpdateBookingStatusAsync(parameter, UserClass.userData.FSUSER_ID);
                
                
            }
            else
            {
                MessageBox.Show("無審核資料!", "訊息", MessageBoxButton.OK);
                this.RadBusyIndicator.IsBusy = false;
                return;
            }

        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.RadBusyIndicator.IsBusy = false;
            this.DialogResult = false;
        }

        private void DataGrid_BookingDetail_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            List<Check> CheckList = new List<Check>();
            CheckList.Add(new Check() { check_value = "Y", check_name = "通過" });
            CheckList.Add(new Check() { check_value = "N", check_name = "不通過" });
            CheckList.Add(new Check() { check_value = "R", check_name = "待審" });

            ((ComboBox)this.DataGrid_BookingDetail.Columns[0].GetCellContent(e.Row).FindName("ComboBox_Check")).ItemsSource = CheckList;

            string textblock_seq = (this.DataGrid_BookingDetail.Columns[3].GetCellContent(e.Row) as TextBlock).Text;
            string textblock_fileNo = (this.DataGrid_BookingDetail.Columns[4].GetCellContent(e.Row) as TextBlock).Text;


            if (checkBookingDetailList.ElementAt(e.Row.GetIndex()).FSFILE_NO.Contains(textblock_fileNo) &&
                checkBookingDetailList.ElementAt(e.Row.GetIndex()).FSSEQ.Contains(textblock_seq))
            {
                if (((ComboBox)this.DataGrid_BookingDetail.Columns[0].GetCellContent(e.Row).FindName("ComboBox_Check")) != null)
                {
                    ((ComboBox)this.DataGrid_BookingDetail.Columns[0].GetCellContent(e.Row).FindName("ComboBox_Check")).SelectedValue = checkBookingDetailList.ElementAt(e.Row.GetIndex()).FCCHECK;
                }
            }


        }

        private void ComboBox_Check_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            ComboBox combobox = sender as ComboBox;
            DataGridRow currentDataGridRow = DataGridRow.GetRowContainingElement(combobox);
            if (currentDataGridRow == null)
            {
                return;
            }
            if (combobox.SelectedValue == null)
            {
                return;
            }
            TextBlock textblock_seq = this.DataGrid_BookingDetail.Columns[3].GetCellContent(currentDataGridRow) as TextBlock;
            TextBlock textblock_fileNo = this.DataGrid_BookingDetail.Columns[4].GetCellContent(currentDataGridRow) as TextBlock;

            if (checkBookingDetailList != null && checkBookingDetailList.Count() > 0)
            {
                for (int i = 0; i <= checkBookingDetailList.Count() - 1; i++)
                {
                    if (checkBookingDetailList.ElementAt(i).FSSEQ == textblock_seq.Text)
                    {
                        checkBookingDetailList.ElementAt(i).FCCHECK = combobox.SelectedValue.ToString();
                        return;
                    }
                }
            }

        }

        private void HyperlinkButton_Memo_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            HyperlinkButton hyperlinkbutton = sender as HyperlinkButton;
            DataGridRow currentDataGridRow = DataGridRow.GetRowContainingElement(hyperlinkbutton);
            if (currentDataGridRow == null)
            {
                return;
            }
            string textblock_seq = (this.DataGrid_BookingDetail.Columns[3].GetCellContent(currentDataGridRow) as TextBlock).Text;

            SRH136_01 srh136_01 = new SRH136_01(_bookingNo, textblock_seq, true);
            srh136_01.Show();

        }

    }

    public class Check
    {
        public string check_name { set; get; }
        public string check_value { set; get; }
    }
}

