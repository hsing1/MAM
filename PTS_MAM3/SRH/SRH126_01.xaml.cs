﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SearchClassLibrary;
using PTS_MAM3.WSBookingFunction;
using System.Xml;
using System.Xml.Linq;
using System.Text;

namespace PTS_MAM3.SRH
{
    public partial class SRH126_01 : ChildWindow
    {

        private string _bookingNo = string.Empty;
        private string _fcFILE_CATEGROY = string.Empty;
        private string _fcINTERPLAY = string.Empty; //新聞調用到檔案伺服器 or 新聞自動化

        WSBookingFunctionSoapClient booking = new WSBookingFunctionSoapClient();
        flowWebService.FlowSoapClient FlowClinet = new flowWebService.FlowSoapClient();

        private List<string> _fsFileOutputCodecList = new List<string>();

        public SRH126_01(string bookingNo, string fcFILE_CATEGROY)
        {
            InitializeComponent();

            this._bookingNo = bookingNo;
            this._fcFILE_CATEGROY = fcFILE_CATEGROY;

            booking.UpdateBookingMasterFlowIDCompleted += new EventHandler<UpdateBookingMasterFlowIDCompletedEventArgs>(booking_UpdateBookingMasterFlowIDCompleted);
            FlowClinet.NewFlowWithFieldCompleted += new EventHandler<flowWebService.NewFlowWithFieldCompletedEventArgs>(FlowClinet_NewFlowWithFieldCompleted);


            booking.GetFileTranscodeInfoCompleted += (s, args) =>
            {
                List<FileTranscodeProfile> fileTransCodeProfileList = new List<FileTranscodeProfile>();
                FileTranscodeProfile p1 = new FileTranscodeProfile();
                p1.fsfile_name = "原生格式";
                p1.fsfile_path = "";
                fileTransCodeProfileList.Add(p1);
                
                if (args.Error != null)
                {
                    return;
                }

                if (!string.IsNullOrEmpty(args.Result) && this._fcFILE_CATEGROY != "5")
                {
                    string[] file = args.Result.Split(';');
                    for (int i = 0; i <= file.Count() - 1; i++)
                    {
                        FileTranscodeProfile p = new FileTranscodeProfile();
                        p.fsfile_path = file[i].Split('|')[0];
                        p.fsfile_name = file[i].Split('|')[1];
                        fileTransCodeProfileList.Add(p);
                    }
                }

                this.ListBox_FileOutputInfo.ItemsSource = null;
                this.ListBox_FileOutputInfo.ItemsSource = fileTransCodeProfileList;
            };

            booking.GetFileLogoInfoCompleted += (s, args) =>
            {
                if (args.Error != null)
                {
                    return;
                }
                if (args.Result == null)
                {
                    return;
                }


                List<FileLogoProfile> fileLogoProfileList = new List<FileLogoProfile>();
                FileLogoProfile p1 = new FileLogoProfile();
                p1.fsfile_path = "";
                p1.fsfile_name = "No Logo";
                fileLogoProfileList.Add(p1);

                if (this._fcFILE_CATEGROY != "5")
                {
                    string[] file = args.Result.Split(';');
                    for (int i = 0; i <= file.Count() - 1; i++)
                    {
                        FileLogoProfile p = new FileLogoProfile();
                        p.fsfile_path = file[i].Split('|')[0];
                        p.fsfile_name = file[i].Split('|')[1];
                        fileLogoProfileList.Add(p);
                    }
                }
                this.ComboBox_Logo.ItemsSource = fileLogoProfileList;
                this.ComboBox_Logo.SelectedIndex = 0;
            };


            booking.GetBookingReasonCompleted += (s, args) =>
            {
                if (args.Error != null)
                {
                    return;
                }
                if (args.Result == null)
                {
                    return;
                }

                XDocument xmlDocResult = XDocument.Parse(args.Result.ToString());

                var paras = from para in xmlDocResult.Descendants("Data")
                            select new BookingReason
                            {
                                fsno = para.Element("FSNO").Value,
                                fsreason = para.Element("FSREASON").Value
                            };

                this.ComboBox_Booking_Reason.ItemsSource = paras;
                this.ComboBox_Booking_Reason.SelectedIndex = 0;
            };

            booking.GetFileTranscodeInfoAsync();
            booking.GetFileLogoInfoAsync();
            booking.GetBookingReasonAsync("");
            //MessageBox.Show(bookingList.Count().ToString());

            booking.SendToTranscodeCompleted += new EventHandler<SendToTranscodeCompletedEventArgs>(booking_SendToTranscodeCompleted);
        }

        //呼叫流程
        void FlowClinet_NewFlowWithFieldCompleted(object sender, flowWebService.NewFlowWithFieldCompletedEventArgs e)
        {
            if (e.Error != null || string.IsNullOrEmpty(e.Result))
            {
                booking.fnINSERT_NEWFLOW_WITHFIELD_LOGAsync(8, "-1", UserClass.userData.FSUSER_ID,"呼叫流程失敗：" + e.Error + ";" + e.Result);
                this.RadBusyIndicator_Booking.IsBusy = false;
                MessageBox.Show("送審核失敗，請聯絡系統管理人員!");
            }
            else
            {
                //記錄流程編號
                int fnFLOW_ID = -1;
                if (!int.TryParse(e.Result, out fnFLOW_ID))
                {
                    booking.fnINSERT_NEWFLOW_WITHFIELD_LOGAsync(8, "-1", UserClass.userData.FSUSER_ID, "呼叫流程失敗：流程編號轉換失敗;【錯誤流程編號：" + e.Result + "】");
                    this.RadBusyIndicator_Booking.IsBusy = false;
                    MessageBox.Show("送審核失敗，請聯絡系統管理人員!");
                }
                else
                {
                    booking.UpdateBookingMasterFlowIDAsync(_bookingNo, int.Parse(e.Result), UserClass.userData.FSUSER_ID);
                }
            }
        }
        
        //更新流程編號至BookingMaster
        void booking_UpdateBookingMasterFlowIDCompleted(object sender, UpdateBookingMasterFlowIDCompletedEventArgs e)
        {
            if (e.Error != null || !e.Result)
            {
                MessageBox.Show("記錄流程編號錯誤!");
            }
            else
            {
                this.RadBusyIndicator_Booking.IsBusy = false;
                this.DialogResult = true;
            }
        }

        void booking_SendToTranscodeCompleted(object sender, SendToTranscodeCompletedEventArgs e)
        {
            this.RadBusyIndicator_Booking.IsBusy = false;
            this.DialogResult = true;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (_fsFileOutputCodecList == null || _fsFileOutputCodecList.Count == 0)
            {
                MessageBox.Show("至少選擇一種輸出格式!");
                return;
            }

            if (this._fcFILE_CATEGROY != "5")
            {
                MessageBox.Show("借調前請先確認節目版權，請先至節目管理系統查詢節目著作權!", "注意", MessageBoxButton.OK);
                this.RadBusyIndicator_Booking.IsBusy = true;
                SendBooking();
            }
            else
            {

                //取得新聞檔案位置
                SRH126_02 _srh126_02 = new SRH126_02();
                _srh126_02.Show();

                _srh126_02.Closed += (s, args) =>
                {
                    if (_srh126_02.DialogResult == true)
                    {
                        this._fcINTERPLAY = _srh126_02._fcINTERPLAY;
                        this.RadBusyIndicator_Booking.IsBusy = true;
                        SendBooking();
                    }
                };
            }
            

        }


        private void SendBooking()
        {
            //判斷是否有權限直接調用
            string fcIsbooking_check = "1";
            //需要給哪些主管審核
            string fsDeptSigner = "";
            
            //送出調用清單
            if (_bookingNo != "")
            {
                //取得審核主管
                booking.GetBookingSupervisorCompleted += (s, args) =>
                {
                    if (args.Error != null || string.IsNullOrEmpty(args.Result))
                    {
                        this.RadBusyIndicator_Booking.IsBusy = false;
                        return;
                    }

                    //todo
                    XDocument xmlDocResult = XDocument.Parse(args.Result);
                    fsDeptSigner = xmlDocResult.Element("Datas").Element("Data").Element("FSSUPERVISOR").Value;
                    if (fsDeptSigner != "")
                    {
                        string[] signer = fsDeptSigner.Split(',');
                        IEnumerable<string> signer_temp = signer.Distinct();
                        fsDeptSigner = "";
                        for (int i = 0; i <= signer_temp.Count() - 1; i++)
                        {
                            fsDeptSigner += signer_temp.ElementAt<string>(i) + ",";
                        }

                        fsDeptSigner = fsDeptSigner.Substring(0, fsDeptSigner.Length - 1);
                    }


                    booking.InsertBookingCompleted += (s1, args1) =>
                    {
                        if (args1.Error == null && args1.Result)
                        {
                            //若為調用新聞而且是新聞部使用者，則不需要審核
                            if (this._fcFILE_CATEGROY == "5" && UserClass.userData.FSDEPT.IndexOf("新聞部") > -1)
                            {
                                //直接送轉檔
                                booking.SendToTranscodeAsync(this._bookingNo, UserClass.userData.FSUSER_ID);
                            }
                            else
                            {
                                //如果是新聞，就不要去判斷原成案單位主管
                                if (this._fcFILE_CATEGROY == "5")
                                {
                                    newAFlow(fcIsbooking_check, string.Empty);
                                }
                                else
                                {
                                    newAFlow(fcIsbooking_check, fsDeptSigner);
                                }
                                
                            }
                        }
                    };

                    string profile = "";
                    if (_fsFileOutputCodecList != null && _fsFileOutputCodecList.Count > 0)
                    {
                        for (int i = 0; i <= _fsFileOutputCodecList.Count - 1; i++)
                        {
                            profile += _fsFileOutputCodecList[i] + ';';
                        }
                    }
                    profile = profile.Substring(0, profile.Length - 1);

                    _fsFileOutputCodecList.Clear();

                    string logo = "";
                    if (this.ComboBox_Logo.Items.Count() > 0)
                    {
                        logo = ((FileLogoProfile)this.ComboBox_Logo.SelectedItem).fsfile_path;
                    }
                    else
                    {
                        logo = "";
                    }

                    string parameter = "";
                    if (this._fcFILE_CATEGROY == "5")
                    {
                        if (UserClass.userData.FSDEPT.IndexOf("新聞部") > -1)
                        {
                            parameter += "<Data>";
                            parameter += "<FSBOOKING_NO>" + _bookingNo + "</FSBOOKING_NO>";
                            parameter += "<FSUSER_ID>" + UserClass.userData.FSUSER_ID + "</FSUSER_ID>";
                            parameter += "<FDBOOKING_DATE>" + DateTime.Now.ToString("yyyy/MM/dd") + "</FDBOOKING_DATE>";
                            parameter += "<FSFILE_OUTPUT_INFO>" + profile + "</FSFILE_OUTPUT_INFO>";
                            parameter += "<FSLOGO_INFO>" + logo + "</FSLOGO_INFO>";
                            parameter += "<FSBOOKING_REASON>" + ((BookingReason)this.ComboBox_Booking_Reason.SelectedItem).fsno + "</FSBOOKING_REASON>";
                            parameter += "<FSBOOKING_REASON_DESC>" + this.TextBox_Booking_Reason_Desc.Text + "</FSBOOKING_REASON_DESC>";
                            parameter += "<FCCHECK_STATUS>Y</FCCHECK_STATUS>";
                            parameter += "<FSCHECK_ID></FSCHECK_ID>";
                            parameter += "<FDCHECK_DATE></FDCHECK_DATE>";
                            parameter += "<FCNEED_CHECK_OUT_BUY></FCNEED_CHECK_OUT_BUY>";
                            parameter += "<FCNEED_CHECK_INTERNATION></FCNEED_CHECK_INTERNATION>";
                            parameter += "<FCFILE_TRANSCODE_STATUS>N</FCFILE_TRANSCODE_STATUS>";
                            parameter += "<FCBOOKING_TYPE>1</FCBOOKING_TYPE>";
                            parameter += "<FCINTERPLAY>" + this._fcINTERPLAY + "</FCINTERPLAY>";
                            parameter += "<FCFILE_CATEGORY>5</FCFILE_CATEGORY>";
                            parameter += "</Data>";
                        }
                        else
                        {
                            parameter += "<Data>";
                            parameter += "<FSBOOKING_NO>" + _bookingNo + "</FSBOOKING_NO>";
                            parameter += "<FSUSER_ID>" + UserClass.userData.FSUSER_ID + "</FSUSER_ID>";
                            parameter += "<FDBOOKING_DATE>" + DateTime.Now.ToString("yyyy/MM/dd") + "</FDBOOKING_DATE>";
                            parameter += "<FSFILE_OUTPUT_INFO>" + profile + "</FSFILE_OUTPUT_INFO>";
                            parameter += "<FSLOGO_INFO>" + logo + "</FSLOGO_INFO>";
                            parameter += "<FSBOOKING_REASON>" + ((BookingReason)this.ComboBox_Booking_Reason.SelectedItem).fsno + "</FSBOOKING_REASON>";
                            parameter += "<FSBOOKING_REASON_DESC>" + this.TextBox_Booking_Reason_Desc.Text + "</FSBOOKING_REASON_DESC>";
                            parameter += "<FCCHECK_STATUS>N</FCCHECK_STATUS>";
                            parameter += "<FSCHECK_ID></FSCHECK_ID>";
                            parameter += "<FDCHECK_DATE></FDCHECK_DATE>";
                            parameter += "<FCNEED_CHECK_OUT_BUY></FCNEED_CHECK_OUT_BUY>";
                            parameter += "<FCNEED_CHECK_INTERNATION></FCNEED_CHECK_INTERNATION>";
                            parameter += "<FCFILE_TRANSCODE_STATUS>N</FCFILE_TRANSCODE_STATUS>";
                            parameter += "<FCBOOKING_TYPE>1</FCBOOKING_TYPE>";
                            parameter += "<FCINTERPLAY>" + this._fcINTERPLAY + "</FCINTERPLAY>";
                            parameter += "<FCFILE_CATEGORY>5</FCFILE_CATEGORY>";
                            parameter += "</Data>";
                        }
                        
                    }
                    else
                    {
                        parameter += "<Data>";
                        parameter += "<FSBOOKING_NO>" + _bookingNo + "</FSBOOKING_NO>";
                        parameter += "<FSUSER_ID>" + UserClass.userData.FSUSER_ID + "</FSUSER_ID>";
                        parameter += "<FDBOOKING_DATE>" + DateTime.Now.ToString("yyyy/MM/dd") + "</FDBOOKING_DATE>";
                        parameter += "<FSFILE_OUTPUT_INFO>" + profile + "</FSFILE_OUTPUT_INFO>";
                        parameter += "<FSLOGO_INFO>" + logo + "</FSLOGO_INFO>";
                        parameter += "<FSBOOKING_REASON>" + ((BookingReason)this.ComboBox_Booking_Reason.SelectedItem).fsno + "</FSBOOKING_REASON>";
                        parameter += "<FSBOOKING_REASON_DESC>" + this.TextBox_Booking_Reason_Desc.Text + "</FSBOOKING_REASON_DESC>";
                        parameter += "<FCCHECK_STATUS>N</FCCHECK_STATUS>";
                        parameter += "<FSCHECK_ID></FSCHECK_ID>";
                        parameter += "<FDCHECK_DATE></FDCHECK_DATE>";
                        parameter += "<FCNEED_CHECK_OUT_BUY></FCNEED_CHECK_OUT_BUY>";
                        parameter += "<FCNEED_CHECK_INTERNATION></FCNEED_CHECK_INTERNATION>";
                        parameter += "<FCFILE_TRANSCODE_STATUS>N</FCFILE_TRANSCODE_STATUS>";
                        parameter += "<FCBOOKING_TYPE>1</FCBOOKING_TYPE>";
                        parameter += "<FCINTERPLAY>" + this._fcINTERPLAY + "</FCINTERPLAY>";
                        parameter += "<FCFILE_CATEGORY>0</FCFILE_CATEGORY>";
                        parameter += "</Data>";
                    }
                    booking.InsertBookingAsync(parameter, UserClass.userData.FSUSER_ID);


                };

                booking.GetBookingSupervisorAsync("<Data><FSUSER_ID>" + UserClass.userData.FSUSER_ID + "</FSUSER_ID></Data>");
            }
        }


        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.RadBusyIndicator_Booking.IsBusy = false;
            this.DialogResult = false;
        }


        private void newAFlow(string fcbooking_check, string fsDeptSigner)
        {
            StringBuilder sb = new StringBuilder();
            
            sb.Append(@"<VariableCollection>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>FormId</name>");
            sb.Append(@"    <value>" + _bookingNo + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>NextXamlName</name>");
            sb.Append(@"    <value>/SRH/SRH136.xaml</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>hid_SendTo</name>");
            sb.Append(@"    <value>" + fcbooking_check + "</value>");
            sb.Append(@"  </variable>");
            //sb.Append(@"  <variable>");
            //sb.Append(@"    <name>Function01</name>");
            //sb.Append(@"    <value>0</value>");
            //sb.Append(@"  </variable>");
            //sb.Append(@"  <variable>");
            //sb.Append(@"    <name>SV_IsOutBuy</name>");
            //sb.Append(@"    <value>" + fcout_buy + "</value>");
            //sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>FormInfo</name>");
            sb.Append(@"    <value>調用清單：" + _bookingNo + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>USERID</name>");
            sb.Append(@"    <value>" + UserClass.userData.FSUSER_ID + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>CancelForm</name>");
            sb.Append(@"    <value>CanCancel</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>DeptSigner</name>");
            sb.Append(@"    <value>" + fsDeptSigner + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"</VariableCollection>");//hid_SendTo

            
            FlowClinet.NewFlowWithFieldAsync(8, UserClass.userData.FSUSER_ID, sb.ToString());
        }

        private void ListBox_FileOutputInfo_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            if (this.ListBox_FileOutputInfo.SelectedItem != null)
            {
                WSBookingFunctionSoapClient booking = new WSBookingFunctionSoapClient();
                booking.GetFileTranscodeInfoTxtCompleted += (s, args) =>
                {
                    if (args.Error != null || string.IsNullOrEmpty(args.Result))
                    {
                        this.TextBox_FileProfile_Desc.Text = "";
                        return;
                    }
                    this.TextBox_FileProfile_Desc.Text = args.Result;
                };

                booking.GetFileTranscodeInfoTxtAsync(((FileTranscodeProfile)this.ListBox_FileOutputInfo.SelectedItem).fsfile_path);
            }
        }

        //private void CheckBox_FileInfo_Checked(object sender, System.Windows.RoutedEventArgs e)
        //{
        //    // TODO: 在此新增事件處理常式執行項目。
        //    _fsFileOutputCodecList.Add((sender as CheckBox).Tag.ToString());
        //}

        //private void CheckBox_FileInfo_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        //{
        //    // TODO: 在此新增事件處理常式執行項目。
        //    if (_fsFileOutputCodecList != null && _fsFileOutputCodecList.Count > 0)
        //    {
        //        for (int i = 0; i <= _fsFileOutputCodecList.Count - 1; i++)
        //        {
        //            if (_fsFileOutputCodecList[i] == (sender as CheckBox).Tag.ToString())
        //            {
        //                _fsFileOutputCodecList.RemoveAt(i);
        //            }
        //        }
        //    }
        //}

        private void CheckBox_FileInfo_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
            if (sender != null)
            {
                if ((sender as CheckBox).IsChecked == true)
                {
                    if (_fsFileOutputCodecList.FirstOrDefault(item => item == (sender as CheckBox).Tag.ToString()) == null)
                        _fsFileOutputCodecList.Add((sender as CheckBox).Tag.ToString());
                }
                else
                {
                    _fsFileOutputCodecList.Remove(_fsFileOutputCodecList.FirstOrDefault(item => item == (sender as CheckBox).Tag.ToString()));
                }
            }
        }




    }

    public class FileTranscodeProfile
    {
        public string fsfile_path { set; get; }
        public string fsfile_name { set; get; }
    }

    public class FileLogoProfile
    {
        public string fsfile_path { set; get; }
        public string fsfile_name { set; get; }
    }

    public class BookingReason
    {
        public string fsno { set; get; }
        public string fsreason { set; get; }
    }
}

