﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Linq;
using PTS_MAM3.WCFSearchDataBaseFunction;
using PTS_MAM3.WSSearchService;

namespace AllSearch
{
    public partial class HotHit : UserControl
    {

        private PTS_MAM3.MainFrame parentFrame;

        public HotHit(PTS_MAM3.MainFrame mainframe)
        {
            InitializeComponent();

            parentFrame = mainframe;

            //顯示前20名熱門點閱
            try
            {
                SearchDataBaseFunctionClient searchDbWeek = new SearchDataBaseFunctionClient();
                SearchDataBaseFunctionClient searchDbMonth = new SearchDataBaseFunctionClient();
                SearchDataBaseFunctionClient searchDbYear = new SearchDataBaseFunctionClient();

                //近一週

                searchDbWeek.GetHotHitCompleted += (s, args) =>
                {
                    this.RadBusyIndicator_HotHit1.IsBusy = true;
                    if (args.Error != null)
                    {
                        this.RadBusyIndicator_HotHit1.IsBusy = false;
                        return;
                    }

                    if (args.Result == "")
                    {
                        this.RadBusyIndicator_HotHit1.IsBusy = false;
                        return;
                    }

                    XDocument xmlDocResult = XDocument.Parse(args.Result.ToString());

                    var paras = from para in xmlDocResult.Descendants("Data")
                                select new SearchHotHitClass
                                {
                                    fsSys_Id = para.Element("FSSYS_ID").Value,
                                    fsTitle = para.Element("FSTITLE").Value,
                                    fsIndexType = para.Element("FSINDEX_TYPE").Value,
                                    fnHothitCount = Int32.Parse(para.Element("FNNO_COUNT").Value),
                                };
                    int count1 = 0;

                    foreach (SearchHotHitClass obj in paras)
                    {
                        TextBlock txtBlock = new TextBlock();
                        txtBlock.Height = 10;
                        this.StackPanel_Week.Children.Add(txtBlock);

                        count1++;
                        HyperlinkButton linkButton = new HyperlinkButton();
                        linkButton.Content = count1.ToString() + ". " + obj.fsTitle + " (" + obj.fnHothitCount.ToString() + ")";
                        linkButton.Tag = obj.fsIndexType + "," + obj.fsSys_Id;
                        linkButton.FontSize = 14;
                        SolidColorBrush b = new SolidColorBrush();
                        b.Color = Colors.Black;
                        linkButton.Foreground = b;
                        linkButton.Click += (s1, args1) =>
                        {
                            //MessageBox.Show(((HyperlinkButton)s1).Tag.ToString());
                            string indexName = "";
                            if (((HyperlinkButton)s1).Tag.ToString().Split(',')[0] == "VW_NEWS")
                            {
                                indexName = "news_2000,news_2001,news_2006,news_2011,news_2016";
                            }
                            else if (((HyperlinkButton)s1).Tag.ToString().Split(',')[0] == "VW_SEARCH_AUDIO")
                            {
                                indexName = "audio_2000,audio_2001,audio_2006,audio_2011,audio_2016";
                            }
                            else if (((HyperlinkButton)s1).Tag.ToString().Split(',')[0] == "VW_SEARCH_PHOTO")
                            {
                                indexName = "photo_2000,photo_2001,photo_2006,photo_2011,photo_2016";
                            }
                            else if (((HyperlinkButton)s1).Tag.ToString().Split(',')[0] == "VW_SEARCH_DOC")
                            {
                                indexName = "doc_2000,doc_2001,doc_2006,doc_2011,doc_2016";
                            }
                            else if (((HyperlinkButton)s1).Tag.ToString().Split(',')[0] == "VW_SEARCH_VIDEO")
                            {
                                indexName = "video_2000,video_2001,video_2006,video_2011,video_2016";
                            }

                            string parameter = "";
                            parameter += "<Search><IniPath></IniPath>";
                            parameter += "<KeyWord></KeyWord><IndexName>"+indexName+"</IndexName><Date></Date>";
                            parameter += "<Channel></Channel><Columns>FSSYS_ID</Columns><Values>" + ((HyperlinkButton)s1).Tag.ToString().Split(',')[1] + "</Values><Logics></Logics><SearchMode>0</SearchMode><Homophone>0</Homophone>";
                            parameter += "<Synonym>0</Synonym><PageSize>10</PageSize><StartPoint>0</StartPoint><GetColumns>FSSYS_ID;FSFILE_NO;FSTITLE;FSCONTENT;FDCREATED_DATE;FSVWNAME;FCFROM;FSPROG_B_PGMNAME;FNEPISODE;FSTYPE_NAME;FSPROG_D_PGDNAME</GetColumns><TreeNode>/</TreeNode><OrderBy></OrderBy><OrderType></OrderType></Search>";

                            parentFrame.generateNewPane("檢索結果", "SEARCHRESULT", parameter,true);
                        };

                        this.StackPanel_Week.Children.Add(linkButton);

                    }

                    this.RadBusyIndicator_HotHit1.IsBusy = false;
                };


                //近一個月
                searchDbMonth.GetHotHitCompleted += (s, args) =>
                {
                    this.RadBusyIndicator_HotHit2.IsBusy = true;
                    if (args.Error != null)
                    {
                        this.RadBusyIndicator_HotHit2.IsBusy = false;
                        return;
                    }

                    if (args.Result == "")
                    {
                        this.RadBusyIndicator_HotHit2.IsBusy = false;
                        return;
                    }

                    XDocument xmlDocResult = XDocument.Parse(args.Result.ToString());

                    var paras = from para in xmlDocResult.Descendants("Data")
                                select new SearchHotHitClass
                                {
                                    fsSys_Id = para.Element("FSSYS_ID").Value,
                                    fsTitle = para.Element("FSTITLE").Value,
                                    fsIndexType = para.Element("FSINDEX_TYPE").Value,
                                    fnHothitCount = Int32.Parse(para.Element("FNNO_COUNT").Value),
                                };
                    int count2 = 0;

                    foreach (SearchHotHitClass obj in paras)
                    {
                        TextBlock txtBlock = new TextBlock();
                        txtBlock.Height = 10;
                        this.StackPanel_Month.Children.Add(txtBlock);

                        count2++;
                        HyperlinkButton linkButton = new HyperlinkButton();
                        linkButton.Content = count2.ToString() + ". " + obj.fsTitle + " (" + obj.fnHothitCount.ToString() + ")";
                        linkButton.Tag = obj.fsIndexType + "," + obj.fsSys_Id;
                        linkButton.FontSize = 14;
                        SolidColorBrush b = new SolidColorBrush();
                        b.Color = Colors.Black;
                        linkButton.Foreground = b;
                        linkButton.Click += (s1, args1) =>
                        {
                            //MessageBox.Show(((HyperlinkButton)s1).Tag.ToString());
                            string indexName = "";
                            if (((HyperlinkButton)s1).Tag.ToString().Split(',')[0] == "VW_NEWS")
                            {
                                indexName = "news_2000,news_2001,news_2006,news_2011,news_2016";
                            }
                            else if (((HyperlinkButton)s1).Tag.ToString().Split(',')[0] == "VW_SEARCH_AUDIO")
                            {
                                indexName = "audio_2000,audio_2001,audio_2006,audio_2011,audio_2016";
                            }
                            else if (((HyperlinkButton)s1).Tag.ToString().Split(',')[0] == "VW_SEARCH_PHOTO")
                            {
                                indexName = "photo_2000,photo_2001,photo_2006,photo_2011,photo_2016";
                            }
                            else if (((HyperlinkButton)s1).Tag.ToString().Split(',')[0] == "VW_SEARCH_DOC")
                            {
                                indexName = "doc_2000,doc_2001,doc_2006,doc_2011,doc_2016";
                            }
                            else if (((HyperlinkButton)s1).Tag.ToString().Split(',')[0] == "VW_SEARCH_VIDEO")
                            {
                                indexName = "video_2000,video_2001,video_2006,video_2011,video_2016";
                            }

                            string parameter = "";
                            parameter += "<Search><IniPath></IniPath>";
                            parameter += "<KeyWord></KeyWord><IndexName>" + indexName + "</IndexName><Date></Date>";
                            parameter += "<Channel></Channel><Columns>FSSYS_ID</Columns><Values>" + ((HyperlinkButton)s1).Tag.ToString().Split(',')[1] + "</Values><Logics></Logics><SearchMode>0</SearchMode><Homophone>0</Homophone>";
                            parameter += "<Synonym>0</Synonym><PageSize>10</PageSize><StartPoint>0</StartPoint><GetColumns>FSSYS_ID;FSFILE_NO;FSTITLE;FSCONTENT;FDCREATED_DATE;FSVWNAME;FCFROM;FSPROG_B_PGMNAME;FNEPISODE;FSTYPE_NAME;FSPROG_D_PGDNAME</GetColumns><TreeNode>/</TreeNode><OrderBy></OrderBy><OrderType></OrderType></Search>";

                            parentFrame.generateNewPane("檢索結果", "SEARCHRESULT", parameter, true);
                        };

                        this.StackPanel_Month.Children.Add(linkButton);

                    }

                    this.RadBusyIndicator_HotHit2.IsBusy = false;
                };

                //近一年
                searchDbMonth.GetHotHitCompleted += (s, args) =>
                {
                    this.RadBusyIndicator_HotHit3.IsBusy = true;
                    if (args.Error != null)
                    {
                        this.RadBusyIndicator_HotHit3.IsBusy = false;
                        return;
                    }

                    if (args.Result == "")
                    {
                        this.RadBusyIndicator_HotHit3.IsBusy = false;
                        return;
                    }

                    XDocument xmlDocResult = XDocument.Parse(args.Result.ToString());

                    var paras = from para in xmlDocResult.Descendants("Data")
                                select new SearchHotHitClass
                                {
                                    fsSys_Id = para.Element("FSSYS_ID").Value,
                                    fsTitle = para.Element("FSTITLE").Value,
                                    fsIndexType = para.Element("FSINDEX_TYPE").Value,
                                    fnHothitCount = Int32.Parse(para.Element("FNNO_COUNT").Value),
                                };
                    int count3 = 0;

                    foreach (SearchHotHitClass obj in paras)
                    {
                        TextBlock txtBlock = new TextBlock();
                        txtBlock.Height = 10;
                        this.StackPanel_Year.Children.Add(txtBlock);

                        count3++;
                        HyperlinkButton linkButton = new HyperlinkButton();
                        linkButton.Content = count3.ToString() + ". " + obj.fsTitle + " (" + obj.fnHothitCount.ToString() + ")";
                        linkButton.Tag = obj.fsIndexType + "," + obj.fsSys_Id;
                        linkButton.FontSize = 14;
                        SolidColorBrush b = new SolidColorBrush();
                        b.Color = Colors.Black;
                        linkButton.Foreground = b;

                        linkButton.Click += (s1, args1) =>
                        {
                            //MessageBox.Show(((HyperlinkButton)s1).Tag.ToString());
                            string indexName = "";
                            if (((HyperlinkButton)s1).Tag.ToString().Split(',')[0] == "VW_NEWS")
                            {
                                indexName = "news_2000,news_2001,news_2006,news_2011,news_2016";
                            }
                            else if (((HyperlinkButton)s1).Tag.ToString().Split(',')[0] == "VW_SEARCH_AUDIO")
                            {
                                indexName = "audio_2000,audio_2001,audio_2006,audio_2011,audio_2016";
                            }
                            else if (((HyperlinkButton)s1).Tag.ToString().Split(',')[0] == "VW_SEARCH_PHOTO")
                            {
                                indexName = "photo_2000,photo_2001,photo_2006,photo_2011,photo_2016";
                            }
                            else if (((HyperlinkButton)s1).Tag.ToString().Split(',')[0] == "VW_SEARCH_DOC")
                            {
                                indexName = "doc_2000,doc_2001,doc_2006,doc_2011,doc_2016";
                            }
                            else if (((HyperlinkButton)s1).Tag.ToString().Split(',')[0] == "VW_SEARCH_VIDEO")
                            {
                                indexName = "video_2000,video_2001,video_2006,video_2011,video_2016";
                            }

                            string parameter = "";
                            parameter += "<Search><IniPath></IniPath>";
                            parameter += "<KeyWord></KeyWord><IndexName>" + indexName + "</IndexName><Date></Date>";
                            parameter += "<Channel></Channel><Columns>FSSYS_ID</Columns><Values>" + ((HyperlinkButton)s1).Tag.ToString().Split(',')[1] + "</Values><Logics></Logics><SearchMode>0</SearchMode><Homophone>0</Homophone>";
                            parameter += "<Synonym>0</Synonym><PageSize>10</PageSize><StartPoint>0</StartPoint><GetColumns>FSSYS_ID;FSFILE_NO;FSTITLE;FSCONTENT;FDCREATED_DATE;FSVWNAME;FCFROM;FSPROG_B_PGMNAME;FNEPISODE;FSTYPE_NAME;FSPROG_D_PGDNAME</GetColumns><TreeNode>/</TreeNode><OrderBy></OrderBy><OrderType></OrderType></Search>";

                            parentFrame.generateNewPane("檢索結果", "SEARCHRESULT", parameter,true);
                        };

                        this.StackPanel_Year.Children.Add(linkButton);

                    }
                    this.RadBusyIndicator_HotHit3.IsBusy = false;
                };


                searchDbWeek.GetHotHitAsync("<Data><FDDATE_BEGIN>" + DateTime.Now.AddDays(-7).ToString("yyyy/MM/dd") + "</FDDATE_BEGIN>" +
                        "<FDDATE_END>" + DateTime.Now.ToString("yyyy/MM/dd") + "</FDDATE_END></Data>");
                searchDbMonth.GetHotHitAsync("<Data><FDDATE_BEGIN>" + DateTime.Now.AddMonths(-1).ToString("yyyy/MM/dd") + "</FDDATE_BEGIN>" +
                        "<FDDATE_END>" + DateTime.Now.ToString("yyyy/MM/dd") + "</FDDATE_END></Data>");
                searchDbYear.GetHotHitAsync("<Data><FDDATE_BEGIN>" + DateTime.Now.AddYears(-1).ToString("yyyy/MM/dd") + "</FDDATE_BEGIN>" +
                        "<FDDATE_END>" + DateTime.Now.ToString("yyyy/MM/dd") + "</FDDATE_END></Data>");

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
            }
        }
    }

    public class SearchHotHitClass
    {
        public string fsSys_Id { get; set; }
        public string fsTitle { get; set; }
        public string fsIndexType { get; set; }
        public int fnHothitCount { get; set; }
    }
}
