﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Linq;
using PTS_MAM3.WCFSearchDataBaseFunction;
using PTS_MAM3.WSSearchService;

namespace AllSearch
{
    public partial class HotKeyWord : UserControl
    {
        private PTS_MAM3.MainFrame parentFrame;
        


        public HotKeyWord(PTS_MAM3.MainFrame mainframe)
        {
            InitializeComponent();

            parentFrame = mainframe;


            string index = "";
            index += "video_2000,video_2001,video_2006,video_2011,video_2016,";
            index += "doc_2000,doc_2001,doc_2006,doc_2011,doc_2016,";
            index += "photo_2000,photo_2001,photo_2006,photo_2011,photo_2016,";
            index += "audio_2000,audio_2001,audio_2006,audio_2011,audio_2016,";
            index += "news_2000,news_2001,news_2006,news_2011,news_2016";
            
            //顯示前20名關鍵字
            try
            {
                
                SearchDataBaseFunctionClient searchDbWeek = new SearchDataBaseFunctionClient();
                SearchDataBaseFunctionClient searchDbMonth = new SearchDataBaseFunctionClient();
                SearchDataBaseFunctionClient searchDbYear = new SearchDataBaseFunctionClient();
                //近一週

                
                searchDbWeek.GetHotKeyWordCompleted += (s, args) =>
                {
                    this.RadBusyIndicator_HotKeyWord1.IsBusy = true;
                    if (args.Error != null)
                    {
                        this.RadBusyIndicator_HotKeyWord1.IsBusy = false;
                        return;
                    }

                    if (args.Result == "")
                    {
                        this.RadBusyIndicator_HotKeyWord1.IsBusy = false;
                        return;
                    }

                    XDocument xmlDocResult = XDocument.Parse(args.Result.ToString());

                    var paras = from para in xmlDocResult.Descendants("Data")
                                select new SearchHotKeyWordClass
                                {
                                    fsKeyWord = para.Element("FSKEYWORD").Value,
                                    fsKeyWordCount = Int32.Parse(para.Element("FNKEYWORD_COUNT").Value),
                                };
                    int count1 = 0;

                    foreach (SearchHotKeyWordClass obj in paras)
                    {
                        TextBlock txtBlock = new TextBlock();
                        txtBlock.Height = 10;
                        this.StackPanel_Week.Children.Add(txtBlock);

                        count1++;
                        HyperlinkButton linkButton = new HyperlinkButton();
                        linkButton.Content = count1.ToString() + ". " + obj.fsKeyWord + " (" + obj.fsKeyWordCount.ToString() + ")";
                        linkButton.Tag = obj.fsKeyWord;
                        linkButton.FontSize = 14;
                        SolidColorBrush b = new SolidColorBrush();
                        b.Color = Colors.Black;
                        linkButton.Foreground = b;
                        linkButton.Click += (s1, args1) =>
                        {
                            //MessageBox.Show(((HyperlinkButton)s1).Tag.ToString());
                            string parameter = "";
                            parameter += "<Search><IniPath></IniPath>";
                            parameter += "<KeyWord>" + ((HyperlinkButton)s1).Tag.ToString() + "</KeyWord><IndexName>" + index + "</IndexName><Date></Date>";
                            parameter += "<Channel></Channel><Columns></Columns><Values></Values><Logics></Logics><SearchMode>0</SearchMode><Homophone>0</Homophone>";
                            parameter += "<Synonym>0</Synonym><PageSize>10</PageSize><StartPoint>0</StartPoint><GetColumns>FSSYS_ID;FSFILE_NO;FSTITLE;FSCONTENT;FDCREATED_DATE;FSVWNAME;FCFROM;FSPROG_B_PGMNAME;FNEPISODE;FSTYPE_NAME;FSPROG_D_PGDNAME</GetColumns><TreeNode>/</TreeNode><OrderBy></OrderBy><OrderType></OrderType></Search>";
                        
                            parentFrame.generateNewPane("檢索結果", "SEARCHRESULT", parameter,true);
                        };

                        this.StackPanel_Week.Children.Add(linkButton);

                    }

                    this.RadBusyIndicator_HotKeyWord1.IsBusy = false;
                };

                //近一個月
                searchDbMonth.GetHotKeyWordCompleted += (s, args) =>
                {
                    this.RadBusyIndicator_HotKeyWord2.IsBusy = true;
                    if (args.Error != null)
                    {
                        this.RadBusyIndicator_HotKeyWord2.IsBusy = false;
                        return;
                    }

                    if (args.Result == "")
                    {
                        this.RadBusyIndicator_HotKeyWord2.IsBusy = false;
                        return;
                    }

                    XDocument xmlDocResult = XDocument.Parse(args.Result.ToString());

                    var paras = from para in xmlDocResult.Descendants("Data")
                                select new SearchHotKeyWordClass
                                {
                                    fsKeyWord = para.Element("FSKEYWORD").Value,
                                    fsKeyWordCount = Int32.Parse(para.Element("FNKEYWORD_COUNT").Value),
                                };
                    int count2 = 0;

                    foreach (SearchHotKeyWordClass obj in paras)
                    {
                        TextBlock txtBlock = new TextBlock();
                        txtBlock.Height = 10;
                        this.StackPanel_Month.Children.Add(txtBlock);

                        count2++;
                        HyperlinkButton linkButton = new HyperlinkButton();
                        linkButton.Content = count2.ToString() + ". " + obj.fsKeyWord + " (" + obj.fsKeyWordCount.ToString() + ")";
                        linkButton.Tag = obj.fsKeyWord;
                        linkButton.FontSize = 14;
                        SolidColorBrush b = new SolidColorBrush();
                        b.Color = Colors.Black;
                        linkButton.Foreground = b;
                        linkButton.Click += (s1, args1) =>
                        {
                            //MessageBox.Show(((HyperlinkButton)s1).Tag.ToString());
                            string parameter = "";
                            parameter += "<Search><IniPath></IniPath>";
                            parameter += "<KeyWord>" + ((HyperlinkButton)s1).Tag.ToString() + "</KeyWord><IndexName>" + index + "</IndexName><Date></Date>";
                            parameter += "<Channel></Channel><Columns></Columns><Values></Values><Logics></Logics><SearchMode>0</SearchMode><Homophone>0</Homophone>";
                            parameter += "<Synonym>0</Synonym><PageSize>10</PageSize><StartPoint>0</StartPoint><GetColumns>FSSYS_ID;FSFILE_NO;FSTITLE;FSCONTENT;FDCREATED_DATE;FSVWNAME;FCFROM;FSPROG_B_PGMNAME;FNEPISODE;FSTYPE_NAME;FSPROG_D_PGDNAME</GetColumns><TreeNode>/</TreeNode><OrderBy></OrderBy><OrderType></OrderType></Search>";
                        
                            parentFrame.generateNewPane("檢索結果", "SEARCHRESULT", parameter, true);
                        };

                        this.StackPanel_Month.Children.Add(linkButton);

                    }

                    this.RadBusyIndicator_HotKeyWord2.IsBusy = false;
                };

                //近一年
                searchDbMonth.GetHotKeyWordCompleted += (s, args) =>
                {
                    this.RadBusyIndicator_HotKeyWord3.IsBusy = true;
                    if (args.Error != null)
                    {
                        this.RadBusyIndicator_HotKeyWord3.IsBusy = false;
                        return;
                    }

                    if (args.Result == "")
                    {
                        this.RadBusyIndicator_HotKeyWord3.IsBusy = false;
                        return;
                    }

                    XDocument xmlDocResult = XDocument.Parse(args.Result.ToString());

                    var paras = from para in xmlDocResult.Descendants("Data")
                                select new SearchHotKeyWordClass
                                {
                                    fsKeyWord = para.Element("FSKEYWORD").Value,
                                    fsKeyWordCount = Int32.Parse(para.Element("FNKEYWORD_COUNT").Value),
                                };
                    int count3 = 0;

                    foreach (SearchHotKeyWordClass obj in paras)
                    {
                        TextBlock txtBlock = new TextBlock();
                        txtBlock.Height = 10;
                        this.StackPanel_Year.Children.Add(txtBlock);

                        count3++;
                        HyperlinkButton linkButton = new HyperlinkButton();
                        linkButton.Content = count3.ToString() + ". " + obj.fsKeyWord + " (" + obj.fsKeyWordCount.ToString() + ")";
                        linkButton.Tag = obj.fsKeyWord;
                        linkButton.FontSize = 14;
                        SolidColorBrush b = new SolidColorBrush();
                        b.Color = Colors.Black;
                        linkButton.Foreground = b;
                        
                        linkButton.Click += (s1, args1) =>
                        {
                            //MessageBox.Show(((HyperlinkButton)s1).Tag.ToString());
                            string parameter = "";
                            parameter += "<Search><IniPath></IniPath>";
                            parameter += "<KeyWord>" + ((HyperlinkButton)s1).Tag.ToString() + "</KeyWord><IndexName>" + index + "</IndexName><Date></Date>";
                            parameter += "<Channel></Channel><Columns></Columns><Values></Values><Logics></Logics><SearchMode>0</SearchMode><Homophone>0</Homophone>";
                            parameter += "<Synonym>0</Synonym><PageSize>10</PageSize><StartPoint>0</StartPoint><GetColumns>FSSYS_ID;FSFILE_NO;FSTITLE;FSCONTENT;FDCREATED_DATE;FSVWNAME;FCFROM;FSPROG_B_PGMNAME;FNEPISODE;FSTYPE_NAME;FSPROG_D_PGDNAME</GetColumns><TreeNode>/</TreeNode><OrderBy></OrderBy><OrderType></OrderType></Search>";
                        
                            parentFrame.generateNewPane("檢索結果", "SEARCHRESULT", parameter, true);
                        };

                        this.StackPanel_Year.Children.Add(linkButton);

                    }
                    this.RadBusyIndicator_HotKeyWord3.IsBusy = false;
                };


                searchDbWeek.GetHotKeyWordAsync("<Data><FDDATE_BEGIN>" + DateTime.Now.AddDays(-7).ToString("yyyy/MM/dd") + "</FDDATE_BEGIN>" +
                        "<FDDATE_END>" + DateTime.Now.ToString("yyyy/MM/dd") + "</FDDATE_END></Data>");

                searchDbMonth.GetHotKeyWordAsync("<Data><FDDATE_BEGIN>" + DateTime.Now.AddMonths(-1).ToString("yyyy/MM/dd") + "</FDDATE_BEGIN>" +
                        "<FDDATE_END>" + DateTime.Now.ToString("yyyy/MM/dd") + "</FDDATE_END></Data>");

                searchDbYear.GetHotKeyWordAsync("<Data><FDDATE_BEGIN>" + DateTime.Now.AddYears(-1).ToString("yyyy/MM/dd") + "</FDDATE_BEGIN>" +
                        "<FDDATE_END>" + DateTime.Now.ToString("yyyy/MM/dd") + "</FDDATE_END></Data>");
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                this.RadBusyIndicator_HotKeyWord1.IsBusy = false;
                this.RadBusyIndicator_HotKeyWord2.IsBusy = false;
                this.RadBusyIndicator_HotKeyWord3.IsBusy = false;
            }
            
        }
    }

    public class SearchHotKeyWordClass
    {
        public string fsKeyWord { get; set; }
        public int fsKeyWordCount { get; set; }
    }

}
