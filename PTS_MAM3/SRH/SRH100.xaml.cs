﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using PTS_MAM3.WSSearchService;
using PTS_MAM3.WSASPNetSession;
using PTS_MAM3.WCFSearchDataBaseFunction;
using System.Xml;
using System.Xml.Linq;

namespace AllSearch
{
    public partial class Search : UserControl
    {
        //查詢條件XML
        private string parameter = "";
        //SearchServiceClient searchWS = new SearchServiceClient();
        SearchDataBaseFunctionClient searchDb = new SearchDataBaseFunctionClient();

        WSASPNetSessionSoapClient ASPSession = new WSASPNetSessionSoapClient();

        private PTS_MAM3.MainFrame parentFrame;

        //暫存目前勾選的索引庫，為了要判斷是否要重新設定查詢條件
        private List<string> fsINDEX_TEMP_LIST = new List<string>();

        public Search(PTS_MAM3.MainFrame mainframe)
        {
            InitializeComponent();

            parentFrame = mainframe;
            
            this.ComboBox_SearchConditionSelect1.Visibility = System.Windows.Visibility.Collapsed;
            this.ComboBox_SearchConditionSelect2.Visibility = System.Windows.Visibility.Collapsed;
            this.ComboBox_SearchConditionSelect3.Visibility = System.Windows.Visibility.Collapsed;

            this.Grid_SearchCondition1.Visibility = System.Windows.Visibility.Collapsed;
            this.Grid_SearchCondition2.Visibility = System.Windows.Visibility.Collapsed;
            this.Grid_SearchCondition3.Visibility = System.Windows.Visibility.Collapsed;
            
            this.ComboBox_Logic1.Items.Clear();
            this.ComboBox_Logic2.Items.Clear();

            this.ComboBox_Logic1.Visibility = System.Windows.Visibility.Collapsed;
            this.ComboBox_Logic2.Visibility = System.Windows.Visibility.Collapsed;
            //this.DatePicker_AllSearchBegin.SelectedDate = DateTime.Now.AddYears(-1);
            //this.DatePicker_AllSearchEnd.SelectedDate = DateTime.Now;

            searchDb.GetSearchColumnByindexCompleted += (s, args) =>
            {

                if (args.Error != null || String.IsNullOrEmpty(args.Result))
                {
                    return;
                }
                
                XDocument xmlDocResult = XDocument.Parse(args.Result);

                var paras = from para in xmlDocResult.Descendants("Data")
                            select new SearchConditionComboBox
                            {
                                fsFieldEname = para.Element("FSFIELD_ENAME").Value,
                                fsFieldCname = para.Element("FSFIELD_CNAME").Value,
                                fsControlType = para.Element("FSCONTROL_TYPE").Value
                            };
                this.ComboBox_SearchCondition1.ItemsSource = null;
                this.ComboBox_SearchCondition2.ItemsSource = null;
                this.ComboBox_SearchCondition3.ItemsSource = null;

                this.ComboBox_SearchCondition1.ItemsSource = paras;
                this.ComboBox_SearchCondition2.ItemsSource = paras;
                this.ComboBox_SearchCondition3.ItemsSource = paras;

                this.ComboBox_SearchCondition1.SelectedIndex = 0;
                this.ComboBox_SearchCondition2.SelectedIndex = 0;
                this.ComboBox_SearchCondition3.SelectedIndex = 0;

                this.ComboBox_SearchCondition2.IsEnabled = false;
                this.ComboBox_SearchCondition3.IsEnabled = false;

            };

            this.fsINDEX_TEMP_LIST.Add("MAM");
            this.CheckBox_Video.IsChecked = true;
            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>Y</FSTABLE_MAM><FSTABLE_NEWS>N</FSTABLE_NEWS></Data>");
            //searchDb.GetSearchColumnByindexAsync("<Data></Data>");



            List<LogicComboBox> LogiccomboBoxSource = new List<LogicComboBox>();
            LogiccomboBoxSource.Add(new LogicComboBox() { columnValue = "AND", columnName = "AND" });
            LogiccomboBoxSource.Add(new LogicComboBox() { columnValue = "OR", columnName = "OR" });

            this.ComboBox_Logic1.ItemsSource = LogiccomboBoxSource;
            this.ComboBox_Logic2.ItemsSource = LogiccomboBoxSource;

            this.ComboBox_Logic1.IsEnabled = false;
            this.ComboBox_Logic2.IsEnabled = false;


            ClearComditionSearch();


            //排序
            List<OrderComboBox> ordercomboBoxSource = new List<OrderComboBox>();
            ordercomboBoxSource.Add(new OrderComboBox() { columnValue = "", columnName = "無" });
            ordercomboBoxSource.Add(new OrderComboBox() { columnValue = "FDCREATED_DATE", columnName = "建檔日期" });
            ordercomboBoxSource.Add(new OrderComboBox() { columnValue = "FDUPDATED_DATE", columnName = "修改日期" });
            ordercomboBoxSource.Add(new OrderComboBox() { columnValue = "$FSFILE_NO", columnName = "檔案編號" });
            ordercomboBoxSource.Add(new OrderComboBox() { columnValue = "$FSPROG_B_PGMNAME", columnName = "節目名稱" });
            ordercomboBoxSource.Add(new OrderComboBox() { columnValue = "$FSPROG_D_PGDNAME", columnName = "節目子集名稱" });
            ordercomboBoxSource.Add(new OrderComboBox() { columnValue = "FNEPISODE", columnName = "集數" });
            ordercomboBoxSource.Add(new OrderComboBox() { columnValue = "$FSTYPE_NAME", columnName = "影片類型" });
            ordercomboBoxSource.Add(new OrderComboBox() { columnValue = "$FSCHANNEL", columnName = "頻道" });
            ordercomboBoxSource.Add(new OrderComboBox() { columnValue = "$FSCREATED_BY", columnName = "建檔者" });
            ordercomboBoxSource.Add(new OrderComboBox() { columnValue = "$FSUPDATED_BY", columnName = "修改者" });
            this.ComboBox_Order.ItemsSource = ordercomboBoxSource;
            this.ComboBox_Order.SelectedIndex = 1;

            this.Grid_SearchMoreCondition.Visibility = System.Windows.Visibility.Collapsed;

        }


        private void Button_AdvanceSearch_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            this.Grid_SearchMoreCondition.Visibility = System.Windows.Visibility.Visible;
            ClearComditionSearch();
        }

        private void Button_SearchCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Grid_SearchMoreCondition.Visibility = System.Windows.Visibility.Collapsed;
            ClearComditionSearch();
        }

        private void ClearComditionSearch()
        {
            this.TextBox_SearchCondition1.Text = "";
            this.TextBox_SearchCondition2.Text = "";
            this.TextBox_SearchCondition3.Text = "";
            this.ComboBox_Logic1.SelectedIndex = 0;
            this.ComboBox_Logic2.SelectedIndex = 0;
        }


        public class SearchConditionComboBox
        {
            public string fsFieldEname { get; set; }
            public string fsFieldCname { get; set; }
            public string fsControlType { get; set; }
        }

        public class LogicComboBox
        {
            public string columnValue { get; set; }
            public string columnName { get; set; }
        }

        public class OrderComboBox
        {
            public string columnValue { set; get; }
            public string columnName { set; get; }
        }

        private void DoSearch()
        {
            string userSearchHistoryParameter = "";
            string keyword = null;
            userSearchHistoryParameter += "<Data>";
            userSearchHistoryParameter += "<FSUSER_ID>" + UserClass.userData.FSUSER_ID + "</FSUSER_ID>";
            userSearchHistoryParameter += "<FDSEARCH_DATE>" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "</FDSEARCH_DATE>";

            parameter = "";
            parameter += "<Search>";
            parameter += "<IniPath></IniPath>";

            //關鍵字
            if (this.TextBox_KeyWord.Text == "")
            {
                parameter += "<KeyWord></KeyWord>";
                userSearchHistoryParameter += "<FSKEY_WORD></FSKEY_WORD>";
            }
            else
            {
                parameter += "<KeyWord><![CDATA[" + this.TextBox_KeyWord.Text.Replace(" ", "&") + "]]></KeyWord>";
                userSearchHistoryParameter += "<FSKEY_WORD>" + this.TextBox_KeyWord.Text.Replace(" ", "&") + "</FSKEY_WORD>";

                keyword = this.TextBox_KeyWord.Text.Replace("&", ":").Replace("|", ":").Replace("!", ":");
            }



            string begYear = "2000";
            string endYear = "2020";

            //索引庫
            if ((bool)(!this.CheckBox_Video.IsChecked) && (bool)(!this.CheckBox_Audio.IsChecked) && (bool)(!this.CheckBox_Photo.IsChecked) &&
                    (bool)(!this.CheckBox_Doc.IsChecked) && (bool)(!this.CheckBox_News.IsChecked))
            {
                MessageBox.Show("資料來源至少勾選一項!");
                return;
            }

            parameter += "<IndexName>";
            userSearchHistoryParameter += "<FSINDEX>";
            if (this.CheckBox_Video.IsChecked == true)
            {

                //parameter += GetIndex("video", begYear, endYear) + ",";

                //userSearchHistoryParameter += GetIndex("video", begYear, endYear) + ",";

                parameter += "Video,";

                userSearchHistoryParameter += "Video,";
            }

            if (this.CheckBox_Audio.IsChecked == true)
            {
                //parameter += GetIndex("audio", begYear, endYear) + ",";

                //userSearchHistoryParameter += GetIndex("audio", begYear, endYear) + ",";

                parameter += "Audio,";

                userSearchHistoryParameter += "Audio,";
            }

            if (this.CheckBox_Photo.IsChecked == true)
            {
                //parameter += GetIndex("photo", begYear, endYear) + ",";

                //userSearchHistoryParameter += GetIndex("photo", begYear, endYear) + ",";
                parameter += "Photo,";

                userSearchHistoryParameter += "Photo,";
            }

            if (this.CheckBox_Doc.IsChecked == true)
            {
                //parameter += GetIndex("doc", begYear, endYear) + ",";

                //userSearchHistoryParameter += GetIndex("doc", begYear, endYear) + ",";

                parameter += "Doc,";

                userSearchHistoryParameter += "Doc,";
            }

            if (this.CheckBox_News.IsChecked == true)
            {
                //parameter += GetIndex("news", begYear, endYear) + ",";

                //userSearchHistoryParameter += GetIndex("news", begYear, endYear) + ",";

                parameter += "News_2006,News_2011,News_2016,";

                userSearchHistoryParameter += "News_2006,News_2011,News_2016,";
            }

            parameter = parameter.Substring(0, parameter.Length - 1);
            userSearchHistoryParameter = userSearchHistoryParameter.Substring(0, userSearchHistoryParameter.Length - 1);

            parameter += "</IndexName>";
            userSearchHistoryParameter += "</FSINDEX>";


            parameter += "<Date></Date>";

            userSearchHistoryParameter += "<FSCREATED_DATE_BEGIN></FSCREATED_DATE_BEGIN>";
            userSearchHistoryParameter += "<FSCREATED_DATE_END></FSCREATED_DATE_END>";


            string channel = "";

            string columnName = "";
            string columnValue = "";
            string logic = "";
            int allColumnCount = 0;


            //頻道
            if (this.CheckBox_PTS.IsChecked == true)
            {
                channel += "公視;";
            }
            if (this.CheckBox_Hakka.IsChecked == true)
            {
                channel += "客家;";
            }
            if (this.CheckBox_Titv.IsChecked == true)
            {
                channel += "原視;";
            }
            if (this.CheckBox_Nactv.IsChecked == true)
            {
                channel += "宏觀;";
            }

            if (channel != "")
            {
                channel = channel.Substring(0, channel.Length - 1);
            }

            userSearchHistoryParameter += "<FSCHANNEL>" + channel + "</FSCHANNEL>";
            if (this.Grid_SearchMoreCondition.Visibility == System.Windows.Visibility.Visible)
            {
                //欄位查詢
                if ((((SearchConditionComboBox)this.ComboBox_SearchCondition1.SelectedItem).fsFieldEname != ""))
                {

                    if (this.TextBox_SearchCondition1.Visibility == System.Windows.Visibility.Visible && this.TextBox_SearchCondition1.Text == "")
                    {
                        MessageBox.Show("請輸入查詢條件!");
                        this.TextBox_SearchCondition1.Focus();
                        return;
                    }
                    else if ((this.Grid_SearchCondition1.Visibility == System.Windows.Visibility.Visible && !this.DatePicker_SearchCondition1_Begin.SelectedDate.HasValue) ||
                        (this.Grid_SearchCondition1.Visibility == System.Windows.Visibility.Visible && !this.DatePicker_SearchCondition1_End.SelectedDate.HasValue))
                    {
                        MessageBox.Show("請輸入起迄日期!");
                        return;
                    }
                    else
                    {
                        if (((SearchConditionComboBox)this.ComboBox_SearchCondition1.SelectedItem).fsFieldEname == "*")
                        {
                            allColumnCount++;
                        }
                        columnName += ((SearchConditionComboBox)this.ComboBox_SearchCondition1.SelectedItem).fsFieldEname + ";";
                        userSearchHistoryParameter += "<FSCOLUMN1>" + ((SearchConditionComboBox)this.ComboBox_SearchCondition1.SelectedItem).fsFieldEname + "</FSCOLUMN1>";

                        if (this.TextBox_SearchCondition1.Visibility == System.Windows.Visibility.Visible)
                        {
                            columnValue += this.TextBox_SearchCondition1.Text.Trim().Replace(" ", "&") + ";";
                            userSearchHistoryParameter += "<FSVALUE1><![CDATA[" + this.TextBox_SearchCondition1.Text.Trim().Replace(" ", "&") + "]]></FSVALUE1>";
                        }
                        else if (this.ComboBox_SearchConditionSelect1.Visibility == System.Windows.Visibility.Visible)
                        {
                            columnValue += ((SearchConditionComboBoxEnum)this.ComboBox_SearchConditionSelect1.SelectedItem).fsValue + ";";
                            userSearchHistoryParameter += "<FSVALUE1>" + ((SearchConditionComboBoxEnum)this.ComboBox_SearchConditionSelect1.SelectedItem).fsValue + "</FSVALUE1>";
                        }
                        else if (this.Grid_SearchCondition1.Visibility == System.Windows.Visibility.Visible)
                        {
                            columnValue += this.DatePicker_SearchCondition1_Begin.SelectedDate.Value.ToString("yyyy/MM/dd") + "~" + this.DatePicker_SearchCondition1_End.SelectedDate.Value.ToString("yyyy/MM/dd") + ";";
                            userSearchHistoryParameter += "<FSVALUE1>" + this.DatePicker_SearchCondition1_Begin.SelectedDate.Value.ToString("yyyy/MM/dd") + "~" + this.DatePicker_SearchCondition1_End.SelectedDate.Value.ToString("yyyy/MM/dd") + "</FSVALUE1>";
                        }
                    }
                }
                else
                {
                    userSearchHistoryParameter += "<FSCOLUMN1></FSCOLUMN1>";
                    userSearchHistoryParameter += "<FSVALUE1></FSVALUE1>";
                }


                if ((((SearchConditionComboBox)this.ComboBox_SearchCondition2.SelectedItem).fsFieldEname != ""))
                {
                    if (this.TextBox_SearchCondition2.Visibility == System.Windows.Visibility.Visible && this.TextBox_SearchCondition2.Text == "")
                    {
                        MessageBox.Show("請輸入查詢條件!");
                        this.TextBox_SearchCondition2.Focus();
                        return;
                    }
                    else if ((this.Grid_SearchCondition2.Visibility == System.Windows.Visibility.Visible && !this.DatePicker_SearchCondition2_Begin.SelectedDate.HasValue) ||
                         (this.Grid_SearchCondition2.Visibility == System.Windows.Visibility.Visible && !this.DatePicker_SearchCondition2_End.SelectedDate.HasValue))
                    {
                        MessageBox.Show("請輸入起迄日期!");
                        return;
                    }
                    else
                    {
                        if (((SearchConditionComboBox)this.ComboBox_SearchCondition2.SelectedItem).fsFieldEname == "*")
                        {
                            allColumnCount++;
                        }
                        columnName += ((SearchConditionComboBox)this.ComboBox_SearchCondition2.SelectedItem).fsFieldEname + ";";
                        userSearchHistoryParameter += "<FSCOLUMN2>" + ((SearchConditionComboBox)this.ComboBox_SearchCondition2.SelectedItem).fsFieldEname + "</FSCOLUMN2>";
                        if (this.TextBox_SearchCondition2.Visibility == System.Windows.Visibility.Visible)
                        {
                            columnValue += this.TextBox_SearchCondition2.Text.Trim().Replace(" ", "&") + ";";
                            userSearchHistoryParameter += "<FSVALUE2><![CDATA[" + this.TextBox_SearchCondition2.Text.Trim().Replace(" ", "&") + "]]></FSVALUE2>";
                        }
                        else if (this.ComboBox_SearchConditionSelect2.Visibility == System.Windows.Visibility.Visible)
                        {
                            columnValue += ((SearchConditionComboBoxEnum)this.ComboBox_SearchConditionSelect2.SelectedItem).fsValue + ";";
                            userSearchHistoryParameter += "<FSVALUE2>" + ((SearchConditionComboBoxEnum)this.ComboBox_SearchConditionSelect2.SelectedItem).fsValue + "</FSVALUE2>";
                        }
                        else if (this.Grid_SearchCondition2.Visibility == System.Windows.Visibility.Visible)
                        {
                            columnValue += this.DatePicker_SearchCondition2_Begin.SelectedDate.Value.ToString("yyyy/MM/dd") + "~" + this.DatePicker_SearchCondition2_End.SelectedDate.Value.ToString("yyyy/MM/dd") + ";";
                            userSearchHistoryParameter += "<FSVALUE2>" + this.DatePicker_SearchCondition2_Begin.SelectedDate.Value.ToString("yyyy/MM/dd") + "~" + this.DatePicker_SearchCondition2_End.SelectedDate.Value.ToString("yyyy/MM/dd") + "</FSVALUE2>";
                        }


                        logic += this.ComboBox_Logic1.SelectedValue + ";";
                        userSearchHistoryParameter += "<FSLOGIC1>" + this.ComboBox_Logic1.SelectedValue + "</FSLOGIC1>";
                    }
                }
                else
                {
                    userSearchHistoryParameter += "<FSCOLUMN2></FSCOLUMN2>";
                    userSearchHistoryParameter += "<FSVALUE2></FSVALUE2>";
                    userSearchHistoryParameter += "<FSLOGIC1></FSLOGIC1>";
                }


                if ((((SearchConditionComboBox)this.ComboBox_SearchCondition3.SelectedItem).fsFieldEname != ""))
                {
                    if (this.TextBox_SearchCondition3.Visibility == System.Windows.Visibility.Visible && this.TextBox_SearchCondition3.Text == "")
                    {
                        MessageBox.Show("請輸入查詢條件!");
                        this.TextBox_SearchCondition1.Focus();
                        return;
                    }
                    else if ((this.Grid_SearchCondition3.Visibility == System.Windows.Visibility.Visible && !this.DatePicker_SearchCondition3_Begin.SelectedDate.HasValue) ||
                     (this.Grid_SearchCondition3.Visibility == System.Windows.Visibility.Visible && !this.DatePicker_SearchCondition3_End.SelectedDate.HasValue))
                    {
                        MessageBox.Show("請輸入起迄日期!");
                        return;
                    }
                    else
                    {
                        if (((SearchConditionComboBox)this.ComboBox_SearchCondition3.SelectedItem).fsFieldEname == "*")
                        {
                            allColumnCount++;
                        }
                        columnName += ((SearchConditionComboBox)this.ComboBox_SearchCondition3.SelectedItem).fsFieldEname + ";";
                        userSearchHistoryParameter += "<FSCOLUMN3>" + ((SearchConditionComboBox)this.ComboBox_SearchCondition3.SelectedItem).fsFieldEname + "</FSCOLUMN3>";
                        if (this.TextBox_SearchCondition3.Visibility == System.Windows.Visibility.Visible)
                        {
                            columnValue += this.TextBox_SearchCondition3.Text.Trim().Replace(" ", "&") + ";";
                            userSearchHistoryParameter += "<FSVALUE3><![CDATA[" + this.TextBox_SearchCondition1.Text.Trim().Replace(" ", "&") + "]]></FSVALUE3>";
                        }
                        else if (this.ComboBox_SearchConditionSelect3.Visibility == System.Windows.Visibility.Visible)
                        {
                            columnValue += ((SearchConditionComboBoxEnum)this.ComboBox_SearchConditionSelect3.SelectedItem).fsValue + ";";
                            userSearchHistoryParameter += "<FSVALUE3>" + ((SearchConditionComboBoxEnum)this.ComboBox_SearchConditionSelect3.SelectedItem).fsValue + "</FSVALUE3>";
                        }
                        else if (this.Grid_SearchCondition3.Visibility == System.Windows.Visibility.Visible)
                        {
                            columnValue += this.DatePicker_SearchCondition3_Begin.SelectedDate.Value.ToString("yyyy/MM/dd") + "~" + this.DatePicker_SearchCondition3_End.SelectedDate.Value.ToString("yyyy/MM/dd") + ";";
                            userSearchHistoryParameter += "<FSVALUE3>" + this.DatePicker_SearchCondition3_Begin.SelectedDate.Value.ToString("yyyy/MM/dd") + "~" + this.DatePicker_SearchCondition3_End.SelectedDate.Value.ToString("yyyy/MM/dd") + "</FSVALUE3>";
                        }

                        logic += this.ComboBox_Logic2.SelectedValue + ";";
                        userSearchHistoryParameter += "<FSLOGIC2>" + this.ComboBox_Logic2.SelectedValue + "</FSLOGIC2>";
                    }
                }
                else
                {
                    userSearchHistoryParameter += "<FSCOLUMN3></FSCOLUMN3>";
                    userSearchHistoryParameter += "<FSVALUE3></FSVALUE3>";
                    userSearchHistoryParameter += "<FSLOGIC2></FSLOGIC2>";
                }

                if (allColumnCount > 1)
                {
                    MessageBox.Show("跨欄位查詢只能一個!");
                    return;
                }

                if (columnName != "")
                {
                    columnName = columnName.Substring(0, columnName.Length - 1);
                    columnValue = columnValue.Substring(0, columnValue.Length - 1);

                }
                if (logic != "")
                {
                    logic = logic.Substring(0, logic.Length - 1);
                }

            }
            else
            {
                userSearchHistoryParameter += "<FSCHANNEL></FSCHANNEL>";
                userSearchHistoryParameter += "<FSCOLUMN1></FSCOLUMN1>";
                userSearchHistoryParameter += "<FSVALUE1></FSVALUE1>";
                userSearchHistoryParameter += "<FSCOLUMN2></FSCOLUMN2>";
                userSearchHistoryParameter += "<FSVALUE2></FSVALUE2>";
                userSearchHistoryParameter += "<FSLOGIC1></FSLOGIC1>";
                userSearchHistoryParameter += "<FSCOLUMN3></FSCOLUMN3>";
                userSearchHistoryParameter += "<FSVALUE3></FSVALUE3>";
                userSearchHistoryParameter += "<FSLOGIC2></FSLOGIC2>";
            }
            parameter += "<Channel>" + channel + "</Channel>";
            parameter += "<Columns><![CDATA[" + columnName + "]]></Columns>";
            parameter += "<Values><![CDATA[" + columnValue + "]]></Values>";
            parameter += "<Logics>" + logic + "</Logics>";



            //查詢模式：0->標準查詢 ; 1->模糊查詢
            if ((bool)this.RadioButton_StandardSearch.IsChecked)
            {
                parameter += "<SearchMode>0</SearchMode>";
                userSearchHistoryParameter += "<FSSEARCH_MODE>0</FSSEARCH_MODE>";
            }
            else if ((bool)this.RadioButton_FuzzySearch.IsChecked)
            {
                parameter += "<SearchMode>1</SearchMode>";
                userSearchHistoryParameter += "<FSSEARCH_MODE>1</FSSEARCH_MODE>";
            }

            //同音字
            if ((bool)this.RadioButton_HomoPhoneActive.IsChecked)
            {
                parameter += "<Homophone>1</Homophone>";
                userSearchHistoryParameter += "<FSHOMOPHONE>1</FSHOMOPHONE>";
            }
            else if ((bool)this.RadioButton_HomoPhoneUnActive.IsChecked)
            {
                parameter += "<Homophone>0</Homophone>";
                userSearchHistoryParameter += "<FSHOMOPHONE>0</FSHOMOPHONE>";
            }

            //同義詞
            if ((bool)this.RadioButton_SynonymActive.IsChecked)
            {
                parameter += "<Synonym>1</Synonym>";
                userSearchHistoryParameter += "<FSSYNONYM>1</FSSYNONYM>";
            }
            else if ((bool)this.RadioButton_SynonymUnActive.IsChecked)
            {
                parameter += "<Synonym>0</Synonym>";
                userSearchHistoryParameter += "<FSSYNONYM>0</FSSYNONYM>";
            }

            //每頁筆數
            if ((bool)this.RadioButton_Record10.IsChecked)
            {
                parameter += "<PageSize>10</PageSize>";
                userSearchHistoryParameter += "<FNPAGE_SIZE>10</FNPAGE_SIZE>";
            }
            else if ((bool)this.RadioButton_Record20.IsChecked)
            {
                parameter += "<PageSize>20</PageSize>";
                userSearchHistoryParameter += "<FNPAGE_SIZE>20</FNPAGE_SIZE>";
            }
            else if ((bool)this.RadioButton_Record50.IsChecked)
            {
                parameter += "<PageSize>50</PageSize>";
                userSearchHistoryParameter += "<FNPAGE_SIZE>50</FNPAGE_SIZE>";
            }
            parameter += "<StartPoint>0</StartPoint>";
            userSearchHistoryParameter += "<FNSTART_POINT>0</FNSTART_POINT>";

            parameter += "<GetColumns>FSSYS_ID;FSFILE_NO;FSTITLE;FSCONTENT;FDCREATED_DATE;FSVWNAME;FCFROM;FSPROG_B_PGMNAME;FNEPISODE;FSTYPE_NAME;FSPROG_D_PGDNAME;FSTRACK</GetColumns>";
            userSearchHistoryParameter += "<FSGET_COLUMN>FSSYS_ID;FSFILE_NO;FSTITLE;FSCONTENT;FDCREATED_DATE;FSVWNAME;FCFROM;FSPROG_B_PGMNAME;FNEPISODE;FSTYPE_NAME;FSPROG_D_PGDNAME;FSTRACK</FSGET_COLUMN>";

            parameter += "<TreeNode>/</TreeNode>";
            userSearchHistoryParameter += "<FSTREE_NODE>/</FSTREE_NODE>";

            //排序
            if (this.ComboBox_Order.SelectedValue.ToString() != "")
            {
                parameter += "<OrderBy>" + this.ComboBox_Order.SelectedValue + "</OrderBy>";
                userSearchHistoryParameter += "<FSORDERBY>" + this.ComboBox_Order.SelectedValue + "</FSORDERBY>";

                if ((bool)this.RadioButton_Asc.IsChecked)
                {
                    parameter += "<OrderType>0</OrderType>";
                    userSearchHistoryParameter += "<FSORDERTYPE>0</FSORDERTYPE>";
                }
                else if ((bool)this.RadioButton_Desc.IsChecked)
                {
                    parameter += "<OrderType>1</OrderType>";
                    userSearchHistoryParameter += "<FSORDERTYPE>1</FSORDERTYPE>";
                }
            }
            else
            {
                parameter += "<OrderBy></OrderBy>";
                userSearchHistoryParameter += "<FSORDERBY></FSORDERBY>";

                parameter += "<OrderType></OrderType>";
                userSearchHistoryParameter += "<FSORDERTYPE></FSORDERTYPE>";
            }
            

            parameter += "</Search>";


            userSearchHistoryParameter += "</Data>";




            //parameter = parameter.Replace("&", "&amp;");
            //userSearchHistoryParameter = userSearchHistoryParameter.Replace("&", "&amp;");

            SearchDataBaseFunctionClient searchDb = new SearchDataBaseFunctionClient();
            //查詢條件寫入資料庫
            searchDb.InsertSearchHistoryParameterAsync(userSearchHistoryParameter, UserClass.userData.FSUSER_ID);
            searchDb.InsertKeyWordAsync("<Data><FSKEYWORD>" + keyword + "</FSKEYWORD><FDDATE>" + DateTime.Now.ToString("yyyy/MM/dd") + "</FDDATE></Data>", UserClass.userData.FSUSER_ID);


            parentFrame.generateNewPane("檢索結果", "SEARCHRESULT", parameter, true);
        }


        private void Button_AllSearch_Click(object sender, RoutedEventArgs e)
        {
            DoSearch();
        }

        public class SearchConditionComboBoxEnum
        {
            public string fsValue { get; set; }
        }


        private void ComboBox_SearchCondition1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            SearchDataBaseFunctionClient searchDb1 = new SearchDataBaseFunctionClient();
            if (this.ComboBox_SearchCondition1.SelectedItem == null)
            {
                return;
            }

            if (((SearchConditionComboBox)this.ComboBox_SearchCondition1.SelectedItem).fsFieldEname != "")
            {
                this.Grid_SearchCondition1.Visibility = System.Windows.Visibility.Collapsed;
                this.DatePicker_SearchCondition1_Begin.SelectedDate = DateTime.Now;
                this.DatePicker_SearchCondition1_End.SelectedDate = DateTime.Now;

                this.TextBox_SearchCondition1.Visibility = System.Windows.Visibility.Collapsed;
                this.TextBox_SearchCondition1.Text = "";

                this.ComboBox_SearchConditionSelect1.Visibility = System.Windows.Visibility.Collapsed;
                //決定顯示哪種控制項
                if (((SearchConditionComboBox)this.ComboBox_SearchCondition1.SelectedItem).fsControlType == "Date")
                {
                    this.Grid_SearchCondition1.Visibility = System.Windows.Visibility.Visible;
                    this.TextBox_SearchCondition1.Visibility = System.Windows.Visibility.Collapsed;
                    this.ComboBox_SearchConditionSelect1.Visibility = System.Windows.Visibility.Collapsed;
                }
                else if (((SearchConditionComboBox)this.ComboBox_SearchCondition1.SelectedItem).fsControlType == "Enum")
                {
                    searchDb1.GetSearchColumnTypeCompleted += (s, args) =>
                    {
                        if (args.Error != null)
                        {
                            return;
                        }

                        if (args.Result == "")
                        {
                            return;
                        }

                        XDocument xmlDocResult = XDocument.Parse(args.Result.ToString());

                        var paras = from para in xmlDocResult.Descendants("Data")
                                    select new SearchConditionComboBoxEnum
                                    {
                                        fsValue = para.Element("FSVALUE").Value,
                                    };

                        this.ComboBox_SearchConditionSelect1.ItemsSource = paras;
                        this.ComboBox_SearchConditionSelect1.SelectedIndex = 0;
                        this.TextBox_SearchCondition1.Visibility = System.Windows.Visibility.Collapsed;
                        this.ComboBox_SearchConditionSelect1.Visibility = System.Windows.Visibility.Visible;
                    };

                    searchDb1.GetSearchColumnTypeAsync("<Data><FSFILED_ENAME>" + ((SearchConditionComboBox)this.ComboBox_SearchCondition1.SelectedItem).fsFieldEname + "</FSFILED_ENAME></Data>");
                }
                else
                {
                    this.TextBox_SearchCondition1.Visibility = System.Windows.Visibility.Visible;
                    this.ComboBox_SearchConditionSelect1.Visibility = System.Windows.Visibility.Collapsed;
                }


                this.ComboBox_Logic1.SelectedIndex = 0;
                this.ComboBox_SearchCondition2.IsEnabled = true;
            }
            else
            {
                this.TextBox_SearchCondition1.Text = "";
                this.Grid_SearchCondition1.Visibility = System.Windows.Visibility.Collapsed;
                this.TextBox_SearchCondition1.Visibility = System.Windows.Visibility.Collapsed;
                this.ComboBox_SearchConditionSelect1.Visibility = System.Windows.Visibility.Collapsed;
                this.ComboBox_SearchCondition2.IsEnabled = false;
            }
        }


        private void ComboBox_SearchCondition2_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            SearchDataBaseFunctionClient searchDb2 = new SearchDataBaseFunctionClient();
            if (this.ComboBox_SearchCondition2.SelectedItem == null)
            {
                return;
            }

            if (((SearchConditionComboBox)this.ComboBox_SearchCondition2.SelectedItem).fsFieldEname != "")
            {
                this.Grid_SearchCondition2.Visibility = System.Windows.Visibility.Collapsed;
                this.DatePicker_SearchCondition2_Begin.SelectedDate = DateTime.Now;
                this.DatePicker_SearchCondition2_End.SelectedDate = DateTime.Now;

                this.TextBox_SearchCondition2.Visibility = System.Windows.Visibility.Collapsed;
                this.TextBox_SearchCondition2.Text = "";

                this.ComboBox_SearchConditionSelect2.Visibility = System.Windows.Visibility.Collapsed;

                //決定顯示哪種控制項
                if (((SearchConditionComboBox)this.ComboBox_SearchCondition2.SelectedItem).fsControlType == "Date")
                {
                    this.Grid_SearchCondition2.Visibility = System.Windows.Visibility.Visible;
                    this.TextBox_SearchCondition2.Visibility = System.Windows.Visibility.Collapsed;
                    this.ComboBox_SearchConditionSelect2.Visibility = System.Windows.Visibility.Collapsed;
                }
                else if (((SearchConditionComboBox)this.ComboBox_SearchCondition2.SelectedItem).fsControlType == "Enum")
                {
                    searchDb2.GetSearchColumnTypeCompleted += (s, args) =>
                    {
                        if (args.Error != null)
                        {
                            return;
                        }

                        if (args.Result == "")
                        {
                            return;
                        }

                        XDocument xmlDocResult = XDocument.Parse(args.Result.ToString());

                        var paras = from para in xmlDocResult.Descendants("Data")
                                    select new SearchConditionComboBoxEnum
                                    {
                                        fsValue = para.Element("FSVALUE").Value,
                                    };

                        this.ComboBox_SearchConditionSelect2.ItemsSource = paras;
                        this.ComboBox_SearchConditionSelect2.SelectedIndex = 0;
                        this.TextBox_SearchCondition2.Visibility = System.Windows.Visibility.Collapsed;
                        this.ComboBox_SearchConditionSelect2.Visibility = System.Windows.Visibility.Visible;
                        this.Grid_SearchCondition2.Visibility = System.Windows.Visibility.Collapsed;
                    };

                    searchDb2.GetSearchColumnTypeAsync("<Data><FSFILED_ENAME>" + ((SearchConditionComboBox)this.ComboBox_SearchCondition2.SelectedItem).fsFieldEname + "</FSFILED_ENAME></Data>");
                }
                else
                {
                    this.TextBox_SearchCondition2.Visibility = System.Windows.Visibility.Visible;
                    this.ComboBox_SearchConditionSelect2.Visibility = System.Windows.Visibility.Collapsed;
                }

                //若前一個欄位為跨欄位，則邏輯運算一律為And
                if (((SearchConditionComboBox)this.ComboBox_SearchCondition1.SelectedItem).fsFieldEname == "*")
                {
                    this.ComboBox_Logic1.SelectedIndex = 0;
                    this.ComboBox_Logic1.IsEnabled = false;
                    
                }
                else
                {
                    this.ComboBox_Logic1.IsEnabled = true;
                    this.ComboBox_Logic1.Visibility = System.Windows.Visibility.Visible;
                }
                this.ComboBox_SearchCondition3.IsEnabled = true;
            }
            else
            {
                this.ComboBox_Logic1.IsEnabled = false;
                this.ComboBox_Logic1.Visibility = System.Windows.Visibility.Collapsed;
                this.Grid_SearchCondition2.Visibility = System.Windows.Visibility.Collapsed;
                this.TextBox_SearchCondition2.Visibility = System.Windows.Visibility.Collapsed;
                this.ComboBox_SearchConditionSelect2.Visibility = System.Windows.Visibility.Collapsed;
                this.TextBox_SearchCondition2.Text = "";
                this.ComboBox_SearchCondition3.IsEnabled = false;
            }
        }

        private void ComboBox_SearchCondition3_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            SearchDataBaseFunctionClient searchDb3 = new SearchDataBaseFunctionClient();
            if (this.ComboBox_SearchCondition3.SelectedItem == null)
            {
                return;
            }

            if (((SearchConditionComboBox)this.ComboBox_SearchCondition3.SelectedItem).fsFieldEname != "")
            {
                this.Grid_SearchCondition3.Visibility = System.Windows.Visibility.Collapsed;
                this.DatePicker_SearchCondition3_Begin.SelectedDate = DateTime.Now;
                this.DatePicker_SearchCondition3_End.SelectedDate = DateTime.Now;

                this.TextBox_SearchCondition3.Visibility = System.Windows.Visibility.Collapsed;
                this.TextBox_SearchCondition3.Text = "";

                this.ComboBox_SearchConditionSelect3.Visibility = System.Windows.Visibility.Collapsed;

                //決定顯示哪種控制項
                if (((SearchConditionComboBox)this.ComboBox_SearchCondition3.SelectedItem).fsControlType == "Date")
                {
                    this.Grid_SearchCondition3.Visibility = System.Windows.Visibility.Visible;
                    this.TextBox_SearchCondition3.Visibility = System.Windows.Visibility.Collapsed;
                    this.ComboBox_SearchConditionSelect3.Visibility = System.Windows.Visibility.Collapsed;
                }
                else if (((SearchConditionComboBox)this.ComboBox_SearchCondition3.SelectedItem).fsControlType == "Enum")
                {
                    searchDb3.GetSearchColumnTypeCompleted += (s, args) =>
                    {
                        if (args.Error != null)
                        {
                            return;
                        }

                        if (args.Result == "")
                        {
                            return;
                        }

                        XDocument xmlDocResult = XDocument.Parse(args.Result.ToString());

                        var paras = from para in xmlDocResult.Descendants("Data")
                                    select new SearchConditionComboBoxEnum
                                    {
                                        fsValue = para.Element("FSVALUE").Value,
                                    };

                        this.ComboBox_SearchConditionSelect3.ItemsSource = paras;
                        this.ComboBox_SearchConditionSelect3.SelectedIndex = 0;
                        this.TextBox_SearchCondition3.Visibility = System.Windows.Visibility.Collapsed;
                        this.ComboBox_SearchConditionSelect3.Visibility = System.Windows.Visibility.Visible;
                    };

                    searchDb3.GetSearchColumnTypeAsync("<Data><FSFILED_ENAME>" + ((SearchConditionComboBox)this.ComboBox_SearchCondition3.SelectedItem).fsFieldEname + "</FSFILED_ENAME></Data>");
                }
                else
                {
                    this.TextBox_SearchCondition3.Visibility = System.Windows.Visibility.Visible;
                    this.ComboBox_SearchConditionSelect3.Visibility = System.Windows.Visibility.Collapsed;
                }

                //若前一個欄位為跨欄位，則邏輯運算一律為And
                if (((SearchConditionComboBox)this.ComboBox_SearchCondition2.SelectedItem).fsFieldEname == "*")
                {
                    this.ComboBox_Logic2.SelectedIndex = 0;
                    this.ComboBox_Logic2.IsEnabled = false;
                }
                else
                {
                    this.ComboBox_Logic2.IsEnabled = true;
                    this.ComboBox_Logic2.Visibility = System.Windows.Visibility.Visible;
                }
            }
            else
            {
                this.TextBox_SearchCondition3.Text = "";
                this.ComboBox_Logic2.IsEnabled = false;
                this.ComboBox_Logic2.Visibility = System.Windows.Visibility.Collapsed;
                this.Grid_SearchCondition3.Visibility = System.Windows.Visibility.Collapsed;
                this.TextBox_SearchCondition3.Visibility = System.Windows.Visibility.Collapsed;
                this.ComboBox_SearchConditionSelect3.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        //private void CheckBox_Video_Checked(object sender, RoutedEventArgs e)
        //{
        //    if ((bool)this.CheckBox_Audio.IsChecked || (bool)this.CheckBox_Photo.IsChecked ||
        //        (bool)this.CheckBox_Doc.IsChecked || (bool)this.CheckBox_Video.IsChecked)
        //    {
        //        if ((bool)this.CheckBox_News.IsChecked)
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>Y</FSTABLE_MAM><FSTABLE_NEWS>Y</FSTABLE_NEWS></Data>");
        //        }
        //        else
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>Y</FSTABLE_MAM><FSTABLE_NEWS>N</FSTABLE_NEWS></Data>");
        //        }
        //    }
        //    else
        //    {
        //        if ((bool)this.CheckBox_News.IsChecked)
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>N</FSTABLE_MAM><FSTABLE_NEWS>Y</FSTABLE_NEWS></Data>");
        //        }
        //        else
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>N</FSTABLE_MAM><FSTABLE_NEWS>N</FSTABLE_NEWS></Data>");
        //        }

        //    }
        //}

        //private void CheckBox_Audio_Checked(object sender, RoutedEventArgs e)
        //{
        //    if ((bool)this.CheckBox_Audio.IsChecked || (bool)this.CheckBox_Photo.IsChecked ||
        //        (bool)this.CheckBox_Doc.IsChecked || (bool)this.CheckBox_Video.IsChecked)
        //    {
        //        if ((bool)this.CheckBox_News.IsChecked)
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>Y</FSTABLE_MAM><FSTABLE_NEWS>Y</FSTABLE_NEWS></Data>");
        //        }
        //        else
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>Y</FSTABLE_MAM><FSTABLE_NEWS>N</FSTABLE_NEWS></Data>");
        //        }
        //    }
        //    else
        //    {
        //        if ((bool)this.CheckBox_News.IsChecked)
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>N</FSTABLE_MAM><FSTABLE_NEWS>Y</FSTABLE_NEWS></Data>");
        //        }
        //        else
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>N</FSTABLE_MAM><FSTABLE_NEWS>N</FSTABLE_NEWS></Data>");
        //        }

        //    }
        //}

        //private void CheckBox_Photo_Checked(object sender, RoutedEventArgs e)
        //{
        //    if ((bool)this.CheckBox_Audio.IsChecked || (bool)this.CheckBox_Photo.IsChecked ||
        //        (bool)this.CheckBox_Doc.IsChecked || (bool)this.CheckBox_Video.IsChecked)
        //    {
        //        if ((bool)this.CheckBox_News.IsChecked)
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>Y</FSTABLE_MAM><FSTABLE_NEWS>Y</FSTABLE_NEWS></Data>");
        //        }
        //        else
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>Y</FSTABLE_MAM><FSTABLE_NEWS>N</FSTABLE_NEWS></Data>");
        //        }
        //    }
        //    else
        //    {
        //        if ((bool)this.CheckBox_News.IsChecked)
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>N</FSTABLE_MAM><FSTABLE_NEWS>Y</FSTABLE_NEWS></Data>");
        //        }
        //        else
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>N</FSTABLE_MAM><FSTABLE_NEWS>N</FSTABLE_NEWS></Data>");
        //        }

        //    }
        //}

        //private void CheckBox_Doc_Checked(object sender, RoutedEventArgs e)
        //{
        //    if ((bool)this.CheckBox_Audio.IsChecked || (bool)this.CheckBox_Photo.IsChecked ||
        //        (bool)this.CheckBox_Doc.IsChecked || (bool)this.CheckBox_Video.IsChecked)
        //    {
        //        if ((bool)this.CheckBox_News.IsChecked)
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>Y</FSTABLE_MAM><FSTABLE_NEWS>Y</FSTABLE_NEWS></Data>");
        //        }
        //        else
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>Y</FSTABLE_MAM><FSTABLE_NEWS>N</FSTABLE_NEWS></Data>");
        //        }
        //    }
        //    else
        //    {
        //        if ((bool)this.CheckBox_News.IsChecked)
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>N</FSTABLE_MAM><FSTABLE_NEWS>Y</FSTABLE_NEWS></Data>");
        //        }
        //        else
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>N</FSTABLE_MAM><FSTABLE_NEWS>N</FSTABLE_NEWS></Data>");
        //        }

        //    }
        //}

        //private void CheckBox_News_Checked(object sender, RoutedEventArgs e)
        //{
        //    if ((bool)this.CheckBox_Audio.IsChecked || (bool)this.CheckBox_Photo.IsChecked ||
        //        (bool)this.CheckBox_Doc.IsChecked || (bool)this.CheckBox_Video.IsChecked)
        //    {
        //        if ((bool)this.CheckBox_News.IsChecked)
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>Y</FSTABLE_MAM><FSTABLE_NEWS>Y</FSTABLE_NEWS></Data>");
        //        }
        //        else
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>Y</FSTABLE_MAM><FSTABLE_NEWS>N</FSTABLE_NEWS></Data>");
        //        }
        //    }
        //    else
        //    {
        //        if ((bool)this.CheckBox_News.IsChecked)
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>N</FSTABLE_MAM><FSTABLE_NEWS>Y</FSTABLE_NEWS></Data>");
        //        }
        //        else
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>N</FSTABLE_MAM><FSTABLE_NEWS>N</FSTABLE_NEWS></Data>");
        //        }

        //    }
        //}

        private string GetIndex(string index, string begYear, string endYear)
        {
            string _returnIndex = "";
            string[] _index = index.Split(';');
            string[] _year = { "2000", "2001", "2006", "2011", "2016" };
            int _yearDiff = Int32.Parse(endYear) - Int32.Parse(begYear);
            int _startYear = 0;
            int _endYear = 0;
            if (Int32.Parse(begYear) <= 2000)
            {
                _startYear = 0;
            }
            else if (Int32.Parse(begYear) >= 2001 && Int32.Parse(begYear) <= 2005)
            {
                _startYear = 1;
            }
            else if (Int32.Parse(begYear) >= 2006 && Int32.Parse(begYear) <= 2010)
            {
                _startYear = 2;
            }
            else if (Int32.Parse(begYear) >= 2011 && Int32.Parse(begYear) <= 2016)
            {
                _startYear = 3;
            }
            else if (Int32.Parse(begYear) >= 2016 && Int32.Parse(begYear) <= 2020)
            {
                _startYear = 4;
            }

            if (Int32.Parse(endYear) <= 2000)
            {
                _endYear = 0;
            }
            else if (Int32.Parse(endYear) >= 2001 && Int32.Parse(endYear) <= 2005)
            {
                _endYear = 1;
            }
            else if (Int32.Parse(endYear) >= 2006 && Int32.Parse(endYear) <= 2010)
            {
                _endYear = 2;
            }
            else if (Int32.Parse(endYear) >= 2011 && Int32.Parse(endYear) <= 2016)
            {
                _endYear = 3;
            }
            else if (Int32.Parse(endYear) >= 2016 && Int32.Parse(endYear) <= 2020)
            {
                _endYear = 4;
            }

            int _yearCount = (_yearDiff / 5) + 1;
            for (int i = 0; i <= _index.Length - 1; i++)
            {
                for (int j = _startYear; j <= _endYear; j++)
                {
                    _returnIndex += _index[i] + "_" + _year[j] + ",";
                }
            }

            if (_returnIndex != "")
            {
                _returnIndex = _returnIndex.Substring(0, _returnIndex.Length - 1);
            }

            return _returnIndex;
        }

        //private void CheckBox_Video_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        //{
        //    // TODO: 在此新增事件處理常式執行項目。
        //    if ((bool)this.CheckBox_Audio.IsChecked || (bool)this.CheckBox_Photo.IsChecked ||
        //        (bool)this.CheckBox_Doc.IsChecked || (bool)this.CheckBox_Video.IsChecked)
        //    {
        //        if ((bool)this.CheckBox_News.IsChecked)
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>Y</FSTABLE_MAM><FSTABLE_NEWS>Y</FSTABLE_NEWS></Data>");
        //        }
        //        else
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>Y</FSTABLE_MAM><FSTABLE_NEWS>N</FSTABLE_NEWS></Data>");
        //        }
        //    }
        //    else
        //    {
        //        if ((bool)this.CheckBox_News.IsChecked)
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>N</FSTABLE_MAM><FSTABLE_NEWS>Y</FSTABLE_NEWS></Data>");
        //        }
        //        else
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>N</FSTABLE_MAM><FSTABLE_NEWS>N</FSTABLE_NEWS></Data>");
        //        }

        //    }
        //}

        //private void CheckBox_Audio_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        //{
        //    // TODO: 在此新增事件處理常式執行項目。
        //    if ((bool)this.CheckBox_Audio.IsChecked || (bool)this.CheckBox_Photo.IsChecked ||
        //        (bool)this.CheckBox_Doc.IsChecked || (bool)this.CheckBox_Video.IsChecked)
        //    {
        //        if ((bool)this.CheckBox_News.IsChecked)
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>Y</FSTABLE_MAM><FSTABLE_NEWS>Y</FSTABLE_NEWS></Data>");
        //        }
        //        else
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>Y</FSTABLE_MAM><FSTABLE_NEWS>N</FSTABLE_NEWS></Data>");
        //        }
        //    }
        //    else
        //    {
        //        if ((bool)this.CheckBox_News.IsChecked)
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>N</FSTABLE_MAM><FSTABLE_NEWS>Y</FSTABLE_NEWS></Data>");
        //        }
        //        else
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>N</FSTABLE_MAM><FSTABLE_NEWS>N</FSTABLE_NEWS></Data>");
        //        }

        //    }
        //}

        //private void CheckBox_Photo_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        //{
        //    // TODO: 在此新增事件處理常式執行項目。
        //    if ((bool)this.CheckBox_Audio.IsChecked || (bool)this.CheckBox_Photo.IsChecked ||
        //        (bool)this.CheckBox_Doc.IsChecked || (bool)this.CheckBox_Video.IsChecked)
        //    {
        //        if ((bool)this.CheckBox_News.IsChecked)
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>Y</FSTABLE_MAM><FSTABLE_NEWS>Y</FSTABLE_NEWS></Data>");
        //        }
        //        else
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>Y</FSTABLE_MAM><FSTABLE_NEWS>N</FSTABLE_NEWS></Data>");
        //        }
        //    }
        //    else
        //    {
        //        if ((bool)this.CheckBox_News.IsChecked)
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>N</FSTABLE_MAM><FSTABLE_NEWS>Y</FSTABLE_NEWS></Data>");
        //        }
        //        else
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>N</FSTABLE_MAM><FSTABLE_NEWS>N</FSTABLE_NEWS></Data>");
        //        }

        //    }
        //}

        //private void CheckBox_Doc_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        //{
        //    // TODO: 在此新增事件處理常式執行項目。
        //    if ((bool)this.CheckBox_Audio.IsChecked || (bool)this.CheckBox_Photo.IsChecked ||
        //        (bool)this.CheckBox_Doc.IsChecked || (bool)this.CheckBox_Video.IsChecked)
        //    {
        //        if ((bool)this.CheckBox_News.IsChecked)
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>Y</FSTABLE_MAM><FSTABLE_NEWS>Y</FSTABLE_NEWS></Data>");
        //        }
        //        else
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>Y</FSTABLE_MAM><FSTABLE_NEWS>N</FSTABLE_NEWS></Data>");
        //        }
        //    }
        //    else
        //    {
        //        if ((bool)this.CheckBox_News.IsChecked)
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>N</FSTABLE_MAM><FSTABLE_NEWS>Y</FSTABLE_NEWS></Data>");
        //        }
        //        else
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>N</FSTABLE_MAM><FSTABLE_NEWS>N</FSTABLE_NEWS></Data>");
        //        }

        //    }
        //}

        //private void CheckBox_News_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        //{
        //    // TODO: 在此新增事件處理常式執行項目。
        //    if ((bool)this.CheckBox_Audio.IsChecked || (bool)this.CheckBox_Photo.IsChecked ||
        //        (bool)this.CheckBox_Doc.IsChecked || (bool)this.CheckBox_Video.IsChecked)
        //    {
        //        if ((bool)this.CheckBox_News.IsChecked)
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>Y</FSTABLE_MAM><FSTABLE_NEWS>Y</FSTABLE_NEWS></Data>");
        //        }
        //        else
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>Y</FSTABLE_MAM><FSTABLE_NEWS>N</FSTABLE_NEWS></Data>");
        //        }
        //    }
        //    else
        //    {
        //        if ((bool)this.CheckBox_News.IsChecked)
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>N</FSTABLE_MAM><FSTABLE_NEWS>Y</FSTABLE_NEWS></Data>");
        //        }
        //        else
        //        {
        //            searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>N</FSTABLE_MAM><FSTABLE_NEWS>N</FSTABLE_NEWS></Data>");
        //        }

        //    }
        //}

        private void Grid_Search_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
            if (e.Key == Key.Enter)
            {
                DoSearch();
            }
        }

        private void CheckBox_Video_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
            if (this.CheckBox_Video.IsChecked == true)
            {
                if (this.fsINDEX_TEMP_LIST.FirstOrDefault(s => s == "MAM") == null)
                {
                    this.fsINDEX_TEMP_LIST.Add("MAM");
                    //判斷是否有新聞
                    if (this.fsINDEX_TEMP_LIST.FirstOrDefault(s => s == "NEWS") != null) 
                        searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>Y</FSTABLE_MAM><FSTABLE_NEWS>Y</FSTABLE_NEWS></Data>");
                    else
                        searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>Y</FSTABLE_MAM><FSTABLE_NEWS>N</FSTABLE_NEWS></Data>");
                }
            }
            else
            {
                if (this.CheckBox_Audio.IsChecked == false && this.CheckBox_Photo.IsChecked == false && this.CheckBox_Doc.IsChecked == false)
                {
                    this.fsINDEX_TEMP_LIST.Remove("MAM");
                    if (this.fsINDEX_TEMP_LIST.FirstOrDefault(s => s == "NEWS") != null)
                        searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>N</FSTABLE_MAM><FSTABLE_NEWS>Y</FSTABLE_NEWS></Data>");
                    else
                        searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>N</FSTABLE_MAM><FSTABLE_NEWS>N</FSTABLE_NEWS></Data>");
                }
            }

        }

        private void CheckBox_Audio_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
            if (this.CheckBox_Audio.IsChecked == true)
            {
                if (this.fsINDEX_TEMP_LIST.FirstOrDefault(s => s == "MAM") == null)
                {
                    this.fsINDEX_TEMP_LIST.Add("MAM");
                    //判斷是否有新聞
                    if (this.fsINDEX_TEMP_LIST.FirstOrDefault(s => s == "NEWS") != null)
                        searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>Y</FSTABLE_MAM><FSTABLE_NEWS>Y</FSTABLE_NEWS></Data>");
                    else
                        searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>Y</FSTABLE_MAM><FSTABLE_NEWS>N</FSTABLE_NEWS></Data>");
                }
            }
            else
            {
                if (this.CheckBox_Video.IsChecked == false && this.CheckBox_Photo.IsChecked == false && this.CheckBox_Doc.IsChecked == false)
                {
                    this.fsINDEX_TEMP_LIST.Remove("MAM");
                    if (this.fsINDEX_TEMP_LIST.FirstOrDefault(s => s == "NEWS") != null)
                        searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>N</FSTABLE_MAM><FSTABLE_NEWS>Y</FSTABLE_NEWS></Data>");
                    else
                        searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>N</FSTABLE_MAM><FSTABLE_NEWS>N</FSTABLE_NEWS></Data>");
                }
            }
        }

        private void CheckBox_Photo_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
            if (this.CheckBox_Photo.IsChecked == true)
            {
                if (this.fsINDEX_TEMP_LIST.FirstOrDefault(s => s == "MAM") == null)
                {
                    this.fsINDEX_TEMP_LIST.Add("MAM");
                    //判斷是否有新聞
                    if (this.fsINDEX_TEMP_LIST.FirstOrDefault(s => s == "NEWS") != null)
                        searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>Y</FSTABLE_MAM><FSTABLE_NEWS>Y</FSTABLE_NEWS></Data>");
                    else
                        searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>Y</FSTABLE_MAM><FSTABLE_NEWS>N</FSTABLE_NEWS></Data>");
                }
            }
            else
            {
                if (this.CheckBox_Video.IsChecked == false && this.CheckBox_Audio.IsChecked == false && this.CheckBox_Doc.IsChecked == false)
                {
                    this.fsINDEX_TEMP_LIST.Remove("MAM");
                    if (this.fsINDEX_TEMP_LIST.FirstOrDefault(s => s == "NEWS") != null)
                        searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>N</FSTABLE_MAM><FSTABLE_NEWS>Y</FSTABLE_NEWS></Data>");
                    else
                        searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>N</FSTABLE_MAM><FSTABLE_NEWS>N</FSTABLE_NEWS></Data>");
                }
            }
        }

        private void CheckBox_Doc_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
            if (this.CheckBox_Doc.IsChecked == true)
            {
                if (this.fsINDEX_TEMP_LIST.FirstOrDefault(s => s == "MAM") == null)
                {
                    this.fsINDEX_TEMP_LIST.Add("MAM");
                    //判斷是否有新聞
                    if (this.fsINDEX_TEMP_LIST.FirstOrDefault(s => s == "NEWS") != null)
                        searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>Y</FSTABLE_MAM><FSTABLE_NEWS>Y</FSTABLE_NEWS></Data>");
                    else
                        searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>Y</FSTABLE_MAM><FSTABLE_NEWS>N</FSTABLE_NEWS></Data>");
                }

            }
            else
            {
                if (this.CheckBox_Video.IsChecked == false && this.CheckBox_Audio.IsChecked == false && this.CheckBox_Photo.IsChecked == false)
                {
                    this.fsINDEX_TEMP_LIST.Remove("MAM");
                    searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>N</FSTABLE_MAM><FSTABLE_NEWS>Y</FSTABLE_NEWS></Data>");
                }
            }
        }

        private void CheckBox_News_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
            if (this.CheckBox_News.IsChecked == true)
            {
                this.fsINDEX_TEMP_LIST.Add("NEWS");
                if (this.fsINDEX_TEMP_LIST.FirstOrDefault(s => s == "MAM") == null)
                    searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>N</FSTABLE_MAM><FSTABLE_NEWS>Y</FSTABLE_NEWS></Data>");
                else
                    searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>Y</FSTABLE_MAM><FSTABLE_NEWS>Y</FSTABLE_NEWS></Data>");
            }
            else
            {
                this.fsINDEX_TEMP_LIST.Remove("NEWS");
                if (this.fsINDEX_TEMP_LIST.FirstOrDefault(s => s == "MAM") == null)
                    searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>N</FSTABLE_MAM><FSTABLE_NEWS>N</FSTABLE_NEWS></Data>");
                else
                    searchDb.GetSearchColumnByindexAsync("<Data><FSTABLE_MAM>Y</FSTABLE_MAM><FSTABLE_NEWS>N</FSTABLE_NEWS></Data>");
            }

        }


    }
}
