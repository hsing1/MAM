﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSBookingFunction;
using SearchClassLibrary;
using System.Xml;
using System.Xml.Linq;
using PTS_MAM3.Common;
using PTS_MAM3.WSTSM_GetFileInfo;
using System.Windows.Media.Imaging;
using PTS_MAM3.WCFSearchDataBaseFunction;

namespace PTS_MAM3.SRH
{
    public partial class SRH126 : UserControl
    {

        private string _bookingNo;
        private List<BookingClass> bookingList = new List<BookingClass>();

        private string _fsfileno;
        private string _fscategory;
        private string _fnno;
        private string _fsguid;
        private bool _fbhas_begin_timecode;
        private string _fsfile_httpPath;

        private string _fsarc_type;

        private long _start_timecode;
        private long _begin_timecode;

        //記錄目前timecode
        private long _start_timecode_temp;
        private long _begin_timecode_temp;

        private string _booking_type;
        private PTS_MAM3.MainFrame parentFrame;

        //建立Video物件
        PreviewPlayer.SLVideoPlayer player = new PreviewPlayer.SLVideoPlayer("", 0);

        //記錄要刪除的物件
        private List<string> fsSELECT_FNNOs = new List<string>();

        public SRH126(string booking_type, PTS_MAM3.MainFrame mainframe)
        {
            InitializeComponent();

            parentFrame = mainframe;
            _booking_type = booking_type;
            //取得預借清單List
            this.Grid_Mark.Visibility = System.Windows.Visibility.Collapsed;
            PreBookingListBind();
        }

        private void Register()
        {

        }

        #region 完成事件


        #endregion

        public void PreBookingListBind()
        {
            string _fsFILE_CATEGORY = string.Empty;
            if (this.RadioButton_News.IsChecked == true) _fsFILE_CATEGORY = "5";

            WSBookingFunctionSoapClient bookDb = new WSBookingFunctionSoapClient();
            bookDb.GetPreBookingListCompleted += (s, args) =>
            {
                if (args.Error != null)
                {
                    return;
                }
                if (string.IsNullOrEmpty(args.Result))
                {
                    return;
                }

                XDocument xmlDocResult = XDocument.Parse(args.Result);

                var paras = from para in xmlDocResult.Descendants("Data")
                            select new PreBookingClass
                            {
                                FNNO = para.Element("FNNO").Value,
                                FSFILE_NO = para.Element("FSFILE_NO").Value,
                                FDPREBOOKING_DATE = para.Element("FDBOOKING_DATE").Value,
                                FSBEG_TIMECODE = para.Element("FSBEG_TIMECODE").Value,
                                FSEND_TIMECODE = para.Element("FSEND_TIMECODE").Value,
                                FSFILE_TYPE = para.Element("FSFILE_TYPE").Value,
                                FSFILE_CATEGORY = para.Element("FSFILE_CATEGORY").Value,
                                FNLOCATION = para.Element("FNLOCATION").Value,
                                FSIMAGEPATH = GetFileCategoryImage(para.Element("FSFILE_CATEGORY").Value),
                                FSFILENO_CATEGORY = para.Element("FSFILE_NO").Value + ";" + para.Element("FSFILE_CATEGORY").Value +
                                                ";" + para.Element("FNNO").Value + ";" + para.Element("FSSTART_TIMECODE").Value +
                                                ";" + para.Element("FSBEG_TIMECODE").Value + ";" + para.Element("FSGUID").Value,
                                FSGUID = para.Element("FSGUID").Value,
                                FSPROG_NAME = para.Element("FSPROG_NAME").Value,
                                _fsarc_type = para.Element("FSARC_TYPE").Value
                            };

                this.DataGrid_PreBooking_List.SelectedIndex = -1;
                this.DataGrid_PreBooking_List.ItemsSource = null;
                this.DataGrid_PreBooking_List.ItemsSource = paras;
                this.lblMESSAGE.Content = "";
                //this.TextBlock_Title.Text = "";

                //_fsfileno = "";
                //_fnno = "";
                //_fscategory = "";
                //_start_timecode = 0;
                //this.Grid_Video.Visibility = System.Windows.Visibility.Collapsed;
                //this.Grid_Mark.Visibility = System.Windows.Visibility.Collapsed;
            };

            bookDb.GetPreBookingListAsync("<Data><FSUSER_ID>" + UserClass.userData.FSUSER_ID + "</FSUSER_ID><FCBOOKING_TYPE>" + _booking_type + "</FCBOOKING_TYPE><FSFILE_CATEGORY>" + _fsFILE_CATEGORY + "</FSFILE_CATEGORY></Data>");
        }

        

        //依照不同類型，顯示不同圖片
        private string GetFileCategoryImage(string category)
        {
            if (category == "1")
            {
                //return "/PTS_MAM3;component/Images/video16_16.png";
                return "../Images/video16_16.png";
            }
            else if (category == "2")
            {
                //return "/PTS_MAM3;component/Images/audio16_16.png";
                return "../Images/audio16_16.png";
            }
            else if (category == "3")
            {
                //return "/PTS_MAM3;component/Images/photo16_16.png";
                return "../Images/photo16_16.png";
            }
            else if (category == "4")
            {
                //return "/PTS_MAM3;component/Images/txt16_16.png";
                return "../Images/txt16_16.png";
            }
            else if (category == "5")
            {
                //return "/PTS_MAM3;component/Images/news16_16.png";
                return "../Images/news16_16.png";
            }
            else
            {
                return "";
            }
        }

        //依照檔案位置，顯示不同圖片
        private string GetFileLocationImage(string location)
        {
            if (location == "1")
            {
                return "/PTS_MAM3;component/Images/HD.png";
            }
            else if (location == "2")
            {
                return "/PTS_MAM3;component/Images/upload.png";
            }
            else if (location == "3")
            {
                return "/PTS_MAM3;component/Images/down.png";
            }
            else
            {
                return "";
            }
        }


        private void PreViewContent(string category, string fileno,string fsARC_TYPE)
        {
            this.Grid_Video.Children.Clear();
            this.Grid_Mark.Visibility = System.Windows.Visibility.Collapsed;
            if (category == "1")
            {
                //影
                //取得路徑
                ServiceTSMSoapClient tsm = new ServiceTSMSoapClient();
                tsm.GetHttpPathByFileIDCompleted += (s, args) =>
                {
                    if ((args.Error != null) || String.IsNullOrEmpty(args.Result))
                    {
                        MessageBox.Show("找不到檔案!");
                        this.RadBusyIndicator_Booking.IsBusy = false;
                        return;
                    }
                    _fsfile_httpPath = args.Result;
                    player.HTML_SetURL(args.Result, _start_timecode);
                    //將Video加置Grid
                    this.Grid_Video.Children.Add(player);

                    this.Grid_Mark.Visibility = System.Windows.Visibility.Visible;
                    this.Grid_Video.Visibility = System.Windows.Visibility.Visible;
                    this.RadBusyIndicator_Booking.IsBusy = false;
                    player.HTML_SetFrame(_begin_timecode - _start_timecode > 0 ? _begin_timecode - _start_timecode : 0);

                    //判斷檔案是否為026類型
                    if (fsARC_TYPE == "026")
                    {
                        this.Button_MarkIn.IsEnabled = false;
                        this.Button_MarkOut.IsEnabled = false;
                        this.TextBox_Start_H.IsEnabled = false;
                        this.TextBox_Start_M.IsEnabled = false;
                        this.TextBox_Start_S.IsEnabled = false;
                        this.TextBox_Start_F.IsEnabled = false;
                        this.TextBox_End_H.IsEnabled = false;
                        this.TextBox_End_M.IsEnabled = false;
                        this.TextBox_End_S.IsEnabled = false;
                        this.TextBox_End_F.IsEnabled = false;
                        this.lblMESSAGE.Content = "此檔案僅能調用原始完整檔案\r\n無法進行切割與轉出其它格式";
                    }
                    else
                    {
                        this.Button_MarkIn.IsEnabled = true;
                        this.Button_MarkOut.IsEnabled = true;
                        this.TextBox_Start_H.IsEnabled = true;
                        this.TextBox_Start_M.IsEnabled = true;
                        this.TextBox_Start_S.IsEnabled = true;
                        this.TextBox_Start_F.IsEnabled = true;
                        this.TextBox_End_H.IsEnabled = true;
                        this.TextBox_End_M.IsEnabled = true;
                        this.TextBox_End_S.IsEnabled = true;
                        this.TextBox_End_F.IsEnabled = true;
                        this.lblMESSAGE.Content = "";
                    }

                };

                tsm.GetHttpPathByFileIDAsync(fileno, FileID_MediaType.LowVideo);

            }
            else if (category == "2")
            {
                //音
                this.RadBusyIndicator_Booking.IsBusy = false;
            }
            else if (category == "3")
            {
                //圖
                ServiceTSMSoapClient tsm_photo = new ServiceTSMSoapClient();
                tsm_photo.GetHttpPathByFileIDCompleted += (s, args) =>
                {
                    if ((args.Error != null) || String.IsNullOrEmpty(args.Result))
                    {
                        this.RadBusyIndicator_Booking.IsBusy = false;
                        return;
                    }
                    else
                    {
                        Image img = new Image();
                        img.Width = 320;
                        img.Height = 320;
                        img.Source = new BitmapImage(new Uri(args.Result, UriKind.Absolute));
                        this.Grid_Video.Children.Add(img);
                        this.RadBusyIndicator_Booking.IsBusy = false;
                    }

                };

                tsm_photo.GetHttpPathByFileIDAsync(fileno, FileID_MediaType.Photo);
            }
            else if (category == "4")
            {
                //文
                SearchDataBaseFunctionClient searchDb = new SearchDataBaseFunctionClient();
                searchDb.GetSearchDetailDataCompleted += (s, args) =>
                {
                    if ((args.Error != null) || String.IsNullOrEmpty(args.Result))
                    {
                        this.RadBusyIndicator_Booking.IsBusy = false;
                        return;
                    }
                    else
                    {
                        XDocument xmlDocResult = XDocument.Parse(args.Result.Replace("", ""));
                        if (xmlDocResult.Element("Datas").Value != "")
                        {
                            TextBlock textblock = new TextBlock();
                            textblock.TextWrapping = TextWrapping.Wrap;
                            textblock.Text = (xmlDocResult.Element("Datas").Element("Data").Element("FSCONTENT") != null ? xmlDocResult.Element("Datas").Element("Data").Element("FSCONTENT").Value.ToString() : "");
                            textblock.FontSize = 12;
                            this.Grid_Video.Children.Add(textblock);
                            this.RadBusyIndicator_Booking.IsBusy = false;
                        }
                    }

                };

                searchDb.GetSearchDetailDataAsync("<Data><FSVW_NAME>" + FileCategoryToType(category) + "</FSVW_NAME><FSSYS_ID>" + fileno + "</FSSYS_ID></Data>");
            }
            else if (category == "5")
            {
                //新聞
                WSBookingFunctionSoapClient booking = new WSBookingFunctionSoapClient();
                booking.GetNewsLVPathCompleted += (s, args) =>
                {
                    if ((args.Error != null) || String.IsNullOrEmpty(args.Result))
                    {
                        this.RadBusyIndicator_Booking.IsBusy = false;
                        return;
                    }

                    //取得開始frame
                    booking.GetNewsStartFrameCompleted += (s1, args1) =>
                    {
                        if ((args1.Error != null) || String.IsNullOrEmpty(args1.Result))
                        {
                            this.RadBusyIndicator_Booking.IsBusy = false;
                            return;
                        }

                        XDocument xDoc = XDocument.Parse(args1.Result);

                        player.HTML_SetURL(args.Result, long.Parse(xDoc.Element("Datas").Element("Data").Element("FSTIMECODE1").Value));

                        //將Video加置Grid
                        this.Grid_Video.Children.Add(player);
                        player.HTML_SetFrame(_begin_timecode - _start_timecode > 0 ? _begin_timecode - _start_timecode : 0);
                        this.Grid_Mark.Visibility = System.Windows.Visibility.Visible;
                        this.Grid_Video.Visibility = System.Windows.Visibility.Visible;
                        this.RadBusyIndicator_Booking.IsBusy = false;
                    };

                    booking.GetNewsStartFrameAsync("<Data><FSFILE_NO>" + fileno + "</FSFILE_NO><FNSEQ_NO>1</FNSEQ_NO></Data>");

                };

                booking.GetNewsLVPathAsync(fileno);
            }
        }

        private void Button_Image_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            //MessageBox.Show(((Button)sender).Tag.ToString());
            ClearTimecodeTextBox();
            this.RadBusyIndicator_Booking.IsBusy = true;
            _fsfileno = ((Button)sender).Tag.ToString().Split(';')[0];
            _fscategory = ((Button)sender).Tag.ToString().Split(';')[1];
            _fnno = ((Button)sender).Tag.ToString().Split(';')[2];

            if (this.DataGrid_PreBooking_List.SelectedItem != null)
            {
                _fsarc_type = (this.DataGrid_PreBooking_List.SelectedItem as PreBookingClass)._fsarc_type;
            }


            if (!long.TryParse(((Button)sender).Tag.ToString().Split(';')[3], out _start_timecode))
            {
                _start_timecode = 0;
            }
            if (((Button)sender).Tag.ToString().Split(';')[4] != "")
            {
                //if (_fscategory == "5")
                //{
                //    _begin_timecode = long.Parse(((Button)sender).Tag.ToString().Split(';')[4]);
                //}
                //else
                //{
                //    _begin_timecode = TransferTimecode.timecodetoframe(((Button)sender).Tag.ToString().Split(';')[4]);
                //}
                if (((Button)sender).Tag.ToString().Split(';')[4].IndexOf(":") > -1)
                {
                    _begin_timecode = TransferTimecode.timecodetoframe(((Button)sender).Tag.ToString().Split(';')[4]);
                }
                else
                {
                    _begin_timecode = long.Parse(((Button)sender).Tag.ToString().Split(';')[4]);
                }
                _fbhas_begin_timecode = true;
            }
            else
            {
                _begin_timecode = 0;
                _fbhas_begin_timecode = false;
            }
            _fsguid = ((Button)sender).Tag.ToString().Split(';')[5];

            //player.HTML_Play_Click();
            //抓取檔案路徑與標題
            WSBookingFunctionSoapClient bookDb = new WSBookingFunctionSoapClient();
            bookDb.GetFilePathCompleted += (s, args) =>
            {
                if (args.Error != null || string.IsNullOrEmpty(args.Result))
                {
                    this.RadBusyIndicator_Booking.IsBusy = false;
                    return;
                }

                XDocument xmlDocResult = XDocument.Parse(args.Result);

                this.TextBlock_Title.Text = "標題： " + xmlDocResult.Element("Datas").Element("Data").Element("FSTITLE").Value;

                PreViewContent(_fscategory, _fsfileno, _fsarc_type);
            };

            bookDb.GetFilePathAsync("<Data><FSFILE_CATEGORY>" + ((Button)sender).Tag.ToString().Split(';')[1] + "</FSFILE_CATEGORY><FSFILE_NO>" + ((Button)sender).Tag.ToString().Split(';')[0] + "</FSFILE_NO></Data>");
        }


        private void Button_Prev_Sec_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            player.HTML_Pause_Click();
            player.HTML_Step_Second_Click(-1);
        }

        private void Button_Prev_Frame_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            player.HTML_Pause_Click();
            player.HTML_Step_Click(-1);
        }

        private void Button_Next_Frame_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            player.HTML_Pause_Click();
            player.HTML_Step_Click(1);
        }

        private void Button_Next_Sec_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            player.HTML_Pause_Click();
            player.HTML_Step_Second_Click(1);
        }

        private void Button_Add_PreBooking_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            if (string.IsNullOrEmpty(this.TextBox_Start_H.Text) &&
                string.IsNullOrEmpty(this.TextBox_Start_M.Text) &&
                string.IsNullOrEmpty(this.TextBox_Start_S.Text) &&
                string.IsNullOrEmpty(this.TextBox_Start_F.Text) &&
                string.IsNullOrEmpty(this.TextBox_End_H.Text) &&
                string.IsNullOrEmpty(this.TextBox_End_M.Text) &&
                string.IsNullOrEmpty(this.TextBox_End_S.Text) &&
                string.IsNullOrEmpty(this.TextBox_End_F.Text))
            {

            }
            else if (
                !string.IsNullOrEmpty(this.TextBox_Start_H.Text) &&
                !string.IsNullOrEmpty(this.TextBox_Start_M.Text) &&
                !string.IsNullOrEmpty(this.TextBox_Start_S.Text) &&
                !string.IsNullOrEmpty(this.TextBox_Start_F.Text) &&
                !string.IsNullOrEmpty(this.TextBox_End_H.Text) &&
                !string.IsNullOrEmpty(this.TextBox_End_M.Text) &&
                !string.IsNullOrEmpty(this.TextBox_End_S.Text) &&
                !string.IsNullOrEmpty(this.TextBox_End_F.Text)
                )
            {
                if (SetTimecodeFormat(this.TextBox_Start_H.Text) == "")
                {
                    MessageBox.Show("TimeCode 格式錯誤!");
                    return;
                }
                else
                {
                    this.TextBox_Start_H.Text = SetTimecodeFormat(this.TextBox_Start_H.Text);
                }
                if (SetTimecodeFormat(this.TextBox_Start_M.Text) == "")
                {
                    MessageBox.Show("TimeCode 格式錯誤!");
                    return;
                }
                else
                {
                    this.TextBox_Start_M.Text = SetTimecodeFormat(this.TextBox_Start_M.Text);
                }
                if (SetTimecodeFormat(this.TextBox_Start_S.Text) == "")
                {
                    MessageBox.Show("TimeCode 格式錯誤!");
                    return;
                }
                else
                {
                    this.TextBox_Start_S.Text = SetTimecodeFormat(this.TextBox_Start_S.Text);
                }
                if (SetTimecodeFormat(this.TextBox_Start_F.Text) == "")
                {
                    MessageBox.Show("TimeCode 格式錯誤!");
                    return;
                }
                else
                {
                    this.TextBox_Start_F.Text = SetTimecodeFormat(this.TextBox_Start_F.Text);
                }
                if (SetTimecodeFormat(this.TextBox_End_H.Text) == "")
                {
                    MessageBox.Show("TimeCode 格式錯誤!");
                    return;
                }
                else
                {
                    this.TextBox_End_H.Text = SetTimecodeFormat(this.TextBox_End_H.Text);
                }
                if (SetTimecodeFormat(this.TextBox_End_M.Text) == "")
                {
                    MessageBox.Show("TimeCode 格式錯誤!");
                    return;
                }
                else
                {
                    this.TextBox_End_M.Text = SetTimecodeFormat(this.TextBox_End_M.Text);
                }
                if (SetTimecodeFormat(this.TextBox_End_S.Text) == "")
                {
                    MessageBox.Show("TimeCode 格式錯誤!");
                    return;
                }
                else
                {
                    this.TextBox_End_S.Text = SetTimecodeFormat(this.TextBox_End_S.Text);
                }
                if (SetTimecodeFormat(this.TextBox_End_F.Text) == "")
                {
                    MessageBox.Show("TimeCode 格式錯誤!");
                    return;
                }
                else
                {
                    this.TextBox_End_F.Text = SetTimecodeFormat(this.TextBox_End_F.Text);
                }


                //若開始大於結束
                if (
                    TransferTimecode.timecodetoframe(SetTimecodeFormat(this.TextBox_End_H.Text) + ":"
                                                        + SetTimecodeFormat(this.TextBox_End_M.Text) + ":"
                                                        + SetTimecodeFormat(this.TextBox_End_S.Text) + ":"
                                                        + SetTimecodeFormat(this.TextBox_End_F.Text)) -
                    TransferTimecode.timecodetoframe(SetTimecodeFormat(this.TextBox_Start_H.Text) + ":"
                                                        + SetTimecodeFormat(this.TextBox_Start_M.Text) + ":"
                                                        + SetTimecodeFormat(this.TextBox_Start_S.Text) + ":"
                                                        + SetTimecodeFormat(this.TextBox_Start_F.Text)) < 0
                    )
                {
                    MessageBox.Show("起迄時間錯誤!");
                    return;
                }

                //判斷是否為有效的TimeCode
                string error = "";
                if (!TransferTimecode.check_timecode_format(this.TextBox_Start_H.Text, this.TextBox_Start_M.Text, this.TextBox_Start_S.Text, this.TextBox_Start_F.Text, out error))
                {
                    MessageBox.Show(error);
                    return;
                }
                if (!TransferTimecode.check_timecode_format(this.TextBox_End_H.Text, this.TextBox_End_M.Text, this.TextBox_End_S.Text, this.TextBox_End_F.Text, out error))
                {
                    MessageBox.Show(error);
                    return;
                }

                //判斷
            }
            else
            {
                MessageBox.Show("請選擇或輸入影片起迄點!");
                return;
            }
            if (_fsfileno != "" && _fnno != "" && _fscategory != "")
            {
                player.HTML_Stop_Click();
                AddPreBooking(_fsfileno, _fnno, FileCategoryToType(_fscategory), _start_timecode, _fsguid);
                //MessageBox.Show(_fsfileno + " / " + _fscategory + " / " + _fsseq);
            }
            else
            {
                MessageBox.Show("請選擇要切割的影片!");
                return;
            }
        }

        private void Button_MarkIn_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            if (string.IsNullOrEmpty(_fsfileno))
            {
                MessageBox.Show("請選擇要切割的影片!");
                return;
            }
            //this.TextBox_MarkIn.Text = TransferTimecode.frame2timecode((Int32.Parse(player.HTML_GetCurrentFrame())));
            SetStartTimecodeToTextBox(TransferTimecode.frame2timecode((Int32.Parse(player.HTML_GetCurrentFrame()))).Split(':')[0],
                                      TransferTimecode.frame2timecode((Int32.Parse(player.HTML_GetCurrentFrame()))).Split(':')[1],
                                      TransferTimecode.frame2timecode((Int32.Parse(player.HTML_GetCurrentFrame()))).Split(':')[2],
                                      TransferTimecode.frame2timecode((Int32.Parse(player.HTML_GetCurrentFrame()))).Split(':')[3]);

        }

        private void Button_MarkOut_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            if (string.IsNullOrEmpty(_fsfileno))
            {
                MessageBox.Show("請選擇要切割的影片!");
                return;
            }
            //this.TextBox_MarkOut.Text = TransferTimecode.frame2timecode((Int32.Parse(player.HTML_GetCurrentFrame())));
            SetEndTimecodeToTextBox(TransferTimecode.frame2timecode((Int32.Parse(player.HTML_GetCurrentFrame()))).Split(':')[0],
                                      TransferTimecode.frame2timecode((Int32.Parse(player.HTML_GetCurrentFrame()))).Split(':')[1],
                                      TransferTimecode.frame2timecode((Int32.Parse(player.HTML_GetCurrentFrame()))).Split(':')[2],
                                      TransferTimecode.frame2timecode((Int32.Parse(player.HTML_GetCurrentFrame()))).Split(':')[3]);
        }



        //取得檔案資訊
        private void AddPreBooking(string fsfile_no, string fnno, string fsfile_type, long start_timecode, string guid)
        {

            WSBookingFunctionSoapClient bookDb = new WSBookingFunctionSoapClient();

            bookDb.GetFileInformationCompleted += (s, args) =>
            {
                if (args.Error != null)
                {
                    return;
                }
                if (args.Result == null && args.Result == "")
                {
                    return;
                }

                //取得檔案位置
                bookDb.GetFileLocationCompleted += (s1, args1) =>
                {
                    if (args1.Error != null)
                    {
                        return;
                    }
                    if (args1.Result == null)
                    {
                        return;
                    }
                    string file_location = args1.Result;


                    //加入集合
                    XDocument xmlDocResult = XDocument.Parse(args.Result.ToString());

                    guid = Guid.NewGuid().ToString();

                    //加入參數
                    string parameter = "";
                    parameter += "<Data>";
                    parameter += "<FNNO>" + fnno + "</FNNO>";
                    parameter += "<FSGUID>" + guid + "</FSGUID>";
                    parameter += "<FSUSER_ID>" + UserClass.userData.FSUSER_ID + "</FSUSER_ID>";
                    parameter += "<FDBOOKING_DATE>" + DateTime.Now.ToString("yyyy/MM/dd") + "</FDBOOKING_DATE>";
                    parameter += "<FSFILE_NO>" + fsfile_no + "</FSFILE_NO>";
                    parameter += "<FSSTART_TIMECODE>" + GetFrame(xmlDocResult.Element("Datas").Element("Data").Element("FSFILE_CATEGORY").Value, xmlDocResult.Element("Datas").Element("Data").Element("FSBEG_TIMECODE").Value.Replace(';', ':')) + "</FSSTART_TIMECODE>";
                    if (this.TextBox_Start_H.Text != "")
                    {
                        //檢查開始時間與結束時間是否超過
                        int fnORI_BEG_TIMECODE = int.Parse(GetFrame(xmlDocResult.Element("Datas").Element("Data").Element("FSFILE_CATEGORY").Value, xmlDocResult.Element("Datas").Element("Data").Element("FSBEG_TIMECODE").Value.Replace(';', ':')));//int.Parse(xmlDocResult.Element("Datas").Element("Data").Element("FSBEG_TIMECODE").Value.Replace(';', ':').Replace(":",""));
                        int fnORI_END_TIMECODE = int.Parse(GetFrame(xmlDocResult.Element("Datas").Element("Data").Element("FSFILE_CATEGORY").Value, xmlDocResult.Element("Datas").Element("Data").Element("FSEND_TIMECODE").Value.Replace(';', ':')));//int.Parse(xmlDocResult.Element("Datas").Element("Data").Element("FSEND_TIMECODE").Value.Replace(';', ':').Replace(":", ""));

                        int fnKEYIN_BEG_TIMECODE = int.Parse(GetFrame("1", SetTimecodeFormat(this.TextBox_Start_H.Text) + ":" + SetTimecodeFormat(this.TextBox_Start_M.Text) + ":" + SetTimecodeFormat(this.TextBox_Start_S.Text) + ":" + SetTimecodeFormat(this.TextBox_Start_F.Text)));//int.Parse(SetTimecodeFormat(this.TextBox_Start_H.Text) + SetTimecodeFormat(this.TextBox_Start_M.Text) + SetTimecodeFormat(this.TextBox_Start_S.Text) + SetTimecodeFormat(this.TextBox_Start_F.Text));
                        int fnKEYIN_END_TIMECODE = int.Parse(GetFrame("1", SetTimecodeFormat(this.TextBox_End_H.Text) + ":" + SetTimecodeFormat(this.TextBox_End_M.Text) + ":" + SetTimecodeFormat(this.TextBox_End_S.Text) + ":" + SetTimecodeFormat(this.TextBox_End_F.Text)));//int.Parse(SetTimecodeFormat(this.TextBox_End_H.Text) + SetTimecodeFormat(this.TextBox_End_M.Text) + SetTimecodeFormat(this.TextBox_End_S.Text) + SetTimecodeFormat(this.TextBox_End_F.Text));

                        if (fnKEYIN_BEG_TIMECODE < fnORI_BEG_TIMECODE || fnKEYIN_END_TIMECODE > fnORI_END_TIMECODE)
                        {
                            MessageBox.Show("輸入開始時間小於影片起點或輸入結束時間大於影片迄點!", "訊息", MessageBoxButton.OK);
                            return;
                        }


                        parameter += "<FSBEG_TIMECODE>" + SetTimecodeFormat(this.TextBox_Start_H.Text) + ":"
                                                        + SetTimecodeFormat(this.TextBox_Start_M.Text) + ":"
                                                        + SetTimecodeFormat(this.TextBox_Start_S.Text) + ":"
                                                        + SetTimecodeFormat(this.TextBox_Start_F.Text) +
                                     "</FSBEG_TIMECODE>";
                        parameter += "<FSEND_TIMECODE>" + SetTimecodeFormat(this.TextBox_End_H.Text) + ":"
                                                        + SetTimecodeFormat(this.TextBox_End_M.Text) + ":"
                                                        + SetTimecodeFormat(this.TextBox_End_S.Text) + ":"
                                                        + SetTimecodeFormat(this.TextBox_End_F.Text) +
                                     "</FSEND_TIMECODE>";
                    }
                    else
                    {
                        parameter += "<FSBEG_TIMECODE>" + 
                            (xmlDocResult.Element("Datas").Element("Data").Element("FSBEG_TIMECODE").Value.IndexOf(":") == -1 ? 
                            TransferTimecode.frame2timecode(Int32.Parse(xmlDocResult.Element("Datas").Element("Data").Element("FSBEG_TIMECODE").Value)) : 
                            xmlDocResult.Element("Datas").Element("Data").Element("FSBEG_TIMECODE").Value.Replace(';', ':')) 
                            + "</FSBEG_TIMECODE>";
                            
                        parameter += "<FSEND_TIMECODE>" +
                            (xmlDocResult.Element("Datas").Element("Data").Element("FSEND_TIMECODE").Value.IndexOf(":") == -1 ?
                            TransferTimecode.frame2timecode(Int32.Parse(xmlDocResult.Element("Datas").Element("Data").Element("FSEND_TIMECODE").Value)) :
                            xmlDocResult.Element("Datas").Element("Data").Element("FSEND_TIMECODE").Value.Replace(';', ':'))
                            + "</FSEND_TIMECODE>";
                        //SetStartTimecodeToTextBox(xmlDocResult.Element("Datas").Element("Data").Element("FSBEG_TIMECODE").Value.Replace(';', ':').Split(':')[0],
                        //                          xmlDocResult.Element("Datas").Element("Data").Element("FSBEG_TIMECODE").Value.Replace(';', ':').Split(':')[1],
                        //                          xmlDocResult.Element("Datas").Element("Data").Element("FSBEG_TIMECODE").Value.Replace(';', ':').Split(':')[2],
                        //                          xmlDocResult.Element("Datas").Element("Data").Element("FSBEG_TIMECODE").Value.Replace(';', ':').Split(':')[3]);

                        //SetStartTimecodeToTextBox(xmlDocResult.Element("Datas").Element("Data").Element("FSEND_TIMECODE").Value.Replace(';', ':').Split(':')[0],
                        //                          xmlDocResult.Element("Datas").Element("Data").Element("FSEND_TIMECODE").Value.Replace(';', ':').Split(':')[1],
                        //                          xmlDocResult.Element("Datas").Element("Data").Element("FSEND_TIMECODE").Value.Replace(';', ':').Split(':')[2],
                        //                          xmlDocResult.Element("Datas").Element("Data").Element("FSEND_TIMECODE").Value.Replace(';', ':').Split(':')[3]);

                        //this.TextBox_MarkIn.Text = xmlDocResult.Element("Datas").Element("Data").Element("FSBEG_TIMECODE").Value.Replace(';', ':');
                        //this.TextBox_MarkOut.Text = xmlDocResult.Element("Datas").Element("Data").Element("FSEND_TIMECODE").Value.Replace(';', ':');
                    }
                    parameter += "<FSFILE_TYPE>" + xmlDocResult.Element("Datas").Element("Data").Element("FSFILE_TYPE").Value + "</FSFILE_TYPE>";
                    parameter += "<FSFILE_CATEGORY>" + xmlDocResult.Element("Datas").Element("Data").Element("FSFILE_CATEGORY").Value + "</FSFILE_CATEGORY>";
                    parameter += "<FNLOCATION>" + file_location + "</FNLOCATION>";
                    parameter += "<FCOUT_BUY>" + xmlDocResult.Element("Datas").Element("Data").Element("FCOUT_BUY").Value + "</FCOUT_BUY>";
                    parameter += "<FCBOOKING_TYPE>" + _booking_type + "</FCBOOKING_TYPE>";
                    //parameter += "<FCSUPERVISOR_CHECK>" + xmlDocResult.Element("Datas").Element("Data").Element("FSSUPERVISOR").Value + "</FCSUPERVISOR_CHECK>";
                    //parameter += "<FSSUPERVISOR>" + xmlDocResult.Element("Datas").Element("Data").Element("FSSupervisor_ID").Value + "</FSSUPERVISOR>";

                    if (UserClass.userData.FNGROUP_ID == 63)
                    {
                        parameter += "<FCSUPERVISOR_CHECK>N</FCSUPERVISOR_CHECK>";
                        parameter += "<FSSUPERVISOR></FSSUPERVISOR>";
                    }
                    else
                    {
                        parameter += "<FCSUPERVISOR_CHECK>" + xmlDocResult.Element("Datas").Element("Data").Element("FSSUPERVISOR").Value + "</FCSUPERVISOR_CHECK>";
                        parameter += "<FSSUPERVISOR>" + xmlDocResult.Element("Datas").Element("Data").Element("FSSupervisor_ID").Value + "</FSSUPERVISOR>";
                    }

                    //原來的片長
                    long _fsori_length = 0;
                    if (xmlDocResult.Element("Datas").Element("Data").Element("FSFILE_CATEGORY").Value == "1")
                    {
                        _fsori_length = TransferTimecode.timecodetoframe((xmlDocResult.Element("Datas").Element("Data").Element("FSEND_TIMECODE").Value == "" ? "00:00:00:00" : xmlDocResult.Element("Datas").Element("Data").Element("FSEND_TIMECODE").Value.Replace(';', ':'))) -
                        TransferTimecode.timecodetoframe((xmlDocResult.Element("Datas").Element("Data").Element("FSBEG_TIMECODE").Value == "" ? "00:00:00:00" : xmlDocResult.Element("Datas").Element("Data").Element("FSBEG_TIMECODE").Value.Replace(';', ':')));
                    }
                    else if (xmlDocResult.Element("Datas").Element("Data").Element("FSFILE_CATEGORY").Value == "5")
                    {
                        _fsori_length = long.Parse(xmlDocResult.Element("Datas").Element("Data").Element("FSEND_TIMECODE").Value) -
                                        long.Parse(xmlDocResult.Element("Datas").Element("Data").Element("FSBEG_TIMECODE").Value);
                    }

                    //目前選擇片長
                    long _fsnow_length = 0;
                    if (string.IsNullOrEmpty(this.TextBox_Start_H.Text) &&
                        string.IsNullOrEmpty(this.TextBox_Start_M.Text) &&
                        string.IsNullOrEmpty(this.TextBox_Start_S.Text) &&
                        string.IsNullOrEmpty(this.TextBox_Start_F.Text) &&
                        string.IsNullOrEmpty(this.TextBox_End_H.Text) &&
                        string.IsNullOrEmpty(this.TextBox_End_M.Text) &&
                        string.IsNullOrEmpty(this.TextBox_End_S.Text) &&
                        string.IsNullOrEmpty(this.TextBox_End_F.Text))
                    {
                        _fsnow_length = _fsori_length;
                    }
                    else
                    {
                        _fsnow_length = TransferTimecode.timecodetoframe(
                                                          SetTimecodeFormat(this.TextBox_End_H.Text) + ":"
                                                        + SetTimecodeFormat(this.TextBox_End_M.Text) + ":"
                                                        + SetTimecodeFormat(this.TextBox_End_S.Text) + ":"
                                                        + SetTimecodeFormat(this.TextBox_End_F.Text)) -
                                        TransferTimecode.timecodetoframe(
                                                          SetTimecodeFormat(this.TextBox_Start_H.Text) + ":"
                                                        + SetTimecodeFormat(this.TextBox_Start_M.Text) + ":"
                                                        + SetTimecodeFormat(this.TextBox_Start_S.Text) + ":"
                                                        + SetTimecodeFormat(this.TextBox_Start_F.Text));
                    }
                    if (_fsnow_length == _fsori_length)
                    {
                        parameter += "<FCALL>Y</FCALL>";
                    }
                    else
                    {
                        parameter += "<FCALL>N</FCALL>";
                    }
                    parameter += "</Data>";


                    WSBookingFunctionSoapClient booking = new WSBookingFunctionSoapClient();
                    booking.InsertPreBooking2Completed += (s2, args2) =>
                    {
                        if (args2.Error != null)
                        {
                            return;
                        }

                        if (args2.Result)
                        {
                            string _fsreal_begin_timecode = "";
                            string _fsduration = "";
                            string _begin_timecode = "";

                            if (string.IsNullOrEmpty(this.TextBox_Start_H.Text) &&
                                   string.IsNullOrEmpty(this.TextBox_Start_M.Text) &&
                                   string.IsNullOrEmpty(this.TextBox_Start_S.Text) &&
                                   string.IsNullOrEmpty(this.TextBox_Start_F.Text) &&
                                   string.IsNullOrEmpty(this.TextBox_End_H.Text) &&
                                   string.IsNullOrEmpty(this.TextBox_End_M.Text) &&
                                   string.IsNullOrEmpty(this.TextBox_End_S.Text) &&
                                   string.IsNullOrEmpty(this.TextBox_End_F.Text))
                            {
                                _fsreal_begin_timecode = "00:00:00:00";
                                _fsduration = TransferTimecode.frame2timecode((int)_fsori_length).Replace(';', ':');
                            }
                            else
                            {
                                if (xmlDocResult.Element("Datas").Element("Data").Element("FSBEG_TIMECODE").Value == "")
                                {
                                    _begin_timecode = "00:00:00:00";
                                }
                                else if (xmlDocResult.Element("Datas").Element("Data").Element("FSBEG_TIMECODE").Value.IndexOf(":") == -1)
                                {
                                    _begin_timecode = TransferTimecode.frame2timecode(Int32.Parse(xmlDocResult.Element("Datas").Element("Data").Element("FSBEG_TIMECODE").Value));
                                }
                                else
                                {
                                    _begin_timecode = xmlDocResult.Element("Datas").Element("Data").Element("FSBEG_TIMECODE").Value.Replace(';', ':');
                                }
                                
                                _fsreal_begin_timecode = TransferTimecode.frame2timecode(TransferTimecode.timecodetoframe(
                                                          SetTimecodeFormat(this.TextBox_Start_H.Text) + ":"
                                                        + SetTimecodeFormat(this.TextBox_Start_M.Text) + ":"
                                                        + SetTimecodeFormat(this.TextBox_Start_S.Text) + ":"
                                                        + SetTimecodeFormat(this.TextBox_Start_F.Text)) -
                                                            TransferTimecode.timecodetoframe(_begin_timecode));

                                _fsduration = TransferTimecode.frame2timecode(
                                                            TransferTimecode.timecodetoframe(SetTimecodeFormat(this.TextBox_End_H.Text) + ":"
                                                            + SetTimecodeFormat(this.TextBox_End_M.Text) + ":"
                                                            + SetTimecodeFormat(this.TextBox_End_S.Text) + ":"
                                                            + SetTimecodeFormat(this.TextBox_End_F.Text)) -
                                                            TransferTimecode.timecodetoframe(SetTimecodeFormat(this.TextBox_Start_H.Text) + ":"
                                                            + SetTimecodeFormat(this.TextBox_Start_M.Text) + ":"
                                                            + SetTimecodeFormat(this.TextBox_Start_S.Text) + ":"
                                                            + SetTimecodeFormat(this.TextBox_Start_F.Text)));
                            }


                            string _fsreal_beginframe = (Int32.Parse(_fsreal_begin_timecode.Substring(_fsreal_begin_timecode.Length - 2, 2)) * 0.033366700033667).ToString();
                            string _fsreal_durationframe = (Int32.Parse(_fsduration.Substring(_fsduration.Length - 2, 2)) * 0.033366700033667).ToString();

                            _fsreal_begin_timecode = _fsreal_begin_timecode.Substring(0, _fsreal_begin_timecode.Length - 3) + _fsreal_beginframe.Replace("0", "");
                            _fsduration = _fsduration.Substring(0, _fsduration.Length - 3) + _fsreal_durationframe.Replace("0", "");

                            booking.WriteAsxFileCompleted += (s3, args3) =>
                            {
                                MessageBox.Show("加入成功!");
                                ClearTimecodeTextBox();
                                PreBookingListBind();
                            };
                            booking.WriteAsxFileAsync(guid, _fsreal_begin_timecode, _fsduration, _fsfile_httpPath);

                        }

                    };

                    booking.InsertPreBooking2Async(parameter, UserClass.userData.FSUSER_ID);

                };

                bookDb.GetFileLocationAsync(fsfile_no, fsfile_type);

            };

            bookDb.GetFileInformationAsync("<Data><FSFILE_NO>" + fsfile_no + "</FSFILE_NO><FSVW_NAME>" + fsfile_type + "</FSVW_NAME></Data>");
        }


        private string GetFrame(string category, string timecode)
        {
            if (timecode != "")
            {
                if (category == "1")
                {
                    return TransferTimecode.timecodetoframe(timecode).ToString();
                }
                else if (category == "5")
                {
                    return timecode;
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
        }

        private string FileCategoryToType(string category)
        {
            if (category == "1")
            {
                return "VW_SEARCH_VIDEO";
            }
            else if (category == "2")
            {
                return "VW_SEARCH_AUDIO";
            }
            else if (category == "3")
            {
                return "VW_SEARCH_PHOTO";
            }
            else if (category == "4")
            {
                return "VW_SEARCH_DOC";
            }
            else if (category == "5")
            {
                return "VW_NEWS";
            }
            else
            {
                return "";
            }
        }

        private void Button_Delete_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            //// TODO: 在此新增事件處理常式執行項目。

            WSBookingFunctionSoapClient bookDb = new WSBookingFunctionSoapClient();
            bookDb.DeletePreBookingCompleted += new EventHandler<DeletePreBookingCompletedEventArgs>(bookDb_DeletePreBookingCompleted);

            if (fsSELECT_FNNOs != null && fsSELECT_FNNOs.Count > 0)
            {
                if (MessageBox.Show("確定要刪除清單項目?", "訊息", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                {
                    string fsNOs = string.Empty;
                    foreach (string fsNO in fsSELECT_FNNOs)
                    {
                        fsNOs += fsNO + ",";
                    }
                    bookDb.DeletePreBookingAsync("<Data><FNNO>" + fsNOs + "</FNNO></Data>", UserClass.userData.FSUSER_ID);
                }
            }
            else
            {
                MessageBox.Show("請先勾選要刪除的項目", "訊息", MessageBoxButton.OK);
            }
            //_fsfileno = ((Button)sender).Tag.ToString().Split(';')[0];
            //_fscategory = ((Button)sender).Tag.ToString().Split(';')[1];
            //_fnno = ((Button)sender).Tag.ToString().Split(';')[2];

            //chWndMessageBox confirm = new chWndMessageBox("刪除預借清單項目", "確定要刪除此清單項目?", 0);
            //confirm.Show();
            //confirm.Closing += (s, args) =>
            //{
            //    if (confirm.DialogResult == true)
            //    {
            //        WSBookingFunctionSoapClient bookDb = new WSBookingFunctionSoapClient();
            //        bookDb.DeletePreBookingCompleted += (s1, args1) =>
            //        {
            //            if (args1.Error != null)
            //            {
            //                return;
            //            }

            //            if (args1.Result)
            //            {
            //                MessageBox.Show("刪除成功!");
            //                _fsfileno = "";
            //                _fnno = "";
            //                _fscategory = "";
            //                _start_timecode = 0;
            //                this.TextBlock_Title.Text = "";
            //                //this.TextBox_MarkIn.Text = "";
            //                //this.TextBox_MarkOut.Text = "";
            //                this.Grid_Mark.Visibility = System.Windows.Visibility.Collapsed;
            //                this.Grid_Video.Visibility = System.Windows.Visibility.Collapsed;
            //                PreBookingListBind();
            //            }

            //        };

            //        bookDb.DeletePreBookingAsync("<Data><FNNO>" + _fnno + "</FNNO></Data>", UserClass.userData.FSUSER_ID);
            //    }
            //};
        }

        void bookDb_DeletePreBookingCompleted(object sender, DeletePreBookingCompletedEventArgs e)
        {
            if (e.Error != null || e.Result == false)
            {
                MessageBox.Show("刪除失敗!","訊息",MessageBoxButton.OK);
            }
            else
            {
                MessageBox.Show("刪除成功!","訊息",MessageBoxButton.OK);
                fsSELECT_FNNOs.Clear();
                _fsfileno = "";
                _fnno = "";
                _fscategory = "";
                _start_timecode = 0;
                this.TextBlock_Title.Text = "";
                //this.TextBox_MarkIn.Text = "";
                //this.TextBox_MarkOut.Text = "";
                this.Grid_Mark.Visibility = System.Windows.Visibility.Collapsed;
                this.Grid_Video.Visibility = System.Windows.Visibility.Collapsed;
                PreBookingListBind();
            }
        }

        private void Button_Send_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            //檢查是否有檔案

            string _fcFILE_CATEGORY = string.Empty;
            if (this.RadioButton_News.IsChecked == true) _fcFILE_CATEGORY = this.RadioButton_News.Tag.ToString();
            if (this.RadioButton_Program.IsChecked == true) _fcFILE_CATEGORY = this.RadioButton_Program.Tag.ToString();

            if (this.DataGrid_PreBooking_List.ItemsSource == null || this.DataGrid_PreBooking_List.ItemsSource.OfType<PreBookingClass>().Count() == 0)
            {
                MessageBox.Show("預借清單中沒有檔案!");
                return;
            }

            WSBookingFunctionSoapClient bookDb = new WSBookingFunctionSoapClient();

            //檢查是否有未切割的檔案
            bookDb.CheckFileMarkCompleted += (s0, args0) =>
            {
                if (args0.Error != null)
                {
                    return;
                }
                if (args0.Result == null)
                {
                    return;
                }

                XDocument xmlDocResult = XDocument.Parse(args0.Result.ToString());
                if (xmlDocResult.Element("Datas").Element("Data").Element("FNCOUNT").Value != "0")
                {
                    MessageBox.Show("尚有" + xmlDocResult.Element("Datas").Element("Data").Element("FNCOUNT").Value + "則影片尚未設定起迄點!");
                    return;
                }
                else
                {

                    //取得調用清單編號
                    bookDb.GetBookingNoCompleted += (s1, args1) =>
                    {
                        if (args1.Error != null)
                        {
                            return;
                        }
                        if (args1.Result == null)
                        {
                            return;
                        }

                        xmlDocResult = XDocument.Parse(args1.Result.ToString());
                        _bookingNo = xmlDocResult.Element("Datas").Element("Data").Element("FSNO").Value;

                        string _fsFILE_CATEGORY = string.Empty;
                        if (this.RadioButton_News.IsChecked == true) _fsFILE_CATEGORY = this.RadioButton_News.Tag.ToString();
                        if (this.RadioButton_Program.IsChecked == true) _fsFILE_CATEGORY = this.RadioButton_Program.Tag.ToString();

                        SRH126_01 bookingInfo = new SRH126_01(_bookingNo, _fsFILE_CATEGORY);
                        bookingInfo.Show();

                        bookingInfo.Closing += (s2, args2) =>
                        {
                            if (bookingInfo.DialogResult == true)
                            {
                                if (this.RadioButton_News.IsChecked == true)
                                {
                                    MessageBox.Show("新聞調用清單送出成功!");
                                }
                                else if (this.RadioButton_Program.IsChecked == true)
                                {
                                    MessageBox.Show("節目調用清單送出成功!");
                                }
                                this.Grid_Video.Children.Clear();
                                this.Grid_Mark.Visibility = System.Windows.Visibility.Collapsed;
                                PreBookingListBind();
                            }
                        };

                    };

                    bookDb.GetBookingNoAsync("<Data><FSTYPE>07</FSTYPE><FSNO_DATE>" + DateTime.Now.ToString("yyyyMMdd") +
                                    "</FSNO_DATE><FSNOTE></FSNOTE><FSCREATED_BY></FSCREATED_BY></Data>");

                }
            };

            bookDb.CheckFileMarkAsync("<Data><FSUSER_ID>" + UserClass.userData.FSUSER_ID + "</FSUSER_ID><FCBOOKING_TYPE>" + _booking_type + "</FCBOOKING_TYPE><FSFILE_CATEGORY>" + _fcFILE_CATEGORY + " </FSFILE_CATEGORY></Data>");
        }

        private void SetStartTimecodeToTextBox(string h, string m, string s, string f)
        {
            this.TextBox_Start_H.Text = SetTimecodeFormat(h);
            this.TextBox_Start_M.Text = SetTimecodeFormat(m);
            this.TextBox_Start_S.Text = SetTimecodeFormat(s);
            this.TextBox_Start_F.Text = SetTimecodeFormat(f);
        }

        private void SetEndTimecodeToTextBox(string h, string m, string s, string f)
        {
            this.TextBox_End_H.Text = SetTimecodeFormat(h);
            this.TextBox_End_M.Text = SetTimecodeFormat(m);
            this.TextBox_End_S.Text = SetTimecodeFormat(s);
            this.TextBox_End_F.Text = SetTimecodeFormat(f);
        }

        private void ClearTimecodeTextBox()
        {
            this.TextBox_Start_H.Text = "";
            this.TextBox_Start_M.Text = "";
            this.TextBox_Start_S.Text = "";
            this.TextBox_Start_F.Text = "";
            this.TextBox_End_H.Text = "";
            this.TextBox_End_M.Text = "";
            this.TextBox_End_S.Text = "";
            this.TextBox_End_F.Text = "";
        }

        private string SetTimecodeFormat(string time)
        {
            int _temp_time = 0;
            if (time == "")
            {
                return "00";
            }
            else
            {
                if (Int32.TryParse(time, out _temp_time))
                {
                    if (_temp_time >= 0 && _temp_time < 10)
                    {
                        return "0" + _temp_time.ToString();
                    }
                    else
                    {
                        return _temp_time.ToString();
                    }
                }
                else
                {
                    return "";
                }
            }
        }

        private void RadioButton_Program_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
            this.Grid_Video.Visibility = System.Windows.Visibility.Collapsed;
            this.Grid_Mark.Visibility = System.Windows.Visibility.Collapsed;
            this.Button_Send.Content = "送出節目調用清單";
            PreBookingListBind();
        }

        private void RadioButton_News_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
            this.Grid_Video.Visibility = System.Windows.Visibility.Collapsed;
            this.Grid_Mark.Visibility = System.Windows.Visibility.Collapsed;
            this.Button_Send.Content = "送出新聞調用清單";
            PreBookingListBind();
        }

        private void CheckBox_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
            CheckBox chk = sender as CheckBox;
            if (chk != null)
            {
                if (chk.IsChecked == true)
                {
                    if (fsSELECT_FNNOs.FirstOrDefault<string>(i => i == chk.Tag.ToString().Split(';')[2]) == null)
                    {
                        fsSELECT_FNNOs.Add(chk.Tag.ToString().Split(';')[2]);
                    }
                }
                else
                {
                    if (fsSELECT_FNNOs.FirstOrDefault<string>(i => i.ToString() == chk.Tag.ToString().Split(';')[2]) != null)
                    {
                        fsSELECT_FNNOs.Remove(fsSELECT_FNNOs.FirstOrDefault<string>(i => i == chk.Tag.ToString().Split(';')[2]));
                    }
                }
            }

        }

    }

}
