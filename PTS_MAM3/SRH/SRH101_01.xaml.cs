﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SearchClassLibrary;
using PTS_MAM3.WSBookingFunction;
using System.Xml;
using System.Xml.Linq;
using PTS_MAM3.WSSearchService;
using PTS_MAM3.WSTSM_GetFileInfo;

namespace PTS_MAM3.SRH
{
    public partial class SRH101_01 : ChildWindow
    {
        private string _parameter = "";
        //目前頁次
        private int currentPageIndex = 1;
        //總頁次
        private int totalPageCount = 0;
        //每頁筆數
        private int perPageCount = 10;
        //總筆數
        private int totalCount = 0;
        WSBookingFunctionSoapClient booking = new WSBookingFunctionSoapClient();

        //資料庫查詢後的結果
        List<ProgramBookingResultClass> programbookingList = new List<ProgramBookingResultClass>();

        //預借清單編號
        public string _preBookingNo = "";

        SearchServiceSoapClient searchWS = new SearchServiceSoapClient();
        SearchServiceSoapClient searchWS1 = new SearchServiceSoapClient();


        string fsFILE_NO = string.Empty;
        string fsTYPE = string.Empty;

        public SRH101_01(string parameter)
        {
            InitializeComponent();
            this.RadBusyIndicator_Search.IsBusy = true;
            _parameter = parameter;

            booking.CheckProgIsExpireCompleted += new EventHandler<CheckProgIsExpireCompletedEventArgs>(booking_CheckProgIsExpireCompleted);

            searchWS.SearchCompleted += (s, args) =>
            {
                if (args == null || args.Error != null)
                {
                    this.RadBusyIndicator_Search.IsBusy = false;
                    return;
                }

                if (args.Result.ToString() == "")
                {
                    this.RadBusyIndicator_Search.IsBusy = false;
                    return;
                }

                //過濾字元字串
                XDocument xmlDocResult = XDocument.Parse(args.Result.ToString().Replace("&", "&amp;"));
                XDocument xmlDocParameter = XDocument.Parse(parameter);

                //取得總筆數
                Int32.TryParse(xmlDocResult.Element("Datas").Attribute("count").Value, out totalCount);

                if (totalCount == 0)
                {
                    MessageBox.Show("查無資料");
                    this.RadBusyIndicator_Search.IsBusy = false;
                    this.DialogResult = false;
                    return;
                }

                //取得每頁筆數
                Int32.TryParse(xmlDocParameter.Element("Search").Element("PageSize").Value, out perPageCount);

                if (totalCount % perPageCount != 0)
                {
                    totalPageCount = (totalCount / perPageCount) + 1;
                }
                else
                {
                    totalPageCount = totalCount / perPageCount;
                }

                this.TextBlock_TotalPage.Text = totalPageCount.ToString();
                this.TextBox_PageIndex.Text = currentPageIndex.ToString();

                var paras = from para in xmlDocResult.Descendants("Data")
                            select new ProgramBookingResultClass
                            {
                                FSFILE_NO = para.Element("FSFILE_NO").Value,
                                FSPGMNAME = para.Element("FSPROG_B_PGMNAME").Value,
                                FSPGM_SUBNAME = para.Element("FSPROG_D_PGDENAME").Value,
                                FSFILE_CATEGORY = FileTypeToeCategory(para.Element("FSVWNAME").Value),
                                FNEPISODE = para.Element("FNEPISODE").Value,
                                FSIMAGE_PATH = GetFileCategoryImage(para.Element("FSVWNAME").Value, para.Element("FCFROM").Value, para.Element("FSFILE_NO").Value, para.Element("FSIMAGE_PATH").Value),
                                FSFILENO_CATEGORY = para.Element("FSFILE_NO").Value + ";" + para.Element("FSVWNAME").Value,
                                FCFROM = IsTape(para.Element("FCFROM").Value),
                                FSFILE_TYPE = GetFileTypeChtName(para.Element("FSVWNAME").Value)
                            };


                this.ListBox_Program_Result.SelectedIndex = -1;
                this.RadBusyIndicator_Search.IsBusy = false;
                this.ListBox_Program_Result.ItemsSource = null;
                this.ListBox_Program_Result.ItemsSource = paras;

                this.ScrollViewer_SearchResult.UpdateLayout();
                this.ScrollViewer_SearchResult.ScrollToTop();

            };

            searchWS.SearchAsync(_parameter);


        }

        void booking_CheckProgIsExpireCompleted(object sender, CheckProgIsExpireCompletedEventArgs e)
        {
            if (e.Error != null || e.Result == null)
            {
                MessageBox.Show("檢查檔案所屬節目是否過期錯誤!", "訊息", MessageBoxButton.OK);
            }
            else
            {
                if (!string.IsNullOrEmpty(e.Result))
                {
                    string fsERROR = string.Empty;
                    foreach (string error in e.Result.Substring(0, e.Result.Length - 1).Split(','))
                    {
                        fsERROR += "檔案編號:" + error + "所屬節目已過期，無法調用!\r\n";
                    }
                    MessageBox.Show(fsERROR, "訊息", MessageBoxButton.OK);
                }
                else
                {
                    //加入資料庫
                    GetFileInformation(this.fsFILE_NO, this.fsTYPE);

                }
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void HyperlinkButton_Booking_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            //檢查檔案是否存在

            this.fsFILE_NO = ((HyperlinkButton)sender).Tag.ToString().Split(';')[0];
            this.fsTYPE = ((HyperlinkButton)sender).Tag.ToString().Split(';')[1];

            WSBookingFunctionSoapClient bookDb = new WSBookingFunctionSoapClient();
            bookDb.GetFileExistPreBookingCompleted += (s, args) =>
            {
                if (args.Error != null || string.IsNullOrEmpty(args.Result))
                {
                    return;
                }
                else
                {
                    XDocument xmlDocResult = XDocument.Parse(args.Result.ToString());
                    if (xmlDocResult.Element("Datas").Element("Data").Element("FNCOUNT").Value != "0")
                    {
                        MessageBox.Show("檔案已加入清單中!");
                        return;
                    }
                    else
                    {
                        booking.CheckProgIsExpireAsync(this.fsFILE_NO, this.fsTYPE);
                    }
                }


            };

            string parameter = "";
            parameter += "<Data>";
            parameter += "<FSUSER_ID>" + UserClass.userData.FSUSER_ID + "</FSUSER_ID>";
            parameter += "<FSFILE_NO>" + ((HyperlinkButton)sender).Tag.ToString().Split(';')[0] + "</FSFILE_NO>";
            parameter += "<FCBOOKING_TYPE>2</FCBOOKING_TYPE>";
            parameter += "</Data>";
            bookDb.GetFileExistPreBookingAsync(parameter);
            // GetFileInformation(((HyperlinkButton)sender).Tag.ToString().Split(';')[0], ((HyperlinkButton)sender).Tag.ToString().Split(';')[1]);
        }

        private string FileTypeToeCategory(string type)
        {

            if (type == "VW_SEARCH_VIDEO")
            {
                return "1";
            }
            else if (type == "VW_SEARCH_AUDIO")
            {
                return "2";
            }
            else if (type == "VW_SEARCH_PHOTO")
            {
                return "3";
            }
            else if (type == "VW_SEARCH_DOC")
            {
                return "4";
            }
            else if (type == "VW_NEWS")
            {
                return "5";
            }
            else
            {
                return "";
            }
        }


        //取得檔案資訊
        private void GetFileInformation(string fsfile_no, string fsfile_type)
        {
            WSBookingFunctionSoapClient bookDb = new WSBookingFunctionSoapClient();

            string _fsguid = Guid.NewGuid().ToString();
            string _fsfile_httpPath = "";
            bookDb.GetFileInformationCompleted += (s, args) =>
            {
                if (args.Error != null)
                {
                    return;
                }
                if (args.Result == null || args.Result == "")
                {
                    return;
                }


                //取得檔案位置
                bookDb.GetFileLocationCompleted += (s1, args1) =>
                {
                    if (args1.Error != null)
                    {
                        return;
                    }
                    if (args1.Result == null)
                    {
                        return;
                    }
                    string file_location = args1.Result;

                    XDocument xmlDocResult = XDocument.Parse(args.Result.ToString());

                    //取得影片起訖點
                    bookDb.GetVideoTimeCodeCompleted += (s2, args2) =>
                    {
                        if (args2.Error != null && String.IsNullOrEmpty(args2.Result))
                        {
                            return;
                        }

                        XDocument xmlDocResult2 = XDocument.Parse(args2.Result.ToString());
                        string start_timecode = "";
                        string end_timecode = "";

                        if (xmlDocResult2.Element("Datas").Value != "")
                        {
                            start_timecode = xmlDocResult2.Element("Datas").Element("Data").Element("FSBEG_TIMECODE").Value;
                            end_timecode = xmlDocResult2.Element("Datas").Element("Data").Element("FSEND_TIMECODE").Value;
                        }

                        //加入參數
                        string parameter = "";
                        parameter += "<Data>";
                        parameter += "<FNNO></FNNO>";
                        parameter += "<FSGUID>" + _fsguid + "</FSGUID>";
                        parameter += "<FSUSER_ID>" + UserClass.userData.FSUSER_ID + "</FSUSER_ID>";
                        parameter += "<FDBOOKING_DATE>" + DateTime.Now.ToString("yyyy/MM/dd") + "</FDBOOKING_DATE>";
                        parameter += "<FSFILE_NO>" + fsfile_no + "</FSFILE_NO>";
                        parameter += "<FSSTART_TIMECODE>" + GetFrame(xmlDocResult.Element("Datas").Element("Data").Element("FSFILE_CATEGORY").Value, xmlDocResult.Element("Datas").Element("Data").Element("FSBEG_TIMECODE").Value) + "</FSSTART_TIMECODE>";
                        parameter += "<FSBEG_TIMECODE>" + start_timecode.Replace(";", ":") + "</FSBEG_TIMECODE>";
                        parameter += "<FSEND_TIMECODE>" + end_timecode.Replace(";", ":") + "</FSEND_TIMECODE>";
                        parameter += "<FSFILE_TYPE>" + xmlDocResult.Element("Datas").Element("Data").Element("FSFILE_TYPE").Value + "</FSFILE_TYPE>";
                        parameter += "<FSFILE_CATEGORY>" + xmlDocResult.Element("Datas").Element("Data").Element("FSFILE_CATEGORY").Value + "</FSFILE_CATEGORY>";
                        parameter += "<FNLOCATION>" + file_location + "</FNLOCATION>";
                        parameter += "<FCOUT_BUY>" + xmlDocResult.Element("Datas").Element("Data").Element("FCOUT_BUY").Value + "</FCOUT_BUY>";
                        parameter += "<FCBOOKING_TYPE>2</FCBOOKING_TYPE>";
                        if (UserClass.userData.FNGROUP_ID == 63)
                        {
                            parameter += "<FCSUPERVISOR_CHECK>N</FCSUPERVISOR_CHECK>";
                            parameter += "<FSSUPERVISOR></FSSUPERVISOR>";
                        }
                        else
                        {
                            parameter += "<FCSUPERVISOR_CHECK>" + xmlDocResult.Element("Datas").Element("Data").Element("FSSUPERVISOR").Value + "</FCSUPERVISOR_CHECK>";
                            parameter += "<FSSUPERVISOR>" + xmlDocResult.Element("Datas").Element("Data").Element("FSSupervisor_ID").Value + "</FSSUPERVISOR>";
                        }
                        //parameter += "<FCSUPERVISOR_CHECK>" + xmlDocResult.Element("Datas").Element("Data").Element("FSSUPERVISOR").Value + "</FCSUPERVISOR_CHECK>";
                        //parameter += "<FSSUPERVISOR>" + xmlDocResult.Element("Datas").Element("Data").Element("FSSupervisor_ID").Value + "</FSSUPERVISOR>";
                        parameter += "<FCALL>Y</FCALL>";
                        parameter += "</Data>";

                        WSBookingFunctionSoapClient booking = new WSBookingFunctionSoapClient();
                        booking.InsertPreBooking2Completed += (s3, args3) =>
                        {
                            if (args3.Error != null)
                            {
                                return;
                            }

                            if (args3.Result)
                            {
                                ServiceTSMSoapClient tsm = new ServiceTSMSoapClient();
                                tsm.GetHttpPathByFileIDCompleted += (s4, args4) =>
                                {
                                    if ((args.Error != null) || String.IsNullOrEmpty(args4.Result))
                                    {
                                        MessageBox.Show("找不到檔案!");
                                        return;
                                    }
                                    _fsfile_httpPath = args4.Result;

                                    string _fsreal_begin_timecode = TransferTimecode.frame2timecode(TransferTimecode.timecodetoframe(start_timecode) - TransferTimecode.timecodetoframe(xmlDocResult.Element("Datas").Element("Data").Element("FSBEG_TIMECODE").Value));
                                    string _fsduration = TransferTimecode.frame2timecode(TransferTimecode.timecodetoframe(end_timecode) - TransferTimecode.timecodetoframe(start_timecode));
                                    string _fsreal_beginframe = (Int32.Parse(_fsreal_begin_timecode.Substring(_fsreal_begin_timecode.Length - 2, 2)) * 0.033366700033667).ToString();
                                    string _fsreal_durationframe = (Int32.Parse(_fsduration.Substring(_fsduration.Length - 2, 2)) * 0.033366700033667).ToString();

                                    _fsreal_begin_timecode = _fsreal_begin_timecode.Substring(0, _fsreal_begin_timecode.Length - 3) + _fsreal_beginframe.Replace("0", "");
                                    _fsduration = _fsduration.Substring(0, _fsduration.Length - 3) + _fsreal_durationframe.Replace("0", "");

                                    booking.WriteAsxFileCompleted += (s5, args5) =>
                                    {
                                        if (xmlDocResult.Element("Datas").Element("Data").Element("FSARC_TYPE").Value == "026")
                                        {
                                            MessageBox.Show("此檔案僅能調用原始完整檔案，無法進行切割與轉出其它格式!");
                                        }
                                        
                                        MessageBox.Show("加入預借清單成功!");
                                    };
                                    booking.WriteAsxFileAsync(_fsguid, _fsreal_begin_timecode, _fsduration, _fsfile_httpPath);

                                };

                                tsm.GetHttpPathByFileIDAsync(fsfile_no, FileID_MediaType.LowVideo);
                            }

                        };

                        booking.InsertPreBooking2Async(parameter, UserClass.userData.FSUSER_ID);

                    };

                    bookDb.GetVideoTimeCodeAsync("<Data><FSFILE_NO>" + fsfile_no + "</FSFILE_NO></Data>");


                };

                bookDb.GetFileLocationAsync(fsfile_no, fsfile_type);

            };

            bookDb.GetFileInformationAsync("<Data><FSFILE_NO>" + fsfile_no + "</FSFILE_NO><FSVW_NAME>" + fsfile_type + "</FSVW_NAME></Data>");
        }


        private string GetFrame(string category, string timecode)
        {
            if (timecode != "")
            {
                if (category == "1" || category == "5")
                {
                    return TransferTimecode.timecodetoframe(timecode).ToString();
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
        }


        private bool IsTape(string fcfrom)
        {
            if (fcfrom == "T")
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void Button_First_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            this.RadBusyIndicator_Search.IsBusy = true;
            currentPageIndex = 1;

            XDocument xDoc = XDocument.Parse(_parameter);
            xDoc.Element("Search").Element("StartPoint").SetValue(((currentPageIndex - 1) * perPageCount).ToString());

            _parameter = xDoc.ToString();
            searchWS.SearchAsync(_parameter);
            //this.RadBusyIndicator_Search.IsBusy = true;
            //currentPageIndex = 1;
            //booking.GetProgramSearchAsync(_parameter);
        }

        private void Button_Prev_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            if (currentPageIndex == 1)
            {
                MessageBox.Show("已經是第一頁!");
                return;
            }
            this.RadBusyIndicator_Search.IsBusy = true;
            currentPageIndex -= 1;
            XDocument xDoc = XDocument.Parse(_parameter);
            xDoc.Element("Search").Element("StartPoint").SetValue(((currentPageIndex - 1) * perPageCount).ToString());

            this.TextBox_PageIndex.Text = currentPageIndex.ToString();
            _parameter = xDoc.ToString();
            searchWS.SearchAsync(_parameter);
            //if (currentPageIndex == 1)
            //{
            //    MessageBox.Show("已經是第一頁!");
            //    return;
            //}
            //currentPageIndex -= 1;
            //this.TextBox_PageIndex.Text = currentPageIndex.ToString();
            //this.RadBusyIndicator_Search.IsBusy = true;
            //booking.GetProgramSearchAsync(_parameter);
        }

        private void Button_Goto_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            int moveToPageIndex = 0;
            if (this.TextBox_PageIndex.Text != "")
            {
                if (Int32.TryParse(this.TextBox_PageIndex.Text, out moveToPageIndex))
                {
                    if (moveToPageIndex > 0 && moveToPageIndex <= totalPageCount)
                    {
                        this.RadBusyIndicator_Search.IsBusy = true;
                        currentPageIndex = moveToPageIndex;
                        XDocument xDoc = XDocument.Parse(_parameter);
                        xDoc.Element("Search").Element("StartPoint").SetValue(((currentPageIndex - 1) * perPageCount).ToString());

                        this.TextBox_PageIndex.Text = currentPageIndex.ToString();
                        _parameter = xDoc.ToString();
                        searchWS.SearchAsync(_parameter);
                    }
                    else
                    {
                        MessageBox.Show("輸入頁數錯誤!");
                    }
                }
                else
                {
                    MessageBox.Show("頁數請輸入數字!");
                }
            }
        }

        private void Button_Next_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            if (currentPageIndex == totalPageCount)
            {
                MessageBox.Show("已經是最後一頁!");
                return;
            }
            this.RadBusyIndicator_Search.IsBusy = true;
            currentPageIndex += 1;
            XDocument xDoc = XDocument.Parse(_parameter);
            xDoc.Element("Search").Element("StartPoint").SetValue(((currentPageIndex - 1) * perPageCount).ToString());

            this.TextBox_PageIndex.Text = currentPageIndex.ToString();
            _parameter = xDoc.ToString();
            searchWS.SearchAsync(_parameter);
            //if (currentPageIndex == totalPageCount)
            //{
            //    MessageBox.Show("已經是最後一頁!");
            //    return;
            //}
            //currentPageIndex += 1;
            //this.TextBox_PageIndex.Text = currentPageIndex.ToString();
            //this.RadBusyIndicator_Search.IsBusy = true;
            //booking.GetProgramSearchAsync(_parameter);
        }

        private void Button_Last_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            this.RadBusyIndicator_Search.IsBusy = true;
            currentPageIndex = totalPageCount;
            XDocument xDoc = XDocument.Parse(_parameter);
            xDoc.Element("Search").Element("StartPoint").SetValue(((currentPageIndex - 1) * perPageCount).ToString());

            this.TextBox_PageIndex.Text = currentPageIndex.ToString();
            _parameter = xDoc.ToString();
            searchWS.SearchAsync(_parameter);
            //currentPageIndex = totalPageCount;
            //this.TextBox_PageIndex.Text = currentPageIndex.ToString();
            //this.RadBusyIndicator_Search.IsBusy = true;
            //booking.GetProgramSearchAsync(_parameter);
        }

        private void HyperlinkButton_View_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            string fsfile_no = ((HyperlinkButton)sender).Tag.ToString().Split(';')[0];
            string fscategory = FileTypeToeCategory(((HyperlinkButton)sender).Tag.ToString().Split(';')[1]);

            SRH101_02 p = new SRH101_02(fsfile_no, fscategory);
            p.Show();

            //MessageBox.Show(fsfile_no + "/" + fscategory);
        }

        private string GetFileCategoryImage(string category, string tape, string fsfile_no, string path)
        {
            if (path == "" && category == "VW_SEARCH_VIDEO" && tape != "T")
            {
                return "/PTS_MAM3;component/Images/Video.png";
            }
            else if (category == "VW_SEARCH_VIDEO" && tape == "T")
            {
                //return "/PTS_MAM3;component/Images/tape.png";
                return "../Images/tape.png";
            }
            //else if (category == "VW_SEARCH_AUDIO")
            //{
            //    //return "/PTS_MAM3;component/Images/audio.png";
            //    return "../Images/audio.png";
            //}
            else if (path == "" && category == "VW_SEARCH_PHOTO")
            {
                //return "/PTS_MAM3;component/Images/photo.png";
                return "../Images/photo.png";
            }
            //else if (category == "VW_SEARCH_DOC")
            //{
            //    //return "/PTS_MAM3;component/Images/txt.png";
            //    return "../Images/txt.png";
            //}
            //else if (category == "VW_NEWS")
            //{
            //    return "../Images/news.png";

            //}
            else
            {
                return path;
            }
        }

        private string GetFileTypeChtName(string fsvmname)
        {
            if (fsvmname == "VW_SEARCH_VIDEO")
            {
                return "影片";
            }
            else if (fsvmname == "VW_SEARCH_AUDIO")
            {
                return "聲音";
            }
            else if (fsvmname == "VW_SEARCH_PHOTO")
            {
                return "圖片";
            }
            else if (fsvmname == "VW_SEARCH_DOC")
            {
                return "其他";
            }
            else
            {
                return "";
            }
        }
    }

    public class ProgramBookingResultClass
    {
        public string FSFILE_NO { set; get; }
        public string FSPGMNAME { set; get; }
        public string FSPGM_SUBNAME { set; get; }
        public string FNEPISODE { set; get; }
        public string FSFILE_CATEGORY { set; get; }
        public string FSIMAGE_PATH { set; get; }
        public string FSFILENO_CATEGORY { set; get; }
        public string FSVWNAME { set; get; }
        public bool FCFROM { set; get; }
        public string FSFILE_TYPE { set; get; }
    }
}

