﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Threading;
using PTS_MAM3.WSBookingFunction;
using System.Windows.Browser;

namespace PTS_MAM3.SRH
{
    public partial class SRH107_01 : ChildWindow
    {
        private WSBookingFunctionSoapClient wsBookingFunctionSoapClient = new WSBookingFunctionSoapClient();
        private Timer _timer;
        
        public SRH107_01()
        {
            InitializeComponent();

            wsBookingFunctionSoapClient.IsExportCompleteCompleted += new EventHandler<IsExportCompleteCompletedEventArgs>(wsBookingFunctionSoapClient_IsExportCompleteCompleted);

            this.HyperlinkButton_Download.Visibility = System.Windows.Visibility.Collapsed;
            this.TextBlock_Hint.Visibility = System.Windows.Visibility.Collapsed;

            _timer = new System.Threading.Timer(new System.Threading.TimerCallback(Check), null, 5000, 5000);
        }


        void wsBookingFunctionSoapClient_IsExportCompleteCompleted(object sender, IsExportCompleteCompletedEventArgs e)
        {
            if (e.Error != null || string.IsNullOrEmpty(e.Result))
            {

            }
            else
            {
                this.TextBlock_Downloading.Visibility = System.Windows.Visibility.Collapsed;
                this.HyperlinkButton_Download.Content = e.Result;
                this.HyperlinkButton_Download.Visibility = System.Windows.Visibility.Visible;
                this.TextBlock_Hint.Visibility = System.Windows.Visibility.Visible;
                _timer.Dispose();
            }
        }


        void Check(object obj)
        {
            this.Dispatcher.BeginInvoke(
                delegate()
                {
                    wsBookingFunctionSoapClient.IsExportCompleteAsync(UserClass.userData.FSUSER_ID);
                }
             );
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void HyperlinkButton_Download_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
            HtmlPage.Window.Navigate(new Uri(this.HyperlinkButton_Download.Content.ToString()), "_blank");
        }

    }
}

