﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSASPNetSession;
using PTS_MAM3.WCFSearchDataBaseFunction;
using System.Xml;
using System.Xml.Linq;

namespace AllSearch
{
    public partial class SearchHistory : UserControl
    {

        WSASPNetSessionSoapClient ASPSession = new WSASPNetSessionSoapClient();

        SearchDataBaseFunctionClient searchDb = new SearchDataBaseFunctionClient();

        private PTS_MAM3.MainFrame parentFrame;

        public SearchHistory(PTS_MAM3.MainFrame mainframe)
        {
            InitializeComponent();

            parentFrame = mainframe;

            ASPSession.GetUserSessionCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        List<PTS_MAM3.WSASPNetSession.UserStruct> SessionList = args.Result.ToList();
                        UserClass.userData = (PTS_MAM3.WSASPNetSession.UserStruct)(SessionList.FirstOrDefault());
                    }
                }

                searchDb.GetSearchHistoryCompleted += (s1, args1) =>
                {
                    if (args1.Error != null)
                    {
                        return;
                    }

                    if (args1.Result == null)
                    {
                        return;
                    }
                    
                    XDocument xmlDocResult = XDocument.Parse(args1.Result.ToString().Replace("&","&amp;"));

                    var paras = from para in xmlDocResult.Descendants("Data")
                                select new SearchHistoryClass
                                {
                                    fnno = para.Element("FNNO").Value,
                                    fdsearch_date = para.Element("FDSEARCH_DATE").Value,
                                    fskey_word = para.Element("FSKEY_WORD").Value,
                                    fsindex = para.Element("FSINDEX").Value,
                                    fddate = para.Element("FDDATE").Value,
                                    fschannel = para.Element("FSCHANNEL").Value,
                                    fscolumn1 = para.Element("FSCOLUMN1").Value,
                                    fsvalue1 = para.Element("FSVALUE1").Value,
                                    fscolumn2 = para.Element("FSCOLUMN2").Value,
                                    fsvalue2 = para.Element("FSVALUE2").Value,
                                    fscolumn3 = para.Element("FSCOLUMN3").Value,
                                    fsvalue3 = para.Element("FSVALUE3").Value,
                                    fslogic1 = para.Element("FSLOGIC1").Value,
                                    fslogic2 = para.Element("FSLOGIC2").Value,
                                    fssearch_mode_name = para.Element("FSSEARCH_MODE_NAME").Value,
                                    fshomophone_name = para.Element("FSHOMOPHONE_NAME").Value,
                                    fssynonym_name = para.Element("FSSYNONYM_NAME").Value,
                                    fssearch_mode = para.Element("FSSEARCH_MODE").Value,
                                    fshomophone = para.Element("FSHOMOPHONE").Value,
                                    fssynonym = para.Element("FSSYNONYM").Value,
                                    fnpagesize = para.Element("FNPAGE_SIZE").Value,
                                    fsorderby = para.Element("FSORDERBY").Value,
                                    fsordertype = para.Element("FSORDERTYPE").Value
                                };

                    this.ListBox_SearchHistory.ItemsSource = paras;

                };

                string parameter = "";
                parameter += "<Data>";
                parameter += "<FSUSER_ID>" + UserClass.userData.FSUSER_ID + "</FSUSER_ID>";
                parameter += "<FDSEARCH_DATE_BEGIN>" + DateTime.Now.AddDays(-7).ToString("yyyy/MM/dd") + "</FDSEARCH_DATE_BEGIN>";
                parameter += "<FDSEARCH_DATE_END>" + DateTime.Now.ToString("yyyy/MM/dd") + "</FDSEARCH_DATE_END>";
                parameter += "</Data>";

                searchDb.GetSearchHistoryAsync(parameter);

            };

            ASPSession.GetUserSessionAsync("USEROBJ");
            
        }

        private void ListBox_SearchHistory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //MessageBox.Show(((SearchHistoryClass)this.ListBox_SearchHistory.SelectedItem).fnno);
            //查詢字串
            SearchHistoryClass selectedItem = (SearchHistoryClass)this.ListBox_SearchHistory.SelectedItem;
            string parameter = "";

            string columnName = "";
            string columnValue = "";
            string columnLogic = "";
            if (selectedItem.fscolumn1 != "")
            {
                columnName += selectedItem.fscolumn1 + ";";
            }
            if (selectedItem.fscolumn2 != "")
            {
                columnName += selectedItem.fscolumn2 + ";";
            }
            if (selectedItem.fscolumn3 != "")
            {
                columnName += selectedItem.fscolumn3 + ";";
            }
            if (columnName != "")
            {
                columnName = columnName.Substring(0, columnName.Length - 1);
            }

            if (selectedItem.fsvalue1 != "")
            {
                columnValue += selectedItem.fsvalue1 + ";";
            }
            if (selectedItem.fsvalue2 != "")
            {
                columnValue += selectedItem.fsvalue2 + ";";
            }
            if (selectedItem.fsvalue3 != "")
            {
                columnValue += selectedItem.fsvalue3 + ";";
            }
            if (columnValue != "")
            {
                columnValue = columnValue.Substring(0, columnValue.Length - 1);
            }

            if (selectedItem.fslogic1 != "")
            {
                columnLogic += selectedItem.fslogic1 + ";";
            }
            if (selectedItem.fslogic2 != "")
            {
                columnLogic += selectedItem.fslogic2 + ";";
            }
            if (columnLogic != "")
            {
                columnLogic = columnLogic.Substring(0, columnLogic.Length - 1);
            }

            parameter += "<Search><IniPath></IniPath>";
            parameter += "<KeyWord>"+selectedItem.fskey_word+"</KeyWord>";
            parameter += "<IndexName>" + selectedItem.fsindex+"</IndexName>";
            parameter += "<Date>" + selectedItem .fddate+ "</Date>";
            parameter += "<Channel>"+selectedItem.fschannel+"</Channel>";
            parameter += "<Columns>" + columnName + "</Columns>";
            parameter += "<Values>"+columnValue+"</Values>";
            parameter += "<Logics>"+columnLogic+"</Logics>";
            parameter += "<SearchMode>"+selectedItem.fssearch_mode+"</SearchMode>";
            parameter += "<Homophone>"+selectedItem.fshomophone+"</Homophone>";
            parameter += "<Synonym>"+selectedItem.fssynonym+"</Synonym>";
            parameter += "<PageSize>" + selectedItem.fnpagesize + "</PageSize>";
            parameter += "<StartPoint>0</StartPoint><GetColumns>FSSYS_ID;FSFILE_NO;FSTITLE;FSCONTENT;FDCREATED_DATE;FSVWNAME;FCFROM;FSPROG_B_PGMNAME;FNEPISODE;FSTYPE_NAME;FSPROG_D_PGDNAME</GetColumns>";
            parameter += "<TreeNode>/</TreeNode><orderby>" + selectedItem.fsorderby + "</orderby><ordertype>" + selectedItem.fsordertype + "</ordertype></Search>";
            parentFrame.generateNewPane("檢索結果", "SEARCHRESULT", parameter, true);
        }
    }

    public class SearchHistoryClass
    {
        public string fnno { set; get; }
        public string fdsearch_date { set; get; }
        public string fskey_word { set; get; }
        public string fsindex { set; get; }
        public string fddate { set; get; }
        public string fschannel { set; get; }
        public string fscolumn1 { set; get; }
        public string fsvalue1 { set; get; }
        public string fscolumn2 { set; get; }
        public string fsvalue2 { set; get; }
        public string fscolumn3 { set; get; }
        public string fsvalue3 { set; get; }
        public string fslogic1 { set; get; }
        public string fslogic2 { set; get; }
        public string fssearch_mode_name { set; get; }
        public string fshomophone_name { set; get; }
        public string fssynonym_name { set; get; }
        public string fssearch_mode { set; get; }
        public string fshomophone { set; get; }
        public string fssynonym { set; get; }
        public string fnpagesize { set; get; }
        public string fsorderby { set; get; }
        public string fsordertype { set; get; }
    }
}
