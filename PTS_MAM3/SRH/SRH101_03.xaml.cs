﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSBookingFunction;
using SearchClassLibrary;
using System.Xml;
using System.Xml.Linq;
using PTS_MAM3.Common;

namespace PTS_MAM3.SRH
{
    public partial class SRH101_03 : ChildWindow
    {
        private string _fsfileno;
        private string _fscategory;
        private string _fnno;

        public SRH101_03()
        {
            InitializeComponent();
            PreBookingListBind();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void PreBookingListBind()
        {
            WSBookingFunctionSoapClient bookDb = new WSBookingFunctionSoapClient();
            bookDb.GetPreBookingListCompleted += (s, args) =>
            {
                if (args.Error != null)
                {
                    return;
                }
                if (string.IsNullOrEmpty(args.Result))
                {
                    return;
                }

                XDocument xmlDocResult = XDocument.Parse(args.Result);

                var paras = from para in xmlDocResult.Descendants("Data")
                            select new PreBookingClass
                            {
                                FNNO = para.Element("FNNO").Value,
                                FSFILE_NO = para.Element("FSFILE_NO").Value,
                                FSTITLE = para.Element("FSTITLE").Value,
                                FDPREBOOKING_DATE = para.Element("FDBOOKING_DATE").Value,
                                FSBEG_TIMECODE = para.Element("FSBEG_TIMECODE").Value,
                                FSEND_TIMECODE = para.Element("FSEND_TIMECODE").Value,
                                FSFILE_TYPE = para.Element("FSFILE_TYPE").Value,
                                FSFILE_CATEGORY = para.Element("FSFILE_CATEGORY").Value,
                                FSIMAGEPATH = GetFileCategoryImage(para.Element("FSFILE_CATEGORY").Value),
                                FSFILENO_CATEGORY = para.Element("FSFILE_NO").Value + ";" + para.Element("FSFILE_CATEGORY").Value + ";" + para.Element("FNNO").Value + ";" + para.Element("FSSTART_TIMECODE").Value

                            };

                this.DataGrid_PreBooking_List.SelectedIndex = -1;
                this.DataGrid_PreBooking_List.ItemsSource = null;
                this.DataGrid_PreBooking_List.ItemsSource = paras;
            };

            bookDb.GetPreBookingListAsync("<Data><FSUSER_ID>" + UserClass.userData.FSUSER_ID + "</FSUSER_ID><FCBOOKING_TYPE>2</FCBOOKING_TYPE><FSFILE_CATEGORY></FSFILE_CATEGORY></Data>");
        }

        //依照不同類型，顯示不同圖片
        private string GetFileCategoryImage(string category)
        {
            if (category == "1")
            {
                return "/PTS_MAM3;component/Images/video16_16.png";
            }
            else if (category == "2")
            {
                return "/PTS_MAM3;component/Images/audio16_16.png";
            }
            else if (category == "3")
            {
                return "/PTS_MAM3;component/Images/photo16_16.png";
            }
            else if (category == "4")
            {
                return "/PTS_MAM3;component/Images/txt16_16.png";
            }
            else if (category == "5")
            {
                return "/PTS_MAM3;component/Images/news16_16.png";
            }
            else
            {
                return "";
            }
        }

        

        private void Button_Delete_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            _fsfileno = ((Button)sender).Tag.ToString().Split(';')[0];
            _fscategory = ((Button)sender).Tag.ToString().Split(';')[1];
            _fnno = ((Button)sender).Tag.ToString().Split(';')[2];

            chWndMessageBox confirm = new chWndMessageBox("刪除預借清單項目", "確定要刪除此清單項目?", 0);
            confirm.Show();
            confirm.Closing += (s, args) =>
            {
                if (confirm.DialogResult == true)
                {
                    WSBookingFunctionSoapClient bookDb = new WSBookingFunctionSoapClient();
                    bookDb.DeletePreBookingCompleted += (s1, args1) =>
                    {
                        if (args1.Error != null)
                        {
                            return;
                        }

                        if (args1.Result)
                        {
                            MessageBox.Show("刪除成功!");
                            PreBookingListBind();
                        }

                    };

                    bookDb.DeletePreBookingAsync("<Data><FNNO>" + _fnno + "</FNNO></Data>", UserClass.userData.FSUSER_ID);
                }
            };
        }
    }
}

