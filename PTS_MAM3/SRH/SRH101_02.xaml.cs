﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SearchClassLibrary;
using PTS_MAM3.WSBookingFunction;
using System.Xml;
using System.Xml.Linq;
using PTS_MAM3.WSTSM_GetFileInfo;
using System.Windows.Media.Imaging;

namespace PTS_MAM3.SRH
{
    public partial class SRH101_02 : ChildWindow
    {
        private string _fsfile_no = "";
        private string _fsfile_categroy = "";
        private string startTimeCode = "00:00:00:00";
        //建立Video物件
        PreviewPlayer.SLVideoPlayer player = new PreviewPlayer.SLVideoPlayer("", 0);
        //建立聲音物件
        //建立圖片物件
        //建立文件物件
        TextBlock textblock = new TextBlock();

        WSBookingFunctionSoapClient bookDb = new WSBookingFunctionSoapClient();
        ServiceTSMSoapClient tsm = new ServiceTSMSoapClient();

        public SRH101_02(string fsfile_no, string fsfile_category)
        {
            InitializeComponent();
            
            this.Grid_Show.Children.Clear();

            _fsfile_no = fsfile_no;
            _fsfile_categroy = fsfile_category;
            this.RadBusyIndicator_Search.IsBusy = true;

            bookDb.GetProgramSearchDetailDataCompleted += (s, args) =>
            {
                if (args.Error != null || string.IsNullOrEmpty(args.Result))
                {
                    this.RadBusyIndicator_Search.IsBusy = false;
                    return;
                }


                XDocument xmlDocResult = XDocument.Parse(args.Result.Replace("", ""));
                if (xmlDocResult.Element("Datas").Value != "")
                {
                    this.TextBlock_File_No.Text = xmlDocResult.Element("Datas").Element("Data").Element("FSFILE_NO").Value;
                    this.TextBlock_Title.Text = xmlDocResult.Element("Datas").Element("Data").Element("FSTITLE").Value;
                    this.TextBlock_ProgramName.Text = xmlDocResult.Element("Datas").Element("Data").Element("FSPROG_B_PGMNAME").Value;
                    this.TextBlock_Episode.Text = xmlDocResult.Element("Datas").Element("Data").Element("FNEPISODE").Value;
                    this.TextBlock_Program_D_Name.Text = xmlDocResult.Element("Datas").Element("Data").Element("FSPROG_D_PGDNAME").Value;
                    this.TextBlock_Channel.Text = xmlDocResult.Element("Datas").Element("Data").Element("FSCHANNEL").Value;
                    this.TextBlock_FileFrom.Text = (xmlDocResult.Element("Datas").Element("Data").Element("FCFROM").Value == "T" ? "影帶" : "數位檔案");
                    this.TextBlock_File_ArcType.Text = (xmlDocResult.Element("Datas").Element("Data").Element("FSFILE_ARCTYPE") != null ? xmlDocResult.Element("Datas").Element("Data").Element("FSFILE_ARCTYPE").Value : "");
                    if (_fsfile_categroy == "4")
                    {
                        textblock.Text = xmlDocResult.Element("Datas").Element("Data").Element("FSCONTENT").Value;
                        textblock.FontSize = 12;
                        this.Grid_Show.Children.Add(textblock);
                        this.RadBusyIndicator_Search.IsBusy = false;
                    }
                    else
                    {
                        ViewBind(xmlDocResult.Element("Datas").Element("Data").Element("FCFROM").Value);
                    }
                    
                }
                
            };

            
            
            bookDb.GetProgramSearchDetailDataAsync("<Data><FSFILE_NO>" + _fsfile_no + "</FSFILE_NO><FSFILE_CATEGORY>" + _fsfile_categroy + "</FSFILE_CATEGORY></Data>");
        }



        private void ViewBind(string fcfrom)
        {
            

            if (_fsfile_categroy == "1")
            {
                if (fcfrom == "T")
                {
                    this.RadBusyIndicator_Search.IsBusy = false;
                    return;
                }
                
                bookDb.GetFileStartTimeCodeCompleted += (s, args) =>
                {
                    if ((args.Error != null) || String.IsNullOrEmpty(args.Result))
                    {
                        this.RadBusyIndicator_Search.IsBusy = false;
                        return;
                    }
                    else
                    {
                        XDocument xDoc = XDocument.Parse(args.Result);
                        if (xDoc.Element("Datas").Value != "")
                        {
                            startTimeCode = xDoc.Element("Datas").Element("Data").Element("FSBEG_TIMECODE").Value;
                            tsm.GetHttpPathByFileIDAsync(_fsfile_no, FileID_MediaType.LowVideo);
                        }
                    }
                };
                
                
                //撈取影片檔案路徑
                
                tsm.GetHttpPathByFileIDCompleted += (s, args) =>
                {
                    if ((args.Error != null) || String.IsNullOrEmpty(args.Result))
                    {
                        this.RadBusyIndicator_Search.IsBusy = false;
                        return;
                    }
                    else
                    {

                        player.HTML_SetURL(args.Result, TransferTimecode.timecodetoframe(startTimeCode));
                        //將Video加置Grid
                        this.Grid_Show.Children.Add(player);
                        this.RadBusyIndicator_Search.IsBusy = false;
                    }

                };

                bookDb.GetFileStartTimeCodeAsync("<Data><FSFILE_NO>"+_fsfile_no+"</FSFILE_NO></Data>");


                //撈取節目片庫keyframe
                tsm.GetKeyframeFilesInfoCompleted += (s, args) =>
                {

                    this.ListBox_KeyFrame.ItemsSource = null;
                    if ((args.Error != null) || args.Result == null)
                    {
                        this.RadBusyIndicator_Search.IsBusy = false;
                        return;
                    }
                    List<KeyFrameClass> KeyFrameClassList = new List<KeyFrameClass>();

                    foreach (PTS_MAM3.WSTSM_GetFileInfo.KeyframeInfo obj in args.Result)
                    {
                        KeyFrameClass k = new KeyFrameClass();
                        string time = obj.KeyframeHttpPath.Substring(obj.KeyframeHttpPath.IndexOf('_') + 1, 8);
                        string hh = time.Substring(0, 2);
                        string mm = time.Substring(2, 2);
                        string ss = time.Substring(4, 2);
                        string ff = time.Substring(6, 2);

                        k.path = obj.KeyframeHttpPath;
                        k.desc = hh + ":" + mm + ":" + ss + ":" + ff + (string.IsNullOrEmpty(obj.KeyframeDescription) ? "" : "：" + obj.KeyframeDescription);
                        k.sec = TransferTimecode.timecodetoframe(hh + ":" + mm + ":" + ss + ":" + ff).ToString();
                        KeyFrameClassList.Add(k);
                    }

                    if (KeyFrameClassList != null && KeyFrameClassList.Count() > 0)
                    {
                        this.ListBox_KeyFrame.ItemsSource = KeyFrameClassList;
                    }
                    this.RadBusyIndicator_Search.IsBusy = false;
                };

                tsm.GetKeyframeFilesInfoAsync(_fsfile_no);
            }
            else if (_fsfile_categroy == "2")
            {
                this.RadBusyIndicator_Search.IsBusy = false;
            }
            else if (_fsfile_categroy == "3")
            {
                ServiceTSMSoapClient tsm_photo = new ServiceTSMSoapClient();
                //撈取圖
                tsm_photo.GetHttpPathByFileIDCompleted += (s, args) =>
                {
                    if ((args.Error != null) || String.IsNullOrEmpty(args.Result))
                    {
                        this.RadBusyIndicator_Search.IsBusy = false;
                    }
                    else
                    {
                        Image img = new Image();
                        img.Width = 320;
                        img.Height = 320;
                        img.Source = new BitmapImage(new Uri(args.Result, UriKind.Absolute));
                        this.Grid_Show.Children.Add(img);
                        this.RadBusyIndicator_Search.IsBusy = false;
                    }
                };

                tsm_photo.GetHttpPathByFileIDAsync(_fsfile_no, FileID_MediaType.Photo);
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.RadBusyIndicator_Search.IsBusy = false;
            this.DialogResult = true;
        }

        private void Button_Keyframe_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            string sec = (sender as Button).Tag.ToString();
            //MessageBox.Show(sec);
            if (player != null)
            {
                player.HTML_SetFrame(long.Parse(sec));
                player.HTML_Play_Click();
            }
        }

    }
}

