﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SearchClassLibrary;
using PTS_MAM3.WSBookingFunction;
using System.Xml;
using System.Xml.Linq;
using System.Windows.Browser;

namespace PTS_MAM3.SRH
{
    public partial class SRH131 : UserControl
    {
        private PTS_MAM3.MainFrame parentFrame;

        WSBookingFunctionSoapClient booking = new WSBookingFunctionSoapClient();

        public SRH131(PTS_MAM3.MainFrame mainframe)
        {
            InitializeComponent();
            parentFrame = mainframe;
            this.DatePicker_BookingDate_Begin.SelectedDate = DateTime.Now;
            this.DatePicker_BookingDate_End.SelectedDate = DateTime.Now;
            this.DataGrid_BookingMaster.ItemsSource = null;
       
        }


        private void Button_Search_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
            if (!this.DatePicker_BookingDate_Begin.SelectedDate.HasValue || !this.DatePicker_BookingDate_End.SelectedDate.HasValue)
            {
                MessageBox.Show("請輸入起迄日期!");
                return;
            }

            booking.GetUserBookingHistoryMasterCompleted += (s, args) =>
            {
                if (args.Error != null)
                {
                    return;
                }
                if (args.Result == null)
                {
                    return;
                }

                XDocument xmlDocResult = XDocument.Parse(args.Result.ToString());

                var paras = from para in xmlDocResult.Descendants("Data")
                            select new UserBookingHistoryMasterClass
                            {
                                FSBOOKING_NO = para.Element("FSBOOKING_NO").Value,
                                FSUSER_ID = para.Element("FSUSER_ID").Value,
                                FDBOOKING_DATE = para.Element("FDBOOKING_DATE").Value,
                                FSBOOKING_REASON = para.Element("FSREASON").Value,
                                FSCHECK_STATUS = para.Element("FSCHECK_STATUS").Value,
                                FDCHECK_DATE = para.Element("FDCHECK_DATE").Value,
                                FSCHECK_ID = para.Element("FSCHECK_ID").Value,
                            };
                this.DataGrid_BookingMaster.SelectedIndex = -1;
                this.DataGrid_BookingDetail.SelectedIndex = -1;
                this.DataGrid_BookingMaster.ItemsSource = null;
                this.DataGrid_BookingDetail.ItemsSource = null;
                this.DataGrid_BookingMaster.ItemsSource = paras;
            };

            string parameter = "";
            parameter += "<Data>";
            parameter += "<FSUSER_ID>"+UserClass.userData.FSUSER_ID+"</FSUSER_ID>";
            parameter += "<FDBOOKINGDATE_BEGIN>"+this.DatePicker_BookingDate_Begin.SelectedDate.Value.ToString("yyyy/MM/dd")+"</FDBOOKINGDATE_BEGIN>";
            parameter += "<FDBOOKINGDATE_END>" + this.DatePicker_BookingDate_End.SelectedDate.Value.ToString("yyyy/MM/dd") + "</FDBOOKINGDATE_END>";
            parameter += "</Data>";

            booking.GetUserBookingHistoryMasterAsync(parameter);
        }

        private void DataGrid_BookingMaster_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
            if (this.DataGrid_BookingMaster.SelectedItem == null)
            {
                return;
            }
            string fsbooking_no = ((UserBookingHistoryMasterClass)this.DataGrid_BookingMaster.SelectedItem).FSBOOKING_NO;
            
            booking.GetUserBookingHistoryDetailCompleted += (s, args) =>
            {
                if (args.Error != null)
                {
                    return;
                }
                if (args.Result == null)
                {
                    return;
                }

                XDocument xmlDocResult = XDocument.Parse(args.Result.ToString());
                var paras = from para in xmlDocResult.Descendants("Data")
                            select new UserBookingHistoryDetailClass
                            {
                                FSSEQ = para.Element("FSSEQ").Value,
                                FSFILE_NO = para.Element("FSFILE_NO").Value,
                                FSTITLE = para.Element("FSTITLE").Value,
                                FSPGMNAME = para.Element("FSPGMNAME").Value,
                                FSTIME_CODE = para.Element("FSTIME_CODE").Value,
                                FSFILE_CATEGORY = para.Element("FSCATEGORY_CNAME").Value,
                                FSCHECK_STATUS = para.Element("FCCHECK").Value,
                                FSCHECK_ID = para.Element("FSCHECK_ID").Value,
                                FDCHECK_DATE = para.Element("FDCHECK_DATE").Value,
                                FCSUPERVISOR = para.Element("FCSUPERVISOR").Value,
                                FSSUPERVISOR_CHECK_STATUS = para.Element("FCSUPERVISOR_CHECK").Value,
                                FSSUPERVISOR_CHECK_ID = para.Element("FSSUPERVISOR_CHECK_ID").Value,
                                FDSUPERVISOR_CHECK_DATE = para.Element("FDSUPERVISOR_CHECK_DATE").Value,
                                FCFILE_STATUS = para.Element("FCFILE_STATUS").Value,
                                FCFILE_TRANSCODE_STATUS = para.Element("FCFILE_TRANSCODE_STATUS").Value,
                            };

                this.DataGrid_BookingDetail.SelectedIndex = -1;
                this.DataGrid_BookingDetail.ItemsSource = null;
                this.DataGrid_BookingDetail.ItemsSource = paras;
            };


            string parameter = "";
            parameter += "<Data>";
            parameter += "<FSBOOKING_NO>" + fsbooking_no + "</FSBOOKING_NO>";
            parameter += "</Data>";

            booking.GetUserBookingHistoryDetailAsync(parameter);

        }

        private void Button_Print_Click(object sender, RoutedEventArgs e)
        {
            if (this.DataGrid_BookingMaster.ItemsSource == null)
            {
                MessageBox.Show("請查詢調用資料!");
                return;
            }
            if (!this.DatePicker_BookingDate_Begin.SelectedDate.HasValue || !this.DatePicker_BookingDate_End.SelectedDate.HasValue)
            {
                MessageBox.Show("請輸入起迄日期!");
                return;
            }
            WSBookingFunctionSoapClient booking1 = new WSBookingFunctionSoapClient();

            booking1.GetUserBookingReportCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result != "")
                    {
                        if (args.Result.ToString().StartsWith("錯誤"))
                            MessageBox.Show(args.Result.ToString(), "提示訊息", MessageBoxButton.OK);
                        else
                            HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + args.Result, UriKind.Relative), "_blank", "height=600,width=1100,top=100,left=100,toolbar=0,statusbar=0,menubar=0,scrollbar=0,resizable=yes");
                    }
                    else
                        MessageBox.Show("查無相關資料，或該筆資料已不存在資料庫中。", "提示訊息", MessageBoxButton.OK);
                }        
            };

            booking1.GetUserBookingReportAsync(this.DatePicker_BookingDate_Begin.SelectedDate.Value.ToString("yyyy/MM/dd"), this.DatePicker_BookingDate_End.SelectedDate.Value.ToString("yyyy/MM/dd"), UserClass.userData.FSUSER_ID, UserClass.userData.FSSESSION_ID);
        }

        private void Button_Refresh_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
            if (this.DataGrid_BookingMaster.SelectedItem == null)
            {
                return;
            }
            string fsbooking_no = ((UserBookingHistoryMasterClass)this.DataGrid_BookingMaster.SelectedItem).FSBOOKING_NO;
            string parameter = "";
            parameter += "<Data>";
            parameter += "<FSBOOKING_NO>" + fsbooking_no + "</FSBOOKING_NO>";
            parameter += "</Data>";
            booking.GetUserBookingHistoryDetailAsync(parameter);
        }

        private void HyperlinkButton_Memo_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            HyperlinkButton hyperlinkbutton = sender as HyperlinkButton;
            DataGridRow currentDataGridRow = DataGridRow.GetRowContainingElement(hyperlinkbutton);
            if (currentDataGridRow == null)
            {
                return;
            }
            string seq = (this.DataGrid_BookingDetail.Columns[1].GetCellContent(currentDataGridRow) as TextBlock).Tag.ToString();
            string fsbooking_no = ((UserBookingHistoryMasterClass)this.DataGrid_BookingMaster.SelectedItem).FSBOOKING_NO;
            SRH136_01 srh136_01 = new SRH136_01(fsbooking_no, seq, false);
            srh136_01.Show();
        }
    }


}
