﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO;
using PTS_MAM3.WSSearchService;
using SearchClassLibrary;
using System.Xml;
using System.Xml.Linq;
using Telerik.Windows.Controls;
using PTS_MAM3.WSASPNetSession;
using PTS_MAM3.WCFSearchDataBaseFunction;
using PTS_MAM3.WSBookingFunction;
using PTS_MAM3.WSTSM_GetFileInfo;
using System.Windows.Media.Imaging;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text.RegularExpressions;

namespace PTS_MAM3.SRH
{
    public partial class SRH107 : UserControl
    {

        WSASPNetSessionSoapClient ASPSession = new WSASPNetSessionSoapClient();
        //目前頁次
        private int currentPageIndex = 1;
        //總頁次
        private int totalPageCount = 0;
        //每頁筆數
        private int perPageCount = 0;
        //總筆數
        private int totalCount = 0;
        //秒數
        private string searchsecond = "";

        //查詢條件XML
        private string parameter = "";

        //關鍵字
        private string _keyword = "";

        //分類樹目前節點
        private RadTreeViewItem currentTreeViewItem = null;

        //換頁
        private bool isPaging = false;

        //SearchServiceClient searchWS = new SearchServiceClient();
        //SearchServiceClient searchWS1 = new SearchServiceClient();

        SearchServiceSoapClient searchWS = new SearchServiceSoapClient();
        SearchServiceSoapClient searchWS1 = new SearchServiceSoapClient();
        SearchServiceSoapClient searchWS2 = new SearchServiceSoapClient();
        SearchResultClass searchResult = new SearchResultClass();

        WSBookingFunctionSoapClient bookDb = new WSBookingFunctionSoapClient();

        SearchDataBaseFunctionClient searchDb = new SearchDataBaseFunctionClient();

        //ASPNetSessionClient ASPSession = new ASPNetSessionClient();

        //預借清單編號
        //private string _preBookingNo;

        private PTS_MAM3.MainFrame parentFrame;

        //預借清單集合
        List<SearchClassLibrary.PreBookingClass> prebookingList = new List<SearchClassLibrary.PreBookingClass>();

        //建立Video物件
        PreviewPlayer.SLVideoPlayer player = new PreviewPlayer.SLVideoPlayer("", 0);


        private string startTimeCode = "00:00:00:00";

        List<SearchResultClass> SearchResultClass_LIST = new List<SearchResultClass>();
        public List<clsPREBOOKING> clsPREBOOKING_LIST = new List<clsPREBOOKING>();

        public bool flag = true;

        public SRH107(string p, PTS_MAM3.MainFrame mainframe)
        {
            InitializeComponent();
            parentFrame = mainframe;
            this.RadBusyIndicator_AllSearchResult.IsBusy = true;
            this.RadOutlookBar_Detail.SelectedIndex = 8;

            parameter = p;

            Register();

            searchWS.SearchCompleted += (s, args) =>
            {


                if (args == null || args.Error != null)
                {
                    this.RadBusyIndicator_AllSearchResult.IsBusy = false;
                    return;
                }

                if (args.Result.ToString() == "")
                {
                    this.RadBusyIndicator_AllSearchResult.IsBusy = false;
                    return;
                }

                //過濾字元字串
                //XDocument xmlDocResult = XDocument.Parse(args.Result.Replace("&", "&amp;").Replace("", ""));
                //XDocument xmlDocResult = XDocument.Parse(args.Result);
                XDocument xmlDocResult = XDocument.Parse(ConvertWhitespaceToSpacesRegex(args.Result).Replace(((char)0x2028), '\0').Replace(((char)0x2029), '\0'));

                XDocument xmlDocParameter = XDocument.Parse(parameter);
                //取得總筆數
                Int32.TryParse(xmlDocResult.Element("Datas").Attribute("count").Value, out totalCount);
                //取得時間
                this.TextBlock_SearchCount.Text = "有 " + xmlDocResult.Element("Datas").Attribute("count").Value + " 項結果 (搜尋時間：" + xmlDocResult.Element("Datas").Attribute("time").Value + " 秒) ";


                if (totalCount == 0)
                {
                    MessageBox.Show("查無資料");
                    this.RadBusyIndicator_AllSearchResult.IsBusy = false;
                    RadPane openedPane = (RadPane)parentFrame.PaneGroup.SelectedItem;
                    parentFrame.PaneGroup.RemovePane(openedPane);
                    parentFrame.PaneGroup.SelectedIndex = parentFrame.PaneGroup.Items.Count - 1;
                    return;
                }

                string tree = xmlDocParameter.Element("Search").Element("TreeNode").Value;
                _keyword = xmlDocParameter.Element("Search").Element("KeyWord").Value;

                //若為換頁，則不重新建樹
                if (!isPaging)
                {
                    searchWS.SearchTreeAsync(parameter, tree);
                }


                //第一層
                searchWS.SearchTreeCompleted += (s1, args1) =>
                {

                    if (args1 == null || args1.Error != null)
                    {
                        this.RadBusyIndicator_AllSearchResult.IsBusy = false;
                        return;
                    }

                    if (args1.Result.ToString() == "")
                    {
                        this.RadBusyIndicator_AllSearchResult.IsBusy = false;
                        return;
                    }

                    BuildRefinerTreeLevelOne(args1.Result.ToString(), null);

                    if (this.RadTreeView_Refiner.Items != null && this.RadTreeView_Refiner.Items.Count > 0)
                    {
                        for (int i = 0; i <= this.RadTreeView_Refiner.Items.Count - 1; i++)
                        {
                            currentTreeViewItem = (RadTreeViewItem)this.RadTreeView_Refiner.Items[i];
                            searchWS1.SearchTreeAsync(parameter, ((RadTreeViewItem)this.RadTreeView_Refiner.Items[i]).Tag.ToString());
                        }
                    }
                };


                //第二層
                searchWS1.SearchTreeCompleted += (s2, args2) =>
                {
                    RadTreeViewItem tempTree = null;
                    tempTree = currentTreeViewItem;
                    if (args2 == null || args2.Error != null)
                    {
                        this.RadBusyIndicator_AllSearchResult.IsBusy = false;
                        return;
                    }

                    if (args2.Result.ToString() == "")
                    {
                        this.RadBusyIndicator_AllSearchResult.IsBusy = false;
                        return;
                    }
                    XDocument xmlTreeDoc = XDocument.Parse(args2.Result);
                    for (int i = 0; i <= this.RadTreeView_Refiner.Items.Count - 1; i++)
                    {
                        //比對母節點
                        if (xmlTreeDoc.Element("Tree").Attribute("Name").Value == ((RadTreeViewItem)this.RadTreeView_Refiner.Items[i]).Tag.ToString())
                        {
                            BuildRefinerTreeLevelOne(args2.Result.ToString(), (RadTreeViewItem)this.RadTreeView_Refiner.Items[i]);
                        }
                    }
                    this.RadBusyIndicator_AllSearchResult.IsBusy = false;
                    this.RadOutlookBar_Tree.IsMinimized = false;
                    this.Button_Show_Refiner.Content = "關閉分類";
                };

                //取得每頁筆數
                Int32.TryParse(xmlDocParameter.Element("Search").Element("PageSize").Value, out perPageCount);

                if (totalCount % perPageCount != 0)
                {
                    totalPageCount = (totalCount / perPageCount) + 1;
                }
                else
                {
                    totalPageCount = totalCount / perPageCount;
                }

                this.TextBlock_TotalPageCount.Text = totalPageCount.ToString();
                this.TextBox_PageIndex.Text = currentPageIndex.ToString();

                SearchResultClass_LIST = (from para in xmlDocResult.Descendants("Data")
                                          select new SearchResultClass
                                          {
                                              FSSYS_ID = para.Element("FSSYS_ID").Value,
                                              FSKEY_WORD = para.Element("FSKEY_WORD").Value,
                                              FSFILE_NO = para.Element("FSFILE_NO").Value,
                                              FSTITLE = para.Element("FSTITLE").Value,
                                              FSCONTENT = para.Element("FSCONTENT").Value,
                                              FDCREATED_DATE = para.Element("FDCREATED_DATE").Value,
                                              FSIMAGE_PATH = GetFileCategoryImage(para.Element("FSVWNAME").Value, para.Element("FCFROM").Value, para.Element("FSFILE_NO").Value, para.Element("FSIMAGE_PATH").Value),
                                              FSVWNAME = para.Element("FSVWNAME").Value,
                                              FSFILENO_VWNAME = para.Element("FSFILE_NO").Value + ";" + para.Element("FSVWNAME").Value,
                                              FCFROM = IsTape(para.Element("FCFROM").Value),
                                              _fsprog_name = para.Element("FSPROG_B_PGMNAME").Value,
                                              _fnepisode = para.Element("FNEPISODE").Value,
                                              _fsarc_type = para.Element("FSTYPE_NAME").Value,
                                              _fsprog_sub_name = para.Element("FSPROG_D_PGDNAME").Value,
                                              _view_metadata_height = ViewMetaDataHeight(para.Element("FSVWNAME").Value),
                                              _fstrack = para.Element("FSTRACK").Value,
                                              FBIS_CHECK = fnCHECK_IS_CHECK(para.Element("FSFILE_NO").Value, para.Element("FSVWNAME").Value)
                                          }
                            ).ToList();

                this.ListBox_SearchResult.SelectedIndex = -1;
                this.ListBox_SearchResult.ItemsSource = null;
                this.ListBox_SearchResult.ItemsSource = SearchResultClass_LIST;
                this.ScrollViewer_SearchResult.UpdateLayout();
                this.ScrollViewer_SearchResult.ScrollToTop();
                
                if (SearchResultClass_LIST.FirstOrDefault<SearchResultClass>(se => se.FBIS_CHECK == false) == null)
                {
                    this.CheckBox_Select_All.IsChecked = true;
                }
                else
                {
                    this.CheckBox_Select_All.IsChecked = false;
                }
                //this.ListBox_SearchResult.UpdateLayout();
                //this.ListBox_SearchResult.ScrollIntoView(this.ListBox_SearchResult.Items[0]);
                this.RadBusyIndicator_AllSearchResult.IsBusy = false;


            };

            searchWS.SearchAsync(parameter);

            //繫節詳細資料
            searchDb.GetSearchDetailDataCompleted += (s, args) =>
            {
                if (args.Error != null || String.IsNullOrEmpty(args.Result))
                {
                    this.RadBusyIndicator_AllSearchResult.IsBusy = false;
                    return;
                }


                XDocument xDoc = XDocument.Parse(args.Result.Replace("", ""));
                //欄位描述
                this.TextBlock_FileNo.HighlightText = _keyword;
                this.TextBlock_FileType.HighlightText = _keyword;
                this.TextBlock_Title.HighlightText = _keyword;
                this.TextBlock_TimeCode.HighlightText = _keyword;
                this.TextBlock_Track.HighlightText = _keyword;
                this.TextBlock_CreatedBy.HighlightText = _keyword;
                this.TextBlock_CreatedDate.HighlightText = _keyword;
                this.TextBlock_UpdatedBy.HighlightText = _keyword;
                this.TextBlock_UpdatedDate.HighlightText = _keyword;
                this.TextBlock_ExtName.HighlightText = _keyword;
                this.TextBlock_ArcType.HighlightText = _keyword;
                this.TextBlock_Channel.HighlightText = _keyword;
                this.TextBlock_PrgoName.HighlightText = _keyword;
                this.TextBlock_PrgoEp.HighlightText = _keyword;
                this.TextBlock_Cr_Type.HighlightText = _keyword;
                this.TextBlock_Cr_Note.HighlightText = _keyword;
                this.TextBlock_SubjectTitle.HighlightText = _keyword;
                this.TextBlock_SubjectContent.HighlightText = _keyword;
                this.TextBlock_Description.HighlightText = _keyword;
                this.TextBlock_Tran_By.HighlightText = _keyword;
                this.TextBlock_Tran_Date.HighlightText = _keyword;

                this.TextBlock_FileNo.Text = (xDoc.Element("Datas").Element("Data").Element("FSFILE_NO") != null ? xDoc.Element("Datas").Element("Data").Element("FSFILE_NO").Value.ToString() : "");
                this.TextBlock_FileType.Text = (xDoc.Element("Datas").Element("Data").Element("FSTYPE") != null ? xDoc.Element("Datas").Element("Data").Element("FSTYPE").Value.ToString() : "");
                this.TextBlock_Title.Text = (xDoc.Element("Datas").Element("Data").Element("FSTITLE") != null ? xDoc.Element("Datas").Element("Data").Element("FSTITLE").Value.ToString() : "");
                this.TextBlock_TimeCode.Text = (xDoc.Element("Datas").Element("Data").Element("FSTIME_CODE") != null ? xDoc.Element("Datas").Element("Data").Element("FSTIME_CODE").Value.ToString() : "");
                this.TextBlock_Track.Text = (xDoc.Element("Datas").Element("Data").Element("FSTRACK") != null ? xDoc.Element("Datas").Element("Data").Element("FSTRACK").Value.ToString() : "");
                this.TextBlock_CreatedBy.Text = (xDoc.Element("Datas").Element("Data").Element("FSCREATED_BY") != null ? xDoc.Element("Datas").Element("Data").Element("FSCREATED_BY").Value.ToString() : "");
                this.TextBlock_CreatedDate.Text = (xDoc.Element("Datas").Element("Data").Element("FDCREATED_DATE") != null ? xDoc.Element("Datas").Element("Data").Element("FDCREATED_DATE").Value.ToString() : "");
                this.TextBlock_UpdatedBy.Text = (xDoc.Element("Datas").Element("Data").Element("FSUPDATED_BY") != null ? xDoc.Element("Datas").Element("Data").Element("FSUPDATED_BY").Value.ToString() : "");
                this.TextBlock_UpdatedDate.Text = (xDoc.Element("Datas").Element("Data").Element("FDUPDATED_DATE") != null ? xDoc.Element("Datas").Element("Data").Element("FDUPDATED_DATE").Value.ToString() : "");
                this.TextBlock_ExtName.Text = (xDoc.Element("Datas").Element("Data").Element("FSFILE_TYPE") != null ? xDoc.Element("Datas").Element("Data").Element("FSFILE_TYPE").Value.ToString() : "");
                this.TextBlock_ArcType.Text = (xDoc.Element("Datas").Element("Data").Element("FSTYPE_NAME") != null ? xDoc.Element("Datas").Element("Data").Element("FSTYPE_NAME").Value.ToString() : "");
                this.TextBlock_Channel.Text = (xDoc.Element("Datas").Element("Data").Element("FSCHANNEL") != null ? xDoc.Element("Datas").Element("Data").Element("FSCHANNEL").Value.ToString() : "");
                this.TextBlock_PrgoName.Text = (xDoc.Element("Datas").Element("Data").Element("FSPROG_B_PGMNAME") != null ? xDoc.Element("Datas").Element("Data").Element("FSPROG_B_PGMNAME").Value.ToString() : "");
                this.TextBlock_PrgoEp.Text = (xDoc.Element("Datas").Element("Data").Element("FNEPISODE") != null ? xDoc.Element("Datas").Element("Data").Element("FNEPISODE").Value.ToString() : "");
                this.TextBlock_Cr_Type.Text = (xDoc.Element("Datas").Element("Data").Element("FSPROG_D_CR_TYPE") != null ? xDoc.Element("Datas").Element("Data").Element("FSPROG_D_CR_TYPE").Value.ToString() : ""); ;
                this.TextBlock_Cr_Note.Text = (xDoc.Element("Datas").Element("Data").Element("FSPROG_D_CR_NOTE") != null ? xDoc.Element("Datas").Element("Data").Element("FSPROG_D_CR_NOTE").Value.ToString() : ""); ;
                this.TextBlock_SubjectTitle.Text = (xDoc.Element("Datas").Element("Data").Element("FSSUBJECT_SUBJECT") != null ? xDoc.Element("Datas").Element("Data").Element("FSSUBJECT_SUBJECT").Value.ToString() : "");
                this.TextBlock_SubjectContent.Text = (xDoc.Element("Datas").Element("Data").Element("FSSUBJECT_DESCRIPTION") != null ? xDoc.Element("Datas").Element("Data").Element("FSSUBJECT_DESCRIPTION").Value.ToString() : "");
                this.TextBlock_Description.Text = (xDoc.Element("Datas").Element("Data").Element("FSDESCRIPTION") != null ? xDoc.Element("Datas").Element("Data").Element("FSDESCRIPTION").Value.ToString() : "");
                this.TextBlock_Tran_By.Text = (xDoc.Element("Datas").Element("Data").Element("FSDESCRIPTION") != null ? xDoc.Element("Datas").Element("Data").Element("FSTRAN_BY").Value.ToString() : "");
                this.TextBlock_Tran_Date.Text = (xDoc.Element("Datas").Element("Data").Element("FSDESCRIPTION") != null ? xDoc.Element("Datas").Element("Data").Element("FDTRAN_DATE").Value.ToString() : "");

                //節目資訊
                this.TextBlock_ProgID.HighlightText = _keyword;
                this.TextBlock_ProgName.HighlightText = _keyword;
                this.TextBlock_ProgContent.HighlightText = _keyword;
                this.TextBlock_ProgMemo.HighlightText = _keyword;
                this.TextBlock_ProgSubName.HighlightText = _keyword;
                this.TextBlock_ProgSubContent.HighlightText = _keyword;
                this.TextBlock_ProgSubMemo.HighlightText = _keyword;
                this.TextBlock_ProgGraceArea.HighlightText = _keyword;
                this.TextBlock_ProgGraceName.HighlightText = _keyword;
                this.TextBlock_ProgGraceRaceDate.HighlightText = _keyword;
                this.TextBlock_ProgGraceStatus.HighlightText = _keyword;
                this.TextBlock_ProgGracePeriod.HighlightText = _keyword;
                this.TextBlock_ProgGraceWin.HighlightText = _keyword;
                this.TextBlock_ProgGraceProgName.HighlightText = _keyword;
                this.TextBlock_ProgGraceMenName.HighlightText = _keyword;
                this.TextBlock_ProgGraceMenMemo.HighlightText = _keyword;
                this.TextBlock_ProgProducer.HighlightText = _keyword;

                this.TextBlock_ProgID.Text = (xDoc.Element("Datas").Element("Data").Element("FSPROD_NO") != null ? xDoc.Element("Datas").Element("Data").Element("FSPROD_NO").Value.ToString() : "");
                this.TextBlock_ProgName.Text = (xDoc.Element("Datas").Element("Data").Element("FSPROG_B_PGMNAME") != null ? xDoc.Element("Datas").Element("Data").Element("FSPROG_B_PGMNAME").Value.ToString() : "");
                this.TextBlock_ProgContent.Text = (xDoc.Element("Datas").Element("Data").Element("FSPROG_B_CONTENT") != null ? xDoc.Element("Datas").Element("Data").Element("FSPROG_B_CONTENT").Value.ToString() : "");
                this.TextBlock_ProgMemo.Text = (xDoc.Element("Datas").Element("Data").Element("FSPROG_B_MEMO") != null ? xDoc.Element("Datas").Element("Data").Element("FSPROG_B_MEMO").Value.ToString() : "");
                this.TextBlock_ProgSubName.Text = (xDoc.Element("Datas").Element("Data").Element("FSPROG_D_PGDNAME") != null ? xDoc.Element("Datas").Element("Data").Element("FSPROG_D_PGDNAME").Value.ToString() : "");
                this.TextBlock_ProgSubContent.Text = (xDoc.Element("Datas").Element("Data").Element("FSPROG_D_CONTENT") != null ? xDoc.Element("Datas").Element("Data").Element("FSPROG_D_CONTENT").Value.ToString() : "");
                this.TextBlock_ProgSubMemo.Text = (xDoc.Element("Datas").Element("Data").Element("FSPROG_D_MEMO") != null ? xDoc.Element("Datas").Element("Data").Element("FSPROG_D_MEMO").Value.ToString() : "");
                this.TextBlock_ProgGraceArea.Text = (xDoc.Element("Datas").Element("Data").Element("FSPROGRACE_AREA") != null ? xDoc.Element("Datas").Element("Data").Element("FSPROGRACE_AREA").Value.ToString() : "");
                this.TextBlock_ProgGraceName.Text = (xDoc.Element("Datas").Element("Data").Element("FSPROGRACE_PROGRACE") != null ? xDoc.Element("Datas").Element("Data").Element("FSPROGRACE_PROGRACE").Value.ToString() : "");
                this.TextBlock_ProgGraceRaceDate.Text = (xDoc.Element("Datas").Element("Data").Element("FDPROGRACE_RACEDATE") != null ? (xDoc.Element("Datas").Element("Data").Element("FDPROGRACE_RACEDATE").Value.ToString().IndexOf("1900/1/1") > -1 ? "" : xDoc.Element("Datas").Element("Data").Element("FDPROGRACE_RACEDATE").Value.ToString()) : "");
                this.TextBlock_ProgGraceStatus.Text = (xDoc.Element("Datas").Element("Data").Element("FSPROGRACE_PROGSTATUS") != null ? xDoc.Element("Datas").Element("Data").Element("FSPROGRACE_PROGSTATUS").Value.ToString() : "");
                this.TextBlock_ProgGracePeriod.Text = (xDoc.Element("Datas").Element("Data").Element("FNPROGRACE_PERIOD") != null ? xDoc.Element("Datas").Element("Data").Element("FNPROGRACE_PERIOD").Value.ToString() : "");
                this.TextBlock_ProgGraceWin.Text = (xDoc.Element("Datas").Element("Data").Element("FSPROGRACE_PROGWIN") != null ? xDoc.Element("Datas").Element("Data").Element("FSPROGRACE_PROGWIN").Value.ToString() : "");
                this.TextBlock_ProgGraceProgName.Text = (xDoc.Element("Datas").Element("Data").Element("FSPROGRACE_PROGNAME") != null ? xDoc.Element("Datas").Element("Data").Element("FSPROGRACE_PROGNAME").Value.ToString() : "");
                this.TextBlock_ProgGraceMenName.Text = (xDoc.Element("Datas").Element("Data").Element("FSPROGRACE_NAME") != null ? xDoc.Element("Datas").Element("Data").Element("FSPROGRACE_NAME").Value.ToString() : "");
                this.TextBlock_ProgGraceMenMemo.Text = (xDoc.Element("Datas").Element("Data").Element("FSPROGRACE_MEMO") != null ? xDoc.Element("Datas").Element("Data").Element("FSPROGRACE_MEMO").Value.ToString() : "");
                this.TextBlock_ProgProducer.Text = (xDoc.Element("Datas").Element("Data").Element("FSPROG_PRODUCER") != null ? xDoc.Element("Datas").Element("Data").Element("FSPROG_PRODUCER").Value.ToString() : "");

                //影帶編號&轉檔來源
                this.TextBlock_TapeNo.Text = (xDoc.Element("Datas").Element("Data").Element("FSTAPE_ID") != null ? xDoc.Element("Datas").Element("Data").Element("FSTAPE_ID").Value.ToString() : "");
                this.TextBlock_TranFrom.Text = (xDoc.Element("Datas").Element("Data").Element("FSTRANS_FROM") != null ? xDoc.Element("Datas").Element("Data").Element("FSTRANS_FROM").Value.ToString() : "");

                //內容
                this.TextBlock_Content.HighlightText = _keyword;
                this.TextBlock_Content.Text = (xDoc.Element("Datas").Element("Data").Element("FSCONTENT") != null ? xDoc.Element("Datas").Element("Data").Element("FSCONTENT").Value.ToString() : "");
                this.ScrollViewer_Content.UpdateLayout();
                this.ScrollViewer_Content.ScrollToTop();

                //Script
                this.TextBlock_Script.HighlightText = _keyword;
                this.TextBlock_Script.Text = (xDoc.Element("Datas").Element("Data").Element("SCRIPT") != null ? xDoc.Element("Datas").Element("Data").Element("SCRIPT").Value.ToString() : "");
                this.ScrollViewer_Script.UpdateLayout();
                this.ScrollViewer_Script.ScrollToTop();

                //EnpsScript
                this.TextBlock_EnpsScript.HighlightText = _keyword;
                this.TextBlock_EnpsScript.Text = (xDoc.Element("Datas").Element("Data").Element("ENPSSCRIPT") != null ? xDoc.Element("Datas").Element("Data").Element("ENPSSCRIPT").Value.ToString() : "");
                this.ScrollViewer_EnpsScript.UpdateLayout();
                this.ScrollViewer_EnpsScript.ScrollToTop();

                //版權
                this.TextBlock_License.Text = (xDoc.Element("Datas").Element("Data").Element("FCLICENSE_NAME") != null ? xDoc.Element("Datas").Element("Data").Element("FCLICENSE_NAME").Value.ToString() : "");

                //版權描述
                this.TextBlock_License_Description.Text = (xDoc.Element("Datas").Element("Data").Element("FSLICENSEDESC") != null ? xDoc.Element("Datas").Element("Data").Element("FSLICENSEDESC").Value.ToString() : "");

                //檔案格式資訊
                searchDb.GetVideoPropertyAsync("<Data><FSFILE_NO>" + (xDoc.Element("Datas").Element("Data").Element("FSFILE_NO") != null ? xDoc.Element("Datas").Element("Data").Element("FSFILE_NO").Value.ToString() : "") + "</FSFILE_NO></Data>");

                //圖片資訊
                this.RadTreeView_PhotoExif.Items.Clear();
                if (xDoc.Element("Datas").Element("Data").Element("FSVWNAME").Value == "VW_SEARCH_PHOTO")
                {
                    string fsEXIF = xDoc.Element("Datas").Element("Data").Element("FSEXIF").Value;
                    if (!string.IsNullOrEmpty(fsEXIF))
                    {
                        XDocument xDocExifProperty = XDocument.Parse(fsEXIF);
                        if (xDocExifProperty.Element("EXIFS_ROOT").Nodes().Count() > 0)
                        {
                            for (int i = 0; i < xDocExifProperty.Element("EXIFS_ROOT").Nodes().Count(); i++)
                            {
                               RadTreeViewItem t = new RadTreeViewItem();
                               t.Header = xDocExifProperty.Element("EXIFS_ROOT").Elements().ElementAt(i).Name + "：" +
                                            xDocExifProperty.Element("EXIFS_ROOT").Elements().ElementAt(i).Value;
                               this.RadTreeView_PhotoExif.Items.Add(t);
 
                            }
                        }
                    }
                }

                this.RadBusyIndicator_AllSearchResult.IsBusy = false;


            };


            searchDb.GetVideoPropertyCompleted += (s, args) =>
            {
                this.RadTreeView_VideoProperty.Items.Clear();
                if (args.Error != null || string.IsNullOrEmpty(args.Result))
                {
                    return;
                }
                XDocument xDoc = XDocument.Parse(args.Result);
                if (xDoc.Element("Datas").Element("Data") == null)
                {
                    return;
                }
                string xml = (xDoc.Element("Datas").Element("Data").Element("FSVIDEO_PROPERTY") != null ? xDoc.Element("Datas").Element("Data").Element("FSVIDEO_PROPERTY").Value.ToString() : "");
                if (xml != "")
                {
                    //MessageBox.Show(xml);
                    XDocument xDocVideoProperty = XDocument.Parse(xml);
                    if (xDocVideoProperty.Element("Mediainfo").Element("File").Elements("track").Count() > 0)
                    {

                        for (int i = 0; i <= xDocVideoProperty.Element("Mediainfo").Element("File").Elements("track").Count() - 1; i++)
                        {
                            RadTreeViewItem t1 = new RadTreeViewItem();
                            t1.Header = xDocVideoProperty.Element("Mediainfo").Element("File").Elements("track").ElementAt(i).Attribute("type").Value;
                            //")";

                            for (int j = 0; j <= xDocVideoProperty.Element("Mediainfo").Element("File").Elements("track").ElementAt(i).Nodes().Count() - 1; j++)
                            {
                                RadTreeViewItem t2 = new RadTreeViewItem();
                                if (xDocVideoProperty.Element("Mediainfo").Element("File").Elements("track").ElementAt(i).Elements().ElementAt(j).Name == "Complete_name")
                                {

                                }
                                else
                                {
                                    t2.Header = xDocVideoProperty.Element("Mediainfo").Element("File").Elements("track").ElementAt(i).Elements().ElementAt(j).Name + "：" +
                                            xDocVideoProperty.Element("Mediainfo").Element("File").Elements("track").ElementAt(i).Elements().ElementAt(j).Value;
                                    t1.Items.Add(t2);
                                }
                            }

                            this.RadTreeView_VideoProperty.Items.Add(t1);
                        }
                    }
                }
                this.RadTreeView_VideoProperty.ExpandAll();

            };

        }


        public void Register()
        {
            bookDb.CheckProgIsExpireCompleted += new EventHandler<CheckProgIsExpireCompletedEventArgs>(bookDb_CheckProgIsExpireCompleted);
            bookDb.GetFileExistPreBooking2Completed += new EventHandler<GetFileExistPreBooking2CompletedEventArgs>(bookDb_GetFileExistPreBooking2Completed);
            bookDb.GetFileInformation2Completed += new EventHandler<GetFileInformation2CompletedEventArgs>(bookDb_GetFileInformation2Completed);
        }

        

        public static string ConvertWhitespaceToSpacesRegex(string value)
        {
            value = Regex.Replace(value, "[\a\b\f\n\r\t\v]", "");
            return value;
        }

        private int ViewMetaDataHeight(string _fsfile_type)
        {
            if (_fsfile_type.ToLower().IndexOf("news") > -1)
            {
                return 0;
            }
            else
            {
                return 24;
            }
        }


        private string GetFileCategoryImage(string category, string tape, string fsfile_no, string path)
        {
            if (path == "" && category == "VW_SEARCH_VIDEO" && tape != "T")
            {
                return "/PTS_MAM3;component/Images/Video.png";
            }
            else if (category == "VW_SEARCH_VIDEO" && tape == "T")
            {
                //return "/PTS_MAM3;component/Images/tape.png";
                return "../Images/tape.png";
            }
            //else if (category == "VW_SEARCH_AUDIO")
            //{
            //    //return "/PTS_MAM3;component/Images/audio.png";
            //    return "../Images/audio.png";
            //}
            else if (path == "" && category == "VW_SEARCH_PHOTO")
            {
                //return "/PTS_MAM3;component/Images/photo.png";
                return "../Images/photo.png";
            }
            //else if (category == "VW_SEARCH_DOC")
            //{
            //    //return "/PTS_MAM3;component/Images/txt.png";
            //    return "../Images/txt.png";
            //}
            //else if (category == "VW_NEWS")
            //{
            //    return "../Images/news.png";

            //}
            else
            {
                return path;
            }
        }


        private string FileTypeToeCategory(string type)
        {

            if (type == "VW_SEARCH_VIDEO")
            {
                return "1";
            }
            else if (type == "VW_SEARCH_AUDIO")
            {
                return "2";
            }
            else if (type == "VW_SEARCH_PHOTO")
            {
                return "3";
            }
            else if (type == "VW_SEARCH_DOC")
            {
                return "4";
            }
            else if (type == "VW_NEWS")
            {
                return "5";
            }
            else
            {
                return "";
            }
        }

        //建樹
        private void BuildRefinerTreeLevelOne(string tree, RadTreeViewItem treeViewItem)
        {
            XDocument xmlTreeDoc = XDocument.Parse(tree);

            if (treeViewItem != null)
            {
                treeViewItem.Items.Clear();
            }

            var treeParas = from para in xmlTreeDoc.Descendants("TreeChild")
                            select new SearchTreeClass
                            {
                                name = para.Attribute("Name").Value,
                                count = para.Attribute("Count").Value,
                            };

            foreach (SearchTreeClass t in treeParas)
            {
                RadTreeViewItem treeItem = new RadTreeViewItem();
                treeItem.Header = t.name + " (" + t.count.ToString() + ")";
                if (xmlTreeDoc.Element("Tree").Attribute("Name").Value == "/")
                {
                    treeItem.Tag = xmlTreeDoc.Element("Tree").Attribute("Name").Value + t.name;
                }
                else
                {
                    treeItem.Tag = xmlTreeDoc.Element("Tree").Attribute("Name").Value + "/" + t.name;
                }


                if (treeViewItem == null)
                {
                    this.RadTreeView_Refiner.Items.Add(treeItem);
                }
                else
                {
                    treeViewItem.Items.Add(treeItem);
                }

            }

            this.RadTreeView_Refiner.ExpandAll();
        }


        //第一頁
        private void Button_First_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            isPaging = true;
            this.RadBusyIndicator_AllSearchResult.IsBusy = true;
            currentPageIndex = 1;

            XDocument xDoc = XDocument.Parse(parameter);
            xDoc.Element("Search").Element("StartPoint").SetValue(((currentPageIndex - 1) * perPageCount).ToString());

            parameter = xDoc.ToString();
            searchWS.SearchAsync(parameter);
        }

        //最後一頁
        private void Button_Last_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            isPaging = true;
            this.RadBusyIndicator_AllSearchResult.IsBusy = true;
            currentPageIndex = totalPageCount;
            XDocument xDoc = XDocument.Parse(parameter);
            xDoc.Element("Search").Element("StartPoint").SetValue(((currentPageIndex - 1) * perPageCount).ToString());

            this.TextBox_PageIndex.Text = currentPageIndex.ToString();
            parameter = xDoc.ToString();
            searchWS.SearchAsync(parameter);
        }

        //前一頁
        private void Button_Previous_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            isPaging = true;
            if (currentPageIndex == 1)
            {
                MessageBox.Show("已經是第一頁!");
                return;
            }
            this.RadBusyIndicator_AllSearchResult.IsBusy = true;
            currentPageIndex -= 1;
            XDocument xDoc = XDocument.Parse(parameter);
            xDoc.Element("Search").Element("StartPoint").SetValue(((currentPageIndex - 1) * perPageCount).ToString());

            this.TextBox_PageIndex.Text = currentPageIndex.ToString();
            parameter = xDoc.ToString();
            searchWS.SearchAsync(parameter);

        }

        //下一頁
        private void Button_Next_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            isPaging = true;
            if (currentPageIndex == totalPageCount)
            {
                MessageBox.Show("已經是最後一頁!");
                return;
            }
            this.RadBusyIndicator_AllSearchResult.IsBusy = true;
            currentPageIndex += 1;
            XDocument xDoc = XDocument.Parse(parameter);
            xDoc.Element("Search").Element("StartPoint").SetValue(((currentPageIndex - 1) * perPageCount).ToString());

            this.TextBox_PageIndex.Text = currentPageIndex.ToString();
            parameter = xDoc.ToString();
            searchWS.SearchAsync(parameter);

        }



        private void Button_MoveTo_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            isPaging = true;
            int moveToPageIndex = 0;
            if (this.TextBox_PageIndex.Text != "")
            {
                if (Int32.TryParse(this.TextBox_PageIndex.Text, out moveToPageIndex))
                {
                    if (moveToPageIndex > 0 && moveToPageIndex <= totalPageCount)
                    {
                        this.RadBusyIndicator_AllSearchResult.IsBusy = true;
                        currentPageIndex = moveToPageIndex;
                        XDocument xDoc = XDocument.Parse(parameter);
                        xDoc.Element("Search").Element("StartPoint").SetValue(((currentPageIndex - 1) * perPageCount).ToString());

                        this.TextBox_PageIndex.Text = currentPageIndex.ToString();
                        parameter = xDoc.ToString();
                        searchWS.SearchAsync(parameter);
                    }
                    else
                    {
                        MessageBox.Show("輸入頁數錯誤!");
                    }
                }
                else
                {
                    MessageBox.Show("頁數請輸入數字!");
                }
            }

        }

        private void RadTreeView_Refiner_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            //MessageBox.Show(((RadTreeViewItem)this.RadTreeView_Refiner.SelectedItem).Tag.ToString());
            //this.RadOutlookBar_Tree.IsMinimized = true;
            //目前頁次
            currentPageIndex = 1;
            //總頁次
            totalPageCount = 0;
            //總筆數
            totalCount = 0;
            this.RadBusyIndicator_AllSearchResult.IsBusy = true;
            //SearchServiceClient searchWS = new SearchServiceClient();
            //SearchServiceClient searchWS1 = new SearchServiceClient();

            SearchServiceSoapClient searchWS = new SearchServiceSoapClient();
            SearchServiceSoapClient searchWS1 = new SearchServiceSoapClient();

            //SearchServiceSoapClient searchWS = new SearchServiceSoapClient();
            //SearchServiceSoapClient searchWS1 = new SearchServiceSoapClient();

            XDocument xmlDocParameter = XDocument.Parse(parameter);

            xmlDocParameter.Element("Search").Element("TreeNode").Value = ((RadTreeViewItem)this.RadTreeView_Refiner.SelectedItem).Tag.ToString();
            xmlDocParameter.Element("Search").Element("StartPoint").SetValue(((currentPageIndex - 1) * perPageCount).ToString());
            parameter = xmlDocParameter.ToString();

            searchWS.SearchCompleted += (s, args) =>
            {
                if (args == null || args.Error != null || String.IsNullOrEmpty(args.Result))
                {
                    this.RadBusyIndicator_AllSearchResult.IsBusy = false;
                    return;
                }

                //過濾字元字串
                XDocument xmlDocResult = XDocument.Parse(args.Result.ToString().Replace("&", "&amp;").Replace("&lt", "").Replace("&gt", ""));

                //取得總筆數
                Int32.TryParse(xmlDocResult.Element("Datas").Attribute("count").Value, out totalCount);

                //取得時間
                this.TextBlock_SearchCount.Text = "有 " + xmlDocResult.Element("Datas").Attribute("count").Value + " 項結果 (搜尋時間：" + xmlDocResult.Element("Datas").Attribute("time").Value + " 秒) ";

                if (totalCount == 0)
                {
                    MessageBox.Show("查無資料");
                    return;
                }

                //取得每頁筆數
                Int32.TryParse(xmlDocParameter.Element("Search").Element("PageSize").Value, out perPageCount);

                if (totalCount % perPageCount != 0)
                {
                    totalPageCount = (totalCount / perPageCount) + 1;
                }
                else
                {
                    totalPageCount = totalCount / perPageCount;
                }

                this.TextBlock_TotalPageCount.Text = totalPageCount.ToString();
                this.TextBox_PageIndex.Text = currentPageIndex.ToString();

                SearchResultClass_LIST = (from para in xmlDocResult.Descendants("Data")
                                          select new SearchResultClass
                                          {
                                              FSSYS_ID = para.Element("FSSYS_ID").Value,
                                              FSKEY_WORD = para.Element("FSKEY_WORD").Value,
                                              FSFILE_NO = para.Element("FSFILE_NO").Value,
                                              FSTITLE = para.Element("FSTITLE").Value,
                                              FSCONTENT = para.Element("FSCONTENT").Value,
                                              FDCREATED_DATE = para.Element("FDCREATED_DATE").Value,
                                              FSIMAGE_PATH = GetFileCategoryImage(para.Element("FSVWNAME").Value, para.Element("FCFROM").Value, para.Element("FSFILE_NO").Value, para.Element("FSIMAGE_PATH").Value),
                                              FSVWNAME = para.Element("FSVWNAME").Value,
                                              FSFILENO_VWNAME = para.Element("FSFILE_NO").Value + ";" + para.Element("FSVWNAME").Value,
                                              FCFROM = IsTape(para.Element("FCFROM").Value),
                                              _fsprog_name = para.Element("FSPROG_B_PGMNAME").Value,
                                              _fnepisode = para.Element("FNEPISODE").Value,
                                              _fsarc_type = para.Element("FSTYPE_NAME").Value,
                                              _fsprog_sub_name = para.Element("FSPROG_D_PGDNAME").Value,
                                              _view_metadata_height = ViewMetaDataHeight(para.Element("FSVWNAME").Value),
                                              _fstrack = para.Element("FSTRACK").Value,
                                              FBIS_CHECK = fnCHECK_IS_CHECK(para.Element("FSFILE_NO").Value, para.Element("FSVWNAME").Value)
                                          }).ToList();


                this.ListBox_SearchResult.SelectedIndex = -1;
                this.ListBox_SearchResult.ItemsSource = null;
                this.ListBox_SearchResult.ItemsSource = SearchResultClass_LIST;
                this.ScrollViewer_SearchResult.UpdateLayout();
                this.ScrollViewer_SearchResult.ScrollToTop();
                //this.ListBox_SearchResult.UpdateLayout();
                //this.ListBox_SearchResult.ScrollIntoView(this.ListBox_SearchResult.Items[0]);
                //this.RadBusyIndicator_AllSearchResult.IsBusy = false;
                if (SearchResultClass_LIST.FirstOrDefault<SearchResultClass>(se => se.FBIS_CHECK == false) == null)
                {
                    this.CheckBox_Select_All.IsChecked = true;
                }
                else
                {
                    this.CheckBox_Select_All.IsChecked = false;
                    flag = true;
                }

                searchWS1.SearchTreeAsync(parameter, ((RadTreeViewItem)this.RadTreeView_Refiner.SelectedItem).Tag.ToString());
            };


            searchWS1.SearchTreeCompleted += (s1, args1) =>
            {
                if (args1 == null || args1.Error != null)
                {
                    this.RadBusyIndicator_AllSearchResult.IsBusy = false;
                    return;
                }

                if (args1.Result.ToString() == "")
                {
                    this.RadBusyIndicator_AllSearchResult.IsBusy = false;
                    return;
                }
                XDocument xmlTreeDoc = XDocument.Parse(args1.Result.ToString().Replace("&", "&amp;"));


                BuildRefinerTreeLevelOne(args1.Result.ToString(), (RadTreeViewItem)this.RadTreeView_Refiner.SelectedItem);
                this.RadBusyIndicator_AllSearchResult.IsBusy = false;
            };

            searchWS.SearchAsync(parameter);

        }

        //選取單一資料，繫結詳細資料
        private void ListBox_SearchResult_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            ClearControl();
            if (this.ListBox_SearchResult.SelectedItem != null)
            {
                this.RadBusyIndicator_AllSearchResult.IsBusy = true;
                string vwname = ((SearchResultClass)this.ListBox_SearchResult.SelectedItem).FSVWNAME;
                string fssys_id = ((SearchResultClass)this.ListBox_SearchResult.SelectedItem).FSSYS_ID;
                string fsfile_no = ((SearchResultClass)this.ListBox_SearchResult.SelectedItem).FSFILE_NO;
                string fstitle = string.Empty;

                if (string.IsNullOrEmpty(((SearchResultClass)this.ListBox_SearchResult.SelectedItem).FSTITLE))
                {
                    fstitle = ((SearchResultClass)this.ListBox_SearchResult.SelectedItem)._fsprog_sub_name;
                }
                else
                {
                    fstitle = ((SearchResultClass)this.ListBox_SearchResult.SelectedItem).FSTITLE;
                }

                this.RadOutlookBar_Detail[5].Visibility = System.Windows.Visibility.Collapsed;
                this.RadOutlookBar_Detail[6].Visibility = System.Windows.Visibility.Collapsed;
                this.RadOutlookBar_Detail[7].Visibility = System.Windows.Visibility.Collapsed;

                this.TextBox_HH.Text = "";
                this.TextBox_MM.Text = "";
                this.TextBox_SS.Text = "";
                this.TextBox_FF.Text = "";

                if (string.IsNullOrEmpty(vwname) || string.IsNullOrEmpty(fssys_id) || string.IsNullOrEmpty(fsfile_no))
                {
                    this.RadBusyIndicator_AllSearchResult.IsBusy = false;
                    MessageBox.Show("查無資料!");
                    return;
                }


                string detailDataParameter = "<Data><FSVW_NAME>" + vwname + "</FSVW_NAME><FSSYS_ID>" + fssys_id + "</FSSYS_ID></Data>";
                string hothitParameter = "<Data><FSSYS_ID>" + fssys_id + "</FSSYS_ID><FSTITLE>" + fstitle + "</FSTITLE><FSINDEX_TYPE>" + vwname + "</FSINDEX_TYPE>";
                hothitParameter += "<FSUSER_ID>" + UserClass.userData.FSUSER_ID + "</FSUSER_ID><FDDATE>" + DateTime.Now.ToString("yyyy/MM/dd") + "</FDDATE></Data>";
                searchDb.GetSearchDetailDataAsync(detailDataParameter);

                this.Grid_Video.Children.Clear();

                //寫入熱門點閱
                searchDb.InsertSearchHotHitAsync(hothitParameter, UserClass.userData.FSUSER_ID);
                //Video

                //取得start timecode
                WSBookingFunctionSoapClient booking = new WSBookingFunctionSoapClient();
                ServiceTSMSoapClient tsm_video = new ServiceTSMSoapClient();
                ServiceTSMSoapClient tsm_photo = new ServiceTSMSoapClient();
                SearchDataBaseFunctionClient searchDbFunction = new SearchDataBaseFunctionClient();

                booking.GetFileStartTimeCodeCompleted += (s, args) =>
                {
                    if ((args.Error != null) || String.IsNullOrEmpty(args.Result))
                    {
                        this.RadBusyIndicator_AllSearchResult.IsBusy = false;
                        return;
                    }

                    XDocument xDoc = XDocument.Parse(args.Result);
                    if (xDoc.Element("Datas").Value != "")
                    {
                        startTimeCode = xDoc.Element("Datas").Element("Data").Element("FSBEG_TIMECODE").Value;
                    }

                    tsm_video.GetHttpPathByFileIDAsync(fsfile_no, FileID_MediaType.LowVideo);

                };


                //撈取影片檔案路徑

                tsm_video.GetHttpPathByFileIDCompleted += (s, args) =>
                {
                    if ((args.Error != null) || String.IsNullOrEmpty(args.Result))
                    {
                        this.RadBusyIndicator_AllSearchResult.IsBusy = false;
                        return;
                    }

                    if (FileTypeToeCategory(vwname) == "1")
                    {
                        this.Grid_Video.Children.Clear();
                        player.HTML_SetStartFrame(long.Parse(TransferTimecode.timecodetoframe(startTimeCode).ToString()));
                        player.HTML_SetURL(args.Result, TransferTimecode.timecodetoframe(startTimeCode));
                        //將Video加置Grid
                        this.Grid_Video.Children.Add(player);
                        this.RadBusyIndicator_AllSearchResult.IsBusy = false;
                    }

                };


                //取得段落資訊
                searchDbFunction.GetVideoSegmentCompleted += (s, args) =>
                {
                    if ((args.Error != null) || String.IsNullOrEmpty(args.Result))
                    {
                        this.RadBusyIndicator_AllSearchResult.IsBusy = false;
                        return;
                    }

                    XDocument xmlDocResult = XDocument.Parse(args.Result);
                    var paras = from para in xmlDocResult.Descendants("Data")
                                select new VideoSegmentClass
                                {
                                    begin_timecode = para.Element("FSBEG_TIMECODE").Value,
                                    end_timecode = para.Element("FSEND_TIMECODE").Value
                                };

                    this.DataGrid_VideoSegment.ItemsSource = null;
                    this.DataGrid_VideoSegment.ItemsSource = paras;
                };

                //影帶資訊

                searchDbFunction.GetTapeDataCompleted += (s, args) =>
                {
                    if ((args.Error != null) || String.IsNullOrEmpty(args.Result))
                    {
                        this.RadBusyIndicator_AllSearchResult.IsBusy = false;
                        return;
                    }

                    if (FileTypeToeCategory(vwname) == "1")
                    {
                        XDocument xDoc = XDocument.Parse(args.Result);
                        if (xDoc.Element("Datas").Value != "")
                        {
                            this.TextBlock_TapeKind.Text = xDoc.Element("Datas").Element("Data").Element("FSKIND").Value;
                            this.TextBlock_TapeCategory.Text = xDoc.Element("Datas").Element("Data").Element("FSCATEGORY").Value;
                            //this.TextBlock_TapeVolume.Text = xDoc.Element("Datas").Element("Data").Element("FSVOLUMN").Value;
                            //this.TextBlock_TapeLocation.Text = xDoc.Element("Datas").Element("Data").Element("FSLOCATION").Value;
                        }
                    }
                    this.RadBusyIndicator_AllSearchResult.IsBusy = false;

                };



                //撈取節目片庫keyframe
                tsm_video.GetKeyframeFilesInfoCompleted += (s, args) =>
                {

                    this.ListBox_KeyFrame.ItemsSource = null;
                    if ((args.Error != null) || args.Result == null)
                    {
                        this.RadBusyIndicator_AllSearchResult.IsBusy = false;
                        return;
                    }
                    List<KeyFrameClass> KeyFrameClassList = new List<KeyFrameClass>();

                    foreach (PTS_MAM3.WSTSM_GetFileInfo.KeyframeInfo obj in args.Result)
                    {
                        KeyFrameClass k = new KeyFrameClass();
                        string time = obj.KeyframeHttpPath.Substring(obj.KeyframeHttpPath.IndexOf('_') + 1, 8);
                        string hh = time.Substring(0, 2);
                        string mm = time.Substring(2, 2);
                        string ss = time.Substring(4, 2);
                        string ff = time.Substring(6, 2);

                        k.path = obj.KeyframeHttpPath;
                        k.desc = hh + ":" + mm + ":" + ss + ":" + ff + (string.IsNullOrEmpty(obj.KeyframeDescription) ? "" : "：" + obj.KeyframeDescription);
                        k.sec = TransferTimecode.timecodetoframe(hh + ":" + mm + ":" + ss + ":" + ff).ToString();
                        KeyFrameClassList.Add(k);
                    }

                    if (KeyFrameClassList != null && KeyFrameClassList.Count() > 0)
                    {
                        this.ListBox_KeyFrame.ItemsSource = KeyFrameClassList;
                    }
                    this.RadBusyIndicator_AllSearchResult.IsBusy = false;

                };



                //撈取圖
                tsm_photo.GetHttpPathByFileIDCompleted += (s, args) =>
                {
                    if ((args.Error != null) || String.IsNullOrEmpty(args.Result))
                    {
                        this.RadBusyIndicator_AllSearchResult.IsBusy = false;
                        return;
                    }

                    Image img = new Image();
                    img.Width = 320;
                    img.Height = 320;
                    img.Source = new BitmapImage(new Uri(args.Result, UriKind.Absolute));
                    this.Grid_Video.Children.Add(img);

                    this.RadBusyIndicator_AllSearchResult.IsBusy = false;

                };

                //撈取文


                //撈取新聞檔案路徑
                booking.GetNewsLVPathCompleted += (s, args) =>
                {
                    if ((args.Error != null) || String.IsNullOrEmpty(args.Result))
                    {
                        this.RadBusyIndicator_AllSearchResult.IsBusy = false;
                        return;
                    }

                    //取得開始frame
                    booking.GetNewsStartFrameCompleted += (s1, args1) =>
                    {
                        if ((args1.Error != null) || String.IsNullOrEmpty(args1.Result))
                        {
                            this.RadBusyIndicator_AllSearchResult.IsBusy = false;
                            return;
                        }

                        XDocument xDoc = XDocument.Parse(args1.Result);

                        if (FileTypeToeCategory(vwname) == "5")
                        {
                            player.HTML_SetURL(args.Result, long.Parse(xDoc.Element("Datas").Element("Data").Element("FSTIMECODE1").Value));
                            //將Video加置Grid
                            this.Grid_Video.Children.Add(player);
                        }
                        this.RadBusyIndicator_AllSearchResult.IsBusy = false;
                    };


                    booking.GetNewsStartFrameAsync("<Data><FSFILE_NO>" + fssys_id.Split('-')[0] + "</FSFILE_NO><FNSEQ_NO>" + fssys_id.Split('-')[1] + "</FNSEQ_NO></Data>");

                };


                //撈取新聞片庫KeyFrame
                booking.GetNewsKeyFramePathCompleted += (s, args) =>
                {
                    this.ListBox_KeyFrame.ItemsSource = null;
                    if ((args.Error != null) || args.Result == null)
                    {
                        this.RadBusyIndicator_AllSearchResult.IsBusy = false;
                        return;
                    }

                    List<KeyFrameClass> KeyFrameClassList = new List<KeyFrameClass>();

                    foreach (PTS_MAM3.WSBookingFunction.KeyframeInfo obj in args.Result)
                    {
                        KeyFrameClass k = new KeyFrameClass();
                        k.path = obj.KeyframeHttpPath;
                        k.desc = obj.KeyframeDescription;
                        KeyFrameClassList.Add(k);
                    }

                    if (KeyFrameClassList != null && KeyFrameClassList.Count() > 0)
                    {
                        this.ListBox_KeyFrame.ItemsSource = KeyFrameClassList;
                    }

                    this.RadBusyIndicator_AllSearchResult.IsBusy = false;


                };


                if (FileTypeToeCategory(vwname) == "1")
                {
                    booking.GetFileStartTimeCodeAsync("<Data><FSFILE_NO>" + fsfile_no + "</FSFILE_NO></Data>");
                    tsm_video.GetKeyframeFilesInfoAsync(fsfile_no);
                    searchDbFunction.GetVideoSegmentAsync("<Data><FSFILE_NO>" + fsfile_no + "</FSFILE_NO></Data>");
                    searchDbFunction.GetTapeDataAsync("<Data><FSFILE_NO>" + fsfile_no + "</FSFILE_NO></Data>");
                }
                else if (FileTypeToeCategory(vwname) == "2")
                {
                    //this.RadBusyIndicator_AllSearchResult.IsBusy = false;
                    //return;
                }
                else if (FileTypeToeCategory(vwname) == "3")
                {
                    tsm_photo.GetHttpPathByFileIDAsync(fsfile_no, FileID_MediaType.Photo);
                }
                else if (FileTypeToeCategory(vwname) == "4")
                {
                    //this.RadBusyIndicator_AllSearchResult.IsBusy = false;
                    //return;
                }
                else if (FileTypeToeCategory(vwname) == "5")
                {
                    //新聞片庫
                    booking.GetNewsLVPathAsync(fsfile_no);
                    booking.GetNewsKeyFramePathAsync(fsfile_no);
                    this.RadOutlookBar_Detail[5].Visibility = System.Windows.Visibility.Visible;
                    this.RadOutlookBar_Detail[6].Visibility = System.Windows.Visibility.Visible;
                    this.RadOutlookBar_Detail[7].Visibility = System.Windows.Visibility.Visible;
                }

                this.RadOutlookBar_Detail.IsMinimized = false;
            }
        }

        //加入預借清單
        //private void HyperlinkButton_PreBooking_Click(object sender, System.Windows.RoutedEventArgs e)
        //{
        //    //檢查是否為數位檔
        //    WSBookingFunctionSoapClient bookDb = new WSBookingFunctionSoapClient();

        //    this.RadBusyIndicator_AllSearchResult.IsBusy = true;

        //    bookDb.CheckProgIsExpireAsync(((HyperlinkButton)sender).Tag.ToString().Split(';')[0], ((HyperlinkButton)sender).Tag.ToString().Split(';')[1]);

        //    //檢查檔案所屬的節目是否過期
        //    bookDb.CheckProgIsExpireCompleted += (s, args) =>
        //    {
        //        if(args.Error != null)
        //        {
        //            MessageBox.Show("系統錯誤!", "訊息", MessageBoxButton.OK);
        //            return;
        //        }
        //        else
        //        {
        //            if(string.IsNullOrEmpty(args.Result))
        //            {
        //                string parameter = "";
        //                parameter += "<Data>";
        //                parameter += "<FSUSER_ID>" + UserClass.userData.FSUSER_ID + "</FSUSER_ID>";
        //                parameter += "<FSFILE_NO>" + ((HyperlinkButton)sender).Tag.ToString().Split(';')[0] + "</FSFILE_NO>";
        //                parameter += "<FCBOOKING_TYPE>1</FCBOOKING_TYPE>";
        //                parameter += "</Data>";
        //                bookDb.GetFileExistPreBookingAsync(parameter);
        //            }
        //            else
        //            {
        //                MessageBox.Show("檔案所屬節目已過期，不可調用!","訊息",MessageBoxButton.OK);
        //                return;
        //            }
        //        }

        //    };


        //    //檢查檔案是否存在
        //    bookDb.GetFileExistPreBookingCompleted += (s, args) =>
        //    {
        //        if (args.Error != null || string.IsNullOrEmpty(args.Result))
        //        {
        //            this.RadBusyIndicator_AllSearchResult.IsBusy = false;
        //            return;
        //        }

        //        XDocument xmlDocResult = XDocument.Parse(args.Result.ToString());

        //        if (xmlDocResult.Element("Datas") == null || xmlDocResult.Element("Datas").Value == "")
        //        {
        //            //MessageBox.Show("檔案不存在!");
        //            this.RadBusyIndicator_AllSearchResult.IsBusy = false;
        //            return;
        //        }

        //        if (xmlDocResult.Element("Datas").Element("Data").Element("FNCOUNT").Value != "0")
        //        {
        //            MessageBox.Show("檔案已加入清單中!");
        //            this.RadBusyIndicator_AllSearchResult.IsBusy = false;
        //            return;
        //        }
        //        else
        //        {
        //            //加入資料庫
        //            GetFileInformation(((HyperlinkButton)sender).Tag.ToString().Split(';')[0], ((HyperlinkButton)sender).Tag.ToString().Split(';')[1]);
        //        }



        //    };



        //}

        ////取得檔案資訊
        //private void GetFileInformation(string fsfile_no, string fsfile_type)
        //{

        //    WSBookingFunctionSoapClient bookDb = new WSBookingFunctionSoapClient();

        //    bookDb.GetFileInformationCompleted += (s, args) =>
        //    {
        //        if (args.Error != null || String.IsNullOrEmpty(args.Result))
        //        {
        //            this.RadBusyIndicator_AllSearchResult.IsBusy = false;
        //            return;
        //        }

        //        //取得檔案位置
        //        bookDb.GetFileLocationCompleted += (s1, args1) =>
        //        {
        //            if (args1.Error != null || String.IsNullOrEmpty(args1.Result))
        //            {
        //                this.RadBusyIndicator_AllSearchResult.IsBusy = false;
        //                return;
        //            }

        //            string file_location = args1.Result;

        //            XDocument xmlDocResult = XDocument.Parse(args.Result.ToString());

        //            if (xmlDocResult.Element("Datas").Value == "")
        //            {
        //                MessageBox.Show("檔案不存在!");
        //                this.RadBusyIndicator_AllSearchResult.IsBusy = false;
        //                return;
        //            }
        //            else
        //            {
        //                // 
        //            }
        //            //加入參數
        //            string parameter = "";
        //            parameter += "<Data>";
        //            parameter += "<FNNO></FNNO>";
        //            parameter += "<FSGUID>" + Guid.NewGuid().ToString() + "</FSGUID>";
        //            parameter += "<FSUSER_ID>" + UserClass.userData.FSUSER_ID + "</FSUSER_ID>";
        //            parameter += "<FDBOOKING_DATE>" + DateTime.Now.ToString("yyyy/MM/dd") + "</FDBOOKING_DATE>";
        //            parameter += "<FSFILE_NO>" + fsfile_no + "</FSFILE_NO>";
        //            parameter += "<FSSTART_TIMECODE>" + GetFrame(xmlDocResult.Element("Datas").Element("Data").Element("FSFILE_CATEGORY").Value, xmlDocResult.Element("Datas").Element("Data").Element("FSBEG_TIMECODE").Value) + "</FSSTART_TIMECODE>";
        //            parameter += "<FSBEG_TIMECODE></FSBEG_TIMECODE>";
        //            parameter += "<FSEND_TIMECODE></FSEND_TIMECODE>";
        //            parameter += "<FSFILE_TYPE>" + xmlDocResult.Element("Datas").Element("Data").Element("FSFILE_TYPE").Value + "</FSFILE_TYPE>";
        //            parameter += "<FSFILE_CATEGORY>" + xmlDocResult.Element("Datas").Element("Data").Element("FSFILE_CATEGORY").Value + "</FSFILE_CATEGORY>";
        //            parameter += "<FNLOCATION>" + file_location + "</FNLOCATION>";
        //            parameter += "<FCOUT_BUY>" + xmlDocResult.Element("Datas").Element("Data").Element("FCOUT_BUY").Value + "</FCOUT_BUY>";
        //            parameter += "<FCBOOKING_TYPE>1</FCBOOKING_TYPE>";
        //            if (UserClass.userData.FNGROUP_ID == 63)
        //            {
        //                parameter += "<FCSUPERVISOR_CHECK>N</FCSUPERVISOR_CHECK>";
        //                parameter += "<FSSUPERVISOR></FSSUPERVISOR>";
        //            }
        //            else
        //            {
        //                parameter += "<FCSUPERVISOR_CHECK>" + xmlDocResult.Element("Datas").Element("Data").Element("FSSUPERVISOR").Value + "</FCSUPERVISOR_CHECK>";
        //                parameter += "<FSSUPERVISOR>" + xmlDocResult.Element("Datas").Element("Data").Element("FSSupervisor_ID").Value + "</FSSUPERVISOR>";
        //            }

        //            parameter += "<FCALL></FCALL>";
        //            parameter += "</Data>";

        //            WSBookingFunctionSoapClient booking = new WSBookingFunctionSoapClient();
        //            booking.InsertPreBooking2Completed += (s2, args2) =>
        //            {
        //                if (args2.Error != null)
        //                {
        //                    this.RadBusyIndicator_AllSearchResult.IsBusy = false;
        //                    return;
        //                }

        //                if (args2.Result)
        //                {
        //                    MessageBox.Show("加入預借清單成功!");
        //                    this.RadBusyIndicator_AllSearchResult.IsBusy = false;
        //                }

        //            };

        //            booking.InsertPreBooking2Async(parameter, UserClass.userData.FSUSER_ID);

        //        };

        //        bookDb.GetFileLocationAsync(fsfile_no, fsfile_type);

        //    };

        //    bookDb.GetFileInformationAsync("<Data><FSFILE_NO>" + fsfile_no + "</FSFILE_NO><FSVW_NAME>" + fsfile_type + "</FSVW_NAME></Data>");
        //}


        private string GetFrame(string category, string timecode)
        {
            if (timecode != "")
            {
                if (category == "1")
                {
                    return TransferTimecode.timecodetoframe(timecode).ToString();
                }
                else if (category == "5")
                {
                    return timecode;
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
        }

        private string GetCategoryName(string category)
        {
            if (string.IsNullOrEmpty(category))
            {
                return "";
            }
            else if (category == "1")
            {
                return "影";
            }
            else if (category == "2")
            {
                return "音";
            }
            else if (category == "3")
            {
                return "圖";
            }
            else if (category == "4")
            {
                return "文";
            }
            else if (category == "5")
            {
                return "新聞";
            }
            else
            {
                return "";
            }

        }


        private bool IsTape(string fcfrom)
        {
            if (fcfrom == "T")
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void ClearControl()
        {
            this.TextBlock_FileNo.Text = "";
            this.TextBlock_FileType.Text = "";
            this.TextBlock_Title.Text = "";
            this.TextBlock_TimeCode.Text = "";
            this.TextBlock_CreatedBy.Text = "";
            this.TextBlock_CreatedDate.Text = "";
            this.TextBlock_UpdatedBy.Text = "";
            this.TextBlock_UpdatedDate.Text = "";
            this.TextBlock_Channel.Text = "";
            this.TextBlock_ExtName.Text = "";
            this.TextBlock_ArcType.Text = "";
            this.TextBlock_PrgoName.Text = "";
            this.TextBlock_PrgoEp.Text = "";
            this.TextBlock_SubjectTitle.Text = "";
            this.TextBlock_SubjectContent.Text = "";
            this.TextBlock_Description.Text = "";
            this.TextBlock_TapeNo.Text = "";
            this.TextBlock_TranFrom.Text = "";

            //內容
            this.TextBlock_Content.Text = "";

            //Script
            this.TextBlock_Script.Text = "";

            //EnpsScript
            this.TextBlock_EnpsScript.Text = "";

            //版權
            this.TextBlock_License.Text = "";

            //版權描述
            this.TextBlock_License_Description.Text = "";
            //影帶資訊
            this.TextBlock_TapeKind.Text = "";
            this.TextBlock_TapeCategory.Text = "";


            this.Grid_Video.Children.Clear();
            this.ListBox_KeyFrame.ItemsSource = null;

        }

        private void RadOutlookBar_Tree_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            //this.RadOutlookBar_Tree.IsMinimized = false;
        }

        private void RadOutlookBar_Tree_MouseLeave(object sender, MouseEventArgs e)
        {
            //this.RadOutlookBar_Tree.IsMinimized = true;
        }

        private void RadOutlookBar_Detail_MouseEnter(object sender, MouseEventArgs e)
        {
            //this.RadOutlookBar_Detail.IsMinimized = false;
        }

        private void RadOutlookBar_Detail_MouseLeave(object sender, MouseEventArgs e)
        {
            //this.RadOutlookBar_Detail.IsMinimized = true;
        }

        private void Button_PreBooking_List_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            parentFrame.generateNewPane("已加入清單項目", "SEARCH_PREBOOKING", "1", false);

        }


        private void Button_Export_Click(object sender, RoutedEventArgs e)
        {

            //匯出檔案
            if (this.ListBox_SearchResult.ItemsSource == null)
            {
                MessageBox.Show("無資料可匯出!");
                return;
            }

            //if (totalCount > 2000)
            //{
            //    MessageBox.Show("筆數過多。無法下載!");
            //    return;
            //}

            WSBookingFunctionSoapClient booking = new WSBookingFunctionSoapClient();

            this.RadBusyIndicator_AllSearchResult.IsBusy = true;
            string _parameter = parameter;
            XDocument xDoc = XDocument.Parse(_parameter);
            xDoc.Element("Search").Element("PageSize").SetValue("50");
            xDoc.Element("Search").Element("GetColumns").SetValue("FSFILE_NO;FSTITLE;FSDESCRIPTION;FSTYPE;FSPROG_B_PGMNAME;FSPROG_D_PGDNAME;FNEPISODE;FSCHANNEL;FSCREATED_BY;FDCREATED_DATE;FSUPDATED_BY;FDUPDATED_DATE");
            _parameter = xDoc.ToString();

            booking.ExportSearchResultAsync(_parameter, UserClass.userData.FSUSER_ID);

            SRH107_01 srh107_01 = new SRH107_01();
            srh107_01.Show();

            srh107_01.Closed += (s, args) =>
            {
                this.RadBusyIndicator_AllSearchResult.IsBusy = false;
            };

            //SaveFileDialog dialog = new SaveFileDialog();
            //dialog.Filter = "csv File" + "|*.csv";
            //dialog.FilterIndex = 1;
            //dialog.ShowDialog();

            //booking.ExportSearchResultCompleted += (s1, args1) =>
            //{
            //    if (args1.Error != null || String.IsNullOrEmpty(args1.Result))
            //    {
            //        this.RadBusyIndicator_AllSearchResult.IsBusy = false;
            //        return;
            //    }


            //    //System.Windows.Browser.HtmlPage.Window.Navigate(new Uri(args1.Result, UriKind.Absolute));
            //    WCF100DL.WCF100Client WCF100client = new WCF100DL.WCF100Client();

            //    WCF100client.DownloadAsync(args1.Result);
            //    //WCF100client.DownloadAsync(args.Result);
            //    WCF100client.DownloadCompleted += (s2, args2) =>
            //    {
            //        if (args2.Error != null)
            //        {
            //            MessageBox.Show(args2.Error.ToString());
            //            this.RadBusyIndicator_AllSearchResult.IsBusy = false;
            //            return;
            //        }

            //        if (args2.Result != null)
            //        {
            //            try
            //            {

            //                Stream fileobj = dialog.OpenFile();
            //                if (fileobj == null)
            //                {
            //                    this.RadBusyIndicator_AllSearchResult.IsBusy = false;
            //                }
            //                else
            //                {
            //                    fileobj.Write(args2.Result.File, 0, args2.Result.File.Length);
            //                    fileobj.Close();
            //                    MessageBox.Show("下載完成!");
            //                    this.RadBusyIndicator_AllSearchResult.IsBusy = false;
            //                }
            //            }
            //            catch (Exception ex)
            //            {
            //                this.RadBusyIndicator_AllSearchResult.IsBusy = false;
            //                return;
            //            }
            //        }
            //    };
            //};

            //booking.ExportSearchResultAsync(_parameter, UserClass.userData.FSUSER_ID);

            //searchWS2.SearchCompleted += (s, args) =>
            //{
            //    if (args.Error != null || String.IsNullOrEmpty(args.Result))
            //    {
            //        this.RadBusyIndicator_AllSearchResult.IsBusy = false;
            //        return;
            //    }

            //    WSBookingFunctionSoapClient booking = new WSBookingFunctionSoapClient();



            //};

            //searchWS2.SearchAsync(_parameter);
        }

        private void Button_Show_Refiner_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            if (this.RadOutlookBar_Tree.IsMinimized)
            {
                this.Button_Show_Refiner.Content = "關閉分類";
                this.RadOutlookBar_Tree.IsMinimized = false;
            }
            else
            {
                this.Button_Show_Refiner.Content = "顯示分類";
                this.RadOutlookBar_Tree.IsMinimized = true;
            }
        }

        private void Button_Keyframe_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            string sec = (sender as Button).Tag.ToString();
            //MessageBox.Show(sec);
            if (player != null)
            {
                player.HTML_SetFrame(long.Parse(sec));
                player.HTML_Play_Click();
            }
        }

        private void Button_Set_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            int i = 0;
            if (string.IsNullOrEmpty(this.TextBox_HH.Text) || !Int32.TryParse(this.TextBox_HH.Text, out i) ||
                string.IsNullOrEmpty(this.TextBox_MM.Text) || !Int32.TryParse(this.TextBox_MM.Text, out i) || i > 59 ||
                string.IsNullOrEmpty(this.TextBox_SS.Text) || !Int32.TryParse(this.TextBox_SS.Text, out i) || i > 59 ||
                string.IsNullOrEmpty(this.TextBox_FF.Text) || !Int32.TryParse(this.TextBox_FF.Text, out i) || i > 30)
            {
                MessageBox.Show("TimeCode格式錯誤!", "訊息", MessageBoxButton.OK);
                return;
            }

            string error = "";
            if (!TransferTimecode.check_timecode_format(SetTimecodeFormat(this.TextBox_HH.Text),
                                                        SetTimecodeFormat(this.TextBox_MM.Text),
                                                        SetTimecodeFormat(this.TextBox_SS.Text),
                                                        SetTimecodeFormat(this.TextBox_FF.Text),
                                                        out error))
            {
                MessageBox.Show(error);
                return;
            }
            if (player != null)
            {
                string timecode = SetTimecodeFormat(this.TextBox_HH.Text) + ":" + SetTimecodeFormat(this.TextBox_MM.Text) + ":" +
                                  SetTimecodeFormat(this.TextBox_SS.Text) + ":" + SetTimecodeFormat(this.TextBox_FF.Text);
                player.HTML_SetFrame(long.Parse(TransferTimecode.timecodetoframe(timecode).ToString()) - long.Parse(TransferTimecode.timecodetoframe(startTimeCode).ToString()));
            }

        }


        private string SetTimecodeFormat(string time)
        {
            int _temp_time = 0;
            if (time == "")
            {
                return "00";
            }
            else
            {
                if (Int32.TryParse(time, out _temp_time))
                {
                    if (_temp_time >= 0 && _temp_time < 10)
                    {
                        return "0" + _temp_time.ToString();
                    }
                    else
                    {
                        return _temp_time.ToString();
                    }
                }
                else
                {
                    return "";
                }
            }
        }

        private void TextBox_HH_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            if (this.TextBox_HH.Text.Length == 2)
            {
                this.TextBox_MM.Focus();
            }
        }

        private void TextBox_MM_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            if (this.TextBox_MM.Text.Length == 2)
            {
                this.TextBox_SS.Focus();
            }
        }

        private void TextBox_SS_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            if (this.TextBox_SS.Text.Length == 2)
            {
                this.TextBox_FF.Focus();
            }
        }

        private void TextBox_FF_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            if (this.TextBox_FF.Text.Length == 2)
            {
                this.Button_Set.Focus();
            }
        }

        private void RadOutlookBar_Detail_SelectionChanged(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            //MessageBox.Show(player.HTML_GetCurrentFrame());
        }

        private void Button_ViewMetaData_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            string _fsfile_no = ((Button)sender).Tag.ToString().Split(';')[0];
            string _fstable_name = ((Button)sender).Tag.ToString().Split(';')[1];
            if (_fstable_name.ToLower().IndexOf("video") > -1)
            {
                _fstable_name = "TBLOG_VIDEO";
            }
            else if (_fstable_name.ToLower().IndexOf("audio") > -1)
            {
                _fstable_name = "TBLOG_AUDIO";
            }
            else if (_fstable_name.ToLower().IndexOf("photo") > -1)
            {
                _fstable_name = "TBLOG_PHOTO";
            }
            else if (_fstable_name.ToLower().IndexOf("doc") > -1)
            {
                _fstable_name = "TBLOG_DOC";
            }

            SRH107_02 srh107_02 = new SRH107_02(_fsfile_no, _fstable_name);
            srh107_02.Show();

        }

        //檢查檔案所屬節目是否過期
        void bookDb_CheckProgIsExpireCompleted(object sender, CheckProgIsExpireCompletedEventArgs e)
        {
            if (e.Error != null || e.Result == null)
            {
                MessageBox.Show("檢查檔案所屬節目是否過期錯誤!", "訊息", MessageBoxButton.OK);
                this.RadBusyIndicator_AllSearchResult.IsBusy = false;
            }
            else
            {
                if (!string.IsNullOrEmpty(e.Result))
                {
                    
                    string fsERROR = string.Empty;
                    foreach (string error in e.Result.Substring(0, e.Result.Length - 1).Split(','))
                    {
                        fsERROR += "檔案編號:" + error + "所屬節目已過期，無法調用!\r\n";
                    }
                    MessageBox.Show(fsERROR, "訊息", MessageBoxButton.OK);

                    //移除無法調用檔案
                    foreach (string error in e.Result.Substring(0, e.Result.Length - 1).Split(','))
                    {
                        clsPREBOOKING_LIST.Remove(clsPREBOOKING_LIST.FirstOrDefault<clsPREBOOKING>(c => c.fsFILE_NO == error));
                    }

                    this.RadBusyIndicator_AllSearchResult.IsBusy = false;
                }
                else
                {
                    List<SearchResultClass> SearchResultClass_LIST_TEMP = this.ListBox_SearchResult.ItemsSource as List<SearchResultClass>;

                    foreach (SearchResultClass s in SearchResultClass_LIST_TEMP)
                    {
                        if (clsPREBOOKING_LIST.FirstOrDefault<clsPREBOOKING>(c => c.fsFILE_NO == s.FSFILE_NO && c.fsVW_NAME == s.FSVWNAME) == null)
                        {
                            s.FBIS_CHECK = false;
                        }
                    }

                    if (clsPREBOOKING_LIST != null && clsPREBOOKING_LIST.Count > 0)
                    {
                        //檢查檔案是否存在
                        string fsFILE_NOs = string.Empty;
                        string fsFILE_TYPEs = string.Empty;
                        foreach (clsPREBOOKING c in clsPREBOOKING_LIST)
                        {
                            fsFILE_NOs += c.fsFILE_NO + ',';
                            fsFILE_TYPEs += c.fsVW_NAME + ',';
                        }
                        bookDb.GetFileExistPreBooking2Async(UserClass.userData.FSUSER_ID, fsFILE_NOs.Substring(0, fsFILE_NOs.Length - 1), "1");
                    }
                    else
                    {
                        this.ListBox_SearchResult.ItemsSource = null;
                        this.ListBox_SearchResult.ItemsSource = SearchResultClass_LIST_TEMP;

                        this.RadBusyIndicator_AllSearchResult.IsBusy = false;
                        MessageBox.Show("請勾選要調用的項目!", "訊息", MessageBoxButton.OK);
                    }
                }
            }

        }

        //檢查檔案是否已存在預借清單
        void bookDb_GetFileExistPreBooking2Completed(object sender, GetFileExistPreBooking2CompletedEventArgs e)
        {
            if (e.Error != null || e.Result == null)
            {
                MessageBox.Show("檢查檔案是否已存在預借清單錯誤!", "訊息", MessageBoxButton.OK);
                this.RadBusyIndicator_AllSearchResult.IsBusy = false;
            }
            else
            {
                if (!string.IsNullOrEmpty(e.Result))
                {
                    string fsERROR = string.Empty;
                    foreach (string error in e.Result.Split(','))
                    {
                        fsERROR += "檔案編號:" + error + "已存在預借清單!\r\n";
                    }
                    MessageBox.Show(fsERROR, "訊息", MessageBoxButton.OK);

                    //移除已存在調用檔案
                    foreach (string error in e.Result.Split(','))
                    {
                        clsPREBOOKING_LIST.Remove(clsPREBOOKING_LIST.FirstOrDefault<clsPREBOOKING>(c => c.fsFILE_NO == error));
                    }
                }

                List<SearchResultClass> SearchResultClass_LIST_TEMP = this.ListBox_SearchResult.ItemsSource as List<SearchResultClass>;

                foreach (SearchResultClass s in SearchResultClass_LIST_TEMP)
                {
                    if (clsPREBOOKING_LIST.FirstOrDefault<clsPREBOOKING>(c => c.fsFILE_NO == s.FSFILE_NO && c.fsVW_NAME == s.FSVWNAME) == null)
                    {
                        s.FBIS_CHECK = false;
                    }
                }


                //加入資料庫
                if (clsPREBOOKING_LIST != null && clsPREBOOKING_LIST.Count > 0)
                {
                    string fsFILE_NOs = string.Empty;
                    string fsFILE_TYPEs = string.Empty;

                    foreach (clsPREBOOKING c in clsPREBOOKING_LIST)
                    {
                        fsFILE_NOs += c.fsFILE_NO + ",";
                        fsFILE_TYPEs += c.fsVW_NAME + ",";
                    }

                    bookDb.GetFileInformation2Async(UserClass.userData.FSUSER_ID, fsFILE_NOs.Substring(0, fsFILE_NOs.Length - 1), fsFILE_TYPEs.Substring(0, fsFILE_TYPEs.Length - 1), int.Parse(UserClass.userData.FNGROUP_ID.ToString()));
                }
                else
                {
                    this.ListBox_SearchResult.ItemsSource = null;
                    this.ListBox_SearchResult.ItemsSource = SearchResultClass_LIST_TEMP;

                    this.RadBusyIndicator_AllSearchResult.IsBusy = false;
                    MessageBox.Show("請勾選要調用的項目!", "訊息", MessageBoxButton.OK);
                }
            }
        }


        void bookDb_GetFileInformation2Completed(object sender, GetFileInformation2CompletedEventArgs e)
        {
            this.RadBusyIndicator_AllSearchResult.IsBusy = false;
            if (e.Error != null)
            {
                MessageBox.Show("加入預借清單發生錯誤!", "訊息", MessageBoxButton.OK);
            }
            else
            {
                if (!string.IsNullOrEmpty(e.Result))
                {
                    MessageBox.Show(e.Result, "訊息", MessageBoxButton.OK);
                }
                else
                {
                    MessageBox.Show("加入預借清單成功!","訊息",MessageBoxButton.OK);
                }
            }

            List<SearchResultClass> SearchResultClass_LIST_TEMP = this.ListBox_SearchResult.ItemsSource as List<SearchResultClass>;
            foreach (SearchResultClass s in SearchResultClass_LIST_TEMP)
            {
                s.FBIS_CHECK = false;
            }

            clsPREBOOKING_LIST.Clear();
            this.CheckBox_Select_All.IsChecked = false;
            this.ListBox_SearchResult.ItemsSource = null;
            this.ListBox_SearchResult.ItemsSource = SearchResultClass_LIST_TEMP;
        }

        private void CheckBox_PreBooking_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            CheckBox chk = sender as CheckBox;

            if (chk != null)
            {
                if (chk.IsChecked == true)
                {
                    if (clsPREBOOKING_LIST.FirstOrDefault<clsPREBOOKING>(p => p.fsFILE_NO == chk.Tag.ToString().Split(';')[0] && p.fsVW_NAME == chk.Tag.ToString().Split(';')[1]) == null)
                    {
                        clsPREBOOKING_LIST.Add(new clsPREBOOKING { fsFILE_NO = chk.Tag.ToString().Split(';')[0], fsVW_NAME = chk.Tag.ToString().Split(';')[1] });
                    }
                }
                else
                {
                    if (clsPREBOOKING_LIST.FirstOrDefault<clsPREBOOKING>(p => p.fsFILE_NO == chk.Tag.ToString().Split(';')[0] && p.fsVW_NAME == chk.Tag.ToString().Split(';')[1]) != null)
                    {
                        clsPREBOOKING_LIST.Remove(clsPREBOOKING_LIST.FirstOrDefault<clsPREBOOKING>(p => p.fsFILE_NO == chk.Tag.ToString().Split(';')[0] && p.fsVW_NAME == chk.Tag.ToString().Split(';')[1]));
                        flag = false;
                        this.CheckBox_Select_All.IsChecked = false;
                    }
                }
            }

        }

        private void CheckBox_Select_All_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            CheckBox chk = sender as CheckBox;

            List<SearchResultClass> SearchResultClass_LIST_TEMP = this.ListBox_SearchResult.ItemsSource as List<SearchResultClass>;

            if (chk != null)
            {
                if (chk.IsChecked == true)
                {
                    foreach (SearchResultClass s in SearchResultClass_LIST_TEMP)
                    {
                        s.FBIS_CHECK = true;

                        if (clsPREBOOKING_LIST.FirstOrDefault<clsPREBOOKING>(p => p.fsFILE_NO == s.FSFILENO_VWNAME.Split(';')[0] && p.fsVW_NAME == s.FSFILENO_VWNAME.Split(';')[1]) == null)
                        {
                            clsPREBOOKING_LIST.Add(new clsPREBOOKING { fsFILE_NO = s.FSFILENO_VWNAME.Split(';')[0], fsVW_NAME = s.FSFILENO_VWNAME.Split(';')[1] });
                        }
                    }
                    this.ListBox_SearchResult.ItemsSource = null;
                    this.ListBox_SearchResult.ItemsSource = SearchResultClass_LIST_TEMP;

                    flag = true;
                }
                else
                {
                    if (flag == true)
                    {
                        foreach (SearchResultClass s in SearchResultClass_LIST_TEMP)
                        {
                            s.FBIS_CHECK = false;
                            if (clsPREBOOKING_LIST.FirstOrDefault<clsPREBOOKING>(p => p.fsFILE_NO == s.FSFILENO_VWNAME.Split(';')[0] && p.fsVW_NAME == s.FSFILENO_VWNAME.Split(';')[1]) != null)
                            {
                                clsPREBOOKING_LIST.Remove(clsPREBOOKING_LIST.FirstOrDefault<clsPREBOOKING>(p => p.fsFILE_NO == s.FSFILENO_VWNAME.Split(';')[0] && p.fsVW_NAME == s.FSFILENO_VWNAME.Split(';')[1]));
                            }
                        }

                        this.ListBox_SearchResult.ItemsSource = null;
                        this.ListBox_SearchResult.ItemsSource = SearchResultClass_LIST_TEMP;
                    }

                    
                }
            }
        }


        public bool fnCHECK_IS_CHECK(string fsFILE_NO, string fsVWNAME)
        {
            if (clsPREBOOKING_LIST != null && clsPREBOOKING_LIST.Count > 0)
            {
                if (clsPREBOOKING_LIST.FirstOrDefault<clsPREBOOKING>(p => p.fsFILE_NO == fsFILE_NO && p.fsVW_NAME == fsVWNAME) != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private void Button_Send_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            if (clsPREBOOKING_LIST != null && clsPREBOOKING_LIST.Count > 0)
            {
                //檢查檔案所屬的節目是否過期
                string fsFILE_NOs = string.Empty;
                string fsFILE_TYPEs = string.Empty;
                foreach (clsPREBOOKING c in clsPREBOOKING_LIST)
                {
                    fsFILE_NOs += c.fsFILE_NO + ',';
                    fsFILE_TYPEs += c.fsVW_NAME + ',';
                }

                bookDb.CheckProgIsExpireAsync(fsFILE_NOs.Substring(0, fsFILE_NOs.Length - 1), fsFILE_TYPEs.Substring(0, fsFILE_TYPEs.Length - 1));

                this.RadBusyIndicator_AllSearchResult.IsBusy = true;
            }
            else
            {
                MessageBox.Show("請至少勾選一則要調用的項目!", "訊息", MessageBoxButton.OK);
            }
        }

    }

    public class SearchTreeClass
    {
        public string name { set; get; }
        public string count { set; get; }
    }

    public class ImageClass
    {
        public string path { set; get; }
        public string tooltip { set; get; }
    }

    public class KeyFrameClass
    {
        public string path { set; get; }
        public string desc { set; get; }
        public string sec { set; get; }
    }

    public class VideoSegmentClass
    {
        public string begin_timecode { set; get; }
        public string end_timecode { set; get; }
    }

    public class clsPREBOOKING
    {
        public string fsFILE_NO { set; get; }
        public string fsVW_NAME { set; get; }

    }
}
