﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using SearchClassLibrary;
using PTS_MAM3.WSBookingFunction;

namespace PTS_MAM3.SRH
{
    public partial class SRH126_02 : ChildWindow
    {
        public string _fcINTERPLAY = string.Empty;

        public SRH126_02()
        {
            InitializeComponent();
        }


        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.RadioButton_NAS.IsChecked == true) this._fcINTERPLAY = "N";
            if (this.RadioButton_AVID.IsChecked == true) this._fcINTERPLAY = "Y";
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
			this.DialogResult = false;
        }

    }
}

