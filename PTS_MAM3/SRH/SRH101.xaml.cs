﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SearchClassLibrary;
using PTS_MAM3.WSBookingFunction;
using System.Xml;
using System.Xml.Linq;
using System.Text;

namespace PTS_MAM3.SRH
{
    public partial class SRH101 : UserControl
    {
        private PTS_MAM3.MainFrame parentFrame;
        private string _bookingNo = "";
        private List<string> _fsFileOutputCodecList = new List<string>();
        WSBookingFunctionSoapClient booking = new WSBookingFunctionSoapClient();
        flowWebService.FlowSoapClient FlowClinet = new flowWebService.FlowSoapClient();

        public SRH101(PTS_MAM3.MainFrame mainframe)
        {
            InitializeComponent();
            parentFrame = mainframe;

            booking.UpdateBookingMasterFlowIDCompleted += new EventHandler<UpdateBookingMasterFlowIDCompletedEventArgs>(booking_UpdateBookingMasterFlowIDCompleted);
            
            FlowClinet.NewFlowWithFieldCompleted += new EventHandler<flowWebService.NewFlowWithFieldCompletedEventArgs>(FlowClinet_NewFlowWithFieldCompleted);


            booking.GetFileTranscodeInfoCompleted += (s, args) =>
            {
                List<FileTranscodeProfile> fileTransCodeProfileList = new List<FileTranscodeProfile>();
                FileTranscodeProfile p1 = new FileTranscodeProfile();
                p1.fsfile_name = "原生格式";
                p1.fsfile_path = "";
                fileTransCodeProfileList.Add(p1);
                
                if (args.Error != null)
                {
                    return;
                }
                if (!string.IsNullOrEmpty(args.Result))
                {
                    string[] file = args.Result.Split(';');
                    for (int i = 0; i <= file.Count() - 1; i++)
                    {
                        FileTranscodeProfile p = new FileTranscodeProfile();
                        p.fsfile_path = file[i].Split('|')[0];
                        p.fsfile_name = file[i].Split('|')[1];
                        fileTransCodeProfileList.Add(p);
                    }
                }

                
                this.ListBox_FileOutputInfo.ItemsSource = null;
                this.ListBox_FileOutputInfo.ItemsSource = fileTransCodeProfileList;

            };


            booking.GetFileLogoInfoCompleted += (s, args) =>
            {
                if (args.Error != null || String.IsNullOrEmpty(args.Result))
                {
                    return;
                }

                string[] file = args.Result.Split(';');
                List<FileLogoProfile> fileLogoProfileList = new List<FileLogoProfile>();

                FileLogoProfile p1 = new FileLogoProfile();
                p1.fsfile_path = "";
                p1.fsfile_name = "No Logo";
                fileLogoProfileList.Add(p1);

                for (int i = 0; i <= file.Count() - 1; i++)
                {
                    FileLogoProfile p = new FileLogoProfile();
                    p.fsfile_path = file[i].Split('|')[0];
                    p.fsfile_name = file[i].Split('|')[1];
                    fileLogoProfileList.Add(p);
                }

                this.ComboBox_Logo.ItemsSource = fileLogoProfileList;
                this.ComboBox_Logo.SelectedIndex = 0;
            };


            booking.GetBookingReasonCompleted += (s, args) =>
            {
                if (args.Error != null || String.IsNullOrEmpty(args.Result))
                {
                    return;
                }

                XDocument xmlDocResult = XDocument.Parse(args.Result.ToString());

                var paras = from para in xmlDocResult.Descendants("Data")
                            select new BookingReason
                            {
                                fsno = para.Element("FSNO").Value,
                                fsreason = para.Element("FSREASON").Value
                            };

                this.ComboBox_Booking_Reason.ItemsSource = paras;
                this.ComboBox_Booking_Reason.SelectedIndex = 0;
            };

            booking.GetFileTranscodeInfoAsync();
            booking.GetFileLogoInfoAsync();
            booking.GetBookingReasonAsync("");

        }

        

        void FlowClinet_NewFlowWithFieldCompleted(object sender, flowWebService.NewFlowWithFieldCompletedEventArgs e)
        {
            //this.RadBusyIndicator_Booking.IsBusy = false;
            //MessageBox.Show("調用清單送出成功!");
            //this.TextBox_ProgramName.Text = "";
            //this.TextBox_Episode_Begin.Text = "";
            //this.TextBox_Episode_End.Text = "";
            //this.TextBox_Reason_Desc.Text = "";
            //booking.GetFileTranscodeInfoAsync();
            //this.ComboBox_Logo.SelectedIndex = 0;
            //this.ComboBox_Booking_Reason.SelectedIndex = 0;

            if (e.Error != null || string.IsNullOrEmpty(e.Result))
            {
                booking.fnINSERT_NEWFLOW_WITHFIELD_LOGAsync(8, "-1", UserClass.userData.FSUSER_ID, "呼叫流程失敗：" + e.Error + ";" + e.Result);
                this.RadBusyIndicator_Booking.IsBusy = false;
                MessageBox.Show("送審核失敗，請聯絡系統管理人員!");
            }
            else
            {
                //記錄流程編號
                int fnFLOW_ID = -1;
                if (!int.TryParse(e.Result, out fnFLOW_ID))
                {
                    booking.fnINSERT_NEWFLOW_WITHFIELD_LOGAsync(8, "-1", UserClass.userData.FSUSER_ID, "呼叫流程失敗：流程編號轉換失敗;【錯誤流程編號：" + e.Result + "】");
                    this.RadBusyIndicator_Booking.IsBusy = false;
                    MessageBox.Show("送審核失敗，請聯絡系統管理人員!");
                }
                else
                {
                    booking.UpdateBookingMasterFlowIDAsync(_bookingNo, int.Parse(e.Result), UserClass.userData.FSUSER_ID);
                    
                }
            }

        }


        //更新流程編號至BookingMaster
        void booking_UpdateBookingMasterFlowIDCompleted(object sender, UpdateBookingMasterFlowIDCompletedEventArgs e)
        {
            if (e.Error != null || !e.Result)
            {
                MessageBox.Show("記錄流程編號錯誤!");
            }
            else
            {
                this.RadBusyIndicator_Booking.IsBusy = false;
                MessageBox.Show("調用清單送出成功!");
                this.TextBox_ProgramName.Text = "";
                this.TextBox_Episode_Begin.Text = "";
                this.TextBox_Episode_End.Text = "";
                this.TextBox_Reason_Desc.Text = "";
                booking.GetFileTranscodeInfoAsync();
                this.ComboBox_Logo.SelectedIndex = 0;
                this.ComboBox_Booking_Reason.SelectedIndex = 0;
            }
        }

        private void Button_Search_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            if (this.TextBox_ProgramName.Text == "")
            {
                MessageBox.Show("請輸入節目名稱!");
                return;
            }
            if (this.TextBox_Episode_Begin.Text == "" || this.TextBox_Episode_End.Text == "")
            {
                MessageBox.Show("請輸入起迄集數!");
                return;
            }
            int _episode_begin = 0;
            int _episode_end = 0;

            if (!Int32.TryParse(this.TextBox_Episode_Begin.Text, out _episode_begin))
            {
                MessageBox.Show("集數需為數字!");
                return;
            }
            if (!Int32.TryParse(this.TextBox_Episode_End.Text, out _episode_end))
            {
                MessageBox.Show("集數需為數字!");
                return;
            }
            string parameter = "";
            parameter += "<Search>";
            parameter += "<IniPath></IniPath>";
            parameter += "<KeyWord></KeyWord>";
            //parameter += "<IndexName>doc_2000,doc_2001,doc_2006,doc_2011,doc_2016,photo_2000,photo_2001,photo_2006,photo_2011,photo_2016,audio_2000,audio_2001,audio_2006,audio_2011,audio_2016,video_2000,video_2001,video_2006,video_2011,video_2016</IndexName>";
            parameter += "<IndexName>Video,Audio,Photo,Doc</IndexName>";
            parameter += "<Date></Date>";
            parameter += "<Channel></Channel>";
            parameter += "<Columns>FSPROG_B_PGMNAME;FNEPISODE;FSTYPE</Columns>";
            parameter += "<Values>" + this.TextBox_ProgramName.Text + ";" + _episode_begin.ToString() + "~" + _episode_end.ToString() + ";節目</Values>";
            parameter += "<Logics>AND;AND</Logics>";
            parameter += "<SearchMode>0</SearchMode>";
            parameter += "<Homophone>0</Homophone>";
            parameter += "<Synonym>0</Synonym>";
            parameter += "<PageSize>10</PageSize>";
            parameter += "<StartPoint>0</StartPoint>";
            parameter += "<GetColumns>FSSYS_ID;FSFILE_NO;FSPROG_B_PGMNAME;FNEPISODE;FSPROG_D_PGDENAME;FSVWNAME;FCFROM</GetColumns>";
            parameter += "<TreeNode>/</TreeNode>";
            parameter += "<OrderBy></OrderBy>";
            parameter += "<OrderType></OrderType>";
            parameter += "</Search>";



            SRH101_01 searchWindow = new SRH101_01(parameter);
            searchWindow.Show();

        }

        private void Button_List_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            //parentFrame.generateNewPane("預借清單", "PREBOOKING","2");
            SRH101_03 prebooking = new SRH101_03();
            prebooking.Show();
        }

        private void Button_Send_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            this.RadBusyIndicator_Booking.IsBusy = true;
            WSBookingFunctionSoapClient booking = new WSBookingFunctionSoapClient();
            //判斷是否有加入的項目
            booking.GetProgramPreBookingCountCompleted += (s0, args0) =>
            {
                if (args0.Error != null || string.IsNullOrEmpty(args0.Result))
                {
                    this.RadBusyIndicator_Booking.IsBusy = false;
                    return;
                }
                XDocument xmlDocResult0 = XDocument.Parse(args0.Result.ToString());
                if (xmlDocResult0.Element("Datas").Element("Data").Element("FNCOUNT").Value == "0")
                {
                    MessageBox.Show("目前沒有預借項目!");
                    this.RadBusyIndicator_Booking.IsBusy = false;
                    return;
                }
                else
                {
                    //判斷是否有權限直接調用
                    string fcIsbooking_check = "1";
                    //需要給哪些主管審核
                    string fsDeptSigner = "";

                    //判斷是否有外購

                    //booking.GetBookingOutBuyCountCompleted += (s, args) =>
                    //{

                    //    if (args.Error != null || string.IsNullOrEmpty(args.Result))
                    //    {
                    //        this.RadBusyIndicator_Booking.IsBusy = false;
                    //        return;
                    //    }


                    //    XDocument xmlDocResult = XDocument.Parse(args.Result.ToString());
                    //    if (xmlDocResult.Element("Datas").Element("Data").Element("FNOUT_BUY_COUNT").Value != "0")
                    //    {
                    //        fcout_buy = "Y";
                    //    }

                    //    //判斷是否為宏觀
                    //    booking.GetBookingInternationCountCompleted += (s3, args3) =>
                    //    {

                    //        if (args3.Error != null || string.IsNullOrEmpty(args3.Result))
                    //        {
                    //            this.RadBusyIndicator_Booking.IsBusy = false;
                    //            return;
                    //        }

                    //        xmlDocResult = XDocument.Parse(args3.Result.ToString());
                    //        if (xmlDocResult.Element("Datas").Element("Data").Element("FNFILE_DEPT_COUNT").Value != "0")
                    //        {
                    //            fcinternation = "Y";
                    //        }

                    //        if (fcout_buy == "Y" && fcinternation == "Y")
                    //        {
                    //            message = "此張調用清單需通過購片組與宏觀審核";
                    //        }
                    //        else if (fcout_buy == "Y" && fcinternation == "N")
                    //        {
                    //            message = "此張調用清單需通過購片組審核";
                    //        }
                    //        else if (fcout_buy == "N" && fcinternation == "Y")
                    //        {
                    //            message = "此張調用清單需通過宏觀審核";
                    //        }
                    //        else
                    //        {
                    //            message = "";
                    //        }

                    //        if (message != "")
                    //        {
                    //            MessageBox.Show(message);
                    //        }

                            
                    //    };

                    //    booking.GetBookingInternationCountAsync("<Data><FSUSER_ID>" + UserClass.userData.FSUSER_ID + "</FSUSER_ID></Data>");
                    //};

                    //booking.GetBookingOutBuyCountAsync("<Data><FSUSER_ID>" + UserClass.userData.FSUSER_ID + "</FSUSER_ID></Data>");

                    //取得調用清單編號
                    booking.GetBookingNoCompleted += (s1, args1) =>
                    {
                        if (args1.Error != null || String.IsNullOrEmpty(args1.Result))
                        {
                            this.RadBusyIndicator_Booking.IsBusy = false;
                            return;
                        }

                        XDocument xmlDocResult = XDocument.Parse(args1.Result.ToString());

                        _bookingNo = xmlDocResult.Element("Datas").Element("Data").Element("FSNO").Value;

                        if (_bookingNo != "")
                        {
                            //取得審核主管
                            booking.GetBookingSupervisorCompleted += (s, args) =>
                            {
                                if (args.Error != null || string.IsNullOrEmpty(args.Result))
                                {
                                    this.RadBusyIndicator_Booking.IsBusy = false;
                                    return;
                                }

                                xmlDocResult = XDocument.Parse(args.Result);
                                fsDeptSigner = xmlDocResult.Element("Datas").Element("Data").Element("FSSUPERVISOR").Value;
                                if (fsDeptSigner != "")
                                {
                                    string[] signer = fsDeptSigner.Split(',');
                                    IEnumerable<string> signer_temp = signer.Distinct();
                                    fsDeptSigner = "";
                                    for (int i = 0; i <= signer_temp.Count() - 1; i++)
                                    {
                                        fsDeptSigner += signer_temp.ElementAt<string>(i) + ",";
                                    }

                                    if (fsDeptSigner.Length > 0) fsDeptSigner = fsDeptSigner.Substring(0, fsDeptSigner.Length - 1);
                                }


                                booking.InsertBookingCompleted += (s2, args2) =>
                                {
                                    if (args2.Error != null)
                                    {
                                        this.RadBusyIndicator_Booking.IsBusy = false;
                                        return;
                                    }

                                    if (args2.Result)
                                    {
                                        newAFlow(fcIsbooking_check, fsDeptSigner);
                                    }

                                };

                                string profile = "";
                                if (_fsFileOutputCodecList != null && _fsFileOutputCodecList.Count > 0)
                                {
                                    for (int i = 0; i <= _fsFileOutputCodecList.Count - 1; i++)
                                    {
                                        profile += _fsFileOutputCodecList[i] + ';';
                                    }
                                }
                                profile = profile.Substring(0, profile.Length - 1);

                                _fsFileOutputCodecList.Clear();

                                string logo = "";
                                if (this.ComboBox_Logo.Items.Count() > 0)
                                {
                                    logo = ((FileLogoProfile)this.ComboBox_Logo.SelectedItem).fsfile_path;
                                }
                                else
                                {
                                    logo = "";
                                }

                                string parameter = "";
                                parameter += "<Data>";
                                parameter += "<FSBOOKING_NO>" + _bookingNo + "</FSBOOKING_NO>";
                                parameter += "<FSUSER_ID>" + UserClass.userData.FSUSER_ID + "</FSUSER_ID>";
                                parameter += "<FDBOOKING_DATE>" + DateTime.Now.ToString("yyyy/MM/dd") + "</FDBOOKING_DATE>";
                                parameter += "<FSFILE_OUTPUT_INFO>" + profile + "</FSFILE_OUTPUT_INFO>";
                                parameter += "<FSLOGO_INFO>" + logo + "</FSLOGO_INFO>";
                                parameter += "<FSBOOKING_REASON>" + ((BookingReason)this.ComboBox_Booking_Reason.SelectedItem).fsno + "</FSBOOKING_REASON>";
                                parameter += "<FSBOOKING_REASON_DESC>" + this.TextBox_Reason_Desc.Text + "</FSBOOKING_REASON_DESC>";
                                parameter += "<FCCHECK_STATUS>N</FCCHECK_STATUS>";
                                parameter += "<FSCHECK_ID></FSCHECK_ID>";
                                parameter += "<FDCHECK_DATE></FDCHECK_DATE>";
                                parameter += "<FCNEED_CHECK_OUT_BUY></FCNEED_CHECK_OUT_BUY>";
                                parameter += "<FCNEED_CHECK_INTERNATION></FCNEED_CHECK_INTERNATION>";
                                parameter += "<FCFILE_TRANSCODE_STATUS>N</FCFILE_TRANSCODE_STATUS>";
                                parameter += "<FCBOOKING_TYPE>2</FCBOOKING_TYPE>";
                                parameter += "<FCINTERPLAY>N</FCINTERPLAY>";
                                parameter += "<FCFILE_CATEGORY>0</FCFILE_CATEGORY>";
                                parameter += "</Data>";
                                booking.InsertBookingAsync(parameter, UserClass.userData.FSUSER_ID);

                            };

                            booking.GetBookingSupervisorAsync("<Data><FSUSER_ID>" + UserClass.userData.FSUSER_ID + "</FSUSER_ID></Data>");

                        }
                    };

                    booking.GetBookingNoAsync("<Data><FSTYPE>07</FSTYPE><FSNO_DATE>" + DateTime.Now.ToString("yyyyMMdd") +
                                        "</FSNO_DATE><FSNOTE></FSNOTE><FSCREATED_BY></FSCREATED_BY></Data>");

                }

            };

            booking.GetProgramPreBookingCountAsync("<Data><FSUSER_ID>" + UserClass.userData.FSUSER_ID + "</FSUSER_ID></Data>");
        }


        private void newAFlow(string fcbooking_check, string fsDeptSigner)
        {
            
            StringBuilder sb = new StringBuilder();
            sb.Append(@"<VariableCollection>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>FormId</name>");
            sb.Append(@"    <value>" + _bookingNo + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>NextXamlName</name>");
            sb.Append(@"    <value>/SRH/SRH136.xaml</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>hid_SendTo</name>");
            sb.Append(@"    <value>" + fcbooking_check + "</value>");
            sb.Append(@"  </variable>");
            //sb.Append(@"  <variable>");
            //sb.Append(@"    <name>Function01</name>");
            //sb.Append(@"    <value>0</value>");
            //sb.Append(@"  </variable>");
            //sb.Append(@"  <variable>");
            //sb.Append(@"    <name>SV_IsOutBuy</name>");
            //sb.Append(@"    <value>" + fcout_buy + "</value>");
            //sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>FormInfo</name>");
            sb.Append(@"    <value>調用清單：" + _bookingNo + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>USERID</name>");
            sb.Append(@"    <value>" + UserClass.userData.FSUSER_ID + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>CancelForm</name>");
            sb.Append(@"    <value>CanCancel</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>DeptSigner</name>");
            sb.Append(@"    <value>" + fsDeptSigner.Trim() + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"</VariableCollection>");//hid_SendTo
 
            FlowClinet.NewFlowWithFieldAsync(8, UserClass.userData.FSUSER_ID, sb.ToString());
        }


        private void Button_Cancel_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            _bookingNo = "";
            this.TextBox_ProgramName.Text = "";
            this.TextBox_Episode_Begin.Text = "";
            this.TextBox_Episode_End.Text = "";
            this.TextBox_Reason_Desc.Text = "";
            booking.GetFileTranscodeInfoAsync();
            this.ComboBox_Logo.SelectedIndex = 0;
        }

        private void ListBox_FileOutputInfo_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            // TODO: 在此新增事件處理常式執行項目。
            if (this.ListBox_FileOutputInfo.SelectedItem != null)
            {
                WSBookingFunctionSoapClient booking = new WSBookingFunctionSoapClient();
                booking.GetFileTranscodeInfoTxtCompleted += (s, args) =>
                {
                    if (args.Error != null || string.IsNullOrEmpty(args.Result))
                    {
                        this.TextBox_FileProfile_Desc.Text = "";
                        return;
                    }
                    this.TextBox_FileProfile_Desc.Text = args.Result;
                };

                booking.GetFileTranscodeInfoTxtAsync(((FileTranscodeProfile)this.ListBox_FileOutputInfo.SelectedItem).fsfile_path);
            }
        }

        //private void CheckBox_FileInfo_Checked(object sender, System.Windows.RoutedEventArgs e)
        //{
        //    // TODO: 在此新增事件處理常式執行項目。
        //    _fsFileOutputCodecList.Add((sender as CheckBox).Tag.ToString());
        //}

        //private void CheckBox_FileInfo_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        //{
        //    // TODO: 在此新增事件處理常式執行項目。
        //    if (_fsFileOutputCodecList != null && _fsFileOutputCodecList.Count > 0)
        //    {
        //        for (int i = 0; i <= _fsFileOutputCodecList.Count - 1; i++)
        //        {
        //            if (_fsFileOutputCodecList[i] == (sender as CheckBox).Tag.ToString())
        //            {
        //                _fsFileOutputCodecList.RemoveAt(i);
        //            }
        //        }
        //    }
        //}

        private void CheckBox_FileInfo_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
            if (sender != null)
            {
                if ((sender as CheckBox).IsChecked == true)
                {
                    if (_fsFileOutputCodecList.FirstOrDefault(item => item == (sender as CheckBox).Tag.ToString()) == null)
                        _fsFileOutputCodecList.Add((sender as CheckBox).Tag.ToString());
                }
                else
                {
                    _fsFileOutputCodecList.Remove(_fsFileOutputCodecList.FirstOrDefault(item => item == (sender as CheckBox).Tag.ToString()));
                }
            }
        }


    }


}
