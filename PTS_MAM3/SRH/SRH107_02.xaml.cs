﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSARCHIVE;

namespace PTS_MAM3.SRH
{
    public partial class SRH107_02 : ChildWindow
    {
        public Class_ARCHIVE_VAPD pageclass;
        WSARCHIVE.WSARCHIVESoapClient client = new WSARCHIVESoapClient();
        WSLogTemplateField.WSLogTemplateFieldSoapClient TFclient = new WSLogTemplateField.WSLogTemplateFieldSoapClient();
        Class_ARCHIVE_VAPD_METADATA VAPD_METADATA = new Class_ARCHIVE_VAPD_METADATA();
        public string VAPD = "", FSFILE_NO, FNSEQ_NO = "0";

        public List<WSLogTemplateField.LogTemplateField> LogList = new List<WSLogTemplateField.LogTemplateField>();
        public List<WSLogTemplateField.LogTemplateField> LogListInsert = new List<WSLogTemplateField.LogTemplateField>();
        
        public SRH107_02(string _fsfile_no,string _fstable_name)
        {
            InitializeComponent();

            Class_ARCHIVE_VAPD myclass = new Class_ARCHIVE_VAPD();
            myclass.FSTableName = _fstable_name;
            myclass.FSFile_No = _fsfile_no;

            client.GetTBLOG_VAPD_METADATACompleted += (s, args) =>
            {
                VAPD_METADATA = args.Result;
                DFieldInit();
            };
            client.GetTBLOG_VAPD_METADATAAsync(myclass);
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        /*Dennis.Wen: 因應自訂欄位增加列舉型態, 本段程式已大改, 依照不同型態產生不同控制項.*/
        private void DFieldInit()
        {
            //string VAPD = "";
            if (VAPD_METADATA.FSTableName == "TBLOG_VIDEO")
                VAPD = "V";
            else if (VAPD_METADATA.FSTableName == "TBLOG_AUDIO")
                VAPD = "A";
            else if (VAPD_METADATA.FSTableName == "TBLOG_PHOTO")
                VAPD = "P";
            else if (VAPD_METADATA.FSTableName == "TBLOG_DOC")
                VAPD = "D";
            //STO.STO300 sto300 = new STO300(VAPD, VAPD_METADATA.FSFile_No, "0");
            //sto300.Show();
            FSFILE_NO = VAPD_METADATA.FSFile_No;
            TFclient.GetTemplateFieldsAsync(VAPD, FSFILE_NO, FNSEQ_NO);
            TFclient.GetTemplateFieldsCompleted += (s, args) =>
            {
                LogList = args.Result;
                int i = 0;
                foreach (WSLogTemplateField.LogTemplateField x in LogList)
                {
                    StackPanel SP = new StackPanel();
                    Label lbl = new Label();
                    Label lbl2 = new Label();
                    //Control ctrl;

                    lbl.Content = x.FSFIELD_NAME;
                    lbl.Width = 100;

                    //if (x.FCISNULLABLE == "N")
                    //{
                    //    lbl2.Content = "*";
                    //}
                    //else
                    //{
                    //    lbl2.Content = " ";
                    //}
                    //lbl2.Foreground = new SolidColorBrush(Colors.Red);
                    //lbl2.Width = 10;

                    if (x.FSCODE_CTRL == "")//此欄位為空表示用一般的文字框控制項即可
                    {
                        //ctrl = new TextBox();

                        //ctrl.Name = "txtBox" + i.ToString();
                        //ctrl.Width = Convert.ToInt32(x.FNOBJECT_WIDTH);
                        //((TextBox)ctrl).MaxLength = Convert.ToInt32(x.FNFIELD_LENGTH);
                        //((TextBox)ctrl).Text = x.FSFIELD_VALUE;

                        //SP.Tag = x;
                        //SP.Orientation = Orientation.Horizontal;
                        //SP.Children.Insert(0, ctrl);
                        //SP.Children.Insert(0, lbl2);
                        //SP.Children.Insert(0, lbl);

                        TextBlock textblock = new TextBlock();

                        textblock.Name = "TextBlock" + i.ToString();
                        textblock.Width = Convert.ToInt32(x.FNOBJECT_WIDTH);
                        textblock.Text = x.FSFIELD_VALUE;

                        SP.Tag = x;
                        SP.Orientation = Orientation.Horizontal;
                        SP.Children.Insert(0, textblock);
                        //SP.Children.Insert(0, lbl2);
                        SP.Children.Insert(0, lbl);
                    }
                    else
                    {
                        switch (x.FSCODE_CTRL)
                        {
                            case "ComboBox":
                                ComboBox comboBox = new ComboBox();
                                comboBox.Name = "cbxBox" + i.ToString();
                                comboBox.Width = Convert.ToInt32(x.FNOBJECT_WIDTH);
                                comboBox.IsEnabled = false;
                                comboBox.DisplayMemberPath = "FSCODE_NAME";
                                comboBox.SelectedValuePath = "FSCODE_CODE";

                                List<WSLogTemplateField.LogTemplateField_CODE> CodeList = new List<WSLogTemplateField.LogTemplateField_CODE>();

                                if (x.CODELIST.Count > 0)
                                {
                                    CodeList.Add(
                                        new WSLogTemplateField.LogTemplateField_CODE
                                        {
                                            FSCODE_ID = "",
                                            FSCODE_CODE = "",
                                            FSCODE_NAME = "(未選擇)",
                                            FSCODE_ENAME = "",
                                            FSCODE_ORDER = "",

                                            FSCODE_SET = "",
                                            FSCODE_NOTE = "",
                                            FSCODE_CHANNEL_ID = "",

                                            FSCREATED_BY = "",
                                            FDCREATED_DATE = DateTime.Parse("1900/01/01"),
                                            FSUPDATED_BY = "",
                                            FDUPDATED_DATE = DateTime.Parse("1900/01/01")
                                        }
                                    );


                                    foreach (WSLogTemplateField.LogTemplateField_CODE code in x.CODELIST)
                                    {
                                        CodeList.Add(code);
                                    }

                                    comboBox.ItemsSource = CodeList;

                                    if (x.FSFIELD_VALUE != "")
                                    {
                                        comboBox.SelectedValue = x.FSFIELD_VALUE;
                                    }
                                    else
                                    {
                                        comboBox.SelectedIndex = 0;
                                    }
                                }

                                SP.Tag = x;
                                SP.Orientation = Orientation.Horizontal;
                                SP.Children.Insert(0, comboBox);
                                SP.Children.Insert(0, lbl2);
                                SP.Children.Insert(0, lbl);


                                break;

                            case "CheckBox":
                                WrapPanel wrap = new WrapPanel();
                                wrap.Orientation = Orientation.Horizontal;
                                wrap.Width = Convert.ToInt32(x.FNOBJECT_WIDTH);
                                wrap.Name = "wrapPnl" + i.ToString();

                                if (x.CODELIST.Count > 0)
                                {
                                    int j = 0;
                                    foreach (WSLogTemplateField.LogTemplateField_CODE code in x.CODELIST)
                                    {
                                        CheckBox chk = new CheckBox();
                                        chk.Name = "chkBox" + i.ToString() + "_" + j.ToString();

                                        chk.Content = code.FSCODE_NAME + "　";
                                        chk.Tag = code.FSCODE_CODE;
                                        chk.IsEnabled = false;

                                        if (code.FSCODE_ISENABLED != true) chk.IsEnabled = false; //<=====

                                        if (x.FSFIELD_VALUE.Contains(code.FSCODE_CODE + ";")) chk.IsChecked = true;

                                        wrap.Children.Insert(wrap.Children.Count, chk);

                                        j++;
                                    }

                                }

                                SP.Tag = x;
                                SP.Orientation = Orientation.Horizontal;
                                SP.Children.Insert(0, wrap);
                                SP.Children.Insert(0, lbl2);
                                SP.Children.Insert(0, lbl);

                                break;
                        }
                    }

                    this.ListBox_Control.Items.Add(SP);
                    i++;
                }
            };
        }
    }
}

