﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSBookingFunction;
using System.Xml.Linq;

namespace PTS_MAM3.SRH
{
    public partial class SRH136_01 : ChildWindow
    {
        private string _fsBookingNo = "";
        private string _fsseq = "";

        WSBookingFunctionSoapClient booking = new WSBookingFunctionSoapClient();

        public SRH136_01(string _fsBookingNo, string _fsseq, bool _fbalter)
        {
            InitializeComponent();
            this._fsBookingNo = _fsBookingNo;
            this._fsseq = _fsseq;

            if (!_fbalter)
            {
                this.TextBox_Memo.IsReadOnly = true;
                this.OKButton.Visibility = System.Windows.Visibility.Collapsed;
                this.CancelButton.Visibility = System.Windows.Visibility.Collapsed;
            }

            //取得原來的備註
            booking.GetBookingMemoCompleted += (s, args) =>
            {
                if (args.Error != null || string.IsNullOrEmpty(args.Result))
                {
                    return;
                }

                XDocument xmlDocResult = XDocument.Parse(args.Result.ToString());
                this.TextBox_Memo.Text = (xmlDocResult.Element("Datas").Element("Data").Element("FSMEMO") != null ? xmlDocResult.Element("Datas").Element("Data").Element("FSMEMO").Value : "");
            };

            booking.GetBookingMemoAsync("<Data><FSBOOKING_NO>" + _fsBookingNo + "</FSBOOKING_NO><FSSEQ>" + _fsseq.ToString() +
                "</FSSEQ></Data>");

        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            //存至資料庫
            booking.UpdateBookingMemoCompleted += (s, args) =>
            {
                this.DialogResult = true;
            };

            booking.UpdateBookingMemoAsync("<Data><FSBOOKING_NO>" + _fsBookingNo + "</FSBOOKING_NO><FSSEQ>" + _fsseq.ToString() +
                "</FSSEQ><FSMEMO>" + this.TextBox_Memo.Text + "</FSMEMO></Data>", UserClass.userData.FSUSER_ID);
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

