﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SearchClassLibrary;
using PTS_MAM3.WSBookingFunction;
using PTS_MAM3.Common;

namespace PTS_MAM3.SRH
{
    public partial class SRH106_01 : ChildWindow
    {
        public List<SearchClassLibrary.PreBookingClass> preBookingListReturn;

        public SRH106_01(List<SearchClassLibrary.PreBookingClass> preBookingList)
        {
            InitializeComponent();

            this.Title = "已加入清單項目";

            preBookingListReturn = preBookingList;

            if (preBookingListReturn != null && preBookingListReturn.Count() > 0)
            {
                BindListBox();
            }
            else
            {
                this.DialogResult = false;
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void Button_Delete_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: 在此新增事件處理常式執行項目。
			//MessageBox.Show(((Button)sender).Tag.ToString());
            chWndMessageBox confirm = new chWndMessageBox("刪除預借清單項目", "確定要刪除此清單項目?", 0);
            confirm.Show();
            confirm.Closing += (s, args) =>
            {
                if (confirm.DialogResult == true)
                {
                    if (preBookingListReturn != null && preBookingListReturn.Count() > 0)
                    {
                        for (int i = 0; i <= preBookingListReturn.Count() - 1; i++)
                        {
                            if (((Button)sender).Tag.ToString() == preBookingListReturn.ElementAt<SearchClassLibrary.PreBookingClass>(i).FSFILE_NO)
                            {
                                preBookingListReturn.RemoveAt(i);
                                break;
                            }
                        }
                        BindListBox();
                    }
                }
            };

        }


        private void BindListBox()
        {
            this.ListBox_Exist_PreBooking.ItemsSource = null;
            this.ListBox_Exist_PreBooking.ItemsSource = preBookingListReturn;
        }
    }

}

