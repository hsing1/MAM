﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using PTS_MAM3.STT;
using PTS_MAM3.WSBROADCAST;
using System.Windows.Browser;
using System.Windows.Data;

namespace PTS_MAM3.STT
{
    public partial class STT500 : Page
    {
        WSBROADCASTSoapClient client = new WSBROADCASTSoapClient();             //產生新的代理類別
        List<Class_BROADCAST> m_ListFormData = new List<Class_BROADCAST>();     //記錄查詢到的成案資料
        private FlowFieldCom.FlowFieldComSoapClient ComClient = new FlowFieldCom.FlowFieldComSoapClient();
        WSDELETE.WSDELETESoapClient wsDeleteClient = new WSDELETE.WSDELETESoapClient();
        WSFlowCancel.WSFlowCancelSoapClient cancelClient = new WSFlowCancel.WSFlowCancelSoapClient();

        public STT500()
        {
            InitializeComponent();

            //取得所有的送帶轉檔置換檔_BY新增者
            client.fnGetTBBROADCAST_ALL_BYUSERID_CHANGECompleted += new EventHandler<fnGetTBBROADCAST_ALL_BYUSERID_CHANGECompletedEventArgs>(client_fnGetTBBROADCAST_ALL_BYUSERID_CHANGECompleted);
           
            //列印送帶轉檔單
            client.CallREPORT_BROCompleted += new EventHandler<CallREPORT_BROCompletedEventArgs>(client_CallREPORT_BROCompleted);

            Load_TBBROADCAST_ALL_BYUSERID_CHANGE();

            //選擇不同表單時 顯示流程狀態
            DGBroListDouble.SelectionChanged += new SelectionChangedEventHandler(DGBroListDouble_SelectionChanged);
            ComClient.GetApprovalListDetailByFormIDCompleted += new EventHandler<FlowFieldCom.GetApprovalListDetailByFormIDCompletedEventArgs>(ComClient_GetApprovalListDetailByFormIDCompleted);

            
            //抽單的實際動作 by Jarvis20130507
            wsDeleteClient.DeleteFileCompleted += new EventHandler<WSDELETE.DeleteFileCompletedEventArgs>(wsDeleteClient_DeleteFileCompleted);

            //根據表單ID尋找FlowID by Jarvis20130507
            ComClient.GetFlowIDfromFormIDCompleted += new EventHandler<FlowFieldCom.GetFlowIDfromFormIDCompletedEventArgs>(ComClient_GetFlowIDfromFormIDCompleted);

            //更新TBBROADCAST中送帶轉檔單的狀態 by Jarvis20130521
            client.UPDATE_TBBROADCAST_Staus_DirectlyCompleted += new EventHandler<UPDATE_TBBROADCAST_Staus_DirectlyCompletedEventArgs>(client_UPDATE_TBBROADCAST_Staus_DirectlyCompleted);

            //根據送帶轉檔單編號查詢檔案所有狀態 by Jarvis20130621
            client.GetTBLOG_VIDEO_FileInfoList_BYBROIDCompleted += new EventHandler<GetTBLOG_VIDEO_FileInfoList_BYBROIDCompletedEventArgs>(client_GetTBLOG_VIDEO_FileInfoList_BYBROIDCompleted);
            //根據檔案編號查傳回所有段落資料 by Jarvis20130621
            client.GetTBLOG_VIDEO_SEG_COUNTCompleted += new EventHandler<GetTBLOG_VIDEO_SEG_COUNTCompletedEventArgs>(client_GetTBLOG_VIDEO_SEG_COUNTCompleted);

            //抽單發生錯誤時回復TBDELETED與TBLOG_VIDEO的資料
            wsDeleteClient.Reply_Detached_ErrorCompleted += new EventHandler<WSDELETE.Reply_Detached_ErrorCompletedEventArgs>(wsDeleteClient_Reply_Detached_ErrorCompleted);

            wsDeleteClient.Reply_Detached_Error_BatchCompleted += new EventHandler<WSDELETE.Reply_Detached_Error_BatchCompletedEventArgs>(wsDeleteClient_Reply_Detached_Error_BatchCompleted);

            //列印送帶轉檔單_多筆短帶
            client.CallREPORT_BRO_PROMOCompleted += new EventHandler<CallREPORT_BRO_PROMOCompletedEventArgs>(client_CallREPORT_BRO_PROMOCompleted);
        }

        

        //實作-列印送帶轉檔單
        void client_CallREPORT_BROCompleted(object sender, CallREPORT_BROCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    if (e.Result.ToString().StartsWith("錯誤"))
                        MessageBox.Show(e.Result.ToString(), "提示訊息", MessageBoxButton.OK);
                    else
                        HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + e.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,resizable=yes");
                }
                else
                    MessageBox.Show("查無相關資料，或該筆資料已不存在資料庫中。", "提示訊息", MessageBoxButton.OK);
            };
        }

        //實作-取得所有的送帶轉檔置換檔_BY新增者
        void client_fnGetTBBROADCAST_ALL_BYUSERID_CHANGECompleted(object sender, fnGetTBBROADCAST_ALL_BYUSERID_CHANGECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    //宣告PagedCollectionView,將撈取的資料放入
                    PagedCollectionView pcv = new PagedCollectionView(e.Result);
                    //指定DataGrid以及DataPager的內容
                    DGBroListDouble.ItemsSource = pcv;
                    DataPager.Source = pcv;
                }
            }
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息
        }

        void ComClient_GetApprovalListDetailByFormIDCompleted(object sender, FlowFieldCom.GetApprovalListDetailByFormIDCompletedEventArgs e)
        {
            dataGrid2.ItemsSource = e.Result;
        }

        //實做 傳回流程狀態
        void DGBroListDouble_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DGBroListDouble.SelectedIndex != -1)
            {
                ComClient.GetApprovalListDetailByFormIDAsync(((Class_BROADCAST)DGBroListDouble.SelectedItem).FSBRO_ID);
            }
        }   

        //載入送帶轉檔置換資料_BY新增者
        private void Load_TBBROADCAST_ALL_BYUSERID_CHANGE()
        {
            DateTime dtStart = DateTime.Now.AddYears(-1);
            DateTime dtEnd = DateTime.Now.AddDays(1);       //要加一天才能帶出今天的
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
            client.fnGetTBBROADCAST_ALL_BYUSERID_CHANGEAsync(UserClass.userData.FSUSER_ID.ToString().Trim(), dtStart, dtEnd);  
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        //新增
        private void btnBroAdd_Click(object sender, RoutedEventArgs e)
        {
            STT500_01 BroChange_Add_frm = new STT500_01();
            BroChange_Add_frm.Show();

            BroChange_Add_frm.Closing += (s, args) =>
            {
                if (BroChange_Add_frm.DialogResult == true)
                {
                    Load_TBBROADCAST_ALL_BYUSERID_CHANGE();
                }                    
            };
        }

        //修改
        private void btBroModify_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGBroListDouble.SelectedItem == null)
            {
                MessageBox.Show("請選擇預修改的送帶轉檔置換資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            if (((Class_BROADCAST)DGBroListDouble.SelectedItem).FCCHECK_STATUS != "C")
            {
                MessageBox.Show("該筆送帶轉檔置換資料不是「審核中」，無法修改", "提示訊息", MessageBoxButton.OK);
                return;
            }

            STT500_02 STT500_02_frm = new STT500_02(((Class_BROADCAST)DGBroListDouble.SelectedItem));
            STT500_02_frm.Show();

            STT500_02_frm.Closing += (s, args) =>
            {
                if (STT500_02_frm.DialogResult == true)
                {
                    Load_TBBROADCAST_ALL_BYUSERID_CHANGE();
                }                   
            };
        }

        //查詢
        private void btnBroQuery_Click(object sender, RoutedEventArgs e)
        {
            STT500_04 STT500_04_frm = new STT500_04();
            STT500_04_frm.Show();

            STT500_04_frm.Closed += (s, args) =>
            {
                if (STT500_04_frm.m_ListFormData.Count != 0)
                {
                    //this.DGBroListDouble.DataContext = null;
                    //this.DGBroListDouble.DataContext = STT500_04_frm.m_ListFormData;
                    //this.DGBroListDouble.SelectedIndex = 0;

                    DGBroListDouble.ItemsSource = null;
                    PagedCollectionView pcv = new PagedCollectionView(STT500_04_frm.m_ListFormData);
                    DGBroListDouble.ItemsSource = pcv;
                    DataPager.Source = pcv; 
                }
            };
        }

        //更新
        private void btnBroRefresh_Click(object sender, RoutedEventArgs e)
        {
            Load_TBBROADCAST_ALL_BYUSERID_CHANGE();           
        }

        //列印
        private void btnRpt_Click(object sender, RoutedEventArgs e)
        {
             //判斷是否有選取DataGrid
            if (DGBroListDouble.SelectedItem == null)
            {
                MessageBox.Show("請選擇預列印的節目資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //列印成案單(丟入參數：成案單編號、查詢者、SessionID)
           // client.CallREPORT_BROAsync(((Class_BROADCAST)DGBroListDouble.SelectedItem).FSBRO_ID.ToString().Trim(), UserClass.userData.FSUSER_ID.ToString().Trim(), UserClass.userData.FSSESSION_ID.ToString().Trim());

            if (((Class_BROADCAST)DGBroListDouble.SelectedItem).FSBRO_TYPE == "P")//主頁面只有短帶能列印資料 by Jarvis 20130822
            {
                PrintSTT_PROMO();
            }
            else
            {
                MessageBox.Show("非短帶資料請點擊兩下要列印的資料後，選擇「列印送帶轉檔明細表」執行列印動作", "提示", MessageBoxButton.OK);
            }
        }

        //列印送帶轉檔單_多筆短帶資料
        private void PrintSTT_PROMO()
        {
            //列印成案單(丟入參數：成案單編號、查詢者、SessionID)
            client.CallREPORT_BRO_PROMOAsync(((Class_BROADCAST)DGBroListDouble.SelectedItem).FSPRINTID.ToString().Trim(), UserClass.userData.FSUSER_ID.ToString().Trim(), UserClass.userData.FSSESSION_ID.ToString().Trim());
        }

        //實作-列印送帶轉檔單_多筆短帶
        void client_CallREPORT_BRO_PROMOCompleted(object sender, CallREPORT_BRO_PROMOCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    if (e.Result.ToString().StartsWith("錯誤"))
                        MessageBox.Show(e.Result.ToString(), "提示訊息", MessageBoxButton.OK);
                    else
                        HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + e.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,resizable=yes");
                }
                else
                    MessageBox.Show("查無相關資料，或該筆資料已不存在資料庫中。", "提示訊息", MessageBoxButton.OK);
            };
        }

        //連續點擊-檢視送帶轉檔  
        private void DGBroListDouble_RowDoubleClicked(object sender, DataGridDCTest.DataGridRowClickedArgs e)
        {
            DataGridDCTest.DoubleClickDataGrid dcdg = sender as DataGridDCTest.DoubleClickDataGrid;

            //判斷是否有選取DataGrid
            if (dcdg.SelectedItem == null)
            {
                MessageBox.Show("請選擇預檢視的送帶轉檔置換資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            STT500_06 STT500_06_frm = new STT500_06(((Class_BROADCAST)DGBroListDouble.SelectedItem).FSBRO_ID.ToString().Trim());
            STT500_06_frm.Show();
        }

        #region Jarvis 送帶轉檔置換單抽單

        Class_BROADCAST class_Broadcast = null;

        void ComClient_GetFlowIDfromFormIDCompleted(object sender, FlowFieldCom.GetFlowIDfromFormIDCompletedEventArgs e)
        {
            cancelClient.BRO100Cancel_2Completed += (s, args) =>
            {
                bool b = args.Result;
            };
            List<PTS_MAM3.FlowFieldCom.Class_FlowIDfromFormID> resault = (List<PTS_MAM3.FlowFieldCom.Class_FlowIDfromFormID>)e.Result;
            cancelClient.BRO100Cancel_2Async(resault.FirstOrDefault().ActiveID, resault.FirstOrDefault().FormId, UserClass.userData.FSUSER_ID);

        }

        void client_UPDATE_TBBROADCAST_Staus_DirectlyCompleted(object sender, UPDATE_TBBROADCAST_Staus_DirectlyCompletedEventArgs e)
        {
            if (STT100.GoFlow)
            {
                var result = e.Result;
            }
            else
            {
                if (e.Error == null)
                {
                    CancelMessage(e.Result);
                }
            }
        }

        private void CancelMessage(bool Result)
        {
            this.DetachedBtn.IsEnabled = true;
            if (Result == true)
            {
                //Load_TBBROADCAST_ALL_BYUSERID();
                Load_TBBROADCAST_ALL_BYUSERID_CHANGE();
                //ShowMainData("P", "", "", 0, 0);
                MessageBox.Show("抽單成功");
            }
            else
            { MessageBox.Show("抽單失敗!"); }

        }

        void client_GetTBLOG_VIDEO_SEG_COUNTCompleted(object sender, GetTBLOG_VIDEO_SEG_COUNTCompletedEventArgs e)
        {
            List<Class_LOG_VIDEO_SEG> SEG_Result = e.Result;
            bool flag = true;
            foreach (Class_LOG_VIDEO_SEG clvs in SEG_Result)
            {
                if (clvs.FCLOW_RES != "N")
                {
                    MessageBox.Show("此筆送帶轉檔單的檔案不為「待轉擋」，無法抽單", "提示", MessageBoxButton.OK);
                    this.DetachedBtn.IsEnabled = true;
                    flag = false;
                    return;
                }
            }

            if (flag)
            {
                if (ASK())
                {
                    DeleteVideoDataMethod(_deleteFileList);
                    //DeleteVideoDataMethod(e.Result[0].FSFILE_NO);
                }
                else
                {
                    this.DetachedBtn.IsEnabled = true;
                }
            }
        }

        PTS_MAM3.WSDELETE.Class_DELETED _class_deleted = new PTS_MAM3.WSDELETE.Class_DELETED();

        void DeleteVideoDataMethod(string _fsfile_no)
        {
            List<PTS_MAM3.WSDELETE.Class_DELETED> _class_deleted_list = new List<WSDELETE.Class_DELETED>();

            _class_deleted._fsfile_no = _fsfile_no;//_deleted_file_no_list[i];
            _class_deleted._fsfile_type = "V";// this._class_deleted_parameter._fsfile_type;
            _class_deleted._fcstatus = "N";
            _class_deleted._fscreated_by = UserClass.userData.FSUSER_ID;
            _class_deleted._fdcreated_date = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
            _class_deleted._fsmemo = "刪除方式：抽單";
            _class_deleted_list.Add(_class_deleted);
            wsDeleteClient.DeleteFileAsync(_class_deleted_list);
        }

        void DeleteVideoDataMethod(List<string> fileList)
        {
            List<PTS_MAM3.WSDELETE.Class_DELETED> _class_deleted_list = new List<WSDELETE.Class_DELETED>();
            foreach (string str in fileList)
            {
                PTS_MAM3.WSDELETE.Class_DELETED _class_deleted = new PTS_MAM3.WSDELETE.Class_DELETED();

                _class_deleted._fsfile_no = str;//_deleted_file_no_list[i];
                _class_deleted._fsfile_type = "V";// this._class_deleted_parameter._fsfile_type;
                _class_deleted._fcstatus = "N";
                _class_deleted._fscreated_by = UserClass.userData.FSUSER_ID;
                _class_deleted._fdcreated_date = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                _class_deleted._fsmemo = "刪除方式：抽單";
                _class_deleted_list.Add(_class_deleted);
            }
            wsDeleteClient.DeleteFileAsync(_class_deleted_list);
        }

        List<string> _deleteFileList = new List<string>();
        List<PTS_MAM3.WSDELETE.Class_DELETED> _class_deleted_list = new List<WSDELETE.Class_DELETED>();

        void client_GetTBLOG_VIDEO_FileInfoList_BYBROIDCompleted(object sender, GetTBLOG_VIDEO_FileInfoList_BYBROIDCompletedEventArgs e)
        {
            if (e.Result.Count > 0)
            {
                string[] fileArr = e.Result[0].Split(new char[] { ';' });

                _deleteFileList.Clear();

                foreach (string str in e.Result)
                {
                    string[] fileStateArr = str.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                    if (fileStateArr.Length == 2)
                    {
                        _deleteFileList.Add(fileStateArr[0]);
                    }
                }

                if (fileArr[1] == "待轉檔")//FCFILE_STATUS=B
                {
                    client.GetTBLOG_VIDEO_SEG_COUNTAsync(fileArr[0]);
                }
                else
                {
                    MessageBox.Show("檔案狀態為「" + fileArr[1] + "」無法抽單", "提示", MessageBoxButton.OK);
                    DetachedBtn.IsEnabled = true;
                }
            }
        }

        void wsDeleteClient_DeleteFileCompleted(object sender, WSDELETE.DeleteFileCompletedEventArgs e)
        {
            if (STT100.GoFlow)
            {
                CancelMessage(e.Result.Count == 0);
            }
            else
            {
                if (e.Result.Count == 0)
                {
                    client.UPDATE_TBBROADCAST_Staus_DirectlyAsync(class_Broadcast.FSBRO_ID, "X", UserClass.userData.FSUSER_ID);//改變該筆資料在TBBROADCAST中的狀態
                }
                else
                {
                    //有回傳結果代表有發生錯誤，要復原資料

                   // wsDeleteClient.Reply_Detached_ErrorAsync(_class_deleted._fsfile_no, class_Broadcast.FCCHECK_STATUS, UserClass.userData.FSUSER_ID);
                    wsDeleteClient.Reply_Detached_Error_BatchAsync(_class_deleted_list);

                    DetachedBtn.IsEnabled = true;
                }

            }
        }

        void wsDeleteClient_Reply_Detached_ErrorCompleted(object sender, WSDELETE.Reply_Detached_ErrorCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {
                    MessageBox.Show("抽單過程發生錯誤，資料已回復!", "提示訊息", MessageBoxButton.OK);
                    Load_TBBROADCAST_ALL_BYUSERID_CHANGE();
                    DetachedBtn.IsEnabled = true;
                }
            }
        }

        void wsDeleteClient_Reply_Detached_Error_BatchCompleted(object sender, WSDELETE.Reply_Detached_Error_BatchCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {
                    MessageBox.Show("抽單過程發生錯誤，資料已回復!", "提示訊息", MessageBoxButton.OK);
                    Load_TBBROADCAST_ALL_BYUSERID_CHANGE();
                    DetachedBtn.IsEnabled = true;
                }
            }
        }

        private void DetachedBtn_Click(object sender, RoutedEventArgs e)
        {
            this.DetachedBtn.IsEnabled = false;
            class_Broadcast = ((Class_BROADCAST)DGBroListDouble.SelectedItem);
            string BRO_ID = class_Broadcast.FSBRO_ID;

            //判斷是否為本人建立
            if (class_Broadcast.FSCREATED_BY != UserClass.userData.FSUSER_ID.ToString().Trim())
            {
                MessageBox.Show("此登入帳號非此資料之申請帳號，無法抽單");
                this.DetachedBtn.IsEnabled = true;
                return;
            }

            if (STT100.GoFlow)
            {
                if (class_Broadcast.FCCHECK_STATUS == "N")
                { //審核中，刪DB資料，刪除Flow裡的資料

                    //更新TBBROADCAST中送帶轉檔單的狀態
                    //client.UPDATE_TBBROADCAST_StausCompleted += new EventHandler<UPDATE_TBBROADCAST_StausCompletedEventArgs>(client_UPDATE_TBBROADCAST_StausCompleted);
                    //client.UPDATE_TBBROADCAST_StausAsync(BRO_ID, "X", UserClass.userData.FSUSER_ID);//改變該筆資料在TBBROADCAST中的狀態
                    if (ASK())
                    {
                        //尋找檔案ID
                        client.GetTBLOG_VIDEO_FileNOList_BYBROIDAsync(BRO_ID);

                        //根據節目編號查詢FlowID
                        ComClient.GetFlowIDfromFormIDAsync(BRO_ID);
                    }
                    else
                    {
                        this.DetachedBtn.IsEnabled = true;
                    }
                }
                else if (class_Broadcast.FCCHECK_STATUS == "Y")
                {//審核通過，刪DB資料(通過審核後已不在Flow中)
                    if (ASK())
                    {
                        //更新TBBROADCAST中送帶轉檔單的狀態(這是置換用)
                        //client.UPDATE_TBBROADCAST_StausAsync(BRO_ID, "X", UserClass.userData.FSUSER_ID);//改變該筆資料在TBBROADCAST中的狀態
                        client.UPDATE_TBBROADCAST_Staus_DirectlyAsync(BRO_ID, "X", UserClass.userData.FSUSER_ID);//改變該筆資料在TBBROADCAST中的狀態

                        //尋找檔案ID
                        client.GetTBLOG_VIDEO_FileNOList_BYBROIDAsync(BRO_ID);
                    }
                    else
                    {
                        this.DetachedBtn.IsEnabled = true;
                    }
                }
                else//轉檔、抽單、退回
                {
                    MessageBox.Show("表單狀態為「" + class_Broadcast.FCCHECK_STATUS_NAME + "」無法抽單!!");
                    this.DetachedBtn.IsEnabled = true;
                }
            }
            else
            {
                if (class_Broadcast.FCCHECK_STATUS == "G")
                {
                    client.GetTBLOG_VIDEO_FileInfoList_BYBROIDAsync(BRO_ID);
                }
                else//轉檔、抽單、退回
                {
                    MessageBox.Show("表單狀態為「" + class_Broadcast.FCCHECK_STATUS_NAME + "」無法抽單!!");
                    this.DetachedBtn.IsEnabled = true;
                }
            }
        }


        bool ASK()
        {
            bool result = false;

            MessageBoxResult dialogResult = MessageBox.Show("確定抽單?", "確認視窗", MessageBoxButton.OKCancel);
            if (dialogResult == MessageBoxResult.OK)
            {
                result = true;
            }

            return result;
        }

        #endregion
    }
}
