﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSMAMFunctions;
using PTS_MAM3.ProgData;
using PTS_MAM3.WSBROADCAST;
using System.Text;
using PTS_MAM3.FlowMAM;

namespace PTS_MAM3.STT
{
    public partial class STT500_05 : ChildWindow
    {
        WSBROADCASTSoapClient client = new WSBROADCASTSoapClient();                 //產生新的代理類別        
        WSMAMFunctionsSoapClient clientMAM = new WSMAMFunctionsSoapClient() ;
        public List<Class_LOG_VIDEO> ListLogVideo = new List<Class_LOG_VIDEO>();    //入庫影像檔集合
        private string m_2ndMemo = "";                                              //Secondary備註
        Class_BROADCAST m_FormData = new Class_BROADCAST();
        FlowMAMSoapClient clientFlow = new FlowMAMSoapClient();                     //後端的流程引擎

        string m_strFileTypeName = "";   //入庫檔案類型名稱
        string m_strFileTypeID = "";     //入庫檔案類型編號
        string m_strType = "";           //類型
        string m_strID = "";             //編碼
        string m_strEpisode = "";        //集別
        string m_strCHANNELID = "";      //頻道別   
        string m_strFileNO = "";         //檔案編號(修改時用的)
        string m_strBroID = "";          //送帶轉檔單單號   

        public string strFromID = "";    //Flow專用(成案單編號)
        public int intFlowID;            //Flow專用
        public int intProcessID;         //Flow專用

        public STT500_05(string strFromID)
        {
            if (strFromID.ToString().Trim() == "")
                return;
            InitializeComponent();
            InitializeForm();     //初始化本頁面
            client.GetTBBROADCAST_BYBROIDAsync(strFromID);
        }

        void InitializeForm() //初始化本頁面
        {   
            //查詢入庫影像檔資料_透過檔案節目編號及集別            
            client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted += new EventHandler<GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompletedEventArgs>(client_GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted);

            //用送帶轉檔單號取得送帶轉檔檔
            client.GetTBBROADCAST_BYBROIDCompleted += new EventHandler<GetTBBROADCAST_BYBROIDCompletedEventArgs>(client_GetTBBROADCAST_BYBROIDCompleted);

            //後端的流程引擎-審核流程
            clientFlow.CallFlow_WorkWithFieldCompleted += new EventHandler<CallFlow_WorkWithFieldCompletedEventArgs>(clientFlow_CallFlow_WorkWithFieldCompleted);
        }

        private void ClassToForm()  //將前一頁點選的節目資料繫集到畫面上
        {         
            if (m_FormData.FSBRO_TYPE.Trim() == "")
            {
                MessageBox.Show("傳入參數異常，請聯絡系統管理員", "提示訊息", MessageBoxButton.OK);
                return;  
            }

            tbxBro_ID.Text = m_FormData.FSBRO_ID;
            tbxPROG_NAME.Text = m_FormData.FSID_NAME;
            tbxPROG_NAME.Tag = m_FormData.FSID;

            m_strBroID = m_FormData.FSBRO_ID.Trim();
            m_strID = m_FormData.FSID.Trim();
            m_strType = m_FormData.FSBRO_TYPE.Trim();
            m_strEpisode = m_FormData.SHOW_FNEPISODE.Trim();
            m_strCHANNELID = m_FormData.FSCHANNELID.Trim();

            if (m_FormData.SHOW_FNEPISODE != "")
            {
                tbxEPISODE.Text = m_FormData.SHOW_FNEPISODE;
                lblEPISODE_NAME.Content = m_FormData.FNEPISODE_NAME;
            }
            else
                m_strEpisode = "0";

            if (m_FormData.FSBRO_TYPE == "G")
            {
                rdbProg.IsChecked = true;                
            }
            else if (m_FormData.FSBRO_TYPE == "P")
            {
                rdbPromo.IsChecked = true;
                tbPROG_NANE.Text = "短帶名稱";
                tbFNEPISODE.Visibility = Visibility.Collapsed;
                tbxEPISODE.Visibility = Visibility.Collapsed;
                btnEPISODE.Visibility = Visibility.Collapsed;
                tbEPISODE_NAME.Visibility = Visibility.Collapsed;
                lblEPISODE_NAME.Visibility = Visibility.Collapsed;               
            }

            //透過節目編號去查詢入庫影像檔
            this.DGLogList.DataContext = null;
            client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDAsync(m_strType, m_strID, m_strEpisode, m_strBroID);
        }

        #region 實作       
                
        //實作-查詢入庫影像檔資料_透過檔案節目編號及集別
        void client_GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted(object sender, GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    this.DGLogList.DataContext = e.Result;
                    ListLogVideo = e.Result;

                    if (this.DGLogList.DataContext != null)
                    {
                        DGLogList.SelectedIndex = 0;
                    }
                }
            }
        }

        //實作-用送帶轉檔單號取得送帶轉檔檔
        void client_GetTBBROADCAST_BYBROIDCompleted(object sender, GetTBBROADCAST_BYBROIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    m_FormData = (Class_BROADCAST)(e.Result[0]);
                    ClassToForm();
                }
            }
        }

        //實作-審核流程
        void clientFlow_CallFlow_WorkWithFieldCompleted(object sender, CallFlow_WorkWithFieldCompletedEventArgs e)
        {
            if (e.Error == null && e.Result != "")
                MessageBox.Show("「" + tbxPROG_NAME.Text.Trim() + "-" + tbxEPISODE.Text.Trim() + "」送帶轉檔置換資料審核完成！", "提示訊息", MessageBoxButton.OK);
            else
                MessageBox.Show("呼叫流程引擎異常，「" + tbxPROG_NAME.Text.Trim() + "-" + tbxEPISODE.Text.Trim() + "」送帶轉檔置換資料審核失敗！", "提示訊息", MessageBoxButton.OK);

            this.DialogResult = true;
        }

        #endregion      

        //駁回
        private void btnReject_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult resultMsg = MessageBox.Show("確定要不通過「" + tbxPROG_NAME.Text.Trim() + "-" + tbxEPISODE.Text.Trim() + "」送帶轉檔置換資料嗎?", "提示訊息", MessageBoxButton.OKCancel);
            if (resultMsg == MessageBoxResult.Cancel)
                return ;

            ContinueFlow("Reject");

            btnApprove.IsEnabled = false;
            btnReject.IsEnabled = false;
            btnCancel.IsEnabled = false;
        }

        //核可
        private void btnApprove_Click(object sender, RoutedEventArgs e)
        {
            btnApprove.IsEnabled = false;
            btnReject.IsEnabled = false;
            btnCancel.IsEnabled = false;

            ContinueFlow("Approve");
        }

        //取消
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //檢視轉檔資訊
        private void btnView_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGLogList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預檢視的資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            string strFileTypeName = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSARC_TYPE_NAME.Trim();
            string strFileTypeID = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSARC_TYPE.Trim();

            STT300_03 STT300_03_frm = new STT300_03(strFileTypeName, strFileTypeID, m_strType, m_strID, m_strEpisode, (Class_LOG_VIDEO)DGLogList.SelectedItem, m_strBroID);
            STT300_03_frm.Show();
        }

        #region DataGrid變色

        private void ChildWindow_Loaded(object sender, RoutedEventArgs e)
        {
            DGLogList.LoadingRow += new EventHandler<DataGridRowEventArgs>(DGLogList_LoadingRow);
        }

        //實作-DataGrid變色
        void DGLogList_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Background = new SolidColorBrush(Colors.White);

            Class_LOG_VIDEO LOG_VIDEO = e.Row.DataContext as Class_LOG_VIDEO;

            if (LOG_VIDEO.FSCHANGE_FILE_NO != "")
                e.Row.Foreground = new SolidColorBrush(Colors.Red);
        }

        #endregion

        #region 流程引擎

        void ContinueFlow(string strSW)
        {
            string hid_SendTo = "";
            string parameter01 = "";

            if (strSW == "Approve")         //核可
            {
                hid_SendTo = "1";
                parameter01 = "C";
            }
            else if (strSW == "Reject")     //駁回
            {
                hid_SendTo = "2";
                parameter01 = "D";
            }

            StringBuilder sb = new StringBuilder();
            sb.Append(@"<VariableCollection>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>hid_SendTo</name>");
            sb.Append(@"    <value>" + hid_SendTo + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>parameter01</name>");
            sb.Append(@"    <value>" + parameter01 + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>USERID</name>");
            sb.Append(@"    <value>" + UserClass.userData.FSUSER_ID + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>CancelForm</name>");
            sb.Append(@"    <value>1</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"</VariableCollection>");//hid_SendTo

            clientFlow.CallFlow_WorkWithFieldAsync(intProcessID.ToString(), sb.ToString());

            //flowWebService.FlowSoapClient client = new flowWebService.FlowSoapClient();
            //client.WorkWithFieldAsync(intProcessID.ToString(), sb.ToString());
            //client.WorkWithFieldCompleted += (s, args) =>
            //{
            //    if (args.Error == null && args.Result != "")
            //        MessageBox.Show("「" + tbxPROG_NAME.Text.Trim() + "-" + tbxEPISODE.Text.Trim() + "」送帶轉檔置換資料審核完成！", "提示訊息", MessageBoxButton.OK);
            //    else
            //        MessageBox.Show("呼叫流程引擎異常，「" + tbxPROG_NAME.Text.Trim() + "-" + tbxEPISODE.Text.Trim() + "」送帶轉檔置換資料審核失敗！", "提示訊息", MessageBoxButton.OK);

            //    this.DialogResult = true;
            //};
        }

        #endregion
        
    }
}

