﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSBROADCAST;
using PTS_MAM3.WSMAMFunctions;
using System.Text;
using System.Collections.ObjectModel;
using PTS_MAM3.WSSTT;
using System.Windows.Threading;

namespace PTS_MAM3.STT
{
    public partial class STT300_02 : ChildWindow
    {
        WSBROADCASTSoapClient client = new WSBROADCASTSoapClient();                 //產生新的代理類別    
        WSSTTSoapClient clientStt = new WSSTTSoapClient();
        public List<Class_LOG_VIDEO_SEG> ListSeg = new List<Class_LOG_VIDEO_SEG>(); //段落集合
        public Class_LOG_VIDEO m_FormList = new Class_LOG_VIDEO();                  //入庫影像集合
        private DispatcherTimer TimerTransCode;                                     //轉檔Timer

        string m_strFileTypeName ="";   //入庫檔案類型名稱 
        string m_strFileNO = "";        //檔案編號  
        string m_strJobID = "";         //JobID

        public STT300_02(string strFileTypeName, string strFileNO)
        {
            InitializeComponent();

            m_strFileTypeName = strFileTypeName.Trim();           
            m_strFileNO = strFileNO ;
            tbFileNO.Text = m_strFileNO;
            tbFileTypeName.Text = m_strFileTypeName;

            InitializeForm();        //初始化本頁面

            if (m_strFileNO.Trim() != "")  //顯示段落資料
            {
                client.GetTBLOG_VIDEO_SEG_BYFileIDAsync(m_strFileNO);
                client.GetTBLOG_VIDEO_BYFILNOAsync(m_strFileNO);
            }       
        }

        void InitializeForm() //初始化本頁面
        {
            //設定Timer
            SetUpTimer();  

            //透過檔案編號取得段落檔
            client.GetTBLOG_VIDEO_SEG_BYFileIDCompleted += new EventHandler<GetTBLOG_VIDEO_SEG_BYFileIDCompletedEventArgs>(client_GetTBLOG_VIDEO_SEG_BYFileIDCompleted);

            //透過檔案編號取得入庫影像檔
            client.GetTBLOG_VIDEO_BYFILNOCompleted += new EventHandler<GetTBLOG_VIDEO_BYFILNOCompletedEventArgs>(client_GetTBLOG_VIDEO_BYFILNOCompleted);

            //透過JobID取得轉檔進度
            clientStt.GetTranscode_ProgressCompleted += new EventHandler<GetTranscode_ProgressCompletedEventArgs>(clientStt_GetTranscode_ProgressCompleted);
        }

     
        #region 實作      

        //實作-透過檔案編號取得段落檔
        void client_GetTBLOG_VIDEO_SEG_BYFileIDCompleted(object sender, GetTBLOG_VIDEO_SEG_BYFileIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    ListSeg = e.Result;
                    DGSeg.DataContext = ListSeg;
                }
            }
        }

        //實作-透過檔案編號取得入庫影像檔
        void client_GetTBLOG_VIDEO_BYFILNOCompleted(object sender, GetTBLOG_VIDEO_BYFILNOCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    m_FormList = e.Result[0];
                    m_strJobID = m_FormList.FSJOB_ID.Trim();
                    TimerTransCode.Start();    //取得JobID後才可以開始呼叫查詢進度
                }
            }
        }

        //透過JobID取得轉檔進度
        void clientStt_GetTranscode_ProgressCompleted(object sender, GetTranscode_ProgressCompletedEventArgs e)
        {
            TimerTransCode.Start();

            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    string strProgress = e.Result;

                    if (strProgress == "完成")
                    {
                        tbProgress.Text = "完成";
                        TimerTransCode.Stop();
                    }                   
                    else               
                        tbProgress.Text = strProgress;  
                }
            }
        }

        //設定Timer
        private void SetUpTimer()
        {
            TimerTransCode = new DispatcherTimer();
            TimerTransCode.Interval = TimeSpan.FromMilliseconds(3000);
            TimerTransCode.Tick += TimerTransCode_Tick;
        }

        //Timer裡作的事情
        void TimerTransCode_Tick(object sender, EventArgs e)
        {
            TimerTransCode.Stop(); 
            clientStt.GetTranscode_ProgressAsync(m_FormList.FSJOB_ID.Trim());
        }

        #endregion
       
        //按下確定時，若是有資料即上傳
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }      

    }
}

