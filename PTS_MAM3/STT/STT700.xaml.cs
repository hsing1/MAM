﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using PTS_MAM3.STT;
using PTS_MAM3.WSBROADCAST;
using System.Windows.Browser;
using System.Windows.Data;
using PTS_MAM3.PGM;

namespace PTS_MAM3.STT
{
    public partial class STT700 : Page
    {
        WSBROADCASTSoapClient client = new WSBROADCASTSoapClient();             //產生新的代理類別
        List<Class_BROADCAST> m_ListFormData = new List<Class_BROADCAST>();     //記錄查詢到的成案資料

        public STT700()
        {
            InitializeComponent();

            //日期查詢條件初始值
            dpBeg.Text = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-01")).AddMonths(-1).ToShortDateString();
            dpEnd.Text = DateTime.Now.ToShortDateString();

            //取得送帶轉檔檔檔的所有資料_BY狀態、日期
            client.fnGetTBBROADCAST_ALL_BYSTATUSCompleted += new EventHandler<fnGetTBBROADCAST_ALL_BYSTATUSCompletedEventArgs>(client_fnGetTBBROADCAST_ALL_BYSTATUSCompleted);

            Load_TBBROADCAST_ALL_BYSTATUS();
        }
        
        //實作-送帶轉檔檔檔的所有資料_BY狀態、日期
        void client_fnGetTBBROADCAST_ALL_BYSTATUSCompleted(object sender, fnGetTBBROADCAST_ALL_BYSTATUSCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    PagedCollectionView pcv = new PagedCollectionView(e.Result);
                    DGBroListDouble.ItemsSource = pcv;
                    DataPager.Source = pcv;
                }
            }
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息
        }

        
        /// <summary>
        /// 載入送帶轉檔資料_BY狀態(影帶回溯)
        /// 影帶回溯在TBBROADCAST資料表的FCCHECK_STATUS欄位為'T'
        /// </summary>
        private void Load_TBBROADCAST_ALL_BYSTATUS()
        {
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
            client.fnGetTBBROADCAST_ALL_BYSTATUSAsync("T", dpBeg.Text.ToString(), dpEnd.Text.ToString());
        }
       
        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        //新增
        private void btnBroAdd_Click(object sender, RoutedEventArgs e)
        {
            STT700_01 HistoryTape_frm = new STT700_01();
            HistoryTape_frm.Show();

            HistoryTape_frm.Closing += (s, args) =>
            {
                if (HistoryTape_frm.DialogResult == true)
                    Load_TBBROADCAST_ALL_BYSTATUS();        
            };
        }

        //修改
        private void btBroModify_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGBroListDouble.SelectedItem == null)
            {
                MessageBox.Show("請選擇預修改的送帶轉檔資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            if (((Class_BROADCAST)DGBroListDouble.SelectedItem).FCCHECK_STATUS != "N")
            {
                MessageBox.Show("該筆送帶轉檔資料已審核完成，無法修改", "提示訊息", MessageBoxButton.OK);
                return;
            }

            STT100_02 STT100_02_frm = new STT100_02(((Class_BROADCAST)DGBroListDouble.SelectedItem));
            STT100_02_frm.Show();

            STT100_02_frm.Closing += (s, args) =>
            {
                if (STT100_02_frm.DialogResult == true)
                    Load_TBBROADCAST_ALL_BYSTATUS();       
            };
        }      

        //連續點擊-檢視送帶轉檔  
        private void DGBroListDouble_RowDoubleClicked(object sender, DataGridDCTest.DataGridRowClickedArgs e)
        {
            DataGridDCTest.DoubleClickDataGrid dcdg = sender as DataGridDCTest.DoubleClickDataGrid;

            //判斷是否有選取DataGrid
            if (dcdg.SelectedItem == null)
            {
                MessageBox.Show("請選擇預檢視的送帶轉檔資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            STT100_06 STT100_06_frm = new STT100_06(((Class_BROADCAST)DGBroListDouble.SelectedItem).FSBRO_ID.ToString().Trim());
            STT100_06_frm.Show();
        }

        //用條件查詢
        private void btnQuery_Click(object sender, RoutedEventArgs e)
        {
            if (Convert.ToDateTime(dpEnd.Text) < Convert.ToDateTime(dpBeg.Text))
                MessageBox.Show("請檢查查詢條件的起迄日期", "提示訊息", MessageBoxButton.OK);
            else
                Load_TBBROADCAST_ALL_BYSTATUS();//查詢送帶轉檔
        }
        
        //mike要加的按鈕 片庫調帶報表
        private void buttonNeedTape_Click(object sender, RoutedEventArgs e)
        {
            PGM100_09 PGMNeedTape_Frm = new PGM100_09();

            PGMNeedTape_Frm.Show();
            PGMNeedTape_Frm.Closed += (s, args) =>
            {

            };
        }
    }
}
