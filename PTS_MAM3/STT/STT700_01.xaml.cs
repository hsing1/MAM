﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSMAMFunctions;
using PTS_MAM3.ProgData;
using PTS_MAM3.WSBROADCAST;
using PTS_MAM3.PRG;
using System.Text;
using PTS_MAM3.WSTAPE;

namespace PTS_MAM3.STT
{
    public partial class STT700_01 : ChildWindow
    {
        WSBROADCASTSoapClient client = new WSBROADCASTSoapClient();                 //產生新的代理類別        
        WSMAMFunctionsSoapClient clientMAM = new WSMAMFunctionsSoapClient();
        WSTAPESoapClient clientTAPE = new WSTAPESoapClient();
        List<WSTAPE.Class_LOG_VIDEO> m_ListLogVideo = new List<WSTAPE.Class_LOG_VIDEO>();           //入庫影像檔集合
        List<WSTAPE.Class_TAPE_LABEL_CLIP> m_ListClip = new List<WSTAPE.Class_TAPE_LABEL_CLIP>();   //段落資訊(影帶)
        List<WSBROADCAST.Class_LOG_VIDEO> check_ListLogVideo = new List<WSBROADCAST.Class_LOG_VIDEO>();
        List<Class_TAPE_LABEL_TAPE> m_ListLABEL_TAPE = new List<Class_TAPE_LABEL_TAPE>();           //影帶館藏集合
        List<string> m_ListCheck = new List<string>();                                              //記錄需要的項目
        List<string> m_ListTTCheck = new List<string>();                                            //記錄需要轉為播出帶的項目
        List<Class_CODE_TBFILE_TYPE> m_ListCodeData = new List<Class_CODE_TBFILE_TYPE>();           //入庫檔案代碼集合

        string m_strType = "";          //類型
        string m_strID = "";            //編碼
        string m_strEpisode = "";       //集別
        string m_strCHANNELID = "";     //頻道別 
        string m_strDep_ID = "";        //成案單位
        string m_strBroID = "";         //送帶轉檔單單號
        string m_strFileNONew = "";     //檔案編號(新增時用的)  
        string m_DIRID = "";            //串結點用的
        string m_ProgName = "";         //串結點用的_節目名稱
        string m_EpisodeName = "";      //串結點用的_子集名稱

        bool bolCheck = false;          //判斷是否CheckBox的LoadingLoad
        string m_strDuration = "";      //影帶長度
        Boolean m_checkSearch = false;  //避免找不到資料，當第一次找不到時再找第二次
        string m_strAudioTrack = "";
        public STT700_01()
        {
            InitializeComponent();
            InitializeForm();          //初始化本頁面         
        }

        void InitializeForm() //初始化本頁面
        {
            //取得送帶轉檔單編號
            clientMAM.GetNoRecordCompleted += new EventHandler<GetNoRecordCompletedEventArgs>(ClientMAM_GetNoRecordCompleted);
            clientMAM.GetNoRecordAsync("07", "", "", UserClass.userData.FSUSER_ID);

            //取得檔案編號
            client.fnGetFileIDCompleted += new EventHandler<fnGetFileIDCompletedEventArgs>(client_fnGetFileIDCompleted);

            //新增送帶轉檔單資料
            client.INSERT_BROADCASTCompleted += new EventHandler<INSERT_BROADCASTCompletedEventArgs>(client_INSERT_BROADCASTCompleted);

            //刪除入庫影像檔及段落檔
            client.DELETE_TBLOG_VIDEO_SEG_BYFILENOCompleted += new EventHandler<DELETE_TBLOG_VIDEO_SEG_BYFILENOCompletedEventArgs>(client_DELETE_TBLOG_VIDEO_SEG_BYFILENOCompleted);

            //查詢影帶管理系統的段落檔
            clientTAPE.GetTAPE_CLIP_BYPROGID_EPISODECompleted += new EventHandler<GetTAPE_CLIP_BYPROGID_EPISODECompletedEventArgs>(clientTAPE_GetTAPE_CLIP_BYPROGID_EPISODECompleted);

            //查詢影帶管理系統的館藏檔
            clientTAPE.GetTAPE_LABEL_BYPROGID_EPISODECompleted += new EventHandler<GetTAPE_LABEL_BYPROGID_EPISODECompletedEventArgs>(clientTAPE_GetTAPE_LABEL_BYPROGID_EPISODECompleted);

            //新增影帶的節目段落檔
            clientTAPE.INSERT_TBLOG_VIDEO_TAPECompleted += new EventHandler<INSERT_TBLOG_VIDEO_TAPECompletedEventArgs>(clientTAPE_INSERT_TBLOG_VIDEO_TAPECompleted);

            //下載代碼檔_影片的檔案類型  
            client.GetTBBROADCAST_CODE_TBFILE_TYPECompleted += new EventHandler<GetTBBROADCAST_CODE_TBFILE_TYPECompletedEventArgs>(client_GetTBBROADCAST_CODE_TBFILE_TYPECompleted);
            client.GetTBBROADCAST_CODE_TBFILE_TYPEAsync();

            //查詢入庫影像檔(避免一筆以上的SD播出帶或是HD播出帶)
            clientTAPE.QUERY_TBLOG_VIDEO_BYProgID_Episode_LIST_CHECKCompleted += new EventHandler<WSTAPE.QUERY_TBLOG_VIDEO_BYProgID_Episode_LIST_CHECKCompletedEventArgs>(clientTAPE_QUERY_TBLOG_VIDEO_BYProgID_Episode_LIST_CHECKCompleted);

            //影帶回溯，每一種類型都只能有一筆，用意是提醒片庫人員，避免重複入庫
            clientTAPE.QUERY_TBLOG_VIDEO_BYProgID_Episode_LIST_CHECK_ALLCompleted += new EventHandler<WSTAPE.QUERY_TBLOG_VIDEO_BYProgID_Episode_LIST_CHECK_ALLCompletedEventArgs>(clientTAPE_QUERY_TBLOG_VIDEO_BYProgID_Episode_LIST_CHECK_ALLCompleted);

            //查看是否可以新增影帶回溯單(邏輯與新增送帶轉檔單同)
            client.Query_CheckIsAbleToCreateNewTransformFormCompleted += new EventHandler<Query_CheckIsAbleToCreateNewTransformFormCompletedEventArgs>(client_Query_CheckIsAbleToCreateNewTransformFormCompleted);
        }



        #region 實作

        //實作-新增影帶的節目段落檔
        void clientTAPE_INSERT_TBLOG_VIDEO_TAPECompleted(object sender, INSERT_TBLOG_VIDEO_TAPECompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息
            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    //// MessageBox.Show("新增影帶回溯資料成功！", "提示訊息", MessageBoxButton.OK);
                    //client.INSERT_BROADCASTAsync(FormToClass_Bro());
                    //BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                    MessageBox.Show("新增影帶回溯資料成功！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("新增影帶回溯資料失敗！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
            else
            {
                MessageBox.Show("新增影帶回溯資料失敗！", "提示訊息", MessageBoxButton.OK);
                this.DialogResult = false;
            }
        }

        //實作-查詢影帶管理系統的段落檔
        void clientTAPE_GetTAPE_CLIP_BYPROGID_EPISODECompleted(object sender, GetTAPE_CLIP_BYPROGID_EPISODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    string strBIB = "";     //避免同一個節目對到多個BIB，因此要只取一個BIB的段落擋，其他要檔掉                   

                    if (e.Result.Count > 0)
                    {
                        strBIB = e.Result[0].FSBIB;

                        for (int i = 0; i < e.Result.Count; i++)
                        {
                            if (e.Result[i].FSBIB == strBIB)
                            {
                                //影帶回溯帶入段落資訊時，也要入Frame數及段落區間
                                e.Result[i].FSDURATION = TransferTimecode.Subtract_timecode(e.Result[i].FSTCSTART, e.Result[i].FSTCEND);
                                e.Result[i].FNBEG_TIMECODE_FRAME = TransferTimecode.timecodetoframe(e.Result[i].FSTCSTART);
                                e.Result[i].FNEND_TIMECODE_FRAME = TransferTimecode.timecodetoframe(e.Result[i].FSTCEND); ;
                                m_ListClip.Add(e.Result[i]);
                            }

                        }
                    }

                    DGTapeClip.ItemsSource = m_ListClip;
                }
            }

            clientTAPE.GetTAPE_LABEL_BYPROGID_EPISODEAsync(m_strID, m_strEpisode);  //查詢影帶管理系統的館藏檔
        }

        //實作-查詢影帶管理系統的館藏檔
        void clientTAPE_GetTAPE_LABEL_BYPROGID_EPISODECompleted(object sender, GetTAPE_LABEL_BYPROGID_EPISODECompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    List<Class_TAPE_LABEL_TAPE> ListLabel = new List<Class_TAPE_LABEL_TAPE>();

                    ListLabel = e.Result;

                    m_ListLABEL_TAPE = ListLabel;
                    DGTapeLabel.ItemsSource = ListLabel;

                    if (ListLabel.Count > 0)
                    {
                        tbxTitle.Text = ""; //ListLabel[0].FSTAPE_TITLE.Trim()  //影帶標題改成不預帶至影帶管理系統
                        tbxDescription.Text = ListLabel[0].FSTAPE_DES.Trim();
                        tbxDuration.Text = ListLabel[0].FSDURATION.Trim();
                        m_strDuration = ListLabel[0].FSDURATION.Trim();

                        if (m_ListClip.Count == 0)  //檢查影帶段落，若是沒有要從影帶長度找出一段
                            CheckClipDuration();
                    }
                    else
                    {
                        tbxTitle.Text = "";
                        tbxDescription.Text = "";
                        tbxDuration.Text = "";
                    }
                }
            }

            //如果館藏及段落都找不到就再查詢一次
            if (m_ListLABEL_TAPE.Count == 0 || m_ListClip.Count == 0)
            {
                if (m_checkSearch == false)
                {
                    m_checkSearch = true;
                    Query_Tape();
                }
            }
        }

        //實作-取得檔案編號
        void client_fnGetFileIDCompleted(object sender, fnGetFileIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    string strGet = e.Result;  //接收回來的參數，前者是檔案編號，後者是DIRID                  

                    char[] delimiterChars = { ';' };
                    string[] words = strGet.Split(delimiterChars);

                    if (words.Length == 2)
                    {
                        m_strFileNONew = words[0];
                        m_DIRID = words[1];
                    }
                    else if (words.Length == 1)
                    {
                        m_strFileNONew = words[0];
                        m_DIRID = "";
                    }
                    else
                    {
                        m_strFileNONew = "";
                        m_DIRID = "";
                    }

                    //client.INSERT_TBLOG_VIDEO_SEGAsync(ListSeg);  //新增段落
                }
            }
            else
            {
                MessageBox.Show("取得檔案編號異常，請聯絡系統管理員！", "提示訊息", MessageBoxButton.OK);
                this.DialogResult = false;
            }
        }

        //實作-取得送帶轉檔單編號
        void ClientMAM_GetNoRecordCompleted(object sender, GetNoRecordCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    tbxBro_ID.Text = e.Result;
                    m_strBroID = e.Result;
                }
                else
                {
                    MessageBox.Show("取得送帶轉檔單編號異常，請聯絡系統管理員！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
            else
            {
                MessageBox.Show("取得送帶轉檔單編號異常，請聯絡系統管理員！", "提示訊息", MessageBoxButton.OK);
                this.DialogResult = false;
            }
        }

        //實作-新增送帶轉檔單資料
        void client_INSERT_BROADCASTCompleted(object sender, INSERT_BROADCASTCompletedEventArgs e)
        {
            //BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    clientTAPE.INSERT_TBLOG_VIDEO_TAPEAsync(m_ListLogVideo, m_ListClip, m_strType, m_strID, m_strEpisode, UserClass.userData.FSUSER_ID.ToString(), m_strCHANNELID, m_ProgName, m_EpisodeName, m_strAudioTrack);
                    //MessageBox.Show("新增影帶回溯資料成功！！", "提示訊息", MessageBoxButton.OK);
                    //this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("新增影帶回溯資料失敗！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
            else
            {
                MessageBox.Show("新增影帶回溯資料失敗！", "提示訊息", MessageBoxButton.OK);
                this.DialogResult = false;
            }
        }

        //實作-刪除入庫影像檔及段落檔
        void client_DELETE_TBLOG_VIDEO_SEG_BYFILENOCompleted(object sender, DELETE_TBLOG_VIDEO_SEG_BYFILENOCompletedEventArgs e)
        {
            //此作業是取消送帶轉檔時觸發，因此不需要顯示訊息
        }

        //實作-取得代碼檔_影片的檔案類型 
        void client_GetTBBROADCAST_CODE_TBFILE_TYPECompleted(object sender, GetTBBROADCAST_CODE_TBFILE_TYPECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    m_ListCodeData = e.Result;
                }
            }
        }

        //實作-查詢入庫影像檔(避免一筆以上的SD播出帶或是HD播出帶)
        void clientTAPE_QUERY_TBLOG_VIDEO_BYProgID_Episode_LIST_CHECKCompleted(object sender, WSTAPE.QUERY_TBLOG_VIDEO_BYProgID_Episode_LIST_CHECKCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    MessageBox.Show(e.Result, "提示訊息", MessageBoxButton.OK);
                    OKButton.IsEnabled = true;
                    return;
                }
                else  //進行影帶回溯
                    clientTAPE.INSERT_TBLOG_VIDEO_TAPEAsync(m_ListLogVideo, m_ListClip, m_strType, m_strID, m_strEpisode, UserClass.userData.FSUSER_ID.ToString(), m_strCHANNELID, m_ProgName, m_EpisodeName, m_strAudioTrack);
            }
            else
            {
                MessageBox.Show("新增影帶回溯資料失敗！請查詢影帶管理系統的資料", "提示訊息", MessageBoxButton.OK);
                this.DialogResult = false;
            }
        }

        //實作-影帶回溯，每一種類型都只能有一筆，用意是提醒片庫人員，避免重複入庫
        void clientTAPE_QUERY_TBLOG_VIDEO_BYProgID_Episode_LIST_CHECK_ALLCompleted(object sender, WSTAPE.QUERY_TBLOG_VIDEO_BYProgID_Episode_LIST_CHECK_ALLCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    MessageBox.Show(e.Result, "提示訊息", MessageBoxButton.OK);
                    OKButton.IsEnabled = true;
                    this.BusyMsg.IsBusy = false;
                    return;
                }
                else  //進行影帶回溯
                {
                    client.INSERT_BROADCASTAsync(FormToClass_Bro());
                }
                //clientTAPE.INSERT_TBLOG_VIDEO_TAPEAsync(m_ListLogVideo, m_ListClip, m_strType, m_strID, m_strEpisode, UserClass.userData.FSUSER_ID.ToString(), m_strCHANNELID, m_ProgName, m_EpisodeName, m_strAudioTrack);
            }
            else
            {
                MessageBox.Show("新增影帶回溯資料失敗！請查詢影帶管理系統的資料", "提示訊息", MessageBoxButton.OK);
                this.DialogResult = false;
            }
        }

        /// <summary>
        /// 實做查看是否可以新增影帶回溯單(邏輯與新增送帶轉檔單同)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">string:"True" 可新增</param>
        void client_Query_CheckIsAbleToCreateNewTransformFormCompleted(object sender, Query_CheckIsAbleToCreateNewTransformFormCompletedEventArgs e)
        {
            if (e.Result == "True")
            {
                //先檢查SD播出帶或是HD播出帶是否已存在，影帶回溯測試時，片庫決議全部的檔案類型都只有上傳一筆
                //clientTAPE.QUERY_TBLOG_VIDEO_BYProgID_Episode_LIST_CHECKAsync(m_ListLogVideo);
                clientTAPE.QUERY_TBLOG_VIDEO_BYProgID_Episode_LIST_CHECK_ALLAsync(m_ListLogVideo);
            }
            else
            {
                MessageBox.Show(e.Result.ToString());
                BusyMsg.IsBusy = false;
            }
        }
        #endregion

        #region 檢查畫面資料及搬值

        private Class_BROADCAST FormToClass_Bro()
        {
            Class_BROADCAST obj = new Class_BROADCAST();

            obj.FSBRO_ID = m_strBroID.Trim();                                      //送帶轉檔單號
            obj.FSBRO_TYPE = m_strType.Trim();                                      //類型
            obj.FSID = m_strID.Trim();                                              //編碼
            obj.FNEPISODE = Convert.ToInt16(m_strEpisode);     //集別            
            obj.FDBRO_DATE = DateTime.Now;                                          //送帶轉檔申請日期
            obj.FSBRO_BY = UserClass.userData.FSUSER_ID.ToString();                 //送帶轉檔申請者
            obj.FCCHECK_STATUS = "T";                                                //審核狀態(影帶回溯)
            obj.FSCHECK_BY = "";                                                    //審核者
            obj.FCCHANGE = "N";                                                     //置換

            obj.FSCREATED_BY = UserClass.userData.FSUSER_ID.ToString();//建檔者
            obj.FSUPDATED_BY = UserClass.userData.FSUSER_ID.ToString();//修改者                     

            return obj;
        }

        //新增入庫影像資料
        private WSTAPE.Class_LOG_VIDEO TAPE_LABEL_TAPE_ToClass_LogVideo(Class_TAPE_LABEL_TAPE Tapeobj)
        {
            WSTAPE.Class_LOG_VIDEO obj = new WSTAPE.Class_LOG_VIDEO();
            CheckTimeCode();            //檢查段落將最後一個的「:」變成「;」

            obj.FSFILE_NO = "";         //在WS裡給;
            obj.FSSUBJECT_ID = "";      //在WS裡給;
            obj.FSTYPE = "G";           //一律為節目
            obj.FSID = m_strID;
            obj.FNEPISODE = Convert.ToInt16(m_strEpisode);
            obj.FSPLAY = "N";

            //檢查是否要變成SD播出帶或是HD播出帶
            obj.FSARC_TYPE = checkSTT_ARC_TYPE(Tapeobj);

            obj.FSFILE_TYPE_HV = "mxf";     //高解檔名
            if (Tapeobj.FSFILETYPE_NAME.Contains("SD"))
                obj.FSFILE_TYPE_LV = "wmv";
            else if (Tapeobj.FSFILETYPE_NAME.Contains("HD"))
                obj.FSFILE_TYPE_LV = "mp4";

            //obj.FSFILE_TYPE_LV = "wmv";     //低解檔名
            obj.FSTITLE = tbxTitle.Text.Trim();             //Tapeobj.FSTAPE_TITLE.Trim();      //影帶標題
            obj.FSDESCRIPTION = tbxDescription.Text.Trim(); //Tapeobj.FSTAPE_DES.Trim();        //影帶描述
            obj.FSFILE_SIZE = "";
            obj.FCFILE_STATUS = "B";        //使用者送轉檔申請後：B

            obj.FSCHANGE_FILE_NO = "";
            obj.FSOLD_FILE_NAME = "";
            obj.FSFILE_PATH_H = "";         //高低解檔案路徑在web services裡取得
            obj.FSFILE_PATH_L = "";

            obj.FSSUPERVISOR = "Y";         //調用通知入庫單位主管，回溯時，給原成案單位審核 Default設定為 Y          

            if (m_DIRID != "")
                obj.FNDIR_ID = Convert.ToUInt32(m_DIRID);
            else
                obj.FNDIR_ID = 0;

            if (m_ListClip.Count > 0)
            {
                obj.FSBEG_TIMECODE = m_ListClip[0].FSTCSTART.ToString().Trim();
                obj.FSEND_TIMECODE = m_ListClip[m_ListClip.Count - 1].FSTCEND.ToString().Trim();
            }
            else
            {
                obj.FSBEG_TIMECODE = "";
                obj.FSEND_TIMECODE = "";
            }

            obj.FSBRO_ID = m_strBroID;
            obj.FSARC_ID = "";
            obj.FSTAPE_ID = Tapeobj.FSTAPEID.Trim();                              //影帶編號

            obj.FSCHANNEL_ID = m_strCHANNELID;                                    //頻道別(要用該節目隸屬的頻道別)
            obj.FSCREATED_BY = UserClass.userData.FSUSER_ID.ToString();           //建檔者
            obj.FSUPDATED_BY = UserClass.userData.FSUSER_ID.ToString();           //修改者

            return obj;
        }

        //檢查是否要轉換為播出帶
        private string checkSTT_ARC_TYPE(Class_TAPE_LABEL_TAPE Tapeobj)
        {
            string strReturn = Tapeobj.FSFILETYPE.Trim();

            if (strReturn == "001" || strReturn == "002")   //若是為SD播出帶或是HD播出帶，就不用檢查了
                return strReturn;

            for (int i = 0; i < m_ListTTCheck.Count; i++)
            {
                if (m_ListTTCheck[i].Trim() == Tapeobj.FSTAPEID.Trim())
                {
                    if (Tapeobj.FSTTAPECODE_NAME.IndexOf("HD") > -1)
                        return "002";   //轉成HD播出帶
                    else
                        return "001";   //轉成SD播出帶
                }
            }

            return strReturn;
        }

        #endregion

        #region 重要的function

        //檢查SD播出帶或是HD播出帶只能上傳一次
        private Boolean CheckSDHD()
        {
            List<string> checkList = new List<string>();
            Int16 intCount = 0;

            //先將選擇到的檔案類型放到集合裡，再去比對，如果有重複點選的就要提醒
            for (int i = 0; i < m_ListLogVideo.Count; i++)
            {
                checkList.Add(m_ListLogVideo[i].FSARC_TYPE.Trim());
            }

            for (int i = 0; i < m_ListLogVideo.Count; i++)
            {
                foreach (string checkobj in checkList)   //如果在被勾選的List裡，就要把它勾選起來
                {
                    if (checkobj == m_ListLogVideo[i].FSARC_TYPE.Trim())
                    {
                        if (intCount > 0)
                        {
                            MessageBox.Show("「" + m_ListLogVideo[i].FSARC_TYPE_NAME.Trim() + "」只能上傳一次，請重新選擇影片類型！", "提示訊息", MessageBoxButton.OK);
                            return false;
                        }

                        intCount++;
                    }
                }

                intCount = 0;
            }

            return true;
        }

        #region 原本的判斷SD播出帶及HD播出帶只能上傳一筆，影帶回溯測試時，片庫決議全部的檔案類型都只有上傳一筆

        //private Boolean CheckSDHD()
        //{            
        //    Int16 intCountSD = 0;
        //    Int16 intCountHD = 0;
        //    string strSD = "";
        //    string strHD = "";

        //    checkListLogVideoName();    //補上入庫檔案的類型名稱

        //    for (int i = 0; i < m_ListLogVideo.Count; i++)
        //    {
        //        if (m_ListLogVideo[i].FSARC_TYPE.Trim() == "001")
        //        {
        //            if (m_ListLogVideo[i].FSARC_TYPE_NAME.Trim() != "")
        //                strSD = m_ListLogVideo[i].FSARC_TYPE_NAME.Trim();
        //            else
        //                strSD = "SD播出帶";

        //            intCountSD++;
        //        }
        //        else if (m_ListLogVideo[i].FSARC_TYPE.Trim() == "002")
        //        {
        //            if (m_ListLogVideo[i].FSARC_TYPE_NAME.Trim() != "")
        //                strHD = m_ListLogVideo[i].FSARC_TYPE_NAME.Trim();
        //            else
        //                strHD = "HD播出帶";

        //            intCountHD++;
        //        }
        //    }

        //    if (intCountSD > 1)
        //    {
        //        MessageBox.Show(strSD + "只能上傳一次，請重新選擇影片類型！", "提示訊息", MessageBoxButton.OK);
        //        return false;
        //    }
        //    else if (intCountHD > 1)
        //    {
        //        MessageBox.Show(strHD + "只能上傳一次，請重新選擇影片類型！", "提示訊息", MessageBoxButton.OK);
        //        return false;
        //    }

        //    return true;
        //}

        #endregion

        //檢查要進行影帶回溯的入庫影像檔集合，補上檔案名稱，以便檢查是否有重複的檔案時，可以顯示檔案名稱
        private void checkListLogVideoName()
        {
            for (int i = 0; i < m_ListLogVideo.Count; i++)
            {
                m_ListLogVideo[i].FSARC_TYPE_NAME = checkFileTypeName(m_ListLogVideo[i].FSARC_TYPE);
            }
        }

        //查詢入庫檔案名稱
        private string checkFileTypeName(string strID)
        {
            for (int i = 0; i < m_ListCodeData.Count; i++)
            {
                if (m_ListCodeData[i].ID.Trim() == strID.Trim())
                    return m_ListCodeData[i].NAME.Trim();
            }

            return "";
        }

        //檢查段落將最後一個的「:」變成「;」
        private void CheckTimeCode()
        {
            for (int i = 0; i < m_ListClip.Count; i++)
            {
                if (m_ListClip[i].FSTCSTART.Length == 11 && m_ListClip[i].FSTCSTART.Trim().Substring(8, 1) == ":")
                {
                    m_ListClip[i].FSTCSTART = m_ListClip[i].FSTCSTART.Trim().Substring(0, 8) + ";" + m_ListClip[i].FSTCSTART.Trim().Substring(9, 2);
                }

                if (m_ListClip[i].FSTCEND.Length == 11 && m_ListClip[i].FSTCEND.Trim().Substring(8, 1) == ":")
                {
                    m_ListClip[i].FSTCEND = m_ListClip[i].FSTCEND.Trim().Substring(0, 8) + ";" + m_ListClip[i].FSTCEND.Trim().Substring(9, 2);
                }
            }
        }

        //將影帶館藏資訊轉為入庫影像檔
        private void ListTapeToListLog()
        {
            m_ListLogVideo.Clear(); //入庫影像檔集合,轉入前先清空

            for (int i = 0; i < m_ListLABEL_TAPE.Count; i++)
            {
                for (int j = 0; j < m_ListCheck.Count; j++)
                {
                    //若是無對應的轉檔類型，就算選取要進行影帶回溯，也要略過
                    if (m_ListLABEL_TAPE[i].FSTAPEID == m_ListCheck[j] && m_ListLABEL_TAPE[i].FSFILETYPE.Trim() != "")
                        m_ListLogVideo.Add(TAPE_LABEL_TAPE_ToClass_LogVideo(m_ListLABEL_TAPE[i]));
                }
            }

            checkListLogVideoName();    //補上入庫檔案的類型名稱
        }

        //若是段落擋沒有，且有長度，就要自動帶一筆段落資料
        private void CheckClipDuration()
        {
            int intHour = 0;
            int intMin = 0;
            int intSec = 0;
            int intCountSec = 0;   //秒數要取幾位
            string strEndTimeCode = "";
            string strDuration = "";

            if (m_ListClip.Count == 0 && m_strDuration.Trim() != "")
            {
                if (m_strDuration.IndexOf("'") > -1 && m_strDuration.IndexOf("\"") > -1)
                {
                    intCountSec = m_strDuration.IndexOf("\"") - m_strDuration.IndexOf("'") - 1;
                    if (int.TryParse(m_strDuration.Substring(0, m_strDuration.IndexOf("'")), out intMin) == true && int.TryParse(m_strDuration.Substring(m_strDuration.IndexOf("'") + 1, intCountSec), out intSec) == true)
                    {
                        if (intMin >= 60)
                        {
                            intHour = intMin / 60;
                            intMin = intMin % 60;
                            strDuration = intHour.ToString().PadLeft(2, '0') + ":" + intMin.ToString().PadLeft(2, '0') + ":" + intSec.ToString().PadLeft(2, '0') + ";00";
                            intHour = intHour + 1;  //因為時從01算起，所以要加1
                            strEndTimeCode = intHour.ToString().PadLeft(2, '0') + ":" + intMin.ToString().PadLeft(2, '0') + ":" + intSec.ToString().PadLeft(2, '0') + ";00";
                        }
                        else
                        {
                            strEndTimeCode = "01:" + intMin.ToString().PadLeft(2, '0') + ":" + intSec.ToString().PadLeft(2, '0') + ";00";
                            strDuration = "00:" + intMin.ToString().PadLeft(2, '0') + ":" + intSec.ToString().PadLeft(2, '0') + ";00";
                        }

                        Class_TAPE_LABEL_CLIP objClip = new Class_TAPE_LABEL_CLIP();
                        objClip.FSBIB = "";
                        objClip.FSTCSTART = "01:00:00;00";
                        objClip.FSTCEND = strEndTimeCode;
                        objClip.FSDURATION = strDuration;
                        objClip.FNBEG_TIMECODE_FRAME = TransferTimecode.timecodetoframe("01:00:00;00");
                        objClip.FNEND_TIMECODE_FRAME = TransferTimecode.timecodetoframe(strEndTimeCode);
                        objClip.FSCLIPTITLE = "";
                        m_ListClip.Add(objClip);
                        DGTapeClip.ItemsSource = null;
                        DGTapeClip.ItemsSource = m_ListClip;
                    }
                }
            }
        }

        #endregion

        #region 按鈕裡的事件

        bool CheckFuntion()
        {
            if (m_ListCheck.Count == 0)
            {
                MessageBox.Show("至少要有一筆影片館藏資料", "提示訊息", MessageBoxButton.OK);
                return false;
            }
            else if (m_ListClip.Count == 0)
            {
                MessageBox.Show("至少要有一筆影片段落資料", "提示訊息", MessageBoxButton.OK);
                return false;
            }
            else if (this.AudioTrackSetControl.GetAudioTrackSetting() == "")
            {
                MessageBox.Show("音軌設定為必填!");
                return false;
            }
            else if (CheckSeg_DropFrame() == false)//檢查影片段落是否符合DropFrame格式
                return false;

            ListTapeToListLog();        //將影帶館藏資訊轉為入庫影像檔

            if (CheckSDHD() == false)   //檢查SD播出帶或是HD播出帶只能上傳一次
                return false;

            return true;
        }

        //按下確定後
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.OKButton.IsEnabled = false;
            this.BusyMsg.IsBusy = true;
            if (CheckFuntion())
            {
                //因為CLASS的參考不同所以只好這樣轉
                foreach (var item in m_ListLogVideo)
                {
                    WSBROADCAST.Class_LOG_VIDEO myClass = new WSBROADCAST.Class_LOG_VIDEO();
                    myClass.FSARC_TYPE = item.FSARC_TYPE;
                    m_strAudioTrack = this.AudioTrackSetControl.GetAudioTrackSetting();
                    myClass.FSTRACK = m_strAudioTrack;
                    check_ListLogVideo.Add(myClass);
                }
                //查看是否可以新增單據
                client.Query_CheckIsAbleToCreateNewTransformFormAsync(m_strID, m_strEpisode, check_ListLogVideo);
                //OKButton.IsEnabled = false;
            }
            else
            {
                this.BusyMsg.IsBusy = false;
                this.OKButton.IsEnabled = true;
            }
        }

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //開啟節目資料查詢畫面
        private void btnPROG_Click(object sender, RoutedEventArgs e)
        {
            PROGDATA_VIEW PROGDATA_VIEW_frm = new PROGDATA_VIEW();
            PROGDATA_VIEW_frm.Show();

            PROGDATA_VIEW_frm.Closed += (s, args) =>
            {
                if (PROGDATA_VIEW_frm.DialogResult == true)
                {
                    //檢查是否要作資料認定
                    if (PROGDATA_VIEW_frm.strChannel_Id.Trim() == "" || PROGDATA_VIEW_frm.strDEP_ID.Trim() == "0")
                    {
                        checkProgM_Channel_Dep(PROGDATA_VIEW_frm.strProgID_View.Trim(), PROGDATA_VIEW_frm.strProgName_View.Trim(), PROGDATA_VIEW_frm.strChannel_Id.Trim(), PROGDATA_VIEW_frm.strDEP_ID.Trim());
                        return;
                    }

                    //載入節目資料
                    checkProg(PROGDATA_VIEW_frm.strProgID_View.Trim(), PROGDATA_VIEW_frm.strProgName_View.Trim(), PROGDATA_VIEW_frm.strChannel_Id.Trim(), PROGDATA_VIEW_frm.strDEP_ID.Trim());
                }
            };
        }

        //開啟節目子集資料查詢畫面
        private void btnEPISODE_Click(object sender, RoutedEventArgs e)
        {
            if (tbxPROG_NAME.Text.ToString().Trim() != "" && tbxPROG_NAME.Tag.ToString().Trim() != "")
            {
                PRG.PRG200_07 PRG200_07_frm = new PRG.PRG200_07(tbxPROG_NAME.Tag.ToString().Trim(), tbxPROG_NAME.Text.ToString().Trim());
                PRG200_07_frm.Show();

                PRG200_07_frm.Closed += (s, args) =>
                {
                    if (PRG200_07_frm.DialogResult == true)
                    {
                        m_strEpisode = PRG200_07_frm.m_strEpisode_View;
                        tbxEPISODE.Text = PRG200_07_frm.m_strEpisode_View;
                        lblEPISODE_NAME.Content = PRG200_07_frm.m_strProgDName_View;
                        m_EpisodeName = PRG200_07_frm.m_strProgDName_View;
                        m_strType = "G";    //影帶回溯只針對節目
                        DGTapeClip.ItemsSource = null;
                        DGTapeLabel.ItemsSource = null;
                        tbxTitle.Text = "";
                        tbxDescription.Text = "";
                        tbxDuration.Text = "";

                        m_checkSearch = false; //查詢不到館藏或段落都會再查一次
                        Query_Tape();
                    }
                };
            }
            else
                MessageBox.Show("請先選擇節目！", "提示訊息", MessageBoxButton.OK);
        }

        //查詢影帶段落資訊
        private void btnAddClip_Click(object sender, RoutedEventArgs e)
        {
            if (m_strID.Trim() == "")
            {
                MessageBox.Show("請先選擇節目！", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (m_strEpisode.Trim() == "0")
            {
                MessageBox.Show("請先選擇集別！", "提示訊息", MessageBoxButton.OK);
                return;
            }

            m_checkSearch = false; //查詢不到館藏或段落都會再查一次
            Query_Tape();
        }

        //查詢影帶段落
        private void Query_Tape()
        {
            DGTapeClip.ItemsSource = null;
            DGTapeLabel.ItemsSource = null;
            m_ListClip.Clear();
            m_ListCheck.Clear();
            m_ListLABEL_TAPE.Clear();
            m_strDuration = "";

            clientTAPE.GetTAPE_CLIP_BYPROGID_EPISODEAsync(m_strID, m_strEpisode);   //查詢影帶管理系統的段落檔，等到資料回來後，再去查館藏資料            

            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }

        //檢查影片段落是否符合DropFrame格式
        private Boolean CheckSeg_DropFrame()
        {
            List<string> ListMsg = new List<string>();
            Boolean bolAlter = false;   //判斷段落是否有自動修改
            string strStartMsg = "";
            string strEndMsg = "";
            string strHour = "";
            string strMinute = "";
            string strSecond = "";
            string strFrame = "";
            string strHourE = "";
            string strMinuteE = "";
            string strSecondE = "";
            string strFrameE = "";

            for (int i = 0; i < m_ListClip.Count; i++)
            {
                strStartMsg = "";
                strEndMsg = "";

                strHour = m_ListClip[i].FSTCSTART.Substring(0, 2);
                strMinute = m_ListClip[i].FSTCSTART.Substring(3, 2);
                strSecond = m_ListClip[i].FSTCSTART.Substring(6, 2);
                strFrame = m_ListClip[i].FSTCSTART.Substring(9, 2);

                strHourE = m_ListClip[i].FSTCEND.Substring(0, 2);
                strMinuteE = m_ListClip[i].FSTCEND.Substring(3, 2);
                strSecondE = m_ListClip[i].FSTCEND.Substring(6, 2);
                strFrameE = m_ListClip[i].FSTCEND.Substring(9, 2);

                if (m_ListClip[i].FSCLIPTITLE.Trim() == "")
                {
                    if (TransferTimecode.check_timecode_format(strHour, strMinute, strSecond, strFrame, out strStartMsg) == false)
                        ListMsg.Add("第" + (i + 1) + "段，開始段落:" + m_ListClip[i].FSTCSTART + "錯誤，原因:" + strStartMsg);

                    if (TransferTimecode.check_timecode_format(strHourE, strMinuteE, strSecondE, strFrameE, out strEndMsg) == false)
                        ListMsg.Add("第" + (i + 1) + "段，結束段落:" + m_ListClip[i].FSTCEND + "錯誤，原因:" + strEndMsg);
                }
                else
                {
                    if (TransferTimecode.check_timecode_format(strHour, strMinute, strSecond, strFrame, out strStartMsg) == false)
                        ListMsg.Add("單元標題:" + m_ListClip[i].FSCLIPTITLE.Trim() + "，開始段落:" + m_ListClip[i].FSTCSTART + "錯誤，原因:" + strStartMsg);

                    if (TransferTimecode.check_timecode_format(strHourE, strMinuteE, strSecondE, strFrameE, out strEndMsg) == false)
                        ListMsg.Add("單元標題:" + m_ListClip[i].FSCLIPTITLE.Trim() + "，結束段落:" + m_ListClip[i].FSTCEND + "錯誤，原因:" + strEndMsg);
                }
            }

            if (ListMsg.Count == 0)
                return true;
            else
            {
                STT700_02 STT700_02_frm = new STT700_02(ListMsg);  //檢查段落資料提示訊息
                STT700_02_frm.Show();

                STT700_02_frm.Closing += (s, args) =>
                {
                    if (STT700_02_frm.DialogResult == true)
                    {
                        AlterSeg_DropFrame();   //自動修改段落資料
                        bolAlter = true;
                    }
                };

                //if (bolAlter == true) //自動修改段落完，一樣回到影帶回溯畫面
                //    return true;
                //else
                return false;
            }
        }

        //自動修改段落資料
        private void AlterSeg_DropFrame()
        {
            List<WSTAPE.Class_TAPE_LABEL_CLIP> AlterListClip = new List<WSTAPE.Class_TAPE_LABEL_CLIP>();   //修改過的影帶段落資訊      
            string strStartMsg = "";
            string strEndMsg = "";
            string strHour = "";
            string strMinute = "";
            string strSecond = "";
            string strFrame = "";
            string strHourE = "";
            string strMinuteE = "";
            string strSecondE = "";
            string strFrameE = "";

            if (m_ListClip.Count == 0)
                return;

            for (int i = 0; i < m_ListClip.Count; i++)
            {
                strStartMsg = "";
                strEndMsg = "";

                strHour = m_ListClip[i].FSTCSTART.Substring(0, 2);
                strMinute = m_ListClip[i].FSTCSTART.Substring(3, 2);
                strSecond = m_ListClip[i].FSTCSTART.Substring(6, 2);
                strFrame = m_ListClip[i].FSTCSTART.Substring(9, 2);

                strHourE = m_ListClip[i].FSTCEND.Substring(0, 2);
                strMinuteE = m_ListClip[i].FSTCEND.Substring(3, 2);
                strSecondE = m_ListClip[i].FSTCEND.Substring(6, 2);
                strFrameE = m_ListClip[i].FSTCEND.Substring(9, 2);

                AlterListClip.Add(m_ListClip[i]);    //先把段落資料都塞到ListClip，若是有錯再一個一個換掉

                //if (m_ListClip[i].FSCLIPTITLE.Trim() == "")
                //{
                if (TransferTimecode.check_timecode_format(strHour, strMinute, strSecond, strFrame, out strStartMsg) == false)
                {
                    if (strStartMsg == "所輸入的時間不符合DropFrame的格式")
                    {
                        AlterListClip[i].FSTCSTART = strHour + ":" + strMinute + ":" + strSecond + ":02";
                    }
                }

                if (TransferTimecode.check_timecode_format(strHourE, strMinuteE, strSecondE, strFrameE, out strEndMsg) == false)
                {
                    if (strEndMsg == "所輸入的時間不符合DropFrame的格式")
                    {
                        AlterListClip[i].FSTCEND = strHourE + ":" + strMinuteE + ":" + strSecondE + ":02";
                        //AlterListClip[i].FSTCEND = checkEndTimecode(m_ListClip[i].FSTCEND);
                        //原本輸入錯誤的Timecode資料要改成後面變02
                        //AlterListClip[i].FSTCEND = strHourE + ":" + strMinuteE + ":" + strSecondE + ":02";
                    }
                }
                //}
                //else
                //{
                //    if (TransferTimecode.check_timecode_format(strHour, strMinute, strSecond, strFrame, out strStartMsg) == false)
                //        AlterListClip[i].FSTCSTART = strHour + ":" + strMinute + ":" + strSecond + ":02";

                //    if (TransferTimecode.check_timecode_format(strHourE, strMinuteE, strSecondE, strFrameE, out strEndMsg) == false)
                //    {
                //        AlterListClip[i].FSTCEND = checkEndTimecode(m_ListClip[i].FSTCEND);
                //        //原本輸入錯誤的Timecode資料要改成後面變02
                //        //AlterListClip[i].FSTCEND = strHourE + ":" + strMinuteE + ":" + strSecondE + ":02";
                //    }
                //}
            }

            m_ListClip.Clear();                     //段落先清掉
            m_ListClip = AlterListClip;
            DGTapeClip.ItemsSource = null;
            DGTapeClip.ItemsSource = m_ListClip;
        }

        //Jarvis在20140903覺得這個方法很奇怪~
        //將不存在的結束Timecode改成存在的
        //private string checkEndTimecode(string strEnd)
        //{
        //    string strReturn = TransferTimecode.frame2timecode(TransferTimecode.timecodetoframe(strEnd));

        //    if (strReturn.EndsWith("28") == true)
        //        strReturn = strReturn.Substring(0, 9) + "29";

        //    return strReturn;
        //}

        //修改段落資料
        private void btnAlter_Click(object sender, RoutedEventArgs e)
        {
            STT700_03 STT700_03_frm = new STT700_03(m_ListClip, tbxTitle.Text.Trim(), tbxDescription.Text.Trim());  //檢查段落資料提示訊息
            STT700_03_frm.Show();

            STT700_03_frm.Closing += (s, args) =>
            {
                if (STT700_03_frm.DialogResult == true)
                {
                    tbxTitle.Text = STT700_03_frm.tbxTITLE.Text.Trim();
                    tbxDescription.Text = STT700_03_frm.tbxDESCRIPTION.Text.Trim();
                }

                //因為STT700_03_frm的修改段落，是透過參考位置，所以不需要STT700_03_frm.DialogResult == true，結果都是一樣的
                m_ListClip = STT700_03_frm.m_ListClipTimeCode;
                DGTapeClip.ItemsSource = null;
                DGTapeClip.ItemsSource = m_ListClip;
            };
        }

        //勾選事件_檢查要回溯的影帶
        private void chbCheck_Checked(object sender, RoutedEventArgs e)
        {
            if (!bolCheck)
            {
                CheckBox objCeeck = sender as CheckBox;

                //原本是找DataGrid裡的影帶編號，但是此方式若是畫面上被蓋住，此寫法有時候就會找不到，因此要改變下面的寫法
                //DataGridRow currentDataGridRow = DataGridRow.GetRowContainingElement(objCeeck);
                //string selectedTapeName = ((TextBlock)DGTapeLabel.Columns[8].GetCellContent(currentDataGridRow)).Text;

                Class_TAPE_LABEL_TAPE obj = DataGridRow.GetRowContainingElement(objCeeck).DataContext as Class_TAPE_LABEL_TAPE;
                string selectedTapeName = obj.FSTAPEID.Trim();

                if (m_ListCheck.Contains(selectedTapeName) == false)
                {
                    m_ListCheck.Add(selectedTapeName);
                }
            }
            //m_ListLABEL_TAPE.Add((Class_TAPE_LABEL_TAPE)DGTapeLabel.SelectedItem); 
        }

        //取消勾選事件_檢查要回溯的影帶
        private void chbCheck_Unchecked(object sender, RoutedEventArgs e)
        {
            if (!bolCheck)
            {
                CheckBox objCeeck = sender as CheckBox;

                //原本是找DataGrid裡的影帶編號，但是此方式若是畫面上被蓋住，此寫法有時候就會找不到，因此要改變下面的寫法
                //DataGridRow currentDataGridRow = DataGridRow.GetRowContainingElement(objCeeck);
                //string selectedTapeName = ((TextBlock)DGTapeLabel.Columns[8].GetCellContent(currentDataGridRow)).Text;

                Class_TAPE_LABEL_TAPE obj = DataGridRow.GetRowContainingElement(objCeeck).DataContext as Class_TAPE_LABEL_TAPE;
                string selectedTapeName = obj.FSTAPEID.Trim();

                if (m_ListCheck.Contains(selectedTapeName))
                {
                    m_ListCheck.Remove(selectedTapeName);
                }
            }
            //m_ListLABEL_TAPE.Remove((Class_TAPE_LABEL_TAPE)DGTapeLabel.SelectedItem);       
        }

        //勾選事件_檢查要回溯並轉成播出帶的影帶
        private void chbSTT_Checked(object sender, RoutedEventArgs e)
        {
            if (!bolCheck)
            {
                CheckBox objCeeck = sender as CheckBox;

                Class_TAPE_LABEL_TAPE obj = DataGridRow.GetRowContainingElement(objCeeck).DataContext as Class_TAPE_LABEL_TAPE;
                string selectedTapeName = obj.FSTAPEID.Trim();

                if (m_ListTTCheck.Contains(selectedTapeName) == false)
                {
                    m_ListTTCheck.Add(selectedTapeName);
                }
            }
        }

        //取消勾選事件_檢查要回溯並轉成播出帶的影帶
        private void chbSTT_Unchecked(object sender, RoutedEventArgs e)
        {
            if (!bolCheck)
            {
                CheckBox objCeeck = sender as CheckBox;

                Class_TAPE_LABEL_TAPE obj = DataGridRow.GetRowContainingElement(objCeeck).DataContext as Class_TAPE_LABEL_TAPE;
                string selectedTapeName = obj.FSTAPEID.Trim();

                if (m_ListTTCheck.Contains(selectedTapeName))
                {
                    m_ListTTCheck.Remove(selectedTapeName);
                }
            }
        }

        //館藏資料的DataGrid_LoadingRow(擋Bug)
        private void DGTapeLabel_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            bolCheck = true;    //避免LoadingLoad時觸發了check及uncheck事件，因此要擋掉
            CheckBox chk = ((CheckBox)DGTapeLabel.Columns[0].GetCellContent(e.Row));
            chk.IsChecked = false;//初始化為勾選

            Class_TAPE_LABEL_TAPE obj = e.Row.DataContext as Class_TAPE_LABEL_TAPE;

            if (!string.IsNullOrEmpty(obj.FSTAPEID.Trim()))
            {
                foreach (string Tapeobj in m_ListCheck)   //如果在被勾選的List裡，就要把它勾選起來
                {
                    if (Tapeobj == obj.FSTAPEID.Trim())
                    {
                        ((CheckBox)DGTapeLabel.Columns[0].GetCellContent(e.Row)).IsChecked = true;
                        break;
                    }
                }
            }
            bolCheck = false;
        }

        //直接輸入長度後，自動產生一筆段落資料
        private void btnDurationTimeCode_Click(object sender, RoutedEventArgs e)
        {
            int intHour = 0;
            int intMin = 0;
            int intSec = 0;
            int intCountSec = 0;   //秒數要取幾位
            string strEndTimeCode = "";
            string strDuration = "";
            string strDurationTimeCode = tbxDuration.Text.Trim();   //手動輸入的影帶長度資料

            if (strDurationTimeCode.Trim() == "")
            {
                MessageBox.Show("請輸入影帶長度欄位！", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (strDurationTimeCode.IndexOf("'") == -1 || strDurationTimeCode.IndexOf("\"") == -1)
            {
                MessageBox.Show("請檢查影帶長度欄位格式，格式為:分'秒\"", "提示訊息", MessageBoxButton.OK);
                return;
            }

            if (strDurationTimeCode.IndexOf("'") > -1 && strDurationTimeCode.IndexOf("\"") > -1)
            {
                intCountSec = strDurationTimeCode.IndexOf("\"") - strDurationTimeCode.IndexOf("'") - 1;
                if (int.TryParse(strDurationTimeCode.Substring(0, strDurationTimeCode.IndexOf("'")), out intMin) == true && int.TryParse(strDurationTimeCode.Substring(strDurationTimeCode.IndexOf("'") + 1, intCountSec), out intSec) == true)
                {
                    if (intMin >= 60)
                    {
                        intHour = intMin / 60;
                        intMin = intMin % 60;
                        strDuration = intHour.ToString().PadLeft(2, '0') + ":" + intMin.ToString().PadLeft(2, '0') + ":" + intSec.ToString().PadLeft(2, '0') + ";00";
                        intHour = intHour + 1;  //因為時從01算起，所以要加1    
                        strEndTimeCode = intHour.ToString().PadLeft(2, '0') + ":" + intMin.ToString().PadLeft(2, '0') + ":" + intSec.ToString().PadLeft(2, '0') + ";00";
                    }
                    else
                    {
                        strEndTimeCode = "01:" + intMin.ToString().PadLeft(2, '0') + ":" + intSec.ToString().PadLeft(2, '0') + ";00";
                        strDuration = "00:" + intMin.ToString().PadLeft(2, '0') + ":" + intSec.ToString().PadLeft(2, '0') + ";00";
                    }

                    Class_TAPE_LABEL_CLIP objClip = new Class_TAPE_LABEL_CLIP();
                    objClip.FSBIB = "";
                    objClip.FSTCSTART = "01:00:00;00";
                    objClip.FSTCEND = strEndTimeCode;
                    objClip.FSDURATION = strDuration;
                    objClip.FSDURATION = TransferTimecode.Subtract_timecode(objClip.FSTCSTART, objClip.FSTCEND);
                    objClip.FNBEG_TIMECODE_FRAME = TransferTimecode.timecodetoframe(objClip.FSTCSTART);
                    objClip.FNEND_TIMECODE_FRAME = TransferTimecode.timecodetoframe(objClip.FSTCEND);
                    objClip.FSCLIPTITLE = "";
                    m_ListClip.Clear();                     //段落先清掉
                    m_ListClip.Add(objClip);
                    DGTapeClip.ItemsSource = null;
                    DGTapeClip.ItemsSource = m_ListClip;
                }
            }
        }

        //檢查是否要作短帶資料認定
        private void checkProgM_Channel_Dep(string strProgID, string strProgName, string strChannelID, string strDepID)
        {
            string strCheck = "";   //判斷基本資料缺少訊息     

            //無頻道別要檔掉     
            if (strChannelID.Trim() == "")
            {
                strCheck = "無頻道別資料";
            }

            //無成案單位要檔掉     
            if (strDepID.Trim() == "0")
            {
                if (strCheck.Trim() == "")
                    strCheck = "無成案單位資料";
                else
                    strCheck = strCheck + "、無成案單位資料";
            }

            if (strCheck.Trim() != "")
            {
                MessageBox.Show(strCheck + "，請先進行節目資料認定", "提示訊息", MessageBoxButton.OK);

                //判斷有無修改「節目資料認定」的權限     
                if (ModuleClass.getModulePermissionByName("01", "節目資料認定") == true)
                {
                    MessageBoxResult resultMsg = MessageBox.Show("確定要修改「" + strProgName.Trim() + "」的頻道別資料嗎?", "提示訊息", MessageBoxButton.OKCancel);
                    if (resultMsg == MessageBoxResult.OK)
                    {
                        PRG100_13 PRG100_13_frm = new PRG100_13("G", strProgID, strProgName, strChannelID, strDepID);
                        PRG100_13_frm.Show();

                        PRG100_13_frm.Closing += (s, args) =>
                        {
                            //如果資料補齊，即可載入節目資料
                            if (PRG100_13_frm.bolcheck == true)
                            {
                                //頻道別及部門別，都要用節目資料認定那一頁選到的資料才正確                         
                                checkProg(strProgID, strProgName, PRG100_13_frm.m_CHANNEL_ID_LAST.ToString().Trim(), PRG100_13_frm.m_FNDept_ID_LAST.ToString());
                            }

                        };
                    }
                }
            }
        }

        //載入節目資料
        private void checkProg(string strProgID, string strProgName, string strChannelID, string strDepID)
        {
            tbxPROG_NAME.Text = "(" + strProgID + ")" + strProgName;
            tbxPROG_NAME.Tag = strProgID;
            m_ProgName = strProgName;

            tbxEPISODE.Text = "";
            lblEPISODE_NAME.Content = "";
            m_strEpisode = "0";
            m_strCHANNELID = strChannelID;
            m_strDep_ID = strDepID;
            m_strID = strProgID;

            DGTapeClip.ItemsSource = null;
            DGTapeLabel.ItemsSource = null;
            tbxTitle.Text = "";
            tbxDescription.Text = "";
            tbxDuration.Text = "";
        }

        #endregion

    }
}

