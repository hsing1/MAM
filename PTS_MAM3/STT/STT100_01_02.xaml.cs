﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSBROADCAST;
using PTS_MAM3.WSMAMFunctions;
using System.Text;
using System.Collections.ObjectModel;

namespace PTS_MAM3.STT
{
    public partial class STT100_01_02 : ChildWindow
    {
        WSBROADCASTSoapClient client = new WSBROADCASTSoapClient();                 //產生新的代理類別        
        public List<Class_LOG_VIDEO_SEG> ListSeg = new List<Class_LOG_VIDEO_SEG>();        //段落集合
        public List<string> CheckList = new List<string>();    //紀錄點選到的List
         
        string m_strFileTypeName ="";   //入庫檔案類型名稱
        string m_strFileTypeID = "";    //入庫檔案類型編號
        string m_strType="" ;           //類型
        string m_strID="" ;             //編碼
        string m_strEpisode ="" ;       //集別
        string m_strCHANNELID = "";     //頻道別
        string m_strFileNOEdit = "";    //檔案編號(修改時用的)
        string m_strBroID = "";         //送帶轉檔單單號
        string m_strFileNONew = "";     //檔案編號(新增時用的)  
        string m_DIRID = "";            //串結點用的
        string m_ProgName = "";         //串結點用的_節目名稱
        string m_EpisodeName = "";      //串結點用的_子集名稱

        public STT100_01_02(string strFileTypeName, string strFileTypeID, string strType, string strID, string strEpisode, string strCHANNELID, string strFileNO, string strBroID,string strProgName , string strEpisodeName)
        {
            InitializeComponent();

            m_strFileTypeName = strFileTypeName.Trim();
            m_strFileTypeID = strFileTypeID.Trim();
            m_strType = strType.Trim();
            m_strID = strID.Trim();
            m_strEpisode = strEpisode.Trim();
            m_strCHANNELID = strCHANNELID ;
            tbFileTypeName.Text = m_strFileTypeName;
            m_strFileNOEdit = strFileNO ;
            m_strBroID = strBroID;
            m_ProgName = strProgName;
            m_EpisodeName = strEpisodeName;

            InitializeForm();        //初始化本頁面

            if (m_strFileNOEdit.Trim() != "")  //若是有資料則為預帶舊資料讓使用者修改
            {
                this.Title = "修改段落資料";
                client.GetTBLOG_VIDEO_SEG_BYFileIDAsync(m_strFileNOEdit);
                client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDAsync(m_strType, m_strID, m_strEpisode, m_strBroID);
            }

            List<string> cbTest = new List<string>();
            cbTest.Add("台灣");
            cbTest.Add("香港");
            cbTest.Add("越南");


        }

        void InitializeForm() //初始化本頁面
        {        
            //透過檔案編號取得段落檔
            client.GetTBLOG_VIDEO_SEG_BYFileIDCompleted += new EventHandler<GetTBLOG_VIDEO_SEG_BYFileIDCompletedEventArgs>(client_GetTBLOG_VIDEO_SEG_BYFileIDCompleted);
        }

        #region 實作        

        //實作透過檔案編號取得段落檔
        void client_GetTBLOG_VIDEO_SEG_BYFileIDCompleted(object sender, GetTBLOG_VIDEO_SEG_BYFileIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    ListSeg = e.Result;
                    DGSeg.DataContext = ListSeg;
                }
            }
        }

        #endregion

        #region 檢查

        private Boolean CheckFormat()    //檢查畫面上的欄位是否都填妥
        {
            StringBuilder strMessage = new StringBuilder();
            int intCheck;   //純粹為了判斷是否為數字型態之用

            if (int.TryParse(tbxS1.Text.Trim(), out intCheck) == false || tbxS1.Text.Trim().Length != 2)
                    strMessage.AppendLine("請檢查「Time Code 起」第一個欄位必須為數值型態且為兩碼");

            if (int.TryParse(tbxS2.Text.Trim(), out intCheck) == false || tbxS2.Text.Trim().Length != 2)
                strMessage.AppendLine("請檢查「Time Code 起」第二個欄位必須為數值型態且為兩碼");

            if (int.TryParse(tbxS3.Text.Trim(), out intCheck) == false || tbxS3.Text.Trim().Length != 2)
                strMessage.AppendLine("請檢查「Time Code 起」第三個欄位必須為數值型態且為兩碼");

            if (int.TryParse(tbxS4.Text.Trim(), out intCheck) == false || tbxS4.Text.Trim().Length != 2)
                strMessage.AppendLine("請檢查「Time Code 起」第四個欄位必須為數值型態且為兩碼");

            if (int.TryParse(tbxS5.Text.Trim(), out intCheck) == false || tbxS5.Text.Trim().Length != 2)
                strMessage.AppendLine("請檢查「Time Code 迄」第一個欄位必須為數值型態且為兩碼");

            if (int.TryParse(tbxS6.Text.Trim(), out intCheck) == false || tbxS6.Text.Trim().Length != 2)
                strMessage.AppendLine("請檢查「Time Code 迄」第二個欄位必須為數值型態且為兩碼");

            if (int.TryParse(tbxS7.Text.Trim(), out intCheck) == false || tbxS7.Text.Trim().Length != 2)
                strMessage.AppendLine("請檢查「Time Code 迄」第三個欄位必須為數值型態且為兩碼");

            if (int.TryParse(tbxS8.Text.Trim(), out intCheck) == false || tbxS8.Text.Trim().Length != 2)
                strMessage.AppendLine("請檢查「Time Code 迄」第四個欄位必須為數值型態且為兩碼");

            if (strMessage.ToString().Trim() == "")
                return true;
            else
            {
                MessageBoxResult result = MessageBox.Show(strMessage.ToString(), "提示訊息", MessageBoxButton.OK);
                return false;
            }
        }    

        #endregion

        //新增段落檔
        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            if (CheckFormat() == false)      //檢查畫面上的欄位是否都填妥
                return;

            FormToClass_Seg();  //新增段落資料
 
            tbxS1.Text = "";    //新增完就把欄位清空
            tbxS2.Text = "";
            tbxS3.Text = "";
            tbxS4.Text = "";
            tbxS5.Text = "";
            tbxS6.Text = "";
            tbxS7.Text = "";
            tbxS8.Text = ""; 
        }

        //新增段落資料
        private void FormToClass_Seg()
        {
            Class_LOG_VIDEO_SEG ClassSeg = new Class_LOG_VIDEO_SEG();            //段落
            ClassSeg.FSFILE_NO = m_strFileNOEdit;
            ClassSeg.FNSEG_ID = (ListSeg.Count + 1).ToString();
            ClassSeg.FSBEG_TIMECODE = tbxS1.Text.ToString().Trim() + ":" + tbxS2.Text.ToString().Trim() + ":" + tbxS3.Text.ToString().Trim() + ";" + tbxS4.Text.ToString().Trim();
            ClassSeg.FSEND_TIMECODE = tbxS5.Text.ToString().Trim() + ":" + tbxS6.Text.ToString().Trim() + ":" + tbxS7.Text.ToString().Trim() + ";" + tbxS8.Text.ToString().Trim();
            ClassSeg.FSCREATED_BY = UserClass.userData.FSUSER_ID.ToString();     //建檔者  
            ClassSeg.FSUPDATED_BY = UserClass.userData.FSUSER_ID.ToString();     //建檔者

            ListSeg.Add(ClassSeg);
            DGSeg.DataContext = null;
            DGSeg.DataContext = ListSeg;
        }

        //刪除最後一筆
        private void btnDel_Click(object sender, RoutedEventArgs e)
        {
            ListSeg.RemoveAt(ListSeg.Count - 1);
            DGSeg.DataContext = null;
            DGSeg.DataContext = ListSeg;

            tbxBEG_TIMECODE.Text = "";
            tbxEND_TIMECODE.Text = "";
        }

        //按下確定時，若是有資料即上傳
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            //if (ListSeg.Count > 0)
            //{
            //    if (m_strFileNOEdit=="") //先判斷是修改還是新增
            //        client.fnGetFileIDAsync(m_strType, m_strID, m_strEpisode, UserClass.userData.FSUSER_ID.ToString(), m_strCHANNELID,m_ProgName,m_EpisodeName);    //取得檔案編號(FSFILE_NO)並且上傳段落檔                 
            //    else
            //    {
            //        //新增段落前先去刪除原本已存在的段落檔
            //        client.DelTBLOG_VIDEO_SEG_BYFileIDAsync(m_strFileNOEdit, UserClass.userData.FSUSER_ID.ToString());
                    
            //         if (ListSeg.Count > 0)
            //             //若是有段落資料，TimeCode的起迄也要修改至入庫影像檔
            //             client.UPDATE_TBLOG_VIDEO_TITLE_DESAsync(m_strFileNOEdit, tbxTITLE.Text.Trim(), tbxDESCRIPTION.Text.Trim(), ListSeg[0].FSBEG_TIMECODE.ToString().Trim(), ListSeg[ListSeg.Count - 1].FSEND_TIMECODE.ToString().Trim(), UserClass.userData.FSUSER_ID.ToString());
            //        else
            //             //若是沒段落資料，TimeCode的起迄不需要修改至入庫影像檔
            //            client.UPDATE_TBLOG_VIDEO_TITLE_DESAsync(m_strFileNOEdit, tbxTITLE.Text.Trim(), tbxDESCRIPTION.Text.Trim(), "", "", UserClass.userData.FSUSER_ID.ToString());
            //    }
            //}
            //else
            //    MessageBox.Show("請先新增段落資料再進行上傳作業！");

            string strCheckList = "";

            for (int i = 0; i < CheckList.Count; i++)
            {
                strCheckList += CheckList[i].ToString().Trim() + "、";
            }

            MessageBox.Show(strCheckList);

        }

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //修改畫面上段落資料
        private void DGSeg_CellEditEnded(object sender, DataGridCellEditEndedEventArgs e)
        {
           // Class_LOG_VIDEO_SEG SegRecord = new Class_LOG_VIDEO_SEG();      
           // SegRecord = ((Class_LOG_VIDEO_SEG)DGSeg.SelectedItem) ;
           //ListSegUpdate(SegRecord.FNSEG_ID,SegRecord.FSBEG_TIMECODE,SegRecord.FSEND_TIMECODE);
        }

        //修改畫面上段落資料時，也將List裡改掉
        private void ListSegUpdate(string strNo, string strBeg, string strEnd)
        {
            for (int i = 0; i < ListSeg.Count; i++)
            {              
              if (ListSeg[i].FNSEG_ID.ToString().Trim().Equals(strNo))
                  {
                  ListSeg[i].FSBEG_TIMECODE = strBeg ;
                  ListSeg[i].FSEND_TIMECODE = strEnd;
                  }
            }
        }

       //DataGrid裡點選CheckBox的事件處理常式
        private void chbCheck_Checked(object sender, RoutedEventArgs e)
        {
            DataGridRow currentDataGridRow = DataGridRow.GetRowContainingElement((sender as CheckBox));
            string strCheck =((TextBlock)DGSeg.Columns[1].GetCellContent(currentDataGridRow)).Text ;
            CheckList.Add(strCheck);

            if (CheckList.Count > 3)
            {
                MessageBox.Show("點選項目超過三筆，請重新選擇！");
                (sender as CheckBox).IsChecked = false;
                return;
            }    
                //MessageBox.Show("點選的段落序號-" + strCheck);
        }

        //DataGrid裡取消點選CheckBox的事件處理常式
        private void chbCheck_Unchecked(object sender, RoutedEventArgs e)
        {
            DataGridRow currentDataGridRow = DataGridRow.GetRowContainingElement((sender as CheckBox));
            string strCheck = ((TextBlock)DGSeg.Columns[1].GetCellContent(currentDataGridRow)).Text;

            CheckList.Remove(strCheck);
            //MessageBox.Show("取消點選的段落序號-" + strCheck);
        }

    }
}

