﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSBROADCAST;
using PTS_MAM3.WSMAMFunctions;
using System.Text;
using System.Collections.ObjectModel;
using PTS_MAM3.WSSTT;

namespace PTS_MAM3.STT
{
    public partial class STT300_01 : ChildWindow
    {

        WSBROADCASTSoapClient client = new WSBROADCASTSoapClient();                 //產生新的代理類別    
        WSSTTSoapClient clientStt = new WSSTTSoapClient();
        public List<Class_LOG_VIDEO_SEG> ListSeg = new List<Class_LOG_VIDEO_SEG>(); //段落集合
        List<ClassVTR> VTRList = new List<ClassVTR>();                              //VTR集合
        List<string> FileList = new List<string>();                                 //File集合
        List<string> FileDList = new List<string>();                                //File集合(只有檔名)

        string m_strFileTypeName = "";   //入庫檔案類型名稱
        string m_strFileTypeID = "";    //入庫檔案類型編號
        string m_strFileTypeSPEC = "";  //入庫檔案類型規格
        string m_strType = "";           //類型
        string m_strID = "";             //編碼
        string m_strEpisode = "";       //集別
        string m_strCHANNELID = "";     //頻道別
        string m_strFileNO = "";        //檔案編號
        string m_strBroID = "";         //送帶轉檔單單號
        string m_DIRID = "";            //串結點用的
        string m_ProgName = "";         //串結點用的_節目名稱
        string m_EpisodeName = "";      //串結點用的_子集名稱
        string m_strHiPath = "";        //高解路徑
        string m_strLowPath = "";       //低解路徑

        public string _fsTREE_PATH = string.Empty;
        public string _fsFILE_PATH = string.Empty;

        string strNote = "";
        string strBeg = "";
        string strEnd = "";

        string TreeViewPath = "";
        string m_strAudioTrack = "";
        Class_LOG_VIDEO Log_Video;
        public STT300_01(string strFileTypeName, string strFileTypeID, string strType, string strID, string strEpisode, string strCHANNELID, string strFileNO, string strBroID, string strProgName, string strEpisodeName, string strHiPath, string strLowPath, string strFileTypeSPEC, string memo, string strAudioTrack, Class_LOG_VIDEO log_video)
        {
            InitializeComponent();

            m_strFileTypeName = strFileTypeName.Trim();
            m_strFileTypeID = strFileTypeID.Trim();
            m_strFileTypeSPEC = strFileTypeSPEC.Trim();
            m_strType = strType.Trim();
            m_strID = strID.Trim();
            m_strEpisode = strEpisode.Trim();
            m_strCHANNELID = strCHANNELID;
            m_strFileNO = strFileNO;
            m_strBroID = strBroID;
            m_ProgName = strProgName;
            m_EpisodeName = strEpisodeName;
            m_strHiPath = strHiPath;
            m_strLowPath = strLowPath;
            tbFileNO.Text = m_strFileNO;
            tbFileTypeName.Text = m_strFileTypeName;

            TreeViewPath = memo;//
            m_strAudioTrack = strAudioTrack;
            TBAudioTrack.Text = m_strAudioTrack;
            Log_Video = log_video;

            InitializeForm();        //初始化本頁面

            if (m_strFileNO.Trim() != "")  //顯示段落資料          
                client.GetTBLOG_VIDEO_SEG_BYFileIDAsync(m_strFileNO);
        }



        void InitializeForm() //初始化本頁面
        {
            //更新入庫影像檔，寫入JobID(檢查轉檔進度用的)
            client.UPDATE_TBLOG_VIDEO_JobIDCompleted += new EventHandler<UPDATE_TBLOG_VIDEO_JobIDCompletedEventArgs>(client_UPDATE_TBLOG_VIDEO_JobIDCompleted);

            //透過檔案編號取得段落檔
            client.GetTBLOG_VIDEO_SEG_BYFileIDCompleted += new EventHandler<GetTBLOG_VIDEO_SEG_BYFileIDCompletedEventArgs>(client_GetTBLOG_VIDEO_SEG_BYFileIDCompleted);

            //呼叫轉檔
            clientStt.CallIngestCompleted += new EventHandler<CallIngestCompletedEventArgs>(clientStt_CallIngestCompleted);

            //2016/10/05 by Jarvis
            clientStt.CallIngest_SSDCompleted += new EventHandler<CallIngest_SSDCompletedEventArgs>(clientStt_CallIngest_SSDCompleted);
            //取得VTR List
            clientStt.GetVTRListCompleted += new EventHandler<GetVTRListCompletedEventArgs>(clientStt_GetVTRListCompleted);
            clientStt.GetVTRListAsync();

            //取得File List
            clientStt.GetFileListCompleted += new EventHandler<GetFileListCompletedEventArgs>(clientStt_GetFileListCompleted);
            clientStt.GetFileListAsync();

            ////線上簽收送帶轉檔檔
            client.UPDATE_TBBROADCAST_StatusCompleted += new EventHandler<UPDATE_TBBROADCAST_StatusCompletedEventArgs>(client_UPDATE_TBBROADCAST_StausCompleted);

            //根據檔案編號尋找TBLOG_VIDEO中的資料 by Jarvis20130621
            client.GetTBLOG_VIDEO_BYFSFILE_NOCompleted += new EventHandler<GetTBLOG_VIDEO_BYFSFILE_NOCompletedEventArgs>(client_GetTBLOG_VIDEO_BYFSFILE_NOCompleted);

            //根據送帶轉檔單號尋找TBBROADCAST中的資料 by Jarvis20130621
            client.GetTBBROADCAST_BYBROIDCompleted += new EventHandler<GetTBBROADCAST_BYBROIDCompletedEventArgs>(client_GetTBBROADCAST_BYBROIDCompleted);

            //開啟本頁後記錄轉檔資訊 by Jarvis 20141027
            client.RecordStartTranscodeCompleted += new EventHandler<RecordStartTranscodeCompletedEventArgs>(client_RecordStartTranscodeCompleted);
        }

        void clientStt_CallIngest_SSDCompleted(object sender, CallIngest_SSDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    if (e.Result.StartsWith("E"))
                    {
                        MessageBox.Show("啟動轉檔失敗，原因：" + e.Result.Substring(2), "提示訊息", MessageBoxButton.OK);
                        this.DialogResult = false;
                    }
                    else
                    {
                        //更新轉檔的JobID                   
                        client.UPDATE_TBLOG_VIDEO_JobIDAsync(m_strFileNO, e.Result, checkTransFrom(), UserClass.userData.FSUSER_ID.ToString());
                    }
                }
                else
                {
                    MessageBox.Show("啟動轉檔失敗", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
        }

        void client_RecordStartTranscodeCompleted(object sender, RecordStartTranscodeCompletedEventArgs e)
        {
            if (!STT100.GoFlow)//增加if else by Jarvis20130621
            {
                #region 取消Flow要增加驗證的部分
                //在這裡檢查TBLOG_VIDEO的檔案狀態與TBBROADCAST中的表單狀態 
                //by Jarvis20130621
                client.GetTBLOG_VIDEO_BYFSFILE_NOAsync(m_strFileNO);
                #endregion
            }
            else
            {
                StartCallIngestMethod();
                //if (rdbTape.IsChecked == true)
                //    clientStt.CallIngestAsync(m_strFileNO, "TAPE", strNote, strBeg, strEnd, m_strHiPath, m_strLowPath, IngestNote(), m_strFileTypeSPEC);
                //else if (rdbFile.IsChecked == true)
                //    clientStt.CallIngestAsync(m_strFileNO, "FILE", strNote, strBeg, strEnd, m_strHiPath, m_strLowPath, IngestNote(), m_strFileTypeSPEC);
                //else if (rdbFile_Fiber.IsChecked == true)
                //    clientStt.CallIngestAsync(m_strFileNO, "FILE", strNote, strBeg, strEnd, m_strHiPath, m_strLowPath, IngestNote(), m_strFileTypeSPEC);
            }
        }

        #region 實作
        //根據檔案編號尋找TBLOG_VIDEO中的資料 by Jarvis20130621
        void client_GetTBLOG_VIDEO_BYFSFILE_NOCompleted(object sender, GetTBLOG_VIDEO_BYFSFILE_NOCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result.Count > 0)
                {
                    if (e.Result[0].FCFILE_STATUS != "X"
                && e.Result[0].FCFILE_STATUS != "B" && e.Result[0].FCFILE_STATUS != "R")
                    {
                        MessageBox.Show("此檔案狀態目前不是「待轉檔」或「轉檔失敗」，無法啟動轉檔！", "提示", MessageBoxButton.OK);
                        return;
                    }

                    client.GetTBBROADCAST_BYBROIDAsync(e.Result[0].FSBRO_ID);
                }
            }
        }
        //根據送帶轉檔單號尋找TBBROADCAST中的資料 by Jarvis20130621
        void client_GetTBBROADCAST_BYBROIDCompleted(object sender, GetTBBROADCAST_BYBROIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result.Count > 0)
                {
                    if (e.Result[0].FCCHECK_STATUS == "X")
                    {
                        MessageBox.Show("此筆送帶轉檔單已抽單，無法轉檔", "提示", MessageBoxButton.OK);
                        this.DialogResult = false;
                    }
                    else
                    {
                        StartCallIngestMethod();
                    }
                }
            }
        }
        //實作-更新入庫影像檔，寫入JobID(檢查轉檔進度用的)
        void client_UPDATE_TBLOG_VIDEO_JobIDCompleted(object sender, UPDATE_TBLOG_VIDEO_JobIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    MessageBox.Show("啟動轉檔成功！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = true;
                }
            }
        }

        //實作透過檔案編號取得段落檔
        void client_GetTBLOG_VIDEO_SEG_BYFileIDCompleted(object sender, GetTBLOG_VIDEO_SEG_BYFileIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    ListSeg = e.Result;
                    DGSeg.DataContext = ListSeg;
                }
            }
        }

        //實作-取得VTR List
        void clientStt_GetVTRListCompleted(object sender, GetVTRListCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        VTRList.Add(e.Result[i]);
                    }

                    rdbTape.IsEnabled = true;   //一進入系統後要先讀取VTR List，有VTR可選才可以讓checkbox可用
                    cbVTR.IsEnabled = true;
                }
            }
        }

        //實作-取得File List
        void clientStt_GetFileListCompleted(object sender, GetFileListCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        FileList.Add(e.Result[i]);
                        FileDList.Add(CheckFileList(e.Result[i]));
                    }
                }
            }
        }

        //實作-呼叫轉檔
        void clientStt_CallIngestCompleted(object sender, CallIngestCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    if (e.Result.StartsWith("E"))
                    {
                        MessageBox.Show("啟動轉檔失敗，原因：" + e.Result.Substring(2), "提示訊息", MessageBoxButton.OK);
                        this.DialogResult = false;
                    }
                    else
                    {
                        //更新轉檔的JobID                   
                        client.UPDATE_TBLOG_VIDEO_JobIDAsync(m_strFileNO, e.Result, checkTransFrom(), UserClass.userData.FSUSER_ID.ToString());
                    }
                }
                else
                {
                    MessageBox.Show("啟動轉檔失敗", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
        }

        //實作-改變送帶轉檔單狀態為已轉檔
        void client_UPDATE_TBBROADCAST_StausCompleted(object sender, UPDATE_TBBROADCAST_StatusCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                //if (e.Result == true) //不秀訊息，背景執行
                //    MessageBox.Show("送帶轉檔資料-" + m_strBroID + "狀態改變為轉檔成功！", "提示訊息", MessageBoxButton.OK);
                //else
                //    MessageBox.Show("送帶轉檔資料-" + m_strBroID + "狀態改變為轉檔失敗！", "提示訊息", MessageBoxButton.OK);
            }
        }


        private void Fill_VTR()     //將VTR List放到下拉式選單裡
        {
            this.cbVTR.Items.Clear();

            for (int i = 0; i < VTRList.Count; i++)
            {
                ComboBoxItem cbiVTR = new ComboBoxItem();
                cbiVTR.Content = VTRList[i].Name.Trim() + "(" + VTRList[i].Comment.Trim() + ")";
                cbiVTR.Tag = VTRList[i].Name.Trim();
                this.cbVTR.Items.Add(cbiVTR);
            }
        }

        private void Fill_File(string strSW)     //開啟選擇檔案位置
        {
            tbFileName.Tag = "";
            tbFileName.Text = "請選擇數位檔案";
            STT300_01_01 STT300_01_01_frm;
            if (TreeViewPath != "")
            {
                STT300_01_01_frm = new STT300_01_01(strSW, TreeViewPath);
            }
            else
            {
                STT300_01_01_frm = new STT300_01_01(strSW);

            }
            STT300_01_01_frm.Show();
            STT300_01_01_frm.Closed += (s, args) =>
            {
                if (STT300_01_01_frm.DialogResult == true)
                {
                    tbFileName.Tag = STT300_01_01_frm.strSelect_File_Path;
                    tbFileName.Text = STT300_01_01_frm.strSelect_File_Name;
                }
            };
        }

        private void checkList()   //影帶擷取或是數位檔案
        {
            if (rdbTape.IsChecked == true)
            {
                tbxType.Text = "VTR NO#";
                tbFileName.Visibility = Visibility.Collapsed;
                cbVTR.Visibility = Visibility.Visible;
                Fill_VTR();
            }
            else
            {
                tbxType.Text = "數位檔案";
                tbFileName.Visibility = Visibility.Visible;
                cbVTR.Visibility = Visibility.Collapsed;

                if (rdbFile.IsChecked == true)
                    Fill_File("Network");     //NetWork 網路
                else if (rdbFile_Fiber.IsChecked == true)
                    Fill_File("Fiber");     //Fiber   光纖
            }
        }

        #endregion

        //按下確定時，若是有資料即上傳
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (ListSeg.Count == 0)
            {
                MessageBox.Show("該筆檔案無段落資料，無法進行轉檔作業", "提示訊息", MessageBoxButton.OK);
                return;
            }

            strNote = "";
            strBeg = ListSeg[0].FSBEG_TIMECODE.ToString().Trim();
            strEnd = ListSeg[ListSeg.Count - 1].FSEND_TIMECODE.ToString().Trim();
            //strNote strBeg strEnd改成類別變數註解by Jarvis20130621
            //string strNote = "";
            //string strBeg =ListSeg[0].FSBEG_TIMECODE.ToString().Trim();
            //string strEnd =ListSeg[ListSeg.Count-1].FSEND_TIMECODE.ToString().Trim();

            if (rdbTape.IsChecked == true && cbVTR.SelectedIndex == -1)
            {
                MessageBox.Show("請先選擇VTR NO#", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (rdbFile.IsChecked == true && tbFileName.Text == "請選擇數位檔案")
            {
                MessageBox.Show("請先選擇數位檔案(網路)", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (rdbFile_Fiber.IsChecked == true && tbFileName.Text == "請選擇數位檔案")
            {
                MessageBox.Show("請先選擇數位檔案(光纖)", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (rdbTape.IsChecked == false && rdbFile.IsChecked == false && rdbFile_Fiber.IsChecked == false)
            {
                MessageBox.Show("請先選擇轉檔類型", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else
            {
                if (cbVTR.SelectedIndex != -1)
                    strNote = ((ComboBoxItem)cbVTR.SelectedItem).Tag.ToString();

                if (tbFileName.Text != "請選擇數位檔案")
                    strNote = tbFileName.Tag.ToString().Trim();
            }

            if (m_strFileNO.Trim() != "")
            {
                OKButton.IsEnabled = false;  //避免使用者按了兩次，按下後要鎖住

                //更新送帶轉檔的狀態為已轉檔 //目前先不更新
                //client.UPDATE_TBBROADCAST_StausAsync(m_strBroID.Trim(), "I", UserClass.userData.FSUSER_ID.ToString());

                client.RecordStartTranscodeAsync(Log_Video, UserClass.userData.FSUSER_ID);
            }
        }

        void StartCallIngestMethod()
        {

            if (rdbTape.IsChecked == true)
                clientStt.CallIngestAsync(m_strFileNO, "TAPE", strNote, strBeg, strEnd, m_strHiPath, m_strLowPath, IngestNote(), m_strFileTypeSPEC);
            else if (rdbFile.IsChecked == true)
            {
                //新增"001"的判斷 2016/10/05 by Jarvis
                if (m_strFileTypeID == "001")
                {
                    clientStt.CallIngest_SSDAsync(m_strFileNO, "FILE", strNote, strBeg, strEnd, m_strHiPath, m_strLowPath, IngestNote(), m_strFileTypeSPEC);
                }
                else
                {
                    clientStt.CallIngestAsync(m_strFileNO, "FILE", strNote, strBeg, strEnd, m_strHiPath, m_strLowPath, IngestNote(), m_strFileTypeSPEC);
                }
            }
            else if (rdbFile_Fiber.IsChecked == true)
            {
                //新增"001"的判斷 2016/10/05 by Jarvis
                if (m_strFileTypeID == "001")
                {
                    clientStt.CallIngest_SSDAsync(m_strFileNO, "FILE", strNote, strBeg, strEnd, m_strHiPath, m_strLowPath, IngestNote(), m_strFileTypeSPEC);
                }
                else
                {
                    clientStt.CallIngestAsync(m_strFileNO, "FILE", strNote, strBeg, strEnd, m_strHiPath, m_strLowPath, IngestNote(), m_strFileTypeSPEC);
                }
            }

        }

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //影帶擷取
        private void rdbTape_Checked(object sender, RoutedEventArgs e)
        {
            checkList();
        }

        //數位轉檔
        private void rdbFile_Checked(object sender, RoutedEventArgs e)
        {
            checkList();
        }

        //取出檔案名稱
        private string CheckFileList(string strFile)
        {
            int intCount = 0;
            intCount = strFile.LastIndexOf("\\");

            if (intCount <= 0)
                return strFile;
            else
                return strFile.Substring(intCount + 1);
        }

        //送帶轉檔備註
        private string IngestNote()
        {
            StringBuilder strIngestNote = new StringBuilder();

            if (m_strType.Trim() == "G")
            {
                strIngestNote.Append("節目名稱：" + m_ProgName);
                strIngestNote.Append(" ，節目編號：" + m_strID);

                if (m_strEpisode.Trim() != "0" || m_strEpisode.Trim() != "")
                    strIngestNote.Append(" ，集別：" + m_strEpisode.Trim());
            }
            else if (m_strType.Trim() == "P")
            {
                strIngestNote.Append("短帶名稱：" + m_ProgName + m_strID);
                strIngestNote.Append(" ，短帶編號：" + m_strID);
            }

            strIngestNote.Append(" ，檔案類型：" + m_strFileTypeName);
            strIngestNote.Append(" ，檔案編號：" + m_strFileNO);

            return strIngestNote.ToString();
        }

        //轉檔來源
        private string checkTransFrom()
        {
            if (rdbTape.IsChecked == true)
                return "影帶擷取-VTR NO#： " + ((ComboBoxItem)(cbVTR.SelectedItem)).Content.ToString().Trim();
            else if (rdbFile.IsChecked == true)
                return "數位轉檔(網路)-檔案名稱： " + tbFileName.Text.ToString().Trim();
            else if (rdbFile_Fiber.IsChecked == true)
                return "數位轉檔(光纖)-檔案名稱： " + tbFileName.Text.ToString().Trim();
            else
                return "";
        }

    }
}

