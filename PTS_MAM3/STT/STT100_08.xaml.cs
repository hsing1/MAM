﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSMAMFunctions;
using PTS_MAM3.ProgData;
using PTS_MAM3.WSBROADCAST;
using PTS_MAM3.PRG;
using System.Text;
using PTS_MAM3.FlowMAM;

namespace PTS_MAM3.STT
{
    public partial class STT100_08 : ChildWindow
    {
        WSBROADCASTSoapClient client = new WSBROADCASTSoapClient();                 //產生新的代理類別        
        WSMAMFunctionsSoapClient clientMAM = new WSMAMFunctionsSoapClient() ;
        //flowWebService.FlowSoapClient FlowClinet = new flowWebService.FlowSoapClient();//流程引擎  
        FlowMAMSoapClient clientFlow = new FlowMAMSoapClient();                     //後端的流程引擎

        public List<Class_LOG_VIDEO> ListLogVideo = new List<Class_LOG_VIDEO>();    //入庫影像檔集合
        private string m_2ndMemo = "";                                              //Secondary備註       

        string m_strFileTypeName = "";   //入庫檔案類型名稱
        string m_strFileTypeID = "";     //入庫檔案類型編號
        string m_strType = "";           //類型
        string m_strID = "";             //編碼
        string m_strEpisode = "";        //集別
        string m_strCHANNELID = "";      //頻道別   
        string m_strFileNO = "";         //檔案編號(修改時用的)
        string m_strBroID = "";          //送帶轉檔單單號
        string m_VIDEOID_PROG = "";      //VideoID
        string m_strCopyFSNO = "";       //複製或修改段落的檔案編號
        Boolean m_bolCopy = false;       //是否是複製的段落
       

        public STT100_08()
        {
            InitializeComponent();
            InitializeForm();          //初始化本頁面
        }

        void InitializeForm() //初始化本頁面
        {
            //取得送帶轉檔單編號
            clientMAM.GetNoRecordCompleted += new EventHandler<GetNoRecordCompletedEventArgs>(ClientMAM_GetNoRecordCompleted);
            clientMAM.GetNoRecordAsync("07", "", "", UserClass.userData.FSUSER_ID);

            //下載代碼檔_影片的檔案類型  
            client.GetTBBROADCAST_CODE_TBFILE_TYPECompleted += new EventHandler<GetTBBROADCAST_CODE_TBFILE_TYPECompletedEventArgs>(client_GetTBBROADCAST_CODE_TBFILE_TYPECompleted);
            client.GetTBBROADCAST_CODE_TBFILE_TYPEAsync();

            //查詢入庫影像檔資料_透過檔案節目編號、集別、送帶轉檔單            
            client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted += new EventHandler<GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompletedEventArgs>(client_GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted);
           
           //新增送帶轉檔單資料
            client.INSERT_BROADCASTCompleted += new EventHandler<INSERT_BROADCASTCompletedEventArgs>(client_INSERT_BROADCASTCompleted);

            //刪除入庫影像檔及段落檔
            client.DELETE_TBLOG_VIDEO_SEG_BYFILENOCompleted += new EventHandler<DELETE_TBLOG_VIDEO_SEG_BYFILENOCompletedEventArgs>(client_DELETE_TBLOG_VIDEO_SEG_BYFILENOCompleted);
             
            //刪除入庫影像檔及段落檔_BY檔案編號
            client.DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONECompleted += new EventHandler<DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONECompletedEventArgs>(client_DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONECompleted);

            ////查詢TBLOG_VIDEOID_MAP的VideoID(資料帶先不考慮VideoID，因為資料帶不送到主控播出)
            //client.QueryVideoID_PROG_STTCompleted += new EventHandler<QueryVideoID_PROG_STTCompletedEventArgs>(client_QueryVideoID_PROG_STTCompleted);

            //不跑Flow新增送帶轉檔單 by Jarvis20130710
            client.INSERT_BROADCAST_WITHOUT_FLOWCompleted += new EventHandler<INSERT_BROADCAST_WITHOUT_FLOWCompletedEventArgs>(client_INSERT_BROADCAST_WITHOUT_FLOWCompleted);
           
            //後端的流程引擎-流程起始
            clientFlow.CallFlow_NewFlowWithFieldCompleted += new EventHandler<CallFlow_NewFlowWithFieldCompletedEventArgs>(clientFlow_CallFlow_NewFlowWithFieldCompleted);
        }

        
        
        //載入入庫影像檔
        private void LoadTBLOG_VIDEO()
        {
            client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDAsync("D", m_strID, "0", m_strBroID);
        }
                
        #region 實作

        //實作-取得送帶轉檔單編號
        void ClientMAM_GetNoRecordCompleted(object sender, GetNoRecordCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    tbxBro_ID.Text = e.Result;
                    m_strBroID = e.Result;
                }
                else
                {
                    MessageBox.Show("取得送帶轉檔單編號異常，請聯絡系統管理員！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
            else
            {
                MessageBox.Show("取得送帶轉檔單編號異常，請聯絡系統管理員！", "提示訊息", MessageBoxButton.OK);
                this.DialogResult = false;
            }
        }

        //實作-取得代碼檔_影片的檔案類型 
        void client_GetTBBROADCAST_CODE_TBFILE_TYPECompleted(object sender, GetTBBROADCAST_CODE_TBFILE_TYPECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null )
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE_TBFILE_TYPE CodeData = new Class_CODE_TBFILE_TYPE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            case "TBFILE_TYPE":      //塞入入庫檔案類型代碼，以及把音圖文的代碼濾掉

                                //只保留資料帶，SD資料帶、HD資料帶
                                if (CodeData.FSTYPE == "影" && CodeData.NAME.IndexOf("資料帶")>0)   
                                {
                                    ComboBoxItem cbiFileTYPE = new ComboBoxItem();
                                    cbiFileTYPE.Content = CodeData.NAME;
                                    cbiFileTYPE.Tag = CodeData.ID;
                                    this.cbVideo_TYPE.Items.Add(cbiFileTYPE);
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }        

        //實作-查詢入庫影像檔資料_透過檔案節目編號、集別及送帶轉檔單
        void client_GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted(object sender, GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    this.DGLogList.DataContext = e.Result;
                    ListLogVideo = e.Result;

                    if (this.DGLogList.DataContext != null)
                    {
                        DGLogList.SelectedIndex = 0;
                    }
                }
            }
        }

        //實作--不跑Flow新增送帶轉檔單 by Jarvis20130710
        void client_INSERT_BROADCAST_WITHOUT_FLOWCompleted(object sender, INSERT_BROADCAST_WITHOUT_FLOWCompletedEventArgs e)
        {
            if (e.Error == null && e.Result == true)
            {
                MessageBox.Show("新增送帶轉檔資料成功！", "提示訊息", MessageBoxButton.OK);
                this.DialogResult = true;
            }
            else
            {
                MessageBox.Show("呼叫流程引擎異常，新增送帶轉檔資料失敗！", "提示訊息", MessageBoxButton.OK);
                this.DialogResult = true;  //雖然流程失敗，但是因為已寫入還是要畫面重新Load 
            }
        }

        //實作-新增送帶轉檔單資料
        void client_INSERT_BROADCASTCompleted(object sender, INSERT_BROADCASTCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    //必須等待流程也成功才彈出成功訊息
                    //MessageBox.Show("新增送帶轉檔資料成功！", "提示訊息", MessageBoxButton.OK);
                    //this.DialogResult = true;
                    newAFlow();     //呼叫流程                               
                }
                else
                {
                    MessageBox.Show("新增送帶轉檔資料失敗！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
        }      

        //實作-刪除入庫影像檔及段落檔
        void client_DELETE_TBLOG_VIDEO_SEG_BYFILENOCompleted(object sender, DELETE_TBLOG_VIDEO_SEG_BYFILENOCompletedEventArgs e)
        {
          //此作業是取消送帶轉檔時觸發，因此不需要顯示訊息
        }   

        //實作-查詢入庫影像檔資料_透過檔案節目編號、集別
        void client_GetTBLOG_VIDEO_BYProgID_EpisodeCompleted(object sender, GetTBLOG_VIDEO_BYProgID_EpisodeCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                ListLogVideo.Clear();                               //查詢前都要先清空，DataGrid也要重新繫結
                this.DGLogList.DataContext = null;

                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)        //要濾掉檔案來源非外部轉入，如影帶管理系統
                    {
                        if (e.Result[i].FCFROM == "N")
                            ListLogVideo.Add(e.Result[i]);
                    }

                    this.DGLogList.DataContext = ListLogVideo;

                    if (this.DGLogList.DataContext != null)
                    {
                        DGLogList.SelectedIndex = 0;
                    }
                }
            }
        }       

        //實作-刪除入庫影像檔及段落檔_BY檔案編號
        void client_DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONECompleted(object sender, DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    MessageBox.Show("刪除段落資料成功！", "提示訊息", MessageBoxButton.OK);
                    LoadTBLOG_VIDEO();  //載入入庫影像檔
                }
                else
                    MessageBox.Show("刪除段落資料失敗！", "提示訊息", MessageBoxButton.OK);
            }
        }
        
        //實作-查詢TBLOG_VIDEOID_MAP的VideoID(資料帶先不考慮取VideoID，因為不會送到主控播出)
        //void client_QueryVideoID_PROG_STTCompleted(object sender, QueryVideoID_PROG_STTCompletedEventArgs e)
        //{
        //    if (e.Error == null)
        //    {
        //        if (e.Result != null)
        //        {
        //            m_VIDEOID_PROG = e.Result.Trim();

        //            //新增段落
        //            Log_Video_Seg();  
        //        }
        //    }
        //}

        //實作--流程起始
        void clientFlow_CallFlow_NewFlowWithFieldCompleted(object sender, CallFlow_NewFlowWithFieldCompletedEventArgs e)
        {
            if (e.Error == null && e.Result != "")
            {
                MessageBox.Show("新增送帶轉檔資料成功！", "提示訊息", MessageBoxButton.OK);
                this.DialogResult = true;
            }
            else
            {
               MessageBox.Show("呼叫流程引擎異常，新增送帶轉檔資料失敗！", "提示訊息", MessageBoxButton.OK);
               this.DialogResult = true;  //雖然流程失敗，但是因為已寫入還是要畫面重新Load 
            }            
        }

        #endregion

        #region 檢查畫面資料及搬值

        private Class_BROADCAST FormToClass_Bro()
        {
            Class_BROADCAST obj = new Class_BROADCAST();

            obj.FSBRO_ID = m_strBroID.Trim() ;                                      //送帶轉檔單號
            obj.FSBRO_TYPE = m_strType.Trim();                                      //類型
            obj.FSID = m_strID.Trim();                                              //編碼
            obj.FNEPISODE = Convert.ToInt16(m_strEpisode);                          //集別            
            obj.FDBRO_DATE = DateTime.Now;                                          //送帶轉檔申請日期
            obj.FSBRO_BY = UserClass.userData.FSUSER_ID.ToString();                 //送帶轉檔申請者
            if (STT100.GoFlow) //修改 by Jarvis20130710
            {
                obj.FCCHECK_STATUS = "N";                                               //審核狀態
            }
            else 
            {
                obj.FCCHECK_STATUS = "G";
                obj.FSSIGNED_BY = UserClass.userData.FSUSER_ID;
            }
            obj.FSCHECK_BY = "";                                                    //審核者
            obj.FCCHANGE = "N";                                                     //置換
            obj.FSMEMO = this.tbMEMO.Text;
            obj.FSCREATED_BY = UserClass.userData.FSUSER_ID.ToString();//建檔者
            obj.FSUPDATED_BY = UserClass.userData.FSUSER_ID.ToString();//修改者                     
            
            return obj;
        }
        #endregion 
   
        #region 按鈕裡的事件

        //按下確定後
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (m_strID == "")
            {
                MessageBox.Show("請先選擇資料帶", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (ListLogVideo.Count == 0)
            {
                MessageBox.Show("請先輸入至少一筆影片段落資料", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (ListLogVideo.Count > 0)
            {
                Boolean bolcheck = false;

                for (int i = 0; i < ListLogVideo.Count; i++)
                {
                    if (ListLogVideo[i].FSBRO_ID.Trim() == m_strBroID)
                        bolcheck = true;
                }

                if (bolcheck == false)  //判斷該筆送帶轉檔單至少輸入一筆
                {
                    MessageBox.Show("請先輸入至少一筆影片段落資料", "提示訊息", MessageBoxButton.OK);
                    return;
                }
            }

            OKButton.IsEnabled = false; //避免使用者按太快，把確定按鈕鎖上

            STT100_01_03 _stt100_01_03 = new STT100_01_03();
            _stt100_01_03.Show();
            _stt100_01_03.Closed += (s, args) =>
            {

                this.tbMEMO.Text += _stt100_01_03._fsTYPE + "\r\n";

                if (!string.IsNullOrEmpty(_stt100_01_03._fsFILE_PATH) && !string.IsNullOrEmpty(_stt100_01_03._fsTREE_PATH))
                {
                    this.tbMEMO.Text += "樹狀路徑：" + _stt100_01_03._fsTREE_PATH + "\r\n";
                    this.tbMEMO.Text += "實體路徑：" + _stt100_01_03._fsFILE_PATH;
                }


                if (STT100.GoFlow && _stt100_01_03.DialogResult == true)
                {
                    client.INSERT_BROADCASTAsync(FormToClass_Bro()); //新增送帶轉檔檔
                }
                else
                {
                    client.INSERT_BROADCAST_WITHOUT_FLOWAsync(FormToClass_Bro());
                }
            };
        }

        //取消時，要去檢查資料表後刪除
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            //刪除TBLOG_VIDEO 以及 TBLOG_VIDEO_SEG，原本是把整個File的List丟進去刪，
            //但是因為目前撈出來的資料是包括不同送帶帶轉檔的資料，不能刪掉這些資料，因此要先檔掉
            if (ListLogVideo.Count > 0)
            {
                List<Class_LOG_VIDEO> tempListLogVideo = new List<Class_LOG_VIDEO>();    //入庫影像檔集合

                for (int i = 0; i < ListLogVideo.Count; i++)
                {
                    if (ListLogVideo[i].FSBRO_ID.Trim() != "" && ListLogVideo[i].FSBRO_ID.Trim() == m_strBroID.Trim())
                    {
                        tempListLogVideo.Add(ListLogVideo[i]);
                    }                 
                }

                if (tempListLogVideo.Count>0)
                client.DELETE_TBLOG_VIDEO_SEG_BYFILENOAsync(tempListLogVideo);  
            }

            //if (m_strType == "G" && m_strID.Trim()!="") //原本預定取消時要取刪除主控播出提示資料Secondary Event，現在先註解
            //{              
            //    //TBPROG_D_INSERT_TAPE 以及 TBPROG_D_MONITER
            //    client.DELETE_TBPROG_D_MONITER_TAPE_BYPROGID_EPISODEAsync(m_strID, m_strEpisode, UserClass.userData.FSUSER_ID);
            //}

            this.DialogResult = false;
        }

        //開啟資料帶查詢/維護畫面
        private void btnPROG_Click(object sender, RoutedEventArgs e)
        {       
          STT100_08_01 STT100_08_01_frm = new STT100_08_01();      
          STT100_08_01_frm.Show();      

          STT100_08_01_frm.Closed += (s, args) =>      
          {      
              if (STT100_08_01_frm.DialogResult == true)      
              {      
                  //MessageBox.Show("編號：" + STT100_08_01_frm.strDATA_ID_View + "/n名稱：" + STT100_08_01_frm.strDATA_Name_View);      

                  if (STT100_08_01_frm.strDATA_ID_View.Trim() == "" || STT100_08_01_frm.strDATA_Name_View == "")      
                  {      
                      MessageBox.Show("資料帶選擇異常，請重新選擇資料帶", "提示訊息", MessageBoxButton.OK);      
                      return;      
                  }
                  
                  m_strID = STT100_08_01_frm.strDATA_ID_View.Trim();      
                  tbxDATA_NAME.Tag = m_strID;      
                  tbxDATA_NAME.Text = STT100_08_01_frm.strDATA_Name_View.Trim();
                  m_strCHANNELID = STT100_08_01_frm.strDATA_Channel_View.Trim();
                  m_strType = "D";      //類型為D，資料帶
                  m_strEpisode = "0";   //集別為0，第零集    

                  //透過節目編號去查詢入庫影像檔      
                  ListLogVideo.Clear();      
                  this.DGLogList.DataContext = null;                             
              }      
          };                 
        } 

        //開啟送帶轉檔，輸入TimeCode的介面
        private void btnSeg_Click(object sender, RoutedEventArgs e)
        {
            if (m_strID == "")
            {
                MessageBox.Show("請先選擇資料帶名稱", "提示訊息", MessageBoxButton.OK);
                return;
            }           

            if (cbVideo_TYPE.SelectedIndex != -1)                     //影片類型
            {
                m_strFileTypeName = ((ComboBoxItem)cbVideo_TYPE.SelectedItem).Content.ToString();
                m_strFileTypeID = ((ComboBoxItem)cbVideo_TYPE.SelectedItem).Tag.ToString();
            }
            else
            {
                MessageBox.Show("請先選擇影片類型", "提示訊息", MessageBoxButton.OK);
                return;
            }      

            //選定好資料帶後，要鎖定  
            btnDATA.IsEnabled = false;

            //檢查SD播出帶或是HD播出帶只能上傳一次
            for (int i = 0; i < ListLogVideo.Count; i++)
            {
                if (ListLogVideo[i].FSARC_TYPE.Trim() == "001" || ListLogVideo[i].FSARC_TYPE.Trim() == "002")
                {
                    //擋SD及HD播出帶時，也要只比對同一筆送帶轉檔單號，如果都擋掉，置換時就無法新增 ==> && ListLogVideo[i].FSBRO_ID.Trim().Equals(m_strBroID.Trim())
                    //這裡不是置換，因此全部的單號都要比對
                    if (ListLogVideo[i].FSARC_TYPE.Trim().Equals(m_strFileTypeID.Trim()) && ListLogVideo[i].FCFILE_STATUS.Trim() !="R")//除非是檔案狀態為轉檔失敗，才能重新提出
                    {
                        MessageBox.Show((ListLogVideo[i].FSARC_TYPE_NAME.Trim() + "只能上傳一次，請重新選擇影片類型！"), "提示訊息", MessageBoxButton.OK);
                        return;
                    }
                }
            }

            m_strCopyFSNO = ""; //新增時，要加修改或複製的檔案編號清空
            m_bolCopy = false;  //不是複製的段落

            m_VIDEOID_PROG ="";
            Log_Video_Seg();  //新增段落

            ////新增或複製前先取VideoID(資料帶先不考慮VideoID，因為資料帶不送到主控播出)
            //client.QueryVideoID_PROG_STTAsync(m_strID, m_strEpisode, m_strFileTypeID, false);
        }

        //修改段落檔
        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGLogList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預修改的資料", "提示訊息", MessageBoxButton.OK);
                return;
            }                        

            m_strFileTypeName  = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSARC_TYPE_NAME ;
            m_strFileTypeID  = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSARC_TYPE ;

            //當送帶轉檔單不同時，表示這不是同一次的送帶轉檔申請，因此無法修改
            if (m_strBroID.Trim() != ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSBRO_ID.Trim())  
            {
                MessageBox.Show("此筆並非為當次申請的段落資料，因此無法修改", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (((Class_LOG_VIDEO)DGLogList.SelectedItem).FCFILE_STATUS.Trim()!="B")
            {
                MessageBox.Show("此筆段落資料狀態不是「待轉檔」，因此無法修改", "提示訊息", MessageBoxButton.OK);
                return;
            }

            m_strCopyFSNO = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSFILE_NO ; //修改時，將要傳到下一頁的檔案編號補上
            m_VIDEOID_PROG = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSVIDEO_PROG.Trim();//若是修改，表示不用重新取VideoID
            m_bolCopy = false;  //不是複製的段落

            //修改段落
            Log_Video_Seg();          
        }

        //刪除段落檔
        private void btnDel_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGLogList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預刪除的段落資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //當送帶轉檔單不同時，表示這不是同一次的送帶轉檔申請，因此無法修改
            if (m_strBroID.Trim() != ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSBRO_ID.Trim())
            {
                MessageBox.Show("此筆並非為當次申請的段落資料，因此無法刪除", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (((Class_LOG_VIDEO)DGLogList.SelectedItem).FCFILE_STATUS.Trim() != "B")
            {
                MessageBox.Show("此筆段落資料狀態不是「待轉檔」，因此無法刪除", "提示訊息", MessageBoxButton.OK);
                return;
            }

            MessageBoxResult resultMsg = MessageBox.Show("確定要刪除「" + ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSARC_TYPE_NAME + "」的段落資料嗎?", "提示訊息", MessageBoxButton.OKCancel);
            if (resultMsg == MessageBoxResult.Cancel)
                return;
            else
                //刪除段落檔
                client.DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONEAsync(((Class_LOG_VIDEO)DGLogList.SelectedItem).FSFILE_NO, UserClass.userData.FSUSER_ID.ToString());           
        }

        //複製段落
        private void btnCopy_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGLogList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預複製的段落資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            if (cbVideo_TYPE.SelectedIndex != -1)                     //影片類型
            {
                m_strFileTypeName = ((ComboBoxItem)cbVideo_TYPE.SelectedItem).Content.ToString();
                m_strFileTypeID = ((ComboBoxItem)cbVideo_TYPE.SelectedItem).Tag.ToString();
            }
            else
            {
                MessageBox.Show("請先選擇影片類型", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //檢查SD播出帶或是HD播出帶只能上傳一次
            for (int i = 0; i < ListLogVideo.Count; i++)
            {
                if (ListLogVideo[i].FSARC_TYPE.Trim() == "001" || ListLogVideo[i].FSARC_TYPE.Trim() == "002")
                {
                    //擋SD及HD播出帶時，也要只比對同一筆送帶轉檔單號，如果都擋掉，置換時就無法新增 ==> && ListLogVideo[i].FSBRO_ID.Trim().Equals(m_strBroID.Trim())
                    //這裡不是置換，因此全部的單號都要比對
                    if (ListLogVideo[i].FSARC_TYPE.Trim().Equals(m_strFileTypeID.Trim()) && ListLogVideo[i].FCFILE_STATUS.Trim() != "R")//除非是檔案狀態為轉檔失敗，才能重新提出
                    {
                        MessageBox.Show((ListLogVideo[i].FSARC_TYPE_NAME.Trim() + "只能上傳一次，請重新選擇影片類型！"), "提示訊息", MessageBoxButton.OK);
                        return;
                    }
                }
            }

            m_strCopyFSNO = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSFILE_NO; //複製時，將要傳到下一頁的檔案編號補上
            m_bolCopy = true;  //複製段落

            m_VIDEOID_PROG = "";
            Log_Video_Seg();   //新增段落
        }

        //新增、修改、複製段落資料
        private void Log_Video_Seg()
        {           
            string strProgName = tbxDATA_NAME.Text.Trim();      //資料帶都給資料帶名稱
            string strEpisodeName = tbxDATA_NAME.Text.Trim();

            if (this.AudioTrackSetControl.GetAudioTrackSetting() == "")
            {
                MessageBox.Show("音軌設定為必填!");
                return;
            }
            STT100_01_01 STT100_01_01_frm = new STT100_01_01(m_strFileTypeName, m_strFileTypeID, m_strType, m_strID, m_strEpisode, m_strCHANNELID, m_strCopyFSNO, m_strBroID, strProgName, strEpisodeName, "", m_bolCopy, m_VIDEOID_PROG,this.AudioTrackSetControl.GetAudioTrackSetting());
            STT100_01_01_frm.Show();

            //若是有填入段落檔，就要去讀取該節目集別的段落資料，並且秀在畫面上
            STT100_01_01_frm.Closing += (s, args) =>
            {
                if (STT100_01_01_frm.DialogResult == true)
                {
                    //this.AudioTrackSetControl.IsEnabled = false;
                    LoadTBLOG_VIDEO();  //載入入庫影像檔
                }
            };
        }  

        #endregion   
     
        #region 流程引擎

        //以下程式將Flow放到Flow引擎中
        private void newAFlow()
        {
            string FlowNote = "資料帶名稱-" + tbxDATA_NAME.Text.ToString().Trim();

            StringBuilder sb = new StringBuilder();
            sb.Append(@"<VariableCollection>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>FormId</name>");
            sb.Append(@"    <value>" + tbxBro_ID.Text + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>NextXamlName</name>");
            sb.Append(@"    <value>/STT/STT100_05.xaml</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>Function01</name>");
            sb.Append(@"    <value>0</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>hid_SendTo</name>");      //預設是給1，不用審核的單子最後顯示才會是通過
            sb.Append(@"    <value>1</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>parameter01</name>");     //預設通過的狀態-Y(提出者是審核人)
            sb.Append(@"    <value>Y</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>USERID</name>");
            sb.Append(@"    <value>" + UserClass.userData.FSUSER_ID.ToString().Trim() + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>FormInfo</name>");
            sb.Append(@"    <value>" + FlowNote + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>CancelForm</name>");
            sb.Append(@"    <value>CanCancel</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"</VariableCollection>");

            //後端呼叫流程引擎
            clientFlow.CallFlow_NewFlowWithFieldAsync(6, UserClass.userData.FSUSER_ID.ToString().Trim(), sb.ToString());
        }

        #endregion

        #region 重要function

        //取得TimeCode區間
        private string CheckDuration()
        {
            string strReturn = "";
            for (int i = 0; i < ListLogVideo.Count; i++)
            {
                //SD播出帶或是HD播出帶，轉檔完成後要更新子集的時長
                if (ListLogVideo[i].FSARC_TYPE.Trim() == "001" || ListLogVideo[i].FSARC_TYPE.Trim() == "002")
                    strReturn = Convert.ToString(TransferTimecode.timecodetoSecond(ListLogVideo[i].FSEND_TIMECODE) - TransferTimecode.timecodetoSecond(ListLogVideo[i].FSBEG_TIMECODE)).Trim();
                return strReturn;
            }
            return "";
        }     
      
        //DataGrid變色
        private void ChildWindow_Loaded(object sender, RoutedEventArgs e)
        {
            DGLogList.LoadingRow += new EventHandler<DataGridRowEventArgs>(DGLogList_LoadingRow);
        }

        //實作-DataGrid變色
        void DGLogList_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Background = new SolidColorBrush(Colors.White);
            //e.Row.Foreground = new SolidColorBrush(Colors.Black);

            Class_LOG_VIDEO LOG_VIDEO = e.Row.DataContext as Class_LOG_VIDEO;

            if (LOG_VIDEO.FSBRO_ID.Trim() != m_strBroID.Trim())
            {
                e.Row.Background = new SolidColorBrush(Colors.LightGray);
                //e.Row.Foreground = new SolidColorBrush(Colors.Black);
            }                  
        }        

        #endregion
    }
}

