﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSBROADCAST;
using PTS_MAM3.PRG;
using PTS_MAM3.ProgData;

namespace PTS_MAM3.STT
{
    public partial class STT800_04 : ChildWindow
    {
        FlowFieldCom.FlowFieldComSoapClient client = new FlowFieldCom.FlowFieldComSoapClient();                 //產生新的代理類別 

        string m_strType = "";           //類型
        string m_strID = "";             //編碼
        string m_strEpisode = "";        //集別
        string m_strCHANNELID = "";      //頻道別   
        public List<FlowFieldCom.Class_BROADCAST> m_ListFormData = new List<FlowFieldCom.Class_BROADCAST>();  //記錄查詢到的資料

        public STT800_04()
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面

            rdbProg.IsChecked = true;
        }

        void InitializeForm() //初始化本頁面
        {
            //用類型、編碼、集別取得送帶轉檔檔
            //client.GetTBBROADCAST_BYIDEPISODECompleted += new EventHandler<GetTBBROADCAST_BYIDEPISODECompletedEventArgs>(client_GetTBBROADCAST_BYIDEPISODECompleted);

            //用類型、編碼、集別、新增者取得送帶轉檔檔
            //client.GetTBBROADCAST_BYIDEPISODE_BYALLUSERComplete += new EventHandler<GetTBBROADCAST_BYIDEPISODE_BYUSERIDCompletedEventArgs>(client_GetTBBROADCAST_BYIDEPISODE_BYUSERIDCompleted);
            //client.GetTBBROADCAST_BYIDEPISODE_BYUSERIDCompleted += new EventHandler<FlowFieldCom.GetTBBROADCAST_BYIDEPISODE_BYUSERIDCompletedEventArgs>(client_GetTBBROADCAST_BYIDEPISODE_BYUSERIDCompleted);
            client.GetTBBROADCAST_BYIDEPISODE_BYALLUSERCompleted += new EventHandler<FlowFieldCom.GetTBBROADCAST_BYIDEPISODE_BYALLUSERCompletedEventArgs>(client_GetTBBROADCAST_BYIDEPISODE_BYALLUSERCompleted);

            dpStart.Text = DateTime.Now.AddYears(-1).ToShortDateString();
            dpEnd.Text = DateTime.Now.ToShortDateString();
        }

        void client_GetTBBROADCAST_BYIDEPISODE_BYALLUSERCompleted(object sender, FlowFieldCom.GetTBBROADCAST_BYIDEPISODE_BYALLUSERCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)  //若是有查詢結果，則顯示，不然就請使用者重新查詢
                {
                    m_ListFormData = new List<FlowFieldCom.Class_BROADCAST>(e.Result);  //把取回來的結果，加上型別定義，才可以塞回去    
                    this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("查無送帶轉檔資料，請重新輸入條件查詢！", "提示訊息", MessageBoxButton.OK);
                    return;
                }
            }
        }

        //void client_GetTBBROADCAST_BYIDEPISODE_BYUSERIDCompleted(object sender, FlowFieldCom.GetTBBROADCAST_BYIDEPISODE_BYUSERIDCompletedEventArgs e)
        //{
        //    BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

        //    if (e.Error == null)
        //    {
        //        if (e.Result != null && e.Result.Count > 0)  //若是有查詢結果，則顯示，不然就請使用者重新查詢
        //        {
        //            m_ListFormData = new List<Class_BROADCAST>(e.Result);  //把取回來的結果，加上型別定義，才可以塞回去    
        //            this.DialogResult = true;
        //        }
        //        else
        //        {
        //            MessageBox.Show("查無送帶轉檔資料，請重新輸入條件查詢！", "提示訊息", MessageBoxButton.OK);
        //            return;
        //        }
        //    }
        //}

        //實作-用類型、編碼、集別取得送帶轉檔檔
        //void client_GetTBBROADCAST_BYIDEPISODECompleted(object sender, GetTBBROADCAST_BYIDEPISODECompletedEventArgs e)
        //{
        //    BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

        //    if (e.Error == null)
        //    {
        //        if (e.Result != null && e.Result.Count > 0)  //若是有查詢結果，則顯示，不然就請使用者重新查詢
        //        {
        //            m_ListFormData = new List<FlowFieldCom.Class_BROADCAST>(e.Result);  //把取回來的結果，加上型別定義，才可以塞回去    
        //            this.DialogResult = true;
        //        }
        //        else
        //        {
        //            MessageBox.Show("查無送帶轉檔資料，請重新輸入條件查詢！", "提示訊息", MessageBoxButton.OK);
        //            return;
        //        }
        //    }
        //}

        //實作-用類型、編碼、集別、新增者取得送帶轉檔檔
        void client_GetTBBROADCAST_BYIDEPISODE_BYUSERIDCompleted(object sender, GetTBBROADCAST_BYIDEPISODE_BYUSERIDCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)  //若是有查詢結果，則顯示，不然就請使用者重新查詢
                {
                    //m_ListFormData = new List<Class_BROADCAST>(e.Result);  //把取回來的結果，加上型別定義，才可以塞回去    
                    this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("查無送帶轉檔資料，請重新輸入條件查詢！", "提示訊息", MessageBoxButton.OK);
                    return;
                }
            }            
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (m_strType == "G" && m_strID == "")
            {
                MessageBox.Show("請先選擇節目名稱", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (m_strType == "P" && m_strID == "")
            {
                MessageBox.Show("請先選擇短帶名稱", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //用類型、編碼、集別取得送帶轉檔檔
            //client.GetTBBROADCAST_BYIDEPISODEAsync(m_strType, m_strID, m_strEpisode);

            //用類型、編碼、集別、新增者取得送帶轉檔檔
            client.GetTBBROADCAST_BYIDEPISODE_BYALLUSERAsync(m_strType, m_strID, m_strEpisode, Convert.ToDateTime(dpStart.Text), Convert.ToDateTime(dpEnd.Text).AddDays(1));
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //選取節目
        private void rdbProg_Checked(object sender, RoutedEventArgs e)
        {
            if (rdbProg != null && rdbProg.IsChecked == true)
            {
                m_strType = "G";
                tbPROG_NANE.Text = "節目名稱";
                tbxPROG_NAME.Text = "";
                tbxPROG_NAME.Tag = "";
                m_strID = "";
                m_strEpisode = "0";

                tbFNEPISODE.Visibility = Visibility.Visible;
                tbxEPISODE.Visibility = Visibility.Visible;
                btnEPISODE.Visibility = Visibility.Visible;
                tbEPISODE_NAME.Visibility = Visibility.Visible;
                lblEPISODE_NAME.Visibility = Visibility.Visible;               
            }
        }

        //選取短帶
        private void rdbPromo_Checked(object sender, RoutedEventArgs e)
        {
            if (rdbPromo.IsChecked == true)
            {
                m_strType = "P";
                tbPROG_NANE.Text = "短帶名稱";
                tbxPROG_NAME.Text = "";
                tbxPROG_NAME.Tag = "";
                tbxEPISODE.Text = "";
                lblEPISODE_NAME.Content = "";
                m_strID = "";
                m_strEpisode = "0";

                tbFNEPISODE.Visibility = Visibility.Collapsed;
                tbxEPISODE.Visibility = Visibility.Collapsed;
                btnEPISODE.Visibility = Visibility.Collapsed;
                tbEPISODE_NAME.Visibility = Visibility.Collapsed;
                lblEPISODE_NAME.Visibility = Visibility.Collapsed;                
            }
        }

        //查詢節目
        private void btnPROG_Click(object sender, RoutedEventArgs e)
        {
            if (rdbProg.IsChecked == true)
            {
                //PRG100_01
                PROGDATA_VIEW PROGDATA_VIEW_frm = new PROGDATA_VIEW();
                PROGDATA_VIEW_frm.Show();

                PROGDATA_VIEW_frm.Closed += (s, args) =>
                {
                    if (PROGDATA_VIEW_frm.DialogResult == true)
                    {
                        tbxPROG_NAME.Text = PROGDATA_VIEW_frm.strProgName_View;
                        tbxPROG_NAME.Tag = PROGDATA_VIEW_frm.strProgID_View;

                        tbxEPISODE.Text = "";
                        lblEPISODE_NAME.Content = "";
                        m_strEpisode = "0";                      
                        m_strCHANNELID = PROGDATA_VIEW_frm.strChannel_Id;
                        m_strID = PROGDATA_VIEW_frm.strProgID_View;  
                    }
                };
            }
            else if (rdbPromo.IsChecked == true)
            {
                //PRG800_07
                PRG800_07 PROMO_VIEW_frm = new PRG800_07();
                PROMO_VIEW_frm.Show();

                PROMO_VIEW_frm.Closed += (s, args) =>
                {
                    if (PROMO_VIEW_frm.DialogResult == true)
                    {
                        tbxPROG_NAME.Text = PROMO_VIEW_frm.strPromoName_View;
                        tbxPROG_NAME.Tag = PROMO_VIEW_frm.strPromoID_View;

                        tbxEPISODE.Text = "";
                        lblEPISODE_NAME.Content = "";
                        m_strEpisode = "0";
                        m_strCHANNELID = PROMO_VIEW_frm.strChannel_Id;
                        m_strID = PROMO_VIEW_frm.strPromoID_View;  
                    }
                };
            }
        }

        //查詢子集
        private void btnEPISODE_Click(object sender, RoutedEventArgs e)
        {
            if (tbxPROG_NAME.Text.ToString().Trim() != "" && tbxPROG_NAME.Tag.ToString().Trim() != "")
            {
                PRG.PRG200_07 PRG200_07_frm = new PRG.PRG200_07(tbxPROG_NAME.Tag.ToString().Trim(), tbxPROG_NAME.Text.ToString().Trim());
                PRG200_07_frm.Show();

                PRG200_07_frm.Closed += (s, args) =>
                {
                    if (PRG200_07_frm.DialogResult == true)
                    {
                        m_strEpisode = PRG200_07_frm.m_strEpisode_View;
                        tbxEPISODE.Text = PRG200_07_frm.m_strEpisode_View;
                        lblEPISODE_NAME.Content = PRG200_07_frm.m_strProgDName_View;                    
                    }
                };
            }
            else
                MessageBox.Show("請先選擇節目！", "提示訊息", MessageBoxButton.OK);
        }
    }
}

