﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSMAMFunctions;
using PTS_MAM3.ProgData;
using PTS_MAM3.WSBROADCAST;
using PTS_MAM3.PRG;
using System.Text;
using PTS_MAM3.FlowMAM;
using PTS_MAM3.WSPROG_M;
using System.Reflection;

namespace PTS_MAM3.STT
{
    /// <summary>
    /// 用來複製送帶轉檔單的
    /// </summary>
    public partial class STT100_10 : ChildWindow
    {
        WSBROADCASTSoapClient client = new WSBROADCASTSoapClient();                 //產生新的代理類別        
        WSPROG_M.WSPROG_MSoapClient programClient = new WSPROG_MSoapClient();
        WSPROMO.WSPROMOSoapClient promoClient = new WSPROMO.WSPROMOSoapClient();
        WSPROG_D.WSPROG_DSoapClient episodeClient = new WSPROG_D.WSPROG_DSoapClient();

        //WSPROG_PRODUCER.WSPROG_PRODUCERSoapClient prducerClient = new WSPROG_PRODUCER.WSPROG_PRODUCERSoapClient();

        WSMAMFunctionsSoapClient clientMAM = new WSMAMFunctionsSoapClient();
        // flowWebService.FlowSoapClient FlowClinet = new flowWebService.FlowSoapClient();//流程引擎  
        FlowMAMSoapClient clientFlow = new FlowMAMSoapClient();                     //後端的流程引擎

        public List<Class_LOG_VIDEO> ListLogVideo = new List<Class_LOG_VIDEO>();    //入庫影像檔集合
        private string m_2ndMemo = "";                                              //Secondary備註       

        string m_strFileTypeName = "";   //入庫檔案類型名稱
        string m_strFileTypeID = "";     //入庫檔案類型編號
        string m_strType = "";           //類型
        string m_strProgID = "";             //編碼
        string m_strEpisode = "";        //集別
        string m_strCHANNELID = "";      //頻道別   
        string m_strFileNO = "";         //檔案編號(修改時用的)
        string m_strBroID = "";          //送帶轉檔單單號
        string m_strProducer = "";       //製作人
        string m_strDep_ID = "";         //成案單位
        string m_VIDEOID_PROG = "";      //VideoID
        string m_strCopyFSNO = "";       //複製或修改段落的檔案編號
        Boolean m_bolCopy = false;       //是否是複製的段落
        int intCountFlow = 0;            //判斷流程若是失敗重複呼叫
        //Class_BROADCAST OriginalBraocast;
        Class_PROGM programData;
        WSPROMO.Class_PROMO promoData;
        public STT100_10(Class_BROADCAST oriBraocast, List<Class_LOG_VIDEO> iniVideoList)
        {
            InitializeComponent();
            ListLogVideo = iniVideoList;//複製原始頁面的TC
            //OriginalBraocast = oriBraocast;
            m_strType = oriBraocast.FSBRO_TYPE;    //類型為節目G或是廣告P
            m_strProgID = oriBraocast.FSID;
            m_strEpisode = oriBraocast.SHOW_FNEPISODE;
            m_strCHANNELID = oriBraocast.FSCHANNELID;
            m_strBroID = oriBraocast.FSBRO_ID;

            InitializeForm();          //初始化本頁面

            if (m_strType == "G")
            {
                programClient.fnGetProg_MAsync(m_strProgID);
            }
            else if (m_strType == "P") 
            {
                promoClient.GetTBPGM_PROMO_BYPROMONAMEAsync(oriBraocast.FSID_NAME);
            }
            //rdbProg.IsChecked = true;  //預設節目被勾起
        }

        private void InitCopy(string TapeType)
        {
            if (TapeType == "P")
            {
                #region 短帶的初始設定
                m_strType = "P";
                tbPROG_NANE.Text = "短帶名稱";
                //tbxPROG_NAME.Text = "";
                //tbxPROG_NAME.Tag = "";
                //tbxEPISODE.Text = "";
                //lblEPISODE_NAME.Content = "";
                //tbxTOTEPISODE.Text = "";
                //tbxLENGTH.Text = "";
                //m_strID = "";
                //m_strEpisode = "0";
                rdbPromo.IsChecked = true;
                Clear_listCHANNEL();
                //for (int j = 0; j < listCHANNEL.Items.Count; j++)   //把可播映頻道裡的資料清掉
                //    ((CheckBox)listCHANNEL.Items.ElementAt(j)).IsChecked = false;

                tbFNEPISODE.Visibility = Visibility.Collapsed;
                tbxEPISODE.Visibility = Visibility.Collapsed;
                btnEPISODE.Visibility = Visibility.Collapsed;
                tbEPISODE_NAME.Visibility = Visibility.Collapsed;
                lblEPISODE_NAME.Visibility = Visibility.Collapsed;
                tbCHANNEL.Visibility = Visibility.Collapsed;
                listCHANNEL.Visibility = Visibility.Collapsed;
                tbTOTEPISODE.Visibility = Visibility.Collapsed;
                tbxTOTEPISODE.Visibility = Visibility.Collapsed;
                tbLENGTH.Visibility = Visibility.Collapsed;
                tbxLENGTH.Visibility = Visibility.Collapsed;
                //btnEvent.Visibility = Visibility.Collapsed;       //使用者要求，先隱藏
                btnSet.Visibility = Visibility.Collapsed;
                this.audioTrackControl1.Visibility = System.Windows.Visibility.Collapsed;
                this.tbAudio_Track.Visibility = System.Windows.Visibility.Collapsed;
                #endregion
            }
            else if (TapeType == "G")
            {
                #region 節目的初始設定
                m_strType = "G";
                tbPROG_NANE.Text = "節目名稱";
                //tbxPROG_NAME.Text = "";
                //tbxPROG_NAME.Tag = "";
                //m_strID = "";
                //m_strEpisode = "0";

                rdbProg.IsChecked = true;//節目的選項
                tbFNEPISODE.Visibility = Visibility.Visible;
                tbxEPISODE.Visibility = Visibility.Visible;
                btnEPISODE.Visibility = Visibility.Visible;
                tbEPISODE_NAME.Visibility = Visibility.Visible;
                lblEPISODE_NAME.Visibility = Visibility.Visible;
                tbCHANNEL.Visibility = Visibility.Visible;
                listCHANNEL.Visibility = Visibility.Visible;
                tbTOTEPISODE.Visibility = Visibility.Visible;
                tbxTOTEPISODE.Visibility = Visibility.Visible;
                tbLENGTH.Visibility = Visibility.Visible;
                tbxLENGTH.Visibility = Visibility.Visible;
                //btnEvent.Visibility = Visibility.Visible;         //使用者要求，先隱藏
                btnSet.Visibility = Visibility.Visible;
                #endregion
            }
            rdbProg.IsEnabled = false;
            rdbPromo.IsEnabled = false;
        }

        void InitializeForm() //初始化本頁面
        {
            //取得送帶轉檔單編號
            clientMAM.GetNoRecordCompleted += new EventHandler<GetNoRecordCompletedEventArgs>(ClientMAM_GetNoRecordCompleted);
            clientMAM.GetNoRecordAsync("07", "", "", UserClass.userData.FSUSER_ID);

            //下載代碼檔_影片的檔案類型  
            client.GetTBBROADCAST_CODE_TBFILE_TYPECompleted += new EventHandler<GetTBBROADCAST_CODE_TBFILE_TYPECompletedEventArgs>(client_GetTBBROADCAST_CODE_TBFILE_TYPECompleted);
            client.GetTBBROADCAST_CODE_TBFILE_TYPEAsync();

            //下載代碼檔
            client.fnGetTBBROADCAST_CODECompleted += new EventHandler<fnGetTBBROADCAST_CODECompletedEventArgs>(client_fnGetTBBROADCAST_CODECompleted);
            client.fnGetTBBROADCAST_CODEAsync();

            //查詢入庫影像檔資料_透過檔案節目編號、集別、送帶轉檔單            
            client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted += new EventHandler<GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompletedEventArgs>(client_GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted);

            //查詢入庫影像檔資料_透過檔案節目編號、集別(因為不檢查子集先註解掉)
            //client.GetTBLOG_VIDEO_BYProgID_EpisodeCompleted += new EventHandler<GetTBLOG_VIDEO_BYProgID_EpisodeCompletedEventArgs>(client_GetTBLOG_VIDEO_BYProgID_EpisodeCompleted);

            //修改節目子集的2ND及段落區間
            client.UPDATE_TBPROG_D_ND_DURATIONCompleted += new EventHandler<UPDATE_TBPROG_D_ND_DURATIONCompletedEventArgs>(client_UPDATE_TBPROG_D_ND_DURATIONCompleted);

            //查詢節目子集是否有2ND
            client.Query_TBPROG_D_NDCompleted += new EventHandler<Query_TBPROG_D_NDCompletedEventArgs>(client_Query_TBPROG_D_NDCompleted);

            //查詢節目子集的主控播出提示資料的TBPROG_D_MONITER資料
            client.Query_TBPROG_D_MONITERCompleted += new EventHandler<Query_TBPROG_D_MONITERCompletedEventArgs>(client_Query_TBPROG_D_MONITERCompleted);

            //新增送帶轉檔單資料
            client.INSERT_BROADCASTCompleted += new EventHandler<INSERT_BROADCASTCompletedEventArgs>(client_INSERT_BROADCASTCompleted);

            //刪除入庫影像檔及段落檔
            client.DELETE_TBLOG_VIDEO_SEG_BYFILENOCompleted += new EventHandler<DELETE_TBLOG_VIDEO_SEG_BYFILENOCompletedEventArgs>(client_DELETE_TBLOG_VIDEO_SEG_BYFILENOCompleted);

            //刪除主控插播帶及主控鏡面
            client.DELETE_TBPROG_D_MONITER_TAPE_BYPROGID_EPISODECompleted += new EventHandler<DELETE_TBPROG_D_MONITER_TAPE_BYPROGID_EPISODECompletedEventArgs>(client_DELETE_TBPROG_D_MONITER_TAPE_BYPROGID_EPISODECompleted);

            //查詢節目子集檔的主控播出提示資料
            client.Query_TBPROG_INSERT_TAPECompleted += new EventHandler<Query_TBPROG_INSERT_TAPECompletedEventArgs>(client_Query_TBPROG_INSERT_TAPECompleted);

            //用節目編號、節別查詢48小時內是否有排播
            client.GetPROG_PLAY_NOTECompleted += new EventHandler<GetPROG_PLAY_NOTECompletedEventArgs>(client_GetPROG_PLAY_NOTECompleted);

            //用短帶編號查詢48小時內是否有排播
            client.GetPROMO_PLAY_NOTECompleted += new EventHandler<GetPROMO_PLAY_NOTECompletedEventArgs>(client_GetPROMO_PLAY_NOTECompleted);

            //刪除入庫影像檔及段落檔_BY檔案編號
            client.DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONECompleted += new EventHandler<DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONECompletedEventArgs>(client_DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONECompleted);

            //查詢TBLOG_VIDEOID_MAP的VideoID
            client.QueryVideoID_PROG_STTCompleted += new EventHandler<QueryVideoID_PROG_STTCompletedEventArgs>(client_QueryVideoID_PROG_STTCompleted);

            //流程引擎
            //FlowClinet.NewFlowWithFieldCompleted += new EventHandler<flowWebService.NewFlowWithFieldCompletedEventArgs>(FlowClinet_NewFlowWithFieldCompleted);

            //後端的流程引擎-流程起始
            clientFlow.CallFlow_NewFlowWithFieldCompleted += new EventHandler<CallFlow_NewFlowWithFieldCompletedEventArgs>(clientFlow_CallFlow_NewFlowWithFieldCompleted);

            //限定SD播出帶只能送出一張 判斷是否可新增一張送帶轉檔單的完成處理
            client.Query_CheckIsAbleToCreateNewTransformFormCompleted += new EventHandler<Query_CheckIsAbleToCreateNewTransformFormCompletedEventArgs>(client_Query_CheckIsAbleToCreateNewTransformFormCompleted);

            client.Set_Class_LOG_VIDEO_AND_SEGCompleted += new EventHandler<Set_Class_LOG_VIDEO_AND_SEGCompletedEventArgs>(client_Set_Class_LOG_VIDEO_AND_SEGCompleted);

            //完成後要結束畫面
            client.INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLISTCompleted += new EventHandler<INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLISTCompletedEventArgs>(client_INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLISTCompleted);

            programClient.fnGetProg_MCompleted += new EventHandler<fnGetProg_MCompletedEventArgs>(programClient_fnGetProg_MCompleted);
            episodeClient.GetProg_DCompleted += new EventHandler<WSPROG_D.GetProg_DCompletedEventArgs>(episodeClient_GetProg_DCompleted);
            promoClient.GetTBPGM_PROMO_BYPROMONAMECompleted += new EventHandler<WSPROMO.GetTBPGM_PROMO_BYPROMONAMECompletedEventArgs>(promoClient_GetTBPGM_PROMO_BYPROMONAMECompleted);
            //prducerClient.QUERY_TBPROG_PRODUCERCompleted += new EventHandler<WSPROG_PRODUCER.QUERY_TBPROG_PRODUCERCompletedEventArgs>(prducerClient_QUERY_TBPROG_PRODUCERCompleted);
        }

        void promoClient_GetTBPGM_PROMO_BYPROMONAMECompleted(object sender, WSPROMO.GetTBPGM_PROMO_BYPROMONAMECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result.Count > 0)
                {
                    promoData = e.Result[0];

                    checkPromo(promoData.FSPROMO_ID, promoData.FSPROMO_NAME, m_strCHANNELID, promoData.FNDEP_ID.ToString(), "");

                }
            }
        }

        void programClient_fnGetProg_MCompleted(object sender, fnGetProg_MCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result.Count > 0)
                {
                    programData = e.Result[0];

                    episodeClient.GetProg_DAsync(programData.FSPROG_ID,m_strEpisode);
                }
            }
        }

        void episodeClient_GetProg_DCompleted(object sender, WSPROG_D.GetProg_DCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result.Count > 0)
                {
                    PTS_MAM3.WSPROG_D.Class_PROGD episodeData = e.Result[0];
                    m_strEpisode = episodeData.SHOW_FNEPISODE;
                    tbxEPISODE.Text = episodeData.SHOW_FNEPISODE;
                    lblEPISODE_NAME.Content = episodeData.FNEPISODE_NAME;

                    //載入節目資料
                    checkProg(programData.FSPROG_ID, programData.FSPGMNAME, programData.FSCHANNEL_ID, programData.FNDEP_ID.ToString(), m_strProducer, programData.FNTOTEPISODE.ToString(), programData.FSCHANNEL, programData.FNLENGTH.ToString());

                    //透過節目編號去查詢入庫影像檔
                    //ListLogVideo.Clear(); 
                    //this.DGLogList.DataContext = null;
                    //client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDAsync("G", m_strID, m_strEpisode, m_strBroID);//避免重複上傳，只用節目編號加集別去查，不用送帶轉檔單去查
                    //client.GetTBLOG_VIDEO_BYProgID_EpisodeAsync("G", m_strID, m_strEpisode);                  //新增送帶轉檔資料時，不去檢查該子集曾經送過的
                    //todo:加入程式
                    //找48小時有無排播記錄
                   
                        client.GetPROG_PLAY_NOTEAsync(m_strProgID, m_strEpisode);
                }
            }
        }

        //void prducerClient_QUERY_TBPROG_PRODUCERCompleted(object sender, WSPROG_PRODUCER.QUERY_TBPROG_PRODUCERCompletedEventArgs e)
        //{
        //    if (e.Error == null)
        //    {
        //        string producer = "";
        //        if (e.Result.Count > 0)
        //        {
        //            producer = e.Result[0].FSPRODUCER;
        //        }
        //        else
        //        {
        //            producer = "";
        //        }

        //    }
        //}

       

        void client_INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLISTCompleted(object sender, INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLISTCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (STT100.GoFlow) //修改 By Jarvis20130808 
                {
                    newAFlow();
                }
                else
                {
                    if (e.Result == true)
                    {
                        MessageBox.Show("新增成功");
                        this.DialogResult = true;
                    }
                    else
                    {
                        MessageBox.Show("複製送帶轉檔TC新增失敗");
                        this.DialogResult = false;
                    }
                }
            }
        }




        //載入入庫影像檔
        private void LoadTBLOG_VIDEO()
        {
            if (rdbProg.IsChecked == true)
            {
                if (m_strProgID != "" && m_strEpisode != "")
                    client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDAsync("G", m_strProgID, m_strEpisode, m_strBroID);
                //client.GetTBLOG_VIDEO_BYProgID_EpisodeAsync("G", m_strID, m_strEpisode);          //不擋子集有一筆以上的SD播出帶或HD播出帶
                else if (m_strProgID != "" && m_strEpisode == "")
                    client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDAsync("G", m_strProgID, "0", m_strBroID);
                //client.GetTBLOG_VIDEO_BYProgID_EpisodeAsync("G", m_strID, "0");                   //不擋子集有一筆以上的SD播出帶或HD播出帶
            }
            else if (rdbPromo.IsChecked == true)
                client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDAsync("P", m_strProgID, "0", m_strBroID);
            //client.GetTBLOG_VIDEO_BYProgID_EpisodeAsync("P", m_strID, "0");                       //不擋子集有一筆以上的SD播出帶或HD播出帶
        }

        #region 實作

        //實作-取得送帶轉檔單編號
        void ClientMAM_GetNoRecordCompleted(object sender, GetNoRecordCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    tbxBro_ID.Text = e.Result;
                    m_strBroID = e.Result;
                }
                else
                {
                    MessageBox.Show("取得送帶轉檔單編號異常，請聯絡系統管理員！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
            else
            {
                MessageBox.Show("取得送帶轉檔單編號異常，請聯絡系統管理員！", "提示訊息", MessageBoxButton.OK);
                this.DialogResult = false;
            }
            InitCopy(m_strType);
        }

        //實作-取得代碼檔_影片的檔案類型 
        void client_GetTBBROADCAST_CODE_TBFILE_TYPECompleted(object sender, GetTBBROADCAST_CODE_TBFILE_TYPECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE_TBFILE_TYPE CodeData = new Class_CODE_TBFILE_TYPE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            case "TBFILE_TYPE":      //塞入入庫檔案類型代碼，以及把音圖文的代碼濾掉
                                int IOF = CodeData.NAME.IndexOf("已停用");
                                if (CodeData.FSTYPE == "影" && !(IOF > 0))
                                {
                                    ComboBoxItem cbiFileTYPE = new ComboBoxItem();
                                    cbiFileTYPE.Content = CodeData.NAME;
                                    cbiFileTYPE.Tag = CodeData.ID;
                                    this.cbVideo_TYPE.Items.Add(cbiFileTYPE);
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

        //實作-取得代碼檔
        void client_fnGetTBBROADCAST_CODECompleted(object sender, fnGetTBBROADCAST_CODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        PTS_MAM3.WSBROADCAST.Class_CODE CodeData = new PTS_MAM3.WSBROADCAST.Class_CODE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            case "TBZCHANNEL":       //頻道別代碼
                                CheckBox cbCHANNEL = new CheckBox();
                                cbCHANNEL.Content = CodeData.NAME;
                                cbCHANNEL.Tag = CodeData.ID;
                                cbCHANNEL.IsEnabled = false;
                                listCHANNEL.Items.Add(cbCHANNEL);
                                break;

                            default:
                                break;
                        }
                    }
                }
            }
        }

        //實作-查詢入庫影像檔資料_透過檔案節目編號、集別及送帶轉檔單
        void client_GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted(object sender, GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    this.DGLogList.DataContext = e.Result;
                    ListLogVideo = e.Result;

                    if (this.DGLogList.DataContext != null)
                    {
                        DGLogList.SelectedIndex = 0;
                    }
                }
            }
        }

        //實作-新增送帶轉檔單資料
        void client_INSERT_BROADCASTCompleted(object sender, INSERT_BROADCASTCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    //必須等待流程也成功才彈出成功訊息
                    //MessageBox.Show("新增送帶轉檔資料成功！", "提示訊息", MessageBoxButton.OK);
                    //this.DialogResult = true;
                    newAFlow();     //呼叫流程                  
                }
                else
                {
                    MessageBox.Show("新增送帶轉檔資料失敗！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
        }

        //實作-修改節目子集檔的2nd及期間
        void client_UPDATE_TBPROG_D_ND_DURATIONCompleted(object sender, UPDATE_TBPROG_D_ND_DURATIONCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    //MessageBox.Show("更新節目子集檔的2nd及期間成功！", "提示訊息", MessageBoxButton.OK);                   
                }
                else
                {
                    //MessageBox.Show("更新節目子集檔的2nd及期間失敗！", "提示訊息", MessageBoxButton.OK);
                }
                client.INSERT_BROADCASTAsync(FormToClass_Bro());   //修改節目子集檔的2nd及期間後才去新增送帶轉檔檔
            }
        }

        //實作-刪除入庫影像檔及段落檔
        void client_DELETE_TBLOG_VIDEO_SEG_BYFILENOCompleted(object sender, DELETE_TBLOG_VIDEO_SEG_BYFILENOCompletedEventArgs e)
        {
            //此作業是取消送帶轉檔時觸發，因此不需要顯示訊息
        }

        //實作-刪除主控插播帶及主控鏡面
        void client_DELETE_TBPROG_D_MONITER_TAPE_BYPROGID_EPISODECompleted(object sender, DELETE_TBPROG_D_MONITER_TAPE_BYPROGID_EPISODECompletedEventArgs e)
        {
            //因為是去檢查有無值，有就刪除，因此不接受回傳值
        }

        //實作-查詢節目子集是否有2ND
        void client_Query_TBPROG_D_NDCompleted(object sender, Query_TBPROG_D_NDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    call2nd(e.Result.Trim());    //呼叫2ND
                }
            }
        }

        //實作-查詢入庫影像檔資料_透過檔案節目編號、集別
        void client_GetTBLOG_VIDEO_BYProgID_EpisodeCompleted(object sender, GetTBLOG_VIDEO_BYProgID_EpisodeCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                ListLogVideo.Clear();                               //查詢前都要先清空，DataGrid也要重新繫結
                this.DGLogList.DataContext = null;

                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)        //要濾掉檔案來源非外部轉入，如影帶管理系統
                    {
                        if (e.Result[i].FCFROM == "N")
                            ListLogVideo.Add(e.Result[i]);
                    }

                    this.DGLogList.DataContext = ListLogVideo;

                    if (this.DGLogList.DataContext != null)
                    {
                        DGLogList.SelectedIndex = 0;
                    }
                }
            }
        }

        //實作-查詢節目子集檔的主控播出提示資料
        void client_Query_TBPROG_INSERT_TAPECompleted(object sender, Query_TBPROG_INSERT_TAPECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == false)          //回傳false表示無資料，要詢問使用者
                {
                    MessageBoxResult resultMsg = MessageBox.Show("沒有設定主控播出提示資料，\n按下「確定」繼續新增，\n按下「取消」回到畫面繼續編輯。", "提示訊息", MessageBoxButton.OKCancel);
                    if (resultMsg == MessageBoxResult.OK)
                        UpdateND_DURATION();
                }
                else
                    UpdateND_DURATION();        //有資料，不詢問使用者，更新送帶轉檔資料                  
            }
        }

        //實作-用節目編號、節別查詢48小時內是否有排播
        void client_GetPROG_PLAY_NOTECompleted(object sender, GetPROG_PLAY_NOTECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.FBSW == true)
                    MessageBox.Show("播出日期:" + e.Result.FDDATE + " 時間:" + e.Result.FSPLAYTIME + " 僅剩:" + e.Result.FSNOTE, "提示訊息", MessageBoxButton.OK);

                

                client.Set_Class_LOG_VIDEO_AND_SEGAsync(m_strType, m_strProgID, m_strEpisode, UserClass.userData.FSUSER_ID, m_strCHANNELID, programData.FSPGMNAME, m_strEpisode, m_strBroID, ListLogVideo);

                //prducerClient.QUERY_TBPROG_PRODUCERAsync(new WSPROG_PRODUCER.Class_PROGPRODUCER() { FSPRO_TYPE = OriginalBraocast.FSBRO_TYPE, FSID = programData.FSPROG_ID, FSPRODUCER = "" });
            }
        }

        //用短帶編號查詢48小時內是否有排播
        void client_GetPROMO_PLAY_NOTECompleted(object sender, GetPROMO_PLAY_NOTECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.FBSW == true)
                    MessageBox.Show("播出日期:" + e.Result.FDDATE + " 時間:" + e.Result.FSPLAYTIME + " 僅剩:" + e.Result.FSNOTE, "提示訊息", MessageBoxButton.OK);


                client.Set_Class_LOG_VIDEO_AND_SEGAsync(m_strType, m_strProgID, m_strEpisode, UserClass.userData.FSUSER_ID, m_strCHANNELID, promoData.FSPROMO_NAME, m_strEpisode, m_strBroID, ListLogVideo);

                //prducerClient.QUERY_TBPROG_PRODUCERAsync(new WSPROG_PRODUCER.Class_PROGPRODUCER() { FSPRO_TYPE = OriginalBraocast.FSBRO_TYPE, FSID = programData.FSPROG_ID, FSPRODUCER = "" });
            }
        }

        //實作-刪除入庫影像檔及段落檔_BY檔案編號
        void client_DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONECompleted(object sender, DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    MessageBox.Show("刪除段落資料成功！", "提示訊息", MessageBoxButton.OK);
                    LoadTBLOG_VIDEO();  //載入入庫影像檔
                }
                else
                    MessageBox.Show("刪除段落資料失敗！", "提示訊息", MessageBoxButton.OK);
            }
        }

        //查詢節目子集的主控播出提示資料的TBPROG_D_MONITER資料
        void client_Query_TBPROG_D_MONITERCompleted(object sender, Query_TBPROG_D_MONITERCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == false)
                {
                    MessageBoxResult resultMsg = MessageBox.Show("沒有設定主控播出提示資料，\n按下「確定」繼續新增，\n按下「取消」回到畫面繼續編輯。", "提示訊息", MessageBoxButton.OKCancel);
                    if (resultMsg == MessageBoxResult.OK)
                        UpdateND_DURATION();
                    else
                        OKButton.IsEnabled = true;
                }
                else
                    UpdateND_DURATION();
            }
        }

        //實作-查詢TBLOG_VIDEOID_MAP的VideoID
        void client_QueryVideoID_PROG_STTCompleted(object sender, QueryVideoID_PROG_STTCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    m_VIDEOID_PROG = e.Result.Trim();

                    //新增段落
                    Log_Video_Seg();
                }
            }
        }

        //實作--流程起始
        void clientFlow_CallFlow_NewFlowWithFieldCompleted(object sender, CallFlow_NewFlowWithFieldCompletedEventArgs e)
        {
            if (e.Error == null && e.Result != "")
            {
                MessageBox.Show("新增送帶轉檔資料成功！", "提示訊息", MessageBoxButton.OK);
                this.DialogResult = true;
            }
            else
            {
                MessageBox.Show("呼叫流程引擎異常，新增送帶轉檔資料失敗！", "提示訊息", MessageBoxButton.OK);
                this.DialogResult = true;  //雖然流程失敗，但是因為已寫入還是要畫面重新Load 
            }
        }

        //實做-複製過來的TC複製 事件處理完成
        void client_Set_Class_LOG_VIDEO_AND_SEGCompleted(object sender, Set_Class_LOG_VIDEO_AND_SEGCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                ListLogVideo = e.Result;
                DGLogList.ItemsSource = ListLogVideo;
                //MessageBox.Show("複製資料成功！", "提示訊息", MessageBoxButton.OK);
                //btnSeg.IsEnabled = true;
                //btnEdit.IsEnabled = true;
                //btnDel.IsEnabled = true;
                //OKButton.IsEnabled = true;
                //btnCopy.IsEnabled = false;
                //btnPROG.IsEnabled = false;      //取回TC後不能再改節目
                //btnEPISODE.IsEnabled = false;   //取回TC後不能再改子集
            }
        }

        #endregion

        #region 比對

        private string[] CheckList(string strChannel)
        {
            char[] delimiterChars = { ';' };
            string[] words = strChannel.Split(delimiterChars);
            return words;
        }

        //比對代碼檔_可播映頻道
        void compareCode_FSCHANNEL(string[] ListTEST_Send)
        {
            for (int i = 0; i < ListTEST_Send.Length - 1; i++)
            {
                for (int j = 0; j < listCHANNEL.Items.Count; j++)
                {
                    CheckBox getcki = (CheckBox)listCHANNEL.Items.ElementAt(j);
                    if (getcki.Tag.ToString().Trim().Equals(ListTEST_Send[i]))
                    {
                        getcki.IsChecked = true;
                        break;
                    }
                }
            }
        }

        //清空_可播映頻道
        void Clear_listCHANNEL()
        {
            for (int j = 0; j < listCHANNEL.Items.Count; j++)
            {
                CheckBox getcki = (CheckBox)listCHANNEL.Items.ElementAt(j);
                getcki.IsChecked = false;
            }
        }

        #endregion

        #region 檢查畫面資料及搬值

        private Class_BROADCAST FormToClass_Bro()
        {
            Class_BROADCAST obj = new Class_BROADCAST();

            obj.FSBRO_ID = m_strBroID.Trim();                                      //送帶轉檔單號
            obj.FSBRO_TYPE = m_strType.Trim();                                      //類型
            obj.FSID = m_strProgID.Trim();                                              //編碼
            obj.FNEPISODE = Convert.ToInt16(m_strEpisode);     //集別            
            obj.FDBRO_DATE = DateTime.Now;                                          //送帶轉檔申請日期
            obj.FSBRO_BY = UserClass.userData.FSUSER_ID.ToString();                 //送帶轉檔申請者
            if (STT100.GoFlow)//by Jarvis 20130808
            {
                obj.FCCHECK_STATUS = "N";                                                //審核狀態
            }
            else
            {
                obj.FCCHECK_STATUS = "G";
                obj.FSSIGNED_BY = UserClass.userData.FSUSER_ID;
            }
            obj.FSCHECK_BY = "";                                                    //審核者
            obj.FCCHANGE = "N";                                                     //置換
            obj.FSMEMO = tbMEMO.Text.Trim();                                        //加入MEMO欄位描述
            obj.FSCREATED_BY = UserClass.userData.FSUSER_ID.ToString();//建檔者
            obj.FSUPDATED_BY = UserClass.userData.FSUSER_ID.ToString();//修改者                     

            return obj;
        }

        #endregion

        #region 按鈕裡的事件

        //按下確定後
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (m_strType == "G" && m_strProgID == "")
            {
                MessageBox.Show("請先選擇節目名稱", "提示訊息", MessageBoxButton.OK);
                return;
            }
            //else if (m_strType == "G" && m_strID != "" && m_strEpisode == "0")
            //{
            //    MessageBox.Show("請先選擇子集名稱", "提示訊息", MessageBoxButton.OK);
            //    return;
            //}
            else if (m_strType == "P" && m_strProgID == "")
            {
                MessageBox.Show("請先選擇短帶名稱", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (ListLogVideo.Count == 0)
            {
                MessageBox.Show("請先輸入至少一筆影片段落資料", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (ListLogVideo.Count > 0)
            {
                Boolean bolcheck = false;

                for (int i = 0; i < ListLogVideo.Count; i++)
                {
                    if (ListLogVideo[i].FSBRO_ID.Trim() == m_strBroID)
                        bolcheck = true;
                }

                if (bolcheck == false)  //判斷該筆送帶轉檔單至少輸入一筆
                {
                    MessageBox.Show("請先輸入至少一筆影片段落資料", "提示訊息", MessageBoxButton.OK);
                    return;
                }
            }

            OKButton.IsEnabled = false; //避免使用者按太快，把確定按鈕鎖上

            //判斷SD播出帶 HD播出帶 的送帶轉檔單是否可新增 by Kyle 2012/07/27
            if (ListLogVideo.Where(A1 => A1.FSARC_TYPE == "001").Count() > 0 || ListLogVideo.Where(A2 => A2.FSARC_TYPE == "002").Count() > 0) //ListLogVideo.Select(A1 => A1.FSARC_TYPE == "001").Count() > 0 || ListLogVideo.Select(A2 => A2.FSARC_TYPE == "002").Count() > 0
            {
                client.Query_CheckIsAbleToCreateNewTransformFormAsync(m_strProgID.Trim(), m_strEpisode.Trim(), ListLogVideo);
            }
            else
            {
                //至軒原本的CODE
                if (m_strProgID.Trim() != "" && m_strEpisode.Trim() != "0")
                {
                    client.Query_TBPROG_D_MONITERAsync(m_strProgID.Trim(), m_strEpisode.Trim());        //檢查是否有填Moniter資料，沒有填要提醒
                    //client.Query_TBPROG_D_INSERT_TAPEAsync(m_strID.Trim(), m_strEpisode.Trim());  //查詢節目子集檔的主控播出提示資料，改成檢查上面的Moniter資料
                }
                else
                    UpdateND_DURATION();    //若是非節目加集別，像是以節目或是短帶送帶轉檔的就直接新增
            }
        }

        void client_Query_CheckIsAbleToCreateNewTransformFormCompleted(object sender, Query_CheckIsAbleToCreateNewTransformFormCompletedEventArgs e)
        {
            if (e.Result == "True")
            {
                //if (m_strID.Trim() != "" && m_strEpisode.Trim() != "0")
                //{
                //    client.Query_TBPROG_D_MONITERAsync(m_strID.Trim(), m_strEpisode.Trim());        //檢查是否有填Moniter資料，沒有填要提醒
                //    //client.Query_TBPROG_D_INSERT_TAPEAsync(m_strID.Trim(), m_strEpisode.Trim());  //查詢節目子集檔的主控播出提示資料，改成檢查上面的Moniter資料
                //}
                //else
                UpdateND_DURATION();    //若是非節目加集別，像是以節目或是短帶送帶轉檔的就直接新增
            }
            else
            {
                MessageBox.Show(e.Result);
                OKButton.IsEnabled = true;
            }
        }

        //寫回子集的實際帶長及2nd Eventc、送帶轉檔資料
        private void UpdateND_DURATION()
        {
            string strDuration = "";      //要寫回子集的時間區間

            if (m_strType == "G")
                strDuration = CheckDuration();

            //寫回子集的實際帶長及2nd Event
            //if (strDuration == "" && m_2ndMemo != "")
            //    client.UPDATE_TBPROG_D_ND_DURATIONAsync(m_strID, m_strEpisode, "", m_2ndMemo, UserClass.userData.FSUSER_ID);
            //else if (strDuration != "" && m_2ndMemo == "")
            //    client.UPDATE_TBPROG_D_ND_DURATIONAsync(m_strID, m_strEpisode, strDuration, "", UserClass.userData.FSUSER_ID);
            //else if (strDuration != "" && m_2ndMemo != "")
            //    client.UPDATE_TBPROG_D_ND_DURATIONAsync(m_strID, m_strEpisode, strDuration, m_2ndMemo, UserClass.userData.FSUSER_ID);
            //else if (strDuration == "" && m_2ndMemo == "")
            client.INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLISTAsync(FormToClass_Bro(), ListLogVideo);
            //client.INSERT_BROADCASTAsync(FormToClass_Bro());        
        }

        //取消時，要去檢查資料表後刪除
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            //TODO:加入取消
            this.DialogResult = false;
        }



        //開啟節目資料查詢畫面
        private void btnPROG_Click(object sender, RoutedEventArgs e)
        {
            if (rdbProg.IsChecked == true)
            {
                //PRG100_01
                PROGDATA_VIEW PROGDATA_VIEW_frm = new PROGDATA_VIEW();
                PROGDATA_VIEW_frm.Show();

                PROGDATA_VIEW_frm.Closed += (s, args) =>
                {
                    if (PROGDATA_VIEW_frm.DialogResult == true)
                    {
                        //檢查是否要作資料認定
                        if (PROGDATA_VIEW_frm.strChannel_Id.Trim() == "" || PROGDATA_VIEW_frm.strDEP_ID.Trim() == "0")
                        {
                            checkProgM_Channel_Dep(PROGDATA_VIEW_frm.strProgID_View.Trim(), PROGDATA_VIEW_frm.strProgName_View.Trim(), PROGDATA_VIEW_frm.strChannel_Id.Trim(), PROGDATA_VIEW_frm.strDEP_ID.Trim(), PROGDATA_VIEW_frm.strProducer.Trim(), PROGDATA_VIEW_frm.strTOTLEEPISODE.Trim(), PROGDATA_VIEW_frm.strCHANNEL.Trim(), PROGDATA_VIEW_frm.strFNLENGTH.Trim());
                            return;
                        }

                        programClient.fnGetProg_MAsync(PROGDATA_VIEW_frm.strProgID_View.Trim());
                    }
                };
            }
            else if (rdbPromo.IsChecked == true)
            {
                //PRG800_07
                PRG800_07 PROMO_VIEW_frm = new PRG800_07();
                PROMO_VIEW_frm.Show();

                PROMO_VIEW_frm.Closed += (s, args) =>
                {
                    if (PROMO_VIEW_frm.DialogResult == true)
                    {
                        //檢查是否要作資料認定
                        if (PROMO_VIEW_frm.strChannel_Id.Trim() == "" || PROMO_VIEW_frm.strDEP_ID.Trim() == "0")
                        {
                            checkProgM_Channel_Dep(PROMO_VIEW_frm.strPromoID_View.Trim(), PROMO_VIEW_frm.strPromoName_View.Trim(), PROMO_VIEW_frm.strChannel_Id.Trim(), PROMO_VIEW_frm.strDEP_ID.Trim(), PROMO_VIEW_frm.strProducer.Trim(), "", "", "");
                            return;
                        }

                        //載入短帶資料
                        checkPromo(PROMO_VIEW_frm.strPromoID_View.Trim(), PROMO_VIEW_frm.strPromoName_View.Trim(), PROMO_VIEW_frm.strChannel_Id.Trim(), PROMO_VIEW_frm.strDEP_ID.Trim(), PROMO_VIEW_frm.strProducer.Trim());
                    }
                };
            }
        }

        //開啟節目子集資料查詢畫面
        private void btnEPISODE_Click(object sender, RoutedEventArgs e)
        {
            if (tbxPROG_NAME.Text.ToString().Trim() != "" && tbxPROG_NAME.Tag.ToString().Trim() != "")
            {
                PRG.PRG200_07 PRG200_07_frm = new PRG.PRG200_07(tbxPROG_NAME.Tag.ToString().Trim(), tbxPROG_NAME.Text.ToString().Trim());
                PRG200_07_frm.Show();

                PRG200_07_frm.Closed += (s, args) =>
                {
                    if (PRG200_07_frm.DialogResult == true)
                    {
                        m_strEpisode = PRG200_07_frm.m_strEpisode_View;
                        tbxEPISODE.Text = PRG200_07_frm.m_strEpisode_View;
                        lblEPISODE_NAME.Content = PRG200_07_frm.m_strProgDName_View;

                        //透過節目編號去查詢入庫影像檔
                        //ListLogVideo.Clear(); 
                        //this.DGLogList.DataContext = null;
                        //client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDAsync("G", m_strID, m_strEpisode, m_strBroID);//避免重複上傳，只用節目編號加集別去查，不用送帶轉檔單去查
                        //client.GetTBLOG_VIDEO_BYProgID_EpisodeAsync("G", m_strID, m_strEpisode);                  //新增送帶轉檔資料時，不去檢查該子集曾經送過的
                        //todo:加入程式
                        //找48小時有無排播記錄
                        client.GetPROG_PLAY_NOTEAsync(m_strProgID, m_strEpisode);
                    }
                };
            }
            else
                MessageBox.Show("請先選擇節目！", "提示訊息", MessageBoxButton.OK);
        }

        //檢查是否要作節目資料認定
        private void checkProgM_Channel_Dep(string strProgID, string strProgName, string strChannelID, string strDepID, string strProducer, string strTEpidode, string strPlayCHANNEL, string strLength)
        {
            string strCheck = "";   //判斷基本資料缺少訊息
            string strType = "";    //類型

            if (rdbProg.IsChecked == true)
                strType = "G";
            else if (rdbPromo.IsChecked == true)
                strType = "P";

            //無頻道別要檔掉     
            if (strChannelID.Trim() == "")
            {
                strCheck = "無頻道別資料";
            }

            //無成案單位要檔掉     
            if (strDepID.Trim() == "0")
            {
                if (strCheck.Trim() == "")
                    strCheck = "無成案單位資料";
                else
                    strCheck = strCheck + "、無成案單位資料";
            }

            if (strCheck.Trim() != "")
            {
                MessageBox.Show(strCheck + "，請先進行節目資料認定", "提示訊息", MessageBoxButton.OK);

                //判斷有無修改「節目資料認定」的權限     
                if (ModuleClass.getModulePermissionByName("01", "節目資料認定") == true)
                {
                    MessageBoxResult resultMsg = MessageBox.Show("確定要修改「" + strProgName.Trim() + "」的頻道別資料嗎?", "提示訊息", MessageBoxButton.OKCancel);
                    if (resultMsg == MessageBoxResult.OK)
                    {
                        PRG100_13 PRG100_13_frm = new PRG100_13(strType, strProgID, strProgName, strChannelID, strDepID);
                        PRG100_13_frm.Show();

                        PRG100_13_frm.Closed += (s, args) =>
                        {
                            if (rdbProg.IsChecked == true)
                            {
                                //如果資料補齊，即可載入節目資料
                                if (PRG100_13_frm.bolcheck == true)
                                    checkProg(strProgID, strProgName, strChannelID, strDepID, strProducer, strTEpidode, strPlayCHANNEL, strLength);
                            }
                            else if (rdbPromo.IsChecked == true)
                            {
                                //如果資料補齊，即可載入短帶資料
                                if (PRG100_13_frm.bolcheck == true)
                                    checkPromo(strProgID, strProgName, strChannelID, strDepID, strProducer);
                            }
                        };
                    }
                }
            }
        }

        //載入節目資料
        private void checkProg(string strProgID, string strProgName, string strChannelID, string strDepID, string strProducer, string strTEpidode, string strPlayCHANNEL, string strLength)
        {

            tbxPROG_NAME.Text = strProgName;
            tbxPROG_NAME.Tag = strProgID;
            m_strProducer = strProducer;                     //製作人
            //tbxEPISODE.Text = "";
            //lblEPISODE_NAME.Content = "";
            //m_strEpisode = "0";
            tbxTOTEPISODE.Text = strTEpidode;
            tbxLENGTH.Text = strLength;
            m_strCHANNELID = strChannelID;
            m_strDep_ID = strDepID; ;
            m_strProgID = strProgID;
            Clear_listCHANNEL();                             //清空-可播映頻道
            compareCode_FSCHANNEL(CheckList(strPlayCHANNEL));//可播映頻道

            //透過節目編號去查詢入庫影像檔
            //ListLogVideo.Clear();
            //this.DGLogList.DataContext = null;
            //client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDAsync("G", m_strID, "0", m_strBroID);     //避免重複上傳，只用節目編號加集別去查，不用送帶轉檔單去查
            //client.GetTBLOG_VIDEO_BYProgID_EpisodeAsync("G", m_strID, "0");                       //新增送帶轉檔資料時，不去檢查該子集曾經送過的
        }

        //載入短帶資料
        private void checkPromo(string strPromoID, string strPromoName, string strChannelID, string strDepID, string strProducer)
        {
            tbxPROG_NAME.Text = strPromoName;
            tbxPROG_NAME.Tag = strPromoID;
            m_strProducer = strProducer;                         //製作人
            m_strDep_ID = strDepID;
            tbxEPISODE.Text = "";
            lblEPISODE_NAME.Content = "";
            m_strEpisode = "0";
            m_strCHANNELID = strChannelID;
            m_strProgID = strPromoID;
            Clear_listCHANNEL();                                 //清空-可播映頻道                  

            //透過節目編號去查詢入庫影像檔
            //ListLogVideo.Clear();
            //this.DGLogList.DataContext = null;
            //client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDAsync("P", m_strID, "0", m_strBroID); //避免重複上傳，只用節目編號加集別去查，不用送帶轉檔單去查
            //client.GetTBLOG_VIDEO_BYProgID_EpisodeAsync("P", m_strID, "0");                   //新增送帶轉檔資料時，不去檢查該子集曾經送過的

            //找48小時有無排播記錄
            client.GetPROMO_PLAY_NOTEAsync(m_strProgID);
        }

        //開啟送帶轉檔，輸入TimeCode的介面
        private void btnSeg_Click(object sender, RoutedEventArgs e)
        {
            if (m_strType == "G" && m_strProgID == "")
            {
                MessageBox.Show("請先選擇節目名稱", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (m_strType == "P" && m_strProgID == "")
            {
                MessageBox.Show("請先選擇短帶名稱", "提示訊息", MessageBoxButton.OK);
                return;
            }
            //else if (m_strType == "G" && m_strID != "" && m_strEpisode == "0")  //因為只選節目或短帶也要可以新增，所以這裡先註解掉
            //{
            //    MessageBox.Show("請先選擇子集名稱", "提示訊息", MessageBoxButton.OK);
            //    return;
            //}

            if (cbVideo_TYPE.SelectedIndex != -1)                     //影片類型
            {
                m_strFileTypeName = ((ComboBoxItem)cbVideo_TYPE.SelectedItem).Content.ToString();
                m_strFileTypeID = ((ComboBoxItem)cbVideo_TYPE.SelectedItem).Tag.ToString();
            }
            else
            {
                MessageBox.Show("請先選擇影片類型", "提示訊息", MessageBoxButton.OK);
                return;
            }

            if (m_strType == "G" && m_strProgID != "" && m_strEpisode == "0" && !(m_strFileTypeID == "019" || m_strFileTypeID == "020"))
            {
                MessageBox.Show("請留意「" + m_strFileTypeName + "」除了\"資料帶\"以外類型轉檔，必須要設定集別", "提示訊息", MessageBoxButton.OK);
                return;
            }
            //}
            //else if(m_strType == "G" && m_strID != "" && m_strEpisode == "0" && m_strFileTypeID == "002")
            //{
            //    MessageBox.Show("請留意「" + m_strFileTypeName + "」若是為了播出用，必須要設定集別", "提示訊息", MessageBoxButton.OK);                
            //}

            //選定好節目或是短帶後，要鎖定
            rdbProg.IsEnabled = false;
            rdbPromo.IsEnabled = false;
            btnPROG.IsEnabled = false;
            btnEPISODE.IsEnabled = false;

            //檢查SD播出帶或是HD播出帶只能上傳一次
            for (int i = 0; i < ListLogVideo.Count; i++)
            {
                if (ListLogVideo[i].FSARC_TYPE.Trim() == "001" || ListLogVideo[i].FSARC_TYPE.Trim() == "002")
                {
                    //擋SD及HD播出帶時，也要只比對同一筆送帶轉檔單號，如果都擋掉，置換時就無法新增 ==> && ListLogVideo[i].FSBRO_ID.Trim().Equals(m_strBroID.Trim())
                    //這裡不是置換，因此全部的單號都要比對
                    if (ListLogVideo[i].FSARC_TYPE.Trim().Equals(m_strFileTypeID.Trim()) && ListLogVideo[i].FCFILE_STATUS.Trim() != "R")//除非是檔案狀態為轉檔失敗，才能重新提出
                    {
                        MessageBox.Show((ListLogVideo[i].FSARC_TYPE_NAME.Trim() + "只能上傳一次，請重新選擇影片類型！"), "提示訊息", MessageBoxButton.OK);
                        return;
                    }
                }
            }

            m_strCopyFSNO = ""; //新增時，要加修改或複製的檔案編號清空
            m_bolCopy = false;  //不是複製的段落

            //新增或複製前先取VideoID
            client.QueryVideoID_PROG_STTAsync(m_strProgID, m_strEpisode, m_strFileTypeID, false);
        }

        //修改段落檔
        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGLogList.SelectedItem == null)
            {
                MessageBox.Show("請選擇欲修改的資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            m_strFileTypeName = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSARC_TYPE_NAME;
            m_strFileTypeID = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSARC_TYPE;

            //當送帶轉檔單不同時，表示這不是同一次的送帶轉檔申請，因此無法修改
            if (m_strBroID.Trim() != ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSBRO_ID.Trim())
            {
                MessageBox.Show("此筆並非為當次申請的段落資料，因此無法修改", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (((Class_LOG_VIDEO)DGLogList.SelectedItem).FCFILE_STATUS.Trim() != "B")
            {
                MessageBox.Show("此筆段落資料狀態不是「待轉檔」，因此無法修改", "提示訊息", MessageBoxButton.OK);
                return;
            }

            m_strCopyFSNO = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSFILE_NO; //修改時，將要傳到下一頁的檔案編號補上
            m_VIDEOID_PROG = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSVIDEO_PROG.Trim();//若是修改，表示不用重新取VideoID
            m_bolCopy = false;  //不是複製的段落

            //修改段落
            Log_Video_Seg_Modify(((Class_LOG_VIDEO)DGLogList.SelectedItem));
        }

        //刪除段落檔
        private void btnDel_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGLogList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預刪除的段落資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //當送帶轉檔單不同時，表示這不是同一次的送帶轉檔申請，因此無法修改
            if (m_strBroID.Trim() != ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSBRO_ID.Trim())
            {
                MessageBox.Show("此筆並非為當次申請的段落資料，因此無法刪除", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (((Class_LOG_VIDEO)DGLogList.SelectedItem).FCFILE_STATUS.Trim() != "B")
            {
                MessageBox.Show("此筆段落資料狀態不是「待轉檔」，因此無法刪除", "提示訊息", MessageBoxButton.OK);
                return;
            }

            MessageBoxResult resultMsg = MessageBox.Show("確定要刪除「" + ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSARC_TYPE_NAME + "」的段落資料嗎?", "提示訊息", MessageBoxButton.OKCancel);
            if (resultMsg == MessageBoxResult.Cancel)
                return;
            else
            {
                //刪除段落檔
                ListLogVideo.Remove((Class_LOG_VIDEO)DGLogList.SelectedItem);
                DGLogList.ItemsSource = null;
                DGLogList.ItemsSource = ListLogVideo;
                //client.DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONEAsync(((Class_LOG_VIDEO)DGLogList.SelectedItem).FSFILE_NO, UserClass.userData.FSUSER_ID.ToString());           
            }

        }

        //複製段落
        private void btnCopy_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGLogList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預複製的段落資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            if (cbVideo_TYPE.SelectedIndex != -1)                     //影片類型
            {
                m_strFileTypeName = ((ComboBoxItem)cbVideo_TYPE.SelectedItem).Content.ToString();
                m_strFileTypeID = ((ComboBoxItem)cbVideo_TYPE.SelectedItem).Tag.ToString();
            }
            else
            {
                MessageBox.Show("請先選擇影片類型", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //檢查SD播出帶或是HD播出帶只能上傳一次
            for (int i = 0; i < ListLogVideo.Count; i++)
            {
                if (ListLogVideo[i].FSARC_TYPE.Trim() == "001" || ListLogVideo[i].FSARC_TYPE.Trim() == "002")
                {
                    //擋SD及HD播出帶時，也要只比對同一筆送帶轉檔單號，如果都擋掉，置換時就無法新增 ==> && ListLogVideo[i].FSBRO_ID.Trim().Equals(m_strBroID.Trim())
                    //這裡不是置換，因此全部的單號都要比對
                    if (ListLogVideo[i].FSARC_TYPE.Trim().Equals(m_strFileTypeID.Trim()) && ListLogVideo[i].FCFILE_STATUS.Trim() != "R")//除非是檔案狀態為轉檔失敗，才能重新提出
                    {
                        MessageBox.Show((ListLogVideo[i].FSARC_TYPE_NAME.Trim() + "只能上傳一次，請重新選擇影片類型！"), "提示訊息", MessageBoxButton.OK);
                        return;
                    }
                }
            }

            m_strCopyFSNO = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSFILE_NO; //複製時，將要傳到下一頁的檔案編號補上
            m_bolCopy = true;  //複製的段落

            //新增或複製前先取VideoID
            client.QueryVideoID_PROG_STTAsync(m_strProgID, m_strEpisode, m_strFileTypeID, false);

            #region Ori
            //if (m_strID == "" || m_strEpisode == "")
            //{
            //    MessageBox.Show("請確認節目及子集資料是否填妥!!", "警告", MessageBoxButton.OK);
            //    return;
            //}
            //else
            //{
            //    client.Set_Class_LOG_VIDEO_AND_SEGAsync(m_strType, m_strID, m_strEpisode, UserClass.userData.FSUSER_ID, m_strCHANNELID, tbxPROG_NAME.Text.Trim(), tbxEPISODE.Text.Trim(), m_strBroID, ListLogVideo);
            //}
            #endregion
        }

        //新增、修改、複製段落資料
        private void Log_Video_Seg()
        {
            string fstrack =audioTrackControl1.GetAudioTrackSetting();

            if (m_strType == "G")
            {
                if (fstrack == "")
                {
                    MessageBox.Show("音軌設定為必填!");
                    return;
                }
            }
            else if (m_strType == "P")
            {
                fstrack = "MMMM";
            }

            string strProgName = tbxPROG_NAME.Text.Trim();
            string strEpisodeName = "";

            if (m_strType == "G")
            {
                if (m_strEpisode == "0")
                    strEpisodeName = strProgName;
                else
                    strEpisodeName = lblEPISODE_NAME.Content.ToString();
            }
            else
                strEpisodeName = strProgName;
            Class_LOG_VIDEO CLV = new Class_LOG_VIDEO();
            Class_LOG_VIDEO selectedVideo = (Class_LOG_VIDEO)this.DGLogList.SelectedItem;
            if (selectedVideo == null) 
            {
                selectedVideo = new Class_LOG_VIDEO();

                selectedVideo.VideoListSeg = new List<Class_LOG_VIDEO_SEG>();
            }
           PropertyInfo[] piArr=CLV.GetType().GetProperties();

           foreach (System.Reflection.PropertyInfo pi in piArr) 
           {
               if(pi.Name!="VideoListSeg")
               pi.SetValue(CLV, pi.GetValue(selectedVideo, null), null);
           }

           CLV.VideoListSeg = new List<Class_LOG_VIDEO_SEG>();
           foreach (Class_LOG_VIDEO_SEG seg in selectedVideo.VideoListSeg) 
           {
               Class_LOG_VIDEO_SEG newSeg = new Class_LOG_VIDEO_SEG();
              PropertyInfo[] segProArr=seg.GetType().GetProperties();

              foreach (PropertyInfo pi in segProArr) 
              {
                  pi.SetValue(newSeg, pi.GetValue(seg, null), null);
              }

              CLV.VideoListSeg.Add(newSeg);
           }
            //CLV.FSTYPE = m_strType;
            //CLV.FSID = m_strID;
            //CLV.FNEPISODE = Convert.ToInt16(m_strEpisode);
            //CLV.FSBRO_ID = m_strBroID;
            CLV.FSVIDEO_PROG = m_VIDEOID_PROG;
            //CLV.FSCHANNEL_ID = m_strCHANNELID;
            //CLV.FCFILE_STATUS = "B";
            //CLV.FCFILE_STATUS_NAME = "待轉檔";
            //CLV.FSARC_TYPE = m_strFileTypeID;
            //CLV.FSARC_TYPE_NAME = m_strFileTypeName;
            //CLV.FSCHANNEL_ID = m_strCHANNELID;
            //CLV.FSCREATED_BY = UserClass.userData.FSUSER_ID;
            //CLV.FSUPDATED_BY = UserClass.userData.FSUSER_ID;

           

            STT100_10_01 STT100_10_01_frm = new STT100_10_01(m_strFileTypeName, m_strFileTypeID, m_strType, m_strProgID, m_strEpisode, m_strCHANNELID, m_strCopyFSNO, m_strBroID, strProgName, strEpisodeName, "", m_bolCopy, m_VIDEOID_PROG, CLV, fstrack);
            STT100_10_01_frm.Show();

            //若是有填入段落檔，就要去讀取該節目集別的段落資料，並且秀在畫面上
            STT100_10_01_frm.Closing += (s, args) =>
            {
                if (STT100_10_01_frm.DialogResult == true)
                {
                    if (ListLogVideo.Select(S => S.FSFILE_NO == STT100_10_01_frm.myCLV.FSFILE_NO).Count() > 0)
                    {
                        //ListLogVideo.Remove(ListLogVideo.Select(T => T.FSFILE_NO == STT100_10_01_frm.myCLV.FSFILE_NO).FirstOrDefault());

                    }
                    
                    ListLogVideo.Add(STT100_10_01_frm.myCLV);
                }
                DGLogList.ItemsSource = null;
                DGLogList.ItemsSource = ListLogVideo;
                //LoadTBLOG_VIDEO();  //載入入庫影像檔
            };
        }

        /// <summary>
        /// 修改時呼叫的函式
        /// 會直接傳入被選擇的Class_LOG_VIDEO
        /// </summary>
        /// <param name="iniCLV">在DGLogList中被選擇的Class_LOG_VIDEO</param>
        private void Log_Video_Seg_Modify(Class_LOG_VIDEO iniCLV)
        {
            string strProgName = tbxPROG_NAME.Text.Trim();
            string strEpisodeName = "";

            if (m_strType == "G")
            {
                if (m_strEpisode == "0")
                    strEpisodeName = strProgName;
                else
                    strEpisodeName = lblEPISODE_NAME.Content.ToString();
            }
            else
                strEpisodeName = strProgName;

            STT100_10_01 STT100_10_01_frm = new STT100_10_01(m_strFileTypeName, m_strFileTypeID, m_strType, m_strProgID, m_strEpisode, m_strCHANNELID, m_strCopyFSNO, m_strBroID, strProgName, strEpisodeName, "", m_bolCopy, m_VIDEOID_PROG, iniCLV, iniCLV.FSTRACK);
            STT100_10_01_frm.Show();

            //若是有填入段落檔，就要去讀取該節目集別的段落資料，並且秀在畫面上
            STT100_10_01_frm.Closing += (s, args) =>
            {
                //if (STT100_10_01_frm.DialogResult == true)
                //    ListLogVideo.Add(STT100_10_01_frm.myCLV);
                DGLogList.ItemsSource = null;
                DGLogList.ItemsSource = ListLogVideo;
            };
        }

        //呼叫Secondary Event新增程式
        private void btnEvent_Click(object sender, RoutedEventArgs e)
        {
            if (m_strType == "G" && m_strProgID == "")
            {
                MessageBox.Show("請先選擇節目名稱", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (m_strType == "G" && m_strProgID != "" && m_strEpisode == "0")
            {
                MessageBox.Show("請先選擇子集名稱", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //選定好節目或是短帶後，要鎖定
            rdbProg.IsEnabled = false;
            rdbPromo.IsEnabled = false;
            btnPROG.IsEnabled = false;
            btnEPISODE.IsEnabled = false;

            if (m_2ndMemo.Trim() == "")
                client.Query_TBPROG_D_NDAsync(m_strProgID, m_strEpisode);   //先查詢出該子集是否有2nd資料，若有要帶入修改
            else
                call2nd(m_2ndMemo.Trim());           //呼叫2ND   
        }

        //設定主控播出提示資料
        private void btnSet_Click(object sender, RoutedEventArgs e)
        {
            if (m_strType == "G" && m_strProgID == "")
            {
                MessageBox.Show("請先選擇節目名稱", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (m_strType == "G" && m_strProgID != "" && m_strEpisode == "0")
            {
                MessageBox.Show("請先選擇子集名稱", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //選定好節目或是短帶後，要鎖定
            rdbProg.IsEnabled = false;
            rdbPromo.IsEnabled = false;
            btnPROG.IsEnabled = false;
            btnEPISODE.IsEnabled = false;

            PTS_MAM3.PGM.PGM100_05 PGM100_05_Frm = new PTS_MAM3.PGM.PGM100_05();

            PGM100_05_Frm.textBoxProgID.Text = m_strProgID;
            PGM100_05_Frm.textBoxProgName.Text = tbxPROG_NAME.Text.Trim();
            PGM100_05_Frm.textBoxEpisode.Text = m_strEpisode;
            PGM100_05_Frm.Show();
            PGM100_05_Frm.Closed += (s, args) =>
            {
            };
        }

        //呼叫2ND
        private void call2nd(string str2nd)
        {
            PGM.PGM100_02 SetLouthKey_Frm = new PGM.PGM100_02();

            //修改時傳入組好的備註字串
            SetLouthKey_Frm.InLouthKeyString = str2nd;

            SetLouthKey_Frm.Show();
            SetLouthKey_Frm.Closed += (s, args) =>
            {
                if (SetLouthKey_Frm.OutLouthKeyString != null)
                {
                    //傳回選擇好的備註字串                  
                    m_2ndMemo = SetLouthKey_Frm.OutLouthKeyString.Trim();
                }
            };
        }

        #endregion

        #region 流程引擎

        //以下程式將Flow放到Flow引擎中
        private void newAFlow()
        {
            string FlowNote = "";

            if (m_strType == "G")
                FlowNote = "節目名稱-" + tbxPROG_NAME.Text.ToString().Trim();
            else
                FlowNote = "短帶名稱-" + tbxPROG_NAME.Text.ToString().Trim();

            if (m_strEpisode.Trim() != "0")
                FlowNote = FlowNote + "，集別-" + tbxEPISODE.Text.ToString().Trim();

            StringBuilder sb = new StringBuilder();
            sb.Append(@"<VariableCollection>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>FormId</name>");
            sb.Append(@"    <value>" + tbxBro_ID.Text + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>NextXamlName</name>");
            sb.Append(@"    <value>/STT/STT100_05.xaml</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>Function01</name>");
            sb.Append(@"    <value>0</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>hid_SendTo</name>");      //預設是給1，不用審核的單子最後顯示才會是通過
            sb.Append(@"    <value>1</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>parameter01</name>");     //預設通過的狀態-Y(提出者是審核人)
            sb.Append(@"    <value>Y</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>USERID</name>");
            sb.Append(@"    <value>" + UserClass.userData.FSUSER_ID.ToString().Trim() + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>FormInfo</name>");
            sb.Append(@"    <value>" + FlowNote + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>CancelForm</name>");
            sb.Append(@"    <value>CanCancel</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"</VariableCollection>");

            //後端呼叫流程引擎
            clientFlow.CallFlow_NewFlowWithFieldAsync(6, UserClass.userData.FSUSER_ID.ToString().Trim(), sb.ToString());
            //FlowClinet.NewFlowWithFieldAsync(6, UserClass.userData.FSUSER_ID.ToString().Trim(), sb.ToString());   //原本的前端呼叫流程引擎
        }

        //原本的前端呼叫流程引擎
        void FlowClinet_NewFlowWithFieldCompleted(object sender, flowWebService.NewFlowWithFieldCompletedEventArgs e)
        {
            if (e.Error == null && e.Result != "")
            {
                MessageBox.Show("新增送帶轉檔資料成功！", "提示訊息", MessageBoxButton.OK);
                this.DialogResult = true;
            }
            else
            {
                if (intCountFlow >= 3)  //不成功就呼叫流程三次，三次都不成功就顯示失敗
                {
                    MessageBox.Show("呼叫流程引擎異常，新增送帶轉檔資料失敗！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = true;  //雖然流程失敗，但是因為已寫入還是要畫面重新Load
                }
                else
                {
                    intCountFlow++;
                    newAFlow();
                }
            }
        }

        #endregion

        #region 重要function

        //取得TimeCode區間
        private string CheckDuration()
        {
            string strReturn = "";
            for (int i = 0; i < ListLogVideo.Count; i++)
            {
                //SD播出帶或是HD播出帶，轉檔完成後要更新子集的時長
                if (ListLogVideo[i].FSARC_TYPE.Trim() == "001" || ListLogVideo[i].FSARC_TYPE.Trim() == "002")
                    strReturn = Convert.ToString(TransferTimecode.timecodetoSecond(ListLogVideo[i].FSEND_TIMECODE) - TransferTimecode.timecodetoSecond(ListLogVideo[i].FSBEG_TIMECODE)).Trim();
                return strReturn;
            }
            return "";
        }

        //找出製作人姓名(製作人放到流程處理)
        private string checkProducerName()
        {
            string strListProducerName = "";

            if (m_strProducer.IndexOf(";") > -1 && m_strProducer.Trim().Length > 1)    //找出製作人Name 
            {
                strListProducerName = m_strProducer.Substring(m_strProducer.IndexOf(";") + 1);
                return strListProducerName.Substring(0, strListProducerName.Length - 1);
            }
            else
                return "";
        }

        //找出製作人編號(製作人放到流程處理)
        private string checkProducerID()
        {
            string strListProducerID = "";

            if (m_strProducer.IndexOf(";") > -1 && m_strProducer.Trim().Length > 1)    //找出製作人編號 
            {
                strListProducerID = m_strProducer.Substring(0, m_strProducer.IndexOf(";"));
                return strListProducerID;
            }
            else
                return "";
        }

        //檢查製作人是否也為送帶轉檔提出者(製作人放到流程處理)
        private Boolean checkProducer()
        {
            string strListProducerID = "";

            if (m_strProducer.IndexOf(";") > -1 && m_strProducer.Trim().Length > 1)   //找出製作人ID         
                strListProducerID = m_strProducer.Substring(0, m_strProducer.IndexOf(";"));
            else
                return false;

            char[] delimiterChars = { ',' };
            string[] words = strListProducerID.Split(delimiterChars);

            if (words.Length > 0)
            {
                for (int i = 0; i < words.Length; i++)
                {
                    if (words[i] == UserClass.userData.FSUSER_ID.ToString().Trim())
                    {
                        return true;          //若是提出申請者就是製作人
                    }
                }
            }
            return false;
        }

        //DataGrid變色
        private void ChildWindow_Loaded(object sender, RoutedEventArgs e)
        {
            DGLogList.LoadingRow += new EventHandler<DataGridRowEventArgs>(DGLogList_LoadingRow);
        }

        //實作-DataGrid變色
        void DGLogList_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Background = new SolidColorBrush(Colors.White);
            //e.Row.Foreground = new SolidColorBrush(Colors.Black);

            Class_LOG_VIDEO LOG_VIDEO = e.Row.DataContext as Class_LOG_VIDEO;

            if (LOG_VIDEO.FSBRO_ID.Trim() != m_strBroID.Trim())
            {
                e.Row.Background = new SolidColorBrush(Colors.LightGray);
                //e.Row.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        #endregion

        private void DGLogList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.DGLogList.SelectedItem != null)
            {
                Class_LOG_VIDEO log_video = (Class_LOG_VIDEO)this.DGLogList.SelectedItem;

                this.audioTrackControl1.SetAudioTrackSetting(log_video.FSTRACK);
            }
        }
    }
}

