﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.PRG
{
    public partial class STT700_02 : ChildWindow
    {
        public STT700_02(List<string> ListSeg_MSG)  //, string strSW
        {
            InitializeComponent();
            
            this.Title = "刪除節目基本資料提示訊息";

            if (ListSeg_MSG.Count > 0)
                DGPROG_MList.ItemsSource = ListSeg_MSG;
        }
       
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void btnAlter_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }       
    }
}

