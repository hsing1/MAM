﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSBROADCAST;
using PTS_MAM3.WSMAMFunctions;
using System.Text;
using System.Collections.ObjectModel;
using PTS_MAM3.SR_WSTSM_GetFileInfo;
using PTS_MAM3.WSTAPE;

namespace PTS_MAM3.STT
{
    public partial class STT700_03 : ChildWindow
    {
        WSBROADCASTSoapClient client = new WSBROADCASTSoapClient();                         //產生新的代理類別        
        public List<Class_TAPE_LABEL_CLIP> m_ListClipTimeCode = new List<Class_TAPE_LABEL_CLIP>();//段落資訊(影帶)
        List<Class_TAPE_LABEL_CLIP> m_ListClipTemp = new List<Class_TAPE_LABEL_CLIP>();     //暫存上一頁丟進來的段落資訊(影帶)
        string m_strTitle = "";                                                             //標題
        string m_strDescription = "";                                                       //描述

        public STT700_03(List<Class_TAPE_LABEL_CLIP> GetListClip,string strTitle,string strDescription)
        {
            InitializeComponent();

            m_strTitle = strTitle;
            m_strDescription = strDescription;

            tbxTITLE.Text = m_strTitle;
            tbxDESCRIPTION.Text = m_strDescription;

            m_ListClipTimeCode = GetListClip;
            copyClip(GetListClip);              //複製段落

            DGSeg.DataContext = m_ListClipTimeCode;
            tbAllDurationS.Text = checkSeg();   //顯示影片長度
        }   
       
        #region 檢查

        private Boolean CheckFormat()    //檢查畫面上的欄位是否都填妥
        {
            StringBuilder strMessage = new StringBuilder();
            int intCheck;   //純粹為了判斷是否為數字型態之用
            string strBEGError = "";
            string strENDError = "";
            string strNewEnd = "";

            if (int.TryParse(tbxS1.Text.Trim(), out intCheck) == false || tbxS1.Text.Trim().Length != 2)
                    strMessage.AppendLine("請檢查「Time Code 起」第一個欄位必須為數值型態且為兩碼");

            if (int.TryParse(tbxS2.Text.Trim(), out intCheck) == false || tbxS2.Text.Trim().Length != 2)
                strMessage.AppendLine("請檢查「Time Code 起」第二個欄位必須為數值型態且為兩碼");

            if (int.TryParse(tbxS3.Text.Trim(), out intCheck) == false || tbxS3.Text.Trim().Length != 2)
                strMessage.AppendLine("請檢查「Time Code 起」第三個欄位必須為數值型態且為兩碼");

            if (int.TryParse(tbxS4.Text.Trim(), out intCheck) == false || tbxS4.Text.Trim().Length != 2)
                strMessage.AppendLine("請檢查「Time Code 起」第四個欄位必須為數值型態且為兩碼");

            if (int.TryParse(tbxS5.Text.Trim(), out intCheck) == false || tbxS5.Text.Trim().Length != 2)
                strMessage.AppendLine("請檢查「Time Code 迄」第一個欄位必須為數值型態且為兩碼");

            if (int.TryParse(tbxS6.Text.Trim(), out intCheck) == false || tbxS6.Text.Trim().Length != 2)
                strMessage.AppendLine("請檢查「Time Code 迄」第二個欄位必須為數值型態且為兩碼");

            if (int.TryParse(tbxS7.Text.Trim(), out intCheck) == false || tbxS7.Text.Trim().Length != 2)
                strMessage.AppendLine("請檢查「Time Code 迄」第三個欄位必須為數值型態且為兩碼");

            if (int.TryParse(tbxS8.Text.Trim(), out intCheck) == false || tbxS8.Text.Trim().Length != 2)
                strMessage.AppendLine("請檢查「Time Code 迄」第四個欄位必須為數值型態且為兩碼");

            if (strMessage.ToString().Trim() == "")
            {
                if (!TransferTimecode.check_timecode_format(tbxS1.Text.Trim(), tbxS2.Text.Trim(), tbxS3.Text.Trim(), tbxS4.Text.Trim(), out strBEGError))
                {
                    //20140902 By Jarvis
                    if (strBEGError == "所輸入的時間不符合DropFrame的格式")
                    {
                        tbxS4.Text = "02";
                        int frames = TransferTimecode.timecodetoframe(tbxS1.Text.Trim() + ":" + tbxS2.Text.Trim() + ":" + tbxS3.Text.Trim() + ";" + tbxS4.Text.Trim());
                        string newFame = TransferTimecode.frame2timecode(frames);
                        MessageBoxResult result = MessageBox.Show("Time Code 起自動改成：「" + newFame + "」", "提示訊息", MessageBoxButton.OK);

                        tbxS1.Text = newFame.Substring(0, 2);
                        tbxS2.Text = newFame.Substring(3, 2);
                        tbxS3.Text = newFame.Substring(6, 2);
                        tbxS4.Text = newFame.Substring(9, 2);
                    }
                    else
                    {
                        MessageBox.Show(strBEGError);
                        return false;
                    }
                    // }
                }

                if (!TransferTimecode.check_timecode_format(tbxS5.Text.Trim(), tbxS6.Text.Trim(), tbxS7.Text.Trim(), tbxS8.Text.Trim(), out strENDError))
                {
                    //20140902 By Jarvis
                    if (strENDError == "所輸入的時間不符合DropFrame的格式")
                    {
                        tbxS8.Text = "02";
                        int frames = TransferTimecode.timecodetoframe(tbxS5.Text.Trim() + ":" + tbxS6.Text.Trim() + ":" + tbxS7.Text.Trim() + ";" + tbxS8.Text.Trim());
                        string newFame = TransferTimecode.frame2timecode(frames);
                        MessageBoxResult result = MessageBox.Show("Time Code 迄自動改成：「" + newFame + "」", "提示訊息", MessageBoxButton.OK);


                        tbxS5.Text = newFame.Substring(0, 2);
                        tbxS6.Text = newFame.Substring(3, 2);
                        tbxS7.Text = newFame.Substring(6, 2);
                        tbxS8.Text = newFame.Substring(9, 2);
                    }
                    else
                    {
                        MessageBox.Show(strENDError);
                        return false;
                    }

                    // }
                }
                //TransferTimecode.check_timecode_format(tbxS1.Text.Trim(), tbxS2.Text.Trim(), tbxS3.Text.Trim(), tbxS4.Text.Trim(),out strBEGError);
                //TransferTimecode.check_timecode_format(tbxS5.Text.Trim(), tbxS6.Text.Trim(), tbxS7.Text.Trim(), tbxS8.Text.Trim(), out strENDError);

                //if (strBEGError.Trim() != "")
                //{
                //    //若是不符合規定時，起始的Timecode，最後一個變成02
                //    tbxS4.Text = "02";
                //    MessageBoxResult result = MessageBox.Show("Time Code 起自動改成：「" + tbxS1.Text.Trim() + ":" + tbxS2.Text.Trim() + ":" + tbxS3.Text.Trim() + ";" + tbxS4.Text.Trim() + "」", "提示訊息", MessageBoxButton.OK);

                //    //原本輸入錯誤的Timecode資料會被擋掉
                //    //MessageBoxResult result = MessageBox.Show("Time Code 起：" + strBEGError, "提示訊息", MessageBoxButton.OK);
                //    //return false;
                //}
                
                //if (strENDError.Trim() != "")
                //{
                //    strNewEnd = checkEndTimecode(tbxS5.Text.Trim() + ":" + tbxS6.Text.Trim() + ":" + tbxS7.Text.Trim() + ";" + tbxS8.Text.Trim());
                //    tbxS5.Text = strNewEnd.Substring(0, 2);
                //    tbxS6.Text = strNewEnd.Substring(3, 2);
                //    tbxS7.Text = strNewEnd.Substring(6, 2);
                //    tbxS8.Text = strNewEnd.Substring(9, 2);
                //    MessageBoxResult result = MessageBox.Show("Time Code 迄自動改成：「" + tbxS5.Text.Trim() + ":" + tbxS6.Text.Trim() + ":" + tbxS7.Text.Trim() + ";" + tbxS8.Text.Trim() + "」", "提示訊息", MessageBoxButton.OK);

                //    //原本輸入錯誤的Timecode資料會被擋掉
                //    //MessageBoxResult result = MessageBox.Show("Time Code 迄：" + strENDError, "提示訊息", MessageBoxButton.OK);
                //    //return false;
                //}

                if (TransferTimecode.Subtract_timecode(tbxS1.Text.Trim() + ":" + tbxS2.Text.Trim() + ":" + tbxS3.Text.Trim() + ";" + tbxS4.Text.Trim(), tbxS5.Text.Trim() + ":" + tbxS6.Text.Trim() + ":" + tbxS7.Text.Trim() + ";" + tbxS8.Text.Trim()).StartsWith("E"))
                {
                    MessageBoxResult result = MessageBox.Show("Time Code (迄) 必須大於Time Code (起)", "提示訊息", MessageBoxButton.OK);
                    return false;
                }
            }            

           if (strMessage.ToString().Trim() == "")     
               return true;     
           else     
           {     
               MessageBoxResult result = MessageBox.Show(strMessage.ToString(), "提示訊息", MessageBoxButton.OK);     
               return false;     
           }        
        }

        //將不存在的結束Timecode改成存在的
        private string checkEndTimecode(string strEnd)
        {
            string strReturn = TransferTimecode.frame2timecode(TransferTimecode.timecodetoframe(strEnd));

            if (strReturn.EndsWith("28") == true)
                strReturn = strReturn.Substring(0, 9) + "29";

            return strReturn;
        }

        #endregion

        //新增段落檔
        private void btnNew_Click(object sender, RoutedEventArgs e)
        {            
            if (CheckFormat() == false)      //檢查畫面上的欄位是否都填妥
                return;

            if (btnNew.Content.ToString().Trim() == "新增")
                FormToClass_Seg();          //新增段落資料
            else
                FormToClass_Seg_Alter();    //修改段落資料

            //clearNote();                    //欄位清空
        }

        //新增段落資料
        private void FormToClass_Seg()
        {
            Class_TAPE_LABEL_CLIP Clip = new Class_TAPE_LABEL_CLIP();            //影帶段落
            Clip.FSCLIPTITLE = "";
            Clip.FSTCSTART = tbxS1.Text.ToString().Trim() + ":" + tbxS2.Text.ToString().Trim() + ":" + tbxS3.Text.ToString().Trim() + ";" + tbxS4.Text.ToString().Trim();
            Clip.FSTCEND = tbxS5.Text.ToString().Trim() + ":" + tbxS6.Text.ToString().Trim() + ":" + tbxS7.Text.ToString().Trim() + ";" + tbxS8.Text.ToString().Trim();
            Clip.FSDURATION = TransferTimecode.Subtract_timecode(Clip.FSTCSTART, Clip.FSTCEND);
            Clip.FNBEG_TIMECODE_FRAME = TransferTimecode.timecodetoframe(Clip.FSTCSTART);
            Clip.FNEND_TIMECODE_FRAME = TransferTimecode.timecodetoframe(Clip.FSTCEND);
            #region 加入TimeCode起始小於之前輸入TimeCode結尾提示 2012/10/24 Kyle
            foreach (var item in m_ListClipTimeCode)
            {
                if (item.FNEND_TIMECODE_FRAME > Clip.FNBEG_TIMECODE_FRAME)
                {
                    MessageBoxResult myDecide = MessageBox.Show("本段落TimeCode起始時間，先於已輸入段落結束時間，\n'確定'加入 或 取消。", "TimeCode提示!!", MessageBoxButton.OKCancel);
                    if (myDecide == MessageBoxResult.OK)
                    {
                        break;
                    }
                    else
                    {
                        return;
                    }
                }
            }
            clearNote();        //欄位清空
            m_ListClipTimeCode.Add(Clip);   //輸入的段落資訊插入的段落集合裡
            #endregion
            

            //自動重新排序
            m_ListClipTimeCode = TransferTimecode.SortListClassTape(m_ListClipTimeCode);

            DGSeg.DataContext = null;
            DGSeg.DataContext = m_ListClipTimeCode;
            tbAllDurationS.Text = checkSeg();   //顯示影片長度
        }

        //修改段落資料
        private void FormToClass_Seg_Alter()
        {
            Class_TAPE_LABEL_CLIP ClassSeg = new Class_TAPE_LABEL_CLIP();            //段落
            ClassSeg.FSTCSTART = tbxS1.Text.ToString().Trim() + ":" + tbxS2.Text.ToString().Trim() + ":" + tbxS3.Text.ToString().Trim() + ";" + tbxS4.Text.ToString().Trim();
            ClassSeg.FSTCEND = tbxS5.Text.ToString().Trim() + ":" + tbxS6.Text.ToString().Trim() + ":" + tbxS7.Text.ToString().Trim() + ";" + tbxS8.Text.ToString().Trim();
            ClassSeg.FSDURATION = TransferTimecode.Subtract_timecode(ClassSeg.FSTCSTART, ClassSeg.FSTCEND);
            ClassSeg.FNBEG_TIMECODE_FRAME = TransferTimecode.timecodetoframe(ClassSeg.FSTCSTART);
            ClassSeg.FNEND_TIMECODE_FRAME = TransferTimecode.timecodetoframe(ClassSeg.FSTCEND);

            ((Class_TAPE_LABEL_CLIP)(DGSeg.SelectedItem)).FSTCSTART = ClassSeg.FSTCSTART;
            ((Class_TAPE_LABEL_CLIP)(DGSeg.SelectedItem)).FSTCEND = ClassSeg.FSTCEND;
            ((Class_TAPE_LABEL_CLIP)(DGSeg.SelectedItem)).FSDURATION = ClassSeg.FSDURATION;
            ((Class_TAPE_LABEL_CLIP)(DGSeg.SelectedItem)).FNBEG_TIMECODE_FRAME = ClassSeg.FNBEG_TIMECODE_FRAME;
            ((Class_TAPE_LABEL_CLIP)(DGSeg.SelectedItem)).FNEND_TIMECODE_FRAME = ClassSeg.FNEND_TIMECODE_FRAME;

            #region 加入TimeCode起始小於之前輸入TimeCode結尾提示 2012/10/24 Kyle(修改時)
            foreach (var item in m_ListClipTimeCode)
            {
                if (item == ((Class_TAPE_LABEL_CLIP)(DGSeg.SelectedItem)))
                {
                    continue;
                }
                if (item.FNEND_TIMECODE_FRAME > ClassSeg.FNBEG_TIMECODE_FRAME)
                {
                    MessageBoxResult myDecide = MessageBox.Show("本段落TimeCode起始時間，先於已輸入段落結束時間，\n'確定'加入 或 取消。", "TimeCode提示!!", MessageBoxButton.OKCancel);
                    if (myDecide == MessageBoxResult.OK)
                    {
                        break;
                    }
                    else
                    {
                        return;
                    }
                }
            }
            clearNote();        //欄位清空
            #endregion

            //自動重新排序
            m_ListClipTimeCode = TransferTimecode.SortListClassTape(m_ListClipTimeCode);

            DGSeg.DataContext = null;
            DGSeg.DataContext = m_ListClipTimeCode;
            tbAllDurationS.Text = checkSeg();         //顯示影片長度
            btnCan.Visibility = Visibility.Collapsed; //取消消失   
            btnNew.Content = "新增";                  //修改完成，要變回新增
        }

        //計算段落長度
        private string checkSeg()
        {
            int intFrame = 0;
            string strTimecode = "";

            for (int i = 0; i < m_ListClipTimeCode.Count; i++)
            {
                intFrame = intFrame + TransferTimecode.Subtract_timecode_toframe(m_ListClipTimeCode[i].FSTCSTART, m_ListClipTimeCode[i].FSTCEND);
            }

            strTimecode = TransferTimecode.frame2timecode(intFrame);
            return strTimecode.Substring(0, 8) + ";" + strTimecode.Substring(9);
        }

        //刪除其中一筆
        private void btnDel_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGSeg.SelectedItem == null)
            {
                MessageBox.Show("請選擇預刪除的段落資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            MessageBoxResult resultMsg = MessageBox.Show("確定要刪除「" + ((Class_TAPE_LABEL_CLIP)(DGSeg.SelectedItem)).FSTCSTART + "~" + ((Class_TAPE_LABEL_CLIP)(DGSeg.SelectedItem)).FSTCEND + "」的段落資料嗎?", "提示訊息", MessageBoxButton.OKCancel);
            if (resultMsg == MessageBoxResult.Cancel)
                return;
            else
            {
                m_ListClipTimeCode.Remove((Class_TAPE_LABEL_CLIP)(DGSeg.SelectedItem));

                DGSeg.DataContext = null;
                DGSeg.DataContext = m_ListClipTimeCode;

                tbAllDurationS.Text = checkSeg();             //顯示影片長度

                if (btnNew.Content.ToString().Trim() == "修改")
                {
                    btnCan.Visibility = Visibility.Collapsed; //取消消失
                    btnNew.Content = "新增";                  //修改改成確定         
                    clearNote();                              //欄位清空
                }
             }
        }

        //按下確定時，若是有資料即上傳
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (m_ListClipTimeCode.Count > 0)
            {
                for (int i = 0; i < m_ListClipTimeCode.Count; i++)
                {
                    if (m_ListClipTimeCode[i].FSDURATION.ToString().Trim().Equals("00:00:00;00"))
                    {
                        MessageBox.Show("「" + m_ListClipTimeCode[i].FSTCSTART.Trim() + "~" + m_ListClipTimeCode[i].FSTCEND.Trim() + "的段落資料異常，請檢查", "提示訊息", MessageBoxButton.OK);
                        return;
                    }
                }
            }          

            OKButton.IsEnabled = false;  //避免使用者按了兩次，按下後要鎖住
            this.DialogResult = true;      
        }

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            m_ListClipTimeCode = m_ListClipTemp;    //取消時，把傳進來的參數再丟回前一個畫面
            this.DialogResult = false;
        }

        //修改畫面上段落資料
        private void DGSeg_CellEditEnded(object sender, DataGridCellEditEndedEventArgs e)
        {
            string strSUB = "";
            Class_TAPE_LABEL_CLIP SegRecord = new Class_TAPE_LABEL_CLIP();
            SegRecord = ((Class_TAPE_LABEL_CLIP)DGSeg.SelectedItem);

            if (check_Grid_TimeCode(SegRecord.FSTCSTART, SegRecord.FSTCEND, out strSUB) == false)
            {               
                SegRecord.FSTCSTART = "01:00:00;00";
                SegRecord.FSTCEND = "01:00:00;00";
                SegRecord.FSDURATION = "00:00:00;00";
            }
            else
                SegRecord.FSDURATION = TransferTimecode.Subtract_timecode(SegRecord.FSTCSTART, SegRecord.FSTCEND);
        }

        private Boolean check_Grid_TimeCode(string strBEG,string strEND,out string strSUB)
        {
            string strBEGError = "";
            string strENDError = "";
            strSUB = "";

            if (strBEG.Length != 11)
            {
                MessageBoxResult result = MessageBox.Show("Time Code 起：長度異常", "提示訊息", MessageBoxButton.OK);
                return false;      
            }

            if (strEND.Length != 11)
            {
                MessageBoxResult result = MessageBox.Show("Time Code 迄：長度異常", "提示訊息", MessageBoxButton.OK);
                return false;             
            }

            TransferTimecode.check_timecode_format(strBEG.Substring(0, 2).Trim(), strBEG.Substring(3, 2).Trim(), strBEG.Substring(6, 2).Trim(), strBEG.Substring(9, 2).Trim(), out strBEGError);
            TransferTimecode.check_timecode_format(strEND.Substring(0, 2).Trim(), strEND.Substring(3, 2).Trim(), strEND.Substring(6, 2).Trim(), strEND.Substring(9, 2).Trim(), out strENDError);

            if (strBEGError.Trim() != "")
            {
                MessageBoxResult result = MessageBox.Show("Time Code 起：" + strBEGError, "提示訊息", MessageBoxButton.OK);
                return false;                 
            }

            if (strENDError.Trim() != "")
            {
                MessageBoxResult result = MessageBox.Show("Time Code 迄：" + strENDError, "提示訊息", MessageBoxButton.OK);
                return false;                
            }

            strSUB = TransferTimecode.Subtract_timecode(strBEG, strEND);

            if (strSUB.StartsWith("E"))
            {
                MessageBoxResult result = MessageBox.Show("Time Code (起) 大於Time Code (迄)", "提示訊息", MessageBoxButton.OK);
                return false;
            }
            else
            {
                return true;
            }

        }

        ////修改畫面上段落資料時，也將List裡改掉
        //private void ListSegUpdate(string strNo, string strBeg, string strEnd,string strSub)
        //{
        //    for (int i = 0; i < ListSeg.Count; i++)
        //    {              
        //      if (ListSeg[i].FNSEG_ID.ToString().Trim().Equals(strNo))
        //          {
        //          ListSeg[i].FSBEG_TIMECODE = strBeg ;
        //          ListSeg[i].FSEND_TIMECODE = strEnd;
        //          ListSeg[i].FSSUB_TIMECODE = strSub;
        //          }
        //    }
        //}

        //輸入完自動跳下一個
        private void tbxS1_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tbxS1.Text.Trim().Length == 2)
                tbxS2.Focus();
        }

        private void tbxS2_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tbxS2.Text.Trim().Length == 2)
                tbxS3.Focus();
        }

        private void tbxS3_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tbxS3.Text.Trim().Length == 2)
                tbxS4.Focus();
        }

        private void tbxS4_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tbxS4.Text.Trim().Length == 2)
                tbxS5.Focus();
        }

        private void tbxS5_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tbxS5.Text.Trim().Length == 2)
                tbxS6.Focus();
        }

        private void tbxS6_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tbxS6.Text.Trim().Length == 2)
                tbxS7.Focus();
        }

        private void tbxS7_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tbxS7.Text.Trim().Length == 2)
                tbxS8.Focus();
        }

        private void tbxS8_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tbxS8.Text.Trim().Length == 2)
                btnNew.Focus();
        }

        //欄位清空
        private void clearNote()
        {
            tbxS1.Text = "";
            tbxS2.Text = "";
            tbxS3.Text = "";
            tbxS4.Text = "";
            tbxS5.Text = "";
            tbxS6.Text = "";
            tbxS7.Text = "";
            tbxS8.Text = "";

            tbxS1.Focus();
        }

        //點選段落DataGrid，即可修改段落
        private void DGSeg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DGSeg.SelectedItem == null)
                return;

            Class_TAPE_LABEL_CLIP SegRecord = new Class_TAPE_LABEL_CLIP();
            SegRecord = ((Class_TAPE_LABEL_CLIP)DGSeg.SelectedItem);

            tbxS1.Text = SegRecord.FSTCSTART.Substring(0, 2);
            tbxS2.Text = SegRecord.FSTCSTART.Substring(3, 2);
            tbxS3.Text = SegRecord.FSTCSTART.Substring(6, 2);
            tbxS4.Text = SegRecord.FSTCSTART.Substring(9, 2);

            tbxS5.Text = SegRecord.FSTCEND.Substring(0, 2);
            tbxS6.Text = SegRecord.FSTCEND.Substring(3, 2);
            tbxS7.Text = SegRecord.FSTCEND.Substring(6, 2);
            tbxS8.Text = SegRecord.FSTCEND.Substring(9, 2);

            btnCan.Visibility = Visibility.Visible; //取消出現  
            btnNew.Content = "修改";                //確定改成修改  
        }

        //取消修改段落
        private void btnCan_Click(object sender, RoutedEventArgs e)
        {
            btnCan.Visibility = Visibility.Collapsed; //取消消失
            btnNew.Content = "新增";                  //修改改成確定         
            clearNote();                              //欄位清空
        }

        //避免使用者按了取消，要先將傳進來的影帶段落複製一份，下面的這個一筆一筆指定的作法，才會正確
        private void copyClip(List<Class_TAPE_LABEL_CLIP> obj)
        {
            foreach (Class_TAPE_LABEL_CLIP tempObj in obj)
            {
                Class_TAPE_LABEL_CLIP tempObj2 = new Class_TAPE_LABEL_CLIP();

                tempObj2.FSBIB = tempObj.FSBIB;
                tempObj2.FSTCSTART = tempObj.FSTCSTART;
                tempObj2.FSTCEND = tempObj.FSTCEND;
                tempObj2.FSDURATION = tempObj.FSDURATION;
                tempObj2.FSCLIPTITLE = tempObj.FSCLIPTITLE;
                tempObj2.FSVIDEO_ID = tempObj.FSVIDEO_ID;
                tempObj2.FNBEG_TIMECODE_FRAME = tempObj.FNBEG_TIMECODE_FRAME;
                tempObj2.FNEND_TIMECODE_FRAME = tempObj.FNEND_TIMECODE_FRAME;
                tempObj2.FSCREATED_BY_NAME = tempObj.FSCREATED_BY_NAME;
                tempObj2.FSUPDATED_BY_NAME = tempObj.FSUPDATED_BY_NAME;
                tempObj2.FDCREATED_DATE = tempObj.FDCREATED_DATE;
                tempObj2.FDUPDATED_DATE = tempObj.FDUPDATED_DATE;
                tempObj2.SHOW_FDCREATED_DATE = tempObj.SHOW_FDCREATED_DATE;
                tempObj2.SHOW_FDUPDATED_DATE = tempObj.SHOW_FDUPDATED_DATE;

                m_ListClipTemp.Add(tempObj2);
            }
        }
    }
}

