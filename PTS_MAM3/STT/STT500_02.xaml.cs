﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSMAMFunctions;
using PTS_MAM3.ProgData;
using PTS_MAM3.WSBROADCAST;

namespace PTS_MAM3.STT
{
    public partial class STT500_02 : ChildWindow
    {
        WSBROADCASTSoapClient client = new WSBROADCASTSoapClient();                 //產生新的代理類別        
        WSMAMFunctionsSoapClient clientMAM = new WSMAMFunctionsSoapClient() ;
        public List<Class_LOG_VIDEO> ListLogVideo = new List<Class_LOG_VIDEO>();    //入庫影像檔集合
        private string m_2ndMemo = "";                                              //Secondary備註
        Class_BROADCAST m_FormData = new Class_BROADCAST();

        string m_strFileTypeName = "";   //入庫檔案類型名稱
        string m_strFileTypeID = "";     //入庫檔案類型編號
        string m_strType = "";           //類型
        string m_strID = "";             //編碼
        string m_strEpisode = "";        //集別
        string m_strCHANNELID = "";      //頻道別   
        string m_strFileNO = "";         //檔案編號(修改時用的)
        string m_strBroID = "";          //送帶轉檔單單號  
        string m_VIDEOID_PROG = "";      //VideoID
        string m_strCopyFSNO = "";       //複製或修改段落的檔案編號
        string m_strChangeFSNO = "";     //置換的檔案編號

        public STT500_02(Class_BROADCAST FormData)
        {
            InitializeComponent();   
            //rdbProg.IsChecked = true;  //預設節目被勾起

            InitializeForm();        //初始化本頁面
            m_FormData = FormData;
            ClassToForm();
        }

        //因為審核時不得修改送帶轉檔資料，因此傳送帶轉檔單單號修改的功能拿掉
        //public STT100_02(string strFromID)
        //{
        //    if (strFromID.ToString().Trim() == "")
        //        return;
        //    InitializeComponent();
        //}

        void InitializeForm() //初始化本頁面
        {     
            //下載代碼檔_影片的檔案類型  
            client.GetTBBROADCAST_CODE_TBFILE_TYPECompleted += new EventHandler<GetTBBROADCAST_CODE_TBFILE_TYPECompletedEventArgs>(client_GetTBBROADCAST_CODE_TBFILE_TYPECompleted);
            client.GetTBBROADCAST_CODE_TBFILE_TYPEAsync();

            //查詢入庫影像檔資料_透過檔案節目編號、集別及送帶轉檔單編號            
            client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted += new EventHandler<GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompletedEventArgs>(client_GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted);

            //查詢入庫影像檔資料_透過檔案節目編號、集別
            client.GetTBLOG_VIDEO_BYProgID_EpisodeCompleted += new EventHandler<GetTBLOG_VIDEO_BYProgID_EpisodeCompletedEventArgs>(client_GetTBLOG_VIDEO_BYProgID_EpisodeCompleted);

            //修改節目子集的2ND及段落區間
            client.UPDATE_TBPROG_D_ND_DURATIONCompleted += new EventHandler<UPDATE_TBPROG_D_ND_DURATIONCompletedEventArgs>(client_UPDATE_TBPROG_D_ND_DURATIONCompleted);

            //查詢節目子集是否有2ND
            client.Query_TBPROG_D_NDCompleted += new EventHandler<Query_TBPROG_D_NDCompletedEventArgs>(client_Query_TBPROG_D_NDCompleted);

            //查詢節目子集檔的主控播出提示資料
            client.Query_TBPROG_INSERT_TAPECompleted += new EventHandler<Query_TBPROG_INSERT_TAPECompletedEventArgs>(client_Query_TBPROG_INSERT_TAPECompleted);

            //刪除入庫影像檔及段落檔_BY檔案編號
            client.DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONECompleted += new EventHandler<DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONECompletedEventArgs>(client_DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONECompleted);

            //查詢TBLOG_VIDEOID_MAP的VideoID
            client.QueryVideoID_PROG_STTCompleted += new EventHandler<QueryVideoID_PROG_STTCompletedEventArgs>(client_QueryVideoID_PROG_STTCompleted);
        }

        private void ClassToForm()  //將前一頁點選的節目資料繫集到畫面上
        {         
            if (m_FormData.FSBRO_TYPE.Trim() == "")
            {
                MessageBox.Show("傳入參數異常，請聯絡系統管理員", "提示訊息", MessageBoxButton.OK);
                return;  
            }

            tbxBro_ID.Text = m_FormData.FSBRO_ID;
            tbxPROG_NAME.Text = m_FormData.FSID_NAME;
            tbxPROG_NAME.Tag = m_FormData.FSID;
            tbMEMO.Text = m_FormData.FSMEMO;    //冠宇加入20120803要加MEMO的欄位
            m_strBroID = m_FormData.FSBRO_ID.Trim();
            m_strID = m_FormData.FSID.Trim();
            m_strType = m_FormData.FSBRO_TYPE.Trim();
            m_strEpisode = m_FormData.SHOW_FNEPISODE.Trim();
            m_strCHANNELID = m_FormData.FSCHANNELID.Trim();

            if (m_FormData.SHOW_FNEPISODE != "")
            {
                tbxEPISODE.Text = m_FormData.SHOW_FNEPISODE;
                lblEPISODE_NAME.Content = m_FormData.FNEPISODE_NAME;
            }
            else
                m_strEpisode = "0";

            if (m_FormData.FSBRO_TYPE == "G")
            {
                rdbProg.IsChecked = true;
                if (m_FormData.SHOW_FNEPISODE == "")
                {
                  btnEvent.Visibility = Visibility.Collapsed;
                  btnSet.Visibility = Visibility.Collapsed;
                }
            }
            else if (m_FormData.FSBRO_TYPE == "P")
            {
                rdbPromo.IsChecked = true;
                tbFNEPISODE.Visibility = Visibility.Collapsed;
                tbxEPISODE.Visibility = Visibility.Collapsed;
                btnEPISODE.Visibility = Visibility.Collapsed;
                tbEPISODE_NAME.Visibility = Visibility.Collapsed;
                lblEPISODE_NAME.Visibility = Visibility.Collapsed;
                btnEvent.Visibility = Visibility.Collapsed;
                btnSet.Visibility = Visibility.Collapsed;
            }

            //透過節目編號去查詢入庫影像檔
            this.DGLogList.DataContext = null;
            //client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDAsync(m_strType, m_strID, m_strEpisode, m_strBroID);//要測新功能，先註解
            client.GetTBLOG_VIDEO_BYProgID_EpisodeAsync(m_strType, m_strID, m_strEpisode);
        }

        //載入入庫影像檔
        private void LoadTBLOG_VIDEO()
        {
            if (rdbProg.IsChecked == true)
            {
                if (m_strID != "" && m_strEpisode != "")
                    //client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDAsync("G", m_strID, m_strEpisode, m_strBroID);//要測新功能，先註解
                    client.GetTBLOG_VIDEO_BYProgID_EpisodeAsync("G", m_strID, m_strEpisode);
                else if (m_strID != "" && m_strEpisode == "")
                    //client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDAsync("G", m_strID, "0", m_strBroID);//要測新功能，先註解
                    client.GetTBLOG_VIDEO_BYProgID_EpisodeAsync("G", m_strID, "0");
            }
            else if (rdbPromo.IsChecked == true)
                //client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDAsync("P", m_strID, "0", m_strBroID);   //要測新功能，先註解
                client.GetTBLOG_VIDEO_BYProgID_EpisodeAsync("P", m_strID, "0");
        }

        #region 實作        

        //實作-取得代碼檔_影片的檔案類型 
        void client_GetTBBROADCAST_CODE_TBFILE_TYPECompleted(object sender, GetTBBROADCAST_CODE_TBFILE_TYPECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null )
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE_TBFILE_TYPE CodeData = new Class_CODE_TBFILE_TYPE();
                        CodeData = e.Result[i];                      
                    }
                }
            }
        }

        //實作-查詢入庫影像檔資料_透過檔案節目編號、集別及送帶轉檔單編號 
        void client_GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted(object sender, GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    this.DGLogList.DataContext = e.Result;
                    ListLogVideo = e.Result;

                    if (this.DGLogList.DataContext != null)
                    {
                        DGLogList.SelectedIndex = 0;
                    }
                }
            }
        }

        //實作-查詢入庫影像檔資料_透過檔案節目編號、集別
        void client_GetTBLOG_VIDEO_BYProgID_EpisodeCompleted(object sender, GetTBLOG_VIDEO_BYProgID_EpisodeCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null )
                {
                    ListLogVideo.Clear();                           //使用前先清空
                    this.DGLogList.DataContext = null;

                    for (int i = 0; i < e.Result.Count; i++)        //要濾掉檔案來源非外部轉入，如影帶管理系統
                    {
                        //也要慮到檔案狀態為D的，就是被置換掉，以及要過濾到檔案狀態為F的，就是被駁回的
                        if (e.Result[i].FCFROM == "N" && e.Result[i].FCFILE_STATUS != "D" && e.Result[i].FCFILE_STATUS != "F")
                            ListLogVideo.Add(e.Result[i]);
                    }

                    this.DGLogList.DataContext = ListLogVideo;

                    if (this.DGLogList.DataContext != null)
                    {
                        DGLogList.SelectedIndex = 0;
                    }
                }
            }
        }

        //實作-修改節目子集檔的2nd及期間
        void client_UPDATE_TBPROG_D_ND_DURATIONCompleted(object sender, UPDATE_TBPROG_D_ND_DURATIONCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    MessageBox.Show("修改送帶轉檔資料成功！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = true;
                    //MessageBox.Show("更新節目子集檔的2nd及期間成功！");                    
                }
                else
                {
                    MessageBox.Show("修改送帶轉檔資料失敗！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                    //MessageBox.Show("更新節目子集檔的2nd及期間失敗！");
                }             
            }
        }       
        
        //實作-查詢節目子集是否有2ND
        void client_Query_TBPROG_D_NDCompleted(object sender, Query_TBPROG_D_NDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    call2nd(e.Result.Trim());    //呼叫2ND
                }
            }
        }

        //實作-查詢節目子集檔的主控播出提示資料
        void client_Query_TBPROG_INSERT_TAPECompleted(object sender, Query_TBPROG_INSERT_TAPECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == false)          //回傳false表示無資料，要詢問使用者
                {
                    MessageBoxResult resultMsg = MessageBox.Show("沒有設定主控播出提示資料，\n按下「確定」繼續新增，\n按下「取消」回到畫面繼續編輯。", "提示訊息", MessageBoxButton.OKCancel);
                    if (resultMsg == MessageBoxResult.OK)
                        UpdateND_DURATION();
                }
                else
                    UpdateND_DURATION();        //有資料，不詢問使用者，更新送帶轉檔資料                  
            }
        }

        //實作-刪除入庫影像檔及段落檔_BY檔案編號
        void client_DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONECompleted(object sender, DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    MessageBox.Show("刪除段落資料成功！", "提示訊息", MessageBoxButton.OK);
                    LoadTBLOG_VIDEO();  //載入入庫影像檔
                }
                else
                    MessageBox.Show("刪除段落資料失敗！", "提示訊息", MessageBoxButton.OK);
            }
        }

        //實作-查詢TBLOG_VIDEOID_MAP的VideoID
        void client_QueryVideoID_PROG_STTCompleted(object sender, QueryVideoID_PROG_STTCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    m_VIDEOID_PROG = e.Result.Trim();

                    //新增段落
                    Log_Video_Seg();
                }
            }
        } 

        #endregion         

        //取得TimeCode區間
        private string CheckDuration()
        {
            string strReturn ="" ;
            for (int i = 0; i < ListLogVideo.Count; i++)
                {
                    if (ListLogVideo[i].FSARC_TYPE.Trim() == "001")
                    {
                        strReturn = Convert.ToString(TransferTimecode.timecodetoSecond(ListLogVideo[i].FSEND_TIMECODE) - TransferTimecode.timecodetoSecond(ListLogVideo[i].FSBEG_TIMECODE)).Trim();
                        return strReturn;
                    }
                }
            return "";
        }

        #region 按鈕裡的事件

        //按下確定後
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            Boolean bolcheck = false;

            for (int i = 0; i < ListLogVideo.Count; i++)
            {
                if (ListLogVideo[i].FSBRO_ID.Trim() == m_strBroID)
                    bolcheck = true;
            }

            //按下確認後更新MEMO欄位 獨立丟出去的MEMO欄位 本按鈕確定並沒有回寫資料 所以只回填MEMO欄位 Kyle 20120803新增
            if (tbxBro_ID.Text.Trim() != "")
                client.UPDATE_BROADCAST_FSMEMOAsync(tbxBro_ID.Text.Trim(), tbMEMO.Text.Trim(), UserClass.userData.FSUSER_ID);

            if (bolcheck == false)  //判斷該筆送帶轉檔單至少輸入一筆
            {
                MessageBox.Show("請先輸入至少一筆影片段落置換資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //if (m_strID.Trim() != "" && m_strEpisode.Trim() != "0")
            //    client.Query_TBPROG_INSERT_TAPEAsync(m_strID.Trim(), m_strEpisode.Trim(), "");  //查詢節目子集檔的主控播出提示資料
            //else
                UpdateND_DURATION();    //若是非節目加集別，像是以節目或是短帶送帶轉檔的就直接新增
        }

        //寫回子集的實際帶長及2nd Eventc、送帶轉檔資料
        private void UpdateND_DURATION()
        {
            string strDuration = "";      //要寫回子集的時間區間 

            strDuration = CheckDuration();

            //寫回子集的實際帶長及2nd Event
            if (strDuration == "" && m_2ndMemo != "")
                client.UPDATE_TBPROG_D_ND_DURATIONAsync(m_strID, m_strEpisode, "", m_2ndMemo, UserClass.userData.FSUSER_ID);
            else if (strDuration != "" && m_2ndMemo == "")
                client.UPDATE_TBPROG_D_ND_DURATIONAsync(m_strID, m_strEpisode, strDuration, "", UserClass.userData.FSUSER_ID);
            else if (strDuration != "" && m_2ndMemo != "")
                client.UPDATE_TBPROG_D_ND_DURATIONAsync(m_strID, m_strEpisode, strDuration, m_2ndMemo, UserClass.userData.FSUSER_ID);
            else
                MessageBox.Show("修改送帶轉檔資料成功！", "提示訊息", MessageBoxButton.OK);
            this.DialogResult = true;
        }

        //取消時，要去檢查資料表後刪除
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {     
            this.DialogResult = false;            
        }

        //開啟送帶轉檔，輸入TimeCode的介面
        private void btnSeg_Click(object sender, RoutedEventArgs e)
        {
            if (m_strType == "G" && m_strID == "")
            {
                MessageBox.Show("請先選擇節目名稱", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (m_strType == "P" && m_strID == "")
            {
                MessageBox.Show("請先選擇短帶名稱", "提示訊息", MessageBoxButton.OK);
                return;
            }

            m_strFileTypeName = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSARC_TYPE_NAME;
            m_strFileTypeID = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSARC_TYPE;

            string strFileStatus = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FCFILE_STATUS.Trim();

            if (strFileStatus == "B")
            {
                //MessageBox.Show("檔案狀態為「待送帶轉檔審核」或是「待送帶轉檔置換審核」，無法置換此檔案", "提示訊息", MessageBoxButton.OK);
                MessageBox.Show("檔案狀態為「待轉檔」，無法置換此檔案", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (strFileStatus == "S")
            {
                MessageBox.Show("檔案狀態為「轉檔中」，無法置換此檔案", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (strFileStatus == "A")
            {
                MessageBox.Show("檔案狀態為「待入庫審核」，無法置換此檔案", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (strFileStatus == "W")
            {
                MessageBox.Show("檔案狀態為「待入庫置換審核」，無法置換此檔案", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (strFileStatus == "Y")
            {
                MessageBox.Show("檔案狀態為「入庫」，請改走入庫法置換流程", "提示訊息", MessageBoxButton.OK);
                return;
            }

            for (int i = 0; i < ListLogVideo.Count; i++)
            {
                //擋同樣的檔案，不能新增一筆以上的檔案置換資料
                if (ListLogVideo[i].FSCHANGE_FILE_NO.Trim().Equals(((Class_LOG_VIDEO)DGLogList.SelectedItem).FSFILE_NO.Trim()))
                {
                    MessageBox.Show("檔案編號為：" + ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSFILE_NO.Trim() + "，只能新增一筆置換資料！", "提示訊息", MessageBoxButton.OK);
                    return;
                }
            }

            m_strCopyFSNO = "";                                                      //新增時，沒有傳到下一頁的檔案編號
            m_strChangeFSNO = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSFILE_NO;   //新增時，將要傳到下一頁的置換檔案編號補上

            //新增前先取VideoID，最後一個參數為true，表示是置換
            client.QueryVideoID_PROG_STTAsync(m_strID, m_strEpisode, m_strFileTypeID, true);
        }

        //修改段落檔
        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGLogList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預修改的資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //要串給長結點用的
            string strProgName = tbxPROG_NAME.Text.Trim();
            string strEpisodeName = "";

            if (m_strType == "G")
            {
                if (m_strEpisode == "0")
                    strEpisodeName = strProgName;
                else
                    strEpisodeName = lblEPISODE_NAME.Content.ToString();
            }                
            else
                strEpisodeName = strProgName;   

            m_strFileTypeName  = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSARC_TYPE_NAME ;
            m_strFileTypeID  = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSARC_TYPE ;

            //當送帶轉檔置換單不同時，表示這不是同一次的送帶轉檔置換申請，因此無法修改
            if (m_strBroID.Trim() != ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSBRO_ID.Trim())
            {
                MessageBox.Show("此筆並非為當次申請的段落資料，因此無法修改", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (((Class_LOG_VIDEO)DGLogList.SelectedItem).FCFILE_STATUS.Trim() != "B")
            {
                MessageBox.Show("此筆段落資料狀態不是「待轉檔」，因此無法修改", "提示訊息", MessageBoxButton.OK);
                return;
            }

            m_strCopyFSNO = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSFILE_NO;            //修改時，將要傳到下一頁的檔案編號補上
            m_strChangeFSNO = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSCHANGE_FILE_NO;   //修改時，將要傳到下一頁的置換檔案編號補上 
            m_VIDEOID_PROG = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSVIDEO_PROG.Trim(); //若是修改，表示不用重新取VideoID

            //修改段落
            Log_Video_Seg(); 
        }

        //刪除段落檔
        private void btnDel_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGLogList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預刪除的段落資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //當送帶轉檔單不同時，表示這不是同一次的送帶轉檔申請，因此無法修改
            if (m_strBroID.Trim() != ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSBRO_ID.Trim())
            {
                MessageBox.Show("此筆並非為當次申請的段落資料，因此無法刪除", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (((Class_LOG_VIDEO)DGLogList.SelectedItem).FCFILE_STATUS.Trim() != "B")
            {
                MessageBox.Show("此筆段落資料狀態不是「待轉檔」，因此無法刪除", "提示訊息", MessageBoxButton.OK);
                return;
            }

            MessageBoxResult resultMsg = MessageBox.Show("確定要刪除「" + ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSARC_TYPE_NAME + "」的段落資料嗎?", "提示訊息", MessageBoxButton.OKCancel);
            if (resultMsg == MessageBoxResult.Cancel)
                return;
            else
                //刪除段落檔
                client.DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONEAsync(((Class_LOG_VIDEO)DGLogList.SelectedItem).FSFILE_NO, UserClass.userData.FSUSER_ID.ToString()); 
        }

        //新增、修改段落資料
        private void Log_Video_Seg()
        {
            //要串給長結點用的
            string strProgName = tbxPROG_NAME.Text.Trim();
            string strEpisodeName = "";

            if (m_strType == "G")
            {
                if (m_strEpisode == "0")
                    strEpisodeName = strProgName;
                else
                    strEpisodeName = lblEPISODE_NAME.Content.ToString();
            }
            else
                strEpisodeName = strProgName;

            //倒數第二參數為false表示不是要複製段落
            STT100_01_01 STT100_01_01_frm = new STT100_01_01(m_strFileTypeName, m_strFileTypeID, m_strType, m_strID, m_strEpisode, m_strCHANNELID, m_strCopyFSNO, m_strBroID, strProgName, strEpisodeName, m_strChangeFSNO, false, m_VIDEOID_PROG,"");
            STT100_01_01_frm.Show();

            //若是有填入段落檔，就要去讀取該節目集別的段落資料，並且秀在畫面上
            STT100_01_01_frm.Closing += (s, args) =>
            {
                if (STT100_01_01_frm.DialogResult == true)
                    LoadTBLOG_VIDEO();
            };
        }

        //呼叫Secondary Event新增程式
        private void btnEvent_Click(object sender, RoutedEventArgs e)
        {
            if (m_strType == "G" && m_strID == "")
            {
                MessageBox.Show("請先選擇節目名稱", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (m_strType == "G" && m_strID != "" && m_strEpisode == "0")
            {
                MessageBox.Show("請先選擇子集名稱", "提示訊息", MessageBoxButton.OK);
                return;
            }

            if (m_2ndMemo.Trim() == "")
                client.Query_TBPROG_D_NDAsync(m_strID, m_strEpisode);   //先查詢出該子集是否有2nd資料，若有要帶入修改
            else
                call2nd(m_2ndMemo.Trim());           //呼叫2ND       
        }

        //設定主控播出提示資料
        private void btnSet_Click(object sender, RoutedEventArgs e)
        {
            if (m_strType == "G" && m_strID == "")
            {
                MessageBox.Show("請先選擇節目名稱", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (m_strType == "G" && m_strID != "" && m_strEpisode == "0")
            {
                MessageBox.Show("請先選擇子集名稱", "提示訊息", MessageBoxButton.OK);
                return;
            }

            PTS_MAM3.PGM.PGM100_05 PGM100_05_Frm = new PTS_MAM3.PGM.PGM100_05();

            PGM100_05_Frm.textBoxProgID.Text = m_strID;
            PGM100_05_Frm.textBoxProgName.Text = tbxPROG_NAME.Text.Trim();
            PGM100_05_Frm.textBoxEpisode.Text = m_strEpisode;
            PGM100_05_Frm.Show();
            PGM100_05_Frm.Closed += (s, args) =>
            {
            };
        }      

        //呼叫2ND
        private void call2nd(string str2nd)
        {          
            PGM.PGM100_02 SetLouthKey_Frm = new PGM.PGM100_02();

            //修改時傳入組好的備註字串
            SetLouthKey_Frm.InLouthKeyString = str2nd;

            SetLouthKey_Frm.Show();
            SetLouthKey_Frm.Closed += (s, args) =>
            {
                if (SetLouthKey_Frm.OutLouthKeyString != null)
                {
                    //傳回選擇好的備註字串                  
                    m_2ndMemo = SetLouthKey_Frm.OutLouthKeyString.Trim();
                }
            };
        }

        #endregion

        #region DataGrid變色

        //DataGrid變色
        private void ChildWindow_Loaded(object sender, RoutedEventArgs e)
        {
            DGLogList.LoadingRow += new EventHandler<DataGridRowEventArgs>(DGLogList_LoadingRow);
        }

        //實作-DataGrid變色
        void DGLogList_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Background = new SolidColorBrush(Colors.White);
            e.Row.Foreground = new SolidColorBrush(Colors.Black);

            Class_LOG_VIDEO LOG_VIDEO = e.Row.DataContext as Class_LOG_VIDEO;

            if (LOG_VIDEO.FSBRO_ID.Trim() != m_strBroID.Trim())
                e.Row.Background = new SolidColorBrush(Colors.LightGray);

            if (LOG_VIDEO.FSCHANGE_FILE_NO != "")
                e.Row.Foreground = new SolidColorBrush(Colors.Red); 
        }

        #endregion

  
    }
}

