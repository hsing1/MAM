﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using PTS_MAM3.STT;
using PTS_MAM3.WSBROADCAST;
using System.Windows.Browser;
using System.Windows.Data;

namespace PTS_MAM3.STT
{
    public partial class STT800 : Page
    {
        WSBROADCASTSoapClient client = new WSBROADCASTSoapClient();             //產生新的代理類別
        List<Class_BROADCAST> m_ListFormData = new List<Class_BROADCAST>();     //記錄查詢到的成案資料
        string m_FileUpload_Path = "";                                          //檔案上傳路徑

        public STT800()
        {
            InitializeComponent();

            //取得所有的送帶轉檔檔
            //client.fnGetTBBROADCAST_ALLCompleted += new EventHandler<fnGetTBBROADCAST_ALLCompletedEventArgs>(client_fnGetTBBROADCAST_ALLCompleted);
            //client.fnGetTBBROADCAST_ALLAsync(); //不Load全部，只Load自己新增的

            //取得所有的送帶轉檔檔_BY新增者
            client.fnGetTBBROADCAST_ALL_BYUSERIDCompleted += new EventHandler<fnGetTBBROADCAST_ALL_BYUSERIDCompletedEventArgs>(client_fnGetTBBROADCAST_ALL_BYUSERIDCompleted);
            
            //列印送帶轉檔單
            client.CallREPORT_BROCompleted += new EventHandler<CallREPORT_BROCompletedEventArgs>(client_CallREPORT_BROCompleted);

            //列印送帶轉檔單_多筆短帶
            client.CallREPORT_BRO_PROMOCompleted += new EventHandler<CallREPORT_BRO_PROMOCompletedEventArgs>(client_CallREPORT_BRO_PROMOCompleted);

            //取得檔案上傳區
            client.GET_FILEUPLOAD_PATHCompleted += new EventHandler<GET_FILEUPLOAD_PATHCompletedEventArgs>(client_GET_FILEUPLOAD_PATHCompleted);
            client.GET_FILEUPLOAD_PATHAsync();

            //用送帶轉檔單號取得入庫檔是否有超過一筆
            client.QUERY_TBLOG_VIDEO_COUNTCompleted += new EventHandler<QUERY_TBLOG_VIDEO_COUNTCompletedEventArgs>(client_QUERY_TBLOG_VIDEO_COUNTCompleted);

            //查詢送帶轉檔檔是否有超過一筆的列印資料
            client.QUERY_TBBROADCAST_PRINT_CHECKCompleted += new EventHandler<QUERY_TBBROADCAST_PRINT_CHECKCompletedEventArgs>(client_QUERY_TBBROADCAST_PRINT_CHECKCompleted);

            //Load_TBBROADCAST_ALL_BYUSERID();    //透過登入帳號取得送帶轉檔檔
        }

        #region 實作

        //實作-列印送帶轉檔單
        void client_CallREPORT_BROCompleted(object sender, CallREPORT_BROCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    if (e.Result.ToString().StartsWith("錯誤"))
                        MessageBox.Show(e.Result.ToString(), "提示訊息", MessageBoxButton.OK);
                    else
                        HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + e.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,resizable=yes");
                }
                else
                    MessageBox.Show("查無相關資料，或該筆資料已不存在資料庫中。", "提示訊息", MessageBoxButton.OK);
            };
        }

        //實作-列印送帶轉檔單_多筆短帶
        void client_CallREPORT_BRO_PROMOCompleted(object sender, CallREPORT_BRO_PROMOCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    if (e.Result.ToString().StartsWith("錯誤"))
                        MessageBox.Show(e.Result.ToString(), "提示訊息", MessageBoxButton.OK);
                    else
                        HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + e.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,resizable=yes");
                }
                else
                    MessageBox.Show("查無相關資料，或該筆資料已不存在資料庫中。", "提示訊息", MessageBoxButton.OK);
            };
        }

        //實作-取得所有的送帶轉檔檔
        void client_fnGetTBBROADCAST_ALLCompleted(object sender, fnGetTBBROADCAST_ALLCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    List<Class_BROADCAST> ListBROADCAST = new List<Class_BROADCAST>();

                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        if (e.Result[i].FCCHANGE.Trim() != "Y")    //要濾掉只剩下送帶轉檔的表單
                        {
                            ListBROADCAST.Add(e.Result[i]);
                        }                       
                    }
                    PagedCollectionView pcv = new PagedCollectionView(ListBROADCAST);
                    DGBroListDouble.ItemsSource = pcv;
                    DataPager.Source = pcv;  
                }
            }             
        }

        //實作-取得所有的送帶轉檔檔_BY新增者
        void client_fnGetTBBROADCAST_ALL_BYUSERIDCompleted(object sender, fnGetTBBROADCAST_ALL_BYUSERIDCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null)
                {  
                    PagedCollectionView pcv = new PagedCollectionView(e.Result);                    
                    DGBroListDouble.ItemsSource = pcv;
                    DataPager.Source = pcv;
                }
            }   
        }

        //實作-取得檔案上傳區
        void client_GET_FILEUPLOAD_PATHCompleted(object sender, GET_FILEUPLOAD_PATHCompletedEventArgs e)
        {
            m_FileUpload_Path = e.Result;
        }

        //用送帶轉檔單號取得入庫檔是否有超過一筆
        void client_QUERY_TBLOG_VIDEO_COUNTCompleted(object sender, QUERY_TBLOG_VIDEO_COUNTCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == true)
                    OpenSTT100_07();    //短帶專屬修改頁面(適用於只有一筆)
                else
                    OpenSTT100_02();
            }
            else
                OpenSTT100_02();
        }

        //實作-查詢送帶轉檔檔是否有超過一筆的列印資料
        void client_QUERY_TBBROADCAST_PRINT_CHECKCompleted(object sender, QUERY_TBBROADCAST_PRINT_CHECKCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result == true )
                {    
                    //要詢問要不要一起列印其他的送帶轉檔單
                    MessageBoxResult resultMsg = MessageBox.Show("確定要列印多筆送帶轉檔單嗎?", "提示訊息", MessageBoxButton.OKCancel);
                    if (resultMsg == MessageBoxResult.OK)                    
                        PrintSTT_PROMO();   //列印多張的送帶轉檔單                  
                    else                   
                      PrintSTT();           //列印單張送帶轉檔單                    
                }
                else               
                  PrintSTT();               //列印單張送帶轉檔單                      
            }  
        }

        #endregion

        //載入送帶轉檔資料_BY新增者
        private void Load_TBBROADCAST_ALL_BYUSERID()
        {
            DateTime dtStart = DateTime.Now.AddYears(-1);
            DateTime dtEnd = DateTime.Now.AddDays(1);       //要加一天才能帶出今天的
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
            client.fnGetTBBROADCAST_ALL_BYUSERIDAsync(UserClass.userData.FSUSER_ID.ToString().Trim(), dtStart, dtEnd);
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        //新增
        private void btnBroAdd_Click(object sender, RoutedEventArgs e)
        {
            STT100_01 Bro_Add_frm = new STT100_01();
            Bro_Add_frm.Show();

            Bro_Add_frm.Closing += (s, args) =>
            {
                if (Bro_Add_frm.DialogResult == true)
                    Load_TBBROADCAST_ALL_BYUSERID();        
            };
        }
        
        //新增短帶
        private void btnBroPromoAdd_Click(object sender, RoutedEventArgs e)
        {
            STT100_07 Bro_Promo_Add_frm = new STT100_07("","");
            Bro_Promo_Add_frm.Show();

            Bro_Promo_Add_frm.Closing += (s, args) =>
            {
                if (Bro_Promo_Add_frm.DialogResult == true)
                    Load_TBBROADCAST_ALL_BYUSERID();
            };
        }

        //修改
        private void btBroModify_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGBroListDouble.SelectedItem == null)
            {
                MessageBox.Show("請選擇預修改的送帶轉檔資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            if (((Class_BROADCAST)DGBroListDouble.SelectedItem).FCCHECK_STATUS != "N")
            {
                MessageBox.Show("該筆送帶轉檔資料不是「審核中」，無法修改", "提示訊息", MessageBoxButton.OK);
                return;
            }

            if (((Class_BROADCAST)DGBroListDouble.SelectedItem).FSBRO_TYPE == "P")
            {
                client.QUERY_TBLOG_VIDEO_COUNTAsync(((Class_BROADCAST)DGBroListDouble.SelectedItem).FSBRO_ID.Trim());
            }
            else
            {
                OpenSTT100_02();  //開啟送帶轉檔修改頁面
            }   
        }

        //查詢
        private void btnBroQuery_Click(object sender, RoutedEventArgs e)
        {
            STT800_04 STT800_04_frm = new STT800_04();
            STT800_04_frm.Show();

            STT800_04_frm.Closed += (s, args) =>
            {
                if (STT800_04_frm.m_ListFormData.Count != 0)
                {
                    DGBroListDouble.ItemsSource = null;
                    PagedCollectionView pcv = new PagedCollectionView(STT800_04_frm.m_ListFormData);                   
                    DGBroListDouble.ItemsSource = pcv;
                    DataPager.Source = pcv;  
                }
            };
        }

        //開啟送帶轉檔修改頁面
        private void OpenSTT100_02()
        {
            STT100_02 STT100_02_frm = new STT100_02(((Class_BROADCAST)DGBroListDouble.SelectedItem));
            STT100_02_frm.Show();

            STT100_02_frm.Closing += (s, args) =>
            {
                if (STT100_02_frm.DialogResult == true)
                    Load_TBBROADCAST_ALL_BYUSERID();
            };
        }

        //開啟送帶轉檔短帶修改頁面
        private void OpenSTT100_07()
        {
            STT100_07 STT100_07_frm = new STT100_07(((Class_BROADCAST)DGBroListDouble.SelectedItem));
            STT100_07_frm.Show();

            STT100_07_frm.Closing += (s, args) =>
            {
                if (STT100_07_frm.DialogResult == true)
                    Load_TBBROADCAST_ALL_BYUSERID();
            };
        }

        //更新
        private void btnBroRefresh_Click(object sender, RoutedEventArgs e)
        {
            Load_TBBROADCAST_ALL_BYUSERID();        
        }

        //列印
        private void btnRpt_Click(object sender, RoutedEventArgs e)
        {
             //判斷是否有選取DataGrid
            if (DGBroListDouble.SelectedItem == null)
            {
                MessageBox.Show("請選擇預列印的節目資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            string strPrintID = ((Class_BROADCAST)DGBroListDouble.SelectedItem).FSPRINTID.ToString().Trim();

            if (strPrintID.Trim() == "")
            {
                PrintSTT(); //列印單張送帶轉檔單
            }
            else
            {
                //查詢送帶轉檔檔是否有超過一筆的列印資料
                client.QUERY_TBBROADCAST_PRINT_CHECKAsync(((Class_BROADCAST)DGBroListDouble.SelectedItem).FSPRINTID.ToString().Trim());       
            }                
        }

        //連續點擊-檢視送帶轉檔  
        private void DGBroListDouble_RowDoubleClicked(object sender, DataGridDCTest.DataGridRowClickedArgs e)
        {
            DataGridDCTest.DoubleClickDataGrid dcdg = sender as DataGridDCTest.DoubleClickDataGrid;

            //判斷是否有選取DataGrid
            if (dcdg.SelectedItem == null)
            {
                MessageBox.Show("請選擇預檢視的送帶轉檔資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            STT100_06 STT100_06_frm = new STT100_06(((FlowFieldCom.Class_BROADCAST)DGBroListDouble.SelectedItem).FSBRO_ID.ToString().Trim());
            STT100_06_frm.Show();
        }

        //檔案上傳區
        private void btnSTT_Click(object sender, RoutedEventArgs e)
        {
            HtmlPage.Window.Navigate(new Uri(m_FileUpload_Path, UriKind.Absolute), "_blank", "resizable=yes");
        }       
 
        //列印單張送帶轉檔單
        private void PrintSTT()
        {
            //列印成案單(丟入參數：成案單編號、查詢者、SessionID)
            client.CallREPORT_BROAsync(((Class_BROADCAST)DGBroListDouble.SelectedItem).FSBRO_ID.ToString().Trim(), UserClass.userData.FSUSER_ID.ToString().Trim(), UserClass.userData.FSSESSION_ID.ToString().Trim());
        }

        //列印送帶轉檔單_多筆短帶資料
        private void PrintSTT_PROMO()
        {
            //列印成案單(丟入參數：成案單編號、查詢者、SessionID)
            client.CallREPORT_BRO_PROMOAsync(((Class_BROADCAST)DGBroListDouble.SelectedItem).FSPRINTID.ToString().Trim(), UserClass.userData.FSUSER_ID.ToString().Trim(), UserClass.userData.FSSESSION_ID.ToString().Trim());
        }
    }
}
