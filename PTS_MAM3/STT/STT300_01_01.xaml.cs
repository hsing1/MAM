﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using PTS_MAM3.WSSTT;
using System.IO;
namespace PTS_MAM3.STT
{
    public partial class STT300_01_01 : ChildWindow
    {
        WSSTTSoapClient client = new WSSTTSoapClient();

        public string strSelect_File_Path = "";                         //選取的檔案路徑
        public string strSelect_File_Name = "";                         //選取的檔案名稱
        public string _fsTREE_PATH = "";                                //樹狀路徑

        string m_strUpLoadPath = "";                                    //送帶轉檔暫存區的路徑
        List<string> DirectoriesList = new List<string>();              //目錄集合
        List<string> FileList = new List<string>();                     //檔案集合
        List<Class_File_Info> FileInformationList = new List<Class_File_Info>();   //檔案內容集合       
        string m_strFileDirectory = "";                                 //記錄目錄資料夾
        string m_strUpLoadType = "";                                    //數位檔案上傳位置(Network或是Fiber)

        bool? NeedFilter = null;//判斷需不需要過濾檔案格式,2016-01-21 by Jarvis

        public STT300_01_01(string strUpLoadType)
        {
            m_strUpLoadType = strUpLoadType;
            InitializeComponent();
            InitializeForm();               //初始化本頁面      

            busyLoadingLeft.IsBusy = true;  //等待讀取server資料夾名稱
        }

        //新增一個多載 2016-01-21 by Jarvis
        public STT300_01_01(string strUpLoadType, bool needFilter)
        {
            m_strUpLoadType = strUpLoadType;
            InitializeComponent();
            InitializeForm();               //初始化本頁面      
            NeedFilter = needFilter;
            busyLoadingLeft.IsBusy = true;  //等待讀取server資料夾名稱
        }

        string DirectoryPath = "";
        string FileFullPath = "";
        //string LastDiretoryName = "";
        public STT300_01_01(string strUpLoadType, string memo)
        {
            m_strUpLoadType = strUpLoadType;
            InitializeComponent();
            FileFullPath = GetFileFullPath(memo);
            if (FileFullPath != "")
            {
                DirectoryPath = GetDiretoryPath(FileFullPath);
            }
            //LastDiretoryName = GetLastDiretoryName();
            InitializeForm();               //初始化本頁面

            busyLoadingLeft.IsBusy = true;  //等待讀取server資料夾名稱

        }

        string GetFileFullPath(string memoText)
        {//﹝

            string[] strArr = memoText.Split(new string[] { "----------------分隔線-------------------", "實體路徑：", "﹝" }, StringSplitOptions.RemoveEmptyEntries);
            
           // int prefixIndex = strArr[0].IndexOf(prefix);

            string resultString = "";
            if (memoText.Contains("﹝原單"))
            {
                resultString = strArr[strArr.Length - 2].Trim();
            }
            else
            {
                resultString = strArr[strArr.Length-1].Trim();
            }
            return resultString;

            //string[] strArr = memoText.Split(new string[] { "----------------分隔線-------------------" }, StringSplitOptions.RemoveEmptyEntries);
            //string prefix = "實體路徑：";
            //int prefixIndex = strArr[strArr.Length - 1].IndexOf(prefix);

            //if (prefixIndex < 0)
            //    return "";

            //return strArr[strArr.Length - 1].Substring(prefixIndex + prefix.Length);
        }

        string GetDiretoryPath(string fullPath)
        {
            string Directory = "";
            try
            {
                Directory = System.IO.Path.GetDirectoryName(fullPath);
            }
            catch (Exception ex)
            {

            }
            return Directory;
        }

        string GetLastDiretoryName()
        {
            string lastDiretory = DirectoryPath.Substring(DirectoryPath.LastIndexOf("\\") + 1); ;
            return lastDiretory;
        }

        void InitializeForm() //初始化本頁面
        {
            //取得送帶轉檔暫存區的路徑
            client.GetSTTFilePathCompleted += new EventHandler<GetSTTFilePathCompletedEventArgs>(client_GetSTTFilePathCompleted);
            client.GetSTTFilePathAsync(m_strUpLoadType);

            //取得最主要目錄
            client.GetTopDirectoriesCompleted += new EventHandler<GetTopDirectoriesCompletedEventArgs>(client_GetTopDirectoriesCompleted);

            //取得目錄下的檔案
            client.GetDirectorieFilesCompleted += new EventHandler<GetDirectorieFilesCompletedEventArgs>(client_GetDirectorieFilesCompleted);

            client.GetDirectorieFiles_Without_FilterCompleted += new EventHandler<GetDirectorieFiles_Without_FilterCompletedEventArgs>(client_GetDirectorieFiles_Without_FilterCompleted);
        }

        void client_GetDirectorieFiles_Without_FilterCompleted(object sender, GetDirectorieFiles_Without_FilterCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                FileInformationList.Clear();

                if (e.Result != null)
                {
                    //lbFileList.ItemsSource = e.Result;
                    this.DataGrid_Result.ItemsSource = e.Result;
                    acbFileName.ItemsSource = e.Result;

                    if (isAutoSelectFlag)
                    {
                        isAutoSelectFlag = false;
                        acbFileName.Text = System.IO.Path.GetFileName(FileFullPath);
                        AutoSelectMethod();
                    }
                }
            }

            this.RadBusyIndicator.IsBusy = false;
        }

        #region 實作

        //實作-取得最主要目錄
        void client_GetTopDirectoriesCompleted(object sender, GetTopDirectoriesCompletedEventArgs e)
        {
            busyLoadingLeft.IsBusy = false;

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    //最主要的目錄節點
                    RadTreeViewItem newNode = new RadTreeViewItem();
                    newNode.Header = "轉檔中心數位檔案上傳區";
                    newNode.Tag = m_strUpLoadPath;

                    TreeViewFileList.Items.Add(newNode);

                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        RadTreeViewItem newNodeChild = new RadTreeViewItem();
                        newNodeChild.Header = e.Result[i].Replace(m_strUpLoadPath, "");
                        newNodeChild.Tag = e.Result[i];
                        newNode.Items.Add(newNodeChild);
                    }

                    newNode.IsExpanded = true;

                    if (FileFullPath != "")
                    {

                        foreach (RadTreeViewItem rtv in newNode.Items)
                        {

                            if (rtv.Tag.ToString() == DirectoryPath || DirectoryPath.Contains(rtv.Tag.ToString()))
                            {
                                rtv.IsExpanded = true;
                                rtv.Focus();
                                isAutoSelectFlag = true;
                                client.GetDirectorieFilesAsync(DirectoryPath);
                                RadBusyIndicator.IsBusy = true;
                                return;
                            }
                        }
                        //有做到這裏表示沒有符合的節點
                        //提醒or~?
                        isAutoSelectFlag = false;
                    }
                    else
                    {
                        client.GetDirectorieFilesAsync(m_strUpLoadPath);
                        RadBusyIndicator.IsBusy = true;
                    }
                }
            }
        }
        bool isAutoSelectFlag = false;
        //實作-取得目錄下的檔案
        void client_GetDirectorieFilesCompleted(object sender, GetDirectorieFilesCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                FileInformationList.Clear();

                if (e.Result != null)
                {
                    //lbFileList.ItemsSource = e.Result;
                    this.DataGrid_Result.ItemsSource = e.Result;
                    acbFileName.ItemsSource = e.Result;

                    if (isAutoSelectFlag)
                    {
                        isAutoSelectFlag = false;
                        acbFileName.Text = System.IO.Path.GetFileName(FileFullPath);
                        AutoSelectMethod();
                    }
                    //原本的作法是程式塞TextBlock到ListBox裡，現在改用Bind的方式
                    //for (int i = 0; i < e.Result.Count; i++)
                    //{
                    //    FileList.Add(e.Result[i]);

                    //    TextBlock tbFile = new TextBlock();
                    //    tbFile.Text = e.Result[i];
                    //    tbFile.Tag = e.Result[i];
                    //    lbFileList.Items.Add(tbFile);
                    //}
                }

            }

            this.RadBusyIndicator.IsBusy = false;
        }

        //實作-取得送帶轉檔暫存區的路徑
        void client_GetSTTFilePathCompleted(object sender, GetSTTFilePathCompletedEventArgs e)
        {
            busyLoadingLeft.IsBusy = false;

            if (e.Error == null)
            {
                if (e.Result != "")
                {
                    m_strUpLoadPath = e.Result;
                    m_strFileDirectory = m_strUpLoadPath;
                    client.GetTopDirectoriesAsync(m_strUpLoadType);    //取得最主要目錄

                    busyLoadingLeft.IsBusy = true;
                }
            }
        }

        # endregion


        //選取時觸發
        private void TreeViewFileList_Selected(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            this.RadBusyIndicator.IsBusy = true;
            RadTreeViewItem treeNode = (RadTreeViewItem)e.Source;
            if (acbFileName.Text != "")
            {
                acbFileName.Text = "";
            }
            //取得目錄列表，取得前先刪除ViewItem            
            preLoadDirectoryList(treeNode.Tag.ToString(), treeNode);

            //取得檔案列表，取得前先刪除ListBox
            //lbFileList.Items.Clear();  //現在用Binding的方式，不用清除ListBox
            m_strFileDirectory = treeNode.Tag.ToString();

            //根據NeedFilter判斷是否需要過濾檔案格式 2016-01-21 by Jarvis
            if (NeedFilter == null)
                client.GetDirectorieFilesAsync(treeNode.Tag.ToString());
            else
                client.GetDirectorieFiles_Without_FilterAsync(treeNode.Tag.ToString());

        }

        //第一次點選時觸發
        private void TreeViewFileList_LoadOnDemand(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            //因為要求即時，因此每次點選都重新去找，LoadOnDemand就不作事          
            ((RadTreeViewItem)(e.Source)).IsLoadOnDemandEnabled = false;
        }

        //取得目錄列表
        private void preLoadDirectoryList(string strPath, RadTreeViewItem treeNode)
        {
            WSSTTSoapClient clientDirectory = new WSSTTSoapClient();         //必須在裡面宣告，complete事件會重覆跑
            clientDirectory.GetDirectoriesAsync(treeNode.Tag.ToString());    //透過路徑，取得該路徑底下的資料夾

            clientDirectory.GetDirectoriesCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null && args.Result.Count > 0)
                    {
                        treeNode.Items.Clear();

                        for (int i = 0; i < args.Result.Count; i++)
                        {
                            RadTreeViewItem newNode = new RadTreeViewItem();

                            if (args.Result[i].Replace(treeNode.Tag.ToString().Trim(), "").Trim().StartsWith("\\") == true)
                                newNode.Header = args.Result[i].Replace(treeNode.Tag.ToString().Trim(), "").Substring(1);
                            else
                                newNode.Header = args.Result[i].Replace(treeNode.Tag.ToString().Trim(), "");

                            newNode.Tag = args.Result[i];
                            treeNode.Items.Add(newNode);
                        }
                    }
                }
                treeNode.IsLoadOnDemandEnabled = false;
            };
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.DataGrid_Result.SelectedItem == null)
            {
                MessageBox.Show("請先選擇欲轉檔的檔案", "提示訊息", MessageBoxButton.OK);
                return;
            }

            this.strSelect_File_Path = ((Class_File_Info)this.DataGrid_Result.SelectedItem).FSFile_Path.ToString().Trim();
            this.strSelect_File_Name = ((Class_File_Info)this.DataGrid_Result.SelectedItem).FSFile_Name.ToString().Trim();

            //組合樹狀路徑
            if (this.TreeViewFileList.SelectedItem != null)
                this._fsTREE_PATH = "\\\\" + (this.TreeViewFileList.SelectedItem as RadTreeViewItem).FullPath + "\\" + this.strSelect_File_Name;

            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        void AutoSelectMethod()
        {

            Class_File_Info fileInfo = ((Class_File_Info)acbFileName.SelectedItem);

            if (fileInfo != null)
            {
                strSelect_File_Path = ((Class_File_Info)acbFileName.SelectedItem).FSFile_Path.ToString().Trim();
                strSelect_File_Name = ((Class_File_Info)acbFileName.SelectedItem).FSFile_Name.ToString().Trim();

                this.DataGrid_Result.ItemsSource = new List<Class_File_Info>() { fileInfo };
            }
            else
            {
                MessageBox.Show("找不到檔案!");
            }
            isAutoSelectFlag = false;

        }

        //選取確認
        private void btnEnter_Click(object sender, RoutedEventArgs e)
        {
            if (acbFileName.Text == "")
            {
                MessageBox.Show("請輸入影片名稱", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (acbFileName.SelectedItem == null)
            {
                MessageBox.Show("查無該檔案名稱，請檢查", "提示訊息", MessageBoxButton.OK);
                return;
            }

            strSelect_File_Path = ((Class_File_Info)acbFileName.SelectedItem).FSFile_Path.ToString().Trim();
            strSelect_File_Name = ((Class_File_Info)acbFileName.SelectedItem).FSFile_Name.ToString().Trim();
            this.DialogResult = true;

        }


    }

}

