﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSBROADCAST;
using PTS_MAM3.WSMAMFunctions;
using System.Text;
using System.Collections.ObjectModel;
using PTS_MAM3.SR_WSTSM_GetFileInfo;

namespace PTS_MAM3.STT
{
    public partial class STT300_03 : ChildWindow
    {
        WSBROADCASTSoapClient client = new WSBROADCASTSoapClient();                 //產生新的代理類別        
        public List<Class_LOG_VIDEO_SEG> ListSeg = new List<Class_LOG_VIDEO_SEG>();        //段落集合

        string m_strFileTypeName = "";   //入庫檔案類型名稱
        string m_strFileTypeID = "";    //入庫檔案類型編號
        string m_strType = "";           //類型
        string m_strID = "";             //編碼
        string m_strEpisode = "";       //集別
        string m_strFileNOEdit = "";    //檔案編號(修改時用的)
        string m_strBroID = "";         //送帶轉檔單單號

        public STT300_03(string strFileTypeName, string strFileTypeID, string strType, string strID, string strEpisode, Class_LOG_VIDEO log_video, string strBroID)
        {
            InitializeComponent();

            m_strFileTypeName = strFileTypeName.Trim();
            m_strFileTypeID = strFileTypeID.Trim();
            m_strType = strType.Trim();
            m_strID = strID.Trim();
            m_strEpisode = strEpisode.Trim();
            tbFileTypeName.Text = m_strFileTypeName;
            m_strFileNOEdit = log_video.FSFILE_NO;
            this.tbAudioTrack.Text = log_video.FSTRACK;
            m_strBroID = strBroID;

            InitializeForm();        //初始化本頁面

            client.GetTBLOG_VIDEO_SEG_BYFileIDAsync(m_strFileNOEdit);
            client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDAsync(m_strType, m_strID, m_strEpisode, m_strBroID);
        }

        public STT300_03(string strFileTypeName, string strFileTypeID, string strType, string strID, string strEpisode, Class_LOG_VIDEO log_video, string strBroID, bool needQuery)
        {
            InitializeComponent();

            m_strFileTypeName = strFileTypeName.Trim();
            m_strFileTypeID = strFileTypeID.Trim();
            m_strType = strType.Trim();
            m_strID = strID.Trim();
            m_strEpisode = strEpisode.Trim();
            tbFileTypeName.Text = m_strFileTypeName;
            m_strFileNOEdit = log_video.FSFILE_NO;
            this.tbAudioTrack.Text = log_video.FSTRACK;
            m_strBroID = strBroID;

            InitializeForm();        //初始化本頁面

            if (needQuery)
                client.GetTBLOG_VIDEO_SEG_BYFileIDAsync(m_strFileNOEdit);
            else
            {
                ListSeg = log_video.VideoListSeg;
                DGSeg.DataContext = ListSeg;

            }
            client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDAsync(m_strType, m_strID, m_strEpisode, m_strBroID);
        }

        void InitializeForm() //初始化本頁面
        {
            //透過檔案編號取得段落檔
            client.GetTBLOG_VIDEO_SEG_BYFileIDCompleted += new EventHandler<GetTBLOG_VIDEO_SEG_BYFileIDCompletedEventArgs>(client_GetTBLOG_VIDEO_SEG_BYFileIDCompleted);

            //查詢入庫影像檔資料_透過檔案節目編號及集別            
            client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted += new EventHandler<GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompletedEventArgs>(client_GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted);
        }

        #region 實作

        //實作透過檔案編號取得段落檔
        void client_GetTBLOG_VIDEO_SEG_BYFileIDCompleted(object sender, GetTBLOG_VIDEO_SEG_BYFileIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    ListSeg = e.Result;
                    DGSeg.DataContext = ListSeg;
                }
            }
        }

        //實作-查詢入庫影像檔資料_透過檔案節目編號及集別
        void client_GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted(object sender, GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        if (m_strFileNOEdit.Trim() == (e.Result[i]).FSFILE_NO.ToString().Trim())
                        {
                            tbxTITLE.Text = (e.Result[i]).FSTITLE.ToString();
                            tbxDESCRIPTION.Text = (e.Result[i]).FSDESCRIPTION.ToString();

                            if ((e.Result[i]).FSSUPERVISOR.ToString().Trim() == "Y")
                                cbBooking.IsChecked = true;

                            return;
                        }
                    }
                }
            }
        }

        #endregion

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

    }
}

