﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using PTS_MAM3.WSMAMFunctions;
using PTS_MAM3.ProgData;
using PTS_MAM3.WSBROADCAST;
using PTS_MAM3.WSSTT;
using System.Windows.Threading;

namespace PTS_MAM3.STT
{
    public partial class STT400 : Page
    {
        WSBROADCASTSoapClient client = new WSBROADCASTSoapClient();                 //產生新的代理類別    
        WSSTTSoapClient clientStt = new WSSTTSoapClient();
        WSMAMFunctionsSoapClient clientMAM = new WSMAMFunctionsSoapClient();
        public List<Class_LOG_VIDEO> ListLogVideo = new List<Class_LOG_VIDEO>();    //入庫影像檔集合
        Class_BROADCAST m_FormData = new Class_BROADCAST();
        private DispatcherTimer TimerTransCode;                                     //轉檔Timer

        string m_strFileTypeName = "";   //入庫檔案類型名稱
        string m_strFileTypeID = "";     //入庫檔案類型編號
        string m_strType = "";           //類型
        string m_strID = "";             //編碼
        string m_strEpisode = "";        //集別
        string m_strCHANNELID = "";      //頻道別   
        string m_strFileNO = "";         //檔案編號
        string m_strBroID = "";          //送帶轉檔單單號 
        string m_strHiPath = "";         //高解路徑
        string m_strLowPath = "";        //低解路徑
        private STT600_01 m_parentForm;  //使用者查詢的畫面

        public STT400()
        {
            InitializeComponent();         
            InitializeForm();        //初始化本頁面           
        }

        public STT400(string strBroID, STT600_01 parentForm)
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面  
            m_parentForm = parentForm;

            if (strBroID.Trim() != "")
            {
                tbxBro_ID.Text = strBroID.Trim();
                LoadBrocast();
            }  
        }

        void InitializeForm() //初始化本頁面
        {
            //設定Timer
            SetUpTimer();  

            //查詢入庫影像檔資料_透過檔案節目編號及集別            
            client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted += new EventHandler<GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompletedEventArgs>(client_GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted);

            //用送帶轉檔單號取得送帶轉檔檔
            client.GetTBBROADCAST_BYBROIDCompleted += new EventHandler<GetTBBROADCAST_BYBROIDCompletedEventArgs>(client_GetTBBROADCAST_BYBROIDCompleted);           
        }
        
        private void ClassToForm()  //將前一頁點選的節目資料繫集到畫面上
        {
            tbxPROG_NAME.Text = "(" + m_FormData.FSID + ")" + m_FormData.FSID_NAME;
            tbxPROG_NAME.Tag = m_FormData.FSID;

            m_strBroID = m_FormData.FSBRO_ID.Trim();
            m_strID = m_FormData.FSID.Trim();
            m_strType = m_FormData.FSBRO_TYPE.Trim();
            m_strEpisode = m_FormData.SHOW_FNEPISODE.Trim();
            m_strCHANNELID = m_FormData.FSCHANNELID.Trim();           

            if (m_FormData.SHOW_FNEPISODE != "")
            {
                tbxEPISODE.Text = m_FormData.SHOW_FNEPISODE;
                lblEPISODE_NAME.Content = m_FormData.FNEPISODE_NAME;
            }
            else
                m_strEpisode = "0";

            if (m_FormData.FSBRO_TYPE == "G")
            {
                rdbProg.IsChecked = true;
                tbFNEPISODE.Visibility = Visibility.Visible;
                tbxEPISODE.Visibility = Visibility.Visible;
                tbEPISODE_NAME.Visibility = Visibility.Visible;
                lblEPISODE_NAME.Visibility = Visibility.Visible;
                tbPROG_NANE.Text = "節目名稱";
            }
            else if (m_FormData.FSBRO_TYPE == "P")
            {
                rdbPromo.IsChecked = true;
                tbFNEPISODE.Visibility = Visibility.Collapsed;
                tbxEPISODE.Visibility = Visibility.Collapsed;
                tbEPISODE_NAME.Visibility = Visibility.Collapsed;
                lblEPISODE_NAME.Visibility = Visibility.Collapsed;
                tbPROG_NANE.Text = "短帶名稱";
            }
            else if (m_FormData.FSBRO_TYPE == "D")
            {
                rdbData.IsChecked = true;
                tbFNEPISODE.Visibility = Visibility.Collapsed;
                tbxEPISODE.Visibility = Visibility.Collapsed;
                tbEPISODE_NAME.Visibility = Visibility.Collapsed;
                lblEPISODE_NAME.Visibility = Visibility.Collapsed;
                tbPROG_NANE.Text = "資料帶名稱";
            }

            //透過節目編號去查詢入庫影像檔
            this.DGLogList.DataContext = null;
            client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDAsync(m_strType, m_strID, m_strEpisode, m_strBroID);           
            TimerTransCode.Start();   //啟動查詢進度的Timer
        }

        #region 實作
      
        //實作-查詢入庫影像檔資料_透過檔案節目編號及集別
        void client_GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted(object sender, GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    this.DGLogList.DataContext = e.Result;                  
                    ListLogVideo = e.Result;

                    if (this.DGLogList.DataContext != null)
                    {
                        DGLogList.SelectedIndex = 0;
                    }
                }
            }
        }

        //實作-用送帶轉檔單號取得送帶轉檔檔
        void client_GetTBBROADCAST_BYBROIDCompleted(object sender, GetTBBROADCAST_BYBROIDCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;

            if (e.Error == null)
            {
                if (e.Result != null )
                {
                    m_FormData = (Class_BROADCAST)(e.Result[0]);

                    switch (m_FormData.FCCHECK_STATUS.Trim())
                    {
                        case "N": //節目製作單位輸入表單後
                            MessageBox.Show("該筆送帶轉檔單資料未審核通過，請檢查！", "提示訊息", MessageBoxButton.OK);
                            TimerTransCode.Stop();
                            ClearSTT();
                            break;
                        case "C": //節目製作單位輸入表單後(送帶轉檔置換)
                            MessageBox.Show("該筆送帶轉檔置換單資料未審核通過，請檢查！", "提示訊息", MessageBoxButton.OK);
                            TimerTransCode.Stop();
                            ClearSTT();
                            break;
                        case "Y": //檔案線上簽收後                          
                            MessageBox.Show("該筆送帶轉檔單資料未線上簽收，請檢查！", "提示訊息", MessageBoxButton.OK);
                            TimerTransCode.Stop();
                            ClearSTT();
                            break;
                        case "F": //審核退回後
                            MessageBox.Show("該筆送帶轉檔單資料已被駁回，請檢查！", "提示訊息", MessageBoxButton.OK);
                            TimerTransCode.Stop();
                            ClearSTT();
                            break;
                        case "G": //檔案線上簽收後                        
                            ClassToForm();
                            break;
                        case "I": //轉檔後                          
                            ClassToForm();
                            break;
                        case "X": //抽單
                            TimerTransCode.Stop();
                            MessageBox.Show("該筆送帶轉檔單資料已抽單，請檢查！", "提示訊息", MessageBoxButton.OK);
                            ClearSTT();
                            break;
                        case "T": //影帶回溯
                            ClassToForm();
                            break;
                        default:
                            MessageBox.Show("該筆送帶轉檔單資料狀態異常，請檢查！", "提示訊息", MessageBoxButton.OK);
                            TimerTransCode.Stop();
                            ClearSTT();
                            break;
                    }
                }
                else
                {
                    MessageBox.Show("查無此送帶轉檔單資料，請檢查！", "提示訊息", MessageBoxButton.OK);
                    ClearSTT();
                }
            }
        }

        //設定Timer
        private void SetUpTimer()
        {
            TimerTransCode = new DispatcherTimer();
            TimerTransCode.Interval = TimeSpan.FromMilliseconds(3000);
            TimerTransCode.Tick += TimerTransCode_Tick;
        }

        //Timer裡作的事情
        void TimerTransCode_Tick(object sender, EventArgs e)
        {
            //透過節目編號去查詢入庫影像檔
            //this.DGLogList.DataContext = null;
            client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDAsync(m_strType, m_strID, m_strEpisode, m_strBroID);
        }

        #endregion

        //取得TimeCode區間
        private string CheckDuration()
        {
            string strReturn = "";
            for (int i = 0; i < ListLogVideo.Count; i++)
            {
                if (ListLogVideo[i].FSARC_TYPE.Trim() == "001")
                {
                    strReturn = Convert.ToString(TransferTimecode.timecodetoSecond(ListLogVideo[i].FSEND_TIMECODE) - TransferTimecode.timecodetoSecond(ListLogVideo[i].FSBEG_TIMECODE)).Trim();
                    return strReturn;
                }
            }
            return "";
        }

        //載入送帶轉檔單
        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            LoadBrocast();
        }          

        //載入送帶轉檔單
        private void LoadBrocast()
        {
            double douCheck;   //純粹為了判斷是否為數字型態之用
            m_strBroID = tbxBro_ID.Text.ToString().Trim();

            if (m_strBroID.Trim() == "")
            {
                MessageBox.Show("請輸入送帶轉檔單編號", "提示訊息", MessageBoxButton.OK);
                m_strBroID = "";
            }
            else
            {
                if (Double.TryParse(m_strBroID, out douCheck) == false || m_strBroID.Trim().Length != 12)
                {
                    MessageBox.Show("請檢查「送帶轉檔單編號」必須為數值型態且為12碼", "提示訊息", MessageBoxButton.OK);
                    m_strBroID = "";
                }
                else
                {
                    client.GetTBBROADCAST_BYBROIDAsync(m_strBroID);
                    BusyMsg.IsBusy = true;
                }
            }               
        }

        //轉檔進度查詢
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGLogList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預修改的資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            string strFileNO = "";      //檔案編號
            string strFileStatus = "";  //檔案狀態
            string strJobID = "" ;      //JobID

            m_strFileTypeName = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSARC_TYPE_NAME.Trim();
            strFileNO = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSFILE_NO.Trim();
            strFileStatus = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FCFILE_STATUS.Trim();
            strJobID = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSJOB_ID.Trim();

            if (strFileStatus.Trim() == "S" && strFileNO.Trim()!="")
            {
                if (strJobID.Trim() != "")
                {
                    STT300_02 STT300_02_frm = new STT300_02(m_strFileTypeName, strFileNO);
                    STT300_02_frm.Show();  
                }
                else
                {
                    MessageBox.Show("轉檔狀態異常，請通知轉檔中心人員！", "提示訊息", MessageBoxButton.OK);
                    return;
                }
            }
            else
            {
                MessageBox.Show("此檔案狀態目前並非為轉檔中，請重新查詢！", "提示訊息", MessageBoxButton.OK);
                return;
            }        
        }

        //清空
        private void ClearSTT()
        {
            m_strBroID = "";
            this.DGLogList.DataContext = null;
            ListLogVideo.Clear();
            m_FormData = null;
            rdbProg.IsChecked = true;
            tbFNEPISODE.Visibility = Visibility.Visible;
            tbxEPISODE.Visibility = Visibility.Visible;
            tbEPISODE_NAME.Visibility = Visibility.Visible;
            lblEPISODE_NAME.Visibility = Visibility.Visible;
            tbxPROG_NAME.Text = "";
            tbxPROG_NAME.Tag = "";
            tbxEPISODE.Text = "";
            lblEPISODE_NAME.Content = "";
            tbPROG_NANE.Text = "節目名稱";
            tbxBro_ID.Text = "";
            m_parentForm.DialogResult = true;   //直接關閉
        }

        //關閉
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            m_parentForm.DialogResult = false;
        }       

    }
}
