﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;
using PTS_MAM3.WSBROADCAST;
using PTS_MAM3.PRG;
using PTS_MAM3.ProgData;
using PTS_MAM3.STB;
using System.ComponentModel;
using System.Windows.Data;
using System.Collections.ObjectModel;

namespace PTS_MAM3.STT
{
    public partial class STT911_01 : ChildWindow, INotifyPropertyChanged
    {
        WSARCHIVE.WSARCHIVESoapClient archive_Client = new WSARCHIVE.WSARCHIVESoapClient();
        PTS_MAM3.WSBROADCAST.WSBROADCASTSoapClient Brocast_Client = new PTS_MAM3.WSBROADCAST.WSBROADCASTSoapClient();
        PTS_MAM3.WSMAMFunctions.WSMAMFunctionsSoapClient MAM_Client = new PTS_MAM3.WSMAMFunctions.WSMAMFunctionsSoapClient();

        string Arc_type_id = "";

        //Class_LOG_VIDEO Log_video=new Class_LOG_VIDEO();
        string fileName = "";
        string filePath = "";
        //string mediaInfo_Xml = "";
        //string video_id = "";
        string m_DIRID = "";
        List<Class_LOG_VIDEO> videoList = new List<Class_LOG_VIDEO>();

        public List<Class_LOG_VIDEO> VideoList
        {
            get { return videoList; }
            set
            {
                videoList = value;

                RaisePropertyChanged("VideoList");
            }
        }

        Class_BROADCAST broadcast = null;
        public Class_BROADCAST Broadcast
        {
            get { return broadcast; }
            set { broadcast = value; RaisePropertyChanged("Broadcast"); }
        }

        ActionType Bro_ActionType = ActionType.Unknow;
        ActionType Seg_ActionType = ActionType.Unknow;


        public STT911_01(Class_BROADCAST broadcast, ActionType bro_ActionType, string arc_type_id)
        {
            InitializeComponent();

            if (Bro_ActionType == ActionType.View)
            {
                if (broadcast.FSBRO_TYPE == "G")
                {
                    this.rdbProg.IsChecked = true;
                }
                else if (broadcast.FSBRO_TYPE == "P")
                {
                    this.rdbPromo.IsChecked = true;
                }
            }

            Bro_ActionType = bro_ActionType;

            if (Bro_ActionType == ActionType.View)
            {
                this.OKButton.Visibility = System.Windows.Visibility.Collapsed;
                this.Title = "檢視其他影片";
                this.CancelButton.Content = "關閉";
            }
            Arc_type_id = arc_type_id;

            #region
            //取一個新的單號
            MAM_Client.GetNoRecordCompleted += new EventHandler<WSMAMFunctions.GetNoRecordCompletedEventArgs>(MAM_Client_GetNoRecordCompleted);

            //取得新的VideoID
            Brocast_Client.QueryVideoID_PROG_STTCompleted += new EventHandler<QueryVideoID_PROG_STTCompletedEventArgs>(Brocast_Client_QueryVideoID_PROG_STTCompleted);

            ////取得新的檔案編號
            //Brocast_Client.fnGetFileIDCompleted += new EventHandler<fnGetFileIDCompletedEventArgs>(Brocast_Client_fnGetFileIDCompleted);

            Brocast_Client.GetTBLOG_VIDEO_BY_Type_EPISODECompleted += new EventHandler<GetTBLOG_VIDEO_BY_Type_EPISODECompletedEventArgs>(Brocast_Client_GetTBLOG_VIDEO_BY_Type_EPISODECompleted);

            archive_Client.fnGetMediaFilePropertyCompleted += new EventHandler<WSARCHIVE.fnGetMediaFilePropertyCompletedEventArgs>(archive_Client_fnGetMediaFilePropertyCompleted);

            Brocast_Client.DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONECompleted += new EventHandler<DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONECompletedEventArgs>(Brocast_Client_DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONECompleted);

            Brocast_Client.INSERT_BROADCAST_WITHOUT_FLOWCompleted += new EventHandler<INSERT_BROADCAST_WITHOUT_FLOWCompletedEventArgs>(Brocast_Client_INSERT_BROADCAST_WITHOUT_FLOWCompleted);

            Brocast_Client.INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLISTCompleted += new EventHandler<INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLISTCompletedEventArgs>(Brocast_Client_INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLISTCompleted);

            Brocast_Client.DELETE_TBLOG_VIDEO_SEG_BYFILENOCompleted += new EventHandler<DELETE_TBLOG_VIDEO_SEG_BYFILENOCompletedEventArgs>(Brocast_Client_DELETE_TBLOG_VIDEO_SEG_BYFILENOCompleted);

            #endregion


            //Binding  binding=new Binding("VideoList");
            //binding.Source = this;
            //binding.Mode = BindingMode.TwoWay;

            //this.DGLogList.SetBinding(DataGrid.ItemsSourceProperty, binding) ;

            if (Bro_ActionType == ActionType.Create)
            {
                Broadcast = new Class_BROADCAST();
                Broadcast.FSBRO_TYPE = "G";
                Broadcast.FCCHANGE = "N";
                Broadcast.FNEPISODE = null;
                MAM_Client.GetNoRecordAsync("07", "", "", UserClass.userData.FSUSER_ID);
            }
            else
            {
                Broadcast = broadcast;
                LockView_All();
                Load_TBLOG_VIDEO();


            }

            this.DataContext = this;
        }

        void Brocast_Client_INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLISTCompleted(object sender, INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLISTCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {
                    MessageBox.Show("其他影片資料新增成功!");
                    this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("新增其他影片資料時發生錯誤!");
                    BusyMsg.IsBusy = false;

                }
            }
        }


        void Brocast_Client_DELETE_TBLOG_VIDEO_SEG_BYFILENOCompleted(object sender, DELETE_TBLOG_VIDEO_SEG_BYFILENOCompletedEventArgs e)
        {
            this.DialogResult = false;
        }

        void Brocast_Client_INSERT_BROADCAST_WITHOUT_FLOWCompleted(object sender, INSERT_BROADCAST_WITHOUT_FLOWCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {
                    MessageBox.Show("其他影片資料新增成功!");
                    this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("新增其他影片資料時發生錯誤!");
                    BusyMsg.IsBusy = false;

                }
            }
        }

        void Brocast_Client_DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONECompleted(object sender, DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                Load_TBLOG_VIDEO();
            }

        }

        void Brocast_Client_GetTBLOG_VIDEO_BY_Type_EPISODECompleted(object sender, GetTBLOG_VIDEO_BY_Type_EPISODECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                VideoList = e.Result;
                foreach (Class_LOG_VIDEO video in VideoList)
                {
                    switch (video.FCFILE_STATUS)
                    {
                        case "B":
                            video.FCFILE_STATUS_NAME = "等待搬檔";
                            break;
                        case "T":
                            video.FCFILE_STATUS_NAME = "搬檔完成";
                            break;
                        case "R":
                            video.FCFILE_STATUS_NAME = "搬檔失敗";
                            break;
                    }

                }
                this.DGLogList.ItemsSource = VideoList;
                BusyMsg.IsBusy = false;
            }
        }

        void Brocast_Client_fnGetFileIDCompleted(object sender, fnGetFileIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    string strGet = e.Result;  //接收回來的參數，前者是檔案編號，後者是DIRID                  

                    char[] delimiterChars = { ';' };
                    string[] words = strGet.Split(delimiterChars);


                    if (words.Length == 2)
                    {
                        VideoList[SelectedIndex].FSFILE_NO = words[0];
                        m_DIRID = words[1];
                    }
                    else if (words.Length == 1)
                    {
                        VideoList[SelectedIndex].FSFILE_NO = words[0];
                        m_DIRID = "";
                    }
                    else
                    {
                        VideoList[SelectedIndex].FSFILE_NO = "";
                        m_DIRID = "";
                    }

                    Load_Video_Seg();
                }
            }
        }

        void Brocast_Client_QueryVideoID_PROG_STTCompleted(object sender, QueryVideoID_PROG_STTCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                VideoList[SelectedIndex].FSVIDEO_PROG = e.Result;
                Load_Video_Seg();
            }
        }

        void MAM_Client_GetNoRecordCompleted(object sender, WSMAMFunctions.GetNoRecordCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                Broadcast.FSBRO_ID = e.Result;
            }
        }

        void archive_Client_fnGetMediaFilePropertyCompleted(object sender, WSARCHIVE.fnGetMediaFilePropertyCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                VideoList[SelectedIndex].FSVIDEO_PROPERTY = e.Result;

                this.DGLogList.ItemsSource = VideoList;
            }
            else
            {
                MessageBox.Show("獲取MediaFileProperty時發生錯誤!錯誤訊息："
                    + Environment.NewLine
                    + e.Error.Message, "提示", MessageBoxButton.OK);
            }

            this.btnSeg.IsEnabled = true;
            this.btnEdit.IsEnabled = true;
            this.btnDel.IsEnabled = true;
            BusyMsg.IsBusy = false;


        }

        private void btnPROG_Click(object sender, RoutedEventArgs e)
        {
            if (rdbProg.IsChecked == true)
            {
                //PRG100_01
                PROGDATA_VIEW PROGDATA_VIEW_frm = new PROGDATA_VIEW();


                PROGDATA_VIEW_frm.Closed += (s, args) =>
                {
                    if (PROGDATA_VIEW_frm.DialogResult == true)
                    {
                        //檢查是否要作資料認定
                        if (PROGDATA_VIEW_frm.strChannel_Id.Trim() == "" || PROGDATA_VIEW_frm.strDEP_ID.Trim() == "0")
                        {
                            checkProgM_Channel_Dep(PROGDATA_VIEW_frm.strProgID_View.Trim(), PROGDATA_VIEW_frm.strProgName_View.Trim(), PROGDATA_VIEW_frm.strChannel_Id.Trim(), PROGDATA_VIEW_frm.strDEP_ID.Trim(), PROGDATA_VIEW_frm.strProducer.Trim(), PROGDATA_VIEW_frm.strTOTLEEPISODE.Trim(), PROGDATA_VIEW_frm.strCHANNEL.Trim(), PROGDATA_VIEW_frm.strFNLENGTH.Trim());
                            return;
                        }

                        //載入節目資料
                        checkProg(PROGDATA_VIEW_frm.strProgID_View.Trim(), PROGDATA_VIEW_frm.strProgName_View.Trim(), PROGDATA_VIEW_frm.strChannel_Id.Trim(), PROGDATA_VIEW_frm.strDEP_ID.Trim(), PROGDATA_VIEW_frm.strProducer.Trim(), PROGDATA_VIEW_frm.strTOTLEEPISODE.Trim(), PROGDATA_VIEW_frm.strCHANNEL.Trim(), PROGDATA_VIEW_frm.strFNLENGTH.Trim());

                    }
                };

                PROGDATA_VIEW_frm.Show();
            }
            else if (rdbPromo.IsChecked == true)
            {
                //PRG800_07
                PRG800_07 PROMO_VIEW_frm = new PRG800_07();
                PROMO_VIEW_frm.Show();

                PROMO_VIEW_frm.Closed += (s, args) =>
                {
                    if (PROMO_VIEW_frm.DialogResult == true)
                    {
                        //檢查是否要作資料認定
                        if (PROMO_VIEW_frm.strChannel_Id.Trim() == "" || PROMO_VIEW_frm.strDEP_ID.Trim() == "0")
                        {
                            checkProgM_Channel_Dep(PROMO_VIEW_frm.strPromoID_View.Trim(), PROMO_VIEW_frm.strPromoName_View.Trim(), PROMO_VIEW_frm.strChannel_Id.Trim(), PROMO_VIEW_frm.strDEP_ID.Trim(), PROMO_VIEW_frm.strProducer.Trim(), "", "", "");
                            return;
                        }

                        //載入短帶資料
                        checkPromo(PROMO_VIEW_frm.strPromoID_View.Trim(), PROMO_VIEW_frm.strPromoName_View.Trim(), PROMO_VIEW_frm.strChannel_Id.Trim(), PROMO_VIEW_frm.strDEP_ID.Trim(), PROMO_VIEW_frm.strProducer.Trim());


                    }
                };
            }
        }

        //檢查是否要作節目資料認定
        private void checkProgM_Channel_Dep(string strProgID, string strProgName, string strChannelID, string strDepID, string strProducer, string strTEpidode, string strPlayCHANNEL, string strLength)
        {
            string strCheck = "";   //判斷基本資料缺少訊息
            //string strType = "";    //類型

            //if (rdbProg.IsChecked == true)
            //    strType = "G";
            //else if (rdbPromo.IsChecked == true)
            //    strType = "P";

            //無頻道別要檔掉     
            if (strChannelID.Trim() == "")
            {
                strCheck = "無頻道別資料";
            }

            //無成案單位要檔掉     
            if (strDepID.Trim() == "0")
            {
                if (strCheck.Trim() == "")
                    strCheck = "無成案單位資料";
                else
                    strCheck = strCheck + "、無成案單位資料";
            }

            if (strCheck.Trim() != "")
            {
                MessageBox.Show(strCheck + "，請先進行節目資料認定", "提示訊息", MessageBoxButton.OK);

                //判斷有無修改「節目資料認定」的權限     
                if (ModuleClass.getModulePermissionByName("01", "節目資料認定") == true)
                {
                    MessageBoxResult resultMsg = MessageBox.Show("確定要修改「" + strProgName.Trim() + "」的頻道別資料嗎?", "提示訊息", MessageBoxButton.OKCancel);
                    if (resultMsg == MessageBoxResult.OK)
                    {
                        PRG100_13 PRG100_13_frm = new PRG100_13(Broadcast.FSBRO_TYPE, strProgID, strProgName, strChannelID, strDepID);
                        PRG100_13_frm.Show();

                        PRG100_13_frm.Closed += (s, args) =>
                        {
                            if (rdbProg.IsChecked == true)
                            {
                                //如果資料補齊，即可載入節目資料
                                if (PRG100_13_frm.bolcheck == true)
                                    checkProg(strProgID, strProgName, strChannelID, strDepID, strProducer, strTEpidode, strPlayCHANNEL, strLength);
                            }
                            else if (rdbPromo.IsChecked == true)
                            {
                                //如果資料補齊，即可載入短帶資料
                                if (PRG100_13_frm.bolcheck == true)
                                    checkPromo(strProgID, strProgName, strChannelID, strDepID, strProducer);
                            }
                        };
                    }
                }
            }
        }

        //載入節目資料
        private void checkProg(string strProgID, string strProgName, string strChannelID, string strDepID, string strProducer, string strTEpidode, string strPlayCHANNEL, string strLength)
        {

            Broadcast.FSID_NAME = strProgName.Trim();
            Broadcast.FSID = strProgID.Trim();
            Broadcast.FNEPISODE = 0;
            Broadcast.FNEPISODE_NAME = "";
            //tbxPROG_NAME.Text = strProgName;
            //tbxPROG_NAME.Tag = strProgID;

            //m_strProducer = strProducer;   //製作人

            tbxEPISODE.Text = "";
            //lblEPISODE_NAME.Content = "";


            tbxTOTEPISODE.Text = strTEpidode;
            tbxLENGTH.Text = strLength;
            Broadcast.FSCHANNELID = strChannelID;

            //m_strCHANNELID = strChannelID;
            //m_strDep_ID = strDepID; //原Code有，但似乎沒用

            //透過節目編號去查詢入庫影像檔
            VideoList.Clear();

        }


        private string[] CheckList(string strChannel)
        {
            char[] delimiterChars = { ';' };
            string[] words = strChannel.Split(delimiterChars);
            return words;
        }




        //載入短帶資料
        private void checkPromo(string strPromoID, string strPromoName, string strChannelID, string strDepID, string strProducer)
        {
            Broadcast.FSID_NAME = strPromoName;
            Broadcast.FSID = strPromoID;
            //tbxPROG_NAME.Text = strPromoName;
            //tbxPROG_NAME.Tag = strPromoID;

            //m_strProducer = strProducer;                         //製作人


            //tbxEPISODE.Text = "";
            //lblEPISODE_NAME.Content = "";
            Broadcast.FNEPISODE = 0;
            Broadcast.FSCHANNELID = strChannelID;

            VideoList.Clear();


            //找48小時有無排播記錄
            Brocast_Client.GetPROMO_PLAY_NOTEAsync(Broadcast.FSID);
        }

        private void btnEPISODE_Click(object sender, RoutedEventArgs e)
        {
            if (Broadcast.FSID != "")
            {
                PRG.PRG200_07 PRG200_07_frm = new PRG.PRG200_07(Broadcast.FSID, Broadcast.FSID_NAME);


                PRG200_07_frm.Closed += (s, args) =>
                {
                    if (PRG200_07_frm.DialogResult == true)
                    {
                        Broadcast.FNEPISODE = Convert.ToInt16(PRG200_07_frm.m_strEpisode_View);
                        Broadcast.FNEPISODE_NAME = PRG200_07_frm.m_strProgDName_View;
                        tbxEPISODE.Text = Broadcast.FNEPISODE.ToString();
                        //lblEPISODE_NAME.Content = PRG200_07_frm.m_strProgDName_View;

                        //透過節目編號去查詢入庫影像檔
                        VideoList.Clear();


                        Brocast_Client.GetPROG_PLAY_NOTEAsync(Broadcast.FSID, Broadcast.FNEPISODE.ToString());

                    }
                };

                PRG200_07_frm.Show();
            }
            else
                MessageBox.Show("請先選擇節目！", "提示訊息", MessageBoxButton.OK);
        }


        private void rdbProg_Checked(object sender, RoutedEventArgs e)
        {
            if (Broadcast != null)
                Broadcast.FSBRO_TYPE = "G";

            if (this.LayoutRoot != null)
            {
                this.gdPROG_M.RowDefinitions[2].Height = new GridLength(29, GridUnitType.Pixel);
                this.gdPROG_M.RowDefinitions[3].Height = new GridLength(29, GridUnitType.Pixel);
            }
        }

        private void rdbPromo_Checked(object sender, RoutedEventArgs e)
        {
            Broadcast.FSBRO_TYPE = "P";
            this.gdPROG_M.RowDefinitions[2].Height = new GridLength(0, GridUnitType.Pixel);
            this.gdPROG_M.RowDefinitions[3].Height = new GridLength(0, GridUnitType.Pixel);
        }

        Dictionary<int, string> filePathDictinary = new Dictionary<int, string>();
        private void SelectFileBtn_Click(object sender, RoutedEventArgs e)
        {
            STT300_01_01 _300_01_01 = new STT300_01_01("Other", false);
            _300_01_01.Closed += (s, args) =>
            {
                if (_300_01_01.DialogResult == true)
                {
                    fileName = _300_01_01.strSelect_File_Name;
                    filePath = _300_01_01.strSelect_File_Path;
                    Class_LOG_VIDEO Log_video = new Class_LOG_VIDEO();

                    Log_video.FSTITLE = fileName;
                    Log_video.FSTYPE = Broadcast.FSBRO_TYPE;
                    Log_video.FSVIDEO_PROG = "";
                    Log_video.FSBRO_ID = Broadcast.FSBRO_ID;
                    Log_video.FSCHANNEL_ID = Broadcast.FSCHANNELID;
                    Log_video.FSFILE_NO = "";
                    Log_video.FSID = Broadcast.FSID;
                    Log_video.FNEPISODE = Convert.ToInt16(Broadcast.FNEPISODE == null ? 0 : Broadcast.FNEPISODE);
                    Log_video.FCLOW_RES = "N";//不擷取keyframe
                    Log_video.FSARC_TYPE = Arc_type_id;
                    Log_video.FSARC_TYPE_NAME = "其他影片";
                    Log_video.FCFILE_STATUS = "B";
                    Log_video.FCFILE_STATUS_NAME = "待轉檔";
                    Log_video.FSOLD_FILE_NAME = filePath;
                    Log_video.FSFILE_TYPE_HV = System.IO.Path.GetExtension(filePath).Replace(".", ""); ;
                    Log_video.FSFILE_TYPE_LV = System.IO.Path.GetExtension(filePath).Replace(".", ""); ;
                    Log_video.FSTRANS_FROM = "數位檔案，檔案名稱：" + fileName + "， 完整路徑：" + filePath;
                    VideoList.Add(Log_video);
                    if (VideoList.Count > 0)
                    {
                        this.rdbProg.IsEnabled = false;
                        this.rdbPromo.IsEnabled = false;
                        this.btnPROG.IsEnabled = false;
                        this.btnEPISODE.IsEnabled = false;
                    }
                    this.DGLogList.ItemsSource = VideoList;
                    this.DGLogList.SelectedIndex = VideoList.Count - 1;
                    filePathDictinary.Add(VideoList.Count - 1, filePath);

                    BusyMsg.IsBusy = true;

                    archive_Client.fnGetMediaFilePropertyAsync(filePath);
                }
            };
            _300_01_01.Show();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (Bro_ActionType == ActionType.Create)
            {
                if (string.IsNullOrEmpty(Broadcast.FSID))
                {
                    MessageBox.Show("請選擇節目");
                    return;
                }
                else if (rdbProg.IsChecked == true && Broadcast.FNEPISODE <= 0)
                {
                    MessageBox.Show("請選擇子集");
                    return;
                }
                else if (VideoList.Count == 0)
                {
                    MessageBox.Show("尚未新增任何檔案");
                    return;
                }
                //else if (VideoList[SelectedIndex].VideoListSeg.Count == 0)
                //{
                //    MessageBox.Show("尚未輸入段落資料");
                //    return;
                //}

                foreach (Class_LOG_VIDEO video in VideoList)
                {
                    if (video.VideoListSeg == null || video.VideoListSeg.Count == 0)
                    {
                        MessageBox.Show("檔案「" + video.FSTITLE + "」尚未輸入段落資料");
                        return;
                    }
                }

                BusyMsg.IsBusy = true;
                Brocast_Client.INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLISTAsync(FormToClass_Bro(), VideoList);
                //Brocast_Client.INSERT_BROADCAST_WITHOUT_FLOWAsync(FormToClass_Bro());
            }
            else
            {
                this.DialogResult = true;
            }
        }

        private Class_BROADCAST FormToClass_Bro()
        {
            Broadcast.FDBRO_DATE = DateTime.Now;                                          //送帶轉檔申請日期
            Broadcast.FSBRO_BY = UserClass.userData.FSUSER_ID.ToString();                 //送帶轉檔申請者

            Broadcast.FCCHECK_STATUS = "K";
            Broadcast.FSSIGNED_BY = UserClass.userData.FSUSER_ID;

            Broadcast.FSCHECK_BY = "";                                                    //審核者
            Broadcast.FSCREATED_BY = UserClass.userData.FSUSER_ID.ToString();//建檔者
            Broadcast.FSUPDATED_BY = UserClass.userData.FSUSER_ID.ToString();//修改者                     

            ////Memo內容格式會影像搬檔程式拆分路徑的邏輯
            //string memo = "其他影片上傳 ─";
            //for (int i = 0; i < VideoList.Count; i++)
            //{
            //    memo += " 檔案編號：" + VideoList[i].FSFILE_NO + Environment.NewLine + " 實體路徑：" + filePathDictinary[i] + ";" + Environment.NewLine;
            //}
            //memo += "----------------分隔線-------------------"+Environment.NewLine;
            //memo += this.tbMEMO.Text;
            Broadcast.FSMEMO = this.tbMEMO.Text;//memo;

            return Broadcast;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            //改成最後一次性寫入所有資料
            //if (VideoList.Count > 0)
            //{
            //    if (Bro_ActionType == ActionType.Create)
            //    {
            //        List<Class_LOG_VIDEO> tempListLogVideo = new List<Class_LOG_VIDEO>();    //入庫影像檔集合

            //        for (int i = 0; i < VideoList.Count; i++)
            //        {
            //            if (VideoList[i].FSBRO_ID != null && VideoList[i].FSBRO_ID.Trim() != "" && VideoList[i].FSBRO_ID.Trim() == Broadcast.FSBRO_ID.Trim())
            //            {
            //                tempListLogVideo.Add(VideoList[i]);
            //            }
            //        }

            //        if (tempListLogVideo.Count > 0)
            //            Brocast_Client.DELETE_TBLOG_VIDEO_SEG_BYFILENOAsync(tempListLogVideo);
            //    }
            //}
            //else
            //{
            if (Bro_ActionType != ActionType.View)
            {
                if (MessageBox.Show("確定取消表單?", "提示", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                    this.DialogResult = false;
            }
            else
            {
                this.DialogResult = false;
            }
            //}
        }

        int SelectedIndex = 0;
        private void btnSeg_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;

            switch (btn.Name)
            {
                case "btnSeg":
                    Seg_ActionType = ActionType.Create;
                    break;
                case "btnEdit":
                    Seg_ActionType = ActionType.Edit;
                    break;
                case "btnDel":
                    Seg_ActionType = ActionType.Delete;
                    break;
            }

            if (Seg_ActionType != ActionType.Create)
            {
                if (Broadcast.FSBRO_TYPE == "G" && string.IsNullOrEmpty(Broadcast.FSID))
                {
                    MessageBox.Show("請先選擇節目", "提示訊息", MessageBoxButton.OK);
                    return;
                }
                else if (Broadcast.FSBRO_TYPE == "G" && Broadcast.FNEPISODE == 0)
                {
                    MessageBox.Show("請先選擇子集", "提示訊息", MessageBoxButton.OK);
                    return;
                }
                else if (Broadcast.FSBRO_TYPE == "P" && string.IsNullOrEmpty(Broadcast.FSID))
                {
                    MessageBox.Show("請先選擇短帶", "提示訊息", MessageBoxButton.OK);
                    return;
                }
                else if (DGLogList.SelectedItem == null)
                {
                    MessageBox.Show("請選擇欲" + (Seg_ActionType == ActionType.Edit ?
                        "修改" : "刪除") + "的檔案", "提示訊息", MessageBoxButton.OK);
                    return;
                }

            }

            SelectedIndex = this.DGLogList.SelectedIndex;
            if (Seg_ActionType == ActionType.Create)
            {
                Load_Video_Seg();
                //Brocast_Client.QueryVideoID_PROG_STTAsync(Broadcast.FSID, Broadcast.FNEPISODE.ToString(), "026", false);
            }
            else if (Seg_ActionType == ActionType.Delete)
            {
                if (MessageBox.Show("確定要刪除「" + VideoList[SelectedIndex].FSTITLE + "」的段落資料嗎?", "提示訊息", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                {
                    //刪除段落檔
                    if (VideoList[SelectedIndex].FSCREATED_BY_NAME == null)//XXXXXX_BY_NAME這種欄位都是從DB撈出來的資料才會有值
                    {
                        this.VideoList.RemoveAt(SelectedIndex);
                        this.DGLogList.ItemsSource = this.VideoList;
                    }
                    else
                    {
                        Brocast_Client.DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONEAsync(VideoList[SelectedIndex].FSFILE_NO, UserClass.userData.FSUSER_ID.ToString());
                    }
                }
            }
            else
            {
                Load_Video_Seg();
            }



        }

        private void Load_TBLOG_VIDEO()
        {
            BusyMsg.IsBusy = true;
            Brocast_Client.GetTBLOG_VIDEO_BY_Type_EPISODEAsync(Broadcast.FSBRO_TYPE, Arc_type_id, Broadcast.FSID, Broadcast.FNEPISODE.ToString(), Broadcast.FSBRO_ID);
        }

        void Load_Video_Seg()
        {
            STT911_01_01 newSegForm = new STT911_01_01(VideoList[SelectedIndex], Broadcast.FSID_NAME, Broadcast.FNEPISODE_NAME == null ? "" : Broadcast.FNEPISODE_NAME, Seg_ActionType == ActionType.Copy);

            newSegForm.Closing += (s, args) =>
                {
                    if (newSegForm.DialogResult == true)
                    {
                        VideoList[SelectedIndex] = newSegForm.Log_Video;
                        //VideoList[SelectedIndex].VideoListSeg = newSegForm.ListSeg;
                        //VideoList[SelectedIndex].FSFILE_NO = newSegForm.Log_Video.FSFILE_NO;
                        this.DGLogList.ItemsSource = VideoList;
                        //Load_TBLOG_VIDEO();
                    }
                };

            newSegForm.Show();
            //STT100_01_01 STT100_01_01_frm = new STT100_01_01("HD", Arc_type_id, Broadcast.FSBRO_TYPE, Broadcast.FSID, Broadcast.FNEPISODE.ToString(), Broadcast.FSCHANNELID,
            //    (Seg_ActionType == ActionType.Create ? "" : VideoList[SelectedIndex].FSFILE_NO), Broadcast.FSBRO_ID, Broadcast.FSID_NAME, Broadcast.FNEPISODE_NAME, "", false,
            //    VideoList[SelectedIndex].FSVIDEO_PROG = "", //null ? "" : VideoList[0].FSVIDEO_PROG,
            //    (Seg_ActionType == ActionType.Create ? "" : VideoList[SelectedIndex].FSTRACK));
            //STT100_01_01_frm.Show();

            ////若是有填入段落檔，就要去讀取該節目集別的段落資料，並且秀在畫面上
            //STT100_01_01_frm.Closing += (s, args) =>
            //{
            //    if (STT100_01_01_frm.DialogResult == true)
            //    {
            //        Load_TBLOG_VIDEO();
            //    }
            //};
        }
        private void EditTrackBtn_Click(object sender, RoutedEventArgs e)
        {
            Button clickedBtn = ((Button)sender);
            Class_LOG_VIDEO log_video = (Class_LOG_VIDEO)clickedBtn.Tag;
            StackPanel templatePanel = (StackPanel)VisualTreeHelper.GetParent(clickedBtn);
            TextBox tbtrack = ((TextBox)templatePanel.Children[0]);

            if (clickedBtn.Content.ToString() == "修改音軌")
            {
                clickedBtn.Content = "確定";
                tbtrack.IsEnabled = true;
            }
            else if (clickedBtn.Content.ToString() == "確定")
            {
                string newTrack = tbtrack.Text.Trim().ToUpper();
                if (!CheckTrackFormate(newTrack))
                {
                    MessageBox.Show("格式不符!");
                    return;
                }
                clickedBtn.Content = "修改音軌";
                log_video.FSTRACK = newTrack;
                clickedBtn.Tag = log_video;
                tbtrack.IsEnabled = false;
            }
            string tarck = VideoList[SelectedIndex].FSTRACK;
        }

        bool CheckTrackFormate(string oriTrack)
        {
            if (oriTrack.Length == 0 && oriTrack.Length != 4 && oriTrack.Length != 8)
            {
                return false;
            }

            char[] charArr = oriTrack.ToCharArray();
            foreach (char ch in charArr)
            {
                if (ch != 'L' && ch != 'M' && ch != 'R' && ch != '-')
                {
                    return false;
                }
            }

            return true;
        }

        void LockView_All()
        {
            foreach (UIElement ui in this.gdPROG_M.Children)
            {
                if (ui is TextBox)
                {
                    ((TextBox)ui).IsReadOnly = true;
                }
                else if (ui is Button)
                {
                    ((Button)ui).IsEnabled = false;
                }
                else if (ui is StackPanel)
                {
                    foreach (UIElement ui2 in ((StackPanel)ui).Children)
                    {
                        if (ui2 is RadioButton)
                        {
                            ((RadioButton)ui2).IsEnabled = false;
                        }
                        else if (ui2 is Button)
                        {
                            ((Button)ui2).IsEnabled = false;
                        }
                    }
                }
            }

            this.OKButton.IsEnabled = true;
            this.CancelButton.IsEnabled = true;
        }

        void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        private void DGLogList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectedIndex = this.DGLogList.SelectedIndex;
        }
    }
}

