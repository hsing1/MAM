﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using PTS_MAM3.WSMAMFunctions;
using PTS_MAM3.ProgData;
using PTS_MAM3.WSBROADCAST;
using PTS_MAM3.WSSTT;

namespace PTS_MAM3.STT
{
    public partial class STT200 : Page
    {
        WSBROADCASTSoapClient client = new WSBROADCASTSoapClient();                 //產生新的代理類別    
        WSMAMFunctionsSoapClient clientMAM = new WSMAMFunctionsSoapClient();
        public List<Class_LOG_VIDEO> ListLogVideo = new List<Class_LOG_VIDEO>();    //入庫影像檔集合
        Class_BROADCAST m_FormData = new Class_BROADCAST();

        string m_strType = "";           //類型
        string m_strID = "";             //編碼
        string m_strEpisode = "";        //集別
        string m_strCHANNELID = "";      //頻道別 
        string m_strBroID = "";          //送帶轉檔單單號 
        private STT600_01 m_parentForm;  //使用者查詢的畫面
        string m_strStatus = "";         //簽收狀態
        Boolean m_bolUrgent = false;     //緊急送播

        public STT200()
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面           
        }

        public STT200(string strBroID,STT600_01 parentForm)
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面 
            m_parentForm = parentForm;

            if (strBroID.Trim() != "")
            {
                tbxBro_ID.Text = strBroID.Trim();
                LoadBrocast();
            }   
        }

        void InitializeForm() //初始化本頁面
        {
            //查詢入庫影像檔資料_透過檔案節目編號及集別            
            client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted += new EventHandler<GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompletedEventArgs>(client_GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted);

            //用送帶轉檔單號取得送帶轉檔檔
            client.GetTBBROADCAST_BYBROIDCompleted += new EventHandler<GetTBBROADCAST_BYBROIDCompletedEventArgs>(client_GetTBBROADCAST_BYBROIDCompleted);

            //線上簽收送帶轉檔檔
            client.UPDATE_TBBROADCAST_StatusCompleted += new EventHandler<UPDATE_TBBROADCAST_StatusCompletedEventArgs>(client_UPDATE_TBBROADCAST_StausCompleted);

            //用節目編號、節別查詢48小時內是否有排播
            client.GetPROG_PLAY_NOTECompleted += new EventHandler<GetPROG_PLAY_NOTECompletedEventArgs>(client_GetPROG_PLAY_NOTECompleted);

            //用短帶編號查詢48小時內是否有排播
            client.GetPROMO_PLAY_NOTECompleted += new EventHandler<GetPROMO_PLAY_NOTECompletedEventArgs>(client_GetPROMO_PLAY_NOTECompleted);

            //查詢入庫影像檔(避免一筆以上的SD播出帶或是HD播出帶)
            client.QUERY_TBLOG_VIDEO_BYProgID_Episode_LIST_CHECKCompleted += new EventHandler<QUERY_TBLOG_VIDEO_BYProgID_Episode_LIST_CHECKCompletedEventArgs>(client_QUERY_TBLOG_VIDEO_BYProgID_Episode_LIST_CHECKCompleted);

            //線上簽收時發現為緊急送播，要寫入主控用的資料表-TBLOG_VIDEO_HIGH_PRIORITY
            client.UrgentsTTCompleted += new EventHandler<UrgentsTTCompletedEventArgs>(client_UrgentsTTCompleted);
        }
             
        private void ClassToForm()  //將前一頁點選的節目資料繫集到畫面上
        {
            tbxPROG_NAME.Text = "(" + m_FormData.FSID + ")" + m_FormData.FSID_NAME;
            tbxPROG_NAME.Tag = m_FormData.FSID;
            tbMEMO.Text = m_FormData.FSMEMO;

            m_strBroID = m_FormData.FSBRO_ID.Trim();
            m_strID = m_FormData.FSID.Trim();
            m_strType = m_FormData.FSBRO_TYPE.Trim();
            m_strEpisode = m_FormData.SHOW_FNEPISODE.Trim();
            m_strCHANNELID = m_FormData.FSCHANNELID.Trim();
            

            if (m_FormData.SHOW_FNEPISODE != "")
            {
                tbxEPISODE.Text = m_FormData.SHOW_FNEPISODE;
                lblEPISODE_NAME.Content = m_FormData.FNEPISODE_NAME;
            }
            else
                m_strEpisode = "0";

            if (m_FormData.FSBRO_TYPE == "G")
            {
                rdbProg.IsChecked = true;
                tbFNEPISODE.Visibility = Visibility.Visible;
                tbxEPISODE.Visibility = Visibility.Visible;
                tbEPISODE_NAME.Visibility = Visibility.Visible;
                lblEPISODE_NAME.Visibility = Visibility.Visible;
                tbPROG_NANE.Text = "節目名稱";
            }
            else if (m_FormData.FSBRO_TYPE == "P")
            {
                rdbPromo.IsChecked = true;
                tbFNEPISODE.Visibility = Visibility.Collapsed;
                tbxEPISODE.Visibility = Visibility.Collapsed;
                tbEPISODE_NAME.Visibility = Visibility.Collapsed;
                lblEPISODE_NAME.Visibility = Visibility.Collapsed;
                tbPROG_NANE.Text = "短帶名稱";
            }
            else if (m_FormData.FSBRO_TYPE == "D")
            {
                rdbData.IsChecked = true;
                tbFNEPISODE.Visibility = Visibility.Collapsed;
                tbxEPISODE.Visibility = Visibility.Collapsed;
                tbEPISODE_NAME.Visibility = Visibility.Collapsed;
                lblEPISODE_NAME.Visibility = Visibility.Collapsed;
                tbPROG_NANE.Text = "資料帶名稱";
            }

            //透過節目編號去查詢入庫影像檔
            this.DGLogList.DataContext = null;
            client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDAsync(m_strType, m_strID, m_strEpisode, m_strBroID);

            //查詢48小時內是否有排播記錄
            if (m_FormData.FSBRO_TYPE == "G")
                client.GetPROG_PLAY_NOTEAsync(m_strID, m_strEpisode);
            else if (m_FormData.FSBRO_TYPE == "P")
                client.GetPROMO_PLAY_NOTEAsync(m_strID);
        }

        #region 實作

        //實作-查詢入庫影像檔資料_透過檔案節目編號及集別
        void client_GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted(object sender, GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null )
                {
                    this.DGLogList.DataContext = e.Result;
                    ListLogVideo = e.Result;

                    if (this.DGLogList.DataContext != null)
                    {
                        DGLogList.SelectedIndex = 0;
                    }

                    client.QUERY_TBLOG_VIDEO_BYProgID_Episode_LIST_CHECKAsync(ListLogVideo);
                }
            }
        }

        //實作-用送帶轉檔單號取得送帶轉檔檔
        void client_GetTBBROADCAST_BYBROIDCompleted(object sender, GetTBBROADCAST_BYBROIDCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;

            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    m_FormData = (Class_BROADCAST)(e.Result[0]);

                    switch (m_FormData.FCCHECK_STATUS.Trim())
                    {
                        case "N": //節目製作單位輸入表單後
                            MessageBox.Show("該筆送帶轉檔單資料未審核通過，請檢查！", "提示訊息", MessageBoxButton.OK);
                            ClearSTT();
                            break;
                        case "C": //節目製作單位輸入表單後(送帶轉檔置換)
                            MessageBox.Show("該筆送帶轉檔置換單資料未審核通過，請檢查！", "提示訊息", MessageBoxButton.OK);
                            ClearSTT();
                            break;
                        case "Y": //審核核可後                          
                            ClassToForm();
                            break;
                        case "F": //審核退回後
                            MessageBox.Show("該筆送帶轉檔單資料已被駁回，請檢查！", "提示訊息", MessageBoxButton.OK);
                            ClearSTT();
                            break;
                        case "G": //線上簽收後
                            MessageBox.Show("該筆送帶轉檔單資料已完成線上簽收，請進行下一步「轉檔」動作！", "提示訊息", MessageBoxButton.OK);
                            ClearSTT();
                            break;
                        case "I": //轉檔後
                            MessageBox.Show("該筆送帶轉檔單資料已完成線上簽收並且進行轉檔，請檢查！", "提示訊息", MessageBoxButton.OK);
                            ClearSTT();
                            break;
                        case "X": //抽單
                            MessageBox.Show("該筆送帶轉檔單資料已抽單，請檢查！", "提示訊息", MessageBoxButton.OK);
                            ClearSTT();
                            break;
                        case "T": //影帶回溯
                            MessageBox.Show("該筆資料進行影帶回溯，不需要經過線上簽收，請檢查！", "提示訊息", MessageBoxButton.OK);
                            ClearSTT();
                            break;
                        default:
                            MessageBox.Show("該筆送帶轉檔單資料狀態異常，請檢查！", "提示訊息", MessageBoxButton.OK);
                            ClearSTT();
                            break;
                    }
                }
                else
                {
                    MessageBox.Show("查無此送帶轉檔單資料，請檢查！", "提示訊息", MessageBoxButton.OK);
                    ClearSTT();
                }                                  
            }
        }

        //實作-線上簽收送帶轉檔檔
        void client_UPDATE_TBBROADCAST_StausCompleted(object sender, UPDATE_TBBROADCAST_StatusCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    //若是緊急送播且為線上簽收，就要寫入主控用的資料表-TBLOG_VIDEO_HIGH_PRIORITY
                    if (m_bolUrgent == true && m_strStatus == "G")
                    {
                        List<Class_LOG_VIDEO> ListLogVideoOBJ = new List<Class_LOG_VIDEO>();    //入庫影像檔集合

                        for (int i = 0; i < ListLogVideo.Count; i++)
                        {
                            if (ListLogVideo[i].FSARC_TYPE == "001" || ListLogVideo[i].FSARC_TYPE == "002")
                                ListLogVideoOBJ.Add(ListLogVideo[i]);
                        }

                        if (ListLogVideoOBJ.Count>0)
                            client.UrgentsTTAsync(ListLogVideo);
                        else
                            saveOK();
                    }                        
                    else
                        saveOK();
                }
                else
                    saveFail();               
            }
        }

        //實作-用節目編號、節別查詢48小時內是否有排播
        void client_GetPROG_PLAY_NOTECompleted(object sender, GetPROG_PLAY_NOTECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.FBSW == true)
                {
                    m_bolUrgent = true; //緊急播出註記，因為要寫入主控用的緊急播出資料表
                    MessageBox.Show("播出日期:" + e.Result.FDDATE + " 時間:" + e.Result.FSPLAYTIME + " 僅剩:" + e.Result.FSNOTE, "提示訊息", MessageBoxButton.OK);
                    lblMsg.Visibility = Visibility.Visible;
                    lblMsg.Content = "播出日期:" + e.Result.FDDATE + " 時間:" + e.Result.FSPLAYTIME;
                }
            }
        }

        //用短帶編號查詢48小時內是否有排播
        void client_GetPROMO_PLAY_NOTECompleted(object sender, GetPROMO_PLAY_NOTECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.FBSW == true)
                {
                    MessageBox.Show("播出日期:" + e.Result.FDDATE + " 時間:" + e.Result.FSPLAYTIME + " 僅剩:" + e.Result.FSNOTE, "提示訊息", MessageBoxButton.OK);
                    lblMsg.Visibility = Visibility.Visible;
                    lblMsg.Content = "播出日期:" + e.Result.FDDATE + " 時間:" + e.Result.FSPLAYTIME;
                }
            }
        }

        //實作-查詢入庫影像檔(避免一筆以上的SD播出帶或是HD播出帶)
        void client_QUERY_TBLOG_VIDEO_BYProgID_Episode_LIST_CHECKCompleted(object sender, QUERY_TBLOG_VIDEO_BYProgID_Episode_LIST_CHECKCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    MessageBox.Show(e.Result, "提示訊息", MessageBoxButton.OK);
                    btnSign.IsEnabled = false;
                }
            }
        }

        //實作-線上簽收時發現為緊急送播，要寫入主控用的資料表
        void client_UrgentsTTCompleted(object sender, UrgentsTTCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == true)
                    saveOK();            
                else
                    saveFail();
            }
        }

        #endregion

        //取得TimeCode區間
        private string CheckDuration()
        {
            string strReturn = "";
            for (int i = 0; i < ListLogVideo.Count; i++)
            {
                if (ListLogVideo[i].FSARC_TYPE.Trim() == "001")
                {
                    strReturn = Convert.ToString(TransferTimecode.timecodetoSecond(ListLogVideo[i].FSEND_TIMECODE) - TransferTimecode.timecodetoSecond(ListLogVideo[i].FSBEG_TIMECODE)).Trim();
                    return strReturn;
                }
            }
            return "";
        }

        //確定-載入送帶轉檔單
        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            LoadBrocast();
        }

        //載入送帶轉檔單
        private void LoadBrocast()
        {
            double douCheck;   //純粹為了判斷是否為數字型態之用
            m_strBroID = tbxBro_ID.Text.ToString().Trim();

            if (m_strBroID.Trim() == "")
            {
                MessageBox.Show("請輸入送帶轉檔單編號", "提示訊息", MessageBoxButton.OK);
                m_strBroID = "";
            }
            else
            {
                if (Double.TryParse(m_strBroID, out douCheck) == false || m_strBroID.Trim().Length != 12)
                {
                    MessageBox.Show("請檢查「送帶轉檔單編號」必須為數值型態且為12碼", "提示訊息", MessageBoxButton.OK);
                    m_strBroID = "";
                }
                else
                {
                    client.GetTBBROADCAST_BYBROIDAsync(m_strBroID);
                    BusyMsg.IsBusy = true;
                }
            }  
        }

        //簽收該筆送帶轉檔
        private void btnSign_Click(object sender, RoutedEventArgs e)
        {
            if (m_strBroID.Trim() != "")
            {
                m_strStatus = "G";

                //繼續要做的就是把這筆送帶轉檔的資料狀態更新
                client.UPDATE_TBBROADCAST_StatusAsync(m_strBroID.Trim(), "G", UserClass.userData.FSUSER_ID.ToString());
            }
            else
                MessageBox.Show("請先載入送帶轉檔單", "提示訊息", MessageBoxButton.OK);
        }
        
        //清空
        private void ClearSTT()
        {
            m_strBroID = "";
            this.DGLogList.DataContext = null;
            ListLogVideo.Clear();
            m_FormData = null;
            rdbProg.IsChecked = true;
            tbFNEPISODE.Visibility = Visibility.Visible;
            tbxEPISODE.Visibility = Visibility.Visible;
            tbEPISODE_NAME.Visibility = Visibility.Visible;
            lblEPISODE_NAME.Visibility = Visibility.Visible;
            lblMsg.Visibility = Visibility.Visible;
            tbxPROG_NAME.Text = "";
            tbxPROG_NAME.Tag = "";
            tbxEPISODE.Text = "";
            lblEPISODE_NAME.Content = "";
            tbPROG_NANE.Text = "節目名稱";
            tbxBro_ID.Text = "";
            lblMsg.Content = "";
            m_parentForm.DialogResult = true;   //直接關閉
        }

        //取消
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            m_parentForm.DialogResult = true;   //取消直接關閉
        }

        //檢視轉檔資訊
        private void btnView_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGLogList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預檢視的資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            string strFileTypeName = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSARC_TYPE_NAME.Trim();
            string strFileTypeID = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSARC_TYPE.Trim();

            STT300_03 STT300_03_frm = new STT300_03(strFileTypeName, strFileTypeID, m_strType, m_strID, m_strEpisode, (Class_LOG_VIDEO)DGLogList.SelectedItem, m_strBroID);
            STT300_03_frm.Show();
        }

        //退回
        private void btnReject_Click(object sender, RoutedEventArgs e)
        {
            if (m_strBroID.Trim() != "")
            {
                MessageBoxResult result = MessageBox.Show("確定要退回送帶轉檔資料-「" + m_strBroID + "」?", "提示訊息", MessageBoxButton.OKCancel);

                if (result == MessageBoxResult.OK)
                {
                    STT200_01 STT200_01_frm = new STT200_01(m_strBroID.Trim());
                    STT200_01_frm.Show();

                    STT200_01_frm.Closing += (s, args) =>
                    {
                        //繼續要做的就是把這筆送帶轉檔的資料狀態更新成退回-F
                        client.UPDATE_TBBROADCAST_StatusAsync(m_strBroID.Trim(), "F", UserClass.userData.FSUSER_ID.ToString());
                    };
                }                
            }
            else
                MessageBox.Show("請先載入送帶轉檔單", "提示訊息", MessageBoxButton.OK);           
        }

        //執行成功
        private void saveOK()
        {
            m_parentForm.bolCheckLoad = true;

            if (m_strStatus == "G")
                MessageBox.Show("簽收送帶轉檔資料-" + m_strBroID + "成功！", "提示訊息", MessageBoxButton.OK);
            else
                MessageBox.Show("退回送帶轉檔資料-" + m_strBroID + "成功！", "提示訊息", MessageBoxButton.OK);

            m_parentForm.DialogResult = true;   //線上簽收完直接關閉
        }

        //執行失敗
        private void saveFail()
        {
            if (m_strStatus == "G")
                MessageBox.Show("簽收送帶轉檔資料-" + m_strBroID + "失敗！", "提示訊息", MessageBoxButton.OK);
            else
                MessageBox.Show("退回送帶轉檔資料-" + m_strBroID + "失敗！", "提示訊息", MessageBoxButton.OK);  
        }

    }
}
