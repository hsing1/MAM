﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSBROADCAST;


namespace PTS_MAM3.STT
{
    public partial class STT911 : Page
    {
        string Arc_Type_ID = "026";

        PTS_MAM3.WSBROADCAST.WSBROADCASTSoapClient Brocast_Client = new PTS_MAM3.WSBROADCAST.WSBROADCASTSoapClient();


        public STT911()
        {
            InitializeComponent();

            Brocast_Client.fnGetTBBROADCAST_OtherVideoCompleted += new EventHandler<fnGetTBBROADCAST_OtherVideoCompletedEventArgs>(Brocast_Client_fnGetTBBROADCAST_OtherVideoCompleted);

            Brocast_Client.GetTBLOG_VIDEO_FileInfoList_BYBROIDCompleted += new EventHandler<GetTBLOG_VIDEO_FileInfoList_BYBROIDCompletedEventArgs>(Brocast_Client_GetTBLOG_VIDEO_FileInfoList_BYBROIDCompleted);

            Brocast_Client.GetTBLOG_VIDEO_SEG_COUNTCompleted += new EventHandler<GetTBLOG_VIDEO_SEG_COUNTCompletedEventArgs>(Brocast_Client_GetTBLOG_VIDEO_SEG_COUNTCompleted);

            wsDeleteClient.DeleteFileCompleted += new EventHandler<WSDELETE.DeleteFileCompletedEventArgs>(wsDeleteClient_DeleteFileCompleted);

            Brocast_Client.UPDATE_TBBROADCAST_Staus_DirectlyCompleted += new EventHandler<UPDATE_TBBROADCAST_Staus_DirectlyCompletedEventArgs>(Brocast_Client_UPDATE_TBBROADCAST_Staus_DirectlyCompleted);

            Load_Brocast_Other();
        }

        void Brocast_Client_UPDATE_TBBROADCAST_Staus_DirectlyCompleted(object sender, UPDATE_TBBROADCAST_Staus_DirectlyCompletedEventArgs e)
        {
            Load_Brocast_Other();
        }

        void Load_Brocast_Other()
        {
            BusyMsg.IsBusy = true;
            Brocast_Client.fnGetTBBROADCAST_OtherVideoAsync("", "", "", "", DateTime.Now.AddYears(-1).ToString("yyyy-MM-dd"), DateTime.Now.ToString("yyyy-MM-dd"), UserClass.userData.FSUSER_ID);

        }
        void Brocast_Client_fnGetTBBROADCAST_OtherVideoCompleted(object sender, fnGetTBBROADCAST_OtherVideoCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                System.Windows.Data.PagedCollectionView pcv = new System.Windows.Data.PagedCollectionView(e.Result);

                this.DGBroListDouble.ItemsSource = pcv;
                DataPager.Source = pcv;
            }
            else
            {
                MessageBox.Show("獲取資料時發生錯誤。錯誤訊息：" + e.Error.Message);
            }
            BusyMsg.IsBusy = false;

        }

        private void btnBroQuery_Click(object sender, RoutedEventArgs e)
        {
            STT911_02 _911_02 = new STT911_02();
            _911_02.Closed += (s, args) =>
            {
                if (_911_02.DialogResult == true)
                {
                    System.Windows.Data.PagedCollectionView pcv = new System.Windows.Data.PagedCollectionView(_911_02.m_ListFormData);

                    this.DGBroListDouble.ItemsSource = pcv;
                    DataPager.Source = pcv;
                    //this.DGBroListDouble.ItemsSource = _911_02.m_ListFormData;
                }
            };
            _911_02.Show();
        }

        private void btnBroAdd_Click(object sender, RoutedEventArgs e)
        {
            STT911_01 _911_01 = new STT911_01(null, PTS_MAM3.STB.ActionType.Create, Arc_Type_ID);
            _911_01.Closed += (s, args) =>
            {
                Load_Brocast_Other();
            };
            _911_01.Show();
        }

        private void btnBroRefresh_Click(object sender, RoutedEventArgs e)
        {
            Load_Brocast_Other();
        }

        private void DGBroListDouble_RowDoubleClicked(object sender, DataGridDCTest.DataGridRowClickedArgs e)
        {
            STT911_01 _911_01 = new STT911_01((Class_BROADCAST)this.DGBroListDouble.SelectedItem, PTS_MAM3.STB.ActionType.View, Arc_Type_ID);
            _911_01.Show();

        }

        Class_BROADCAST class_Broadcast;
        private void DetachedBtn_Click(object sender, RoutedEventArgs e)
        {
            this.DetachedBtn.IsEnabled = false;
            class_Broadcast = ((Class_BROADCAST)DGBroListDouble.SelectedItem);
            string BRO_ID = class_Broadcast.FSBRO_ID;

            //判斷是否為本人建立
            if (class_Broadcast.FSCREATED_BY != UserClass.userData.FSUSER_ID.ToString().Trim())
            {
                MessageBox.Show("此登入帳號非此資料之申請帳號，無法抽單");
                this.DetachedBtn.IsEnabled = true;
                return;
            }


            if (class_Broadcast.FCCHECK_STATUS == "K")
            {
                Brocast_Client.GetTBLOG_VIDEO_FileInfoList_BYBROIDAsync(BRO_ID);
            }
            else//轉檔、抽單、退回
            {
                MessageBox.Show("表單狀態為「" + class_Broadcast.FCCHECK_STATUS_NAME + "」無法抽單!!");
                this.DetachedBtn.IsEnabled = true;
            }
        }

        void Brocast_Client_GetTBLOG_VIDEO_FileInfoList_BYBROIDCompleted(object sender, GetTBLOG_VIDEO_FileInfoList_BYBROIDCompletedEventArgs e)
        {
            if (e.Result.Count > 0)
            {
                string[] fileArr = e.Result[0].Split(new char[] { ';' });
                if (fileArr[1] == "待轉檔")//FCFILE_STATUS=B
                {
                    Brocast_Client.GetTBLOG_VIDEO_SEG_COUNTAsync(fileArr[0]);
                }
                else
                {
                    MessageBox.Show("檔案狀態為「" + fileArr[1] + "」無法抽單", "提示", MessageBoxButton.OK);
                    DetachedBtn.IsEnabled = true;
                }

            }
        }

        void Brocast_Client_GetTBLOG_VIDEO_SEG_COUNTCompleted(object sender, GetTBLOG_VIDEO_SEG_COUNTCompletedEventArgs e)
        {
            List<Class_LOG_VIDEO_SEG> SEG_Result = e.Result;
            bool flag = true;
            foreach (Class_LOG_VIDEO_SEG clvs in SEG_Result)
            {
                if (clvs.FCLOW_RES != "N")
                {
                    MessageBox.Show("此筆送帶轉檔單的檔案不為「待轉擋」，無法抽單", "提示", MessageBoxButton.OK);
                    this.DetachedBtn.IsEnabled = true;
                    flag = false;
                    return;
                }
            }

            if (flag)
            {
                if (ASK())
                {
                    DeleteVideoDataMethod(e.Result[0].FSFILE_NO);
                }
                else
                {
                    this.DetachedBtn.IsEnabled = true;
                }
            }
        }

        bool ASK()
        {
            bool result = false;

            MessageBoxResult dialogResult = MessageBox.Show("確定抽單?", "確認視窗", MessageBoxButton.OKCancel);
            if (dialogResult == MessageBoxResult.OK)
            {
                result = true;
            }

            return result;
        }

        PTS_MAM3.WSDELETE.Class_DELETED _class_deleted = new PTS_MAM3.WSDELETE.Class_DELETED();
        WSDELETE.WSDELETESoapClient wsDeleteClient = new WSDELETE.WSDELETESoapClient();

        void DeleteVideoDataMethod(string _fsfile_no)
        {
            List<PTS_MAM3.WSDELETE.Class_DELETED> _class_deleted_list = new List<WSDELETE.Class_DELETED>();
            _class_deleted._fsfile_no = _fsfile_no;//_deleted_file_no_list[i];
            _class_deleted._fsfile_type = "V";// this._class_deleted_parameter._fsfile_type;
            _class_deleted._fcstatus = "N";
            _class_deleted._fscreated_by = UserClass.userData.FSUSER_ID;
            _class_deleted._fdcreated_date = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
            _class_deleted._fsmemo = "刪除方式：抽單";
            _class_deleted_list.Add(_class_deleted);
            wsDeleteClient.DeleteFileAsync(_class_deleted_list);
        }

        void wsDeleteClient_DeleteFileCompleted(object sender, WSDELETE.DeleteFileCompletedEventArgs e)
        {
            if (e.Result.Count == 0)
            {
                Brocast_Client.UPDATE_TBBROADCAST_Staus_DirectlyAsync(class_Broadcast.FSBRO_ID, "J", UserClass.userData.FSUSER_ID);//改變該筆資料在TBBROADCAST中的狀態
            }
            else
            {
                //有回傳結果代表有發生錯誤，要復原資料
                wsDeleteClient.Reply_Detached_ErrorAsync(_class_deleted._fsfile_no, class_Broadcast.FCCHECK_STATUS, UserClass.userData.FSUSER_ID);
            }
        }


    }
}
