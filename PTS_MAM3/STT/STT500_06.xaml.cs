﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSMAMFunctions;
using PTS_MAM3.ProgData;
using PTS_MAM3.WSBROADCAST;
using System.Windows.Browser;

namespace PTS_MAM3.STT
{
    public partial class STT500_06 : ChildWindow
    {
        WSBROADCASTSoapClient client = new WSBROADCASTSoapClient();                 //產生新的代理類別        
        WSMAMFunctionsSoapClient clientMAM = new WSMAMFunctionsSoapClient() ;
        public List<Class_LOG_VIDEO> ListLogVideo = new List<Class_LOG_VIDEO>();    //入庫影像檔集合
        private string m_2ndMemo = "";                                              //Secondary備註
        Class_BROADCAST m_FormData = new Class_BROADCAST();

        string m_strFileTypeName = "";   //入庫檔案類型名稱
        string m_strFileTypeID = "";     //入庫檔案類型編號
        string m_strType = "";           //類型
        string m_strID = "";             //編碼
        string m_strEpisode = "";        //集別
        string m_strCHANNELID = "";      //頻道別   
        string m_strFileNO = "";         //檔案編號(修改時用的)
        string m_strBroID = "";          //送帶轉檔單單號     

        public STT500_06(string strFromID)
        {
            if (strFromID.ToString().Trim() == "")
                return;
            InitializeComponent();
            InitializeForm();     //初始化本頁面
            client.GetTBBROADCAST_BYBROIDAsync(strFromID);
        }

        void InitializeForm() //初始化本頁面
        {     
            //查詢入庫影像檔資料_透過檔案節目編號及集別            
            client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted += new EventHandler<GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompletedEventArgs>(client_GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted);

            //用送帶轉檔單號取得送帶轉檔檔
            client.GetTBBROADCAST_BYBROIDCompleted += new EventHandler<GetTBBROADCAST_BYBROIDCompletedEventArgs>(client_GetTBBROADCAST_BYBROIDCompleted);

            //列印送帶轉檔明細單
            client.CallREPORT_BRO_DetailCompleted += new EventHandler<CallREPORT_BRO_DetailCompletedEventArgs>(client_CallREPORT_BRO_DetailCompleted);
        }

        private void ClassToForm()  //將前一頁點選的節目資料繫集到畫面上
        {         
            if (m_FormData.FSBRO_TYPE.Trim() == "")
            {
                MessageBox.Show("傳入參數異常，請聯絡系統管理員", "提示訊息", MessageBoxButton.OK);
                return;  
            }

            tbxBro_ID.Text = m_FormData.FSBRO_ID;
            tbxPROG_NAME.Text = m_FormData.FSID_NAME;
            tbxPROG_NAME.Tag = m_FormData.FSID;

            m_strBroID = m_FormData.FSBRO_ID.Trim();
            m_strID = m_FormData.FSID.Trim();
            m_strType = m_FormData.FSBRO_TYPE.Trim();
            m_strEpisode = m_FormData.SHOW_FNEPISODE.Trim();
            m_strCHANNELID = m_FormData.FSCHANNELID.Trim();

            if (m_strType.Trim() == "P")
                btnPrint.Visibility = Visibility.Collapsed;   

            if (m_FormData.SHOW_FNEPISODE != "")
            {
                tbxEPISODE.Text = m_FormData.SHOW_FNEPISODE;
                lblEPISODE_NAME.Content = m_FormData.FNEPISODE_NAME;
            }
            else
                m_strEpisode = "0";

            if (m_FormData.FSBRO_TYPE == "G")
            {
                rdbProg.IsChecked = true;               
            }
            else if (m_FormData.FSBRO_TYPE == "P")
            {
                rdbPromo.IsChecked = true;
                tbPROG_NANE.Text = "短帶名稱";
                tbFNEPISODE.Visibility = Visibility.Collapsed;
                tbxEPISODE.Visibility = Visibility.Collapsed;
                btnEPISODE.Visibility = Visibility.Collapsed;
                tbEPISODE_NAME.Visibility = Visibility.Collapsed;
                lblEPISODE_NAME.Visibility = Visibility.Collapsed;             
            }

            //透過節目編號去查詢入庫影像檔
            this.DGLogList.DataContext = null;
            client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDAsync(m_strType, m_strID, m_strEpisode, m_strBroID);
        }

        #region 實作        
                
        //實作-查詢入庫影像檔資料_透過檔案節目編號及集別
        void client_GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted(object sender, GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    this.DGLogList.DataContext = e.Result;
                    ListLogVideo = e.Result;

                    if (this.DGLogList.DataContext != null)
                    {
                        DGLogList.SelectedIndex = 0;
                    }
                }
            }
        }

        //實作-用送帶轉檔單號取得送帶轉檔檔
        void client_GetTBBROADCAST_BYBROIDCompleted(object sender, GetTBBROADCAST_BYBROIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    m_FormData = (Class_BROADCAST)(e.Result[0]);
                    ClassToForm();
                }
            }
        }

        //實作-列印送帶轉檔明細單
        void client_CallREPORT_BRO_DetailCompleted(object sender, CallREPORT_BRO_DetailCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    if (e.Result.ToString().StartsWith("錯誤"))
                        MessageBox.Show(e.Result.ToString(), "提示訊息", MessageBoxButton.OK);
                    else
                        HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + e.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,resizable=yes");
                }
                else
                    MessageBox.Show("查無相關資料，或該筆資料已不存在資料庫中。", "提示訊息", MessageBoxButton.OK);
            };
        }

        #endregion        

        #region DataGrid變色

        private void ChildWindow_Loaded(object sender, RoutedEventArgs e)
        {
            DGLogList.LoadingRow += new EventHandler<DataGridRowEventArgs>(DGLogList_LoadingRow);
        }

        //實作-DataGrid變色
        void DGLogList_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Background = new SolidColorBrush(Colors.White);

            Class_LOG_VIDEO LOG_VIDEO = e.Row.DataContext as Class_LOG_VIDEO;

            if (LOG_VIDEO.FSCHANGE_FILE_NO != "")
                e.Row.Foreground = new SolidColorBrush(Colors.Red);
        }

        #endregion

        //關閉
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //列印送帶轉檔置換明細
        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGLogList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預列印的送帶轉檔置換資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //列印成案單(丟入參數：檔案編號、查詢者、SessionID)
            client.CallREPORT_BRO_DetailAsync(((Class_LOG_VIDEO)DGLogList.SelectedItem).FSFILE_NO.ToString().Trim(), UserClass.userData.FSUSER_ID.ToString().Trim(), UserClass.userData.FSSESSION_ID.ToString().Trim());        
        }

        //檢視轉檔資訊
        private void btnView_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGLogList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預檢視的資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            string strFileTypeName = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSARC_TYPE_NAME.Trim();
            string strFileTypeID = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSARC_TYPE.Trim();

            STT300_03 STT300_03_frm = new STT300_03(strFileTypeName, strFileTypeID, m_strType, m_strID, m_strEpisode, (Class_LOG_VIDEO)DGLogList.SelectedItem, m_strBroID);
            STT300_03_frm.Show();
        }
    }
}

