﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSMAMFunctions;
using PTS_MAM3.ProgData;
using PTS_MAM3.WSBROADCAST;

namespace PTS_MAM3.STT
{
    public partial class STT100_09 : ChildWindow
    {
        WSBROADCASTSoapClient client = new WSBROADCASTSoapClient();                 //產生新的代理類別        
        WSMAMFunctionsSoapClient clientMAM = new WSMAMFunctionsSoapClient() ;
        public List<Class_LOG_VIDEO> ListLogVideo = new List<Class_LOG_VIDEO>();    //入庫影像檔集合
        private string m_2ndMemo = "";                                              //Secondary備註
        Class_BROADCAST m_FormData = new Class_BROADCAST();

        string m_strFileTypeName = "";   //入庫檔案類型名稱
        string m_strFileTypeID = "";     //入庫檔案類型編號
        string m_strType = "";           //類型
        string m_strID = "";             //編碼
        string m_strEpisode = "";        //集別
        string m_strCHANNELID = "";      //頻道別   
        string m_strFileNO = "";         //檔案編號(修改時用的)
        string m_strBroID = "";          //送帶轉檔單單號     
        string m_VIDEOID_PROG = "";      //VideoID(資料帶的VIDEO ID為空值)
        string m_strCopyFSNO = "";       //複製或修改段落的檔案編號
        Boolean m_bolCopy = false;       //是否是複製的段落

        public STT100_09(Class_BROADCAST FormData)
        {
            InitializeComponent();   
            //rdbProg.IsChecked = true;  //預設節目被勾起

            InitializeForm();        //初始化本頁面
            m_FormData = FormData;
            ClassToForm();
        }

        void InitializeForm() //初始化本頁面
        {     
            //下載代碼檔_影片的檔案類型  
            client.GetTBBROADCAST_CODE_TBFILE_TYPECompleted += new EventHandler<GetTBBROADCAST_CODE_TBFILE_TYPECompletedEventArgs>(client_GetTBBROADCAST_CODE_TBFILE_TYPECompleted);
            client.GetTBBROADCAST_CODE_TBFILE_TYPEAsync();

            //查詢入庫影像檔資料_透過檔案節目編號、集別及送帶轉檔單編號            
            client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted += new EventHandler<GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompletedEventArgs>(client_GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted);

            //刪除入庫影像檔及段落檔_BY檔案編號
            client.DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONECompleted += new EventHandler<DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONECompletedEventArgs>(client_DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONECompleted);

            ////查詢TBLOG_VIDEOID_MAP的VideoID(資料帶先不考慮VideoID，因為資料帶不送到主控播出)
            //client.QueryVideoID_PROG_STTCompleted += new EventHandler<QueryVideoID_PROG_STTCompletedEventArgs>(client_QueryVideoID_PROG_STTCompleted);
        }

        private void ClassToForm()  //將前一頁點選的節目資料繫集到畫面上
        {         
            if (m_FormData.FSBRO_TYPE.Trim() == "")
            {
                MessageBox.Show("傳入參數異常，請聯絡系統管理員", "提示訊息", MessageBoxButton.OK);
                return;  
            }

            tbxBro_ID.Text = m_FormData.FSBRO_ID;
            tbxPROG_NAME.Text = m_FormData.FSID_NAME;
            tbxPROG_NAME.Tag = m_FormData.FSID;

            m_strBroID = m_FormData.FSBRO_ID.Trim();
            m_strID = m_FormData.FSID.Trim();
            m_strType = m_FormData.FSBRO_TYPE.Trim();
            m_strEpisode = m_FormData.SHOW_FNEPISODE.Trim();
            m_strCHANNELID = m_FormData.FSCHANNELID.Trim();              

            //透過資料帶編號去查詢入庫影像檔
            this.DGLogList.DataContext = null;
            client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDAsync(m_strType, m_strID, m_strEpisode, m_strBroID);         
        }

        //載入入庫影像檔
        private void LoadTBLOG_VIDEO()
        {
            client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDAsync("D", m_strID, "0", m_strBroID);  
        }

        #region 實作        

        //實作-取得代碼檔_影片的檔案類型 
        void client_GetTBBROADCAST_CODE_TBFILE_TYPECompleted(object sender, GetTBBROADCAST_CODE_TBFILE_TYPECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null )
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE_TBFILE_TYPE CodeData = new Class_CODE_TBFILE_TYPE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            case "TBFILE_TYPE":      //塞入入庫檔案類型代碼，以及把音圖文的代碼濾掉

                                //只保留資料帶，SD資料帶、HD資料帶
                                if (CodeData.FSTYPE == "影" && CodeData.NAME.IndexOf("資料帶") > 0)
                                {
                                    ComboBoxItem cbiFileTYPE = new ComboBoxItem();
                                    cbiFileTYPE.Content = CodeData.NAME;
                                    cbiFileTYPE.Tag = CodeData.ID;
                                    this.cbVideo_TYPE.Items.Add(cbiFileTYPE);
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

        //實作-查詢入庫影像檔資料_透過檔案節目編號、集別及送帶轉檔單編號 
        void client_GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted(object sender, GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null )
                {
                    this.DGLogList.DataContext = e.Result;
                    ListLogVideo = e.Result;

                    if (this.DGLogList.DataContext != null)
                    {                       
                        DGLogList.SelectedIndex = 0;
                    }
                }
            }
        }

        //實作-查詢入庫影像檔資料_透過檔案節目編號、集別
        void client_GetTBLOG_VIDEO_BYProgID_EpisodeCompleted(object sender, GetTBLOG_VIDEO_BYProgID_EpisodeCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    ListLogVideo.Clear();                           //使用前先清空
                    this.DGLogList.DataContext = null;

                    for (int i = 0; i < e.Result.Count; i++)        //要濾掉檔案來源非外部轉入，如影帶管理系統
                    {
                        if (e.Result[i].FCFROM == "N")
                            ListLogVideo.Add(e.Result[i]);
                    }

                    this.DGLogList.DataContext = ListLogVideo;

                    if (this.DGLogList.DataContext != null)
                    {
                        DGLogList.SelectedIndex = 0;
                    }
                }
            }
        }
        
        //實作-刪除入庫影像檔及段落檔_BY檔案編號
        void client_DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONECompleted(object sender, DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    MessageBox.Show("刪除段落資料成功！", "提示訊息", MessageBoxButton.OK);
                    LoadTBLOG_VIDEO();  //載入入庫影像檔
                }
                else
                    MessageBox.Show("刪除段落資料失敗！", "提示訊息", MessageBoxButton.OK);
            }
        }

        ////實作-查詢TBLOG_VIDEOID_MAP的VideoID(資料帶先不考慮VideoID，因為資料帶不送到主控播出)
        //void client_QueryVideoID_PROG_STTCompleted(object sender, QueryVideoID_PROG_STTCompletedEventArgs e)
        //{
        //    if (e.Error == null)
        //    {
        //        if (e.Result != null)
        //        {
        //            m_VIDEOID_PROG = e.Result.Trim();

        //            //新增段落
        //            Log_Video_Seg();
        //        }
        //    }
        //}  

        #endregion         

        //取得TimeCode區間
        private string CheckDuration()
        {
            string strReturn ="" ;
            for (int i = 0; i < ListLogVideo.Count; i++)
                {
                    if (ListLogVideo[i].FSARC_TYPE.Trim() == "001")
                    {
                        strReturn = Convert.ToString(TransferTimecode.timecodetoSecond(ListLogVideo[i].FSEND_TIMECODE) - TransferTimecode.timecodetoSecond(ListLogVideo[i].FSBEG_TIMECODE)).Trim();
                        return strReturn;
                    }
                }
            return "";
        }

        //按下確定後
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            Boolean bolcheck = false;

            for (int i = 0; i < ListLogVideo.Count; i++)
            {
                if (ListLogVideo[i].FSBRO_ID.Trim() == m_strBroID)
                    bolcheck = true;
            }

            if (bolcheck == false)  //判斷該筆送帶轉檔單至少輸入一筆
            {
                MessageBox.Show("請先輸入至少一筆影片段落資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            MessageBox.Show("修改送帶轉檔資料成功！", "提示訊息", MessageBoxButton.OK);
            this.DialogResult = true;
        }
        
        //取消時，要去檢查資料表後刪除
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {     
            this.DialogResult = false;            
        }
        
        #region 按鈕裡的事件

        //開啟送帶轉檔，輸入TimeCode的介面
        private void btnSeg_Click(object sender, RoutedEventArgs e)
        {
            if (m_strID == "")
            {
                MessageBox.Show("請先選擇資料帶", "提示訊息", MessageBoxButton.OK);
                return;
            }           

            if (cbVideo_TYPE.SelectedIndex != -1)                     //影片類型
            {
                m_strFileTypeName = ((ComboBoxItem)cbVideo_TYPE.SelectedItem).Content.ToString();
                m_strFileTypeID = ((ComboBoxItem)cbVideo_TYPE.SelectedItem).Tag.ToString();
            }
            else
            {
                MessageBox.Show("請先選擇影片類型", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //檢查SD播出帶或是HD播出帶只能上傳一次
            for (int i = 0; i < ListLogVideo.Count; i++)
            {
                if (ListLogVideo[i].FSARC_TYPE.Trim() == "001" || ListLogVideo[i].FSARC_TYPE.Trim() == "002")
                {
                    //擋SD及HD播出帶時，也要只比對同一筆送帶轉檔單號，如果都擋掉，置換時就無法新增 ==> && ListLogVideo[i].FSBRO_ID.Trim().Equals(m_strBroID.Trim())
                    //這裡不是置換，因此全部的單號都要比對
                    if (ListLogVideo[i].FSARC_TYPE.Trim().Equals(m_strFileTypeID.Trim()) && ListLogVideo[i].FCFILE_STATUS.Trim() != "R")//除非是檔案狀態為轉檔失敗，才能重新提出
                    {
                        MessageBox.Show((ListLogVideo[i].FSARC_TYPE_NAME.Trim() + "只能上傳一次，請重新選擇影片類型！"), "提示訊息", MessageBoxButton.OK);
                        return;
                    }
                }
            }

            m_strCopyFSNO = ""; //新增時，要加修改或複製的檔案編號清空
            m_bolCopy = false;  //不是複製的段落

            //新增或複製前先取VideoID
            client.QueryVideoID_PROG_STTAsync(m_strID, m_strEpisode, m_strFileTypeID, false); 
        }

        //修改段落檔
        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGLogList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預修改的資料", "提示訊息", MessageBoxButton.OK);
                return;
            } 

            m_strFileTypeName  = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSARC_TYPE_NAME ;
            m_strFileTypeID  = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSARC_TYPE ;

            //當送帶轉檔單不同時，表示這不是同一次的送帶轉檔申請，因此無法修改
            if (m_strBroID.Trim() != ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSBRO_ID.Trim())
            {
                MessageBox.Show("此筆並非為當次申請的段落資料，因此無法修改", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (((Class_LOG_VIDEO)DGLogList.SelectedItem).FCFILE_STATUS.Trim() != "B")
            {
                MessageBox.Show("此筆段落資料狀態不是「待轉檔」，因此無法修改", "提示訊息", MessageBoxButton.OK);
                return;
            }

            m_strCopyFSNO = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSFILE_NO; //修改時，將要傳到下一頁的檔案編號補上
            m_VIDEOID_PROG = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSVIDEO_PROG.Trim();//若是修改，表示不用重新取VideoID
            m_bolCopy = false;  //不是複製的段落

            //修改段落
            Log_Video_Seg();   
        }

        //刪除段落
        private void btnDel_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGLogList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預刪除的段落資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //當送帶轉檔單不同時，表示這不是同一次的送帶轉檔申請，因此無法修改
            if (m_strBroID.Trim() != ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSBRO_ID.Trim())
            {
                MessageBox.Show("此筆並非為當次申請的段落資料，因此無法刪除", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (((Class_LOG_VIDEO)DGLogList.SelectedItem).FCFILE_STATUS.Trim() != "B")
            {
                MessageBox.Show("此筆段落資料狀態不是「待轉檔」，因此無法刪除", "提示訊息", MessageBoxButton.OK);
                return;
            }

            MessageBoxResult resultMsg = MessageBox.Show("確定要刪除「" + ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSARC_TYPE_NAME + "」的段落資料嗎?", "提示訊息", MessageBoxButton.OKCancel);
            if (resultMsg == MessageBoxResult.Cancel)
                return;
            else
                //刪除段落檔
                client.DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONEAsync(((Class_LOG_VIDEO)DGLogList.SelectedItem).FSFILE_NO, UserClass.userData.FSUSER_ID.ToString());           
        }

        //複製段落
        private void btnCopy_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGLogList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預複製的段落資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            if (cbVideo_TYPE.SelectedIndex != -1)                     //影片類型
            {
                m_strFileTypeName = ((ComboBoxItem)cbVideo_TYPE.SelectedItem).Content.ToString();
                m_strFileTypeID = ((ComboBoxItem)cbVideo_TYPE.SelectedItem).Tag.ToString();
            }
            else
            {
                MessageBox.Show("請先選擇影片類型", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //檢查SD播出帶或是HD播出帶只能上傳一次
            for (int i = 0; i < ListLogVideo.Count; i++)
            {
                if (ListLogVideo[i].FSARC_TYPE.Trim() == "001" || ListLogVideo[i].FSARC_TYPE.Trim() == "002")
                {
                    //擋SD及HD播出帶時，也要只比對同一筆送帶轉檔單號，如果都擋掉，置換時就無法新增 ==> && ListLogVideo[i].FSBRO_ID.Trim().Equals(m_strBroID.Trim())
                    //這裡不是置換，因此全部的單號都要比對
                    if (ListLogVideo[i].FSARC_TYPE.Trim().Equals(m_strFileTypeID.Trim()) && ListLogVideo[i].FCFILE_STATUS.Trim() != "R")//除非是檔案狀態為轉檔失敗，才能重新提出
                    {
                        MessageBox.Show((ListLogVideo[i].FSARC_TYPE_NAME.Trim() + "只能上傳一次，請重新選擇影片類型！"), "提示訊息", MessageBoxButton.OK);
                        return;
                    }
                }
            }

            m_strCopyFSNO = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSFILE_NO; //複製時，將要傳到下一頁的檔案編號補上
            m_bolCopy = true;  //複製的段落

            //新增或複製前先取VideoID
            client.QueryVideoID_PROG_STTAsync(m_strID, m_strEpisode, m_strFileTypeID, false);        
        }

        //新增、修改、複製段落資料
        private void Log_Video_Seg()
        {
            string strProgName = tbxPROG_NAME.Text.Trim();
            string strEpisodeName = tbxPROG_NAME.Text.Trim();           

            STT100_01_01 STT100_01_01_frm = new STT100_01_01(m_strFileTypeName, m_strFileTypeID, m_strType, m_strID, m_strEpisode, m_strCHANNELID, m_strCopyFSNO, m_strBroID, strProgName, strEpisodeName, "", m_bolCopy, m_VIDEOID_PROG,"");
            STT100_01_01_frm.Show();

            //若是有填入段落檔，就要去讀取該節目集別的段落資料，並且秀在畫面上
            STT100_01_01_frm.Closing += (s, args) =>
            {
                if (STT100_01_01_frm.DialogResult == true)
                    LoadTBLOG_VIDEO();  //載入入庫影像檔
            };
        }   

        #endregion

        //DataGrid變色
        private void ChildWindow_Loaded(object sender, RoutedEventArgs e)
        {
            DGLogList.LoadingRow += new EventHandler<DataGridRowEventArgs>(DGLogList_LoadingRow);
        }

        //實作-DataGrid變色
        void DGLogList_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Background = new SolidColorBrush(Colors.White);

            Class_LOG_VIDEO LOG_VIDEO = e.Row.DataContext as Class_LOG_VIDEO;

            if (LOG_VIDEO.FSBRO_ID.Trim() != m_strBroID.Trim())
            {
                e.Row.Background = new SolidColorBrush(Colors.LightGray);               
            }
        }
        
  
    }
}

