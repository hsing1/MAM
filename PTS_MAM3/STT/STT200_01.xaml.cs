﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSBROADCAST;

namespace PTS_MAM3.STT
{
    public partial class STT200_01 : ChildWindow
    {
        WSBROADCASTSoapClient client = new WSBROADCASTSoapClient();                 //產生新的代理類別  
        string m_Bro_ID = "";

        public STT200_01(string strBro_ID)
        {
            InitializeComponent();

            m_Bro_ID = strBro_ID;

            //修改送帶轉檔檔的片庫駁回原因
            client.UPDATE_TBBROADCAST_RejectCompleted += new EventHandler<UPDATE_TBBROADCAST_RejectCompletedEventArgs>(client_UPDATE_TBBROADCAST_RejectCompleted);
        }

        //實作-修改送帶轉檔檔的片庫駁回原因
        void client_UPDATE_TBBROADCAST_RejectCompleted(object sender, UPDATE_TBBROADCAST_RejectCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == true)             
                    this.DialogResult = true;              
                else
                    this.DialogResult = false;
            }
        }

        //確定
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (tbxReject.Text.Trim() == "")
                MessageBox.Show("請輸入退回原因", "提示訊息", MessageBoxButton.OK);
            else
                client.UPDATE_TBBROADCAST_RejectAsync(m_Bro_ID, tbxReject.Text.Trim(), UserClass.userData.FSUSER_ID.ToString());
        }

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

