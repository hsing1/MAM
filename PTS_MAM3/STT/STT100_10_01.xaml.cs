﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSBROADCAST;
using PTS_MAM3.WSMAMFunctions;
using System.Text;
using System.Collections.ObjectModel;
using PTS_MAM3.SR_WSTSM_GetFileInfo;

namespace PTS_MAM3.STT
{
    public partial class STT100_10_01 : ChildWindow
    {
        WSBROADCASTSoapClient client = new WSBROADCASTSoapClient();                         //產生新的代理類別        
        public List<Class_LOG_VIDEO_SEG> ListSeg = new List<Class_LOG_VIDEO_SEG>();         //段落集合
        public Class_LOG_VIDEO_SEG currentSelectItemClass = new Class_LOG_VIDEO_SEG();      //目前所選到的類別

        string m_strFileTypeName = "";   //入庫檔案類型名稱
        string m_strFileTypeID = "";    //入庫檔案類型編號
        string m_strType = "";           //類型
        string m_strID = "";             //編碼
        string m_strEpisode = "";       //集別
        string m_strCHANNELID = "";     //頻道別
        string m_strFileNOEdit = "";    //檔案編號(修改時用的)  \\沒有""就是新增 有號碼就是修改
        string m_strBroID = "";         //送帶轉檔單單號
        string m_strFileNONew = "";     //檔案編號(新增時用的)  
        string m_DIRID = "";            //串結點用的
        string m_ProgName = "";         //串結點用的_節目名稱
        string m_EpisodeName = "";      //串結點用的_子集名稱
        string m_CHANGE_FILE_NO = "";   //置換檔案編碼       
        string m_VIDEOID_PROG = "";     //VideoID
        public Class_LOG_VIDEO myCLV = new Class_LOG_VIDEO();
        Boolean m_bolCopy = false;      //複製段落用
        string m_audio_track = "";
        string file_no_Ori = "";//2015-04-16 進來前已經取過新的File_no,為了避開在這裡再取一次,另外宣告一個變數記在這裡


        public STT100_10_01(string strFileTypeName, string strFileTypeID, string strType, string strID, string strEpisode, string strCHANNELID, string strFileNO, string strBroID, string strProgName, string strEpisodeName, string strCHANGE_FILE_NO, Boolean bolCopy, string VIDEOID_PROG, Class_LOG_VIDEO iniCLV, string audio_track)
        {
            InitializeComponent();

            m_strFileTypeName = strFileTypeName.Trim();
            m_strFileTypeID = strFileTypeID.Trim();
            m_strType = strType.Trim();
            m_strID = strID.Trim();
            m_strEpisode = strEpisode.Trim();
            m_strCHANNELID = strCHANNELID;
            tbFileTypeName.Text = m_strFileTypeName;
            m_strFileNOEdit = strFileNO;
            m_strBroID = strBroID;
            m_ProgName = strProgName;
            m_EpisodeName = strEpisodeName;
            m_CHANGE_FILE_NO = strCHANGE_FILE_NO;
            m_VIDEOID_PROG = VIDEOID_PROG;
            m_bolCopy = bolCopy;
            //將TBLOG_VIDEO內的段落資料傳入本頁中 以便修改資料 2012/10/01 
            myCLV = iniCLV;
            m_audio_track = audio_track;

            if (myCLV.VideoListSeg != null)
            {
                ListSeg = myCLV.VideoListSeg;
                for (int i = 0; i < ListSeg.Count; i++)
                {
                    ListSeg[i].FSVIDEO_ID = m_VIDEOID_PROG;
                }
                DGSeg.ItemsSource = ListSeg;
            }
            InitializeForm();        //初始化本頁面

            if (m_strFileNOEdit.Trim() != "" && bolCopy == false)
            {
                //若是帶入檔案編號且不需要複製段落則為預帶舊資料讓使用者修改
                this.Title = "修改段落資料";
                client.GetTBLOG_VIDEO_SEG_BYFileIDAsync(m_strFileNOEdit);
                client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDAsync(m_strType, m_strID, m_strEpisode, m_strBroID);
            }
            else if (m_strFileNOEdit.Trim() != "" && bolCopy == true)
            {
                file_no_Ori = m_strFileNOEdit.Trim();
                m_strFileNOEdit = "";   //必須把m_strFileNOEdit清空，後續判斷才會認定是新增段落                           
                //client.GetTBLOG_VIDEO_SEG_BYFileIDAsync(strFileNO_temp); //預帶舊資料讓使用者新增                
            }

            if (m_strType == "P")
                cbBooking.Content = "調用時通知「建立短帶資料」的單位主管";
            else if (m_strType == "D")
                cbBooking.Content = "調用時通知「建立資料帶資料」的單位主管";

            tbxS1.Focus(); //預設停在第一個填段落的欄位
        }

        void InitializeForm() //初始化本頁面
        {
            //新增節目或短帶段落檔
            client.INSERT_TBLOG_VIDEO_SEGCompleted += new EventHandler<INSERT_TBLOG_VIDEO_SEGCompletedEventArgs>(client_INSERT_TBLOG_VIDEO_SEGCompleted);

            //取得檔案編號
            client.fnGetFileIDCompleted += new EventHandler<fnGetFileIDCompletedEventArgs>(client_fnGetFileIDCompleted);

            //透過檔案編號取得段落檔
            client.GetTBLOG_VIDEO_SEG_BYFileIDCompleted += new EventHandler<GetTBLOG_VIDEO_SEG_BYFileIDCompletedEventArgs>(client_GetTBLOG_VIDEO_SEG_BYFileIDCompleted);


            //新增入庫影像檔
            client.INSERT_LOG_VIDEOCompleted += new EventHandler<INSERT_LOG_VIDEOCompletedEventArgs>(client_INSERT_LOG_VIDEOCompleted);

            //查詢入庫影像檔資料_透過檔案節目編號及集別            
            client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted += new EventHandler<GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompletedEventArgs>(client_GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted);

            //修改入庫影像檔的標題及描述_透過檔案編號
            client.UPDATE_TBLOG_VIDEO_TITLE_DESCompleted += new EventHandler<UPDATE_TBLOG_VIDEO_TITLE_DESCompletedEventArgs>(client_UPDATE_TBLOG_VIDEO_TITLE_DESCompleted);

            //一次寫入TBLOG_VIDEO TBLOG_VIDEO_SEG TBBROADCAST
            client.INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLISTCompleted += new EventHandler<INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLISTCompletedEventArgs>(client_INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLISTCompleted);
        }

        void client_INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLISTCompleted(object sender, INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLISTCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("新增錯誤請通知管理員!!");
                    this.DialogResult = false;
                }
            }
        }

        #region 實作

        //實作-新增節目或短帶段落檔
        void client_INSERT_TBLOG_VIDEO_SEGCompleted(object sender, INSERT_TBLOG_VIDEO_SEGCompletedEventArgs e)
        {
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    if (m_strFileNOEdit == "")
                    {
                        //MessageBox.Show("新增段落資料成功！", "提示訊息", MessageBoxButton.OK);
                        //client.INSERT_LOG_VIDEOAsync(FormToClass_LogVideo());  //段落新增成功後，再去新增入庫影像檔
                        FormToClass_LogVideo();
                    }
                    else
                    {
                        MessageBox.Show("修改段落資料成功！請通知排表人員時間段落已修改!", "提示訊息", MessageBoxButton.OK);
                        this.DialogResult = true;
                    }
                }
                else
                {
                    if (m_strFileNOEdit == "")
                    {
                        //MessageBox.Show("新增段落資料失敗！", "提示訊息", MessageBoxButton.OK);
                    }
                    else
                    {
                        MessageBox.Show("修改段落資料失敗！", "提示訊息", MessageBoxButton.OK);
                    }
                }
            }
        }

        //實作-取得檔案編號
        /// <summary>
        /// 取得檔案編號但不寫入TBLOG_VIDOE_SEG裡面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void client_fnGetFileIDCompleted(object sender, fnGetFileIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    string strGet = e.Result;  //接收回來的參數，前者是檔案編號，後者是DIRID                  

                    char[] delimiterChars = { ';' };
                    string[] words = strGet.Split(delimiterChars);

                    if (words.Length == 2)
                    {
                        m_strFileNONew = words[0];
                        m_DIRID = words[1];
                    }
                    else if (words.Length == 1)
                    {
                        m_strFileNONew = words[0];
                        m_DIRID = "";
                    }
                    else
                    {
                        m_strFileNONew = "";
                        m_DIRID = "";
                    }


                    for (int i = 0; i < ListSeg.Count; i++)
                    {
                        ListSeg[i].FSFILE_NO = m_strFileNONew.Trim();  //取得檔案編號(FSFILE_NO)塞進段落List裡
                    }
                    FormToClass_LogVideo();
                    myCLV.FSFILE_NO = m_strFileNONew.Trim(); //將資料塞入要寫回TBLOG_VIDEO的 類別
                    myCLV.VideoListSeg = ListSeg;
                    myCLV.FSTRACK = m_audio_track;
                    this.DialogResult = true;
                    //client.INSERT_TBLOG_VIDEO_SEGAsync(ListSeg);  //新增段落
                }
            }
        }


        //實作透過檔案編號取得段落檔
        void client_GetTBLOG_VIDEO_SEG_BYFileIDCompleted(object sender, GetTBLOG_VIDEO_SEG_BYFileIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    ListSeg = e.Result;

                    //若是複製段落，也要把VideoID換掉才不會出錯
                    if (m_bolCopy == true)
                    {
                        for (int i = 0; i < ListSeg.Count; i++)
                        {
                            ListSeg[i].FSVIDEO_ID = m_VIDEOID_PROG;
                        }
                    }

                    //ListSeg[i].FNBEG_TIMECODE_FRAME = MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(sqlResult[i]["FSBEG_TIMECODE"]);               //Time Code起,Frame數
                    //ListSeg[i].FNEND_TIMECODE_FRAME = MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(sqlResult[i]["FSEND_TIMECODE"]);               //Time Code迄,Frame數

                    DGSeg.ItemsSource = ListSeg;
                    tbAllDurationS.Text = checkSeg();   //顯示影片長度
                }
            }
        }

        //實作-入庫影像檔
        void client_INSERT_LOG_VIDEOCompleted(object sender, INSERT_LOG_VIDEOCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    //MessageBox.Show("新增入庫影像檔資料成功！", "提示訊息", MessageBoxButton.OK);
                    MessageBox.Show("新增資料成功！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = true;
                }
                else
                {
                    //MessageBox.Show("新增入庫影像檔資料失敗！", "提示訊息", MessageBoxButton.OK);
                    MessageBox.Show("新增資料失敗！", "提示訊息", MessageBoxButton.OK);
                }
            }
        }

        //實作-查詢入庫影像檔資料_透過檔案節目編號及集別
        void client_GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted(object sender, GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        if (m_strFileNOEdit.Trim() == (e.Result[i]).FSFILE_NO.ToString().Trim())
                        {
                            tbxTITLE.Text = (e.Result[i]).FSTITLE.ToString();
                            tbxDESCRIPTION.Text = (e.Result[i]).FSDESCRIPTION.ToString();

                            if ((e.Result[i]).FSSUPERVISOR.ToString().Trim() == "Y")
                                cbBooking.IsChecked = true;

                            return;
                        }
                    }
                }
            }
        }

        //實作-修改入庫影像檔的標題及描述_透過檔案編號
        void client_UPDATE_TBLOG_VIDEO_TITLE_DESCompleted(object sender, UPDATE_TBLOG_VIDEO_TITLE_DESCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != true)
                {
                    //MessageBox.Show("修改入庫影像檔資料成功！", "提示訊息", MessageBoxButton.OK);
                }
            }
        }

        #endregion

        #region 檢查

        private Boolean CheckFormat()    //檢查畫面上的欄位是否都填妥
        {
            StringBuilder strMessage = new StringBuilder();
            int intCheck;   //純粹為了判斷是否為數字型態之用
            string strBEGError = "";
            string strENDError = "";
            string strNewEnd = "";

            if (int.TryParse(tbxS1.Text.Trim(), out intCheck) == false || tbxS1.Text.Trim().Length != 2)
                strMessage.AppendLine("請檢查「Time Code 起」第一個欄位必須為數值型態且為兩碼");

            if (int.TryParse(tbxS2.Text.Trim(), out intCheck) == false || tbxS2.Text.Trim().Length != 2)
                strMessage.AppendLine("請檢查「Time Code 起」第二個欄位必須為數值型態且為兩碼");

            if (int.TryParse(tbxS3.Text.Trim(), out intCheck) == false || tbxS3.Text.Trim().Length != 2)
                strMessage.AppendLine("請檢查「Time Code 起」第三個欄位必須為數值型態且為兩碼");

            if (int.TryParse(tbxS4.Text.Trim(), out intCheck) == false || tbxS4.Text.Trim().Length != 2)
                strMessage.AppendLine("請檢查「Time Code 起」第四個欄位必須為數值型態且為兩碼");

            if (int.TryParse(tbxS5.Text.Trim(), out intCheck) == false || tbxS5.Text.Trim().Length != 2)
                strMessage.AppendLine("請檢查「Time Code 迄」第一個欄位必須為數值型態且為兩碼");

            if (int.TryParse(tbxS6.Text.Trim(), out intCheck) == false || tbxS6.Text.Trim().Length != 2)
                strMessage.AppendLine("請檢查「Time Code 迄」第二個欄位必須為數值型態且為兩碼");

            if (int.TryParse(tbxS7.Text.Trim(), out intCheck) == false || tbxS7.Text.Trim().Length != 2)
                strMessage.AppendLine("請檢查「Time Code 迄」第三個欄位必須為數值型態且為兩碼");

            if (int.TryParse(tbxS8.Text.Trim(), out intCheck) == false || tbxS8.Text.Trim().Length != 2)
                strMessage.AppendLine("請檢查「Time Code 迄」第四個欄位必須為數值型態且為兩碼");

            if (strMessage.ToString().Trim() == "")
            {
                TransferTimecode.check_timecode_format(tbxS1.Text.Trim(), tbxS2.Text.Trim(), tbxS3.Text.Trim(), tbxS4.Text.Trim(), out strBEGError);
                TransferTimecode.check_timecode_format(tbxS5.Text.Trim(), tbxS6.Text.Trim(), tbxS7.Text.Trim(), tbxS8.Text.Trim(), out strENDError);

                if (strBEGError.Trim() != "")
                {
                    //若是不符合規定時，起始的Timecode，最後一個變成02
                    tbxS4.Text = "02";
                    MessageBoxResult result = MessageBox.Show("Time Code 起自動改成：「" + tbxS1.Text.Trim() + ":" + tbxS2.Text.Trim() + ":" + tbxS3.Text.Trim() + ";" + tbxS4.Text.Trim() + "」", "提示訊息", MessageBoxButton.OK);

                    //原本輸入錯誤的Timecode資料會被擋掉
                    //MessageBoxResult result = MessageBox.Show("Time Code 起：" + strBEGError, "提示訊息", MessageBoxButton.OK);
                    //return false;
                }

                if (strENDError.Trim() != "")
                {
                    strNewEnd = checkEndTimecode(tbxS5.Text.Trim() + ":" + tbxS6.Text.Trim() + ":" + tbxS7.Text.Trim() + ";" + tbxS8.Text.Trim());
                    tbxS5.Text = strNewEnd.Substring(0, 2);
                    tbxS6.Text = strNewEnd.Substring(3, 2);
                    tbxS7.Text = strNewEnd.Substring(6, 2);
                    tbxS8.Text = strNewEnd.Substring(9, 2);
                    MessageBoxResult result = MessageBox.Show("Time Code 迄自動改成：「" + tbxS5.Text.Trim() + ":" + tbxS6.Text.Trim() + ":" + tbxS7.Text.Trim() + ";" + tbxS8.Text.Trim() + "」", "提示訊息", MessageBoxButton.OK);

                    //原本輸入錯誤的Timecode資料會被擋掉
                    //MessageBoxResult result = MessageBox.Show("Time Code 迄：" + strENDError, "提示訊息", MessageBoxButton.OK);
                    //return false;
                }

                if (TransferTimecode.Subtract_timecode(tbxS1.Text.Trim() + ":" + tbxS2.Text.Trim() + ":" + tbxS3.Text.Trim() + ";" + tbxS4.Text.Trim(), tbxS5.Text.Trim() + ":" + tbxS6.Text.Trim() + ":" + tbxS7.Text.Trim() + ";" + tbxS8.Text.Trim()).StartsWith("E"))
                {
                    MessageBoxResult result = MessageBox.Show("Time Code (迄) 必須大於Time Code (起)", "提示訊息", MessageBoxButton.OK);
                    return false;
                }
            }

            if (strMessage.ToString().Trim() == "")
                return true;
            else
            {
                MessageBoxResult result = MessageBox.Show(strMessage.ToString(), "提示訊息", MessageBoxButton.OK);
                return false;
            }
        }

        //將不存在的結束Timecode改成存在的
        private string checkEndTimecode(string strEnd)
        {
            string strReturn = TransferTimecode.frame2timecode(TransferTimecode.timecodetoframe(strEnd));

            if (strReturn.EndsWith("28") == true)
                strReturn = strReturn.Substring(0, 9) + "29";

            return strReturn;
        }

        #endregion

        //新增段落檔
        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            if (m_strType.Trim() == "P" && ListSeg.Count >= 1 && btnNew.Content.ToString().Trim() == "新增")
            {
                MessageBox.Show("短帶最多只能輸入一筆段落資料，請檢查！", "提示訊息", MessageBoxButton.OK);
                return;
            }

            if (CheckFormat() == false)      //檢查畫面上的欄位是否都填妥
                return;

            if (btnNew.Content.ToString().Trim() == "新增")
                FormToClass_Seg();          //新增段落資料
            else
                FormToClass_Seg_Alter();    //修改段落資料

            //clearNote();                    //欄位清空
        }

        //新增段落資料
        private void FormToClass_Seg()
        {
            Class_LOG_VIDEO_SEG ClassSeg = new Class_LOG_VIDEO_SEG();            //段落
            ClassSeg.FSVIDEO_ID = m_VIDEOID_PROG.Trim();
            ClassSeg.FSFILE_NO = m_strFileNOEdit;
            ClassSeg.FNSEG_ID = (ListSeg.Count + 1).ToString();
            ClassSeg.FSBEG_TIMECODE = tbxS1.Text.ToString().Trim() + ":" + tbxS2.Text.ToString().Trim() + ":" + tbxS3.Text.ToString().Trim() + ";" + tbxS4.Text.ToString().Trim();
            ClassSeg.FSEND_TIMECODE = tbxS5.Text.ToString().Trim() + ":" + tbxS6.Text.ToString().Trim() + ":" + tbxS7.Text.ToString().Trim() + ";" + tbxS8.Text.ToString().Trim();
            ClassSeg.FSSUB_TIMECODE = TransferTimecode.Subtract_timecode(ClassSeg.FSBEG_TIMECODE, ClassSeg.FSEND_TIMECODE);
            ClassSeg.FSCREATED_BY = UserClass.userData.FSUSER_ID.ToString();     //建檔者  
            ClassSeg.FSUPDATED_BY = UserClass.userData.FSUSER_ID.ToString();     //建檔者
            ClassSeg.FNBEG_TIMECODE_FRAME = TransferTimecode.timecodetoframe(ClassSeg.FSBEG_TIMECODE);
            ClassSeg.FNEND_TIMECODE_FRAME = TransferTimecode.timecodetoframe(ClassSeg.FSEND_TIMECODE);
            #region 加入TimeCode起始小於之前輸入TimeCode結尾提示 2012/10/24 Kyle
            foreach (var item in ListSeg)
            {
                if (item.FNEND_TIMECODE_FRAME > ClassSeg.FNBEG_TIMECODE_FRAME)
                {
                    MessageBoxResult myDecide = MessageBox.Show("本段落TimeCode起始時間，先於已輸入段落結束時間，\n'確定'加入 或 取消。", "TimeCode提示!!", MessageBoxButton.OKCancel);
                    if (myDecide == MessageBoxResult.OK)
                    {
                        break;
                    }
                    else
                    {
                        return;
                    }
                }
            }
            clearNote();        //欄位清空
            ListSeg.Add(ClassSeg);  //輸入的段落資訊插入的段落集合裡
            #endregion

            //自動重新排序
            ListSeg = TransferTimecode.SortListClass(ListSeg);

            DGSeg.ItemsSource = null;
            DGSeg.ItemsSource = ListSeg;
            tbAllDurationS.Text = checkSeg();   //顯示影片長度
        }

        //修改段落資料
        private void FormToClass_Seg_Alter()
        {

            //Class_LOG_VIDEO_SEG ClassSeg = new Class_LOG_VIDEO_SEG();            //段落
            currentSelectItemClass.FSBEG_TIMECODE = tbxS1.Text.ToString().Trim() + ":" + tbxS2.Text.ToString().Trim() + ":" + tbxS3.Text.ToString().Trim() + ";" + tbxS4.Text.ToString().Trim();
            currentSelectItemClass.FSEND_TIMECODE = tbxS5.Text.ToString().Trim() + ":" + tbxS6.Text.ToString().Trim() + ":" + tbxS7.Text.ToString().Trim() + ";" + tbxS8.Text.ToString().Trim();
            currentSelectItemClass.FSSUB_TIMECODE = TransferTimecode.Subtract_timecode(currentSelectItemClass.FSBEG_TIMECODE, currentSelectItemClass.FSEND_TIMECODE);
            currentSelectItemClass.FNBEG_TIMECODE_FRAME = TransferTimecode.timecodetoframe(currentSelectItemClass.FSBEG_TIMECODE);
            currentSelectItemClass.FNEND_TIMECODE_FRAME = TransferTimecode.timecodetoframe(currentSelectItemClass.FSEND_TIMECODE);

            //((Class_LOG_VIDEO_SEG)(DGSeg.SelectedItem)).FSBEG_TIMECODE = ClassSeg.FSBEG_TIMECODE ;
            //((Class_LOG_VIDEO_SEG)(DGSeg.SelectedItem)).FSEND_TIMECODE = ClassSeg.FSEND_TIMECODE ;
            //((Class_LOG_VIDEO_SEG)(DGSeg.SelectedItem)).FSSUB_TIMECODE = ClassSeg.FSSUB_TIMECODE ;
            //((Class_LOG_VIDEO_SEG)(DGSeg.SelectedItem)).FNBEG_TIMECODE_FRAME = ClassSeg.FNBEG_TIMECODE_FRAME ;
            //((Class_LOG_VIDEO_SEG)(DGSeg.SelectedItem)).FNEND_TIMECODE_FRAME = ClassSeg.FNEND_TIMECODE_FRAME;
            #region 加入TimeCode起始小於之前輸入TimeCode結尾提示 2012/10/24 Kyle(修改時)
            foreach (var item in ListSeg)
            {
                if (item == ((Class_LOG_VIDEO_SEG)(DGSeg.SelectedItem)))
                {
                    break;
                }
                if (item.FNEND_TIMECODE_FRAME > currentSelectItemClass.FNBEG_TIMECODE_FRAME)
                {
                    MessageBoxResult myDecide = MessageBox.Show("本段落TimeCode起始時間，先於已輸入段落結束時間，\n'確定'加入 或 取消。", "TimeCode提示!!", MessageBoxButton.OKCancel);
                    if (myDecide == MessageBoxResult.OK)
                    {
                        break;
                    }
                    else
                    {
                        return;
                    }
                }
            }
            clearNote();        //欄位清空
            #endregion


            //自動重新排序
            ListSeg = TransferTimecode.SortListClass(ListSeg);


            DGSeg.ItemsSource = null;
            DGSeg.ItemsSource = ListSeg;
            tbAllDurationS.Text = checkSeg();           //顯示影片長度
            btnCan.Visibility = Visibility.Collapsed;   //取消消失 
            btnNew.Content = "新增";                    //修改完成，要變回新增
        }

        //計算段落長度
        private string checkSeg()
        {
            int intFrame = 0;
            string strTimecode = "";

            for (int i = 0; i < ListSeg.Count; i++)
            {
                intFrame = intFrame + TransferTimecode.Subtract_timecode_toframe(ListSeg[i].FSBEG_TIMECODE, ListSeg[i].FSEND_TIMECODE);
            }

            strTimecode = TransferTimecode.frame2timecode(intFrame);
            return strTimecode.Substring(0, 8) + ";" + strTimecode.Substring(9);
        }

        //新增入庫影像資料
        private void FormToClass_LogVideo()
        {
            myCLV.FSFILE_NO = m_strFileNONew;
            myCLV.FSSUBJECT_ID = m_strFileNONew.Trim().Substring(0, 12);
            myCLV.FSTYPE = m_strType;

            myCLV.FSID = m_strID;
            myCLV.FNEPISODE = Convert.ToInt16(m_strEpisode == "" ? "0" : m_strEpisode);
            myCLV.FSPLAY = "N";
            myCLV.FSARC_TYPE_NAME = m_strFileTypeName;
            myCLV.FSARC_TYPE = m_strFileTypeID;
            myCLV.FSFILE_TYPE_HV = "mxf";     //高解檔名
            if (this.tbFileTypeName.Text.Contains("SD"))
                myCLV.FSFILE_TYPE_LV = "wmv";
            else if (this.tbFileTypeName.Text.Contains("HD"))
                myCLV.FSFILE_TYPE_LV = "mp4";

            //myCLV.FSFILE_TYPE_LV = "wmv";     //低解檔名
            myCLV.FSTITLE = tbxTITLE.Text.Trim();
            myCLV.FSDESCRIPTION = tbxDESCRIPTION.Text.Trim();
            myCLV.FSFILE_SIZE = "";
            myCLV.FCFILE_STATUS = "B";        //使用者送轉檔申請後：B

            if (m_CHANGE_FILE_NO.Trim() != "")
                myCLV.FSCHANGE_FILE_NO = m_CHANGE_FILE_NO.Trim();
            else
                myCLV.FSCHANGE_FILE_NO = "";

            myCLV.FSOLD_FILE_NAME = "";
            myCLV.FSFILE_PATH_H = "";         //高低解檔案路徑在web services裡取得
            myCLV.FSFILE_PATH_L = "";

            if (cbBooking.IsChecked == true)  //調用通知入庫單位主管
                myCLV.FSSUPERVISOR = "Y";
            else
                myCLV.FSSUPERVISOR = "N";

            if (m_DIRID != "")
                myCLV.FNDIR_ID = Convert.ToUInt32(m_DIRID);
            else
                myCLV.FNDIR_ID = 0;

            myCLV.FSCHANNEL_ID = m_strCHANNELID;//頻道別

            if (ListSeg.Count > 0)
            {
                myCLV.FSBEG_TIMECODE = ListSeg[0].FSBEG_TIMECODE.ToString().Trim();
                myCLV.FSEND_TIMECODE = ListSeg[ListSeg.Count - 1].FSEND_TIMECODE.ToString().Trim();
            }
            else
            {
                myCLV.FSBEG_TIMECODE = "";
                myCLV.FSEND_TIMECODE = "";
            }

            myCLV.FSBRO_ID = m_strBroID;
            myCLV.FSARC_ID = "";
            myCLV.FSVIDEO_PROG = m_VIDEOID_PROG;

            myCLV.FSCREATED_BY = UserClass.userData.FSUSER_ID.ToString();           //建檔者
            myCLV.FSUPDATED_BY = UserClass.userData.FSUSER_ID.ToString();           //修改者
        }

        //刪除其中一筆
        private void btnDel_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGSeg.SelectedItem == null)
            {
                MessageBox.Show("請選擇預刪除的段落資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            MessageBoxResult resultMsg = MessageBox.Show("確定要刪除「" + ((Class_LOG_VIDEO_SEG)(DGSeg.SelectedItem)).FNSEG_ID + "段」的段落資料嗎?", "提示訊息", MessageBoxButton.OKCancel);
            if (resultMsg == MessageBoxResult.Cancel)
                return;
            else
            {
                ListSeg.RemoveAt(Convert.ToInt16((((Class_LOG_VIDEO_SEG)(DGSeg.SelectedItem)).FNSEG_ID)) - 1);

                //重新給定排序
                for (int i = 0; i < ListSeg.Count; i++)
                {
                    ListSeg[i].FNSEG_ID = Convert.ToString(i + 1);
                }

                //ListSeg.RemoveAt(ListSeg.Count - 1);  //原本刪除最後一筆的作法
                DGSeg.ItemsSource = null;
                DGSeg.ItemsSource = ListSeg;

                tbxBEG_TIMECODE.Text = "";
                tbxEND_TIMECODE.Text = "";

                tbAllDurationS.Text = checkSeg();   //顯示影片長度

                if (btnNew.Content.ToString().Trim() == "修改")
                {
                    btnCan.Visibility = Visibility.Collapsed; //取消消失
                    btnNew.Content = "新增";                  //修改改成確定         
                    clearNote();                              //欄位清空
                }
            }
        }

        //按下確定時，若是有資料即上傳
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            string strBooking = "N";

            if (cbBooking.IsChecked == true)
                strBooking = "Y";

            if (ListSeg.Count > 0)
            {
                for (int i = 0; i < ListSeg.Count; i++)
                {
                    if (ListSeg[i].FSSUB_TIMECODE.ToString().Trim().Equals("00:00:00;00"))
                    {
                        MessageBox.Show("第" + ListSeg[i].FNSEG_ID.Trim() + "段的段落資料異常，請檢查", "提示訊息", MessageBoxButton.OK);
                        return;
                    }
                }

                OKButton.IsEnabled = false;  //避免使用者按了兩次，按下後要鎖住
                myCLV.FSBEG_TIMECODE = ListSeg[0].FSBEG_TIMECODE.ToString().Trim();
                myCLV.FSEND_TIMECODE = ListSeg[ListSeg.Count - 1].FSEND_TIMECODE.ToString().Trim();
                //if (m_strFileNOEdit == "")
                //{
                //    m_strFileNONew = file_no_Ori;
                //    FormToClass_LogVideo();
                //    myCLV.FSFILE_NO = m_strFileNONew.Trim(); //將資料塞入要寫回TBLOG_VIDEO的 類別
                //    myCLV.VideoListSeg = ListSeg;
                //    myCLV.FSTRACK = m_audio_track;
                //  //  this.DialogResult = true;
                //}//先判斷是修改還是新增
                //else
                //{
                //    m_strFileNONew = m_strFileNOEdit;
                //    FormToClass_LogVideo();
                //    myCLV.FSFILE_NO = m_strFileNONew.Trim(); //將資料塞入要寫回TBLOG_VIDEO的 類別
                //    myCLV.VideoListSeg = ListSeg;
                //    myCLV.FSTRACK = m_audio_track;
                //    //this.DialogResult = true;
                //}

                client.fnGetFileIDAsync(m_strType, m_strID, m_strEpisode, UserClass.userData.FSUSER_ID.ToString(), m_strCHANNELID, m_ProgName, m_EpisodeName);    //取得檔案編號(FSFILE_NO)並且上傳段落檔   
            }
            else
                MessageBox.Show("請先新增段落資料再進行上傳作業！", "提示訊息", MessageBoxButton.OK);
        }

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            myCLV.FSFILE_NO = m_strFileNONew.Trim(); //將資料塞入要寫回TBLOG_VIDEO的 類別
            myCLV.VideoListSeg = ListSeg;
            this.DialogResult = false;
        }

        //修改畫面上段落資料
        private void DGSeg_CellEditEnded(object sender, DataGridCellEditEndedEventArgs e)
        {
            string strSUB = "";
            Class_LOG_VIDEO_SEG SegRecord = new Class_LOG_VIDEO_SEG();
            SegRecord = ((Class_LOG_VIDEO_SEG)DGSeg.SelectedItem);

            if (check_Grid_TimeCode(SegRecord.FSBEG_TIMECODE, SegRecord.FSEND_TIMECODE, out strSUB) == false)
                ListSegUpdate(SegRecord.FNSEG_ID, "01:00:00;00", "01:00:00;00", "00:00:00;00");
            else
                ListSegUpdate(SegRecord.FNSEG_ID, SegRecord.FSBEG_TIMECODE, SegRecord.FSEND_TIMECODE, strSUB);

            tbAllDurationS.Text = checkSeg();   //顯示影片長度
        }

        private Boolean check_Grid_TimeCode(string strBEG, string strEND, out string strSUB)
        {
            string strBEGError = "";
            string strENDError = "";
            strSUB = "";

            if (strBEG.Length != 11)
            {
                MessageBoxResult result = MessageBox.Show("Time Code 起：長度異常", "提示訊息", MessageBoxButton.OK);
                return false;
            }

            if (strEND.Length != 11)
            {
                MessageBoxResult result = MessageBox.Show("Time Code 迄：長度異常", "提示訊息", MessageBoxButton.OK);
                return false;
            }

            TransferTimecode.check_timecode_format(strBEG.Substring(0, 2).Trim(), strBEG.Substring(3, 2).Trim(), strBEG.Substring(6, 2).Trim(), strBEG.Substring(9, 2).Trim(), out strBEGError);
            TransferTimecode.check_timecode_format(strEND.Substring(0, 2).Trim(), strEND.Substring(3, 2).Trim(), strEND.Substring(6, 2).Trim(), strEND.Substring(9, 2).Trim(), out strENDError);

            if (strBEGError.Trim() != "")
            {
                MessageBoxResult result = MessageBox.Show("Time Code 起：" + strBEGError, "提示訊息", MessageBoxButton.OK);
                return false;
            }

            if (strENDError.Trim() != "")
            {
                MessageBoxResult result = MessageBox.Show("Time Code 迄：" + strENDError, "提示訊息", MessageBoxButton.OK);
                return false;
            }

            strSUB = TransferTimecode.Subtract_timecode(strBEG, strEND);

            if (strSUB.StartsWith("E"))
            {
                MessageBoxResult result = MessageBox.Show("Time Code (迄) 必須大於Time Code (起)", "提示訊息", MessageBoxButton.OK);
                return false;
            }
            else
            {
                return true;
            }

        }

        //修改畫面上段落資料時，也將List裡改掉
        private void ListSegUpdate(string strNo, string strBeg, string strEnd, string strSub)
        {
            for (int i = 0; i < ListSeg.Count; i++)
            {
                if (ListSeg[i].FNSEG_ID.ToString().Trim().Equals(strNo))
                {
                    ListSeg[i].FSBEG_TIMECODE = strBeg;
                    ListSeg[i].FSEND_TIMECODE = strEnd;
                    ListSeg[i].FSSUB_TIMECODE = strSub;
                    ListSeg[i].FNBEG_TIMECODE_FRAME = TransferTimecode.timecodetoframe(strBeg);
                    ListSeg[i].FNEND_TIMECODE_FRAME = TransferTimecode.timecodetoframe(strEnd);
                }
            }

            //修改畫面上段落資料後，自動重新排序
            ListSeg = TransferTimecode.SortListClass(ListSeg);
            DGSeg.ItemsSource = null;
            DGSeg.ItemsSource = ListSeg;
        }

        //輸入完自動跳下一個
        private void tbxS1_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tbxS1.Text.Trim().Length == 2)
                tbxS2.Focus();
        }

        private void tbxS2_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tbxS2.Text.Trim().Length == 2)
                tbxS3.Focus();
        }

        private void tbxS3_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tbxS3.Text.Trim().Length == 2)
                tbxS4.Focus();
        }

        private void tbxS4_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tbxS4.Text.Trim().Length == 2)
                tbxS5.Focus();
        }

        private void tbxS5_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tbxS5.Text.Trim().Length == 2)
                tbxS6.Focus();
        }

        private void tbxS6_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tbxS6.Text.Trim().Length == 2)
                tbxS7.Focus();
        }

        private void tbxS7_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tbxS7.Text.Trim().Length == 2)
                tbxS8.Focus();
        }

        private void tbxS8_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tbxS8.Text.Trim().Length == 2)
                btnNew.Focus();
        }

        //欄位清空
        private void clearNote()
        {
            tbxS1.Text = "";
            tbxS2.Text = "";
            tbxS3.Text = "";
            tbxS4.Text = "";
            tbxS5.Text = "";
            tbxS6.Text = "";
            tbxS7.Text = "";
            tbxS8.Text = "";

            tbxS1.Focus();
        }

        //點選段落DataGrid，即可修改段落
        private void DGSeg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DGSeg.SelectedItem == null)
                return;

            //Class_LOG_VIDEO_SEG SegRecord = new Class_LOG_VIDEO_SEG();
            //SegRecord = ((Class_LOG_VIDEO_SEG)DGSeg.SelectedItem);
            currentSelectItemClass = ((Class_LOG_VIDEO_SEG)DGSeg.SelectedItem);

            tbxS1.Text = currentSelectItemClass.FSBEG_TIMECODE.Substring(0, 2);
            tbxS2.Text = currentSelectItemClass.FSBEG_TIMECODE.Substring(3, 2);
            tbxS3.Text = currentSelectItemClass.FSBEG_TIMECODE.Substring(6, 2);
            tbxS4.Text = currentSelectItemClass.FSBEG_TIMECODE.Substring(9, 2);

            tbxS5.Text = currentSelectItemClass.FSEND_TIMECODE.Substring(0, 2);
            tbxS6.Text = currentSelectItemClass.FSEND_TIMECODE.Substring(3, 2);
            tbxS7.Text = currentSelectItemClass.FSEND_TIMECODE.Substring(6, 2);
            tbxS8.Text = currentSelectItemClass.FSEND_TIMECODE.Substring(9, 2);

            btnCan.Visibility = Visibility.Visible; //取消出現  
            btnNew.Content = "修改";                //確定改成修改         
        }

        //取消修改段落
        private void btnCan_Click(object sender, RoutedEventArgs e)
        {
            btnCan.Visibility = Visibility.Collapsed; //取消消失
            btnNew.Content = "新增";                  //修改改成確定         
            clearNote();                              //欄位清空
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

    }
}

