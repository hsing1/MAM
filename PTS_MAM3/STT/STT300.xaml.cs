﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using PTS_MAM3.WSMAMFunctions;
using PTS_MAM3.ProgData;
using PTS_MAM3.WSBROADCAST;
using PTS_MAM3.WSSTT;

namespace PTS_MAM3.STT
{
    public partial class STT300 : Page
    {
        WSBROADCASTSoapClient client = new WSBROADCASTSoapClient();                 //產生新的代理類別    
        WSSTTSoapClient clientStt = new WSSTTSoapClient();
        WSMAMFunctionsSoapClient clientMAM = new WSMAMFunctionsSoapClient();
        public List<Class_LOG_VIDEO> ListLogVideo = new List<Class_LOG_VIDEO>();    //入庫影像檔集合
        Class_BROADCAST m_FormData = new Class_BROADCAST();

        string m_strFileTypeName = "";   //入庫檔案類型名稱
        string m_strFileTypeID = "";     //入庫檔案類型編號
        string m_strFileTypeSPEC = "";   //入庫檔案類型規格
        string m_strType = "";           //類型
        string m_strID = "";             //編碼
        string m_strEpisode = "";        //集別
        string m_strCHANNELID = "";      //頻道別   
        string m_strFileNO = "";         //檔案編號
        string m_strBroID = "";          //送帶轉檔單單號 
        string m_strHiPath = "";         //高解路徑
        string m_strLowPath = "";        //低解路徑
        private STT600_01 m_parentForm;  //使用者查詢的畫面
        string m_strProgName = "";       //節目名稱(串結點用的)
        string m_strEpisodeName = "";    //子集名稱(串結點用的)
        string m_VIDEOID_PROG = "";      //VideoID
        string m_strAudioTrack = "";
        public STT300()
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面           
        }

        public STT300(string strBroID, STT600_01 parentForm)
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面
            m_parentForm = parentForm;

            if (strBroID.Trim() != "")
            {
                tbxBro_ID.Text = strBroID.Trim();
                LoadBrocast();
            }
        }

        void InitializeForm() //初始化本頁面
        {
            //查詢入庫影像檔資料_透過檔案節目編號及集別            
            client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted += new EventHandler<GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompletedEventArgs>(client_GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted);

            //用送帶轉檔單號取得送帶轉檔檔
            client.GetTBBROADCAST_BYBROIDCompleted += new EventHandler<GetTBBROADCAST_BYBROIDCompletedEventArgs>(client_GetTBBROADCAST_BYBROIDCompleted);

            //查詢入庫影像檔(避免一筆以上的SD播出帶或是HD播出帶)
            client.QUERY_TBLOG_VIDEO_BYProgID_Episode_CHECKCompleted += new EventHandler<QUERY_TBLOG_VIDEO_BYProgID_Episode_CHECKCompletedEventArgs>(client_QUERY_TBLOG_VIDEO_BYProgID_Episode_CHECKCompleted);

            //修改入庫影像檔狀態-刪除
            client.UPDATE_TBLOG_VIDEO_FILE_STATUSCompleted += new EventHandler<UPDATE_TBLOG_VIDEO_FILE_STATUSCompletedEventArgs>(client_UPDATE_TBLOG_VIDEO_FILE_STATUSCompleted);

            //透過檔案編號，檢查JobID及檢查轉檔是否成功
            clientStt.CallIngest_CheckCompleted += new EventHandler<CallIngest_CheckCompletedEventArgs>(clientStt_CallIngest_CheckCompleted);

            //透過檔案編號，更改轉檔狀態為轉檔中(會發生於轉檔完成搬檔失敗)
            client.UPDATE_TBLOG_VIDEO_JobIDCompleted += new EventHandler<UPDATE_TBLOG_VIDEO_JobIDCompletedEventArgs>(client_UPDATE_TBLOG_VIDEO_JobIDCompleted);

            //修改入庫影像檔及轉檔檔_強制失敗時入庫影像檔更改檔案狀態為失敗，轉檔檔也要更改狀態為失敗
            client.UPDATE_TBLOG_VIDEO_TBTRANSCODE_FORCE_FAILCompleted += new EventHandler<UPDATE_TBLOG_VIDEO_TBTRANSCODE_FORCE_FAILCompletedEventArgs>(client_UPDATE_TBLOG_VIDEO_TBTRANSCODE_FORCE_FAILCompleted);

            client.UPDATE_TBLOG_VIDEO_TRACKCompleted += new EventHandler<UPDATE_TBLOG_VIDEO_TRACKCompletedEventArgs>(client_UPDATE_TBLOG_VIDEO_TRACKCompleted);
        }

        void client_UPDATE_TBLOG_VIDEO_TRACKCompleted(object sender, UPDATE_TBLOG_VIDEO_TRACKCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {
                    LoadBrocast();
                }
            }
        }

        private void ClassToForm()  //將前一頁點選的節目資料繫集到畫面上
        {
            tbxPROG_NAME.Text = "(" + m_FormData.FSID + ")" + m_FormData.FSID_NAME;
            tbxPROG_NAME.Tag = m_FormData.FSID;
            tbMEMO.Text = m_FormData.FSMEMO;//加入MEMO欄位顯示
            m_strBroID = m_FormData.FSBRO_ID.Trim();
            m_strID = m_FormData.FSID.Trim();
            m_strType = m_FormData.FSBRO_TYPE.Trim();
            m_strEpisode = m_FormData.SHOW_FNEPISODE.Trim();
            m_strCHANNELID = m_FormData.FSCHANNELID.Trim();

            if (m_FormData.SHOW_FNEPISODE != "")
            {
                tbxEPISODE.Text = m_FormData.SHOW_FNEPISODE;
                lblEPISODE_NAME.Content = m_FormData.FNEPISODE_NAME;
            }
            else
                m_strEpisode = "0";

            if (m_FormData.FSBRO_TYPE == "G")
            {
                rdbProg.IsChecked = true;
                tbFNEPISODE.Visibility = Visibility.Visible;
                tbxEPISODE.Visibility = Visibility.Visible;
                tbEPISODE_NAME.Visibility = Visibility.Visible;
                lblEPISODE_NAME.Visibility = Visibility.Visible;
                tbPROG_NANE.Text = "節目名稱";
            }
            else if (m_FormData.FSBRO_TYPE == "P")
            {
                rdbPromo.IsChecked = true;
                tbFNEPISODE.Visibility = Visibility.Collapsed;
                tbxEPISODE.Visibility = Visibility.Collapsed;
                tbEPISODE_NAME.Visibility = Visibility.Collapsed;
                lblEPISODE_NAME.Visibility = Visibility.Collapsed;
                tbPROG_NANE.Text = "短帶名稱";
            }
            else if (m_FormData.FSBRO_TYPE == "D")
            {
                rdbData.IsChecked = true;
                tbFNEPISODE.Visibility = Visibility.Collapsed;
                tbxEPISODE.Visibility = Visibility.Collapsed;
                tbEPISODE_NAME.Visibility = Visibility.Collapsed;
                lblEPISODE_NAME.Visibility = Visibility.Collapsed;
                tbPROG_NANE.Text = "資料帶名稱";
            }

            //透過節目編號去查詢入庫影像檔
            this.DGLogList.DataContext = null;
            client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDAsync(m_strType, m_strID, m_strEpisode, m_strBroID);
        }

        #region 實作

        //實作-查詢入庫影像檔資料_透過檔案節目編號及集別
        void client_GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted(object sender, GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    this.DGLogList.DataContext = e.Result;
                    ListLogVideo = e.Result;

                    if (this.DGLogList.DataContext != null)
                    {
                        DGLogList.SelectedIndex = 0;
                    }
                }
            }
        }

        //實作-用送帶轉檔單號取得送帶轉檔檔
        void client_GetTBBROADCAST_BYBROIDCompleted(object sender, GetTBBROADCAST_BYBROIDCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;

            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    m_FormData = (Class_BROADCAST)(e.Result[0]);

                    switch (m_FormData.FCCHECK_STATUS.Trim())
                    {
                        case "N": //節目製作單位輸入表單後
                            MessageBox.Show("該筆送帶轉檔單資料未審核通過，請檢查！", "提示訊息", MessageBoxButton.OK);
                            ClearSTT();
                            break;
                        case "C": //節目製作單位輸入表單後(送帶轉檔置換)
                            MessageBox.Show("該筆送帶轉檔置換單資料未審核通過，請檢查！", "提示訊息", MessageBoxButton.OK);
                            ClearSTT();
                            break;
                        case "Y": //檔案線上簽收後後                          
                            MessageBox.Show("該筆送帶轉檔單資料未線上簽收，請檢查！", "提示訊息", MessageBoxButton.OK);
                            ClearSTT();
                            break;
                        case "F": //審核退回後
                            MessageBox.Show("該筆送帶轉檔單資料已被駁回，請檢查！", "提示訊息", MessageBoxButton.OK);
                            ClearSTT();
                            m_strBroID = "";
                            break;
                        case "G": //檔案線上簽收後                          
                            ClassToForm();
                            break;
                        case "I": //轉檔後
                            ClassToForm();
                            break;
                        case "X": //抽單
                            MessageBox.Show("該筆送帶轉檔單資料已抽單，請檢查！", "提示訊息", MessageBoxButton.OK);
                            ClearSTT();
                            break;
                        case "T": //影帶回溯
                            ClassToForm();
                            break;
                        default:
                            MessageBox.Show("該筆送帶轉檔單資料狀態異常，請檢查！", "提示訊息", MessageBoxButton.OK);
                            ClearSTT();
                            break;
                    }
                }
                else
                {
                    MessageBox.Show("查無此送帶轉檔單資料，請檢查！", "提示訊息", MessageBoxButton.OK);
                    ClearSTT();
                }

            }
        }

        //實作-查詢入庫影像檔(避免一筆以上的SD播出帶或是HD播出帶)
        void client_QUERY_TBLOG_VIDEO_BYProgID_Episode_CHECKCompleted(object sender, QUERY_TBLOG_VIDEO_BYProgID_Episode_CHECKCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == true)
                    MessageBox.Show("該子集的「" + m_strFileTypeName + "」已存在一筆，因此該檔案無法進行轉檔，請檢查！", "提示訊息", MessageBoxButton.OK);
                else
                {
                    if (((Class_LOG_VIDEO)DGLogList.SelectedItem).FCFILE_STATUS.Trim() == "R")//轉檔失敗的才要檢查JobID及檢查轉檔是否成功
                        clientStt.CallIngest_CheckAsync(((Class_LOG_VIDEO)DGLogList.SelectedItem).FSFILE_NO);
                    else
                        GoINGEST();                 //第一次轉檔的
                }
            }
        }

        //實作-修改入庫影像檔狀態-刪除
        void client_UPDATE_TBLOG_VIDEO_FILE_STATUSCompleted(object sender, UPDATE_TBLOG_VIDEO_FILE_STATUSCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    MessageBox.Show("刪除檔案成功！", "提示訊息", MessageBoxButton.OK);
                    client.GetTBBROADCAST_BYBROIDAsync(m_strBroID);
                }
                else
                    MessageBox.Show("刪除檔案失敗！", "提示訊息", MessageBoxButton.OK);
            }
        }

        //實作-透過檔案編號，檢查JobID及檢查轉檔是否成功
        void clientStt_CallIngest_CheckCompleted(object sender, CallIngest_CheckCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == true)//進行搬檔作業，將檔案狀態修改為轉檔中
                    client.UPDATE_TBLOG_VIDEO_JobIDAsync(((Class_LOG_VIDEO)DGLogList.SelectedItem).FSFILE_NO, "-1", "", UserClass.userData.FSUSER_ID.ToString());
                else
                    GoINGEST(); //開啟下一頁的轉檔作業
            }
        }

        //實作-透過檔案編號，更改轉檔狀態為轉檔中(會發生於轉檔完成搬檔失敗)
        void client_UPDATE_TBLOG_VIDEO_JobIDCompleted(object sender, UPDATE_TBLOG_VIDEO_JobIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    MessageBox.Show("因為該檔案已轉檔完成，僅進行搬檔作業！", "提示訊息", MessageBoxButton.OK);
                    client.GetTBBROADCAST_BYBROIDAsync(m_strBroID);
                    m_parentForm.bolCheckLoad = true;
                }
                else
                    MessageBox.Show("因為該檔案已轉檔完成，僅進行搬檔作業，更新檔案狀態失敗！", "提示訊息", MessageBoxButton.OK);
            }
        }

        //實作-修改入庫影像檔及轉檔檔_強制失敗時入庫影像檔更改檔案狀態為失敗，轉檔檔也要更改狀態為失敗
        void client_UPDATE_TBLOG_VIDEO_TBTRANSCODE_FORCE_FAILCompleted(object sender, UPDATE_TBLOG_VIDEO_TBTRANSCODE_FORCE_FAILCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == "")
                    MessageBox.Show("「強制轉檔失敗」指令成功！", "提示訊息", MessageBoxButton.OK);
                else
                    MessageBox.Show(e.Result, "提示訊息", MessageBoxButton.OK);
            }
        }

        #endregion

        //取得TimeCode區間
        private string CheckDuration()
        {
            string strReturn = "";
            for (int i = 0; i < ListLogVideo.Count; i++)
            {
                if (ListLogVideo[i].FSARC_TYPE.Trim() == "001")
                {
                    strReturn = Convert.ToString(TransferTimecode.timecodetoSecond(ListLogVideo[i].FSEND_TIMECODE) - TransferTimecode.timecodetoSecond(ListLogVideo[i].FSBEG_TIMECODE)).Trim();
                    return strReturn;
                }
            }
            return "";
        }

        //載入送帶轉檔單
        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            LoadBrocast();
        }

        //載入送帶轉檔單
        private void LoadBrocast()
        {
            double douCheck;   //純粹為了判斷是否為數字型態之用
            m_strBroID = tbxBro_ID.Text.ToString().Trim();

            if (m_strBroID.Trim() == "")
            {
                MessageBox.Show("請輸入送帶轉檔單編號", "提示訊息", MessageBoxButton.OK);
                m_strBroID = "";
            }
            else
            {
                if (Double.TryParse(m_strBroID, out douCheck) == false || m_strBroID.Trim().Length != 12)
                {
                    MessageBox.Show("請檢查「送帶轉檔單編號」必須為數值型態且為12碼", "提示訊息", MessageBoxButton.OK);
                    m_strBroID = "";
                }
                else
                {
                    client.GetTBBROADCAST_BYBROIDAsync(m_strBroID);
                    BusyMsg.IsBusy = true;
                }
            }
        }

        //轉檔
        private void btnIngest_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGLogList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預轉檔的資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //要串給長結點用的
            string m_strProgName = tbxPROG_NAME.Text.Trim();
            string strFileStatus = "";      //檔案狀態
            string strChangeFileNo = "";    //置換檔案編號

            if (m_strType == "G")
            {
                if (m_strEpisode == "0")
                    m_strEpisodeName = m_strProgName;
                else
                    m_strEpisodeName = lblEPISODE_NAME.Content.ToString();
            }
            else
                m_strEpisodeName = m_strProgName;

            m_strFileTypeName = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSARC_TYPE_NAME.Trim();
            m_strFileTypeID = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSARC_TYPE.Trim();
            m_strFileTypeSPEC = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSARC_SPEC_NAME.Trim();
            m_strHiPath = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSFILE_PATH_H.Trim();
            m_strLowPath = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSFILE_PATH_L.Trim();
            strFileStatus = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FCFILE_STATUS.Trim();
            strChangeFileNo = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSCHANGE_FILE_NO.Trim();
            m_strAudioTrack = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSTRACK.Trim();
            if (m_strHiPath.Trim() == "")
            {
                MessageBox.Show("此檔案無高解路徑，因此無法轉檔，請檢查！", "提示訊息", MessageBoxButton.OK);
                return;
            }

            if (strFileStatus.Trim() == "B" || strFileStatus.Trim() == "O" || strFileStatus.Trim() == "R")
            {
                //要擋掉同一個子集超過一筆SD播出帶或是HD播出帶，就提醒轉擋人員無法轉檔
                if (m_strFileTypeID.Trim() == "001" || m_strFileTypeID.Trim() == "002")
                    client.QUERY_TBLOG_VIDEO_BYProgID_Episode_CHECKAsync(m_strType, m_strID, m_strEpisode, m_strFileTypeID, strChangeFileNo);
                else
                {
                    if (strFileStatus.Trim() == "R")//轉檔失敗的才要檢查JobID及檢查轉檔是否成功
                        clientStt.CallIngest_CheckAsync(((Class_LOG_VIDEO)DGLogList.SelectedItem).FSFILE_NO);
                    else
                        GoINGEST();                 //第一次轉檔的
                }
            }
            else
            {
                MessageBox.Show("此檔案狀態目前不是「待轉檔」或「轉檔失敗」，無法啟動轉檔！", "提示訊息", MessageBoxButton.OK);
                return;
            }
        }

        //進入轉檔頁面
        private void GoINGEST()
        {
            STT300_01 STT300_01_frm = new STT300_01(m_strFileTypeName, m_strFileTypeID, m_strType, m_strID, m_strEpisode, m_strCHANNELID, ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSFILE_NO, m_strBroID, tbxPROG_NAME.Text.Trim(), m_strEpisodeName, m_strHiPath, m_strLowPath, m_strFileTypeSPEC, this.tbMEMO.Text, m_strAudioTrack, (Class_LOG_VIDEO)this.DGLogList.SelectedItem);
            STT300_01_frm.Show();

            //若是有將檔案轉檔，回到主畫面後要重新整理
            STT300_01_frm.Closing += (s, args) =>
            {
                if (STT300_01_frm.DialogResult == true)
                    client.GetTBBROADCAST_BYBROIDAsync(m_strBroID);
                m_parentForm.bolCheckLoad = true;
            };
        }

        //清空
        private void ClearSTT()
        {
            m_strBroID = "";
            this.DGLogList.DataContext = null;
            ListLogVideo.Clear();
            m_FormData = null;
            rdbProg.IsChecked = true;
            tbFNEPISODE.Visibility = Visibility.Visible;
            tbxEPISODE.Visibility = Visibility.Visible;
            tbEPISODE_NAME.Visibility = Visibility.Visible;
            lblEPISODE_NAME.Visibility = Visibility.Visible;
            tbxPROG_NAME.Text = "";
            tbxPROG_NAME.Tag = "";
            tbxEPISODE.Text = "";
            lblEPISODE_NAME.Content = "";
            tbPROG_NANE.Text = "節目名稱";
            tbxBro_ID.Text = "";
            m_parentForm.DialogResult = true;   //直接關閉
        }

        //取消
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            m_parentForm.DialogResult = false;
        }

        //修改轉檔資訊
        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGLogList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預修改及檢視的資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //要串給長結點用的
            m_strProgName = tbxPROG_NAME.Text.Trim();
            string strFileStatus = "";  //檔案狀態

            if (m_strType == "G")
            {
                if (m_strEpisode == "0")
                    m_strEpisodeName = m_strProgName;
                else
                    m_strEpisodeName = lblEPISODE_NAME.Content.ToString();
            }
            else
                m_strEpisodeName = m_strProgName;

            m_strFileTypeName = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSARC_TYPE_NAME.Trim();
            m_strFileTypeID = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSARC_TYPE.Trim();
            strFileStatus = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FCFILE_STATUS.Trim();
            m_VIDEOID_PROG = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSVIDEO_PROG.Trim();

            if (strFileStatus.Trim() == "B" || strFileStatus.Trim() == "R")
            {
                STT100_01_01 STT100_01_01_frm = new STT100_01_01(m_strFileTypeName, m_strFileTypeID, m_strType, m_strID, m_strEpisode, m_strCHANNELID, ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSFILE_NO, m_strBroID, m_strProgName, m_strEpisodeName, "", false, m_VIDEOID_PROG, m_strAudioTrack);
                STT100_01_01_frm.Show();

                //若是有填入段落檔，就要去讀取該節目集別的段落資料，並且秀在畫面上
                STT100_01_01_frm.Closing += (s, args) =>
                {
                    if (STT100_01_01_frm.DialogResult == true)
                        client.GetTBBROADCAST_BYBROIDAsync(m_strBroID);
                };
            }
            else
            {
                //MessageBox.Show("此筆段落資料狀態不是「待轉檔」，因此無法修改", "提示訊息", MessageBoxButton.OK);
                STT300_03 STT300_03_frm = new STT300_03(m_strFileTypeName, m_strFileTypeID, m_strType, m_strID, m_strEpisode, (Class_LOG_VIDEO)DGLogList.SelectedItem, m_strBroID);
                STT300_03_frm.Show();
                return;
            }
        }

        //檢視轉檔資訊
        private void btnView_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGLogList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預檢視的資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            m_strFileTypeName = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSARC_TYPE_NAME.Trim();
            m_strFileTypeID = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSARC_TYPE.Trim();

            STT300_03 STT300_03_frm = new STT300_03(m_strFileTypeName, m_strFileTypeID, m_strType, m_strID, m_strEpisode, (Class_LOG_VIDEO)DGLogList.SelectedItem, m_strBroID);
            STT300_03_frm.Show();
        }

        //刪除入庫影像檔
        private void btnEdit_Del_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGLogList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預刪除的資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            string strFileStatus = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FCFILE_STATUS.Trim();

            if (strFileStatus.Trim() == "B" || strFileStatus.Trim() == "O" || strFileStatus.Trim() == "R")
            {
                MessageBoxResult resultMsg = MessageBox.Show("確定要刪除標題-「" + ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSTITLE.Trim() + "」、檔案類型-「" + ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSARC_TYPE_NAME.Trim() + "」資料嗎?", "提示訊息", MessageBoxButton.OKCancel);
                if (resultMsg == MessageBoxResult.Cancel)
                    return;
                else
                    client.UPDATE_TBLOG_VIDEO_FILE_STATUSAsync(((Class_LOG_VIDEO)DGLogList.SelectedItem).FSFILE_NO.Trim(), "D", UserClass.userData.FSUSER_ID.ToString());
            }
            else
            {
                MessageBox.Show("此檔案狀態目前不是「待轉檔」或「轉檔失敗」，無法刪除檔案！", "提示訊息", MessageBoxButton.OK);
                return;
            }
        }

        private void btnEdit_Fail_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGLogList.SelectedItem == null)
            {
                MessageBox.Show("請選擇預設定為「轉檔失敗」的資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //判斷有無修改此製作目的的權限，若是無，就讓使用者無法修改
            if (ModuleClass.getModulePermissionByName("A3", "設定轉檔失敗") == false)
            {
                MessageBox.Show("無法使用，因為沒有設定轉檔失敗的權限", "提示訊息", MessageBoxButton.OK);
                return;
            }

            string strFileStatus = ((Class_LOG_VIDEO)DGLogList.SelectedItem).FCFILE_STATUS.Trim();

            if (strFileStatus.Trim() == "S")
            {
                MessageBoxResult resultMsg = MessageBox.Show("確定要將標題-「" + ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSTITLE.Trim() + "」、檔案類型-「" + ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSARC_TYPE_NAME.Trim() + "」設定為轉檔失敗嗎?", "提示訊息", MessageBoxButton.OKCancel);
                if (resultMsg == MessageBoxResult.Cancel)
                    return;
                else
                    client.UPDATE_TBLOG_VIDEO_TBTRANSCODE_FORCE_FAILAsync(((Class_LOG_VIDEO)DGLogList.SelectedItem).FSFILE_NO.Trim(), ((Class_LOG_VIDEO)DGLogList.SelectedItem).FSJOB_ID.Trim(), UserClass.userData.FSUSER_ID.ToString());
            }
            else
            {
                MessageBox.Show("此檔案狀態目前不是「轉檔中」，無法設定為轉檔失敗！", "提示訊息", MessageBoxButton.OK);
                return;
            }
        }

        private void EditTrackBtn_Click(object sender, RoutedEventArgs e)
        {
            Button clickedBtn = ((Button)sender);
            Class_LOG_VIDEO log_video = (Class_LOG_VIDEO)clickedBtn.Tag;
            StackPanel templatePanel = (StackPanel)VisualTreeHelper.GetParent(clickedBtn);
            TextBox tbtrack = ((TextBox)templatePanel.Children[0]);
            //string tempstr = "";
            //foreach (UIElement ui in templateGrid.Children) 
            //{
            //    tempstr+=ui.ToString()+Environment.NewLine;
            //}
            //MessageBox.Show(tempstr);
            if (clickedBtn.Content.ToString() == "修改音軌")
            {
                clickedBtn.Content = "確定";
                btnIngest.IsEnabled = false;
                btnEdit.IsEnabled = false;
                btnEdit_Del.IsEnabled = false;
                btnEdit_Fail.IsEnabled = false;
                btnCancel.IsEnabled = false;

                tbtrack.IsEnabled = true;
            }
            else if (clickedBtn.Content.ToString() == "確定")
            {
                string newTrack = tbtrack.Text.Trim().ToUpper();
                if (!CheckTrackFormate(newTrack))
                {
                    MessageBox.Show("格式不符!");
                    return;
                }
                clickedBtn.Content = "修改音軌";
                log_video.FSTRACK = newTrack;
                clickedBtn.Tag = log_video;
                btnIngest.IsEnabled = true;
                btnEdit.IsEnabled = true;
                btnEdit_Del.IsEnabled = true;
                btnEdit_Fail.IsEnabled = true;
                btnCancel.IsEnabled = true;

                tbtrack.IsEnabled = false;
                client.UPDATE_TBLOG_VIDEO_TRACKAsync(log_video.FSFILE_NO, log_video.FSTRACK,UserClass.userData.FSUSER_ID);
            }

        }

        bool CheckTrackFormate(string oriTrack)
        {
            if (oriTrack.Length != 4)
            {
                return false;
            }

            char[] charArr = oriTrack.ToCharArray();
            foreach (char ch in charArr)
            {
                if (ch != 'L' && ch != 'M' && ch != 'R' && ch != '-')
                {
                    return false;
                }
            }

            return true;
        }



    }
}
