﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using PTS_MAM3.STT;
using PTS_MAM3.WSBROADCAST;
using System.Windows.Browser;
using System.Windows.Data;

namespace PTS_MAM3.STT
{
    public partial class STT600 : Page
    {
        WSBROADCASTSoapClient client = new WSBROADCASTSoapClient();             //產生新的代理類別
        List<Class_BROADCAST> m_ListFormData = new List<Class_BROADCAST>();     //記錄查詢到的成案資料
        int m_intPageCount = -1;                                                //紀錄PageIndex
        string m_strSW = "";

        public STT600(string strSW)
        {
            InitializeComponent();

            //日期查詢條件初始值
            dpBeg.Text = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-01")).AddMonths(-1).ToShortDateString();
            dpEnd.Text = DateTime.Now.ToShortDateString();

            //取得所有的送帶轉檔檔
            //client.fnGetTBBROADCAST_ALLCompleted += new EventHandler<fnGetTBBROADCAST_ALLCompletedEventArgs>(client_fnGetTBBROADCAST_ALLCompleted);
            
            //取得所有的送帶轉檔檔_BY狀態
            client.fnGetTBBROADCAST_ALL_BYSTATUSCompleted += new EventHandler<fnGetTBBROADCAST_ALL_BYSTATUSCompletedEventArgs>(client_fnGetTBBROADCAST_ALL_BYSTATUSCompleted);

            //取得所有的送帶轉檔檔_轉檔專用
            client.fnGetTBBROADCAST_ALL_BYINGESTCompleted += new EventHandler<fnGetTBBROADCAST_ALL_BYINGESTCompletedEventArgs>(client_fnGetTBBROADCAST_ALL_BYINGESTCompleted);

            //取得所有的送帶轉檔檔_轉檔專用_顯示送帶轉檔單裡有其中一個未轉檔
            client.fnGetTBBROADCAST_ALL_BYINGEST_COMPLETECompleted += new EventHandler<fnGetTBBROADCAST_ALL_BYINGEST_COMPLETECompletedEventArgs>(client_fnGetTBBROADCAST_ALL_BYINGEST_COMPLETECompleted);

            //取得所有的送帶轉檔檔_轉檔專用_顯示送帶轉檔單裡有其中一個轉檔失敗
            client.fnGetTBBROADCAST_ALL_BYINGEST_FAILCompleted += new EventHandler<fnGetTBBROADCAST_ALL_BYINGEST_FAILCompletedEventArgs>(client_fnGetTBBROADCAST_ALL_BYINGEST_FAILCompleted);

            m_strSW = strSW.Trim();

            LoadBrocast();//查詢送帶轉檔
        }    

        //實作-取得所有的送帶轉檔檔
        void client_fnGetTBBROADCAST_ALLCompleted(object sender, fnGetTBBROADCAST_ALLCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null && e.Result != null)
            {
                FillBrocast(e.Result);
            }            
        }
      
        //實作-取得所有的送帶轉檔檔_BY狀態
        void client_fnGetTBBROADCAST_ALL_BYSTATUSCompleted(object sender, fnGetTBBROADCAST_ALL_BYSTATUSCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null && e.Result != null)
            {
                FillBrocast(e.Result);
            }              
        }

        //實作-取得所有的送帶轉檔檔_轉檔專用
        void client_fnGetTBBROADCAST_ALL_BYINGESTCompleted(object sender, fnGetTBBROADCAST_ALL_BYINGESTCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null && e.Result != null)
            {
                FillBrocast(e.Result);
            }              
        }

        //實作-取得所有的送帶轉檔檔_轉檔專用_顯示送帶轉檔單裡有其中一個未轉檔
        void client_fnGetTBBROADCAST_ALL_BYINGEST_COMPLETECompleted(object sender, fnGetTBBROADCAST_ALL_BYINGEST_COMPLETECompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null && e.Result != null)
            {
                FillBrocast(e.Result);
            }            
        }

        //實作-取得所有的送帶轉檔檔_轉檔專用_顯示送帶轉檔單裡有其中一個轉檔失敗
        void client_fnGetTBBROADCAST_ALL_BYINGEST_FAILCompleted(object sender, fnGetTBBROADCAST_ALL_BYINGEST_FAILCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null && e.Result != null)
            {
                FillBrocast(e.Result);
            }            
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        //將取得的送帶轉檔資料放到DataGrid及分頁
        private void FillBrocast(List<Class_BROADCAST> ListBROADCAST)
        {
            PagedCollectionView pcv = new PagedCollectionView(ListBROADCAST);
            DGBroListDouble.ItemsSource = pcv;
            DataPager.Source = pcv;

            if (m_intPageCount != -1)        //設定修改後查詢完要回到哪一頁
            {
                DataPager.PageIndex = m_intPageCount;
                m_intPageCount = -1;
            }   
        }

        //查詢送帶轉檔
        private void LoadBrocast()
        {
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息

            switch (m_strSW)
            {
                case "CheckIn":         //線上簽收
                    client.fnGetTBBROADCAST_ALL_BYSTATUSAsync("Y", dpBeg.Text.ToString(), dpEnd.Text.ToString());
                    this.DGBroListDouble.Columns[0].Visibility = Visibility.Visible;

                    this.DGBroListDouble.Columns[14].Visibility = Visibility.Collapsed;
                    this.DGBroListDouble.Columns[15].Visibility = Visibility.Collapsed;
                    break;
                case "TransCode":       //轉檔
                    //client.fnGetTBBROADCAST_ALL_BYSTATUSAsync("G"); 
                    client.fnGetTBBROADCAST_ALL_BYINGESTAsync(dpBeg.Text.ToString(), dpEnd.Text.ToString());
                    this.DGBroListDouble.Columns[1].Visibility = Visibility.Visible;
                    this.DGBroListDouble.Columns[2].Visibility = Visibility.Visible;
                    RBSIngest.Visibility = Visibility.Visible;
                    btnQuery_NONINGEST.Visibility = Visibility.Visible;
                    btnQuery_Fail.Visibility = Visibility.Visible;
                    Sep_Fail.Visibility = Visibility.Visible;
                    break;
                case "TransCodeProgress":     //查詢進度  
                    client.fnGetTBBROADCAST_ALL_BYSTATUSAsync("I", dpBeg.Text.ToString(), dpEnd.Text.ToString());
                    this.DGBroListDouble.Columns[2].Visibility = Visibility.Visible;
                    break;
                default:
                    break;
            }
        }

        //連續點擊-檢視送帶轉檔  
        private void DGBroListDouble_RowDoubleClicked(object sender, DataGridDCTest.DataGridRowClickedArgs e)
        {
            DataGridDCTest.DoubleClickDataGrid dcdg = sender as DataGridDCTest.DoubleClickDataGrid;

            //判斷是否有選取DataGrid
            if (dcdg.SelectedItem == null)
            {
                MessageBox.Show("請選擇預檢視的送帶轉檔資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            STT100_06 STT100_06_frm = new STT100_06(((Class_BROADCAST)DGBroListDouble.SelectedItem).FSBRO_ID.ToString().Trim());
            STT100_06_frm.Show();
        }

        private void btnCheckIN_Click(object sender, RoutedEventArgs e)
        {
            OpenChildWindow("");
        }

        private void btnIngest_Click(object sender, RoutedEventArgs e)
        {
            OpenChildWindow("Ingest");
        }

        private void btnProcess_Click(object sender, RoutedEventArgs e)
        {
            OpenChildWindow("Process");
        }

        private void OpenChildWindow(string strSW2)
        {       
            STT600_01 STT600_01_frm = new STT600_01(((Class_BROADCAST)DGBroListDouble.SelectedItem).FSBRO_ID.ToString().Trim(), m_strSW, strSW2);
            STT600_01_frm.Show();

            STT600_01_frm.Closed += (s, args) =>
            {
                if (STT600_01_frm.bolCheckLoad == true)
                {
                    switch (m_strSW.Trim())
                    {
                        case "CheckIn":     //線上簽收
                            m_intPageCount = DataPager.PageIndex; //取得PageIndex，只有線上簽收會重Load，所以要特別指定頁數
                            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
                            client.fnGetTBBROADCAST_ALL_BYSTATUSAsync("Y", dpBeg.Text.ToString(), dpEnd.Text.ToString());
                            break;
                        //case "TransCode":     //轉檔    //轉檔後的狀態是改成I，但是轉檔是轉檔中與轉檔後都能看到，因此不用重新Load
                        //    //client.fnGetTBBROADCAST_ALL_BYSTATUSAsync("G");
                        //    client.fnGetTBBROADCAST_ALL_BYINGESTAsync();
                        //    break;
                        //case "TransCodeProgress":     //查詢進度  //查詢進度不會改變狀態，因此不用重新Load 
                        //    client.fnGetTBBROADCAST_ALL_BYSTATUSAsync("I");
                        //    break;
                        default:
                            break;
                    }
                }
            };
        }

        //DataGrid變色
        private void ChildWindow_Loaded(object sender, RoutedEventArgs e)
        {
            //DGBroListDouble.LoadingRow += new EventHandler<DataGridRowEventArgs>(DGBroListDouble_LoadingRow);
        }

        ////實作-DataGrid變色 //點進去只秀每一個的選項，因此不判斷
        //void DGBroListDouble_LoadingRow(object sender, DataGridRowEventArgs e)
        //{     
        //    Class_BROADCAST BROADCAST = e.Row.DataContext as Class_BROADCAST;            

        //    switch (BROADCAST.FCCHECK_STATUS.Trim())
        //    {
        //        case "Y":     //線上簽收
        //           ((Button)DGBroListDouble.Columns[0].GetCellContent(e.Row)).IsEnabled = true;
        //           this.DGBroListDouble.Columns[0].Visibility = Visibility.Visible;
        //            break;
        //        case "G":     //轉檔
        //           ((Button)DGBroListDouble.Columns[1].GetCellContent(e.Row)).IsEnabled = true;
        //           this.DGBroListDouble.Columns[1].Visibility = Visibility.Visible;
        //            break;
        //        case "I":     //查詢進度  
        //           ((Button)DGBroListDouble.Columns[1].GetCellContent(e.Row)).IsEnabled = true;      
        //           ((Button)DGBroListDouble.Columns[2].GetCellContent(e.Row)).IsEnabled = true;
        //           this.DGBroListDouble.Columns[1].Visibility = Visibility.Visible;
        //           this.DGBroListDouble.Columns[2].Visibility = Visibility.Visible;
        //            break;
        //         default:
        //            break;                    
        //    }            
        // }

        //待線上簽收 

        private void btnCheckINM_Click(object sender, RoutedEventArgs e)
        {
            client.fnGetTBBROADCAST_ALL_BYSTATUSAsync("Y", dpBeg.Text.ToString(), dpEnd.Text.ToString());
        }

        //待轉檔
        private void btnIngestM_Click(object sender, RoutedEventArgs e)
        {
            client.fnGetTBBROADCAST_ALL_BYSTATUSAsync("G", dpBeg.Text.ToString(), dpEnd.Text.ToString());
        }

        //查全部
        private void btnAll_Click(object sender, RoutedEventArgs e)
        {
           //client.fnGetTBBROADCAST_ALLAsync(); 
        }

        //用條件查詢
        private void btnQuery_Click(object sender, RoutedEventArgs e)
        {
            //加入若無輸入日期強制預設
            if (dpBeg.Text == "")
                dpBeg.Text = "2000/01/01";
            if (dpEnd.Text == "")
                dpEnd.Text = DateTime.Today.AddDays(1).ToShortDateString();

            if (Convert.ToDateTime(dpEnd.Text) < Convert.ToDateTime(dpBeg.Text))
                MessageBox.Show("請檢查查詢條件的起迄日期", "提示訊息", MessageBoxButton.OK);
            else
                LoadBrocast();//查詢送帶轉檔            
        }

        //取得所有的送帶轉檔檔_轉檔專用_顯示送帶轉檔單裡有其中一個未轉檔
        private void btnQuery_NONINGEST_Click(object sender, RoutedEventArgs e)
        {
            //加入若無輸入日期強制預設
            if (dpBeg.Text == "")
                dpBeg.Text = "2000/01/01";
            if (dpEnd.Text == "")
                dpEnd.Text = DateTime.Today.AddDays(1).ToShortDateString();

            if (Convert.ToDateTime(dpEnd.Text) < Convert.ToDateTime(dpBeg.Text))
                MessageBox.Show("請檢查查詢條件的起迄日期", "提示訊息", MessageBoxButton.OK);
            else
                client.fnGetTBBROADCAST_ALL_BYINGEST_COMPLETEAsync(dpBeg.Text.ToString(), dpEnd.Text.ToString());
        }

        //取得所有的送帶轉檔檔_轉檔專用_顯示送帶轉檔單裡有其中一個轉檔失敗
        private void btnQuery_Fail_Click(object sender, RoutedEventArgs e)
        {
            //加入若無輸入日期強制預設
            if (dpBeg.Text == "")
                dpBeg.Text = "2000/01/01";
            if (dpEnd.Text == "")
                dpEnd.Text = DateTime.Today.AddDays(1).ToShortDateString();

            if (Convert.ToDateTime(dpEnd.Text) < Convert.ToDateTime(dpBeg.Text))
                MessageBox.Show("請檢查查詢條件的起迄日期", "提示訊息", MessageBoxButton.OK);
            else
                client.fnGetTBBROADCAST_ALL_BYINGEST_FAILAsync(dpBeg.Text.ToString(), dpEnd.Text.ToString());
        }

        //進階查詢
        private void btnQuery_Advance_Click(object sender, RoutedEventArgs e)
        {
            STT600_04 STT600_04_frm = new STT600_04(m_strSW);
            STT600_04_frm.Show();

            STT600_04_frm.Closed += (s, args) =>
            {
                if (STT600_04_frm.m_ListFormData.Count != 0)
                {
                    DGBroListDouble.ItemsSource = null;
                    myList = STT600_04_frm.m_ListFormData;
                    PagedCollectionView pcv = new PagedCollectionView(STT600_04_frm.m_ListFormData);
                    DGBroListDouble.ItemsSource = pcv;
                    DataPager.Source = pcv;
                }
            };
        }
        List<Class_BROADCAST> myList = new List<Class_BROADCAST>();
        private void DGBroListDouble_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            Class_BROADCAST myClass = new Class_BROADCAST();
            //myClass=
            myClass = (Class_BROADCAST)e.Row.DataContext;
            if (myClass.FCCHECK_STATUS=="Y")
            {
                ((Button)DGBroListDouble.Columns[0].GetCellContent(e.Row)).IsEnabled = true;
                ((Button)DGBroListDouble.Columns[0].GetCellContent(e.Row)).Content = "線上簽收";

            }
            else if (myClass.FCCHECK_STATUS == "G")
            {
                ((Button)DGBroListDouble.Columns[0].GetCellContent(e.Row)).IsEnabled = false;
                ((Button)DGBroListDouble.Columns[0].GetCellContent(e.Row)).Content = "已簽收";
            }   
            //if (((Class_BROADCAST)e.Row.ge).FCCHECK_STATUS == "")
            //{ }
        }

    }

}
