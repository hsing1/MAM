﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PTS_MAM3.STT
{
    public partial class STT100_01_03 : ChildWindow
    {
        public string _fsTREE_PATH = string.Empty;
        public string _fsFILE_PATH = string.Empty;
        public string _fsTYPE = string.Empty;

        public STT100_01_03()
        {
            InitializeComponent();
            this.Title = "檔案來源";
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            //若選擇數位檔案，則要跳出挑選檔案路徑視窗
            STT300_01_01 _stt300_01_01 = null;
            if (this.RadioButton_Digital.IsChecked == true)
            {
                _stt300_01_01 = new STT300_01_01("Network");
                this._fsTYPE = "數位檔案(網路)";
                _stt300_01_01.Show();
            }
            else if (this.RadioButton_Digital_Fiber.IsChecked == true)
            {
                _stt300_01_01 = new STT300_01_01("Fiber");
                this._fsTYPE = "數位檔案(光纖)";
                _stt300_01_01.Show();
            }
            else
            {
                this._fsTYPE = "影帶擷取";
                this.DialogResult = true;
            }

            if (_stt300_01_01 != null)
            {
                _stt300_01_01.Closed += (s, args) =>
                {
                    if (_stt300_01_01.DialogResult == true)
                    {
                        this._fsTREE_PATH = _stt300_01_01._fsTREE_PATH;
                        this._fsFILE_PATH = _stt300_01_01.strSelect_File_Path;

                        this.DialogResult = true;
                    }
                };
            }

        }

    }
}

