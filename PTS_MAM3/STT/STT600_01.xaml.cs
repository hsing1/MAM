﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;

namespace PTS_MAM3.STT
{
    public partial class STT600_01 : ChildWindow
    {
        public Boolean bolCheckLoad = false; //判斷是否有更新檔案，要不要重新Load

        public STT600_01(string strBroID,string strSW,string strSW2)
        {
            InitializeComponent();
            //setPane(strBroID);    //若是要一次開啟許多頁的寫法

            switch (strSW.Trim())
            {
                case "CheckIn":     //線上簽收                    
                    STT200 ucSTT200 = new STT200(strBroID,this);
                    this.PageContainer.Child = ucSTT200;
                    this.Width = 720;
                    this.Title = this.Title + "-線上簽收";
                    break;
                case "TransCode":      //轉檔
                    if (strSW2 == "Ingest")
                    {
                        STT300 ucSTT300 = new STT300(strBroID,this);
                        this.PageContainer.Child = ucSTT300;
                        this.Width = 820;
                        this.Title = this.Title + "-轉檔";
                        break;
                    }
                    else
                    {
                        STT400 ucSTT400 = new STT400(strBroID, this);
                        this.PageContainer.Child = ucSTT400;
                        this.Width = 1000;
                        this.Title = this.Title + "-進度查詢";
                        break;
                    }
                   
                case "TransCodeProgress":     //查詢進度                          
                    STT400 ucSTT400_2 = new STT400(strBroID,this);
                    this.PageContainer.Child = ucSTT400_2;
                    this.Width = 1000;
                    this.Title = this.Title + "-進度查詢";
                    break;
            }
        }

        //一次開啟許多頁的寫法，目前只開一頁，因此程式先mark
        //private void setPane(string strBroID)
        //{

        //    RadPane radPane1 = new RadPane();

        //    radPane1.Header = "線上簽收";
        //    STT.STT200 ctrTapeSTT200 = new STT.STT200(strBroID);
        //    radPane1.Content = ctrTapeSTT200;
        //    this.RadPaneSign.Items.Add(radPane1);

        //    RadPane radPane2 = new RadPane();
        //    radPane2.Header = "轉檔";
        //    STT.STT300 ctrTapeSTT300 = new STT.STT300(strBroID);
        //    radPane2.Content = ctrTapeSTT300;
        //    this.RadPaneSign.Items.Add(radPane2);

        //    RadPane radPane3 = new RadPane();
        //    radPane3.Header = "進度查詢";
        //    STT.STT400 ctrTapeSTT400 = new STT.STT400(strBroID);
        //    radPane3.Content = ctrTapeSTT400;
        //    this.RadPaneSign.Items.Add(radPane3);
        //}

    }
}

