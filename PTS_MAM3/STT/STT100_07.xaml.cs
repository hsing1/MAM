﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSMAMFunctions;
using PTS_MAM3.ProgData;
using PTS_MAM3.WSBROADCAST;
using PTS_MAM3.PRG;
using System.Text;
using PTS_MAM3.WSPROMO;
using PTS_MAM3.FlowMAM;

namespace PTS_MAM3.STT
{
    public partial class STT100_07 : ChildWindow
    {
        WSBROADCASTSoapClient client = new WSBROADCASTSoapClient();                         //產生新的代理類別        
        WSMAMFunctionsSoapClient clientMAM = new WSMAMFunctionsSoapClient();
        WSPROMOSoapClient clientPromo = new WSPROMOSoapClient();
        public List<Class_LOG_VIDEO_SEG> ListSeg = new List<Class_LOG_VIDEO_SEG>();         //段落集合
        FlowMAMSoapClient clientFlow = new FlowMAMSoapClient();                             //後端的流程引擎

        string m_strBroID = "";         //送帶轉檔單單號
        string m_strID = "";            //短帶編碼
        string m_strFileTypeID = "";    //入庫檔案類型編號
        string m_strCHANNELID = "";     //頻道別
        string m_strFileNOEdit = "";    //檔案編號(修改時用的)
        string m_strFileNONew = "";     //檔案編號(新增時用的)  
        string m_DIRID = "";            //串結點用的
        string m_strBeg = "";           //起始段落
        string m_strEnd = "";           //結束段落
        Boolean m_continue = false;     //繼續新增
        string m_strPromoName = "";     //短帶名稱(新增短帶後就要送帶轉檔才有)
        string m_strPrintID = "";       //列印編號
        string m_strDep_ID = "";        //成案單位
        string m_VIDEOID_PROG = "";     //VideoID
        bool isOpenPGM110 = false;      //增加判斷是否開啟短帶托播的頁面

        public STT100_07(string strPromoName, string strPrintID)
        {
            InitializeComponent();
            InitializeForm();               //初始化本頁面   

            m_strPromoName = strPromoName;  //新增短帶後就要送帶轉檔
            clientMAM.GetNoRecordAsync("07", "", "", UserClass.userData.FSUSER_ID);//取得送帶轉檔單編號
            BusyMsg.IsBusy = true;

            if (strPrintID == "")
                GetPrintID();               //取得列印序號
            else
                tbxPRINTID.Text = strPrintID;
        }

        public STT100_07(Class_BROADCAST FormData)
        {
            InitializeComponent();
            InitializeForm();          //初始化本頁面  

            classToForm(FormData);
            this.Title = "修改短帶送帶轉檔";
            ContinueButton.IsEnabled = false;
            cbVideo_TYPE.IsEnabled = false;     //修改段落時，不能修改檔案類型
            tbxPRINTID.IsReadOnly = false;       //修改時，可以異動列印編號

            client.GetTBBROADCAST_CODE_TBFILE_TYPEAsync();  //下載代碼檔_影片的檔案類型 
            BusyMsg.IsBusy = true;
        }

        void InitializeForm() //初始化本頁面
        {
            //取得送帶轉檔單編號
            clientMAM.GetNoRecordCompleted += new EventHandler<GetNoRecordCompletedEventArgs>(ClientMAM_GetNoRecordCompleted);

            //下載代碼檔_影片的檔案類型  
            client.GetTBBROADCAST_CODE_TBFILE_TYPECompleted += new EventHandler<GetTBBROADCAST_CODE_TBFILE_TYPECompletedEventArgs>(client_GetTBBROADCAST_CODE_TBFILE_TYPECompleted);

            //新增送帶轉檔單資料
            client.INSERT_BROADCASTCompleted += new EventHandler<INSERT_BROADCASTCompletedEventArgs>(client_INSERT_BROADCASTCompleted);

            //新增入庫影像檔
            client.INSERT_LOG_VIDEOCompleted += new EventHandler<INSERT_LOG_VIDEOCompletedEventArgs>(client_INSERT_LOG_VIDEOCompleted);

            //新增節目或短帶段落檔
            client.INSERT_TBLOG_VIDEO_SEGCompleted += new EventHandler<INSERT_TBLOG_VIDEO_SEGCompletedEventArgs>(client_INSERT_TBLOG_VIDEO_SEGCompleted);

            //取得檔案編號
            client.fnGetFileIDCompleted += new EventHandler<fnGetFileIDCompletedEventArgs>(client_fnGetFileIDCompleted);

            //修改入庫影像段落檔
            client.UPDATE_TBLOG_VIDEO_SEG_BYFILENOCompleted += new EventHandler<UPDATE_TBLOG_VIDEO_SEG_BYFILENOCompletedEventArgs>(client_UPDATE_TBLOG_VIDEO_SEG_BYFILENOCompleted);

            //修改入庫影像檔
            client.UPDATE_TBLOG_VIDEO_TITLE_DES_ARCCompleted += new EventHandler<UPDATE_TBLOG_VIDEO_TITLE_DES_ARCCompletedEventArgs>(client_UPDATE_TBLOG_VIDEO_TITLE_DES_ARCCompleted);

            //用短帶編號查詢48小時內是否有排播
            client.GetPROMO_PLAY_NOTECompleted += new EventHandler<GetPROMO_PLAY_NOTECompletedEventArgs>(client_GetPROMO_PLAY_NOTECompleted);

            //查詢入庫影像檔資料_透過檔案節目編號及集別            
            client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted += new EventHandler<GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompletedEventArgs>(client_GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted);

            //透過檔案編號取得段落檔
            client.GetTBLOG_VIDEO_SEG_BYFileIDCompleted += new EventHandler<GetTBLOG_VIDEO_SEG_BYFileIDCompletedEventArgs>(client_GetTBLOG_VIDEO_SEG_BYFileIDCompleted);

            //查詢宣傳帶資料檔_透過宣傳帶名稱(新增短帶資料完就要送帶轉擋)
            clientPromo.QUERY_TBPROMO_BYNAME_STTCompleted += new EventHandler<QUERY_TBPROMO_BYNAME_STTCompletedEventArgs>(clientPromo_QUERY_TBPROMO_BYNAME_STTCompleted);

            //修改送帶轉檔檔的列印編號
            client.UPDATE_BROADCAST_PRINTIDCompleted += new EventHandler<UPDATE_BROADCAST_PRINTIDCompletedEventArgs>(client_UPDATE_BROADCAST_PRINTIDCompleted);

            //查詢TBLOG_VIDEOID_MAP的VideoID
            client.QueryVideoID_PROG_STTCompleted += new EventHandler<QueryVideoID_PROG_STTCompletedEventArgs>(client_QueryVideoID_PROG_STTCompleted);

            //後端的流程引擎-流程起始
            clientFlow.CallFlow_NewFlowWithFieldCompleted += new EventHandler<CallFlow_NewFlowWithFieldCompletedEventArgs>(clientFlow_CallFlow_NewFlowWithFieldCompleted);
            //不跑Flow新增送帶轉檔單 by Jarvis20130710
            client.INSERT_BROADCAST_WITHOUT_FLOWCompleted += new EventHandler<INSERT_BROADCAST_WITHOUT_FLOWCompletedEventArgs>(client_INSERT_BROADCAST_WITHOUT_FLOWCompleted);
            //
            client.Query_CheckIsAbleToCreateNewTransformFormCompleted += new EventHandler<Query_CheckIsAbleToCreateNewTransformFormCompletedEventArgs>(client_Query_CheckIsAbleToCreateNewTransformFormCompleted);
        }
        

        //時做只有一個SD或HD播出帶
        void client_Query_CheckIsAbleToCreateNewTransformFormCompleted(object sender, Query_CheckIsAbleToCreateNewTransformFormCompletedEventArgs e)
        {
            if (e.Result == "True" && m_strFileNOEdit.Trim() == "")
            {
                //新增段落 先取VideoID
                client.QueryVideoID_PROG_STTAsync(m_strID, "0", m_strFileTypeID, false);
            }
            else
            {
                MessageBox.Show(e.Result);
                OKButton.IsEnabled = true;
                ContinueButton.IsEnabled = true;
                CancelButton.IsEnabled = true;
                BusyMsg.IsBusy = false;
            }
        }

        #region 實作

        //實作--不跑Flow新增送帶轉檔單 by Jarvis20130710
        void client_INSERT_BROADCAST_WITHOUT_FLOWCompleted(object sender, INSERT_BROADCAST_WITHOUT_FLOWCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {
                    MessageBox.Show("新增送帶轉檔資料成功！", "提示訊息", MessageBoxButton.OK);
                    //加入呼叫短帶托播單畫面
                    if (isOpenPGM110 == true)
                    {
                        PGM.PGM110 pagePGM110 = new PGM.PGM110(m_strID, tbxPROG_NAME.Text.Trim(), tbAllDurationS.Text.Trim());
                        pagePGM110.Show();
                        pagePGM110.Closed += (sss, aaargs) =>
                        { this.DialogResult = true; };


                    }

                    if (m_continue == false)        //若是按下確定，就要跳回主畫面，反之則可繼續新增
                        this.DialogResult = true;
                    else
                        iniContinue();
                }
                else
                {
                    MessageBox.Show("新增送帶轉檔資料失敗！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
        }

        //實作-取得送帶轉檔單編號
        void ClientMAM_GetNoRecordCompleted(object sender, GetNoRecordCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;

            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    tbxBro_ID.Text = e.Result;
                    m_strBroID = e.Result;

                    if (m_strPromoName.Trim() != "")
                    {
                        clientPromo.QUERY_TBPROMO_BYNAME_STTAsync(m_strPromoName);  //查詢短帶內容
                        BusyMsg.IsBusy = true;
                    }
                    else
                    {
                        client.GetTBBROADCAST_CODE_TBFILE_TYPEAsync();  //下載代碼檔_影片的檔案類型  
                        BusyMsg.IsBusy = true;
                    }
                }
                else
                {
                    MessageBox.Show("取得送帶轉檔單編號異常，請聯絡系統管理員！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
            else
            {
                MessageBox.Show("取得送帶轉檔單編號異常，請聯絡系統管理員！", "提示訊息", MessageBoxButton.OK);
                this.DialogResult = false;
            }
        }

        //實作-取得代碼檔_影片的檔案類型 
        void client_GetTBBROADCAST_CODE_TBFILE_TYPECompleted(object sender, GetTBBROADCAST_CODE_TBFILE_TYPECompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;

            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    this.cbVideo_TYPE.Items.Clear();

                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        Class_CODE_TBFILE_TYPE CodeData = new Class_CODE_TBFILE_TYPE();
                        CodeData = e.Result[i];

                        switch (CodeData.TABLENAME)
                        {
                            case "TBFILE_TYPE":      //塞入入庫檔案類型代碼，以及把音圖文的代碼濾掉

                                if (CodeData.FSTYPE == "影")
                                {
                                    ComboBoxItem cbiFileTYPE = new ComboBoxItem();
                                    cbiFileTYPE.Content = CodeData.NAME;
                                    cbiFileTYPE.Tag = CodeData.ID;
                                    this.cbVideo_TYPE.Items.Add(cbiFileTYPE);
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

        //實作-新增送帶轉檔單資料
        void client_INSERT_BROADCASTCompleted(object sender, INSERT_BROADCASTCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;

            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    newAFlow();
                }
                else
                {
                    MessageBox.Show("新增送帶轉檔資料失敗！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
        }

        //用短帶編號查詢48小時內是否有排播
        void client_GetPROMO_PLAY_NOTECompleted(object sender, GetPROMO_PLAY_NOTECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.FBSW == true)
                    MessageBox.Show("播出日期:" + e.Result.FDDATE + " 時間:" + e.Result.FSPLAYTIME + " 僅剩:" + e.Result.FSNOTE, "提示訊息", MessageBoxButton.OK);
            }
        }

        //實作-新增節目或短帶段落檔
        void client_INSERT_TBLOG_VIDEO_SEGCompleted(object sender, INSERT_TBLOG_VIDEO_SEGCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;

            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    //MessageBox.Show("新增段落資料成功！", "提示訊息", MessageBoxButton.OK);     
                    client.INSERT_LOG_VIDEOAsync(FormToClass_LogVideo());  //段落新增成功後，再去新增入庫影像檔 
                    BusyMsg.IsBusy = true;
                }
                else
                    MessageBox.Show("新增段落資料失敗！", "提示訊息", MessageBoxButton.OK);
            }
        }

        //實作-取得檔案編號
        void client_fnGetFileIDCompleted(object sender, fnGetFileIDCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;

            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    string strGet = e.Result;  //接收回來的參數，前者是檔案編號，後者是DIRID                  

                    char[] delimiterChars = { ';' };
                    string[] words = strGet.Split(delimiterChars);

                    if (words.Length == 2)
                    {
                        m_strFileNONew = words[0];
                        m_DIRID = words[1];
                    }
                    else if (words.Length == 1)
                    {
                        m_strFileNONew = words[0];
                        m_DIRID = "";
                    }
                    else
                    {
                        m_strFileNONew = "";
                        m_DIRID = "";
                    }

                    FormToClass_Seg();
                    client.INSERT_TBLOG_VIDEO_SEGAsync(ListSeg);  //新增段落
                    BusyMsg.IsBusy = true;
                }
                else
                    MessageBox.Show("取得檔案編號失敗！", "提示訊息", MessageBoxButton.OK);
            }
        }

        //實作-入庫影像檔
        void client_INSERT_LOG_VIDEOCompleted(object sender, INSERT_LOG_VIDEOCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;

            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    //新增短帶的送帶轉檔
                    if (STT100.GoFlow)
                    {
                        client.INSERT_BROADCASTAsync(FormToClass_Bro());
                    }
                    else
                    {
                        client.INSERT_BROADCAST_WITHOUT_FLOWAsync(FormToClass_Bro());

                    }
                    BusyMsg.IsBusy = true;
                }
                else
                    MessageBox.Show("新增入庫影像檔資料失敗！", "提示訊息", MessageBoxButton.OK);
            }
        }

        //實作-修改入庫影像段落檔
        void client_UPDATE_TBLOG_VIDEO_SEG_BYFILENOCompleted(object sender, UPDATE_TBLOG_VIDEO_SEG_BYFILENOCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;

            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    Update_TBLOG_VIDEO();   //修改入庫影像檔
                    BusyMsg.IsBusy = true;
                }
                else
                {
                    MessageBox.Show("修改送帶轉檔資料失敗！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
        }

        //實作透過檔案編號取得段落檔
        void client_GetTBLOG_VIDEO_SEG_BYFileIDCompleted(object sender, GetTBLOG_VIDEO_SEG_BYFileIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    ListSeg = e.Result;
                    tbxS1.Text = ListSeg[0].FSBEG_TIMECODE.Substring(0, 2);
                    tbxS2.Text = ListSeg[0].FSBEG_TIMECODE.Substring(3, 2);
                    tbxS3.Text = ListSeg[0].FSBEG_TIMECODE.Substring(6, 2);
                    tbxS4.Text = ListSeg[0].FSBEG_TIMECODE.Substring(9, 2);

                    tbxS5.Text = ListSeg[0].FSEND_TIMECODE.Substring(0, 2);
                    tbxS6.Text = ListSeg[0].FSEND_TIMECODE.Substring(3, 2);
                    tbxS7.Text = ListSeg[0].FSEND_TIMECODE.Substring(6, 2);
                    tbxS8.Text = ListSeg[0].FSEND_TIMECODE.Substring(9, 2);
                }
            }

            compareCode_TYPEID(m_strFileTypeID);    //顯示影帶類型
        }

        //實作-查詢入庫影像檔資料_透過檔案節目編號及集別
        void client_GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompleted(object sender, GetTBLOG_VIDEO_BYProgID_Episode_BROIDCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    m_strFileNOEdit = (e.Result[0]).FSFILE_NO.ToString();

                    tbxTITLE.Text = (e.Result[0]).FSTITLE.ToString();
                    tbxDESCRIPTION.Text = (e.Result[0]).FSDESCRIPTION.ToString();
                    m_strFileTypeID = (e.Result[0]).FSARC_TYPE.Trim();
                    m_VIDEOID_PROG = (e.Result[0]).FSVIDEO_PROG.Trim(); //VideoID

                    if ((e.Result[0]).FSSUPERVISOR.ToString().Trim() == "Y")
                        cbBooking.IsChecked = true;

                    //用檔案編號找到段落資料
                    client.GetTBLOG_VIDEO_SEG_BYFileIDAsync(m_strFileNOEdit);
                    BusyMsg.IsBusy = true;
                }
            }
        }

        //實作-修改入庫影像檔
        void client_UPDATE_TBLOG_VIDEO_TITLE_DES_ARCCompleted(object sender, UPDATE_TBLOG_VIDEO_TITLE_DES_ARCCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;

            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    //檢查列印編號是否有改變，若是有改變，要更新資料庫
                    if (m_strPrintID.Trim() == tbxPRINTID.Text.Trim())
                    {
                        MessageBox.Show("修改送帶轉檔資料成功！", "提示訊息", MessageBoxButton.OK);
                        this.DialogResult = true;
                    }
                    else
                    {
                        client.UPDATE_BROADCAST_PRINTIDAsync(m_strBroID, tbxPRINTID.Text.Trim(), UserClass.userData.FSUSER_ID.ToString());
                        BusyMsg.IsBusy = true;
                    }

                }
                else
                {
                    MessageBox.Show("修改送帶轉檔資料失敗！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
        }

        //實作-查詢宣傳帶資料檔_透過宣傳帶名稱(新增短帶資料完就要送帶轉擋)
        void clientPromo_QUERY_TBPROMO_BYNAME_STTCompleted(object sender, QUERY_TBPROMO_BYNAME_STTCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;

            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    Class_PROMO obj = new Class_PROMO();
                    obj = e.Result;
                    tbxPROG_NAME.Text = obj.FSPROMO_NAME.Trim();
                    tbxPROG_NAME.Tag = obj.FSPROMO_ID.Trim();
                    m_strCHANNELID = obj.FSMAIN_CHANNEL_ID.Trim();
                    m_strID = obj.FSPROMO_ID.Trim();
                }
            }

            client.GetTBBROADCAST_CODE_TBFILE_TYPEAsync();  //下載代碼檔_影片的檔案類型  
            BusyMsg.IsBusy = true;
        }

        //實作-修改送帶轉檔檔的列印編號
        void client_UPDATE_BROADCAST_PRINTIDCompleted(object sender, UPDATE_BROADCAST_PRINTIDCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;

            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    MessageBox.Show("修改送帶轉檔資料成功！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("修改送帶轉檔資料失敗！", "提示訊息", MessageBoxButton.OK);
                    this.DialogResult = false;
                }
            }
        }

        //實作-查詢TBLOG_VIDEOID_MAP的VideoID
        void client_QueryVideoID_PROG_STTCompleted(object sender, QueryVideoID_PROG_STTCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;

            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    m_VIDEOID_PROG = e.Result.Trim();

                    //新增-取得檔案編號(FSFILE_NO)並且上傳段落檔 
                    client.fnGetFileIDAsync("P", m_strID, "0", UserClass.userData.FSUSER_ID.ToString(), m_strCHANNELID, tbxPROG_NAME.Text.Trim(), tbxPROG_NAME.Text.Trim());
                    BusyMsg.IsBusy = true;
                }
                else
                    MessageBox.Show("取得VideoID失敗！", "提示訊息", MessageBoxButton.OK);
            }
        }

        //實作--流程起始
        void clientFlow_CallFlow_NewFlowWithFieldCompleted(object sender, CallFlow_NewFlowWithFieldCompletedEventArgs e)
        {
            if (e.Error == null && e.Result != "")
            {
                MessageBox.Show("新增送帶轉檔資料成功！", "提示訊息", MessageBoxButton.OK);

                //加入呼叫短帶托播單畫面
                if (isOpenPGM110 == true)
                {
                    PGM.PGM110 pagePGM110 = new PGM.PGM110(m_strID, tbxPROG_NAME.Text.Trim(), tbAllDurationS.Text.Trim());
                    pagePGM110.Show();
                    pagePGM110.Closed += (sss, aaargs) =>
                        { this.DialogResult = true; };


                }

                if (m_continue == false)        //若是按下確定，就要跳回主畫面，反之則可繼續新增
                    this.DialogResult = true;
                else
                    iniContinue();
            }
            else
            {
                MessageBox.Show("呼叫流程引擎異常，新增送帶轉檔資料失敗！", "提示訊息", MessageBoxButton.OK);
                this.DialogResult = true;  //雖然流程失敗，但是因為已寫入還是要畫面重新Load 
            }
        }

        #endregion

        #region 檢查畫面資料及搬值

        //比對代碼檔_製作目的
        void compareCode_TYPEID(string strID)
        {
            for (int i = 0; i < cbVideo_TYPE.Items.Count; i++)
            {
                ComboBoxItem getcbi = (ComboBoxItem)cbVideo_TYPE.Items.ElementAt(i);
                if (getcbi.Tag.ToString().Trim().Equals(strID.Trim()))
                {
                    cbVideo_TYPE.SelectedIndex = i;
                    break;
                }
            }

            BusyMsg.IsBusy = false;
        }

        private Class_BROADCAST FormToClass_Bro()
        {
            Class_BROADCAST obj = new Class_BROADCAST();

            obj.FSBRO_ID = m_strBroID.Trim();                                      //送帶轉檔單號
            obj.FSBRO_TYPE = "P";                                                   //宣傳帶類型為P
            obj.FSID = m_strID.Trim();                                              //編碼
            obj.FNEPISODE = 0;                                                      //宣傳帶的集別為0            
            obj.FDBRO_DATE = DateTime.Now;                                          //送帶轉檔申請日期
            obj.FSBRO_BY = UserClass.userData.FSUSER_ID.ToString();                 //送帶轉檔申請者
            if (STT100.GoFlow)
            {
                obj.FCCHECK_STATUS = "N";//審核狀態
            }
            else
            {
                obj.FCCHECK_STATUS = "G";
                obj.FSSIGNED_BY = UserClass.userData.FSUSER_ID;

            }
            obj.FSCHECK_BY = "";                                                    //審核者
            obj.FCCHANGE = "N";                                                     //置換
            obj.FSPRINTID = tbxPRINTID.Text.Trim();                                 //列印序號
            obj.FSMEMO = tbMEMO.Text.Trim();                                        //MEMO欄位2012/08/09 kyle新增


            obj.FSCREATED_BY = UserClass.userData.FSUSER_ID.ToString();//建檔者
            obj.FSUPDATED_BY = UserClass.userData.FSUSER_ID.ToString();//修改者                     

            return obj;
        }

        //新增段落資料
        private void FormToClass_Seg()
        {
            Class_LOG_VIDEO_SEG ClassSeg = new Class_LOG_VIDEO_SEG();            //段落

            if (m_strFileNOEdit.Trim() == "")
                ClassSeg.FSFILE_NO = m_strFileNONew.Trim();
            else
                ClassSeg.FSFILE_NO = m_strFileNOEdit.Trim();

            ClassSeg.FNSEG_ID = "01";                                            //段落只有一段
            ClassSeg.FSVIDEO_ID = m_VIDEOID_PROG;                                //VideoID
            ClassSeg.FSBEG_TIMECODE = tbxS1.Text.ToString().Trim() + ":" + tbxS2.Text.ToString().Trim() + ":" + tbxS3.Text.ToString().Trim() + ";" + tbxS4.Text.ToString().Trim();
            ClassSeg.FSEND_TIMECODE = tbxS5.Text.ToString().Trim() + ":" + tbxS6.Text.ToString().Trim() + ":" + tbxS7.Text.ToString().Trim() + ";" + tbxS8.Text.ToString().Trim();
            ClassSeg.FSSUB_TIMECODE = TransferTimecode.Subtract_timecode(ClassSeg.FSBEG_TIMECODE, ClassSeg.FSEND_TIMECODE);
            ClassSeg.FSCREATED_BY = UserClass.userData.FSUSER_ID.ToString();     //建檔者  
            ClassSeg.FSUPDATED_BY = UserClass.userData.FSUSER_ID.ToString();     //建檔者

            ListSeg.Add(ClassSeg);
        }

        //新增入庫影像資料
        private Class_LOG_VIDEO FormToClass_LogVideo()
        {
            Class_LOG_VIDEO obj = new Class_LOG_VIDEO();


            obj.FSFILE_NO = m_strFileNONew;
            obj.FSSUBJECT_ID = m_strFileNONew.Trim().Substring(0, 12);

            obj.FSTYPE = "P";               //宣傳帶類型為P
            obj.FSID = m_strID;
            obj.FNEPISODE = 0;              //宣傳帶的集別為0
            obj.FSPLAY = "N";
            obj.FSARC_TYPE = m_strFileTypeID;
            obj.FSFILE_TYPE_HV = "mxf";     //高解檔名
            if (this.cbVideo_TYPE.SelectionBoxItem.ToString().Contains("SD"))
                obj.FSFILE_TYPE_LV = "wmv";        //低解檔名
            else if (this.cbVideo_TYPE.SelectionBoxItem.ToString().Contains("HD"))
                obj.FSFILE_TYPE_LV = "mp4";
           // obj.FSFILE_TYPE_LV = "wmv";     //低解檔名
            obj.FSTITLE = tbxTITLE.Text.Trim();
            obj.FSDESCRIPTION = tbxDESCRIPTION.Text.Trim();
            obj.FSFILE_SIZE = "";
            obj.FCFILE_STATUS = "B";        //使用者送轉檔申請後：B
            obj.FSVIDEO_PROG = m_VIDEOID_PROG; //VideoID

            obj.FSCHANGE_FILE_NO = "";      //沒有置換所以置換檔案為空
            obj.FSOLD_FILE_NAME = "";
            obj.FSFILE_PATH_H = "";         //高低解檔案路徑在web services裡取得
            obj.FSFILE_PATH_L = "";

            if (cbBooking.IsChecked == true)  //調用通知入庫單位主管
                obj.FSSUPERVISOR = "Y";
            else
                obj.FSSUPERVISOR = "N";

            if (m_DIRID != "")
                obj.FNDIR_ID = Convert.ToUInt32(m_DIRID);
            else
                obj.FNDIR_ID = 0;

            obj.FSCHANNEL_ID = m_strCHANNELID;//頻道別

            if (ListSeg.Count > 0)
            {
                obj.FSBEG_TIMECODE = ListSeg[0].FSBEG_TIMECODE.ToString().Trim();
                obj.FSEND_TIMECODE = ListSeg[ListSeg.Count - 1].FSEND_TIMECODE.ToString().Trim();
            }
            else
            {
                obj.FSBEG_TIMECODE = "";
                obj.FSEND_TIMECODE = "";
            }

            obj.FSBRO_ID = m_strBroID;
            obj.FSARC_ID = "";

            obj.FSCREATED_BY = UserClass.userData.FSUSER_ID.ToString();           //建檔者
            obj.FSUPDATED_BY = UserClass.userData.FSUSER_ID.ToString();           //修改者

            return obj;
        }
        //新增入庫影像資料
        private Class_LOG_VIDEO FormToClass_LogVideoForCheck()
        {
            Class_LOG_VIDEO obj = new Class_LOG_VIDEO();

            obj.FSTYPE = "P";               //宣傳帶類型為P
            obj.FSID = m_strID;
            obj.FNEPISODE = 0;              //宣傳帶的集別為0
            obj.FSPLAY = "N";
            obj.FSARC_TYPE = m_strFileTypeID;
            obj.FSFILE_TYPE_HV = "mxf";     //高解檔名

            if (this.cbVideo_TYPE.SelectionBoxItem.ToString().Contains("SD"))
                obj.FSFILE_TYPE_LV = "wmv";        //低解檔名
            else if (this.cbVideo_TYPE.SelectionBoxItem.ToString().Contains("HD"))
                obj.FSFILE_TYPE_LV = "mp4";
            //obj.FSFILE_TYPE_LV = "wmv";     //低解檔名
            obj.FSTITLE = tbxTITLE.Text.Trim();
            obj.FSDESCRIPTION = tbxDESCRIPTION.Text.Trim();
            obj.FSFILE_SIZE = "";
            obj.FCFILE_STATUS = "B";        //使用者送轉檔申請後：B
            obj.FSVIDEO_PROG = m_VIDEOID_PROG; //VideoID

            obj.FSCHANGE_FILE_NO = "";      //沒有置換所以置換檔案為空
            obj.FSOLD_FILE_NAME = "";
            obj.FSFILE_PATH_H = "";         //高低解檔案路徑在web services裡取得
            obj.FSFILE_PATH_L = "";

            if (cbBooking.IsChecked == true)  //調用通知入庫單位主管
                obj.FSSUPERVISOR = "Y";
            else
                obj.FSSUPERVISOR = "N";

            if (m_DIRID != "")
                obj.FNDIR_ID = Convert.ToUInt32(m_DIRID);
            else
                obj.FNDIR_ID = 0;

            obj.FSCHANNEL_ID = m_strCHANNELID;//頻道別

            if (ListSeg.Count > 0)
            {
                obj.FSBEG_TIMECODE = ListSeg[0].FSBEG_TIMECODE.ToString().Trim();
                obj.FSEND_TIMECODE = ListSeg[ListSeg.Count - 1].FSEND_TIMECODE.ToString().Trim();
            }
            else
            {
                obj.FSBEG_TIMECODE = "";
                obj.FSEND_TIMECODE = "";
            }

            obj.FSBRO_ID = m_strBroID;
            obj.FSARC_ID = "";

            obj.FSCREATED_BY = UserClass.userData.FSUSER_ID.ToString();           //建檔者
            obj.FSUPDATED_BY = UserClass.userData.FSUSER_ID.ToString();           //修改者

            return obj;
        }

        //修改送帶轉檔資料時，要把class帶到畫面上
        private void classToForm(Class_BROADCAST FormData)
        {
            tbxBro_ID.Text = FormData.FSBRO_ID.Trim();
            tbxPROG_NAME.Text = FormData.FSID_NAME.Trim();
            tbxPROG_NAME.Tag = FormData.FSID.Trim();
            tbMEMO.Text = FormData.FSMEMO.Trim();//將MEMO擺上去
            m_strBroID = FormData.FSBRO_ID.Trim();
            m_strID = FormData.FSID.Trim();

            if (FormData.FSPRINTID.Trim() == "")
            {
                btnPRINTID.Visibility = Visibility.Collapsed;
                tbxPRINTID.IsReadOnly = false;
            }
            else
            {
                tbxPRINTID.Text = FormData.FSPRINTID.Trim();
                m_strPrintID = FormData.FSPRINTID.Trim();
            }

            client.GetTBLOG_VIDEO_BYProgID_Episode_BROIDAsync("P", m_strID, "0", m_strBroID);
            BusyMsg.IsBusy = true;
        }

        //修改入庫影像資料
        private void Update_TBLOG_VIDEO()
        {
            string strSUPERVISOR = "N";
            string strFileTypeID = "";

            if (cbBooking.IsChecked == true)  //調用通知入庫單位主管
                strSUPERVISOR = "Y";

            strFileTypeID = ((ComboBoxItem)cbVideo_TYPE.SelectedItem).Tag.ToString().Trim();

            client.UPDATE_TBLOG_VIDEO_TITLE_DES_ARCAsync(m_strFileNOEdit, tbxTITLE.Text.Trim(), tbxDESCRIPTION.Text.Trim(), m_strBeg, m_strEnd, strSUPERVISOR, strFileTypeID, UserClass.userData.FSUSER_ID.ToString().Trim());
        }

        //取得列印序號
        private void GetPrintID()
        {
            tbxPRINTID.Text = System.Guid.NewGuid().ToString().Trim();
        }

        #endregion

        #region 按鈕裡的事件
        //按下繼續進入托播單後
        private void btnOpenPGM110_Click(object sender, RoutedEventArgs e)
        {
            isOpenPGM110 = true;
            m_continue = false;


            if (OkButton() == false)
                return;

            //避免使用者連續按，要鎖住
            OKButton.IsEnabled = false;
            ContinueButton.IsEnabled = false;
            CancelButton.IsEnabled = false;
            btnOpenPGM110.IsEnabled = false;

        }

        //按下確定後
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            m_continue = false;
            

            if (OkButton() == false)
                return;

            //避免使用者連續按，要鎖住
            OKButton.IsEnabled = false;
            ContinueButton.IsEnabled = false;
            CancelButton.IsEnabled = false;
            btnOpenPGM110.IsEnabled = false;
        }

        //繼續新增
        private void ContinueButton_Click(object sender, RoutedEventArgs e)
        {
            m_continue = true;

            if (OkButton() == false)
                return;

            //避免使用者連續按，要鎖住
            OKButton.IsEnabled = false;
            ContinueButton.IsEnabled = false;
            CancelButton.IsEnabled = false;
            btnOpenPGM110.IsEnabled = false;
        }

        //新增
        private Boolean OkButton()
        {
            if (m_strID == "")
            {
                MessageBox.Show("請先選擇短帶名稱", "提示訊息", MessageBoxButton.OK);
                return false;
            }

            if (cbVideo_TYPE.SelectedIndex != -1)                     //影片類型
            {
                m_strFileTypeID = ((ComboBoxItem)cbVideo_TYPE.SelectedItem).Tag.ToString();
            }
            else
            {
                MessageBox.Show("請先選擇影片類型", "提示訊息", MessageBoxButton.OK);
                return false;
            }

            if (check_TimeCode() == false)      //檢查段落格式是否正確
                return false;

            //檢查短帶資料是否還存在(把原code全部塞進QUERY_TBPGM_PROMOCompleted的事件中) by Jarvis 20141107
            WSPROMO.WSPROMOSoapClient promoClient = new WSPROMO.WSPROMOSoapClient();
            promoClient.QUERY_TBPGM_PROMOCompleted += (sen, para) =>
            {
                if (para.Error == null)
                {
                    if (para.Result.Count > 0)
                    {
                        STT100_01_03 _stt100_01_03 = new STT100_01_03();
                        _stt100_01_03.Show();
                        _stt100_01_03.Closed += (s, args) =>
                        {

                            this.tbMEMO.Text = _stt100_01_03._fsTYPE + "\r\n";

                            if (!string.IsNullOrEmpty(_stt100_01_03._fsFILE_PATH) && !string.IsNullOrEmpty(_stt100_01_03._fsTREE_PATH))
                            {
                                this.tbMEMO.Text += "樹狀路徑：" + _stt100_01_03._fsTREE_PATH + "\r\n";
                                this.tbMEMO.Text += "實體路徑：" + _stt100_01_03._fsFILE_PATH;
                            }

                            //判斷SD播出帶 HD播出帶 的送帶轉檔單是否可新增 by Kyle 2012/07/27
                            List<Class_LOG_VIDEO> ListLogVideo = new List<Class_LOG_VIDEO>();
                            ListLogVideo.Add(FormToClass_LogVideoForCheck());
                            if ((ListLogVideo.Where(A1 => A1.FSARC_TYPE == "001").Count() > 0 || ListLogVideo.Where(A2 => A2.FSARC_TYPE == "002").Count() > 0) && m_strFileNOEdit.Trim() == "")
                            {

                                client.Query_CheckIsAbleToCreateNewTransformFormAsync(m_strID.Trim(), "0", ListLogVideo);
                            }
                            else
                            {
                                //接下來的步驟：1.取得VideoID、2.取得FileNO、3.新增段落檔、4.新增入庫影像檔、5.短帶送帶轉檔
                                if (m_strFileNOEdit.Trim() == "")   //檢查是新增或是修改
                                {
                                    //先取VideoID
                                    client.QueryVideoID_PROG_STTAsync(m_strID, "0", m_strFileTypeID, false);
                                }
                                else
                                {
                                    //修改段落
                                    client.UPDATE_TBLOG_VIDEO_SEG_BYFILENOAsync(m_strFileNOEdit, "01", m_strBeg, m_strEnd, UserClass.userData.FSUSER_ID.ToString());
                                    //按下確認後更新MEMO欄位 獨立丟出去的MEMO欄位 本確定並沒有回寫資料 Kyle 20120803新增
                                    if (tbxBro_ID.Text.Trim() != "")
                                        client.UPDATE_BROADCAST_FSMEMOAsync(tbxBro_ID.Text.Trim(), tbMEMO.Text.Trim(), UserClass.userData.FSUSER_ID);
                                }
                            }

                            //志軒原本的CODE
                            ////接下來的步驟：1.取得VideoID、2.取得FileNO、3.新增段落檔、4.新增入庫影像檔、5.短帶送帶轉檔
                            //if (m_strFileNOEdit.Trim() == "")   //檢查是新增或是修改
                            //{
                            //    //先取VideoID
                            //    client.QueryVideoID_PROG_STTAsync(m_strID, "0", m_strFileTypeID, false);
                            //}
                            //else
                            //{
                            //    //修改段落
                            //    client.UPDATE_TBLOG_VIDEO_SEG_BYFILENOAsync(m_strFileNOEdit, "01", m_strBeg, m_strEnd, UserClass.userData.FSUSER_ID.ToString());
                            //}

                            BusyMsg.IsBusy = true;
                            ////判斷SD播出帶 HD播出帶 的送帶轉檔單是否可新增 by Kyle 2012/07/27
                            //if (ListLogVideo.Where(A1 => A1.FSARC_TYPE == "001").Count() > 0 || ListLogVideo.Where(A2 => A2.FSARC_TYPE == "002").Count() > 0)
                            //{
                            //    client.Query_CheckIsAbleToCreateNewTransformFormAsync(m_strID.Trim(), m_strEpisode.Trim(), ListLogVideo);
                            //}
                            //else
                            //{
                            //    //至軒原本的CODE
                            //    if (m_strID.Trim() != "" && m_strEpisode.Trim() != "0")
                            //    {
                            //        client.Query_TBPROG_D_MONITERAsync(m_strID.Trim(), m_strEpisode.Trim());        //檢查是否有填Moniter資料，沒有填要提醒
                            //        //client.Query_TBPROG_INSERT_TAPEAsync(m_strID.Trim(), m_strEpisode.Trim());  //查詢節目子集檔的主控播出提示資料，改成檢查上面的Moniter資料
                            //    }
                            //    else
                            //        UpdateND_DURATION();    //若是非節目加集別，像是以節目或是短帶送帶轉檔的就直接新增
                            //}

                        };
           
                    }
                    else
                    {
                        MessageBox.Show("短帶資料已刪除，無法新增送帶轉檔。");
                        OKButton.IsEnabled = true;
                        ContinueButton.IsEnabled = true;
                        CancelButton.IsEnabled = true;
                        btnOpenPGM110.IsEnabled = true;
                        BusyMsg.IsBusy = false;

                        return;
                    }
                }
            };
            promoClient.QUERY_TBPGM_PROMOAsync(new PTS_MAM3.WSPROMO.Class_PROMO() { FSPROMO_ID = m_strID.Trim(), FSDEL = "N" });

           

            return true;
        }

        //移除列印關係
        private void btnPRINTID_Click(object sender, RoutedEventArgs e)
        {
            tbxPRINTID.Text = "";
        }

        //繼續新增的初始化
        private void iniContinue()
        {
            ListSeg.Clear();            //段落資料清除

            m_strBroID = "";            //送帶轉檔單單號
            m_strID = "";               //短帶代碼
            m_strBeg = "";              //段落起
            m_strEnd = "";              //段落迄
            m_strFileTypeID = "";       //入庫檔案類型編號
            m_strCHANNELID = "";        //頻道別
            m_DIRID = "";               //串結點用的
            m_strFileNONew = "";        //檔案編號(新增時用的) 
            m_strPromoName = "";        //短帶名稱

            tbxPROG_NAME.Text = "";     //短帶名稱
            tbxPROG_NAME.Tag = "";      //短帶編號
            tbxTITLE.Text = "";         //標題
            tbxDESCRIPTION.Text = "";   //描述
            cbBooking.IsChecked = false;//調用通知
            tbxS1.Text = "";
            tbxS2.Text = "";
            tbxS3.Text = "";
            tbxS4.Text = "";
            tbxS5.Text = "";
            tbxS6.Text = "";
            tbxS7.Text = "";
            tbxS8.Text = "";
            m_VIDEOID_PROG = "";        //VideoID

            //按鈕解開
            OKButton.IsEnabled = true;
            ContinueButton.IsEnabled = true;
            CancelButton.IsEnabled = true;
            btnOpenPGM110.IsEnabled = true;

            client.GetTBBROADCAST_CODE_TBFILE_TYPEAsync();
            clientMAM.GetNoRecordAsync("07", "", "", UserClass.userData.FSUSER_ID); //取得送帶轉檔單編號            
        }

        //檢查段落格式是否正確
        private Boolean check_TimeCode()
        {
            string strBEGError = "";
            string strENDError = "";
            string strSUB = "";
            StringBuilder strMessage = new StringBuilder();
            int intCheck;       //純粹為了判斷是否為數字型態之用
            string strNewEnd = "";

            if (int.TryParse(tbxS1.Text.Trim(), out intCheck) == false || tbxS1.Text.Trim().Length != 2)
                strMessage.AppendLine("請檢查「Time Code 起」第一個欄位必須為數值型態且為兩碼");

            if (int.TryParse(tbxS2.Text.Trim(), out intCheck) == false || tbxS2.Text.Trim().Length != 2)
                strMessage.AppendLine("請檢查「Time Code 起」第二個欄位必須為數值型態且為兩碼");

            if (int.TryParse(tbxS3.Text.Trim(), out intCheck) == false || tbxS3.Text.Trim().Length != 2)
                strMessage.AppendLine("請檢查「Time Code 起」第三個欄位必須為數值型態且為兩碼");

            if (int.TryParse(tbxS4.Text.Trim(), out intCheck) == false || tbxS4.Text.Trim().Length != 2)
                strMessage.AppendLine("請檢查「Time Code 起」第四個欄位必須為數值型態且為兩碼");

            if (int.TryParse(tbxS5.Text.Trim(), out intCheck) == false || tbxS5.Text.Trim().Length != 2)
                strMessage.AppendLine("請檢查「Time Code 迄」第一個欄位必須為數值型態且為兩碼");

            if (int.TryParse(tbxS6.Text.Trim(), out intCheck) == false || tbxS6.Text.Trim().Length != 2)
                strMessage.AppendLine("請檢查「Time Code 迄」第二個欄位必須為數值型態且為兩碼");

            if (int.TryParse(tbxS7.Text.Trim(), out intCheck) == false || tbxS7.Text.Trim().Length != 2)
                strMessage.AppendLine("請檢查「Time Code 迄」第三個欄位必須為數值型態且為兩碼");

            if (int.TryParse(tbxS8.Text.Trim(), out intCheck) == false || tbxS8.Text.Trim().Length != 2)
                strMessage.AppendLine("請檢查「Time Code 迄」第四個欄位必須為數值型態且為兩碼");

            m_strBeg = tbxS1.Text.Trim() + ":" + tbxS2.Text.Trim() + ":" + tbxS3.Text.Trim() + ";" + tbxS4.Text.Trim();
            m_strEnd = tbxS5.Text.Trim() + ":" + tbxS6.Text.Trim() + ":" + tbxS7.Text.Trim() + ";" + tbxS8.Text.Trim();

            if (strMessage.ToString().Trim() == "")
            {
                //修改 by Jarvis 20140903
                if (!TransferTimecode.check_timecode_format(tbxS1.Text.Trim(), tbxS2.Text.Trim(), tbxS3.Text.Trim(), tbxS4.Text.Trim(), out strBEGError))
                {
                    if (strBEGError == "所輸入的時間不符合DropFrame的格式")
                    {
                        tbxS4.Text = "02";
                        int frames = TransferTimecode.timecodetoframe(tbxS1.Text.Trim() + ":" + tbxS2.Text.Trim() + ":" + tbxS3.Text.Trim() + ";" + tbxS4.Text.Trim());
                        string newFame = TransferTimecode.frame2timecode(frames);
                        MessageBoxResult result = MessageBox.Show("Time Code 起自動改成：「" + newFame + "」", "提示訊息", MessageBoxButton.OK);

                        tbxS1.Text = newFame.Substring(0, 2);
                        tbxS2.Text = newFame.Substring(3, 2);
                        tbxS3.Text = newFame.Substring(6, 2);
                        tbxS4.Text = newFame.Substring(9, 2);
                    }
                    else
                    {
                        MessageBox.Show(strBEGError);
                        return false;
                    }
                }

                if (TransferTimecode.check_timecode_format(tbxS5.Text.Trim(), tbxS6.Text.Trim(), tbxS7.Text.Trim(), tbxS8.Text.Trim(), out strENDError))
                {
                    if (strENDError.Trim() != "")
                    {
                        if (strENDError == "所輸入的時間不符合DropFrame的格式")
                        {
                            tbxS8.Text = "02";
                            int frames = TransferTimecode.timecodetoframe(tbxS5.Text.Trim() + ":" + tbxS6.Text.Trim() + ":" + tbxS7.Text.Trim() + ";" + tbxS8.Text.Trim());
                            string newFame = TransferTimecode.frame2timecode(frames);
                            MessageBoxResult result = MessageBox.Show("Time Code 迄自動改成：「" + newFame + "」", "提示訊息", MessageBoxButton.OK);


                            tbxS5.Text = newFame.Substring(0, 2);
                            tbxS6.Text = newFame.Substring(3, 2);
                            tbxS7.Text = newFame.Substring(6, 2);
                            tbxS8.Text = newFame.Substring(9, 2);
                        }
                        else
                        {
                            MessageBox.Show(strENDError);
                            return false;
                        }
                    }
                }

                //Ori
                //TransferTimecode.check_timecode_format(tbxS1.Text.Trim(), tbxS2.Text.Trim(), tbxS3.Text.Trim(), tbxS4.Text.Trim(), out strBEGError);
                //TransferTimecode.check_timecode_format(tbxS5.Text.Trim(), tbxS6.Text.Trim(), tbxS7.Text.Trim(), tbxS8.Text.Trim(), out strENDError);

                //if (strBEGError.Trim() != "")
                //{
                //    //若是不符合規定時，起始的Timecode，最後一個變成02
                //    tbxS4.Text = "02";
                //    MessageBoxResult result = MessageBox.Show("Time Code 起自動改成：「" + tbxS1.Text.Trim() + ":" + tbxS2.Text.Trim() + ":" + tbxS3.Text.Trim() + ";" + tbxS4.Text.Trim() + "」", "提示訊息", MessageBoxButton.OK);

                //    //原本輸入錯誤的Timecode資料會被擋掉
                //    //MessageBoxResult result = MessageBox.Show("Time Code 起：" + strBEGError, "提示訊息", MessageBoxButton.OK);
                //    //return false;
                //}

                //if (strENDError.Trim() != "")
                //{
                //    strNewEnd = checkEndTimecode(tbxS5.Text.Trim() + ":" + tbxS6.Text.Trim() + ":" + tbxS7.Text.Trim() + ";" + tbxS8.Text.Trim());
                //    tbxS5.Text = strNewEnd.Substring(0, 2);
                //    tbxS6.Text = strNewEnd.Substring(3, 2);
                //    tbxS7.Text = strNewEnd.Substring(6, 2);
                //    tbxS8.Text = strNewEnd.Substring(9, 2);
                //    MessageBoxResult result = MessageBox.Show("Time Code 迄自動改成：「" + tbxS5.Text.Trim() + ":" + tbxS6.Text.Trim() + ":" + tbxS7.Text.Trim() + ";" + tbxS8.Text.Trim() + "」", "提示訊息", MessageBoxButton.OK);

                //    //原本輸入錯誤的Timecode資料會被擋掉
                //    //MessageBoxResult result = MessageBox.Show("Time Code 迄：" + strENDError, "提示訊息", MessageBoxButton.OK);
                //    //return false;
                //}

                strSUB = TransferTimecode.Subtract_timecode(m_strBeg, m_strEnd);

                if (strSUB.StartsWith("E"))
                {
                    MessageBoxResult result = MessageBox.Show("Time Code (迄) 必須大於Time Code (起)", "提示訊息", MessageBoxButton.OK);
                    return false;
                }
                else
                {
                    return true;
                }
            }

            if (strMessage.ToString().Trim() == "")
                return true;
            else
            {
                MessageBoxResult result = MessageBox.Show(strMessage.ToString(), "提示訊息", MessageBoxButton.OK);
                return false;
            }
        }

        //將不存在的結束Timecode改成存在的
        private string checkEndTimecode(string strEnd)
        {
            string strReturn = TransferTimecode.frame2timecode(TransferTimecode.timecodetoframe(strEnd));

            if (strReturn.EndsWith("28") == true)
                strReturn = strReturn.Substring(0, 9) + "29";

            return strReturn;
        }

        //取消時，要去檢查資料表後刪除
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //開啟節目資料查詢畫面
        private void btnPROG_Click(object sender, RoutedEventArgs e)
        {
            //PRG800_07
            PRG800_07 PROMO_VIEW_frm = new PRG800_07();
            PROMO_VIEW_frm.Show();

            PROMO_VIEW_frm.Closed += (s, args) =>
            {
                if (PROMO_VIEW_frm.DialogResult == true)
                {
                    //檢查是否要作資料認定
                    if (PROMO_VIEW_frm.strChannel_Id.Trim() == "" || PROMO_VIEW_frm.strDEP_ID.Trim() == "0")
                    {
                        //若是有作資料認定就回傳True，就可以繼續往下作
                        checkProgM_Channel_Dep(PROMO_VIEW_frm.strPromoID_View.Trim(), PROMO_VIEW_frm.strPromoName_View.Trim(), PROMO_VIEW_frm.strChannel_Id.Trim(), PROMO_VIEW_frm.strDEP_ID.Trim());
                        return;
                    }

                    //載入短帶資料
                    checkPromo(PROMO_VIEW_frm.strPromoID_View.Trim(), PROMO_VIEW_frm.strPromoName_View.Trim(), PROMO_VIEW_frm.strChannel_Id.Trim(), PROMO_VIEW_frm.strDEP_ID.Trim());
                }
            };
        }

        //檢查是否要作短帶資料認定
        private void checkProgM_Channel_Dep(string strProgID, string strProgName, string strChannelID, string strDepID)
        {
            string strCheck = "";   //判斷基本資料缺少訊息 

            //無頻道別要檔掉     
            if (strChannelID.Trim() == "")
            {
                strCheck = "無頻道別資料";
            }

            //無成案單位要檔掉     
            if (strDepID.Trim() == "0")
            {
                if (strCheck.Trim() == "")
                    strCheck = "無成案單位資料";
                else
                    strCheck = strCheck + "、無成案單位資料";
            }

            if (strCheck.Trim() != "")
            {
                MessageBox.Show(strCheck + "，請先進行節目資料認定", "提示訊息", MessageBoxButton.OK);

                //判斷有無修改「短帶資料認定」的權限     
                if (ModuleClass.getModulePermissionByName("01", "節目資料認定") == true)
                {
                    MessageBoxResult resultMsg = MessageBox.Show("確定要修改「" + strProgName.Trim() + "」的頻道別資料嗎?", "提示訊息", MessageBoxButton.OKCancel);
                    if (resultMsg == MessageBoxResult.OK)
                    {
                        PRG100_13 PRG100_13_frm = new PRG100_13("P", strProgID, strProgName, strChannelID, strDepID);
                        PRG100_13_frm.Show();

                        PRG100_13_frm.Closing += (s, args) =>
                        {
                            //如果資料補齊，即可載入短帶資料
                            if (PRG100_13_frm.bolcheck == true)
                                checkPromo(strProgID, strProgName, strChannelID, strDepID);
                        };
                    }
                }
            }
        }

        //載入短帶資料
        private void checkPromo(string strPromoID, string strPromoName, string strChannelID, string strDepID)
        {
            tbxPROG_NAME.Text = strPromoName;
            tbxPROG_NAME.Tag = strPromoID;
            m_strCHANNELID = strChannelID;
            m_strID = strPromoID;
            m_strDep_ID = strDepID; ;

            //找48小時有無排播記錄
            client.GetPROMO_PLAY_NOTEAsync(m_strID);
        }

        #endregion

        #region 流程引擎

        //以下程式將Flow放到Flow引擎中
        private void newAFlow()
        {
            string FlowNote = "";

            FlowNote = "短帶名稱-" + tbxPROG_NAME.Text.ToString().Trim();
            StringBuilder sb = new StringBuilder();
            sb.Append(@"<VariableCollection>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>FormId</name>");
            sb.Append(@"    <value>" + tbxBro_ID.Text + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>NextXamlName</name>");
            sb.Append(@"    <value>/STT/STT100_05.xaml</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>Function01</name>");
            sb.Append(@"    <value>0</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>hid_SendTo</name>");      //預設是給1，不用審核的單子最後顯示才會是通過
            sb.Append(@"    <value>1</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>parameter01</name>");     //預設通過的狀態-Y(提出者是審核人)
            sb.Append(@"    <value>Y</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>USERID</name>");
            sb.Append(@"    <value>" + UserClass.userData.FSUSER_ID.ToString().Trim() + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>FormInfo</name>");
            sb.Append(@"    <value>" + FlowNote + "</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"  <variable>");
            sb.Append(@"    <name>CancelForm</name>");
            sb.Append(@"    <value>CanCancel</value>");
            sb.Append(@"  </variable>");
            sb.Append(@"</VariableCollection>");

            //後端呼叫流程引擎
            clientFlow.CallFlow_NewFlowWithFieldAsync(6, UserClass.userData.FSUSER_ID.ToString().Trim(), sb.ToString());

            //flowWebService.FlowSoapClient FlowClinet = new flowWebService.FlowSoapClient();
            //FlowClinet.NewFlowWithFieldCompleted += new EventHandler<flowWebService.NewFlowWithFieldCompletedEventArgs>(FlowClinet_NewFlowWithFieldCompleted);
            //FlowClinet.NewFlowWithFieldAsync(6, UserClass.userData.FSUSER_ID.ToString().Trim(), sb.ToString());
        }

        //原本的前端呼叫流程引擎
        //void FlowClinet_NewFlowWithFieldCompleted(object sender, flowWebService.NewFlowWithFieldCompletedEventArgs e)
        //{
        //    //MessageBox.Show("表單已送出", "提示訊息", MessageBoxButton.OK);     //不顯示流程的表單送出訊息
        //    //this.DialogResult = true;
        //}

        #endregion

        #region 欄位順序

        //輸入完自動跳下一個
        private void tbxS1_TextChanged(object sender, TextChangedEventArgs e)
        {
            checkTime();

            if (tbxS1.Text.Trim().Length == 2)
                tbxS2.Focus();
        }

        private void tbxS2_TextChanged(object sender, TextChangedEventArgs e)
        {
            checkTime();

            if (tbxS2.Text.Trim().Length == 2)
                tbxS3.Focus();
        }

        private void tbxS3_TextChanged(object sender, TextChangedEventArgs e)
        {
            checkTime();

            if (tbxS3.Text.Trim().Length == 2)
                tbxS4.Focus();
        }

        private void tbxS4_TextChanged(object sender, TextChangedEventArgs e)
        {
            checkTime();

            if (tbxS4.Text.Trim().Length == 2)
                tbxS5.Focus();
        }

        private void tbxS5_TextChanged(object sender, TextChangedEventArgs e)
        {
            checkTime();

            if (tbxS5.Text.Trim().Length == 2)
                tbxS6.Focus();
        }

        private void tbxS6_TextChanged(object sender, TextChangedEventArgs e)
        {
            checkTime();

            if (tbxS6.Text.Trim().Length == 2)
                tbxS7.Focus();
        }

        private void tbxS7_TextChanged(object sender, TextChangedEventArgs e)
        {
            checkTime();

            if (tbxS7.Text.Trim().Length == 2)
                tbxS8.Focus();
        }

        private void tbxS8_TextChanged(object sender, TextChangedEventArgs e)
        {
            checkTime();

            if (tbxS8.Text.Trim().Length == 2)
                tbxTITLE.Focus();
        }

        private void checkTime()
        {
            int intCheck;
            int intBeg;
            int intEnd;

            if (tbxS1.Text.Trim() != "" && tbxS2.Text.Trim() != "" && tbxS3.Text.Trim() != "" && tbxS4.Text.Trim() != "" &&
               tbxS5.Text.Trim() != "" && tbxS6.Text.Trim() != "" && tbxS7.Text.Trim() != "" && tbxS8.Text.Trim() != "")
            {
                if (tbxS1.Text.Trim().Length == 2 && tbxS2.Text.Trim().Length == 2 && tbxS3.Text.Trim().Length == 2 && tbxS4.Text.Trim().Length == 2 &&
                 tbxS5.Text.Trim().Length == 2 && tbxS6.Text.Trim().Length == 2 && tbxS7.Text.Trim().Length == 2 && tbxS8.Text.Trim().Length == 2)
                {
                    if (int.TryParse(tbxS1.Text.Trim(), out intCheck) == true && int.TryParse(tbxS2.Text.Trim(), out intCheck) == true && int.TryParse(tbxS3.Text.Trim(), out intCheck) == true && int.TryParse(tbxS4.Text.Trim(), out intCheck) == true &&
                        int.TryParse(tbxS5.Text.Trim(), out intCheck) == true && int.TryParse(tbxS6.Text.Trim(), out intCheck) == true && int.TryParse(tbxS7.Text.Trim(), out intCheck) == true && int.TryParse(tbxS8.Text.Trim(), out intCheck) == true)
                    {
                        intBeg = TransferTimecode.timecodetoframe(tbxS1.Text.ToString().Trim() + ":" + tbxS2.Text.ToString().Trim() + ":" + tbxS3.Text.ToString().Trim() + ";" + tbxS4.Text.ToString().Trim());
                        intEnd = TransferTimecode.timecodetoframe(tbxS5.Text.ToString().Trim() + ":" + tbxS6.Text.ToString().Trim() + ":" + tbxS7.Text.ToString().Trim() + ";" + tbxS8.Text.ToString().Trim());

                        if (intBeg > intEnd)
                        {
                            tbAllDurationS.Text = "00:00:00;00";
                            return;
                        }

                        tbAllDurationS.Text = TransferTimecode.frame2timecode(TransferTimecode.Subtract_timecode_toframe(tbxS1.Text.ToString().Trim() + ":" + tbxS2.Text.ToString().Trim() + ":" + tbxS3.Text.ToString().Trim() + ";" + tbxS4.Text.ToString().Trim(),
                    tbxS5.Text.ToString().Trim() + ":" + tbxS6.Text.ToString().Trim() + ":" + tbxS7.Text.ToString().Trim() + ";" + tbxS8.Text.ToString().Trim()));
                    }
                }
            }
        }

        #endregion



    }
}

