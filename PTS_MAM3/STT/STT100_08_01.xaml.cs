﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PTS_MAM3.WSDATA;
using PTS_MAM3.ProgData;
using DataGridDCTest;
using PTS_MAM3.WSBROADCAST;

namespace PTS_MAM3.STT
{
    public partial class STT100_08_01 : ChildWindow
    {
        WSDATASoapClient client = new WSDATASoapClient();  //產生新的代理類別

        public string strDATA_ID_View = "";     //選取的資料帶編號
        public string strDATA_Name_View = "";   //選取的資料帶名稱  
        public string strDATA_Channel_View = "";//選取的資料帶頻道別  
        public Boolean bolUpdate = false;       //修改狀態

        public STT100_08_01()
        {
            InitializeComponent();
            InitializeForm();        //初始化本頁面           
        }

        void InitializeForm() //初始化本頁面
        {
            //透過資料帶名稱查詢所有資料
            client.GetTBDATA_BYDATANAMECompleted += new EventHandler<GetTBDATA_BYDATANAMECompletedEventArgs>(client_GetTBDATA_BYDATANAMECompleted);

            //新資料帶資料檔
            client.INSERT_TBDATACompleted += new EventHandler<INSERT_TBDATACompletedEventArgs>(client_INSERT_TBDATACompleted);
            
            //修改宣傳帶資料檔
            client.UPDATE_TBDATACompleted += new EventHandler<UPDATE_TBDATACompletedEventArgs>(client_UPDATE_TBDATACompleted);

            //透過資料帶名稱查詢資料帶基本資料，修改資料帶用
            client.QUERY_TBDATA_BYNAME_CHECKCompleted += new EventHandler<QUERY_TBDATA_BYNAME_CHECKCompletedEventArgs>(client_QUERY_TBDATA_BYNAME_CHECKCompleted);

            //資料帶資料檔的所有資料_透過資料帶名稱(新增資料帶時的檢查)
            client.GetTBDATA_BYDATANAME_CHECKCompleted += new EventHandler<GetTBDATA_BYDATANAME_CHECKCompletedEventArgs>(client_GetTBDATA_BYDATANAME_CHECKCompleted);

            //修改資料帶後連動修改UpdateTBDIRECTORIES
            client.UpdateTBDIRECTORIESCompleted += new EventHandler<UpdateTBDIRECTORIESCompletedEventArgs>(client_UpdateTBDIRECTORIESCompleted);
            //修改資料帶後連動修改UpdateTBSubject
            client.UpdateTBSubjectCompleted += new EventHandler<UpdateTBSubjectCompletedEventArgs>(client_UpdateTBSubjectCompleted);
        }

              
        
        void LoadBusy()
        {
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
            btnSearch.IsEnabled = false;
            btnAdd.IsEnabled = false;
            btnAlter.IsEnabled = false;
            tbxNAME.IsEnabled = false;
            tbxMEMO.IsEnabled = false;
        }

        void LoadBusyUnLock()
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息
            btnSearch.IsEnabled = true;
            btnAdd.IsEnabled = true;
            btnAlter.IsEnabled = true;
            tbxNAME.IsEnabled = true;
            tbxMEMO.IsEnabled = true;
            tbxNAME.Focus();
        }

        #region 實作  
      
        //實作-透過資料帶名稱查詢所有資料
        void client_GetTBDATA_BYDATANAMECompleted(object sender, GetTBDATA_BYDATANAMECompletedEventArgs e)
        {
            LoadBusyUnLock();

            if (e.Error == null)
            {
                if (e.Result != null && e.Result.Count > 0)
                {
                    this.DGDATAList.DataContext = e.Result;                  
                }
                else
                    MessageBox.Show("查無資料帶資料，請重新輸入條件查詢！", "提示訊息", MessageBoxButton.OK);
            }
        }          

        //實作-新資料帶資料檔
        void client_INSERT_TBDATACompleted(object sender, INSERT_TBDATACompletedEventArgs e)
        {
            LoadBusyUnLock();

            if (e.Error == null && e.Result == true)
            {
                MessageBox.Show("新增資料帶「" + strDATA_Name_View + "」成功！", "提示訊息", MessageBoxButton.OK);
                tbxNAME.Text = "";
                tbxMEMO.Text = "";                
                client.GetTBDATA_BYDATANAMEAsync(strDATA_Name_View);
                LoadBusy();
            }                
            else
                MessageBox.Show("新增資料帶「" + strDATA_Name_View.ToString() + "」失敗！", "提示訊息", MessageBoxButton.OK);
        }

        //實作-修改宣傳帶資料檔
        void client_UPDATE_TBDATACompleted(object sender, UPDATE_TBDATACompletedEventArgs e)
        {
           
           // LoadBusyUnLock();

            if (e.Error == null && e.Result == true)
            {
                client.UpdateTBDIRECTORIESAsync(FormToClass_DATA(), ((Class_DATA)this.DGDATAList.SelectedItem).FSDATA_NAME);
                // MessageBox.Show("修改資料帶「" + strDATA_Name_View.ToString() + "」成功！", "提示訊息", MessageBoxButton.OK);
                // tbxNAME.Text = "";
                //tbxMEMO.Text = "";
                //client.GetTBDATA_BYDATANAMEAsync(strDATA_Name_View);
                // LoadBusy();
            }
            else
            {
                MessageBox.Show("修改資料帶「" + strDATA_Name_View.ToString() + "」失敗！", "提示訊息", MessageBoxButton.OK);

                bolUpdate = false;
            }
        }

        //實作-修改TBSubject中該筆資料帶的資料
        void client_UpdateTBSubjectCompleted(object sender, UpdateTBSubjectCompletedEventArgs e)
        {
            if (e.Error == null && e.Result)
            {
                MessageBox.Show("修改資料帶「" + strDATA_Name_View.ToString() + "」成功！", "提示訊息", MessageBoxButton.OK);
                tbxNAME.Text = "";
                tbxMEMO.Text = "";
                client.GetTBDATA_BYDATANAMEAsync(strDATA_Name_View);
                LoadBusy();
            }
            else
            {
                MessageBox.Show("更新資料帶樹狀結構時發生錯誤!");
            }

            bolUpdate = false;
        }

        //實作-修改TBDIRECTORIES中該筆資料帶的資料
        void client_UpdateTBDIRECTORIESCompleted(object sender, UpdateTBDIRECTORIESCompletedEventArgs e)
        {
            if (e.Error == null && e.Result)
            {
                //繼續更新Subject
                client.UpdateTBSubjectAsync(FormToClass_DATA());
            }
            else
            {
                MessageBox.Show("更新資料帶樹狀結構時發生錯誤!");
            }
        }

        //實作-透過資料帶名稱查詢資料帶基本資料，修改資料帶用
        void client_QUERY_TBDATA_BYNAME_CHECKCompleted(object sender, QUERY_TBDATA_BYNAME_CHECKCompletedEventArgs e)
        {
            LoadBusyUnLock();

            if (e.Error == null)
            {
                if (e.Result == false)
                {
                    //修改資料帶
                    client.UPDATE_TBDATAAsync(FormToClass_DATA());
                    LoadBusy();
                }
                else
                    MessageBox.Show("已有重複的「資料帶名稱」資料，請檢查後重新輸入！", "提示訊息", MessageBoxButton.OK);
            }
            else
            {
                MessageBox.Show("查詢「資料帶名稱」失敗！", "提示訊息", MessageBoxButton.OK);

                bolUpdate = false;
            }
        }

        //實作-資料帶資料檔的所有資料_透過資料帶名稱(新增資料帶時的檢查)
        void client_GetTBDATA_BYDATANAME_CHECKCompleted(object sender, GetTBDATA_BYDATANAME_CHECKCompletedEventArgs e)
        {
             LoadBusyUnLock();

            if (e.Error == null)
            {
                if (e.Result == true)
                {                   
                    client.INSERT_TBDATAAsync(FormToClass_DATA());  //新增資料帶
                    LoadBusy();
                }
                else                                       
                    MessageBox.Show("資料庫已有重複的「資料帶名稱」資料，請檢查後重新輸入！", "提示訊息", MessageBoxButton.OK);  
            }
        }

        #endregion

        #region 檢查畫面資料及搬值

        private Class_DATA FormToClass_DATA()
        {
            Class_DATA obj = new Class_DATA();

          
            //資料帶編號
            if (bolUpdate == true && tbxNAME.Tag != null)   //修改時要記DATA_ID
            {
                strDATA_ID_View = tbxNAME.Tag.ToString().Trim();
                obj.FSDATA_ID = tbxNAME.Tag.ToString().Trim();
                obj.FSDEL = "N";
                obj.FSDELUSER = "";
            }
            else
            {
                strDATA_ID_View = "";                       //新增時不用記DATA_ID
                obj.FSDATA_ID = "";
            }

            obj.FSDATA_NAME = tbxNAME.Text.ToString().Trim();        //資料帶名稱
            strDATA_Name_View = tbxNAME.Text.ToString().Trim();                         
            obj.FSMEMO = tbxMEMO.Text.ToString().Trim();            //備註
            obj.FSCREATED_BY = UserClass.userData.FSUSER_ID.ToString();//建檔者
            obj.FSUPDATED_BY = UserClass.userData.FSUSER_ID.ToString();//修改者 
            return obj;
        }

        #endregion   
 
        //確定
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGDATAList.SelectedItem == null)
            {
                MessageBox.Show("請選擇資料帶", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else
            {
                strDATA_ID_View = ((Class_DATA)DGDATAList.SelectedItem).FSDATA_ID.ToString();
                strDATA_Name_View = ((Class_DATA)DGDATAList.SelectedItem).FSDATA_NAME.ToString();
                strDATA_Channel_View = ((Class_DATA)DGDATAList.SelectedItem).FSCHANNEL_ID.ToString();
                this.DialogResult = true;
            }
        }

        //取消
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
       
        //查詢全部
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {          
            client.GetTBDATA_BYDATANAMEAsync(tbxNAME.Text.Trim());
            LoadBusy();
        }

        //連點事件
        private void RowDoubleClick(object sender, DataGridDCTest.DataGridRowClickedArgs e)
        {
            DoubleClickDataGrid dcdg = sender as DoubleClickDataGrid;

            strDATA_ID_View = ((Class_DATA)DGDATAList.SelectedItem).FSDATA_ID.ToString();
            strDATA_Name_View = ((Class_DATA)DGDATAList.SelectedItem).FSDATA_NAME.ToString();
            strDATA_Channel_View = ((Class_DATA)DGDATAList.SelectedItem).FSCHANNEL_ID.ToString();
            this.DialogResult = true;
        }

        //輸入完查詢內容後按下enter即可查詢
        private void tbxPROMONAME_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                btnSearch_Click(sender, e);
            }
        }

        //新增
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (tbxNAME.Text.Trim() == "")      //檢查畫面上的欄位是否都填妥
            {
                MessageBox.Show("請輸入「資料帶名稱」欄位", "提示訊息", MessageBoxButton.OK);
                return;
            } 

            //檢查是否有相同名稱的資料帶，若有就無法新增
            client.GetTBDATA_BYDATANAME_CHECKAsync(tbxNAME.Text.Trim());
            LoadBusy();
        }

        //修改
        private void btnAlter_Click(object sender, RoutedEventArgs e)
        {
            if (DGDATAList.SelectedItem == null)
            {
                MessageBox.Show("請先選擇欲修改的資料帶，資料帶修改失敗", "提示訊息", MessageBoxButton.OK);
                return;
            }

            if (tbxNAME.Text.Trim() == "")      //檢查畫面上的欄位是否都填妥
            {
                MessageBox.Show("請輸入「資料帶名稱」欄位", "提示訊息", MessageBoxButton.OK);
                return;
            }

            if (tbxNAME.Text.Trim() == ((Class_DATA)DGDATAList.SelectedItem).FSDATA_NAME.ToString().Trim() && tbxMEMO.Text.Trim() == ((Class_DATA)DGDATAList.SelectedItem).FSMEMO.ToString().Trim())
            {
                MessageBox.Show("「資料帶名稱」欄位與「備註」欄位未修改，資料帶未修改", "提示訊息", MessageBoxButton.OK);
                return;
            }

            bolUpdate = true;
            //查詢是否有相同資料帶名稱的資料
            client.QUERY_TBDATA_BYNAME_CHECKAsync(tbxNAME.Text.ToString().Trim(), tbxNAME.Tag.ToString().Trim());
            LoadBusy();
        }

        private void DGDATAList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DGDATAList.SelectedItem != null)
            {
                tbxNAME.Tag = ((Class_DATA)DGDATAList.SelectedItem).FSDATA_ID.ToString();
                tbxNAME.Text = ((Class_DATA)DGDATAList.SelectedItem).FSDATA_NAME.ToString();
                tbxMEMO.Text = ((Class_DATA)DGDATAList.SelectedItem).FSMEMO.ToString();
            }
        }		
       
    }
}

