﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using PTS_MAM3.STT;
using PTS_MAM3.WSBROADCAST;
using System.Windows.Browser;
using System.Windows.Data;

namespace PTS_MAM3.STT
{
    public partial class STT900 : Page
    {
        WSBROADCASTSoapClient client = new WSBROADCASTSoapClient();             //產生新的代理類別
        List<Class_BROADCAST> m_ListFormData = new List<Class_BROADCAST>();     //記錄查詢到的成案資料
        private FlowFieldCom.FlowFieldComSoapClient ComClient = new FlowFieldCom.FlowFieldComSoapClient();

        public STT900()
        {
            InitializeComponent();

            //取得所有的送帶轉檔置換檔_BY新增者
            client.fnGetTBBROADCAST_ALL_BYUSERID_CHANGECompleted += new EventHandler<fnGetTBBROADCAST_ALL_BYUSERID_CHANGECompletedEventArgs>(client_fnGetTBBROADCAST_ALL_BYUSERID_CHANGECompleted);
           
            //列印送帶轉檔單
            client.CallREPORT_BROCompleted += new EventHandler<CallREPORT_BROCompletedEventArgs>(client_CallREPORT_BROCompleted);

            Load_TBBROADCAST_ALL_BYUSERID_CHANGE();

            //選擇不同表單時 顯示流程狀態
            DGBroListDouble.SelectionChanged += new SelectionChangedEventHandler(DGBroListDouble_SelectionChanged);
            ComClient.GetApprovalListDetailByFormIDCompleted += new EventHandler<FlowFieldCom.GetApprovalListDetailByFormIDCompletedEventArgs>(ComClient_GetApprovalListDetailByFormIDCompleted);
        }

        //實作-列印送帶轉檔單
        void client_CallREPORT_BROCompleted(object sender, CallREPORT_BROCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    if (e.Result.ToString().StartsWith("錯誤"))
                        MessageBox.Show(e.Result.ToString(), "提示訊息", MessageBoxButton.OK);
                    else
                        HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + e.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,resizable=yes");
                }
                else
                    MessageBox.Show("查無相關資料，或該筆資料已不存在資料庫中。", "提示訊息", MessageBoxButton.OK);
            };
        }

        //實作-取得所有的送帶轉檔置換檔_BY新增者
        void client_fnGetTBBROADCAST_ALL_BYUSERID_CHANGECompleted(object sender, fnGetTBBROADCAST_ALL_BYUSERID_CHANGECompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    //宣告PagedCollectionView,將撈取的資料放入
                    PagedCollectionView pcv = new PagedCollectionView(e.Result);
                    //指定DataGrid以及DataPager的內容
                    DGBroListDouble.ItemsSource = pcv;
                    DataPager.Source = pcv;
                }
            }
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息
        }

        void ComClient_GetApprovalListDetailByFormIDCompleted(object sender, FlowFieldCom.GetApprovalListDetailByFormIDCompletedEventArgs e)
        {
            dataGrid2.ItemsSource = e.Result;
        }

        //實做 傳回流程狀態
        void DGBroListDouble_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DGBroListDouble.SelectedIndex != -1)
            {
                ComClient.GetApprovalListDetailByFormIDAsync(((Class_BROADCAST)DGBroListDouble.SelectedItem).FSBRO_ID);
            }
        }   

        //載入送帶轉檔置換資料_BY新增者
        private void Load_TBBROADCAST_ALL_BYUSERID_CHANGE()
        {
            DateTime dtStart = DateTime.Now.AddYears(-1);
            DateTime dtEnd = DateTime.Now.AddDays(1);       //要加一天才能帶出今天的
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
            client.fnGetTBBROADCAST_ALL_BYUSERID_CHANGEAsync(UserClass.userData.FSUSER_ID.ToString().Trim(), dtStart, dtEnd);  
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }
        #region 用不到的CODE
        //新增
        private void btnBroAdd_Click(object sender, RoutedEventArgs e)
        {
            STT500_01 BroChange_Add_frm = new STT500_01();
            BroChange_Add_frm.Show();

            BroChange_Add_frm.Closing += (s, args) =>
            {
                if (BroChange_Add_frm.DialogResult == true)
                {
                    Load_TBBROADCAST_ALL_BYUSERID_CHANGE();
                }
            };
        }

        //修改
        private void btBroModify_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGBroListDouble.SelectedItem == null)
            {
                MessageBox.Show("請選擇預修改的送帶轉檔置換資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            if (((Class_BROADCAST)DGBroListDouble.SelectedItem).FCCHECK_STATUS != "C")
            {
                MessageBox.Show("該筆送帶轉檔置換資料不是「審核中」，無法修改", "提示訊息", MessageBoxButton.OK);
                return;
            }

            STT500_02 STT500_02_frm = new STT500_02(((Class_BROADCAST)DGBroListDouble.SelectedItem));
            STT500_02_frm.Show();

            STT500_02_frm.Closing += (s, args) =>
            {
                if (STT500_02_frm.DialogResult == true)
                {
                    Load_TBBROADCAST_ALL_BYUSERID_CHANGE();
                }
            };
        }
        #endregion
        //查詢
        private void btnBroQuery_Click(object sender, RoutedEventArgs e)
        {
            STT900_04 STT900_04_frm = new STT900_04();
            STT900_04_frm.Show();

            STT900_04_frm.Closed += (s, args) =>
            {
                if (STT900_04_frm.m_ListFormData.Count != 0)
                {
                    //this.DGBroListDouble.DataContext = null;
                    //this.DGBroListDouble.DataContext = STT500_04_frm.m_ListFormData;
                    //this.DGBroListDouble.SelectedIndex = 0;

                    DGBroListDouble.ItemsSource = null;
                    PagedCollectionView pcv = new PagedCollectionView(STT900_04_frm.m_ListFormData);
                    DGBroListDouble.ItemsSource = pcv;
                    DataPager.Source = pcv; 
                }
            };
        }

        //更新
        private void btnBroRefresh_Click(object sender, RoutedEventArgs e)
        {
            Load_TBBROADCAST_ALL_BYUSERID_CHANGE();           
        }

        //列印
        private void btnRpt_Click(object sender, RoutedEventArgs e)
        {
             //判斷是否有選取DataGrid
            if (DGBroListDouble.SelectedItem == null)
            {
                MessageBox.Show("請選擇預列印的節目資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //列印成案單(丟入參數：成案單編號、查詢者、SessionID)
            client.CallREPORT_BROAsync(((Class_BROADCAST)DGBroListDouble.SelectedItem).FSBRO_ID.ToString().Trim(), UserClass.userData.FSUSER_ID.ToString().Trim(), UserClass.userData.FSSESSION_ID.ToString().Trim());
        }

        //連續點擊-檢視送帶轉檔  
        private void DGBroListDouble_RowDoubleClicked(object sender, DataGridDCTest.DataGridRowClickedArgs e)
        {
            DataGridDCTest.DoubleClickDataGrid dcdg = sender as DataGridDCTest.DoubleClickDataGrid;

            //判斷是否有選取DataGrid
            if (dcdg.SelectedItem == null)
            {
                MessageBox.Show("請選擇預檢視的送帶轉檔置換資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            STT500_06 STT500_06_frm = new STT500_06(((Class_BROADCAST)DGBroListDouble.SelectedItem).FSBRO_ID.ToString().Trim());
            STT500_06_frm.Show();
        }

    }
}
