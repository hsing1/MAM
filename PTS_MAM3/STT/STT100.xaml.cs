﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using PTS_MAM3.STT;
using PTS_MAM3.WSBROADCAST;
using System.Windows.Browser;
using System.Windows.Data;


namespace PTS_MAM3.STT
{
    public partial class STT100 : Page
    {
        WSBROADCASTSoapClient client = new WSBROADCASTSoapClient();             //產生新的代理類別
        List<Class_BROADCAST> m_ListFormData = new List<Class_BROADCAST>();     //記錄查詢到的成案資料
        string m_FileUpload_Path = "";                                          //檔案上傳路徑
        private FlowFieldCom.FlowFieldComSoapClient ComClient = new FlowFieldCom.FlowFieldComSoapClient();
        WSDELETE.WSDELETESoapClient wsDeleteClient = new WSDELETE.WSDELETESoapClient();
        //決定是否執行Flow模式 by Jarvis20130621
        public static readonly bool GoFlow = false;//false:不走flow

        public STT100()
        {
            InitializeComponent();

            //取得所有的送帶轉檔檔
            //client.fnGetTBBROADCAST_ALLCompleted += new EventHandler<fnGetTBBROADCAST_ALLCompletedEventArgs>(client_fnGetTBBROADCAST_ALLCompleted);
            //client.fnGetTBBROADCAST_ALLAsync(); //不Load全部，只Load自己新增的

            //取得所有的送帶轉檔檔_BY新增者
            client.fnGetTBBROADCAST_ALL_BYUSERIDCompleted += new EventHandler<fnGetTBBROADCAST_ALL_BYUSERIDCompletedEventArgs>(client_fnGetTBBROADCAST_ALL_BYUSERIDCompleted);

            //列印送帶轉檔單
            client.CallREPORT_BROCompleted += new EventHandler<CallREPORT_BROCompletedEventArgs>(client_CallREPORT_BROCompleted);

            //列印送帶轉檔單_多筆短帶
            client.CallREPORT_BRO_PROMOCompleted += new EventHandler<CallREPORT_BRO_PROMOCompletedEventArgs>(client_CallREPORT_BRO_PROMOCompleted);

            //取得檔案上傳區
            client.GET_FILEUPLOAD_PATHCompleted += new EventHandler<GET_FILEUPLOAD_PATHCompletedEventArgs>(client_GET_FILEUPLOAD_PATHCompleted);
            client.GET_FILEUPLOAD_PATHAsync();

            //用送帶轉檔單號取得入庫檔是否有超過一筆
            client.QUERY_TBLOG_VIDEO_COUNTCompleted += new EventHandler<QUERY_TBLOG_VIDEO_COUNTCompletedEventArgs>(client_QUERY_TBLOG_VIDEO_COUNTCompleted);

            //查詢送帶轉檔檔是否有超過一筆的列印資料
            client.QUERY_TBBROADCAST_PRINT_CHECKCompleted += new EventHandler<QUERY_TBBROADCAST_PRINT_CHECKCompletedEventArgs>(client_QUERY_TBBROADCAST_PRINT_CHECKCompleted);

            //用送帶轉檔單編號取得製作人列表字串
            client.GetPRODUCER_BYBroIDCompleted += new EventHandler<GetPRODUCER_BYBroIDCompletedEventArgs>(client_GetPRODUCER_BYBroIDCompleted);

            //根據節目尋找檔案編號
            client.GetTBLOG_VIDEO_FileNOList_BYBROIDCompleted += new EventHandler<GetTBLOG_VIDEO_FileNOList_BYBROIDCompletedEventArgs>(client_GetTBLOG_VIDEO_FileNOList_BYBROIDCompleted);

            //抽單的實際動作 by Jarvis20130507
            wsDeleteClient.DeleteFileCompleted += new EventHandler<WSDELETE.DeleteFileCompletedEventArgs>(wsDeleteClient_DeleteFileCompleted);

            //根據表單ID尋找FlowID by Jarvis20130507
            ComClient.GetFlowIDfromFormIDCompleted += new EventHandler<FlowFieldCom.GetFlowIDfromFormIDCompletedEventArgs>(ComClient_GetFlowIDfromFormIDCompleted);

            //更新TBBROADCAST中送帶轉檔單的狀態 by Jarvis20130521
            client.UPDATE_TBBROADCAST_Staus_DirectlyCompleted += new EventHandler<UPDATE_TBBROADCAST_Staus_DirectlyCompletedEventArgs>(client_UPDATE_TBBROADCAST_Staus_DirectlyCompleted);

            //根據送帶轉檔單編號查詢檔案所有狀態 by Jarvis20130621
            client.GetTBLOG_VIDEO_FileInfoList_BYBROIDCompleted += new EventHandler<GetTBLOG_VIDEO_FileInfoList_BYBROIDCompletedEventArgs>(client_GetTBLOG_VIDEO_FileInfoList_BYBROIDCompleted);
            //根據檔案編號查傳回所有段落資料 by Jarvis20130621
            client.GetTBLOG_VIDEO_SEG_COUNTCompleted += new EventHandler<GetTBLOG_VIDEO_SEG_COUNTCompletedEventArgs>(client_GetTBLOG_VIDEO_SEG_COUNTCompleted);

            //抽單發生錯誤時回復TBDELETED與TBLOG_VIDEO的資料
            wsDeleteClient.Reply_Detached_ErrorCompleted += new EventHandler<WSDELETE.Reply_Detached_ErrorCompletedEventArgs>(wsDeleteClient_Reply_Detached_ErrorCompleted);

            wsDeleteClient.Reply_Detached_Error_BatchCompleted += new EventHandler<WSDELETE.Reply_Detached_Error_BatchCompletedEventArgs>(wsDeleteClient_Reply_Detached_Error_BatchCompleted);
            //client.UPDATE_TBBROADCAST_StausCompleted += new EventHandler<UPDATE_TBBROADCAST_StausCompletedEventArgs>(client_UPDATE_TBBROADCAST_StausCompleted);

            //透過登入帳號取得送帶轉檔檔
            Load_TBBROADCAST_ALL_BYUSERID();

            //選擇不同表單時 顯示流程狀態
            DGBroListDouble.SelectionChanged += new SelectionChangedEventHandler(DGBroListDouble_SelectionChanged);
            ComClient.GetApprovalListDetailByFormIDCompleted += new EventHandler<FlowFieldCom.GetApprovalListDetailByFormIDCompletedEventArgs>(ComClient_GetApprovalListDetailByFormIDCompleted);
        }

        void wsDeleteClient_Reply_Detached_Error_BatchCompleted(object sender, WSDELETE.Reply_Detached_Error_BatchCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {
                    MessageBox.Show("抽單過程發生錯誤，資料已回復!", "提示訊息", MessageBoxButton.OK);
                    Load_TBBROADCAST_ALL_BYUSERID();
                    DetachedBtn.IsEnabled = true;
                }
                else 
                {
                   
                }
            }
        }



        #region 實作

        //實作-列印送帶轉檔單
        void client_CallREPORT_BROCompleted(object sender, CallREPORT_BROCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    if (e.Result.ToString().StartsWith("錯誤"))
                        MessageBox.Show(e.Result.ToString(), "提示訊息", MessageBoxButton.OK);
                    else
                        HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + e.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,resizable=yes");
                }
                else
                    MessageBox.Show("查無相關資料，或該筆資料已不存在資料庫中。", "提示訊息", MessageBoxButton.OK);
            };
        }

        //實作-列印送帶轉檔單_多筆短帶
        void client_CallREPORT_BRO_PROMOCompleted(object sender, CallREPORT_BRO_PROMOCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    if (e.Result.ToString().StartsWith("錯誤"))
                        MessageBox.Show(e.Result.ToString(), "提示訊息", MessageBoxButton.OK);
                    else
                        HtmlPage.Window.Navigate(new Uri("REPORT.aspx?url=" + e.Result, UriKind.Relative), "_blank", "height=600,width=800,top=100,left=100,resizable=yes");
                }
                else
                    MessageBox.Show("查無相關資料，或該筆資料已不存在資料庫中。", "提示訊息", MessageBoxButton.OK);
            };
        }

        //實作-取得所有的送帶轉檔檔
        void client_fnGetTBBROADCAST_ALLCompleted(object sender, fnGetTBBROADCAST_ALLCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    List<Class_BROADCAST> ListBROADCAST = new List<Class_BROADCAST>();

                    for (int i = 0; i < e.Result.Count; i++)
                    {
                        if (e.Result[i].FCCHANGE.Trim() != "Y")    //要濾掉只剩下送帶轉檔的表單
                        {
                            ListBROADCAST.Add(e.Result[i]);
                        }
                    }

                    //this.DGBroListDouble.DataContext = ListBROADCAST;

                    //if (this.DGBroListDouble.DataContext != null)
                    //{
                    //    DGBroListDouble.SelectedIndex = 0;
                    //}

                    //宣告PagedCollectionView,將撈取的資料放入
                    PagedCollectionView pcv = new PagedCollectionView(ListBROADCAST);
                    //指定DataGrid以及DataPager的內容
                    DGBroListDouble.ItemsSource = pcv;
                    DataPager.Source = pcv;
                }
            }
        }

        //實作-取得所有的送帶轉檔檔_BY新增者
        void client_fnGetTBBROADCAST_ALL_BYUSERIDCompleted(object sender, fnGetTBBROADCAST_ALL_BYUSERIDCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    PagedCollectionView pcv = new PagedCollectionView(e.Result);
                    DGBroListDouble.ItemsSource = pcv;
                    DataPager.Source = pcv;
                }
            }
        }

        //實作-取得檔案上傳區
        void client_GET_FILEUPLOAD_PATHCompleted(object sender, GET_FILEUPLOAD_PATHCompletedEventArgs e)
        {
            m_FileUpload_Path = e.Result;
        }

        //實作-用送帶轉檔單號取得入庫檔是否有超過一筆
        void client_QUERY_TBLOG_VIDEO_COUNTCompleted(object sender, QUERY_TBLOG_VIDEO_COUNTCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result == true)
                    OpenSTT100_07();    //短帶專屬修改頁面(適用於只有一筆)
                else
                {
                    if (DGBroListDouble.SelectedItem != null)
                        OpenSTT100_02();
                    else
                    {
                        if (((Class_BROADCAST)DGBroListDouble.SelectedItem).FSBRO_TYPE == "G")
                            OpenSTT100_02();    //節目修改頁面
                        else
                            OpenSTT100_09();    //資料帶修改頁面
                    }
                }
            }
            else
                OpenSTT100_02();
        }

        //實作-查詢送帶轉檔檔是否有超過一筆的列印資料
        void client_QUERY_TBBROADCAST_PRINT_CHECKCompleted(object sender, QUERY_TBBROADCAST_PRINT_CHECKCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result == true)
                {
                    //要詢問要不要一起列印其他的送帶轉檔單
                    MessageBoxResult resultMsg = MessageBox.Show("確定要列印多筆送帶轉檔單嗎?", "提示訊息", MessageBoxButton.OKCancel);
                    if (resultMsg == MessageBoxResult.OK)
                        PrintSTT_PROMO();   //列印多張的送帶轉檔單                  
                    else
                        PrintSTT();           //列印單張送帶轉檔單                    
                }
                else
                    PrintSTT();               //列印單張送帶轉檔單                      
            }
        }

        //實作-用送帶轉檔單編號取得製作人列表字串
        void client_GetPRODUCER_BYBroIDCompleted(object sender, GetPRODUCER_BYBroIDCompletedEventArgs e)
        {
            BusyMsg.IsBusy = false;  //關閉忙碌的Loading訊息

            if (e.Error == null)
            {
                if (e.Result == "me")
                    OpenUpdateSTT();    //開啟送帶轉檔的介面
                else
                    MessageBox.Show("該筆送帶轉檔資料不是「審核中」，無法修改", "提示訊息", MessageBoxButton.OK);
            }
            else
                MessageBox.Show("該筆送帶轉檔資料不是「審核中」，無法修改", "提示訊息", MessageBoxButton.OK);
        }

        void ComClient_GetApprovalListDetailByFormIDCompleted(object sender, FlowFieldCom.GetApprovalListDetailByFormIDCompletedEventArgs e)
        {
            dataGrid2.ItemsSource = e.Result;
        }

        //實做 傳回流程狀態
        void DGBroListDouble_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DGBroListDouble.SelectedIndex != -1)
            {
                ComClient.GetApprovalListDetailByFormIDAsync(((Class_BROADCAST)DGBroListDouble.SelectedItem).FSBRO_ID);
            }
        }

        #endregion

        //載入送帶轉檔資料_BY新增者
        private void Load_TBBROADCAST_ALL_BYUSERID()
        {
            DateTime dtStart = DateTime.Now.AddYears(-1);
            DateTime dtEnd = DateTime.Now.AddDays(1);       //要加一天才能帶出今天的
            BusyMsg.IsBusy = true;  //開啟忙碌的Loading訊息
            client.fnGetTBBROADCAST_ALL_BYUSERIDAsync(UserClass.userData.FSUSER_ID.ToString().Trim(), dtStart, dtEnd);
        }

        // 使用者巡覽至這個頁面時執行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        //新增
        private void btnBroAdd_Click(object sender, RoutedEventArgs e)
        {
            STT100_01 Bro_Add_frm = new STT100_01();
            Bro_Add_frm.Show();

            Bro_Add_frm.Closing += (s, args) =>
            {
                if (Bro_Add_frm.DialogResult == true)
                    Load_TBBROADCAST_ALL_BYUSERID();
            };
        }

        //新增短帶
        private void btnBroPromoAdd_Click(object sender, RoutedEventArgs e)
        {
            STT100_07 Bro_Promo_Add_frm = new STT100_07("", "");
            Bro_Promo_Add_frm.Show();

            Bro_Promo_Add_frm.Closing += (s, args) =>
            {
                if (Bro_Promo_Add_frm.DialogResult == true)
                    Load_TBBROADCAST_ALL_BYUSERID();
            };
        }

        //新增資料帶
        private void btnBroDataAdd_Click(object sender, RoutedEventArgs e)
        {
            STT100_08 Bro_Data_Add_frm = new STT100_08();
            Bro_Data_Add_frm.Show();

            Bro_Data_Add_frm.Closing += (s, args) =>
            {
                if (Bro_Data_Add_frm.DialogResult == true)
                    Load_TBBROADCAST_ALL_BYUSERID();
            };
        }

        //修改
        private void btBroModify_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGBroListDouble.SelectedItem == null)
            {
                MessageBox.Show("請選擇欲修改的送帶轉檔資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //送單人任何情況下都能修改送帶轉檔的TC 2012/07/31 kyle
            ////只有狀態為已簽收並且申請者就是製作人，才可以修改此單，不然狀態都要是未審核才可以修改
            //if (((Class_BROADCAST)DGBroListDouble.SelectedItem).FCCHECK_STATUS == "Y")
            //{
            //    //狀態為已簽收，要檢查申請者是否是製作人
            //    client.GetPRODUCER_BYBroIDAsync(((Class_BROADCAST)DGBroListDouble.SelectedItem).FSBRO_ID.Trim());
            //}

            //else if (((Class_BROADCAST)DGBroListDouble.SelectedItem).FCCHECK_STATUS != "N")
            //{
            //    MessageBox.Show("該筆送帶轉檔資料不是「審核中」，無法修改", "提示訊息", MessageBoxButton.OK);
            //    return;
            //}
            //else if (((Class_BROADCAST)DGBroListDouble.SelectedItem).FCCHECK_STATUS == "N" && ((Class_BROADCAST)DGBroListDouble.SelectedItem).FSCREATED_BY.Trim() != UserClass.userData.FSUSER_ID.ToString().Trim())
            if (((Class_BROADCAST)DGBroListDouble.SelectedItem).FSCREATED_BY.Trim() != UserClass.userData.FSUSER_ID.ToString().Trim())
            {
                MessageBox.Show("非「本人」申請的送帶轉檔單，無法修改", "提示訊息", MessageBoxButton.OK);
                return;
            }
            else if (((Class_BROADCAST)DGBroListDouble.SelectedItem).FCCHECK_STATUS != "N")                 //未審核的才能改
            {
                MessageBox.Show("該筆送帶轉檔單並非「審核中」，無法修改", "提示訊息", MessageBoxButton.OK);
            }
            else if (((Class_BROADCAST)DGBroListDouble.SelectedItem).FCCHECK_STATUS == "G")
            { MessageBox.Show("本筆記錄已被轉檔中心簽收無法修改若欲修改，請由轉檔中心退回重送"); }
            else
            {
                OpenUpdateSTT();    //開啟送帶轉檔的介面
            }
        }

        //開啟送帶轉檔的介面
        private void OpenUpdateSTT()
        {
            if (((Class_BROADCAST)DGBroListDouble.SelectedItem).FSBRO_TYPE == "P")
            {
                client.QUERY_TBLOG_VIDEO_COUNTAsync(((Class_BROADCAST)DGBroListDouble.SelectedItem).FSBRO_ID.Trim());
            }
            else
            {
                if (((Class_BROADCAST)DGBroListDouble.SelectedItem).FSBRO_TYPE == "G")
                    OpenSTT100_02();    //節目修改頁面
                else
                    OpenSTT100_09();    //資料帶修改頁面          
            }
        }

        //查詢
        private void btnBroQuery_Click(object sender, RoutedEventArgs e)
        {
            STT100_04 STT100_04_frm = new STT100_04();
            STT100_04_frm.Show();

            STT100_04_frm.Closed += (s, args) =>
            {
                if (STT100_04_frm.m_ListFormData.Count != 0)
                {
                    //this.DGBroListDouble.DataContext = null;
                    //this.DGBroListDouble.DataContext = STT100_04_frm.m_ListFormData;
                    //this.DGBroListDouble.SelectedIndex = 0;

                    DGBroListDouble.ItemsSource = null;
                    PagedCollectionView pcv = new PagedCollectionView(STT100_04_frm.m_ListFormData);
                    DGBroListDouble.ItemsSource = pcv;
                    DataPager.Source = pcv;
                }
            };
        }

        //開啟送帶轉檔修改頁面
        private void OpenSTT100_02()
        {
            STT100_02 STT100_02_frm = new STT100_02(((Class_BROADCAST)DGBroListDouble.SelectedItem));
            STT100_02_frm.Show();

            STT100_02_frm.Closing += (s, args) =>
            {
                if (STT100_02_frm.DialogResult == true)
                    Load_TBBROADCAST_ALL_BYUSERID();
            };
        }

        //開啟送帶轉檔短帶修改頁面
        private void OpenSTT100_07()
        {
            STT100_07 STT100_07_frm = new STT100_07(((Class_BROADCAST)DGBroListDouble.SelectedItem));
            STT100_07_frm.Show();

            STT100_07_frm.Closing += (s, args) =>
            {
                if (STT100_07_frm.DialogResult == true)
                    Load_TBBROADCAST_ALL_BYUSERID();
            };
        }

        //開啟送帶轉檔短帶修改頁面
        private void OpenSTT100_09()
        {
            STT100_09 STT100_09_frm = new STT100_09(((Class_BROADCAST)DGBroListDouble.SelectedItem));
            STT100_09_frm.Show();

            STT100_09_frm.Closing += (s, args) =>
            {
                if (STT100_09_frm.DialogResult == true)
                    Load_TBBROADCAST_ALL_BYUSERID();
            };
        }

        //更新
        private void btnBroRefresh_Click(object sender, RoutedEventArgs e)
        {
            Load_TBBROADCAST_ALL_BYUSERID();
        }

        //列印
        private void btnRpt_Click(object sender, RoutedEventArgs e)
        {
            //判斷是否有選取DataGrid
            if (DGBroListDouble.SelectedItem == null)
            {
                MessageBox.Show("請選擇預列印的節目資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            //string strPrintID = ((Class_BROADCAST)DGBroListDouble.SelectedItem).FSPRINTID.ToString().Trim();
            if (((Class_BROADCAST)DGBroListDouble.SelectedItem).FSBRO_TYPE == "P")//主頁面只有短帶能列印資料 by Jarvis 20130822
            {
                //if (strPrintID.Trim() == "")
                //{
                //  PrintSTT(); //列印單張送帶轉檔單
                PrintSTT_PROMO();
                //}
                //else
                //{
                //    //查詢送帶轉檔檔是否有超過一筆的列印資料
                //    client.QUERY_TBBROADCAST_PRINT_CHECKAsync(((Class_BROADCAST)DGBroListDouble.SelectedItem).FSPRINTID.ToString().Trim());
                //}
            }
            else
            {
                MessageBox.Show("非短帶資料請點擊兩下要列印的資料後，選擇「列印送帶轉檔明細表」執行列印動作", "提示", MessageBoxButton.OK);
            }
        }

        //連續點擊-檢視送帶轉檔  
        private void DGBroListDouble_RowDoubleClicked(object sender, DataGridDCTest.DataGridRowClickedArgs e)
        {
            DataGridDCTest.DoubleClickDataGrid dcdg = sender as DataGridDCTest.DoubleClickDataGrid;

            //判斷是否有選取DataGrid
            if (dcdg.SelectedItem == null)
            {
                MessageBox.Show("請選擇預檢視的送帶轉檔資料", "提示訊息", MessageBoxButton.OK);
                return;
            }

            STT100_06 STT100_06_frm = new STT100_06(((Class_BROADCAST)DGBroListDouble.SelectedItem).FSBRO_ID.ToString().Trim());
            STT100_06_frm.Show();
            STT100_06_frm.Closing += (s6, args6) =>
                {
                    if (STT100_06_frm.DialogResult == true)
                    {
                        Load_TBBROADCAST_ALL_BYUSERID();
                    }
                };
        }

        //檔案上傳區
        private void btnSTT_Click(object sender, RoutedEventArgs e)
        {
            HtmlPage.Window.Navigate(new Uri(m_FileUpload_Path, UriKind.Absolute), "_blank", "resizable=yes");
        }

        //列印單張送帶轉檔單
        private void PrintSTT()
        {
            //列印成案單(丟入參數：成案單編號、查詢者、SessionID)
            client.CallREPORT_BROAsync(((Class_BROADCAST)DGBroListDouble.SelectedItem).FSBRO_ID.ToString().Trim(), UserClass.userData.FSUSER_ID.ToString().Trim(), UserClass.userData.FSSESSION_ID.ToString().Trim());
        }

        //列印送帶轉檔單_多筆短帶資料
        private void PrintSTT_PROMO()
        {
            //列印成案單(丟入參數：成案單編號、查詢者、SessionID)
            client.CallREPORT_BRO_PROMOAsync(((Class_BROADCAST)DGBroListDouble.SelectedItem).FSPRINTID.ToString().Trim(), UserClass.userData.FSUSER_ID.ToString().Trim(), UserClass.userData.FSSESSION_ID.ToString().Trim());
        }

        #region Jarvis
        //抽單
        WSFlowCancel.WSFlowCancelSoapClient cancelClient = new WSFlowCancel.WSFlowCancelSoapClient();
        Class_BROADCAST class_Broadcast = null;
        //WSVideoMetaEdit.WSVideoMetaEditSoapClient videoMetaEditClient = new WSVideoMetaEdit.WSVideoMetaEditSoapClient();

        List<string> _deleteFileList = new List<string>();
        void client_GetTBLOG_VIDEO_FileInfoList_BYBROIDCompleted(object sender, GetTBLOG_VIDEO_FileInfoList_BYBROIDCompletedEventArgs e)
        {
            if (e.Result.Count > 0)
            {
                string[] fileArr = e.Result[0].Split(new char[] { ';' });
                _deleteFileList.Clear();

                foreach (string str in e.Result)
                {
                    string[] fileStateArr = str.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                    if (fileStateArr.Length == 2)
                    {
                        _deleteFileList.Add(fileStateArr[0]);
                    }
                }

                if (fileArr[1] == "待轉檔")//FCFILE_STATUS=B
                {
                    client.GetTBLOG_VIDEO_SEG_COUNTAsync(fileArr[0]);
                }
                else
                {
                    MessageBox.Show("檔案狀態為「" + fileArr[1] + "」無法抽單", "提示", MessageBoxButton.OK);
                    DetachedBtn.IsEnabled = true;
                }
            }
        }

        void client_GetTBLOG_VIDEO_SEG_COUNTCompleted(object sender, GetTBLOG_VIDEO_SEG_COUNTCompletedEventArgs e)
        {
            List<Class_LOG_VIDEO_SEG> SEG_Result = e.Result;
            bool flag = true;
            foreach (Class_LOG_VIDEO_SEG clvs in SEG_Result)
            {
                if (clvs.FCLOW_RES != "N")
                {
                    MessageBox.Show("此筆送帶轉檔單的檔案不為「待轉擋」，無法抽單", "提示", MessageBoxButton.OK);
                    this.DetachedBtn.IsEnabled = true;
                    flag = false;
                    return;
                }
            }

            if (flag)
            {
                if (ASK())
                {
                    DeleteVideoDataMethod(_deleteFileList);
                }
                else
                {
                    this.DetachedBtn.IsEnabled = true;
                }
            }
        }
        //抽單按鈕事件 by Jarvis20130430
        private void DetachedBtn_Click(object sender, RoutedEventArgs e)
        {
            this.DetachedBtn.IsEnabled = false;
            class_Broadcast = ((Class_BROADCAST)DGBroListDouble.SelectedItem);
            string BRO_ID = class_Broadcast.FSBRO_ID;

            //判斷是否為本人建立
            if (class_Broadcast.FSCREATED_BY != UserClass.userData.FSUSER_ID.ToString().Trim())
            {
                MessageBox.Show("此登入帳號非此資料之申請帳號，無法抽單");
                this.DetachedBtn.IsEnabled = true;
                return;
            }

            if (GoFlow)
            {
                if (class_Broadcast.FCCHECK_STATUS == "N")
                { //審核中，刪DB資料，刪除Flow裡的資料

                    //更新TBBROADCAST中送帶轉檔單的狀態
                    //client.UPDATE_TBBROADCAST_StausCompleted += new EventHandler<UPDATE_TBBROADCAST_StausCompletedEventArgs>(client_UPDATE_TBBROADCAST_StausCompleted);
                    //client.UPDATE_TBBROADCAST_StausAsync(BRO_ID, "X", UserClass.userData.FSUSER_ID);//改變該筆資料在TBBROADCAST中的狀態
                    if (ASK())
                    {
                        //尋找檔案ID
                        client.GetTBLOG_VIDEO_FileNOList_BYBROIDAsync(BRO_ID);

                        //根據節目編號查詢FlowID
                        ComClient.GetFlowIDfromFormIDAsync(BRO_ID);
                    }
                    else
                    {
                        this.DetachedBtn.IsEnabled = true;
                    }
                }
                else if (class_Broadcast.FCCHECK_STATUS == "Y")
                {//審核通過，刪DB資料(通過審核後已不在Flow中)
                    if (ASK())
                    {
                        //更新TBBROADCAST中送帶轉檔單的狀態(這是置換用)
                        //client.UPDATE_TBBROADCAST_StausAsync(BRO_ID, "X", UserClass.userData.FSUSER_ID);//改變該筆資料在TBBROADCAST中的狀態
                        client.UPDATE_TBBROADCAST_Staus_DirectlyAsync(BRO_ID, "X", UserClass.userData.FSUSER_ID);//改變該筆資料在TBBROADCAST中的狀態

                        //尋找檔案ID
                        client.GetTBLOG_VIDEO_FileNOList_BYBROIDAsync(BRO_ID);
                    }
                    else
                    {
                        this.DetachedBtn.IsEnabled = true;
                    }
                }
                else//轉檔、抽單、退回
                {
                    MessageBox.Show("表單狀態為「" + class_Broadcast.FCCHECK_STATUS_NAME + "」無法抽單!!");
                    this.DetachedBtn.IsEnabled = true;
                }
            }
            else
            {
                if (class_Broadcast.FCCHECK_STATUS == "G")
                {
                    //if (ASK())
                    //{
                    //    //client.UPDATE_TBBROADCAST_Staus_DirectlyAsync(BRO_ID, "X", UserClass.userData.FSUSER_ID);//改變該筆資料在TBBROADCAST中的狀態

                    //    ////尋找檔案ID
                    //    //client.GetTBLOG_VIDEO_FileNOList_BYBROIDAsync(BRO_ID);
                    client.GetTBLOG_VIDEO_FileInfoList_BYBROIDAsync(BRO_ID);

                    //}
                    //else
                    //{
                    //    this.DetachedBtn.IsEnabled = true;
                    //}
                }
                else//轉檔、抽單、退回
                {
                    MessageBox.Show("表單狀態為「" + class_Broadcast.FCCHECK_STATUS_NAME + "」無法抽單!!");
                    this.DetachedBtn.IsEnabled = true;
                }
            }
        }

        void client_UPDATE_TBBROADCAST_Staus_DirectlyCompleted(object sender, UPDATE_TBBROADCAST_Staus_DirectlyCompletedEventArgs e)
        {
            if (GoFlow)
            {
                var result = e.Result;
            }
            else
            {
                if (e.Error == null)
                {
                    CancelMessage(e.Result);
                }
            }
        }

        bool ASK()
        {
            bool result = false;

            MessageBoxResult dialogResult = MessageBox.Show("確定抽單?", "確認視窗", MessageBoxButton.OKCancel);
            if (dialogResult == MessageBoxResult.OK)
            {
                result = true;
            }

            return result;
        }

        void client_GetTBLOG_VIDEO_FileNOList_BYBROIDCompleted(object sender, GetTBLOG_VIDEO_FileNOList_BYBROIDCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                DeleteVideoDataMethod(e.Result.ToList());
            }
        }



        //void client_UPDATE_TBBROADCAST_StausCompleted(object sender, UPDATE_TBBROADCAST_StausCompletedEventArgs e)
        //{
        //    var result = e.Result;
        //}

        //PTS_MAM3.WSDELETE.Class_DELETED _class_deleted = new PTS_MAM3.WSDELETE.Class_DELETED();

        List<PTS_MAM3.WSDELETE.Class_DELETED> _class_deleted_list = new List<WSDELETE.Class_DELETED>();

        void DeleteVideoDataMethod(string _fsfile_no)
        {
            PTS_MAM3.WSDELETE.Class_DELETED _class_deleted = new PTS_MAM3.WSDELETE.Class_DELETED();

            _class_deleted._fsfile_no = _fsfile_no;//_deleted_file_no_list[i];
            _class_deleted._fsfile_type = "V";// this._class_deleted_parameter._fsfile_type;
            _class_deleted._fcstatus = "N";
            _class_deleted._fscreated_by = UserClass.userData.FSUSER_ID;
            _class_deleted._fdcreated_date = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
            _class_deleted._fsmemo = "刪除方式：抽單";
            _class_deleted_list.Add(_class_deleted);
            wsDeleteClient.DeleteFileAsync(_class_deleted_list);
        }

        void DeleteVideoDataMethod(List<string> fileList)
        {
            List<PTS_MAM3.WSDELETE.Class_DELETED> _class_deleted_list = new List<WSDELETE.Class_DELETED>();
            foreach (string str in fileList)
            {
                PTS_MAM3.WSDELETE.Class_DELETED _class_deleted = new PTS_MAM3.WSDELETE.Class_DELETED();

                _class_deleted._fsfile_no = str;//_deleted_file_no_list[i];
                _class_deleted._fsfile_type = "V";// this._class_deleted_parameter._fsfile_type;
                _class_deleted._fcstatus = "N";
                _class_deleted._fscreated_by = UserClass.userData.FSUSER_ID;
                _class_deleted._fdcreated_date = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                _class_deleted._fsmemo = "刪除方式：抽單";
                _class_deleted_list.Add(_class_deleted);
            }
            wsDeleteClient.DeleteFileAsync(_class_deleted_list);
        }

        void wsDeleteClient_DeleteFileCompleted(object sender, WSDELETE.DeleteFileCompletedEventArgs e)
        {
            if (GoFlow)
            {
                CancelMessage(e.Result.Count == 0);
            }
            else
            {
                if (e.Result.Count == 0)
                {
                    client.UPDATE_TBBROADCAST_Staus_DirectlyAsync(class_Broadcast.FSBRO_ID, "X", UserClass.userData.FSUSER_ID);//改變該筆資料在TBBROADCAST中的狀態
                }
                else
                {//有回傳結果代表有發生錯誤，要復原資料

                    wsDeleteClient.Reply_Detached_Error_BatchAsync(_class_deleted_list);
                }
            }
        }

        void wsDeleteClient_Reply_Detached_ErrorCompleted(object sender, WSDELETE.Reply_Detached_ErrorCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {
                    MessageBox.Show("抽單過程發生錯誤，資料已回復!", "提示訊息", MessageBoxButton.OK);
                    Load_TBBROADCAST_ALL_BYUSERID();
                    DetachedBtn.IsEnabled = true;
                }
            }
        }

        void ComClient_GetFlowIDfromFormIDCompleted(object sender, FlowFieldCom.GetFlowIDfromFormIDCompletedEventArgs e)
        {
            //cancelClient.BRO100CancelAsync(((FlowFieldCom.DFWaitToCheck_Main_Class)(DGBroListDouble.SelectedItem)).DF_QueryActivity_Activeid, ((FlowFieldCom.DFWaitToCheck_Main_Class)(DGBroListDouble.SelectedItem)).DF_Form_FormId, UserClass.userData.FSUSER_ID);            
            //cancelClient.BRO100CancelCompleted += (s, args) =>
            //{ CancelMessage(args.Result); };
            cancelClient.BRO100Cancel_2Completed += (s, args) =>
            {
                bool b = args.Result;
                //CancelMessage(args.Result);
            };
            List<PTS_MAM3.FlowFieldCom.Class_FlowIDfromFormID> resault = (List<PTS_MAM3.FlowFieldCom.Class_FlowIDfromFormID>)e.Result;
            //cancelClient.BRO100CancelAsync(resault.FirstOrDefault().ActiveID, resault.FirstOrDefault().FormId, UserClass.userData.FSUSER_ID);
            cancelClient.BRO100Cancel_2Async(resault.FirstOrDefault().ActiveID, resault.FirstOrDefault().FormId, UserClass.userData.FSUSER_ID);
        }

        private void CancelMessage(bool Result)
        {
            this.DetachedBtn.IsEnabled = true;
            if (Result == true)
            {
                Load_TBBROADCAST_ALL_BYUSERID();
                //ShowMainData("P", "", "", 0, 0);
                MessageBox.Show("抽單成功");
            }
            else
            { MessageBox.Show("抽單失敗!"); }

        }
        #endregion

    }
}
