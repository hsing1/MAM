﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace MAM_PTS_DLL
{
    public class CheckFormat
    {
        /// <summary>
        /// 驗證FileID
        /// </summary>
        /// <param name="fileID">首字為英文字母，後帶15個數字</param>
        /// <returns></returns>
        public static bool CheckFormat_FileID(string fileID)
        {
            Regex objPattern = new Regex(@"^[A-Z]\d{15}$");
            return objPattern.IsMatch(fileID, 0);
        }

        /// <summary>
        /// 驗證FileID
        /// 如果格式正確，再拆解成 progID(節目ID)、episodeNo(節目集數)
        /// </summary>
        /// <param name="fileID">首字為英文字母，後帶15個數字</param>
        /// <param name="progID"></param>
        /// <param name="episodeNo"></param>
        /// <returns></returns>
        public static bool CheckFormat_FileID(string fileID, out string progID, out string episodeNo)
        {
            progID = string.Empty;
            episodeNo = string.Empty;

            if (!CheckFormat_FileID(fileID))
                return false;

            progID = fileID.Substring(1, 7);
            episodeNo = fileID.Substring(8, 3);
            return true;
        }

        /// <summary>
        /// 驗證VideoID
        /// </summary>
        /// <param name="videoID">共八或十個字元，內容為A-Z或0-9</param>
        /// <returns></returns>
        public static bool CheckFormat_VideoID(string videoID)
        {
            // 因為目前 VideoID 的定義變了，有可能為八碼或十碼，所以…
            if (videoID.Length == 8 || videoID.Length == 10)
            {
                Regex objPattern = new Regex(@"^[0-9A-Z]+$");
                return objPattern.IsMatch(videoID, 0);
            }
            return false;
        }

        /// <summary>
        /// 驗證是否為合法的UNC路徑
        /// </summary>
        /// <param name="uncPath"></param>
        /// <returns></returns>
        public static bool CheckFormat_UncPath(string uncPath)
        {
            string dummy1, dummy2;

            return CheckFormat_UncPath(uncPath, out dummy1, out dummy2);
        }

        /// <summary>
        /// 驗證是否為合法的UNC路徑，並將解析出來的內容放至變數
        /// </summary>
        /// <param name="uncPath"></param>
        /// <param name="serverName"></param>
        /// <param name="pathName"></param>
        /// <returns></returns>
        public static bool CheckFormat_UncPath(string uncPath, out string serverName, out string pathName)
        {
            serverName = string.Empty;
            pathName = string.Empty;

            Match dataMatch = Regex.Match(uncPath, @"^\\\\(?<ServerName>[0-9a-zA-Z\.]+)\\(?<PathValue>.+)");
            if (!dataMatch.Success)
                return false;

            serverName = dataMatch.Groups["ServerName"].Value;
            pathName = dataMatch.Groups["PathValue"].Value;
            return true;
        }

        /// <summary>
        /// 檢查是否為合法的 TimeCode 格式 (hh:mm:ss;ff)
        /// </summary>
        /// <param name="timeCode"></param>
        /// <returns></returns>
        public static bool CheckFormat_TimeCode(string timeCode)
        {
            string dummy = string.Empty;
            return CheckFormat_TimeCode(timeCode, out dummy, out dummy, out dummy, out dummy);
        }

        /// <summary>
        /// 檢查是否為合法的 TimeCode 格式 (hh:mm:ss;ff)
        /// </summary>
        /// <param name="timeCode"></param>
        /// <returns></returns>
        [Obsolete("請哪位好心的大爺寫一下唷")]
        public static bool CheckFormat_TimeCode(string timeCode, out string hour, out string minute, out string second, out string frame)
        {
            hour = string.Empty;
            minute = string.Empty;
            second = string.Empty;
            frame = string.Empty;

            return true;
        }

        /// <summary>
        /// 驗證是否為合法的TSM路徑，範例：/HSM/ptstv/HVideo/2011/Prog/
        /// </summary>
        /// <param name="tsmPath"></param>
        /// <returns></returns>
        public static bool CheckFormat_TsmPath(string tsmPath)
        {
            Match dataMatch = Regex.Match(tsmPath, @"^/(?<ServerName>[0-9a-zA-Z]+)/(?<PathValue>.+)");
            if (!dataMatch.Success)
                return false;
            return true;
        }

        /// <summary>
        /// 驗證是否為合法的"頻道名稱"，例：pts、hakka
        /// </summary>
        /// <param name="channelName"></param>
        /// <returns></returns>
        public static bool CheckFormat_ChannelName(string channelName)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            // SQL Parameters
            source.Add("FSSHORTNAME", channelName);

            // Receive SQL Result
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            DbAccess.Do_Query("SP_Q_TBZCHANNEL_SHORTNAME", source, out resultData);

            if (resultData.Count > 0 && resultData[0]["RESULT"] == "1")
                return true;

            return false;
        }

        /// <summary>
        /// 檢查 HTTP 路徑是否正確，目前只檢查是否為 http://
        /// </summary>
        /// <param name="httpAddr"></param>
        /// <returns></returns>
        public static bool CheckFormat_HttpAddress(string httpAddr)
        {
            if (string.IsNullOrEmpty(httpAddr) || !httpAddr.StartsWith("http://"))
                return false;
            else
                return true;
        }
    }
}
