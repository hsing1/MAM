﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.Data.SqlClient; 

namespace MAM_PTS_DLL
{
    public  class SysConfig
    {


        /// <summary>
        /// 以single path 讀取系統設定
        /// </summary>
        /// <param name="Path">String(XML path)</param>
        /// <returns>string(設定值)</returns>
        public  string sysConfig_Read(string Path)
        {
            string sRtn = "";
            string FSSYSTEM_CONFIG = ""; 
            SqlDataReader dr;  
            SqlConnection cn = new SqlConnection(DbAccess.sqlConnStr);
            SqlCommand cmd = new SqlCommand("", cn);
            
            try
            {
                cn.Open();
                cmd.CommandText = "SELECT FSSYSTEM_CONFIG FROM TBSYSTEM_CONFIG "; 
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    FSSYSTEM_CONFIG = dr[0].ToString() ; 
                }
                dr.Close(); 
            }
            catch (Exception ex)
            {

            }
            finally
            {               
                cn.Close();
            }

            if (FSSYSTEM_CONFIG != string.Empty)
            {
                XmlDocument xdoc = new XmlDocument();
                xdoc.LoadXml(FSSYSTEM_CONFIG);
                
                XmlNode node = xdoc.SelectSingleNode(Path);
                if (node == null)
                    return "";

                sRtn = node.InnerText; 
            }

            //DbAccess.sqlConnStr 
            return sRtn; 
        }



        /// <summary>
        /// 以Dictonary 傳回系統設定
        /// </summary>
        /// <param name="Path">String(XML Path)</param>
        /// <returns>Dictornary(傳回node name, value)</returns>
        public Dictionary<string, string> sysConfig_Read_From_List(string Path)
        {
            XmlNodeList nodeList = sysConfig_Read_From_NodeList(Path);
            Dictionary<string, string> rtn = new Dictionary<string, string>();
            if (nodeList != null)
                foreach (XmlNode item in nodeList)
                    rtn.Add(item.Name,  item.InnerText);
            return rtn;

            //return rtn; 

            //Dictionary<string, string> rtn = new Dictionary<string,string>();

           
            //string FSSYSTEM_CONFIG = "";
            //SqlDataReader dr;
            //SqlConnection cn = new SqlConnection(DbAccess.sqlConnStr);
            //SqlCommand cmd = new SqlCommand("", cn);

            //try
            //{
            //    cn.Open();
            //    cmd.CommandText = "SELECT FSSYSTEM_CONFIG FROM TBSYSTEM_CONFIG ";
            //    dr = cmd.ExecuteReader();
            //    if (dr.Read())
            //    {
            //        FSSYSTEM_CONFIG = dr[0].ToString();
            //    }
            //    dr.Close();
            //}
            //catch (Exception ex)
            //{

            //}
            //finally
            //{
            //    cn.Close();
            //}

            //if (FSSYSTEM_CONFIG != string.Empty)
            //{
            //    XmlDocument xdoc = new XmlDocument();
            //    xdoc.LoadXml(FSSYSTEM_CONFIG);

            //    XmlNodeList nodeList = xdoc.SelectNodes(Path);
            //    //sRtn = node.InnerText;

            //    foreach (XmlNode item in nodeList)
            //    {
            //        rtn.Add(item.Name,  item.InnerText); 
            //    }
            //}

            //return rtn; 
        }

        /// <summary>
        /// 以Dictonary 傳回系統設定
        /// </summary>
        /// <param name="Path">String(XML Path)</param>
        /// <returns>XmlNodeList</returns>
        public XmlNodeList sysConfig_Read_From_NodeList(string Path)
        {
            Dictionary<string, string> rtn = new Dictionary<string, string>();

            string FSSYSTEM_CONFIG = "";
            SqlDataReader dr;
            SqlConnection cn = new SqlConnection(DbAccess.sqlConnStr);
            SqlCommand cmd = new SqlCommand("", cn);

            try
            {
                cn.Open();
                cmd.CommandText = "SELECT FSSYSTEM_CONFIG FROM TBSYSTEM_CONFIG";
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    FSSYSTEM_CONFIG = dr[0].ToString();
                }
                dr.Close();
            }
            catch
            {
                //
            }
            finally
            {
                cn.Close();
            }

            if (FSSYSTEM_CONFIG != string.Empty)
            {
                XmlDocument xdoc = new XmlDocument();
                xdoc.LoadXml(FSSYSTEM_CONFIG);
                return xdoc.SelectNodes(Path);
            }
            else
            {
                return null;
            }
        }
    }


}
