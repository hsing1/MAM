﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace MAM_PTS_DLL
{

    /// <summary>
    ///  新增與建立TBDirectories And TBSUBJECT 
    /// </summary>
    public class DirAndSubjectAccess
    {

        /// <summary>
        /// 取得節目所屬Directory ID
        /// </summary>
        /// <param name="fschannel_id">頻道ID</param>
        /// <param name="fspgmname">節目名稱</param>
        /// <param name="year">節目所屬年份</param>
        /// <returns>Directory ID</returns>
        public long getProgDirId(string fschannel_id, string fspgmname, string year)
        {
            long rtn = 0;

            rtn = createDir(GetChannelDirID(fschannel_id), year.ToString(), "N");//頻道別要換成DirID
            rtn = createDir(rtn.ToString(), fspgmname, "Y");
            return rtn; 
        }
        
        /// <summary>
        /// 變更Directory 所屬頻道
        /// </summary>
        /// <param name="fsnew_channel_id">新的頻道ID</param>
        /// <param name="fspgmname">節目名稱</param>
        /// <param name="fnold_dir_id">原有節目Dir_Id</param>
        /// <param name="year">節目所屬年份</param>
        /// <returns>新的Directory ID</returns>
        public long chgDirAndChannel(string fsnew_channel_id, string fspgmname, string fnold_dir_id, string year)
        {
            long rtn = 0;

            rtn = createDir(GetChannelDirID(fsnew_channel_id), year.ToString(), "N");//頻道別要換成DirID
            rtn = createDir(rtn.ToString(), fspgmname, "Y");

            chgMediaDir(rtn, fnold_dir_id);  
            return rtn; 
        }

        private void chgMediaDir(long fndir_id, string fnold_dir_id)
        {
            SqlConnection sqlConn = new SqlConnection(DbAccess.sqlConnStr);
            SqlCommand cmd = new SqlCommand("", sqlConn);
            SqlTransaction trans;

            try
            {
                sqlConn.Open();
                trans = sqlConn.BeginTransaction();

                cmd.Transaction = trans;
                try
                {
                    cmd.CommandText = "UPDATE TBSUBJECT SET FNDIR_ID = " + fndir_id + " WHERE FNDIR_ID = " + fnold_dir_id.Replace("'", "''") + " "; 
                    cmd.ExecuteNonQuery();

                    cmd.CommandText = "UPDATE TBLOG_VIDEO SET FNDIR_ID = " + fndir_id + " WHERE FNDIR_ID = " + fnold_dir_id.Replace("'", "''") + " ";
                    cmd.ExecuteNonQuery();

                    cmd.CommandText = "UPDATE TBLOG_VIDEO_D SET FNDIR_ID = " + fndir_id + " WHERE FNDIR_ID = " + fnold_dir_id.Replace("'", "''") + " ";
                    cmd.ExecuteNonQuery();

                    cmd.CommandText = "UPDATE TBLOG_VIDEO_KEYFRAME SET FNDIR_ID = " + fndir_id + " WHERE FNDIR_ID = " + fnold_dir_id.Replace("'", "''") + " ";
                    cmd.ExecuteNonQuery();

                    cmd.CommandText = "UPDATE TBLOG_AUDIO SET FNDIR_ID = " + fndir_id + " WHERE FNDIR_ID = " + fnold_dir_id.Replace("'", "''") + " ";
                    cmd.ExecuteNonQuery();

                    cmd.CommandText = "UPDATE TBLOG_PHOTO SET FNDIR_ID = " + fndir_id + " WHERE FNDIR_ID = " + fnold_dir_id.Replace("'", "''") + " ";
                    cmd.ExecuteNonQuery();

                    cmd.CommandText = "UPDATE TBLOG_DOC SET FNDIR_ID = " + fndir_id + " WHERE FNDIR_ID = " + fnold_dir_id.Replace("'", "''") + " ";
                    cmd.ExecuteNonQuery();

                    cmd.CommandText = "DELETE FROM TBDIRECTORIES WHERE FNDIR_ID = " + fnold_dir_id;
                    cmd.ExecuteNonQuery();     

                    trans.Commit(); 
                }
                catch (Exception ex)
                {
                    cmd.Cancel();
                    trans.Rollback(); 
                }            

            }
            catch (Exception ex)
            {
               
            }

            finally
            {
                sqlConn.Close();
            }


        }
            

        /// <summary>
        /// 檢查與建立TBDirectory
        /// </summary>
        /// <param name="fschannel_id">頻道代碼</param>
        /// <param name="fspgmname">節目名稱/宣傳帶名稱</param>
        /// <param name="fssubject_id">subject_id</param>
        /// <param name="fssubject">節目名稱_子集/宣傳帶名稱</param>
        /// <param name="year">播映年份</param>
        /// <returns></returns>
        public int ChkAndCreateDir(string fschannel_id, string fspgmname,string fssubject_id,string fssubject,  string year)
        {
            int rtn = 0; 
            //SqlConnection sqlConn = new SqlConnection(DbAccess.sqlConnStr);
            //SqlCommand cmd = new SqlCommand("", sqlConn);
            //SqlDataReader dr;


            rtn = createDir(GetChannelDirID(fschannel_id), year.ToString(), "N");//頻道別要換成DirID
            rtn = createDir(rtn.ToString(), fspgmname, "Y");

            chkAndCreateSubject(rtn, fssubject_id, fssubject);  
            return rtn; 
        }


        private void chkAndCreateSubject(int fndir_id, string fssubject_id, string fssubject) 
        {
            SqlConnection sqlConn = new SqlConnection(DbAccess.sqlConnStr);
            SqlCommand cmd = new SqlCommand("", sqlConn);
            SqlDataReader dr;
            Boolean bRtn = false; 



            try
            {
                sqlConn.Open();

                cmd.CommandText = "SELECT COUNT(1) FROM TBSUBJECT WHERE FSSUBJECT_ID = '" + fssubject_id.Replace("'", "''") + "' ";

                dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    if (Convert.ToInt32(dr[0]) == 0)
                        bRtn = false; 
                    else                   
                        bRtn = true;                                      
                }
                else
                    bRtn = false;

                dr.Close(); 

                if (!bRtn)
                {
                    cmd.CommandText = "insert into TBSUBJECT (FSSUBJECT_ID,FSSUBJECT,FNDIR_ID,FSCREATED_BY,FDCREATED_DATE" + ") " + " values ('" + fssubject_id + "','";

                    cmd.CommandText += fssubject.Replace("'", "''") + "', " + fndir_id + ", 'system',getDate() ) ";
                    cmd.ExecuteNonQuery(); 
                }
            }
            catch (Exception ex)
            {
                
            }
            finally
            {
                sqlConn.Close();
            }

        }


        private int createDir(string fnparent_id, string fsdir, string fcqueue)
        {

            int rtn = -1;
            SqlConnection SqlConnection1 = new SqlConnection(DbAccess.sqlConnStr);
            SqlCommand sqlCommand1 = new SqlCommand("", SqlConnection1);
            sqlCommand1.CommandTimeout = 240;
            SqlConnection SqlConnection2 = new SqlConnection(DbAccess.sqlConnStr);
            SqlCommand sqlCommand2 = new SqlCommand("", SqlConnection2);
            sqlCommand2.CommandTimeout = 240;
            SqlDataReader dr1 = default(SqlDataReader);
            SqlDataReader dr2 = default(SqlDataReader);

            sqlCommand1.CommandText = "SELECT FNDIR_ID FROM TBDIRECTORIES WHERE FNPARENT_ID = " + fnparent_id + " AND FSDIR = '" + fsdir + "' ";

            SqlConnection1.Open();
            dr1 = sqlCommand1.ExecuteReader() ;

            if (dr1.Read())
            {
                rtn = Convert.ToInt32(dr1[0]);
                //建立新的Dir
            }
            else
            {
                //取得新的fndir_id
                rtn = getMaxDirId();
                sqlCommand2.CommandText = "INSERT INTO TBDIRECTORIES (FNDIR_ID, FSDIR, FNPARENT_ID,  FCQUEUE, FSCREATED_BY, FDCREATED_DATE) VALUES " + "(" + rtn + ",'" + fsdir + "', " + fnparent_id + ",  '" + fcqueue + "', 'system', getdate()  ) ";
                SqlConnection2.Open();
                sqlCommand2.ExecuteNonQuery();

                //_fndir_id += 1
                SqlConnection2.Close();
            }

            dr1.Close();
            SqlConnection1.Close();

            return rtn;
        }


        public int getMaxDirId()
        {
            SqlConnection SqlConnection1 = new SqlConnection(DbAccess.sqlConnStr);
            SqlCommand sqlCommand1 = new SqlCommand("", SqlConnection1);
            sqlCommand1.CommandTimeout = 240;
            SqlDataReader dr1 = default(SqlDataReader);
            int fndir_id = 0;

            sqlCommand1.CommandText = "SELECT MAX(FNDIR_ID) + 1 FROM TBDIRECTORIES ";

            SqlConnection1.Open();
            dr1 = sqlCommand1.ExecuteReader();
            if (dr1.Read())
            {
                fndir_id = Convert.ToInt32(dr1[0]);
            }
            dr1.Close();
            SqlConnection1.Close();

            return fndir_id;

        }

        //查詢頻道對照的DirectoriesID，這樣建樹才會正確
        public string GetChannelDirID(string strChannelID)
        {
            // SQL Parameters
            Dictionary<string, string> source = new Dictionary<string, string>();
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            source.Add("FSCHANNEL_ID", strChannelID);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBZCHANNEL", source, out resultData))
                return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (resultData.Count > 0)
                return resultData[0]["FNDIR_ID"];
            else
                return "";
        }
        


    }


}
