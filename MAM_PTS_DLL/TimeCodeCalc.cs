﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MAM_PTS_DLL
{
    public static class TimeCodeCalc
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="frames"></param>
        /// <returns></returns>
        public static string frame2timecode(int frames)
        {
            int HH = 0;
            int MM = 0;
            int SS = 0;
            int FF = 0;
            int itmp = 0;

            //frames = frames - 1;
            HH = (int)(Math.Floor((double)frames / (double)107892));
            itmp = frames % 107892;

            //2011/05/05 by Mike
            MM = (int)(Math.Floor((double)itmp / (double)17982)) * 10;
            itmp = itmp % 17982;
            if ((int)itmp > 2)
            {
                MM = MM + (int)(Math.Floor(((double)itmp - (double)2) / (double)1798));
                itmp = (itmp - 2) % 1798;
                itmp = itmp + 2;
            }
            SS = (int)Math.Floor((double)itmp / 30);
            FF = itmp % 30;
            return String.Format("{0:00}", HH) + ":" + String.Format("{0:00}", MM) + ":" + String.Format("{0:00}", SS) + ":" + String.Format("{0:00}", FF);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Timecode"></param>
        /// <returns></returns>
        public static int timecodetoframe(string Timecode)
        {
            int HH = 0;
            int MM = 0;
            int SS = 0;
            int FF = 0;
            int itmp = 0;

            HH = Int32.Parse(Timecode.Substring(0, 2));
            MM = Int32.Parse(Timecode.Substring(3, 2));
            SS = Int32.Parse(Timecode.Substring(6, 2));

            FF = Int32.Parse(Timecode.Substring(9, 2));
            itmp = HH * 107892;
            //itmp = itmp + MM * 1798;
            if (MM >= 50)
            {
                itmp = itmp + (17982 * 5);
                MM = MM - 50;
            }
            else if (MM >= 40)
            {
                itmp = itmp + (17982 * 4);
                MM = MM - 40;
            }
            else if (MM >= 30)
            {
                itmp = itmp + (17982 * 3);
                MM = MM - 30;
            }
            else if (MM >= 20)
            {
                itmp = itmp + (17982 * 2);
                MM = MM - 20;
            }
            else if (MM >= 10)
            {
                itmp = itmp + (17982 * 1);
                MM = MM - 10;
            }
            if (MM > 0)
            {
                itmp = itmp + MM * 1798 + 2;
                itmp = itmp + SS * 30;
                //if (FF >= 2)
                    itmp = itmp + FF + 1 - 2;
            }
            else if (MM == 0)
            {
                itmp = itmp + SS * 30;
                itmp = itmp + FF + 1;
            }
            return itmp -1 ;
        }

        /// <summary>
        /// TimeCode to Second 
        /// </summary>
        /// <param name="Timecode"></param>
        /// <returns></returns>
        public static int timecodetoSecond(string Timecode)
        {
            try
            {
                int HH = 0;
                int MM = 0;
                int SS = 0;

                HH = Int32.Parse(Timecode.Substring(0, 2));
                MM = Int32.Parse(Timecode.Substring(3, 2));
                SS = Int32.Parse(Timecode.Substring(6, 2));

                return HH * 3600 + MM * 60 + SS;
            }
            catch
            {
                return 0;
            }            
        }

        /// <summary>
        /// Second to  TimeCode
        /// </summary>
        /// <param name="seconds"></param>
        /// <returns></returns>
        public static string Secondstotimecode(int seconds)
        {
            int HH = 0;
            int MM = 0;
            int SS = 0;

            int itmp = 0;

            HH = (int)(Math.Floor((double)seconds / (double)3600));
            itmp = seconds - 3600 * HH;

            MM = (int)(Math.Floor((double)itmp / (double)60));

            itmp = itmp - MM * 60;
            SS = itmp;
            return String.Format("{0:00}", HH) + ":" + String.Format("{0:00}", MM) + ":" + String.Format("{0:00}", SS) + ";" + "00";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="MM1"></param>
        /// <returns></returns>
        public static string MMtotimecode(int MM1)
        {
            int HH = 0;
            int MM = 0;

            HH = (int)(Math.Floor((double)MM1 / (double)60));
            MM = MM1 - 60 * HH;
           
            return String.Format("{0:00}", HH) + ":" + String.Format("{0:00}", MM) + ":" + "00" + ";" + "00";
        }

        /// <summary>
        /// 起始TimeCode與結束TimeCode相差計算
        /// </summary>
        /// <param name="BegTimecode">起始TimeCode</param>
        /// <param name="EndTimeCode">結束TimeCode</param>
        /// <returns>相差TimeCode</returns>
        public static string Subtract_timecode(string BegTimecode, string EndTimeCode)
        {
            int intBegFrame = timecodetoframe(BegTimecode);
            int intEndFrame = timecodetoframe(EndTimeCode);

            if (intBegFrame == intEndFrame)
                return "00:00:00;00";
            else if (intBegFrame > intEndFrame)
                return "Error";

            //return frame2timecode(intEndFrame - intBegFrame+1 ); 
            return frame2timecode(intEndFrame - intBegFrame );//20120621 冠宇修正 送帶轉檔 Duration 回傳會多一的問題 
        }

        /// <summary>
        /// 檢查 Timecode 格式
        /// </summary>
        /// <param name="stringHour">時</param>
        /// <param name="stringMinute">分</param>
        /// <param name="stringSecond">秒</param>
        /// <param name="stringFrame">格</param>
        /// <returns>true-正確;false-錯誤</returns>
        //public static Boolean check_timecode_format(string stringHour, string stringMinute, string stringSecond, string stringFrame, out string strError)
        //{
        //    int intHour = Int32.Parse(stringHour);
        //    int intMinute = Int32.Parse(stringMinute);
        //    int intSecond = Int32.Parse(stringSecond);
        //    int intFrame = Int32.Parse(stringFrame);
        //    // 由於前面鎖定為兩位數字，所以這邊出來的結果必不可能為負數，僅需檢查最大值是否有誤
        //    // intHour 可以從 00 - 99，不做限制
        //    if (intMinute > 59)
        //    {
        //        strError = "{分}不得大於59";
        //        return false;
        //    }
        //    if (intSecond > 59)
        //    {
        //        strError = "{秒}不得大於59";
        //        return false;
        //    }
        //    if (intFrame > 29)
        //    {
        //        strError = "{格}不得大於29";
        //        return false;
        //    }

        //    // 接下來檢查 DropFrame 的部份
        //    if (intMinute % 10 > 0 && intSecond == 0 && intFrame < 2)
        //    {
        //        strError = "所輸入的時間不符合DropFrame的格式";
        //        return false;
        //    }
        //    strError = "";
        //    return true;
        //}
    }
}
