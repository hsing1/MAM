﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MAM_PTS_DLL
{
    public static class TimeCalc
    {
        /// <summary>
        /// 將 t1 與 t2 相減後，回傳相差的秒數
        /// </summary>
        /// <param name="t1"></param>
        /// <param name="?"></param>
        /// <param name="?"></param>
        /// <returns></returns>
        public static long TimeSubstract_Second(DateTime t1, DateTime t2)
        {
            return (long)(((TimeSpan)(t1 - t2)).TotalSeconds);
        }
    }
}
