﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;

using System.Net.Mail; 

namespace MAM_PTS_DLL
{
    public static class Protocol
    {
        // 預寫的，不要可以刪掉
        public const string MAIL_SERVER_ADDR = @"mail.pts.org.tw";
        public const string MAIL_SERVER_USER = @"mamsystem";
        public const string MAIL_SERVER_PASS = @"p@ssw0rd";
        public const string MAIL_EMAIL = @"mamsystem@mail.pts.org.tw"; 

        /// <summary>
        /// 系統內建的群組，不可刪，建 enum 可以讓程式在 compiler 時期取得內容
        /// 記得要與資料庫的名稱相符
        /// </summary>
        public enum enumDEFAULT_USERGROUPS { Admins, Everyone };

        /// <summary>
        /// 寄送郵件
        /// </summary>
        /// <param name="mailTo"></param>
        /// <param name="mailHeader"></param>
        /// <param name="mailContent"></param>
        /// <returns></returns>
        public static bool SendEmail(string mailTo, string mailHeader, string mailContent)
        {
            return SendEmail(new List<string>() { mailTo }, mailHeader, mailContent);
        }

        /// <summary>
        /// 寄送郵件
        /// </summary>
        /// <param name="mailTo"></param>
        /// <param name="mailHeader"></param>
        /// <param name="mailContent"></param>
        /// <returns></returns>
        public static bool SendEmail(List<string> mailTo, string mailHeader, string mailContent)
        {
            bool flagErr = false;

            foreach (string m in mailTo)
            {
                try
                {
                    MailAddress mailFrom = new MailAddress(MAIL_EMAIL, "MAM 系統管理帳號", Encoding.UTF8);
                    MailMessage mailMessage = new MailMessage(mailFrom, new MailAddress(m));
                    mailMessage.Subject = string.Concat("數位片庫系統-", mailHeader);
                    mailMessage.SubjectEncoding = Encoding.UTF8;
                    mailMessage.Body = mailContent;
                    mailMessage.BodyEncoding = Encoding.UTF8;
                    mailMessage.IsBodyHtml = true;
                    mailMessage.Priority = MailPriority.High;

                    // SMTP Setting
                    SmtpClient client = new SmtpClient();
                    client.Host = MAIL_SERVER_ADDR;
                    client.Port = 25;
                    client.Credentials = new NetworkCredential(MAIL_SERVER_USER, MAIL_SERVER_PASS);
                    client.Send(mailMessage);

                    mailMessage.Dispose();
                }
                catch (Exception ex)
                {
                    MAM_PTS_DLL.Log.AppendTrackingLog("DLL.SendEmail", Log.TRACKING_LEVEL.ERROR, ex.ToString());
                    flagErr = true;
                }
            }

            return !flagErr;
        }

        /// <summary>
        /// 發送 HTTP POST 訊息
        /// </summary>
        /// <param name="addr"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static bool SendHttpPostMessage(string addr, string msg)
        {
            // 檢查輸入的位址是否正確
            if (!MAM_PTS_DLL.CheckFormat.CheckFormat_HttpAddress(addr))
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("MAM_PTS_DLL/Protocol/SendHttpPostMessage", Log.TRACKING_LEVEL.WARNING, "無效的參數：" + addr);
                return false;
            }

            bool isSuccess = false;
            string errMsg = string.Empty;

            try
            {
                WebRequest objWebRequest = WebRequest.Create(addr);
                objWebRequest.ContentType = "text/plain; charset=UTF-8";
                objWebRequest.Method = "POST";

                byte[] byteArr = System.Text.Encoding.Default.GetBytes(msg);
                objWebRequest.ContentLength = byteArr.Length;

                Stream os = objWebRequest.GetRequestStream();
                os.Write(byteArr, 0, byteArr.Length);
                os.Close();

                // 2012-04-13: Edit by William
                WebResponse objWebResponse = objWebRequest.GetResponse();
                objWebResponse.Close();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                // 讓錯誤的訊息在這邊產生，然後在後面的地方做LOG
                errMsg = string.Concat("addr = ", addr, ", send_msg = " , msg, ", errmsg = ", ex.ToString());
            }

            // 將結果記入資料表
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSADDR", addr);
            sqlParameters.Add("FSCONTENT", msg);
            if (isSuccess)
                sqlParameters.Add("FNSUCCESS", "1");
            else
                sqlParameters.Add("FNSUCCESS", "0");
            sqlParameters.Add("FSERROR_MSG", errMsg);

            // 這邊就不管理是否有成功寫入資料表 ...
            MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBLOG_SEND_HTTPPOST", sqlParameters, "system", false);

            // 
            return isSuccess;
        }

        /// <summary>
        /// 發送 SMS 簡訊，其方法為公視提供，嚴禁拷貝使用！
        /// </summary>
        /// <param name="phoneNo"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static bool SendSMSMessage(string phoneNo, string msg)
        {
            try
            {
                string sHTML = string.Concat("http://imsp.emome.net:8008/imsp/sms/servlet/SubmitSM?account=10414&password=10414&from_addr_type=1&from_addr=10414&to_addr_type=0&msg_type=0&msg_dcs=0&to_addr=",
                    phoneNo, "&msg=", System.Web.HttpUtility.UrlEncode(msg, Encoding.GetEncoding("big5")));
                WebRequest objWebRequest = WebRequest.Create(sHTML);
                Uri ourUri = new Uri(sHTML);
                WebResponse objWebResponse = objWebRequest.GetResponse();
                Stream objStream = objWebResponse.GetResponseStream();
                StreamReader objStreamReader = new StreamReader(objStream, System.Text.Encoding.Default);
                string ret = objStreamReader.ReadToEnd();
                objStreamReader.Dispose();
                objStream.Dispose();
                objWebResponse.Close();

                if (!ret.Contains("Success"))
                    throw new Exception(string.Concat("發送簡訊時，回傳值異常：" + ret));

                return true;
            }
            catch (Exception ex)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("SendSMSMessage", Log.TRACKING_LEVEL.ERROR, ex.ToString());
                return false;
            }
        }
    }
}
