﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;

namespace MAM_PTS_DLL
{
    public static class PhotoSrv
    {
        /// <summary>
        /// .jpeg .bmp .tiff .png .gif -->jpeg
        /// </summary>
        /// <param name="srcFileName">來源檔名 "完整路徑+完整檔名"</param>
        /// <param name="destFileName">目的檔名 "完整路徑+完整檔名"</param>
        /// <param name="nImgSize">影像寬~~~</param>
        /// <returns>true:成功 false:失敗</returns>
        public static Boolean genThumbnailImg(string srcFileName, string destFileName, int nImgSize)
        {
            Boolean bRtn = false;

            try
            {
                if (!System.IO.File.Exists(destFileName))
                {
                    System.IO.File.Delete(destFileName);

                    System.Drawing.Image img = System.Drawing.Imaging.Metafile.FromFile(srcFileName);

                    int sWidth;
                    int sHeight;
                   

                    sWidth = img.Width;
                    sHeight = img.Height; 

                     //if ((sWidth > nImgSize) || (sHeight > nImgSize) ) {
                         int[] r = GetProportionalThumbnailSize(nImgSize, nImgSize, sWidth, sHeight);
                         System.Drawing.Bitmap b = new System.Drawing.Bitmap(r[0], r[1], PixelFormat.Format24bppRgb);
                         Graphics g = Graphics.FromImage(b);
                         g.Clear(Color.White);

                         g.DrawImage(img, 0, 0, r[0], r[1]);

                    

                         b.Save(destFileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                        
                         b.Dispose();
                         img.Dispose();
                     //}


                    bRtn = true;
                }
            }
            catch (Exception ex)
            {

                bRtn = false; 
            }




            return bRtn; 
        }


        public static  int[] GetProportionalThumbnailSize(int MAX_THUMBNAIL_WIDTH, int MAX_THUMBNAIL_HEIGHT, int originalWidth, int originalHeight)
        {


            int newWidth = MAX_THUMBNAIL_WIDTH;
            int newHeight = MAX_THUMBNAIL_HEIGHT;
            //If (originalWidth <= MAX_THUMBNAIL_WIDTH) And (originalHeight <= MAX_THUMBNAIL_HEIGHT) Then
            //    newWidth = originalWidth
            //    newHeight = originalHeight
            //Else

            if (originalWidth > originalHeight)
            {

                newHeight = Convert.ToInt32(Math.Round(Convert.ToDouble(originalHeight * MAX_THUMBNAIL_WIDTH / originalWidth)));
               
            }
            else
            {
                newWidth = Convert.ToInt32(Math.Round(Convert.ToDouble(originalWidth * MAX_THUMBNAIL_HEIGHT / originalHeight)));
            }
            //End If

            return new int[] {
		        newWidth,
		        newHeight
	        };

        }


    }
}
