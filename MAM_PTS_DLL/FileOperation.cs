﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace MAM_PTS_DLL
{
    public class FileOperation
    {
        /// <summary>
        /// 比對檔案大小
        /// </summary>
        /// <param name="fName1"></param>
        /// <param name="fName2"></param>
        /// <returns></returns>
        public static bool CompareFileSize(string fName1, string fName2)
        {
            FileInfo fInfo1 = new FileInfo(fName1);
            FileInfo fInfo2 = new FileInfo(fName2);
            if (fInfo1.Length == fInfo2.Length)
                return true;
            else
                return false;
        }

        /// <summary>
        /// 取得檔案大小，如發生錯誤會回傳 -1
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static long GetFileSize(string fileName)
        {
            try
            {
                if (!File.Exists(fileName))
                    return -1;

                FileInfo file = new FileInfo(fileName);
                return file.Length;
            }
            catch
            {
                return -1;
            }
        }

        /// <summary>
        /// 檢查檔案是否有被上鎖，判斷是否可用
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static bool isFileLocked(string path)
        {
            FileStream f;

            if (!System.IO.File.Exists(path)) { return false; }
            try
            {
                f = System.IO.File.Open(path, FileMode.Open, FileAccess.ReadWrite);
                f.Close();
            }
            catch
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// 依照指定的檔案名稱規範，找出目錄中符合的檔案清單
        /// </summary>
        /// <param name="rootDir"></param>
        /// <param name="pattern">全部的檔案的方式：*，指定副檔名的方式：*.txt</param>
        /// <param name="isRecursive"></param>
        /// <returns></returns>
        public static List<string> GetDirectoryFileList(string rootDir, string pattern, bool isRecursive, out string errMsg)
        {
            List<string> fileList = new List<string>();
            errMsg = string.Empty;

            SearchOption option;
            if (isRecursive)
                option = SearchOption.AllDirectories;
            else
                option = SearchOption.TopDirectoryOnly;

            try
            {
                fileList = new List<string>(System.IO.Directory.GetFiles(rootDir, pattern, option));
            }
            catch (Exception ex)
            {
                errMsg = string.Concat("查詢目錄內的檔案時發生錯誤", Environment.NewLine, "路徑：", rootDir, Environment.NewLine, "訊息：", ex.ToString());
            }

            return fileList;
        }

        /// <summary>
        /// 刪除指定檔案（有自動重試、且不會跳出例外錯誤）
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static bool DeleteFile(string filePath, out string errMsg)
        {
            errMsg = string.Empty;

            // 目前指定最多重試次數為三次
            for (int i = 0; i < 3; i++)
            {
                try
                {
                    if (File.Exists(filePath))
                        File.Delete(filePath);
                    return true;
                }
                catch (Exception ex)
                {
                    errMsg = string.Format("刪除檔案發生錯誤，檔案路徑={0}, 錯誤訊息={1}", filePath, ex.ToString());
                    System.Threading.Thread.Sleep(500);
                }
            }
            return false;
        }

        /// <summary>
        /// 刪除指定目錄及其內容（有自動重試、且不會跳出例外錯誤）
        /// </summary>
        /// <param name="dirPath"></param>
        /// <param name="errMsg"></param>
        /// <returns></returns>
        public static bool DeleteDirectory(string dirPath, out string errMsg)
        {
            errMsg = string.Empty;

            // 目前指定最多重試次數為三次
            for (int i = 0; i < 3; i++)
            {
                try
                {
                    // 檢查要刪除的目錄是否存在
                    if (!Directory.Exists(dirPath))
                        return true;
                    // 刪除目錄
                    Directory.Delete(dirPath, true);
                    // 回傳
                    return true;
                }
                catch (Exception ex)
                {
                    errMsg = string.Format("刪除目錄發生錯誤，目錄路徑={0}, 錯誤訊息={1}", dirPath, ex.ToString());
                    System.Threading.Thread.Sleep(500);
                }
            }
            return false;
        }
    }
}
