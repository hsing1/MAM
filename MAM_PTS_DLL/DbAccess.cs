﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Xml;
using System.IO;

namespace MAM_PTS_DLL
{
    public static class DbAccess
    {
        // 正式環境
        public static string sqlConnStr = @"server='10.13.210.33';uid='mam_admin';pwd='ptsP@ssw0rd';database='MAM'";

        /// <summary>
        /// CreateDoTranscationXml：建立新增用的XML
        /// </summary>
        /// <param name="source">要傳入的參數(Key-Value)</param>
        /// <returns></returns>
        public static string CreateDoTranscationXml(Dictionary<string, string> source)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<Data>");
            foreach (string index in source.Keys)
                //if (string.IsNullOrEmpty(source[index]))
                //    sb.AppendLine(string.Format("<{0}></{0}>", index));
                //else
                    sb.AppendLine(string.Concat("<", index, ">", "<![CDATA[", source[index], "]]>", "</", index, ">"));
                    //sb.AppendLine(string.Format(@"<{0}>{1}</{0}>", index, source[index]));
                    //sb.AppendLine(string.Format(@"<{0}><![CDATA[{1}]]></{0}>", index, source[index]));
                    //sb.AppendLine(string.Format(@"<{0}>{1}</{0}>", index, source[index]));
                    //sb.AppendLine(string.Concat("<",index, "><![CDATA[", source[index], "]]></", index, ">"));
                    //sb.AppendLine(string.Format(@"<{0}><![CDATA[{1}]]></{0}>", index, source[index]));
            sb.AppendLine("</Data>");
            return sb.ToString();
        }

        /// <summary>
        /// Do_Query
        /// </summary>
        /// <param name="strSQL">要查詢的Store Procesure名稱</param>
        /// <param name="parameters">輸入的參數，需組為XML內容(請參考DB_Function_Sample.txt)</param>
        /// <returns></returns>
        public static string Do_Query(string strSQL, string parameters)
        {
            List<Dictionary<string, string>> result = new List<Dictionary<string, string>>();

            Dictionary<string, string> newParameters;
            fnSP_ConverXmlToDict(parameters, out newParameters);

            if (!Do_Query(strSQL, newParameters, out result))
                return "Error: got exception when Do_Query.";

            // 解析得到的內容，並拆成XML做回傳
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<Datas>");
            for (int i = 0; i < result.Count; i++)
            {
                sb.AppendLine("<Data>");
                foreach (string fieldName in result[i].Keys)
                    sb.AppendLine(string.Concat("<", fieldName, ">", "<![CDATA[", result[i][fieldName], "]]>", "</", fieldName, ">"));
                sb.AppendLine("</Data>");
            }
            sb.AppendLine("</Datas>");
            //
            return sb.ToString();
        }

        /// <summary>
        /// Do_Query
        /// </summary>
        /// <param name="strSQL">要查詢的Store Procesure名稱</param>
        /// <param name="inputParameters">輸入的參數，List中的每一個Dict都是一筆資料內容；Dict的每個Key值都是一個欄位</param>
        /// <param name="resultData"></param>
        /// <returns></returns>
        public static bool Do_Query(string strSQL, Dictionary<string, string> inputParameters, out List<Dictionary<string, string>> resultData)
        {
            resultData = new List<Dictionary<string, string>>();

            return fnExecuteQuery(sqlConnStr, strSQL, inputParameters, out resultData);
        }

        /// <summary>
        /// 查詢 FLOW 的資料庫
        /// </summary>
        /// <param name="strSQL">要查詢的Store Procesure名稱</param>
        /// <param name="inputParameters"></param>
        /// <param name="resultData"></param>
        /// <returns></returns>
        public static bool Do_Query_Flow(string strSQL, Dictionary<string, string> inputParameters, out List<Dictionary<string, string>> resultData)
        {
            resultData = new List<Dictionary<string, string>>();

            SysConfig obj = new SysConfig();

            return fnExecuteQuery(obj.sysConfig_Read("/ServerConfig/FLOW_Conn_Str"), strSQL, inputParameters, out resultData);
        }

        /// <summary>
        /// Do_Transaction
        /// </summary>
        /// <param name="spName">要查詢的Store Procesure名稱</param>
        /// <param name="parameters">輸入的參數，需組為XML內容(請參考DB_Function_Sample.txt)</param>
        /// <returns></returns>
        public static bool Do_Transaction(string spName, string parameters, string userID, bool doTranLog = true)
        {
            if (string.IsNullOrEmpty(spName) || string.IsNullOrEmpty(parameters) || string.IsNullOrEmpty(userID))
                return false;

            // 參數剖析
            Dictionary<string, string> newParameters;
            fnSP_ConverXmlToDict(parameters, out newParameters);

            return Do_Transaction(spName, newParameters, userID, doTranLog);
        }

        /// <summary>
        /// Do_Transaction
        /// </summary>
        /// <param name="spName">要查詢的Store Procesure名稱</param>
        /// <param name="parameters">輸入的參數，以Key-Value的方式呈現</param>
        /// <returns></returns>
        public static bool Do_Transaction(string spName, Dictionary<string, string> parameters, string userID, bool doTranLog = true)
        {
            string exceptionMsg = string.Empty;

            if (string.IsNullOrEmpty(spName) || parameters == null || string.IsNullOrEmpty(userID))
            {
                // 當有人傳入的參數錯誤時，寫入文字檔記錄
                string logFileName = string.Concat(@"D:\LogFiles\DB_W-", DateTime.Now.ToString("yyyyMMdd"), ".txt");
                if (!string.IsNullOrEmpty(spName))
                    MAM_PTS_DLL.Log.WriteTxtLog(logFileName, "Got Error Input. spName = " + spName, "DBERROR");
                else
                    MAM_PTS_DLL.Log.WriteTxtLog(logFileName, "Got Error Input. spName = empty", "DBERROR");
                return false;
            }

            return fnExecuteTransaction(sqlConnStr, sqlConnStr, spName, parameters, out exceptionMsg, userID, doTranLog, true);
        }

        /// <summary>
        /// Do_Transaction
        /// </summary>
        /// <param name="spName">要查詢的Store Procesure名稱</param>
        /// <param name="parameters">輸入的參數，以Key-Value的方式呈現</param>
        /// <returns></returns>
        public static bool Do_Transaction(string spName, Dictionary<string, string> parameters, out string exceptionMsg, string userID, bool doTranLog = true)
        {
            exceptionMsg = string.Empty;

            if (string.IsNullOrEmpty(spName) || parameters == null || string.IsNullOrEmpty(userID))
            {
                // 當有人傳入的參數錯誤時，寫入文字檔記錄
                string logFileName = string.Concat(@"D:\LogFiles\DB_W-", DateTime.Now.ToString("yyyyMMdd"), ".txt");
                if (!string.IsNullOrEmpty(spName))
                    MAM_PTS_DLL.Log.WriteTxtLog(logFileName, "Got Error Input. spName = " + spName, "DBERROR");
                else
                    MAM_PTS_DLL.Log.WriteTxtLog(logFileName, "Got Error Input. spName = empty", "DBERROR");
                return false;
            }

            return fnExecuteTransaction(sqlConnStr, sqlConnStr, spName, parameters, out exceptionMsg, userID, doTranLog, true);
        }

        /// <summary>
        /// 當 StoreProcedure 使用 DBLink 的時候，要用這個函式
        /// </summary>
        /// <param name="spName"></param>
        /// <param name="parameters"></param>
        /// <param name="userID"></param>
        /// <param name="doTranLog"></param>
        /// <returns></returns>
        public static bool Do_Transaction_With_DBLink(string spName, Dictionary<string, string> parameters, out string exceptionMsg, string userID, bool doTranLog = true)
        {
            exceptionMsg = string.Empty;

            if (string.IsNullOrEmpty(spName) || parameters == null || string.IsNullOrEmpty(userID))
            {
                // 當有人傳入的參數錯誤時，寫入文字檔記錄
                string logFileName = string.Concat(@"D:\LogFiles\DB_W-", DateTime.Now.ToString("yyyyMMdd"), ".txt");
                if (!string.IsNullOrEmpty(spName))
                    MAM_PTS_DLL.Log.WriteTxtLog(logFileName, "Got Error Input. spName = " + spName, "DBERROR");
                else
                    MAM_PTS_DLL.Log.WriteTxtLog(logFileName, "Got Error Input. spName = empty", "DBERROR");
                return false;
            }

            return fnExecuteTransaction(sqlConnStr, sqlConnStr, spName, parameters, out exceptionMsg, userID, doTranLog, false);
        }

        /// <summary>
        /// Do_Transaction to News
        /// </summary>
        /// <param name="spName">要查詢的Store Procesure名稱</param>
        /// <param name="parameters">輸入的參數，需組為XML內容(請參考DB_Function_Sample.txt)</param>
        /// <returns></returns>
        public static bool Do_Transaction_News(string spName, string parameters, string userID, bool doTranLog = true)
        {
            if (string.IsNullOrEmpty(spName) || string.IsNullOrEmpty(parameters) || string.IsNullOrEmpty(userID))
                return false;

            // 參數剖析
            Dictionary<string, string> newParameters;
            fnSP_ConverXmlToDict(parameters, out newParameters);

            return Do_Transaction_News(spName, newParameters, userID, doTranLog);
        }

        /// <summary>
        /// Do_Transaction
        /// </summary>
        /// <param name="spName">要查詢的Store Procesure名稱</param>
        /// <param name="parameters">輸入的參數，以Key-Value的方式呈現</param>
        /// <returns></returns>
        public static bool Do_Transaction_News(string spName, Dictionary<string, string> parameters, string userID, bool doTranLog = true)
        {
            string exceptionMsg = string.Empty;

            if (string.IsNullOrEmpty(spName) || parameters == null || string.IsNullOrEmpty(userID))
                return false;

            SysConfig obj = new SysConfig();

            return fnExecuteTransaction(obj.sysConfig_Read("/ServerConfig/NEWS_Conn_Str"), sqlConnStr, spName, parameters, out exceptionMsg, userID, doTranLog, true);
        }

        /// <summary>
        /// 實際執行 DoQuery 的底層函式
        /// </summary>
        /// <param name="connString"></param>
        /// <param name="strSQL"></param>
        /// <param name="inputParameters"></param>
        /// <param name="resultData"></param>
        /// <returns></returns>
        private static bool fnExecuteQuery(string connString, string strSQL, Dictionary<string, string> inputParameters, out List<Dictionary<string, string>> resultData)
        {
            resultData = new List<Dictionary<string, string>>();

            if (inputParameters == null)
                return false;

            SqlConnection connection = new SqlConnection(connString);
            SqlDataAdapter da = new SqlDataAdapter(strSQL, connection);
            DataTable dt = new DataTable();
            StringBuilder sb = new StringBuilder();

            // 加入參數
            fnSP_AddDataAdapterParameters(ref da, inputParameters);

            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            try
            {
                // 檢查SQL連線狀態
                if (connection.State == System.Data.ConnectionState.Closed)
                    connection.Open();

                da.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    // 處理每一筆資料
                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        Dictionary<string, string> tmpDict = new Dictionary<string, string>();
                        // 處理每一個欄位
                        for (int j = 0; j <= dt.Columns.Count - 1; j++)
                            if (Convert.IsDBNull(dt.Rows[i][j]))
                                tmpDict.Add(dt.Columns[j].ColumnName, string.Empty);
                            else
                                tmpDict.Add(dt.Columns[j].ColumnName, dt.Rows[i][j].ToString());
                        resultData.Add(tmpDict);
                    }
                }
            }
            catch (Exception ex)
            {
                string logFileName = string.Concat(@"D:\LogFiles\DB_R-", DateTime.Now.ToString("yyyyMMdd"), ".txt");
                StringBuilder outputLog = new StringBuilder();
                outputLog.AppendLine(string.Format("執行{0}發生錯誤", strSQL));
                outputLog.AppendLine("參數：");
                foreach (string index in inputParameters.Keys)
                    outputLog.AppendLine(string.Format("{0}->{1}", index, inputParameters[index]));
                outputLog.AppendLine("錯誤訊息：");
                outputLog.AppendLine(ex.ToString());
                MAM_PTS_DLL.Log.WriteTxtLog(logFileName, outputLog.ToString(), "DBERROR");
                return false;
            }
            finally
            {
                try
                {
                    connection.Close();
                    connection.Dispose();
                }
                catch
                {
                    //
                }
                GC.Collect();
            }
            return true;
        }

        /// <summary>
        /// 實際處理 Do_Transaction 的地方，配合新的需求
        /// </summary>
        /// <param name="connStringExecute">要執行SQL語法的「SQL連接字串」</param>
        /// <param name="connStringLog">要執行TranLog語法的「SQL連接字串」</param>
        /// <param name="spName">要執行的預存程序名稱</param>
        /// <param name="parameters">預存程序的參數及其值</param>
        /// <param name="exceptionMsg">發生錯誤時，會回寫的錯誤訊息</param>
        /// <param name="userID">執行該筆交易的使用者ID</param>
        /// <param name="doTransLog">是否要將執行的內容寫入TransLog資料表</param>
        /// <param name="doTransaction">是否要使用Transaction的功能(失敗時會自動Rollback)</param>
        /// <returns></returns>
        private static bool fnExecuteTransaction(string connStringExecute, string connStringLog, string spName, Dictionary<string, string> parameters, out string exceptionMsg, string userID, bool doTransLog, bool doTransaction)
        {
            // 如果更新資料庫失敗了，還去 Call 資料庫記錄失敗，這是件不安全的事情，所以設定在本機端做一個 Log 檔
            string logFileName = string.Concat(@"D:\LogFiles\DB_W-", DateTime.Now.ToString("yyyyMMdd"), ".txt");

            // 當發生錯誤時要回寫的錯誤訊息
            exceptionMsg = string.Empty;

            SqlConnection connExecute = new SqlConnection(connStringExecute);
            SqlConnection connLog = new SqlConnection(connStringLog);
            
            SqlCommand sqlCmd = new SqlCommand(spName, connExecute);
            sqlCmd.CommandTimeout = 180;    //因為有時候會太久，因此DB的連線等待改為180秒
            SqlCommand sqlCmdLog = new SqlCommand("SP_I_TBTRAN_LOG", connLog);      // 爛寫法，不過先將就一下

            SqlTransaction tran = null;

            // 準備 sqlcmdLog 要用的內容
            Dictionary<string, string> inputParameters = new Dictionary<string, string>();
            inputParameters.Add("FSUSER_ID", userID);
            inputParameters.Add("FDDATE", System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            inputParameters.Add("FSSP_NAME", spName);
            inputParameters.Add("FSPARAMETER", fnSP_CombineTBTRAN_LOG_FSPARAMETER(parameters));

            // 將SqlCommand物件加入SP的參數
            fnSP_AddSqlCommandParameters(ref sqlCmd, parameters);
            fnSP_AddSqlCommandParameters(ref sqlCmdLog, inputParameters);

            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmdLog.CommandType = CommandType.StoredProcedure;

            try
            {
                // 開啟與SQL主機的連線
                connExecute.Open();

                // 判斷是否需要SqlTransaction的保護機制
                if (doTransaction)
                {
                    tran = connExecute.BeginTransaction();
                    sqlCmd.Transaction = tran;
                    sqlCmd.ExecuteNonQuery();
                    tran.Commit();
                    tran.Dispose();
                    tran = null;
                } else
                    sqlCmd.ExecuteNonQuery();

                // 成功後，將執行的SP內容也寫入資料表做稽核
                if (doTransLog)
                {
                    connLog.Open();
                    sqlCmdLog.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                // 回寫錯誤資訊
                exceptionMsg = ex.Message;
                // 寫入記錄檔
                StringBuilder outputLog = new StringBuilder();
                outputLog.AppendLine(string.Format("執行{0}發生錯誤", spName));
                outputLog.AppendLine("參數：");
                foreach (string index in parameters.Keys)
                    outputLog.AppendLine(string.Format("{0}->{1}", index, parameters[index]));
                outputLog.AppendLine("錯誤訊息：");
                outputLog.AppendLine(ex.ToString());
                MAM_PTS_DLL.Log.WriteTxtLog(logFileName, outputLog.ToString(), "DBERROR");
                // RollBack
                if (tran != null)
                {
                    try
                    {
                        tran.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        string errMsg2 = string.Concat("Rollback SQL Command Error, Command = ", spName, ", ErrMsg = ", Environment.NewLine, ex2.ToString());
                        MAM_PTS_DLL.Log.WriteTxtLog(logFileName, errMsg2, "DBERROR");
                    }
                }
                // 
                return false;
            }
            finally
            {
                // 
                if (connLog.State == ConnectionState.Open)
                {
                    try
                    {
                        connLog.Close();
                    }
                    catch (Exception ex)
                    {
                        string errMsg = string.Concat("connLog.Close() got exception: ", ex.ToString());
                        MAM_PTS_DLL.Log.WriteTxtLog(logFileName, errMsg, "DBERROR");
                    }
                }
                else if (connLog.State == ConnectionState.Closed)
                {
                    // Do Nothing
                }
                else
                {
                    // 如果到這的狀態不是 Open 的話，就記錄一下吧
                    string errMsg = string.Concat("connLog got unexpected state: ", connLog.State.ToString());
                    MAM_PTS_DLL.Log.WriteTxtLog(logFileName, errMsg, "DBERROR");
                }
                // 
                if (connExecute.State == ConnectionState.Open)
                {
                    try
                    {
                        connExecute.Close();
                    }
                    catch (Exception ex)
                    {
                        string errMsg = string.Concat("connExecute.Close() got exception: ", ex.ToString());
                        MAM_PTS_DLL.Log.WriteTxtLog(logFileName, errMsg, "DBERROR");
                    }
                }
                else if (connLog.State == ConnectionState.Closed)
                {
                    // Do Nothing
                }
                else
                {
                    // 如果到這的狀態不是 Open 的話，就記錄一下吧
                    string errMsg = string.Concat("connExecute got unexpected state: ", connExecute.State.ToString());
                    MAM_PTS_DLL.Log.WriteTxtLog(logFileName, errMsg, "DBERROR");
                }
                // 
                GC.Collect();
            }

            //
            return true;
        }

        /// <summary>
        /// 組合 TBTRAN_LOG_的 FSPARAMETER 欄位內容
        /// </summary>
        /// <returns></returns>
        private static string fnSP_CombineTBTRAN_LOG_FSPARAMETER(Dictionary<string, string> input)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string fieldName in input.Keys)
                sb.AppendLine(string.Concat(fieldName, ": ", input[fieldName]));
            return sb.ToString();
        }

        private static bool fnSP_AddDataAdapterParameters(ref SqlDataAdapter dataAdapterObj, Dictionary<string, string> parameters)
        {
            if (parameters == null)
                return false;
            //
            foreach (string strFiledName in parameters.Keys)
                if (!string.IsNullOrEmpty(strFiledName))
                    //dataAdapterObj.SelectCommand.Parameters.Add(string.Concat("@", strFiledName), parameters[strFiledName]);
                    dataAdapterObj.SelectCommand.Parameters.AddWithValue(string.Concat("@", strFiledName), parameters[strFiledName]);
            return true;
        }

        private static bool fnSP_AddSqlCommandParameters(ref SqlCommand sqlCmdObj, Dictionary<string, string> parameters)
        {
            if (parameters == null)
                return false;
            // 目前先只處理單筆
            foreach (string strFiledName in parameters.Keys)
            {
                if (!string.IsNullOrEmpty(strFiledName))
                {
                    string spVarName = string.Concat("@", strFiledName);
                    //sqlCmdObj.Parameters.Add(spVarName, parameters[strFiledName]);
                    sqlCmdObj.Parameters.AddWithValue(spVarName, parameters[strFiledName]);
                }
            }
            //
            return true;
        }

        // 目前只支援一層的XML輸入
        private static bool fnSP_ConverXmlToDict(string xmlData, out Dictionary<string, string> resultData)
        {
            resultData = new Dictionary<string, string>();

            // 參數剖析
            XmlDocument xmldoc = new XmlDocument();
            try
            {
                xmldoc.LoadXml(xmlData);
            }
            catch (Exception ex)
            {
                // 傳入的 parameters 不合XML架構
                //Log.AppendTrackingLog("DbFuncException", Log.TRACKING_LEVEL.ERROR, string.Format("source:{0}, parameter:{1}, error:{2}", strSQL, parameters, e.ToString()));
                // DB錯誤交由 TXT 來LOG
                string logFileName = string.Concat(@"D:\LogFiles\DB_ConvertErr-", DateTime.Now.ToString("yyyyMMdd"), ".txt");
                MAM_PTS_DLL.Log.WriteTxtLog(xmlData, ex.ToString(), "DBERROR");
                return false;
            }

            // 解析XML內容
            XmlNode xmlNode = xmldoc.FirstChild;
            // 單筆資料的結構
            for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
            {
                // 這樣碰到Key為 string.empty 的東西時可能會掛掉 =_=
                if (xmlNode.ChildNodes[i].FirstChild == null || string.IsNullOrEmpty(xmlNode.ChildNodes[i].FirstChild.Value))
                {
                    // 爛寫法，不過先將就
                    if (resultData.ContainsKey(xmlNode.ChildNodes[i].Name))
                        continue;
                    resultData.Add(xmlNode.ChildNodes[i].Name, string.Empty);
                }
                else
                {
                    // 爛寫法，不過先將就
                    if (resultData.ContainsKey(xmlNode.ChildNodes[i].Name))
                        continue;
                    string msg = xmlNode.ChildNodes[i].FirstChild.Value;
                    // 檢查 msg 的開頭跟結尾是否為 xml 的特殊字，要濾掉
                    if (msg.StartsWith("<![CDATA[") && msg.EndsWith("]]>"))
                        msg = msg.Substring(9, (msg.Length - 12));
                    resultData.Add(xmlNode.ChildNodes[i].Name, msg);
                }
            }

            //
            return true;
        }
    }
}