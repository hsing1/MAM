﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace MAM_PTS_DLL
{
    class File
    {
        public static bool CompareFileSize(string fName1, string fName2)
        {
            FileInfo fInfo1 = new FileInfo(fName1);
            FileInfo fInfo2 = new FileInfo(fName2);
            if (fInfo1.Length == fInfo2.Length)
                return true;
            else
                return false;
        }

        // 檢查檔案是否有被上鎖，判斷是否可用
        public static bool isFileLocked(string path)
        {
            FileStream f;

            if (!System.IO.File.Exists(path)) { return false; }
            try
            {
                f = System.IO.File.Open(path, FileMode.Open, FileAccess.ReadWrite);
                f.Close();
            }
            catch
            {
                return true;
            }
            return false;
        }
    }
}
