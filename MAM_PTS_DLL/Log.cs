﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MAM_PTS_DLL
{
    public class Log
    {
        // 如果有其他層級，可以在這邊增加
        public enum TRACKING_LEVEL { TRACE, DEBUG, INFO, WARNING, ERROR, FATALERROR };

        public static bool AppendTrackingLog(string programFile, TRACKING_LEVEL trackingLevel, string trackingMessage)
        {
            return AppendTrackingLog(programFile, trackingLevel.ToString(), trackingMessage);
        }

        public static bool AppendTrackingLog(string programFile, string trackingLevel, string trackingMessage)
        {
            // SQL Parameters
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROGRAM", programFile);
            source.Add("FSCATALOG", trackingLevel);
            source.Add("FSMESSAGE", trackingMessage);
            // Do Transcation
            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBTRACKINGLOG", source, "system", false);
        }

        #region Write_Txt_Log
        public static bool WriteTxtLog(string filePath, string fileContent, string logStatus = "NoDefine")
        {
            // 建立LOG檔所需的目錄，不然後面的AppendLog會有Exception出來
            try
            {
                // 沒目錄的建目錄
                if (!Directory.Exists(Path.GetDirectoryName(filePath)))
                    Directory.CreateDirectory(Path.GetDirectoryName(filePath));
                // 沒檔案的建檔案
                if (!File.Exists(filePath))
                {
                    FileStream fileObj = File.Create(filePath);
                    fileObj.Close();
                }
            }
            catch
            {
                // 連 SystemIO 都錯，真的沒解了 ... 
                return false;
            }

            try
            {
                // 寫入LOG檔
                StreamWriter sw = new StreamWriter(filePath, true);
                sw.WriteLine(@"---------------------");
                sw.WriteLine(string.Concat("[", logStatus, "] ", DateTime.Now.ToString()));
                sw.WriteLine(fileContent);
                sw.Flush();
                sw.Close();
            }
            catch
            {
                // 連 SystemIO 都錯，真的沒解了 ... 
                return false;
            }

            // 
            return true;
        }

        // 在 List<string> 中的每個成員都是一行內容
        public static bool WriteTxtLog(string filePath, List<string> fileContent, string logStatus = null)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string line in fileContent)
                sb.AppendLine(line);
            return WriteTxtLog(filePath, sb.ToString(), logStatus);
        }
        #endregion
    }
}
