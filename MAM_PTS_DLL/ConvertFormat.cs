﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MAM_PTS_DLL
{
    public class ConvertFormat
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string Convert_TimeCode_to_MasterControlDec(string input)
        {
            return string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string Convert_MasterControlDec_to_TimeCode(string input)
        {
            return string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sysCode"></param>
        /// <param name="funcName"></param>
        /// <returns></returns>
        public static string Combine_TraceLogFSProgram(string sysCode, string funcName)
        {
            return string.Concat(sysCode, "/", funcName);
        }

        /// <summary>
        /// 將 List Of String 轉成 String
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string ConvertListStringToString(List<string> source)
        {
            if (source == null || source.Count == 0)
                return string.Empty;

            StringBuilder sb = new StringBuilder();
            foreach (string s in source)
                sb.AppendLine(s);
            return sb.ToString();
        }

        /// <summary>
        /// 置換 Xml 的特殊字元
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string ReplaceXmlSpecialCharacters(string source)
        {
            // 空字串就不處理了
            if (string.IsNullOrEmpty(source))
                return string.Empty;

            // 記得 & 的置換要放最前面
            return source.Replace("&", "&amp;").Replace("'", "&apos;").Replace("\"", "&quot;").Replace("<", "&lt;").Replace(">", "&gt;");
        }
    }
}