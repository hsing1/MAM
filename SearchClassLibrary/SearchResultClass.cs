﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SearchClassLibrary
{
    public class SearchResultClass
    {
        private string _fssys_id;
        private string _fskey_word;
        private string _fsfile_no;
        private string _fstitle;
        private string _fscontent;
        private string _fdcreated_date;
        private string _fsimage_path;
        private string _fsvwname;
        private string _fileno_vwname;
        private bool _fcfrom;

        public string _fsprog_name { get; set; }
        public string _fnepisode { get; set; }
        public string _fsarc_type { get; set; }
        public string _fsprog_sub_name { get; set; }

        public int _view_metadata_height { set; get; }

        public string _fstrack { set; get; }

        public bool _fbis_check { set; get; }

        public string FSSYS_ID
        {
            get { return this._fssys_id; }
            set
            {
                if (value != this._fssys_id)
                {
                    _fssys_id = value;
                }
            }
        }

        public string FSKEY_WORD
        {
            get { return this._fskey_word; }
            set
            {
                if (value != this._fskey_word)
                {
                    _fskey_word = value;
                }
            }
        }

        public string FSFILE_NO
        {
            get { return this._fsfile_no; }
            set
            {
                if (value != this._fsfile_no)
                {
                    this._fsfile_no = value;
                }
            }
        }

        public string FSTITLE
        {
            get { return this._fstitle; }
            set
            {
                if (value != this._fstitle)
                {
                    this._fstitle = value;
                }
            }
        }


        public string FSCONTENT
        {
            get { return this._fscontent; }
            set
            {
                if (value != this._fscontent)
                {
                    this._fscontent = value;
                }
            }
        }

        public string FDCREATED_DATE
        {
            get { return this._fdcreated_date; }
            set
            {
                if (value != this._fdcreated_date)
                {
                    this._fdcreated_date = value;
                }
            }
        }

       
        public string FSIMAGE_PATH
        {
            get { return this._fsimage_path; }
            set
            {
                if (value != this._fsimage_path)
                {
                    this._fsimage_path = value;
                }
            }
        }

        public string FSVWNAME
        {
            get { return this._fsvwname; }
            set
            {
                if (value != this._fsvwname)
                {
                    this._fsvwname = value;
                }
            }
        }

        public string FSFILENO_VWNAME
        {
            get { return this._fileno_vwname; }
            set
            {
                if (value != this._fileno_vwname)
                {
                    this._fileno_vwname = value;
                }
            }
        }

        public bool FCFROM
        {
            get { return this._fcfrom; }
            set
            {
                if (value != this._fcfrom)
                {
                    this._fcfrom = value;
                }
            }
        }

        public bool FBIS_CHECK
        {
            get { return this._fbis_check; }
            set
            {
                if (value != this._fbis_check)
                {
                    this._fbis_check = value;
                }
            }
        }
    }
}
