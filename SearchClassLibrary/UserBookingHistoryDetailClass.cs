﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SearchClassLibrary
{
    public class UserBookingHistoryDetailClass
    {
        private string _fsseq;
        private string _fsfile_no;
        private string _fstitle;
        private string _fspgmname;
        private string _fstime_code;
        private string _fsfile_category;
        private string _fscheck_status;
        private string _fscheck_id;
        private string _fdcheck_date;
        private string _fcsupervisor;
        private string _fssupervisor_check_status;
        private string _fssupervisor_check_id;
        private string _fdsupervisor_check_date;
        private string _fcfile_status;
        private string _fcfile_transcode_status;


        public string FSSEQ
        {
            get { return this._fsseq; }
            set
            {
                if (value != this._fsseq)
                {
                    this._fsseq = value;
                }
            }
        }
        
        public string FSFILE_NO
        {
            get { return this._fsfile_no; }
            set
            {
                if (value != this._fsfile_no)
                {
                    this._fsfile_no = value;
                }
            }
        }

        public string FSTITLE
        {
            get { return this._fstitle; }
            set
            {
                if (value != this._fstitle)
                {
                    this._fstitle = value;
                }
            }
        }

        public string FSPGMNAME
        {
            get { return this._fspgmname; }
            set
            {
                if (value != this._fspgmname)
                {
                    this._fspgmname = value;
                }
            }
        }

        public string FSTIME_CODE
        {
            get { return this._fstime_code; }
            set
            {
                if (value != this._fstime_code)
                {
                    this._fstime_code = value;
                }
            }
        }

        public string FSFILE_CATEGORY
        {
            get { return this._fsfile_category; }
            set
            {
                if (value != this._fsfile_category)
                {
                    this._fsfile_category = value;
                }
            }
        }

        public string FSCHECK_STATUS
        {
            get { return this._fscheck_status; }
            set
            {
                if (value != this._fscheck_status)
                {
                    this._fscheck_status = value;
                }
            }
        }

        public string FSCHECK_ID
        {
            get { return this._fscheck_id; }
            set
            {
                if (value != this._fscheck_id)
                {
                    this._fscheck_id = value;
                }
            }
        }

        public string FDCHECK_DATE
        {
            get { return this._fdcheck_date; }
            set
            {
                if (value != this._fdcheck_date)
                {
                    this._fdcheck_date = value;
                }
            }
        }

        
        public string FCSUPERVISOR
        {
            get { return this._fcsupervisor; }
            set
            {
                if (value != this._fcsupervisor)
                {
                    this._fcsupervisor = value;
                }
            }
        }

        public string FSSUPERVISOR_CHECK_STATUS
        {
            get { return this._fssupervisor_check_status; }
            set
            {
                if (value != this._fssupervisor_check_status)
                {
                    this._fssupervisor_check_status = value;
                }
            }
        }

        public string FSSUPERVISOR_CHECK_ID
        {
            get { return this._fssupervisor_check_id; }
            set
            {
                if (value != this._fssupervisor_check_id)
                {
                    this._fssupervisor_check_id = value;
                }
            }
        }

        public string FDSUPERVISOR_CHECK_DATE
        {
            get { return this._fdsupervisor_check_date; }
            set
            {
                if (value != this._fdsupervisor_check_date)
                {
                    this._fdsupervisor_check_date = value;
                }
            }
        }


        public string FCFILE_STATUS
        {
            get { return this._fcfile_status; }
            set
            {
                if (value != this._fcfile_status)
                {
                    this._fcfile_status = value;
                }
            }
        }

        public string FCFILE_TRANSCODE_STATUS
        {
            get { return this._fcfile_transcode_status; }
            set
            {
                if (value != this._fcfile_transcode_status)
                {
                    this._fcfile_transcode_status = value;
                }
            }
        }
    }
}
