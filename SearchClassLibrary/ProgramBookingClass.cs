﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SearchClassLibrary
{
    public class ProgramBookingClass
    {
        private string _fsprebooking_no;
        private string _fsseq;
        private string _fsfile_no;
        private string _fsprogname;
        private string _fsfile_type;
        private string _fsfile_category;
        private string _fsfileno_category;

        public string FSPREBOOKING_NO
        {
            get { return this._fsprebooking_no; }
            set
            {
                if (value != this._fsprebooking_no)
                {
                    this._fsprebooking_no = value;
                }
            }
        }

        public string FSSEQ
        {
            get { return this._fsseq; }
            set
            {
                if (value != this._fsseq)
                {
                    this._fsseq = value;
                }
            }
        }

        public string FSFILE_NO
        {
            get { return this._fsfile_no; }
            set
            {
                if (value != this._fsfile_no)
                {
                    this._fsfile_no = value;
                }
            }
        }

        public string FSPROGNAME
        {
            get { return this._fsprogname; }
            set
            {
                if (value != this._fsprogname)
                {
                    this._fsprogname = value;
                }
            }
        }

        
        public string FSFILE_TYPE
        {
            get { return this._fsfile_type; }
            set
            {
                if (value != this._fsfile_type)
                {
                    this._fsfile_type = value;
                }
            }
        }

        public string FSFILE_CATEGORY
        {
            get { return this._fsfile_category; }
            set
            {
                if (value != this._fsfile_category)
                {
                    this._fsfile_category = value;
                }
            }
        }

        public string FSFILENO_CATEGORY
        {
            get { return this._fsfileno_category; }
            set
            {
                if (value != this._fsfileno_category)
                {
                    this._fsfileno_category = value;
                }
            }
        }
    }
}
