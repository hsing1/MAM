﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SearchClassLibrary
{
    public class PreBookingClass
    {
        private string _fnno;
        private string _fsuserid;
        private string _fcsend;
        private string _fsfile_no;
        private string _fstitle;
        private string _fsstart_timecode;
        private string _fsbeg_timecode;
        private string _fsend_timecode;
        private string _fsfile_type;
        private string _fsfile_category;
        private string _fsfile_category_name;
        private string _fnlocation;
        private string _fsimagepath;
        private string _fsfileno_category;
        private string _fdprebooking_date;
        private string _fcout_buy;
        private string _fsguid;
        private string _fsprog_name;
        private bool _fbis_check;
        public string _fsarc_type { get; set; }


        public string FNNO
        {
            get { return this._fnno; }
            set
            {
                if (value != this._fnno)
                {
                    this._fnno = value;
                }
            }
        }

        public string FSUSER_ID
        {
            get { return this._fsuserid; }
            set
            {
                if (value != this._fsuserid)
                {
                    this._fsuserid = value;
                }
            }
        }


        public string FCSEND
        {
            get { return this._fcsend; }
            set
            {
                if (value != this._fcsend)
                {
                    this._fcsend = value;
                }
            }
        }


        public string FSFILE_NO
        {
            get { return this._fsfile_no; }
            set
            {
                if (value != this._fsfile_no)
                {
                    this._fsfile_no = value;
                }
            }
        }

        public string FSTITLE
        {
            get { return this._fstitle; }
            set
            {
                if (value != this._fstitle)
                {
                    this._fstitle = value;
                }
            }
        }

        public string FSSTART_TIMECODE
        {
            get { return this._fsstart_timecode; }
            set
            {
                if (value != this._fsstart_timecode)
                {
                    this._fsstart_timecode = value;
                }
            }
        }

        
        public string FSBEG_TIMECODE
        {
            get { return this._fsbeg_timecode; }
            set
            {
                if (value != this._fsbeg_timecode)
                {
                    this._fsbeg_timecode = value;
                }
            }
        }

        public string FSEND_TIMECODE
        {
            get { return this._fsend_timecode; }
            set
            {
                if (value != this._fsend_timecode)
                {
                    this._fsend_timecode = value;
                }
            }
        }

        public string FSFILE_TYPE
        {
            get { return this._fsfile_type; }
            set
            {
                if (value != this._fsfile_type)
                {
                    this._fsfile_type = value;
                }
            }
        }

        public string FSFILE_CATEGORY
        {
            get { return this._fsfile_category; }
            set
            {
                if (value != this._fsfile_category)
                {
                    this._fsfile_category = value;
                }
            }
        }


        public string FSFILE_CATEGORY_NAME
        {
            get { return this._fsfile_category_name; }
            set
            {
                if (value != this._fsfile_category_name)
                {
                    this._fsfile_category_name = value;
                }
            }
        }
        
        public string FNLOCATION
        {
            get { return this._fnlocation; }
            set
            {
                if (value != this._fnlocation)
                {
                    this._fnlocation = value;
                }
            }
        }

        public string FSIMAGEPATH
        {
            get { return this._fsimagepath; }
            set
            {
                if (value != this._fsimagepath)
                {
                    this._fsimagepath = value;
                }
            }
        }

        public string FSFILENO_CATEGORY
        {
            get { return this._fsfileno_category; }
            set
            {
                if (value != this._fsfileno_category)
                {
                    this._fsfileno_category = value;
                }
            }
        }

        public string FDPREBOOKING_DATE
        {
            get { return this._fdprebooking_date; }
            set
            {
                if (value != this._fdprebooking_date)
                {
                    this._fdprebooking_date = value;
                }
            }
        }

        public string FCOUT_BUY
        {
            get { return this._fcout_buy; }
            set
            {
                if (value != this._fcout_buy)
                {
                    this._fcout_buy = value;
                }
            }
        }

        public string FSGUID
        {
            get { return this._fsguid; }
            set
            {
                if (value != this._fsguid)
                {
                    this._fsguid = value;
                }
            }
        }


        public string FSPROG_NAME
        {
            get { return this._fsprog_name; }
            set
            {
                if (value != this._fsprog_name)
                {
                    this._fsprog_name = value;
                }
            }
        }


        public bool FBIS_CHECK
        {
            get { return this._fbis_check; }
            set
            {
                if (value != this._fbis_check)
                {
                    this._fbis_check = value;
                }
            }
        }
    }
}
