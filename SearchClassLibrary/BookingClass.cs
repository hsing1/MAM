﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SearchClassLibrary
{
    public class BookingClass
    {
        private string _fsbooking_no;
        private string _fsseq;
        private string _fsuserid;
        private string _fsbooking_date;
        private string _fsfile_no;
        private string _fsbeg_timecode;
        private string _fsend_timecode;
        private string _fsfile_type;
        private string _fsfile_category;
        private string _fnlocation;
        private string _fccheck;
        private string _fscheck_id;
        private string _fdchech_date;
        private string _fnfile_status;
        private string _fcout_buy;
        private string _fscheck_out_buy_id;
        private string _fdcheck_out_out_buy_date;
        private string _fccheck_out_buy;

        public string FSBOOKING_NO
        {
            get { return this._fsbooking_no; }
            set
            {
                if (value != this._fsbooking_no)
                {
                    this._fsbooking_no = value;
                }
            }
        }

        public string FSSEQ
        {
            get { return this._fsseq; }
            set
            {
                if (value != this._fsseq)
                {
                    this._fsseq = value;
                }
            }
        }

        public string FSUSER_ID
        {
            get { return this._fsuserid; }
            set
            {
                if (value != this._fsuserid)
                {
                    this._fsuserid = value;
                }
            }
        }

        public string FSBOOKING_DATE
        {
            get { return this._fsbooking_date; }
            set
            {
                if (value != this._fsbooking_date)
                {
                    this._fsbooking_date = value;
                }
            }
        }

       
        public string FSFILE_NO
        {
            get { return this._fsfile_no; }
            set
            {
                if (value != this._fsfile_no)
                {
                    this._fsfile_no = value;
                }
            }
        }


        public string FSBEG_TIMECODE
        {
            get { return this._fsbeg_timecode; }
            set
            {
                if (value != this._fsbeg_timecode)
                {
                    this._fsbeg_timecode = value;
                }
            }
        }

        public string FSEND_TIMECODE
        {
            get { return this._fsend_timecode; }
            set
            {
                if (value != this._fsend_timecode)
                {
                    this._fsend_timecode = value;
                }
            }
        }

        public string FSFILE_TYPE
        {
            get { return this._fsfile_type; }
            set
            {
                if (value != this._fsfile_type)
                {
                    this._fsfile_type = value;
                }
            }
        }

        public string FSFILE_CATEGORY
        {
            get { return this._fsfile_category; }
            set
            {
                if (value != this._fsfile_category)
                {
                    this._fsfile_category = value;
                }
            }
        }


        public string FNLOCATION
        {
            get { return this._fnlocation; }
            set
            {
                if (value != this._fnlocation)
                {
                    this._fnlocation = value;
                }
            }
        }

        
        public string FCCHECK
        {
            get { return this._fccheck; }
            set
            {
                if (value != this._fccheck)
                {
                    this._fccheck = value;
                }
            }
        }

        public string FSCHECK_ID
        {
            get { return this._fscheck_id; }
            set
            {
                if (value != this._fscheck_id)
                {
                    this._fscheck_id = value;
                }
            }
        }

        public string FDCHECK_DATE
        {
            get { return this._fdchech_date; }
            set
            {
                if (value != this._fdchech_date)
                {
                    this._fdchech_date = value;
                }
            }
        }

        

        public string FNFILE_STATUS
        {
            get { return this._fnfile_status; }
            set
            {
                if (value != this._fnfile_status)
                {
                    this._fnfile_status = value;
                }
            }
        }

        public string FCOUT_BUY
        {
            get { return this._fcout_buy; }
            set
            {
                if (value != this._fcout_buy)
                {
                    this._fcout_buy = value;
                }
            }
        }


        public string FSCHECK_OUT_BUY_ID
        {
            get { return this._fscheck_out_buy_id; }
            set
            {
                if (value != this._fscheck_out_buy_id)
                {
                    this._fscheck_out_buy_id = value;
                }
            }
        }

        public string FDCHECK_OUT_BUY_DATE
        {
            get { return this._fdcheck_out_out_buy_date; }
            set
            {
                if (value != this._fdcheck_out_out_buy_date)
                {
                    this._fdcheck_out_out_buy_date = value;
                }
            }
        }

        public string FCCHECK_OUT_BUY
        {
            get { return this._fccheck_out_buy; }
            set
            {
                if (value != this._fccheck_out_buy)
                {
                    this._fccheck_out_buy = value;
                }
            }
        }


    }
}
