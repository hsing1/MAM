﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SearchClassLibrary
{
    public class UserBookingHistoryMasterClass
    {
        private string _fsbooking_no;
        private string _fsuser_id;
        private string _fdbooking_date;
        private string _fsbooking_reason;
        private string _fscheck_status;
        private string _fscheck_id;
        private string _fdcheck_date;
        private string _fcneed_check_outbuy;
        private string _fcneed_check_internation;

        public string FSBOOKING_NO
        {
            get { return this._fsbooking_no; }
            set
            {
                if (value != this._fsbooking_no)
                {
                    this._fsbooking_no = value;
                }
            }
        }

        public string FSUSER_ID
        {
            get { return this._fsuser_id; }
            set
            {
                if (value != this._fsuser_id)
                {
                    this._fsuser_id = value;
                }
            }
        }

        public string FDBOOKING_DATE
        {
            get { return this._fdbooking_date; }
            set
            {
                if (value != this._fdbooking_date)
                {
                    this._fdbooking_date = value;
                }
            }
        }

        public string FSBOOKING_REASON
        {
            get { return this._fsbooking_reason; }
            set
            {
                if (value != this._fsbooking_reason)
                {
                    this._fsbooking_reason = value;
                }
            }
        }

        public string FSCHECK_STATUS
        {
            get { return this._fscheck_status; }
            set
            {
                if (value != this._fscheck_status)
                {
                    _fscheck_status = value;
                }
            }
        }

        public string FSCHECK_ID
        {
            get { return this._fscheck_id; }
            set
            {
                if (value != this._fscheck_id)
                {
                    this._fscheck_id = value;
                }
            }
        }

        public string FDCHECK_DATE
        {
            get { return this._fdcheck_date; }
            set
            {
                if (value != this._fdcheck_date)
                {
                    this._fdcheck_date = value;
                }
            }
        }

        public string FCNEED_CHECK_OUTBUY
        {
            get { return this._fcneed_check_outbuy; }
            set
            {
                if (value != this._fcneed_check_outbuy)
                {
                    this._fcneed_check_outbuy = value;
                }
            }
        }

        public string FCNEED_CHECK_INTERNATION
        {
            get { return this._fcneed_check_internation; }
            set
            {
                if (value != this._fcneed_check_internation)
                {
                    this._fcneed_check_internation = value;
                }
            }
        }
    }
}
