﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SearchClassLibrary
{
    public class CheckBookingDetailClass
    {
        private string _fsbooking_no;
        private string _fsseq;
        private string _fsfile_no;
        private string _fstitle;
        private string _fsbeg_timecode;
        private string _fsend_timecode;
        private string _fsfile_category;
        private string _fccheck;
        private string _fccheck_name;
        private string _fscheck_id;
        private string _fdchech_date;
        private string _fcsupervisor_check;
        private string _fcsupervisor_check_name;
        private string _fssupervisor_check_id;
        private string _fdsupervisor_chech_date;
        private string _fsimagepath;
        private string _fsfileno_category;
        private string _fcfile_status;
        private string _fcfile_transcode_status;
        private bool _fcenable_check;
        private string _fsfile_output_info;

        public string FSBOOKING_NO
        {
            get { return this._fsbooking_no; }
            set
            {
                if (value != this._fsbooking_no)
                {
                    this._fsbooking_no = value;
                }
            }
        }

        public string FSSEQ
        {
            get { return this._fsseq; }
            set
            {
                if (value != this._fsseq)
                {
                    this._fsseq = value;
                }
            }
        }

        public string FSFILE_NO
        {
            get { return this._fsfile_no; }
            set
            {
                if (value != this._fsfile_no)
                {
                    this._fsfile_no = value;
                }
            }
        }

        public string FSTITLE
        {
            get { return this._fstitle; }
            set
            {
                if (value != this._fstitle)
                {
                    this._fstitle = value;
                }
            }
        }


        public string FSBEG_TIMECODE
        {
            get { return this._fsbeg_timecode; }
            set
            {
                if (value != this._fsbeg_timecode)
                {
                    this._fsbeg_timecode = value;
                }
            }
        }

        public string FSEND_TIMECODE
        {
            get { return this._fsend_timecode; }
            set
            {
                if (value != this._fsend_timecode)
                {
                    this._fsend_timecode = value;
                }
            }
        }


        public string FSFILE_CATEGORY
        {
            get { return this._fsfile_category; }
            set
            {
                if (value != this._fsfile_category)
                {
                    this._fsfile_category = value;
                }
            }
        }


        public string FCCHECK
        {
            get { return this._fccheck; }
            set
            {
                if (value != this._fccheck)
                {
                    this._fccheck = value;
                }
            }
        }

        public string FCCHECK_NAME
        {
            get { return this._fccheck_name; }
            set
            {
                if (value != this._fccheck_name)
                {
                    this._fccheck_name = value;
                }
            }
        }

        public string FSCHECK_ID
        {
            get { return this._fscheck_id; }
            set
            {
                if (value != this._fscheck_id)
                {
                    this._fscheck_id = value;
                }
            }
        }

        public string FDCHECK_DATE
        {
            get { return this._fdchech_date; }
            set
            {
                if (value != this._fdchech_date)
                {
                    this._fdchech_date = value;
                }
            }
        }


        public string FCSUPERVISOR_CHECK
        {
            get { return this._fcsupervisor_check; }
            set
            {
                if (value != this._fcsupervisor_check)
                {
                    this._fcsupervisor_check = value;
                }
            }
        }

        public string FCSUPERVISOR_CHECK_NAME
        {
            get { return this._fcsupervisor_check_name; }
            set
            {
                if (value != this._fcsupervisor_check_name)
                {
                    this._fcsupervisor_check_name = value;
                }
            }
        }

        public string FSSUPERVISOR_CHECK_ID
        {
            get { return this._fssupervisor_check_id; }
            set
            {
                if (value != this._fscheck_id)
                {
                    this._fssupervisor_check_id = value;
                }
            }
        }

        public string FDSUPERVISOR_CHECK_DATE
        {
            get { return this._fdsupervisor_chech_date; }
            set
            {
                if (value != this._fdsupervisor_chech_date)
                {
                    this._fdsupervisor_chech_date = value;
                }
            }
        }

        public string FSIMAGEPATH
        {
            get { return this._fsimagepath; }
            set
            {
                if (value != this._fsimagepath)
                {
                    this._fsimagepath = value;
                }
            }
        }

        public string FSFILENO_CATEGORY
        {
            get { return this._fsfileno_category; }
            set
            {
                if (value != this._fsfileno_category)
                {
                    this._fsfileno_category = value;
                }
            }
        }

        public string FCFILE_STATUS
        {
            get { return this._fcfile_status; }
            set
            {
                if (value != this._fcfile_status)
                {
                    this._fcfile_status = value;
                }
            }
        }

        public string FCFILE_TRANSCODE_STATUS
        {
            get { return this._fcfile_transcode_status; }
            set
            {
                if (value != this._fcfile_transcode_status)
                {
                    this._fcfile_transcode_status = value;
                }
            }
        }

        public bool FCENABLE_CHECK
        {
            get { return this._fcenable_check; }
            set
            {
                if (value != this._fcenable_check)
                {
                    this._fcenable_check = value;
                }
            }
        }

        public string FSFILE_OUTPUT_INFO
        {
            get { return this._fsfile_output_info; }
            set
            {
                if (value != this._fsfile_output_info)
                {
                    this._fsfile_output_info = value;
                }
            }
        }

    }
}
