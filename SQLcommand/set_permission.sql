use MAM
GO

--------------------------------------------------------------------------------------------------------------
declare @case int
set @case = 1

if @case = 1 BEGIN -- 查詢使用者在職及權限狀態

	declare @emp TABLE (empID  varchar(20)) 
	declare @res TABLE (empID varchar(20), empName varchar(50), empStatus varchar(20), permission varchar(20))

	insert into @emp(empID) (
	select FSUSER_ID from TBUSERS)

	declare CSR cursor for select empId from @emp
	declare @empId varchar(20)

	open CSR 
	fetch next from CSR INTO @empID
	while (@@FETCH_STATUS = 0) BEGIN
		if (EXISTS(select * from TBUSER_GROUP where FSUSER_ID = @empID)) BEGIN
			INSERT into @res(empID, empName, empStatus, permission)(
			select FSUSER_ID, FSUSER_ChtName,
			CASE FCACTIVE
				WHEN 1 THEN '在職'
				WHEN 0 THEN '已離職'
			END as '員工狀態' 
			,
			'有權限' as '權限狀態'
			from TBUSERS where FSUSER_ID = @empID )
		END
		else
			INSERT into @res(empID, empName, empStatus, permission)(
			select FSUSER_ID, FSUSER_ChtName,
			CASE FCACTIVE
				WHEN 1 THEN '在職'
				WHEN 0 THEN '已離職'
			END as '員工狀態' 
			,
			'無權限' as '權限狀態'
			from TBUSERS where FSUSER_ID = @empID)

		fetch next from CSR into @empID
	END

	select * from @res

END -- case 1

else if (@case = 101) BEGIN

	select * from TBUSERS where FSUSER='prg60167'
	select * from TBUSERS where FSUSER_ChtName = '黃靜國'
	select * from TBUSERS where FSUSER_ID= '50598'
	select * from TBUSER_GROUP where FSUSER_ID= '07194'


	select u.FSDEPT, u.FSUSER, u.FSUSER_ChtName, g.* from TBUSERS u, TBUSER_GROUP g where u.FSUSER_ID=g.FSUSER_ID and (u.FSUSER_ID='51124' or u.FSUSER_ChtName='朱幸一' or u.FSUSER='hsing2') 

	exec usp_DeleteUserRole '05136';



	-- 查詢不在職員工
	select distinct b.FSUSER_ChtName,b.FCACTIVE,a.* 
	from TBUSER_GROUP a inner join TBUSERS b 
	on b.FSUSER_ID  = a.FSUSER_ID
	where b.FCACTIVE  = 0 
	order by b.FCACTIVE


	-- 查詢群組使用者
	select * from TBUSER_GROUP where FSGROUP_ID='Everyone'
	select u.FSUSER_ChtName name, u.FSEMAIL from TBUSER_GROUP g, TBUSERS u where (g.FSUSER_ID=u.FSUSER_ID) AND (g.FSGROUP_ID='Everyone')
	-- 查詢有使用權限者
	select * from TBUSERS as u inner join TBUSER_GROUP as g on u.FSUSER_ID = g.FSUSER_ID where u.FSUSER_ID = '51204'

	select u.FSUSER_ID, g.FSGROUP_ID from TBUSERS as u inner join TBUSER_GROUP as g on u.FSUSER_ID = g.FSUSER_ID where u.FCACTIVE = 0
	select * from TBUSER_GROUP
	select FSUSER_ID as 工號, FSDEPT as 部門 from TBUSERS where FCACTIVE = 0


	select distinct(u.FSUSER_ID) from TBUSERS as u inner join TBUSER_GROUP as g on u.FSUSER_ID = g.FSUSER_ID where u.FCACTIVE = 0

	select * from TBUSERS where FSUSER = 'mamsystem'
END --case 101

GO
-------------------------------------------------------------------------------------------