

declare @case int, @subcase int

/*
 * 上傳失敗，重新標記上傳
 */
if @case = 2016 begin
	--2016-08-22 17:26
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002PMP'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002PMP'

	-- 201608291044
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002XOG' --2016-08-29
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002XOG' --201608260126
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002XOG'

	--201609091055
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002YWK'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002YWK'

	--201609101629
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002YXL'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002YXL' -- 無上傳紀錄

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002PMS'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002PMS' -- 2016-07-05 16:14:39.470 88

	--201609130929
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002Z6Q'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002Z6Q'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002Z6R'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002Z6R'

	--201609301440 22G 傳到 17G 失敗 
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000030N3' --201609290203, G201080100090001

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000030R2'  --手動上傳，會從排程移除

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000030RI'
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000030QG'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000030QG'

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000031I2'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000031GS'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000031GL'

	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '0000319K'

	--201610181326
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('000031E3', '000032GD')
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID in ('000031E3', '000032GD') --0031E3:88, 0032GD no record
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000032GD'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000032GE'


	--201610191326 檔名錯誤
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000032KR'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '000032KR'

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000032H8'

	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '000032RO'

	--20161019 上傳失敗
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('000032JT', '000032JS', '000032JP', '000032JO', '000032JN', '000032JM', '000032JL', '000032JK',
	 '000032JJ',  '000032JH', '000032JG', '000032JF', '000032JE', '000032JD', '000032JB')

	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '000032RQ' --88
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000032RQ'

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000032RO'  --> X
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000032UA'  --100 個種子的祕密 置換

	--20161024
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '0000335B' --201610240144, 2016-10-24 17:14:53.857 markupload
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '0000335B' -- no record
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '0000335B' order by FDDATE

	--20161108
	select FSBRO_ID, FSVIDEO_ID from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('000034FI', '000034FJ', '000034FK', '000034FM', '0000314R', '0000314S', '0000314U', '000034G4', '000034G1')

	--201611210920
	select * from TBLOG_VIDEO_HD_MC where CAST(FDUPDATED_DATE as DATE) = '2016-11-16' and FCSTATUS = 'E'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('0000336M', '0000337U', '0000337V', '0000337W', '0000337Z', '000034SD', '000034SE', '000034SF', '000034SG')
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in ('0000336M', '0000337U', '0000337V', '0000337W', '0000337Z', '000034SD', '000034SE', '000034SF', '000034SG')

	--201611211341  00002NYG 審核通過 會同時出現在 Review-> Approved and GPFS -> Approved : todo
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002NYG' --2016-07-11 20:44:36.340 -> O

	--201611231700
	select A.*, B.FSFILE_NO, B.FSVIDEO_PROG, C.FSPGMNAME, B.FNEPISODE, C.FSCHANNEL from TBLOG_VIDEO_HD_MC A left join TBLOG_VIDEO B on A.FSVIDEO_ID = B.FSVIDEO_PROG left join TBPROG_M C on B.FSID = C.FSPROG_ID
	where CAST(A.FDUPDATED_DATE as DATE) = '2016-11-22'

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('000035QF', '000035QA', '000035QG')

	select * from TBLOG_VIDEO_HD_MC where CAST(FDUPDATED_DATE as DATE) = '2016-11-22'
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002WKB'

	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in ('000036ST', '000037MD', '0000391J', '000037X1', '0000392Y')


if @case = 201701091021 begin  -- A 棟檔案上傳機無法連線 10.13.15.60
	select * from TBPTSAP_STATUS_LOG
	-- 標記上傳仍在等待上傳狀態
	select * from TBLOG_VIDEO_HD_MC where FDUPDATED_DATE >= '2016-07-01' and FCSTATUS = '' and FSUPDATED_BY <> '程式'
	-- 標記上傳仍在等待上傳狀態
	select * from TBLOG_VIDEO_HD_MC where FDUPDATED_DATE >= '2017-01-09' and (FCSTATUS = '' or FCSTATUS = 'E') and (FSUPDATED_BY <> '程式' or FSUPDATED_BY = 'PTS_PA') order by FDUPDATED_DATE
	
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '0000397Z'

	select B.*,A.* from TBPTSAP_STATUS_LOG A join TBLOG_VIDEO_HD_MC B on A.FSVIDEOID = B.FSVIDEO_ID 
	   where A.FDCREATED_DATE >= '2017-01-09' and A.FCSTATUS <> '80' order by A.FDUPDATE_DATE --16

	   select B.*,A.* from TBPTSAP_STATUS_LOG A join TBLOG_VIDEO_HD_MC B on A.FSVIDEOID = B.FSVIDEO_ID 
	   where A.FDCREATED_DATE > '2017-01-09' and A.FCSTATUS <> '80' order by A.FDUPDATE_DATE --16
		   
	select B.*,A.* from TBPTSAP_STATUS_LOG A join TBLOG_VIDEO_HD_MC B on A.FSVIDEOID = B.FSVIDEO_ID
	   where A.FDCREATED_DATE between '2017-01-10 01:00' and '2017-01-10 07:00' and A.FCSTATUS = '80' order by A.FDUPDATE_DATE

	select * from TBPTSAP_STATUS_LOG where FDCREATED_DATE >= '2017-01-09' and FCSTATUS <> '80' order by FDUPDATE_DATE

	--狀態在上傳中，改為上傳失敗，重新標記
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'E' where FSVIDEO_ID = '00003AAU'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00003AAU' -->88
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00003AAU' --C

	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'E' where FSVIDEO_ID = '000039QA'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00003AAU'

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00003AC4'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00003AC4' --no data

    select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00003AAV'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00003AAV' --no data

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00003AB8'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00003AB8' --no data

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00003ABD'  --FCSTATUS = ''
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00003ABD'

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00003ABE'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00003ABE' --no data

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00003ABF'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00003ABF' --no data

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00003ABH'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00003ABH' --no data

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00003ABI'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00003ABI' --no data

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '0000351R'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '0000351R'

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00003ABN'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00003ABN' --88

	select * from TBLOG_VIDEO_HD_MC where FDUPDATED_DATE between '2017-01-10 01:00' and '2017-01-10 07:00'
end

if @case = 201701111007 begin
	select A.*, B.FSFILE_NO, B.FSVIDEO_PROG, C.FSPGMNAME, B.FNEPISODE, C.FSCHANNEL from TBLOG_VIDEO_HD_MC A left join TBLOG_VIDEO B on A.FSVIDEO_ID = B.FSVIDEO_PROG left join TBPROG_M C on B.FSID = C.FSPROG_ID
	where CAST(A.FDUPDATED_DATE as DATE) = '2017-01-11' and FCSTATUS = 'E'

	--1/10 有執行的標記上傳
	select * from TBPTSAP_STATUS_LOG A left join TBLOG_VIDEO_HD_MC B on A.FSVIDEOID = B.FSVIDEO_ID where CAST(A.FDCREATED_DATE as DATE) = '2017-01-10' order by A.FSVIDEOID

	--1/10 標記上傳 78
	select * from TBLOG_VIDEO_HD_MC where CAST(FDCREATED_DATE as DATE) = '2017-01-10' order by FSVIDEO_ID

	--1/10 標記上傳 78, FCSTATUS = '' 仍在等待上傳
	select * from TBLOG_VIDEO_HD_MC where CAST(FDCREATED_DATE as DATE) = '2017-01-10' and FCSTATUS = ''  order by FSVIDEO_ID --50
	select B.FSID, B.FNEPISODE, B.FCFILE_STATUS, A.* from TBLOG_VIDEO_HD_MC A left join TBLOG_VIDEO B on A.FSVIDEO_ID = B.FSVIDEO_PROG where CAST(A.FDCREATED_DATE as DATE) = '2017-01-10' and A.FCSTATUS = '' order by A.FSVIDEO_ID --50

	--1/10 標記上傳 78, FCSTATUS = ''  仍在等待上傳, TBPTSAP_LOG_STATUS 無紀錄
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00003AG6'

	select * from TBLOG_VIDEO_HD_MC where CAST(FDCREATED_DATE as DATE) = '2017-01-10' and FCSTATUS = '' and  
	FSVIDEO_ID not in (select FSVIDEOID from TBPTSAP_STATUS_LOG) order by FSVIDEO_ID

	--from 宗晉,尚未到待審  -> 排隊搬檔中
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00003AG6', '00003AFG', '00003AFH', '00003AFB', '00003AFB', '00003AFA', '00003AF9', '00003AF8')

	--from hakka  N
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00001ZK0', '00001ZK2', '00001K7G', '00001T0B', '00001R7H')

end

if @case = 201701111419 begin  --排表：檔案已到待審區，QC 狀態顯示檔案未到
	--00001ZK0, 00001ZK2, 00001R7H 是由 GPFS --> Review 但是 FCSTATUS = '' 等待上傳
	select B.*, A.* from TBLOG_VIDEO A left join TBLOG_VIDEO_HD_MC B on A.FSVIDEO_PROG = B.FSVIDEO_ID 
	where A.FSVIDEO_PROG in ('00001ZK0', '00001ZK2', '00001R7G', '00001TQB', '00001R7H')
	select * from TBBROADCAST where FSBRO_ID = '201507140076' --2016-11-09 09:32:34.067

	--改為待審何狀態
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSVIDEO_ID in ('00001ZK0', '00001ZK2', '00001R7H')
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSVIDEO_ID = '00001R7G'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSVIDEO_ID = '00001TQB'

	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00001ZK0'
end


if @case = 201701111551 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00003AF8' --2017-01-10 15:59:14.130  created
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00003AF8'  --2017-01-11 10:35:17.563, 2017-01-11 10:35:32.263
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00003AF8'  --T

	select * from TBLOG_VIDEO_HD_MC A left join TBPTSAP_STATUS_LOG B on A.FSVIDEO_ID = B.FSVIDEOID where A.FSVIDEO_ID in ('00003AF8', '00003AD5', '00003AFV')  --> 1/10 標記，TBPTSAP_STATUS_LOG 是 1/11 才上傳
	select * from TBLOG_VIDEO_HD_MC A left join TBPTSAP_STATUS_LOG B on A.FSVIDEO_ID = B.FSVIDEOID where A.FSVIDEO_ID in ('000039VP', '00003AEG') 
	select * from TBBROADCAST where FSBRO_ID = '201701100058' --2017-01-10 15:59:14.050 標記上傳
end


if @case = 201701121000 begin -- 南部上傳暫停排程重新啟動後檔案有到，但是狀態仍是在上傳中
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00003AI2' --FCSTATUS = '', 201701110007, G201752400020001
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00003AI2' --80
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00003AI2'
	--update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201752400020001'
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00003AI2'
end

if @case = 201711121027 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000038YY' --FCSTATUS = ''  檔案已在 Review --> neet set FCSTATUS = N, G201408600010008
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '000038YY' --no data
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '000038YY' --T  from ingest
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000038YY' --創新與創業論壇  全國巡迴演講2下集：產學論壇, 1/14, 12

	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201408600010008'
end


if @case = 201701121053 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000023Q7' --FCSTATUS = ''  檔案已在 Review --> neet set FCSTATUS = N, G201604200010001
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '000023Q7' --no data
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '000023Q7' --T  from ingest
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000023Q7' --尋物少女 1/15 ch12

	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201604200010001'
end


if @case = 201701221105 begin  -- QC 已審核 已轉檔 檔案仍在 Review
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00003AF0' --FCSTATUS = ''  --O?  檔案仍在 Review, G201755500040001, 手動搬擋到 Approved
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00003AF0'
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00003AF0' --T  from ingest
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00003AF0' --PeoPo公民新聞報(2017週末版) 1/14 ch12
end

if @case = 201701121130 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00001XVJ' --FCSTATUS = '' 
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00001XVJ' --no data
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00001XVJ' --T  from ingest
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00001XVJ'  --佩佩與小貓2 17 #ch62, 2017-01-16

	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201512800170002'

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00001XVL' --FCSTATUS = '' 
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00001XVL' --no data
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00001XVL' --T  from ingest
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00001XVL'  --佩佩與小貓2 18 #ch62, 2017-01-17

	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201512800180003'
end

if @case = 201701121655 begin
	exec SP_Q_INFO_BY_VIDEOID '00003AEJ'
	exec SP_Q_INFO_BY_VIDEOID '00003AIN'
	exec SP_Q_INFO_BY_VIDEOID '000036DD'

	select * from TBPGM_PROMO where FSPROMO_ID =  '20161100690' --書店裡的影像詩第二季_書集囍室 未排播但是在搬檔 Review -> Approved

end

if @case = 201701161223-201701121130 begin --佩佩與小貓2 19~23
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00001XVJ' --FCSTATUS = '' 
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00001XVJ' --no data
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00001XVJ' --T  from ingest
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00001XVJ'  --佩佩與小貓2 17 #ch62, 2017-01-16

	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201512800170002'

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00001XVL' --FCSTATUS = '' 
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00001XVL' --no data
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00001XVL' --T  from ingest
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00001XVL'  --佩佩與小貓2 18 #ch62, 2017-01-17

	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201512800180003'

	-- 19, 20,21,23 檔案都在 Review , 23 在排隊搬到 Approved
	select * from TBLOG_VIDEO where FSID = '2015128' and FNEPISODE between 19 and 23 and FSARC_TYPE = '002'
	select B.*, A.* from TBLOG_VIDEO A left join TBLOG_VIDEO_HD_MC B on A.FSFILE_NO = B.FSFILE_NO where A.FSID = '2015128' and A.FNEPISODE between 19 and 23 and A.FSARC_TYPE = '002'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSVIDEO_ID = '00001XVN' --19
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSVIDEO_ID = '00001XVP' --20
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N', FSUPDATED_BY = '51204', FDUPDATE_DATED = GETDATE() where FSVIDEO_ID = '00001XZC'

	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00001XZE' --#22 : X

	exec SP_Q_INFO_BY_VIDEOID '00001XVN'
	exec SP_Q_INFO_BY_VIDEOID '00001XVP'
	exec SP_Q_INFO_BY_VIDEOID '00001XZC'
	exec SP_Q_INFO_BY_VIDEOID '00001XZG' --O
	exec SP_Q_INFO_BY_VIDEOID '00001XZE'

	select * from TBBROADCAST where FSBRO_ID = '201506180056' --2016-01-08 09:27:47.400
end

if @case = 201701161512 begin  -- 佩佩與小貓  22 轉檔成功 搬檔失敗? 狀態為刪除 未排播
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201512800220002' --00001XZE, 201506180056, X
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-01-08'
			and (FSMESSAGE like '%117073%' or FSMESSAGE like '%146480%' or FSMESSAGE like '%70898%' or FSMESSAGE like '%G201512800220002%' or FSMESSAGE like '%001XZE%') order by  FDTIME
	exec SP_Q_INFO_BY_VIDEOID '00001XZE' --08255
	select * from TBUSERS where FSUSER_ID = '08255'  --陳素娥
end

if @case = 201701201136 begin
	exec SP_Q_INFO_BY_VIDEOID '00003BCD'
end

----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
-------------------------------------- Routine -----------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
	
	select A.*, B.FSFILE_NO, B.FSVIDEO_PROG, C.FSPGMNAME, B.FNEPISODE, C.FSCHANNEL from TBLOG_VIDEO_HD_MC A left join TBLOG_VIDEO B on A.FSVIDEO_ID = B.FSVIDEO_PROG left join TBPROG_M C on B.FSID = C.FSPROG_ID
	where CAST(A.FDUPDATED_DATE as DATE) = '2016-11-23' and FCSTATUS = 'E'


	-- 根據日期查詢當天標記或建立的送播單
	--為什麼 FSUPDATE_BY 會有 NULL?
	select A.*, B.*, C.* from TBLOG_VIDEO_HD_MC A left join TBLOG_VIDEO B on A.FSVIDEO_ID = B.FSVIDEO_PROG and LEN(B.FSTRACK) = 8 and A.FSUPDATED_BY is not NULL
	join TBBROADCAST C on A.FSBRO_ID = C.FSBRO_ID
	where CAST(C.FDUPDATED_DATE as DATE) = '2017-01-11'

	select * from TBBROADCAST where FSBRO_ID = '201701110051'
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00003AJ7'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00003AJ7'

	--003AJ7 A. FSUPDATED_BY = NULL, B.FSFILE_NO = NULL
	select * from TBLOG_VIDEO_HD_MC A left join TBLOG_VIDEO B on A.FSVIDEO_ID = B.FSVIDEO_PROG and LEN(B.FSTRACK) = 8 and A.FSUPDATED_BY is not NULL

	--for 003AJ7 B.FSFILE_NO = G201692500220002
	select * from TBLOG_VIDEO_HD_MC A left join TBLOG_VIDEO B on A.FSVIDEO_ID = B.FSVIDEO_PROG and LEN(B.FSTRACK) = 8

	select * from TBLOG_VIDEO_HD_MC where FSUPDATED_BY is not NULL --no 003AJ7

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '' order by FDCREATED_DATE --1556
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '' --1773
	select * from TBLOG_VIDEO where FSVIDEO_PROG is NULL --0

	select A.FSBRO_ID, A.FSFILE_NO,A.FSUPDATED_BY, FSVIDEO_ID, B.FSFILE_NO, B.FSVIDEO_PROG from TBLOG_VIDEO_HD_MC A join TBLOG_VIDEO B 
	    on A.FSVIDEO_ID = B.FSVIDEO_PROG and LEN(B.FSTRACK) = 8 order by FSVIDEO_PROG

	--join : 12549
	select A.FSBRO_ID, A.FSFILE_NO,A.FSUPDATED_BY, FSVIDEO_ID, B.FSFILE_NO, B.FSVIDEO_PROG from TBLOG_VIDEO_HD_MC A join TBLOG_VIDEO B 
	    on A.FSVIDEO_ID = B.FSVIDEO_PROG and LEN(B.FSTRACK) = 8 and A.FSUPDATED_BY is not null order by FSVIDEO_PROG

	--left join : 18845
	select A.FSBRO_ID, A.FSFILE_NO,A.FSUPDATED_BY, FSVIDEO_ID, B.FSFILE_NO, B.FSVIDEO_PROG from TBLOG_VIDEO_HD_MC A left join TBLOG_VIDEO B 
	    on A.FSVIDEO_ID = B.FSVIDEO_PROG and LEN(B.FSTRACK) = 8 and A.FSUPDATED_BY is not null order by FSVIDEO_PROG --18845





	select * from TBLOG_VIDEO_HD_MC where CAST(FDUPDATED_DATE as DATE) = '2017-01-11' and FSBRO_ID = '201701110045'
	select * from TBLOG_VIDEO_HD_MC where FSBRO_ID = '201701110045'
	select * from TBLOG_VIDEO_HD_MC where CAST(FDCREATED_DATE as DATE) = '2017-01-11' and FSUPDATED_BY is not NULL

	select A.*, B.* from TBLOG_VIDEO_HD_MC A left join TBLOG_VIDEO B on A.FSVIDEO_ID = B.FSVIDEO_PROG and LEN(B.FSTRACK) = 8 and A.FSUPDATED_BY is not NULL where A.FDCREATED_DATE >= '2017-01-11'
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00003AJ6'
	select A.*, B.* from TBLOG_VIDEO_HD_MC A left join TBLOG_VIDEO B on (A.FSVIDEO_ID = B.FSVIDEO_PROG and LEN(B.FSTRACK) = 8 and A.FSUPDATED_BY is not NULL)





-------------------------------------------------  HD_FILE_MANAGER 檔案搬動問題 ----------------------------------------------------------




------------------------------------------------------------------------------------------------------
if @case = 2016 begin  -- HD_FILE_MANAGER 檔案搬動問題

	if @subcase = 201609101026 begin
		select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002X9Q' --2016-09-02 10:57:32.960 --> O  檔案仍在 HDUpload
		select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002X9Q' --201608230102, P201608006600001, R 搬檔失敗
		select * from TBBROADCAST where FSBRO_ID = '201608230102' --FDTRAN_DATE = 2016-09-02 10:57:34.803
		--先將HDUpload檔案放到Review, 查搬檔失敗原因
	end


	if @subcase = 201609100948 begin
		select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002RUV' --2016-07-18 13:48:02.600 -> O
		select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002RUV' --T  為什麼沒從 GPFS 搬檔？ HDUpload 已經無檔案, P201607006510001

		--2016-09-09 14:16:54.070  DB2查詢：SELECT VOLUME_NAME FROM CONTENTS WHERE FILESPACE_NAME = '/ptstv' AND FILE_NAME = '/HVideo/2016/20160700651/P201607006510001.mxf' AND STGPOOL NOT LIKE '%CP'
		-- 有查詢
		select * from TBTRACKINGLOG where CAST(FDTIME as DATE) > '2016-09-08' and (FSMESSAGE like '%P201607006510001%' or FSMESSAGE like '%00002RUV%') order by FDTIME
		select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002RUV' --no scheduled
		select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002RUV' order by FDDATE
	end

	if @case = 201609101026-201609230914 begin
		select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002X9Q' --2016-09-25
		select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002X9Q' --2016-09-02 10:57:32.960 --> O  檔案仍在 HDUpload
		select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002X9Q' --201608230102, P201608006600001, R anystream 轉檔完成 搬檔失敗
		select * from TBBROADCAST where FSBRO_ID = '201608230102' --FDTRAN_DATE = 2016-09-02 10:57:34.803

		--先將HDUpload檔案放到Review, 查搬檔失敗原因
		select * from TBTRACKINGLOG where FDTIME between '2016-09-02 10:50' and '2016-09-02 12:30' 
			and (FSMESSAGE like '%142877%' or FSMESSAGE like '%188119%' or FSMESSAGE like '%94501%' or FSMESSAGE like '%P201608006600001%') order by  FDTIME

		--Anystream reschedule
		select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-09-23'
			and (FSMESSAGE like '%142877%' or FSMESSAGE like '%188119%' or FSMESSAGE like '%94501%' or FSMESSAGE like '%P201608006600001%') order by  FDTIME
	end

end

if @case = 201609260920 begin -- TSM 處裡中，但案未入庫無法搬動
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002WRU' --2016-08-18 17:51:22.030 : O
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002WRU' --S, 201608180135,P201608005610001
	select * from TBBROADCAST where FSBRO_ID = '201608180135' --FDTRAN_DATE = 2016-08-18 17:51:23.133
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002WRU' --2016-09-30 14:52:15:02, 14
	select * from TBZCHANNEL
	--重新審核
	--update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201608005610001'
    --update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201608005610001'

end


if @case = 201609231604 begin --奧林匹克 卡在 TSM 處裡中
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-09-23' and (FSMESSAGE like '%G200618607990006%' or FSMESSAGE like '%144332%' or FSMESSAGE like '%191265%') order by FDTIME
	select * from TBTRACKINGLOG where FDTIME between '2016-09-23 12:30' and '2016-09-23 13:30' order by FDTIME
	select * from TBTSM_JOB where FNTSMJOB_ID = '191265' -- FSSOURCE_PATH = ''
	select * from TBTSM_JOB where CAST(FDREGISTER_TIME as DATE) = '2016-09-23' order by FDREGISTER_TIME
	select * from TBLOG_VIDEO_HD_MC where FSFILE_NO = 'G200618607990006' --0000304L, 2016-09-23 12:38:07.743 -> O
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '0000304L' --2016-09-29

	--重新審核
	--update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G200618607990006'
    --update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G200618607990006'

	select * from TBTRACKINGLOG 
	   where CAST(FDTIME as DATE) = '2016-09-23' and (FSMESSAGE like '%G200618607990006%' or FSMESSAGE like '%144376%' or FSMESSAGE like '%19349%') order by FDTIME
end


if @case = 201609271020 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002XA0' --2016-09-02 11:17:55.810 O
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002XA0' --G201620000130003 , R, 201608230113, 9/27 : T
	select * from TBBROADCAST where FSBRO_ID = '201608230113' --2016-09-02 11:18:07.460
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002XA0' --2016-10-05 0900, 13 PTS2
	select * from TBZCHANNEL
	--重新審核
	--update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201620000130003'
    --update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201620000130003'
	select * from TBLOG_VIDEO_HD_MC where FSFILE_NO = 'G201323200010063' --201608310046 -> no such TBBROADCAST
	select * from TBLOG_VIDEO where FSBRO_ID = '201608310046' --T, 026
	select * from TBBROADCAST where FSBRO_ID = '201608310046'
end

if @case = 201610051518 begin --低解沒有副檔名 搬檔失敗
	select * from TBTRACKINGLOG
	   where CAST(FDTIME as DATE) = '2016-08-10' and (FSMESSAGE like '%G201713600040003%' or FSMESSAGE like '%183985%' or FSMESSAGE like '%140528%' or FSMESSAGE like '%92787%') order by FDTIME
	   --低解搬檔時發生錯誤：重試次數：0;From T:\IngestWorkingTemp\G201713600040003. To \\mamstream\MAMDFS\Media\Lv\ptstv\2017\2017136\G201713600040003.，錯誤：來源檔案不存在

	--Anystream reschedule
	select * from TBTRACKINGLOG
	   where CAST(FDTIME as DATE) = '2016-10-05' and (FSMESSAGE like '%G201713600040003%' or FSMESSAGE like '%183985%' or FSMESSAGE like '%140528%' or FSMESSAGE like '%92787%') 
	   order by FDTIME

	select * from TBLOG_VIDEO where FSFILE_NO = 'G201713600040003' --R, \\mamstream\MAMDFS\Media\Lv\ptstv\2017\2017136\G201713600040003.???  低解沒有副檔名
	select top 100 * from TBTRACKINGLOG order by FDTIME desc

	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002UM8' --not scheduled

	--update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201713600040003'
    --update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201713600040003'

	--重新審核轉檔
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-10-06' 
	and (FSMESSAGE like '%G201713600040003%' or FSMESSAGE like '%145151%' or FSMESSAGE like '%002UM8%' or FSMESSAGE like '%193097%' or FSMESSAGE like '%96676%') 
	order by FDTIME

	--低解路徑無副檔名搬檔失敗
	--update TBLOG_VIDEO set FSFILE_PATH_L = '\\mamstream\MAMDFS\Media\Lv\ptstv\2017\2017136\G201713600040003.mp4' where FSFILE_NO = 'G201713600040003'
	--update TBLOG_VIDEO set FSFILE_TYPE_LV = 'mp4' where FSFILE_NO = 'G201713600040003'


	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-10-06' 
	and (FSMESSAGE like '%G201713600040003%' or FSMESSAGE like '%145199%' or FSMESSAGE like '%002UM8%' or FSMESSAGE like '%193188%' or FSMESSAGE like '%96676%') 
	order by FDTIME

end


if @case = 201610071518 begin --ans 處理錯誤（anystream 轉檔失敗，當案有問題，更新檔案重新審核轉檔）
	select * from TBPROG_M where FSPGMNAME like '%爸媽%' --2009285
	select * from TBLOG_VIDEO where FSID = '2009285' and FNEPISODE = 390 and FSARC_TYPE = '002' --G200928503900024, 0000319K
	--update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G200928503900024'
    --update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G200928503900024'
	select * from TBBROADCAST where FSBRO_ID = '201610050062' --2016-10-06 09:55:30.967
	--HDUpload 00319K.mxf 無法刪除
end

if @case = 201610110902 begin
	--00319K.mxf 已請小胖刪除
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '0000319K'
	
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-10-11' 
	and (FSMESSAGE like '%G200928503900024%' or FSMESSAGE like '%145562%' or FSMESSAGE like '%00319K%' or FSMESSAGE like '%193849%' or FSMESSAGE like '%97048%') 
	order by FDTIME  --21 道程序
end

if @case = 201610121600 begin --檔案上傳順序問題，後播出先上傳
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in ('000031UT', '000031VD') order by FDDATE, FSPLAY_TIME
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID in ('000031UT', '000031VD') --21vd : 2016-10-12 12:10 31ut:2016-10-12 10:33
end

if @case = 201610130958 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('000031YD', '000031KZ') --50311, 51039 created, 201610070097, 201610120018
	select * from TBBROADCAST where FSBRO_ID in ('201610070097', '201610120018') --50311 10/7 Update, 51039 10/12 update
	select * from TBUSERS where FSUSER_ID in ('50311', '51216', '51018', '51039')
end

----------------------------------20161014---------------------------------------------------

if @case = 201610141453 begin --爸媽  395 轉檔完成搬檔失敗
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '000031EW' --R  -> T

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-10-07' 
	and (FSMESSAGE like '%G200928503950024%' or FSMESSAGE like '%145313%' or FSMESSAGE like '%0031EW%' or FSMESSAGE like '%193371%' or FSMESSAGE like '%96824%')
	order by FDTIME

	-- Anystream Rechedule

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-10-14' 
	and (FSMESSAGE like '%G200928503950024%' or FSMESSAGE like '%145313%' or FSMESSAGE like '%0031EW%' or FSMESSAGE like '%193371%' or FSMESSAGE like '%96824%')
	order by FDTIME


	--update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G200928503950024'
    --update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G200928503950024'
end

if @case = 201610141521 begin --爸媽  396 Anystream 處裡錯誤
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '000031HX'  --T
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-10-07' 
	and (FSMESSAGE like '%G200928503960024%' or FSMESSAGE like '%145318%' or FSMESSAGE like '%193379%' or FSMESSAGE like '%0031HX%' or FSMESSAGE like '%97000%')
	order by FDTIME

	--重啟任務
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-10-14' 
	and (FSMESSAGE like '%G200928503960024%' or FSMESSAGE like '%145318%' or FSMESSAGE like '%193379%' or FSMESSAGE like '%0031HX%' or FSMESSAGE like '%97308%')
	order by FDTIME
	--update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G200928503960024'
    --update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G200928503960024'
end


if @case = 201610141521 begin --爸媽  399 TSM 處裡中
	select *  from TBLOG_VIDEO where FSFILE_NO = 'G200928503990024' --S, 000031I6
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-10-07' 
	and (FSMESSAGE like '%G200928503990024%' or FSMESSAGE like '%145239%' or FSMESSAGE like '%000031I6%')
	order by FDTIME
	--update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G200928503990024'
    --update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G200928503990024'
end

if @case = 201610141521 begin --紫色大稻埕  1 Anystream 轉檔完成，搬檔完成，檢索不到
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-07-21' 
	and (FSMESSAGE like '%G201713600010001%' or FSMESSAGE like '%138458%' or FSMESSAGE like '%180429%' or FSMESSAGE like '%002QAP%' or FSMESSAGE like '%91140%')
	order by FDTIME

	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002QAP' --T, 201607010143
	--update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201713600010001'
    --update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201713600010001'
end

if @case = 201610141642 begin --檔案已到人不能審核，比對段落有差異
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '0000326V'
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '0000326V' --2016-10-16 10:00:20:18 懷念郭金發短片 14
end

if @case = 201610141747 begin
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in ('0000324Y', '0000320N') order by FDDATE, FSPLAY_TIME
end

if @case = 201610180918-201610141521 begin --紫色大稻埕  1 Anystream 轉檔完成，搬檔完成，檢索不到
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-07-21' 
	and (FSMESSAGE like '%G201713600010001%' or FSMESSAGE like '%138458%' or FSMESSAGE like '%180429%' or FSMESSAGE like '%002QAP%' or FSMESSAGE like '%91140%')
	order by FDTIME

	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002QAP' --T, 201607010143
	--update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201713600010001'
    --update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201713600010001'

	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201713600010001' -- FCFILE_STATUS = D
	update TBLOG_VIDEO_D set FCFILE_STATUS = 'T' where FSFILE_NO = 'G201713600010001'  --6 個資料列受影響

	select * from TBLOG_VIDEO_D where FSID = '2017136' and FNEPISODE = 1
	select * from TBLOG_VIDEO where FSID = '2017136' and FNEPISODE = 1 --201607010143 T, 201607220006 X

	select * from TBLOG_VIDEO_DIFFERENT_2 where FSSYS_ID = 'G2017136%' --no data
	select * from TBLOG_VIDEO_DIFFERENT --no data
end


if @case = 201610191546 begin --一堆 TSM 處裡中 (SOA1 無法連接  \\hsm1)
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-10-19' 
	and (FSMESSAGE like '%G201708800030001%' or FSMESSAGE like '%146098%' or FSMESSAGE like '%195110%')
	order by FDTIME

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-10-19' --毛起來幸福站在 TSM 恢復後仍無法完成
	and (FSMESSAGE like '%G201708800030001%' or FSMESSAGE like '%146070%' or FSMESSAGE like '%195082%' or FSMESSAGE like '%003252%')
	order by FDTIME

	select * from TBLOG_VIDEO where FSFILE_NO = 'G201654300770001'
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201654300770001'


	select * from TBTSM_JOB where FNTSMJOB_ID = '195082' -- FNSTATUS = 3
	select * from TBTSM_JOB where FNTSMJOB_ID = '195081' -- FNSTATUS = 3
	select * from TBTSM_JOB_WORKER
	select * from TBTSM_LOCKOP
	select * from TBTSM_SYSOP

	select * from TBLOG_VIDEO where FSFILE_NO = 'G201729200010003' --201609190074
    select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in ('00002OXY', '00002QQV')

	select * from TBLOG_VIDEO where FSFILE_NO = 'G201508800020009'
	select * from TBPROG_M where FSPROG_ID = '2015088' --對照記

	--重新審核轉檔
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201708800030001'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201708800030001'
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00003252'
end


if @case = 201610271041 begin --TSM 處裡中
	select * from TBLOG_VIDEO where FSFILE_NO = 'G200928504500024'  --R, 000032M9, 201610190084
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-10-20' --爸媽 450 TSM 處裡中
	and (FSMESSAGE like '%G200928504500024%' or FSMESSAGE like '%146147%' or FSMESSAGE like '%000032M9%')
	order by FDTIME

	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000032M9' --not scheduled
	select * from TBTRANSCODE_RECV_LOG
	select * from TBTRANSCODE_ANS_BAK
	select * from TBTRANSCODE_MGMT_LOG
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G200928504500024'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G200928504500024'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000032M9'

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-10-27' --爸媽 450 TSM 處裡中
	and (FSMESSAGE like '%G200928504500024%' or FSMESSAGE like '%146650%' or FSMESSAGE like '%196115%' or FSMESSAGE like '98081' or FSMESSAGE like '%000032M9%')
	order by FDTIME
end

if @case = 201610271435 begin --根據 Video ID 查詢未來的播出時間

	select CONVERT(char(10), FDDATE, 20) FDDATE, FSPLAY_TIME from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002HWY' and FDDATE >= getdate() - 1 union all
	select CONVERT(char(10), FDDATE, 20) FDDATE, FSPLAYTIME from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002HWY'  and FDDATE >= getdate() - 1
	order by FDDATE, FSPLAY_TIME

end


if @case = 201610281030 begin --轉檔失敗(non drop frame)
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '0000339G' --R  P201610006590001, file is still in Approved
	select * from TBPGM_PROMO where FSPROMO_ID = '20161000659' --小O事件簿73集PROMO
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201610006590001'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201610006590001'
	select * from TBLOG_VIDEO where FSID = '20161000659'  --todo 檢查是否有置換
end

if @case = 201610281426 begin  --TSM 處裡中 Anystream 未收到任務, TSM_RECALL error
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-10-27' --一修和尚 242 TSM 處裡中
	and (FSMESSAGE like '%G201558202420002%' or FSMESSAGE like '%146675%' or FSMESSAGE like '%00338J%' or FSMESSAGE like '%196140%')
	order by FDTIME

	select * from TBTRACKINGLOG where FDTIME between '2016-10-27 14:55' and '2016-10-27 14:59' order by FDTIME
	select * from TBTRACKINGLOG where FDTIME between '2016-10-27 09:55' and '2016-10-27 23:59' and FSCATALOG = 'ERROR'  order by FDTIME
	select * from TBTSM_JOB where FNTSMJOB_ID = '196140'


	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '0000338J' --2016-11-08
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201558202420002'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201558202420002'

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-10-28' --一修和尚 242 TSM 處裡中
	and (FSMESSAGE like '%G201558202420002%' or FSMESSAGE like '%146847%' or FSMESSAGE like '%00338J%' or FSMESSAGE like '%196394%' or FSMESSAGE like '%98252%')
	order by FDTIME
end


if @case = 201610281502 begin -- SD 分軌帶卡在 ANS 處裡中
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-10-27'
	and (FSMESSAGE like '%G000169000050001%' or FSMESSAGE like '%146676%' or FSMESSAGE like '%196141%')
	order by FDTIME

	select * from TBLOG_VIDEO where FSFILE_NO = 'G000169000050001' --S, 201610270073  -> T

	select * from TBTSM_JOB where FNTSMJOB_ID = '196141'-- Node08, FNSTATUS = 3

	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G000169000050001'

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-10-28'
	and (FSMESSAGE like '%G000169000050001%' or FSMESSAGE like '%146676%' or FSMESSAGE like '%196141%')
	order by FDTIME

end

if @case = 201611011055 begin --Louth DB
	select FSFILE_NO, FSVIDEO_PROG from TBLOG_VIDEO where FSVIDEO_PROG in ('000031WU', '00001KYD', '00001KYE', '000031TZ', '00001VBP', '00002X61')
end

if @case = 201611011210 begin  --村民大會 489, 495 送檔轉檔 ANS 處裡錯誤
	
	--489
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-11-01'
	and (FSMESSAGE like '%G200626504890002%' or FSMESSAGE like '%146962%' or FSMESSAGE like '%196797%' or FSMESSAGE like '%98367%')
	order by FDTIME

	select * from TBLOG_VIDEO where FSFILE_NO = 'G200626504890002' --201611010006, S
	select * from TBUSERS where FSUSER_ID = '50841' --劉家彤
	select * from TBBROADCAST where FSBRO_ID = '201611010006'



	--495 強制失敗，重啟轉檔
	select * from TBLOG_VIDEO where FSFILE_NO = 'G200626504950004' --數位轉檔(網路)-檔案名稱： 村民大會第495集 尼伯特災
	
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-11-01'
	and (FSMESSAGE like '%G200626504950004%' or FSMESSAGE like '%147017%' or FSMESSAGE like '%196892%' or FSMESSAGE like '%98422%')
	order by FDTIME

	--499
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-11-01'
	and (FSMESSAGE like '%G200626504990002%' or FSMESSAGE like '%147015%' or FSMESSAGE like '%196890%' or FSMESSAGE like '%98420%' or FSCATALOG = 'ERROR')
	order by FDTIME
end

if @case = 201611021348 begin
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '000033TG'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000033TG'
end


if @case = 201611021603 begin --檔案有排播，但是會被刪除 
	select * from TBLOG_VIDEO where FSVIDEO_PROG in ('00002ZWM', '0000306B') --> FSARC_TYPE = 001
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID in ('00002ZWM') order by FDDATE --P201609004690001
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID in ('0000306B') order by FDDATE --P201609005730001
	exec SP_Q_GET_PLAY_VIDEOID_LIST --not found 00002ZWM and 0000306B

	select * from TBPROG_M where FSPROG_ID = '2016239'   
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201654600070004' --no Video ID
end

if @case = 201611021617 begin  -- 無法審核
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '0000336O' --N
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '0000336O' --2016-11-04 07:58:30:04 ch12
	--> 可以審核
end

if @case = 201611021622 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000033TJ'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '000033TJ'
	--檔案仍在 MarkUpload 檔名未改
end

if @case = 201611221218 begin --這麼晚誰還在標記上傳?
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('0000345Z', '000033TH', '00002WK4', '000035KQ', '00003467')
end


if @case = 201611241447 begin --發現南台灣 #2 由片庫獨立轉檔標記上傳，有在排隊但是正興先手動上傳到 Review
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000035QD'
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000035QD'
end

if @case = 201611250950 begin
	select *  from TBLOG_VIDEO where FSVIDEO_PROG = '0000336M'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '0000336M' --C 2016-11-23 09:10:48.087
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '0000336M' --戴帽子的貓 II #6
	--檔案沒放到 HDUpload 一共有9 支
	select * from TBLOG_VIDEO where FSFILE_NO like 'G20142250011%'
	select * from TBLOG_VIDEO_HD_MC where FSFILE_NO = 'G201422500110002'
end

if @case = 201611251024 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('000035M2', '000035KT', '00002WKB')
end

if @case = 201612081041 begin
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '0000300C' --T, not in Review and Approved, P201609005160001
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '0000300C' -- 未排播
end

if @case = 201612090447 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000036QG' --2016-12-06 17:20:59.457 -> O
end

if @case = 201612101044 begin
	select * from TBLOG_VIDEO where FSVIDEO_PROG in ('00002XEU', '00002XF6', '00002ZBP', '00002K08')  -- all are in T
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in ('00002XEU', '00002XF6', '00002ZBP', '00002K08')  --not schedule
	
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID in ('00002XEU', '00002XF6', '00002ZBP') order by FDDATE
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID in ('00002XEU', '00002XF6', '00002ZBP') order by FDDATE

	select * from TBBROADCAST where FSBRO_ID = '201605160059'

	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002RUS'
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002RUS'
end

if @case = 201612120927 begin --ans 轉檔完成 MAM 轉檔中
	select * from TBTSM_JOB where FNTSMJOB_ID in (202369, 202365, 202356, 202353, 202361, 202368) order by FDREGISTER_TIME
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '000036Q6' --FSJOB_ID = 149954, P201612000190001, 201612010086
	select * from TBBROADCAST where FSBRO_ID = '201612010086' --2016-12-10 17:31:22.370

	select * from TBTRANSCODE where FNTRANSCODE_ID = 149954 -- FNTSMDISPATCH_ID = 202297, FNPROCESS_STATUS = 2

	--查詢速度很慢
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-10'
	and (FSMESSAGE like '%P201612000190001%' or FSMESSAGE like '%149954%' or FSMESSAGE like '%0036Q6%' )
	order by FDTIME

	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000036Q6' --2016-12-12 25:49:55:11
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201612000190001'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201612000190001'

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-12'
	and (FSMESSAGE like '%P201612000190001%' or FSMESSAGE like '%150001%' or FSMESSAGE like '%202454%' or FSMESSAGE like '%101324%' or FSMESSAGE like '%0036Q6%' )
	order by FDTIME

	select * from TBTSM_JOB where FNTSMJOB_ID = 202454 --FNSTATUS = 3

	select * from TBTSM_JOB where FDREGISTER_TIME between CAST(GETDATE() -1 as DATE) and GETDATE() order by FDREGISTER_TIME

	select * from TBLOG_VIDEO where FSFILE_NO = 'G201734800240001' --000030QK
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000030QK' --12/25

	select * from TBTSM_JOB where CAST(FDREGISTER_TIME as DATE) = '2016-12-12' and FSSOURCE_PATH like '%G201699900120001%' --FNSTATUS = 3
	select * from TBTSM_JOB where CAST(FDREGISTER_TIME as DATE) = '2016-12-12' and FSSOURCE_PATH like '%G201689300010003%' --FNSTATUS = 3

	select * from TBTSM_JOB where FNTSMJOB_ID = 202476 --FDREGISTER_TIME = 2016-12-12 12:23:11.317

	select * from TBTSM_JOB where FDREGISTER_TIME between CAST(GETDATE() -4 as DATE) and GETDATE()  and FNSTATUS = 2 order by FDREGISTER_TIME

	select * from TBTSM_JOB where FDREGISTER_TIME > '2015-12-01' and FSSOURCE_PATH like '%G000006019530001%' order by FDREGISTER_TIME

end

if @case = 201612131243 begin --002UFI 無法從 Tape 取回 GPFS
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002UFI' --\\mamstream\MAMDFS\ptstv\HVideo\0000\0000060\G000006019530001.mxf
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002UFI' --2016-12-22 17:30:00;00, 62
	select * from TBZCHANNEL

	--調用
	select * from TBTRACKINGLOG where FDTIME > '2016-12-13 10:40' and 
	(FSMESSAGE like '%51204%' or FSMESSAGE like 'G000006019530001' or FSMESSAGE like '002UFI' or ) order by FDTIME
end

if @case = 201612130920 begin
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002VH8' --ㄤ牯ㄤ牯咕咕咕(詔安腔) #166 2016-12-25
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002VH8' --R G200785201660002, 201608050153, 142873
	select * from TBBROADCAST where FSBRO_ID = '201608050153' --2016-09-02 10:39:50.127
	select * from TBTRANSCODE_RECV_LOG

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-09-02'
	and (FSMESSAGE like '%G200785201660002%' or FSMESSAGE like '%142873%' or FSMESSAGE like '%181114%' or FSMESSAGE like '%94499%' or FSMESSAGE like '%002VH8%' )
	order by FDTIME

	--Error : 低解搬檔時發生錯誤：重試次數：0;From T:\IngestWorkingTemp\G200785201660002.mp4 To \\mamstream\MAMDFS\Media\Lv\hakkatv\2007\2007852\G200785201660002.mp4，錯誤：來源檔案不存在

	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G200785201660002'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G200785201660002'
end

if @case = 201612131339 begin  --調用卡在 TSM 處理中  --partial 處理失敗
	select * from TBTSM_JOB where FNTSMJOB_ID = 202650 --2016-12-13 13:34:44.837
	select * from TBTSM_JOB where FDREGISTER_TIME between CAST(GETDATE() -1 as DATE) and GETDATE() and FNSTATUS = 2 order by FDREGISTER_TIME --39
	--先手動抓檔
	select * from TBLOG_VIDEO where FSFILE_NO in ('G201680200100002', 'G201680200110001', 'G201680200170012', 'G201554800070002', 'G201554800290006', 'G000006017410005', 'G201523100010015', 'G000006019530001')

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-13' 
	and (FSMESSAGE like '%G201680200100002%' or FSMESSAGE like '%150096%' or FSMESSAGE like '%202622%') order by FDTIME

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-13' 
	and (FSMESSAGE like '%G201680200100002%' or FSMESSAGE like '%150096%' or FSMESSAGE like '%202622%') order by FDTIME

	select * from TBTRACKINGLOG where FDTIME between '2016-12-13 10:55' and '2016-12-13 11:20' order by FDTIME
	--WSTSM/TSMRecallApp/fnGetTsmFileStatus	2016-12-13 10:59:04.423	WARNING   	查詢 SR_ServiceTSM.GetFileStatusByTsmPath 時間異常：timeS = 2016/12/13 上午 10:58:51, timeE = 2016/12/13 上午 10:59:04

	select * from TBTRACKINGLOG where FDTIME between '2016-12-13 10:35' and '2016-12-13 11:10' order by FDTIME


	select * from TBLOG_VIDEO where FSFILE_NO = 'G201680200110001' -- 誰來晚餐 11, \\mamstream\MAMDFS\ptstv\HVideo\2016\2016802\G201680200110001.mxf
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201680200170012' -- 誰來晚餐 17, \\mamstream\MAMDFS\ptstv\HVideo\2016\2016802\G201680200170012.mxf

end

if @case = 201612131516 begin --轉檔成功搬檔失敗
	select * from TBPROG_M where FSPGMNAME like '%毛起來%' --毛起來幸福站, 2017088
	select * from TBLOG_VIDEO where FSID = '2017088' and FNEPISODE = 11 --149966, R, 201612080035, 000037AI, G201708800110001
	select * from TBBROADCAST where FSBRO_ID = '201612080035' --2016-12-11 14:33:42.163

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-11'
	and (FSMESSAGE like '%G201708800110001%' or FSMESSAGE like '%149966%' or FSMESSAGE like '%202389%' or FSMESSAGE like '%101289%' or FSMESSAGE like '%0037AI%' )
	order by FDTIME
	 
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000037AI' --2016-12-16

	--Reschedule
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-13'
	and (FSMESSAGE like '%G201708800110001%' or FSMESSAGE like '%149966%' or FSMESSAGE like '%202389%' or FSMESSAGE like '%101289%' or FSMESSAGE like '%0037AI%' )
	order by FDTIME
	--2016-12-13 16:47:04.063 : hang on plink開始進行高解搬檔：Job_ID：149966
	select * from TBTRACKINGLOG where FDTIME between '2016-12-13 16:40' and '2016-12-13 16:55' order by FDTIME

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-13' and FSCATALOG = 'ERROR' order by FDTIME --157

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-9' and FSCATALOG = 'ERROR' order by FDTIME 

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-13' and FSPROGRAM  like '%WSTSM/TSMRecallAPP%' order by FDTIME

	--reschedule #2
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-14'
	and (FSMESSAGE like '%G201708800110001%' or FSMESSAGE like '%149966%' or FSMESSAGE like '%202389%' or FSMESSAGE like '%101289%' or FSMESSAGE like '%0037AI%' )
	order by FDTIME

	select * from TBTRACKINGLOG where FDTIME > '2016-12-14 16:12' order by FDTIME

end

if @case = 201612132326 begin
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002N6O' --台灣美樂地MV-起肖的風HD, P201606004330001, \\mamstream\MAMDFS\ptstv\HVideo\2016\20160600433\P201606004330001.mxf
end

if @case = 201612141658 begin --TSM 搬檔數量
	select CAST(FDREGISTER_TIME as DATE), COUNT(FNTSMJOB_ID)from TBTSM_JOB where FDREGISTER_TIME > '2016-07-01' 
	group by CAST(FDREGISTER_TIME as DATE) order by CAST(FDREGISTER_TIME as DATE)

	select CAST(FDUPDATED_DATE as DATE) 'DATE', count(FSFILE_NO) from TBLOG_VIDEO where FDUPDATED_DATE > '2016-11-01' and FCFILE_STATUS = 'T' 
	group by CAST(FDUPDATED_DATE as DATE) order by CAST(FDUPDATED_DATE as DATE)
end

if @case = 201612141407 begin --低解檔名是 wmv 造成搬檔失敗
	select * from TBBROADCAST where FSBRO_ID = '201502020030' --2016-12-14 10:39:42.343

	select * from TBLOG_VIDEO where FSBRO_ID = '201502020030' --R, G200928511010001, \\mamstream\MAMDFS\ptstv\HVideo\2009\2009285\G200928511010001.mxf, 00001OFV
	--\\mamstream\MAMDFS\Media\Lv\ptstv\2009\2009285\G200928511010001.wmv
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-14'
	and (FSMESSAGE like '%G200928511010001%' or FSMESSAGE like '%150172%' or FSMESSAGE like '%202732%' or FSMESSAGE like '%101485%' or FSMESSAGE like '%001OFV%')
	order by FDTIME

	/*
	低解搬檔時發生錯誤：From T:\IngestWorkingTemp\G200928511010001.wmv To \\mamstream\MAMDFS\Media\Lv\ptstv\2009\2009285\G200928511010001.wmv，錯誤：System.IO.FileNotFoundException: Could not find file 'T:\IngestWorkingTemp\G200928511010001.wmv'.
File name: 'T:\IngestWorkingTemp\G200928511010001.wmv'
   at System.IO.__Error.WinIOError(Int32 errorCode, String maybeFullPath)
   at System.IO.File.InternalCopy(String sourceFileName, String destFileName, Boolean overwrite)
   at System.IO.File.Copy(String sourceFileName, String destFileName, Boolean overwrite)
   at PTS_MAM3.Web.DataSource.Handler_ReceiveTransCode.fnMoveLowResFile(String sourceFile, String targetFile) in E:\PTSMAM_Source\PTS_MAM\PTS_MAM3.Web\DataSource\Handler_ReceiveTransCode.ashx.cs:line 243
   */
   
   --早期建的轉檔單低解是轉 wmv?
   UPDATE TBLOG_VIDEO set FSFILE_PATH_L = '\\mamstream\MAMDFS\Media\Lv\ptstv\2009\2009285\G200928511010001.mp4' where FSFILE_NO = 'G200928511010001'
   update TBLOG_VIDEO set FSFILE_TYPE_LV = 'mp4' where FSFILE_NO = 'G200928511010001'


   select * from TBLOG_VIDEO where FSID = '2009285' and FSARC_TYPE = '002' and FSFILE_TYPE_LV = 'wmv' order by FSFILE_NO --15 筆
   select * from TBLOG_VIDEO where FSBRO_ID in (select FSBRO_ID from TBLOG_VIDEO where FSID = '2009285' and FSARC_TYPE = '002' and FSFILE_TYPE_LV = 'wmv') order by FSBRO_ID --15 筆
   --並無一單有 SD 和 HD 的情況
end

if @case = 201612141740 begin
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in ('000030QX', '0000346E', '000020KP') order by FDDATE
	select * from TBLOG_VIDEO where FSVIDEO_PROG in ('000030QX', '0000346E', '000020KP')
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000030QX' -- O, B
end

if @case = 201612211021 begin --todo reschedule
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201207800890010' --000038BF, S

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-22'
	and (FSMESSAGE like '%G201207800890010%' or FSMESSAGE like '%150660%' or FSMESSAGE like '%203578%' or FSMESSAGE like '%101947%' or FSMESSAGE like '%0038BF%' )
	order by FDTIME
end

if @case = 201612250912 begin --轉檔後搬檔失敗
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002S2I' --2016-08-03 16:06:20.830
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002S2I' --R, G200618607590002, 201607150065 --> T
	select * from TBBROADCAST where FSBRO_ID = '201607150065' --2016-08-03 16:06:28.470
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002S2I' --奧林P客#759 2017-01-07 08:00:00;00

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-08-03'
	and (FSMESSAGE like '%G200618607590002%' or FSMESSAGE like '%139880%' or FSMESSAGE like '%182795%' or FSMESSAGE like '%92289%' or FSMESSAGE like '%002S2I%' )
	order by FDTIME
	--plink高解搬檔遇未預期錯誤：/mnt/HDUpload/002S2I.mxf /控制代碼無效。  18T 已無檔案

	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G200618607590002'
    update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G200618607590002'
end

if @case = 201612301345 begin
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002JF5' --T, G200618607590002, \\mamstream\MAMDFS\hakkatv\HVideo\2006\2006186\G200618607590002.mxf
	--目前在 TSM-RECALL 狀態
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002JF5' --我家是戰國 #22, 1/2 play
end

if @case = 201612311034 begin
	--review to approved pending
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in  ('000036DE', '00003583', '000033OT', '000039ER', '000039EW', '000039FJ', '0000392V', '0000399R')
	--TSM RECALL pending
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in  ('00002TVXE', '00001UGH', '00002OQ0', '000034HC')

	--restart AP 系統在搬 12/31 已播出檔案？ why?
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in  ('00003859', '00003857', '0000397H', '0000396O', '00003980', '0000397I', '0000385S', 
	'0000385O', '00003911', '0000397C', '0000397J', '00003912')

	select * from TBPGM_COMBINE_QUEUE where  FSVIDEO_ID in  ('00003859', '00003857', '0000397H', '0000396O', '00003980', '0000397I', '0000385S', 
	'0000385O', '00003911', '0000397C', '0000397J', '00003912') order by FDDATE, FSCHANNEL_ID, FSPLAY_TIME

     select * from TBLOG_VIDEO_HD_MC where  FSVIDEO_ID in  ('00003859', '00003857', '0000397H', '0000396O', '00003980', '0000397I', '0000385S', 
	'0000385O', '00003911', '0000397C', '0000397J', '00003912') -- 都是 12/30 審核通過

	select * from TBUSERS where FSUSER_ID = '51256' --林怡慧

	select * from TBPGM_COMBINE_QUEUE where  FSVIDEO_ID in  ('00003857') order by FDDATE, FSCHANNEL_ID, FSPLAY_TIME  --上家下屋(22、23不重播(內容與過年有關)) #!
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00003857' --2016-12-30 11:48:18.543 -> O

	select * from TBPGM_COMBINE_QUEUE where  FSVIDEO_ID in  ('00003859') order by FDDATE, FSCHANNEL_ID, FSPLAY_TIME --上家下屋(22、23不重播(內容與過年有關)) #2
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00003859' --2016-12-30 11:48:18.543 -> O

	select * from TBPGM_COMBINE_QUEUE where  FSVIDEO_ID in  ('0000397H') order by FDDATE, FSCHANNEL_ID, FSPLAY_TIME --明天一起去樂園 #13
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '0000397H' --2016-12-30 11:29:56.900 -> O

	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000034HC'
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000036QI'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000036QI' --2016-12-10 19:29:29.420 : O
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '000036QI' --R

end

if @case = 201701032310 begin --2016世界羽聯大獎賽-蘇格蘭公開賽 8 轉檔成功搬檔失敗
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '000036QI'  --R, \\mamstream\MAMDFS\ptstv\HVideo\2016\2016985\G201698500080002.mxf, G201698500080002, 201612010098  -> T
	select * from TBBROADCAST where FSBRO_ID = '201612010098' --2016-12-10 19:29:30.447
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000036QI' --2017-01-10 ch62, 2016世界羽聯大獎賽-蘇格蘭公開賽 8

	select * from TBZCHANNEL

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-10'
	and (FSMESSAGE like '%G201698500080002%' or FSMESSAGE like '%149960%' or FSMESSAGE like '%202303%' or FSMESSAGE like '%101283%' or FSMESSAGE like '%0036QI%' )
	order by FDTIME

	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201698500080002'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201698500080002'

	--搬檔失敗
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-01-04'
	and (FSMESSAGE like '%G201698500080002%' or FSMESSAGE like '%151788%' or FSMESSAGE like '%205709%' or FSMESSAGE like '%103005%' or FSMESSAGE like '%0036QI%' )
	order by FDTIME

	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201698500080002'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201698500080002'

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-01-05'
	and (FSMESSAGE like '%G201698500080002%' or FSMESSAGE like '%151930%' or FSMESSAGE like '%205960%' or FSMESSAGE like '%103137%' or FSMESSAGE like '%0036QI%' )
	order by FDTIME
end

if @case = 201701041237 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('000037TF', '000037U6', '000037U7', '000037U8', '000038KS', '000038KT', '000038KU', 
	'000038KV', '000038KW', '000038KY', '000038KZ', '000038L0', '000038L8', '000038L9', '000038LB', '000038LN', '000036QF', '000036QJ')
end

if @case = 201701051538 begin
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002SJ3' --S, G201632100290001, 201607190154 -> T
	select * from TBBROADCAST where FSBRO_ID = '201607190154' --2016-09-02 10:56:53.083

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-09-02'
	and (FSMESSAGE like '%G201632100290001%' or FSMESSAGE like '%142876%' or FSMESSAGE like '%188118%' or FSMESSAGE like '%94496%' or FSMESSAGE like '%002SJ3%' )
	order by FDTIME

	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201632100290001'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201632100290001'


	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-01-05'
	and (FSMESSAGE like '%G201083200010002%' or FSMESSAGE like '%151930%' or FSMESSAGE like '%205960%' or FSMESSAGE like '%103137%' or FSMESSAGE like '%002SJ3%' )
	order by FDTIME

end


if @case = 201701100902 begin --轉檔成功搬檔失敗  HSM4, HSM5 18TB 未掛載，HSM4  restart metasand HSM5 reboot (restart metasand fail)
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201083200010002' --R, 00003A9D, \\mamstream\MAMDFS\ptstv\HVideo\2010\2010832\G201083200010002.mxf,\\mamstream\MAMDFS\Media\Lv\ptstv\2010\2010832\G201083200010002.mp4
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-01-09'
	and (FSMESSAGE like '%G201083200010002%' or FSMESSAGE like '%152165%' or FSMESSAGE like '%207404%' or FSMESSAGE like '%103360%' or FSMESSAGE like '%003A9D%' )
	order by FDTIME

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-01-10'
	and (FSMESSAGE like '%G201083200010002%' or FSMESSAGE like '%152165%' or FSMESSAGE like '%207404%' or FSMESSAGE like '%103360%' or FSMESSAGE like '%003A9D%' )
	order by FDTIME
end

if @case = 201701101108 begin --搬檔狀態未更新
	select * from TBPROG_M where FSPGMNAME = '甜心兔' --2016772
	select * from TBLOG_VIDEO where FSID = '2016772' and FNEPISODE in (83, 84, 85) --T, 000039J7, 000039J8, 000039JH :O
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in ('000039J7', '000039J8', '000039JH')
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('000039J7', '000039J8', '000039JH') -- 此為回朔轉檔，所以 TBPTSAP_STATUS_LOG 不會有紀錄 G201677200830001, G201677200840001

	--手動修改為待審
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201677200830001'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201677200840001'

	select * from TBPTSAP_STATUS_LOG where FSVIDEOID in ('000039J7', '000039J8', '000039JH')

	select * from TBPTSAP_STATUS_LOG where FDUPDATE_DATE >= '2017-01-10' order by FDUPDATE_DATE

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00003ACT', '00003AAT') --00003AAT C, 00003ACT 已標記，程式未上傳，仍在等待上傳，手動搬檔更新狀態
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in ('00003ACT', '00003AAT')
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID in ('00003ACT', '00003AAT')

	select * from TBLOG_VIDEO_HD_MC A join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where A.FSVIDEO_ID in ('0000240O', '00001XW0', '00001PV8', '00003AAR')

	--GPFS -> Review 沒有改成 N
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201559500130003'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201207801550001'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G000015907830001'

	--標記後未上傳
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000039W7'

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00003ACT', '00002NYC', '00003580') --O  檔案已在待播區主控仍抓不到

	select * from TBLOG_VIDEO_HD_MC A join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where A.FSVIDEO_ID in (
	'00001XHK', '00001XHM', '000039JI', '00003AB2', '00003AAY')

	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201512800150002'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201512800160002'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201677200860001'
end

if @case = 201701102322 begin
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002UA4' --\\mamstream\MAMDFS\hakkatv\HVideo\2015\2015809\G201580901410002.mxf
	select * from TBTSM_JOB_WORKER
	select * from TBTSM_JOB where FNTSMJOB_ID = 207512
end

if @case = 201701130813 begin
	select * from TBPROG_M where FSPGMNAME like '%音樂萬萬歲3%' --2015586
	select * from TBPROG_M where FSPGMNAME like '%寶島好手%' --2016602

	select * from TBLOG_VIDEO where FSID = '2015586' and FNEPISODE in (22, 25, 27) and FSARC_TYPE = '002' --S x3

	--高解搬檔失敗：/tcbuffer/18TB/IngestWorkingTemp/G201558600220002.mxf/10.13.210.15 -l root -pw TSMP@ssw0rdTSM "/Media/WorkingTemp/MoveMetaSANFile /tcbuffer/18TB/00000000.lock /tcbuffer/18TB/IngestWorkingTemp/G201558600220002.mxf /ptstv/HVideo/2015/2015586/G201558600220002.mxf"/(Error)Source /tcbuffer/18TB/IngestWorkingTemp/G201558600220002.mxf is not exist.
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-01-09'
	and (FSMESSAGE like '%G201558600220002%' or FSMESSAGE like '%152162%' or FSMESSAGE like '%207401%' or FSMESSAGE like '%103357%' or FSMESSAGE like '%000025KI%' )
	order by FDTIME

	--將狀態改為失敗, 重啟轉檔
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201558600220002' --201510150131
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201558600250004' --201511050082
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201558600270002' --201511190066

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-01-11'
	and (FSMESSAGE like '%G201558600220002%' or FSMESSAGE like '%152162%' or FSMESSAGE like '%207401%' or FSMESSAGE like '%103357%' or FSMESSAGE like '%000025KI%' )
	order by FDTIME

	select * from TBLOG_VIDEO where FSID = '2016602' and FNEPISODE = 1 and FSARC_TYPE = '002' --S, G201660200010005
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201660200010005' --201604010115

end

if @case = 201701131200 begin  -- 檔案無法由 GPFS 取回
	exec SP_Q_INFO_BY_VIDEOID '00002UA4'
end


if @case = 201701142203-201701102322 begin
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002UA4' --\\mamstream\MAMDFS\hakkatv\HVideo\2015\2015809\G201580901410002.mxf, T
	select * from TBTSM_JOB_WORKER
	select * from TBTSM_JOB where FNTSMJOB_ID = 207512
end



if @case = 201701161158-201612281739 begin  --唐朝小栗子 11 手動搬檔標記上傳
	select *  from TBLOG_VIDEO where FSVIDEO_PROG = '000037X1' --201612150084
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000037X1'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000037X1' --T
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000037X1'  --not schedule yet
end

if @case = 201701232133 begin --未排播，造成 Review -> Approved 卡住
	exec SP_Q_INFO_BY_VIDEOID '000037OD'
end

select * from TBLOG_VIDEO where FSFILE_NO = 'G201510500440013' --000034JM
select * from TBLOG_VIDEO_HD_MC where FSFILE_NO = 'G201510500440013' --2016-11-09 10:47:51.800 O