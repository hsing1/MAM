



if OBJECT_ID(N'tempdb.dbo.#TEST_CASE', N'P') IS NOT NULL
	DROP PROC #TEST_CASE;
GO

CREATE PROC #TEST_CASE
	@case integer, @subcase integer
AS

if @case = 0 BEGIN
	select * from TBZCHANNEL
END
else if @case = 1 BEGIN --查詢節目播出資訊
	if @subcase = 1 BEGIN
		select * from TBPGM_QUEUE where FDDATE > '2015-06-20' AND FSCHANNEL_ID='11'order by FDDATE DESC  --01 :pts 02:pts2 11:HD
		--select * from TBZCHANNEL
		select * from TBPGM_QUEUE where FDDATE = '2015-12-31' and FSCHANNEL_ID = '51' order by FSBEG_TIME
	END
	if @subcase = 2 begin
		select * from TBPGM_QUEUE where FSPROG_ID = '0000163'
		select * from TBPGM_QUEUE where FSPROG_NAME = '紀錄觀點'
		select * from TBPGM_QUEUE where FSPROG_NAME like '%孟山都%'
		select * from TBPGM_QUEUE where FSPROG_NAME like '%探春%'
		select * from TBPGM_QUEUE where FSPROG_NAME like '%青春的進擊%'
		select * from TBPGM_QUEUE where FSPROG_NAME like '%2012觀點短片展%'
		select * from TBPGM_QUEUE where FSPROG_NAME like '%給雨季的歌%'
		select * from TBPGM_QUEUE where FSPROG_NAME like '%觀點短片展%'
		select * from TBPGM_QUEUE where FSPROG_NAME like '%勝利催落去%' and FSCHANNEL_ID = '11' and FNEPISODE = 1
		select * from TBPGM_QUEUE where FSPROG_NAME like '愛的萬物論' and FSCHANNEL_ID = '02' and FNEPISODE = 3
		select * from TBPGM_QUEUE where FSPROG_NAME like '愛的萬物論' and FNEPISODE = 3 
		select * from TBPGM_QUEUE where FDDATE = '2016-4-1' and FSCHANNEL_ID = '11'
		select * from TBPGM_QUEUE where FSPROG_NAME like '%安藤%'
		select * from TBPGM_COMBINE_QUEUE where FDDATE = '2015-11-15'
		select * from TBPGM_COMBINE_QUEUE where FDDATE = '2015-9-2' and FSCHANNEL_ID = '01'
	
		select * from TBZCHANNEL
	end

	if @subcase = 2 begin -- 小溪邊的沙灘
		-- 如何查到該節目
		select * from TBPGM_QUEUE where FDDATE = '2016-4-22' and FSCHANNEL_ID = '11' --2016-04-20 15:00:39.270 更新
		select * from TBPGM_COMBINE_QUEUE where FDDATE = '2016-4-22' and FSCHANNEL_ID = '11' -- 2016-04-20 16:00:58.790 更新
		select * from TBTRAN_LOG where FDDATE between '2016/04/20 14:50' and '2016/04/20 15:10' order by FDDATE desc
	end

	if @subcase = 3 begin -- 片庫轉檔失敗
		select * from TBPGM_COMBINE_QUEUE where FSPROG_ID = '00001R05'
		select * from TBPGM_QUEUE where FSPROG_NAME like '%探索新美台灣%' and FNEPISODE = 6 and FSCHANNEL_ID = '11'
		select * from TBPGM_QUEUE where FSPROG_NAME like '%水果冰淇淋%' and FNEPISODE = 1841 and FSCHANNEL_ID = '11' --4/27 播出
		select * from TBPGM_QUEUE where FSPROG_NAME like '%打破皮涅塔(口述影像版)%' and FSCHANNEL_ID = '11' --4/30
		select * from TBPGM_QUEUE where FSPROG_NAME like '%臺灣資深藝術家%' and FSCHANNEL_ID = '11'

END --case 1

else if @case = 2 BEGIN --查詢主控派送資訊
	select top 100 * from TBPGM_MSSCOPY
END

else if @case = 3 BEGIN -- 查詢播出運行表, TBPGM_COMBINE_QUEUE 節目表 (已有破口 但無短帶資訊)
	select * from TBPGM_COMBINE_QUEUE where FSPROG_ID='2015001' -- -- 根據節目編號查詢主控運行表播出紀錄，有分段落
	select * from TBPGM_COMBINE_QUEUE where FDDATE='2014-12-04' and FSPROG_ID='2004085'
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002GC0' order by FDDATE desc
	select * from TBPGM_COMBINE_QUEUE where FSCHANNEL_ID = '51' and FDDATE = '2015-12-31' order by FSPLAY_TIME
END
else if @case = 4 BEGIN --查詢短帶相關
	if @subcase = 1 BEGIN  --根據和 Video ID 查詢主控運行表短帶播出紀錄
		select * from TBPGM_ARR_PROMO where FSVIDEO_ID='00002GTJ' order by FDDATE desc
		select * from TBZCHANNEL
	END

else if @case = 5 begin 
	if @subcase = 1 begin
		select * from TBPGM_QUEUE where FSCHANNEL_ID = '51' and FDDATE = '2015-12-31' order by FSBEG_TIME
		select * from TBPGM_ARR_PROMO where FDDATE = '2015-12-31' and FSCHANNEL_ID = '51'
		select * from TBPGM_BANK_DETAIL where FDBEG_DATE = '2015-12-31' and FSCHANNEL_ID = '51' order by FSBEG_TIME
		select * from TBPGM_QUEUE_INSERT_TAPE
		select * from TBPGM_ACTION
		select * from TBPGM_COMBINE_QUEUE where FDDATE = '2015-12-31' and FSCHANNEL_ID = '51'
	end
end

else if @case = 6 begin -- 建議刪除清單
	if @subcase = 1 begin -- 有FTP 寫入louth DB 的video id資料
		select * from TBLOG_MASTERCONTROL_FTP where FSTIME = '20160106'
		select FSVIDEO_ID, count(FSVIDEO_ID) from TBLOG_MASTERCONTROL_FTP group by FSVIDEO_ID
		select * from TBLOG_MASTERCONTROL_FTP where FSVIDEO_ID = 'IMPI0101'
		select * from TBLOG_MASTERCONTROL_FTP where FSCHANNEL_ID  = '99'
	end
	else if @subcase = 2 begin
		select * from TBLOG_VIDEO_SEG order by FSFILE_NO
	end
	else if @subcase = 3 begin
		select * from TBLOG_VIDEO
		-- 查詢有置換過的節目
		select FSID, FNEPISODE, count(FSFILE_NO) from TBLOG_VIDEO group by FSID, FNEPISODE having count(FSFILE_NO) > 1
	end
	if @subcase = 4 begin
		select * from TBPROG_M
	end
	if @subcase = 5 begin
		select distinct A.FSVIDEO_ID AS FSVIDEOID ,D.FSPGMNAME AS PROGNAME ,C.FNEPISODE, B.FNSEG_ID
		from TBLOG_MASTERCONTROL_FTP A
		left join TBLOG_VIDEO_SEG B on A .FSVIDEO_ID = B.FSVIDEO_ID
		left join TBLOG_VIDEO C on B .FSFILE_NO = C.FSFILE_NO
		join TBPROG_M D on C.FSID = D. FSPROG_ID
	end
	else if @subcase = 5 begin
		select * from TBLOG_VIDEO

end -- case 6

if @case = 7 begin --短帶資料
	if @subcase = 1 begin -- 查詢 RM 到期刪除的清單
	    select FSPROMO_ID as '短帶編號', FSPROMO_NAME as '短帶名稱', FDEXPIRE_DATE '到期日' from TBPGM_PROMO where FSPROMO_NAME like '%RM%' and FDEXPIRE_DATE between '2016-1-12' and '2016-6-30' and FSEXPIRE_DATE_ACTION = 'Y' order by FDEXPIRE_DATE
		select FSPROMO_ID as '短帶編號', FSPROMO_NAME as '短帶名稱', FDEXPIRE_DATE '到期日' from TBPGM_PROMO where FSPROMO_NAME like '%RM%' and FSPROMO_NAME not like '%HD%' and FDEXPIRE_DATE between '2016-1-12' and '2016-6-30' and FSEXPIRE_DATE_ACTION = 'Y' order by FDEXPIRE_DATE
		select FSPROMO_ID as '短帶編號', FSPROMO_NAME as '短帶名稱', FDEXPIRE_DATE '到期日' from TBPGM_PROMO where FSPROMO_NAME like '%HD%'  and FDEXPIRE_DATE between '2016-1-12' and '2016-6-30' and FSEXPIRE_DATE_ACTION = 'Y' order by FDEXPIRE_DATE
	end
	if @subcase = 2 begin
		select * from TBPGM_PROMO
	end

END -- case 7

-- FCFILE_STATUS B:待轉檔 D:刪除 F:取消（送帶轉檔單不通過）R:轉檔失敗 S:轉檔中 T:轉檔完成 X:刪除 Y:入庫（停用）
if @case = 8 begin 
	if @subcase = 1 begin -- 查詢由片庫轉檔的節目 
		select * from TBPGM_QUEUE where FDDATE = '2016-4-1' and FSCHANNEL_ID = '11'
		
		select * from TBLOG_VIDEO_D where FCFILE_STATUS = 'T' and FSTYPE = 'G'
		select * from TBLOG_VIDEO where FSFILE_NO = 'G201542700120003'
		select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201542700120003'
		select * from TBBROADCAST where FSBRO_ID = '201501280035'

	if @subcase = 2	-- 根據 Video id 查播出日期
		select * from TBPGM_COMBINE_QUEUE where (FSVIDEO_ID = '000028J0' or FSVIDEO_ID = '00002FLG') and FDDATE > GETDATE()
		select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002H1S' and FDDATE > GETDATE() order by FDDATE
		select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '000028J0' or FSVIDEO_ID = '00002F7X' and FDDATE > CAST(CURRENT_TIMESTAMP AS DATE)
		select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002HZT' and FDDATE > '2016/04/14'
		select * from TBPGM_COMBINE_QUEUE where FDDATE = '2016-04-25' order by FSPLAY_TIME
	end
	else if @subcase = 3 -- 根據節目名稱查詢播出資訊
		select * from TBPGM_COMBINE_QUEUE where FDDATE > CAST(CURRENT_TIMESTAMP AS DATE) and FSCHANNEL_ID = '11' and FSPROG_NAME like '%再見女兒%' order by FDDATE, FNCONBINE_NO 
		select * from TBPGM_COMBINE_QUEUE where FDDATE > CAST(CURRENT_TIMESTAMP AS DATE) and FSCHANNEL_ID = '01' and FSPROG_NAME like '%滾石%' order by FDDATE, FNCONBINE_NO
		select * from TBPGM_COMBINE_QUEUE where FDDATE > CAST(CURRENT_TIMESTAMP AS DATE) and FSCHANNEL_ID = '11' and FSPROG_NAME like '%水果冰淇淋%' order by FDDATE, FNCONBINE_NO 
		select * from TBPGM_COMBINE_QUEUE where FDDATE = '2016-04-18' and FSCHANNEL_ID = '11' and FSPROG_NAME like '%水果冰淇淋%' order by FDDATE, FNCONBINE_NO 
		select * from TBPROG_M where FSPGMNAME like '%滾石%'
		select * from TBPGM_COMBINE_QUEUE where FDDATE = '2016-04-18' and FSCHANNEL_ID = '11' order by FSPLAY_TIME
		select * from TBTRAN_LOG where FDDATE between '2016/04/15' and '2016/04/17'  order by FDDATE
		select * from TBTRAN_LOG where FSSP_NAME like '%TBPGM_COMBINE%' order by FDDATE
		select * from TBTRAN_LOG where FDDATE between '2015/04/15' and '2016/04/17'  and FSPARAMETER like '51272'  order by FDDATE
		select * from TBTRAN_LOG where FDDATE between '2016/04/15' and '2016/04/17'  and FSUSER_ID like '51272'  order by FDDATE
		select * from TBUSERS where FSUSER_ChtName = '孫祖琪' or FSUSER_ChtName = '謝佳紋' or FSUSER_ID = '50311'
		select * from TBTRAN_LOG where CAST(FDDATE as DATE) = '2016/04/16' order by FDDATE desc
		select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016/04/16' order by FDTIME desc

		select GETDATE()
		select CAST(CURRENT_TIMESTAMP AS DATE)
	end

	else if @subcase = 4 -- 根據播出日期查詢節目表
		select * from TBPGM_COMBINE_QUEUE where FDDATE = '2016-4-11' and FSCHANNEL_ID = '11'
	end
	else if @subcase = 5 -- 根據播出日期查詢短帶插播
		select pr.FSPROMO_NAME, ta.* from TBPGM_ARR_PROMO ta inner join TBPGM_PROMO pr on ta.FSPROMO_ID = pr.FSPROMO_ID
		where ta.FDDATE = '2016-4-11' and ta.FSCHANNEL_ID = '11' order by FNARR_PROMO_NO

		select p.FSPROMO_NAME, ta.* from TBPGM_ARR_PROMO ta inner join TBPGM_PROMO p on ta.FSPROMO_ID = p.FSPROMO_ID where p.FSPROMO_NAME like '%滾石%' and ta.FDDATE > GETDATE() order by FDDATE

		select p.FSPROMO_NAME, ta.* from TBPGM_ARR_PROMO ta inner join TBPGM_PROMO p on ta.FSPROMO_ID = p.FSPROMO_ID where ta.FSVIDEO_ID = '00002H1E' order by FDDATE
		select * from TBPGM_PROMO
		select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002H6J'
	end
	else if @subcase = 6 -- 根據 video id 查詢轉檔時間
		select t.FSID, p.FSPGMNAME '節目名稱', t.FNEPISODE '集數', t.FSFILE_NO '檔案編號', t.FSVIDEO_PROG '主控編號', t.FDCREATED_DATE '建單日期', td.FDCREATED_DATE '轉檔開始時間', td.FDUPDATED_DATE '轉檔結束時間'   
		from (TBLOG_VIDEO t join TBLOG_VIDEO_D td on t.FSFILE_NO = td.FSFILE_NO)
		join  TBPROG_M p on t.FSID = p.FSPROG_ID  where t.FSVIDEO_PROG = '00002H6J' and t.FSARC_TYPE = '002'

		select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00001H8S' and FDDATE = '2016-4-12' -- 收播並無轉檔單 TBLOG_VIDEO 並不會有資料
		select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00001KP5'

		-- 根據日期、頻道、 Video ID 查詢節目表維護資料及轉檔狀態
		select v.FSFILE_NO, v.FSARC_TYPE, v.FSARC_TYPE, v.FCFILE_STATUS, q.* from 
		TBPGM_COMBINE_QUEUE q left join TBLOG_VIDEO v on q.FSVIDEO_ID = v.FSVIDEO_PROG where q.FSVIDEO_ID = '00002H6H' and q.FDDATE = '2016-04-12' and q.FSCHANNEL_ID = '11' -- and v.FCFILE_STATUS = 'B'

		select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002H6H' and FDDATE = '2016-04-12' and FSCHANNEL_ID = '11'
		select * from TBPGM_COMBINE_QUEUE where FDDATE = '2016-04-12' and FSCHANNEL_ID = '11' order by FSPLAY_TIME
		select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002H6H'
	end

END -- case 8

if @case = 100 BEGIN

---------------------------------------------------------------------------------------------------------------

--執行 插入短帶＼列印主控運行表 就會有資料
select * from RPT_DATE_LST where 播出日期='2014-12-04' and QUERY_BY = '朱幸一' order by 播出時間

select * from TBPGM_COMBINE_QUEUE where FDDATE= '2014-12-04' order by FSPLAY_TIME asc



-- 統計單日長帶節目數量
select count (distinct( FSPROG_ID)) from TBPGM_COMBINE_QUEUE where (FDDATE >'2014-11-01'  and FDDATE<'2014-11-30') and FCPROPERTY='G'
select count (distinct( FSPROG_ID)) from TBPGM_COMBINE_QUEUE where FDDATE = '2014-12-04'
select count (distinct FSPROG_ID) from TBPGM_COMBINE_QUEUE where FDDATE = '2014-12-04'
select count (FNEPISODE) from TBPGM_COMBINE_QUEUE where FDDATE = '2014-12-04' group by FSPROG_ID
select count (distinct(FNEPISODE)), FSPROG_ID, FNEPISODE from TBPGM_COMBINE_QUEUE where FDDATE = '2014-12-04' group by FSPROG_ID, FNEPISODE order by FSPROG_ID

select FSPROG_ID, FNEPISODE from TBPGM_COMBINE_QUEUE where FDDATE = '2014-12-04' group by FSPROG_ID, FNEPISODE order by FSPROG_ID
select FSPROG_ID, FNEPISODE from TBPGM_QUEUE where FDDATE = '2014-12-04' group by FSPROG_ID, FNEPISODE order by FSPROG_ID
select FSPROG_ID, FNEPISODE from TBPGM_QUEUE where FDDATE='2014-12-04' order by FSPROG_ID

-- 查詢節目播出資訊

select * from TBPGM_QUEUE where FDDATE='2015-5-20' and FSPROG_ID='2004085' AND FSCHANNEL_ID='01' order by FSBEG_TIME
select * from TBPGM_QUEUE where FDDATE='2015-5-20' AND FSCHANNEL_ID='01' order by FSBEG_TIME
select * from TBPGM_QUEUE where FSPROG_ID='2013778' -- 根據節目編號查詢節目表維護播出紀錄
Select FSOLDTABLENAME from TBZCHANNEL where FSCHANNEL_ID='01'
Select * from TBZCHANNEL



select * from TBPGM_QUEUE where FDDATE='2014-12-04' order by FSBEG_TIME asc
select count(FNEPISODE) from TBPGM_QUEUE where FDDATE='2014-12-04'

select count(distinct FSPROG_ID) from TBPGM_QUEUE where FDDATE='2014-12-04'

--查詢播出運行表的短帶
select pa.*, p.FSPROMO_NAME from TBPGM_ARR_PROMO pa , TBPGM_PROMO p where pa.FSPROMO_ID=p.FSPROMO_ID and pa.FDDATE = '2014-12-04' order by FNARR_PROMO_NO
select pa.FSPLAYTIME, pa.FSVIDEO_ID, pa.FSFILE_NO, p.* from TBPGM_ARR_PROMO pa , TBPGM_PROMO p where pa.FSPROMO_ID=p.FSPROMO_ID and pa.FDDATE = '2014-12-04' order by FNARR_PROMO_NO

--查詢播出運行表的短帶中有公益託播單號者：播出紀錄
select ch.FSCHANNEL_NAME, pa.FSPLAYTIME, pa.FSVIDEO_ID, pa.FSFILE_NO, p.*
from TBPGM_ARR_PROMO pa , TBPGM_PROMO p, TBZCHANNEL ch
where pa.FSPROMO_ID=p.FSPROMO_ID and p.FSWELFARE<>'' and pa.FSCHANNEL_ID=ch.FSCHANNEL_ID order by FNARR_PROMO_NO

select pa.FDDATE as 播出日期, pa.FSPLAYTIME as 播出時間, p.FSDURATION as 播出長度, p.FSWELFARE 託播單號, p.FSPROMO_NAME 短帶名稱 from TBPGM_ARR_PROMO pa , TBPGM_PROMO p where pa.FSPROMO_ID=p.FSPROMO_ID and p.FSWELFARE<>'' order by pa.FDDATE, pa.FSPLAYTIME


/*
 * 根據短帶查詢播出紀錄
 */
select ch.FSCHANNEL_NAME, pa.FSPLAYTIME, pa.FSVIDEO_ID, pa.FSFILE_NO, p.*
from TBPGM_ARR_PROMO pa , TBPGM_PROMO p, TBZCHANNEL ch
where pa.FSPROMO_ID=p.FSPROMO_ID and pa.FSCHANNEL_ID=ch.FSCHANNEL_ID and (pa.FSPROMO_ID='20150200203' or p.FSPROMO_NAME like '爸媽囧很大 1104%')


--查詢播出運行表的短帶中有公益託播單號者：播出次數
select count(distinct pa.FSPLAYTIME) as  播出次數, p.FSWELFARE as 公益託播單號, p.FSPROMO_NAME  as 短帶名稱  from TBPGM_ARR_PROMO pa , TBPGM_PROMO p where pa.FSPROMO_ID=p.FSPROMO_ID and p.FSWELFARE<>'' group by p.FSWELFARE, p.FSPROMO_NAME

select count(FNARR_PROMO_NO),  FNARR_PROMO_NO,  FSPROMO_ID from TBPGM_ARR_PROMO  where  FDDATE = '2014-12-04' group by FSPROMO_ID, FNARR_PROMO_NO order by FNARR_PROMO_NO

--統計短帶播出的次數
select count(FNARR_PROMO_NO),   FSPROMO_ID from TBPGM_ARR_PROMO  where  FDDATE = '2014-12-04' group by FSPROMO_ID



/*
 * 查詢短帶播出資料
 */
select * from TBPGM_PROMO where FSPROMO_NAME like '爸媽囧很大 1104%' --根據名稱查詢短帶資料

select * from TBPGM_PROMO where FSPROMO_ID='20150200203' --根據短帶編號查詢短帶資料
select * from TBPGM_PROMO_BOOKING where FSPROMO_ID= '20150200203'
select count(FNARR_PROMO_NO) from TBPGM_ARR_PROMO
select * from TBPGM_ARR_PROMO where FDDATE='2015-05-12' and FSVIDEO_ID='00001KX1' order by FNARR_PROMO_NO --根據播出日和 Video ID 查詢主控運行表短帶播出紀錄

select * from TBPGM_ARR_PROMO where FSPROMO_ID='20150200203'


END --CASE 100

GO

DECLARE @case integer, @subcase integer

set @case = 1 set @subcase = 1

EXEC #TEST_CASE @case, @subcase

GO


select d.FSFILE_NO, d.FCFILE_STATUS, t.* from TBLOG_VIDEO t left join TBLOG_VIDEO_D d on t.FSFILE_NO = d.FSFILE_NO where t.FSVIDEO_PROG = '00002GL7'