use MAM;

--create PROC SP_U_TBUSERS_from_DB1 as

-- use sa account
alter proc SP_U_TBUSERS_from_DB1 as

/*
 * temp table is removed after procedure is finiehed
  if OBJECT_ID('tempdb..#UserDepTempTable') is not null
	 drop table #UserDepTempTable
 */

-- declare a temp table
SELECT * INTO #UserDepTempTable
FROM
  [PTS_MAM].[PTS_MAM].[dbo].[TBUSERS]
WHERE
  1=0

SELECT * INTO #UserDepTempTable2
FROM
  [PTS_MAM].[PTS_MAM].[dbo].[TBUSERS]
WHERE
  1=0

INSERT #UserDepTempTable
	SELECT * FROM [PTS_MAM].[PTS_MAM].[dbo].[TBUSERS]


DECLARE
	@FSUSER_ID nvarchar(50),
	@FSUSER nvarchar(50),
	@FSPASSWD varchar(255),
	@FSUSER_ChtName nvarchar(50),
	@FSUSER_TITLE nvarchar(50),
	@FSDEPT nvarchar(50),
	@FSEMAIL nvarchar(50),
	@FCACTIVE nvarchar(1),
	@FSDESCRIPTION nvarchar(max),
	@FCSECRET char(1),
	@FSCREATED_BY varchar(50),
	@FDCREATED_DATE datetime,
	@FSUPDATED_BY varchar(50),
	@FDUPDATED_DATE datetime,
	@FIIMAGE varchar(100)

DECLARE CSR2 CURSOR FOR select * from #UserDepTempTable

OPEN CSR2
FETCH NEXT FROM CSR2 
INTO @FSUSER_ID
      ,@FSUSER
      ,@FSPASSWD
      ,@FSDEPT
      ,@FSEMAIL
      ,@FCACTIVE
      ,@FSDESCRIPTION
      ,@FSCREATED_BY
      ,@FDCREATED_DATE
      ,@FSUPDATED_BY
      ,@FDUPDATED_DATE
      ,@FCSECRET
	  ,@FIIMAGE

while (@@FETCH_STATUS = 0)
begin
	insert #UserDeptempTable2(FSUSER_ID, FSPASSWD, FSDEPT, FSEMAIL, FCACTIVE, FSDESCRIPTION, FSCREATED_BY, FSCREATED_BY, FSUPDATED_BY, FDUPDATED_DATE, FCSECRET)
	VALUES(@FSUSER_ID, @FSPASSWD, @FSDEPT, @FSEMAIL, @FCACTIVE, @FSDESCRIPTION, @FSCREATED_BY, @FSCREATED_BY, @FSUPDATED_BY, @FDUPDATED_DATE, @FCSECRET)

	FETCH NEXT FROM CSR2 
	INTO @FSUSER_ID
      ,@FSUSER
      ,@FSPASSWD
      ,@FSDEPT
      ,@FSEMAIL
      ,@FCACTIVE
      ,@FSDESCRIPTION
      ,@FSCREATED_BY
      ,@FDCREATED_DATE
      ,@FSUPDATED_BY
      ,@FDUPDATED_DATE
      ,@FCSECRET
	  ,@FIIMAGE
end

select * from #UserDeptempTable2
GO

exec SP_U_TBUSERS_from_DB1
