﻿
declare @case int, @subcase int

if @case = 20160512 begin --20160512 一把青 28 低解轉檔失敗
	select * from TBPROG_M where FSPGMNAME = '一把青'
	select * from TBPGM_COMBINE_QUEUE where FNEPISODE = 28 and FSPROG_ID = '2016478' order by FDDATE
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002FAM' --file size 14G
end

if @case = 201505121600 begin -- 已審核通過檔案未到待播區
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002IQ6'  --2016-04-28 17:14:44.957 標記上傳成功
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002IQ6'  --FCSTATUS = O  2016-04-29 16:26:21.210 審核通過並入庫，但是Review and Approved 找不到檔案
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002IQ6' order by FDDATE --5/12 排入
	select * from TBTRACKINGLOG where FDTIME between '2016/04/29 09:00' and  '2016/05/13 18:00' and FSMESSAGE like '%P201604005830001%' order by FDTIME  --2016-04-29 16:26:51.860 完成入庫
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002IQ6' -- 2016-04-29 16:27:22.000 入庫
	-- Review 檔案在 5/3 被刪除
	select * from TBPGM_COMBINE_QUEUE where FDDATE = '2016-05-14'  --5/12 開表

	--5/13 10:08 已出現在 Approved
end

if @case = 201605121703  --201605121703 那人‧那歌‧那歲月 anystream 轉檔完成，MAM 仍轉檔中
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201101800010001' --FCFILE_STATUS = S 轉檔中
	select * from TBTRAN_LOG where FDDATE between '2016/05/12 09:00' and '2016/05/12 15:00' and FSPARAMETER like '%82703%' order by FDDATE
	select * from TBTRAN_LOG where FDDATE between '2016/05/12 09:00' and '2016/05/12 15:00' and FSPARAMETER like '%166205%' order by FDDATE

	--轉檔成功系統紀錄
	select * from TBTRAN_LOG where FDDATE between '2016/05/12 09:00' and '2016/05/12 15:00' and
	(FSPARAMETER like '%129795%' or FSPARAMETER like '%166208%' or FSPARAMETER like '%82706%' or FSPARAMETER like '%G201647800250002%') order by FDDATE

	select * from TBTRACKINGLOG where FDTIME  between '2016/05/12 09:24' and '2016/05/12 12:35' and 
	(FSMESSAGE like '%129795%' or FSMESSAGE like '%166208%' or FSMESSAGE like '%82706%' or FSMESSAGE like '%G201647800250002%') order by FDTIME


	--轉檔失敗
	select * from TBTRAN_LOG where FDDATE between '2016/05/12 09:00' and '2016/05/12 15:00' and
	(FSPARAMETER like '%129792%' or FSPARAMETER like '%166205%' or FSPARAMETER like '%82703%' or FSPARAMETER like '%G201101800010001%') order by FDDATE

    select * from TBTRACKINGLOG where FDTIME  between '2016/05/12 09:24' and '2016/05/12 12:35' and 
	(FSMESSAGE like '%129792%' or FSMESSAGE like '%166205%' or FSMESSAGE like '%82703%' or FSMESSAGE like '%G201101800010001%') order by FDTIME
end

if @case = 201605131009 begin -- 5/14 播出，檔案未到（from GPFS)
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00001Q4L' -- from GPFS
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00001Q4L' -- FCSTATUS = ''  2016-05-12 12:04:06.533 開表，已經正在搬到 Review
end

if @case = 201605131546 begin --主控搬檔問題
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '000023IT'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('000023IT', '00001UGH') --2016-05-06 20:18:25 已審核
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID in ('000023IT', '00001UGH') order by FDDATE
	--2016/5/6 下午 08:48:28 : 開始搬移檔案:\\10.13.200.6\Review\001UGH.mxf:到待審區
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00001UGH'
end

if @case = 201605131819 begin
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002JRO'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002JRO'  --已標記尚未開始上傳 手動上傳 FCSTATUS = N 待審核
end

if @case = 201605141734 begin
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002JUH'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002JUH'
	select * from TBPTSAP_STATUS_LOG order by FDCREATED_DATE desc
	select * from TBLOG_VIDEO_HD_MC where FCSTATUS = '' order by FDUPDATED_DATE desc
	select * from TBLOG_VIDEO_HD_MC where FCSTATUS = 'C' order by FDUPDATED_DATE desc
	select * from TBLOG_VIDEO_HD_MC  order by FDUPDATED_DATE desc
	select * from TBPTSAP_STATUS_LOG order by FDUPDATE_DATE desc

	-- 在 10.13.24.12 啟動搬檔程式後 002JUJ 上傳失敗
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002JUJ'
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002JUJ' order by FDDATE
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002JUJ' --2016-05-14 17:50:25.210 FCSTATUS = E 上傳失敗, 2016-05-16 18:23:28.960 手動上傳
end

if @case = 201605161535 begin
	select * from TBLOG_VIDEO where FSFILE_NO = 'G200678600930003' --2016-01-21 18:27:27.767 71056 邱映筑 刪除
	select * from TBUSERS where FSUSER_ID = '71056'
end

if @case = 201605161615 begin
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002JQM'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002JQM' --送播單 = 201605120038 FCSTATUS = O 審核通過
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002JQM'  -- FCFILE_STATUS = R 轉檔失敗  -> 移除 5/19 排播 -> 手動刪除檔案 FCFILE_STATUS = X
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002JQM' --5/19 26:03:10:00 play
	select * from TBPGM_PROMO where FSPROMO_ID = '20160500282'

	--轉檔時間 2016-05-15 17:42:45.963

	select * from TBTRAN_LOG where FSPARAMETER like '%P201605002820001%' 
	select * from TBTRAN_LOG where CAST(FDDATE as DATE) = '2016/05/15' and FSPARAMETER like '%P201605002820001%'  --5/15 轉檔  FSSP_NAME = SP_U_TBLOG_VIDEO_JOBID
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016/05/15' and FSMESSAGE like '%P201605002820001%'
	(FSPARAMETER like '%129792%' or FSPARAMETER like '%166205%' or FSPARAMETER like '%82703%' or FSPARAMETER like '%G201101800010001%') order by FDDATE

    select * from TBTRACKINGLOG where FDTIME  between '2016/05/12 09:24' and '2016/05/12 12:35' and 
	(FSMESSAGE like '%129792%' or FSMESSAGE like '%166205%' or FSMESSAGE like '%82703%' or FSMESSAGE like '%G201101800010001%') order by FDTIME

end

---------------- 2016-05-17 ----------------
if @case = 201605171014 begin
	select * from TBLOG_VIDEO_HD_MC where FCSTATUS = '' and CAST(FDUPDATED_DATE as DATE) = '20160516' -- 已標記上傳，但仍未開始上傳 19 筆
	--FCSTATUS = E 上傳失敗
	select * from TBLOG_VIDEO_HD_MC where FCSTATUS = 'E' and CAST(FDUPDATED_DATE as DATE) = '20160517'
	select * from TBLOG_VIDEO_HD_MC order by FDCREATED_DATE desc, FDUPDATED_DATE desc
end



if @case = 201605161704 begin --無法刪除轉單已抽單的短帶
	select * from TBBROADCAST where FSBRO_ID = '201605150004' -- FCCHECK_STATUS = X
	select * from TBLOG_VIDEO where FSID = '20160500337' --FCFILE_STATUS = X
end

if @case = 201605171104 begin -- 5/12 調用失敗
	select * from TBUSERS where FSUSER = 'prg70085' --林裕貴
end

-------------- 2016-05-18 -------------------
if @case = 201605181007 begin -- 我們的島 227 集送播單未建完就跳出，造成只有 TBLOG_VIDEO
	select * from TBBROADCAST where CAST(FDCREATED_DATE as DATE) = '20160517' and FSCREATED_BY = '70406'
	select * from TBBROADCAST where FSCREATED_BY = '70406' order by FDCREATED_DATE desc
	select * from TBLOG_VIDEO where FSCREATED_BY = '70406' order by FDCREATED_DATE desc -- 201605170033 無送播單
	select * from TBUSERS where FSUSER_ChtName = '廖婕妤' --70406
	select * from TBLOG_VIDEO where FSBRO_ID = '201605170033'
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201083802270001'
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G201083802270001'

	exec SP_D_TBLOG_VIDEO_BYBROID '201605170033' --using sa
	exec SP_D_TBLOG_VIDEO_SEG_BYFILEID 'G201083802270001'
	
end


--------- 2016-05-19 ---------------
if @case = 201605191543 begin
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002JTM'  -- 2016-05-17 10:30:11.977 FCSTATUS = 88 上傳失敗
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002JTM'  -- 2016-05-17 21:11:46.447 50311 重新標記上傳，FCSTATUS = '' 等待上傳 -> 上傳程式資料要先刪除

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002JTR'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002JTR'

	--打卡台灣有 3 支上傳失敗
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID in ('00002JPQ', '00002JPP', '00002JPN') --2016-05-18 14:09:29.017 已上傳成功
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002JPQ', '00002JPP', '00002JPN')
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID in ('00002JPQ', '00002JPP', '00002JPN')
end

if @case = 201605191811 begin
	select * from TBPGM_PROMO where FSPROMO_NAME like '%HD傳承%'  --20150800865 HD傳承。看見更好未來(短版)
	select * from TBLOG_VIDEO where FSID = '20150800865'
	select * from TBBROADCAST where FSBRO_ID = '201605190031'
	select * from TBLOG_VIDEO where FSBRO_ID = '201605190031'

	select * from TBPGM_PROMO where FSPROMO_NAME like '%HD漢字好好玩%'
	select t.FSBRO_ID '轉檔單編號', p.FSPROMO_ID '短帶編號', p.FSPROMO_NAME '短帶名稱', p.* from TBPGM_PROMO p left join TBLOG_VIDEO t on p.FSPROMO_ID = t.FSID where t.FSID in ('20150400674', '20150400675', '20150400676', '20150400677', '20150400678', '20150400681')

	select t.FSBRO_ID '轉檔單編號', p.FSPROMO_ID '短帶編號', p.FSPROMO_NAME '短帶名稱', t.*, p.* from TBPGM_PROMO p left join TBLOG_VIDEO t on p.FSPROMO_ID = t.FSID where p.FSPROMO_NAME like '%HD延續。看見更好未來%'
	select t.FSBRO_ID '轉檔單編號', p.FSPROMO_ID '短帶編號', p.FSPROMO_NAME '短帶名稱', t.*, p.* from TBPGM_PROMO p left join TBLOG_VIDEO t on p.FSPROMO_ID = t.FSID where p.FSPROMO_NAME like '%公視HD分級卡_護-2013%'
end

if @case = 201605200843 begin --5/18 3 支檔案未審核
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002OZ9'
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '000020Z9' order by FDDATE desc  -- HD書店裡的影像詩-蕃藝書屋  20150800105
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '000020Y4' order by FDDATE desc
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '000020YX' order by FDDATE desc
	select * from TBPGM_PROMO where FSPROMO_ID = '20150800105'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002JS4', '00002JTU', '00002JTZ') -- 2016-05-20 11:16:04.300 審核通過
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID in ('00002JS4', '00002JTU', '00002JTZ') order by FDUPDATE_DATE desc

	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002JS4'
	select * from TBTRAN_LOG where CAST(FDDATE as DATE) = '2016-05-20' and FSPARAMETER like '%P201508000670002%'
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-05-20' and FSMESSAGE like '%P201508000670002%'
end

if @case = 201605201127 begin -- 新增測試頻道 PTS1
	select * from TBZCHANNEL
	select COLUMN_NAME, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH, * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'TBZCHANNEL'
	-- 新增頻道
	declare @FSCHANNEL_ID varchar(2) = '12'
	declare @FSCHANNEL_NAME nvarchar(100) = 'PTS1'
	declare @FSSHOWNAME nvarchar(20) = 'PTS1'
	declare @FSCHANNEL_TYPE varchar(3) = '002'
	declare @FSSORT varchar(2) = '09'
	declare @FSSHORTNAME varchar(10) = ''
	declare @FSOLDTABLENAME varchar(20) = ''
	declare @FSTSM_POOLNAME varchar(20) = '/ptstv'
	declare @FNDIR_ID bigint = 1
	declare @FSCREATED_BY varchar(50) = '51204'
	declare @FDCREATED_DATE datetime = (select CURRENT_TIMESTAMP)
	declare @FSUPDATED_BY varchar(50) = '51204'
	declare @FDUPDATED_DATE datetime = (select CURRENT_TIMESTAMP)
	declare @FBISENABLE bit = 1

	insert into TBZCHANNEL(FSCHANNEL_ID, FSCHANNEL_NAME, FSSHOWNAME, FSCHANNEL_TYPE, FSSORT, FSTSM_POOLNAME, FNDIR_ID, FSCREATED_BY, FDCREATED_DATE, FSUPDATED_BY, FDUPDATED_DATE, FBISENABLE)
	values(@FSCHANNEL_ID, @FSCHANNEL_NAME, @FSSHOWNAME, @FSCHANNEL_TYPE, @FSSORT, @FSTSM_POOLNAME, @FNDIR_ID, @FSCREATED_BY, @FDCREATED_DATE, @FSUPDATED_BY, @FDUPDATED_DATE, @FBISENABLE)

	update TBZCHANNEL set FSSHORTNAME = '', FSOLDTABLENAME = '' where FSCHANNEL_ID = '12'
	update TBZCHANNEL set FSOLDTABLENAME = 'PSHOW_PTS1' where FSCHANNEL_ID = '12'

	declare @d datetime = (select CURRENT_TIMESTAMP)
	select @d

	--exec SP_I_TBZCHANNEL @FSCHANNEL_ID, @FSCHANNEL_NAME, @FSSHOWNAME, @FSSORT, @FSSHORTNAME, @FSOLDTABLENAME, @FSCREATED_BY, @FSUPDATED_BY

	select * from TBDIRECTORY_GROUP
	select * from TBGROUPS
	select * from TBMODULE_GROUP
	select * from TBMODULE_DETAIL --
	select * from TBMODULE_CAT  --查詢群組權限列表

	select * from TBMODULE_GROUP where FSMODULE_ID = 'P101002'
end

if @case = 201605231358 begin --主頻派送測試
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002JNX'
end


if @case = 201605240853 begin -- 
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00001VK9' order by FDDATE
end

if @case = 201605241154 begin
	select * from TBPGM_PROMO where FSPROMO_NAME like '%2016公視成人影像創作%'  -- FSPROMO_ID = 20160500521
	select * from TBLOG_VIDEO where FSID = '20160500521'  -- 有  2 筆資料  201605230130
	select * from TBBROADCAST where FSBRO_ID = '201605230130'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002KF5' N 待審 -> F 審核不通過
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002KF5' --2016-05-26 26:30:30:16 play
end

if @case = 201605241530 begin --系統設定 config
	select * from TBMACHINE
	select * from TBMACHINE_DETAIL
	select * from TBSYSTEM_CONFIG
	select * from TBSYSTEM_NOTIFY
	select * from TBMASTERCONTROL_FTP_FINISH
end

if @case = 201605250944 begin --待審核檔案，但是QC 無檔案
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002JTJ' -- N 待審核, file in HDUpload
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002KA2' -- N 待審核, file in HDUpload
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002JTJ'
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002KA2'
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002KA2' --FSVIDEO_ID = '00002KA2'
end

if @case = 201605251216 begin
	select * from TBZCHANNEL
end

if @case = 201605251506 begin --送播單資料未建完整
	select * from TBPROG_M where FSPGMNAME like '%台灣人沒在怕%'
	select * from TBLOG_VIDEO where FSID = '2016200'
	select * from TBBROADCAST where FSBRO_ID = '201605250060'
	exec SP_D_TBLOG_VIDEO_BY_FSFILE_NO 'G201620000010002' -- 只是將 FSARC_TYPE = ''
	exec SP_D_TBLOG_VIDEO_BYBROID '201605250060'
end

if @case = 201605251811 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002KV0'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002KV0'
end

if @case = 201605261015 begin
	select * from TBPGM_ARR_PROMO where CAST(FDDATE as DATE) = '2016-05-24' and FSCHANNEL_ID = '01' order by FSPLAYTIME -- PLAYTIME 超過 24 繼續累加 29:47:42:00
end

if @case = 201605271020 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002KXW' -- no record 未填送播單時的 id
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002KXV' -- 2016-05-26 15:48:27.680 N 待審
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002KXW' order by FDDATE --2016-05-28
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002KXV' --未排播 
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002KXW' -- no record
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002KXV' -- 有建送播單
end

if @case = 201605271618 begin
	select * from TBPTSAP_STATUS_LOG order by FDCREATED_DATE, FDUPDATE_DATE desc
	select * from TBPTSAP_STATUS_LOG where CAST(FDUPDATE_DATE as DATE) = '2016-05-27'
end

if @case = 201605271651 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002L2F'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002L2D'
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002L2F' order by FDDATE
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002L2D' order by FDDATE
end

if @case = 201605301136 begin
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002L3R' --2016-05-27 17:26:52.120 標記上傳失敗
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002L3R'
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002L3R' order by FDDATE desc

	select t.FSVIDEOID, h.FSBRO_ID, t.*, h.* from TBPTSAP_STATUS_LOG t join TBLOG_VIDEO_HD_MC h on t.FSVIDEOID = h.FSVIDEO_ID where t.FDUPDATE_DATE > '2016-05-27' and t.FCSTATUS = '88'
	select t.FSVIDEOID, h.FSBRO_ID, t.*, h.* from TBPTSAP_STATUS_LOG t join TBLOG_VIDEO_HD_MC h on t.FSVIDEOID = h.FSVIDEO_ID where t.FDUPDATE_DATE > '2016-05-27' and t.FCSTATUS = '80'

	-- 李小龍 5/17 已經轉檔完成 MAM 未收到回報，5/30 在轉檔中心再執行一次轉檔，讓回報資訊傳回系統
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002K24'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002K24' -- 2016-05-30 09:44:20.397 才產生
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002K24' order by FDDATE
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002K24'
end

if @case = 201605301523 begin -- 將客台改成 HD 頻道
	select * from TBZCHANNEL
	update TBZCHANNEL set FSCHANNEL_TYPE = '002' where FSCHANNEL_ID = '07'
end

if @case = 201605310954 begin
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002L3G'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002L3G'
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002L3G'

	select * from TBPTSAP_STATUS_LOG where FSVIDEOID in ('00002L4R', '00002L4P', '00002L42', '00002L44', '00002L46')
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002L4R', '00002L4P', '00002L42', '00002L44', '00002L46')
	select * from TBTRAN_LOG where FDDATE between '2016/05/30 14:20' and '2016/05/30 14:40' and FSPARAMETER like '%00002l4p%' order by FDDATE  --上傳完成又重新標記？
	select * from TBTRAN_LOG where FDDATE between '2016/05/30 14:30' and '2016/05/30 14:40' order by FDDATE
end

if @case = 201605311059 begin
	select * from TBTRACKINGLOG where FDTIME > '2016-05-31 10:30' order by FDTIME --使用者51204要從TS3500退出磁帶
end

if @case = 201605311346 begin
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002LIG'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002LIG'
end

if @case = 201605311403 begin -- 新增測試頻道 PTS1-MOD
	select * from TBZCHANNEL
	select COLUMN_NAME, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH, * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'TBZCHANNEL'
	-- 新增頻道
	declare @FSCHANNEL_ID varchar(2) = '62'
	declare @FSCHANNEL_NAME nvarchar(100) = 'PTS1-MOD'
	declare @FSSHOWNAME nvarchar(20) = 'PTS1-MOD'
	declare @FSCHANNEL_TYPE varchar(3) = '002'
	declare @FSSORT varchar(2) = '10'
	declare @FSSHORTNAME varchar(10) = ''
	declare @FSOLDTABLENAME varchar(20) = ''
	declare @FSTSM_POOLNAME varchar(20) = '/ptstv'
	declare @FNDIR_ID bigint = 1
	declare @FSCREATED_BY varchar(50) = '51204'
	declare @FDCREATED_DATE datetime = (select CURRENT_TIMESTAMP)
	declare @FSUPDATED_BY varchar(50) = '51204'
	declare @FDUPDATED_DATE datetime = (select CURRENT_TIMESTAMP)
	declare @FBISENABLE bit = 1

	insert into TBZCHANNEL(FSCHANNEL_ID, FSCHANNEL_NAME, FSSHOWNAME, FSCHANNEL_TYPE, FSSORT, FSTSM_POOLNAME, FNDIR_ID, FSCREATED_BY, FDCREATED_DATE, FSUPDATED_BY, FDUPDATED_DATE, FBISENABLE)
	values(@FSCHANNEL_ID, @FSCHANNEL_NAME, @FSSHOWNAME, @FSCHANNEL_TYPE, @FSSORT, @FSTSM_POOLNAME, @FNDIR_ID, @FSCREATED_BY, @FDCREATED_DATE, @FSUPDATED_BY, @FDUPDATED_DATE, @FBISENABLE)

	update TBZCHANNEL set FSSHORTNAME = '', FSOLDTABLENAME = '' where FSCHANNEL_ID = '62'
	update TBZCHANNEL set FSOLDTABLENAME = 'PSHOW_PTS1' where FSCHANNEL_ID = '62'

	declare @d datetime = (select CURRENT_TIMESTAMP)
	select @d

	--exec SP_I_TBZCHANNEL @FSCHANNEL_ID, @FSCHANNEL_NAME, @FSSHOWNAME, @FSSORT, @FSSHORTNAME, @FSOLDTABLENAME, @FSCREATED_BY, @FSUPDATED_BY

	select * from TBDIRECTORY_GROUP
	select * from TBGROUPS
	select * from TBMODULE_GROUP
	select * from TBMODULE_DETAIL where FSMODULE_ID = 'S100001'--
	select * from TBMODULE_CAT  --查詢群組權限列表

	select * from TBMODULE_GROUP where FSMODULE_ID = 'P101002'
	select g.FSGROUP_ID, g.FSGROUP, g.FSDESCRIPTION, m.FSMODULE_ID, m.FSFUNC_NAME, m.FSHANDLER, m.FSDESCRIPTION
	from TBMODULE_GROUP mg join TBGROUPS g on mg.FSGROUP_ID = g.FSGROUP_ID join TBMODULE_DETAIL m on mg.FSMODULE_ID = m.FSMODULE_ID
	order by g.FSGROUP_ID

	select g.FSGROUP_ID, g.FSGROUP, g.FSDESCRIPTION, m.FSMODULE_ID, m.FSFUNC_NAME, m.FSHANDLER, m.FSDESCRIPTION
	from TBMODULE_GROUP mg join TBGROUPS g on mg.FSGROUP_ID = g.FSGROUP_ID join TBMODULE_DETAIL m on mg.FSMODULE_ID = m.FSMODULE_ID
	where FSGROUP = '排表群組'
	order by g.FSGROUP_ID

	select g.FSGROUP_ID, g.FSGROUP, g.FSDESCRIPTION, m.FSMODULE_ID, m.FSFUNC_NAME, m.FSHANDLER, m.FSDESCRIPTION
	from TBMODULE_GROUP mg join TBGROUPS g on mg.FSGROUP_ID = g.FSGROUP_ID join TBMODULE_DETAIL m on mg.FSMODULE_ID = m.FSMODULE_ID
	where m.FSMODULE_ID = 'P101001'
	order by g.FSGROUP_ID -- P101001 = 匯入公視節目表

	-- 其他模組
	select * from TBMODULE_DETAIL where FSFUNC_NAME like '%匯入%'
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P162001', '匯入PTS1-MOD節目表', '', 'treeViewPGMQUEUE')
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P162002', 'PTS1-MOD節目表維護', '', 'treeViewPGMQUEUE')
	update TBMODULE_DETAIL set FSDESCRIPTION = '匯入PTS1-MOD節目表', FSFUNC_NAME = '匯入PTS1-MOD節目表' where FSMODULE_ID = 'P162001'

	select * from TBMODULE_CAT where FSMODULE_CAT_NAME like '%預排%' --頻道預排 = P2
	select * from TBMODULE_DETAIL where FSFUNC_NAME like '%預排%'
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P262001', 'PTS1-MOD頻道預排設定', '', 'treeViewPromoChannelLink')
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P262002', 'PTS1-MOD節目預排設定', '', 'treeViewPromoProglLink')

	select * from TBMODULE_CAT where FSMODULE_CAT_NAME like '%播出%' --播出運行表 = P5
	select * from TBMODULE_DETAIL where FSFUNC_NAME like '%播出%'
	select * from TBMODULE_DETAIL where FSMODULE_ID like 'P501%'
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P562001', 'PTS1-MOD播出運行表維護', '', NULL)
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P562002', 'PTS1-MOD查詢播出運行表', '', NULL)
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P562003', 'PTS1-MOD插入宣傳帶', '', 'treeViewInsertPromo')
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P562006', 'PTS1-MOD轉出LST', '', NULL)
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P562007', 'PTS1-MOD列印播出運行表', '', NULL)
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P562008', 'PTS1-MOD匯入Louth Key', '', 'treeViewLouthKey')

	select * from TBMODULE_DETAIL where FSMODULE_ID like '%62%'
	select * from TBMODULE_DETAIL where FSMODULE_ID like '%61%'

end

if @case = 201605311731 begin -- 刪除客台未建完整的送播單
	select * from TBPROG_M where FSPGMNAME like '%動手做好簡單%' --FSPROG_ID 2015318
	select FSBRO_ID, * from TBLOG_VIDEO where FSID = '2015318' and FNEPISODE in (12, 14, 15, 13, 10) and FSARC_TYPE = '002' -- 201605300057
	-- 201605300051, 201605300057,201605300102, 201605300134, 201605300090
	select * from TBBROADCAST where FSBRO_ID in ('201605300051', '201605300057', '201605300102', '201605300134', '201605300090')

	exec SP_D_TBLOG_VIDEO_BYBROID '201605250060'
	delete from TBLOG_VIDEO where FSBRO_ID in  ('201605300051', '201605300057', '201605300102', '201605300134', '201605300090')

	select * from TBPROG_M where FSPGMNAME like '%客家人有名堂%' --2014480
	select * from TBLOG_VIDEO where FSID = '2014480' and FNEPISODE = 27 -- 201605300136
	select * from TBBROADCAST where FSBRO_ID = '201605300136'
	exec SP_D_TBLOG_VIDEO_BYBROID '201605300136'

	select * from TBPROG_M where FSPGMNAME like '%作客他鄉%' --2007049
	select * from TBLOG_VIDEO where FSID = '2007049' and FNEPISODE = 145 and FSARC_TYPE = '002' -- 201605300137
	select * from TBBROADCAST where FSBRO_ID = '201605300137'
	exec SP_D_TBLOG_VIDEO_BYBROID '201605300137'
end

if @case = 201605311403 begin -- 新增測試頻道 PTS2
	select * from TBZCHANNEL
	select COLUMN_NAME, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH, * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'TBZCHANNEL'
	-- 新增頻道
	declare @FSCHANNEL_ID varchar(2) = '13'
	declare @FSCHANNEL_NAME nvarchar(100) = 'PTS2'
	declare @FSSHOWNAME nvarchar(20) = 'PTS2'
	declare @FSCHANNEL_TYPE varchar(3) = '002'
	declare @FSSORT varchar(2) = '11'
	declare @FSSHORTNAME varchar(10) = ''
	declare @FSOLDTABLENAME varchar(20) = 'PSHOW_DIMO'
	declare @FSTSM_POOLNAME varchar(20) = '/ptstv'
	declare @FNDIR_ID bigint = 1
	declare @FSCREATED_BY varchar(50) = '51204'
	declare @FDCREATED_DATE datetime = (select CURRENT_TIMESTAMP)
	declare @FSUPDATED_BY varchar(50) = '51204'
	declare @FDUPDATED_DATE datetime = (select CURRENT_TIMESTAMP)
	declare @FBISENABLE bit = 1


	insert into TBZCHANNEL(FSCHANNEL_ID, FSCHANNEL_NAME, FSSHOWNAME, FSCHANNEL_TYPE, FSSORT, FSTSM_POOLNAME, FNDIR_ID, FSCREATED_BY, 
	FDCREATED_DATE, FSUPDATED_BY, FDUPDATED_DATE, FBISENABLE, FSSHORTNAME, FSOLDTABLENAME)
	values(@FSCHANNEL_ID, @FSCHANNEL_NAME, @FSSHOWNAME, @FSCHANNEL_TYPE, @FSSORT, @FSTSM_POOLNAME, @FNDIR_ID, @FSCREATED_BY, @FDCREATED_DATE, @FSUPDATED_BY, @FDUPDATED_DATE, @FBISENABLE,
	@FSSHORTNAME, @FSOLDTABLENAME)

	update TBZCHANNEL set FSSHORTNAME = '', FSOLDTABLENAME = 'PSHOW_DIMO' where FSCHANNEL_ID = '13'
	update TBZCHANNEL set FSSHORTNAME = '', FSOLDTABLENAME = 'PSHOW_PTS' where FSCHANNEL_ID = '12'

	declare @d datetime = (select CURRENT_TIMESTAMP)
	select @d

	--exec SP_I_TBZCHANNEL @FSCHANNEL_ID, @FSCHANNEL_NAME, @FSSHOWNAME, @FSSORT, @FSSHORTNAME, @FSOLDTABLENAME, @FSCREATED_BY, @FSUPDATED_BY

	select * from TBDIRECTORY_GROUP
	select * from TBGROUPS
	select * from TBMODULE_GROUP
	select * from TBMODULE_DETAIL where FSMODULE_ID = 'S100001'--
	select * from TBMODULE_CAT  --查詢群組權限列表

	select * from TBMODULE_GROUP where FSMODULE_ID = 'P101002'
	select g.FSGROUP_ID, g.FSGROUP, g.FSDESCRIPTION, m.FSMODULE_ID, m.FSFUNC_NAME, m.FSHANDLER, m.FSDESCRIPTION
	from TBMODULE_GROUP mg join TBGROUPS g on mg.FSGROUP_ID = g.FSGROUP_ID join TBMODULE_DETAIL m on mg.FSMODULE_ID = m.FSMODULE_ID
	order by g.FSGROUP_ID

	select g.FSGROUP_ID, g.FSGROUP, g.FSDESCRIPTION, m.FSMODULE_ID, m.FSFUNC_NAME, m.FSHANDLER, m.FSDESCRIPTION
	from TBMODULE_GROUP mg join TBGROUPS g on mg.FSGROUP_ID = g.FSGROUP_ID join TBMODULE_DETAIL m on mg.FSMODULE_ID = m.FSMODULE_ID
	where FSGROUP = '排表群組'
	order by g.FSGROUP_ID

	select g.FSGROUP_ID, g.FSGROUP, g.FSDESCRIPTION, m.FSMODULE_ID, m.FSFUNC_NAME, m.FSHANDLER, m.FSDESCRIPTION
	from TBMODULE_GROUP mg join TBGROUPS g on mg.FSGROUP_ID = g.FSGROUP_ID join TBMODULE_DETAIL m on mg.FSMODULE_ID = m.FSMODULE_ID
	where m.FSMODULE_ID = 'P101001'
	order by g.FSGROUP_ID -- P101001 = 匯入公視節目表

	-- 其他模組
	select * from TBMODULE_DETAIL where FSFUNC_NAME like '%匯入%'
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P113001', '匯入PTS2節目表', '', 'treeViewPGMQUEUE')
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P113002', 'PTS2節目表維護', '', 'treeViewPGMQUEUE')
	update TBMODULE_DETAIL set FSDESCRIPTION = '匯入PTS1-MOD節目表', FSFUNC_NAME = '匯入PTS1-MOD節目表' where FSMODULE_ID = 'P162001'

	select * from TBMODULE_CAT where FSMODULE_CAT_NAME like '%預排%' --頻道預排 = P2
	select * from TBMODULE_DETAIL where FSFUNC_NAME like '%預排%'
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P213001', 'PTS2頻道預排設定', '', 'treeViewPromoChannelLink')
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P213002', 'PTS2節目預排設定', '', 'treeViewPromoProglLink')

	select * from TBMODULE_CAT where FSMODULE_CAT_NAME like '%播出%' --播出運行表 = P5
	select * from TBMODULE_DETAIL where FSFUNC_NAME like '%播出%'
	select * from TBMODULE_DETAIL where FSMODULE_ID like 'P501%'
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P513001', 'PTS2播出運行表維護', '', NULL)
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P513002', 'PTS2查詢播出運行表', '', NULL)
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P513003', 'PTS2插入宣傳帶', '', 'treeViewInsertPromo')
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P513006', 'PTS2轉出LST', '', NULL)
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P513007', 'PTS2列印播出運行表', '', NULL)
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P513008', 'PTS2匯入Louth Key', '', 'treeViewLouthKey')

	select * from TBMODULE_DETAIL where FSMODULE_ID like '%62%'
	select * from TBMODULE_DETAIL where FSMODULE_ID like '%61%'

end

if @case = 201606011515 begin --送播單資料未建完整
	select * from TBPROG_M where FSPGMNAME like '%台灣人沒在怕%'
	select * from TBLOG_VIDEO where FSID = '2016200' and FSARC_TYPE = '002' and FNEPISODE = 3 --201606010042
	select * from TBBROADCAST where FSBRO_ID = '201606010042'
	exec SP_D_TBLOG_VIDEO_BY_FSFILE_NO 'G201620000010002' -- 只是將 FSARC_TYPE = ''
	exec SP_D_TBLOG_VIDEO_BYBROID '201606010042'
	delete from TBLOG_VIDEO where FSBRO_ID = '201606010042'
end

if @case = 201606011842 begin
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002LPP'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002LPP' --2016-06-01 15:59:29.857 201606010056
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002LPP'

	select * from TBPTSAP_STATUS_LOG order by FDUPDATE_DATE desc, FDCREATED_DATE desc
	select * from TBLOG_VIDEO_HD_MC order by FDUPDATED_DATE desc , FDCREATED_DATE desc
end

if @case = 201606012301 begin
	select * from TBPROG_M where FSPGMNAME like '%作客他鄉%' --2007049
	select * from TBLOG_VIDEO where FSID = '2007049' and FNEPISODE = 143 and FSARC_TYPE = '002'
	select * from TBBROADCAST where FSBRO_ID = '201605310099'
	select * from TBLOG_VIDEO where FSID = '2007049' and FNEPISODE = 150 and FSARC_TYPE = '002'
	select * from TBBROADCAST where FSBRO_ID = '201605310102'
	delete from TBLOG_VIDEO where FSBRO_ID = '201605310099'
	exec SP_D_TBLOG_VIDEO_BYBROID '201605310102'
end

if @case = 201606021338 begin -- 百變小露露 51 CG 只能看到一個音軌
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '000021VE'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000021VE'
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '000021VE' order by FDDATE
end

if @case = 201606021449 begin -- 要重新審核 O=已到待播區，因為審核通過未走完入庫程序
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002L9A' --G201083802290001 2016-06-01 20:31:20.423
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002L9A' order by FDDATE --我們的島(HD版) 6/3
	select * from TBTRACKINGLOG where FDTIME between '2016-06-01 20:00' and '2016-06-01 21:00' and FSMESSAGE like '%G201083802290001%'
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-06-02' and FSMESSAGE like '%G201083802290001%' order by FDTIME

	select * from TBPROG_M where FSPGMNAME like '%一沙一世界%' -- 2016553
	select * from TBLOG_VIDEO where FSID = '2016553' and FNEPISODE = 1 and FSARC_TYPE = '002' --00002LNQ
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002LNQ'
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-06-02' and FSMESSAGE like '%G201655300010005%' or FSMESSAGE like '%00002LNQ%' order by FDTIME


	select * from TBLOG_VIDEO_HD_MC where FCSTATUS = 'O' and CAST(FDUPDATED_DATE as DATE) = '2016-06-02'
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-06-02' and FSMESSAGE like '%G201542700100001%'
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00001N8W' order by FDDATE
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-06-02' and FSMESSAGE like '%G201542700100001%'

	--已走完入庫
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002LPP'
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-06-01' and FSMESSAGE like '%P201606000050001%'
	
end

if @case = 201606021457 begin -- 填到送帶轉檔單
	select * from TBPGM_PROMO where FSPROMO_NAME like '%我家是戰國%' order by FDCREATED_DATE desc and FSPROMO_ID = '201605270056'
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '201605270056'
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002L2N' --201605270056
	select * from TBBROADCAST where FSBRO_ID = '201605270056'  --送帶轉檔單
end

if @case = 201606021743 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('0000275T', '00007GE', '00001O4T', '00002JOR', '00001SKX', '00001VKE', '00002JRB')
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-06-01' and FSMESSAGE like '%G201647800310006%' order by FDTIME
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-06-01' and FSMESSAGE like '%G201502700030002%' order by FDTIME
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-06-01' and FSMESSAGE like '%G201543400030002%' order by FDTIME
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-06-01' and FSMESSAGE like '%G201620100330002%' order by FDTIME
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-06-01' and FSMESSAGE like '%G201217500170004%' order by FDTIME
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-06-01' and FSMESSAGE like '%G201558900010003%' order by FDTIME
end

if @case = 201606030950 begin -- 如何判斷送播單或送帶轉檔單
	select * from TBLOG_VIDEO where FSARC_TYPE = '002' and FDCREATED_DATE > '2016-05-01' order by FDCREATED_DATE desc
	select * from TBLOG_VIDEO_HD_MC where FSBRO_ID = '201606020202'
	select * from TBLOG_VIDEO where FSARC_TYPE = '002' and FDCREATED_DATE > '2016-05-01' and FSBRO_ID not in (select FSBRO_ID from TBLOG_VIDEO_HD_MC) order by FDCREATED_DATE desc

	-- 送帶轉檔單，如果 FCFILE_STATUS = X FSTRACK 都是 8 軌
	select FSFILE_NO, FSTYPE, FSID, FNEPISODE, FSARC_TYPE, FSFILE_TYPE_HV, FSFILE_TYPE_LV, FSFILE_SIZE, FCFILE_STATUS, FSCHANGE_FILE_NO, FSFILE_PATH_H, FSFILE_PATH_L,
	FSBRO_ID, FSVIDEO_PROG, FDCREATED_DATE, FSUPDATED_BY, FSTRACK from TBLOG_VIDEO 
	where FSARC_TYPE = '002' and FDCREATED_DATE > '2016-05-01' and FSBRO_ID not in (select FSBRO_ID from TBLOG_VIDEO_HD_MC) order by FDCREATED_DATE desc

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002LYT' -- 無送播單
	select * from TBLOG_VIDEO where FSID = '2015989' and FNEPISODE = 26
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002LYU'  -- 有送播單
end

if @case = 201606031026 begin --測試 SD
	select * from TBLOG_VIDEO where FSARC_TYPE = '001' and FDCREATED_DATE > '2016-06-01' and FCFILE_STATUS = 'T' order by FDUPDATED_DATE desc --G000081602680001 201606030004
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G000081602680001'
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G000081602680001'

	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002M11'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002M11'
end

if @case = 201606031137 begin --排表＼插入短帶：比對段落錯誤，重新載入抓到舊的檔案
	select * from TBLOG_VIDEO where FSID = '2011421' and FNEPISODE = 14 and FSARC_TYPE = '001' order by FDCREATED_DATE desc
	select * from TBLOG_VIDEO where FSID = '2011421' and FNEPISODE = 15 and FSARC_TYPE = '001' order by FDCREATED_DATE desc
	select * from TBLOG_VIDEO where FSID = '2011421' and FNEPISODE = 16 and FSARC_TYPE = '001' order by FDCREATED_DATE desc
	select * from TBLOG_VIDEO where FSID = '2011421' and FSARC_TYPE = '001' and CAST(FDUPDATED_DATE as DATE) = '2016-06-02' --14~26 有被更動過
	select * from TBLOG_VIDEO where FSID = '2011421' and FSARC_TYPE = '002' and CAST(FDUPDATED_DATE as DATE) = '2016-06-02'

	select * from TBLOG_VIDEO where FSID = '2011421' and FNEPISODE between 14 and 27 and FSARC_TYPE = '002' order by FDCREATED_DATE desc
	select * from TBTRACKINGLOG where FDTIME between '2016-06-02 11:55' and '2016-06-02 12:10' and FSMESSAGE like '%G201142100140004%' order by FDTIME
	select * from TBTRACKINGLOG where FDTIME between '2016-06-02 11:55' and '2016-06-02 12:10' order by FDTIME
	select * from TBTRAN_LOG where FDDATE between '2016/05/01 10:55' and '2016/06/02 16:10' and FSPARAMETER like '%201503050070%' order by FDDATE --2016/06/02 11:59:59 審核入庫

	--入庫
	select * from TBARCHIVE where FSARC_ID = '201503050070' -- 入庫單主表
	select * from TBARCHIVE_SET
	select * from TBLOG_PHOTO where FSARC_ID = '201503050070' --入庫圖片
	select * from TBLOG_DOC where FSARC_ID = '201503050070'
	select * from TBPROG_ARCHIVE where FSARC_ID = '201503050070' --no data
	select * from TBLOG_VIDEO where FSARC_ID = '201503050070'
	select * from TBLOG_VIDEO_D where FSARC_ID = '201503050070'
	
	select * from TBDOCUMENT --no data
	select * from TBFILE_TYPE
	select * from TBFILE_CATEGORY
	select * from TBPGM_INSERT_TAPE

	select * from TBLOG_VIDEO where FSID = '2013232' and FNEPISODE = 1 order by FDCREATED_DATE, FDUPDATED_DATE

end

if @case = 201606031556 begin
	select * from TBLOG_VIDEO where FSID = '2011092' and FNEPISODE = 6
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002LB6'
end

if @case = 201606051005 begin
	select * from TBPTSAP_STATUS_LOG where FCSTATUS = '80' and FDUPDATE_DATE > '2016-06-04' order by FDUPDATE_DATE
	select * from TBLOG_VIDEO_HD_MC where FDUPDATED_DATE > '2016-06-04' order by FDUPDATED_DATE
	select * from TBZFILE_STATUS
	select * from TBLOG_VIDEO where FSBRO_ID = '201606030121'
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002M26'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002MA6'
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002JCF'
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002JJN' order by FDDATE desc
	select * from TBPGM_PROMO where FSPROMO_ID = '20160500200'
end

if @case = 201606061401 begin
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002M4M' --2016-06-03 16:38:02.000 上傳完成
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002M4M' --在 HDUpload 的檔名是 002M4M.mxf.mxf  N=待審
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID in ('00002MA8', '00002MA9', '00002MAA')  -- 珊珊上傳的檔名是  *.mxf.MXF
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002MA8', '00002MA9', '00002MAA')
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002M4M'
	select * from TBZCHANNEL
end

if @case = 201606061448 begin --6/8 haka  檔案已在Review 當是狀態是檔案未到--> 未比對段落
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002M3U'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002M3U'
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'P201606001000001'
	
	select * from TBPGM_PROMO where FSPROMO_ID = '20160600100'

	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002MBW'
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002M71'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002M71'
end

if @case = 201606061528 begin --客台上傳測試
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002LQJ'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002MDN'

	select * from 
end

if @case = 201606061747 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002KD7', '00002M0X') -- 2016-06-06 16:38:55.903 審核通過 17:50 檔案仍未到
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID in ('00002KD7', '00002M0X') order by FDDATE desc

	-- 列出待審區到帶播區的檔案
	select * from TBLOG_VIDEO_HD_MC where FCSTATUS = 'O' and FDUPDATED_DATE between '2016-06-06 16:00' and '2016-06-06 17:00' order by FDUPDATED_DATE desc
end


if @case = 201606062120 begin -- 新增測試頻道 PTS3
	select * from TBZCHANNEL
	select COLUMN_NAME, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH, * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'TBZCHANNEL'
	-- 新增頻道
	declare @FSCHANNEL_ID varchar(2) = '14'
	declare @FSCHANNEL_NAME nvarchar(100) = 'PTS3'
	declare @FSSHOWNAME nvarchar(20) = 'PTS3'
	declare @FSCHANNEL_TYPE varchar(3) = '002'
	declare @FSSORT varchar(2) = '12'
	declare @FSSHORTNAME varchar(10) = ''
	declare @FSOLDTABLENAME varchar(20) = 'PSHOW_HD'
	declare @FSTSM_POOLNAME varchar(20) = '/ptstv'
	declare @FNDIR_ID bigint = 1
	declare @FSCREATED_BY varchar(50) = '51204'
	declare @FDCREATED_DATE datetime = (select CURRENT_TIMESTAMP)
	declare @FSUPDATED_BY varchar(50) = '51204'
	declare @FDUPDATED_DATE datetime = (select CURRENT_TIMESTAMP)
	declare @FBISENABLE bit = 1


	insert into TBZCHANNEL(FSCHANNEL_ID, FSCHANNEL_NAME, FSSHOWNAME, FSCHANNEL_TYPE, FSSORT, FSTSM_POOLNAME, FNDIR_ID, FSCREATED_BY, 
	FDCREATED_DATE, FSUPDATED_BY, FDUPDATED_DATE, FBISENABLE, FSSHORTNAME, FSOLDTABLENAME)
	values(@FSCHANNEL_ID, @FSCHANNEL_NAME, @FSSHOWNAME, @FSCHANNEL_TYPE, @FSSORT, @FSTSM_POOLNAME, @FNDIR_ID, @FSCREATED_BY, @FDCREATED_DATE, @FSUPDATED_BY, @FDUPDATED_DATE, @FBISENABLE,
	@FSSHORTNAME, @FSOLDTABLENAME)

	update TBZCHANNEL set FSSHORTNAME = '', FSOLDTABLENAME = 'PSHOW_DIMO' where FSCHANNEL_ID = '13'
	update TBZCHANNEL set FSSHORTNAME = '', FSOLDTABLENAME = 'PSHOW_PTS' where FSCHANNEL_ID = '12'

	declare @d datetime = (select CURRENT_TIMESTAMP)
	select @d

	--exec SP_I_TBZCHANNEL @FSCHANNEL_ID, @FSCHANNEL_NAME, @FSSHOWNAME, @FSSORT, @FSSHORTNAME, @FSOLDTABLENAME, @FSCREATED_BY, @FSUPDATED_BY

	select * from TBDIRECTORY_GROUP
	select * from TBGROUPS
	select * from TBMODULE_GROUP
	select * from TBMODULE_DETAIL where FSMODULE_ID = 'S100001'--
	select * from TBMODULE_CAT  --查詢群組權限列表

	select * from TBMODULE_GROUP where FSMODULE_ID = 'P101002'
	select g.FSGROUP_ID, g.FSGROUP, g.FSDESCRIPTION, m.FSMODULE_ID, m.FSFUNC_NAME, m.FSHANDLER, m.FSDESCRIPTION
	from TBMODULE_GROUP mg join TBGROUPS g on mg.FSGROUP_ID = g.FSGROUP_ID join TBMODULE_DETAIL m on mg.FSMODULE_ID = m.FSMODULE_ID
	order by g.FSGROUP_ID

	select g.FSGROUP_ID, g.FSGROUP, g.FSDESCRIPTION, m.FSMODULE_ID, m.FSFUNC_NAME, m.FSHANDLER, m.FSDESCRIPTION
	from TBMODULE_GROUP mg join TBGROUPS g on mg.FSGROUP_ID = g.FSGROUP_ID join TBMODULE_DETAIL m on mg.FSMODULE_ID = m.FSMODULE_ID
	where FSGROUP = '排表群組'
	order by g.FSGROUP_ID

	select g.FSGROUP_ID, g.FSGROUP, g.FSDESCRIPTION, m.FSMODULE_ID, m.FSFUNC_NAME, m.FSHANDLER, m.FSDESCRIPTION
	from TBMODULE_GROUP mg join TBGROUPS g on mg.FSGROUP_ID = g.FSGROUP_ID join TBMODULE_DETAIL m on mg.FSMODULE_ID = m.FSMODULE_ID
	where m.FSMODULE_ID = 'P101001'
	order by g.FSGROUP_ID -- P101001 = 匯入公視節目表

	-- 其他模組
	select * from TBMODULE_DETAIL where FSFUNC_NAME like '%匯入%'
	---------------execute sql command ------------- P1 + 14(Channel id) + 
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P114001', '匯入PTS3節目表', '', 'treeViewPGMQUEUE')
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P114002', 'PTS3節目表維護', '', 'treeViewPGMQUEUE')
	-----------------------------
	update TBMODULE_DETAIL set FSDESCRIPTION = 'PTS3 節目表維護', FSFUNC_NAME = 'PTS3節目表維護' where FSMODULE_ID = 'P114002'

	select * from TBMODULE_CAT where FSMODULE_CAT_NAME like '%預排%' --頻道預排 = P2
	select * from TBMODULE_DETAIL where FSFUNC_NAME like '%預排%'

	--------------execute sql command ------------------------------
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P214001', 'PTS3頻道預排設定', '', 'treeViewPromoChannelLink')
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P214002', 'PTS3節目預排設定', '', 'treeViewPromoProglLink')
	-----------------------------------------------------------------

	select * from TBMODULE_CAT where FSMODULE_CAT_NAME like '%播出%' --播出運行表 = P5
	select * from TBMODULE_DETAIL where FSFUNC_NAME like '%播出%'
	select * from TBMODULE_DETAIL where FSMODULE_ID like 'P501%'

	----------------------------execute sql command ---------------------------
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P514001', 'PTS3播出運行表維護', '', NULL)
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P514002', 'PTS3查詢播出運行表', '', NULL)
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P514003', 'PTS3插入宣傳帶', '', 'treeViewInsertPromo')
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P514006', 'PTS3轉出LST', '', NULL)
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P514007', 'PTS3列印播出運行表', '', NULL)
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P514008', 'PTS3匯入Louth Key', '', 'treeViewLouthKey')
	----------------------------------------------------------------------------
	select * from TBMODULE_DETAIL where FSMODULE_ID like '%62%'
	select * from TBMODULE_DETAIL where FSMODULE_ID like '%61%'
end

if @case = 201606062145 begin --6/14 HD QC 按重新整理，原本是檔案未到變成審核通過
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002131'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00001ZL9'
	select * from TBUSERS where FSUSER_ID = '50746'
end

if @case = 201606062300 begin -- 新增測試頻道 PTS3-MOD
	select * from TBZCHANNEL
	select COLUMN_NAME, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH, * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'TBZCHANNEL'
	-- 新增頻道
	declare @FSCHANNEL_ID varchar(2) = '64'
	declare @FSCHANNEL_NAME nvarchar(100) = 'PTS3-MOD'
	declare @FSSHOWNAME nvarchar(20) = 'PTS3-MOD'
	declare @FSCHANNEL_TYPE varchar(3) = '002'
	declare @FSSORT varchar(2) = '13'
	declare @FSSHORTNAME varchar(10) = ''
	declare @FSOLDTABLENAME varchar(20) = 'PSHOW_HD_MOD'
	declare @FSTSM_POOLNAME varchar(20) = '/ptstv'
	declare @FNDIR_ID bigint = 1
	declare @FSCREATED_BY varchar(50) = '51204'
	declare @FDCREATED_DATE datetime = (select CURRENT_TIMESTAMP)
	declare @FSUPDATED_BY varchar(50) = '51204'
	declare @FDUPDATED_DATE datetime = (select CURRENT_TIMESTAMP)
	declare @FBISENABLE bit = 1


	insert into TBZCHANNEL(FSCHANNEL_ID, FSCHANNEL_NAME, FSSHOWNAME, FSCHANNEL_TYPE, FSSORT, FSTSM_POOLNAME, FNDIR_ID, FSCREATED_BY, 
	FDCREATED_DATE, FSUPDATED_BY, FDUPDATED_DATE, FBISENABLE, FSSHORTNAME, FSOLDTABLENAME)
	values(@FSCHANNEL_ID, @FSCHANNEL_NAME, @FSSHOWNAME, @FSCHANNEL_TYPE, @FSSORT, @FSTSM_POOLNAME, @FNDIR_ID, @FSCREATED_BY, @FDCREATED_DATE, @FSUPDATED_BY, @FDUPDATED_DATE, @FBISENABLE,
	@FSSHORTNAME, @FSOLDTABLENAME)

	update TBZCHANNEL set FSSHORTNAME = '', FSOLDTABLENAME = 'PSHOW_DIMO' where FSCHANNEL_ID = '13'
	update TBZCHANNEL set FSSHORTNAME = '', FSOLDTABLENAME = 'PSHOW_PTS' where FSCHANNEL_ID = '12'

	declare @d datetime = (select CURRENT_TIMESTAMP)
	select @d

	--exec SP_I_TBZCHANNEL @FSCHANNEL_ID, @FSCHANNEL_NAME, @FSSHOWNAME, @FSSORT, @FSSHORTNAME, @FSOLDTABLENAME, @FSCREATED_BY, @FSUPDATED_BY

	select * from TBDIRECTORY_GROUP
	select * from TBGROUPS
	select * from TBMODULE_GROUP
	select * from TBMODULE_DETAIL where FSMODULE_ID = 'S100001'--
	select * from TBMODULE_CAT  --查詢群組權限列表

	select * from TBMODULE_GROUP where FSMODULE_ID = 'P101002'
	select g.FSGROUP_ID, g.FSGROUP, g.FSDESCRIPTION, m.FSMODULE_ID, m.FSFUNC_NAME, m.FSHANDLER, m.FSDESCRIPTION
	from TBMODULE_GROUP mg join TBGROUPS g on mg.FSGROUP_ID = g.FSGROUP_ID join TBMODULE_DETAIL m on mg.FSMODULE_ID = m.FSMODULE_ID
	order by g.FSGROUP_ID

	select g.FSGROUP_ID, g.FSGROUP, g.FSDESCRIPTION, m.FSMODULE_ID, m.FSFUNC_NAME, m.FSHANDLER, m.FSDESCRIPTION
	from TBMODULE_GROUP mg join TBGROUPS g on mg.FSGROUP_ID = g.FSGROUP_ID join TBMODULE_DETAIL m on mg.FSMODULE_ID = m.FSMODULE_ID
	where FSGROUP = '排表群組'
	order by g.FSGROUP_ID

	select g.FSGROUP_ID, g.FSGROUP, g.FSDESCRIPTION, m.FSMODULE_ID, m.FSFUNC_NAME, m.FSHANDLER, m.FSDESCRIPTION
	from TBMODULE_GROUP mg join TBGROUPS g on mg.FSGROUP_ID = g.FSGROUP_ID join TBMODULE_DETAIL m on mg.FSMODULE_ID = m.FSMODULE_ID
	where m.FSMODULE_ID = 'P101001'
	order by g.FSGROUP_ID -- P101001 = 匯入公視節目表

	-- 其他模組
	select * from TBMODULE_DETAIL where FSFUNC_NAME like '%匯入%'
	---------------execute sql command ------------- P1 + 14(Channel id) + 
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P164001', '匯入PTS3-MOD節目表', '', 'treeViewPGMQUEUE')
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P164002', 'PTS3-MOD節目表維護', '', 'treeViewPGMQUEUE')
	-----------------------------
	update TBMODULE_DETAIL set FSDESCRIPTION = 'PTS3 節目表維護', FSFUNC_NAME = 'PTS3節目表維護' where FSMODULE_ID = 'P114002'

	select * from TBMODULE_CAT where FSMODULE_CAT_NAME like '%預排%' --頻道預排 = P2
	select * from TBMODULE_DETAIL where FSFUNC_NAME like '%預排%'

	--------------execute sql command ------------------------------
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P264001', 'PTS3-MOD頻道預排設定', '', 'treeViewPromoChannelLink')
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P264002', 'PTS3-MOD節目預排設定', '', 'treeViewPromoProglLink')
	-----------------------------------------------------------------

	select * from TBMODULE_CAT where FSMODULE_CAT_NAME like '%播出%' --播出運行表 = P5
	select * from TBMODULE_DETAIL where FSFUNC_NAME like '%播出%'
	select * from TBMODULE_DETAIL where FSMODULE_ID like 'P501%'

	----------------------------execute sql command ---------------------------
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P564001', 'PTS3-MOD播出運行表維護', '', NULL)
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P564002', 'PTS3-MOD查詢播出運行表', '', NULL)
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P564003', 'PTS3-MOD插入宣傳帶', '', 'treeViewInsertPromo')
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P564006', 'PTS3-MOD轉出LST', '', NULL)
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P564007', 'PTS3-MOD列印播出運行表', '', NULL)
	insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P564008', 'PTS3-MOD匯入Louth Key', '', 'treeViewLouthKey')
	----------------------------------------------------------------------------
	select * from TBMODULE_DETAIL where FSMODULE_ID like '%62%'
	select * from TBMODULE_DETAIL where FSMODULE_ID like '%61%'
end

if @case = 201606070939 begin 
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002DXD' -- 2016-06-06 19:56:57.490 created, 2016-06-07 00:36:33.003 上傳待播區完成
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002DXD' order by FDDATE
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002DXD' --48.3G
end

if @case = 201606070939 begin -- 託播單
	select p.FSPROMO_NAME, l.* from TBPGM_PROG_PROMO_LINK l join TBPGM_PROMO p on l.FSPROMO_ID = p.FSPROMO_ID order by FDCREATED_DATE desc
	select p.FSPROMO_NAME, l.* from TBPGM_PROG_PROMO_LINK l join TBPGM_PROMO p on l.FSPROMO_ID = p.FSPROMO_ID where l.FSPROMO_ID in ('20120100160', '20160600080') order by FDCREATED_DATE desc
	select pgm.FSPGMNAME, p.FSPROMO_NAME, l.* from TBPGM_PROG_PROMO_LINK l join TBPGM_PROMO p on l.FSPROMO_ID = p.FSPROMO_ID join TBPROG_M pgm on l.FSPROG_ID = pgm.FSPROG_ID 
	where l.FSPROMO_ID in  ('20160600087') order by FDCREATED_DATE desc

	select pgm.FSPGMNAME, p.FSPROMO_NAME, l.* from TBPGM_PROG_PROMO_LINK l join TBPGM_PROMO p on l.FSPROMO_ID = p.FSPROMO_ID join TBPROG_M pgm on l.FSPROG_ID = pgm.FSPROG_ID 
	where l.FSPROMO_ID in ('20120100160', '20160600080') order by FDCREATED_DATE desc

	select * from TBUSERS where FSUSER_ID in ('50746', '51063')
	select * from TBPROG_D where FSPROG_ID = '2007003' and FSPGDNAME like '%連煜佳霓%' --449

	select * from TBPGM_PROMO

	select pgm.FSPGMNAME, p.FSPROMO_NAME, l.* from TBPGM_PROG_PROMO_LINK l join TBPGM_PROMO p on l.FSPROMO_ID = p.FSPROMO_ID join TBPROG_M pgm on l.FSPROG_ID = pgm.FSPROG_ID 
	where l.FSPROMO_ID in ('20120100160', '20160600080') order by FDCREATED_DATE desc
end

if @case = 201606071128 begin -- 系統權限
	select * from TBUSERS
	select * from TBMODULE_CAT --功能模組
	select * from TBMODULE_DETAIL -- 功能清單
	select * from TBMODULE_GROUP
	select * from TBUSER_GROUP
	select * from TBGROUPS -- 權限群組
	select FSUSER_ID, count(FSGROUP_ID) '角色個數' from TBUSER_GROUP group by FSUSER_ID

	select u.FSUSER_ID, u.FSUSER_ChtName, ug.FSGROUP_ID, g.FSGROUP, mg.FSMODULE_ID, md.FSFUNC_NAME from (((TBUSERS u left join TBUSER_GROUP ug on u.FSUSER_ID = ug.FSUSER_ID) 
	join TBMODULE_GROUP mg on ug.FSGROUP_ID = mg.FSGROUP_ID) join TBGROUPS g on g.FSGROUP_ID = ug.FSGROUP_ID) join TBMODULE_DETAIL md on mg.FSMODULE_ID = md.FSMODULE_ID order by u.FSUSER_ID, ug.FSGROUP_ID

	--列出所有有權限的 user 的權限清單
	select u.FSUSER_ChtName 姓名, g.FSGROUP 群組, md.FSFUNC_NAME 功能 from (((TBUSERS u left join TBUSER_GROUP ug on u.FSUSER_ID = ug.FSUSER_ID) 
	join TBMODULE_GROUP mg on ug.FSGROUP_ID = mg.FSGROUP_ID) join TBGROUPS g on g.FSGROUP_ID = ug.FSGROUP_ID) join TBMODULE_DETAIL md on mg.FSMODULE_ID = md.FSMODULE_ID order by u.FSUSER_ID, ug.FSGROUP_ID
end

if @case = 201606071504 begin --6/8 haka 
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002MBW' -- 無轉檔單
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002M71' -- 6/4 刪除
end

if @case = 201606071517 begin
	select * from TBPGM_COMBINE_QUEUE where FSPROG_ID = '2013232' and FNEPISODE = 99
	select * from TBPGM_COMBINE_QUEUE where CAST(FDDATE as DATE) = '2016-05-29' and FSCHANNEL_ID = '51' order by FSPLAY_TIME  --在節目表維護插入波西測試一二，未建轉檔單時取號 00002MGQ, 00002MGW
	select * from TBPGM_COMBINE_QUEUE where CAST(FDDATE as DATE) = '2016-05-29' and FSCHANNEL_ID = '01' order by FSPLAY_TIME 
	select * from TBPGM_QUEUE where CAST(FDDATE as DATE) = '2016-05-29' and FSCHANNEL_ID = '51' order by FSBEG_TIME
	select * from TBPGM_ARR_PROMO where CAST(FDDATE as DATE) = '2016-05-29' and FSCHANNEL_ID = '51' order by FSPLAYTIME
	select * from TBPGM_ARR_PROMO where CAST(FDDATE as DATE) = '2016-05-29' and FSCHANNEL_ID = '01' order by FSPLAYTIME
end

if @case = 201606081020 begin -- 6/7 審核通過，插入短帶無法預覽
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID in ('00002MDP', '00002MDQ') order by FDDATE
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002MDP', '00002MDQ')
	select * from TBLOG_VIDEO where FSVIDEO_PROG in ('00002MDP', '00002MDQ') --R : 轉檔失敗 Anystream 註冊錯誤 P201606001330003, P201606001340003
	select * from TBTRACKINGLOG where FDTIME between '2016-06-07 15:20' and '2016-06-07 15:40' order by FDTIME --Unable to communicate with the ECS
	select * from TBTRACKINGLOG where FDTIME between '2016-06-07 15:20' and '2016-06-07 15:40' and FSCATALOG = 'ERROR' order by FDTIME 
	--重啟，只有檔案送播才能在檢視轉檔作業流程中重啟 
	select * from TBTRACKINGLOG where FDTIME between '2016-06-08 10:45' and '2016-06-08 11:10'  order by FDTIME 
end 

if @case = 201606081501 begin --6/9 haka 有標記，檔案未到，無法比對段落
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002LYX'

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID  = '00002MIB'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID  = '00002MHW'-- 2016-06-07 18:03:14.853 建立 11:00 標記
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID in ('00002MHW', '00002MHU', '00002MHZ', '00002MI0', '00002MI5')
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002MHW', '00002MHU', '00002MHZ', '00002MI0', '00002MI5')order by FDCREATED_DATE desc, FDUPDATED_DATE desc
	select * from TBUSERS where FSUSER_ID = '50868'
	select * from TBLOG_VIDEO where FSFILE_NO in ('P201606002220001', 'P201606002190001', 'P201606002180001')

	select v.FSTYPE,t.* from TBPTSAP_STATUS_LOG t join TBLOG_VIDEO v  on t.FSVIDEOID = v.FSVIDEO_PROG order by t.FDCREATED_DATE desc, t.FDUPDATE_DATE desc
	select * from TBLOG_VIDEO_HD_MC order by FDUPDATED_DATE desc, FDCREATED_DATE desc
end

if @case = 201605081502 begin
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID in ('00002MH9', '00002MHA', '00002MHH')
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002MH9', '00002MHA', '00002MHH')
end

if @case = 201606081557 begin
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002MIO' --FCFILE_STATUS = S 轉檔中
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'P201606002270001'
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'P201606002270001'
end

if @case = 201606081703 begin
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID in ('00002L0Y', '00002LCI', '00002KUW', '00002KUU') order by FDDATE --6/11, 6/12
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID in ('00002L0Y', '00002LCI', '00002KUW', '00002KUU')
	select * from TBPTSAP_STATUS_LOG order by FDUPDATE_DATE desc
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002L0Y', '00002LCI', '00002KUW', '00002KUU')
end

if @case = 20160090424 begin --上傳測試
	select * from TBLOG_VIDEO_HD_MC where FDCREATED_DATE > '20160608 23:00' and FSCREATED_BY = '51204'  order by FDCREATED_DATE desc
	select * from TBLOG_VIDEO_HD_MC where FDCREATED_DATE > '20160608 23:00' and FSCREATED_BY = '51204'  order by FDUPDATED_DATE desc
end

if @case = 201606091900 begin --上傳失敗
	select * from TBPTSAP_STATUS_LOG where FDUPDATE_DATE > '20160609' order by FDUPDATE_DATE desc -- 29
	select * from TBLOG_VIDEO_HD_MC where FDUPDATED_DATE > '20160609' order by FDUPDATED_DATE desc --37 FCSTATUS has changed

	-- upload failed
	select SUBSTRING(FSVIDEOID, 3, 6), * from TBPTSAP_STATUS_LOG where FDUPDATE_DATE > '20160609' and FCSTATUS <> 80 order by FSVIDEOID, FDUPDATE_DATE desc -- 8
	-- upload failed
	select * from TBLOG_VIDEO_HD_MC where FDUPDATED_DATE > '20160609' and FCSTATUS = 'E' order by FSVIDEO_ID, FDUPDATED_DATE desc --9 00002MRG was uploaded by PTS_AP, but not been log, its file name is 002MRG.mxf.mxf
	-- waiting for upload
	select SUBSTRING(FSVIDEO_ID, 3, 6), * from TBLOG_VIDEO_HD_MC where FDUPDATED_DATE > '20160609' and FCSTATUS = '' and FSUPDATED_BY IS NOT NULL order by FSVIDEO_ID, FDUPDATED_DATE desc

	select SUBSTRING(FSVIDEO_ID, 3, 6), * from TBLOG_VIDEO_HD_MC where FDUPDATED_DATE > '20160609' and FCSTATUS = 'C' and FSUPDATED_BY IS NOT NULL order by FSVIDEO_ID, FDUPDATED_DATE desc --23

	
	-- upload failed
	select UPPER(SUBSTRING(FSVIDEOID, 3, 6)), * from TBPTSAP_STATUS_LOG where FDUPDATE_DATE between '20160608' and '20160609' and FCSTATUS <> 80 order by FSVIDEOID, FDUPDATE_DATE desc -- 8
	-- upload failed
	select UPPER(SUBSTRING(FSVIDEO_ID, 3, 6)), * from TBLOG_VIDEO_HD_MC where FDUPDATED_DATE between '20160608' and '20160609' and FCSTATUS = 'E' order by FSVIDEO_ID, FDUPDATED_DATE desc --9 00002MRG was uploaded by PTS_AP, but not been log, its file name is 002MRG.mxf.mxf
	-- waiting for upload
	select SUBSTRING(FSVIDEO_ID, 3, 6), * from TBLOG_VIDEO_HD_MC where FDUPDATED_DATE > '20160610' and FCSTATUS = '' and FSUPDATED_BY IS NOT NULL order by FSVIDEO_ID, FDUPDATED_DATE desc

	select SUBSTRING(FSVIDEO_ID, 3, 6), * from TBLOG_VIDEO_HD_MC where FDUPDATED_DATE >= '20160610' and FCSTATUS <> 'E' and FSUPDATED_BY IS NOT NULL order by FSVIDEO_ID, FDUPDATED_DATE desc


	select * from TBLOG_VIDEO_HD_MC t join TBPGM_PROMO p on t.FSFILE_NO = p.FSPROMO_ID where t.FDUPDATED_DATE > '20160609' and t.FCSTATUS = 'E' order by t.FSVIDEO_ID, t.FDUPDATED_DATE desc
	select * from TBPGM_PROMO where FSPROMO_ID = '20160500746'
end

if @case = 201606111627 begin
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002MQP'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002MQP'
end

if @case = 201606120800 begin  --HD 節目重新載入後會變成 SD
	select * from TBLOG_VIDEO where FSVIDEO_PROG in ('0002J3G', '00002JEW', '00002K2A', '00002LIB', '00002MHH')
	select * from TBLOG_VIDEO where FSVIDEO_PROG in ('00002MH6', '00002MBO', '00002GNS', '00002K2M', '00002K29', '00002MBI')
end

if @case = 201606130927 begin
	select top 20 SUBSTRING(FSVIDEOID, 3, 6), * from TBPTSAP_STATUS_LOG order by FDUPDATE_DATE desc
	select top 20 SUBSTRING(FSVIDEO_ID, 3, 6), * from TBLOG_VIDEO_HD_MC order by FDUPDATED_DATE desc
end

if @case = 201606130936 begin --片庫派檔正常，但是到了 Approved 檔案只有 19 秒
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00001Z7E' 
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00001Z7E'
	select * from TBUSERS where FSUSER_ID = '08361'
end

if @case = 201606130948 begin --\\10.13.24.248\新聞部\短帶\獨立特派員\002MQ7日期錯置換.mxf
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID  = '00002MQ7' order by FDUPDATED_DATE desc
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002MQ7'
end

if @case = 201606131020 begin

end

if @case = 201606131118 begin -- 送帶轉檔單音短帶音軌是 MMMMMMM
	select * from TBLOG_VIDEO where FDUPDATED_DATE > '20160612' order by FDUPDATED_DATE
	select * from TBLOG_VIDEO_D where FDUPDATED_DATE > '20160612' order by FDUPDATED_DATE
	select * from TBLOG_VIDEO where FSBRO_ID = '201606100015' --G201695900220004
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201695900220004' --update by 08307 潘珍妮 at 2016-06-10 18:48:38.670
	select * from TBUSERS where FSUSER_ID = '08307'
	select * from TBLOG_VIDEO where FSTRACK = 'MMMMMMMM' order by FDUPDATED_DATE desc --636
	select FSFILE_NO, FSARC_TYPE, FCFILE_STATUS  from TBLOG_VIDEO where FDCREATED_DATE >= '20160613' order by FDCREATED_DATE
	select * from TBUSERS where FSUSER_ID = '51040'

end

if @case = 201606131217 begin  --HD 節目重新載入會變成 SD
	select p.FSPROMO_NAME, t.FSBRO_ID, t.FSFILE_NO, t.FSID, t.FNEPISODE, t.FSARC_TYPE, t.FCFILE_STATUS, t.FSTRANS_FROM, t.FSUPDATED_BY, t.FDUPDATED_DATE, t.FSVIDEO_PROPERTY, t.FCINGEST_QC, t.FSINGEST_QC_MSG,  t.FSTRACK 
	from TBLOG_VIDEO t join TBPGM_PROMO p on t.FSID = p.FSPROMO_ID
	where FSVIDEO_PROG in ('00002J3F', '00002J3G', '00002K2Q', '00002K2R', '00002K2M', '00002JEW', '00002MBI', '00002MB2', '00002MBO', 
	'00002LIB', '00002MH6', '00002MHH', '00002GNT', '00002GNS', '00002GNT', '00002L2K', '00002LW3')

	select p.FSPROMO_NAME, t.FSVIDEO_PROG ,t.FSBRO_ID, t.FSFILE_NO, t.FSID, t.FNEPISODE, t.FSARC_TYPE, t.FCFILE_STATUS, t.FSTRANS_FROM, t.FSUPDATED_BY, t.FDUPDATED_DATE, t.FCINGEST_QC, t.FSINGEST_QC_MSG,  t.FSTRACK 
	from TBLOG_VIDEO t join TBPGM_PROMO p on t.FSID = p.FSPROMO_ID  
	where FSVIDEO_PROG in ('00002J3F', '00002J3G', '00002K2Q', '00002K2R', '00002K2M', '00002JEW', '00002MBI', '00002MB2', '00002MBO', 
	'00002LIB', '00002MH6', '00002MHH', '00002GNT', '00002GNS', '00002GNT', '00002L2K', '00002LW3')

	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002MH6' order by FDDATE desc

	select * from TBBROADCAST where FSBRO_ID = '201605170068'

	select * from TBPGM_QUEUE where FDDATE = '20160624' order by FSBEG_TIME
	select * from TBPGM_COMBINE_QUEUE where FDDATE = '2016-06-24' and FSCHANNEL_ID = '12' order by FSPLAY_TIME
end

if @case = 201606131447 begin -- 檔案上傳問題, 已經上傳完成
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID in ('00002MRO', '00002MRD', '00002MRJ', '00002MRA', '00002MRM')
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002MRO', '00002MRD', '00002MRJ', '00002MRA', '00002MRM')
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID in ('00002MRO', '00002MRD', '00002MRJ', '00002MRA', '00002MRM') order by FDDATE
end

if @case = 201606131527 begin --最後 16 分鐘黑畫面 重新排播後
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002LNQ'
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002LNQ'
	select * from TBUSERS where FSUSER_ID = '08361'
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002LNQ'
end

if @case = 201606131557 begin  -- 比對段落問題
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002MZV', '00002MQ4', '00002LYX')
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002MM0', '00002MZR')
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID in ('00002MQ4', '00002MM0', '00002MZR')
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002LYX'   --標記後未上傳-> haka nas has no file
	select * from TBPGM_PROMO where FSPROMO_ID = '20160600368'

	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002MM0'  --無檔案
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID in ('00002MM2', '00002MM1', '00002MLY', '00002MQ4')
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002MM2', '00002MM1', '00002MLY', '00002MQ4')
end

--bug
if @case = 201606131713 begin -- 審核通過有低解但是高解未入庫
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002MK0' --2016-06-09 11:48:28.900 審核通過 
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002MK0'
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002MK0' order by FDDATE
	select * from TBPGM_PROMO where FSPROMO_ID = '20160600232' --PTS1 一字千金榜中榜promo #24

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002MJS'
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002MJS'
	select * from TBLOG_VIDEO where FSVIDEO_PROG in (
'00002MHW','00002MHX','00002MHY','00002MI5','00002MHZ','00002MI0','00002M3X','00002M3Y','00002KEG','00002KEH','00002MUS','00002MGZ','00002MQ0','00002MK0','00002MUI','00002MUJ','00002MUK','00002MUM','00002MUN','00002MUQ','00002MUP',
'00002MUO','00002M1Q','00002M1R','00002M1S','00002M1T','00002M1U','00002M20','00002M1V','00002M1W','00002M1X','00002M1Y','00002M1Z','00002M10','00002MHH','00002MPH','00002MHA','00002MH9','00002MPC','00002MPI',
'00002MPF','00002MQQ','00002L2O','00002M41','00002MOT','00002MH2','00002MH3','00002MH4','00002MH1','00002M46','00002M4A','00002M49','00002M4B','00002MRG','00002LTO','00002MB2','00002M4D','00002MM2','00002MLY','00002MM1',
'00002MM0','00002MQ4','00002M3U','00002MP4','00002LCI','00002MC8','00002MM7','00002KY6','00002MH8','00002LUU','00002KUU','00002KUW','00002LZP','00002LTH','00002LTI','00002LTJ','00002LTK','00002LTL','00002KTO','00002L0Y','00002LYX','00002MG6',
'00002LZ8','00002MQP','00002LXW','00002MJS'
)

select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201366600050002'

select * from TBLOG_VIDEO_D where FDCREATED_DATE > '2016-05-01'


end

if @case = 201606161435 begin --上傳檔案問題  HD-> SD
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002N3U', '00002N48', '00002LIE') --標記後未上傳
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID in ('00002N3U', '00002N47', '00002N55') order by FDDATE
	select * from TBLOG_VIDEO_HD_MC order by FDUPDATED_DATE desc
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002LIE'  order by FDUPDATE_DATE desc -- 2016-06-01 15:09:55.833 上傳
end

if @case = 201606141653 begin --TS3310, TS3500 磁帶上下架
	select  * from TBTRACKINGLOG where FDTIME > '2016-06-14 15:00' and FSMESSAGE like '%TS3310%' order by FDTIME
	--TS3310 磁帶上架 2016-06-14 16:53:40.080 / WSTSM/CheckinTapes_TSM3310 / 執行TS3310簽入動作：51204
	select  * from TBTRACKINGLOG where FDTIME > '2016-06-14 16:50' order by FDTIME
	--TS3500 磁帶上架 18 支
	select  * from TBTRACKINGLOG where FDTIME > '2016-06-14 17:50' order by FDTIME
	select * from TBTRAN_LOG where FDDATE > '2016/06/14 17:50' order by FDDATE
end

if @case = 201606141953 begin --審核通過 檔案未到
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002LZP'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002LZP' --2016-06-08 09:24:38.183 審核通過
end

if @case = 201606142007 begin --檔案在Approved  但是檔案狀態為檔案未到
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002N55' -- 2016-06-14 16:38:13.400 審核通過
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002N55'
	select * from TBPGM_ARR_PROMO where FDDATE = '2016-06-15' and FSCHANNEL_ID = '11' and FSVIDEO_ID = '00002N55' order by FSPLAYTIME  --2016-06-13 16:22:30.507 建立
end

if @case = 201606150930 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002MUU'
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002MUU' order by FDDATE --2016-06-15 #31   created : 2016-06-14 18:06:53.693
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002MUU' -- 無送播單
	select * from TBPGM_PROMO where FSPROMO_ID = '20160600377'
	select * from TBLOG_VIDEO where FSID = '20160600377'
end


if @case = 201606161335 begin
	select * from TBPGM_COMBINE_QUEUE where FDDATE = '2016-06-14' order by FSPLAY_TIME
	select * from TBPGM_COMBINE_QUEUE where FDDATE >= CURRENT_TIMESTAMP and FSVIDEO_ID = '00002LTN' order by FDDATE
end

if @case = 201606151406 begin --檔案長度30 秒 QC只有20 秒
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002MOB'
	select * from TBLOG_VIDEO_D where FSFILE_NO =  'P201606003370002' --no record
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002MOB'
	select * from TBPGM_PROMO where FSPROMO_ID = '20160600337' --十點全紀錄-天有風雲 DOC VIEW_wild weather
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'P201606003370002'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002MOB'
end

if @case = 201606151448 begin --HD (B:帶轉） -> SD（B:已轉）
	select * from TBLOG_VIDEO where FSVIDEO_PROG in ('00002J3F', '00002K2M', '00002K29', '00002K2Q', '00002ND9', '00002AU3')
	select * from TBLOG_VIDEO where FSID in ('20160500372', '20160500373', '20160500145', '20160500001', '20160600496', '20151200796')
end



if @case = 201606151558 begin
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002NED' --2016-07-06
	select * from TBPGM_ARR_PROMO where FDDATE = '2016-07-06' and FSCHANNEL_ID = '11'
end

/******************************************************************************
 ***************** 查詢 HD 有入庫審核通過的節目，有 SD 的檔案 ****************
 ******************************************************************************/
if @case = 201606161831 begin 
	select * from TBPGM_COMBINE_QUEUE where FDDATE > '2016-06-15' order by FSVIDEO_ID

	-- 查詢播出節目的審核狀態(帶頻道資訊）
	select q.FSCHANNEL_ID ,t.* from TBLOG_VIDEO_HD_MC t 
		join (select FSCHANNEL_ID, FSVIDEO_ID from TBPGM_COMBINE_QUEUE where FDDATE = '2016-06-17'
			group by FSVIDEO_ID, FSCHANNEL_ID) q on t.FSVIDEO_ID = q.FSVIDEO_ID

	-- 查詢播出節目的審核狀態(不帶頻道資訊）
	-- TBLOG_VIDEO_HD_MC 也有 SD
	select q.FSPROG_ID, q.FSPROG_NAME, q.FNEPISODE, t.* from TBLOG_VIDEO_HD_MC t
		join 
			(select FSPROG_NAME, FSPROG_ID, FNEPISODE, FSVIDEO_ID from TBPGM_COMBINE_QUEUE 
				where FDDATE = '2016-06-17'
				group by FSPROG_NAME, FSPROG_ID, FNEPISODE, FSVIDEO_ID) q 
		on t.FSVIDEO_ID = q.FSVIDEO_ID

	-- filter SD in TBLOG_VIDEO_HD_MC
	select q.FSPROG_ID, q.FSPROG_NAME, q.FNEPISODE, t.* from TBLOG_VIDEO_HD_MC t
		join 
			(select FSPROG_NAME, FSPROG_ID, FNEPISODE, FSVIDEO_ID from TBPGM_COMBINE_QUEUE 
				where FDDATE = '2016-06-17'
				group by FSPROG_NAME, FSPROG_ID, FNEPISODE, FSVIDEO_ID) q
		on t.FSVIDEO_ID = q.FSVIDEO_ID
		where t.FSVIDEO_ID not in (select FSVIDEO_PROG from TBLOG_VIDEO where FSARC_TYPE = '001') and t.FCSTATUS = 'O'

	-- HD(片庫轉檔或檔案送播）已播出審核過，同時也有SD 檔案的節目
	select th.FSPROG_ID, th.FSPROG_NAME, th.FNEPISODE, t.FSBRO_ID, t.FSVIDEO_PROG, t.FSARC_TYPE from TBLOG_VIDEO t
		join
			(select q.FSPROG_ID, q.FSPROG_NAME, q.FNEPISODE, t.* from TBLOG_VIDEO_HD_MC t 
				join 
					(select FSPROG_NAME, FSPROG_ID, FNEPISODE, FSVIDEO_ID from TBPGM_COMBINE_QUEUE where FDDATE >= '2016-03-29'
						group by FSPROG_NAME, FSPROG_ID, FNEPISODE, FSVIDEO_ID) q 
				on t.FSVIDEO_ID = q.FSVIDEO_ID
				where t.FSVIDEO_ID not in (select FSVIDEO_PROG from TBLOG_VIDEO where FSARC_TYPE = '001') and t.FCSTATUS = 'O') th
		on t.FSID = th.FSPROG_ID and t.FNEPISODE = th.FNEPISODE and t.FSARC_TYPE = '001' and t.FCFILE_STATUS = 'T'
		order by th.FSPROG_ID, th.FNEPISODE

	--  HD(片庫轉檔或檔案送播）已播出審核過，同時也有SD 檔案的節目, 增加檔案大小欄位
	select th.FSPROG_ID, th.FSPROG_NAME, th.FNEPISODE, t.FSBRO_ID, t.FSVIDEO_PROG, t.FSARC_TYPE, SUBSTRING(t.FSFILE_SIZE, 0, LEN(t.FSFILE_SIZE)-1) 檔案大小, t.FSFILE_SIZE from TBLOG_VIDEO t
		join
			(select q.FSPROG_ID, q.FSPROG_NAME, q.FNEPISODE, t.* from TBLOG_VIDEO_HD_MC t 
				join 
					(select FSPROG_NAME, FSPROG_ID, FNEPISODE, FSVIDEO_ID from TBPGM_COMBINE_QUEUE where FDDATE >= '2016-3-29'
						group by FSPROG_NAME, FSPROG_ID, FNEPISODE, FSVIDEO_ID) q 
				on t.FSVIDEO_ID = q.FSVIDEO_ID
				where t.FSVIDEO_ID not in (select FSVIDEO_PROG from TBLOG_VIDEO where FSARC_TYPE = '001') and t.FCSTATUS = 'O') th
		on t.FSID = th.FSPROG_ID and t.FNEPISODE = th.FNEPISODE and t.FSARC_TYPE = '001' and t.FCFILE_STATUS = 'T'
		order by th.FSPROG_ID, th.FNEPISODE

	select * from TBLOG_VIDEO_HD_MC where FSBRO_ID = '201306280003'
end


if @case = 201606161027 begin  --主控說檔案未到
	--除了 002MBQ 為 SD 送帶轉檔單，其餘都已經在 Approved
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID in ('00002MH4', '00002MGZ', '00002KAE', '00002JHZ', '00002MAS', '00002LPL', '00002MBQ', '0000NAJ')
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002MH4', '00002MGZ', '00002KAE', '00002JHZ', '00002MAS', '00002LPL', '00002MBQ', '0000NAJ')

	select * from (select FDDATE pDATE, FSVIDEO_ID videoID from TBPGM_COMBINE_QUEUE UNION (select FDDATE pDate, FSVIDEO_ID videoID from TBPGM_ARR_PROMO)) t 
	where videoID in ('00002MH4', '00002MGZ', '00002KAE', '00002JHZ', '00002MAS', '00002LPL', '00002MBQ', '0000NAJ') order by pDate desc

	select * from (select FDDATE pDATE, FSVIDEO_ID videoID from TBPGM_COMBINE_QUEUE UNION (select FDDATE pDate, FSVIDEO_ID videoID from TBPGM_ARR_PROMO)) t 
	where videoID in ('00002MBQ') --2016-06-16 播出，尚未上傳

	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002MBQ'  --SD 送帶轉檔單
end

if @case = 201606161057 begin --未轉低解檔及入庫的檔案要重新審核
	select p.FSPROMO_NAME, t.* from TBPGM_ARR_PROMO t join  TBPGM_PROMO p on t.FSPROMO_ID = p.FSPROMO_ID where FSVIDEO_ID in ('00002M4M', '00002MHU') order by FDDATE desc --要重新審核
	--黑盒子 宣傳帶測試版 00002MHU 檔案仍在 Approved 用 6/15 的表審核
	--颱風警報promo 00002M4M 檔案已刪除，手動搬到 Review 用 6/8 表審核 -> 未搬到 Approved
end

if @case = 201606161411 begin --找出 SD 節目
	select p.FSPGMNAME , t.* from TBLOG_VIDEO t join TBPROG_M p on t.FSID = p.FSPROG_ID where t.FSARC_TYPE = '001' and t.FCFILE_STATUS = 'T' order by FDCREATED_DATE
	select p.FSPGMNAME 節目名稱, t.FNEPISODE, t.FSBRO_ID 送帶轉檔單號, t.FSFILE_NO, t.FSFILE_SIZE, t.FSFILE_PATH_H, t.FDCREATED_DATE, t.FDUPDATED_DATE from TBLOG_VIDEO t join TBPROG_M p on t.FSID = p.FSPROG_ID 
		where t.FSARC_TYPE = '001' and t.FCFILE_STATUS = 'T' order by FDUPDATED_DATE

	select p.FSPGMNAME 節目名稱, t.FNEPISODE 集數, t.FSBRO_ID 送帶轉檔單號, CAST(t.FDUPDATED_DATE as DATE) 轉檔時間 from TBLOG_VIDEO t join TBPROG_M p on t.FSID = p.FSPROG_ID 
		where t.FSBRO_ID 
		in ('201203120006', '201203090019', '201203120012',  '201203230099', '201208010031', '201208010056', '201208140122', '201208210001', '201208240009', '201305170030'
		, '201305030082', '201305240065', '201306170007', '201306130015', '201306240021', '201307190027', '201307110049', '201211210033', '201403130029', '201403240009'
		, '201404250051', '201404300064', '201404100087', '201409230007', '201407280144', '201410240001', '201410150002', '201411100013', '201412030044', '201411130048'
		, '201412230114', '201501140007', '201503170028') order by t.FDUPDATED_DATE
end

if @case = 201606161505 begin --轉檔成功，搬檔失敗
	select * from TBLOG_VIDEO where FSID = '2012078' and FNEPISODE = 190 -- FCFILE_STATUS = R 轉檔成功，搬檔失敗  00002LCB
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002LCB'
end

if @case = 201606161546 begin --客台送帶轉檔單抽單問題
	select * from TBLOG_VIDEO where FSID = '2011092' and FSARC_TYPe = '001' and FCFILE_STATUS = 'T'
	select FCFILE_STATUS, * from TBLOG_VIDEO where FSID = '2011092' and FSARC_TYPe = '001' order by FNEPISODE
end

if @case = 201606161836 begin
	select * from TBPROG_M where FSPGMNAME = '轉動百年阿里山'
	select * from TBLOG_VIDEO where FSID = '2012389' and FNEPISODE = 1 and FSARC_TYPE = '002'
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201238900010007'
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201238900010007'

	exec SP_U_TBLOG_VIDEO_FSTRACK 'G201238900010007','MMMM', '51204'
	Update TBLOG_VIDEO Set FSTRACK = 'MMMM'
		where FSFILE_NO = 'G201238900010007'

	select * from TBPROG_M where FSPROG_ID = '2016658'
	select * from TBLOG_VIDEO where FSID = '2016658' and FNEPISODE = 2 and FSARC_TYPE = '001' and FCFILE_STATUS = 'T' --G201665800020010
	exec SP_U_TBLOG_VIDEO_FSTRACK 'G201665800020010','LRLR', '51204'

	select * from TBPROG_M where FSPROG_ID = '2012447' --加油！小廚師
	select * from TBLOG_VIDEO where FSID = '2012447' and FNEPISODE = 6 and FSARC_TYPE = '002'--
	select * from TBLOG_VIDEO where FSID = '2012447' and FNEPISODE = 7 and FSARC_TYPE = '002'
	exec SP_U_TBLOG_VIDEO_FSTRACK 'G201244700060002','MMMM', '51204'
	exec SP_U_TBLOG_VIDEO_FSTRACK 'G201244700070002','MMMM', '51204'
end

if @case = 201606171024 begin --台灣食堂 報表輸出只有20 集
	select * from TBLOG_VIDEO where FSID = '2013540' and FCFILE_STATUS = 'T' order by FNEPISODE
end

if @case = 201606171044 begin --找出有雙語節目
	select FSPROG_ID 節目單號, FSPGMNAME 節目名稱, (select FSPROGLANGNAME from TBZPROGLANG where FSPROGLANGID1 = FSPROGLANGID) 主聲道,
    (select FSPROGLANGNAME from TBZPROGLANG where FSPROGLANGID2 = FSPROGLANGID) 副聲道 from TBPROG_M  where  FSPROGLANGID1 <> FSPROGLANGID2 --FSPROGLANGID1, FSPROGLANGID2
	select * from TBZPROGLANG
	select * from TBZPROGAUD --目標觀眾
end

if @case = 2016061724 begin --標記上傳失敗
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00001X1F'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00001X1F' -- 201506030024 程式 產生

	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002MMO'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002MMO'

	select * from TBPTSAP_STATUS_LOG where FCSTATUS <> '80' order by FDUPDATE_DATE desc
end

if @case = 201606200951 begin --片庫調用
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID in ('000027WT', '00001M2S') -- 未上傳
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('000027WT', '00001M2S')
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID in ('000027WT', '00001M2S')
	--圓點女王 草間彌生 000027WT 2016-06-22 司馬庫斯 森林共和國 000027WT 2016-06-22
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID in ('000027WT', '00001M2S') order by FDDATE desc
	select * from TBLOG_VIDEO where FSVIDEO_PROG in ('000027WT', '00001M2S')
	select * from TBLOG_VIDEO where FSID = '2015002' --圓點
	select * from TBBROADCAST where FSBRO_ID in ('201412300182', '201511190011')  --50391, 60159
	select * from TBUSERS where FSUSER_ID in ('71355', '08255') --陳素娥, 郭哲安 轉檔

end

if @case = 201606201359 begin
	select * from TBZCHANNEL where FSCHANNEL_ID = '62'
	--update TBZCHANNEL
end

if @case = 201606201403 -- 檔案審核轉檔後未入庫
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002MH8' --\\mamstream\MAMDFS\ptstv\HVideo\2014\2014841\G201484100400003.mxf 22G, 在 Approved 只有 1.6G
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002MH8' order by FDDATE --6/19 PTS1 #191 
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002MH8' --2016-06-12 17:31 審核通過，但是檔案未到  --> 必須要有警示
end

if @case = 201606201517 begin --anystream 轉檔完成，但是 MAM 未收到通知，在轉檔中心仍顯示轉檔中
	select * from TBLOG_VIDEO where FSID = '2006855' and FNEPISODE = 488 -- S:轉檔中
end

if @case = 201606201529 begin
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002NQ2' --2016-06-16 14:55:05.543 上傳失敗
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002NQ2'
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002NQ2'
end

if @case = 201606201655 begin --2 支檔案手動搬檔入庫失敗
	
end

if @case = 201606201716 begin -- 匯入 Etere SOM  錯誤
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002LA3'
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002LA3' --2016-06-22 最美麗的花
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002LA3'
	--XDCAMViewer 檢查檔案正常
end

if @case = 201606202049 begin
	select * from TBPROG_M where FSPGMNAME like '%客家人有名堂%'
	select * from TBLOG_VIDEO where FSID = '2014480' and FNEPISODE = 31
end

if @case = 201606210502 begin
	select * from TBPTSAP_STATUS_LOG where CAST(FDUPDATE_DATE as DATE) = '2016-06-16' and FSVIDEOID = '00002NQ2'
	select * from TBPTSAP_STATUS_LOG where CAST(FDCREATED_DATE as DATE) = '2016-06-16' and FSVIDEOID = '00002NQ2' --2016-06-20 16:04:25.167 重新上傳
	select * from TBPTSAP_STATUS_LOG where CAST(FDCREATED_DATE as DATE) = '2016-06-20' and FCSTATUS <> 80
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002NQ2'
end

if @case = 201606210907 begin --SD TEST
	select top 100 * from TBLOG_VIDEO where FSARC_TYPE = '001'
	select * from TBLOG_VIDEO where FSBRO_ID = '201208010056'  --小王子 46 00000AG3 --> 00002OJL
	select * from TBLOG_VIDEO where FSBRO_ID = '201203230099'   --搖滾保母 000002ZC -> 00002OJM
	select * from TBLOG_VIDEO where FSBRO_ID = '201411130048' --勝利催落去 1 00001J32 -> 00002OJN
	select * from TBLOG_VIDEO_HD_MC where FSCREATED_BY =  '51204' and CAST(FDCREATED_DATE as DATE) = '2016-06-21'
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002NBN'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002NBN'
end

if @case = 201606211338 begin  --無法建立短帶送帶轉檔單
	select * from TBPGM_PROMO where FSPROMO_ID = '20160600842' --PTS1公視藝文大道第193集Promo
	select * from TBLOG_VIDEO where FSID = '20160600842' and FSARC_TYPE = '001' -- 已有 SD 送播單 201606200229，且已上傳無法抽單
	select * from TBBROADCAST where FSBRO_ID = '201606200229'
	--可以些讓 SD 送播單審核通過，轉檔完成後，再以送帶轉檔單置換
	
end

if @case = 201606211709 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002I4Q', '00002J62')--GPFS -> 待播區 拷貝失敗，錯誤訊息：指定的網路名稱無法使用
	select * from TBLOG_VIDEO where FSVIDEO_PROG in ('00002I4Q', '00002J62')
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID in  ('00002I4Q', '00002J62') order by FDDATE
end

/************************************************************
			統計各頻道已上傳檔案
*************************************************************/
if @case = 201606212329 begin --
	select * from TBLOG_VIDEO where FSVIDEO_PROG in  ('00002LZR', '00002M3L')
	select FSCHANNEL_ID, COUNT(FSVIDEO_PROG) from TBLOG_VIDEO group by FSCHANNEL_ID

	--declare tmpTable TABLE(PLAY_TIME varchar(30), FSPGMNAME varchar(50),
	--select OBJECT_ID(N'tempdb.dbo.#tmpFileUpload', N'U')
	drop table #tmpFileUpload

--------------------------------------------------------------------------------------------------------------------

	-- 查詢已上傳的節目數量
	declare @date DATE = '2016-06-27' 
	if OBJECT_ID(N'tempdb.dbo.#tmpPGMUpload', N'U') IS NOT NULL
		DROP TABLE #tmpPGMUpload

	-- 無播出日期
	select p.FSPGMNAME, t.FNEPISODE, UPPER(s.FSVIDEOID) as VIDEO_ID, t.FSCHANNEL_ID, t.FSFILE_SIZE, s.FDCREATED_DATE '開始上傳', s.FDUPDATE_DATE '上傳完成', DATEDIFF(MINUTE, s.FDCREATED_DATE, s.FDUPDATE_DATE) '時程(MIN)',
	s.FCSTATUS, FSPERCENTAGE, s.FCRESENDTIMES into #tmpPGMUpload
	from (TBPTSAP_STATUS_LOG s left join TBLOG_VIDEO t on s.FSVIDEOID = t.FSVIDEO_PROG) join TBPROG_M p on t.FSID = p.FSPROG_ID 
	left join (select FSPROG_NAME, FNEPISODE, FSVIDEO_ID from TBPGM_COMBINE_QUEUE group by FSPROG_NAME, FNEPISODE, FSVIDEO_ID) q on s.FSVIDEOID = q.FSVIDEO_ID
	where t.FSTYPE = 'G' and s.FCSTATUS = 80 and CAST(s.FDUPDATE_DATE as DATE) >= @date
	group by p.FSPGMNAME, s.FSVIDEOID, t.FSID, t.FNEPISODE, t.FSFILE_SIZE, s.FDCREATED_DATE, s.FDUPDATE_DATE, s.FCSTATUS, s.FSPERCENTAGE, s.FCRESENDTIMES, t.FSCHANNEL_ID
	order by s.FDCREATED_DATE, s.FDUPDATE_DATE


	-- 有播出日期
	--declare @date DATE = '2016-06-23' 
	if OBJECT_ID(N'tempdb.dbo.#tmpPGMUpload2', N'U') IS NOT NULL
		DROP TABLE #tmpPGMUpload2

	select CAST(q.FDDATE as varchar(20)) + ' ' + MIN(q.FSPLAY_TIME) as 'PLAY TIME', p.FSPGMNAME, t.FNEPISODE, UPPER(s.FSVIDEOID) as VIDEO_ID, t.FSCHANNEL_ID, t.FSFILE_SIZE, s.FDCREATED_DATE '開始上傳', s.FDUPDATE_DATE '上傳完成', DATEDIFF(MINUTE, s.FDCREATED_DATE, s.FDUPDATE_DATE) '時程(MIN)',
	s.FCSTATUS, FSPERCENTAGE, s.FCRESENDTIMES into #tmpPGMUpload2
	from (TBPTSAP_STATUS_LOG s left join TBLOG_VIDEO t on s.FSVIDEOID = t.FSVIDEO_PROG) join TBPROG_M p on t.FSID = p.FSPROG_ID 
	left join TBPGM_COMBINE_QUEUE q on s.FSVIDEOID = q.FSVIDEO_ID
	where t.FSTYPE = 'G' and s.FCSTATUS = 80 and CAST(s.FDUPDATE_DATE as DATE) >= @date
	group by p.FSPGMNAME, s.FSVIDEOID, t.FSID, t.FNEPISODE, t.FSFILE_SIZE, s.FDCREATED_DATE, s.FDUPDATE_DATE, s.FCSTATUS, s.FSPERCENTAGE, q.FDDATE, s.FCRESENDTIMES, t.FSCHANNEL_ID
	order by s.FDCREATED_DATE, s.FDUPDATE_DATE

	select FSCHANNEL_ID, FSPGMNAME, FNEPISODE from #tmpPGMUpload2 group by FSPGMNAME, FNEPISODE, VIDEO_ID, FSCHANNEL_ID order by FSCHANNEL_ID

	select u.FSCHANNEL_ID, c.FSCHANNEL_NAME '頻道', COUNT(u.FSCHANNEL_ID) '上傳節目數量' from #tmpPGMUpload u join TBZCHANNEL c on u.FSCHANNEL_ID = c.FSCHANNEL_ID 
	group by u.FSCHANNEL_ID, c.FSCHANNEL_NAME
	order by  u.FSCHANNEL_ID

	select distinct FSPGMNAME, FNEPISODE, FSCHANNEL_ID from #tmpPGMUpload
	select FSPGMNAME, FNEPISODE, FSCHANNEL_ID, COUNT(FSPGMNAME) from #tmpPGMUpload group by FSPGMNAME, FNEPISODE, FSCHANNEL_ID
	select FSCHANNEL_ID, COUNT(FSPGMNAME) from #tmpPGMUpload group by FSCHANNEL_ID
	select * from #tmpPGMUpload

	select * from TBPTSAP_STATUS_LOG where CAST(FDCREATED_DATE as DATE) = '2016-06-22'

	select FSPROG_NAME, FNEPISODE, FSVIDEO_ID from TBPGM_COMBINE_QUEUE group by FSPROG_NAME, FNEPISODE, FSVIDEO_ID

	--select * from #tmpPGMUpload
	--select * from #tmpPGMUpload2

---------------------------------------------------------------------------------------------------------------------------

	-- 查詢已上傳的短帶數量
	declare @date DATE = '2016-06-24' -- 上傳日期
	if OBJECT_ID(N'tempdb.dbo.#tmpPromoUpload', N'U') IS NOT NULL
		DROP TABLE #tmpPromoUpload

	select CAST(q.FDDATE as varchar(20)) + ' ' + MIN(q.FSPLAYTIME) as 'PLAY TIME', p.FSPROMO_NAME, t.FNEPISODE, UPPER(s.FSVIDEOID) as VIDEO_ID, t.FSCHANNEL_ID, t.FSFILE_SIZE, s.FDCREATED_DATE '開始上傳', s.FDUPDATE_DATE '上傳完成', DATEDIFF(MINUTE, s.FDCREATED_DATE, s.FDUPDATE_DATE) '時程(MIN)',
	s.FCSTATUS, FSPERCENTAGE, s.FCRESENDTIMES
	into #tmpPromoUpload
	from (TBPTSAP_STATUS_LOG s left join TBLOG_VIDEO t on s.FSVIDEOID = t.FSVIDEO_PROG) join TBPGM_PROMO p on t.FSID = p.FSPROMO_ID left join TBPGM_ARR_PROMO q on s.FSVIDEOID = q.FSVIDEO_ID
	where t.FSTYPE = 'P' and s.FCSTATUS = 80 and CAST(s.FDUPDATE_DATE as DATE) >= @date 
	group by p.FSPROMO_NAME, s.FSVIDEOID, t.FSID, t.FNEPISODE, t.FSFILE_SIZE, s.FDCREATED_DATE, s.FDUPDATE_DATE, s.FCSTATUS, s.FSPERCENTAGE, s.FCRESENDTIMES, q.FDDATE, t.FSCHANNEL_ID
	order by s.FDCREATED_DATE desc, s.FDUPDATE_DATE desc

	select FSCHANNEL_ID, FSPROMO_NAME, FNEPISODE from #tmpPromoUpload  group by FSPROMO_NAME, FNEPISODE, VIDEO_ID, FSCHANNEL_ID order by FSCHANNEL_ID

	select u.FSCHANNEL_ID, c.FSCHANNEL_NAME '頻道', COUNT(u.FSCHANNEL_ID) '上傳短帶數量' from #tmpPromoUpload u join TBZCHANNEL c on u.FSCHANNEL_ID = c.FSCHANNEL_ID 
	group by u.FSCHANNEL_ID, c.FSCHANNEL_NAME
	order by u.FSCHANNEL_ID

------------------------------------------------------------------------------------------------------------------------------

	select * from TBZCHANNEL
	select * from sys.objects where name like '%FileUpload%'

-- store in tempdb 暫存資料表

	drop table newTable
	select * from newTable
end

if @case = 201606221044 begin -- 10.13.15.60 斷線, 192.168.100.102 無法連線
	select * from TBLOG_VIDEO_HD_MC order by FDUPDATED_DATE desc
	select * from TBPTSAP_STATUS_LOG order by FDCREATED_DATE desc --00002onl 上傳失敗 客台檔案 00002onl, 00002om8, 00002omo, 00002oo6

	select SUBSTRING(t.FSVIDEOID, 3, 6) VIDEO_ID, t.FCSTATUS, h.* from TBPTSAP_STATUS_LOG t left join TBLOG_VIDEO_HD_MC h on t.FSVIDEOID = h.FSVIDEO_ID
	where t.FCSTATUS <> 80 and CAST(t.FDCREATED_DATE as DATE) = '2016-06-22' order by t.FDCREATED_DATE desc --8 支上傳失敗

	select * from TBLOG_VIDEO_HD_MC where FCSTATUS = 'E' and CAST(FDUPDATED_DATE as DATE) = '2016-06-22'  order by FDUPDATED_DATE desc --6 ?  00002nnx
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002nnx'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002NNX'
end

if @case = 201606221824 begin -- 以歷史紀錄審核的檔案
	select * from TBLOG_VIDEO where FSVIDEO_PROG in ('00002F5K', '00002NEW', '00002BKA', '00002F5J', '00002O3E')
end

if @case = 201606231000 begin --客台檔案上傳問題
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002mql'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002mql', '00002NE2', '00002NYY')
end

select * from TBPGM_COMBINE_QUEUE where CAST(FDDATE as DATE) = '2016-06-23' and FSVIDEO_ID = '00002MQL'
select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002MQL'

if @case = 201606231434 begin --找出有雙語節目
	select FSPROG_ID 節目單號, FSPGMNAME 節目名稱, t.FNEPISODE 集數, (select FSPROGLANGNAME from TBZPROGLANG where FSPROGLANGID1 = FSPROGLANGID) 主聲道,
    (select FSPROGLANGNAME from TBZPROGLANG where FSPROGLANGID2 = FSPROGLANGID) 副聲道,
	t.FSBRO_ID, t.FSVIDEO_PROG, 
	CASE 
		WHEN t.FSARC_TYPE = '001' THEN 'SD'
		WHEN t.FSARC_TYPE = '002' THEN 'HD'
	END 規格
	-- t.FSFILE_PATH_H, t.FCFILE_STATUS
	from TBPROG_M p join TBLOG_VIDEO t on p.FSPROG_ID = t.FSID
	where  FSPROGLANGID1 <> FSPROGLANGID2 and t.FCFILE_STATUS = 'T' and (t.FSARC_TYPE = '001' or t.FSARC_TYPE = '002')  --FSPROGLANGID1, FSPROGLANGID2
	order by 規格

	-- 審核通過 205
	select FSPROG_ID 節目單號, FSPGMNAME 節目名稱, t.FNEPISODE 集數, (select FSPROGLANGNAME from TBZPROGLANG where FSPROGLANGID1 = FSPROGLANGID) 主聲道,
    (select FSPROGLANGNAME from TBZPROGLANG where FSPROGLANGID2 = FSPROGLANGID) 副聲道,
	t.FSBRO_ID, t.FSVIDEO_PROG, 
	CASE 
		WHEN t.FSARC_TYPE = '001' THEN 'SD'
		WHEN t.FSARC_TYPE = '002' THEN 'HD'
	END 規格
	--, t.FSFILE_PATH_H, t.FCFILE_STATUS
	from TBPROG_M p join TBLOG_VIDEO t on p.FSPROG_ID = t.FSID
	join TBLOG_VIDEO_HD_MC h on t.FSVIDEO_PROG = h.FSVIDEO_ID
	where  FSPROGLANGID1 <> FSPROGLANGID2 and t.FCFILE_STATUS = 'T' and (t.FSARC_TYPE = '001' or t.FSARC_TYPE = '002')  --FSPROGLANGID1, FSPROGLANGID2
	and h.FCSTATUS = 'O'
	order by 規格

	--SD/HD 檔案數量
	select * from TBLOG_VIDEO where FSARC_TYPE = '001' and FSTYPE = 'G' and FCFILE_STATUS = 'T' --15589
	select * from TBLOG_VIDEO where FSARC_TYPE = '002' and FSTYPE = 'G' and FCFILE_STATUS = 'T' --1148

	select * from TBZPROGLANG
	select * from TBZPROGAUD --目標觀眾
end

if @case = 201606231606 begin --將宏觀改成 HD 頻道
	select * from TBZCHANNEL where FSCHANNEL_ID = '08'
	update TBZCHANNEL set FSCHANNEL_TYPE = '002' where FSCHANNEL_ID = '08'
end

if @case = 201606231648 begin -- 雙語測試 6/24 macro

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002NFH' --待審核 N, copy from Review to Approved manually 無資料寫入 louth db
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00001MLL' --待審核 N, copy from Review to Approved manually
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002EZD' -- 審核通過 O copy from GPFS to Approved by system
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002EZF' -- GPFS to Review
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00001M2S' -- O-
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00001O3J'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00000CB2'

	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00001O3J' -- HD 送帶轉檔單待轉檔
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00000CB2' -- HD 送帶轉檔單待轉檔
end

if @case = 201606231721 begin --檔案未到主控
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002B7K'
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002B7K'  --SD 待轉檔(B)
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002B7K' order by FDDATE
end

if @case = 201606232152 begin  -- 只有 SD 送帶轉檔單且以轉檔的節目 7084
	select p.FSPGMNAME, t.* from TBLOG_VIDEO t join TBPROG_M p on t.FSID = p.FSPROG_ID  
	where FSARC_TYPE = '001' and FCFILE_STATUS = 'T' and FSTYPE = 'G' and FSID not in (select FSID from TBLOG_VIDEO where FSARC_TYPE = '002')

	--只有SD 送帶轉檔單且已轉檔的雙語節目 1421
	select p.FSPROG_ID, p.FSPGMNAME,  t.FNEPISODE, (select FSPROGLANGNAME from TBZPROGLANG where p.FSPROGLANGID1 = FSPROGLANGID) 主聲道,
    (select FSPROGLANGNAME from TBZPROGLANG where p.FSPROGLANGID2 = FSPROGLANGID) 副聲道, t.FSBRO_ID, t.FSVIDEO_PROG
	from TBLOG_VIDEO t join TBPROG_M p on t.FSID = p.FSPROG_ID  
	where FSARC_TYPE = '001' and FCFILE_STATUS = 'T' and FSTYPE = 'G' and FSID not in (select FSID from TBLOG_VIDEO where FSARC_TYPE = '002') and (p.FSPROGLANGID1 <> p.FSPROGLANGID2)

end

if @case = 201606240405 begin --UNION, INTERSECT, EXCEPT
	select * from TBLOG_VIDEO
	select * into #tmpUNIONtest from TBLOG_VIDEO  where FSARC_TYPE = '001' UNION select * from TBLOG_VIDEO where FSARC_TYPE = '002' --82926
	select * from #tmpUNIONtest
	select FSVIDEO_PROG into #tmpUNIONtest2 from TBLOG_VIDEO where FSARC_TYPE = '001' UNION select FSVIDEO_PROG from TBLOG_VIDEO where FSARC_TYPE = '002' --79250 ???  因為有 FSVIDEO_PROG 重複，重複不顯示
	select FSVIDEO_PROG from #tmpUNIONtest EXCEPT select FSVIDEO_PROG from #tmpUNIONtest2
	select FSVIDEO_PROG from #tmpUNIONtest2 EXCEPT select FSVIDEO_PROG from #tmpUNIONtest
	select FSVIDEO_PROG from #tmpUNIONtest2 INTERSECT select FSVIDEO_PROG from #tmpUNIONtest

	select distinct FSVIDEO_PROG from #tmpUNIONtest
	select * from #tmpUNIONtest where FSVIDEO_PROG IS NULL

	--2852 筆多個轉檔單的 Video ID 相同
	select FSVIDEO_PROG, count(FSVIDEO_PROG) from #tmpUNIONtest group by FSVIDEO_PROG having count(FSVIDEO_PROG) > 1  
	select FSVIDEO_PROG, count(FSVIDEO_PROG) from #tmpUNIONtest group by FSVIDEO_PROG having count(FSVIDEO_PROG) > 10
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '000000WJ' --22筆 客家新樂園 FSID = 20120100187
	select * from TBPGM_PROMO where FSPROMO_ID = '20120100187'

	select 82926-79250  --3676

	--declare @tab TABLE(C INTEGER)
	select count(FSVIDEO_PROG) C INTO #tmpC from #tmpUNIONtest group by FSVIDEO_PROG having count(FSVIDEO_PROG) > 1
	select SUM(C) from #tmpC -- 6528

	select 6528 - 2852 --3676

	--UNION ALL
	select FSVIDEO_PROG into #tmpUNIONtest3 from TBLOG_VIDEO where FSARC_TYPE = '001' UNION ALL select FSVIDEO_PROG from TBLOG_VIDEO where FSARC_TYPE = '002' --82926

end

if @case = 201606240818 begin
	select * from TBLOG_VIDEO where FSID = '0000344' and FNEPISODE = 1
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002P32'
	select top 100 * from TBPGM_QUEUE
	select * from TBLOG_VIDEO where FSID = '2010615' and FNEPISODE = 1
end

if @case = 201606240954 begin  -- TSM 搬檔失敗，重啟 HD_FILE_MANAGER
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002BAX'
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002BAX' order by FDDATE
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002BAX'
end

if @case = 20166241111 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002OTY'
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002OTY'
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002OTY'
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002OSC' order by FDDATE
end

if @case = 201606241345 begin --2016-06-24 10:54:51.767 審核通過 14:00 檔案仍未開始搬，手動搬檔
	select * from TBLOG_VIDEO where FSVIDEO_PROG in ('00002A8X', '00002KAS')
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002A8X', '00002KAS')
end


if @case = 201606241550 begin
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002KTX'
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G201622700070001'
end

if @case = 201606242104 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002MN0', '00002MN2')  -- 002MN0 XDCAMViewer cannot play
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID in  ('00002MNO', '00002MN2')
	select * from TBLOG_VIDEO where FSVIDEO_PROG in ('00002MN0', '00002MN2')

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002P1T', '00002P1Q')
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID in  ('00002P1T', '00002P1Q','00002P1O')
end

if @case = 201606271054 begin --「電視音樂專輯」 第一集~第三集的送帶轉檔單退單
	select * from TBLOG_VIDEO where FSID = '2011092' and FNEPISODE = 1  --退單 FCFILE_STATUS = F -> 手動刪除 FCFILE_STATUS = X
	select * from TBBROADCAST where FSBRO_ID = '201201120067'
	--退單後仍無法新增
end


if @case = 201606271253 begin --PTS2 重新載入後 節目變成 SD
	select * from TBLOG_VIDEO where FSVIDEO_PROG in ('00002L02')
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID in ('00002L02') order by FDDATE
	select * from TBPGM_COMBINE_QUEUE where FDDATE = '2016-06-26' and FSCHANNEL_ID = '13' order by FNCONBINE_NO

	select t.FSARC_TYPE, q.* from TBPGM_COMBINE_QUEUE q join TBLOG_VIDEO t on q.FSVIDEO_ID = t.FSVIDEO_PROG  
	where q.FDDATE = '2016-06-26' and q.FSCHANNEL_ID = '13' and FSARC_TYPE = '001' order by q.FNCONBINE_NO

/****************************************************
	-- 6/28 PTS2 重新載入會 HD 變成 SD 
	-- 查詢 playlist 有哪些是 SD 節目
*****************************************************/
	select t.FSARC_TYPE, q.* from TBPGM_COMBINE_QUEUE q join TBLOG_VIDEO t on q.FSVIDEO_ID = t.FSVIDEO_PROG   
	where q.FDDATE = '2016-06-28' and q.FSCHANNEL_ID = '13' and FSARC_TYPE = '001' order by q.FNCONBINE_NO --重新載入並未發現有 HD 變成 SD
	
	select * from TBPGM_COMBINE_QUEUE --192580
	select * from TBPGM_ARR_PROMO --837288

	select top 5 * from TBPGM_COMBINE_QUEUE --192580
	select top 5 * from TBPGM_ARR_PROMO --837288

	select FNCONBINE_NO QUEUE_NO, FSPROG_ID FSID, FDDATE from TBPGM_COMBINE_QUEUE 
	UNION ALL (select FNARR_PROMO_NO QUEUE_NO, FSPROMO_ID FSID, FDDATE from TBPGM_ARR_PROMO) --1029868

	select 192580 + 837288 --1029868

	declare @date DATE = '2016-06-27'
	declare @channel varchar(10) = '13'

	-- 查詢主控運行表
	select FNCONBINE_NO QUEUE_NO, FSPROG_ID FSID, FSPROG_NAME FSNAME, FDDATE from TBPGM_COMBINE_QUEUE where FDDATE = @date and FSCHANNEL_ID = @channel
	UNION ALL (select FNARR_PROMO_NO QUEUE_NO, a.FSPROMO_ID FSID, p.FSPROMO_NAME FSNAME, FDDATE from TBPGM_ARR_PROMO a join TBPGM_PROMO p on a.FSPROMO_ID = p.FSPROMO_ID where FDDATE = @date and FSCHANNEL_ID = @channel) 
	order by QUEUE_NO

	declare @date DATE = '2016-06-27'
	declare @channel varchar(10) = '13'
	select QUEUE_NO, FSID, FSNAME, SPEC, FDDATE from 
		(select q.FNCONBINE_NO QUEUE_NO, q.FSPROG_ID FSID, q.FSPROG_NAME FSNAME, q.FDDATE, t.FSARC_TYPE SPEC 
			from TBPGM_COMBINE_QUEUE q left join TBLOG_VIDEO t on q.FSVIDEO_ID = t.FSVIDEO_PROG where FDDATE = @date and q.FSCHANNEL_ID = @channel
			UNION ALL (
				select a.FNARR_PROMO_NO QUEUE_NO, a.FSPROMO_ID FSID, p.FSPROMO_NAME FSNAME, a.FDDATE, t.FSARC_TYPE SPEC from TBPGM_ARR_PROMO a join TBPGM_PROMO p on a.FSPROMO_ID = p.FSPROMO_ID 
					left join TBLOG_VIDEO t on a.FSVIDEO_ID = t.FSVIDEO_PROG where a.FDDATE = @date and a.FSCHANNEL_ID = @channel)) u
	order by QUEUE_NO


end

if @case = 201606271426 begin -- 客家新聞雜誌(客家製播) 上傳完成但是無法 QC
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002NE1', '00002P28')  -- *.mxf.mxf
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID in ('00002NE1', '00002P28') order by FDDATE
end

if @case = 201606271514 begin
	--00002O4D, 00002OIB 2016-06-21 已經審核過，但是 6/27 排播後又帶出 00002O82 00002OIC 假的 ID
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002O82', '00002O4D', '00002OIC', '00002OIB')
	select * from TBLOG_VIDEO where FSVIDEO_PROG in ('00002O82', '00002O4D', '00002OIC', '00002OIB')
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID in ('00002O82', '00002O4D', '00002OIC', '00002OIB') and FSCHANNEL_ID = '07' order by FDDATE
end


------------------------------20160628----------------------------------------------------------------------------------------------------------

if @case = 201606280043 begin
	select * from TBLOG_VIDEO where CAST(FDUPDATED_DATE as DATE) = '2016-06-27'
end

if @case = 201606280945 begin
	declare @date DATE = '2016-06-24'
	declare @channel varchar(10) = '08'
	select QUEUE_NO, FSID, FSNAME, SPEC, FDDATE, VIDEO_ID, BRO_ID, STATUS from 
		(select q.FNCONBINE_NO QUEUE_NO, q.FSPROG_ID FSID, q.FSPROG_NAME FSNAME, q.FDDATE, t.FSARC_TYPE SPEC, q.FSVIDEO_ID VIDEO_ID, t.FSBRO_ID BRO_ID, h.FCSTATUS STATUS
			from TBPGM_COMBINE_QUEUE q left join TBLOG_VIDEO t on q.FSVIDEO_ID = t.FSVIDEO_PROG
				left join TBLOG_VIDEO_HD_MC h on q.FSVIDEO_ID = h.FSVIDEO_ID where FDDATE = @date and q.FSCHANNEL_ID = @channel 
			UNION ALL (
				select a.FNARR_PROMO_NO QUEUE_NO, a.FSPROMO_ID FSID, p.FSPROMO_NAME FSNAME, a.FDDATE, t.FSARC_TYPE SPEC, a.FSVIDEO_ID VIDEO_ID, t.FSBRO_ID BRO_ID, h.FCSTATUS STATUS from TBPGM_ARR_PROMO a join TBPGM_PROMO p on a.FSPROMO_ID = p.FSPROMO_ID 
					left join TBLOG_VIDEO t on a.FSVIDEO_ID = t.FSVIDEO_PROG
					left join TBLOG_VIDEO_HD_MC h on a.FSVIDEO_ID = h.FSVIDEO_ID where a.FDDATE = @date and a.FSCHANNEL_ID = @channel)) u
	order by QUEUE_NO
	select * from TBZCHANNEL
end

if @case = 201606281419 begin --死神少女 7 高解長度不對 46mn 48s
	select * from TBLOG_VIDEO where FSID = '2009639' and FNEPISODE = 7
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G200963900070003'
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G200963900070003'
end

-- Review -> Approved 搬檔錯誤
if @case = 201606281521 begin
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002NSO' -- Approved 檔案 XDCAM Viewer 無法開啟，Review 的 OK
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002NSO' -- 手動由 Review 搬檔到 Approved
	select * from TBUSERS where FSUSER_ID = '51241'
end

if @case = 201606281556 begin
	select * from TBPROG_M where FSPGMNAME like '%今晚你想點什麼%'
	select * from TBLOG_VIDEO where FSID = '2016227' and FNEPISODE = 7
end

if @case = 201606282214 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002NXX', '00002P3H', '00002O1V')
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID in ('00002NXX', '00002P3H', '00002O1V')
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID in ('00002NXX', '00002P3H', '00002O1V') order by FDDATE
	select * from TBZCHANNEL
end

if @case = 201606282300 begin  --刪除未建立完整的送帶轉檔單
	select * from TBPROG_M where FSPGMNAME like '%食在料理王%' --2014803
	select * from TBLOG_VIDEO where FSID = '2014803' and FNEPISODE = 108
	select * from TBBROADCAST where FSBRO_ID in ('201412300001', '201412090020') --no 201412300001
	exec SP_D_TBLOG_VIDEO_BYFILENO'G201480301080002'
	exec SP_D_TBLOG_VIDEO_BYBROID 'G201480301080002'

	select * from TBLOG_VIDEO where FSID = '2015809' and FNEPISODE = 108 --食在料理王(60分鐘版本) G201580901080002
	exec SP_D_TBLOG_VIDEO_BYBROID 'G201580901080002'
end


if @case = 201606290918 begin -- 查詢主控運行表

	select FNCONBINE_NO QUEUE, FSPROG_NAME NAME, FDDATE, FSCHANNEL_ID, FNEPISODE, FSPLAY_TIME from TBPGM_COMBINE_QUEUE
		UNION (select t.FNARR_PROMO_NO QUEUE, p.FSPROMO_NAME NAME, t.FDDATE, t.FSCHANNEL_ID, 0 FNEPISODE, t.FSPLAYTIME from TBPGM_ARR_PROMO t join TBPGM_PROMO p on t.FSPROMO_ID = p.FSPROMO_ID)

	declare @date DATE = '2016-06-29'
	declare @ch varchar(2) = '01'
	select FNCONBINE_NO QUEUE, FSPROG_NAME NAME, FDDATE, FSCHANNEL_ID, FNEPISODE, FSPLAY_TIME from TBPGM_COMBINE_QUEUE where FSCHANNEL_ID = @ch and FDDATE = @date
		UNION (select t.FNARR_PROMO_NO QUEUE, p.FSPROMO_NAME NAME, t.FDDATE, t.FSCHANNEL_ID, 0 FNEPISODE, t.FSPLAYTIME from TBPGM_ARR_PROMO t 
		join TBPGM_PROMO p on t.FSPROMO_ID = p.FSPROMO_ID where t.FSCHANNEL_ID = @ch and t.FDDATE = @date)
	order by QUEUE

	
	declare @current DATE = CAST(GETDATE() - 1 as DATE)
	select FNCONBINE_NO QUEUE, FSPROG_NAME NAME, FDDATE, FSCHANNEL_ID, FNEPISODE, FSPLAY_TIME from TBPGM_COMBINE_QUEUE where FDDATE >= @current
		UNION (select t.FNARR_PROMO_NO QUEUE, p.FSPROMO_NAME NAME, t.FDDATE, t.FSCHANNEL_ID, 0 FNEPISODE, t.FSPLAYTIME from TBPGM_ARR_PROMO t join TBPGM_PROMO p on t.FSPROMO_ID = p.FSPROMO_ID where FDDATE >= @current)
	order by FDDATE, FSCHANNEL_ID, QUEUE


	-------------------------
	declare @current DATE = CAST(GETDATE() - 1 as DATE)
	declare @VIDEO_ID TABLE(video_id varchar(10))
	insert into @VIDEO_ID VALUES
	 ('00002NZX'), ('00002NJI'), ('00002M42'), ('00002LUO'), ('00002NJF'), ('00002P8J'), ('00001LZW')
	--declare @VIDEO_ID varchar(100) = '(''00002NZX'', ''00002NJI'', ''00002M42'', ''00002LUO'', ''00002NJF'', ''00002P8J'')'

	select QUEUE, NAME, FNEPISODE, FDDATE, FSPLAY_TIME, FSCHANNEL_ID, FSVIDEO_ID from (
		select FNCONBINE_NO QUEUE, FSPROG_NAME NAME, FDDATE, FSCHANNEL_ID, FNEPISODE, FSPLAY_TIME, FSVIDEO_ID from TBPGM_COMBINE_QUEUE where FDDATE >= @current
			UNION (select t.FNARR_PROMO_NO QUEUE, p.FSPROMO_NAME NAME, t.FDDATE, t.FSCHANNEL_ID, 0 FNEPISODE, t.FSPLAYTIME, t.FSVIDEO_ID from TBPGM_ARR_PROMO t 
				join TBPGM_PROMO p on t.FSPROMO_ID = p.FSPROMO_ID where FDDATE >= @current)) u
	where FSVIDEO_ID in (select video_id from @VIDEO_ID)
	order by FDDATE, FSCHANNEL_ID, QUEUE

	------------------------------------------------------------------

	--CREATE VIEW VW_PLAYLIST_FROM_NOW as
	ALTER VIEW VW_PLAYLIST_FROM_NOW as
	select QUEUE, NAME, FNEPISODE, FDDATE, FSPLAY_TIME, FSCHANNEL_ID, FSVIDEO_ID from (
		select FNCONBINE_NO QUEUE, FSPROG_NAME NAME, FDDATE, FSCHANNEL_ID, FNEPISODE, FSPLAY_TIME, FSVIDEO_ID from TBPGM_COMBINE_QUEUE where FDDATE >= (GETDATE() - 2)
			UNION (select t.FNARR_PROMO_NO QUEUE, p.FSPROMO_NAME NAME, t.FDDATE, t.FSCHANNEL_ID, 0 FNEPISODE, t.FSPLAYTIME, t.FSVIDEO_ID from TBPGM_ARR_PROMO t 
				join TBPGM_PROMO p on t.FSPROMO_ID = p.FSPROMO_ID where FDDATE >= (GETDATE() - 2))) u
	--order by FDDATE, FSCHANNEL_ID, QUEUE

	select * from VW_PLAYLIST_FROM_NOW where FDDATE = '2016-06-29' and FSCHANNEL_ID = '01' order by FDDATE, FSCHANNEL_ID, QUEUE

	--------------------------------------------------------------------


	select top 100 * from TBPGM_ARR_PROMO

	--查詢未來已經開出的playlist
	select * from TBPGM_COMBINE_QUEUE where FDDATE > GETDATE() order by FDDATE, FSCHANNEL_ID, FNCONBINE_NO
	select FDDATE, FSCHANNEL_ID from TBPGM_COMBINE_QUEUE where FDDATE > GETDATE() group by FDDATE, FSCHANNEL_ID order by FDDATE, FSCHANNEL_ID
end

---------------------20160629---------------------------------------------------------------------------------------------------

if @case = 201606290824 begin --HDS 上傳失敗
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002PTD'  --E ->
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002PTD' -- FCSTATUS = 88, 2016-06-28 19:59:33.053
end



if @case = 201606291212 begin --修正查詢尚未標記上傳檔名  h.FSVIDEO_ID <> ''
	SELECT TOP 10000 FSBRO_ID, tmpName, FNEPISODE, SUBSTRING( FSVIDEO_ID, 3 , 6) as FSVIDEO_PROG
		from (select h.FSBRO_ID FSBRO_ID, h.FSVIDEO_ID FSVIDEO_ID , h.FDCREATED_DATE, h.FCSTATUS, t.FSID FSID, t.FNEPISODE , t.FSTYPE FSTYPE, t.FSARC_TYPE FSARC_TYPE, t.FCFILE_STATUS FCFILE_STATUS 
				from TBLOG_VIDEO_HD_MC h join TBLOG_VIDEO t on h.FSVIDEO_ID <> '' and (h.FSVIDEO_ID = t.FSVIDEO_PROG)) th
                join ( select FSPROG_ID tmpID, FSPGMNAME tmpName from TBPROG_M union (select FSPROMO_ID tmpID , FSPROMO_NAME tmpName from TBPGM_PROMO )) u
                on th.FSID = u.tmpID
                where th.FSTYPE in ('P', 'G')
					and th.FSARC_TYPE in ('002', '001')
                    and th.FCSTATUS = ''
                order by FDCREATED_DATE desc

	SELECT FSBRO_ID, tmpName, FNEPISODE, SUBSTRING( FSVIDEO_ID, 3 , 6) as FSVIDEO_PROG
		from (select h.FSBRO_ID FSBRO_ID, h.FSVIDEO_ID FSVIDEO_ID , h.FDCREATED_DATE, h.FCSTATUS, t.FSID FSID, t.FNEPISODE , t.FSTYPE FSTYPE, t.FSARC_TYPE FSARC_TYPE, t.FCFILE_STATUS FCFILE_STATUS 
				from TBLOG_VIDEO_HD_MC h join TBLOG_VIDEO t on h.FSVIDEO_ID <> '' and (h.FSVIDEO_ID = t.FSVIDEO_PROG)) th
                join ( select FSPROG_ID tmpID, FSPGMNAME tmpName from TBPROG_M union (select FSPROMO_ID tmpID , FSPROMO_NAME tmpName from TBPGM_PROMO )) u
                on th.FSID = u.tmpID
                where th.FSTYPE in ('P', 'G')
					and th.FSARC_TYPE in ('002', '001')
                    and th.FCSTATUS = ''
                order by FDCREATED_DATE desc

	--尚未標記的檔案
	select u.tmpName, u.tmpId, h.FSBRO_ID FSBRO_ID, h.FSVIDEO_ID FSVIDEO_ID, h.FDCREATED_DATE, h.FCSTATUS, t.FSID , t.FNEPISODE , t.FSTYPE FSTYPE, t.FSARC_TYPE FSARC_TYPE, t.FCFILE_STATUS FCFILE_STATUS 
		from TBLOG_VIDEO_HD_MC h join TBLOG_VIDEO t on h.FSVIDEO_ID <> '' and (h.FSVIDEO_ID = t.FSVIDEO_PROG)
        join (select FSPROG_ID tmpID, FSPGMNAME tmpName from TBPROG_M union (select FSPROMO_ID tmpID , FSPROMO_NAME tmpName from TBPGM_PROMO)) u on t.FSID = u.tmpID
		where t.FSTYPE in ('P', 'G')
		and t.FSARC_TYPE in ('001', '002')
		and h.FCSTATUS = ''
		order by FDCREATED_DATE desc

	--Wrong version
	SELECT  FSBRO_ID, tmpName, FNEPISODE, SUBSTRING(FSVIDEO_ID, 3, 6) as FSVIDEO_PROG 
		from (select h.FSBRO_ID, h.FSVIDEO_ID, h.FDCREATED_DATE, h.FCSTATUS, t.FSID, t.FNEPISODE, t.FSTYPE, t.FSARC_TYPE, t.FCFILE_STATUS from TBLOG_VIDEO_HD_MC h join TBLOG_VIDEO t on h.FSVIDEO_ID = t.FSVIDEO_PROG) th 
			join (select FSPROG_ID tmpID, FSPGMNAME tmpName from TBPROG_M union (select FSPROMO_ID tmpID, FSPROMO_NAME tmpName from TBPGM_PROMO)) u 
				on th.FSID = u.tmpID
			where FSTYPE in ('P', 'G')
			and FSARC_TYPE in ('002', '001')
			and FCSTATUS = ''
		order by FDCREATED_DATE desc
	-----------------------------------------------------

	select * from TBLOG_VIDEO_HD_MC h join TBLOG_VIDEO t on h.FSVIDEO_ID <> '' and (h.FSVIDEO_ID = t.FSVIDEO_PROG) --4376

	select distinct FSVIDEO_ID from TBLOG_VIDEO_HD_MC --4197
	select * from TBLOG_VIDEO_HD_MC --5593
	select FSVIDEO_ID, count(FSVIDEO_ID) from TBLOG_VIDEO_HD_MC group by FSVIDEO_ID having count(FSVIDEO_ID) > 1
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '' --1396 --資料帶
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID IS NULL --1396 --資料帶
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00001SKS' --2 筆送帶轉檔單

	select * from TBLOG_VIDEO where FSVIDEO_PROG = '' --1518
	select * from TBLOG_VIDEO where FSBRO_ID = '201405300025'

end

if @case = 201606291225 begin -- cannot play with XDCAM Viewer
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002OE2' order by FDDATE  --文人政事 2016-06-30
end

if @case = 201606291419 begin --註冊Anystream服務時發生錯誤, 重新調用
	select u.FSUSER_ChtName, b.* from TBBOOKING_MASTER b join TBUSERS u on b.FSUSER_ID = u.FSUSER_ID where CAST(FDBOOKING_DATE as DATE) = '2016-06-24' order by FDBOOKING_DATE
	select * from TBBOOKING_DETAIL where FSBOOKING_NO = '201606240051'
end

if @case = 201606291739 begin --查詢由片庫調檔案的節目
	select * from TBLOG_VIDEO_HD_MC where CAST(FDUPDATED_DATE as DATE) = '2016-06-29' and FSCREATED_BY = '程式' order by FDUPDATED_DATE
	select * from TBLOG_VIDEO_HD_MC where FCSTATUS = 'N' order by FDUPDATED_DATE --待審檔案

	--根據日期頻道查詢待審檔案
	select p.QUEUE, p.NAME, p.FNEPISODE, p.FDDATE, p.FSCHANNEL_ID, h.* from TBLOG_VIDEO_HD_MC h join VW_PLAYLIST_FROM_NOW p on h.FSVIDEO_ID <> '' and  (h.FSVIDEO_ID = p.FSVIDEO_ID)
	where h.FCSTATUS = 'N' and p.FDDATE = '2016-07-01' and p.FSCHANNEL_ID in ('11', '12', '13', '07') order by p.FSCHANNEL_ID, p.QUEUE

	select p.NAME, p.FNEPISODE, p.FSCHANNEL_ID from TBLOG_VIDEO_HD_MC h join VW_PLAYLIST_FROM_NOW p on h.FSVIDEO_ID <> '' and  (h.FSVIDEO_ID = p.FSVIDEO_ID)
	where h.FCSTATUS = 'N' and p.FDDATE = '2016-07-01' and p.FSCHANNEL_ID in ('11', '12', '13', '14', '07')  group by p.NAME, p.FNEPISODE, p.FSCHANNEL_ID order by p.FSCHANNEL_ID

	select p.NAME, p.FNEPISODE, p.FDDATE, p.FSCHANNEL_ID, p.QUEUE, h.* from TBLOG_VIDEO_HD_MC h join VW_PLAYLIST_FROM_NOW p on h.FSVIDEO_ID <> '' and (h.FSVIDEO_ID = p.FSVIDEO_ID)
	where h.FCSTATUS = 'N' and p.FDDATE = '2016-06-29' and p.FSCHANNEL_ID = '02' order by h.FDUPDATED_DATE --待審檔案

	select * from VW_PLAYLIST_FROM_NOW where FSCHANNEL_ID = '12' and FDDATE = '2016-07-01' order by QUEUE
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002NY9'
	select * from TBPGM_COMBINE_QUEUE where FSCHANNEL_ID = '14' order by FDDATE
end

---------------------20160630---------------------------------------------------------------------------------------------------


if @case = 201606301528 begin  --上傳失敗
	select * from TBLOG_VIDEO_HD_MC where FCSTATUS = 'E' and CAST(FDUPDATED_DATE as DATE) = '2016-06-29'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002pw5', '00002pw6', '00002pw7', '00002pw8', '00002pw9', '00002ppw')
	select * from TBPTSAP_STATUS_LOG where FCSTATUS <> '80' and CAST(FDUPDATE_DATE as DATE) >= '2016-06-29'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002ppw'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002ppw'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002q2q'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002q2q'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002krv', '00002q25', '00002pqx')
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID in ('00002krv', '00002q25', '00002pqx') --00002Q25 0630 已標記上傳但是 0701 早上仍未搬檔
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002Q25' order by FDDATE
	
end

if @case = 201607022210 begin
	select * from TBLOG_VIDEO_HD_MC where CAST(FDUPDATED_DATE as DATE) >= '2016-07-01'  and FCSTATUS = 'E' order by FDCREATED_DATE desc
	select * from TBPTSAP_STATUS_LOG where CAST(FDUPDATE_DATE as DATE) >= '2016-07-01' and FCSTATUS = '88' order by FDUPDATE_DATE desc

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002q5l' -- 51063 remark
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002Q5L' order by FDDATE desc --2016-07-03 play

	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002PB6' --2016-06-27 12:47:38.553 upload to HDUpload, but this still not move to Review
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002PB6' order by FDDATE desc --created in playlist on 2016-07-02 19:44:02.127
end

if @case = 201607041416 begin
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002PU1' order by FDDATE
end

if @case = 201607041602 begin --水果 置換問題
	select * from TBPROG_M where FSPGMNAME like '%水果%'
	select * from TBPGM_COMBINE_QUEUE where FSPROG_ID = '0000060' and FNEPISODE = 1932 order by FDDATE
	select * from TBLOG_VIDEO where FSID = '0000060' and FNEPISODE = 1932 and FSARC_TYPE = '002' order by FDUPDATED_DATE

	select * from TBLOG_VIDEO where FSID = '0000060' and FNEPISODE in (1932, 1933, 1942, 1943) and FCFILE_STATUS not in ('X', 'D') and FSARC_TYPE = '002' order by FNEPISODE, FDUPDATED_DATE
	select * from TBLOG_VIDEO where FSID = '0000060' and FNEPISODE in (1932, 1933, 1942, 1943) and FCFILE_STATUS in ('X', 'D') and FSARC_TYPE = '002' order by FNEPISODE, FDUPDATED_DATE
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002MC7', '00002QBJ', '00002QBD', '00002QBE')
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002M87'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002MC7'
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002MC7'
	select * from TBZCHANNEL
end

if @case = 201607041651 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002PQ6', '00002PQ7') 
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID in ('00002PQ6', '00002PQ7') -- 有兩筆上傳紀錄, why?
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002PQ2', '00002PQ3', '00002PQ4', '00002PQ5', '00002PQ6', '00002PQ7')
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID in ('00002PQ2', '00002PQ3', '00002PQ4', '00002PQ5', '00002PQ6', '00002PQ7')
end

if @case = 201607042255 begin --檢查 HD_FILE_MANAGER 上傳順序
	select * from TBLOG_VIDEO_HD_MC where FDUPDATED_DATE > '2016-07-04 21:00' and FCSTATUS = 'O' order by FDUPDATED_DATE
	select * from TBLOG_VIDEO_HD_MC where FDUPDATED_DATE > '2016-07-04 21:00' and FCSTATUS = 'O' order by FDCREATED_DATE
	select * from TBLOG_VIDEO_HD_MC where FDUPDATED_DATE > '2016-07-04 21:00' and FCSTATUS = 'N' order by FDCREATED_DATE
	select * from TBLOG_VIDEO_HD_MC where FDUPDATED_DATE > '2016-07-04 20:00' and FCSTATUS = 'C' order by FDCREATED_DATE
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002QJX', '00002QJU', '00002QJR') order by FDUPDATED_DATE
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002PK2', '00001QZW', '00002PO1', '00002O0O', '00002NRB') order by FDUPDATED_DATE

	-- Review -> Approved
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002PK2', '00001QZW', '00002PO1', '00002O0O', '00002Q45', '00002QOK', '00002Q4K', '00002Q31', '00002Q62',
	'00002Q44', '00002OGD', '00002QRW', '00002PB7', '00002NRB', '00002MS6', '00002GLO', '00001XGC', '00001Z26', '00001RUG', '00002IEW', '000010N5', '00002L05', '0000294M', '00002Q60', '00002PAV') 
	order by FDUPDATED_DATE

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002MN2', '00002OY5', '00002PWB', '00002MS6')
	-- G201623500010002 T  FDRAN_DATE 2016-06-24 10:47
	-- G201702600010003 T  FDTRAN_DATE 2016-06-25 21:42
	-- P201606003790001 T
	-- P201606012230002 R FDTRAN_DATE 2016-06-29 16:12
	select * from TBLOG_VIDEO where FSVIDEO_PROG in ('00002MN2', '00002OY5', '00002PWB', '00002MS6')
	select * from TBBROADCAST where FSBRO_ID in ('201606230094', '201606080138', '201606290126') --2016-06-24 10:47 , 2016-06-25 21:42

	select * from TBUSERS where FSUSER_ID = '51216'
end

----------------------------------------20160705---------------------------------------------------------------------------------------

if @case = 201607051416 begin -- 轉低解失敗 TC 錯誤
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002QN3' --P201607001190002, FCSTATUS = R, 201607040212
	select * from TBBROADCAST where FSBRO_ID = '201607040212'
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002QN3' order by FDDATE --2016-07-06 PTS1
end

if @case = 201607051456 begin --TC in = 00:00:00:00
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002PWB'
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002PWB' order by FDDATE --7/5 22:00
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002PWB' --20160700119
	select * from TBPGM_PROMO where FSPROMO_ID = '20160700119' --PTS1_ID_0703
end


if @case = 201607051646 begin -- 查 PTS1 7/5 播出 轉檔失敗 R
	--00002QN3 已更新
	--00002PWB 將更新
	select p.FSVIDEO_ID, t.FCFILE_STATUS from TBPGM_ARR_PROMO p join TBLOG_VIDEO t on p.FSVIDEO_ID = t.FSVIDEO_PROG
	where p.FDDATE between '2016-07-06' and '2016-07-09' and t.FCFILE_STATUS = 'R' order by p.FSPLAYTIME
	select * from TBLOG_VIDEO where FSFILE_NO = 'P201607001190002'  --00002QN3

	--00002OS2 
	--00002PAF TSM 處裡中
	select p.FDDATE, p.FNCONBINE_NO, p.FSCHANNEL_ID , p.FSVIDEO_ID, p.FSPLAY_TIME, t.FCFILE_STATUS, t.FSBRO_ID from TBPGM_COMBINE_QUEUE p join TBLOG_VIDEO t on p.FSVIDEO_ID = t.FSVIDEO_PROG
	where p.FDDATE between '2016-07-06' and '2016-07-09' and t.FCFILE_STATUS = 'R'

	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002OS2'  --FCFILE_STATUS = R 201606220152
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002OS2'
	select * from TBBROADCAST where FSBRO_ID = '201606220152'

	select * from TBBROADCAST where FSBRO_ID = '201606240229'

end

if @case = 201607051714 begin --copy file from Review to Approved manually 手動搬檔
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002QWC'
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002QWC' -- FCFILE_STATUS = T
	select * from TBPGM_PROMO where FSPROMO_ID = '20160600885' --無線頻道收視戶重新掃瞄30秒短片HD
	select * from TBBROADCAST where FSBRO_ID = '201607050149' --FDTRAN_DATE 2016-07-05 17:41
end

-------------------------------------20160706----------------------------------------------------------------------

if @case = 201607061006 begin -- 標記上傳失敗
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID in ('00002p4h','00002pms','00002pmr', '00002pmq', '00002pmp', '00002pmo', '00002pmn', '00002qsa')
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002qsa'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002QUC' --客台 施懿倫 2022
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002PQK'

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002qsa'

	-- *.mxf.mxf issue
	select u.FSUSER_ChtName, t.* from TBLOG_VIDEO t join TBUSERS u on t.FSUPDATED_BY = u.FSUSER_ID   where FSVIDEO_PROG = '00002QUC'
	select * from TBBROADCAST where FSBRO_ID = '201607050022'
	select * from TBUSERS where FSUSER_ID = '50848' --施懿倫 update
end

if @case = 201607061549 begin --流言追追追 105
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002MLZ' -- SD
	select * from TBLOG_VIDEO where FSID = '2006786' and FNEPISODE = 105 --G200678601050002 FCFILE_STATUs = X  送帶轉檔抽單 201505070130
	select * from TBBROADCAST where FSBRO_ID = '201505070130' --2016-07-07 播出
end

if @case = 201607061615 begin --無法置換
	select * from TBPROG_M where FSPGMNAME = '村民大會'
	select * from TBLOG_VIDEO where FSID = '2006265' and FNEPISODE = 488 --00002QX0 FSEND_TIMECODE = 01:01:00;02
end

--  QC 無審核選項
if @case = 201607061713 begin
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002QX0' --2016-07-05 18:20:06.357 create
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002QX0' -- 2016-07-06 17:34:32.287
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002QX0' --FDUR, 00:01:00;00, FDCREATED_DATE = 2016-07-06 16:57:38.410
end

-- 手動搬檔 HDUpload -> Review
if @case = 201607061758 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002R25'  --2016-07-06 14:43:31.710 上傳完成
	--17:15 手動搬檔
	--檢查 002R25 是否有在 Review create *.ok
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002R25' --2016-07-07 播出

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002NBC', '00002R25', '00002R2Y', '00002PVX')
	select * from TBLOG_VIDEO where FSVIDEO_PROG in ('00002NBC', '00002R25', '00002R2Y', '00002PVX')
end

if @case = 201607062336 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002PQP'
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002PQP'
end

if @case = 201607062345 begin
	--已標記，未到QC，故無法審核:FCSTATUS = C
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in
	('00002QVQ',
	'00002QVO',
	'00002QT0',
	'00002QSG',
	'00002MNY')

	--已審核，但未到APPROVED:
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002QUY' 排隊中
end

if @case = 201607070513 begin --標記上傳失敗
	select * from TBLOG_VIDEO_HD_MC where CAST(FDUPDATED_DATE as DATE) >= '2016-07-06'  and FCSTATUS = 'E' order by FDUPDATED_DATE desc

	select UPPER(SUBSTRING(l.FSVIDEOID, 3, 6)), l.FSVIDEOID, l.*, t.* from TBPTSAP_STATUS_LOG l left join TBLOG_VIDEO_HD_MC t on l.FSVIDEOID = t.FSVIDEO_ID 
	where CAST(l.FDUPDATE_DATE as DATE) >= '2016-07-06' and l.FCSTATUS = '88' order by l.FDUPDATE_DATE desc

	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002R2A'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002R2A'
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002R2A'

	select * from TBPGM_COMBINE_QUEUE
	select * from TBPGM_ARR_PROMO
end

--todo
if @case = 201607071212 begin --Anystream 無法轉檔，由主控轉檔再入庫
	select * from TBPROG_M where FSPGMNAME = '流言追追追'
	select * from TBLOG_VIDEO where FSID = '2006786' and FNEPISODE = 105 and FSARC_TYPE = '002'
	select * from TBBROADCAST where FSBRO_ID = '201607060111'
end

if @case = 201607071429 begin -- playlist 短帶名稱與 LOUTH 系統顯示名稱不相符
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002MLY'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002MLY' --2016-06-13 16:48:14.397 審核通過
	select * from TBPGM_PROMO where FSPROMO_ID = '20160600331'  --2016-06-18 19:09:44.723 改成微電影聊聊好朋友
	select * from TBPGM_PROMO where FSPROMO_NAME like '%微電影%'
	select * from TBLOG_VIDEO where FSVIDEO_PROG in ('00002MM0', '00002MM1', '00002MM2') --20160600328, 20160600329, 20160600330
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002MM0', '00002MM1', '00002MM2') --2016-06-13 17:02:35.003, 2016-06-13 16:49:24.597, 2016-06-13 16:47:03.057 審核通過
	select * from TBPGM_PROMO where FSPROMO_ID in ('20160600328', '20160600329', '20160600330') --2016-06-18 19:11:04.700, 2016-06-18 19:10:28.957, 2016-06-18 19:10:04.227
	select * from TBUSERS where FSUSER_ID in ('50868', '51063')
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID in  ('00002MM0', '00002MM1', '00002MM2') order by FDDATE
end


--  HD(片庫轉檔或檔案送播）已播出審核過，同時也有SD 檔案的節目, 增加檔案大小欄位
if @case = 201607071632 begin
	select th.FSPROG_ID, th.FSPROG_NAME, th.FNEPISODE, t.FSBRO_ID, t.FSVIDEO_PROG, t.FSARC_TYPE, SUBSTRING(t.FSFILE_SIZE, 0, LEN(t.FSFILE_SIZE)-1) 檔案大小, t.FSFILE_SIZE from TBLOG_VIDEO t
		join
			(select q.FSPROG_ID, q.FSPROG_NAME, q.FNEPISODE, t.* from TBLOG_VIDEO_HD_MC t 
				join 
					(select FSPROG_NAME, FSPROG_ID, FNEPISODE, FSVIDEO_ID from TBPGM_COMBINE_QUEUE where FDDATE >= '2016-3-29'
						group by FSPROG_NAME, FSPROG_ID, FNEPISODE, FSVIDEO_ID) q 
				on t.FSVIDEO_ID = q.FSVIDEO_ID
				where t.FSVIDEO_ID not in (select FSVIDEO_PROG from TBLOG_VIDEO where FSARC_TYPE = '001') and t.FCSTATUS = 'O') th
		on t.FSID = th.FSPROG_ID and t.FNEPISODE = th.FNEPISODE and t.FSARC_TYPE = '001' and t.FCFILE_STATUS = 'T'
		order by th.FSPROG_ID, th.FNEPISODE


	select * from TBLOG_VIDEO_HD_MC where FCSTATUS = 'O' order by FDUPDATED_DATE --3178
	select distinct FSVIDEO_ID from TBLOG_VIDEO_HD_MC where FCSTATUS = 'O' --3178

	select * from VW_PLAYLIST_FROM_NOW where FSCHANNEL_ID = '07' and FDDATE = '2016-07-05'

end

if @case = 201607072021 begin
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00001KLY'
end

if @case = 201607082120 begin --files are not in Approved folder
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002OC3', '00002R7J', '00002QVB') --00002OC3 N
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID in ('00002OC3', '00002R7J', '00002QVB') order by FDDATE
end

if @case = 201607082200 begin --CHECK for UPLOAD FAIL
	select FSVIDEOID, count(FSVIDEOID) RESEND from TBPTSAP_STATUS_LOG where CAST(FDUPDATE_DATE as DATE) > '2016-07-06' group by FSVIDEOID having COUNT(FSVIDEOID) > 1
end

-----------------------------------------------20160711----------------------------------------------

if @case = 201607110952 begin --00002rbl, 00002r94 mark upload fail
	select * from TBLOG_VIDEO_HD_MC where CAST(FDUPDATED_DATE as DATE) >= '2016-07-08'  and FCSTATUS = 'E' order by FDUPDATED_DATE desc

	select p.FDDATE,p.NAME, s.*, h.* from TBPTSAP_STATUS_LOG s left join TBLOG_VIDEO_HD_MC h on s.FSVIDEOID = h.FSVIDEO_ID left join VW_PLAYLIST_FROM_NOW p on s.FSVIDEOID = p.FSVIDEO_ID
	where CAST(s.FDUPDATE_DATE as DATE) >= '2016-07-08' and s.FCSTATUS = '88' order by s.FDUPDATE_DATE desc

	select * from TBPTSAP_STATUS_LOG where FSVIDEOID in ('00002rbl', '00002r94') --客家新聞雜誌第497集預告, 客粽飄香慶端午宣傳帶
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002rbl', '00002r94')

	select * from VW_PLAYLIST_FROM_NOW order by FDDATE, FSCHANNEL_ID, QUEUE
end


/******************
 * 建議刪除清單
 ******************/
if @case = 201607111056 begin
	select * from TBLOG_VIDEO_HD_MC where FCSTATUS = 'O' and FDUPDATED_DATE >= '2016-03-29' order by FDUPDATED_DATE --3418

	select * from TBLOG_VIDEO_HD_MC h left join VW_PLAYLIST_FROM_NOW vp on h.FSVIDEO_ID = vp.FSVIDEO_ID where h.FCSTATUS = 'O' and h.FDUPDATED_DATE >= '2016-03-29' and vp.FSVIDEO_ID IS NULL --2584
	select * from TBLOG_VIDEO_HD_MC h left join VW_PLAYLIST_FROM_NOW vp on h.FSVIDEO_ID = vp.FSVIDEO_ID where h.FCSTATUS = 'O' and h.FDUPDATED_DATE >= '2016-03-29' and vp.FSVIDEO_ID IS NOT NULL --7545


	--查詢刪除清單，列出所有審核通過但是未來未排播的檔案
	select DISTINCT FSVIDEO_ID from
		(select h.FSVIDEO_ID FSVIDEO_ID from TBLOG_VIDEO_HD_MC h left join VW_PLAYLIST_FROM_NOW vp on h.FSVIDEO_ID = vp.FSVIDEO_ID 
			where h.FCSTATUS = 'O' and h.FDUPDATED_DATE >= '2016-03-29' and vp.FSVIDEO_ID IS NULL) PURGE --2584

	select DISTINCT FSVIDEO_ID from
		(select h.FSVIDEO_ID FSVIDEO_ID from TBLOG_VIDEO_HD_MC h left join VW_PLAYLIST_FROM_NOW vp on h.FSVIDEO_ID = vp.FSVIDEO_ID 
			where h.FCSTATUS = 'O' and h.FDUPDATED_DATE >= '2016-03-29' and vp.FSVIDEO_ID IS NOT NULL) KEEP --839

	--查詢刪除清單，列出所有審核通過但是未來未排播的檔案
	select DISTINCT FSVIDEO_ID, NAME, EPISODE from
		(select h.FSVIDEO_ID FSVIDEO_ID, pp.NAME, t.FNEPISODE EPISODE, t.FSBRO_ID FSBRO_ID, t.FSARC_TYPE FSARC_TYPE from TBLOG_VIDEO_HD_MC h left join VW_PLAYLIST_FROM_NOW vp on h.FSVIDEO_ID = vp.FSVIDEO_ID join TBLOG_VIDEO t on h.FSVIDEO_ID = t.FSVIDEO_PROG
		join (select m.FSPROG_ID ID, m.FSPGMNAME NAME from TBPROG_M m union all select p.FSPROMO_ID ID, p.FSPROMO_NAME from TBPGM_PROMO p) PP on t.FSID = PP.ID
			where h.FCSTATUS = 'O' and h.FDUPDATED_DATE >= '2016-03-29' and vp.FSVIDEO_ID IS NULL) PURGE --2584

	select DISTINCT FSVIDEO_ID, NAME, EPISODE, FSBRO_ID from
		(select h.FSVIDEO_ID FSVIDEO_ID, pp.NAME, t.FNEPISODE EPISODE, t.FSBRO_ID FSBRO_ID, t.FSARC_TYPE FSARC_TYPE from TBLOG_VIDEO_HD_MC h left join VW_PLAYLIST_FROM_NOW vp on h.FSVIDEO_ID = vp.FSVIDEO_ID join TBLOG_VIDEO t on h.FSVIDEO_ID = t.FSVIDEO_PROG
		join (select m.FSPROG_ID ID, m.FSPGMNAME NAME from TBPROG_M m union all select p.FSPROMO_ID ID, p.FSPROMO_NAME from TBPGM_PROMO p) PP on t.FSID = PP.ID
			where h.FCSTATUS = 'O' and h.FDUPDATED_DATE >= '2016-03-29' and vp.FSVIDEO_ID IS NULL) PURGE --2739

	select DISTINCT FSVIDEO_ID, NAME, EPISODE, FSBRO_ID, FSARC_TYPE from
		(select h.FSVIDEO_ID FSVIDEO_ID, pp.NAME, t.FNEPISODE EPISODE, t.FSBRO_ID FSBRO_ID, t.FSARC_TYPE FSARC_TYPE from TBLOG_VIDEO_HD_MC h left join VW_PLAYLIST_FROM_NOW vp on h.FSVIDEO_ID = vp.FSVIDEO_ID join TBLOG_VIDEO t on h.FSVIDEO_ID = t.FSVIDEO_PROG
		join (select m.FSPROG_ID ID, m.FSPGMNAME NAME from TBPROG_M m union all select p.FSPROMO_ID ID, p.FSPROMO_NAME from TBPGM_PROMO p) PP on t.FSID = PP.ID
			where h.FCSTATUS = 'O' and h.FDUPDATED_DATE >= '2016-03-29' and vp.FSVIDEO_ID IS NULL) PURGE --2739

------------------------------EXPORT HISTORY ------------------------------------------------------------------------------------------------------------------------
	select SUBSTRING(FSVIDEO_ID, 3, 6), NAME, EPISODE, FSBRO_ID, FSARC_TYPE from
		(select h.FSVIDEO_ID FSVIDEO_ID, pp.NAME, t.FNEPISODE EPISODE, t.FSBRO_ID FSBRO_ID, t.FSARC_TYPE FSARC_TYPE from TBLOG_VIDEO_HD_MC h left join VW_PLAYLIST_FROM_NOW vp on h.FSVIDEO_ID = vp.FSVIDEO_ID join TBLOG_VIDEO t on h.FSVIDEO_ID = t.FSVIDEO_PROG and t.FCFILE_STATUS = 'T'
		join (select m.FSPROG_ID ID, m.FSPGMNAME NAME from TBPROG_M m union all select p.FSPROMO_ID ID, p.FSPROMO_NAME from TBPGM_PROMO p) PP on t.FSID = PP.ID
			where h.FCSTATUS = 'O' and h.FDUPDATED_DATE >= '2016-06-18' and vp.FSVIDEO_ID IS NULL) PURGE --2478 長短帶

	select SUBSTRING(FSVIDEO_ID, 3, 6), NAME, EPISODE, FSBRO_ID, FSARC_TYPE from
		(select h.FSVIDEO_ID FSVIDEO_ID, pp.NAME, t.FNEPISODE EPISODE, t.FSBRO_ID FSBRO_ID, t.FSARC_TYPE FSARC_TYPE from TBLOG_VIDEO_HD_MC h left join VW_PLAYLIST_FROM_NOW vp on h.FSVIDEO_ID = vp.FSVIDEO_ID join TBLOG_VIDEO t on h.FSVIDEO_ID = t.FSVIDEO_PROG and t.FCFILE_STATUS = 'T'
		join (select m.FSPROG_ID ID, m.FSPGMNAME NAME from TBPROG_M m union all select p.FSPROMO_ID ID, p.FSPROMO_NAME from TBPGM_PROMO p) PP on t.FSID = PP.ID
			where h.FCSTATUS = 'O' and h.FDUPDATED_DATE >= '2016-07-11' and vp.FSVIDEO_ID IS NULL) PURGE --319 長短帶

--------------------------------------------------------------------------------------------------------------------------------------------------------

	select FSVIDEO_ID NAME, EPISODE, FSBRO_ID, count(FSVIDEO_ID) from
		(select h.FSVIDEO_ID FSVIDEO_ID, pp.NAME, t.FNEPISODE EPISODE, t.FSBRO_ID FSBRO_ID, t.FSARC_TYPE FSARC_TYPE from TBLOG_VIDEO_HD_MC h left join VW_PLAYLIST_FROM_NOW vp on h.FSVIDEO_ID = vp.FSVIDEO_ID join TBLOG_VIDEO t on h.FSVIDEO_ID = t.FSVIDEO_PROG
		join (select m.FSPROG_ID ID, m.FSPGMNAME NAME from TBPROG_M m union all select p.FSPROMO_ID ID, p.FSPROMO_NAME from TBPGM_PROMO p) PP on t.FSID = PP.ID
			where h.FCSTATUS = 'O' and h.FDUPDATED_DATE >= '2016-03-29' and vp.FSVIDEO_ID IS NULL) PURGE group by FSVIDEO_ID, NAME, EPISODE, FSBRO_ID having COUNT(FSVIDEO_ID) > 1 --我的這一班 438

	select * from TBLOG_VIDEO where FSID = '0000216' and FNEPISODE = 438

	--查詢所有節目短帶名稱
	select * from (select m.FSPROG_ID ID, m.FSPGMNAME NAME from TBPROG_M m union select p.FSPROMO_ID ID, p.FSPROMO_NAME from TBPGM_PROMO p) PROGRAM --52449
	select * from (select m.FSPROG_ID ID, m.FSPGMNAME NAME from TBPROG_M m union all select p.FSPROMO_ID ID, p.FSPROMO_NAME from TBPGM_PROMO p) PROGRAM order by ID --52449

	select * from TBPROG_M order by FSPROG_ID --14424
	select * from TBPGM_PROMO order by FSPROMO_ID --38025
	select 38025 + 14424 --52449
end

if @case = 201607111145 begin
	select p.FDDATE,p.NAME, s.*, h.* from TBPTSAP_STATUS_LOG s left join TBLOG_VIDEO_HD_MC h on s.FSVIDEOID = h.FSVIDEO_ID left join VW_PLAYLIST_FROM_NOW p on s.FSVIDEOID = p.FSVIDEO_ID
	where CAST(s.FDUPDATE_DATE as DATE) >= '2016-07-11' and (s.FCSTATUS = '88' or h.FCSTATUS = 'E') order by s.FDUPDATE_DATE desc --201607110035

	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002RHF'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002RHF'
end


if @case = 201607111822 begin --公視藝文大道轉低解失敗
	select * from TBLOG_VIDEO where FSID = '2012078' and FNEPISODE = 193
end

if @case = 201607111424 begin --RM第三十一首簽 轉低解失敗 檔案 SOM 少了 2 格
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002R0I' --201607060054, P201607003640001
	select * from TBBROADCAST where FSBRO_ID = '201607060054' --FDTRAN_DATE 2016-07-07 19:05:47.523
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002R0I'
	-- 刪除檔案，重填送播單
end

if @case = 201607111457 begin -- 手動搬檔, 7/13 play
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002RGA'
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002RGA'
end

if @case = 201607111521 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002RGK'
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002RGK' --2016公益-家庭凝聚篇
end

if @case = 201607111538 begin -- 要建送帶轉檔單才對
	select * from TBPROG_M where FSPGMNAME like '%有話好說%' --2016503
	select * from TBLOG_VIDEO where FSID = '2016503' and FNEPISODE = 105 --201607060149, G201650301050001
	select * from TBBROADCAST where FSBRO_ID = '201607060149'
end

if @case = 201607111655 begin
	select * from TBUSERS where FSUSER_ID = 'prg1123'
end


------------------------------------------20160712--------------------------------------------------------
if @case = 201607112116 begin
	/*
	G20160711.log GPFS -> Approved
	2016/7/11 下午 09:15:42 : 開始複製檔案:\\mamstream\MAMDFS\ptstv\HVideo\2016\20160500503\P201605005030001.mxf:到待播區
	2016/7/11 下午 09:15:59 : 檔案:\\mamstream\MAMDFS\ptstv\HVideo\2016\20160500503\P201605005030001.mxf:拷貝到待播區完成
	*/
		select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002KEI' --P201605005030001
		select * from TBLOG_VIDEO where FSFILE_NO = 'P201508000710003' --00002JU0

	/*
	20160711.log  HDUpload ->Review
	14:49:40 mark upload
	21:01:57 : 開始搬移檔案:\\10.13.200.6\HDUpload\002RIO.mxf:到待審區
	2016/7/11 下午 09:29:46 : 檔案:\\10.13.200.6\HDUpload\002RIO.mxf:上傳完成
	2016/7/11 下午 09:29:46 : 開始將G201704400010002:的TBLOG_VIDEO_HD_MC的狀態修改成N
	Why it takes so long to begin move file to Review?
	*/
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002RIO' 
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002RIO' --22G, G201704400010002

	/*
	P20160711.log  Review -> Approved
	2016/7/11 下午 09:33:29 : 開始搬移檔案:\\10.13.200.6\Review\002HQS.mxf:到待審區
	Louth20160711.log
	2016/7/11 下午 09:39:49 : 開始寫入檔案[00002HQS ]的段落
	2016/7/11 下午 09:39:49 : 寫入檔案[00002HQS ]的段落完畢
	*/
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002HQS' --10G
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002QX9'

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002RL8', '00002RO5')
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID in ('00002RL8', '00002RO5') order by FDDATE
end

if @case = 201607121000 begin -- modify FSTRACK 林秀珍 request
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201018200010018' --201607050063
	exec SP_U_TBLOG_VIDEO_FSTRACK 'G201018200010018','MMM-----', '51204' --use sa, 檢索的內容要等索引更新才會更新
end

if @case = 201607121138 begin
	select * from TBZCHANNEL

	UPDATE TBZCHANNEL SET FSSORT='01' WHERE FSCHANNEL_ID= '12' --PTS1
	UPDATE TBZCHANNEL SET FSSORT='02' WHERE FSCHANNEL_ID= '13' --PTS2
	UPDATE TBZCHANNEL SET FSSORT='03' WHERE FSCHANNEL_ID= '14' --PTS3
	UPDATE TBZCHANNEL SET FSSORT='04' WHERE FSCHANNEL_ID= '07'
	UPDATE TBZCHANNEL SET FSSORT='05' WHERE FSCHANNEL_ID= '62' --PTS1-MOD
	UPDATE TBZCHANNEL SET FSSORT='06' WHERE FSCHANNEL_ID= '64' --PTS3-MOD
	UPDATE TBZCHANNEL SET FSSORT='07' WHERE FSCHANNEL_ID= '08'
	UPDATE TBZCHANNEL SET FSSORT='08' WHERE FSCHANNEL_ID= '09'

	UPDATE TBZCHANNEL SET FSSORT='51' WHERE FSCHANNEL_ID= '01'
	UPDATE TBZCHANNEL SET FSSORT='52' WHERE FSCHANNEL_ID= '02'
	UPDATE TBZCHANNEL SET FSSORT='61' WHERE FSCHANNEL_ID= '03'
	UPDATE TBZCHANNEL SET FSSORT='62' WHERE FSCHANNEL_ID= '04'
	UPDATE TBZCHANNEL SET FSSORT='63' WHERE FSCHANNEL_ID= '05'
	UPDATE TBZCHANNEL SET FSSORT='64' WHERE FSCHANNEL_ID= '06'

	UPDATE TBZCHANNEL SET FSSORT='65' WHERE FSCHANNEL_ID= '10'
	UPDATE TBZCHANNEL SET FSSORT='53' WHERE FSCHANNEL_ID= '11'

	UPDATE TBZCHANNEL SET FSSORT='52' WHERE FSCHANNEL_ID= '51'
	UPDATE TBZCHANNEL SET FSSORT='54' WHERE FSCHANNEL_ID= '61'
end


if @case = 201607121502 begin -- 節目表維護可以匯入 MOD 頻道
	select * from TBZCHANNEL
	UPDATE TBZCHANNEL SET FSOLDTABLENAME ='PSHOW_PTS_MOD' WHERE FSCHANNEL_ID= '62'
	UPDATE TBZCHANNEL SET FSOLDTABLENAME ='PSHOW_HD_MOD' WHERE FSCHANNEL_ID= '64'
end

if @case = 201607121513 begin --手動標記上傳
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002QXH'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002QXH'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002qzk'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002qzk'

	select * from TBPTSAP_STATUS_LOG where FSVIDEOID in ('00002MNY', '00002R9F')
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002MNY', '00002R9F')
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID in ('00002MNY', '00002R9F')
end


-------------------------------------201607131010----------------------------------------------
if @case = 201607130007 begin
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002R2H'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002R2H'  --FCSTATUS = N
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002R2H'
end

if @case = 201607130508 begin
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID in ('00002RP0', '00002RP3', '00002RP1', '00002RP5', '00002RP4', '00002RP6', '00002ROP', '00002ROQ', '00002ROU', '00002ROV')
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002RP0', '00002RP3', '00002RP1', '00002RP5', '00002RP4', '00002RP6', '00002ROP', '00002ROQ', '00002ROU', '00002ROV')
end


if @case = 201607131014-201607061549 begin --流言追追追 105 轉低解後仍檢索不到
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002MLZ' -- SD
	select * from TBLOG_VIDEO where FSID = '2006786' and FNEPISODE = 105 --201607120009, G200678601050017, FCLOW_RES = R, FCKEYFRAME = N, FILE_SIZE = '', FCFILE_STATUS = S
	select * from TBTRACKINGLOG where FDTIME between '2016-07-12 16:15' and '2016-07-12 17:10' and FSMESSAGE like '%137425%' order by FDTIME --2016-07-12 16:23:34.600 啟動低解轉檔，2016-07-12 16:59:01.657 轉檔完成
	select * from TBTRACKINGLOG where FDTIME between '2016-07-12 16:15' and '2016-07-12 19:10' and (FSMESSAGE like '%137425%' or FSMESSAGE like '%G200678601050017%') order by FDTIME 
	select * from TBTRACKINGLOG where FDTIME between '2016-07-12 16:15' and '2016-07-12 17:10' order by FDTIME
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002RL8' order by FDDATE

	select * from TBLOG_VIDEO where FSID = '2015992' and FNEPISODE = 9 -- FCLOW_RES = Y FCKEYFRAME = Y  -- 2015 變臉台灣 轉低解有成功 G201599200090003, 00002RO5
	select * from TBTRACKINGLOG where FDTIME between '2016-07-12 15:00' and '2016-07-12 17:10' and FSMESSAGE like '%137405%' order by FDTIME --2016-07-12 15:13:09.457 start transcoding, 2016-07-12 16:19:50.320 finish
	select * from TBTRACKINGLOG where FDTIME between '2016-07-12 15:00' and '2016-07-12 19:10' and (FSMESSAGE like '%137405%' or FSMESSAGE like '%G201599200090003%') order by FDTIME
	select * from TBTRACKINGLOG where FDTIME between '2016-07-12 15:00' and '2016-07-12 17:10' order by FDTIME
end

if @case = 201607131711 begin --TSM 狀態異常
	select * from TBTRACKINGLOG where FDTIME between '2016-07-13 16:40' and '2016-07-14 17:50' order by FDTIME
	select * from TBTRACKINGLOG where FDTIME between '2016-07-13 16:40' and '2016-07-14 17:50' and FSCATALOG in ('ERROR', 'WARNING') order by FDTIME
	select * from TBTRACKINGLOG where FDTIME between '2016-07-13 17:30' and '2016-07-14 18:50' and FSCATALOG in ('ERROR', 'WARNING') order by FDTIME
end

if @case = 201607131718 begin --手動搬檔到 Approved
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002RRU'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002RRU'
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002RRU' order by FDDATE --2016-07-15
end

if @case = 201607141756 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002RUC', '00002RRA', '00002RT1')
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID in ('00002RUC', '00002RRA', '00002RT1') order by FDDATE
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID in ('00002RT1') order by FSPLAYTIME -- FCSTATUS = O，插入短帶：檔案已在待審區，QC 軟體：審核通過
end

if @case = 201607151147 begin --標記上傳失敗
    select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002S1H'
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002S1H' order by FDDATE
	select * from TBPGM_PROMO where FSPROMO_ID = '20160700710'
end

if @case = 201607151340 begin
   select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002RPU'
   select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002MDK'
   select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002RPU' order by FDDATE, FSPLAY_TIME
end

-------------------------------20160714------------------------------------------------

if @case = 201607141016 begin --調用失敗，手動調用
	select * from TBUSERS where FSUSER_ChtName = '左珮華' --09478
	select * from TBLOG_VIDEO where FSID = '2016224' and FNEPISODE = 1 --\\mamstream\MAMDFS\ptstv\HVideo\2016\2016224\G201622400010002.mxf
	select * from TBBOOKING_MASTER where FSUSER_ID = '09478' --201607130115
	select * from TBBOOKING_DETAIL where FSBOOKING_NO = '201607130115' --G201622400020001 FCFILE_TRANSCODE_STATUS = F
end

if @case = 201607141208 begin
	select CONVERT(char(8), GETDATE(), 112) --20160714
end

--------------------------------------------------------------------------------------------------

if @case = 201607152249 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002RQ4'
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002RQ4'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002RYJ','00002S1K', '00002S1T', '00002RK9', '00002S1', '00002RIN', '00002OGN', '00002QVS')
end

if @case = 201607181042 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002P4H', '00002RWL', '00002RWM')
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in ('00002P4H', '00002RWL', '00002RWM')
end

if @case = 201607181102 begin --4gtv todo:要補要補 mp4 給公行
	select * from TBLOG_VIDEO where FSFILE_NO = 'G200576800160001'
end

if @case = 201607191359 begin --台灣親nagˋnagˋ 抽單 todo 
	select * from TBLOG_VIDEO where FSID = '2010742' and FSARC_TYPE = '002' --10, 11
	select * from TBPROG_M where FSPROG_ID = '2010742'
end

if @case = 201607200852 begin
	select * from TBPROG_M where FSPGMNAME like '%水果%' --0000060
	select * from TBLOG_VIDEO where FSID = '0000060' and FNEPISODE = 1946 --201607060123, FCFILE_STATUS = T
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002R30' --O
end

if @case = 201607201211 begin -- Approved 檔案有問題
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '000029ZK'
end

if @case = 201607201505 begin
	select * from TBLOG_VIDEO where FSID = '2015229'
	select * from TBPROG_M where FSPROG_ID = '2015229' --201607200083, FCFILE_STATUS = S
	select * from TBBROADCAST where FSBRO_ID = '201607200083' --2016-07-20 13:45:49.120
end

if @case = 201607201647-201607111822 begin --公視藝文大道轉低解失敗
	select * from TBLOG_VIDEO where FSID = '2012078' and FNEPISODE = 193 --201606240229, B
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002PAF' -- N
	select * from TBBROADCAST where FSBRO_ID = '201606240229' --FDTRAN_DATE 2016-06-29 12:01:11.607
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002PAF' --
end

if @case = 201607210540 begin --計算一週的檔案容量
	select FSVIDEO_ID, count(FSVIDEO_ID) repeat from TBPGM_COMBINE_QUEUE where FDDATE between '2016-07-06' and '2016-07-12' group by FSVIDEO_ID order by repeat desc --524

	select pgmq.FSVIDEO_ID, count(FSVIDEO_ID), t.FSFILE_SIZE repeat from TBPGM_COMBINE_QUEUE pgmq join TBLOG_VIDEO t on pgmq.FSVIDEO_ID = t.FSVIDEO_PROG
	where FDDATE between '2016-07-06' and '2016-07-12' group by pgmq.FSVIDEO_ID, t.FSFILE_SIZE --493

	select FSFILE_SIZE, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) -2) from TBLOG_VIDEO where FSFILE_SIZE <> ''
	select FSFILE_SIZE, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) -2) from TBLOG_VIDEO where FSFILE_SIZE <> '' and FSFILE_SIZE  not like '%MB%'

	select FSFILE_SIZE, CAST(SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) -2) as FLOAT) from TBLOG_VIDEO where FSFILE_SIZE <> '' and FSFILE_SIZE  not like '%MB%' 
		and FSVIDEO_PROG in (select FSVIDEO_ID from TBPGM_COMBINE_QUEUE where FDDATE between '2016-07-06' and '2016-07-12' group by FSVIDEO_ID) --438

	select SUM(CAST(SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) -2) as FLOAT)) from TBLOG_VIDEO where FSFILE_SIZE <> '' and FSFILE_SIZE  not like '%MB%' 
		and FSVIDEO_PROG in (select FSVIDEO_ID from TBPGM_COMBINE_QUEUE where FDDATE between '2016-07-06' and '2016-07-12' group by FSVIDEO_ID) --7047.2 GB

	select FSFILE_SIZE, CAST(SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) -2) as FLOAT) from TBLOG_VIDEO where FSFILE_SIZE <> '' and FSFILE_SIZE  not like '%MB%' 
		and FSVIDEO_PROG in (select FSVIDEO_ID from TBPGM_COMBINE_QUEUE where FDDATE between '2016-07-13' and '2016-07-19' group by FSVIDEO_ID) --438

	select SUM(CAST(SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) -2) as FLOAT)) from TBLOG_VIDEO where FSFILE_SIZE <> '' and FSFILE_SIZE  not like '%MB%' 
		and FSVIDEO_PROG in (select FSVIDEO_ID from TBPGM_COMBINE_QUEUE where FDDATE between '2016-07-13' and '2016-07-19' group by FSVIDEO_ID) --6812 GB

	select CAST('2.9' as FLOAT)
	select FSFILE_SIZE, LEN(FSFILE_SIZE) from TBLOG_VIDEO
	select FSFILE_SIZE from TBLOG_VIDEO
end

if @case = 201607211158 begin --鬧熱打擂台(120分鐘版) 未抓到 SD ID
	select * from TBLOG_VIDEO where FSID = '2016527' and FNEPISODE = 17 --00002S2N
	select  t.FSBRO_ID, t.FSFILE_NO, t.FSARC_TYPE, pgmq.* from TBPGM_COMBINE_QUEUE pgmq left join TBLOG_VIDEO t on pgmq.FSVIDEO_ID = t.FSVIDEO_PROG  where pgmq.FSPROG_ID = '2016527' and pgmq.FNEPISODE = 17 --00002TTR
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002S2N'
end

if @case = 201607211702 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002SOM'
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002SOM'
end

if @case = 201607211711-201607201647-201607111822 begin --公視藝文大道轉低解失敗
	select * from TBLOG_VIDEO where FSID = '2012078' and FNEPISODE = 193 --201606240229, B
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002PAF' -- N
	select * from TBBROADCAST where FSBRO_ID = '201606240229' --FDTRAN_DATE 2016-06-29 12:01:11.607
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002PAF' --

	--update TBLOG_VIDEO_HD_MC set FCSTATUS = 'C' where FSVIDEO_ID = '00002PAF' --伺服器主體 "hsing1" 在目前的安全性內容下無法存取資料庫 "MAM_LOG"。

end

if @case = 201607220910 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in (
'0000ZSLQ',  -- no data
'00002SFP',  --o  20160719-183838.xml, pgmok
'00002SAU',  --o  20160720-193807.xml, pgmok
'00002SCQ',  --o  20160720-173052.xml, pgmok
'00002RT8',  --o  20160720-175042.xml, pgmok
'00002T7N',  --o  20160720-172343.xml, pgmok
'00002P4H',  --o  no xml
'00002SAV',  --o  20160720-193907.xml, pgmok
'00002RUW',  --o  20160719-202036.xml, pgmok
'00002SAW',  --o  20160720-194834.xml, pgmok
'00002SGH',  --o  20160720-140245.xml, pgmok
'00002SAX',  --o  20160720-194214.xml, pgmok
'00002SAY',  --o  20160720-194317.xml, pgmok
'00002SND',  --o  20160721-144229.xml, pgmok
'00002S10',  --o  20160720-125111.xml, pgmok
'00002SR0',  --no data
'00002SUH',  --o  20160720-190512.xml. pgmok
'00002RV6',  --o  20160720-194521.xml, pgmok
'00002RU9',  --o  20160719-202036.xml, pgmok
'00002SI1',  --o  20160720-190112.xml, pgmok
'00002RV7',  --o  20160720-194729.xml, pgmok
'000023LE',  --o  20160719-190342.xml, pgmok
'00002SB1')  --o  20160720-194624.xml, pgmok

	select * from TBLOG_VIDEO where FSVIDEO_PROG in ('0000ZSLQ', '00002RV7', '00002SR0') --00002RV7 201607130170
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002RV7' --2016-07-22 23:29:50:28
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('0000ZSLQ', '00002RV7', '00002SR0')

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in (
	'00002SVR', --o 20160720-132349.xml, pgmok
	'00002RMR', --o 20160712-211317.xml, pgmok, dbok
	'00002S3T', --o 20160719-183941.xml, pgmok
	'00002LBR', --o 20160621-202332.xml, pgmok, dbok,  20160711-111144.xml, pgmok, dberr
	'00002TIG', --no data
	'00002ONF', --o 20160703-184746.xml, 20160703-184746.xml, 20160711-111247.xml, 20160711-111247.xml, pgmok, dbok
	'00002TIU', --no data
	'00002QQ1', --o 20160719-225512.xml, pgmok
	'00002SLL', --o 20160720-132031.xml, pgmok
	'00002VBC') --no data

	select * from TBLOG_VIDEO where FSVIDEO_PROG in ('00002TIG', '00002TIU', '00002VBC')
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID in ('00002TIG', '00002TIU', '00002VBC')
end


if @case = 201607221148-201607121138 begin --修改頻道順序
	select * from TBZCHANNEL where FBISENABLE = 1

	UPDATE TBZCHANNEL SET FSSORT='01' WHERE FSCHANNEL_ID= '01'
	UPDATE TBZCHANNEL SET FSSORT='02' WHERE FSCHANNEL_ID= '12'
	UPDATE TBZCHANNEL SET FSSORT='03' WHERE FSCHANNEL_ID= '02'
	UPDATE TBZCHANNEL SET FSSORT='04' WHERE FSCHANNEL_ID= '13'
	UPDATE TBZCHANNEL SET FSSORT='05' WHERE FSCHANNEL_ID= '11'
	UPDATE TBZCHANNEL SET FSSORT='06' WHERE FSCHANNEL_ID= '14'
	UPDATE TBZCHANNEL SET FSSORT='07' WHERE FSCHANNEL_ID= '07'

	UPDATE TBZCHANNEL SET FSSORT='08' WHERE FSCHANNEL_ID= '62'
	UPDATE TBZCHANNEL SET FSSORT='10' WHERE FSCHANNEL_ID= '64'

	UPDATE TBZCHANNEL SET FSSORT='11' WHERE FSCHANNEL_ID= '08'
	UPDATE TBZCHANNEL SET FSSORT='20' WHERE FSCHANNEL_ID= '09'

	update TBZCHANNEL set FSCHANNEL_NAME = '公視主頻' where FSCHANNEL_ID = '01'
	update TBZCHANNEL set FSCHANNEL_NAME = '公視2台' where FSCHANNEL_ID = '02'
	update TBZCHANNEL set FSCHANNEL_NAME = '公視3台' where FSCHANNEL_ID = '11'

	update TBZCHANNEL set FSCHANNEL_NAME = 'PTS1(短帶託播用)' where FSCHANNEL_ID = '12'
	update TBZCHANNEL set FSCHANNEL_NAME = 'PTS2(短帶託播用)' where FSCHANNEL_ID = '13'
	update TBZCHANNEL set FSCHANNEL_NAME = 'PTS3(短帶託播用)' where FSCHANNEL_ID = '14'
	update TBZCHANNEL set FSCHANNEL_NAME = 'PTS1-MOD(短帶託播用)' where FSCHANNEL_ID = '62'
	update TBZCHANNEL set FSCHANNEL_NAME = 'PTS3-MOD(短帶託播用)' where FSCHANNEL_ID = '64'
end

/*
 * troubleshooting
 */
if @case = 201607221536 begin -- 檢視轉檔作業
	select * from TBLOG_VIDEO where FSID = '2014841' and FNEPISODE = 46 --G201484100460001
	select * from TBPROG_M where FSPGMNAME like '%行走TIT%' --2014841
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201484100460001' --00002TW5
	select * from TBBROADCAST where FSBRO_ID = '201607210168'
	--update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201484100460001' -- sa

	select * from TBTRAN_LOG where FDDATE between '2016/07/22 09:30' and '2016/07/22 14:50' and 
	(FSPARAMETER like '%G201484100460001%' or FSPARAMETER like '%138563%') order by FDDATE

	-- SP_U_TBTRANSCODE_JOBID

	select * from TBTRANSCODE where FNTRANSCODE_ID = 138563  --TSM 處裡中
	select * from TBTRANSCODE where FNTRANSCODE_ID = 138561  --ANS 處裡錯誤
	select * from TBTRANSCODE where FNTRANSCODE_ID = 138651
	select * from TBTRANSCODE where FNTRANSCODE_ID in (136128, 138653)

	--update TBTRANSCODE set FNPROCESS_STATUS = 8 where FNTRANSCODE_ID = 138563

	--update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201484100460001'
	--update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201484100460001'

	select * from TBTRANSCODE where FNTRANSCODE_ID = 138652 --FNPROCESS_STATUS 3 ANS 處裡中, 4 轉檔完成

	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002TW5' order by FDDATE
	select * from TBTSM_JOB where FNTSMJOB_ID = -1

	select * from TBLOG_VIDEO where FSFILE_NO = 'G000006019020002'
	select distinct(FCFILE_STATUS) from TBLOG_VIDEO

	select * from TBTRANSCODE where FDSTART_TIME > '2016-07-22 18:00' order by FDSTART_TIME --FNPROCESS_STATUS : 2 TSM 處裡中
end


if @case = 201607221839 begin -- 調整搬檔時間
	--SP_Q_GET_MOVE_PENDING_LIST_MIX
end

if @case = 201607240905 begin  --*.mxf.mxf issue
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002TT4')
end

if @case = 201607242333 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002q9Z'
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002q9z'  --not scheduled play
end

if @case = 201607251138 begin -- 無法抽單，應該是登入帳號不對
	select * from TBLOG_VIDEO where FSBRO_ID = '201607050037' -- X 抽單
end


if @case = 201607251656-201607051416 begin --Timecode error : Bad input parameter specified issued by Anystream
	select * from TBLOG_VIDEO where FSVIDEO_PROG in ('00002QN3', '00002QJD') --00002QN3 R, 00002QJD D 刪除
	select * from TBBROADCAST where FSBRO_ID = '201607040212'
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID  = '00002QN3' order by FDDATE
end


if @case = 201607252300 begin
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID in ('00002SKN', '00002TTH')
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002SKN', '00002TTH')
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002RSH'
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002RSH'
end


if @case = 201607261051 begin  -- email
	select * from TBSYSTEM_CONFIG
	select * from TBSYSTEM_NOTIFY
	SELECT FSSYSTEM_CONFIG FROM TBSYSTEM_CONFIG
end

if @case = 201607261124 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002HQH', '00002GBU')
	select * from TBLOG_VIDEO_HD_MC where FSUPDATED_BY = '程式' and FCSTATUS = ''  -- 35 沒有狀態
	select * from TBLOG_VIDEO_HD_MC where FSUPDATED_BY <> '' and FCSTATUS = ''
end

if @case = 201607261458 begin
	select FSSYSTEM_CONFIG from TBSYSTEM_CONFIG
end


if @case = 201607261553-201607211158 begin --鬧熱打擂台(120分鐘版) 未抓到 SD ID
	select * from TBLOG_VIDEO where FSID = '2016527' and FNEPISODE = 17 --00002S2N
	select  t.FSBRO_ID, t.FSFILE_NO, t.FSARC_TYPE, pgmq.* from TBPGM_COMBINE_QUEUE pgmq left join TBLOG_VIDEO t on pgmq.FSVIDEO_ID = t.FSVIDEO_PROG  where pgmq.FSPROG_ID = '2016527' and pgmq.FNEPISODE = 17 --00002TTR
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002S2N'
end


----------------------------------20160727---------------------------------
if @case = 201607270525 begin
	select * from TBUSERS where FSUSER_ID = 'PTSMAM20155566'  --4gTV
end

if @case = 201607270847 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in (
	'00002F4M', '00002RN7', '00002QWT', '00002RJZ', '00002PZP', '00002QV2', '00002RN8', '00002RK1', '00002R8X', '00001ZA0')
	select * from TBUSERS where FSUSER_ID = '51256'
end

if @case = 201607271043 begin -- 送播單狀態是 T_
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002U6L' --201607250022, FCSTATUS = NULL  -> O
	select * from TBUSERS where FSUSER_ID = '70062' --盧厚樺
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002U6L' --FNARR_PROMO_NO = 75, FNSEQNO = 46
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002U6L'
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002U6L' --FDFILE_STATUS = T
	select * from TBBROADCAST where FSBRO_ID = '201607250022' --FDTRAN_DATE = 2016-07-26 17:30:02.587, FSUPDATED_BY : 標記上傳者
	select * from TBPGM_PROMO where FSPROMO_ID = '20160701019' --2016 07/29 pts1 menu 08:30
	select h.*, u.FSUSER_ChtName from TBLOG_VIDEO_HD_MC h join TBUSERS u on h.FSUPDATED_BY = u.FSUSER_ID where FCSTATUS = 'O' and CAST(h.FDUPDATED_DATE as DATE) = '2016-07-20' 
end

if @case = 201607271149 begin --動手做好簡單 #21
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002NR8'
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002NR8' --2016-07-07, 2016-08-18 play
	select pn.FSPROMO_NAME, p.* from TBPGM_ARR_PROMO p join TBPGM_PROMO pn on p.FSPROMO_ID = pn.FSPROMO_ID  where FSCHANNEL_ID = '07' and FDDATE = '2016-08-18' order by FNARR_PROMO_NO
	select * from VW_PLAYLIST_FROM_NOW where FSCHANNEL_ID = '07' and FDDATE = '2016-08-18' order by QUEUE
end

if @case = 201607271417 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002F4M', '00002HRB')
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in ('00002F4M', '00002HRB')  --8/6 在 PTS3-MOD 播出
end

if @case = 201607271511 begin --Anystream 轉檔完成， MAM 轉檔失敗（搬檔失敗）
	select * from TBPROG_M where FSPGMNAME like '%重返犯罪現場%' --2017100 重返犯罪現場第13季
	select * from TBLOG_VIDEO where FSID = '2017100' and FNEPISODE = 21 -- FCFILE_STATUS = R 00002U6O \\mamstream\MAMDFS\Media\Lv\ptstv\2017\2017100\G201710000210003.mp4  -> T
	select * from TBBROADCAST where FSBRO_ID = '201607250025' --FDTRAN_DATE = 2016-07-26 15:15:41.330
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002U6O'
	
	--檔案已經在 18T ，將 TBTRANSCODE.FNPROCESS_STATUS = 8
	select * from TBTRANSCODE where FNTRANSCODE_ID = 138905 --FNPRROCESS_STATUS = 4
end

/****************
	重新載入送單長度會更新到短帶資料長度 
*****************/
if @case = 201607271540 begin --長度或莫名其妙被改
	select * from TBPGM_PROMO where FSPROMO_ID = '20160701109' --PTS1主題之夜promo 面對死亡 FSDURATION = 00:01:00;00 (60s) updated by 51204  --> 09:00:59:28
	select * from TBLOG_VIDEO where FSID = '20160701109' --FSEND_TIMECODE = 10:00:59;28 送播單長度錯誤
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002UCL' --no data
	select * from TBPGM_ARR_PROMO where FSPROMO_ID = '20160701109'
end

/******************************
	modify  TBSYSTEMCONFIG
*******************************/
if @case = 201607271724 begin
	select FSSYSTEM_CONFIG into #tmp_TBSYSTEM_CONFIG from TBSYSTEM_CONFIG
	select * from #tmp_TBSYSTEM_CONFIG
	update #tmp_TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<BroadcastEmail Desc=""送播單Email"">oappts@mail.pts.org.tw</BroadcastEmail>', 
	'<BroadcastEmail Desc=""送播單Email"">oappts@mail.pts.org.tw; hsing1@mail.pts.org.tw</BroadcastEmail>')

	update #tmp_TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, 'oappts@mail.pts.org.tw', 
	'oappts@mail.pts.org.tw; hsing1@mail.pts.org.tw')

	update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, 'oappts@mail.pts.org.tw', 
	'oappts@mail.pts.org.tw; hsing1@mail.pts.org.tw')

	update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, 'oappts@mail.pts.org.tw; hsing1@mail.pts.org.tw', 
	'oappts@mail.pts.org.tw')

	select * from TBSYSTEM_CONFIG

	if @subcase = 201611081427 begin --modify SQLLAB CompletePath
		update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '\\10.13.200.6\Approved\', '\\10.13.200.6\test\Approved\')
	end
end


--------------------------------------20160729--------------------------------
if @case = 201607290855 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002UN0', '00002UGE', '00002UGF', '00002UGG', '00002UGH')
end

if @case = 201607291142 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('0000204C', '00002PMD', '00002ROX')
end

if @case = 201607291558 begin
	select * from TBPROG_PRODUCER  --製作人
	select * from TBPROGMEN --節目＼相關人員
end

------------------------------------20160801--------------------------------
if @case = 201608011015 begin
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002Q2Z'
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002Q2Z' order by FDDATE
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002QII' order by FDDATE
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002OBA' order by FDDATE --飛越客庄 0801 播出
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002P19' order by FDDATE --唱客小黃 0815 播出 0801 已經有檔案
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002P19' -- B 待轉
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002P19' --N
end


if @case = 201608011619 begin --TS3310 退帶問題
	select * from TBTRACKINGLOG where FDTIME between '2016-08-01 16:20' and '2016-08-01 16:30' order by FDTIME --使用者51204要從TS3310退出磁帶：A00832L4,A00855L.4A00911L4,A00914L4
	select * from TBTRACKINGLOG where FDTIME between '2016-08-01 16:20' and '2016-08-01 16:40' and FSPROGRAM like '%fnUpdateTapeList%'
	select * from TBTRACKINGLOG where FDTIME between '2016-07-20' and '2016-08-02' and FSMESSAGE like '%要從TS3310退出磁帶%' order by FDTIME
	select * from TBTRACKINGLOG where FDTIME between '2016-07-28 08:50' and  '2016-07-28 09:30' order by FDTIME
	--只要執行重新整理就會呼叫 WSTSM/fnUpdateTapeList_TS3310
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-08-01' and FSPROGRAM like '%fnUpdateTapeList_TS3310%' order by FDTIME
end

if @case = 201608011633 begin --有檔案 Louth DB 無資料
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '000021VD' order by FDDATE --浩克慢遊 #11 20160724-212256.xml.dberr
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '000002ZC' --搖滾保母 #1
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '000008E5'
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '000008E5' order by FDDATE
end

---------------------------------20160802--------------------------------------------

if @case = 201608020915 begin  --查詢今天以前播出過的 playlist
	declare @dateBEG DATE = '2016-07-01', @dateEND DATE = '2016-07-31'
	declare @channel varchar(10) = '07'
	select QUEUE_NO, FSID, FSNAME, SPEC, FDDATE, VIDEO_ID, BRO_ID, STATUS, FILE_NO from
		(select q.FNCONBINE_NO QUEUE_NO, q.FSPROG_ID FSID, q.FSPROG_NAME FSNAME, q.FDDATE, t.FSARC_TYPE SPEC, q.FSVIDEO_ID VIDEO_ID, t.FSBRO_ID BRO_ID, h.FCSTATUS STATUS, t.FSFILE_NO FILE_NO
			from TBPGM_COMBINE_QUEUE q left join TBLOG_VIDEO t on q.FSVIDEO_ID = t.FSVIDEO_PROG
				left join TBLOG_VIDEO_HD_MC h on q.FSVIDEO_ID = h.FSVIDEO_ID where FDDATE between @dateBEG and @dateEND and q.FSCHANNEL_ID = @channel 
			UNION ALL (
				select a.FNARR_PROMO_NO QUEUE_NO, a.FSPROMO_ID FSID, p.FSPROMO_NAME FSNAME, a.FDDATE, t.FSARC_TYPE SPEC, a.FSVIDEO_ID VIDEO_ID, t.FSBRO_ID BRO_ID, h.FCSTATUS STATUS, t.FSFILE_NO FILE_NO from TBPGM_ARR_PROMO a join TBPGM_PROMO p on a.FSPROMO_ID = p.FSPROMO_ID 
					left join TBLOG_VIDEO t on a.FSVIDEO_ID = t.FSVIDEO_PROG
					left join TBLOG_VIDEO_HD_MC h on a.FSVIDEO_ID = h.FSVIDEO_ID where a.FDDATE between @dateBEG and @dateEND and a.FSCHANNEL_ID = @channel)) u
	order by FDDATE, QUEUE_NO

	select * from TBPGM_COMBINE_QUEUE where CAST(FDDATE as DATE) = '2016-08-02'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000010N5'
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '000010N5'

	--查詢 7/6 至今所有頻道的 playlist （節目）含分段
	select FDDATE, FNCONBINE_NO,FSCHANNEL_ID, FSPROG_NAME, FSVIDEO_ID from TBPGM_COMBINE_QUEUE where FDDATE between '2016-07-06' and GETDATE() order by FDDATE, FSCHANNEL_ID, FNCONBINE_NO

	--查詢播出過的節目的Video ID
	select q.FSVIDEO_ID, q.FSPROG_NAME, q.FNEPISODE from TBPGM_COMBINE_QUEUE q where q.FDDATE between '2016-07-06' and GETDATE() group by q.FSVIDEO_ID, q.FSPROG_NAME, q.FNEPISODE --1856

	select * from (
		select q.FSVIDEO_ID, q.FSPROG_NAME, q.FNEPISODE from TBPGM_COMBINE_QUEUE q where q.FDDATE between '2016-07-06' and GETDATE() group by q.FSVIDEO_ID, q.FSPROG_NAME, q.FNEPISODE)	q

	----------------
	--查詢播出過的節目的轉檔狀態， 同一支 Video ID 有可能有多個 FCFILE_STATUS
	select q.FSVIDEO_ID, q.FSPROG_NAME, q.FNEPISODE, t.FCFILE_STATUS from TBPGM_COMBINE_QUEUE q left join TBLOG_VIDEO t on q.FSVIDEO_ID = t.FSVIDEO_PROG 
	where q.FDDATE between '2016-07-06' and GETDATE() group by q.FSVIDEO_ID, q.FSPROG_NAME, t.FCFILE_STATUS, q.FNEPISODE order by q.FSPROG_NAME, q.FNEPISODE, t.FCFILE_STATUS --1956

	select q.FSVIDEO_ID, q.FSPROG_NAME, q.FNEPISODE, t.FCFILE_STATUS from TBPGM_COMBINE_QUEUE q left join TBLOG_VIDEO t on q.FSVIDEO_ID = t.FSVIDEO_PROG 
	where q.FDDATE between '2016-07-06' and GETDATE() group by q.FSVIDEO_ID, q.FSPROG_NAME order by q.FSPROG_NAME, q.FNEPISODE --1956

	--error
	select FSVIDEO_ID, FSPROG_NAME, (select FCFILE_STATUS from TBLOG_VIDEO where FSVIDEO_PROG = FSVIDEO_ID) STATUS from TBPGM_COMBINE_QUEUE  
	where FDDATE between '2016-07-06' and GETDATE() group by FSVIDEO_ID, FSPROG_NAME
end

if @case = 201608020957 begin -- haka 上傳失敗
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002RXX', '00002RXS', '00002RYG') --00002RXX, 00002RYG
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID in ('00002RXX', '00002RXS', '00002RYG') --00002rxs
	select * from TBUSERS where FSUSER_ID in ('08343', '50818')
	select * from TBBROADCAST where FSBRO_ID = '201607140040'
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002RYG' order by FDDATE
	--上傳失敗紀錄
	select FSVIDEOID, count(FSVIDEOID) as NUM from TBPTSAP_STATUS_LOG group by FSVIDEOID having count(FSVIDEOID) > 1
end

if @case = 201608021612 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002S3P' --FCSTATUS = O 2016-07-17 09:21 審核通過, copy from GPFS 待審區到待播區
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002S3P' --FCFILE_STATUS = T
	select * from TBBROADCAST where FSBRO_ID = '201607150115' --FDTRAN_DATE = 2016-07-17 09:21
end

if @case = 201608021645-201608011619 begin --TS3310 退帶問題
	select * from TBTRACKINGLOG where FDTIME between '2016-07-29 15:20' and '2016-07-29 16:30' order by FDTIME --使用者51204要從TS3310退出磁帶：A00832L4,A00855L.4A00911L4,A00914L4
	select * from TBTRACKINGLOG where FDTIME between '2016-07-29 12:20' and '2016-07-29 18:40' and FSPROGRAM like '%fnUpdateTapeList%'
	select * from TBTRACKINGLOG where FDTIME between '2016-07-29 17:20' and '2016-07-29 18:40' and FSMESSAGE like '%A0143%'
	select * from TBTRACKINGLOG where FDTIME between '2016-07-29 14:20' and '2016-07-29 18:40' and FSPROGRAM like '%WSTSM%' order by FDTIME
	select * from TBUSERS where FSUSER_ID in ('08255', '51031')

	select * from TBTRACKINGLOG where FDTIME between '2016-07-20' and '2016-08-02' and FSMESSAGE like '%要從TS3310退出磁帶%' order by FDTIME
	select * from TBTRACKINGLOG where FDTIME between '2016-07-28 08:50' and  '2016-07-28 09:30' order by FDTIME
	--只要執行重新整理就會呼叫 WSTSM/fnUpdateTapeList_TS3310
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-08-01' and FSPROGRAM like '%fnUpdateTapeList_TS3310%' order by FDTIME
end

if @case = 201608021716 begin -- haka 上傳失敗
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID in ('00002RX5', '00002RY9')  --手動搬檔
end

if @case = 201608021746 begin
	select * from TBLOG_VIDEO where FSBRO_ID = '201608020075' --G201720500010001
	select * from TBLOG_VIDEO where FSBRO_ID = '201608020130'
	exec SP_U_TBLOG_VIDEO_FSTRACK 'G201720500010001','MMMM----', '51204' --FSTRACK is nvarchar(4)

	UPDATE TBLOG_VIDEO SET FSTRACK = 'MMMM----' , FSUPDATED_BY='51204' WHERE  FSFILE_NO='G201720500010001'
	UPDATE TBLOG_VIDEO_D set FSTITLE=FSTITLE WHERE  FSFILE_NO='G201720500010001'
end

if @case = 201608021822 begin
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002V04' order by FDDATE --調整播出時間看看會不會先搬檔案
end

	

----------------------------------------20160803-------------------------------------
if @case = 201608030913 begin --Approved 搬檔失敗
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002HXB' --回朔重播  2016-05-07 19 審核通過
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002HXB' --T \\mamstream\MAMDFS\ptstv\HVideo\2014\2014756\G201475600160012.mxf
	select * from TBBROADCAST where FSBRO_ID = '201604210003' -- FDTRAN_DATE = 2016-04-21 09:06
	select *  from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002HXB' order by FDDATE
end

if @case = 201608031120 begin -- 節目規格增加代碼 todo
	select * from TBPROG_M where FSPGMNAME like '%石居的春天%' -- FSPROGSPEC = 02;50
	select * from TBPROG_M where FSPGMNAME like '%與海豚的約定%' --02;50;
	select * from TBPROG_D
	select * from TBZPROGSPEC --節目規格 SD, HD 其他
	select * from TBZSHOOTSPEC -- 拍攝規格
end


-- 排表＼節目表維護＼修改預設段數問題  todo 檢查是否有更新取號規則
if @case = 201608031424 begin
	-- test with Macro channel 0702 playlist
	select distinct FDDATE from TBPGM_COMBINE_QUEUE where FSCHANNEL_ID = '08' order by FDDATE
	select * from TBZCHANNEL

	select * from TBPGM_QUEUE where FDDATE = '2016-07-02' and FSCHANNEL_ID = '08' order by FSBEG_TIME --FNDEF_SEG = 2
	select * from TBPGM_COMBINE_QUEUE where FSCHANNEL_ID = '08' and CAST(FDDATE as DATE) = '2016-07-02' order by FNCONBINE_NO --4 段
	select * from TBPROG_M where FSPROG_ID = '2016520'
	select * from TBPROG_D where FSPROG_ID = '2016520' and FNEPISODE = 184

end

if @case = 201608031648 begin
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-08-03' and FSPROGRAM like '%fnUpdateTapeList_TS3310%' order by FDTIME
	select * from TBTRACKINGLOG where FDTIME between '2016-08-03 13:20' and '2016-08-03 16:40' and FSPROGRAM like '%WSTSM%' order by FDTIME
end

if @case = 201608031730 begin -- 未比對段落，所以檔案到待審區仍無法審核
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002qqv' order by FDDATE --2016-08-05 PTS3 211 17:52:07:20
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002qqv'
end

--------------------------------------20160804-------------------------------------------------

if @case = 201608041018 begin -- dummy table? todo
	select * from tbhdmc20160726
	select * from TBLOG_VIDEO_HD_MC_BAK order by FDCREATED_DATE
	select * from TBLOG_VIDEO_HD_MC order by FDCREATED_DATE
end

if @case = 201608041019 begin -- 插入短帶及節目表維護取號問題 todo
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002U49' --FSPROG_ID = 2016680, FSFILE_NO = ''
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002U49' --no record
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID in ('00002UX7') --FSPROMO_ID = 20150400774 FSFILE_NO = ''
	select * from TBLOG_VIDEO where FSVIDEO_PROG in  ('00002UX7') --no record
end

if @case = 201608021822 begin
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002V04' order by FDDATE --調整播出時間看看會不會先搬檔案
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002LJM' order by FDDATE --水果冰淇淋 1929
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002IZ5' order by FDDATE --2016世界花式滑冰錦標賽 15
end

if @case = 201608041130 begin
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002PMN' --2016-07-05 16:12:10.660 fail
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002PMN'
end

if @case = 201608041649 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002V2I', '00002V2Q')
	select * from TBLOG_VIDEO_HD_MC where CAST(FDUPDATED_DATE as DATE) = '2016-08-04' and FCSTATUS = 'C' --77
	select * from TBLOG_VIDEO_HD_MC where CAST(FDUPDATED_DATE as DATE) = '2016-08-03' and FCSTATUS = 'C' --23
	select * from TBPTSAP_STATUS_LOG where CAST(FDUPDATE_DATE as DATE) = '2016-08-04' --84
	select * from TBPTSAP_STATUS_LOG where CAST(FDUPDATE_DATE as DATE) = '2016-08-03' --68
	--標記上傳數量
	select count(FSVIDEOID), CAST(FDUPDATE_DATE as DATE) UD from TBPTSAP_STATUS_LOG group by CAST(FDUPDATE_DATE as DATE) order by UD
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in ('00002UUI', '00002V5A', '00002VBU', '00002UBT', '00002UBP', '00002UBJ', '00002UBQ', '00002RS8', '00002V88') order by  FDDATE
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in ('00002SCJ', '00002SCC', '00002SGQ') order by FDDATE --marco 8/4

	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in ('00002vbd', '00002vbe', '00002vbf', '00002v32', '00002v2i')
end

if @case = 201608050837 begin --louth purge list
	select CHARINDEX('ab', 'abcdefg') --1
	select CHARINDEX('bc', 'abcdefg') --2
	select SUBSTRING('abcdefg', 1, 3) --abc
	select SUBSTRING('abcdefg', 0, 3) --ab  傳回長度 = 0 + 3 -1 = 2



	--------------- fail-------------------
	declare @tmpTab TABLE (line varchar(10));
	---接近關鍵字 'with' 的語法不正確。如果這個陳述式是通用資料表運算式、xmlnamespaces 子句或變更追蹤內容子句，則前一個陳述式必須以分號結束
	BULK insert @tmpTab from 'c:\temp\no-louth-db.txt' WITH  
	(
		ROWTERMINATOR = '\n'
	)
	----------------------------------------

	CREATE TABLE #tempImport (
		line varchar(10)
	)
	--put input file on MAMDB2
	BULK insert #tempImport from 'c:\temp\no-louth-db.txt' WITH
	(
		ROWTERMINATOR = '\n'
	);

	select * from #tempImport
	select * from @tmpTab

	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in (

end

if @case = 201608051337 begin -- 異數狂潮 3 被刪除
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002ofx'  --08255 刪除 2016-07-20 14:17:53.127
	select * from TBBROADCAST where FSBRO_ID = '201606200180' --轉檔 2016-07-14 11:53:18.110
	select * from TBUSERS where FSUSER_ID = '08255' --陳素娥
	select * from TBUSERS where FSUSER_ID = '70613' --黃暄媛
end

if @case = 201608051528 begin
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-08-05' and FSPROGRAM = 'WSTSM/CheckoutTapes_TSM3310' -- 退 30 支
end

if @case = 201608051700 begin
	select * from TBUSERS where FSUSER_ChtName = '楊琇雯' --71247 離職
	select * from TBPGM_COMBINE_QUEUE where FSCREATED_BY = '71247'
end

if @case = 201608101039 begin  --XML for LOUTH DB
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002HXB' --01:00:00;00 ~ 01:50:20;00
	/*
		01:00:00;00 ~ 01:14:15;00 : SOM = 16777216 = 15:32:42:04 (play time?) DUR = 1316096 ?   why not 107892?
		01:14:25;00 ~ 01:22:55;00
		01:23:05;00 ~ 01:39:45;00
		01:39:55;00 ~ 01:50:20;00
	 */
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G201475600160012'
end

if @case = 201608101514 begin  
	select * from TBPGM_COMBINE_QUEUE where FSPROG_ID = '2014756' and FSCHANNEL_ID = '01'
	select * from TBPGM_COMBINE_QUEUE where FSPROG_NAME like '%浩克%' and FNEPISODE = 10 and FSCHANNEL_ID = '01'
end

------------------------------------20160811---------------------------------------------
if @case = 201608110859 begin --查詢列印的Filing節目清單
	select convert(datetime, '2016/08/11', 111)
	select convert(datetime, '2016/08/11', 110)
	select right('00' + CAST(CONVERT(INT, '51') -50 as varchar(2)), 2) --01
	select right('00' + '01', 3) --001

	select * from TBPGM_COMBINE_QUEUE a left join TBZCHANNEL d on a.FSCHANNEL_ID = d.FSCHANNEL_ID  --61
		where a.FDDATE = '2016-08-10' and a.FSCHANNEL_ID = '12'

	select distinct a.FSPROG_ID, a.FNSEQNO, a.FNEPISODE, a.FSPROG_NAME, a.FSVIDEO_ID, a.FNBREAK_NO, a.FSPLAY_TIME, a.FDDATE, a.FSDUR, d.FSCHANNEL_NAME from TBPGM_COMBINE_QUEUE a left join TBZCHANNEL d on a.FSCHANNEL_ID = d.FSCHANNEL_ID  --61
		where a.FDDATE = '2016-08-10' and a.FSCHANNEL_ID = '12' --61

	--前一天有播出的濾掉
	declare @date DATE
	declare @CH varchar(2)
	set @date = '2016-08-06'
	set @CH = '07'
	select distinct a.FSPROG_ID, a.FNSEQNO, a.FNEPISODE, a.FSPROG_NAME, a.FSVIDEO_ID, a.FNBREAK_NO, a.FSPLAY_TIME, a.FDDATE, a.FSDUR, d.FSCHANNEL_NAME from TBPGM_COMBINE_QUEUE a left join TBZCHANNEL d on a.FSCHANNEL_ID = d.FSCHANNEL_ID  --61
		where a.FDDATE = @date and a.FSCHANNEL_ID = @CH
		and a.FSVIDEO_ID not in (select FSVIDEO_ID from TBPGM_COMBINE_QUEUE where FDDATE = convert(datetime, @date, 111) -1 and FSCHANNEL_ID = @CH) order by a.FSPLAY_TIME

	declare @date DATE
	declare @CH varchar(2)
	set @date = '2016-08-06'
	set @CH = '07'
	--濾掉前 7 天內有播出
	select distinct a.FSPROG_ID, a.FNSEQNO, a.FNEPISODE, a.FSPROG_NAME, a.FSVIDEO_ID, a.FNBREAK_NO, a.FSPLAY_TIME, a.FDDATE, a.FSDUR, d.FSCHANNEL_NAME from TBPGM_COMBINE_QUEUE a left join TBZCHANNEL d on a.FSCHANNEL_ID = d.FSCHANNEL_ID
		where a.FDDATE = @date and a.FSCHANNEL_ID = @CH
		and a.FSVIDEO_ID not in (select FSVIDEO_ID from TBPGM_COMBINE_QUEUE where FDDATE between convert(datetime, @date, 111) - 7 and convert(datetime, @date, 111) -1 and FSCHANNEL_ID = @CH) --61
		order by a.FSPLAY_TIME

	--TEST
	select FSVIDEO_ID from TBPGM_COMBINE_QUEUE where FDDATE between convert(datetime, '2016-08-10', 111) -7 and convert(datetime, '2016-08-10', 111) -1 and FSCHANNEL_ID = '12'
	select * from TBPGM_COMBINE_QUEUE where FDDATE = '2016-08-10' and FSCHANNEL_ID = right('00' + CAST(CONVERT(INT, '12') - 50 as varchar(2)), 2)
	select right('00' + CAST(CONVERT(INT, '12') - 50 as varchar(2)), 2)
	select CAST(CONVERT(INT, '12') - 50 as varchar(2)) --*
	select CAST(CONVERT(INT, '12') - 50 as varchar(3)) -- -38
	select right('00' + CAST(CONVERT(INT, '12') - 50 as varchar(2)), 3)

	select * from TBZCHANNEL
end



if @case = 201608110917 begin
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002VEQ'
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002VEQ'
end


/************************************************************
louth purge list
*************************************************************/

if @case = 201608111641-201608050837 begin
	select CHARINDEX('ab', 'abcdefg') --1
	select CHARINDEX('bc', 'abcdefg') --2
	select SUBSTRING('abcdefg', 1, 3) --abc
	select SUBSTRING('abcdefg', 0, 3) --ab  傳回長度 = 0 + 3 -1 = 2



	--------------- fail-------------------
	declare @tmpTab TABLE (line varchar(10));
	---接近關鍵字 'with' 的語法不正確。如果這個陳述式是通用資料表運算式、xmlnamespaces 子句或變更追蹤內容子句，則前一個陳述式必須以分號結束
	BULK insert @tmpTab from 'c:\temp\no-louth-db.txt' WITH  
	(
		ROWTERMINATOR = '\n'
	)
	----------------------------------------

	if OBJECT_ID(N'tempdb.dbo.#tempImport', N'U') IS NOT NULL
		DROP TABLE #tempImport

	CREATE TABLE #tempImport (
		line varchar(10)
	)
	--put input file on MAMDB2
	BULK insert #tempImport from 'c:\temp\no-louth-db.txt' WITH
	(
		ROWTERMINATOR = '\n'
	);

	--select * from #tempImport

	if OBJECT_ID(N'tempdb.dbo.#tmp', N'U') IS NOT NULL
		DROP TABLE #tmp

    select * into #tmp from VW_PLAYLIST_FROM_NOW where 1=0
	--select * from  #tmp

	declare CSR CURSOR for select * from #tempImport
	declare @videoID varchar(8)

	declare @cnt int = 1

	open CSR
	fetch next from CSR into @videoID
	while (@@FETCH_STATUS = 0)
	begin
		--print CAST(@cnt as varchar(5)) + ' : ' +  @videoID
		if EXISTS(select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = right('00' + @videoID, 8)) begin
			--print CAST(@cnt as varchar(5)) + ' : ' +  @videoID
			insert #tmp select top 1 * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = right('00' + @videoID, 8) order by FDDATE, FSPLAY_TIME, FSCHANNEL_ID
		end
		fetch next from CSR into @videoID
		set @cnt = @cnt + 1
	end

	select * from #tmp
end


if @case = 201608121425 begin -- louth db has no data
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002HX7' --2016-08-09 14:24:43.420 審核通過
	--20160809-152813.xml 寫入louth db
end

if @case = 201608121500 begin
	select * from TBPROG_M where FSPGMNAME like '%水果%' --0000060
	select SUM(CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) -2))) from TBLOG_VIDEO where FSID = '0000060' and FCFILE_STATUS = 'T' --水果的檔案大小 4871G
	select FSFILE_SIZE from TBLOG_VIDEO where FCFILE_STATUS = 'T' and FSTYPE = 'G' -- 11913 files
	select SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2) from TBLOG_VIDEO where FCFILE_STATUS = 'T' and FSTYPE = 'G' --傳遞到 LEFT 或 SUBSTRING 函數的參數長度無效。
	select FSID, FNEPISODE, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2) from TBLOG_VIDEO where FCFILE_STATUS = 'T' and FSTYPE = 'G' and FSFILE_SIZE <> ''  --2014438 896G
	select * from TBPROG_M where FSPROG_ID = '2014438' --PeoPo公民新聞報(2014週末版)
	select * from TBLOG_VIDEO where FSID = '2014438' --896MB

	--todo
	select LEN(FSFILE_SIZE), FSFILE_SIZE from TBLOG_VIDEO where FCFILE_STATUS = 'T' and FSTYPE = 'G' and LEN(FSFILE_SIZE) = 0
	select * from TBLOG_VIDEO where FSFILE_SIZE = '' and FCFILE_STATUS = 'T' --22G

	select SUM(CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2))) from TBLOG_VIDEO where FCFILE_STATUS = 'T' and FSTYPE = 'G' and FSFILE_SIZE <> '' --206286426.8 G = 206286 T?
	
	--估算已轉檔入庫的資料量
	select 
	SUM(CASE
			WHEN FSFILE_SIZE like '%GB%' THEN CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2))
			WHEN FSFILE_SIZE like '%MB%' THEN CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2))/1000
		END
	)
	from TBLOG_VIDEO where FCFILE_STATUS = 'T' and FSTYPE = 'G' and FSFILE_SIZE <> ''  --328857.246 GB = 328T
end

if @case = 201608151511 begin -- 上傳失敗
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002VR8'  --2016-08-15 11:08:38.007 標記上傳
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002VR8'  -- 2016-08-15 10:45:46.073 上傳失敗  -> 2016-08-15 15:40:00.340
end


if @case = 201608151546 begin  -- 由 Video ID 查名稱
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00001UH2'
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002BAX'
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002TL2'
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002UM0'
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002VD7'

	select * from TBPROG_M where FSPROG_ID = '2012119' --公主與王子(白天不宜) #1
	select * from TBPROG_M where FSPROG_ID = '2015586' --音樂萬萬歲3 #35
	select * from TBPROG_M where FSPROG_ID = '2016773' --氣球小子 #50
	select * from TBPROG_M where FSPROG_ID = '2016604' --台北工廠2-愛情肥皂劇 #1
	select * from TBPROG_M where FSPROG_ID = '2016542' --PeoPo公民新聞報(2016週間版) #151

	select FSPROMO_ID, FSPROMO_NAME from TBPGM_PROMO
	select FSPGMNAME name from TBPROG_M union (select FSPROMO_NAME name from TBPGM_PROMO) --53323
	select FSPGMNAME name from TBPROG_M union all (select FSPROMO_NAME name from TBPGM_PROMO) --53649
	select distinct name from (select FSPGMNAME name from TBPROG_M union all (select FSPROMO_NAME name from TBPGM_PROMO)) u --53323

	-- 查詢名稱重複
	select name, count(name) from (select FSPGMNAME name from TBPROG_M union all (select FSPROMO_NAME name from TBPGM_PROMO)) u group by name having count(name) > 1 --299
	select * from TBPGM_PROMO where FSPROMO_NAME = '下課花路米23蠶絲promo' -- 2012 建立
	select name, count(name) from (select FSPGMNAME name, FDCRTDATE CDATE from TBPROG_M union all (select FSPROMO_NAME name, FDCREATED_DATE CDATE from TBPGM_PROMO)) u where CDATE > '2016-01-01' group by name having count(name) > 1
	select * from TBPGM_PROMO where FSPROMO_NAME = 'RM-動一動好健康' --FSDEL = 'Y'
	select name, count(name) from (select FSPGMNAME name, FDCRTDATE CDATE from TBPROG_M union all (select FSPROMO_NAME name, FDCREATED_DATE CDATE from TBPGM_PROMO where FSDEL = 'N')) u where CDATE > '2016-01-01' group by name having count(name) > 1
	select * from TBPGM_PROMO where FSPROMO_NAME = '國寶神獸闖天關' --FSDEL = 'Y'
	select * from TBPGM_PROMO where FSPROMO_NAME = '聽客語在搖擺' --FSDEL = 'Y'
	-------------------------------------------------------------------------------------


	select sum(c) from (select name, count(name) c from (select FSPGMNAME name from TBPROG_M union all (select FSPROMO_NAME name from TBPGM_PROMO)) u group by name having count(name) > 1) uu --625 625-299 = 326 = 53649 - 53323

	select FSPGMNAME name, FSPROG_ID FSID from TBPROG_M union (select FSPROMO_NAME NAME, FSPROMO_ID FSID from TBPGM_PROMO) --53649

    /****************FAIL 資料帶和短帶有相同 FSID ******
	select v.FSBRO_ID, v.FSVIDEO_PROG, u.FSID, u.NAME, v.FNEPISODE from TBLOG_VIDEO v join 
		(select FSPGMNAME name, FSPROG_ID FSID from TBPROG_M union (select FSPROMO_NAME name, FSPROMO_ID FSID from TBPGM_PROMO)) u 
		on v.FSID = u.FSID

	select * from TBLOG_VIDEO group by FSID
	select * from TBLOG_VIDEO where FSID = '20121100008'
	***************************************************/


	select v.FSBRO_ID, v.FSVIDEO_PROG, u.FSID, u.NAME, v.FNEPISODE from TBLOG_VIDEO v join 
		(select FSPGMNAME name, FSPROG_ID FSID from TBPROG_M union (select FSPROMO_NAME name, FSPROMO_ID FSID from TBPGM_PROMO)) u 
		on v.FSID = u.FSID and v.FSTYPE = 'G' or v.FSTYPE = 'P'

	select * from TBPGM_PROMO where FSPROMO_ID = '20121100008'
	select * from TBPROG_M where FSPROG_ID = '20121100008'
end


------------------------------------------------20160816---------------------------------
if @case = 201608160927 begin -- 檢查節目或短帶名稱重複 _todo
	select FSPROMO_NAME, count(FSPROMO_NAME) from TBPGM_PROMO where FSDEL = 'N' and FDCREATED_DATE > '2015-01-01' group by FSPROMO_NAME having count(FSPROMO_NAME) > 1 --26 筆
	select * from TBPGM_PROMO where FSPROMO_NAME = '獨立特派員 第434集 預告'
	--節目
	select FSPGMNAME, count(FSPGMNAME) from TBPROG_M group by FSPGMNAME having count(FSPGMNAME) > 1 -- 7
	select FSPGMNAME, count(FSPGMNAME) from TBPROG_M where FSDEL = 'N' group by FSPGMNAME having count(FSPGMNAME) > 1 --0
	select  * from TBPROG_M where FSPGMNAME = '林鵰－山林魅影'
end

if @case = 201608161129 begin --只有 SD 送播單的短帶檔案可以排播
	select * from TBLOG_VIDEO where FSBRO_ID = '201608110064' --20160800366
	select * from TBLOG_VIDEO where FSID = '20160800366'
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002W14' order by FDDATE

	select * from TBLOG_VIDEO where FDCREATED_DATE > '2016-06-08' and FSARC_TYPE = '001' and LEN(FSTRACK) > 4 and FSTYPE = 'G' --有28筆 SD 送播單
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID in ('00002RIV', '00002MQE', '00002MRF', '00002MRU') -- 未排播
end


if @case = 201608161707 begin --手動刪除送帶轉檔單
	select * from TBPROG_M where FSPGMNAME like '%出境事務所%' --2015006
	select * from TBLOG_VIDEO where FSID = '2015006' and FSARC_TYPE = '002' and FCFILE_STATUS = 'B'
end


----------------------------------------20160817-----------------------------------

if @case = 201608171019 begin --特殊字
	select * from TBPROPOSAL where FSPROG_ID = '2017217'
end

if @case = 201608171748 begin --列印成案呈核報表紀錄
	select * from RPT_TBPROPOSAL_01 order by QUERY_DATE DESC
end


if @case = 201608170912 begin --已經有送帶轉檔單仍可以建回朔單?
	--感恩故事集89集
	select * from TBLOG_VIDEO where FSID = '2007456' and FNEPISODE  = 89 --FSBRO_ID = 201412220037, 00001L4N
	select * from TBBROADCAST where FSBRO_ID in ('201608160107', '201608160102', '201608150116')
	select * from TBLOG_VIDEO where FSBRO_ID in ('201608160107', '201608160102', '201608150116') --no record

	select * from TBLOG_VIDEO where FSID = '2014841' and FNEPISODE = 9 --行走TIT #9 無法建回朔改走送播流程

	select * from TBLOG_VIDEO where FSID = '2015385' and FNEPISODE = 3 and FSARC_TYPE = '002' --陳素娥 刪除
	select * from TBUSERS where FSUSER_ID = '08255'

	select * from TBLOG_VIDEO where FSID = '2015385' and FNEPISODE = 6 and FSARC_TYPE = '002'
	select * from TBLOG_VIDEO where FSID = '2015385' and FNEPISODE = 5 and FSARC_TYPE = '002'

	select * from TBLOG_VIDEO where FSID = '0000001' and FSARC_TYPE = '002' order by FDCREATED_DATE
end

----------------------------------------20160818----------------------------------
if @case = 201608180959-201608171748 begin --列印成案呈核報表紀錄
	select * from RPT_TBPROPOSAL_01 order by QUERY_DATE DESC
end

if @case = 201608181427 begin
	--以經建了短帶資料，新增短帶送播資料時當掉，請使用者補建送播單和託播單
	select * from TBPGM_PROMO where FSPROMO_NAME like '%客家人有名堂第39集朱海君%' --20160800539 2016-08-18 11:50 created by 71172
	select * from TBLOG_VIDEO where FSID = '20160800539' --無送播單
	select * from TBBROADCAST where FDCREATED_DATE between '2016-08-18 11:45' and '2016-08-18 11:55'
end

if @case = 201608181503 begin --todo
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002PMO' --9/2 播出
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002PMO' --2016-07-05 上傳失敗
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002PMO'
end


------------------------------20160819------------------------------
if @case = 201608191427 begin -- 台灣親NAGNAG
	select * from TBLOG_VIDEO where FSID = '2010742' and FSARC_TYPE = '002'
end

-----------------------------------------------------------------------------------------



----------------------------20160822---------------------------------------------------------
if @case = 201608221527 begin --託播單查詢應該要篩掉以經刪除的短帶託播單  todo
	select * from TBPGM_PROMO_BOOKING where FDCREATED_DATE > '2016-08-19'
	select * from TBPGM_PROMO where FSPROMO_NAME like '%身體百科100%' --20160800586 短帶已經刪除，所以插入短帶查不到該短帶託播單
end

---------------------------20160824-----------------------------------------------------
if @case = 201608240927 begin  --no louth data
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002UQQ' --not been schedule 
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002UQQ'
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00001POW' -- not been schedule

	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002UQQ' order by FDDATE --2016-08-10, 2016-08-22, 2016-08-23
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00001PWO' order by FDDATE --2016-08-05, 2016-08-22
end


if @case = 201608241154 begin --審核通過後，轉檔失敗，無法置換  todo
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002X4X'
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'P201608006290001'
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002x4x' order by FDDATE --2016-08-25

	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002X4W' order by FDDATE  --2016-08-24
	select top 1000 * from TBTRACKINGLOG order by FDTIME desc
	select top 1000 * from TBTRAN_LOG order by FDDATE desc

	--更新 HDUpload 檔案，重啟任務卡在 TSM
end

if @case = 201608241344 begin  --haka no louth db
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002ONB' order by FDDATE --2016-07-05, 2016-07-21, 2016-07-23, 2016-08-25
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002SDH' order by FDDATE --2016-08-05, 2016-08-27, 2016-09-08
end

if @case = 201608241406 begin --尚未轉檔，所以未到待審區
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in ('00002P4B', '00002P4C', '00002I96') order by FDDATE
	select * from TBLOG_VIDEO where FSVIDEO_PROG in ('00002P4B', '00002P4C', '00002I96')  --
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002P4B', '00002P4C', '00002I96')
	select * from TBUSERS where FSUSER_ID = '08361' --孫祖琪 2016-08-25 10:09:11.940 審核通過
	select * from TBUSERS where FSUSER_ID = '71355'
	select * from TBBROADCAST where FSBRO_ID = '201606240039' --郭哲安 2016-08-24 15:02:48.290 轉檔
end

-------------------------------20160825------------------------------------------------------------
if @case = 201608251123 begin  --在送播單和送帶轉檔單都可以查詢到
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002I96'  --201604200040, E, 我家是戰國 5, 2016-08-30
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002I96'
end

-------------------------------20160826-------------------------------------------------------------
if @case = 201608260927 begin --轉低解失敗，檔案未入庫
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002WRU' --2016-08-18 17:04:47.080
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002WRU' --O 2016-08-18 17:51:22.030 P201608005610001
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002WRU' --2016-08-28
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002WRU' --201608180135, S P201608005610001
	select * from TBBROADCAST where FSBRO_ID = '201608180135' -- TRAN DATE 2016-08-18 17:51:23.133
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002WRU' --2016-08-28

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002UM8' -- O 2016-08-10 14:16:55.830
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002UM8' --R 201607280096 FSID=2017136 #4 , \\mamstream\MAMDFS\Media\Lv\ptstv\2017\2017136\G201713600040003.???
	select * from TBBROADCAST where FSBRO_ID = '201607280096' --TRAN DATE 2016-08-10 14:17:09.983
	select top 100 * from TBLOG_VIDEO where FCFILE_STATUS = 'T' and LEN(FSTRACK) = 8 order by FDUPDATED_DATE desc
end

if @case = 201608261346 begin
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002XJF'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002XJF' --201608250123
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002XJF' order by FDDATE
end

if @case = 201608261611 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002WQL' --O 2016-08-25 17:09:19.910 by 51256
end

if @case = 201608261612 begin
	select * from TBPROG_M where FSPGMNAME like '%就是愛運動%' --2010113
	select * from TBLOG_VIDEO where FSID = '2010113' and FSARC_TYPE = '002' and FCFILE_STATUS = 'T' order by FNEPISODE
	select * from TBLOG_VIDEO where FSID = '2010113' order by FNEPISODE
end

---------------------------------------------20160829--------------------------------------
if @case = 201608291044 begin --標記上傳失敗
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002XOG' --2016-08-29
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002XOG' --201608260126
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002XOG'
end


---------------------------------------------20160830--------------------------------------
if @case = 201608300851 begin --檢索查倒置換的檔案
	select * from TBLOG_VIDEO where FSID = '2015496' and FNEPISODE = 27
end

if @case = 201608300915 begin
	select * from TBLOG_VIDEO where FSBRO_ID = '201608250136'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002XK7'
end

if @case = 201608300958 begin
	select * from TBTRACKINGLOG where FDTIME between '2016-08-28' and '2016-08-31' and FSMESSAGE like '%TS3310%' order by FDTIME desc
	select * from TBUSERS where FSUSER_ID = '08255' --陳素娥
end

-----------------------------------------20160831---------------------------------------
if @case = 201608311036 begin -- 藝數狂潮 #3 無法建送播單
	select * from TBPROG_M where FSPGMNAME = '藝數狂潮'
	select * from TBLOG_VIDEO where FSID = '2016224' and FNEPISODE = 3 and FSARC_TYPE = '002' -- 因為素娥已經建送帶轉檔單
end

if @case = 201608311203 begin --todo 調用 1,2 集只下來 第 2 集
	select * from TBPROG_M where FSPGMNAME = '無米樂' --2005101
	select * from TBLOG_VIDEO where FSID = '2005101' and FNEPISODE = 1 --G200510100010001
	select * from TBLOG_VIDEO where FSID = '2005101' and FNEPISODE = 2 --G200510100020001

	select * from TBBOOKING_MASTER where CAST(FDBOOKING_DATE as DATE) = '2016-08-29' --201608290003, 節目銷售，無米樂(上、下)、一把青1-2
	select * from TBBOOKING_DETAIL where FSBOOKING_NO = '201608290003'
end 

if @case = 201608311417 begin --TPPTSAP_STATUS_LOG 無上傳紀錄，但是檔案已經上傳到 HDUpload
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002XXB' --no data
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002XXB' --201608300036
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002UCZ' --no data
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002UCZ'
end


/******************************
	modify  TBSYSTEM_CONFIG
*******************************/
if @case = 201609010935-201607271724 begin
	select FSSYSTEM_CONFIG into #tmp_TBSYSTEM_CONFIG from TBSYSTEM_CONFIG
	select * from #tmp_TBSYSTEM_CONFIG
	update #tmp_TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<BroadcastEmail Desc=""送播單Email"">oappts@mail.pts.org.tw</BroadcastEmail>', 
	'<BroadcastEmail Desc=""送播單Email"">oappts@mail.pts.org.tw; hsing1@mail.pts.org.tw</BroadcastEmail>')

	update #tmp_TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, 'oappts@mail.pts.org.tw', 
	'oappts@mail.pts.org.tw; hsing1@mail.pts.org.tw')

	update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, 'oappts@mail.pts.org.tw', 
	'oappts@mail.pts.org.tw; hsing1@mail.pts.org.tw')

	update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, 'oappts@mail.pts.org.tw; hsing1@mail.pts.org.tw', 
	'oappts@mail.pts.org.tw')

	select * from TBSYSTEM_CONFIG
end

if @case = 201609011053 begin --瀏覽器放大功能造成表單無法顯示確認鍵，強制關閉瀏覽氣後，資料不完整，需手動刪除
	select * from TBLOG_VIDEO where FSID = '2016802' and FNEPISODE = 22 --201609010007 --> no data  G201680200220001
	exec SP_D_TBLOG_VIDEO_BYBROID '201609010007' --using sa
	exec SP_D_TBLOG_VIDEO_SEG_BYFILEID 'G201680200220001'
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G201680200220001'
end

if @case = 201609011121 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002Y1O'  --2016-08-31 18:21:46.070 標記上傳，但是無上傳紀錄
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002Y1O'
end

if @case = 201609011545 begin --TSM 回傳錯誤訊息
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002NTB' --FCSTATUS = O
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002NUJ' --FCSTATUS = O
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002UN9' --FCSTATUS = O
	select * from TBLOG_VIDEO where FSVIDEO_PROG in ('00002NTB', '00002NUJ', '00002UN9')
end

if @case = 201609021056 begin
	select * from TBTRACKINGLOG where FDTIME > '2016-09-01' order by FDTIME
end

if @case = 201609021121 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002UO2' --2016-08-01 16:49:40.567 -> O
    select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000029KV' --2016-07-21 11:25:43.000 -> O
	
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002UO2' order by FDDATE --2016-09-03
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000029KV' order by FDDATE --2016-09-03

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002Y0C' --2016-08-31 10:39:03.070 -> O

	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002Y0C' order by FDDATE --2016-09-02
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002Y0C' --T
end

if @case = 201609021718 begin
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002PMR'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002PMR'
end


if @case = 201609011336 begin --TPPTSAP_STATUS_LOG 無上傳紀錄，但是檔案已經上傳到 HDUpload
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002XJ8'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002XJ8'
	select * from TBPTSAP_STATUS_LOG where CAST(FDUPDATE_DATE as DATE) = '2016-08-31' order by FDUPDATE_DATE
	select * from TBLOG_VIDEO_HD_MC where CAST(FDUPDATED_DATE as DATE) = '2016-08-31' order by FDUPDATED_DATE --FCSTATUS = '' :00002XEE, 00002Y0V
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID in ('00002XEE', '00002Y0V')
end

if @case = 201609021127 begin --HD_FILE_MANAGER
	select * from TBLOG_VIDEO where FSVIDEO_PROG in ('00002ROQ', '00002S24', '00002W14')
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002Y9C' order by FDDATE  --2016-09-02 05:00:00;00 first time play

	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002SJ3' order by FDDATE
	select * from TBLOG_VIDEO where FSFILE_NO = 'P201608006320001' -- 00002X5G
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002X5G' --00002X5G
end

if @case = 201609040104 begin --todo Block HD_FILE_MANAGER
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002YAQ' --R , 201609020009, P201608008510002
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002YAQ' --2016-09-05
	select * from TBBROADCAST where FSBRO_ID = '201609020009' --2016-09-02 12:02:19.477
	select * from TBPGM_PROMO where FSPROMO_ID = '20160800851' --2016 09/05 pts1 menu 12:00
	--copy file from HDUpload to Review
	select * from TBLOG_VIDEO where FSVIDEO_PROG in ('00002JPL') --T
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002JPL' --not play
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002JPL' order by FDDATE

	select * from TBLOG_VIDEO where FSVIDEO_PROG in ('00002JPG', '00002NV0') --T
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in  ('00002JPG', '00002NV0') order by FDDATE
end

--高解搬檔失敗：/tcbuffer/18TB/IngestWorkingTemp/G000021603820012.mxf/10.13.210.16 -l root -pw TSMP@ssw0rdTSM "/Media/WorkingTemp/MoveMetaSANFile /tcbuffer/18TB/00000000.lock /tcbuffer/18TB/IngestWorkingTemp/G000021603820012.mxf /ptstv/HVideo/0000/0000216/G000021603820012.mxf"/(Error)Source /tcbuffer/18TB/IngestWorkingTemp/G000021603820012.mxf is not exist.
if @case = 201609051222 begin 
	select top 500 * from TBTRACKINGLOG where FDTIME between '2016-09-05 12:00' and '2016-09-05 12:30' and FSCATALOG = 'ERROR' order by FDTIME 
	select * from TBLOG_VIDEO where FSFILE_NO = 'G000021603820012'
	select top 500 * from TBTRACKINGLOG where FDTIME between '2016-09-05 14:45' and '2016-09-05 15:30' order by FDTIME 
end

if @case = 201609051821 begin
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002Y0V'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002Y0V'
end

if @case = 201609061042 begin --轉檔成功搬檔失敗 水果 1817
	select * from TBLOG_VIDEO where FSFILE_NO = 'G000006018170001'  -- R
	select * from TBBROADCAST where FSBRO_ID = '201412220067' --2016-09-06 09:06:38.620
	--Could not find file 'T:\IngestWorkingTemp\G000006018170001.wmv'.  --> 應該搬 mp4
	select top 100 * from TBTRACKINGLOG where FSPROGRAM like '%ProcessRequest%' and CAST(FDTIME as DATE) = '2016-09-06' and FSCATALOG = 'ERROR' order by FDTIME
	select top 100 * from TBTRACKINGLOG where FSPROGRAM like '%ProcessRequest%' and CAST(FDTIME as DATE) = '2016-09-06'  order by FDTIME
end

if @case = 201609061129 begin --TBPTSAP_STATUS_LOG 上傳完成無紀錄
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002XEE'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002XEE'
end

if @case = 201609061619 begin --002IS6 在 QC 和插入短帶看到都是待審核，但是檔案仍在 HDUpload  todo
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002IS6' --2016-09-14
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002IS6' --2016-05-23 11:29:35.240 -> N
end


if @case = 201609070821 begin  -- 節目表無法顯示難字
	select FSPROG_ID, FSPGMNAME, FSWEBNAME from TBPROG_M where FSWEBNAME like '%&#%'
	-- 看錯節目 口述影像版
end

if @case = 201609071634 begin  -- QC 轉檔一直卡在 TSM 處裡中
	select * from TBLOG_VIDEO where FSID = '2016321' and FNEPISODE = 28 --S -> T, 00002SIX, 201607190149, G201632100280001
	select * from TBLOG_VIDEO where FSID = '20160800872' and FNEPISODE = 0 --S -> T, 00002Y1U, 201608310067, P201608008720001
	select * from TBLOG_VIDEO where FSID = '2010294' and FNEPISODE = 152 --S, 00002Y7K, 201609010038, G201029401520002
	select * from TBLOG_VIDEO where FSID = '2006186' and FNEPISODE = 789 --S , 00002Y2C, 201608310088, G200618607890001, 奧林P客
	select * from TBLOG_VIDEO where FSID = '2017138' and FNEPISODE = 6 --S, 00002XXD, 201608300038
	select * from TBLOG_VIDEO where FSID = '20160900009' and FNEPISODE = 0 --S, 00002Y8H, 201609010081, P201609000090001
	select * from TBLOG_VIDEO where FSID = '2006262' and FNEPISODE = 504 --S, 00002Y9C, 201609010116, G200626205040002
	select * from TBLOG_VIDEO where FSID = '2015729' and FNEPISODE = 38 --S, 00002USK, 201607290155, G201572900380003

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002SIX', '00002Y1U', '00002Y7K', '00002Y2C', '00002XXD', '00002Y8H', '00002Y9C', '00002USK') --O
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in ('00002SIX', '00002Y1U', '00002Y7K', '00002Y2C', '00002XXD', '00002Y8H', '00002Y9C', '00002USK') order by FDDATE
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002Y9C' --not scheduled
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002Y1U' order by FDDATE

	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201608008720001'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201608008720001'
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002Y1U' --聽聽看105年9月Promo_pts2, 13

	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201632100280001'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201632100280001'
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002SIX' --湯姆歷險記(2015數位版) #28, 07

	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201029401520002'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201029401520002'
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002Y7K' --客庄好味道(第19集不能播) 152, 07

	--todo
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G200618607890001'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'C' where FSFILE_NO = 'G200618607890001'  -- HDUpload -> Review
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002Y2C' --not scheduled
	select * from TBPROG_M where FSPROG_ID = '2006186' --奧林P客 

	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201713800060001'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201713800060001'
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002XXD' --小手掌廚(客語版) #6, 07

	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201609000090001'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201609000090001'
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002Y8H' --奧林P客第一周PROMO, 07

	--todo
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G200626205040002'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G200626205040002'
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002Y9C' --not scheduled

	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201572900380003'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201572900380003'
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002USK' --妙鼠先瘋 #38, 07

	select * from VW_PLAYLIST_FROM_NOW where FSCHANNEL_ID = '08' order by FDDATE



	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002XWN'  --MXF -> mxf
end

if @case = 201609081052 begin --查詢表單下拉選單欄位無資料
	select * from TBPROG_M where FSPROG_ID = '2013232'
	select * from TBTRACKINGLOG where FDTIME between '2016-09-7' and '2016-09-09' and FSPROGRAM like '%MAMsystemMonitor%' and FSCATALOG = 'ERROR' order by FDTIME --136
	select * from TBTRACKINGLOG where FDTIME between '2016-07-7' and '2016-07-08' and FSPROGRAM like '%MAMsystemMonitor%' and FSCATALOG = 'ERROR' order by FDTIME --94
	select * from TBTRACKINGLOG where FDTIME between '2016-09-04' and '2016-09-05' and FSPROGRAM like '%MAMsystemMonitor%' and FSCATALOG = 'ERROR' order by FDTIME --143

	select CAST(FDTIME as DATE), count(FDTIME) from TBTRACKINGLOG  where FDTIME between '2016-06-1' and '2016-09-09' and FSPROGRAM like '%MAMsystemMonitor%' and FSCATALOG = 'ERROR' 
	group by CAST(FDTIME as DATE) order by CAST(FDTIME as DATE)

	select CAST(FDTIME as DATE), count(FDTIME) from TBTRACKINGLOG  where FDTIME between '2016-06-1' and '2016-09-09' and FSPROGRAM like '%MAMsystemMonitor%' and FSCATALOG = 'ERROR'
	and FSMESSAGE like '%MAMDB%' group by CAST(FDTIME as DATE) order by CAST(FDTIME as DATE)

	select * from TBTRACKINGLOG where FDTIME between '2016-09-7' and '2016-09-09' and FSPROGRAM like '%MAMsystemMonitor%' and FSCATALOG = 'ERROR' and FSMESSAGE like '%MAMDB%' 
	order by FDTIME --136

	select * from TBZCHANNEL
end

if @case = 201609082122 begin  --上傳檔案未到
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002YWE', '00002YWH') --C -> N
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in ('00002YWE', '00002YWH') order by FDDATE
end

/*
 * troubleshooting : anystream 轉檔完成，轉檔單仍顯示轉檔中 （送帶轉檔）
 */
if @case = 201609090924 begin  -- 
	select * from TBLOG_VIDEO where FSFILE_NO = 'G000006018210001' --S, 201501080057, 00001MVZ  -- files are in 18T
	select * from TBLOG_VIDEO where FSFILE_NO = 'G000006018220001' --S. 201501090165, 00001N1M  -- files are in 18T
	select * from TBBROADCAST where FSBRO_ID = '201501080057' --2016-09-06 13:32:52.823
	select * from TBTRACKINGLOG where FDTIME between '2016-09-06 13:20' and '2016-09-06 14:00' order by FDTIME -- 任務編號 143145, TSM:188768, Anystream 94731
	select * from TBTRACKINGLOG where FDTIME between '2016-09-06 13:20' and '2016-09-06 14:00' and FSMESSAGE like '%143145%' order by FDTIME --no data
	select * from TBTRACKINGLOG where FDTIME between '2016-09-06 13:20' and '2016-09-06 14:00' and FSMESSAGE like '%G000006018210001%' order by FDTIME --2016-09-06 13:32:52.073

	--1821 log  -> Error : Unable to communicate with the ECS on [ECS-JM:3501] 檔案未搬
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-09-06'  
	and (FSMESSAGE like '%G000006018210001%' or FSMESSAGE like '%143145%' or FSMESSAGE like '%94731%' or FSMESSAGE like '%188768%' or FSMESSAGE like '%00001MVZ%') order by FDTIME

	--1822 log -> last process : 開始進行高解搬檔：/tcbuffer/18TB/IngestWorkingTemp/G000006018220001.mxf
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-09-06'  
	and (FSMESSAGE like '%G000006018220001%' or FSMESSAGE like '%143144%' or FSMESSAGE like '%94730%' or FSMESSAGE like '%188767%' or FSMESSAGE like '%00001N1M%') order by FDTIME

	--將轉檔狀態改為失敗重新轉檔
	--update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G000006018210001' -- sa
	select * from TBTRACKINGLOG where FDTIME > '2016-09-09 15:50' and (FSMESSAGE like '%G000006018210001%' or FSMESSAGE like '%143145%' or FSMESSAGE like '%94731%' or FSMESSAGE like '%188768%' or FSMESSAGE like '%00001MVZ%') order by FDTIME

	--update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G000006018220001' -- sa
	select * from TBTRACKINGLOG where FDTIME between '2016-09-09 16:20' and '2016-09-10' and  (FSMESSAGE like '%G000006018220001%' or FSMESSAGE like '%143144%' or FSMESSAGE like '%94730%' or FSMESSAGE like '%188767%' or FSMESSAGE like '%00001N1M%') order by FDTIME


	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in ('00001MVZ', '00001N1M') --not scheduled

    select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = CAST(CURRENT_TIMESTAMP - 1 as DATE) and FSCATALOG = 'ERROR'

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-09-06' and FSMESSAGE like '%143099%' order by FDTIME
end
 
if @case = 201609091142 begin --Study TBTRACKINGLOG for transcode job
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-09-06' and FSPROGRAM = 'ClickToStartTrancode' order by FDTIME  -- 查詢由帶子轉檔數量
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-09-06' and FSPROGRAM = 'ClickToStartTrancode' and FSMESSAGE like '%G000081613920003%' order by FDTIME  -- 2016-09-06 14:07:20.093 submit 時間，並非開始轉檔時間
	select * from TBTRACKINGLOG where FDTIME between '2016-09-06 14:00' and '2016-09-06 15:00' order by FDTIME

	--根據轉檔任務 ID, Anystream ID, TSM ID, Video ID
	select * from TBTRACKINGLOG where FDTIME between '2016-09-06 14:00' and '2016-09-06 15:00' 
	and (FSMESSAGE like '%G000081613920003%' or FSMESSAGE like '%143155%' or FSMESSAGE like '%94740%' or FSMESSAGE like '%188778%' or FSMESSAGE like '%00001V6L%') order by FDTIME

	select * from TBLOG_VIDEO where FSFILE_NO = 'G000081613920003'
end

if @case = 201609091729 begin --檔案送播轉檔，重啟任務後卡在 TSM 處裡中
	select * from TBBROADCAST where FSBRO_ID = '201608230043' --2016-08-26 21:41:59.100, 好好吃實驗室 5
	select * from TBLOG_VIDEO where FSBRO_ID = '201608230043' --G201243900050010, R, 00002X7Z

	select * from TBTRACKINGLOG where FDTIME between '2016-08-01 14:00' and '2016-09-09 20:00' 
	and (FSMESSAGE like '%G201243900050010%' or FSMESSAGE like '%142322%' or FSMESSAGE like '%002X7Z%') order by FDTIME

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002X7Z' --2016-08-26 21:41:57.407 -> O

	--201609101500
	--update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201243900050010'
	--update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201243900050010'

	--2016-09-10 15:27:36.810 開始轉檔, 2016-09-10 16:04:48.293 轉檔完成, 2016-09-10 16:20:56.147 入庫完成
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-09-10' 
	and (FSMESSAGE like '%G201243900050010%' or FSMESSAGE like '%1423547%' or FSMESSAGE like '%002X7Z%') order by FDTIME

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) between '2016-08-01' and '2016-09-15'
	and (FSMESSAGE like '%G201243900050010%' or FSMESSAGE like '%1423547%' or FSMESSAGE like '%142322%' or FSMESSAGE like '%002X7Z%') order by FDTIME

end

if @case = 201609091756 begin --study TBTRACKINGLOG for 檔案送播轉檔. 21 information
	--根據轉檔任務 ID, Anystream ID, TSM ID, Video ID
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-09-09' 
	and (FSMESSAGE like '%G201558202040001%' or FSMESSAGE like '%143439%' or FSMESSAGE like '%95032%' or FSMESSAGE like '%189378%' or FSMESSAGE like '%002YYD%') order by FDTIME
end

----------------------------------------------20160910------------------------------------
if @case = 201609101059 begin -- 調用劇照
	select * from TBPROG_M where FSPGMNAME = '就是愛運動' --2010113
	select FSFILE_NO + ',' +  FSTITLE +',' + FSFILE_PATH + ',' + FSFILE_TYPE from TBLOG_PHOTO where FSID = '2010113' and FNEPISODE between 14 and 37
end

if @case = 201609101539 begin
	select * from TBPROG_M where FSPGMNAME = '感恩故事集' --2007456
	select * from TBLOG_VIDEO where FSID = '2007456' and FNEPISODE = 89 --G200745600890011

	--exec SP_U_TBLOG_VIDEO_FSTRACK 'G200745600890011','MMMM', '51204' --sa
	Update TBLOG_VIDEO Set FSTRACK = 'MMMM'
		where FSFILE_NO = 'G200745600890011'
end

if @case = 201609101711 begin -- 片庫轉檔檔案 TC 不對
	--dur = 1:32:45;00
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00001LAQ' --\\mamstream\MAMDFS\ptstv\HVideo\2014\2014936\G201493600010008.mxf  dur = 21
	select * from TBPROG_M where FSPROG_ID = '2014936' --春鬥2014(2-1)
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00001LAQ' --scheduleed on 2016-09-19
	select * from TBLOG_VIDEO where FSID = '2014936' --00002Z3X, 38.9GB
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002Z3X' --scheduled on 0919
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002Z3X'
end

if @case = 201609101733 begin -- 0525 play but no QC approved, so the FCSTATUS still on N
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002IS7' --2016-05-23 11:39:54.473 -> N
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002IS7'
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002IS7' --2016-09-19
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002IS7' order by FDDATE --0525 play on ch 12
end

if @case = 201609101807 begin  -- 刪除轉檔單
	select * from TBLOG_VIDEO where FSBRO_ID = '201605200159' --20160500522, 一把青 DVD PR (設計版) 原價  SD
	select * from TBLOG_VIDEO where FSBRO_ID = '201605130049' --20160500324, 一把青 DVD PR (導演版) 原價 SD, delete
	select * from TBLOG_VIDEO where FSBRO_ID = '201602220092' --20160200404, 燦爛時光 DVD promo (原價版) HD, B
	select * from TBPGM_PROMO where FSPROMO_ID in ('20160500522', '20160500324', '20160200404') 
end

-----------------------------------20160912-------------------------------------
if @case = 201609120910 begin
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = CAST(CURRENT_TIMESTAMP - 1 as DATE) and FSCATALOG = 'ERROR' order by FDTIME
	select * from TBTRAN_LOG where CAST(FDDATE as DATE) = CAST(CURRENT_TIMESTAMP - 1 as DATE) order by FDDATE
	select * from TBTRANSCODE_RECV_LOG where CAST(FDTIME as DATE) = CAST(CURRENT_TIMESTAMP - 1 as DATE) order by FDTIME
end

if @case = 201609121347 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002XVF' --2016-09-05 17:27:22.013 -> O
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002XVF' --2016-09-05 17:27:23.737 -> , G200928503380005, 2016-09-12 15:38 -> S
	select * from TBBROADCAST where FSBRO_ID = '201608290037' --FDTRAN_DATE = 2016-09-05 17:27:23.737
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-09-05' and (FSMESSAGE like '%002XVF%' or FSMESSAGE like '%G200928503380005%') order by FDTIME
	select * from TBPROG_M where FSPROG_ID = '2009285' --爸媽囧很大

	--update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G200928503380005'
	--update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G200928503380005'

	--2016-09-12 15:38:00.960 開始轉檔, 2016-09-12 16:23  轉檔完成, 2016-09-12 16:48:40.870 入庫完成
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-09-12'
	and (FSMESSAGE like '%G200928503380005%' or FSMESSAGE like '%143647%' or FSMESSAGE like '%002XVF%' or FSMESSAGE like '%95224%') order by FDTIME

end

if @case = 201609131639 begin --其他檔案建檔失敗，哲安在 Firefox 上建會失敗，在 IE 建 ok
	select top 100  * from TBTRAN_LOG where CAST(FDDATE as DATE)  = '2016-09-13' order by FDDATE desc
	select *  from TBLOG_VIDEO where FSFILE_NO = 'G201323200010064'
	select * from TBLOG_VIDEO where CAST(FDCREATED_DATE as DATE) = '2016-09-13' and FSARC_TYPE = '026'  -- 其他影片
	select v.FSFILE_NO, b.FSSIGNED_BY_NAME, b.* from TBBROADCAST b left join TBLOG_VIDEO v on b.FSBRO_ID = v.FSBRO_ID  where CAST(b.FDCREATED_DATE as DATE) = '2016-09-13' order by b.FDCREATED_DATE
	/*
	exec SP_D_TBBROADCAST_AND_VIDEO_SEG '201609130087'
	exec SP_D_TBBROADCAST_AND_VIDEO_SEG '201609130088'
	exec SP_D_TBBROADCAST_AND_VIDEO_SEG '201609130090'
	exec SP_D_TBBROADCAST_AND_VIDEO_SEG '201609130098'
	*/

	select * from TBFILE_TYPE
end

if @case = 201609130636 begin
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002YS9' --201609070107, G201668001720001
	select * from TBBROADCAST where FSBRO_ID = '201609070107' --FDTRAN_DATE = 2016-09-08 12:05:11.367
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-09-08'
	and (FSMESSAGE like '%143329%' or FSMESSAGE like '%189137%' or FSMESSAGE like '%94917%' or FSMESSAGE like '%G201668001720001%') order by FDTIME
end

-------------------------------------20160914--------------------------------------
if @case = 201609140855 begin --其他檔案建立失敗 todo
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = CAST(CURRENT_TIMESTAMP as DATE) order by FDTIME
	select * from TBTRAN_LOG where FDDATE between '2016/09/14 08:50' and '2016/09/14 08:55' order by FDDATE
    select * from TBUSERS where FSUSER_ID = '71355' --郭哲安

	select * from TBLOG_VIDEO where FSARC_TYPE = '026' and CAST(FDCREATED_DATE as DATE) = '2016-09-13' --201609130099, 201609130100, 201609130095, FCFILE_STATUS = X
	select * from TBLOG_VIDEO where FSARC_TYPE = '026' and CAST(FDCREATED_DATE as DATE) = '2016-09-14' --8 支 B -> T
	select * from TBLOG_VIDEO where FSCREATED_BY = '71355' and FSARC_TYPE = '026'
	
	--下課花路米 1491
	select * from TBLOG_VIDEO where FNEPISODE = 1491 and FSARC_TYPE = '026'  -- no record
	select * from TBBROADCAST where FSBRO_ID = '201609140002' --delete this item, 2016-09-14 08:51:01.370, created by 71355
	--exec SP_D_TBBROADCAST_AND_VIDEO_SEG '201609140002'
	select * from TBTRACKINGLOG where FDTIME between '2016-09-14 08:45' and '2016-09-14 08:59' order by FDTIME --一次動作送帶轉檔寫入TBLOG_VIDEO資料表發生錯誤
end

if @case = 201609141105 begin --Anystream 轉檔成功 MAM 未收到回報 
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201718400010001' --201608160038, 00002WDY, DUR = 01:38:15;00
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002WDY' --9/18
	select * from TBBROADCAST where FSBRO_ID = '201608160038' --FDTRAN_DATE 2016-09-13 15:12:01.190
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-09-13' and (FSMESSAGE like '%G201718400010001%' or FSMESSAGE like '%143707%' or FSMESSAGE like '%189931%' or FSMESSAGE like '%95284%' ) order by FDTIME
	--Anystream reshcedule job 95284
	select * from TBTRANSCODE_RECV_LOG where FNJOB_ID = 95284 --RECEIEVE TIME = 2016-09-14 16:49:01.000
end

if @case = 201609141139 begin --音軌只有三軌
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002ZBW' --50309, 201609130062
	select * from TBUSERS where FSUSER_ID = '50309' --林步青
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002ZBW'
	select * from TBUSERS where FSUSER_ID = '70062' --盧厚樺
end

if @case = 201609141419 begin -- 檔案在 18T/IngestWorkingTemp/ 可能是  HSM5 看不到 18T
	select * from TBTRACKINGLOG where FDTIME between '2016-09-13 18:20' and '2016-09-13 19:50' order by FDTIME
	select * from TBBOOKING_MASTER where FSUSER_ID = '51344' --201609050098
	select * from TBUSERS where FSUSER = 'cc17921' --林貞吟, 51344
	select * from TBBOOKING_DETAIL where FSBOOKING_NO = '201609050098'
end

if @case = 201609141733 begin -- 檔案長度問題，goto 會少一個 frame
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002ZEH' --201609140026, G201727600010002, 01:00:00;00-> 02:48:40;00
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002ZEH' order by FDDATE --9/19 07:00:00;00, ch 07
end

if @case = 201609141023 begin
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002VSE' --P201608002880001, T
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002VSE' --not schedule, but it appear in queue for Review -> Approved

	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002ZE8' --201609140016
	select * from TBBROADCAST where FSBRO_ID = '201609140016' -- FDTRAN_DATE = 2016-09-14 20:35
end


if @case = 201609190941 begin
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = CAST(CURRENT_TIMESTAMP - 1 as DATE) and FSCATALOG = 'ERROR' order by FDTIME
	select * from TBTRAN_LOG where CAST(FDDATE as DATE) = CAST(CURRENT_TIMESTAMP - 1 as DATE) order by FDDATE
end

if @case = 201609191211 begin
	select * from TBZPROGSPEC
	select * from TBZSHOOTSPEC
end

if @case = 201609201352 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002ZID'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002ZID'

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002ZUM'
	select * from TBUSERS where FSUSER = 'hsincheng' --鄭文欣 HD partial 調用
end

if @case = 201609210923 begin
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = CAST(CURRENT_TIMESTAMP - 1 as DATE) and FSCATALOG = 'ERROR' order by FDTIME
end

if @case = 201609211330 begin -- 查XML, SOM
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002HY9' ----G201689600010003
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G201689600010003'
end

if @case = 201609211436 begin
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002ZT8' --201609190087, P201609003510003, FSCHANGE_FILE_NO = P201609003510001?
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002ZT5' --送帶轉檔置換單，P201609003510002, 201609190081, FSID = 20160900351, PTS1主題之夜PROMO誰讓你憂鬱, B?
	select * from TBBROADCAST where FSBRO_ID = '201609190081' 
	select * from TBLOG_VIDEO where FSFILE_NO = 'P201609003510001' --D, 00002ZE8, 201609140016, P201609003510001 送播單
	select * from TBPGM_PROMO where FSPROMO_ID = '20160900351'
	select * from TBLOG_VIDEO where FSFILE_NO = 'P201609003510001' --D, 00002ZE8 -> replaced by 00002ZT5
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002ZT8' order by FDDATE --PTS1主題之夜PROMO誰讓你憂鬱, 2016-09-22 05:12:49:28
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002ZT5'
end

if @case = 201609211748 begin -- 刪除送帶轉檔單
	select * from TBBROADCAST where FSBRO_ID = '201601050083'
	select * from TBBROADCAST where FSBRO_ID = '201601050081'
	select * from TBLOG_VIDEO where FSBRO_ID in ('201601050083', '201601050081')
	select * from TBPROG_M where FSPROG_ID = '2016623' --山歌浪想 當代山歌音樂會(60分)
	select * from TBPROG_M where FSPROG_ID = '2016625' --向賴碧霞致敬 傳統山歌音樂會(60分)

	--exec SP_D_TBBROADCAST_AND_VIDEO_SEG '201601050083'
	--exec SP_D_TBBROADCAST_AND_VIDEO_SEG '201601050081'

	select top 100 * from TBTRACKINGLOG order by FDTIME desc
	select top 100 * from TBTRAN_LOG order by FDDATE desc
end

if @case = 201609220916 begin  --study TBTSM_JOB
	select * from TBTRAN_LOG where CAST(FDDATE as DATE) = CAST(CURRENT_TIMESTAMP - 1 as DATE) order by FDDATE
	/*
	G20160920.log
	2016/9/20 上午 12:01:46 : 開始取得檔案:\\mamstream\MAMDFS\hakkatv\HVideo\2009\2009299\G200929905420002.mxf:在TSM狀態
	2016/9/20 上午 12:01:58 : 檔案:\\mamstream\MAMDFS\hakkatv\HVideo\2009\2009299\G200929905420002.mxf:在TSM狀態為[1]需要呼叫TSM-ReCall
	2016/9/20 上午 12:01:58 : 檔案:\\mamstream\MAMDFS\hakkatv\HVideo\2009\2009299\G200929905420002.mxf:呼叫TSM-ReCall,JOBID=190662
	2016/9/20 上午 12:07:54 : 檔案:\\mamstream\MAMDFS\hakkatv\HVideo\2009\2009299\G200929905420002.mxf:在TSM狀態為[0]可以開始複製
	2016/9/20 上午 02:10:34 : 開始取得檔案:\\mamstream\MAMDFS\hakkatv\HVideo\2009\2009299\G200929905420002.mxf:在TSM狀態
	2016/9/20 上午 02:10:41 : 檔案:\\mamstream\MAMDFS\hakkatv\HVideo\2009\2009299\G200929905420002.mxf:在TSM狀態為[1]需要呼叫TSM-ReCall
	2016/9/20 上午 02:10:41 : 檔案:\\mamstream\MAMDFS\hakkatv\HVideo\2009\2009299\G200929905420002.mxf:呼叫TSM-ReCall,JOBID=190675
	2016/9/20 上午 02:15:04 : 檔案:\\mamstream\MAMDFS\hakkatv\HVideo\2009\2009299\G200929905420002.mxf:在TSM狀態為[0]可以開始複製
	2016/9/20 上午 02:18:15 : 檔案:\\mamstream\MAMDFS\hakkatv\HVideo\2009\2009299\G200929905420002.mxf:在TSM狀態為[1]需要呼叫TSM-ReCall
	2016/9/20 上午 02:18:15 : 檔案:\\mamstream\MAMDFS\hakkatv\HVideo\2009\2009299\G200929905420002.mxf:呼叫TSM-ReCall,JOBID=190676
	2016/9/20 上午 02:19:17 : 檔案:\\mamstream\MAMDFS\hakkatv\HVideo\2009\2009299\G200929905420002.mxf:在TSM狀態為[0]可以開始複製
	2016/9/20 上午 02:19:25 : 開始複製檔案:\\mamstream\MAMDFS\hakkatv\HVideo\2009\2009299\G200929905420002.mxf:到待播區
	2016/9/20 上午 02:25:00 : 檔案:\\mamstream\MAMDFS\hakkatv\HVideo\2009\2009299\G200929905420002.mxf:拷貝到待播區完成
	*/

	select top 100 * from TBTSM_JOB where CAST(FDREGISTER_TIME as DATE) = '2016-09-21' order by FDREGISTER_TIME --100 JOB
	select * from TBTSM_JOB where CAST(FDREGISTER_TIME as DATE) = '2016-09-21' and FSSOURCE_PATH like '%mxf%' order by FDREGISTER_TIME --74 從GPFS 搬檔

	select distinct FSSOURCE_PATH, FNSTATUS, FSNOTIFY_ADDR from TBTSM_JOB where CAST(FDREGISTER_TIME as DATE) = '2016-09-21' and FSSOURCE_PATH like '%mxf%' --69
	--190911, 190913 FSNOTIFY_ADDR = http://mam.pts.org.tw/WS/WSASC/Handler_TsmNotifyReceiver.ashx  調用？

	select distinct FSSOURCE_PATH, FNSTATUS, FSNOTIFY_ADDR from TBTSM_JOB
	where CAST(FDREGISTER_TIME as DATE) = '2016-09-21' and (FSSOURCE_PATH like '%mxf%' and FSSOURCE_PATH like '%G%') --40 支節目

	select FSSOURCE_PATH, FNSTATUS, FSNOTIFY_ADDR from TBTSM_JOB
	where CAST(FDREGISTER_TIME as DATE) = '2016-09-21' and (FSSOURCE_PATH like '%mxf%' and FSSOURCE_PATH like '%G%') group by FSSOURCE_PATH, FNSTATUS, FSNOTIFY_ADDR --40

	select FSSOURCE_PATH, count(FSSOURCE_PATH) RESEND from TBTSM_JOB
	where CAST(FDREGISTER_TIME as DATE) = '2016-09-21' and (FSSOURCE_PATH like '%mxf%' and FSSOURCE_PATH like '%G%') group by FSSOURCE_PATH, FNSTATUS, FSNOTIFY_ADDR

	select * from TBTRANSCODE_RECV_LOG where CAST(FDTIME as DATE) between '2016-09-19' and '2016-09-22' and FSCONTENT like '%190913%'

	--G200929905420002 從片庫調檔案
	select top 100 * from TBTSM_JOB where CAST(FDREGISTER_TIME as DATE) between '2016-09-19' and '2016-09-22' and FSSOURCE_PATH like '%G200929905420002%' 	order by FDREGISTER_TIME

end

if @case = 201609221407 begin --成案查詢
	select p.* from TBPROPOSAL p join TBUSERS u on p.FSCREATED_BY = u.FSUSER_ID where u.FSUSER_ChtName = '張文嘉' order by p.FDCREATED_DATE
	select p.* from TBPROPOSAL p join TBUSERS u on p.FSCREATED_BY = u.FSUSER_ID where u.FSUSER_ChtName = '林喬屏' order by p.FDCREATED_DATE --9
	select p.* from TBPROPOSAL p join TBUSERS u on p.FSCREATED_BY = u.FSUSER_ID where p.FSCHANNEL_ID like '%07%' order by p.FDCREATED_DATE --911
	select * from TBPROPOSAL where CAST(FDCREATED_DATE as DATE) between '2016-08-01' and '2016-09-22' and FSCHANNEL_ID like '%07%' order by FDCREATED_DATE --24
end

if @case = 201609221449 begin
	select * from TBZCHANNEL
end

if @case = 201609221509 begin
	select * from TBUSERS where FSUSER like '%chun3535%'
	select * from TBUSERS where FSUSER_ID = '51358'  -- FSUSER = ''  林阡郡  2016-09-22 03:08:02.357 created
	select * from TBUSERS where FSUSER like '%hsing1%'  --2016-09-22 03:08:02.283 每天同步
end

if @case = 201609140855-201609231103 begin
	select * from TBLOG_VIDEO where FNEPISODE = 1491 and FSARC_TYPE = '026'  -- no record
	select * from TBBROADCAST where FSBRO_ID = '201609140002' --delete this item, 2016-09-14 08:51:01.370, created by 71355
	--exec SP_D_TBBROADCAST_AND_VIDEO_SEG '201609140002'
	select * from TBTRACKINGLOG where FDTIME between '2016-09-23 11:00' and '2016-09-23 12:59' and FSMESSAGE like '%一次動作%' order by FDTIME --一次動作送帶轉檔寫入TBLOG_VIDEO資料表發生錯誤
	select * from TBBROADCAST where CAST(FDCREATED_DATE as DATE) = '2016-09-22' and FSCREATED_BY = '71355'  order by FDCREATED_DATE
	select * from TBUSERS where FSUSER_ChtName = '郭哲安' --71355

	select * from TBBROADCAST where FSBRO_ID = '201609230027' -- FSBRO_TYPE = G, FSID = 0000816, 1491
	select * from TBBROADCAST where FSID = '0000816' and FNEPISODE = 1491 --201609190096, 201609230027
	select * from TBLOG_VIDEO where FSBRO_ID = '201609230027' --no record
	select * from TBLOG_VIDEO where FSID = '0000816' and FNEPISODE = 1491
	exec SP_D_TBBROADCAST_AND_VIDEO_SEG '201609190096'
	exec SP_D_TBBROADCAST_AND_VIDEO_SEG '201609230027'
	select * from TBTRACKINGLOG where FDTIME between '2016-09-23 11:00' and '2016-09-23 12:59' order by FDTIME
end

if @case = 201609231151 begin --Support export PTS2 MOD EPG

	--PTS MOD
	select A.PAENO FSPROGID,A.PSDAT FDDATE,A.PSTME FSBTIME,B.FSWEBNAME,A.PBENO FNEPISODE,B.FNTOTEPISODE
		from OPENQUERY(PTS_PROG_MYSQL, 'SELECT  * FROM programX.PSHOW_PTS_MOD where PSDAT>=sysdate()
		and PSDAT<= sysdate()+ interval 14 day') A,TBPROG_M B
		where A.PAENO=B.FSPROG_ID order by A.PSDAT,A.PSTME

	--PTS DIMO
	select A.PAENO FSPROGID,A.PSDAT FDDATE,A.PSTME FSBTIME,B.FSWEBNAME,A.PBENO FNEPISODE,B.FNTOTEPISODE
		from OPENQUERY(PTS_PROG_MYSQL, 'SELECT  * FROM programX.PSHOW_DIMO where PSDAT>=sysdate()
		and PSDAT<= sysdate()+ interval 14 day') A,TBPROG_M B
		where A.PAENO=B.FSPROG_ID order by A.PSDAT,A.PSTME

	select top 100 A.* from OPENQUERY(PTS_PROG_MYSQL, 'SELECT  * FROM programX.PSHOW_DIMO order by PSDAT desc') A
	select * from  OPENQUERY(PTS_PROG_MYSQL, 'SELECT  sysdate()') A -- 2016-09-23 12:12:33

	select * from OPENQUERY(PTS_PROG_MYSQL, 'select * from programX.CHANNEL')
	select * from TBZCHANNEL

	select distinct CHEID from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA') -- DIMO, PTS, PTS_MOD, HD_MOD, HAKA, HD ...
	select top 1000 * from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA')
	select top 1000 * from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA WHERE CHEID = "DIMO"')
	select top 1000 * from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA WHERE CHEID = "DIMO" and PRSDT <= ?? and PREDT >= '' and PRTMS <= ''and PRTME >= '' and ??? = 1 order by PRENO DESC')

end



if @case = 201609261030 begin --台灣親 NAG, NAG 查不到 6-13 集送播單
	select * from TBLOG_VIDEO where FSID = '2010742' and FNEPISODE in (6, 7, 8, 9, 10, 11, 12, 13) and FSARC_TYPE = '002'
end


if @case = 201609291003 begin --標記上傳後如果  QC 尚未審核通過無法置換先在 DB 修改
	select * from TBLOG_VIDEO where FSBRO_ID = '201609280005' --P201609006310001, 000030FZ
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'P201609006310001'
	select * from TBLOG_VIDEO_SEG_CHG where FSFILE_NO = 'P201609006310001' --no recoder

	--update TBLOG_VIDEO set FSBEG_TIMECODE = '00:00:00;00', FSEND_TIMECODE = '00:00:30;00', FSUPDATED_BY= '51204', FDUPDATED_DATE=GETDATE()
	--where FSFILE_NO = 'P201609006310001'

	--exec SP_U_TBLOG_VIDEO_SEG_BYFILEID 'P201609006310001', '01', '00:00:00;00', '00:00:30;00', '51204'
end

if @case = 201609292053 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000030ND' --2016-09-29 20:35:50.470 O
end

if @case = 201609300910 begin  -- SD 資料帶 anystream 轉檔完成，MAM 仍在轉檔中
	select * from TBLOG_VIDEO where FSFILE_NO = 'D201609000060003'  --S, 201609230016
	exec SP_U_TBLOG_VIDEO_FILE_STATUS 'D201609000060003', 'R', '51204'

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-09-23'
		and (FSMESSAGE like '%D201609000060003%' or FSMESSAGE like '%144324%' or FSMESSAGE like '%191257%' or FSMESSAGE like '%95887%')  order by FDTIME

	select * from TBTRACKINGLOG where FDTIME between '2016-09-23 10:50' and '2016-09-23 11:30' order by FDTIME

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-09-30' 
		and (FSMESSAGE like '%D201609000060003%' or FSMESSAGE like '%144324%' or FSMESSAGE like '%191257%' or FSMESSAGE like '%95887%')  order by FDTIME
end

if @case = 201609301154 begin
	select count (distinct pa.FSPLAYTIME) as  播出次數 , p.FSWELFARE as 公益託播單號 , p.FSPROMO_NAME  as 短帶名稱  
		from TBPGM_ARR_PROMO pa , TBPGM_PROMO p where pa.FSPROMO_ID= p.FSPROMO_ID and p.FSWELFARE<>'' group by p. FSWELFARE, p .FSPROMO_NAME  -- 98

	select count (pa.FSPLAYTIME) as  播出次數 , p. FSWELFARE as 公益託播單號 , p.FSPROMO_NAME  as 短帶名稱  
		from TBPGM_ARR_PROMO pa , TBPGM_PROMO p where pa.FSPROMO_ID= p.FSPROMO_ID and p.FSWELFARE<>'' group by p.FSWELFARE, p.FSPROMO_NAME --98

	select p.FSPROMO_NAME 短帶名稱, p.FSWELFARE 公益託播單號 ,pa.FDDATE 播出日期, pa.FSPLAYTIME 播出時間 
		from TBPGM_ARR_PROMO pa join TBPGM_PROMO p on pa.FSPROMO_ID= p.FSPROMO_ID where p.FSWELFARE <> '' and pa.FSCHANNEL_ID = '07' order by pa.FDDATE, pa.FSPLAYTIME
end

if @case = 201609301539 begin
	select top 100 * from TBTRACKINGLOG order by FDTIME desc
end

if @case = 201610041148 begin --1~18 走送帶轉檔 20 ~ 走檔案送播，刪除 19 及送播單，改填回朔
	select * from TBPROG_M where FSPGMNAME = '汪達的外星朋友' --2016771
	select * from TBLOG_VIDEO where FSID = '2016771' and FNEPISODE = 19 --G201677100190006, 執行手動刪除 F-> X
	select * from TBLOG_VIDEO where FSID = '2016771' and FNEPISODE = 18
	select * from TBLOG_VIDEO where FSID = '2016771' and FNEPISODE = 20
	select top 100 * from TBTRACKINGLOG order by FDTIME desc
	select top 100 * from TBTRAN_LOG order by FDDATE desc
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G201677100190006'
	select * from TBBROADCAST where FSBRO_ID = '201604130081'
end

if @case = 201610051201 begin
	select * from TBBROADCAST where FSBRO_ID = '201606070125' --紀錄觀點第469集
	select * from TBLOG_VIDEO where FSBRO_ID = '201606070125' --G201704200010002, B
	select * from TBLOG_VIDEO where FSID = '2017042'
end

if @case = 201610071357 begin
	select * from TBPGM_QUEUE where FSCHANNEL_ID = 12 and FDDATE = '2016-10-09' order by FSBEG_TIME
	select * from TBZCHANNEL
	select * from TBPROG_D where FSPROG_ID = '0000816' and FNEPISODE = 1353 --下課花路米 
	--FSMEMO = 1019,1020集內容有爭議第十四季(794-857集),第十五季(858-921集)第880,881集重播需告知對方重播時間第十六季922-993集第18季1058-1119集,第19季1120-1179集,第21季1230-1279集
	select A.PAENO FSPROGID,A.PSDAT FDDATE,A.PSTME FSBTIME,B.FSWEBNAME,A.PBENO FNEPISODE,B.FNTOTEPISODE
		from OPENQUERY(PTS_PROG_MYSQL, 'SELECT  * FROM programX.PSHOW_PTS where PSDAT>=sysdate()
		and PSDAT<= sysdate()+ interval 14 day') A,TBPROG_M B
		where A.PAENO=B.FSPROG_ID order by A.PSDAT,A.PSTME

	select * from TBPGM_BANK_DETAIL where FSCHANNEL_ID = '12' and FDBEG_DATE = '2016-10-09' order by FSBEG_TIME 
end

if @case = 201610111119 begin
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-10-11'
		and (FSMESSAGE like '%D201609000060003%' or FSMESSAGE like '%145139%' or FSMESSAGE like '%G200928503900024%')  order by FDTIME  --轉檔任務145139重置，原 SubID =193085/143935/96697
	--143935??
	select top 100 * from TBTRANSCODE order by FDSTART_TIME desc
	select * from TBTRANSCODE where FNTRANSCODE_ID = 143935  order by FDSTART_TIME --節目名稱：(2012313)在河左岸 ，節目編號：2012313 ，集別：9 ，檔案類型：HD播出帶 ，檔案編號：G201231300090002
	select * from TBTRANSCODE where FNTRANSCODE_ID = 145139  order by FDSTART_TIME --{節目名稱：爸媽囧很大 ，節目編號：2009285 ，集別：390 ，檔案類型：HD送播(QC)，檔案編號：G200928503900024}
	select * from TBTRANSCODE where  FNTSMDISPATCH_ID = 193085 or FNANYSTREAM_ID = 96697 order by FDSTART_TIME --no record
end

if @case = 201610111628 begin
	select count (distinct pa.FSPLAYTIME) as  播出次數 , p.FSWELFARE as 公益託播單號 , p.FSPROMO_NAME  as 短帶名稱  
		from TBPGM_ARR_PROMO pa , TBPGM_PROMO p where pa.FSPROMO_ID= p.FSPROMO_ID and p.FSWELFARE<>'' group by p. FSWELFARE, p .FSPROMO_NAME  -- 98

	select count (pa.FSPLAYTIME) as  播出次數 , p. FSWELFARE as 公益託播單號 , p.FSPROMO_NAME  as 短帶名稱  
		from TBPGM_ARR_PROMO pa , TBPGM_PROMO p where pa.FSPROMO_ID= p.FSPROMO_ID and p.FSWELFARE<>'' group by p.FSWELFARE, p.FSPROMO_NAME --98

	select p.FSPROMO_NAME 短帶名稱, p.FSWELFARE 公益託播單號 ,pa.FDDATE 播出日期, pa.FSPLAYTIME 播出時間 
		from TBPGM_ARR_PROMO pa join TBPGM_PROMO p on pa.FSPROMO_ID= p.FSPROMO_ID where p.FSWELFARE <> '' and pa.FSCHANNEL_ID = '07' order by pa.FDDATE, pa.FSPLAYTIME

	select p.FSPROMO_NAME 短帶名稱, p.FSWELFARE 公益託播單號 ,pa.FDDATE 播出日期, pa.FSPLAYTIME 播出時間 
		from TBPGM_ARR_PROMO pa join TBPGM_PROMO p on pa.FSPROMO_ID= p.FSPROMO_ID where p.FSWELFARE <> '' and pa.FSCHANNEL_ID = '07' 
		and p.FSPROMO_NAME = '2016公益-接種流感疫苗即刻行動'  order by pa.FDDATE, pa.FSPLAYTIME
end


if @case = 201610041148 begin --1~18 走送帶轉檔 20 ~ 走檔案送播，刪除 19 及送播單，改填回朔
	select * from TBPROG_M where FSPGMNAME = '汪達的外星朋友' --2016771
	select * from TBLOG_VIDEO where FSID = '2016771' and FNEPISODE = 19 --G201677100190006, 執行手動刪除 F-> X
	select * from TBLOG_VIDEO where FSID = '2016771' and FNEPISODE = 18
	select * from TBLOG_VIDEO where FSID = '2016771' and FNEPISODE = 20
	select top 100 * from TBTRACKINGLOG order by FDTIME desc
	select top 100 * from TBTRAN_LOG order by FDDATE desc
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G201677100190006'
	select * from TBBROADCAST where FSBRO_ID = '201604130081'


	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002HL3' --G201677100190006:X G201677100190007:T 回朔單
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002HL3'
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002HL3' and FDDATE = '2016-10-17' --G201677100190007 插入短帶抓的是回朔單
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002HL3' --有兩筆資料
	select FSVIDEO_ID, count(FSFILE_NO) from TBLOG_VIDEO_HD_MC group by FSVIDEO_ID having count(FSFILE_NO) > 1 --00001SKS, 00002HL3
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00001SKS'
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00001SKS'
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00001SKS' order by FDDATE
	select * from TBZCHANNEL

	select * from TBLOG_QC_RESULT where FSVIDEO_ID = '002HL3'
end


if @case = 201610181039-201607261553-201607211158 begin --鬧熱打擂台(120分鐘版) 未抓到 SD ID
	select * from TBLOG_VIDEO where FSID = '2016527' and FNEPISODE = 17 --00002S2N
	select  t.FSBRO_ID, t.FSFILE_NO, t.FSARC_TYPE, pgmq.* from TBPGM_COMBINE_QUEUE pgmq left join TBLOG_VIDEO t on pgmq.FSVIDEO_ID = t.FSVIDEO_PROG  where pgmq.FSPROG_ID = '2016527' and pgmq.FNEPISODE = 17 --00002TTR
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002S2N'  --C 2016-07-21 15:26:56.403

	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002TTR' --no record
end

if @case = 201610201130 begin --無關鍵影格
	select * from TBPROG_M where FSPGMNAME like '%變心%' --2013681, 2013682
	select * from TBLOG_VIDEO where FSID = '2013682' and FNEPISODE = 1 --G201368200010003, T, 201511020016, FCKEYFRAME = R -> Y
	select * from TBBROADCAST where FSBRO_ID = '201511020016' --2016-10-19 12:35:45.613
	select * from TBLOG_VIDEO where FSID = '2013681' and FNEPISODE = 1 --FCKEYFRAME = Y
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201368200010003' --T
	select top 100 * from TBLOG_VIDEO order by FDCREATED_DATE desc

	select * from TBPROG_M where FSPGMNAME like '%宋宮秘史%' --2013483
	select * from TBLOG_VIDEO where FSID = '2013483' --G201348300010003  --2016-10-19 17:13 完成

	update TBLOG_VIDEO set FCKEYFRAME = 'N' where FSFILE_NO = 'G201368200010003'
end

if @case = 201610211618 begin --10/20 刪除 disk 及 DB 檔案又被搬回去
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in ('000031KX', '00002UPB', '00002SE1', '00002WPB')
end

if @case = 201610211733 begin --not louth data
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000032KR' --2016-10-19 14:04:38.963 : O
	select * from TBUSERS where FSUSER_ChtName = '黃安慈' --51063
	select * from TBLOG_VIDEO_HD_MC where FSUPDATED_BY = '51063' and CAST(FDUPDATED_DATE as DATE) = '2016-10-19' and FCSTATUS = 'O' order by FDUPDATED_DATE

	--  10/19 審核 14 支檔案，其中只有 0032KR 未寫入 Louth DB
	--select-string -path LOUTH2016*.log -pattern "0032KR"  --> not found

end




if @case = 201610260942 begin  --有檔案無 louth DB
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in ('000031XD', '000032HE', '000031WY', '000032FS', '000031WV', '000032FV', '000032FX', '000031WO',
	'000032FY', '000031WS', '000032G3', '000032G4', '000031WG', '000032G7', '00001VPB', '000030XU', '000030KU')

	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID in ('000031XD', '000032HE', '000031WY', '000032FS', '000031WV', '000032FV', '000032FX', '000031WO',
	'000032FY', '000031WS', '000032G3', '000032G4', '000031WG', '000032G7', '00001VPB', '000030XU', '000030KU')

	select * from TBPGM_ARR_PROMO where FSVIDEO_ID in ('000031XD', '000032HE', '000031WY', '000032FS', '000031WV', '000032FV', '000032FX', '000031WO',
	'000032FY', '000031WS', '000032G3', '000032G4', '000031WG', '000032G7', '00001VPB', '000030XU', '000030KU')

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002STK', '00002UL4')
	--00002UL4 Louth20161011.log, G201654600030004, G20161011.log 有搬檔紀錄
	--002STK G201549600510004, scheduled on 2016-10-26
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002UL4' order by FDDATE
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002STK' order by FDDATE
end

if @case = 201610271012 begin --送播單未建完整
	select * from TBPROG_M where FSPGMNAME like '%食在料理王%' --2015809
	select * from TBLOG_VIDEO where FSID = '2015809' and FNEPISODE = 197--201610040042, 0000314D, G201580901970002
	select * from TBBROADCAST where FSBRO_ID = '201610040042'

	exec SP_D_TBLOG_VIDEO_BYBROID '201610040042' --using sa
	exec SP_D_TBLOG_VIDEO_SEG_BYFILEID 'G201580901970002'
end

if @case = 201610271536 begin
	--查詢要從 HDUpload 搬到 Review 的清單
	exec SP_Q_GET_MOVE_PENDING_LIST_MIX  -- 未照播出時間排序


	exec SP_Q_GET_CHECK_COMPLETE_LIST_MIX  --1163?

end

if @case = 201610281112 begin --SP_Q_GET_CHECK_COMPLETE_LIST_MIX
	select distinct A.FSPROG_ID as FSID,A.FNEPISODE,D.FSFILE_NO,D.FSVIDEO_ID,B.FCFILE_STATUS,C.FCCHECK_STATUS,D.FCSTATUS,C.FSBRO_ID
	from tbpgm_combine_queue A
	left join TBLOG_Video B on A.FSVIDEO_ID=B.FSVIDEO_PROG and B.FCFILE_STATUS not in('D','X','F')
	left join tbbroadcast C on B.FSBRO_ID=C.FSBRO_ID and C.FCCHECK_STATUS IN ('G','T','U')
	left join TBLOG_Video_HD_MC D on B.FSFILE_NO=D.FSFILE_NO and D.FCSTATUS='O'
	inner join TBZCHANNEL E on A.FSCHANNEL_ID=E.FSCHANNEL_ID and E.FSCHANNEL_TYPE='002'
	where 
	A.FDDATE>=getdate()-1 and A.FDDATE<=getdate()+1
	and B.FCFILE_STATUS in ('B','S','R','T','Y')
	and C.FCCHECK_STATUS IN ('G','T','U')
	and D.FCSTATUS='O'
end

if @case = 201611021802 begin 
	select * from TBPROG_M where FSPGMNAME like '%爸媽囧很大%' --2009285
	select * from TBLOG_VIDEO where FSID = '2009285' and FNEPISODE = 648 --G200928506480003 T, FCKEYFRAME Y,  201611020017
	--低解檔案有問題

	-- 如果改成 R 則傳態是轉檔成功搬檔失敗，會只有搬 18T 的檔案，所以改成待轉
	exec SP_U_TBLOG_VIDEO_FILE_STATUS 'G200928506480003', 'B', '51204'

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-11-03'
	and (FSMESSAGE like '%G200928506480003%' or FSMESSAGE like '%147160%' or FSMESSAGE like '%197233%' or FSMESSAGE like '%98555%')
	order by FDTIME
end

if @case = 201611030943 begin
	select * from TBPGM_PROMO where FSPROMO_ID = '20160800569' --2016紀錄片行動列車
	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID 
	join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20160800569' and FDDATE between '2016-08-25' and '2016-10-25' and A.FSCHANNEL_ID in ('12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	select * from TBPGM_PROMO where FSPROMO_ID = '20160900490' --2016紀錄片行動列車Promo2
	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID 
	join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20160900490' and FDDATE between '2016-08-25' and '2016-10-25' and A.FSCHANNEL_ID in ('12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	select * from TBZCHANNEL
end

if @case = 201611041343 begin
	--Parse: transcodeID = 147301 , jobStatus = SYSERROR
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-11-04'
	and (FSMESSAGE like '%G201489300020001%' or FSMESSAGE like '%147301%' or FSMESSAGE like '%197492%')
	order by FDTIME

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-11-04'
	and (FSMESSAGE like '%G201489300020001%' or FSMESSAGE like '%147301%' or FSMESSAGE like '%197492%' or FSCATALOG = 'ERROR')
	order by FDTIME

	select * from TBLOG_VIDEO where FSVIDEO_PROG = '000033QQ' --G201734900170002

	
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-11-04'
	and (FSMESSAGE like '%G201715100480001%' or FSMESSAGE like '%147329%' or FSMESSAGE like '%197520%' or FSMESSAGE like '%98719%')
	order by FDTIME

	--請編表 QC 一支短帶測試 QC 入庫卡在 TSM 處裡中，TSM 恢復後正常
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-11-04'
	and (FSMESSAGE like '%P201611001080001%' or FSMESSAGE like '%147330%' or FSMESSAGE like '%197521%')
	order by FDTIME


	select * from TBTRACKINGLOG where FDTIME between '2016-11-04 15:00' and '2016-11-04 16:00'
	and (FSMESSAGE like '%G201489300020001%' or FSMESSAGE like '%147353%' or FSMESSAGE like '%197544%' or FSCATALOG = 'ERROR')
	order by FDTIME

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-11-04'
	and (FSMESSAGE like '%G201489300020001%' or FSMESSAGE like '%147362%' or FSMESSAGE like '%197556%')
	order by FDTIME
end

if @case = 201611041423 begin -- 送播資料未建完成
	select * from TBLOG_VIDEO where FSID = '2007851' and FNEPISODE = 193 -- 201611030142
	select * from TBBROADCAST where FSBRO_ID = '201611030142'
	--exec SP_D_TBBROADCAST_AND_VIDEO_SEG '201611030142'
end

if @case = 201611041741 begin --TS3310 磁帶下架
	select  * from TBTRACKINGLOG where FDTIME > '2016-11-04 17:40' order by FDTIME
	select  * from TBTRACKINGLOG where FDTIME > '2016-11-04 18:05' order by FDTIME
end

if @case = 201611071128 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00001P1X'  --O 2016-06-21 16:50:01.240
end

if @case = 2016110801144 begin --GetMinPlayTime() 取得未來的播出時間
	select CONVERT(char(10), FDDATE, 20) FDDATE, FSPLAY_TIME FSPLAYTIME from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '000034G1' and FDDATE >= getdate() - 1 union all
	select CONVERT(char(10), FDDATE, 20) FDDATE, FSPLAYTIME from TBPGM_ARR_PROMO where FSVIDEO_ID = '000034G1'  and FDDATE >= getdate() - 1
	order by FDDATE, FSPLAY_TIME

	select top 1 FDDATE, FSPLAY_TIME from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '000034G1' and FDDATE >= getdate() - 1 union
	select top 1 FDDATE, FSPLAYTIME from TBPGM_ARR_PROMO where FSVIDEO_ID = '000034G1' and FDDATE >= getdate() - 1
end

if @case = 201611081515 begin
	select * from TBZCHANNEL
end

if @case = 201611091701 begin -- 無法置換
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '000034IU'--201611080072
end

if @case = 201611101002 begin
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00001S0V' order by FDDATE --2015817, 永恆的生命脈動幕後製作
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00001S0V' --G201581700010003
	--刪除檔案重新搬檔
	select * from TBTSM_JOB where FNTSMJOB_ID = 198189 --/ptstv/HVideo/2015/2015817/G201581700010003.mxf, FNSTATUS = 2  -> 3
	select * from TBTSM_JOB where FNTSMJOB_ID = 198190 --FNSTATUS = 3
	
	
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002PKY' --G201018000010016
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002PKY' order by FDDATE --出走的好理由
	
end

if @case = 201611101200 begin -- 建立短帶資料 託播單 送播單後 送播單抽單 無法再新增
	select * from TBLOG_VIDEO where FSID = '20161100244' --P201611002440001, 201611090010
	select * from TBBROADCAST where FSBRO_ID = '201611090010'
end

if @case = 201611101615 begin -- 無法置換
	select * from TBLOG_VIDEO where FSID = '2016802' and FNEPISODE = 14 order by FDCREATED_DATE  --G201680200140022 為送播單, Bqi 
end

if @case = 201611110848 begin -- 完成帶測試
	select * from TBLOG_VIDEO where FSID = '2013232' order by FDCREATED_DATE --G201323200010065, FSARC_TYPE = '007'
	select * from TBLOG_VIDEO where FSARC_TYPE = '007' and FSTRANS_FROM like '%數位轉檔%' order by FDCREATED_DATE --201503190033, 201503190034
	select * from TBUSERS where FSUSER_ID = '09451' --張正興

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-11-11'
	and (FSMESSAGE like '%G201323200010065%' or FSMESSAGE like '%147789%' or FSMESSAGE like '%198382%' or FSMESSAGE like '%9134%')
	order by FDTIME
end

if @case = 201611151634 begin
	select * from TBPROG_M where FSPGMNAME = '一把青' --2016478
	select * from TBLOG_VIDEO where FSID = '2016478' and FSARC_TYPE = '002' and FCFILE_STATUS = 'T'
	select * from TBLOG_VIDEO where FSFILE_NO = 'P201607002870001' --00002QQR
end

if @case = 201611151722 begin
	exec SP_Q_GET_FILE_SEG_BY_VIDEOID_MIX '0000289Q'
end

if @case = 201611161438 begin --purge list
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002WFI' order by FDDATE
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '000033P9' order by FDDATE --11/13 complete, 11/15 purge
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '0000347B' order by FDDATE --11/13 complete, 11/16 purge
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '000033OD' order by FDDATE --11/13 complete, 11/16 purge
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '0000344B' order by FDDATE --11/13 complete, 11/16 purge

		select distinct A.FSCHANNEL_ID, A.FDDATE, A.FSPLAYTIME as PLAYTIME, A.FSPROMO_ID as FSID, P.FSPROMO_NAME as NAME, 0, D.FSFILE_NO, D.FSVIDEO_ID, B.FCFILE_STATUS, C.FCCHECK_STATUS, D.FCSTATUS, C.FSBRO_ID from TBPGM_ARR_PROMO A
		left join TBLOG_VIDEO B on A.FSPROMO_ID = B.FSID and B.FNEPISODE = 0 and B.FSARC_TYPE = '002' and (B.FCFILE_STATUS not in ('D', 'X') or B.FCFILE_STATUS is NULL)
		left join TBBROADCAST C on B.FSBRO_ID = C.FSBRO_ID and C.FCCHECK_STATUS = 'U'
		left join TBLOG_VIDEO_HD_MC D on B.FSFILE_NO = D.FSFILE_NO
		left join TBPGM_PROMO P on A.FSPROMO_ID = P.FSPROMO_ID
		inner join TBZCHANNEL E on A.FSCHANNEL_ID = E.FSCHANNEL_ID and E.FSCHANNEL_TYPE = '002'
		where A.FDDATE >= CONVERT(datetime, '2016-11-15 01:00', 121) - 3 and A.FDDATE <= CONVERT(datetime, '2016-11-15 01:00', 121) + 14
		order by FDDATE, FSCHANNEL_ID, PLAYTIME

	select * from TBLOG_VIDEO where FSVIDEO_PROG = '000033P9' --SD, 11/14 刪除 所以會 11/15 purge --> 到期後刪除  20161100022
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00003469' order by FDDATE
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00003469'

	--todo 檢查到期後刪除是否會檢查有無排播
	select * from TBPGM_PROMO where FSPROMO_ID = '20161100022' --FDEXPIRE_DATE : 2016-11-13 00:00:00.000
end

if @case = 201611161600 begin --檢索 hang 住
	select * from TBTRACKINGLOG where cast(FDTIME as DATE) = '2016-11-17' and FSCATALOG = 'ERROR' order by FDTIME
	select * from TBTRACKINGLOG where cast(FDTIME as DATE) = '2016-11-17' order by FDTIME
end

if @case = 201611171220 begin
	select * from TBUSERS where FSUSER = 'hsing1'
	select * from TBUSERS_TEMP
end

if @case = 201611171426 begin
	select * from TBPROG_M where FSPGMNAME = '一把青' --2016478
	select * from VW_SEARCH_VIDEO where FSID = '2016478'
end

if @case = 201611171616 begin --2016PTS1 RM 公視演講廳  東西文創高峰會 TC 填錯, ANS 轉檔失敗
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '000034FY'  --20161100185, R, 201611070105
	select * from TBPGM_PROMO where FSPROMO_ID = '20161100185'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000034FY' --P201611001850001
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'P201611001850001'

	--直接修改 TC 重啟任務會卡在 TSM -1
	update TBLOG_VIDEO_SEG set FSBEG_TIMECODE = '01:04:15;02' where FSFILE_NO = 'P201611001850001'
	update TBLOG_VIDEO set FSBEG_TIMECODE = '01:04:15;02' where FSFILE_NO = 'P201611001850001'

	update TBLOG_VIDEO_SEG set FSEND_TIMECODE = '01:04:25;02' where FSFILE_NO = 'P201611001850001'
	update TBLOG_VIDEO set FSEND_TIMECODE = '01:04:25;02' where FSFILE_NO = 'P201611001850001'

	--改成待轉待審
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201611001850001'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201611001850001'

	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000034FY'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '000034FY'
end

if @case = 201611212247 begin --Linked Server
	select * from NEWSARCHIVE.PTS_MAM.dbo.TBGROUPS
	select * from PTS_MAM.dbo.TBGROUPS  -- error
	select * from [10.1.253.88].EF2KWeb.dbo.resal
end

if @case = 201611230940 begin --修改音軌
	select * from TBPROG_M where FSPGMNAME = '爸媽囧很大' --2009285
	select * from TBLOG_VIDEO where FSID = '2009285' and FNEPISODE in (673, 672, 671) and FSARC_TYPE = '002' and FCFILE_STATUS = 'T' --G200928506730008, G200928506710008, G200928506720008
	exec SP_U_TBLOG_VIDEO_FSTRACK 'G200928506730008','MMMM', '51204'
	exec SP_U_TBLOG_VIDEO_FSTRACK 'G200928506710008','MMMM', '51204'
	exec SP_U_TBLOG_VIDEO_FSTRACK 'G200928506720008','MMMM', '51204'
end

if @case = 201611231547 begin
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-11-23'
	and (FSMESSAGE like '%D201502000030001%' or FSMESSAGE like '%148532%' or FSMESSAGE like '%199709%' or FSMESSAGE like '%99868%')
	order by FDTIME

	select * from TBTRACKINGLOG where FDTIME between '2016-11-23 10:30' and '2016-11-23 13:10' order by FDTIME

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-11-23'
	and (FSMESSAGE like '%D201502000030001%' or FSMESSAGE like '%148557%' or FSMESSAGE like '%199762%' or FSMESSAGE like '%99892%')
	order by FDTIME

	select * from TBTRACKINGLOG where FDTIME between '2016-11-23 15:30' and '2016-11-23 15:50' order by FDTIME
end

if @case = 201612121354 begin --HD 有一個 NODE 掛掉
	select * from TBTRACKINGLOG where FDTIME > '2016-12-10' and FSCATALOG = 'ERROR' order by FDTIME
end


if @case = 201612121451 begin --應該建送播置換單卻建到送帶轉檔置換單，抽單重填
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '000037BP'
	select * from TBBROADCAST where FSBRO_ID = '201612080089'
end

if @case = 201612121453 begin  --場景設定表單查不到節目資料
	select * from PTSPROG.PTSProgram.dbo.TBPROG_M where FSPROGID = '2017561' --麼(介)麼(介)
end



------------------------------------------------------------------------------------------------------
select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002TXZ'
select * from TBZCHANNEL order by FSSORT
select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002E51'
select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002Q28', '00002PTQ')
select * from TBPTSAP_STATUS_LOG where FSVIDEOID  = '00002ptq'
select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002ptq' order by FDDATE
select top 100 * from TBPGM_INSERT_TAPE
select * from TBLOG_VIDEO where FSFILE_NO = 'G000059200010001'
select * from TBUSERS where FSUSER_ChtName = '施雅慧'
------------------------------------------------------------------------------------------------------------------------------------

select * from TBLOG_VIDEO where FSID = '20161000061'


if @case = 100 begin
	select * from TBZCHANNEL
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002M3L'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002M3L'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002NED'

	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002NC6'

	--檢查標記上傳失敗
	select * from TBLOG_VIDEO_HD_MC where CAST(FDUPDATED_DATE as DATE) >= '2016-07-05'  and FCSTATUS = 'E' order by FDUPDATED_DATE desc
	select * from TBPTSAP_STATUS_LOG where CAST(FDUPDATE_DATE as DATE) >= '2016-07-05' and FCSTATUS = '88' order by FDUPDATE_DATE desc

	select * from TBLOG_VIDEO_HD_MC where CAST(FDUPDATED_DATE as DATE) >= '2016-07-08'  order by FDUPDATED_DATE desc
	select * from TBPTSAP_STATUS_LOG where CAST(FDUPDATE_DATE as DATE) >= '2016-07-08'  order by FDUPDATE_DATE desc

	--查詢未來以排播的節目表
	select * from VW_PLAYLIST_FROM_NOW
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in ('00002p4h','00002pms','00002pmr', '00002pmq', '00002pmp', '00002pmo', '00002pmn', '00002qsa') 
end

if @case = 201611211431 begin
	select * from sys.servers
end

if @case = 201611251542 begin
	select * from TBPROPOSAL where FSPROG_ID = '2016443' --為什麼有 2 筆成案？
	--201510050013  : 01;51;02;61;11;07;08;     201611170122: 01;12;02;13;11;14;07;62;64;08;51;61; 續製
	select * from TBPROPOSAL where FSPRO_ID = '201510050013' -- FSPRO_TYPE = 1 新製
	select * from TBPROPOSAL where FSPRO_ID = '201611170122' -- FSPRO_TYPE = 1 續製 --2016-11-17 17:44:34.793

	select * from TBZCHANNEL
	select * from TBPROG_M where FSPROG_ID = '2016443' --01;12;02;13;11;14;07;62;64;08;51;61;  FDUPDATE = 2016-11-25 12:00:58.210
end

if @case = 201611291110 begin
	select * from TBPROG_M where FSPGMNAME like '%山歌唱來%' --2012025
	select * from TBLOG_VIDEO where FSID = '2012025' and FNEPISODE in (27, 28, 29) and FSARC_TYPE = '002' --G201202500270001, G201202500280001, G201202500290001 ,手動刪除作業 B -> X
	select * from TBLOG_VIDEO where FSID = '2012025' and FNEPISODE between 1 and 60 and FSARC_TYPE = '002' and FCFILE_STATUS = 'B' order by FNEPISODE
	select * from TBLOG_VIDEO where FSID = '2012025' and FNEPISODE = 27 and FSARC_TYPE = '002' -- 手動刪除作業 查不到 27 集?? 000010BW
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000010BW' --2016-12-17 有排播，先請編表抽節目
end

if @case = 201611291336 begin
	select * from TBPGM_PROMO where FSPROMO_ID = '20161100709' --2014藝起看公視 既視感De'ja' Vu --> 單引號會造成建立送播單失敗，
	select * from TBLOG_VIDEO where FSID = '20161100709' -- no record
end

if @case = 201611291427 begin  --SP_Q_DELETED_FILE 查詢可以被刪除的檔案
	select * from TBLOG_VIDEO A left join TBPROG_D B on A.FSID = B.FSPROG_ID and A.FNEPISODE = B.FNEPISODE
		left join TBTRANSCODE C on A.FSJOB_ID = C.FNTRANSCODE_ID
		left join TBZCHANNEL D on A.FSCHANNEL_ID = D.FSCHANNEL_ID
		left join TBUSERS E on A.FSCREATED_BY = E.FSUSER_ID
		left join TBZFILE_STATUS F on A.FCFILE_STATUS = F.FSFILE_STATUS_ID
		left join TBFILE_TYPE G on A.FSARC_TYPE = G.FSARC_TYPE
	where
		A.FSCHANNEL_ID = '07' and
		A.FSTYPE = 'G' and
		(A.FSID = '20161100709') and
		

end

if @case = 201611291632 begin-- 提供主控主頻 12/2 的檔案 10 支
	select NAME as '節目名稱', FNEPISODE 集數, FDDATE 播出日期, FSPLAY_TIME 播出時間, FSVIDEO_ID from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in ('00002WFQ', '000033TE', '0000363Z', '000031Y4', '0000367V', 
	'00002NQW', '000035AR', '00002N40', '0000346J', '00003206') order by FDDATE, FSPLAY_TIME

	select distinct NAME as '節目名稱', FNEPISODE 集數, FSVIDEO_ID from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in ('00002WFQ', '000033TE', '0000363Z', '000031Y4', '0000367V', 
	'00002NQW', '000035AR', '00002N40', '0000346J', '00003206')
end

if @case = 201611291735 begin
	select * from TBPROG_M
	select * from TBZPROGSRC --節目來源代碼 自製、委製
	select * from TBZPROGLANG --聲道

	select A.FSPROG_ID, A.FSWEBNAME, A.FSPGMNAME, A.FNLENGTH,B.FSPROGSRCNAME, A.FNTOTEPISODE, C.FSPROGLANGNAME as FSPROGLANGNAME1, D.FSPROGLANGNAME as FSPROGLANGNAME2
	from TBPROG_M A left join TBZPROGSRC B on A.FSPROGSRCID = B.FSPROGSRCID
	left join TBZPROGLANG C on A.FSPROGLANGID1 = C.FSPROGLANGID
	left join TBZPROGLANG D on A.FSPROGLANGID2 = C.FSPROGLANGID
	order by A.FSPROG_ID

	select A.FSPROG_ID, A.FNEPISODE, A.FSPGDNAME, B.FSPROGSRCNAME, C.FSPROGLANGNAME FSPROGLANGNAME1, D.FSPROGLANGNAME FSPROGLANGNAME2
	from TBPROG_D A left join TBZPROGSRC B on A.FSPROGSRCID = B.FSPROGSRCID
	left join TBZPROGLANG C on A.FSPROGLANGID1 = C.FSPROGLANGID
	left join TBZPROGLANG D on A.FSPROGLANGID2 = D.FSPROGLANGID
	order by A.FSPROG_ID

/*
	ALTER VIEW VW_TBPROG_M as
		select A.FSPROG_ID, A.FSWEBNAME, A.FSPGMNAME, A.FNLENGTH,B.FSPROGSRCNAME, A.FNTOTEPISODE, C.FSPROGLANGNAME as FSPROGLANGNAME1, D.FSPROGLANGNAME as FSPROGLANGNAME2
			from TBPROG_M A left join TBZPROGSRC B on A.FSPROGSRCID = B.FSPROGSRCID
			left join TBZPROGLANG C on A.FSPROGLANGID1 = C.FSPROGLANGID
			left join TBZPROGLANG D on A.FSPROGLANGID2 = D.FSPROGLANGID
	GO

	select * from VW_TBPROG_M

	CREATE VIEW VW_TBPROG_D as
		select A.FSPROG_ID, A.FNEPISODE, A.FSPGDNAME, B.FSPROGSRCNAME, C.FSPROGLANGNAME FSPROGLANGNAME1, D.FSPROGLANGNAME FSPROGLANGNAME2
			from TBPROG_D A left join TBZPROGSRC B on A.FSPROGSRCID = B.FSPROGSRCID
			left join TBZPROGLANG C on A.FSPROGLANGID1 = C.FSPROGLANGID
			left join TBZPROGLANG D on A.FSPROGLANGID2 = D.FSPROGLANGID
	GO
	*/

	select * from VW_TBPROG_M order by FSPROG_ID
end


if @case = 201611301705 begin --俊豐建到短帶送帶轉檔單
	select * from TBPGM_PROMO where FSPROMO_NAME like '%2016 WTA女子職業%' --20161100767
	select * from TBLOG_VIDEO where FSID = '20161100767' --201611300003
	select * from TBBROADCAST where FSBRO_ID = '201611300003' --影帶擷取
end

if @case = 201611301751 begin --調用？
	select * from TBPREBOOKING --??
	select * from TBBOOKING_DETAIL
	select * from TBBOOKING_MASTER where CAST(FDBOOKING_DATE as DATE) = '2016-11-30'
	select * from TBBOOKING_DETAIL where FSBOOKING_NO = '201611300110' --FCCHECK = R, FSCHECK_ID = NULL FDCHECK_DATE = NULL

	select * from TBUSERS where FSUSER = '4gtv' --PTSMAM20155566
	select * from TBBOOKING_MASTER where FSUSER_ID = 'PTSMAM20155566' --FCCHECK_STATUS = 'Y', FSCHECK_ID = NULL, FDCHECK_DATE = NULL ...
	select * from TBBOOKING_DETAIL where FSBOOKING_NO = '201609049999'
end

if @case = 201612010858 begin --水果冰淇淋 1684 ANS 轉檔完成，MAM 仍在轉檔中 REF to TICKET:201609090924
	select * from TBLOG_VIDEO where FSFILE_NO = 'G000006016840004' --S, 00001LS1, 201412290022

	--2016-11-23 16:00:07.173
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-11-23'
	and (FSMESSAGE like '%G000006016840004%' or FSMESSAGE like '%148580%' or FSMESSAGE like '%199785%' or FSMESSAGE like '%99916%' or FSMESSAGE like '%001LS1%')
	order by FDTIME

	select * from TBTRACKINGLOG where FDTIME between '2016-11-23 16:00' and '2016-11-23 18:00' and FSCATALOG = 'ERROR' order by FDTIME
	select * from TBTRACKINGLOG where FDTIME between '2016-11-23 16:00' and '2016-11-23 18:00' order by FDTIME

	select * from TBTRACKINGLOG where FDTIME between '2016-11-22' and '2016-11-30' and
	 (FSMESSAGE like '%G000006016840004%' or FSMESSAGE like '%148580%' or FSMESSAGE like '%199785%' or FSMESSAGE like '%99916%' or FSMESSAGE like '%001LS1%')
	order by FDTIME

	--將轉檔狀態改為失敗重新轉檔
	--update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G000006016840004' -- sa

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-01' and
	 (FSMESSAGE like '%G000006016840004%' or FSMESSAGE like '%148580%' or FSMESSAGE like '%199785%' or FSMESSAGE like '%99916%' or FSMESSAGE like '%001LS1%')
	order by FDTIME

end

if @case = 201612012318 begin --查詢 VideoId
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002MRA'
	select * from TBPGM_PROMO where FSPROMO_ID = '20160600371' --看見福爾摩莎_空拍
end


if @case = 201612012320 begin --成案
	exec SP_Q_TBPROPOSAL_ALL_BYUSERID '60042' --程瓊瑤
	exec SP_Q_TBPROG_M_CODE
	exec SP_Q_TBZPROGBUYD_ALL
	select * from TBPROPOSAL
end





if @case = 201612021611 begin
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '000036NP' --T, P201611008120001, FSID = 20161100812, 01:01:45;00 ~ 01:02:50;00
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000036NP' --2016-12-05
	select * from TBBROADCAST where FSBRO_ID = '201611300128'
end

if @case = 201612051619 begin
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000036QE' --2016世界羽聯大獎賽-蘇格蘭公開賽 105 G
end


if @case = 201612051729 begin --世界這YOUNG說 61, 側標要修改，需要置換，必須重新轉檔
	select * from TBLOG_VIDEO where FSBRO_ID = '201611280065' --G201715100610001, S, 000036AL

	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000036AL' order by FDDATE --2016-12-07

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-05' and
	 (FSMESSAGE like '%G201715100610001%' or FSMESSAGE like '%149469%' or FSMESSAGE like '%201271%' or FSMESSAGE like '%100821%' or FSMESSAGE like '%0036AL%')
	order by FDTIME

	--卡在 plink開始進行高解搬檔：Job_ID：149469

	--ANS reschedule
end

if @case = 201612060915 begin --10.13.200.1 無法連線
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002WQK' --T
	SELECT FSSYSTEM_CONFIG FROM TBSYSTEM_CONFIG
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002XEU' -- 未排播，已被刪除

	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000036QB' --2016-17 歐洲冠軍足球聯賽 37  todo 檢查主控的檔案
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '000036QB'  --S, 201612010092, G201734900370002   Anystream reschedule -> T
	select * from TBBROADCAST where FSBRO_ID = '201612010092'

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-05' and
	 (FSMESSAGE like '%G201734900370002%' or FSMESSAGE like '%149490%' or FSMESSAGE like '%201293%' or FSMESSAGE like '%100842%' or FSMESSAGE like '%0036QB%')
	order by FDTIME

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-07' and
	 (FSMESSAGE like '%G201734900370002%' or FSMESSAGE like '%149490%' or FSMESSAGE like '%201293%' or FSMESSAGE like '%100842%' or FSMESSAGE like '%0036QB%')
	order by FDTIME

	select * from TBLOG_VIDEO where FSVIDEO_PROG = '000036QF' --S, G201698500050002, 201612010095, 000036QF  2016世界羽聯大獎賽-蘇格蘭公開賽, 搬檔失敗
	select * from TBBROADCAST where FSBRO_ID = '201612010095' --2016-12-05 18:40:33.283

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-05' and
	 (FSMESSAGE like '%G201698500050002%' or FSMESSAGE like '%149519%' or FSMESSAGE like '%201329%' or FSMESSAGE like '%100871%' or FSMESSAGE like '%000036QF%')
	order by FDTIME

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-07' and --2016世界羽聯大獎賽-蘇格蘭公開賽 reschdeule
	 (FSMESSAGE like '%G201698500050002%' or FSMESSAGE like '%149519%' or FSMESSAGE like '%201329%' or FSMESSAGE like '%100871%' or FSMESSAGE like '%000036QF%')
	order by FDTIME

	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002NQA' --T

	select * from TBLOG_VIDEO where FSVIDEO_PROG = '0000346E' --T
end



if @case = 201612061807 begin --查詢寫入 Louth DB 的段落資訊
	exec SP_Q_GET_FILE_SEG_BY_VIDEOID_MIX '0000371I'
end

if @case = 201612071049 begin --檔案上傳時間延遲問題
	select * from TBPROG_M where FSPGMNAME = '巴西生態地圖' --2017306, G201730600040002, 000032U0
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '000032U0'  --2016-10-26 11:27:41.510
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000032U0'  --2016-11-10 20:22:58.390
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in  ('000032U0', '000035CG', '000035CS')

	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '000032U0' order by FDDATE
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '000035CG' order by FDDATE
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '000035CS' order by FDDATE
end

if @case = 201612080910 begin
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002ZBP' --T
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002ZBP'

	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00003723'  --S
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00001Y7M'
end

if @case = 201612080943 begin
	-- Input : Video ID 清單 8 碼
	-- Output : 排播資料

	if OBJECT_ID(N'tempdb.dbo.#tempImport', N'U') IS NOT NULL
		DROP TABLE #tempImport

	CREATE TABLE #tempImport (
		line varchar(10)
	)
	--put input file on MAMDB2
	BULK insert #tempImport from '\\10.13.200.5\OtherVideo\temp\videoid.txt' WITH
	(
		ROWTERMINATOR = '\n'
	);

	--select * from #tempImport

	if OBJECT_ID(N'tempdb.dbo.#tmp', N'U') IS NOT NULL
		DROP TABLE #tmp

    select * into #tmp from VW_PLAYLIST_FROM_NOW where 1=0
	--select * from  #tmp

	declare CSR CURSOR for select line from #tempImport

	declare @videoID varchar(8)

	declare @cnt int = 1

	open CSR
	fetch next from CSR into @videoID
	while (@@FETCH_STATUS = 0)
	begin
		--print CAST(@cnt as varchar(5)) + ' : ' +  @videoID
		if EXISTS(select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = right('00' + @videoID, 8)) begin
			--print CAST(@cnt as varchar(5)) + ' : ' +  @videoID
			insert #tmp select top 1 * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = right('00' + @videoID, 8) order by FDDATE, FSPLAY_TIME, FSCHANNEL_ID
		end
		fetch next from CSR into @videoID
		set @cnt = @cnt + 1
	end

	select A.QUEUE, A.NAME 名稱, A.FNEPISODE 集數, A.FDDATE 播出日期, A.FSPLAY_TIME 播出時間, B.FSCHANNEL_NAME 頻道, A.FSVIDEO_ID from #tmp A left join TBZCHANNEL B on A.FSCHANNEL_ID = B.FSCHANNEL_ID  order by A.FSCHANNEL_ID, FDDATE, FSPLAY_TIME

	exec SP_Q_PLAYLIST_BY_VIDEOID '1111', 1
end

if @case = 201612081101 begin --審核通過，未轉低解
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '000036MV' --B, P201611008000001, 201611300092  -> 重新審核轉檔 T
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000036MV' --O, 2016-11-30 19:15:12.270
	select * from TBBROADCAST where FSBRO_ID = '201611300092'
	select * from TBPGM_PROMO where FSPROMO_ID = '20161100800' --感動99_我的續世代_短片徵件Promo
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-11-30' and FSCATALOG = 'ERROR' order by FDTIME
	UPDATE TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSVIDEO_ID = '000036MV'
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000036MV'
	select * from TBZCHANNEL
end

if @case = 201612081435 begin --有檔案無 Louth DB  在20161208 刪除清單
--P20161104.log:20:2016/11/4 上午 02:23:38 : 開始搬移檔案:\\10.13.200.6\Review\002OCG.mxf:到待審區
--Louth20161104.log:55:2016/11/4 上午 02:23:43 : 開始寫入檔案[00002OCG ]的段落
--Louth20161104.log:56:2016/11/4 上午 02:23:43 : 寫入檔案[00002OCG ]的段落完畢

	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID =  '00002OCG'
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID =  '00002OCG'
	select * from TBPGM_PROMO where FSPROMO_ID = '20160600794' --PTS3 2016RM  燦爛時光
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002OCG' --P201606007940001, 201606200050
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002OCG' --2016-11-03 19:42:01.103 O
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002OCG' order by FDDATE
	select * from TBBROADCAST where FSBRO_ID = '201606200050' --FDTRAN_DATE 2016-11-03 19:42:02.840
end

if @case = 201612081457 begin --查轉檔失敗
	select * from TBLOG_VIDEO A left join TBBROADCAST B on A.FSBRO_ID = B.FSBRO_ID left join TBTRANSCODE C on A.FSJOB_ID = C.FNTRANSCODE_ID
	where A.FSARC_TYPE = '002' and A.FCFILE_STATUS = 'S' and B.FDTRAN_DATE < CAST(GETDATE() as DATE)  order by B.FDTRAN_DATE

	select A.FSFILE_NO, A.FSID, A.FNEPISODE, A.FCFILE_STATUS, A.FSBRO_ID, A.FSVIDEO_PROG, B.FDTRAN_DATE, B.FSSIGNED_BY_NAME, C.FNTRANSCODE_ID, C.FNTSMDISPATCH_ID, C.FNANYSTREAM_ID,
	C.FDSTART_TIME, C.FSNOTE
	from TBLOG_VIDEO A left join TBBROADCAST B on A.FSBRO_ID = B.FSBRO_ID left join TBTRANSCODE C on A.FSJOB_ID = C.FNTRANSCODE_ID
	where A.FSARC_TYPE = '002' and A.FCFILE_STATUS = 'S' and B.FDTRAN_DATE < CAST(GETDATE() as DATE)  order by B.FDTRAN_DATE

	select * from TBTRANSCODE

	--20161208 reschedule
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-02' and --2016-17 歐洲冠軍足球聯賽 34 搬檔失敗
	(FSMESSAGE like '%G201734900340001%' or FSMESSAGE like '%149282%' or FSMESSAGE like '%200972%' or FSMESSAGE like '%100636%' or FSMESSAGE like '%0036MZ%')
	order by FDTIME

	--reschedule fail on plink開始進行高解搬檔：Job_ID：149282
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-08' and --2016-17 歐洲冠軍足球聯賽 34 搬檔失敗
	(FSMESSAGE like '%G201734900340001%' or FSMESSAGE like '%149282%' or FSMESSAGE like '%200972%' or FSMESSAGE like '%100636%' or FSMESSAGE like '%0036MZ%')
	order by FDTIME
	----------------------------------
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-05' and --2016-17 歐洲冠軍足球聯賽 36 搬檔失敗
	(FSMESSAGE like '%G201734900360002%' or FSMESSAGE like '%149451%' or FSMESSAGE like '%201250%' or FSMESSAGE like '%100804%' or FSMESSAGE like '%0036Q9%')
	order by FDTIME

	--reschedule fail
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-08' and --2016-17 歐洲冠軍足球聯賽 36 搬檔失敗
	(FSMESSAGE like '%G201734900360002%' or FSMESSAGE like '%149451%' or FSMESSAGE like '%201250%' or FSMESSAGE like '%100804%' or FSMESSAGE like '%0036Q9%')
	order by FDTIME
	------------------------------------------
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-07' and --2016-17 歐洲冠軍足球聯賽 38 搬檔失敗
	(FSMESSAGE like '%G201734900380001%' or FSMESSAGE like '%149680%' or FSMESSAGE like '%201771%' or FSMESSAGE like '%101021%' or FSMESSAGE like '%003723%')
	order by FDTIME

	--reschedule ok
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-08' and --2016-17 歐洲冠軍足球聯賽 38 搬檔失敗
	(FSMESSAGE like '%G201734900380001%' or FSMESSAGE like '%149680%' or FSMESSAGE like '%201771%' or FSMESSAGE like '%101021%' or FSMESSAGE like '%003723%')
	order by FDTIME
	-------------------------------------------------
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in ('000036MZ', '000036Q9', '00003723') --2016-12-08 #36, 2016-12-12 #38
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in ('000036O3', '000036QE', '000036QG') order by FDDATE --2016-12-08 #4, 2016-12-12 #6


	select * from TBLOG_VIDEO where FSVIDEO_PROG = '000036O3' --48min, 105G
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '000036QE' --37min, 105G
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '000036QG' --40min, 134G

	select * from TBLOG_VIDEO where FSVIDEO_PROG = '000036MZ' --1hr 52 min, 50G
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '000036Q9' --1hr 43 min, 50G
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00003723' --1hr 41 min, 44.5G

	select CAST(GETDATE() as DATE)
end

if @case = 201612081524 begin --2016游泳世界盃系列賽  香港站 在 Review 的檔案無法以 XDCAMViewer 開啟, HDUpload 的檔案是正常
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00003729' --2016-12-11 07:00:00;00 play
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00003729'
	-- 搬完後 Approved 的檔案是正常，所以正在 move 的檔案無法開啟？
end


if @case = 201612131207 begin
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201680200100002' --\\mamstream\MAMDFS\ptstv\HVideo\2016\2016802\G201680200100002.mxf
end


if @case = 201612132244 begin -- 成案表單無法審核 todo
	select * from TBPROPOSAL where FSPRO_ID = '201612050068' --FSCREATED_BY=07189
	select * from PTSFlow.dbo.Flow3_Activity where txt_FSPRO_ID = '201512140090'
    select * from PTSFlow.dbo.Flow3_Activity where F_ActiveID = 66135  --FormId = 201512140090 成案單號  一把青有資料
    select * from PTSFlow.dbo.Flow3_Activity where FormId = '201612050068' --音樂萬萬歲--回娘家 無資料 -> 系統管理＼流程補作管理: 69754
	select * from TBUSERS where FSUSER_ID = '07189' --莊惠琳
end

if @case = 201612141141 begin --轉檔失敗無法置換
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '000037QU' --R, P201611007210002
	select * from TBPGM_PROMO where FSPROMO_ID = '20161100721' --2016_12 Highlight
	select * from TBBROADCAST where FSBRO_ID = '201612130104' --2016-12-13 19:55:42.337
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000037QU' --2016-12-15 08:28:15:02

	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201611007210002'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201611007210002'

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-13'
	and (FSMESSAGE like '%P201611007210002%' or FSMESSAGE like '%150151%' or FSMESSAGE like '%202704%' or FSMESSAGE like '%101465%' or FSMESSAGE like '%0037QU%' )
	order by FDTIME
	--轉檔任務錯誤，編號：150151

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-14'
	and (FSMESSAGE like '%P201611007210002%' or FSMESSAGE like '%150151%' or FSMESSAGE like '%202704%' or FSMESSAGE like '%101465%' or FSMESSAGE like '%0037QU%' )
	order by FDTIME

	select * from TBTSM_JOB where FNTSMJOB_ID = 202704 --FNSTATUS = 3
	select * from TBTRANSCODE where FNanystream_ID = 101465 --FNPROCESS_STATUS = 8 Anystream 處理錯誤
	select * from TBTRANSCODE_RECV_LOG where FNJOB_ID = 101465

	select * from TBTRANSCODE where FNANYSTREAM_ID = 101469  --FNPROCESS_STATUS = 4

	select FNPROCESS_STATUS from TBTRANSCODE group by FNPROCESS_STATUS  --0 ~ 9
	select * from TBTRANSCODE where FNPROCESS_STATUS = 0 order by FDSTART_TIME  --0 : 轉檔任務狀態：檢查狀態  (3)
	select * from TBTRANSCODE where FNPROCESS_STATUS = 2 order by FDSTART_TIME  --1 :
end

if @case = 201612151543 begin --轉檔完成無低解
	select * from TBPROG_M where FSPROG_ID = '2017497' --2016 WTA女子職業網球巡迴賽 香港站

	select * from TBLOG_VIDEO where FSID = '2017497' and FNEPISODE = 1 --R, 201612080004, G201749700010001, 000037A5
	select * from TBBROADCAST where FSBRO_ID = '201612080004' --2016-12-10 19:28:28.660
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-10'
	and (FSMESSAGE like '%G201749700010001%' or FSMESSAGE like '%149959%' or FSMESSAGE like '%202302%' or FSMESSAGE like '%101282%' or FSMESSAGE like '0037A5%' )
	order by FDTIME
	--plink高解搬檔失敗：JobID 149959/10.13.210.16 -l root -pw TSMP@ssw0rdTSM "cp -r /mnt/HDUpload/0037A5.mxf  /ptstv/HVideo/2017/2017497/G201749700010001.mxf"/cp: writing `/ptstv/HVideo/2017/2017497/G201749700010001.mxf': No space left on device
	select * from TBTRACKINGLOG where FDTIME between '2016-12-10 19:00' and '2016-12-10 23:00' order by FDTIME

	--reschedule 34G
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-15'
	and (FSMESSAGE like '%G201749700010001%' or FSMESSAGE like '%149959%' or FSMESSAGE like '%202302%' or FSMESSAGE like '%101282%' or FSMESSAGE like '0037A5%' )
	order by FDTIME

	--重新審核
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201749700010001'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201749700010001'

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-15'
	and (FSMESSAGE like '%G201749700010001%' or FSMESSAGE like '%150356%' or FSMESSAGE like '%203052%' or FSMESSAGE like '%101650%' or FSMESSAGE like '%0037A5%' )
	order by FDTIME
	
	--第 2 集
	select * from TBLOG_VIDEO where FSID = '2017497' and FNEPISODE = 2 --S, 201612080009, G201749700020002, 000037A7
	select * from TBBROADCAST where FSBRO_ID = '201612080009' --2016-12-13 11:35:02.513
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-13'
	and (FSMESSAGE like '%G201749700020002%' or FSMESSAGE like '%150105%' or FSMESSAGE like '%202632%' or FSMESSAGE like '%101419%' or FSMESSAGE like '%0037A7%' )
	order by FDTIME

	--reschedule 43G
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-15'
	and (FSMESSAGE like '%G201749700020002%' or FSMESSAGE like '%150105%' or FSMESSAGE like '%202632%' or FSMESSAGE like '%101419%' or FSMESSAGE like '%0037A7%' )
	order by FDTIME

	select * from TBTRACKINGLOG where FDTIME between '2016-12-15 16:40' and '2016-12-15 19:00' order by FDTIME

	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201749700020002'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201749700020002'

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) between '2016-12-15' and '2016-12-16'
	and (FSMESSAGE like '%G201749700020002%' or FSMESSAGE like '%150357%' or FSMESSAGE like '%203053%' or FSMESSAGE like '%101651%' or FSMESSAGE like '%0037A7%' )
	order by FDTIME

	select * from TBTRACKINGLOG where FDTIME between '2016-12-16 00:50' and '2016-12-16 01:50' order by FDTIME
	--HSM3 ping error

	--QC again
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) between '2016-12-16' and '2016-12-16'
	and (FSMESSAGE like '%G201749700020002%' or FSMESSAGE like '%150358%' or FSMESSAGE like '%203064%' or FSMESSAGE like '%101652%' or FSMESSAGE like '%0037A7%' )
	order by FDTIME

	select * from TBTRACKINGLOG where FDTIME between '2016-12-16 09:18' and '2016-12-16 09:50' order by FDTIME

	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000037A5' --2016-12-17 ch14
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000037A7' --2016-12-18
end

if @case = 201612160923 begin --check ticome2LouthTimecode() bug
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '000036L3'
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G201558200550001'
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G201558200550002'

	exec SP_Q_GET_FILE_SEG_BY_VIDEOID_MIX '000037WY'
	exec SP_Q_GET_FILE_SEG_BY_VIDEOID_MIX '000037PK'
end


if @case = 201612161345 begin --調用轉檔完成，未見檔案
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201364900010007' --\\mamstream\MAMDFS\ptstv\HVideo\2013\2013649\G201364900010007.mxf, 00002I99
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-16'
	and (FSMESSAGE like '%G201364900010007%' or FSMESSAGE like '%150399%' or FSMESSAGE like '%203108%' or FSMESSAGE like '%101696%' or FSMESSAGE like '%002I99%' )
	order by FDTIME

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2016-12-16'
	and (FSMESSAGE like '%G201364900010007%' or FSMESSAGE like '%150399%' or FSMESSAGE like '%203108%' or FSMESSAGE like '%101696%' or FSMESSAGE like '%002I99%' )
	order by FDTIME

	--我們的島
end

if @case = 201612161620 begin
	select * from TBLOG_VIDEO where FSID = '2013232' and FCFILE_STATUS = 'T' and FSARC_TYPE = '002' --G201323201000004 11.4G
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G201323201000004' --00002MDN
	exec SP_Q_GET_FILE_SEG_BY_VIDEOID_MIX '00002MDN'
end


if @case = 201612181020 begin --查詢 TSM 搬檔作業
    select * from TBLOG_VIDEO_HD_MC where FDUPDATED_DATE > '2016-12-18' and FCSTATUS = 'O'
	select * from TBTSM_JOB where FDREGISTER_TIME > '2016-12-19' and FSSOURCE_PATH like '%HVideo%' and FSNOTIFY_ADDR = '' and FNSTATUS = 3 --查詢由 GPFS 搬檔到 Review or Approved 的job
	select * from TBTRANSCODE where FNTSMDISPATCH_ID = 203385
	select * from TBTRANSCODE where FNTSMDISPATCH_ID = 203261 --調用
	select * from TBTRANSCODE where FNTSMDISPATCH_ID = 203390 -- 播出檔的搬檔的 不會在 TRANSCODE 有紀錄


	select * from VW_FILE_NAME
	select * from VW_PLAYLIST_FROM_NOW
end

if @case = 201612191427 begin
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '000037TM' --R, G201742100010001
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000037TM' --O
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201742100010001'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201742100010001'
end



if @case = 201612191457 begin  --補寫 Louth DB
	exec SP_Q_GET_FILE_SEG_BY_VIDEOID_MIX '00002XER'
	exec SP_Q_GET_FILE_SEG_BY_VIDEOID_MIX '00002XF9'
    exec SP_Q_GET_FILE_SEG_BY_VIDEOID_MIX '00002XIW'

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in (
	'00002XER','00002XF9','00002XIW','00002XH9','00002XEZ','00002XHM','00002XFH','00002XJ1','00002XF4','00002XF5','00002QSG','00002XFA','00002XFB','00002XF1','00002XIB'
	,'00002JJH','00002OC0','00002JJO','00002QBX','00002QSG','00002JJI','00002OCF','00002JJJ','00002OBX','00002JJK','00002OBY','00002RPO')
end

if @case = 201612192322 begin
	select * from TBLOG_VIDEO_HD_MC where FDUPDATED_DATE > '2016-12-19' and FCSTATUS = 'O' order by FDUPDATED_DATE
	exec SP_Q_GET_CHECK_COMPLETE_LIST_MIX
	exec SP_Q_GET_CHECK_COMPLETE_LIST_MIX_BEFORE

	declare @resultSet TABLE(VIDEO_ID varchar(8))
	insert into @resultSet(VIDEO_ID) exec SP_Q_GET_CHECK_COMPLETE_LIST_MIX_BEFORE

	select A.VIDEO_ID VIDEO_ID, B.FDDATE FDDATE from @resultSet A left join 
	(
	select FSVIDEO_ID, FDDATE from TBPGM_COMBINE_QUEUE where FDDATE >= getdate() - 3 and FDDATE < getdate()
	union
	select FSVIDEO_ID, FDDATE from TBPGM_ARR_PROMO where FDDATE >= getdate() - 3 and FDDATE < getdate()
	) B on A.VIDEO_ID = B.FSVIDEO_ID
	group by VIDEO_ID, FDDATE
	order by FDDATE
end

if @case = 201612201030 begin
	select * from TBTRACKINGLOG where FDTIME > '2016-12-20 10:30' order by FDTIME
	select * from TBBOOKING_MASTER
end


/******************************
	modify  TBSYSTEM_CONFIG
*******************************/
if @case = 201612190858-201609010935-201607271724 begin

	if OBJECT_ID(N'tempdb.dbo.#tmp_TBSYSTEM_CONFIG', N'U') IS NOT NULL
		DROP TABLE #tmp_TBSYSTEM_CONFIG

	select FSSYSTEM_CONFIG into #tmp_TBSYSTEM_CONFIG from TBSYSTEM_CONFIG



	update #tmp_TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<LouthXmlPath>\\10.13.200.1\MasterCtrl\MSG\LOUTH\HD-InputXML\</LouthXmlPath>', 
	'<LouthXmlPath>\\10.13.200.5\OtherVideo\LOUTH_DATA_TEST\HD-InputXML</LouthXmlPath>')

    select * from #tmp_TBSYSTEM_CONFIG

	select * from TBSYSTEM_CONFIG
	select * from #tmp_TBSYSTEM_CONFIG
	update #tmp_TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<BroadcastEmail Desc=""送播單Email"">oappts@mail.pts.org.tw</BroadcastEmail>', 
	'<BroadcastEmail Desc=""送播單Email"">oappts@mail.pts.org.tw; hsing1@mail.pts.org.tw</BroadcastEmail>')

	update #tmp_TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, 'oappts@mail.pts.org.tw', 
	'oappts@mail.pts.org.tw; hsing1@mail.pts.org.tw')

	update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, 'oappts@mail.pts.org.tw', 
	'oappts@mail.pts.org.tw; hsing1@mail.pts.org.tw')

	update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, 'oappts@mail.pts.org.tw; hsing1@mail.pts.org.tw', 
	'oappts@mail.pts.org.tw')

	select * from TBSYSTEM_CONFIG

	if OBJECT_ID(N'tempdb.dbo.#tmp_TBSYSTEM_CONFIG', N'U') IS NOT NULL
		DROP TABLE #tmp_TBSYSTEM_CONFIG
end


if @case = 201612201523 begin  --implement FT_GET_VIDEOID_FROM_TSMJOB()
	select * from TBTSM_JOB where FDREGISTER_TIME > '2016-12-19' and FSSOURCE_PATH like '%HVideo%' and FSNOTIFY_ADDR = '' and FNSTATUS = 3 --72

	--------------------------------------------------------
	declare @tmpSourcePATH TABLE (FSSRCPATH varchar(100))

	insert into @tmpSourcePATH(FSSRCPATH) select FSSOURCE_PATH from TBTSM_JOB where FDREGISTER_TIME > '2016-12-19' and FSSOURCE_PATH like '%HVideo%' and FSNOTIFY_ADDR = '' and FNSTATUS = 3

	declare iCur CURSOR FOR select FSSRCPATH from @tmpSourcePATH

	declare @path varchar(100)
	declare @rowNum int
	declare @tokenTab TABLE(FSFILE varchar(30))
	declare @f varchar(30)

	OPEN iCUR
	FETCH NEXT FROM iCur INTO @path
	while (@@FETCH_STATUS = 0) BEGIN
		select top 1 @f = data from FN_SPLIT(@path, '/') order by id desc
		set @f = SUBSTRING(@f, 1, CHARINDEX('.', @f) - 1)
		insert into @tokenTab(FSFILE) VALUES(@f)
		FETCH NEXT FROM iCUR INTO @path
	end
	select B.FSVIDEO_PROG from @tokenTab A left join TBLOG_VIDEO B on A.FSFILE = B.FSFILE_NO
	-------------------------------------------------------------------------


	select * from FT_GET_VIDEOID_FROM_TSMJOB('2016-12-19 00:00', '2016-12-19 23:59')
	--check 00002HQZ, G201354000130009 is duplicate
	select * from TBTSM_JOB where FDREGISTER_TIME > '2016-12-19' and FSSOURCE_PATH like '%HVideo%' and FSNOTIFY_ADDR = '' and FNSTATUS = 3 and FSSOURCE_PATH like '%G201354000130009%'
	--203513, 203489

	select * from TBTSM_JOB where FDREGISTER_TIME between '2016-12-19 00:00' and '2016-12-19 23:59' and FSSOURCE_PATH like '%HVideo%' and FSNOTIFY_ADDR = '' and FNSTATUS = 3 order by FDLASTUPDATE_TIME
	declare @t varchar(30)
	set @t = 'P201611007370001.mxf'
	select CHARINDEX('.', @t)
	select SUBSTRING(@t, 1, CHARINDEX('.', @t) -1)
end

if @case = 201612211813 begin --調用
	exec SP_Q_CHECK_PROG_IS_EXPIRE_CANNOT_BOOKING '201303050010099', 'V' --enpty news
	exec SP_Q_CHECK_PROG_IS_EXPIRE_CANNOT_BOOKING 'G201489300020001', 'V' --empty
	exec SP_Q_CHECK_PROG_IS_EXPIRE_CANNOT_BOOKING 'G201323200030004', 'V' --G201323200030004

	select * from TBLOG_VIDEO where FSFILE_NO = '201303050010099'
end


if @case = 201612231428 begin --TS3500, TS3310 上下架
	select * from TBTRACKINGLOG where FDTIME > '2016-12-23 14:00' order by FDTIME
	select * from TBTRACKINGLOG where FDTIME > '2016-12-23 14:30' order by FDTIME
	select * from TBUSERS where FSUSER_ID = '08255' --陳素娥
end

if @case = 201612281219 begin -- timccode 錯誤轉檔失敗
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '000038MX' -- FSEND_TIMECODE = 01:00:00;00, P201612006070001, 201612220111
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'P201612006070001'

	--update TBLOG_VIDEO set FSBEG_TIMECODE = '01:00:00;00', FSEND_TIMECODE = '01:00:30;00', FSUPDATED_BY= '51204', FDUPDATED_DATE=GETDATE()
	--where FSFILE_NO = 'P201612006070001'

	--exec SP_U_TBLOG_VIDEO_SEG_BYFILEID 'P201612006070001', '01', '01:00:00;00', '01:00:30;00', '51204'

	--重啟任務仍然失敗

	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201612006070001'
    update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201612006070001'

	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000038MX'
end

if @case = 201612281605 begin
	exec SP_Q_PREBOOKING_BY_USERID '51204', '1', '5'  -- 查詢新聞已加入清單
	exec SP_Q_PREBOOKING_BY_USERID '51204', '1', ''
end

if @case = 201612281739 begin
	select *  from TBLOG_VIDEO where FSVIDEO_PROG = '000037X1' --201612150084
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000037X1'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000037X1' --T
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000037X1'  --not schedule yet
end

if @case = 201612291111 begin
    select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002JFS' --X
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002JFS' --not schedule
end

if @case = 201701031720 begin  --HD partial retrieve
	select * from TBTRACKINGLOG where FDTIME > '2017-01-03 17:22' and (FSMESSAGE like '%G201680200190003%' or FSMESSAGE like '%151773%' or FSMESSAGE like '%205663%' or FSMESSAGE like '%102990%') 
	order by FDTIME

	select * from TBTRACKINGLOG where FDTIME > '2017-01-03 17:22' order by FDTIME
	select * from TBTRANSCODE_PARTIAL where FDCREATE_DATE > '2017-01-03' order by FDCREATE_DATE
	select * from TBTRANSCODE where FNPARTIAL_ID = '150533'

	select * from TBBOOKING_MASTER where FDBOOKING_DATE > '2017/01/03'
	select * from TBTRANSCODE where FNPARTIAL_ID = '150489'
	select * from TBTRANSCODE_PARTIAL where FNPARTIAL_ID = '150489'
end

if @case = 201701040900 begin --由 VideoID 查節目資料
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '000036ST'
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in ('000036ST', '000037MD', '0000391J', '000037X1', '0000392Y')
	select * from VW_FILE_NAME
	select * from TBPGM_COMBINE_QUEUE --268156
	select top 10 * from TBPGM_ARR_PROMO --1104048
	select * from TBBROAD_CAST

	SELECT  FSBRO_ID, tmpName, FNEPISODE, SUBSTRING( FSVIDEO_ID, 3 , 6) as FSVIDEO_PROG
		from (select h.FSBRO_ID FSBRO_ID, h.FSVIDEO_ID FSVIDEO_ID , h.FDCREATED_DATE, h.FCSTATUS, t.FSID FSID, t.FNEPISODE , t.FSTYPE FSTYPE, t.FSARC_TYPE FSARC_TYPE, t.FCFILE_STATUS FCFILE_STATUS 
				from TBLOG_VIDEO_HD_MC h join TBLOG_VIDEO t on h.FSVIDEO_ID <> '' and (h.FSVIDEO_ID = t.FSVIDEO_PROG)) th
                join ( select FSPROG_ID tmpID, FSPGMNAME tmpName from TBPROG_M union (select FSPROMO_ID tmpID , FSPROMO_NAME tmpName from TBPGM_PROMO )) u
                on th.FSID = u.tmpID
                where th.FSTYPE in ('P', 'G')
					and th.FSARC_TYPE in ('002', '001')
					and FSVIDEO_ID in ('000036ST', '000037X1', '0000392Y')
                order by FDCREATED_DATE desc

	select * from TBLOG_VIDEO where FSVIDEO_PROG = '0000391J'  --X
	select * from TBLOG_VIDEO where FSVIDEO_PROG in ('000036ST', '000037X1', '0000392Y')
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('000036ST', '000037X1', '0000392Y')
end


if @case = 201701041111-201609220916 begin  --study TBTSM_JOB
	select * from TBTRAN_LOG where CAST(FDDATE as DATE) = CAST(CURRENT_TIMESTAMP - 1 as DATE) order by FDDATE
	/*
	G20160920.log
	2016/9/20 上午 12:01:46 : 開始取得檔案:\\mamstream\MAMDFS\hakkatv\HVideo\2009\2009299\G200929905420002.mxf:在TSM狀態
	2016/9/20 上午 12:01:58 : 檔案:\\mamstream\MAMDFS\hakkatv\HVideo\2009\2009299\G200929905420002.mxf:在TSM狀態為[1]需要呼叫TSM-ReCall
	2016/9/20 上午 12:01:58 : 檔案:\\mamstream\MAMDFS\hakkatv\HVideo\2009\2009299\G200929905420002.mxf:呼叫TSM-ReCall,JOBID=190662
	2016/9/20 上午 12:07:54 : 檔案:\\mamstream\MAMDFS\hakkatv\HVideo\2009\2009299\G200929905420002.mxf:在TSM狀態為[0]可以開始複製
	2016/9/20 上午 02:10:34 : 開始取得檔案:\\mamstream\MAMDFS\hakkatv\HVideo\2009\2009299\G200929905420002.mxf:在TSM狀態
	2016/9/20 上午 02:10:41 : 檔案:\\mamstream\MAMDFS\hakkatv\HVideo\2009\2009299\G200929905420002.mxf:在TSM狀態為[1]需要呼叫TSM-ReCall
	2016/9/20 上午 02:10:41 : 檔案:\\mamstream\MAMDFS\hakkatv\HVideo\2009\2009299\G200929905420002.mxf:呼叫TSM-ReCall,JOBID=190675
	2016/9/20 上午 02:15:04 : 檔案:\\mamstream\MAMDFS\hakkatv\HVideo\2009\2009299\G200929905420002.mxf:在TSM狀態為[0]可以開始複製
	2016/9/20 上午 02:18:15 : 檔案:\\mamstream\MAMDFS\hakkatv\HVideo\2009\2009299\G200929905420002.mxf:在TSM狀態為[1]需要呼叫TSM-ReCall
	2016/9/20 上午 02:18:15 : 檔案:\\mamstream\MAMDFS\hakkatv\HVideo\2009\2009299\G200929905420002.mxf:呼叫TSM-ReCall,JOBID=190676
	2016/9/20 上午 02:19:17 : 檔案:\\mamstream\MAMDFS\hakkatv\HVideo\2009\2009299\G200929905420002.mxf:在TSM狀態為[0]可以開始複製
	2016/9/20 上午 02:19:25 : 開始複製檔案:\\mamstream\MAMDFS\hakkatv\HVideo\2009\2009299\G200929905420002.mxf:到待播區
	2016/9/20 上午 02:25:00 : 檔案:\\mamstream\MAMDFS\hakkatv\HVideo\2009\2009299\G200929905420002.mxf:拷貝到待播區完成
	*/

	select top 100 * from TBTSM_JOB where CAST(FDREGISTER_TIME as DATE) = '2016-09-21' order by FDREGISTER_TIME --100 JOB
	select * from TBTSM_JOB where CAST(FDREGISTER_TIME as DATE) = '2016-09-21' and FSSOURCE_PATH like '%mxf%' order by FDREGISTER_TIME --74 從GPFS 搬檔

	select distinct FSSOURCE_PATH, FNSTATUS, FSNOTIFY_ADDR from TBTSM_JOB where CAST(FDREGISTER_TIME as DATE) = '2016-09-21' and FSSOURCE_PATH like '%mxf%' --69
	--190911, 190913 FSNOTIFY_ADDR = http://mam.pts.org.tw/WS/WSASC/Handler_TsmNotifyReceiver.ashx  調用？

	select distinct FSSOURCE_PATH, FNSTATUS, FSNOTIFY_ADDR from TBTSM_JOB
	where CAST(FDREGISTER_TIME as DATE) = '2017-01-03' and (FSSOURCE_PATH like '%mxf%' and FSSOURCE_PATH like '%G%') --40 支節目

	select FSSOURCE_PATH, FNSTATUS, FSNOTIFY_ADDR from TBTSM_JOB
	where CAST(FDREGISTER_TIME as DATE) = '2016-09-21' and (FSSOURCE_PATH like '%mxf%' and FSSOURCE_PATH like '%G%') group by FSSOURCE_PATH, FNSTATUS, FSNOTIFY_ADDR --40

	select * from TBTSM_JOB where FDREGISTER_TIME > '2017-01-04'

	select FSSOURCE_PATH, count(FSSOURCE_PATH) RESEND from TBTSM_JOB
	where CAST(FDREGISTER_TIME as DATE) = '2016-09-21' and (FSSOURCE_PATH like '%mxf%' and FSSOURCE_PATH like '%G%') group by FSSOURCE_PATH, FNSTATUS, FSNOTIFY_ADDR

	select * from TBTRANSCODE_RECV_LOG where CAST(FDTIME as DATE) between '2016-09-19' and '2016-09-22' and FSCONTENT like '%190913%'

	--G200929905420002 從片庫調檔案
	select top 100 * from TBTSM_JOB where CAST(FDREGISTER_TIME as DATE) between '2016-09-19' and '2016-09-22' and FSSOURCE_PATH like '%G200929905420002%' 	order by FDREGISTER_TIME

	select * from TBTRANSCODE where FDSTART_TIME > '2017-01-01' and FNTYPE = 2 and FSNOTE like '%節目名稱%' and FNANYSTREAM_ID <> -1 order by FDSTART_TIME --44 支節目轉檔入庫

	select * from TBTRANSCODE where FDSTART_TIME between '2016-12-11' and '2016-12-17' and FNTYPE = 2 and FSNOTE like '%節目名稱%' and FNANYSTREAM_ID <> -1 order by FDSTART_TIME -- 139	
	select * from TBTRANSCODE where FDSTART_TIME between '2016-12-18' and '2016-12-24' and FNTYPE = 2 and FSNOTE like '%節目名稱%' and FNANYSTREAM_ID <> -1 order by FDSTART_TIME -- 115

	select CAST(FDSTART_TIME as DATE) 日期, COUNT(FDSTART_TIME) 數量  from TBTRANSCODE where FDSTART_TIME between '2016-12-01' and '2017-01-04' and FNTYPE = 2 and FSNOTE like '%節目名稱%' and FNANYSTREAM_ID <> -1  group by CAST(FDSTART_TIME as DATE)

	select CAST(FDUPDATED_DATE as DATE), COUNT(FCFILE_STATUS) from TBLOG_VIDEO where FDUPDATED_DATE between '2016-12-01' and '2017-01-04' and FCFILE_STATUS = 'T' and FSTYPE = 'G' group by CAST(FDUPDATED_DATE as DATE)

	select CAST(FDUPDATED_DATE as DATE), COUNT(FCFILE_STATUS) from TBLOG_VIDEO where FDUPDATED_DATE between '2016-12-01' and '2017-01-04' 
	and FCFILE_STATUS = 'T' and FSTYPE = 'G' and FSTRANS_FROM like '%來源檔案名稱%' group by CAST(FDUPDATED_DATE as DATE)

	select CAST(FDUPDATED_DATE as DATE), COUNT(FCFILE_STATUS) from TBLOG_VIDEO where FDUPDATED_DATE between '2016-12-01' and '2017-01-04' 
	and FCFILE_STATUS = 'T' and FSTYPE = 'G' and (FSTRANS_FROM like '%影帶擷取%' or FSTRANS_FROM = '') group by CAST(FDUPDATED_DATE as DATE)

	select * from TBLOG_VIDEO where CAST(FDUPDATED_DATE as DATE) = '2017-01-03' and FCFILE_STATUS = 'T' --98

	select * from TBFILE_TYPE

	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000038MX'
end

if @case = 201701041811 begin  -- 挑選短帶功能
	exec SP_Q_TBPGM_PROMO_BY_EFF_ZONE '', '', '', '', '', '12', '2016-09-01', '1111111' --return 819, UI 實際 824 筆 (包涵 RM)
	--20141200350	HD打卡台灣公視按讚-雞籠中元祭2
	select * from TBPGM_PROMO_EFFECTIVE_ZONE where FSPROMO_ID = '20141200350'
	select * from TBLOG_VIDEOID_MAP
	select * from TBPGM_PROMO_BOOKING where FNPROMO_BOOKING_NO = 20380
	select * from TBPGM_PROMO_BOOKING_SETTING where FNPROMO_BOOKING_NO = 20380
	select * from TBZCHANNEL
	select * from TBFILE_TYPE

	--819 包涵 RM
	select distinct 
		A.FSPROMO_ID,A.FSPROMO_NAME,A.FSPROMO_NAME_ENG,A.FSDURATION,
		A.FSPROG_ID,A.FNEPISODE,A.FSPROMOTYPEID,A.FSPROGOBJID,
		A.FSPRDCENID,A.FSPRDYYMM,A.FSPROMOCATID,
		A.FSPROMOCATDID,A.FSPROMOVERID,A.FSMAIN_CHANNEL_ID,F.FSMEMO,
		B.FSBEG_TIME,B.FSEND_TIME,B.FCDATE_TIME_TYPE,D.FSFILE_NO,
		ISNULL(CONVERT(VARCHAR(20) ,B.FDBEG_DATE , 111),'') AS FDBEG_DATE,
		ISNULL(CONVERT(VARCHAR(20) ,B.FDEND_DATE , 111),'') AS FDEND_DATE,
		E.FSVIDEO_ID_PROG FSVIDEO_ID,D.FCFILE_STATUS,D.FCLOW_RES,
		G.FSTIME1,G.FSTIME2,G.FSTIME3,G.FSTIME4,G.FSTIME5,G.FSTIME6,G.FSTIME7,G.FSTIME8,G.FSCMNO
	from TBPGM_PROMO A
			Inner Join TBPGM_PROMO_EFFECTIVE_ZONE B on A.FSPROMO_ID=B.FSPROMO_ID
			Left Join TBZCHANNEL C  on B.FSCHANNEL_ID=C.FSCHANNEL_ID 
			LEFT join TBLOG_VIDEO D on A.FSPROMO_ID=D.FSID and D.FSARC_TYPE=C.FSCHANNEL_TYPE 
			and (D.FCFILE_STATUS='T' or D.FCFILE_STATUS='Y')
			left join TBLOG_VIDEOID_MAP E on A.FSPROMO_ID=E.FSID and C.FSCHANNEL_TYPE=E.FSARC_TYPE
			LEFT Join TBPGM_PROMO_BOOKING F on A.FSPROMO_ID=F.FSPROMO_ID  and  B.FNPROMO_BOOKING_NO=F.FNPROMO_BOOKING_NO
			left join TBPGM_PROMO_BOOKING_SETTING G on F.FNPROMO_BOOKING_NO=G.FNPROMO_BOOKING_NO
	where
	(F.FCSTATUS='Y' or F.FCSTATUS='R')
	and B.FSCHANNEL_ID = '12'
	and A.FSDEL<>'Y'
	and B.FDBEG_DATE <= '2016-09-01'
	and B.FDEND_DATE >= '2016-09-01'
	and B.FSWEEK like '1111111'
	order by FSPROMO_ID

	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002KQ3' --no record
	select * from TBLOG_VIDEO where FSID = '20150900137' --000022YX, 001
	select * from TBPGM_PROMO where FSPROMO_ID = '20150900137' --2015藝起看公視-陳正勝船老大的魚世界1
end

if @case = 201701050920 begin --hang on HDUpload -> Review
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000039Q6' --2017-01-15
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000039VD' --2017-01-08, *.mxf.mxf
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '000039VD'
	-- SOA1 reboot
end

if @case = 201701051403 begin -- 直接拉檔案 MarkUpload -> Approved
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00003A0E' --N -> O
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00003A0E'
end

if @case = 201701051425 begin -- 查詢 TS3500 上架
	select * from TBTRACKINGLOG where FDTIME > '2017-01-05 13:40' order by FDTIME
	select * from TBUSERS where FSUSER_ID = '08255' --陳素娥
end

if @case = 201701072011-201701040900 begin --由 VideoID 查節目資料
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '000036ST'
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in ('000036ST', '000037MD', '0000391J', '000037X1', '0000392Y')
	select * from VW_FILE_NAME

	SELECT  FSBRO_ID, tmpName, FNEPISODE, SUBSTRING( FSVIDEO_ID, 3 , 6) as FSVIDEO_PROG
		from (select h.FSBRO_ID FSBRO_ID, h.FSVIDEO_ID FSVIDEO_ID , h.FDCREATED_DATE, h.FCSTATUS, t.FSID FSID, t.FNEPISODE , t.FSTYPE FSTYPE, t.FSARC_TYPE FSARC_TYPE, t.FCFILE_STATUS FCFILE_STATUS 
				from TBLOG_VIDEO_HD_MC h join TBLOG_VIDEO t on h.FSVIDEO_ID <> '' and (h.FSVIDEO_ID = t.FSVIDEO_PROG)) th
                join ( select FSPROG_ID tmpID, FSPGMNAME tmpName from TBPROG_M union (select FSPROMO_ID tmpID , FSPROMO_NAME tmpName from TBPGM_PROMO )) u
                on th.FSID = u.tmpID
                where th.FSTYPE in ('P', 'G')
					and th.FSARC_TYPE in ('002', '001')
					and FSVIDEO_ID in ('000036ST', '000037X1', '0000392Y')
                order by FDCREATED_DATE desc

	select * from TBLOG_VIDEO where FSVIDEO_PROG = '0000391J'  --X
	select * from TBLOG_VIDEO where FSVIDEO_PROG in ('000036ST', '000037X1', '0000392Y')
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('000036ST', '000037X1', '0000392Y')
end


if @case = 201701080218 begin
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00003A8E' --no data
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00003A8E' --no data
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00003A6V' --20170100179
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00003A6V'

	select * from TBLOG_VIDEO where FSFILE_NO = 'G201013200010001'
end



if @case = 201701091053 begin -- PTS3 比對段落問題
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00003A29' order by FDDATE --有置換過
end

if @case = 201701091644 begin -- 調用失敗
	select * from TBLOG_VIDEO where FSFILE_NO in ('G201242700010019', 'G201018600010004', 'G000006017510005', 'G000006017550005', 'G000006017600003')
	select * from TBBOOKING_MASTER where FSBOOKING_NO in ('201612300044', '201701060007', '201701060008')

	--根據調用單號查詢轉檔失敗的檔案路徑 -> XMIND:MAM DB
	select C.FSFILE_PATH_H ,A.* from TBBOOKING_MASTER A left join TBBOOKING_DETAIL B on A.FSBOOKING_NO = B.FSBOOKING_NO
		left join TBLOG_VIDEO C on B.FSFILE_NO = C.FSFILE_NO
		where A.FSBOOKING_NO in ('201612300044', '201701060007', '201701060008') and FCFILE_TRANSCODE_STATUS = 'F'
end

if @case = 201701101835 begin --刪除 HD 送帶轉檔單
	select * from TBPROG_M where FSPGMNAME like '%按讚社區好過家%' --2012015
	select * from TBLOG_VIDEO where FSID = '2012015' and FNEPISODE >= 8 and FNEPISODE <= 13 and FSARC_TYPE = '002'
end


if @case = 201701130905 begin
	--FCSTATUS : ''=等待上傳, C=已標記上傳, N=待審核,  O=已審核通過
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = @videoID
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = @videoID
	select * from TBLOG_VIDEO where FSVIDEO_PROG = @videoID
	select B.*, A.* from TBLOG_VIDEO A left join (select FSPGMNAME NAME, FSPROG_ID FSID from TBPROG_M union select FSPROMO_NAME NAME, FSPROMO_ID FSID from TBPGM_PROMO) B 
		on A.FSID = B.FSID
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = @videoID
end

if @case = 201701161604 begin  --CHECK TS3310 下架
	select * from TBTRACKINGLOG where FDTIME > '2017-01-16 16:29' and FSMESSAGE like '%GetTs3310IOCount%'  order by FDTIME
	select * from TBTRAN_LOG where FDDATE > '2017/01/16 16:29' order by FDDATE
	select * from TBTSM_SYSOP order by FDREQUEST_TIME
	--使用者08255要從TS3310退出磁帶：A01426L4 ... 共 18 支
	select * from TBTRACKINGLOG where FDTIME > '2017-01-07' and FSMESSAGE like '%要從TS3310退出磁帶%' order by FDTIME --WSTSM/CheckoutTapes_TSM3310, 2017-01-08 15:55:06.563
	select * from TBTRACKINGLOG where FDTIME > '2017-01-18 11:45' order by FDTIME
end

if @case = 201701171451 begin
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in ('000038QJ', '000039A3', '000035XN', '000035XP')
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000039VD'
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '000039VD' --P201701000930001
end

if @case = 201701171739 begin
	exec SP_Q_INFO_BY_VIDEOID '00003B45'  --N
	exec SP_Q_INFO_BY_VIDEOID '00003B46' --N

	exec SP_Q_INFO_BY_VIDEOID '000024IJ' --聽聽看 784 T, @Review, but FSSTATUS = '' 檔案已在 Review 但是狀態仍是檔案未到
	--update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N', FSUPDATED_BY = '51204', FDUPDATED_DATE = GETDATE() where FSFILE_NO = 'G000015907840006'

	--轉檔完成 卡在 TSM 處理中
	exec SP_Q_INFO_BY_VIDEOID '000038KO' --O, R, not scheduled, 2016-12-22 18:06:07.000, FCKEYFRAME = Y
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201749400010002'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201749400010002'
    update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201749400010002'

	--關鍵影格不完整重轉
	update TBLOG_VIDEO set FCKEYFRAME = 'N' where FSFILE_NO = 'G201749400010002'
	--HDUpload 檔案已被刪除
end

if @case = 201701181348 begin --SP_Q_GET_CHECK_COMPLETE_LIST_MIX, 取得審核完成要搬移到已審區的檔案LIST
select distinct A.FSPROG_ID as FSID,A.FNEPISODE,D.FSFILE_NO,D.FSVIDEO_ID,B.FCFILE_STATUS,C.FCCHECK_STATUS,D.FCSTATUS,C.FSBRO_ID
	from tbpgm_combine_queue A
	left join TBLOG_Video B on A.FSVIDEO_ID=B.FSVIDEO_PROG and B.FCFILE_STATUS not in('D','X','F')
	left join tbbroadcast C on B.FSBRO_ID=C.FSBRO_ID and C.FCCHECK_STATUS IN ('G','T','U')
	left join TBLOG_Video_HD_MC D on B.FSFILE_NO=D.FSFILE_NO and D.FCSTATUS='O'
	inner join TBZCHANNEL E on A.FSCHANNEL_ID=E.FSCHANNEL_ID and E.FSCHANNEL_TYPE='002'
	where
	A.FDDATE>=getdate()-1 and A.FDDATE<=getdate()+3
	and B.FCFILE_STATUS in ('B','S','R','T','Y')
	and C.FCCHECK_STATUS IN ('G','T','U')
	and D.FCSTATUS='O'
	union
	select distinct A.FSPROMO_ID AS FSID,0,D.FSFILE_NO,D.FSVIDEO_ID,B.FCFILE_STATUS,C.FCCHECK_STATUS,D.FCSTATUS,C.FSBRO_ID
	from TBPGM_ARR_PROMO A
	left join TBLOG_Video B on A.FSVIDEO_ID=B.FSVIDEO_PROG and B.FCFILE_STATUS not in('D','X','F')
	left join tbbroadcast C on B.FSBRO_ID=C.FSBRO_ID and C.FCCHECK_STATUS IN ('G','T','U')
	left join TBLOG_Video_HD_MC D on B.FSFILE_NO=D.FSFILE_NO and D.FCSTATUS='O'
	inner join TBZCHANNEL E on A.FSCHANNEL_ID=E.FSCHANNEL_ID and E.FSCHANNEL_TYPE='002'
	where
	A.FDDATE>=getdate()-1 and A.FDDATE<=getdate()+3
	and B.FCFILE_STATUS in ('B','S','R','T','Y')
	and C.FCCHECK_STATUS IN ('G','T','U')
	and D.FCSTATUS='O'
end

if @case = 201701181359 begin  --手動刪除作業, Hak Idea 客家熱點子
	select B.FDDATE 播出日期, B.FSPLAY_TIME 時間, B.NAME 節目名稱, B.FNEPISODE 集數, A.FSVIDEO_PROG VIDEOID, A.FSFILE_NO 檔案編號 from TBLOG_VIDEO A join VW_PLAYLIST_FROM_NOW B 
	on A.FSVIDEO_PROG = B.FSVIDEO_ID where FSID = '2016335' and A.FNEPISODE in (1,2,3,4,5,7) and A.FSARC_TYPE = '002' and A.FCFILE_STATUS <> 'X' order by B.FDDATE, B.FSPLAY_TIME

	select * from TBLOG_VIDEO where FSID = '2016335' and FNEPISODE in (1,2,3,4,5,7) and FSARC_TYPE = '002' and FCFILE_STATUS <> 'X'  --no episode 7
	select * from TBLOG_VIDEO where FSID = '2016335' order by FNEPISODE
end

if @case = 201701181426 begin --第50屆電視金鐘獎頒獎典禮(3-3), 無關鍵影格 todo
	select * from TBLOG_VIDEO where FSID = '2016212' -- FCKEYFRAME = R
	select *  from TBBROADCAST where FSBRO_ID = '201701170005' --2017-01-17 09:13:00.023
	--將FCKEYFRAME 改為 N APP_KEYFRAME_INTERVAL 會自動重轉，但是仍失敗
	--update TBLOG_VIDEO set FCKEYFRAME = 'N' where FSFILE_NO = 'G201621200010003'
	select * from TBTRACKINGLOG where FDTIME > '2017-01-18 14:30' order by FDTIME
end

if @case = 201701180106 begin
	exec SP_Q_INFO_BY_VIDEOID '00002UUJ'
	exec SP_Q_INFO_BY_VIDEOID '00001P1E'
	exec SP_Q_INFO_BY_VIDEOID '000037KE'
	exec SP_Q_INFO_BY_VIDEOID
	exec SP_Q_INFO_BY_VIDEOID
	exec SP_Q_INFO_BY_VIDEOID
	exec SP_Q_INFO_BY_VIDEOID
end

if @case = 201701180933 begin
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002HL7' order by FDDATE, FSPLAY_TIME
end

if @case = 201701181543 begin --建立置換單 browser 會跳掉
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00003B8C' --G201729300010002, 201701180013, 105年國慶大會特別報導(暫定), F 審核駁回 -> X 刪除
	exec SP_Q_INFO_BY_VIDEOID '00003B8C'
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G201729300010002'
	select * from TBLOG_VIDEO where FSID = '2017293' --201701180096, 00003BAB, G201729300010003
	exec SP_Q_INFO_BY_VIDEOID '00003BAB'

	-- todo 轉好後再置換看看，仍是失敗
	select * from TBPROG_M
end

if @case = 201701190933 begin --查詢節目表狀態
	select u.QUEUE, u.PRGID, u.NAME, u.EPISODE, u.FDDATE, u.FSPLAY_TIME, u.CHANNEL_ID, u.FSVIDEO_ID, v.FCFILE_STATUS from (
		select FNCONBINE_NO QUEUE, FSPROG_ID PRGID, FSPROG_NAME NAME, FDDATE, FSCHANNEL_ID CHANNEL_ID, FNEPISODE EPISODE, FSPLAY_TIME, FSVIDEO_ID from TBPGM_COMBINE_QUEUE where FDDATE >= (GETDATE() - 1)
			UNION (select t.FNARR_PROMO_NO QUEUE, p.FSPROMO_ID PRGID, p.FSPROMO_NAME NAME, t.FDDATE, t.FSCHANNEL_ID, 0 EPISODE, t.FSPLAYTIME, t.FSVIDEO_ID from TBPGM_ARR_PROMO t 
				join TBPGM_PROMO p on t.FSPROMO_ID = p.FSPROMO_ID where FDDATE >= (GETDATE() - 1))) u
	left join TBLOG_VIDEO v on u.PRGID = v.FSID and u.EPISODE = v.FNEPISODE and v.FSARC_TYPE = '002' and v.FCFILE_STATUS not in ('X', 'F', 'D')
	order by u.FDDATE, u.CHANNEL_ID, u.FSPLAY_TIME--14773

	select u.QUEUE, u.PRGID, u.NAME, u.EPISODE, u.FDDATE, u.FSPLAY_TIME, u.CHANNEL_ID, u.FSVIDEO_ID, v.FCFILE_STATUS, v.FSARC_TYPE from (
		select FNCONBINE_NO QUEUE, FSPROG_ID PRGID, FSPROG_NAME NAME, FDDATE, FSCHANNEL_ID CHANNEL_ID, FNEPISODE EPISODE, FSPLAY_TIME, FSVIDEO_ID from TBPGM_COMBINE_QUEUE where FDDATE >= (GETDATE() - 1)
			UNION (select t.FNARR_PROMO_NO QUEUE, p.FSPROMO_ID PRGID, p.FSPROMO_NAME NAME, t.FDDATE, t.FSCHANNEL_ID, 0 EPISODE, t.FSPLAYTIME, t.FSVIDEO_ID from TBPGM_ARR_PROMO t 
				join TBPGM_PROMO p on t.FSPROMO_ID = p.FSPROMO_ID where FDDATE >= (GETDATE() - 1))) u
	left join TBLOG_VIDEO v on u.PRGID = v.FSID and u.EPISODE = v.FNEPISODE and v.FSARC_TYPE in ('001', '002') and v.FCFILE_STATUS not in ('X', 'F', 'D')
	order by u.FDDATE, u.CHANNEL_ID, u.QUEUE--16578, 如果一節目有 SD 和 HD 會出現兩筆

	select * from VW_PLAYLIST_FROM_NOW order by FDDATE, FSCHANNEL_ID, FSPLAY_TIME--14750
	select * from TBPGM_COMBINE_QUEUE where CAST(FDDATE as DATE) = '2017-01-19' and FSCHANNEL_ID = '07' order by FSPLAY_TIME  --26:00
	select * from TBPGM_ARR_PROMO where CAST(FDDATE as DATE) = '2017-01-19' and FSCHANNEL_ID = '07' order by FSPLAYTIME
	select * from TBPGM_PROMO where FSPROMO_Id = '20160601207'
	select * from TBLOG_VIDEO where FSID =  '20160601207'
end

if @case = 201701191457 begin --TS3310 連線問題
	select * from TBTRACKINGLOG where FDTIME > '2016-12-01' and FSMESSAGE like '%10.1.254.51%' order by FDTIME
	select * from TBTRACKINGLOG where FDTIME between '2017-01-18 11:45' and '2017-01-18 12:00' order by FDTIME
	exec SP_Q_TBMEDIA_MAPPING
end

if @case = 201701201100 begin
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-01-19'  and 
	(FSMESSAGE like '%153162%' or FSMESSAGE like '%209633%' or FSMESSAGE like '%G201405000010015%') order by FDTIME --2017-01-19 14:33:22.580, Parse: transcodeID = 153162 , jobStatus = PARERROR

	select * from TBTRACKINGLOG where FDTIME between '2017-01-19 09:35'  and '2017-01-19 10:00' order by FDTIME
	select * from TBTRACKINGLOG where FDTIME between '2017-01-19 14:20'  and '2017-01-19 15:00' order by FDTIME
end

if @case = 201701201131 begin --客台大量調用
	select * from TBUSERS where FSUSER = 'chiameilo' --羅乙心, 70824
	select * from TBPROG_M where FSPROG_ID = '2013572' --新丁花開(原名:來去花蓮港)
	select * from TBBOOKING_MASTER where FSUSER_ID = '70824' order by FDBOOKING_DATE --201701190090, 201701190089
	select * from TBBOOKING_DETAIL where FSBOOKING_NO = '201701190090'
	select * from TBBOOKING_DETAIL where FSBOOKING_NO = '201701190089'  --20
end

if @case = 201701201216 begin --檔案未到
	exec SP_Q_INFO_BY_VIDEOID '00003AU4' --已置換
end

if @case = 201701201401 begin --完成帶資料不完整，狀態為回朔不能抽單
	select * from TBLOG_VIDEO where FSID = '2015454'
	select * from TBLOG_VIDEO where FSBRO_ID = '201701200087'
	select * from TBBROADCAST where FSBRO_ID = '201701200087' --no record
	--建了第 3 筆才成功 G201545400010005, 201701200101
	--exec SP_D_TBLOG_VIDEO_BYBROID 'G201580901080002'
	select * from TBTRACKINGLOG where FDTIME between '2017-01-20 14:00' and '2017-01-20 14:22' order by FDTIME
	select * from TBTRAN_LOG where FDDATE between '2017/01/20 14:01' and '2017/01/20 14:04' order by FDDATE
	select B.FSUSER_ChtName ,A.* from TBTRAN_LOG A left join TBUSERS B on A.FSUSER_ID = B.FSUSER_ID where FDDATE between '2017/01/20 14:00' and '2017/01/20 14:22' and FSSP_NAME = 'SP_I_TBBROADCAST' order by FDDATE
end

if @case = 201701201217 begin --調用失敗，手動搬檔
	select * from TBBOOKING_MASTER where FSBOOKING_NO = '201701180010'
	select A.FCFILE_TRANSCODE_STATUS, B.FSFILE_PATH_H , A.* from TBBOOKING_DETAIL A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FSBOOKING_NO = '201701180010' --3 
	select A.FCFILE_TRANSCODE_STATUS, B.FSFILE_PATH_H , A.* from TBBOOKING_DETAIL A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FSBOOKING_NO = '201701180009' --2
end


if @case = 201701201746 begin --SP_Q_GET_MOVE_PENDING_LIST_MIX, 取得要派送到待審區的LIST
	select distinct A.FSPROG_ID as FSID,A.FNEPISODE,D.FSFILE_NO,D.FSVIDEO_ID,B.FCFILE_STATUS,C.FCCHECK_STATUS,D.FCSTATUS,C.FSBRO_ID
		from tbpgm_combine_queue A
		left join TBLOG_Video B on A.FSVIDEO_ID=B.FSVIDEO_PROG and B.FCFILE_STATUS not in('D','X','F')
		left join tbbroadcast C on B.FSBRO_ID=C.FSBRO_ID and C.FCCHECK_STATUS='U'
		left join TBLOG_Video_HD_MC D on B.FSFILE_NO=D.FSFILE_NO and D.FCSTATUS='C'
		inner join TBZCHANNEL E on A.FSCHANNEL_ID=E.FSCHANNEL_ID and E.FSCHANNEL_TYPE='002'
		where 
		A.FDDATE>=getdate()-1 and A.FDDATE<=getdate()+14
		and B.FCFILE_STATUS='B'
		and C.FCCHECK_STATUS='U'
		and D.FCSTATUS='C'
		union
		select distinct A.FSPROMO_ID AS FSID,0,D.FSFILE_NO,D.FSVIDEO_ID,B.FCFILE_STATUS,C.FCCHECK_STATUS,D.FCSTATUS,C.FSBRO_ID
		from TBPGM_ARR_PROMO A
		left join TBLOG_Video B on A.FSVIDEO_ID=B.FSVIDEO_PROG and B.FCFILE_STATUS not in('D','X','F')
		left join tbbroadcast C on B.FSBRO_ID=C.FSBRO_ID and C.FCCHECK_STATUS='U'
		left join TBLOG_Video_HD_MC D on B.FSFILE_NO=D.FSFILE_NO and D.FCSTATUS='C'
		inner join TBZCHANNEL E on A.FSCHANNEL_ID=E.FSCHANNEL_ID and E.FSCHANNEL_TYPE='002'
	where 
	A.FDDATE>=getdate()-1 and A.FDDATE<=getdate()+14
	and B.FCFILE_STATUS='B'
	and C.FCCHECK_STATUS='U'
	and D.FCSTATUS='C'
end

if @case = 201701201756 begin --SP_Q_GET_IN_TAPE_LIBRARY_LIST_MIX, 取得14天內要播出的已入庫的檔案LIST
	select distinct A.FSPROG_ID as FSID,A.FNEPISODE,B.FSFILE_NO,B.FSVIDEO_PROG,B.FCFILE_STATUS,B.FSBRO_ID,C.FCSTATUS,C.FSCREATED_BY,B.FSFILE_PATH_H
		from tbpgm_combine_queue A
		left join TBLOG_Video B on A.FSVIDEO_ID=B.FSVIDEO_PROG and B.FCFILE_STATUS not in('D','X','F')
		left join TBLOG_Video_HD_MC C on B.FSFILE_NO=C.FSFILE_NO 
		inner join TBZCHANNEL E on A.FSCHANNEL_ID=E.FSCHANNEL_ID and E.FSCHANNEL_TYPE='002'
		where 
		A.FDDATE>=getdate()-1 and A.FDDATE<=getdate()+3
		and (B.FCFILE_STATUS='T' or B.FCFILE_STATUS='Y')
		and C.FCSTATUS = 'O'
		union
		select distinct A.FSPROMO_ID AS FSID,0,B.FSFILE_NO,B.FSVIDEO_PROG,B.FCFILE_STATUS,B.FSBRO_ID,C.FCSTATUS,C.FSCREATED_BY,B.FSFILE_PATH_H
			from TBPGM_ARR_PROMO A
			left join TBLOG_Video B on A.FSVIDEO_ID=B.FSVIDEO_PROG and B.FCFILE_STATUS not in('D','X','F')
			left join TBLOG_Video_HD_MC C on B.FSFILE_NO=C.FSFILE_NO 
			inner join TBZCHANNEL E on A.FSCHANNEL_ID=E.FSCHANNEL_ID and E.FSCHANNEL_TYPE='002'
			where 
			A.FDDATE>=getdate()-1 and A.FDDATE<=getdate()+3
			and (B.FCFILE_STATUS='T' or B.FCFILE_STATUS='Y')
			and C.FCSTATUS = 'O'
		union
		select distinct A.FSPROG_ID as FSID,A.FNEPISODE,B.FSFILE_NO,B.FSVIDEO_PROG,B.FCFILE_STATUS,B.FSBRO_ID,C.FCSTATUS,C.FSCREATED_BY,B.FSFILE_PATH_H
			from tbpgm_combine_queue A
			left join TBLOG_Video B on A.FSVIDEO_ID=B.FSVIDEO_PROG and B.FCFILE_STATUS not in('D','X','F')
			left join TBLOG_Video_HD_MC C on B.FSFILE_NO=C.FSFILE_NO 
			inner join TBZCHANNEL E on A.FSCHANNEL_ID=E.FSCHANNEL_ID and E.FSCHANNEL_TYPE='002'
			where 
			A.FDDATE>=getdate()-1 and A.FDDATE<=getdate()+14
			and (B.FCFILE_STATUS='T' or B.FCFILE_STATUS='Y')
			and (C.FCSTATUS is NULL or C.FCSTATUS <> 'O')
		union
			select distinct A.FSPROMO_ID AS FSID,0,B.FSFILE_NO,B.FSVIDEO_PROG,B.FCFILE_STATUS,B.FSBRO_ID,C.FCSTATUS,C.FSCREATED_BY,B.FSFILE_PATH_H
			from TBPGM_ARR_PROMO A
			left join TBLOG_Video B on A.FSVIDEO_ID=B.FSVIDEO_PROG and B.FCFILE_STATUS not in('D','X','F')
			left join TBLOG_Video_HD_MC C on B.FSFILE_NO=C.FSFILE_NO 
			inner join TBZCHANNEL E on A.FSCHANNEL_ID=E.FSCHANNEL_ID and E.FSCHANNEL_TYPE='002'
			where 
			A.FDDATE>=getdate()-1 and A.FDDATE<=getdate()+14
			and (B.FCFILE_STATUS='T' or B.FCFILE_STATUS='Y')
			and (C.FCSTATUS is NULL or C.FCSTATUS <> 'O')

end

if @case = 201701201816 begin
	exec SP_Q_INFO_BY_VIDEOID '00003B1A' --20170100456, B, 音樂萬萬歲--回娘家promo2, 2017-01-22, 12
	-- 呼叫 Web Service 失敗
	-- 手動呼叫 Web Service
end

if @case = 201701230919 begin 
	select * from TBLOG_VIDEO where FSID = '2010294' and FNEPISODE = 143
end

if @case = 201701231005 begin
	select * from TBLOG_VIDEO where FSFILE_NO = 'P201603003910001' --00002FI5
	exec SP_Q_INFO_BY_VIDEOID '00002FI5'
	select * from TBZCHANNEL
end

if @case = 201701231758 begin --no louth DB
	exec SP_Q_INFO_BY_VIDEOID '00002XFF'
end

if @case = 201701241205-201611291110 begin --手動刪除
	select * from TBPROG_M where FSPGMNAME like '%山歌唱來%' --2012025
	select * from TBLOG_VIDEO where FSID = '2012025' and FNEPISODE in (27, 28, 29) and FSARC_TYPE = '002' --G201202500270001, G201202500280001, G201202500290001 ,手動刪除作業 B -> X
	select * from TBLOG_VIDEO where FSID = '2012025' and FNEPISODE between 1 and 60 and FSARC_TYPE = '002' and FCFILE_STATUS = 'B' order by FNEPISODE
	select * from TBLOG_VIDEO where FSID = '2012025' and FNEPISODE = 27 and FSARC_TYPE = '002' -- 手動刪除作業 查不到 27 集?? 000010BW
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '000010BW' --2016-12-17 有排播，先請編表抽節目

	--201701241205 73~92
	select * from TBLOG_VIDEO where FSID = '2012025' and FNEPISODE >= 34 and FSARC_TYPE = '002' and (FCFILE_STATUS = 'B' or FCFILE_STATUS = 'T') order by FNEPISODE
end

if @case = 201701241628 begin
	select * from TBUSERS where FSUSER_ID = '71264'
	select * from TBUSERS where FSUSER_ID = '51204'
end

if @case = 201701251439 begin --20161213 created
	select * from TBPROG_M where FSPGMNAME like '%誰來晚餐8%' --2016802
	select * from TBLOG_PHOTO where FSID = '2016802' and FNEPISODE between 33 and 35 --FSARC_ID = 201612130021, 201612130022
end

select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002OOX'
select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002OOX'
select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002OOX' order by FDDATE


select * from TBLOG_VIDEO where FSID = '2009285' and FNEPISODE between 400 and 500 and FSARC_TYPE <> '001' and FSARC_TYPE <> '002'




--------------------------------------------------------------------BEGIN_PROJ-----------------------------------------------------------------
if @case = 101 begin
	-- 查詢當天已上傳的檔案，包含有播出及未播出的 gruop by TBPTSAP_STATUS_LOG.FDCREATED_DATE
	declare @date DATE = '2017-01-16' -- 上傳日期

	select CAST(q.FDDATE as varchar(20)) + ' ' + MIN(q.FSPLAY_TIME) as 'PLAY TIME', p.FSPGMNAME, t.FNEPISODE, UPPER(s.FSVIDEOID) as VIDEO_ID, t.FSCHANNEL_ID, t.FSFILE_SIZE, 
	s.FDCREATED_DATE '開始上傳', s.FDUPDATE_DATE '上傳完成', DATEDIFF(MINUTE, s.FDCREATED_DATE, s.FDUPDATE_DATE) '時程(MIN)',
	s.FCSTATUS, FSPERCENTAGE, s.FCRESENDTIMES
	from (TBPTSAP_STATUS_LOG s left join TBLOG_VIDEO t on s.FSVIDEOID = t.FSVIDEO_PROG) join TBPROG_M p on t.FSID = p.FSPROG_ID left join TBPGM_COMBINE_QUEUE q on s.FSVIDEOID = q.FSVIDEO_ID
	where t.FSTYPE = 'G' and s.FCSTATUS = 80 and CAST(s.FDUPDATE_DATE as DATE) = @date 
	group by p.FSPGMNAME, s.FSVIDEOID, t.FSID, t.FNEPISODE, t.FSFILE_SIZE, s.FDCREATED_DATE, s.FDUPDATE_DATE, s.FCSTATUS, s.FSPERCENTAGE, s.FCRESENDTIMES, q.FDDATE, t.FSCHANNEL_ID
	order by s.FDCREATED_DATE, s.FDUPDATE_DATE

	/*
	declare @date DATE = '2016-06-21'
	select CAST(q.FDDATE as varchar(20)) + ' ' + MIN(q.FSPLAY_TIME) as 'PLAY TIME', p.FSPGMNAME, t.FNEPISODE, UPPER(s.FSVIDEOID) as VIDEO_ID, t.FSFILE_SIZE, s.FDCREATED_DATE '開始上傳', s.FDUPDATE_DATE '上傳完成', DATEDIFF(MINUTE, s.FDCREATED_DATE, s.FDUPDATE_DATE) '時程(MIN)',
	s.FCSTATUS, FSPERCENTAGE, s.FCRESENDTIMES
	from (TBPTSAP_STATUS_LOG s left join TBLOG_VIDEO t on s.FSVIDEOID = t.FSVIDEO_PROG) join TBPROG_M p on t.FSID = p.FSPROG_ID left join TBPGM_COMBINE_QUEUE q on s.FSVIDEOID = q.FSVIDEO_ID
	where t.FSTYPE = 'G' and s.FCSTATUS = 80 and CAST(s.FDUPDATE_DATE as DATE) = @date 
	group by p.FSPGMNAME, s.FSVIDEOID, t.FSID, t.FNEPISODE, t.FSFILE_SIZE, s.FDCREATED_DATE, s.FDUPDATE_DATE, s.FCSTATUS, s.FSPERCENTAGE, s.FCRESENDTIMES, q.FDDATE
	order by s.FDCREATED_DATE, s.FDUPDATE_DATE
	*/

	-- 查詢當天已上傳的短帶檔案及首播
	declare @date DATE = '2016-06-20' -- 上傳日期
	select CAST(q.FDDATE as varchar(20)) + ' ' + MIN(q.FSPLAYTIME) as 'PLAY TIME', p.FSPROMO_NAME, t.FNEPISODE, UPPER(s.FSVIDEOID) as VIDEO_ID, t.FSFILE_SIZE, s.FDCREATED_DATE '開始上傳', s.FDUPDATE_DATE '上傳完成', DATEDIFF(MINUTE, s.FDCREATED_DATE, s.FDUPDATE_DATE) '時程(MIN)',
	s.FCSTATUS, FSPERCENTAGE, s.FCRESENDTIMES
	from (TBPTSAP_STATUS_LOG s left join TBLOG_VIDEO t on s.FSVIDEOID = t.FSVIDEO_PROG) join TBPGM_PROMO p on t.FSID = p.FSPROMO_ID left join TBPGM_ARR_PROMO q on s.FSVIDEOID = q.FSVIDEO_ID
	where t.FSTYPE = 'P' and s.FCSTATUS = 80 and CAST(s.FDUPDATE_DATE as DATE) = @date 
	group by p.FSPROMO_NAME, s.FSVIDEOID, t.FSID, t.FNEPISODE, t.FSFILE_SIZE, s.FDCREATED_DATE, s.FDUPDATE_DATE, s.FCSTATUS, s.FSPERCENTAGE, s.FCRESENDTIMES, q.FDDATE
	order by s.FDCREATED_DATE desc, s.FDUPDATE_DATE desc
	
	-- 查詢當天已上傳的短帶檔案及播出時間
	declare @date DATE = '2016-06-19' -- 上傳日期
	select CAST(q.FDDATE as varchar(20)) + ' ' + MIN(q.FSPLAYTIME) as 'PLAY TIME', p.FSPROMO_NAME, t.FNEPISODE, UPPER(s.FSVIDEOID) as VIDEO_ID, t.FSFILE_SIZE, s.FDCREATED_DATE '開始上傳', s.FDUPDATE_DATE '上傳完成', DATEDIFF(MINUTE, s.FDCREATED_DATE, s.FDUPDATE_DATE) '時程(MIN)',
	s.FCSTATUS, FSPERCENTAGE, s.FCRESENDTIMES
	from (TBPTSAP_STATUS_LOG s left join TBLOG_VIDEO t on s.FSVIDEOID = t.FSVIDEO_PROG) join TBPGM_PROMO p on t.FSID = p.FSPROMO_ID left join TBPGM_ARR_PROMO q on s.FSVIDEOID = q.FSVIDEO_ID
	where t.FSTYPE = 'P' and s.FCSTATUS = 80 and CAST(s.FDUPDATE_DATE as DATE) = @date 
	group by p.FSPROMO_NAME, s.FSVIDEOID, t.FSID, t.FNEPISODE, t.FSFILE_SIZE, s.FDCREATED_DATE, s.FDUPDATE_DATE, s.FCSTATUS, s.FSPERCENTAGE, s.FCRESENDTIMES, q.FDDATE
	order by s.FDCREATED_DATE desc, s.FDUPDATE_DATE desc

	-- 查詢當天上傳未成功（包含上傳中的短帶檔案)
	declare @date DATE = '2016-10-19' -- 上傳日期
	select CAST(q.FDDATE as varchar(20)) + ' ' + MIN(q.FSPLAYTIME) as 'PLAY TIME', p.FSPROMO_NAME, t.FNEPISODE, UPPER(s.FSVIDEOID) as VIDEO_ID, t.FSFILE_SIZE, s.FDCREATED_DATE '開始上傳', s.FDUPDATE_DATE '上傳完成', DATEDIFF(MINUTE, s.FDCREATED_DATE, s.FDUPDATE_DATE) '時程(MIN)',
	s.FCSTATUS, FSPERCENTAGE, s.FCRESENDTIMES
	from (TBPTSAP_STATUS_LOG s left join TBLOG_VIDEO t on s.FSVIDEOID = t.FSVIDEO_PROG) join TBPGM_PROMO p on t.FSID = p.FSPROMO_ID left join TBPGM_ARR_PROMO q on s.FSVIDEOID = q.FSVIDEO_ID
	where t.FSTYPE = 'P' and s.FCSTATUS <> 80 and CAST(s.FDUPDATE_DATE as DATE) = @date 
	group by p.FSPROMO_NAME, s.FSVIDEOID, t.FSID, t.FNEPISODE, t.FSFILE_SIZE, s.FDCREATED_DATE, s.FDUPDATE_DATE, s.FCSTATUS, s.FSPERCENTAGE, s.FCRESENDTIMES, q.FDDATE
	order by s.FDCREATED_DATE desc, s.FDUPDATE_DATE desc

	declare @date DATE = '2016-06-19'
	select SUBSTRING(FSVIDEOID, 3, 6), * from TBPTSAP_STATUS_LOG where CAST(FDUPDATE_DATE as DATE) = @date and FCSTATUS <> 80 order by FDUPDATE_DATE desc
	select SUBSTRING(FSVIDEO_ID, 3, 6), * from TBLOG_VIDEO_HD_MC where CAST(FDUPDATED_DATE as DATE) = @date and FCSTATUS = 'E' order by FDUPDATED_DATE desc

	select * from TBPTSAP_STATUS_LOG order by FDUPDATE_DATE desc
	select * from TBLOG_VIDEO_HD_MC order by FDUPDATED_DATE desc


	/**************************************************************
		查詢主控運行表
	***************************************************************/
	declare @date DATE = '2016-06-27'
	declare @channel varchar(10) = '01'
	select QUEUE_NO, FSID, FSNAME, SPEC, FDDATE, VIDEO_ID, BRO_ID, STATUS, FILE_NO from
		(select q.FNCONBINE_NO QUEUE_NO, q.FSPROG_ID FSID, q.FSPROG_NAME FSNAME, q.FDDATE, t.FSARC_TYPE SPEC, q.FSVIDEO_ID VIDEO_ID, t.FSBRO_ID BRO_ID, h.FCSTATUS STATUS, t.FSFILE_NO FILE_NO
			from TBPGM_COMBINE_QUEUE q left join TBLOG_VIDEO t on q.FSVIDEO_ID = t.FSVIDEO_PROG
				left join TBLOG_VIDEO_HD_MC h on q.FSVIDEO_ID = h.FSVIDEO_ID where FDDATE = @date and q.FSCHANNEL_ID = @channel 
			UNION ALL (
				select a.FNARR_PROMO_NO QUEUE_NO, a.FSPROMO_ID FSID, p.FSPROMO_NAME FSNAME, a.FDDATE, t.FSARC_TYPE SPEC, a.FSVIDEO_ID VIDEO_ID, t.FSBRO_ID BRO_ID, h.FCSTATUS STATUS, t.FSFILE_NO FILE_NO from TBPGM_ARR_PROMO a join TBPGM_PROMO p on a.FSPROMO_ID = p.FSPROMO_ID 
					left join TBLOG_VIDEO t on a.FSVIDEO_ID = t.FSVIDEO_PROG
					left join TBLOG_VIDEO_HD_MC h on a.FSVIDEO_ID = h.FSVIDEO_ID where a.FDDATE = @date and a.FSCHANNEL_ID = @channel)) u
	order by QUEUE_NO

	select * from VW_PLAYLIST_FROM_NOW where FSCHANNEL_ID = '07' order by FDDATE, FSCHANNEL_ID, QUEUE
	select * from VW_PLAYLIST_FROM_NOW where FSCHANNEL_ID = '12' order by FDDATE, FSCHANNEL_ID, QUEUE
	select * from VW_PLAYLIST_FROM_NOW where FSCHANNEL_ID = '13' order by FDDATE, FSCHANNEL_ID, QUEUE
	select * from VW_PLAYLIST_FROM_NOW where FSCHANNEL_ID = '14' order by FDDATE, FSCHANNEL_ID, QUEUE
	select * from VW_PLAYLIST_FROM_NOW  order by FDDATE, FSCHANNEL_ID, QUEUE
	select * from TBPGM_COMBINE_QUEUE where FSCHANNEL_ID = '07' and FDDATE = '2016-07-25'

	select distinct FSVIDEO_ID from VW_PLAYLIST_FROM_NOW where FSCHANNEL_ID in ('13', '14') and FDDATE < '2016-08-21' --Etere 4 天 2 個頻道的檔案數量 328


	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002OGH'
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '0000283G'
end

if @case = 102 begin --輸出建議刪除清單
------------------------------EXPORT HISTORY ------------------------------------------------------------------------------------------------------------------------
	select SUBSTRING(FSVIDEO_ID, 3, 6), NAME, EPISODE, FSBRO_ID, FSARC_TYPE from
		(select h.FSVIDEO_ID FSVIDEO_ID, pp.NAME, t.FNEPISODE EPISODE, t.FSBRO_ID FSBRO_ID, t.FSARC_TYPE FSARC_TYPE from TBLOG_VIDEO_HD_MC h left join VW_PLAYLIST_FROM_NOW vp on h.FSVIDEO_ID = vp.FSVIDEO_ID join TBLOG_VIDEO t on h.FSVIDEO_ID = t.FSVIDEO_PROG and t.FCFILE_STATUS = 'T'
		join (select m.FSPROG_ID ID, m.FSPGMNAME NAME from TBPROG_M m union all select p.FSPROMO_ID ID, p.FSPROMO_NAME from TBPGM_PROMO p) PP on t.FSID = PP.ID
			where h.FCSTATUS = 'O' and h.FDUPDATED_DATE >= '2016-06-18' and vp.FSVIDEO_ID IS NULL) PURGE --2478 長短帶

	--20160711-20160720
	select SUBSTRING(FSVIDEO_ID, 3, 6), NAME, EPISODE, FSBRO_ID, FSARC_TYPE from
		(select h.FSVIDEO_ID FSVIDEO_ID, pp.NAME, t.FNEPISODE EPISODE, t.FSBRO_ID FSBRO_ID, t.FSARC_TYPE FSARC_TYPE from TBLOG_VIDEO_HD_MC h left join VW_PLAYLIST_FROM_NOW vp on h.FSVIDEO_ID = vp.FSVIDEO_ID join TBLOG_VIDEO t on h.FSVIDEO_ID = t.FSVIDEO_PROG and t.FCFILE_STATUS = 'T'
		join (select m.FSPROG_ID ID, m.FSPGMNAME NAME from TBPROG_M m union all select p.FSPROMO_ID ID, p.FSPROMO_NAME from TBPGM_PROMO p) PP on t.FSID = PP.ID
			where h.FCSTATUS = 'O' and h.FDUPDATED_DATE >= '2016-07-11' and vp.FSVIDEO_ID IS NULL) PURGE --319 長短帶

	--20160719-20160725 (For Louth) 0719 開始未寫入 Louth DB
	select SUBSTRING(FSVIDEO_ID, 3, 6), NAME, EPISODE, FSBRO_ID, FSARC_TYPE from
		(select h.FSVIDEO_ID FSVIDEO_ID, pp.NAME, t.FNEPISODE EPISODE, t.FSBRO_ID FSBRO_ID, t.FSARC_TYPE FSARC_TYPE from TBLOG_VIDEO_HD_MC h left join VW_PLAYLIST_FROM_NOW vp on h.FSVIDEO_ID = vp.FSVIDEO_ID join TBLOG_VIDEO t on h.FSVIDEO_ID = t.FSVIDEO_PROG and t.FCFILE_STATUS = 'T'
		join (select m.FSPROG_ID ID, m.FSPGMNAME NAME from TBPROG_M m union all select p.FSPROMO_ID ID, p.FSPROMO_NAME from TBPGM_PROMO p) PP on t.FSID = PP.ID
			where h.FCSTATUS = 'O' and h.FDUPDATED_DATE >= '2016-07-19' and vp.FSVIDEO_ID IS NULL) PURGE

	--20160721-20160725 (For Etere)
	select SUBSTRING(FSVIDEO_ID, 3, 6), NAME, EPISODE, FSBRO_ID, FSARC_TYPE from
		(select h.FSVIDEO_ID FSVIDEO_ID, pp.NAME, t.FNEPISODE EPISODE, t.FSBRO_ID FSBRO_ID, t.FSARC_TYPE FSARC_TYPE from TBLOG_VIDEO_HD_MC h left join VW_PLAYLIST_FROM_NOW vp on h.FSVIDEO_ID = vp.FSVIDEO_ID join TBLOG_VIDEO t on h.FSVIDEO_ID = t.FSVIDEO_PROG and t.FCFILE_STATUS = 'T'
		join (select m.FSPROG_ID ID, m.FSPGMNAME NAME from TBPROG_M m union all select p.FSPROMO_ID ID, p.FSPROMO_NAME from TBPGM_PROMO p) PP on t.FSID = PP.ID
			where h.FCSTATUS = 'O' and h.FDUPDATED_DATE >= '2016-07-21' and vp.FSVIDEO_ID IS NULL) PURGE

	--20160726-20160805 (For Louth)
	select SUBSTRING(FSVIDEO_ID, 3, 6), NAME, EPISODE, FSBRO_ID, FSARC_TYPE from
		(select h.FSVIDEO_ID FSVIDEO_ID, pp.NAME, t.FNEPISODE EPISODE, t.FSBRO_ID FSBRO_ID, t.FSARC_TYPE FSARC_TYPE from TBLOG_VIDEO_HD_MC h left join VW_PLAYLIST_FROM_NOW vp on h.FSVIDEO_ID = vp.FSVIDEO_ID join TBLOG_VIDEO t on h.FSVIDEO_ID = t.FSVIDEO_PROG and t.FCFILE_STATUS = 'T'
		join (select m.FSPROG_ID ID, m.FSPGMNAME NAME from TBPROG_M m union all select p.FSPROMO_ID ID, p.FSPROMO_NAME from TBPGM_PROMO p) PP on t.FSID = PP.ID
			where h.FCSTATUS = 'O' and h.FDUPDATED_DATE >= '2016-07-26' and vp.FSVIDEO_ID IS NULL) PURGE

	--QC
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in ('00002NTH', '00002PAF', '00002TS0', '00002RVG')
end


if @case = 201607221839 begin -- 調整搬檔時間
	--SP_Q_GET_MOVE_PENDING_LIST_MIX
	--SP_Q_GET_CHECK_COMPLETE_LIST_MIX
	--SP_Q_GET_IN_TAPE_LIBRARY_LIST_MIX
end

if @case = 201701231516 begin

	exec SP_Q_INFO_BY_VIDEOID '00002XFF'
end

/************************************************************
 *******************************PROJECT**********************
*************************************************************/

/************************************************************
louth purge list
*************************************************************/

if @case = 201608111641-201608050837 begin
	select CHARINDEX('ab', 'abcdefg') --1
	select CHARINDEX('bc', 'abcdefg') --2
	select SUBSTRING('abcdefg', 1, 3) --abc
	select SUBSTRING('abcdefg', 0, 3) --ab  傳回長度 = 0 + 3 -1 = 2


	--------------- fail-------------------
	declare @tmpTab TABLE (line varchar(10));
	---接近關鍵字 'with' 的語法不正確。如果這個陳述式是通用資料表運算式、xmlnamespaces 子句或變更追蹤內容子句，則前一個陳述式必須以分號結束
	BULK insert @tmpTab from 'c:\temp\no-louth-db.txt' WITH  
	(
		ROWTERMINATOR = '\n'
	)
	----------------------------------------


	---------- 給 Video ID 清單檢查哪些是有排播的 -------------------
	--- 為什麼 8/11 查會出現 8/10  的  --> 因為 VW_PLAYLIST_FROM_NOW 是給 GETDATE() - 2 的 playlist
	if OBJECT_ID(N'tempdb.dbo.#tempImport', N'U') IS NOT NULL
		DROP TABLE #tempImport

	CREATE TABLE #tempImport (
		line varchar(10)
	)
	--put input file on MAMDB2
	BULK insert #tempImport from 'd:\temp\nolouth.txt' WITH
	(
		ROWTERMINATOR = '\n'
	);

	--select * from #tempImport

	if OBJECT_ID(N'tempdb.dbo.#tmp', N'U') IS NOT NULL
		DROP TABLE #tmp

    select * into #tmp from VW_PLAYLIST_FROM_NOW where 1=0
	--select * from  #tmp

	declare CSR CURSOR for select SUBSTRING(line, 1, LEN(line) - 4) from #tempImport
	declare @videoID varchar(8)

	declare @cnt int = 1

	open CSR
	fetch next from CSR into @videoID
	while (@@FETCH_STATUS = 0)
	begin
		--print CAST(@cnt as varchar(5)) + ' : ' +  @videoID
		if EXISTS(select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = right('00' + @videoID, 8)) begin
			--print CAST(@cnt as varchar(5)) + ' : ' +  @videoID
			insert #tmp select top 1 * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = right('00' + @videoID, 8) order by FDDATE, FSPLAY_TIME, FSCHANNEL_ID
		end
		fetch next from CSR into @videoID
		set @cnt = @cnt + 1
	end

	select * from #tmp

	--------------------------201608120953-----------------------------
	--------------------------改成正面表列 ----------------------------
	if OBJECT_ID(N'tempdb.dbo.#tempImport', N'U') IS NOT NULL
		DROP TABLE #tempImport

	CREATE TABLE #tempImport (
		line varchar(10)
	)
	--put input file on MAMDB2
	BULK insert #tempImport from 'c:\temp\s.txt' WITH
	(
		ROWTERMINATOR = '\n'
	);

	--select SUBSTRING(line, 1, LEN(line) - 4) from #tempImport

	if OBJECT_ID(N'tempdb.dbo.#tmp', N'U') IS NOT NULL
		DROP TABLE #tmp

    --select * into #tmp from VW_PLAYLIST_FROM_NOW where 1=0
	--select * from  #tmp

	declare @tmp TABLE(videoID varchar(8))

	declare CSR CURSOR for select SUBSTRING(line, 1, LEN(line) - 4) from #tempImport
	declare @videoID varchar(8)

	declare @cnt int = 1

	open CSR
	fetch next from CSR into @videoID
	while (@@FETCH_STATUS = 0)
	begin
		--print CAST(@cnt as varchar(5)) + ' : ' +  @videoID
		--set @videoID = SUBSTRING(@videoID, 1, LEN(@videoID) - 4)
		if NOT EXISTS(select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = right('00' + @videoID, 8)) begin
			--print CAST(@cnt as varchar(5)) + ' : ' +  @videoID
			insert @tmp VALUES(@videoID)
		end
		fetch next from CSR into @videoID
		set @cnt = @cnt + 1
	end

	select * from @tmp

	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID in ('000026H5', '000026W4', '000027ZG', '000029SS', '00001ONJ', '00001P91')
end

/*
 * 估算已轉檔入庫的資料量
 */
if @case = 201612271527-201608181145-201608121500 begin
	select * from TBPROG_M where FSPGMNAME like '%水果%' --0000060
	select SUM(CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) -2))) from TBLOG_VIDEO where FSID = '0000060' and FCFILE_STATUS = 'T' --水果的檔案大小 4871G
	select FSFILE_SIZE from TBLOG_VIDEO where FCFILE_STATUS = 'T' and FSTYPE = 'G' -- 11913 files
	select SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2) from TBLOG_VIDEO where FCFILE_STATUS = 'T' and FSTYPE = 'G' --傳遞到 LEFT 或 SUBSTRING 函數的參數長度無效。
	select FSID, FNEPISODE, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2) from TBLOG_VIDEO where FCFILE_STATUS = 'T' and FSTYPE = 'G' and FSFILE_SIZE <> ''  --2014438 896G
	select * from TBPROG_M where FSPROG_ID = '2014438' --PeoPo公民新聞報(2014週末版)
	select * from TBLOG_VIDEO where FSID = '2014438' --896MB

	--todo
	select LEN(FSFILE_SIZE), FSFILE_SIZE from TBLOG_VIDEO where FCFILE_STATUS = 'T' and FSTYPE = 'G' and LEN(FSFILE_SIZE) = 0
	select * from TBLOG_VIDEO where FSFILE_SIZE = '' and FCFILE_STATUS = 'T' --22G

	select SUM(CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2))) from TBLOG_VIDEO where FCFILE_STATUS = 'T' and FSTYPE = 'G' and FSFILE_SIZE <> '' --206286426.8 G = 206286 T?
	
	--估算已轉檔入庫的資料量  328857.246 GB = 328T, 20170123:440T
	select 
	SUM(CASE
			WHEN FSFILE_SIZE like '%GB%' THEN CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2))
			WHEN FSFILE_SIZE like '%MB%' THEN CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2))/1000
		END
	)
	from TBLOG_VIDEO where FCFILE_STATUS = 'T' and FSTYPE = 'G' and FSFILE_SIZE <> ''

	select * from TBLOG_VIDEO where FSFILE_SIZE not like '%GB%' and FSFILE_SIZE not like '%MB%' and FSFILE_SIZE <> ''  --由 othervideo 入庫的 FILE_SIZE 都是以 byte 為單位

	-- 查詢播出帶、完成帶、分軌帶、資料帶檔案大小
	select
		CASE
			WHEN FSFILE_SIZE like '%GB%' THEN CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2))
			WHEN FSFILE_SIZE like '%MB%' THEN CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2))/1000
		END as 'FILE_SIZE(GB)'
	, * from TBLOG_VIDEO
	where (FSFILE_SIZE like '%GB%' or FSFILE_SIZE like '%MB%') and FSFILE_SIZE <> '' and FSARC_TYPE = '002' and FCFILE_STATUS = 'T' and FSTYPE = 'G'
	and FDCREATED_DATE between '2016-12-23' and '2016-12-26' --42



	select B.FSPGMNAME, A.FNEPISODE, A.FDCREATED_DATE,
		CASE
			WHEN FSFILE_SIZE like '%GB%' THEN CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2))
			WHEN FSFILE_SIZE like '%MB%' THEN CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2))/1000
		END as 'FILE_SIZE(GB)'
	, * from TBLOG_VIDEO A left join TBPROG_M B on A.FSID = B.FSPROG_ID
	where (FSFILE_SIZE like '%GB%' or FSFILE_SIZE like '%MB%') and FSFILE_SIZE <> '' and FSARC_TYPE = '002' and FCFILE_STATUS = 'T' and FSTYPE = 'G'
	and FDCREATED_DATE between '2016-12-23' and '2016-12-26'
	order by A.FDCREATED_DATE --

-----------------------------------------------------------------------------------
	select A.FSBRO_ID 轉檔單編號 ,A.FSFILE_NO 檔案編號, B.FSPGMNAME 節目名稱, C.FSPGDNAME 子集名稱 ,C.FNEPISODE 集數, 
		CASE
			WHEN F.FSCHANNEL_ID in ('01', '02', '11') THEN '公視'
			WHEN F.FSCHANNEL_ID = '07' THEN '客家'
			WHEN F.FSCHANNEL_ID = '08' THEN '宏觀'
			WHEN F.FSCHANNEL_ID = '99' THEN '資料帶'
		END '台別'
				
		, E.FSUSER_ChtName 建檔者, A.FDCREATED_DATE 建檔日期, G.FSUSER_ChtName 異動者, D.FDUPDATED_DATE 異動日期,H.FSUSER_ChtName 轉檔者, D.FDTRAN_DATE,

		CASE
			WHEN FSFILE_SIZE like '%GB%' THEN CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2))
			WHEN FSFILE_SIZE like '%MB%' THEN CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2))/1000
		END as '檔案大小(GB)'

		, A.FSTRACK 音軌
	from TBLOG_VIDEO A left join TBPROG_M B on A.FSID = B.FSPROG_ID
	left join TBPROG_D C on A.FSID = C.FSPROG_ID and A.FNEPISODE = C.FNEPISODE
	left join TBBROADCAST D on A.FSBRO_ID = D.FSBRO_ID
	left join TBUSERS E on D.FSCREATED_BY = E.FSUSER_ID
	left join TBZCHANNEL F on B.FSCHANNEL_ID = F.FSCHANNEL_ID
	left join TBUSERS G on D.FSUPDATED_BY = G.FSUSER_ID
	left join TBUSERS H on D.FSTRAN_BY = H.FSUSER_ID

	where (A.FSFILE_SIZE like '%GB%' or A.FSFILE_SIZE like '%MB%') and A.FSFILE_SIZE <> '' and A.FSARC_TYPE = '002' and A.FCFILE_STATUS = 'T' and A.FSTYPE = 'G'
	and A.FDUPDATED_DATE between '2016-12-18' and '2016-12-25'
	and B.FSCHANNEL_ID in ('01', '02', '11')
	order by D.FDUPDATED_DATE desc --272

	----------------------------------------------------------------------------------------------------------
	select * from TBBROADCAST where FSBRO_ID = '201612150073' --世界這YOUNG說
	
	select * from TBPROG_M
	select * from TBLOG_VIDEO where FSID = '2015595' and FNEPISODE = 8

	select * from TBZPROGSPEC  --01:SD, 02:HD
	select * from TBFILE_CATEGORY --1 VIDEO 影, 2 AUDIO 音, 3 PHOTO 圖 ..
	select * from TBFILE_TYPE --001 SD播出帶, 002 HD播出帶, 006 SD完成帶, 007 HD完成帶, 011 SD分軌帶, 012 HD分軌帶, 019 SD資料帶, 020 HD資料帶
	select * from TBZCHANNEL
end





if @case = 201609231151 begin --Support export PTS2 MOD EPG

	--PTS MOD
	select A.PAENO FSPROGID,A.PSDAT FDDATE,A.PSTME FSBTIME,B.FSWEBNAME,A.PBENO FNEPISODE,B.FNTOTEPISODE
		from OPENQUERY(PTS_PROG_MYSQL, 'SELECT  * FROM programX.PSHOW_PTS_MOD where PSDAT>=sysdate()
		and PSDAT<= sysdate()+ interval 30 day') A,TBPROG_M B
		where A.PAENO=B.FSPROG_ID order by A.PSDAT,A.PSTME
	--PTS
	select A.PAENO FSPROGID,A.PSDAT FDDATE,A.PSTME FSBTIME,B.FSWEBNAME,A.PBENO FNEPISODE,B.FNTOTEPISODE
		from OPENQUERY(PTS_PROG_MYSQL, 'SELECT  * FROM programX.PSHOW_PTS where PSDAT>=sysdate()
		and PSDAT<= sysdate()+ interval 14 day') A,TBPROG_M B
		where A.PAENO=B.FSPROG_ID order by A.PSDAT,A.PSTME

	--PTS DIMO
	select A.PAENO FSPROGID,A.PSDAT FDDATE,A.PSTME FSBTIME,B.FSWEBNAME,A.PBENO FNEPISODE,B.FNTOTEPISODE
		from OPENQUERY(PTS_PROG_MYSQL, 'SELECT  * FROM programX.PSHOW_DIMO where PSDAT>=sysdate()
		and PSDAT<= sysdate()+ interval 14 day') A,TBPROG_M B
		where A.PAENO=B.FSPROG_ID order by A.PSDAT,A.PSTME

	select top 100 A.* from OPENQUERY(PTS_PROG_MYSQL, 'SELECT  * FROM programX.PSHOW_DIMO order by PSDAT desc') A
	select * from  OPENQUERY(PTS_PROG_MYSQL, 'SELECT  sysdate()') A -- 2016-09-23 12:12:33

	select * from OPENQUERY(PTS_PROG_MYSQL, 'select * from programX.CHANNEL')
	select * from TBZCHANNEL

	--查詢排檔名稱
	select distinct CHEID from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA') -- DIMO, PTS, PTS_MOD, HD_MOD, HAKA, HD ...
	select top 1000 * from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA')
	select top 1000 * from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA WHERE CHEID = "DIMO"')
	select top 1000 * from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA WHERE CHEID = "DIMO" and PRSDT <= "2016-09-26" and PREDT >= "2016-09-26" and PRTMS <= "06:00" and PRTME >= "06:30" and PRCB0 = 1 order by PRENO DESC')
	select * from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA WHERE CHEID = "PTS" and PRNAM like "%人生劇展%" and PRSDT')

	--Export DIMO EPG
	select A.PAENO FSPROGID,A.PSDAT FDDATE,A.PSTME FSBTIME,B.FSWEBNAME,A.PBENO FNEPISODE,B.FNTOTEPISODE
		from OPENQUERY(PTS_PROG_MYSQL, 'SELECT  * FROM programX.PSHOW_DIMO where PSDAT>=sysdate()
		and PSDAT<= sysdate()+ interval 14 day') A,TBPROG_M B
		where A.PAENO=B.FSPROG_ID order by A.PSDAT,A.PSTME


	--201611211107
	select * from TBPGM_PROG_EPG

	--201701091130
	select * from TBPGM_QUEUE

	select * from TBPGM_PROG_EPG
end


/*
 * SP_Q_GET_PLAY_VIDEOID_LIST : 取得14天內要播出的VIDEO檔案清單
 */
if @case = 201609121209 begin -- 取得14天內要播出的VIDEO檔案清單, SP_Q_GET_PLAY_VIDEOID_LIST
	if @subcase = 201609121209 begin
		select * from TBPGM_QUEUE q inner join TBZCHANNEL c on q.FSCHANNEL_ID = c.FSCHANNEL_ID and c.FSCHANNEL_TYPE = '002' and CAST(FDDATE as DATE) = '2016-09-12'  order by q.FDDATE, q.FSCHANNEL_ID, q.FSBEG_TIME
		select * from TBPGM_QUEUE q inner join TBZCHANNEL c on q.FSCHANNEL_ID = c.FSCHANNEL_ID and c.FSCHANNEL_TYPE = '002' where q.FDDATE >= getdate() - 1 and q.FDDATE <= getdate() + 14 order by q.FDDATE, q.FSCHANNEL_ID, q.FSBEG_TIME --1824
		--883
		select distinct q.FSPROG_ID, q.FNEPISODE, q.FSPROG_NAME from TBPGM_QUEUE q inner join TBZCHANNEL c on q.FSCHANNEL_ID = c.FSCHANNEL_ID and c.FSCHANNEL_TYPE = '002' where q.FDDATE >= getdate() - 1 and q.FDDATE <= getdate() + 14 

		--177
		select distinct q.FSPROG_ID, q.FNEPISODE, q.FSPROG_NAME from TBPGM_QUEUE q 
			left join TBLOG_VIDEO t on q.FSPROG_ID = t.FSID and q.FNEPISODE = t.FNEPISODE and t.FSARC_TYPE = '002' 
			left join TBBROADCAST b on t.FSBRO_ID = b.FSBRO_ID
			left join TBLOG_VIDEO_HD_MC h on t.FSFILE_NO = h.FSFILE_NO 
			inner join TBZCHANNEL c on q.FSCHANNEL_ID = c.FSCHANNEL_ID and c.FSCHANNEL_TYPE = '002' where q.FDDATE >= getdate() - 1 and q.FDDATE <= getdate() + 1

		--131 有些 Live 節目是未審核的
		select distinct q.FSPROG_ID as FSID, q.FNEPISODE, q.FSPROG_NAME from TBPGM_QUEUE q 
			left join TBLOG_VIDEO t on q.FSPROG_ID = t.FSID and q.FNEPISODE = t.FNEPISODE and t.FSARC_TYPE = '002' 
			left join TBBROADCAST b on t.FSBRO_ID = b.FSBRO_ID
			left join TBLOG_VIDEO_HD_MC h on t.FSFILE_NO = h.FSFILE_NO 
			inner join TBZCHANNEL c on q.FSCHANNEL_ID = c.FSCHANNEL_ID and c.FSCHANNEL_TYPE = '002' where q.FDDATE >= getdate() - 1 and q.FDDATE <= getdate() + 1 and h.FSVIDEO_ID is not NULL

		--137
		select distinct q.FSPROG_ID as FSID, q.FNEPISODE, q.FSPROG_NAME as NAME, t.FSVIDEO_PROG as FSVIDEO_ID, t.FCFILE_STATUS, b.FCCHECK_STATUS, h.FCSTATUS, b.FSBRO_ID from TBPGM_QUEUE q 
			left join TBLOG_VIDEO t on q.FSPROG_ID = t.FSID and q.FNEPISODE = t.FNEPISODE and t.FSARC_TYPE = '002' 
			left join TBBROADCAST b on t.FSBRO_ID = b.FSBRO_ID
			left join TBLOG_VIDEO_HD_MC h on t.FSFILE_NO = h.FSFILE_NO 
			inner join TBZCHANNEL c on q.FSCHANNEL_ID = c.FSCHANNEL_ID and c.FSCHANNEL_TYPE = '002' where q.FDDATE >= getdate() - 1 and q.FDDATE <= getdate() + 1 and h.FSVIDEO_ID is not NULL


		-- 短帶
		--678
		select distinct p.FSPROMO_ID  from TBPGM_ARR_PROMO p inner join TBZCHANNEL c on p.FSCHANNEL_ID = c.FSCHANNEL_ID and c.FSCHANNEL_TYPE = '002' where p.FDDATE >= getdate() - 1 and p.FDDATE <= getdate() + 14

		--406
		select distinct p.FSPROMO_ID as FSID from TBPGM_ARR_PROMO p 
		left join TBLOG_VIDEO t on p.FSPROMO_ID = t.FSID and t.FNEPISODE = 0 and t.FSARC_TYPE = '002'
		left join TBBROADCAST b on t.FSBRO_ID = b.FSBRO_ID
		left join TBLOG_VIDEO_HD_MC h on t.FSFILE_NO = h.FSFILE_NO and b.FCCHECK_STATUS = 'U'
		inner join TBZCHANNEL c on p.FSCHANNEL_ID = c.FSCHANNEL_ID and c.FSCHANNEL_TYPE = '002' where p.FDDATE >= getdate() - 1 and p.FDDATE <= getdate() + 1

		--489,  置換會有一個 FSPROMO_ID  有多個 FSFILE_NO
		select distinct p.FSPROMO_ID as FSID, 0, h.FSFILE_NO, h.FSVIDEO_ID, t.FCFILE_STATUS, b.FCCHECK_STATUS, h.FCSTATUS from TBPGM_ARR_PROMO p 
			left join TBLOG_VIDEO t on p.FSPROMO_ID = t.FSID and t.FNEPISODE = 0 and t.FSARC_TYPE = '002'
			left join TBBROADCAST b on t.FSBRO_ID = b.FSBRO_ID
			left join TBLOG_VIDEO_HD_MC h on t.FSFILE_NO = h.FSFILE_NO and b.FCCHECK_STATUS = 'U'
			inner join TBZCHANNEL c on p.FSCHANNEL_ID = c.FSCHANNEL_ID and c.FSCHANNEL_TYPE = '002' where p.FDDATE >= getdate() - 1 and p.FDDATE <= getdate() + 1


		--UNION 629
		select distinct q.FSPROG_ID as FSID, q.FNEPISODE, h.FSFILE_NO, q.FSPROG_NAME as NAME, t.FSVIDEO_PROG as FSVIDEO_ID, t.FCFILE_STATUS, b.FCCHECK_STATUS, h.FCSTATUS, b.FSBRO_ID from TBPGM_QUEUE q 
			left join TBLOG_VIDEO t on q.FSPROG_ID = t.FSID and q.FNEPISODE = t.FNEPISODE and t.FSARC_TYPE in ('001', '002') and (t.FCFILE_STATUS not in ('D', 'X') or t.FCFILE_STATUS is NULL)
			left join TBBROADCAST b on t.FSBRO_ID = b.FSBRO_ID
			left join TBLOG_VIDEO_HD_MC h on t.FSFILE_NO = h.FSFILE_NO 
			inner join TBZCHANNEL c on q.FSCHANNEL_ID = c.FSCHANNEL_ID and c.FSCHANNEL_TYPE = '002' where q.FDDATE >= getdate() - 1 and q.FDDATE <= getdate() + 1 and h.FSVIDEO_ID is not NULL
		UNION
		select distinct p.FSPROMO_ID as FSID, 0, h.FSFILE_NO, pm.FSPROMO_NAME NAME, h.FSVIDEO_ID, t.FCFILE_STATUS, b.FCCHECK_STATUS, h.FCSTATUS, b.FSBRO_ID from TBPGM_ARR_PROMO p 
			left join TBLOG_VIDEO t on p.FSPROMO_ID = t.FSID and t.FNEPISODE = 0 and t.FSARC_TYPE in ('001', '002') and (t.FCFILE_STATUS not in ('D', 'X') or t.FCFILE_STATUS is NULL)
			left join TBBROADCAST b on t.FSBRO_ID = b.FSBRO_ID
			left join TBLOG_VIDEO_HD_MC h on t.FSFILE_NO = h.FSFILE_NO and b.FCCHECK_STATUS = 'U'
			left join TBPGM_PROMO pm on p.FSPROMO_ID = pm.FSPROMO_ID
			inner join TBZCHANNEL c on p.FSCHANNEL_ID = c.FSCHANNEL_ID and c.FSCHANNEL_TYPE = '002' where p.FDDATE >= getdate() - 1 and p.FDDATE <= getdate() + 1


		select * from TBPGM_ARR_PROMO where FSPROMO_ID = '20120600703' order by FDDATE
		select * from TBLOG_VIDEO where FSVIDEO_PROG = '000008NR'
		select * from TBPGM_PROMO where FSPROMO_ID = '20120600703'  --公視人生劇展警示卡1
		select  getdate()

	end

	if @subcase = 201610241056 -- SP_Q_GET_PLAY_VIDEOID_LIST  取得 14 天內要播出的 VIDEO 檔案清單( 包含未審核未入庫) FSARC_TYPE = 002 會有問題，如果該節目是播 SD 會漏掉
		--1799
		select * from TBPGM_QUEUE A 
		left join TBLOG_VIDEO B on A.FSPROG_ID = B.FSID and A.FNEPISODE = B.FNEPISODE and B.FSARC_TYPE = '002' and (B.FCFILE_STATUS not in ('D', 'X') or B.FCFILE_STATUS is NULL)
		left join TBBROADCAST C on B.FSBRO_ID = C.FSBRO_ID
		left join TBLOG_VIDEO_HD_MC D on B.FSFILE_NO = D.FSFILE_NO
		inner join TBZCHANNEL E on A.FSCHANNEL_ID = E.FSCHANNEL_ID and E.FSCHANNEL_TYPE = '002'
		where A.FDDATE >= getdate() - 1 and A.FDDATE <= getdate() + 14
		order by A.FDDATE, A.FSCHANNEL_ID, A.FSBEG_TIME
	
		select * from TBPGM_ARR_PROMO A
		left join TBLOG_VIDEO B on A.FSPROMO_ID = B.FSID and B.FNEPISODE = 0 and B.FSARC_TYPE = '002' and (B.FCFILE_STATUS not in ('D', 'X') or B.FCFILE_STATUS is NULL)
		left join TBBROADCAST C on B.FSBRO_ID = C.FSBRO_ID and C.FCCHECK_STATUS = 'U'
		left join TBLOG_VIDEO_HD_MC D on B.FSFILE_NO = D.FSFILE_NO
		inner join TBZCHANNEL E on A.FSCHANNEL_ID = E.FSCHANNEL_ID and E.FSCHANNEL_TYPE = '002'
		where A.FDDATE >= getdate() - 1 and A.FDDATE <= getdate() + 14
		order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

		--913
		select distinct A.FSCHANNEL_ID, A.FDDATE, A.FSBEG_TIME as PLAYTIME, A.FSPROG_ID as FSID, A.FSPROG_NAME as NAME, A.FNEPISODE, D.FSFILE_NO, D.FSVIDEO_ID, B.FCFILE_STATUS, C.FCCHECK_STATUS, D.FCSTATUS, C.FSBRO_ID from TBPGM_QUEUE A 
		left join TBLOG_VIDEO B on A.FSPROG_ID = B.FSID and A.FNEPISODE = B.FNEPISODE and B.FSARC_TYPE = '002' and (B.FCFILE_STATUS not in ('D', 'X') or B.FCFILE_STATUS is NULL)
		left join TBBROADCAST C on B.FSBRO_ID = C.FSBRO_ID
		left join TBLOG_VIDEO_HD_MC D on B.FSFILE_NO = D.FSFILE_NO
		inner join TBZCHANNEL E on A.FSCHANNEL_ID = E.FSCHANNEL_ID and E.FSCHANNEL_TYPE = '002'
		where A.FDDATE >= getdate() - 1 and A.FDDATE <= getdate() + 14
		union
		select distinct A.FSCHANNEL_ID, A.FDDATE, A.FSPLAYTIME as PLAYTIME, A.FSPROMO_ID as FSID, P.FSPROMO_NAME as NAME, 0, D.FSFILE_NO, D.FSVIDEO_ID, B.FCFILE_STATUS, C.FCCHECK_STATUS, D.FCSTATUS, C.FSBRO_ID from TBPGM_ARR_PROMO A
		left join TBLOG_VIDEO B on A.FSPROMO_ID = B.FSID and B.FNEPISODE = 0 and B.FSARC_TYPE = '002' and (B.FCFILE_STATUS not in ('D', 'X') or B.FCFILE_STATUS is NULL)
		left join TBBROADCAST C on B.FSBRO_ID = C.FSBRO_ID and C.FCCHECK_STATUS = 'U'
		left join TBLOG_VIDEO_HD_MC D on B.FSFILE_NO = D.FSFILE_NO
		left join TBPGM_PROMO P on A.FSPROMO_ID = P.FSPROMO_ID
		inner join TBZCHANNEL E on A.FSCHANNEL_ID = E.FSCHANNEL_ID and E.FSCHANNEL_TYPE = '002'
		where A.FDDATE >= getdate() - 1 and A.FDDATE <= getdate() + 14

		order by FDDATE, FSCHANNEL_ID, PLAYTIME


		-- use TBPGM_COMBINE_QUEUE instead og TBPGM_QUEUE
		select distinct A.FSCHANNEL_ID, A.FDDATE, A.FSPLAY_TIME as PLAYTIME, A.FSPROG_ID as FSID, A.FSPROG_NAME as NAME, A.FNEPISODE, D.FSFILE_NO, D.FSVIDEO_ID, B.FCFILE_STATUS, C.FCCHECK_STATUS, D.FCSTATUS, C.FSBRO_ID from TBPGM_COMBINE_QUEUE A 
		left join TBLOG_VIDEO B on A.FSPROG_ID = B.FSID and A.FNEPISODE = B.FNEPISODE and B.FSARC_TYPE = '002' and (B.FCFILE_STATUS not in ('D', 'X') or B.FCFILE_STATUS is NULL)
		left join TBBROADCAST C on B.FSBRO_ID = C.FSBRO_ID
		left join TBLOG_VIDEO_HD_MC D on B.FSFILE_NO = D.FSFILE_NO
		inner join TBZCHANNEL E on A.FSCHANNEL_ID = E.FSCHANNEL_ID and E.FSCHANNEL_TYPE = '002'
		where A.FDDATE >= getdate() - 1 and A.FDDATE <= getdate() + 14
		union
		select distinct A.FSCHANNEL_ID, A.FDDATE, A.FSPLAYTIME as PLAYTIME, A.FSPROMO_ID as FSID, P.FSPROMO_NAME as NAME, 0, D.FSFILE_NO, D.FSVIDEO_ID, B.FCFILE_STATUS, C.FCCHECK_STATUS, D.FCSTATUS, C.FSBRO_ID from TBPGM_ARR_PROMO A
		left join TBLOG_VIDEO B on A.FSPROMO_ID = B.FSID and B.FNEPISODE = 0 and B.FSARC_TYPE = '002' and (B.FCFILE_STATUS not in ('D', 'X') or B.FCFILE_STATUS is NULL)
		left join TBBROADCAST C on B.FSBRO_ID = C.FSBRO_ID and C.FCCHECK_STATUS = 'U'
		left join TBLOG_VIDEO_HD_MC D on B.FSFILE_NO = D.FSFILE_NO
		left join TBPGM_PROMO P on A.FSPROMO_ID = P.FSPROMO_ID
		inner join TBZCHANNEL E on A.FSCHANNEL_ID = E.FSCHANNEL_ID and E.FSCHANNEL_TYPE = '002'
		where A.FDDATE >= CONVERT(datetime, '2016-11-15 01:00', 121) - 3 and A.FDDATE <= CONVERT(datetime, '2016-11-15 01:00', 121) + 14
		order by FDDATE, FSCHANNEL_ID, PLAYTIME

		select CAST('2016-11-08 01:00' as DATE) -3 --error

		SELECT convert(datetime, '2016-11-08 01:44:11', 121) -3



		select top 100 * from TBPGM_COMBINE_QUEUE order by FDDATE desc

		select * from TBPGM_QUEUE where FSPROG_ID = '2006265' and FNEPISODE = 506
		select getdate() -1

		select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '000030KU'
		select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '000030XU'
		select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '000030KU'
	end

	if @case = 201610111628 begin --輸出公益托播 todo ：寫成 procedure
		select count (distinct pa.FSPLAYTIME) as  播出次數 , p.FSWELFARE as 公益託播單號 , p.FSPROMO_NAME  as 短帶名稱  
			from TBPGM_ARR_PROMO pa , TBPGM_PROMO p where pa.FSPROMO_ID= p.FSPROMO_ID and p.FSWELFARE<>'' group by p. FSWELFARE, p .FSPROMO_NAME  -- 98

		select count (pa.FSPLAYTIME) as  播出次數 , p. FSWELFARE as 公益託播單號 , p.FSPROMO_NAME  as 短帶名稱  
			from TBPGM_ARR_PROMO pa , TBPGM_PROMO p where pa.FSPROMO_ID= p.FSPROMO_ID and p.FSWELFARE<>'' group by p.FSWELFARE, p.FSPROMO_NAME --98

		select p.FSPROMO_NAME 短帶名稱, p.FSWELFARE 公益託播單號 ,pa.FDDATE 播出日期, pa.FSPLAYTIME 播出時間 
			from TBPGM_ARR_PROMO pa join TBPGM_PROMO p on pa.FSPROMO_ID = p.FSPROMO_ID where p.FSWELFARE <> '' and pa.FSCHANNEL_ID = '07' order by pa.FDDATE, pa.FSPLAYTIME

		select p.FSPROMO_NAME 短帶名稱, p.FSWELFARE 公益託播單號 ,pa.FDDATE 播出日期, pa.FSPLAYTIME 播出時間 
			from TBPGM_ARR_PROMO pa join TBPGM_PROMO p on pa.FSPROMO_ID= p.FSPROMO_ID where p.FSWELFARE <> '' and pa.FSCHANNEL_ID = '07' 
			and p.FSPROMO_NAME = '2016公益-接種流感疫苗即刻行動'  order by pa.FDDATE, pa.FSPLAYTIME

		--201611241142
		select * from TBPGM_PROMO where FSPROMO_NAME like '%2016公益-第五屆監察委員補提名候選人推(自)薦宣導%' --20161100255
		select p.FSPROMO_NAME 短帶名稱, p.FSWELFARE 公益託播單號 ,pa.FDDATE 播出日期, pa.FSPLAYTIME 播出時間 
			from TBPGM_ARR_PROMO pa join TBPGM_PROMO p on pa.FSPROMO_ID= p.FSPROMO_ID where p.FSWELFARE <> '' and pa.FSCHANNEL_ID = '07' 
			and p.FSPROMO_ID = '20161100255'  order by pa.FDDATE, pa.FSPLAYTIME


		select * from TBPGM_PROMO where FSPROMO_ID = '20151100336' --感動99綻微光徵件短片-訊息篇
		--no data
		select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID 
		join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20151100336' and A.FSCHANNEL_ID in ('01', '11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

		select * from TBPGM_PROMO where FSPROMO_ID = '20151100340' --感動99綻微光徵件短片-付出篇
		select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID 
		join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20151100340'  and A.FSCHANNEL_ID in ('01', '11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

		select * from TBPGM_PROMO where FSPROMO_ID = '20151100341' --感動99綻微光徵件短片-訊息篇HD
		select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID 
		join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20151100341'  and A.FSCHANNEL_ID in ('11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

		select * from TBPGM_PROMO where FSPROMO_ID = '20151100342' --感動99綻微光徵件短片-付出篇HD
		select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID 
		join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20151100342'  and A.FSCHANNEL_ID in ('11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

		select * from TBPGM_ARR_PROMO where FSPROMO_ID = '20151100342' order by FDDATE

		--201611290900
		select * from TBPGM_PROMO where FSPROMO_ID = '20141200656' --感動99綻微光徵件短片-付出篇HD

		--64
		select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID 
		join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20141200656'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

		exec SP_Q_WELFAREPROMO_PLAYOUT_BY_DATE '01', '2014-01-01', '2016-12-31', '20141200656' --64
		select * from TBPGM_PROMO where FSPROMO_ID = '20141200656'

		--201701131024 客台  瓊慈 客家電視20160701-20161231 公益託播清單 包含日期與播出次數
		select * from TBPGM_PROMO where FSMAIN_CHANNEL_ID = '07' and FSWELFARE <> '' --83

		select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, B.FSWELFARE 公益編號, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID 
		join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where B.FSMAIN_CHANNEL_ID = '07' and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') and B.FSWELFARE <> ''
		and A.FDDATE between '2016-07-01' and '2016-12-31' order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME --3889

		select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, B.FSWELFARE 公益編號, COUNT(A.FSPROMO_ID) 播出次數 from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID 
		join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where B.FSMAIN_CHANNEL_ID = '07' and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') and B.FSWELFARE <> ''
		and A.FDDATE between '2016-07-01' and '2016-12-31' group by A.FSPROMO_ID, B.FSPROMO_NAME, B.FSWELFARE order by A.FSPROMO_ID
		
		exec SP_Q_WELFAREPROMO_PLAYOUT_BY_DATE '07', '2016-07-01', '2016-12-31', '1'
		exec SP_Q_WELFAREPROMO_PLAY_COUNT_BY_DATE '07', '2016-07-01', '2016-12-31'
		select * from TBZCHANNEL
	end

	if @case = 201612011132 begin   --??
		select * from TBZCHANNEL

		select * from PTSPROG.PTSProgram.dbo.TBPROG_M
		select * from INFORMATION_SCHEMA.COLUMNS where TABLE_CATALOG = 'PTSPROG.PTSProgram' and TABLE_NAME = 'TBPROG_M' -- no data
		--查詢節目管理系統 SCHEMA
		select  * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'TBPROG_M' --FSCHANNEL max varchar=100
		select * from  PTSPROG.PTSProgram.INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'TBPROG_M' --FSCHANNEL max char=20

		declare @FSCHANNEL char(20)
		declare @FSPROGID char(7), @FSPGMNAME varchar(80)
		declare CSR CURSOR for select FSPROGID, FSPGMNAME, FSCHANNEL from PTSPROG.PTSProgram.dbo.TBPROG_M

		open CSR
		FETCH NEXT from CSR into @FSPROGID, @FSPGMNAME, @FSCHANNEL
		while (@@FETCH_STATUS = 0) begin
			print SUBSTRING(@FSCHANNEL, 1, 1)
			FETCH NEXT from CSR into @FSPROGID, @FSPGMNAME, @FSCHANNEL
		end --while


		select * from TBPROG_M where FSPROG_ID = '2013400' --电 気 風 暴 城市冒險童話
		select * from PTSPROG.PTSProgram.dbo.TBPROG_M where FSPROGID = '2013400' --? ? 風 暴 城市冒險童話

		/*
		接近 '=' 之處的語法不正確。
		declare @a varchar(10)
		if EXISTS (select @a = FSPROGID from PTSPROG.PTSProgram.dbo.TBPROG_M where FSPROGID = '2013400')
			print 'OK'
		*/

		declare @a varchar(10)
		select @a = FSPROGID from PTSPROG.PTSProgram.dbo.TBPROG_M where FSPROGID = '2013400'
		if (@@ROWCOUNT > 0)
			print @a + ':' + 'OK'

		select FSPROGID, FSPGMNAME, FSCHANNEL from PTSPROG.PTSProgram.dbo.TBPROG_M

		declare @b varchar(10)
		select  @b = FSPROGId from PTSPROG.PTSProgram.dbo.TBPROG_M where FSPROGID = '2012412'
	    if (@@ROWCOUNT > 0)
		   print @b + ':' + 'OK'
		else
	       print 'No Data'

		select * from TBPROG_M where FSPROG_ID = '2012412' --MAM 有資料 PTSProgram 無資料

		select * from TBPROG_M where FSPROG_ID = '2017183'
		select * from PTSPROG.PTSProgram.dbo.TBPROG_M where FSPROGID = '2017183'

		----------------------------------------------------------------------------------------
		declare @MAM_FSCHANNEL varchar(100), @PROG_FSCHANNEL char(30)
		declare @FSPROGID char(7), @FSPGMNAME varchar(80)
		--declare CSR CURSOR for select FSPROGID, FSPGMNAME, FSCHANNEL from PTSPROG.PTSProgram.dbo.TBPROG_M order by FSPROGID
		declare CSR CURSOR for select FSPROG_ID, FSPGMNAME, FSCHANNEL from TBPROG_M where FSCHANNEL like '%08%' order by FSPROG_ID
		declare @outTable TABLE(MACRO varchar(20), FSPROGID varchar(10), FSPGMNAME varchar(100), PROG_FSCHANNEL varchar(100), MAM_FSCHANNEL varchar(100))
		declare @ch1 varchar(20), @ch2 varchar(20)

		declare @macro varchar(5)

		open CSR
		FETCH NEXT from CSR into @FSPROGID, @FSPGMNAME, @MAM_FSCHANNEL
		while (@@FETCH_STATUS = 0) begin
		    select top 1 @PROG_FSCHANNEL = FSCHANNEL from PTSPROG.PTSProgram.dbo.TBPROG_M where FSPROGID = @FSPROGID
			if (@@ROWCOUNT = 0) begin
			    insert into @outTable(MACRO, FSPROGID, FSPGMNAME, PROG_FSCHANNEL, MAM_FSCHANNEL) VALUES('NODATA', @FSPROGID, @FSPGMNAME, '', @MAM_FSCHANNEL)
				FETCH NEXT from CSR into @FSPROGID, @FSPGMNAME, @MAM_FSCHANNEL
				CONTINUE
			end

			set @ch1 = SUBSTRING(@PROG_FSCHANNEL, 1, 7)
			set @ch2 = SUBSTRING(@PROG_FSCHANNEL, 9, 12)
			set @macro = SUBSTRING(@PROG_FSCHANNEL, 8,1)
	
			if (@macro = '1') begin
				set @macro = '-' + @macro + '-'
			   set @PROG_FSCHANNEL = @ch1 + @macro + @ch2
			    --print @FSPROGID + ',' + @FSPGMNAME + ',' + @PROG_FSCHANNEL
			
				insert into @outTable(MACRO, FSPROGID, FSPGMNAME, PROG_FSCHANNEL, MAM_FSCHANNEL) VALUES('', @FSPROGID, @FSPGMNAME, @PROG_FSCHANNEL, @MAM_FSCHANNEL)
			end
			else begin
				set @macro = '-' + @macro + '-'
			    set @PROG_FSCHANNEL = @ch1 + @macro + @ch2
				insert into @outTable(MACRO, FSPROGID, FSPGMNAME, PROG_FSCHANNEL, MAM_FSCHANNEL) VALUES('NOT AVAILABLE', @FSPROGID, @FSPGMNAME, @PROG_FSCHANNEL, @MAM_FSCHANNEL)
			    --print '***' + ' ' + @FSPROGID + ',' + @FSPGMNAME + ',' + @PROG_FSCHANNEL
			end
			set @macro = ''
			set @ch1 = ''
			set @ch2 = ''
			set @PROG_FSCHANNEL = ''
			FETCH NEXT from CSR into @FSPROGID, @FSPGMNAME, @MAM_FSCHANNEL
		end --while

		select * from @outTable order by MACRO desc, FSPROGID


		-----------------201612020821--------------------------
		declare @idx int = 2
		declare @char varchar(3)
		select CAST((100 + @idx) as VARCHAR(3))
		select SUBSTRING(CAST((100 + @idx) as VARCHAR(3)), 2, 3)

		select dbo.FN_Q_SET_CODELIST_TO_PURESTRING('01;02;03;04;05;06;09;10;11;12;13;14;51;07;', 20)
		select dbo.FN_Q_SET_CODELIST_TO_PURESTRING('01;51;02;61;11;07;08;', 20)
		select dbo.FN_Q_SET_CODELIST_TO_PURESTRING('01;12;02;13;11;14;07;62;64;08;51;61;', 20) --11000011001111000000
		select dbo.FN_Q_SET_CODELIST_TO_PURESTRING('01;12;13;14;62;64;08;', 20) --1000,0001,0001,1100,0000 wrong
		select dbo.FN_Q_SET_CODELIST_TO_PURESTRING('01;12;02;13;11;14;07;62;64;', 20) --1100,0010,0011,1100,0000 wrong

		--TEST NVARCHAR to CHAR
		declare @c char(20)
		declare @a nvarchar(100)
		set @a = dbo.FN_Q_SET_CODELIST_TO_PURESTRING('01;12;02;13;11;14;07;62;64;', 20)
		set @c = @a
		print @a
		print @c

		declare @input varchar(100)
		set @input = '01;12;02;13;11;14;07;62;64;08;51;61;'
		declare @char varchar(3) = '02;'
		if (@input like '%' + @char + '%')
			print 'found'

		select top 100 * from TBTRACKINGLOG where FSPROGRAM like '%WriteToPGM-SYSTEM%' order by FDTIME

		select * from TBPROG_M where FSPROG_ID = '2017213'
		select * from PTSPROG.PTSProgram.dbo.TBPROG_M where FSPROGID = '2017213'

		select * from TBPROG_M where FSPGMNAME like '%波西測試%' --2013232
		select * from PTSPROG.PTSProgram.dbo.TBPROG_M where FSPROGID = '2013232'
		select * from TBZCHANNEL

		select * from TBPROPOSAL where FSPROG_ID = '2013232' --FSPRO_ID = 201301070028


		exec SP_Q_TBPROPOSAL '201301070028' --FSCHANNEL = 01
		exec SP_Q_TBPROG_M_CODE
	end

	if @case = 20161225 begin -- switch LOUTH XML path
		--step1 關閉 SOA1  HD_FILE_MANAGER
		--step2 關閉 TWS1  APP_MasterControl_Louth

		update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<LouthXmlPath>\\10.13.200.1\MasterCtrl\MSG\LOUTH\HD-InputXML\</LouthXmlPath>', 
		'<LouthXmlPath>\\10.13.200.5\OtherVideo\LOUTH_DATA_TEST\HD-InputXML\</LouthXmlPath>')

	
		select FSSYSTEM_CONFIG from TBSYSTEM_CONFIG

		--switch back
		update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<LouthXmlPath>\\10.13.200.5\OtherVideo\LOUTH_DATA_TEST\HD-InputXML\</LouthXmlPath>', 
		'<LouthXmlPath>\\10.13.200.1\MasterCtrl\MSG\LOUTH\HD-InputXML\</LouthXmlPath>')

		select videoID from FT_GET_VIDEOID_FROM_TSMJOB('2016-12-25', '2016-12-26')
	end
	
end