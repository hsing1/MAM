declare @case int
use PTS_MAM;

if @case = 201605231213 begin
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) = '2016-5-23'  and FCSTATUS != 'Y' order by FDDATE
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) = '2016-5-23' order by FDDATE desc
	select * from tbmMEDIAIMPORT_HD where CAST(fdCREATE_DATE as DATE) = '2016-05-23' and fsUPDATE_BY = 'fushd1' order by fdCREATE_DATE desc
	select * from tbmMEDIAIMPORT_HD where CAST(fdCREATE_DATE as DATE) = '2016-05-23' order by fdCREATE_DATE desc
	select * from tbmMEDIAIMPORT_HD where fnSEQ = '28504' --【ERR_WRAP】封裝AvidMXF時發生錯誤
	select * from tbmMEDIAIMPORT_HD where CAST(fdCREATE_DATE as DATE) = '2016-05-23'  and fsFILE_PATH like '%機捷滑軌%'  --\\10.1.160.240\AvidBookingUpload\PTS\news70289\【公共電視】1900機捷滑軌生鏽.MXF
	select * from tbmMEDIAIMPORT_HD where CAST(fdCREATE_DATE as DATE) = '2016-05-23'  and fsFILE_PATH like '%歐巴馬%'
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) = '2016-5-23' and FSFILENAME like '%歐巴馬%' --no record
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) = '2016-5-23' and FSFILENAME like '%台股最新%' --no record
	select * from tbmMEDIAIMPORT_HD where CAST(fdCREATE_DATE as DATE) = '2016-05-23'  and fsFILE_PATH like '%台股最新%'

	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) = '2016-5-23' and FSFILENAME like '%特教生被電擊%' --no record
	select * from tbmMEDIAIMPORT_HD where CAST(fdCREATE_DATE as DATE) = '2016-05-23'  and fsFILE_PATH like '%特教生被電擊%'
end

if @case = 201605241356 begin
	select * from TBLOG_VIDEO where FSFILE_NO = '201605240010002'
	select * from TBLOG_VIDEO_LOG where CAST(FDCREATED_DATE as DATE) = '2016-05-24'  and FSFILE_NO = '201605240010002'
	select * from TBLOG_VIDEO_LOG where FSFILE_NO = '201605240010002' --上傳4次有4 筆資料
	select * from TBLOG_VIDEO_D where FSFILE_NO = '201605240010002'
	select COLUMN_NAME, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'TBLOG_VIDEO_LOG'
	select * from TBLOG_VIDEO where FSFILE_NO = '201605240010016'
	select * from TBLOG_VIDEO where FSFILE_NO = '201605240010007'
	select * from TBLOG_VIDEO where FSFILE_NO = '201605240010019'  -- hsing1 upload
	select * from TBLOG_VIDEO where FSFILE_NO = '201605240010045'  --FDEncDate = 2016-05-25 05:09:29.367
end

if @case = 201605241635 begin
		declare @tableName varchar(100) = 'TBPROG_M'
		declare @columnTable TABLE (tableName varchar(200), col_name varchar(200), type varchar(25)) -- column name
		declare @foreignTable TABLE (FKname varchar(300), tableName varchar(200), columnName varchar(200), referenceTable varchar(200), referenceColumn varchar(200))
		declare @primaryTable TABLE (tbName varchar(200), columnName varchar(200))
		declare @tableInfo TABLE(tableName varchar(200), columnName varchar(300), type varchar(20), primaryKey varchar(1), foreignKeyConstraint varchar(300), referencedTable varchar(200), referencedColumn varchar(200))

		--List column name
		INSERT into @columnTable select TABLE_NAME, COLUMN_NAME, DATA_TYPE from INFORMATION_SCHEMA.COLUMNS order by TABLE_NAME
		--select COLUMN_NAME, DATA_TYPE from INFORMATION_SCHEMA.COLUMNS where TABLE_CATALOG = 'MAM' and TABLE_NAME = @tableName

		-- List foreign key
		INSERT into @foreignTable
		SELECT f.name AS ForeignKey, 
		OBJECT_NAME(f.parent_object_id) AS TableName, 
		COL_NAME(fc.parent_object_id, fc.parent_column_id) AS ColumnName, 
		OBJECT_NAME (f.referenced_object_id) AS ReferenceTableName, 
		COL_NAME(fc.referenced_object_id, fc.referenced_column_id) AS ReferenceColumnName 
		FROM sys.foreign_keys AS f
		INNER JOIN sys.foreign_key_columns AS fc 
		ON f.OBJECT_ID = fc.constraint_object_id
		order by tableName

		--select * from @foreignTable

	
		-- List primary key
		INSERT into @primaryTable
		SELECT KU.table_name as tablename,column_name as primarykeycolumn
		FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS TC
		INNER JOIN
		INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KU
		ON TC.CONSTRAINT_TYPE = 'PRIMARY KEY' AND
		TC.CONSTRAINT_NAME = KU.CONSTRAINT_NAME
		ORDER BY KU.TABLE_NAME, KU.ORDINAL_POSITION;

		--select * from @primaryTable

		-- create result table
		INSERT into @tableInfo
		select ct.tableName as 'TABLE NAME', ct.col_name as 'COLUMN NAME',  ct.type as 'TYPE',
		(CASE ISNULL(pt.columnName, '')
			WHEN '' THEN ''
			ELSE 'Y'
		END) as 'PRIMARY KEY', 	
		ISNULL(ft.FKname, '') as 'FOREIGN KEY CONSTRAINT', ISNULL(ft.referenceTable, '') as 'REFERENCED TABLE', ISNULL(ft.referenceColumn, '') as 'REFERENCED COLUMN'
		from (@columnTable ct left outer join @foreignTable ft on ct.col_name = ft.columnName and ct.tableName = ft.tableName) left outer join @primaryTable pt on ct.col_name = pt.columnName and ct.tableName = pt.tbName

		select tableName as 'TABLE NAME', columnName as 'COLUMN NAME', type as 'TYPE', primaryKey as 'PRIMARY KEY', foreignKeyConstraint as 'FOREIGN KEY CONSTRAINT', referencedTable as 'REFERENCED TABLE', referencedColumn 'REFERENCED COLUMN' from @tableInfo
end


if @case = 201605241644 begin  --頻道統計
	select * from RPT_館藏量統計 where FDCAMERA_DATE > '2016-05-01' order by FDCAMERA_DATE desc
	select * from TBLOG_VIDEO where CAST(FDCAMERA_DATE as DATE) = '2016-05-24' order by FDCAMERA_DATE desc
	select * from RPT_館藏量統計 where CAST(FDCAMERA_DATE as DATE) = '2016-05-24' order by FDCAMERA_DATE desc
	select * from RPT_館藏數位檔統計 where CAST(FDCAMERA_DATE as DATE) = '2016-05-24' order by FDCAMERA_DATE desc
	select * from VW_NEWS where CAST(FDCREATED_DATE as DATE) > '2016-05-21' order by FDCREATED_DATE
	select * from TBDIRECTORIES
	select * from TBCodeChannel --頻道別
	select * from TBCodeArcLibrary
	select * from TBCodeArcLocation
	select * from TBCodeAudioType
	select * from TBCodeCategory
	select * from TBCodeCountry
	select * from TBCodeLanguage
	select * from TBCodeLtoNum
	select * from TBCodeSource

	select r.*, c.name from RPT_館藏數位檔統計 r join TBCodeChannel c on r.Channel = c.id where CAST(FDCAMERA_DATE as DATE) = '2016-05-24' order by FDCAMERA_DATE desc

	select top(100) * from RPT_館藏數位檔統計 order by FDCAMERA_DATE

	select 
	(CASE
		WHEN r.Channel = 1 THEN '公視'
		WHEN r.Channel = 2 THEN '客家'
		WHEN r.Channel = 3 THEN '原視'
		WHEN r.Channel = 4 THEN '華視'
		WHEN r.Channel = 5 THEN '宏觀'
		WHEN r.Channel = 6 THEN '其他'
	END) as Channel
	, sum(r.FNFRAMES) as FRAMES, sum(r.FNFRAMES)/(30 * 60) as MIN from RPT_館藏數位檔統計 r join TBCodeChannel c on r.Channel = c.id where CAST(FDCAMERA_DATE as DATE) > '2010-01-01' group by r.Channel order by sum(r.FNFRAMES) desc

	select 
	(CASE
		WHEN r.Channel = 1 THEN '公視'
		WHEN r.Channel = 2 THEN '客家'
		WHEN r.Channel = 3 THEN '原視'
		WHEN r.Channel = 4 THEN '華視'
		WHEN r.Channel = 5 THEN '宏觀'
		WHEN r.Channel = 6 THEN '其他'
	END) as Channel
	, sum(r.FNFRAMES) as FRAMES, sum(r.FNFRAMES)/(30 * 60) as MIN from RPT_館藏數位檔統計 r join TBCodeChannel c on r.Channel = c.id group by r.Channel order by sum(r.FNFRAMES) desc

	select * from TBLOG_VIDEO 



end


if @case = 201605251348 begin --201605250010010 入庫失敗
	select * from TBLOG_VIDEO where FSFILE_NO = '201605250010010' --(錯誤)搜尋高解檔路徑.
	select * from TBLOG_VIDEO_LOG where FSFILE_NO = '201605250010010' --FCWORK_STATUS = U
	select * from VW_NEWS where FSFILE_NO = '201605250010010'
end

if @case = 201605261156 begin
	select Channel, count(FDCAMERA_DATE) from RPT_館藏數位檔統計 group by Channel
	select * from RPT_館藏數位檔統計 where Channel = '1' and RecordType = '拍攝精華'  --4844
	select * from RPT_館藏數位檔統計 where Channel = '2' and RecordType = '拍攝精華'  --15263
	select sum(FNFRAMES)/(30 * 60 * 60) from RPT_館藏數位檔統計 where Channel = '2' and RecordType = '拍攝精華' --3400 hr
	select sum(FNFRAMES)/(30 * 60 * 60) from RPT_館藏數位檔統計 where Channel = '1' and RecordType = '拍攝精華' --2526 hr
	select Channel, sum(FNFRAMES)/(30 * 60 * 60)  from RPT_館藏數位檔統計 group by Channel, RecordType having RecordType = '拍攝精華' order by Channel
	select Channel, sum(FNFRAMES)/(30 * 60 * 60)  from RPT_館藏數位檔統計 group by Channel, RecordType having RecordType = 'SOT' order by Channel

	select 
		(CASE
		WHEN Channel = 1 THEN '公視'
		WHEN Channel = 2 THEN '客家'
		WHEN Channel = 3 THEN '原視'
		WHEN Channel = 4 THEN '華視'
		WHEN Channel = 5 THEN '宏觀'
		WHEN Channel = 6 THEN '其他'
	END) as 頻道, 
	RecordType, SUM(FNFRAMES) / (30 * 60 * 60) DUR from RPT_館藏數位檔統計 group by Channel, RecordType order by Channel, RecordType
end


if @case = 201605261410 begin
	select * from TBVIDEO_RETRIVE where FSFILENAME like '%建山特色學校1900%' --201309090010068 (錯誤)轉成AvidMXF格式錯誤(逾時：578.833333333333)
	select * from TBUSERS where FSUSER_ID = 'news1009'  --黃守銘
	select * from TBVIDEO_RETRIVE where FSFILENAME like '%.m2t%' order by FDDATE desc --*.m2t 無法轉到 Avid  禁漁期止擾1900_1.M2T
	select * from TBVIDEO_RETRIVE where FSFILENAME like '%禁漁期止擾1900%' --news50574, kurt 有調用到 200.7 成功 dur = 4475
end

if @case = 201605270753 begin
	select * from TBLOG_VIDEO where FSTITLE like '%金字塔消失%'
	select * from TBLOG_VIDEO where FSTITLE like '%FRA%PYRAMID%' -- no record
	select * from TBLOG_VIDEO where FSTITLE like '%20160524E%' --no record
	select * from TBLOG_VIDEO where FSTITLE like '%G7%' --no record
end

if @case = 201605301720 begin
	select * from TBVIDEO_RETRIVE where FSFILENAME like '%1900大巨蛋定案%' --(錯誤)轉成AvidMXF格式錯誤(逾時：606.833333333333)
	select * from TBLOG_VIDEO where FSFILE_NO = '201605230010057'
	select * from TBLOG_VIDEO where CAST(FDEncDate as DATE) = '2016-05-30' order by FDEncDate desc -- 5/29 拍攝 5/30 轉檔入庫
end

if @case = 201606161157 begin
	select top 100 * from TBLOG_VIDEO where FSTITLE like '%1900%' and CAST(FDUPDATED_DATE as DATE) >= '2016-06-15' order by FDUPDATED_DATE desc
	select top 100 * from TBLOG_VIDEO where FSTITLE like '%1900國台辦%'
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) = '2016-06-16' and FSFILE_NO = '201605250010102'
end 

if @case = 201606281744 begin
	select * from TBLOG_VIDEO where FSTITLE like '%申訴制度被部門%' --no record
end


if @case = 201607011048 begin
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) = '2016-07-01' order by FDDATE
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) = '2016-06-29' order by FDDATE
end

if @case = 201607041131 begin
	select top 500 * from TBVIDEO_RETRIVE order by FDDATE desc
end

if @case = 201607050951 begin
	select * from TBVIDEO_RETRIVE where FDDATE >= '2016-01-04' order by FDDATE
end

if @case = 201607291224 begin
	select top 10 * from TBVIDEO_RETRIVE where FSFILENAME like '%第十四任總統%'
	--2016-07-29 11:22:02.220 調的檔案 2016-07-29 12:17:49.183 完成
	--2016-07-29 12:09:22.243 調的檔案 2016-07-29 12:17:50.993 完成
end

if @case = 201608041400 begin
	select * from TBVIDEO_RETRIVE where FSFILENAME like '%1900年金聲援%' --(錯誤)轉成AvidMXF格式錯誤(逾時：526.166666666667)
	--FUSHD1 wrape OP1a fail
end

if @case = 201608301159 begin -- 調用到 AvidBooking 是黑畫面
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) = '2016-08-30' and FSUSER_ID = 'news51014' order by FDDATE  -- FSFILE_NO 201608110010152
	select * from TBVIDEO_RETRIVE where FSFILE_NO = '201608110010152'
	select * from TBLOG_VIDEO where FSFILE_NO = '201608110010152' --\\MAMStream\MAMDFS\news\WorkingTemp\Incoming\201608110010152.mxf, \\MAMStream\MAMDFS\news\pts\2016\08\11\201608110010152\
	select * from TBUSERS where FSUSER_ID = 'news50273' --陳昌維
	select * from TBUSERS where FSUSER_ID = 'hahaha1234567'
end

if @case = 201609010916 begin
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) = '2016-08-30' order by FDDATE
end

if @case = 201609051529 begin --調用失敗
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) = '2016-09-05' and FSUSER_ID = 'news1009' order by FDDATE
	select * from TBLOG_VIDEO where FSFILE_NO = '201609020010095' --69955327112 bytes
	select 69955327112/1024/1024/1024 --65G
end

if @case = 201609221509 begin
	select * from TBUSERS where FSUSER = '%chun353%'

	select * from TBUSERS where FSUSER_ID = 'chun3535' -- no record
	select * from TBUSERS where FSUSER = '林阡郡' --no record
	select * from TBUSERS where FSUSER_ID = 'hsing1' --2014-07-12 01:00:04.000
	select * from TBUSERS order by FDCREATED_DATE -- 每天凌晨 1:00 同步
end

if @case = 201610041347 begin
	select * from TBUSERS where FSUSER = '應磊奇' --news51014
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) = '2016-10-04' and FSUSER_ID = 'news51014' order by FDDATE --2016-10-04 15:40:15.420  2000高鐵有雜音
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) = '2016-10-04' and FSFILETYPE_TO = 'SD' order by FDDATE --(錯誤)派送檔案到新聞自動化系統
	select * from TBLOG_VIDEO where FSFILE_NO = '201509150010023' --【公共電視】1900六輕空汙環評, NEWSTRANS3
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) = '2016-10-04' and FSFILE_NO = '201507100010030' --(錯誤)派送檔案到新聞自動化系統, \\10.1.160.240\AvidbookingUpload
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) = '2016-10-04' order by FDDATE
	select * from TBLOG_VIDEO where FSFILE_NO = '200612170010003'

	select * from TBLOG_VIDEO where FSTITLE like '%清竹合併%' --201604190010140
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) = '2016-10-04' and FSFILE_NO = '201604190010140'

	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) = '2016-10-04' and FSFILE_NO = '201610030010033'
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) = '2016-10-04' and FSFILETYPE_TO = 'SD' order by FDDATE

	select * from TBVIDEO_RETRIVE where FSFILE_NO = '200612170010003' order by FDDATE
end

if @case = 201610111200 begin
	select * from TBLOG_VIDEO where FSTITLE like '%20161008客庄走透透81集%' --201610060010019
	select top 100 * from TBLOG_VIDEO order by FDCREATED_DATE desc
	select * from TBApplyJOB_LOG
	select * from TBApprove_Message
	select * from tbmMEDIAIMPORT_HD order by fdCREATE_DATE desc
	select * from TBSYSTEM_CONFIG
	select * from TBTSM_Contents
	select * from tbWORKER
end


if @case = 201610261021 begin  -- 調用失敗：儲存體取回檔案錯誤
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) = '2016-10-26' order by FDDATE --201609290010028
	select * from TBLOG_VIDEO where FSFILE_NO = '201609290010028' --2016-10-06 18:37:08.127 approved

	select * from TBLOG_VIDEO where FSFILE_NO = '201610190010009' --this file is still in news\workingtemp,  FSMoveToArchive = N, FSAPPROVE_STATE = N
	select * from TBVIDEO_RETRIVE where FSFILE_NO = '201610190010009'

	select * from TBLOG_VIDEO where FSFILE_NO = '201610190010033' --FSMoveToArchive = Y, FSAPPROVE_STATE = Y

	select * from TBLOG_VIDEO where FSFILE_NO = '201610250010011'

	select * from TBLOG_VIDEO where FSFILE_NO = '201610260010080'  --檢查是否有入庫，是入到 pts or hakka  \\hsm1\news\hakka\2016\10\26\201610260010080

	select * from TBLOG_VIDEO where FSFILE_NO = '201610010010010'

	select top 100 * from TBLOG_VIDEO order by FDCREATED_DATE desc
end


if @case = 201610281007 begin
	select * from TBLOG_VIDEO where FSCREATED_BY = 'hsing1' order by FDCREATED_DATE
end

if @case = 201611011453 begin

	select * from TBLOG_VIDEO where FSFILE_NO = '201609300010001'
end

if @case = 201611021156 begin
	select * from TBLOG_VIDEO where FSFILE_NO = '201609280010027' --FDCLOW_RES = F
end

if @case = 201611071142 begin  --調用到新聞自動化一直卡在  Prepare  狀態
	--56 服務未開
	select top 100 * from TBVIDEO_RETRIVE order by FDDATE desc
end

if @case = 201611071746 begin --調用到伺服器影音不同步，匯到 Avid  再轉成 MXF 就正常
	select  top 100 * from TBVIDEO_RETRIVE  where FSFILENAME like '%0609糧倉搶水戰%' order by FDDATE desc
	select * from TBVIDEO_PARTIAL
end

if @case = 201611071549 begin
	select * from TBSYSTEM_CONFIG
end

if @case = 201611081728 begin
	select * from TBLOG_VIDEO where CAST(FDCREATED_DATE as DATE) = '2016-11-08' 
	select * from TBLOG_VIDEO where FSTITLE like '%美濃%'
	select * from TBLOG_VIDEO where FSFILE_NO = '201611020010027' --(錯誤)分析媒體檔資訊., 客家新聞雜誌-1022美濃文創老街_HD拍攝精華帶
	select * from TBLOG_VIDEO where FSFILE_NO = '201611020010028' --(錯誤)進行低解轉檔., 客家新聞雜誌-1112南庄十三間老街_HD拍攝精華帶

	select * from TBLOG_VIDEO where CAST(FDCREATED_DATE as DATE) = '2016-11-02'
	select * from TBLOG_VIDEO where CAST(FDCREATED_DATE as DATE) = '2016-11-08' order by FDCREATED_DATE --201611080010015, 201611080010016, 201611080010017
end

if @case = 201611091525 begin
	select * from TBLOG_VIDEO where CAST(FDCREATED_DATE as DATE) = '2016-11-09' order by FDCREATED_DATE 
end

if @case = 201611151220 begin --轉成AvidMXF格式錯誤
	select * from TBVIDEO_RETRIVE where  CAST(FDDATE as DATE) = '2016-11-14' and FCSTATUS = 'F' order by FDDATE --36 筆
	select * from TBVIDEO_RETRIVE where FSFILE_NO in

	-- 大部分檔案有調用成功紀錄
	('201606210010028', '201609150010050', '201609150010051', '201609150010052', '201610280010028', '201611040010081', '201611060010051', '201606140010114', 
	'201603110010027', '201607230010019', '201611060010055', '201611130010044', '201611130010043' )  and FCSTATUS = 'Y' order by FDDATE

	select distinct FSFILE_NO from TBVIDEO_RETRIVE where FSFILE_NO in
	('201606210010028', '201609150010050', '201609150010051', '201609150010052', '201610280010028', '201611040010081', '201611060010051', '201606140010114', 
	'201603110010027', '201607230010019', '201611060010055', '201611130010044', '201611130010043' )  and FCSTATUS = 'Y'
end

if @case = 201611161138 begin --(錯誤)轉成AvidMXF格式錯誤(逾時：564.833333333333)
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'news08276' and CAST(FDDATE as DATE) = '2016-11-16' order by FDDATE
	select * from TBUSERS where FSUSER_ID = 'news08276' --陳立峰
end

if @case = 201611181100 begin
	select * from TBVIDEO_RETRIVE where FSFILE_NO = '201412190010007'
	select * from TBLOG_VIDEO where FSFILE_NO = '201412190010007' --【公共電視】1900各方看偵結
end

if @case = 201612201640 begin
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) = '2016-12-20' and FSCREATED_BY = 'news50278' --邱福財

end

if @case = 201612271813 begin
	select * from TBLOG_VIDEO where FDCREATED_DATE > '2016-12-27' order by FDCREATED_DATE
end

if @case = 201701161751 begin
	select * from TBLOG_VIDEO where FSFILE_NO = '201701150010001' --客家新聞雜誌第73集完成帶
	select * from TBLOG_VIDEO where FSFILE_NO = '201701160010005'
end

if @case = 201702201658 begin  --客台入庫失敗
	select * from TBLOG_VIDEO where CAST(FDCREATED_DATE as DATE) = '2017-02-20' order by FDCREATED_DATE
	select * from TBLOG_VIDEO where FSFILE_NO = '201702100010006' --村民大會0207龍潭茶業拍精
	select * from TBLOG_VIDEO where FSFILE_NO = '201702060010005' --村民大會0202六星客家拍精, (錯誤)搜尋高解檔路徑.
	select * from TBLOG_VIDEO where FSFILE_NO = '201702100010006' --(錯誤)搜尋高解檔路徑.  \\MAMStream\MAMDFS\news\WorkingTemp\Incoming\201702100010006.MXF
	select * from TBLOG_VIDEO where FSFILE_NO = '201702220010001' --ok  村民大會0213東門市場拍精
end

if @case = 201703081809 begin --調用失敗
	select * from TBLOG_VIDEO where FSFILE_NO = '201408150010201' --20140814 當晝新聞(分軌帶/第226集) tapponly
	select * from TBLOG_VIDEO where FSFILE_NO = '201408150010202' --20140814 當晝新聞(完成帶/第226集) tapeonly
end

if @case = 201703211132 begin
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) = '2017-03-21' and FSUSER_ID = 'news50196' --201501250010024, 201510010010028, 201501260010012
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) = '2017-03-21' order by FDDATE
end

if @case = 201703231006 begin
   select * from TBLOG_VIDEO where FSFILE_NO = '201103140010064'
end

if @case = 201704171159 begin
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) = '2017-04-17' order by FDDATE
	select * from TBVIDEO_RETRIVE where FSFILE_NO = '201508250010004' --(錯誤)派送檔案到檔案伺服器
	select * from TBLOG_VIDEO where FSFILE_NO = '201508250010004'
end

if @case = 201704201201 begin
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) = '2017-04-20' and FSUSER_ID = 'news1009'  order by FDDATE --news1009, #0933723257  黃首名
	--201202080010084, (錯誤)從儲存體取回檔案
	--201202080010084, (錯誤)從儲存體取回檔案, 部落好醫生1900
	select * from TBLOG_VIDEO where FSFILE_NO = '201202080010084' --/news/titv/2012/02/08/201202080010084/201202080010084.MOV, tapeonly, dsmrecall fail
	--201702090010010, (錯誤)派送檔案到檔案伺服器 【公共電視】(獨立SOT)第481集 小醫院大醫師, nearline
end

if @case = 201705021705 begin
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'news9456' and CAST(FDDATE as DATE) = '2017-05-02' --201510130010047, (錯誤)從儲存體取回檔案
	select * from TBLOG_VIDEO where FSFILE_NO = '201510130010047' --/news/hakka/2015/10/13/201510130010047/201510130010047.MOV --TapeOnly
	--(錯誤)從儲存體取回檔案
	--201011270010197  /news/titv/2010/11/27/201011270010197/201011270010197.MXF --new1241 ok
	--201304110010115  /news/hakka/2013/04/11/201304110010115/201304110010115.MOV --chun3535
	select * from TBUSERS where FSUSER_ID = 'news9456' --吳翠芹
	select * from TBVIDEO_RETRIVE where FSFILE_NO = '201011270010197'
	select * from TBVIDEO_RETRIVE where FSFILE_NO = '201304110010115'
end

if @case = 201705151658 begin  --調用失敗
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) = '2017-05-15' order by FDDATE
	--201104280010030 (錯誤)從儲存體取回檔案
	select * from TBLOG_VIDEO where FSFILE_NO = '201104280010030' --客演藝工會成立
	--201206080010085 (錯誤)從儲存體取回檔案
	select * from TBLOG_VIDEO where FSFILE_NO = '201206080010085' --1900農粽查價格
end


if @case = 201705261147 begin
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'natashakuo' order by FDDATE
	select * from TBLOG_VIDEO where FSFILE_NO = '201605200010103' --【公共電視】(拍精)520總統就職廣場交接
	select * from TBLOG_VIDEO where FSFILE_NO = '201605200010172' --【公共電視】(拍精)520總統就職
	select * from TBLOG_VIDEO where FSFILE_NO = '201605200010103' --【公共電視】(拍精)520總統就職廣場交接
end

if @case = 201706131059 begin --低解畫面有問題
	select * from TBLOG_VIDEO where FSFILE_NO = '201607250010032' --【公共電視】(拍精)英翔專案-巴拉圭-蔡英文記者茶敘
end

if @case = 201707120952 begin --新聞片庫低解無法播
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) = '2017-07-12' order by FDDATE
	--早上有調用成功
	select * from TBUSERS where FSUSER_ID = 'news50480' --張梓嘉
	select * from TBUSERS where FSUSER_ID = 'newlive0810' --吳其昌
	--201405070010034 翠芹調我們的島 partial 10 min 有下來 
end

if @case = 201707181017 begin
	select * from TBLOG_VIDEO where FSFILE_NO = '201202110010108' --高市飢餓301900 titv tapeonly
	select * from TBVIDEO_RETRIVE where FSFILE_NO = '201202110010108' --(錯誤)從儲存體取回檔案 tsm recall fail
end

if @case = 201707281437 begin  --無法調用 (錯誤)從儲存體取回檔案 tsm recall fail
	select * from TBVIDEO_RETRIVE where FSFILE_NO in ('201404300010167', '201405090010127', '201406140010058')
	--\\10.13.200.7\Retrieve\hakka\news50807\20161218\20140614客家新聞雜誌第388集無字幕分軌帶.mov --> /news/hakka/2014/04/30/201404300010167/201404300010167.mov  tapeonly, access to the object is denied
	--\\10.13.200.7\Retrieve\hakka\news50807\20161218\20140426客家新聞雜誌第381集無字幕分軌帶.mov --> /news/hakka/2014/05/09/201405090010127/201405090010127.mov tapeonly, recall
	--\\10.13.200.7\Retrieve\hakka\news50807\20161218\20140510客家新聞雜誌第383集無字幕分軌帶.mov --> /news/hakka/2014/06/14/201406140010058/201406140010058.mov tapeonly recall
end

if @case = 201708171503 begin --新聞掉用卡住
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) = '2017-08-17' and FSUSER_ID = 'news50324' and FCSTATUS = 'R'  order by FDDATE --0
end

if @case = 201708211716 begin
	select * from TBLOG_VIDEO where FSFILE_NO = '201403240010056' --20140322客家新聞雜誌第376集無字幕分軌帶 --/news/hakka/2014/03/24/201403240010056/201403240010056.mov tapeonly
	select * from TBLOG_VIDEO where FSFILE_NO = '201401030010031' --20131228客家新聞雜誌第364集 無字幕分軌帶 --/news/hakka/2014/01/03/201401030010031/201401030010031.mov tapeonly

	select * from TBVIDEO_RETRIVE where FSFILE_NO = '201403240010056' --(錯誤)從儲存體取回檔案 2017-07-31 11:08:47.817
	--2017-08-10 12:27:36.490 檔案已存放到新聞部檔案伺服器
	select * from TBVIDEO_RETRIVE where FSFILE_NO = '201401030010031' --2017-08-14 17:54:45.413 檔案已存放到新聞部檔案伺服器
end


if @case = 201708241429 begin --磁帶已下架 (錯誤)從儲存體取回檔案
	select * from TBLOG_VIDEO where FSFILE_NO = '201309160010055' --20130914客家新聞雜誌第349集 無字幕分軌帶 /news/hakka/2013/09/16/201309160010055/201309160010055.mov tapeonly recall fali done
	select * from TBLOG_VIDEO where FSFILE_NO = '201309250010127' --20130921客家新聞雜誌第350集 無字幕分軌帶 /news/hakka/2013/09/25/201309250010127/201309250010127.mov tapeonly recall fail done
	select * from TBLOG_VIDEO where FSFILE_NO = '201310020010037' --20131005客家新聞雜誌第352集 無字幕分軌帶 /news/hakka/2013/10/02/201310020010037/201310020010037.mov tapeonly recall fail processing
	select * from TBLOG_VIDEO where FSFILE_NO = '201311150010044' --20131116客家新聞雜誌第358集 無字幕分軌帶/news/hakka/2013/11/15/201311150010044/201311150010044.mov tapeonly recall fail processing
	select * from TBVIDEO_RETRIVE where FSFILE_NO = '201311150010044' --news50807
	select * from TBVIDEO_RETRIVE where FSFILE_NO = '201309160010055' --news50807
	select * from TBUSERS where FSUSER_ID = 'news50807' --吳湘屏
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'news50807' order by FDDATE
end

if @case = 201708251043 begin --客家新聞雜誌-0617 峨眉在野一餐_HD拍攝精華帶入庫檔案不存在
	select * from TBLOG_VIDEO where FSFILE_NO = '201707030010015' --客家新聞雜誌-0617峨眉在野一餐_HD拍攝精華帶
	select * from TBVIDEO_RETRIVE where FSFILE_NO = '201707030010015' --2017-07-27 20:24:25.477 調用就是 (錯誤)從儲存體取回檔案
end

if @case = 201708281745 begin
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'news50000' --201012080010420 (錯誤)從儲存體取回檔案
	select * from TBLOG_VIDEO where FSFILE_NO = '201012080010420' --英嘉年祖魯(無字)
end

if @case = 201708311408 begin
	select top 1 * from TBLOG_VIDEO 
end

if @case = 201709071556 begin
	select * from TBVIDEO_RETRIVE where FSFILE_NO = '201303210010393' --/news/hakka/2013/03/21/201303210010393/201303210010393.MOV
	select * from TBVIDEO_RETRIVE where FSFILE_NO = '201303240010054' --/news/hakka/2013/03/24/201303240010054/201303240010054.MOV
	select * from TBVIDEO_RETRIVE where FSFILE_NO = '201304110010172' --/news/hakka/2013/04/11/201304110010172/201304110010172.MOV
	select * from TBVIDEO_RETRIVE where FSFILE_NO = '201305170010064' --/news/hakka/2013/05/17/201305170010064/201305170010064.MOV
	select * from TBVIDEO_RETRIVE where FSFILE_NO = '201305240010168' --/news/hakka/2013/05/24/201305240010168/201305240010168.MOV
	select * from TBVIDEO_RETRIVE where FSFILE_NO = '201306270010112' --/news/hakka/2013/06/27/201306270010112/201306270010112.MOV
	select * from TBVIDEO_RETRIVE where FSFILE_NO = '201307040010034' --/news/hakka/2013/07/04/201307040010034/201307040010034.MOV
	--offline unavailable B00250L4, B00258L4, D00306L4, D00344L4

	select * from TBLOG_VIDEO where FSFILE_NO in ('201303210010393', '201303240010054', '201304110010172', '201305170010064', '201305240010168', '201306270010112', '201307040010034')
end

if @case = 201709111729 begin --(錯誤)從儲存體取回檔案
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'news50271' and FDDATE > '2017-09-11'
	select * from TBLOG_VIDEO where FSFILE_NO = '201101260010031'
end

if @case = 201709191545 begin
	select * from TBVIDEO_RETRIVE where FDDATE > '2017-09-19' order by FDDATE
	select * from TBVIDEO_RETRIVE where FDDATE > '2017-09-19' and FSUSER_ID = 'news50510'  order by FDDATE
end

if @case = 201709221856 begin
	select * from TBLOG_VIDEO where FSCREATED_BY = 'news50839' and CAST(FDCREATED_DATE as DATE) = '2017-09-22'
end

if @case = 201709231145 begin
	select * from TBSYSTEM_CONFIG
	select * from TBVIDEO_RETRIVE
end


if @case = 201709251417 begin
	select * from TBMACHINE_DETAIL    
end

if @case = 201709261456 begin
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) = '2017-09-26' order by FDDATE
end

if @case = 201709280909 begin
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) = '2017-09-27' order by FDDATE
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) > '2017-09-01' and FSUSER_ID = 'news9456' order by FDDATE
	select * from TBUSERS where FSUSER_ID = 'news9456' --吳翠芹
end

if @case = 201709281705 begin --(錯誤)搜尋高解檔路徑. 上傳失敗 Avid 未連上 \\10.13.200.5
	select * from TBLOG_VIDEO where CAST(FDCREATED_DATE as DATE) = '2017-09-28'
end

if @case = 201709300925 begin
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'news70079' and FDDATE > '2017-09-28' order by FDDATE --201410020010093
	select * from TBLOG_VIDEO where FSFILE_NO = '201410020010093' --《客雜》0704他山之石(一)-愛爾蘭語復興
end

if @case = 201710031647 begin --A00122L4, A00137L4, B00023L4, B00025L4, B00033L4 (錯誤)從儲存體取回檔案
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'news70079' and FDDATE >= '2017-10-03' order by FDDATE --201105040010026
	select * from TBLOG_VIDEO where FSFILE_NO = '201105040010026' --itv70513 世青培訓1900
	select * from TBUSERS where FSUSER_ID = 'itv70513' --原住民電視台/新聞部/採訪組

end

if @case = 201710061839 begin
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'news1009' and FDDATE > '2017-10-06' order by FDDATE 
	select * from TBLOG_VIDEO where FSFILE_NO = '201708100010016' --文府國小-3 檔案未入庫
	select top 900 * from TBLOG_VIDEO order by FDCREATED_DATE desc
	select * from TBLOG_VIDEO where FSFILE_NO = '201707030010011' --FSMoveToArchive = Y
	select * from TBLOG_VIDEO where FSMoveToArchive = 'N'  order by FDCREATED_DATE desc
	select * from TBLOG_VIDEO where FSMoveToArchive = 'Y' and FDCREATED_DATE > '2017-05-01'  order by FDCREATED_DATE
	select * from TBLOG_VIDEO where FSFILE_NO = '201710060010008' ----FSMoveToArchive = N
end


if @case = 201710111149-201710061839 begin
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'news1009' and FDDATE > '2017-10-06' order by FDDATE 
	select * from TBLOG_VIDEO where FSFILE_NO = '201708100010016' --文府國小-3 檔案未入庫
	select top 900 * from TBLOG_VIDEO order by FDCREATED_DATE desc
	select * from TBLOG_VIDEO where FSFILE_NO = '201707030010011' --FSMoveToArchive = Y
	select * from TBLOG_VIDEO where FSMoveToArchive = 'N'  order by FDCREATED_DATE desc
	select * from TBLOG_VIDEO where FSMoveToArchive = 'Y' and FDCREATED_DATE > '2017-05-01'  order by FDCREATED_DATE
	select * from TBLOG_VIDEO where FSFILE_NO = '201710060010008' ----FSMoveToArchive = N

	select * from TBLOG_VIDEO where CAST(FDCREATED_DATE as DATE) = '2017-10-11' order by FDCREATED_DATE --【公共電視】(獨立SOT)第516集 紫爆的天空 201710110010001

	select * from TBLOG_VIDEO where FDCREATED_DATE > '2017-07-01' order by FDCREATED_DATE
end

if @case = 201710161032 begin --手動入庫 GPFS
	select * from TBLOG_VIDEO where FSFILE_NO = '201708150010010' --【公共電視】(獨立拍精)第505集 無人巴士在台灣 1
	select * from TBSUBJECT
	select * from TBLOG_VIDEO_D
	select * from TBLOG_DOC
	select * from TBINEWS
	select * from TBCodeChannel

	if OBJECT_ID(N'tempdb.dbo.#tempImport', N'U') IS NOT NULL  --您沒有大量載入陳述式的使用權限
        DROP TABLE #tempImport

	 CREATE TABLE #tempImport (
		FSFILE_NO varchar(20)
     )

    BULK insert #tempImport from 'D:\temp\news-not-found.txt' WITH  ----您沒有大量載入陳述式的使用權限
        (
               ROWTERMINATOR = '\n'
        );

	select * from #tempImport

	select FSFILE_NO, FSTITLE from TBLOG_VIDEO

	select * from #tempImport A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO order by A.FSFILE_NO
	select A.FSFILE_NO + '.' + FSFILE_TYPE_HV, FSTITLE, FSAPPROVE_STATE, FDAPPROVED_DATE from #tempImport A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO order by A.FSFILE_NO
	select * from TBLOG_VIDEO where FSFILE_NO = '201707030010015' --客家新聞雜誌-0617在峨眉野一餐_HD拍攝精華帶 FSAPPROVE_STATE = 'T'
	select FSFILE_NO, FSAPPROVE_STATE, * from TBLOG_VIDEO where FDCREATED_DATE > '2017-06-01' order by FDCREATED_DATE


if @case = 201710191024 begin
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'news51097' and FDDATE > '2017-10-18' order by FDDATE --201206210010098, (錯誤)從儲存體取回檔案
	select * from TBLOG_VIDEO where FSFILE_NO = '201206210010098' --1900同富避難所 /news/hakka/2012/06/21/201206210010098/201206210010098.MOV
end

if @case = 201710201146 begin --(錯誤)從儲存體取回檔案
	select * from TBUSERS where FSUSER_ID = 'news50415' --'洪炎山 <news50415@hakkatv.org.tw> 客家電視台/新聞部/節目群
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'news50415' and FDDATE > '2017-10-18' order by FDDATE --201112130010052, 201112150010117 (錯誤)從儲存體取回檔案
	select * from TBLOG_VIDEO where FSFILE_NO in ('201112130010052', '201112150010117') --農凱道之夜-拍帶, 201112130010052 2200農凱道之夜, 201112150010117

	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'hakka50879' and FDDATE > '2017-10-18'
	select * from TBUSERS where FSUSER_ID = 'hakka50879' --曾傳翔
end

if @case = 201711021341 begin --檔案都到
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'news1009' and FDDATE > '2017-11-02' 
end

if @case = 201711051224 begin
	select * from TBVIDEO_RETRIVE where FDDATE > '2017-11-05' order by FDDATE
	select * from TBUSERS where FSUSER_ID = 'news50556' --莊志成
	select * from TBLOG_VIDEO where FSFILE_NO = '201408140010079' --台鐵樹林調車場
end

if @case = 201711061051 begin
	select * from TBVIDEO_RETRIVE where FDDATE > '2017-11-06' order by FDDATE
	select * from TBUSERS where FSUSER_ID = 'news70185' --林群展
	select * from TBLOG_VIDEO where FSFILE_NO = '201606200010028' --金色百林莊園(復育基地)
end


if @case = 201711100950 begin --提供新聞檔案長度大於1 小時，內容描述小於 50 個字
	select * from TBUSERS where FCACTIVE = 'N' order by FSDEPT
	update TBUSERS set FCACTIVE = 'Y' where FSUSER_ID = 'news1248'
	select * from TBUSERS where FSDEPT like '%新媒體%' order by FCACTIVE
end

if @case = 201711131119 begin ----新聞檔案長度大於1 小時，內容描述小於 50 個字
	select FSFILE_NO, FSTITLE, LEN(FSDESCRIPTION) DESCRIPT_LEN,  FNFILE_SIZE from TBLOG_VIDEO where FDCREATED_DATE > '2017-10-09' and FSAPPROVE_STATE ='Y'
	select FSFILE_NO, FSTITLE, LEN(FSDESCRIPTION) DESCRIPT_LEN, FSDESCRIPTION,  FNFILE_SIZE from TBLOG_VIDEO where FSAPPROVE_STATE ='Y' and LEN(FSDESCRIPTION) < 50 --23804

	select FSFILE_NO 檔案編號, FSTITLE 標題, FSDESCRIPTION 內容, LEN(FSDESCRIPTION) 內容長度, CAST(ROUND(FNFILE_SIZE/POWER(2.0,30), 2) as decimal(6, 2)) "檔案大小(GB)" from TBLOG_VIDEO 
		where FSAPPROVE_STATE ='Y' and LEN(FSDESCRIPTION) < 50 and FNFILE_SIZE > POWER(2.0, 30)  order by FSFILE_NO--11970963328 = 12G  total : 21378

	select FSFILE_NO 檔案編號, FSTITLE 標題, LEN(FSDESCRIPTION) 內容長度, CAST(ROUND(FNFILE_SIZE/POWER(2.0,30), 2) as decimal(6, 2)) "檔案大小(GB)" from TBLOG_VIDEO 
		where FSAPPROVE_STATE ='Y' and LEN(FSDESCRIPTION) < 50 and FNFILE_SIZE > POWER(2.0, 30)  order by FSFILE_NO--11970963328 = 12G  total : 21378

	--新聞檔案大於 1G 內容描述小於 50 個字
	select FSFILE_NO 檔案編號, FSTITLE 標題, REPLACE(FSDESCRIPTION, char(13) + char(10), '') 內容, LEN(FSDESCRIPTION) 內容長度, CAST(ROUND(FNFILE_SIZE/POWER(2.0,30), 2) as decimal(6, 2)) "檔案大小(GB)" from TBLOG_VIDEO 
		where FSAPPROVE_STATE ='Y' and LEN(FSDESCRIPTION) < 50 and FNFILE_SIZE > POWER(2.0, 30)  order by FSFILE_NO--11970963328 = 12G  total : 21378

	--新聞檔案長度大於1 小時，內容描述小於 50 個字
	select FSFILE_NO 檔案編號, FSTITLE 標題, REPLACE(FSDESCRIPTION, char(13) + char(10), '') 內容, LEN(FSDESCRIPTION) 內容長度, FNDURATION ,CAST(ROUND(FNFILE_SIZE/POWER(2.0,30), 2) as decimal(6, 2)) "檔案大小(GB)" from TBLOG_VIDEO 
		where FSAPPROVE_STATE ='Y' and LEN(FSDESCRIPTION) < 50 and FNDURATION > 30*60*60  order by FSFILE_NO --1952

	select * from TBLOG_VIDEO where FSFILE_NO = '201302210010163'
	select * from TBLOG_VIDEO where FSFILE_NO = '201710030010005'
end

if @case = 201711131736 begin -- 新聞調用失敗【公共電視】(外電)南北韓2
	select * from TBVIDEO_RETRIVE where FDDATE >= '2017-11-13' and FSFILE_NO = '201508260010153' order by FDDATE --news50196
	select * from TBLOG_VIDEO where FSTITLE like '%南北韓2%' --【公共電視】(外電)南北韓2 201508260010153
end

if @case = 201711151711 begin --(錯誤)從儲存體取回檔案
	select * from TBLOG_VIDEO where FSFILE_NO = '201609060010011' --北加州天樂語文學校夏令營成果表演
	select * from TBVIDEO_RETRIVE where FDDATE >= '2017-11-15' and FSFILE_NO = '201609060010011' --ptshsiung
end

--WEEK 20171116
if @case = 201711161024 begin --統計新聞片庫時數
	select top 1000 * from RPT_館藏數位檔統計
	select count(channel) from RPT_館藏數位檔統計 where Channel = 1 --147516
	select count(Channel) from RPT_館藏數位檔統計 where Channel = 2 --38284
	select sum(FNFRAMES)/(30 * 60 * 60) from RPT_館藏數位檔統計 where Channel = '2' --10447 hr
	select sum(FNFRAMES)/(30 * 60 * 60) from RPT_館藏數位檔統計 where Channel = '1' --7334 hr
end

if @case = 201711211735 begin --檔案未審核，被刪除
	select * from TBVIDEO_RETRIVE where FSFILE_NO = '201707300010003' --news50838 (錯誤)從儲存體取回檔案
	select * from TBLOG_VIDEO where FSFILE_NO = '201707300010003' --20170729 當晝新聞(完成帶/第210集) FSMoveToArchive =N , FSApprove_STATE = N
	select * from TBUSERS where FSUSER_ID = 'news50838' --林玟伶
	select * from TBLOG_VIDEO where FDCREATED_DATE > '2017-01-01' and FSAPPROVE_STATE = 'N' and FCLOW_RES = 'Y' order by FDCREATED_DATE
	select * from TBLOG_VIDEO where FDCREATED_DATE between '2014-01-01' and '2017-01-01' and FSAPPROVE_STATE = 'N' and FCLOW_RES = 'Y' order by FDCREATED_DATE
	select * from TBLOG_VIDEO where FDCREATED_DATE between '2014-01-01' and '2017-01-01' and FSAPPROVE_STATE = 'N' order by FDCREATED_DATE
end

if @case = 201711221024 begin
	select top 1000 * from TBINEWS
	select top 100 * from tblTRAN_FILE_LOG --empty
	select * from TBCodeChannel   --1	公視 2	客家 3	原視 4	華視 6	其他 5	宏觀
	select top 1000 * from TBLOG_DOC
	select * from tbMAM_2015
	select * from TBSUBJECT
	select top 1000 * from VSEARCH_TBLOG_DOC
	select top 1000 * from VW_NEWS_2016
	select top 1000 * from TBLOG_VIDEO_D
	select top 1000 * from TBLOG_VIDEO

	select FXEXTEND.query('<result>{for $i in /Media/Keyword/KeyItem[@type="關鍵字"] return string($i)}</result>') AS KEYWORD from TBLOG_VIDEO where FSFILE_NO = '200604050010001'
	select FXEXTEND.query('{for $i in /Media/Keyword/KeyItem[@type="關鍵字"] return string($i)}') AS KEYWORD from TBLOG_VIDEO where FSFILE_NO = '200604050010001' --XQuery [TBLOG_VIDEO.FXEXTEND.query()]: 接近 '{' 的語法錯誤
	select FXEXTEND.value('(/Media/@type)[1]','nvarchar(2000)') AS [FSTYPE] from TBLOG_VIDEO where FSFILE_NO = '200604050010001'

	--新聞檔案長度大於1 小時，內容描述小於 50 個字
	select A.FSFILE_NO 檔案編號, 
		FSTITLE 標題, 
		REPLACE(A.FSDESCRIPTION, char(13) + char(10), '') 內容, 
		LEN(A.FSDESCRIPTION) 文字長度, CAST(FNDURATION/(30.0*60*60) as decimal(4,1)) 時長,
		CAST(ROUND(FNFILE_SIZE/POWER(2.0,30), 2) as decimal(6, 2)) "檔案大小(GB)", 
		ISNULL(D.FSUSER, '') as 建立者, ISNULL(D.FSDEPT, '') 單位,
		--ISNULL(FXEXTEND, ''),
		C.name 頻道,
		B.RECORDTYPE 類別 from TBLOG_VIDEO A
	left join (select FSFILE_NO, 
						FXEXTEND.value('(/Media/Channel)[1]','nvarchar(2000)') AS [CHANNEL],
						FXEXTEND.value('(/Media/RecordType)[1]','nvarchar(2000)') AS [RECORDTYPE]
		from TBLOG_VIDEO) B on A.FSFILE_NO = B.FSFILE_NO
	left join TBCodeChannel C on B.CHANNEL = C.id
	left join TBUSERS D on A.FSCREATED_BY = D.FSUSER_ID
	where FSAPPROVE_STATE ='Y' and LEN(A.FSDESCRIPTION) < 50 and FNDURATION > 30*60*60  order by A.FSFILE_NO --1952

	select * from TBUSER_GROUP

	select * from TBDIRECTORIES

	select * from TBLOG_VIDEO where FSFILE_NO = '200810150010026'
end

if @case = 201711221658 begin
	select * from TBVIDEO_RETRIVE where FDDATE >= '2017-11-22' order by FDDATE
end

if @case = 201711241353 begin --access denied 201609280010005, 201607090010029, yangyt
	select * from TBVIDEO_RETRIVE where FSFILE_NO = '201609170010004'
	select * from TBVIDEO_RETRIVE where FSFILE_NO in ('201607090010029', '201609170010003', '201609280010005')
end

if @case = 201711271359 begin
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'news1009' and FDDATE >= '2017-11-27' --201505210010040, 201505210010036
	select * from TBLOG_VIDEO where FSFILE_NO in ('201505210010040', '201505210010036')
end


if @case = 201711271837 begin -- 【公共電視】有話好說2000機構照服員.MXF (錯誤)從儲存體取回檔案
	select * from TBVIDEO_RETRIVE where FSFILE_NO = '201611030010079' order by FDDATE --amylonely1 \\10.13.200.22\mamnas1\Retrieve\pts\m.7413005\20171127\【公共電視】有話好說2000機構照服員.MXF
	select * from TBLOG_VIDEO where FSFILE_NO = '201611030010079' --【公共電視】(有話好說) 2000機構照服員
	--\\10.13.200.7\Retrieve\pts\news50556\20170925\【公共電視】有話好說2000機構照服員.MXF
	--\\10.13.200.22\mamnas1\Retrieve\pts\amylonely1\20171127\【公共電視】有話好說2000機構照服員.MXF
end

if @case = 201711291156 begin
	select * from TBUSERS where FSUSER_ID = 'news1248'
	update TBUSERS set FCACTIVE = 'Y' where FSUSER_ID = 'news1248'
end

if @case = 201711291502 begin --tapeoffline
	select * from TBLOG_VIDEO where FSFILE_NO = '201301310010046' --1900書說水利圳 m.7413005
	select * from TBUSERS where FSUSER_ID = 'm.7413005' --陳珮瑜
	select * from TBVIDEO_RETRIVE where FSFILE_NO = '201301310010046'
end

if @case = 201711291527 begin --【公共電視】1200不友善婚宴 調用失敗
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'oreoou0911' order by FDDATE desc --201504090010133
	select * from TBLOG_VIDEO where FSFILE_NO = '201504090010133' --【公共電視】1200不友善婚宴 /news/pts/2015/04/09/201504090010133//201504090010133.MOV
end

if @case = 201711291604 begin
	select * from TBLOG_VIDEO where FSFILE_NO = '201505290010028' --【公共電視】1900看山難究責
	select * from TBVIDEO_RETRIVE where FSFILE_NO = '201505290010028' order by FDDATE desc
	select * from TBUSERS where FSUSER_ID = 'news51081' --謝其文
end

if @case = 201711301456 begin
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'oreoou0911' order by FDDATE desc --201503180010083 (錯誤)從儲存體取回檔案, oreoou0911
	--/news/pts/2015/03/18/201503180010083//201503180010083.MPG
	select * from TBLOG_VIDEO where FSFILE_NO = '201503180010083' --蘭陽博物館
end

if @case = 201712011147 begin --A00219L4
	select * from TBLOG_VIDEO where FSFILE_NO = '201404130010037' --/news/hakka/2014/04/13/201404130010037//201404130010037.mov
	select * from TBLOG_VIDEO where FSFILE_NO = '201404040010076' --/news/hakka/2014/04/04/201404040010076//201404040010076.mov
	select * from TBVIDEO_RETRIVE where FSFILE_NO = '201404130010037' --robert99
end

if @case = 201712051350 begin
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'news50991' and FDDATE >= '2017-12-05'
	--/news/pts/2016/06/07/201606070010135//201606070010135.MXF
	--/news/pts/2015/11/06/201511060010041//201511060010041.MOV
end

if @case = 201712061039 begin
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'news51097' and FDDATE >= '2017-12-06' --201608160010031 --處理中 /news/pts/2016/08/16/201608160010031/201608160010031.MXF  -> F (錯誤)從儲存體取回檔案
	--dsmrecall success at HSM4
end

if @case = 201712071746 begin
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'news50556' and FDDATE >=  '2017-12-07' --200807010010030, (錯誤)從儲存體取回檔案
	select * from TBLOG_VIDEO where FSFILE_NO = '200807010010030' --【公共電視】2000慈湖兩蔣園區
end

if @case = 201712081129 begin
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'news50985' and FDDATE >= '2017-12-08' --201010200010065 (錯誤)從儲存體取回檔案
	select * from TBLOG_VIDEO where FSFILE_NO = '201010200010065' --老爺溫泉標章(上字) /news/titv/2010/10/20/201010200010065/201010200010065.MOV
end

if @case = 201712121538 begin
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'news9456' and FDDATE >= '2017-12-12' --201311260010083 news9456
	select * from TBLOG_VIDEO where FSFILE_NO = '201311260010083' --1900桐花攝影展 /news/hakka/2013/11/26/201311260010083//201311260010083.mov
end

if @case = 201712121729 begin
	select * from TBVIDEO_RETRIVE where FSFILE_NO in ('201204270010066', '201401080010173') order by FDDATE --apple25754
	select * from TBLOG_VIDEO where FSFILE_NO = '201401080010173' --1900曾文忠畫展
	select * from TBLOG_VIDEO where FSFILE_NO = '201204270010066' --《客雜》0428素人畫家陳秋妹
end

if @case = 201712181052 begin
	select * from TBLOG_VIDEO where FSFILE_NO = '20140708001007'  --no such file
	select * from TBLOG_VIDEO where FSTITLE like '%村民大會第373集%' --201407080010073
end

if @case = 201712191207 begin
	select * from TBUSERS where FSUSER_ID = 'news50279' --陳柏諭
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'news50279' and FDDATE >= '2017-12-19' order by FDDATE

/*
/mnt/Retrieve/pts/news50279/20171219
201403050010006 /news/pts/2014/03/05/201403050010006//201403050010006.mov ok
201407210010035 ok
201407180010045 ok
201402070010031 ok

*/

end

if @case = 201712191436 begin
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'news70185' and FDDATE >= '2017-12-19' order by FDDATE
/*
201201010010073 : /news/titv/2012/01/01/201201010010073/201201010010073.MXF
201012100010001 : /news/titv/2010/12/10/201012100010001//201012100010001.MXF
201012090011333
*/
end

if @case = 201712201158 begin
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'news51081' and FDDATE >= '2017-12-20' order by FDDATE
end

if @case = 201712201411 begin
	select * from TBVIDEO_RETRIVE where FDDATE > '2017-12-20' order by FDDATE --news50278, 201505300010005
end

if @case = 201712210950 begin
	select * from TBVIDEO_RETRIVE where FSFILE_NO = '2016090800010006' --/news/hakka/2016/09/08/201609080010006//201609080010006.MXF
	select * from TBVIDEO_RETRIVE where FSFILE_NO = '201609080010006' and FDDATE >= '2017-12-20' order by FDDATE
end


if @case = 201801031400 begin
	select * from TBVIDEO_RETRIVE where FDDATE >= '2018-01-03' and FSFILE_NO = '201710120010012' order by FDDATE
	select * from TBVIDEO_RETRIVE where FSFILE_NO = '201710120010012' order by FDDATE
	select * from TBLOG_VIDEO where FSFILE_NO = '201710120010012' --20171010  國慶大會特別報導(分軌帶/第1集)
end

if @case = 201801031521 begin
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'news1240' and FDDATE >= '2018-01-03' --201611010010080, 2018-01-03 13:37:26.160
	select * from TBLOG_VIDEO where FSFILE_NO = '201611010010080' --【公共電視】1900採茶雇遊覽車
end

if @case = 201801041055 begin
	--新聞檔案長度大於1 小時，內容描述小於 50 個字 濾掉最夜新聞
	select A.FSFILE_NO 檔案編號, 
		FSTITLE 標題, 
		REPLACE(A.FSDESCRIPTION, char(13) + char(10), '') 內容, 
		LEN(A.FSDESCRIPTION) 文字長度, CAST(FNDURATION/(30.0*60*60) as decimal(4,1)) 時長,
		CAST(ROUND(FNFILE_SIZE/POWER(2.0,30), 2) as decimal(6, 2)) "檔案大小(GB)", 
		ISNULL(D.FSUSER, '') as 建立者, ISNULL(D.FSDEPT, '') 單位,
		--ISNULL(FXEXTEND, ''),
		C.name 頻道,
		B.RECORDTYPE 類別 from TBLOG_VIDEO A
	left join (select FSFILE_NO, 
						FXEXTEND.value('(/Media/Channel)[1]','nvarchar(2000)') AS [CHANNEL],
						FXEXTEND.value('(/Media/RecordType)[1]','nvarchar(2000)') AS [RECORDTYPE]
		from TBLOG_VIDEO) B on A.FSFILE_NO = B.FSFILE_NO
	left join TBCodeChannel C on B.CHANNEL = C.id
	left join TBUSERS D on A.FSCREATED_BY = D.FSUSER_ID
	where FSAPPROVE_STATE ='Y' and LEN(A.FSDESCRIPTION) < 50 and FNDURATION > 30*60*60 and FSTITLE not like '%最夜新聞%' order by A.FSFILE_NO, CHANNEL --1952 --> 1772 -> 796 (filter 最夜新聞)


end

if @case = 201801041624 begin
	select * from TBVIDEO_RETRIVE where FDDATE >= '2018-01-04' and FSUSER_ID = 'news70079'  order by FDDATE  --201609230010103, (錯誤)從儲存體取回檔案
	select * from TBLOG_VIDEO where FSFILE_NO = '201609230010103' --【公共電視】1900災後不幫清 /news/pts/2016/09/23/201609230010103//201609230010103.MXF
end

if @case = 201801081128 begin
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) = '2018-01-09' and FSUSER_ID = 'news70079' order by FDDATE --no record 201503120010013
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) = '2018-01-08' order by FDDATE
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) > '2018-01-01' and FSUSER_ID = 'news70079' order by FDDATE
end

if @case = 201801101540 begin
	select * from TBUSERS where FSUSER = '王德心' --prg51002
	select * from TBUSER_GROUP where FSUSER_ID = 'prg51002'
	select * from TBUSER_GROUP where FSUSER_ID = 'hsing1'
	select * from TBUSERS where FSUSER_ID = 'hsing1'
	update TBUSERS set FCACTIVE = 'Y' where FSUSER_ID = 'hsing1'
end

if @case = 201801111654 begin
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) >= '2018-01-11' and FSUSER_ID = 'news50324' order by FDDATE
	--201202090010006, 201605020010080
end

if @case = 201801111654 begin
	select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) >= '2018-01-11' and FSUSER_ID = 'news50556' order by FDDATE --200709190010002
	select * from TBLOG_VIDEO where FSFILE_NO = '200709190010002' --2000樂生找俊雄
end

if @case = 201801121023 begin
	select * from TBVIDEO_RETRIVE where FDDATE >= '2018-01-12' order by FDDATE
end

if @case = 201801121641 begin
	select * from TBUSERS where FSUSER_ID = 'news50556' --莊志成
	select * from TBUSERS where FSUSER = '孟昭權' --news50552
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'news50552' and FDDATE >= '2018-01-12' order by FDDATE
end

if @case = 201801151014 begin
	select * from TBVIDEO_RETRIVE  where FDDATE >= '2018-01-14' and FSUSER_ID = 'ying0218' --201610060010003
	select * from TBUSERS where FSUSER_ID = 'news51537' --??
	select * from TBUSERS where FSUSER_ID = 'ying0218' --韓瑩
end

if @case = 201801151618 begin
	select * from TBUSERS where FSUSER = '黃守銘' --NEWS1009
	select * from TBVIDEO_RETRIVE where FDDATE >= '2018-01-15' and FSUSER_ID = 'NEWS1009'
	select * from TBLOG_VIDEO where FSFILE_NO = '201605180010054' --【公共電視】(有話好說備帶) 勞動檢查
end

if @case = 201801170945 begin
	select B.FSTITLE, A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO  where FSUSER_ID = 'news50324' and FDDATE >= '2018-01-17' order by FDDATE
end

if @case = 201801241132 begin
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'news50271' and FDDATE >= '2018-01-24'
/*
201610280010055  /news/pts/2016/10/28/201610280010055//201610280010055.MXF
201510310010051
201510310010056
*/
	select * from TBLOG_VIDEO where FSFILE_NO in  (select FSFILE_NO from TBVIDEO_RETRIVE where FSUSER_ID = 'news50271' and FDDATE >= '2018-01-24')
end

if @case = 201801251121 begin
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'news50278' and FDDATE >= '2018-01-25'
/*
201605060010004 《客雜HD》0514蕉金蕉土 done
201609210010084 【公共電視】1900香蕉價大漲 done
201606070010098 《客雜HD》0618鳳梨鍍金 --ANS4007E Error processing '/news/hakka/2016/06/07/201606070010098/201606070010098.MXF': access to the object is denied  ==> E01805L4
*/
	select * from TBLOG_VIDEO where FSFILE_NO in (select FSFILE_NO from TBVIDEO_RETRIVE where FSUSER_ID = 'news50278' and FDDATE >= '2018-01-25')
end

if @case = 201801261354 begin
	select * from TBLOG_VIDEO where FSFILE_NO in ('201402250010164', '201402240010055', '201401070010038', '201312130010078', 
	'201312130010084', '201312130010093', '201312130010091', '201312130010095', '201312130010082')

/*
201312130010078, D00292L4
201312130010082, E00402L4
201312130010084, E00402L4
201312130010091, E00402L4
201312130010093, E00402L4
201312130010095, E00402L4
201401070010038, E00402L4
201402240010055, E00427L4
201402250010164, E00427L4

D00292L4, E00402L4,E00427L4

HSM5 : ./mydsmrecall.py recall.txt /mnt/Retrieve/hakka/FAIL
*/

end


if @case = 201801291603 begin
	select top 1000 * from TBLOG_VIDEO where FDCREATED_DATE between '2017-07-01' and '2017-10-31' order by FDCREATED_DATE
	select * from TBLOG_VIDEO where FSFILE_NO = '201708070010004'
	select * from TBLOG_VIDEO where FDCREATED_DATE > '2018-01-01'
end

if @case = 201801311041 begin
    select * from TBVIDEO_RETRIVE where FSUSER_ID = 'news70079' and FDDATE >= '2018-01-31' --201605250010059
	select * from TBLOG_VIDEO where FSFILE_NO = '201605250010059' --【公共電視】1900台灣土雞推手
	select *  from TBUSERS where FSUSER_ID = 'news70079' --駱恩蘋
end

if @case = 201802021016 begin
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'news08276' and FDDATE >= '2018-02-02' order by FDDATE
	select * from TBVIDEO_RETRIVE where  FDDATE >= '2018-02-02' order by FDDATE
	select distinct(FSFILE_NO) from TBVIDEO_RETRIVE where  FDDATE >= '2018-02-02'

	select * from TBVIDEO_RETRIVE where  FDDATE >= '2018-02-02' and FSUSER_ID = 'news50553' order by FDDATE

/*
201009090010005  /news/pts/2010/09/09/201009090010005//201009090010005.AVI done
201404120010041  檔案已存放到新聞部檔案伺服器
201407160010029  queue
*/
end

if @case = 201802070933 begin
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'news51205' and FDDATE >= '2018-02-07'
	select * from TBVIDEO_RETRIVE where FDDATE >= '2018-02-07' order by FDDATE --ph860129d
	select * from TBUSERS where FSUSER_ID = 'ph860129d' --黃郁婷  /mnt/Retrieve/pts/ph860129d/
/*
201608090010010  /news/pts/2016/08/09/201608090010010//201608090010010.MXF
201605100010054  /news/pts/2016/05/10/201605100010054//201605100010054.MXF  A00854L4 NEWSPTSHSMCP, E01861L4 NEWSPTSHSMCP
201603010010002  /news/pts/2016/03/01/201603010010002//201603010010002.MXF
*/
end

if @case = 201802081515 begin
	select * from TBVIDEO_RETRIVE where FDDATE >= '2018-02-08' order by FDDATE
end

if @case = 2018002221144 begin
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'news50985' and FDDATE >= '2018-02-22'
/*
201201100010044
201303040010020
201206280010084
200712100010025
201303130010047
201003310010001
200801280010001
201206290010076
*/
end

if @case = 201802231629 begin
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'news08276' and FDDATE >= '2018-02-23' --201603090010088 /news/pts/2016/03/09/201603090010088//201603090010088.MXF
	select * from TBLOG_VIDEO where FSFILE_NO = '201603090010088'
	select * from TBUSERS where FSUSER_ID = 'news08276' --陳立峰
end

if @case = 201802261429 begin --xxoo
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'itv50700' order by  FDDATE
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'itv50700' and FDDATE >= '2018-02-26' --201101210010003
	select * from TBUSERS where FSUSER_ID = 'itv50700' --楊光耀
	select * from TBVIDEO_RETRIVE where FSUSER_ID = 'koli4301' and FDDATE >= '2018-02-23'
	select * from TBLOG_VIDEO where FSFILE_NO = '201101210010003' --xxoo 讓瓦祿更甜美  B00025L4 NEWSTITVHSM , B00046L4 NEWSTITVHSM(none)
	select * from TBLOG_VIDEO where FSFILE_NO = '201111230010123' --xxoo 原產博覽會(無字,阿美語-歐嗨)  B00046L4  
end

if @case = 201802271030 begin
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= '2018-02-27' order by FDDATE
end

if @case = 201803051451 begin --prg60138
    select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE)  order by FDDATE
	--201311160010110 1900高美抗減招  HSM3 : root     13029  0.0  0.0 114628  9332 ?        Ss   14:23   0:00 dsmrecall /news/hakka/2013/11/16/20131116001011
	--201501030010004 【公共電視】1900競技物理治療
end

if @case = 201803061532 begin --news50273
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE) and FSUSER_ID = 'news50273' order by FDDATE
	/*
	【公視原雜】富山禁漁區,設禁漁區 部落未知情 201304150010131 /news/pts/2013/04/15/201304150010131//201304150010131.MXF
	美麗灣還美麗嗎  201101240010020, /news/titv/2011/01/24/201101240010020//201101240010020.MXF
	*/
end

if @case = 201803141137 begin --oo
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE)  order by FDDATE
	--東防空演習1900 news51083, 201106090010071  /news/titv/2011/06/09/201106090010071//201106090010071.MXF
end

if @case = 201803141411 begin
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE)  and FSUSER_ID = 'news1009' order by FDDATE
	/*
	客家新聞雜誌-0617農業師傅上工 _HD拍攝精華帶  201706070010006  /news/hakka/2017/06/07/201706070010006//201706070010006.MXF
	【公共電視】2000農忙找外勞 201107280010001 /news/pts/2011/07/28/201107280010001//201107280010001.MOV
	【公共電視】1230台東芒果豐收 201606160010130  /news/pts/2016/06/16/201606160010130//201606160010130.MXF
	《客雜HD》1029農務團，缺工解藥？(下) 201611250010002 /news/hakka/2016/11/25/201611250010002//201611250010002.MXF
	*/
end

if @case = 201803141618 begin
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE)  and FSUSER_ID = 'news50556' order by FDDATE
/*
201112250010021 
201308300010013
201503190010054
201504100010001
200910170010017
201503060010024
201412130010020
201503310010089
201509170010131

/news/pts/2011/12/25/201112250010021//201112250010021.MOV
/news/pts/2013/08/30/201308300010013//201308300010013.mov
/news/hakka/2015/03/19/201503190010054//201503190010054.MOV
/news/pts/2015/04/10/201504100010001//201504100010001.MOV
/news/pts/2009/10/17/200910170010017//200910170010017.avi
/news/pts/2015/03/06/201503060010024//201503060010024.MOV
/news/pts/2014/12/13/201412130010020//201412130010020.MOV
/news/hakka/2015/03/31/201503310010089//201503310010089.MOV
/news/pts/2015/09/17/201509170010131//201509170010131.MOV

*/
end

if @case = 201803151510 begin
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE)  and FSUSER_ID = 'news70185' order by FDDATE
	--201603210010095 E01772L4 (on-site) /news/pts/2016/03/21/201603210010095//201603210010095.MXF
end

if @case = 201803151510 begin
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE) order by FDDATE
	--201011220010263, 阿里山補牙(無字),apple25754, update vol A00164L4 acc=READWRITE, audit vol A00164L4 fix=yes
	--【公共電視】1900蛀牙缺牙多, apple25754, 201507250010021, /news/titv/2010/11/22/201011220010263//201011220010263.MXF ok
end

if @case = 201803151510 begin
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE) order by FDDATE
	--201011220010263, 阿里山補牙(無字),apple25754, update vol A00164L4 acc=READWRITE, audit vol A00164L4 fix=yes
	--【公共電視】1900蛀牙缺牙多, apple25754, 201507250010021, /news/titv/2010/11/22/201011220010263//201011220010263.MXF ok
end

if @case = 201803191122 begin
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE) order by FDDATE --【公共電視】1900蚵仔去哪裡 201502170010010 news1009
end

if @case = 201803191506 begin --oo news70185
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE) order by FDDATE
/*
【公共電視】1900沒學生開學 201608290010052 /hakkatv/HVideo/2007/2007342/G200734200210002.mxf
【公共電視】1900偏鄉借學生 201609200010062 /news/pts/2016/09/20/201609200010062//201609200010062.MXF
*/
end

if @case = 201803200932 begin --oo
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE) order by FDDATE
	--【公共電視】1200美王子獨創性(國) 全部卡在處理中
end

if @case = 201803200932 begin --
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE) order by FDDATE
end

if @case = 201803221637 begin --oo
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE) and FSUSER_ID = 'news08276' order by FDDATE
	--【公共電視】1200女兵變排長 201210220010079
/*
[root@HSM5 VOL]# dsmrecall /news/pts/2012/10/22/201210220010079//201210220010079.MOV
IBM Tivoli Storage Manager
Command Line Space Management Client Interface
  Client Version 6, Release 3, Level 0.0
  Client date/time: 03/22/2018 16:58:48
(c) Copyright by IBM Corporation and other(s) 1990, 2011. All Rights Reserved.

ANS9531E recall5: cannot create a DM session: old session = 0
session info = dsmrecall. Reason : Cannot allocate memory

*/
end

if @case = 201803251035 begin
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE)  order by FDDATE
end


if @case = 201803251035 begin--oo
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE) and FSUSER_ID = 'news1009'  order by FDDATE
	--島中鋼拍帶精華入庫 201408060010137, R, /news/pts/2014/08/06/201408060010137//201408060010137.MXF 54G
	--【公共電視】(有話好說備帶) 經濟景氣衰退 201310300010030 /news/pts/2015/08/14/201508140010040//201508140010040.MOV
	select * from TBUSERS where FSUSER_ID = 'news1009' --黃守銘
end

if @case = 201803271645 begin --xx
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE) and FSUSER_ID = 'news51014' and FCSTATUS = 'F' order by FDDATE
/*
201108060010015, /news/pts/2011/08/06/201108060010015//201108060010015.MOV done
201302240010051, /news/pts/2013/02/24/201302240010051//201302240010051.MOV done
201407110010036, /news/pts/2014/07/11/201407110010036//201407110010036.MOV done
201407230010027, /news/pts/2014/07/23/201407230010027//201407230010027.MOV done
201312250010009, /news/pts/2013/12/25/201312250010009//201312250010009.mov
*/
end

if @case = 201803271645 begin --xx
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE) and FSUSER_ID = 'news08276' order by FDDATE
	--【公共電視】1900糾正復航空難 201609140010073
	--【公共電視】1900復興空難偵結 201607130010109 /news/pts/2016/07/13/201607130010109//201607130010109.MXF
end

if @case = 201804111539 begin --finnchang
		select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE) and FSUSER_ID = 'finnchang' order by FDDATE
		--【公共電視】(外電)吳宇森新片"太平輪" 201409280010057
end

if @case = 2018004131054 begin
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE) order by FDDATE 

end

if @case = 201804161512 begin
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE) and FSUSER_ID = 'prg50660'  order by FDDATE
	--201412070010004, 【公共電視】1900連柯選後感謝
	--/news/pts/2014/12/07/201412070010004//201412070010004.MOV, E00597L4
end

if @case = 201804171510 begin
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO  where FDDATE >= CAST(GETDATE() as DATE) order by FDDATE
	select * from TBUSERS where FSUSER ='劉漢麟' --news50675
	select * from TBUSERS where FSUSER_ID = 'prg1316' --張其錚
end

if @case = 201804241636 begin
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE)  order by FDDATE
end

if @case = 201804261225  begin
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE) and FSUSER_ID = 'vincent'  order by FDDATE
	/*
	《客雜HD》0507酸入台糖地 (錯誤)從儲存體取回檔案 201604220010044, /news/hakka/2016/04/22/201604220010044//201604220010044.MXF
	客家新聞雜誌-0507酸入台糖地_HD拍攝精華帶
	*/
end

if @case = 201804301145
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE) and FSUSER_ID = 'news50556'  order by FDDATE --201512020010065 【公共電視】1230新屋大火再遞狀
	--/news/pts/2015/12/02/201512020010065//201512020010065.MOV
end

if @case = 201804301605 begin
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE) and FSUSER_ID = 'news1009'  order by FDDATE --201501210010046 1230新屋夜惡火
	--/news/hakka/2015/01/21/201501210010046//201501210010046.MOV
	--201501200010054 【公共電視】1200新屋大火最新SNG
	--access to the object is denied E00974L4
end

if @case = 201805021347 begin --xxoo ptshsiung
	--【公共電視】1900老鷹想飛 201511030010022 /news/pts/2015/11/03/201511030010022//201511030010022.MOV E01548L4
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE)  and FSUSER_ID = 'ptshsiung'  order by FDDATE
end

if @case = 201805141712 begin
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE)  and FSUSER_ID = 'news1009'  order by FDDATE
	--201503120010003 【公共電視】1900子宮頸癌HPV16, /news/pts/2015/03/12/201503120010003//201503120010003.MOV, E00983L4
end

if @case = 201805211450 begin
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE) and FSUSER_ID = 'news1009' order by FDDATE
	--【公共電視】 (有話備帶)中國富士康及科技業 201203220010078 /news/pts/2012/03/22/201203220010078//201203220010078.MOV
end

if @case = 201805211450 begin --news70209
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE) and FSUSER_ID = 'news70209' order by FDDATE
/*
【公共電視】(有話好說備帶)社宅青宅 201509230010071 /news/pts/2015/09/23/201509230010071//201509230010071.MOV
【公共電視】(有話好說備帶) 租屋環境 201503050010074 /news/pts/2015/03/05/201503050010074//201503050010074.MOV
*/
end

if @case = 201805221440 begin --news1009
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE) and FSUSER_ID = 'news1009' order by FDDATE
/*
【公共電視】(有話好說備帶) 中國飛機飛越釣魚台領空 201212200010043 /news/pts/2012/12/20/201212200010043//201212200010043.MOV E01861L4
【公共電視】(獨立拍精)誰的釣魚台 拍精 4 201605020010009 /news/pts/2016/05/02/201605020010009//201605020010009.MXF D00350L4
*/
end

if @case = 201806051749 begin
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE) and FSUSER_ID = 'news51014' order by FDDATE
	--201606090010031
end

if @case = 201806191623 begin
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE) and FSUSER_ID = 'news70185' order by FDDATE
	--【公共電視】1900南屯木屐節 201606090010064
end

if @case = 201806201058 begin
	select 
		(CASE
		WHEN Channel = 1 THEN '公視'
		WHEN Channel = 2 THEN '客家'
		WHEN Channel = 3 THEN '原視'
		WHEN Channel = 4 THEN '華視'
		WHEN Channel = 5 THEN '宏觀'
		WHEN Channel = 6 THEN '其他'
	END) as 頻道, 
	* from RPT_館藏數位檔統計 where Channel = 5 order by Channel, RecordType --17738
	
	select * from VW_TBLOG_VIDEO where CHANNEL = '宏觀' order by FDCREATED_DATE --17738  2010-06-11 ~ 2016-12-23
end

if @case = 201806201149 begin
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE) and FSUSER_ID = 'news08276' order by FDDATE
	--【公共電視】1900禁全美語教學 --201206050010020
end

if @case = 201806201149 begin --news50556
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE) and FSUSER_ID = 'news1009' order by FDDATE
	--新任大使陳經銓史瓦濟蘭抵任 201309250010085
	--【公共電視】1900貧窮小確幸 201510230010008
end

if @case = 201806261645 begin
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE) and FSUSER_ID = 'news50556' order by FDDATE
	--1919贈物資1900 --201109090010094 /news/titv/2011/09/09/201109090010094//201109090010094.MXF B00040L4 off-site
end

if @case = 201806261645 begin
	select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE) and FSUSER_ID = 'news51094' order by FDDATE
/*
【公共電視】1900新屋大火六死 201501210010032, /news/pts/2015/01/21/201501210010032//201501210010032.MOV, E00974L4
【公共電視】1900違建釀火究責 201501210010003 /news/pts/2015/01/21/201501210010003//201501210010003.MOV, E00974L4
【公共電視】1200新屋大火最新SNG 201501200010054, /news/pts/2015/01/20/201501200010054//201501200010054.MOV, E00974L4
*/

end


----------------------------------Routine---------------------------------------
use PTS_MAM
select B.FSTITLE ,A.* from TBVIDEO_RETRIVE A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FDDATE >= CAST(GETDATE() as DATE)  order by FDDATE
select * from TBVIDEO_RETRIVE where CAST(FDDATE as DATE) > '2018-02-15' order by FDDATE
select * from TBLOG_VIDEO where CAST(FDCREATED_DATE as DATE) = '2017-09-04' order by FDCREATED_DATE
update TBUSERS set FCACTIVE = 'Y' where FSUSER_ID = 'hsing1'
update TBUSERS set FCACTIVE = 'Y' where FSUSER_ID = 'news1248'
select * from TBUSERS where FSUSER_ID = 'itv9426' --蔡富榮
select * from TBUSERS where FSUSER_ID = 'news1248'

select * from TBUSERS where FSUSER = '連國程'


if @case = 201708311414-201605261156 begin
	select Channel, count(FDCAMERA_DATE) from RPT_館藏數位檔統計 group by Channel
	select * from RPT_館藏數位檔統計 where Channel = '1' and RecordType = '拍攝精華'  --4844
	select * from RPT_館藏數位檔統計 where Channel = '2' and RecordType = '拍攝精華'  --15263
	select sum(FNFRAMES)/(30 * 60 * 60) from RPT_館藏數位檔統計 where Channel = '2' and RecordType = '拍攝精華' --3400 hr
	select sum(FNFRAMES)/(30 * 60 * 60) from RPT_館藏數位檔統計 where Channel = '1' and RecordType = '拍攝精華' --2526 hr
	select Channel, sum(FNFRAMES)/(30 * 60 * 60)  from RPT_館藏數位檔統計 group by Channel, RecordType having RecordType = '拍攝精華' order by Channel
	select Channel, sum(FNFRAMES)/(30 * 60 * 60)  from RPT_館藏數位檔統計 group by Channel, RecordType having RecordType = 'SOT' order by Channel

	select 
		(CASE
		WHEN Channel = 1 THEN '公視'
		WHEN Channel = 2 THEN '客家'
		WHEN Channel = 3 THEN '原視'
		WHEN Channel = 4 THEN '華視'
		WHEN Channel = 5 THEN '宏觀'
		WHEN Channel = 6 THEN '其他'
	END) as 頻道, 
	RecordType, SUM(FNFRAMES) / (30 * 60 * 60) DUR from RPT_館藏數位檔統計 group by Channel, RecordType order by Channel, RecordType

	select FNFRAMES/(30 * 60), * from RPT_館藏數位檔統計 where Channel = 5 and RecordType = 'SOT'
end

if @case = 201711221024 begin
	select top 1000 * from TBINEWS
	select top 100 * from tblTRAN_FILE_LOG --empty
	select * from TBCodeChannel   --1	公視 2	客家 3	原視 4	華視 6	其他 5	宏觀
	select top 1000 * from TBLOG_DOC
	select * from tbMAM_2015
	select * from TBSUBJECT
	select top 1000 * from VSEARCH_TBLOG_DOC
	select top 1000 * from VW_NEWS_2016
	select top 1000 * from TBLOG_VIDEO_D
	select top 1000 * from TBLOG_VIDEO

	select FXEXTEND.query('<result>{for $i in /Media/Keyword/KeyItem[@type="關鍵字"] return string($i)}</result>') AS KEYWORD from TBLOG_VIDEO where FSFILE_NO = '200604050010001'
	select FXEXTEND.query('{for $i in /Media/Keyword/KeyItem[@type="關鍵字"] return string($i)}') AS KEYWORD from TBLOG_VIDEO where FSFILE_NO = '200604050010001' --XQuery [TBLOG_VIDEO.FXEXTEND.query()]: 接近 '{' 的語法錯誤
	select FXEXTEND.value('(/Media/@type)[1]','nvarchar(2000)') AS [FSTYPE] from TBLOG_VIDEO where FSFILE_NO = '200604050010001'

	--新聞檔案長度大於1 小時，內容描述小於 50 個字
	select A.FSFILE_NO 檔案編號, 

		FSTITLE 標題, 
		REPLACE(A.FSDESCRIPTION, char(13) + char(10), '') 內容, 
		LEN(A.FSDESCRIPTION) 文字長度, CAST(FNDURATION/(30.0*60*60) as decimal(4,1)) 時長,
		CAST(ROUND(FNFILE_SIZE/POWER(2.0,30), 2) as decimal(6, 2)) "檔案大小(GB)", 
		ISNULL(D.FSUSER, '') as 建立者, ISNULL(D.FSDEPT, '') 單位,
		--ISNULL(FXEXTEND, ''),
		C.name 頻道,
		B.RECORDTYPE 類別 from TBLOG_VIDEO A
	left join (select FSFILE_NO, 
						FXEXTEND.value('(/Media/Channel)[1]','nvarchar(2000)') AS [CHANNEL],
						FXEXTEND.value('(/Media/RecordType)[1]','nvarchar(2000)') AS [RECORDTYPE]
		from TBLOG_VIDEO) B on A.FSFILE_NO = B.FSFILE_NO
	left join TBCodeChannel C on B.CHANNEL = C.id
	left join TBUSERS D on A.FSCREATED_BY = D.FSUSER_ID
	where FSAPPROVE_STATE ='Y' and LEN(A.FSDESCRIPTION) < 50 and FNDURATION > 30*60*60  order by A.FSFILE_NO --1952

	select * from TBUSER_GROUP

	select * from TBDIRECTORIES

	select * from TBLOG_VIDEO where FSFILE_NO = '200810150010026'

	select count(FSFILE_NAME) from TBINEWS --372405
	select top 1000 * from TBINEWS order by FDCREATED_DATE desc
	select top 1000 * from TBLOG_VIDEO
	select top 10 * from VW_NEWS
	select * from TBUSERS where FSUSER_ID = 'hsing1'
	update TBUSERS set FCACTIVE = 'Y' where FSUSER_ID = 'hsing1'
	select top 100 * from TBLOG_VIDEO_D

	select count(FSFILE_NO) from VW_NEWS where SCRIPT <> '' --209209
	select count(FSFILE_NO) from VW_NEWS --244179

	select * from TBLOG_VIDEO where FSFILE_NO = '200604050010003'
end


if @case = 201711161024 begin --統計新聞片庫時數
	select top 1000 * from RPT_館藏數位檔統計
	select count(channel) from RPT_館藏數位檔統計 where Channel = 1 --147518
	select count(Channel) from RPT_館藏數位檔統計 where Channel = 2 --38284
	select sum(FNFRAMES)/(30 * 60 * 60) from RPT_館藏數位檔統計 where Channel = '2' --10447 hr
	select sum(FNFRAMES)/(30 * 60 * 60) from RPT_館藏數位檔統計 where Channel = '1' --7334 hr
	select * from RPT_館藏數位檔統計 where Channel = 1
	select count(FSFILE_NO) from TBLOG_DOC --179
	select count(FSFILE_NO) from TBLOG_VIDEO --244185
	select count(*) from TBLOG_VIDEO where FSAPPROVE_STATE = 'Y' --243525
end

if @case = 201801041055 begin
	--新聞檔案長度大於1 小時，內容描述小於 50 個字 濾掉最夜新聞
	select A.FSFILE_NO 檔案編號, 
		FSTITLE 標題, 
		REPLACE(A.FSDESCRIPTION, char(13) + char(10), '') 內容, 
		LEN(A.FSDESCRIPTION) 文字長度, CAST(FNDURATION/(30.0*60*60) as decimal(4,1)) 時長,
		CAST(ROUND(FNFILE_SIZE/POWER(2.0,30), 2) as decimal(6, 2)) "檔案大小(GB)", 
		ISNULL(D.FSUSER, '') as 建立者, ISNULL(D.FSDEPT, '') 單位,
		--ISNULL(FXEXTEND, ''),
		C.name 頻道,
		B.RECORDTYPE 類別 from TBLOG_VIDEO A
	left join (select FSFILE_NO, 
						FXEXTEND.value('(/Media/Channel)[1]','nvarchar(2000)') AS [CHANNEL],
						FXEXTEND.value('(/Media/RecordType)[1]','nvarchar(2000)') AS [RECORDTYPE]
		from TBLOG_VIDEO) B on A.FSFILE_NO = B.FSFILE_NO
	left join TBCodeChannel C on B.CHANNEL = C.id
	left join TBUSERS D on A.FSCREATED_BY = D.FSUSER_ID
	where FSAPPROVE_STATE ='Y' and LEN(A.FSDESCRIPTION) < 550 and FNDURATION > 30*60*60 and FSTITLE not like '%最夜新聞%' order by A.FSFILE_NO, CHANNEL --1952 --> 1772 -> 796 (filter 最夜新聞)

	select * from TBINEWS --FSSTORY_ID=000F47AA:0007015E:4E0F5356, FSFILE_NAME, FSCONTENT
end

if @case = 201801171001-201801111034-201801041055 begin
	--新聞檔案長度大於1 小時，摘要描述與文稿內容小於 50 個字 濾掉最夜新聞
	select A.FSFILE_NO 檔案編號, 
		A.FSTITLE 標題, 
		REPLACE(A.FSDESCRIPTION, char(13) + char(10), '') 摘要內容, 
		LEN(A.FSDESCRIPTION) 摘要長度, CAST(FNDURATION/(30.0*60*60) as decimal(4,1)) 時長,
		ISNULL(E.FSCONTENT, '') 文稿內容, LEN(ISNULL(E.FSCONTENT, '')) 文稿長度,
		CAST(ROUND(FNFILE_SIZE/POWER(2.0,30), 2) as decimal(6, 2)) "檔案大小(GB)", 
		ISNULL(D.FSUSER, '') as 建立者, ISNULL(D.FSDEPT, '') 單位,
		--ISNULL(FXEXTEND, ''),
		C.name 頻道,
		B.RECORDTYPE 類別 from TBLOG_VIDEO A
	left join (select FSFILE_NO, 
						FXEXTEND.value('(/Media/Channel)[1]','nvarchar(2000)') AS [CHANNEL],
						FXEXTEND.value('(/Media/RecordType)[1]','nvarchar(2000)') AS [RECORDTYPE]
		from TBLOG_VIDEO) B on A.FSFILE_NO = B.FSFILE_NO
	left join TBCodeChannel C on B.CHANNEL = C.id
	left join TBUSERS D on A.FSCREATED_BY = D.FSUSER_ID
	left join TBINEWS E on A.FSSTORY_ID = E.FSSTORY_ID
	where FSAPPROVE_STATE ='Y' and (LEN(A.FSDESCRIPTION) < 50 and LEN(ISNULL(E.FSCONTENT, '')) < 50) and FNDURATION > 30*60*60 and A.FSTITLE not like '%最夜新聞%' order by A.FSFILE_NO, CHANNEL 
	--1952 --> 1772 -> 796 -> 626 (filter 最夜新聞)

	select * from TBINEWS --FSSTORY_ID=000F47AA:0007015E:4E0F5356, FSFILE_NAME, FSCONTENT
	select top 1000 * from TBLOG_VIDEO
	select * from TBLOG_VIDEO where FSFILE_NO = '201709030010003'
	select * from TBINEWS where FSFILE_NAME = 'H030916FGE' --201005300010018

	select * from TBLOG_VIDEO where FSFILE_NO = '201005300010018' --2000搶救白海豚信託, FSSTORY_ID = 2AFC00DF:009CE806:4C0158A7
	select * from TBINEWS where FSFILE_NO = '201005300010018' --FSSTORY_ID = 2AFC00DF:009CE806:4C0158A7
	select * from TBLOG_VIDEO A join TBINEWS B on A.FSSTORY_ID = B.FSSTORY_ID --12618

	select A.FSFILE_NO, B.FSFILE_NO, A.FSTITLE, A.FSDESCRIPTION, B.FSCONTENT, B.FSCONTENT_1, B.FSCONTENT_2, A.FXEXTEND, A.FSSTORY_ID, B.FSSTORY_ID
	from TBLOG_VIDEO A join TBINEWS B on A.FSSTORY_ID = B.FSSTORY_ID --12618

	select A.FSFILE_NO, A.FSTITLE, A.FSDESCRIPTION, B.FSCONTENT, B.FSCONTENT_1, B.FSCONTENT_2, A.FXEXTEND, A.FSSTORY_ID, B.FSSTORY_ID
	from TBLOG_VIDEO A left join TBINEWS B on A.FSSTORY_ID = B.FSSTORY_ID --244164

	select * from TBLOG_VIDEO where FSFILE_NO = '200906110010118'
	select * from TBINEWS where FSFILE_NO = '200906110010118'

	select A.FSFILE_NAME, B.FSFILE_NO, * from TBINEWS A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where  A.FSFILE_NO is not NULL order by A.FSFILE_NAME

	select A.FSFILE_NAME, count(B.FSFILE_NO) from TBINEWS A left join TBLOG_VIDEO B on  A.FSFILE_NO = B.FSFILE_NO
	group by A.FSFILE_NAME having count(B.FSFILE_NO) >= 1  order by A.FSFILE_NAME

	select count(FSFILE_NAME) from TBINEWS --372495
	select * from TBINEWS order by FDCREATED_DATE --2006-07-24 14:59:55.000 ~ 2012-01-17 19:50:24.000
	select * from TBINEWS order by FDUPDATED_DATE
	select * from TBINEWS where FSFILE_NO is not NULL order by FDCREATED_DATE --18853

	select * from tbinews_20151105
	select * from TBINEWS where FSFILE_NO = '201511050010091' --no record
	select * from TBINEWS where FSFILE_NAME = '2AC56104' --201105160010064

	--problem
	select * from TBLOG_VIDEO where FSFILE_NO = '201112220010078'
	select * from TBLOG_VIDEO where FSFILE_NO = '201311130010145'  --no FSSTORY_ID

	select A.FSFILE_NO 檔案編號, 
		A.FSTITLE 標題, 
		REPLACE(A.FSDESCRIPTION, char(13) + char(10), '') 摘要內容, 
		LEN(A.FSDESCRIPTION) 摘要長度, CAST(FNDURATION/(30.0*60*60) as decimal(4,1)) 時長,
		ISNULL(E.FSCONTENT, '') 文稿內容, LEN(ISNULL(E.FSCONTENT, '')) 文稿長度,
		CAST(ROUND(FNFILE_SIZE/POWER(2.0,30), 2) as decimal(6, 2)) "檔案大小(GB)", 
		ISNULL(D.FSUSER, '') as 建立者, ISNULL(D.FSDEPT, '') 單位,
		--ISNULL(FXEXTEND, ''),
		C.name 頻道,
		B.RECORDTYPE 類別, B.SCRIPT SCRIPT from TBLOG_VIDEO A
	left join (select FSFILE_NO, 
						FXEXTEND.value('(/Media/Script)[1]','nvarchar(2000)') AS [SCRIPT],
						FXEXTEND.value('(/Media/Channel)[1]','nvarchar(2000)') AS [CHANNEL],
						FXEXTEND.value('(/Media/RecordType)[1]','nvarchar(2000)') AS [RECORDTYPE]
		from TBLOG_VIDEO) B on A.FSFILE_NO = B.FSFILE_NO
	left join TBCodeChannel C on B.CHANNEL = C.id
	left join TBUSERS D on A.FSCREATED_BY = D.FSUSER_ID
	left join TBINEWS E on A.FSSTORY_ID = E.FSSTORY_ID
	where FSAPPROVE_STATE ='Y' and (LEN(A.FSDESCRIPTION) < 50 and LEN(ISNULL(E.FSCONTENT, '')) < 50 and LEN(B.SCRIPT) < 50) and FNDURATION > 30*60*60 and A.FSTITLE not like '%最夜新聞%' order by A.FSFILE_NO, CHANNEL --412

	--- 摘要和文稿內容字數小於 50 字
	declare @lf char(1)  set @lf = char(10)
	declare @cr char(1) set @cr = char(13)

	select A.FSFILE_NO 檔案編號, 
		A.FSTITLE 標題, 
		REPLACE(A.FSDESCRIPTION, char(13) + char(10), '') 摘要內容, 
		LEN(A.FSDESCRIPTION) 摘要長度, CAST(FNDURATION/(30.0*60*60) as decimal(4,1)) 時長,
		REPLACE(REPLACE(B.SCRIPT, @cr, ''), @lf, '') 文稿內容, LEN(ISNULL(B.SCRIPT, '')) 文稿長度,
		CAST(ROUND(FNFILE_SIZE/POWER(2.0,30), 2) as decimal(6, 2)) "檔案大小(GB)", 
		ISNULL(D.FSUSER, '') as 建立者, ISNULL(D.FSDEPT, '') 單位,
		--ISNULL(FXEXTEND, ''),
		C.name 頻道,
		B.RECORDTYPE 類別 from TBLOG_VIDEO A
	left join (select FSFILE_NO, 
						FXEXTEND.value('(/Media/Script)[1]','nvarchar(2000)') AS [SCRIPT],
						FXEXTEND.value('(/Media/Channel)[1]','nvarchar(2000)') AS [CHANNEL],
						FXEXTEND.value('(/Media/RecordType)[1]','nvarchar(2000)') AS [RECORDTYPE]
		from TBLOG_VIDEO) B on A.FSFILE_NO = B.FSFILE_NO
	left join TBCodeChannel C on B.CHANNEL = C.id
	left join TBUSERS D on A.FSCREATED_BY = D.FSUSER_ID
	where FSAPPROVE_STATE ='Y' and (LEN(A.FSDESCRIPTION) < 50 and LEN(B.SCRIPT) < 50) and FNDURATION > 30*60*60 and A.FSTITLE not like '%最夜新聞%' order by A.FSFILE_NO, CHANNEL --412

	--公視新聞 166-84-84
	declare @lf char(1)  set @lf = char(10)
	declare @cr char(1) set @cr = char(13)
	select A.FSFILE_NO 檔案編號, 
		A.FSTITLE 標題, 
		REPLACE(A.FSDESCRIPTION, char(13) + char(10), '') 摘要內容, 
		LEN(A.FSDESCRIPTION) 摘要長度, CAST(FNDURATION/(30.0*60*60) as decimal(4,1)) 時長,
		REPLACE(REPLACE(B.SCRIPT, @cr, ''), @lf, '') 文稿內容, LEN(ISNULL(B.SCRIPT, '')) 文稿長度,
		CAST(ROUND(FNFILE_SIZE/POWER(2.0,30), 2) as decimal(6, 2)) "檔案大小(GB)", 
		ISNULL(D.FSUSER, '') as 建立者, ISNULL(D.FSDEPT, '') 單位,
		--ISNULL(FXEXTEND, ''),
		C.name 頻道,
		B.RECORDTYPE 類別 from TBLOG_VIDEO A
	left join (select FSFILE_NO, 
						FXEXTEND.value('(/Media/Script)[1]','nvarchar(2000)') AS [SCRIPT],
						FXEXTEND.value('(/Media/Channel)[1]','nvarchar(2000)') AS [CHANNEL],
						FXEXTEND.value('(/Media/RecordType)[1]','nvarchar(2000)') AS [RECORDTYPE]
		from TBLOG_VIDEO) B on A.FSFILE_NO = B.FSFILE_NO
	left join TBCodeChannel C on B.CHANNEL = C.id
	left join TBUSERS D on A.FSCREATED_BY = D.FSUSER_ID
	where FSAPPROVE_STATE ='Y' and (LEN(A.FSDESCRIPTION) < 50 and LEN(B.SCRIPT) < 50) and FNDURATION > 30*60*60 and A.FSTITLE not like '%最夜新聞%' and C.name = '公視' order by A.FSFILE_NO, CHANNEL

	-- 客家新聞 79 -> 78 --> 12 -> 5 -> 5 -> 5
	declare @lf char(1)  set @lf = char(10)
	declare @cr char(1) set @cr = char(13)
	select A.FSFILE_NO 檔案編號, 
		A.FSTITLE 標題, 
		REPLACE(A.FSDESCRIPTION, char(13) + char(10), '') 摘要內容, 
		LEN(A.FSDESCRIPTION) 摘要長度, CAST(FNDURATION/(30.0*60*60) as decimal(4,1)) 時長,
		REPLACE(REPLACE(B.SCRIPT, @cr, ''), @lf, '') 文稿內容, LEN(ISNULL(B.SCRIPT, '')) 文稿長度,
		CAST(ROUND(FNFILE_SIZE/POWER(2.0,30), 2) as decimal(6, 2)) "檔案大小(GB)", 
		ISNULL(D.FSUSER, '') as 建立者, ISNULL(D.FSDEPT, '') 單位,
		--ISNULL(FXEXTEND, ''),
		C.name 頻道,
		B.RECORDTYPE 類別 from TBLOG_VIDEO A
	left join (select FSFILE_NO, 
						FXEXTEND.value('(/Media/Script)[1]','nvarchar(2000)') AS [SCRIPT],
						FXEXTEND.value('(/Media/Channel)[1]','nvarchar(2000)') AS [CHANNEL],
						FXEXTEND.value('(/Media/RecordType)[1]','nvarchar(2000)') AS [RECORDTYPE]
		from TBLOG_VIDEO) B on A.FSFILE_NO = B.FSFILE_NO
	left join TBCodeChannel C on B.CHANNEL = C.id
	left join TBUSERS D on A.FSCREATED_BY = D.FSUSER_ID
	where FSAPPROVE_STATE ='Y' and (LEN(A.FSDESCRIPTION) < 50 and LEN(B.SCRIPT) < 50) and FNDURATION > 30*60*60 and A.FSTITLE not like '%最夜新聞%' and C.name = '客家' order by A.FSFILE_NO, CHANNEL

	--宏觀 17702
	declare @lf char(1)  set @lf = char(10)
	declare @cr char(1) set @cr = char(13)
	select A.FSFILE_NO 檔案編號,
		A.FSTITLE 標題, 
		REPLACE(A.FSDESCRIPTION, char(13) + char(10), '') 摘要內容, 
		LEN(A.FSDESCRIPTION) 摘要長度, CAST(FNDURATION/(30.0*60*60) as decimal(4,1)) 時長,
		REPLACE(REPLACE(B.SCRIPT, @cr, ''), @lf, '') 文稿內容, LEN(ISNULL(B.SCRIPT, '')) 文稿長度,
		CAST(ROUND(FNFILE_SIZE/POWER(2.0,30), 2) as decimal(6, 2)) "檔案大小(GB)", 
		ISNULL(D.FSUSER, '') as 建立者, ISNULL(D.FSDEPT, '') 單位,
		--ISNULL(FXEXTEND, ''),
		C.name 頻道,
		B.RECORDTYPE 類別 from TBLOG_VIDEO A
	left join (select FSFILE_NO, 
						FXEXTEND.value('(/Media/Script)[1]','nvarchar(2000)') AS [SCRIPT],
						FXEXTEND.value('(/Media/Channel)[1]','nvarchar(2000)') AS [CHANNEL],
						FXEXTEND.value('(/Media/RecordType)[1]','nvarchar(2000)') AS [RECORDTYPE]
		from TBLOG_VIDEO) B on A.FSFILE_NO = B.FSFILE_NO
	left join TBCodeChannel C on B.CHANNEL = C.id
	left join TBUSERS D on A.FSCREATED_BY = D.FSUSER_ID
	where FSAPPROVE_STATE ='Y' and C.name = '宏觀' order by A.FSFILE_NO, CHANNEL

	select * from TBCodeChannel
end