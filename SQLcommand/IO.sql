USE MAM
GO

IF OBJECT_ID(N'dbo.Split', N'TF') IS NOT NULL
	DROP FUNCTION Split;
GO
CREATE FUNCTION Split (
      @InputString VARCHAR(8000),
      @Delimiter VARCHAR(50)
)

RETURNS @Items TABLE (
      Item VARCHAR(8000)
)

AS
BEGIN
      	DECLARE @Item VARCHAR(8000)
      	DECLARE @ItemList VARCHAR(8000)
      	DECLARE @DelimIndex INTEGER 

	SET @InputString = REPLACE(@InputString, CHAR(9), ' ') -- replace tab with white space

      	IF (@Delimiter IS NULL)
            SET @Delimiter = ','

	--INSERT INTO @Items VALUES (@Delimiter) -- Diagnostic
	--INSERT INTO @Items VALUES (@InputString) -- Diagnostic

	SET @ItemList = RTRIM(LTRIM(@InputString))
	SET @DelimIndex = CHARINDEX(@Delimiter, @ItemList, 1)

	WHILE (@DelimIndex != 0)
	BEGIN
      		SET @Item = RTRIM(SUBSTRING(@ItemList, 0, @DelimIndex))
		if (@item != '')
			INSERT INTO @Items VALUES (@Item)

            	SET @ItemList = LTRIM(SUBSTRING(@ItemList, @DelimIndex + 1, LEN(@ItemList) - @DelimIndex))
            	SET @DelimIndex = CHARINDEX(@Delimiter, @ItemList, 0)
	END -- End WHILE

        INSERT INTO @Items VALUES (@ItemList)

	RETURN

END -- End Function
GO

IF OBJECT_ID(N'dbo.READFILE', N'P') IS NOT NULL
	DROP PROC READFILE;
GO
CREATE PROC READFILE
	@file varchar(100)
AS

	DECLARE @TBLOG_MASTERCONTROL_FTP TABLE (
		FSTIME varchar(8) NOT NULL,
		FSVIDEO_ID nvarchar(10) NOT NULL,
		FSCHANNEL_ID char(2) NOT NULL,
		FSFTP_ID char(1) NOT NULL,
		FNFILE_SIZE bigint NOT NULL
		/*
		CONSTRAINT [PK_TBLOG_MASTERCONTROL_FTP] PRIMARY KEY CLUSTERED (
					 [FSTIME] ASC ,
					 [FSVIDEO_ID] ASC ,
					 [FSFTP_ID] ASC
		) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF , IGNORE_DUP_KEY = OFF , ALLOW_ROW_LOCKS= ON, ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
		*/
	) --declare table variable

	DECLARE @videoID nvarchar(10)
	CREATE TABLE #tempImport (
		line nvarchar(300)
	)

	DECLARE @itemTab TABLE(
		item nvarchar(200)
	) 

	BULK INSERT #tempImport
	FROM '\\10.13.200.5\MAMUpload\MASTER_CONTROL\VIDEO_SERVER_FILES\MEDIALIST.txt'
	--FROM '\\10.13.200.5\MAMUpload\\MASTER_CONTROL\VIDEO_SERVER_FILES\MEDIALIST_06052015-utf8.txt'
	--FROM 'C:\temp\MEDIALIST_06052015-utf8.txt'
	--FROM @file
	WITH
	(
		ROWTERMINATOR = '\n'
	)
	--select * from #tempImport


	DECLARE @lineStr varchar(300)
	DECLARE CSR CURSOR FOR select * from #tempImport
	DECLARE @timeStr char(8) = CONVERT(char(8), GETDATE(), 112)

	OPEN CSR
	FETCH NEXT FROM CSR INTO @lineStr
	WHILE (@@FETCH_STATUS = 0) BEGIN
		--print @lineStr
		--select * from Split(@lineStr, ' ')
		INSERT INTO @itemTab(item) select TOP 1 Item from Split(@lineStr, ' ')
		FETCH NEXT FROM CSR INTO @lineStr
	END --WHILE

	CLOSE CSR
	DEALLOCATE CSR

	--select item from @itemTab where len(item) = 6 group by item having count(item) = 1
	--return

	DECLARE CSR1 CURSOR FOR select item from @itemTab where len(item) = 6 group by item having count(item) = 1
	
	OPEN CSR1

	FETCH NEXT FROM CSR1 INTO @videoID
	
	WHILE (@@FETCH_STATUS = 0) BEGIN

		--INSERT INTO @TBLOG_MASTERCONTROL_FTP(FSTIME, FSVIDEO_ID, FSCHANNEL_ID, FSFTP_ID, FNFILE_SIZE) VALUES(CONVERT(char(8), GETDATE(), 112), @videoID, '11', '1', 0) --DEBUG
		if NOT EXISTS(select * from TBLOG_MASTERCONTROL_FTP where FSTIME = @timeStr and FSVIDEO_ID = @videoID and FSFTP_ID = '1') BEGIN
			PRINT 'INSERT ' + @videoID
			INSERT INTO TBLOG_MASTERCONTROL_FTP(FSTIME, FSVIDEO_ID, FSCHANNEL_ID, FSFTP_ID, FNFILE_SIZE) VALUES(@timeStr, @videoID, '11', '1', 0)
		END


		--select CONVERT(char(8), GETDATE(), 112) as FSTIME, item as FSVIDEO_ID, '11' as FSCHANNEL_ID, '1' as FSFTP_ID, 0 as FNFILE_SIZE from @itemTab group by item having count(item) = 1
		FETCH NEXT FROM CSR1 INTO @videoID
	END -- WHILE

	CLOSE CSR1
	DEALLOCATE CSR1
	
	select * from @TBLOG_MASTERCONTROL_FTP
	--select count(item) number, item from @itemTab group by item order by number DESC
	--select * from #tempImport

GO --END READFILE




--------------------------------------------------------------------------------------------------
IF OBJECT_ID(N'dbo.TEST_CASE', N'P') IS NOT NULL
	DROP PROC TEST_CASE;
GO
CREATE PROC TEST_CASE
	@case integer, @subcase integer
AS
DECLARE @file varchar(100) = 'C:\temp\MEDIALIST_06052015.txt'
if @case = 1 BEGIN
	EXEC READFILE @file
	--select * from TBLOG_MASTERCONTROL_FTP order by FSTIME DESC
	--delete from TBLOG_MASTERCONTROL_FTP where FSTIME = '20150615'
END

GO
--------------------------------------------------------------------------------------------------

DECLARE @case integer, @subcase integer

set @case = 1 set @subcase = 1
EXEC TEST_CASE @case, @subcase

GO

--DROP PROC TEST_CASE
--DROP PROC READFILE
--DROP FUNCTION Split
select * from TBLOG_MASTERCONTROL_FTP where FSCHANNEL_ID = '11' order by FSTIME DESC