
use MAM

-- 根據節目 子集 頭銜 公司姓名 搜尋重複人員
select * from tbprogmen x where exists (
	select fsprog_id, fnepisode, fstitleid, fsname
	from tbprogmen 
	group by fsprog_id, fnepisode, fstitleid, fsname
	having count(*) > 1
	and tbprogmen.fsprog_id = x.fsprog_id
	and tbprogmen.fnepisode = x.fnepisode
	and tbprogmen.fstitleid = x.fstitleid
	and tbprogmen.fsname = x.fsname)
order by fsprog_id, fnepisode, fstitleid, fsname
