
if OBJECT_ID(N'tempdb.dbo.#TEST_CASE', N'P') IS NOT NULL
	DROP PROC #TEST_CASE
GO

CREATE PROC #TEST_CASE
	@case integer, @subcase integer
AS

if @case = 1 BEGIN
	select * from TBMASTERCONTROL_FTP_FINISH

END --case 1
else if @case = 2 BEGIN 
	if @subcase = 1 BEGIN
		select * from TBTRACKINGLOG where FDTIME between '2015/08/27 14:20' and '2015/08/27 18:35' and FSMESSAGE like '%104114%'  ORDER BY FDTIME DESC
		select * from TBTRACKINGLOG where FDTIME between '2015/08/27 17:28' and '2015/08/27 17:30' and FSMESSAGE like '%UPDATE_TBLOG_VIDEO_STT_OK%'  ORDER BY FDTIME DESC
		select * from TBTRACKINGLOG where FDTIME between '2015/08/27 17:28' and '2015/08/27 17:30'   ORDER BY FDTIME
		-- [USER] 檢索與調用：轉檔完成，檢索不到資料 https://groups.google.com/forum/?hl=zh-TW#!topic/pts-mam/S_6XB6XD4mk
		select * from TBTRACKINGLOG where FDTIME between '2015/08/31 12:28' and '2015/08/31 17:30' and FSMESSAGE like '%P201412009040002%' ORDER BY FDTIME
		select * from TBTRACKINGLOG where FDTIME between '2015/08/31 15:11' and '2015/08/31 15:30'  ORDER BY FDTIME
	END
	else if @subcase = 2 BEGIN
		select top 1000 * from TBTRACKINGLOG where FSPROGRAM like '%TSM3310%' and FDTIME between '2015-08-12 00:00' and '2015-08-13 17:30' order by FDTIME DESC
		select top 1000 * from TBTRACKINGLOG where FDTIME between '2015-09-15 20:00' and '2015-09-15 21:30' order by FDTIME DESC
	END
	else if @subcase = 3 BEGIN --201509230033 轉檔完成但是 anystream 顯示轉檔失敗，無法重啟任務，以置換方式重轉
		select top 1000 * from TBTRACKINGLOG where FDTIME between '2015-09-24 19:00' and '2015-09-24 21:00' order by FDTIME
		select top 1000 * from TBTRACKINGLOG where FDTIME between '2015-09-24 19:00' and '2015-09-25 21:00' and FSPROGRAM = 'ClickToStartTrancode' and FSMESSAGE like '%G201628000010003%'
		select top 1000 * from TBTRACKINGLOG where FDTIME between '2015-09-24 19:00' and '2015-09-25 21:00' and FSMESSAGE like '%G201628000010003%'
		select top 1000 * from TBTRACKINGLOG where FDTIME between '2015-09-24 19:00' and '2015-09-25 21:00' and FSMESSAGE like '%G201628000010002%'
		select top 1000 * from TBTRACKINGLOG where FDTIME between '2015-09-24 19:00' and '2015-09-25 21:00' and FSMESSAGE like '%107009%' --以轉檔任務編號查詢
		select top 1000 * from TBTRACKINGLOG where FDTIME between '2015-09-24 19:34' and '2015-09-24 19:50'
	END
	else if @subcase = 4 BEGIN -- 鳳梨 TV 調用出現失敗
		select * from TBTRACKINGLOG where FDTIME between '2015-10-14 15:00' and '2015-10-14 15:30'
		select * from TBTRACKINGLOG where FDTIME between '2015-10-13 00:00' and '2015-10-13 06:00'
		select * from TBTRACKINGLOG where FDTIME between '2015-10-16 15:50' and '2015-10-16 16:00' order by FDTIME desc
	END
	else if @subcase = 5 BEGIN -- 新聞片庫入庫轉檔 30 支但是有10支轉很久
		select * from TBTRACKINGLOG where FDTIME between '2015-10-20 00:00' and '2015-10-20 09:00' order by FDTIME desc

	END
	else if @subcase = 5 BEGIN -- TS3310 無法上架
		select * from TBTRACKINGLOG where FDTIME between '2015-10-30 09:00' and '2015-10-30 20:00' and FSPROGRAM like '%WSTSM%' order by FDTIME desc
		select * from TBTRACKINGLOG where FDTIME between '2015-11-02 11:49' and '2015-11-02 12:00' and FSPROGRAM like '%WSTSM%' order by FDTIME desc
		select * from TBTRACKINGLOG where FDTIME between '2015-10-30 11:49' and '2015-11-02 12:00' and FSMESSAGE like '%A00526L4%' order by FDTIME desc
		select * from TBTRACKINGLOG where FDTIME between '2015-11-02 11:49' and '2015-11-02 12:10' order by FDTIME desc
		select * from TBTRACKINGLOG where FDTIME between '2015-12-30 15:49' and '2015-12-30 22:10' order by FDTIME desc


	END
	else if @subcase = 6 BEGIN
	  select * from TBTRACKINGLOG where FDTIME between '2015-12-6 10:50:00' and '2015-12-6 10:59:00'and FSMESSAGE like '000028UL' order by FDTIME desc 
	END
	else if @subcase = 7 BEGIN --查詢照片入庫紀錄
		-- error
		select * from TBTRACKINGLOG where FDTIME between '2015-12-15 09:00:00' and '2015-12-15 17:59:00' and FSPROGRAM = 'InsTBLOG_PHOTO' order by FDTIME
		-- ok
		select * from TBTRACKINGLOG where FDTIME between '2015-12-16 09:00:00' and '2015-12-16 17:59:00' and FSPROGRAM = 'InsTBLOG_PHOTO' order by FDTIME
	END
	else if @subcase = 8
		select * from TBTRACKINGLOG where FDTIME between '2015-12-18 10:00:00' and '2015-12-18 18:00'

	else if @subcase = 9
		select * from TBTRACKINGLOG where FDTIME between '2016-1-5 12:10:00' and '2016-1-5 12:28:00'
	else if @subcase = 10 --流言追追追 13 已轉檔完成但是送帶轉檔單仍顯示轉檔中
		select * from TBTRACKINGLOG where FDTIME between '2016-1-6 10:30' and '2016-1-6 11:30' order by FDTIME

	else if @subcase = 10
		select * from TBTRACKINGLOG where FDTIME between '2016-1-25 17:20' and '2016-1-25 17:40'

END --case 2

else if @case = 3 -- TAPE Library issue
	if @subcase = 5 BEGIN -- TS3310 無法上架
		select * from TBTRACKINGLOG where FDTIME between '2015-10-30 09:00' and '2015-10-30 20:00' and FSPROGRAM like '%WSTSM%' order by FDTIME desc
		select * from TBTRACKINGLOG where FDTIME between '2015-11-02 11:49' and '2015-11-02 12:00' and FSPROGRAM like '%WSTSM%' order by FDTIME desc
		select * from TBTRACKINGLOG where FDTIME between '2015-10-30 11:49' and '2015-11-02 12:00' and FSMESSAGE like '%A00526L4%' order by FDTIME desc
		select * from TBTRACKINGLOG where FDTIME between '2015-11-02 11:49' and '2015-11-02 12:10' order by FDTIME desc
		select * from TBTRACKINGLOG where FDTIME between '2015-12-30 15:49' and '2015-12-30 22:10' order by FDTIME desc
	end
	else if @subcase = 1 begin -- [TS3500] TS3500退帶異常
		select * from TBTRACKINGLOG where FDTIME between '2016-1-13 07:00' and '2016-1-13 9:00' and FSPROGRAM like '%WSTSM%' order by FDTIME desc
		select * from TBTRACKINGLOG where FDTIME between '2016-1-13 07:00' and '2016-1-13 12:00' and FSPROGRAM like '%WSTSM%' and FSMESSAGE like '%退出磁帶%' order by FDTIME desc
		select * from TBTRACKINGLOG where FDTIME between '2016-1-13 11:10' and '2016-1-13 11:30' order by FDTIME desc
	end
end --end case 3
 
else if @case = 4 begin
	select * from TBTRACKINGLOG where FDTIME between '20160406 09:00' and '20160406 12:00' order by FDTIME desc
	select * from TBTRAN_LOG where FDDATE between '2016/04/06 10:00' and '2016/04/06 12:00' order by FDDATE desc
	select * from TBTRAN_LOG where FDDATE between '2016/04/06 10:00' and '2016/04/06 12:00' and FSUSER_ID = 'PTS_AP' order by FDDATE desc
	select * from TBTRAN_LOG where FDDATE between '2016/04/11 15:20' and '2016/04/11 15:30'
end -- case 4

else if @case = 5 begin --查詢派送檔案紀錄
	select * from TBTRACKINGLOG where FDTIME between '20160502 09:00' and '20160502 14:00' and FSMESSAGE like '%00002IZQ%' order by FDTIME
	select * from TBTRACKINGLOG where FDTIME between '20160502 18:00' and '20160502 21:00' and FSMESSAGE like '%000027CI%'

	-- 查詢時段區間的每天派送最早和最晚時間
	select MIN(FDTIME) 'START TIME', MAX(FDTIME) 'COMPLETE TIME' from TBTRACKINGLOG where FDTIME between '2016-04-01' and '2016-05-01' and FSPROGRAM = 'WSPGM_MSSCopy/RegisterMSSCopyService'
	group by CAST(FDTIME as DATE) order by MIN(FDTIME) desc

end


select [Current LSN],
       [Operation],
       [Transaction Name],
       [Transaction ID],
       [Transaction SID],
       [SPID],
       [Begin Time]
FROM   fn_dblog(null,null)

GO

DECLARE @case integer, @subcase integer
DECLARE @begin date, @end date


set @case = 2 set @subcase = 2

EXEC #TEST_CASE @case, @subcase

GO
