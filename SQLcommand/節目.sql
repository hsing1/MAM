


declare @case int, @subcase int
set @case = 1
set @subcase = 1

if @case = 1 begin
	if @subcase = 1 begin-- used in TranProgList()
		select A.FNLENGTH,A.FSPGDNAME,A.FSPGDENAME,B.FSPGMNAME from TBPROG_D A,TBPROG_M B where A.FSPROG_ID=B.FSPROG_ID and A.FSPROG_ID='0000001' and A.FnEpisode=1
		select * from TBPROG_D where FNEPISODE = 0
	end
end -- case 1

if @case = 2 begin -- 查詢節目相關資料
	if @subcase = 1 begin
		select * from TBPROG_M where FSCHANNEL like '%51%'  --PTS-MOD
		select * from TBPROG_M where FSCHANNEL like '%61%'  --HD-MOD
		select * from TBPROG_M where FSCHANNEL like '%99%'  
		select * from TBPROG_M where FSCHANNEL like '%02%'
		select * from TBPROG_M where FSCHANNEL like '%07%'
		select * from TBZCHANNEL
	end
	else if @subcase = 1 begin --SP_Q_TBPROG_M_BYCONDITIONS 查詢節目基本資料
		declare @FSCHANNEL_ID varchar(2)
		declare @FSCHANNEL varchar(50)
		declare @channelStr nvarchar(150)
		set @FSCHANNEL = '51'
		select @channelStr=COALESCE(@channelStr + '%','')+Item from  dbo.Split(@FSCHANNEL,';')
		print @channelStr


		set @FSCHANNEL_ID = ''
		set @FSCHANNEL = '51;'
		SELECT ISNULL (b.FSUSER_ChtName,'' ) as FSCREATED_BY_NAME, ISNULL(C.FSUSER_ChtName, '') as FSUPDATED_BY_NAME ,
		ISNULL(CONVERT (VARCHAR( 20) ,a. FDSHOWDATE , 111 ),'') as WANTSHOWDATE ,
		ISNULL(CONVERT (VARCHAR ,a.FDCRTDATE , 120 ),'') as WANT_FDCREATEDDATE ,
		ISNULL(CONVERT (VARCHAR ,a.FDUPDDATE , 120 ),'') as WANT_FDUPDATEDDATE ,
		a.* FROM TBPROG_M as a
		left outer JOIN TBUSERS as b on a.FSCRTUSER = b. FSUSER_ID
		left outer join TBUSERS as c on a.FSUPDUSER = c. FSUSER_ID
		where 
		@FSCHANNEL = case when @FSCHANNEL = '' then @FSCHANNEL else a.FSCHANNEL end
		and a.FSCHANNEL_ID = case when @FSCHANNEL_ID = '' then @FSCHANNEL_ID else @FSCHANNEL_ID end
		-- a.FSPGMNAME like '%' or b.FSUSER_ChtName = '系統管理帳號'
		--@FSCHANNEL_ID = case when  @FSCHANNEL_ID = '' then @FSCHANNEL_ID else a.FSCHANNEL_ID end
	end
end -- case 2
else if @case = 3 begin
	if @subcase = 1 begin
		select * from TBPGM_PROMO where FDEXPIRE_DATE < '2015-12-31' order by FDEXPIRE_DATE desc
		select * from TBPGM_PROMO where FSPROMO_ID = '20141200714'
	end
end
else if @case = 4 begin
	if @subcase = 1 begin
		select * from TBPROG_D where FSPGDNAME like '%2-1%' and FNEPISODE > 1 and FSDEL != 'Y'
		select * from TBPROG_D where FSPGDNAME like '%客家猴厲害%'
		select * from TBPROG_D where FSPGDNAME like '%熊蜂的祕密%'
	end
end


GO

select * from TBPROG_M where FSPROG_ID='0000082'


select * from TBPGM_FIRST_PLAY_LIST



-- 統計單日長帶節目數量
select * from TBPGM_QUEUE order by FDDATE asc

--查詢某一天節目表
select * from TBPGM_QUEUE where FDDATE='2015-03-16' order by FSBEG_TIME
select * from TBPGM_QUEUE where FSPROG_NAME='元味好生活' and FNEPISODE=5 order by FSBEG_TIME
select * from TBPGM_COMBINE_QUEUE where FDDATE='2014-09-23' order by FSPLAY_TIME asc
select distinct(FSPROG_ID), FSPROG_NAME from TBPGM_COMBINE_QUEUE where FDDATE='2014-09-23' and FCPROPERTY='G'
select count(distinct(FSPROG_ID)) from TBPGM_COMBINE_QUEUE where FDDATE='2014-09-23' and FCPROPERTY='G'
select * from TBPGM_COMBINE_QUEUE where FCPROPERTY='P'

select * from TBPGM_BANK_DETAIL where FNREPLAY>1
select * from TBPGM_BANK_DETAIL order by FDBEG_DATE
select * from TBPGM_BANK_DETAIL where FDBEG_DATE='2014-09-23'

select * from TBLOG_WORKER order by FDSTIME desc


select * from TBNORECORD_FILEID

select * from TBPGM_FIRST_PLAY_LIST

select * from TBPGM_INSERT_TAPE

select * from TBPGM_QUEUE where FDDATE='2014-09-23' 
select * from TBPGM_QUEUE where FNREPLAY=1

select * from TBPGM_QUEUE_INSERT_TAPE
select * from TBPGM_PROG_PROMO_LINK order by FSPROG_ID

select * from TBPGM_ARR_PROMO where FDDATE='2014-09-23' order by FSPLAYTIME
select * from TBPGM_PROMO


--
select C.FSPGMNAME, B.FSPROMO_NAME, A.FSPLAYTIME,  A.FSDUR from TBPGM_ARR_PROMO A, TBPGM_PROMO B, TBPROG_M C where (A.FSPROMO_ID=B.FSPROMO_ID) and (C.FSPROG_ID=A.FSPROG_ID) and (A.FDDATE='2014-09-23') order by A.FSPLAYTIME

--查詢節目基本資料、子集

select * from TBPROG_M where FSPGMNAME like '公視藝文大道'
select * from TBPROG_D where FSPROG_ID = '0002197'
select * from TBPROG_M where FSPGMNAME='我們的島' --0000072
select * from TBPROG_M where FSPGMNAME='水果冰淇淋' --0000060
select * from TBPROG_M where FSPGMNAME='16個夏天'
select * from TBPROG_M where FSPGMNAME like '%白教堂血案%'
select * from TBPROG_M where FSPROG_ID = '2010975'
select * from TBPROG_D where FSPROG_ID = '2010975'

select m.FSPROG_ID, m.FSPGMNAME, d.FSPGDNAME, d.FNEPISODE from TBPROG_M as m, TBPROG_D as d where m.FSPROG_ID=d.FSPROG_ID and d.FSPROG_ID='0000060'

select m.FSPGMNAME, d.* from TBPROG_M as m, TBPROG_D as d where (m.FSPROG_ID=d.FSPROG_ID) and  d.FSPROG_ID='0000060' and (d.FNEPISODE=1533 or d.FNEPISODE=1530 or d.FNEPISODE=1532 or d.FNEPISODE=1536 or d.FNEPISODE=1531)

select * from TBPROG_D where FSPROG_ID='0000060' and FNEPISODE='1531'
select * from TBPROG_D where FNLENGTH >= 120

select * from TBPROPOSAL where FSPROG_ID = '2013811'
select * from TBPROPOSAL where FSPROG_NAME like '%決戰%'
select count(FSPROG_ID) '筆數', FSPROG_NAME '成案名稱' from TBPROPOSAL group by FSPROG_NAME having count(FSPROG_ID) > 1
select * from TBPROG_M where FSPROG_ID = '2013811'
select count(FSPROG_ID), FSPGMNAME from TBPROG_M where FSDEL = 'N' group by FSPGMNAME having count(FSPROG_ID) > 1
select * from TBPROG_M where FSPGMNAME ='PeoPo公民新聞報(2012週末版)'

select * from TBZCHANNEL
select * from TBPROG_M