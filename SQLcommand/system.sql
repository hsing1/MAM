
if OBJECT_ID(N'dbo.TEST_CASE', N'P') is not NULL
	DROP PROC TEST_CASE
GO
CREATE PROC TEST_CASE
	@case integer, @subcase integer
AS
if @case = 0 BEGIN --查詢 table 資訊
	if @subcase = 1 begin
		declare @tableName varchar(100) = 'TBPROG_M'
		declare @columnTable TABLE (tableName varchar(200), col_name varchar(200), type varchar(25)) -- column name
		declare @foreignTable TABLE (FKname varchar(300), tableName varchar(200), columnName varchar(200), referenceTable varchar(200), referenceColumn varchar(200))
		declare @primaryTable TABLE (tbName varchar(200), columnName varchar(200))
		declare @tableInfo TABLE(tableName varchar(200), columnName varchar(300), type varchar(20), primaryKey varchar(1), foreignKeyConstraint varchar(300), referencedTable varchar(200), referencedColumn varchar(200))

		--List column name
		INSERT into @columnTable select TABLE_NAME, COLUMN_NAME, DATA_TYPE from INFORMATION_SCHEMA.COLUMNS order by TABLE_NAME
		--select COLUMN_NAME, DATA_TYPE from INFORMATION_SCHEMA.COLUMNS where TABLE_CATALOG = 'MAM' and TABLE_NAME = @tableName

		-- List foreign key
		INSERT into @foreignTable
		SELECT f.name AS ForeignKey, 
		OBJECT_NAME(f.parent_object_id) AS TableName, 
		COL_NAME(fc.parent_object_id, fc.parent_column_id) AS ColumnName, 
		OBJECT_NAME (f.referenced_object_id) AS ReferenceTableName, 
		COL_NAME(fc.referenced_object_id, fc.referenced_column_id) AS ReferenceColumnName 
		FROM sys.foreign_keys AS f
		INNER JOIN sys.foreign_key_columns AS fc 
		ON f.OBJECT_ID = fc.constraint_object_id
		order by tableName

		--select * from @foreignTable

	
		-- List primary key
		INSERT into @primaryTable
		SELECT KU.table_name as tablename,column_name as primarykeycolumn
		FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS TC
		INNER JOIN
		INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KU
		ON TC.CONSTRAINT_TYPE = 'PRIMARY KEY' AND
		TC.CONSTRAINT_NAME = KU.CONSTRAINT_NAME
		ORDER BY KU.TABLE_NAME, KU.ORDINAL_POSITION;

		--select * from @primaryTable

		-- create result table
		INSERT into @tableInfo
		select ct.tableName as 'TABLE NAME', ct.col_name as 'COLUMN NAME',  ct.type as 'TYPE',
		(CASE ISNULL(pt.columnName, '')
			WHEN '' THEN ''
			ELSE 'Y'
		END) as 'PRIMARY KEY', 	
		ISNULL(ft.FKname, '') as 'FOREIGN KEY CONSTRAINT', ISNULL(ft.referenceTable, '') as 'REFERENCED TABLE', ISNULL(ft.referenceColumn, '') as 'REFERENCED COLUMN'
		from (@columnTable ct left outer join @foreignTable ft on ct.col_name = ft.columnName and ct.tableName = ft.tableName) left outer join @primaryTable pt on ct.col_name = pt.columnName and ct.tableName = pt.tbName

		select tableName as 'TABLE NAME', columnName as 'COLUMN NAME', type as 'TYPE', primaryKey as 'PRIMARY KEY', foreignKeyConstraint as 'FOREIGN KEY CONSTRAINT', referencedTable as 'REFERENCED TABLE', referencedColumn 'REFERENCED COLUMN' from @tableInfo
	end --subcase 1

	else if @subcase = 2 begin

		declare @tableName varchar(100) = 'TBLOG_VIDEO_HD_MC'
		declare @columnTable TABLE (tableName varchar(200), col_name varchar(200), type varchar(25)) -- column name
		declare @foreignTable TABLE (FKname varchar(300), tableName varchar(200), columnName varchar(200), referenceTable varchar(200), referenceColumn varchar(200))
		declare @primaryTable TABLE (tbName varchar(200), columnName varchar(200))

		--List column name
		INSERT into @columnTable select TABLE_NAME, COLUMN_NAME, DATA_TYPE from INFORMATION_SCHEMA.COLUMNS  where TABLE_CATALOG = 'MAM' and TABLE_NAME = @tableName
		--select * from @columnTable
		--select COLUMN_NAME, DATA_TYPE from INFORMATION_SCHEMA.COLUMNS where TABLE_CATALOG = 'MAM' and TABLE_NAME = 'TBLOG_VIDEO_HD_MC'

		-- List foreign key
		INSERT into @foreignTable
		SELECT f.name AS ForeignKey, 
		OBJECT_NAME(f.parent_object_id) AS TableName, 
		COL_NAME(fc.parent_object_id, fc.parent_column_id) AS ColumnName, 
		OBJECT_NAME (f.referenced_object_id) AS ReferenceTableName, 
		COL_NAME(fc.referenced_object_id, fc.referenced_column_id) AS ReferenceColumnName 
		FROM sys.foreign_keys AS f
		INNER JOIN sys.foreign_key_columns AS fc 
		ON f.OBJECT_ID = fc.constraint_object_id
		where OBJECT_NAME(f.parent_object_id) = @tableName

		--select * from @foreignTable

	
		-- List primary key
		INSERT into @primaryTable
		SELECT KU.table_name as tablename,column_name as primarykeycolumn
		FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS TC
		INNER JOIN
		INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KU
		ON TC.CONSTRAINT_TYPE = 'PRIMARY KEY' AND
		TC.CONSTRAINT_NAME = KU.CONSTRAINT_NAME
		and ku.table_name = @tableName
		ORDER BY KU.TABLE_NAME, KU.ORDINAL_POSITION;

		--select * from @primaryTable
		select ct.tableName as 'TABLE NAME', ct.col_name as 'COLUMN NAME',  ct.type as 'TYPE',
		(CASE ISNULL(pt.columnName, '')
			WHEN '' THEN ''
			ELSE 'Y'
		END) as 'PRIMARY KEY', 	
		ISNULL(ft.FKname, '') as 'FOREIGN KEY CONSTRAINT', ISNULL(ft.referenceTable, '') as 'REFERENCED TABLE', ISNULL(ft.referenceColumn, '') as 'REFERENCED COLUMN'
		from (@columnTable ct left outer join @foreignTable ft on ct.col_name = ft.columnName) left outer join @primaryTable pt on ct.col_name = pt.columnName

		-----------------------------------------------------------------------------------


		select * from sys.foreign_key_columns

		select * from sys.foreign_keys as f where OBJECT_NAME(f.parent_object_id) = 'TBPROG_M'

		select TABLE_NAME, COLUMN_NAME, DATA_TYPE, fk.name FK_Name from (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'TBPROG_M') as col
		inner join sys.foreign_keys fk on col.table_name = OBJECT_NAME(fk.parent_object_id)
		-- where TABLE_CATALOG = 'MAM' and TABLE_NAME = @tableName
		select * from INFORMATION_SCHEMA.COLUMNS

	end --subcase 2

END

if @case = 1 BEGIN
	if @subcase = 1 --查詢server有哪些資料庫
		select * from sys.databases
	if @subcase = 2 --查詢server有哪些 linked server
		select * from sys.servers
	if @subcase = 3
		exec sp_readerrorlog --查詢SQL Server error log, 只有 securityadmin 角色的成員可以執行此預存程序。

	if @subcase = 4 BEGIN-- 查詢 DB 有幾個 tables
		SELECT COUNT(*) from information_schema.tables WHERE table_type = 'base table' 
		select * from INFORMATION_SCHEMA.TABLES where TABLE_CATALOG = 'MAM'
	END

	if @subcase = 5 BEGIN --INFORMATION_SCHEMA
		 select * from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where TABLE_NAME = 'TBPROG_M' --查詢 constraint information
		 SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS
		 --查詢 column information
		 select * from INFORMATION_SCHEMA.COLUMNS where TABLE_CATALOG = 'MAM' and TABLE_NAME = 'TBUSERS'
		 select COLUMN_NAME, DATA_TYPE from INFORMATION_SCHEMA.COLUMNS where TABLE_CATALOG = 'MAM' and TABLE_NAME = 'TBUSERS'

		 select * from sys.tables where name = 'TBPROG_M'
		 select * from sys.default_constraints
		 SELECT 
			TableName = t.Name,
			ColumnName = c.Name,
			dc.Name,
			dc.definition
			FROM sys.tables t
			INNER JOIN sys.default_constraints dc ON t.object_id = dc.parent_object_id
			INNER JOIN sys.columns c ON dc.parent_object_id = c.object_id AND c.column_id = dc.parent_column_id
			where t.Name = 'TBPROG_M'
			ORDER BY t.Name


END --CASE 1

else if @case = 2 BEGIN --sys.procedures 查詢 stored procedure 資訊
	if @subcase = 1 BEGIN	
		SELECT * FROM sys.procedures where name = 'PMAPURI' and OBJECT_DEFINITION(object_id) LIKE '%Any-Field (OR) WORD%'
		SELECT * FROM sys.procedures order by name
		select * from sys.objects where type_desc ='SQL_STORED_PROCEDURE' order by name
		select * from sys.objects where type ='P' order by name
		SELECT name FROM sys.procedures order by name
		SELECT * FROM sys.procedures order by modify_date desc
		
		select * from sys.objects where object_id = '990014658'
	END
	if @subcase = 2 BEGIN -- check constrain
		SELECT object_name(constid)  FROM sys.sysconstraints WHERE id = OBJECT_ID('TBPROG_M');
	END
END --CASE 2 

else if @case = 3 BEGIN --sysobjects
	if @subcase = 1
		select * from sysobjects where name='SP_U_TBUSERS1'
	if @subcase = 2
		select * from sysobjects where name like '%UserDepTempTable%'

	
END

else if @case = 4 BEGIN
	if @subcase = 1 -- show schema name by object id
		select OBJECT_SCHEMA_NAME(1979154096)
	if @subcase = 2 -- show object definition
		select OBJECT_DEFINITION(1803153469)
END

else if @case = 5 BEGIN --OBJECT_ID() 查詢 object id
	if @subcase = 1
		select OBJECT_ID('tempdb..#UserDepTempTable')
	if @subcase = 2
		select OBJECT_ID('tempdb..#UserDepTempTable', 'U')
	if @subcase = 3 BEGIN
		--use tempdb;
		select OBJECT_ID('#UserDepTempTable')
	END
END

else if @case = 6  BEGIN -- 查詢 stored procedure and function information
	if @subcase = 1
		SELECT * FROM   master.INFORMATION_SCHEMA.ROUTINES  where ROUTINE_NAME='SP_U_TBUSERS1'
	if @subcase = 2
		select *  from master.information_schema.routines where routine_type = 'PROCEDURE'
	if @subcase = 3 -- columns information
		select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME ='TBUSERS'
END

else if @case = 7 BEGIN -- 取得 SQL Server 資料庫正在執行的 T-SQL 指令與詳細資訊
	SELECT      r.scheduler_id as 排程器識別碼,
				status         as 要求的狀態,
				r.session_id   as SPID,
				r.blocking_session_id as BlkBy,
				substring(
									ltrim(q.text),
									r.statement_start_offset/2+1,
									(CASE
					 WHEN r.statement_end_offset = -1
					 THEN LEN(CONVERT(nvarchar(MAX), q.text)) * 2
					 ELSE r.statement_end_offset
					 END - r.statement_start_offset)/2)
					 AS [正在執行的 T-SQL 命令],
				r.cpu_time      as [CPU Time(ms)],
				r.start_time    as [開始時間],
				r.total_elapsed_time as [執行總時間],
				r.reads              as [讀取數],
				r.writes             as [寫入數],
				r.logical_reads      as [邏輯讀取數],
				-- q.text, /* 完整的 T-SQL 指令碼 */
				d.name               as [資料庫名稱]
	FROM        sys.dm_exec_requests r 
							CROSS APPLY sys.dm_exec_sql_text(sql_handle) AS q
							LEFT JOIN sys.databases d ON (r.database_id=d.database_id)
	WHERE       r.session_id > 50 AND r.session_id <> @@SPID

	ORDER BY    r.total_elapsed_time desc

END -- case 7

else if @case = 8 BEGIN -- check deadlock
	SELECT  L.request_session_id AS SPID, 
		DB_NAME(L.resource_database_id) AS DatabaseName,
		O.Name AS LockedObjectName, 
		P.object_id AS LockedObjectId, 
		L.resource_type AS LockedResource, 
		L.request_mode AS LockType,
		ST.text AS SqlStatementText,        
		ES.login_name AS LoginName,
		ES.host_name AS HostName,
		TST.is_user_transaction as IsUserTransaction,
		AT.name as TransactionName,
		CN.auth_scheme as AuthenticationMethod
	FROM    sys.dm_tran_locks L
		JOIN sys.partitions P ON P.hobt_id = L.resource_associated_entity_id
		JOIN sys.objects O ON O.object_id = P.object_id
		JOIN sys.dm_exec_sessions ES ON ES.session_id = L.request_session_id
		JOIN sys.dm_tran_session_transactions TST ON ES.session_id = TST.session_id
		JOIN sys.dm_tran_active_transactions AT ON TST.transaction_id = AT.transaction_id
		JOIN sys.dm_exec_connections CN ON CN.session_id = ES.session_id
		CROSS APPLY sys.dm_exec_sql_text(CN.most_recent_sql_handle) AS ST
	WHERE   resource_database_id = db_id()
	ORDER BY L.request_session_id
end --case 8

else if @case = 9 BEGIN --about backup model
	if @subcase = 1 begin --查詢 DB recovery model
		select name "Database Name", recovery_model_desc from sys.databases
	end
	if @subcase = 2 begin -- query mirroring status
		select * from sys.database_mirroring
	end
end -- CASE 9

else if @case = 10 BEGIN -- check constrain
	if @subcase = 1 BEGIN --查詢table 的foreign key
		SELECT f.name AS ForeignKey, 
		OBJECT_NAME(f.parent_object_id) AS TableName, 
		COL_NAME(fc.parent_object_id, fc.parent_column_id) AS ColumnName, 
		OBJECT_NAME (f.referenced_object_id) AS ReferenceTableName, 
		COL_NAME(fc.referenced_object_id, fc.referenced_column_id) AS ReferenceColumnName 
		FROM sys.foreign_keys AS f
		INNER JOIN sys.foreign_key_columns AS fc 
		ON f.OBJECT_ID = fc.constraint_object_id
		where OBJECT_NAME(f.parent_object_id) = 'TBPROG_M'
	END
	if @subcase = 2 BEGIN -- check constrain from sys.sysconstraints
		SELECT object_name(constid)  FROM sys.sysconstraints WHERE id = OBJECT_ID('TBPROG_M');
		select * from sys.sysconstraints
	END
	if @subcase = 3 BEGIN -- check from TABLE_CONSTRAINTS
		select * from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where TABLE_NAME = 'TBPROG_M' --查詢 constraint information
	END
	if @subcase = 4 BEGIN -- find primary key for a specified table
		SELECT KU.table_name as tablename,column_name as primarykeycolumn
		FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS TC
		INNER JOIN
		INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KU
		ON TC.CONSTRAINT_TYPE = 'PRIMARY KEY' AND
		TC.CONSTRAINT_NAME = KU.CONSTRAINT_NAME
		and ku.table_name='TBPROG_M'
		ORDER BY KU.TABLE_NAME, KU.ORDINAL_POSITION;

		select * from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where table_name = 'TBPROG_M'
		select * from INFORMATION_SCHEMA.KEY_COLUMN_USAGE where table_name = 'TBPROG_M'
	END
END

else if @case = 11 BEGIN -- INFORMATION_SCHEMA
	if @subcase = 1 begin
		select * from INFORMATION_SCHEMA.CHECK_CONSTRAINTS
		select * from INFORMATION_SCHEMA.COLUMN_DOMAIN_USAGE
		select * from INFORMATION_SCHEMA.COLUMN_PRIVILEGES
		
		select * from INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE
		select * from INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE
		select * from INFORMATION_SCHEMA.DOMAIN_CONSTRAINTS
		select * from INFORMATION_SCHEMA.DOMAINS
		select * from INFORMATION_SCHEMA.KEY_COLUMN_USAGE order by CONSTRAINT_NAME
		select * from INFORMATION_SCHEMA.PARAMETERS
		select * from INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS
		select * from INFORMATION_SCHEMA.ROUTINE_COLUMNS
		select * from INFORMATION_SCHEMA.ROUTINES

	end
	else if @subcase = 2 begin -- View information
		select * from INFORMATION_SCHEMA.VIEWS order by TABLE_NAME
		select TABLE_NAME 'TABLE NAME' from INFORMATION_SCHEMA.VIEWS order by TABLE_NAME
		select * from INFORMATION_SCHEMA.VIEW_COLUMN_USAGE order by VIEW_NAME
		-- 查詢 View 參考哪些 table
		select VIEW_NAME 'VIEW NAME', TABLE_NAME 'TABLE NAME' from INFORMATION_SCHEMA.VIEW_TABLE_USAGE order by VIEW_NAME
	end
	else if @subcase = 3 begin -- Table information
		select * from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where CONSTRAINT_TYPE = 'PRIMARY KEY'
		select * from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where CONSTRAINT_TYPE = 'FOREIGN KEY'
		select * from INFORMATION_SCHEMA.TABLE_PRIVILEGES
		select * from INFORMATION_SCHEMA.TABLES
	end
	else if @subcase = 4 begin
		select * from INFORMATION_SCHEMA.SCHEMATA
	end
	else if @subcase = 5 begin
		select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'TBZCHANNEL'
		select * from INFORMATION_SCHEMA.COLUMNS where COLUMN_NAME = 'FSSORT'
	end
	else if @subcase = 6 begin -- 列出table column type
		--USE AdventureWorks2008R2
		--GO
		SELECT o.name AS ObjectName, c.name AS ColumnName, TYPE_NAME(c.user_type_id) as DataType
		FROM sys.objects o JOIN sys.columns c
		ON o.object_id = c.object_id
		WHERE o.name = 'Department'
		and o.Schema_ID = SCHEMA_ID('HumanResources')

		select * from sys.objects where name = 'Department' order by object_id  -- 757577737
		select * from sys.columns where object_id = '757577737' order by object_id
	end
END -- case 5
if @case = 6 BEGIN -- DMO dynamic management objects
	if @subcase = 1
		select login_name, COUNT(session_id) as NumberSessions FROM sys.dm_exec_sessions GROUP BY login_name
END


else if @case = 101 BEGIN
	if @case = 1 --??
		select * from master.dbo.spt_values
	
END

GO --END PROC TEST_CASE

----------------------------------------------------------------------------------------------------
DECLARE @case integer, @subcase integer

set @case = 1 set @subcase = 1
EXEC TEST_CASE @case, @subcase
GO

if OBJECT_ID(N'dbo.TEST_CASE', N'P') is not NULL
	DROP PROC TEST_CASE
GO
