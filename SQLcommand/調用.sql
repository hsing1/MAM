
USE MAM
GO

declare @case int

set @case = 1

if @case = 1 BEGIN --查詢調用明細需製作主管審核的項目
	declare @BOOKING_NO varchar(20), @FSUSER_ID varchar(10)
	set @BOOKING_NO = '201408280098'
	set @FSUSER_ID = '51204'
	exec SP_Q_TBBOOKING_DETAIL_CHECK_BY_FILE_SUPERVISOR @BOOKING_NO, @FSUSER_ID
END

else if @case = 101 BEGIN
SELECT FNUpperDept_ID FROM TBUSERS WHERE FSUSER_ID = '51204'
UNION
Select FNDEP_ID from TBUSER_DEP
		Where FNPARENT_DEP_ID IN
		(SELECT FNUpperDept_ID FROM TBUSERS WHERE FSUSER_ID = '51204')
UNION
Select FNDEP_ID from TBUSER_DEP
  Where FNPARENT_DEP_ID IN 
	(Select FNDEP_ID from TBUSER_DEP
		Where FNPARENT_DEP_ID IN
		(SELECT FNUpperDept_ID FROM TBUSERS WHERE FSUSER_ID = '51204')
		)		
UNION
Select FNDEP_ID from TBUSER_DEP
  Where FNDEP_ID IN 
  (Select FNDEP_ID from TBUSER_DEP
  Where FNPARENT_DEP_ID IN 
	(Select FNDEP_ID from TBUSER_DEP
		Where FNPARENT_DEP_ID IN
		(SELECT FNUpperDept_ID FROM TBUSERS WHERE FSUSER_ID = '51204')
		))
END

else BEGIN
	select * from TBUSER_DEP
	select * from TBUSERS where FSUSER_ID = '51204'
	select * from PTSPROG.PTSProgram.dbo.TBPROGMEN 
	select * from PTSPROG.PTSProgram.dbo.TBSTAFF

	select * from TBUSERS where FSUSER_ID='71261'

	select * from TBBOOKING_MASTER where FSBOOKING_NO='201409150009  '
	select * from TBBOOKING_DETAIL where FSBOOKING_NO='201409150009  '
END

GO