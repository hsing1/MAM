

declare @case int, @subcase int

set @case = 1
set @subcase = 1

if @case = 1 begin -- 查詢短帶相關資訊
	if @subcase = 1 begin -- 查詢藝起看公視短帶相關資訊
		select * from TBPGM_PROMO where FSPROMO_NAME like '%藝起看公視%'
		select * from TBPGM_PROMO_EFFECTIVE_ZONE where FSPROMO_ID = '20151200549'
		select * from TBPGM_PROMO p left join TBPGM_PROMO_EFFECTIVE_ZONE e on p.FSPROMO_ID = e.FSPROMO_ID where p.FSPROMO_NAME like '%藝起看公視%' and p.FSDEL = 'N'

		select p.FSPROMO_ID '短帶編號', p.FSPROMO_NAME '短帶名稱', 
	    ISNULL(p.FDEXPIRE_DATE, '') '短帶到期日', ISNULL(p.FSEXPIRE_DATE_ACTION, 'N') '到期是否刪除', ISNULL(e.FNPROMO_BOOKING_NO, NULL) '託播單編號', 
		ISNULL(e.FNNO, NULL) '託播序號', ISNULL(e.FDEND_DATE, NULL) '託播有效期限', z.FSCHANNEL_NAME '託播頻道'
		from TBPGM_PROMO p left join TBPGM_PROMO_EFFECTIVE_ZONE e on p.FSPROMO_ID = e.FSPROMO_ID left join TBZCHANNEL z on e.FSCHANNEL_ID = z.FSCHANNEL_ID
		where p.FSPROMO_NAME like '%藝起看公視%' and p.FSDEL = 'N' order by p.FSPROMO_ID, e.FNPROMO_BOOKING_NO, e.FNNO, e.FDEND_DATE
	end
	else if @subcase = 2 begin -- 查詢藝起看公視短帶轉檔單資訊
		select * from TBBROADCAST where FSID = '20151200549'
		-- 未填轉檔單
		select p.FSPROMO_ID, p.FSPROMO_NAME, t.FSBRO_ID from TBPGM_PROMO p left join TBBROADCAST t on p.FSPROMO_ID = t.FSID and p.FSDEL = 'N'
		select p.FSPROMO_ID, p.FSPROMO_NAME, t.FSBRO_ID from TBPGM_PROMO p left join TBBROADCAST t on p.FSPROMO_ID = t.FSID where t.FSBRO_ID IS NULL and FSPROMO_NAME like '%藝起看公視%' and p.FSDEL = 'N'
	end

end --case 1



-------------------------------------------------------------
SELECT * FROM MAM.dbo.TBZCHANNEL
WHERE MAM. dbo.TBZCHANNEL .FSCHANNEL_NAME = '公共電視台'
ORDER BY   FSSORT 



USE [MAM]
GO
/****** Object:  StoredProcedure [dbo].[SP_Q_TBZCHANNEL_ALL]    Script Date: 2014/10/8 上午 10:15:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER Proc [dbo].[SP_Q_TBZCHANNEL_ALL]

As
SELECT FSCHANNEL_ID as ID,FSCHANNEL_NAME as NAME,FSCHANNEL_TYPE as TYPE,FSSHOWNAME FROM MAM.dbo.TBZCHANNEL
where FSCHANNEL_NAME<>''
ORDER BY  FSSORT



USE [MAM]
GO
/****** Object:  StoredProcedure [dbo].[SP_Q_TBZCHANNEL_ALL]    Script Date: 2014/10/8 上午 10:15:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER Proc [dbo].[SP_Q_TBZCHANNEL_ALL]

As
SELECT FSCHANNEL_ID as ID,FSCHANNEL_NAME as NAME,FSCHANNEL_TYPE as TYPE,FSSHOWNAME FROM MAM.dbo.TBZCHANNEL
where FSCHANNEL_NAME<>''
ORDER BY  FSSORT 

select * from TBZCHANNEL

select * from TBPGM_PROMO where FSMAIN_CHANNEL_ID=''
select * from TBPGM_PROMO where FSPROMO_NAME like '%刪海經'
select * from TBPGM_PROMO where FSPROMO_NAME = '爸媽囧很大1104'
select * from TBPGM_PROMO where FSPROMO_ID = '20150900420'
--查詢短帶更新資訊
select u.FSUSER_ChtName '更新者' , t.* from TBPGM_PROMO t, TBUSERS u where FSPROMO_ID = '20151200218' and t.FSUPDATED_BY = u.FSUSER_ID 

select * from TBPGM_PROMO where FSPROMO_NAME = '2013新春賀年ID'
select * from TBPGM_PROMO_BOOKING where FSPROMO_ID='20130200054'
select * from TBPGM_PROMO_BOOKING where FSPROMO_ID='20150900420'

select * from TBPGM_PROMO_SCHEDULED

-- 顯示託播單的有效時區
select * from TBPGM_PROMO_EFFECTIVE_ZONE where FNPROMO_BOOKING_NO='6369'

-- 顯示託播單未設頻道
select A.FSPROMO_ID, A.FSPROMO_NAME, B.FNPROMO_BOOKING_NO, C.FSCHANNEL_ID, C.FNNO from TBPGM_PROMO A, TBPGM_PROMO_BOOKING B, TBPGM_PROMO_EFFECTIVE_ZONE C where A.FSPROMO_ID = B.FSPROMO_ID and C.FSPROMO_ID=B.FSPROMO_ID and C.FSCHANNEL_ID=''
-- 二台託播短帶
select A.FSPROMO_ID, A.FSPROMO_NAME, B.FNPROMO_BOOKING_NO, C.FSCHANNEL_ID, C.FNNO from TBPGM_PROMO A, TBPGM_PROMO_BOOKING B, TBPGM_PROMO_EFFECTIVE_ZONE C where A.FSPROMO_ID = B.FSPROMO_ID and C.FSPROMO_ID=B.FSPROMO_ID and C.FSCHANNEL_ID='02'
-- 主頻託播短帶
select A.FSPROMO_ID, A.FSPROMO_NAME, B.FNPROMO_BOOKING_NO, C.FSCHANNEL_ID, C.FNNO from TBPGM_PROMO A, TBPGM_PROMO_BOOKING B, TBPGM_PROMO_EFFECTIVE_ZONE C where A.FSPROMO_ID = B.FSPROMO_ID and C.FSPROMO_ID=B.FSPROMO_ID and C.FSCHANNEL_ID='01'

-- 查詢有公益託播編號(FSWELFARE)的短帶
select * from TBPGM_PROMO where FSWELFARE <> ''
select count(FSWELFARE) from TBPGM_PROMO where FSWELFARE <> ''


