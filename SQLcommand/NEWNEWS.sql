declare @case int
use PTSNewsWarehousing

if @case = 201805111046 begin
	select * from ArchiveJob
	select * from INFORMATION_SCHEMA.SCHEMATA
	select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'ArchiveJob'
	select * from INFORMATION_SCHEMA.TABLES
end

if @case = 201805111123 begin
		declare @tableName varchar(100) = ''
		declare @columnTable TABLE (tableName varchar(200), col_name varchar(200), type varchar(25)) -- column name
		declare @foreignTable TABLE (FKname varchar(300), tableName varchar(200), columnName varchar(200), referenceTable varchar(200), referenceColumn varchar(200))
		declare @primaryTable TABLE (tbName varchar(200), columnName varchar(200))
		declare @tableInfo TABLE(tableName varchar(200), columnName varchar(300), type varchar(20), primaryKey varchar(1), foreignKeyConstraint varchar(300), referencedTable varchar(200), referencedColumn varchar(200))

		--List column name
		INSERT into @columnTable select TABLE_NAME, COLUMN_NAME, DATA_TYPE from INFORMATION_SCHEMA.COLUMNS order by TABLE_NAME
		--select COLUMN_NAME, DATA_TYPE from INFORMATION_SCHEMA.COLUMNS where TABLE_CATALOG = 'MAM' and TABLE_NAME = @tableName

		-- List foreign key
		INSERT into @foreignTable
		SELECT f.name AS ForeignKey, 
		OBJECT_NAME(f.parent_object_id) AS TableName, 
		COL_NAME(fc.parent_object_id, fc.parent_column_id) AS ColumnName, 
		OBJECT_NAME (f.referenced_object_id) AS ReferenceTableName, 
		COL_NAME(fc.referenced_object_id, fc.referenced_column_id) AS ReferenceColumnName 
		FROM sys.foreign_keys AS f
		INNER JOIN sys.foreign_key_columns AS fc 
		ON f.OBJECT_ID = fc.constraint_object_id
		order by tableName

		--select * from @foreignTable

	
		-- List primary key
		INSERT into @primaryTable
		SELECT KU.table_name as tablename,column_name as primarykeycolumn
		FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS TC
		INNER JOIN
		INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KU
		ON TC.CONSTRAINT_TYPE = 'PRIMARY KEY' AND
		TC.CONSTRAINT_NAME = KU.CONSTRAINT_NAME
		ORDER BY KU.TABLE_NAME, KU.ORDINAL_POSITION;

		--select * from @primaryTable

		-- create result table
		INSERT into @tableInfo
		select ct.tableName as 'TABLE NAME', ct.col_name as 'COLUMN NAME',  ct.type as 'TYPE',
		(CASE ISNULL(pt.columnName, '')
			WHEN '' THEN ''
			ELSE 'Y'
		END) as 'PRIMARY KEY', 	
		ISNULL(ft.FKname, '') as 'FOREIGN KEY CONSTRAINT', ISNULL(ft.referenceTable, '') as 'REFERENCED TABLE', ISNULL(ft.referenceColumn, '') as 'REFERENCED COLUMN'
		from (@columnTable ct left outer join @foreignTable ft on ct.col_name = ft.columnName and ct.tableName = ft.tableName) left outer join @primaryTable pt on ct.col_name = pt.columnName and ct.tableName = pt.tbName

		select tableName as 'TABLE NAME', columnName as 'COLUMN NAME', type as 'TYPE', primaryKey as 'PRIMARY KEY', foreignKeyConstraint as 'FOREIGN KEY CONSTRAINT', referencedTable as 'REFERENCED TABLE', referencedColumn 'REFERENCED COLUMN' from @tableInfo


end

if @case = 201805251814 begin
	select * from BiggerLimit
	select * from BorrowList --\\10.13.200.15\ShareFolder\Borrow\50991\20161207
	select * from ConvertCode
	select * from ExceptionLog 
	--System.IO.FileNotFoundException: 找不到檔案 '\\10.13.200.15\PreConvert\84de3b8e6a2043a793e5c2229a514576.mxf'。  檔案名稱: '\\10.13.200.15\PreConvert\84de3b8e6a2043a793e5c2229a514576.mxf'     於 System.IO.__Error.WinIOError(Int32 errorCode, String maybeFullPath)     於 System.IO.File.InternalMove(String sourceFileName, String destFileName, Boolean checkHost)     於 NewsUploadApp.MainForm.MoveToWarehouse() 於 D:\Projects46\PTSNewsWarehousing\NewsUploadApp\MainForm.cs: 行 521
	select * from FileSrNo
	select * from iNewsBackup --no data
	select * from NewsMeta --StoreID = 0d25b496:009b992a:58326c94
	select * from NewsRaw --no data
	select * from PreArchive
	select * from ScheduleLog
	select * from SysReference
	/*
	150 = \\10.13.200.15\ShareFolder\Borrow
	160 = \\\\10.1.164.34\PTS_Import_New\Archive_new
	*/
	select * from UploadTemp
	select * from VideoMeta --ArchivePath = \\10.13.200.15\Archive\PTS\2016\11\22
	--\\10.13.200.98\mamspace\News\PTS\2017\12\06
	select * from VideoReporter
	select * from VideoTracks
	select * from vwBorrowList
end


if @case = 201805280924 begin
	select * from VideoMeta
/*
0ada7d49dd3a487f897fa04c6ae71212
*/
	select * from VideoMeta where ArchiveName = '0ada7d49dd3a487f897fa04c6ae71212' --no record
	select * from VideoMeta where ArchivePath like '%Macroview%' --4428
/*
測試調用
/ONAIR/1205MN日惹大學與成功大學學術合作_00000HG6, \\10.13.200.15\Archive\Macroview\2017\01\02, MN20170102000014.mxf /ONAIR/1205MN日惹大學與成功大學學術合作_00000HG6, VideoId=4738
*/

	--\\10.13.200.15\Archive\Macroview\2017\01\02 -> \\10.13.200.97\mam_data\200_15\Macroview\2017\01\02
	select * from VideoMeta where ArchiveNAME = 'MN20170102000014.mxf'	
	select ArchivePath, ArchiveName into #tmp_VideoMeta from VideoMeta where ArchivePath like '%Macroview%'
    select * from #tmp_VideoMeta

	--TEST
	update #tmp_VideoMeta set ArchivePath = REPLACE(ArchivePath, '\\10.13.200.15\Archive',  '\\10.13.200.97\mam_data\200_15') where ArchiveName = 'MN20170102000014.mxf'
	select * from #tmp_VideoMeta where ArchiveName = 'MN20170102000014.mxf' --\\10.13.200.97\mam_data\200_15\Macroview\2017\01\02
	update #tmp_VideoMeta set ArchivePath = REPLACE(ArchivePath,  '\\10.13.200.97\mam_data\200_15', '\\10.13.200.15\Archive') where ArchiveName = 'MN20170102000014.mxf'
	select * from #tmp_VideoMeta where ArchiveName = 'MN20170102000014.mxf' --\\10.13.200.15\Archive\Macroview\2017\01\02

	update VideoMeta set ArchivePath = REPLACE(ArchivePath, '\\10.13.200.15\Archive',  '\\10.13.200.97\mam_data\200_15') where ArchiveName = 'MN20170102000014.mxf'

	select * from BorrowList  where UserID = '51204' --BorrowStatus = 3 (調用完成), 99(發生錯誤) VideoID = 4738

	/*
	\\10.13.200.15\Archive\Macroview\2016\12\08\-- no ffile
	\\10.13.200.97\mam_data\200_15\Macroview\2017\12\17\MN20171217000001.mxf
	*/
	use PTSNewsWarehousing
	select * from VideoMeta where ArchiveName = 'MN20171217000001.mxf' --\\10.13.200.15\Archive\Macroview\2017\12\17 --> \\10.13.200.97\mam_data\200_15\Macroview\2017\12\17\MN20171217000001.mxf
	select * from ExceptionLog where CreateDate > '2018-05-23' order by CreateDate
end

if @case = 201805281048 begin
	--use master
	--GRANT ADMINISTER BULK OPERATIONS TO NewsWarehousing --new use sa


	if OBJECT_ID(N'tempdb.dbo.#tempFILE_NO', N'U') IS NOT NULL
		DROP TABLE #tempFILE_NO

	CREATE TABLE  #tempVideo (
		FSFILE_NO varchar(30)
	)
	
	INSERT INTO #tempVideo(FSFILE_NO) VALUES ('MN20171217000001.mxf')

	select * from #tempVideo

	--BULK INSERT #tempImport from '\\10.13.200.5\OtherVideo\temp\fileno.txt' WITH
	BULK INSERT #tempFILE_NO from 'E:\TEMP\out.txt' WITH
	(
		ROWTERMINATOR = '\n'
	);

	select * from #tempFILE_NO
end

if @case = 201805291928 begin  --check MoveToConvert()
		select * from ExceptionLog where CreateDate > '2018-05-29' order by CreateDate
/*
0120翻銅藝術家拍帶1.MXF
System.IO.FileNotFoundException: 找不到檔案 '\\10.13.200.15\PreConvert\84de3b8e6a2043a793e5c2229a514576.mxf'。  
檔案名稱: '\\10.13.200.15\PreConvert\84de3b8e6a2043a793e5c2229a514576.mxf'     
於 System.IO.__Error.WinIOError(Int32 errorCode, String maybeFullPath)     
於 System.IO.File.InternalMove(String sourceFileName, String destFileName, Boolean checkHost)     
於 NewsUploadApp.MainForm.MoveToWarehouse() 於 D:\Projects46\PTSNewsWarehousing\NewsUploadApp\MainForm.cs: 行 521
*/
		select * from UploadTemp where FileStatus = 1 and ConverterStatus = 1 order by CreateDate
		select * from UploadTemp where FileStatus = 1 order by CreateDate
		select * from UploadTemp where VideoName like '%翻銅藝術家拍帶%'
		select * from UploadTemp where JobGuid is not NULL

		select * from UploadTemp where VideoName like '%中研院雲端生物%' --1,2


		--手動上傳 TETS_20180529.MXF 測試
		select * from UploadTemp where CreateDate >= '2018-05-29' --TETS_20180529.MXF File FileStatus = 0, ConterterStatus = 0
		--確認上傳
		--Archivename = ed889cbd241045fbb6cdbc6509082fec.mxf, ConvertFolder = \\10.13.200.56\source_mxf
		select * from UploadTemp where ArchiveName = 'ed889cbd241045fbb6cdbc6509082fec.mxf' --FileStatus = 1, ConverterStat = 2, ConfirmMark = 1
		select * from PreArchive where MP4Name = 'ed889cbd241045fbb6cdbc6509082fec' --no record
		--\\10.13.200.15\PreConvert\ed889cbd241045fbb6cdbc6509082fec.mxf has file
		--10.13.200.56 E:\source_mxf\ed889cbd241045fbb6cdbc6509082fec.mxf

		select * from UploadTemp where UploadID = 50768
		select * from VideoMeta where VideoID = 50768 --/ONAIR/1200泰新未來黨_00003BUQ
		select * from UploadTemp where VideoName like '%泰新未來黨%' --no record
		--無法審核

		select * from PreArchive where CreateDate > '2018-05-20' order by CreateDate --1900印尼暴動周年 ArchiveStatus 5
		select * from PreArchive where Slogan = '1930黎女導作品' --MP4Name = a3d18c8b953c42459dd64bf24dd3acae ArchiveStatus = 3
		select * from VideoMeta where MP4Name = 'a3d18c8b953c42459dd64bf24dd3acae' --no data
		select * from PreArchive where ArchiveStatus = 3 order by CreateDate --Since 2018-05-29 16:35:18.953
		select * from PreArchive where MXFName like '%翻銅藝術家拍帶%'

		select * from VideoMeta where CreateDate >= '2018-05-29' order by CreateDate --FileStatus = 0
		select * from VideoMeta where CreateDate >= '2018-05-24' order by CreateDate --FileStatus = 0 --2018-05-25 10:39:27.687 FileStatus = 2 , the last one
end

if @case = 201805301121-201805281048 begin
	--use master
	--GRANT ADMINISTER BULK OPERATIONS TO NewsWarehousing --new use sa

	if OBJECT_ID(N'tempdb.dbo.#tempFILE_NO', N'U') IS NOT NULL
		DROP TABLE #tempFILE_NO

	CREATE TABLE  #tempFILE_NO (
		FSFILE_NO varchar(30)
	)
	
	--INSERT INTO #tempVideo(FSFILE_NO) VALUES ('MN20171217000001.mxf')

	--select * from #tempVideo

	--BULK INSERT #tempImport from '\\10.13.200.5\OtherVideo\temp\fileno.txt' WITH
	BULK INSERT #tempFILE_NO from 'E:\TEMP\out_macroview_2016.txt' WITH
	(
		ROWTERMINATOR = '\n'
	);

	update VideoMeta set ArchivePath = REPLACE(ArchivePath, '\\10.13.200.15\Archive',  '\\10.13.200.97\mam_data\200_15') where ArchiveName in (select FSFILE_NO from #tempFILE_NO

	--select * from #tempFILE_NO --289
	select * from #tempFILE_NO A left join VideoMeta B on A.FSFILE_NO = B.ArchiveName
	select * from VideoMeta where ArchiveName in (select FSFILE_NO from #tempFILE_NO)
	select * from VideoMeta where ArchiveName = '1de041a4b0da4620adc314f5ed28f635.mxf' --/ONAIR/1222MF聽見希望_00002ICE
end

if @case = 201806041416 begin
use PTSNewsWarehousing
	select * from VideoMeta  where ArchiveName = 'HN20161130000001.mxf'
	select * from VideoMeta where ArchivePath like '\\10.13.200.97\mam_data\NewsMamArchive%' --3669
	select * from VideoMeta where ArchivePath like '\\10.13.200.97\mam_data\200_15%' --3558
	select * from VideoMeta where ApproveDate > '2018-06-01' order by CreateDate --818
	select COUNT(ArchiveName)  ,SUM(FileSize)/1024/1024/1024 from VideoMeta where ApproveDate > '2018-06-01' --818, 2172GB
end

if @case = 201806121128 begin --陳信隆
	select * from vwBorrowList where UserID = '50271' order by BorrowID desc
	/*
	--borrowstatus 99
	--【公共電視】1900禁塑吸管配套 \\10.13.200.15\Archive\PTS\2018\03\12\PN20180312000026.mxf
	--【公共電視】1900不鏽鋼吸管刺傷 \\10.13.200.15\Archive\PTS\2018\02\23\PN20180223000036.mxf
	-- 【公共電視】1900麻竹做吸管 \\10.13.200.15\Archive\PTS\2018\02\27\PN20180227000037.mxf
	--實際路徑
	\\10.13.200.97\mam_data\NewsMamArchive\PTS\2018\02\27
	--\\10.13.200.15\ShareFolder\Borrow\50271\20180612
	*/
	select * from BorrowList where UserID = '50271'
end

if @case = 201806141107 begin --黃守明
	select * from vwBorrowList where UserID = '09404' order by BorrowID desc
	/*
	\\10.13.200.97\mam_data\NewsMamArchive\PTS\2018\06\11\PN20180611000008.mxf
	\\10.13.200.97\mam_data\NewsMamArchive\PTS\2018\06\11\PN20180611000007.mxf
	--borrowstatus 99

	--實際路徑
	\\10.13.200.15\Archive\PTS\2018\06\11

	*/
	select * from BorrowList where UserID = '50271'
	--\\10.13.200.15\Archive\PTS\2018\06\11 metada 是6/11 建的但是檔案是 6/12 進去
	select * from VideoMeta where ArchiveName = 'PN20180611000008.mxf' --ModifyDate = 2018-06-12 11:05:03.000 Approved = 018-06-12 11:05:06.000
	select * from VideoMeta where ArchiveName = 'PN20180611000005.mxf' --create date 2018-06-11 19:09:33.927 2018-06-12 11:06:05.000  
	select * from VideoMeta where ArchiveName = 'PN20180611000006.mxf'
	select * from VideoMeta where ArchiveName = 'PN20180611000007.mxf'

	select * from VideoMeta where ArchiveName = 'PN20180613000002.mxf' --Approved 2018-06-13 10:11:00.000

	--research
	select top 10000 * from VideoMeta order by CreateDate desc, ApproveDate desc
	select * from VideoMeta where ArchiveName = 'cd323d6487314ff3854322149ba836cf.mxf'
    --VideoId = 54270 originalfile = null , originalLow = null ArchivePath = \\10.13.200.15\Archive\PTS, ApproveDate = null, PreArchiveId = null
	select * from VideoMeta where ArchiveName = '0ac591be33e44cbaa74990be15ff712f.mxf'
	--VideoId = 54267 orignalfile = /ONAIR/美朝磋商ADO_00003FV9, originalLow = \\PROXYSVR\proxy\02090a84f57e45cf8db51942db45bba7\proxy.mp4, Archivepath = \\10.13.200.15\Archive\PTS

	select top 1000 ArchivePath, ArchiveName, CreateDate, ApproveDate from VideoMeta order by CreateDate desc

end

if @case = 201806141347 begin
	select count(VideoID) from VideoMeta --54235
	select top 1000 * from VideoMeta order by CreateDate desc
	select ArchivePath, ArchiveName from VideoMeta where AlreadyDelete = 0 --49265
	select ArchivePath + '\' + ArchiveName from VideoMeta where AlreadyDelete = 0 --49265

	select * from SysReference
	select distinct(DeleteMark) from VideoMeta --0, 1
	select distinct(AlreadyDelete) from VideoMeta --0, 1
	select distinct(ArchivePosition) from VideoMeta --1,2,3

	select * from VideoMeta where ArchivePosition = 1 --52856
	select * from VideoMeta where ArchivePosition = 2 --397 Hakka, \\10.13.200.98
	select * from VideoMeta where ArchivePosition = 3 --984 \\10.13.200.98

	--ArchivePath = NULL and ArchiveName = NULL
	select * from VideoMeta where ArchivePath is NULL or ArchiveName is NULL --59
end


if @case = 201806150907 begin
	select * from VideoMeta where ArchiveName = 'PN20180611000005.mxf'
	--select ArchivePath + '\' + ArchiveName as Path, * from VideoMeta where Path = '\\10.13.200.97\mam_data\NewsMamArchive\PTS\2018\06\11\PN20180611000005.mxf'
	select CreateDate, ApproveDate, * from VideoMeta where ArchivePath + '\' + ArchiveName = '\\10.13.200.97\mam_data\NewsMamArchive\PTS\2018\06\11\PN20180611000005.mxf'
	select CONCAT('A', 'B')  --SQL Server 2012
	SELECT CONCAT ( 'Happy ', 'Birthday ', 11, '/', '25' ) AS Result;

	select CreateDate, ApproveDate, ArchivePath, * from VideoMeta
	select CreateDate, ApproveDate, * from VideoMeta a where CreateDate between '2018-06-11' and '2018-06-12' order by a.ApproveDate
	select CreateDate, ApproveDate, * from VideoMeta a where CreateDate between '2018-06-14' and '2018-06-15' order by a.ApproveDate
	--6/15 審核通過檔案尚未入庫 ==> 搬檔時間晚於審核時間
	select * from VideoMeta where VideoID = '54377' --\\10.13.200.15\Archive\PTS\6029afc540c34c6196783d3812c22d7b.mxf ApproveDate = 2018-06-15 11:00:20.000 ApproveUser = 70079
	--\\10.13.200.15\Archive\PTS\2018\06\04 尚未有檔案，需檢查檔案最後搬入時間
	--\\10.13.200.15\Archive\MP4\6029afc540c34c6196783d3812c22d7b.mp4 --> exists

	--6/14 14:29 審核通過，檔案是 6/14 02:11 搬檔入庫 ==> 搬檔時間早於審核時間
	select * from VideoMeta where VideoID = '54259' --\\10.13.200.15\Archive\PTS\2018\06\13\PN20180613000057.mxf ApproveDate = 2018-06-14 14:29:09.000 ApproveUser = 70079
	--\\10.13.200.15\Archive\MP4\c7965321ea7a4ba481b1eb5d246b5299.mp4

	select * from VideoMeta where ApproveDate is NULL order by CreateDate desc --6970  PTS=1457, Hakka=755
	--9002, create at 2016-11-22 16:42:00.543 /ONAIR/1900推食農教育_000008NZ d511e807e2c0496180a4cc72c526f726.mxf file not found

	select * from VideoMeta where VideoID = '22972'
	select * from UploadTemp where UploadID = '22972' --20180613最夜新聞(完成帶-第158集).mxf 影片超時

	select * from UploadTemp where UploadID = 21215 --TETS_20180529.MXF Archive name = ed889cbd241045fbb6cdbc6509082fec.mxf
	select * from VideoMeta where createDate between '2018-05-29' and '2018-05-30'
end

if @case = 201806201036 begin --Metadata
	select * from SysReference --200, Macroview, 宏觀. 501, A 公視, E 宏觀
	select * from VideoMeta where Channel = 'E' order by CreateDate --3205  since 2016-11-22，有兩筆2018入庫
end

if @case = 201806251743 begin
/*
幸一：
新片庫請加開權限-----謝宗蕙
手動上傳----入庫大型檔案60分鐘
謝謝

*/
	select * from BiggerLimit
	select * from Users where EmpName = '謝宗蕙' --50839
	update Users set IsUploadBigger60 = 1 where EmpID = '50839'
	select * from Users where EmpID = '50839'
	select * from Users where EmpId = '51204'
	update Users set IsUploadBigger60 = 1 where EmpID = '51204'
end

if @case = 201807111618 begin --news70079 手動上傳確認不成功
	select * from VideoMeta where ModifyDate >= '2018-07-11' order by CreateDate
	select * from UploadTemp where CreateDate >= '2018-07-11' order by CreateDate
	--0711北市街景(柏諭).MXF UploadId = 24440 24441 24442 24445
	--24440, b8a9aec0d9d54521bb2a90e3450fc103.mxf filestatus = 1 converterstatus = 3  --> 2, 4
end

if @case = 201807120626 begin
	select top 1000 * from VideoMeta order by CreateDate --2016-11-22 09:31:18.493
end

if @case = 201806201036 begin --Metadata
	select * from SysReference --200, Macroview, 宏觀. 501, A 公視, E 宏觀
	select * from VideoMeta where Channel = 'E' order by CreateDate desc --3205  since 2016-11-22，有兩筆2018入庫
end

if @case = 201807121130 begin --調用問題處理
	select * from BorrowList where BorrowID = 52473 --VideoId = 47841
	select top 100 * from vwBorrowList order by BorrowID desc
	select * from vwBorrowList where BorrowId = 52473
	select * from VideoMeta where VideoID = 47841 --\\10.13.200.97\mam_data\200_15\HakkaMgz\2018\04\25
end


/*******************************************
 *
 *	       MYReference
 *
 *******************************************/

 if @case = 201805251814 begin
	select * from BiggerLimit
	select * from BorrowList --\\10.13.200.15\ShareFolder\Borrow\50991\20161207
	select * from ConvertCode
	select * from ExceptionLog 
	--System.IO.FileNotFoundException: 找不到檔案 '\\10.13.200.15\PreConvert\84de3b8e6a2043a793e5c2229a514576.mxf'。  檔案名稱: '\\10.13.200.15\PreConvert\84de3b8e6a2043a793e5c2229a514576.mxf'     於 System.IO.__Error.WinIOError(Int32 errorCode, String maybeFullPath)     於 System.IO.File.InternalMove(String sourceFileName, String destFileName, Boolean checkHost)     於 NewsUploadApp.MainForm.MoveToWarehouse() 於 D:\Projects46\PTSNewsWarehousing\NewsUploadApp\MainForm.cs: 行 521
	select * from FileSrNo
	select * from iNewsBackup --no data
	select * from NewsMeta --StoreID = 0d25b496:009b992a:58326c94
	select * from NewsRaw --no data
	select * from PreArchive
	select * from ScheduleLog
	select * from SysReference
	/*
	150 = \\10.13.200.15\ShareFolder\Borrow
	160 = \\\\10.1.164.34\PTS_Import_New\Archive_new toGV
	*/
	select * from UploadTemp
	select * from VideoMeta --ArchivePath = \\10.13.200.15\Archive\PTS\2016\11\22
	--\\10.13.200.98\mamspace\News\PTS\2017\12\06
	select * from VideoReporter
	select * from VideoTracks
	select * from vwBorrowList
end

 /*
	新聞片庫檔案轉移
 */
if @case = 201805312100 begin
--W:\DevOpsUtilsTest\Python\migrate_news_archive_file.py

		if OBJECT_ID(N'tempdb.dbo.#tempFILE_NO', N'U') IS NOT NULL
		DROP TABLE #tempFILE_NO

	CREATE TABLE  #tempFILE_NO (
		FSFILE_NO varchar(30)
	)

	declare @INPUT_FILE varchar(200)
	
	--INSERT INTO #tempVideo(FSFILE_NO) VALUES ('MN20171217000001.mxf')

	--select * from #tempVideo

	--BULK INSERT #tempImport from '\\10.13.200.5\OtherVideo\temp\fileno.txt' WITH
	--set @INPUT_FILE = 'E:\TEMP\out_macroview_2016.txt'
	--set @INPUT_FILE = 'E:\TEMP\out_macroview_2017.txt'
	--BULK INSERT #tempFILE_NO from 'E:\TEMP\out_macroview_2017.txt' WITH
	BULK INSERT #tempFILE_NO from 'E:\TEMP\out_hakkamgz.txt' WITH
	(
		ROWTERMINATOR = '\n'
	);

	select * from #tempFILE_NO A left join VideoMeta B on A.FSFILE_NO = B.ArchiveName --2016(289), 2017(2826) hakkamgz(442)

	--update VideoMeta set ArchivePath = REPLACE(ArchivePath, '\\10.13.200.15\Archive',  '\\10.13.200.97\mam_data\200_15') where ArchiveName in (select FSFILE_NO from #tempFILE_NO) --289, 2826, 442

	--/ONAIR/1121MO民進黨立法院美國觀選團抵達洛杉磯_00000D40  (2016)
	--/ONAIR/1214MN105年馬來西亞中學教育參訪團_00000HGE (2017)
	--/ONAIR/0505青草職能學苑_000036GS
end

if @case = 201805312100 begin
--W:\DevOpsUtilsTest\Python\migrate_news_archive_file.py

	if OBJECT_ID(N'tempdb.dbo.#tempFILE_PATH', N'U') IS NOT NULL
		DROP TABLE #tempFILE_PATH

	CREATE TABLE  #tempFILE_PATH (
		FSFILE_PATH varchar(300)
	)

	declare @INPUT_FILE varchar(200)
	
	--INSERT INTO #tempVideo(FSFILE_NO) VALUES ('MN20171217000001.mxf')

	--select * from #tempVideo

	--BULK INSERT #tempImport from '\\10.13.200.5\OtherVideo\temp\fileno.txt' WITH
	--set @INPUT_FILE = 'E:\TEMP\out_macroview_2016.txt'
	--set @INPUT_FILE = 'E:\TEMP\out_macroview_2017.txt'
	--BULK INSERT #tempFILE_NO from 'E:\TEMP\out_macroview_2017.txt' WITH
	BULK INSERT #tempFILE_PATH from 'E:\LIST_MIGRATE_TO_200_97\pts_2018_06_11.txt' WITH
	(
		ROWTERMINATOR = '\n'
	);

	select B.CreateDate, B.ApproveDate, A.* , B.* from #tempFILE_PATH A left join VideoMeta B on A.FSFILE_PATH = B.ArchivePath + '\' + B.ArchiveName order by B.ApproveDate --
	select B.CreateDate, B.ApproveDate, A.FSFILE_PATH, B.OriginalFile from #tempFILE_PATH A left join VideoMeta B on A.FSFILE_PATH = B.ArchivePath + '\' + B.ArchiveName order by B.ApproveDate --

	--update VideoMeta set ArchivePath = REPLACE(ArchivePath, '\\10.13.200.15\Archive',  '\\10.13.200.97\mam_data\200_15') where ArchiveName in (select FSFILE_NO from #tempFILE_NO) --289, 2826, 442

	--/ONAIR/1121MO民進黨立法院美國觀選團抵達洛杉磯_00000D40  (2016)
	--/ONAIR/1214MN105年馬來西亞中學教育參訪團_00000HGE (2017)
	--/ONAIR/0505青草職能學苑_000036GS
end