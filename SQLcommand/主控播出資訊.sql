
-- 根據 playoutID 轉入主控資訊表
CREATE PROC #SP_I_PLAYOUT2TBPGM_INSERT_TAPE
	@playoutID varchar(20)
AS

	declare @SQLstring nvarchar(3000), @paramDef nvarchar(200);
	declare @SQLstring_org nvarchar(3000);


	set @SQLstring = 'select playout002, programID, prgset, spec, smoking, alerts, alertsName, customName, customS, title, titleName, programSide, liveSide,
		timeSide, replay, video, stereo, logo, ch1, ch2, ch3, ch4 from [10.1.253.88].EF2KWeb.EZAdmin.playout as p
		where  playout002 = @playoutID'

		set @SQLstring_org = '
		select
		playout002 單號,
		programID 節目編碼,
		(select pgm.FSPGMNAME from TBPROG_M pgm where pgm.FSPROG_ID = programID collate Chinese_Taiwan_Stroke_CI_AS) 節目名稱,
		prgset 集數,
		(CASE 
			WHEN spec=1 THEN ''SD''
			WHEN spec=2 THEN ''HD''
			ELSE ''N/A''
		END) 規格,

		(CASE
			WHEN smoking=1 THEN ''要''
			WHEN smoking=2 THEN ''不要''
			ELSE ''N/A''
		END) 吸煙卡,

		(CASE
			WHEN alerts=1 THEN ''要''
			WHEN alerts=2 THEN ''不要''
			ELSE ''N/A''
		END) 警示卡,
		(CASE
			WHEN alertsName=0 THEN ''''
			WHEN alertsName=1 THEN ''制式版1''
			WHEN alertsName=2 THEN ''制式版2''
			WHEN alertsName=3 THEN ''制式版3''
			WHEN alertsName=4 THEN ''制式版4''
			WHEN alertsName=5 THEN ''制式版5''
			WHEN alertsName=6 THEN ''制式版6''
			WHEN alertsName=7 THEN ''客製版''
			ELSE ''N/A''
		END) 制式版,
		customName 警示卡名稱,
		customS 警示卡長度,

		(CASE
			WHEN title=1 THEN ''要''
			WHEN title=2 THEN ''不要''
			ELSE ''N/A''
		END) 檔名總片頭,

		titleName 檔名總片頭名稱,
		titleS 檔名總片頭長度,

		(CASE
			WHEN programSide=1 THEN ''要''
			WHEN programSide=2 THEN ''不要''
			ELSE ''N/A''
		END) 節目側標,

		(CASE
			WHEN liveSide=1 THEN ''要''
			WHEN liveSide=2 THEN ''不要''
			ELSE ''N/A''
		END) LIVE側標,

		(CASE
			WHEN timeSide=1 THEN ''要''
			WHEN timeSide=2 THEN ''不要''
			ELSE ''N/A''
		END) 時間側標,

		(CASE
			WHEN replay=1 THEN ''要''
			WHEN replay=2 THEN ''不要''
			ELSE ''N/A''
		END) 重播,

		(CASE
			WHEN video=1 THEN ''要''
			WHEN video=2 THEN ''不要''
			ELSE ''N/A''
		END) 錄影轉播,

		(CASE
			WHEN stereo=1 THEN ''要''
			WHEN stereo=2 THEN ''不要''
			ELSE ''N/A''
		END) 立體聲,

		(CASE 
			WHEN logo=0 THEN ''不要''
			WHEN logo=1 THEN ''右邊''
			ELSE ''N/A''
		END) 公視LOGO,

		ch1 雙語1,
		ch2 雙語2,
		ch3 雙語3,
		ch4 雙語4
		from [10.1.253.88].EF2KWeb.EZAdmin.playout as p
		where  playout002 = @playoutID'

		set @paramDef = '@playoutID varchar(20)'
		EXEC sp_executesql @SQLstring, @paramDef,  @playoutID = @playoutID

GO -- END SP_I_PLAYOUT2TBPGM_INSERT_TAPE

declare @playoutID varchar(20)
set @playoutID = '0000012757'

EXEC #SP_I_PLAYOUT2TBPGM_INSERT_TAPE @playoutID
DROP PROC #SP_I_PLAYOUT2TBPGM_INSERT_TAPE

GO

-- 查詢 playout 主控資訊 by playoutID or programID
CREATE PROC #SP_Q_PLAYOUT_BY_ID
	@playoutID varchar(20),
	@programID varchar(8),
	@episode integer,
	@spec integer
AS



/*
	declare @playoutID varchar(20)
	declare @programID varchar(8)
	declare @episode integer
	declare @spec integer

	set @playoutID = '0000012757'
	set @programID = '2011365'
	set @episode = 1
	set @spec = 1
*/
	declare @SQLstring nvarchar(3000), @paramDef nvarchar(100);

	--set @SQLString = 'select * from TBPROG_M where FSPROG_ID = @programID'
	--set @paramDef = '@programID varchar(8)'
	--select * from TBPROG_M where FSPROG_ID = @programID
	--EXEC sp_executesql @SQLstring, @paramDef, @programID

	set @SQLstring = '
		select
		playout002 單號,
		programID 節目編碼,
		(select pgm.FSPGMNAME from TBPROG_M pgm where pgm.FSPROG_ID = @programID collate Chinese_Taiwan_Stroke_CI_AS) 節目名稱,
		prgset 集數,
		(CASE 
			WHEN spec=1 THEN ''SD''
			WHEN spec=2 THEN ''HD''
			ELSE ''N/A''
		END) 規格,

		(CASE
			WHEN smoking=1 THEN ''要''
			WHEN smoking=2 THEN ''不要''
			ELSE ''N/A''
		END) 吸煙卡,

		(CASE
			WHEN alerts=1 THEN ''要''
			WHEN alerts=2 THEN ''不要''
			ELSE ''N/A''
		END) 警示卡,
		(CASE
			WHEN alertsName=0 THEN ''''
			WHEN alertsName=1 THEN ''制式版1''
			WHEN alertsName=2 THEN ''制式版2''
			WHEN alertsName=3 THEN ''制式版3''
			WHEN alertsName=4 THEN ''制式版4''
			WHEN alertsName=5 THEN ''制式版5''
			WHEN alertsName=6 THEN ''制式版6''
			WHEN alertsName=7 THEN ''客製版''
			ELSE ''N/A''
		END) 制式版,
		customName 警示卡名稱,
		customS 警示卡長度,

		(CASE
			WHEN title=1 THEN ''要''
			WHEN title=2 THEN ''不要''
			ELSE ''N/A''
		END) 檔名總片頭,

		titleName 檔名總片頭名稱,
		titleS 檔名總片頭長度,

		(CASE
			WHEN programSide=1 THEN ''要''
			WHEN programSide=2 THEN ''不要''
			ELSE ''N/A''
		END) 節目側標,

		(CASE
			WHEN liveSide=1 THEN ''要''
			WHEN liveSide=2 THEN ''不要''
			ELSE ''N/A''
		END) LIVE側標,

		(CASE
			WHEN timeSide=1 THEN ''要''
			WHEN timeSide=2 THEN ''不要''
			ELSE ''N/A''
		END) 時間側標,

		(CASE
			WHEN replay=1 THEN ''要''
			WHEN replay=2 THEN ''不要''
			ELSE ''N/A''
		END) 重播,

		(CASE
			WHEN video=1 THEN ''要''
			WHEN video=2 THEN ''不要''
			ELSE ''N/A''
		END) 錄影轉播,

		(CASE
			WHEN stereo=1 THEN ''要''
			WHEN stereo=2 THEN ''不要''
			ELSE ''N/A''
		END) 立體聲,

		(CASE 
			WHEN logo=0 THEN ''不要''
			WHEN logo=1 THEN ''右邊''
			ELSE ''N/A''
		END) 公視LOGO,

		ch1 雙語1,
		ch2 雙語2,
		ch3 雙語3,
		ch4 雙語4
		from [10.1.253.88].EF2KWeb.EZAdmin.playout as p '
		--where (@programID = '' or programID = @programID) and (@episode = '' or prgset = @episode) and (spec = @spec) or (playout002 = @playoutID)
		--where  programID = @programID and prgset = @episode and spec = @spec

		--ORDER BY playout002 DESC'


	if (@playoutID = '') BEGIN
		set @SQLstring = @SQLstring + ' where  programID = @programID and prgset = @episode and spec = @spec ORDER BY playout002 DESC'
		set @paramDef = '@programID varchar(8), @episode integer, @spec integer'
		EXEC sp_executesql @SQLstring, @paramDef,  @programID, @episode, @spec
	END
	else BEGIN
		set @SQLstring = @SQLstring + ' where playout002 = @playoutID'
		set @paramDef = '@playoutID varchar(20), @programID varchar(8)'
		EXEC sp_executesql @SQLstring, @paramDef,  @playoutID, @programID
	END

GO --END PROC SP_Q_PLAYOUY

/*
EXEC #SP_Q_PLAYOUT '0000012757', '', 0, 0
EXEC #SP_Q_PLAYOUT '', '2011365', 1, 1

GO
*/


CREATE PROC #PLAYOUT2MAM	--轉一筆主控播出資訊單到MAM，有給playoutID 就直接用，如果沒有就以 programID, episode, spec 找
	@playoutID varchar(20),
	@programID varchar(8),
	@episode integer,
	@spec integer
AS

	declare @id varchar(20)
	set @id =  '0000012757'
	select * from  [10.1.253.88].EF2KWeb.EZAdmin.playout where playout002 = @id
	
	select * into #tempPLAYOUT from [10.1.253.88].EF2KWeb.EZAdmin.playout where 1 = 0

	

GO --END PROC PLAYOUT2MAM

-- 讀取每一筆 TBPGM_INSERT_TAPE 資料和 playout 的資料和比較，顯示 MAM 裡頭的資料和  playout 有差異的部份，以及 playout 沒有主控資訊
-- 可以作為驗證用
CREATE PROC #COMPARE_PLAYOUT_MAM_20151124
AS

	DECLARE @pID varchar(8), @ep integer, @vType integer, @pair integer = 0, @pairCount integer = 0
	select * INTO #temp_PLAYOUT2MAM from TBPGM_INSERT_TAPE where 1=0 -- the scope of tempPLAYOUT is inside the procedure
	ALTER TABLE #temp_PLAYOUT2MAM
		add PLAYOUT002 varchar(20)

	ALTER TABLE #temp_PLAYOUT2MAM
		ALTER COLUMN FSCH1 varchar(50)
	ALTER TABLE #temp_PLAYOUT2MAM
		ALTER COLUMN FSCH2 varchar(50)
	ALTER TABLE #temp_PLAYOUT2MAM
		ALTER COLUMN FSCH3 varchar(50)
	ALTER TABLE #temp_PLAYOUT2MAM
		ALTER COLUMN FSCH4 varchar(50)



	select * INTO #diff_PLAYOUT2MAM from TBPGM_INSERT_TAPE where 1=0
	ALTER TABLE #diff_PLAYOUT2MAM
		add type char(3)

	-- 增加電子表單單號欄位
	ALTER TABLE #diff_PLAYOUT2MAM
		add PLAYOUT002 varchar(20)

	ALTER TABLE #diff_PLAYOUT2MAM
		add PAIR integer

	ALTER TABLE #diff_PLAYOUT2MAM
		ALTER COLUMN FSCH1 varchar(50)
	ALTER TABLE #diff_PLAYOUT2MAM
		ALTER COLUMN FSCH2 varchar(50)
	ALTER TABLE #diff_PLAYOUT2MAM
		ALTER COLUMN FSCH3 varchar(50)
	ALTER TABLE #diff_PLAYOUT2MAM
		ALTER COLUMN FSCH4 varchar(50)
		

	print 'CREATE #tempPLAYOUT'
	CREATE TABLE #tempPLAYOUT (
		playoutID varchar(20),
		programID varchar(8),
		prgset integer,
		spec integer
	)

	print 'CREATE #tempPLAYOUT1'
	CREATE TABLE #tempPLAYOUT1 (
		playoutID varchar(20),
		programID varchar(8),
		prgset integer,
		spec integer
	)


	print '列出有效的單號且根據單號排序，最新的在前面'
	INSERT #tempPLAYOUT(playoutID, programID, prgset, spec)
	SELECT playout002, programID, prgset, spec from [10.1.253.88].EF2KWeb.EZAdmin.playout as p
	inner join [10.1.253.88].[EF2KWeb].[dbo].[resda] as res 
	on p.PLAYOUT001 = res.resda001 and p.PLAYOUT002 = res.resda002 and res.resda021 = 2 order by playout002 DESC

	--select * from #tempPLAYOUT
	--select  distinct programID, prgset, spec from #tempPLAYOUT
	--select  distinct programID from #tempPLAYOUT
	--select programID, prgset, spec from #tempPLAYOUT group by  programID, prgset, spec

	--return
	
	print 'DECLARE tmpCSR'
	DECLARE tmpCSR CURSOR FOR select programID, prgset, spec from #tempPLAYOUT group by  programID, prgset, spec
	OPEN tmpCSR

	FETCH NEXT FROM tmpCSR INTO @pID, @ep, @vType
	
	print 'FETCH from tmpCSR in WHIEL LOOP'
	DECLARE @count1 integer = 0
	WHILE (@@FETCH_STATUS = 0) BEGIN
		--print 'Count = ' + CAST(@count1 AS varchar(10))
		INSERT #temp_PLAYOUT2MAM(PLAYOUT002, FSPROG_ID, FNEPISODE, FSARC_TYPE, FCSMOKE, FCALERTS, FSINSERT_TAPE, FSINSERT_TAPE_NAME, FSINSERT_TAPE_DUR, FSTITLE, FSTITLE_NAME, FSTITLE_DUR, FCPROG_SIDE_LABEL,
		FCLIVE_SIDE_LABEL, FCTIME_SIDE_LABEL, FCREPLAY, FCRECORD_PLAY, FCSTEREO, FCLOGO, FSCH1, FSCH2, FSCH3, FSCH4, FSCREATED_BY, FDCREATED_DATE)
		SELECT
			top 1
			playout002 as PLAYOUT002,
			programID as FSPROG_ID,
			prgset as FNEPISODE,

			(CASE spec
				WHEN 1 THEN '001'
				WHEN 2 THEN '002'
			END) as FSARC_TYPE,

			(CASE smoking
				WHEN  1 THEN 'Y'
				WHEN  2 THEN 'N'
			END) as FCSMOKE,

			(CASE alerts
				WHEN 1 THEN 'Y'
				WHEN 2 THEN 'N'
			END) as FCALERTS,

			(CASE alertsName
				WHEN 0 THEN 0
				WHEN 1 THEN 1
				WHEN 2 THEN 2
				WHEN 3 THEN 3
				WHEN 4 THEN 4
				WHEN 5 THEN 5
				WHEN 6 THEN 6
				WHEN 7 THEN 7
			END) as FSINSERT_TAPE,

			customName as FNINSERT_TAPE_NAME,
			customS as FSINSERT_TAPE_DUR,
			
			(CASE title
				WHEN 1 THEN 'Y'
				WHEN 2 THEN 'N'
			END) as FSTITLE,

			titleName as FSTITLE,
			titleS as FSTITLE_DUR,


			(CASE programSide
				WHEN 1 THEN 'Y'
				WHEN 2 THEN 'N'
			END) as FCPROG_SIDE_LABEL,

			(CASE liveSide
				WHEN 1 THEN 'Y'
				WHEN 2 THEN 'N'
			END) as FCLIVE_SIDE_LABEL,

			(CASE timeSide
				WHEN 1 THEN 'Y'
				WHEN 2 THEN 'N'
				ELSE NULL
			END) as FCTIME_SIDE_LABEL,

			(CASE replay
				WHEN 1 THEN 'Y'
				WHEN 2 THEN 'N'
			END) as FCREPLAY,

			(CASE video
				WHEN 1 THEN 'Y'
				WHEN 2 THEN 'N'
			END) as FCRECORD_PLAY,

			(CASE stereo
				WHEN 1 THEN 'Y'
				WHEN 2 THEN 'N'
			END) as FCSTEREO,

			(CASE logo
				WHEN '0' THEN 'N'
				WHEN 1 THEN 'Y'  --右邊
			END) as FCLOGO,
			
			ch1 as FSCH1,	
			ch2 as FSCH2,			
			ch3 as FSCH3,	
			ch4 as FSCH4,
		
			USER_ID as FSCREATED_BY,
			GETDATE() as FDCREATE_DATE

		FROM [10.1.253.88].EF2KWeb.EZAdmin.playout as p
		inner join [10.1.253.88].[EF2KWeb].[dbo].[resda] as res 
		on p.PLAYOUT001 = res.resda001 and p.PLAYOUT002 = res.resda002 and res.resda021 = 2 where programID = @pID and prgset = @ep and spec = @vType ORDER BY playout002 DESC

		FETCH NEXT FROM tmpCSR INTO @pID, @ep, @vType
		--set @count1 += 1

	END --WHILE

	CLOSE tmpCSR


	/*
	
	DECLARE PLAYOUTCSR CURSOR FOR 
		SELECT playout002, programID, prgset, spec from [10.1.253.88].EF2KWeb.EZAdmin.playout as p
		inner join [10.1.253.88].[EF2KWeb].[dbo].[resda] as res 
		on p.PLAYOUT001 = res.resda001 and p.PLAYOUT002 = res.resda002 and res.resda021 = 2 ORDER BY playout002 DESC

	OPEN PLAYOUTCSR

	FETCH NEXT FROM PLAYOUTCSR INTO
	*/

	--select * from #temp_PLAYOUT2MAM
	--select FSPROG_ID, FNEPISODE, FSARC_TYPE, count(FSPROG_ID) from #temp_PLAYOUT2MAM group by FSPROG_ID, FNEPISODE, FSARC_TYPE HAVING count(FSPROG_ID) > 1
	if 0=1 BEGIN -- for DEBUG
		if (exists (select count(FSPROG_ID) from #temp_PLAYOUT2MAM group by FSPROG_ID, FNEPISODE, FSARC_TYPE having count(FSPROG_ID) > 1) ) BEGIN
			RAISERROR('ERROR:Found duplicate playout ticket!', 0, 1) with NOWAIT
			select FSPROG_ID, FNEPISODE, FSARC_TYPE, count(FSPROG_ID) 數量 from #temp_PLAYOUT2MAM group by FSPROG_ID, FNEPISODE, FSARC_TYPE having count(FSPROG_ID) > 1
		END
		else
			select FSPROG_ID, FNEPISODE, FSARC_TYPE from #temp_PLAYOUT2MAM group by FSPROG_ID, FNEPISODE, FSARC_TYPE
		return
	END

	-- 宣告變數存放 TBPGM_INSERT_TAPE 的資訊
	DECLARE 
	@FSPROG_ID char(7)
	,@FNEPISODE int
	,@FSARC_TYPE varchar(3)
	,@FCSMOKE char(1)
	,@FCALERTS char(1)
	,@FSINSERT_TAPE varchar(1)
	,@FSINSERT_TAPE_NAME varchar(50)
	,@FSINSERT_TAPE_DUR int
	,@FSTITLE char(1)
	,@FSTITLE_NAME varchar(50)
	,@FSTITLE_DUR int
	,@FCPROG_SIDE_LABEL char(1)
	,@FCLIVE_SIDE_LABEL char(1)
	,@FCTIME_SIDE_LABEL char(1)
	,@FCRECORD_PLAY char(1)
	,@FCREPLAY char(1)
	,@FCSTEREO char(1)
	,@FCLOGO char(1)
	,@FSCH1 char(50)
	,@FSCH2 char(50)
	,@FSCH3 char(50)
	,@FSCH4 char(50)

	-- 宣告變數存放 playout （#temp_PLAYOUT2MAM) 的資訊
	DECLARE
	@PLAYOUT002 varchar(20)
	,@FSPROG_ID1 char(7)
	,@FNEPISODE1 int
	,@FSARC_TYPE1 varchar(3)
	,@FCSMOKE1 char(1)
	,@FCALERTS1 char(1)
	,@FSINSERT_TAPE1 varchar(1)
	,@FSINSERT_TAPE_NAME1 varchar(50)
	,@FSINSERT_TAPE_DUR1 int
	,@FSTITLE1 char(1)
	,@FSTITLE_NAME1 varchar(50)
	,@FSTITLE_DUR1 int
	,@FCPROG_SIDE_LABEL1 char(1)
	,@FCLIVE_SIDE_LABEL1 char(1)
	,@FCTIME_SIDE_LABEL1 char(1)
	,@FCREPLAY1 char(1)
	,@FCRECORD_PLAY1 char(1)
	,@FCSTEREO1 char(1)
	,@FCLOGO1 char(1)
	,@FSCH11 char(50)
	,@FSCH21 char(50)
	,@FSCH31 char(50)
	,@FSCH41 char(50)

	DECLARE @diff integer = 1, @count integer = 0
	-- 建立 TBPGM_INSERT_TAPE CURSOR
	DECLARE CSR CURSOR FOR SELECT FSPROG_ID, FNEPISODE, FSARC_TYPE, FCSMOKE, FCALERTS, FSINSERT_TAPE, FSINSERT_TAPE_NAME, FSINSERT_TAPE_DUR, FSTITLE, FSTITLE_NAME, FSTITLE_DUR, FCPROG_SIDE_LABEL, 
	FCLIVE_SIDE_LABEL, FCTIME_SIDE_LABEL, FCREPLAY, FCRECORD_PLAY, FCSTEREO, FCLOGO, FSCH1, FSCH2, FSCH3, FSCH4 from TBPGM_INSERT_TAPE

	OPEN CSR

	--讀取 TBPGM_INSERT_TAPE 存到暫存變數
	FETCH NEXT FROM CSR INTO 
	@FSPROG_ID
	,@FNEPISODE
	,@FSARC_TYPE
	,@FCSMOKE
	,@FCALERTS
	,@FSINSERT_TAPE
	,@FSINSERT_TAPE_NAME
	,@FSINSERT_TAPE_DUR
	,@FSTITLE
	,@FSTITLE_NAME
	,@FSTITLE_DUR
	,@FCPROG_SIDE_LABEL
	,@FCLIVE_SIDE_LABEL
	,@FCTIME_SIDE_LABEL
	,@FCREPLAY
	,@FCRECORD_PLAY
	,@FCSTEREO
	,@FCLOGO
	,@FSCH1
	,@FSCH2
	,@FSCH3
	,@FSCH4
	
	WHILE (@@FETCH_STATUS = 0) BEGIN
		if EXISTS( select FSPROG_ID from #temp_PLAYOUT2MAM where FSPROG_ID = @FSPROG_ID and FNEPISODE = @FNEPISODE and FSARC_TYPE = @FSARC_TYPE ) BEGIN --COMPARE need check number of records found
			
			--print @count
			--set @count += 1
			set @pair = 1 -- Both eip and MAM has records
			select
			@PLAYOUT002 = PLAYOUT002
			,@FCSMOKE1 = FCSMOKE
			,@FCALERTS1 = FCALERTS
			,@FSINSERT_TAPE1 = FSINSERT_TAPE
			,@FSINSERT_TAPE_NAME1 = FSINSERT_TAPE_NAME
			,@FSINSERT_TAPE_DUR1 = FSINSERT_TAPE_DUR
			,@FSTITLE1 = FSTITLE
			,@FSTITLE_NAME1 = FSTITLE_NAME
			,@FSTITLE_DUR1 = FSTITLE_DUR
			,@FCPROG_SIDE_LABEL1 = FCPROG_SIDE_LABEL
			,@FCLIVE_SIDE_LABEL1 = FCLIVE_SIDE_LABEL
			,@FCTIME_SIDE_LABEL1 = FCTIME_SIDE_LABEL
			,@FCREPLAY1 = FCREPLAY
			,@FCRECORD_PLAY1 = FCRECORD_PLAY
			,@FCSTEREO1 = FCSTEREO
			,@FCLOGO1 = FCLOGO
			,@FSCH11 = FSCH1
			,@FSCH21 = FSCH2
			,@FSCH31 = FSCH3
			,@FSCH41 = FSCH4
			from #temp_PLAYOUT2MAM where FSPROG_ID = @FSPROG_ID and FNEPISODE = @FNEPISODE and FSARC_TYPE = @FSARC_TYPE

			-- COMPARE
			if @FCSMOKE1 != @FCSMOKE BEGIN
				set @diff = 1
			END
			if @FCALERTS1 != @FCALERTS1 BEGIN
				set @diff = 1
			END
			if @FSINSERT_TAPE1 != 0 AND @FSINSERT_TAPE !='' BEGIN
				if @FSINSERT_TAPE1 != @FSINSERT_TAPE
					set @diff = 1
			END
			if @FSINSERT_TAPE_NAME1 != @FSINSERT_TAPE_NAME BEGIN
				set @diff = 1
			END
			if @FSINSERT_TAPE_DUR1 != @FSINSERT_TAPE_DUR BEGIN
				set @diff = 1
			END
			if @FSTITLE1 != @FSTITLE BEGIN
				set @diff = 1
			END
			if @FSTITLE_NAME1 != @FSTITLE_NAME BEGIN
				set @diff = 1
			END
			if @FSTITLE_DUR1 != @FSTITLE_DUR BEGIN
				set @diff = 1
			END
			if @FCPROG_SIDE_LABEL1 != @FCPROG_SIDE_LABEL BEGIN
				set @diff = 1
			END
			if @FCLIVE_SIDE_LABEL1 != @FCLIVE_SIDE_LABEL BEGIN
				set @diff = 1
			END
			if @FCTIME_SIDE_LABEL1 != @FCTIME_SIDE_LABEL BEGIN
				set @diff = 1
			END
			if @FCREPLAY1 != @FCREPLAY BEGIN
				set @diff = 1
			END
			if @FCRECORD_PLAY1 != @FCRECORD_PLAY BEGIN
				set @diff = 1
			END
			if @FCSTEREO1 != @FCSTEREO BEGIN
				set @diff = 1
			END
			if @FCLOGO1 != @FCLOGO BEGIN
				set @diff = 1
			END
			/*
			if @FSCH11 != @FSCH1 BEGIN
				set @diff = 1
			END
			if @FSCH21 != @FSCH2 BEGIN
				set @diff = 1
			END
			if @FSCH31 != @FSCH3 BEGIN
				set @diff = 1
			END
			if @FSCH41 != @FSCH4 BEGIN
				set @diff = 1
			END
			*/
		END -- IF-CMPARE
		
		if @diff = 1 BEGIN --list different record, 只有 MAM 有，EIP 沒有也會列出
		    if @pair = 1
				set @pairCount += 1
			
			-- insert EIP data	 
			insert into #diff_PLAYOUT2MAM(PAIR, TYPE, PLAYOUT002, FSPROG_ID, FNEPISODE, FSARC_TYPE, FCSMOKE, FCALERTS, FSINSERT_TAPE, FSINSERT_TAPE_NAME, FSINSERT_TAPE_DUR, FSTITLE, FSTITLE_NAME, FSTITLE_DUR, FCPROG_SIDE_LABEL, FCLIVE_SIDE_LABEL, FCTIME_SIDE_LABEL, FCREPLAY, FCRECORD_PLAY, FCSTEREO, FCLOGO, FSCH1, FSCH2, FSCH3, FSCH4, FSCREATED_BY, FDCREATED_DATE) 
			select  (CASE WHEN @pair = 0 THEN 0 ELSE @pairCount END) PAIR, 'EIP', PLAYOUT002, FSPROG_ID, FNEPISODE, FSARC_TYPE, FCSMOKE, FCALERTS, FSINSERT_TAPE, FSINSERT_TAPE_NAME, FSINSERT_TAPE_DUR, FSTITLE, FSTITLE_NAME, FSTITLE_DUR, FCPROG_SIDE_LABEL, FCLIVE_SIDE_LABEL, FCTIME_SIDE_LABEL, FCREPLAY, FCRECORD_PLAY, FCSTEREO, FCLOGO, FSCH1, FSCH2, FSCH3, FSCH4, FSCREATED_BY, FDCREATED_DATE
			from #temp_PLAYOUT2MAM where FSPROG_ID = @FSPROG_ID and FNEPISODE = @FNEPISODE and FSARC_TYPE = @FSARC_TYPE

			-- insert MAM data
			insert into #diff_PLAYOUT2MAM(PAIR, TYPE, PLAYOUT002, FSPROG_ID, FNEPISODE, FSARC_TYPE, FCSMOKE, FCALERTS, FSINSERT_TAPE, FSINSERT_TAPE_NAME, FSINSERT_TAPE_DUR, FSTITLE, FSTITLE_NAME, FSTITLE_DUR, FCPROG_SIDE_LABEL, FCLIVE_SIDE_LABEL, FCTIME_SIDE_LABEL, FCREPLAY, FCRECORD_PLAY, FCSTEREO, FCLOGO, FSCH1, FSCH2, FSCH3, FSCH4, FSCREATED_BY, FDCREATED_DATE)
			select (CASE WHEN @pair = 0 THEN 0 ELSE @pairCount END) PAIR, 'MAM', '', FSPROG_ID, FNEPISODE, FSARC_TYPE, FCSMOKE, FCALERTS, FSINSERT_TAPE, FSINSERT_TAPE_NAME, FSINSERT_TAPE_DUR, FSTITLE, FSTITLE_NAME, FSTITLE_DUR, FCPROG_SIDE_LABEL, FCLIVE_SIDE_LABEL, FCTIME_SIDE_LABEL, FCREPLAY, FCRECORD_PLAY, FCSTEREO, FCLOGO, FSCH1, FSCH2, FSCH3, FSCH4, FSCREATED_BY, FDCREATED_DATE
			from TBPGM_INSERT_TAPE where FSPROG_ID = @FSPROG_ID and FNEPISODE = @FNEPISODE and FSARC_TYPE = @FSARC_TYPE

			--print 'found diff on FSPROG_ID=' + @FSPROG_ID + 'FNEPISODE=' + CAST(@FNEPISODE as varchar(4)) + 'FSARC_TYPE=' + @FSARC_TYPE
			
			set @diff = 0
			set @pair = 0
		END --if
		
		FETCH NEXT FROM CSR INTO
			@FSPROG_ID
			,@FNEPISODE
			,@FSARC_TYPE
			,@FCSMOKE
			,@FCALERTS
			,@FSINSERT_TAPE
			,@FSINSERT_TAPE_NAME
			,@FSINSERT_TAPE_DUR
			,@FSTITLE
			,@FSTITLE_NAME
			,@FSTITLE_DUR
			,@FCPROG_SIDE_LABEL
			,@FCLIVE_SIDE_LABEL
			,@FCTIME_SIDE_LABEL
			,@FCREPLAY
			,@FCRECORD_PLAY
			,@FCSTEREO
			,@FCLOGO
			,@FSCH1
			,@FSCH2
			,@FSCH3
			,@FSCH4	
	END --WHILE
	CLOSE CSR
	select PAIR, TYPE 來源, PLAYOUT002 EIP單號, FSPROG_ID 節目編號, FNEPISODE 集數, FSARC_TYPE 影片規格, FCSMOKE 吸煙卡, FCALERTS 警示卡, FSINSERT_TAPE 制式版, FSINSERT_TAPE_NAME 客製版名稱, FSINSERT_TAPE_DUR 長度, FSTITLE 檔名總片頭, FSTITLE_NAME 檔名總片頭名稱, FSTITLE_DUR 長度, 
	FCPROG_SIDE_LABEL 節目測標, FCLIVE_SIDE_LABEL LIVE測標, FCTIME_SIDE_LABEL 時間測標, FCREPLAY 重播, FCRECORD_PLAY 錄影重播, FCSTEREO 立體聲, FCLOGO 頻道LOGO, FSCH1 雙語CH1, FSCH2 雙語CH2, FSCH3 雙語CH3, FSCH4 雙語CH4, 
	u.FSUSER_ChtName 建立者, p.FDCREATED_DATE 建立日期 from #diff_PLAYOUT2MAM p join TBUSERS u on p.FSCREATED_BY = u.FSUSER_ID order by p.PAIR ASC, p.TYPE ASC

GO -- END CREATE PROC COMPARE_PLAYOUT_MAM_20151124

CREATE PROC #COMPARE_PLAYOUT_MAM
AS

	DECLARE @pID varchar(8), @ep integer, @vType integer, @pair integer = 0, @pairCount integer = 0
	select * INTO #temp_PLAYOUT2MAM from TBPGM_INSERT_TAPE where 1=0 -- the scope of tempPLAYOUT is inside the procedure
	ALTER TABLE #temp_PLAYOUT2MAM
		add PLAYOUT002 varchar(20)

	ALTER TABLE #temp_PLAYOUT2MAM
		ALTER COLUMN FSCH1 varchar(50)
	ALTER TABLE #temp_PLAYOUT2MAM
		ALTER COLUMN FSCH2 varchar(50)
	ALTER TABLE #temp_PLAYOUT2MAM
		ALTER COLUMN FSCH3 varchar(50)
	ALTER TABLE #temp_PLAYOUT2MAM
		ALTER COLUMN FSCH4 varchar(50)



	select * INTO #diff_PLAYOUT2MAM from TBPGM_INSERT_TAPE where 1=0
	ALTER TABLE #diff_PLAYOUT2MAM
		add type char(3)

	-- 增加電子表單單號欄位
	ALTER TABLE #diff_PLAYOUT2MAM
		add PLAYOUT002 varchar(20)

	ALTER TABLE #diff_PLAYOUT2MAM
		add PAIR integer

	ALTER TABLE #diff_PLAYOUT2MAM
		ALTER COLUMN FSCH1 varchar(50)
	ALTER TABLE #diff_PLAYOUT2MAM
		ALTER COLUMN FSCH2 varchar(50)
	ALTER TABLE #diff_PLAYOUT2MAM
		ALTER COLUMN FSCH3 varchar(50)
	ALTER TABLE #diff_PLAYOUT2MAM
		ALTER COLUMN FSCH4 varchar(50)
		

	print 'CREATE #tempPLAYOUT'
	CREATE TABLE #tempPLAYOUT (
		playoutID varchar(20),
		programID varchar(8),
		prgset integer,
		spec integer
	)

	print 'CREATE #tempPLAYOUT1'
	CREATE TABLE #tempPLAYOUT1 (
		playoutID varchar(20),
		programID varchar(8),
		prgset integer,
		spec integer
	)


	print '列出有效的單號且根據單號排序，最新的在前面'
	INSERT #tempPLAYOUT(playoutID, programID, prgset, spec)
	SELECT playout002, programID, prgset, spec from [10.1.253.88].EF2KWeb.EZAdmin.playout as p
	inner join [10.1.253.88].[EF2KWeb].[dbo].[resda] as res 
	on p.PLAYOUT001 = res.resda001 and p.PLAYOUT002 = res.resda002 and res.resda021 = 2 order by playout002 DESC

	--select * from #tempPLAYOUT
	--select  distinct programID, prgset, spec from #tempPLAYOUT
	--select  distinct programID from #tempPLAYOUT
	--select programID, prgset, spec from #tempPLAYOUT group by  programID, prgset, spec

	--return
	
	print 'DECLARE tmpCSR'
	DECLARE tmpCSR CURSOR FOR select programID, prgset, spec from #tempPLAYOUT group by  programID, prgset, spec
	OPEN tmpCSR

	FETCH NEXT FROM tmpCSR INTO @pID, @ep, @vType
	
	print 'FETCH from tmpCSR in WHIEL LOOP'
	DECLARE @count1 integer = 0
	WHILE (@@FETCH_STATUS = 0) BEGIN
		--print 'Count = ' + CAST(@count1 AS varchar(10))
		INSERT #temp_PLAYOUT2MAM(PLAYOUT002, FSPROG_ID, FNEPISODE, FSARC_TYPE, FCSMOKE, FCALERTS, FSINSERT_TAPE, FSINSERT_TAPE_NAME, FSINSERT_TAPE_DUR, FSTITLE, FSTITLE_NAME, FSTITLE_DUR, FCPROG_SIDE_LABEL,
		FCLIVE_SIDE_LABEL, FCTIME_SIDE_LABEL, FCREPLAY, FCRECORD_PLAY, FCSTEREO, FCLOGO, FSCH1, FSCH2, FSCH3, FSCH4, FSCREATED_BY, FDCREATED_DATE)
		SELECT
			top 1
			playout002 as PLAYOUT002,
			programID as FSPROG_ID,
			prgset as FNEPISODE,

			(CASE spec
				WHEN 1 THEN '001'
				WHEN 2 THEN '002'
			END) as FSARC_TYPE,

			(CASE smoking
				WHEN  1 THEN 'Y'
				WHEN  2 THEN 'N'
			END) as FCSMOKE,

			(CASE alerts
				WHEN 1 THEN 'Y'
				WHEN 2 THEN 'N'
			END) as FCALERTS,

			(CASE alertsName
				WHEN 0 THEN 0
				WHEN 1 THEN 1
				WHEN 2 THEN 2
				WHEN 3 THEN 3
				WHEN 4 THEN 4
				WHEN 5 THEN 5
				WHEN 6 THEN 6
				WHEN 7 THEN 7
			END) as FSINSERT_TAPE,

			customName as FNINSERT_TAPE_NAME,
			customS as FSINSERT_TAPE_DUR,
			
			(CASE title
				WHEN 1 THEN 'Y'
				WHEN 2 THEN 'N'
			END) as FSTITLE,

			titleName as FSTITLE,
			titleS as FSTITLE_DUR,


			(CASE programSide
				WHEN 1 THEN 'Y'
				WHEN 2 THEN 'N'
			END) as FCPROG_SIDE_LABEL,

			(CASE liveSide
				WHEN 1 THEN 'Y'
				WHEN 2 THEN 'N'
			END) as FCLIVE_SIDE_LABEL,

			(CASE timeSide
				WHEN 1 THEN 'Y'
				WHEN 2 THEN 'N'
				ELSE NULL
			END) as FCTIME_SIDE_LABEL,

			(CASE replay
				WHEN 1 THEN 'Y'
				WHEN 2 THEN 'N'
			END) as FCREPLAY,

			(CASE video
				WHEN 1 THEN 'Y'
				WHEN 2 THEN 'N'
			END) as FCRECORD_PLAY,

			(CASE stereo
				WHEN 1 THEN 'Y'
				WHEN 2 THEN 'N'
			END) as FCSTEREO,

			(CASE logo
				WHEN '0' THEN 'N'
				WHEN 1 THEN 'Y'  --右邊
			END) as FCLOGO,
			
			ch1 as FSCH1,	
			ch2 as FSCH2,			
			ch3 as FSCH3,	
			ch4 as FSCH4,
		
			'51204' as FSCREATED_BY,
			GETDATE() as FDCREATE_DATE

		FROM [10.1.253.88].EF2KWeb.EZAdmin.playout as p
		inner join [10.1.253.88].[EF2KWeb].[dbo].[resda] as res 
		on p.PLAYOUT001 = res.resda001 and p.PLAYOUT002 = res.resda002 and res.resda021 = 2 where programID = @pID and prgset = @ep and spec = @vType ORDER BY playout002 DESC

		FETCH NEXT FROM tmpCSR INTO @pID, @ep, @vType
		--set @count1 += 1

	END --WHILE

	CLOSE tmpCSR


	/*
	
	DECLARE PLAYOUTCSR CURSOR FOR 
		SELECT playout002, programID, prgset, spec from [10.1.253.88].EF2KWeb.EZAdmin.playout as p
		inner join [10.1.253.88].[EF2KWeb].[dbo].[resda] as res 
		on p.PLAYOUT001 = res.resda001 and p.PLAYOUT002 = res.resda002 and res.resda021 = 2 ORDER BY playout002 DESC

	OPEN PLAYOUTCSR

	FETCH NEXT FROM PLAYOUTCSR INTO
	*/

	--select * from #temp_PLAYOUT2MAM
	--select FSPROG_ID, FNEPISODE, FSARC_TYPE, count(FSPROG_ID) from #temp_PLAYOUT2MAM group by FSPROG_ID, FNEPISODE, FSARC_TYPE HAVING count(FSPROG_ID) > 1
	if 0=1 BEGIN -- for DEBUG
		if (exists (select count(FSPROG_ID) from #temp_PLAYOUT2MAM group by FSPROG_ID, FNEPISODE, FSARC_TYPE having count(FSPROG_ID) > 1) ) BEGIN
			RAISERROR('ERROR:Found duplicate playout ticket!', 0, 1) with NOWAIT
			select FSPROG_ID, FNEPISODE, FSARC_TYPE, count(FSPROG_ID) 數量 from #temp_PLAYOUT2MAM group by FSPROG_ID, FNEPISODE, FSARC_TYPE having count(FSPROG_ID) > 1
		END
		else
			select FSPROG_ID, FNEPISODE, FSARC_TYPE from #temp_PLAYOUT2MAM group by FSPROG_ID, FNEPISODE, FSARC_TYPE
		return
	END

	-- 建立 TBPGM_INSERT_TAPE CURSOR
	DECLARE CSR CURSOR FOR SELECT FSPROG_ID, FNEPISODE, FSARC_TYPE, FCSMOKE, FCALERTS, FSINSERT_TAPE, FSINSERT_TAPE_NAME, FSINSERT_TAPE_DUR, FSTITLE, FSTITLE_NAME, FSTITLE_DUR, FCPROG_SIDE_LABEL, 
	FCLIVE_SIDE_LABEL, FCTIME_SIDE_LABEL, FCREPLAY, FCRECORD_PLAY, FCSTEREO, FCLOGO, FSCH1, FSCH2, FSCH3, FSCH4 from TBPGM_INSERT_TAPE

	-- 宣告變數存放 TBPGM_INSERT_TAPE 的資訊
	DECLARE 
	@FSPROG_ID char(7)
	,@FNEPISODE int
	,@FSARC_TYPE varchar(3)
	,@FCSMOKE char(1)
	,@FCALERTS char(1)
	,@FSINSERT_TAPE varchar(1)
	,@FSINSERT_TAPE_NAME varchar(50)
	,@FSINSERT_TAPE_DUR int
	,@FSTITLE char(1)
	,@FSTITLE_NAME varchar(50)
	,@FSTITLE_DUR int
	,@FCPROG_SIDE_LABEL char(1)
	,@FCLIVE_SIDE_LABEL char(1)
	,@FCTIME_SIDE_LABEL char(1)
	,@FCRECORD_PLAY char(1)
	,@FCREPLAY char(1)
	,@FCSTEREO char(1)
	,@FCLOGO char(1)
	,@FSCH1 char(50)
	,@FSCH2 char(50)
	,@FSCH3 char(50)
	,@FSCH4 char(50)

	-- 宣告變數存放 playout （#temp_PLAYOUT2MAM) 的資訊
	DECLARE
	@PLAYOUT002 varchar(20)
	,@FSPROG_ID1 char(7)
	,@FNEPISODE1 int
	,@FSARC_TYPE1 varchar(3)
	,@FCSMOKE1 char(1)
	,@FCALERTS1 char(1)
	,@FSINSERT_TAPE1 varchar(1)
	,@FSINSERT_TAPE_NAME1 varchar(50)
	,@FSINSERT_TAPE_DUR1 int
	,@FSTITLE1 char(1)
	,@FSTITLE_NAME1 varchar(50)
	,@FSTITLE_DUR1 int
	,@FCPROG_SIDE_LABEL1 char(1)
	,@FCLIVE_SIDE_LABEL1 char(1)
	,@FCTIME_SIDE_LABEL1 char(1)
	,@FCREPLAY1 char(1)
	,@FCRECORD_PLAY1 char(1)
	,@FCSTEREO1 char(1)
	,@FCLOGO1 char(1)
	,@FSCH11 char(50)
	,@FSCH21 char(50)
	,@FSCH31 char(50)
	,@FSCH41 char(50)

	DECLARE @diff integer = 1, @count integer = 0

	OPEN CSR

	--讀取 TBPGM_INSERT_TAPE 存到暫存變數
	FETCH NEXT FROM CSR INTO 
	@FSPROG_ID
	,@FNEPISODE
	,@FSARC_TYPE
	,@FCSMOKE
	,@FCALERTS
	,@FSINSERT_TAPE
	,@FSINSERT_TAPE_NAME
	,@FSINSERT_TAPE_DUR
	,@FSTITLE
	,@FSTITLE_NAME
	,@FSTITLE_DUR
	,@FCPROG_SIDE_LABEL
	,@FCLIVE_SIDE_LABEL
	,@FCTIME_SIDE_LABEL
	,@FCREPLAY
	,@FCRECORD_PLAY
	,@FCSTEREO
	,@FCLOGO
	,@FSCH1
	,@FSCH2
	,@FSCH3
	,@FSCH4
	
	WHILE (@@FETCH_STATUS = 0) BEGIN
		if EXISTS( select FSPROG_ID from #temp_PLAYOUT2MAM where FSPROG_ID = @FSPROG_ID and FNEPISODE = @FNEPISODE and FSARC_TYPE = @FSARC_TYPE ) BEGIN --COMPARE need check number of records found
			
			--print @count
			--set @count += 1
			set @pair = 1 -- Both eip and MAM has records
			select
			@PLAYOUT002 = PLAYOUT002
			,@FCSMOKE1 = FCSMOKE
			,@FCALERTS1 = FCALERTS
			,@FSINSERT_TAPE1 = FSINSERT_TAPE
			,@FSINSERT_TAPE_NAME1 = FSINSERT_TAPE_NAME
			,@FSINSERT_TAPE_DUR1 = FSINSERT_TAPE_DUR
			,@FSTITLE1 = FSTITLE
			,@FSTITLE_NAME1 = FSTITLE_NAME
			,@FSTITLE_DUR1 = FSTITLE_DUR
			,@FCPROG_SIDE_LABEL1 = FCPROG_SIDE_LABEL
			,@FCLIVE_SIDE_LABEL1 = FCLIVE_SIDE_LABEL
			,@FCTIME_SIDE_LABEL1 = FCTIME_SIDE_LABEL
			,@FCREPLAY1 = FCREPLAY
			,@FCRECORD_PLAY1 = FCRECORD_PLAY
			,@FCSTEREO1 = FCSTEREO
			,@FCLOGO1 = FCLOGO
			,@FSCH11 = FSCH1
			,@FSCH21 = FSCH2
			,@FSCH31 = FSCH3
			,@FSCH41 = FSCH4
			from #temp_PLAYOUT2MAM where FSPROG_ID = @FSPROG_ID and FNEPISODE = @FNEPISODE and FSARC_TYPE = @FSARC_TYPE

			-- COMPARE
			if @FCSMOKE1 != @FCSMOKE BEGIN
				set @diff = 1
			END
			if @FCALERTS1 != @FCALERTS1 BEGIN
				set @diff = 1
			END
			if @FSINSERT_TAPE1 != 0 AND @FSINSERT_TAPE !='' BEGIN
				if @FSINSERT_TAPE1 != @FSINSERT_TAPE
					set @diff = 1
			END
			if @FSINSERT_TAPE_NAME1 != @FSINSERT_TAPE_NAME BEGIN
				set @diff = 1
			END
			if @FSINSERT_TAPE_DUR1 != @FSINSERT_TAPE_DUR BEGIN
				set @diff = 1
			END
			if @FSTITLE1 != @FSTITLE BEGIN
				set @diff = 1
			END
			if @FSTITLE_NAME1 != @FSTITLE_NAME BEGIN
				set @diff = 1
			END
			if @FSTITLE_DUR1 != @FSTITLE_DUR BEGIN
				set @diff = 1
			END
			if @FCPROG_SIDE_LABEL1 != @FCPROG_SIDE_LABEL BEGIN
				set @diff = 1
			END
			if @FCLIVE_SIDE_LABEL1 != @FCLIVE_SIDE_LABEL BEGIN
				set @diff = 1
			END
			if @FCTIME_SIDE_LABEL1 != @FCTIME_SIDE_LABEL BEGIN
				set @diff = 1
			END
			if @FCREPLAY1 != @FCREPLAY BEGIN
				set @diff = 1
			END
			if @FCRECORD_PLAY1 != @FCRECORD_PLAY BEGIN
				set @diff = 1
			END
			if @FCSTEREO1 != @FCSTEREO BEGIN
				set @diff = 1
			END
			if @FCLOGO1 != @FCLOGO BEGIN
				set @diff = 1
			END
			/*
			if @FSCH11 != @FSCH1 BEGIN
				set @diff = 1
			END
			if @FSCH21 != @FSCH2 BEGIN
				set @diff = 1
			END
			if @FSCH31 != @FSCH3 BEGIN
				set @diff = 1
			END
			if @FSCH41 != @FSCH4 BEGIN
				set @diff = 1
			END
			*/
		END -- IF-CMPARE
		
		if @diff = 1 BEGIN --list different record, 只有 MAM 有，EIP 沒有也會列出
		    if @pair = 1
				set @pairCount += 1
				 
			insert into #diff_PLAYOUT2MAM(PAIR, TYPE, PLAYOUT002, FSPROG_ID, FNEPISODE, FSARC_TYPE, FCSMOKE, FCALERTS, FSINSERT_TAPE, FSINSERT_TAPE_NAME, FSINSERT_TAPE_DUR, FSTITLE, FSTITLE_NAME, FSTITLE_DUR, FCPROG_SIDE_LABEL, FCLIVE_SIDE_LABEL, FCTIME_SIDE_LABEL, FCREPLAY, FCRECORD_PLAY, FCSTEREO, FCLOGO, FSCH1, FSCH2, FSCH3, FSCH4, FSCREATED_BY, FDCREATED_DATE) 
			select  (CASE WHEN @pair = 0 THEN 0 ELSE @pairCount END) PAIR, 'EIP', PLAYOUT002, FSPROG_ID, FNEPISODE, FSARC_TYPE, FCSMOKE, FCALERTS, FSINSERT_TAPE, FSINSERT_TAPE_NAME, FSINSERT_TAPE_DUR, FSTITLE, FSTITLE_NAME, FSTITLE_DUR, FCPROG_SIDE_LABEL, FCLIVE_SIDE_LABEL, FCTIME_SIDE_LABEL, FCREPLAY, FCRECORD_PLAY, FCSTEREO, FCLOGO, FSCH1, FSCH2, FSCH3, FSCH4, FSCREATED_BY, FDCREATED_DATE
			from #temp_PLAYOUT2MAM where FSPROG_ID = @FSPROG_ID and FNEPISODE = @FNEPISODE and FSARC_TYPE = @FSARC_TYPE

			insert into #diff_PLAYOUT2MAM(PAIR, TYPE, PLAYOUT002, FSPROG_ID, FNEPISODE, FSARC_TYPE, FCSMOKE, FCALERTS, FSINSERT_TAPE, FSINSERT_TAPE_NAME, FSINSERT_TAPE_DUR, FSTITLE, FSTITLE_NAME, FSTITLE_DUR, FCPROG_SIDE_LABEL, FCLIVE_SIDE_LABEL, FCTIME_SIDE_LABEL, FCREPLAY, FCRECORD_PLAY, FCSTEREO, FCLOGO, FSCH1, FSCH2, FSCH3, FSCH4, FSCREATED_BY, FDCREATED_DATE)
			select (CASE WHEN @pair = 0 THEN 0 ELSE @pairCount END) PAIR, 'MAM', '', FSPROG_ID, FNEPISODE, FSARC_TYPE, FCSMOKE, FCALERTS, FSINSERT_TAPE, FSINSERT_TAPE_NAME, FSINSERT_TAPE_DUR, FSTITLE, FSTITLE_NAME, FSTITLE_DUR, FCPROG_SIDE_LABEL, FCLIVE_SIDE_LABEL, FCTIME_SIDE_LABEL, FCREPLAY, FCRECORD_PLAY, FCSTEREO, FCLOGO, FSCH1, FSCH2, FSCH3, FSCH4, FSCREATED_BY, FDCREATED_DATE
			from TBPGM_INSERT_TAPE where FSPROG_ID = @FSPROG_ID and FNEPISODE = @FNEPISODE and FSARC_TYPE = @FSARC_TYPE

			--print 'found diff on FSPROG_ID=' + @FSPROG_ID + 'FNEPISODE=' + CAST(@FNEPISODE as varchar(4)) + 'FSARC_TYPE=' + @FSARC_TYPE
			
			set @diff = 0
			set @pair = 0
		END --if
		
		FETCH NEXT FROM CSR INTO
			@FSPROG_ID
			,@FNEPISODE
			,@FSARC_TYPE
			,@FCSMOKE
			,@FCALERTS
			,@FSINSERT_TAPE
			,@FSINSERT_TAPE_NAME
			,@FSINSERT_TAPE_DUR
			,@FSTITLE
			,@FSTITLE_NAME
			,@FSTITLE_DUR
			,@FCPROG_SIDE_LABEL
			,@FCLIVE_SIDE_LABEL
			,@FCTIME_SIDE_LABEL
			,@FCREPLAY
			,@FCRECORD_PLAY
			,@FCSTEREO
			,@FCLOGO
			,@FSCH1
			,@FSCH2
			,@FSCH3
			,@FSCH4	
	END --WHILE
	CLOSE CSR
	select PAIR, TYPE 來源, PLAYOUT002 EIP單號, FSPROG_ID 節目編號, FNEPISODE 集數, FSARC_TYPE 影片規格, FCSMOKE 吸煙卡, FCALERTS 警示卡, FSINSERT_TAPE 制式版, FSINSERT_TAPE_NAME 客製版名稱, FSINSERT_TAPE_DUR 長度, FSTITLE 檔名總片頭, FSTITLE_NAME 檔名總片頭名稱, FSTITLE_DUR 長度, 
	FCPROG_SIDE_LABEL 節目測標, FCLIVE_SIDE_LABEL LIVE測標, FCTIME_SIDE_LABEL 時間測標, FCREPLAY 重播, FCRECORD_PLAY 錄影重播, FCSTEREO 立體聲, FCLOGO 頻道LOGO, FSCH1 雙語CH1, FSCH2 雙語CH2, FSCH3 雙語CH3, FSCH4 雙語CH4, 
	u.FSUSER_ChtName 建立者, p.FDCREATED_DATE 建立日期 from #diff_PLAYOUT2MAM p join TBUSERS u on p.FSCREATED_BY = u.FSUSER_ID order by p.PAIR ASC, p.TYPE ASC

GO -- END CREATE PROC COMPARE_PLAYOUT_MAM

CREATE PROC #COMPARE_PLAYOUT_MAM_ORG
AS

	DECLARE @pID varchar(8), @ep integer, @vType integer, @pair integer = 0, @pairCount integer = 0
	select * INTO #temp_PLAYOUT2MAM from TBPGM_INSERT_TAPE where 1=0 -- the scope of tempPLAYOUT is inside the procedure
	ALTER TABLE #temp_PLAYOUT2MAM
		add PLAYOUT002 varchar(20)

	select * INTO #diff_PLAYOUT2MAM from TBPGM_INSERT_TAPE where 1=0
	ALTER TABLE #diff_PLAYOUT2MAM
		add type char(3)

	ALTER TABLE #diff_PLAYOUT2MAM
		add PLAYOUT002 varchar(20)

	ALTER TABLE #diff_PLAYOUT2MAM
		add PAIR integer
		

	print 'CREATE #tempPLAYOUT'
	CREATE TABLE #tempPLAYOUT (
		playoutID varchar(20),
		programID varchar(8),
		prgset integer,
		spec integer
	)

	print 'CREATE #tempPLAYOUT1'
	CREATE TABLE #tempPLAYOUT1 (
		playoutID varchar(20),
		programID varchar(8),
		prgset integer,
		spec integer
	)


	print '列出有效的單號且根據單號排序，最新的在前面'
	INSERT #tempPLAYOUT(playoutID, programID, prgset, spec)
	SELECT playout002, programID, prgset, spec from [10.1.253.88].EF2KWeb.EZAdmin.playout as p
	inner join [10.1.253.88].[EF2KWeb].[dbo].[resda] as res 
	on p.PLAYOUT001 = res.resda001 and p.PLAYOUT002 = res.resda002 and res.resda021 = 2 order by playout002 DESC

	--select * from #tempPLAYOUT
	--select  distinct programID, prgset, spec from #tempPLAYOUT
	--select  distinct programID from #tempPLAYOUT
	--select programID, prgset, spec from #tempPLAYOUT group by  programID, prgset, spec

	--return
	
	print 'DECLARE tmpCSR'
	DECLARE tmpCSR CURSOR FOR select programID, prgset, spec from #tempPLAYOUT group by  programID, prgset, spec
	OPEN tmpCSR

	FETCH NEXT FROM tmpCSR INTO @pID, @ep, @vType
	
	print 'FETCH from tmpCSR in WHIEL LOOP'
	DECLARE @count1 integer = 0
	WHILE (@@FETCH_STATUS = 0) BEGIN
		--print 'Count = ' + CAST(@count1 AS varchar(10))
		INSERT #temp_PLAYOUT2MAM(PLAYOUT002, FSPROG_ID, FNEPISODE, FSARC_TYPE, FCSMOKE, FCALERTS, FSINSERT_TAPE, FSINSERT_TAPE_NAME, FSINSERT_TAPE_DUR, FSTITLE, FSTITLE_NAME, FSTITLE_DUR, FCPROG_SIDE_LABEL,
		FCLIVE_SIDE_LABEL, FCTIME_SIDE_LABEL, FCREPLAY, FCRECORD_PLAY, FCSTEREO, FCLOGO, FSCH1, FSCH2, FSCH3, FSCH4, FSCREATED_BY, FDCREATED_DATE)
		SELECT
			top 1
			playout002 as PLAYOUT002,
			programID as FSPROG_ID,
			prgset as FNEPISODE,

			(CASE spec
				WHEN 1 THEN '001'
				WHEN 2 THEN '002'
			END) as FSARC_TYPE,

			(CASE smoking
				WHEN  1 THEN 'Y'
				WHEN  2 THEN 'N'
			END) as FCSMOKE,

			(CASE alerts
				WHEN 1 THEN 'Y'
				WHEN 2 THEN 'N'
			END) as FCALERTS,

			(CASE alertsName
				WHEN 0 THEN 0
				WHEN 1 THEN 1
				WHEN 2 THEN 2
				WHEN 3 THEN 3
				WHEN 4 THEN 4
				WHEN 5 THEN 5
				WHEN 6 THEN 6
				WHEN 7 THEN 7
			END) as FSINSERT_TAPE,

			customName as FNINSERT_TAPE_NAME,
			customS as FSINSERT_TAPE_DUR,
			
			(CASE title
				WHEN 1 THEN 'Y'
				WHEN 2 THEN 'N'
			END) as FSTITLE,

			titleName as FSTITLE,
			titleS as FSTITLE_DUR,


			(CASE programSide
				WHEN 1 THEN 'Y'
				WHEN 2 THEN 'N'
			END) as FCPROG_SIDE_LABEL,

			(CASE liveSide
				WHEN 1 THEN 'Y'
				WHEN 2 THEN 'N'
			END) as FCLIVE_SIDE_LABEL,

			(CASE timeSide
				WHEN 1 THEN 'Y'
				WHEN 2 THEN 'N'
				ELSE NULL
			END) as FCTIME_SIDE_LABEL,

			(CASE replay
				WHEN 1 THEN 'Y'
				WHEN 2 THEN 'N'
			END) as FCREPLAY,

			(CASE video
				WHEN 1 THEN 'Y'
				WHEN 2 THEN 'N'
			END) as FCRECORD_PLAY,

			(CASE stereo
				WHEN 1 THEN 'Y'
				WHEN 2 THEN 'N'
			END) as FCSTEREO,

			(CASE logo
				WHEN '0' THEN 'N'
				WHEN 1 THEN 'Y'  --右邊
			END) as FCLOGO,
			
			(CASE 
				WHEN ch1 like '%韓%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '韓')
				WHEN ch1 like '%法%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '法')
				WHEN ch1 like '%英%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '英')
				WHEN ch1 like '%國%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '國')		
			END) as FSCH2,

					(CASE 
				WHEN ch2 like '%韓%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '韓')
				WHEN ch2 like '%法%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '法')
				WHEN ch2 like '%英%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '英')
				WHEN ch2 like '%國%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '國')			
			END) as FSCH2,

			(CASE 
				WHEN ch3 like '%韓%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '韓')
				WHEN ch3 like '%法%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '法')
				WHEN ch3 like '%英%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '英')
				WHEN ch3 like '%國%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '國')			
			END) as FSCH3,

			(CASE 
				WHEN ch4 like '%韓%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '韓')
				WHEN ch4 like '%法%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '法')
				WHEN ch4 like '%英%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '英')
				WHEN ch4 like '%國%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '國')		
			END) as FSCH4,
		
			'51204' as FSCREATED_BY,
			GETDATE() as FDCREATE_DATE

		FROM [10.1.253.88].EF2KWeb.EZAdmin.playout as p
		inner join [10.1.253.88].[EF2KWeb].[dbo].[resda] as res 
		on p.PLAYOUT001 = res.resda001 and p.PLAYOUT002 = res.resda002 and res.resda021 = 2 where programID = @pID and prgset = @ep and spec = @vType ORDER BY playout002 DESC

		FETCH NEXT FROM tmpCSR INTO @pID, @ep, @vType
		--set @count1 += 1

	END --WHILE

	CLOSE tmpCSR

	/* 
	 * 無法過濾出唯一的有效資料
	INSERT #temp_PLAYOUT2MAM(FSPROG_ID, FNEPISODE, FSARC_TYPE, FCSMOKE, FCALERTS, FSINSERT_TAPE, FSINSERT_TAPE_NAME, FSINSERT_TAPE_DUR, FSTITLE_NAME, FSTITLE_DUR, FCPROG_SIDE_LABEL, FCLIVE_SIDE_LABEL, FCREPLAY, FCRECORD_PLAY, FCSTEREO, FCLOGO, FSCH1, FSCH2, FSCH3, FSCH4, FSCREATED_BY, FDCREATED_DATE)
	SELECT
		programID as FSPROG_ID,
		prgset as FNEPISODE,

		(CASE spec
			WHEN 1 THEN '001'
			WHEN 2 THEN '002'
		END) as FSARC_TYPE,

		(CASE smoking
			WHEN  1 THEN 'Y'
			WHEN  2 THEN 'N'
		END) as FCSMOKE,

		(CASE alerts
			WHEN 1 THEN 'Y'
			WHEN 2 THEN 'N'
		END) as FCALERTS,

		(CASE alertsName
			WHEN 1 THEN 1
			WHEN 2 THEN 2
			WHEN 3 THEN 3
			WHEN 4 THEN 4
			WHEN 5 THEN 5
			WHEN 6 THEN 6
			WHEN 7 THEN 7
			ELSE 0
		END) as FSINSERT_TAPE,

		customName as FNINSERT_TAPE_NAME,
		customS as FSINSERT_TAPE_DUR,
		titleName as FSTITLE,
		titleS as FSTITLE_DUR,


		(CASE programSide
			WHEN 1 THEN 'Y'
			WHEN 2 THEN 'N'
		END) as FCPROG_SIDE_LABEL,

		(CASE liveSide
			WHEN 1 THEN 'Y'
			WHEN 2 THEN 'N'
		END) as FCLIVE_SIDE_LABEL,

		(CASE replay
			WHEN 1 THEN 'Y'
			WHEN 2 THEN 'N'
		END) as FCREPLAY,

		(CASE video
			WHEN 1 THEN 'Y'
			WHEN 2 THEN 'N'
		END) as FCRECORD_PLAY,

		(CASE stereo
			WHEN 1 THEN 'Y'
			WHEN 2 THEN 'N'
		END) as FCSTEREO,

		(CASE logo
			WHEN 1 THEN 'Y'
			WHEN 2 THEN 'N'
		END) as FCLOGO,
			
		(CASE 
			WHEN ch1 like '%韓%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '韓')
			WHEN ch1 like '%法%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '法')
			WHEN ch1 like '%英%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '英')
			WHEN ch1 like '%國%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '國')			
		END) as FSCH2,

				(CASE 
			WHEN ch2 like '%韓%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '韓')
			WHEN ch2 like '%法%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '法')
			WHEN ch2 like '%英%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '英')
			WHEN ch2 like '%國%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '國')			
		END) as FSCH2,

		(CASE 
			WHEN ch3 like '%韓%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '韓')
			WHEN ch3 like '%法%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '法')
			WHEN ch3 like '%英%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '英')
			WHEN ch3 like '%國%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '國')			
		END) as FSCH3,

		(CASE 
			WHEN ch4 like '%韓%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '韓')
			WHEN ch4 like '%法%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '法')
			WHEN ch4 like '%英%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '英')
			WHEN ch4 like '%國%' THEN (select FSPROGLANGID from TBZPROGLANG where FSPROGLANGNAME = '國')		
		END) as FSCH4,
		
		'51204' as FSCREATED_BY,
		GETDATE() as FDCREATE_DATE

	FROM [10.1.253.88].EF2KWeb.EZAdmin.playout as p
	inner join [10.1.253.88].[EF2KWeb].[dbo].[resda] as res 
	on p.PLAYOUT001 = res.resda001 and p.PLAYOUT002 = res.resda002 and res.resda021 = 2

	*/

	/*
	
	DECLARE PLAYOUTCSR CURSOR FOR 
		SELECT playout002, programID, prgset, spec from [10.1.253.88].EF2KWeb.EZAdmin.playout as p
		inner join [10.1.253.88].[EF2KWeb].[dbo].[resda] as res 
		on p.PLAYOUT001 = res.resda001 and p.PLAYOUT002 = res.resda002 and res.resda021 = 2 ORDER BY playout002 DESC

	OPEN PLAYOUTCSR

	FETCH NEXT FROM PLAYOUTCSR INTO
	*/

	--select * from #temp_PLAYOUT2MAM
	--select FSPROG_ID, FNEPISODE, FSARC_TYPE, count(FSPROG_ID) from #temp_PLAYOUT2MAM group by FSPROG_ID, FNEPISODE, FSARC_TYPE HAVING count(FSPROG_ID) > 1
	if 0=1 BEGIN -- for DEBUG
		if (exists (select count(FSPROG_ID) from #temp_PLAYOUT2MAM group by FSPROG_ID, FNEPISODE, FSARC_TYPE having count(FSPROG_ID) > 1) ) BEGIN
			RAISERROR('ERROR:Found duplicate playout ticket!', 0, 1) with NOWAIT
			select FSPROG_ID, FNEPISODE, FSARC_TYPE, count(FSPROG_ID) 數量 from #temp_PLAYOUT2MAM group by FSPROG_ID, FNEPISODE, FSARC_TYPE having count(FSPROG_ID) > 1
		END
		else
			select FSPROG_ID, FNEPISODE, FSARC_TYPE from #temp_PLAYOUT2MAM group by FSPROG_ID, FNEPISODE, FSARC_TYPE
		return
	END

	-- 建立 TBPGM_INSERT_TAPE CURSOR
	DECLARE CSR CURSOR FOR SELECT FSPROG_ID, FNEPISODE, FSARC_TYPE, FCSMOKE, FCALERTS, FSINSERT_TAPE, FSINSERT_TAPE_NAME, FSINSERT_TAPE_DUR, FSTITLE, FSTITLE_NAME, FSTITLE_DUR, FCPROG_SIDE_LABEL, 
	FCLIVE_SIDE_LABEL, FCTIME_SIDE_LABEL, FCREPLAY, FCRECORD_PLAY, FCSTEREO, FCLOGO, FSCH1, FSCH2, FSCH3, FSCH4 from TBPGM_INSERT_TAPE

	DECLARE 
	@FSPROG_ID char(7)
	,@FNEPISODE int
	,@FSARC_TYPE varchar(3)
	,@FCSMOKE char(1)
	,@FCALERTS char(1)
	,@FSINSERT_TAPE varchar(1)
	,@FSINSERT_TAPE_NAME varchar(50)
	,@FSINSERT_TAPE_DUR int
	,@FSTITLE char(1)
	,@FSTITLE_NAME varchar(50)
	,@FSTITLE_DUR int
	,@FCPROG_SIDE_LABEL char(1)
	,@FCLIVE_SIDE_LABEL char(1)
	,@FCTIME_SIDE_LABEL char(1)
	,@FCRECORD_PLAY char(1)
	,@FCREPLAY char(1)
	,@FCSTEREO char(1)
	,@FCLOGO char(1)
	,@FSCH1 char(2)
	,@FSCH2 char(2)
	,@FSCH3 char(2)
	,@FSCH4 char(2)

	DECLARE
	@PLAYOUT002 varchar(20)
	,@FSPROG_ID1 char(7)
	,@FNEPISODE1 int
	,@FSARC_TYPE1 varchar(3)
	,@FCSMOKE1 char(1)
	,@FCALERTS1 char(1)
	,@FSINSERT_TAPE1 varchar(1)
	,@FSINSERT_TAPE_NAME1 varchar(50)
	,@FSINSERT_TAPE_DUR1 int
	,@FSTITLE1 char(1)
	,@FSTITLE_NAME1 varchar(50)
	,@FSTITLE_DUR1 int
	,@FCPROG_SIDE_LABEL1 char(1)
	,@FCLIVE_SIDE_LABEL1 char(1)
	,@FCTIME_SIDE_LABEL1 char(1)
	,@FCREPLAY1 char(1)
	,@FCRECORD_PLAY1 char(1)
	,@FCSTEREO1 char(1)
	,@FCLOGO1 char(1)
	,@FSCH11 char(2)
	,@FSCH21 char(2)
	,@FSCH31 char(2)
	,@FSCH41 char(2)

	DECLARE @diff integer = 1, @count integer = 0

	OPEN CSR

	--讀取 TBPGM_INSERT_TAPE 存到暫存變數
	FETCH NEXT FROM CSR INTO 
	@FSPROG_ID
	,@FNEPISODE
	,@FSARC_TYPE
	,@FCSMOKE
	,@FCALERTS
	,@FSINSERT_TAPE
	,@FSINSERT_TAPE_NAME
	,@FSINSERT_TAPE_DUR
	,@FSTITLE
	,@FSTITLE_NAME
	,@FSTITLE_DUR
	,@FCPROG_SIDE_LABEL
	,@FCLIVE_SIDE_LABEL
	,@FCTIME_SIDE_LABEL
	,@FCREPLAY
	,@FCRECORD_PLAY
	,@FCSTEREO
	,@FCLOGO
	,@FSCH1
	,@FSCH2
	,@FSCH3
	,@FSCH4
	
	WHILE (@@FETCH_STATUS = 0) BEGIN
		if EXISTS( select FSPROG_ID from #temp_PLAYOUT2MAM where FSPROG_ID = @FSPROG_ID and FNEPISODE = @FNEPISODE and FSARC_TYPE = @FSARC_TYPE ) BEGIN --COMPARE need check number of records found
		/*
		if EXISTS(
			select 
			@FCSMOKE1 = FCSMOKE
			,@FCALERTS1 = FCALERTS
			,@FSINSERT_TAPE1 = FSINSERT_TAPE
			,@FSINSERT_TAPE_NAME1 = FSINSERT_TAPE_NAME
			,@FSINSERT_TAPE_DUR1 = FSINSERT_TAPE_DUR
			,@FSTITLE1 = FSTITLE
			,@FSTITLE_NAME1 = FSTITLE_NAME
			,@FSTITLE_DUR1 = FSTITLE_DUR
			,@FCPROG_SIDE_LABEL1 = FCPROG_SIDE_LABEL
			,@FCLIVE_SIDE_LABEL1 = FCLIVE_SIDE_LABEL
			,@FCTIME_SIDE_LABEL1 = FCTIME_SIDE_LABEL
			,@FCREPLAY1 = FCREPLAY
			,@FCRECORD_PLAY1 = FCRECORD_PLAY
			,@FCSTEREO1 = FCSTEREO
			,@FCLOGO1 = FCLOGO
			,@FSCH11 = FSCH1
			,@FSCH21 = FSCH2
			,@FSCH31 = FSCH3
			,@FSCH41 = FSCH4
			from #temp_PLAYOUT2MAM where FSPROG_ID = @FSPROG_ID and FNEPISODE = @FNEPISODE and FSARC_TYPE = @FSARC_TYPE
			) 
		BEGIN --COMPARE
		*/
			
			--print @count
			--set @count += 1
			set @pair = 1 -- Both eip and MAM has records
			select
			@PLAYOUT002 = PLAYOUT002
			,@FCSMOKE1 = FCSMOKE
			,@FCALERTS1 = FCALERTS
			,@FSINSERT_TAPE1 = FSINSERT_TAPE
			,@FSINSERT_TAPE_NAME1 = FSINSERT_TAPE_NAME
			,@FSINSERT_TAPE_DUR1 = FSINSERT_TAPE_DUR
			,@FSTITLE1 = FSTITLE
			,@FSTITLE_NAME1 = FSTITLE_NAME
			,@FSTITLE_DUR1 = FSTITLE_DUR
			,@FCPROG_SIDE_LABEL1 = FCPROG_SIDE_LABEL
			,@FCLIVE_SIDE_LABEL1 = FCLIVE_SIDE_LABEL
			,@FCTIME_SIDE_LABEL1 = FCTIME_SIDE_LABEL
			,@FCREPLAY1 = FCREPLAY
			,@FCRECORD_PLAY1 = FCRECORD_PLAY
			,@FCSTEREO1 = FCSTEREO
			,@FCLOGO1 = FCLOGO
			,@FSCH11 = FSCH1
			,@FSCH21 = FSCH2
			,@FSCH31 = FSCH3
			,@FSCH41 = FSCH4
			from #temp_PLAYOUT2MAM where FSPROG_ID = @FSPROG_ID and FNEPISODE = @FNEPISODE and FSARC_TYPE = @FSARC_TYPE

			-- COMPARE
			if @FCSMOKE1 != @FCSMOKE BEGIN
				set @diff = 1
			END
			if @FCALERTS1 != @FCALERTS1 BEGIN
				set @diff = 1
			END
			if @FSINSERT_TAPE1 != 0 AND @FSINSERT_TAPE !='' BEGIN
				if @FSINSERT_TAPE1 != @FSINSERT_TAPE
					set @diff = 1
			END
			if @FSINSERT_TAPE_NAME1 != @FSINSERT_TAPE_NAME BEGIN
				set @diff = 1
			END
			if @FSINSERT_TAPE_DUR1 != @FSINSERT_TAPE_DUR BEGIN
				set @diff = 1
			END
			if @FSTITLE1 != @FSTITLE BEGIN
				set @diff = 1
			END
			if @FSTITLE_NAME1 != @FSTITLE_NAME BEGIN
				set @diff = 1
			END
			if @FSTITLE_DUR1 != @FSTITLE_DUR BEGIN
				set @diff = 1
			END
			if @FCPROG_SIDE_LABEL1 != @FCPROG_SIDE_LABEL BEGIN
				set @diff = 1
			END
			if @FCLIVE_SIDE_LABEL1 != @FCLIVE_SIDE_LABEL BEGIN
				set @diff = 1
			END
			if @FCTIME_SIDE_LABEL1 != @FCTIME_SIDE_LABEL BEGIN
				set @diff = 1
			END
			if @FCREPLAY1 != @FCREPLAY BEGIN
				set @diff = 1
			END
			if @FCRECORD_PLAY1 != @FCRECORD_PLAY BEGIN
				set @diff = 1
			END
			if @FCSTEREO1 != @FCSTEREO BEGIN
				set @diff = 1
			END
			if @FCLOGO1 != @FCLOGO BEGIN
				set @diff = 1
			END
			/*
			if @FSCH11 != @FSCH1 BEGIN
				set @diff = 1
			END
			if @FSCH21 != @FSCH2 BEGIN
				set @diff = 1
			END
			if @FSCH31 != @FSCH3 BEGIN
				set @diff = 1
			END
			if @FSCH41 != @FSCH4 BEGIN
				set @diff = 1
			END
			*/
		END -- IF-CMPARE
		
		if @diff = 1 BEGIN --list different record, 只有 MAM 有，EIP 沒有也會列出
		        if @pair = 1
					set @pairCount += 1
				 
			insert into #diff_PLAYOUT2MAM(PAIR, TYPE, PLAYOUT002, FSPROG_ID, FNEPISODE, FSARC_TYPE, FCSMOKE, FCALERTS, FSINSERT_TAPE, FSINSERT_TAPE_NAME, FSINSERT_TAPE_DUR, FSTITLE, FSTITLE_NAME, FSTITLE_DUR, FCPROG_SIDE_LABEL, FCLIVE_SIDE_LABEL, FCTIME_SIDE_LABEL, FCREPLAY, FCRECORD_PLAY, FCSTEREO, FCLOGO, FSCH1, FSCH2, FSCH3, FSCH4, FSCREATED_BY, FDCREATED_DATE) 
			select  (CASE WHEN @pair = 0 THEN 0 ELSE @pairCount END) PAIR, 'EIP', PLAYOUT002, FSPROG_ID, FNEPISODE, FSARC_TYPE, FCSMOKE, FCALERTS, FSINSERT_TAPE, FSINSERT_TAPE_NAME, FSINSERT_TAPE_DUR, FSTITLE, FSTITLE_NAME, FSTITLE_DUR, FCPROG_SIDE_LABEL, FCLIVE_SIDE_LABEL, FCTIME_SIDE_LABEL, FCREPLAY, FCRECORD_PLAY, FCSTEREO, FCLOGO, FSCH1, FSCH2, FSCH3, FSCH4, FSCREATED_BY, FDCREATED_DATE
			from #temp_PLAYOUT2MAM where FSPROG_ID = @FSPROG_ID and FNEPISODE = @FNEPISODE and FSARC_TYPE = @FSARC_TYPE

			insert into #diff_PLAYOUT2MAM(PAIR, TYPE, PLAYOUT002, FSPROG_ID, FNEPISODE, FSARC_TYPE, FCSMOKE, FCALERTS, FSINSERT_TAPE, FSINSERT_TAPE_NAME, FSINSERT_TAPE_DUR, FSTITLE, FSTITLE_NAME, FSTITLE_DUR, FCPROG_SIDE_LABEL, FCLIVE_SIDE_LABEL, FCTIME_SIDE_LABEL, FCREPLAY, FCRECORD_PLAY, FCSTEREO, FCLOGO, FSCH1, FSCH2, FSCH3, FSCH4, FSCREATED_BY, FDCREATED_DATE)
			select (CASE WHEN @pair = 0 THEN 0 ELSE @pairCount END) PAIR, 'MAM', '', FSPROG_ID, FNEPISODE, FSARC_TYPE, FCSMOKE, FCALERTS, FSINSERT_TAPE, FSINSERT_TAPE_NAME, FSINSERT_TAPE_DUR, FSTITLE, FSTITLE_NAME, FSTITLE_DUR, FCPROG_SIDE_LABEL, FCLIVE_SIDE_LABEL, FCTIME_SIDE_LABEL, FCREPLAY, FCRECORD_PLAY, FCSTEREO, FCLOGO, FSCH1, FSCH2, FSCH3, FSCH4, FSCREATED_BY, FDCREATED_DATE
			from TBPGM_INSERT_TAPE where FSPROG_ID = @FSPROG_ID and FNEPISODE = @FNEPISODE and FSARC_TYPE = @FSARC_TYPE

			--print 'found diff on FSPROG_ID=' + @FSPROG_ID + 'FNEPISODE=' + CAST(@FNEPISODE as varchar(4)) + 'FSARC_TYPE=' + @FSARC_TYPE
			
			set @diff = 0
			set @pair = 0
		END --if
		
		FETCH NEXT FROM CSR INTO
			@FSPROG_ID
			,@FNEPISODE
			,@FSARC_TYPE
			,@FCSMOKE
			,@FCALERTS
			,@FSINSERT_TAPE
			,@FSINSERT_TAPE_NAME
			,@FSINSERT_TAPE_DUR
			,@FSTITLE
			,@FSTITLE_NAME
			,@FSTITLE_DUR
			,@FCPROG_SIDE_LABEL
			,@FCLIVE_SIDE_LABEL
			,@FCTIME_SIDE_LABEL
			,@FCREPLAY
			,@FCRECORD_PLAY
			,@FCSTEREO
			,@FCLOGO
			,@FSCH1
			,@FSCH2
			,@FSCH3
			,@FSCH4	
	END --WHILE
	CLOSE CSR
	select PAIR, TYPE 來源, PLAYOUT002 EIP單號, FSPROG_ID 節目編號, FNEPISODE 集數, FSARC_TYPE 影片規格, FCSMOKE 吸煙卡, FCALERTS 警示卡, FSINSERT_TAPE 制式版, FSINSERT_TAPE_NAME 客製版名稱, FSINSERT_TAPE_DUR 長度, FSTITLE 檔名總片頭, FSTITLE_NAME 檔名總片頭名稱, FSTITLE_DUR 長度, 
	FCPROG_SIDE_LABEL 節目測標, FCLIVE_SIDE_LABEL LIVE測標, FCTIME_SIDE_LABEL 時間測標, FCREPLAY 重播, FCRECORD_PLAY 錄影重播, FCSTEREO 立體聲, FCLOGO 頻道LOGO, FSCH1 雙語CH1, FSCH2 雙語CH2, FSCH3 雙語CH3, FSCH4 雙語CH4, 
	FSCREATED_BY 建立者, FDCREATED_DATE 建立日期 from #diff_PLAYOUT2MAM

GO -- END CREATE PROC COMPARE_PLAYOUT_MAM




CREATE PROC #TEST_CASE
	@case integer, @subcase integer, @programID varchar(10), @episode integer, @PLAYOUT_ID varchar(10)
AS
if @case = 1 BEGIN -- basic query for PLAYOUT
	/*
	 * 電子表單 PLAYOUT
	 */
	if @subcase = 1
		select * from [10.1.253.88].EF2KWeb.EZAdmin.playout
	else if @subcase = 2 BEGIN
		select * from [10.1.253.88].EF2KWeb.EZAdmin.playout where programID = @programID -- 根據節目編號查詢
		select * from [10.1.253.88].EF2KWeb.EZAdmin.playout where programID = '2012457'  --飛虎傳奇(客語版)
		select * from [10.1.253.88].EF2KWeb.EZAdmin.playout where programID = '2016079'  --飛虎傳奇
	END
	else if @subcase = 3
		select * from [10.1.253.88].EF2KWeb.EZAdmin.playout where spec = 2 -- 1:SD 2:HD
	else if @subcase = 4 -- 查詢雙語的所有項目
		select ch1, ch2, ch3, ch4, count(ch4) 筆數 from [10.1.253.88].EF2KWeb.EZAdmin.playout group by ch1, ch2, ch3, ch4
		--select ch1, count(ch1) #ch1, ch2, count(ch2) #ch2, ch3, count(ch3) #ch3, ch4, count(ch4) #ch4 from [10.1.253.88].EF2KWeb.EZAdmin.playout group by ch1, ch2, ch3, ch4
	else if @subcase = 5 BEGIN -- 查詢雙語的所有項目MAM, PLAYOUT
		--select DISTINCT(ch1)  collate Chinese_Taiwan_Stroke_CS_AS from [10.1.253.88].EF2KWeb.EZAdmin.playout  -- 67筆
		--select DISTINCT(ch1)  collate Chinese_Taiwan_Stroke_CI_AS from [10.1.253.88].EF2KWeb.EZAdmin.playout -- 58 筆
		--select DISTINCT(ch1) from [10.1.253.88].EF2KWeb.EZAdmin.playout --collate Chinese_Taiwan_Stroke_BIN 69筆
		-- error select DISTINCT(ch1) from [10.1.253.88].EF2KWeb.EZAdmin.playout collate Chinese_Taiwan_Stroke_CS_AS
	
		--select ch1, count(ch1 ) # from [10.1.253.88].EF2KWeb.EZAdmin.playout group by ch1
		DECLARE @LANGTABLE TABLE(
			langName varchar(30)
		)

		INSERT @LANGTABLE select DISTINCT(ch1) from [10.1.253.88].EF2KWeb.EZAdmin.playout
		INSERT @LANGTABLE select DISTINCT(ch2) from [10.1.253.88].EF2KWeb.EZAdmin.playout
		INSERT @LANGTABLE select DISTINCT(ch3) from [10.1.253.88].EF2KWeb.EZAdmin.playout
		INSERT @LANGTABLE select DISTINCT(ch4) from [10.1.253.88].EF2KWeb.EZAdmin.playout

		--select langName from @LANGTABLE
		--select langName, count(langName) # from @langTable group by langName
		select DISTINCT(langName) collate Chinese_Taiwan_Stroke_BIN from @LANGTABLE
		select * from TBZPROGLANG
		--select DISTINCT(langName) from @LANGTABLE
		--select * from @LANGTABLE where langName = '中MiX'
		--select (CASE WHEN '中MIX' = '中Mixx' THEN  2 END)
	END
	else if @subcase = 6 BEGIN
		select DISTINCT(ch1) collate Chinese_Taiwan_Stroke_CS_AS langName into #cs from [10.1.253.88].EF2KWeb.EZAdmin.playout  -- 67筆
		select DISTINCT(ch1) langName into #bin from [10.1.253.88].EF2KWeb.EZAdmin.playout --collate Chinese_Taiwan_Stroke_BIN69筆
		select langName from #cs where langName not in (select langName collate Chinese_Taiwan_Stroke_CS_AS from #bin)
		select langName from #bin where langName not in (select langName collate Chinese_Taiwan_Stroke_BIN from #cs)
	END
	
	else if @subcase = 7 BEGIN -- 查詢所有有效的表單，包含舊的
		select * from [10.1.253.88].EF2KWeb.EZAdmin.playout as p inner join [10.1.253.88].[EF2KWeb].[dbo].[resda] as c on p.PLAYOUT001 = c.resda001 and p.PLAYOUT002 = c.resda002 and c.resda021 = 2 
		where (@programID = '' or p.programID = @programID) and (@episode = 0 or p.prgset = @episode)
		order by p.PLAYOUT002 DESC
	END

	else if @subcase = 8 BEGIN --查詢有效單號筆數
		select count(programID), programId, prgset, spec from [10.1.253.88].EF2KWeb.EZAdmin.playout as p inner join [10.1.253.88].[EF2KWeb].[dbo].[resda] as c on p.PLAYOUT001 = c.resda001 and p.PLAYOUT002 = c.resda002 and c.resda021 = 2 
		group by programID, prgset, spec
	END

	else if @subcase = 9 BEGIN -- 查詢所有有效的表單，包含舊的
		select * from [10.1.253.88].EF2KWeb.EZAdmin.playout as p inner join [10.1.253.88].[EF2KWeb].[dbo].[resda] as c on p.PLAYOUT001 = c.resda001 and p.PLAYOUT002 = c.resda002 and c.resda021 = 2 
		where  p.programID = '2012457' and p.prgset = 1
		order by p.PLAYOUT002 DESC
	END
		
END --CASE1

else if @case = 2 BEGIN -- 根據節目編號和集數查詢 playout

	select
	playout002 單號,
	programID 節目編碼,
	(select pgm.FSPGMNAME from TBPROG_M pgm where pgm.FSPROG_ID = programID collate Chinese_Taiwan_Stroke_CI_AS) 節目名稱,
	prgset 集數,
	(CASE 
		WHEN spec=1 THEN 'SD'
		WHEN spec=2 THEN 'HD'
		ELSE 'N/A'
	END) 規格,

	(CASE
		WHEN smoking=1 THEN '要'
		WHEN smoking=2 THEN '不要'
		ELSE 'N/A'
	END) 吸煙卡,

	(CASE
		WHEN alerts=1 THEN '要'
		WHEN alerts=2 THEN '不要'
		ELSE 'N/A'
	END) 警示卡,
	(CASE
		WHEN alertsName=0 THEN ''
		WHEN alertsName=1 THEN '制式版1'
		WHEN alertsName=2 THEN '制式版2'
		WHEN alertsName=3 THEN '制式版3'
		WHEN alertsName=4 THEN '制式版4'
		WHEN alertsName=5 THEN '制式版5'
		WHEN alertsName=6 THEN '制式版6'
		WHEN alertsName=7 THEN '客製版'
		ELSE 'N/A'
	END) 制式版,
	customName 警示卡名稱,
	customS 警示卡長度,

	(CASE
		WHEN title=1 THEN '要'
		WHEN title=2 THEN '不要'
		ELSE 'N/A'
	END) 檔名總片頭,

	titleName 檔名總片頭名稱,
	titleS 檔名總片頭長度,

	(CASE
		WHEN programSide=1 THEN '要'
		WHEN programSide=2 THEN '不要'
		ELSE 'N/A'
	END) 節目側標,

	(CASE
		WHEN liveSide=1 THEN '要'
		WHEN liveSide=2 THEN '不要'
		ELSE 'N/A'
	END) LIVE側標,

	(CASE
		WHEN timeSide=1 THEN '要'
		WHEN timeSide=2 THEN '不要'
		ELSE 'N/A'
	END) 時間側標,

	(CASE
		WHEN replay=1 THEN '要'
		WHEN replay=2 THEN '不要'
		ELSE 'N/A'
	END) 重播,

	(CASE
		WHEN video=1 THEN '要'
		WHEN video=2 THEN '不要'
		ELSE 'N/A'
	END) 錄影轉播,

	(CASE
		WHEN stereo=1 THEN '要'
		WHEN stereo=2 THEN '不要'
		ELSE 'N/A'
	END) 立體聲,

	(CASE 
		WHEN logo=0 THEN '不要'
		WHEN logo=1 THEN '右邊'
		ELSE 'N/A'
	END) 公視LOGO,

	ch1 雙語1,
	ch2 雙語2,
	ch3 雙語3,
	ch4 雙語4
	from [10.1.253.88].EF2KWeb.EZAdmin.playout as p where (@programID = '' or programID = @programID) and (@episode = '' or prgset = @episode) ORDER BY playout002 DESC -- 2010336

END -- CASE 2

else if @case = 3 BEGIN --bacic query for MAM TBPGM_INSERT_TAPE
	/*
	 * MAM
	 */
	if @subcase = 1 --查詢所有主控播出資訊
		select * from TBPGM_INSERT_TAPE
	else if @subcase = 2 --查詢所有主控播出資訊筆數
		select count(FSPROG_ID) from TBPGM_INSERT_TAPE
	else if @subcase = 3 --根據建檔者查詢
		select * from TBPGM_INSERT_TAPE where FSCREATED_BY=51204
	else if @subcase = 4 --根據節目編號、集數、檔案類型查詢
		select (select pgm.FSPGMNAME from TBPROG_M pgm where pgm.FSPROG_ID = tb.FSPROG_ID) as 節目名稱, * from TBPGM_INSERT_TAPE tb where FSPROG_ID='2012002' and FNEPISODE = 1 AND FSARC_TYPE = '001'
	else if @subcase = 5 -- 
		select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='TBPGM_INSERT_TAPE'
	else if @subcase = 6 -- 查詢雙語代碼
		select * from TBZPROGLANG

END --CASE3

else if @case = 4 BEGIN -- 查詢MAM主控資訊表


	select
	FSPROG_ID 節目編碼,
	(select pgm.FSPGMNAME from TBPROG_M pgm where pgm.FSPROG_ID = i.FSPROG_ID) 節目名稱,
	FNEPISODE 集數, 
	(CASE 
		WHEN i.FSARC_TYPE='001' THEN 'SD'
		WHEN i.FSARC_TYPE='002' THEN 'HD'
		ELSE 'N/A'
	END) 規格, 

	(CASE
		WHEN i.FCSMOKE='Y' THEN '要'
		WHEN i.FCSMOKE='N' THEN '不要'
		ELSE 'N/A'
	END) 吸煙卡,
	(CASE
		WHEN i.FCALERTS='Y' THEN '要'
		WHEN i.FCALERTS='N' THEN '不要'
		ELSE 'N/A'
	END) 警示卡,
	(CASE
		WHEN i.FSINSERT_TAPE=0 THEN ''
		WHEN i.FSINSERT_TAPE=1 THEN '制式版1'
		WHEN i.FSINSERT_TAPE=2 THEN '制式版2'
		WHEN i.FSINSERT_TAPE=3 THEN '制式版3'
		WHEN i.FSINSERT_TAPE=4 THEN '制式版4'
		WHEN i.FSINSERT_TAPE=5 THEN '制式版5'
		WHEN i.FSINSERT_TAPE=6 THEN '制式版6'
		WHEN i.FSINSERT_TAPE=7 THEN '客製版'
		ELSE 'N/A'
	END) 制式版,
	i.FSINSERT_TAPE_NAME 警示卡名稱,
	i.FSINSERT_TAPE_DUR 警示卡長度,

	(CASE
		WHEN i.FSTITLE='Y' THEN '要'
		WHEN i.FSTITLE='N' THEN '不要'
		ELSE 'N/A'
	END) 檔名總片頭,

	i.FSTITLE_NAME 檔名總片頭名稱,
	i.FSTITLE_DUR 檔名總片頭長度,
	(CASE
		WHEN i.FCPROG_SIDE_LABEL='Y' THEN '要'
		WHEN i.FCPROG_SIDE_LABEL='N' THEN '不要'
		ELSE 'N/A'
	END) 節目側標,
	(CASE
		WHEN i.FCLIVE_SIDE_LABEL='Y' THEN '要'
		WHEN i.FCLIVE_SIDE_LABEL='N' THEN '不要'
		ELSE 'N/A'
	END) LIVE側標,
	(CASE
		WHEN i.FCTIME_SIDE_LABEL='Y' THEN '要'
		WHEN i.FCTIME_SIDE_LABEL='N' THEN '不要'
		ELSE 'N/A'
	END) 時間側標,
	(CASE
		WHEN i.FCREPLAY='Y' THEN '要'
		WHEN i.FCREPLAY='N' THEN '不要'
		ELSE 'N/A'
	END) 重播,
	(CASE
		WHEN i.FCRECORD_PLAY='Y' THEN '要'
		WHEN i.FCRECORD_PLAY='N' THEN '不要'
		ELSE 'N/A'
	END) 錄影轉播,
	(CASE
		WHEN i.FCSTEREO='Y' THEN '要'
		WHEN i.FCSTEREO='N' THEN '不要'
		ELSE 'N/A'
	END) 立體聲,
	(CASE 
		WHEN i.FCLOGO='R' THEN '右邊'
		WHEN i.FCLOGO='L' THEN '左邊'
		WHEN i.FCLOGO='N' THEN '不要'
		ELSE 'N/A'
	END) 公視LOGO,

	(select lan.FSPROGLANGNAME from TBZPROGLANG lan where lan.FSPROGLANGID=i.FSCH1) 雙語1,
	(select lan.FSPROGLANGNAME from TBZPROGLANG lan where lan.FSPROGLANGID=i.FSCH2) 雙語2,
	(select lan.FSPROGLANGNAME from TBZPROGLANG lan where lan.FSPROGLANGID=i.FSCH3) 雙語3,
	(select lan.FSPROGLANGNAME from TBZPROGLANG lan where lan.FSPROGLANGID=i.FSCH4) 雙語4
	from TBPGM_INSERT_TAPE as i  where FSPROG_ID = @programID and FNEPISODE = @episode
END --CASE4

else if @case = 5 BEGIN
	exec SP_Q_TBPGM_INSERT_TAPE '0000001', 1, '001'
END

else if @case = 6 BEGIN
	-- 根據節目編號和集數查詢
	Select
	A.FSPROG_ID ,A.FNEPISODE,A.FSARC_TYPE, A.FCSMOKE ,A.FCALERTS,
	A.FSINSERT_TAPE ,A.FSINSERT_TAPE_NAME,A.FSINSERT_TAPE_DUR,
	A.FSTITLE ,A.FSTITLE_NAME,A.FSTITLE_DUR, A.FCPROG_SIDE_LABEL ,
	A.FCLIVE_SIDE_LABEL ,A.FCTIME_SIDE_LABEL,A.FCREPLAY,
	A.FCRECORD_PLAY ,A.FCSTEREO,A.FCLOGO, A.FSCH1 ,A.FSCH2,
	A.FSCH3 ,A.FSCH4,A.FSCREATED_BY, B.FSPROGLANGNAME FSCHNAME1 ,
	C.FSPROGLANGNAME FSCHNAME2 ,D.FSPROGLANGNAME FSCHNAME3,E.FSPROGLANGNAME FSCHNAME4

	from TBPGM_INSERT_TAPE A left join TBZPROGLANG B on A.FSCH1=B.FSPROGLANGID
		 left join   TBZPROGLANG C on A.FSCH2= C.FSPROGLANGID
		 left join   TBZPROGLANG D on A.FSCH3= D.FSPROGLANGID
		 left join   TBZPROGLANG E on A.FSCH4= E.FSPROGLANGID
	where FSPROG_ID='2010336' and FNEPISODE=1

END --CASE6
if @case = 7 BEGIN
	/*
	 * JOIN PLAYOUT and TBPGM_INSERT_TAPE
	 */

	select po.playout002, tb.* from TBPGM_INSERT_TAPE as tb LEFT OUTER JOIN [10.1.253.88].EF2KWeb.EZAdmin.playout as po 
	on po.programID  collate Chinese_Taiwan_Stroke_CI_AS = tb.FSPROG_ID and tb.FNEPISODE = po.prgset
	AND CAST(CASE
			WHEN tb.FSARC_TYPE='001' AND po.spec = 1 THEN 1
			WHEN tb.FSARC_TYPE='002' AND po.spec = 2 THEN 1
			ELSE 0
		END as bit) = 1
	 ORDER BY tb.FSPROG_ID
END --CASE7

else if @case = 8 BEGIN
	select * from TBPGM_INSERT_TAPE where FSCREATED_BY='admin'
	select FDCREATED_DATE, COUNT(FSPROG_ID) from TBPGM_INSERT_TAPE GROUP BY FDCREATED_DATE HAVING COUNT(FSPROG_ID) > 1
	select FDCREATED_DATE, COUNT(FDCREATED_DATE) from TBPGM_INSERT_TAPE GROUP BY FDCREATED_DATE HAVING COUNT(FDCREATED_DATE) > 1
	select FDCREATED_DATE, COUNT(FDCREATED_DATE), FSCREATED_BY, COUNT(FSCREATED_BY) from TBPGM_INSERT_TAPE GROUP BY FDCREATED_DATE, FSCREATED_BY HAVING COUNT(FDCREATED_DATE) > 1
	select FDCREATED_DATE, COUNT(FDCREATED_DATE), FSCREATED_BY, COUNT(FSCREATED_BY) from TBPGM_INSERT_TAPE GROUP BY FDCREATED_DATE, FSCREATED_BY HAVING COUNT(FSCREATED_BY) > 1
	select * from TBZPROGLANG
END --CASE8

else if @case = 9 BEGIN

	select FSPROG_ID, FNEPISODE, count(FSPROG_ID) from TBPGM_INSERT_TAPE as tb LEFT OUTER JOIN [10.1.253.88].EF2KWeb.EZAdmin.playout as po 
	on po.programID  collate Chinese_Taiwan_Stroke_CI_AS = tb.FSPROG_ID and tb.FNEPISODE = po.prgset
	AND CAST(CASE
			WHEN tb.FSARC_TYPE='001' AND po.spec = 1 THEN 1
			WHEN tb.FSARC_TYPE='002' AND po.spec = 2 THEN 1
			ELSE 0
		END as bit) = 1
	GROUP BY FSPROG_ID, FNEPISODE
	HAVING COUNT(FSPROG_ID) > 1

END --CASE9

else if @case = 10 BEGIN
	select po.playout002, tb.* from TBPGM_INSERT_TAPE as tb LEFT OUTER JOIN [10.1.253.88].EF2KWeb.EZAdmin.playout as po 
	on po.programID  collate Chinese_Taiwan_Stroke_CI_AS = tb.FSPROG_ID and tb.FNEPISODE = po.prgset
	AND CAST(1 as bit) = 1
	ORDER BY tb.FSPROG_ID
END --CASE10

else if @case = 11 BEGIN
	select * from TBPGM_INSERT_TAPE as i LEFT OUTER JOIN TBZPROGLANG as lan on i.FSCH1 = lan.FSPROGLANGID where FSPROG_ID = '2010336' and FNEPISODE=1
	select i.*, lan.FSPROGLANGNAME from TBPGM_INSERT_TAPE as i LEFT OUTER JOIN TBZPROGLANG as lan on i.FSCH1 = lan.FSPROGLANGID
	select i.*, lan.FSPROGLANGNAME from TBPGM_INSERT_TAPE as i, TBZPROGLANG as lan where i.FSCH1 = lan.FSPROGLANGID
END --CASE11

else if @case = 12 BEGIN
	Select A.*, B.FSPROGLANGNAME FSCHNAME1
	from TBPGM_INSERT_TAPE A left join TBZPROGLANG B on A.FSCH1=B.FSPROGLANGID

	-- inner join
	Select A.*, B.FSPROGLANGNAME FSCHNAME1
	from TBPGM_INSERT_TAPE A join TBZPROGLANG B on A.FSCH1=B.FSPROGLANGID


	-- cross product #A * #B
	Select A.*, B.FSPROGLANGNAME FSCHNAME1
	from TBPGM_INSERT_TAPE A, TBZPROGLANG B
END --CASE12

else if @case = 13 BEGIN -- 比較 Playout and TBPGM_INSERT_TAPE 資料

	select
	playout002 單號,
	programID 節目編碼,
	(select pgm.FSPGMNAME from TBPROG_M pgm where pgm.FSPROG_ID = programID collate Chinese_Taiwan_Stroke_CI_AS) 節目名稱,
	prgset 集數,
	(CASE 
		WHEN spec=1 THEN 'SD'
		WHEN spec=2 THEN 'HD'
		ELSE 'N/A'
	END) 規格,

	(CASE
		WHEN smoking=1 THEN '要'
		WHEN smoking=2 THEN '不要'
		ELSE 'N/A'
	END) 吸煙卡,

	(CASE
		WHEN alerts=1 THEN '要'
		WHEN alerts=2 THEN '不要'
		ELSE 'N/A'
	END) 警示卡,
	(CASE
		WHEN alertsName=0 THEN ''
		WHEN alertsName=1 THEN '制式版1'
		WHEN alertsName=2 THEN '制式版2'
		WHEN alertsName=3 THEN '制式版3'
		WHEN alertsName=4 THEN '制式版4'
		WHEN alertsName=5 THEN '制式版5'
		WHEN alertsName=6 THEN '制式版6'
		WHEN alertsName=7 THEN '客製版'
		ELSE 'N/A'
	END) 制式版,
	customName 警示卡名稱,
	customS 警示卡長度,

	(CASE
		WHEN title=1 THEN '要'
		WHEN title=2 THEN '不要'
		ELSE 'N/A'
	END) 檔名總片頭,

	titleName 檔名總片頭名稱,
	titleS 檔名總片頭長度,

	(CASE
		WHEN programSide=1 THEN '要'
		WHEN programSide=2 THEN '不要'
		ELSE 'N/A'
	END) 節目側標,

	(CASE
		WHEN liveSide=1 THEN '要'
		WHEN liveSide=2 THEN '不要'
		ELSE 'N/A'
	END) LIVE側標,

	(CASE
		WHEN timeSide=1 THEN '要'
		WHEN timeSide=2 THEN '不要'
		ELSE 'N/A'
	END) 時間側標,

	(CASE
		WHEN replay=1 THEN '要'
		WHEN replay=2 THEN '不要'
		ELSE 'N/A'
	END) 重播,

	(CASE
		WHEN video=1 THEN '要'
		WHEN video=2 THEN '不要'
		ELSE 'N/A'
	END) 錄影轉播,

	(CASE
		WHEN stereo=1 THEN '要'
		WHEN stereo=2 THEN '不要'
		ELSE 'N/A'
	END) 立體聲,

	(CASE 
		WHEN logo=0 THEN '不要'
		WHEN logo=1 THEN '右邊'
		ELSE 'N/A'
	END) 公視LOGO,

	ch1 雙語1,
	ch2 雙語2,
	ch3 雙語3,
	ch4 雙語4
	from [10.1.253.88].EF2KWeb.EZAdmin.playout as p 
	inner join [10.1.253.88].[EF2KWeb].[dbo].[resda] as c on p.PLAYOUT001 = c.resda001 and p.PLAYOUT002 = c.resda002 and c.resda021 = 2 -- 過濾有效單號
	where programID = @programID and prgset = @episode order by 單號

	/*
	 * MAM record
	 */
	select
	FSPROG_ID 節目編碼,
	(select pgm.FSPGMNAME from TBPROG_M pgm where pgm.FSPROG_ID = i.FSPROG_ID) 節目名稱,
	FNEPISODE 集數, 
	(CASE 
		WHEN i.FSARC_TYPE='001' THEN 'SD'
		WHEN i.FSARC_TYPE='002' THEN 'HD'
		ELSE 'N/A'
	END) 規格, 

	(CASE
		WHEN i.FCSMOKE='Y' THEN '要'
		WHEN i.FCSMOKE='N' THEN '不要'
		ELSE 'N/A'
	END) 吸煙卡,
	(CASE
		WHEN i.FCALERTS='Y' THEN '要'
		WHEN i.FCALERTS='N' THEN '不要'
		ELSE 'N/A'
	END) 警示卡,
	(CASE
		WHEN i.FSINSERT_TAPE=0 THEN ''
		WHEN i.FSINSERT_TAPE=1 THEN '制式版1'
		WHEN i.FSINSERT_TAPE=2 THEN '制式版2'
		WHEN i.FSINSERT_TAPE=3 THEN '制式版3'
		WHEN i.FSINSERT_TAPE=4 THEN '制式版4'
		WHEN i.FSINSERT_TAPE=5 THEN '制式版5'
		WHEN i.FSINSERT_TAPE=6 THEN '制式版6'
		WHEN i.FSINSERT_TAPE=7 THEN '客製版'
		ELSE 'N/A'
	END) 制式版,
	i.FSINSERT_TAPE_NAME 警示卡名稱,
	i.FSINSERT_TAPE_DUR 警示卡長度,

	(CASE
		WHEN i.FSTITLE='Y' THEN '要'
		WHEN i.FSTITLE='N' THEN '不要'
		ELSE 'N/A'
	END) 檔名總片頭,

	i.FSTITLE_NAME 檔名總片頭名稱,
	i.FSTITLE_DUR 檔名總片頭長度,
	(CASE
		WHEN i.FCPROG_SIDE_LABEL='Y' THEN '要'
		WHEN i.FCPROG_SIDE_LABEL='N' THEN '不要'
		ELSE 'N/A'
	END) 節目側標,
	(CASE
		WHEN i.FCLIVE_SIDE_LABEL='Y' THEN '要'
		WHEN i.FCLIVE_SIDE_LABEL='N' THEN '不要'
		ELSE 'N/A'
	END) LIVE側標,
	(CASE
		WHEN i.FCTIME_SIDE_LABEL='Y' THEN '要'
		WHEN i.FCTIME_SIDE_LABEL='N' THEN '不要'
		ELSE 'N/A'
	END) 時間側標,
	(CASE
		WHEN i.FCREPLAY='Y' THEN '要'
		WHEN i.FCREPLAY='N' THEN '不要'
		ELSE 'N/A'
	END) 重播,
	(CASE
		WHEN i.FCRECORD_PLAY='Y' THEN '要'
		WHEN i.FCRECORD_PLAY='N' THEN '不要'
		ELSE 'N/A'
	END) 錄影轉播,
	(CASE
		WHEN i.FCSTEREO='Y' THEN '要'
		WHEN i.FCSTEREO='N' THEN '不要'
		ELSE 'N/A'
	END) 立體聲,
	(CASE 
		WHEN i.FCLOGO='R' THEN '右邊'
		WHEN i.FCLOGO='L' THEN '左邊'
		WHEN i.FCLOGO='N' THEN '不要'
		ELSE 'N/A'
	END) 公視LOGO,

	(select lan.FSPROGLANGNAME from TBZPROGLANG lan where lan.FSPROGLANGID=i.FSCH1) 雙語1,
	(select lan.FSPROGLANGNAME from TBZPROGLANG lan where lan.FSPROGLANGID=i.FSCH2) 雙語2,
	(select lan.FSPROGLANGNAME from TBZPROGLANG lan where lan.FSPROGLANGID=i.FSCH3) 雙語3,
	(select lan.FSPROGLANGNAME from TBZPROGLANG lan where lan.FSPROGLANGID=i.FSCH4) 雙語4
	from TBPGM_INSERT_TAPE as i  where FSPROG_ID = @programID and FNEPISODE = @episode
END --CASE13

else if @case = 14 BEGIN --輸出PLAYOUT 和 TBPGM_INSERT_TAPE 有差異的部份, 根據TBPGM_INSERT_TAPE 每一筆資料去比對 PLAYOUT 資料，MAM 有而PLAYOUT 無的資料也會列出來
	if @subcase = 1
		EXEC #COMPARE_PLAYOUT_MAM
	else if @subcase = 2
		EXEC #COMPARE_PLAYOUT_MAM_20151124

END

if @case = 15 BEGIN -- http://event.pts.org.tw/ptsplayout/
	if @subcase = 1 BEGIN  --FAIL
		SELECT Distinct a.[FSPROGID],a.[FSWEBNAME],a.[FSPGMNAME],a.[FSPROGOBJNAME],a.[FSDEPTNAME],a.[FSPROGSRCNAME],a.[FNTOTEPISODE], 
			CASE 
				when isnull(b.[playout001], '*') ='*' then '' 
				else 'Y' 
			end as pCheck,
			(Case 
				when b.spec = 1 then 'SD'
				else 'HD' 
			end) as SpecDesc,
			b.spec
		FROM [PTSProgram].[dbo].[vwTBSProgInfo] as a     -- cannot access vwTBSProgInfo
		inner join  (
			select b.* from [10.1.253.88].[EF2KWeb].[EZAdmin].[playout] as b
			inner join [10.1.253.88].[EF2KWeb].[dbo].[resda] as c on b.PLAYOUT001=c.resda001 and b.PLAYOUT002=c.resda002  and c.resda021=2
		) as b 
		on a.[FSPROGID] = b.[programID] COLLATE Chinese_Taiwan_Stroke_CI_AS 
		where 1=1 and a.[FSWEBNAME] like '%飛虎傳奇%' or a.[FSPGMNAME] like '%飛虎傳奇%' and a.[FSPROGID] = '0000001';
	END
	else if @subcase = 2 BEGIN --WORK，根據節目名稱關鍵字查詢
		SELECT Distinct a.[FSPROG_ID], a.[FSWEBNAME], a.[FSPGMNAME], a.[FSPROGOBJID], a.[FSPRDDEPTID], a.[FSPROGSRCID], a.[FNTOTEPISODE], 
			CASE 
				when isnull(b.[playout001], '*') ='*' then '' 
				else 'Y' 
			end as pCheck,
			(Case 
				when b.spec = 1 then 'SD'
				else 'HD' 
			end) as SpecDesc,
			b.spec
		FROM TBPROG_M as a     -- cannot access vwTBSProgInfo
		inner join  (
			select b.* from [10.1.253.88].[EF2KWeb].[EZAdmin].[playout] as b
			inner join [10.1.253.88].[EF2KWeb].[dbo].[resda] as c on b.PLAYOUT001=c.resda001 and b.PLAYOUT002=c.resda002  and c.resda021=2
		) as b 
		on a.[FSPROG_ID] = b.[programID] COLLATE Chinese_Taiwan_Stroke_CI_AS 
		where 1=1 and a.[FSWEBNAME] like '%飛虎傳奇%' or a.[FSPGMNAME] like '%飛虎傳奇%' and a.[FSPROG_ID] = '0000001';
	END --SUBCASE 2

	else if @subcase = 3 BEGIN --WORK，根據節目名稱關鍵字查詢
		SELECT Distinct a.[FSPROG_ID], a.[FSWEBNAME], a.[FSPGMNAME], a.[FSPROGOBJID], a.[FSPRDDEPTID], a.[FSPROGSRCID], a.[FNTOTEPISODE], 
			CASE 
				when isnull(b.[playout001], '*') ='*' then '' 
				else 'Y' 
			end as pCheck,
			(Case 
				when b.spec = 1 then 'SD'
				else 'HD' 
			end) as SpecDesc,
			b.spec
		FROM TBPROG_M as a
		inner join  (
			select b.* from [10.1.253.88].[EF2KWeb].[EZAdmin].[playout] as b
			inner join [10.1.253.88].[EF2KWeb].[dbo].[resda] as c on b.PLAYOUT001=c.resda001 and b.PLAYOUT002=c.resda002  and c.resda021=2
		) as b 
		on a.[FSPROG_ID] = b.[programID] COLLATE Chinese_Taiwan_Stroke_CI_AS 
		where a.[FSPGMNAME] like '%飛虎傳奇%' and a.[FSPROG_ID] = '2012457';
	END --SUBCASE 3

	else if @subcase = 4 BEGIN
		SELECT p.playout001, p.playout002, p.[programID], p.[programNT], p.[prgset], p.[categories],
		p.[spec], p.[length_m], p.[length_s], p.[alerts], p.[alertsName], p.[customName], p.[customS], p.[title], p.[titleName],
		p.[titleS], p.[programSide], p.[liveSide], p.[timeSide], p.[Replay], p.[video], p.[stereo], p.[user_ID], p.[logo], p.[note],
		p.[smoking], p.[ch1], p.[ch2], p.[ch3], p.[ch4], p.[grade], al.[resal001], ak.[resak002], al.[resal003], ak.resak003
		FROM [10.1.253.88].[EF2KWeb].[EZAdmin].[playout] as p
		inner join [10.1.253.88].[EF2KWeb].[dbo].[resda] as da on p.playout001= da.resda001 and p.playout002=da.resda002  and da.resda021=2
		left outer join [10.1.253.88].[EF2KWeb].[dbo].[resak] as ak on p.user_ID = ak.resak001
		left outer join [10.1.253.88].[EF2KWeb].[dbo].[resal] as al on ak.resak015 = al.resal001
		where p.[programID] = '2012457' 
		and p.[prgset] = '4' and p.spec = '2'  
		Order by p.[playout002] Desc
		------------------------------------------------------------------------------------------------------

		-- NO JOIN with resal, resak
		SELECT p.playout001, p.playout002, p.[programID], p.[programNT], p.[prgset], p.[categories],
		p.[spec], p.[length_m], p.[length_s], p.[alerts], p.[alertsName], p.[customName], p.[customS], p.[title], p.[titleName],
		p.[titleS], p.[programSide], p.[liveSide], p.[timeSide], p.[Replay], p.[video], p.[stereo], p.[user_ID], p.[logo], p.[note],
		p.[smoking], p.[ch1], p.[ch2], p.[ch3], p.[ch4], p.[grade]
		FROM [10.1.253.88].[EF2KWeb].[EZAdmin].[playout] as p
		inner join [10.1.253.88].[EF2KWeb].[dbo].[resda] as da on p.playout001= da.resda001 and p.playout002=da.resda002  and da.resda021=2
		where p.[programID] = '2012457' 
		and p.[prgset] = '4' and p.spec = '2'  
		Order by p.[playout002] Desc

	END --SUBCASE 4


END

else if @case = 16 BEGIN   --about resxx table
	if @subcase = 1
		select * from [10.1.253.88].[EF2KWeb].[dbo].resda where resda031 like '%朱幸一%'
	else if @subcase = 2
		select count(resda001) from [10.1.253.88].[EF2KWeb].[dbo].resda
	else if @subcase = 3
		select * from [10.1.253.88].[EF2KWeb].[dbo].resda where  resda001 = 'PLAYOUT' and resda002 = @PLAYOUT_ID and resda021 = 2
		--select * from [10.1.253.88].[EF2KWeb].[dbo].resda where  resda001 = 'PLAYOUT' and resda002 = '0000001214'
	else if @subcase = 4 BEGIN
		select * from [10.1.253.88].[EF2KWeb].[dbo].resak where resak014 = '朱幸一'
		select * from [10.1.253.88].[EF2KWeb].[dbo].resal where resal001 = '01D1'
		select * from [10.1.253.88].[EF2KWeb].[dbo].resak where resak014 = '董家瑋'  --離職
		select * from [10.1.253.88].[EF2KWeb].[dbo].resal where resal001 = ''
	END

END
else if @case = 17 BEGIN -- 查詢資料結構
	SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
            WHERE TABLE_NAME = 'TBPGM_INSERT_TAPE'
END

else if @case = 18 BEGIN --新增、刪除資料
	if @subcase = 1
		exec SP_D_TBPGM_INSERT_TAPE @FSPROG_ID='0000003', @FNEPISODE='2', @FSARC_TYPE='001'
	else if @subcase = 2
		exec SP_I_TBPGM_INSERT_TAPE @FSPROG_ID=N'0000003',@FNEPISODE=N'2',@FSARC_TYPE=N'001',@FCSMOKE=N'Y',@FCALERTS=N'Y',@FSINSERT_TAPE=N'1',@FSINSERT_TAPE_NAME=N'',@FSINSERT_TAPE_DUR=N'',
		@FSTITLE_NAME=N'', @FSTITLE_DUR=N'20', @FSTITLE=N'Y', @FCPROG_SIDE_LABEL=N'Y', @FCLIVE_SIDE_LABEL=N'Y', @FCTIME_SIDE_LABEL=N'Y', @FCREPLAY=N'Y', @FCRECORD_PLAY=N'Y', @FCSTEREO=N'Y', @FCLOGO=N'L',
		@FSCH1=N'20', @FSCH2=N'01', @FSCH3=N'02', @FSCH4=N'03',
		@FSCREATED_BY=N'PTSMAM20144410'
	else if @subcase = 3
		exec SP_I_TBPGM_INSERT_TAPE @FSPROG_ID='0000003',@FNEPISODE=2,@FSARC_TYPE='001',@FCSMOKE='Y',@FCALERTS='Y',@FSINSERT_TAPE='1',@FSINSERT_TAPE_NAME='',@FSINSERT_TAPE_DUR='',
		@FSTITLE_NAME='', @FSTITLE_DUR='20', @FSTITLE='Y', @FCPROG_SIDE_LABEL='Y', @FCLIVE_SIDE_LABEL='Y', @FCTIME_SIDE_LABEL='Y', @FCREPLAY='Y', @FCRECORD_PLAY='Y', @FCSTEREO='Y', @FCLOGO='L',
		@FSCH1='20', @FSCH2='01', @FSCH3='02', @FSCH4='03',
		@FSCREATED_BY='PTSMAM20144410'
END

else if @case = 19 BEGIN -- 查詢MAM主控資訊表

	select * from TBPROG_M where FSPGMNAME = '星星的秘密'
	select * from TBPGM_INSERT_TAPE where FSPROG_ID = '0000079' and FNEPISODE = 11

	select
	FDCREATED_DATE '建立日期',
	(select u.FSUSER_ChtName from TBUSERS u where u.FSUSER_ID = i.FSCREATED_BY) '建立者',
	FSPROG_ID 節目編碼,
	(select pgm.FSPGMNAME from TBPROG_M pgm where pgm.FSPROG_ID = i.FSPROG_ID) 節目名稱,
	FNEPISODE 集數, 
	(CASE 
		WHEN i.FSARC_TYPE='001' THEN 'SD'
		WHEN i.FSARC_TYPE='002' THEN 'HD'
		ELSE 'N/A'
	END) 規格, 

	(CASE
		WHEN i.FCSMOKE='Y' THEN '要'
		WHEN i.FCSMOKE='N' THEN '不要'
		ELSE 'N/A'
	END) 吸煙卡,
	(CASE
		WHEN i.FCALERTS='Y' THEN '要'
		WHEN i.FCALERTS='N' THEN '不要'
		ELSE 'N/A'
	END) 警示卡,
	(CASE
		WHEN i.FSINSERT_TAPE=0 THEN ''
		WHEN i.FSINSERT_TAPE=1 THEN '制式版1'
		WHEN i.FSINSERT_TAPE=2 THEN '制式版2'
		WHEN i.FSINSERT_TAPE=3 THEN '制式版3'
		WHEN i.FSINSERT_TAPE=4 THEN '制式版4'
		WHEN i.FSINSERT_TAPE=5 THEN '制式版5'
		WHEN i.FSINSERT_TAPE=6 THEN '制式版6'
		WHEN i.FSINSERT_TAPE=7 THEN '客製版'
		ELSE 'N/A'
	END) 制式版,
	i.FSINSERT_TAPE_NAME 警示卡名稱,
	i.FSINSERT_TAPE_DUR 警示卡長度,

	(CASE
		WHEN i.FSTITLE='Y' THEN '要'
		WHEN i.FSTITLE='N' THEN '不要'
		ELSE 'N/A'
	END) 檔名總片頭,

	i.FSTITLE_NAME 檔名總片頭名稱,
	i.FSTITLE_DUR 檔名總片頭長度,
	(CASE
		WHEN i.FCPROG_SIDE_LABEL='Y' THEN '要'
		WHEN i.FCPROG_SIDE_LABEL='N' THEN '不要'
		ELSE 'N/A'
	END) 節目側標,
	(CASE
		WHEN i.FCLIVE_SIDE_LABEL='Y' THEN '要'
		WHEN i.FCLIVE_SIDE_LABEL='N' THEN '不要'
		ELSE 'N/A'
	END) LIVE側標,
	(CASE
		WHEN i.FCTIME_SIDE_LABEL='Y' THEN '要'
		WHEN i.FCTIME_SIDE_LABEL='N' THEN '不要'
		ELSE 'N/A'
	END) 時間側標,
	(CASE
		WHEN i.FCREPLAY='Y' THEN '要'
		WHEN i.FCREPLAY='N' THEN '不要'
		ELSE 'N/A'
	END) 重播,
	(CASE
		WHEN i.FCRECORD_PLAY='Y' THEN '要'
		WHEN i.FCRECORD_PLAY='N' THEN '不要'
		ELSE 'N/A'
	END) 錄影轉播,
	(CASE
		WHEN i.FCSTEREO='Y' THEN '要'
		WHEN i.FCSTEREO='N' THEN '不要'
		ELSE 'N/A'
	END) 立體聲,
	(CASE 
		WHEN i.FCLOGO='R' THEN '右邊'
		WHEN i.FCLOGO='L' THEN '左邊'
		WHEN i.FCLOGO='N' THEN '不要'
		ELSE 'N/A'
	END) 公視LOGO,

	(select lan.FSPROGLANGNAME from TBZPROGLANG lan where lan.FSPROGLANGID=i.FSCH1) 雙語1,
	(select lan.FSPROGLANGNAME from TBZPROGLANG lan where lan.FSPROGLANGID=i.FSCH2) 雙語2,
	(select lan.FSPROGLANGNAME from TBZPROGLANG lan where lan.FSPROGLANGID=i.FSCH3) 雙語3,
	(select lan.FSPROGLANGNAME from TBZPROGLANG lan where lan.FSPROGLANGID=i.FSCH4) 雙語4
	from TBPGM_INSERT_TAPE as i 
	where FSPROG_ID = '0000079' and FNEPISODE = 11
	order by FDCREATED_DATE desc
END --CASE19

---------------------------------- TESTING SECTION ------------------------------------------------------
/*
if @case = 101 BEGIN
	select 
			@FCSMOKE1 = FCSMOKE
			,@FCALERTS1 = FCALERTS
			,@FSINSERT_TAPE1 = FSINSERT_TAPE
			,@FSINSERT_TAPE_NAME1 = FSINSERT_TAPE_NAME
			,@FSINSERT_TAPE_DUR1 = FSINSERT_TAPE_DUR
			,@FSTITLE1 = FSTITLE
			,@FSTITLE_NAME1 = FSTITLE_NAME
			,@FSTITLE_DUR1 = FSTITLE_DUR
			,@FCPROG_SIDE_LABEL1 = FCPROG_SIDE_LABEL
			,@FCLIVE_SIDE_LABEL1 = FCLIVE_SIDE_LABEL
			,@FCTIME_SIDE_LABEL1 = FCTIME_SIDE_LABEL
			,@FCREPLAY1 = FCREPLAY
			,@FCRECORD_PLAY1 = FCRECORD_PLAY
			,@FCSTEREO1 = FCSTEREO
			,@FCLOGO1 = FCLOGO
			,@FSCH11 = FSCH1
			,@FSCH21 = FSCH2
			,@FSCH31 = FSCH3
			,@FSCH41 = FSCH4
			from TBPGM_INSERT_TAPE where FSPROG_ID = '0000060' and FNEPISODE = 1 and FSARC_TYPE = '001'

select
			@FCSMOKE1
			,@FCALERTS1
			,@FSINSERT_TAPE1
			,@FSINSERT_TAPE_NAME1
			,@FSINSERT_TAPE_DUR1
			,@FSTITLE1
			,@FSTITLE_NAME1
			,@FSTITLE_DUR1
			,@FCPROG_SIDE_LABEL1
			,@FCLIVE_SIDE_LABEL1
			,@FCTIME_SIDE_LABEL1
			,@FCREPLAY1
			,@FCRECORD_PLAY1
			,@FCSTEREO1
			,@FCLOGO1
			,@FSCH11
			,@FSCH21
			,@FSCH31
			,@FSCH41
END --CASE101
*/

if @case = 102 BEGIN
	if EXISTS( select FSPROG_ID from TBPGM_INSERT_TAPE where FSPROG_ID = '0000001' )
		print 'found'
END

GO -- END CREATE PROC




-------------------------------------------------------------------------------------------------------------------------------------------

DECLARE
@case integer, @subcase integer = 0

-- 查詢 Playout 相關
--set @case = 1 set @subcase = 1		--1-1:所有 palyout 資訊
--set @case = 1 set @subcase = 4		--1-4:查詢雙語的所有項目PLAYOYT
--set @case = 1 set @subcase = 5		--1-5:查詢雙語的所有項目MAM, PLAYOUT
--set @case = 1 set @subcase = 7		--1-7:查詢有效的表單
--set @case = 1 set @subcase = 8		--查詢有效單號筆數

--set @case = 2 set @subcase = 0		-- 根據節目編號和集數查詢 playout

-- 查詢 MAM TBPGM_INSERT_TAPE
--set @case = 3 set @subcase = 1		--3-1:查詢所有主控播出資訊(MAM)
--set @case = 3 set @subcase = 5		--查詢 TBPGM_INSERT_TAPE schema information
--set @case = 3 set @subcase = 6		--查詢雙語代碼

--set @case = 13 set @subcase = 0	--比較 Playout and TBPGM_INSERT_TAPE 資料
--set @case = 14 set @subcase = 1 	--輸出PLAYOUT 和 TBPGM_INSERT_TAPE 有差異的部份
set @case = 14 set @subcase = 2		--輸出PLAYOUT 和 TBPGM_INSERT_TAPE 有差異的部份 20151124


-- 查詢 resda 相關
--set @case = 16 set @subcase = 1 
--set @case = 16 set @subcase = 2
--set @case = 16 set @subcase = 3
--set @case = 17 set @subcase = 0	--查詢SCHEMA INFORMATION

--set @case = 18 set @subcase = 1	--刪除資料
--set @case = 18 set @subcase = 2	--新增資料
--set @case = 18 set @subcase = 3	--新增資料

------------------ TESTING BLOCK
--set @case = 101
--set @case = 102

DECLARE
@programID varchar(10) = '2009285', --節目編號
@episode integer = 542, --子集
@PLAYOUT_ID varchar(10) =  '0000001192' --0000001214

EXEC #TEST_CASE @case, @subcase, @programID, @episode, @PLAYOUT_ID

GO

DROP PROC #TEST_CASE
DROP PROC #COMPARE_PLAYOUT_MAM
DROP PROC #COMPARE_PLAYOUT_MAM_ORG
DROP PROC #COMPARE_PLAYOUT_MAM_20151124
DROP PROC #PLAYOUT2MAM
DROP PROC #SP_Q_PLAYOUT
DROP PROC #SP_Q_PLAYOUT_EFFECTIVE_ALL
GO


