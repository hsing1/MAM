
declare @case int

select A.*, B.FSFILE_NO, B.FSVIDEO_PROG, C.FSPGMNAME, B.FNEPISODE, C.FSCHANNEL from TBLOG_VIDEO_HD_MC A left join TBLOG_VIDEO B on A.FSVIDEO_ID = B.FSVIDEO_PROG left join TBPROG_M C on B.FSID = C.FSPROG_ID
	where CAST(A.FDUPDATED_DATE as DATE) = '2016-11-23' and FCSTATUS = 'E'

select * from TBTSM_JOB where FDREGISTER_TIME between CAST(GETDATE() -1 as DATE) and GETDATE() and FNSTATUS = 2 order by FDREGISTER_TIME


-- Routin Job
if @case = 201611220910 begin --TEST Linked Server

	if EXISTS(select top 1 * from [10.1.253.88].EF2KWEB.EZAdmin.playout)
		print '10.1.253.88 OK'
	else
		print '10.1.253.88 is not accessible'
	
	if EXISTS(select top 1 * from PTS_MAM.PTS_MAM.dbo.TBUSERS)
		print 'PTS_MAM is OK'
	else
		print 'PTS_MAM is not accessible'

	if EXISTS(select top 1 * from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA'))
		print 'PTS_PROG_MYSQL is OK'
	else
		print 'PTS_PROG_MYSQL is not accessible'

	if EXISTS(select top 1 * from [pts-app1].PTSHR.dbo.tblDept)
		print 'pts-app1 is OK'
	else
		print 'pts-app1 is not accessible'

	if EXISTS(select top 1 * from PTSPROG.PTSProgram.dbo.TBPROG_M)
		print 'PTSPROG is OK'
	else
		print 'PTSPROG is not accessible'

	if EXISTS(select top 1 * from PTSTAPE.PTSProgram.dbo.TBPROG_M)
		print 'PTSTAPE is OK'
	else
		print 'PTSTAPE is not accessible'

	if EXISTS(select top 1 * from PTSTAPE.PTSTape.dbo.tblUser)
		print 'PTSTAPE is OK'
	else
		print 'PTSTAPE is not accessible'

	if EXISTS(select top 1 * from NEWSARCHIVE.PTS_MAM.dbo.TBUSERS)
		print 'NEWSARCHIVE is OK'
	else
		print 'NEWSARCHIVE is not accessible'

	if EXISTS(select top 1 * from NEWS.PTS_MAM.dbo.TBUSERS)
		print 'NEWS is OK'
	else
		print 'NEWS is not accessible'


	--not work
	if EXISTS(select top 1 * from PTSHR.MAMTEST.dbo.TEST)
		print 'PTSHR is OK'
	else
		print 'PTSHR is not accessible'


	--not work
	BEGIN TRY
		if EXISTS(select top 1 * from PTSHR.MAMTEST.dbo.TEST)
			print 'PTSHR is OK'
	END TRY
	BEGIN CATCH
		print 'PTSHR is not accessible'
	END CATCH

end

if @case = 201607221839 begin -- 調整搬檔時間
	exec SP_Q_GET_MOVE_PENDING_LIST_MIX
	exec SP_Q_GET_CHECK_COMPLETE_LIST_MIX
	exec SP_Q_GET_IN_TAPE_LIBRARY_LIST_MIX
end

if @case = 201701191409 begin --查詢節目資訊
	select u.QUEUE, u.PRGID, u.NAME, u.EPISODE, u.FDDATE, u.FSPLAY_TIME, u.CHANNEL_ID, u.FSVIDEO_ID, v.FCFILE_STATUS, v.FSARC_TYPE, h.FCSTATUS from (
		select FNCONBINE_NO QUEUE, FSPROG_ID PRGID, FSPROG_NAME NAME, FDDATE, FSCHANNEL_ID CHANNEL_ID, FNEPISODE EPISODE, FSPLAY_TIME, FSVIDEO_ID from TBPGM_COMBINE_QUEUE where FDDATE >= (GETDATE() - 1)
			UNION (select t.FNARR_PROMO_NO QUEUE, p.FSPROMO_ID PRGID, p.FSPROMO_NAME NAME, t.FDDATE, t.FSCHANNEL_ID, 0 EPISODE, t.FSPLAYTIME, t.FSVIDEO_ID from TBPGM_ARR_PROMO t 
				join TBPGM_PROMO p on t.FSPROMO_ID = p.FSPROMO_ID where FDDATE >= (GETDATE() - 1))) u
	left join TBLOG_VIDEO v on u.PRGID = v.FSID and u.EPISODE = v.FNEPISODE and v.FSARC_TYPE in ('001', '002') and v.FCFILE_STATUS not in ('X', 'F', 'D')
	left join TBLOG_VIDEO_HD_MC h on v.FSFILE_NO = h.FSFILE_NO
	order by u.FDDATE, u.CHANNEL_ID, u.QUEUE--16578, 如果一節目有 SD 和 HD 會出現兩筆
end

if 0 = 1 begin --Update command
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201612000190001'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201612000190001'
end