
Use Northwind
GO
IF OBJECT_ID(N'dbo.Split1', N'TF') IS NOT NULL 
	DROP FUNCTION Split1;
GO

CREATE FUNCTION [dbo].[Split1]
(
	@RowData nvarchar(2000),
	@SplitOn nvarchar(5)
)
RETURNS @RtnValue table
(
	Id int identity(1,1),
	Data nvarchar(100)
) 
AS
BEGIN
	Declare @Cnt int
	Set @Cnt = 1
	--insert into @RtnValue(Data) VALUES(@Cnt)  --DEBUG
	--insert into @RtnValue(Data) VALUES(@RowData) --DEBUG

	While (Charindex(@SplitOn, @RowData) > 0)
	Begin
		Insert Into @RtnValue (data)
		Select 
			Data = ltrim(rtrim(Substring(@RowData, 1, Charindex(@SplitOn, @RowData) - 1)))

		Set @RowData = Substring(@RowData, Charindex(@SplitOn, @RowData) + 1, len(@RowData))
	       	--如果 start 和 length 的總和大於 expression 中的字元數，則會傳回從 start 開始的整個值運算式
		Set @Cnt = @Cnt + 1
		--insert into @RtnValue(Data) VALUES(@Cnt)  --DEBUG
		--insert into @RtnValue(Data) VALUES(@RowData) --DEBUG
	End
	
	Insert Into @RtnValue (data) Select Data = ltrim(rtrim(@RowData))

	Return
END --END function Split1 
GO

IF OBJECT_ID(N'dbo.Split2', N'TF') IS NOT NULL
	DROP FUNCTION Split2;
GO
CREATE FUNCTION Split2 (
      @InputString VARCHAR(8000),
      @Delimiter VARCHAR(50)
)

RETURNS @Items TABLE (
      Item VARCHAR(8000)
)

AS
BEGIN
	/*
      IF @Delimiter = ' '
      BEGIN
            SET @Delimiter = ','
	    SET @InputString = REPLACE(@InputString, ' ', @Delimiter)
	    SET @InputString = REPLACE(@InputString, CHAR(9), @Delimiter) --replace tab
      END
*/


      IF (@Delimiter IS NULL OR @Delimiter = '')
            SET @Delimiter = ','

	--INSERT INTO @Items VALUES (@Delimiter) -- Diagnostic
	--INSERT INTO @Items VALUES (@InputString) -- Diagnostic


      DECLARE @Item VARCHAR(8000)
      DECLARE @ItemList VARCHAR(8000)
      DECLARE @DelimIndex INTEGER 

      SET @ItemList = LTRIM(@InputString)
      SET @DelimIndex = CHARINDEX(@Delimiter, @ItemList, 1)
      WHILE (@DelimIndex != 0)
      BEGIN
            SET @Item = SUBSTRING(@ItemList, 0, @DelimIndex)
		--SET @Item = SUBSTRING(@ItemList, 1, @DelimIndex - 1) -- 多了一個運算
		if (@item != '')
			INSERT INTO @Items VALUES (@Item)

            -- Set @ItemList = @ItemList minus one less item
            SET @ItemList = SUBSTRING(@ItemList, @DelimIndex + 1, LEN(@ItemList) - @DelimIndex)
		INSERT INTO @Items VALUES (CAST(@DelimIndex as varchar(10)))-- Diagnostic
		INSERT INTO @Items VALUES (@ItemList) -- Diagnostic
            SET @DelimIndex = CHARINDEX(@Delimiter, @ItemList, 0)
      END -- End WHILE

      IF @Item IS NOT NULL -- At least one delimiter was encountered in @InputString
      BEGIN
            SET @Item = @ItemList
            INSERT INTO @Items VALUES (@Item)
      END

      -- No delimiters were encountered in @InputString, so just return @InputString
      ELSE INSERT INTO @Items VALUES (@InputString)

      RETURN

END -- End Function Split2
GO

IF OBJECT_ID(N'dbo.Split3', N'P') IS NOT NULL
	DROP PROC Split3;
GO
CREATE PROC Split3
      @InputString VARCHAR(8000),
      @Delimiter VARCHAR(50)
AS

	DECLARE @Item VARCHAR(8000)
	DECLARE @ItemList VARCHAR(8000)
	DECLARE @DelimIndex INTEGER
	declare @cnt integer = 1
	declare @Items TABLE (
      		Item VARCHAR(8000)
	)

	SET @InputString = REPLACE(@InputString, CHAR(9), ' ') --replace tab white space
	print 'BEGIN PROC Split3'
	print 'Delimiter = ''' + @Delimiter + '''' --DEBUG
      	if (@Delimiter IS NULL) BEGIN
		print 'Delimiter is NULL, change delimiter to '','''
        	SET @Delimiter = ','
	END

	SET @ItemList = RTRIM(LTRIM(@InputString))
	SET @DelimIndex = CHARINDEX(@Delimiter, @ItemList, 1)
	print 'DelimIndex = ' + CAST(@DelimIndex as char(5)) --DEBUG
	print 'Delimiter = ''' + @Delimiter + '''' --DEBUG

	
	WHILE (@DelimIndex != 0)
	BEGIN
		print 'String' + CAST(@cnt as char(3)) + ' : ''' + @ItemList + ''''
		print 'DelimIndex = ' + CAST(@DelimIndex as char(5)) --DEBUG
		SET @Item = RTRIM(SUBSTRING(@ItemList, 1, @DelimIndex - 1))
		print 'item = ''' + @item + ''''
		if (@item != '') BEGIN
			print 'insert item:''' + @item + ''''
			INSERT INTO @Items VALUES (@Item)
		END

		-- Set @ItemList = @ItemList minus one less item
		SET @ItemList = SUBSTRING(@ItemList, @DelimIndex + 1, LEN(@ItemList) - @DelimIndex)
		SET @ItemList = LTRIM(@ItemList)
		print '''' + @ItemList + ''''
            	SET @DelimIndex = CHARINDEX(@Delimiter, @ItemList, 1)
		set @cnt += 1
      	END -- End WHILE

       	INSERT INTO @Items VALUES (@ItemList)

	select * from @Items
      RETURN

-- End PROC Split3
GO
-----------------------------------------------------------------------------------------------------------------

IF OBJECT_ID(N'tempdb.dbo.#TEST_CASE', N'P') IS NOT NULL
	DROP PROC #TEST_CASE;
GO

CREATE PROC #TEST_CASE
	@case integer, @subcase integer
AS
DECLARE @str varchar(100) = 'a,abc,efg'

if @case = 1 BEGIN --String Function
	if @subcase = 1 BEGIN
		set @str = ',abc,efg'
		select CHARINDEX(',', @str, 1) -- str[1] = ','
	END
	if @subcase = 2 BEGIN
		set @str = 'abcdef'
		select SUBSTRING(@str, 1, 6) -- output = 'abcdef'
	END
	if @subcase = 21 BEGIN
		set @str = '   ab cd ef'
		print LEN(@str) -- length = 11
		set @str = 'ab cd ef    '
		print LEN(@str) -- length = 8 not count trail space
		print '''' + SUBSTRING(@str, 3, LEN(@str) - 3) + '''' -- output = 'abcdef'
	END
	if @subcase = 3
		select len('abcd')
	if @subcase = 4 BEGIN
		set @str = '	abc|	 cde| fff	| ddd'
		print REPLACE(@str, CHAR(9), ',') --',abc|, cde| fff,| ddd'
	END
END

else if @case = 2 BEGIN --Time Function
	if @subcase = 1 BEGIN
		select CAST(GETDATE() as nvarchar(40))
		select CONVERT(char(4), YEAR(GETDATE())) + CONVERT(char(2), MONTH(GETDATE())) + CONVERT(char(2), DAY(GETDATE()))
		select CONVERT(char(8), GETDATE(), 112)
	END
END

else if @case = 3 BEGIN --split function
	declare @testStr nvarchar(100) = '	abc	 cde fff	 ddd  		'
	declare @testStr1 nvarchar(100) = '	abc|	 cde| fff	| ddd'
	declare @testStr2 nvarchar(100) = 'abc cde fff ddd'
	declare @testStr3 nvarchar(100) = '	abc cde fff ddd'
	declare @testStr4 nvarchar(100) = ' abc cde fff ddd'
	declare @testStr5 nvarchar(100) = '  abc   cde   fff   ddd   '
	if @subcase = 1
		select * from Split1(@testStr, ' ')
	if @subcase = 2
		select * from Split2(@testStr2, ' ')
	if @subcase = 3
		EXEC Split3 @testStr1, '|'
END

else if @case = 4 begin --COALESCE(), ISNULL()
	if @subcase = 1 begin --COALESCE()
		declare @ITEM_PRICE table (
			ITEM_ID nvarchar(10) NOT NULL,
			ITEM_NAME nvarchar(50) NOT NULL,
			PRICE1 int NULL,
			PRICE2 int NULL,
			PRICE3 int NULL
		)
		
			--CONSTRAINT PK_ITEM PRIMARY KEY CLUSTERED (
			--	ITEM_ID ASC
			--) with (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON PRIMARY) ON PRIMARY
	
		insert into @ITEM_PRICE values('A0000001', '男用皮包', 10000, NULL, NULL)
		insert into @ITEM_PRICE values('A0000002', '女用皮包', 15000, NULL, NULL)
		insert into @ITEM_PRICE values('A0000003', '陶瓷茶具組', NULL, 5000, NULL)
		insert into @ITEM_PRICE values('A0000004', '高級酒杯組', NULL, 8000, NULL)
		insert into @ITEM_PRICE values('A0000005', '咖啡壺', NULL, NULL, 1999)
		insert into @ITEM_PRICE values('A0000006', '高級炒菜鍋', NULL, NULL, 12000)

		SELECT CASE WHEN x IS NOT NULL THEN x ELSE 1 END
			from (
				select (select price1 from @ITEM_PRICE where ITEM_ID = 'A0000001') as x
			) as T

		select * from @ITEM_PRICE

		select ITEM_ID, ITEM_NAME, COALESCE(PRICE1 * 0.7, PRICE2 * 0.8, PRICE3 * 0.9) from @ITEM_PRICE

		select ITEM_ID, ITEM_NAME, 
		CASE
			WHEN ISNULL(PRICE1, '') <> 0 THEN PRICE1 * 0.7
			WHEN ISNULL(PRICE2, '') <> 0 THEN PRICE2 * 0.8
			WHEN ISNULL(PRICE3, '') <> 0 THEN PRICE3 * 0.9
		END
		from @ITEM_PRICE

		
	end

	else if @subcase = 2 begin --COALESCE
		--USE tempdb; Error : cannot use use statement in stored procedure, function, trigger
		-- This statement fails because the PRIMARY KEY cannot accept NULL values 
		-- and the nullability of the COALESCE expression for col2
		-- evaluates to NULL. 
		drop table #Demo
		CREATE TABLE #Demo ( 
		col1 integer NULL, 
		col2 AS COALESCE(col1, 0) PRIMARY KEY, 
		col3 AS ISNULL(col1, 0) );

		select * from #demo

		-- This statement succeeds because the nullability of the
		-- ISNULL function evaluates AS NOT NULL. 
		CREATE TABLE #Demo ( 
			col1 integer NULL, 
			col2 AS COALESCE(col1, 0), 
			col3 AS ISNULL(col1, 0) PRIMARY KEY );
	end --subcase 2

	else if @subcase = 3 begin -- ISNULL() vs COALESCE()
		select ISNULL('A', 2) as T1, ISNULL(NULL, 2) as T2, ISNULL(NULL, NULL) T3
		select COALESCE(NULL, 'A', NULL) as T
		select ISNULL(NULL, 1) as T, COALESCE(NULL, 1) as TT --the expressions ISNULL(NULL, 1) and COALESCE(NULL, 1) although equivalent have different nullability values
		select COALESCE(NULL, NULL) as T -- 至少有一個 COALESCE 的引數必須為 NULL 類型。
		SELECT COALESCE(NULL, NULL)
		select DATALENGTH(ISNULL(NULL, 5)) as T, DATALENGTH(ISNULL(NULL, 'a')) as T1, DATALENGTH(ISNULL(NULL, N'a')) as T2
		select DATALENGTH(COALESCE(NULL, 'This is a book')) as T, DATALENGTH(ISNULL(NULL, 'This is a book')) as T2

		declare  @str1 varchar(7), @str2 varchar(100)
		set @str1 = NULL;
		set @str2 = 'This is a book'

		select DATALENGTH(ISNULL(NULL, @str2)) as T1, DATALENGTH(ISNULL(@str1, @str2)) as T2 -- T1 = 14, T2 = 7

	end
end
GO --END #TEST_CASE

DECLARE @case integer, @subcase integer

--set @case = 1 set @subcase = 21 -- SUBSTRING function
--set @case = 1 set @subcase = 4 -- REPLACE function
set @case = 3 set @subcase = 3 -- split function
EXEC #TEST_CASE @case, @subcase

GO
