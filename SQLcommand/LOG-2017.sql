declare @case int, @subcase int

if @case = 201701260937 begin --測試檔案上傳
	select * from TBPGM_QUEUE where FSCHANNEL_ID = '08' and CAST(FDDATE as DATE) = '2017-01-26'
	exec SP_Q_INFO_BY_VIDEOID '00002QTQ'
	exec SP_Q_INFO_BY_VIDEOID '00002PAN'

	


	exec SP_Q_INFO_BY_VIDEOID '00003C7M'
	-- 未標記，等待上傳 FCSTATUS = '',  201701260031	G201323200010067	00003C7M		51204	2017-01-26 11:21:06.860	NULL	NULL
	
	exec SP_U_TBLOG_VIDEO_HD_MC_BY_FILE_NO 'G201323200010067', 'C', '51204'  --不會搬檔 HDUpload -> Review
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00003C7M'

	exec SP_U_TBLOG_VIDEO_HD_MC_BY_FILE_NO 'G201323200010067', 'N', '51204'

end

if @case = 201701261413 begin
	exec SP_Q_INFO_BY_VIDEOID '000022ZA' --2016-06-13 20:26:38.860 轉檔 可能帶子是 offline, 我愛親家 #1, play : 2017-01-29 12:30:00;00 62, 2017-02-04 14
	exec SP_Q_INFO_BY_VIDEOID '00003BGA' --2017-02-05, 村民大會 518
	exec SP_Q_INFO_BY_VIDEOID '00003BYV' --2017-01-29, 18:53:07:14, 村民大會第519集預告_三義茶花爭豔!
	exec SP_Q_INFO_BY_VIDEOID '000038LX' --2017-01-29	19:00:01:10, 劃破天際看世界
	exec SP_Q_INFO_BY_VIDEOID '00003BSW' --2017-01-29 19:30:00;00, 2016金嗓金曲演唱會(90分鐘) 2
	exec SP_Q_INFO_BY_VIDEOID '00002AM1'
	exec SP_Q_INFO_BY_VIDEOID '00003AUR'
	exec SP_Q_INFO_BY_VIDEOID '00003B2K'
	exec SP_Q_INFO_BY_VIDEOID '00003BJ1'

	select * from TBTSM_JOB where FSSOURCE_PATH like '%G201417100010015%' order by FDREGISTER_TIME desc
	select * from TBZCHANNEL
end

if @case = 201701261802 begin -- 上傳完成無法 play
	exec SP_Q_INFO_BY_VIDEOID '00003BFX'
	exec SP_Q_INFO_BY_VIDEOID '000039FW'
end

if @case = 201701300820 begin
	select * from TBBROADCAST where FSBRO_ID = '201612300139'
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-01-26' and 
	(FSMESSAGE like '%154051%' or FSMESSAGE like '%211986%' or FSMESSAGE like '%105118%' or FSMESSAGE like '%0039FW%' or FSMESSAGE like '%G201757400010002%')
	order by FDTIME --2017-01-26 20:58:53.287	INFO      	plink開始進行高解搬檔：Job_ID：154051

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-01-29' and 
	(FSMESSAGE like '%154051%' or FSMESSAGE like '%211986%' or FSMESSAGE like '%105118%' or FSMESSAGE like '%0039FW%' or FSMESSAGE like '%G201757400010002%')
	order by FDTIME
end


----------------------------------------20170206---------------------------------------
if @case = 201702060854 begin --手動入庫
   select * from TBLOG_VIDEO where FSFILE_NO = 'G201764200010002' --00003AMH, R, 201701120049
   exec SP_Q_INFO_BY_VIDEOID '00003AMH' --2016花式滑冰大獎賽  日本站

   select * from TBLOG_VIDEO where FSID = '2017642' and FNEPISODE = 1 --FCLOW_RES = 'F'

   select * from TBBROADCAST where FSBRO_ID = '201701120049' --2017-01-23 11:37:21.603

   select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-01-23' and
	(FSMESSAGE like '%153593%' or FSMESSAGE like '%003AMH%' or FSMESSAGE like '%G201764200010002%')
	order by FDTIME --已完成檔案編號：G201764200010002 所有轉檔完成處理程序

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-01-29' and 
	(FSMESSAGE like '%153593%' or FSMESSAGE like '%003AMH%' or FSMESSAGE like '%G201764200010002%')
	order by FDTIME

	--exec SP_U_TBLOG_VIDEO_FILE_STATUS 'G201764200010002', 'T', '51204'  -- crash
	update TBLOG_VIDEO set FCLOW_RES = 'Y' where FSFILE_NO = 'G201764200010002' --OK
    update TBLOG_VIDEO set FCKEYFRAME = 'F' where FSFILE_NO = 'G201764200010002'  --> 應該改成 N

	select * from TBLOG_VIDEO where FCKEYFRAME = 'R'

	select * from TBLOG_VIDEO_KEYFRAME where FDCREATED_DATE > '2017-01-20' and FSVIDEO_NO = 'G201764200010002'

	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201764200010002'
end

if @case = 201702060938 begin --TS3310 無法下架問題
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-02-06' and FSMESSAGE like '%10.1.254.51%' order by FDTIME
	select * from TBTRACKINGLOG where FDTIME > '2017-02-06 09:30' order by FDTIME
end

if @case = 201702061036 begin --檔案已由 GPFS -> Review  但是狀態未更新
	exec SP_Q_INFO_BY_VIDEOID '000015LU' --T, 幾點回家 #1, FCSTATUS = '', G201404600010002
	exec SP_U_TBLOG_VIDEO_HD_MC_BY_FILE_NO 'G201404600010002', 'N', '51204' 
end

if @case = 201702061541 begin --飛越客庄 TC  錯誤 Anystream 轉檔失敗，無法置換，無法手動刪除
	exec SP_Q_INFO_BY_VIDEOID '0000395B' --R G201680400100002, 2016804 #10 飛越客庄, 201612280094
	select * from TBLOG_VIDEO where FSID = '2016804' and FNEPISODE = 10
	select * from TBBROADCAST where FSBRO_ID = '201612280094' --2017-01-26 15:16:39.130

	select * from TBTRACKINGLOG where FDTIME > '2017-02-06 17:03' order by FDTIME

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-01-26' and 
	(FSMESSAGE like '%154009%' or FSMESSAGE like '%211940%' or FSMESSAGE like '%105077%' or FSMESSAGE like '%00395B%' or FSMESSAGE like '%G201680400100002%')
	order by FDTIME

	select * from TBBROADCAST where FSBRO_ID = '201608300041'

	--Anystream 處理錯誤
	select * from TBTRANSCODE_RECV_LOG where  FNJOB_ID = '105077'  --(Timecode-based in- point is earlier than start timecode)

	--無法手動刪除
	update TBLOG_VIDEO set FCFILE_STATUS = 'D' where FSFILE_NO = 'G201680400100002'

	--以新增送播單的方式
	exec SP_Q_INFO_BY_VIDEOID '00003CWE'

end


if @case = 201702061544 begin --TS3310 無法下架
	select * from TBTRACKINGLOG where FDTIME > '2017-02-06 15:20' order by FDTIME --WSTSM/CheckoutTapes_TSM3310 使用者08307要從TS3310退出磁帶：A01393L4, A01448L4

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) ='2017-02-06' and FSPROGRAM like '%CheckoutTapes_TSM3310%' order by FDTIME
	select * from TBUSERS where FSUSER_ID = '08307' --潘珍妮
end

------------------------------20170207--------------------------------------------

if @case = 201702070941 begin --TSM RECALL FAIL
	select * from TBTSM_JOB where FNTSMJOB_ID = 213796  --FNSTATUS 3 -> 5
	select * from TBTRACKINGLOG where FDTIME between '2017-02-06 23:58' and '2017-02-07 01:00' order by FDTIME
	exec SP_Q_INFO_BY_VIDEOID '000027YM' --誰來晚餐7 #35
	select * from TBTSM_JOB where FNTSMJOB_ID in (213796, 214390, 214389, 214388, 214387) --213796 FNSTATUS = 5 (誰來晚餐 #35)
end

-------------------------------20170208--------------------------------------------
if @case = 201702081014 begin
	exec SP_Q_GET_MOVE_PENDING_LIST_MIX --FSID, FNEPISODE, FSFILE_NO, FCFILE_STATUS, FCCHECK_STATUS, FCSTATUS, FSBRO_ID
	exec SP_Q_GET_CHECK_COMPLETE_LIST_MIX_MACRO
end

if @case = 201702081350 begin
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-02-07' and 
	(FSMESSAGE like '%154750%' or FSMESSAGE like '%213995%' or FSMESSAGE like '%G201715300010002%')
	order by FDTIME

	select * from TBTSM_JOB where FNTSMJOB_ID = 213995 -- 2017-02-07 20:04
	select * from TBTRACKINGLOG where FDTIME between '2017-02-07 20:04:20' and '2017-02-07 20:04:50' order by FDTIME

	select * from TBBOOKING_DETAIL where FSBOOKING_NO = '201702070054' --G201715300010002 x 2 --> 有兩個轉檔任務編號

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-02-07' and 
	(FSMESSAGE like '%154679%' or FSMESSAGE like '%213911%' or FSMESSAGE like '%G000037200010001%')
	order by FDTIME
end

-------------------------------------20170209-----------------------------------------
if @case = 201702091349 begin
	select * from TBTSM_JOB where FSSOURCE_PATH = '/ptstv/HVideo/2014/2014171/G201417100010015.mxf' order by FDREGISTER_TIME
	select * from TBTSM_JOB where FSSOURCE_PATH = '/hakkatv/HVideo/2015/2015809/G201580901410002.mxf' order by FDREGISTER_TIME

	select * from TBLOG_VIDEO where FSFILE_NO = 'G201580901410002' --00002UA4
	exec SP_Q_INFO_BY_VIDEOID '00002UA4'
	select * from TBPROG_M where FSPROG_ID = '2015809' --食在料理王(60分鐘版本)

	select * from TBLOG_VIDEO where FSFILE_NO = 'G201554800350002' --000027YM
	exec SP_Q_INFO_BY_VIDEOID '000027YM' --誰來晚餐7 #35  Tape Library Status = SysError
	select * from TBTSM_JOB where FSSOURCE_PATH = '/ptstv/HVideo/2015/2015548/G201554800350002.mxf' order by FDREGISTER_TIME

	select * from TBTSM_JOB where CAST(FDREGISTER_TIME as DATE) = '2017-02-09' and FNSTATUS <> 3 order by FDREGISTER_TIME

	-- dsmrecall success
end

if @case = 201702100907 begin
	select * from TBPGM_QUEUE where FDDATE between '2016-08-05' and '2016-08-23' and FSCHANNEL_ID in (12, 13, 14) and FSPROG_NAME like '%2016里約奧運%'
	order by FDDATE, FSCHANNEL_ID, FSBEG_TIME --124

	select A.FSPROG_ID, A.FSPROG_NAME, A.FSCHANNEL_ID, A.FDDATE, A.FSBEG_TIME, A.FSEND_TIME, B.FSPROG_ID, B.FSPROG_NAME, B.FNBREAK_NO, B.FSCHANNEL_ID, B.FDDATE, B.FSPLAY_TIME, B.FSDUR from TBPGM_QUEUE A 
	left join TBPGM_COMBINE_QUEUE B on A.FSPROG_ID = B.FSPROG_ID and A.FSCHANNEL_ID = B.FSCHANNEL_ID and A.FDDATE = B.FDDATE
	where B.FDDATE between '2016-08-06' and '2016-08-22' and B.FSCHANNEL_ID in (12, 13, 14) and B.FSPROG_NAME like '%2016里約奧運%'
	order by B.FSCHANNEL_ID, B.FDDATE, FSBEG_TIME --217

	select A.FSPROG_ID, A.FSPROG_NAME, A.FSCHANNEL_ID, A.FDDATE, A.FSBEG_TIME, A.FSEND_TIME, B.FSPROG_ID, B.FSPROG_NAME, B.FNBREAK_NO, B.FSCHANNEL_ID, B.FDDATE, B.FSPLAY_TIME, B.FSDUR from TBPGM_QUEUE A 
	left join TBPGM_COMBINE_QUEUE B on A.FSPROG_ID = B.FSPROG_ID and A.FSCHANNEL_ID = B.FSCHANNEL_ID and A.FDDATE = B.FDDATE
	where B.FDDATE between '2016-08-06' and '2016-08-22' and B.FSCHANNEL_ID in (12, 13, 14) and B.FSPROG_NAME like '%2016里約奧運%'
	order by B.FDDATE, B.FSPLAY_TIME, B.FSCHANNEL_ID  --217

	select A.FSPROG_ID, A.FSPROG_NAME, A.FSCHANNEL_ID, A.FDDATE, A.FSBEG_TIME, A.FSEND_TIME, 
	B.FSPROG_ID, B.FSPROG_NAME, B.FNBREAK_NO, B.FSCHANNEL_ID, B.FDDATE, B.FSPLAY_TIME, B.FSDUR, (DATEPART(hh, CAST(B.FSDUR as DATETIME)) * 60 + DATEPART(mi, CAST(B.FSDUR as DATETIME))) as DURATION
	from TBPGM_QUEUE A 
	left join TBPGM_COMBINE_QUEUE B on A.FSPROG_ID = B.FSPROG_ID and A.FSCHANNEL_ID = B.FSCHANNEL_ID and A.FDDATE = B.FDDATE
	where B.FDDATE between '2016-08-06' and '2016-08-22' and B.FSCHANNEL_ID in (12, 13, 14) and B.FSPROG_NAME like '%2016里約奧運%'
	order by B.FDDATE, B.FSPLAY_TIME, B.FSCHANNEL_ID  --217

	select A.FSPROG_NAME, A.FSPROG_ID, B.FSPROG_ID, B.FSPROG_NAME, B.FSCHANNEL_ID, C.FSCHANNEL_NAME, B.FDDATE, A.FSBEG_TIME, B.FSPLAY_TIME, B.FSDUR, (DATEPART(hh, CAST(B.FSDUR as DATETIME)) * 60 + DATEPART(mi, CAST(B.FSDUR as DATETIME))) as DURATION
	from TBPGM_QUEUE A 
	left join TBPGM_COMBINE_QUEUE B on A.FSPROG_ID = B.FSPROG_ID and A.FSCHANNEL_ID = B.FSCHANNEL_ID and A.FDDATE = B.FDDATE
	join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID
	where B.FDDATE between '2016-08-06' and '2016-08-22' and B.FSCHANNEL_ID in (12, 13, 14) and B.FSPROG_NAME like '%2016里約奧運%'
	group by A.FSPROG_ID, A.FSPROG_NAME, A.FDDATE, A.FSBEG_TIME, B.FSPROG_ID, B.FSPROG_NAME, B.FSCHANNEL_ID, B.FDDATE, B.FSPLAY_TIME, B.FSDUR, C.FSCHANNEL_NAME
	order by B.FSCHANNEL_ID, B.FDDATE, A.FSBEG_TIME  --217  有重複資料，不同時間播出的同一支節目長度不同

	select  B.FSPROG_ID, B.FSPROG_NAME, B.FSCHANNEL_ID, C.FSCHANNEL_NAME, B.FDDATE, B.FSPLAY_TIME, B.FSDUR, (DATEPART(hh, CAST(B.FSDUR as DATETIME)) * 60 + DATEPART(mi, CAST(B.FSDUR as DATETIME))) as DURATION
	from TBPGM_QUEUE A 
	left join TBPGM_COMBINE_QUEUE B on A.FSPROG_ID = B.FSPROG_ID and A.FSCHANNEL_ID = B.FSCHANNEL_ID and A.FDDATE = B.FDDATE
	join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID
	where B.FDDATE between '2016-08-06' and '2016-08-22' and B.FSCHANNEL_ID in (12, 13, 14) and B.FSPROG_NAME like '%2016里約奧運%'
	group by B.FSPROG_ID, B.FSPROG_NAME, B.FSCHANNEL_ID, B.FDDATE, B.FSPLAY_TIME, B.FSDUR, C.FSCHANNEL_NAME
	order by B.FSCHANNEL_ID, B.FDDATE, B.FSPLAY_TIME --123

	select  B.FNCONBINE_NO, B.FSPROG_ID, B.FSPROG_NAME, B.FSCHANNEL_ID, C.FSCHANNEL_NAME, B.FDDATE, B.FSPLAY_TIME, B.FSDUR, (DATEPART(hh, CAST(B.FSDUR as DATETIME)) * 60 + DATEPART(mi, CAST(B.FSDUR as DATETIME))) as DURATION
	from TBPGM_QUEUE A 
	left join TBPGM_COMBINE_QUEUE B on A.FSPROG_ID = B.FSPROG_ID and A.FSCHANNEL_ID = B.FSCHANNEL_ID and A.FDDATE = B.FDDATE
	join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID
	where B.FDDATE between '2016-08-06' and '2016-08-22' and B.FSCHANNEL_ID in (12, 13, 14) and B.FSPROG_NAME like '%2016里約奧運%'
	group by B.FNCONBINE_NO, B.FSPROG_ID, B.FSPROG_NAME, B.FSCHANNEL_ID, B.FDDATE, B.FSPLAY_TIME, B.FSDUR, C.FSCHANNEL_NAME
	order by B.FSCHANNEL_ID, B.FDDATE, B.FNCONBINE_NO, B.FSPLAY_TIME --123


	select A.FSPROG_ID, A.FSPROG_NAME, A.FSBEG_TIME, A.FSEND_TIME, A.FSCHANNEL_ID,
	B.FSPROG_ID, B.FSPROG_NAME, B.FDDATE, B.FSPLAY_TIME, B.FSCHANNEL_ID, B.FSDUR
	 from TBPGM_QUEUE A left join TBPGM_COMBINE_QUEUE B on A.FSPROG_ID = B.FSPROG_ID and A.FSCHANNEL_ID = B.FSCHANNEL_ID and A.FDDATE = B.FDDATE
	where CAST(B.FDDATE as DATE) = '2016-08-06' and A.FSCHANNEL_ID = '14' and A.FSPROG_ID = '2017226' order by A.FSBEG_TIME -- 2016里約奧運  射箭 2500 播出

	select top 100 * from TBPGM_COMBINE_QUEUE

	select CAST('01:45:00' as TIME) + CAST('01:45:00' as TIME) --add 運算子的運算元資料類型 time 無效
	declare @t DATETIME = '01:45:23'
	select DATEPART(hh, @t), DATEPART(mi, @t), DATEPART(s, @t)


	select * from TBPGM_COMBINE_QUEUE where CAST(FDDATE as DATE) = '2016-08-09' and FSCHANNEL_ID = '12' order by FSPLAY_TIME
	select * from TBZCHANNEL
end

if @case = 201702101205 begin --MAM 無法調用新片庫檔案
	select * from TBUSERS where FSUSER_ChtName = '洪潤德' --bobohung
end


if @case = 201702101333 begin --TSM recall fail
	select * from TBTSM_JOB where FNTSMJOB_ID in (21544, 21578, 214602)  --2124602 FNSTATUS = 2
	exec SP_Q_INFO_BY_VIDEOID '00002NYE' --2016公視台呼_吳朋奉HD, P201606006680001 tsm recall fail, 主控的 server 還有檔案, \\mamstream\MAMDFS\ptstv\HVideo\2016\20160600668\P201606006680001.mxf
end

if @case = 201702111701 begin
	exec SP_Q_INFO_BY_VIDEOID '00002E48'
end

---------------------------------------------20170213---------------------------------------
if @case = 201702130934 begin
	exec SP_Q_INFO_BY_VIDEOID '00002JUD' --野性加拿大 4 2017-02-14, G201700100040003
	exec SP_Q_INFO_BY_VIDEOID '00002KY1' --就是這Young! 10  2017-02-16 G201644200100001
end


if @case = 201702131123 begin
	select * from TBTRACKINGLOG where FSPROGRAM like '%fnGetTsmDB2QueryDSN%' and FSMESSAGE like '%DB2_DSN%' --no record
	select * from TBTRACKINGLOG where FSMESSAGE like '%DB2_DSN%'
end

if @case = 201702131702 begin
	select * from TBLOG_VIDEO where FSID = '20170200293'
	select * from TBUSERS where FSUSER_ChtName = '鄧潔' --prg1182, 07174
	select * from TBLOG_VIDEO where FSCREATED_BY = '07174' and CAST(FDCREATED_DATE as DATE) = '2017-02-13'
	select B.FSPROMO_NAME, A.FSTRACK, A.* from TBLOG_VIDEO A left join TBPGM_PROMO B on A.FSID = B.FSPROMO_ID where A.FSCREATED_BY = '07174' and A.FSTYPE = 'P' order by A.FDCREATED_DATE

	select * from TBPGM_PROMO where FSCREATED_BY = '07174' order by FDCREATED_DATE
end

if @case = 201702132208 begin
	select B.NAME, A.FCFILE_STATUS, A.* from TBLOG_VIDEO A join
	(select FSPROG_ID FSID, FSPGMNAME NAME from TBPROG_M union (select FSPROMO_ID FSID, FSPROMO_NAME NAME from TBPGM_PROMO)) B
	on A.FSID = B.FSID
	where CAST(A.FDUPDATED_DATE as DATE) = '2017-02-13' order by A.FDUPDATED_DATE

	select * from TBTRACKINGLOG where FDTIME > '2017-02-13 22:20' order by FDTIME --高解搬檔失敗：/tcbuffer/18TB/IngestWorkingTemp/G201207801260010.mxf/10.13.210.15 -l root -pw TSMP@ssw0rdTSM "/Media/WorkingTemp/MoveMetaSANFile /tcbuffer/18TB/00000000.lock /tcbuffer/18TB/IngestWorkingTemp/G201207801260010.mxf /ptstv/HVideo/2012/2012078/G201207801260010.mxf"/(Error)Source /tcbuffer/18TB/IngestWorkingTemp/G201207801260010.mxf is not exist.
end


-------------------------------201702151314-------------------------------------
if @case = 201702151315 begin
	-- MAM : DUR = EOM - SOM
	exec SP_Q_INFO_BY_VIDEOID '00003AGF' --01:00:00;00 ~ 01:24:10;00  XDCAMViewer EOM = 01:24:09:29
	exec SP_Q_INFO_BY_VIDEOID '00002JQM'
end

if @case = 201702151542 begin
	select * from TBTRACKINGLOG where FDTIME > '2017-02-15 15:39' order by FDTIME
end

if @case = 201702151609 begin --檔案已過期，tape offline A00207L4 為什麼只備份一支？
	select * from TBLOG_VIDEO where FSFILE_NO = 'P201404000920001' --2014水果冰淇淋-音樂迷宮小勇士徵求 FSID=20140400092, 201404030061 \\mamstream\MAMDFS\ptstv\HVideo\2014\20140400092\P201404000920001.mxf
	select * from TBPGM_PROMO where FSPROMO_ID = '20140400092'
	--將磁帶A00207L4 放回
	--dsmrecall /ptstv/HVideo/2014/20140400092/P201404000920001.mxf
	select * from TBUSERS where FSUSER_ChtName = '劉燕樟' --prg70411
end

------------------------------ 201702161119 begin --------------------------------------------
if @case = 201702161119 begin
	select * from TBTRACKINGLOG where FDTIME > '2017-02-09' and FSPROGRAM like '%CheckoutTapes_TSM3310%' order by FDTIME --WSTSM/CheckoutTapes_TSM3310 使用者08307要從TS3310退出磁帶
	select * from TBTRACKINGLOG where FDTIME > '2017-02-01' and FSPROGRAM like '%TSM3310%' order by FDTIME  --2017-02-13 12:35:39.757 WSTSM/CheckinTapes_TSM3310 失敗
end

if @case = 201702161730 begin
	select * from TBZCHANNEL
	select * from TBPGM_ARR_PROMO where FDDATE = '2017-02-17' and FSCHANNEL_ID = '14' order by FSPLAYTIME
	select * from VW_PLAYLIST_FROM_NOW where  FDDATE = '2017-02-17' and FSCHANNEL_ID = '14' order by FSPLAY_TIME
end

if @case = 201702171111 begin
	select * from TBPGM_PROMO where FSPROMO_NAME like '%住宅地震基本保險%' --2017公益-住宅地震基本保險-家園篇, 20170100224

	--60
    select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20170100224'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	--60
	select p.FSPROMO_NAME 短帶名稱, p.FSWELFARE 公益託播單號 ,pa.FDDATE 播出日期, pa.FSPLAYTIME 播出時間
    from TBPGM_ARR_PROMO pa join TBPGM_PROMO p on pa.FSPROMO_ID= p.FSPROMO_ID where p.FSWELFARE <> '' and pa.FSCHANNEL_ID = '07'
    and p.FSPROMO_NAME = '2017公益-住宅地震基本保險-家園篇'  order by pa.FDDATE, pa.FSPLAYTIME
end

if @case = 201702171353 begin
	select * from TBPROG_M where FSPGMNAME like '%客家新聞雜誌%' --2006262 客家新聞雜誌(客家製播)-242集排播需注意
	select * from TBLOG_VIDEO where FSID = '2006262' and FNEPISODE = 504 --G200626205040002, S, 201609010116, 00002Y9C
	exec SP_Q_INFO_BY_VIDEOID '00002Y9C' --2016-09-02 09:47:54.593

	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G200626205040002'
    update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G200626205040002'

	--Anystream 處理錯誤，reschedule ok
end

if @case = 201702171611 begin --已排播的檔案，被置換，但是新檔案仍是待轉檔，Approved 的原檔案會被刪除
	exec SP_Q_INFO_BY_VIDEOID '000035CG' --2016693 大腦先生 7, D, 201611180061, G201669300070001
	select * from TBLOG_VIDEO where FSID = '2016693' and FNEPISODE = 7 --201702160173 2/16 置換 所以 Approved 被刪除

	exec SP_Q_INFO_BY_VIDEOID '00003DMO' --0000072 我們的島 893, 201702150008, G000007208930002
	select * from TBPGM_COMBINE_QUEUE where FSPROG_ID = '2016693' and FNEPISODE = 7 order by FDDATE --2017-02-17
	select * from TBPGM_COMBINE_QUEUE where FSPROG_ID = '0000072' and FNEPISODE = 893 order by FDDATE
end

if @case = 201702181229 begin
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-02-17' and FSPROGRAM like '%CallIngest%' order by FDTIME
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-02-18' and FSPROGRAM like '%WSASC%' order by FDTIME
	select * from TBTRACKINGLOG where FDTIME between '2017-02-18 13:30' and '2017-02-18 13:50' order by FDTIME
end


if @case = 201702210929 begin --SD 播出帶轉檔成功搬檔失敗，SD 不轉高解 所以 18T 無檔案
	--T:\MAMUpload_Fiber\PTS\SD\0220-sdN10-8400.mxf
	select * from TBLOG_VIDEO where FSFILE_NO = 'G000016300240001' --R, 201702200109, 紀錄觀點  24 \\mamstream\MAMDFS\ptstv\HVideo\0000\0000163\G000016300240001.mxf
	--update TBLOG_VIDEO set FCFILE_STATUS = 'T' where FSFILE_NO = 'G000016300240001'
	--update TBLOG_VIDEO set FCLOW_RES = 'Y' where FSFILE_NO = 'G000016300240001'
	--update TBLOG_VIDEO set FCKEYFRAME = 'N' where FSFILE_NO = 'G000016300240001'  --FCKEYFRAME = N-> R -> Y
	select * from TBPROG_D where FSPROG_ID = '0000163' and FNEPISODE = 24
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G000016300240001'
	select * from TBLOG_VIDEO where FSID = '0000163' and FNEPISODE = 286 --G000016302860008
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G000016302860008'

	--T:\MAMUpload_Fiber\PTS\SD\0220-sdN9-6842.mxf
	select * from TBLOG_VIDEO where FSFILE_NO = 'G000016300220001'  --\\mamstream\MAMDFS\ptstv\HVideo\0000\0000163\G000016300220001.mxf

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-02-20' and 
	(FSMESSAGE like '%155732%' or FSMESSAGE like '%216080%' or FSMESSAGE like '%106687%' or FSMESSAGE like '%G201757400010002%')
	order by FDTIME

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-02-20' and 
	(FSMESSAGE like '%155730%' or FSMESSAGE like '%216078%' or FSMESSAGE like '%106685%' or FSMESSAGE like '%G000016300220001%')
	order by FDTIME --高解搬檔失敗：/tcbuffer/18TB/IngestWorkingTemp/G000016300220001.mxf/10.13.210.16 -l root -pw TSMP@ssw0rdTSM "/Media/WorkingTemp/MoveMetaSANFile /tcbuffer/18TB/00000000.lock /tcbuffer/18TB/IngestWorkingTemp/G000016300220001.mxf /ptstv/HVideo/0000/0000163/G000016300220001.mxf"/(Error)Source /tcbuffer/18TB/IngestWorkingTemp/G000016300220001.mxf is not exist.


	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-02-20' and 
	(FSMESSAGE like '%155718%' or FSMESSAGE like '%216066%' or FSMESSAGE like '%106673%' or FSMESSAGE like '%G000016300180001%')
	order by FDTIME --高解搬檔失敗：/tcbuffer/18TB/IngestWorkingTemp/G000016300180001.mxf/10.13.210.16 -l root -pw TSMP@ssw0rdTSM "/Media/WorkingTemp/MoveMetaSANFile /tcbuffer/18TB/00000000.lock /tcbuffer/18TB/IngestWorkingTemp/G000016300180001.mxf /ptstv/HVideo/0000/0000163/G000016300180001.mxf"/(Error)Source /tcbuffer/18TB/IngestWorkingTemp/G000016300180001.mxf is not exist.
end

if @case = 201702211358 begin --修改段落
	exec SP_Q_INFO_BY_VIDEOID '00003E6M' --大腦先生 #21 promo, 201702200088
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00003E6M' --P201702005330001, 01:17:59;28 ~ 01:18:30;00
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'P201702005330001' --01:17:59;28 ~ 01:18:30;00

	--update TBLOG_VIDEO set FSBEG_TIMECODE = '01:18:00;02', FSEND_TIMECODE = '01:18:30;00', FSUPDATED_BY= '51204', FDUPDATED_DATE=GETDATE()
    --where FSFILE_NO = 'P201702005330001'
	--exec SP_U_TBLOG_VIDEO_SEG_BYFILEID 'P201702005330001', '01', '01:18:00;02', '01:18:30;00', '51204'

	select * from TBPGM_ARR_PROMO where CAST(FDDATE as DATE) = '2017-02-23' and FSCHANNEL_ID = '12' order by FNARR_PROMO_NO
end

----------------------------------------20170222----------------------------------------

if @case = 201702211714 begin --hakka 3/12 P201612005540001(短帶已刪除）但是
	exec SP_Q_INFO_BY_VIDEOID '00003DMG'
	select * from TBPGM_PROMO where FSPROMO_NAME like '%2016RM-創業有事嗎%' --20161100619:2016RM-創業有事嗎? 20161200554:2016RM-創業有事嗎 (D)
	select * from TBPGM_PROMO where FSPROMO_ID = '20161100619' --2016RM-創業有事嗎?
	select * from TBPGM_PROMO where FSPROMO_ID = '20161200554' --2016RM-創業有事嗎  2016-12-22 17:33 安慈 deleted

	select * from TBLOG_VIDEO where FSID = '20161100619' --P201611006190001 00003602 -> P201611006190003, 00003E7V
	select * from TBLOG_VIDEO where FSID = '20161200554' --P201612005540001, 000038GU B, 2016-12-21 16:20 created

	select * from TBLOG_VIDEO where FSFILE_NO = 'P201611006190002' --no recorder
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00003DMG' --no record
	select * from TBLOG_AUDIO where FSFILE_NO = 'P201611006190002'
	select * from TBLOG_DOC where FSFILE_NO = 'P201611006190002'

	select * from TBPGM_ARR_PROMO where CAST(FDDATE as DATE) = '2017-03-12' order by FNARR_PROMO_NO --2017-02-20 11:31:20.750

	exec SP_Q_INFO_BY_VIDEOID '000038GU'

	select * from TBPGM_ARR_PROMO where CAST(FDDATE as DATE) between '2016-12-10' and '2017-03-20' and FSPROMO_ID = '20161200554' order by FDDATE --2017-03-12 00003DMG Wrong ID. 2017-02-20 11:31 created
	select * from TBPGM_ARR_PROMO where CAST(FDDATE as DATE) between '2016-12-10' and '2017-03-20' and FSPROMO_ID = '20161100619' order by FDDATE --2017-03-05, 00003E7V correct, 2017-02-20 17:24 created

	-- 預排短帶
	select * from TBPGM_BANK_DETAIL
	select * from TBPGM_PROMO_BOOKING where FSPROMO_ID = '20161100619'  --託播單
	select * from TBPGM_PROMO_SCHEDULED where FSPROMO_ID = '20161100619'

end


if @case = 201702221048 begin --檢索出現兩筆相同但是
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201749400010002'
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201749400010002' --有兩筆
end

if @case = 201702221740 begin --檢查 TS3500 退帶紀錄
	select * from TBTRACKINGLOG where FDTIME between '2017-02-20' and '2017-02-23' and  FSPROGRAM like '%CheckoutTapes_TSM3500%' order by FDTIME
end

if @case = 201702221835 begin
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-02-20' and FSPROGRAM like '%CallIngest%' order by FDTIME
end

----------------------------------------20170223----------------------------------------
if @case = 201702230937 begin --波西測試一二
	select * from TBPROG_M where FSPGMNAME like '%波西測試%' --2013232
	select * from TBLOG_VIDEO where FSID = '2013232' and FNEPISODE = 1 and FSARC_TYPE = '002' and FCFILE_STATUS = 'T' --G201323200010067 無法複製置換段落 todo
	exec SP_Q_INFO_BY_VIDEOID '00003C7M'  --G201323200010067 T

	select * from TBLOG_VIDEO where FSID = '2013232' and FSARC_TYPE = '002' and FCFILE_STATUS in ('B', 'T')

	exec SP_Q_INFO_BY_VIDEOID '00002JD0'  --4 上傳失敗 201605050115
	--update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201323200040004' --改成待審
	--審核不過 FCSTATUS = X, FCFILE_STATUS = F 但無法新增送播單

end


if @case = 201702231135 begin --短帶 QUE 表
    select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20170100295'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20170100296'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20170100119'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20160900123'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	--no record
	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20160900122'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	select * from TBPGM_PROMO where FSPROMO_ID = '20160900122' --106年外交小尖兵徵件CF

	select * from TBPGM_ARR_PROMO where FSPROMO_ID = '20160900122'
end

if @case = 201702231429 begin --誰來晚餐調用轉檔失敗
	select * from TBBOOKING_MASTER where FSBOOKING_NO = '201702210033' --70718
	select * from TBBOOKING_DETAIL where FSBOOKING_NO = '201702210033' --FCFILE_TRANSCODE_STATUS = F G201680200080002
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201680200080002' --\\mamstream\MAMDFS\ptstv\HVideo\2016\2016802\G201680200080002.mxf
	select * from TBUSERS where FSUSER_ID = '70718' --鄭文欣 hsincheng
	--dsmrecall /ptstv/HVideo/2016/2016802/G201680200080002.mxf
end

if @case = 201702231551 begin --我們的島 低解檢索未出來，keyframe 有問題
	select * from TBLOG_VIDEO where FSFILE_NO = 'G000007208890001' --T, 00003ACB, 201701090098, 0000072 889
	--\\mamstream\MAMDFS\ptstv\HVideo\0000\0000072\G000007208890001.mxf, \\mamstream\MAMDFS\Media\Lv\ptstv\0000\0000072\G000007208890001.mp4
	exec SP_Q_INFO_BY_VIDEOID '00003ACB'  --TRAN DATE : 2017-01-10 10:05
	select * from TBPROG_M where FSPROG_ID = '0000072' --我們的島
	--低解檔有問題

	--anystream reschedule 16:58 ~ 17:38
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-02-23' and 
	(FSMESSAGE like '%152277%' or FSMESSAGE like '%207549%' or FSMESSAGE like '%103470%' or FSMESSAGE like '%003ACB%' or FSMESSAGE like '%G000007208890001%')
	order by FDTIME

	--HSM4 : PID=16147 cp-r /mnt/HDUpload/003ACB.mxf /ptstv/HVideo/0000/0000072/G000007208890001.mxf  17:38 ~ 18:05
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G000007208890001' --兩筆紀錄

	--keyframe 未更新
	--update TBLOG_VIDEO set FCKEYFRAME = 'N' where FSFILE_NO = 'G000007208890001'
end

if @case = 201702231607 begin --調用無法顯示預借清單 --todo
	select * from TBBOOKING_MASTER where FSUSER_ID = '51204' --201701030164, 201701200071, 201702130112, 
	select * from TBBOOKING_DETAIL where FSBOOKING_NO = '201701030164'
	select * from TBPREBOOKING where FSUSER_ID = '51204'  --news FSFILE_CATEGORY = 5
end


----------------------------------------20170224----------------------------------------

if @case = 201702241530 begin
	exec SP_Q_INFO_BY_VIDEOID '000038QO' --20161200633, P201612006330001
	select * from TBPGM_PROMO where FSPROMO_ID = '20161200633' --2017-02-22 00:00:00.000 暗香風華第115集陳隆昊預告 到期日為 2/22 ，排表 2/22 仍可以排入該短帶
	select * from TBLOG_VIDEO where FSFILE_NO = 'P201612006330001' --2017-02-23 01:56:08.790, 201612230099， 系統 2/23 才執行刪除
end

if @case = 201702241649 begin
	select * from TBTRACKINGLOG where FDTIME = '2017-02-24 16:15:10.907' order by FDTIME
end

if @case = 201702241658 begin --已下架 A00303L4
	exec SP_Q_INFO_BY_VIDEOID '000019ZU' --白色王子 G201489100010001 \\mamstream\MAMDFS\ptstv\HVideo\2014\2014891\G201489100010001.mxf
end

if @case = 201702241726 begin --檔案已入庫，但是仍在 Review，排播後不會由 GPFS 到 Approved
	exec SP_Q_INFO_BY_VIDEOID '00003CXJ' --2017-02-16 14:21:08.043
	exec SP_Q_INFO_BY_VIDEOID '00003DRI' --2017-02-17 12:18:46.950
	exec SP_Q_INFO_BY_VIDEOID '00003DUX' --2017-02-17 17:38:06.307
	exec SP_Q_INFO_BY_VIDEOID '00003DQT' --2017-02-16 12:16:49.003
	exec SP_Q_INFO_BY_VIDEOID '00003853'
	exec SP_Q_INFO_BY_VIDEOID '00003AOL'
	exec SP_Q_INFO_BY_VIDEOID '00003AOO'

	exec SP_Q_INFO_BY_VIDEOID '00003DQT'
	exec SP_Q_INFO_BY_VIDEOID '00002WQJ'
	exec SP_Q_INFO_BY_VIDEOID '00003DVL'

	exec SP_Q_INFO_BY_VIDEOID '00003EOH'
	
	exec SP_Q_GET_IN_TAPE_LIBRARY_LIST_MIX

	select getdate()+5

	select FSVIDEO_PROG, FDUPDATED_DATE, FCFILE_STATUS from TBLOG_VIDEO where FSVIDEO_PROG in (
	'00003CXJ', '00003DRI', '00003DUX', '00003DQT', '00003853', '00003AOL', '00003AOO', '00003DQT', '00002WQJ', '00003DVL', '00003EOH'
	,'00001R8I', '00003DVL', '00002H0A', '00003EMZ', '00003BVG', '00003EN0'
	)	order by  FDUPDATED_DATE

	exec SP_Q_INFO_BY_VIDEOID '00003C2W' --tran 2/17
	exec SP_Q_INFO_BY_VIDEOID '00002ISE'
end



----------------------------------------20170227----------------------------------------

if @case = 201702271658 begin --已轉檔未審核但是檔案已在 Review
	exec SP_Q_INFO_BY_VIDEOID  '000002QB'
	exec SP_Q_INFO_BY_VIDEOID  '000008NR'
end


----------------------------------------20170303----------------------------------------
if @case = 201703030630 begin
	select * from TBSYSTEM_CONFIG
end

----------------------------------------20170306----------------------------------------
if @case = 201703061417 begin --已轉檔由 GPFS 搬到 Review，但是 TBLOG_VIDEO_HD_MC.FCSTATUS = ''
	exec SP_Q_INFO_BY_VIDEOID '00001N50' --G000081614770008
	exec SP_Q_INFO_BY_VIDEOID '00001NF9' --G000081614780002
	exec SP_Q_INFO_BY_VIDEOID '00001NP4' --G000081614790002
	exec SP_Q_INFO_BY_VIDEOID '000037R3' --G000006019890005
	exec SP_Q_INFO_BY_VIDEOID '00001O3Y' --G201562400010002
	--update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G000081614770008'
	--update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G000081614780002'
	--update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G000081614790002'
	--update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G000006019890005'
	--update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201562400010002'
	select * from TBTRACKINGLOG where FDTIME > '2017-03-06 14:29' order by FDTIME

	exec SP_Q_GET_IN_TAPE_LIBRARY_LIST_MIX
end

if @case = 201703061511 begin --其他影片  狀態是等待搬檔 --todo
	select * from TBPROG_M where FSPGMNAME like '%台灣心動線%' --2015105:台灣心動線-戀上台灣
	select * from TBLOG_VIDEO where FSID = '2015105' and FNEPISODE = 44 --G201510500440014, R, 007, HD完成帶, 026 :T
	select * from TBBROADCAST where FSBRO_ID = '201703020094' --FCCHECK_STATUS = K
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201510500440015' --G201510500440015, 026, 其他影片, B, 201703020094--> 但是轉檔中心轉檔看不到(因為不轉檔)，在其他影片
	select * from TBFILE_TYPE

	select * from TBLOG_VIDEO where FSARC_TYPE = '026' and FDUPDATED_DATE > '2017-03-02'

end


if @case = 201703061554 begin --無法強制失敗,轉檔完成
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-03-02' and 
	(FSMESSAGE like '%156437%' or FSMESSAGE like '%217883%' or FSMESSAGE like '%107451%' or FSMESSAGE like '%003F5M%' or FSMESSAGE like '%G000016300920001%')
	order by FDTIME

	select * from TBLOG_VIDEO where FSBRO_ID = '201703020105' --G000016300920001, 001, S, 0000163, 92  紀錄觀點
	select * from TBLOG_VIDEO where FSFILE_NO = 'G000016300920001' --S. 001, FCLOG_RES = R
	
	--update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G000016300920001' --> 轉檔成功搬檔失敗 -> 重新轉檔
end

if @case = 201703061616 begin --民歌40  再唱一段思想起演唱會(90分鐘版) 由 GPFS -> Review FSSTATUS = ''
	exec SP_Q_INFO_BY_VIDEOID '00002D72' --G201669200040001, FCSTATUS = ''  \\mamstream\MAMDFS\ptstv\HVideo\2016\2016692\G201669200040001.mxf
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201669200040001'
end

if @case = 201703061134 begin --標記上傳失敗
	exec SP_Q_INFO_BY_VIDEOID '00003F58' --E, 201703020086
	exec SP_Q_INFO_BY_VIDEOID '00003EKI' --201702230159
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'E' where FSFILE_NO = 'G201719400040001' --改為失敗，重新標記
	exec SP_Q_INFO_BY_VIDEOID '00003F61'
end

if @case = 201703071143 begin  --檔案已在 Review 但是 QC 無法審核 FCSTATUS = ''
	exec SP_Q_INFO_BY_VIDEOID '00001SY9' --G201582900010003
	exec SP_Q_INFO_BY_VIDEOID '00002EML' --G201680700010003
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201582900010003'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201680700010003'
end


if @case = 201703071207-201609010935-201607271724 begin --18T 轉移到 \\10.13.200.22\mamnas1

		----TEST---------
        select FSSYSTEM_CONFIG into #tmp_TBSYSTEM_CONFIG from TBSYSTEM_CONFIG
        select * from #tmp_TBSYSTEM_CONFIG
		select FSSYSTEM_CONFIG from TBSYSTEM_CONFIG

        update #tmp_TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<INGEST_TEMP_DIR_UNC Desc="轉檔暫存區">T:\IngestWorkingTemp\</INGEST_TEMP_DIR_UNC>', 
        '<INGEST_TEMP_DIR_UNC Desc="轉檔暫存區">\\10.13.200.22\mamnas1\</INGEST_TEMP_DIR_UNC>')

		update #tmp_TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<INGEST_TEMP_DIR_LINUX Desc="轉檔暫存區">/tcbuffer/18TB/IngestWorkingTemp/</INGEST_TEMP_DIR_LINUX>', 
        '<INGEST_TEMP_DIR_LINUX Desc="轉檔暫存區">/mnt/mamnas1/</INGEST_TEMP_DIR_LINUX>')

		update #tmp_TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<METASAN_CHECK_FILE Desc="檢測檔位置">/tcbuffer/18TB/00000000.lock</METASAN_CHECK_FILE>', 
        '<METASAN_CHECK_FILE Desc="檢測檔位置">/mnt/mamnas1/00000000.lock</METASAN_CHECK_FILE>')

		----------recover----------------
		update #tmp_TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<INGEST_TEMP_DIR_UNC Desc="轉檔暫存區">\\10.13.200.22\mamnas1\</INGEST_TEMP_DIR_UNC>',
		'<INGEST_TEMP_DIR_UNC Desc="轉檔暫存區">T:\IngestWorkingTemp\</INGEST_TEMP_DIR_UNC>'
        )

		update #tmp_TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<INGEST_TEMP_DIR_LINUX Desc="轉檔暫存區">/mnt/mamnas1/</INGEST_TEMP_DIR_LINUX>', 
		'<INGEST_TEMP_DIR_LINUX Desc="轉檔暫存區">/tcbuffer/18TB/IngestWorkingTemp/</INGEST_TEMP_DIR_LINUX>'
        )

		update #tmp_TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<METASAN_CHECK_FILE Desc="檢測檔位置">/mnt/mamnas1/00000000.lock</METASAN_CHECK_FILE>',
		'<METASAN_CHECK_FILE Desc="檢測檔位置">/tcbuffer/18TB/00000000.lock</METASAN_CHECK_FILE>'
        )



		-------------記得要 dump 出來 diff 比對結果
        update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<INGEST_TEMP_DIR_UNC Desc="轉檔暫存區">T:\IngestWorkingTemp\</INGEST_TEMP_DIR_UNC>', 
        '<INGEST_TEMP_DIR_UNC Desc="轉檔暫存區">\\10.13.200.22\mamnas1\IngestWorkingTemp\</INGEST_TEMP_DIR_UNC>')

		update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<INGEST_TEMP_DIR_LINUX Desc="轉檔暫存區">/tcbuffer/18TB/IngestWorkingTemp/</INGEST_TEMP_DIR_LINUX>', 
        '<INGEST_TEMP_DIR_LINUX Desc="轉檔暫存區">/mnt/mamnas1/IngestWorkingTemp/</INGEST_TEMP_DIR_LINUX>')

		update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<METASAN_CHECK_FILE Desc="檢測檔位置">/tcbuffer/18TB/00000000.lock</METASAN_CHECK_FILE>', 
        '<METASAN_CHECK_FILE Desc="檢測檔位置">/mnt/mamnas1/00000000.lock</METASAN_CHECK_FILE>')


		----------recover----------------
		update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<INGEST_TEMP_DIR_UNC Desc="轉檔暫存區">\\10.13.200.22\mamnas1\IngestWorkingTemp\</INGEST_TEMP_DIR_UNC>',
		'<INGEST_TEMP_DIR_UNC Desc="轉檔暫存區">T:\IngestWorkingTemp\</INGEST_TEMP_DIR_UNC>'
        )

		update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<INGEST_TEMP_DIR_LINUX Desc="轉檔暫存區">/mnt/mamnas1/IngestWorkingTemp/</INGEST_TEMP_DIR_LINUX>', 
		'<INGEST_TEMP_DIR_LINUX Desc="轉檔暫存區">/tcbuffer/18TB/IngestWorkingTemp/</INGEST_TEMP_DIR_LINUX>'
        )

		update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<METASAN_CHECK_FILE Desc="檢測檔位置">/mnt/mamnas1/00000000.lock</METASAN_CHECK_FILE>',
		'<METASAN_CHECK_FILE Desc="檢測檔位置">/tcbuffer/18TB/00000000.lock</METASAN_CHECK_FILE>'
        )

        select * from TBSYSTEM_CONFIG

		select * from TBTRACKINGLOG where FDTIME > '2017-03-07 20:15' order by FDTIME
end

if @case = 201703071535 begin --調用轉檔完成後檔案未搬到 MAMDownload
	select * from TBBOOKING_MASTER where FSBOOKING_NO in ('201703070013', '201703070028', '201703070040')
	select * from TBBOOKING_DETAIL where FSBOOKING_NO = '201703070013'
	select * from TBBOOKING_DETAIL where FSBOOKING_NO = '201703070028'
	select * from TBBOOKING_DETAIL where FSBOOKING_NO = '201703070040'
	select FSBOOKING_NO, FSFILE_NO, FCFILE_TRANSCODE_STATUS from TBBOOKING_DETAIL where FSBOOKING_NO = '201703070040' --G201278600010001, R, G201562300010002, F
	select * from TBLOG_VIDEO where FSFILE_NO in ('G201278600010001', 'G201562300010002') --G201278600010001 無法 recall
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201562300010002' --2015623
	select * from TBPROG_M where FSPROG_ID = '2015623' --只想比你多活一天(口述影像版)
end

if @case = 201703071548 begin
	exec SP_Q_INFO_BY_VIDEOID '00001NXT' --G000081614800002
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G000081614800002'
	exec SP_Q_INFO_BY_VIDEOID '00001Z1O' --O
	exec SP_Q_INFO_BY_VIDEOID '00001ONN' --G000081614810002
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G000081614810002'
	exec SP_Q_INFO_BY_VIDEOID '00002VE1' --G201680200140023 原本是檔案送播，後來改送帶轉檔
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201680200140023'
	exec SP_Q_INFO_BY_VIDEOID '000034OZ' --G201680200150011
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201680200150011'
	exec SP_Q_INFO_BY_VIDEOID '00001PFR'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G000081614830002'
	exec SP_Q_INFO_BY_VIDEOID '000034P0'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201680200160014'
	exec SP_Q_INFO_BY_VIDEOID '00003EMA'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G200928504630025'

	exec SP_Q_INFO_BY_VIDEOID '00001Z1P' --G201207801570001
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201207801570001'
end

if @case = 201703071720 begin
	select * from TBPROG_M where FSPGMNAME like '%波西測試%' --2013232
	select * from TBLOG_VIDEO where FSID = '2013232' and FNEPISODE = 1 and FSARC_TYPE = '002' and FCFILE_STATUS = 'T' --G201323200010067 無法複製置換段落 todo
	exec SP_Q_INFO_BY_VIDEOID '00003C7M'  --G201323200010067 T

	select * from TBLOG_VIDEO where FSID = '2013232' and FSARC_TYPE = '002' and FCFILE_STATUS in ('B', 'T')

	exec SP_Q_INFO_BY_VIDEOID '00002JD0'  --4 上傳失敗 201605050115
	--update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201323200040004' --改成待審
	--審核不過 FCSTATUS = X, FCFILE_STATUS = F 但無法新增送播單

	select * from TBLOG_VIDEO where FSID = '2013232' and FNEPISODE = 5 and FSARC_TYPE = '002' and FCFILE_STATUS = 'B' --201605050116, 00002JD1, G201323200050003

	--update TBLOG_VIDEO set FSBEG_TIMECODE = '01:02:30;00', FSEND_TIMECODE = '01:12:30;00', FSUPDATED_BY= '51204', FDUPDATED_DATE=GETDATE()
    --where FSFILE_NO = 'G201323200050003'
	--exec SP_U_TBLOG_VIDEO_SEG_BYFILEID 'G201323200050003', '01', '01:02:30;00', '01:12:30;00', '51204'

	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201323200050003'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201323200050003'

end

if @case = 201703071823 begin
	exec SP_Q_INFO_BY_VIDEOID '00000K7U' --G200747800010017 與男友的前女友密談 08
	exec SP_Q_INFO_BY_VIDEOID '00002ED9' --P201602004760001 2016main RM 誰來晚餐2 08
end


if @case = 201703072037 begin
	select * from TBLOG_VIDEO where FSFILE_NO = 'G000081613950004'  --R, 00003FMV, 201703070197 搬檔失敗
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-03-07' and 
	(FSMESSAGE like '%156798%' or FSMESSAGE like '%218767%' or FSMESSAGE like '%107837%' or FSMESSAGE like '%003FMV%' or FSMESSAGE like '%G000081613950004%')
	order by FDTIME

	select * from TBPROG_M where FSPROG_ID = '0000816' --下課花路米(1323集起HD可播) 1395
end

-------------------------------------20170308-------------------------------
if @case = 201703081047 begin --送檔轉檔，轉檔成功搬檔失敗
	select * from TBLOG_VIDEO where FSFILE_NO = 'D201703000030001' --T
    select * from TBLOG_VIDEO where FSFILE_NO = 'D201703000040001' --R --> T,  T:\MAMUpload_Fiber\PTS\HD\mamnas\0218-N5-2.mxf --檔案在 18T 搬到mamnas1

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-03-08' and 
	(FSMESSAGE like '%1%' or FSMESSAGE like '%218767%' or FSMESSAGE like '%107837%' or FSMESSAGE like '%003FMV%' or FSMESSAGE like '%G000081613950004%')
	order by FDTIME

	select * from TBTRACKINGLOG where FDTIME > '2017-03-08 10:56' order by FDTIME --高解搬檔失敗：/mnt/mamnas1/IngestWorkingTemp/D201703000040001.mxf/10.13.210.15 -l root -pw TSMP@ssw0rdTSM "/Media/WorkingTemp/MoveMetaSANFile /mnt/mamnas1/00000000.lock /mnt/mamnas1/IngestWorkingTemp/D201703000040001.mxf /ptstv/HVideo/2017/20170300004/D201703000040001.mxf"/(Error)Source /mnt/mamnas1/IngestWorkingTemp/D201703000040001.mxf is not exist.
end

if @case = 201703081135 begin --調用一直在轉檔中
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201099100010002' --\\mamstream\MAMDFS\ptstv\HVideo\2010\2010991\G201099100010002.mxf  tape offline
end

if @case = 201703081337 begin --送帶轉檔 output to mamnas1 測試
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201142100060017' --00003FN8
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201142100050015' --T, 00003FN7
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201142100040014' --S, 00003FN6
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201142100030017' --S, 00003FN5
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201142100020024' --S, 00003FN4
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201142100010030' --201703080003, 00003FN3
	
	select * from TBLOG_VIDEO where FSFILE_NO = 'P201703000950001'

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-03-08' and --Dur : 51:25 Transcode:09:28 ~ 13:36
	(FSMESSAGE like '%156810%' or FSMESSAGE like '%218851%' or FSMESSAGE like '%107847%' or FSMESSAGE like '%003FN7%' or FSMESSAGE like '%G201142100050015%')
	order by FDTIME

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-03-08' and --Dur : 51:25 Transcode:09:28 ~ 13:36
	(FSMESSAGE like '%156811%' or FSMESSAGE like '%218852%' or FSMESSAGE like '%107848%' or FSMESSAGE like '%003FN8%' or FSMESSAGE like '%G201142100060017%')
	order by FDTIME

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-03-06' and --Dur : 23:30 Transcode:09:28 ~ 13:36
	(FSMESSAGE like '%156659%' or FSMESSAGE like '%218478%' or FSMESSAGE like '%107695%' or FSMESSAGE like '%G000081613540008%')
	order by FDTIME

	select * from TBLOG_VIDEO where FSFILE_NO = 'G000081613970004'

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-03-03' and --Dur : 23:30 Transcode:09:28 ~ 13:36
	(FSMESSAGE like '%156522%' or FSMESSAGE like '%218074%' or FSMESSAGE like '%107548%' or FSMESSAGE like '%G201362500090011%')
	order by FDTIME --18T -> GPFS 22G, 2min

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-03-03' and --Dur : 23:30 Transcode:09:28 ~ 13:36
	(FSMESSAGE like '%156518%' or FSMESSAGE like '%218070%' or FSMESSAGE like '%107544%' or FSMESSAGE like '%G201362500070013%')
	order by FDTIME --18T -> GPFS 22G, 2min
end

if @case = 201703081850 begin --videoid in purge list has louth db
	exec SP_Q_INFO_BY_VIDEOID '00003E9T' --P201702005650001 3/5 purge
end

-----------------------------------20170309----------------------------------
if @case = 201703090904 begin  --todo
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201403600020009'
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201403600010008'
	select * from TBLOG_VIDEO where FSFILE_NO = 'G000081614000004'

	select * from TBLOG_VIDEO where CAST(FDUPDATED_DATE as DATE) = '2017-03-08' and FSARC_TYPE = '007' order by FDUPDATED_DATE  -- S:7
	select * from TBLOG_VIDEO where CAST(FDUPDATED_DATE as DATE) = '2017-03-08' and FSARC_TYPE = '007' and FCFILE_STATUS = 'T'  order by FDUPDATED_DATE --6
	select * from TBLOG_VIDEO where CAST(FDUPDATED_DATE as DATE) = '2017-03-08' and FCFILE_STATUS not in  ('T', 'B')  order by FDUPDATED_DATE
	select FSFILE_NO, FSBRO_ID, FCFILE_STATUS, FSARC_TYPE from TBLOG_VIDEO where CAST(FDUPDATED_DATE as DATE) = '2017-03-08' and FCFILE_STATUS = 'S' --007 完成帶 7, 020 HD資料帶 1
	select FSFILE_NO, FSBRO_ID, FCFILE_STATUS, FSARC_TYPE from TBLOG_VIDEO where CAST(FDUPDATED_DATE as DATE) = '2017-03-09' and FCFILE_STATUS = 'S'

	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201142100010030'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201142100020024'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201142100030017'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201142100040014'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G000081613980004'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201142100060017'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'D201703000020001'

	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201243900030011' --201703070128 測試一下搬檔效能, 重啟搬檔失敗

	select * from TBLOG_VIDEO where FSFILE_NO in ('G201142100010030', 'G201142100020024', 'G201142100030017', 'G201142100040014', 'G000081613980004', 
	'G201142100060017', 'D201703000020001', 'G201243900030011')

	select * from TBBROADCAST where FSBRO_ID = '201703070128' --2017-03-07 17:46:40.610

	--3/7 轉檔搬檔失敗  \\mamstream\MAMDFS\ptstv\HVideo\2012\2012439\G201243900030011.mxf 低解未入庫  高解檔正常, 重搬兩次都失敗
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-03-10' and --Dur : 23:30 Transcode:09:28 ~ 13:36
	(FSMESSAGE like '%156778%' or FSMESSAGE like '%218741%' or FSMESSAGE like '%107816%' or FSMESSAGE like '%G201243900030011%')
	order by FDTIME --job 到開始進行高解搬檔：/mnt/mamnas1/IngestWorkingTemp/G201243900030011.mxf

	select * from TBSYSTEM_CONFIG
end

if @case = 201703091016 begin --回朔檔案 搬入 Review 後 FCSTATUS = ''
	exec SP_Q_INFO_BY_VIDEOID '00002HSK' --G201486500010014  3/1 移入 Review 
	exec SP_Q_INFO_BY_VIDEOID '00002HSL' --G201486500020027
	exec SP_Q_INFO_BY_VIDEOID '00002HSG' --G201486500030014
	exec SP_Q_INFO_BY_VIDEOID '00002HSH' --G201486500040024
	exec SP_Q_INFO_BY_VIDEOID '00002HOQ' --G201486500050014

	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO in ('G201486500010014', 'G201486500020027', 'G201486500030014', 'G201486500040024', 'G201486500050014')

	exec SP_Q_INFO_BY_VIDEOID '00003C4G' --G201586400010008
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201586400010008'
end

if @case = 201703091208 begin --playlist 有 ID 但 no louth DB
	exec SP_Q_INFO_BY_VIDEOID '00003FSQ' --201703090035
end

if @case = 201703091504 begin
	select FDUPDATED_DATE, FSFILE_NO, FSBRO_ID, FSTYPE, FSARC_TYPE, FCFILE_STATUS, FSJOB_ID from TBLOG_VIDEO where CAST(FDUPDATED_DATE as DATE) = '2017-03-09' and FCFILE_STATUS = 'S' order by FDUPDATED_DATE
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201142100080014' --156916
	select * from TBTSM_JOB where FNTSMJOB_ID = 156916
	select * from TBTRANSCODE where FNTRANSCODE_ID = 156916 --FNANYSTREAM_ID = 107935
	select * from TBTSM_JOB where FNTSMJOB_ID = 219041 --Node13 TSM任務 FNSTATUS = 3 ok
	select * from TBTRANSCODE_RECV_LOG where FNJOB_ID = 107935

	--TBTRANSCODE.FNPROCESS_STATUS = 4 
	select A.FDUPDATED_DATE, A.FSFILE_NO, A.FSBRO_ID, A.FSTYPE, A.FSARC_TYPE, A.FCFILE_STATUS, A.FSJOB_ID, 
	B.FNTRANSCODE_ID, B.FNANYSTREAM_ID, B.FNPROCESS_STATUS,
	C.FNTSMJOB_ID, C.FNSTATUS, C.FDREGISTER_TIME, C.FDREGISTER_TIME, C.FSRESULT,
	D.FNJOB_ID, D.FSCONTENT, D.FDTIME 
	from TBLOG_VIDEO A left join TBTRANSCODE B on A.FSJOB_ID = B.FNTRANSCODE_ID
	left join TBTSM_JOB C on B.FNTSMDISPATCH_ID = C.FNTSMJOB_ID 
	left join TBTRANSCODE_RECV_LOG D on B.FNANYSTREAM_ID = D.FNJOB_ID
	where CAST(A.FDUPDATED_DATE as DATE) = '2017-03-09' and A.FCFILE_STATUS = 'S'
	order by A.FDUPDATED_DATE


	--ANS 處理錯誤
	select A.FDUPDATED_DATE, A.FSFILE_NO, A.FSBRO_ID, A.FSTYPE, A.FSARC_TYPE, A.FCFILE_STATUS, A.FSJOB_ID, 
	B.FNTRANSCODE_ID, B.FNANYSTREAM_ID, B.FNPROCESS_STATUS,
	C.FNTSMJOB_ID, C.FNSTATUS, C.FDREGISTER_TIME, C.FDREGISTER_TIME, C.FSRESULT,
	D.FNJOB_ID, D.FSCONTENT, D.FDTIME 
	from TBLOG_VIDEO A left join TBTRANSCODE B on A.FSJOB_ID = B.FNTRANSCODE_ID
	left join TBTSM_JOB C on B.FNTSMDISPATCH_ID = C.FNTSMJOB_ID 
	left join TBTRANSCODE_RECV_LOG D on B.FNANYSTREAM_ID = D.FNJOB_ID
	where CAST(A.FDUPDATED_DATE as DATE) = '2017-03-09' and B.FNTRANSCODE_ID = 156895 --no record 因為重啟後紀錄新的 FNTRANSCODE_ID
	order by A.FDUPDATED_DATE

	select * from TBTRANSCODE where FNTRANSCODE_ID = 156895 --FNPROCESS_STATUS
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201142100080014' --FSJOB_ID = 156916?
	select * from TBTRANSCODE where FNTRANSCODE_ID = 156916

	--查詢前一天後的轉檔任務
	select A.FNTRANSCODE_ID, A.FNANYSTREAM_ID, A.FNPROCESS_STATUS, A.FSNOTE,
	D.FDUPDATED_DATE, D.FSFILE_NO, D.FSBRO_ID, D.FSTYPE, D.FSARC_TYPE, D.FCFILE_STATUS, D.FSJOB_ID, 
	B.FNTSMJOB_ID, B.FNSTATUS, B.FDREGISTER_TIME, B.FSRESULT,
	C.FNJOB_ID, C.FSCONTENT, C.FDTIME 
	from TBTRANSCODE A left join TBTSM_JOB B on A.FNTSMDISPATCH_ID = B.FNTSMJOB_ID
	left join TBTRANSCODE_RECV_LOG C on A.FNANYSTREAM_ID = C.FNJOB_ID
	left join TBLOG_VIDEO D on A.FNTRANSCODE_ID = D.FSJOB_ID
	where CAST(B.FDREGISTER_TIME as DATE) >= GETDATE() -2
	order by A.FNTRANSCODE_ID

	select * from TBLOG_VIDEO where FSFILE_NO = 'G000016301100001' --156640

    select * from TBTRANSCODE where FNTRANSCODE_ID = 156804 --FNTRPY = 2 調用單的 轉任務編號不會紀錄在 TBLOG_VIDEO.FNSJOB_ID
	select * from TBBOOKING_MASTER
	select * from TBBOOKING_DETAIL where FSJOB_ID = 156804 --會紀錄在 TBBOOKING_DETAIL
	select * from TBTRANSCODE where FNTRANSCODE_ID = 156825
	select * from TBTRANSCODE where FNTRANSCODE_ID = 156862 --FNPROCESS_STATUS = 7 PARTIAL 處理錯誤 FNPARTIAL_ID = 155625
	select * from TBTRANSCODE_PARTIAL where FNPARTIAL_ID = 156804

	--CREATE VIEW VW_TBTRANSCODE as
	select A.FNTRANSCODE_ID, A.FNANYSTREAM_ID, A.FNPROCESS_STATUS, A.FSNOTE,
	D.FDUPDATED_DATE, D.FSFILE_NO, D.FSBRO_ID, D.FSTYPE, D.FSARC_TYPE, D.FCFILE_STATUS, D.FSJOB_ID, 
	B.FNTSMJOB_ID, B.FNSTATUS, B.FDREGISTER_TIME, B.FSRESULT,
	C.FNJOB_ID, C.FSCONTENT, C.FDTIME 
	from TBTRANSCODE A left join TBTSM_JOB B on A.FNTSMDISPATCH_ID = B.FNTSMJOB_ID
	left join TBTRANSCODE_RECV_LOG C on A.FNANYSTREAM_ID = C.FNJOB_ID
	left join TBLOG_VIDEO D on A.FNTRANSCODE_ID = D.FSJOB_ID
	where CAST(B.FDREGISTER_TIME as DATE) = '2017-03-09'
	order by A.FNTRANSCODE_ID

	select * from VW_TBTRANSCODE where CAST(FDUPDATED_DATE as DATE) = '2017-03-09'  order by FNTRANSCODE_ID --127 有包含重轉
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-09'  order by FNTRANSCODE_ID --123 用 FDSTART_TIME 就會是當時的轉檔任務
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-09' and FNPROCESS_STATUS <> 4 order by FNTRANSCODE_ID --123 用 FDSTART_TIME 就會是當時的轉檔任務
end

----------------------------------20170310-------------------------------------
if @case = 201703101014 begin  --3/9 轉檔成功搬檔失敗, TSM 處理中
    select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-9' order by FNTRANSCODE_ID
	select * from VW_TBTRANSCODE_BY_TSM_REGISTER_TIME where CAST(FDREGISTER_TIME as DATE) = '2017-03-10' order by FNTRANSCODE_ID
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-9' and FSFILE_NO = 'P201703002780001' order by FNTRANSCODE_ID
	select * from TBLOG_VIDEO where FSFILE_NO = 'P201703002780001'

	--156895 ANS處理錯誤
	--156922 轉檔中 G000016301200001, 201703090056
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G000016301200001'
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157109', '108118', '219424', 'G201527200010006', '2017-03-11'

	--156923 轉檔中 G000016301210001, 201703090057
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G000016301210001'
	--156950 轉檔中 G200745600730015, 201703090073
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G200745600730015'
    --156951 轉檔中 G201095300010003, 201703090074
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201095300010003'
	--156955 轉檔中 G200745600700051, 201703090037
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G200745600700051'
	--156961 轉檔中 G201549400010007, 201703090075
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201549400010007'
	--156988 轉檔中 G201584000010008, 201703090102
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201584000010008'
	--156998 轉檔中 G201605100010006, 201703090123
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201605100010006'

	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-9' and FNTRANSCODE_ID in (156922, 156923, 156950, 156951, 156955, 156961, 156988, 156998) 
	order by FNTRANSCODE_ID

	--156994 TSM處理中 G201632400010007, 201604270127, 00002ISV, 大湧來拍岸─台灣子婿．馬偕(2-1)
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201632400010007'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' FSFILE_NO = 'G201632400010007'
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201632400010007'
	exec SP_Q_INFO_BY_VIDEOID '00002ISV'

	--157006 TSM處理中 FNPROCESS_STATUS = 1, FCFILE_STATUS = R --{短帶名稱：2017 03/13 pts1 menu 18:00 ，短帶編號：20170300261 P201703002610001} 00003FRQ
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201703002610001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201703002610001'
	exec SP_Q_INFO_BY_VIDEOID '00003FRQ'

	--157008 TSM處理中 FNPROCESS_STATUS = 1 --P201703002840001, 201703090092, 00003FTV
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201703002840001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201703002840001'
	exec SP_Q_INFO_BY_VIDEOID '00003FTV'

	--157010 TSM處理中 FNPROCESS_STATUS = 1 --G201771700010001, 201703020055, 00003F4I
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201771700010001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201771700010001'
	exec SP_Q_INFO_BY_VIDEOID '00003F4I'



	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-09' and FSFILE_NO = 'G200745600700051' order by FNTRANSCODE_ID
	select * from TBLOG_VIDEO where FSFILE_NO = 'G200745600700051'

	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-03-09' and --Dur : 23:30 Transcode:09:28 ~ 13:36
	(FSMESSAGE like '%156955%' or FSMESSAGE like '%219102%' or FSMESSAGE like '%107971%' or FSMESSAGE like '%G200745600700051%')
	order by FDTIME --開始進行高解搬檔：/mnt/mamnas1/IngestWorkingTemp/G200745600700051.mxf


	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-03-10' and FSCATALOG = 'ERROR' order by FDTIME

	--S 2017-03-10 09:13 開始轉  2017-03-10 17:35 才接收到轉檔完成
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-03-10' and
	(FSMESSAGE like '%157018%' or FSMESSAGE like '%108031%' or FSMESSAGE like '%219244%' or FSMESSAGE like '%G201594800010011%')
	order by FDTIME --2017-03-10 17:35:38.323 開始進行高解搬檔：/mnt/mamnas1/IngestWorkingTemp/G201594800010011.mxf

	select * from TBTRACKINGLOG where FDTIME between '2017-03-10 12:40' and '2017-03-10 13:10' order by FDTIME
end

if @case = 201703101042 begin  --FCSTATUS = ''
	exec SP_Q_INFO_BY_VIDEOID '000026OB' --G201629900010001
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201629900010001'
end


if @case = 201703101201 begin
		select A.FNTRANSCODE_ID, A.FNANYSTREAM_ID, A.FNPROCESS_STATUS, A.FSNOTE, A.FDSTART_TIME,
		D.FDUPDATED_DATE, D.FSFILE_NO, D.FSBRO_ID, D.FSTYPE, D.FSARC_TYPE, D.FCFILE_STATUS, D.FSJOB_ID, 
		B.FNTSMJOB_ID, B.FNSTATUS, B.FDREGISTER_TIME, B.FSRESULT,
		C.FNJOB_ID, C.FSCONTENT, C.FDTIME 
		from TBTRANSCODE A left join TBTSM_JOB B on A.FNTSMDISPATCH_ID = B.FNTSMJOB_ID
		left join TBTRANSCODE_RECV_LOG C on A.FNANYSTREAM_ID = C.FNJOB_ID
		left join TBLOG_VIDEO D on A.FNTRANSCODE_ID = D.FSJOB_ID
		where CAST(A.FDSTART_TIME as DATE) = '2017-03-09' --如果用 B.FDREGISTER  時間會查不到
		order by FDSTART_TIME --no 156994 TSM 處理中

		select * from TBTRANSCODE where FNTRANSCODE_ID = 156994
		select * from TBTSM_JOB where FNTSMJOB_ID = -1

		select * from TBTRANSCODE A left join TBTSM_JOB B on A.FNTSMDISPATCH_ID = B.FNTSMJOB_ID where CAST(A.FDSTART_TIME as DATE) = '2017-03-09' order by FNTRANSCODE_ID --found 156994
		select * from TBTRANSCODE A left join TBTRANSCODE_RECV_LOG B on A.FNANYSTREAM_ID = B.FNJOB_ID where CAST(A.FDSTART_TIME as DATE) = '2017-03-09' order by FNTRANSCODE_ID --found 156994
end

if @case = 201703101425 begin --ANS 處理錯誤低解轉檔失敗, non-drop frame
	exec SP_Q_INFO_BY_VIDEOID '00003FTM' --P201703002820001, 201703090083 最後的詩句60s PROMO, R, 01:00:00;00 ~ 01:00:59;29
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-10' and FSFILE_NO = 'P201703002820001' order by FNTRANSCODE_ID --FNPROCESS_STATUS = 8 ANS 處理錯誤, FCFILE_STATUS = R, FNSTATUS = 3
	--重啟任務 失敗 TSM 處理中
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201703002820001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' FSFILE_NO = 'P201703002820001'
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'P201703002820001'

	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-10' order by FNTRANSCODE_ID
end

if @case = 201703101716 begin --刪除HD 轉檔單
	select * from TBPROG_M where FSPGMNAME = '已讀不回' --2015400
	select * from TBLOG_VIDEO where FSID = '2015400' --G201540000010008, 201506110118
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201540000010008'
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G201540000010008'
	update TBLOG_VIDEO set FCFILE_STATUS = 'D' where FSFILE_NO = 'G201540000010008'
end

if @case = 201703110800 begin --處理 3/10 轉檔成功搬檔失敗問題
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-10' and FSARC_TYPE = '007' order by FNTRANSCODE_ID
	select * from TBTRANSCODE where FNTRANSCODE_ID = 157095
	select * from TBTRANSCODE_RECV_LOG where FNJOB_ID = 108106 --有 2 筆表示有重轉，第一次失敗, 是由 Anystream 重啟

	---------搬檔失敗------
	--157018 108031,G201594800010011,201703100003,219244
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201594800010011'
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-03-10' and
	(FSMESSAGE like '%157018%' or FSMESSAGE like '%108031%' or FSMESSAGE like '%219244%' or FSMESSAGE like '%G201594800010011%')
	order by FDTIME
	 -- Got error when execute DoIngestQueryProcess: System.ServiceModel.FaultException: Unable to communicate with the ECS on [ECS-JM:3501]
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157018', '108031', '219244', 'G201594800010011', '', '2017-03-10'

	--157021,108034,G201359900010005,201703100006,219247
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201359900010005'
	-- Got error when execute DoIngestQueryProcess: System.ServiceModel.FaultException: Unable to communicate with the ECS on [ECS-JM:3501]
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157021', '108034', '219247', 'G201359900010005', '', '2017-03-10'


	--157046,108059,G201496200010011,201703100034,219282
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201496200010011'
	--Got error when execute DoIngestQueryProcess: System.ServiceModel.FaultException: Unable to communicate with the ECS on [ECS-JM:3501]
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157046', '108059', '219282', 'G201496200010011', '', '2017-03-10'

	--157047,108060,G201496300010003,201703100037,219283
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201496300010003'
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157047', '108060', '219283', 'G201496300010003', '', '2017-03-10'

	--157071,108083,G200745600740010,201703100029,219308
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G200745600740010'
	--Got error when execute DoIngestQueryProcess: System.ServiceModel.FaultException: Unable to communicate with the ECS on [ECS-JM:3501]
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157071', '108083', '219308', 'G200745600740010', '', '2017-03-10'


	--轉檔成功
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157016', '108029', '219242', 'G201142100100023', '', '2017-03-10'
	--157017,108030,G201142100110010,219243
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157017', '108030', '219243', 'G201142100110010', '', '2017-03-10'
	--157019, 108032,G201631000010006, 219245
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157019', '108032', '219245', 'G201631000010006', '', '2017-03-10'
    --157095, 108106, G201527100010011, 219333
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157095', '108106', '219333', 'G201527100010011', '', '2017-03-10'

	--轉檔失敗 157020,108033,G201359800010012,201703100005,219246  todo  高解搬檔遇未預期錯誤：/mnt/mamnas1/IngestWorkingTemp/G201359800010012.mxf/控制代碼無效。
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-03-10' and
	(FSMESSAGE like '%157020%' or FSMESSAGE like '%108033%' or FSMESSAGE like '%219246%' or FSMESSAGE like '%G201359800010012%')
	order by FDTIME

	exec SP_Q_TBTRACKINGLOG_BY_DATE '157020', '108033', '219246', 'G201359800010012', '', '2017-03-11'
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157020', '108033', '219246', 'G201359800010012', '00002VBJ', '2017-03-11 12:40', '2017-03-11 13:10'

	select '2017-03-09 12:20'
	
end

-----------------------------------20170312-------------------------------------------------------
if @case = 201703120743 begin --3/11 搬檔失敗 rodo
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-11' and FSARC_TYPE = '007' order by FNTRANSCODE_ID --fail rate : 4/12 no 157129, 157128
	--失敗
	--157109, 108118, G201527200010006, 219424, 201703110001
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157109', '108118', '219424', 'G201527200010006', '', '2017-03-11'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201527200010006'
	--157114, 108123, G200745600770011, 201703110005, 219429
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157114', '108123', '219429', 'G200745600770011', '', '2017-03-11'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G200745600770011'
	--157118,108127,G200745600780013,201703110008,219435
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157118', '108127', '219435', 'G200745600780013', '', '2017-03-11'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G200745600780013'
	--157120,108129,G200745600800011,201703110010,219437
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157120', '108129', '219437', 'G200745600800011', '', '2017-03-11'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G200745600800011'

	

	--成功
	--157110,108119,G201579400010009,201703110002,219425 也是有 error
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157110', '108119', '219425', 'G201579400010009', '', '2017-03-11'
	--157111,108120,G201617700010008,201703110003,219426
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157111', '108120', '219426', 'G201617700010008', '', '2017-03-11'
	--157112,108121,G200745600750009,201703100066,219427
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157112', '108121', '219427', 'G200745600750009', '', '2017-03-11'
	--157113,108122,G200745600760009,201703110004,219428
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157113', '108122', '219428', 'G200745600760009', '', '2017-03-11'
	--157119,108128,G200745600790014,201703110009,219436
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157119', '108128', '219436', 'G200745600790014', '', '2017-03-11'
	--157129,-1,G201617600010008,201703110012,219454
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157129', '-1', '219454', 'G201617600010008', '', '2017-03-11'

	--bug
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201617600010008'
	select * from TBTRANSCODE where FNTRANSCODE_ID = 157129 -- FNPROCESS = 8 註冊 anystream 發生錯誤
end

-------------------------------------------20170313-----------------------------------------------------
if @case = 201703131035 begin
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-12' and FSARC_TYPE = '007' order by FNTRANSCODE_ID --2/6
	--157140,108147,G201632400010013,201703120001,219533
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157140', '108147', '219533', 'G201632400010013', '', '2017-03-12'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201632400010013'
	--157142,108149,G200745600810011,201703120003,219535
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157142', '108149', '219535', 'G200745600810011', '', '2017-03-12'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G200745600810011'

	--processing -> done...........

	--success
	--157139,108146,G201617600010008,201703110012,219532 --no error
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157139', '108146', '219532', 'G201617600010008', '', '2017-03-12'
	--157141,108148,G201632500010005,201703120002,219534  --got ECS-JM connection erro, but still work
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157141', '108148', '219532', 'G201632500010005', '', '2017-03-12'
	--157143,108150,G200745600820011,201703120004,219536 --no error
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157143', '108150', '219536', 'G200745600820011', '', '2017-03-12'
	--157144,108151,G200745600830015,201703120005,219537 --no error
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157144', '108151', '219537', 'G200745600830015', '', '2017-03-12'

	exec SP_Q_TBTRANSCODE_INFO_FNTRANSCODE_ID '157139'
end

if @case = 201703131157 begin --檔案未到  todo 無法磁帶取出
	--TapeOnly
	exec SP_Q_INFO_BY_VIDEOID '00001Z28' --2014變臉台灣, 2017-03-15, 24:00:00;00, 13, G201529100050001, \\mamstream\MAMDFS\ptstv\HVideo\2015\2015291\G201529100050001.mxf
	select * from TBZCHANNEL
end

if @case = 201703131204 begin --3/10 有一支播出帶轉檔完成搬檔失敗
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-10' and FSARC_TYPE = '002' order by FNTRANSCODE_ID
	--157085,108097,G201589100060001,00001WDE,201505250100,219322
end

if @case = 201703131551 begin --檔案錯誤直接更新 HDUpload 檔案
	exec SP_Q_INFO_BY_VIDEOID '00003ED3' --G201711100090001, B
end

if @case = 201703131702 begin --其他檔案入庫卡住
	select * from TBPROG_M where FSPGMNAME like '%客家新聞雜誌%' --0000896:客家新聞雜誌, 2006262:客家新聞雜誌(客家製播)-242集排播需注意, 2006911:客家新聞雜誌(公視衍生2007-2012)
	select * from TBLOG_VIDEO where FSID = '0000896'--no record
	select * from TBLOG_VIDEO where FSID = '2006262' and FNEPISODE = 528 -- G200626205280003, 201703020093 \\10.13.200.5\OtherVideo\客家新雜誌無字慕分軌帶\客家新聞雜誌第528集 無字慕分軌帶.MXF
	select * from TBLOG_VIDEO where FSID = '2006911' --no record
	select * from TBLOG_VIDEO where FSID = '2004788' --no record
	select * from TBLOG_VIDEO where FSID = '2005672' --no record

	select * from TBLOG_VIDEO where FSID = '2006262' and FSARC_TYPE = '026' order by FDCREATED_DATE

	select * from TBLOG_VIDEO where FSARC_TYPE = '026' and FDCREATED_DATE > '2017-03-01' order by FDCREATED_DATE
	select FSFILE_NO, FSBRO_ID, FSTITLE, FCFILE_STATUS, FDCREATED_DATE, FDUPDATED_DATE from TBLOG_VIDEO where FSARC_TYPE = '026' and FDUPDATED_DATE > '2017-03-13 17:00' order by FDUPDATED_DATE
	--搬檔完後不會自動清除
end

if @case = 201703131800 begin --3/13 轉檔失敗
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-13' and FSARC_TYPE = '007' order by FNTRANSCODE_ID
end

if @case = 201703131815 begin --短帶名稱有 single quote 造成新增 Timecode hang 住
	select * from TBLOG_VIDEO where FSBRO_ID = '201703130220'
	select * from TBBROADCAST where FSBRO_ID = '201703130220'
	select * from TBPGM_PROMO where FSPROMO_NAME like '%0319%' --20170300409, 0319-帶作品回家90'-1-PPOMO
end

-------------------------------20170314-------------------------------
if @case = 201703141131 begin
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-13' and FSARC_TYPE = '007' order by FNTRANSCODE_ID --3/14
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) between '2017-03-11' and '2017-03-13' and FSARC_TYPE = '007' order by FNTRANSCODE_ID --3/14
	--157173,108180,G201370400080010,201703130002,219649 --no connection ECS-JM error 人間相對論8
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157173', '108180', '219649', 'G201370400080010', '', '2017-03-13'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201370400080010'
	--157175,108182,G201370400100010,201703130004,219651 --no connection error 人間相對論10
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157175', '108182', '219651', 'G201370400100010', '', '2017-03-13'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201370400100010'
	--157176,108183,G201370400110010,201703130005,219652 --no connection error 人間相對論11
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157176', '108183', '219652', 'G201370400110010', '', '2017-03-13'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201370400110010'

	--processing ......fail....done
end

if @case = 201703141417 begin
	exec SP_Q_INFO_BY_VIDEOID '00003EDP'
	exec SP_Q_INFO_BY_VIDEOID '000032IS'
	exec SP_Q_INFO_BY_VIDEOID '00002QRW'
	exec SP_Q_INFO_BY_VIDEOID '00003G8Q'
end

if @case = 201703141552 begin  --播出帶 冰血暴 6, 8, 9 搬檔失敗

    --播出帶
    select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) between '2017-03-10' and '2017-03-14' and FSARC_TYPE = '002' order by FNTRANSCODE_ID
    --轉檔完成搬檔失敗
    --157196,108201,G201589100080002,00001WSK,201506010049,219684, 70807
    exec SP_Q_TBTRACKINGLOG_BY_DATE '157196', '108201', '219684', 'G201589100080002', '001WSK', '2017-03-13'
    select * from TBUSERS where FSUSER_ID = '70807' --許偉桓
    update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201589100080002'
    --157200,108205,G201589100090002,00001WSN,201506010052,219688
    exec SP_Q_TBTRACKINGLOG_BY_DATE '157200', '108205', '219688', 'G201589100090002', '001WSN', '2017-03-13'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201589100090002'
	--processing ...fail 傍晚執行... 23:00 再執行 done

	select * from TBTRANSCODE where FNTRANSCODE_ID = 157200 --FSANYSTREAM_XML, FSNOTIFY_ADDRESS= http://mam.pts.org.tw/DataSource/Handler_ReceiveTransCode.ashx
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201589100090002'

	exec SP_Q_TBTRANSCODE_INFO_FNANYSTREAM_ID '108201'


end

if @case = 201703142328 begin --完成帶失敗
    select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-14' and FSARC_TYPE = '007' order by FNTRANSCODE_ID --fail rate 6/17
	--157244,108235,G201370400120010,201703140001,219826
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157244', '108235', '219826', 'G201370400120010', '', '2017-03-14'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201370400120010'
	--157245,108236,G201370400130010,201703140002,219827
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157245', '108236', '219827', 'G201370400130010', '', '2017-03-14'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201370400130010'
	--157248,108239,G200745600920010,201703140005,219830
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157248', '108239', '219830', 'G200745600920010', '', '2017-03-14'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G200745600920010'
	--157249,108240,G200745600930021,201703140006,219831
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157249', '108240', '219831', 'G200745600930021', '', '2017-03-14'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G200745600930021'
	--157272,108260,G201623500010004,201703140047,219857
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157272', '108260', '219857', 'G201623500010004', '', '2017-03-14'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201623500010004'
	--157291,108265,G201370400150010,201703140081,219896  --got connection error
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157291', '108265', '219896', 'G201370400150010', '', '2017-03-14'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201370400150010'

	select * from TBLOG_VIDEO where FSFILE_NO = 'G201623500010004'
	select * from TBPROG_M where FSPROG_ID = '2016235'
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-14' and FCFILE_STATUS = 'S' order by FNTRANSCODE_ID
end

if @case = 201703151108 begin --送播單未建完就跳出
	select * from TBPROG_M where FSPROG_ID = '2006851' --台灣一周焦點
	select * from TBLOG_VIDEO where FSID = '2006851' and FNEPISODE = 516 --G200685105160001, 201703140115
	select * from TBBROADCAST where FSBRO_ID = '201703140115' --no record
	--exec SP_D_TBLOG_VIDEO_BYBROID '201703140115' --using sa
    --exec SP_D_TBLOG_VIDEO_SEG_BYFILEID 'G200685105160001'
end

if @case = 201703051406 begin -- 婚禮 搬檔失敗
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-9' and FCFILE_STATUS = 'S' order by FNTRANSCODE_ID
	--156988,108004,G201584000010008,201703090102,219141
	exec SP_Q_TBTRACKINGLOG_BY_DATE '156988', '108004', '219141', 'G201584000010008', '', '2017-03-09'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201584000010008'
	--156998,108014,G201605100010006,201703090123,219155g
	exec SP_Q_TBTRACKINGLOG_BY_DATE '156998', '108014', '219155', 'G201605100010006', '', '2017-03-09'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201605100010006'
end

----------------------------- 20170316------------------------------
if @case = 201703160956 begin --人間相對論
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-15' and FSARC_TYPE = '007' order by FNTRANSCODE_ID --1/12
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-15' and FCFILE_STATUS = 'S'  order by FNTRANSCODE_ID
	--157366,108340,G201370400180011,201703150022,220065
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157366', '108340', '220065', 'G201370400180011', '', '2017-03-16'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201370400180011'  --processing ...
end

if @case = 201703161711 begin -- 3/15 審核完轉檔入庫後，2017-03-25 才要播出，但是檔案仍在 Review
	exec SP_Q_INFO_BY_VIDEOID '00003G43'
end

if @case = 201703161713 begin --*.mxf.mxf 改完副檔名後，上傳失敗，但是檔案最後有到待審區
	exec SP_Q_INFO_BY_VIDEOID '00003GJS'
end

-------------------------------------20170317---------------------------------
if @case = 201703171152 begin --冰血暴 6 搬檔失敗 --todo

	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) >= '2017-03-8' and FCFILE_STATUS in ('S', 'R') order by FNTRANSCODE_ID


	--轉檔完成搬檔失敗
	--157085,108097,G201589100060001,201505250100,219322
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157085', '108097', '219322', 'G201589100060001', '001WSN', '2017-03-10'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201589100060001'  --processing ..done


	--157366,108340,G201370400180011,00003GEL,201703150022,220065
	exec SP_Q_INFO_BY_VIDEOID '00003GEL' --人間相對論 18
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201370400180011' --processing.. done


	--TSM 處理中 --processing..
	--157051,-1,G201754400070001,00003FTP,201703090085,219287
	exec SP_Q_INFO_BY_VIDEOID '00003FTP' --猜猜我有多愛你  第二季(海陸腔) 7
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201754400070001' --入庫失敗重審
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201754400070001'

	--157087,-1,G201653700250001,00003D9I,201702090126,219324
	exec SP_Q_INFO_BY_VIDEOID '00003D9I' --任務客 25 2017-03-24, 07
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201653700250001' --入庫失敗重審
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201653700250001'

	--157480,-1,G201773100010001,00003GH0,201703150121,220283
	exec SP_Q_INFO_BY_VIDEOID '00003GH0' --2017-03-19, 12, 帶作品（回家）(90分鐘版)
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201773100010001'
	update TBTRANSCODE set FNPROCESS_STATUS = 8 where FNTRANSCODE_ID = 157480  -- FNTSMJB_ID = -1?  bug todo
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201773100010001' --入庫失敗重審
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201773100010001'

	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) >= '2017-03-17' order by FNTRANSCODE_ID

end

if @case = 201703171846 begin
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-17' order by FNTRANSCODE_ID

	--157506,108476,G200928504710023,00003GIX,201703170003,220382
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157506', '108476', '220382', 'G200928504710023', '00003GIX', '2017-03-17' --002
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G200928504710023'

	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-17' and FSARC_TYPE = '007' order by FNTRANSCODE_ID --FAIL RATE 0


	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-18'  order by FNTRANSCODE_ID --FAIL RATE 2/45
	--8,R, ANS處裡錯誤
	--157596,108563,G000081613060003,00003GP2,201703180003,220592, 下課花路米 1306
	exec SP_Q_INFO_BY_VIDEOID '00003GP2'

	--157602,108566,G201531100010003,00003GPC,201703180012,220614, 天天想你 2-2
	exec SP_Q_INFO_BY_VIDEOID '00003GPC'


	--157597,108569,G201496400010008,00003GP7,201703180007,220609 --done
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157597', '108569', '220609', 'G201496400010008', '00003GP7', '2017-03-18' --007
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201496400010008'

	--157601,108565,G201531000010010,00003GPB,201703180011,220613 --done
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157601', '108565', '220613', 'G201531000010010', '00003GPB', '2017-03-18' --007
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201531000010010'

end

-----------------------------20170319--------------------------------------
if @case = 201703190946-201702081014 begin
	exec SP_Q_GET_MOVE_PENDING_LIST_MIX --FSID, FNEPISODE, FSFILE_NO, FCFILE_STATUS, FCCHECK_STATUS, FCSTATUS, FSBRO_ID
	exec SP_Q_GET_CHECK_COMPLETE_LIST_MIX_MACRO --230
	exec SP_Q_GET_CHECK_COMPLETE_LIST_MIX --801
end

--------------------------201703200840---------------------------
if @case = 201703200840 begin
	select * from VW_GET_CHECK_COMPLETE_LIST_MIX_MACRO
	select * from VW_GET_IN_TAPE_LIBRARY_LIST_MIX_MACRO
	exec SP_Q_INFO_BY_VIDEOID '00001LSY'
	exec SP_Q_INFO_BY_VIDEOID '00001VBP'
	exec SP_Q_INFO_BY_VIDEOID '00003F5F'
	exec SP_Q_INFO_BY_VIDEOID '00003CA2'
	exec SP_Q_INFO_BY_VIDEOID '00002HRF'
	exec SP_Q_INFO_BY_VIDEOID '00001NPP'
	exec SP_Q_INFO_BY_VIDEOID '00001NPP'
	exec SP_Q_INFO_BY_VIDEOID '00001Y9G'

	select * from VW_GET_PLAY_VIDEOID_LIST_MACRO
end

if @case = 201703201742 begin  --QC 轉檔 TSM 註冊失敗 JOB_ID = -1  --TODO
	select * from TBTRACKINGLOG where FDTIME between '2017-03-20 14:30' and '2017-03-20 15:30' order by FDTIME
end

----------------------------20170321----------------------------------------------------------
if @case = 201703210945 begin
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-20' order by FNTRANSCODE_ID --97
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-20' and FSARC_TYPE = '002' and FCFILE_STATUS <> 'T' order by FNTRANSCODE_ID --8/72
	--卡在轉檔中
	--157677,108643,G201773700020002,00003GD2,201703140127,220838, done
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157677', '108643', '220838', 'G201773700020002', '003GD2', '2017-03-20'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201773700020002' --入庫失敗重審
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201773700020002'
	exec SP_Q_INFO_BY_VIDEOID '00003GD2' --2017歐洲花式滑冰錦標賽 3/25, 14
	select * from TBTRANSCODE where FNTRANSCODE_ID = 157848

	--157701,108663,G201758100500001,00003GO5,201703170122,220872 ,轉檔完成，搬檔失敗, done
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157701', '108663', '220872', 'G201758100500001', '003GO5', '2017-03-20'
	exec SP_Q_INFO_BY_VIDEOID '00003GO5' --2017福氣來了, 07, 3/24, R
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201758100500001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201758100500001'
	exec SP_Q_INFO_BY_VIDEOID '00003GXH'  --審核到 49 集

	--157741,108694,G201652700510001,00003G1D,201703130045,220918, done
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157741', '108694', '220918', 'G201652700510001', '003G1D', '2017-03-20'
	exec SP_Q_INFO_BY_VIDEOID '00003G1D' --鬧熱打擂台(120分鐘版) 51, 4/1, 07
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201652700510001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201652700510001'
	--搬檔失敗
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157853', '108799', '221113', 'G201652700510001', '003G1D', '2017-03-21'

	--157753,108707,G000008253990003,00003GGC,201703150084,220930
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157753', '108707', '220930', 'G000008253990003', '003GGC', '2017-03-20'
	exec SP_Q_INFO_BY_VIDEOID '00003GGC' --看公視說英語, 5399, 3/30, 14
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G000008253990003'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G000008253990003'

	--卡在 TSM 處理中 TSMJOB_ID = -1
	--157702,G201637400290001,00003GNH,201703170090, done
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157702', '108707', '220930', 'G201637400290001', '003GGC', '2017-03-20'
	exec SP_Q_INFO_BY_VIDEOID '00003GNH' --多奇探險隊 第2季 29, 5399, 4/1, 7, 3/27 重轉
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201637400290001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201637400290001'

	--157703,G201766600120001,00003FTL,201703090081, done
	exec SP_Q_INFO_BY_VIDEOID '00003FTL' --姜老師，妳談過戀愛嗎？ 12, 5399, 3/28, 12, 3/27 重轉
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201766600120001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201766600120001'

	--157706,P201703006180001,00003GTF,201703200078
	exec SP_Q_INFO_BY_VIDEOID '00003GTF' --未排播 3/27 重審 done
	select * from TBPGM_PROMO where FSPROMO_ID = '20170300618' --三芝櫻花_20秒
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201703006180001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201703006180001'

	--157708,P201703003750001,00003G32,201703130111 3/27 重審 done
	exec SP_Q_INFO_BY_VIDEOID '00003G32' --訂閱宏觀Youtube
	select * from TBPGM_PROMO where FSPROMO_ID = '20170300375'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201703003750001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201703003750001'



	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-20' and FSARC_TYPE = '007' order by FNTRANSCODE_ID --6/15  done
	--157667,108633,G201370400220011,00003GLQ,201703200001,220828 人間相對論 22 王俠軍 donn
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201370400220011'
	--157668,108634,G201669300030009,00003GQS,201703200002,220829 大腦先生 3
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201669300030009'
	--157679,108645,G201142100170013,00003GQX,201703200007,220840 成語賽恩思 17
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201142100170013'
	--157687,108649,G201669300080008,00003GTH,201703200081,220851 大腦先生 8
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201669300080008'
	--157688,108650,G201669300090008,00003GTI,201703200082,220852 大腦先生 9
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201669300090008'
	--157697,108659,G201669300110008,00003GTZ,201703200089,220866 大腦先生 11
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201669300110008'
end

if @case = 201703211032 begin
	exec SP_Q_INFO_BY_VIDEOID '000035AO' -- O, -T, 2017-03-23, 08, 緝兇(白天不宜) TSM sysError
	exec SP_Q_INFO_BY_VIDEOID '00001LSY'
end

if @case = 201703211457 begin --短帶送播資料無法新增，按確認後 Loading 很久
	select * from TBLOG_VIDEO where CAST(FDCREATED_DATE as DATE) = '2017-03-21' and FSCREATED_BY = '51204' --FSID = 20170300669, 201703210069
	exec SP_Q_INFO_BY_VIDEOID '00003GY2'
	select * from TBPGM_PROMO where FSPROMO_ID = '20170300669' --TEST20170321-abc
	select * from TBPGM_PROMO where CAST(FDCREATED_DATE as DATE) = '2017-03-21' and FSCREATED_BY = '51204' --20170300668, TEST20170317 (送播單和託播單沒建成功） : 20170300669, TEST20170321-abc
	select * from TBPGM_PROMO where CAST(FDCREATED_DATE as DATE) = '2017-03-21' and FSCREATED_BY = '71015' --PTS1_行走TIT-54(重) 有建成功, PTS3_行走TIT-54(重) 重建成功
	select * from TBLOG_VIDEO where CAST(FDCREATED_DATE as DATE) = '2017-03-21' and FSCREATED_BY = '71015' -- 1 筆
end

if @case = 201703211719 begin
	exec SP_Q_INFO_BY_VIDEOID '00002U8R' --風華大歌廳 08 卡在 TSM RECALL
	exec SP_Q_INFO_BY_VIDEOID '00003FXI'
end

----------------------------------201703221412-------------------------------------
if @case = 201703221413 begin
	select * from TBPGM_COMBINE_QUEUE where CAST(FDDATE as DATE) = '2017-03-22' and FSCHANNEL_ID = '08' order by FNCONBINE_NO
end

if @case = 201703221724 begin --QUE 表
	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20170300109'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20170300108'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	
	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20170200582'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

		
	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20170300279'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

			
	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20170300278'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

end

if @case = 201703231200 begin
	exec SP_Q_INFO_BY_VIDEOID '000035QC'
	exec SP_Q_INFO_BY_VIDEOID '000035OC'
	exec SP_Q_INFO_BY_VIDEOID '00002XPO'

	exec SP_Q_INFO_BY_VIDEOID '00003GVN'
	exec SP_Q_INFO_BY_VIDEOID '00002GI8' --3/30 夢遊動物園 62
	exec SP_Q_INFO_BY_VIDEOID '00002F7X'

	exec SP_Q_INFO_BY_VIDEOID '000035AO'
	exec SP_Q_INFO_BY_VIDEOID '00002U8R'


end

if @case = 201703231532 begin --4/107 轉檔中
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-22'  order by FNTRANSCODE_ID --107
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-22' and FCFILE_STATUS <> 'T' order by FNTRANSCODE_ID --S:4, D:3 短帶
	--157873,108816,G200805300040001,00003H08,201703220001,221205
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157873', '108816', '221205', 'G200805300040001', '003H08', '2017-03-22'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G200805300040001'
	--157874,108814,G200805300010001,00003H07,201703220003,221207
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157874', '108814', '221207', 'G200805300010001', '003H07', '2017-03-22'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G200805300010001'
	--157907,108837,G200805300090001,00003H3V,201703220046,221404
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157907', '108837', '221404', 'G200805300090001', '003H3V', '2017-03-22'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G200805300090001'
	--157909,108839,G200805300100001,00003H3W,201703220047,221406
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157909', '108839', '221406', 'G200805300100001', '003H3W', '2017-03-22'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G200805300100001'

	--processing...done
end



if @case = 201703231557-201609231151 begin --匯入節目表發生錯誤，原因為寫入節目表有問題

	select * from TBPGM_QUEUE where CAST(FDDATE as DATE) = '2017-03-26' and FSCHANNEL_ID = '14' order by FSBEG_TIME

	select * from TBTRACKINGLOG where FDTIME > '2017-03-23 16:28' order by FDTIME
	select * from TBTRAN_LOG where FDDATE > '2017/3/23 16:20' order by FDDATE

	--2016台北國際合唱音樂節閉幕音樂會 FSBTIME = '' 未標時間
	select B.FSPGMNAME, A.* from OPENQUERY(PTS_PROG_MYSQL, 'select PAENO FSPROGID,PBENO FNEPISODE,PSDAT FDDATE,PSTME FSBTIME,PSMIN FSDURATION,PSKND FNREPLAY,PSKSE FSKSE,PSKLV FSLIVE,PSDEP FSDEP,PSDIR 
	from programX.PSHOW_HD where PSDAT = ''2017-03-26''') A left join TBPROG_M B on A.FSPROGID = B.FSPROG_ID order by FSBTIME

	select B.FSPGMNAME, A.* from OPENQUERY(PTS_PROG_MYSQL, 'select *
		from programX.PSHOW_HD where PSDAT = ''2017-03-26''') A left join TBPROG_M B on A.FSPROGID = B.FSPROG_ID

	--select PAENO FSPROGID,PBENO FNEPISODE,PSDAT FDDATE,PSTME FSBTIME,PSMIN FSDURATION,PSKND FNREPLAY,PSKSE FSKSE,PSKLV FSLIVE,PSDEP FSDEP,PSDIR 
	--	from PSHOW_HD where PSDAT = '2017-03-26' order by PSTIME

	--sqltxt1
	Select FSOLDTABLENAME from TBZCHANNEL where FSCHANNEL_ID = '14' --PSHOW_HD

        --PTS MOD
        select A.PAENO FSPROGID,A.PSDAT FDDATE,A.PSTME FSBTIME,B.FSWEBNAME,A.PBENO FNEPISODE,B.FNTOTEPISODE
                from OPENQUERY(PTS_PROG_MYSQL, 'SELECT  * FROM programX.PSHOW_PTS_MOD where PSDAT>=sysdate()
                and PSDAT<= sysdate()+ interval 30 day') A,TBPROG_M B
                where A.PAENO=B.FSPROG_ID order by A.PSDAT,A.PSTME
        --PTS
        select A.PAENO FSPROGID,A.PSDAT FDDATE,A.PSTME FSBTIME,B.FSWEBNAME,A.PBENO FNEPISODE,B.FNTOTEPISODE
                from OPENQUERY(PTS_PROG_MYSQL, 'SELECT  * FROM programX.PSHOW_PTS where PSDAT>=sysdate()
                and PSDAT<= sysdate()+ interval 14 day') A,TBPROG_M B
                where A.PAENO=B.FSPROG_ID order by A.PSDAT,A.PSTME

        --PTS DIMO
        select A.PAENO FSPROGID,A.PSDAT FDDATE,A.PSTME FSBTIME,B.FSWEBNAME,A.PBENO FNEPISODE,B.FNTOTEPISODE
                from OPENQUERY(PTS_PROG_MYSQL, 'SELECT  * FROM programX.PSHOW_DIMO where PSDAT>=sysdate()
                and PSDAT<= sysdate()+ interval 14 day') A,TBPROG_M B
                where A.PAENO=B.FSPROG_ID order by A.PSDAT,A.PSTME

        select top 100 A.* from OPENQUERY(PTS_PROG_MYSQL, 'SELECT  * FROM programX.PSHOW_DIMO order by PSDAT desc') A
        select * from  OPENQUERY(PTS_PROG_MYSQL, 'SELECT  sysdate()') A -- 2016-09-23 12:12:33

        select * from OPENQUERY(PTS_PROG_MYSQL, 'select * from programX.CHANNEL')
        select * from TBZCHANNEL

        --查詢排檔名稱
        select distinct CHEID from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA') -- DIMO, PTS, PTS_MOD, HD_MOD, HAKA, HD ...
        select top 1000 * from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA')
        select top 1000 * from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA WHERE CHEID = "DIMO"')
        select top 1000 * from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA WHERE CHEID = "DIMO" and PRSDT <= "2016-09-26" and PREDT >= "2016-09-26" and PRTMS <= "06:00" and PRTME >= "06:30" and PRCB0 = 1 order by PRENO DESC')
        select * from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA WHERE CHEID = "PTS" and PRNAM like "%人生劇展%" and PRSDT')

        --Export DIMO EPG
        select A.PAENO FSPROGID,A.PSDAT FDDATE,A.PSTME FSBTIME,B.FSWEBNAME,A.PBENO FNEPISODE,B.FNTOTEPISODE
                from OPENQUERY(PTS_PROG_MYSQL, 'SELECT  * FROM programX.PSHOW_DIMO where PSDAT>=sysdate()
                and PSDAT<= sysdate()+ interval 14 day') A,TBPROG_M B
                where A.PAENO=B.FSPROG_ID order by A.PSDAT,A.PSTME


        --201611211107
        select * from TBPGM_PROG_EPG

        --201701091130
        select * from TBPGM_QUEUE

        select * from TBPGM_PROG_EPG
end

if @case = 201703240100 begin  --QC 轉檔失敗
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-23'  order by FNTRANSCODE_ID --151
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-23' and FCFILE_STATUS <> 'T' order by FNTRANSCODE_ID --S:2, R:5
	--158058,108971,G201652200650002,00003F5S,201703020110,221845
	exec SP_Q_TBTRACKINGLOG_BY_DATE '158058', '108971', '221845', 'G201652200650002', '003F5S', '2017-03-23' --QC 轉檔
	exec SP_Q_INFO_BY_VIDEOID '00003F5S' --贏在地球村, 65, 3/28, 13 --file still in Review, done
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201652200650002'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201652200650002'
	
	--158062,108975,G201773600020003,00003H4V,201703220080,221856  --3/27 重審第 3 次,  done
	exec SP_Q_TBTRACKINGLOG_BY_DATE '158062', '108975', '221856', 'G201773600020003', '003H4V', '2017-03-23' --QC 轉檔
	exec SP_Q_INFO_BY_VIDEOID '00003H4V' --2016-17 歐霸足球聯賽, 2, 3/28, 14  file in Review
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201773600020003'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201773600020003'
end

if @case = 201703241209 begin
	exec SP_Q_INFO_BY_VIDEOID '00001WSF' --冰血暴 7 2017-03-29, 14, O, T
	exec SP_Q_INFO_BY_VIDEOID '00001WSK' --冰血暴 8 2017-03-30, 14, T
end

if @case = 201703241336 begin  --鄭文欣 調用失敗 註冊Anystream 服務時發生錯誤
	select * from TBBOOKING_MASTER where FSBOOKING_NO = '201703220018' --70718
	select * from TBBOOKING_DETAIL where FSBOOKING_NO = '201703220018' --FCFILE_TRANSCODE_STATUS = R G201069700010001, FSJOB_ID 157988 TSM 處理中
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201069700010001' --\\mamstream\MAMDFS\ptstv\HVideo\2010\2010697\G201069700010001.mxf --tape offline /ptstv/HVideo/2010/2010697/G201069700010001.mxf
	--dsmrecall /ptstv/HVideo/2010/2010697/G201069700010001.mxf
	select * from TBPROG_M where FSPROG_ID = '2010697' --為了明天的歌唱
	select * from TBUSERS where FSUSER_ID = '70718' --鄭文欣 hsincheng
	--dsmrecall /ptstv/HVideo/2016/2016802/G201680200080002.mxf

	select * from TBBOOKING_MASTER where FSBOOKING_NO = '201703220103' --70718
	select * from TBBOOKING_DETAIL where FSBOOKING_NO = '201703220103' --FCFILE_TRANSCODE_STATUS = F G201680200040002, G201680200270001, G201716700010002
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201069700010001' --\\mamstream\MAMDFS\ptstv\HVideo\2010\2010697\G201069700010001.mxf
end

if @case = 201703241418 begin --置換檔案
	exec SP_Q_INFO_BY_VIDEOID '00003GO0' --下課花路米－小學生出任務 27 檔案有問題 後製的檔案直接覆蓋 HDUpload Review
	select * from TBZCHANNEL
end

if @case = 201703241429 begin --標記上傳失敗
	exec SP_Q_INFO_BY_VIDEOID '00003FLT' --TBLOG_VIDEO_HD_MC 紀錄 檔案已在 HDUpload 且以標記上傳, 201703070154, FCSTATUS = 80
	select * from TBLOG_VIDEO_HD_MC where FSFILE_NO = 'P201703002170001'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00003FLT'
	select * from TBLOG_VIDEO_HD_MC where FSBRO_ID = '201703070154'
	exec SP_I_TBLOG_VIDEO_HD_MC '201703070154', 'P201703002170001', '00003FLT', '51204'
	exec SP_U_TBLOG_VIDEO_HD_MC '00003FLT', 'S', '51204' --上傳中
	exec SP_U_TBLOG_VIDEO_HD_MC '00003FLT', 'C', '51204' --上傳完成
	select * from TBPGM_PROMO where FSPROMO_ID = '20170300217' --PTS2墊檔用_動物排排站05_長臂猿

	exec SP_Q_INFO_BY_VIDEOID '00003GJS' --O, T
	exec SP_Q_INFO_BY_VIDEOID '00003FGY' --浩克慢遊3#24二林二水promo_pts3, O, T
	
	--無檔案
	exec SP_Q_INFO_BY_VIDEOID '00003FE5' --FCSTATUS = 88, MarkUpload 無檔案, 20170300163
	select * from TBPGM_PROMO where FSPROMO_ID = '20170300163' --PTS1文學fb#3噬夢人PROMO  --> 已經有新的短帶檔案

	select * from TBLOG_VIDEO where FSCHANGE_FILE_NO <> ''
	exec SP_Q_INFO_BY_VIDEOID '00003HE7'
end

if @case = 201704241540-201703241757 begin --荼蘼
	exec SP_Q_INFO_BY_VIDEOID '00003B5J' --201701170134, 154739, G201749200090001, 2017-02-07 17:31:24.813, done
	exec SP_Q_TBTRACKINGLOG_BY_DATE '154739', '213981', '105745', 'G201749200090001', '003B5J', '2017-02-07' --QC 轉檔
	select * from TBTRANSCODE where FNTRANSCODE_ID = 154739
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201749200090001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201749200090001'


	select * from TBTRACKINGLOG where FDTIME > '2017-04-24' and FSPROGRAM like '%CallIngest_HD%' order by FDTIME --執行網路服務(WSBROADCAST.GetTBLOG_VIDEO_PATH_BYJOBID)時發生錯誤，轉檔編號：160503
	 
	--http://10.13.210.3/DataSource/WSSTT.asmx?op=CallIngest_HD
	--return TRANSCODE_ID = 160503
	select * from TBTRANSCODE where FNTRANSCODE_ID = 160503 --111232
	select * from TBTRACKINGLOG where FDTIME > '2017-04-24 14:00' order by FDTIME
	exec SP_Q_TBTRACKINGLOG_BY_DATE '160503', '227141', '111232', 'G201749200090001', '003B5J', '2017-04-24'
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201749200090001'

	--\\mamstream\MAMDFS\ptstv\HVideo\2017\2017492\G201749200090001.mxf
	--\\mamstream\MAMDFS\Media\Lv\ptstv\2017\2017492\G201749200090001.mp4

	select * from TBTRAN_LOG where FDDATE > '2017/04/24' and FSSP_NAME = 'SP_U_TBLOG_VIDEO_STT_OK'
	exec SP_U_TBLOG_VIDEO_STT_OK '160503', '19.7GB', ''
end


-------------------------------------------20170327-------------------------------------
if @case = 201703271121 begin -- 卡在轉檔中 -processing
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-19'  order by FNTRANSCODE_ID --36
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-19' and FCFILE_STATUS <> 'T' order by FNTRANSCODE_ID --2
	--S, 007
	--157637,108609,G201719000010017,00003GPE,201703180013,220720 告別 1
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157637', '108609', '220720', 'G201719000010017', '003GPE', '2017-03-19'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201719000010017'

	--157639,108607,G201719200010014,00003GPG,201703180015,220722 銀河戰士特訓班 1
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157639', '108607', '220722', 'G201719200010014', '003GPG', '2017-03-19'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201719200010014'

end

if @case = 201703271133 begin --卡在轉檔中 processing
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-21'  order by FNTRANSCODE_ID --99
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-21' and FCFILE_STATUS <> 'T' order by FNTRANSCODE_ID --4

	--07, S
	--157765,108719,G201142100210016,00003GVW,201703210001,220999, 成語賽恩思 21
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201142100210016'
	--157767,108718,G201142100230015,00003GVY,201703210003,221001 成語賽恩思 23
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201142100230015'
	--157812,108762,G201589200010005,00001TIS,201511120064,221056, 人生圓舞曲 1
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201589200010005'
	--157818,108768,G201142100310014,00003GXT,201703210061,221066, 成語賽恩思 31
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201142100310014'
end

if @case = 201703271140 begin --送播單資料沒建完整
	select * from TBLOG_VIDEO where FSID = '2017194' and FNEPISODE = 6 --G201719400060001, 201703270043
	select * from TBBROADCAST where FSBRO_ID = '201703270043' --no record
	--exec SP_D_TBLOG_VIDEO_BYBROID '201703270043' --using sa
    --exec SP_D_TBLOG_VIDEO_SEG_BYFILEID 'G201719400060001'
end

if @case = 201703271157 begin
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-26'  order by FNTRANSCODE_ID --22
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-26' and FCFILE_STATUS <> 'T' order by FNTRANSCODE_ID --0

	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-25'  order by FNTRANSCODE_ID --25
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-25' and FCFILE_STATUS <> 'T' order by FNTRANSCODE_ID --0

	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-24'  order by FNTRANSCODE_ID --78
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-24' and FCFILE_STATUS <> 'T' order by FNTRANSCODE_ID --S:1 D:3,R:1
	--158113,109026,G201645500010009,00003HBW,201703240003,221999 天問2-2
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201645500010009' --processing, done

	select * from TBLOG_VIDEO where FSFILE_NO = 'G201667000010001'


end

if @case = 201703281051 begin --搬檔失敗
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-27'  order by FNTRANSCODE_ID --102
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-27' and FCFILE_STATUS <> 'T' order by FNTRANSCODE_ID --5
	--158275,109173,G201667300010003,00003HKT,201703270073,222394 寂寞瑪奇朵 2-2 done
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201667300010003'
	exec SP_Q_INFO_BY_VIDEOID '00003HKT'
	--158287,109185,G201669300090009,00003HLE,201703270094,222408 大腦先生 9, done
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201669300090009'
	exec SP_Q_INFO_BY_VIDEOID '00003HLE'

	--002
	--158280,109178.G201652700520001,00003GJN,201703160072,222399
	exec SP_Q_INFO_BY_VIDEOID '00003GJN' --鬧熱打擂台(120分鐘版) 52, 4/8, 07, done 重轉兩次
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201652700520001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201652700520001'
	--158321,109209,G000007208990001,00003HLD,201703270091,222448
	exec SP_Q_INFO_BY_VIDEOID '00003HLD' --我們的島 899, 3/30 13, done
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G000007208990001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G000007208990001'
	--158322,109210,G201669300250002,00003HDF,201703240049,222455
	exec SP_Q_INFO_BY_VIDEOID '00003HDF' --大腦先生 25, 3/30 12, done
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201669300250002'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201669300250002'
end

if @case = 201703281746 begin
	select * from TBLOG_VIDEO where FSARC_TYPE = '002' and FCFILE_STATUS = 'D'
	select * from TBLOG_VIDEO where FSARC_TYPE = '002' and FSCHANGE_FILE_NO <> ''
	select * from TBLOG_VIDEO where FSID = '2007907' and FNEPISODE = 1
end

if @case = 201703282325 begin --主控播出資訊
	select * from TBPROG_M where FSPGMNAME = '通靈少女' --2017724
	exec SP_Q_TBPGM_INSERT_TAPE '2017724', 6, '002'
	select * from TBLOG_VIDEO where FSID = '2017724' and FNEPISODE = 1
	--波西測試
	select * from TBLOG_VIDEO where FSID = '2013232'
	--FCFILE_STATUS = F 審核不通過
	select * from TBLOG_VIDEO where FSID = '2013232' and FNEPISODE = 16 --X 抽單
	exec SP_Q_TBPGM_INSERT_TAPE '2013232', 4, '002'  --如果沒建就不會有資料
	--建立送播單不會檢查主控播出提示
end

if @case = 201703291439-201703221413 begin
	select * from TBPGM_COMBINE_QUEUE where CAST(FDDATE as DATE) = '2017-03-22' and FSCHANNEL_ID = '08' order by FNCONBINE_NO
	--FSMEMO 有記錄 +LOGO右上
	select * from TBPGM_ARR_PROMO where CAST(FDDATE as DATE) = '2017-03-22' and FSCHANNEL_ID = '08' order by FNARR_PROMO_NO
	select * from TBPGM_COMBINE_QUEUE where CAST(FDDATE as DATE) = '2016-08-18' and FSCHANNEL_ID = '14'
end

if @case = 201703300849 begin
	exec SP_Q_INFO_BY_VIDEOID '000036DC' --not scheduled
	exec SP_Q_INFO_BY_VIDEOID '000034H6' --not scheduled
	exec SP_Q_INFO_BY_VIDEOID '00003HON' --我們的島900promo 3/31 12, R, Review
	exec SP_Q_INFO_BY_VIDEOID '00003HJK' --2017 03/31 pts1 menu 16:00 3/31, R, Review
	exec SP_Q_INFO_BY_VIDEOID '00003GUA' --2016PTS1 RM 台灣心動線-戀上台灣, 2017-03-31, 12, R 
end

if @case = 201703301000 begin --3/29 入庫失敗 Media 容量已滿
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-29'  order by FNTRANSCODE_ID --111
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-29' and FCFILE_STATUS <> 'T' order by FNTRANSCODE_ID --48
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-29' and FCFILE_STATUS = 'S' order by FNTRANSCODE_ID  --9

	--158472,-1,P201703008160001,00003HEO,201703240106,222739
	exec SP_Q_INFO_BY_VIDEOID '00003HEO' --公視生態特區-野性日本, 4/2, 14, done
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201703008160001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201703008160001'

	--轉檔完成搬檔失敗
	--158455,109325,G201570200010003,00001QYJ,201503160056,222722
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201570200010003'
	exec SP_Q_INFO_BY_VIDEOID '00001QYJ' --done
	--158494,109360,G201375500010009,00003HYD,201703290093,222771
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201375500010009'
	exec SP_Q_INFO_BY_VIDEOID '00003HYD' --done
	--158495,109361,G201527300010012,00003HYE,201703290094,222772
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201527300010012'
	exec SP_Q_INFO_BY_VIDEOID '00003HYE' --T, done
	--158496,109362,G201539200010008,00003HYG,201703290095,222773
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201539200010008'
	exec SP_Q_INFO_BY_VIDEOID '00003HYG' --T, done

	 --ANS 處理中
	--158469,109339,P201703006970001,00003H4A,201703220061,222736, FNPROCESS_STATUS=3, done
	exec SP_Q_INFO_BY_VIDEOID '00003H4A'

	--TSM 處理中
	--158470,-1,1,P201703007620001,00003HA7,201703230127,222737
	exec SP_Q_INFO_BY_VIDEOID '00003HA7' --PTS3 冰血暴 第10集 , 3/31, 14,done
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201703007620001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201703007620001'
    --158471,-1,1,P201703007850001,00003HC8,201703240016,222738
	exec SP_Q_INFO_BY_VIDEOID '00003HC8' --獨立特派員 第489集預告(三台) , 3/31, 14, done
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201703007850001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201703007850001'
	--158472,-1,1,P201703008160001,00003HEO,201703240106,222739
	exec SP_Q_INFO_BY_VIDEOID '00003HC8' --獨立特派員 第489集預告(三台) , 3/31, 14, done
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201703007850001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201703007850001'
	--158473,-1,1,P201701006540001,00003BLJ,201701200146,222740
	exec SP_Q_INFO_BY_VIDEOID '00003BLJ' --PTS3 下課花路米 小學生出任務 22 翻轉小校GO 周六 , 4/1, 14, done
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201701006540001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201701006540001'

	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-29' and FCFILE_STATUS = 'R' order by FNTRANSCODE_ID --39, FNPROCESS_STATUS = 4, done
	--158468,109338,G201734900700001,00003HP8,201703280012,222735
	exec SP_Q_TBTRACKINGLOG_BY_DATE '158468', '109338', '222735', 'G201734900700001', '003HP8', '2017-03-29' --磁碟的空間不足

	--158500,109366,G201734900730002,00003HQC,201703280063,222786
	--158504,109367,G201083802710001,00003HOM,201703270139,222790

	--reschedule
	--158518,109378,P201703004870001,00003GI3,201703160013,222811, reschedule
	exec SP_Q_INFO_BY_VIDEOID '00003GI3' --國際幸福城市 臺東_英文版, done

end

if @case = 201703301019 begin --磁帶被下架 done
	exec SP_Q_INFO_BY_VIDEOID '00000BGQ' --G00005L$, 曬棉被的好天氣, 4/10, 08, already in Review
	exec SP_Q_INFO_BY_VIDEOID '00000CXS' --E000049L4, 應屆退休生(破口版), 4/10, 08, already in Review
end

if @case = 201703301055 begin --轉檔成功搬檔失敗 無法置換 done
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-30'  order by FNTRANSCODE_ID
	exec SP_Q_INFO_BY_VIDEOID '00003HJX' --O, R, P201703008560001, 2017 04/01 pts1 menu 01:00, 4/1 12
	--158560,109420,P201703008560001,00003HJX,201703270031,222927
	exec SP_Q_TBTRACKINGLOG_BY_DATE '158560', '109420', '222927', 'P201703008560001', '003HJX', '2017-03-30'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'P201703008560001'
	--刪除重建短帶
	--exec SP_D_TBLOG_VIDEO_BYBROID '201703270031' --using sa
    --exec SP_D_TBLOG_VIDEO_SEG_BYFILEID 'P201703008560001'

	exec SP_Q_INFO_BY_VIDEOID '00003I1O'
end

if @case = 201703301911 begin --轉檔失敗
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-30'  order by FNTRANSCODE_ID --88
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-30' and FCFILE_STATUS <> 'T' order by FNTRANSCODE_ID 
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-30' and FCFILE_STATUS = 'S' order by FNTRANSCODE_ID  --8
	--158551,109411,G201328800010003,00003I3N,201703300004,222915, 007, processing, done
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201328800010003'
	exec SP_Q_INFO_BY_VIDEOID '00003I3N'
	--158626,109489,G201497000010008,00003I7U,201703300186,223012, 007
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201497000010008'
	exec SP_Q_INFO_BY_VIDEOID '00003I7U' --done

	--002
	--158624,109487,G201719100010013,00003DB7,201702100006,223010
	exec SP_Q_INFO_BY_VIDEOID '00003DB7'
	update TBLOG_VIDEO set FCFILE_STATUS = 'T' where FSFILE_NO = 'G201719100010013'
	--158630,109492,G201758100580001,00003HRG,201703280113,223016
	exec SP_Q_INFO_BY_VIDEOID '00003HRG'
	update TBLOG_VIDEO set FCFILE_STATUS = 'T' where FSFILE_NO = 'G201758100580001'
	--158651,109514,G200785301720002,00003HQU,201703280084,223046
	exec SP_Q_INFO_BY_VIDEOID '00003HQU'
	update TBLOG_VIDEO set FCFILE_STATUS = 'T' where FSFILE_NO = 'G200785301720002'
	--158654,-1,G200785101770003,00003HQR,201703280081,223049
	exec SP_Q_INFO_BY_VIDEOID '00003HQR' --S, ㄤ牯ㄤ牯咕咕咕(饒平腔) 177 4/15 07 processing...done
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G200785101770003'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G200785101770003'
	--158656,109518,G200785201820002,00003HQT,201703280083,223051
	exec SP_Q_INFO_BY_VIDEOID '00003HQT' --T
	--158657,-1,G200618607880002,00003HQY,201703280093,223052
	exec SP_Q_INFO_BY_VIDEOID '00003HQY' --S, 奧林P客 788 4/16, 07, processing... done
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G200618607880002'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G200618607880002'

	--call Web service do anystream reschedule http://10.13.220.3/WS/WSASC/WSAnystream.asmx?op=DoJobRetry
	--158604,109464,G000081614210005,00003I7A,201703300168,222982
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G000081614210005'

	--158555,109415,G000081614170004,00003I3Q,201703300014,222919
	exec SP_Q_INFO_BY_VIDEOID '00003I3Q'

end

if @case = 201703302312 begin --QUE 表
	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20170300290'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20170300130'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

end

if @case = 201703311559 begin --2017福氣來了 56 檔案有問題無法轉檔，重建送播單換檔案
	exec SP_Q_INFO_BY_VIDEOID '00003I7Q' --G201758100560001,2017-04-03, 201703300183
    --exec SP_D_TBLOG_VIDEO_BYBROID '201703300183' --using sa
    --exec SP_D_TBLOG_VIDEO_SEG_BYFILEID 'G201758100560001'
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201758100560001'
end

if @case = 201703311607 begin
	select * from TBPROG_M where FSPGMNAME like '%深海傳奇%' --2017707
	select * from TBLOG_VIDEO where FSID = '2017707' and FNEPISODE = 2 --G201770700020001, 201703310077
	select * from TBBROADCAST where FSBRO_ID = '201703310077' --no record
	--exec SP_D_TBLOG_VIDEO_BYBROID '201703310077' --using sa 此為刪除轉檔單下所有轉檔資料
    --exec SP_D_TBLOG_VIDEO_SEG_BYFILEID 'G201770700020001'
	exec SP_D_TBLOG_VIDEO_BYFILENO 'G201770700020001'
end

if @case = 201703311732-201612271527-201608181145-201608121500 begin

        --估算已轉檔入庫的資料量  328857.246 GB = 328T, 20170123:440T, 20170331 486T
        select
        SUM(CASE
                        WHEN FSFILE_SIZE like '%GB%' THEN CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2))
                        WHEN FSFILE_SIZE like '%MB%' THEN CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2))/1000
                END
        )
        from TBLOG_VIDEO where FCFILE_STATUS = 'T' and FSTYPE = 'G' and FSFILE_SIZE <> '' --486992.877 GB = 486T , 440T 20170123
		select 486/0.8 --607 LTO4

		select
        SUM(CASE
                        WHEN FSFILE_SIZE like '%GB%' THEN CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2))
                        WHEN FSFILE_SIZE like '%MB%' THEN CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2))/1000
                END
        )
        from TBLOG_VIDEO where FCFILE_STATUS = 'T' and FSTYPE = 'G' and FSFILE_SIZE <> ''  and FDUPDATED_DATE between '2017-01-23' and '2017-03-31' --47T
	
		--2017-01 26T
		select
        SUM(CASE
                        WHEN FSFILE_SIZE like '%GB%' THEN CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2))
                        WHEN FSFILE_SIZE like '%MB%' THEN CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2))/1000
                END
        )
        from TBLOG_VIDEO where FCFILE_STATUS = 'T' and FSTYPE = 'G' and FSFILE_SIZE <> ''  and FDUPDATED_DATE between '2017-01-01' and '2017-01-31'

		--201702 20T
		select
        SUM(CASE
                        WHEN FSFILE_SIZE like '%GB%' THEN CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2))
                        WHEN FSFILE_SIZE like '%MB%' THEN CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2))/1000
            END
        )
        from TBLOG_VIDEO where FCFILE_STATUS = 'T' and FSTYPE = 'G' and FSFILE_SIZE <> ''  and FDUPDATED_DATE between '2017-02-01' and '2017-02-28'

		--201703 19T 轉檔量下降原因為 18T 切換到 NAS
		select
        SUM(CASE
                        WHEN FSFILE_SIZE like '%GB%' THEN CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2))
                        WHEN FSFILE_SIZE like '%MB%' THEN CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2))/1000
            END
        )
        from TBLOG_VIDEO where FCFILE_STATUS = 'T' and FSTYPE = 'G' and FSFILE_SIZE <> ''  and FDUPDATED_DATE between '2017-03-01' and '2017-03-31'
		
		--每日入庫量統計
		select
		CAST(FDUPDATED_DATE as DATE) 日期,
        SUM(CASE
                        WHEN FSFILE_SIZE like '%GB%' THEN CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2))
                        WHEN FSFILE_SIZE like '%MB%' THEN CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2))/1000
            END
        ) '入庫量(GB)'
        from TBLOG_VIDEO where FCFILE_STATUS = 'T' and FSTYPE = 'G' and FSFILE_SIZE <> ''  and FDUPDATED_DATE between '2017-01-01' and GETDATE()
		group by CAST(FDUPDATED_DATE as DATE)
		order by CAST(FDUPDATED_DATE as DATE)

		select 1000*8/2 --4000S
end

if @case = 201704021929 begin
	exec SP_Q_INFO_BY_VIDEOID '00003G9E' --PTS1 台灣囝仔讚promo-123, 2017-04-03, R
	exec SP_Q_INFO_BY_VIDEOID '00003HEB' --勝利催落去21_溫暖篇Promo_pts2
end

if @case = 201704022232 begin
	select * from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-03-28' and FCFILE_STATUS <> 'T' order by FNTRANSCODE_ID --S:3, R:3
	--158331,109219,4,G201073200190007,00003HP4,201703280006,222524
	--158332,109220,201073200200015,00003HP5,201703280008,222525
end

if @case = 201704030812 begin
	select '--' + CAST(FNTRANSCODE_ID as varchar(20)) + ', ' + CAST(FNANYSTREAM_ID as varchar(20)) + ', ' + CAST(FNPROCESS_STATUS as varchar(3)) + ', '
	+ FSFILE_NO + ', ' + FSVIDEO_PROG + ', ' + FSBRO_ID + ', ' + FSARC_TYPE + ', ' + FCFILE_STATUS + ', ' + CAST(ISNULL(FNTSMJOB_ID, 0) as varchar(20)) as ITEM, 
	'exec SP_Q_INFO_BY_VIDEOID ' + '''' + FSVIDEO_PROG + '''' as CMD1,
	'update TBLOG_VIDEO set FCFILE_STATUS = ''R'' where FSFILE_NO = ' + ''''+ FSFILE_NO + '''' as CMD2,
	* from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) between '2017-03-1' and CAST(GETDATE() -1 as DATE)  and FCFILE_STATUS in ('R', 'S') order by FNTRANSCODE_ID --23

	--156316, 107325, 8, G000081613750007, 00003F13, 201703010145, 007, R, 0
	exec SP_Q_INFO_BY_VIDEOID '00003F13' --R
	--157058, 108070, 8, P201703002820001, 00003FTM, 201703090083, 002, R, 219295
	exec SP_Q_INFO_BY_VIDEOID '00003FTM'

	--157490, 108456, 4, P201703005140001, 00003GJU, 201703160082, 002, R, 220295
	exec SP_Q_INFO_BY_VIDEOID '00003GJU' --reschedule

	--158331, 109219, 4, G201073200190007, 00003HP4, 201703280006, 007, S, 222524, try 3, done
	exec SP_Q_INFO_BY_VIDEOID '00003HP4'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201073200190007'

	--158332, 109220, 4, G201073200200015, 00003HP5, 201703280008, 007, S, 222525, done
	exec SP_Q_INFO_BY_VIDEOID '00003HP5'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201073200200015'
	--158384, 109260, 4, G201529000010007, 00003HQH, 201703280069, 007, S, 222577, done
	exec SP_Q_INFO_BY_VIDEOID '00003HQH'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G20107320020007' --no record
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201529000010007'
	select * from TBLOG_VIDEO where FSFILE_NO = 'G20107320020007'

	--158413, -1, 1, P201703008080001, 00003HEB, 201703240095, 002, R, 0, done
	exec SP_Q_INFO_BY_VIDEOID '00003HEB' --勝利催落去21_溫暖篇Promo_pts2, 2017-04-03, 13
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201703008080001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201703008080001'

	--158415, -1, 1, P201703004370001, 00003G9E, 201703140067, 002, R, 0, done
	exec SP_Q_INFO_BY_VIDEOID '00003G9E' --PTS1 台灣囝仔讚promo-123, 2017-04-03, 13
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201703004370001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201703004370001'

	--158417, -1, 1, P201703008140001, 00003HEM, 201703240104, 002, R, 0, done
	exec SP_Q_INFO_BY_VIDEOID '00003HEM' --生態全紀錄-深海傳奇綜合版2, 2017-04-05, 12
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201703008140001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201703008140001'

	--158555, 109415, 8, G000081614170004, 00003I3Q, 201703300014, 007, R, 222919
	exec SP_Q_INFO_BY_VIDEOID '00003I3Q'
	--158555, 109415, 8, G000081614170004, 00003I3Q, 201703300014, 007, R, 222919
	exec SP_Q_INFO_BY_VIDEOID '00003I3Q'
	-------------------------------------------------------------------------------

	--158661, 109522, 4, G000081614250007, 00003IA4, 201703310004, 007, S, 223119, done
	exec SP_Q_INFO_BY_VIDEOID '00003IA4'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G000081614250007'

	--158677, 109538, 4, G000081614280005, 00003IB9, 201703310049, 007, S, 223136, done
	exec SP_Q_INFO_BY_VIDEOID '00003IB9'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G000081614280005'

	--158678, 109539, 4, G000081614290005, 00003IBA, 201703310050, 007, S, 223137, done
	exec SP_Q_INFO_BY_VIDEOID '00003IBA'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G000081614290005'

	--158689, 109550, 4, G201644300110015, 00003IBB, 201703310051, 007, S, 223148, done
	exec SP_Q_INFO_BY_VIDEOID '00003IBB'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201644300110015'

	--158690, 109551, 4, G201644300120010, 00003IBE, 201703310052, 007, S, 223149, done
	exec SP_Q_INFO_BY_VIDEOID '00003IBE'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201644300120010'

	--158739, 109601, 4, G201499600010009, 00003ICN, 201703310115, 007, S, 223211, done
	exec SP_Q_INFO_BY_VIDEOID '00003ICN'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201499600010009'

	--158776, 109639, 4, G201560700010008, 00003IFS, 201704010001, 007, S, 223325, done
	exec SP_Q_INFO_BY_VIDEOID '00003IFS'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201560700010008'

	--158777, 109640, 4, G201554000010007, 00003IFT, 201704010002, 007, S, 223326, done
	exec SP_Q_INFO_BY_VIDEOID '00003IFT'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201554000010007'

	--158779, 109642, 4, G201569000010009, 00003IFV, 201704010004, 007, S, 223328, done
	exec SP_Q_INFO_BY_VIDEOID '00003IFV'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201569000010009'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G000081614290005'--??
	select * from TBLOG_VIDEO where FSFILE_NO = 'G000081614290005'

	--158785, 109648, 4, G201577500010008, 00003IFX, 201704010006, 007, S, 223334. done
	exec SP_Q_INFO_BY_VIDEOID '00003IFX'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201577500010008'

	--158834, 109696, 4, G201021400010001, 00003IHM, 201704020020, 007, S, 223454, done
	exec SP_Q_INFO_BY_VIDEOID '00003IHM'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201021400010001'

end

if @case = 201704042227 begin
	select * from TBPGM_COMBINE_QUEUE where FDDATE = '2016-07-10' and FSCHANNEL_ID = '12' order by FSPLAY_TIME
	select * from TBPROG_M where FSPGMNAME like '%滾石%' --2016883
	select * from TBPGM_COMBINE_QUEUE where FSPROG_ID = '2016883' and FSCHANNEL_ID = '12' order by FDDATE --2016-05-28
	select * from TBPGM_COMBINE_QUEUE where FDDATE = '2016-06-11' and FSCHANNEL_ID = '01' order by FSPLAY_TIME --滾石愛情故事 傷痕
end

if @case = 201704050919 begin --tape offsite
	exec SP_Q_INFO_BY_VIDEOID '00000NAS' --爸媽囧很大, 2017-04-17, 13, G200928504850015
end

if @case = 201704051026 begin --trace 轉檔
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-04-05' and FSMESSAGE like '%ClickTranscode%'
	select * from TBSYSTEM_CONFIG
end

if @case = 201704051347 begin --已標記上傳, 檔案已在 HDUpload, 但是, TBLOG_VIDEO_HD_MC 無資料
	exec SP_Q_INFO_BY_VIDEOID '00003HYA' --P201703008980001, 201703290091
	select * from TBPGM_PROMO where FSPROMO_ID = '20170300898' --誰來晚餐9#1_promo_pts1今晚版
	select * from TBBROADCAST where FSBRO_ID = '201703290091'
	
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00003HYA'
	select * from TBLOG_VIDEO_HD_MC where FSFILE_NO = 'P201703008980001' or FSVIDEO_ID = '00003HYA' or FSBRO_ID = '201703290091'
	--http://10.13.220.3/DataSource/WSHD_MC.asmx?op=Update_HD_MC_STATUS 無效
	exec SP_U_TBLOG_VIDEO_HD_MC_BY_FILE_NO 'P201703008980001', 'C', '51204'

	exec SP_Q_INFO_BY_VIDEOID '00003IIX' --誰來晚餐9#1_promo_pts1今晚版, 2017-04-07
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00003IIX' --no record

	select * from TBLOG_VIDEO where FSFILE_NO = 'P201703004370001'
	select * from TBLOG_VIDEO where FSFILE_NO = 'P201703008980001' --FCFILE_STATUS = B
	select * from TBLOG_VIDEO where FSID = '20170300898'

	--FSVIDEO_ID = 00003IIX, FCFILE_STATUS = NULL, FCLOW_RES = NULL
	exec SP_Q_TBPGM_PROMO_BY_EFF_ZONE '20170300898', '誰來晚餐9#1_promo_pts1今晚版', '', '00:00:59:28', '', '12', '2017-04-07', '0000100'
	select * from TBPGM_PROMO_EFFECTIVE_ZONE where FSPROMO_ID = '20170300898' --FSWEEK = 0000100
	select * from TBPGM_PROMO_BOOKING where FSPROMO_ID = '20170300898' --FCSTATUS = R

	select * from TBLOG_VIDEOID_MAP where FSID = '20170300898' --00003IIX, not 00003HYA

	select * from TBLOG_VIDEOID_MAP where FSID = '2013232'
	exec SP_Q_INFO_BY_VIDEOID '00001O7H' --X,X
	exec SP_Q_INFO_BY_VIDEOID '00003C7M' --T
	exec SP_Q_INFO_BY_VIDEOID '00002CCZ' --no record
	exec SP_Q_INFO_BY_VIDEOID '00001XH5' --T, X
	exec SP_Q_INFO_BY_VIDEOID '00001XH6' --X
	exec SP_Q_INFO_BY_VIDEOID '00002B7J' --no record
	exec SP_Q_INFO_BY_VIDEOID '00001XH3' --X,X
	exec SP_Q_INFO_BY_VIDEOID '00001XH4' --X,R
	exec SP_Q_INFO_BY_VIDEOID '00001XH1' --X,X,X

	select FSID, COUNT(FSID) from TBLOG_VIDEOID_MAP where FNEPISODE = 0 group by FSID having COUNT(FSID) > 1
	select * from TBLOG_VIDEOID_MAP where FSID = '0000060'
	select * from TBLOG_VIDEO where FSID = '20111200004' --2
	select * from TBLOG_VIDEOID_MAP where FSID = '20111200004'
	select * from TBLOG_VIDEO where FSID = '20111200010' --3

	exec SP_I_TBLOG_VIDEOID_MAP '20170300898', 0, '002', '00003HYA'  --會取代原有的
end

if @case = 201704051639 begin -- 無法 QC
	exec SP_Q_INFO_BY_VIDEOID '00003GAG' --20170300455, N, 4/12  有排播, 實際尚未排播--> 審核不通過 FCSTATUS = X, FCFILE_STATUS = F
	select * from TBPGM_PROMO where FSPROMO_ID = '20170300455' --循環帶-新北市新月橋
	select * from TBPGM_ARR_PROMO where FSPROMO_ID = '20170300455'
	select * from TBLOG_VIDEOID_MAP where FSID = '20170300455'
end



if @case = 201704051806 begin --todo
	exec SP_Q_INFO_BY_VIDEOID '000022YX' --not scheduled, E001536L4, N
end


if @case = 201704051840 begin  --註冊 TSM recall job
	select * from TBTSM_JOB --FSSOURCE_PATH : 有 Node 的資訊
	select * from TBTSM_JOB where FNSTATUS <> 3 order by FNTSMJOB_ID desc
	select * from TBTSM_JOB_WORKER
end


---------------------------------------------20170406----------------------------------
if @case = 201704061431 begin --todo
	exec SP_Q_INFO_BY_VIDEOID '00003HQI' --O, B, not scheduled, 審核通過卻未轉檔 G201770800010003, 2017708, 1
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201770800010003'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201770800010003'

	select * from TBPROG_M where FSPROG_ID = '2017708' --女媧補天(寮)天穿
end

if @case = 201704061447-201704051744 begin
	select '--' + CAST(FNTRANSCODE_ID as varchar(20)) + ', ' + CAST(FNANYSTREAM_ID as varchar(20)) + ', ' + CAST(FNPROCESS_STATUS as varchar(3)) + ', '
	+ FSFILE_NO + ', ' + FSVIDEO_PROG + ', ' + FSBRO_ID + ', ' + FSARC_TYPE + ', ' + FCFILE_STATUS + ', ' + CAST(ISNULL(FNTSMJOB_ID, 0) as varchar(20)) as ITEM, 
	'exec SP_Q_INFO_BY_VIDEOID ' + '''' + FSVIDEO_PROG + '''' as CMD1,
	'update TBLOG_VIDEO set FCFILE_STATUS = ''R'' where FSFILE_NO = ' + ''''+ FSFILE_NO + '''' as CMD2,
	* from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-04-05' and FCFILE_STATUS in ('R', 'S') order by FNTRANSCODE_ID --(4,S) : 2

	--158924, 109784, 4, G201733100010001, 00003HPG, 201703280028, 002, S, 223744
	exec SP_Q_INFO_BY_VIDEOID '00003HPG' --雲係麼(個)色, 4/15, 07, reschedule, done
	--158962, 109803, 4, G201772200010001, 00003HYN, 201703290106, 002, S, 223786
	exec SP_Q_INFO_BY_VIDEOID '00003HYN' --多元樂活  精彩好客(寮)天穿, 4/17, 07, reschedule, done
end

if @case = 201704061622 begin
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-04-06' and FSMESSAGE like '%Handler_AnsNotifyReceiver%'

end

if @case = 201704071531 begin
	--已標記上傳, 找不到檔案
	exec SP_Q_INFO_BY_VIDEOID '00003E7M' --P201702005500001, 隱形冠軍系列(臺灣螺旋槳)水上歡樂的推手 臺灣螺旋槳, 2017-04-15, 08, 201702200125
	exec SP_Q_INFO_BY_VIDEOID '00003E7U' --民俗采風系列(傳統布袋戲)掌中乾坤_英語版
end

--------------------------------------20170407------------------------------------
if @case = 201704070844 begin
	select '--' + CAST(FNTRANSCODE_ID as varchar(20)) + ', ' + CAST(FNANYSTREAM_ID as varchar(20)) + ', ' + CAST(FNPROCESS_STATUS as varchar(3)) + ', '
	+ FSFILE_NO + ', ' + FSVIDEO_PROG + ', ' + FSBRO_ID + ', ' + FSARC_TYPE + ', ' + FCFILE_STATUS + ', ' + CAST(ISNULL(FNTSMJOB_ID, 0) as varchar(20)) as ITEM, 
	'exec SP_Q_INFO_BY_VIDEOID ' + '''' + FSVIDEO_PROG + '''' as CMD1,
	'update TBLOG_VIDEO set FCFILE_STATUS = ''R'' where FSFILE_NO = ' + ''''+ FSFILE_NO + '''' as CMD2,
	* from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-04-06' and FCFILE_STATUS in ('R', 'S') order by FNTRANSCODE_ID --(4,S) : 1

	--159037, 109866, 4, G201769800040002, 00003IMP, 201704050085, 002, S, 223958
	exec SP_Q_INFO_BY_VIDEOID '00003IMP' --酸甜之味, 4, 12, reschedule, done
end

if @case = 201704070910 begin
	select * from TBTSM_JOB where FDREGISTER_TIME between CAST(GETDATE() -1 as DATE) and GETDATE() order by FDREGISTER_TIME
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = CAST(CURRENT_TIMESTAMP - 1 as DATE) and FSCATALOG = 'ERROR' order by FDTIME
	--https://docs.google.com/spreadsheets/d/18tfmY1r8YCXe4nvfRerkziQ4ozrj2gGCKi0z0yrVdLE/edit#gid=60206590&range=B1881
end

if @case = 201704071747-201605311403 begin -- 新增測試頻道 PTS_TEST
        select * from TBZCHANNEL
        select COLUMN_NAME, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH, * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'TBZCHANNEL'
        -- 新增頻道
        declare @FSCHANNEL_ID varchar(2) = '15'
        declare @FSCHANNEL_NAME nvarchar(100) = 'PTS_TEST'
        declare @FSSHOWNAME nvarchar(20) = 'PTS_TEST'
        declare @FSCHANNEL_TYPE varchar(3) = '002'
        declare @FSSORT varchar(2) = '12'
        declare @FSSHORTNAME varchar(10) = ''
        declare @FSOLDTABLENAME varchar(20) = 'PSHOW_DIMO'
        declare @FSTSM_POOLNAME varchar(20) = '/ptstv'
        declare @FNDIR_ID bigint = 1
        declare @FSCREATED_BY varchar(50) = '51204'
        declare @FDCREATED_DATE datetime = (select CURRENT_TIMESTAMP)
        declare @FSUPDATED_BY varchar(50) = '51204'
        declare @FDUPDATED_DATE datetime = (select CURRENT_TIMESTAMP)
        declare @FBISENABLE bit = 1


        insert into TBZCHANNEL(FSCHANNEL_ID, FSCHANNEL_NAME, FSSHOWNAME, FSCHANNEL_TYPE, FSSORT, FSTSM_POOLNAME, FNDIR_ID, FSCREATED_BY, 
        FDCREATED_DATE, FSUPDATED_BY, FDUPDATED_DATE, FBISENABLE, FSSHORTNAME, FSOLDTABLENAME)
        values(@FSCHANNEL_ID, @FSCHANNEL_NAME, @FSSHOWNAME, @FSCHANNEL_TYPE, @FSSORT, @FSTSM_POOLNAME, @FNDIR_ID, @FSCREATED_BY, @FDCREATED_DATE, @FSUPDATED_BY, @FDUPDATED_DATE, @FBISENABLE,
        @FSSHORTNAME, @FSOLDTABLENAME)

        update TBZCHANNEL set FSSHORTNAME = '', FSOLDTABLENAME = 'PSHOW_DIMO' where FSCHANNEL_ID = '13'
        update TBZCHANNEL set FSSHORTNAME = '', FSOLDTABLENAME = 'PSHOW_PTS' where FSCHANNEL_ID = '12'
		update TBZCHANNEL set FSSHOWNAME = 'PTS_TEST' where FSCHANNEL_ID = '15'

        declare @d datetime = (select CURRENT_TIMESTAMP)
        select @d

        --exec SP_I_TBZCHANNEL @FSCHANNEL_ID, @FSCHANNEL_NAME, @FSSHOWNAME, @FSSORT, @FSSHORTNAME, @FSOLDTABLENAME, @FSCREATED_BY, @FSUPDATED_BY

        select * from TBDIRECTORY_GROUP
        select * from TBGROUPS --功能群組
        select * from TBMODULE_GROUP
        select * from TBMODULE_DETAIL where FSMODULE_ID = 'S100001'-- 模組列表
        select * from TBMODULE_CAT  --查詢群組權限列表，模組的分類

        select * from TBMODULE_GROUP where FSMODULE_ID = 'P101002'
        select g.FSGROUP_ID, g.FSGROUP, g.FSDESCRIPTION, m.FSMODULE_ID, m.FSFUNC_NAME, m.FSHANDLER, m.FSDESCRIPTION
        from TBMODULE_GROUP mg join TBGROUPS g on mg.FSGROUP_ID = g.FSGROUP_ID join TBMODULE_DETAIL m on mg.FSMODULE_ID = m.FSMODULE_ID
        order by g.FSGROUP_ID

        select g.FSGROUP_ID, g.FSGROUP, g.FSDESCRIPTION, m.FSMODULE_ID, m.FSFUNC_NAME, m.FSHANDLER, m.FSDESCRIPTION
        from TBMODULE_GROUP mg join TBGROUPS g on mg.FSGROUP_ID = g.FSGROUP_ID join TBMODULE_DETAIL m on mg.FSMODULE_ID = m.FSMODULE_ID
        where FSGROUP = '排表群組'
        order by g.FSGROUP_ID

        select g.FSGROUP_ID, g.FSGROUP, g.FSDESCRIPTION, m.FSMODULE_ID, m.FSFUNC_NAME, m.FSHANDLER, m.FSDESCRIPTION
        from TBMODULE_GROUP mg join TBGROUPS g on mg.FSGROUP_ID = g.FSGROUP_ID join TBMODULE_DETAIL m on mg.FSMODULE_ID = m.FSMODULE_ID
        where m.FSMODULE_ID = 'P101001'
        order by g.FSGROUP_ID -- P101001 = 匯入公視節目表

        -- 增加模組模組
		--匯入節目表
        select * from TBMODULE_DETAIL where FSFUNC_NAME like '%匯入%' --FSMODULE_ID = P1 + ch + 001
        insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P115001', '匯入PTS_TEST節目表', '', 'treeViewPGMQUEUE')

		select * from TBMODULE_DETAIL where FSFUNC_NAME like '%節目表維護%' --FSMODULE_ID = P1 + ch + 002
        insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P115002', 'PTS_TEST節目表維護', '', 'treeViewPGMQUEUE')
        update TBMODULE_DETAIL set FSDESCRIPTION = '匯入PTS_TEST節目表', FSFUNC_NAME = '匯入PTS_TEST節目表' where FSMODULE_ID = 'P115002'

        select * from TBMODULE_CAT where FSMODULE_CAT_NAME like '%預排%' --頻道預排 = P2
        select * from TBMODULE_DETAIL where FSFUNC_NAME like '%預排%' --FSMODULE_ID = P2 + ch + 001 | 002
        insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P215001', 'PTS_TEST頻道預排設定', '', 'treeViewPromoChannelLink')
        insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P215002', 'PTS_TEST節目預排設定', '', 'treeViewPromoProglLink')

        select * from TBMODULE_CAT where FSMODULE_CAT_NAME like '%播出%' --播出運行表 = P5
        select * from TBMODULE_DETAIL where FSFUNC_NAME like '%播出%'  
		--FSMODULE_ID = P5 + ch + 001(播出運行表維護) | 002（查詢播出運行表）| 003(插入宣傳帶) | 006(轉出 LST) | 007（列印播出運行表）| 008(匯入 LOUTH KEY)
        select * from TBMODULE_DETAIL where FSMODULE_ID like 'P513%'
        insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P515001', 'PTS_TEST播出運行表維護', '', NULL)
        insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P515002', 'PTS_TEST查詢播出運行表', '', NULL)
        insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P515003', 'PTS_TEST插入宣傳帶', '', 'treeViewInsertPromo')
        insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P515006', 'PTS_TEST轉出LST', '', NULL)
        insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P515007', 'PTS_TEST列印播出運行表', '', NULL)
        insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P515008', 'PTS_TEST匯入Louth Key', '', 'treeViewLouthKey')

        select * from TBMODULE_DETAIL where FSMODULE_ID like '%62%'
        select * from TBMODULE_DETAIL where FSMODULE_ID like '%61%'
end

if @case = 201704080719 begin
	exec SP_Q_INFO_BY_VIDEOID '00003DCJ'
end

if @case = 201704080720 begin --done
	select '--' + CAST(FNTRANSCODE_ID as varchar(20)) + ', ' + CAST(FNANYSTREAM_ID as varchar(20)) + ', ' + CAST(FNPROCESS_STATUS as varchar(3)) + ', '
	+ FSFILE_NO + ', ' + FSVIDEO_PROG + ', ' + FSBRO_ID + ', ' + FSARC_TYPE + ', ' + FCFILE_STATUS + ', ' + CAST(ISNULL(FNTSMJOB_ID, 0) as varchar(20)) as ITEM, 
	'exec SP_Q_INFO_BY_VIDEOID ' + '''' + FSVIDEO_PROG + '''' as CMD1,
	'update TBLOG_VIDEO set FCFILE_STATUS = ''R'' where FSFILE_NO = ' + ''''+ FSFILE_NO + '''' as CMD2,
	* from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-04-07' and FCFILE_STATUS in ('R', 'S') order by FNTRANSCODE_ID --(4,S) : 1

	--159165, 109985, 4, G201597400010011, 00003ISI, 201704070001, 007, S, 224199
	exec SP_Q_INFO_BY_VIDEOID '00003ISI'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201597400010011' --done
	--159169, 109989, 4, G201658300010010, 00003ISU, 201704070012, 007, S, 224204
	exec SP_Q_INFO_BY_VIDEOID '00003ISU'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201658300010010' --done
	--159272, 110082, 4, G201667000100001, 00003ITT, 201704070052, 002, S, 224321
	exec SP_Q_INFO_BY_VIDEOID '00003ITT' --done
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201667000100001'
end

if @case = 201704081915 begin
	exec SP_Q_INFO_BY_VIDEOID '00000DG3'
end

if @case = 201704100842 begin --done
	select '--' + CAST(FNTRANSCODE_ID as varchar(20)) + ', ' + CAST(FNANYSTREAM_ID as varchar(20)) + ', ' + CAST(FNPROCESS_STATUS as varchar(3)) + ', '
	+ FSFILE_NO + ', ' + FSVIDEO_PROG + ', ' + FSBRO_ID + ', ' + FSARC_TYPE + ', ' + FCFILE_STATUS + ', ' + CAST(ISNULL(FNTSMJOB_ID, 0) as varchar(20)) as ITEM, 
	'exec SP_Q_INFO_BY_VIDEOID ' + '''' + FSVIDEO_PROG + '''' as CMD1,
	'update TBLOG_VIDEO set FCFILE_STATUS = ''R'' where FSFILE_NO = ' + ''''+ FSFILE_NO + '''' as CMD2,
	* from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-04-07' and FCFILE_STATUS in ('R', 'S') order by FNTRANSCODE_ID

	--TSM 處理中
	--159181, -1, 1, P201703010880001, 00003IDZ, 201703310149, 002, S, 224217, done
	exec SP_Q_INFO_BY_VIDEOID '00003IDZ' --天涯共此時1400-3, 2017-04-15, 08
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'P201703010880001'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201703010880001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201703010880001'

	--159182, -1, 1, P201703010890001, 00003IE0, 201703310150, 002, S, 224218. done
	exec SP_Q_INFO_BY_VIDEOID '00003IE0' --天涯共此時1500-1, 2017-04-15, 08
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'P201703010890001'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201703010890001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201703010890001'

	--159183, -1, 1, P201704000580001, 00003INU, 201704050141, 002, S, 224219, done
	exec SP_Q_INFO_BY_VIDEOID '00003INU' --promo_勞動之王1-4集, 2017-04-15, 08
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'P201704000580001'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201704000580001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201704000580001'

	--159184, -1, 1, P201703008950001, 00003HXW, 201703290078, 002, S, 224220, done
	exec SP_Q_INFO_BY_VIDEOID '00003HXW' --勞動之王promo綜合版, 2017-04-15, 08
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'P201703008950001'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201703008950001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201703008950001'

	--159185, -1, 1, P201704000790001, 00003IP4, 201704060029, 002, S, 224221, done
	exec SP_Q_INFO_BY_VIDEOID '00003IP4' --文宣-臺灣文博會, 2017-04-15, 08
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'P201704000790001'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201704000790001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201704000790001'

	--轉檔失敗
	--159300, -1, 1, G200626205210002, 00003DCK, 201702100078, 002, R, 0 轉檔失敗，重啟後卡在 TSM 處理中，重新審核, done
	exec SP_Q_INFO_BY_VIDEOID '00003DCK'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G200626205210002'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G200626205210002'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G200626205210002'

	--搬檔失敗
	--159165, 109985, 4, G201597400010011, 00003ISI, 201704070001, 007, S, 224199	
	exec SP_Q_INFO_BY_VIDEOID '00003ISI'	--done
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201597400010011'

	--159169, 109989, 4, G201658300010010, 00003ISU, 201704070012, 007, S, 224204	
	exec SP_Q_INFO_BY_VIDEOID '00003ISU'	--done
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201658300010010'

	--ANS 處理中
	--159180, 110000, 3, P201703010860001, 00003IDF, 201703310147, 002, S, 224216, done
	exec SP_Q_INFO_BY_VIDEOID '00003IDF'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'P201703010860001'

end

if @case = 201704101017 begin
	exec SP_Q_INFO_BY_VIDEOID '00003DCK' --G200626205210002, 2006262, 521
	select * from TBLOG_VIDEO where FSID = '2006262' and FNEPISODE = 520 --T
end

if @case = 201704101046 begin --4/8, 4/9 都正常
	select '--' + CAST(FNTRANSCODE_ID as varchar(20)) + ', ' + CAST(FNANYSTREAM_ID as varchar(20)) + ', ' + CAST(FNPROCESS_STATUS as varchar(3)) + ', '
	+ FSFILE_NO + ', ' + FSVIDEO_PROG + ', ' + FSBRO_ID + ', ' + FSARC_TYPE + ', ' + FCFILE_STATUS + ', ' + CAST(ISNULL(FNTSMJOB_ID, 0) as varchar(20)) as ITEM, 
	'exec SP_Q_INFO_BY_VIDEOID ' + '''' + FSVIDEO_PROG + '''' as CMD1,
	'update TBLOG_VIDEO set FCFILE_STATUS = ''R'' where FSFILE_NO = ' + '''' + FSFILE_NO + '''' as CMD2,
	* from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-04-10' and FCFILE_STATUS in ('R', 'S') order by FNTRANSCODE_ID --(S:8) (R:2)

	--159385, 110195, 8, D201704000010001,         , 201704070112, 020, R, 224730	
	exec SP_Q_INFO_BY_VIDEOID '        '	
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'D201704000010001'

	--159387, 110197, 4, G201668800010006, 00003IZG, 201704100004, 007, S, 224732	done
	exec SP_Q_INFO_BY_VIDEOID '00003IZG'	
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201668800010006'

	--159396, 110206, 4, G201224900010004, 00003IZJ, 201704100010, 007, S, 224742	done
	exec SP_Q_INFO_BY_VIDEOID '00003IZJ'	
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201224900010004'

	--159416, 110224, 4, G201784400010001, 00003J0U, 201704100047, 007, S, 224765	done
	exec SP_Q_INFO_BY_VIDEOID '00003J0U'	
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201784400010001'

	--ANS錯誤
	--159447, 110255, 8, G000081614980008, 00003IRF, 201704060127, 007, R, 224807	
	exec SP_Q_INFO_BY_VIDEOID '00003IRF'	
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G000081614980008'

	--002
	--159417, 110225, 4, G201652700540001, 00003HRO, 201703280116, 002, S, 224766	鬧熱打擂台(120分鐘版). done
	exec SP_Q_INFO_BY_VIDEOID '00003HRO'	
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201652700540001'

	--159422, 110230, 4, G201781300010003, 00003IZS, 201704100014, 002, S, 224771	冰河古流路的芳香-鹿過台灣, done
	exec SP_Q_INFO_BY_VIDEOID '00003IZS'	
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201781300010003'

	--159448, 110256, 4, G201773600080002, 00003HA1, 201703230121, 002, S, 224808	2016-17 歐霸足球聯賽. done
	exec SP_Q_INFO_BY_VIDEOID '00003HA1'	
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201773600080002'

	--159457, 110265, 4, G201773700110001, 00003ILC, 201704050037, 002, S, 224819	2017歐洲花式滑冰錦標賽
	exec SP_Q_INFO_BY_VIDEOID '00003ILC'	
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201773700110001'

	--159465, 110273, 4, G201772500010001, 00003IBT, 201703310073, 002, S, 224829	童遊頭份(寮)天穿不插電音樂會 done
	exec SP_Q_INFO_BY_VIDEOID '00003IBT'	
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201772500010001'
end

if @case = 201704101413 begin --P201703010700002 查無資料
	select * from TBLOG_VIDEO where FSFILE_NO = 'P201703010700002' --no record
	select * from TBLOG_VIDEO where FSFILE_NO = 'P201703010700001'
	select * from TBLOG_VIDEO where FSFILE_NO = 'P201703010700003'
end

if @case = 201704101454 begin
	select * from TBLOG_SEND_HTTPPOST order by FDTIME desc
	--2017-04-10 14:55:17.707 http://mam.pts.org.tw/WS/WSASC/Handler_ParNotifyReceiver.ashx
	select * from TBTRANSCODE_PARTIAL where FNPARTIAL_ID = 158202
	select * from TBTRANSCODE where FNPARTIAL_ID = 158202 --no record
	select top 100 * from TBTRANSCODE order by FDSTART_TIME desc

	select DISTINCT FSADDR from TBLOG_SEND_HTTPPOST

	--酸甜之味
	exec SP_Q_TBLOG_VIDEO_BY_ID_EPISODE_ARCTPYE_CHECK 'G', '2017698', 4, '002' --G201769800040002
	select * from TBLOG_VIDEO where FSID = '2017698' and FNEPISODE = 4 --G201769800040001(X), G201769800040002(T)
end

if @case = 201704111348 begin --檔案已經在 HDUpload 但是仍是等待上傳
	exec SP_Q_INFO_BY_VIDEOID '00003IZY' --2017 04/13 pts1 menu 12:00, 201704100021
end

if @case = 201704120849 begin
	select '--' + CAST(FNTRANSCODE_ID as varchar(20)) + ', ' + CAST(FNANYSTREAM_ID as varchar(20)) + ', ' + CAST(FNPROCESS_STATUS as varchar(3)) + ', '
	+ FSFILE_NO + ', ' + FSVIDEO_PROG + ', ' + FSBRO_ID + ', ' + FSARC_TYPE + ', ' + FCFILE_STATUS + ', ' + CAST(ISNULL(FNTSMJOB_ID, 0) as varchar(20)) as ITEM, 
	'exec SP_Q_INFO_BY_VIDEOID ' + '''' + FSVIDEO_PROG + '''' as CMD1,
	'update TBLOG_VIDEO set FCFILE_STATUS = ''R'' where FSFILE_NO = ' + '''' + FSFILE_NO + '''' as CMD2,
	* from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-04-11' and FCFILE_STATUS in ('R', 'S') order by FNTRANSCODE_ID --(S:5)
	
	--159482, 110285, 4, G201650800010007, 00003J4M, 201704110003, 007, S, 224920	
	exec SP_Q_INFO_BY_VIDEOID '00003J4M'	
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201650800010007'  

	--159483, 110286, 4, G201644200070010, 00003J4P, 201704110006, 007, S, 224921	
	exec SP_Q_INFO_BY_VIDEOID '00003J4P'	
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201644200070010'

	--159487, 110289, 4, G201644200100011, 00003J4W, 201704110028, 007, S, 224925	
	exec SP_Q_INFO_BY_VIDEOID '00003J4W'	
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201644200100011'

	--159517, 110319, 4, G201644200130010, 00003J5K, 201704110067, 007, S, 224960	
	exec SP_Q_INFO_BY_VIDEOID '00003J5K'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201644200130010'

	--159500, 110302, 4, G201775800010002, 00003IQW, 201704060106, 002, S, 224939	
	exec SP_Q_INFO_BY_VIDEOID '00003IQW'	--全民瘋電玩(主題之夜導讀版) 2017-04-14
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201775800010002'
end

if @case = 201704121244 begin --送播單的內容是紅色
	exec SP_Q_INFO_BY_VIDEOID '00003J5B'
	select * from TBLOG_VIDEO where FSCHANGE_FILE_NO <> '' order by FDCREATED_DATE desc
end

if @case = 201704121652 begin --置換後 無法 QC
	exec SP_Q_INFO_BY_VIDEOID '00003G42' --G201690400020005 201703130145, (HD)文學Face & Book (第三季), 2017-04-13 15. done
end

if @case = 201704131526 begin --MACRO
	exec SP_Q_INFO_BY_VIDEOID '0000330O'
end

if @case = 201704141102 begin
	select * from TBLOG_VIDEO where FSID = '2016712' and FNEPISODE = 1 
	--G201671200010001, 201605310020, 00002LIC,X
	--G201671200010002, 201607040315, 00002QON,T
	--G201671200010003, 201607050157, 00002QWJ,X
	exec SP_Q_INFO_BY_VIDEOID '00002QON' --他們在島嶼寫作2-洛夫 2017-04-24, 14
	exec SP_Q_INFO_BY_VIDEOID '00002QWJ'
	exec SP_Q_INFO_BY_VIDEOID '00002LIC'
end

if @case = 201704141152 begin --刪除轉檔單
	select * from TBLOG_VIDEO where FSID = '2016634' and FSARC_TYPE = '002' --1~3 
	--G201663400010002, G201663400020002, G201663400030002
	exec SP_D_TBLOG_VIDEO_BYFILENO 'G201663400010002'  --201604060044 手動刪除查不到檔案
	--exec SP_D_TBLOG_VIDEO_BYFILENO 'G201663400020002' --201604080009
	--exec SP_D_TBLOG_VIDEO_BYFILENO 'G201663400030002' --201604200137

	select * from TBBROADCAST where FSBRO_ID = '201604080009'
	select * from TBBROADCAST where FSBRO_ID = '201604060044'

	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G201663400020002'
	update TBLOG_VIDEO set FCFILE_STATUS = 'X' where FSFILE_NO = 'G201663400010002'
	update TBLOG_VIDEO_D set FCFILE_STATUS = 'X' where FSFILE_NO = 'G201663400010002'

	select * from TBLOG_VIDEO where FSFILE_NO = 'G201663400010002'
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201663400010002' --no data

	select * from TBTRAN_LOG where FDDATE > '2017/04/14 12:00' order by FDDATE
end

if @case = 201704141132 begin --標記上傳失敗
	exec SP_Q_INFO_BY_VIDEOID '00003JF5' --客家新聞雜誌(客家製播)-242集排播需注意, 535, 2017-04-14
end

if @case = 201704141409 begin --宏觀搬檔問題，因為編表排完，把表轉到 Etere 就刪除，所以檔案不會搬
	select * from VW_PLAYLIST_FROM_NOW where FDDATE = '2017-04-17' and FSCHANNEL_ID = '08' order by FSPLAY_TIME --306
	select DISTINCT NAME, FSVIDEO_ID from VW_PLAYLIST_FROM_NOW where FDDATE = '2017-04-17' and FSCHANNEL_ID = '08' --113

	exec SP_Q_INFO_BY_VIDEOID '00003G8W' --畫嘉義 話嘉義, 2017-04-24 08
	exec SP_Q_INFO_BY_VIDEOID '00003DSY' --中華美食在臺灣系列(江蘇菜)刀工與火候的展現 創意江蘇菜, 2017-04-19 08
end

if @case = 201704141714 begin --刪除送播單
	exec SP_Q_INFO_BY_VIDEOID '00003JFC' --G201758100710001
	exec SP_D_TBLOG_VIDEO_BYFILENO 'G201758100710001'
end

if @case = 201704141809 begin
	exec SP_Q_INFO_BY_VIDEOID '00003H3R' --贏在地球村, 2017-04-18 08
	exec SP_Q_INFO_BY_VIDEOID '00003CA3'
	exec SP_Q_INFO_BY_VIDEOID '00001VM4'

	exec SP_Q_GET_CHECK_COMPLETE_LIST_MIX
end

if @case = 201704160715 begin
	select '--' + CAST(FNTRANSCODE_ID as varchar(20)) + ', ' + CAST(FNANYSTREAM_ID as varchar(20)) + ', ' + CAST(FNPROCESS_STATUS as varchar(3)) + ', '
	+ FSFILE_NO + ', ' + FSVIDEO_PROG + ', ' + FSBRO_ID + ', ' + FSARC_TYPE + ', ' + FCFILE_STATUS + ', ' + CAST(ISNULL(FNTSMJOB_ID, 0) as varchar(20)) as ITEM, 
	'exec SP_Q_INFO_BY_VIDEOID ' + '''' + FSVIDEO_PROG + '''' as CMD1,
	'update TBLOG_VIDEO set FCFILE_STATUS = ''R'' where FSFILE_NO = ' + ''''+ FSFILE_NO + '''' as CMD2,
	* from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) between '2017-04-01' and '2017-04-16' and FCFILE_STATUS in ('R', 'S') order by FNTRANSCODE_ID
	
--R
--159385, 110195, 8, D201704000010001,         , 201704070112, 020, R, 224730	
exec SP_Q_INFO_BY_VIDEOID '        '	
update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'D201704000010001'

--159447, 110255, 8, G000081614980008, 00003IRF, 201704060127, 007, R, 224807	Error, worker (439, mxf) failed, unexpectedly closed the connection while running.
exec SP_Q_INFO_BY_VIDEOID '00003IRF'
update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G000081614980008'

--159878, -1, 1, G201734900740001, 00003JJR, 201704140105, 002, R, 0 TSM 處理中
exec SP_Q_INFO_BY_VIDEOID '00003JJR' --2016-17 歐洲冠軍足球聯賽 74 2017-04-19, 14
update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201734900740001'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201734900740001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201734900740001'


--S
--007
--159482, 110285, 4, G201650800010007, 00003J4M, 201704110003, 007, S, 224920	
exec SP_Q_INFO_BY_VIDEOID '00003J4M'	
update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201650800010007'

--159557, 110352, 4, G201478600130007, 00003J7Q, 201704120004, 007, S, 225074	done
exec SP_Q_INFO_BY_VIDEOID '00003J7Q'	
update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201478600130007'

--159564, 110356, 4, G201478600090012, 00003J7T, 201704120008, 007, S, 225082	done
exec SP_Q_INFO_BY_VIDEOID '00003J7T'	
update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201478600090012'

--159594, 110377, 4, G201660700010008, 00003J88, 201704120026, 007, S, 225125	done
exec SP_Q_INFO_BY_VIDEOID '00003J88'	
update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201660700010008'


--159604, 110387, 4, G201478600210005, 00003J8Q, 201704120044, 007, S, 225150	done
exec SP_Q_INFO_BY_VIDEOID '00003J8Q'	
update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201478600210005'

--159785, 110555, 4, G201362000010003, 00003JGH, 201704140002, 007, S, 225504	done
exec SP_Q_INFO_BY_VIDEOID '00003JGH'	
update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201362000010003'

--159815, 110584, 4, G201451400290014, 00003JGU, 201704140027, 007, S, 225535	done
exec SP_Q_INFO_BY_VIDEOID '00003JGU'	
update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201451400290014'

--159818, 110587, 4, G201451400250009, 00003JGS, 201704140023, 007, S, 225538	done
exec SP_Q_INFO_BY_VIDEOID '00003JGS'	
update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201451400250009'

--159819, 110588, 4, G201451400340009, 00003JHR, 201704140061, 007, S, 225539	done
exec SP_Q_INFO_BY_VIDEOID '00003JHR'	
update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201451400340009'

--002
--159598, 110381, 4, G201685900010004, 00003J8B, 201704120030, 002, S, 225133	
exec SP_Q_INFO_BY_VIDEOID '00003J8B'	
update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201685900010004'
select * from TBPROG_M where FSPROG_ID = '2016859' --台灣經濟發展論壇-與未來國家領導人對談(第一集，2-1)

--159659, 110436, 3, G200928504900024, 00003ITA, 201704070031, 002, S, 225216, 轉到 80 就停，要用帶子重轉上傳
exec SP_Q_INFO_BY_VIDEOID '00003ITA'	--爸媽囧很大 490
update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G200928504900024'

--159900, 110674, 4, G200785201840002, 00003JH5, 201704140040, 002, S, 225774	
exec SP_Q_INFO_BY_VIDEOID '00003JH5'	--ㄤ牯ㄤ牯咕咕咕(詔安腔) 2017-04-30 07
update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G200785201840002'

end


---------------------------------------20170417-----------------------------------
if @case = 201704170936 begin --搬檔失敗
	exec SP_Q_INFO_BY_VIDEOID '00002YBX'
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) between '2017-04-16' and '2017-04-17' order by FDTIME
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) = '2017-04-17' order by FDTIME

	--2017-04-15 13:16:59.590 第一次出現
	select * from TBTRACKINGLOG where CAST(FDTIME as DATE) between '2017-04-14' and '2017-04-17' and FSCATALOG = 'ERROR' and FSMESSAGE like '%Cannot allocate memory%'  order by FDTIME

	select * from VW_PLAYLIST_FROM_NOW order by FDDATE, FSCHANNEL_ID, FSPLAY_TIME

	select * from TBTRACKINGLOG where FDTIME > '2017-04-17 17:10' order by FDTIME

	--17:00 PTSTV -> 75%
	exec SP_Q_INFO_BY_VIDEOID '00003J9S' --T
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201596800020002' --T


end


if @case = 201704171354 begin --播出帶置換到其他格式
	select * from TBPROG_M where FSPGMNAME like '%通靈少女%' --2017724
	select * from TBLOG_VIDEO where FSID = '2017724' and FNEPISODE = 2 and FSARC_TYPE = '002' 
	--G201772400020001, 201702230072, 00003EHT, T
	--G201772400020004, 201704140119, 00003JKM, X
	--G201772400020005, 201704140122, 00003JKP, B
	--G201772400020006, 201704140125, 00003JKS, B  -- 有問題 FSCHANGE_FILE_NO = G201772400020003
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201772400020003' --026, 201704120101, D
	update TBLOG_VIDEO set FCFILE_STATUS = 'D' where FSFILE_NO = 'G201772400020006'

	select * from TBPGM_COMBINE_QUEUE where FDDATE > '2017-04-10' and FSPROG_ID = '2017724'  and FNEPISODE = 2 order by FDDATE
	--重填 G201772400020007, 00003JQM, FSCHANGE_FILE_NO = G201772400020001
	select * from TBUSERS where FSUSER_ID = '60044' --唐翊雯

	exec SP_Q_INFO_BY_VIDEOID '00003JQM' --newest

	--將被置換的其他格式狀態改回 T  看看檢索是否會恢復
	update TBLOG_VIDEO set FCFILE_STATUS = 'T' where FSFILE_NO = 'G201772400020003'
	update TBLOG_VIDEO_D set FCFILE_STATUS = 'T' where FSFILE_NO = 'G201772400020003'
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201772400020003'
end


--------------------------------------------- 20170418--------------------------------------------
if @case = 201704181116 begin
	select * from TBLOG_VIDEO where FSID = '2017689' --G201768900010002, 26 轉檔完成檢索不到
	select * from TBPROG_M where FSPROG_ID = '2017689' --臺中市2017新丁粄節
	select * from TBLOG_VIDEO where FSARC_TYPE = '026' and FCFILE_STATUS = 'T'
end

if @case = 201704181151 begin --週節目表查詢
	select A.PAENO FSPROGID,A.PSDAT FDDATE,A.PSTME FSBTIME,B.FSWEBNAME,A.PBENO FNEPISODE,B.FNTOTEPISODE
        from OPENQUERY(PTS_PROG_MYSQL, 'SELECT  * FROM programX.PSHOW_PTS where PSDAT>=sysdate()
        and PSDAT<= sysdate()+ interval 14 day') A,TBPROG_M B
        where A.PAENO=B.FSPROG_ID order by A.PSDAT,A.PSTME

	select * from TBZCHANNEL --12 PSHOW_PTS

	--先抓週節目表
	select PAENO FSPROGID,PBENO FNEPISODE,PSDAT FDDATE,PSTME FSBTIME,PSMIN FSDURATION,PSKND FNREPLAY,PSKSE FSKSE,PSKLV FSLIVE,PSDEP FSDEP,PSDIR
	from OPENQUERY(PTS_PROG_MYSQL, 'SELECT  * FROM programX.PSHOW_PTS where PSDAT>=sysdate() and PSDAT<= sysdate()+ interval 14 day') A 
	where PSDAT = '2017-04-19' order by A.PSTME


	--根據週節目表查每一筆節目資料
	--0000060, 1974
	select A.FNLENGTH,A.FSPGDNAME,A.FSPGDENAME,B.FSPGMNAME,A.FSMEMO from TBPROG_D A,TBPROG_M B  where A.FSPROG_ID=B.FSPROG_ID and A.FSPROG_ID='0000060'  and A.FnEpisode=1974
	--FBLENGTH = 30, FSPGDNAME = 想做披薩的淇淇哇嚕, FSPGMNAME = 水果冰淇淋

	select FnSeqno,FSprog_ID,FsProg_Name,FNSEQNO,FNDUR,FNSEC,FSSIGNAL,FSMEMO,FSMEMO1 from tbpgm_bank_Detail where fsprog_id='0000060' and FSCHANNEL_ID = '12'

	select * from tbpgm_bank_Detail where FSPROG_ID = '0000072' -- 2017-01-21, 12, FNSEQNO = 43, FSBEG_TIME = 2802
	select * from TBPGM_QUEUE where FSCHANNEL_ID = '12' and FDDATE = '2017-01-21' and FSPROG_ID = '0000072' order by FSBEG_TIME
end

if @case = 201704241819-201704181400 begin --轉檔成功搬檔失敗，直接刪除，但是 ANYSTREAM 狀態已經是失敗要避免重轉
	exec SP_Q_INFO_BY_VIDEOID '00003JKD' --大聲My客風 -G201718100090002

	select '--' + CAST(FNTRANSCODE_ID as varchar(20)) + ', ' + CAST(FNANYSTREAM_ID as varchar(20)) + ', ' + CAST(FNPROCESS_STATUS as varchar(3)) + ', '
	+ FSFILE_NO + ', ' + FSVIDEO_PROG + ', ' + FSBRO_ID + ', ' + FSARC_TYPE + ', ' + FCFILE_STATUS + ', ' + CAST(ISNULL(FNTSMJOB_ID, 0) as varchar(20)) as ITEM, 
	'exec SP_Q_INFO_BY_VIDEOID ' + '''' + FSVIDEO_PROG + '''' as CMD1,
	'update TBLOG_VIDEO set FCFILE_STATUS = ''R'' where FSFILE_NO = ' + ''''+ FSFILE_NO + '''' as CMD2,
	* from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-04-18' and FCFILE_STATUS in ('R', 'S') order by FNTRANSCODE_ID


	--TSM 處理中
	--159997, -1, 1, G201766700110002, 00003JEC, 201704130080, 002, R, 0	, 天黑請閉眼 11 2017-04-24, 62
	exec SP_Q_INFO_BY_VIDEOID '00003JEC'	
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201766700110002'

	--160051, -1, 1, G201718100090003, 00003JZM, 201704180051, 002, R, 0
	exec SP_Q_INFO_BY_VIDEOID '00003JZM'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201718100090003'

	--160074, -1, 1, G201772400060002, 00003J9O, 201704120078, 002, R, 0, 通靈少女, 6, 2017-04-30 62 重審，要轉 mp4, done
	exec SP_Q_INFO_BY_VIDEOID '00003J9O'	
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201772400060002' 
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201772400060002'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201772400060002'

	--------------
	--搬檔失敗
	--159991, 110758, 4, G201554800110013, 00003JZG, 201704180035, 007, S, 226143	
	exec SP_Q_INFO_BY_VIDEOID '00003JZG'	
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201554800110013'

	--159992, 110759, 4, G201554800120009, 00003JZH, 201704180037, 007, S, 226144	
	exec SP_Q_INFO_BY_VIDEOID '00003JZH'	
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201554800120009'

	--160070, 110830, 4, G201777500010001, 00003JDD, 201704130038, 002, S, 226260	
	exec SP_Q_INFO_BY_VIDEOID '00003JDD'	
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201777500010001'

	--160075, 110835, 4, G201766700140001, 00003JGM, 201704140014, 002, S, 226273	
	exec SP_Q_INFO_BY_VIDEOID '00003JGM'	
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201766700140001'


	--轉檔失敗
	--160047, 110810, 8, G200928504950025, 00002F22, 201704180062, 002, R, 226228	
	exec SP_Q_INFO_BY_VIDEOID '00002F22'	
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G200928504950025'

	--160064, 110824, 8, G200928504970023, 00003JZY, 201704180072, 002, R, 226246	
	exec SP_Q_INFO_BY_VIDEOID '00003JZY'	
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G200928504970023'

	--160076, 110836, 8, G200928504990025, 00003K1E, 201704180124, 002, R, 226274	
	exec SP_Q_INFO_BY_VIDEOID '00003K1E'	
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G200928504990025'

	--160077, 110837, 8, G200928505000024, 000038HB, 201704180125, 002, R, 226275	
	exec SP_Q_INFO_BY_VIDEOID '000038HB'	
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G200928505000024'

	--160078, 110838, 8, G200928505010023, 00003K1G, 201704180127, 002, R, 226276	
	exec SP_Q_INFO_BY_VIDEOID '00003K1G'	
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G200928505010023'

	------------------------------------------------------
	--159975, 110742, 4, G201718100090002, 00003JKD, 201704140114, 002, S, 226109
	exec SP_Q_INFO_BY_VIDEOID '00003JKD'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201718100090002'

	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201718100090002'
	update TBLOG_VIDEO set FCFILE_STATUS = 'X' where FSFILE_NO = 'G201718100090002'

	select * from TBTRANSCODE where FNTRANSCODE_ID =  159975 --2017-04-18 09:37:03.183, 110792, 158776
	select * from TBTRANSCODE_RECV_LOG where FNJOB_ID = 110792 --2017-04-18 15:38:37.000 ANystream 有 reschedule ，雖然 stop  但是仍有 notify
	select * from TBTRANSCODE_PARTIAL where FNPARTIAL_ID = 158776
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201718100090003' --2017-04-18 14:17:40.257 create

	--G201718100090002 要作廢
	update TBLOG_VIDEO set FCFILE_STATUS = 'X' where FSFILE_NO = 'G201718100090002'
end

if @case = 201704181613 begin
	exec SP_Q_INFO_BY_VIDEOID '000032R6' --亂紅(2-1), 2017-04-22, 08, 還未到 Approved TSM_RECALL 中
	exec SP_Q_INFO_BY_VIDEOID '00002XM5' --節氣好生活 3, 2017-04-18, 08 節目異動, G201652300030007
end


if @case = 201704181753 begin
	--001WEU 未在搬檔清單
	exec SP_Q_INFO_BY_VIDEOID '00001WEU' --老師，您哪位？(勿排19集), 13, 2017-04-21, 08 G201478600130005
	select * from VW_GET_CHECK_COMPLETE_LIST_MIX_MACRO
end


------------------------------------------20170419-------------------------------------
if @case = 201704190858 begin --亂紅(2-1) 2017-04-22, 08 recall 很慢
	exec SP_Q_INFO_BY_VIDEOID '000032R6' --\\mamstream\MAMDFS\ptstv\HVideo\2016\2016594\G201659400010003.mxf
end

----------------------------------------201704200901---------------------------------------------------------
if @case = 201704200924 begin --刪除客台送帶轉檔單
	select * from TBLOG_VIDEO A left join TBPROG_M B on A.FSID = B.FSPROG_ID  where A.FSCHANNEL_ID = '07' and A.FSARC_TYPE = '002' and A.FCFILE_STATUS = 'B' and LEN(A.FSTRACK) = 4 --91
	select * from TBLOG_VIDEO where FSCHANNEL_ID like '%07%'
	select A.FSFILE_NO, B.FSPGMNAME, A.FNEPISODE from TBLOG_VIDEO A left join TBPROG_M B on A.FSID = B.FSPROG_ID  where A.FSCHANNEL_ID = '07' and A.FSARC_TYPE = '002' and A.FCFILE_STATUS = 'B' 
	and LEN(A.FSTRACK) = 4 --91

	select * from TBLOG_VIDEO_D where FSFILE_NO in 
	(select FSFILE_NO from TBLOG_VIDEO A left join TBPROG_M B on A.FSID = B.FSPROG_ID  where A.FSCHANNEL_ID = '07' and A.FSARC_TYPE = '002' and A.FCFILE_STATUS = 'B' and LEN(A.FSTRACK) = 4)

	--G201666400210001, 2016最夜新聞, 21
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201666400210001' --00002OFO
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002OFO'
end

if @case = 201704201110 begin --分析 TSM recall 時間
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201258200050002' --22.4GB
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201503601880001' --6.5GB
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201379400120007' --10.3GB
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201651300020002' --21.6GB
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201203300010010' --31.5GB
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201620100530002' --21.5GB
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201677100480005' --4.2GB
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201684900100005' --10.4GB
end

if @case = 201704201737 begin --置換錯誤
	exec SP_Q_INFO_BY_VIDEOID '00003IUM' --T, O, G201207802190003
	exec SP_Q_INFO_BY_VIDEOID '00003K6A' --replace G201207802190004, 51322, 201704200064, 2012078, 219
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201207802190004' --公視藝文大道(219).mp4, 026
	select * from TBUSERS where FSUSER_ID = '51322' --林秭瑜
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201207802190004'
	update TBLOG_VIDEO set FCFILE_STATUS = 'T' where FSFILE_NO = 'G201207802190004'
	update TBLOG_VIDEO_D set FCFILE_STATUS = 'T' where FSFILE_NO = 'G201207802190004'
end

---------------------------------------20170421---------------------------------
if @case = 201704210905 begin --審核通過轉檔失敗，因為無檔案
	--新聞是晚上 12:00 播，檔案 20:30 才會到 MarkUpload，XDCAMViewer QC  後直接拿到主控播出
	exec SP_Q_INFO_BY_VIDEOID '00003HWL' --2017宏觀公視國語新聞, 114, 2017-04-24 O, R, 2017-04-20 12:58 審核通過？但是為什麼會有檔案可以審核
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00003WHL' --G201750801140001
	select * from TBUSERS where FSUSER_ID = '51226' --林玉枝
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201750801140001'

	select * from TBTRAN_LOG where CAST(FDDATE as DATE) = '2017-04-20' and  FSPARAMETER like '%003HWL%' order by FDDATE
	--2017/04/20 12:58:20 SP_U_TBLOG_VIDEO_JOBID , FSFILE_NO: G201750801140001 FSJOB_ID: 160186 FSTRANS_FROM: 來源檔案名稱：003HWL FSUPDATED_BY: 51226
	--2017/04/20 12:58:20, SP_U_TBLOG_VIDEOID_MAP, FSID: 2017508 FNEPISODE: 114 FSARC_TYPE: 002 FSVIDEO_ID_PROG: 00003HWL
end

if @case = 201704211116 begin --測試檔用在測試站
	exec SP_Q_INFO_BY_VIDEOID '000030R1' --20160900741, 201609300121
	select * from TBLOG_VIDEO where FSFILE_NO = 'P201609007410001' --01:00:00;00, 01:00:10;00
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'P201609007410001' --01:00:00;00 ~ 01:00:10;00
	select * from TBPGM_PROMO where FSPROMO_ID = '20160900741'
	exec SP_Q_INFO_BY_VIDEOID '00003K8M'
end



if @case = 201704211447-201704141102 begin
	select * from TBLOG_VIDEO where FSID = '2016712' and FNEPISODE = 1 
	--G201671200010001, 201605310020, 00002LIC,X
	--G201671200010002, 201607040315, 00002QON,T
	--G201671200010003, 201607050157, 00002QWJ,X
	exec SP_Q_INFO_BY_VIDEOID '00002QON' --他們在島嶼寫作2-洛夫 2017-04-24, 14
	exec SP_Q_INFO_BY_VIDEOID '00002QWJ'
	exec SP_Q_INFO_BY_VIDEOID '00002LIC'


	--填置換單後舊的置換單還在
	select * from TBLOG_VIDEO where FSID = '2016712' and FNEPISODE = 2
	select * from TBPROG_M where FSPGMNAME like '%他們在島嶼寫作%'
	update TBLOG_VIDEO set FCFILE_STATUS = 'D' where FSFILE_NO = 'G201671200020002' --201607050159
	exec SP_D_TBLOG_VIDEO_BYFILENO 'G201671200020002' --不小心刪到第一支 G201671200020001

	select * from TBLOG_VIDEO where FSID = '2016712' and FNEPISODE = 1
	exec SP_Q_INFO_BY_VIDEOID '00002QOP' --N, 但是舊的審核紀錄仍在 2017-04-25, ch14
	select * from TBLOG_QC_RESULT where FSPROG_ID like '%2016712%' and FNEPISODE = 2 --2016-07-10 審核通過 51272 重填單子仍沿用  00002QOP

	update TBLOG_QC_RESULT set FSVIDEO_ID = '002QOP-D' where FSFILE_NO = 'G201671200020001'
	update TBLOG_QC_RESULT set FSPROG_ID = '2016712-D' where FSFILE_NO = 'G201671200020001'
	update TBLOG_QC_RESULT set FSFILE_NO = 'G201671200020001-D' where ID = 3398
	select * from TBLOG_VIDEO_HD_MC where FSFILE_NO = 'G201671200020001'
	update TBLOG_VIDEO_HD_MC where FSFILE_NO = 'G201671200020001'

	select COUNT(FSVIDEO_ID), FSPROG_ID, FNEPISODE, FSPROG_NAME, FSVIDEO_ID from TBLOG_QC_RESULT group by FSPROG_ID, FNEPISODE, FSPROG_NAME, FSVIDEO_ID having COUNT(FSVIDEO_ID) > 1

	select * from TBLOG_QC_RESULT where FSPROG_ID like '%2017736%' and FNEPISODE = 2
	select * from TBLOG_QC_RESULT where FSPROG_ID = '2017724' and FNEPISODE = 6

	select * from TBLOG_VIDEO_HD_MC where FSFILE_NO = 'G201671200020001' --O --need remove
	select * from TBLOG_VIDEO_HD_MC where FSFILE_NO = 'G201671200020003' --N
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002QOP'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201671200020001'
	select * from TBLOG_QC_RESULT where ID = 3398
	update TBLOG_QC_RESULT set FSVIDEO_ID = '002QOP', FSPROG_ID = '2016712',FSFILE_NO = 'G201671200020001' where ID = 3398
end

if @case = 201704211541 begin --搬檔失敗, done
	select * from TBTRANSCODE where FNTRANSCODE_ID = 159921
	select '--' + CAST(FNTRANSCODE_ID as varchar(20)) + ', ' + CAST(FNANYSTREAM_ID as varchar(20)) + ', ' + CAST(FNPROCESS_STATUS as varchar(3)) + ', '
	+ FSFILE_NO + ', ' + FSVIDEO_PROG + ', ' + FSBRO_ID + ', ' + FSARC_TYPE + ', ' + FCFILE_STATUS + ', ' + CAST(ISNULL(FNTSMJOB_ID, 0) as varchar(20)) as ITEM, 
	'exec SP_Q_INFO_BY_VIDEOID ' + '''' + FSVIDEO_PROG + '''' as CMD1,
	'update TBLOG_VIDEO set FCFILE_STATUS = ''R'' where FSFILE_NO = ' + ''''+ FSFILE_NO + '''' as CMD2,
	* from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) ='2017-04-17' and FCFILE_STATUS in ('R', 'S') order by FNTRANSCODE_ID
	--159921, 110695, 4, G201679300040001, 00002GHF, 201603290080, 002, S, 225795
	exec SP_Q_INFO_BY_VIDEOID '00002GHF'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201679300040001'

	--159922, 110696, 4, G201679300050001, 00002GHG, 201603290081, 002, S, 225796
	exec SP_Q_INFO_BY_VIDEOID '00002GHG'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201679300050001'

	select * from TBPGM_COMBINE_QUEUE where FSPROG_ID = '2016712' and FNEPISODE = 2 --00002QOP
end


if @case = 201704211636 begin --QC 審核通過紀錄
	select * from TBLOG_QC_RESULT
end

--------------------------------20170424--------------------------------
if @case = 201704241057 begin
	select '--' + CAST(FNTRANSCODE_ID as varchar(20)) + ', ' + CAST(FNANYSTREAM_ID as varchar(20)) + ', ' + CAST(FNPROCESS_STATUS as varchar(3)) + ', '
	+ FSFILE_NO + ', ' + FSVIDEO_PROG + ', ' + FSBRO_ID + ', ' + FSARC_TYPE + ', ' + FCFILE_STATUS + ', ' + CAST(ISNULL(FNTSMJOB_ID, 0) as varchar(20)) as ITEM, 
	'exec SP_Q_INFO_BY_VIDEOID ' + '''' + FSVIDEO_PROG + '''' as CMD1,
	'update TBLOG_VIDEO set FCFILE_STATUS = ''R'' where FSFILE_NO = ' + ''''+ FSFILE_NO + '''' as CMD2,
	* from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) = '2017-04-21'  and FCFILE_STATUS in ('R', 'S') order by FNTRANSCODE_ID
	
	select * from TBTRANSCODE where FNTRANSCODE_ID = 160335 --\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700043\D201307000430001.mxf
	select * from TBTRANSCODE where FNTRANSCODE_ID = 160334 --\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700042\D201307000420001.mxf
	select * from TBTRANSCODE where FNTRANSCODE_ID between 160329 and 160335
	--\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700043\D201307000430001.mxf --recall failTapeOffline
	--\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700040\D201307000400001.mxf -- tape offline
	--\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700041\D201307000410001.mxf
	--\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700044\D201307000440001.mxf
	--\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700042\D201307000420001.mxf
	--\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700043\D201307000430001.mxf

	--G000012L4 recall 仍有問題
	select * from TBBOOKING_DETAIL where FSBOOKING_NO = '201704210022'
end



---------------------------------20170425--------------------------------
if @case = 201704251024 begin
	exec SP_Q_INFO_BY_VIDEOID '000005ZO' --爸媽囧很大 319 2017-04-29
end


-----------------------------------20170426---------------------------
if @case = 201704261904 begin
	select * from TBTSM_JOB order by FDREGISTER_TIME desc
	--FSSOURCE_PATH = /ptstv/HVideo/2016/2016690/G201669000010001.mxf
	--FSNOTIFY_ADDR = http://mam.pts.org.tw/WS/WSASC/Handler_TsmNotifyReceiver.ashx

	--227523, /mvtv/HVideo/2016/20160700636/P201607006360001.mxf 調用
	select * from TBTRANSCODE where FNTSMDISPATCH_ID = 227523

	exec SP_Q_TBTRANSCODE_INFO_FNTSMDISPATCH_ID 227523
	select * from TBTRANSCODE order by FDSTART_TIME desc
	select * from TBTRANSCODE_PARTIAL order by FDCREATE_DATE desc


end

-----------------------------20170427---------------------------------
if @case = 201704271351 begin --5/1 macro 未到檔
	exec SP_Q_INFO_BY_VIDEOID '00002EC4' --T O 2017-05-01
	exec SP_Q_INFO_BY_VIDEOID '000034R4' --T,O 2017-05-01
	exec SP_Q_INFO_BY_VIDEOID '000035VY' --T,O 2017-05-01
	exec SP_Q_INFO_BY_VIDEOID '00002F7E' --T,O 2017-05-01
	exec SP_Q_INFO_BY_VIDEOID '00002HSK' --T,O 2017-04-30
end


------------------------------------20170502------------------------------------------
if @case = 201705021046 begin
	exec SP_Q_INFO_BY_VIDEOID '00003JFB' --2017福氣來了 67, G201758100670001
	exec SP_Q_INFO_BY_VIDEOID '00003JFS'
end

if @case = 201705021516 begin
	select * from TBUSERS where FSPASSWD = 'ptsAdmin1234'
	select top 100 * from TBLOG_VIDEO order by FDCREATED_DATE desc
end


----------------------------------20170503---------------------------------------------
if @case = 201705031053 begin 
	select * from TBTRACKINGLOG where FDTIME > '2017-04-20' and FSMESSAGE like '%高解搬檔失敗%' order by FDTIME
	select * from TBTRANSCODE where FNTRANSCODE_ID = 111067
	select * from TBTRANSCODE where FDSTART_TIME > '2017-05-03' order by FDSTART_TIME  --FSANYSTREAM_XML = 傳給 Anystream 的參數
	select * from TBTRANSCODE_RECV_LOG where FNJOB_ID = 111727 --FCCONTENT <node-host>
end

if @case = 201705031358 begin --檔名取錯，標記後未上傳
	exec SP_Q_INFO_BY_VIDEOID '00003JCM' --201704130010, 0513-PTS1-2016金嗓金曲演唱會PROMO, 2017-05-06, 12, P201704003030001
end

if @case = 201705031620 begin --刪除HD送帶轉檔單
	select * from TBLOG_VIDEO where FSID = '2014774' and FNEPISODE = 1 --00001MD3
	exec SP_Q_INFO_BY_VIDEOID '00001MD3' --阿滿姑的雜貨店
	update TBLOG_VIDEO set FCFILE_STATUS = 'X' where FSFILE_NO = 'G201477400010002'

	--new G201477400010003, 00003LEL
end

if @case = 201705040856 begin
	select * from VW_PLAYLIST_FROM_NOW where CAST(FDDATE as DATE) = '2017-05-04' --1880
	select distinct FSVIDEO_ID from VW_PLAYLIST_FROM_NOW where CAST(FDDATE as DATE) = '2017-05-04' --529
	select A.NAME, A.FSVIDEO_ID, B.FSVIDEO_PROG, B.FSFILE_SIZE from VW_PLAYLIST_FROM_NOW A left join TBLOG_VIDEO B on A.FSVIDEO_ID = B.FSVIDEO_PROG where CAST(A.FDDATE as DATE) = '2017-05-04' --1881 ??

	select A.FSVIDEO_ID, B.FSFILE_SIZE from VW_PLAYLIST_FROM_NOW A left join TBLOG_VIDEO B on A.FSVIDEO_ID = B.FSVIDEO_PROG 
	where CAST(A.FDDATE as DATE) = '2017-05-04' 
	group by A.FSVIDEO_ID, B.FSFILE_SIZE
end

if @case = 201705041127 begin --調用卡在 TSM 處理中 帶子放上去後系統自動完成作業
	select * from TBBOOKING_MASTER where FSUSER_ID = '60145'
	select * from TBBOOKING_DETAIL
	select * from TBUSERS where FSUSER = 'prg60145' --60145

	--4 支 tape offline
	select * from TBBOOKING_MASTER A left join TBBOOKING_DETAIL B on A.FSBOOKING_NO = B.FSBOOKING_NO left join TBTRANSCODE C on B.FSJOB_ID = C.FNTRANSCODE_ID
	left join TBTSM_JOB D on C.FNTSMDISPATCH_ID = D.FNTSMJOB_ID
	where A.FDBOOKING_DATE = '2017/05/03' and D.FNSTATUS = 1

	select * from TBBOOKING_MASTER A left join TBBOOKING_DETAIL B on A.FSBOOKING_NO = B.FSBOOKING_NO left join TBTRANSCODE C on B.FSJOB_ID = C.FNTRANSCODE_ID
	left join TBTSM_JOB D on C.FNTSMDISPATCH_ID = D.FNTSMJOB_ID
	where A.FDBOOKING_DATE = '2017/05/04' and D.FNSTATUS = 1
end

if @case = 201705041322 begin --統計 TSM JOB 作業數量
	select * from TBTSM_JOB where CAST(FDREGISTER_TIME as DATE) = '2017-04-18' order by FDREGISTER_TIME --324

	select count(FNTSMJOB_ID), CAST(FDREGISTER_TIME as DATE)from TBTSM_JOB where CAST(FDREGISTER_TIME as DATE) between '2017-04-01' and '2017-05-03' 
	group by CAST(FDREGISTER_TIME as DATE) order by CAST(FDREGISTER_TIME as DATE)
	
	select COUNT(FNTSMJOB_ID)/30 from TBTSM_JOB where CAST(FDREGISTER_TIME as DATE) between '2017-04-01' and '2017-04-30' --163
	select COUNT(FNTSMJOB_ID)/30 from TBTSM_JOB where CAST(FDREGISTER_TIME as DATE) between '2017-03-01' and '2017-03-30' --186
end

if @case = 201705041556 begin --置換單要抽單無法抽單
	exec SP_Q_INFO_BY_VIDEOID '00003LA7' --一字千金 2017-05-04 被置換 2015404, 79
	exec SP_Q_INFO_BY_VIDEOID '00003LHC' --新的單子有問題 沒有段落資料 需刪除 
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G201540400790005'
	--update TBLOG_VIDEO set FCFILE_STATUS = 'X' where FSFILE_NO = 'G201540400790005'
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G201540400790004'
	select * from TBLOG_VIDEO where FSID = '2015404' and FNEPISODE = 79 and FSARC_TYPE = '002' --new one G201540400790009, 00003LIB, 201705040118
	-- 需要標記上傳
	exec SP_Q_INFO_BY_VIDEOID '00003LIB' --2017-05-06 12
end

if @case = 201705041616 begin  --QUE
	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20170300007'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20170200568'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20170200529'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20170200525'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20170200523'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20170200522'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20170200445'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME
end


if @case = 201705041824 begin
	select * from TBTRANSCODE where CAST(FDSTART_TIME as DATE) between '2017-05-01' and '2017-05-04' order by FDSTART_TIME desc
	select FSNOTIFY_ADDRESS from TBTRANSCODE where CAST(FDSTART_TIME as DATE) between '2017-01-01' and '2017-05-04' group by FSNOTIFY_ADDRESS
	select * from TBLOG_VIDEO where FSARC_TYPE = '002' and LEN(FSTRACK) = 4 and FCFILE_STATUS = 'T' and FDUPDATED_DATE between '2017-01-01' and '2017-05-04'
	select * from TBLOG_VIDEO A join TBTRANSCODE B on A.FSJOB_ID = B.FNTRANSCODE_ID where FSARC_TYPE = '002' and LEN(A.FSTRACK) = 4 and A.FCFILE_STATUS = 'T' and B.FDSTART_TIME between '2017-01-01' and '2017-05-04'
end

if @case = 201705051422 begin
	select FSVIDEO_ID from VW_GET_PLAY_VIDEOID_LIST_MACRO
	select * from VW_GET_CHECK_COMPLETE_LIST_MIX_MACRO
end

if @case = 201705051525 begin
	exec SP_Q_INFO_BY_VIDEOID '00003LFB' --暴雨將至 2017-05-06 12
end

if @case = 201705070717 begin
	exec SP_Q_INFO_BY_VIDEOID '00003EJ6'
	exec SP_Q_INFO_BY_VIDEOID '00003EJE' --5/11
	exec SP_Q_INFO_BY_VIDEOID '00003JZ2'
	exec SP_Q_INFO_BY_VIDEOID '00003JZ4'
	exec SP_Q_INFO_BY_VIDEOID '00003K5T'
	exec SP_Q_INFO_BY_VIDEOID '00003KSX'
	exec SP_Q_INFO_BY_VIDEOID '00003KSY'
	exec SP_Q_INFO_BY_VIDEOID '00003KSZ'
	exec SP_Q_INFO_BY_VIDEOID '00003L88'
	exec SP_Q_INFO_BY_VIDEOID '00001OYH'
	exec SP_Q_INFO_BY_VIDEOID '00003EIH'
	exec SP_Q_INFO_BY_VIDEOID '00003EJF'
	exec SP_Q_INFO_BY_VIDEOID '00003G3C' --Next on TAIWAN IN FOCUS 2017-05-12 08
	exec SP_Q_INFO_BY_VIDEOID '00002YRS'
	exec SP_Q_INFO_BY_VIDEOID '00001OYI'
	exec SP_Q_INFO_BY_VIDEOID '00002K9O'


	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00003G3C' and FSCHANNEL_ID = '08' order by FDDATE
end

if @case = 201705081105 begin --調用失敗 ANS 註冊錯誤
	select * from TBUSERS where FSUSER = 'prg1159' --龔美靜
	select * from TBTRANSCODE where FNTRANSCODE_ID = 161073 --MAM-調用清單編號： 201705030001，prg1159調用
	select * from TBBOOKING_DETAIL where FSBOOKING_NO = '201705030001' --G201719400050001
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201719400050001' --\\mamstream\MAMDFS\ptstv\HVideo\2017\2017194\G201719400050001.mxf
end

if @case = 201705081655 begin  --入庫檔案無法上傳
	select top 1000 * from TBLOG_DOC order by FDCREATED_DATE desc
	select top 1000 * from TBLOG_PHOTO order by FDCREATED_DATE desc
	select top 1000 * from TBLOG_AUDIO order by FDCREATED_DATE desc
	select * from TBTRACKINGLOG where FDTIME > '2017-05-08 16:40' order by FDTIME
end

if @case = 201705090614 begin
	exec SP_Q_INFO_BY_VIDEOID '00003INP'  --文宣-穆斯林友善旅遊 2017-05-09 why in purge list
	exec SP_Q_INFO_BY_VIDEOID '00003F3E'  --循環帶-平溪天燈 2017-05-09
	exec SP_Q_INFO_BY_VIDEOID ''
	exec SP_Q_INFO_BY_VIDEOID ''
end

if @case = 201705091044 begin
	select * from TBTRACKINGLOG where FDTIME > '2017-05-09 08:00' order by FDTIME
end

if @case = 201705091356 begin
	exec SP_Q_INFO_BY_VIDEOID '000021Z7' --摩鐵路之城(白天不宜) 沒有審核紀錄 2017-05-22 08
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '000021Z7'
end

if @case = 201705091523 begin  --宏觀刪檔問題
	exec SP_Q_INFO_BY_VIDEOID '00003LHY'  --5/9宏觀 menu1430 2017-05-09
end

if @case = 201705091523 begin
	select * from TBUSERS where FSUSER_ChtName = '洪于婷' --remember23, 51342
	select * from TBBOOKING_MASTER where FSUSER_ID = '51342'
	select * from TBBOOKING_MASTER A join TBBOOKING_DETAIL B on A.FSBOOKING_NO = B.FSBOOKING_NO where A.FSUSER_ID = '51342' order by FDBOOKING_DATE
	select * from TBLOG_VIDEO where FSJOB_ID = '161552'
	select * from TBTRANSCODE where FNTRANSCODE_ID = 161548 --\\mamstream\MAMDFS\ptstv\HVideo\2016\2016205\G201620500010002.mxf?\\hdsdownload\MAMDownload\remember23\201705050118\G201620500010002_002.mxf?01:00:00:00?02:06:00:02
	select * from TBBOOKING_MASTER A join TBBOOKING_DETAIL B on A.FSBOOKING_NO = B.FSBOOKING_NO left join TBTRANSCODE C on A.FCCHECK_STATUS = 'Y' and B.FSJOB_ID = C.FNTRANSCODE_ID
	where A.FSUSER_ID = '51342' order by FDBOOKING_DATE

	select * from TBBOOKING_MASTER A join TBBOOKING_DETAIL B on A.FSBOOKING_NO = B.FSBOOKING_NO left join TBTRANSCODE C on A.FCCHECK_STATUS = 'Y'
	where A.FSUSER_ID = '51342' order by FDBOOKING_DATE

	select * from TBBOOKING_MASTER A join TBBOOKING_DETAIL B on A.FSBOOKING_NO = B.FSBOOKING_NO and A.FCCHECK_STATUS = 'Y' and B.FSJOB_ID is not NULL and B.FCFILE_TRANSCODE_STATUS = 'F'
	where A.FSUSER_ID = '51342' order by FDBOOKING_DATE  --6

	--查詢 user 調用失敗紀錄
	select * from TBBOOKING_MASTER A join TBBOOKING_DETAIL B on A.FSBOOKING_NO = B.FSBOOKING_NO and A.FCCHECK_STATUS = 'Y' and B.FSJOB_ID is not NULL and B.FCFILE_TRANSCODE_STATUS = 'F'
	join TBLOG_VIDEO C on B.FSFILE_NO = C.FSFILE_NO
	where A.FSUSER_ID = '51342' order by FDBOOKING_DATE

	--tapeonly \\mamstream\MAMDFS\ptstv\HVideo\2016\2016595\G201659500010002.mxf
	--tapeonly \\mamstream\MAMDFS\ptstv\HVideo\2016\2016963\G201696300010016.mxf
	--tapeonly \\mamstream\MAMDFS\ptstv\HVideo\2016\2016206\G201620600010003.mxf
end

if @case = 201705091646 begin
	exec SP_Q_INFO_BY_VIDEOID '000039BQ' --2016PTS1 RM 唐朝小栗子 2017-05-12 12, T, O
	exec SP_Q_INFO_BY_VIDEOID '000035VZ'
	exec SP_Q_INFO_BY_VIDEOID '00003LHU'
	exec SP_Q_INFO_BY_VIDEOID '00003KSM'
	exec SP_Q_INFO_BY_VIDEOID '00003J8S'
	exec SP_Q_INFO_BY_VIDEOID '00003G0E'
	exec SP_Q_INFO_BY_VIDEOID '00003EKH'
	exec SP_Q_INFO_BY_VIDEOID '00003LI0'
end

-----------------------------------------20170510-------------------------------------
if @case = 201705101226 begin
	exec SP_Q_INFO_BY_VIDEOID '00003LVA' --劃破天際看世界-11-14, 2017-05-12 12, 201705090130
end

if @case = 201705101521 begin
	select * from TBLOG_VIDEO where FSID = '2006786' and FNEPISODE = 55
	select * from TBPROG_M where FSPROG_ID = '2006786' --流言追追追 有字幕置換後變無字幕
end

if @case = 201705110953 begin --MOD EPG
	exec SP_Q_TBPGM_PROG_EPG '62' --PTS1-MOD	62	PSHOW_PTS_MOD	PTS_MOD	13.txt

	--TeansProgList.ExportModProglist() 從排檔系統取得節目表資料 沒有結束時間
	select A.PAENO FSPROGID,A.PSDAT FDDATE,A.PSTME FSBTIME,B.FSWEBNAME,A.PBENO FNEPISODE,B.FNTOTEPISODE
	from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PSHOW_PTS_MOD  where PSDAT>=sysdate() and PSDAT<= sysdate()+ interval 14 day') A,TBPROG_M B 
	where A.PAENO=B.FSPROG_ID order by A.PSDAT,A.PSTME

	select *
	from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PSHOW_PTS_MOD  where PSDAT>=sysdate() and PSDAT<= sysdate()+ interval 14 day') A,TBPROG_M B 
	where A.PAENO=B.FSPROG_ID order by A.PSDAT,A.PSTME


	 --查詢排檔名稱, ex : 人生劇展, 迷你影集
     select distinct CHEID from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA') -- DIMO, PTS, PTS_MOD, HD_MOD, HAKA, HD ...
     select top 1000 * from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA')
     select top 1000 * from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA WHERE CHEID = "DIMO"')
     select top 1000 * from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA WHERE CHEID = "HD_MOD" and PRSDT <= "2017-05-11" and PREDT >= "2017-05-11" and PRTMS <= "23:00" and PRTME >= "23:00" and PRCB4 = 1 order by PRENO DESC')
     select * from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA WHERE CHEID = "PTS" and PRNAM like "%人生劇展%" and PRSDT')

	 select PRNAM from OPENQUERY(PTS_PROG_MYSQL, 'SELECT PRNAM FROM programX.PAREA WHERE CHEID = "PTS_MOD"')
	 --got error  無法從連結伺服器 "PTS_PROG_MYSQL" 的 OLE DB 提供者 "MSDASQL" 取得資料行資訊。
	 select top 100 * 
	 from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA WHERE CHEID = ''PTS_MOD'' and PRSTD <= "2016-09-26" and PRETD >= "2016-09-26" and PRTMS <= "06:00" and PRTME >= "06:00" and PRCB1 = 1 order by PRENO DESC')

	 --13595	公視藝文特區	2017-05-01	2017-05-31	19:00	19:10	0	1	1	1	1	1	0	HD_MOD
	 select top 100 * 
	 from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA WHERE CHEID = "HD_MOD" and PRSDT <= "2017-05-08" and PREDT >= "2017-05-08" and PRTMS <= "19:00" and PRTME >= "19:10" and PRCB1 = 1 order by PRENO DESC')

	 --13605	阿賈克斯VS哥本哈根	2017-05-08	2017-05-08	23:00	23:10	0	1	0	0	0	0	0	HD_MOD
	 select top 100 * 
	 from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA WHERE CHEID = "HD_MOD" and PRSDT <= "2017-05-08" and PREDT >= "2017-05-08" and PRTMS <= "23:00" and PRTME >= "23:10" and PRCB1 = 1 order by PRENO DESC')

	 --13596	公視藝文特區	2017-05-01	2017-05-31	25:00	25:10	0	1	1	1	1	1	0	HD_MOD
	 select top 100 * 
	 from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA WHERE CHEID = "HD_MOD" and PRSDT <= "2017-05-08" and PREDT >= "2017-05-08" and PRTMS <= "25:00" and PRTME >= "25:00" and PRCB1 = 1 order by PRENO DESC')

	 --5/8 有 4  筆有檔期名稱
	 select top 100 * 
	 from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA WHERE CHEID = "HD_MOD" and PRSDT <= "2017-05-08" and PREDT >= "2017-05-08" order by PRENO DESC')

	 select FSPROG_ID ID from TBPROG_M union select FSPROMO_ID ID from TBPGM_PROMO

	 select FSWEBNAME +  '(' + CAST(FNEPISODE as varchar(10)) + ')' + ',' + CAST(FDDATE as varchar(20)) + ',' + FSBTIME from VW_GET_PSHOW_PTS_MOD order by FDDATE, FSBTIME

	 select * from VW_GET_PSHOW_PTS_MOD

end

--------------------------------------------20170511---------------------------------------------
if @case = 201705111641 begin
	select * from TBBROADCAST where FSBRO_ID = '201704110086' --no data
	select * from TBBROADCAST where FSBRO_ID = '201705080122' --no data
	select * from TBBROADCAST where FSBRO_ID = '201705080107' --no data
	select * from TBBROADCAST where FSBRO_ID = '201705110089'
	select * from TBBROADCAST where FSBRO_ID = '201704270125'

	select * from TBLOG_PHOTO where FSARC_ID = '201704270125'
	select * from TBLOG_DOC where FSARC_ID = '201704270125'

	select * from (
	select FSARC_ID, FSFILE_NO, FSTYPE, FSID, FNEPISODE, FCFILE_STATUS, FSSUPERVISOR, FSARC_TYPE, FSFILE_TYPE, FSTITLE, FSOLD_FILE_NAME from TBLOG_PHOTO 
	UNION ALL
	select FSARC_ID, FSFILE_NO, FSTYPE, FSID, FNEPISODE, FCFILE_STATUS, FSSUPERVISOR, FSARC_TYPE, FSFILE_TYPE, FSTITLE, FSOLD_FILE_NAME from TBLOG_DOC
	UNION ALL
	select FSARC_ID, FSFILE_NO, FSTYPE, FSID, FNEPISODE, FCFILE_STATUS, FSSUPERVISOR, FSARC_TYPE, FSFILE_TYPE, FSTITLE, FSOLD_FILE_NAME from TBLOG_AUDIO
	) A
	where FSARC_ID = '201704110086'

	select * from TBLOG_PHOTO where FSARC_ID = '201704110086' --51189, 2014011, 22
	select * from TBUSERS where FSUSER_ID = '51189' --王紫嫣
	select * from TBPROG_M where FSPROG_ID = '2014011' --勝利催落去

	select * from TBBROADCAST where FSBRO_ID = '201704110086' --no data
	select * from TBARCHIVE where FSARC_ID = '201704110086'
	select * from TBARCHIVE where FSARC_ID = '201705110089'

	select * from TBLOG_PHOTO where FSFILE_NO = 'G000015903890005'

	select * from TBFLOW_MAIL
end

------------------------------------------------------20170512----------------------------------------------
if @case = 201705120931 begin  --201705090116 調用的檔案，轉檔完成都未下來
	select A.FSBOOKING_NO, A.FSUSER_ID, A.FDBOOKING_DATE, A.FCCHECK_STATUS, B.FSJOB_ID, B.FCFILE_TRANSCODE_STATUS, C.FSID, C.FNEPISODE, C.FSFILE_PATH_H 
	from TBBOOKING_MASTER A join TBBOOKING_DETAIL B on A.FSBOOKING_NO = B.FSBOOKING_NO and A.FCCHECK_STATUS = 'Y' and B.FSJOB_ID is not NULL and B.FCFILE_TRANSCODE_STATUS = 'F'
	join TBLOG_VIDEO C on B.FSFILE_NO = C.FSFILE_NO
	where A.FSUSER_ID = '08303' order by FDBOOKING_DATE

	select A.FSBOOKING_NO, A.FSUSER_ID, A.FDBOOKING_DATE, A.FCCHECK_STATUS, B.FSJOB_ID, B.FCFILE_TRANSCODE_STATUS, C.FSID, C.FNEPISODE, C.FSFILE_PATH_H 
	from TBBOOKING_MASTER A join TBBOOKING_DETAIL B on A.FSBOOKING_NO = B.FSBOOKING_NO and A.FCCHECK_STATUS = 'Y' and B.FSJOB_ID is not NULL
	join TBLOG_VIDEO C on B.FSFILE_NO = C.FSFILE_NO
	where A.FSUSER_ID = '08303' and A.FSBOOKING_NO = '201705090116' order by FDBOOKING_DATE

	select A.FSBOOKING_NO, A.FSUSER_ID, A.FDBOOKING_DATE, A.FCCHECK_STATUS, B.FSJOB_ID, B.FCFILE_TRANSCODE_STATUS, C.FSFILE_NO ,C.FSID, D.NAME, C.FNEPISODE, C.FSFILE_PATH_H 
	from TBBOOKING_MASTER A join TBBOOKING_DETAIL B on A.FSBOOKING_NO = B.FSBOOKING_NO and B.FSJOB_ID is not NULL
	join TBLOG_VIDEO C on B.FSFILE_NO = C.FSFILE_NO
	left join (select FSPROG_ID FSID, FSPGMNAME NAME from TBPROG_M UNION ALL (select FSPROMO_ID FSID, FSPROMO_NAME NAME from TBPGM_PROMO)) D on D.FSID = C.FSID
	where A.FSBOOKING_NO = '201705090116' order by FDBOOKING_DATE

	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201705090116'

	select * from TBBOOKING_DETAIL where FSBOOKING_NO = '201705090116'
	select * from TBUSERS where FSUSER_ChtName = '龔美靜' --08303, prg1159

	exec SP_Q_GET_BOOKING_FILE_INTERNATION_BY_BOOKINGNO '201705090116'

	--查詢 user 調用失敗紀錄
	select A.FSBOOKING_NO, A.FSUSER_ID, A.FDBOOKING_DATE, A.FCCHECK_STATUS, B.FSJOB_ID, B.FCFILE_TRANSCODE_STATUS, C.FSFILE_NO, C.FSID, D.NAME, C.FNEPISODE, C.FSFILE_PATH_H from TBBOOKING_MASTER A 
	join TBBOOKING_DETAIL B on A.FSBOOKING_NO = B.FSBOOKING_NO and A.FCCHECK_STATUS = 'Y' and B.FSJOB_ID is not NULL
	join TBLOG_VIDEO C on B.FSFILE_NO = C.FSFILE_NO
	left join (select FSPROG_ID FSID, FSPGMNAME NAME from TBPROG_M UNION ALL (select FSPROMO_ID FSID, FSPROMO_NAME NAME from TBPGM_PROMO)) D on C.FSID = D.FSID
	where A.FSUSER_ID = '51342' order by FDBOOKING_DATE

	exec SP_Q_GET_BOOKING_FILE_BY_USERID '51342', 'A'
end

if @case = 201705121206 begin --根據 Transcode ID 查Anystream 轉檔狀態
	exec SP_Q_TBTRANSCODE_ONEJOB 161984  --FSCONTENT: Anystream 回傳, FSANYSTREAM_XML:傳給 anystream 參數
end

if @case = 201705121435 begin

	--感恩基金會公益短片-感恩篇
	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20150100635'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	--感恩基金會公益短片-古厝篇
	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20150100634'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME
end

if @case = 201705151023 begin
	exec SP_Q_INFO_BY_VIDEOID '00002LQU' --獨立特派員 449 FCSTATUS = '' , 手動更新標記
end

if @case = 201705151207 begin --檔案置換後，尚未入庫，無法調用
	select * from TBPROG_M where FSPGMNAME like '%柏林%' --2017039 柏林愛樂在台北
	select * from TBLOG_VIDEO where FSID = '2017039' and FNEPISODE = 1
end

if @case = 201705151500 begin
	select * from TBTRANSCODE where FNTRANSCODE_ID = 158560 --20170300856
	select * from TBLOG_VIDEO where FSFILE_NO = 'P201703008560001' --no record
	select * from TBLOG_VIDEO where FSID = '20170300856' --P201703008560002  新建
end

if @case = 201705171418 begin
	exec SP_Q_INFO_BY_VIDEOID '00003LVR' --獨立特派員, 2017-05-17, 13, T  無低解
	--低解檔案無法播放
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G200721704950001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G200721704950001'
end

if @case = 201705161448 begin
	select * from TBZPROGGRADE
	INSERT INTO TBZPROGGRADE(FSPROGGRADEID, FSPROGGRADENAME, FDCREATED_DATE, FSSORT, FSUPDATED_BY, FDUPDATED_DATE, FBISENABLE)
	VALUES('06', '輔12：有警語且16:00-21:00不可播', '04', CAST('2017-05-16' as DATETIME), '51204', CAST('2017-05-16' as DATETIME), 1)

	UPDATE TBZPROGGRADE set FSPROGGRADENAME = '護：有警語且 16:00-19:00不可播', FDUPDATED_DATE = GETDATE(), FSUPDATED_BY = '51204' where FSPROGGRADEID = '03'
	UPDATE TBZPROGGRADE set FSSORT = '05', FDUPDATED_DATE = GETDATE(), FSUPDATED_BY = '51204' where FSPROGGRADEID = '04'
	UPDATE TBZPROGGRADE set FSSORT = '06', FDUPDATED_DATE = GETDATE(), FSUPDATED_BY = '51204' where FSPROGGRADEID = '05' 
	 
	select GETDATE()

	--給節目管理系統查詢
			select A.FSPROG_ID, A.FSWEBNAME, A.FSPGMNAME, A.FNLENGTH,B.FSPROGSRCNAME, A.FNTOTEPISODE, C.FSPROGLANGNAME as FSPROGLANGNAME1, D.FSPROGLANGNAME as FSPROGLANGNAME2, E.FSPROGGRADENAME, E.FSPROGGRADEID
			from TBPROG_M A left join TBZPROGSRC B on A.FSPROGSRCID = B.FSPROGSRCID
			left join TBZPROGLANG C on A.FSPROGLANGID1 = C.FSPROGLANGID
			left join TBZPROGLANG D on A.FSPROGLANGID2 = D.FSPROGLANGID
			left join TBZPROGGRADE E on A.FSPROGGRADEID = E.FSPROGGRADEID

			select * from TBPROG_M


			SELECT   A.FSPROG_ID, A.FNEPISODE, A.FSPGDNAME, B.FSPROGSRCNAME, C.FSPROGLANGNAME AS FSPROGLANGNAME1, 
                     D.FSPROGLANGNAME AS FSPROGLANGNAME2, A.FNLENGTH, A.FSCONTENT, E.FSPROGGRADENAME, E.FSPROGGRADEID
			FROM   dbo.TBPROG_D AS A LEFT OUTER JOIN
                   dbo.TBZPROGSRC AS B ON A.FSPROGSRCID = B.FSPROGSRCID LEFT OUTER JOIN
                   dbo.TBZPROGLANG AS C ON A.FSPROGLANGID1 = C.FSPROGLANGID LEFT OUTER JOIN
                   dbo.TBZPROGLANG AS D ON A.FSPROGLANGID2 = D.FSPROGLANGID
				   left join TBZPROGGRADE E on E.FSPROGGRADEID = A.FSPROGGRADEID
end


if @case = 201705171233 begin --查詢公益託播
	select A.FSPROMO_ID 短帶編號, B.FSWELFARE 公益編號, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間, B.FSPROMO_NAME 短帶名稱 from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
        join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where B.FSMAIN_CHANNEL_ID = '07' and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') and B.FSWELFARE <> ''
        and A.FDDATE between '2017-01-01' and '2017-04-30' order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME --2583

	exec SP_Q_WELFAREPROMO_PLAYOUT_BY_DATE '07', '2017-01-01', '2017-04-30', '1'
    exec SP_Q_WELFAREPROMO_PLAY_COUNT_BY_DATE '07', '2017-01-01', '2017-04-30'

end

------------------------------------------20170518--------------------------------
if @case = 201705180904 begin
    select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20170300320'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	EXEC SP_Q_TBPGM_PROMO_PLAYLIST '20170300320'
	EXEC SP_Q_TBPGM_PROMO_PLAYLIST '20170300319'
	EXEC SP_Q_TBPGM_PROMO_PLAYLIST '20170400285'
	EXEC SP_Q_TBPGM_PROMO_PLAYLIST '20170400284'
	EXEC SP_Q_TBPGM_PROMO_PLAYLIST '20170400099'
	EXEC SP_Q_TBPGM_PROMO_PLAYLIST '20170400098'
	EXEC SP_Q_TBPGM_PROMO_PLAYLIST '20170300868'
	EXEC SP_Q_TBPGM_PROMO_PLAYLIST '20170300865'
	EXEC SP_Q_TBPGM_PROMO_PLAYLIST '20170300584'
	EXEC SP_Q_TBPGM_PROMO_PLAYLIST '20170300583'
	EXEC SP_Q_TBPGM_PROMO_PLAYLIST '20170300471'
	EXEC SP_Q_TBPGM_PROMO_PLAYLIST '20170300470'

	select * from TBPGM_COMBINE_QUEUE
	select * from VW_PLAYLIST_FROM_NOW where FDDATE = '2017-05-19' and FSCHANNEL_ID = '07' order by FSPLAY_TIME
	select FSPROG_ID, FSPROG_NAME, FDDATE, FSBEG_TIME from TBPGM_QUEUE where FDDATE = '2017-05-19' and FSCHANNEL_ID = '07' order by FSBEG_TIME
	select FSPROG_ID '節目編號', FSPROG_NAME 節目名稱, FNEPISODE 集數, FDDATE 播出日期, FSBEG_TIME 播出時間, B.FSSHOWNAME 播出頻道 from TBPGM_QUEUE A join TBZCHANNEL B on A.FSCHANNEL_ID = B.FSCHANNEL_ID where FSPROG_ID = '2016904' order by FDDATE, A.FSCHANNEL_ID,FSBEG_TIME

	select * from TBZCHANNEL

end

if @case = 201705181218 begin --主控播出資訊
	select * from TBZPROGGRADE
	INSERT INTO TBZPROGGRADE(FSPROGGRADEID, FSPROGGRADENAME, FDCREATED_DATE, FSSORT, FSUPDATED_BY, FDUPDATED_DATE, FBISENABLE)
	VALUES('06', '輔12：有警語且16:00-21:00不可播', '04', CAST('2017-05-16' as DATETIME), '51204', CAST('2017-05-16' as DATETIME), 1)

	UPDATE TBZPROGGRADE set FSPROGGRADENAME = '護：16:00-19:00不可播', FDUPDATED_DATE = GETDATE(), FSUPDATED_BY = '51204' where FSPROGGRADEID = '03'
	UPDATE TBZPROGGRADE set FSPROGGRADENAME = '輔12：06:00-21:00不可播', FSSORT='04', FDUPDATED_DATE = GETDATE(), FSUPDATED_BY = '51204' where FSPROGGRADEID = '06'
	UPDATE TBZPROGGRADE set FSPROGGRADENAME = '輔15：06:00-23:00不可播', FSSORT='05', FDUPDATED_DATE = GETDATE(), FSUPDATED_BY = '51204' where FSPROGGRADEID = '04'

	UPDATE TBZPROGGRADE set FSSORT = '05', FDUPDATED_DATE = GETDATE(), FSUPDATED_BY = '51204' where FSPROGGRADEID = '04'
	UPDATE TBZPROGGRADE set FSSORT = '06', FDUPDATED_DATE = GETDATE(), FSUPDATED_BY = '51204' where FSPROGGRADEID = '05' 

	exec SP_Q_TBZPROGLANG_ALL
	exec SP_Q_TBZPROGGRADE  --取得分級資料 ID, NAME
	exec SP_Q_TBPROG_D  '2013232', 1  --FSPROGGRADEID = 05 分級 ID

	select * from TBPGM_INSERT_TAPE --並無分級資料

	--test 2013232 #17
	select * from TBPROG_M where FSPROG_ID = '2013232' --FSPROGGRADEID = 05
	select * from TBPROG_D where FSPROG_ID = '2013232' and FNEPISODE = 17 --FSPROGGRADEID = 03  --> 03
	select * from TBPROG_D where FSPROG_ID = '2013232' and FNEPISODE = 18 --FSPROGGRADEID = 03
end




if @case = 201705191404-201705110953 begin --MOD EPG
	exec SP_Q_TBPGM_PROG_EPG '62' --PTS1-MOD	62	PSHOW_PTS_MOD	PTS_MOD	13.txt

	--TeansProgList.ExportModProglist() 從排檔系統取得節目表資料 沒有結束時間
	select A.PAENO FSPROGID,A.PSDAT FDDATE,A.PSTME FSBTIME,B.FSWEBNAME,A.PBENO FNEPISODE,B.FNTOTEPISODE
	from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PSHOW_PTS_MOD  where PSDAT>=sysdate() and PSDAT<= sysdate()+ interval 14 day') A,TBPROG_M B 
	where A.PAENO=B.FSPROG_ID order by A.PSDAT,A.PSTME

	select *
	from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PSHOW_PTS_MOD  where PSDAT>=sysdate() and PSDAT<= sysdate()+ interval 14 day') A,TBPROG_M B 
	where A.PAENO=B.FSPROG_ID order by A.PSDAT,A.PSTME


	 --查詢排檔名稱, ex : 人生劇展, 迷你影集
     select distinct CHEID from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA') -- DIMO, PTS, PTS_MOD, HD_MOD, HAKA, HD ...
     select top 1000 * from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA')
     select top 1000 * from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA WHERE CHEID = "DIMO"')
     select top 1000 * from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA WHERE CHEID = "HD_MOD" and PRSDT <= "2017-05-11" and PREDT >= "2017-05-11" and PRTMS <= "23:00" and PRTME >= "23:00" and PRCB4 = 1 order by PRENO DESC')
     select * from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA WHERE CHEID = "PTS" and PRNAM like "%人生劇展%" and PRSDT')

	 select PRNAM from OPENQUERY(PTS_PROG_MYSQL, 'SELECT PRNAM FROM programX.PAREA WHERE CHEID = "PTS_MOD"')
	 --got error  無法從連結伺服器 "PTS_PROG_MYSQL" 的 OLE DB 提供者 "MSDASQL" 取得資料行資訊。
	 select top 100 * 
	 from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA WHERE CHEID = ''PTS_MOD'' and PRSTD <= "2016-09-26" and PRETD >= "2016-09-26" and PRTMS <= "06:00" and PRTME >= "06:00" and PRCB1 = 1 order by PRENO DESC')

	 --13595	公視藝文特區	2017-05-01	2017-05-31	19:00	19:10	0	1	1	1	1	1	0	HD_MOD
	 select top 100 * 
	 from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA WHERE CHEID = "HD_MOD" and PRSDT <= "2017-05-08" and PREDT >= "2017-05-08" and PRTMS <= "19:00" and PRTME >= "19:10" and PRCB1 = 1 order by PRENO DESC')

	 --13605	阿賈克斯VS哥本哈根	2017-05-08	2017-05-08	23:00	23:10	0	1	0	0	0	0	0	HD_MOD
	 select top 100 * 
	 from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA WHERE CHEID = "HD_MOD" and PRSDT <= "2017-05-08" and PREDT >= "2017-05-08" and PRTMS <= "23:00" and PRTME >= "23:10" and PRCB1 = 1 order by PRENO DESC')

	 --13596	公視藝文特區	2017-05-01	2017-05-31	25:00	25:10	0	1	1	1	1	1	0	HD_MOD
	 select top 100 * 
	 from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA WHERE CHEID = "HD_MOD" and PRSDT <= "2017-05-08" and PREDT >= "2017-05-08" and PRTMS <= "25:00" and PRTME >= "25:00" and PRCB1 = 1 order by PRENO DESC')

	 --5/8 有 4  筆有檔期名稱
	 select top 100 * 
	 from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PAREA WHERE CHEID = "HD_MOD" and PRSDT <= "2017-05-08" and PREDT >= "2017-05-08" order by PRENO DESC')

	 select FSPROG_ID ID from TBPROG_M union select FSPROMO_ID ID from TBPGM_PROMO

	 select FSWEBNAME +  '(' + CAST(FNEPISODE as varchar(10)) + ')' + ',' + CAST(FDDATE as varchar(20)) + ',' + FSBTIME from VW_GET_PSHOW_PTS_MOD order by FDDATE, FSBTIME

	 select * from VW_GET_PSHOW_PTS_MOD

	--加入分級資訊
	select A.PAENO FSPROGID,A.PSDAT FDDATE,A.PSTME FSBTIME,B.FSWEBNAME,A.PBENO FNEPISODE,B.FNTOTEPISODE, C.FSPROGGRADEID, C.FSPROGGRADE
	from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PSHOW_PTS_MOD  where PSDAT>=sysdate() and PSDAT<= sysdate()+ interval 14 day') A 
	join TBPROG_M B on  A.PAENO=B.FSPROG_ID
	join TBPROG_D C on C.FSPROG_ID = A.PAENO and A.PBENO = C.FNEPISODE
	order by A.PSDAT,A.PSTME --385

	--2017811	2017-05-23	16:58	公視新聞報	ENEPISODE = 0	400	NULL	NULL
	select A.PAENO FSPROGID,A.PSDAT FDDATE,A.PSTME FSBTIME,B.FSWEBNAME,A.PBENO FNEPISODE,B.FNTOTEPISODE, ISNULL(D.FSPROGGRADENAME, '普') FSPROGGRADENAME , ISNULL(C.FSPROGGRADE, '') FSPROGRADE
	from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PSHOW_PTS_MOD  where PSDAT>=sysdate() and PSDAT<= sysdate()+ interval 30 day') A 
	join TBPROG_M B on  A.PAENO=B.FSPROG_ID
	left join TBPROG_D C on C.FSPROG_ID = A.PAENO and A.PBENO = C.FNEPISODE
	left join TBZPROGGRADE D on C.FSPROGGRADEID = D.FSPROGGRADEID
	order by A.PSDAT,A.PSTME --423

	select * from TBPROG_D where FSPROG_ID = '2017811' and FNEPISODE = 0 --NO RECORD 公視新聞報, 0

	select A.PAENO FSPROGID,A.PSDAT FDDATE,A.PSTME FSBTIME,B.FSWEBNAME,A.PBENO FNEPISODE,B.FNTOTEPISODE
	from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.PSHOW_PTS_MOD  where PSDAT>=sysdate() and PSDAT<= sysdate()+ interval 14 day') A,TBPROG_M B 
	where A.PAENO=B.FSPROG_ID order by A.PSDAT,A.PSTME --423

	select * from TBPROG_D where FSPROG_ID = '0000082' and FNEPISODE = 5442
	select * from TBZPROGGRADE

end

if @case = 201705191539 begin --調用卡在 TSM 處理中
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201705190028' --FCFILE_TRANSCODE_STATUS = R
	/* tapeoffline
	\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700004\D201307000040002.mxf
	\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700047\D201307000470001.mxf
	*/
end

if @case = 201705191626 begin
	select * from TBLOG_VIDEO where FSID = '2017812' and FNEPISODE = 5 --R, G201781200050003
end

if @case = 201705231024 begin
	select * from TBTRACKINGLOG where FDTIME > '2017-05-23 10:00' order by FDTIME
end

if @case = 201705231345-201704181116 begin
	select * from TBLOG_VIDEO where FSID = '2017689' --G201768900010002, 26 轉檔完成檢索不到
	select * from TBPROG_M where FSPROG_ID = '2017689' --臺中市2017新丁粄節
	select * from TBLOG_VIDEO where FSARC_TYPE = '026' and FCFILE_STATUS = 'T'
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201768900010002' --FCFILE_STATUS = D, 026
	update TBLOG_VIDEO_D set FCFILE_STATUS = 'T' where FSFILE_NO = 'G201768900010002' --6 個資料列受影響

	select * from TBTRACKINGLOG where FDTIME between '2017-03-22' and '2017-03-24' and FSMESSAGE like '%G201768900010002%'  order by FDTIME

	select * from TBLOG_VIDEO where FSBRO_ID = '201703220092' --臺中市2017新丁粄節(1)分軌帶.mxf, G201768900010002, 2017-03-23 02:00:04.397
	select * from TBBROADCAST where FSBRO_ID = '201703220092' --FCCHECK_STATUS = 'K'
	
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201768900010005' --T
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201768900010005' --002

	--G201768900010002
	select * from TBLOG_VIDEO A left join TBLOG_VIDEO_D B on A.FSFILE_NO = B.FSFILE_NO where A.FSARC_TYPE = '026' 
	and A.FCFILE_STATUS = 'T' and B.FCFILE_STATUS <> 'T'
	--\\mamstream\MAMDFS\hakkatv\HVideo\2017\2017689\G201768900010002.mxf
	--\\mamstream\MAMDFS\Media\Lv\hakkatv\2017\2017689\G201768900010002.mxf

	select * from TBLOG_VIDEO where FSARC_TYPE = '026' and FCFILE_STATUS = 'T' order by FSFILE_NO
	select * from TBLOG_VIDEO_D where FSFILE_NO in ('G201768900010002', 'G201768600020002')
end

if @case = 201705231804 begin
	select * from TBTRACKINGLOG where FDTIME > '2017-05-24 11:00' order by FDTIME
end


if @case = 201705241151 begin --標記上傳完成 插入短帶仍用舊的編號
	exec SP_Q_INFO_BY_VIDEOID '00003LJD'  --C --P201705001560001, FSID = 20170500156  -> O, T
	exec SP_Q_INFO_BY_VIDEOID '00003N3W' --PTS3_藝文大道重播第210集_Promo 2017-05-28, 14

	exec SP_Q_TBPGM_COMBINE_QUEUE '2017-05-28', '14'
	exec SP_Q_TBPGM_ARR_PROMO_NEW_VIDEOID_MIX '2017-05-28', '14'

	select * from TBLOG_VIDEOID_MAP where FSID = '20170500156' -- FSVIDEO_ID_PROG = 00003N3W
	update TBLOG_VIDEOID_MAP set FSVIDEO_ID_PROG = '00003LJD' where FSID = '20170500156'
end

if @case = 20175241645 begin  -- 調用卡在 TSM 處理中
	select * from TBUSERS where FSUSER_ChtName = '劉秋信' --05065, adm3036
	exec SP_Q_GET_BOOKING_FILE_BY_USERID '05065', '' --201705230080
	--\\mamstream\MAMDFS\ptstv\HVideo\0000\0000072\G000007208290001.mxf 我們的島, 829 offline
	--\\mamstream\MAMDFS\ptstv\HVideo\0000\0000072\G000007208350001.mxf 我們的島, 835  tape only access deny
end


if @case = 201705251158 begin
	exec SP_Q_TBPROG_M_CODE --查詢所有代碼檔
end

if @case = 201705251222 begin
	exec SP_Q_INFO_BY_VIDEOID '00003MO5'
	select * from TBUSERS where FSUSER_ID = '71244' --鍾依芹
	select * from TBUSERS where FSUSER_ID = '50311' --鄒立文 審核通過
end


if @case = 201705251338 begin
	select * from TBARCHIVE_SET --節目入庫項目管理
	select COUNT(*)  from TBARCHIVE_SET as a  where FSID= 2017581 and FSTYPE='G' and FNEPISODE=1
	select * from TBUSERS where FSUSER_ID = '07194' --黃靜國

	select * from TBLOG_PHOTO order by FDCREATED_DATE desc
	select * from TBTRACKINGLOG where FDTIME > '2017-05-25 15:55' order by FDTIME
	select * from TBTRACKINGLOG where FDTIME > '2017-05-25 22:50' order by FDTIME

	select * from TBTRACKINGLOG where FDTIME > '2017-05-26 07:10' order by FDTIME
end

if @case = 201705251736 begin --00003N8H 原本排在 5/29 審核，但是在 5/30 的表沒有重新載入，所以仍是檔案未到的狀態
	exec SP_Q_INFO_BY_VIDEOID '00003N8H'
end

if @case = 201705261150 begin
	exec SP_Q_INFO_BY_VIDEOID '00003MVZ' --O, B
	update TBLOG_VIDEO set FCFILE_STATUS = 'X' where FSFILE_NO = 'P201705006290001'
	update TBLOG_VIDEO_D set FCFILE_STATUS = 'X' where FSFILE_NO = 'P201705006290001'
end

if @case = 201705261202 begin
	select @@SERVERNAME --MAMSQL
end

if @case = 201705261427 begin --標記上傳失敗
	exec SP_Q_INFO_BY_VIDEOID '00003NB3' --客家新聞雜誌(客家製播)-242集排播需注意, G200626205410001
	exec SP_Q_INFO_BY_VIDEOID '00003N2Y'
	exec SP_Q_INFO_BY_VIDEOID '00003N2Z'
	exec SP_Q_INFO_BY_VIDEOID ''
	exec SP_Q_INFO_BY_VIDEOID ''
end

if @case = 201705311038 begin
	select * from VW_GET_CHECK_COMPLETE_LIST_MIX_MACRO
end

if @case = 201705311039 begin
	select * from TBTRACKINGLOG where FDTIME > '2017-05-31 10:35' order by FDTIME
end

if @case = 201705311553 begin  --調用問題
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201705240060' --重新調用 抗癌新紀元(主題之夜導讀版) \\mamstream\MAMDFS\ptstv\HVideo\2017\2017857\G201785700010001.mxf
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201705240064' --\\mamstream\MAMDFS\ptstv\HVideo\2017\2017815\G201781500010001.mxf
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201705240066' --\\mamstream\MAMDFS\ptstv\HVideo\2017\2017856\G201785600010001.mxf
	select * from TBBOOKING_DETAIL where FSBOOKING_NO = '201705240066'
	select * from TBUSERS where FSUSER_ID = '71264' --賴慕寧, ninalai
end

if @case = 201706011031 begin
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201705310065' --\\mamstream\MAMDFS\ptstv\HVideo\2008\2008936\G200893600010001.mxf 爺奶搶時間 /ptstv/HVideo/2008/2008936/G200893600010001.mxf
end

if @case = 201706011539 begin --轉檔失敗卻有低解
	exec SP_Q_INFO_BY_VIDEOID '00003MIN' --G200791300040001 R
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G200791300040001' --T
	update TBLOG_VIDEO set FCLOW_RES = 'Y', FCFILE_STATUS = 'T' where FSFILE_NO = 'G200791300040001'
	select * from TBTRANSCODE where FNTRANSCODE_ID = 163138 --2017-05-30 16:18:52.743
	select * from TBTRANSCODE_RECV_LOG where FNJOB_ID = 113505 --有兩筆紀錄 2017-05-30 16:37:46.000, 2017-05-31 17:18:28.000
	exec SP_Q_TBTRACKINGLOG_BY_DATE '163138', '113505', '233542', 'G200791300040001', '', '2017-05-30', NULL --2017-05-30 16:52:01.043 已完成檔案編號：G200791300040001 所有轉檔完成處理程序
end


if @case = 201706020958 begin --check fragmentation --117
	SELECT  DB_NAME(ips.database_id) DBName, OBJECT_NAME(ips.object_id) ObjName, i.name InxName, ips.avg_fragmentation_in_percent 
	FROM sys.dm_db_index_physical_stats(db_id('MAM'), default, default, default, default) ips 
	INNER JOIN sys.indexes i ON ips.index_id = i.index_id AND ips.object_id = i.object_id 
	WHERE 
     ips.object_id > 99 AND 
     ips.avg_fragmentation_in_percent >= 10 AND 
     ips.index_id > 0
end

if @case = 201706021001 begin
	exec SP_Q_INFO_BY_VIDEOID '00003NQ7' --6/11宏觀 menu0030 2017-06-11 201706010134
end

if @case = 201706021644 begin  --成案成核 delta flow 顯示已審核 但是 MAM 顯示未審核
	select * from TBPROG_M where FSPROG_ID = '2017926' --no data
	select * from TBPROPOSAL where FSPRO_ID = '201705050104' --頭期  張文嘉 2017-05-09 10:26:34.027 審核 FCCOMPLETE = N
	select * from TBUSERS where FSUSER_ID = '50594' --張文嘉
	select * from TBPROG_M where FSPGMNAME like '%一把青%' --2016478
	select * from TBPROPOSAL where FSPROG_ID = '2016478' --201510160016 FCCOMPLETE = C
	select * from TBUSERS where FSUSER_ID = '70110' --劉盈潔  -> new flow ID

	select * from TBPROPOSAL where FSPRO_ID in ('201705050097', '201705050096', '201705050095') -- FCCHECK_STATUS N->Y
	--停車格 201705050095, 2017923 --> new flowid 71210
	--門裡門外 201705050096, 2017924 --> 71211
	--無知 201705050097, 2017925 --> 71212
	select * from TBPROG_M where FSPROG_ID in ('2017923', '2017924', '2017925') --no record -> has record
	select * from TBPROPOSAL where FSPRO_ID = '201705050097' --FCCHECK_STATUS = N
end

if @case = 201706021838 begin --抽單後無法再新增 FCFILE_STATUS = N
	select * from TBLOG_VIDEO where FSID = '2007049' and FNEPISODE = 203
	update TBLOG_VIDEO set FCFILE_STATUS = 'D' where FSFILE_NO = 'G200704902030001'
end

if @case = 201706030927 begin
	select * from TBTRACKINGLOG where FDTIME between '2017-06-03 08:30' and '2017-06-03 09:20' order by FDTIME 
	select * from TBTRACKINGLOG where FDTIME > '2017-06-03 10:50' order by FDTIME 
end

if @case = 201706031038 begin --05065 劉秋信 #1852
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201705240099'
	--公視藝文大道 160 \\mamstream\MAMDFS\ptstv\HVideo\2012\2012078\G201207801600001.mxf  offline E01453L4
	--我們的島 832 \\mamstream\MAMDFS\ptstv\HVideo\0000\0000072\G000007208320001.mxf offline E01666L4
	--我們的島 845 \\mamstream\MAMDFS\ptstv\HVideo\0000\0000072\G000007208450001.mxf offline E01714L4
	select * from TBUSERS where FSUSER_ID = '05065' --adm3036

	select * from TBBOOKING_DETAIL where FSBOOKING_NO = '201705240099'
end

if @case = 201706031255 begin --卡在 HD_FILE_MANAGER
	exec SP_Q_INFO_BY_VIDEOID '000004OE' --爸媽囧很大 526
	--G200928505260001 T
	--G200928505260002 X ??
end

-- dsmrecall /ptstv/HVideo/0000/0000072/G000007208350001.mxf hang, who recall ?
if @case = 201706031259 begin
	select * from TBLOG_VIDEO where FSFILE_NO = 'G000007208350001' --T 0000288I 001
	exec SP_Q_INFO_BY_VIDEOID '0000288I' -- not scheduled
end

if @case = 201706050902 begin
	exec SP_Q_INFO_BY_VIDEOID '000004QU' --爸媽囧很大 527, ch13  2017-06-14 B00285L4
end

if @case = 201706051410 begin --調用檔案已下架 /ptstv/HVideo/0000/0000072/G000007207600002.mxf, B00136L4
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201706030010' --我們的島 760
end

if @case = 201706061156 begin
	select * from TBLOG_VIDEO where FSID = '2017548' and FNEPISODE = 20 --G201754800200001, 201706050085
	select * from TBBROADCAST where FSBRO_ID = '201706050085' --no record
	update TBLOG_VIDEO set FCFILE_STATUS = 'X' where FSFILE_NO = 'G201754800200001'
end

if @case = 201706062039 begin --Tapeonly partial 處理錯誤
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201706010105'
	--一個家庭的事 \\mamstream\MAMDFS\ptstv\HVideo\2016\2016932\G201693200010001.mxf /ptstv/HVideo/2016/2016932/G201693200010001.mxf
	--公梯 \\mamstream\MAMDFS\ptstv\HVideo\2015\2015885\G201588500010017.mxf  /ptstv/HVideo/2015/2015885/G201588500010017.mxf
	select * from TBUSERS where FSUSER_ID = '08303' --prg1159 龔美靜
end

if @case = 201706080931 begin --響度測試
	select * from TBLOG_VIDEO where FSID = '2013232' and FCFILE_STATUS in ('B') order by FNEPISODE
	-- #6, G201323200060005
	exec SP_Q_INFO_BY_VIDEOID '00002JD6' --已標記上傳 FCSTATUS = '' -> C -> N
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G201323200060005' --只有一段
	--模擬檔案先傳到 HDUpload\AST_IN 先將  FCSTATUS 改成上傳完成 FCSTATUS = 'C'
	--在檔案傳到 HDUpload  前會出現失敗 retry
end

if @case = 201706081137 begin --HD_FILE_MANAGER 問題
	exec SP_Q_INFO_BY_VIDEOID '00003I5O'
end

if @case = 201706061203 begin -- 在 MAM 修改級別，沒有更新到節目管理系統，即使更重新新到節目管理系統，週節目表仍然未更新
	--改成護 系統會要求輸入說明 why FSPROGRADE is empty?
	select * from TBPROG_D where FSPROG_ID = '2017917' and FNEPISODE = 1 --我的告別式(客語版)  2017-06-07 15:13:04.563 update  FSPROGGRADEID = 03 護：16:00-19:00不可播
	select * from TBZPROGGRADE
	select * from TBLOG_VIDEO where FSID = '2017917' --G201791700010001, 00003O0Q
	exec SP_Q_INFO_BY_VIDEOID '00003O0Q' --2017-06-10, ch=07
	select * from PTSPROG.PTSProgram.dbo.TBPROG_D where FSPROGID = '2017917' --FSPROGRADEID = 01 --未同步到節目管理系統  
	--重新  submit FSPROGRADEID = 03
	-- 但是節目表查詢的資料仍未更新 http://web.pts.org.tw/php/programX/user.php
    
	select * from TBPROG_D where FSPROG_Id = '2013232' and FNEPISODE = 1 --FSPROGRADEID = 03
	select * from PTSPROG.PTSProgram.dbo.TBPROG_D where FSPROGID = '2013232' and FNEPISODE = 1 --FSPROGRADEID = 03
end

if @case = 201706081359 begin --2016-09-02 轉檔失敗，重播卡在 Review -> Approved
	exec SP_Q_INFO_BY_VIDEOID '00002VRY' --R 142878 2016PTS1 RM 公視學生劇展 彌撒 2017-06-11 12
	select * from TBTRANSCODE where FNTRANSCODE_ID = 142878  --2016-09-02 10:57:54.503
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201608002720001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201608002720001'
end

if @case = 201706081648 begin --2016-17 歐洲冠軍足球聯賽 #81 2017-06-12 ch12 卡在 TSM 處理中
	exec SP_Q_INFO_BY_VIDEOID '00003LMR' --O, S 161645
	select * from TBTRANSCODE where FNTRANSCODE_ID = 161645 --FNANYSTREAM_ID = -1 2017-05-09 10:29:26.760
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201734900810001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201734900810001'
end

if @case = 201706122314 begin --hang on Review -> approved
	exec SP_Q_INFO_BY_VIDEOID '000034HA'  --not scheduled
end

if @case = 201706131035 begin
	exec SP_Q_INFO_BY_VIDEOID '00003OME' --活動-4K測試招募 2017-06-15 ch12, T
end

if @case = 201706141519 begin --QC 轉出低解無法播放
	select * from TBPROG_M where FSPGMNAME like '%誰來晚餐%' --誰來晚餐9, 2017744, R, 164217
	select * from TBLOG_VIDEO where FSID = '2017744' and FNEPISODE = 11 --R 00003NYX, \\mamstream\MAMDFS\ptstv\HVideo\2017\2017744\G201774400110001.mxf exist
	select * from TBTRANSCODE where FNTRANSCODE_ID = 164217 --FDSTRT_TIME 2017-06-13 16:21:56.720, FNPARTIAL_ID 162931, ANS 114416
	select * from TBTRANSCODE_PARTIAL where FNPARTIAL_ID = 162931
end

if @case = 201706141701 begin --已標記上傳，檔案已上傳到 HDUpload 但狀態未更新
	exec SP_Q_INFO_BY_VIDEOID '00003OS9'
end


if @case = 201706150927 begin --tape offline
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201706130088'
	--下課花路米(1323集起HD可播) 1385 \\mamstream\MAMDFS\ptstv\HVideo\0000\0000816\G000081613850001.mxf --offline /ptstv/HVideo/0000/0000816/G000081613850001.mxf
	--下課花路米(1323集起HD可播) 1413 \\mamstream\MAMDFS\ptstv\HVideo\0000\0000816\G000081614130001.mxf --offline /ptstv/HVideo/0000/0000816/G000081614130001.mxf
end

if @case = 201706151056 begin --麻醉風暴搶先看 offline
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201706140105' --\\mamstream\MAMDFS\ptstv\HVideo\2015\2015857\G201585700010001.mxf 麻醉風暴搶先看 offline /ptstv/HVideo/2015/2015857/G201585700010001.mxf
end

if @case = 201706151124 begin --tape offline, 真情旅歷 17 G201644300170002,
	exec SP_Q_INFO_BY_VIDEOID '00002HMM' --真情旅歷 17 G201644300170002, \\mamstream\MAMDFS\mvtv\HVideo\2016\2016443\G201644300170002.mxf
end

if @case = 201706151126 begin --阿罩霧風雲II 落子 搬檔失敗 done
	select * from TBPROG_M where FSPROG_ID = '2017117' --阿罩霧風雲II 落子 done
	select * from TBLOG_VIDEO where FSID = '2017117' --S, G201711700010002, 201701030155, 000039QJ, 162637
	exec SP_Q_INFO_BY_VIDEOID '000039QJ' --2017-06-25 ch14

	--162637, 113048, 4, G201711700010002, 000039QJ, 201701030155, 002, S, 232181,LRLR
	exec SP_Q_INFO_BY_VIDEOID '000039QJ'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201711700010002'
end

if @case = 201706151340 begin --積木之家 4 OC 通過 轉檔成功, 低解有在 IngestWorkingTemp need reschedule, done
	select * from TBLOG_VIDEO where FSBRO_ID = '201705090020' --B. G201766800040001, 2017668, 4, 00003LRA (in HDUpload)
	exec SP_Q_INFO_BY_VIDEOID '00003LRA' --積木之家, O, FSJOB_ID = -2
	select * from TBTRANSCODE where FNTRANSCODE_ID = 162235 --231154, 4
	exec SP_Q_TBTRACKINGLOG_BY_DATE '162235', '112710', '231154', 'G201766800040001', '00003LRA', '2017-05-16', NULL
	--Handler_ReceiverTransCode_HD/ProcessRequest 執行網路服務(WSBROADCAST.GetTBLOG_VIDEO_PATH_BYJOBID)時發生錯誤，轉檔編號：162235
	exec SP_Q_TBTRACKINGLOG_BY_DATE '162235', '112710', '231154', 'G201766800040001', '00003LRA', '2017-06-15', NULL
	--2017-06-15 23:19:34.990 執行網路服務(WSBROADCAST.GetTBLOG_VIDEO_PATH_BYJOBID)時發生錯誤，轉檔編號：162235


	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201766800040001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201766800040001'

	exec SP_Q_TBTRACKINGLOG_BY_DATE '164454', '114626', '236969', 'G201766800040001', '00003LRA', '2017-06-16', NULL
	--plink高解搬檔失敗：JobID 164454/10.13.210.15 -l root -pw TSMP@ssw0rdTSM "cp -r /mnt/HDUpload/003LRA.mxf  /ptstv/HVideo/2017/2017668/G201766800040001.mxf"/cp: writing `/ptstv/HVideo/2017/2017668/G201766800040001.mxf': No space left on device
	--GPFS -> Approved
end

if @case = 201706151703 begin -- 搬檔失敗 爸媽囧很大, 530
	select * from TBLOG_VIDEO where FSFILE_NO = 'G200928505300013'
	select * from TBPROG_M where FSPGMNAME like '%爸媽%' --2009285
	select * from TBLOG_VIDEO where FSID = '2009285' and FNEPISODE = 530 --00003O2F, 201706090012
	exec SP_Q_INFO_BY_VIDEOID '00003O2F' --爸媽囧很大, 530 ch13
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G200928505300013' --done
end

if @case = 201706151707 begin
	exec SP_Q_INFO_BY_VIDEOID '00002HMM' --真情旅歷 17 /mvtv/HVideo/2016/2016443/G201644300170002.mxf 2017-06-16 ch 08
end

if @case = 201706152253 begin --調用檔案已下架
	exec SP_Q_GET_BOOKING_FILE_BY_USERID '51322', 'F'
	select * from TBUSERS where FSUSER_ChtName = '林秭瑜' --51322, wanheda
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201706120021' --D201303000040001. \\mamstream\MAMDFS\ptstv\HVideo\2013\20130300004\D201303000040001.mxf offline
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201706120022' 
	--\\mamstream\MAMDFS\ptstv\HVideo\2013\20130300015\D201303000150001.mxf offline
	--\\mamstream\MAMDFS\ptstv\HVideo\2013\20130300016\D201303000160001.mxf offline B00315L4, B00235L4(online?? 為什麼有 2 支？既然 online 為什麼無法 recall)
	--\\mamstream\MAMDFS\ptstv\HVideo\2013\20130300017\D201303000170001.mxf offline
	select * from TBLOG_VIDEO where FSFILE_NO = 'D201303000150001' --20130300015
	select * from TBPROG_M where FSPROG_ID = '20130300015' --no data

	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201706120023' 
	--\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700006\D201307000060001.mxf offline
	--\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700010\D201307000100001.mxf offline
	--\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700011\D201307000110001.mxf offline
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201706120024' 
	--\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700018\D201307000180001.mxf offline
	--\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700020\D201307000200001.mxf offline
	--\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700026\D201307000260001.mxf offline

	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201706120030'
	--\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700067\D201307000670001.mxf offline B00070L4
	--\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700072\D201307000720001.mxf offline B00070L4
	--\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700090\D201307000900001.mxf offline A00063L4
end

if @case = 201706161205 begin --阿罩霧風雲I 抉擇 1, 2017-06-18 ch14, file is in Review
	exec SP_Q_INFO_BY_VIDEOID '000039QH' --T, FCSTATUS = '' --\\mamstream\MAMDFS\ptstv\HVideo\2017\2017116\G201711600010002.mxf
	--FCSSTATUS -> N
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201711600010002'
end

if @case = 201706161223 begin --AUDIO test
	exec SP_Q_INFO_BY_VIDEOID '00003P3G'
end

if @case = 201706191026 begin --估算已轉檔入庫的資料量 
	--估算已轉檔入庫的資料量  328857.246 GB = 328T, 20170123:440T --> 526094.153 G --> 526T
	select
	SUM(CASE
			WHEN FSFILE_SIZE like '%GB%' THEN CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2))
			WHEN FSFILE_SIZE like '%MB%' THEN CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2))/1000
		END
	)
	from TBLOG_VIDEO where FCFILE_STATUS = 'T' and FSTYPE = 'G' and FSFILE_SIZE <> ''


	--------------------------------------------------------------------------

	--匯入 子集資料(境外託捐-體育節目).xls
	if OBJECT_ID(N'tempdb.dbo.#tempImport', N'U') IS NOT NULL
        DROP TABLE #tempImport

	 CREATE TABLE #tempImport (
				FNEPISODE int,
                FSID varchar(10)
     )

    BULK insert #tempImport from 'G:\pts-sport.csv' WITH
        (
                FIELDTERMINATOR=',', ROWTERMINATOR = '\n'
        );
    --select * from  #tempImport --2794

	--select * from #tempImport A left join TBLOG_VIDEO B on A.FSID = B.FSID and A.FNEPISODE = B.FNEPISODE and B.FCFILE_STATUS = 'T' --2794

	--select FSFILE_SIZE from #tempImport A join TBLOG_VIDEO B on A.FSID = B.FSID and A.FNEPISODE = B.FNEPISODE and B.FCFILE_STATUS = 'T' --408

    select 
		SUM(CASE
			WHEN FSFILE_SIZE like '%GB%' THEN CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2))
			WHEN FSFILE_SIZE like '%MB%' THEN CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2))/1000
		END
	) from #tempImport A join TBLOG_VIDEO B on A.FSID = B.FSID and A.FNEPISODE = B.FNEPISODE and B.FCFILE_STATUS = 'T' --10761.5 G --> 10.7T

	----------------------------------------------------------------------------
	--已入庫檔案統計
	select REPLACE(SUBSTRING(FSFILE_PATH_H , 19, LEN(FSFILE_PATH_H)), '\', '/') from TBLOG_VIDEO where FCFILE_STATUS = 'T' --51565
	/*
	Total file size = 604.76010084072, 51564 files
    PTSTV : 510.761722080168, 43891 files
    HAKKATV : 72.247840518612, 5931 files
    MVTV : 19.445457367956, 1590 files
    TITVTV : 2.305080873984, 152 files
	*/

end

if @case = 201706191524 begin
    exec SP_Q_INFO_BY_VIDEOID '00003OYT' --20G 48min, 18:05 ~ 18:44 = 39min
	exec SP_Q_INFO_BY_VIDEOID '00003NDQ' --4.7G (11min), 17:59 ~ 18:04 - 5min
end


if @case = 201706201113 begin --查詢公益託播
	if @case = 201705171233 begin --查詢公益託播
	select A.FSPROMO_ID 短帶編號, B.FSWELFARE 公益編號, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間, B.FSPROMO_NAME 短帶名稱 from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
        join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where B.FSMAIN_CHANNEL_ID = '07' and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') and B.FSWELFARE <> ''
        and A.FDDATE between '2017-01-01' and '2017-04-30' order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME --2583

	exec SP_Q_WELFAREPROMO_PLAYOUT_BY_DATE '07', '2017-01-01', '2017-04-30', '1'
    exec SP_Q_WELFAREPROMO_PLAY_COUNT_BY_DATE '07', '2017-01-01', '2017-04-30'

	select * from TBPGM_PROMO where FSWELFARE = '3609' --2017公益-105年度綜所稅結算申報宣導-憑證報稅篇, 20170400921
	select * from TBPGM_PROMO where FSWELFARE = '3608' --2017公益-105年度綜所稅結算申報宣導-稅額試算篇, 20170400920

	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20170400921'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20170400920'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

end

if @case = 201706211014 begin
	--匯入 要修改級別資料
	if OBJECT_ID(N'tempdb.dbo.#tempImport', N'U') IS NOT NULL
        DROP TABLE #tempImport

	 CREATE TABLE #tempImport (
	    FSID varchar(10),
		FNEPISODE int,
		FSGRADEID varchar(3)  
     )

    BULK insert #tempImport from 'G:\proggrade.csv' WITH
        (
                FIELDTERMINATOR=',', ROWTERMINATOR = '\n'
        );
    --select * from  #tempImport --41

	select B.FSPROG_ID 節目編號, B.FSPGDNAME 子集名稱, B.FNEPISODE 集數, B.FSPROGGRADEID 分級代碼, C.FSPROGGRADENAME 分級
	from #tempImport A left join TBPROG_D B on  A.FSID = B.FSPROG_ID and A.FNEPISODE = B.FNEPISODE
	   left join TBZPROGGRADE C on B.FSPROGGRADEID = C.FSPROGGRADEID

	select * from TBZPROGGRADE
end

if @case = 201706211048 begin --修改子即位同步到節目管理系統
	select * from TBPROG_D where FSPROG_ID = '2017023' --03
	select * from TBPROG_D where FSPROG_ID = '2017260'
    select * from PTSPROG.PTSProgram.dbo.TBPROG_D where FSPROGID = '2017023' --FSPROGGRADEID = 01
	select * from PTSPROG.PTSProgram.dbo.TBPROG_D where FSPROGID = '2017260' --FSPROGGRADEID = 01
	select * from TBTRACKINGLOG where FDTIME > '2017-06-21 10:50' order by FDTIME
end

if @case = 201706221213 begin --已標記上傳 FCSTATUS = ''
	exec SP_Q_INFO_BY_VIDEOID '00003OSA' --生態全紀錄-四季的華麗之歌(夏天) 2017-06-24 ch12, P201706003940001, 201706130087
end

if @case = 201706221507 begin
	select * from TBLOG_VIDEO where FSFILE_NO = 'G000007209050001' --01:00:00;00 ~ 01:51:20;00
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G000007209050001'
end

if @case = 201706221624 begin
	exec SP_Q_INFO_BY_VIDEOID '00003OKW' --夢裡的一千道牆 4 2017-06-26 ch12
	exec SP_Q_INFO_BY_VIDEOID '00003OKX' --夢裡的一千道牆 5 2017-06-27 ch62
end

if @case = 201706221807 begin
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201774400120001' --00003NZ7
	exec SP_Q_INFO_BY_VIDEOID '00003NZ7' --誰來晚餐9, 12 2017-06-23 ch12
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G201774400120001'
end

if @case = 201706230957 begin --動物小遊俠, 36 2017-06-26 ch12
	exec SP_Q_INFO_BY_VIDEOID '00003NNJ'
end

if @case = 201706231339 begin  --卡在 TSM 處理中. recall 正常
	exec SP_Q_INFO_BY_VIDEOID '00003PLK' --P201706006490001, 公視國際影展-纏繞之蛇 
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201706006490001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201706006490001'
end

if @case = 201706231353 begin
	exec SP_Q_INFO_BY_VIDEOID '00003N6D' --PTS1_海_墊檔帶_陸蟹危機_50秒 2017-06-27 ch12, O, T 卡在  TSMRECALL
	exec SP_Q_INFO_BY_VIDEOID '00003NII' --2016PTS1 RM 台灣心動線-情牽四海 2017-06-23, O, T
end

if @case = 201706261748 begin --標記上傳後狀態未更新
	exec SP_Q_INFO_BY_VIDEOID '00003PG7' --彩色寧靜海 17
	exec SP_Q_INFO_BY_VIDEOID '00003PGC' --彩色寧靜海 18
	exec SP_Q_INFO_BY_VIDEOID '00003PGI' --彩色寧靜海 19
end


if @case = 201706270727 begin --demrecall hang
	exec SP_Q_INFO_BY_VIDEOID '00002U9Z' --種歌．眾歌(2-2) 2017-06-29 ch14 \\mamstream\MAMDFS\ptstv\HVideo\2014\2014761\G201476100010007.mxf
end


if @case = 201706270954 begin --done
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201706210123'
	--兩個爸爸 兩個娃 \\mamstream\MAMDFS\ptstv\HVideo\2014\2014890\G201489000010001.mxf offline
end

if @case = 201706271052 begin --轉檔失敗 done
	select * from  TBLOG_VIDEO where FSID = 2017910 and FNEPISODE = 2 --G201791000020001, R, 00003NEH
    select * from  TBLOG_VIDEO where FSID = 2017910 and FNEPISODE = 1 --T
	exec SP_Q_INFO_BY_VIDEOID '00003NEH' --費里尼:夢是唯一的現實(60分鐘版)  done
	select * from TBTRANSCODE where FNTRANSCODE_ID = 163682 --4, 2017-06-06 13:55:12.207, ANSID 113964 --> reschedule anystream
end

if @case = 201706271155 begin --done
	exec SP_Q_INFO_BY_VIDEOID '00003OP6' --台灣心動線HD版 2017-06-30 #1 ch62 --S --G201097500010001, 201706130003, 164170
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201097500010001'
	select * from TBTRANSCODE where FNTRANSCODE_ID = 164170 --FNPROCESS_STATUS = 4
end

if @case = 201706271715 begin --調用 partial 處理錯誤
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201706270092'
	--\\mamstream\MAMDFS\ptstv\HVideo\2016\2016442\G201644200010010.mxf
	--\\mamstream\MAMDFS\ptstv\HVideo\2016\2016442\G201644200050004.mxf
	--\\mamstream\MAMDFS\ptstv\HVideo\2016\2016442\G201644200080002.mxf
	--\\mamstream\MAMDFS\ptstv\HVideo\2016\2016442\G201644200110012.mxf
	--\\mamstream\MAMDFS\ptstv\HVideo\2016\2016442\G201644200120002.mxf
	--\\mamstream\MAMDFS\ptstv\HVideo\2016\2016442\G201644200140001.mxf

	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201706270094'
	--\\mamstream\MAMDFS\ptstv\HVideo\2016\2016674\G201667400010012.mxf

	select * from TBUSERS where FSUSER_ID = '05065' --adm3036
end

if @case = 201706280926 begin --上傳程式卡住
	exec SP_Q_INFO_BY_VIDEOID '00001O14' -- 台灣食堂 #2 2017-07-02 62 tapeonly
end

if @case = 201706280929 begin --置換未排播無法入庫，後製不幫標記上傳
	select * from TBPROG_M where FSPGMNAME like '%青絲劍%' --2017057, 2017827
	select * from TBLOG_VIDEO where FSID = '2017827' --G201782700010002, 00003M3O, 201705120053  done
	select * from TBLOG_VIDEO where FSID = '2017057' --G201705700010009, 00003M3N, 201705120052 done

	exec SP_Q_INFO_BY_VIDEOID '00003M3O'
	exec SP_Q_INFO_BY_VIDEOID '00003M3N'
end

if @case = 201706281522 begin --搬檔失敗 done
	exec SP_Q_INFO_BY_VIDEOID '000026DD' --纏繞之蛇(2-1) 1 2017-07-01 ch14164072, 164072, 201510270054, G201651500010001
	select * from TBTRANSCODE where FNTRANSCODE_ID = 164072 --FNPROCESS_STATUS = 4, 2017-06-12 09:38:07.140
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201651500010001'
end


if @case = 201706281637 begin
	select * from TBLOG_VIDEO where FSBRO_ID = '201706210060' --G200928505440017, 00003PKK, 164774 done
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G200928505440017'
	select * from TBLOG_VIDEO where FSBRO_ID = '201706210063' --G200928505450018, 00003PKL, 164776 done
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G200928505450018'
end

if @case = 201706291348 begin --文彬說檢索不到，只打節目名稱查不到?
	select * from TBLOG_VIDEO where FSBRO_ID = '201702230183'
end


if @case = 201706301033 begin -- 在 MAM 修改級別，沒有更新到節目管理系統，即使更重新新到節目管理系統，週節目表仍然未更新
	--改成護 系統會要求輸入說明 why FSPROGRADE is empty?
	select * from TBPROG_D where FSPROG_ID = '2017904' and FNEPISODE = 1 --偽婚男女  2017-06-07 15:13:04.563 update  FSPROGGRADEID = 03 護：16:00-19:00不可播
	select * from TBZPROGGRADE
	select * from TBLOG_VIDEO where FSID = '2017917' --G201791700010001, 00003O0Q
	exec SP_Q_INFO_BY_VIDEOID '00003O0Q' --2017-06-10, ch=07
	select * from PTSPROG.PTSProgram.dbo.TBPROG_D where FSPROGID = '2017917' --FSPROGRADEID = 01 --未同步到節目管理系統  
	--重新  submit FSPROGRADEID = 03
	-- 但是節目表查詢的資料仍未更新 http://web.pts.org.tw/php/programX/user.php
    
	select * from TBPROG_D where FSPROG_Id = '2013232' and FNEPISODE = 1 --FSPROGRADEID = 03
	select * from PTSPROG.PTSProgram.dbo.TBPROG_D where FSPROGID = '2013232' and FNEPISODE = 1 --FSPROGRADEID = 03
end


if @case = 201706301225 begin --台灣心動線HD版 6 2017-07-07 ch62  done
	exec SP_Q_INFO_BY_VIDEOID '00003PS4' --S, G201097500060001, 201706230062, 165064
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201097500060001'
	select * from TBTRANSCODE where FNTRANSCODE_ID = 165064 --4
end

if @case = 201706301330 begin --搬檔失敗 done
	select * from TBLOG_VIDEO where FSJOB_ID = '165432' --G201077500010021, 201706300004, 2010775
	select * from TBPROG_M where FSPROG_ID = '2010775' --IP@台灣
	select * from TBPROG_M where FSPGMNAME like '%回家路上%' --回家路上(口述影像版) 2016818
	select * from TBLOG_VIDEO where FSID = '2016818' --G201681800010005, S, 201706280094, 165342, 00003Q8E
	exec SP_Q_INFO_BY_VIDEOID '00003Q8E' --not scheduled

	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201681800010005'
end

if @case = 201706301558 begin  --搬檔失敗， reschedule, done
	exec SP_Q_INFO_BY_VIDEOID '00003LZR' --就是這Young! 16, R, 201705110074, 165093
	select * from TBTRANSCODE where FNTRANSCODE_ID = 165093 --ANS 115191, 4 --2017-06-23 20:54:19.087
end

if @case = 201707030919 begin --卡在 HD_FILE_MANAGER
	exec SP_Q_INFO_BY_VIDEOID '00003DTH' --中華美食在臺灣系列(徽菜)紮實又經濟的美食 創意徽菜 2017-07-06 ch08
end


--------------------------------------20170703---------------------------------------------------

if @case = 201707030927 begin --hang on TSMRECALL
	select * from TBTSM_JOB where FNTSMJOB_ID = 239822 --FNSTATUS = 0
	select * from TBTSM_JOB where FDREGISTER_TIME > '2017-07-03' order by FDREGISTER_TIME
	select * from TBTSM_JOB where FNSTATUS <> 3 --2163
	select * from TBTSM_JOB where FNSTATUS = 4 --118
	select * from TBTSM_JOB where FNSTATUS = 0 --1
	select * from TBTSM_JOB where FNSTATUS = 2 --3
	select * from TBTSM_JOB where FNSTATUS = 5 --1814
end

if @case = 201707031025 begin --dsmrecall fail
	exec SP_Q_INFO_BY_VIDEOID '00002W2J' --一休和尚 183 2017-07-05
end

if @case = 201707031337 begin
	exec SP_Q_INFO_BY_VIDEOID '00003PL1' --日頭下•月光光(30分鐘版) 122 FCSTATUS = '', update 3 times
end

if @case = 2017070312555 begin --到期被刪除--> 重轉
	select * from TBLOG_VIDEO where FSID = '2010113' and FNEPISODE = 30
	select * from TBPROG_M where FSPGMNAME like '%就是愛運動%'
	select * from TBPROG_D where FSPROG_ID = '2010113' and FNEPISODE = 30
	select * from TBPROG_D where FSPROG_ID = '2010113' and FDEXPIRE_DATE <> ''
end

if @case = 201707031809 begin --標記上傳後狀態未更新
	exec SP_Q_INFO_BY_VIDEOID '00003PQ3'
	exec SP_Q_INFO_BY_VIDEOID '00003PQA'
end

if @case = 201707041143 begin --已標記但是 TBLOG_VIDEO_HD_MC 無資料
	exec SP_Q_INFO_BY_VIDEOID '00003QHK' --201706300084, P201706009270001
	--補建資料
	exec SP_I_TBLOG_VIDEO_HD_MC '201706300084', 'P201706009270001', '00003QHK', '51204'

	--call web service  http://10.13.220.3/DataSource/WSPTS_AP.asmx?op=PTS_Status_MoveJob
end

if @case = 201707041347 begin
	select * from TBPROG_M where FSPGMNAME like '%爸媽%' --2009285
	select * from TBLOG_VIDEO where FSID = '2009285' and FNEPISODE = 550 --R, G200928505500014, 00003QEF, 165605, 201707030140
	select * from TBTRANSCODE where FNTRANSCODE_ID = 165605 --ANS_ID = 115651, 8, 2017-07-03 17:24:59.677
	exec SP_Q_INFO_BY_VIDEOID '00003QEF' --爸媽囧很大, 2017-07-13 ch13
end

if @case = 201707041424 begin --標記上傳後，插入短帶重新載入仍抓到舊的 VideoID
	exec SP_Q_INFO_BY_VIDEOID '00003QNQ' --2017 07/06 pts1 menu 18:00, 2017-07-06 ch12
	exec SP_Q_INFO_BY_VIDEOID '00003QHK' --重新載入仍抓到舊的 20170600927

	select * from TBLOG_VIDEOID_MAP where FSID = '20170600927' -- FSVIDEO_ID_PROG = 00003QNQ 舊的
    update TBLOG_VIDEOID_MAP set FSVIDEO_ID_PROG = '00003QHK' where FSID = '20170600927'
end

if @case = 201707041718 begin --狀態為 SC
	exec SP_Q_INFO_BY_VIDEOID '00003QNJ' --奧林P客, 875, 2017-07-04 ch07
end

if @case = 201707051429 begin --標記上傳後狀態未更新
	exec SP_Q_INFO_BY_VIDEOID '00003QNP' --2017福氣來了 125 
	exec SP_Q_INFO_BY_VIDEOID '00003PRR' --你所不知的創意科技 markupload 的檔名還未改
end

if @case = 201707060952 begin
	select * from TBTRACKINGLOG where FDTIME > '2017-07-06 10:05' order by FDTIME
	select * from TBTRAN_LOG where FDDATE > '2017/07/06 10:35' order by FDDATE
end

if @case = 201707061426 begin
	exec SP_Q_INFO_BY_VIDEOID '00003PS1' --R 他們在畢業的前一天爆炸2-角色篇王丁筑 2017-07-06 ch12 P201706007050001  done
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201706007050001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201706007050001'

	exec SP_Q_INFO_BY_VIDEOID '00003PS7' --R 他們在畢業的前一天爆炸2-長大篇-王丁筑 2017-07-06 ch12 P201706007070001 done
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201706007070001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201706007070001'

	exec SP_Q_INFO_BY_VIDEOID '00003PT2' --R 他們在畢業的前一天爆炸2-角色篇蔡成揖 2017-07-06 ch12 P201706002800004
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201706002800004'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201706002800004'

	exec SP_Q_INFO_BY_VIDEOID '00003OD6' --T 他們在畢業的前一天爆炸2-長大篇-蔡成揖 2017-07-06 ch12 P201706002810001
end


if @case = 201707071114 begin
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201728600010002' --T, 00003Q37
	exec SP_Q_INFO_BY_VIDEOID '00003Q37' --起鼓•出獅 1
end

if @case = 201707071358 begin --置換單被刪除，舊的單仍在用，但是帶置換清單會出現要置換
	select * from TBLOG_VIDEO where FSID = '2014756' and FNEPISODE = 16 --G201475600160012, (G201475600160014 201705120086 置換後又刪除）
end

if @case = 201707071423 begin --88
	exec SP_Q_INFO_BY_VIDEOID '00003R0S'
end

if @case = 201707071429 begin
	select * from TBTRACKINGLOG where FDTIME > '2017-07-07 14:20' order by FDTIME
end

if @case = 201707071459 begin --無法檢索
	select * from TBUSER_SEARCH_HISTORY where FDSEARCH_DATE > '2017/07/07'  order by FDSEARCH_DATE
	select * from TBTRAN_LOG where FDDATE > '2017/07/07' and FSSP_NAME = 'SP_I_TBUSER_SEARCH_HISTORY' order by FDDATE
	select * from TBSEARCH_KEYWORD where FDDATE = '2017/07/07' order by FNNO
end

if @case = 201707071746 begin
	select * from TBUSERS where FSUSER_ChtName = '莊海娣' --公服暨行銷部
end

if @case = 201707101425 begin --已標記上傳 TBLOG_VIDEO_HD_MC 無資料
	exec SP_Q_INFO_BY_VIDEOID '00003R4L' --P201707002270001, 201707100035, not schedule
	exec SP_I_TBLOG_VIDEO_HD_MC '201707100035', 'P201707002270001', '00003R4L', '51204'
	exec SP_Q_INFO_BY_VIDEOID '00003R4G' --201707100031, P201707002240001
	exec SP_I_TBLOG_VIDEO_HD_MC '201707100031', 'P201707002240001', '00003R4G', '51204'
end

if @case = 201707111112-201707071459 begin --無法檢索
	select * from TBUSER_SEARCH_HISTORY where FDSEARCH_DATE > '2017/07/07'  order by FDSEARCH_DATE
	select * from TBTRAN_LOG where FDDATE > '2017/07/07' and FSSP_NAME = 'SP_I_TBUSER_SEARCH_HISTORY' order by FDDATE
	select * from TBSEARCH_KEYWORD where FDDATE = '2017/07/07' order by FNNO

	select * from TBSEARCH_COLUMN_ENUM
	select * from TBSEARCH_KEYWORD where FDDATE = '2017/07/11' order by FNNO

	select * from TBLOG_VIDEO where FDCREATED_DATE > '2017-07-10' order by FDCREATED_DATE
	select FSFILE_NO, FSFILE_TYPE_HV, FDCREATED_DATE from TBLOG_VIDEO order by FDCREATED_DATE
	select * from TBLOG_VIDEO where FDUPDATED_DATE > '2017-07-10' order by FDUPDATED_DATE

	select * from TBLOG_VIDEO where FDCREATED_DATE between '2017-07-10 17:59' and '2017-07-10 18:16' order by FDCREATED_DATE --no record
	select * from TBLOG_VIDEO where FDUPDATED_DATE between '2017-07-10 17:59' and '2017-07-10 18:16' order by FDUPDATED_DATE --3
	select * from TBLOG_VIDEO where FSFILE_NO = 'G200936600270001'

	select * from TBLOG_VIDEO
	select FSFILE_NO from TBLOG_VIDEO group by FSFILE_NO having count(FSFILE_NO) > 1 --no record
	select FSSYS_ID, count([FSSYS_ID]) from VW_SEARCH_VIDEO group by [FSSYS_ID] having count([FSSYS_ID]) > 1 --P2017070023900010, 2
	select * from TBLOG_VIDEO where FSFILE_NO = 'P2017070023900010' --no record
	select * from TBLOG_VIDEO where FSFILE_NO = 'P201707002390001' -- 00003R7N
	exec SP_Q_INFO_BY_VIDEOID '00003R7N' --2017-07-12 ch12
	select * from TBLOG_VIDEO where FSFILE_NO like 'P20170700239000%' --P201707002390001 --T
	select * from VW_SEARCH_VIDEO where FSSYS_ID = 'P2017070023900010'
	select * from TBPGM_PROMO where FSPROMO_ID = '20170700239' --PTS1_關於花甲＿故事篇
	select * from TBLOG_VIDEO_D where FSID = '20170700239'
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'P201707002390001'
	select top 1000 * from VW_SEARCH_VIDEO

	--置換
	select * from TBLOG_VIDEO_D where FSID = '20170700239'  --FCFILE_STATUS D
	select FSSYS_ID, count([FSSYS_ID]) from VW_SEARCH_VIDEO group by [FSSYS_ID] having count([FSSYS_ID]) > 1 --no record
	select * from TBLOG_VIDEO where FSID = '20170700239' --P201707002390003 00003RHV
	exec SP_Q_INFO_BY_VIDEOID '00003RHV' --2017-07-14, ch12 T
	select count(FSSYS_ID) from VW_SEARCH_VIDEO

	select * from TBLOG_VIDEO where FCFILE_STATUS = 'T' and FDUPDATED_DATE > '2017-07-11'
end

if @case = 201707111116-201707101425 begin --已標記上傳 TBLOG_VIDEO_HD_MC 無資料 插入短帶仍用舊的
	exec SP_Q_INFO_BY_VIDEOID '00003R4L' --P201707002270001, 201707100035, not schedule FSID = 20170700227
	exec SP_I_TBLOG_VIDEO_HD_MC '201707100035', 'P201707002270001', '00003R4L', '51204'
	exec SP_Q_INFO_BY_VIDEOID '00003R4G' --201707100031, P201707002240001, 20170700224
	exec SP_I_TBLOG_VIDEO_HD_MC '201707100031', 'P201707002240001', '00003R4G', '51204'

	select * from TBPGM_ARR_PROMO where FSPROMO_ID = '20170700227' --2017-07-15, 12, 00003R56
	select * from TBLOG_VIDEOID_MAP where FSID = '20170700227' --00003R56 -> 00003R4L
	update TBLOG_VIDEOID_MAP set FSVIDEO_ID_PROG = '00003R4L' where FSID = '20170700227'

	select * from TBLOG_VIDEOID_MAP where FSID = '20170700224' --00003R55 -> 00003R4G
	update TBLOG_VIDEOID_MAP set FSVIDEO_ID_PROG = '00003R4G' where FSID = '20170700224'
end

if @case = 201707121403 begin --其他檔案入庫失敗 --> 檢索問題
	select * from TBUSERS where FSUSER = 'prg50850' --饒瑞軍 50850
	select * from TBLOG_VIDEO where FSCREATED_BY = '50850' and FDCREATED_DATE > '2017-07-06' and FSARC_TYPE = '026'  --7 皆已入庫
end

if @case = 201707121423 begin
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201707060039' --\\mamstream\MAMDFS\ptstv\HVideo\2017\2017952\G201795200010001.mxf
	select * from TBUSERS where FSUSER_ID = '60061' --bobohung 洪潤德
end

if @case = 201707121509 begin --2016世界碗池滑板大賽(第2集60') '的符號造成無法新增段落
	select * from TBUSERS where FSUSER_ChtName = '陳俊峰' --pub50149, 05088
	select * from TBLOG_VIDEO where FSCREATED_BY = '05088' and FDCREATED_DATE > '2017-07-11'
	select * from TBPROG_M where FSPGMNAME like '%2016世界碗池滑板大賽%' --2017533
	select * from TBLOG_VIDEO where FSID = '2017533' --no record
	select * from TBBROADCAST where FSID = '2017533' --no record
	select * from TBTRACKINGLOG where FDTIME > '2017-07-12 15:30' order by FDTIME
end


if @case = 201707121855-201707111112-201707071459 begin --無法檢索
	select * from TBUSER_SEARCH_HISTORY where FDSEARCH_DATE > '2017/07/07'  order by FDSEARCH_DATE
	select * from TBTRAN_LOG where FDDATE > '2017/07/07' and FSSP_NAME = 'SP_I_TBUSER_SEARCH_HISTORY' order by FDDATE
	select * from TBSEARCH_KEYWORD where FDDATE = '2017/07/07' order by FNNO

	select * from TBSEARCH_COLUMN_ENUM
	select * from TBSEARCH_KEYWORD where FDDATE = '2017/07/11' order by FNNO

	select * from TBLOG_VIDEO where FDCREATED_DATE > '2017-07-10' order by FDCREATED_DATE
	select FSFILE_NO, FSFILE_TYPE_HV, FDCREATED_DATE from TBLOG_VIDEO order by FDCREATED_DATE
	select * from TBLOG_VIDEO where FDUPDATED_DATE > '2017-07-10' order by FDUPDATED_DATE

	select * from TBLOG_VIDEO where FDCREATED_DATE between '2017-07-10 17:59' and '2017-07-10 18:16' order by FDCREATED_DATE --no record
	select * from TBLOG_VIDEO where FDUPDATED_DATE between '2017-07-10 17:59' and '2017-07-10 18:16' order by FDUPDATED_DATE --3
	select * from TBLOG_VIDEO where FSFILE_NO = 'G200936600270001'

	select * from TBLOG_VIDEO
	select FSFILE_NO from TBLOG_VIDEO group by FSFILE_NO having count(FSFILE_NO) > 1 --no record
	select FSSYS_ID, count([FSSYS_ID]) from VW_SEARCH_VIDEO group by [FSSYS_ID] having count([FSSYS_ID]) > 1 --P2017070023900010, 2
	select * from TBLOG_VIDEO where FSFILE_NO = 'P2017070023900010' --no record
	select * from TBLOG_VIDEO where FSFILE_NO = 'P201707002390001' -- 00003R7N
	exec SP_Q_INFO_BY_VIDEOID '00003R7N' --2017-07-12 ch12
	select * from TBLOG_VIDEO where FSFILE_NO like 'P20170700239000%' --P201707002390001 --T
	select * from VW_SEARCH_VIDEO where FSSYS_ID = 'P2017070023900010'
	select * from TBPGM_PROMO where FSPROMO_ID = '20170700239' --PTS1_關於花甲＿故事篇
	select * from TBLOG_VIDEO_D where FSID = '20170700239'
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'P201707002390001'
	select top 1000 * from VW_SEARCH_VIDEO

	--置換  轉檔完成後仍一樣
	select * from TBLOG_VIDEO_D where FSID = '20170700239'  --FCFILE_STATUS D
	select FSSYS_ID, count([FSSYS_ID]) from VW_SEARCH_VIDEO group by [FSSYS_ID] having count([FSSYS_ID]) > 1 --P2017070023900030, 2
	select * from TBLOG_VIDEO where FSID = '20170700239' --P201707002390003 00003RHV
	exec SP_Q_INFO_BY_VIDEOID '00003RHV' --2017-07-14, ch12 T
	select count(FSSYS_ID) from VW_SEARCH_VIDEO

	select * from TBLOG_VIDEO where FCFILE_STATUS = 'T' and FDUPDATED_DATE > '2017-07-11'

	select  ISNULL(A.FSFILE_NO,'')+ISNULL(CONVERT(varchar(10),A.FNSEQ_NO),'0') As FSSYS_ID,
		    ISNULL(A.FSFILE_NO,'') As FSFILE_NO, FNSEQ_NO
		from TBLOG_VIDEO_D A
		left join TBPROG_M B on A.FSID = B.FSPROG_ID
		left join TBPROG_D C on A.FNEPISODE=C.FNEPISODE AND B.FSPROG_ID=C.FSPROG_ID
		LEFT JOIN TBDIRECTORIES D ON A.FNDIR_ID = D.FNDIR_ID
		LEFT JOIN TBSUBJECT E ON A.FSSUBJECT_ID = E.FSSUBJECT_ID
	    LEFT JOIN TBZCHANNEL F ON A.FSCHANNEL_ID = F.FSCHANNEL_ID
		LEFT JOIN TBLOG_VIDEO G ON A.FSFILE_NO = G.FSFILE_NO
		LEFT JOIN TBFILE_TYPE H ON G.FSARC_TYPE = H.FSARC_TYPE
		where A.FSFILE_NO = 'P201707002390003'

	select *
		from TBLOG_VIDEO_D A
		left join TBPROG_M B on A.FSID = B.FSPROG_ID --20170700239
		left join TBPROG_D C on A.FNEPISODE=C.FNEPISODE AND B.FSPROG_ID=C.FSPROG_ID --20170700239, 0
		LEFT JOIN TBDIRECTORIES D ON A.FNDIR_ID = D.FNDIR_ID --56453
		LEFT JOIN TBSUBJECT E ON A.FSSUBJECT_ID = E.FSSUBJECT_ID --P20170700239
	    LEFT JOIN TBZCHANNEL F ON A.FSCHANNEL_ID = F.FSCHANNEL_ID --01
		LEFT JOIN TBLOG_VIDEO G ON A.FSFILE_NO = G.FSFILE_NO --P201707002390003
		LEFT JOIN TBFILE_TYPE H ON G.FSARC_TYPE = H.FSARC_TYPE --002
		where A.FSFILE_NO in  ('P201707002390003') -- G000006015710005

		select * from TBLOG_VIDEO_D where FSID = '20170700239' --2
		select * from TBPROG_M where FSPROG_ID = '20170700239' --no record
		select * from TBDIRECTORIES where FNDIR_ID = 56453
		select * from TBSUBJECT where FSSUBJECT_ID = 'P20170700239' --2
		select FSSUBJECT_ID from TBSUBJECT group by FSSUBJECT_ID having count(FSSUBJECT_ID) > 1 --P20170700239 2
		select ROW_NUMBER(), * from TBSUBJECT where FSSUBJECT_ID = 'P20170700239'

		select *, ROW_NUMBER() over(Partition by FSSUBJECT_ID order by FSSUBJECT_ID) as rowNumber from TBSUBJECT order by FDCREATED_DATE

		Declare @table table
			(col1 varchar(10),col2 int,col3 int, col4 int, col5 int, col6 int, col7 int)
			Insert into @table values 
			('john',1,1,1,1,1,1),
			('john',1,1,1,1,1,1),
			('sally',2,2,2,2,2,2),
			('sally',2,2,2,2,2,2)

		Delete  aliasName from (
			Select  *,
					ROW_NUMBER() over (Partition by col1,col2,col3,col4,col5,col6,col7 order by col1) as rowNumber
			From    @table) aliasName 
			Where   rowNumber > 1

			Select * from @table
end


-----------------------------20170717--------------------------------------

if @case = 201707170932 begin --已標記上傳 但插入仍用舊的 VideoID
	exec SP_Q_INFO_BY_VIDEOID '00003R4Q' --C, not schedule, 20170700231, 0
	select * from TBLOG_VIDEOID_MAP where FSID = '20170700231' --00003RHQ
	update TBLOG_VIDEOID_MAP set FSVIDEO_ID_PROG = '00003R4Q' where FSID = '20170700231'
	select * from TBLOG_VIDEO where FSID = '20170700231' --00003R4Q
end

if @case = 201707170940 begin --標記上傳後狀態未更新
	exec SP_Q_INFO_BY_VIDEOID '00003RI2' --FCSTATUS = '' --生態全紀錄-蒙古高原, 2017-07-22 ch12
	--call http://10.13.220.3/DataSource/WSPTS_AP.asmx?op=PTS_Status_MoveJob
end

if @case = 201707170949 begin
	select * from TBBOOKING_MASTER where FDBOOKING_DATE > '2017/07/13'
	select * from TBBOOKING_DETAIL where FDCHECK_DATE > '2017/07/13' and  FCFILE_TRANSCODE_STATUS <> 'Y'
end

if @case = 201707171015-201707121855-201707111112-201707071459 begin --無法檢索
	select * from TBUSER_SEARCH_HISTORY where FDSEARCH_DATE > '2017/07/07'  order by FDSEARCH_DATE
	select * from TBTRAN_LOG where FDDATE > '2017/07/07' and FSSP_NAME = 'SP_I_TBUSER_SEARCH_HISTORY' order by FDDATE
	select * from TBSEARCH_KEYWORD where FDDATE = '2017/07/07' order by FNNO

	select * from TBSEARCH_COLUMN_ENUM
	select * from TBSEARCH_KEYWORD where FDDATE = '2017/07/11' order by FNNO

	select * from TBLOG_VIDEO where FDCREATED_DATE > '2017-07-10' order by FDCREATED_DATE
	select FSFILE_NO, FSFILE_TYPE_HV, FDCREATED_DATE from TBLOG_VIDEO order by FDCREATED_DATE
	select * from TBLOG_VIDEO where FDUPDATED_DATE > '2017-07-10' order by FDUPDATED_DATE

	select * from TBLOG_VIDEO where FDCREATED_DATE between '2017-07-10 17:59' and '2017-07-10 18:16' order by FDCREATED_DATE --no record
	select * from TBLOG_VIDEO where FDUPDATED_DATE between '2017-07-10 17:59' and '2017-07-10 18:16' order by FDUPDATED_DATE --3
	select * from TBLOG_VIDEO where FSFILE_NO = 'G200936600270001'

	select * from TBLOG_VIDEO
	select FSFILE_NO from TBLOG_VIDEO group by FSFILE_NO having count(FSFILE_NO) > 1 --no record
	select FSSYS_ID, count([FSSYS_ID]) from VW_SEARCH_VIDEO group by [FSSYS_ID] having count([FSSYS_ID]) > 1 --P2017070023900010, 2
	select * from TBLOG_VIDEO where FSFILE_NO = 'P2017070023900010' --no record
	select * from TBLOG_VIDEO where FSFILE_NO = 'P201707002390001' -- 00003R7N
	exec SP_Q_INFO_BY_VIDEOID '00003R7N' --2017-07-12 ch12
	select * from TBLOG_VIDEO where FSFILE_NO like 'P20170700239000%' --P201707002390001 --T
	select * from VW_SEARCH_VIDEO where FSSYS_ID = 'P2017070023900010'
	select * from TBPGM_PROMO where FSPROMO_ID = '20170700239' --PTS1_關於花甲＿故事篇
	select * from TBLOG_VIDEO_D where FSID = '20170700239'
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'P201707002390001'
	select top 1000 * from VW_SEARCH_VIDEO

	--置換  轉檔完成後仍一樣
	select * from TBLOG_VIDEO_D where FSID = '20170700239'  --FCFILE_STATUS D
	select FSSYS_ID, count([FSSYS_ID]) from VW_SEARCH_VIDEO group by [FSSYS_ID] having count([FSSYS_ID]) > 1 --P2017070023900030, 2
	select * from TBLOG_VIDEO where FSID = '20170700239' --P201707002390003 00003RHV
	exec SP_Q_INFO_BY_VIDEOID '00003RHV' --2017-07-14, ch12 T
	select count(FSSYS_ID) from VW_SEARCH_VIDEO

	select * from TBLOG_VIDEO where FCFILE_STATUS = 'T' and FDUPDATED_DATE > '2017-07-11'

	select  ISNULL(A.FSFILE_NO,'')+ISNULL(CONVERT(varchar(10),A.FNSEQ_NO),'0') As FSSYS_ID,
		    ISNULL(A.FSFILE_NO,'') As FSFILE_NO, FNSEQ_NO
		from TBLOG_VIDEO_D A
		left join TBPROG_M B on A.FSID = B.FSPROG_ID
		left join TBPROG_D C on A.FNEPISODE=C.FNEPISODE AND B.FSPROG_ID=C.FSPROG_ID
		LEFT JOIN TBDIRECTORIES D ON A.FNDIR_ID = D.FNDIR_ID
		LEFT JOIN TBSUBJECT E ON A.FSSUBJECT_ID = E.FSSUBJECT_ID
	    LEFT JOIN TBZCHANNEL F ON A.FSCHANNEL_ID = F.FSCHANNEL_ID
		LEFT JOIN TBLOG_VIDEO G ON A.FSFILE_NO = G.FSFILE_NO
		LEFT JOIN TBFILE_TYPE H ON G.FSARC_TYPE = H.FSARC_TYPE
		where A.FSFILE_NO = 'P201707002390003'

	select *
		from TBLOG_VIDEO_D A
		left join TBPROG_M B on A.FSID = B.FSPROG_ID --20170700239
		left join TBPROG_D C on A.FNEPISODE=C.FNEPISODE AND B.FSPROG_ID=C.FSPROG_ID --20170700239, 0
		LEFT JOIN TBDIRECTORIES D ON A.FNDIR_ID = D.FNDIR_ID --56453
		LEFT JOIN TBSUBJECT E ON A.FSSUBJECT_ID = E.FSSUBJECT_ID --P20170700239
	    LEFT JOIN TBZCHANNEL F ON A.FSCHANNEL_ID = F.FSCHANNEL_ID --01
		LEFT JOIN TBLOG_VIDEO G ON A.FSFILE_NO = G.FSFILE_NO --P201707002390003
		LEFT JOIN TBFILE_TYPE H ON G.FSARC_TYPE = H.FSARC_TYPE --002
		where A.FSFILE_NO in  ('P201707002390003') -- G000006015710005

		select * from TBLOG_VIDEO_D where FSID = '20170700239' --2
		select * from TBPROG_M where FSPROG_ID = '20170700239' --no record
		select * from TBDIRECTORIES where FNDIR_ID = 56453
		select * from TBSUBJECT where FSSUBJECT_ID = 'P20170700239' --2
		select FSSUBJECT_ID from TBSUBJECT group by FSSUBJECT_ID having count(FSSUBJECT_ID) > 1 --P20170700239 2
		select ROW_NUMBER() OVER(partition by FSSUBJECT_ID order by FSSUBJECT_ID) MYROW,* from TBSUBJECT where FSSUBJECT_ID = 'P20170700239'

		select *, ROW_NUMBER() over(Partition by FSSUBJECT_ID order by FSSUBJECT_ID) as rowNumber from TBSUBJECT order by FDCREATED_DATE

		Declare @table table
			(col1 varchar(10),col2 int,col3 int, col4 int, col5 int, col6 int, col7 int)
			Insert into @table values 
			('john',1,1,1,1,1,1),
			('john',1,1,1,1,1,1),
			('sally',2,2,2,2,2,2),
			('sally',2,2,2,2,2,2)

		Delete  aliasName from (
			Select  *,
					ROW_NUMBER() over (Partition by col1,col2,col3,col4,col5,col6,col7 order by col1) as rowNumber
			From    @table) aliasName 
			Where   rowNumber > 1

			Select * from @table


		---------------------------------------------
		select ROW_NUMBER() OVER(order by FSSUBJECT_ID) MYROW,  * from TBSUBJECT --ROWNUMBER = MYROW = 90933
		select ROW_NUMBER() OVER(partition by FSSUBJECT_ID order by FSSUBJECT_ID) MYROW,  * from TBSUBJECT --MYROW = 1
		select ROW_NUMBER() OVER(partition by FSSUBJECT_ID order by FSSUBJECT_ID) MYROW, * from TBSUBJECT where MYROW > 1 --Error 無效的資料行名稱 'MYROW'

		--查出重複的資料
		select * from (select ROW_NUMBER() OVER(partition by FSSUBJECT_ID order by FSSUBJECT_ID) MYROW, * from TBSUBJECT) aliasName where MYROW > 1

		------TEST------
		if OBJECT_ID(N'tempdb.dbo.#tmp_TBSUBJECT', N'U') IS NOT NULL
			DROP TABLE #tmp_TBSUBJECT

		select * into #tmp_TBSUBJECT from TBSUBJECT
		select count(FSSUBJECT_ID) from #tmp_TBSUBJECT
        select * from (select ROW_NUMBER() OVER(partition by FSSUBJECT_ID order by FSSUBJECT_ID) MYROW, * from #tmp_TBSUBJECT) aliasName where MYROW > 1
		delete aliasName from (select ROW_NUMBER() OVER(partition by FSSUBJECT_ID order by FSSUBJECT_ID) MYROW, * from #tmp_TBSUBJECT) aliasName where MYROW > 1
		select * from (select ROW_NUMBER() OVER(partition by FSSUBJECT_ID order by FSSUBJECT_ID) MYROW, * from #tmp_TBSUBJECT) aliasName where MYROW > 1
		select count(FSSUBJECT_ID) from #tmp_TBSUBJECT


		--backup
		if OBJECT_ID(N'tmp_TBSUBJECT', N'U') IS NOT NULL
			DROP TABLE tmp_TBSUBJECT

		select * into tmp_TBSUBJECT from TBSUBJECT
		select * from tmp_TBSUBJECT
		delete from tmp_TBSUBJECT
		insert into tmp_TBSUBJECT select * from TBSUBJECT

		--刪除重複的資料
		select count(FSSUBJECT_ID) from TBSUBJECT
        select * from (select ROW_NUMBER() OVER(partition by FSSUBJECT_ID order by FSSUBJECT_ID) MYROW, * from TBSUBJECT) aliasName where MYROW > 1
		delete aliasName from (select ROW_NUMBER() OVER(partition by FSSUBJECT_ID order by FSSUBJECT_ID) MYROW, * from TBSUBJECT) aliasName where MYROW > 1
		select * from (select ROW_NUMBER() OVER(partition by FSSUBJECT_ID order by FSSUBJECT_ID) MYROW, * from TBSUBJECT) aliasName where MYROW > 1
		select count(FSSUBJECT_ID) from TBSUBJECT

		select FSSYS_ID, count([FSSYS_ID]) from VW_SEARCH_VIDEO group by [FSSYS_ID] having count([FSSYS_ID]) > 1 --no record
		--execute IncrenmentalIndex
end

if @case = 291707171233 begin
	exec SP_Q_INFO_BY_VIDEOID '00003RHF' --0723-pts3-2016長笛音樂節閉幕音樂會promo, 2017-07-18, ch14, 無送播單資料
	select * from TBPGM_PROMO where FSPROMO_NAME = '0723-pts3-2016長笛音樂節閉幕音樂會promo' --20170600572
	select * from TBLOG_VIDEO where FSID = '20170600572' --00003PC9
	exec SP_Q_INFO_BY_VIDEOID '00003PC9'
	select * from TBLOG_VIDEOID_MAP where FSID = '20170600572' --00003RHF -> 00003PC9
	update TBLOG_VIDEOID_MAP set FSVIDEO_ID_PROG = '00003PC9' where FSID = '20170600572'
end

if @case = 201707171241 begin --轉檔成功搬檔失敗
	exec SP_Q_INFO_BY_VIDEOID '00003QH8' --重返犯罪現場第14季 23, 2017-07-18 ch14 FCFILE_STATUS = S, 166509 -- reschedule
	select * from TBTRANSCODE where FNTRANSCODE_ID = 166509 --ANSID = 116455
	exec SP_Q_INFO_BY_VIDEOID '00003QH9' --重返犯罪現場第14季 24, 2017-07-19 ch14 FCFILE_STATUS = S, 166511 --reschedule done
	select * from TBTRANSCODE where FNTRANSCODE_ID = 166511 --ANSID = 116457, FNPROCESS)STATUS = 4  --reschedule done
end

if @case = 201707171434 begin --客台上傳問題
	exec SP_Q_INFO_BY_VIDEOID '00002SDP' --T, 湯姆歷險記(2015數位版), 2017-07-18 ch07 tapeonly
	exec SP_Q_INFO_BY_VIDEOID '00002YM9' --T, 花樹下的約定(大埔腔)(排播版) 6 2017-07-18 ch07 tapeonly

	exec SP_Q_INFO_BY_VIDEOID '00003RL9' --客家新聞雜誌(客家製播)-242集排播需注意 S, 166457 reschedule done
	select * from TBTRANSCODE where FNTRANSCODE_ID = 166457 --ANSID = 116413, 4

	--TSM 處理中 -> 重審 --> T 已可檢索
	exec SP_Q_INFO_BY_VIDEOID '00003P2R' --S 2017RM-客週求見 夏客藝起趣 164424(2) , 2017-07-17 ch07 P201706004720001 --> T
	select * from TBTRANSCODE where FNTRANSCODE_ID = 164424 --ANS = -1 FCPROCESS_STATUS = 2
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201706004720001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201706004720001'
	select * from TBTRANSCODE where FNTRANSCODE_ID = 166637 --4

	--標記上傳後狀態未更新
	exec SP_Q_INFO_BY_VIDEOID '00003RJ3' --客家戲曲 608 2017-07-26 ch07, FCSTATUS
	exec SP_Q_INFO_BY_VIDEOID '00003QDR' --G201000900160002, FCSTATUS = ''
	exec SP_Q_INFO_BY_VIDEOID '00003QDS' --G201000900220002, FCSTATUS = ''

	select * from TBLOG_VIDEO where FSFILE_NO = 'G200928505300001'
end

if @case = 201707171645 begin --搬檔失敗 -> done
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201652700680001' --S, 00003QG0, 166585
	exec SP_Q_INFO_BY_VIDEOID '00003QG0' --鬧熱打擂台(120分鐘版) 68 2017-07-29 ch07
	select * from TBTRANSCODE where FNTRANSCODE_ID = 166585 --4, ANSID = 116516 --reschedule done
end

-----------------------------------------20170718------------------------------------

if @case = 201707181159 begin --搬檔失敗
	select * from TBLOG_VIDEO where FSBRO_ID = '201605270147' --G201687300010004, S, 166243, 00002L4V
	select * from TBTRANSCODE where FNTRANSCODE_ID = 166243 --4
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201687300010004'
end

if @case = 201707181439 begin --調用失敗 user 看到  \\10.13.200.7
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201707120097' --Y 太平島 \\mamstream\MAMDFS\ptstv\HVideo\2017\2017775\G201777500010001.mxf tapeonly
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201707120099' --Y 風景畫 \\mamstream\MAMDFS\ptstv\HVideo\2016\2016876\G201687600010009.mxf nearline
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201707120100' --Y 鐵皮人生 \\mamstream\MAMDFS\ptstv\HVideo\2016\2016470\G201647000010006.mxf
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201707130139' --翡翠之城 \\mamstream\MAMDFS\ptstv\HVideo\2017\2017730\G201773000010001.mxf
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201707130141' --太平島 \\mamstream\MAMDFS\ptstv\HVideo\2017\2017775\G201777500010001.mxf
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201707130142' --風景畫 \\mamstream\MAMDFS\ptstv\HVideo\2016\2016876\G201687600010009.mxf

	select * from TBBOOKING_MASTER where FSUSER_ID = '60126'
	select * from TBBOOKING_MASTER A left join TBBOOKING_DETAIL B on A.FSBOOKING_NO = B.FSBOOKING_NO where A.FSUSER_ID = '60126' and B.FCFILE_TRANSCODE_STATUS <> 'Y'
	select * from TBUSERS where FSUSER_ID = '60126' --prg60126, 江靜儀
end

if @case = 201707181615 begin --轉檔成功搬檔失敗 reschedule --> done
	exec SP_Q_INFO_BY_VIDEOID '00003ROI' --我們的島 915 2017-07-20 ch13, S, 166666
	select * from TBTRANSCODE where FNTRANSCODE_ID = 166666 --ANSID = 116581, 4
end

if @case = 201707181629 begin --標記上傳後狀態未更新
	exec SP_Q_INFO_BY_VIDEOID '00002XBC' --FCSTATUS = ''
	exec SP_Q_INFO_BY_VIDEOID '00002XBL' --FCSTATUS = ''
end

-------------------------------------20170719----------------------------------------

if @case = 201707190947 begin --reschedule x 3
	exec SP_Q_INFO_BY_VIDEOID '00003N2Z' --夏風(口述影像版) 2017-07-22 ch13 S, 166230, G201778800010001
	select * from TBTRANSCODE where FNTRANSCODE_ID = 166230 --4 ANSID = 116198
end

if @case = 201707191136 begin --標記上傳後狀態未更新
	exec SP_Q_INFO_BY_VIDEOID '00003RMR' --花甲男孩轉大人 6
	select * from TBPROG_M where FSPROG_ID like '2016929%'
end

if @case = 201707191141 begin --入庫上傳照片沒有上傳清單出現  用  10.13.220.2 照片上傳後最後會被刪除 如果用 10.13.220.3  照片仍會上傳到 UploadFoldr 但是照片會一直存在
	select * from TBPROG_M where FSPROG_ID like '2016929%' --我的陌生爸爸
end

if @case = 201707191411 begin --調用卡在 TSM 處理中
	select * from TBLOG_VIDEO where FSFILE_NO = 'G200500700010003' --\\mamstream\MAMDFS\ptstv\HVideo\2005\2005007\G200500700010003.mxf E01674L4
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201707180021' --08303
	select * from TBBOOKING_MASTER where FSUSER_ID = '08303' order by  FSBOOKING_NO
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201707070011'
end

if @case = 201707202030 begin --製作單位填單後無法在MAM系統內查到此單
	exec SP_Q_INFO_BY_VIDEOID '00003S1P' --G201244401180001 Bilibolo唱唱跳跳 118 2017-07-23 ch07 201707200045
	select * from TBBROADCAST where FSBRO_ID = '201707200045'  --no data
	-- 刪除轉檔單及段落資料
	exec SP_D_TBLOG_VIDEO_BYFILENO 'G201244401180001'
	select * from TBLOG_VIDEO where FSID = '2012444' and FNEPISODE = 118 --00003S3L
	exec SP_Q_INFO_BY_VIDEOID '00003S3L' --51063
	select * from TBUSERS where FSUSER_ID = '51063' --黃安慈
end

if @case = 201707202043 begin
	select * from TBLOG_VIDEO where FSBRO_ID = '201707190173'-- G201759600010001 00003RZU
	exec SP_Q_INFO_BY_VIDEOID '00003RZU' --2016長笛音樂藝術節閉幕音樂會, 2017-07-23, ch14 FCSTATUS = ''
end


-----------------------------------------20170724-------------------------------------
if @case = 201707240844 begin --入庫其他格式檔案出現空間已滿 10.13.200.5
	select * from TBLOG_VIDEO where FSARC_TYPE = '026' order by FDCREATED_DATE
end


if @case = 201707241046 begin --強制停止
	exec SP_Q_INFO_BY_VIDEOID '00003R0S' --T 環遊世界野餐音樂會, P201707001970001 ch07
	exec SP_Q_INFO_BY_VIDEOID '00003ROS' --ch01 T
end



if @case = 201707240907 begin
	select * from TBLOG_VIDEO where FSBRO_ID = '201707180070' --G201540400910001, 00003RTA
	exec SP_Q_INFO_BY_VIDEOID '00003RTA' --一字千金 91 2017-07-27 ch62
	select * from TBBROADCAST where FSBRO_ID = '201707180070' --no data
	exec SP_D_TBLOG_VIDEO_BYFILENO 'G201540400910001'
end

if @case = 201707241155 begin --標記上傳後未更新狀態
	exec SP_Q_INFO_BY_VIDEOID '00003S9E' --客家戲曲 569 2017-08-03 ch07
	exec SP_Q_INFO_BY_VIDEOID '00003S9F' --客家戲曲 570 2017-08-04 ch07
end

if @case = 201707241219 begin --標記上傳後未更新狀態
	exec SP_Q_INFO_BY_VIDEOID '00003GT0' --PTS3台歡迎光臨第22集 2017-07-25 ch12
end

if @case = 201707241702 begin --S rescheduling -> done
	exec SP_Q_INFO_BY_VIDEOID '00003RTN' --我們的島 915 166771 2017-07-24 ch12
	select * from TBTRANSCODE where FNTRANSCODE_ID = 166771 --116673, 4
end

if @case = 201707241709 begin --世紀的寶貝 只需建立 59, 61 送播單
	select * from TBLOG_VIDEO where FSID = '2007342' and FNEPISODE between 59 and 64 --已有送播單 60, 62, 63, 64, 59 為送帶轉檔單
	select * from TBBROADCAST where FSBRO_ID = '201707210018' --61 資料不完整
	select * from TBBROADCAST where FSBRO_ID = '201512300037' --59
	select * from TBLOG_VIDEO A left join TBBROADCAST B on A.FSBRO_ID = B.FSBRO_ID  where A.FSID = '2007342' and A.FNEPISODE between 59 and 64 
end

if @case = 201707241843 begin --S rescheduleing -> done
	select * from TBLOG_VIDEO where FSBRO_ID = '201707180012' --S, G201796400010001, 166752, 00003RQL 我的奶奶20歲
	exec SP_Q_INFO_BY_VIDEOID '00003RQL'
	select * from TBTRANSCODE where FNTRANSCODE_ID = 166752 --ANSID = 116654
end

-------------------------20170725-----------------------
if @case = 201707250928 begin
	select * from TBLOG_VIDEO where FCFILE_STATUS = 'T'
	select FSFILE_NO + '.' + FSFILE_TYPE_HV  + ',' +  FSFILE_NO + '.' +  FSFILE_TYPE_LV from TBLOG_VIDEO where FCFILE_STATUS = 'T' and FDUPDATED_DATE > '2017-01-01'
	--分軌帶或其他影片沒有低解
end

if @case = 201707251520 begin
	select * from TBTRACKINGLOG where FDTIME > '2017-07-25' and FSPROGRAM like '%GetVolumeNameByTsmPath%'
	--DB2查詢：SELECT VOLUME_NAME FROM CONTENTS WHERE FILESPACE_NAME = '/ptstv' AND FILE_NAME = '/HVideo/0000/0000216/G000021604640007.mxf' AND STGPOOL NOT LIKE '%CP'
	--SELECT VOLUME_NAME FROM CONTENTS WHERE FILESPACE_NAME = '{0}' AND FILE_NAME = '{1}' AND STGPOOL NOT LIKE '%CP'  ->  沒有 STGPOOL 欄位
	select * from TBSYSTEM_CONFIG
end

if @case = 201707251533 begin
	select * from TBLOG_VIDEO where FSID = '2014756' and FNEPISODE = 31 --有 4 筆資料 但 user 選到已刪除的
	select * from TBPROG_M where FSPROG_ID = '2014756' --浩克慢遊
end

if @case = 20170251621 begin --公視英語新聞 要修改每集時長 15->13
	select * from TBLOG_VIDEO where FSID = '2017828'
	select * from TBPROG_M where FSPROG_ID = '2017828'
	select * from TBPROG_D where FSPROG_ID = '2017828'
end

if @case = 201707251806 begin -- 關鍵影格是綠色
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201790500010001'
	select * from TBPROG_M where FSPROG_ID = '2017905' --吉娃斯愛科學
	select * from TBLOG_VIDEO where FSID = '2017905'
end


if @case = 201707251846 begin
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201401100140020' --S
end

if @case = 201707261005 begin --QC 轉檔卡在  TSM 處中--> done
	select * from TBLOG_VIDEO where FSBRO_ID = '201707040009' --G201753100010001, 00003QOE, S, FSJOB_ID = 166155, FSTRACK = MMMMMMMM
	select * from TBTRANSCODE where FNTRANSCODE_ID = 166155 -- FNANYSTREAM_ID = -1 FDSTART_TIME = 2017-07-10 12:22:46.090
	select * from TBPROG_M where FSPROG_ID = '2017531' --2016世界登山越野腳踏車大賽
	exec SP_Q_INFO_BY_VIDEOID '00003QOE' --2016世界登山越野腳踏車大賽

    --重新審核
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201753100010001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201753100010001'
end


if @case = 201707261140 begin --ANS 轉檔完成 MAM 未收到通知
	select * from TBLOG_VIDEO where FSBRO_ID = '201707260006' --G201693300010014, S, 00003SIM, 167309
	select * from TBTRANSCODE where FNTRANSCODE_ID = 167309 --FNPROCESS_STATUS = 3 --ANSID = 117178
	select * from TBTRANSCODE where FNTRANSCODE_ID = 117199 --4

	exec SP_Q_INFO_BY_VIDEOID '00003SIM' --not schedule
end

if @case = 201707261440 begin --Audio Tools
	exec SP_Q_INFO_BY_VIDEOID '00003P4X' --快樂新住民
	exec SP_Q_INFO_BY_VIDEOID '00003OQC' --快樂新住民
end

-----------------------------------20170727---------------------------------------
if @case = 201707271739 begin --送播單備註的檔案名稱無法選
	select * from TBUSERS where FSUSER_ChtName = '郭品慈' --70484, natashakuo
	select * from TBLOG_VIDEO where FSUPDATED_BY = '70484'
end

if @case = 201707271857 begin -- HSM4 : netstat -tnpc --> Send-Q = 126
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201719400130006' --S 00003SN7
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201774400050011' --T
end

if @case = 201707280902 begin --2017194 歡迎光臨 15 轉檔失敗 註冊 Anystream 時發生錯誤  done
	select * from TBLOG_VIDEO where FSBRO_ID = '201705310083' --G201719400150001, R, 163618, 00003NL1
	exec SP_Q_INFO_BY_VIDEOID '00003NL1'
	select * from TBTRANSCODE where FNTRANSCODE_ID = 163618 --8 2017-06-05 15:12:44.577 ANS ID = 117317
	--cp 00003NL1.mxf to approved
	--重新審核
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201719400150001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201719400150001'

	select * from TBTRANSCODE where FNTRANSCODE_ID = 167481 --ANSID = 117327
end

if @case = 201707281116 begin --HSM4 能，比較 3 支 QC 轉檔，和 2 支回朔轉檔
	exec SP_Q_INFO_BY_VIDEOID '00003SOE' --QC T
	exec SP_Q_INFO_BY_VIDEOID '00003SO4' --QC T
	exec SP_Q_INFO_BY_VIDEOID '00003SN6' --QC T
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201622700200009' --00003SQY
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201622700190009' --00003SQX
	exec SP_Q_INFO_BY_VIDEOID '00003SQY' --T
	exec SP_Q_INFO_BY_VIDEOID '00003SQX' --T
end

if @case = 201707281559 begin --尚未標記
	exec SP_Q_INFO_BY_VIDEOID '00003SQP' --P201707005810002 201707270164
end

if @case = 201707281629 begin --7/31 PTS2 會跑出帶置換清單
	exec SP_Q_INFO_BY_VIDEOID '00001V6A' --00001V6A 下課花路米(1323集起HD可播) 1388 -G000081613880003 201505070232
	select * from TBLOG_VIDEO where FSID = '0000816' and FNEPISODE = 1388 --G000081613880005 置換 00002ZCK 201609130083 <- 201505070232
	select * from TBLOG_VIDEO where FSFILE_NO = 'G000081613880003' --00001V6A 回朔
	select * from TBBROADCAST where FSBRO_ID = '201609130083'  --送帶轉檔單
end


if @case = 201707311415 begin  --轉檔卡在 TSM 處理中 done
	exec SP_Q_INFO_BY_VIDEOID '00003P4P' --快樂新住民 10 2017-08-11 ch08 G201789500100001, S, 00003P4P
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201789500100001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201789500100001'
end

if @case = 201707311523 begin --調用失敗
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201707280052' --167514, 科技風向 7 \\mamstream\MAMDFS\ptstv\HVideo\2016\2016545\G201654500070003.mxf tapeonly
	select * from TBTRANSCODE where FNTRANSCODE_ID = 167514 --7
end

if @case = 201707311633 begin --低解有問題，重轉關鍵影格有問題 done
	exec SP_Q_INFO_BY_VIDEOID '00003RZE' --花甲男孩轉大人 9 2017-08-02 ch12, G201767000090001, 201707190142  
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G201767000090001' --01:00:00;00 ~ 01:12:42;00, 01:12:52;00 ~ 01:22:43;00
	-- \\mamstream\MAMDFS\ptstv\HVideo\2017\2017670\G201767000090001.mxf
	-- \\mamstream\MAMDFS\Media\Lv\ptstv\2017\2017670\G201767000090001.mp4  --低解檔案有問題
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201767000090001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201767000090001'
end

if @case = 201707311816 begin --標記上傳後未更新狀態
	exec SP_Q_INFO_BY_VIDEOID '00003SXG' --他們在畢業的前一天爆炸2-巫建和訪問, 2017-08-02 ch12
end


if @case = 201708011404 begin
	exec SP_Q_WELFAREPROMO_PLAY_COUNT_BY_DATE '07', '2017-01-01', '2017-06-30'  --播出次數
	exec SP_Q_WELFAREPROMO_PLAYOUT_BY_DATE '07', '2017-01-01', '2017-06-30', '1' --播出紀錄
	select * from TBPGM_PROMO where FSWELFARE = '3599' --2017公益-105年工商普查--健檢篇, 20170301070, FSMAIN_CHANNEL_ID = 01
	select * from TBPGM_ARR_PROMO where FSPROMO_ID = '20170301070' order by FDDATE

	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, B.FSWELFARE 公益編號, COUNT(A.FSPROMO_ID) 播出次數 from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID 
	join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where B.FSMAIN_CHANNEL_ID = '07' and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') and B.FSWELFARE <> ''
	and A.FDDATE between '2017-01-01' and '2017-06-30'  group by A.FSPROMO_ID, B.FSPROMO_NAME, B.FSWELFARE order by A.FSPROMO_ID
end


if @case = 201708021046 begin --10.13.210.5 搬檔程式停掉
	exec SP_Q_INFO_BY_VIDEOID '00003SY5' --循環帶-宜蘭省道台九線
	exec SP_Q_INFO_BY_VIDEOID '00003SY6'
	exec SP_Q_INFO_BY_VIDEOID '00003SY7'
	exec SP_Q_INFO_BY_VIDEOID '00003SY8'
	exec SP_Q_INFO_BY_VIDEOID '00003SY9'
end

if @case = 201708021049 begin --標記上傳後未更新狀態
	exec SP_Q_INFO_BY_VIDEOID '00003SXZ' --花甲男孩轉大人 11
end

if @case = 201708021225 begin --hakka 8/14 playlist 無法選到 已被刪除
	exec SP_Q_INFO_BY_VIDEOID '00003T8D' --客庄好味道 #176關西promo, 20170800057
	select * from TBPGM_PROMO_BOOKING where FSPROMO_ID = '20170800057' --FCSTATUS = R FNPROMO_BOOKING_NO=47758
	select * from TBPGM_PROMO_EFFECTIVE_ZONE where FNPROMO_BOOKING_NO = 47758
	select * from TBPGM_PROMO where FSPROMO_ID = '20170800057' --FSDEL = Y 2017-08-02 11:38:05.320
	select * from TBPGM_PROMO where FSPROMO_ID = '20170800065' --2017-08-02 11:39:51.103
	select * from TBLOG_VIDEO where FSID = '20170800065'
	select * from TBLOG_VIDEO where FSID = '20170800057' --201708020026 2017-08-02 11:39:55.077
end

if @case = 201708021418 begin --no louth db
	exec SP_Q_INFO_BY_VIDEOID '000032QA' --爸媽囧很大 565
	select * from TBPGM_COMBINE_QUEUE where FSPROG_ID = '2009285' and FNEPISODE = 565 order by FDDATE --2017-08-01 ch13

	exec SP_Q_INFO_BY_VIDEOID '00003ST1' --小O事件簿
end

if @case = 201708031222 begin --標記上傳問題
	exec SP_Q_INFO_BY_VIDEOID '00003T8S' --南部開講(2017) 30 FCSTATUS = 88  201708020038
	exec SP_Q_INFO_BY_VIDEOID '00003S3Z' --就是這Young!
end

if @case = 201708031500 begin
	exec SP_Q_INFO_BY_VIDEOID '0000265E' --TBLOG_VIDEO 有2 筆資料 video id 一樣 , 科技•靈感•大自然  ch14 2017-08-13, 201510300112, G201630300010004
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201630300010004'
end

if @case = 201708031728 begin
	exec SP_Q_INFO_BY_VIDEOID '00003TBG' --檔案已到 Approved 但是未到 MACRO
end

if @case = 201708041041 begin --dsmrecall fail
	select * from TBLOG_VIDEO where FSFILE_NO = 'G000006015370001' --\\mamstream\MAMDFS\ptstv\HVideo\0000\0000060\G000006015370001.mxf, /ptstv/HVideo/0000/0000060/G000006015370001.mxf
end

if @case = 201708041042 begin --調用卡在 ANS 處理中 磁帶已下架
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201707240117' --R, 167880 網際大戰序曲：危機四伏(主題之夜導讀版) \\mamstream\MAMDFS\ptstv\HVideo\2016\2016972\G201697200010002.mxf
	--/ptstv/HVideo/2016/2016972/G201697200010002.mxf
	select * from TBTRANSCODE where FNTRANSCODE_ID = 167880 --ANSID=117635, 3, 2017-08-03 10:39:05.373 anystream 處理中 
	select * from TBUSERS where FSUSER_ID = '71264'
end

if @case = 201708041047 begin --磁帶下架
	exec SP_Q_INFO_BY_VIDEOID '000035WD' --PTS3 RM【公視表演廳】2011霹靂魔幻交響詩音樂會 2017-08-07 ch14
end

if @case = 201708041603 begin --轉檔成功搬檔失敗
	exec SP_Q_INFO_BY_VIDEOID '00003TET' --2017客庄走透透 62 S G201756000620001 167994 --reschedule
	select * from TBTRANSCODE where FNTRANSCODE_ID = 167994 --117713, 4 2017-08-03 18:22:35.563
	exec SP_Q_INFO_BY_VIDEOID '00003TF2' --2017客庄走透透 63 S 168005 --reschedule
	select * from TBTRANSCODE where FNTRANSCODE_ID = 168005 --117724, 4, 2017-08-03 20:14:17.230
end


if @case = 201708041733 begin --prg1159
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201708030091'
	/*
	
	\\mamstream\MAMDFS\ptstv\HVideo\2014\2014756\G201475600010016.mxf, F partial 處理錯誤 offline  E01818L4 done
	\\mamstream\MAMDFS\ptstv\HVideo\2014\2014756\G201475600020013.mxf offline 
	\\mamstream\MAMDFS\ptstv\HVideo\2014\2014756\G201475600060015.mxf offline
	\\mamstream\MAMDFS\ptstv\HVideo\2014\2014756\G201475600070018.mxf nearline, F, 167943, partial 處理錯誤, done
	\\mamstream\MAMDFS\ptstv\HVideo\2014\2014756\G201475600080014.mxf nearline. F partial 處理錯誤, done
	\\mamstream\MAMDFS\ptstv\HVideo\2014\2014756\G201475600030013.mxf offline
	\\mamstream\MAMDFS\ptstv\HVideo\2014\2014756\G201475600040011.mxf offline. F 167946 partial 處理錯誤 done
	\\mamstream\MAMDFS\ptstv\HVideo\2014\2014756\G201475600050012.mxf offline
	\\mamstream\MAMDFS\ptstv\HVideo\2014\2014756\G201475600110002.mxf nearline, F done
	\\mamstream\MAMDFS\ptstv\HVideo\2014\2014756\G201475600170014.mxf offline, F, 167968, partial 處理錯誤 recall
	\\mamstream\MAMDFS\ptstv\HVideo\2014\2014756\G201475600190002.mxf offline, F, 167969, partial 處理錯誤 recall
	\\mamstream\MAMDFS\ptstv\HVideo\2014\2014756\G201475600130019.mxf offline, F, 167970, partial 處理錯誤 done
	\\mamstream\MAMDFS\ptstv\HVideo\2014\2014756\G201475600140002.mxf offline
	\\mamstream\MAMDFS\ptstv\HVideo\2014\2014756\G201475600150002.mxf offline
	\\mamstream\MAMDFS\ptstv\HVideo\2014\2014756\G201475600200002.mxf offline
	*/

	select * from TBTRANSCODE where FNTRANSCODE_ID = 167943 --2017-08-03 15:35:00.923
	select * from TBTRANSCODE where FNTRANSCODE_ID = 167940 --7, 2017-08-03 15:35:00.393

	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201708030093' --done
	--台灣心動線 357 R \\mamstream\MAMDFS\mvtv\HVideo\2006\2006850\G200685003570001.mxf
	--台灣心動線 364 R \\mamstream\MAMDFS\mvtv\HVideo\2006\2006850\G200685003640001.mxf done
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201708040047' --台灣心動線 307 R \\mamstream\MAMDFS\mvtv\HVideo\2006\2006850\G200685003070001.mxf done
end

if @case = 201708071059 begin --已標記上傳無法抽單
	select * from TBLOG_VIDEO where FSBRO_ID = '201707210015' --G201644200220001, 00003S40
	exec SP_D_TBLOG_VIDEO_BYFILENO 'G201644200220001'
end

if @case = 201708071656 begin --百變小露露 27 檢索有兩筆資料
	select * from TBLOG_VIDEO where FSID = '2015496' and FNEPISODe = 27
	--G201549600270003 F 00001SKS 201507170030 --> 審核不過 50311 \\mamstream\MAMDFS\ptstv\HVideo\2015\2015496\G201549600270003.mxf 檔案仍在
	--G201549600270004 T 00001SKS 201604180010 
	select * from TBUSERS where FSUSER_ID = '50311' --鄒立文
	select * from TBBROADCAST where FSBRO_ID = '201507170030' --50598
	select * from TBUSERS where FSUSER_ID = '50598'
	exec SP_Q_INFO_BY_VIDEOID '00001SKS' --百變小露露

	select * from TBLOG_VIDEO_D where FSID = '2015496' and FNEPISODe = 27
	--G201549600270003 T
	--G201549600270004 T
	update TBLOG_VIDEO_D set FCFILE_STATUS = 'X' where FSFILE_NO = 'G201549600270003'
end

if @case = 201708071729 begin --
	select * from TBLOG_VIDEO where FSID = '2017095' and FNEPISODE = 1 --1 筆紀錄 00003LW1
	select * from TBLOG_VIDEO_D where FSID = '2017095' and FNEPISODE = 1 -- 1 筆紀錄
	exec SP_Q_INFO_BY_VIDEOID '00003LW1' --哈客狂潮音樂會(客委會捐贈)
end


if @case = 201708091020 begin
	select * from TBTRACKINGLOG where FDTIME > '2017-08-09 09:30' order by FDTIME
end

if @case = 201708091435 begin --響度併入MAM 流程測試
	select * from TBPGM_PROMO where FSPROMO_NAME = '波西小測試'
	exec SP_Q_INFO_BY_VIDEOID '00003TVT'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'C' where FSFILE_NO = 'G201323200160002'
	exec SP_Q_INFO_BY_VIDEOID '00003TV5'
end

if @case = 201708091442 begin --音樂萬萬歲3 #48 轉檔成功搬檔失敗
	exec SP_Q_INFO_BY_VIDEOID '00003RM4' --G201558600480012, 201707140004, 2015586 #48, 166470 donw
	select * from TBBROADCAST where FSBRO_ID = '201707140004'
	select * from TBTRANSCODE where FNTRANSCODE_ID = 166470 --4, 116426, 2017-07-14 08:53:07.697
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201558600480012'
	select * from TBPROG_M where FSPROG_ID = '2015586' --音樂萬萬歲3

	exec SP_Q_INFO_BY_VIDEOID '00002IHO' --音樂萬萬歲3 #48 2017-08-13 --T 磁帶已下架
end


if @case = 201708091727 begin
	select * from TBLOG_VIDEO where FSBRO_ID = '201708030196' --G200928505770013, 167997, 00003TFB
	exec SP_Q_INFO_BY_VIDEOID '00003TFB' --爸媽囧很大 577, 2017-08-16
	select * from TBTRANSCODE where FNTRANSCODE_ID = 167997 --4
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G200928505770013'  --重搬 x 2 done
end

if @case = 201708091812 begin --檔案尚未到  MACRO
	exec SP_Q_INFO_BY_VIDEOID '00003TMM' --(官)臺灣名廚紐澳巡迴 2017-08-12
end

if @case = 201708100955 begin --其他格式入庫檢索不到
	select * from TBLOG_VIDEO where FDCREATED_DATE > '2017-08-09' and FSARC_TYPE = '026' order by FDCREATED_DATE
	--check APP_MoveOtherVideoFile
	--作客他鄉(205)HD 分軌帶.MXF 檔案找不到 -> 作客他鄉(205)HD分軌檔.MXF
	--作客他鄉(209)HD分軌檔.MXF -->  作客他鄉(209)HD分軌.MXF
end

if @case = 201708101031 begin --誰來晚餐7 #25 無法 recall
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201554800250001' --2015548, 000023GB
	exec SP_Q_INFO_BY_VIDEOID '000023GB' --2015548, 25
	select * from TBPROG_M where FSPROG_ID = '2015548'
end

if @case = 201708101158 begin --檔案未搬
	exec SP_Q_INFO_BY_VIDEOID '00003SX4' --PTS3_公視植劇場 荼蘼_台北篇起始版_30秒 2017-08-14 ch14
end

if @case = 201708101346 begin
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201708090053'
	--R \\mamstream\MAMDFS\ptstv\HVideo\2013\20130800197\D201308001970001.mxf offline
	--R \\mamstream\MAMDFS\ptstv\HVideo\2014\20140200002\D201402000020001.mxf offline
end

if @case = 201708101436 begin
	exec SP_Q_INFO_BY_VIDEOID '00003TY6' --PTS3台歡迎光臨第24集, 2017-08-12,27:00:16:13, ch14 舊的編號
	exec SP_Q_INFO_BY_VIDEOID '00003GT4' --P201702003430001 ,20170200343, PTS3台歡迎光臨第24集, 201703200067
	select * from TBLOG_VIDEOID_MAP where FSID = '20170200343' --00003TY6 -> 00003GT4
	update TBLOG_VIDEOID_MAP set FSVIDEO_ID_PROG = '00003GT4' where FSID = '20170200343'
end

if @case = 201708101452 begin -- 響度測試失敗檔案卡在 ATS_IN
	exec SP_Q_INFO_BY_VIDEOID '00003TX9'
end

if @case = 201708101510 begin  --科技•靈感•大自然 1~3 調用檔案已下架
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201708070115'
	--R \\mamstream\MAMDFS\ptstv\HVideo\2016\2016303\G201630300010007.mxf offline E01654L4
	--R \\mamstream\MAMDFS\ptstv\HVideo\2016\2016303\G201630300020002.mxf offline E01666L4
	--R \\mamstream\MAMDFS\ptstv\HVideo\2016\2016303\G201630300030003.mxf offline E01676L4
	select * from TBUSERS where FSUSER_ID = '50391' --prg50391
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201630300020002'
	select * from TBLOG_VIDEO where FSID = '2016303'
end

if @case = 201708101601 begin --2016-17 歐洲冠軍足球聯賽卡在 TSM 處理中
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201734900740001' --201704140105, 00003JJR, T, 159926
	exec SP_Q_INFO_BY_VIDEOID '00003JJR' --T
	select * from TBTRANSCODE where FNTRANSCODE_ID = 159926 --2017-04-17 09:35:50.810 已重新排播
end

if @case = 201708101615 begin --2016-17 歐霸足球聯賽  14 ANS 註冊錯誤 重啟轉檔 done
	select * from  TBLOG_VIDEO where FSBRO_ID = '201704060143' --G201773600140002, R, 00003IRT, 161077
	select * from TBTRANSCODE where FNTRANSCODE_ID = 161077 -- -1, 8, 2017-05-03 10:50:34.637 -> 重啟後 ANSID = 118101
	exec SP_Q_INFO_BY_VIDEOID '00003IRT'
end

if @case = 201708111137 begin
	exec SP_Q_INFO_BY_VIDEOID '00003TYY' --上傳完成未更新狀態 G000008255180002 no progress bar
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'C' where FSFILE_NO = 'G000008255180002'
	exec SP_Q_INFO_BY_VIDEOID '00003TZ0'
	exec SP_Q_INFO_BY_VIDEOID '00003TSH'
	exec SP_Q_INFO_BY_VIDEOID '00003TYQ' --上傳完成未更新狀態 G201553701020001 no progress bar
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'C' where FSFILE_NO = 'G201553701020001' 
end

if @case = 201708111645 begin --調用檔案已下架
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201708110026' --\\mamstream\MAMDFS\ptstv\HVideo\0002\0002137\G000213700050002.mxf, R
end


if @case = 201708111811 begin
	select * from TBPROG_M where FSPGMNAME like '歡迎光臨' --2017194
	select * from TBLOG_VIDEO where FSID = '2017194' and FNEPISODE = 19 --S, 00003R53, 166159
	select * from TBTRANSCODE where FNTRANSCODE_ID = 166159 --ANS_ID = -1, 2017-07-10 12:28:48.650
end

if @case = 201708131013-201703071207-201609010935-201607271724 begin --\\10.13.200.22\mamnas1 轉移到 18T

  
		----TEST---------
        select FSSYSTEM_CONFIG into #tmp_TBSYSTEM_CONFIG from TBSYSTEM_CONFIG
        select * from #tmp_TBSYSTEM_CONFIG
		select FSSYSTEM_CONFIG from TBSYSTEM_CONFIG

        update #tmp_TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<INGEST_TEMP_DIR_UNC Desc="轉檔暫存區">T:\IngestWorkingTemp\</INGEST_TEMP_DIR_UNC>', 
        '<INGEST_TEMP_DIR_UNC Desc="轉檔暫存區">\\10.13.200.22\mamnas1\</INGEST_TEMP_DIR_UNC>')

		update #tmp_TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<INGEST_TEMP_DIR_LINUX Desc="轉檔暫存區">/tcbuffer/18TB/IngestWorkingTemp/</INGEST_TEMP_DIR_LINUX>', 
        '<INGEST_TEMP_DIR_LINUX Desc="轉檔暫存區">/mnt/mamnas1/</INGEST_TEMP_DIR_LINUX>')

		update #tmp_TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<METASAN_CHECK_FILE Desc="檢測檔位置">/tcbuffer/18TB/00000000.lock</METASAN_CHECK_FILE>', 
        '<METASAN_CHECK_FILE Desc="檢測檔位置">/mnt/mamnas1/00000000.lock</METASAN_CHECK_FILE>')

		----------recover----------------
		update #tmp_TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<INGEST_TEMP_DIR_UNC Desc="轉檔暫存區">\\10.13.200.22\mamnas1\</INGEST_TEMP_DIR_UNC>',
		'<INGEST_TEMP_DIR_UNC Desc="轉檔暫存區">T:\IngestWorkingTemp\</INGEST_TEMP_DIR_UNC>'
        )

		update #tmp_TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<INGEST_TEMP_DIR_LINUX Desc="轉檔暫存區">/mnt/mamnas1/</INGEST_TEMP_DIR_LINUX>', 
		'<INGEST_TEMP_DIR_LINUX Desc="轉檔暫存區">/tcbuffer/18TB/IngestWorkingTemp/</INGEST_TEMP_DIR_LINUX>'
        )

		update #tmp_TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<METASAN_CHECK_FILE Desc="檢測檔位置">/mnt/mamnas1/00000000.lock</METASAN_CHECK_FILE>',
		'<METASAN_CHECK_FILE Desc="檢測檔位置">/tcbuffer/18TB/00000000.lock</METASAN_CHECK_FILE>'
        )



		-------------記得要 dump 出來 diff 比對結果
        update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<INGEST_TEMP_DIR_UNC Desc="轉檔暫存區">T:\IngestWorkingTemp\</INGEST_TEMP_DIR_UNC>', 
        '<INGEST_TEMP_DIR_UNC Desc="轉檔暫存區">\\10.13.200.22\mamnas1\IngestWorkingTemp\</INGEST_TEMP_DIR_UNC>')

		update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<INGEST_TEMP_DIR_LINUX Desc="轉檔暫存區">/tcbuffer/18TB/IngestWorkingTemp/</INGEST_TEMP_DIR_LINUX>', 
        '<INGEST_TEMP_DIR_LINUX Desc="轉檔暫存區">/mnt/mamnas1/IngestWorkingTemp/</INGEST_TEMP_DIR_LINUX>')

		update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<METASAN_CHECK_FILE Desc="檢測檔位置">/tcbuffer/18TB/00000000.lock</METASAN_CHECK_FILE>', 
        '<METASAN_CHECK_FILE Desc="檢測檔位置">/mnt/mamnas1/00000000.lock</METASAN_CHECK_FILE>')


		----------recover----------------
		update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<INGEST_TEMP_DIR_UNC Desc="轉檔暫存區">\\10.13.200.22\mamnas1\IngestWorkingTemp\</INGEST_TEMP_DIR_UNC>',
		'<INGEST_TEMP_DIR_UNC Desc="轉檔暫存區">T:\IngestWorkingTemp\</INGEST_TEMP_DIR_UNC>'
        )

		update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<INGEST_TEMP_DIR_LINUX Desc="轉檔暫存區">/mnt/mamnas1/IngestWorkingTemp/</INGEST_TEMP_DIR_LINUX>', 
		'<INGEST_TEMP_DIR_LINUX Desc="轉檔暫存區">/tcbuffer/18TB/IngestWorkingTemp/</INGEST_TEMP_DIR_LINUX>'
        )

		update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<METASAN_CHECK_FILE Desc="檢測檔位置">/mnt/mamnas1/00000000.lock</METASAN_CHECK_FILE>',
		'<METASAN_CHECK_FILE Desc="檢測檔位置">/tcbuffer/18TB/00000000.lock</METASAN_CHECK_FILE>'
        )

        select * from TBSYSTEM_CONFIG

		select * from TBTRACKINGLOG where FDTIME > '2017-03-07 20:15' order by FDTIME
end

if @case = 201708140958 begin --提供回朔轉檔失敗清單
	exec SP_Q_INFO_BY_VIDEOID '00003U1N'
	select * from VW_TBTRANSCODE where LEN(FSTRACK) = 4 and FNPROCESS_STATUS = 4 and FCFILE_STATUS = 'S' and FDSTART_TIME > '2017-03-01' order by FDSTART_TIME --253

	declare @TBVIDEO TABLE (
		file_number int,
		fsbro_id varchar(20),
		file_no varchar(20)
	)
	
	declare @FSFILE_NO varchar(20)
	declare @FSBRO_ID varchar(20)
	declare @count int = 1
	declare CSR CURSOR for select FSBRO_ID ,FSFILE_NO from VW_TBTRANSCODE where LEN(FSTRACK) = 4 and FNPROCESS_STATUS = 4 and FCFILE_STATUS = 'S' and FDSTART_TIME > '2017-03-01' order by FDSTART_TIME
	open CSR
    FETCH NEXT from CSR into @FSBRO_ID, @FSFILE_NO
    while (@@FETCH_STATUS = 0) begin
		insert into @TBVIDEO(file_number, fsbro_id, file_no) select @count, @FSBRO_ID, @FSFILE_NO 
		set @count = @count + 1
		--update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = @FSFILE_NO
		FETCH NEXT from CSR into @FSBRO_ID, @FSFILE_NO
	end

	select A.*, C.FSPGMNAME, B.FNEPISODE from @TBVIDEO A left join TBLOG_VIDEO B on A.file_no =  B.FSFILE_NO
	left join TBPROG_M C on B.FSID = C.FSPROG_ID 
	CLOSE CSR


	-- no file
	select * from TBLOG_VIDEO where FSFILE_NO = 'G000006015430003' --S
/*
/mnt/18TB/IngestWorkingTemp/G000006015430003.mxf
/mnt/18TB/IngestWorkingTemp/G000006015450003.mxf
/mnt/18TB/IngestWorkingTemp/G000006015460003.mxf
/mnt/18TB/IngestWorkingTemp/G000006015630001.mxf
/mnt/18TB/IngestWorkingTemp/G000006015650003.mxf
/mnt/18TB/IngestWorkingTemp/G201628600010017.mxf
*/

	select * from TBLOG_VIDEO where FDUPDATED_DATE > '2017-08-14' and FCFILE_STATUS = 'T' and LEN(FSTRACK) = 4 order by FDUPDATED_DATE --36
	select * from TBLOG_VIDEO where FDUPDATED_DATE > '2017-08-14' and FCFILE_STATUS = 'S' and LEN(FSTRACK) = 4 order by FDUPDATED_DATE --56
end

if @case = 201708241554-201708101510 begin --科技•靈感•大自然 1~3 調用檔案已下架
	select * from TBLOG_VIDEO where FSID = '2016303' --\\mamstream\MAMDFS\ptstv\HVideo\2016\2016303\G201630300030001.mxf
end


if @case = 201708150949 begin
	declare @TBFILENO TABLE (
		FSFILE_NO varchar(20)
	)
	--用variable 會 error : 接近關鍵字 'with' 的語法不正確。如果這個陳述式是通用資料表運算式、xmlnamespaces 子句或變更追蹤內容子句，則前一個陳述式必須以分號結束。


	if OBJECT_ID(N'tempdb.dbo.#tempImport', N'U') IS NOT NULL
		DROP TABLE #tempImport

	CREATE TABLE  #tempImport (
		FSFILE_NO varchar(20)
	)

	--BULK INSERT #tempImport from '\\10.13.200.5\OtherVideo\temp\fileno.txt' WITH
	BULK INSERT #tempImport from 'c:\temp\fileno.txt' WITH
	(
		ROWTERMINATOR = '\n'
	);

	select * from #tempImport

end

if @case = 201708151007 begin --檔案未到
	exec SP_Q_INFO_BY_VIDEOID '000025UG' --白袍下的高跟鞋 2 ch13 2017-08-17 \\mamstream\MAMDFS\ptstv\HVideo\2013\2013846\G201384600020010.mxf
	exec SP_Q_INFO_BY_VIDEOID '000025UH' --白袍下的高跟鞋 3 2017-08-18 \\mamstream\MAMDFS\ptstv\HVideo\2013\2013846\G201384600030009.mxf

	exec SP_Q_INFO_BY_VIDEOID '00002QVW' --高峰客家力(第110集不排播) 2017-08-16 ch07 G200731901160005, 142788
	select * from TBTRANSCODE where FNTRANSCODE_ID = 142788 --ANSID = 94442 , FNPROCESS_STATUS = 3, 2016-09-01 09:56:46.170
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G200731901160005'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G200731901160005'

	select * from TBLOG_VIDEO where FSFILE_NO = 'P201705002740001' --00003LW8
end


if @case = 201708141614 begin --HSM4 dsmrecall 卡住
	select * from TBTSM_JOB where FNTSMJOB_ID = 247674 --FNSTATUS = 5 /ptstv/HVideo/2015/20150400674/P201504006740002.mxf no dsmrecall job 執行plink得到錯誤的回傳內容：指令 = dsmrecall /ptstv/HVideo/2015/20150400674/P201504006740002.mxf, 參數 = 10.13.210.15 -l root -pw TSMP@ssw0rdTSM ". /root/.bash_profile";"dsmrecall /ptstv/HVideo/2015/20150400674/P201504006740002.mxf", 錯誤訊息 = ANS1005I TCP/IP read error on socket = 5, errno = 104, reason : 'Connection reset by peer'.
	select * from TBTSM_JOB where FNTSMJOB_ID = 247672 --FNSTATUS = 2 /hakkatv/HVideo/2016/20160600609/P201606006090001.mxf --> 3
	exec SP_Q_INFO_BY_VIDEOID '00003FLS'
	select * from TBTSM_JOB where FNTSMJOB_ID = 247721 --5 執行plink得到錯誤的回傳內容：指令 = dsmrecall /ptstv/HVideo/2016/20160700267/P201607002670001.mxf, 參數 = 10.13.210.15 -l root -pw TSMP@ssw0rdTSM ". /root/.bash_profile";"dsmrecall /ptstv/HVideo/2016/20160700267/P201607002670001.mxf", 錯誤訊息 = ANS1005I TCP/IP read error on socket = 5, errno = 104, reason : 'Connection reset by peer'.
	select * from TBTSM_JOB where FNTSMJOB_ID = 247722 --5 執行plink得到錯誤的回傳內容：指令 = dsmrecall /ptstv/HVideo/2017/20170300216/P201703002160001.mxf, 參數 = 10.13.210.15 -l root -pw TSMP@ssw0rdTSM ". /root/.bash_profile";"dsmrecall /ptstv/HVideo/2017/20170300216/P201703002160001.mxf", 錯誤訊息 = ANS1005I TCP/IP read error on socket = 5, errno = 104, reason : 'Connection reset by peer'.
	select * from TBTSM_JOB where FNTSMJOB_ID = 247689 --5 執行plink得到錯誤的回傳內容：指令 = dsmrecall /ptstv/HVideo/2017/20170500831/P201705008310001.mxf, 參數 = 10.13.210.15 -l root -pw TSMP@ssw0rdTSM ". /root/.bash_profile";"dsmrecall /ptstv/HVideo/2017/20170500831/P201705008310001.mxf", 錯誤訊息 = ANS1005I TCP/IP read error on socket = 5, errno = 104, reason : 'Connection reset by peer'.
	select * from TBTSM_JOB where FNTSMJOB_ID = 247726 --5 執行plink得到錯誤的回傳內容：指令 = dsmrecall /hakkatv/HVideo/2016/20160700347/P201607003470001.mxf, 參數 = 10.13.210.15 -l root -pw TSMP@ssw0rdTSM ". /root/.bash_profile";"dsmrecall /hakkatv/HVideo/2016/20160700347/P201607003470001.mxf", 錯誤訊息 = ANS1005I TCP/IP read error on socket = 5, errno = 104, reason : 'Connection reset by peer'.
	select * from TBTSM_JOB where FNTSMJOB_ID = 247787 --2

	select * from TBTSM_JOB where FDREGISTER_TIME > '2017-08-15 12:00' order by FDREGISTER_TIME
	/*
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 247721  --FNPROCESS -> 5 ->1->2->3
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 247722
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 247689
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 247726
	*/
end


if @case = 201708161128 begin  --調用失敗
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201708140131'
	--F, \\mamstream\MAMDFS\ptstv\HVideo\2014\20140100062\D201401000620001.mxf, 168672
	--R, \\mamstream\MAMDFS\ptstv\HVideo\2014\20140100060\D201401000600001.mxf, 168674 offline  E00380L4 is unavailable
	--R, \\mamstream\MAMDFS\ptstv\HVideo\2014\20140100060\D201401000600001.mxf, 168678 offline
	--F, \\mamstream\MAMDFS\ptstv\HVideo\2014\20140200001\D201402000010001.mxf, 168679

	select * from TBTRANSCODE where FNTRANSCODE_ID = 168672 --ANS=-1, 2017-08-15 16:59:08.163
	select * from TBUSERS where FSUSER_ID = '50360' --news50360, 林珍汝
end

if @case = 201708161726 begin --標記上傳後狀態未更新
	exec SP_Q_INFO_BY_VIDEOID '00003U0I' --客家戲曲 797 2017-08-22
	exec SP_Q_INFO_BY_VIDEOID '00003UB2' --藤小妹與大樹哥 2017-08-19
	exec SP_Q_INFO_BY_VIDEOID '00003UBQ' --8/22宏觀 menu0030 2017-08-22 201708160063
end

if @case = 201708171101 begin --HD_FILE_MANAGER 狀態是拷貝完成 檔案已在 Approved
	exec SP_Q_INFO_BY_VIDEOID '00003U0H' --客家戲曲796
end


if @case = 201708171345 begin  --大量調用失敗
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201708170040'
	select * from TBTSM_JOB where FNTSMJOB_ID = 248213 --3
	select * from TBBOOKING_DETAIL where FSBOOKING_NO = '201708170040'
	select * from TBTRANSCODE where FNTRANSCODE_ID = 168874 --ANS= -1 FNPAR = 167597
	select * from TBTRANSCODE where FNTRANSCODE_ID = 168852 --ANS = -1 FNPAR = -1
	select * from TBTRANSCODE where FNTRANSCODE_ID = 168868 --ANS = -1 PAR = -1
	--\\mamstream\MAMDFS\ptstv\HVideo\0001\0001133\G000113300170002.mxf
end

if @case = 201708171349 begin --標記上傳後狀態未更新
	exec SP_Q_INFO_BY_VIDEOID '00003UCZ' --2017福氣來了 162 2017-08-29
	exec SP_Q_INFO_BY_VIDEOID '00003UGW' --PTS1_五味八珍的歲月-安心亞專訪 2017-08-19
end


if @case = 201708171905 begin
	if OBJECT_ID(N'tempdb.dbo.#tempVideo', N'U') IS NOT NULL
		DROP TABLE #tempVideo

	CREATE TABLE  #tempVideo (
		VIDEO_ID varchar(20)
	)

	--BULK INSERT #tempImport from '\\10.13.200.5\OtherVideo\temp\fileno.txt' WITH
	BULK INSERT #tempVIDEO from 'c:\temp\video.txt' WITH
	(
		ROWTERMINATOR = '\n'
	);
	
	/*
	select SUBSTRING(VIDEO_ID, 1, 6), B.FSPROG_NAME, B.FSPLAY_TIME, B.FSCHANNEL_ID ,B.FSPLAY_TIME  from #tempVideo A left join TBPGM_COMBINE_QUEUE B
	on SUBSTRING(A.VIDEO_ID, 1, 6) = SUBSTRING(B.FSVIDEO_ID, 3, 6) order by B.FDDATE, B.FSPLAY_TIME
	*/

	select SUBSTRING(A.VIDEO_ID, 1, 6), B.NAME, B.FDDATE, C.FSCHANNEL_NAME, B.FSPLAY_TIME from #tempVideo A left join 
	(select FSVIDEO_ID VIDEO_ID, FSPROG_NAME NAME, FDDATE, FSCHANNEL_ID, FSPLAY_TIME from TBPGM_COMBINE_QUEUE union (select A.FSVIDEO_ID VIDEO_ID, B.FSPROMO_NAME NAME, A.FDDATE, A.FSCHANNEL_ID, FSPLAYTIME from TBPGM_ARR_PROMO A left join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID)) B
	on SUBSTRING(A.VIDEO_ID, 1, 6) = SUBSTRING(B.VIDEO_ID, 3, 6) left join TBZCHANNEL C on B.FSCHANNEL_ID = C.FSCHANNEL_ID
	order by B.FDDATE, B.FSCHANNEL_ID, B.FSPLAY_TIME
	


	select SUBSTRING(VIDEO_ID, 1, 6), B.FSPROG_NAME, B.FSPLAY_TIME, B.FSCHANNEL_ID ,B.FSPLAY_TIME  from #tempVideo A left join TBPGM_COMBINE_QUEUE B 
	on SUBSTRING(A.VIDEO_ID, 1, 6) = SUBSTRING(B.FSVIDEO_ID, 3, 6) order by B.FDDATE, B.FSPLAY_TIME

	--UNION
	select FSVIDEO_ID VIDEO_ID, FSPROG_NAME NAME from TBPGM_COMBINE_QUEUE union (select A.FSVIDEO_ID VIDEO_ID, B.FSPROMO_NAME NAME from TBPGM_ARR_PROMO A left join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID)

	select * from TBPGM_ARR_PROMO
end

if @case = 201708180923 begin
	select * from TBTSM_JOB where FNTSMJOB_ID = 247967 --5 002Q2Z
	select * from TBTSM_JOB where FNTSMJOB_ID = 248370 --5 003TVY
	select * from TBTSM_JOB where FNTSMJOB_ID = 248421 --5 003U0J
	select * from TBTSM_JOB where FNTSMJOB_ID = 247913 --5 003TZV
	
	/*
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 247967 --3
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 248370
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 248421
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 247913
	*/
end

if @case = 201708171220 begin --調用 partial 處理錯誤
	select * from TBTRACKINGLOG where FDTIME  between '2017-08-17' and '2017-08-18' and FSMESSAGE like '%168867%' or FSMESSAGE like '%201708170040%'  order by FDTIME 
	select * from TBTRACKINGLOG where FDTIME  between '2017-08-17 10:50' and '2017-08-17 11:50'  order by FDTIME 
	select * from TBTRACKINGLOG where FDTIME  between '2017-08-17 14:20' and '2017-08-17 14:50'  order by FDTIME 
end

if @case = 201708181337 begin
	exec SP_Q_INFO_BY_VIDEOID '00003UGW' --PTS1_五味八珍的歲月 關於傅培梅 2017-08-19 ch12
end


if @case = 201708181547 begin --QC 審核後程式擋掉 雖然狀態已是審核通過但是未進行轉檔 done
	exec SP_Q_INFO_BY_VIDEOID '00003UJ5' --O 2016變臉台灣PROMO P201708004620001 2017-08-18 14:34:32.823
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201708004620001'
end

if @case = 201708181748 begin
	exec SP_Q_INFO_BY_VIDEOID '00003UC1' --8/23宏觀 menu2000 2017-08-23 ch08
end

if @case = 201708181814 begin --手動搬到  HDUpload
	exec SP_Q_INFO_BY_VIDEOID '00003ONH' --TAIWAN IN FOCUS(短版) 2017-08-19
end

if @case = 201708181816 begin
	exec SP_Q_INFO_BY_VIDEOID '00003UMC' --世大運中華電視卡 2017-08-20 ch 14
	exec SP_Q_INFO_BY_VIDEOID '00003UH6' --藝術很有事（106年內容產製與運用計畫）2017-08-19 ch12
end

if @case = 201708210958 begin
	exec SP_Q_INFO_BY_VIDEOID '00001XUL' --美味縱貫現 1 2017-08-24 ch08
end

if @case = 201708211035 begin
	select * from TBLOG_VIDEO where FSBRO_ID = '201708160046' --S -> T
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201623900010028'
end

if @case = 201708211118 begin --匯入 video ID 清單取得相關資訊
	if OBJECT_ID(N'tempdb.dbo.#tempVideo', N'U') IS NOT NULL
		DROP TABLE #tempVideo

	CREATE TABLE  #tempVideo (
		VIDEO_ID varchar(20)
	)

	--BULK INSERT #tempImport from '\\10.13.200.5\OtherVideo\temp\fileno.txt' WITH
	BULK INSERT #tempVideo from 'c:\temp\videoid.txt' WITH
	(
		ROWTERMINATOR = '\n'
	);

	--select * from #tempVideo

	--SELECT RIGHT('000'+ISNULL(null,''),3)

	select A.VIDEO_ID, B.FSVIDEO_PROG, B.FSFILE_SIZE, RIGHT('00:' + LEFT(RIGHT(B.FSBEG_TIMECODE, 8), 5), 8) BEGIN_TIME, 
	RIGHT('00:' + LEFT(RIGHT(B.FSEND_TIMECODE, 8), 5), 8) END_TIME, B.FSBEG_TIMECODE, B.FSEND_TIMECODE
	from #tempVideo A left join TBLOG_VIDEO B on RIGHT('00000000' + A.VIDEO_ID, 8) = B.FSVIDEO_PROG order by A.VIDEO_ID
end

if @case = 201708211349 begin --青春發言人 #27 29 38 調用失敗
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201708160156'
	--\\mamstream\MAMDFS\ptstv\HVideo\2016\2016925\G201692500290001.mxf
	--\\mamstream\MAMDFS\ptstv\HVideo\2016\2016925\G201692500270003.mxf
	--\\mamstream\MAMDFS\ptstv\HVideo\2016\2016925\G201692500380001.mxf
	select * from TBUSERS where FSUSER_ID = '70347' --yenrul 賴彥如
end

if @case = 201708211442-201704211447-201704141102 begin
	select * from TBLOG_VIDEO where FSID = '2016712' and FNEPISODE = 1 
	--G201671200010001, 201605310020, 00002LIC,X
	--G201671200010002, 201607040315, 00002QON,T
	--G201671200010003, 201607050157, 00002QWJ,X
	exec SP_Q_INFO_BY_VIDEOID '00002QON' --他們在島嶼寫作2-洛夫 2017-04-24, 14, 201607040315
	exec SP_Q_INFO_BY_VIDEOID '00002QWJ'
	exec SP_Q_INFO_BY_VIDEOID '00002LIC'


	--填置換單後舊的置換單還在
	select * from TBLOG_VIDEO where FSID = '2016712' and FNEPISODE = 2
	select * from TBPROG_M where FSPGMNAME like '%他們在島嶼寫作%'
	update TBLOG_VIDEO set FCFILE_STATUS = 'D' where FSFILE_NO = 'G201671200020002' --201607050159
	--exec SP_D_TBLOG_VIDEO_BYFILENO 'G201671200020002' --不小心刪到第一支 G201671200020001 --> VTR 重轉

	select * from TBLOG_VIDEO where FSID = '2016712' and FNEPISODE = 1
	exec SP_Q_INFO_BY_VIDEOID '00002QOP' --N, 但是舊的審核紀錄仍在 2017-04-25, ch14
	select * from TBLOG_QC_RESULT where FSPROG_ID like '%2016712%' and FNEPISODE = 2 --2016-07-10 審核通過 51272 重填單子仍沿用  00002QOP

	update TBLOG_QC_RESULT set FSVIDEO_ID = '002QOP-D' where FSFILE_NO = 'G201671200020001'
	update TBLOG_QC_RESULT set FSPROG_ID = '2016712-D' where FSFILE_NO = 'G201671200020001'
	update TBLOG_QC_RESULT set FSFILE_NO = 'G201671200020001-D' where ID = 3398
	select * from TBLOG_VIDEO_HD_MC where FSFILE_NO = 'G201671200020001'
	update TBLOG_VIDEO_HD_MC where FSFILE_NO = 'G201671200020001'

	select COUNT(FSVIDEO_ID), FSPROG_ID, FNEPISODE, FSPROG_NAME, FSVIDEO_ID from TBLOG_QC_RESULT group by FSPROG_ID, FNEPISODE, FSPROG_NAME, FSVIDEO_ID having COUNT(FSVIDEO_ID) > 1

	select * from TBLOG_QC_RESULT where FSPROG_ID like '%2017736%' and FNEPISODE = 2
	select * from TBLOG_QC_RESULT where FSPROG_ID = '2017724' and FNEPISODE = 6

	select * from TBLOG_VIDEO_HD_MC where FSFILE_NO = 'G201671200020001' --O --need remove
	select * from TBLOG_VIDEO_HD_MC where FSFILE_NO = 'G201671200020003' --N
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002QOP'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201671200020001'
	select * from TBLOG_QC_RESULT where ID = 3398
	update TBLOG_QC_RESULT set FSVIDEO_ID = '002QOP', FSPROG_ID = '2016712',FSFILE_NO = 'G201671200020001' where ID = 3398


	--8/21
	exec SP_Q_INFO_BY_VIDEOID '00002QOP' --G201671200020003 他們在島嶼寫作2-洛夫 #2 2017-08-24 ch14, 201704210094
	select * from TBLOG_VIDEO where FSID = '2016712' and FNEPISODE = 2 --G201671200020003 only 1 record
	select * from TBLOG_VIDEO_D where FSID = '2016712' and FNEPISODE = 2
	--G201671200020001 T
	--G201671200020003 T

	select * from TBLOG_VIDEO where FSID = '2016712'  --00002QON #1, 00002QOP #2


	select * from TBLOG_VIDEO where FSBRO_ID = '201607040315' --G201671200010002, #1, T, 00002QON
	

	exec SP_Q_INFO_BY_VIDEOID '00002QOP' --2017-08-24 ch14 201704210094, G201671200020003 他們在島嶼寫作2-洛夫 #2

	exec SP_Q_INFO_BY_VIDEOID '00002QON' --他們在島嶼寫作2-洛夫 2017-08-23 #14
	select * from TBLOG_VIDEO where FSID = '2016712' and FNEPISODE = 1  --G201671200010002 T, G201671200010003 X
	select * from TBLOG_VIDEO_D where FSID = '2016712' and FNEPISODE = 1 
	--G201671200010002 T, 201607040315 會出現待置換清單
	--G201671200010003,X, 201607050157 FSCHANGE_FILE_NO = G201671200010002
	exec SP_D_TBLOG_VIDEO_BYFILENO 'G201671200010003'


	select * from TBPROG_M where FSPGMNAME like '%他們在島嶼寫作2%' --2017101 他們在島嶼寫作2-白先勇
	select * from TBLOG_VIDEO where FSID = '2017101' --00002QWQ #1 --00002QWR #2
end


if @case = 201708211546 begin --大腦生意經 調用審核問題
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201708210060'
	select * from TBBOOKING_MASTER where FSBOOKING_NO = '201708210060'
	select * from TBBOOKING_DETAIL where FSBOOKING_NO = '201708210060' --G201795900010006
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201795900010006' --2017959 大腦生意經 製作人 林玉清
end

if @case = 201708211814 begin --Timecode-based in- point is earlier than start timecode 無法重轉 刪除送播單重填
	exec SP_Q_INFO_BY_VIDEOID '00003ULI' --奧林P客 896 2017-08-23 ch07, R 169080 G200618608960001
	select * from TBTRANSCODE where FNTRANSCODE_ID = 169080 --ANSID = 118651, 8 2017-08-18 20:00:52.270
	exec SP_D_TBLOG_VIDEO_BYFILENO 'G200618608960001'
end

if @case = 201708231857 begin
	exec SP_Q_INFO_BY_VIDEOID '00003U46' --第39屆威廉瓊斯盃國際籃球邀請賽 1 2017-08-25 ch62 R 檔案有問題無法轉檔
	exec SP_Q_INFO_BY_VIDEOID '00003U48' --第39屆威廉瓊斯盃國際籃球邀請賽 2 2017-08-25 ch62 R 檔案有問題無法轉檔

	--non-drop frame
	exec SP_Q_INFO_BY_VIDEOID '00003UN8' --麻醉風暴2 陳惟愉(許瑋甯飾) 角色篇 2017-08-29 ch12 R
	exec SP_Q_INFO_BY_VIDEOID '00003UN9' --麻醉風暴2 沈柔伊(孟耿如 飾) 角色篇 2017-08-29 ch12 R
end


------------------------20170824-------------------------

if @case = 201708241018 begin --標記後狀態未更新
	exec SP_Q_INFO_BY_VIDEOID '00003V6N' --2017金鐘獎入圍捷報 2017-08-24 ch12
	exec SP_Q_INFO_BY_VIDEOID '00003V4P' --南部開講第33集promoe(2017) 2017-08-25 ch12
end

if @case = 201710260824 begin --雷孝慈 的入庫單會到張瀠之（原製作人）
	select * from TBUSERS where FSUSER_ChtName = '雷孝慈' --hsiaotzu 51388
	select * from TBUSERS where FSUSER_ID = '51343' --張瀠之 FCACTIVE 0 社會服務群

	select * from TBPROG_M where FSPGMNAME like '%乒乓%' --2017740
end

if @case = 201708241127 begin --調用失敗
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201708210099' --一克拉的室友(白天不宜) \\mamstream\MAMDFS\ptstv\HVideo\2015\2015780\G201578000010022.mxf
	select * from TBTRANSCODE where FNTRANSCODE_ID = 169408 --ANSID = -1 2017-08-23 10:32:13.507
	select * from TBUSERS where FSUSER_ID = '05065' --劉秋信 adm3036
end

if @case = 201708241415-201708211546 begin --大腦生意經 調用審核問題 轉檔完成，檔案卻沒下來
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201708210060' --\\mamstream\MAMDFS\ptstv\HVideo\2017\2017959\G201795900010006.mxf
	select * from TBBOOKING_MASTER where FSBOOKING_NO = '201708210060'
	select * from TBBOOKING_DETAIL where FSBOOKING_NO = '201708210060' --G201795900010006
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201795900010006' --2017959 大腦生意經 製作人 林玉清
	select * from TBUSERS where FSUSER_ID = '60061' --bobohung
end

if @case = 201708241437 begin --標記上傳後未更新狀態
	exec SP_Q_INFO_BY_VIDEOID '00003UMT' --PTS1_十點全紀錄 生命物語第二季01 2017-08-26
	exec SP_Q_INFO_BY_VIDEOID '00003UAK' --PTS3 RM【公視學生劇展】登山
end

if @case = 201708241724 begin
	select * from TBLOG_VIDEO where FSBRO_ID = '201708210070' --G201381100600026, 2013811 #60, S 00003UU7 169222
	select * from TBTRANSCODE where FNTRANSCODE_ID = 169222 -- ANSID 118788, 4
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201381100600026'
	select * from TBLOG_VIDEO where FSBRO_ID = '201708210072' --G201381100610020, 2013811 #61 S 169223
	select * from TBTRANSCODE where FNTRANSCODE_ID = 169223  --4 ANSID = 118789
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201381100610020'
end

if @case = 201708250710 begin --切換 HDUpload

	if OBJECT_ID(N'tempdb.dbo.#tmp_TBSYSTEM_CONFIG', N'U') IS NOT NULL
		DROP TABLE #tmp_TBSYSTEM_CONFIG

	select FSSYSTEM_CONFIG into #tmp_TBSYSTEM_CONFIG from TBSYSTEM_CONFIG

	select * from #tmp_TBSYSTEM_CONFIG

		
	if OBJECT_ID(N'tempdb.dbo.#tmp_TBSYSTEM_CONFIG', N'U') IS NOT NULL
		DROP TABLE #tmp_TBSYSTEM_CONFIG

	select FSSYSTEM_CONFIG into #tmp_TBSYSTEM_CONFIG from TBSYSTEM_CONFIG

	select * from #tmp_TBSYSTEM_CONFIG

		
	update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<STB_HDUPLOAD  Desc="後製檔案上傳區">\\10.13.200.6\HDUpload\</STB_HDUPLOAD>',
		'<STB_HDUPLOAD  Desc="後製檔案上傳區">\\10.13.200.25\HDUpload\</STB_HDUPLOAD>'
	)

	update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<PTS_AP_UPLOAD_PATH>\\10.13.200.6\HDUpload\</PTS_AP_UPLOAD_PATH>',
		'<PTS_AP_UPLOAD_PATH>\\10.13.200.25\HDUpload\</PTS_AP_UPLOAD_PATH>'
    )

	--restart HD_FILE_MANAGER
	--restart BigFILETRANSFER

	exec SP_Q_INFO_BY_VIDEOID '00003V6E' --ok
end

if @case = 201708251011 begin --標記上傳後未更新狀態
	exec SP_Q_INFO_BY_VIDEOID '00003UJL' --五味八珍的歲月 5 2017-08-28 ch12
	exec SP_Q_INFO_BY_VIDEOID '00003UJM' --五味八珍的歲月 6 2017-08-29 ch12
end

if @case = 201708251024 begin --調用失敗 partial 處理錯誤
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201708230059' 
	/*
	科技風向 1 \\mamstream\MAMDFS\ptstv\HVideo\2016\2016545\G201654500010002.mxf 169427
	\\mamstream\MAMDFS\ptstv\HVideo\2016\2016545\G201654500050007.mxf 169430
	\\mamstream\MAMDFS\ptstv\HVideo\2016\2016545\G201654500060003.mxf 169431
	\\mamstream\MAMDFS\ptstv\HVideo\2016\2016545\G201654500070003.mxf 169432
	\\mamstream\MAMDFS\ptstv\HVideo\2016\2016545\G201654500080003.mxf 169433

	青春的進擊 2 \\mamstream\MAMDFS\ptstv\HVideo\2013\2013684\G201368400020010.mxf 169434
	*/


	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201708230101' --對照記 2 \\mamstream\MAMDFS\ptstv\HVideo\2015\2015088\G201508800020009.mxf

	select * from TBUSERS where FSUSER_ID = '05224' --pub50202 蘇慧真
end


if @case = 201708251207 begin --再見原鄉 調用失敗 註冊Anystream服務時發生錯誤
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201708250029' --\\mamstream\MAMDFS\ptstv\HVideo\2016\2016669\G201666900010010.mxf 169646
	select * from TBUSERS where FSUSER_ID = '09478' --news50335 左珮華
end

if @case = 201708251420 begin --標記上傳後狀態未更新
	exec SP_Q_INFO_BY_VIDEOID '00003VCH' --2017金鐘獎入圍捷報_濁流 2017-08-26
	exec SP_Q_INFO_BY_VIDEOID '00003VCC' --客家戲曲 818 2017-08-30
	exec SP_Q_INFO_BY_VIDEOID '00003V6I' --2017福氣來了 163 2017-08-30
end

if @case = 201708280858 begin --TSMRECALL 失敗 錯誤訊息 = FATAL ERROR: Server unexpectedly closed network connection
	select * from TBTSM_JOB where FNTSMJOB_ID = 250069 --FNSTATUS = 5  
	--執行plink得到錯誤的回傳內容：指令 = dsmrecall /ptstv/HVideo/2017/2017615/G201761500400001.mxf, 參數 = 10.13.210.15 -l root -pw TSMP@ssw0rdTSM ". /root/.bash_profile";"dsmrecall /ptstv/HVideo/2017/2017615/G201761500400001.mxf", 錯誤訊息 = FATAL ERROR: Server unexpectedly closed network connection

	select * from TBTSM_JOB where FDREGISTER_TIME > '2017-08-26' and FNSTATUS <> 3 --5
	

	--update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 250069
	--update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 250180
	--update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 250169
	--update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 250249
	--update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 250252
	select * from TBTRACKINGLOG where FDTIME > '2017-08-28 09:10' order by FDTIME
end

if @case = 201708281020 begin --TBTSM_JOB FNSTATUS = 3 不一定代表 recall  成功
	select * from TBTSM_JOB where FNTSMJOB_ID = 250098 --FNSTATUS = 3 --Tapeonly
	--update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 250098
end


if @case = 201708281042 begin --檔案到 review 只有 288MB
	exec SP_Q_INFO_BY_VIDEOID '00003U2X' --四海一家(授權至2019/12/31) 201708140041, 2014431 5
	--10.13.200.6\HDUpload size 正常 \\10.13.200.25\HDUpload size 只有 288M
	select * from TBLOG_VIDEO where FSID = '2014431' and FNEPISODE = 5 --00003U2X -> 00003VFD

	exec SP_Q_INFO_BY_VIDEOID '00003U2P' --not scheduled R
	exec SP_Q_INFO_BY_VIDEOID '00003SJI' --no scheduled N, B
	exec SP_Q_INFO_BY_VIDEOID '00003TK7' --C not schedule
end

if @case = 201708281449 begin
	select * from TBTSM_JOB where CAST(FDREGISTER_TIME as DATE) = '2017-05-21' order by FDREGISTER_TIME --84
	select * from TBTSM_JOB where CAST(FDREGISTER_TIME as DATE) = '2017-06-29' order by FDREGISTER_TIME --219
	--/ptstv/HVideo/0000/0000216/G000021604290009.mxf
	select * from TBLOG_VIDEO where FSFILE_NO = 'G000021604290009'

	select fileNo, videoId,
	CASE
		WHEN fileSize like '%GB%' THEN CONVERT(FLOAT, SUBSTRING(fileSize, 1, LEN(fileSize) - 2))
		WHEN fileSize like '%MB%' THEN CONVERT(FLOAT, SUBSTRING(fileSize, 1, LEN(fileSize) - 2))/1000
	END as fsize,
	fdTime
	from FT_GET_VIDEOID_FROM_TSMJOB('2017-06-28 08:25', '2017-06-29 08:25')

	select
	SUM(
		CASE
			WHEN fileSize like '%GB%' THEN CONVERT(FLOAT, SUBSTRING(fileSize, 1, LEN(fileSize) - 2))
			WHEN fileSize like '%MB%' THEN CONVERT(FLOAT, SUBSTRING(fileSize, 1, LEN(fileSize) - 2))/1000
		END
	)
	from FT_GET_VIDEOID_FROM_TSMJOB('2017-06-07 08:25', '2017-06-08 08:25')


	select
	CAST(fdTime as DATE) as 'Retrieve Date',
	ROUND(
	SUM(
		CASE
			WHEN fileSize like '%GB%' THEN CONVERT(FLOAT, SUBSTRING(fileSize, 1, LEN(fileSize) - 2))
			WHEN fileSize like '%MB%' THEN CONVERT(FLOAT, SUBSTRING(fileSize, 1, LEN(fileSize) - 2))/1000
		END
	), 2) as 'File Size(GB)'	
	from FT_GET_VIDEOID_FROM_TSMJOB('2017-01-01', '2017-07-31')
	Group by CAST(fdTime as DATE)
	order by CAST(fdTime as DATE)

	select
	ROUND(
	AVG(
		CASE
			WHEN fileSize like '%GB%' THEN CONVERT(FLOAT, SUBSTRING(fileSize, 1, LEN(fileSize) - 2))
			WHEN fileSize like '%MB%' THEN CONVERT(FLOAT, SUBSTRING(fileSize, 1, LEN(fileSize) - 2))/1000
		END
	), 2) as 'File Size(GB)'	
	from FT_GET_VIDEOID_FROM_TSMJOB('2017-01-01', '2017-07-31')
end

if @case = 201708282332 begin --有些檔案仍在 \\10.13.200.6\HDUpload 的會成功
	exec SP_Q_INFO_BY_VIDEOID '00003RF9' --客庄好味道(第19集不能播) 179 T ??
	exec SP_Q_INFO_BY_VIDEOID '00003VC5' --R 客庄好味道 #178龍潭promo 2017-09-01 169717 --> T
	select * from TBTRANSCODE where FNTRANSCODE_ID = 169717 --4. 119223 2017-08-25 16:18:42.470
	select * from TBTRACKINGLOG where FDTIME > '2017-08-28 23:35' order by FDTIME
	--plink高解搬檔失敗：JobID 169717/10.13.210.16 -l root -pw TSMP@ssw0rdTSM "cp -r /mnt/HDUpload/003VC5.mxf  /hakkatv/HVideo/2017/20170800692/P201708006920001.mxf"/cp: cannot stat `/mnt/HDUpload/003VC5.mxf': No such file or directory
	select * from TBTRACKINGLOG where FDTIME between '2017-08-28 09:35' and '2017-08-28 12:35' order by FDTIME
	--[PTSMAM1] 執行 plink 指令的結果碰到錯誤訊息：arug = 10.13.210.15 -l root -pw TSMP@ssw0rdTSM ". /root/.bash_profile";"dsmls /mvtv/HVideo/2017/20170300479/P201703004790001.mxf | sed '1,8d'", msg = FATAL ERROR: Server unexpectedly closed network connection
	exec SP_Q_INFO_BY_VIDEOID '00003V3S'

	select * from TBLOG_VIDEO where FDUPDATED_DATE > '2017-08-25' and FCFILE_STATUS = 'R' --66

	select * from TBSYSTEM_CONFIG
end


if @case = 201708291003-201708281020 begin --TBTSM_JOB FNSTATUS = 3 不一定代表 recall  成功
	select * from TBTSM_JOB where FNTSMJOB_ID = 250098 --FNSTATUS = 3 --Tapeonly
	--update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 250098

	select * from TBTSM_JOB where FNTSMJOB_ID = 250098 --3 /ptstv/HVideo/2013/2013793/G201379300120005.mxf ? G200928505920017

	exec SP_Q_INFO_BY_VIDEOID '00003V8U' --FCSTATUS = '' T  爸媽囧很大 592 2017-09-05 ch13  G200928505920017 2009285
	select * from TBTRACKINGLOG where FDTIME > '2017-08-29 10:05' order by FDTIME

	select * from TBLOG_VIDEO where FSFILE_NO = 'G201379300120005' --T 2013793 #12 00001ULW
	exec SP_Q_INFO_BY_VIDEOID '00001ULW'
	select * from TBPROG_M where FSPROG_ID = '2013793' --阿默和他的貓兒們 2017-08-31
	select * from TBLOG_VIDEO where FSID = '2013793' and FNEPISODE = 12 --G201379300120005

	--/ptstv/HVideo/2013/2013793/G201379300120005.mxf  --> /ptstv/HVideo/2009/2009285/G200928505920017.mxf
	update TBTSM_JOB set FSSOURCE_PATH = '/ptstv/HVideo/2009/2009285/G200928505920017.mxf', FNSTATUS = 1 where FNTSMJOB_ID = 250098
end

if @case = 201708291444 begin
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201708280118'
	--\\mamstream\MAMDFS\ptstv\HVideo\2017\2017775\G201777500010001.mxf nearline
	--\\mamstream\MAMDFS\ptstv\HVideo\2016\2016545\G201654500010002.mxf tapeonly
	--\\mamstream\MAMDFS\ptstv\HVideo\2013\2013684\G201368400020010.mxf nearline
	--\\mamstream\MAMDFS\ptstv\HVideo\2016\2016545\G201654500020001.mxf tapeonly

	select * from TBUSERS where FSUSER_ID = '05065' --adm3036 劉秋信
end

if @case = 201708291452 begin --標記上傳後狀態未更新
	exec SP_Q_INFO_BY_VIDEOID '00003VH3' --我們的島 920
	exec SP_Q_INFO_BY_VIDEOID '00003VIT' --奧林P客 901
end


if @case = 201708292227 begin --切換 Review

	if OBJECT_ID(N'tempdb.dbo.#tmp_TBSYSTEM_CONFIG', N'U') IS NOT NULL
		DROP TABLE #tmp_TBSYSTEM_CONFIG

	select FSSYSTEM_CONFIG into #tmp_TBSYSTEM_CONFIG from TBSYSTEM_CONFIG

	select * from #tmp_TBSYSTEM_CONFIG

		
	update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<STB_REVIEW  Desc="待審區">\\10.13.200.6\Review\</STB_REVIEW>',
		'<STB_REVIEW  Desc="待審區">\\10.13.200.22\mamnas1\Review\</STB_REVIEW>'
	)

	update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<PendingPath>\\10.13.200.6\Review\</PendingPath>',
		'<PendingPath>\\10.13.200.22\mamnas1\Review\</PendingPath>'
    )

	--restart HD_FILE_MANAGER
	--check PGM_QC

	select * from TBSYSTEM_CONFIG

	exec SP_Q_INFO_BY_VIDEOID '00001van'
end


if @case = 201708300914 begin
	select * from TBLOG_VIDEO where FSBRO_ID = '201708290064' --G201290000010003 S -> T
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201290000010003'
end

if @case = 201708301029 begin
	exec SP_Q_INFO_BY_VIDEOID '00003V8U' --爸媽囧很大 592 2017-09-05  --原本是待審，因為檔案重搬狀態被更新？
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G200928505920017'

	exec SP_Q_INFO_BY_VIDEOID '00003VL9' --PTS1_我的這一班424(8/30∼9/3) 標記後狀態未更新
end

if @case = 201708301209 begin
	exec SP_Q_INFO_BY_VIDEOID '00003VG3'
	exec SP_Q_INFO_BY_VIDEOID '00003VG5'
	exec SP_Q_INFO_BY_VIDEOID '00003VGD'
	exec SP_Q_INFO_BY_VIDEOID '00003VIW'

	exec SP_Q_INFO_BY_VIDEOID '00003VGE'
	exec SP_Q_INFO_BY_VIDEOID '00003VL6'
	exec SP_Q_INFO_BY_VIDEOID '00003VMB'
	exec SP_Q_INFO_BY_VIDEOID '00003VGO'
	exec SP_Q_INFO_BY_VIDEOID '00003VGP'
	exec SP_Q_INFO_BY_VIDEOID '00003VLU'
	exec SP_Q_INFO_BY_VIDEOID '00003VGT'
	exec SP_Q_INFO_BY_VIDEOID '00003VGU'
end

if @case = 201708301743 begin
	select * from TBTSM_JOB where FNTSMJOB_ID = 250889 --5 -> 3, 執行plink得到錯誤的回傳內容：指令 = dsmrecall /ptstv/HVideo/2016/20160600374/P201606003740001.mxf, 參數 = 10.13.210.15 -l root -pw TSMP@ssw0rdTSM ". /root/.bash_profile";"dsmrecall /ptstv/HVideo/2016/20160600374/P201606003740001.mxf", 錯誤訊息 = FATAL ERROR: Server unexpectedly closed network connection
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 250889
	select * from TBTSM_JOB where FNTSMJOB_ID = 251003 --5 執行plink得到錯誤的回傳內容：指令 = dsmrecall /ptstv/HVideo/2017/2017088/G201708800010003.mxf, 參數 = 10.13.210.15 -l root -pw TSMP@ssw0rdTSM ". /root/.bash_profile";"dsmrecall /ptstv/HVideo/2017/2017088/G201708800010003.mxf", 錯誤訊息 = FATAL ERROR: Server unexpectedly closed network connection
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 251003
end

if @case = 201708310935 begin --TSM RECALL 失敗
	select * from TBTSM_JOB where FNTSMJOB_ID = 251037 --5 執行plink得到錯誤的回傳內容：指令 = dsmrecall /ptstv/HVideo/2017/2017255/G201725500060003.mxf, 參數 = 10.13.210.15 -l root -pw TSMP@ssw0rdTSM ". /root/.bash_profile";"dsmrecall /ptstv/HVideo/2017/2017255/G201725500060003.mxf", 錯誤訊息 = FATAL ERROR: Server unexpectedly closed network connection
	select * from TBTRACKINGLOG where FDTIME between '2017-08-31 00:01' and '2017-08-31 00:05' order by FDTIME
end

if @case = 201708311047 begin --台影新聞史料 無法調用
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201708310010'
	select * from TBBOOKING_DETAIL where FSBOOKING_NO = '201708310010' --D201307000750001, D201307000020001, D201307000620001
	/*
	\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700075\D201307000750001.mxf offline
	\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700002\D201307000020001.mxf offline
	\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700062\D201307000620001.mxf nearline
	*/

	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201708310011' --\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700007\D201307000070001.mxf offline
	/*
	A00109L4
	B00063L4
	B00070L4
	D00285L4
	*/

	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201708290149'
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201708310014'

	select * from TBPROG_M where FSPROG_ID = '20130700075'
	select * from TBLOG_VIDEO where FSFILE_NO = 'D201307000750001' --台影新聞史料088 20130700075
	select * from TBPGM_PROMO where FSPROMO_ID = '20130700075' --7/9 pts menu0600

	select * from TBLOG_VIDEO where FSTITLE like '%台影新聞史料%'  and FCFILE_STATUS = 'T' order by FSTITLE --112

	select * from TBLOG_VIDEO where FSID = '20130200004' --短帶和資料帶

/*
A00063L4
A00109L4
A00109L4
G00012L4
B00063L4
B00070L4
B00235L4
B00245L4
B00272L4
B00276L4
D00284L4
B00315L4
B00315L4
B00235L4
D00285L4
D00328L4
E00042L4
E00042L4
G00020L4
E00101L4
G00012L4
G00020L4
*/
end

if @case = 201708311129 begin
	select * from TBUSERS where FSUSER = 'news50274' --于立平
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201708300094' --\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700075\D201307000750001.mxf nearline
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201708300081' --\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700002\D201307000020001.mxf nearline
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201708300073' --\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700075\D201307000750001.mxf nearline
end


if @case = 201708311448 begin
	select * from TBTRACKINGLOG where FDTIME > '2017-08-31 14:45' order by FDTIME
end

if @case = 201708311559 begin
	select *  from TBTSM_JOB where FNTSMJOB_ID = 251037 --5, 執行plink得到錯誤的回傳內容：指令 = dsmrecall /ptstv/HVideo/2017/2017255/G201725500060003.mxf, 參數 = 10.13.210.15 -l root -pw TSMP@ssw0rdTSM ". /root/.bash_profile";"dsmrecall /ptstv/HVideo/2017/2017255/G201725500060003.mxf", 錯誤訊息 = FATAL ERROR: Server unexpectedly closed network connection
	--update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 251037
end

if @case = 201708311620 begin --標記上傳未更新狀態
	exec SP_Q_INFO_BY_VIDEOID '00003UN0' --誰來晚餐9#23 歡迎布丁和多多-兩個拉媽一個家_PTS1週五版
	exec SP_Q_INFO_BY_VIDEOID '00003VI0' --村民大會 549
	exec SP_Q_INFO_BY_VIDEOID '00003VWF' -- 檔案未傳
	exec SP_Q_INFO_BY_VIDEOID '00003VUS'
	exec SP_Q_INFO_BY_VIDEOID '00003VUT'
	exec SP_Q_INFO_BY_VIDEOID '00003VUX'
end

if @case = 201709011144 begin --標記上傳未更新狀態
	exec SP_Q_INFO_BY_VIDEOID '00003VUC'
	exec SP_Q_INFO_BY_VIDEOID '00003VUU'
	exec SP_Q_INFO_BY_VIDEOID '00003VUV'
	exec SP_Q_INFO_BY_VIDEOID '00003VWY'
	exec SP_Q_INFO_BY_VIDEOID '00003VXO'
end


-----------------------20170901-----------------------
if @case = 201709010943 begin --標記上傳未更新狀態
	exec SP_Q_INFO_BY_VIDEOID '00003VXK'
end

if @case = 201708311340 begin
	select * from TBTSM_JOB where FNTSMJOB_ID = 251311 --/hakkatv/HVideo/2017/2017778/G201777800230001.mxf
	exec SP_Q_INFO_BY_VIDEOID '00002LCZ' --G201622700100001 \\mamstream\MAMDFS\ptstv\HVideo\2016\2016227\G201622700100001.mxf /ptstv/HVideo/2016/2016227/G201622700100001.mxf
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 251311
	update TBTSM_JOB set FSSOURCE_PATH = '/ptstv/HVideo/2016/2016227/G201622700100001.mxf', FNSTATUS = 1 where FNTSMJOB_ID = 251311
end

if @case = 201709011355 begin --標記上傳未更新狀態
	exec SP_Q_INFO_BY_VIDEOID '00003VH5' --O
end

if @case = 201709011422 begin --轉檔成功搬檔失敗 但原始檔已不在
	select * from TBLOG_VIDEO where FSBRO_ID = '201708290075' --S, 170030 G201011300240012
	select * from TBTRANSCODE where FNTRANSCODE_ID = 170030 --4
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201011300240012'
end

if @case = 201709011438 begin
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201708310119'
	select * from TBUSERS where FSUSER_ID = '50598' --prg50598
	/*
	R 小魔女的華麗冒險 4 \\mamstream\MAMDFS\ptstv\HVideo\2014\2014828\G201482800040001.mxf offline 
	F 小魔女的華麗冒險 11 \\mamstream\MAMDFS\ptstv\HVideo\2014\2014828\G201482800110002.mxf tapeonly partial 錯誤
	prg50598
	*/
end

if @case = 201709011641 begin
	exec SP_Q_INFO_BY_VIDEOID '00003VF6' --N
	exec SP_Q_INFO_BY_VIDEOID '00003VFJ' --N
	exec SP_Q_INFO_BY_VIDEOID '00003VFI' --N
	exec SP_Q_INFO_BY_VIDEOID '00003VFK' --N
	exec SP_Q_INFO_BY_VIDEOID '00003VZU' --N

	exec SP_Q_INFO_BY_VIDEOID '00003VV4' --N
	exec SP_Q_INFO_BY_VIDEOID '00003VX5' --四問四吵 2017-09-04 201708310148 pts PTS_AP 無資料 G201802300010001
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'C' where FSFILE_NO = 'G201802300010001'  --手動上傳

	exec SP_Q_INFO_BY_VIDEOID '00003ONL' --S 正在傳 N
end

if @case = 201709011712 begin
	select * from VW_PLAYLIST_FROM_NOW where FSCHANNEL_ID = '08' and FDDATE = '2017-09-05' and NAME NOT LIKE '%新聞%' order by FSPLAY_TIME
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00003SS4'
end

if @case = 20179011837 begin --切換 Approved

	if OBJECT_ID(N'tempdb.dbo.#tmp_TBSYSTEM_CONFIG', N'U') IS NOT NULL
		DROP TABLE #tmp_TBSYSTEM_CONFIG

	select FSSYSTEM_CONFIG into #tmp_TBSYSTEM_CONFIG from TBSYSTEM_CONFIG

	select * from #tmp_TBSYSTEM_CONFIG

		
	update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<STB_APPROVED  Desc="待播區">\\10.13.200.6\Approved\</STB_APPROVED>',
		'<STB_APPROVED  Desc="待播區">\\10.13.200.22\mamnas1\Approved\</STB_APPROVED>'
	)

	update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<CompletePath>\\10.13.200.6\Approved\</CompletePath>',
		'<CompletePath>\\10.13.200.22\mamnas1\Approved\</CompletePath>'
    )

	--restart HD_FILE_MANAGER
	--check PGM_QC

	select * from TBSYSTEM_CONFIG

	exec SP_Q_INFO_BY_VIDEOID '00003VX5' --四問四吵
end

if @case = 201709031458 begin
	select * from TBTSM_JOB where CAST(FDREGISTER_TIME as DATE) = '2017-09-03' and FNSTATUS <> 3 order by FDREGISTER_TIME
	--執行plink得到錯誤的回傳內容：指令 = dsmrecall /ptstv/HVideo/2016/20160700148/P201607001480002.mxf, 參數 = 10.13.210.15 -l root -pw TSMP@ssw0rdTSM ". /root/.bash_profile";"dsmrecall /ptstv/HVideo/2016/20160700148/P201607001480002.mxf", 錯誤訊息 = ANS1005I TCP/IP read error on socket = 5, errno = 104, reason : 'Connection reset by peer'.

	select FNSTATUS, count(FNSTATUS) from TBTSM_JOB where CAST(FDREGISTER_TIME as DATE) = '2017-09-03'  Group by FNSTATUS --3 = 343 5 383
	update TBTSM_JOB set FNSTATUS = 1 where FDREGISTER_TIME between '2017-09-03 14:00' and '2017-09-03 15:00' and FNSTATUS = 5 --114 -> 5 -> 0
	select FNSTATUS, count(FNSTATUS) from TBTSM_JOB where CAST(FDREGISTER_TIME as DATE) = '2017-09-03'  Group by FNSTATUS --2 = 23 3 = 616 5 = 87 6 = 1

	select * from TBTSM_JOB where FDLASTUPDATE_TIME > '2017-09-03 18:30' --144
	update TBTSM_JOB set FNSTATUS = 1 where FDREGISTER_TIME between '2017-09-03 15:00' and '2017-09-03 15:30' and FNSTATUS = 5 --237

	exec SP_Q_INFO_BY_VIDEOID '00003VWV' --0901 O


	select * from TBLOG_VIDEO where FSFILE_NO = 'P201703000840001' --00003F3R
	exec SP_Q_INFO_BY_VIDEOID '00003F3R'

	select FSFILE_PATH_H, FSVIDEO_PROG from VW_GET_IN_TAPE_LIBRARY_LIST_MIX_TO_APPROVED where   --921

	select * from VW_GET_IN_TAPE_LIBRARY_LIST_MIX_TO_APPROVED  --沒有播出時間

	exec SP_Q_INFO_BY_VIDEOID '00003K5R'
	select * from TBLOG_VIDEO where FSFILE_NO = 'P201708004630001'
end

if @case = 201709041014 begin --夢裡的一千道牆 7 0623 轉檔 Anystream job 已經不見 done
	select * from TBLOG_VIDEO where FSJOB_ID = '165086' --R 00003OLJ
	exec SP_Q_INFO_BY_VIDEOID '00003OLJ'
	select * from TBTRANSCODE where FNTRANSCODE_ID = 165086

	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201766900070001'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201766900070001'
end

if @case = 2017090041450 begin --標記後狀態未更新
	exec SP_Q_INFO_BY_VIDEOID '00003VVJ'
	exec SP_Q_INFO_BY_VIDEOID '00003VVG'
end

if @case = 201709041456 begin --2017-07-03 13:56:17.477 審核通過未轉檔
	exec SP_Q_INFO_BY_VIDEOID '00003QKT' --2017福氣來了運動預告2 P201707000020001
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201707000020001' --2017-09-04 ch07
	--因為每天都有播所以位在刪除清單

	exec SP_Q_INFO_BY_VIDEOID '00003V8Y' --S TSM 處理中 169927 2017-08-29 15:06:35.267 家家有好食 22 2017-09-05 ch 07 --> T
	select * from TBTRANSCODE where FNTRANSCODE_ID = 169927 --1 ANSID = -1
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201777800220002'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201777800220002'
end


if @case = 201709041616 begin --轉檔不過無法入庫
	exec SP_Q_INFO_BY_VIDEOID '00003U46' --第39屆威廉瓊斯盃國際籃球邀請賽 ch12 2017-09-04
	--G201797400010002 \\mamstream\MAMDFS\ptstv\HVideo\2017\2017974\G201797400010002.mxf, \\mamstream\MAMDFS\Media\Lv\ptstv\2017\2017974\G201797400010002.mp4
	exec SP_Q_INFO_BY_VIDEOID '00003U48' --第39屆威廉瓊斯盃國際籃球邀請賽 2 ch12

	select * from TBLOG_VIDEO where FSFILE_NO = 'G201523800010015' --\\mamstream\MAMDFS\ptstv\HVideo\2015\2015238\G201523800010015.mxf 手動入庫
end


if @case = 201709041704 begin
	exec SP_Q_INFO_BY_VIDEOID '00003TSV' --鬧熱打擂台(120分鐘版)第74集預告 T 2017-09-07 ch07
end


if @case = 201709041725 begin --搬檔程式未處理標記上傳  檔名是八碼
	exec SP_Q_INFO_BY_VIDEOID '00003VYY' --水果冰淇淋 1538 201709010040
	exec SP_Q_INFO_BY_VIDEOID '00003VYU' --週日狂熱夜(40-52集為HD版本)
end

if @case = 201709041802 begin
	exec SP_Q_INFO_BY_VIDEOID '00003W3N' --2017-09-04 15:15:52.293
end

if @case = 201709041803 begin --主控無法播出 直接拿檔案上去正常
	exec SP_Q_INFO_BY_VIDEOID '00003AI5' --T 4.9G --猜猜我有多愛你(海陸腔) 36 2012817 Display size 720x486 4:3
	select * from TBLOG_VIDEO where FSID = '2012817'
end

if @case = 201709042132 begin --歡迎光臨 19 轉檔卡在 TSM 處理中 done
	exec SP_Q_INFO_BY_VIDEOID '00003R53' --S 166159 G201719400190002 歡迎光臨 19  2017-09-09 ch13
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201719400190002'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201719400190002'
	select * from TBTRANSCODE where FNTRANSCODE_ID = 166159 --ANSID = -1
end

if @case = 201709042149 begin --爸媽囧很大 338
	exec SP_Q_INFO_BY_VIDEOID '00002XVF' --G200928503380005
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G200928503380005' --4 段 
	/*
	01:00:00;00 01:10:25;00
	01:10:35;00	01:29:02;00
	01:29:12;00	01:42:54;00
	01:43:04;00	01:51:00;02
	*/
end

if @case = 201709051027 begin --昨天 2017-09-17 ch 14  2017-08-28 標記 已上傳到 HDUpload  但是檔案不完整
	exec SP_Q_INFO_BY_VIDEOID '00003U5O'  --FCSTATUS = E G201553800010004 -> C
end

if @case = 201709051105 begin
	exec SP_Q_INFO_BY_VIDEOID '00003W8J' --日頭下•月光光(30分鐘版) 10
end

if @case = 201709051121 begin --檔案有問題，但是尚未轉檔無法覆蓋，直接覆蓋舊檔
	select * from TBLOG_VIDEO where FSID = '2017375' and FNEPISODE = 2 --G201737500020002 B 00003VOY
	select * from TBPROG_M where FSPROG_ID = '2017375' --熊星人和地球人
	exec SP_Q_INFO_BY_VIDEOID '00003VOY' --熊星人與地球人 2 2017-09-08 ch62
end

if @case = 201709051407 begin --轉檔完成檔案未下載 
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201708210115' --蘆葦之歌(主題之夜導讀版) 轉檔完成檔案未下載  \\mamstream\MAMDFS\ptstv\HVideo\2017\2017958\G201795800010001.mxf
	select * from TBUSERS where FSUSER_ID = '71264' --賴慕寧 ninalai
end

if @case = 201709051437 begin --標記上傳狀態未更新
	exec SP_Q_INFO_BY_VIDEOID '00003VVN'
	exec SP_Q_INFO_BY_VIDEOID '00003WC8'
	exec SP_Q_INFO_BY_VIDEOID '00003VOY'
	exec SP_Q_INFO_BY_VIDEOID '00003V4E'
	exec SP_Q_INFO_BY_VIDEOID '00003WG5'
	exec SP_Q_INFO_BY_VIDEOID '00003WH0'
end

if @case = 201709051548 begin --回朔檔案已到待播區，狀態仍未更新
	exec SP_Q_INFO_BY_VIDEOID '00002A4K' --T 萬惡高譚市 16 G201634500160003
	exec SP_Q_INFO_BY_VIDEOID '00002A4M' --T 萬惡高譚市 17 G201634500170002
	exec SP_Q_INFO_BY_VIDEOID '00002B6D' --T 萬惡高譚市 19 尚未搬到
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201634500160003'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201634500170002'
end

if @case = 201709061153 begin  --調用：Anystream 註冊錯誤
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201709040095' --\\mamstream\MAMDFS\ptstv\HVideo\2018\2018022\G201802200030002.mxf
	select * from TBUSERS where FSUSER_ID = '05224' --pub50202 蘇慧真
end

if @case = 201709061406 begin
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201709040245' --\\mamstream\MAMDFS\ptstv\HVideo\2007\2007478\G200747800010017.mxf offline
end

if @case = 201709070906 begin --執行plink得到錯誤的回傳內容：指令 = dsmrecall /ptstv/HVideo/2016/20160700248/P201607002480001.mxf, 參數 = 10.13.210.15 -l root -pw TSMP@ssw0rdTSM ". /root/.bash_profile";"dsmrecall /ptstv/HVideo/2016/20160700248/P201607002480001.mxf", 錯誤訊息 = FATAL ERROR: Server unexpectedly closed network connection
	select * from TBTSM_JOB where FNTSMJOB_ID = 255516 --5 /ptstv/HVideo/2016/20160700248/P201607002480001.mxf
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 255516
	select * from TBTSM_JOB where FNTSMJOB_ID = 255581 --5 執行plink得到錯誤的回傳內容：指令 = dsmrecall /hakkatv/HVideo/2015/2015582/G201558202320001.mxf, 參數 = 10.13.210.15 -l root -pw TSMP@ssw0rdTSM ". /root/.bash_profile";"dsmrecall /hakkatv/HVideo/2015/2015582/G201558202320001.mxf", 錯誤訊息 = FATAL ERROR: Server unexpectedly closed network connection
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 255581
end

if @case = 201709071106 begin
	exec SP_Q_INFO_BY_VIDEOID '00003WMH' --P201312002530002 X 201709060097
end

if @case = 201709071151 begin --轉檔成功搬檔失敗 無法置換  --> Anystream Reschedule
	exec SP_Q_INFO_BY_VIDEOID '00003WI6' --藝術很有事（106年內容產製與運用計畫）2017-09-09 12 R 170708 G201775700050001
	select * from TBTRANSCODE where FNTRANSCODE_ID = 170708 --4 ANSID = 120068 2017-09-06 11:04:38.260  255434
	exec SP_Q_TBTRACKINGLOG_BY_DATE '170708', '120068', '255434', 'G201775700050001', '0003WI6', '2017-09-07', NULL
	--plink高解搬檔失敗：JobID 170708/10.13.210.15 -l root -pw TSMP@ssw0rdTSM "cp -r /mnt/HDUpload/003WI6.mxf  /ptstv/HVideo/2017/2017757/G201775700050001.mxf"/cp: cannot stat `/mnt/HDUpload/003WI6.mxf': No such file or directory
	--003WI6.MXF -> 003WI6.mxf
	select * from TBLOG_VIDEO where FSID = '2017757' and FNEPISODE = 5
end

if @case = 201709080926 begin --其他影片 出現在檔案交播搬檔
	select * from TBLOG_VIDEO where FSFILE_NO = 'P201606006600005' --FSVIDE_PROG = '' 20160600660, 201611080002 FSARC_TYPE = 26 其他影片
	select * from TBPGM_PROMO where FSPROMO_ID = '20160600660' --PTS1_公視+7服務10秒字卡HD
	select * from VW_PLAYLIST_FROM_NOW where FDDATE = '2017-09-09' order by FSPLAY_TIME
	--無法取得檔案狀態
	select * from TBTRACKINGLOG where FDTIME > '2017-09-08 09:50' order by FDTIME
	--[PTSMAM2] 執行 plink 指令的結果碰到錯誤訊息：arug = 10.13.210.15 -l root -pw TSMP@ssw0rdTSM ". /root/.bash_profile";"dsmls /hakkatv/HVideo/2016/20160600611/P201606006110001.mxf | sed '1,8d'", msg = FATAL ERROR: Server unexpectedly closed network connection

	select * from TBLOG_VIDEO where FSFILE_NO = 'P201709002260001' --00003WQG --201709070181
	exec SP_Q_INFO_BY_VIDEOID '00003WQG' --no schedule

	select * from VW_PLAYLIST_FROM_NOW where FDDATE = '2017-09-16' and FSVIDEO_ID = ''  order by FSPLAY_TIME  --不管如合都要有 FSVIDEO_ID
	select * from VW_PLAYLIST_FROM_NOW where FDDATE = '2017-09-16'  order by FSPLAY_TIME

	select * from TBPGM_COMBINE_QUEUE where FDDATE = '2017-09-09' and FSCHANNEL_ID = '12'
	select * from TBPGM_ARR_PROMO where FDDATE = '2017-09-09' and FSCHANNEL_ID = '12' order by FSPLAYTIME  --P201709002260001, 20170900226, 06:59:13:16
	select * from TBPGM_PROMO where FSPROMO_ID = '20170900226' --麻醉風暴S2 Never Give Up 就在今晚版-許瑋甯

	select * from TBPGM_ARR_PROMO where FSFILE_NO = 'P201709002260001'
end

if @case = 201709081142 begin
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201709070061'
	select * from TBTSM_JOB where FNTSMJOB_ID = 255700 --3
	/*
	\\mamstream\MAMDFS\ptstv\HVideo\2017\2017299\G201729900010014.mxf
	\\mamstream\MAMDFS\ptstv\HVideo\2017\2017190\G201719000010001.mxf
	\\mamstream\MAMDFS\ptstv\HVideo\2017\2017191\G201719100010013.mxf
	\\mamstream\MAMDFS\ptstv\HVideo\2017\2017193\G201719300010001.mxf
	\\mamstream\MAMDFS\ptstv\HVideo\2017\2017189\G201718900010014.mxf
	\\mamstream\MAMDFS\ptstv\HVideo\2017\2017192\G201719200010001.mxf
	*/
	select * from TBUSERS where FSUSER_ID = '05065' --adm3036
end

if @case = 201709081209 begin
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201709070021'
	/*
\\mamstream\MAMDFS\ptstv\HVideo\2017\2017194\G201719400090001.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2017\2017194\G201719400100001.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2017\2017194\G201719400120001.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2017\2017194\G201719400140001.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2017\2017194\G201719400150001.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2017\2017194\G201719400170001.mxf
*/
	select * from TBUSERS where FSUSER_ID = '05224'  --pub50202

end


if @case = 201709081225 begin --宏觀頻道 purge 問題
	 select * from VW_GET_CHECK_COMPLETE_LIST_MIX_MACRO
	 exec SP_Q_INFO_BY_VIDEOID '00001MNC'
end

if @case = 201709081529 begin
	select * from TBLOG_VIDEO where CAST(FDCREATED_DATE as DATE) >= '2017-09-07' and FSARC_TYPE = '026' order by FDCREATED_DATE
end

if @case = 201709091913 begin
	exec SP_Q_INFO_BY_VIDEOID '00002N8U' --家事提案 2017-09-10 ch13 --\\mamstream\MAMDFS\ptstv\HVideo\2014\2014047\G201404700010015.mxf
	exec SP_Q_INFO_BY_VIDEOID '00002FJ3' --殘響世界 \\mamstream\MAMDFS\ptstv\HVideo\2016\2016671\G201667100010006.mxf
	exec SP_Q_INFO_BY_VIDEOID '00001OI2' --前輩，好樣的! 2 \\mamstream\MAMDFS\ptstv\HVideo\2015\2015241\G201524100020006.mxf
end

if @case = 201709110918 begin  --錯誤訊息 = FATAL ERROR: Server unexpectedly closed network connection
	select * from TBTSM_JOB where FNTSMJOB_ID = 256016 --5 執行plink得到錯誤的回傳內容：指令 = dsmrecall /ptstv/HVideo/2015/2015100/G201510000090010.mxf, 參數 = 10.13.210.15 -l root -pw TSMP@ssw0rdTSM ". /root/.bash_profile";"dsmrecall /ptstv/HVideo/2015/2015100/G201510000090010.mxf", 錯誤訊息 = FATAL ERROR: Server unexpectedly closed network connection
	select * from TBTSM_JOB where FNTSMJOB_ID = 256118 --5
	select * from TBTSM_JOB where FNTSMJOB_ID = 256232 --5
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 256016
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 256118
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 256232
end

if @case = 201709111200 begin
	exec SP_Q_INFO_BY_VIDEOID '00003VXP' --以標記上傳 TBPTS_AP 無上傳紀錄 手動上傳
end

if @case = 201709111115 begin
	exec SP_Q_INFO_BY_VIDEOID '00003WHY' --2017福氣來了 174 R --> reschedule
	select * from TBTRANSCODE where FNTRANSCODE_ID = 170739 --4 ANSID = 120090 2017-09-06 14:59:24.870

	exec SP_Q_INFO_BY_VIDEOID '000036OL' --客客客棧 6 Tapeoffline \\mamstream\MAMDFS\hakkatv\HVideo\2017\2017122\G201712200060001.mxf
end

if @case = 201709130930 begin  --節目以被置換
	exec SP_Q_INFO_BY_VIDEOID '00003WWM' --X 聽說桐島退社了 2017-09-16 ch14 D, 2018031
	select * from TBLOG_VIDEO where FSID = '2018031' --G201803100010002, B. 00003X20
	exec SP_Q_INFO_BY_VIDEOID '00003X20'
	exec SP_Q_INFO_BY_VIDEOID '00003WQN' --X, D PTS3主題之夜PROMO氣候變遷逆轉勝？_new2, 2017-09-17 ch14, 20170900233
	select * from TBLOG_VIDEO where FSID = '20170900233' --00003WZQ
	exec SP_Q_INFO_BY_VIDEOID '00003WZQ'
end


if @case = 201709130940 begin  --檢索不到
	select * from TBPROG_M where FSPGMNAME like '%一字千金%' --2015404 一字千金, 2015599	一字千金闔家樂, 2016618 一字千金榜中榜
	select * from TBLOG_VIDEO where FSID = '2015404'

	select * from TBLOG_VIDEO
end


if @case = 201709131132 begin  --麻醉風暴2 第10集劇照入庫單無流程編號
	select * from TBLOG_PHOTO where FDCREATED_DATE > '2017-09-13' --07210
	select * from TBUSERS where FSUSER_ID = '07210' --李淑屏
	select * from TBLOG_PHOTO where FSARC_ID = '201709130007' --10 record 2017182
	select * from TBLOG_PHOTO where FSARC_ID = '201709130008'
	select * from TBLOG_PHOTO where FSID = '2017182' and FNEPISODE = 10 order by FSFILE_NO
	select * from TBLOG_PHOTO where FSARC_ID = '201709130119'
end

if @case = 201709131159 begin
	select * from TBLOG_VIDEO where FSBRO_ID = '201303210005'
end

if @case = 201709131422 begin --標記上傳未更新狀態
	exec SP_Q_INFO_BY_VIDEOID '00003U0S' --家家有好食 32
	exec SP_Q_INFO_BY_VIDEOID '00003UQO' --好奇的凱米 7
	exec SP_Q_INFO_BY_VIDEOID '00003WQU'
end


if @case = 201709140941 begin --執行plink得到錯誤的回傳內容：指令 = dsmrecall /ptstv/HVideo/2016/20161100699/P201611006990001.mxf, 參數 = 10.13.210.15 -l root -pw TSMP@ssw0rdTSM ". /root/.bash_profile";"dsmrecall /ptstv/HVideo/2016/20161100699/P201611006990001.mxf", 錯誤訊息 = FATAL ERROR: Server unexpectedly closed network connection
	select * from TBTSM_JOB where FNTSMJOB_ID = 256726 --5
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 256726
	select * from TBTSM_JOB where FNTSMJOB_ID = 256822 --5
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 256822
end

if @case = 201709141418 begin --轉檔完成檢索不到
	select * from TBLOG_VIDEO where FSJOB_ID = '169027' --no record  G201756100080001, B, 00003T9Y
	select * from TBPROG_M where FSPGMNAME like '%麼(介)%' --麼(介)麼(介), 2017561
	select * from TBLOG_VIDEO where FSID = '2017561' and FNEPISODE = 8 --201708020095
	exec SP_Q_INFO_BY_VIDEOID '00003T9Y' --O, 2017-08-18 14:40:28.477   
	exec SP_Q_TBTRACKINGLOG_BY_DATE 169027, 118601, 248500, 'G201756100080001', '003T9Y',  '2017-09-14' --執行網路服務(WSBROADCAST.GetTBLOG_VIDEO_PATH_BYJOBID)時發生錯誤，轉檔編號：169027
	--update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201756100080001' --done

	--小O事件簿 轉檔成功搬檔失敗
	select * from TBLOG_VIDEO where FSJOB_ID = '170874' --R, G201553701060001 00003WML  done
	select * from TBTRANSCODE where FNTRANSCODE_ID = 170874 --4, ANSID = 120196 2017-09-07 15:48:39.227  reschedule
	exec SP_Q_INFO_BY_VIDEOID '00003WML'
end

if @case = 201709140613 begin --送播單建一半當掉資料不完整
	select * from TBPROG_M where FSPGMNAME like '%新住民學成語%' --2017982
	select * from TBLOG_VIDEO where FSID = '2017982' and FNEPISODE = 9 --201709140194, G201798200090001
	select * from TBBROADCAST where FSBRO_ID = '201709140194' --no record
	exec SP_D_TBLOG_VIDEO_BYFILENO 'G201798200090001'
end


if @case = 201709150917 begin
	select * from TBTSM_JOB where FNTSMJOB_ID = 257115 --5 執行plink得到錯誤的回傳內容：指令 = dsmrecall /hakkatv/HVideo/2008/2008676/G200867600340003.mxf, 參數 = 10.13.210.15 -l root -pw TSMP@ssw0rdTSM ". /root/.bash_profile";"dsmrecall /hakkatv/HVideo/2008/2008676/G200867600340003.mxf", 錯誤訊息 = FATAL ERROR: Server unexpectedly closed network connection
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 257115
end


if @case = 201709151425-201708171905 begin
	if OBJECT_ID(N'tempdb.dbo.#tempVideo', N'U') IS NOT NULL
		DROP TABLE #tempVideo

	CREATE TABLE  #tempVideo (
		VIDEO_ID varchar(20)
	)

	--BULK INSERT #tempImport from '\\10.13.200.5\OtherVideo\temp\fileno.txt' WITH
	BULK INSERT #tempVIDEO from 'c:\temp\video.txt' WITH
	(
		ROWTERMINATOR = '\n'
	);
	
	/*
	select SUBSTRING(VIDEO_ID, 1, 6), B.FSPROG_NAME, B.FSPLAY_TIME, B.FSCHANNEL_ID ,B.FSPLAY_TIME  from #tempVideo A left join TBPGM_COMBINE_QUEUE B
	on SUBSTRING(A.VIDEO_ID, 1, 6) = SUBSTRING(B.FSVIDEO_ID, 3, 6) order by B.FDDATE, B.FSPLAY_TIME
	*/

	select SUBSTRING(A.VIDEO_ID, 1, 6), B.NAME, B.FDDATE, C.FSCHANNEL_NAME, B.FSPLAY_TIME from #tempVideo A left join 
	(select FSVIDEO_ID VIDEO_ID, FSPROG_NAME NAME, FDDATE, FSCHANNEL_ID, FSPLAY_TIME from TBPGM_COMBINE_QUEUE union (select A.FSVIDEO_ID VIDEO_ID, B.FSPROMO_NAME NAME, A.FDDATE, A.FSCHANNEL_ID, FSPLAYTIME from TBPGM_ARR_PROMO A left join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID)) B
	on SUBSTRING(A.VIDEO_ID, 1, 6) = SUBSTRING(B.VIDEO_ID, 3, 6) left join TBZCHANNEL C on B.FSCHANNEL_ID = C.FSCHANNEL_ID
	order by B.FDDATE, B.FSCHANNEL_ID, B.FSPLAY_TIME
	


	select SUBSTRING(VIDEO_ID, 1, 6), B.FSPROG_NAME, B.FSPLAY_TIME, B.FSCHANNEL_ID ,B.FSPLAY_TIME  from #tempVideo A left join TBPGM_COMBINE_QUEUE B 
	on SUBSTRING(A.VIDEO_ID, 1, 6) = SUBSTRING(B.FSVIDEO_ID, 3, 6) order by B.FDDATE, B.FSPLAY_TIME

	--UNION
	select FSVIDEO_ID VIDEO_ID, FSPROG_NAME NAME from TBPGM_COMBINE_QUEUE union (select A.FSVIDEO_ID VIDEO_ID, B.FSPROMO_NAME NAME from TBPGM_ARR_PROMO A left join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID)
	select * from TBPGM_ARR_PROMO
end

if @case = 201709151425 begin --調用 PARTIAL 處理錯誤
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201709140018' --公主與王子(白天不宜) \\mamstream\MAMDFS\ptstv\HVideo\2012\2012119\G201211900010006.mxf
	select * from TBUSERS where FSUSER_ID = '05065' --adm3036
end

if @case = 201709181036 begin
	exec SP_Q_INFO_BY_VIDEOID '00003XD2' --2017 09/19 pts1 menu 1800, status T, O, but not found in Approved 
	--拷貝失敗,錯誤訊息:拒絕存取路徑 '\\mamstream\MAMDFS\ptstv\HVideo\2017\20170900385\P201709003850001.mxf'。
	--cp /ptstv/HVideo/2017/20170900385/P201709003850001.mxf /mnt/mamnas1/Approved/003XD2.mxf

	exec SP_Q_INFO_BY_VIDEOID '00003W03' --PTS1_下課花路米 同理心大考驗 #13 我的街頭朋友 promo 2017-09-19

	exec SP_Q_INFO_BY_VIDEOID '00003XD3' --\\mamstream\MAMDFS\ptstv\HVideo\2017\20170900386\P201709003860001.mxf
	exec SP_Q_INFO_BY_VIDEOID '00002TTL'
	exec SP_Q_INFO_BY_VIDEOID '00003BUK' --2016PTS1 RM 【公視人生劇展】母親練習曲

	exec SP_Q_INFO_BY_VIDEOID '00003VWQ' --【公視2台兒少館】熊星人和地球人_2016PTS2 RM

	exec SP_Q_INFO_BY_VIDEOID '00003WRZ'
	exec SP_Q_INFO_BY_VIDEOID '00003XFA'

	exec SP_Q_INFO_BY_VIDEOID '00002N6W' --台灣美樂地MV-萬安晚安HD 2017-09-19
	exec SP_Q_INFO_BY_VIDEOID '00003X91' --忙碌鎮的神秘事件 2017-09-21
	exec SP_Q_INFO_BY_VIDEOID '00002NUX' --唸謠-青蛙-饒平腔2 2017-09-21

	exec SP_Q_INFO_BY_VIDEOID '00003I6V'

	
end

if @case = 201709181353 begin --無破口
	select * from TBPROG_M where FSPGMNAME like '%我在墾丁%' --2017162, 2006171 我在墾丁*天氣晴(破口版)
	select * from TBLOG_VIDEO where FSID = '2017162' --G201716200010003
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G201716200010003'
end

if @case = 201709181519 begin --要置換，但是檔案未到待審區，無法退單
    exec SP_Q_INFO_BY_VIDEOID '00003XFS' --PTS3 【國際換日線】閃電俠 第2季 週間版 2017-09-22 P201709004140001, 20170900414
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201709004140001'
	select * from TBLOG_VIDEO where FSID = '20170900414' --00003XKT
	exec SP_Q_INFO_BY_VIDEOID '00003XED' --PTS3 【國際換日線】閃電俠 第2季 開始版
	exec SP_Q_INFO_BY_VIDEOID '00003XLM' 
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201709003990003'
end

if @case = 201709181541 begin --置換單填到送帶轉檔，居然還可以置換
	select * from TBLOG_VIDEO where FSFILE_NO = 'P201709003990002'
	select * from TBLOG_VIDEO where FSID = '20170900399'
	exec SP_D_TBLOG_VIDEO_BYFILENO 'P201709003990002'
end

if @case = 201709181752 begin
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201600700040005' --2016007, #4, 0000255T
	exec SP_Q_INFO_BY_VIDEOID '0000255T' --\\mamstream\MAMDFS\ptstv\HVideo\2016\2016007\G201600700040005.mxf
	select * from TBLOG_VIDEO where FSID = '2016007' and FNEPISODE = 4 --\\mamstream\MAMDFS\ptstv\HVideo\2016\2016007\G201600700040003.mxf
	select * from TBBOOKING_DETAIL where FSFILE_NO like '%G201600700040005%' --201708070114
	select * from TBBOOKING_MASTER where FSBOOKING_NO = '201708070114' --50391
	select * from TBUSERS where FSUSER_ID = '50391' --吳娟寧 prg50391
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201708070114'
end

if @case = 201709190901 begin --cannot recall cannot find tape info
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201718100170001' --\\mamstream\MAMDFS\hakkatv\HVideo\2017\2017181\G201718100170001.mxf, 00003P2D
end


if @case = 201709191046 begin --標記上傳問題
	exec SP_Q_INFO_BY_VIDEOID '00003XLG' --PTS1_行走TIT-66集PROMO(重) 2017-09-20 00003XLG
	exec SP_Q_INFO_BY_VIDEOID '00003XF8' --金鐘預測活動「3王5后大對決 誰會抱走金鐘大獎？」
	exec SP_Q_INFO_BY_VIDEOID '00003VCQ' --R 我的No1表情#10promo 2017-09-22 --nee reschdeule
	exec SP_Q_INFO_BY_VIDEOID '00003BY8' 
end

if @case = 201709191102 begin --調用問題
	select * from TBBOOKING_MASTER where FSBOOKING_NO = '201709190047'
	select * from TBBOOKING_DETAIL where FSBOOKING_NO = '201709190047'
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201011300410001' --\\mamstream\MAMDFS\ptstv\HVideo\2010\2010113\G201011300410001.mxf 2010113
	select * from TBLOG_VIDEO where FSID = '2010113' and FNEPISODE = 38
end

if @case = 201709191153 begin --dsmrecall 很久
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201718100170001'
end


if @case = 201709191210 begin --更改分級週節目表未更新
	select * from TBPROG_M where FSPGMNAME like '%閃電俠%' --閃電俠第二季 2017196
	select * from PTSPROG.PTSProgram.dbo.TBPROG_D where FSPROGID = '2017196'
	select * from TBPROG_D where FSPROG_ID = '2017196' --FSPROGGRADEID = 03
end

if @case = 201709191448 begin
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201709110118' 
	--\\mamstream\MAMDFS\ptstv\HVideo\2015\2015404\G201540400970001.mxf
	--\\mamstream\MAMDFS\ptstv\HVideo\2015\2015404\G201540400960002.mxf
	select * from TBUSERS where FSUSER_ID = '60167' --prg60167
end

if @case = 201709191518 begin --標記上傳失敗
	exec SP_Q_INFO_BY_VIDEOID '00003XKS'
	exec SP_Q_INFO_BY_VIDEOID '00003XMH'  --E 201709190019 重新標記 done
	exec SP_Q_INFO_BY_VIDEOID '00003XML' --done
	exec SP_Q_INFO_BY_VIDEOID '00003XML'
	exec SP_Q_INFO_BY_VIDEOID '00002MM0'

	exec SP_Q_INFO_BY_VIDEOID '00003XMN'
	exec SP_Q_INFO_BY_VIDEOID '00003XML'  --C ->N
	exec SP_Q_INFO_BY_VIDEOID '00003XF8'  --O, T
	exec SP_Q_INFO_BY_VIDEOID '00003XLH'
	exec SP_Q_INFO_BY_VIDEOID '00003XLG' -- O,T
	exec SP_Q_INFO_BY_VIDEOID '00003TTN' --E 201708080071 remark
	exec SP_Q_INFO_BY_VIDEOID '00003XLI' --E 201709180115 remark
	exec SP_Q_INFO_BY_VIDEOID '00003XLK' --E 201709180118
end

if @case = 201709191725 begin
	exec SP_Q_INFO_BY_VIDEOID '00003P2D' --大聲My客風 already in approved
	exec SP_Q_INFO_BY_VIDEOID '00003TLJ' --明天一起去樂園第17集預告0828 2017-09-23, 2017-09-11 14:22 O, T  2017-09-11 14:22 P201708001810001 審核後未到排播區間，卻未刪除
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00003TLJ' order by FDDATE

	exec SP_Q_INFO_BY_VIDEOID '00003V6F' --R 鬧熱打擂台(120分鐘版) 171053 2017-09-23 need reschedule done
	select * from TBTRANSCODE where FNTRANSCODE_ID = 171053 --4 ANSID = 120359

	select * from TBLOG_VIDEO where FSID = '2016527' and FNEPISODE = 77 --G201652700770002 R, 00003XC7, 171647
	select * from TBTRANSCODE where FNTRANSCODE_ID = 171647 --ANSID = 120850, 4
end

if @case = 201709191744 begin --S-1-22-1-0 SMB problem
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201011300380004' --\\mamstream\MAMDFS\ptstv\HVideo\2010\2010113\G201011300380004.mxf
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201011300410001' --\\mamstream\MAMDFS\ptstv\HVideo\2010\2010113\G201011300410001.mxf  S-1-22-1-0 
end


if @case = 201709191746 begin
	exec SP_Q_INFO_BY_VIDEOID '00003XLD' --他們在畢業的前一天爆炸2(重)ep1 2017-09-20 already in approved
end

if @case = 201709201039 begin --權限問題
	select * from  TBLOG_VIDEO where FSFILE_NO = 'P201709003660001' --\\mamstream\MAMDFS\ptstv\HVideo\2017\20170900366\P201709003660001.mxf, 00003X9R, 201709130181
	select * from TBBROADCAST where FSBRO_ID = '201709130181' --2017-09-20 10:12:15.003
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201750101000002'
	select * from TBLOG_VIDEO where FSFILE_NO = 'P201709004280001' --2017-09-20 08:52:36.000
	select FSFILE_PATH_H from TBLOG_VIDEO where FDUPDATED_DATE > '2017-07-19' and FCFILE_STATUS = 'T' order by FDENC_DATE
end

if @case = 201709211033 begin --標記上傳狀態未更新
	exec SP_Q_INFO_BY_VIDEOID '00003V0Z' --第39屆威廉瓊斯盃國際籃球邀請賽 20 2017-09-30
	exec SP_Q_INFO_BY_VIDEOID '00003V1E' --第39屆威廉瓊斯盃國際籃球邀請賽 21 not schedule HDUpload 檔案不完整
	exec SP_Q_INFO_BY_VIDEOID '00003V2M' --第39屆威廉瓊斯盃國際籃球邀請賽 22 E
	exec SP_Q_INFO_BY_VIDEOID '00003V2R' --第39屆威廉瓊斯盃國際籃球邀請賽 23 --檔案不完整
	exec SP_Q_INFO_BY_VIDEOID '00003VCR' --第39屆威廉瓊斯盃國際籃球邀請賽 24	
end

if @case = 201709211139 begin --你所不知的創意科技 看看是否有失敗
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201709140160'
end

if @case = 201709211146 begin --FSSUPERVISOR = N
	select * from TBLOG_VIDEO where FSFILE_NO in ('G201805100010001', 'G201805000010001', 'G201805300010003') --2018050, 2018051, 2018053, 201708210104, 201708310091, 201709130122
	select * from TBPROG_D where FSPROG_ID in ('2018050', '2018051', '2018053')
	select * from TBBOOKING_DETAIL where FSFILE_NO in ('G201805100010001', 'G201805000010001', 'G201805300010003') order by FSBOOKING_NO
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201709210010' --60061, FLOWId = 72142
	select * from TBUSERS where FSUSER_ID = '60061' --洪潤德

end

if @case = 201709211202 begin --標記上傳狀態未更新
	exec SP_Q_INFO_BY_VIDEOID '00003UQP'
	exec SP_Q_INFO_BY_VIDEOID '00003VL5'
end

if @case = 201709211205 begin --奧林P客 910 2017-09-20 QC 通過，檔案在 Approved 但是 9/21 檔案不見 G200618609100001
	exec SP_Q_INFO_BY_VIDEOID '00003XOJ' --2017-09-22 奧林P客 910
	exec SP_Q_INFO_BY_VIDEOID '00003TLK' --明天一起去樂園第18集預告0828 2017-09-25
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00003XQJ' --P201709005140001
	exec SP_Q_INFO_BY_VIDEOID '00003XQJ'
end

if @case = 201709211427 begin
	exec SP_Q_INFO_BY_VIDEOID '00003XT4' --熊星人和地球人E5 promo R 171961, 2017-09-22 ch12, P201709005260001
	select * from TBTRANSCODE where FNTRANSCODE_ID = 171961 --ANSID = -1 2017-09-20 18:26:37.410
	select * from TBTSM_JOB where FNTSMJOB_ID = 258302 --FNSTATUS = 3
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201709005260001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201709005260001'

	exec SP_Q_INFO_BY_VIDEOID '00003X2H' --藝術很有事（106年內容產製與運用計畫） 2017-09-22 ch12
end

if @case = 201709211622 begin --送播單填錯無法刪除
    select * from TBLOG_VIDEO where FSBRO_ID = '201708250045' --G000015908440001
	exec SP_D_TBLOG_VIDEO_BYFILENO 'G000015908440001'
end

if @case = 2017092117642 begin
	select * from TBTSM_JOB where FNTSMJOB_ID in (258540, 258539, 258538, 258537, 258536) --2
	select * from TBTSM_JOB where CAST(FDREGISTER_TIME as DATE) = '2017-09-21' and FNSTATUS <> 3 order by FNTSMJOB_ID
end

if @case = 201709211937 begin
	exec SP_Q_INFO_BY_VIDEOID '00003X6P' --\\mamstream\MAMDFS\ptstv\HVideo\2017\2017375\G201737500040001.mxf
end

if @case = 201709221013 begin --調用失敗
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201709190066' --2017TIFA-朱宗慶打擊樂團《島．樂》(2-2) \\mamstream\MAMDFS\ptstv\HVideo\2018\2018102\G201810200010001.mxf
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201709190068' --太陽的女兒 \\mamstream\MAMDFS\ptstv\HVideo\2017\2017727\G201772700010001.mxf
	select * from TBUSERS where FSUSER_ID = '60013' --職大偉 prg60013
	select * from TBBOOKING_DETAIL where FSBOOKING_NO = '201709190068' --2017/09/20

	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201709190078' --king8506 \\mamstream\MAMDFS\ptstv\HVideo\2017\2017375\G201737500040001.mxf
	select * from TBBOOKING_DETAIL where FDCHECK_DATE = '2017/09/20' or FDSUPERVISOR_CHECK_DATE = '2017/09/20' order by  FSBOOKING_NO 
end

if @case = 201709221056 begin --reschedule 
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201011300400001' --R, 00003WR0, 171346
	exec SP_Q_INFO_BY_VIDEOID '00003WR0' --就是愛運動 40 2017-09-23 ch12
	select * from TBTRANSCODE where FNTRANSCODE_ID = 171346 --4 ANSID = 120612, 2017-09-13 21:39:26.480
end

if @case = 201709221219 begin  --2017-09-21 17:26 QC 2017-09-21 17:29 Transcode, 
	exec SP_Q_INFO_BY_VIDEOID '00003WQ4' --2017-09-26, ch08
end

if @case = 201709221504 begin --檔案無法 recall 
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201709220046'
	--/ptstv/HVideo/2017/20170600904/P201706009040001.mxf Permission denied  Tapeonly
	--/ptstv/HVideo/2017/20170600905/P201706009050001.mxf permission denied  TapeOnly
	select * from TBLOG_VIDEO where FSFILE_NO = 'P201706009040001'
end

if @case = 201709221612 begin
	exec SP_Q_INFO_BY_VIDEOID '00003XXJ' --C  未到待播區
	exec SP_Q_INFO_BY_VIDEOID '00003Y0Y' --C  未到待播區
end

if @case = 201709221659 begin
	exec SP_Q_INFO_BY_VIDEOID '00002HWO'
	exec SP_Q_INFO_BY_VIDEOID '00003XD0'
	exec SP_Q_INFO_BY_VIDEOID '00003Y02'
end

if @case = 201709221709 begin
	exec SP_Q_INFO_BY_VIDEOID '00003ONQ' --TAIWAN IN FOCUS(短版) 201706120086 未標記上傳
end

if @case = 201709221724 begin
    -- 09/26 pts1 menu 0830 QC 後未轉檔
	exec SP_Q_INFO_BY_VIDEOID '00003XVL' --O, B 2017 09/26 pts1 menu 0830 2017-09-26, 2017-09-21 17:59, P201709005510001 --T
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201709005510001'

	exec SP_Q_INFO_BY_VIDEOID '00003XVM' --O, S 172098  2017 09/26 pts1 menu 1200 卡在TSM處理中 P201709005520001  2017-09-26 ch12
	select * from TBTRANSCODE where FNTRANSCODE_ID = 172098 --ANSID = -1 FNPROCESS_STATUS = 1, 2017-09-21 18:00:24.393
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201709005520001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201709005520001'

	exec SP_Q_INFO_BY_VIDEOID '00003BM9' --十女(白天不宜), 2017-09-26 ch08 \\mamstream\MAMDFS\ptstv\HVideo\2017\2017193\G201719300010001.mxf
end

if @case = 201709231153 begin
	select * from TBSYSTEM_CONFIG
end

if @case = 201709250944 begin
	exec SP_Q_INFO_BY_VIDEOID '00003XKS' --R --第39屆威廉瓊斯盃國際籃球邀請賽 15
	exec SP_Q_INFO_BY_VIDEOID '00003UI9' --R, 第39屆威廉瓊斯盃國際籃球邀請賽 16
end

if @case = 201709251051-201709231153 begin
	if OBJECT_ID(N'tempdb.dbo.#tmp_TBSYSTEM_CONFIG', N'U') IS NOT NULL
		DROP TABLE #tmp_TBSYSTEM_CONFIG

	select FSSYSTEM_CONFIG into #tmp_TBSYSTEM_CONFIG from TBSYSTEM_CONFIG

	select * from #tmp_TBSYSTEM_CONFIG

	--MAMDownload
	update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<BOOKING_RETRIEVE>\\hdsdownload\MAMDownload\</BOOKING_RETRIEVE>',
		'<BOOKING_RETRIEVE>\\10.13.200.25\MAMDownload\</BOOKING_RETRIEVE>'
	)

	--recover
	update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<BOOKING_RETRIEVE>\\10.13.200.25\MAMDownload\</BOOKING_RETRIEVE>',
	 '<BOOKING_RETRIEVE>\\hdsdownload\MAMDownload\</BOOKING_RETRIEVE>'
	)

	select * from TBTRACKINGLOG where FDTIME > '2017-09-25 21:00' order by FDTIME


	-- OtherVideo
	update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<STT_OTHERVIDEO  Desc="其他類上傳區">\\10.13.200.5\OtherVideo\</STT_OTHERVIDEO>',
		'<STT_OTHERVIDEO  Desc="其他類上傳區">\\10.13.200.22\mamnas1\OtherVideo\</STT_OTHERVIDEO>'
    )

	--recover
	update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<STT_OTHERVIDEO  Desc="其他類上傳區">\\10.13.200.22\mamnas1\OtherVideo\</STT_OTHERVIDEO>',
	    '<STT_OTHERVIDEO  Desc="其他類上傳區">\\10.13.200.5\OtherVideo\</STT_OTHERVIDEO>'
    )


	--MAMUpload donw
	update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<STT_UPLOAD  Desc="數位轉檔上傳區位址">\\10.13.200.5\MAMUpload\</STT_UPLOAD>',
		'<STT_UPLOAD  Desc="數位轉檔上傳區位址">\\10.13.200.22\mamnas1\MAMUpload\</STT_UPLOAD>'
    )


	--done
	update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<TranscodeSharingDir>\\10.13.200.5\MAMUpload\MAMTemp\</TranscodeSharingDir>',
		'<TranscodeSharingDir>\\10.13.200.22\mamnas1\MAMUpload\MAMTemp\</TranscodeSharingDir>'
    )


	--MasterCtrl
	update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<LouthXmlPath>\\10.13.200.1\MasterCtrl\MSG\LOUTH\HD-InputXML\</LouthXmlPath>',
		'<LouthXmlPath>\\10.13.200.22\mamnas1\MasterCtrl\MSG\LOUTH\HD-InputXML\</LouthXmlPath>'
    )

	update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<MASTERCONTROL_NAS_ROOT>\\10.13.200.2\MasterCtrl</MASTERCONTROL_NAS_ROOT>',
		'<MASTERCONTROL_NAS_ROOT>\\10.13.200.22\mamnas1\MasterCtrl</MASTERCONTROL_NAS_ROOT>'
    )

	update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<MASTERCONTROL_NAS_TRANSCODE>\\10.13.200.2\MasterCtrl\TRANSCODE\</MASTERCONTROL_NAS_TRANSCODE>',
		'<MASTERCONTROL_NAS_TRANSCODE>\\10.13.200.22\mamnas1\MasterCtrl\TRANSCODE\</MASTERCONTROL_NAS_TRANSCODE>'
    )

	update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<MASTERCONTROL_NAS_SEND>\\10.13.200.2\MasterCtrl\SEND\</MASTERCONTROL_NAS_SEND>',
		'<MASTERCONTROL_NAS_SEND>\\10.13.200.22\mamnas1\MasterCtrl\SEND\</MASTERCONTROL_NAS_SEND>'
    )

	update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<MASTERCONTROL_NAS_MSG>\\10.13.200.2\MasterCtrl\MSG\</MASTERCONTROL_NAS_MSG>',
		'<MASTERCONTROL_NAS_MSG>\\10.13.200.22\mamnas1\MasterCtrl\MSG\</MASTERCONTROL_NAS_MSG>'
    )

	update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<MASTERCONTROL_NAS_FTPLIST>\\10.13.200.2\MasterCtrl\MSG\FTP\FTPList\</MASTERCONTROL_NAS_FTPLIST>',
		'<MASTERCONTROL_NAS_FTPLIST>\\10.13.200.22\mamnas1\MasterCtrl\MSG\FTP\FTPList\</MASTERCONTROL_NAS_FTPLIST>'
    )

	update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<MASTERCONTROL_NAS_LOUTH_XML>\\10.13.200.2\MasterCtrl\MSG\LOUTH\InputXML\</MASTERCONTROL_NAS_LOUTH_XML>',
		'<MASTERCONTROL_NAS_LOUTH_XML>\\10.13.200.22\mamnas1\MasterCtrl\MSG\LOUTH\InputXML\</MASTERCONTROL_NAS_LOUTH_XML>'
    )

	update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<MASTERCONTROL_NAS_FTP_HIGHPRIORITY>\\10.13.200.2\MasterCtrl\MSG\FTP\FTP_HighPriority\</MASTERCONTROL_NAS_FTP_HIGHPRIORITY>',
		'<MASTERCONTROL_NAS_FTP_HIGHPRIORITY>\\10.13.200.22\mamnas1\MasterCtrl\MSG\FTP\FTP_HighPriority\</MASTERCONTROL_NAS_FTP_HIGHPRIORITY>'
    )

	update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<DIR_KEYFRAME Desc="KEYFRAME的暫存資料夾">\\10.13.200.5\MAMUpload\MAMTemp\</DIR_KEYFRAME>',
		'<DIR_KEYFRAME Desc="KEYFRAME的暫存資料夾">\\10.13.200.22\mamnas1\MAMUpload\MAMTemp\</DIR_KEYFRAME>'
    )

	update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<STT_UPLOAD Desc="數位轉檔上傳區位址">\\10.13.200.5\MAMUpload\</STT_UPLOAD>',
		'<STT_UPLOAD Desc="數位轉檔上傳區位址">\\10.13.200.22\mamnas1\MAMUpload\</STT_UPLOAD>'
    )

	update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<ServerName Desc="SMTP 主機">mail.pts.org.tw</ServerName>',
		'<ServerName Desc="SMTP 主機">webmail.pts.org.tw</ServerName>'
    )



		-------------記得要 dump 出來 diff 比對結果
        update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<INGEST_TEMP_DIR_UNC Desc="轉檔暫存區">T:\IngestWorkingTemp\</INGEST_TEMP_DIR_UNC>', 
        '<INGEST_TEMP_DIR_UNC Desc="轉檔暫存區">\\10.13.200.22\mamnas1\IngestWorkingTemp\</INGEST_TEMP_DIR_UNC>')

		update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<INGEST_TEMP_DIR_LINUX Desc="轉檔暫存區">/tcbuffer/18TB/IngestWorkingTemp/</INGEST_TEMP_DIR_LINUX>', 
        '<INGEST_TEMP_DIR_LINUX Desc="轉檔暫存區">/mnt/mamnas1/IngestWorkingTemp/</INGEST_TEMP_DIR_LINUX>')

		update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<METASAN_CHECK_FILE Desc="檢測檔位置">/tcbuffer/18TB/00000000.lock</METASAN_CHECK_FILE>', 
        '<METASAN_CHECK_FILE Desc="檢測檔位置">/mnt/mamnas1/00000000.lock</METASAN_CHECK_FILE>')


		----------recover----------------
		update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<INGEST_TEMP_DIR_UNC Desc="轉檔暫存區">\\10.13.200.22\mamnas1\IngestWorkingTemp\</INGEST_TEMP_DIR_UNC>',
		'<INGEST_TEMP_DIR_UNC Desc="轉檔暫存區">T:\IngestWorkingTemp\</INGEST_TEMP_DIR_UNC>'
        )

		update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<INGEST_TEMP_DIR_LINUX Desc="轉檔暫存區">/mnt/mamnas1/IngestWorkingTemp/</INGEST_TEMP_DIR_LINUX>', 
		'<INGEST_TEMP_DIR_LINUX Desc="轉檔暫存區">/tcbuffer/18TB/IngestWorkingTemp/</INGEST_TEMP_DIR_LINUX>'
        )

		update TBSYSTEM_CONFIG set FSSYSTEM_CONFIG = REPLACE(FSSYSTEM_CONFIG, '<METASAN_CHECK_FILE Desc="檢測檔位置">/mnt/mamnas1/00000000.lock</METASAN_CHECK_FILE>',
		'<METASAN_CHECK_FILE Desc="檢測檔位置">/tcbuffer/18TB/00000000.lock</METASAN_CHECK_FILE>'
        )

        select * from TBSYSTEM_CONFIG

		select * from TBTRACKINGLOG where FDTIME > '2017-03-07 20:15' order by FDTIME

end

if @case = 201709251141 begin
	exec SP_Q_INFO_BY_VIDEOID '00003KNL' --O, R 羅曼波蘭斯基:戲如人生(60分鐘版)	2 2017-09-27 ch14, G201785100020001, 165068, donw
	select * from TBTRANSCODE where FNTRANSCODE_ID = 165068 --4, ANSID = 115166 2017-06-23 17:49, too old, cannot reschedule
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201785100020001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201785100020001'

	exec SP_Q_INFO_BY_VIDEOID '00003XVR' --2017 09/27 pts1 menu 0600 標記上傳後仍在等待上傳
end

if @case = 201709251335 begin
	select * from TBTSM_JOB where FNTSMJOB_ID = 259199 --FNSTATUS = 5 tapeonly /ptstv/HVideo/2017/2017530/G201753000070002.mxf
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 259199 --5 -> 0 -> 2 -> 0 ?  because E003001L4
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201753000070002' --00003QOW
	exec SP_Q_INFO_BY_VIDEOID '00003QOW' --2016世界極限運動大賽 7
end

if @case = 201709251622 begin --播出會卡住
	exec SP_Q_INFO_BY_VIDEOID '000033BB' --爸媽囧很大 #613 2017-10-03 ch13 \\mamstream\MAMDFS\ptstv\HVideo\2009\2009285\G200928506130003.mxf,2009285
	select * from TBLOG_VIDEO where FSID = '2009285' and FNEPISODE = 613 --G200928506130004, 00003Y7L
	exec SP_Q_INFO_BY_VIDEOID '00003Y7L'
end

if @case = 201709251649 begin
	exec SP_Q_INFO_BY_VIDEOID '00003Y4A'
	exec SP_Q_INFO_BY_VIDEOID '00003Y4C'
	exec SP_Q_INFO_BY_VIDEOID '00003Y4D'
end

if @case = 201709252039 begin
	exec SP_Q_INFO_BY_VIDEOID '00003Y8U'
	exec SP_Q_INFO_BY_VIDEOID '00003Y8T'  --標記上傳未更新
end


if @case = 201709261101 begin  --調用失敗
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201709190078' --F, 熊星人和地球人 \\mamstream\MAMDFS\ptstv\HVideo\2017\2017375\G201737500040001.mxf, king8506
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201709220064' --wanheda
	--\\mamstream\MAMDFS\ptstv\HVideo\2008\2008245\G200824500010002.mxf  --tape nly
	--\\mamstream\MAMDFS\ptstv\HVideo\2008\2008245\G200824500020002.mxf  --tape only
	--\\mamstream\MAMDFS\ptstv\HVideo\2015\2015419\G201541900010012.mxf  --tapeoffline  E02643L4
	--\\mamstream\MAMDFS\ptstv\HVideo\2015\2015420\G201542000010004.mxf  --tapeoffline  E02643L4
	--\\mamstream\MAMDFS\ptstv\HVideo\2016\2016442\G201644200200007.mxf  --nealline
	--\\mamstream\MAMDFS\ptstv\HVideo\2016\2016442\G201644200200007.mxf  
end

if @case = 201709261120 begin
	exec SP_Q_INFO_BY_VIDEOID '00003OUA' --R, 美味3分鐘, 美味3分鐘 ch08, 171622 , G201796900010001  --> T donw
	select * from TBTRANSCODE where FNTRANSCODE_ID = 171622 --8 ANSID = -1 2017-09-18 10:06  ANS 處理錯誤 重起會卡在 TSM =-1
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201796900010001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201796900010001'
end

if @case = 201709261528 begin
	select @@servername
end

if @case = 201709261528 begin
	exec SP_Q_INFO_BY_VIDEOID '00003X2H' --R, O 藝術很有事（106年內容產製與運用計畫）2017-09-29 ch13 171255
	select * from TBTRANSCODE where FNTRANSCODE_ID = 171255 --4 ANSID = 120534, 2017-09-13 10:46:22.683
end

if @case = 201709261652 begin
	exec SP_Q_INFO_BY_VIDEOID '00003ORU'
end

if @case = 201709262052 begin
	exec SP_Q_INFO_BY_VIDEOID '00003Y8E'
	exec SP_Q_INFO_BY_VIDEOID '00003Y8A'
end

if @case = 201709261836 begin --cannot recall
	exec SP_Q_INFO_BY_VIDEOID '00002SH1' --\\mamstream\MAMDFS\hakkatv\HVideo\2007\2007853\G200785301630002.mxf --E01975L4 ㄤ牯ㄤ牯咕咕咕(大埔腔) 2017-09-27 11:15:07:02
end

if @case = 201709262058 begin --南水北調 MarkUpload 檔案無法播放
	exec SP_Q_INFO_BY_VIDEOID '00003Y7I' --E, 201709250167
end

if @case = 201709271055 begin
	select * from TBUSERS where FSUSER_ChtName = '林克隆' --prg50076 07219
	select * from TBBOOKING_MASTER where FSUSER_ID = 'prg50076'
end

if @case = 201709271125 begin
	exec SP_Q_INFO_BY_VIDEOID '00003VIF' --我的這一班 475
end

if @case = 201709271129 begin --明天一起去樂園 20 低解無法播放
	exec SP_Q_INFO_BY_VIDEOID '00003A12'  --G201629000200001
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201629000200001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201629000200001'
	select * from TBTRANSCODE where FNTRANSCODE_ID = 172597 --ANSID = 121570
end

if @case = 2017090271137 begin
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201709220065' --a29082241
/*
\\mamstream\MAMDFS\ptstv\HVideo\2016\2016227\G201622700010001.mxf taponly
\\mamstream\MAMDFS\ptstv\HVideo\2016\2016227\G201622700040001.mxf taponly
\\mamstream\MAMDFS\ptstv\HVideo\2016\2016227\G201622700060001.mxf taponly
\\mamstream\MAMDFS\ptstv\HVideo\2016\2016227\G201622700080001.mxf taponly
*/
end

if @case = 201709271811 begin
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201805400010001'
end

if @case = 201709280917 begin
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201709260112' -- 文學Face & Book 20 \\mamstream\MAMDFS\ptstv\HVideo\2014\2014738\G201473800200001.mxf, news9456
end


if @case = 201709281034 begin --檢索資訊錯亂
	select * from TBLOG_VIDEO where FSID = '0000198' and FNEPISODE = 9 --G000019800090001
	select * from TBPROG_M where FSPROG_ID = '0000198'
	select * from TBPROG_D where FSPROG_ID = '0000198' and FNEPISODE = 9
end

if @case = 201709281104 begin
	exec SP_Q_INFO_BY_VIDEOID '00003XWN' -- 標記上傳問題
	exec SP_Q_INFO_BY_VIDEOID '00003Y6X' -- QC 未審核 要趕入庫
end

if @case = 201709281126 begin
	exec SP_Q_INFO_BY_VIDEOID '00003YHM' --P201709007770001, 20170900777 檔案未排播所以未到Review
	exec SP_Q_INFO_BY_VIDEOID '00003YHO' --2017RM-幕後有藝思 2017-10-11 ch07

end

if @case = 201709281130 begin
	select * from TBTSM_JOB where FNTSMJOB_ID = 259998 --3 FCSTATUS = ''
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N'
end

if @case = 201709281158 begin --爸媽囧很大 613 片庫重轉
	exec SP_Q_INFO_BY_VIDEOID '00003Y7L' --爸媽囧很大 613 2017-10-03 ch13 G200928506130004
	/*
	G20170928.log:2786:2017/9/28 上午 11:56:51 : 檔案:\\mamstream\MAMDFS\ptstv\HVideo\2009\2009285\G20092
叫TSM-ReCall,JOBID=260007
*/

	select * from TBTRACKINGLOG where FDTIME > '2017-09-28 10:00' order by FDTIME
end

if @case = 201709281222 begin
	exec SP_Q_INFO_BY_VIDEOID '00003XUR' --PTS3就是愛運動綜合版Promo
end

if @case = 201709281338 begin --003XUM.mxf.MXF 卡住
	exec SP_Q_INFO_BY_VIDEOID '00003XUM' --51322
	select * from TBUSERS where FSUSER_ID = '51322' --林秭瑜
	exec SP_Q_INFO_BY_VIDEOID '00003YLW' --60042
	select * from TBUSERS where FSUSER_ID = '60042' --程瓊瑤
end

if @case = 201709281403 begin
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201709210010' --bobohung	洪潤德
	--\\mamstream\MAMDFS\ptstv\HVideo\2018\2018051\G201805100010001.mxf tapeonly
	--\\mamstream\MAMDFS\ptstv\HVideo\2018\2018053\G201805300010003.mxf nearline
	--\\mamstream\MAMDFS\ptstv\HVideo\2018\2018050\G201805000010001.mxf tapeonly
end

if @case = 201709281519 begin --FCSTATUS = '', C, E, N, O, S, X
	--列出要保存的video id
	select * from TBLOG_VIDEO_HD_MC where FCSTATUS not in ('C', 'N', 'O', 'X') and FSFILE_NO not like 'D%' order by FDCREATED_DATE desc
	select FCSTATUS, count(FSFILE_NO) from TBLOG_VIDEO_HD_MC group by FCSTATUS
	select* from TBLOG_VIDEO_HD_MC where FCSTATUS = 'B'

	select FSVIDEO_ID from TBLOG_VIDEO_HD_MC where FCSTATUS not in ('C', 'N', 'O', 'X') and FSFILE_NO not like 'D%' and FSVIDEO_ID <> '' order by FDCREATED_DATE
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '' and FSFILE_NO not like 'D%'

	exec SP_Q_INFO_BY_VIDEOID '00002WYF'
end

if @case = 201709290949 begin --調用失敗
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201381100720001' --\\mamstream\MAMDFS\ptstv\HVideo\2013\2013811\G201381100720001.mxf
	select * from TBUSERS where FSUSER_ChtName = '湯又新' --prg1181, 07183
	select * from TBBOOKING_MASTER where FSUSER_ID = '07183' and FDBOOKING_DATE > '2017/09/20' order by FDBOOKING_DATE
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201709280119' --我在台灣 你好嗎, F
end

if @case = 201709291045 begin --00003VIG 乒乓-公視新創電影 user 查不到 videoid
	select * from TBLOG_VIDEO where FSID = '2017740' and FNEPISODE = 1 --G201774000010083
	exec SP_Q_INFO_BY_VIDEOID '00003VIG'
end

if @case = 201709291056 begin  --調用失敗
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201709260035' --adm3036
	select A.FCFILE_TRANSCODE_STATUS, B.FSFILE_PATH_H from TBBOOKING_DETAIL A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FSBOOKING_NO = '201709260035' and A.FCFILE_TRANSCODE_STATUS = 'F'

	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201709250176' --adm3036
	select A.FCFILE_TRANSCODE_STATUS, B.FSFILE_PATH_H from TBBOOKING_DETAIL A left join TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where FSBOOKING_NO = '201709250176' and A.FCFILE_TRANSCODE_STATUS = 'F'
end

if @case = 201709291609 begin -- louth 要搬檔時檔案不見
	exec SP_Q_INFO_BY_VIDEOID '00003QP2'  
	exec SP_Q_INFO_BY_VIDEOID '00002SX9'
	exec SP_Q_INFO_BY_VIDEOID '00000CB2' --2016-09-29 19:29 轉檔
	exec SP_Q_INFO_BY_VIDEOID '00002WRE' --2017-10-04
end

if @case = 201709291632 begin
	select * from  TBTSM_JOB where FNTSMJOB_ID = 260241 --5 執行plink得到錯誤的回傳內容：指令 = dsmrecall /ptstv/HVideo/2016/20160700748/P201607007480001.mxf, 參數 = 10.13.210.15 -l root -pw TSMP@ssw0rdTSM ". /root/.bash_profile";"dsmrecall /ptstv/HVideo/2016/20160700748/P201607007480001.mxf", 錯誤訊息 = FATAL ERROR: Server unexpectedly closed network connection
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 260241
end

if @case = 201709291745 begin -- 在 approved 有出現，但是又被移除
	exec SP_Q_INFO_BY_VIDEOID '00002W15'
end

if @case = 201709300932 begin
	exec SP_Q_INFO_BY_VIDEOID '00003WHF' --鬧熱打擂台(120分鐘版)
end

if @case = 201709301040 begin
	exec SP_Q_INFO_BY_VIDEOID '00003XXV' --PTS3 RM【中華台北VS韓國】2017亞洲棒球錦標賽
end

if @case = 201709301054 begin
	exec SP_Q_INFO_BY_VIDEOID '00003W0V'
end

if @case = 201709301805 begin
	exec SP_Q_INFO_BY_VIDEOID '00003V1E' --第39屆威廉瓊斯盃國際籃球邀請賽 2017-10-09 ch62
end

if @case = 201709301843 begin
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201709260045' --prg60167 \\mamstream\MAMDFS\ptstv\HVideo\2015\2015404\G201540400990001.mxf
end

if @case = 201710020903 begin --檔案有問題
	exec SP_Q_INFO_BY_VIDEOID '000021B1' --百變小露露 #46 G201549600460002 \\mamstream\MAMDFS\ptstv\HVideo\2015\2015496\G201549600460002.mxf, 2017-10-02 17:00:40:00 ch02
	exec SP_Q_INFO_BY_VIDEOID '00002P8Z' --我在墾丁*天氣晴 3 2017-10-02, 14:00:41:23 ch13
end

if @case = 201710021007 begin
	exec SP_Q_INFO_BY_VIDEOID '00003YXC' --S 林羽葶-2017金鐘獎 P201709009360001
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'C' where FSFILE_NO = 'P201709009360001'
end

if @case = 201710021222 begin --段落不見？
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201510700010008' --麻醉風暴第1集 字幕播出帶
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G201510700010008'
end

if @case = 201710021553 begin --permission problem
	exec SP_Q_INFO_BY_VIDEOID '00003Z07' --10/2 transcode P201710000080001
	exec SP_Q_INFO_BY_VIDEOID '00003Z3H' --10/2 transcode P201710000440001
	exec SP_Q_INFO_BY_VIDEOID '00003YSB' --10/2 transcode P201709009070001
	exec SP_Q_INFO_BY_VIDEOID '00003Z4M' --O, T 10/2 transcode P201710000690001
end

if @case = 2017100212557 begin --標記上傳問題
	exec SP_Q_INFO_BY_VIDEOID '00003WDW' --FCFILESTATUS = S 麻醉風暴2 8, file in HDUpload is fine
	exec SP_Q_INFO_BY_VIDEOID '00003WEB'
	exec SP_Q_INFO_BY_VIDEOID '00003WED' --2017182, 11, FCSTATUS = ''
	select * from TBPROG_M where FSPROG_ID = '2017182' --麻醉風暴2
end

if @case = 201710021731 begin
	exec SP_Q_INFO_BY_VIDEOID '00003Z11'
end

if @case = 201710021737 begin
	exec SP_Q_INFO_BY_VIDEOID '00003V1E' --S
	exec SP_Q_INFO_BY_VIDEOID '00003V2R'
end

if @case = 201710020915 begin
	select * from TBTSM_JOB where FNTSMJOB_ID = 260586 --執行plink得到錯誤的回傳內容：指令 = dsmrecall /hakkatv/HVideo/2016/20160601366/P201606013660001.mxf, 參數 = 10.13.210.15 -l root -pw TSMP@ssw0rdTSM ". /root/.bash_profile";"dsmrecall /hakkatv/HVideo/2016/20160601366/P201606013660001.mxf", 錯誤訊息 = FATAL ERROR: Server unexpectedly closed network connection
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 260586
	select * from TBTSM_JOB where FNTSMJOB_ID = 260924
end

if @case = 201710031158 begin --QC 過在 Approved
	exec SP_Q_INFO_BY_VIDEOID '00003XYY' --2017-10-03 11:48, \\mamstream\MAMDFS\ptstv\HVideo\2017\20170900598\P201709005980001.mxf, 2017-10-05 ch12
	--P201709005980001
end

if @case = 201710031400 begin --調用失敗 a29082241 PARTIAL 處理錯誤
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201709280037'
/*
\\mamstream\MAMDFS\ptstv\HVideo\2017\2017182\G201718200050014.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2017\2017193\G201719300010001.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2017\2017192\G201719200010001.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2017\2017190\G201719000010001.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2017\2017191\G201719100010013.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2017\2017739\G201773900010015.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2017\2017741\G201774100010028.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2017\2017742\G201774200010018.MXF
*/
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201710020217'
end

if @case = 201710031442 begin
	exec SP_Q_INFO_BY_VIDEOID '00003068' --G201232000040002, 南風。六堆(原名:六堆系列) 2017-10-05
end

if @case = 201710031608 begin --查詢播出 ID
	select FSPROG_ID ID, FSPROG_NAME NAME, FDDATE, FSCHANNEL_ID, FSVIDEO_ID, FSPLAY_TIME from TBPGM_COMBINE_QUEUE where FDDATE = '2017-10-05' and FSCHANNEL_ID = '12' order by FSPLAY_TIME

	select A.FSPROMO_ID, B.FSPROMO_NAME, A.FSVIDEO_ID, A.FDDATE, A.FSPLAYTIME from TBPGM_ARR_PROMO A left join TBPGM_PROMO B 
	on A.FSPROMO_ID = B.FSPROMO_ID where FDDATE = '2017-10-05' and FSCHANNEL_ID = '12' order by FSPLAYTIME

	select FSPROG_ID ID, FSPROG_NAME NAME, FDDATE, FSCHANNEL_ID, FSVIDEO_ID, FSPLAY_TIME from TBPGM_COMBINE_QUEUE
	union
	select A.FSPROMO_ID, B.FSPROMO_NAME, A.FSVIDEO_ID, A.FDDATE, A.FSPLAYTIME from TBPGM_ARR_PROMO A left join TBPGM_PROMO B 
	on A.FSPROMO_ID = B.FSPROMO_ID
end

if @case = 201710031649 begin
	exec SP_Q_INFO_BY_VIDEOID '00003Z7U'  ---> X
end

if @case = 201710051206 begin --音樂萬萬歲3 #8 長度不對 G201558600080002
	exec SP_Q_INFO_BY_VIDEOID '00001TNQ' --N, T. 音樂萬萬歲3, 2017-10-15, 01:54:15;00
end

if @case = 201710051414 begin --\\mamstream\MAMDFS\ptstv\HVideo\2017\2017062\G201706200010002.mxf
	exec SP_Q_INFO_BY_VIDEOID '00003KOL' --2017-04-27 15:01:45.030
end

if @case = 201710051436 begin  -- GPFS -> Approved 一直拷貝失敗
	exec SP_Q_INFO_BY_VIDEOID '00003QIB'
end

if @case = 201710051554 begin
	exec SP_Q_INFO_BY_VIDEOID '00003Z75'  --行走TIT, G201484100680002
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'C' where FSFILE_NO = 'G201484100680002'
end

if @case = 201710051941 begin --OTT 檔案調用
	select * from TBPROG_M where FSPGMNAME = '通靈少女' --2017724(6)
	select * from TBLOG_VIDEO where FSID = '2017724' and FSARC_TYPE = '002' and FCFILE_STATUS = 'T'  order by FNEPISODE

	select * from TBPROG_M where FSPGMNAME like '%騙過你的病-安慰劑效應%' --2018094(1)
	select * from TBLOG_VIDEO where FSID = '2018094' and FSARC_TYPE = '002' and FCFILE_STATUS = 'T'  order by FNEPISODE --NA

	select * from TBPROG_M where FSPGMNAME like '%小獅救援行動%' --2018060(1)
	select * from TBLOG_VIDEO where FSID = '2018060' and FSARC_TYPE = '002' and FCFILE_STATUS = 'T'  order by FNEPISODE --NA

	select * from TBPROG_M where FSPGMNAME like '%大地黑龍江%' --2018061(3)
	select * from TBLOG_VIDEO where FSID = '2018061' and FSARC_TYPE = '002' and FCFILE_STATUS = 'T'  order by FNEPISODE --N/A

	select * from TBPROG_M where FSPGMNAME like '%小魔女的華麗冒險%' --小魔女的華麗冒險2 2018098(26)
	select * from TBLOG_VIDEO where FSID = '2018098' and FSARC_TYPE = '002' and FCFILE_STATUS = 'T'  order by FNEPISODE --1 #2

	select * from TBPROG_M where FSPGMNAME like '%廢氣賣大錢%' --2018124(1)
	select * from TBLOG_VIDEO where FSID = '2018124' and FSARC_TYPE = '002' and FCFILE_STATUS = 'T'  order by FNEPISODE --N/A

	select * from TBPROG_M where FSPGMNAME like '%神奇的猴子家族%' --2017622(3)
	select * from TBLOG_VIDEO where FSID = '2017622' and FSARC_TYPE = '002' and FCFILE_STATUS = 'T'  order by FNEPISODE
end

if @case = 201710060932 begin --匯出段落資訊
	select * from TBLOG_VIDEO where FSID = '2017194' and FSARC_TYPE = '002' and FCFILE_STATUS = 'T' order by FNEPISODE --26

	select A.FSFILE_NO, B.FNSEG_ID ,B.FSBEG_TIMECODE, B.FSEND_TIMECODE from TBLOG_VIDEO A left join TBLOG_VIDEO_SEG B on A.FSFILE_NO = B.FSFILE_NO  
	where A.FSID = '2017194' and A.FSARC_TYPE = '002' and A.FCFILE_STATUS = 'T' order by A.FNEPISODE, B.FNSEG_ID
end

if @case = 20171061051 begin --Review -> Approved 搬檔問題
    exec SP_Q_INFO_BY_VIDEOID '00003XWN' --2017-09-28 22:10:15.507
	exec SP_Q_INFO_BY_VIDEOID '00003XWL' --2017-09-26 10:25:24.147
end

if @case = 201710061404 begin --查詢已標記上傳完成檔案, E 上傳失敗, X 置換刪除
	select * from TBLOG_VIDEO_HD_MC where FCSTATUS in ('C', 'N', 'O') order by FDUPDATED_DATE desc
	select * from TBLOG_VIDEO_HD_MC where FCSTATUS not in ('C', 'N', 'O', 'X') and FSVIDEO_ID <> '' order by FDUPDATED_DATE desc
	select * from TBLOG_VIDEO_HD_MC where FCSTATUS = 'X' and FSVIDEO_ID <> '' order by FDUPDATED_DATE desc
end

if @case = 201710061600 begin
	exec SP_Q_INFO_BY_VIDEOID '00003CXV' --艷后和她的小丑們(2-1) 2017-10-16 ch14
end

if @case = 201710061615 begin
	exec SP_Q_INFO_BY_VIDEOID '00003ZE5' --搬戲人生new 2017-10-10
	exec SP_Q_INFO_BY_VIDEOID '00003YAW' --O, R 猜猜我有多愛你(大埔腔) 41 仍在 Review 2017-10-11
	exec SP_Q_INFO_BY_VIDEOID '00003ZD0' --O, S, 2017福氣來了 193, 2017-10-11 ch07 still in Review
end

if @case = 201710061719 begin --調用失敗
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201710060010' --adm3036
/*
\\mamstream\MAMDFS\ptstv\HVideo\2017\2017868\G201786800030002.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2017\2017088\G201708800010003.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2011\2011421\G201142100010029.mxf
*/
end

if @case = 201710061741 begin  --查詢已標記上傳可以刪除的檔案
	exec SP_Q_INFO_BY_VIDEOID '00003WKF'
	exec SP_Q_INFO_BY_VIDEOID '00002H0B' --O

	if OBJECT_ID(N'tempdb.dbo.#tempImport', N'U') IS NOT NULL
        DROP TABLE #tempImport

	 CREATE TABLE #tempImport (
		FSVIDEO_ID varchar(10)
     )

    BULK insert #tempImport from 'C:\temp\DELETE.log' WITH
        (
               ROWTERMINATOR = '\n'
        );

	select * from #tempImport

	select B.* from #tempImport A left join TBLOG_VIDEO_HD_MC B on A.FSVIDEO_ID = B.FSVIDEO_ID
end

if @case = 201710061816 begin
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201492800010014' --\\mamstream\MAMDFS\ptstv\HVideo\2014\2014928\G201492800010014.mxf
end

if @case = 201710091533 begin --reschedule still fail 
	exec SP_Q_INFO_BY_VIDEOID '00003Z83' --2017-18 歐洲冠軍足球聯賽 2 2017-10-10 ch14, O, R, G201810400020002
	select * from TBTRANSCODE where FNTRANSCODE_ID = 173478 --ANSID = 122305 TRANSCODEID = 173478 TSMJOBID = 261860
	exec SP_Q_TBTRACKINGLOG_BY_DATE 173478, 122305, 261860, 'G201810400020002', '003Z83',  '2017-10-09'
	--plink高解搬檔失敗：JobID 173478/10.13.210.16 -l root -pw TSMP@ssw0rdTSM "cp -r /mnt/HDUpload/003Z83.mxf  /ptstv/HVideo/2018/2018104/G201810400020002.mxf"/cp: cannot stat `/mnt/HDUpload/003Z83.mxf': No such file or directory
	--org file *.MXF
end

if @case = 201710101126 begin
	exec SP_Q_INFO_BY_VIDEOID '0000338K' --100個種子的秘密 3 2017-10-11, \\mamstream\MAMDFS\ptstv\HVideo\2016\2016810\G201681000030003.mxf
	select * from TBTSM_JOB where FNTSMJOB_ID = 261751 --5 執行plink得到錯誤的回傳內容：指令 = dsmrecall /ptstv/HVideo/2016/2016810/G201681000030003.mxf, 參數 = 10.13.210.15 -l root -pw TSMP@ssw0rdTSM ". /root/.bash_profile";"dsmrecall /ptstv/HVideo/2016/2016810/G201681000030003.mxf", 錯誤訊息 = FATAL ERROR: Server unexpectedly closed network connection
	select * from TBTSM_JOB where FNTSMJOB_ID = 261685 --5
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 261751
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 261685
end

if @case = 201710101134 begin
	select * from TBPROG_M where FSPGMNAME like '%車正在追%' --車正在追-0000190 (5)
	select * from TBPROG_M where FSPGMNAME like '%風中緋櫻%' --風中緋櫻-霧社事件-0002079 (20)
	select * from TBPROG_M where FSPGMNAME like '%天平上的馬爾濟斯%' --2007987 (20)
	select * from TBPROG_M where FSPGMNAME like '%鹽田兒女%' -- 鹽田兒女-0000195(20)

	select * from TBLOG_VIDEO where FSID in ('0000190', '0002079', '2007987', '0000195') and FSARC_TYPE in ('002', '001') and FCFILE_STATUS in ('T', 'B') order by FSID, FNEPISODE

	select * from TBLOG_VIDEO where FSID = '0000195' and FCFILE_STATUS = 'T'
	select * from TBLOG_VIDEO where FSID = '0002079' and FCFILE_STATUS = 'T'
	select * from TBLOG_VIDEO where FSID = '2007987' and FCFILE_STATUS = 'T'
	select * from TBLOG_VIDEO where FSID = '0000195' and FCFILE_STATUS = 'T'
end


if @case = 201710110950 begin
	exec SP_Q_INFO_BY_VIDEOID '00003Z86' --R, 2017-18 歐洲冠軍足球聯賽 4, 2017-10-12
	exec SP_Q_INFO_BY_VIDEOID '00003Z87' --T, 2017-18 歐洲冠軍足球聯賽 5, 2017-10-13
end

if @case = 201710111108 begin --調用 Partial 處理錯誤
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201710020201' --claire0123
/*
\\mamstream\MAMDFS\ptstv\HVideo\0000\0000060\G000006020110001.mxf
\\mamstream\MAMDFS\ptstv\HVideo\0000\0000060\G000006020170001.mxf
\\mamstream\MAMDFS\ptstv\HVideo\0000\0000060\G000006020190001.mxf
\\mamstream\MAMDFS\ptstv\HVideo\0000\0000060\G000006020220001.mxf
\\mamstream\MAMDFS\ptstv\HVideo\0000\0000060\G000006020240001.mxf
*/
end


if @case = 201710111201 begin --003Q3N, 003Q3O, 003Q3P, QC 錯誤，需複製原始檔到 MarkUpload
	exec SP_Q_INFO_BY_VIDEOID '00003Q3N' --TBLOG_VIDEO_HD_MC = X, TBLOG_VIDEO.FCFILE_STATUS = D, G201798100010001
	select * from TBLOG_VIDEO where FSID = '2017981' and FNEPISODE = 1
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G201798100010001'
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201798100010001' --no record
end

if @case = 201710111206 begin --跟著台灣一起說 4 ~ 32 段落填錯，要如何整批置換
	select * from TBPROG_M where FSPGMNAME like '%跟著台灣%' --2017981
	select * from TBLOG_VIDEO where FSID = '2017981' and FNEPISODE between 4 and 32 order by FNEPISODE
	select * from TBLOG_VIDEO_HD_MC A left join TBLOG_VIDEO B  on A.FSFILE_NO = B.FSFILE_NO where B.FSID = '2017981'  and FNEPISODE between 4 and 32 order by FNEPISODE
	select * from TBLOG_VIDEO_HD_MC A left join TBLOG_VIDEO B  on A.FSFILE_NO = B.FSFILE_NO where B.FSID = '2013232' and A.FCSTATUS = 'C' and B.FCFILE_STATUS = 'B'

	exec SP_Q_INFO_BY_VIDEOID '00002JDT' --C, B
	select FCSTATUS from TBLOG_VIDEO_HD_MC group by FCSTATUS
	select * from TBLOG_VIDEO_HD_MC where FCSTATUS = 'E' order by FDUPDATED_DATE
	select * from TBLOG_VIDEO_HD_MC where FCSTATUS = 'X' order by FDUPDATED_DATE

	select A.FCSTATUS from TBLOG_VIDEO_HD_MC A left join TBLOG_VIDEO B  on A.FSFILE_NO = B.FSFILE_NO where B.FSID = '2013232' group by A.FCSTATUS
	select * from TBLOG_VIDEO where FSID = '2013232' and FCFILE_STATUS not in ('B', 'T') order by FNEPISODE

	exec SP_Q_INFO_BY_VIDEOID '00003ZTO' 
	--G201323200950001 95 FCSTATUS = '' 退單後 --> TBLOG_VIDEO.FCFILE_STATUS = 'X' --> 修段落後 FCFILE_STATUS = 'B'
	--> 重新標記上傳 FCSTATUS = '' FSUPDATED_BY, FDUPDATED_DATE has value
	update TBLOG_VIDEO set FCFILE_STATUS = 'X' where FSFILE_NO = 'G201323200950001'
	select * from  TBPTSAP_STATUS_LOG where FSVIDEOID = '00003ZTO'
	update TBLOG_VIDEO_HD_MC set FSUPDATED_BY = NULL, FDUPDATED_DATE = NULL where FSFILE_NO = 'G201323200950001'
	select * from TBLOG_VIDEOID_MAP where FSVIDEO_ID_PROG = '00003ZTO'
	delete TBLOG_VIDEOID_MAP where FSVIDEO_ID_PROG = '00003ZTO'  --刪除也無效

	select * from TBLOG_VIDEOID_MAP where FSVIDEO_ID_PROG = '00003ZU6'  --未標記得不會有資料
end

if @case = 201710111405 begin --大量調用，磁帶已下架
	select * from TBUSERS where FSUSER = 'a29082241' --黃琬婷
	select * from TBTSM_JOB where FNTSMJOB_ID = 262594
	select * from TBTSM_JOB where FNTSMJOB_ID = 262578
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201710110089' --13 D00318L4, E01399L4
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201710110136' --24 A00006L4
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201710110128' --13 A00006L4, D00318L4, E01601L4
end

if @case = 201710111531 begin --9/27 QC 轉檔失敗 10/11 播出，但是檔案仍在 Review 
	exec SP_Q_INFO_BY_VIDEOID '00003YAW' --R, 00003YAW 172525 reschedule
	select * from TBTRANSCODE where FNTRANSCODE_ID = 172525 --ANSID = 121559, 4

end

if @case = 201710111545 begin
	select * from TBPTSAP_STATUS_LOG where CAST(FDUPDATE_DATE as DATE) = '2017-10-11' and FCSTATUS = '80' order by FDUPDATE_DATE
	select * from TBLOG_VIDEO_HD_MC where CAST(FDUPDATED_DATE as DATE) = '2017-10-11' order by FDUPDATED_DATE

	select * from TBPTSAP_STATUS_LOG A left join TBLOG_VIDEO_HD_MC B on A.FSVIDEOID = B.FSVIDEO_ID 
	where CAST(A.FDUPDATE_DATE as DATE) = '2017-10-11' and A.FCSTATUS = '80' and B.FCSTATUS not in ('E', 'X')
end

if @case = 201710111748 begin
	exec SP_Q_INFO_BY_VIDEOID '00003ZUW' --PTS3主題之夜PROMO瑪莉亞的控訴
	select * from TBPTSAP_STATUS_LOG where CAST(FDUPDATE_DATE as DATE) = '2017-10-11' order by FDUPDATE_DATE
end

if @case = 201710121123 begin
    select * from TBPROG_M where FSPROG_ID = '2012429' --權力過程
	select * from TBLOG_VIDEO where FSID = '2012429' --\\mamstream\MAMDFS\ptstv\HVideo\2012\2012429\G201242900010012.mxf
end

if @case = 201710121332 begin --其他影片置換問題
	select * from TBPROG_M where FSPGMNAME like '%城市情歌%' --2017441 城市情歌, 2017441 城市情歌(60分鐘版)
	select * from TBLOG_VIDEO where FSID = '2017441' and FNEPISODE = 14
	select * from TBUSERS where FSUSER_ID = '51370' --陳昱蓉
	select * from TBLOG_VIDEO where FSID = '2017441' and FSARC_TYPE = '026'
    select * from TBLOG_VIDEO where FSBRO_ID = '201710110278' --有2筆, 城市情歌(11)HD分軌帶.mxf, 城市情歌(11)HD完成帶.mxf
	select * from TBLOG_VIDEO where FSARC_TYPE = '007'
	select * from TBLOG_VIDEO where FSID = '2017441' and FSARC_TYPE = '002' order by FNEPISODE 
end

if @case = 201710121450 begin --reschedule
	exec SP_Q_INFO_BY_VIDEOID '00003XO9' --【中華台北VS韓國】2017亞洲棒球錦標賽_2016PTS2 RM R, 171822
	select * from TBTRANSCODE where FNTRANSCODE_ID = 171822 --120991
end

if @case = 201710121808 begin --藝術很有事（106年內容產製與運用計畫）10 HDUpload 檔案無法播 MarkUpload 有問題
	exec SP_Q_INFO_BY_VIDEOID '00003ZYX'
end

if @case = 201710121846 begin --藝術很有事（106年內容產製與運用計畫） 9 轉檔成功搬檔失敗無法置換 done
	exec SP_Q_INFO_BY_VIDEOID '00003ZAE' --173323
	select * from TBTRANSCODE where FNTRANSCODE_ID = 173323 --ANSID = 122217
end

if @case = 201710121904 begin
	exec SP_Q_INFO_BY_VIDEOID '00003WOH' --家家有好食 #57
	exec SP_Q_INFO_BY_VIDEOID '00003VC4'
end

if @case = 201710130940 begin --調用檔案已過期刪除 prg1131
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201710030133'
	select * from TBBOOKING_DETAIL where FSBOOKING_NO = '201710030133'
/*
\\mamstream\MAMDFS\ptstv\HVideo\2013\20130300015\D201303000150001.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700019\D201307000190001.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700042\D201307000420001.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700043\D201307000430001.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700046\D201307000460001.mxf
*/
end

if @case = 201710131010-201710111206 begin --跟著台灣一起說 4 ~ 32 段落填錯，要如何整批置換
	select * from TBPROG_M where FSPGMNAME like '%跟著台灣%' --2017981
	select * from TBLOG_VIDEO where FSID = '2017981' and FNEPISODE between 4 and 32 order by FNEPISODE
	select * from TBLOG_VIDEO_HD_MC A left join TBLOG_VIDEO B  on A.FSFILE_NO = B.FSFILE_NO where B.FSID = '2017981'  and FNEPISODE between 4 and 32 order by FNEPISODE
	select * from TBLOG_VIDEO_HD_MC A left join TBLOG_VIDEO B  on A.FSFILE_NO = B.FSFILE_NO where B.FSID = '2013232' and A.FCSTATUS = 'C' and B.FCFILE_STATUS = 'B'

	exec SP_Q_INFO_BY_VIDEOID '00002JDT' --C, B
	select FCSTATUS from TBLOG_VIDEO_HD_MC group by FCSTATUS
	select * from TBLOG_VIDEO_HD_MC where FCSTATUS = 'E' order by FDUPDATED_DATE
	select * from TBLOG_VIDEO_HD_MC where FCSTATUS = 'X' order by FDUPDATED_DATE

	select A.FCSTATUS from TBLOG_VIDEO_HD_MC A left join TBLOG_VIDEO B  on A.FSFILE_NO = B.FSFILE_NO where B.FSID = '2013232' group by A.FCSTATUS
	select * from TBLOG_VIDEO where FSID = '2013232' and FCFILE_STATUS not in ('B', 'T') order by FNEPISODE

	exec SP_Q_INFO_BY_VIDEOID '00003ZTO' 
	--G201323200950001 95 FCSTATUS = '' 退單後 --> TBLOG_VIDEO.FCFILE_STATUS = 'X' --> 修段落後 FCFILE_STATUS = 'B'
	--> 重新標記上傳 FCSTATUS = '' FSUPDATED_BY, FDUPDATED_DATE has value
	update TBLOG_VIDEO set FCFILE_STATUS = 'X' where FSFILE_NO = 'G201323200950001'
	select * from  TBPTSAP_STATUS_LOG where FSVIDEOID = '00003ZTO'
	update TBLOG_VIDEO_HD_MC set FSUPDATED_BY = NULL, FDUPDATED_DATE = NULL where FSFILE_NO = 'G201323200950001'
	select * from TBLOG_VIDEOID_MAP where FSVIDEO_ID_PROG = '00003ZTO'
	delete TBLOG_VIDEOID_MAP where FSVIDEO_ID_PROG = '00003ZTO'  --刪除也無效

	select * from TBLOG_VIDEOID_MAP where FSVIDEO_ID_PROG = '00003ZU6'  --未標記的不會有資料


	-----------------------------------------------------------------------------------
	select A.FSFILE_NO, A.FCFILE_STATUS, A.FSVIDEO_PROG, B.* from TBLOG_VIDEO A left join TBLOG_VIDEO_SEG B on A.FSFILE_NO = B.FSFILE_NO where FSID = '2017981' 
	and FNEPISODE between 1 and 32 order by FNEPISODE

	update TBLOG_VIDEO set FCFILE_STATUS = 'X' where FSFILE_NO = 'G201798100040001' --X : 刪除 D: 置換刪除
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201798100040001'

	update TBLOG_VIDEO set FCFILE_STATUS = 'X' where FSFILE_NO = 'G201798100050001'
	exec SP_Q_INFO_BY_VIDEOID '0000402V' --G201798100050002
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'C' where FSFILE_NO = 'G201798100050002'
end

if @case = 201710131431 begin --台影調用磁帶已下架
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201710110335'
/*
\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700062\D201307000620001.mxf offline
\\mamstream\MAMDFS\ptstv\HVideo\2013\20130700059\D201307000590001.mxf offline
*/
end

if @case = 201710131750 begin --未排播所以檔案不會到 Review done
	exec SP_Q_INFO_BY_VIDEOID '0000403H'
end

if @case = 201719161141 --#2017 客家戲曲 已被退單無法置換，應該用修改方式重送
	exec SP_Q_INFO_BY_VIDEOID '00002LC9'
	select * from TBLOG_VIDEO where FSID = '2009299' and FNEPISODE = 675
	select * from TBLOG_VIDEO where FSID = '2009299' and FNEPISODE = 671 --HD 201605300089
	select * from TBPROG_M where FSPROG_ID = '2009299' --客家戲曲
	select * from TBLOG_VIDEO where FSID = '2009299' and FNEPISODE between 671 and 675 and FSARC_TYPE = '002' order by FNEPISODE
end

if @case = 201710161547 begin --將送播單註記刪除，重新填單, done
	select * from TBLOG_VIDEO where FSBRO_ID in ('201710120103', '201710120106', '201710120107') --2017868 G201786800010004, G201786800020003, G201786800040002
	select * from TBPROG_M where FSPROG_ID = '2017868' --下課花路米－同理心大考驗系列及南向世界遺產系列
	update TBLOG_VIDEO set FCFILE_STATUS = 'X' where FSFILE_NO in ('G201786800010004', 'G201786800020003', 'G201786800040002')
end


if @case = 201710161641-201710131010-201710111206 begin --跟著台灣一起說 4 ~ 32 段落填錯，要如何整批置換 done
	select * from TBPROG_M where FSPGMNAME like '%跟著台灣%' --2017981
	select * from TBLOG_VIDEO where FSID = '2017981' and FNEPISODE between 4 and 32 order by FNEPISODE
	select * from TBLOG_VIDEO_HD_MC A left join TBLOG_VIDEO B  on A.FSFILE_NO = B.FSFILE_NO where B.FSID = '2017981'  and FNEPISODE between 4 and 32 order by FNEPISODE
	select * from TBLOG_VIDEO_HD_MC A left join TBLOG_VIDEO B  on A.FSFILE_NO = B.FSFILE_NO where B.FSID = '2013232' and A.FCSTATUS = 'C' and B.FCFILE_STATUS = 'B'

	exec SP_Q_INFO_BY_VIDEOID '00002JDT' --C, B
	select FCSTATUS from TBLOG_VIDEO_HD_MC group by FCSTATUS
	select * from TBLOG_VIDEO_HD_MC where FCSTATUS = 'E' order by FDUPDATED_DATE
	select * from TBLOG_VIDEO_HD_MC where FCSTATUS = 'X' order by FDUPDATED_DATE

	select A.FCSTATUS from TBLOG_VIDEO_HD_MC A left join TBLOG_VIDEO B  on A.FSFILE_NO = B.FSFILE_NO where B.FSID = '2013232' group by A.FCSTATUS
	select * from TBLOG_VIDEO where FSID = '2013232' and FCFILE_STATUS not in ('B', 'T') order by FNEPISODE

	exec SP_Q_INFO_BY_VIDEOID '00003ZTO' 
	--G201323200950001 95 FCSTATUS = '' 退單後 --> TBLOG_VIDEO.FCFILE_STATUS = 'X' --> 修段落後 FCFILE_STATUS = 'B'
	--> 重新標記上傳 FCSTATUS = '' FSUPDATED_BY, FDUPDATED_DATE has value
	update TBLOG_VIDEO set FCFILE_STATUS = 'X' where FSFILE_NO = 'G201323200950001'
	select * from  TBPTSAP_STATUS_LOG where FSVIDEOID = '00003ZTO'
	update TBLOG_VIDEO_HD_MC set FSUPDATED_BY = NULL, FDUPDATED_DATE = NULL where FSFILE_NO = 'G201323200950001'
	select * from TBLOG_VIDEOID_MAP where FSVIDEO_ID_PROG = '00003ZTO'
	delete TBLOG_VIDEOID_MAP where FSVIDEO_ID_PROG = '00003ZTO'  --刪除也無效

	select * from TBLOG_VIDEOID_MAP where FSVIDEO_ID_PROG = '00003ZU6'  --未標記的不會有資料


	-----------------------------------------------------------------------------------
	select A.FSFILE_NO, A.FCFILE_STATUS, A.FSVIDEO_PROG, B.* from TBLOG_VIDEO A left join TBLOG_VIDEO_SEG B on A.FSFILE_NO = B.FSFILE_NO where FSID = '2017981' 
	and FNEPISODE between 1 and 32 order by FNEPISODE

	update TBLOG_VIDEO set FCFILE_STATUS = 'X' where FSFILE_NO = 'G201798100040001' --X : 刪除 D: 置換刪除
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201798100040001'

	update TBLOG_VIDEO set FCFILE_STATUS = 'X' where FSFILE_NO = 'G201798100050001'
	exec SP_Q_INFO_BY_VIDEOID '0000402V' --G201798100050002
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'C' where FSFILE_NO = 'G201798100050002'

	select * from TBLOG_VIDEO where FSID = '2017981' and FNEPISODE between 7 and 32 and FSARC_TYPE = '002' and FCFILE_STATUS = 'X' order by FNEPISODE --26

	--將 7 ~ 32 註記刪除
	--update TBLOG_VIDEO set FCFILE_STATUS = 'X' where FSID = '2017981' and FNEPISODE between 7 and 32 and FSARC_TYPE = '002' and FCFILE_STATUS = 'B'
	select FSBRO_ID, FSFILE_NO, FNEPISODE from TBLOG_VIDEO where FSID = '2017981' and FNEPISODE between 7 and 32 and FSARC_TYPE = '002' and FCFILE_STATUS = 'X' order by FNEPISODE

	select * from TBUSERS where FSUSER_ID = '60026' --邱絨琰
end


if @case = 201710171112-201710061741 begin  --查詢已標記上傳可以刪除的檔案
	exec SP_Q_INFO_BY_VIDEOID '00003WKF'
	exec SP_Q_INFO_BY_VIDEOID '00002H0B' --O

	if OBJECT_ID(N'tempdb.dbo.#tempImport', N'U') IS NOT NULL
        DROP TABLE #tempImport

	 CREATE TABLE #tempImport (
		FSVIDEO_ID varchar(10)
     )

    BULK insert #tempImport from 'C:\temp\DELETE.log' WITH
        (
               ROWTERMINATOR = '\n'
        );

	select * from #tempImport

	select A.*, B.* from #tempImport A left join TBLOG_VIDEO_HD_MC B on A.FSVIDEO_ID = B.FSVIDEO_ID where B.FCSTATUS not in ('C', 'N', 'O')
	--00003WDP X 置換刪除 00003VLJ FCSTATUS = '' 等待上傳

	select FSVIDEO_ID from MAM..TBLOG_VIDEO_HD_MC where FCSTATUS not in ('C', 'N', 'O') and FSVIDEO_ID <> '' order by FDUPDATED_DATE desc --有003LVJ

	exec SP_Q_INFO_BY_VIDEOID '00003XU9'
end

if @case = 201710171224 begin --TSM recall 卡住
	select * from TBTSM_JOB where FNTSMJOB_ID = 263500 --5 執行plink得到錯誤的回傳內容：指令 = dsmrecall /hakkatv/HVideo/2016/20160600617/P201606006170001.mxf, 參數 = 10.13.210.15 -l root -pw TSMP@ssw0rdTSM ". /root/.bash_profile";"dsmrecall /hakkatv/HVideo/2016/20160600617/P201606006170001.mxf", 錯誤訊息 = FATAL ERROR: Server unexpectedly closed network connection
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 263500
	exec SP_Q_INFO_BY_VIDEOID '00002NU7'
	select * from TBTSM_JOB where FNTSMJOB_ID = 263431 --3
	select * from TBTSM_JOB where FNTSMJOB_ID = 263629 --3
end

if @case = 201710171536 begin --主控需要檢查聲音
	exec SP_Q_INFO_BY_VIDEOID '00002PVT' --\\mamstream\MAMDFS\ptstv\HVideo\2013\2013569\G201356900010015.mxf
end

if @case = 201710181726 begin --2我們的島調用檔案轉檔完成但無檔案
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201710130001' --news9456 \\mamstream\MAMDFS\ptstv\HVideo\0000\0000072\G000007209170001.mxf
end

if @case = 201710190953 begin --紫色大稻埕 14  審核通過未轉檔入庫
	exec SP_Q_INFO_BY_VIDEOID '0000305H'  --B, O
	select * from TBLOG_VIDEO where FSID = '2017136' and FSARC_TYPE = '002' order by FNEPISODE
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201713600140001'
end


if @case = 201710191439 begin --TSM recall 卡住
	exec SP_Q_INFO_BY_VIDEOID '00003J8V' --勞動之王 13 G201667000130001
	select * from TBTSM_JOB where FNTSMJOB_ID = 263944 --5 執行plink得到錯誤的回傳內容：指令 = dsmrecall /hakkatv/HVideo/2016/2016670/G201667000130001.mxf, 參數 = 10.13.210.15 -l root -pw TSMP@ssw0rdTSM ". /root/.bash_profile";"dsmrecall /hakkatv/HVideo/2016/2016670/G201667000130001.mxf", 錯誤訊息 = FATAL ERROR: Server unexpectedly closed network connection
	select * from TBTSM_JOB where FNTSMJOB_ID = 264199
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 263944
end

if @case = 201710191450 begin --要置換送播單填到送帶轉檔置換單
	exec SP_Q_INFO_BY_VIDEOID '00003FXI' --歡迎光臨 7 2017194 , 2017-10-21
	select * from TBLOG_VIDEO where FSID = '2017194' and FNEPISODE = 7 --G201719400070001 00003FXI, T, G201719400070006, 00003Y40 B --> 置換 G201719400070001
	exec SP_Q_INFO_BY_VIDEOID '00003Y40'
	update TBLOG_VIDEO set FCFILE_STATUS = 'D' where FSFILE_NO = 'G201719400070006' --201709250021
end

if @case = 201710191524 begin --檔案無法調用
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201526500010001' --2015265, 1
end

if @case = 201710201012 begin --2017亞洲棒球錦標賽 R, Anystream 出現錯誤
	exec SP_Q_INFO_BY_VIDEOID '00003ZXC' --201710120037
    select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G201811800060001' --TC in= 11:58:06;00, TC out = 14:34:56;00
end

if @case = 201710201019 begin --酸甜之味 6 reschedule
	exec SP_Q_INFO_BY_VIDEOID '00003V47' --169844, R
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201769800060002'
	select * from TBTRANSCODE where FNTRANSCODE_ID = 169844 --4, ANDID = 119341
end

if @case = 201710201202 begin
	exec SP_Q_INFO_BY_VIDEOID '000040J4' --G201732000140001, 201710200038
	select * from TBBROADCAST where FSBRO_ID = '201710200038' --no data
	-- 刪除轉檔單及段落資料
	exec SP_D_TBLOG_VIDEO_BYFILENO 'G201732000140001'
end


/* 
 * 手動入庫 : 
 */
if @case = 201710201442 begin --手動入庫
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201786800040009' --R 174261, 000040DV
	select * from TBTRANSCODE where FNTRANSCODE_ID = 174261

	exec SP_Q_TBTRACKINGLOG_BY_DATE '174261', '122928', '264040', 'G201786800040009', '0040DV', '2017-10-18', NULL
	--plink高解搬檔失敗：JobID 174261/10.13.210.15 -l root -pw TSMP@ssw0rdTSM "cp -r /mnt/HDUpload/0040DV.mxf  /ptstv/HVideo/2017/2017868/G201786800040009.mxf"/cp: cannot stat `/mnt/HDUpload/0040DV.mxf': No such file or directory

	update TBLOG_VIDEO set FCFILE_STATUS = 'T', FCLOW_RES = 'Y', FCKEYFRAME = 'N' where FSFILE_NO = 'G201786800040009'
	--cp /mnt/HDUpload/0040DV.mxf  /ptstv/HVideo/2017/2017868/G201786800040009.mxf
	--cp /mnt/18TB/IngestWorkingTemp/G201786800040009.mp4 /Media/Lv/ptstv/2017/2017868
	exec SP_U_TBLOG_VIDEO_FILE_STATUS 'G201786800040009', 'T', '51204'

	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201786800040009' --no data

	select * from TBLOG_VIDEO where FSFILE_NO = 'G201786800020012' --R 174262, 000040DU
	exec SP_Q_TBTRACKINGLOG_BY_DATE '174262', '122929', '264041', 'G201786800020012', '0040DU', '2017-10-18', NULL
	--plink高解搬檔失敗：JobID 174262/10.13.210.15 -l root -pw TSMP@ssw0rdTSM "cp -r /mnt/HDUpload/0040DU.mxf  /ptstv/HVideo/2017/2017868/G201786800020012.mxf"/cp: cannot stat `/mnt/HDUpload/0040DU.mxf': No such file or directory

	select * from TBTRANSCODE where FNTRANSCODE_ID = 174262
	update TBLOG_VIDEO set FCFILE_STATUS = 'T', FCLOW_RES = 'Y', FCKEYFRAME = 'N' where FSFILE_NO = 'G201786800020012'

	exec SP_I_TBLOG_VIDEO_D 'G201786800040009', 'G20178680004', 'G201786800040009_00000000', 'G', '2017868', 4, 'N', '', '', 'T', '', ''
	, 55869, '01', '01:00:00;00', '01:24:10;00', 'N', '', '來源檔案名稱：0040DV', '51204', '51204'

	exec SP_I_TBLOG_VIDEO_D 'G201786800020012', 'G20178680002', 'G201786800020012_00000000', 'G', '2017868', 2, 'N', '', '', 'T', '', ''
	, 55869, '01', '01:00:00;00', '01:24:10;00', 'N', '', '來源檔案名稱：0040DU', '51204', '51204'

	select * from TBLOG_VIDEO_D where FSID = '2017868' and FCFILE_STATUS = 'T' order by FSFILE_NO


	/*
	cp /mnt/18TB/IngestWorkingTemp/G201786800020012.mp4 /Media/Lv/ptstv/2017/2017868
	cp -r /mnt/HDUpload/0040DU.mxf  /ptstv/HVideo/2017/2017868/G201786800020012.mxf
	*/
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201786800020012' --no data
end

----------------------------------------------------20171030------------------------------------

if @case = 201710301121 begin --提前執行 MoveOtherVideo
	select * from TBLOG_VIDEO where FDCREATED_DATE > '2017-10-30' and FSARC_TYPE = '026' order by FDCREATED_DATE
end

if @case = 201710301125 begin --獨立特派員 第498集 無法 retrieve
	select * from TBLOG_VIDEO where FSFILE_NO = 'G200721704980001' --\\mamstream\MAMDFS\ptstv\HVideo\2007\2007217\G200721704980001.mxf taponly
	select * from TBLOG_VIDEO where FSFILE_NO = 'G200721704990001' --獨立特派員 第499集 \\mamstream\MAMDFS\ptstv\HVideo\2007\2007217\G200721704990001.mxf access denied
end

if @case = 201710301407 begin --查何時搬檔
	exec SP_Q_INFO_BY_VIDEOID '00002GRE' --P201603008130001 X
end

if @case = 201710301408 begin
	exec SP_Q_INFO_BY_VIDEOID '00003GAE' --動畫說漢字49_材料篇, T 2017-11-01
end

if @case = 201710301411 begin --調用檔案已過期被清除
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201710130033' --prg60145, 李美佩
/*
\\mamstream\MAMDFS\ptstv\HVideo\2012\2012419\G201241900040003.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2012\2012419\G201241900030002.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2012\2012419\G201241900020002.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2012\2012419\G201241900050002.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2012\2012419\G201241900060002.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2012\2012771\G201277100010022.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2012\2012419\G201241900010008.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2012\2012771\G201277100020013.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2012\2012771\G201277100030013.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2012\2012771\G201277100040014.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2012\2012771\G201277100050013.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2012\2012771\G201277100060013.mxf
*/
end

if @case = 201710301417 begin --TSM recall 卡住
	select * from TBTSM_JOB where FNTSMJOB_ID in (265653, 265849, 265953, 265944, 265931, 265902, 266034, 266080) --5
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID in (265653, 265849, 265953, 265944, 265931, 265902, 266034, 266080)
	exec SP_Q_INFO_BY_VIDEOID '000040H1' --not scheduled

	--access denied
	select * from TBTSM_JOB where FNTSMJOB_ID = 265653 --still fail after reset /mvtv/HVideo/2017/20170300454/P201703004540001.mxf dsmrecall fail, access denied
	select * from TBTSM_JOB where FNTSMJOB_ID = 265931 --still fail after reset /mvtv/HVideo/2017/20170600538/P201706005380001.mxf
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 265653

	select * from TBLOG_VIDEO where FSFILE_NO = 'P201703004540001' --\\mamstream\MAMDFS\mvtv\HVideo\2017\20170300454\P201703004540001.mxf

	--E02930L4
	select * from TBLOG_VIDEO where FSFILE_NO = 'P201703004540001' --00003GAE
	exec SP_Q_INFO_BY_VIDEOID '00003GAE' --動畫說漢字49_材料篇
	select * from TBTSM_JOB where FNTSMJOB_ID = 266387 --5
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 266387

	exec SP_Q_INFO_BY_VIDEOID '00003P8X' --華陶窯 2017-11-03 ch08
end

if @case = 201710301423 begin --a29082241
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201710230055'
/*
\\mamstream\MAMDFS\ptstv\HVideo\2016\2016200\G201620000020010.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2016\2016200\G201620000040014.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2016\2016200\G201620000070013.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2016\2016200\G201620000100011.mxf
*/
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201710240035'
/*
\\mamstream\MAMDFS\ptstv\HVideo\2015\2015100\G201510000020008.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2015\2015100\G201510000030008.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2011\2011421\G201142100270023.mxf
*/
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201710240001'
/*
\\mamstream\MAMDFS\ptstv\HVideo\0000\0000816\G000081613060002.mxf offline, E02394L4
\\mamstream\MAMDFS\ptstv\HVideo\2015\2015767\G201576700040015.mxf
*/
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201710260028'
/*
\\mamstream\MAMDFS\ptstv\HVideo\0000\0000060\G000006018530003.mxf
\\mamstream\MAMDFS\ptstv\HVideo\0000\0000060\G000006018570001.mxf
\\mamstream\MAMDFS\ptstv\HVideo\0000\0000060\G000006018580002.mxf
*/

	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201710300057' --\\mamstream\MAMDFS\ptstv\HVideo\2017\2017375\G201737500010002.mxf a29082241

	--recall fail /ptstv/HVideo/0000/0000816/G000081613060002.mxf tape only

end

if @case = 201710301611 begin --adm3036 劉秋信 調用失敗 done
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201710260116'
/*
\\mamstream\MAMDFS\ptstv\HVideo\2006\2006123\G200612300010001.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2007\2007813\G200781300010014.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2008\2008277\G200827700010001.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2009\2009266\G200926600010001.mxf
*/
end

if @case = 201710301649 begin --reschedule done
	exec SP_Q_INFO_BY_VIDEOID '00003XHD' --R, G000008255540002, 0000082, 5554, 171779
	select * from TBTRANSCODE where FNTRANSCODE_ID = 171779 --4, ANSID=120950
end

if @case = 201710310839 begin --done
	exec SP_Q_INFO_BY_VIDEOID '0000413S' --PTS1_城市情歌5+6集預告(週間+週末版) 2017-10-31
end


/* 
 * 手動入庫 : 下課花路米-同理心(6) (9)低解檔無法檢索 done
 */
if @case = 201710311027-201710201442 begin --手動入庫
    ----------- process episode 6 -------------
	exec SP_Q_INFO_BY_VIDEOID '000040J6' --T, 下課花路米－同理心大考驗系列及南向世界遺產系列, G201786800060006, 2017868, 6
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201786800060006' --no data
	/*
	Hi res and low res file already exist
	\\mamstream\MAMDFS\ptstv\HVideo\2017\2017868\G201786800060006.mxf
	\\mamstream\MAMDFS\Media\Lv\ptstv\2017\2017868\G201786800060006.mp4
	-rw-rw-rw- 1 nobody nobody 11085360380 Oct 23 10:20 G201786800060006.mxf
	*/
	--update TBLOG_VIDEO set FCLOW_RES = 'Y', FCKEYFRAME = 'N' where FSFILE_NO = 'G201786800060006'
	exec SP_I_TBLOG_VIDEO_D 'G201786800060006', 'G20178680006', 'G201786800060006_00000000', 'G', '2017868', 6, 'N', '', '', 'T', '', ''
	, 55869, '01', '01:00:00;00', '01:24:05;00', 'N', '', '來源檔案名稱：0040J6', '51204', '51204'

	----------- process episode 9 ----------
	exec SP_Q_INFO_BY_VIDEOID '000040J9' --T, 2017868, 9, G201786800090006
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201786800090006' --no data
	/*
	\\mamstream\MAMDFS\ptstv\HVideo\2017\2017868\G201786800090006.mxf , no such file cp manually
	\\mamstream\MAMDFS\Media\Lv\ptstv\2017\2017868\G201786800090006.mp4
	*/
	--update TBLOG_VIDEO set FCLOW_RES = 'Y', FCKEYFRAME = 'N' where FSFILE_NO = 'G201786800090006'
	exec SP_I_TBLOG_VIDEO_D 'G201786800090006', 'G20178680009', 'G201786800090006_00000000', 'G', '2017868', 9, 'N', '', '', 'T', '', ''
	, 55869, '01', '01:00:00;00', '01:24:10;00', 'N', '', '來源檔案名稱：0040J9', '51204', '51204'
end


if @case = 201710201012 begin --2017亞洲棒球錦標賽 R, Anystream 出現錯誤  -> 手動入庫
	exec SP_Q_INFO_BY_VIDEOID '00003ZXC' --201710120037
    select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G201811800060001' --TC in= 11:58:06;00, TC out = 14:34:56;00

	select FSPROG_ID from TBPROG_M where FSPGMNAME like '%亞洲棒球錦標賽%'
	select * from TBPROG_D where FSPROG_ID  in  (select FSPROG_ID from TBPROG_M where FSPGMNAME like '%亞洲棒球錦標賽%')
end

if @case = 201710310936 begin
	exec SP_Q_INFO_BY_VIDEOID '00001617' --湊陣 2017-11-02 ch12 \\mamstream\MAMDFS\ptstv\HVideo\2014\2014048\G201404800010013.mxf, O, T
end


if @case = 201710311623 begin --城市情歌 已經轉檔成功，刪除 TBLOG_VIDEO 資料，重新建立送播單後，檢索會重複
	select * from TBLOG_VIDEO where FSBRO_ID = '201710020062'
	select * from TBPROG_M where FSPGMNAME like '%城市情歌%'
	select * from TBLOG_VIDEO where FSID = '2017441' and FSARC_TYPE = '002' and FCFILE_STATUS = 'T' order by FNEPISODE
	select * from TBLOG_VIDEO_D where FSFILE_NO in ('G201744100010019', 'G201744100010014')
	update TBLOG_VIDEO_D set FCFILE_STATUS = 'D' where FSFILE_NO = 'G201744100010014'
end

if @case = 201710311745 begin --purge OtherVideo file
	select * from TBLOG_VIDEO where FSARC_TYPE = '026' and FDCREATED_DATE >= '2017-10-31'
	--check FSOLD_FILE_NAME : test-path -path \\10.13.200.22\mamnas1\OtherVideo\客家新聞雜誌\客家新聞雜誌第558集 無字慕分軌檔.mxf
end

if @case = 2017010311749 begin --playlist 會抓到舊的 VIDEOID done
	exec SP_Q_INFO_BY_VIDEOID '000040RD'
	exec SP_Q_INFO_BY_VIDEOID '000041EU' --PTS3 RM【尤文圖斯VS奧林匹亞科斯】2017-18 歐洲冠軍足球聯賽 2017-11-03 ch14
	select * from TBLOG_VIDEOID_MAP where FSID = '20171000590' --CURRENT=000041EU
	update TBLOG_VIDEOID_MAP set FSVIDEO_ID_PROG = '000040RD' where FSID = '20171000590'
end

if @case = 201710411751 begin --檔案仍在 Review 卡在 ANS 處理中 done
	exec SP_Q_INFO_BY_VIDEOID '000040RP' --聽聽看106年11月Promo輪標舞_pts1,O, S P201710006010001, 2017-11-01, 174986
	select * from TBTRANSCODE where FNTRANSCODE_ID = 174986 --ANSID = 123548, 3, 2017-10-30 12:06:10.777
end

if @case = 201711011107 begin
	select * from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = '00002GRE'
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002GRE' --P201603008130001, X
	select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002GRE'
end

if @case = 201711011202 begin --PTS1 2017/11/03 無法匯出
	select * from TBPGM_COMBINE_QUEUE where FSCHANNEL_ID = '12' and FDDATE = '2017-11-03' order  by FSPLAY_TIME
	select * from TBPGM_ARR_PROMO where FSCHANNEL_ID = '12' and FDDATE = '2017-11-03' order by FSPLAYTIME
	select * from TBPGM_COMBINE_QUEUE where FSCHANNEL_ID = '62' and FDDATE = '2017-11-03' order  by FSPLAY_TIME
	select * from TBPGM_ARR_PROMO where FSCHANNEL_ID = '62' and FDDATE = '2017-11-03' order by FSPLAYTIME
	select * from TBTRACKINGLOG where FDTIME > '2017-11-01 12:00' order by FDTIME
	select * from TBTRAN_LOG where FDDATE > '2017/11/01 13:50' order by FDDATE
	select * from TBZCHANNEL
	update TBZCHANNEL set FSOLDTABLENAME = 'PSHOW_PTS' where FSCHANNEL_ID = '15'

	--刪除運行表
	declare d = 
	exec SP_D_TBPGM_COMBINE_QUEUE '12', select CAST('2017-11-03' as DATE)
	exec SP_D_TBPGM_ARR_PROMO '12', '2017-11-03'

	select * from TBPGM_ARR_PROMO where FSCHANNEL_ID = '15' and FDDATE = '2017-04-11'
	select * from TBPGM_COMBINE_QUEUE where FSCHANNEL_ID = '15' and FDDATE = '2017-04-11' order by FSPLAY_TIME

	delete from TBPGM_COMBINE_QUEUE where FSCHANNEL_ID = '15' and FDDATE = '2017-04-11'
	delete from TBPGM_ARR_PROMO where FSCHANNEL_ID = '15' and FDDATE = '2017-04-11'

	select * from TBPGM_COMBINE_QUEUE where FSCHANNEL_ID = '15' and FDDATE = '2017-04-13' order by FSPLAY_TIME
	select * from TBPGM_ARR_PROMO where FSCHANNEL_ID = '15' and FDDATE = '2017-04-13' order by FSPLAYTIME

	
end

if @case = 201711011537 begin --手動刪除置換 done
	select * from TBLOG_VIDEO where FSBRO_ID = '201710200082' --G201803000090003, 四重奏, 9
	select * from TBPROG_M where FSPROG_ID = '2018030'
	update TBLOG_VIDEO set FCFILE_STATUS = 'D' where FSFILE_NO = 'G201803000090003'
end

if @case = 201711011545 begin --入庫無審核清單 無流程單號
	select * from TBARCHIVE where FSARC_ID = '201710170034' --847
	select * from TBUSERS where FSUSER_ID = '51189' --王紫嫣
	select * from TBPROG_M where FSPROG_ID = '0000159' --聽聽看
end

if @case = 201711011609 begin --轉檔失敗無法置換 done
	select * from TBLOG_VIDEO where FSBRO_ID in ('201710310113', '201710310115', '201710310116', '201710310117')
	update TBLOG_VIDEO set FCFILE_STATUS = 'D' where FSFILE_NO in ('G201735600010012', 'G201735600020017', 'G201735600030015', 'G201735600040017')
end

if @case = 201711011645 begin --pub50686, 李梅毓
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201710260069'
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201710260087'
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201710260089'
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201710260091'
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201710260112'
/*
\\mamstream\MAMDFS\ptstv\HVideo\0000\0000060\G000006020300001.mxf
\\mamstream\MAMDFS\ptstv\HVideo\0000\0000060\G000006020300001.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2015\2015404\G201540400890008.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2015\2015404\G201540400890008.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2015\2015404\G201540400890008.mxf
\\mamstream\MAMDFS\ptstv\HVideo\0000\0000816\G000081614260008.mxf R
\\mamstream\MAMDFS\ptstv\HVideo\0000\0000816\G000081614260008.mxf
\\mamstream\MAMDFS\ptstv\HVideo\0000\0000816\G000081614280005.mxf
\\mamstream\MAMDFS\ptstv\HVideo\0000\0000816\G000081614280005.mxf
\\mamstream\MAMDFS\ptstv\HVideo\0000\0000816\G000081614280005.mxf
\\mamstream\MAMDFS\ptstv\HVideo\0000\0000816\G000081614280005.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2017\2017670\G201767000040001.mxf
*/
end

if @case = 201711011701 begin --客台大批短帶被刪
	exec SP_Q_INFO_BY_VIDEOID '000036QL'
	select * from TBPGM_PROMO where FSPROMO_ID = '20161200022'
	select * from TBLOG_VIDEO where FSID = '20161200022'
	select * from TBUSERS where FSUSER_ID = '70390' --陳忻沛
	select * from TBPGM_PROMO where FSCREATED_BY = '70390' order by FDCREATED_DATE
	select top 100 *  from TBPGM_PROMO order by FDCREATED_DATE desc
end


	select * from TBLOG_VIDEO where FSFILE_NO = 'G200721704980001'

if @case = 201711021059 begin --無法置換問題
	select * from TBPROG_M where FSPROG_ID = '2017375' --熊星人和地球人

	select * from TBLOG_VIDEO where FSID = '2017375' and FNEPISODE in (12, 13) and FSARC_TYPE = '002' order by FNEPISODE --G201737500120006, G201737500130006
	update TBLOG_VIDEO set FCFILE_STATUS = 'D' where FSFILE_NO in ('G201737500120006',  'G201737500130006')
end

if @case = 201711021449 begin --done
	exec SP_Q_INFO_BY_VIDEOID '000041EW' --喔走!48小時(原名:小創客探險去) R 要置換 G201732000160001
	update TBLOG_VIDEO set FCFILE_STATUS = 'D' where FSFILE_NO = 'G201732000160001'
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201732000160001'
end

if @case = 201711021521 begin
	exec SP_Q_INFO_BY_VIDEOID '00003LEX' --動物小遊俠 10 \\mamstream\MAMDFS\ptstv\HVideo\2017\2017704\G201770400100001.mxf
	select * from TBTSM_JOB where FNTSMJOB_ID = 266627
end

if @case = 201711021655 begin
	select distinct(FDDATE) from TBPGM_COMBINE_QUEUE where FSCHANNEL_ID = '08' order by FDDATE
end

if @case = 201711021746 begin
	exec SP_Q_INFO_BY_VIDEOID '000041MW' --N
	exec SP_Q_INFO_BY_VIDEOID '000041NC' --N
	exec SP_Q_INFO_BY_VIDEOID '000041ND' --N
	exec SP_Q_INFO_BY_VIDEOID '000041NQ'
end

if @case = 201711031112 begin --a29082241 調用失敗
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201711010007'
/*
\\mamstream\MAMDFS\ptstv\HVideo\2017\2017254\G201725400010010.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2017\2017254\G201725400050009.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2017\2017254\G201725400070009.mxf
*/
end

if @case = 201711031412 begin
	select * from TBLOG_VIDEO where FSBRO_ID in ('201711030009', '201711030010', '201711030012') --175398, 175399, 175400
	--G201540400920008, G201540400930008, G000021604780008
	select * from TBTRANSCODE where FNTRANSCODE_ID in (175398, 175399, 175400)
	exec SP_Q_TBTRACKINGLOG_BY_DATE 175398, 123903, 267087, 'G201540400920008', '0041Q0', '2017-11-03'
	exec SP_Q_TBTRACKINGLOG_BY_DATE 175399, 123904, 267088, 'G201540400930008', '0041Q1', '2017-11-03'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201540400920008'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G201540400930008'
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G000021604780008'
end

if @case = 201711031507 begin --TSM 處理中 重新審核 done
	exec SP_Q_INFO_BY_VIDEOID '000041QA' --S 175408 【北市府公益託播】吃在台北 2+3台共播 2017-11-06, ch13, P201710007430002
	select * from TBTRANSCODE where FNTRANSCODE_ID = 175408 --2, 2017-11-03 09:46:14.607
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201710007430002'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201710007430002'
end

if @case = 201711031625 begin --
	select * from TBLOG_VIDEO where FSJOB_ID = '170516' --G201802300030002, 2018023 3, 00003W3Q
	select * from TBTRANSCODE where FNTRANSCODE_ID = 170516 --ANSID = 119901, 4 2017-09-04 17:24:59.370
	exec SP_Q_INFO_BY_VIDEOID '00003W3Q' --R

end

if @case = 201711031705 begin --短帶 QUE 表
    select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20170100113'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20161200275'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20161100800'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20161100511'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME
--------
	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20170900207'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20170900413'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20171000178'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20171000163'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	--no data
	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20171000162'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20170900076'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20171000297'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	--節目
	select * from TBPGM_QUEUE A left join TBZCHANNEL B on A.FSCHANNEL_ID = B.FSCHANNEL_ID where FSPROG_ID = '2018044' order by A.FDDATE
	select FSPROG_ID 節目編號, FSPROG_NAME 節目名稱, FDDATE 播出日期, FSBEG_TIME 播出時間, B.FSSHOWNAME 播出頻道 from TBPGM_QUEUE A left join TBZCHANNEL B on A.FSCHANNEL_ID = B.FSCHANNEL_ID where FSPROG_ID = '2018044' order by A.FDDATE, A.FSCHANNEL_ID
	select FSPROG_ID 節目編號, FSPROG_NAME 節目名稱, FDDATE 播出日期, FSBEG_TIME 播出時間, B.FSSHOWNAME 播出頻道 from TBPGM_QUEUE A left join TBZCHANNEL B on A.FSCHANNEL_ID = B.FSCHANNEL_ID where FSPROG_ID = '2018151' order by A.FDDATE, A.FSCHANNEL_ID

end

if @case = 201711031657 begin --客台的組織架構有問題
	select * from TBUSERS where FSUSER_ID = '50842' --yangyt 楊雅婷 --> 吳奕蓉 not 范光中
end

if @case = 201711060926 begin
	select * from TBPROG_M where FSPGMNAME like '%四問四吵%' --2018023, reschedule, done
	select * from TBLOG_VIDEO where FSID = '2018023' and FNEPISODE = 3 --R, G201802300030002, 170516, 00003W3Q
	select * from TBTRANSCODE where FNTRANSCODE_ID = 170516 --ANSID = 119901 --2017-09-04 17:24:59.370, 4
end

if @case = 201711061110 begin
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201809400010005'
end

if @case = 201711061111 begin --刪除送播單置換
	select * from TBLOG_VIDEO where FSBRO_ID = '201710200077' --G201803000080001, 2018030, #8
	select * from TBPROG_M where FSPROG_ID = '2018030' --四重奏
	update TBLOG_VIDEO set FCFILE_STATUS = 'D' where FSFILE_NO = 'G201803000080001'
end

if @case = 201711061120 begin
	exec SP_Q_INFO_BY_VIDEOID '00003ZC0' --看公視說英語, 5566, 2017-11-09 ch13
	select * from TBTSM_JOB where FNTSMJOB_ID in (267288, 267432, 267423, 267490)
	--執行plink得到錯誤的回傳內容：指令 = dsmrecall /ptstv/HVideo/2016/20160500040/P201605000400002.mxf, 參數 = 10.13.210.15 -l root -pw TSMP@ssw0rdTSM ". /root/.bash_profile";"dsmrecall /ptstv/HVideo/2016/20160500040/P201605000400002.mxf", 錯誤訊息 = FATAL ERROR: Server unexpectedly closed network connection
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID in (267288, 267432, 267423, 267490)
end

if @case = 201711061133 begin
	select * from TBPROG_M where FSPGMNAME like '%地球的朋友%' --0000198
	select * from TBLOG_VIDEO where FSID = '0000198' and FSARC_TYPE = '001' --\\mamstream\MAMDFS\Media\Lv\ptstv\0000\0000198\G000019800130001.wmv, G000019800090001
	--\\mamstream\MAMDFS\Media\Lv\ptstv\0000\0000198\G000019800090001.wmv
end

if @case = 201711061207 begin --adm3036
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201710270078'
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201710270083'
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201710270092'
/*
\\mamstream\MAMDFS\ptstv\HVideo\2014\2014514\G201451400360008.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2015\2015548\G201554800090002.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2015\2015548\G201554800260002.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2015\2015548\G201554800330002.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2010\2010688\G201068800340011.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2015\2015548\G201554800350002.mxf
*/
end

if @case = 201711061228 begin --林喬屏
	--查詢公益託播
	exec SP_Q_WELFAREPROMO_PLAYOUT_BY_DATE '07', '2017-01-01', '2017-06-30', '1'  --播出紀錄
    exec SP_Q_WELFAREPROMO_PLAY_COUNT_BY_DATE '07', '2017-01-01', '2017-06-30'  --播出次數
end

if @case = 201711070918 begin
	select * from TBTSM_JOB where FNTSMJOB_ID = 267735  --/ptstv/HVideo/2017/2017599/G201759900010001.mxf
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201759900010001' --00003BR1
	select * from TBTSM_JOB where FSSOURCE_PATH like '%G201759900010001%' order by FDREGISTER_TIME
end

if @case = 201711070932-201710311745 begin --purge OtherVideo file purge-othervideo.ps1
	select * from TBLOG_VIDEO where FSARC_TYPE = '026' and FDCREATED_DATE >= '2017-10-31'
	--check FSOLD_FILE_NAME : test-path -path \\10.13.200.22\mamnas1\OtherVideo\客家新聞雜誌\客家新聞雜誌第558集 無字慕分軌檔.mxf
	select FSOLD_FILE_NAME, FDCREATED_DATE from TBLOG_VIDEO where FSARC_TYPE = '026' and FCFILE_STATUS = 'T' order by FDCREATED_DATE
	select 'ORG_PATH ,  FSFILE_NO' union (select FSOLD_FILE_NAME + ',' + FSFILE_NO from TBLOG_VIDEO where FSARC_TYPE = '026' and FCFILE_STATUS = 'T')
end

if @case = 201711071056 begin --轉檔失敗, reschedule
	exec SP_Q_INFO_BY_VIDEOID '000041IJ' --看公視說英語 5591, R
	select * from TBTRANSCODE where FNTRANSCODE_ID = 175336 --ANSID=123846, 4
end

if @case = 201711071531 begin
	exec SP_Q_INFO_BY_VIDEOID '000041WE' --G201448000480001 未排播
end

if @case = 201711071533 begin --標記上傳後狀態未更新
	exec SP_Q_INFO_BY_VIDEOID '00003VKR' --P201708007660001
	exec SP_U_TBLOG_VIDEO_HD_MC_BY_FILE_NO'P201708007660001', 'C', '51204'
end

if @case = 201711071551 begin --公益託播
    select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20170900907'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	exec SP_Q_INFO_BY_VIDEOID '00003YSB' --20170900907
	select * from TBPGM_PROMO where FSPROMO_ID = '20170900907' --2017公益-網路直播必知的著作權
end

if @case = 201711071600 begin --已播出但未轉檔
	exec SP_Q_INFO_BY_VIDEOID '00003Y7I' --2018049, G201804900010001, N
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00003Y7I' --南水北調, 2017-09-28 ch12
	select * from TBLOG_QC_RESULT where FSFILE_NO = 'G201804900010001' --無審核紀錄
end

if @case = 201711071750 begin --bobohung 調用失敗
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201711020003' --\\mamstream\MAMDFS\ptstv\HVideo\2018\2018094\G201809400010005.mxf
end

if @case = 201711080923 begin --調用磁帶下架
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201711060071'
/*
\\mamstream\MAMDFS\ptstv\HVideo\2014\2014514\G201451400120001.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2011\2011203\G201120300160001.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2011\2011203\G201120300160001.mxf F
*/
end

if @case = 201711101026 begin
	exec SP_Q_INFO_BY_VIDEOID '0000423J'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'C' where FSFILE_NO = 'G201812800010001'
	exec SP_Q_INFO_BY_VIDEOID '000041Z7'
	update TBLOG_VIDEO set FCFILE_STATUS = 'D' where FSFILE_NO = 'G201758102160001'
end

if @case = 201711101111 begin  --reschedule
	exec SP_Q_INFO_BY_VIDEOID '00004128' --R, G201744100220014, 175132
	select * from TBTRANSCODE where FNTRANSCODE_ID = 175132 --ANSID = 123677
	exec SP_Q_INFO_BY_VIDEOID '00003XM5' --天黑請閉眼(白天不宜) R 171719
	select * from TBTRANSCODE where FNTRANSCODE_ID = 171719 --ANSID = 120908
end

if @case = 201711101145 begin --宏觀檔案未到
	exec SP_Q_INFO_BY_VIDEOID '00003DHG' --T
	exec SP_Q_INFO_BY_VIDEOID '00003JZ2' --T
	exec SP_Q_INFO_BY_VIDEOID '00003F3T' --T
	exec SP_Q_INFO_BY_VIDEOID '00003DHG' --T, O

	exec SP_Q_INFO_BY_VIDEOID '00003KU3'
end

if @case = 201711101837 begin --QUE 表 江靜儀
	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20160800569'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20160900490'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME
end

if @case = 201711101847 begin
	exec SP_Q_INFO_BY_VIDEOID '000041ZL' --R
	select * from TBLOG_VIDEO where FSID = '2017868' and FNEPISODE = 13 --G201786800130006, 000041ZM
end

if @case = 201711101956 begin
	select * from TBTSM_JOB where FNTSMJOB_ID = 268411 --5
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 268411
end

if @case = 201711130933 begin
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201558202800001' --000038E9 \\mamstream\MAMDFS\hakkatv\HVideo\2015\2015582\G201558202800001.mxf
	select * from TBLOG_VIDEO where FDUPDATED_DATE > '2017-11-13' order by FDCREATED_DATE
	--\\mamstream\MAMDFS\ptstv\HVideo\2017\20171100241\P201711002410002.mxf
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201558202790001' --000038E8
end

if @case = 201711131620 begin --主控檔案卡住
	exec SP_Q_INFO_BY_VIDEOID '000042GB' --PTS3_酸甜之味_第十集預告 娶我篇_30秒 2017-11-14 ch14
	exec SP_Q_INFO_BY_VIDEOID '000042GA'

	exec SP_Q_INFO_BY_VIDEOID '000041YR' --極樂世界, O, S, G201735600040022, 2017-11-14 ch14 176272 --> T
	select * from TBTRANSCODE where FNTRANSCODE_ID = 176272 --3 ANSID = 124657
end

if @case = 201711131805 begin --水果冰淇淋
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO  '201711010198' --claire0123
/*
\\mamstream\MAMDFS\ptstv\HVideo\0000\0000060\G000006020320001.mxf
\\mamstream\MAMDFS\ptstv\HVideo\0000\0000060\G000006020250003.mxf
*/
end

if @case = 201711151030 begin --回首台灣報業 轉檔成功搬檔失敗
	select * from TBLOG_VIDEO where FSFILE_NO = 'G200513100030002' --S, 00000L6I, 001, 201711110028, 176158
	select * from TBTRANSCODE where FNTRANSCODE_ID = 176158 --4
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G200513100030002'
	select * from TBPROG_M where FSPROG_ID = '2005131' --回首台灣報業
	select * from TBLOG_VIDEO where FSFILE_NO = 'G200513100050002' --S, 201711110030, 001
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G200513100050002'
end

if @case = 201711151044 begin --*.MXF 造成入庫失敗
	select * from TBPROG_M where FSPGMNAME like '%下課花路米-同理心%' --2017868
	select * from TBLOG_VIDEO where FSID = '2017868' and FNEPISODE = 14 --R 000041ZN, 175980, G201786800140006
	select * from TBTRANSCODE where FNTRANSCODE_ID = 175980 --4
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201786800140006'
	exec SP_Q_INFO_BY_VIDEOID '000041ZN' --\\mamstream\MAMDFS\ptstv\HVideo\2017\2017868\G201786800140006.mxf
/*
\\mamstream\MAMDFS\Media\Lv\ptstv\2017\2017868\G201786800140006.mp4
	cp /mnt/HDUpload/0041ZN.mxf /ptstv/HVideo/2017/2017868/G201786800140006.mxf
	cp /mnt/18TB/IngestWorkingTemp/G201786800140006.mp4 /Media/Lv/ptstv/2017/2017868/G201786800140006.mp4
	FNDIR_ID = 55869 01:00:00;00 ~ 01:23:59;29
*/
	update TBLOG_VIDEO set FCFILE_STATUS = 'T', FCLOW_RES = 'Y', FCKEYFRAME = 'N' where FSFILE_NO = 'G201786800140006'

    exec SP_I_TBLOG_VIDEO_D 'G201786800140006', 'G20178680014', 'G20178680014_00000000', 'G', '2017868', 14, 'N', '', '', 'T', '', ''
       , 55869, '01', '01:00:00;00', '01:23:59;29', 'N', '', '來源檔案名稱：0041ZN', '51204', '51204'
end

if @case = 201711151105 begin --大象這一家 尚未入庫
	select * from TBPROG_M where FSPGMNAME like '%大象這一家%' --2018084
	select * from TBLOG_VIDEO where FSID = '2018084'
end

if @case = 201711151152 begin --QUE
	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20170700418'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME
end

if @case = 201711151647  begin --公益託播
    select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20170500819'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20170900251'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	select * from TBPGM_PROMO where FSPROMO_NAME like '%客家學苑%' --20170900251 2017客家學苑-秋季班招生
end

--WEEK 20171116

if @case = 201711161006 begin --TSM recall 卡住
	select * from TBTSM_JOB where FNTSMJOB_ID in  (270452, 270449, 270474, 270489, 270486) --FNSTATUS = 5
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID in  (270452, 270449, 270474, 270489, 270486)
end

if @case = 201711161109 begin
	exec SP_Q_INFO_BY_VIDEOID '00001SUA' --FCSTATUS = '', T G201569900110004
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201238800220017'
	exec SP_Q_INFO_BY_VIDEOID '00001SUE' --FCSTATUS = '', T --G201569900120004
	exec SP_Q_INFO_BY_VIDEOID '00003CIC' --FCSTATUS = '', T --G201238800220017
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'O' where FSFILE_NO in ('G201569900110004', 'G201569900120004', 'G201238800220017')
end

if @case = 201711161536 begin
	exec SP_Q_INFO_BY_VIDEOID '000042TD' --no TBLOG_VIDEO record 1116_PTS3_全球現場
	select * from TBLOG_VIDEOID_MAP where FSVIDEO_ID_PROG = '000042TD'
	select * from TBPGM_PROMO where FSPROMO_NAME like '%1116_PTS3_全球現場%' --20171100388
	select * from TBLOG_VIDEO where FSID = '20171100388' --P201711003880001 000042PE, B
	exec SP_Q_INFO_BY_VIDEOID '000042PE' --1116_PTS3_全球現場 2017-11-19 ch14
	select * from TBLOG_VIDEOID_MAP where FSVIDEO_ID_PROG = '000042PE' --20171100388
end

if @case = 201711161745 begin --GPFS -> Review 狀態未更新
	exec SP_Q_INFO_BY_VIDEOID '00001QQW' --T
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201559700360003'
end

if @case = 201711170957 begin
	select * from TBTSM_JOB where FNTSMJOB_ID in (270885, 270881, 270890, 270889, 270888, 270872, 270864, 270863, 270854) --FNSTATUS = 5
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID in (270885, 270881, 270890, 270889, 270888, 270872, 270864, 270863, 270854)
end

if @case = 201711171116 begin
	exec SP_Q_INFO_BY_VIDEOID '00003VLJ' --紫色大稻埕(60分鐘版) FCSTATUS = '' no TBPTS_AP recorder
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'C' where FSFILE_NO = 'G201808000190001'
end

if @case = 201711171546 begin --reschedule
	exec SP_Q_INFO_BY_VIDEOID '000042VW' --鑽石鑽石亮晶晶 5 S  2017-11-19, 11/16 QC 檔案未搬到 Approved --176612
	select * from TBTRANSCODE where FNTRANSCODE_ID = 176612 --4 ANSID = 124939
end

if @case = 201711171622 begin
	select * from TBPROG_M where FSPGMNAME like '%動物小遊俠%' --2017704
	select * from TBLOG_VIDEO where FSID = '2017704' and FNEPISODE = 56 --R --166788 G201770400560001, 00003OYI
	select * from TBTRANSCODE where FNTRANSCODE_ID = 166788 --ANSID = -1
	exec SP_Q_INFO_BY_VIDEOID '00003OYI'
end

if @case = 201711201014 begin --感恩故事集 轉檔成功搬檔失敗 --> 重新執行轉檔
	select * from TBLOG_VIDEO where FSFILE_NO = 'G200745600190001' --2007456 -S, 201711150101, 176465
	select * from TBPROG_M where FSPROG_ID = '2007456'
	select * from TBTRANSCODE where FNTRANSCODE_ID = 176465 --4
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G200745600190001'
end

if @case = 201711201021 begin --TSM Recall 卡住
	select * from TBTSM_JOB where FNTSMJOB_ID in (271133, 271157, 271162) --FNSTATSU = 5
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID in (271133, 271157, 271162)
end

if @case = 201711201052 begin
	select * from TBBOOKING_DETAIL where FSBOOKING_NO = '201711010039' order by FSFILE_NO
	select * from TBBOOKING_MASTER where FSBOOKING_NO = '201711010039' --70062
	select * from TBUSERS where FSUSER_ID = '70062' --news0002

/*
G201432000010002
G201432000010003
G201432000010004
G201432000010005
G201432000010006
G201432000010007
G201432000010008
G201432000010009
G201432000010010
G201432000010011
G201441500010002
G201441500010003
G201441500010004
G201441500010005
G201441500010006
G201441500010007
G201441500010008
G201441500010009
G201441500010010
G201441500010011
G201523000010003
G201523000010004
G201523000010005
G201523000010006
G201523000010007
G201523000010008
G201523000010009
G201523000010010
G201523000010011
G201523000010012
*/
end

if @case = 201711201341 begin --轉檔失敗，但是檔案不在 HDUpload
	exec SP_Q_INFO_BY_VIDEOID '000041Y6' --R 2017-18 歐洲冠軍足球聯賽 --2017-11-21
end

if @case = 201711201735 begin
	exec SP_Q_INFO_BY_VIDEOID '000042H0'
end

if @case = 201711211109 begin --TSM RECALL 卡住
	select * from TBTSM_JOB where FNTSMJOB_ID in (271133, 271351)
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 271351
end

if @case = 201711211345 begin --a29082241 tapeoffline
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201711160021'
/*
\\mamstream\MAMDFS\ptstv\HVideo\2010\2010154\G201015400010002.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2009\2009234\G200923400010002.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2010\2010176\G201017600010003.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2010\2010176\G201017600010003.mxf
*/
end

if @case = 201711211645 begin
	exec SP_Q_INFO_BY_VIDEOID '000042LY' --D 2017554, 224
	select * from TBPROG_M where FSPROG_ID = '2017554' --000042LY : PeoPo公民新聞報(2017週間版) #224 ：置換
	select * from TBLOG_VIDEO where FSID = '2017554' and FNEPISODE = 224 --000042XG
	exec SP_Q_INFO_BY_VIDEOID '000042AX' --2017福氣來了 #230 2017581, 230 F:審核不通過
	select * from TBLOG_VIDEO where FSID = '2017581' and FNEPISODE = 230
	exec SP_Q_INFO_BY_VIDEOID '00003VLK' --D, 2018080, 20
	select * from TBLOG_VIDEO where FSID = '2018080' and FNEPISODE = 20 --000042Y5
	select * from TBPROG_M where FSPROG_ID = '2018080' --00003VLK 紫色大稻埕(60分鐘版) #20
end

if @case = 201711221432 begin --a29082241
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201711210055'
/*
\\mamstream\MAMDFS\ptstv\HVideo\2014\2014952\G201495200010004.mxf
*/
	exec SP_Q_INFO_BY_VIDEOID '0000434G'
end

if @case = 201711221451 begin --TSM recall 失敗
	select * from TBTSM_JOB where FNTSMJOB_ID in (271519, 271526, 271525, 271515, 271527, 271522, 271531, 271523, 271528, 271520) --FNSTATUS = 5
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID in (271519, 271526, 271525, 271515, 271527, 271522, 271531, 271523, 271528, 271520)
end

if @case = 201711221717 begin
	exec SP_Q_INFO_BY_VIDEOID '0000436L'
end

if @case = 201711231035 begin --刪除送帶轉檔單
	select * from TBPROG_M where FSPROG_ID = '2011025' --飆!企鵝里德(原名:我為你加油)
	select * from TBLOG_VIDEO where FSID = '2011025' and FSARC_TYPE = '002' and FCFILE_STATUS = 'B' --20 手動刪除作業 B -> X
	--1,2,3, 4 查詢不到 G201102500010002, G201102500020002, G201102500030002, G201102500040008
	update TBLOG_VIDEO set FCFILE_STATUS = 'X' where FSFILE_NO in ('G201102500010002', 'G201102500020002', 'G201102500030002', 'G201102500040008')
	select * from TBLOG_VIDEO_D where FSFILE_NO in ('G201102500010002', 'G201102500020002', 'G201102500030002', 'G201102500040008') --no record
end

if @case = 201711231127 begin --熊星人和地球人E10 分軌帶.mxf 入庫卡在轉檔中
	select * from TBLOG_VIDEO where FSBRO_ID = '201711210115' --G201737500100020 S --> T
	--update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201737500100020' --> exec PTS_MoveOtherVideoFile.exe (@MAM1)
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201737500100020' --no record -> T
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201737500100021'
end

if @case = 201711231417 begin --汪達的外星朋友_2016PTS2 RM 
	exec SP_Q_INFO_BY_VIDEOID '0000410L' --S 174822 P201710006520001  --> T
	select * from TBTRANSCODE where FNTRANSCODE_ID = 174822 --8 2017-10-26 15:12:21.950
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201710006520001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201710006520001'

	select * from TBUSERS where FSUSER = 'hsing1'
end

if @case = 201711231529 begin --TSM 處理中
	select * from TBPROG_M where FSPGMNAME like '%我的這一班%' --0000216
	select * from TBLOG_VIDEO where FSID = '0000216' and FNEPISODE = 474 --R 00003VIE, 171623
	exec SP_Q_INFO_BY_VIDEOID '00003VIE'
	select * from TBTRANSCODE where FNTRANSCODE_ID = 171623 --FNPROCESS_STATUS = 1 --2017-09-18 10:09:23.673
	select * from TBUSERS where FSUSER_ChtName = '鄭文欣' --hsincheng

	select * from TBPROG_M where FSPGMNAME like '%一字千金%' --2015404
	select * from TBLOG_VIDEO where FSID = '2015404' and FNEPISODE = 84 --G201540400840010, 00003Z37
	exec SP_Q_INFO_BY_VIDEOID '00003Z37'
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00003Z37'
	select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00003NO4'
end

if @case = 201711231732 begin --tapeoffline
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201711210120' --廖婕妤 -news70406 \\mamstream\MAMDFS\ptstv\HVideo\0000\0000072\G000007201340002.mxf
end


if @case = 201711240917 begin --TSMRECALL fail
	select * from TBTSM_JOB where FNTSMJOB_ID in (271700,271709, 271898, 271897, 271895, 271894, 271887, 271885, 271882, 271882, 271881, 271880, 271905, 271874, 271872) --5x4
	--update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID in (271887, 271885, 271709, 271700)
	
end

if @case = 201711241358 begin
	select * from TBTSM_JOB where FDREGISTER_TIME >= '2017-11-24' order by FDREGISTER_TIME
	select * from TBTSM_JOB where FNTSMJOB_ID = 271928 --0
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID = 271928
end

if @case = 201711241434 begin
	exec SP_Q_INFO_BY_VIDEOID '000042P1' --G201798100270003, O, B -> T
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201798100270003'
end

if @case = 201711241504 begin --水果冰淇淋 2003 claire0123 調用失敗
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201711210103' --\\mamstream\MAMDFS\ptstv\HVideo\0000\0000060\G000006020330001.mxf --F
end

if @case = 201711241538 begin
	exec SP_Q_INFO_BY_VIDEOID '00003WOR' --2017-11-27
	exec SP_Q_INFO_BY_VIDEOID '00003VA8' --2017-11-27
end

if @case = 201711241741 begin --標記上傳卡住
	exec SP_Q_INFO_BY_VIDEOID '0000439F' --S, 4 PeoPo公民新聞報(2017週間版) G201755402250001
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'C' where FSFILE_NO = 'G201755402250001'
	exec SP_Q_INFO_BY_VIDEOID '0000439G' --FCSTATUS = '' --G201755402260001
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'C' where FSFILE_NO = 'G201755402260001'
end

if @case = 201711270929 begin --TSM RECALL FAIL
	select * from TBTSM_JOB where FDREGISTER_TIME >= '2017-11-26' and FNSTATUS = 5 order by FDREGISTER_TIME --13 FNSTATUS = 5
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID in (select FNTSMJOB_ID from TBTSM_JOB where FDREGISTER_TIME >= '2017-11-26' and FNSTATUS = 5) --13

	select * from TBTSM_JOB where FNTSMJOB_ID = 272168
end

if @case = 201711271056 begin
	exec SP_Q_INFO_BY_VIDEOID '00003WXE' --D, 2018062 11, O
	select * from TBPROG_M where FSPROG_ID = '2018062' --大聲MY客風單歌
	select * from TBLOG_VIDEO where FSID = '2018062' and FNEPISODE = 11 --G201806200110001, G201806200110002 0000402N
	exec SP_Q_INFO_BY_VIDEOID '0000402N'

	exec SP_Q_INFO_BY_VIDEOID '00003WX1' --2018062, 1 R 2017-11-29 G201806200010001, 大聲MY客風單歌 #1
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201806200010001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201806200010001'
end

if @case = 201711271757 begin
	select * from TBTSM_JOB where FNTSMJOB_ID in (272079, 272111, 272087, 272089, 272081, 272164) --執行plink得到錯誤的回傳內容：指令 = dsmrecall /hakkatv/HVideo/2009/2009299/G200929906900002.mxf, 參數 = 10.13.210.15 -l root -pw TSMP@ssw0rdTSM ". /root/.bash_profile";"dsmrecall /hakkatv/HVideo/2009/2009299/G200929906900002.mxf", 錯誤訊息 = FATAL ERROR: Server unexpectedly closed network connection
	update TBTSM_JOB set FNSTATUS = 1 where FNTSMJOB_ID in (272079, 272111, 272087, 272089, 272081, 272164)
end


if @case = 201711281454 begin --bobohung 調用失敗
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201711270084'
/*
\\mamstream\MAMDFS\ptstv\HVideo\2018\2018136\G201813600010001.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2018\2018158\G201815800010002.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2018\2018094\G201809400010005.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2018\2018137\G201813700010002.mxf
*/
end

if @case = 201711281607 begin --百變小露露 第二季#轉檔失敗
	select * from TBPROG_M where FSPGMNAME like '%百變小露露%' --2017838 百變小露露 第二季
	select * from TBLOG_VIDEO where FSID = '2017838' and FNEPISODE = 10 --hsingcheng G201783800100001, 000041BS, R
	exec SP_Q_INFO_BY_VIDEOID '000041BS' --2017-11-29, 12
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201783800100001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201783800100001'
end

if @case = 201711291157 begin  --調用失敗
	select * from TBBOOKING_DETAIL where FSFILE_NO = 'G201789000010005'
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201789000010005'
	select * from TBPROG_M where FSPROG_ID = '2017890' --黑道治國(主題之夜導讀版)
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201711030138' --賴慕寧, ninalai \\mamstream\MAMDFS\ptstv\HVideo\2017\2017890\G201789000010005.mxf
end

if @case = 201711291209 begin --a29082241
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201711270067'
/*
\\mamstream\MAMDFS\ptstv\HVideo\2007\2007817\G200781700010003.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2012\2012945\G201294500010018.mxf
*/
end

if @case = 201711291340 begin
	select * from TBTSM_JOB where FNTSMJOB_ID = 272579 --5
	update TBTSM_JOB set FNSTATUS = 0 where FNTSMJOB_ID = 272579
end

if @case = 201711291343 begin  --其他檔案入庫卡住
	select * from TBLOG_VIDEO where FSARC_TYPE = '026' and FCFILE_STATUS <> 'T' and FDCREATED_DATE > '2017-11-01' order by FDCREATED_DATE
	--與海豚的約定_EN_HD完成帶.MXF
	--村民大會第471集垃圾無處燒無字幕分軌帶1050306.MXF G200626504710002 -->delete
	select * from TBLOG_VIDEO where FSFILE_NO = 'G200626504710002' --B -> X
	--村民大會第472集深山裡的寶無字幕分軌帶1050313.MXF G200626504720002, 村民大會第473集苗栗雙慢城無字幕分軌帶1050320.MXF, G200626504730002

	--蜂狂2(英文版)HD完成檔.mxf S 檔案有在

	select * from TBLOG_VIDEO where FSARC_TYPE = '026' and FCFILE_STATUS = 'T' and FDCREATED_DATE > '2017-11-01' order by FDCREATED_DATE

	-- G200626504520002 村民大會第452集正視「老」問題無字幕分軌帶1041025.MXF
	-- G200626504620002 村民大會第462集大山背亮起來無字幕分軌帶1050103.MXF
	update TBLOG_VIDEO set FCFILE_STATUS = 'X' where FSFILE_NO in ('G200626504520002', 'G200626504620002')
end

if @case = 201711301033 begin
	select * from TBLOG_VIDEO where FSBRO_ID in ('201711290028', '201711290029')
	update TBLOG_VIDEO set FCFILE_STATUS = 'X' where FSBRO_ID in ('201711290028', '201711290029') --> 刪除
end

if @case = 201711301544 begin --標記上傳尚未排到，前面有大檔
	exec SP_Q_INFO_BY_VIDEOID '000043QN' --1130_PTS1_全球現場
end

if @case = 201711301645 begin --森林偵探隊, 13 重新排播審核 done
	exec SP_Q_INFO_BY_VIDEOID '00003UIP' --R 171640  森林偵探隊, 13, G201769900130001
	select * from TBTRANSCODE where FNTRANSCODE_ID = 171640 --4, ANSID = 120826, 2017-09-18 11:12:41.277
	select * from TBPROG_M where FSPROG_ID = '2017699' --森林偵探隊
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201769900130001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201769900130001'
end

if @case = 201711301817 begin
	select * from TBTSM_JOB where FNTSMJOB_ID in (272579, 272800, 272758, 272812) --5
	update TBTSM_JOB set FNSTATUS = 0 where FNTSMJOB_ID in (272579, 272800, 272758, 272812)
end

if @case = 201712011006 begin
	select * from TBTSM_JOB where FNTSMJOB_ID in (272579, 273000) --272579, 5
	--/hakkatv/HVideo/2015/2015581/G201558100170002.mxf
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201558100170002' --00002NP1
	exec SP_Q_INFO_BY_VIDEOID '00002NP1'
end

if @case = 201712011657 begin
	select top 100 * from VW_SEARCH_VIDEO
	select count(FSSYS_ID) from VW_SEARCH_VIDEO --62517
end

if @case = 201712051026 begin
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201711280041' --ninalai
--\\mamstream\MAMDFS\ptstv\HVideo\2018\2018050\G201805000010001.mxf
end

if @case = 201712051051 begin
	exec SP_Q_INFO_BY_VIDEOID '00004460'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'C' where FSFILE_NO = 'P201712000890001'
end

if @case = 201712051516 begin
	exec SP_Q_INFO_BY_VIDEOID '0000447T'
end

if @case = 201712051540 begin --在整集調用下"創業有事" 會查不到
	select * from TBPROG_M where FSPGMNAME like '%創業%' --創業，有事嗎? 2017053
end

if @case = 201712051552 begin --修改音軌
/*
請幫忙修改以下檔案的音軌資訊
將原本的LRM- 改為LRMM
感謝！

G000042700010001 越劇 紅樓夢 v.1
G000042700020001 越劇 紅樓夢 v.2
G000042700030001 越劇 紅樓夢 v.3
G000042800010001 越劇 梁山伯與祝英台 v.1
G000042800020001 越劇 梁山伯與祝英台 v.2
*/
	select * from TBLOG_VIDEO where FSFILE_NO in ('G000042700010001', 'G000042700020001', 'G000042700030001', 'G000042800010001', 'G000042800020001')
	select * from TBLOG_VIDEO_D where FSFILE_NO in ('G000042700010001', 'G000042700020001', 'G000042700030001', 'G000042800010001', 'G000042800020001')
	update TBLOG_VIDEO set FSTRACK = 'LRMM' where FSFILE_NO in ('G000042700010001', 'G000042700020001', 'G000042700030001', 'G000042800010001', 'G000042800020001')
	select * from TBLOG_VIDEO where FSFILE_NO = 'G000042700020001'
	select * from TBPROG_D where FSPROG_ID = '0000427' --FSCONTENT 敘述林黛玉與賈寶玉之間的愛情故事.越劇發源於浙江嵊縣一帶,它是從民間說唱藝術"小歌班"、"的篤班"發展起來的.經過七十多年的演變,現已成為中國地方戲曲的一大劇種.
end

if @case = 201712051611 begin --調用失敗
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201711290023' --a29082241 \\mamstream\MAMDFS\ptstv\HVideo\0000\0000060\G000006018640002.mxf
end

if @case = 201712051750 begin --資料不完全
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '0000446Y' --G201758102380001, 201712050043
	select * from TBBROADCAST where FSBRO_ID = '201712050043' --no record
	exec SP_D_TBLOG_VIDEO_BYFILENO 'G201758102380001'
end

if @case = 201712060958 begin
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201813000010001' --R, 2018130, 1 000041XI
	exec SP_Q_INFO_BY_VIDEOID '000041XI' --童言童語 175841
	select * from TBTRANSCODE where FNTRANSCODE_ID = 175841 --4 2017-11-08 18:47:58.787 ANSID = 124311
end

if @case = 201712061104 begin --TSM recall fail
	exec SP_Q_INFO_BY_VIDEOID '00002IEB' --O, T, 小王子第3季 #11 \\mamstream\MAMDFS\ptstv\HVideo\2016\2016849\G201684900110003.mxf
	select * from TBTSM_JOB where FNTSMJOB_ID in (273594, 273799) --5
	update TBTSM_JOB set FNSTATUS = 0 where FNTSMJOB_ID in (273594, 273799)
end

if @case = 201712061448 begin --手動搬檔
	exec SP_Q_INFO_BY_VIDEOID '0000443C' --S 2017-18 歐洲冠軍足球聯賽
end

if @case = 201712061545 begin
	exec SP_Q_INFO_BY_VIDEOID '00003X63' --FCSTATUS = '' G201133400070004
	exec SP_Q_INFO_BY_VIDEOID '00003X6M' --FCSTATUS = '' G201133400080005
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'C' where FSFILE_NO = 'G201133400070004'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'C' where FSFILE_NO = 'G201133400080005'
end

if @case = 201712071000 begin
	exec SP_Q_INFO_BY_VIDEOID '00003OFS' --O, T
	select * from TBTSM_JOB where FNTSMJOB_ID = 273952 --5 執行plink得到錯誤的回傳內容：指令 = dsmrecall /ptstv/HVideo/2017/20170600292/P201706002920001.mxf, 參數 = 10.13.210.15 -l root -pw TSMP@ssw0rdTSM ". /root/.bash_profile";"dsmrecall /ptstv/HVideo/2017/20170600292/P201706002920001.mxf", 錯誤訊息 = FATAL ERROR: Server unexpectedly closed network connection
	--dsmrecall /ptstv/HVideo/2017/20170600292/P201706002920001.mxf
end

if @case = 201712071037 begin
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201712060036' --\\mamstream\MAMDFS\ptstv\HVideo\2009\2009748\G200974800020025.mxf a29082241
end

if @case = 201712071136 begin
	exec SP_Q_INFO_BY_VIDEOID '00004444' --O, B 魔法DIY種子盆栽#11promo 宏觀台
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201712000440001'
	exec SP_Q_INFO_BY_VIDEOID '000043VP' --我是留台幫 201711300023 尚未標記上傳
end

if @case = 201712071436 begin
	exec SP_Q_INFO_BY_VIDEOID '00002SFQ' --R 多奇探險隊(原名:偵探狗多奇) --140924 G201422600230002
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201422600230002'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201422600230002'
	select * from TBTRANSCODE where FNTRANSCODE_ID = 140924 --2016-08-13 20:47:42.543

	select * from TBUSERS
end

if @case = 201712081619 begin --轉檔失敗 done
	select * from TBLOG_VIDEO where FSBRO_ID = '201706130074' --G201789500290001 R, 178161, 00003ORV
	select * from TBTRANSCODE where FNTRANSCODE_ID = 178161 --8, ANSID = -1
	exec SP_Q_INFO_BY_VIDEOID '00003ORV'
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201789500290001' --快樂新住民 #29
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201789500290001'
end

if @case = 201712081729 begin
	exec SP_Q_INFO_BY_VIDEOID '00003OO7'
end


if @case = 201712111437 begin --check in Tape 並無紀錄
	select * from TBTRACKINGLOG where FDTIME > '2017-12-11 14:00' and FSPROGRAM like '%CheckoutTapes%' order by FDTIME
	select * from TBTRACKINGLOG where FDTIME > '2017-12-11 14:00' and FSPROGRAM like '%CheckinTapes%' order by FDTIME
	select * from TBTRACKINGLOG where FDTIME > '2017-12-11 14:00'  order by FDTIME
end

if @case = 201712111518 begin
	select * from TBTSM_JOB where FNTSMJOB_ID in (274555, 274422) --274555=5
	update TBTSM_JOB set FNSTATUS = 0 where FNTSMJOB_ID = 274555
end

if @case = 201712111545 begin --刪除送帶轉檔單
	exec SP_Q_INFO_BY_VIDEOID '000044M6' --奧林P客 945, G200618609450002, 201712110012, B
	select * from TBBROADCAST where FSBRO_ID = '201712110012'
	select * from TBLOG_VIDEO where FSID = '2006186' and FNEPISODE = 945
	exec SP_D_TBLOG_VIDEO_BY_FSFILE_NO 'G200618609450002'
	exec SP_D_TBLOG_VIDEO_BYFILENO 'G200618609450002'
end

if @case = 201712111547 begin
	select * from TBLOG_VIDEO where FSID = '2018225' --蜂狂2(英文版)HD完成檔.mxf, S, 026
end

if @case = 201712120923 begin
	select * from TBPROG_M where FSPGMNAME like '%我們的島%' 
	--我們的島(短版) 2010248
	select * from TBLOG_VIDEO where FSID = '2010248' and FCFILE_STATUS = 'T'
	--我們的島950 0000072
	select * from TBLOG_VIDEO where FSID = '0000072' and FSARC_TYPE = '002' and FCFILE_STATUS = 'T'

	select * from TBPROG_M where FSPGMNAME like '%水果冰淇淋%'
	--0000060 水果冰淇淋
	select * from TBLOG_VIDEO where FSID = '0000060' and FSARC_TYPE = '002'

	select * from TBPROG_M where FSPGMNAME like '%我的這一班%' --0000216
	select * from TBLOG_VIDEO where FSID = '0000216' and FSARC_TYPE = '002'
end

if @case = 201712121138 begin
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201712070123' --bobohung
/*
\\mamstream\MAMDFS\ptstv\HVideo\2018\2018158\G201815800010002.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2018\2018208\G201820800010002.mxf
*/
end

if @case = 201712121412 begin
	exec SP_Q_INFO_BY_VIDEOID '00002ZTC' --水果冰淇淋-故事系列-芭芭拉樂團 2017-12-14
end

if @case = 201712121445 begin --QUE 表
	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20171000537'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20170900594'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME
end

if @case = 201712121605 begin
	select * from TBLOG_VIDEO where FSBRO_ID = '201712110123' --G201110502340001 X
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201110502340001' --X
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G201110502340001'
	select * from TBBROADCAST where FSBRO_ID = '201712110123'

	select * from TBLOG_VIDEO where FSBRO_ID in ('201712110122', '201712110120', '201712110119', '201712110114', 
	'201712110112', '201712110110', '201712110109', '201712110108', '201712110104', '201712110103', '201712110101', '201712110092') order by FSFILE_NO

	select * from TBLOG_VIDEO where FSID = '2011105' and FNEPISODE between 219 and 232
end

if @case = 201712131032 begin --處理當案 QC 後卡在 Review
	exec SP_Q_INFO_BY_VIDEOID '000044N0' --O, R, P201712002200001
	exec SP_Q_INFO_BY_VIDEOID '00004457' --O, R P201712000740001
	exec SP_Q_INFO_BY_VIDEOID '000043YY' --O, R P201712000060001
	exec SP_Q_INFO_BY_VIDEOID '000044N1' --O, R 2017 12/16 pts1 menu 10:00, P201712002210001
	exec SP_Q_INFO_BY_VIDEOID '00004459' --O, R 2016PTS1 RM 菜鳥記者94狂, P201712000750001
	exec SP_Q_INFO_BY_VIDEOID '0000445A' --O, R2016PTS1 RM【週末電影院】女孩壞壞, P201712000760001
	exec SP_Q_INFO_BY_VIDEOID '000044N4' --O, R P201712002230001
	exec SP_Q_INFO_BY_VIDEOID '000044NB' --O, S P201712002260001
	exec SP_Q_INFO_BY_VIDEOID '0000445C' --O, R P201712000770001
	exec SP_Q_INFO_BY_VIDEOID '000044NC' --O, R P201712002270001
	exec SP_Q_INFO_BY_VIDEOID '00003VMU' --紫色大稻埕(60分鐘版) G201808000290001
	exec SP_Q_INFO_BY_VIDEOID '000044N3' --P201712002220001 2017 12/16 pts1 menu 12:00

	select A.FSFILE_NO 當案編號, A.FSVIDEO_PROG 主控編號, A.FCFILE_STATUS, B.FSPROMO_NAME 節目名稱, C.FDDATE 播出日期, C.FSPLAYTIME 播出時間, C.FSCHANNEL_ID, D.FSSHOWNAME 頻道 
	from TBLOG_VIDEO A left join TBPGM_PROMO B on A.FSID = B.FSPROMO_ID 
	left join TBPGM_ARR_PROMO C on A.FSID = C.FSPROMO_ID
	left join TBZCHANNEL D on C.FSCHANNEL_ID = D.FSCHANNEL_ID
	  where A.FSVIDEO_PROG in ('000044N0', '00004457', '000043YY', '000044N1', '00004459', '0000445A', '000044N4', '000044NB', '0000445C', '000044NC', '00003VMU', '000044N3')
	  and C.FDDATE >= '2017-12-13'
	order by C.FDDATE, C.FSPLAYTIME

	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO in ('P201712002200001', 'P201712000740001', 'P201712000060001', 'P201712002220001',
	'P201712002210001', 'P201712000750001', 'P201712000760001', 'P201712002230001', 'P201712002260001', 'P201712000770001',  'P201712002270001', 'G201808000290001')

	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO in ('P201712002200001', 'P201712000740001', 'P201712000060001', 'P201712002220001',
	'P201712002210001', 'P201712000750001', 'P201712000760001', 'P201712002230001', 'P201712002260001', 'P201712000770001',  'P201712002270001', 'G201808000290001')

	exec SP_Q_TBTRACKINGLOG_BY_DATE FNTRANSCODE_ID, FNANYSTREAM_ID, FNTSM_BJOID, FSFILE_NO, FSVIDEO_ID,  BEGIN_TIME, END_TIME (default = NULL)
	exec SP_Q_TBTRACKINGLOG_BY_DATE '178325', '', '274771', 'P201712002220001', '0044N3', '2017-12-11', NULL
	select * from TBTRACKINGLOG where FDTIME between '2017-12-11 22:20' and '2017-12-11 22:50' order by FDTIME
end

if @case = 201712131043 begin --處理檔案無法 recall
	select * from TBTSM_JOB where FNTSMJOB_ID in (275132, 274774) --5
	update TBTSM_JOB set FNSTATUS = 0 where FNTSMJOB_ID in (275132, 274774)
end

if @case = 201712141404 begin --reschedule 
	exec SP_Q_INFO_BY_VIDEOID '000044DG' --R 178299 P201712001240001
	select * from TBTRANSCODE where FNTRANSCODE_ID = 178299 --4 ANSID 126432
end

if @case = 201712150941 begin
	exec SP_Q_INFO_BY_VIDEOID '00003QQT'
	select *  from TBTSM_JOB where FNTSMJOB_ID in (275414, 275366, 275143) --5 執行plink得到錯誤的回傳內容：指令 = dsmrecall /ptstv/HVideo/2017/20170700109/P201707001090001.mxf, 參數 = 10.13.210.15 -l root -pw TSMP@ssw0rdTSM ". /root/.bash_profile";"dsmrecall /ptstv/HVideo/2017/20170700109/P201707001090001.mxf", 錯誤訊息 = FATAL ERROR: Server unexpectedly closed network connection
	update TBTSM_JOB set FNSTATUS = 0 where FNTSMJOB_ID in (275414, 275366, 275143)
end

if @case = 201712150951 begin
	select * from TBPGM_PROMO where FSPROMO_ID = '20171109010'
	select * from TBPROG_M where FSPGMNAME like '%我在台灣%' --我在台灣 你好嗎 2013811
	select top 1000 * from TBPGM_PROMO_BOOKING
	select * from TBPGM_PROMO_SCHEDULED
	select * from TBPGM_PROMO where FSPROG_ID = '2013811' and FNEPISODE = 79 --20171100291, 20171100292

	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID in (
	'20171100836',
'20171100835',
'20171100834',
'20171100641',
'20171100457',
'20171100459',
'20171100455',
'20171100457',
'20171100457',
'20171100291',
'20171100292',
'201711090109',
'20171100001',
'20171100003',
'20171100006',
'20171000636',
'20171000637',
'20171000657',
'20171000415',
'20171000416',
'20171000417',
'20171000265',
'20171000266',
'20171000267',
'20171000126',
'20171000127',
'20171000128',
'20170900791',
'20170900792',
'20170900793',
'20170900460',
'20170900461',
'20170900462',
'20170900361',
'20170900362',
'20170900363',
'20170900164',
'20170900160',
'20170900074',
'20170900168',
'20170900166',
'20170900204')
	and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FSPROMO_ID, A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

	select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID in (
	'20171200188', '20171200187', '20171100730')
	and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FSPROMO_ID, A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME
end

if @case = 201712151018 begin --QC 後未轉檔
	exec SP_Q_INFO_BY_VIDEOID '000043DG' --B O 夢裡的一千道牆_2016PTS2 RM P201711006510001
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201711006510001'
end

if @case = 201712151539 begin
	exec SP_Q_INFO_BY_VIDEOID '000044EC' --PTS3 RM【巴塞爾VS曼徹斯特聯隊】2017-18 歐洲冠軍足球聯賽 P201712001340001 B. N, 2017-12-19 ch14
	exec SP_Q_INFO_BY_VIDEOID '000044DJ' --PTS3 重版出來 第8集 2017.12重播 P201712001260001 , B, N 2017-12-19 ch14
end


if @case = 201712151657-201711291343 begin  --其他檔案入庫卡住

	--蜂狂2(英文版)HD完成檔.mxf S 檔案有在
	select * from TBLOG_VIDEO where FSBRO_ID = '201711160103' --蜂狂2(英文版)HD完成檔.mxf S 2018225 G201822500010001 , T
	select * from TBPROG_M where FSPROG_ID = '2018225'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G201822500010001'
	
	--update TBLOG_VIDEO set FCFILE_STATUS = 'X' where FSFILE_NO in ('G200626504520002', 'G200626504620002')
	select * from TBLOG_VIDEO where FSARC_TYPE = '026' and FCFILE_STATUS <> 'T' and FDCREATED_DATE > '2017-11-01' order by FDCREATED_DATE --幕後有藝思_EP11如何成為演員_分軌帶.mxf G201779500110002, 201712130074
end

if @case = 201712180934 begin --標記上傳失敗，原始檔不見
	exec SP_Q_INFO_BY_VIDEOID '000044XN' --新雷鳥神機隊 第二季 201712140074
end

if @case = 201712181221 begin
	select * from TBTRACKINGLOG where FDTIME > '2017-12-18 12:15' order by FDTIME
end

if @case = 201712181423 begin --審核不通過 X, F
	exec SP_Q_INFO_BY_VIDEOID '000044ZD' --X, F 獨立特派員 524 2007217, 
	select * from TBLOG_VIDEO where FSID = '2007217' and FNEPISODE = 524
end

if @case = 2017121801427 begin --調用失敗
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201712180011' --a29082241
/*
\\mamstream\MAMDFS\ptstv\HVideo\2015\2015107\G201510700020011.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2015\2015107\G201510700040011.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2015\2015107\G201510700050011.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2015\2015107\G201510700060011.mxf
*/
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201712180012'
/*
\\mamstream\MAMDFS\ptstv\HVideo\2009\2009748\G200974800020025.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2009\2009748\G200974800040026.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2009\2009748\G200974800050026.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2016\2016834\G201683400020010.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2016\2016834\G201683400010010.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2016\2016834\G201683400050010.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2016\2016834\G201683400060010.mxf
*/
end

if @case = 201712181551 begin --調用失敗
	select * from TBLOG_VIDEO where FSFILE_NO in ('G201774400210011', 'G201774400240012')
	select * from TBUSERS where FSUSER_ChtName = '陳昭文' --chaowenchen
/*
\\mamstream\MAMDFS\ptstv\HVideo\2017\2017744\G201774400210011.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2017\2017744\G201774400240012.mxf
*/
end

if @case = 201712181559 begin --TSM recall 失敗
	exec SP_Q_INFO_BY_VIDEOID '00003YB3' --\\mamstream\MAMDFS\hakkatv\HVideo\2017\2017833\G201783300060001.mxf
	select * from TBTSM_JOB where FNTSMJOB_ID in (275143, 275715, 275778, 275842, 275975) --5
	update TBTSM_JOB set FNSTATUS = 0 where FNTSMJOB_ID in (275143, 275715, 275778, 275842, 275975)
end

if @case = 201712191003 begin --我們的島 入庫
	select * from TBLOG_VIDEO where FSID = '0000072' and FNEPISODE = 934 --G000007209340001, 00004545
	exec SP_Q_INFO_BY_VIDEOID '00004545' --N, B
end

if @case = 201712191014 begin
	exec SP_Q_INFO_BY_VIDEOID '000044KR' --藝術很有事（106年內容產製與運用計畫）18 , 2017-12-22, O, R, 178194
	select * from TBTRANSCODE where FNTRANSCODE_ID = 178194  -- 4, ANDID = 126337 --reschedule done
end

if @case = 201712191014 begin
	exec SP_Q_INFO_BY_VIDEOID '000044XN' --新雷鳥神機隊 第二季 16 標記上傳失敗 G201763700160002
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'C' where FSFILE_NO = 'G201763700160002'
end

if @case = 201712191731 begin
	exec SP_Q_INFO_BY_VIDEOID '000044SN' --標記標記上傳失敗 G201799300010003
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'C' where FSFILE_NO = 'G201799300010003'
end

if @case = 201712191749 begin
	select * from TBLOG_VIDEO where FSBRO_ID in ('201712190098', '201712190101', '201712190109')
end

if @case = 2017122010555 begin
	exec SP_Q_INFO_BY_VIDEOID '000043S1' --一字千金 110
	select * from TBTSM_JOB where FNTSMJOB_ID in (275143, 275778) --5
	update TBTSM_JOB set FNSTATUS = 0 where FNTSMJOB_ID in (275143, 275778)
	--/ptstv/HVideo/2015/2015404/G201540401100001.mxf access denied
	--/ptstv/HVideo/0000/0000216/G000021604810007.mxf access denied
end

if @case = 201712201120 begin --檢索檔案資訊空白
	select * from TBLOG_VIDEO where FSBRO_ID = '201712190077' --G000021800080001
end

if @case = 201712201526 begin
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201760500900002'
end

if @case = 201712221215 begin
	select * from TBLOG_DOC where FSFILE_NO = 'G201601900010003' --2016019
	select * from TBPROG_M where FSPROG_ID = '2016019' --人生的最後功課(主題之夜導讀版)
	select * from TBLOG_DOC where FSFILE_NO = 'G201645500010005' --2016455
	select * from TBPROG_M where FSPROG_ID = '2016455' --天問(2-2)

	select * from TBPROG_M where FSPROG_ID = '2016224' --藝數狂潮
	select * from TBLOG_DOC where FSID = '2016224' and FNEPISODE = 1 --G201622400010013 txt
end

if @case = 201712221216 begin --Tape offline : A00875L4, E01921L4, E02155L4
	exec SP_Q_INFO_BY_VIDEOID  '000043DM' --O, S 177236, P201711006570001, 薩丁尼亞島_2016PTS2 RM, 2017-12-26
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201711006570001'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'P201711006570001'
	
	select * from TBTRANSCODE where FNTRANSCODE_ID =177236 --2, ANSID = -1

	select * from TBLOG_AUDIO

end

if @case = 201712211026 begin
	exec SP_Q_INFO_BY_VIDEOID '000043DI' --O, B 罐頭與普普藝術_2016PTS2 RM P201711006530001
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'P201711006530001' 
	select * from TBZCHANNEL
end

if @case = 201712221739 begin
	exec SP_Q_INFO_BY_VIDEOID '00003WHQ'
end

if @case = 201712221705 begin
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201712220039' --ninalai \\mamstream\MAMDFS\ptstv\HVideo\2017\2017976\G201797600010001.mxf
end

if @case = 201712251334 begin
	select * from TBTSM_JOB where FNTSMJOB_ID in (276775, 276942) --5
	update TBTSM_JOB set FNSTATUS = 0 where FNTSMJOB_ID in (276775, 276942)
end

if @case = 201712251555 begin --調用磁帶已下架
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201712220084' --adm3036 R \\mamstream\MAMDFS\mvtv\HVideo\2006\2006850\G200685004210003.mxf tapeoffline 台灣心動線 421
end

if @case = 201712261445 begin --reschedule
	exec SP_Q_INFO_BY_VIDEOID '000045QU' --S, 179312
	select * from TBTRANSCODE where FNTRANSCODE_ID = 179312 --127376 4
end

if @case = 20172261512 begin
	select * from TBTSM_JOB where FNTSMJOB_ID = 277340 --5 /hakkatv/HVideo/2015/2015582/G201558200050002.mxf
	update TBTSM_JOB set FNSTATUS = 0 where FNTSMJOB_ID = 277340
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201558200050002' --00002UQZ
end

if @case = 201712271000 begin
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201712260058'
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201712260044'
	select * from TBBOOKING_DETAIL where FSBOOKING_NO = '201712260058'
end

if @case = 201712281004 begin
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201712260107'
/*
\\mamstream\MAMDFS\ptstv\HVideo\2018\2018208\G201820800010002.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2018\2018209\G201820900010001.mxf
\\mamstream\MAMDFS\ptstv\HVideo\2018\2018210\G201821000010006.mxf
*/
end

if @case = 201712281436 begin
	select * from TBLOG_VIDEO where FSBRO_ID = '201711030117' --000041RO, G201758102110002, 2017581, 211
	exec SP_Q_INFO_BY_VIDEOID '000041RO' --2017福氣來了 O, B
	select * from TBLOG_VIDEO where FSID = '2017581' and FNEPISODE = 211
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G201758102110002'
end

if @case = 201712281524 begin
	select * from TBLOG_VIDEO where FSARC_TYPE = '026' and FDCREATED_DATE > '2017-12-20' order by FDCREATED_DATE  --G201481301540002 暗香風華節目第154集 (FHD_MXF曾年有分軌帶).MXF
	select * from TBLOG_VIDEO where FSID = '2014813' and FNEPISODE = 154 --G201481301540001 000045YF
	exec SP_Q_INFO_BY_VIDEOID '000045YF'
	select * from TBLOG_VIDEO where FSID = '2014813' and FNEPISODE = 154
	exec SP_Q_INFO_BY_VIDEOID '0000457S'
	select * from TBLOG_VIDEO where FSID = '2014813' and FNEPISODE = 148 --G201481301480001 0000457S
end

if @case = 201712281559 begin --標記上傳後無紀錄，狀態未更新
	exec SP_Q_INFO_BY_VIDEOID '000045W3' --1228_PTS1_全球現場 --P201712006140001 201712270030
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'C' where FSFILE_NO = 'P201712006140001'
end

if @case = 201712281622 begin
	exec SP_Q_INFO_BY_VIDEOID '000043SU' --O, T 飆!企鵝里德(原名:我為你加油) 17
	select * from TBTSM_JOB where FNTSMJOB_ID = 277787 --5
	update TBTSM_JOB set FNSTATUS = 0 where FNTSMJOB_ID = 277787
end

if @case = 201712281721 begin
	exec SP_Q_INFO_BY_VIDEOID '00004547' --聽聽看 852 2017-12-30 ch 12
	select * from TBLOG_VIDEO where FSID = '0000159' and FNEPISODE = 852 --G000015908520003 0000462S
	exec SP_Q_INFO_BY_VIDEOID '0000462S' --2017-12-31 ch14
end

if @case = 201712291017 begin
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201712250024' --a29082241
/*
\\mamstream\MAMDFS\ptstv\HVideo\0000\0000060\G000006018710002.mxf
\\mamstream\MAMDFS\ptstv\HVideo\0000\0000060\G000006018730002.mxf
\\mamstream\MAMDFS\ptstv\HVideo\0000\0000060\G000006018760002.mxf
\\mamstream\MAMDFS\ptstv\HVideo\0000\0000060\G000006018790002.mxf
\\mamstream\MAMDFS\ptstv\HVideo\0000\0000060\G000006018800002.mxf
*/
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201712280062' --sineadchien
/*
\\mamstream\MAMDFS\ptstv\HVideo\2016\2016240\G201624000010017.mxf --tapeonly
\\mamstream\MAMDFS\ptstv\HVideo\2015\2015106\G201510600010001.mxf --tapeonly
*/
end

if @case = 201712291118 begin --回朔轉檔成功搬檔失敗
	select * from TBLOG_VIDEO where FSBRO_ID = '201712280096' --G000188700480001 S 179568
	select * from TBTRANSCODE where FNTRANSCODE_ID = 179568 --3 ANDIS = 127615 2017-12-28 14:49:32.517
	update TBLOG_VIDEO set FCFILE_STATUS = 'R' where FSFILE_NO = 'G000188700480001'
end

if @case = 201712291622 begin
	select * from TBTSM_JOB where FNTSMJOB_ID = 278028 --5
	update TBTSM_JOB set FNSTATUS = 0 where FNTSMJOB_ID = 278028
end

/************************************************************************
 *****************************  Reference section ***********************
 ************************************************************************/

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
if @case = 0 begin
	--波西測試
	select * from TBLOG_VIDEO where FSID = '2013232' and FCFILE_STATUS in ('B', 'T') order by FNEPISODE --4 無法建
	select * from TBLOG_VIDEO where FSID = '2013232' and FCFILE_STATUS not in ('B', 'T') order by FNEPISODE
	select * from TBLOG_VIDEO where FSID = '2013232' order by FNEPISODE 
	--第 4集無法新增第 2 筆

	--check transcode
	select '--' + CAST(FNTRANSCODE_ID as varchar(20)) + ', ' + CAST(FNANYSTREAM_ID as varchar(20)) + ', ' + CAST(FNPROCESS_STATUS as varchar(3)) + ', '
	+ FSFILE_NO + ', ' + FSVIDEO_PROG + ', ' + FSBRO_ID + ', ' + FSARC_TYPE + ', ' + FCFILE_STATUS + ', ' + CAST(ISNULL(FNTSMJOB_ID, 0) as varchar(20)) as ITEM, 
	'exec SP_Q_INFO_BY_VIDEOID ' + '''' + FSVIDEO_PROG + '''' as CMD1,
	'update TBLOG_VIDEO set FCFILE_STATUS = ''R'' where FSFILE_NO = ' + ''''+ FSFILE_NO + '''' as CMD2,
	* from VW_TBTRANSCODE where CAST(FDSTART_TIME as DATE) between '2017-03-1' and CAST(GETDATE() -1 as DATE)  and FCFILE_STATUS in ('R', 'S') order by FNTRANSCODE_ID
	 
	--註記轉檔單為刪除
	update TBLOG_VIDEO set FCFILE_STATUS = 'X' where FSFILE_NO = 'G201772400020003'
	update TBLOG_VIDEO_D set FCFILE_STATUS = 'X' where FSFILE_NO = 'G201772400020003'

	-- 刪除轉檔單及段落資料
	exec SP_D_TBLOG_VIDEO_BYFILENO 'G201770700020001'

	--每日入庫量統計
	select
	CAST(FDUPDATED_DATE as DATE) 日期,
       SUM(CASE
                        WHEN FSFILE_SIZE like '%GB%' THEN CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2))
                        WHEN FSFILE_SIZE like '%MB%' THEN CONVERT(FLOAT, SUBSTRING(FSFILE_SIZE, 1, LEN(FSFILE_SIZE) - 2))/1000
            END
       ) '入庫量(GB)'
     from TBLOG_VIDEO where FCFILE_STATUS = 'T' and FSTYPE = 'G' and FSFILE_SIZE <> ''  and FDUPDATED_DATE between '2017-01-01' and GETDATE()
     group by CAST(FDUPDATED_DATE as DATE)
	 order by CAST(FDUPDATED_DATE as DATE)

	--重審
	update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = 'G200626205210002'
	update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = 'G200626205210002'

	--查詢 user 調用失敗紀錄
	exec SP_Q_GET_BOOKING_FILE_BY_USERID '51342', 'F'

	--查詢 user 調用紀錄（根據調用單查）只查「影」其他格式查不到
	exec SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO '201705120007'

	--查詢 TBTRACKINGLOG SP_Q_TBTRACKINGLOG_BY_DATE FNTRANSCODE_ID, FNANYSTREAM_ID, FNTSM_BJOID, FSFILE_NO, FSVIDEO_ID,  BEGIN_TIME, END_TIME (default = NULL)
	exec SP_Q_TBTRACKINGLOG_BY_DATE '157018', '108031', '219244', 'G201594800010011', '', '2017-03-10', NULL

	--查詢公益託播
	exec SP_Q_WELFAREPROMO_PLAYOUT_BY_DATE '07', '2017-01-01', '2017-04-30', '1'  --播出紀錄
    exec SP_Q_WELFAREPROMO_PLAY_COUNT_BY_DATE '07', '2017-01-01', '2017-04-30'  --播出次數

	--about index
	select top 1000 * from sys.dm_db_index_physical_stats(db_id('MAM'), default, default, default, default)
	select db_id('MAM') --5
	select db_name(5) --MAM

	SELECT  DB_NAME(ips.database_id) DBName, OBJECT_NAME(ips.object_id) ObjName, i.name InxName, ips.avg_fragmentation_in_percent 
	FROM sys.dm_db_index_physical_stats(db_id('MAM'), default, default, default, default) ips 
	INNER JOIN sys.indexes i ON ips.index_id = i.index_id AND ips.object_id = i.object_id 
	WHERE 
     ips.object_id > 99 AND 
     ips.avg_fragmentation_in_percent >= 10 AND 
     ips.index_id > 0


	--我們的島 950
	select FSPROG_ID 節目編號, FNEPISODE 集數, FSPGDNAME 子集名稱, FSCONTENT 節目內容, FNLENGTH 長度, FNTAPELENGTH 實際帶長, FSPROGSRCID 節目來源代碼, FSPROGATTRID 內容屬性代碼, 
		FSPROGAUDID 目標觀眾代碼, FSPROGTYPEID 表現方式代碼, FSPROGGRADEID 節目分級代碼, FSPROGLANGID1 主聲道代碼, FSPROGLANGID2 副聲道代碼, FSPROGSALEID 行銷類別代碼, FSPROGBUYID 外購類別代碼, FSPROGBUYDID 外購類別細項代碼, FCRACE 可排檔,
		FSSHOOTSPEC 拍攝規格代碼, FSPRDYEAR 完成年度, FSPROGGRADE 非正常分級的說明, FSMEMO 備註, FNSHOWEPISODE 播出集別, FSCHANNEL_ID 頻道別代碼, FSDEL 是否已刪除, FSDELUSER 刪除者,
		FSNDMEMO '2ND備註', FSPROGSPEC 節目規格, 
		FSCR_TYPE 著作類型, FSCR_NOTE 著作權備註, FSCRTUSER 建檔人, FDCRTDATE 建檔日期,FSUPDUSER 修改人, FDUPDDATE 修改日期, _FDTRANDATE, FSCREATED_BY_PGM, FSUPDATED_BY_PGM, FDEXPIRE_DATE 到期日期, FSEXPIRE_DATE_ACTION 到期後刪除數位檔,
		FSPROGNATIONID 來源國家
	from TBPROG_D where FSPROG_ID = '0000072' order by FNEPISODE

	select * from TBPROG_D where FSPROG_ID = '0000072' order by FNEPISODE

	select FSPROG_ID 節目編號, FNEPISODE 集數, FSPGDNAME 子集名稱, FSCONTENT 節目內容, FNLENGTH 長度, FNTAPELENGTH 實際帶長, A.FSPROGSRCID 節目來源代碼, K.FSPROGSRCNAME 節目來源, 
	    A.FSPROGATTRID 節目型態代碼, J.FSPROGATTRNAME 節目型態, 
		A.FSPROGAUDID 目標觀眾代碼, I.FSPROGAUDNAME 目標觀眾, A.FSPROGTYPEID 表現方式代碼, H.FSPROGTYPENAME 表現方式, A.FSPROGGRADEID 節目分級代碼, D.FSPROGGRADENAME 節目分級,
		A.FSPROGLANGID1 主聲道代碼, F.FSPROGLANGNAME 主聲道, A.FSPROGLANGID2 副聲道代碼, G.FSPROGLANGNAME 副聲道, 
		FSPROGSALEID 行銷類別代碼, A.FSPROGBUYID 外購類別代碼, E.FSPROGBUYNAME 外購類別, A.FSPROGBUYDID 外購類別細項代碼, M.FSPROGBUYDNAME 外購類別細項, FCRACE 可排檔,
		A.FSSHOOTSPEC 拍攝規格代碼, L.FSSHOOTSPECNAME 拍攝規格, FSPRDYEAR 完成年度, FSPROGGRADE 非正常分級的說明, FSMEMO 備註, FNSHOWEPISODE 播出集別, FSCHANNEL_ID 頻道別代碼, FSDEL 是否已刪除, FSDELUSER 刪除者,
		FSNDMEMO '2ND備註', A.FSPROGSPEC 節目規格代碼, C.FSPROGSPECNAME 節目規格, 
		FSCR_TYPE 著作類型, FSCR_NOTE 著作權備註, FSCRTUSER 建檔人, FDCRTDATE 建檔日期,FSUPDUSER 修改人, FDUPDDATE 修改日期, _FDTRANDATE, FSCREATED_BY_PGM, FSUPDATED_BY_PGM, FDEXPIRE_DATE 到期日期, FSEXPIRE_DATE_ACTION 到期後刪除數位檔,
		A.FSPROGNATIONID 來源國家代碼, B.FSNATION_NAME 來源國家
	from TBPROG_D A left join TBZPROGNATION B on A.FSPROGNATIONID = B.FSNATION_ID
		left join TBZPROGSPEC C on A.FSPROGSPEC = C.FSPROGSPECID
		left join TBZPROGGRADE D on A.FSPROGGRADEID = D.FSPROGGRADEID
		left join TBZPROGBUY E on A.FSPROGBUYID = E.FSPROGBUYID
		left join TBZPROGLANG F on A.FSPROGLANGID1 = F.FSPROGLANGID
		left join TBZPROGLANG G on A.FSPROGLANGID2 = G.FSPROGLANGID
		left join TBZPROGTYPE H on A.FSPROGTYPEID = H.FSPROGTYPEID
		left join TBZPROGAUD I on A.FSPROGAUDID = I.FSPROGAUDID
		left join TBZPROGATTR J on A.FSPROGATTRID = J.FSPROGATTRID
		left join TBZPROGSRC K on A.FSPROGSRCID = K.FSPROGSRCID
		left join TBZSHOOTSPEC L on A.FSSHOOTSPEC = L.FSSHOOTSPECID
		left join TBZPROGBUYD M on A.FSPROGBUYDID = M.FSPROGBUYDID
     where A.FSPROG_ID = '0000072'
	 order by A.FNEPISODE

	 select FSPROG_ID + '|' 節目編號, CAST(FNEPISODE as varchar(10)) + '|' 集數, FSPGDNAME + '|' 子集名稱, FSCONTENT + '|' 節目內容, CAST(FNLENGTH as varchar(20)) + '|' 長度, CAST(FNTAPELENGTH as varchar(20)) + '|' 實際帶長, A.FSPROGSRCID + '|' 節目來源代碼, K.FSPROGSRCNAME + '|' 節目來源, 
	    A.FSPROGATTRID + '|' 節目型態代碼, J.FSPROGATTRNAME + '|' 節目型態, 
		A.FSPROGAUDID + '|'  目標觀眾代碼, I.FSPROGAUDNAME + '|'  目標觀眾, A.FSPROGTYPEID + '|'  表現方式代碼, H.FSPROGTYPENAME + '|'  表現方式, A.FSPROGGRADEID + '|'  節目分級代碼, D.FSPROGGRADENAME + '|'  節目分級,
		A.FSPROGLANGID1 + '|'  主聲道代碼, F.FSPROGLANGNAME + '|'  主聲道, A.FSPROGLANGID2 + '|'  副聲道代碼, G.FSPROGLANGNAME + '|'  副聲道, 
		A.FSPROGSALEID + '|'  行銷類別代碼, A.FSPROGBUYID + '|'  外購類別代碼, E.FSPROGBUYNAME + '|'  外購類別, A.FSPROGBUYDID + '|'  外購類別細項代碼, M.FSPROGBUYDNAME + '|'  外購類別細項, FCRACE + '|'  可排檔,
		A.FSSHOOTSPEC + '|'  拍攝規格代碼, L.FSSHOOTSPECNAME + '|'  拍攝規格, FSPRDYEAR + '|'  完成年度, FSPROGGRADE + '|'  非正常分級的說明, FSMEMO + '|'  備註, CAST(FNSHOWEPISODE as varchar(10)) + '|'  播出集別, A.FSCHANNEL_ID + '|'  頻道別代碼, O.FSCHANNEL_NAME 頻道別, FSDEL + '|'  是否已刪除, FSDELUSER + '|'  刪除者,
		FSNDMEMO + '|'  '2ND備註', A.FSPROGSPEC + '|'  節目規格代碼, C.FSPROGSPECNAME + '|'  節目規格,
		FSCR_TYPE + '|'  著作類型, FSCR_NOTE + '|'  著作權備註, A.FSCRTUSER + '|'  建檔人工號, N.FSUSER_ChtName + '|'  建檔人, CAST(FDCRTDATE as varchar(20)) + '|'  建檔日期, FSUPDUSER + '|'  修改人, CAST(FDUPDDATE as varchar(20)) + '|'  修改日期, CAST(_FDTRANDATE as varchar(20)) + '|',  FSCREATED_BY_PGM + '|' ,FSUPDATED_BY_PGM + '|' , CAST(FDEXPIRE_DATE as varchar(20)) + '|'  到期日期, FSEXPIRE_DATE_ACTION + '|'  到期後刪除數位檔,
		A.FSPROGNATIONID + '|'  來源國家代碼, B.FSNATION_NAME + '|'  來源國家
	from TBPROG_D A left join TBZPROGNATION B on A.FSPROGNATIONID = B.FSNATION_ID
		left join TBZPROGSPEC C on A.FSPROGSPEC = C.FSPROGSPECID
		left join TBZPROGGRADE D on A.FSPROGGRADEID = D.FSPROGGRADEID
		left join TBZPROGBUY E on A.FSPROGBUYID = E.FSPROGBUYID
		left join TBZPROGLANG F on A.FSPROGLANGID1 = F.FSPROGLANGID
		left join TBZPROGLANG G on A.FSPROGLANGID2 = G.FSPROGLANGID
		left join TBZPROGTYPE H on A.FSPROGTYPEID = H.FSPROGTYPEID
		left join TBZPROGAUD I on A.FSPROGAUDID = I.FSPROGAUDID
		left join TBZPROGATTR J on A.FSPROGATTRID = J.FSPROGATTRID
		left join TBZPROGSRC K on A.FSPROGSRCID = K.FSPROGSRCID
		left join TBZSHOOTSPEC L on A.FSSHOOTSPEC = L.FSSHOOTSPECID
		left join TBZPROGBUYD M on A.FSPROGBUYDID = M.FSPROGBUYDID
		left join TBUSERS N on A.FSCRTUSER = N.FSUSER_ID
		left join TBZCHANNEL O on A.FSCHANNEL_ID = O.FSCHANNEL_ID
     where A.FSPROG_ID = '0000072'
	 order by A.FNEPISODE

	 select * from TBUSERS



	if @subcase = 201706291625 begin --SP_Q_TBPGM_PROG_PROMO_LINK 查詢節目預排短帶
		declare @FNSEQNO int = -1
		declare @FSPROG_ID char(7) = '2013232'
		declare @CHANNEL_ID char(2) = '15'

		SELECT A.FSPROG_ID,A.FNSEQNO,A.FSPROMO_ID,A.FNNO,B.FSPROMO_NAME,B.FSDURATION,B.FSMEMO,A.FSCHANNEL_ID
		from dbo.TBPGM_PROG_PROMO_LINK A,TBPGM_PROMO B
		where A.FSPROMO_ID=B.FSPROMO_ID and 
		A.FSPROG_ID=@FSPROG_ID
		and @FNSEQNO = case when @FNSEQNO = '' then @FNSEQNO else FNSEQNO end   --如果沒有播映序號就不用比
		and FSCHANNEL_ID =@CHANNEL_ID
		order by FNNO

		select * from TBPGM_PROG_PROMO_LINK where FSPROG_ID = '2013232'

		--頻道預排
		select * from TBPGM_CHANNEL_PROMO_LINK where FSCHANNEL_ID = '07'
		select * from TBPGM_CHANNEL_PROMO_LINK where FSCHANNEL_ID = '13' --6
		select * from TBPGM_CHANNEL_PROMO_LINK where FSCHANNEL_ID = '14' --6
		/*
		FCLINK_TYPE : A 第一段節前，B ？每一段節後，C 最後一段節後
		FCTIME_TYPE 時間類型 : 1 單點, 2 雙點, 3 每三小時, A 全部
		FCINSERT_TYPE 插入類型 : C 循環,  A 全部 
		*/


		SELECT A.FSPROMO_ID,A.FSCHANNEL_ID,A.FNNO,A.FCLINK_TYPE,A.FCTIME_TYPE,
			A.FCINSERT_TYPE,B.FSPROMO_NAME,C.FSCHANNEL_NAME,B.FSDURATION
			from dbo.TBPGM_CHANNEL_PROMO_LINK A,TBPGM_PROMO B,TBZCHANNEL C
			where A.FSPROMO_ID=B.FSPROMO_ID and A.FSCHANNEL_ID=C.FSCHANNEL_ID 
			and @FSCHANNEL_ID = case when @FSCHANNEL_ID = '' then @FSCHANNEL_ID else A.FSCHANNEL_ID end
			and @FCLINK_TYPE = case when @FCLINK_TYPE = '' then @FCLINK_TYPE else A.FCLINK_TYPE end
			and @FCTIME_TYPE = case when @FCTIME_TYPE = '' then @FCTIME_TYPE else A.FCTIME_TYPE end
			and @FCINSERT_TYPE = case when @FCINSERT_TYPE = '' then @FCINSERT_TYPE else A.FCINSERT_TYPE end
			order by A.FNNO --<< 頻道預排有排序
	end

	if @subcase = 201707241438 begin --未登打製作人節目
		Select B.FSDEP as 製作部門名稱 ,A.FSPROG_ID as 節目編號,A.FSPGMNAME as 節目名稱 from MAM.dbo.TBPROG_M A inner join MAM.dbo.TBUSER_DEP B on A.FNDEP_ID = B.FNDEP_ID 
		where A.FSPROG_ID not in (Select distinct FSID  from MAM.dbo.TBPROG_PRODUCER) order by A.FNDEP_ID
	end


	if @case = 201708281449 begin --統計 TSM recall 的數量
	select * from TBTSM_JOB where CAST(FDREGISTER_TIME as DATE) = '2017-05-21' order by FDREGISTER_TIME --84
	select * from TBTSM_JOB where CAST(FDREGISTER_TIME as DATE) = '2017-06-29' order by FDREGISTER_TIME --219
	--/ptstv/HVideo/0000/0000216/G000021604290009.mxf
	select * from TBLOG_VIDEO where FSFILE_NO = 'G000021604290009'

	select fileNo, videoId,
	CASE
		WHEN fileSize like '%GB%' THEN CONVERT(FLOAT, SUBSTRING(fileSize, 1, LEN(fileSize) - 2))
		WHEN fileSize like '%MB%' THEN CONVERT(FLOAT, SUBSTRING(fileSize, 1, LEN(fileSize) - 2))/1000
	END as fsize,
	fdTime
	from FT_GET_VIDEOID_FROM_TSMJOB('2017-06-28 08:25', '2017-06-29 08:25')

	select
	SUM(
		CASE
			WHEN fileSize like '%GB%' THEN CONVERT(FLOAT, SUBSTRING(fileSize, 1, LEN(fileSize) - 2))
			WHEN fileSize like '%MB%' THEN CONVERT(FLOAT, SUBSTRING(fileSize, 1, LEN(fileSize) - 2))/1000
		END
	)
	from FT_GET_VIDEOID_FROM_TSMJOB('2017-06-07 08:25', '2017-06-08 08:25')


	select
	CAST(fdTime as DATE) as 'Retrieve Date',
	ROUND(
	SUM(
		CASE
			WHEN fileSize like '%GB%' THEN CONVERT(FLOAT, SUBSTRING(fileSize, 1, LEN(fileSize) - 2))
			WHEN fileSize like '%MB%' THEN CONVERT(FLOAT, SUBSTRING(fileSize, 1, LEN(fileSize) - 2))/1000
		END
	), 2) as 'File Size(GB)'	
	from FT_GET_VIDEOID_FROM_TSMJOB('2017-01-01', '2017-07-31')
	Group by CAST(fdTime as DATE)
	order by CAST(fdTime as DATE)

	select
	ROUND(
	AVG(
		CASE
			WHEN fileSize like '%GB%' THEN CONVERT(FLOAT, SUBSTRING(fileSize, 1, LEN(fileSize) - 2))
			WHEN fileSize like '%MB%' THEN CONVERT(FLOAT, SUBSTRING(fileSize, 1, LEN(fileSize) - 2))/1000
		END
	), 2) as 'File Size(GB)'	
	from FT_GET_VIDEOID_FROM_TSMJOB('2017-01-01', '2017-07-31')
end


if @case = 201704071747-201605311403 begin -- 新增測試頻道 PTS_TEST
        select * from TBZCHANNEL
        select COLUMN_NAME, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH, * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'TBZCHANNEL'
        -- 新增頻道
        declare @FSCHANNEL_ID varchar(2) = '15'
        declare @FSCHANNEL_NAME nvarchar(100) = 'PTS_TEST'
        declare @FSSHOWNAME nvarchar(20) = 'PTS_TEST'
        declare @FSCHANNEL_TYPE varchar(3) = '002'
        declare @FSSORT varchar(2) = '12'
        declare @FSSHORTNAME varchar(10) = ''
        declare @FSOLDTABLENAME varchar(20) = 'PSHOW_DIMO'
        declare @FSTSM_POOLNAME varchar(20) = '/ptstv'
        declare @FNDIR_ID bigint = 1
        declare @FSCREATED_BY varchar(50) = '51204'
        declare @FDCREATED_DATE datetime = (select CURRENT_TIMESTAMP)
        declare @FSUPDATED_BY varchar(50) = '51204'
        declare @FDUPDATED_DATE datetime = (select CURRENT_TIMESTAMP)
        declare @FBISENABLE bit = 1


        insert into TBZCHANNEL(FSCHANNEL_ID, FSCHANNEL_NAME, FSSHOWNAME, FSCHANNEL_TYPE, FSSORT, FSTSM_POOLNAME, FNDIR_ID, FSCREATED_BY, 
        FDCREATED_DATE, FSUPDATED_BY, FDUPDATED_DATE, FBISENABLE, FSSHORTNAME, FSOLDTABLENAME)
        values(@FSCHANNEL_ID, @FSCHANNEL_NAME, @FSSHOWNAME, @FSCHANNEL_TYPE, @FSSORT, @FSTSM_POOLNAME, @FNDIR_ID, @FSCREATED_BY, @FDCREATED_DATE, @FSUPDATED_BY, @FDUPDATED_DATE, @FBISENABLE,
        @FSSHORTNAME, @FSOLDTABLENAME)

        update TBZCHANNEL set FSSHORTNAME = '', FSOLDTABLENAME = 'PSHOW_DIMO' where FSCHANNEL_ID = '13'
        update TBZCHANNEL set FSSHORTNAME = '', FSOLDTABLENAME = 'PSHOW_PTS' where FSCHANNEL_ID = '12'
		update TBZCHANNEL set FSSHOWNAME = 'PTS_TEST' where FSCHANNEL_ID = '15'

		select * from TBZCHANNEL

        declare @d datetime = (select CURRENT_TIMESTAMP)
        select @d

        --exec SP_I_TBZCHANNEL @FSCHANNEL_ID, @FSCHANNEL_NAME, @FSSHOWNAME, @FSSORT, @FSSHORTNAME, @FSOLDTABLENAME, @FSCREATED_BY, @FSUPDATED_BY

        select * from TBDIRECTORY_GROUP
        select * from TBGROUPS --功能群組
        select * from TBMODULE_GROUP
        select * from TBMODULE_DETAIL where FSMODULE_ID = 'S100001'-- 模組列表
        select * from TBMODULE_CAT  --查詢群組權限列表，模組的分類

        select * from TBMODULE_GROUP where FSMODULE_ID = 'P101002'
        select g.FSGROUP_ID, g.FSGROUP, g.FSDESCRIPTION, m.FSMODULE_ID, m.FSFUNC_NAME, m.FSHANDLER, m.FSDESCRIPTION
        from TBMODULE_GROUP mg join TBGROUPS g on mg.FSGROUP_ID = g.FSGROUP_ID join TBMODULE_DETAIL m on mg.FSMODULE_ID = m.FSMODULE_ID
        order by g.FSGROUP_ID

        select g.FSGROUP_ID, g.FSGROUP, g.FSDESCRIPTION, m.FSMODULE_ID, m.FSFUNC_NAME, m.FSHANDLER, m.FSDESCRIPTION
        from TBMODULE_GROUP mg join TBGROUPS g on mg.FSGROUP_ID = g.FSGROUP_ID join TBMODULE_DETAIL m on mg.FSMODULE_ID = m.FSMODULE_ID
        where FSGROUP = '排表群組'
        order by g.FSGROUP_ID

        select g.FSGROUP_ID, g.FSGROUP, g.FSDESCRIPTION, m.FSMODULE_ID, m.FSFUNC_NAME, m.FSHANDLER, m.FSDESCRIPTION
        from TBMODULE_GROUP mg join TBGROUPS g on mg.FSGROUP_ID = g.FSGROUP_ID join TBMODULE_DETAIL m on mg.FSMODULE_ID = m.FSMODULE_ID
        where m.FSMODULE_ID = 'P101001'
        order by g.FSGROUP_ID -- P101001 = 匯入公視節目表

        -- 增加模組模組
		--匯入節目表
        select * from TBMODULE_DETAIL where FSFUNC_NAME like '%匯入%' --FSMODULE_ID = P1 + ch + 001
        insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P115001', '匯入PTS_TEST節目表', '', 'treeViewPGMQUEUE')

		select * from TBMODULE_DETAIL where FSFUNC_NAME like '%節目表維護%' --FSMODULE_ID = P1 + ch + 002
        insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P115002', 'PTS_TEST節目表維護', '', 'treeViewPGMQUEUE')
        update TBMODULE_DETAIL set FSDESCRIPTION = '匯入PTS_TEST節目表', FSFUNC_NAME = '匯入PTS_TEST節目表' where FSMODULE_ID = 'P115002'

        select * from TBMODULE_CAT where FSMODULE_CAT_NAME like '%預排%' --頻道預排 = P2
        select * from TBMODULE_DETAIL where FSFUNC_NAME like '%預排%' --FSMODULE_ID = P2 + ch + 001 | 002
        insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P215001', 'PTS_TEST頻道預排設定', '', 'treeViewPromoChannelLink')
        insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P215002', 'PTS_TEST節目預排設定', '', 'treeViewPromoProglLink')

        select * from TBMODULE_CAT where FSMODULE_CAT_NAME like '%播出%' --播出運行表 = P5
        select * from TBMODULE_DETAIL where FSFUNC_NAME like '%播出%'  
		--FSMODULE_ID = P5 + ch + 001(播出運行表維護) | 002（查詢播出運行表）| 003(插入宣傳帶) | 006(轉出 LST) | 007（列印播出運行表）| 008(匯入 LOUTH KEY)
        select * from TBMODULE_DETAIL where FSMODULE_ID like 'P513%'
        insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P515001', 'PTS_TEST播出運行表維護', '', NULL)
        insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P515002', 'PTS_TEST查詢播出運行表', '', NULL)
        insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P515003', 'PTS_TEST插入宣傳帶', '', 'treeViewInsertPromo')
        insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P515006', 'PTS_TEST轉出LST', '', NULL)
        insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P515007', 'PTS_TEST列印播出運行表', '', NULL)
        insert TBMODULE_DETAIL(FSMODULE_ID, FSFUNC_NAME, FSDESCRIPTION, FSHANDLER) values('P515008', 'PTS_TEST匯入Louth Key', '', 'treeViewLouthKey')

        select * from TBMODULE_DETAIL where FSMODULE_ID like '%62%'
        select * from TBMODULE_DETAIL where FSMODULE_ID like '%61%'
end

    select A.FSPROMO_ID 短帶編號, B.FSPROMO_NAME 短帶名稱, A.FDDATE 播出日期, C.FSSHOWNAME 播出頻道, A.FSPLAYTIME 播出時間  from TBPGM_ARR_PROMO A join TBPGM_PROMO B on A.FSPROMO_ID = B.FSPROMO_ID
    join TBZCHANNEL C on A.FSCHANNEL_ID = C.FSCHANNEL_ID where A.FSPROMO_ID = '20170100295'  and A.FSCHANNEL_ID in ('01', '02', '07','11', '12', '13', '14') order by A.FDDATE, A.FSCHANNEL_ID, A.FSPLAYTIME

-- SP_Q_TBTRACKINGLOG_BY_DATE FNTRANSCODE_ID, FNANYSTREAM_ID, FNTSM_BJOID, FSFILE_NO, FSVIDEO_ID,  BEGIN_TIME, END_TIME (default = NULL)
