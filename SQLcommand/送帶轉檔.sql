
CREATE PROC #TEST_CASE
	@case integer, @subcase integer, 
	@FSPRO_ID varchar(20),		--節目編號
	@FNEPISODE integer,			--子集
	@FSBRO_ID varchar(15),		--送帶轉檔單
	@FSFILE_NO varchar(16),		--檔案編號
	@FSTRACK nvarchar(4)			--音軌
AS

if @case=1 BEGIN --basic query
	if @subcase = 1 --查詢轉檔狀態代碼
		select * from TBZFILE_STATUS
	else if @subcase = 2
		select t.*, s.FSFILE_STATUS_NAME from TBLOG_VIDEO t, TBZFILE_STATUS s where t.FSFILE_NO='P201411000070001' and t.FCFILE_STATUS=s.FSFILE_STATUS_ID
	else if @subcase = 3 --查詢影片類型
		select * from TBFILE_TYPE
	else if @subcase = 4 --頻道別代碼
		select * from TBZCHANNEL
	else if @subcase = 5 -- 查詢送帶轉檔單狀態代碼
		select * from TBZBOOKING_CHECK_STATUS
END
	
else if @case = 2 BEGIN --查詢水果冰淇淋有哪些未填送帶轉檔單

	-- 將所有子集寫進array
	declare @arr_fnepisode_all TABLE (v_fnepisode integer)
	insert into @arr_fnepisode_all
	select fnepisode from  TBPROG_D where FSPROG_ID='0000060'
	--select * from @arr_fnepisode_all

	-- 將有填送帶轉檔單TC 的集數，狀態不為R(失敗）F(駁回）X(刪除)寫進 array, 0000060 水果冰淇淋
	declare @arr_fnepisode TABLE (v_fnepisode integer)
	insert into @arr_fnepisode
	--select fnepisode from TBLOG_VIDEO_D where FSId='0000060'  -- 有轉檔成功的
	select fnepisode from TBLOG_VIDEO where FSId='0000060' and FCFILE_STATUS not in ('X', 'F', 'R') AND FSARC_TYPE = '002' --找出HD播出帶尚未失敗或刪除或駁回的單
	--select fnepisode from TBLOG_VIDEO where FSId='0000060' AND FSARC_TYPE = '002' --找出尚未填過HD的
	--select fnepisode from TBLOG_VIDEO where FSId='0000060' and (FCFILE_STATUS='X' or FCFILE_STATUS='F' or FCFILE_STATUS='R')
	--select * from @arr_fnepisode
	--select * from TBLOG_VIDEO where FSID='0000060' and FNEPISODE in (select * from @arr_fnepisode) AND FCFILE_STATUS not in ('X', 'F', 'R') AND FSARC_TYPE='002'
	select v_fnepisode from @arr_fnepisode_all where v_fnepisode not in (select * from @arr_fnepisode)
	--select * from TBBROADCAST where FSID='0000060' and FNEPISODE in (select * from @arr_fnepisode_all where v_fnepisode not in (select * from @arr_fnepisode)) -- 驗證

	/*
	select fnepisode from TBLOG_VIDEO where FSId='0000060'
	select fnepisode from TBLOG_VIDEO_D where FSId='0000060' and fnepisode=1547
	select fnepisode from TBLOG_VIDEO where FSId='0000060' and fnepisode=1547
	select * from TBLOG_VIDEO where FSId='0000060' and fnepisode=1547
	*/
	--select * from @arr_fnepisode

	if @subcase = 2 begin -- 查詢藝起看公視短帶轉檔單資訊，有那些未填轉檔單
		--查詢藝起看公視短帶 id
		declare @arrPromoID TABLE (v_promoId varchar(20))
		declare @arrTranscode TABLE (v_promoId varchar(20), v_promoName varchar(200), v_broadcastId varchar(20))
		declare @arrTranscodeSD TABLE (v_promoId varchar(20), v_promoName varchar(200), v_broadcastId varchar(20))
		declare @arrTranscodeHD TABLE (v_promoId varchar(20), v_promoName varchar(200), v_broadcastId varchar(20))

		-- 列出有效的短帶
		insert @arrPromoID select FSPROMO_ID from TBPGM_PROMO where FSPROMO_NAME like '%藝起看公視%' and FSDEL = 'N'

		--select * from @arrPromoID
		declare iCur CURSOR for select FSPROMO_ID from TBPGM_PROMO where FSPROMO_NAME like '%藝起看公視%' and FSDEL = 'N'

		declare @promoId varchar(20)
		declare @count int
		set @count = 1
		open iCur
		fetch next from iCur into @promoId
		while (@@FETCH_STATUS = 0) BEGIN

			--insert @arrTranscode select FSPROMO_ID, FSPROMO_NAME, '' from TBPGM_PROMO where FSPROMO_ID = @promoId
			--insert into @arrTranscode select FSPROMO_ID, FSPROMO_NAME, '' from TBPGM_PROMO where FSPROMO_ID = @promoId
			
			if (exists(select FSBRO_ID from TBLOG_VIDEO where FSId = @promoId and FCFILE_STATUS not in ('X', 'F', 'R') AND FSARC_TYPE = '001')) --找出SD播出帶尚未失敗或刪除或駁回的單
				insert @arrTranscodeSD select FSPROMO_ID, FSPROMO_NAME, '' from TBPGM_PROMO where FSPROMO_ID = @promoID 

			if (exists(select FSBRO_ID from TBLOG_VIDEO where FSId = @promoId and FCFILE_STATUS not in ('X', 'F', 'R') AND FSARC_TYPE = '002')) --找出HD播出帶尚未失敗或刪除或駁回的單
				insert @arrTranscodeHD select FSPROMO_ID, FSPROMO_NAME, '' from TBPGM_PROMO where FSPROMO_ID = @promoID 
			
		    if (exists(select FSBRO_ID from TBLOG_VIDEO where FSId = @promoId and FCFILE_STATUS not in ('X', 'F', 'R') AND (FSARC_TYPE = '001' or FSARC_TYPE = '002'))) --找出HD播出帶尚未失敗或刪除或駁回的單
				insert @arrTranscode select FSPROMO_ID, FSPROMO_NAME, '' from TBPGM_PROMO where FSPROMO_ID = @promoID 
			
			fetch next from iCur into @promoId
		END

		select * from @arrTranscodeSD
		select * from @arrTranscodeHD

		select v_promoId, p.FSPROMO_NAME from @arrPromoID, TBPGM_PROMO p where v_promoId not in (select v_promoId from @arrTranscode) and p.FSPROMO_ID = v_promoId

		select * from TBPGM_PROMO where FSPROMO_ID = '20120100060'
		if (exists(select FSBRO_ID from TBLOG_VIDEO where FSId = '20120100060' and FCFILE_STATUS not in ('X', 'F', 'R') AND FSARC_TYPE = '002'))
			select 'TRUE'

		select * from TBBROADCAST where FSID = '20151200549'
		-- 未填轉檔單
		select p.FSPROMO_ID, p.FSPROMO_NAME, t.FSBRO_ID from TBPGM_PROMO p left join TBBROADCAST t on p.FSPROMO_ID = t.FSID and p.FSDEL = 'N'
		select p.FSPROMO_ID, p.FSPROMO_NAME, t.FSBRO_ID from TBPGM_PROMO p left join TBBROADCAST t on p.FSPROMO_ID = t.FSID where t.FSBRO_ID IS NULL and FSPROMO_NAME like '%藝起看公視%' and p.FSDEL = 'N'
		select * from TBLOG_VIDEO where FSID = '20151200549'
		select * from TBPGM_PROMO where FSPROMO_ID = '20120100060'
	end
END


ELSE IF @case = 3 BEGIN -- 查詢送帶轉檔單（第一層)
	if @subcase = 1 BEGIN
		select * from TBBROADCAST where FSBRO_ID=@FSPRO_ID --根據轉檔單號查詢送帶轉檔單
		select * from TBBROADCAST where FSBRO_ID = '201508270096'
		select * from TBBROADCAST where FSBRO_ID = '201511200043'
		select distinct(FSMEMO) from TBBROADCAST -- 備註
		select distinct(FCFROM) from TBLOG_VIDEO
		select tb.FSBRO_ID, tb.FSID, tb.FCCHANGE, tb.FCCHECK_STATUS, v.FSFILE_NO, v.FCFILE_STATUS, v.FSCHANGE_FILE_NO, v.FDCREATED_DATE  from TBBROADCAST tb inner join TBLOG_VIDEO v on tb.FSBRO_ID = v.FSBRO_ID
		select tb.FSBRO_ID, tb.FSID, tb.FCCHANGE, tb.FCCHECK_STATUS, v.FSFILE_NO, v.FCFILE_STATUS, v.FSCHANGE_FILE_NO, v.FCFROM from TBBROADCAST tb inner join TBLOG_VIDEO v on tb.FSBRO_ID = v.FSBRO_ID
		where tb.FCCHANGE = 'N' and tb.FSBRO_TYPE = 'P' and v.FCFILE_STATUS = 'B' order by v.FDCREATED_DATE desc --非置換的待轉檔

		select tb.FSBRO_ID, tb.FSID, tb.FCCHANGE, tb.FCCHECK_STATUS, v.FSFILE_NO, v.FCFILE_STATUS, v.FSCHANGE_FILE_NO, v.FCFROM from TBBROADCAST tb inner join TBLOG_VIDEO v on tb.FSBRO_ID = v.FSBRO_ID
		where tb.FCCHANGE = 'N' and tb.FSBRO_TYPE = 'P' and v.FCFILE_STATUS = 'T' order by v.FDCREATED_DATE desc --短帶影帶擷取

		-- 查詢短帶第一次送檔轉檔且是待轉檔狀態
		select tb.FSBRO_ID, tb.FSID, tb.FCCHANGE, tb.FCCHECK_STATUS, v.FSFILE_NO, v.FCFILE_STATUS, v.FSCHANGE_FILE_NO, tb.FSMEMO from TBBROADCAST tb inner join TBLOG_VIDEO v on tb.FSBRO_ID = v.FSBRO_ID
		where tb.FCCHANGE = 'N' and tb.FSBRO_TYPE = 'P' and v.FCFILE_STATUS = 'B'  and tb.FSMEMO  like '%數位檔案%' order by v.FDCREATED_DATE desc

		-- 查詢短帶置換轉檔且是待轉檔狀態
		select tb.FSBRO_ID, tb.FSID, tb.FCCHANGE, tb.FCCHECK_STATUS, v.FSFILE_NO, v.FCFILE_STATUS, v.FSCHANGE_FILE_NO, tb.FSMEMO from TBBROADCAST tb inner join TBLOG_VIDEO v on tb.FSBRO_ID = v.FSBRO_ID
		where tb.FCCHANGE = 'Y' and tb.FSBRO_TYPE = 'P' and v.FCFILE_STATUS = 'B'  order by v.FDCREATED_DATE desc

		select * from TBLOG_VIDEO where FSBRO_ID = '201511200079'
		select * from TBLOG_VIDEO where FSBRO_ID = '201602220013'
		select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201600700200001'

		select * from TBBROADCAST where FSCREATED_BY = '51191'
		
	END
	else if @subcase = 2
		select * from TBBROADCAST where FSBRO_ID  like '' + @FSPRO_ID + '%' -- 根據轉檔單號查詢送帶轉檔單
	else if @subcase = 3 BEGIN
		select * from TBBROADCAST where FSID='0000060' -- 根據節目id 查詢送帶轉檔單 0000060 水果冰淇淋、0000001 波西小園丁
		select * from TBBROADCAST where FSID='20150800273'
		select * from TBBROADCAST where FSID='2016280'
		select * from TBBROADCAST where FSBRO_ID = '201601290001'  -- 轉檔單無段落資訊
		select * from TBBROADCAST where FDCREATED_DATE between '2016-01-29 16:30' and '2016-01-29 16:45' --查詢同一批短帶轉檔單的列印編號
	END
	else if @subcase = 4
		select FNEPISODE, COUNT(FSBRO_ID) from TBBROADCAST where FSID='0000060' GROUP BY FNEPISODE -- 根據節目編號查尋每一集各有多少筆送帶轉檔單
	else if @subcase = 5
		select * from TBBROADCAST where FSID='2016657' and FNEPISODE=3
	else if @subcase = 6 BEGIN --查詢有填置換申請
		 select p.FSPGMNAME, t.* from TBBROADCAST t inner join TBPROG_M p on t.FSID = p.FSPROG_ID where t.FSBRO_TYPE = 'G' and t.FCCHANGE = 'Y' order by FDCREATED_DATE desc
		 select p.FSPROMO_NAME, t.* from TBBROADCAST t inner join TBPGM_PROMO p on t.FSID = p.FSPROMO_ID where t.FSBRO_TYPE = 'P' and t.FCCHANGE = 'Y' order by FDCREATED_DATE desc
	END
	else if @subcase = 5 BEGIN
		select p.FSPROMO_NAME, t.* from TBBROADCAST t inner join TBPGM_PROMO p on t.FSID = p.FSPROMO_ID where t.FSCREATED_BY = '71183' and t.FDBRO_DATE between '2015-01-01' and '2015-12-08' and t.FSBRO_TYPE = 'P' order by FDBRO_DATE
		select * from TBBROADCAST t where t.FSCREATED_BY = '71183' and t.FDBRO_DATE between '2015-01-01' and '2015-12-08'
		select * from TBPGM_PROMO where FSPROMO_ID = '20160200095'
		select * from TBBROADCAST where FSID = '20150300570'
	END
END --CASE3

ELSE IF @case = 4 BEGIN -- 查詢轉檔影片
	if @subcase = 1 BEGIN--根據轉檔單查詢轉檔影片 (TBLOG_VIDEO)
		select * from TBLOG_VIDEO where FSBRO_ID = @FSBRO_ID order by FDCREATED_DATE
		select * from TBLOG_VIDEO where FSBRO_ID = '201512180004' order by FDCREATED_DATE
		select * from TBLOG_VIDEO where FCFILE_STATUS = 'B' and FSTYPE = 'P' and FSCHANGE_FILE_NO = '' order by FDCREATED_DATE desc  -- 非置換的短帶（待轉檔）
		select * from TBLOG_VIDEO where FSBRO_ID = '201601290001' order by FDCREATED_DATE --原本看不到轉檔段落，過一會又出現
		select * from TBLOG_VIDEO_D where FSFILE_NO = 'G200678600300007' order by FDCREATED_DATE
		select * from TBLOG_VIDEO_D where FSFILE_NO = 'P201602000950001' order by FDCREATED_DATE


	END
	else if @subcase = 2 BEGIN --根據節目編號、集數及轉檔單查詢 TBLOG_VIDEO, TBLOG_VIDEO_D
		DECLARE @tmp TABLE( FSFILE_NO varchar(20) )
		CREATE TABLE #tmpFSILE_NO( FSFILE_NO varchar(20) )
		select * from TBLOG_VIDEO where FSID = @FSPRO_ID and FNEPISODE = @FNEPISODE and (@FSBRO_ID = '' or FSBRO_ID = @FSBRO_ID) order by FDCREATED_DATE --根據節目編號、集數和轉檔單查詢
		select * into #tmpFSFILE_NO from TBLOG_VIDEO where FSID = @FSPRO_ID and FNEPISODE = @FNEPISODE and (@FSBRO_ID = '' or FSBRO_ID = @FSBRO_ID) --暫存檔案編號
		select * from TBLOG_VIDEO_D where FSFILE_NO in (select FSFILE_NO from #tmpFSFILE_NO) --根據檔案編號查詢有轉檔成功的資料
		--select * from TBLOG_VIDEO_D where FSSUBJECT_ID = @FSPRO_ID and FNEPISODE = @FNEPISODE
		select * from TBLOG_VIDEO where FSID = '2013811' and FNEPISODE = 67
	END
	else if @subcase = 3 BEGIN -- 根據檔案編號查詢轉檔單影片
		select * from TBLOG_VIDEO where FSFILE_NO= @FSFILE_NO
		select pgm.FSPGMNAME, t.* from TBLOG_VIDEO t, TBPROG_M pgm where t.FSID = pgm.FSPROG_ID and t.FSFILE_NO = 'G200798700040002'
		select pgm.FSPGMNAME, t.* from TBLOG_VIDEO t, TBPROG_M pgm where t.FSID = pgm.FSPROG_ID and t.FSFILE_NO = 'G201510200390001' 
	END

	else if @subcase = 4 begin --根據videoid查詢
		select * from TBLOG_VIDEO as t, TBPROG_M as p where t.FSID = p.FSPROG_ID and FSVIDEO_PROG = '00001H8S'
		select  t.FSVIDEO_PROG, t.FSFILE_NO, p.FSPGMNAME, t.FNEPISODE, t.FSARC_TYPE, t.FCFILE_STATUS,t.FDCREATED_DATE, t.FDUPDATED_DATE from TBLOG_VIDEO as t, TBPROG_M as p 
		where t.FSID = p.FSPROG_ID and FSVIDEO_PROG = '00002FGV'
		select * from TBLOG_VIDEO as t, TBPROG_M as p where t.FSID = p.FSPROG_ID and FSVIDEO_PROG = '000JX6'
		exec SP_Q_TBLOG_VIDEO_SEG_DURATION_BY_VIDEOID '00000JX601'  --Video Id 是有帶段落的
		exec SP_Q_TBLOG_VIDEO_SEG_DURATION_BY_VIDEOID '000020FM01'
		select * from TBLOG_VIDEO where FSVIDEO_PROG = '000023AG'
		select * from TBLOG_VIDEO where FSID = 
		select * from TBLOG_VIDEO_D where FSVIDEO_PROG = '000023AG'
		select * from TBBROADCAST where FSBRO_ID = '201603080091'
		select * from TBLOG_VIDEO where FSVIDEO_PROG = '00001H8S'

	end
	else if @subcase = 5 BEGIN
		select * from TBLOG_VIDEO where FSID = '20150800273'
		select * from TBLOG_VIDEO_D where FSID = '20150800273'
		select * from TBLOG_VIDEO where FSID = '2014580'
		select * from TBLOG_VIDEO_D where FSID = '2014580'
		
	END
	else if @subcase = 6 BEGIN  --根據節目 id 查詢轉檔檔案及段落
		select * from TBLOG_VIDEO_D where FSID = '2007987' and FNEPISODE = 4
		select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G200798700040002'
		select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G201549600300001'
		select seg.FSFILE_NO, seg.FNSEG_ID, seg.FSVIDEO_ID, seg.FSBEG_TIMECODE, seg.FSEND_TIMECODE, pgm.FSPGMNAME, t.* from TBLOG_VIDEO t, TBPROG_M pgm, TBLOG_VIDEO_SEG seg
		where t.FSID = pgm.FSPROG_ID and t.FSFILE_NO = seg.FSFILE_NO and t.FSFILE_NO = 'G200798700040002'
	END
	else if @subcase = 7 BEGIN -- 查詢置換相關資料
		select * from TBLOG_VIDEO where FCFILE_STATUS = 'B' and FSTYPE = 'P' and FSCHANGE_FILE_NO != '' order by FDCREATED_DATE desc  -- 待置換的短帶
		select * from TBLOG_VIDEO where FCFILE_STATUS = 'B' and FSTYPE = 'G' order by FDCREATED_DATE desc
	END
END --CASE4

else if @case = 5 BEGIN --音軌
	if @subcase = 1 BEGIN -- 修改音軌
		Update TBLOG_VIDEO Set FSTRACK = 'MMMM'
		where FSFILE_NO = @FSFILE_NO and FSTRACK= @FSTRACK

		UPDATE TBLOG_VIDEO_D
		set TBLOG_VIDEO_D.FSTITLE = TBLOG_VIDEO_D.FSTITLE
		WHERE TBLOG_VIDEO_D.FSFILE_NO = @FSFILE_NO
	END
	else if @subcase = 2 BEGIN -- 修改音軌
		EXEC SP_U_TBLOG_VIDEO_FSTRACK @FSFILE_NO, @FSTRACK, '51204'
		EXEC SP_U_TBLOG_VIDEO_FSTRACK 'G201549600300001', 'LRLR', '51204'
		select * from TBLOG_VIDEO where FSFILE_NO = @FSFILE_NO
		-- 修改百變小露露 27~30 音軌為 LRLR
	    select pgm.FSPGMNAME, t.* from TBLOG_VIDEO t, TBPROG_M pgm where t.FSID = pgm.FSPROG_ID and t.FSFILE_NO = 'G201549600300001'
		select pgm.FSPGMNAME, t.* from TBLOG_VIDEO t, TBPROG_M pgm where t.FSID = pgm.FSPROG_ID and t.FSFILE_NO = 'G201549600290001'
		select pgm.FSPGMNAME, t.* from TBLOG_VIDEO t, TBPROG_M pgm where t.FSID = pgm.FSPROG_ID and t.FSFILE_NO = 'G201549600280001'
		select pgm.FSPGMNAME, t.* from TBLOG_VIDEO t, TBPROG_M pgm where t.FSID = pgm.FSPROG_ID and t.FSFILE_NO = 'G201549600270001'
	END

END --CASE5

else if @case = 6 BEGIN -- 全文檢索一直搜尋不到新檔案只看到舊檔案
	-- 1. 檢查該檔案編號的TBLOG_VIDEO_D.FCFILE_STATUS為『T』或『Y』，若否，非索引問題，可能檔案未轉檔完成或其他原因
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'P201508007860001'
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'P201412009040002'   -- https://groups.google.com/forum/?hl=zh-TW#!topic/pts-mam/S_6XB6XD4mk
	-- 2. 檢查TBLOG_VIDEO_DIFFERENT_2011(2011~2015年落在此TABLE)無該筆檔案。若有此筆資料代表incremental index尚未執行，非索引問題
	select * from TBLOG_VIDEO_DIFFERENT_2
	select * from TBLOG_VIDEO_DIFFERENT
	-- 以上兩點確認後，全文檢索仍查無該筆資料，便執行重建索引(FULL INDEX)如附件步驟

END -- case 6

else if @case = 7 BEGIN --[USER] 送帶轉檔：Anystream 轉檔完成, 轉檔中心仍顯示轉檔中 https://groups.google.com/forum/?hl=zh-TW#!topic/pts-mam/eGhGJqpw26Y
	select * from TBBROADCAST where FSBRO_ID = '201508270096'
	select s.FSFILE_STATUS_NAME, tb.* from TBLOG_VIDEO tb, TBZFILE_STATUS s where FSBRO_ID = '201508270096' and tb.FCFILE_STATUS = s.FSFILE_STATUS_ID
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'P201508007860001'  --no record
	select * from TBZFILE_STATUS

END -- case 7

else if @case = 8 BEGIN -- SP_U_TBLOG_VIDEO_STT_OK   Anystream 轉檔完後更新轉檔狀態
	declare @file_no char(16) = '', @change_file_no char(16) = ''
	select @change_file_no = ISNULL(FSCHANGE_FILE_NO, '') from TBLOG_VIDEO where FSJOB_ID = '104114'
	select @file_no = ISNULL(FSFILE_NO, '') from TBLOG_VIDEO where FSJOB_ID = '104114'
	--select * from TBLOG_VIDEO where FSJOB_ID = '104114'
	print 'file no = ' + @file_no + '  change file no = ' + @change_file_no
	-- exec sp_readerrorlog  --無相關資訊
END --case 8

else if @case = 9 BEGIN --查詢HD節目有哪些未填送帶轉檔單，存成文字檔 tab 分隔

	-- 將所有節目規格為 HD 子集寫進，且未刪除的 array
	declare @arr_fnepisode_all_hd TABLE (v_fsprog_id varchar(10), v_fsprogname nvarchar(50), v_fnepisode integer, v_dept varchar(246))
	insert into @arr_fnepisode_all_hd
	select d.FSPROG_ID, m.FSPGMNAME, d.FNEPISODE , dep.FULLName
	from  TBPROG_D d inner join TBPROG_M m on d.FSPROG_ID = m.FSPROG_ID inner join tblTBSDept dep on m.FNDEP_ID = dep.DeptID
	where (CHARINDEX('02', d.FSPROGSPEC) > 0) and d.FSDEL != 'Y'--and FSPROG_ID = '0000060'

	--select * from @arr_fnepisode_all_hd order by v_fsprog_id

	-- 將有填送帶轉檔單TC 的集數，狀態不為R(失敗）F(駁回）X(刪除)寫進 array
	declare @arr_fnepisode_hd TABLE (v_fsprog_id varchar(11), v_fnepisode integer, v_fsbro_id varchar(15))
	 --找出送帶轉檔單不是抽單(X)或不通過(F) 且轉檔單中的HD播出帶尚未失敗(X)或刪除(D)或取消 (F)的單  (含節目目和短帶)
	insert into @arr_fnepisode_hd
	select tb.FSID, tb.FNEPISODE, tb.FSBRO_ID from TBLOG_VIDEO tb, TBBROADCAST bro where tb.FSBRO_ID = bro.FSBRO_ID and (bro.FCCHECK_STATUS not in ('X', 'F'))
	and (tb.FCFILE_STATUS not in ('X', 'F', 'D')) AND tb.FSARC_TYPE = '002'
	--select FSID, FNEPISODE from TBLOG_VIDEO where FCFILE_STATUS not in ('X', 'F', 'R') AND FSARC_TYPE = '002' --找出HD播出帶尚未失敗或刪除或駁回的單(會查到已抽單的轉檔單)
	--select fnepisode from TBLOG_VIDEO_D where FSId='0000060'  -- 有轉檔成功的
	--select fnepisode from TBLOG_VIDEO where FSId='0000060' AND FSARC_TYPE = '002' --找出尚未填過HD的
	--select fnepisode from TBLOG_VIDEO where FSId='0000060' and (FCFILE_STATUS='X' or FCFILE_STATUS='F' or FCFILE_STATUS='R')
	--select * from @arr_fnepisode_hd order by v_fsprog_id, v_fnepisode
	--select * from TBLOG_VIDEO where FSID='0000060' and FNEPISODE in (select * from @arr_fnepisode) AND FCFILE_STATUS not in ('X', 'F', 'R') AND FSARC_TYPE='002'
	--select v_fsprog_id, v_fnepisode from @arr_fnepisode_all_hd where v_fnepisode not in (select * from @arr_fnepisode_hd)

	declare CSR CURSOR for
	select * from @arr_fnepisode_all_hd

	declare @arr_no_hd TABLE(v_fsprog_id varchar(11), v_fsprogname nvarchar(50), v_fnepisode integer, deptName varchar(246))
	declare @fsprog_id varchar(11), @episode integer, @t_fsbro_id varchar(15), @pgm_name nvarchar(50), @deptName varchar(246)
	open CSR
	FETCH NEXT FROM CSR INTO @fsprog_id, @pgm_name, @episode, @deptName

	while (@@FETCH_STATUS = 0)
	BEGIN
		print 'Check ' + @fsprog_id+ '#' + CAST(@episode as varchar(10))
		if (not EXISTS(select * from @arr_fnepisode_hd where v_fsprog_id = @fsprog_id and v_fnepisode = @episode))
			insert @arr_no_hd VALUES (@fsprog_id, @pgm_name, @episode, @deptName)
	/*
		else BEGIN
			set @t_fsbro_id = (select v_fsbro_id from @arr_fnepisode_hd where v_fsprog_id = @fsprog_id and v_fnepisode = @episode)
			print 'Found:' + @t_fsbro_id
		END
	*/
		FETCH NEXT FROM CSR INTO @fsprog_id, @pgm_name, @episode, @deptName
	END -- while

	CLOSE CSR

	--select v_fsprog_id 節目編號, v_fsprogname 節目名稱, v_fnepisode 集數, deptName 製作單位 from @arr_no_hd
	select v_fsprog_id 節目編號, v_fsprogname 節目名稱, v_fnepisode 集數, deptName 製作單位 from @arr_no_hd group by v_fsprog_id, v_fsprogname, v_fnepisode, deptName order by v_fsprog_id

	--select * from TBBROADCAST where FSID='0000060' and FNEPISODE in (select * from @arr_fnepisode_all where v_fnepisode not in (select * from @arr_fnepisode)) -- 驗證

	
	select fnepisode from TBLOG_VIDEO where FSId='0000060'
	select fnepisode from TBLOG_VIDEO_D where FSId='0000060' and fnepisode=1547
	select fnepisode from TBLOG_VIDEO where FSId='0000060' and fnepisode=1547
	select * from TBLOG_VIDEO where FSId='0000072' and fnepisode=600
	
	select * from TBLOG_VIDEO where FSId='0000060' and fnepisode=1547
	select * from TBLOG_VIDEO where FSID = '0000060' and FNEPISODE = 1531
	select * from TBLOG_VIDEO_D where FSID = '0000060' and FNEPISODE = 1531 --查詢有轉檔過的狀態
	select * from TBPROG_M where FSPGMNAME like '%44屆六堆%'
	select * from TBLOG_VIDEO where FSID = '0000001' and FNEPISODE = 1 and FCFILE_STATUS not in ('X', 'F', 'R') and FSARC_TYPE = '002'
	select * from TBLOG_VIDEO where FSBRO_ID = '201210090031'
	select dep.*, m.FNDEP_ID, m.* from TBPROG_M m, tblTBSDept dep where m.FNDEP_ID = dep.DeptID and m.FSPROG_ID = '2007740' order by dep.FULLName
	select DeptId, DeptName from tblTBSDept group by DeptID, DeptName-- 有重複資料


	--B:待轉檔 D:刪除 F:取消（送帶轉檔單不通過）R:轉檔失敗 S:轉檔中 T:轉檔完成 X:刪除 Y:入庫（停用）
	select FCFILE_STATUS, count(*) number from TBLOG_VIDEO group by FCFILE_STATUS
	select FCFILE_STATUS, FSBRO_ID from TBLOG_VIDEO where FCFILE_STATUS = 'B' order by FSBRO_ID
	select FCFILE_STATUS, FSBRO_ID from TBLOG_VIDEO where FCFILE_STATUS = 'D' order by FSBRO_ID
	select FCFILE_STATUS, FSBRO_ID from TBLOG_VIDEO where FCFILE_STATUS = 'F' order by FSBRO_ID
	select FCFILE_STATUS, FSBRO_ID from TBLOG_VIDEO where FCFILE_STATUS = 'R' order by FSBRO_ID
	select FCFILE_STATUS, FSBRO_ID from TBLOG_VIDEO where FCFILE_STATUS = 'S' order by FSBRO_ID
	select FCFILE_STATUS, FSBRO_ID from TBLOG_VIDEO where FCFILE_STATUS = 'T' order by FSBRO_ID
	select FCFILE_STATUS, FSBRO_ID from TBLOG_VIDEO where FCFILE_STATUS = 'X' order by FSBRO_ID
	select FCFILE_STATUS, FSBRO_ID from TBLOG_VIDEO where FCFILE_STATUS = 'Y' order by FSBRO_ID
	select * from TBLOG_VIDEO where FSBRO_ID = '201602220013'
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '0000ASQB'
	select * from TBLOG_VIDEO where FCFILE_STATUS = 'X' --查詢轉檔影片狀態為刪除的轉檔單，如果一張轉檔單有2個轉檔片，刪除其中一個是否會變成抽單
	select * from TBBROADCAST where FSBRO_ID = '201603090028' -- FCCHECK_STATUS = 'X'


	-- 轉檔單狀態 X:抽單 C:審核 N:審核 T:回朔 F:不通過 Y:通過 G:簽收
	select FCCHECK_STATUS, count(*) from TBBROADCAST group by FCCHECK_STATUS
	select * from TBBROADCAST where FSBRO_ID in ('201412240072', '201412290058', '201301080036', '201210260052')
	select * from TBBROADCAST where FCCHECK_STATUS = 'X'
	select * from TBBROADCAST where FCCHECK_STATUS = 'C'
	select * from TBBROADCAST where FCCHECK_STATUS = 'N'
	select * from TBBROADCAST where FCCHECK_STATUS = 'T'
	select * from TBBROADCAST where FCCHECK_STATUS = 'F'
	select * from TBBROADCAST where FCCHECK_STATUS = 'Y'
	select * from TBBROADCAST where FCCHECK_STATUS = 'G'

	-- new for HD flow
	-- N:未上傳 E:抽單 U:標記上傳 R:退單
	select * from TBBROADCAST where FCCHECK_STATUS = 'K'
	select * from TBBROADCAST where FCCHECK_STATUS = 'J'
	select * from TBBROADCAST where FCCHECK_STATUS = 'U'
	select * from TBBROADCAST where FCCHECK_STATUS = 'R'


END --case 9

else if @case = 10 BEGIN  -- 只有查節目部分，資料帶不會查 B-待審核 T-轉檔完成 D-被置換
	if @subcase = 1 BEGIN
		select (tbv.FSID + pgm.FSPGMNAME) as 節目名稱, tbv.FNEPISODE as 集別, (tbv.FSARC_TYPE + typ.FSTYPE_NAME) as 帶別, (tbv.FCFILE_STATUS + z.FSFILE_STATUS_NAME) 狀態,tbv.* 
			from TBLOG_VIDEO tbv, TBFILE_TYPE typ, TBPROG_M pgm, TBZFILE_STATUS z
			where tbv.FSARC_TYPE = typ.FSARC_TYPE and tbv.FSID = pgm.FSPROG_ID and tbv.FCFILE_STATUS = z.FSFILE_STATUS_ID
			and (tbv.FCFILE_STATUS = 'T' or tbv.FCFILE_STATUS = 'B' or tbv.FCFILE_STATUS = 'D')
			order by tbv.FSID, tbv.FNEPISODE
	END --subcase 1
	else if @subcase = 2 BEGIN -- HD 播出帶
		select (tbv.FSID + pgm.FSPGMNAME) as 節目名稱, tbv.FNEPISODE as 集別, (tbv.FSARC_TYPE + typ.FSTYPE_NAME) as 帶別, (tbv.FCFILE_STATUS + z.FSFILE_STATUS_NAME) 狀態,tbv.* 
			from TBLOG_VIDEO tbv, TBFILE_TYPE typ, TBPROG_M pgm, TBZFILE_STATUS z
			where tbv.FSARC_TYPE = typ.FSARC_TYPE and tbv.FSID = pgm.FSPROG_ID and tbv.FCFILE_STATUS = z.FSFILE_STATUS_ID
			and (tbv.FCFILE_STATUS = 'T' or tbv.FCFILE_STATUS = 'B' or tbv.FCFILE_STATUS = 'D')
			and tbv.FSARC_TYPE = '001'
			order by tbv.FSID, tbv.FNEPISODE

			select * from TBPROG_M where (CHARINDEX('11', FSCHANNEL) > 0) -- 查詢 HD HD-MOD 可播出節目
			select * from TBPROG_M where FSCHANNEL like '%11%'
			select * from TBZCHANNEL
			select * from TBPROG_D where FSPROG_ID = '2016345' --查詢節目子集
			select * from TBPROG_D where (CHARINDEX('02', FSPROGSPEC) > 0) -- 查詢節目規格有HD
			select * from TBPROG_D where (CHARINDEX('01', FSPROGSPEC) > 0) -- 查詢節目規格有SD
			select * from TBPROG_M where FSPROG_ID = '0000060'
			select FSPROG_ID, count(*) as EPISODE from TBPROG_D group by FSPROG_ID
	END --subcase 2

	--select * from @arr_fnepisode
END --case 10

else if @case = 11 BEGIN -- AnyStream
	if @subcase = 1 begin --purge FNANYSTREAM_ID
		select * from TBTRANSCODE where FNPARTIAL_ID = '114847'	
		select * from TBTRANSCODE where FDSTART_TIME between '2015-7-7' and '2015-07-9'  -- purge on 20150708
		 --purge on 20160218
		select * from TBTRANSCODE where FDSTART_TIME between '2016-02-17' and '2016-02-18'
		exec SP_D_TBTRANSCODE_ANSID

		select * from TBTRANSCODE_ANS_BAK
	end
end

else if @case = 12 BEGIN -- 入庫分析
	declare @BeginDate DATE = '2015-01-01'
	declare @i int = 0

	if @subcase = 1 BEGIN
		-- FSTYPE : P:PROMO, G:PROGRAM. D:DATA?
		select COUNT (*) from TBLOG_VIDEO as LV left outer join TBBROADCAST as BRO on LV.FSBRO_ID= BRO.FSBRO_ID
        where (LV.[FDCREATED_DATE] between DATEADD(MONTH, @i, @BeginDate) and DATEADD(MONTH, @i+ 1, @BeginDate ))
		and LV.FSCHANNEL_ID ='01' and LV.FSTYPE<>'P' and BRO.FCCHECK_STATUS<> 'T'
		/*
		FCCHECK_STATUS     number
		T					回朔
		N					審核
		X					抽單
		F					不通過
		G					簽收   新流程的狀態
		C                   審核?
		Y					通過
		*/

	END
	else if @subcase = 2 BEGIN 
		select LV.FSBRO_ID, BRO.FSBRO_ID from TBLOG_VIDEO as LV left outer join TBBROADCAST as BRO on LV.FSBRO_ID = BRO.FSBRO_ID
		-- 有轉檔影片但是無轉檔單
		select LV.FSBRO_ID, BRO.FSBRO_ID from TBLOG_VIDEO as LV left outer join TBBROADCAST as BRO on LV.FSBRO_ID = BRO.FSBRO_ID where BRO.FSBRO_ID IS NULL
		select * from TBLOG_VIDEO
		select * from TBBROADCAST

	END
END
else if @case = 13 begin -- HD 檔案交播測試
	if @subcase = 1 begin
		/*
			FCSTATUS
			1:開始上傳
			80:上傳完成
		*/
		select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '0000ASQB'
		select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '0000ASKL'
		select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '0000ASRH'
		select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '0000ASUX'
		select * from TBPTSAP_STATUS_LOG order by FDUPDATE_DATE desc
	end
end
if @case = 14 begin -- 第八屆總統文化獎置換單問題
		select * from TBLOG_VIDEO where FSID = '2016657' and FNEPISODE = 3
		select * from TBLOG_VIDEO_D where FSID = '2016657'and FNEPISODE = 3
		select * from TBUSERS where FSUSER_ID = '50522'
		select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G201665700030003'
		select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G201665700030001'


		select FSBRO_ID, * from TBLOG_VIDEO where FCFILE_STATUS = 'X'  -- 抽單，狀態為刪除
		select FSBRO_ID, * from TBLOG_VIDEO where FCFILE_STATUS = 'D'  -- 有簽收，但是最後狀台為刪除
		select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G201665700030003'
		select FCLOW_RES, count(FCLOW_RES)  from TBLOG_VIDEO_SEG group by FCLOW_RES
		select * from TBLOG_VIDEO_SEG where FCLOW_RES = 'N' -- FCCHECK_STATUS : 抽單 FCFILE_STATUS 為 待轉檔 or 刪除
		select * from TBLOG_VIDEO_SEG where FCLOW_RES = 'F' -- FCCHECK_STATUS : 不通過 FCFILE_STATUS F:取消
		select * from TBLOG_VIDEO_SEG where FCLOW_RES = 'R' -- FCCHECK_STATUS : 抽單, 簽收 FCFILE_STATUS X:刪除，T:轉檔完成 ??
		select * from TBLOG_VIDEO_SEG where FCLOW_RES = 'Y' -- FCCHECK_STATUS : 簽收 FCFILE_STATUS T:轉檔完成

		select FSBRO_ID, * from TBLOG_VIDEO where FSFILE_NO = 'P201603001730001'
		select b.FSBRO_ID, b.FCCHECK_STATUS, t.FSFILE_NO, t.FCFILE_STATUS, s.FCLOW_RES
		from (TBBROADCAST b inner join TBLOG_VIDEO t on b.FSBRO_ID = t.FSBRO_ID) inner join TBLOG_VIDEO_SEG s on  t.FSFILE_NO = s.FSFILE_NO 
		where s.FCLOW_RES = 'F'


		-- 將檔案編號 G201665700030003 的轉檔影片 注記為刪除
		update TBLOG_VIDEO set FCFILE_STATUS = 'X',FDUPDATED_DATE = GETDATE(), FSUPDATED_BY = '51204' where FSFILE_NO = 'G201665700030003'
		update TBLOG_VIDEO_SEG set FCLOW_RES = 'R', FSUPDATED_BY='51204', FDUPDATED_DATE=GETDATE() where FSFILE_NO = 'G201665700030003'
		--改成刪除資料，因 TBBROADCAST 並無該筆資料
		delete from TBLOG_VIDEO where FSFILE_NO = 'G201665700030003'
		delete from TBLOG_VIDEO_SEG where FSFILE_NO = 'G201665700030003'
		--Ref
		--SP_U_TBLOG_VIDEO_SEG

end --case 14

if @case = 15 BEGIN -- HD 檔案交播
	if @subcase = 1 begin -- 查詢有哪些檔案已標記上傳，但尚未上傳 => 檔案未到
		select * from TBLOG_VIDEO_HD_MC where FCSTATUS = '' order by FDCREATED_DATE
		select h.*, t.FCFILE_STATUS, b.FCCHECK_STATUS from (TBLOG_VIDEO_HD_MC h join TBLOG_VIDEO t on h.FSBRO_ID = t.FSBRO_ID) join TBBROADCAST b on h.FSBRO_ID = b.FSBRO_ID  where h.FCSTATUS = '' and h.FSUPDATED_BY is not null order by h.FDCREATED_DATE
		select * from TBLOG_VIDEO_HD_MC where FSBRO_ID = '201603310154'
	end
	if @subcase = 2 begin -- 查詢尚未標記上傳，且不是退單(X)  FCFILE_STATUS:B(等待上傳）FCCHECK_STATUS:N(未上傳)
		select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002IE0' order by FDCREATED_DATE
		select h.*, t.FCFILE_STATUS, b.FCCHECK_STATUS from (TBLOG_VIDEO_HD_MC h join TBLOG_VIDEO t on h.FSBRO_ID = t.FSBRO_ID) join TBBROADCAST b on h.FSBRO_ID = b.FSBRO_ID  where h.FCSTATUS = '' and h.FSUPDATED_BY is null and t.FCFILE_STATUS <> 'X' order by h.FDCREATED_DATE
	end

	if @subcase = 3 begin
		select * from TBPTSAP_STATUS_LOG where FCSTATUS in (1, 4) --查詢正在上傳的檔案
	end

	if @subcase = 4 begin -- 查詢平均每天上傳的檔案量

		-- 長帶		
		select p.FSPGMNAME, t.FNEPISODE, s.* from (TBPTSAP_STATUS_LOG s left join TBLOG_VIDEO t on s.FSVIDEOID = t.FSVIDEO_PROG) join TBPROG_M p on t.FSID = p.FSPROG_ID
		where t.FSTYPE = 'G' order by s.FDCREATED_DATE desc, s.FDUPDATE_DATE desc

		select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002IHO'
		select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002IHO'
		select * from TBPGM_ARR_PROMO a, TBPGM_PROMO p where a.FSPROMO_ID = p.FSPROMO_ID and   a.FSVIDEO_ID = '00002IPP'

		declare @date DATE = '2016-05-02' -- 上傳日期
		--declare @date DATE = CAST(CURRENT_TIMESTAMP as DATE) -- 上傳日期

		-- 查詢當天已上傳的檔案，包含有播出及未播出的
		select CAST(q.FDDATE as varchar(20)) + ' ' + MIN(q.FSPLAY_TIME) as 'PLAY TIME', p.FSPGMNAME, t.FNEPISODE, UPPER(s.FSVIDEOID) as VIDEO_ID, t.FSFILE_SIZE, s.FDCREATED_DATE '開始上傳', s.FDUPDATE_DATE '上傳完成', DATEDIFF(MINUTE, s.FDCREATED_DATE, s.FDUPDATE_DATE) '時程(MIN)',
		s.FCSTATUS, FSPERCENTAGE, s.FCRESENDTIMES
		from (TBPTSAP_STATUS_LOG s left join TBLOG_VIDEO t on s.FSVIDEOID = t.FSVIDEO_PROG) join TBPROG_M p on t.FSID = p.FSPROG_ID left join TBPGM_COMBINE_QUEUE q on s.FSVIDEOID = q.FSVIDEO_ID
		where t.FSTYPE = 'G' and s.FCSTATUS = 80 and CAST(s.FDUPDATE_DATE as DATE) = @date 
		group by p.FSPGMNAME, s.FSVIDEOID, t.FSID, t.FNEPISODE, t.FSFILE_SIZE, s.FDCREATED_DATE, s.FDUPDATE_DATE, s.FCSTATUS, s.FSPERCENTAGE, s.FCRESENDTIMES, q.FDDATE
		order by s.FDCREATED_DATE desc, s.FDUPDATE_DATE desc

		-- 查詢當天上傳中的檔案，包含有播出及未播出的
		select CAST(q.FDDATE as varchar(20)) + ' ' + MIN(q.FSPLAY_TIME) as 'PLAY TIME', p.FSPGMNAME, t.FNEPISODE, UPPER(s.FSVIDEOID) as VIDEO_ID, t.FSFILE_SIZE, s.FDCREATED_DATE '開始上傳', s.FDUPDATE_DATE '上傳完成', DATEDIFF(MINUTE, s.FDCREATED_DATE, s.FDUPDATE_DATE) '時程(MIN)',
		s.FCSTATUS, FSPERCENTAGE, s.FCRESENDTIMES
		from (TBPTSAP_STATUS_LOG s left join TBLOG_VIDEO t on s.FSVIDEOID = t.FSVIDEO_PROG) join TBPROG_M p on t.FSID = p.FSPROG_ID left join TBPGM_COMBINE_QUEUE q on s.FSVIDEOID = q.FSVIDEO_ID
		where t.FSTYPE = 'G' and s.FCSTATUS <> 80 and CAST(s.FDUPDATE_DATE as DATE) = @date 
		group by p.FSPGMNAME, s.FSVIDEOID, t.FSID, t.FNEPISODE, t.FSFILE_SIZE, s.FDCREATED_DATE, s.FDUPDATE_DATE, s.FCSTATUS, s.FSPERCENTAGE, s.FCRESENDTIMES, q.FDDATE
		order by s.FDCREATED_DATE desc, s.FDUPDATE_DATE desc

		-- 查詢當天已上傳的短帶檔案
		--declare @date DATE = '2016-05-02' -- 上傳日期
		select CAST(q.FDDATE as varchar(20)) + ' ' + MIN(q.FSPLAYTIME) as 'PLAY TIME', p.FSPROMO_NAME, t.FNEPISODE, UPPER(s.FSVIDEOID) as VIDEO_ID, t.FSFILE_SIZE, s.FDCREATED_DATE '開始上傳', s.FDUPDATE_DATE '上傳完成', DATEDIFF(MINUTE, s.FDCREATED_DATE, s.FDUPDATE_DATE) '時程(MIN)',
		s.FCSTATUS, FSPERCENTAGE, s.FCRESENDTIMES
		from (TBPTSAP_STATUS_LOG s left join TBLOG_VIDEO t on s.FSVIDEOID = t.FSVIDEO_PROG) join TBPGM_PROMO p on t.FSID = p.FSPROMO_ID left join TBPGM_ARR_PROMO q on s.FSVIDEOID = q.FSVIDEO_ID
		where t.FSTYPE = 'P' and s.FCSTATUS = 80 and CAST(s.FDUPDATE_DATE as DATE) = @date 
		group by p.FSPROMO_NAME, s.FSVIDEOID, t.FSID, t.FNEPISODE, t.FSFILE_SIZE, s.FDCREATED_DATE, s.FDUPDATE_DATE, s.FCSTATUS, s.FSPERCENTAGE, s.FCRESENDTIMES, q.FDDATE
		order by s.FDCREATED_DATE desc, s.FDUPDATE_DATE desc

		-- 查詢當天已上傳的檔案，且有播出的
		--declare @date DATE = '2016-04-21' -- 上傳日期
		select CAST(q.FDDATE as varchar(20)) + ' ' + MIN(q.FSPLAY_TIME) as 'PLAY TIME', p.FSPGMNAME, t.FNEPISODE, UPPER(s.FSVIDEOID) as VIDEO_ID, t.FSFILE_SIZE, s.FDCREATED_DATE '開始上傳', s.FDUPDATE_DATE '上傳完成', DATEDIFF(MINUTE, s.FDCREATED_DATE, s.FDUPDATE_DATE) '時程(MIN)',
		s.FCSTATUS, FSPERCENTAGE, s.FCRESENDTIMES
		from (TBPTSAP_STATUS_LOG s left join TBLOG_VIDEO t on s.FSVIDEOID = t.FSVIDEO_PROG) join TBPROG_M p on t.FSID = p.FSPROG_ID left join TBPGM_COMBINE_QUEUE q on s.FSVIDEOID = q.FSVIDEO_ID
		where t.FSTYPE = 'G' and s.FCSTATUS = 80 and CAST(s.FDUPDATE_DATE as DATE) = @date and q.FDDATE >= @DATE
		group by p.FSPGMNAME, s.FSVIDEOID, t.FSID, t.FNEPISODE, t.FSFILE_SIZE, s.FDCREATED_DATE, s.FDUPDATE_DATE, s.FCSTATUS, s.FSPERCENTAGE, s.FCRESENDTIMES, q.FDDATE
		order by s.FDCREATED_DATE desc, s.FDUPDATE_DATE desc



		select * from TBPGM_ARR_PROMO
		select * from TBPGM_PROMO


		select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002I7O' -- 聽聽看 802 有兩筆紀錄，因為有重新標記

		select * from TBUSERS where FSUSER in ('prg70347', 'prg60058', 'prg50676', 'prg51258', 'news51080', 'news51079')
		select * from TBUSERS where FSUSER_CHTNAME = '賴彥如'

		select s.*, t.* from TBPTSAP_STATUS_LOG s left join TBLOG_VIDEO t on s.FSVIDEOID = t.FSVIDEO_PROG where CAST(s.FDUPDATE_DATE as DATE) = '2016-04-26'

		select * from TBLOG_VIDEO_HD_MC where CAST(FDUPDATED_DATE as DATE) = '2016-04-26'

		select CAST(FDDATE as varchar(20)) + ' ' +  MIN(FSPLAY_TIME) as 'PLAY TIME', FSPROG_NAME, FNEPISODE 
		from TBPGM_COMBINE_QUEUE where FSVIDEO_ID in ('000028U6', '000025U6', '00002IE3', '00002I49', '00001ZSJ') and CAST(FDDATE as DATE) = '2016-04-26' and FNBREAK_NO = 1 group by FSPROG_NAME

		select MIN(FSPLAY_TIME) as 'PLAY TIME', FSPROG_NAME, FNEPISODE 
		from TBPGM_COMBINE_QUEUE where FSVIDEO_ID in ('000028U6', '000025U6', '00002IE3', '00002I49', '00001ZSJ') and CAST(FDDATE as DATE) = '2016-04-26' and FNBREAK_NO = 1 
		group by FSPROG_NAME, FNEPISODE

		select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID in ('00002II0', '00002IBS')

		-- 查詢正在上傳的檔案

		declare @date1 DATE = '2016-05-02' -- 上傳日期
		select CAST(q.FDDATE as varchar(10)) + ' ' + q.FSPLAY_TIME as 'PLAY TIME' , p.FSPGMNAME, t.FNEPISODE, UPPER(s.FSVIDEOID), t.FSFILE_SIZE, s.FDCREATED_DATE '開始上傳', s.FDUPDATE_DATE '上傳完成', DATEDIFF(MINUTE, s.FDCREATED_DATE, s.FDUPDATE_DATE) '時程(MIN)',
		s.FCSTATUS, FSPERCENTAGE, s.FCRESENDTIMES
		from (TBPTSAP_STATUS_LOG s left join TBLOG_VIDEO t on s.FSVIDEOID = t.FSVIDEO_PROG) join TBPROG_M p on t.FSID = p.FSPROG_ID join TBPGM_COMBINE_QUEUE q on s.FSVIDEOID = q.FSVIDEO_ID
		where t.FSTYPE = 'G' and s.FCSTATUS = 80 and CAST(s.FDUPDATE_DATE as DATE) = @date1 order by q.FDDATE, q.FSPLAY_TIME, s.FDCREATED_DATE desc, s.FDUPDATE_DATE desc

		select p.FSPGMNAME, t.FNEPISODE, s.FSVIDEOID, t.FSFILE_SIZE, s.FCSTATUS, FSPERCENTAGE, s.FCRESENDTIMES, s.FDCREATED_DATE '開始上傳', s.FDUPDATE_DATE '上傳完成', DATEDIFF(MINUTE, s.FDCREATED_DATE, s.FDUPDATE_DATE) '時程(MIN)'
		from (TBPTSAP_STATUS_LOG s left join TBLOG_VIDEO t on s.FSVIDEOID = t.FSVIDEO_PROG) join TBPROG_M p on t.FSID = p.FSPROG_ID
		where t.FSTYPE = 'G' and s.FCSTATUS = 80 and s.FSVIDEOID in ('00002IE1', '00002II2', '00002II8', '0000202W', '0000202Y') order by s.FDCREATED_DATE desc, s.FDUPDATE_DATE desc

		-- 就是愛運動 35 無檔案大小資訊
		select * from TBLOG_VIDEO where FSVIDEO_PROG in ('0000202W', '0000202Y')
		select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002ifz' -- 尚未排播


		select p.FSPGMNAME, t.* from TBLOG_VIDEO t join TBPROG_M p on t.FSID = p.FSPROG_ID where t.FSVIDEO_PROG = '0000202Y'


		select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '0000202Y'
		select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002i7o'


		-- 短帶
		select p.FSPROMO_NAME, s.* from (TBPTSAP_STATUS_LOG s left join TBLOG_VIDEO t on s.FSVIDEOID = t.FSVIDEO_PROG) join TBPGM_PROMO p on t.FSID = p.FSPROMO_ID
		where t.FSTYPE = 'P' and s.FSVIDEOID in ('00002IE1', '00002II2', '00002II8', '0000202W', '0000202Y') order by s.FDCREATED_DATE desc, s.FDUPDATE_DATE desc

		select * from TBLOG_VIDEO
	end
	select * from TBPTSAP_STATUS_LOG  order by FDCREATED_DATE desc 

	select * from TBPTSAP_STATUS_LOG ap join TBLOG_VIDEO v on ap.FSVIDEOID = v.FSVIDEO_PROG

	-- TBLOG_VIDEO.FCFILE_STATUS = 'D' ：簽收後被注記刪除
	select b.FSBRO_ID, b.FCCHECK_STATUS ,v.*, '分隔', d.* from (TBBROADCAST b join TBLOG_VIDEO v on b.FSBRO_ID = v.FSBRO_ID) left join TBLOG_VIDEO_D d on v.FSFILE_NO = d.FSFILE_NO where v.FCFILE_STATUS = 'D'
	
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002GL7'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002GC0'
	SELECT * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002GRO'
	SELECT * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002GTJ'
	SELECT * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002GC0' --一把青 #31
	SELECT * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002GTM' --探春
	SELECT * from TBLOG_VIDEO_HD_MC order by FDUPDATED_DATE desc
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002GTM'
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002GL7'

	--一字千金 16 內容是 15 集的
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002HLE'
	select * from TBPTSAP_STATUS_LOG where FSVIDEOID = '00002HLE'
	select * from TBUSERS where FSUSER_ID = '07171' or FSUSER_ID = '51272'  --江麗禎
    select * from TBPGM_COMBINE_QUEUE where FSVIDEO_ID = '00002HLE'and FDDATE > '2016-04-10'
	select * from TBTRAN_LOG where FDDATE between '2016/04/13 13:30' and '2016/04/13 19:30' and FSUSER_ID = 'PTS_AP' and FSPARAMETER like '%00002HLE%'
	select * from TBPGM_COMBINE_QUEUE where FDDATE = '2016-04-22' and FSCHANNEL_ID = '11'  order by FSPLAY_TIME
	select * from TBPGM_ARR_PROMO where FDDATE = '2016-04-22' and FSCHANNEL_ID = '11'


	select * from TBPTSAP_STATUS_LOG order by FDCREATED_DATE desc, FDUPDATE_DATE desc

	select * from TBUSERS where FSUSER_ID = '71183'
	select * from TBLOG_VIDEO_HD_MC where FCSTATUS = 'S'
	select * from TBLOG_VIDEO_HD_MC where FCSTATUS = 'C'
	select * from TBLOG_VIDEO_HD_MC where FCSTATUS = 'N'
	select * from TBLOG_VIDEO_HD_MC where FCSTATUS = 'O'
	select * from TBLOG_VIDEO_HD_MC where FCSTATUS = ''
	select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID = '00002I7O'

	select * from TBLOG_VIDEO_HD_MC order by FDUPDATED_DATE desc, FDCREATED_DATE

	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002GRX'
	select * from TBLOG_VIDEO_D

	-- 4/9 的表在 4/11  才審核通過，檔案仍可繼續推送到待播區，但是未轉檔入庫
	select d.FSFILE_NO, d.FCFILE_STATUS, t.* from TBLOG_VIDEO t left join TBLOG_VIDEO_D d on t.FSFILE_NO = d.FSFILE_NO where t.FSVIDEO_PROG = '00002GL7'

	-- 查詢 Video id 的播出時間及轉檔狀態（根據插入短帶）
	-- 4/9 的表在 4/11 才審核通過，檔案仍可繼續推送到待播區，但是未轉檔入庫
	select q.FNCONBINE_NO, q.FDDATE, q.FNBREAK_NO,p.FSPGMNAME, q.FNEPISODE, q.FSPLAY_TIME, d.*, t.*
	from ((TBLOG_VIDEO t left join TBLOG_VIDEO_D d on t.FSFILE_NO = d.FSFILE_NO) join TBPROG_M p on t.FSID = p.FSPROG_ID) join TBPGM_COMBINE_QUEUE q on p.FSPROG_ID = q.FSPROG_ID 
	where t.FSVIDEO_PROG = '00001ORM' and q.FDDATE = '2016-04-9' and q.FSCHANNEL_ID = '11' order by FNCONBINE_NO

	-- 查詢 Video id 的播出時間及轉檔狀態 （根據節目表維護）
	select q.FDDATE, p.FSPGMNAME, q.FNEPISODE, q.FSBEG_TIME, d.FSFILE_NO, d.FSARC_ID, d.FCFILE_STATUS, d.FSTRANS_FROM, t.FSFILE_NO, t.FCFILE_STATUS, t.FSVIDEO_PROG
	from ((TBLOG_VIDEO t left join TBLOG_VIDEO_D d on t.FSFILE_NO = d.FSFILE_NO) join TBPROG_M p on t.FSID = p.FSPROG_ID) join TBPGM_QUEUE q on p.FSPROG_ID = q.FSPROG_ID 
	where t.FSVIDEO_PROG = '00002HLA' and q.FSCHANNEL_ID = '11' and q.FDDATE > CAST(CURRENT_TIMESTAMP AS DATE) order by q.FDDATE

	-- 查詢節目名稱集數 的播出時間及轉檔狀態 （根據節目表維護）
	select q.FDDATE, q.FSPROG_NAME, q.FNEPISODE, q.FSBEG_TIME, d.FSFILE_NO, d.FSARC_ID, d.FCFILE_STATUS, d.FSTRANS_FROM, t.FSFILE_NO, t.FSARC_TYPE, t.FCFILE_STATUS, t.FSVIDEO_PROG
	from ((TBLOG_VIDEO t left join TBLOG_VIDEO_D d on t.FSFILE_NO = d.FSFILE_NO and t.FSID = d.FSID and t.FNEPISODE = d.FNEPISODE) join TBPROG_M p on t.FSID = p.FSPROG_ID) join TBPGM_QUEUE q on (p.FSPROG_ID = q.FSPROG_ID) 
	where (q.FSPROG_NAME like '%音樂萬萬歲3%' and q.FNEPISODE = 45) and q.FSCHANNEL_ID = '11' and q.FDDATE = '2016-04-9' and t.FSARC_TYPE = '002'

	select q.*, d.*, t.*
	from ((TBLOG_VIDEO t left join TBLOG_VIDEO_D d on t.FSFILE_NO = d.FSFILE_NO) join TBPROG_M p on t.FSID = p.FSPROG_ID) join TBPGM_QUEUE q on (p.FSPROG_ID = q.FSPROG_ID) 
	where (q.FSPROG_NAME like '%音樂萬萬歲3%' and q.FNEPISODE = 45) and q.FSCHANNEL_ID = '11' and q.FDDATE = '2016-04-9' and t.FSARC_TYPE = '002'

	-- 根據 Video Id 查節目表維護及轉檔段落，t.FSID 並非唯一，同一  FSID 會有多筆置換
	select q.FDDATE, q.FSBEG_TIME, p.FSPGMNAME,t.*, d.* 
	from ((TBLOG_VIDEO t left join TBLOG_VIDEO_D d on t.FSFILE_NO = d.FSFILE_NO) join TBPROG_M p on t.FSID = p.FSPROG_ID) join TBPGM_QUEUE q on  t.FSID = q.FSPROG_ID
	where t.FSVIDEO_PROG = '00002I7O' and FDDATE > '2016-04-21' order by FDDATE

	-- t.FSID 並非唯一，同一  FSID 會有多筆置換
	select q.FDDATE, q.FSBEG_TIME ,q.FSPROG_NAME, q.FNEPISODE, q.FSCHANNEL_ID, t.*, d.*
	from ((TBLOG_VIDEO t left join TBLOG_VIDEO_D d on t.FSFILE_NO = d.FSFILE_NO) join TBPROG_M p on t.FSID = p.FSPROG_ID) join TBPGM_QUEUE q on  t.FSID = q.FSPROG_ID and t.FNEPISODE = q.FNEPISODE
	where q.FSPROG_NAME like  '%汪達的外星朋友%' and q.FNEPISODE = 21 and q.FDDATE between '2016-04-01' and '2016-04-30' and (q.FSCHANNEL_ID = '11' and t.FSARC_TYPE = '002')
	order by q.FDDATE, q.FSBEG_TIME

	select FSFILE_NO, FSID, FNEPISODE, FCFILE_STATUS, FSARC_TYPE, FSVIDEO_PROG, FSCREATED_BY, FDCREATED_DATE, FDUPDATED_DATE from TBLOG_VIDEO where FSID = '2016771' and FNEPISODE = 21
	select * from TBBROADCAST where FSBRO_ID = '201604140061'

	select * from TBUSERS where FSUSER_ID = '50598'


	select * from TBPGM_QUEUE where FSCHANNEL_ID = '11' and FSPROG_NAME like '%就是愛運動%' and FNEPISODE = 34

	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002GL7'

	select * from TBLOG_VIDEO
	select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'TBPGM_QUEUE'

	if @subcase = 2 begin -- 測試 B 棟檔案上傳效能
		-- 建立送播單
		select b.*, '分隔' ,v.*, '分隔', d.* 
		from (TBBROADCAST b join TBLOG_VIDEO v on b.FSBRO_ID = v.FSBRO_ID) left join TBLOG_VIDEO_D d on v.FSFILE_NO = d.FSFILE_NO 
		where CAST(v.FDCREATED_DATE as DATE) = '2016-04-14' and v.FSFILE_NO like 'G2013232%' order by v.FDCREATED_DATE desc


		select * from TBPTSAP_STATUS_LOG where CAST(FDCREATED_DATE as DATE) = '2016-04-14'
		select * from TBLOG_VIDEO_HD_MC where CAST(FDCREATED_DATE as DATE) = '2016-04-14' and FSFILE_NO like 'G2013232%'

		select * from TBLOG_VIDEO_HD_MC mc left join TBPTSAP_STATUS_LOG ap on mc.FSVIDEO_ID = ap.FSVIDEOID where CAST(mc.FDCREATED_DATE as DATE) = '2016-04-14' and mc.FSFILE_NO like 'G2013232%'

		-- 查詢 NAS 上傳到HDUpload 的狀態
		select mc.FSFILE_NO, mc.FSVIDEO_ID, mc.FCSTATUS, ap.FCSTATUS, ap.FDCREATED_DATE, ap.FDUPDATE_DATE, DATEDIFF(MINUTE, ap.FDCREATED_DATE, ap.FDUPDATE_DATE) as 'DUR(MIN)'
		from TBLOG_VIDEO_HD_MC mc left join TBPTSAP_STATUS_LOG ap on mc.FSVIDEO_ID = ap.FSVIDEOID where CAST(mc.FDCREATED_DATE as DATE) = '2016-04-14' and mc.FSFILE_NO like 'G2013232%'

		select * from TBTRAN_LOG where CAST(FDDATE as DATE) = '2016-04-14' order by FDDATE desc
		select * from TBTRAN_LOG where FDDATE between '2016/04/14 10:00' and '2016/04/14 12:00' order by FDDATE desc
		select * from TBTRAN_LOG where FDDATE between '2016/04/14 14:30' and '2016/04/14 16:50'and FSPARAMETER like '%002HQ3%' order by FDDATE 


		select * from VW_LOG_VIDEO_TIMECODE
	end

	if @subcase = 3 begin -- 測試 A 棟檔案上傳效能
		select * from TBLOG_VIDEO where FSID = '2013232' and CAST(FDCREATED_DATE as DATE) = '2016-04-20' order by FDCREATED_DATE desc
		select * from TBPTSAP_STATUS_LOG where CAST(FDCREATED_DATE as DATE) = '2016-04-21'
		select * from TBLOG_VIDEO_HD_MC where CAST(FDCREATED_DATE as DATE) = '2016-04-20' and FSFILE_NO like 'G2013232%'
		select * from TBLOG_VIDEO_HD_MC mc left join TBPTSAP_STATUS_LOG ap on mc.FSVIDEO_ID = ap.FSVIDEOID where CAST(mc.FDCREATED_DATE as DATE) = '2016-04-20' and mc.FSFILE_NO like 'G2013232%'

		select mc.FSBRO_ID, mc.FSFILE_NO, mc.FSVIDEO_ID, mc.FCSTATUS, ap.FCSTATUS, ap.FDCREATED_DATE, ap.FDUPDATE_DATE, DATEDIFF(MINUTE, ap.FDCREATED_DATE, ap.FDUPDATE_DATE) as 'DUR(MIN)'
		from TBLOG_VIDEO_HD_MC mc left join TBPTSAP_STATUS_LOG ap on mc.FSVIDEO_ID = ap.FSVIDEOID where CAST(mc.FDCREATED_DATE as DATE) = '2016-04-20' and mc.FSFILE_NO like 'G2013232%' order by ap.FDCREATED_DATE

		select mc.FSFILE_NO, mc.FSVIDEO_ID, mc.FCSTATUS, ap.FCSTATUS, ap.FDCREATED_DATE, ap.FDUPDATE_DATE, DATEDIFF(MINUTE, ap.FDCREATED_DATE, ap.FDUPDATE_DATE) as 'DUR(MIN)'
		from TBLOG_VIDEO_HD_MC mc left join TBPTSAP_STATUS_LOG ap on mc.FSVIDEO_ID = ap.FSVIDEOID where CAST(mc.FDCREATED_DATE as DATE) = '2016-04-20'  order by ap.FDCREATED_DATE

		select DATEDIFF(MINUTE, CAST('2016-04-22 01:05:55' as DATE), CAST('2016-04-21 18:48:20.837' as DATE))
		select DATEDIFF(MINUTE, '2016-04-21 18:48:20.837', '2016/04/22 01:05:55') as DUR

		--  1~5 送播單被刪除
		select * from TBBROADCAST where FSBRO_ID = '201604200007'
		
		select * from TBBROADCAST where FSBRO_ID = '201604200024'
		select * from TBLOG_VIDEO where FSBRO_ID = '201604200007'
		select * from TBLOG_VIDEO where FSBRO_ID in ('201604200024', '201604200007', '201604200010', '201604200011', '201604200012', '201604200009')
		select * from TBUSERS where FSUSER_ID = 'admin'

	end

	if @subcase = 3 begin --2015-16 歐洲冠軍足球聯賽 無法建立送播單
		select * from TBBROADCAST where FSID = '2016267'
		select * from TBLOG_VIDEO where FSID = '2016267'
		--查詢轉檔單及轉檔段落
		select b.FSBRO_ID, t.* from TBLOG_VIDEO t left join TBBROADCAST b on t.FSBRO_ID = b.FSBRO_ID where t.FSID = '2016267' and t.FNEPISODE = 80   order by b.FSBRO_ID
		select * from TBPROG_M where FSPGMNAME like '%2015-16 歐洲冠軍足球聯賽%'
		select * from TBUSERS where FSUSER_ID = '05088' --陳俊峰
		select * from TBPGM_QUEUE where FSPROG_ID = '2016267' and FNEPISODE = 80 and FSCHANNEL_ID = '11'
		select * from TBPGM_QUEUE where FSPROG_NAME like '%2015-16 歐洲冠軍足球聯賽%' and FNEPISODE = 80 and FSCHANNEL_ID = '11'
		-- 無轉檔編號 201604210033, 刪除 TBLOG_VIDEO 資料
		exec SP_D_TBLOG_VIDEO_BYBROID '201604210033' --using sa

		select * from TBLOG_VIDEO_SEG where FSCREATED_BY = '05088' and FSFILE_NO like 'G2016267%'
		select * from TBLOG_VIDEO_D where FSID = '2016267' and FNEPISODE = 80
		select t.*, s.* from TBLOG_VIDEO_SEG s left join TBLOG_VIDEO t on s.FSFILE_NO = t.FSFILE_NO where s.FSFILE_NO = 'G201694000010001'

		select * from TBTRAN_LOG where CAST(FDDATE as DATE) = '2016-04-25' order by FDDATE desc


	end

	--音樂萬萬歲3 48
	select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002IEU'
	select * from TBLOG_VIDEO where FSFILE_NO = 'G201558600480002'
	select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201558600480002'
	select * from TBLOG_VIDEO_SEG where FSFILE_NO = 'G201558600480002'

	select * from TBUSERS where FSUSER_ID = '71292'

	select * from TBLOG_VIDEO where FSVIDEO_PROG  = '00002IPG'

	if @subcase = 4 -- 根據 Video ID 查詢檔案由 NAS 上傳狀態
		select * from TBLOG_VIDEO_HD_MC m join TBPTSAP_STATUS_LOG t on m.FSVIDEO_ID = t.FSVIDEOID where m.FSVIDEO_ID in ('00002IDJ', '00002HNV', '00002HNW')

		select * from TBLOG_VIDEO_HD_MC where FSVIDEO_ID in ('00002IDJ', '00002HNV', '00002HNW')
		select * from  TBPTSAP_STATUS_LOG where FSVIDEOID in ('00002IDJ', '00002HNV', '00002HNW')
	end
END


else if @case = 16 BEGIN --查詢HD節目有哪些已填送帶轉檔單，存成文字檔 tab 分隔

	-- 將所有節目規格為 HD 且未刪除的子集寫進array
	declare @arr_fnepisode_all_hd TABLE (v_fsprog_id varchar(10), v_fsprogname nvarchar(50), v_fnepisode integer, v_dept varchar(246))
	insert into @arr_fnepisode_all_hd
	select d.FSPROG_ID, m.FSPGMNAME, d.FNEPISODE , dep.FULLName
	from  TBPROG_D d inner join TBPROG_M m on d.FSPROG_ID = m.FSPROG_ID inner join tblTBSDept dep on m.FNDEP_ID = dep.DeptID
	where (CHARINDEX('02', d.FSPROGSPEC) > 0) and d.FSDEL != 'Y'--and FSPROG_ID = '0000060'

	--select * from @arr_fnepisode_all_hd order by v_fsprog_id

	-- 將有填送帶轉檔單TC 的集數，狀態不為R(失敗）F(駁回）X(刪除) T(回朔) 寫進 array, 短帶、資料帶不包含
	declare @arr_fnepisode_hd TABLE (v_fsprog_id varchar(11), v_fnepisode integer, v_fsbro_id varchar(15))
	 --找出送帶轉檔單不是抽單(X)或不通過(F) 且轉檔單中的HD播出帶尚未失敗(X)或刪除(D)或取消 (F)的單  (含節目目和短帶)
	insert into @arr_fnepisode_hd
	select tb.FSID, tb.FNEPISODE, tb.FSBRO_ID from TBLOG_VIDEO tb, TBBROADCAST bro where tb.FSBRO_ID = bro.FSBRO_ID and (bro.FCCHECK_STATUS not in ('X', 'F', 'T') and bro.FSBRO_TYPE = 'G')
	and (tb.FCFILE_STATUS = 'B') AND tb.FSARC_TYPE = '002'

	select * from @arr_fnepisode_hd


	--select FSID, FNEPISODE from TBLOG_VIDEO where FCFILE_STATUS not in ('X', 'F', 'R') AND FSARC_TYPE = '002' --找出HD播出帶尚未失敗或刪除或駁回的單(會查到已抽單的轉檔單)
	--select fnepisode from TBLOG_VIDEO_D where FSId='0000060'  -- 有轉檔成功的
	--select fnepisode from TBLOG_VIDEO where FSId='0000060' AND FSARC_TYPE = '002' --找出尚未填過HD的
	--select fnepisode from TBLOG_VIDEO where FSId='0000060' and (FCFILE_STATUS='X' or FCFILE_STATUS='F' or FCFILE_STATUS='R')
	--select * from @arr_fnepisode_hd order by v_fsprog_id, v_fnepisode
	--select * from TBLOG_VIDEO where FSID='0000060' and FNEPISODE in (select * from @arr_fnepisode) AND FCFILE_STATUS not in ('X', 'F', 'R') AND FSARC_TYPE='002'
	--select v_fsprog_id, v_fnepisode from @arr_fnepisode_all_hd where v_fnepisode not in (select * from @arr_fnepisode_hd)
	select * from TBLOG_VIDEO
	select * from TBBROADCAST where FCCHECK_STATUS = 'T'

	declare CSR CURSOR for
	select * from @arr_fnepisode_all_hd

	declare @arr_no_hd TABLE(v_fsprog_id varchar(11), v_fsprogname nvarchar(50), v_fnepisode integer, deptName varchar(246))
	declare @fsprog_id varchar(11), @episode integer, @t_fsbro_id varchar(15), @pgm_name nvarchar(50), @deptName varchar(246)
	open CSR
	FETCH NEXT FROM CSR INTO @fsprog_id, @pgm_name, @episode, @deptName

	while (@@FETCH_STATUS = 0)
	BEGIN
		print 'Check ' + @fsprog_id+ '#' + CAST(@episode as varchar(10))
		if (not EXISTS(select * from @arr_fnepisode_hd where v_fsprog_id = @fsprog_id and v_fnepisode = @episode))
			insert @arr_no_hd VALUES (@fsprog_id, @pgm_name, @episode, @deptName)
	/*
		else BEGIN
			set @t_fsbro_id = (select v_fsbro_id from @arr_fnepisode_hd where v_fsprog_id = @fsprog_id and v_fnepisode = @episode)
			print 'Found:' + @t_fsbro_id
		END
	*/
		FETCH NEXT FROM CSR INTO @fsprog_id, @pgm_name, @episode, @deptName
	END -- while

	CLOSE CSR

	--select v_fsprog_id 節目編號, v_fsprogname 節目名稱, v_fnepisode 集數, deptName 製作單位 from @arr_no_hd
	select v_fsprog_id 節目編號, v_fsprogname 節目名稱, v_fnepisode 集數, deptName 製作單位 from @arr_no_hd group by v_fsprog_id, v_fsprogname, v_fnepisode, deptName order by v_fsprog_id

	--select * from TBBROADCAST where FSID='0000060' and FNEPISODE in (select * from @arr_fnepisode_all where v_fnepisode not in (select * from @arr_fnepisode)) -- 驗證

end --case 16

if @case = 17 begin
	if @subcase = 1 begin
		select * from VW_FILE_NAME where tmpName like '%一把青%' and FNEPISODE <> 0
		select * from VW_FILE_NAME where tmpName like '%%一字千金%'
	end
end

if @case = 18 begin
	if @subcase = 1 begin -- 飆!企鵝里德
		select * from TBBROADCAST where FSBRO_ID = '201207130050'
		select * from TBLOG_VIDEO where FSBRO_ID = '201207130050'
		select * from TBUSERS where FSUSER_ID = '70363'
	end
end

ELSE IF @case = 101 BEGIN
	if @subcase = 20160502 begin --https://groups.google.com/forum/?hl=zh-TW#!topic/pts-mam/DRznNqvFHQo
		--20160502 李永平-第19屆國家文藝講得主短片1 00002J0Q 與送播單Video ID 不符
		select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002IZP'
		select * from TBLOG_VIDEO where FSVIDEO_PROG = '00002J0Q'
		select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002IZP'
		select * from TBPGM_ARR_PROMO where FSVIDEO_ID = '00002J0Q'
		select * from TBLOG_VIDEO_SEG where FSVIDEO_ID = '00002J0Q'
	end


select * from TBZFILE_STATUS -- 查詢轉檔狀態
select t.*, s.FSFILE_STATUS_NAME from TBLOG_VIDEO t, TBZFILE_STATUS s where t.FSFILE_NO='P201411000070001' and t.FCFILE_STATUS=s.FSFILE_STATUS_ID
select t.*, s.FSFILE_STATUS_NAME from TBLOG_VIDEO t, TBZFILE_STATUS s where t.FSFILE_NO='G200721703760001' and t.FCFILE_STATUS=s.FSFILE_STATUS_ID

select * from TBLOG_VIDEO_D where fsid='201411190069'

select * from TBLOG_VIDEO where fsid='201411190069'


select * from TBLOG_VIDEO_D where FSFILE_NO = 'G201207801290009' or FSFILE_NO ='G201207801290001' or FSFILE_NO ='G201207801290002'

select * from TBLOG_VIDEO where FSFILE_NO = 'G201207801290009' or FSFILE_NO ='G201207801290001' or FSFILE_NO ='G201207801290002'

--------------------------------------------------------------------------------------------------------------------
/* 
查詢送帶轉檔單, 送帶轉檔，根據影片類型 
FSARC_TYPE: 001=SD播出帶 002=HD播出帶
*/
select b.FSBRO_ID ,pgm.FSPGMNAME, t.FSTYPE_NAME, v.*  from TBBROADCAST b, TBLOG_VIDEO v , TBFILE_TYPE t, TBPROG_M pgm where b.FSID=pgm.FSPROG_ID and b.FSID =v.FSID and b.FNEPISODE = v.FNEPISODE and v.FSARC_TYPE = t.FSARC_TYPE and t.FSARC_TYPE= '002'
select * from TBPROG_M where FSPROG_ID='0000816'



/*
 * wrong : 查送帶轉檔單，根據HD播出帶,節目名稱和集數, 同一節目如果有 SD 和 HD 兩張轉檔單，不能只靠節目編號過濾
 */
select b.FSID, b.FSBRO_ID, pgm.FSPGMNAME,  v.FNEPISODE, v.*  from TBBROADCAST b, TBLOG_VIDEO v , TBFILE_TYPE t, TBPROG_M pgm where b.FSID=pgm.FSPROG_ID and b.FSID =v.FSID and b.FNEPISODE = v.FNEPISODE and v.FSARC_TYPE = t.FSARC_TYPE
and t.FSARC_TYPE= '002' 
and pgm.FSPGMNAME like '%星球寶寶%' and b.FNEPISODE=10

select b.FSID, b.FSBRO_ID, pgm.FSPGMNAME,  v.FNEPISODE, v.*  from TBBROADCAST b, TBLOG_VIDEO v , TBFILE_TYPE t, TBPROG_M pgm where b.FSID=pgm.FSPROG_ID and b.FSID =v.FSID and b.FNEPISODE = v.FNEPISODE and v.FSARC_TYPE = t.FSARC_TYPE
and pgm.FSPGMNAME like '%星球寶寶%' and b.FNEPISODE=10

-- fixed
select b.FSBRO_ID, pgm.FSPGMNAME, b.FNEPISODE, v.*  from TBBROADCAST  b, (select * from TBLOG_VIDEO where FSARC_TYPE='002') v , TBFILE_TYPE t, TBPROG_M pgm
where b.FSID=pgm.FSPROG_ID and b.FSID =v.FSID and b.FNEPISODE = v.FNEPISODE and v.FSARC_TYPE = t.FSARC_TYPE and b.FSBRO_ID = v.FSBRO_ID
and v.FSARC_TYPE= '002' 
and pgm.FSPGMNAME like '%星球寶寶%' and b.FNEPISODE=10

select b.FSBRO_ID, pgm.FSPGMNAME, b.FNEPISODE, v.*  from TBBROADCAST  b, (select * from TBLOG_VIDEO where FSARC_TYPE='002') v , TBFILE_TYPE t, TBPROG_M pgm
where b.FSID=pgm.FSPROG_ID and b.FSID =v.FSID and b.FNEPISODE = v.FNEPISODE and v.FSARC_TYPE = t.FSARC_TYPE and b.FSBRO_ID = v.FSBRO_ID
and v.FSARC_TYPE= '002' 
and pgm.FSPGMNAME like '探索新美台灣' and b.FNEPISODE=4

select b.FSBRO_ID, pgm.FSPGMNAME, b.FNEPISODE, v.*  from TBBROADCAST  b, (select * from TBLOG_VIDEO where FSARC_TYPE='002') v , TBFILE_TYPE t, TBPROG_M pgm
where b.FSID=pgm.FSPROG_ID and b.FSID =v.FSID and b.FNEPISODE = v.FNEPISODE and v.FSARC_TYPE = t.FSARC_TYPE and b.FSBRO_ID = v.FSBRO_ID
and v.FSARC_TYPE= '002' 
and  pgm.FSPROG_ID='2015097' and b.FNEPISODE=4



select * from TBLOG_VIDEO_D where FSFILE_NO='G201451400080001' or FSFILE_NO='G201451400080003'
select * from TBLOG_VIDEO where FSFILE_NO='G201451400080001' or FSFILE_NO='G201451400080003'
select * from TBLOG_VIDEO where FSFILE_NO='G201509700040001'
select * from TBLOG_VIDEO where FSVIDEO_PROG='00001TSE' or FSVIDEO_PROG='00001QZY'

select * from TBBROADCAST where FSID='2014514' and FNEPISODE=8 --根據節目編號和集數查詢


-- good one 已轉檔 FCFILE_STATUS = 'T', 未轉檔 FCFILE_STATUS = 'B'
select b.FSBRO_ID, pgm.FSPROG_ID, pgm.FSPGMNAME, b.FNEPISODE, v.FSVIDEO_PROG, v.*  from TBBROADCAST  b, TBLOG_VIDEO v , TBFILE_TYPE t, TBPROG_M pgm
where b.FSID=pgm.FSPROG_ID and b.FSID =v.FSID and b.FNEPISODE = v.FNEPISODE and v.FSARC_TYPE = t.FSARC_TYPE and b.FSBRO_ID = v.FSBRO_ID
and v.FSARC_TYPE= '002' and v.FCFILE_STATUS='T'
and pgm.FSPGMNAME like '%爸媽%' and b.FNEPISODE=1075 

select * from TBBROADCAST where FSID='2015434' and FNEPISODE=10
select * from TBBROADCAST where FSID='2015097' and FNEPISODE=4

select * from TBLOG_VIDEO where FSID='2015434' and FNEPISODE=10

select * from TBLOG_VIDEO where FSBRO_ID='201412180091'
select * from TBLOG_VIDEO where FSID='2009285' and FNEPISODE=1075
select * from TBLOG_VIDEO_D where FSID='2009285'  and FNEPISODE=1075

select * from TBPROG_M where FSPGMNAME like '%星球寶寶%'

select v.*  from TBBROADCAST b, TBLOG_VIDEO v , TBFILE_TYPE t, TBPROG_M pgm where b.FSID=pgm.FSPROG_ID and b.FSID =v.FSID and b.FNEPISODE = v.FNEPISODE and v.FSARC_TYPE = t.FSARC_TYPE
and pgm.FSPGMNAME like '%星球寶寶%' and b.FNEPISODE=10


---------------------------------------------------------------------------------------------------------------------------

--置換有些有音軌資訊，有些沒有音軌資訊: 16 個夏天 15集 置換後仍有音軌 why?
--https://www.evernote.com/shard/s204/nl/23912146/176ae595-31a3-43df-8c60-8ea2dff0b16a
--公視藝文大道 129集
select * from TBLOG_VIDEO  where FSId='2012078' and FNEPISODE=129 

--16個夏天 15集
select * from TBLOG_VIDEO where FSId='2015054' and FNEPISODE=15

--成語賽恩思
select * from TBLOG_VIDEO where FSId='2011421' and FNEPISODE=24
select * from TBLOG_VIDEO  where FSARC_TYPE='002' and FSTYPE='G'
select * from TBLOG_VIDEO where FSId='2014434' and FNEPISODE=1

--公視藝文大道 129集
select * from TBLOG_VIDEO_D where FSId='2012078' and FNEPISODE=129

select * from TBLOG_VIDEO_D where FSId='2015518' and FNEPISODE=6
select * from TBLOG_VIDEO where FSId='2015518' and FNEPISODE=6
select * from TBLOG_VIDEO where FSVIDEO_PROG='0000NKI'

--16個夏天 15集
select * from TBLOG_VIDEO_D where FSId='2015054' and FNEPISODE=15

-- 水果冰淇淋 未送帶轉檔單
select * from TBLOG_VIDEO_D where FSId='0000060' and (FNEPISODE=1530 or FNEPISODE=1532 or FNEPISODE=1533 or FNEPISODE=1536)

-- 查詢送帶轉檔單, 送帶轉檔及影片類型
select b.FSBRO_ID,t.FSTYPE_NAME, v.*  from TBBROADCAST b, TBLOG_VIDEO v, TBFILE_TYPE t where b.FSID=v.FSID and b.FNEPISODE = v.FNEPISODE and v.FSARC_TYPE = t.FSARC_TYPE and t.FSARC_TYPE='011'



--查詢送帶轉檔單
select * from TBBROADCAST where FSBRO_ID='201411210001'

select FSID, FNEPISODE, count(FSID) from TBLOG_VIDEO group by FSID, FNEPISODE
select * from TBLOG_VIDEO where FSId='0000001' and FNEPISODE=1


select * from TBLOG_VIDEO_D where FSId= '0000060'




select * from TBBROADCAST where FSID='0000060' and FNEPISODE=1547



select * from TBLOG_VIDEO_D where FSFILE_NO= 'G201510000050003'
select * from TBLOG_VIDEO_DIFFERENT




/*
select * from TBBROADCAST where FSID= '0000001'
波西小園丁有 56   筆送帶轉檔單
select * from TBLOG_VIDEO where FSID= '0000001'
只有 54   筆？
第一集少了 2 筆
*/

-- 查出第一集的所以送帶轉檔單號
declare @arr_brodID TABLE (fsbrodid varchar(12))
insert into @arr_brodID
select FSBRO_ID from  TBBROADCAST where FSID='0000001' and FNEPISODE=1

-- 查出第一集的轉檔資訊
declare @arr_tblogid TABLE (fsbrodid varchar(12))
insert into @arr_tblogid
select FSBRO_ID from TBLOG_VIDEO where FSID='0000001' and FNEPISODE=1
--select * from @arr_tblogid

-- 有出現在TBBROADCAST 但是未出現在 TBLOG_VIDEO
select fsbrodid from @arr_brodID where fsbrodid not in (select * from @arr_tblogid)

-- 有出現在 TBLOG_VIDEO 但是未出現在 TBBROADCAST
select fsbrodid from @arr_tblogid where fsbrodid not in (select * from @arr_brodid)


--select * from @arr_brodID  第1集有33 筆送帶轉檔單 20150522

------------------------------------------------------------------------------------------------------------------------


select * from TBLOG_VIDEO where FSID='0000060'
select * from TBLOG_VIDEO where FSID='0000060' and FNEPISODE=1570 --根據節目編號和集數查詢轉檔影片(有TC，但無段落)
select * from TBLOG_VIDEO where FSVIDEO_PROG='00000BWO' -- 根據 Video ID 查轉檔單
select * from TBLOG_VIDEO where FSBRO_ID like '20120828%' -- 查詢20120828 轉檔
select * from TBLOG_VIDEO where FDCREATED_DATE between '2012-08-28 12:00' and '2012-08-28 16:00' order by FDCREATED_DATE -- 根據建立日期查詢轉檔
select * from TBLOG_VIDEO where FSID='0000001' and FSFILE_SIZE != '' -- 只有3筆，G000000100010008 被置換
select * from TBLOG_VIDEO where FSBRO_ID='201205070075'
select * from TBLOG_VIDEO where FSFILE_NO='G000000100010008'

select * from TBLOG_VIDEO_D where FSID='0000001' -- 只有4 筆資料，有轉檔完成
select count(FSBRO_ID) as number, FSBRO_ID from TBLOG_VIDEO where FSID='0000001' and FNEPISODE=1  group by FSBRO_ID having count(FSBRO_ID) > 1 -- 查詢波西小園丁第1集中，一張送帶轉檔單有多筆轉檔紀錄的



select * from TBLOG_VIDEO_D where FCFILE_STATUS='D' ORDER BY FDCREATED_DATE DESC --根據檔案狀態查詢 D:被置換 X:抽單 T:轉檔完成 R:轉檔失敗 B:待審核
select * from TBLOG_VIDEO_D where FSID='0000072' and FNEPISODE=802
select * from TBLOG_VIDEO where FSID='0000072' and FNEPISODE=802
select t.FSFILE_NO, t.FSID, t.FNEPISODE,  p.FSPGMNAME from TBLOG_VIDEO_D as t, TBPROG_M p where t.FSID = p.FSPROG_Id and t.FSID='0000072' and t.FNEPISODE=802 ORDER BY FDCREATED_DATE DESC
select t.FSBRO_ID, t.FSID, p.FSPGMNAME, t.FCCHANGE, t.* from TBBROADCAST as t, TBPROG_M p where t.FSID = p.FSPROG_Id and t.FSID='0000072' and t.FNEPISODE=802 ORDER BY FDCREATED_DATE DESC

select * from TBZFILE_STATUS -- 查訊送帶轉檔單狀態代碼
select * from TBFILE_TYPE -- 查詢帶別


END

GO

/*
	TEST_CASE @case, @subcase, @FSPRO_ID
	CASE INDEX
	2: 查詢水果冰淇淋有哪些未填送帶轉檔單
	3-*: 查詢送帶轉檔單 TBBROADCAST

 */

DECLARE
@case integer, @subcase integer
--set @case = 3 set @subcase = 1			--根據轉檔單號查詢送帶轉檔
--set @case = 4 set @subcase = 1			--根據轉檔單查詢轉檔影片 (TBLOG_VIDEO)
--set @case = 4 set @subcase = 2			--根據節目編號、集數及轉檔單查詢 TBLOG_VIDEO, TBLOG_VIDEO_D, FSBRO_ID = '' | ID
--@case = 4 set @subcase = 3			--根據檔案編號查詢轉檔單影片

--set @case = 5 set @subcase = 1			-- 修改音軌
--set @case = 5 set @subcase = 2			-- 修改音軌 SP_U_TBLOG_VIDEO_FSTRACK
set @case = 9
-- set @case = 10 set @subcase = 1
--set @case = 10 set @subcase = 2


DECLARE
@FSPRO_ID varchar(10) = '',
@FNEPISODE integer = 1,
@FSBRO_ID varchar(15) = '',
@FSFILE_NO varchar(16) = 'G201562800010001',
@FSTRACK nvarchar(4) = N'LRMM'

EXEC #TEST_CASE @case, @subcase, @FSPRO_ID, @FNEPISODE, @FSBRO_ID, @FSFILE_NO, @FSTRACK
GO
DROP PROC #TEST_CASE
GO


