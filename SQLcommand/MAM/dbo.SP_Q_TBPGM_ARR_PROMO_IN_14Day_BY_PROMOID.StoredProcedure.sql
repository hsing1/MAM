USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

-- Author:        <Jarvis.Yang>

-- Create date:   <2016/07/25>

-- Description:   查詢短帶在運行表中14天內的排播狀況
-- Update: 被嫌棄找太多，加上top1

-- =============================================

create Proc [dbo].[SP_Q_TBPGM_ARR_PROMO_IN_14Day_BY_PROMOID]
@FSPROMO_ID	varchar(11)

As
Select top 1 ISNULL(CONVERT(VARCHAR(20) ,A.FDDATE , 111),'') AS Want_Date,A.FSPLAYTIME,B.FSCHANNEL_NAME 
from TBPGM_ARR_PROMO A,TBZCHANNEL B 
where A.FSCHANNEL_ID=B.FSCHANNEL_ID and A.FSPROMO_ID=@FSPROMO_ID 
and a.FDDATE>=CONVERT(VARCHAR(20),GETDATE(),111) 
--and a.FDDATE<=CONVERT(VARCHAR(20),DATEADD(day,7,GETDATE()),111)
order by Want_Date,FSPLAYTIME,FSCHANNEL_NAME



GO
