USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_I_TBZPROGATTR]

@FSPROGATTRID char(2) ,
@FSPROGATTRNAME nvarchar(100) ,
@FSSORT char(2) ,
@FSCREATED_BY varchar(50) ,
@FDCREATED_DATE datetime ,
@FSUPDATED_BY varchar(50) ,
@FDUPDATED_DATE datetime

As
Insert Into TBZPROGATTR(FSPROGATTRID,FSPROGATTRNAME,FSSORT,FSCREATED_BY,FDCREATED_DATE,FSUPDATED_BY,FDUPDATED_DATE)
Values(@FSPROGATTRID,@FSPROGATTRNAME,@FSSORT,@FSCREATED_BY,GETDATE(),@FSUPDATED_BY,GETDATE())
GO
