USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		林冠宇
-- Create date: 2012/12/19
-- Description:	修改國家來源代碼
-- =============================================
create PROCEDURE [dbo].[SP_U_TBZPROGNATION]
      @ID nvarchar(100),
	  @FSNATION_ID nvarchar(100),
      @FSNATION_NAME nvarchar(100),
      @FSUPDATE_BY nvarchar(100),
      @FBISENABLE nvarchar(100),
      @FSSORT nvarchar(100)
AS
BEGIN
	update MAM.dbo.TBZPROGNATION 
		   set FSNATION_ID=@FSNATION_ID,
		   FSNATION_NAME=@FSNATION_NAME,
		   FSUPDATE_BY=@FSUPDATE_BY,
		   FBISENABLE=@FBISENABLE,
		   FSSORT=@FSSORT,
		   FDUPDATE_DATE=GETDATE()
		   where ID=@ID
		   
END

GO
