USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBZCODE](
	[FSCODE_ID] [varchar](10) NOT NULL,
	[FSCODE_CODE] [varchar](5) NOT NULL,
	[FSCODE_NAME] [nvarchar](50) NOT NULL,
	[FSCODE_ENAME] [varchar](200) NOT NULL,
	[FSCODE_ORDER] [varchar](2) NOT NULL,
	[FSCODE_SET] [varchar](100) NOT NULL,
	[FSCODE_NOTE] [nvarchar](200) NOT NULL,
	[FSCODE_CHANNEL_ID] [varchar](100) NOT NULL,
	[FSCODE_ISENABLED] [bit] NOT NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
	[FSUPDATED_BY] [varchar](50) NOT NULL,
	[FDUPDATED_DATE] [datetime] NOT NULL,
 CONSTRAINT [PK_TBZCODE] PRIMARY KEY CLUSTERED 
(
	[FSCODE_ID] ASC,
	[FSCODE_CODE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[TBZCODE] ADD  CONSTRAINT [DF_TBZCODE_FSCODE_ID]  DEFAULT ('') FOR [FSCODE_ID]
GO
ALTER TABLE [dbo].[TBZCODE] ADD  CONSTRAINT [DF_TBZCODE_FSCODE_CODE]  DEFAULT ('') FOR [FSCODE_CODE]
GO
ALTER TABLE [dbo].[TBZCODE] ADD  CONSTRAINT [DF_TBZCODE_FSCODE_NAME]  DEFAULT ('') FOR [FSCODE_NAME]
GO
ALTER TABLE [dbo].[TBZCODE] ADD  CONSTRAINT [DF_TBZCODE_FSCODE_ENAME]  DEFAULT ('') FOR [FSCODE_ENAME]
GO
ALTER TABLE [dbo].[TBZCODE] ADD  CONSTRAINT [DF_TBZCODE_FSCODE_ORDER]  DEFAULT ((99)) FOR [FSCODE_ORDER]
GO
ALTER TABLE [dbo].[TBZCODE] ADD  CONSTRAINT [DF_TBZCODE_FSCODE_SET]  DEFAULT ('') FOR [FSCODE_SET]
GO
ALTER TABLE [dbo].[TBZCODE] ADD  CONSTRAINT [DF_TBZCODE_FSCODE_NOTE]  DEFAULT ('') FOR [FSCODE_NOTE]
GO
ALTER TABLE [dbo].[TBZCODE] ADD  CONSTRAINT [DF_TBZCODE_FSCODE_CHANNEL_ID]  DEFAULT ('') FOR [FSCODE_CHANNEL_ID]
GO
ALTER TABLE [dbo].[TBZCODE] ADD  CONSTRAINT [DF_TBZCODE_FSCODE_ISENABLED]  DEFAULT ((1)) FOR [FSCODE_ISENABLED]
GO
ALTER TABLE [dbo].[TBZCODE] ADD  CONSTRAINT [DF_TBZCODE_FSCREATED_BY]  DEFAULT ('') FOR [FSCREATED_BY]
GO
ALTER TABLE [dbo].[TBZCODE] ADD  CONSTRAINT [DF_TBZCODE_FSCREATED_DATE]  DEFAULT (getdate()) FOR [FDCREATED_DATE]
GO
ALTER TABLE [dbo].[TBZCODE] ADD  CONSTRAINT [DF_TBZCODE_FSUPDATED_BY]  DEFAULT ('') FOR [FSUPDATED_BY]
GO
ALTER TABLE [dbo].[TBZCODE] ADD  CONSTRAINT [DF_TBZCODE_FDUPDATED_DATE]  DEFAULT ('1900/01/01') FOR [FDUPDATED_DATE]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'代碼群組ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZCODE', @level2type=N'COLUMN',@level2name=N'FSCODE_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'代碼CODE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZCODE', @level2type=N'COLUMN',@level2name=N'FSCODE_CODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'代碼顯示名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZCODE', @level2type=N'COLUMN',@level2name=N'FSCODE_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'代碼英文名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZCODE', @level2type=N'COLUMN',@level2name=N'FSCODE_ENAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'代碼顯示順序' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZCODE', @level2type=N'COLUMN',@level2name=N'FSCODE_ORDER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'代碼設定欄' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZCODE', @level2type=N'COLUMN',@level2name=N'FSCODE_SET'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'代碼備註欄' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZCODE', @level2type=N'COLUMN',@level2name=N'FSCODE_NOTE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'代碼開放頻道' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZCODE', @level2type=N'COLUMN',@level2name=N'FSCODE_CHANNEL_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'新增人員' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZCODE', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'新增日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZCODE', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人員' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZCODE', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZCODE', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
