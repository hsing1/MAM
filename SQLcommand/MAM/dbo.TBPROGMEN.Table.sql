USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBPROGMEN](
	[AUTO] [bigint] IDENTITY(1,1) NOT NULL,
	[FSPROG_ID] [char](7) NOT NULL,
	[FNEPISODE] [smallint] NOT NULL,
	[FSTITLEID] [char](4) NOT NULL,
	[FSEMAIL] [nvarchar](100) NULL,
	[FSSTAFFID] [char](10) NULL,
	[FSNAME] [nvarchar](100) NULL,
	[FSMEMO] [nvarchar](1000) NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
	[FSUPDATED_BY] [varchar](50) NULL,
	[FDUPDATED_DATE] [datetime] NULL,
	[FSCREATED_BY_PGM] [varchar](50) NULL,
	[FSUPDATED_BY_PGM] [varchar](50) NULL,
 CONSTRAINT [PK_TBPROGMEN_1] PRIMARY KEY CLUSTERED 
(
	[AUTO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
CREATE NONCLUSTERED INDEX [INDEX_PROGID_EPISODE] ON [dbo].[TBPROGMEN]
(
	[FSPROG_ID] ASC,
	[FNEPISODE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_TBPROGMEN_combo] ON [dbo].[TBPROGMEN]
(
	[FSPROG_ID] ASC,
	[FNEPISODE] ASC,
	[FSTITLEID] ASC,
	[FSNAME] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
CREATE NONCLUSTERED INDEX [IX_TBPROGMEN_FSNAME] ON [dbo].[TBPROGMEN]
(
	[FSNAME] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TBPROGMEN]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROGMEN_TBPROG_M] FOREIGN KEY([FSPROG_ID])
REFERENCES [dbo].[TBPROG_M] ([FSPROG_ID])
GO
ALTER TABLE [dbo].[TBPROGMEN] CHECK CONSTRAINT [FK_TBPROGMEN_TBPROG_M]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自動編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGMEN', @level2type=N'COLUMN',@level2name=N'AUTO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目編碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGMEN', @level2type=N'COLUMN',@level2name=N'FSPROG_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'集別' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGMEN', @level2type=N'COLUMN',@level2name=N'FNEPISODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'頭銜代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGMEN', @level2type=N'COLUMN',@level2name=N'FSTITLEID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'E-Mail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGMEN', @level2type=N'COLUMN',@level2name=N'FSEMAIL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'工作人員代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGMEN', @level2type=N'COLUMN',@level2name=N'FSSTAFFID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'公司/姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGMEN', @level2type=N'COLUMN',@level2name=N'FSNAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'備註' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGMEN', @level2type=N'COLUMN',@level2name=N'FSMEMO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGMEN', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGMEN', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGMEN', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGMEN', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
