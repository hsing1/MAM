USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <David.Sin>
-- Create date:   <2010/10/02>
-- Description:   查詢調用需宏觀審核的數量
-- =============================================

CREATE Proc [dbo].[SP_Q_GET_BOOKING_NEED_CHECK_INTERNATION]
@FSBOOKING_NO varchar(12)
AS

Select FCNEED_CHECK_INTERNATION From TBBOOKING_MASTER Where FSBOOKING_NO=@FSBOOKING_NO
GO
