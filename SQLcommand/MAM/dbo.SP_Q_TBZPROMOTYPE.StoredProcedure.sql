USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_Q_TBZPROMOTYPE]

As
SELECT FSPROMOTYPEID as ID,FSPROMOTYPENAME as NAME FROM MAM.dbo.TBZPROMOTYPE 
ORDER BY  FSSORT
GO
