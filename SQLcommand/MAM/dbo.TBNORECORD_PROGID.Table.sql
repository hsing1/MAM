USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBNORECORD_PROGID](
	[FSNO] [char](7) NOT NULL,
	[FSNO_DATE] [char](4) NOT NULL,
	[FSNOTE] [nvarchar](50) NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'取號記錄' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBNORECORD_PROGID', @level2type=N'COLUMN',@level2name=N'FSNO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'取號年分' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBNORECORD_PROGID', @level2type=N'COLUMN',@level2name=N'FSNO_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目中文名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBNORECORD_PROGID', @level2type=N'COLUMN',@level2name=N'FSNOTE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'取號者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBNORECORD_PROGID', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'取號日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBNORECORD_PROGID', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
