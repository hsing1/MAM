USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/24>
-- Description:   刪除所有的首播清單(沒有使用)
-- =============================================
CREATE Proc [dbo].[SP_D_TBPGM_FIRST_PLAY_LIST_ALL]

@FSCHANNEL_ID	varchar(2),
@FDDATE		date


As
DELETE from dbo.TBPGM_FIRST_PLAY_LIST 
where FSCHANNEL_ID=@FSCHANNEL_ID and FDDATE=@FDDATE





	
	
GO
