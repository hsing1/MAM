USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   查詢節目集數資料檔案狀態
-- =============================================
CREATE Proc [dbo].[SP_Q_PGM_PROG_EPISODE]

@FNEPISODE	smallint,
@FSARC_TYPE	char(3),
@FSPGDNAME nvarchar(50)

As
Select A.FSFILE_NO,B.FSPROG_ID,B.FNEPISODE,A.FSARC_TYPE,A.FCLOW_RES,A.FSBEG_TIMECODE,A.FSEND_TIMECODE ,B.FSPGDNAME
from TBPROG_D B left join dbo.TBLOG_VIDEO A on 
 A.FSID=B.FSPROG_ID and A.FNEPISODE=B.FNEPISODE
where 1=1
and @FNEPISODE = case when @FNEPISODE = '' then @FNEPISODE else A.FNEPISODE end
and A.FSARC_TYPE=@FSARC_TYPE
and FSPGDNAME like '%' + @FSPGDNAME + '%'

GO
