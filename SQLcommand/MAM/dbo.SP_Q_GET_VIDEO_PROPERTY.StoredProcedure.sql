USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_Q_GET_VIDEO_PROPERTY]
@FSFILE_NO varchar(16)
AS

SELECT FSVIDEO_PROPERTY FROM TBLOG_VIDEO WHERE FSFILE_NO=@FSFILE_NO
GO
