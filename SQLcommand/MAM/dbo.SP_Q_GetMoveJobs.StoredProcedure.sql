USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[SP_Q_GetMoveJobs] --'U','B','','2015-12-01'
	@FCCHECK_STATUS char(1),
	@FCFILE_STATUS char(1),
	@BegeinDate varchar(10),
	@EndDate varchar(10)
AS
BEGIN

	select video.FSVIDEO_PROG,bro.FDUPDATED_DATE from TBBROADCAST bro 
	join tblog_video video on bro.FSBRO_ID=video.FSBRO_ID
	left outer join TBLOG_VIDEO_HD_MC mc on mc.FSBRO_ID=video.FSBRO_ID
	where 
	bro.FCCHECK_STATUS=case when @FCCHECK_STATUS ='' then  bro.FCCHECK_STATUS else @FCCHECK_STATUS end
	and video.FCFILE_STATUS= case when @FCFILE_STATUS='' then video.FCFILE_STATUS else @FCFILE_STATUS end
	and  ISNULL(mc.FCSTATUS,'') in('','E')
	and video.FDUPDATED_DATE >= (case when @BegeinDate='' then '1911-01-01' else @BegeinDate end) 
	and video.FDUPDATED_DATE <= (case when @EndDate='' then '9999-12-31' else CAST(@EndDate as datetime)+1 end)
	
END

GO
