USE [MAM]
GO
CREATE USER [sp_user] FOR LOGIN [sp_user] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [sp_user]
GO
