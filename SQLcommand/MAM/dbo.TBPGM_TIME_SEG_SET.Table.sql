USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBPGM_TIME_SEG_SET](
	[FSTIME_SEG_TYPE] [varchar](10) NULL,
	[FSBEG_TIME] [varchar](4) NULL,
	[FSEND_TIME] [varchar](4) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'時段設定類型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_TIME_SEG_SET', @level2type=N'COLUMN',@level2name=N'FSTIME_SEG_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'起始時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_TIME_SEG_SET', @level2type=N'COLUMN',@level2name=N'FSBEG_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'結束時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_TIME_SEG_SET', @level2type=N'COLUMN',@level2name=N'FSEND_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'描述', @value=N'時段設定表(預約破口使用)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_TIME_SEG_SET'
GO
