USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Q_TBPROGMEN_BYCONDITIONS]
@FSPROG_ID char(7) ,
@FNEPISODE smallint ,
@FSTITLEID char(2) ,
@FSSTAFFID char(10) ,
@FSEMAIL varchar(50) ,
@FSNAME nvarchar(200) ,
@FSMEMO nvarchar(600) 

As
select ISNULL(b.FSUSER_ChtName,'') as FSCREATED_BY_NAME, ISNULL(C.FSUSER_ChtName,'') as FSUPDATED_BY_NAME ,
ISNULL(CONVERT(VARCHAR ,a.FDCREATED_DATE , 120),'') as WANT_FDCREATEDDATE,
ISNULL(CONVERT(VARCHAR ,a.FDUPDATED_DATE , 120),'') as WANT_FDUPDATEDDATE,
ISNULL(d.FSPGMNAME,'')  as FSPROG_ID_NAME,
ISNULL(e.FSPGDNAME,'')  as FNEPISODE_NAME,
a.* FROM TBPROGMEN  as a  
               left outer JOIN TBUSERS as b on a.FSCREATED_BY=b.FSUSER_ID
               left outer join TBUSERS as c on a.FSUPDATED_BY=c.FSUSER_ID
               left outer join TBPROG_M as d on a.FSPROG_ID=d.FSPROG_ID -----[MAM].[dbo].[TBPROG_M]
               left outer join TBPROG_D as e on a.FSPROG_ID=e.FSPROG_ID and a.FNEPISODE=e.FNEPISODE
where @FSPROG_ID = case when @FSPROG_ID ='' then @FSPROG_ID else a.FSPROG_ID end
AND @FNEPISODE = case when @FNEPISODE ='' then @FNEPISODE else a.FNEPISODE end
AND @FSTITLEID = case when @FSTITLEID ='' then @FSTITLEID else a.FSTITLEID end
AND @FSSTAFFID = case when @FSSTAFFID ='' then @FSSTAFFID else a.FSSTAFFID end
AND a.FSEMAIL like @FSEMAIL +'%'
AND a.FSNAME like @FSNAME +'%'
AND a.FSMEMO like @FSMEMO +'%'
order by FSPROG_ID,FNEPISODE
GO
