USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   修改短帶遺失清單狀態
-- =============================================
CREATE Proc [dbo].[SP_U_TBPGM_ARR_PROMO_LOST]
@FNCREATE_LIST_NO int,
@FNARR_PROMO_NO	int,
@FDDATE	date,
@FSCHANNEL_ID	varchar(2),
@FSSTATUS	varchar(1)

As
update dbo.TBPGM_ARR_PROMO_LOST set FSSTATUS=@FSSTATUS,FDINSERT_DATE=GETDATE()
where 
FNCREATE_LIST_NO=@FNCREATE_LIST_NO 
and FNARR_PROMO_NO=@FNARR_PROMO_NO
and FDDATE=@FDDATE
and FSCHANNEL_ID=@FSCHANNEL_ID




	
	
GO
