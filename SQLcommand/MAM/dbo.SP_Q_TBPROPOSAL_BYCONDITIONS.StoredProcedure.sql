USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Q_TBPROPOSAL_BYCONDITIONS] --'201201180001','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''
@FSPRO_ID  varchar(12),
@FCPRO_TYPE char(1) ,
@FSPROG_NAME nvarchar(100) ,
@FSPLAY_NAME nvarchar(100), 
@FSNAME_FOR varchar(100) ,
@FSCONTENT nvarchar(MAX) ,
@FNLENGTH int ,
@FNBEG_EPISODE smallint ,
@FNEND_EPISODE smallint ,
@FSCHANNEL varchar(20) ,
@FDSHOW_DATE date ,
@FSPRD_DEPT_ID varchar(2) ,
@FSPAY_DEPT_ID varchar(2) ,
@FSSIGN_DEPT_ID varchar(2) ,
@FSBEF_DEPT_ID varchar(100) ,
@FSAFT_DEPT_ID varchar(100) ,
@FSOBJ_ID varchar(2) ,
@FSGRADE_ID varchar(2) ,
@FSSRC_ID varchar(2) ,
@FSTYPE varchar(2) ,
@FSAUD_ID varchar(2) ,
@FSSHOW_TYPE varchar(2) ,
@FSLANG_ID_MAIN varchar(2) ,
@FSLANG_ID_SUB varchar(2) ,
@FSBUY_ID varchar(2) ,
@FSBUYD_ID varchar(2) ,
@FCCHECK_STATUS char(1) ,
@FSCHANNEL_ID varchar(2) ,
@FCEMERGENCY  char(1) ,
@FCCOMPLETE  char(1) ,
@FSPRDCENID varchar(5) ,
@FSPROGSPEC varchar(100) ,
@FCPROGD  char(1) ,
@FSENOTE  NVARCHAR(200),
@FDSTART datetime ,
@FDEND datetime ,
@FCSTATUS  char(1),
@FSPRODUCER varchar(20)

As
if(@FSPRO_ID='')
begin
select ISNULL(b.FSUSER_ChtName,'') as FSCREATED_BY_NAME, 
ISNULL(b.FSEMAIL,'') as FSCREATED_BY_EMAIL, 
ISNULL(C.FSUSER_ChtName,'') as FSUPDATED_BY_NAME ,
ISNULL(CONVERT(VARCHAR(20) ,a.FDCOMPLETE , 111),'') as WANT_FDCOMPLETE,
ISNULL(CONVERT(VARCHAR(20) ,a.FDSHOW_DATE , 111),'') as WANTSHOWDATE,
ISNULL(CONVERT(VARCHAR(20) ,a.FDCREATED_DATE , 111),'') as WANT_FDCREATEDDATE,
ISNULL(CONVERT(VARCHAR(20) ,a.FDUPDATED_DATE , 111),'') as WANT_FDUPDATEDDATE,
ISNULL(a.FSPRODUCER,'') as FSPRODUCER,
a.* FROM TBPROPOSAL  as a  
               left outer JOIN TBUSERS as b on a.FSCREATED_BY=b.FSUSER_ID
               left outer join TBUSERS as c on a.FSUPDATED_BY=c.FSUSER_ID
WHERE 
 @FCPRO_TYPE = case when @FCPRO_TYPE ='' then @FCPRO_TYPE else a.FCPRO_TYPE end
AND a.FSPROG_NAME like '%'+@FSPROG_NAME +'%'
AND a.FSPLAY_NAME like '%'+@FSPLAY_NAME +'%'
AND a.FSNAME_FOR like '%'+@FSNAME_FOR +'%'
AND a.FSCONTENT like '%'+@FSCONTENT +'%'
AND @FNLENGTH = case when @FNLENGTH ='' then @FNLENGTH else a.FNLENGTH end
AND @FNBEG_EPISODE = case when @FNBEG_EPISODE ='' then @FNBEG_EPISODE else a.FNBEG_EPISODE end
AND @FNEND_EPISODE = case when @FNEND_EPISODE ='' then @FNEND_EPISODE else a.FNEND_EPISODE end
AND @FSCHANNEL = case when @FSCHANNEL ='' then @FSCHANNEL else a.FSCHANNEL end
AND @FDSHOW_DATE = case when @FDSHOW_DATE ='' then @FDSHOW_DATE else a.FDSHOW_DATE end
AND @FSPRD_DEPT_ID = case when @FSPRD_DEPT_ID ='' then @FSPRD_DEPT_ID else a.FSPRD_DEPT_ID end
AND @FSPAY_DEPT_ID = case when @FSPAY_DEPT_ID ='' then @FSPAY_DEPT_ID else a.FSPAY_DEPT_ID end
AND @FSSIGN_DEPT_ID = case when @FSSIGN_DEPT_ID ='' then @FSSIGN_DEPT_ID else a.FSSIGN_DEPT_ID end
AND @FSBEF_DEPT_ID = case when @FSBEF_DEPT_ID ='' then @FSBEF_DEPT_ID else a.FSBEF_DEPT_ID end
AND @FSAFT_DEPT_ID = case when @FSAFT_DEPT_ID ='' then @FSAFT_DEPT_ID else a.FSAFT_DEPT_ID end
AND @FSOBJ_ID = case when @FSOBJ_ID ='' then @FSOBJ_ID else a.FSOBJ_ID end
AND @FSGRADE_ID = case when @FSGRADE_ID ='' then @FSGRADE_ID else a.FSGRADE_ID end
AND @FSSRC_ID = case when @FSSRC_ID ='' then @FSSRC_ID else a.FSSRC_ID end
AND @FSTYPE = case when @FSTYPE ='' then @FSTYPE else a.FSTYPE end
AND @FSAUD_ID = case when @FSAUD_ID ='' then @FSAUD_ID else a.FSAUD_ID end
AND @FSSHOW_TYPE = case when @FSSHOW_TYPE ='' then @FSSHOW_TYPE else a.FSSHOW_TYPE end
AND @FSLANG_ID_MAIN = case when @FSLANG_ID_MAIN ='' then @FSLANG_ID_MAIN else a.FSLANG_ID_MAIN end
AND @FSLANG_ID_SUB = case when @FSLANG_ID_SUB ='' then @FSLANG_ID_SUB else a.FSLANG_ID_SUB end
AND @FSBUY_ID = case when @FSBUY_ID ='' then @FSBUY_ID else a.FSBUY_ID end
AND @FSBUYD_ID = case when @FSBUYD_ID ='' then @FSBUYD_ID else a.FSBUYD_ID end
AND @FCCHECK_STATUS = case when @FCCHECK_STATUS ='' then @FCCHECK_STATUS else a.FCCHECK_STATUS end
AND @FSCHANNEL_ID = case when @FSCHANNEL_ID ='' then @FSCHANNEL_ID else a.FSCHANNEL_ID end
AND @FCEMERGENCY = case when @FCEMERGENCY ='' then @FCEMERGENCY else a.FCEMERGENCY end
AND @FCCOMPLETE = case when @FCCOMPLETE ='' then @FCCOMPLETE else a.FCCOMPLETE end
AND @FSPRDCENID = case when @FSPRDCENID ='' then @FSPRDCENID else a.FSPRDCENID end
AND @FSPROGSPEC = case when @FSPROGSPEC ='' then @FSPROGSPEC else a.FSPROGSPEC end
AND @FCPROGD = case when @FCPROGD ='' then @FCPROGD else a.FCPROGD end
AND @FSENOTE = case when @FSENOTE ='' then @FSENOTE else a.FSENOTE end
and a.FDCREATED_DATE between @FDSTART and @FDEND
AND @FCSTATUS = case when @FCSTATUS ='' then @FCSTATUS else a.FCCHECK_STATUS end
AND @FSPRODUCER=case when @FSPRODUCER ='' then @FSPRODUCER else a.FSPRODUCER end
order by a.FDCREATED_DATE desc
end
else
begin

select ISNULL(b.FSUSER_ChtName,'') as FSCREATED_BY_NAME, 
ISNULL(b.FSEMAIL,'') as FSCREATED_BY_EMAIL, 
ISNULL(C.FSUSER_ChtName,'') as FSUPDATED_BY_NAME ,
ISNULL(CONVERT(VARCHAR(20) ,a.FDCOMPLETE , 111),'') as WANT_FDCOMPLETE,
ISNULL(CONVERT(VARCHAR(20) ,a.FDSHOW_DATE , 111),'') as WANTSHOWDATE,
ISNULL(CONVERT(VARCHAR(20) ,a.FDCREATED_DATE , 111),'') as WANT_FDCREATEDDATE,
ISNULL(CONVERT(VARCHAR(20) ,a.FDUPDATED_DATE , 111),'') as WANT_FDUPDATEDDATE,
ISNULL(a.FSPRODUCER,'') as FSPRODUCER,
a.* FROM TBPROPOSAL  as a  
               left outer JOIN TBUSERS as b on a.FSCREATED_BY=b.FSUSER_ID
               left outer join TBUSERS as c on a.FSUPDATED_BY=c.FSUSER_ID
WHERE 
a.FSPRO_ID=@FSPRO_ID
end
/*
SP_Q_TBPROPOSAL_BYCONDITIONS '','','','','','','','','','2011/2/20'
*/


GO
