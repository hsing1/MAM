USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- 只列出尚未標記上傳的送播單，不含送帶轉檔單
CREATE VIEW [dbo].[VW_FILE_NAME]
AS
SELECT top 100000 FSBRO_ID, tmpName, FNEPISODE, SUBSTRING( FSVIDEO_ID, 3 , 6) as FSVIDEO_PROG
		from (select h.FSBRO_ID FSBRO_ID, h.FSVIDEO_ID FSVIDEO_ID , h.FDCREATED_DATE, h.FCSTATUS, t.FSID FSID, t.FNEPISODE , t.FSTYPE FSTYPE, t.FSARC_TYPE FSARC_TYPE, t.FCFILE_STATUS FCFILE_STATUS 
				from TBLOG_VIDEO_HD_MC h join TBLOG_VIDEO t on h.FSVIDEO_ID <> '' and (h.FSVIDEO_ID = t.FSVIDEO_PROG)) th
                join ( select FSPROG_ID tmpID, FSPGMNAME tmpName from TBPROG_M union (select FSPROMO_ID tmpID , FSPROMO_NAME tmpName from TBPGM_PROMO )) u
                on th.FSID = u.tmpID
                where th.FSTYPE in ('P', 'G')
					and th.FSARC_TYPE in ('002', '001')
                    and th.FCSTATUS = ''
                order by FDCREATED_DATE desc



GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "TBLOG_VIDEO"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 140
               Right = 243
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_FILE_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_FILE_NAME'
GO
