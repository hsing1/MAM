USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBARCHIVE_SET_CLASS](
	[FIID] [int] IDENTITY(1,1) NOT NULL,
	[FSCLASSNAME] [nvarchar](50) NULL,
	[FSVIDEO] [nvarchar](50) NULL,
	[FSAUDIO] [nvarchar](50) NULL,
	[FSPHOTO] [nvarchar](50) NULL,
	[FSDOC] [nvarchar](50) NULL,
 CONSTRAINT [PK_TBARCHIVE_SET_CLASS] PRIMARY KEY CLUSTERED 
(
	[FIID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBARCHIVE_SET_CLASS', @level2type=N'COLUMN',@level2name=N'FIID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'類別名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBARCHIVE_SET_CLASS', @level2type=N'COLUMN',@level2name=N'FSCLASSNAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'影的必須上傳數量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBARCHIVE_SET_CLASS', @level2type=N'COLUMN',@level2name=N'FSVIDEO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'音的必須上傳數量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBARCHIVE_SET_CLASS', @level2type=N'COLUMN',@level2name=N'FSAUDIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'圖的必須上傳數量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBARCHIVE_SET_CLASS', @level2type=N'COLUMN',@level2name=N'FSPHOTO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'其他的必須上傳數量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBARCHIVE_SET_CLASS', @level2type=N'COLUMN',@level2name=N'FSDOC'
GO
