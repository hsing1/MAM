USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   查詢短帶名稱與長度資料
-- =============================================
CREATE Proc [dbo].[SP_Q_TBPGM_PROMO_ALLNAME]

@FSPROMO_NAME	nvarchar(50)

As
select 
FSPROMO_ID,FSPROMO_NAME,FSDURATION
from 
TBPGM_PROMO 
where 
FSPROMO_NAME= @FSPROMO_NAME
GO
