USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_Q_TBLOG_VIDEO_SEG_BY_FSFILENO]
@FSFILE_NO varchar(16)
AS
Select FSBEG_TIMECODE,FSEND_TIMECODE
From TBLOG_VIDEO_SEG
Where FSFILE_NO=@FSFILE_NO
Order By FNSEG_ID
GO
