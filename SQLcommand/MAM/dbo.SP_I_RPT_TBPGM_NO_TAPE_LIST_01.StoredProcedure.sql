USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   新增未到帶清單報表資料
-- =============================================
CREATE Proc [dbo].[SP_I_RPT_TBPGM_NO_TAPE_LIST_01]

@QUERY_KEY	uniqueidentifier,
@QUERY_KEY_SUBID	uniqueidentifier,
@QUERY_BY	varchar(50),
@播出日期	date,
@頻道	nvarchar(50),
@節目名稱	nvarchar(50),
@集數	int,
@影像編號	varchar(16),
@起始時間	varchar(4),
@結束時間	varchar(4),
@LIVE播出	varchar(5),
@上檔下檔	varchar(5)


As
Insert Into dbo.RPT_TBPGM_NO_TAPE_LIST_01(QUERY_KEY,QUERY_KEY_SUBID,QUERY_BY,QUERY_DATE,播出日期,頻道,
節目名稱,集數,影像編號,起始時間,結束時間,LIVE播出,上檔下檔)
Values(@QUERY_KEY,@QUERY_KEY_SUBID,@QUERY_BY,GETDATE(),@播出日期,
@頻道,@節目名稱,@集數,@影像編號,@起始時間,@結束時間,@LIVE播出,@上檔下檔	)
GO
