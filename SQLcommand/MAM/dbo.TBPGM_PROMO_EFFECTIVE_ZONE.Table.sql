USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBPGM_PROMO_EFFECTIVE_ZONE](
	[FNPROMO_BOOKING_NO] [int] NOT NULL,
	[FSPROMO_ID] [varchar](11) NULL,
	[FNNO] [int] NOT NULL,
	[FDBEG_DATE] [date] NOT NULL,
	[FDEND_DATE] [date] NOT NULL,
	[FSBEG_TIME] [varchar](4) NULL,
	[FSEND_TIME] [varchar](4) NULL,
	[FCDATE_TIME_TYPE] [varchar](1) NULL,
	[FSCHANNEL_ID] [varchar](2) NULL,
	[FSWEEK] [char](7) NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
	[FSUPDATED_BY] [varchar](50) NULL,
	[FDUPDATED_DATE] [datetime] NULL,
 CONSTRAINT [PK_TBPGM_PROMO_EFFECTIVE_ZONE] PRIMARY KEY CLUSTERED 
(
	[FNPROMO_BOOKING_NO] ASC,
	[FNNO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'短帶託播單編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_EFFECTIVE_ZONE', @level2type=N'COLUMN',@level2name=N'FNPROMO_BOOKING_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'短帶編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_EFFECTIVE_ZONE', @level2type=N'COLUMN',@level2name=N'FSPROMO_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'序號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_EFFECTIVE_ZONE', @level2type=N'COLUMN',@level2name=N'FNNO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'有效起始日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_EFFECTIVE_ZONE', @level2type=N'COLUMN',@level2name=N'FDBEG_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'有效結束日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_EFFECTIVE_ZONE', @level2type=N'COLUMN',@level2name=N'FDEND_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'有效起始時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_EFFECTIVE_ZONE', @level2type=N'COLUMN',@level2name=N'FSBEG_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'有效結束時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_EFFECTIVE_ZONE', @level2type=N'COLUMN',@level2name=N'FSEND_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'日期時間格式(1:日期時間區間,2:時間區間)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_EFFECTIVE_ZONE', @level2type=N'COLUMN',@level2name=N'FCDATE_TIME_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'頻道' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_EFFECTIVE_ZONE', @level2type=N'COLUMN',@level2name=N'FSCHANNEL_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'有效星期(1100000:代表星期一二可排)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_EFFECTIVE_ZONE', @level2type=N'COLUMN',@level2name=N'FSWEEK'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_EFFECTIVE_ZONE', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_EFFECTIVE_ZONE', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_EFFECTIVE_ZONE', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_EFFECTIVE_ZONE', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'描述', @value=N'短帶託播單有效區間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_EFFECTIVE_ZONE'
GO
