USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   新增播出提示報表資料
-- =============================================
CREATE Proc [dbo].[SP_I_RPT_TBPGM_QUEUE_INSERT_TAPE_01]

@QUERY_KEY	uniqueidentifier,
@QUERY_KEY_SUBID	uniqueidentifier,
@QUERY_BY	varchar(50),
@播出日期	date,
@頻道	nvarchar(50),
@節目名稱	nvarchar(50),
@集數	int,
@主控插播帶	varchar(100),
@主控鏡面	varchar(100)

As
Insert Into dbo.RPT_TBPGM_QUEUE_INSERT_TAPE_01(
QUERY_KEY,QUERY_KEY_SUBID,QUERY_BY,QUERY_DATE,
播出日期,頻道,節目名稱,集數,主控插播帶,主控鏡面
)
Values(@QUERY_KEY,@QUERY_KEY_SUBID,@QUERY_BY,Getdate(),
@播出日期,@頻道,@節目名稱,@集數,@主控插播帶,@主控鏡面
)
GO
