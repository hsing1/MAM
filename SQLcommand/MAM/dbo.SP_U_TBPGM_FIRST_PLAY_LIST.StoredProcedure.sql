USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   修改首播清單(目前不使用)
-- =============================================
CREATE Proc [dbo].[SP_U_TBPGM_FIRST_PLAY_LIST]

@FSCHANNEL_ID	varchar(2),
@FDDATE		date,
@FSVIDEO_ID	varchar(10),
@FNVERSION		int

As
UPDATE dbo.TBPGM_FIRST_PLAY_LIST SET FDCREATEDATE=GETDATE(),FNVERSION=@FNVERSION
where FSCHANNEL_ID=@FSCHANNEL_ID and FDDATE=@FDDATE and FSVIDEO_ID=@FSVIDEO_ID





	
	
GO
