USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
create FUNCTION [dbo].[Caculate_TotalMinute] 
(
@hours int,
@minute int
)
RETURNS int
AS
BEGIN
	return (@hours*60)+@minute

END

GO
