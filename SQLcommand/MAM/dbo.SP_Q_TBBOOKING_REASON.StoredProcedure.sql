USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <David.Sin>
-- Create date:   <2010/10/02>
-- Description:   查詢調用原因
-- =============================================

CREATE PROC [dbo].[SP_Q_TBBOOKING_REASON]
AS

SELECT FSNO,FSREASON FROM TBZBOOKING_REASON WHERE FBISENABLE='1' ORDER BY FSSORT
GO
