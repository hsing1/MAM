USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2015/04/29>
-- Description:   主控FILING LIST
-- =============================================
CREATE Proc [dbo].[SP_I_RPT_DAY_FILING_LIST]

@QUERY_KEY	uniqueidentifier,
@QUERY_KEY_SUBID	uniqueidentifier,
@QUERY_BY	varchar(50),
@播出日期	date,
@頻道	nvarchar(50),
@FNNO	int,
@FSPLAY_TIME	varchar(11),
@FSIN		varchar(4),
@VIDEO_ID	varchar(8),
@FSOUT		varchar(4),
@節目名稱	nvarchar(50),
@集數	int,
@長度	varchar(11),
@FNBREAK_NO	int,
@FSMEMO		varchar(20)

As
Insert Into RPT_DAY_FILING_LIST(
QUERY_KEY,QUERY_KEY_SUBID,QUERY_BY,QUERY_DATE,
播出日期,頻道,FNNO,FSPLAY_TIME,FSIN,VIDEO_ID,FSOUT,節目名稱,集數,長度,FNBREAK_NO,FSMEMO
)
Values(@QUERY_KEY,@QUERY_KEY_SUBID,@QUERY_BY,Getdate(),
@播出日期,@頻道,@FNNO,@FSPLAY_TIME,@FSIN,@VIDEO_ID,@FSOUT,@節目名稱,@集數,@長度,@FNBREAK_NO,@FSMEMO
)
GO
