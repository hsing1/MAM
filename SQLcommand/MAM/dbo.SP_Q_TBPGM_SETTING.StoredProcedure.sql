USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        
-- Create date: 
-- Description: 查詢TBPGM_SETTING資料
-- Update desc:    <2016/02/01><Mike>加入註解。
-- =============================================
CREATE PROCEDURE [dbo].[SP_Q_TBPGM_SETTING]

AS
BEGIN

SELECT FSTYPE,FSNAME,FSMEMO FROM TBPGM_SETTING

END
GO
