USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

-- Author:        <Mike.Lin>

-- Create date:   <2012/10/25>

-- Description:   查詢短帶在運行表中的排播狀況

-- =============================================

create Proc [dbo].[SP_Q_TBPGM_CombinQueue_IN_14Day] --'0000060',1924
@fsid	char(7),
@FNEPISODE int

As
Select top 1 ISNULL(CONVERT(VARCHAR(20) ,A.FDDATE , 111),'') AS Want_Date,A.FSPLAY_TIME as 'FSPLAYTIME' ,B.FSCHANNEL_NAME 
from TBPGM_COMBINE_QUEUE A,TBZCHANNEL B 
where A.FSCHANNEL_ID=B.FSCHANNEL_ID and A.FSPROG_ID=@fsid and A.FNEPISODE=@FNEPISODE
 and a.FDDATE>=CONVERT(VARCHAR(20),GETDATE(),111) --and a.FDDATE<=CONVERT(VARCHAR(20),DATEADD(day,7,GETDATE()),111)
 order by Want_Date,B.FSCHANNEL_NAME ,A.FSPLAY_TIME

GO
