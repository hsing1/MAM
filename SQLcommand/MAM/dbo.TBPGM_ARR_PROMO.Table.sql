USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBPGM_ARR_PROMO](
	[FNARR_PROMO_NO] [int] NOT NULL,
	[FSPROG_ID] [char](7) NOT NULL,
	[FNSEQNO] [int] NOT NULL,
	[FDDATE] [date] NOT NULL,
	[FSCHANNEL_ID] [varchar](2) NOT NULL,
	[FNBREAK_NO] [int] NOT NULL,
	[FNSUB_NO] [int] NOT NULL,
	[FSPROMO_ID] [varchar](12) NOT NULL,
	[FSPLAYTIME] [varchar](11) NOT NULL,
	[FSDUR] [varchar](11) NOT NULL,
	[FSSIGNAL] [nvarchar](50) NULL,
	[FSMEMO] [nvarchar](500) NOT NULL,
	[FSMEMO1] [nvarchar](100) NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
	[FCTIMETYPE] [char](1) NULL,
	[FSVIDEO_ID] [varchar](8) NULL,
	[FSFILE_NO] [varchar](16) NULL,
 CONSTRAINT [PK_TBPGM_ARR_PROMO_1] PRIMARY KEY CLUSTERED 
(
	[FNARR_PROMO_NO] ASC,
	[FDDATE] ASC,
	[FSCHANNEL_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
CREATE NONCLUSTERED INDEX [INDEX_FSVIDEO_ID_FDDATE] ON [dbo].[TBPGM_ARR_PROMO]
(
	[FSVIDEO_ID] ASC,
	[FDDATE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'序號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO', @level2type=N'COLUMN',@level2name=N'FNARR_PROMO_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'所屬節目編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO', @level2type=N'COLUMN',@level2name=N'FSPROG_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'所屬節目播映編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO', @level2type=N'COLUMN',@level2name=N'FNSEQNO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'排播日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO', @level2type=N'COLUMN',@level2name=N'FDDATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'排播頻道' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO', @level2type=N'COLUMN',@level2name=N'FSCHANNEL_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'所屬節目段落' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO', @level2type=N'COLUMN',@level2name=N'FNBREAK_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO', @level2type=N'COLUMN',@level2name=N'FNSUB_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'宣傳帶編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO', @level2type=N'COLUMN',@level2name=N'FSPROMO_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'播出時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO', @level2type=N'COLUMN',@level2name=N'FSPLAYTIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'長度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO', @level2type=N'COLUMN',@level2name=N'FSDUR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'訊號源' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO', @level2type=N'COLUMN',@level2name=N'FSSIGNAL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'備註(放置2nd Event)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO', @level2type=N'COLUMN',@level2name=N'FSMEMO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'備註' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO', @level2type=N'COLUMN',@level2name=N'FSMEMO1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'時間類型(O:表示固定,A:表示連續)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO', @level2type=N'COLUMN',@level2name=N'FCTIMETYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主控編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO', @level2type=N'COLUMN',@level2name=N'FSVIDEO_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'檔案編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO', @level2type=N'COLUMN',@level2name=N'FSFILE_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'描述', @value=N'宣傳帶排播表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO'
GO
