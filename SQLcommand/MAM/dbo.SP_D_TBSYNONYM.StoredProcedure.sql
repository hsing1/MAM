USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <David.Sin>
-- Create date:   <2010/10/02>
-- Description:   �R���P�q��
-- =============================================

CREATE PROC [dbo].[SP_D_TBSYNONYM]
@FNNO int,
@FSSYNONYM nvarchar(100)
AS
IF @FSSYNONYM=''
BEGIN
	DELETE FROM TBSYNONYM WHERE FNNO=@FNNO
END
ELSE
BEGIN
	DELETE FROM TBSYNONYM WHERE FNNO=@FNNO AND FSSYNONYM=@FSSYNONYM
END
GO
