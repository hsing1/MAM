USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

-- Author:        <Mike.Lin>

-- Create date:   <2013/05/10>

-- Description:   修改節目播映資料,加入欄位預設秒數與訊號源

-- =============================================

CREATE Proc [dbo].[SP_U_TBPGM_BANK_DETAIL_SEG_DUR]

@FSPROG_ID	char(7),
@FNSEQNO	int,
@FSPROG_NAME	nvarchar(50),
@FNSEG	int,
@FNDUR	int,
@FNSEC	int,
@FSSIGNAL	nvarchar(50),
@FSPLAY_TYPE	char(1),
@FSUPDATED_BY	varchar(50),
@FSMEMO	nvarchar(500),
@FSMEMO1	nvarchar(500)		
As
update dbo.TBPGM_BANK_DETAIL set FSPROG_NAME=@FSPROG_NAME ,
FNSEG=@FNSEG,FNDUR=@FNDUR,FNSEC=@FNSEC,FSSIGNAL=@FSSIGNAL,FSPLAY_TYPE=@FSPLAY_TYPE,FSUPDATED_BY=@FSUPDATED_BY,
FSMEMO=@FSMEMO,FSMEMO1=@FSMEMO1,FDUPDATED_DATE=GETDATE() 
where FSPROG_ID=@FSPROG_ID and FNSEQNO=@FNSEQNO

GO
