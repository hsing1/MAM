USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_U_TBPGM_PROMO_IDENTIFIED]

@FSPROMO_ID varchar(11) ,
@FSMAIN_CHANNEL_ID char(2) ,
@FNDEP_ID int

As
UPDATE MAM.dbo.TBPGM_PROMO
SET FNDEP_ID=@FNDEP_ID,FSMAIN_CHANNEL_ID=@FSMAIN_CHANNEL_ID
WHERE  MAM.dbo.TBPGM_PROMO.FSPROMO_ID=@FSPROMO_ID
GO
