USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<kyle.Lin>
-- Create date: <2011/11/14>
-- Description:	For RPT_TBARCHIVE_02(入庫單),依年月查詢數量
-- =============================================
CREATE PROCEDURE [dbo].[SP_R_STO_ByTBZPROGSRC]
    @CREATE_BY varchar(50)	
AS
BEGIN
	declare @QUERY_BY nvarchar(50)=''
	declare @TBZTable table(
	AllTBZ nvarchar(4),
	公視 int,
	客台 int,
	原台 int,
	宏觀 int,
	總計 int
	)
	insert @TBZTable
	select TBZPROGSRC.FSPROGSRCID,0,0,0,0,0 from TBZPROGSRC

	declare CSR1 cursor for select AllTBZ,公視,客台,原台,宏觀,總計 from @TBZTable
	declare 
	@AllTBZ nvarchar(4),
	@公視 int,
	@客台 int,
	@原台 int,
	@宏觀 int,
	@總計 int

	declare @FinalTable table(
	AllTBZ nvarchar(4),
	公視 int,
	客台 int,
	原台 int,
	宏觀 int,
	總計 int,
	QUERY_BY nvarchar(50)
	)
	set @QUERY_BY = (select FSUSER_ChtName from [MAM].[dbo].[TBUSERS] where [FSUSER_ID]=@CREATE_BY)
	open csr1
		fetch next from csr1 into @AllTBZ,@公視,@客台,@原台,@宏觀,@總計
		while(@@FETCH_STATUS=0)
			begin
			if(@AllTBZ<>'')
				begin
			---Start 公視
				set @公視 = (select COUNT(*) as Total from TBLOG_VIDEO left outer join TBPROG_M  on TBLOG_VIDEO.FSID = TBPROG_M.FSPROG_ID
					left outer join TBZPROGSRC on TBPROG_M.FSPROGSRCID=TBZPROGSRC.FSPROGSRCID
					where tblog_video.FSCHANNEL_ID = '01' and TBLOG_VIDEO.FSTYPE='G' and TBPROG_M.FSPROGSRCID = @AllTBZ
					Group by TBZPROGSRC.FSPROGSRCNAME,TBPROG_M.FSPROGSRCID, TBLOG_VIDEO.FSCHANNEL_ID)  
			---
			---Start 客台
				set @客台 = (select COUNT(*) as Total from TBLOG_VIDEO left outer join TBPROG_M  on TBLOG_VIDEO.FSID = TBPROG_M.FSPROG_ID
					left outer join TBZPROGSRC on TBPROG_M.FSPROGSRCID=TBZPROGSRC.FSPROGSRCID
					where tblog_video.FSCHANNEL_ID = '07' and TBLOG_VIDEO.FSTYPE='G' and TBPROG_M.FSPROGSRCID = @AllTBZ
					Group by TBZPROGSRC.FSPROGSRCNAME,TBPROG_M.FSPROGSRCID, TBLOG_VIDEO.FSCHANNEL_ID)  
			---
			---Start 原台
				set @原台 = (select COUNT(*) as Total from TBLOG_VIDEO left outer join TBPROG_M  on TBLOG_VIDEO.FSID = TBPROG_M.FSPROG_ID
					left outer join TBZPROGSRC on TBPROG_M.FSPROGSRCID=TBZPROGSRC.FSPROGSRCID
					where tblog_video.FSCHANNEL_ID = '09' and TBLOG_VIDEO.FSTYPE='G' and TBPROG_M.FSPROGSRCID = @AllTBZ
					Group by TBZPROGSRC.FSPROGSRCNAME,TBPROG_M.FSPROGSRCID, TBLOG_VIDEO.FSCHANNEL_ID)  
			---
			---Start 宏觀
				set @宏觀 = (select COUNT(*) as Total from TBLOG_VIDEO left outer join TBPROG_M  on TBLOG_VIDEO.FSID = TBPROG_M.FSPROG_ID
					left outer join TBZPROGSRC on TBPROG_M.FSPROGSRCID=TBZPROGSRC.FSPROGSRCID
					where tblog_video.FSCHANNEL_ID = '08' and TBLOG_VIDEO.FSTYPE='G' and TBPROG_M.FSPROGSRCID = @AllTBZ
					Group by TBZPROGSRC.FSPROGSRCNAME,TBPROG_M.FSPROGSRCID, TBLOG_VIDEO.FSCHANNEL_ID)  
			---
				set @總計=@公視+@客台+@原台+@宏觀
				insert @FinalTable (AllTBZ,公視,客台,原台,宏觀,總計,QUERY_BY)
				values(@AllTBZ,ISNULL(@公視,0),ISNULL(@客台,0),ISNULL(@原台,0),ISNULL(@宏觀,0),ISNULL(@總計,0),@QUERY_BY)
				end
				fetch next from csr1 into @AllTBZ,@公視,@客台,@原台,@宏觀,@總計
			end
	close csr1
	select src.FSPROGSRCNAME as 類型,公視,客台,原台,宏觀,總計,QUERY_BY from @FinalTable left outer join [MAM].[dbo].[TBZPROGSRC] as src on src.FSPROGSRCID=AllTBZ
	
END


GO
