USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   取得短帶託播單最大編號
-- =============================================
CREATE Proc [dbo].[SP_Q_TBPGM_PROMO_BOOKING_GET_MAX_ID]
As
select ISNULL(MAX(FNPROMO_BOOKING_NO),0)+1 as NEWACTIONID from dbo.TBPGM_PROMO_BOOKING 

GO
