USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE Proc [dbo].[SP_Q_TBMODULE_CAT]
@FSMOUDULE_CAT_ID varchar(2)

As

IF @FSMOUDULE_CAT_ID='' 
BEGIN 
	select FSMOUDULE_CAT_ID,FSMODULE_CAT_NAME  
	from TBMODULE_CAT  
	ORDER BY FSMOUDULE_CAT_ID 
END 

ELSE 
BEGIN 
	select FSMOUDULE_CAT_ID,FSMODULE_CAT_NAME  
	from TBMODULE_CAT  
	WHERE FSMOUDULE_CAT_ID = @FSMOUDULE_CAT_ID
	ORDER BY FSMOUDULE_CAT_ID 
END 




GO
