USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  Proc [dbo].[SP_U_TBLOG_TEMPLATE]
@FSTABLE_TYPE char(1) ,
@FSFILE_NO varchar(16), 
@FNSEQ_NO int, 
@FSFIELD varchar(50), 
@FSFIELD_VALUE nvarchar(max) 

As

Declare @fstable_name varchar(50)  
set @fstable_name=''

 SET  @fstable_name =
 (
CASE @FSTABLE_TYPE 

WHEN 'V' THEN  'TBLOG_VIDEO_D' 

WHEN 'A' THEN  'TBLOG_AUDIO' 

WHEN 'P' THEN  'TBLOG_PHOTO' 

WHEN 'D' THEN  'TBLOG_DOC' 

ELSE  ''  END
)

declare @SQL nvarchar(MAX)  

set @SQL= 'UPDATE ' + @fstable_name + ' SET ' + @FSFIELD + ' = ''' + @FSFIELD_VALUE + ''' WHERE FSFILE_NO = ''' + @FSFILE_NO + ''' ' 

if @FSTABLE_TYPE = 'V' 
BEGIN 
 set @SQL = @SQL + ' AND FNSEQ_NO = ' + CONVERT(varchar(10), @FNSEQ_NO)
END 


exec(@SQL) 


GO
