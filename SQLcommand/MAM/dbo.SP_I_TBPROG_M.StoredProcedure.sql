USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_I_TBPROG_M]
@FSPROG_ID char(7) ,
@FSPGMNAME nvarchar(50) ,
@FSPGMENAME nvarchar(100) ,
@FSPRDDEPTID varchar(2) ,
@FSCHANNEL varchar(100) ,
@FNTOTEPISODE smallint ,
@FNLENGTH smallint ,
@FSPROGOBJID varchar(2) ,
@FSPROGSRCID varchar(2) ,
@FSPROGATTRID varchar(2) ,
@FSPROGAUDID varchar(2) ,
@FSPROGTYPEID varchar(2) ,
@FSPROGGRADEID varchar(2) ,
@FSPROGLANGID1 varchar(2) ,
@FSPROGLANGID2 varchar(2) ,
@FSPROGSALEID varchar(2) ,
@FSPROGBUYID varchar(2) ,
@FSPROGBUYDID varchar(2) ,
@FSPRDCENID varchar(5) ,
@FSPRDYYMM varchar(6) ,
@FDSHOWDATE datetime ,
@FSCONTENT nvarchar(MAX) ,
@FSMEMO nvarchar(500) ,
@FSEWEBPAGE nvarchar(MAX)  ,
@FCDERIVE varchar(1) ,
@FSWEBNAME nvarchar(100) ,
@FSCHANNEL_ID varchar(2) ,
@FSDEL char(1) ,
@FSDELUSER varchar(50) ,
@FCOUT_BUY char(1) ,
@FSCRTUSER varchar(5)  ,
@FSUPDUSER varchar(5) ,
@FNDEP_ID int,
@FSPROGSPEC varchar(100),
@FSEXPIRE_DATE_ACTION nvarchar(50),
@FDEXPIRE_DATE nvarchar(50),
@FSPROGNATIONID nvarchar(50) 

As
Insert Into TBPROG_M(FSPROG_ID,FSPGMNAME,FSPGMENAME,FSPRDDEPTID,FSCHANNEL,FNTOTEPISODE,FNLENGTH,FSPROGOBJID,FSPROGSRCID,FSPROGATTRID,FSPROGAUDID,FSPROGTYPEID,FSPROGGRADEID,FSPROGLANGID1,FSPROGLANGID2,FSPROGSALEID,FSPROGBUYID,FSPROGBUYDID,FSPRDCENID,FSPRDYYMM,FDSHOWDATE,FSCONTENT,FSMEMO,FSEWEBPAGE,FCDERIVE,FSWEBNAME,FSCHANNEL_ID,FSDEL,FSDELUSER,FCOUT_BUY,FNDEP_ID,FSPROGSPEC,FSCRTUSER,FDCRTDATE,FSUPDUSER,FDUPDDATE,FSEXPIRE_DATE_ACTION,FDEXPIRE_DATE,FSPROGNATIONID)
Values(@FSPROG_ID,@FSPGMNAME,@FSPGMENAME,@FSPRDDEPTID,@FSCHANNEL,@FNTOTEPISODE,@FNLENGTH,@FSPROGOBJID,@FSPROGSRCID,@FSPROGATTRID,@FSPROGAUDID,@FSPROGTYPEID,@FSPROGGRADEID,@FSPROGLANGID1,@FSPROGLANGID2,@FSPROGSALEID,@FSPROGBUYID,@FSPROGBUYDID,@FSPRDCENID,@FSPRDYYMM,@FDSHOWDATE,@FSCONTENT,@FSMEMO,@FSEWEBPAGE,@FCDERIVE,@FSWEBNAME,@FSCHANNEL_ID,@FSDEL,@FSDELUSER,@FCOUT_BUY,@FNDEP_ID,@FSPROGSPEC,@FSCRTUSER,GETDATE(),@FSUPDUSER,GETDATE(),@FSEXPIRE_DATE_ACTION,@FDEXPIRE_DATE,@FSPROGNATIONID)



GO
