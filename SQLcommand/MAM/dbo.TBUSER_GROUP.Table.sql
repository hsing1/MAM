USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBUSER_GROUP](
	[FSUSER_ID] [varchar](50) NOT NULL,
	[FSGROUP_ID] [varchar](20) NOT NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
	[FSUPDATED_BY] [varchar](50) NULL,
	[FDUPDATED_DATE] [datetime] NULL,
 CONSTRAINT [PK_TBUSER_GROUP] PRIMARY KEY CLUSTERED 
(
	[FSUSER_ID] ASC,
	[FSGROUP_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[TBUSER_GROUP]  WITH CHECK ADD  CONSTRAINT [FK_TBUSER_GROUP_TBGROUPS] FOREIGN KEY([FSGROUP_ID])
REFERENCES [dbo].[TBGROUPS] ([FSGROUP_ID])
GO
ALTER TABLE [dbo].[TBUSER_GROUP] CHECK CONSTRAINT [FK_TBUSER_GROUP_TBGROUPS]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用者ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_GROUP', @level2type=N'COLUMN',@level2name=N'FSUSER_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'群組ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_GROUP', @level2type=N'COLUMN',@level2name=N'FSGROUP_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_GROUP', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_GROUP', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_GROUP', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_GROUP', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
