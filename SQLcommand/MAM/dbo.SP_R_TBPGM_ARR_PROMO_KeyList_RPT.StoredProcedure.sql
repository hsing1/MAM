USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   查詢播出運行表所使用的Key報表
-- =============================================
CREATE PROCEDURE [dbo].[SP_R_TBPGM_ARR_PROMO_KeyList_RPT]
	@QUERY_KEY	VARCHAR(36)
AS
BEGIN
	SELECT
		[QUERY_KEY]
      ,[QUERY_KEY_SUBID]
      ,[QUERY_BY]
      ,[QUERY_DATE]
      ,[播出日期]
      ,[頻道]
      ,[序號]
      ,[KeyLayer]
      ,[KeyNumber]
      ,[Title]
      ,[ID]
     
	FROM
		RPT_TBPGM_ARR_PROMO_KeyList
	WHERE
		(QUERY_KEY = @QUERY_KEY)
	ORDER BY
		[KeyLayer], [KeyNumber]
END
GO
