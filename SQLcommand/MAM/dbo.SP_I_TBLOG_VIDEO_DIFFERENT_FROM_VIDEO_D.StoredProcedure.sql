USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_I_TBLOG_VIDEO_DIFFERENT_FROM_VIDEO_D]
@FSFILE_NO char(16)
AS

INSERT INTO TBLOG_VIDEO_DIFFERENT(MODE,FSFILE_NO)
(SELECT 1,FSFILE_NO
	FROM TBLOG_VIDEO_D WHERE FSFILE_NO=@FSFILE_NO)
GO
