USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        
-- Create date: 
-- Description:   根據類型、節目子集資料、送帶轉檔單單號查詢入庫檔案資料
-- Update desc:    <2013/07/03><Jarvis.Yang>加入註解。
-- =============================================
CREATE Proc [dbo].[SP_Q_TBLOG_VIDEO_By_Type_EPISODE] --'','','','','201606080035'
@FSTYPE	char(1),
@FSID	varchar(11),
@FNEPISODE	smallint,
@FSARC_TYPE char(3),
@FSBRO_ID char(12)


As
select ISNULL(b.FSUSER_ChtName,'') as FSCREATED_BY_NAME, ISNULL(C.FSUSER_ChtName,'') as FSUPDATED_BY_NAME ,
ISNULL(CONVERT(VARCHAR(20) ,a.FDCREATED_DATE , 111),'') as WANT_FDCREATEDDATE,
ISNULL(CONVERT(VARCHAR(20) ,a.FDUPDATED_DATE , 111),'') as WANT_FDUPDATEDDATE,
bro.FCCHECK_STATUS as 'Brocast_Status',
a.* FROM TBLOG_VIDEO  as a  
               left outer JOIN TBUSERS as b on a.FSCREATED_BY=b.FSUSER_ID
               left outer join TBUSERS as c on a.FSUPDATED_BY=c.FSUSER_ID
			   left outer join TBBROADCAST bro on bro.FSBRO_ID =a.FSBRO_ID
where 
a.FSID = @FSID 
and a.FNEPISODE = @FNEPISODE 
and a.FSTYPE = @FSTYPE
and a.FSARC_TYPE=case when  @FSARC_TYPE='' then a.FSARC_TYPE else @FSARC_TYPE end
and a.FSBRO_ID=case when @FSBRO_ID='' then a.FSBRO_ID else @FSBRO_ID end
GO
