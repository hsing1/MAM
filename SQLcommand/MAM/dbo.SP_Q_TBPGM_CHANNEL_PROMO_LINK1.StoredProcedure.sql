USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   查詢頻道與短帶連結資料(只有第六頻道)(目前應已未使用)
-- =============================================
CREATE Proc [dbo].[SP_Q_TBPGM_CHANNEL_PROMO_LINK1]


As
SELECT A.FSPROMO_ID,A.FSCHANNEL_ID,A.FNNO,A.FCLINK_TYPE,A.FCTIME_TYPE,
A.FCINSERT_TYPE,B.FSPROMO_NAME,C.FSCHANNEL_NAME
from dbo.TBPGM_CHANNEL_PROMO_LINK A,TBPGM_PROMO B,TBZCHANNEL C
where A.FSPROMO_ID=B.FSPROMO_ID and A.FSCHANNEL_ID=C.FSCHANNEL_ID 
and A.FSCHANNEL_ID='06'
order by A.FNNO






GO
