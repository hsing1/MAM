USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Q_REVIEWLIST_GPD_NAME] 
@FSNAME nvarchar(50)

As

--declare @FSNAME nvarchar(100)
--set @FSNAME='波西'
  declare @GPDTable table(GPD nvarchar(100),GPD_Name nvarchar(100), FSID nvarchar(100) ,FSNAME nvarchar(100))
  
  insert @GPDTable
  select 'G' as GPD,'節目' as GPD_Name, FSPROG_ID as FSID, FSPGMNAME as FSNAME from MAM.dbo.TBPROG_M where [FSPGMNAME] like '%'+@FSNAME+'%'
  
  insert @GPDTable
  select 'P' as GPD,'PROMO' as GPD_Name,FSPROMO_ID as FSID, FSPROMO_NAME as FSNAME from MAM.dbo.TBPGM_PROMO where FSPROMO_NAME like '%'+@FSNAME+'%'
  
  insert @GPDTable
  select 'D' as GPD,'資料' as GPD_Name,FSDATA_ID as FSID,FSDATA_NAME as FSNAME from MAM.dbo.TBDATA where FSDATA_NAME like '%'+@FSNAME+'%'
  
  select * from @GPDTable

GO
