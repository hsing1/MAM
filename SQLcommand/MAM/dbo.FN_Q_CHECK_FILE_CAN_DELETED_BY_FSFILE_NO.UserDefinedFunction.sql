USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO










CREATE FUNCTION [dbo].[FN_Q_CHECK_FILE_CAN_DELETED_BY_FSFILE_NO](@FSFILE_NO char(16), @FSTBNAME char(1))
RETURNS NVARCHAR(50)
AS
BEGIN
	--節目/短帶
	DECLARE @FSTYPE char(1)
	
	IF @FSTBNAME = 'V'
	BEGIN
		SELECT @FSTYPE = [FSTYPE] FROM [dbo].[TBLOG_VIDEO] WHERE FSFILE_NO = @FSFILE_NO 
	END
	
	--錯誤訊息
	DECLARE @RESULT nvarchar(50)
	SET @RESULT = ''
	
	--若為'音','圖','文' 則不判斷是否有排播問題
	IF @FSTBNAME = 'A' OR @FSTBNAME = 'P' OR @FSTBNAME = 'D'
	BEGIN
		SET @RESULT = ''
		RETURN @RESULT
	END
	ELSE
	BEGIN
	
		--未來有排播的不可刪除
		--一個月內已播的不可刪除
	
		DECLARE @VIDEO_ID char(8)
		SET @VIDEO_ID = ''
	
		--取得Video Id
		SELECT @VIDEO_ID = [FSVIDEO_PROG] FROM [dbo].[TBLOG_VIDEO] WHERE FSFILE_NO = @FSFILE_NO
	
		IF @FSTYPE = 'G'
		BEGIN
			--節目
			IF EXISTS(SELECT [FSPROG_ID] FROM [dbo].[TBPGM_COMBINE_QUEUE] 
				WHERE [FSVIDEO_ID] = @VIDEO_ID AND ([FDDATE] >= CONVERT(VARCHAR(10),GETDATE(),121)))
			BEGIN
				SET @RESULT = '節目有排播，不可刪除!'
			END
			ELSE
			BEGIN
				SET @RESULT = ''
			END
	
			
		END
		ELSE IF @FSTYPE = 'P'
		BEGIN
			--短帶
			IF EXISTS(SELECT [FSPROMO_ID] FROM [dbo].[TBPGM_ARR_PROMO] 
				WHERE [FSVIDEO_ID] = @VIDEO_ID AND ([FDDATE] >= CONVERT(VARCHAR(10),GETDATE(),121)))
			BEGIN
				SET @RESULT = '短帶有排播，不可刪除!'
			END
			ELSE
			BEGIN
				SET @RESULT = ''
			END
		END
		ELSE
		BEGIN
			SET @RESULT = ''
		END
	END

	RETURN @RESULT
END










GO
