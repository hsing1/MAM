USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Q_GET_PROG_DEPT_SUPERVISOR_EMAIL]
@FSFILE_NO varchar(16)
AS

select FSUSER_ChtName,FSEMAIL from TBUSERS where FSUSER_ID=
(select FSSupervisor_ID from TBUSER_DEP where FNDEP_ID=
(select FNDEP_ID from TBPROG_M where FSPROG_ID=
(select FSID from TBLOG_VIDEO where FSSUPERVISOR='Y' And FSFILE_NO=@FSFILE_NO)))
UNION
select FSUSER_ChtName,FSEMAIL from TBUSERS where FSUSER_ID=
(select FSSupervisor_ID from TBUSER_DEP where FNDEP_ID=
(select FNDEP_ID from TBPROG_M where FSPROG_ID=
(select FSID from TBLOG_AUDIO where FSSUPERVISOR='Y' And FSFILE_NO=@FSFILE_NO)))
UNION
select FSUSER_ChtName,FSEMAIL from TBUSERS where FSUSER_ID=
(select FSSupervisor_ID from TBUSER_DEP where FNDEP_ID=
(select FNDEP_ID from TBPROG_M where FSPROG_ID=
(select FSID from TBLOG_PHOTO where FSSUPERVISOR='Y' And FSFILE_NO=@FSFILE_NO)))
UNION
select FSUSER_ChtName,FSEMAIL from TBUSERS where FSUSER_ID=
(select FSSupervisor_ID from TBUSER_DEP where FNDEP_ID=
(select FNDEP_ID from TBPROG_M where FSPROG_ID=
(select FSID from TBLOG_DOC where FSSUPERVISOR='Y' And FSFILE_NO=@FSFILE_NO)))
GO
