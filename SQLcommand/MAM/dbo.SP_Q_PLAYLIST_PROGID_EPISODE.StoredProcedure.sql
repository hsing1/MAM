USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   查詢節目集數的未來排播狀況
-- =============================================
CREATE Proc [dbo].[SP_Q_PLAYLIST_PROGID_EPISODE]

@FSPROG_ID	varchar(7),
@FNEPISODE	int

As
select A.FDDATE,A.FSCHANNEL_ID,B.FSCHANNEL_NAME,A.FSCREATED_BY,C.FSUSER_ChtName
from TBPGM_COMBINE_QUEUE A
left join TBZCHANNEL B on  A.FSCHANNEL_ID=B.FSCHANNEL_ID
left join TBUSERS C on A.FSCREATED_BY=C.FSUSER_ID
where A.FDDATE >=GETDATE() and A.FSPROG_ID=@FSPROG_ID and A.FNEPISODE=@FNEPISODE

GO
