USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[FN_Q_GET_XML_BY_FILTER](@XML varchar(max)) 
RETURNS nvarchar(max)
AS
BEGIN
	SET @XML = REPLACE(@XML,'<','&lt;')
	SET @XML = REPLACE(@XML,'>','&gt;')
	SET @XML = REPLACE(@XML,'&','&amp;')
	SET @XML = REPLACE(@XML,'''','&apos;')
	SET @XML = REPLACE(@XML,'"','&quot;') 
	
	RETURN @XML
End 

GO
