USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   新增頻道與短帶連結資料
-- =============================================
CREATE Proc [dbo].[SP_I_TBPGM_CHANNEL_PROMO_LINK]

@FSPROMO_ID	varchar(12),
@FSCHANNEL_ID	char(2),
@FNNO	int,
@FCLINK_TYPE	char(1),
@FCTIME_TYPE	char(1),
@FCINSERT_TYPE	char(1),
@FSCREATED_BY	varchar(50),
@FSUPDATED_BY	varchar(50)
	
As
Insert Into dbo.TBPGM_CHANNEL_PROMO_LINK(FSPROMO_ID,FSCHANNEL_ID,FNNO,FCLINK_TYPE,FCTIME_TYPE,
FCINSERT_TYPE,FSCREATED_BY,FDCREATED_DATE,FSUPDATED_BY,FDUPDATED_DATE)
Values(@FSPROMO_ID,@FSCHANNEL_ID,@FNNO,@FCLINK_TYPE,@FCTIME_TYPE,
@FCINSERT_TYPE,@FSCREATED_BY,GETDATE(),@FSUPDATED_BY,GETDATE())







GO
