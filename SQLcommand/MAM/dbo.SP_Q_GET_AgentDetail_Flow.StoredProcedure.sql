USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_Q_GET_AgentDetail_Flow]
@aid varchar(50)
AS

SELECT [AgentDetailid]
      ,[aid]
      ,[eflowid]
  FROM [PTSFlow].[dbo].[AgentDetail_Flow] 
  where aid=@aid

GO
