USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Proc [dbo].[SP_U_TBUSERS_TreeViewDept]
      @USER_ID nvarchar(50),
      @UPDATE_BY nvarchar(50),
      @USER_DEP_ID nvarchar(50)
AS
UPDATE [MAM].[dbo].[TBUSERS] 
SET   FNTreeViewDept=@USER_DEP_ID
      ,FSUPDATED_BY=@UPDATE_BY
      ,FDUPDATED_DATE=GETDATE()

WHERE FSUSER_ID=@USER_ID
GO
