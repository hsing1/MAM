USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Sam.wu>
-- Create date: <2011/09/30>
-- Description:	<根據傳入之TBProposal(成案單檔)之FSPROGSPEC(節目規格)代碼串,
--				比對TBZPROGSPEC(節目規格代碼檔)後,取回FSPROGSPECNAME(節目規格名稱)名稱串>
-- =============================================
CREATE FUNCTION [dbo].[FN_Q_TBPROPOSAL_GETNAMELISTBYCODE_PROGSPEC]
(
	@FSPROGSPEC	VARCHAR(100)
)
RETURNS	VARCHAR(500)
AS
BEGIN
	-- Declare the return variable here
		Declare @cnt int, @idx int, @i int, @result VARCHAR(500)

	-- Add the T-SQL statements to compute the return value here
		SELECT @cnt = 0, @idx = 0 , @i = 0, @result = ''

		While @idx < Len(@FSPROGSPEC)
		BEGIN
			IF CHARINDEX(';',SUBSTRING(@FSPROGSPEC,@idx,Len(@FSPROGSPEC)-@idx))<>0
				BEGIN
					Set @idx = @idx + CHARINDEX(';',SUBSTRING(@FSPROGSPEC,@idx,Len(@FSPROGSPEC)-@idx))
					Set @cnt = @cnt + 1
				END
			ELSE
				BEGIN
					Set @idx = Len(@FSPROGSPEC)
				END
		END

		While @i < @cnt +1
		BEGIN 
			Declare @Code nvarchar(20)
			Declare @Name nvarchar(100)
			
			Set @Code = dbo.FN_Q_GETITEMBYINDEX(@FSPROGSPEC,@i)
			
			IF(@Code <> '')
			 BEGIN
			     Set @Name = ISNULL((SELECT FSPROGSPECNAME FROM TBZPROGSPEC WHERE FSPROGSPECID = @Code),@Code)
			     Set @result = @result + @Name + ';'
		     END
			
			Set @i = @i + 1
		END

	-- Return the result of the function
		RETURN @result
END

GO
