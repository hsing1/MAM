USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Mike  
-- Create date: 2016/01/04
-- Description: 依VIDEOID取得檔案狀態,先查詢是否多於一筆,如果多於一筆則過濾掉審核不通過的資料
-- Update desc: <Mike>加入註解。
-- =============================================
CREATE PROC [dbo].[SP_Q_GET_HD_VIDEO_STATUS]
@FSVIDEO_ID	VARCHAR(8)
AS

IF ((select COUNT(*) from (select distinct B.FSFILE_NO,B.FSVIDEO_PROG FSVIDEO_ID,B.FCFILE_STATUS,C.FCCHECK_STATUS,D.FCSTATUS,C.FSBRO_ID
from TBLOG_Video B 
left join tbbroadcast C on B.FSBRO_ID=C.FSBRO_ID
left join TBLOG_Video_HD_MC D on B.FSFILE_NO=D.FSFILE_NO 
where 
B.FSVIDEO_PROG=@FSVIDEO_ID and B.FCFILE_STATUS not in ('D','X')) as a) >1)
(
select distinct B.FSFILE_NO,B.FSVIDEO_PROG FSVIDEO_ID,B.FCFILE_STATUS,C.FCCHECK_STATUS,D.FCSTATUS,C.FSBRO_ID
from TBLOG_Video B 
left join tbbroadcast C on B.FSBRO_ID=C.FSBRO_ID
left join TBLOG_Video_HD_MC D on B.FSFILE_NO=D.FSFILE_NO 
where 
 B.FSVIDEO_PROG=@FSVIDEO_ID and B.FCFILE_STATUS not in ('D','X','F')
)
else
(
select distinct B.FSFILE_NO,B.FSVIDEO_PROG FSVIDEO_ID,B.FCFILE_STATUS,C.FCCHECK_STATUS,D.FCSTATUS,C.FSBRO_ID
from TBLOG_Video B 
left join tbbroadcast C on B.FSBRO_ID=C.FSBRO_ID
left join TBLOG_Video_HD_MC D on B.FSFILE_NO=D.FSFILE_NO 
where 
 B.FSVIDEO_PROG=@FSVIDEO_ID and B.FCFILE_STATUS not in ('D','X')
)
GO
