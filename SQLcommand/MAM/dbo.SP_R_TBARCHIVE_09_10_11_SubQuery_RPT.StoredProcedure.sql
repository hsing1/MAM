USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_R_TBARCHIVE_09_10_11_SubQuery_RPT] --'G200628205620002','0'
@FSFILE_NO  char(16),
@page_no int

AS
BEGIN

declare  @temp_Table  table
(
ID int,
FSFILE_NO char(16),
FNSEG_ID char(2),
FSVIDEO_ID varchar(10),
FSBEG_TIMECODE char(11),
FSEND_TIMECODE char(11),
FCLOW_RES char(1),
FNFILE_SIZE bigint,
FDDISPLAY_TIME datetime,
FSDISPLAY_CHANNEL_ID char(2),
FCMSS_QC char(1),
FSMSS_QC_MSG nvarchar(100),
FSCREATED_BY varchar(50),
FDCREATED_DATE datetime,
FSUPDATED_BY varchar(50),
FDUPDATED_DATE datetime

)

	select IDENTITY(INT,1,1) as ID,* into #temp_Table from TBLOG_VIDEO_SEG seg
	where FSFILE_NO=@FSFILE_NO


	select 	
	ISNULL(seg.FSBEG_TIMECODE,'00:00:00:00') as 'TImecode_�_'
	,ISNULL(seg.FSEND_TIMECODE,'00:00:00:00') as 'Timecode_��'
	,dbo.CaculateDuration(ISNULL( seg.FSBEG_TIMECODE,'00:00:00:00'),ISNULL(seg.FSEND_TIMECODE,'00:00:00:00')) as '���'
 from #temp_Table seg
where  	ID>=@page_no*5+1 and ID<=@page_no*5+5
--UNION ALL
--SELECT TOP(5-(SELECT COUNT(*) FROM #temp_Table WHERE ID>=@page_no*5+1 and ID<=@page_no*5+5))
--NULL, NULL, NULL
--FROM #temp_Table
--where 'TImecode_�_' is not null
END

GO
