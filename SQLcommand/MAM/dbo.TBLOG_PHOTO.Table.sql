USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBLOG_PHOTO](
	[FSFILE_NO] [char](16) NOT NULL,
	[FSSUBJECT_ID] [char](12) NOT NULL,
	[FSTYPE] [char](1) NOT NULL,
	[FSID] [varchar](11) NOT NULL,
	[FNEPISODE] [nvarchar](50) NOT NULL,
	[FSSUPERVISOR] [char](1) NULL,
	[FSARC_TYPE] [varchar](3) NULL,
	[FSFILE_TYPE] [varchar](5) NULL,
	[FSTITLE] [nvarchar](100) NULL,
	[FSDESCRIPTION] [nvarchar](200) NULL,
	[FSFILE_SIZE] [nvarchar](50) NULL,
	[FNWIDTH] [int] NULL,
	[FNHEIGHT] [int] NULL,
	[FNDPI] [int] NULL,
	[FCFILE_STATUS] [char](1) NULL,
	[FSOLD_FILE_NAME] [nvarchar](100) NULL,
	[FSCHANGE_FILE_NO] [varchar](20) NULL,
	[FSFILE_PATH] [nvarchar](100) NULL,
	[FNDIR_ID] [bigint] NULL,
	[FSCHANNEL_ID] [char](2) NULL,
	[FSARC_ID] [char](12) NULL,
	[FCMETA] [char](1) NULL,
	[FSMETA_PATH] [nvarchar](500) NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
	[FSUPDATED_BY] [varchar](50) NULL,
	[FDUPDATED_DATE] [datetime] NULL,
	[FSATTRIBUTE1] [nvarchar](max) NULL,
	[FSATTRIBUTE2] [nvarchar](max) NULL,
	[FSATTRIBUTE3] [nvarchar](max) NULL,
	[FSATTRIBUTE4] [nvarchar](max) NULL,
	[FSATTRIBUTE5] [nvarchar](max) NULL,
	[FSATTRIBUTE6] [nvarchar](max) NULL,
	[FSATTRIBUTE7] [nvarchar](max) NULL,
	[FSATTRIBUTE8] [nvarchar](max) NULL,
	[FSATTRIBUTE9] [nvarchar](max) NULL,
	[FSATTRIBUTE10] [nvarchar](max) NULL,
	[FSATTRIBUTE11] [nvarchar](max) NULL,
	[FSATTRIBUTE12] [nvarchar](max) NULL,
	[FSATTRIBUTE13] [nvarchar](max) NULL,
	[FSATTRIBUTE14] [nvarchar](max) NULL,
	[FSATTRIBUTE15] [nvarchar](max) NULL,
	[FSATTRIBUTE16] [nvarchar](max) NULL,
	[FSATTRIBUTE17] [nvarchar](max) NULL,
	[FSATTRIBUTE18] [nvarchar](max) NULL,
	[FSATTRIBUTE19] [nvarchar](max) NULL,
	[FSATTRIBUTE20] [nvarchar](max) NULL,
	[FSATTRIBUTE21] [nvarchar](max) NULL,
	[FSATTRIBUTE22] [nvarchar](max) NULL,
	[FSATTRIBUTE23] [nvarchar](max) NULL,
	[FSATTRIBUTE24] [nvarchar](max) NULL,
	[FSATTRIBUTE25] [nvarchar](max) NULL,
	[FSATTRIBUTE26] [nvarchar](max) NULL,
	[FSATTRIBUTE27] [nvarchar](max) NULL,
	[FSATTRIBUTE28] [nvarchar](max) NULL,
	[FSATTRIBUTE29] [nvarchar](max) NULL,
	[FSATTRIBUTE30] [nvarchar](max) NULL,
	[FSATTRIBUTE31] [nvarchar](max) NULL,
	[FSATTRIBUTE32] [nvarchar](max) NULL,
	[FSATTRIBUTE33] [nvarchar](max) NULL,
	[FSATTRIBUTE34] [nvarchar](max) NULL,
	[FSATTRIBUTE35] [nvarchar](max) NULL,
	[FSATTRIBUTE36] [nvarchar](max) NULL,
	[FSATTRIBUTE37] [nvarchar](max) NULL,
	[FSATTRIBUTE38] [nvarchar](max) NULL,
	[FSATTRIBUTE39] [nvarchar](max) NULL,
	[FSATTRIBUTE40] [nvarchar](max) NULL,
	[FSATTRIBUTE41] [nvarchar](max) NULL,
	[FSATTRIBUTE42] [nvarchar](max) NULL,
	[FSATTRIBUTE43] [nvarchar](max) NULL,
	[FSATTRIBUTE44] [nvarchar](max) NULL,
	[FSATTRIBUTE45] [nvarchar](max) NULL,
	[FSATTRIBUTE46] [nvarchar](max) NULL,
	[FSATTRIBUTE47] [nvarchar](max) NULL,
	[FSATTRIBUTE48] [nvarchar](max) NULL,
	[FSATTRIBUTE49] [nvarchar](max) NULL,
	[FSATTRIBUTE50] [nvarchar](max) NULL,
	[FSEXIF] [nvarchar](max) NULL,
 CONSTRAINT [ TBLOG_PHOTO] PRIMARY KEY CLUSTERED 
(
	[FSFILE_NO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
CREATE NONCLUSTERED INDEX [INDEX_FCFILE_STATUS] ON [dbo].[TBLOG_PHOTO]
(
	[FCFILE_STATUS] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDEX_FDCREATED_DATE] ON [dbo].[TBLOG_PHOTO]
(
	[FDCREATED_DATE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
CREATE NONCLUSTERED INDEX [INDEX_FSID_FNEPISODE] ON [dbo].[TBLOG_PHOTO]
(
	[FSID] ASC,
	[FNEPISODE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
CREATE NONCLUSTERED INDEX [INDEX_FSTYPE] ON [dbo].[TBLOG_PHOTO]
(
	[FSTYPE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TBLOG_PHOTO] ADD  CONSTRAINT [DF_TBLOG_PHOTO_FSSUPERVISOR]  DEFAULT ('N') FOR [FSSUPERVISOR]
GO
ALTER TABLE [dbo].[TBLOG_PHOTO] ADD  CONSTRAINT [DF_TBLOG_PHOTO_FSTITLE]  DEFAULT ('') FOR [FSTITLE]
GO
ALTER TABLE [dbo].[TBLOG_PHOTO] ADD  CONSTRAINT [DF_TBLOG_PHOTO_FSDESCRIPTION]  DEFAULT ('') FOR [FSDESCRIPTION]
GO
ALTER TABLE [dbo].[TBLOG_PHOTO] ADD  CONSTRAINT [DF_TBLOG_PHOTO_FSUPDATED_BY]  DEFAULT ('') FOR [FSUPDATED_BY]
GO
ALTER TABLE [dbo].[TBLOG_PHOTO] ADD  CONSTRAINT [DF_TBLOG_PHOTO_FDUPDATED_DATE]  DEFAULT (getdate()) FOR [FDUPDATED_DATE]
GO
ALTER TABLE [dbo].[TBLOG_PHOTO]  WITH CHECK ADD  CONSTRAINT [FK_TBLOG_PHOTO_TBFILE_TYPE] FOREIGN KEY([FSARC_TYPE])
REFERENCES [dbo].[TBFILE_TYPE] ([FSARC_TYPE])
GO
ALTER TABLE [dbo].[TBLOG_PHOTO] CHECK CONSTRAINT [FK_TBLOG_PHOTO_TBFILE_TYPE]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'檔案編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_PHOTO', @level2type=N'COLUMN',@level2name=N'FSFILE_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用於節點用的編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_PHOTO', @level2type=N'COLUMN',@level2name=N'FSSUBJECT_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目或是PROMO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_PHOTO', @level2type=N'COLUMN',@level2name=N'FSTYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目或PROMO編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_PHOTO', @level2type=N'COLUMN',@level2name=N'FSID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'子集編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_PHOTO', @level2type=N'COLUMN',@level2name=N'FNEPISODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'簽核時是否需原成案單位主管簽核' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_PHOTO', @level2type=N'COLUMN',@level2name=N'FSSUPERVISOR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'影帶類型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_PHOTO', @level2type=N'COLUMN',@level2name=N'FSARC_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'檔案類型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_PHOTO', @level2type=N'COLUMN',@level2name=N'FSFILE_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'抬頭' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_PHOTO', @level2type=N'COLUMN',@level2name=N'FSTITLE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_PHOTO', @level2type=N'COLUMN',@level2name=N'FSDESCRIPTION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'檔案大小' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_PHOTO', @level2type=N'COLUMN',@level2name=N'FSFILE_SIZE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'圖片寬' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_PHOTO', @level2type=N'COLUMN',@level2name=N'FNWIDTH'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'圖片高度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_PHOTO', @level2type=N'COLUMN',@level2name=N'FNHEIGHT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'圖片解析度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_PHOTO', @level2type=N'COLUMN',@level2name=N'FNDPI'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'檔案狀態' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_PHOTO', @level2type=N'COLUMN',@level2name=N'FCFILE_STATUS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'上傳前檔案名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_PHOTO', @level2type=N'COLUMN',@level2name=N'FSOLD_FILE_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'要置換的檔案名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_PHOTO', @level2type=N'COLUMN',@level2name=N'FSCHANGE_FILE_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TSM路徑' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_PHOTO', @level2type=N'COLUMN',@level2name=N'FSFILE_PATH'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'所屬Queue代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_PHOTO', @level2type=N'COLUMN',@level2name=N'FNDIR_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'所屬頻道' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_PHOTO', @level2type=N'COLUMN',@level2name=N'FSCHANNEL_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'入庫單號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_PHOTO', @level2type=N'COLUMN',@level2name=N'FSARC_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立者的ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_PHOTO', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_PHOTO', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_PHOTO', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_PHOTO', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
