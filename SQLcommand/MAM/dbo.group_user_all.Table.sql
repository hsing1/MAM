USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[group_user_all](
	[頻道/部門/組別] [nvarchar](255) NULL,
	[姓名] [nvarchar](255) NULL,
	[成案] [nvarchar](255) NULL,
	[檢索調用] [nvarchar](255) NULL,
	[子集維護] [nvarchar](255) NULL,
	[節目相關人員] [nvarchar](255) NULL,
	[節目入庫管理] [nvarchar](255) NULL,
	[參展記錄] [nvarchar](255) NULL,
	[送帶轉檔及入庫] [nvarchar](255) NULL,
	[轉檔管理相關權限] [nvarchar](255) NULL,
	[公視製播] [nvarchar](255) NULL,
	[參展版本] [nvarchar](255) NULL,
	[公視代製] [nvarchar](255) NULL,
	[宏觀製播] [nvarchar](255) NULL,
	[客家製播] [nvarchar](255) NULL,
	[原民台製播] [nvarchar](255) NULL,
	[DVB-H 製播] [nvarchar](255) NULL
) ON [PRIMARY]

GO
