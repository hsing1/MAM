USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   查詢頻道與短帶連結資料
-- =============================================
CREATE Proc [dbo].[SP_Q_TBPGM_CHANNEL_PROMO_LINK]

@FSCHANNEL_ID	varchar(2),
@FCLINK_TYPE	char(1),
@FCTIME_TYPE	char(1),
@FCINSERT_TYPE	char(1)
	
As
SELECT A.FSPROMO_ID,A.FSCHANNEL_ID,A.FNNO,A.FCLINK_TYPE,A.FCTIME_TYPE,
A.FCINSERT_TYPE,B.FSPROMO_NAME,C.FSCHANNEL_NAME,B.FSDURATION
from dbo.TBPGM_CHANNEL_PROMO_LINK A,TBPGM_PROMO B,TBZCHANNEL C
where A.FSPROMO_ID=B.FSPROMO_ID and A.FSCHANNEL_ID=C.FSCHANNEL_ID 
and @FSCHANNEL_ID = case when @FSCHANNEL_ID = '' then @FSCHANNEL_ID else A.FSCHANNEL_ID end
and @FCLINK_TYPE = case when @FCLINK_TYPE = '' then @FCLINK_TYPE else A.FCLINK_TYPE end
and @FCTIME_TYPE = case when @FCTIME_TYPE = '' then @FCTIME_TYPE else A.FCTIME_TYPE end
and @FCINSERT_TYPE = case when @FCINSERT_TYPE = '' then @FCINSERT_TYPE else A.FCINSERT_TYPE end
order by A.FNNO






GO
