USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        
-- Create date: 
-- Description: 依頻道與日期取得節目表的檔案狀態,PGM145使用
-- Update desc:    <2016/01/27><Mike>加入註解。
-- =============================================
CREATE PROC [dbo].[SP_Q_GET_HD_PLAYLIST_FILE_STATUS]
@FDDATE	date,
@FSCHANNEL_ID	varchar(2)
AS

select A.FNCONBINE_NO ORNO,A.FSPROG_ID,A.FNEPISODE,A.FSPROG_NAME,A.FNBREAK_NO,A.FCPROPERTY,A.fsplay_time,A.FSDUR,A.FNLIVE,A.FSFILE_NO,A.FSVIDEO_ID,A.FSMEMO,G.FCCHECK_STATUS,C.FCFILE_STATUS,D.FCSTATUS,F.FSBEG_TIMECODE,F.FSEND_TIMECODE 
from tbpgm_combine_Queue A
left join TBZCHANNEL B on A.FSCHANNEL_ID=B.FSCHANNEL_ID
left join TBLOG_VIDEO C on A.FSFILE_NO=C.FSFILE_NO   and C.FCFILE_STATUS not in('D','X')
left join TBLOG_Video_HD_MC D on C.FSFILE_NO=D.FSFILE_NO 
LEFT join TBLOG_VIDEO_SEG F on C.FSFILE_NO=F.FSFILE_NO and A.FNBREAK_NO= F.FNSEG_ID
left join TBBROADCAST G on C.FSBRO_ID=G.FSBRO_ID
 where A.fddate=@FDDATE and A.fschannel_id=@FSCHANNEL_ID 
union
select A.FNARR_PROMO_NO ORNO,A.FSPROMO_ID,'',E.FSPROMO_NAME,'','P',A.fsplaytime as fsplay_time,A.FSDUR,'',A.FSFILE_NO,A.FSVIDEO_ID,A.FSMEMO,G.FCCHECK_STATUS,C.FCFILE_STATUS,D.FCSTATUS,F.FSBEG_TIMECODE,F.FSEND_TIMECODE   
from tbpgm_Arr_promo A
left join TBZCHANNEL B on A.FSCHANNEL_ID=B.FSCHANNEL_ID
left join TBLOG_VIDEO C on A.FSFILE_NO=C.FSFILE_NO  and C.FCFILE_STATUS not in('D','X')
left join TBLOG_Video_HD_MC D on C.FSFILE_NO=D.FSFILE_NO 
left join TBPGM_PROMO E on A.FSPROMO_ID=E.FSPROMO_ID
LEFT join TBLOG_VIDEO_SEG F on C.FSFILE_NO=F.FSFILE_NO and F.FNSEG_ID='01'
left join TBBROADCAST G on C.FSBRO_ID=G.FSBRO_ID
 where A.fddate=@FDDATE and A.fschannel_id=@FSCHANNEL_ID
  order by ORNO
GO
