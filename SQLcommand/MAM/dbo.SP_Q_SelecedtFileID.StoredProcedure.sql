USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jarvis.Yang
-- Create date: 2013-07-09
-- Description:	為了入庫MetaData查詢ID，因為要完全一樣,不能用like所以新建一個
-- =============================================
create PROCEDURE [dbo].[SP_Q_SelecedtFileID]
	-- Add the parameters for the stored procedure here
	@FSPGMNAME nvarchar(50) ,
	@CatalogID char
AS
BEGIN
if(@CatalogID='G')
	begin
	select FSPROG_ID as 'ID' from TBPROG_M where FSPGMNAME=@FSPGMNAME
	end
else if(@CatalogID='P')
begin
	select FSPROMO_ID as 'ID' from TBPGM_PROMO where FSPROMO_NAME=@FSPGMNAME

end
else if(@CatalogID='D')
begin
	select FSDATA_ID as 'ID' from TBDATA where FSDATA_NAME=@FSPGMNAME
end
END




GO
