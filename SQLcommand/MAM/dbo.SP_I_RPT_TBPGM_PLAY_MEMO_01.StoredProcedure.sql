USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   新增播出提示報表資料(應已未使用)
-- =============================================
CREATE Proc [dbo].[SP_I_RPT_TBPGM_PLAY_MEMO_01]

@QUERY_KEY	uniqueidentifier,
@QUERY_KEY_SUBID	uniqueidentifier,
@QUERY_BY	varchar(50),
@頻道名稱	nvarchar(50),
@播出日期	datetime,
@節目編碼	nvarchar(7),
@播映序號	int,
@節目名稱	nvarchar(50),
@短帶名稱	nvarchar(50),
@主控鏡面	nvarchar(200),
@CH1	nvarchar(20),
@CH2	nvarchar(20),
@CH3	nvarchar(20),
@CH4	nvarchar(20)
	
As
Insert Into dbo.RPT_TBPGMPLAYMEMO_01
(QUERY_KEY,QUERY_KEY_SUBID,QUERY_BY,QUERY_DATE,
頻道名稱,播出日期,節目編碼,播映序號,節目名稱,
短帶名稱,主控鏡面,CH1,CH2,CH3,CH4)
Values(@QUERY_KEY,@QUERY_KEY_SUBID,@QUERY_BY,GETDATE(),
@頻道名稱,@播出日期,@節目編碼,@播映序號,@節目名稱,
@短帶名稱,@主控鏡面,@CH1,@CH2,@CH3,@CH4)
GO
