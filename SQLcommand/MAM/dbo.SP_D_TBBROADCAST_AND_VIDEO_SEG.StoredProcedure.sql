USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[SP_D_TBBROADCAST_AND_VIDEO_SEG]
	@FSBRO_ID char(12)
AS
BEGIN
    delete TBBROADCAST where FSBRO_ID=@FSBRO_ID
	delete TBLOG_VIDEO_SEG where FSFILE_NO in( select FSFILE_NO from TBLOG_VIDEO where FSBRO_ID=@FSBRO_ID)
	delete TBLOG_VIDEO where FSBRO_ID=@FSBRO_ID
END

GO
