USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[SP_Q_TBLOG_VIDEO_BY_ARCTPYE_STATUS] --'026','B'
	@FSARC_TYPE varchar(3),
	@FCFILE_STATUS varchar(1)
AS
BEGIN
	select video.*,bro.fsmemo as 'FSMEMO' from TBLOG_VIDEO  as video
	join tbbroadcast as bro on video.FSBRO_ID=bro.FSBRO_ID
	where FSARC_TYPE=@FSARC_TYPE and FCFILE_STATUS=@FCFILE_STATUS and FCCHECK_STATUS='K'
END

GO
