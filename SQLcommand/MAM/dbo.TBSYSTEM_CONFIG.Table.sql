USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBSYSTEM_CONFIG](
	[FSSYSTEM_CONFIG] [nvarchar](max) NOT NULL,
	[FDUPDATED_DATE] [datetime] NULL,
	[FDCREATED_DATE] [datetime] NULL,
	[FSCREATED_BY] [varchar](50) NULL,
	[FSUPDATED_BY] [varchar](50) NULL,
	[FXSYSTEM_CONFIG] [xml] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[TBSYSTEM_CONFIG] ADD  CONSTRAINT [DF_TBSYSTEM_CONFIG_FDUPDATED_DATE]  DEFAULT (getdate()) FOR [FDUPDATED_DATE]
GO
ALTER TABLE [dbo].[TBSYSTEM_CONFIG] ADD  CONSTRAINT [DF_TBSYSTEM_CONFIG_FDCREATED_DATE]  DEFAULT (getdate()) FOR [FDCREATED_DATE]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'系統設定內容(XML)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSYSTEM_CONFIG', @level2type=N'COLUMN',@level2name=N'FSSYSTEM_CONFIG'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSYSTEM_CONFIG', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FDCREATED_DATE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSYSTEM_CONFIG', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSYSTEM_CONFIG', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSYSTEM_CONFIG', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
