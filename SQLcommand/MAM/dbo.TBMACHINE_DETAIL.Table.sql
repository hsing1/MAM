USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBMACHINE_DETAIL](
	[FSSERVER_NAME] [varchar](255) NOT NULL,
	[FSCPU] [varchar](50) NULL,
	[FSMEMORY] [varchar](50) NULL,
	[FSHD_FREEPERCENT] [varchar](100) NULL,
	[FSHD_FREEMB] [varchar](100) NULL,
	[FSSEND_NETWORK] [varchar](50) NULL,
	[FSREAD_NETWORK] [varchar](50) NULL,
	[FSWEB_USERS] [varchar](3) NULL,
	[FDUPDATE_TIME] [varchar](20) NULL,
 CONSTRAINT [PK_TBMACHINE_DETAIL_1] PRIMARY KEY CLUSTERED 
(
	[FSSERVER_NAME] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'伺服器名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMACHINE_DETAIL', @level2type=N'COLUMN',@level2name=N'FSSERVER_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CPU 使用率' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMACHINE_DETAIL', @level2type=N'COLUMN',@level2name=N'FSCPU'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'記憶體使用率' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMACHINE_DETAIL', @level2type=N'COLUMN',@level2name=N'FSMEMORY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'磁碟剩餘百分比' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMACHINE_DETAIL', @level2type=N'COLUMN',@level2name=N'FSHD_FREEPERCENT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'磁碟剩餘MB' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMACHINE_DETAIL', @level2type=N'COLUMN',@level2name=N'FSHD_FREEMB'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'網路卡傳送傳輸率' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMACHINE_DETAIL', @level2type=N'COLUMN',@level2name=N'FSSEND_NETWORK'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'網路卡讀取傳輸率' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMACHINE_DETAIL', @level2type=N'COLUMN',@level2name=N'FSREAD_NETWORK'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'網站使用者數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMACHINE_DETAIL', @level2type=N'COLUMN',@level2name=N'FSWEB_USERS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最後資料更新時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMACHINE_DETAIL', @level2type=N'COLUMN',@level2name=N'FDUPDATE_TIME'
GO
