USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   新增播出提示資料(目前已不使用)
-- =============================================
CREATE Proc [dbo].[SP_I_TBPGM_QUEUE_MONITER]

@FSPROG_ID	char(7),
@FNSEQNO	int,
@FDDATE	date,
@FSCHANNEL_ID	varchar(2),
@FNEPISODE	int,
@FCPROG_SIDE_LABEL	char(1),
@FCLIVE_SIDE_LABEL	char(1),
@FCTIME_SIDE_LABEL	char(1),
@FCREPLAY	char(1),
@FCRECORD_PLAY	char(1),
@FCSTEREO	char(1),
@FCLOGO	char(1),
@FSCH1	char(2),
@FSCH2	char(2),
@FSCH3	char(2),
@FSCH4	char(2),
@FSCREATED_BY	varchar(50)


As
Insert Into dbo.TBPGM_QUEUE_MONITER(
FSPROG_ID,FNSEQNO,FDDATE,FSCHANNEL_ID,
FNEPISODE,FCPROG_SIDE_LABEL,FCLIVE_SIDE_LABEL,FCTIME_SIDE_LABEL,
FCREPLAY,FCRECORD_PLAY,FCSTEREO,
FCLOGO,FSCH1,FSCH2,FSCH3,FSCH4,
FSCREATED_BY,FDCREATED_DATE)
Values(@FSPROG_ID,@FNSEQNO,@FDDATE,@FSCHANNEL_ID,
@FNEPISODE,@FCPROG_SIDE_LABEL,@FCLIVE_SIDE_LABEL,@FCTIME_SIDE_LABEL,
@FCREPLAY,@FCRECORD_PLAY,@FCSTEREO,@FCLOGO,
@FSCH1,@FSCH2,@FSCH3,@FSCH4,
@FSCREATED_BY,GETDATE())






	
	
GO
