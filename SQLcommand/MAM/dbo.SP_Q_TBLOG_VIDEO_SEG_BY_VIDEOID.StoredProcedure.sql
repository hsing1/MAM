USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   依照主控編號查詢此檔案的首播日期
-- =============================================
CREATE Proc [dbo].[SP_Q_TBLOG_VIDEO_SEG_BY_VIDEOID]
@FSVIDEO_ID varchar(10)
As
SELECT 
 CASE  WHEN  CONVERT(char(10), ISNULL(A.FDDISPLAY_TIME,''), 111) = '1900/01/01'  THEN ''
 ELSE     CONVERT(char(10), ISNULL(A.FDDISPLAY_TIME,''), 111)    END
 as FDDISPLAY_TIME,
ISNULL(A.FSDISPLAY_CHANNEL_ID,'') as FSDISPLAY_CHANNEL_ID,
CONVERT(VARCHAR(20) ,GETDATE() , 111) sysdate
 FROM TBLOG_VIDEO_SEG  A ,TBLOG_VIDEO B
               WHERE A.FSVIDEO_ID=@FSVIDEO_ID and A.FSFILE_NO=B.FSFILE_NO
              
                  
GO
