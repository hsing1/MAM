USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   新增救命帶清單報表資料
-- =============================================
CREATE Proc [dbo].[SP_I_RPT_TBPGM_SAVE_TAPE_LIST_01]

@QUERY_KEY	uniqueidentifier,
@QUERY_KEY_SUBID	uniqueidentifier,
@QUERY_BY	varchar(50),
@播出日期	date,
@頻道	nvarchar(50),
@來源節目名稱	nvarchar(50),
@來源節目集數	int,
@起始時間	varchar(4),
@結束時間	varchar(4),
@序號	int,
@替換節目名稱	varchar(50),
@替換節目集數	int,
@替換節目檔案編號	varchar(16),
@替換節目檔案長度	varchar(11)

	
As
Insert Into dbo.RPT_TBPGM_SAVE_TAPE_LIST_01
(QUERY_KEY,QUERY_KEY_SUBID,QUERY_BY,QUERY_DATE,播出日期,頻道,
來源節目名稱,來源節目集數,起始時間,結束時間,序號,
替換節目名稱,替換節目集數,替換節目檔案編號,替換節目檔案長度)
Values(@QUERY_KEY,@QUERY_KEY_SUBID,@QUERY_BY,GETDATE(),@播出日期,
@頻道,@來源節目名稱,@來源節目集數,@起始時間,@結束時間,@序號,
@替換節目名稱,@替換節目集數,@替換節目檔案編號,@替換節目檔案長度)
GO
