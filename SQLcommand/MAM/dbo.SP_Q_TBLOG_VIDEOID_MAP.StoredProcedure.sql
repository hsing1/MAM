USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   依照ID與集數與資料帶類型資料查詢此節目或短帶的主控編號
-- =============================================
CREATE Proc [dbo].[SP_Q_TBLOG_VIDEOID_MAP]

@FSID varchar(11),
@FNEPISODE smallint,
@FSARC_TYPE char(3)

As

SELECT FSVIDEO_ID_PROG 
FROM TBLOG_VIDEOID_MAP 
WHERE FSID = @FSID AND FNEPISODE = @FNEPISODE AND FSARC_TYPE = @FSARC_TYPE

GO
