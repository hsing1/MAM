USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_U_TBPROG_M_EXTERNAL_GRADE]

@FSPROGID char(7) ,
@FSPROGGRADEID varchar(2) 

As
UPDATE [PTSProg].[PTSProgram].[dbo].TBPROG_M
SET 
FSPROGGRADEID=@FSPROGGRADEID

WHERE  FSPROGID=@FSPROGID

GO
