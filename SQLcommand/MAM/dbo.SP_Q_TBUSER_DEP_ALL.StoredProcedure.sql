USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Q_TBUSER_DEP_ALL]

As
SELECT ISNULL([FNDEP_ID],0) as FNDEP_ID
      ,isnull([FSDEP],'') as FSDEP
      ,isnull([FNPARENT_DEP_ID],0) as FNPARENT_DEP_ID
      ,isnull([FSSupervisor_ID],'') as FSSupervisor_ID
      ,isnull([FSUpDepName_ChtName],'') as FSUpDepName_ChtName
      ,isnull([FSDpeName_ChtName],'') as FSDpeName_ChtName
      ,isnull([FSFullName_ChtName],'') as FSFullName_ChtName
      ,isnull([FNDEP_LEVEL],0) as FNDEP_LEVEL
      ,isnull([FBIsDepExist],'') as FBIsDepExist
      ,isnull([FSDEP_MEMO],'')  as FSDEP_MEMO
FROM MAM.dbo.TBUSER_DEP 
ORDER BY  FNDEP_ID

GO
