USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   查詢短帶託播單預排資料(目前已不使用)
-- =============================================
CREATE Proc [dbo].[SP_Q_TBPGM_PROMO_SCHEDULED_BY_CHANNEL_DATE]

@FSCHANNEL_ID	varchar(2),
@FDDATE datetime
As
select A.FNPROMO_BOOKING_NO,A.FSPROMO_ID,A.FNNO,
A.FDDATE,A.FSCHANNEL_ID,A.FSTIME,A.FCSTATUS,B.FSCHANNEL_NAME,
C.FSPROMO_NAME,C.FSDURATION,D.FSFILE_NO,E.FSVIDEO_ID_PROG as FSVIDEO_ID
from TBPGM_PROMO_SCHEDULED A
left join TBZCHANNEL B on  A.FSCHANNEL_ID=B.FSCHANNEL_ID
left join TBPGM_PROMO C on A.FSPROMO_ID=C.FSPROMO_ID
left join TBLOG_VIDEO D on A.FSPROMO_ID=D.FSID and D.FSARC_TYPE=B.FSCHANNEL_TYPE 
and (D.FCFILE_STATUS='T' or D.FCFILE_STATUS='Y')
left join TBLOG_VIDEOID_MAP E on A.FSPROMO_ID=E.FSID and B.FSCHANNEL_TYPE=E.FSARC_TYPE
where  @FDDATE = case when @FDDATE = '' then @FDDATE else A.FDDATE end
and @FSCHANNEL_ID = case when @FSCHANNEL_ID = '' then @FSCHANNEL_ID else A.FSCHANNEL_ID end

GO
