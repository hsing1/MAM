USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE Proc [dbo].[SP_Q_TBMODULE_GROUP_PERMISSION]
@FSMODULE_ID char(7), 
@FSGROUP_ID varchar(20) 

As
	Declare @brtn int 
	set @brtn=0
	select @brtn = Count(*) 
	from TBMODULE_GROUP
	WHERE FSMODULE_ID = @FSMODULE_ID AND FSGROUP_ID = @FSGROUP_ID 

	

if @brtn > 0 
Begin 
	Select 'True' FCHasPermission
End 

Else 
Begin 
	Select 'False' FCHasPermission
End 

GO
