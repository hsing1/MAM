USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <David.Sin>
-- Create date:   <2010/10/02>
-- Description:   根據調用單編號更新調用轉檔狀態
-- =============================================

CREATE PROC [dbo].[SP_U_TBBOOKING_DETAIL_FILE_STATUS_BY_BOOKINGNO]
@FSBOOKING_NO varchar(12),
@FSSEQ varchar(3),
@FCFILE_TRANSCODE_STATUS char(1)
AS

UPDATE TBBOOKING_DETAIL
SET FCFILE_TRANSCODE_STATUS=@FCFILE_TRANSCODE_STATUS
WHERE FSBOOKING_NO=@FSBOOKING_NO AND FSSEQ=@FSSEQ
GO
