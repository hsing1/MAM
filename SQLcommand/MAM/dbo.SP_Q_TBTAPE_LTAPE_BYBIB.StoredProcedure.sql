USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Q_TBTAPE_LTAPE_BYBIB]
@BIB VARCHAR (7)

As
SELECT 
TapeType.TapeName,
ZSystem.TypeName,
LTape.*

FROM
[PTSTape].[PTSTape].[dbo].[tblLTape] LTape
LEFT OUTER JOIN [PTSTape].[PTSTape].[dbo].[tblTapeType] as TapeType on LTape.TTapeCode = TapeType.TapeCode
LEFT OUTER JOIN [PTSTape].[PTSTape].[dbo].[tblZSystem] as ZSystem on LTape.TType = ZSystem.TypeCode
WHERE
LTape.BIB=@BIB

--SP_Q_TBTAPE_LTAPE_BYBIB '0013996'

GO
