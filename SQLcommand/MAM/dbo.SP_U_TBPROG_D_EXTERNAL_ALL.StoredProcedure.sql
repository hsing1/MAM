USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  Proc [dbo].[SP_U_TBPROG_D_EXTERNAL_ALL]

@FSPROGID char(7) ,
@FNEPISODE smallint ,
@FSPGDNAME varchar(50) ,
@FSPGDENAME varchar(100) ,
@FNLENGTH smallint ,
@FSPROGSRCID varchar(2) ,
@FSPROGATTRID varchar(2) ,
@FSPROGAUDID varchar(2) ,
@FSPROGTYPEID varchar(2) ,
@FSPROGGRADEID varchar(2) ,
@FSPROGLANGID1 varchar(2) ,
@FSPROGLANGID2 varchar(2) ,
@FSPROGSALEID varchar(2) ,
@FSPROGBUYID varchar(2) ,
@FSPROGBUYDID varchar(2) ,
@FSSHOOTSPEC char(50) ,
@FSPROGSPEC char(100) ,
@FSPROGGRADE varchar(100) ,
@FSCONTENT text ,
@FSMEMO varchar(500) ,
@FNEPISODE_END smallint ,
@FSPROGNATIONID varchar(2),
@FSPRDYEAR varchar(4),
@bolPGDNAME  char(1) ,
@bolPGDENAME  char(1) ,
@bolLENGTH  char(1) ,
@bolPROGGRADEID  char(1) ,
@bolPROGGRADE  char(1) ,
@bolCONTENT  char(1) ,
@bolcbMEMO  char(1) ,
@bolCR_TYPE  char(1) ,
@bolCR_NOTE  char(1) ,
@bolPROGSRCID  char(1) ,
@bolPROGATTRID  char(1) ,
@bolPROGAUDID  char(1) ,
@bolPROGTYPEID  char(1) ,
@bolPROGLANGID1  char(1) ,
@bolPROGLANGID2  char(1) ,
@bolPROGBUYID  char(1) ,
@bolPROGSALEID  char(1),
@bolPROGSPEC  char(1) ,
@bolSHOOTSPEC  char(1),
@bolPROGNATIONID char(1),
@bolPRDYEAR char(1)

As

IF @FSPGDENAME = ''
	BEGIN 
		SET @FSPGDENAME = null
	END

IF @FNLENGTH = ''
	BEGIN 
		SET @FNLENGTH = null
	END

IF @FSPROGGRADE = ''
	BEGIN 
		SET @FSPROGGRADE = null
	END

IF @FSMEMO = ''
	BEGIN 
		SET @FSMEMO = null
	END	
	
IF @FSPROGBUYID = ''
	BEGIN 
		SET @FSPROGBUYID = null
	END

IF @FSPROGBUYDID = ''
	BEGIN 
		SET @FSPROGBUYDID = null
	END

IF @FSPROGSALEID = ''
	BEGIN 
		SET @FSPROGSALEID = null
	END		

IF @FSSHOOTSPEC  = ''
	BEGIN 
		SET @FSSHOOTSPEC  = null
	END

IF @FSPROGSPEC = ''
	BEGIN 
		SET @FSPROGSPEC = null
	END	
	
IF @FSPROGNATIONID = ''
BEGIN 
	SET @FSPROGNATIONID = null
END		

IF @FSPRDYEAR=''
	BEGIN
		SET @FSPRDYEAR = null
	END

UPDATE [PTSProg].[PTSProgram].[dbo].TBPROG_D 
SET 
FSPGDNAME = case when @bolPGDNAME ='Y' then @FSPGDNAME else FSPGDNAME end,
FSPGDENAME = case when @bolPGDENAME ='Y' then @FSPGDENAME else FSPGDENAME end,
FNLENGTH = case when @bolLENGTH ='Y' then @FNLENGTH else FNLENGTH end,
FSPROGGRADEID = case when @bolPROGGRADEID ='Y' then @FSPROGGRADEID else FSPROGGRADEID end,
FSPROGGRADE = case when @bolPROGGRADE ='Y' then @FSPROGGRADE else FSPROGGRADE end,
FSCONTENT = case when @bolCONTENT ='Y' then @FSCONTENT else FSCONTENT end,
FSMEMO = case when @bolcbMEMO ='Y' then @FSMEMO else FSMEMO end,

FSPROGSRCID = case when @bolPROGSRCID ='Y' then @FSPROGSRCID else FSPROGSRCID end,
FSPROGATTRID = case when @bolPROGATTRID ='Y' then @FSPROGATTRID else FSPROGATTRID end,
FSPROGAUDID = case when @bolPROGAUDID ='Y' then @FSPROGAUDID else FSPROGAUDID end,
FSPROGTYPEID = case when @bolPROGTYPEID ='Y' then @FSPROGTYPEID else FSPROGTYPEID end,
FSPROGLANGID1 = case when @bolPROGLANGID1 ='Y' then @FSPROGLANGID1 else FSPROGLANGID1 end,
FSPROGLANGID2 = case when @bolPROGLANGID2 ='Y' then @FSPROGLANGID2 else FSPROGLANGID2 end,
FSPROGBUYID = case when @bolPROGBUYID ='Y' then @FSPROGBUYID else FSPROGBUYID end,
FSPROGBUYDID = case when @bolPROGBUYID ='Y' then @FSPROGBUYDID else FSPROGBUYDID end,
FSPROGSALEID = case when @bolPROGSALEID ='Y' then @FSPROGSALEID else FSPROGSALEID end,
FSPROGSPEC = case when @bolPROGSPEC ='Y' then MAM.dbo.FN_Q_SET_CODELIST_TO_PURESTRING(@FSPROGSPEC,'50') else FSPROGSPEC end,
FSSHOOTSPEC = case when @bolSHOOTSPEC ='Y' then MAM.dbo.FN_Q_SET_CODELIST_TO_PURESTRING(@FSSHOOTSPEC,'50') else FSSHOOTSPEC end,
FSUPDUSER='01065',
FDUPDDATE=GETDATE(),
FSPROGNATIONID=case when @bolPROGNATIONID='Y' then @FSPROGNATIONID else FSPROGNATIONID end,
FSPRDYEAR=case when @bolPRDYEAR='Y' then @FSPRDYEAR else FSPRDYEAR end
WHERE  FSPROGID=@FSPROGID AND (FNEPISODE BETWEEN @FNEPISODE AND @FNEPISODE_END)
GO
