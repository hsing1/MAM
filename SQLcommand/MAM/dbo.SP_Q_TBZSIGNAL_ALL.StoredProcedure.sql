USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   查詢訊號源資料
-- =============================================
CREATE Proc [dbo].[SP_Q_TBZSIGNAL_ALL]


As
SELECT * FROM MAM.dbo.TBZSIGNAL

GO
