USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Proc [dbo].[SP_D_PTSFlow_AgentType3]

@aid nvarchar(100)

AS

delete from [PTSFlow].[dbo].[AgentDetail_Flow] where aid=@aid

delete FROM [PTSFlow].[dbo].[Agent_Flow] where aid=@aid

GO
