USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   查詢預約破口資源資料
-- =============================================
CREATE Proc [dbo].[SP_Q_TBPGM_PROMO_PLAN_BY_ID_DATE_CHANNEL]

@FSPROMO_ID	varchar(11),
@FDDATE	nvarchar(50),
@WEEK	varchar(7)
As
select
A.FSPROMO_ID,A.FSPROMO_NAME,B.FSPROG_ID,B.FSPROG_NAME,
B.FNPROMO_TYPEA,B.FNPROMO_TYPEB,B.FNPROMO_TYPEC,B.FNPROMO_TYPED,B.FNPROMO_TYPEE,
B.FSPLAY_COUNT_MAX,B.FSPLAY_COUNT_MIN
from
TBPGM_PROMO A
left join TBPGM_PROMO_PLAN B on A.FSPROG_ID=B.FSPROG_ID
where
A.FSPROMO_ID=@FSPROMO_ID
and B.FSPROMO_END_DATE >=@FDDATE
and B.FSPROMO_BEG_DATE <= @FDDATE
and B.FSWEEK like @WEEK


GO
