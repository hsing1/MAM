USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBLOG_VIDEO_SEG](
	[FSFILE_NO] [char](16) NOT NULL,
	[FNSEG_ID] [char](2) NOT NULL,
	[FSVIDEO_ID] [varchar](10) NOT NULL,
	[FSBEG_TIMECODE] [char](11) NOT NULL,
	[FSEND_TIMECODE] [char](11) NOT NULL,
	[FCLOW_RES] [char](1) NULL,
	[FNFILE_SIZE] [bigint] NULL,
	[FDDISPLAY_TIME] [datetime] NULL,
	[FSDISPLAY_CHANNEL_ID] [char](2) NULL,
	[FCMSS_QC] [char](1) NULL,
	[FSMSS_QC_MSG] [nvarchar](100) NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
	[FSUPDATED_BY] [varchar](50) NULL,
	[FDUPDATED_DATE] [datetime] NULL,
 CONSTRAINT [PK_TBLOG_VIDEO_SEG_1] PRIMARY KEY CLUSTERED 
(
	[FSFILE_NO] ASC,
	[FNSEG_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
CREATE NONCLUSTERED INDEX [IX_TBLOG_VIDEO_SEG_VIDEOID] ON [dbo].[TBLOG_VIDEO_SEG]
(
	[FSVIDEO_ID] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'檔案編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_SEG', @level2type=N'COLUMN',@level2name=N'FSFILE_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'段落序號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_SEG', @level2type=N'COLUMN',@level2name=N'FNSEG_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主控播出編碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_SEG', @level2type=N'COLUMN',@level2name=N'FSVIDEO_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time Code起' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_SEG', @level2type=N'COLUMN',@level2name=N'FSBEG_TIMECODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time Code迄' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_SEG', @level2type=N'COLUMN',@level2name=N'FSEND_TIMECODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'段落轉檔狀態' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_SEG', @level2type=N'COLUMN',@level2name=N'FCLOW_RES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'檔案大小' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_SEG', @level2type=N'COLUMN',@level2name=N'FNFILE_SIZE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_SEG', @level2type=N'COLUMN',@level2name=N'FSDISPLAY_CHANNEL_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_SEG', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_SEG', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_SEG', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_SEG', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
