USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   新增建議刪除清單報表資料
-- =============================================
CREATE Proc [dbo].[SP_I_RPT_PROPOSE_DELETE_LIST_01]

@QUERY_KEY	uniqueidentifier,
@QUERY_KEY_SUBID	uniqueidentifier,
@QUERY_BY	varchar(50),
@FSVIDEOID	varchar(10),
@FSNAME	varchar(50),
@FNEPISODE	varchar(50),
@FNSEG_ID	varchar(50)
As
Insert Into dbo.RPT_PROPOSE_DELETE_LIST_01
(QUERY_KEY,QUERY_KEY_SUBID,QUERY_BY,
QUERY_DATE,FSVIDEOID,FSNAME,FNEPISODE,FNSEG_ID)
Values(@QUERY_KEY,@QUERY_KEY_SUBID,@QUERY_BY,
GETDATE(),@FSVIDEOID,@FSNAME,@FNEPISODE,@FNSEG_ID)
GO
