USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Q_TBPGM_PROG_PROMO_LINK_DETAIL]

@FSPROG_ID	char(7)

As
SELECT A.FSPROG_ID,A.FNSEQNO,A.FSPROMO_ID,A.FNNO,B.FSPROMO_NAME,B.FSDURATION,B.FSMEMO,A.FSCHANNEL_ID,C.FSCHANNEL_NAME
from dbo.TBPGM_PROG_PROMO_LINK A
Left Join TBPGM_PROMO B on A.FSPROMO_ID=B.FSPROMO_ID 
Left Join TBZCHANNEL C on A.FSCHANNEL_ID=C.FSCHANNEL_ID
where A.FSPROG_ID=@FSPROG_ID


GO
