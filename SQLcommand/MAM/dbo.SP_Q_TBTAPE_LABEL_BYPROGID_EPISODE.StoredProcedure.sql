USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Q_TBTAPE_LABEL_BYPROGID_EPISODE]
@FSPROG_ID char(7) ,
@FNEPISODE smallint

As
SELECT 
Label.*
--LabelProg.BIB,Label.LabelType
FROM
[PTSTape].[PTSTape].[dbo].[tblLLabelProg] LabelProg,
[PTSTape].[PTSTape].[dbo].[tblLLabel] Label
WHERE
LabelProg.BIB=Label.BIB
AND LabelProg.Progid=@FSPROG_ID
AND LabelProg.EpNo=@FNEPISODE

--SP_Q_TBTAPE_LABEL_BYPROGID_EPISODE '2006408','128'

GO
