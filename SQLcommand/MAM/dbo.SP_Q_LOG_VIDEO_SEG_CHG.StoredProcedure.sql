USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Jarvis.Yang>
-- Create date: <2013-06-05>
-- Description:	<多條件查詢TBLOG_VIDEO_SEG_CHG的資料>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Q_LOG_VIDEO_SEG_CHG]
	@FSTYPE char,
	@FSPROGID varchar(50),
	@FNEPISOD varchar(12),
	@SDATE varchar(12),
	@EDATE varchar(12),
	@FSCREATED_BY nvarchar(50)
	
AS
BEGIN
if(@FSTYPE='G')
begin
select v.FSFILE_NO,v.FNEPISODE,v.FSID,
(select FSPGMNAME from TBPROG_M m where m.FSPROG_ID=v.FSID )as 'FSPGMNAME',
(select FSPGDNAME from TBPROG_D d where d.FSPROG_ID=v.FSID and d.FNEPISODE=v.FNEPISODE)as 'FSPGDNAME',
(select FSUSER_ChtName from TBUSERS where FSUSER_ID=chg.FSCREATED_BY) as 'FSUSER_ChtName',
chg.* from 
  (
  select * from TBLOG_VIDEO 
  where  FSTYPE ='G'
  )
  as v
  join TBLOG_VIDEO_SEG_CHG chg on v.FSFILE_NO=chg.FSFILE_NO
  where
 @FSCREATED_BY=case when @FSCREATED_BY='' then @FSCREATED_BY else chg.FSCREATED_BY end
 and chg.FDCREATED_DATE between @SDATE and @EDATE
 and v.FSID=case when @FSPROGID='' then v.FSID else  @FSPROGID end
 and @FNEPISOD =case when @FNEPISOD='' then @FNEPISOD else  v.FNEPISODE end
 order by chg.FDCREATED_DATE DESC
  end
  else if(@FSTYPE='P')
  begin
  select v.FSFILE_NO,v.FNEPISODE,v.FSID,p.FSPROMO_NAME as 'FSPGMNAME' ,'FSPGDNAME' as 'FSPGDNAME'
  ,(select FSUSER_ChtName from TBUSERS where FSUSER_ID=chg.FSCREATED_BY) as 'FSUSER_ChtName'
  ,chg.*from 
  (
   select * from TBLOG_VIDEO 
  where  FSTYPE ='P'
  )
  as v
  join TBPGM_PROMO as p on p.FSPROMO_ID=v.FSID
  join TBLOG_VIDEO_SEG_CHG chg on v.FSFILE_NO=chg.FSFILE_NO
  where
 @FSCREATED_BY=case when @FSCREATED_BY='' then @FSCREATED_BY else chg.FSCREATED_BY end
 and @FSPROGID =case when @FSPROGID='' then @FSPROGID else p.FSPROMO_ID end
 and chg.FDCREATED_DATE between @SDATE and @EDATE
 order by chg.FDCREATED_DATE DESC
  end
    else if(@FSTYPE='D')
  begin
  
  select v.FSFILE_NO,v.FNEPISODE,v.FSID,da.FSDATA_NAME as 'FSPGMNAME','FSPGDNAME'
  ,(select FSUSER_ChtName from TBUSERS where FSUSER_ID=chg.FSCREATED_BY) as 'FSUSER_ChtName'
  ,chg.* from 
  (
   select * from TBLOG_VIDEO 
  where  FSTYPE ='D'
  )
  as v
  join TBDATA as da on da.FSDATA_ID= v.FSID
  join TBLOG_VIDEO_SEG_CHG chg on v.FSFILE_NO=chg.FSFILE_NO
  where
 @FSCREATED_BY=case when @FSCREATED_BY='' then @FSCREATED_BY else chg.FSCREATED_BY end
 and @FSPROGID=case when @FSPROGID='' then @FSPROGID else da.FSDATA_ID end
 and chg.FDCREATED_DATE between @SDATE and @EDATE
 order by chg.FDCREATED_DATE DESC
  end
  

END







GO
