USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2015/04/27>
-- Description:   查詢Filing節目清單
-- =============================================
CREATE Proc [dbo].[SP_Q_FILING_LIST]
@FDDATE	date,
@FSCHANNEL_ID	varchar(2)
As
IF (CONVERT(INT, @FSCHANNEL_ID) ) >= 50 
select distinct A.FSPROG_ID,A.FNEPISODE,B.FSFILE_NO,A.fsprog_name ,C.FSVIDEO_ID,C.FNSEG_ID,C.FSBEG_TIMECODE,C.FSEND_TIMECODE
from tbpgm_combine_queue A
left join TBZCHANNEL D on A.FSCHANNEL_ID=D.FSCHANNEL_ID
left join TBLOG_VIDEO B on A.FSPROG_ID=B.FSID and A.FNEPISODE=B.FNEPISODE and B.[FSARC_TYPE]=D.FSCHANNEL_TYPE and A.FSVIDEO_ID=B.FSVIDEO_PROG
left join tblog_video_seg C on B.FSFILE_NO=C.FSFILE_NO
where A.FDDATE=@FDDATE and A.FSCHANNEL_ID=@FSCHANNEL_ID
and C.FSVIDEO_ID <>'' 
EXCEPT  select distinct A.FSPROG_ID,A.FNEPISODE,B.FSFILE_NO,A.fsprog_name ,C.FSVIDEO_ID,C.FNSEG_ID,C.FSBEG_TIMECODE,C.FSEND_TIMECODE
from tbpgm_combine_queue A
left join TBZCHANNEL D on A.FSCHANNEL_ID=D.FSCHANNEL_ID
left join TBLOG_VIDEO B on A.FSPROG_ID=B.FSID and A.FNEPISODE=B.FNEPISODE and B.[FSARC_TYPE]=D.FSCHANNEL_TYPE and A.FSVIDEO_ID=B.FSVIDEO_PROG 
left join tblog_video_seg C on B.FSFILE_NO=C.FSFILE_NO
where A.FDDATE=convert(datetime, @FDDATE, 111)-1 and A.FSCHANNEL_ID=@FSCHANNEL_ID
and C.FSVIDEO_ID <>'' 
EXCEPT select distinct A.FSPROG_ID,A.FNEPISODE,B.FSFILE_NO,A.fsprog_name ,C.FSVIDEO_ID,C.FNSEG_ID,C.FSBEG_TIMECODE,C.FSEND_TIMECODE
from tbpgm_combine_queue A
left join TBZCHANNEL D on A.FSCHANNEL_ID=D.FSCHANNEL_ID
left join TBLOG_VIDEO B on A.FSPROG_ID=B.FSID and A.FNEPISODE=B.FNEPISODE and B.[FSARC_TYPE]=D.FSCHANNEL_TYPE and A.FSVIDEO_ID=B.FSVIDEO_PROG
left join tblog_video_seg C on B.FSFILE_NO=C.FSFILE_NO
where A.FDDATE=convert(datetime,  @FDDATE, 111) and A.FSCHANNEL_ID=right('00' + Cast(CONVERT(INT, @FSCHANNEL_ID)-50 as varchar(2)),2)
and C.FSVIDEO_ID <>'' 
order by FSVIDEO_ID
else
begin
select distinct A.FSPROG_ID,A.FNEPISODE,B.FSFILE_NO,A.fsprog_name ,C.FSVIDEO_ID,C.FNSEG_ID,C.FSBEG_TIMECODE,C.FSEND_TIMECODE
from tbpgm_combine_queue A
left join TBZCHANNEL D on A.FSCHANNEL_ID=D.FSCHANNEL_ID
left join TBLOG_VIDEO B on A.FSPROG_ID=B.FSID and A.FNEPISODE=B.FNEPISODE and B.[FSARC_TYPE]=D.FSCHANNEL_TYPE and A.FSVIDEO_ID=B.FSVIDEO_PROG
left join tblog_video_seg C on B.FSFILE_NO=C.FSFILE_NO
where A.FDDATE=@FDDATE and A.FSCHANNEL_ID=@FSCHANNEL_ID
and C.FSVIDEO_ID <>'' 
EXCEPT  select distinct A.FSPROG_ID,A.FNEPISODE,B.FSFILE_NO,A.fsprog_name ,C.FSVIDEO_ID,C.FNSEG_ID,C.FSBEG_TIMECODE,C.FSEND_TIMECODE
from tbpgm_combine_queue A
left join TBZCHANNEL D on A.FSCHANNEL_ID=D.FSCHANNEL_ID
left join TBLOG_VIDEO B on A.FSPROG_ID=B.FSID and A.FNEPISODE=B.FNEPISODE and B.[FSARC_TYPE]=D.FSCHANNEL_TYPE and A.FSVIDEO_ID=B.FSVIDEO_PROG
left join tblog_video_seg C on B.FSFILE_NO=C.FSFILE_NO
where A.FDDATE=convert(datetime, @FDDATE, 111)-1 and A.FSCHANNEL_ID=@FSCHANNEL_ID
and C.FSVIDEO_ID <>'' 
order by FSVIDEO_ID
end
GO
