USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_I_TBPGM_ACTION]

@FSACTIONID	varchar(2),
@FSACTIONNAME	nvarchar(50)
As
Insert Into dbo.TBPGM_ACTION(FSACTIONID,FSACTIONNAME)
Values(@FSACTIONID,@FSACTIONNAME)
GO
