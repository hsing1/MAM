USE [MAM]
GO
CREATE USER [audiotest] FOR LOGIN [audiotest] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [audiotest]
GO
ALTER ROLE [db_datareader] ADD MEMBER [audiotest]
GO
