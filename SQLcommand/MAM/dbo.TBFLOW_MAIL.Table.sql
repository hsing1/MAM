USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBFLOW_MAIL](
	[FSFLOW_ID] [char](2) NOT NULL,
	[FSITEM_ID] [char](2) NOT NULL,
	[FSCONTENT] [varchar](500) NOT NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
 CONSTRAINT [PK_TBFLOW_MAIL] PRIMARY KEY CLUSTERED 
(
	[FSFLOW_ID] ASC,
	[FSITEM_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[TBFLOW_MAIL] ADD  CONSTRAINT [DF_TBFLOW_MAIL_FSCREATED_BY]  DEFAULT ('admin') FOR [FSCREATED_BY]
GO
ALTER TABLE [dbo].[TBFLOW_MAIL] ADD  CONSTRAINT [DF_TBFLOW_MAIL_FDCREATED_DATE]  DEFAULT (getdate()) FOR [FDCREATED_DATE]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'流程的ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBFLOW_MAIL', @level2type=N'COLUMN',@level2name=N'FSFLOW_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'流程關卡的ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBFLOW_MAIL', @level2type=N'COLUMN',@level2name=N'FSITEM_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'信件的內容' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBFLOW_MAIL', @level2type=N'COLUMN',@level2name=N'FSCONTENT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBFLOW_MAIL', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBFLOW_MAIL', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
