USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblTBSDept](
	[DeptID] [varchar](4) NOT NULL,
	[UpperDept] [varchar](4) NOT NULL,
	[LevelCode] [varchar](2) NOT NULL,
	[BossID] [nchar](10) NULL,
	[StopDate] [nchar](10) NULL,
	[UUUDeptName] [varchar](60) NULL,
	[UUDeptName] [varchar](60) NULL,
	[UDeptName] [varchar](60) NULL,
	[DeptName] [varchar](60) NOT NULL,
	[FULLName] [varchar](246) NULL,
	[Exist] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'场絏' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tblTBSDept', @level2type=N'COLUMN',@level2name=N'DeptID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'糷场絏' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tblTBSDept', @level2type=N'COLUMN',@level2name=N'UpperDept'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'场' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tblTBSDept', @level2type=N'COLUMN',@level2name=N'LevelCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'恨ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tblTBSDept', @level2type=N'COLUMN',@level2name=N'BossID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'氨ノ丁' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tblTBSDept', @level2type=N'COLUMN',@level2name=N'StopDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'糷场いゅ嘿' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tblTBSDept', @level2type=N'COLUMN',@level2name=N'UUUDeptName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'糷场いゅ嘿' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tblTBSDept', @level2type=N'COLUMN',@level2name=N'UUDeptName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'糷场いゅ嘿' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tblTBSDept', @level2type=N'COLUMN',@level2name=N'UDeptName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'场嘿' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tblTBSDept', @level2type=N'COLUMN',@level2name=N'DeptName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'场' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tblTBSDept', @level2type=N'COLUMN',@level2name=N'FULLName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'琌ノい' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tblTBSDept', @level2type=N'COLUMN',@level2name=N'Exist'
GO
