USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/03/21>
-- Description:	For RPT_TBARCHIVE_01(入庫單), 列印前存入暫存資料庫
-- =============================================
CREATE PROCEDURE [dbo].[SP_R_STT_CountV_ByChannel]
	@BeginYear nvarchar(4),	
	@BeginMonth		nvarchar(2),	
	@EndYear nvarchar(4),
    @EndMonth nvarchar(2),
    @CREATED_BY varchar(50)	
AS
BEGIN
	--declare @BeginYear nvarchar(4)='2010'
	--declare @BeginMonth nvarchar(2)='09'
	--declare @EndYear nvarchar(4)='2011'
	--declare @EndMonth nvarchar(2)='11'
	--declare @CREATED_BY varchar(50) ='sa'
  declare @BeginDate datetime = @BeginYear+'/'+@BeginMonth+'/01'
  declare @EndDate datetime = @EndYear+'/'+@EndMonth+'/01'
  declare @i int=0
  declare @VP int =99999
  declare @VH int=99999
  declare @VO int=99999
  declare @VG int=99999
  declare @QUERY_BY nvarchar(50)=''
  declare @CountTable Table(
  [Year] nvarchar (4),
  [Month] nvarchar (2),
  [公視] int,
  [客台] int,
  [原台] int,
  [宏觀] int,
  [QUERY_BY] nvarchar (50)
  )
  while DATEADD(MONTH,@i,@BeginDate)<=@EndDate
  begin
  set @VP=
  (select COUNT(*) from [MAM].[dbo].[TBLOG_VIDEO]
	  where ([FDCREATED_DATE] between DATEADD(MONTH,@i,@BeginDate) and DATEADD(MONTH,@i+1,@BeginDate)) and [FSCHANNEL_ID]='01')
  set @VH=
  (select COUNT(*) from [MAM].[dbo].[TBLOG_VIDEO]
	  where ([FDCREATED_DATE] between DATEADD(MONTH,@i,@BeginDate) and DATEADD(MONTH,@i+1,@BeginDate)) and [FSCHANNEL_ID]='07')
  set @VO=
	  (select COUNT(*) from [MAM].[dbo].[TBLOG_VIDEO]
	  where ([FDCREATED_DATE] between DATEADD(MONTH,@i,@BeginDate) and DATEADD(MONTH,@i+1,@BeginDate)) and [FSCHANNEL_ID]='09')
  set @VG=
	  (select COUNT(*) from [MAM].[dbo].[TBLOG_VIDEO]
	  where ([FDCREATED_DATE] between DATEADD(MONTH,@i,@BeginDate) and DATEADD(MONTH,@i+1,@BeginDate)) and [FSCHANNEL_ID]='08')
  
  set @QUERY_BY = (select FSUSER_ChtName from [MAM].[dbo].[TBUSERS] where [FSUSER_ID]=@CREATED_BY)
  
  insert @CountTable values(CONVERT(nvarchar, DATEPART(YEAR,DATEADD(MONTH,@i,@BeginDate))),DATEPART(MONTH,DATEADD(MONTH,@i,@BeginDate)),@VP,@VH,@VO,@VG,@QUERY_BY)
  set @i+=1
  
  end
  
  select * From @CountTable
END


GO
