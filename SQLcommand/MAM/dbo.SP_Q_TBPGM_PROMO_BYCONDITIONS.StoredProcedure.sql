USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Q_TBPGM_PROMO_BYCONDITIONS]
@FSPROMO_ID varchar(11),
@FSPROMO_NAME nvarchar(50) ,
@FSPROMO_NAME_ENG varchar(50) ,
@FSDURATION char(11) ,
@FSPROG_ID char(7) ,
@FNEPISODE smallint ,
@FSPROMOTYPEID char(2) ,
@FSPROGOBJID varchar(2) ,
@FSPRDCENID varchar(5) ,
@FSPRDYYMM varchar(6) ,
@FSACTIONID char(2) ,
@FSPROMOCATID char(2) ,
@FSPROMOCATDID char(2) ,
@FSPROMOVERID char(2) ,
@FSMAIN_CHANNEL_ID char(2)  ,
@FSMEMO nvarchar(500) ,
@FSWELFARE  nvarchar(100) ,
@FSCREATED_BY varchar(50),
@FSDEL char

As
select ISNULL(b.FSUSER_ChtName,'') as FSCREATED_BY_NAME, ISNULL(C.FSUSER_ChtName,'') as FSUPDATED_BY_NAME , ISNULL(f.FSUSER_ChtName,'') as FSDEL_BY_NAME,
ISNULL(CONVERT(VARCHAR ,a.FDCREATED_DATE , 120),'') as WANT_FDCREATEDDATE,
ISNULL(CONVERT(VARCHAR ,a.FDUPDATED_DATE , 120),'') as WANT_FDUPDATEDDATE,
ISNULL(d.FSPGMNAME,'')  as FSPROG_ID_NAME,
ISNULL(e.FSPGDNAME,'')  as FNEPISODE_NAME,
a.* FROM TBPGM_PROMO  as a  
               left outer JOIN TBUSERS as b on a.FSCREATED_BY=b.FSUSER_ID
               left outer join TBUSERS as c on a.FSUPDATED_BY=c.FSUSER_ID
               left outer join TBUSERS as f on a.FSDELUSER=f.FSUSER_ID               
               left outer join TBPROG_M as d on a.FSPROG_ID=d.FSPROG_ID -----[MAM].[dbo].[TBPROG_M]
               left outer join TBPROG_D as e on a.FSPROG_ID=e.FSPROG_ID and a.FNEPISODE=e.FNEPISODE
where 
@FSPROMO_ID=case when @FSPROMO_ID='' then @FSPROMO_ID else a.FSPROMO_ID end
AND a.FSPROMO_NAME like '%' + @FSPROMO_NAME +'%'
AND a.FSPROMO_NAME_ENG like @FSPROMO_NAME_ENG +'%'
AND a.FSMEMO like @FSMEMO +'%'
AND @FSDURATION = case when @FSDURATION ='' then @FSDURATION else a.FSDURATION end
AND @FSPROG_ID = case when @FSPROG_ID ='' then @FSPROG_ID else  a.FSPROG_ID end
AND @FNEPISODE = case when @FNEPISODE ='' then @FNEPISODE else  a.FNEPISODE end
AND @FSPROMOTYPEID = case when @FSPROMOTYPEID ='' then @FSPROMOTYPEID else  a.FSPROMOTYPEID end
AND @FSPROGOBJID = case when @FSPROGOBJID ='' then @FSPROGOBJID else  a.FSPROGOBJID end
AND @FSPRDCENID = case when @FSPRDCENID ='' then @FSPRDCENID else  a.FSPRDCENID end
AND @FSPRDYYMM = case when @FSPRDYYMM ='' then @FSPRDYYMM else  a.FSPRDYYMM end
AND @FSACTIONID = case when @FSACTIONID ='' then @FSACTIONID else  a.FSACTIONID end
AND @FSPROMOCATID = case when @FSPROMOCATID ='' then @FSPROMOCATID else  a.FSPROMOCATID end
AND @FSPROMOCATDID = case when @FSPROMOCATDID ='' then @FSPROMOCATDID else  a.FSPROMOCATDID end
AND @FSPROMOVERID = case when @FSPROMOVERID ='' then @FSPROMOVERID else  a.FSPROMOVERID end
AND @FSMAIN_CHANNEL_ID = case when @FSMAIN_CHANNEL_ID ='' then @FSMAIN_CHANNEL_ID else  a.FSMAIN_CHANNEL_ID end
And @FSDEL =case when @FSDEL ='' then @FSDEL else  a.FSDEL end
AND a.FSWELFARE like @FSWELFARE +'%'
AND @FSCREATED_BY = case when @FSCREATED_BY ='' then @FSCREATED_BY else  a.FSCREATED_BY end
order by a.FDCREATED_DATE desc 
GO
