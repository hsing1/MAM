USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        
-- Create date: 
-- Description: 取得首派清單
-- Update desc:    <2013/10/28><Mike>加入註解。
-- =============================================
CREATE PROC [dbo].[SP_Q_GET_First_Post_List]

AS

select distinct C.FSPROG_NAME FSTITLE,CONVERT(VARCHAR(20) ,B.FDDISPLAY_TIME , 111) as FDDISPLAY_TIME 
,B.FSVIDEO_ID,B.FNSEG_ID,B.FSBEG_TIMECODE,B.FSEND_TIMECODE,C.FNEPISODE
from 
tblog_video_seg B
left join TBLOG_VIDEO A on B.FSFILE_NO=A.FSFILE_NO
left join TBPGM_COMBINE_QUEUE C on A.FSID=C.FSPROG_ID and A.FNEPISODE=C.FNEPISODE
where 
B.FSVIDEO_ID in 
(select FSVIDEO_ID from TBPGM_FILE_FTP_DATE 
where  CONVERT(VARCHAR(20) ,fdFTP_date , 111) > CONVERT(VARCHAR(20) ,getdate()-2 , 111)
and len(FSVIDEO_ID)=10
union 
 (select distinct A.fsvideo_id + 
RIGHT(REPLICATE('0', 2) + CAST(CONVERT(varchar(2),A.fnbreak_no) as NVARCHAR), 2) 
from tbpgm_combine_queue A
left join TBLOG_VIDEO B  on  A.FSPROG_ID= B.FSID and A.FNEPISODE=B.FNEPISODE 
 and A.FSVIDEO_ID=B.FSVIDEO_PROG
 where CONVERT(VARCHAR(20) ,fddate , 111)= CONVERT(VARCHAR(20) ,getdate()+1 , 111)
 and B.FCFILE_STATUS<>'T' and B.FCFILE_STATUS<>'Y')
 EXCEPT 
 (
 select distinct A.fsvideo_id + 
RIGHT(REPLICATE('0', 2) + CAST(CONVERT(varchar(2),A.fnbreak_no) as NVARCHAR), 2) 
from tbpgm_combine_queue A
left join TBLOG_VIDEO B  on  A.FSPROG_ID= B.FSID and A.FNEPISODE=B.FNEPISODE 
 and A.FSVIDEO_ID=B.FSVIDEO_PROG
 where CONVERT(VARCHAR(20) ,fddate , 111)= CONVERT(VARCHAR(20) ,getdate() , 111)
 and B.FCFILE_STATUS<>'T' and B.FCFILE_STATUS<>'Y'
 ))
GO
