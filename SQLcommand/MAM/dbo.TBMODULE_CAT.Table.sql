USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBMODULE_CAT](
	[FSMOUDULE_CAT_ID] [varchar](2) NOT NULL,
	[FSMODULE_CAT_NAME] [nvarchar](50) NULL,
 CONSTRAINT [PK_TBMODULE_CAT] PRIMARY KEY CLUSTERED 
(
	[FSMOUDULE_CAT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模組類別代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMODULE_CAT', @level2type=N'COLUMN',@level2name=N'FSMOUDULE_CAT_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模組類別名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMODULE_CAT', @level2type=N'COLUMN',@level2name=N'FSMODULE_CAT_NAME'
GO
