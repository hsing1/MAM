USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        
-- Create date: 
-- Description:   新增入庫影像檔
-- Update desc:    <2013/04/17><Jarvis.Yang>加入註解。
-- =============================================
CREATE Proc [dbo].[SP_I_TBLOG_VIDEO]
@FSFILE_NO char(16) ,
@FSSUBJECT_ID char(12) ,
@FSTYPE char(1) ,
@FSID varchar(11) ,
@FNEPISODE smallint ,
@FSSUPERVISOR char(1) ,
@FSPLAY char(1) ,
@FSARC_TYPE char(3) ,
@FSFILE_TYPE_HV varchar(5) ,
@FSFILE_TYPE_LV varchar(5) ,
@FSTITLE nvarchar(200) ,
@FSDESCRIPTION nvarchar(200) ,
@FSFILE_SIZE nvarchar(50) ,
@FCFILE_STATUS char(1) ,
@FSCHANGE_FILE_NO varchar(20) ,
@FSOLD_FILE_NAME nvarchar(200) ,
@FSFILE_PATH_H nvarchar(200) ,
@FSFILE_PATH_L nvarchar(200) ,
@FNDIR_ID nvarchar(100) ,
@FSCHANNEL_ID char(2) ,
@FSBEG_TIMECODE char(11) ,
@FSEND_TIMECODE char(11) ,
@FSBRO_ID char(12) ,
@FSARC_ID char(12) ,
@FSJOB_ID Bigint ,
@FCFROM char(1) ,
@FSVIDEO_PROG char(8),
@FSTAPE_ID  varchar(50),
@FSCREATED_BY varchar(50)  ,
@FSUPDATED_BY varchar(50) ,
@FCLOW_RES char(1) ,
@FCKEYFRAME char(1) ,
@FNENC_CNT int ,
@FSENC_MSG ntext ,
@FDENC_DATE datetime ,
@FCINGEST_QC char(1) ,
@FSINGEST_QC_MSG nvarchar(100),
@FSTRACK nvarchar(8)

As
declare  @canInsert bit
set @canInsert=1

if(@FSARC_TYPE='001' or @FSARC_TYPE='002')
begin
if(exists(select * from TBLOG_VIDEO v join TBBROADCAST b on b.FSBRO_ID=v.FSBRO_ID where v.FSID=@FSID and v.fnepisode=@fnepisode and v.FSARC_TYPE=@FSARC_TYPE and @FSCHANGE_FILE_NO='' and b.FCCHECK_STATUS in('N','R','U','E') and v.FCFILE_STATUS not in('D','X','F')))
--if(exists(select * from TBLOG_VIDEO v join TBBROADCAST b on b.FSBRO_ID=v.FSBRO_ID where v.FSID=@FSID and v.fnepisode=@fnepisode and v.FSARC_TYPE=@FSARC_TYPE and @FSCHANGE_FILE_NO='' and b.FCCHECK_STATUS in('N','R','U','E') and v.FCFILE_STATUS not in('D','X','F')))
begin
set @canInsert=0
end
else
begin
set @canInsert=1
end
end


if(@canInsert=1)
begin
Insert Into TBLOG_VIDEO(FSFILE_NO,FSSUBJECT_ID,FSTYPE,FSID,FNEPISODE,FSSUPERVISOR,FSPLAY,FSARC_TYPE,FSFILE_TYPE_HV,FSFILE_TYPE_LV,FSTITLE,FSDESCRIPTION,FSFILE_SIZE,FCFILE_STATUS,FSCHANGE_FILE_NO,FSOLD_FILE_NAME,FSFILE_PATH_H,FSFILE_PATH_L,FNDIR_ID,FSCHANNEL_ID,FSBEG_TIMECODE,FSEND_TIMECODE,FSBRO_ID,FSARC_ID,FSJOB_ID,FCFROM,FSVIDEO_PROG,FSTAPE_ID,FSCREATED_BY,FDCREATED_DATE,FSUPDATED_BY,FDUPDATED_DATE,FCLOW_RES,FCKEYFRAME,FNENC_CNT,FSENC_MSG,FDENC_DATE,FCINGEST_QC,FSINGEST_QC_MSG,[FSTRACK])
Values(@FSFILE_NO,@FSSUBJECT_ID,@FSTYPE,@FSID,@FNEPISODE,@FSSUPERVISOR,@FSPLAY,@FSARC_TYPE,@FSFILE_TYPE_HV,@FSFILE_TYPE_LV,@FSTITLE,@FSDESCRIPTION,@FSFILE_SIZE,@FCFILE_STATUS,@FSCHANGE_FILE_NO,@FSOLD_FILE_NAME,@FSFILE_PATH_H,@FSFILE_PATH_L,@FNDIR_ID,@FSCHANNEL_ID,@FSBEG_TIMECODE,@FSEND_TIMECODE,@FSBRO_ID,@FSARC_ID,@FSJOB_ID,@FCFROM,@FSVIDEO_PROG,@FSTAPE_ID,@FSCREATED_BY,GETDATE(),@FSUPDATED_BY,GETDATE(),@FCLOW_RES,@FCKEYFRAME,@FNENC_CNT,@FSENC_MSG,@FDENC_DATE,@FCINGEST_QC,@FSINGEST_QC_MSG,@FSTRACK)
end
else
begin
 RAISERROR ('重複新增LogVideo', -- Message text.
               16, -- Severity.
               1 -- State.
               );
end

GO
