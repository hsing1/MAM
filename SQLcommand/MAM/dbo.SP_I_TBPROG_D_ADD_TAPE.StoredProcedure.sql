USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_I_TBPROG_D_ADD_TAPE]

@FSPROG_ID	char(7),
@FNEPISODE	int,
@FSWORNING_PROMO_ID	varchar(11)	,
@FSHEAD_PROMO_ID	varchar(11),	
@FCStandard1	bit	,
@FCStandard2	bit	,
@FCStandard3	bit	,
@FCStandard4	bit	,
@FCStandard5	bit	,
@FCStandard6	bit	,
@FSCREATED_BY	varchar(50)	

As
Insert Into dbo.TBPROG_D_ADD_TAPE(FSPROG_ID,
FNEPISODE,FSWORNING_PROMO_ID,FSHEAD_PROMO_ID,
FCStandard1,FCStandard2,FCStandard3,FCStandard4,FCStandard5,FCStandard6,
FSCREATED_BY,FDCREATED_DATE)
Values(@FSPROG_ID,
@FNEPISODE,@FSWORNING_PROMO_ID,@FSHEAD_PROMO_ID,
@FCStandard1,@FCStandard2,@FCStandard3,@FCStandard4,@FCStandard5,@FCStandard6,
@FSCREATED_BY,GETDATE())
GO
