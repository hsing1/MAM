USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/06/03>
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[SP_D_TBZCODE_SET]
	@FSCODE_ID			varchar(10),
	@FSCODE_CODE		varchar(5)
AS
 
BEGIN
	DELETE FROM
		[TBZCODE]
	WHERE
		(FSCODE_ID = @FSCODE_ID) AND  ([FSCODE_CODE] = @FSCODE_CODE)
		
	IF (@@ROWCOUNT = 0)	
		BEGIN
			SELECT RESULT = 'ERROR:沒有受影響的資料列, 刪除失敗'
		END	
	ELSE
		BEGIN
			SELECT RESULT = ''
		END		
END
 


GO
