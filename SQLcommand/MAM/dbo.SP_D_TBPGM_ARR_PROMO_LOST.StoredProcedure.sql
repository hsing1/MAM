USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/24>
-- Description:   刪除宣傳帶排播表遺失清單
-- =============================================
CREATE Proc [dbo].[SP_D_TBPGM_ARR_PROMO_LOST]
@FDDATE	date,
@FSCHANNEL_ID	varchar(2)

As
delete from TBPGM_ARR_PROMO_LOST where 
FSCHANNEL_ID=@FSCHANNEL_ID and FDDATE=@FDDATE 
and FSSTATUS='Y'

GO
