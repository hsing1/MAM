USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBUSER_DEP_SIGNATURE_PRIORITY](
	[FNDEP_ID] [int] NOT NULL,
	[FSSignatureID] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_TBUSER_DEP_SIGNATURE_PRIORITY] PRIMARY KEY CLUSTERED 
(
	[FNDEP_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'要強制設定FLOW簽核的部門代號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_DEP_SIGNATURE_PRIORITY', @level2type=N'COLUMN',@level2name=N'FNDEP_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'簽核者的ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_DEP_SIGNATURE_PRIORITY', @level2type=N'COLUMN',@level2name=N'FSSignatureID'
GO
