USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <David.Sin>
-- Create date:   <2010/10/02>
-- Description:   根據節目ID取得得獎地點
-- =============================================



CREATE FUNCTION [dbo].[FN_Q_TBPROGRACE_AREA_BY_PROGID](@FSPROG_ID char(7),@FNEPISODE int)
RETURNS NVARCHAR(max)
AS
BEGIN
	DECLARE @FSAREA_TOTAL NVARCHAR(max)
	SET @FSAREA_TOTAL=''
	SELECT DISTINCT
		@FSAREA_TOTAL = ISNULL((SELECT FSNAME + ',' FROM TBPROGRACE A JOIN TBZPROGRACE_AREA B ON A.FSAREA=B.FSID 
									WHERE C.FSPROG_ID = A.FSPROG_ID AND C.FNEPISODE = A.FNEPISODE FOR XML PATH('')),'')
	FROM TBPROG_D C
	WHERE C.FSPROG_ID=@FSPROG_ID AND C.FNEPISODE=@FNEPISODE
	--DECLARE @FSAREA NVARCHAR(max)
	
	--DECLARE CURSOR_A CURSOR FOR
	--SELECT FSNAME AS FSAREA_NAME 
	--FROM TBPROGRACE A JOIN TBZPROGRACE_AREA B ON A.FSAREA=B.FSID
	--WHERE FSPROG_ID=@FSPROG_ID AND FNEPISODE=@FNEPISODE
	
	--OPEN CURSOR_A
	--FETCH NEXT FROM CURSOR_A INTO @FSAREA
	--WHILE @@FETCH_STATUS=0
	--BEGIN
	--	IF @FSAREA<>''
	--	BEGIN
	--		SET @FSAREA_TOTAL = @FSAREA_TOTAL + @FSAREA + ','
	--	END
	--	FETCH NEXT FROM CURSOR_A INTO @FSAREA
	--END
	--CLOSE CURSOR_A
	
	RETURN @FSAREA_TOTAL
END





GO
