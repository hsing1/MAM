USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <David.Sin>
-- Create date:   <2010/10/02>
-- Description:   查詢預借清單檔案是否存在
-- =============================================

CREATE PROC [dbo].[SP_Q_CHECK_PREBOOKING_FILE_EXIST]
@FSUSER_ID varchar(20),
@FSFILE_NO varchar(16),
@FCBOOKING_TYPE char(1)
AS

SELECT COUNT(FNNO) AS FNCOUNT FROM TBPREBOOKING 
WHERE FSUSER_ID=@FSUSER_ID AND FSFILE_NO=@FSFILE_NO AND FCBOOKING_TYPE=@FCBOOKING_TYPE
GO
