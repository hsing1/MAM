USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_I_TRANSCODE_MGMT_LOG]

@FNTRANSCODE_ID	BIGINT,
@FSACTION		VARCHAR(1024),
@FSCALLER		VARCHAR(20)

AS

INSERT INTO TBTRANSCODE_MGMT_LOG(FDEXECUTE_TIME,FNTRANSCODE_ID,FSACTION,FSCALLER) VALUES(GETDATE(),@FNTRANSCODE_ID,@FSACTION,@FSCALLER)

GO
