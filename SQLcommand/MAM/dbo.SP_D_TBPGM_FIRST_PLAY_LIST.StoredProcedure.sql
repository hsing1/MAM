USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/24>
-- Description:   刪除首播清單(沒有使用)
-- =============================================
CREATE Proc [dbo].[SP_D_TBPGM_FIRST_PLAY_LIST]

@FSCHANNEL_ID	varchar(2),
@FDDATE		date,
@FSVIDEO_ID	varchar(10)


As
DELETE from dbo.TBPGM_FIRST_PLAY_LIST 
where FSCHANNEL_ID=@FSCHANNEL_ID and FDDATE=@FDDATE and FSVIDEO_ID=@FSVIDEO_ID





	
	
GO
