USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        
-- Create date: 
-- Description: 取得14天內要播出的VIDEO檔案清單
-- Update desc:    <2016/01/04><Mike>加入註解。
-- =============================================
CREATE PROC [dbo].[SP_Q_GET_PLAY_VIDEOID_LIST]

AS
select distinct A.FSPROG_ID as FSID,A.FNEPISODE,D.FSFILE_NO,D.FSVIDEO_ID,B.FCFILE_STATUS,C.FCCHECK_STATUS,D.FCSTATUS,C.FSBRO_ID
from tbpgm_queue A
left join TBLOG_Video B on A.FSPROG_ID=B.FSID and A.FNEPISODE=B.FNEPISODE and B.FSARC_TYPE in ('001', '002') and  (B.[FCFILE_STATUS]   NOT IN ('D','X') or B.[FCFILE_STATUS] is null)
left join tbbroadcast C on B.FSBRO_ID=C.FSBRO_ID
left join TBLOG_Video_HD_MC D on B.FSFILE_NO=D.FSFILE_NO 
inner join TBZCHANNEL E on A.FSCHANNEL_ID=E.FSCHANNEL_ID and E.FSCHANNEL_TYPE='002'
where 
A.FDDATE>=getdate()-1 and A.FDDATE<=getdate()+14 and D.FSVIDEO_ID is not null
union
select distinct A.FSPROMO_ID AS FSID,0,D.FSFILE_NO,D.FSVIDEO_ID,B.FCFILE_STATUS,C.FCCHECK_STATUS,D.FCSTATUS,C.FSBRO_ID
from TBPGM_ARR_PROMO A
left join TBLOG_Video B on A.FSPROMO_ID=B.FSID and B.FNEPISODE=0 and B.FSARC_TYPE in ('001', '002') and  (B.[FCFILE_STATUS]   NOT IN ('D','X') or B.[FCFILE_STATUS] is null)
left join tbbroadcast C on B.FSBRO_ID=C.FSBRO_ID and C.FCCHECK_STATUS='U'
left join TBLOG_Video_HD_MC D on B.FSFILE_NO=D.FSFILE_NO
inner join TBZCHANNEL E on A.FSCHANNEL_ID=E.FSCHANNEL_ID and E.FSCHANNEL_TYPE='002'
where 
A.FDDATE>=getdate()-1 and A.FDDATE<=getdate()+14 and D.FSVIDEO_ID is not null

GO
