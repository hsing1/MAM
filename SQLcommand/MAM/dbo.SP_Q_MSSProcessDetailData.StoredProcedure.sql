USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Jarvis.Yang>
-- Create date: <2013/06/03>
-- Description:	取得轉檔作業中的段落詳細資料
-- =============================================

create PROCEDURE [dbo].[SP_Q_MSSProcessDetailData]
	@VIDEO_ID nvarchar(12)
AS
BEGIN
select 
s.FSFILE_NO as 'FSFILE_NO',
FNSEG_ID ,
s.FCLOW_RES as 'FCLOW_RES',
@VIDEO_ID as 'FSVIDEO_ID',
s.FSBEG_TIMECODE as 'FSBEG_TIMECODE',
s.FSEND_TIMECODE as 'FSEND_TIMECODE',
v.FSCREATED_BY as 'FSCREATED_BY',
(select FSPGDNAME from TBPROG_D as d where d.FSPROG_ID=v.FSID and d.FNEPISODE=v.FNEPISODE) as 'FSPGDNAME',
(select FSPGMNAME from TBPROG_M as m where m.FSPROG_ID=v.FSID) as 'FSPGMNAME',
v.FSID as 'FSID',
v.FNEPISODE as 'FNEPISODE',
FSVIDEO_PROG
from TBLOG_VIDEO_SEG as s
inner join TBLOG_VIDEO as v on s.FSFILE_NO = v.FSFILE_NO

where s.FSVIDEO_ID = @VIDEO_ID and v.FCLOW_RES in('Y','F') and v.FCFILE_STATUS IN ('T', 'Y')
END



/****** Object:  StoredProcedure [dbo].[SP_I_TBLOG_VIDEO_SEG_CHG]    Script Date: 06/06/2013 15:49:53 ******/
SET ANSI_NULLS ON




/****** Object:  StoredProcedure [dbo].[SP_I_TBLOG_VIDEO_SEG_CHG]    Script Date: 06/18/2013 16:16:40 ******/
SET ANSI_NULLS ON

GO
