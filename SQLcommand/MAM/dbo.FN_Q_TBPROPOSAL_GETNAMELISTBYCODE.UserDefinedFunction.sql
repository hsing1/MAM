USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/03/14>
-- Description:	<根據傳入之TBProposal(成案單檔)之FSCHANNEL(預訂播出頻道)代碼串,
--				比對TBZCHANNEL(頻道別代碼檔)後,取回FSCHANNEL_NAME(頻道別名稱)名稱串>
-- =============================================
CREATE FUNCTION [dbo].[FN_Q_TBPROPOSAL_GETNAMELISTBYCODE]
(
	@FSCHANNEL	VARCHAR(100)
)
RETURNS	VARCHAR(500)
AS
BEGIN
	-- Declare the return variable here
		Declare @cnt int, @idx int, @i int, @result VARCHAR(500)

	-- Add the T-SQL statements to compute the return value here
		SELECT @cnt = 0, @idx = 0 , @i = 0, @result = ''

		While @idx < Len(@FSCHANNEL)
		BEGIN
			IF CHARINDEX(';',SUBSTRING(@FSCHANNEL,@idx,Len(@FSCHANNEL)-@idx))<>0
				BEGIN
					Set @idx = @idx + CHARINDEX(';',SUBSTRING(@FSCHANNEL,@idx,Len(@FSCHANNEL)-@idx))
					Set @cnt = @cnt + 1
				END
			ELSE
				BEGIN
					Set @idx = Len(@FSCHANNEL)
				END
		END

		While @i < @cnt +1
		BEGIN 
			Declare @Code nvarchar(20)
			Declare @Name nvarchar(100)
			
			Set @Code = dbo.FN_Q_GETITEMBYINDEX(@FSCHANNEL,@i)
			
			IF(@Code <> '')
			 BEGIN
				Set @Name = ISNULL((SELECT FSCHANNEL_NAME FROM TBZCHANNEL WHERE FSCHANNEL_ID = @Code),@Code)
				Set @result = @result + @Name + ';'			 
			 END

			Set @i = @i + 1
		END

	-- Return the result of the function
		RETURN @result
END

GO
