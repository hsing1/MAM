USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   查詢就命帶清單資料
-- =============================================
CREATE Proc [dbo].[SP_Q_TBPGM_SAVE_TAPE_LIST]
@FSPROG_ID	char(7),
@FNSEQNO	int,
@FDDATE	date,
@FSCHANNEL_ID	varchar(2)

As
select A.FSPROG_ID,A.FNSEQNO,A.FDDATE,A.FSCHANNEL_ID,A.FSBEG_TIME,A.FSEND_TIME,A.FSPROG_NAME,
A.FNEPISODE,A.FSTAPENO,A.FNSAVE_NO,A.FSSAVE_PROG_ID,A.FNSAVE_EPISODE,A.FSSAVE_TAPENO,
A.FSSAVE_LENGTH,B.FSPGDNAME,C.FSCHANNEL_NAME
from TBPGM_SAVE_TAPE_LIST A
left join TBPROG_D B on  A.FSSAVE_PROG_ID=B.FSPROG_ID and A.FNSAVE_EPISODE=B.FNEPISODE 
left join TBZCHANNEL C on A.FSCHANNEL_ID=C.FSCHANNEL_ID
where A.FDDATE=@FDDATE and A.FSCHANNEL_ID=@FSCHANNEL_ID 
and @FSPROG_ID = case when @FSPROG_ID = '' then @FSPROG_ID else A.FSPROG_ID end
and @FNSEQNO = case when @FNSEQNO = '' then @FNSEQNO else A.FNSEQNO end
order by A.FNSAVE_NO

GO
