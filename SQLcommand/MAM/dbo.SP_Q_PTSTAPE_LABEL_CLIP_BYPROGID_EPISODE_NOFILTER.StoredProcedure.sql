USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<sam.wu>
-- Create date: <2011/6/1>
-- Description:	<透過節目編號+集別，找到段落資料，不濾掉集別資料>
-- =============================================
CREATE Proc [dbo].[SP_Q_PTSTAPE_LABEL_CLIP_BYPROGID_EPISODE_NOFILTER]
@FSPROG_ID char(7),
@FNEPISODE smallint

as

select *,
ISNULL(CONVERT(VARCHAR(20) ,LabelClip.CreateDT , 111),'') as WANT_FDCREATEDDATE,
ISNULL(CONVERT(VARCHAR(20) ,LabelClip.UpdateDT , 111),'') as WANT_FDUPDATEDDATE
from  [PTSTape].[PTSTape].[dbo].[tblLLabelProg] LabelProg,
[PTSTape].[PTSTape].[dbo].[tblLLabelClip] LabelClip

where 
LabelProg.Bib=LabelClip.bib
and  LabelProg.progid=@FSPROG_ID and LabelProg.epno=@FNEPISODE

GO
