USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_I_TBPROGMEN_EXTERNAL]

@FSPROGID char(7) ,
@FNEPISODE smallint ,
@FSTITLEID varchar(2) ,
@FSEMAIL varchar(50) ,
@FSSTAFFID varchar(5) ,
@FSNAME varchar(100) ,
@FSMEMO varchar(300) ,
@FSCRTUSER varchar(5) 

As
			
INSERT [PTSProg].[PTSProgram].[dbo].TBPROGMEN 
(FSPROGID,FNEPISODE,FSTITLEID,FSEMAIL,FSSTAFFID,FSNAME,FSMEMO,FSCRTUSER,FDCRTDATE)
Values(
@FSPROGID,
case when @FNEPISODE ='0' then null else @FNEPISODE end,
@FSTITLEID,
case when @FSEMAIL ='' then null else @FSEMAIL end,
case when @FSSTAFFID ='' then null else @FSSTAFFID end,
case when @FSNAME ='' then null else @FSNAME end,
case when @FSMEMO ='' then null else @FSMEMO end,
'01065',
GETDATE())

GO
