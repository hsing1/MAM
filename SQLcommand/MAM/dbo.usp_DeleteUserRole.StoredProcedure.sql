USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<吳百川>
-- Create date: <20140718>
-- Description:	<刪除指定使用者權限>
-- =============================================
CREATE PROCEDURE [dbo].[usp_DeleteUserRole](@striUserID nvarchar(10))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	

	IF EXISTS(SELECT * FROM TBUSERS WHERE FSUSER_ID = @striUserID)
	BEGIN
	    IF EXISTS(SELECT * FROM TBUSER_GROUP WHERE FSUSER_ID = @striUserID)
	    BEGIN
	        Delete from TBUSER_GROUP where FSUSER_ID = @striUserID

	        RAISERROR (N'處理結果： 此員工權限已全部刪除', 10, 1);
	    END
	    ELSE
	    BEGIN
		    RAISERROR (N'處理結果： 此員工已無權限', 10, 1);
        END
	END
	ELSE
	BEGIN
	    RAISERROR (N'處理結果： 此員工在MAM無資料', 10, 1);
	END

    
END

GO
