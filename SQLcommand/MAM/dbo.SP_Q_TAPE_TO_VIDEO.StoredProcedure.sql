USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_Q_TAPE_TO_VIDEO]
AS

SELECT
'G' AS FSTYPE,
Label.BIB AS FSBIB,
Label.Title AS FSTAPE_TITLE,
LabelProg.ProgID AS FSPROG_ID,
LabelProg.EpNo AS FNPROG_EP,
CONVERT(VARCHAR(10),Label.CreateDT,111) AS FDCREATED_DATE,
prog_m.FSPGMNAME AS FSPROG_M_NAME,
prog_d.FSPGDNAME AS FSPROG_D_NAME,
CASE ISNULL(prog_d.FSCHANNEL_ID,'')
	WHEN '' THEN '01'
END AS FSCHANNEL_ID
FROM
TBPROG_D prog_d
LEFT JOIN 
TBPROG_M prog_m ON prog_d.FSPROG_ID=prog_m.FSPROG_ID
LEFT JOIN 
[PTSTape].[PTSTape].[dbo].[tblLLabelProg] LabelProg 
	ON prog_d.FSPROG_ID=LabelProg.ProgID AND prog_d.FNEPISODE=LabelProg.EpNo
LEFT JOIN 
[PTSTape].[PTSTape].[dbo].[tblLLabel] Label ON LabelProg.BIB=Label.BIB

--[PTSTape].[PTSTape].[dbo].[tblLLabelProg] LabelProg
--LEFT JOIN
--[PTSTape].[PTSTape].[dbo].[tblLLabel] Label ON LabelProg.BIB=Label.BIB
--LEFT JOIN 
--TBPROG_M prog_m ON LabelProg.ProgID=prog_m.FSPROG_ID 
--LEFT JOIN 
--TBPROG_D prog_d ON LabelProg.ProgID=prog_d.FSPROG_ID And LabelProg.EpNo = prog_d.FNEPISODE




GO
