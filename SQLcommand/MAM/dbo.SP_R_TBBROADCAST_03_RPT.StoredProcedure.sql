USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_R_TBBROADCAST_03_RPT]
	@FSPRINTID		CHAR(36),
	@QUERY_BY		VARCHAR(50),	--查詢的使用者
	@FSSESSION_ID	VARCHAR(255)	--確認查詢權限的FSSESSION_ID
AS
BEGIN
	-------------------	以下, 宣告並賦予值給本次查詢的QUERY_KEY, 作為查詢篩選條件 -------------------<< 此段落可延用不需要修改 >>

	DECLARE @AVATIME_MIN SMALLINT = 5				--預設為五分鐘有效, 後續可以考慮改讀取設定檔
	DECLARE @QUERY_KEY UNIQUEIDENTIFIER = NEWID()	--請留意只有在SQL2008以上才可以在宣告時直接給值, by Dennis	
	DECLARE @TIME_NOW DATETIME = GETDATE()
	DECLARE @TIME_SESSION DATETIME = '1900/01/01'
	DECLARE @NODATA NVARCHAR(20) = 'Y'
	
	SELECT	@TIME_SESSION = ISNULL(FDLAST_ACCESS_TIME,'1900/01/01'),
			@NODATA	=  ISNULL(FSUSER_ID,'Y')		--當取不到資料時, @NODATA會為'Y'
	FROM TBUSER_SESSION
	WHERE FSSESSION_ID = @FSSESSION_ID
	
	IF(@NODATA = 'Y')
		BEGIN
			INSERT TBLOG_ERRORQRY(QRY_TYPE, QRY_ITEM, QRY_BY, QRY_DATE, NOTE)
			VALUES ('REPORT', 'RPT_TBBROADCAST_02', @QUERY_BY, GETDATE(), 'SESSION_NOTEXIST')
			--SELECT QUERY_KEY = 'ERROR:SESSION_NOTEXIST'
			SELECT
				檔案編號		= 'SESSION_NOTEXIST',
				送帶轉檔單編號	= '',
				短帶編號		= '',
				短帶名稱		= '您的登入資訊不存在，請重新登入。',
				送帶轉檔_標題		= '',
				送帶轉檔_檔案類型	= '',
				段落檔_TimeCode起	= '',
				段落檔_TimeCode迄	= '',
				QUERY_BY = @QUERY_BY,
				列印人員 = @QUERY_BY
		END
	ELSE IF(@TIME_NOW > DATEADD(minute, @AVATIME_MIN, @TIME_SESSION))	--若現在時間(@TIME_NOW)已經超過了前一次動作時間(@TIME_SESSION)+允許時間(@AVATIME_MIN)
		BEGIN
		-------------------	以下, 檢查使用者的查詢權限有無過期, 過期的無效查詢紀錄於TBLOG_ERRORQRY, NOTE為'SESSION_TIMEOUT'
			INSERT TBLOG_ERRORQRY(QRY_TYPE, QRY_ITEM, QRY_BY, QRY_DATE, NOTE)
			VALUES ('REPORT', 'RPT_TBBROADCAST_02', @QUERY_BY, GETDATE(), 'SESSION_TIMEOUT')
			--SELECT QUERY_KEY = 'ERROR:SESSION_TIMEOUT'
			SELECT				
				檔案編號		= 'SESSION_TIMEOUT',
				送帶轉檔單編號	= '',
				短帶編號		= '',
				短帶名稱		= '您的登入資訊已過期，請重新登入。',
				送帶轉檔_標題		= '',
				送帶轉檔_檔案類型	= '',
				段落檔_TimeCode起	= '',
				段落檔_TimeCode迄	= '',	
				QUERY_BY = @QUERY_BY,
				列印人員 = @QUERY_BY
		END
	ELSE
		BEGIN 
			SELECT
				檔案編號		= VDO.FSFILE_NO,
				送帶轉檔單編號	= BROD.FSBRO_ID,

				短帶編號	= VDO.FSID,
				短帶名稱	= ISNULL(PROMO.FSPROMO_NAME,'???'),				
	
	            VIDEO_ID=SUBSTRING(VDO.fsVIDEO_PROG,3,LEN(VDO.fsVIDEO_PROG)-2),
				送帶轉檔_標題		= VDO.FSTITLE,
				送帶轉檔_檔案類型	= FTP.FSTYPE_NAME,
				段落檔_TimeCode起	= VDO.FSBEG_TIMECODE,
				段落檔_TimeCode迄	= VDO.FSEND_TIMECODE,	
                時長=dbo.CaculateDuration(VDO.FSBEG_TIMECODE,VDO.FSEND_TIMECODE),

				QUERY_BY = @QUERY_BY,				
				列印人員 = ISNULL(USERS.FSUSER_ChtName,@QUERY_BY)		    /*列印人員*/
	
			FROM
				TBBROADCAST AS BROD
	
				LEFT JOIN TBLOG_VIDEO AS VDO ON (BROD.FSBRO_ID = VDO.FSBRO_ID)
				LEFT JOIN TBPGM_PROMO AS PROMO ON (VDO.FSID = PROMO.FSPROMO_ID)
				LEFT JOIN TBFILE_TYPE AS FTP ON (FTP.FSARC_TYPE = VDO.FSARC_TYPE)
				LEFT JOIN TBUSERS AS USERS ON (USERS.FSUSER_ID =  @QUERY_BY)					/*使用者檔		    =>  列印人員*/
			WHERE
				(BROD.FSPRINTID = @FSPRINTID)	
		END
END

GO
