USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_SEARCH_VIDEO]
AS
SELECT          ISNULL(A.FSFILE_NO, '') + ISNULL(CONVERT(varchar(10), A.FNSEQ_NO), '0') AS FSSYS_ID, ISNULL(A.FSFILE_NO, '') 
                            AS FSFILE_NO, CASE A.FSTYPE WHEN NULL 
                            THEN '' WHEN 'G' THEN '節目' WHEN 'P' THEN 'Promo' WHEN 'D' THEN '其他' END AS FSTYPE, ISNULL(A.FSID, '') 
                            AS FSID, ISNULL(A.FNEPISODE, '') AS FNEPISODE, ISNULL(A.FSTITLE, '') AS FSTITLE, ISNULL(A.FSDESCRIPTION, '') 
                            AS FSDESCRIPTION, ISNULL(A.FNDIR_ID, '') AS FNDIR_ID, U1.FSUSER_ChtName AS FSCREATED_BY, 
                            /*dbo.FN_Q_GET_USERNAME_BY_USERID(ISNULL(A.FSCREATED_BY,'')) As FSCREATED_BY,*/ CONVERT(varchar(10),
                             ISNULL(A.FDCREATED_DATE, ''), 111) AS FDCREATED_DATE, U2.FSUSER_ChtName AS FSUPDATED_BY, 
                            /*dbo.FN_Q_GET_USERNAME_BY_USERID(ISNULL(A.FSUPDATED_BY,'')) As FSUPDATED_BY,*/ CONVERT(varchar(10),
                             ISNULL(A.FDUPDATED_DATE, ''), 111) AS FDUPDATED_DATE, ISNULL(A.FCFROM, '') AS FCFROM, 
                            ISNULL(H.FSTYPE_NAME, '') AS FSTYPE_NAME, ISNULL(A.FSTAPE_ID, '') AS FSTAPE_ID, ISNULL(A.FSTRANS_FROM,
                             '') AS FSTRANS_FROM, /*節目*/ ISNULL(C.FSPROG_ID, '') + '-' + CONVERT(varchar(5), C.FNEPISODE) 
                            AS FSPROD_NO, ISNULL(B.FSPGMNAME, '') AS FSPROG_B_PGMNAME, ISNULL(B.FSPGMENAME, '') 
                            AS FSPROG_B_PGMENAME, ISNULL(B.FSCHANNEL, '') AS FSPROG_B_CHANNEL, CONVERT(varchar(10), 
                            ISNULL(B.FDSHOWDATE, ''), 111) AS FSPROG_B_SHOWDATE, ISNULL(B.FSCONTENT, '') AS FSPROG_B_CONTENT, 
                            ISNULL(B.FSMEMO, '') AS FSPROG_B_MEMO, ISNULL(B.FSEWEBPAGE, '') AS FSPROG_B_EWEBPAGE, 
                            ISNULL(C.FSPGDNAME, '') AS FSPROG_D_PGDNAME, ISNULL(C.FSPGDENAME, '') AS FSPROG_D_PGDENAME, 
                            ISNULL(C.FSPRDYEAR, '') AS FSPROG_D_PRDYEAR, ISNULL(C.FSPROGGRADE, '') AS FSPROG_D_PROGGRADE, 
                            ISNULL(C.FSCONTENT, '') AS FSPROG_D_CONTENT, ISNULL(C.FSMEMO, '') AS FSPROG_D_MEMO, 
                            /*Dir*/ ISNULL(D .FSDESCIPTION, '') AS FSDIR_DESCIPTION, /*subject*/ ISNULL(E.FSSUBJECT, '') 
                            AS FSSUBJECT_SUBJECT, ISNULL(E.FSDESCRIPTION, '') AS FSSUBJECT_DESCRIPTION, 
                            /*channel*/ ISNULL(F.FSSHOWNAME, '') AS FSCHANNEL, 
                            /*dbo.fnKeyframe_Desc(A.FSFILE_NO) AS FSKEY_FRAME,*/ tbKEYFRAME.FSKEY_FRAME, 
                            /*參展記錄*/ tbPROGRACE.FSPROGRACE_AREA AS FSPROGRACE_AREA, tbPROGRACE.FSPROGRACE_PROGRACE, 
                            tbPROGRACE.FDPROGRACE_RACEDATE AS FDPROGRACE_RACEDATE, 
                            tbPROGRACE.FSPROGRACE_PROGSTATUS AS FSPROGRACE_PROGSTATUS, 
                            tbPROGRACE.FNPROGRACE_PERIOD AS FNPROGRACE_PERIOD, 
                            tbPROGRACE.FSPROGRACE_PROGWIN AS FSPROGRACE_PROGWIN, 
                            tbPROGRACE.FSPROGRACE_PROGNAME AS FSPROGRACE_PROGNAME, 
                            /*相關人員與備註*/ tbPROGMEN.FSPROGRACE_NAME AS FSPROGRACE_NAME, 
                            tbPROGMEN.FSPROGRACE_MEMO AS FSPROGRACE_MEMO, 
                            /*製作人*/ tbPRODUCER.FSPROG_PRODUCER AS FSPROG_PRODUCER, '/頻道/' + CASE ISNULL(F.FSSHOWNAME, 
                            '') WHEN '' THEN '公視' ELSE F.FSSHOWNAME END + '/' + CASE A.FSTYPE WHEN NULL 
                            THEN '' WHEN 'G' THEN '節目' WHEN 'P' THEN 'Promo' WHEN 'D' THEN '其他' END + ';/日期/' + CONVERT(VARCHAR(4),
                             YEAR(A.FDCREATED_DATE)) + '/' + CASE CONVERT(VARCHAR(4), MONTH(A.FDCREATED_DATE)) 
                            WHEN '1' THEN '01' WHEN '2' THEN '02' WHEN '3' THEN '03' WHEN '4' THEN '04' WHEN '5' THEN '05' WHEN '6' THEN
                             '06' WHEN '7' THEN '07' WHEN '8' THEN '08' WHEN '9' THEN '09' WHEN '10' THEN '10' WHEN '11' THEN '11' WHEN
                             '12' THEN '12' WHEN '01' THEN '01' WHEN '02' THEN '02' WHEN '03' THEN '03' WHEN '04' THEN '04' WHEN '05' THEN
                             '05' WHEN '06' THEN '06' WHEN '07' THEN '07' WHEN '08' THEN '08' WHEN '09' THEN '09' END + ';/類別/' + CASE A.FSTYPE
                             WHEN NULL THEN '' WHEN 'G' THEN '節目' WHEN 'P' THEN 'Promo' WHEN 'D' THEN '其他' END AS refiner1, 
                            'VW_SEARCH_VIDEO' AS FSVWNAME, A.FSATTRIBUTE1, A.FSATTRIBUTE2, A.FSATTRIBUTE3, A.FSATTRIBUTE4, 
                            A.FSATTRIBUTE5, A.FSATTRIBUTE6, A.FSATTRIBUTE7, A.FSATTRIBUTE8, A.FSATTRIBUTE9, A.FSATTRIBUTE10, 
                            A.FSATTRIBUTE11, A.FSATTRIBUTE12, A.FSATTRIBUTE13, A.FSATTRIBUTE14, A.FSATTRIBUTE15, 
                            A.FSATTRIBUTE16, A.FSATTRIBUTE17, A.FSATTRIBUTE18, A.FSATTRIBUTE19, A.FSATTRIBUTE20, 
                            A.FSATTRIBUTE21, A.FSATTRIBUTE22, A.FSATTRIBUTE23, A.FSATTRIBUTE24, A.FSATTRIBUTE25, 
                            A.FSATTRIBUTE26, A.FSATTRIBUTE27, A.FSATTRIBUTE28, A.FSATTRIBUTE29, A.FSATTRIBUTE30, 
                            A.FSATTRIBUTE31, A.FSATTRIBUTE32, A.FSATTRIBUTE33, A.FSATTRIBUTE34, A.FSATTRIBUTE35, 
                            A.FSATTRIBUTE36, A.FSATTRIBUTE37, A.FSATTRIBUTE38, A.FSATTRIBUTE39, A.FSATTRIBUTE40, 
                            A.FSATTRIBUTE41, A.FSATTRIBUTE42, A.FSATTRIBUTE43, A.FSATTRIBUTE44, A.FSATTRIBUTE45, 
                            A.FSATTRIBUTE46, A.FSATTRIBUTE47, A.FSATTRIBUTE48, A.FSATTRIBUTE49, A.FSATTRIBUTE50, 
                            ISNULL(G.FSTRACK, '') AS FSTRACK
FROM              TBLOG_VIDEO_D A WITH (NOLOCK) LEFT JOIN
                            TBPROG_M B WITH (NOLOCK) ON A.FSID = B.FSPROG_ID LEFT JOIN
                            TBPROG_D C WITH (NOLOCK) ON A.FNEPISODE = C.FNEPISODE AND B.FSPROG_ID = C.FSPROG_ID LEFT JOIN
                            TBDIRECTORIES D WITH (NOLOCK) ON A.FNDIR_ID = D .FNDIR_ID LEFT JOIN
                            TBSUBJECT E WITH (NOLOCK) ON A.FSSUBJECT_ID = E.FSSUBJECT_ID LEFT JOIN
                            TBZCHANNEL F WITH (NOLOCK) ON A.FSCHANNEL_ID = F.FSCHANNEL_ID LEFT JOIN
                            TBLOG_VIDEO G WITH (NOLOCK) ON A.FSFILE_NO = G.FSFILE_NO LEFT JOIN
                            TBFILE_TYPE H WITH (NOLOCK) ON G.FSARC_TYPE = H.FSARC_TYPE LEFT JOIN
                                (SELECT          C.[FSFILE_NO], ISNULL
                                                                  ((SELECT          FSDESCRIPTION + ','
                                                                      FROM              TBLOG_VIDEO_KEYFRAME A
                                                                      WHERE          C.FSFILE_NO = A.FSFILE_NO FOR XML PATH('')), '') AS FSKEY_FRAME
                                  FROM               TBLOG_VIDEO C) AS tbKEYFRAME ON A.FSFILE_NO = tbKEYFRAME.FSFILE_NO LEFT JOIN
                                (SELECT          C.[FSPROG_ID], C.[FNEPISODE], ISNULL
                                                                  ((SELECT          FSNAME + ','
                                                                      FROM              TBPROGRACE A JOIN
                                                                                                  TBZPROGRACE_AREA B ON A.FSAREA = B.FSID
                                                                      WHERE          C.FSPROG_ID = A.FSPROG_ID AND C.FNEPISODE = A.FNEPISODE FOR 
                                                                                                  XML PATH('')), '') AS FSPROGRACE_AREA, ISNULL
                                                                  ((SELECT          FSPROGRACE + ','
                                                                      FROM              TBPROGRACE A
                                                                      WHERE          C.FSPROG_ID = A.FSPROG_ID AND C.FNEPISODE = A.FNEPISODE FOR 
                                                                                                  XML PATH('')), '') AS FSPROGRACE_PROGRACE, ISNULL
                                                                  ((SELECT          CONVERT(VARCHAR(20), [FDRACEDATE], 121) + ','
                                                                      FROM              TBPROGRACE A
                                                                      WHERE          C.FSPROG_ID = A.FSPROG_ID AND C.FNEPISODE = A.FNEPISODE FOR 
                                                                                                  XML PATH('')), '') AS FDPROGRACE_RACEDATE, ISNULL
                                                                  ((SELECT          FSPROGSTATUS + ','
                                                                      FROM              TBPROGRACE A
                                                                      WHERE          C.FSPROG_ID = A.FSPROG_ID AND C.FNEPISODE = A.FNEPISODE FOR 
                                                                                                  XML PATH('')), '') AS FSPROGRACE_PROGSTATUS, ISNULL
                                                                  ((SELECT          CONVERT(VARCHAR(10), FNPERIOD) + ','
                                                                      FROM              TBPROGRACE A
                                                                      WHERE          C.FSPROG_ID = A.FSPROG_ID AND C.FNEPISODE = A.FNEPISODE FOR 
                                                                                                  XML PATH('')), '') AS FNPROGRACE_PERIOD, ISNULL
                                                                  ((SELECT          FSPROGWIN + ','
                                                                      FROM              TBPROGRACE A
                                                                      WHERE          C.FSPROG_ID = A.FSPROG_ID AND C.FNEPISODE = A.FNEPISODE FOR 
                                                                                                  XML PATH('')), '') AS FSPROGRACE_PROGWIN, ISNULL
                                                                  ((SELECT          A.FSPROGNAME + ','
                                                                      FROM              TBPROGRACE A
                                                                      WHERE          C.FSPROG_ID = A.FSPROG_ID AND C.FNEPISODE = A.FNEPISODE FOR 
                                                                                                  XML PATH('')), '') AS FSPROGRACE_PROGNAME
                                  FROM               TBPROGRACE C
                                  GROUP BY    C.[FSPROG_ID], C.[FNEPISODE]) AS tbPROGRACE ON A.FSID = tbPROGRACE.FSPROG_ID AND 
                            A.FNEPISODE = tbPROGRACE.FNEPISODE LEFT JOIN
                                (SELECT          C.[FSPROG_ID], C.[FNEPISODE], ISNULL
                                                                  ((SELECT          A.FSNAME + ','
                                                                      FROM              TBPROGMEN A
                                                                      WHERE          C.FSPROG_ID = A.FSPROG_ID AND C.FNEPISODE = A.FNEPISODE FOR 
                                                                                                  XML PATH('')), '') AS FSPROGRACE_NAME, ISNULL
                                                                  ((SELECT          A.FSMEMO + ','
                                                                      FROM              TBPROGMEN A
                                                                      WHERE          C.FSPROG_ID = A.FSPROG_ID AND C.FNEPISODE = A.FNEPISODE FOR 
                                                                                                  XML PATH('')), '') AS FSPROGRACE_MEMO
                                  FROM               TBPROG_D C) AS tbPROGMEN ON A.FSID = tbPROGMEN.FSPROG_ID AND 
                            A.FNEPISODE = tbPROGMEN.FNEPISODE LEFT JOIN
                                (SELECT          C.[FSPROG_ID], C.[FNEPISODE], ISNULL
                                                                  ((SELECT          B.FSUSER_ChtName + ','
                                                                      FROM              TBPROG_PRODUCER A JOIN
                                                                                                  TBUSERS B ON A.FSPRODUCER = B.FSUSER_ID
                                                                      WHERE          C.FSPROG_ID = A.FSID FOR XML PATH('')), '') AS FSPROG_PRODUCER
                                  FROM               TBPROG_D C) AS tbPRODUCER ON A.FSID = tbPRODUCER.FSPROG_ID AND 
                            A.FNEPISODE = tbPRODUCER.FNEPISODE LEFT JOIN
                            TBUSERS U1 ON A.FSCREATED_BY = U1.FSUSER_ID LEFT JOIN
                            TBUSERS U2 ON A.FSUPDATED_BY = U2.FSUSER_ID
WHERE          A.FSTYPE = 'G' AND A.FCFILE_STATUS IN ('Y', 'T')
UNION ALL
SELECT          ISNULL(A.FSFILE_NO, '') + ISNULL(CONVERT(varchar(10), A.FNSEQ_NO), '0') AS FSSYS_ID, ISNULL(A.FSFILE_NO, '') 
                            AS FSFILE_NO, CASE A.FSTYPE WHEN NULL 
                            THEN '' WHEN 'G' THEN '節目' WHEN 'P' THEN 'Promo' WHEN 'D' THEN '其他' END AS FSTYPE, ISNULL(A.FSID, '') 
                            AS FSID, ISNULL(A.FNEPISODE, '') AS FNEPISODE, ISNULL(A.FSTITLE, '') AS FSTITLE, ISNULL(A.FSDESCRIPTION, '') 
                            AS FSDESCRIPTION, ISNULL(A.FNDIR_ID, '') AS FNDIR_ID, U1.FSUSER_ChtName AS FSCREATED_BY, 
                            /*dbo.FN_Q_GET_USERNAME_BY_USERID(ISNULL(A.FSCREATED_BY,'')) As FSCREATED_BY,*/ CONVERT(varchar(10),
                             ISNULL(A.FDCREATED_DATE, ''), 111) AS FDCREATED_DATE, U2.FSUSER_ChtName AS FSUPDATED_BY, 
                            /*dbo.FN_Q_GET_USERNAME_BY_USERID(ISNULL(A.FSUPDATED_BY,'')) As FSUPDATED_BY,*/ CONVERT(varchar(10),
                             ISNULL(A.FDUPDATED_DATE, ''), 111) AS FDUPDATED_DATE, ISNULL(A.FCFROM, '') AS FCFROM, 
                            ISNULL(H.FSTYPE_NAME, '') AS FSTYPE_NAME, ISNULL(A.FSTAPE_ID, '') AS FSTAPE_ID, ISNULL(A.FSTRANS_FROM,
                             '') AS FSTRANS_FROM, /*PROMO*/ ISNULL(B.FSPROMO_ID, '') AS FSPROD_NO, ISNULL(B.FSPROMO_NAME, '') 
                            AS FSPROG_B_PGMNAME, ISNULL(B.FSPROMO_NAME_ENG, '') AS FSPROG_B_PGMENAME, 
                            ISNULL(B.FSMAIN_CHANNEL_ID, '') AS FSPROG_B_CHANNEL, '' AS FSPROG_B_SHOWDATE, 
                            '' AS FSPROG_B_CONTENT, ISNULL(B.FSMEMO, '') AS FSPROG_B_MEMO, '' AS FSPROG_B_EWEBPAGE, 
                            '' AS FSPROG_D_PGDNAME, '' AS FSPROG_D_PGDENAME, '' AS FSPROG_D_PRDYEAR, 
                            '' AS FSPROG_D_PROGGRADE, '' AS FSPROG_D_CONTENT, '' AS FSPROG_D_MEMO, 
                            /*Dir*/ ISNULL(D .FSDESCIPTION, '') AS FSDIR_DESCIPTION, /*subject*/ ISNULL(E.FSSUBJECT, '') 
                            AS FSSUBJECT_SUBJECT, ISNULL(E.FSDESCRIPTION, '') AS FSSUBJECT_DESCRIPTION, 
                            /*channel*/ ISNULL(F.FSSHOWNAME, '') AS FSCHANNEL, 
                            /*dbo.fnKeyframe_Desc(A.FSFILE_NO) AS FSKEY_FRAME,*/ tbKEYFRAME.FSKEY_FRAME, '' AS FSPROGRACE_AREA, 
                            '' AS FSPROGRACE_PROGRACE, '' AS FDPROGRACE_RACEDATE, '' AS FSPROGRACE_PROGSTATUS, 
                            '' AS FNPROGRACE_PERIOD, '' AS FSPROGRACE_PROGWIN, '' AS FSPROGRACE_PROGNAME, 
                            '' AS FSPROGRACE_NAME, '' AS FSPROGRACE_MEMO, 
                            /*dbo.FN_Q_TBPROG_PRODUCER_NAME_BY_PROGID(A.FSID) AS FSPROG_PRODUCER,*/ tbPRODUCER.FSPROG_PRODUCER,
                             '/頻道/' + CASE ISNULL(F.FSSHOWNAME, '') 
                            WHEN '' THEN '公視' ELSE F.FSSHOWNAME END + '/' + CASE A.FSTYPE WHEN NULL 
                            THEN '' WHEN 'G' THEN '節目' WHEN 'P' THEN 'Promo' WHEN 'D' THEN '其他' END + ';/日期/' + CONVERT(VARCHAR(4),
                             YEAR(A.FDCREATED_DATE)) + '/' + CASE CONVERT(VARCHAR(4), MONTH(A.FDCREATED_DATE)) 
                            WHEN '1' THEN '01' WHEN '2' THEN '02' WHEN '3' THEN '03' WHEN '4' THEN '04' WHEN '5' THEN '05' WHEN '6' THEN
                             '06' WHEN '7' THEN '07' WHEN '8' THEN '08' WHEN '9' THEN '09' WHEN '10' THEN '10' WHEN '11' THEN '11' WHEN
                             '12' THEN '12' WHEN '01' THEN '01' WHEN '02' THEN '02' WHEN '03' THEN '03' WHEN '04' THEN '04' WHEN '05' THEN
                             '05' WHEN '06' THEN '06' WHEN '07' THEN '07' WHEN '08' THEN '08' WHEN '09' THEN '09' END + ';/類別/' + CASE A.FSTYPE
                             WHEN NULL THEN '' WHEN 'G' THEN '節目' WHEN 'P' THEN 'Promo' WHEN 'D' THEN '其他' END AS refiner1, 
                            'VW_SEARCH_VIDEO' AS FSVWNAME, A.FSATTRIBUTE1, A.FSATTRIBUTE2, A.FSATTRIBUTE3, A.FSATTRIBUTE4, 
                            A.FSATTRIBUTE5, A.FSATTRIBUTE6, A.FSATTRIBUTE7, A.FSATTRIBUTE8, A.FSATTRIBUTE9, A.FSATTRIBUTE10, 
                            A.FSATTRIBUTE11, A.FSATTRIBUTE12, A.FSATTRIBUTE13, A.FSATTRIBUTE14, A.FSATTRIBUTE15, 
                            A.FSATTRIBUTE16, A.FSATTRIBUTE17, A.FSATTRIBUTE18, A.FSATTRIBUTE19, A.FSATTRIBUTE20, 
                            A.FSATTRIBUTE21, A.FSATTRIBUTE22, A.FSATTRIBUTE23, A.FSATTRIBUTE24, A.FSATTRIBUTE25, 
                            A.FSATTRIBUTE26, A.FSATTRIBUTE27, A.FSATTRIBUTE28, A.FSATTRIBUTE29, A.FSATTRIBUTE30, 
                            A.FSATTRIBUTE31, A.FSATTRIBUTE32, A.FSATTRIBUTE33, A.FSATTRIBUTE34, A.FSATTRIBUTE35, 
                            A.FSATTRIBUTE36, A.FSATTRIBUTE37, A.FSATTRIBUTE38, A.FSATTRIBUTE39, A.FSATTRIBUTE40, 
                            A.FSATTRIBUTE41, A.FSATTRIBUTE42, A.FSATTRIBUTE43, A.FSATTRIBUTE44, A.FSATTRIBUTE45, 
                            A.FSATTRIBUTE46, A.FSATTRIBUTE47, A.FSATTRIBUTE48, A.FSATTRIBUTE49, A.FSATTRIBUTE50, 
                            ISNULL(G.FSTRACK, '') AS FSTRACK
FROM              TBLOG_VIDEO_D A WITH (NOLOCK) LEFT JOIN
                            TBPGM_PROMO B WITH (NOLOCK) ON A.FSID = B.FSPROMO_ID LEFT JOIN
                            TBDIRECTORIES D WITH (NOLOCK) ON A.FNDIR_ID = D .FNDIR_ID LEFT JOIN
                            TBSUBJECT E WITH (NOLOCK) ON A.FSSUBJECT_ID = E.FSSUBJECT_ID LEFT JOIN
                            TBZCHANNEL F WITH (NOLOCK) ON A.FSCHANNEL_ID = F.FSCHANNEL_ID LEFT JOIN
                            TBLOG_VIDEO G WITH (NOLOCK) ON A.FSFILE_NO = G.FSFILE_NO LEFT JOIN
                            TBFILE_TYPE H WITH (NOLOCK) ON 
                            G.FSARC_TYPE = H.FSARC_TYPE /*LEFT JOIN TBUSERS K WITH(NOLOCK) ON K.FSUSER_ID=J.FSPRODUCER*/ LEFT
                             JOIN
                                (SELECT          C.[FSFILE_NO], ISNULL
                                                                  ((SELECT          FSDESCRIPTION + ','
                                                                      FROM              TBLOG_VIDEO_KEYFRAME A
                                                                      WHERE          C.FSFILE_NO = A.FSFILE_NO FOR XML PATH('')), '') AS FSKEY_FRAME
                                  FROM               TBLOG_VIDEO C) AS tbKEYFRAME ON A.FSFILE_NO = tbKEYFRAME.FSFILE_NO LEFT JOIN
                                (SELECT          C.[FSPROG_ID], C.[FNEPISODE], ISNULL
                                                                  ((SELECT          B.FSUSER_ChtName + ','
                                                                      FROM              TBPROG_PRODUCER A JOIN
                                                                                                  TBUSERS B ON A.FSPRODUCER = B.FSUSER_ID
                                                                      WHERE          C.FSPROG_ID = A.FSID FOR XML PATH('')), '') AS FSPROG_PRODUCER
                                  FROM               TBPROG_D C) AS tbPRODUCER ON A.FSID = tbPRODUCER.FSPROG_ID AND 
                            A.FNEPISODE = tbPRODUCER.FNEPISODE LEFT JOIN
                            TBUSERS U1 ON A.FSCREATED_BY = U1.FSUSER_ID LEFT JOIN
                            TBUSERS U2 ON A.FSUPDATED_BY = U2.FSUSER_ID
WHERE          A.FSTYPE = 'P' AND A.FCFILE_STATUS IN ('Y', 'T')
UNION ALL
/*非屬節目*/ SELECT ISNULL(A.FSFILE_NO, '') + ISNULL(CONVERT(varchar(10), A.FNSEQ_NO), '0') AS FSSYS_ID, 
                            ISNULL(A.FSFILE_NO, '') AS FSFILE_NO, CASE A.FSTYPE WHEN NULL 
                            THEN '' WHEN 'G' THEN '節目' WHEN 'P' THEN 'Promo' WHEN 'D' THEN '其他' END AS FSTYPE, ISNULL(A.FSID, '') 
                            AS FSID, ISNULL(A.FNEPISODE, '') AS FNEPISODE, ISNULL(A.FSTITLE, '') AS FSTITLE, ISNULL(A.FSDESCRIPTION, '') 
                            AS FSDESCRIPTION, ISNULL(A.FNDIR_ID, '') AS FNDIR_ID, U1.FSUSER_ChtName AS FSCREATED_BY, 
                            /*dbo.FN_Q_GET_USERNAME_BY_USERID(ISNULL(A.FSCREATED_BY,'')) As FSCREATED_BY,*/ CONVERT(varchar(10),
                             ISNULL(A.FDCREATED_DATE, ''), 111) AS FDCREATED_DATE, U2.FSUSER_ChtName AS FSUPDATED_BY, 
                            /*dbo.FN_Q_GET_USERNAME_BY_USERID(ISNULL(A.FSUPDATED_BY,'')) As FSUPDATED_BY,*/ CONVERT(varchar(10),
                             ISNULL(A.FDUPDATED_DATE, ''), 111) AS FDUPDATED_DATE, ISNULL(A.FCFROM, '') AS FCFROM, 
                            ISNULL(H.FSTYPE_NAME, '') AS FSTYPE_NAME, ISNULL(A.FSTAPE_ID, '') AS FSTAPE_ID, ISNULL(A.FSTRANS_FROM,
                             '') AS FSTRANS_FROM, '' AS FSPROD_NO, C.FSDATA_NAME AS FSPROG_B_PGMNAME, 
                            '' AS FSPROG_B_PGMENAME, '' AS FSPROG_B_CHANNEL, '' AS FSPROG_B_SHOWDATE, 
                            '' AS FSPROG_B_CONTENT, '' AS FSPROG_B_MEMO, '' AS FSPROG_B_EWEBPAGE, '' AS FSPROG_D_PGDNAME, 
                            '' AS FSPROG_D_PGDENAME, '' AS FSPROG_D_PRDYEAR, '' AS FSPROG_D_PROGGRADE, 
                            '' AS FSPROG_D_CONTENT, '' AS FSPROG_D_MEMO, /*Dir*/ ISNULL(D .FSDESCIPTION, '') AS FSDIR_DESCIPTION, 
                            /*subject*/ ISNULL(E.FSSUBJECT, '') AS FSSUBJECT_SUBJECT, ISNULL(E.FSDESCRIPTION, '') 
                            AS FSSUBJECT_DESCRIPTION, /*channel*/ ISNULL(F.FSSHOWNAME, '') AS FSCHANNEL, 
                            /*dbo.fnKeyframe_Desc(A.FSFILE_NO) AS FSKEY_FRAME,*/ tbKEYFRAME.FSKEY_FRAME, '' AS FSPROGRACE_AREA, 
                            '' AS FSPROGRACE_PROGRACE, '' AS FDPROGRACE_RACEDATE, '' AS FSPROGRACE_PROGSTATUS, 
                            '' AS FNPROGRACE_PERIOD, '' AS FSPROGRACE_PROGWIN, '' AS FSPROGRACE_PROGNAME, 
                            '' AS FSPROGRACE_NAME, '' AS FSPROGRACE_MEMO, '' AS FSPROG_PRODUCER, 
                            '/頻道/' + CASE ISNULL(F.FSSHOWNAME, '') 
                            WHEN '' THEN '公視' ELSE F.FSSHOWNAME END + '/' + CASE A.FSTYPE WHEN NULL 
                            THEN '' WHEN 'G' THEN '節目' WHEN 'P' THEN 'Promo' WHEN 'D' THEN '其他' END + ';/日期/' + CONVERT(VARCHAR(4),
                             YEAR(A.FDCREATED_DATE)) + '/' + CASE CONVERT(VARCHAR(4), MONTH(A.FDCREATED_DATE)) 
                            WHEN '1' THEN '01' WHEN '2' THEN '02' WHEN '3' THEN '03' WHEN '4' THEN '04' WHEN '5' THEN '05' WHEN '6' THEN
                             '06' WHEN '7' THEN '07' WHEN '8' THEN '08' WHEN '9' THEN '09' WHEN '10' THEN '10' WHEN '11' THEN '11' WHEN
                             '12' THEN '12' WHEN '01' THEN '01' WHEN '02' THEN '02' WHEN '03' THEN '03' WHEN '04' THEN '04' WHEN '05' THEN
                             '05' WHEN '06' THEN '06' WHEN '07' THEN '07' WHEN '08' THEN '08' WHEN '09' THEN '09' END + ';/類別/' + CASE A.FSTYPE
                             WHEN NULL THEN '' WHEN 'G' THEN '節目' WHEN 'P' THEN 'Promo' WHEN 'D' THEN '其他' END AS refiner1, 
                            'VW_SEARCH_VIDEO' AS FSVWNAME, A.FSATTRIBUTE1, A.FSATTRIBUTE2, A.FSATTRIBUTE3, A.FSATTRIBUTE4, 
                            A.FSATTRIBUTE5, A.FSATTRIBUTE6, A.FSATTRIBUTE7, A.FSATTRIBUTE8, A.FSATTRIBUTE9, A.FSATTRIBUTE10, 
                            A.FSATTRIBUTE11, A.FSATTRIBUTE12, A.FSATTRIBUTE13, A.FSATTRIBUTE14, A.FSATTRIBUTE15, 
                            A.FSATTRIBUTE16, A.FSATTRIBUTE17, A.FSATTRIBUTE18, A.FSATTRIBUTE19, A.FSATTRIBUTE20, 
                            A.FSATTRIBUTE21, A.FSATTRIBUTE22, A.FSATTRIBUTE23, A.FSATTRIBUTE24, A.FSATTRIBUTE25, 
                            A.FSATTRIBUTE26, A.FSATTRIBUTE27, A.FSATTRIBUTE28, A.FSATTRIBUTE29, A.FSATTRIBUTE30, 
                            A.FSATTRIBUTE31, A.FSATTRIBUTE32, A.FSATTRIBUTE33, A.FSATTRIBUTE34, A.FSATTRIBUTE35, 
                            A.FSATTRIBUTE36, A.FSATTRIBUTE37, A.FSATTRIBUTE38, A.FSATTRIBUTE39, A.FSATTRIBUTE40, 
                            A.FSATTRIBUTE41, A.FSATTRIBUTE42, A.FSATTRIBUTE43, A.FSATTRIBUTE44, A.FSATTRIBUTE45, 
                            A.FSATTRIBUTE46, A.FSATTRIBUTE47, A.FSATTRIBUTE48, A.FSATTRIBUTE49, A.FSATTRIBUTE50, 
                            ISNULL(G.FSTRACK, '') AS FSTRACK
FROM              TBLOG_VIDEO_D A WITH (NOLOCK) LEFT JOIN
                            TBDATA C ON A.FSID = C.FSDATA_ID LEFT JOIN
                            TBDIRECTORIES D WITH (NOLOCK) ON A.FNDIR_ID = D .FNDIR_ID LEFT JOIN
                            TBSUBJECT E WITH (NOLOCK) ON A.FSSUBJECT_ID = E.FSSUBJECT_ID LEFT JOIN
                            TBZCHANNEL F WITH (NOLOCK) ON A.FSCHANNEL_ID = F.FSCHANNEL_ID LEFT JOIN
                            TBLOG_VIDEO G WITH (NOLOCK) ON A.FSFILE_NO = G.FSFILE_NO LEFT JOIN
                            TBFILE_TYPE H WITH (NOLOCK) ON G.FSARC_TYPE = H.FSARC_TYPE LEFT JOIN
                                (SELECT          C.[FSFILE_NO], ISNULL
                                                                  ((SELECT          FSDESCRIPTION + ','
                                                                      FROM              TBLOG_VIDEO_KEYFRAME A
                                                                      WHERE          C.FSFILE_NO = A.FSFILE_NO FOR XML PATH('')), '') AS FSKEY_FRAME
                                  FROM               TBLOG_VIDEO C) AS tbKEYFRAME ON A.FSFILE_NO = tbKEYFRAME.FSFILE_NO LEFT JOIN
                            TBUSERS U1 ON A.FSCREATED_BY = U1.FSUSER_ID LEFT JOIN
                            TBUSERS U2 ON A.FSUPDATED_BY = U2.FSUSER_ID
WHERE          A.FSTYPE = 'D' AND A.FCFILE_STATUS IN ('Y', 'T')

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_SEARCH_VIDEO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_SEARCH_VIDEO'
GO
