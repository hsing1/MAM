USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/06/02>
-- Description:	
-- =============================================
create PROCEDURE [dbo].[SP_I_TBZCODE]
	@FSCODE_ID			varchar(10),
	@FSCODE_CODE		varchar(5),
	@FSCODE_NAME		nvarchar(50),
	@FSCODE_ORDER		varchar(2),
	@FSCODE_NOTE		nvarchar(200),
	@FSCODE_ISENABLED	bit,
	@FSCREATED_BY		varchar(50)
AS
BEGIN
	IF EXISTS(SELECT * FROM [TBZCODE] WHERE ([FSCODE_ID] = @FSCODE_ID) AND ([FSCODE_CODE] = @FSCODE_CODE))
		BEGIN
			SELECT RESULT = 'ERROR:已存在此代碼編號, 請重新設定'
		END
	ELSE IF (@FSCODE_CODE = '')
		BEGIN
			SELECT RESULT = 'ERROR:代碼不可為空字串, 請重新設定'
		END	
	ELSE
		BEGIN
			INSERT [TBZCODE]
				(FSCODE_ID,FSCODE_CODE,FSCODE_NAME,FSCODE_ORDER,FSCODE_NOTE,FSCODE_ISENABLED,
				[FSCREATED_BY],[FDCREATED_DATE],[FSUPDATED_BY],[FDUPDATED_DATE])
			VALUES
				(@FSCODE_ID,@FSCODE_CODE,@FSCODE_NAME,@FSCODE_ORDER,@FSCODE_NOTE,@FSCODE_ISENABLED,
				@FSCREATED_BY, GETDATE(), '', '1900/01/01') 
				
			IF (@@ROWCOUNT = 0)	
				BEGIN
					SELECT RESULT = 'ERROR:沒有受影響的資料列, 新增失敗'
				END	
			ELSE
				BEGIN
					SELECT RESULT = ''
				END		
		END
END


GO
