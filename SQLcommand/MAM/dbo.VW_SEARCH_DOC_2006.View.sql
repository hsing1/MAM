USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO























CREATE View [dbo].[VW_SEARCH_DOC_2006]
AS

Select  ISNULL(A.FSFILE_NO,'') AS FSSYS_ID,
		ISNULL(A.FSFILE_NO,'') As FSFILE_NO,
		CASE A.FSTYPE
			WHEN null THEN ''
			WHEN 'G' THEN '節目'
			WHEN 'P' THEN 'Promo'
		END AS FSTYPE,
		ISNULL(A.FSID,'') As FSID,
		ISNULL(A.FNEPISODE,'') As FNEPISODE,
		ISNULL(A.FSTITLE,'') As FSTITLE,
		ISNULL(A.FSDESCRIPTION,'') As FSDESCRIPTION,
		ISNULL(A.FSCONTENT,'') As FSCONTENT,
		ISNULL(A.FNDIR_ID,'') As FNDIR_ID,
		U1.FSUSER_ChtName AS FSCREATED_BY,
		CONVERT(varchar(10),ISNULL(A.FDCREATED_DATE,''),111) As FDCREATED_DATE,
		U2.FSUSER_ChtName AS FSUPDATED_BY,
		CONVERT(varchar(10),ISNULL(A.FDUPDATED_DATE,''),111) As FDUPDATED_DATE,
		'' AS FSTAPE_ID,
		'' AS FSTRANS_FROM,
		'' AS FCFROM,
		ISNULL(H.FSTYPE_NAME,'') AS FSTYPE_NAME,
		ISNULL(C.FSPROG_ID,'')+'-'+CONVERT(varchar(5),C.FNEPISODE) As FSPROD_NO,
		ISNULL(B.FSPGMNAME,'') As FSPROG_B_PGMNAME,
		ISNULL(B.FSPGMENAME,'') As FSPROG_B_PGMENAME,
		ISNULL(B.FSCHANNEL,'') As FSPROG_B_CHANNEL,
		CONVERT(varchar(10),ISNULL(B.FDSHOWDATE,''),111) As FSPROG_B_SHOWDATE,
		ISNULL(B.FSCONTENT,'') As FSPROG_B_CONTENT,
		ISNULL(B.FSMEMO,'') As FSPROG_B_MEMO,
		ISNULL(B.FSEWEBPAGE,'') As FSPROG_B_EWEBPAGE,
		ISNULL(C.FSPGDNAME,'') As FSPROG_D_PGDNAME,
		ISNULL(C.FSPGDENAME,'') As FSPROG_D_PGDENAME,
		ISNULL(C.FSPRDYEAR,'') As FSPROG_D_PRDYEAR,
		ISNULL(C.FSPROGGRADE,'') As FSPROG_D_PROGGRADE,
		ISNULL(C.FSCONTENT,'') As FSPROG_D_CONTENT,
		ISNULL(C.FSMEMO,'') As FSPROG_D_MEMO,
		--ISNULL(C.FSDPLAY_NAME,'') As FSPROG_D_DPLAY_NAME,
		ISNULL(D.FSDESCIPTION,'') As FSDIR_DESCIPTION,
		ISNULL(E.FSSUBJECT,'') As FSSUBJECT_SUBJECT,
		ISNULL(E.FSDESCRIPTION,'') As FSSUBJECT_DESCRIPTION,
		ISNULL(F.FSSHOWNAME,'') As FSCHANNEL,
		--參展記錄
		tbPROGRACE.FSPROGRACE_AREA AS FSPROGRACE_AREA,
		tbPROGRACE.FSPROGRACE_PROGRACE,
		tbPROGRACE.FDPROGRACE_RACEDATE AS FDPROGRACE_RACEDATE,
		tbPROGRACE.FSPROGRACE_PROGSTATUS AS FSPROGRACE_PROGSTATUS,
		tbPROGRACE.FNPROGRACE_PERIOD AS FNPROGRACE_PERIOD,
		tbPROGRACE.FSPROGRACE_PROGWIN AS FSPROGRACE_PROGWIN,
		tbPROGRACE.FSPROGRACE_PROGNAME AS FSPROGRACE_PROGNAME,
		--相關人員與備註
		tbPROGMEN.FSPROGRACE_NAME AS FSPROGRACE_NAME,
		tbPROGMEN.FSPROGRACE_MEMO AS FSPROGRACE_MEMO,
		--製作人
		tbPRODUCER.FSPROG_PRODUCER AS FSPROG_PRODUCER,
		'/頻道/'+
			CASE ISNULL(F.FSSHOWNAME,'')
				WHEN '' THEN '公視'
				ELSE F.FSSHOWNAME
			END
			+'/'+ 
			CASE A.FSTYPE
			WHEN null THEN ''
			WHEN 'G' THEN '節目'
			WHEN 'P' THEN 'Promo'
		END 
		+ ';/日期/'+
		
		CONVERT(VARCHAR(4),YEAR(A.FDCREATED_DATE))+'/'+
		CASE CONVERT(VARCHAR(4),MONTH(A.FDCREATED_DATE)) 
				When '1' Then '01'
				When '2' Then '02'
				When '3' Then '03'
				When '4' Then '04'
				When '5' Then '05'
				When '6' Then '06'
				When '7' Then '07'
				When '8' Then '08'
				When '9' Then '09'
				When '10' Then '10'
				When '11' Then '11'
				When '12' Then '12'
				When '01' Then '01'
				When '02' Then '02'
				When '03' Then '03'
				When '04' Then '04'
				When '05' Then '05'
				When '06' Then '06'
				When '07' Then '07'
				When '08' Then '08'
				When '09' Then '09'
		End
		+ ';/類別/'+
		CASE A.FSTYPE
			WHEN null THEN ''
			WHEN 'G' THEN '節目'
			WHEN 'P' THEN 'Promo'
		END
		As refiner1,'VW_SEARCH_DOC' As FSVWNAME,
		A.FSATTRIBUTE1,
		A.FSATTRIBUTE2,
		A.FSATTRIBUTE3,
		A.FSATTRIBUTE4,
		A.FSATTRIBUTE5,
		A.FSATTRIBUTE6,
		A.FSATTRIBUTE7,
		A.FSATTRIBUTE8,
		A.FSATTRIBUTE9,
		A.FSATTRIBUTE10,
		A.FSATTRIBUTE11,
		A.FSATTRIBUTE12,
		A.FSATTRIBUTE13,
		A.FSATTRIBUTE14,
		A.FSATTRIBUTE15,
		A.FSATTRIBUTE16,
		A.FSATTRIBUTE17,
		A.FSATTRIBUTE18,
		A.FSATTRIBUTE19,
		A.FSATTRIBUTE20,
		A.FSATTRIBUTE21,
		A.FSATTRIBUTE22,
		A.FSATTRIBUTE23,
		A.FSATTRIBUTE24,
		A.FSATTRIBUTE25,
		A.FSATTRIBUTE26,
		A.FSATTRIBUTE27,
		A.FSATTRIBUTE28,
		A.FSATTRIBUTE29,
		A.FSATTRIBUTE30,
		A.FSATTRIBUTE31,
		A.FSATTRIBUTE32,
		A.FSATTRIBUTE33,
		A.FSATTRIBUTE34,
		A.FSATTRIBUTE35,
		A.FSATTRIBUTE36,
		A.FSATTRIBUTE37,
		A.FSATTRIBUTE38,
		A.FSATTRIBUTE39,
		A.FSATTRIBUTE40,
		A.FSATTRIBUTE41,
		A.FSATTRIBUTE42,
		A.FSATTRIBUTE43,
		A.FSATTRIBUTE44,
		A.FSATTRIBUTE45,
		A.FSATTRIBUTE46,
		A.FSATTRIBUTE47,
		A.FSATTRIBUTE48,
		A.FSATTRIBUTE49,
		A.FSATTRIBUTE50
		
From TBLOG_DOC A WITH(NOLOCK) LEFT JOIN TBPROG_M B WITH(NOLOCK) ON A.FSID=B.FSPROG_ID
					LEFT JOIN TBPROG_D C WITH(NOLOCK) ON A.FNEPISODE=C.FNEPISODE AND B.FSPROG_ID=C.FSPROG_ID
					LEFT JOIN TBDIRECTORIES D WITH(NOLOCK) ON A.FNDIR_ID = D.FNDIR_ID
					LEFT JOIN TBSUBJECT E WITH(NOLOCK) ON A.FSSUBJECT_ID = E.FSSUBJECT_ID
					LEFT JOIN TBZCHANNEL F WITH(NOLOCK) ON A.FSCHANNEL_ID = F.FSCHANNEL_ID
					LEFT JOIN TBFILE_TYPE H WITH(NOLOCK) ON A.FSARC_TYPE = H.FSARC_TYPE
					--LEFT JOIN TBPROGRACE I WITH(NOLOCK) ON I.FSPROG_ID=A.FSID AND I.FNEPISODE=A.FNEPISODE
					-- LEFT JOIN TBPROG_PRODUCER J WITH(NOLOCK) ON J.FSID=A.FSID
					-- LEFT JOIN TBUSERS K WITH(NOLOCK) ON K.FSUSER_ID=J.FSPRODUCER
					LEFT JOIN
						(
							SELECT C.[FSPROG_ID],C.[FNEPISODE], 
									ISNULL((SELECT FSNAME + ',' FROM TBPROGRACE A JOIN TBZPROGRACE_AREA B ON A.FSAREA=B.FSID 
									WHERE C.FSPROG_ID = A.FSPROG_ID AND C.FNEPISODE = A.FNEPISODE FOR XML PATH('')),'') AS FSPROGRACE_AREA,
									ISNULL((SELECT FSPROGRACE + ',' FROM TBPROGRACE A
									WHERE C.FSPROG_ID = A.FSPROG_ID AND C.FNEPISODE = A.FNEPISODE FOR XML PATH('')),'') AS FSPROGRACE_PROGRACE,
									ISNULL((SELECT CONVERT(VARCHAR(20),[FDRACEDATE],121)+ ',' FROM TBPROGRACE A 
									WHERE C.FSPROG_ID = A.FSPROG_ID AND C.FNEPISODE = A.FNEPISODE FOR XML PATH('')),'') AS FDPROGRACE_RACEDATE,
									ISNULL((SELECT FSPROGSTATUS+ ',' FROM TBPROGRACE A 
									WHERE C.FSPROG_ID = A.FSPROG_ID AND C.FNEPISODE = A.FNEPISODE FOR XML PATH('')),'') AS FSPROGRACE_PROGSTATUS,
									ISNULL((SELECT CONVERT(VARCHAR(10),FNPERIOD)+ ',' FROM TBPROGRACE A 
									WHERE C.FSPROG_ID = A.FSPROG_ID AND C.FNEPISODE = A.FNEPISODE FOR XML PATH('')),'') AS FNPROGRACE_PERIOD,
									ISNULL((SELECT FSPROGWIN+ ',' FROM TBPROGRACE A 
									WHERE C.FSPROG_ID = A.FSPROG_ID AND C.FNEPISODE = A.FNEPISODE FOR XML PATH('')),'') AS FSPROGRACE_PROGWIN,
									ISNULL((SELECT A.FSPROGNAME+ ',' FROM TBPROGRACE A 
									WHERE C.FSPROG_ID = A.FSPROG_ID AND C.FNEPISODE = A.FNEPISODE FOR XML PATH('')),'') AS FSPROGRACE_PROGNAME
							FROM TBPROGRACE C
						) AS tbPROGRACE ON A.FSID = tbPROGRACE.FSPROG_ID AND A.FNEPISODE=tbPROGRACE.FNEPISODE
					LEFT JOIN
						(
							SELECT C.[FSPROG_ID],C.[FNEPISODE], 
								ISNULL((SELECT A.FSNAME+ ',' FROM TBPROGMEN A 
								WHERE C.FSPROG_ID = A.FSPROG_ID AND C.FNEPISODE = A.FNEPISODE FOR XML PATH('')),'') AS FSPROGRACE_NAME,
								ISNULL((SELECT A.FSMEMO+ ',' FROM TBPROGMEN A 
								WHERE C.FSPROG_ID = A.FSPROG_ID AND C.FNEPISODE = A.FNEPISODE FOR XML PATH('')),'') AS FSPROGRACE_MEMO
							FROM TBPROG_D C
						) AS tbPROGMEN ON A.FSID = tbPROGMEN.FSPROG_ID AND A.FNEPISODE=tbPROGMEN.FNEPISODE
					 LEFT JOIN
						(
							SELECT C.[FSPROG_ID],C.[FNEPISODE],
								ISNULL((SELECT B.FSUSER_ChtName +',' FROM TBPROG_PRODUCER A JOIN TBUSERS B ON A.FSPRODUCER=B.FSUSER_ID
									WHERE C.FSPROG_ID = A.FSID FOR XML PATH('')),'') AS FSPROG_PRODUCER
							FROM TBPROG_D C
						) AS tbPRODUCER ON A.FSID=tbPRODUCER.FSPROG_ID AND A.FNEPISODE=tbPRODUCER.FNEPISODE
					LEFT JOIN TBUSERS U1 ON A.FSCREATED_BY = U1.FSUSER_ID
					LEFT JOIN TBUSERS U2 ON A.FSUPDATED_BY = U2.FSUSER_ID
WHERE A.FSTYPE='G' AND YEAR(A.FDCREATED_DATE) Between 2006 And 2010 AND A.FCFILE_STATUS IN('Y','T')

UNION

Select  ISNULL(A.FSFILE_NO,'') AS FSSYS_ID,
		ISNULL(A.FSFILE_NO,'') As FSFILE_NO,
		CASE A.FSTYPE
			WHEN null THEN ''
			WHEN 'G' THEN '節目'
			WHEN 'P' THEN 'Promo'
		END AS FSTYPE,
		ISNULL(A.FSID,'') As FSID,
		ISNULL(A.FNEPISODE,'') As FNEPISODE,
		ISNULL(A.FSTITLE,'') As FSTITLE,
		ISNULL(A.FSDESCRIPTION,'') As FSDESCRIPTION,
		ISNULL(A.FSCONTENT,'') As FSCONTENT,
		ISNULL(A.FNDIR_ID,'') As FNDIR_ID,
		U1.FSUSER_ChtName AS FSCREATED_BY,
		CONVERT(varchar(10),ISNULL(A.FDCREATED_DATE,''),111) As FDCREATED_DATE,
		U2.FSUSER_ChtName AS FSUPDATED_BY,
		CONVERT(varchar(10),ISNULL(A.FDUPDATED_DATE,''),111) As FDUPDATED_DATE,
		'' AS FSTAPE_ID,
		'' AS FSTRANS_FROM,
		'' AS FCFROM,
		ISNULL(H.FSTYPE_NAME,'') AS FSTYPE_NAME,
		--PROMO
		ISNULL(B.FSPROMO_ID,'') As FSPROD_NO,
		ISNULL(B.FSPROMO_NAME,'') As FSPROG_B_PGMNAME,
		ISNULL(B.FSPROMO_NAME_ENG,'') As FSPROG_B_PGMENAME,
		ISNULL(B.FSMAIN_CHANNEL_ID,'') As FSPROG_B_CHANNEL,
		'' As FSPROG_B_SHOWDATE,
		'' As FSPROG_B_CONTENT,
		ISNULL(B.FSMEMO,'') As FSPROG_B_MEMO,
		'' As FSPROG_B_EWEBPAGE,
		'' As FSPROG_D_PGDNAME,
		'' As FSPROG_D_PGDENAME,
		'' As FSPROG_D_PRDYEAR,
		'' As FSPROG_D_PROGGRADE,
		'' As FSPROG_D_CONTENT,
		'' As FSPROG_D_MEMO,
		--ISNULL(C.FSDPLAY_NAME,'') As FSPROG_D_DPLAY_NAME,
		ISNULL(D.FSDESCIPTION,'') As FSDIR_DESCIPTION,
		ISNULL(E.FSSUBJECT,'') As FSSUBJECT_SUBJECT,
		ISNULL(E.FSDESCRIPTION,'') As FSSUBJECT_DESCRIPTION,
		ISNULL(F.FSSHOWNAME,'') As FSCHANNEL,
		--參展記錄
		'' AS FSPROGRACE_AREA,
		'' AS FSPROGRACE_PROGRACE,
		'' AS FDPROGRACE_RACEDATE,
		'' AS FSPROGRACE_PROGSTATUS,
		'' AS FNPROGRACE_PERIOD,
		'' AS FSPROGRACE_PROGWIN,
		'' AS FSPROGRACE_PROGNAME,
		--相關人員與備註
		'' AS FSPROGRACE_NAME,
		'' AS FSPROGRACE_MEMO,
		--製作人
		--ISNULL(K.FSUSER_ChtName,'') AS FSPROG_PRODUCER,
		dbo.FN_Q_TBPROG_PRODUCER_NAME_BY_PROGID(A.FSID) AS FSPROG_PRODUCER,
		'/頻道/'+
			CASE ISNULL(F.FSSHOWNAME,'')
				WHEN '' THEN '公視'
				ELSE F.FSSHOWNAME
			END
			+'/'+ 
			CASE A.FSTYPE
			WHEN null THEN ''
			WHEN 'G' THEN '節目'
			WHEN 'P' THEN 'Promo'
		END 
		+ ';/日期/'+
		
		CONVERT(VARCHAR(4),YEAR(A.FDCREATED_DATE))+'/'+
		CASE CONVERT(VARCHAR(4),MONTH(A.FDCREATED_DATE)) 
				When '1' Then '01'
				When '2' Then '02'
				When '3' Then '03'
				When '4' Then '04'
				When '5' Then '05'
				When '6' Then '06'
				When '7' Then '07'
				When '8' Then '08'
				When '9' Then '09'
				When '10' Then '10'
				When '11' Then '11'
				When '12' Then '12'
				When '01' Then '01'
				When '02' Then '02'
				When '03' Then '03'
				When '04' Then '04'
				When '05' Then '05'
				When '06' Then '06'
				When '07' Then '07'
				When '08' Then '08'
				When '09' Then '09'
		End
		+ ';/類別/'+
		CASE A.FSTYPE
			WHEN null THEN ''
			WHEN 'G' THEN '節目'
			WHEN 'P' THEN 'Promo'
		END
		As refiner1,'VW_SEARCH_DOC' As FSVWNAME,
		A.FSATTRIBUTE1,
		A.FSATTRIBUTE2,
		A.FSATTRIBUTE3,
		A.FSATTRIBUTE4,
		A.FSATTRIBUTE5,
		A.FSATTRIBUTE6,
		A.FSATTRIBUTE7,
		A.FSATTRIBUTE8,
		A.FSATTRIBUTE9,
		A.FSATTRIBUTE10,
		A.FSATTRIBUTE11,
		A.FSATTRIBUTE12,
		A.FSATTRIBUTE13,
		A.FSATTRIBUTE14,
		A.FSATTRIBUTE15,
		A.FSATTRIBUTE16,
		A.FSATTRIBUTE17,
		A.FSATTRIBUTE18,
		A.FSATTRIBUTE19,
		A.FSATTRIBUTE20,
		A.FSATTRIBUTE21,
		A.FSATTRIBUTE22,
		A.FSATTRIBUTE23,
		A.FSATTRIBUTE24,
		A.FSATTRIBUTE25,
		A.FSATTRIBUTE26,
		A.FSATTRIBUTE27,
		A.FSATTRIBUTE28,
		A.FSATTRIBUTE29,
		A.FSATTRIBUTE30,
		A.FSATTRIBUTE31,
		A.FSATTRIBUTE32,
		A.FSATTRIBUTE33,
		A.FSATTRIBUTE34,
		A.FSATTRIBUTE35,
		A.FSATTRIBUTE36,
		A.FSATTRIBUTE37,
		A.FSATTRIBUTE38,
		A.FSATTRIBUTE39,
		A.FSATTRIBUTE40,
		A.FSATTRIBUTE41,
		A.FSATTRIBUTE42,
		A.FSATTRIBUTE43,
		A.FSATTRIBUTE44,
		A.FSATTRIBUTE45,
		A.FSATTRIBUTE46,
		A.FSATTRIBUTE47,
		A.FSATTRIBUTE48,
		A.FSATTRIBUTE49,
		A.FSATTRIBUTE50
		
From TBLOG_DOC A WITH(NOLOCK) LEFT JOIN TBPGM_PROMO B WITH(NOLOCK) ON A.FSID=B.FSPROMO_ID
					LEFT JOIN TBPROG_D C WITH(NOLOCK) ON A.FNEPISODE=C.FNEPISODE AND B.FSPROG_ID=C.FSPROG_ID
					LEFT JOIN TBDIRECTORIES D WITH(NOLOCK) ON A.FNDIR_ID = D.FNDIR_ID
					LEFT JOIN TBSUBJECT E WITH(NOLOCK) ON A.FSSUBJECT_ID = E.FSSUBJECT_ID
					LEFT JOIN TBZCHANNEL F WITH(NOLOCK) ON A.FSCHANNEL_ID = F.FSCHANNEL_ID
					LEFT JOIN TBFILE_TYPE H WITH(NOLOCK) ON A.FSARC_TYPE = H.FSARC_TYPE
					LEFT JOIN TBPROG_PRODUCER J WITH(NOLOCK) ON J.FSID=B.FSPROMO_ID
					 LEFT JOIN TBUSERS K WITH(NOLOCK) ON K.FSUSER_ID=J.FSPRODUCER
					LEFT JOIN TBUSERS U1 ON A.FSCREATED_BY = U1.FSUSER_ID
					LEFT JOIN TBUSERS U2 ON A.FSUPDATED_BY = U2.FSUSER_ID
WHERE A.FSTYPE='P' AND YEAR(A.FDCREATED_DATE) Between 2006 And 2010 AND A.FCFILE_STATUS IN('Y','T')












































GO
