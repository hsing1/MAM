USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBSYSTEM_NOTIFY](
	[FNID] [bigint] IDENTITY(1,1) NOT NULL,
	[FSUSER_ID] [nvarchar](50) NOT NULL,
	[FDDATE] [datetime] NOT NULL,
	[FSMESSAGE] [nvarchar](1024) NULL,
	[FNMSG_LEVEL] [int] NOT NULL,
	[FSMSG_TYPE] [nvarchar](50) NULL,
	[FCHAS_READED] [char](1) NOT NULL,
	[FDREAD_TIME] [datetime] NULL,
 CONSTRAINT [PK_TBSYSTEM_NOTIFY] PRIMARY KEY CLUSTERED 
(
	[FNID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[TBSYSTEM_NOTIFY] ADD  CONSTRAINT [DF_TBSYSTEM_NOTIFY_FDDATE]  DEFAULT (getdate()) FOR [FDDATE]
GO
ALTER TABLE [dbo].[TBSYSTEM_NOTIFY] ADD  CONSTRAINT [DF_TBSYSTEM_NOTIFY_FNMSG_LEVEL]  DEFAULT ((0)) FOR [FNMSG_LEVEL]
GO
ALTER TABLE [dbo].[TBSYSTEM_NOTIFY] ADD  CONSTRAINT [DF_TBSYSTEM_NOTIFY_FCHAS_READED]  DEFAULT ('N') FOR [FCHAS_READED]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'記錄號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSYSTEM_NOTIFY', @level2type=N'COLUMN',@level2name=N'FNID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用者編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSYSTEM_NOTIFY', @level2type=N'COLUMN',@level2name=N'FSUSER_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'通知開始時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSYSTEM_NOTIFY', @level2type=N'COLUMN',@level2name=N'FDDATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'通知訊息' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSYSTEM_NOTIFY', @level2type=N'COLUMN',@level2name=N'FSMESSAGE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'訊息等級' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSYSTEM_NOTIFY', @level2type=N'COLUMN',@level2name=N'FNMSG_LEVEL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'訊息類型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSYSTEM_NOTIFY', @level2type=N'COLUMN',@level2name=N'FSMSG_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否已閱讀' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSYSTEM_NOTIFY', @level2type=N'COLUMN',@level2name=N'FCHAS_READED'
GO
