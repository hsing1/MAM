USE [MAM]
GO
CREATE USER [mam_admin] FOR LOGIN [mam_admin] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [mam_admin]
GO
