USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   查詢節目表2天內要播出的節目
-- =============================================
CREATE Proc [dbo].[SP_Q_TBPGM_QUEUE_BY_PROGID_48Hour]
@FSPROG_ID	char(7),
@FNEPISODE	smallint
As
Select 
CONVERT(NVARCHAR(10), A.FDDATE, 111) FDDATE,A.FSBEG_TIME,B.FSCHANNEL_NAME,
ltrim(DATEDIFF(ss, CONVERT(varchar(100), GETDATE(), 20),CONVERT(varchar(100), A.FDDATE, 23) + ' ' + SUBSTRING(A.FSBEG_TIME,1,2) + ':' +SUBSTRING(A.FSBEG_TIME,3,2) + ':00' )/(24*60*60)) +'天'
+ltrim((DATEDIFF(MI, CONVERT(varchar(100), GETDATE(), 20),CONVERT(varchar(100), A.FDDATE, 23) + ' ' + SUBSTRING(A.FSBEG_TIME,1,2) + ':' +SUBSTRING(A.FSBEG_TIME,3,2) + ':00')/60)%24)+'小時'
+ltrim(DATEDIFF(MI, CONVERT(varchar(100), GETDATE(), 20),CONVERT(varchar(100), A.FDDATE, 23) + ' ' + SUBSTRING(A.FSBEG_TIME,1,2) + ':' +SUBSTRING(A.FSBEG_TIME,3,2) + ':00')%(60))+'分'
minusDate
from TBPGM_QUEUE A,TBZCHANNEL B 
where A.FSCHANNEL_ID=B.FSCHANNEL_ID and A.FSBEG_TIME<'2400'
and CONVERT(varchar(100), A.FDDATE, 23) + ' ' + SUBSTRING(A.FSBEG_TIME,1,2) + ':' +SUBSTRING(A.FSBEG_TIME,3,2) + ':00' < CONVERT(varchar(100), GETDATE()+2, 20) 
and CONVERT(varchar(100), A.FDDATE, 23) + ' ' + SUBSTRING(A.FSBEG_TIME,1,2) + ':' +SUBSTRING(A.FSBEG_TIME,3,2) + ':00' > CONVERT(varchar(100), GETDATE(), 20) 
and A.FSPROG_ID = @FSPROG_ID 
and A.FNEPISODE = @FNEPISODE 
union
Select 
CONVERT(NVARCHAR(10), DATEADD (day , 1 , A.FDDATE ), 111)  FDDATE,
 Right('00' + Cast(SUBSTRING(A.FSBEG_TIME,1,2)-24 as nvarchar),2)+SUBSTRING(A.FSBEG_TIME,3,2)  FSBEG_TIME,B.FSCHANNEL_NAME,
ltrim(DATEDIFF(ss, CONVERT(varchar(100), GETDATE(), 20),CONVERT(varchar(100),  DATEADD (day , 1 , A.FDDATE ) , 23) + ' ' 
+ Right('00' + Cast(SUBSTRING(A.FSBEG_TIME,1,2)-24 as nvarchar),2) + ':'  
+SUBSTRING(A.FSBEG_TIME,3,2) + ':00' )/(24*60*60)) +'天'
+ltrim((DATEDIFF(MI, CONVERT(varchar(100), GETDATE(), 20),CONVERT(varchar(100), DATEADD (day , 1 , A.FDDATE ), 23) + ' ' 
+ Right('00' + Cast(SUBSTRING(A.FSBEG_TIME,1,2)-24 as nvarchar),2) + ':' 
+SUBSTRING(A.FSBEG_TIME,3,2) + ':00')/60)%24)+'小時'
+ltrim(DATEDIFF(MI, CONVERT(varchar(100), GETDATE(), 20),CONVERT(varchar(100), DATEADD (day , 1 , A.FDDATE ), 23) + ' ' 
+ Right('00' + Cast(SUBSTRING(A.FSBEG_TIME,1,2)-24 as nvarchar),2) + ':' 
+SUBSTRING(A.FSBEG_TIME,3,2) + ':00')%(60))+'分'
minusDate
from TBPGM_QUEUE A,TBZCHANNEL B 
where A.FSCHANNEL_ID=B.FSCHANNEL_ID and A.FSBEG_TIME>='2400'
and CONVERT(varchar(100), DATEADD (day , 1 , A.FDDATE ), 23) + ' ' 
+ Right('00' + Cast(SUBSTRING(A.FSBEG_TIME,1,2)-24 as nvarchar),2) + ':' 
+SUBSTRING(A.FSBEG_TIME,3,2) + ':00' < CONVERT(varchar(100), GETDATE()+2, 20) 
and CONVERT(varchar(100), DATEADD (day , 1 , A.FDDATE ), 23) + ' ' 
+ Right('00' + Cast(SUBSTRING(A.FSBEG_TIME,1,2)-24 as nvarchar),2) + ':' 
+SUBSTRING(A.FSBEG_TIME,3,2) + ':00' > CONVERT(varchar(100), GETDATE(), 20) 
and A.FSPROG_ID = @FSPROG_ID 
and A.FNEPISODE = @FNEPISODE 





GO
