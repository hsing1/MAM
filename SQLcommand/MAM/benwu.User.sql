USE [MAM]
GO
CREATE USER [benwu] FOR LOGIN [benwu] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_datareader] ADD MEMBER [benwu]
GO
ALTER ROLE [db_denydatawriter] ADD MEMBER [benwu]
GO
