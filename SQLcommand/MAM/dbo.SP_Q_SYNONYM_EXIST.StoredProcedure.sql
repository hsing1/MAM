USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <David.Sin>
-- Create date:   <2010/10/02>
-- Description:   查詢同義詞是否存在
-- =============================================

CREATE PROC [dbo].[SP_Q_SYNONYM_EXIST]
@FSSYNONYM nvarchar(100)
AS

SELECT COUNT(FNNO) AS FNNO_COUNT FROM TBSYNONYM WHERE FSSYNONYM=@FSSYNONYM
GO
