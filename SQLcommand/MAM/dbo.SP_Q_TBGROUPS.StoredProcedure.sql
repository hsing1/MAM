USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE Proc [dbo].[SP_Q_TBGROUPS]
@FSGROUP_ID varchar(50)

As

IF @FSGROUP_ID='' 
BEGIN 
	select FSGROUP_ID, FSGROUP, FSDESCRIPTION, FSCREATED_BY, FDCREATED_DATE, FSUPDATED_BY, FDUPDATED_DATE from TBGROUPS  ORDER BY FSGROUP_ID 
END 

ELSE 
BEGIN 
	select FSGROUP_ID, FSGROUP, FSDESCRIPTION, FSCREATED_BY, FDCREATED_DATE, FSUPDATED_BY, FDUPDATED_DATE from TBGROUPS WHERE FSGROUP_ID = @FSGROUP_ID ORDER BY FSGROUP_ID 
END 



GO
