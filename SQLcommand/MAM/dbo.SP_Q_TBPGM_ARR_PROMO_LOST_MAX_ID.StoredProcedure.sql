USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   取得遺失清單最大號碼料
-- =============================================
CREATE Proc [dbo].[SP_Q_TBPGM_ARR_PROMO_LOST_MAX_ID]
@FDDATE	date,
@FSCHANNEL_ID	varchar(2)


As
Select 
max(FNCREATE_LIST_NO)
from dbo.TBPGM_ARR_PROMO_LOST 
where 
FSCHANNEL_ID=@FSCHANNEL_ID and FDDATE=@FDDATE  

GO
