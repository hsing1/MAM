USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/04/01>
-- Description:	新增或修改MAM端的Clip明細檔
-- =============================================
Create PROC [dbo].[SP_I_TBTAPE_LABEL_CLIP]
	@BIB		varchar(7),
	@TCStart	varchar(11),
	@TCEnd		varchar(11),
	@Duration	varchar(11),
	@ClipTitle	nvarchar(255),
	@Source		nvarchar(255),
	@RTAudio	bit,
	@Desp1 		nvarchar(500),
	@Desp2 		nvarchar(500),
	@Desp3 		nvarchar(500),
	@Desp4 		nvarchar(500),
	@Memo		nvarchar(255),
	@VideoFile	nvarchar(60),
	@TCStart1	varchar(11),
	@Creator	varchar(20),
	@CreateDT	datetime,
	@Updator	varchar(20),
	@UpdateDT	datetime,
	@SID		int,
	@ItemNo		int,
	@EXEC_GUID	varchar(36)
AS

IF EXISTS ( SELECT	BIB, ItemNo
			FROM	TBTAPE_LABEL_CLIP
			WHERE	(BIB = @BIB) AND (ItemNo = @ItemNo) )
	BEGIN
		UPDATE
			TBTAPE_LABEL_CLIP
		SET
			BIB			= @BIB,
			TCStart		= @TCStart,
			TCEnd		= @TCEnd,		
			Duration	= @Duration,
			ClipTitle	= @ClipTitle,	
			[Source]	= @Source,	
			RTAudio		= @RTAudio,	
			Desp1 		= @Desp1,		
			Desp2 		= @Desp2,		
			Desp3 		= @Desp3,		
			Desp4 		= @Desp4,		
			Memo		= @Memo,	
			VideoFile	= @VideoFile,
			TCStart1	= @TCStart1,
			Creator		= @Creator,
			CreateDT	= @CreateDT,	
			Updator		= @Updator,
			UpdateDT	= @UpdateDT,	
			[SID]		= @SID,
			ItemNo		= @ItemNo,
			EXEC_GUID	= @EXEC_GUID	
		WHERE
			(BIB = @BIB) AND (ItemNo = @ItemNo)
	END
ELSE
	BEGIN
		INSERT
			TBTAPE_LABEL_CLIP
			(BIB, TCStart, TCEnd, Duration, ClipTitle,
			[Source], RTAudio, Desp1, Desp2, Desp3,
			Desp4, Memo, VideoFile, TCStart1, Creator,
			CreateDT, Updator, UpdateDT, [SID], ItemNo, EXEC_GUID)
		VALUES
			(@BIB, @TCStart, @TCEnd, @Duration, @ClipTitle,
			@Source, @RTAudio, @Desp1, @Desp2, @Desp3,
			@Desp4, @Memo, @VideoFile, @TCStart1, @Creator,
			@CreateDT, @Updator, @UpdateDT, @SID, @ItemNo, @EXEC_GUID) 				
	END


SELECT	COUNT(*)
FROM	TBTAPE_LABEL_CLIP
WHERE	(BIB = @BIB) AND
		(ItemNo = @ItemNo) AND
		(EXEC_GUID	= @EXEC_GUID)
GO
