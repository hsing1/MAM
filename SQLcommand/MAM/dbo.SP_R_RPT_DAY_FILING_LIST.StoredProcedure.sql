USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2015/04/29>
-- Description:   主控FILING LIST
-- =============================================
CREATE Proc [dbo].[SP_R_RPT_DAY_FILING_LIST]
	@QUERY_KEY	VARCHAR(36)
As
BEGIN
SELECT 
QUERY_KEY,QUERY_KEY_SUBID,QUERY_BY,QUERY_DATE,
播出日期,頻道,FNNO,FSPLAY_TIME,FSIN,VIDEO_ID,FSOUT,節目名稱,集數,長度,FNBREAK_NO,FSMEMO
from RPT_DAY_FILING_LIST
	WHERE
		(QUERY_KEY = @QUERY_KEY)
	ORDER BY
		FNNO
END

GO
