USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/03/30>
-- Description:	把影片管理系統傳來的Title與Episode跟MAM中的TBPROG_進行比對
-- =============================================
CREATE Proc [dbo].[SP_Q_TBPROG_MD_TRN]
	@TITLE		NVARCHAR(50),
	@EPISODE	INT

As
	DECLARE @FSPROG_ID VARCHAR(7) = ''

	SELECT
		@FSPROG_ID = ISNULL(D.FSPROG_ID,'')
	FROM TBPROG_M AS M
		LEFT JOIN TBPROG_D AS D ON (D.FSPROG_ID = M.FSPROG_ID) AND (D.FNEPISODE = @EPISODE)
	WHERE
		M.FSPGMNAME = @TITLE
		
	SELECT
		FSPROG_ID = @FSPROG_ID

GO
