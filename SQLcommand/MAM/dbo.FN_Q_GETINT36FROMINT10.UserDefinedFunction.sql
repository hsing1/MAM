USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/05/16>
-- Description:	10進位轉36進位(其實可套用2~36進位)
-- =============================================
CREATE FUNCTION [dbo].[FN_Q_GETINT36FROMINT10](@val AS BIGINT)
  RETURNS VARCHAR(63)
AS
BEGIN
	DECLARE @base AS INT
	SET @base = 36

  IF @val < 0 OR @base < 2 OR @base > 36 RETURN NULL;
  DECLARE @r AS VARCHAR(63), @alldigits AS VARCHAR(36);

  SET @alldigits = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

  SET @r = '';
  WHILE @val > 0
  BEGIN
    SET @r = SUBSTRING(@alldigits, @val % @base + 1, 1) + @r;
    SET @val = @val / @base;
  END

  RETURN @r;
END
GO
