USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_R_STO_CountV_Analysis] 
	
AS
BEGIN
	select  b. FSSHOWNAME '頻道',
	substring (c. FSTYPE_NAME,1 ,2) '型態' ,
	--a.FSTYPE as '類別',
(case a .fstype when   'P' then '短帶' when 'G' then '節目' else '資料帶' end) as '類別' ,
ROUND(Sum(((cast ((SUBSTRING( isnull(FSEND_TIMECODE ,''), 1,2 )) as real)* 60*60 *30)+
(cast( SUBSTRING(isnull (FSEND_TIMECODE, ''),4 ,2) as real)*60 *30)+
(cast( SUBSTRING(isnull (FSEND_TIMECODE, ''),7 ,2) as real)*30 )+
cast(SUBSTRING (isnull( FSEND_TIMECODE,'' ),10, 2) as real )) -
((cast(( SUBSTRING(isnull (FSBEG_TIMECODE, ''),1 ,2)) as real)*60 *60* 30)+
(cast( SUBSTRING(isnull (FSBEG_TIMECODE, ''),4 ,2) as real)*60 *30)+
(cast( SUBSTRING(isnull (FSBEG_TIMECODE, ''),7 ,2) as real)*30 )+
cast(SUBSTRING (isnull( FSBEG_TIMECODE,'' ),10, 2) as real )) )/ 30.0/60 /60,2)
as '時長(小時)'
from TBLOG_VIDEO a, TBZCHANNEL b, dbo .TBFILE_TYPE c
where FCFILE_STATUS in ('T' ,'Y')
and a. FSCHANNEL_ID = b .FSCHANNEL_ID
and a. FSARC_TYPE = c .FSARC_TYPE
group by   a.fstype , substring (c. FSTYPE_NAME,1 ,2), FSSHOWNAME
order by FSSHOWNAME, substring(c .FSTYPE_NAME, 1,2 ), a. FSTYPE desc

END

GO
