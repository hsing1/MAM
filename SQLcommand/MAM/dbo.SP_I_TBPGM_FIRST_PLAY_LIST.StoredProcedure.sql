USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   新增首播清單資料(應已未使用)
-- =============================================
CREATE Proc [dbo].[SP_I_TBPGM_FIRST_PLAY_LIST]

@FSCHANNEL_ID	varchar(2),
@FDDATE		date,
@FSVIDEO_ID	varchar(10),
@FCTYPE		varchar(1),
@FNVERSION		int

As
Insert Into dbo.TBPGM_FIRST_PLAY_LIST(FSCHANNEL_ID,FDDATE,FSVIDEO_ID,FCTYPE,FDCREATEDATE,FNVERSION)
Values(@FSCHANNEL_ID,@FDDATE,@FSVIDEO_ID,@FCTYPE,GETDATE(),@FNVERSION)






	
	
GO
