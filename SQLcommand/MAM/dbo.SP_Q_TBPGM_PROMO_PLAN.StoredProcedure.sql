USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   查詢預約破口資源資料
-- =============================================
CREATE Proc [dbo].[SP_Q_TBPGM_PROMO_PLAN]

@FSPROG_ID	char(7),
@FSPROG_NAME	nvarchar(50),
@FCSOURCETYPE	varchar(2),
@FSPROMO_BEG_DATE	varchar(50),
@FSPROMO_END_DATE	varchar(50)

As
Select FNNO,FSPROG_ID,FSPROG_NAME,FCSOURCETYPE,FCSOURCE,
FSPLAY_COUNT_MAX,FSPLAY_COUNT_MIN,FNPROMO_TYPEA,FNPROMO_TYPEB,FNPROMO_TYPEC,FNPROMO_TYPED,
FNPROMO_TYPEE,FSPROMO_BEG_DATE,FSPROMO_END_DATE,FSWEEK,FSMEMO From TBPGM_PROMO_PLAN
where FSPROG_NAME like '%' + @FSPROG_NAME + '%'
and @FSPROG_ID = case when @FSPROG_ID = '' then @FSPROG_ID else FSPROG_ID end
and @FCSOURCETYPE = case when @FCSOURCETYPE = '' then @FCSOURCETYPE else FCSOURCETYPE end
and @FSPROMO_BEG_DATE <= case when @FSPROMO_BEG_DATE = '' then @FSPROMO_BEG_DATE else FSPROMO_END_DATE end
and @FSPROMO_END_DATE >= case when @FSPROMO_END_DATE = '' then @FSPROMO_END_DATE else FSPROMO_BEG_DATE end





GO
