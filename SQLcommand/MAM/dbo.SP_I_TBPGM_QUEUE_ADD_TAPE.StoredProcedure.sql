USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_I_TBPGM_QUEUE_ADD_TAPE]

@FSPROG_ID	char(7),
@FNSEQNO	int,
@FDDATE	date,
@FSCHANNEL_ID	varchar(2),
@FSWORNING_PROMO_ID	varchar(11)	,
@FSHEAD_PROMO_ID	varchar(11),	
@FCStandard1	bit	,
@FCStandard2	bit	,
@FCStandard3	bit	,
@FCStandard4	bit	,
@FCStandard5	bit	,
@FCStandard6	bit	,
@FSINSERT_CHANNEL_ID	varchar(2),
@FSCREATED_BY	varchar(50)	

As
Insert Into dbo.TBPGM_QUEUE_ADD_TAPE(FSPROG_ID,FNSEQNO,FDDATE,FSCHANNEL_ID,
FSWORNING_PROMO_ID,FSHEAD_PROMO_ID,
FCStandard1,FCStandard2,FCStandard3,FCStandard4,FCStandard5,FCStandard6,
FSCREATED_BY,FDCREATED_DATE)
Values(@FSPROG_ID,@FNSEQNO,@FDDATE,@FSCHANNEL_ID,
@FSWORNING_PROMO_ID,@FSHEAD_PROMO_ID,
@FCStandard1,@FCStandard2,@FCStandard3,@FCStandard4,@FCStandard5,@FCStandard6,
@FSCREATED_BY,GETDATE())


 
GO
