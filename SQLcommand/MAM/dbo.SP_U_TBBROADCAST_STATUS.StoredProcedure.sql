USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_U_TBBROADCAST_STATUS]
@FSBRO_ID CHAR(12),
@FCCHECK_STATUS CHAR(1),
@FSUPDATED_BY VARCHAR(50)
As

declare @FSSIGNED_BY_NAME nvarchar(50)
set @FSSIGNED_BY_NAME = (select FSUSER_ChtName from MAM.dbo.TBUSERS where FSUSER_ID=@FSUPDATED_BY)
if @FCCHECK_STATUS='G'
begin	
	--print @FSSIGNED_BY_NAME
	UPDATE MAM.dbo.TBBROADCAST
	SET 
		FCCHECK_STATUS=@FCCHECK_STATUS ,
		FSUPDATED_BY=@FSUPDATED_BY,
		FDUPDATED_DATE=GETDATE(),
		FSSIGNED_BY=@FSUPDATED_BY,
		FDSIGNED_DATE=GETDATE(),
		FSSIGNED_BY_NAME=@FSSIGNED_BY_NAME
	WHERE  MAM.dbo.TBBROADCAST.FSBRO_ID=@FSBRO_ID
end
else if (@FCCHECK_STATUS='F') and
	exists(select * from TBBROADCAST WHERE  MAM.dbo.TBBROADCAST.FSBRO_ID=@FSBRO_ID
	and FCCHECK_STATUS = 'Y') begin
	UPDATE MAM.dbo.TBBROADCAST
	SET 
		FCCHECK_STATUS=@FCCHECK_STATUS ,
		FSUPDATED_BY=@FSUPDATED_BY,
		FDUPDATED_DATE=GETDATE(),
		FSSIGNED_BY=@FSUPDATED_BY,
		FDSIGNED_DATE=GETDATE(),
		FSSIGNED_BY_NAME=@FSSIGNED_BY_NAME
	WHERE  MAM.dbo.TBBROADCAST.FSBRO_ID=@FSBRO_ID
END	
else 
begin
	--UPDATE MAM.dbo.TBBROADCAST
	--SET FCCHECK_STATUS=@FCCHECK_STATUS ,FSUPDATED_BY=@FSUPDATED_BY,FDUPDATED_DATE=GETDATE(),FSSIGNED_BY=@FSUPDATED_BY,FDSIGNED_DATE=GETDATE()
	--WHERE  MAM.dbo.TBBROADCAST.FSBRO_ID=@FSBRO_ID		

	UPDATE MAM.dbo.TBBROADCAST
	SET 
		FCCHECK_STATUS=@FCCHECK_STATUS ,
		FSUPDATED_BY=@FSUPDATED_BY,
		FDUPDATED_DATE=GETDATE()
		--FSSIGNED_BY=@FSUPDATED_BY,
		--FDSIGNED_DATE=GETDATE()
	WHERE  MAM.dbo.TBBROADCAST.FSBRO_ID=@FSBRO_ID

	if (@FCCHECK_STATUS='E')
	begin 
	  --declare @oriStatus char(1)
	  --declare @change char(1)
	  --select @change=FCCHANGE from TBBROADCAST  where fsbro_id=@FSBRO_ID

	 -- if(@change='Y')--是送播置換單時
	 -- begin

	  declare @changedFile_no char(16) --找出原本被置換的檔案
	  select @changedFile_no=FSCHANGE_FILE_NO from  TBLOG_VIDEO where fsbro_id=@FSBRO_ID
	  --確認他還沒被刪掉
	  if(ISNULL(@changedFile_no,'')!='')
	  begin
	  if(not Exists(select * from TBDELETED where FSFILE_NO=@changedFile_no and FCSTATUS='N'))
	  begin
	  --將該檔案更換成備置換前的狀態
	  update TBLOG_VIDEO set fcfile_status=FSENC_MSG where fsfile_no=@changedFile_no

	  declare @fsid varchar(11)
	  declare @episode smallint
	  declare @videoID char(8)
	  select @fsid=FSID,@episode=FNEPISODE,@videoID=FSVIDEO_PROG from TBLOG_VIDEO where fsfile_no=@changedFile_no
	  update TBLOG_VIDEOID_MAP set FSVIDEO_ID_PROG=@videoID where FSID=@fsid and FNEPISODE=@episode and FSARC_TYPE='002'
	 
	  end
	  end
	 
	update TBLOG_VIDEO set fcfile_status='X' where fsbro_id=@FSBRO_ID
	 
	end
end
GO
