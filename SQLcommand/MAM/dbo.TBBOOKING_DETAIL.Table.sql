USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBBOOKING_DETAIL](
	[FSBOOKING_NO] [varchar](12) NOT NULL,
	[FSSEQ] [varchar](3) NOT NULL,
	[FSFILE_NO] [varchar](16) NOT NULL,
	[FSSTART_TIMECODE] [varchar](11) NULL,
	[FSBEG_TIMECODE] [varchar](11) NULL,
	[FSEND_TIMECODE] [varchar](11) NULL,
	[FSFILE_TYPE] [varchar](5) NULL,
	[FSFILE_CATEGORY] [char](1) NULL,
	[FNLOCATION] [int] NULL,
	[FCCHECK] [char](1) NULL,
	[FSCHECK_ID] [varchar](50) NULL,
	[FDCHECK_DATE] [varchar](10) NULL,
	[FCSUPERVISOR] [char](1) NULL,
	[FCSUPERVISOR_CHECK] [char](1) NULL,
	[FSSUPERVISOR_CHECK_ID] [varchar](50) NULL,
	[FDSUPERVISOR_CHECK_DATE] [varchar](10) NULL,
	[FCFILE_EXIST] [char](1) NULL,
	[FSGUID] [varchar](500) NULL,
	[FSJOB_ID] [bigint] NULL,
	[FCFILE_TRANSCODE_STATUS] [char](1) NULL,
	[FCALL] [char](1) NULL,
	[FCORIGINAL] [char](1) NULL,
	[FSMEMO] [nvarchar](max) NULL,
	[FSFILE_OUTPUT_INFO] [varchar](300) NULL,
	[FCINTERPLAY] [char](1) NULL,
 CONSTRAINT [ TBBOOKING_DETAIL] PRIMARY KEY CLUSTERED 
(
	[FSBOOKING_NO] ASC,
	[FSSEQ] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
CREATE NONCLUSTERED INDEX [INDEX_GUID] ON [dbo].[TBBOOKING_DETAIL]
(
	[FSGUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDEX_JOBID] ON [dbo].[TBBOOKING_DETAIL]
(
	[FSJOB_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'調用單編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_DETAIL', @level2type=N'COLUMN',@level2name=N'FSBOOKING_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'序號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_DETAIL', @level2type=N'COLUMN',@level2name=N'FSSEQ'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'檔案編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_DETAIL', @level2type=N'COLUMN',@level2name=N'FSFILE_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Timecode起點' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_DETAIL', @level2type=N'COLUMN',@level2name=N'FSSTART_TIMECODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'調用Timecode起點' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_DETAIL', @level2type=N'COLUMN',@level2name=N'FSBEG_TIMECODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'調用Timecode迄點' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_DETAIL', @level2type=N'COLUMN',@level2name=N'FSEND_TIMECODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'檔案類型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_DETAIL', @level2type=N'COLUMN',@level2name=N'FSFILE_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'檔案類別' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_DETAIL', @level2type=N'COLUMN',@level2name=N'FSFILE_CATEGORY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'檔案路徑' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_DETAIL', @level2type=N'COLUMN',@level2name=N'FNLOCATION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'審核結果' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_DETAIL', @level2type=N'COLUMN',@level2name=N'FCCHECK'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'審核者編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_DETAIL', @level2type=N'COLUMN',@level2name=N'FSCHECK_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'審核日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_DETAIL', @level2type=N'COLUMN',@level2name=N'FDCHECK_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否需主管審核' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_DETAIL', @level2type=N'COLUMN',@level2name=N'FCSUPERVISOR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主管審核結果' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_DETAIL', @level2type=N'COLUMN',@level2name=N'FCSUPERVISOR_CHECK'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主管審核者編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_DETAIL', @level2type=N'COLUMN',@level2name=N'FSSUPERVISOR_CHECK_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主管審核日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_DETAIL', @level2type=N'COLUMN',@level2name=N'FDSUPERVISOR_CHECK_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'檔案是否存在' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_DETAIL', @level2type=N'COLUMN',@level2name=N'FCFILE_EXIST'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'GUID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_DETAIL', @level2type=N'COLUMN',@level2name=N'FSGUID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'JOB ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_DETAIL', @level2type=N'COLUMN',@level2name=N'FSJOB_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'轉檔狀態' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_DETAIL', @level2type=N'COLUMN',@level2name=N'FCFILE_TRANSCODE_STATUS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否整段調用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_DETAIL', @level2type=N'COLUMN',@level2name=N'FCALL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否為原生格式' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_DETAIL', @level2type=N'COLUMN',@level2name=N'FCORIGINAL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'備註' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_DETAIL', @level2type=N'COLUMN',@level2name=N'FSMEMO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'轉出檔案資訊' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_DETAIL', @level2type=N'COLUMN',@level2name=N'FSFILE_OUTPUT_INFO'
GO
EXEC sys.sp_addextendedproperty @name=N'描述', @value=N'調用明細表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_DETAIL'
GO
