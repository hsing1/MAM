USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/24>
-- Description:   刪除預約破口資源資料
-- =============================================
CREATE Proc [dbo].[SP_D_TBPGM_PROMO_PLAN]

@FNNO	int
As
Delete from TBPGM_PROMO_PLAN 
where FNNO=@FNNO






GO
