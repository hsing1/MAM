USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_U_TBPROGMEN_EXTERNAL]

@FSPROGID char(7) ,
@FNEPISODE smallint ,
@FSTITLEID varchar(2) ,
@FSEMAIL varchar(50) ,
@FSSTAFFID varchar(5) ,
@FSNAME varchar(100) ,
@FSMEMO varchar(300) ,
@FSUPDUSER varchar(5) 

As
	

UPDATE [PTSProg].[PTSProgram].[dbo].TBPROGMEN --OPENDATASOURCE('SQLOLEDB','Data Source=172.20.141.83;User ID=sa;Password=pts2010').PTSProg.dbo.TBPROGMEN
SET 
FSEMAIL = case when @FSEMAIL ='' then null else @FSEMAIL end,
FSMEMO = case when @FSMEMO ='' then null else @FSMEMO end,
FSSTAFFID = case when @FSSTAFFID ='' then null else @FSSTAFFID end,
FSUPDUSER='01065',
FDUPDDATE=GETDATE()

where FSPROGID =  @FSPROGID 
and isnull(FNEPISODE,'') =  @FNEPISODE 
and isnull(FSTITLEID,'') = @FSTITLEID
and isnull(FSNAME,'') = @FSNAME

GO
