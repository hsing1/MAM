USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jarvis.Yang
-- Create date: 2013-11-29
-- Description:	更新TBDIRECTORIES資料，目前只有資料帶用
-- =============================================
create PROCEDURE [dbo].[SP_U_TBDIRECTORIES]
	@FSDATA_NAME nvarchar(50),
	@FSMEMO nvarchar(50),
	@OriName nvarchar(50),
	@UpdateUser varchar(50)

AS
BEGIN
	update TBDIRECTORIES SET FSDIR=@FSDATA_NAME,FSDESCIPTION=@FSMEMO,FDUPDATED_DATE=GETDATE(),FSUPDATED_BY=@UpdateUser where FSDIR=@OriName
END



SET ANSI_NULLS ON

GO
