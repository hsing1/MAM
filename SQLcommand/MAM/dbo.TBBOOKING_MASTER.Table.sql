USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBBOOKING_MASTER](
	[FSBOOKING_NO] [varchar](12) NOT NULL,
	[FSUSER_ID] [varchar](50) NOT NULL,
	[FDBOOKING_DATE] [varchar](10) NOT NULL,
	[FSFILE_OUTPUT_INFO] [varchar](300) NOT NULL,
	[FSLOGO_INFO] [varchar](300) NOT NULL,
	[FSBOOKING_REASON] [varchar](2) NOT NULL,
	[FSBOOKING_REASON_DESC] [nvarchar](max) NULL,
	[FCCHECK_STATUS] [char](2) NOT NULL,
	[FSCHECK_ID] [varchar](50) NULL,
	[FDCHECK_DATE] [varchar](10) NULL,
	[FCNEED_CHECK_OUT_BUY] [char](1) NULL,
	[FCNEED_CHECK_INTERNATION] [char](1) NULL,
	[FNFLOW_ID] [int] NULL,
 CONSTRAINT [ TBBOOKING_MASTER] PRIMARY KEY CLUSTERED 
(
	[FSBOOKING_NO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'調用單編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_MASTER', @level2type=N'COLUMN',@level2name=N'FSBOOKING_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'調用者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_MASTER', @level2type=N'COLUMN',@level2name=N'FSUSER_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'調用日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_MASTER', @level2type=N'COLUMN',@level2name=N'FDBOOKING_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'轉出檔案資訊' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_MASTER', @level2type=N'COLUMN',@level2name=N'FSFILE_OUTPUT_INFO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'浮水印資訊' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_MASTER', @level2type=N'COLUMN',@level2name=N'FSLOGO_INFO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'調用原因' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_MASTER', @level2type=N'COLUMN',@level2name=N'FSBOOKING_REASON'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'調用原因描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_MASTER', @level2type=N'COLUMN',@level2name=N'FSBOOKING_REASON_DESC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'審核狀態' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_MASTER', @level2type=N'COLUMN',@level2name=N'FCCHECK_STATUS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'審核者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_MASTER', @level2type=N'COLUMN',@level2name=N'FSCHECK_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'審核日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_MASTER', @level2type=N'COLUMN',@level2name=N'FDCHECK_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否需外購審核' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_MASTER', @level2type=N'COLUMN',@level2name=N'FCNEED_CHECK_OUT_BUY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否需宏觀審核' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_MASTER', @level2type=N'COLUMN',@level2name=N'FCNEED_CHECK_INTERNATION'
GO
EXEC sys.sp_addextendedproperty @name=N'描述', @value=N'調用主表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBOOKING_MASTER'
GO
