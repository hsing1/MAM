USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/03/21>
-- Description:	For RPT_TBBOOKING_01(調用清單表), 報表呼叫用
-- =============================================
CREATE PROCEDURE [dbo].[SP_R_TBBOOKING_01_RPT]
	@QUERY_KEY	VARCHAR(36)
AS
BEGIN
	SELECT
		[QUERY_KEY]
      ,[QUERY_KEY_SUBID]
      ,[QUERY_BY]
      ,[QUERY_DATE]
      ,[主檔_調用單號]
      ,[主檔_調用者帳號]
      ,[主檔_調用日期]
      ,[主檔_調用原因]
      ,[主檔_審核狀態]
      ,[主檔_審核人員]
      ,[主檔_審核日期]
      ,[明細_檔案編號]
      ,[明細_標題]
      ,[明細_節目名稱]
      ,[明細_時間起迄點]
      ,[明細_檔案類型]
      ,[明細_審核狀態]
      ,[明細_是否外購]
      ,[SDATE]
      ,[EDATE]
	FROM
		RPT_TBBOOKING_01
	WHERE
		(QUERY_KEY = @QUERY_KEY)
	ORDER BY
		[主檔_調用單號], [明細_檔案編號]
END




GO
