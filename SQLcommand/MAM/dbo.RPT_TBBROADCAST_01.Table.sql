USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RPT_TBBROADCAST_01](
	[QUERY_KEY] [uniqueidentifier] NOT NULL,
	[QUERY_KEY_SUBID] [uniqueidentifier] NOT NULL,
	[QUERY_BY] [varchar](50) NOT NULL,
	[QUERY_DATE] [datetime] NOT NULL,
	[送帶轉檔單編號] [varchar](12) NOT NULL,
	[類型_名稱] [nvarchar](50) NOT NULL,
	[節目_節目名稱] [nvarchar](50) NOT NULL,
	[節目_集別] [smallint] NOT NULL,
	[節目_子集名稱] [nvarchar](50) NOT NULL,
	[節目_播出頻道] [varchar](1700) NOT NULL,
	[節目_總集數] [smallint] NOT NULL,
	[節目_每集時長] [int] NOT NULL,
	[宣傳帶_宣傳帶名稱] [nvarchar](50) NOT NULL,
	[已轉檔_標題] [nvarchar](100) NOT NULL,
	[已轉檔_檔案類型] [nvarchar](20) NOT NULL,
	[已轉檔_起迄] [nvarchar](50) NOT NULL,
	[列印人員] [nvarchar](10) NOT NULL,
	[列印編號] [char](36) NOT NULL,
 CONSTRAINT [PK_RPT_TBBROADCAST_01] PRIMARY KEY CLUSTERED 
(
	[QUERY_KEY] ASC,
	[QUERY_KEY_SUBID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
