USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        
-- Create date: 
-- Description:   查詢短帶託播單
-- Update desc:    <2013/07/03><Jarvis.Yang>加入註解。
-- =============================================
create Proc [dbo].[SP_Q_TBPGM_PROMO_BOOKING_Check] --'20141100007'
@FSPROMO_ID	varchar(12)
As
Select (select FSCHANNEL_NAME from TBZCHANNEL where FSCHANNEL_ID=zone.FSCHANNEL_ID) as FSCHANNEL_NAME,zone.FDBEG_DATE,zone.FDEND_DATE
From dbo.TBPGM_PROMO_BOOKING A
left outer join TBPGM_PROMO_EFFECTIVE_ZONE zone
on A.FNPROMO_BOOKING_NO=zone.FNPROMO_BOOKING_NO
where A.FSPROMO_ID=case when @FSPROMO_ID = '' then A.FSPROMO_ID else @FSPROMO_ID end
order by FDBEG_DATE

   







GO
