USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBTEMPLATE_FIELDS](
	[FNTEMPLATE_ID] [int] NULL,
	[FNID] [int] IDENTITY(1,1) NOT NULL,
	[FSTABLE] [varchar](50) NULL,
	[FSFIELD] [varchar](50) NULL,
	[FSFIELD_NAME] [varchar](50) NULL,
	[FSFIELD_TYPE] [varchar](50) NULL,
	[FNFIELD_LENGTH] [smallint] NULL,
	[FSDESCRIPTION] [varchar](50) NULL,
	[FNORDER] [smallint] NULL,
	[FNOBJECT_WIDTH] [smallint] NULL,
	[FCGRID] [char](1) NULL,
	[FCISNULLABLE] [char](1) NULL,
	[FCLIST] [char](1) NULL,
	[FCLIST_TYPE] [char](1) NULL,
	[FSLIST_NAME] [varchar](20) NULL,
	[FSDEFAULT] [nvarchar](50) NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [smalldatetime] NOT NULL,
	[FSUPDATED_BY] [varchar](50) NULL,
	[FDUPDATED_DATE] [smalldatetime] NULL,
	[FSCODE_ID] [varchar](10) NULL,
	[FSCODE_CNT] [int] NULL,
	[FSCODE_CTRL] [varchar](10) NULL,
 CONSTRAINT [PK_TBTEMPLATE_FIELDS] PRIMARY KEY CLUSTERED 
(
	[FNID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[TBTEMPLATE_FIELDS] ADD  CONSTRAINT [DF_TBTEMPLATE_FIELDS_FCGRID]  DEFAULT ('N') FOR [FCGRID]
GO
ALTER TABLE [dbo].[TBTEMPLATE_FIELDS] ADD  CONSTRAINT [DF_TBTEMPLATE_FIELDS_FCISNULLABLE]  DEFAULT ('Y') FOR [FCISNULLABLE]
GO
ALTER TABLE [dbo].[TBTEMPLATE_FIELDS] ADD  CONSTRAINT [DF_TBTEMPLATE_FIELDS_FCLIST]  DEFAULT ('N') FOR [FCLIST]
GO
ALTER TABLE [dbo].[TBTEMPLATE_FIELDS] ADD  CONSTRAINT [DF_TBTEMPLATE_FIELDS_FCLIST_TYPE]  DEFAULT ('N') FOR [FCLIST_TYPE]
GO
ALTER TABLE [dbo].[TBTEMPLATE_FIELDS] ADD  CONSTRAINT [DF_TBTEMPLATE_FIELDS_FCLIST_NAME]  DEFAULT ('FIELD') FOR [FSLIST_NAME]
GO
ALTER TABLE [dbo].[TBTEMPLATE_FIELDS] ADD  CONSTRAINT [DF_TBTEMPLATE_FIELDS_FSCODE_ID]  DEFAULT ('') FOR [FSCODE_ID]
GO
ALTER TABLE [dbo].[TBTEMPLATE_FIELDS] ADD  CONSTRAINT [DF_TBTEMPLATE_FIELDS_FSCODE_CNT]  DEFAULT ((0)) FOR [FSCODE_CNT]
GO
ALTER TABLE [dbo].[TBTEMPLATE_FIELDS] ADD  CONSTRAINT [DF_TBTEMPLATE_FIELDS_FSCODE_CTRL]  DEFAULT ('') FOR [FSCODE_CTRL]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'選單名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBTEMPLATE_FIELDS', @level2type=N'COLUMN',@level2name=N'FSLIST_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'新增者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBTEMPLATE_FIELDS', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'新增日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBTEMPLATE_FIELDS', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBTEMPLATE_FIELDS', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBTEMPLATE_FIELDS', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'引用的代碼編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBTEMPLATE_FIELDS', @level2type=N'COLUMN',@level2name=N'FSCODE_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'設定的代碼數量上限, 預設1, 0表示不限, 其他正數表上限' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBTEMPLATE_FIELDS', @level2type=N'COLUMN',@level2name=N'FSCODE_CNT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用的控制像' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBTEMPLATE_FIELDS', @level2type=N'COLUMN',@level2name=N'FSCODE_CTRL'
GO
