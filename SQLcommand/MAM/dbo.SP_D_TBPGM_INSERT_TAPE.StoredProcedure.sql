USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/24>
-- Description:   �R�����X����
-- =============================================
CREATE Proc [dbo].[SP_D_TBPGM_INSERT_TAPE]

@FSPROG_ID	char(7),
@FNEPISODE	int,
@FSARC_TYPE	varchar(3)

As
delete from TBPGM_INSERT_TAPE
where 
FSPROG_ID=@FSPROG_ID and FNEPISODE=@FNEPISODE
and FSARC_TYPE=@FSARC_TYPE 
GO
