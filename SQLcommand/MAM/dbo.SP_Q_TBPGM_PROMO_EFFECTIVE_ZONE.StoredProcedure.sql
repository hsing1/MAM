USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   查詢短帶託播單有效區間資料
-- =============================================
CREATE Proc [dbo].[SP_Q_TBPGM_PROMO_EFFECTIVE_ZONE]

@FNPROMO_BOOKING_NO	int

As
select A.FNPROMO_BOOKING_NO,A.FSPROMO_ID,A.FNNO,A.FDBEG_DATE,
A.FDEND_DATE,A.FSBEG_TIME,A.FSEND_TIME,A.FCDATE_TIME_TYPE,A.FSCHANNEL_ID,
A.FSWEEK,B.FSCHANNEL_NAME
from TBPGM_PROMO_EFFECTIVE_ZONE A,TBZCHANNEL B
where A.FSCHANNEL_ID=B.FSCHANNEL_ID
and @FNPROMO_BOOKING_NO = case when @FNPROMO_BOOKING_NO = '' then @FNPROMO_BOOKING_NO else A.FNPROMO_BOOKING_NO end
order by A.FNNO


GO
