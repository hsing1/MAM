USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2013/01/09>
-- Description:   查詢節目檔案播出紀錄
-- 查出日期區間內播出的節目檔案
-- =============================================
CREATE Proc [dbo].[SP_Q_GET_PLAYLIST_ZONE_PLAY]
@FDBDATE varchar(10),
@FDEDATE varchar(10)

As

select FSPROG_ID,FSPROG_NAME,FNEPISODE,FSFILE_NO,FSVIDEO_ID,count(*) from TBPGM_COMBINE_QUEUE
where FDDATE>=@FDBDATE and FDDATE<=@FDEDATE and FNLIVE<>1
group by FSPROG_ID,FSPROG_NAME,FNEPISODE,FSFILE_NO,FSVIDEO_ID

GO
