USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <???>
-- Create date:   <???>
-- Description:   依照ID與集數與資料帶類型且狀態為'S','T','Y','W','A'資料查詢檔案資料
-- =============================================
CREATE Proc [dbo].[SP_Q_TBLOG_VIDEO_BY_ID_EPISODE_ARCTPYE_CHECK]
@FSTYPE char(1),
@FSID	varchar(11),
@FNEPISODE	smallint,
@FSARC_TYPE	char(3)

As
--SELECT COUNT(*) as dbCount FROM TBLOG_VIDEO
SELECT FSFILE_NO FROM TBLOG_VIDEO
WHERE 
FSTYPE = @FSTYPE
and FSID = @FSID 
and FNEPISODE = @FNEPISODE 
and FSARC_TYPE = @FSARC_TYPE
and ( FCFILE_STATUS = 'S' or  FCFILE_STATUS = 'T' or  FCFILE_STATUS = 'Y' or  FCFILE_STATUS = 'A' or  FCFILE_STATUS = 'W')
GO
