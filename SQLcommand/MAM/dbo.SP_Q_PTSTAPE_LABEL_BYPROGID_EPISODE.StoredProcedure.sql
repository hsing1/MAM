USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<sam.wu>
-- Create date: <2011/6/1>
-- Description:	<透過節目編號+集別，找到館藏資料>
-- =============================================
CREATE Proc [dbo].[SP_Q_PTSTAPE_LABEL_BYPROGID_EPISODE]
@FSPROG_ID char(7),
@FNEPISODE smallint	

as
declare @str varchar(20) = 'v.@FNEPISODE '
declare @str2 varchar(20) = '@FNEPISODE '
declare @str3 varchar(20) = 'v. @FNEPISODE '

select tblLTape.*,
ZSystem.TypeName as TType_Name ,
TapeType.TapeName as TTapeCode_Name,
LLabel.Title as  TAPE_TITLE,
LLabel.Description as  TAPE_DES,
LLabel.Duration as  TAPE_DURATION,
ISNULL(CONVERT(VARCHAR(20) ,tblLTape.CreateDT , 111),'') as WANT_FDCREATEDDATE,
ISNULL(CONVERT(VARCHAR(20) ,tblLTape.UpdateDT , 111),'') as WANT_FDUPDATEDDATE
from  
[PTSTape].[PTSTape].[dbo].[tblLLabelProg] LabelProg,
[PTSTape].[PTSTape].[dbo].[tblLTape] tblLTape
               left outer join [PTSTape].[PTSTape].[dbo].tblZSystem as ZSystem on tblLTape.TType=ZSystem.TypeCode
               left outer join [PTSTape].[PTSTape].[dbo].tblTapeType as TapeType on tblLTape.TTapeCode=TapeType.TapeCode
               left outer join [PTSTape].[PTSTape].[dbo].tblLLabel as LLabel on tblLTape.BIB=LLabel.BIB
                              
where 
LabelProg.Bib=tblLTape.bib
and  LabelProg.progid=@FSPROG_ID and LabelProg.epno=@FNEPISODE
and  tblLTape.deadmark = '0' --影帶回溯時，館藏資料若是已停用就不要帶到MAM
and  (
(tblLTape.TVolume like (REPLACE(@str, '@FNEPISODE', @FNEPISODE)+'%'))		--v.集別 要過濾
or 
(tblLTape.TVolume like (REPLACE(@str2, '@FNEPISODE', @FNEPISODE)+'%'))		--集別 要過濾
or
(tblLTape.TVolume like (REPLACE(@str3, '@FNEPISODE', @FNEPISODE)+'%'))		--v. 集別 要過濾
)

--SP_Q_PTSTAPE_LABEL_BYPROGID_EPISODE '2008783','21'

GO
