USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   依照ID與集數資料查詢檔案資料
-- =============================================
CREATE Proc [dbo].[SP_Q_TBLOG_VIDEO_BY_ID_EPISODE]
@FSTYPE	char(1),
@FSID	varchar(11),
@FNEPISODE	smallint


As
select ISNULL(b.FSUSER_ChtName,'') as FSCREATED_BY_NAME, ISNULL(C.FSUSER_ChtName,'') as FSUPDATED_BY_NAME ,
ISNULL(CONVERT(VARCHAR(20) ,a.FDCREATED_DATE , 111),'') as WANT_FDCREATEDDATE,
ISNULL(CONVERT(VARCHAR(20) ,a.FDUPDATED_DATE , 111),'') as WANT_FDUPDATEDDATE,
a.* FROM TBLOG_VIDEO  as a  
               left outer JOIN TBUSERS as b on a.FSCREATED_BY=b.FSUSER_ID
               left outer join TBUSERS as c on a.FSUPDATED_BY=c.FSUSER_ID
where a.FSID = @FSID 
and a.FNEPISODE = @FNEPISODE 
and a.FSTYPE = @FSTYPE
GO
