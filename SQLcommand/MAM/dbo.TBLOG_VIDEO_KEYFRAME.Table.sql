USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBLOG_VIDEO_KEYFRAME](
	[FSSUBJECT_ID] [char](12) NOT NULL,
	[FSVIDEO_NO] [varchar](16) NOT NULL,
	[FSFILE_NO] [varchar](25) NOT NULL,
	[FSCREATED_BY] [nvarchar](20) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
	[FSFILE_PATH] [varchar](150) NULL,
	[FNFILE_SIZE] [int] NULL,
	[FNDIR_ID] [int] NULL,
	[FSSERVER_NAME] [varchar](20) NULL,
	[FSSHARE_FOLDER] [varchar](100) NULL,
	[FSDESCRIPTION] [nvarchar](max) NULL,
 CONSTRAINT [PK_TBLOG_VIDEO_KEYFRAME] PRIMARY KEY CLUSTERED 
(
	[FSFILE_NO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
CREATE NONCLUSTERED INDEX [IX_TBLOG_VIDEO_KEYFRAME] ON [dbo].[TBLOG_VIDEO_KEYFRAME]
(
	[FSVIDEO_NO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TBLOG_VIDEO_KEYFRAME] ADD  CONSTRAINT [D_dbo_TBLOG_VIDEO_KEYFRAME_1]  DEFAULT (getdate()) FOR [FDCREATED_DATE]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'活動代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_KEYFRAME', @level2type=N'COLUMN',@level2name=N'FSSUBJECT_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'影片編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_KEYFRAME', @level2type=N'COLUMN',@level2name=N'FSVIDEO_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'檔案編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_KEYFRAME', @level2type=N'COLUMN',@level2name=N'FSFILE_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_KEYFRAME', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_KEYFRAME', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'圖檔檔案大小' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_KEYFRAME', @level2type=N'COLUMN',@level2name=N'FNFILE_SIZE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'所屬Queue代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_KEYFRAME', @level2type=N'COLUMN',@level2name=N'FNDIR_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主機名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_KEYFRAME', @level2type=N'COLUMN',@level2name=N'FSSERVER_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'檔案存放位置' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_KEYFRAME', @level2type=N'COLUMN',@level2name=N'FSSHARE_FOLDER'
GO
