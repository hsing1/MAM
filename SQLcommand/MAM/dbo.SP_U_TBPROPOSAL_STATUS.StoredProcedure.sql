USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_U_TBPROPOSAL_STATUS]
@FSPRO_ID CHAR(12),
@FCCHECK_STATUS CHAR(1),
@FSUPDATED_BY VARCHAR(50)

As
UPDATE MAM.dbo.TBPROPOSAL
SET FCCHECK_STATUS=@FCCHECK_STATUS ,FSUPDATED_BY=@FSUPDATED_BY,FDUPDATED_DATE=GETDATE()
WHERE  MAM.dbo.TBPROPOSAL.FSPRO_ID=@FSPRO_ID
GO
