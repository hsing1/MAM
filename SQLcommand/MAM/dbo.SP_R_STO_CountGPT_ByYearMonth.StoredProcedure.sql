USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<kyle.Lin>
-- Create date: <2011/11/14>
-- Description:	For RPT_TBARCHIVE_07(入庫單),依年月查詢數量
--              (by mihsiu)
-- =============================================
CREATE PROCEDURE [dbo].[SP_R_STO_CountGPT_ByYearMonth]
	@BeginYear nvarchar(4),	
	@BeginMonth		nvarchar(2),	
	@EndYear nvarchar(4),
    @EndMonth nvarchar(2),
    @CREATED_BY varchar(50)	
AS
BEGIN
	--declare @BeginYear nvarchar(4)='2011'
	--declare @BeginMonth nvarchar(2)='08'
	--declare @EndYear nvarchar(4)='2011'
	--declare @EndMonth nvarchar(2)='11'
	declare @BeginDate datetime = @BeginYear+'/'+@BeginMonth+'/01'
	declare @EndDate datetime = @EndYear+'/'+@EndMonth+'/01'
	declare @i int=0
	declare @PTS_GCount int = 99999
	declare @PTS_PCount int = 99999
	declare @PTS_TCount int = 99999
	
	declare @HK_GCount int = 99999
	declare @HK_PCount int = 99999
	declare @HK_TCount int = 99999
	
	declare @YEN_GCount int = 99999
	declare @YEN_PCount int = 99999
	declare @YEN_TCount int = 99999
	
	
	declare @HOM_GCount int = 99999
	declare @HOM_PCount int = 99999
	declare @HOM_TCount int = 99999

	declare @QUERY_BY nvarchar(50)=''
	declare @FinalTable Table(
	[Year] nvarchar(4),
	[MONTH] nvarchar(2),
	[公視節目] int,
	[公視短片] int,
	[公視回溯] int,
	[客台節目] int,
	[客台短片] int,
	[客台回溯] int,
	[原台節目] int,
	[原台短片] int,
	[原台回溯] int,
	[宏觀節目] int,
	[宏觀短片] int,
	[宏觀回溯] int,
	[QUERY_BY] nvarchar (50)
	)

	while DATEADD(MONTH,@i,@BeginDate)<=@EndDate
	BEGIN
		---===========================公視 Start
		---節目
		SET @PTS_GCount=(
		select COUNT(*) from TBLOG_VIDEO as LV left outer join TBBROADCAST as BRO on LV.FSBRO_ID=BRO.FSBRO_ID
		where(LV.[FDCREATED_DATE] between DATEADD(MONTH,@i,@BeginDate) and DATEADD(MONTH,@i+1,@BeginDate)) and LV.FSCHANNEL_ID='01' and LV.FSTYPE<>'P' and BRO.FCCHECK_STATUS<>'T')
		---短片
		SET @PTS_PCount=(
		select COUNT(*) from TBLOG_VIDEO as LV left outer join TBBROADCAST as BRO on LV.FSBRO_ID=BRO.FSBRO_ID
		where(LV.[FDCREATED_DATE] between DATEADD(MONTH,@i,@BeginDate) and DATEADD(MONTH,@i+1,@BeginDate)) and LV.FSCHANNEL_ID='01' and LV.FSTYPE<>'G')
		---回溯
		SET @PTS_TCount=(
		select COUNT(*) from TBLOG_VIDEO as LV left outer join TBBROADCAST as BRO on LV.FSBRO_ID=BRO.FSBRO_ID
		where(LV.[FDCREATED_DATE] between DATEADD(MONTH,@i,@BeginDate) and DATEADD(MONTH,@i+1,@BeginDate)) and LV.FSCHANNEL_ID='01' and LV.FSTYPE<>'P' and BRO.FCCHECK_STATUS='T' )
		---===========================公視 End
		---===========================客台 Start
		---節目
		SET @HK_GCount=(
		select COUNT(*) from TBLOG_VIDEO as LV left outer join TBBROADCAST as BRO on LV.FSBRO_ID=BRO.FSBRO_ID
		where(LV.[FDCREATED_DATE] between DATEADD(MONTH,@i,@BeginDate) and DATEADD(MONTH,@i+1,@BeginDate)) and LV.FSCHANNEL_ID='07' and LV.FSTYPE<>'P' and BRO.FCCHECK_STATUS<>'T' )
		---短片
		SET @HK_PCount=(
		select COUNT(*) from TBLOG_VIDEO as LV left outer join TBBROADCAST as BRO on LV.FSBRO_ID=BRO.FSBRO_ID
		where(LV.[FDCREATED_DATE] between DATEADD(MONTH,@i,@BeginDate) and DATEADD(MONTH,@i+1,@BeginDate)) and LV.FSCHANNEL_ID='07' and LV.FSTYPE<>'G' )
		---回溯
		SET @HK_TCount=(
		select COUNT(*) from TBLOG_VIDEO as LV left outer join TBBROADCAST as BRO on LV.FSBRO_ID=BRO.FSBRO_ID
		where(LV.[FDCREATED_DATE] between DATEADD(MONTH,@i,@BeginDate) and DATEADD(MONTH,@i+1,@BeginDate)) and LV.FSCHANNEL_ID='07' and LV.FSTYPE<>'P' and BRO.FCCHECK_STATUS='T' )
		---===========================客台 End
		---===========================原台 Start
		---節目
		SET @YEN_GCount=(
		select COUNT(*) from TBLOG_VIDEO as LV left outer join TBBROADCAST as BRO on LV.FSBRO_ID=BRO.FSBRO_ID
		where(LV.[FDCREATED_DATE] between DATEADD(MONTH,@i,@BeginDate) and DATEADD(MONTH,@i+1,@BeginDate)) and LV.FSCHANNEL_ID='09' and LV.FSTYPE<>'P' and BRO.FCCHECK_STATUS<>'T')
		---短片
		SET @YEN_PCount=(
		select COUNT(*) from TBLOG_VIDEO as LV left outer join TBBROADCAST as BRO on LV.FSBRO_ID=BRO.FSBRO_ID
		where(LV.[FDCREATED_DATE] between DATEADD(MONTH,@i,@BeginDate) and DATEADD(MONTH,@i+1,@BeginDate)) and LV.FSCHANNEL_ID='09' and LV.FSTYPE<>'G')
		---回溯
		SET @YEN_TCount=(
		select COUNT(*) from TBLOG_VIDEO as LV left outer join TBBROADCAST as BRO on LV.FSBRO_ID=BRO.FSBRO_ID
		where(LV.[FDCREATED_DATE] between DATEADD(MONTH,@i,@BeginDate) and DATEADD(MONTH,@i+1,@BeginDate)) and LV.FSCHANNEL_ID='09' and LV.FSTYPE<>'P' and BRO.FCCHECK_STATUS='T' )
		---===========================原台 End
		---===========================宏觀 Start
		---節目
		SET @HOM_GCount=(
		select COUNT(*) from TBLOG_VIDEO as LV left outer join TBBROADCAST as BRO on LV.FSBRO_ID=BRO.FSBRO_ID
		where(LV.[FDCREATED_DATE] between DATEADD(MONTH,@i,@BeginDate) and DATEADD(MONTH,@i+1,@BeginDate)) and LV.FSCHANNEL_ID='08' and LV.FSTYPE<>'P' and BRO.FCCHECK_STATUS<>'T' )
		---短片
		SET @HOM_PCount=(
		select COUNT(*) from TBLOG_VIDEO as LV left outer join TBBROADCAST as BRO on LV.FSBRO_ID=BRO.FSBRO_ID
		where(LV.[FDCREATED_DATE] between DATEADD(MONTH,@i,@BeginDate) and DATEADD(MONTH,@i+1,@BeginDate)) and LV.FSCHANNEL_ID='08' and LV.FSTYPE<>'G' )
		---回溯
		SET @HOM_TCount=(
		select COUNT(*) from TBLOG_VIDEO as LV left outer join TBBROADCAST as BRO on LV.FSBRO_ID=BRO.FSBRO_ID
		where(LV.[FDCREATED_DATE] between DATEADD(MONTH,@i,@BeginDate) and DATEADD(MONTH,@i+1,@BeginDate)) and LV.FSCHANNEL_ID='08' and LV.FSTYPE<>'P' and BRO.FCCHECK_STATUS='T' )
		---===========================宏觀 End
		set @QUERY_BY = (select FSUSER_ChtName from [MAM].[dbo].[TBUSERS] where [FSUSER_ID]=@CREATED_BY)
		insert @FinalTable values(CONVERT(nvarchar, DATEPART(YEAR,DATEADD(MONTH,@i,@BeginDate))),DATEPART(MONTH,DATEADD(MONTH,@i,@BeginDate)),@PTS_GCount,@PTS_PCount,@PTS_TCount,
		@HK_GCount,@HK_PCount,@HK_TCount,@YEN_GCount,@YEN_PCount,@YEN_TCount,@HOM_GCount,@HOM_PCount,@HOM_TCount,@QUERY_BY)
		set @i+=1
	END
	select * from @FinalTable
END


GO
