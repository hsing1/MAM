USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/04/27>
-- Description:	<根據傳入條件,取回最新單號>
-- =============================================
CREATE FUNCTION [dbo].[FN_Q_SET_CODELIST_TO_PURESTRING]
(
	@INPUT	NVARCHAR(300),	--此函數目前輸入的限制是300長
	@LEN	INT
)
RETURNS	NVARCHAR(100)
AS
BEGIN
--宣告程序內變數
DECLARE	@idx INT, @char VARCHAR(3), @RESULT NVARCHAR(100)

--初始化變數
SELECT @idx = 1, @RESULT = ''

--跑迴圈處理每一個設定
WHILE(@idx <= @LEN)
	BEGIN
		SET @char = SUBSTRING(CAST((100 + @idx) AS VARCHAR(3)),2,3) + ';'
	
		IF(@INPUT LIKE '%'+@char+'%')
			BEGIN
				SET @RESULT = @RESULT + '1'
				SET @INPUT = REPLACE(@INPUT,@char,'')
			END
		ELSE
			BEGIN
				SET @RESULT = @RESULT + '0'
			END
		
		SET @idx = @idx + 1
	END

	RETURN @RESULT
END


GO
