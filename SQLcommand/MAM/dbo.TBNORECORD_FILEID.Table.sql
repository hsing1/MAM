USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBNORECORD_FILEID](
	[FSTYPE] [char](1) NOT NULL,
	[FSSUBJECT_ID] [char](11) NOT NULL,
	[FSNO] [char](4) NOT NULL,
	[FSCREATED_BY] [varchar](50) NULL,
	[FDCREATED_DATE] [datetime] NULL,
 CONSTRAINT [PK_TBNORECORD_FILEID_1] PRIMARY KEY CLUSTERED 
(
	[FSTYPE] ASC,
	[FSSUBJECT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'種類P-PROMO G-節目 D-資料帶' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBNORECORD_FILEID', @level2type=N'COLUMN',@level2name=N'FSTYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SUBJECTID的取號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBNORECORD_FILEID', @level2type=N'COLUMN',@level2name=N'FSSUBJECT_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'??' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBNORECORD_FILEID', @level2type=N'COLUMN',@level2name=N'FSNO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'取號者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBNORECORD_FILEID', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'取號日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBNORECORD_FILEID', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
