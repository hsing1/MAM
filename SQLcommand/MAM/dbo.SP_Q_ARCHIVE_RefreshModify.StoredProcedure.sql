USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Q_ARCHIVE_RefreshModify]

@FSPROG_ID varchar(11) ,
@FNEPISODE smallint 

As
	if @FNEPISODE>0
	begin
		select 
		ISNULL(d.FSPGMNAME,'')  as FSPGMNAME,
		ISNULL(d.FNLENGTH,'') as FNLENGTH,
		ISNULL(d.FNTOTEPISODE,'') as FNTOTEPISODE,
		ISNULL(a.FSPGDNAME,'')  as FSPGDNAME,
		ISNULL(d.FSCHANNEL,'') as FSCHANNEL ,
		a.* FROM TBPROG_D  as a  
					   left outer join TBPROG_M as d on a.FSPROG_ID=d.FSPROG_ID -----[MAM].[dbo].[TBPROG_M]
		               
		where a.FSPROG_ID  = @FSPROG_ID and a.FNEPISODE=@FNEPISODE
		order by FSPROG_ID,FNEPISODE
	end

	if @FNEPISODE=0
	begin
		select Top 1
		ISNULL(a.FSPGMNAME,'')  as FSPGMNAME,
		ISNULL(a.FNLENGTH,'') as FNLENGTH,
		ISNULL(a.FNTOTEPISODE,'') as FNTOTEPISODE,
		'以節目為單位入庫'  as FSPGDNAME,
		ISNULL(a.FSCHANNEL,'') as FSCHANNEL 
	from TBPROG_M as a 
	where a.FSPROG_ID=@FSPROG_ID
	end
		--原本的資料~~~
		--select top 1
		--ISNULL(d.FSPGMNAME,'')  as FSPGMNAME,
		--ISNULL(d.FNLENGTH,'') as FNLENGTH,
		--ISNULL(d.FNTOTEPISODE,'') as FNTOTEPISODE,
		--ISNULL(a.FSPGDNAME,'')  as FSPGDNAME,
		--ISNULL(d.FSCHANNEL,'') as FSCHANNEL ,
		--a.* FROM TBPROG_D  as a  
		--			   left outer join TBPROG_M as d on a.FSPROG_ID=d.FSPROG_ID -----[MAM].[dbo].[TBPROG_M]
		               
		--where a.FSPROG_ID  = @FSPROG_ID
		--order by FSPROG_ID,FNEPISODE
	
	
	
	
	--------------------------------------------------------
	
	--select Top 1
	--	ISNULL(a.FSPGMNAME,'')  as FSPGMNAME,
	--	ISNULL(a.FNLENGTH,'') as FNLENGTH,
	--	ISNULL(a.FNTOTEPISODE,'') as FNTOTEPISODE,
	--	'以節目為單位入庫'  as FSPGDNAME,
	--	ISNULL(a.FSCHANNEL,'') as FSCHANNEL 
	--from TBPROG_M as a 
	--where a.FSPROG_ID='2011003'
	--201104110002
		--select 
		--ISNULL(d.FSPGMNAME,'')  as FSPGMNAME,
		--ISNULL(d.FNLENGTH,'') as FNLENGTH,
		--ISNULL(d.FNTOTEPISODE,'') as FNTOTEPISODE,
		--ISNULL(a.FSPGDNAME,'')  as FSPGDNAME,
		--ISNULL(d.FSCHANNEL,'') as FSCHANNEL ,
		--a.* FROM TBPROG_D  as a  
		--			   right join TBPROG_M as d on a.FSPROG_ID=d.FSPROG_ID -----[MAM].[dbo].[TBPROG_M]
		               
		--where a.FSPROG_ID  = '2011003'
		--order by FSPROG_ID,FNEPISODE
GO
