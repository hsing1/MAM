USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <David.Sin>
-- Create date:   <2010/10/02>
-- Description:   �d�ߦP�q����
-- =============================================

CREATE PROC [dbo].[SP_Q_TBSYNONYM_BY_FNNO]
@FNNO int
AS

SELECT FNNO,FSSYNONYM FROM TBSYNONYM WHERE FNNO=@FNNO
GO
