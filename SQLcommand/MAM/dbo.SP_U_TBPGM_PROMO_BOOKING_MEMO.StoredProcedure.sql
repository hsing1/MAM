USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   修改短帶託播單備註欄位
-- =============================================
CREATE Proc [dbo].[SP_U_TBPGM_PROMO_BOOKING_MEMO]

@FNPROMO_BOOKING_NO	int,
@FSMEMO	varchar(50)

As

update TBPGM_PROMO_BOOKING set FSMEMO=@FSMEMO where FNPROMO_BOOKING_NO=@FNPROMO_BOOKING_NO
GO
