USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RPT_HD_DATE_LST](
	[QUERY_KEY] [uniqueidentifier] NOT NULL,
	[QUERY_KEY_SUBID] [uniqueidentifier] NOT NULL,
	[QUERY_BY] [varchar](50) NOT NULL,
	[QUERY_DATE] [datetime] NOT NULL,
	[頻道] [varchar](50) NOT NULL,
	[播出日期] [date] NOT NULL,
	[序號] [varchar](8) NULL,
	[節目編碼] [varchar](7) NULL,
	[時間類型] [varchar](1) NULL,
	[名稱] [varchar](50) NULL,
	[節目集數] [int] NULL,
	[段數] [int] NULL,
	[支數] [int] NULL,
	[主控編號] [varchar](8) NULL,
	[播出時間] [nvarchar](11) NULL,
	[長度] [nvarchar](11) NULL,
	[播映序號] [int] NULL,
	[短帶編碼] [varchar](11) NULL,
	[影像編號] [varchar](16) NULL,
	[資料類型] [varchar](1) NULL,
	[備註] [varchar](200) NULL,
	[FSSIGNAL] [varchar](10) NULL,
	[FSRAN] [varchar](1) NULL,
	[FNLIVE] [varchar](1) NULL,
	[FSSOM] [varchar](11) NULL,
 CONSTRAINT [PK_RPT_HD_DATE_LST] PRIMARY KEY CLUSTERED 
(
	[QUERY_KEY] ASC,
	[QUERY_KEY_SUBID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
