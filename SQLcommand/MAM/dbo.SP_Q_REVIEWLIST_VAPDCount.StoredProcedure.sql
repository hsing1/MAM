USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Q_REVIEWLIST_VAPDCount] 
@CatalogID nvarchar(50),
@FSID nvarchar(50)

As


IF @CatalogID='G' 
BEGIN 
  --declare @FSID nvarchar(100)
  --set @FSID='0000001'
  declare @CountTable table(FSID nvarchar(100) ,EPISODE nvarchar(100),NAME nvarchar(100),VCount nvarchar(100),ACount nvarchar(100),PCount nvarchar(100),DCount nvarchar(100))
  declare @CountedTable table(FSID nvarchar(100) ,EPISODE nvarchar(100),NAME nvarchar(100),VCount nvarchar(100),ACount nvarchar(100),PCount nvarchar(100),DCount nvarchar(100))
  insert @CountTable--(EPISODE,NAME,VCount,ACount,PCount,DCount)
  select @FSID as FSID,FNEPISODE,FSPGDNAME,'','','','' from [MAM].[dbo].[TBPROG_D] where FSPROG_ID=@FSID
  --select * from @CountTable
  declare @EPISODE nvarchar(100),@NAME nvarchar(100),@VCount nvarchar(100),@ACount nvarchar(100),@PCount nvarchar(100),@DCount nvarchar(100)
  declare CSR cursor for select EPISODE,NAME,VCount,ACount,PCount,DCount from @CountTable
  --------------
		set @VCount=(select COUNT(*) from MAM.dbo.TBLOG_VIDEO where FSID=@FSID and FNEPISODE=0 and (FCFILE_STATUS='Y' or FCFILE_STATUS='T') and FSTYPE ='G')
		set @ACount=(select COUNT(*) from MAM.dbo.TBLOG_AUDIO where FSID=@FSID and FNEPISODE=0 and FCFILE_STATUS='Y' and FSTYPE ='G')
		set @PCount=(select COUNT(*) from MAM.dbo.TBLOG_PHOTO where FSID=@FSID and FNEPISODE=0 and FCFILE_STATUS='Y' and FSTYPE ='G')
		set @DCount=(select COUNT(*) from MAM.dbo.TBLOG_DOC where FSID=@FSID and FNEPISODE=0 and FCFILE_STATUS='Y' and FSTYPE ='G')
		if(@VCount<>'0' or @ACount<>'0' or @PCount<>'0' or @DCount<>'0')
		begin
			insert @CountedTable(FSID,EPISODE,NAME,VCount,ACount,PCount,DCount) values(@FSID,'0',@NAME,@VCount,@ACount,@PCount,@DCount)
		end
  --------------
  
  open CSR
	fetch next from CSR into @EPISODE,@NAME,@VCount,@ACount,@PCount,@DCount
	while(@@FETCH_STATUS=0)
	begin
		set @VCount=(select COUNT(*) from MAM.dbo.TBLOG_VIDEO where FSID=@FSID and FNEPISODE=@EPISODE and (FCFILE_STATUS='Y' or FCFILE_STATUS='T') and FSTYPE ='G')
		set @ACount=(select COUNT(*) from MAM.dbo.TBLOG_AUDIO where FSID=@FSID and FNEPISODE=@EPISODE and FCFILE_STATUS='Y' and FSTYPE ='G')
		set @PCount=(select COUNT(*) from MAM.dbo.TBLOG_PHOTO where FSID=@FSID and FNEPISODE=@EPISODE and FCFILE_STATUS='Y' and FSTYPE ='G')
		set @DCount=(select COUNT(*) from MAM.dbo.TBLOG_DOC where FSID=@FSID and FNEPISODE=@EPISODE and FCFILE_STATUS='Y' and FSTYPE ='G')
		insert @CountedTable(FSID,EPISODE,NAME,VCount,ACount,PCount,DCount) values(@FSID,@EPISODE,@NAME,@VCount,@ACount,@PCount,@DCount)
	fetch next from CSR into @EPISODE,@NAME,@VCount,@ACount,@PCount,@DCount
	end
  close CSR
  select * from @CountedTable
END 

IF @CatalogID='P' 
Begin 
  --declare @FSID nvarchar(100)
  --set @FSID='20111200002'
  declare @PCountTable table(FSID nvarchar(100) ,EPISODE nvarchar(100),NAME nvarchar(100),VCount nvarchar(100),ACount nvarchar(100),PCount nvarchar(100),DCount nvarchar(100))
  declare @PEPISODE nvarchar(100),@PNAME nvarchar(100),@PVCount nvarchar(100),@PACount nvarchar(100),@PPCount nvarchar(100),@PDCount nvarchar(100) 
  		set @PVCount=(select COUNT(*) from MAM.dbo.TBLOG_VIDEO where FSID=@FSID and (FCFILE_STATUS='Y' or FCFILE_STATUS='T') and FSTYPE ='P')
		set @PACount=(select COUNT(*) from MAM.dbo.TBLOG_AUDIO where FSID=@FSID and FCFILE_STATUS='Y' and FSTYPE ='P')
		set @PPCount=(select COUNT(*) from MAM.dbo.TBLOG_PHOTO where FSID=@FSID and FCFILE_STATUS='Y' and FSTYPE ='P')
		set @PDCount=(select COUNT(*) from MAM.dbo.TBLOG_DOC where FSID=@FSID and FCFILE_STATUS='Y' and FSTYPE ='P')
		set @PNAME=(select FSPROMO_NAME from MAM.dbo.TBPGM_PROMO where FSPROMO_ID=@FSID)
  insert @PCountTable(FSID,EPISODE,NAME,VCount,ACount,PCount,DCount) values(@FSID,'0',@PNAME,@PVCount,@PACount,@PPCount,@PDCount)
  select * from @PCountTable	
End 

if @CatalogID='D'
begin
  declare @DCountTable table(FSID nvarchar(100) ,EPISODE nvarchar(100),NAME nvarchar(100),VCount nvarchar(100),ACount nvarchar(100),PCount nvarchar(100),DCount nvarchar(100))
  declare @DEPISODE nvarchar(100),@DNAME nvarchar(100),@DVCount nvarchar(100),@DACount nvarchar(100),@DPCount nvarchar(100),@DDCount nvarchar(100) 
  		set @DVCount=(select COUNT(*) from MAM.dbo.TBLOG_VIDEO where FSID=@FSID and (FCFILE_STATUS='Y' or FCFILE_STATUS='T') and FSTYPE ='D')
		set @DACount=(select COUNT(*) from MAM.dbo.TBLOG_AUDIO where FSID=@FSID and FCFILE_STATUS='Y' and FSTYPE ='D')
		set @DPCount=(select COUNT(*) from MAM.dbo.TBLOG_PHOTO where FSID=@FSID and FCFILE_STATUS='Y' and FSTYPE ='D')
		set @DDCount=(select COUNT(*) from MAM.dbo.TBLOG_DOC where FSID=@FSID and FCFILE_STATUS='Y' and FSTYPE ='D')
		set @DNAME=(select FSDATA_NAME from MAM.dbo.TBDATA where FSDATA_ID=@FSID)
  insert @DCountTable(FSID,EPISODE,NAME,VCount,ACount,PCount,DCount) values(@FSID,'0',@DNAME,@DVCount,@DACount,@DPCount,@DDCount)
  select * from @DCountTable
end

GO
