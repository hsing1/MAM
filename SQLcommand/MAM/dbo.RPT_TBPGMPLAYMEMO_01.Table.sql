USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RPT_TBPGMPLAYMEMO_01](
	[QUERY_KEY] [uniqueidentifier] NOT NULL,
	[QUERY_KEY_SUBID] [uniqueidentifier] NOT NULL,
	[QUERY_BY] [varchar](50) NOT NULL,
	[QUERY_DATE] [datetime] NOT NULL,
	[頻道名稱] [nvarchar](50) NULL,
	[播出日期] [datetime] NULL,
	[節目編碼] [nvarchar](7) NOT NULL,
	[播映序號] [int] NOT NULL,
	[節目名稱] [nvarchar](50) NOT NULL,
	[短帶名稱] [nvarchar](50) NULL,
	[主控鏡面] [nvarchar](200) NULL,
	[CH1] [nvarchar](20) NULL,
	[CH2] [nvarchar](20) NULL,
	[CH3] [nvarchar](20) NULL,
	[CH4] [nvarchar](20) NULL,
 CONSTRAINT [PK_RPT_TBPGMPLAYMEMO_01] PRIMARY KEY CLUSTERED 
(
	[QUERY_KEY] ASC,
	[QUERY_KEY_SUBID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'描述', @value=N'播出題示報表(舊版)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGMPLAYMEMO_01'
GO
