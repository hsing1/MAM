USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBPREBOOKING](
	[FNNO] [bigint] NOT NULL,
	[FSGUID] [varchar](200) NULL,
	[FSUSER_ID] [varchar](50) NOT NULL,
	[FDBOOKING_DATE] [varchar](10) NOT NULL,
	[FSFILE_NO] [varchar](16) NOT NULL,
	[FSSTART_TIMECODE] [varchar](11) NOT NULL,
	[FSBEG_TIMECODE] [varchar](11) NOT NULL,
	[FSEND_TIMECODE] [varchar](11) NOT NULL,
	[FSFILE_TYPE] [varchar](5) NOT NULL,
	[FSFILE_CATEGORY] [nvarchar](1) NOT NULL,
	[FNLOCATION] [int] NOT NULL,
	[FCOUT_BUY] [char](1) NULL,
	[FCBOOKING_TYPE] [char](1) NULL,
	[FCSUPERVISOR_CHECK] [char](1) NULL,
	[FSSUPERVISOR] [varchar](50) NULL,
	[FCALL] [char](1) NULL,
 CONSTRAINT [PK_TBPREBOOKING] PRIMARY KEY CLUSTERED 
(
	[FNNO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'序號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPREBOOKING', @level2type=N'COLUMN',@level2name=N'FNNO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'GUID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPREBOOKING', @level2type=N'COLUMN',@level2name=N'FSGUID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPREBOOKING', @level2type=N'COLUMN',@level2name=N'FSUSER_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'預借日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPREBOOKING', @level2type=N'COLUMN',@level2name=N'FDBOOKING_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'檔案編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPREBOOKING', @level2type=N'COLUMN',@level2name=N'FSFILE_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Timecode起點' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPREBOOKING', @level2type=N'COLUMN',@level2name=N'FSSTART_TIMECODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'調用Timecode起點' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPREBOOKING', @level2type=N'COLUMN',@level2name=N'FSBEG_TIMECODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'調用Timecode迄點' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPREBOOKING', @level2type=N'COLUMN',@level2name=N'FSEND_TIMECODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'檔案類型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPREBOOKING', @level2type=N'COLUMN',@level2name=N'FSFILE_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'檔案類別' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPREBOOKING', @level2type=N'COLUMN',@level2name=N'FSFILE_CATEGORY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'檔案路徑' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPREBOOKING', @level2type=N'COLUMN',@level2name=N'FNLOCATION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否為外購' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPREBOOKING', @level2type=N'COLUMN',@level2name=N'FCOUT_BUY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'調用類別' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPREBOOKING', @level2type=N'COLUMN',@level2name=N'FCBOOKING_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否需主管審核' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPREBOOKING', @level2type=N'COLUMN',@level2name=N'FCSUPERVISOR_CHECK'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主管審核者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPREBOOKING', @level2type=N'COLUMN',@level2name=N'FSSUPERVISOR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否為完整調用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPREBOOKING', @level2type=N'COLUMN',@level2name=N'FCALL'
GO
EXEC sys.sp_addextendedproperty @name=N'描述', @value=N'預借清單' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPREBOOKING'
GO
