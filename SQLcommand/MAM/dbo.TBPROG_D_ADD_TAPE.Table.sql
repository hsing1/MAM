USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBPROG_D_ADD_TAPE](
	[FSPROG_ID] [char](7) NOT NULL,
	[FNEPISODE] [int] NOT NULL,
	[FSWORNING_PROMO_ID] [varchar](11) NULL,
	[FSHEAD_PROMO_ID] [varchar](11) NULL,
	[FCStandard1] [bit] NULL,
	[FCStandard2] [bit] NULL,
	[FCStandard3] [bit] NULL,
	[FCStandard4] [bit] NULL,
	[FCStandard5] [bit] NULL,
	[FCStandard6] [bit] NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
	[FSUPDATED_BY] [varchar](50) NULL,
	[FDUPDATED_DATE] [datetime] NULL,
 CONSTRAINT [PK_TBPROG_D_ADD_TAPE] PRIMARY KEY CLUSTERED 
(
	[FSPROG_ID] ASC,
	[FNEPISODE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D_ADD_TAPE', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D_ADD_TAPE', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D_ADD_TAPE', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D_ADD_TAPE', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
