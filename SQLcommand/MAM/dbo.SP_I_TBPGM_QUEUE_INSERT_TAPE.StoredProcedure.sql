USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   新增播出提示資料(目前已不使用)
-- =============================================
CREATE Proc [dbo].[SP_I_TBPGM_QUEUE_INSERT_TAPE]

@FSPROG_ID	char(7),
@FNSEQNO	int,
@FDDATE	date,
@FSCHANNEL_ID	varchar(2),
@FNEPISODE	int,
@FNNO	int,
@FSPROMO_ID	varchar(11),
@FSINSERT_CHANNEL_ID	varchar(2),
@FSCREATED_BY	varchar(50)	

As
Insert Into dbo.TBPGM_QUEUE_INSERT_TAPE(FSPROG_ID,FNSEQNO,FDDATE,FSCHANNEL_ID,
FNEPISODE,FNNO,FSPROMO_ID,FSINSERT_CHANNEL_ID,
FSCREATED_BY,FDCREATED_DATE)
Values(@FSPROG_ID,@FNSEQNO,@FDDATE,@FSCHANNEL_ID,
@FNEPISODE,@FNNO,@FSPROMO_ID,@FSINSERT_CHANNEL_ID,
@FSCREATED_BY,GETDATE())






	
	
GO
