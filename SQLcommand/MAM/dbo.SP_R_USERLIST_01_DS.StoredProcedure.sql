USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/04/19>
-- Description:	取回TBUSERS清單
-- =============================================
CREATE PROCEDURE [dbo].[SP_R_USERLIST_01_DS]
	@QUERY_KEY	VARCHAR(36)
AS
BEGIN
	SELECT DISTINCT FSUSER_ID='', FSUSER_ChtName='(所有人員)', IDNAME ='(所有人員)'

	UNION

	SELECT DISTINCT FSUSER_ID, FSUSER_ChtName, IDNAME = FSUSER_ID +': '+ FSUSER_ChtName  
	FROM TBUSERS
	ORDER BY FSUSER_ID
END




GO
