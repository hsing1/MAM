USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        
-- Create date: 
-- Description:   取得新的VideoID
-- Update desc:    <2013/04/17><Jarvis.Yang>加入註解。
-- =============================================

CREATE Proc [dbo].[SP_I_TBLOGVIDEOID_NEWID]

@FSNOTE NVARCHAR(50)

As

DECLARE @MAX_VIDEOID BIGINT

IF EXISTS(SELECT * FROM TBLOG_VIDEOID)
	BEGIN
		-- 如果資料表內有資料，就找出最大值並加1
		SELECT @MAX_VIDEOID = MAX(FNVIDEO_ID) + 1 FROM TBLOG_VIDEOID	
	END
ELSE
	BEGIN
		-- 如果資料表內沒有資料，直接設定為 0
		SET @MAX_VIDEOID = 0
	END

-- 將VideoID寫入資料表
INSERT INTO TBLOG_VIDEOID(FNVIDEO_ID, FDAPPLY_TIME, FSNOTE) VALUES(@MAX_VIDEOID, GETDATE(), @FSNOTE)

-- 回傳VideoID
SELECT @MAX_VIDEOID AS FNVIDEO_ID

GO
