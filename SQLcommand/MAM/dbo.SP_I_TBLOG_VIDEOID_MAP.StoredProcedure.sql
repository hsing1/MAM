USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        
-- Create date: 
-- Description: 新增VIDEOID
-- Update desc:    <2013/07/02><Jarvis.Yang>加入註解。
-- =============================================
CREATE Proc [dbo].[SP_I_TBLOG_VIDEOID_MAP]

@FSID varchar(11),
@FNEPISODE smallint,
@FSARC_TYPE char(3),
@FSVIDEO_ID_PROG char(8)

As

-- 因為發現，當網路異常時，還是很有機會碰到「已申請，但程式那邊還沒讀到」的狀況，所以在這邊我重新檢查一次
IF NOT EXISTS (SELECT * FROM TBLOG_VIDEOID_MAP WHERE FSID = @FSID AND FNEPISODE = @FNEPISODE AND FSARC_TYPE = @FSARC_TYPE)
BEGIN
	INSERT INTO TBLOG_VIDEOID_MAP(FSID, FNEPISODE, FSARC_TYPE, FSVIDEO_ID_PROG) VALUES (@FSID, @FNEPISODE, @FSARC_TYPE, @FSVIDEO_ID_PROG)
END
else
begin
exec [SP_U_TBLOG_VIDEOID_MAP] @FSID,@FNEPISODE,@FSARC_TYPE,@FSVIDEO_ID_PROG
--update TBLOG_VIDEOID_MAP set FSVIDEO_ID_PROG=@FSVIDEO_ID_PROG where  FSID=@FSID and FNEPISODE=@FNEPISODE and FSARC_TYPE=@FSARC_TYPE
end

GO
