USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Award](
	[節目名稱] [nvarchar](255) NULL,
	[FSID] [char](7) NULL,
	[子集名稱] [nvarchar](255) NULL,
	[集別] [float] NULL,
	[EPISODE] [char](7) NULL,
	[參展狀態] [nvarchar](255) NULL,
	[參展區域] [nvarchar](255) NULL,
	[AWARD_AREA_ID] [varchar](4) NULL,
	[第_屆] [float] NULL,
	[參展名稱] [nvarchar](255) NULL,
	[得獎名稱] [nvarchar](255) NULL,
	[得獎日期] [datetime] NULL,
	[國際參展節目名] [nvarchar](255) NULL,
	[備註] [nvarchar](255) NULL,
	[F12] [nvarchar](255) NULL,
	[F13] [nvarchar](255) NULL,
	[F14] [nvarchar](255) NULL,
	[F15] [nvarchar](255) NULL,
	[F16] [nvarchar](255) NULL,
	[F17] [nvarchar](255) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
