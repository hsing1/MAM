USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <David.Sin>
-- Create date:   <2010/10/02>
-- Description:   �d�߼�������r
-- =============================================

CREATE Proc [dbo].[SP_Q_SEARCH_HOT_KEYWORD]
@FDDATE_BEGIN varchar(20),
@FDDATE_END varchar(20)
As

Select top 20 FSKEYWORD,COUNT(FSKEYWORD) As FNKEYWORD_COUNT
From TBSEARCH_KEYWORD
Where FDDATE Between @FDDATE_BEGIN And @FDDATE_END
Group By FSKEYWORD
Order By FNKEYWORD_COUNT Desc
GO
