USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[SP_I_TBLOG_VIDEO_HD_MC]
@FSBRO_ID	char(12)	,
@FSFILE_NO	char(16)	,
@FSVIDEO_ID	varchar(10)	,
@FSCREATED_BY	varchar(50)
AS
BEGIN
if(exists(select * from TBLOG_VIDEO_HD_MC where FSBRO_ID=@FSBRO_ID))
begin
update TBLOG_VIDEO_HD_MC set FCSTATUS='',FSUPDATED_BY=@FSCREATED_BY,FDUPDATED_DATE=GETDATE() where FSBRO_ID=@FSBRO_ID
end
else
begin
	insert into TBLOG_VIDEO_HD_MC
	(
	fsbro_ID,
	fsfile_no,
	fsvideo_id,
	FCSTATUS,
	FSCREATED_BY,
	fdcreated_date
	)
	values
	(
	@FSBRO_ID	,
@FSFILE_NO	,
@FSVIDEO_ID		,
'',
@FSCREATED_BY,
GETDATE()
	)
END
END

GO
