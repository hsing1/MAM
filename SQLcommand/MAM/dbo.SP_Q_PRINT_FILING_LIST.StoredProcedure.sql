USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2015/04/27>
-- Description:   查詢列印的Filing節目清單
-- =============================================
CREATE Proc [dbo].[SP_Q_PRINT_FILING_LIST]
@FDDATE	date,
@FSCHANNEL_ID	varchar(2)
As
IF (CONVERT(INT, @FSCHANNEL_ID) ) >= 50 
select  distinct A.FSPROG_ID,A.FNSEQNO,A.FNEPISODE,A.FSPROG_NAME ,A.FSVIDEO_ID,A.FNBREAK_NO,A.FSPLAY_TIME,A.FDDATE,A.FSDUR,D.FSCHANNEL_NAME
from tbpgm_combine_queue A
left join TBZCHANNEL D on A.FSCHANNEL_ID=D.FSCHANNEL_ID
where A.FDDATE=@FDDATE and A.FSCHANNEL_ID=@FSCHANNEL_ID
and A.FSVIDEO_ID not in(select FSVIDEO_ID from tbpgm_combine_queue where FDDATE=convert(datetime, @FDDATE, 111)-1  and FSCHANNEL_ID=@FSCHANNEL_ID)
order by A.FSPLAY_TIME
else
begin
select distinct A.FSPROG_ID,A.FNSEQNO,A.FNEPISODE,A.FSPROG_NAME ,A.FSVIDEO_ID,A.FNBREAK_NO,A.FSPLAY_TIME,A.FDDATE,A.FSDUR,D.FSCHANNEL_NAME
from tbpgm_combine_queue A
left join TBZCHANNEL D on A.FSCHANNEL_ID=D.FSCHANNEL_ID
where A.FDDATE=@FDDATE and A.FSCHANNEL_ID=@FSCHANNEL_ID
and A.FSVIDEO_ID not in(select FSVIDEO_ID from tbpgm_combine_queue where FDDATE=convert(datetime, @FDDATE, 111)-1  and FSCHANNEL_ID=@FSCHANNEL_ID)
and A.FSVIDEO_ID not in(select FSVIDEO_ID from tbpgm_combine_queue where FDDATE=@FDDATE and FSCHANNEL_ID=right('00' + Cast(CONVERT(INT, @FSCHANNEL_ID)-50 as varchar(2)),2))
order by A.FSPLAY_TIME
end


GO
