USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_Q_TBLOG_PHOTO]

@FSFILE_NO char(16)
As
SELECT * FROM MAM.dbo.TBLOG_PHOTO
WHERE MAM.dbo.TBLOG_PHOTO.FSFILE_NO=@FSFILE_NO
GO
