USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBLOG_ERRORQRY](
	[LOG_NO] [int] IDENTITY(1,1) NOT NULL,
	[QRY_TYPE] [varchar](50) NOT NULL,
	[QRY_ITEM] [varchar](50) NOT NULL,
	[QRY_BY] [varchar](50) NOT NULL,
	[QRY_DATE] [datetime] NOT NULL,
	[NOTE] [varchar](200) NOT NULL,
 CONSTRAINT [PK_TBLOG_ERRORQRY] PRIMARY KEY CLUSTERED 
(
	[LOG_NO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[TBLOG_ERRORQRY] ADD  CONSTRAINT [DF_Table_1_QRYTYPE]  DEFAULT ('') FOR [QRY_TYPE]
GO
ALTER TABLE [dbo].[TBLOG_ERRORQRY] ADD  CONSTRAINT [DF_Table_1_ITEM]  DEFAULT ('') FOR [QRY_ITEM]
GO
ALTER TABLE [dbo].[TBLOG_ERRORQRY] ADD  CONSTRAINT [DF_TBLOG_ERRORQRY_QRY_BY]  DEFAULT ('') FOR [QRY_BY]
GO
ALTER TABLE [dbo].[TBLOG_ERRORQRY] ADD  CONSTRAINT [DF_TBLOG_ERRORQRY_QRY_DATE]  DEFAULT (getdate()) FOR [QRY_DATE]
GO
ALTER TABLE [dbo].[TBLOG_ERRORQRY] ADD  CONSTRAINT [DF_TBLOG_ERRORQRY_NOTE]  DEFAULT ('') FOR [NOTE]
GO
