USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/04/06>
-- Description:	新增執行記錄並取回執行GUID(新增失敗的會回傳空字串)
-- =============================================
CREATE PROC [dbo].[SP_I_TBLOG_WORKER]
	@FSWORKER_NAME	NVARCHAR(50),
	@FSCREATED_BY	NVARCHAR(50),
	@FSNOTE			NVARCHAR(500)

AS
	SET NOCOUNT ON;

	DECLARE @FSGUID NVARCHAR(36) = NEWID()

	INSERT
		TBLOG_WORKER
		(FSGUID, FSWORKER_NAME, FSCREATED_BY, FSNOTE)
	VALUES
		(@FSGUID, @FSWORKER_NAME, @FSCREATED_BY, @FSNOTE)
		
	IF NOT EXISTS(SELECT FSGUID FROM TBLOG_WORKER WHERE (FSGUID = @FSGUID))
		BEGIN
			SET @FSGUID = ''		-- 新增失敗的會回傳空字串
		END
		
	SELECT FSGUID = @FSGUID


GO
