USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RPT_TBPROPOSAL_01](
	[QUERY_KEY] [uniqueidentifier] NOT NULL,
	[QUERY_BY] [varchar](50) NOT NULL,
	[QUERY_DATE] [datetime] NOT NULL,
	[成案單編號] [char](12) NOT NULL,
	[成案種類_名稱] [varchar](10) NOT NULL,
	[頻道別_名稱] [nvarchar](50) NULL,
	[節目名稱] [nvarchar](50) NOT NULL,
	[播出名稱] [nvarchar](50) NULL,
	[外文名稱] [varchar](100) NULL,
	[製作目的_名稱] [nvarchar](50) NOT NULL,
	[節目編號] [char](7) NOT NULL,
	[每集時長] [int] NOT NULL,
	[集次起] [smallint] NOT NULL,
	[集次迄] [smallint] NOT NULL,
	[節目內容簡述] [nvarchar](max) NULL,
	[預訂播出頻道_名稱] [nvarchar](1700) NOT NULL,
	[製作部門_名稱] [nvarchar](50) NULL,
	[付款部門_名稱] [nvarchar](50) NULL,
	[節目來源_名稱] [nvarchar](50) NULL,
	[節目型態_名稱] [nvarchar](50) NULL,
	[目標觀眾_名稱] [nvarchar](50) NULL,
	[表現方式_名稱] [nvarchar](50) NOT NULL,
	[主聲道_名稱] [nvarchar](50) NULL,
	[副聲道_名稱] [nvarchar](50) NULL,
	[預訂上檔日期] [date] NULL,
	[節目分級_名稱] [nvarchar](50) NULL,
	[前會部門_名稱] [nvarchar](500) NULL,
	[後會部門_名稱] [nvarchar](500) NULL,
	[外購類別大類_名稱] [nvarchar](50) NULL,
	[外購類別細類_名稱] [nvarchar](50) NULL,
	[節目規格] [nvarchar](1700) NULL,
	[列印人員] [nvarchar](50) NULL,
	[分集備註] [nvarchar](200) NULL,
 CONSTRAINT [PK_RPT_TBPROPOSAL_01] PRIMARY KEY CLUSTERED 
(
	[QUERY_KEY] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
