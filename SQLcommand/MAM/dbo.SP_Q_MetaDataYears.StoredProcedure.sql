USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jarvis.Yang
-- Create date: 2013-07-09
-- Description:	尋找特定頻道中節目(or短帶or資料帶)的年分
-- =============================================
CREATE PROCEDURE [dbo].[SP_Q_MetaDataYears] 
@ChannelName varchar(50),
@type char
AS
BEGIN
declare @channelid bigint
	--select  @channelid=FSCHANNEL_ID from TBZCHANNEL where FSCHANNEL_NAME=@ChannelName
	--select LEFT(FSPROMO_ID,4) as Years, * from TBPGM_PROMO where FSMAIN_CHANNEL_ID=@channelid 
select @channelid=FNDIR_ID from TBDIRECTORIES where FSDIR=@ChannelName

if(@type='G')
begin
select distinct LEFT(m.FSPROG_ID,4) as Years from TBDIRECTORIES d
join TBPROG_M m on d.FSDIR COLLATE Chinese_Taiwan_Stroke_BIN =LEFT(m.FSPROG_ID,4) COLLATE Chinese_Taiwan_Stroke_BIN 
 where d.FNPARENT_ID=@channelid
end
else if(@type='P')
begin
select distinct LEFT(p.FSPROMO_ID,4) as Years from TBDIRECTORIES d
join TBPGM_PROMO p on d.FSDIR COLLATE Chinese_Taiwan_Stroke_BIN =LEFT(p.FSPROMO_ID,4) COLLATE Chinese_Taiwan_Stroke_BIN 
 where d.FNPARENT_ID=@channelid
end
else if(@type='D')
begin
select distinct LEFT(da.FSDATA_ID,4) as Years from TBDIRECTORIES d
join TBDATA da on d.FSDIR COLLATE Chinese_Taiwan_Stroke_BIN =LEFT(da.FSDATA_ID,4) COLLATE Chinese_Taiwan_Stroke_BIN 
 where d.FNPARENT_ID=@channelid
end

END




GO
