USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RPT_TBPGM_QUEUE_INSERT_TAPE_01](
	[QUERY_KEY] [uniqueidentifier] NOT NULL,
	[QUERY_KEY_SUBID] [uniqueidentifier] NOT NULL,
	[QUERY_BY] [varchar](50) NOT NULL,
	[QUERY_DATE] [datetime] NOT NULL,
	[播出日期] [date] NOT NULL,
	[頻道] [nvarchar](50) NOT NULL,
	[節目名稱] [nvarchar](50) NOT NULL,
	[集數] [int] NOT NULL,
	[主控插播帶] [varchar](100) NOT NULL,
	[主控鏡面] [varchar](100) NOT NULL,
 CONSTRAINT [PK_RPT_TBPGM_QUEUE_INSERT_TAPE_01] PRIMARY KEY CLUSTERED 
(
	[QUERY_KEY] ASC,
	[QUERY_KEY_SUBID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'報表編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_QUEUE_INSERT_TAPE_01', @level2type=N'COLUMN',@level2name=N'QUERY_KEY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'資料編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_QUEUE_INSERT_TAPE_01', @level2type=N'COLUMN',@level2name=N'QUERY_KEY_SUBID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_QUEUE_INSERT_TAPE_01', @level2type=N'COLUMN',@level2name=N'QUERY_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_QUEUE_INSERT_TAPE_01', @level2type=N'COLUMN',@level2name=N'QUERY_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'播出日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_QUEUE_INSERT_TAPE_01', @level2type=N'COLUMN',@level2name=N'播出日期'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'頻道' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_QUEUE_INSERT_TAPE_01', @level2type=N'COLUMN',@level2name=N'頻道'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_QUEUE_INSERT_TAPE_01', @level2type=N'COLUMN',@level2name=N'節目名稱'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'集數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_QUEUE_INSERT_TAPE_01', @level2type=N'COLUMN',@level2name=N'集數'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主控插播帶' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_QUEUE_INSERT_TAPE_01', @level2type=N'COLUMN',@level2name=N'主控插播帶'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主控鏡面' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_QUEUE_INSERT_TAPE_01', @level2type=N'COLUMN',@level2name=N'主控鏡面'
GO
EXEC sys.sp_addextendedproperty @name=N'描述', @value=N'主控播出題示報表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_QUEUE_INSERT_TAPE_01'
GO
