USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBSEARCH_COLUMN_ENUM](
	[FNNO] [int] NOT NULL,
	[FSFILED_ENAME] [varchar](30) NOT NULL,
	[FSVALUE] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_TBSEARCH_COLUMN_ENUM] PRIMARY KEY CLUSTERED 
(
	[FNNO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�Ǹ�' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSEARCH_COLUMN_ENUM', @level2type=N'COLUMN',@level2name=N'FNNO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���W��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSEARCH_COLUMN_ENUM', @level2type=N'COLUMN',@level2name=N'FSFILED_ENAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSEARCH_COLUMN_ENUM', @level2type=N'COLUMN',@level2name=N'FSVALUE'
GO
EXEC sys.sp_addextendedproperty @name=N'�y�z', @value=N'�˯����C�|' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSEARCH_COLUMN_ENUM'
GO
