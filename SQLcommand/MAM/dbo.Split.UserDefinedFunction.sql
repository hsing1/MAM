USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Split] (
      @InputString VARCHAR(8000),
      @Delimiter VARCHAR(50)
)

RETURNS @Items TABLE (
      Item VARCHAR(8000)
)

AS
BEGIN
      	DECLARE @Item VARCHAR(8000)
      	DECLARE @ItemList VARCHAR(8000)
      	DECLARE @DelimIndex INTEGER 

	SET @InputString = REPLACE(@InputString, CHAR(9), ' ') -- replace tab with white space

      	IF (@Delimiter IS NULL)
            SET @Delimiter = ','

	--INSERT INTO @Items VALUES (@Delimiter) -- Diagnostic
	--INSERT INTO @Items VALUES (@InputString) -- Diagnostic

	SET @ItemList = RTRIM(LTRIM(@InputString))
	SET @DelimIndex = CHARINDEX(@Delimiter, @ItemList, 1)

	WHILE (@DelimIndex != 0)
	BEGIN
      		SET @Item = RTRIM(SUBSTRING(@ItemList, 0, @DelimIndex))
		if (@item != '')
			INSERT INTO @Items VALUES (@Item)

            	SET @ItemList = LTRIM(SUBSTRING(@ItemList, @DelimIndex + 1, LEN(@ItemList) - @DelimIndex))
            	SET @DelimIndex = CHARINDEX(@Delimiter, @ItemList, 0)
	END -- End WHILE

        INSERT INTO @Items VALUES (@ItemList)

	RETURN

END -- End Function

GO
