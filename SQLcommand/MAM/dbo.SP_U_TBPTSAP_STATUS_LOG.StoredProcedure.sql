USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_U_TBPTSAP_STATUS_LOG]
@FSVIDEO_ID    char(8),    
@FCSTATUS    int    ,
@FSPERCENTAGE    int    ,
@FCRESENDTIMES    int    
AS
BEGIN

update TBPTSAP_STATUS_LOG
set 
FCSTATUS=@FCSTATUS,
FSPERCENTAGE=@FSPERCENTAGE,
FCRESENDTIMES=@FCRESENDTIMES,
fdupdate_date=GETDATE()
where FSVIDEOID=@FSVIDEO_ID
    --select * from TBPTSAP_STATUS_LOG
END

GO
