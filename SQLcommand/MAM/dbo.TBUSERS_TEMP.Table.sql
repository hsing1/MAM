USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBUSERS_TEMP](
	[DeptID] [nvarchar](100) NULL,
	[UserID] [nvarchar](100) NULL,
	[UserChtName] [nvarchar](100) NULL,
	[Mail] [nvarchar](100) NULL,
	[JobTitle] [nvarchar](100) NULL,
	[IsSupervisor] [nvarchar](100) NULL,
	[IsMainJobTitle] [nvarchar](100) NULL,
	[Memo] [nvarchar](100) NULL
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'所屬部門ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS_TEMP', @level2type=N'COLUMN',@level2name=N'DeptID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用者ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS_TEMP', @level2type=N'COLUMN',@level2name=N'UserID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用者中文名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS_TEMP', @level2type=N'COLUMN',@level2name=N'UserChtName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'電子郵件' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS_TEMP', @level2type=N'COLUMN',@level2name=N'Mail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'職稱抬頭' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS_TEMP', @level2type=N'COLUMN',@level2name=N'JobTitle'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否為主管(預設N)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS_TEMP', @level2type=N'COLUMN',@level2name=N'IsSupervisor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否為主職稱(預設Y)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS_TEMP', @level2type=N'COLUMN',@level2name=N'IsMainJobTitle'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'註記' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS_TEMP', @level2type=N'COLUMN',@level2name=N'Memo'
GO
