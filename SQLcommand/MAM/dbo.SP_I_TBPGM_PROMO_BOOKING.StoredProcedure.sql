USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   新增短帶託播單資料
-- =============================================
CREATE Proc [dbo].[SP_I_TBPGM_PROMO_BOOKING]

@FNPROMO_BOOKING_NO	int,
@FSPROMO_ID	varchar(12),
@FCSTATUS	varchar(1),
@FSCREATED_BY	varchar(50),
@FSUPDATED_BY	varchar(50),
@FSMEMO	nvarchar(50)

As
Insert Into dbo.TBPGM_PROMO_BOOKING(FNPROMO_BOOKING_NO,FSPROMO_ID,FCSTATUS,FSCREATED_BY,FDCREATED_DATE,
FSUPDATED_BY,FDUPDATED_DATE,FSMEMO)
Values(@FNPROMO_BOOKING_NO,@FSPROMO_ID,@FCSTATUS,@FSCREATED_BY,GETDATE(),
@FSUPDATED_BY,GETDATE(),@FSMEMO)









GO
