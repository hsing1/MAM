USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBMENUS](
	[FSMENU_ID] [varchar](10) NOT NULL,
	[FSMENU] [nvarchar](50) NOT NULL,
	[FSPARENT_MENU_ID] [varchar](10) NULL,
	[FSDESCRIPTION] [nvarchar](100) NULL,
	[FNLEVEL] [int] NULL,
	[FNORDER] [int] NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
	[FSUPDATED_BY] [varchar](50) NULL,
	[FDUPDATED_DATE] [datetime] NULL,
	[FSIMAGECOLLAPSED] [varchar](50) NULL,
	[FSIMAGEEXPANDED] [varchar](50) NULL,
	[FSIMAGEHOVER] [varchar](50) NULL,
	[FCEXPANDED] [char](1) NULL,
	[FSTEMPLATEID] [varchar](50) NULL,
 CONSTRAINT [ TBMENUS] PRIMARY KEY CLUSTERED 
(
	[FSMENU_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'選單代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMENUS', @level2type=N'COLUMN',@level2name=N'FSMENU_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'選單名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMENUS', @level2type=N'COLUMN',@level2name=N'FSMENU'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'上層選單代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMENUS', @level2type=N'COLUMN',@level2name=N'FSPARENT_MENU_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'選單說明' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMENUS', @level2type=N'COLUMN',@level2name=N'FSDESCRIPTION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'階層' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMENUS', @level2type=N'COLUMN',@level2name=N'FNLEVEL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'順序' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMENUS', @level2type=N'COLUMN',@level2name=N'FNORDER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMENUS', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMENUS', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMENUS', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMENUS', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'選單物件使用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMENUS', @level2type=N'COLUMN',@level2name=N'FSIMAGECOLLAPSED'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'選單物件使用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMENUS', @level2type=N'COLUMN',@level2name=N'FSIMAGEEXPANDED'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'選單物件使用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMENUS', @level2type=N'COLUMN',@level2name=N'FSIMAGEHOVER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'選單物件使用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMENUS', @level2type=N'COLUMN',@level2name=N'FCEXPANDED'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'選單物件使用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMENUS', @level2type=N'COLUMN',@level2name=N'FSTEMPLATEID'
GO
