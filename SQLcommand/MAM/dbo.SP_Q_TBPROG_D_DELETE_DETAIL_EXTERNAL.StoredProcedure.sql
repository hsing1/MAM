USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Q_TBPROG_D_DELETE_DETAIL_EXTERNAL]
@FSPROG_ID char(7),
@FNEPISODE varchar(10) --原本是只設定varchar，沒有設定varchar(10)，可是會造成多查了許多資料，改成varchar(10)就正常了，還未找到答案

AS
-----------------------------------

/*把設定的資料表清單取出到@tblList變數*/
DECLARE	@tblList TABLE(TableName VARCHAR(50), TableCName VARCHAR(50))
DECLARE	@tblCnt TABLE(TableName VARCHAR(50), DataCount INT, TableCName VARCHAR(50))

INSERT @tblList SELECT TableName, DataNAme FROM [PTSProg].[PTSProgram].[dbo].[tblProgDependency] WHERE  EpsLink = 1

DECLARE /*@_TableName		VARCHAR(50),*/
		@_SqlTemplate	VARCHAR(1000),
		@_Sql			VARCHAR(1000),
		@_Cnt			INT,
		@_ExistData		BIT

SELECT	/*@_TableName		= '',*/
		@_SqlTemplate	= 'SELECT TableName = ''@_TableName'', DataCount = COUNT(*),TableCName = ''@_TableCName''  FROM [PTSProg].[PTSProgram].[dbo].[@_TableName] WHERE (FSPROGID = ''' + @FSPROG_ID + ''' AND FNEPISODE = '''  + @FNEPISODE +''' ) ',
		@_Sql			= '',
		@_Cnt			= 0 
  
/*判斷是否有列出清單, 沒有列出的預設為避免刪除失敗, 回傳1*/	
DECLARE CSR1 CURSOR FOR SELECT TableName, TableCName FROM @tblList
DECLARE @TableName NVARCHAR(50), @TableCName NVARCHAR(50)

OPEN CSR1
	FETCH NEXT FROM CSR1 INTO @TableName, @TableCName
	WHILE (@@FETCH_STATUS=0)
		BEGIN 
		---------開始處理每一筆資料
			SET @_Sql = REPLACE(@_SqlTemplate,'@_TableName',@TableName) 
			SET @_Sql = REPLACE(@_Sql,'@_TableCName',@TableCName) 			
			INSERT @tblCnt EXEC(@_Sql)
 
		---------處理完畢每一筆資料
		FETCH NEXT FROM CSR1 INTO @TableName, @TableCName
		END
Close CSR1

DELETE FROM @tblCnt WHERE (DATACOUNT = 0)
SELECT * FROM @tblCnt

GO
