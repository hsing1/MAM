USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   查詢時段設定資料
-- =============================================
CREATE Proc [dbo].[SP_Q_TBPGM_TIME_SEG_SET]

As
select FSTIME_SEG_TYPE,
FSBEG_TIME,
FSEND_TIME
from TBPGM_TIME_SEG_SET

GO
