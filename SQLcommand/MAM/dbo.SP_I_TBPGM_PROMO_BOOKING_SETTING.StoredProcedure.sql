USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   新增短帶託播單時段/檔次資料
-- =============================================
CREATE Proc [dbo].[SP_I_TBPGM_PROMO_BOOKING_SETTING]

@FNPROMO_BOOKING_NO	int,
@FSTIME1 varchar(10),
@FSTIME2 varchar(10),
@FSTIME3 varchar(10),
@FSTIME4 varchar(10),
@FSTIME5 varchar(10),
@FSTIME6 varchar(10),
@FSTIME7 varchar(10),
@FSTIME8 varchar(10),
@FSCMNO	varchar(10)


As
Insert Into dbo.TBPGM_PROMO_BOOKING_SETTING(FNPROMO_BOOKING_NO,FSTIME1,FSTIME2,FSTIME3,FSTIME4,FSTIME5,FSTIME6,FSTIME7,FSTIME8,FSCMNO)
Values(@FNPROMO_BOOKING_NO,@FSTIME1,@FSTIME2,@FSTIME3,@FSTIME4,@FSTIME5,@FSTIME6,@FSTIME7,@FSTIME8,@FSCMNO)









GO
