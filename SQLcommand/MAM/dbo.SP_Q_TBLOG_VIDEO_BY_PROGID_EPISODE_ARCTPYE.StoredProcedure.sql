USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   依照ID與集數與資料帶類型資料查詢檔案編號
-- =============================================
CREATE Proc [dbo].[SP_Q_TBLOG_VIDEO_BY_PROGID_EPISODE_ARCTPYE]
@FSID	varchar(11),
@FNEPISODE	smallint,
@FSARC_TYPE	char(3)

As
select FSFILE_NO 
FROM TBLOG_VIDEO  
where FSTYPE = 'G'
and FSID = @FSID 
and FNEPISODE = @FNEPISODE 
and FSARC_TYPE = @FSARC_TYPE
GO
