USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBTRANSCODE_RECV_LOG](
	[FDTIME] [datetime] NOT NULL,
	[FNJOB_ID] [int] NOT NULL,
	[FSCONTENT] [nvarchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
