USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_I_TBUSER_DEP_MAM]

	  --@FNDEP_ID nvarchar(60),
      @FSDEP nvarchar(60),
      @FNPARENT_DEP_ID nvarchar(60),
      @FSSupervisor_ID nvarchar(60),
      @FSUpDepName_ChtName nvarchar(60),
      @FSDpeName_ChtName nvarchar(60),
      @FSFullName_ChtName nvarchar(246),
      @FNDEP_LEVEL nvarchar(60),
      @FBIsDepExist nvarchar(60),
      @FSDEP_MEMO nvarchar(2000)

As
Insert Into dbo.TBUSER_DEP 
(
      [FSDEP]
      ,[FNPARENT_DEP_ID]
      ,[FSSupervisor_ID]
      ,[FSUpDepName_ChtName]
      ,[FSDpeName_ChtName]
      ,[FSFullName_ChtName]
      ,[FNDEP_LEVEL]
      ,[FBIsDepExist]
      ,[FSDEP_MEMO])  
VALUES 
(
      @FSDEP ,
      @FNPARENT_DEP_ID ,
      @FSSupervisor_ID ,
      @FSUpDepName_ChtName ,
      @FSDpeName_ChtName ,
      @FSFullName_ChtName,
      @FNDEP_LEVEL,
      @FBIsDepExist ,
      @FSDEP_MEMO
)

GO
