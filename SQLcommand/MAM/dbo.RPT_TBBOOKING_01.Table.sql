USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RPT_TBBOOKING_01](
	[QUERY_KEY] [uniqueidentifier] NOT NULL,
	[QUERY_KEY_SUBID] [uniqueidentifier] NOT NULL,
	[QUERY_BY] [varchar](50) NOT NULL,
	[QUERY_DATE] [datetime] NOT NULL,
	[主檔_調用單號] [varchar](12) NOT NULL,
	[主檔_調用者帳號] [varchar](50) NOT NULL,
	[主檔_調用日期] [varchar](10) NOT NULL,
	[主檔_調用原因] [varchar](50) NOT NULL,
	[主檔_審核狀態] [varchar](10) NOT NULL,
	[主檔_審核人員] [varchar](50) NOT NULL,
	[主檔_審核日期] [varchar](10) NOT NULL,
	[明細_檔案編號] [varchar](16) NULL,
	[明細_標題] [nvarchar](100) NULL,
	[明細_節目名稱] [nvarchar](50) NULL,
	[明細_時間起迄點] [varchar](50) NULL,
	[明細_檔案類型] [varchar](10) NULL,
	[明細_審核狀態] [varchar](10) NULL,
	[明細_是否外購] [varchar](1) NULL,
	[SDATE] [varchar](10) NOT NULL,
	[EDATE] [varchar](10) NOT NULL,
 CONSTRAINT [PK_RPT_TBBOOKING_01] PRIMARY KEY CLUSTERED 
(
	[QUERY_KEY] ASC,
	[QUERY_KEY_SUBID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
