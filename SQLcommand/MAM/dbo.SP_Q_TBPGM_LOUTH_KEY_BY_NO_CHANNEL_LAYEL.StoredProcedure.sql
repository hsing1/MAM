USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   �d��Louth Key���
-- =============================================
CREATE Proc [dbo].[SP_Q_TBPGM_LOUTH_KEY_BY_NO_CHANNEL_LAYEL]
@FSNO	varchar(4),
@FSCHANNEL_ID varchar(2),
@FSLayel varchar(1)
As
SELECT 
FSCHANNEL_ID,
FSGROUP,
FSNAME,
FSNO,
FSMEMO,
FSLayel,
FSRULE
 FROM TBPGM_LOUTH_KEY where FSNO=@FSNO
 and FSCHANNEL_ID=@FSCHANNEL_ID and FSLayel=@FSLayel

GO
