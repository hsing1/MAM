USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/03/21>
-- Description:	For RPT_TBBROADCAST_01(送帶轉檔記錄), 報表呼叫用
-- =============================================
CREATE PROCEDURE [dbo].[SP_R_TBBROADCAST_01_RPT]
	@QUERY_KEY	VARCHAR(36)
AS
BEGIN
	SELECT
		[QUERY_KEY]
      ,[QUERY_KEY_SUBID]
      ,[QUERY_BY]
      ,[QUERY_DATE]
      ,[送帶轉檔單編號]
      ,[類型_名稱]
      ,[節目_節目名稱]
      ,[節目_集別]
      ,[節目_子集名稱]
      ,[節目_播出頻道]
      ,[節目_總集數]
      ,[節目_每集時長]
      ,[宣傳帶_宣傳帶名稱]
      ,[已轉檔_標題]
      ,[已轉檔_檔案類型]
      ,[已轉檔_起迄]
      ,[列印人員]
	FROM
		RPT_TBBROADCAST_01
	WHERE
		(QUERY_KEY = @QUERY_KEY)
	ORDER BY
		[送帶轉檔單編號], [已轉檔_標題]
END



GO
