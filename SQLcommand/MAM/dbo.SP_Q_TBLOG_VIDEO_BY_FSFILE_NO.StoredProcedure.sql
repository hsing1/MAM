USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        
-- Create date: 
-- Description: 根據FSFILE_NO取得TBLOG_VIDEO資料
-- Update desc:    <2013/04/18><Jarvis.Yang>加入註解。
--Update desc :<2013/06/21><Jarvis.Yang>將原where條件
--WHERE MAM.dbo.TBLOG_VIDEO.FSBRO_ID=@FSFILE_NO
--改成
--WHERE MAM.dbo.TBLOG_VIDEO.FSFILE_NO=@FSFILE_NO

--Update desc :<2013/06/21><Jarvis.Yang>加入join TBUSERS
-- =============================================
CREATE Proc [dbo].[SP_Q_TBLOG_VIDEO_BY_FSFILE_NO]
@FSFILE_NO nvarchar(30)

As
select ISNULL(b.FSUSER_ChtName,'') as FSCREATED_BY_NAME, ISNULL(C.FSUSER_ChtName,'') as FSUPDATED_BY_NAME ,
ISNULL(CONVERT(VARCHAR(20) ,a.FDCREATED_DATE , 111),'') as WANT_FDCREATEDDATE,
ISNULL(CONVERT(VARCHAR(20) ,a.FDUPDATED_DATE , 111),'') as WANT_FDUPDATEDDATE,
a.* FROM TBLOG_VIDEO  as a  
               left outer JOIN TBUSERS as b on a.FSCREATED_BY=b.FSUSER_ID
               left outer join TBUSERS as c on a.FSUPDATED_BY=c.FSUSER_ID
WHERE a.FSFILE_NO=@FSFILE_NO

--SELECT * FROM MAM.dbo.TBLOG_VIDEO
--WHERE MAM.dbo.TBLOG_VIDEO.FSFILE_NO=@FSFILE_NO

GO
