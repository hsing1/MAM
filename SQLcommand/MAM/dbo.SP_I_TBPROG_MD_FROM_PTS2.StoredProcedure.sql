USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/04/10>
-- Description: 從PTS節目資料庫轉入
-- =============================================
CREATE PROCEDURE [dbo].[SP_I_TBPROG_MD_FROM_PTS2]
	@SDate		DATE,
	@EDate		DATE,
	@OldSDate	DATE,
	@OldEDate	DATE,
	@PreDate	SMALLDATETIME
AS
BEGIN
/*============= 轉入PROG_M =============*/
	SET NOCOUNT ON;

	/*宣告資料表變數*/
	DECLARE @TempTable_M TABLE(	[FSPROG_ID] [char](7),
								[FSPGMNAME] [nvarchar](50),		--<<VARCHAR
								[FSPGMENAME] [varchar](100),
								[FSPRDDEPTID] [varchar](2),
								[FSCHANNEL] [varchar](100),		--<<CHAR(20)	NULL=>不可NULL !!!
								[FNTOTEPISODE] [smallint],
								[FNLENGTH] [smallint],
								[FSPROGOBJID] [varchar](2),		--<<NULL=>不可NULL !!!
								[FSPROGSRCID] [varchar](2),
								[FSPROGATTRID] [varchar](2),
								[FSPROGAUDID] [varchar](2),
								[FSPROGTYPEID] [varchar](2),
								[FSPROGGRADEID] [varchar](2),
								[FSPROGLANGID1] [varchar](2),
								[FSPROGLANGID2] [varchar](2),
								[FSPROGSALEID] [varchar](2),
								[FSPROGBUYID] [varchar](2),
								[FSPROGBUYDID] [varchar](2),
								[FSPRDCENID] [varchar](5),
								[FSPRDYYMM] [varchar](6),
								[FDSHOWDATE] [datetime],
								[FSCONTENT] [nvarchar](MAX),	--<< text !!!
								[FSMEMO] [nvarchar](500),		--<< VARCHAR(200)
								[FSEWEBPAGE] [nvarchar](MAX),	--<< text !!!
								[FCDERIVE] [varchar](1),
								[FSWEBNAME] [nvarchar](50),		--<< VARCHAR
								[FSCHANNEL_ID] [char](2),		--<< XXX
								[FSDEL] [char](1),				--<< XXX
								[FSDELUSER] [varchar](50),		--<< XXX
								[FCOUT_BUY] [char](1),			--<< XXX
								[FSCRTUSER] [varchar](5),
								[FDCRTDATE] [datetime],
								[FSUPDUSER] [varchar](5),
								[FDUPDDATE] [datetime],
								--[_FSCONTENT] [text],
								--[_FSEWEBPAGE] [text],
								[_FDTRANDATE] [datetime])
								
	DECLARE @TRANTIME DATETIME = GETDATE()
								
	/*篩選出增修日期落於篩選範圍者*/
	INSERT
		@TempTable_M
	SELECT
		[FSPROGID],[FSPGMNAME],
		[FSPGMENAME] = ISNULL([FSPGMENAME],''),
		[FSPRDDEPTID] = ISNULL([FSPRDDEPTID],''),
		[FSCHANNEL] = MAM.dbo.FN_Q_GET_CODELIST_FROM_PURESTRING(ISNULL([FSCHANNEL],'')),
		[FNTOTEPISODE] = ISNULL([FNTOTEPISODE],0),
		[FNLENGTH] = ISNULL([FNLENGTH],0),
		[FSPROGOBJID] = ISNULL([FSPROGOBJID],''),
		[FSPROGSRCID],[FSPROGATTRID],
		[FSPROGAUDID],[FSPROGTYPEID],[FSPROGGRADEID],[FSPROGLANGID1],[FSPROGLANGID2],
		[FSPROGSALEID] = ISNULL([FSPROGSALEID],''),
		[FSPROGBUYID] = ISNULL([FSPROGBUYID],''),
		[FSPROGBUYDID] = ISNULL([FSPROGBUYDID],''),
		[FSPRDCENID],
		[FSPRDYYMM] = ISNULL([FSPRDYYMM],''),
		[FDSHOWDATE] = ISNULL([FDSHOWDATE],'1900/01/01'),
		[FSCONTENT] =  ISNULL(CAST([FSCONTENT] AS [nvarchar](MAX)),''),
		[FSMEMO] = ISNULL([FSMEMO],''),
		[FSEWEBPAGE] =  ISNULL(CAST([FSEWEBPAGE] AS [nvarchar](MAX)),''),
		[FCDERIVE],
		[FSWEBNAME] = ISNULL([FSWEBNAME],''),
		'','','','',[FSCRTUSER],
		[FDCRTDATE] = ISNULL([FDCRTDATE],'1900/01/01'),
		[FSUPDUSER] = ISNULL([FSUPDUSER],''),
		[FDUPDDATE] = ISNULL([FDUPDDATE],'1900/01/01'),
		--[_FSCONTENT] = CASE WHEN DATALENGTH([FSCONTENT])>1000 THEN [FSCONTENT] ELSE '' END,
		--[_FSEWEBPAGE] = CASE WHEN DATALENGTH([FSEWEBPAGE])>500 THEN [FSEWEBPAGE] ELSE '' END,
		[_FDTRANDATE] = @TRANTIME
	FROM
		[TEST].[PTSProg].[dbo].[TBPROG_M] AS PTS_M
	WHERE
		(CONVERT(NVARCHAR(10), PTS_M.[FDCRTDATE], 111) BETWEEN @SDate AND @EDate) OR
		((CONVERT(NVARCHAR(10), PTS_M.[FDCRTDATE], 111) BETWEEN @OldSDate AND @OldEDate) AND
		 (CONVERT(NVARCHAR(10), PTS_M.[FDUPDDATE], 111) BETWEEN @PreDate AND @TRANTIME))
		
	/*跑回圈處理每一筆資料*/	
	DECLARE CSR1 CURSOR FOR SELECT FSPROG_ID FROM @TempTable_M
	DECLARE @FSPROG_ID_M CHAR(7)

	OPEN CSR1
		FETCH NEXT FROM CSR1 INTO @FSPROG_ID_M
		WHILE (@@FETCH_STATUS=0)
			BEGIN 
			---------開始處理每一筆資料
			DELETE FROM TBPROG_M WHERE (FSPROG_ID = @FSPROG_ID_M)
			
			INSERT TBPROG_M
			SELECT * 
			FROM @TempTable_M
			WHERE ([FSPROG_ID] = @FSPROG_ID_M)
			---------處理完畢每一筆資料
			FETCH NEXT FROM CSR1 INTO @FSPROG_ID_M
			END
	Close CSR1
	
/*============= 轉入PROG_D =============*/
	/*宣告資料表變數*/
	DECLARE @TempTable_D TABLE(	[FSPROG_ID] [char](7),
								[FNEPISODE] [smallint],
								[FSPGDNAME] [nvarchar](50),		--<<VARCHAR
								[FSPGDENAME] [varchar](100),
								[FNLENGTH] [smallint],
								[FNTAPELENGTH] [int],
								[FSPROGSRCID] [varchar](2),
								[FSPROGATTRID] [varchar](2),
								[FSPROGAUDID] [varchar](2),
								[FSPROGTYPEID] [varchar](2),
								[FSPROGGRADEID] [varchar](2),
								[FSPROGLANGID1] [varchar](2),
								[FSPROGLANGID2] [varchar](2),
								[FSPROGSALEID] [varchar](2),	--<<NULL=>不可NULL !!!
								[FSPROGBUYID] [varchar](2),
								[FSPROGBUYDID] [varchar](2),
								[FCRACE] [varchar](1),
								[FSSHOOTSPEC] [varchar](100),	--<<CHAR(50)
								[FSPRDYEAR] [varchar](4),
								[FSPROGGRADE] [nvarchar](100),
								[FSCONTENT] [nvarchar](MAX),	--<<text !!!
								[FSMEMO] [nvarchar](500),		--<<VARCHAR
								[FNSHOWEPISODE] [smallint],
								[FSCHANNEL_ID] [char](2),		--<<XXX
								[FSDEL] [char](1),				--<<XXX
								[FSDELUSER] [varchar](5),		--<<XXX
								[FSNDMEMO] [varchar](500),		--<<XXX
									[FSPROGSPEC] [varchar](100),
								[FSCRTUSER] [varchar](5),
								[FDCRTDATE] [datetime],
								[FSUPDUSER] [varchar](5),
								[FDUPDDATE] [datetime],
								--[_FSCONTENT] [text],	--<<text !!!
								[_FDTRANDATE] [datetime])
								 
	/*篩選出增修日期落於篩選範圍者*/
	INSERT
		@TempTable_D
	SELECT
		[FSPROGID],[FNEPISODE],[FSPGDNAME],
		[FSPGDENAME]= ISNULL([FSPGDENAME],''),
		[FNLENGTH]= ISNULL([FNLENGTH],0),
		[FNTAPELENGTH]= ISNULL([FNTAPELENGTH],0),
		[FSPROGSRCID],[FSPROGATTRID],[FSPROGAUDID],[FSPROGTYPEID],
		[FSPROGGRADEID],[FSPROGLANGID1],[FSPROGLANGID2],
		[FSPROGSALEID] = ISNULL([FSPROGSALEID],''),
		[FSPROGBUYID] = ISNULL([FSPROGBUYID],''),
		[FSPROGBUYDID] = ISNULL([FSPROGBUYDID],''),
		[FCRACE],
		[FSSHOOTSPEC] = MAM.dbo.FN_Q_GET_CODELIST_FROM_PURESTRING(ISNULL([FSSHOOTSPEC],'')),
		[FSPRDYEAR] = ISNULL([FSPRDYEAR],''),
		[FSPROGGRADE] = ISNULL([FSPROGGRADE],''),
		[FSCONTENT] = ISNULL(CAST([FSCONTENT] AS [nvarchar](MAX)),''),
		[FSMEMO] = ISNULL([FSMEMO],''),
		[FNSHOWEPISODE] = ISNULL([FNSHOWEPISODE],0),
		'','','','',
		[FSPROGSPEC] = MAM.dbo.FN_Q_GET_CODELIST_FROM_PURESTRING(ISNULL([FSPROGSPEC],0)),
		[FSCRTUSER],[FDCRTDATE],
		[FSUPDUSER] = ISNULL([FSUPDUSER],''),
		[FDUPDDATE] = ISNULL([FDUPDDATE],'1900/01/01'),
		--[_FSCONTENT] = CASE WHEN DATALENGTH([FSCONTENT])>1000 THEN [FSCONTENT] ELSE '' END,
		[_FDTRANDATE] = @TRANTIME
	FROM
		[TEST].[PTSProg].[dbo].[TBPROG_D] AS PTS_D
	WHERE
		(CONVERT(NVARCHAR(10), PTS_D.[FDCRTDATE], 111) BETWEEN @SDate AND @EDate) OR
		((CONVERT(NVARCHAR(10), PTS_D.[FDCRTDATE], 111) BETWEEN @OldSDate AND @OldEDate) AND
		 (CONVERT(NVARCHAR(10), PTS_D.[FDUPDDATE], 111) BETWEEN @PreDate AND @TRANTIME))
		
	/*跑回圈處理每一筆資料*/	
	DECLARE CSR2 CURSOR FOR SELECT FSPROG_ID, FNEPISODE FROM @TempTable_D
	DECLARE @FSPROG_ID_D CHAR(7), @FNEPISODE_D smallint

	OPEN CSR2
		FETCH NEXT FROM CSR2 INTO @FSPROG_ID_D, @FNEPISODE_D
		WHILE (@@FETCH_STATUS=0)
			BEGIN 
			---------開始處理每一筆資料
			DELETE FROM TBPROG_D WHERE (FSPROG_ID = @FSPROG_ID_D) AND (FNEPISODE = @FNEPISODE_D)
			
			INSERT TBPROG_D
			SELECT * 
			FROM @TempTable_D
			WHERE (FSPROG_ID = @FSPROG_ID_D) AND (FNEPISODE = @FNEPISODE_D)
			---------處理完畢每一筆資料
			FETCH NEXT FROM CSR2 INTO @FSPROG_ID_D, @FNEPISODE_D
			END
	Close CSR2	
END



GO
