USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/24>
-- Description:   刪除節目播映資料
-- =============================================
CREATE Proc [dbo].[SP_D_TBPGM_BANK_DETAIL]

@FSPROG_ID	char(7),
@FNSEQNO	int
As
delete from dbo.TBPGM_BANK_DETAIL 
where FSPROG_ID=@FSPROG_ID and FNSEQNO=@FNSEQNO
GO
