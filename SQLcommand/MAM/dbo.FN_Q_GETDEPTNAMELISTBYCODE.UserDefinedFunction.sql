USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/05/04>
-- Description: 
-- =============================================
CREATE FUNCTION [dbo].[FN_Q_GETDEPTNAMELISTBYCODE]
(
	@DEPT	VARCHAR(100)
)
RETURNS	VARCHAR(1000)
AS
BEGIN
	-- Declare the return variable here
		Declare @cnt int, @idx int, @i int, @result VARCHAR(500)

	-- Add the T-SQL statements to compute the return value here
		SELECT @cnt = 0, @idx = 0 , @i = 0, @result = ''

		While @idx < Len(@DEPT)
		BEGIN
			IF CHARINDEX(';',SUBSTRING(@DEPT,@idx,Len(@DEPT)-@idx))<>0
				BEGIN
					Set @idx = @idx + CHARINDEX(';',SUBSTRING(@DEPT,@idx,Len(@DEPT)-@idx))
					Set @cnt = @cnt + 1
				END
			ELSE
				BEGIN
					Set @idx = Len(@DEPT)
				END
		END

		While @i < @cnt +1
		BEGIN 
			Declare @Code nvarchar(20)
			Declare @Name nvarchar(100)
			
			Set @Code = dbo.FN_Q_GETITEMBYINDEX(@DEPT,@i)
			
			IF(@Code <> '')
			 BEGIN
			 	Set @Name = ISNULL((SELECT [FSDEPTNAME] FROM [TBZDEPT] WHERE [FSDEPTID] = @Code),@Code)
				Set @result = @result + @Name + ';'
			 END

			Set @i = @i + 1
		END

	-- Return the result of the function
		RETURN @result
END

GO
