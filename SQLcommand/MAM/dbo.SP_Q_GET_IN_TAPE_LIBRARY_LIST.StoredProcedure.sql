USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        
-- Create date: 
-- Description: 取得14天內要播出的已入庫的檔案LIST
-- Update desc:    <2016/01/04><Mike>加入註解。
-- =============================================
CREATE PROC [dbo].[SP_Q_GET_IN_TAPE_LIBRARY_LIST]

AS
select distinct A.FSPROG_ID as FSID,A.FNEPISODE,B.FSFILE_NO,B.FSVIDEO_PROG,B.FCFILE_STATUS,B.FSBRO_ID,C.FCSTATUS,C.FSCREATED_BY
from tbpgm_queue A
left join TBLOG_Video B on A.FSPROG_ID=B.FSID and A.FNEPISODE=B.FNEPISODE and B.FSARC_TYPE='002'
inner join TBZCHANNEL E on A.FSCHANNEL_ID=E.FSCHANNEL_ID and E.FSCHANNEL_TYPE='002'
left join TBLOG_Video_HD_MC C on B.FSFILE_NO=C.FSFILE_NO 
where 
A.FDDATE>=getdate()-1 and A.FDDATE<=getdate()+14
and (B.FCFILE_STATUS='T' or B.FCFILE_STATUS='Y')
union
select distinct A.FSPROMO_ID AS FSID,0,B.FSFILE_NO,B.FSVIDEO_PROG,B.FCFILE_STATUS,B.FSBRO_ID,C.FCSTATUS,C.FSCREATED_BY
from TBPGM_ARR_PROMO A
left join TBLOG_Video B on A.FSPROMO_ID=B.FSID and B.FNEPISODE=0 and B.FSARC_TYPE='002'
inner join TBZCHANNEL E on A.FSCHANNEL_ID=E.FSCHANNEL_ID and E.FSCHANNEL_TYPE='002'
left join TBLOG_Video_HD_MC C on B.FSFILE_NO=C.FSFILE_NO 
where 
A.FDDATE>=getdate()-1 and A.FDDATE<=getdate()+14
and (B.FCFILE_STATUS='T' or B.FCFILE_STATUS='Y')

GO
