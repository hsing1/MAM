USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   查詢短帶在運行表中的排播狀況
-- =============================================
CREATE Proc [dbo].[SP_Q_TBPGM_ARR_PROMO_BY_PROMOID]
@FSPROMO_ID	varchar(11)

As
Select ISNULL(CONVERT(VARCHAR(20) ,A.FDDATE , 111),'') AS Want_Date,A.FSPLAYTIME,B.FSCHANNEL_NAME 
from TBPGM_ARR_PROMO A,TBZCHANNEL B 
where A.FSCHANNEL_ID=B.FSCHANNEL_ID and A.FSPROMO_ID=@FSPROMO_ID


GO
