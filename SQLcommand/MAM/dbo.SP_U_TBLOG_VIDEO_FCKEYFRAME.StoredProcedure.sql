USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/04/07>
-- Description:	修改TBLOG_VIDEO的FCKEYFRAME欄位
-- =============================================
CREATE PROC [dbo].[SP_U_TBLOG_VIDEO_FCKEYFRAME]
	@FSFILE_NO		NVARCHAR(16),
	@FCKEYFRAME		NVARCHAR(1),
	@FSUPDATED_BY	NVARCHAR(50),
	@FDUPDATED_DATE	DATETIME
	
AS
	SET NOCOUNT ON;
	 
	DECLARE @RESULT NVARCHAR(50)
	 
	UPDATE
		TBLOG_VIDEO
	SET
		FCKEYFRAME		= @FCKEYFRAME,
		FSUPDATED_BY	= @FSUPDATED_BY,
		FDUPDATED_DATE	= @FDUPDATED_DATE 
	WHERE
		(FSFILE_NO		= @FSFILE_NO)
		
	IF NOT EXISTS(SELECT FSFILE_NO FROM TBLOG_VIDEO WHERE (FSFILE_NO = @FSFILE_NO) AND (FDUPDATED_DATE	= @FDUPDATED_DATE))
		BEGIN
			SET @RESULT = ''	-- 比對不到的會回傳錯誤訊息
		END
	ELSE
		BEGIN
			SET @RESULT = @FSFILE_NO	 
		END	
		
		 
 

GO
