USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------
--2012/11/19 <David.Sin> 新增SP_Q_DELETED_PROGRAM 查詢節目
------------------------------------------------------------------
CREATE PROC [dbo].[SP_Q_DELETED_PROGRAM]
@FSCHANNEL_ID varchar(2),
@FSPROG_TYPE char(1),
@FSPROG_ID varchar(11),
@FSPROG_NAME nvarchar(50)

AS

--節目
IF @FSPROG_TYPE = 'G'
BEGIN

	SELECT
		[FSPROG_ID] AS [ID] , [FSPGMNAME] AS [NAME]
	FROM
		[dbo].[TBPROG_M]
	WHERE
		(@FSPROG_ID = '' OR [FSPROG_ID] = @FSPROG_ID) AND
		(@FSPROG_NAME = '' OR [FSPGMNAME] LIKE '%' + @FSPROG_NAME + '%') AND
		(@FSCHANNEL_ID = '' OR [FSCHANNEL_ID] = @FSCHANNEL_ID)
		
END
ELSE IF @FSPROG_TYPE = 'P'
BEGIN

	SELECT
		[FSPROMO_ID] AS [ID] , [FSPROMO_NAME] AS [NAME]
	FROM
		[dbo].[TBPGM_PROMO]
	WHERE
		(@FSPROG_ID = '' OR [FSPROMO_ID] = @FSPROG_ID) AND
		(@FSPROG_NAME = '' OR [FSPROMO_NAME] LIKE '%' + @FSPROG_NAME + '%') AND
		(@FSCHANNEL_ID = '' OR [FSMAIN_CHANNEL_ID] = @FSCHANNEL_ID)
		
END
ELSE IF @FSPROG_TYPE = 'D'
BEGIN

	SELECT
		[FSDATA_ID] AS [ID] , [FSDATA_NAME] AS [NAME]
	FROM
		[dbo].[TBDATA]
	WHERE
		(@FSPROG_ID = '' OR [FSDATA_ID] = @FSPROG_ID) AND
		(@FSPROG_NAME = '' OR [FSDATA_NAME] LIKE '%' + @FSPROG_NAME + '%')
		
END


GO
