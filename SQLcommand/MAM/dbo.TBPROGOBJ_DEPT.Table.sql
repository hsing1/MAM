USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBPROGOBJ_DEPT](
	[FSOBJ_ID] [varchar](2) NOT NULL,
	[FSBEF_DEPT_ID] [varchar](100) NULL,
	[FSAFT_DEPT_ID] [varchar](100) NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
	[FSUPDATED_BY] [varchar](50) NULL,
	[FDUPDATED_DATE] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'製作目的' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGOBJ_DEPT', @level2type=N'COLUMN',@level2name=N'FSOBJ_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'前會部門' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGOBJ_DEPT', @level2type=N'COLUMN',@level2name=N'FSBEF_DEPT_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'後會部門' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGOBJ_DEPT', @level2type=N'COLUMN',@level2name=N'FSAFT_DEPT_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGOBJ_DEPT', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGOBJ_DEPT', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGOBJ_DEPT', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGOBJ_DEPT', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
