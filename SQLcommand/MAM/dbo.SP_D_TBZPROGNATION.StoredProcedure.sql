USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		林冠宇
-- Create date: 2012/12/24
-- Description:	刪除國家來源代碼
-- =============================================
create PROCEDURE [dbo].[SP_D_TBZPROGNATION]
      @ID nvarchar(100)
AS
BEGIN
	delete TBZPROGNATION where ID=@ID
END

GO
