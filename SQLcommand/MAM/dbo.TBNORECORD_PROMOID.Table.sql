USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBNORECORD_PROMOID](
	[FSYYYYMM] [char](6) NOT NULL,
	[FSNO] [char](5) NOT NULL,
	[FSPROMO_NAME] [nvarchar](50) NULL,
	[FSCREATED_BY] [varchar](50) NULL,
	[FDCREATED_DATE] [datetime] NULL,
 CONSTRAINT [PK_TBNORECORD_PROMOID] PRIMARY KEY CLUSTERED 
(
	[FSYYYYMM] ASC,
	[FSNO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'取PROMO的年月日' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBNORECORD_PROMOID', @level2type=N'COLUMN',@level2name=N'FSYYYYMM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'流水編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBNORECORD_PROMOID', @level2type=N'COLUMN',@level2name=N'FSNO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'取號的中文名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBNORECORD_PROMOID', @level2type=N'COLUMN',@level2name=N'FSPROMO_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'取號者ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBNORECORD_PROMOID', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'取號日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBNORECORD_PROMOID', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
