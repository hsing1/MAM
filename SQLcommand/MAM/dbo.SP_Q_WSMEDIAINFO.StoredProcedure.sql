USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/05/06>
-- Description:	For WSMEDIAINFO, 報表呼叫用
-- =============================================
CREATE ProcEDURE [dbo].[SP_Q_WSMEDIAINFO]
	@FSFILE_NO	VARCHAR(16)
AS
BEGIN
	--DECLARE @FSFILE_NO	VARCHAR(16)	
	--SELECT @FSFILE_NO = 'G201000100040001'--'G000034400050001'
		
	-------------------------宣告程序內使用各參數

	DECLARE @Descriptor0 NVARCHAR(500), @Descriptor1 NVARCHAR(1000), @Descriptor2 NVARCHAR(1000),
			@Component1 NVARCHAR(1000), @Component2 NVARCHAR(1000), 
			@Component3_all NVARCHAR(MAX), @Component3_template NVARCHAR(300), @Component3_item NVARCHAR(300),
			@XMLROOT	NVARCHAR(MAX), @XDOC	XML
	DECLARE @D0_01 VARCHAR(10), @D0_02 VARCHAR(50), @D0_03 VARCHAR(MAX), @D0_04 VARCHAR(5), @D0_05 NVARCHAR(50)
	DECLARE @D1_01 SMALLINT, @D1_02 NVARCHAR(50), @D1_03 NVARCHAR(MAX), @D1_04 VARCHAR(5), @D1_05 VARCHAR(10), 
			@D1_06 VARCHAR(10), @D1_07 VARCHAR(10), @D1_08 NVARCHAR(100), @D1_09 NVARCHAR(100), @D1_10 NVARCHAR(500), 
			@D1_11 NVARCHAR(50), @D1_12 NVARCHAR(50), @D1_13 NVARCHAR(50), @D1_14 NVARCHAR(10) 
	DECLARE @D2_01 VARCHAR(16), @D2_02 SMALLINT, /*@D2_03, @D2_04, @D2_05, */
			@D2_06 NVARCHAR(20), /*@D2_07, @D2_08, @D2_09,*/  @D2_10 VARCHAR(10), 
			@D2_11 NVARCHAR(50), @D2_12 NVARCHAR(50), @D2_13 NVARCHAR(200),
			@D2_12a VARCHAR(15), @D2_12b VARCHAR(15)
	DECLARE	@C1_01 NVARCHAR(200)	
	DECLARE	@C3_01 NVARCHAR(200)
			
	-------------------------設定程序內XML參數

	SELECT @Descriptor0 =
	'
	<Descriptor>
		<Statement mimeType="text/xml">
			<MediaProgram>
				<Id>@D0_01</Id>
				<Name>@D0_02</Name>
				<Description>@D0_03</Description> 
				<TotalEpisode>@D0_04</TotalEpisode>
				<Notes>@D0_05</Notes>
			</MediaProgram>
		</Statement>
	</Descriptor>
	'

	SELECT @Descriptor1 =
	'
	<Descriptor>
		<Statement mimeType="text/xml">
			<MediaProgramEpisode>
				<Episode>@D1_01</Episode> 
				<Name>@D1_02</Name>
				<Description>@D1_03</Description> 
				<Duration>@D1_04</Duration> 
				<Language>@D1_05</Language> 
				<ProduceDepartment>@D1_06</ProduceDepartment> 
				<ProduceDate>@D1_07</ProduceDate> 
				<Director>@D1_08</Director> 
				<Producer>@D1_09</Producer> 
				<Actors>@D1_10</Actors> 
				<ThemeCategory>@D1_11</ThemeCategory> 
				<MediaCategory>@D1_12</MediaCategory> 
				<AudienceCategory>@D1_13</AudienceCategory> 
				<PremiereDate>@D1_14</PremiereDate> 
			</MediaProgramEpisode>
		</Statement>
	</Descriptor>
	'

	SELECT @Descriptor2 =
	'
	<Descriptor>
		<Statement mimeType="text/xml">
			<MediaEssence>
				<MAMId>@D2_01</MAMId> 
				<Segment>@D2_02</Segment> 
				<IsSchedulable></IsSchedulable> 
				<IsForSubscribe></IsForSubscribe> 
				<IsMediaShown></IsMediaShown> 
				<MediaType>@D2_06</MediaType> 
				<MediaCategory></MediaCategory> 
				<FPS>29.97</FPS>
				<TimeCodeFormat>HH:mm:SS;FF</TimeCodeFormat> 
				<VideoFormat>@D2_10</VideoFormat> 
				<FileSize>@D2_11</FileSize> 
				<Duration>@D2_12</Duration> 
				<Notes>@D2_13</Notes> 
			</MediaEssence>
		</Statement>
	</Descriptor>
	'

	SELECT @Component1 =
	'
	<Component>
		<Descriptor>
			<Statement mimeType="text/xml">
				<PreviewFile>
					<Description></Description> 
				</PreviewFile>
			</Statement>
		</Descriptor>
		<Resource ref="@C1_01" mimeType="" /> 
	</Component>
	'
	 
	SELECT @Component2 =
	'
	<Component>
		<Descriptor>
			<Statement mimeType="text/xml">
				<FullFile>
					<Description></Description>
				</FullFile>
			</Statement>
		</Descriptor>
		<Resource ref="" mimeType="" /> 
	</Component>
	'

	SELECT @Component3_template = 
	'
	<Component>
		<Descriptor>
			<Statement mimeType="text/xml">
				<KeyFrame>
					<Description>@C3_01</Description> 
				</KeyFrame>
			</Statement>
		</Descriptor>
		<Resource ref="待替換@C3_ref" mimeType="image/jpg" /> 
	</Component>
	'

	-------------------------取出各欄位至相關參數內

	SELECT
	/* MediaProgram */
		@D0_01	= _M.FSPROG_ID, 
		@D0_02	= _M.FSPGMNAME,
		@D0_03	= ISNULL(_M.FSCONTENT,''),
		@D0_04	= ISNULL(_M.FNTOTEPISODE,''),	--smallint
		@D0_05	= ISNULL(_M.FSMEMO,''),
		
	/* MediaProgramEpisode */		
		@D1_01	= _D.FNEPISODE,
		@D1_02	= _D.FSPGDNAME,
		@D1_03	= ISNULL(_D.FSCONTENT,''),
		@D1_04	= ISNULL(_D.FNLENGTH,''),		--smallint
		@D1_05	= ISNULL(LANG.FSPROGLANGNAME,'???'+_D.FSPROGLANGID1),
			
		@D1_06 	= _M.FSPRDCENID,	--下面處理 
		@D1_07 	= ISNULL(_M.FSPRDYYMM,''),
		@D1_08 	= '',		--下面處理
		@D1_09 	= '',		--下面處理
		@D1_10 	= '',		--下面處理
			
		@D1_11 	= ISNULL(ATTR.FSPROGATTRNAME,'???'+_M.FSPROGATTRID),
		@D1_12 	= ISNULL(GTYP.FSPROGTYPENAME,'???'+_M.FSPROGTYPEID),
		@D1_13 	= ISNULL(AUD.FSPROGAUDNAME,'???'+_M.FSPROGAUDID),
		@D1_14 	= ISNULL(_D.FSPRDYEAR,''),
		
	/* MediaEssence */	
		@D2_01	= VDO.FSFILE_NO,
		@D2_02	= (SELECT COUNT(*) FROM TBLOG_VIDEO_SEG WHERE (FSFILE_NO = VDO.FSFILE_NO)),
		
		@D2_06	= ISNULL(FT.FSTYPE_NAME,'???'+VDO.FSARC_TYPE),
		@D2_10	= ISNULL(VDO.FSFILE_TYPE_HV,''),
		
		@D2_11	= ISNULL(VDO.FSFILE_SIZE,''),
		@D2_12	= '',	--下面處理
			@D2_12a	= VDO.FSBEG_TIMECODE,
			@D2_12b	= VDO.FSEND_TIMECODE,
		@D2_13	= ISNULL(VDO.FSDESCRIPTION ,'')

	FROM
		TBLOG_VIDEO AS VDO
		LEFT JOIN TBPROG_D AS _D ON (_D.FSPROG_ID = VDO.FSID) AND (_D.FNEPISODE = VDO.FNEPISODE)
		LEFT JOIN TBPROG_M AS _M ON (_M.FSPROG_ID = _D.FSPROG_ID)
		--LEFT JOIN TBPROGMEN AS MEN ON (MEN.FSPROG_ID = VDO.FSID) AND (MEN..FNEPISODE = VDO.FNEPISODE)
		LEFT JOIN TBZPROGLANG AS LANG ON (LANG.FSPROGLANGID = _D.FSPROGLANGID1)
		LEFT JOIN TBZPROGATTR AS ATTR ON (ATTR.FSPROGATTRID = _M.FSPROGATTRID)
		LEFT JOIN TBZPROGTYPE AS GTYP ON (GTYP.FSPROGTYPEID = _M.FSPROGTYPEID)
		LEFT JOIN TBZPROGAUD AS AUD ON (AUD.FSPROGAUDID = _M.FSPROGAUDID)
		LEFT JOIN TBFILE_TYPE AS FT ON (FT.FSARC_TYPE = VDO.FSARC_TYPE)

	WHERE
		(FSFILE_NO = @FSFILE_NO)

	-------------------------從節目管理系統取出參數

	/*製作單位@D1_06*/
	--@D1_06 = _M.FSPRDCENID
	BEGIN TRY 
		SET @D1_06	= ISNULL((SELECT FSPRDCENNAME FROM [PTSProg].[PTSProgram].dbo.TBZPRDCENTER WHERE (FSPRDCENID = @D1_06)),'???')  
	END TRY
	BEGIN CATCH 
		SET @D1_06	= 'ERROR:連結節目管理系統時發生錯誤'  
	END CATCH

	--select FSPRDCENNAME FROM [PTSProg].[PTSProg].[dbo].[TBZPRDCENTER] WHERE (FSPRDCENID = _M.FSPRDCENID)

	/*相關人員*/
	--@D0_01 = _M.FSPROG_ID
	--@D1_01 = _D.FNEPISODE
		
	DECLARE @tblMEN TABLE(FSTITLEID NVARCHAR(4), FSSTAFFID VARCHAR(10), _FSNAME NVARCHAR(50))
	INSERT	@tblMEN
	SELECT	FSTITLEID, FSSTAFFID, ''
	FROM	TBPROGMEN
	WHERE	(FSPROG_ID = @D0_01) AND (FNEPISODE = @D1_01) AND (FSTITLEID IN ('01','02','03','49'))	--01:製作人/02:導演/03:演出人員/49:導播

		--/*下面這幾行是暫時塞資料用*/
		--INSERT	@tblMEN	SELECT	'01', '00002', ''
		--INSERT	@tblMEN	SELECT	'02', '00003', ''
		--INSERT	@tblMEN	SELECT	'03', '00005', ''
		--INSERT	@tblMEN	SELECT	'03', '00005', ''
		--INSERT	@tblMEN	SELECT	'03', '00005', ''
		--INSERT	@tblMEN	SELECT	'03', '00005', '' 
		--/*上面這幾行是暫時塞資料用*/
		
	DECLARE CSR2 CURSOR FOR SELECT FSTITLEID, FSSTAFFID FROM @tblMEN
	DECLARE @_FSTITLEID NVARCHAR(4), @_FSSTAFFID NVARCHAR(10), @_FSNAME NVARCHAR(50)

	OPEN CSR2
		FETCH NEXT FROM CSR2 INTO @_FSTITLEID, @_FSSTAFFID
		WHILE (@@FETCH_STATUS=0)
			BEGIN 
			---------開始處理每一筆資料
				BEGIN TRY 
					SET @_FSNAME	= ISNULL((SELECT FSNAME FROM [PTSProg].[PTSProgram].dbo.TBSTAFF WHERE (FSSTAFFID = @_FSSTAFFID)),'???')  
				END TRY
				BEGIN CATCH 
					SET @_FSNAME	= 'ERROR:連結節目管理系統時發生錯誤'  
				END CATCH
				
				IF (@_FSTITLEID IN ('02','49'))
					SET @D1_08 	= @D1_08 + @_FSNAME + ';'
				IF (@_FSTITLEID = '01')
					SET @D1_09 	= @D1_09 + @_FSNAME + ';'
				IF (@_FSTITLEID = '03')
					SET @D1_10 	= @D1_10 + @_FSNAME + ';'
						
			---------處理完畢每一筆資料
			FETCH NEXT FROM CSR2 INTO @_FSTITLEID, @_FSSTAFFID
			END
	Close CSR2 

	-------------------------計算時間
	--@D2_12
		
	IF ((ISNULL(@D2_12a,'') = '') OR (ISNULL(@D2_12b,'') = ''))
		BEGIN
			SET @D2_12 = ''
		END
	ELSE
		BEGIN
			BEGIN TRY
				DECLARE @HH1 SMALLINT = 0, @MM1 SMALLINT = 0, @SS1 SMALLINT = 0,
						@HH2 SMALLINT = 0, @MM2 SMALLINT = 0, @SS2 SMALLINT = 0
				SELECT
					@HH1 = CAST(SUBSTRING(@D2_12a,1,2) AS SMALLINT),
					@MM1 = CAST(SUBSTRING(@D2_12a,4,2) AS SMALLINT),
					@SS1 = CAST(SUBSTRING(@D2_12a,7,2) AS SMALLINT),
					@HH2 = CAST(SUBSTRING(@D2_12b,1,2) AS SMALLINT),
					@MM2 = CAST(SUBSTRING(@D2_12b,4,2) AS SMALLINT),
					@SS2 = CAST(SUBSTRING(@D2_12b,7,2) AS SMALLINT)
				SET @D2_12 = (@HH2-@HH1)*3600 + (@MM2-@MM1)*60 + (@SS2-@SS1)
			END TRY
			BEGIN CATCH
				SET @D2_12 =  @D2_12a + '-' + @D2_12b
			END CATCH	
		END
		  
	-------------------------把相關的KEYFRAME串回

	--@D2_01 = VDO.FSFILE_NO

	DECLARE @tblKEYFRAME TABLE (FSFILE_NO VARCHAR(25),FSDESCRIPTION NVARCHAR(MAX))
	 
	INSERT	@tblKEYFRAME
	SELECT	FSFILE_NO, FSDESCRIPTION
	FROM	TBLOG_VIDEO_KEYFRAME
	WHERE	(FSVIDEO_NO = @D2_01)

	--依照抓出來的跑回圈塞值

	DECLARE CSR1 CURSOR FOR SELECT FSFILE_NO, FSDESCRIPTION FROM @tblKEYFRAME
	DECLARE @_FSFILE_NO NVARCHAR(25), @_FSDESCRIPTION NVARCHAR(MAX)

	SET @Component3_all = ''

	OPEN CSR1
		FETCH NEXT FROM CSR1 INTO @_FSFILE_NO, @_FSDESCRIPTION
		WHILE (@@FETCH_STATUS=0)
			BEGIN 
			---------開始處理每一筆資料
				SET @Component3_item = @Component3_template 			
				SET @Component3_item = REPLACE(@Component3_item,'@C3_01',RIGHT(@_FSFILE_NO,8) + ': ' + dbo.FN_Q_GET_XML_BY_FILTER(@_FSDESCRIPTION))
				SET @Component3_item = REPLACE(@Component3_item,'@C3_ref','@' +RIGHT(@_FSFILE_NO,8))
				SET @Component3_all += @Component3_item
			---------處理完畢每一筆資料
			FETCH NEXT FROM CSR1 INTO @_FSFILE_NO, @_FSDESCRIPTION
			END
	Close CSR1

	-------------------------置換參數內各XML保留字

	--&lt; < // &gt; > // &amp; & // &apos; ' // &quot; "

	SET @D0_01 = dbo.FN_Q_GET_XML_BY_FILTER(@D0_01)		SET @D0_02 = dbo.FN_Q_GET_XML_BY_FILTER(@D0_02)
	SET @D0_03 = dbo.FN_Q_GET_XML_BY_FILTER(@D0_03)		SET @D0_04 = dbo.FN_Q_GET_XML_BY_FILTER(@D0_04)
	SET @D0_05 = dbo.FN_Q_GET_XML_BY_FILTER(@D0_05)

	SET @D1_01 = dbo.FN_Q_GET_XML_BY_FILTER(@D1_01)		SET @D1_02 = dbo.FN_Q_GET_XML_BY_FILTER(@D1_02)
	SET @D1_03 = dbo.FN_Q_GET_XML_BY_FILTER(@D1_03)		SET @D1_04 = dbo.FN_Q_GET_XML_BY_FILTER(@D1_04)
	SET @D1_05 = dbo.FN_Q_GET_XML_BY_FILTER(@D1_05)		SET @D1_06 = dbo.FN_Q_GET_XML_BY_FILTER(@D1_06)
	SET @D1_07 = dbo.FN_Q_GET_XML_BY_FILTER(@D1_07)		SET @D1_08 = dbo.FN_Q_GET_XML_BY_FILTER(@D1_08)
	SET @D1_09 = dbo.FN_Q_GET_XML_BY_FILTER(@D1_09)		SET @D1_10 = dbo.FN_Q_GET_XML_BY_FILTER(@D1_10)
	SET @D1_11 = dbo.FN_Q_GET_XML_BY_FILTER(@D1_11)		SET @D1_12 = dbo.FN_Q_GET_XML_BY_FILTER(@D1_12)
	SET @D1_13 = dbo.FN_Q_GET_XML_BY_FILTER(@D1_13)		SET @D1_14 = dbo.FN_Q_GET_XML_BY_FILTER(@D1_14)

	SET @D2_01 = dbo.FN_Q_GET_XML_BY_FILTER(@D2_01)		SET @D2_02 = dbo.FN_Q_GET_XML_BY_FILTER(@D2_02) 
	SET @D2_06 = dbo.FN_Q_GET_XML_BY_FILTER(@D2_06)		SET @D2_10 = dbo.FN_Q_GET_XML_BY_FILTER(@D2_10)
	SET @D2_11 = dbo.FN_Q_GET_XML_BY_FILTER(@D2_11)		SET @D2_12 = dbo.FN_Q_GET_XML_BY_FILTER(@D2_12)
	SET @D2_13 = dbo.FN_Q_GET_XML_BY_FILTER(@D2_13)	
	 
	-------------------------開始置換XML參數內各變數

	SET @Descriptor0 = REPLACE(@Descriptor0,'@D0_01',@D0_01)	SET @Descriptor0 = REPLACE(@Descriptor0,'@D0_02',@D0_02)
	SET @Descriptor0 = REPLACE(@Descriptor0,'@D0_03',@D0_03)	SET @Descriptor0 = REPLACE(@Descriptor0,'@D0_04',@D0_04)
	SET @Descriptor0 = REPLACE(@Descriptor0,'@D0_05',@D0_05)

	SET @Descriptor1 = REPLACE(@Descriptor1,'@D1_01',@D1_01)	SET @Descriptor1 = REPLACE(@Descriptor1,'@D1_02',@D1_02)
	SET @Descriptor1 = REPLACE(@Descriptor1,'@D1_03',@D1_03)	SET @Descriptor1 = REPLACE(@Descriptor1,'@D1_04',@D1_04)
	SET @Descriptor1 = REPLACE(@Descriptor1,'@D1_05',@D1_05)	SET @Descriptor1 = REPLACE(@Descriptor1,'@D1_06',@D1_06)
	SET @Descriptor1 = REPLACE(@Descriptor1,'@D1_07',@D1_07)	SET @Descriptor1 = REPLACE(@Descriptor1,'@D1_08',@D1_08)
	SET @Descriptor1 = REPLACE(@Descriptor1,'@D1_09',@D1_09)	SET @Descriptor1 = REPLACE(@Descriptor1,'@D1_10',@D1_10)
	SET @Descriptor1 = REPLACE(@Descriptor1,'@D1_11',@D1_11)	SET @Descriptor1 = REPLACE(@Descriptor1,'@D1_12',@D1_12)
	SET @Descriptor1 = REPLACE(@Descriptor1,'@D1_13',@D1_13)	SET @Descriptor1 = REPLACE(@Descriptor1,'@D1_14',@D1_14)

	SET @Descriptor2 = REPLACE(@Descriptor2,'@D2_01',@D2_01)	SET @Descriptor2 = REPLACE(@Descriptor2,'@D2_02',@D2_02)
	SET @Descriptor2 = REPLACE(@Descriptor2,'@D2_06',@D2_06)	SET @Descriptor2 = REPLACE(@Descriptor2,'@D2_10',@D2_10)
	SET @Descriptor2 = REPLACE(@Descriptor2,'@D2_11',@D2_11)	SET @Descriptor2 = REPLACE(@Descriptor2,'@D2_12',@D2_12)
	SET @Descriptor2 = REPLACE(@Descriptor2,'@D2_13',@D2_13)

	-------------------------

	SELECT @XMLROOT = 
	'<?xml version="1.0" ?>' +
	'<DIDL xmlns="urn:mpeg:mpeg21:2002:02-DIDL-NS" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="urn:mpeg:mpeg21:2002:02-DIDL-NS http://standards.iso.org/ittf/PubliclyAvailableStandards/MPEG-21_schema_files/did/didl.xsd">' +
	'	<Container>' +
			@Descriptor0 +
	'		<Item>' +
			@Descriptor1 +
			@Descriptor2 +
			@Component1 +
			@Component2 +
			@Component3_all +
	'		</Item>' +
	'	</Container>' +
	'</DIDL>'

	-----------------------

	SET @XDOC = @XMLROOT

	SELECT @XDOC

END

GO
