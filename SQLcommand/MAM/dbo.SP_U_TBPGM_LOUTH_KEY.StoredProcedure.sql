USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   �ק�Louth Key ���
-- =============================================
CREATE Proc [dbo].[SP_U_TBPGM_LOUTH_KEY]
@FSCHANNEL_ID	varchar(2),
@FSGROUP	nvarchar(10),
@FSNAME	nvarchar(50),
@FSNO	varchar(4),
@FSMEMO	nvarchar(100),
@FSLAYEL	varchar(1),
@FSRULE	varchar(20)

As
begin
  update  dbo.TBPGM_LOUTH_KEY set FSGROUP=@FSGROUP,FSNAME=@FSNAME,FSMEMO=@FSMEMO,FSRULE=@FSRULE
  where FSCHANNEL_ID=@FSCHANNEL_ID and FSNO=@FSNO and FSLAYEL=@FSLAYEL
end




GO
