USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/03/31>
-- Description:	把影片管理系統某個BIB以往比對的相關資訊全移除
-- =============================================
CREATE PROC [dbo].[SP_D_TBTAPE_LOOKUP]
	@BIB	VARCHAR(7)
AS
	-- 移除對照檔
	DELETE FROM TBTAPE_LOOKUP WHERE (FSBIB = @BIB)
		
	-- 移除影帶主檔
	DELETE FROM TBTAPE_LABEL WHERE (BIB = @BIB)

	-- 移除影帶段落檔
	DELETE FROM TBTAPE_LABEL_CLIP WHERE (BIB = @BIB)

	-- 移除影帶資料檔
	DELETE FROM TBTAPE_TAPE WHERE (BIB = @BIB)


GO
