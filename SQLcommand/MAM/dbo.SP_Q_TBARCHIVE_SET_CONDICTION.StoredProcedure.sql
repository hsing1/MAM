USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE Proc [dbo].[SP_Q_TBARCHIVE_SET_CONDICTION]
@FSTYPE char(1), 
@FSID varchar(11),
@FNEPISODE smallint


As

--select 
--a.*
--from TBARCHIVE_SET as a
--where FSID=@FSID and FSTYPE=@FSTYPE and FNEPISODE=@FNEPISODE


if(@FSTYPE='P')
begin
	if((select COUNT(*)	from TBARCHIVE_SET as a	where FSID=@FSID and FSTYPE=@FSTYPE and FNEPISODE=@FNEPISODE)=0)
	begin
		if((select COUNT(*)	from TBARCHIVE_SET as a	where FSID=@FSID and FSTYPE=@FSTYPE and FNEPISODE=19999)=0)
			select * from TBARCHIVE_SET	where FSID='節目預設' and FSTYPE='P' and FNEPISODE=99
		else
			select * from TBARCHIVE_SET	where FSID=@FSID and FSTYPE=@FSTYPE and FNEPISODE=19999
	end
	else
	begin
		select a.* from TBARCHIVE_SET as a where FSID=@FSID and FSTYPE=@FSTYPE and FNEPISODE=@FNEPISODE
	end
end
else
begin
	if((select COUNT(*)	from TBARCHIVE_SET as a	where FSID=@FSID and FSTYPE=@FSTYPE and FNEPISODE=@FNEPISODE)=0)
	begin
		if((select COUNT(*)	from TBARCHIVE_SET as a	where FSID=@FSID and FSTYPE=@FSTYPE and FNEPISODE=19999)=0)
			select * from TBARCHIVE_SET	where FSID='短帶預設' and FSTYPE='G' and FNEPISODE=99
		else
			select a.* from TBARCHIVE_SET as a where FSID=@FSID and FSTYPE=@FSTYPE and FNEPISODE=19999
	end
	else
	begin
		select a.* from TBARCHIVE_SET as a where FSID=@FSID and FSTYPE=@FSTYPE and FNEPISODE=@FNEPISODE
	end
end





GO
