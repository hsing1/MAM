USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Q_FlowidFromFormid]

@FormID VARCHAR(15)
As
if Exists(SELECT * FROM PTSFlow.dbo.Flow3_Activity as a where a.FormId=@FormID )
SELECT * FROM PTSFlow.dbo.Flow3_Activity as a where a.FormId=@FormID

if Exists(SELECT * FROM PTSFlow.dbo.Flow4_Activity as a where a.FormId=@FormID )
SELECT * FROM PTSFlow.dbo.Flow4_Activity as a where a.FormId=@FormID

if Exists(SELECT * FROM PTSFlow.dbo.Flow5_Activity as a where a.FormId=@FormID )
SELECT * FROM PTSFlow.dbo.Flow5_Activity as a where a.FormId=@FormID

if Exists(SELECT * FROM PTSFlow.dbo.Flow6_Activity as a where a.FormId=@FormID )
SELECT * FROM PTSFlow.dbo.Flow6_Activity as a where a.FormId=@FormID

if Exists(SELECT * FROM PTSFlow.dbo.Flow7_Activity as a where a.FormId=@FormID )
SELECT * FROM PTSFlow.dbo.Flow7_Activity as a where a.FormId=@FormID

if Exists(SELECT * FROM PTSFlow.dbo.Flow8_Activity as a where a.FormId=@FormID )
SELECT * FROM PTSFlow.dbo.Flow8_Activity as a where a.FormId=@FormID

if Exists(SELECT * FROM PTSFlow.dbo.Flow9_Activity as a where a.FormId=@FormID )
SELECT * FROM PTSFlow.dbo.Flow9_Activity as a where a.FormId=@FormID

if Exists(SELECT * FROM PTSFlow.dbo.Flow10_Activity as a where a.FormId=@FormID )
SELECT * FROM PTSFlow.dbo.Flow10_Activity as a where a.FormId=@FormID

if Exists(SELECT * FROM PTSFlow.dbo.Flow11_Activity as a where a.FormId=@FormID )
SELECT * FROM PTSFlow.dbo.Flow11_Activity as a where a.FormId=@FormID
--WHERE MAM.dbo.TBZDEPT.FSDEPTID=@FSDEPTID

GO
