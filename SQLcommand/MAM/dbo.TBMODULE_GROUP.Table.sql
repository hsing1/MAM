USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBMODULE_GROUP](
	[FSMOD_GRP_ID] [int] IDENTITY(1,1) NOT NULL,
	[FSMODULE_ID] [char](7) NULL,
	[FSGROUP_ID] [varchar](20) NULL,
 CONSTRAINT [PK_TBMODULE_GROUP] PRIMARY KEY CLUSTERED 
(
	[FSMOD_GRP_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模組群組ＩＤ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMODULE_GROUP', @level2type=N'COLUMN',@level2name=N'FSMOD_GRP_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模組代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMODULE_GROUP', @level2type=N'COLUMN',@level2name=N'FSMODULE_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'群組代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMODULE_GROUP', @level2type=N'COLUMN',@level2name=N'FSGROUP_ID'
GO
