USE [MAM]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[CaculateDuration](@beginTime [nvarchar](11), @endTime [nvarchar](11))
RETURNS [nvarchar](11) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [SqlServerProject1].[StoredProcedures].[StoredProcedure1]
GO
