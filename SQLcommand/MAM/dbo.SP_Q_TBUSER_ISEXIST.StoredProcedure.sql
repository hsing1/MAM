USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jarvis.Yang
-- Create date: 2013-06-26
-- Description:	查詢使用者是存在
-- =============================================
create PROCEDURE [dbo].[SP_Q_TBUSER_ISEXIST]
@FSUSER nvarchar(50)
AS
BEGIN
  select * from TBUSERS where FSUSER=@FSUSER and FSUSER !=''
END




GO
