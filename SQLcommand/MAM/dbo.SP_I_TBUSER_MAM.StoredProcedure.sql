USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_I_TBUSER_MAM]

@FSUSER_ID nvarchar(50),
      @FSUSER nvarchar(50), 
      @FSPASSWD nvarchar(50),
      @FSUSER_ChtName nvarchar(50),
      @FSUSER_TITLE nvarchar(50),
      --@FSDEPT nvarchar(50),
      @FSEMAIL nvarchar(50),
      @FCACTIVE nvarchar(1),
      @FSDESCRIPTION nvarchar(2000),
      @FCSECRET nvarchar(1),
      --@FSCHANNEL_ID nvarchar(10),
      --@FNUpperDept_ID nvarchar(50),
      --@FNDep_ID nvarchar(50),
      --@FNGroup_ID nvarchar(50),
      @FNTreeViewDept nvarchar(50),
      --@FSUpperDept_ChtName nvarchar(50),
      @FNTreeViewDept_ChtName nvarchar(50),
      --@FSDep_ChtName nvarchar(50),
      --@FSGroup_ChtName nvarchar(50),
      --@FSIs_Main_Title nvarchar(2),
      --@FSIs_Supervisor nvarchar(2),
      @FSCREATED_BY nvarchar(50),
      --@FDCREATED_DATE nvarchar(50),
      @FSUPDATED_BY nvarchar(50)
      --@FDUPDATED_DATE nvarchar(50)



As
Insert Into dbo.TBUSERS 
([FSUSER_ID]
      ,[FSUSER]
      ,[FSPASSWD]
      ,[FSUSER_ChtName]
      ,[FSUSER_TITLE]
      ,[FSDEPT]
      ,[FSEMAIL]
      ,[FCACTIVE]
      ,[FSDESCRIPTION]
      ,[FCSECRET]
      ,[FSCHANNEL_ID]
      ,[FNUpperDept_ID]
      ,[FNDep_ID]
      ,[FNGroup_ID]
      ,[FNTreeViewDept]
      ,[FSUpperDept_ChtName]
      ,[FNTreeViewDept_ChtName]
      ,[FSDep_ChtName]
      ,[FSGroup_ChtName]
      ,[FSIs_Main_Title]
      ,[FSIs_Supervisor]
      ,[FSCREATED_BY]
      ,[FDCREATED_DATE]
      ,[FSUPDATED_BY]
      ,[FDUPDATED_DATE])  
VALUES 
(@FSUSER_ID,
      @FSUSER, 
      @FSPASSWD,
      @FSUSER_ChtName,
      @FSUSER_TITLE,
      'MAM內部帳號',--@FSDEPT,
      @FSEMAIL,
      @FCACTIVE,
      @FSDESCRIPTION,
      @FCSECRET,
      '01',--@FSCHANNEL_ID,
      9999,--@FNUpperDept_ID,
      9999,--@FNDep_ID,
      9999,--@FNGroup_ID,
      @FNTreeViewDept,
      'MAM內部帳號',--@FSUpperDept_ChtName,
      @FNTreeViewDept_ChtName,
      'MAM內部帳號',--@FSDep_ChtName,
      'MAM內部帳號',--@FSGroup_ChtName,
      '1',--@FSIs_Main_Title,
      '0',--@FSIs_Supervisor,
      @FSCREATED_BY,
      GETDATE(),--@FDCREATED_DATE,
      @FSUPDATED_BY,
      GETDATE())--@FDUPDATED_DATE)

GO
