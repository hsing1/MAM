USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBPROG_M](
	[FSPROG_ID] [char](7) NOT NULL,
	[FSPGMNAME] [nvarchar](50) NOT NULL,
	[FSPGMENAME] [nvarchar](100) NULL,
	[FSPRDDEPTID] [varchar](2) NULL,
	[FSCHANNEL] [varchar](100) NOT NULL,
	[FNTOTEPISODE] [smallint] NULL,
	[FNLENGTH] [smallint] NULL,
	[FSPROGOBJID] [varchar](2) NOT NULL,
	[FSPROGSRCID] [varchar](2) NOT NULL,
	[FSPROGATTRID] [varchar](2) NOT NULL,
	[FSPROGAUDID] [varchar](2) NOT NULL,
	[FSPROGTYPEID] [varchar](2) NOT NULL,
	[FSPROGGRADEID] [varchar](2) NOT NULL,
	[FSPROGLANGID1] [varchar](2) NOT NULL,
	[FSPROGLANGID2] [varchar](2) NOT NULL,
	[FSPROGSALEID] [varchar](2) NULL,
	[FSPROGBUYID] [varchar](2) NULL,
	[FSPROGBUYDID] [varchar](2) NULL,
	[FSPRDCENID] [varchar](5) NOT NULL,
	[FSPRDYYMM] [varchar](6) NULL,
	[FDSHOWDATE] [datetime] NULL,
	[FSCONTENT] [nvarchar](max) NULL,
	[FSMEMO] [nvarchar](500) NULL,
	[FSEWEBPAGE] [nvarchar](max) NULL,
	[FCDERIVE] [varchar](1) NOT NULL,
	[FSWEBNAME] [nvarchar](50) NULL,
	[FSCHANNEL_ID] [char](2) NULL,
	[FSDEL] [char](1) NULL,
	[FSDELUSER] [varchar](50) NULL,
	[FCOUT_BUY] [char](1) NULL,
	[FNDEP_ID] [int] NULL,
	[FSPROGSPEC] [varchar](100) NULL,
	[FSCRTUSER] [varchar](5) NOT NULL,
	[FDCRTDATE] [datetime] NOT NULL,
	[FSUPDUSER] [varchar](5) NULL,
	[FDUPDDATE] [datetime] NULL,
	[_FDTRANDATE] [datetime] NULL,
	[FSCREATED_BY_PGM] [varchar](50) NULL,
	[FSUPDATED_BY_PGM] [varchar](50) NULL,
	[FDEXPIRE_DATE] [datetime] NULL,
	[FSEXPIRE_DATE_ACTION] [nvarchar](10) NULL,
	[FSPROGNATIONID] [nvarchar](50) NULL,
 CONSTRAINT [PK_TBPROG_M] PRIMARY KEY CLUSTERED 
(
	[FSPROG_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[TBPROG_M] ADD  CONSTRAINT [DF_TBPROG_M_FDEXPIRE_DATE]  DEFAULT ('1900-1-1') FOR [FDEXPIRE_DATE]
GO
ALTER TABLE [dbo].[TBPROG_M] ADD  CONSTRAINT [DF_TBPROG_M_FSEXPIRE_DATE_ACTION]  DEFAULT ('') FOR [FSEXPIRE_DATE_ACTION]
GO
ALTER TABLE [dbo].[TBPROG_M] ADD  CONSTRAINT [DF_TBPROG_M_FSPROGNATIONID]  DEFAULT ('01') FOR [FSPROGNATIONID]
GO
ALTER TABLE [dbo].[TBPROG_M]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROG_M_TBZCHANNEL] FOREIGN KEY([FSCHANNEL_ID])
REFERENCES [dbo].[TBZCHANNEL] ([FSCHANNEL_ID])
GO
ALTER TABLE [dbo].[TBPROG_M] CHECK CONSTRAINT [FK_TBPROG_M_TBZCHANNEL]
GO
ALTER TABLE [dbo].[TBPROG_M]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROG_M_TBZDEPT] FOREIGN KEY([FSPRDDEPTID])
REFERENCES [dbo].[TBZDEPT] ([FSDEPTID])
GO
ALTER TABLE [dbo].[TBPROG_M] CHECK CONSTRAINT [FK_TBPROG_M_TBZDEPT]
GO
ALTER TABLE [dbo].[TBPROG_M]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROG_M_TBZPROGATTR] FOREIGN KEY([FSPROGATTRID])
REFERENCES [dbo].[TBZPROGATTR] ([FSPROGATTRID])
GO
ALTER TABLE [dbo].[TBPROG_M] CHECK CONSTRAINT [FK_TBPROG_M_TBZPROGATTR]
GO
ALTER TABLE [dbo].[TBPROG_M]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROG_M_TBZPROGAUD] FOREIGN KEY([FSPROGAUDID])
REFERENCES [dbo].[TBZPROGAUD] ([FSPROGAUDID])
GO
ALTER TABLE [dbo].[TBPROG_M] CHECK CONSTRAINT [FK_TBPROG_M_TBZPROGAUD]
GO
ALTER TABLE [dbo].[TBPROG_M]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROG_M_TBZPROGBUY] FOREIGN KEY([FSPROGBUYID])
REFERENCES [dbo].[TBZPROGBUY] ([FSPROGBUYID])
GO
ALTER TABLE [dbo].[TBPROG_M] CHECK CONSTRAINT [FK_TBPROG_M_TBZPROGBUY]
GO
ALTER TABLE [dbo].[TBPROG_M]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROG_M_TBZPROGGRADE] FOREIGN KEY([FSPROGGRADEID])
REFERENCES [dbo].[TBZPROGGRADE] ([FSPROGGRADEID])
GO
ALTER TABLE [dbo].[TBPROG_M] CHECK CONSTRAINT [FK_TBPROG_M_TBZPROGGRADE]
GO
ALTER TABLE [dbo].[TBPROG_M]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROG_M_TBZPROGLANG] FOREIGN KEY([FSPROGLANGID1])
REFERENCES [dbo].[TBZPROGLANG] ([FSPROGLANGID])
GO
ALTER TABLE [dbo].[TBPROG_M] CHECK CONSTRAINT [FK_TBPROG_M_TBZPROGLANG]
GO
ALTER TABLE [dbo].[TBPROG_M]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROG_M_TBZPROGLANG1] FOREIGN KEY([FSPROGLANGID2])
REFERENCES [dbo].[TBZPROGLANG] ([FSPROGLANGID])
GO
ALTER TABLE [dbo].[TBPROG_M] CHECK CONSTRAINT [FK_TBPROG_M_TBZPROGLANG1]
GO
ALTER TABLE [dbo].[TBPROG_M]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROG_M_TBZPROGOBJ] FOREIGN KEY([FSPROGOBJID])
REFERENCES [dbo].[TBZPROGOBJ] ([FSPROGOBJID])
GO
ALTER TABLE [dbo].[TBPROG_M] CHECK CONSTRAINT [FK_TBPROG_M_TBZPROGOBJ]
GO
ALTER TABLE [dbo].[TBPROG_M]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROG_M_TBZPROGSRC] FOREIGN KEY([FSPROGSRCID])
REFERENCES [dbo].[TBZPROGSRC] ([FSPROGSRCID])
GO
ALTER TABLE [dbo].[TBPROG_M] CHECK CONSTRAINT [FK_TBPROG_M_TBZPROGSRC]
GO
ALTER TABLE [dbo].[TBPROG_M]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROG_M_TBZPROGTYPE] FOREIGN KEY([FSPROGTYPEID])
REFERENCES [dbo].[TBZPROGTYPE] ([FSPROGTYPEID])
GO
ALTER TABLE [dbo].[TBPROG_M] CHECK CONSTRAINT [FK_TBPROG_M_TBZPROGTYPE]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSPROG_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSPGMNAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外文名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSPGMENAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'製作部門代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSPRDDEPTID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FSCHANNEL' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSCHANNEL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'總集數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FNTOTEPISODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'每集時長' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FNLENGTH'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'製作目的代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSPROGOBJID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目來源代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSPROGSRCID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'內容屬性代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSPROGATTRID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'目標觀眾代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSPROGAUDID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'表現方式代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSPROGTYPEID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目分級代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSPROGGRADEID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主聲道代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSPROGLANGID1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'副聲道代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSPROGLANGID2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'行銷類別代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSPROGSALEID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外購類別代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSPROGBUYID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外購類別細項代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSPROGBUYDID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'製作單位代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSPRDCENID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'製作年月' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSPRDYYMM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'預計上檔日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FDSHOWDATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'內容' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSCONTENT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'備註' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSMEMO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'英文網頁資料' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSEWEBPAGE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'衍生性節目註記' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FCDERIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'網路播出名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSWEBNAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'頻道別代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSCHANNEL_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'未知欄位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSDEL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'未知欄位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSDELUSER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外購節目' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FCOUT_BUY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'所屬部門ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FNDEP_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目規格' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSPROGSPEC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSCRTUSER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FDCRTDATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSUPDUSER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FDUPDDATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'未知欄位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'_FDTRANDATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'未知欄位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY_PGM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'未知欄位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY_PGM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'到期日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FDEXPIRE_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'到期後是否刪除' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSEXPIRE_DATE_ACTION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'國家代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_M', @level2type=N'COLUMN',@level2name=N'FSPROGNATIONID'
GO
