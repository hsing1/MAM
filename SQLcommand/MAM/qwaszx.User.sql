USE [MAM]
GO
CREATE USER [qwaszx] FOR LOGIN [qwaszx] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_datareader] ADD MEMBER [qwaszx]
GO
