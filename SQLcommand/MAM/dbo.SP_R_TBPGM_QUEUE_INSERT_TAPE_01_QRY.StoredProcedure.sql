USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   查詢節目表播出提示資料報表
-- =============================================

CREATE PROCEDURE [dbo].[SP_R_TBPGM_QUEUE_INSERT_TAPE_01_QRY]
	@QUERY_KEY	VARCHAR(36)
AS
BEGIN
	SELECT
		[QUERY_KEY]
      ,[QUERY_KEY_SUBID]
      ,[QUERY_BY]
      ,[QUERY_DATE]
      ,[播出日期]
      ,[頻道]
      ,[節目名稱]
      ,[集數]
      ,[主控插播帶]
      ,[主控鏡面]

	FROM
		RPT_TBPGM_QUEUE_INSERT_TAPE_01
	WHERE
		(QUERY_KEY = @QUERY_KEY)
	ORDER BY
		[播出日期]
END
GO
