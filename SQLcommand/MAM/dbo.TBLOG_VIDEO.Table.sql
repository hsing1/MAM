USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBLOG_VIDEO](
	[FSFILE_NO] [char](16) NOT NULL,
	[FSSUBJECT_ID] [char](12) NOT NULL,
	[FSTYPE] [char](1) NOT NULL,
	[FSID] [varchar](11) NOT NULL,
	[FNEPISODE] [smallint] NOT NULL,
	[FSSUPERVISOR] [char](1) NULL,
	[FSPLAY] [char](1) NOT NULL,
	[FSARC_TYPE] [varchar](3) NULL,
	[FSFILE_TYPE_HV] [varchar](5) NULL,
	[FSFILE_TYPE_LV] [varchar](5) NULL,
	[FSTITLE] [nvarchar](100) NULL,
	[FSDESCRIPTION] [nvarchar](200) NULL,
	[FSFILE_SIZE] [nvarchar](50) NULL,
	[FCFILE_STATUS] [char](1) NULL,
	[FSCHANGE_FILE_NO] [varchar](20) NULL,
	[FSOLD_FILE_NAME] [nvarchar](100) NULL,
	[FSFILE_PATH_H] [nvarchar](100) NULL,
	[FSFILE_PATH_L] [nvarchar](100) NULL,
	[FNDIR_ID] [bigint] NULL,
	[FSCHANNEL_ID] [char](2) NULL,
	[FSBEG_TIMECODE] [char](11) NULL,
	[FSEND_TIMECODE] [char](11) NULL,
	[FSBRO_ID] [char](12) NULL,
	[FSARC_ID] [char](12) NULL,
	[FDPLAY_DATE] [datetime] NULL,
	[FSJOB_ID] [bigint] NULL,
	[FCFROM] [char](1) NULL,
	[FSVIDEO_PROG] [char](8) NULL,
	[FSTAPE_ID] [varchar](50) NULL,
	[FSTRANS_FROM] [varchar](100) NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
	[FSUPDATED_BY] [varchar](50) NULL,
	[FDUPDATED_DATE] [datetime] NULL,
	[FSVIDEO_PROPERTY] [varchar](max) NULL,
	[FCLOW_RES] [char](1) NULL,
	[FCKEYFRAME] [char](1) NULL,
	[FNENC_CNT] [int] NULL,
	[FSENC_MSG] [nvarchar](max) NULL,
	[FDENC_DATE] [datetime] NULL,
	[FCINGEST_QC] [char](1) NULL,
	[FSINGEST_QC_MSG] [nvarchar](100) NULL,
	[FSTRACK] [varchar](8) NULL,
 CONSTRAINT [ TBLOG_VIDEO] PRIMARY KEY CLUSTERED 
(
	[FSFILE_NO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
CREATE NONCLUSTERED INDEX [INDEX_FSID_FSEPISODE_FSARCTYPE] ON [dbo].[TBLOG_VIDEO]
(
	[FSID] ASC,
	[FNEPISODE] ASC,
	[FSARC_TYPE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
CREATE NONCLUSTERED INDEX [INDEX_FSTYPE_FSID_FSEPISODE] ON [dbo].[TBLOG_VIDEO]
(
	[FSTYPE] ASC,
	[FSID] ASC,
	[FNEPISODE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
CREATE NONCLUSTERED INDEX [IX_TBLOG_VIDEO_BROID] ON [dbo].[TBLOG_VIDEO]
(
	[FSBRO_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TBLOG_VIDEO_JOBID] ON [dbo].[TBLOG_VIDEO]
(
	[FSJOB_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TBLOG_VIDEO] ADD  CONSTRAINT [DF_TBLOG_VIDEO_FSSUPERVISOR]  DEFAULT ('N') FOR [FSSUPERVISOR]
GO
ALTER TABLE [dbo].[TBLOG_VIDEO] ADD  CONSTRAINT [DF_TBLOG_VIDEO_FSPLAY]  DEFAULT ('N') FOR [FSPLAY]
GO
ALTER TABLE [dbo].[TBLOG_VIDEO] ADD  CONSTRAINT [DF_TBLOG_VIDEO_FSARC_TYPE]  DEFAULT ('') FOR [FSARC_TYPE]
GO
ALTER TABLE [dbo].[TBLOG_VIDEO] ADD  CONSTRAINT [DF_TBLOG_VIDEO_FSTITLE]  DEFAULT ('') FOR [FSTITLE]
GO
ALTER TABLE [dbo].[TBLOG_VIDEO] ADD  CONSTRAINT [DF_TBLOG_VIDEO_FSDESCRIPTION]  DEFAULT ('') FOR [FSDESCRIPTION]
GO
ALTER TABLE [dbo].[TBLOG_VIDEO] ADD  CONSTRAINT [DF_TBLOG_VIDEO_FSOLD_FILE_NAME]  DEFAULT ('') FOR [FSOLD_FILE_NAME]
GO
ALTER TABLE [dbo].[TBLOG_VIDEO] ADD  CONSTRAINT [DF_TBLOG_VIDEO_FSFILE_PATH_H]  DEFAULT ('') FOR [FSFILE_PATH_H]
GO
ALTER TABLE [dbo].[TBLOG_VIDEO] ADD  CONSTRAINT [DF_TBLOG_VIDEO_FSFILE_PATH_L]  DEFAULT ('') FOR [FSFILE_PATH_L]
GO
ALTER TABLE [dbo].[TBLOG_VIDEO] ADD  CONSTRAINT [DF_TBLOG_VIDEO_FSCHANNEL_ID]  DEFAULT ('') FOR [FSCHANNEL_ID]
GO
ALTER TABLE [dbo].[TBLOG_VIDEO] ADD  CONSTRAINT [DF_TBLOG_VIDEO_FSBEG_TIMECODE]  DEFAULT ('') FOR [FSBEG_TIMECODE]
GO
ALTER TABLE [dbo].[TBLOG_VIDEO] ADD  CONSTRAINT [DF_TBLOG_VIDEO_FSEND_TIMECODE]  DEFAULT ('') FOR [FSEND_TIMECODE]
GO
ALTER TABLE [dbo].[TBLOG_VIDEO] ADD  CONSTRAINT [DF_TBLOG_VIDEO_FSBRO_ID]  DEFAULT ('') FOR [FSBRO_ID]
GO
ALTER TABLE [dbo].[TBLOG_VIDEO] ADD  CONSTRAINT [DF_TBLOG_VIDEO_FSARC_ID]  DEFAULT ('') FOR [FSARC_ID]
GO
ALTER TABLE [dbo].[TBLOG_VIDEO] ADD  CONSTRAINT [DF_TBLOG_VIDEO_FSTAPE_ID]  DEFAULT ('') FOR [FSTAPE_ID]
GO
ALTER TABLE [dbo].[TBLOG_VIDEO] ADD  CONSTRAINT [DF_TBLOG_VIDEO_FSTRANS_FROM]  DEFAULT ('') FOR [FSTRANS_FROM]
GO
ALTER TABLE [dbo].[TBLOG_VIDEO] ADD  CONSTRAINT [DF_TBLOG_VIDEO_FSUPDATED_BY]  DEFAULT ('') FOR [FSUPDATED_BY]
GO
ALTER TABLE [dbo].[TBLOG_VIDEO] ADD  CONSTRAINT [DF_TBLOG_VIDEO_FDUPDATED_DATE]  DEFAULT (getdate()) FOR [FDUPDATED_DATE]
GO
ALTER TABLE [dbo].[TBLOG_VIDEO]  WITH CHECK ADD  CONSTRAINT [FK_TBLOG_VIDEO_TBLOG_VIDEO] FOREIGN KEY([FSARC_TYPE])
REFERENCES [dbo].[TBFILE_TYPE] ([FSARC_TYPE])
GO
ALTER TABLE [dbo].[TBLOG_VIDEO] CHECK CONSTRAINT [FK_TBLOG_VIDEO_TBLOG_VIDEO]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'檔案編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO', @level2type=N'COLUMN',@level2name=N'FSFILE_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主題代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO', @level2type=N'COLUMN',@level2name=N'FSSUBJECT_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'資料類型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO', @level2type=N'COLUMN',@level2name=N'FSTYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'編碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO', @level2type=N'COLUMN',@level2name=N'FSID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'集別' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO', @level2type=N'COLUMN',@level2name=N'FNEPISODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'調用通知主管' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO', @level2type=N'COLUMN',@level2name=N'FSSUPERVISOR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'調用通知主管' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO', @level2type=N'COLUMN',@level2name=N'FSPLAY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主控播出' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO', @level2type=N'COLUMN',@level2name=N'FSARC_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'低解檔案類型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO', @level2type=N'COLUMN',@level2name=N'FSFILE_TYPE_HV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'高解檔案類型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO', @level2type=N'COLUMN',@level2name=N'FSFILE_TYPE_LV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'標題' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO', @level2type=N'COLUMN',@level2name=N'FSTITLE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO', @level2type=N'COLUMN',@level2name=N'FSDESCRIPTION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'檔案大小' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO', @level2type=N'COLUMN',@level2name=N'FSFILE_SIZE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'檔案狀態' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO', @level2type=N'COLUMN',@level2name=N'FCFILE_STATUS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'置換檔案編碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO', @level2type=N'COLUMN',@level2name=N'FSCHANGE_FILE_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'原始檔名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO', @level2type=N'COLUMN',@level2name=N'FSOLD_FILE_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TSM路徑_高解' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO', @level2type=N'COLUMN',@level2name=N'FSFILE_PATH_H'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TSM路徑_低解' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO', @level2type=N'COLUMN',@level2name=N'FSFILE_PATH_L'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'所屬Queue代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO', @level2type=N'COLUMN',@level2name=N'FNDIR_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'頻道別代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO', @level2type=N'COLUMN',@level2name=N'FSCHANNEL_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time Code起' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO', @level2type=N'COLUMN',@level2name=N'FSBEG_TIMECODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time Code迄' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO', @level2type=N'COLUMN',@level2name=N'FSEND_TIMECODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送帶轉檔單號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO', @level2type=N'COLUMN',@level2name=N'FSBRO_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'入庫單號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO', @level2type=N'COLUMN',@level2name=N'FSARC_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'播出日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO', @level2type=N'COLUMN',@level2name=N'FDPLAY_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'轉檔進度代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO', @level2type=N'COLUMN',@level2name=N'FSJOB_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'新增者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'新增日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
