USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		冠宇
-- Create date: 2012/12/17
-- Description:	查詢國家來源代碼表的資料
-- =============================================
create PROCEDURE [dbo].[SP_Q_TBZPROGNATION] 

AS
BEGIN
	select * from TBZPROGNATION where FSNATION_ID<>''
END

GO
