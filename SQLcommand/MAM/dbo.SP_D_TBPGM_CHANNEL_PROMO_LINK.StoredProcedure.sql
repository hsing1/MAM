USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/24>
-- Description:   刪除頻道短帶預排資料
-- =============================================
CREATE Proc [dbo].[SP_D_TBPGM_CHANNEL_PROMO_LINK]

@FSCHANNEL_ID	varchar(2),
@FCLINK_TYPE	char(1),
@FCTIME_TYPE	char(1),
@FCINSERT_TYPE	char(1)
	
As
delete from  dbo.TBPGM_CHANNEL_PROMO_LINK
where FSCHANNEL_ID=@FSCHANNEL_ID and FCLINK_TYPE=@FCLINK_TYPE 
and FCTIME_TYPE=@FCTIME_TYPE and FCINSERT_TYPE=@FCINSERT_TYPE







GO
