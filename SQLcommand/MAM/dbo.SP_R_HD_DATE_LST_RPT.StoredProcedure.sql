USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[SP_R_HD_DATE_LST_RPT]
	@QUERY_KEY	VARCHAR(36)
AS
BEGIN
	SELECT [QUERY_KEY]
		  ,[QUERY_KEY_SUBID]
		  ,[QUERY_BY]
		  ,[QUERY_DATE]
      ,[頻道]
      ,[播出日期]
      ,[序號]
      ,[節目編碼]
      ,[時間類型]
      ,[名稱]
      ,[節目集數]
      ,[段數]
      ,[支數]
      ,[主控編號]
      ,[播出時間]
      ,[長度]
      ,[播映序號]
      ,[短帶編碼]
      ,[影像編號]
      ,[資料類型]
      ,[備註]
       ,[FSSIGNAL]
        ,[FSRAN]
,[FNLIVE]
,[FSSOM]
	FROM
		[MAM].[dbo].[RPT_HD_DATE_LST]
	WHERE
		(QUERY_KEY = @QUERY_KEY)
	ORDER BY
		[播出日期], [頻道], CAST([序號] AS SMALLINT)
END

GO
