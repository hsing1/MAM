USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  function [dbo].[F_test](@nid int)
returns nvarchar(1000)
as
begin
    declare @s nvarchar(1000),@npid int
    select @s=rtrim(ccategory),@npid=npid from isc_catalog where nid=@nid
    if @s is null
        return null
    
    return isnull(dbo.F_test(@npid)+',','')+@s --��,���j
    --@s+isnull('.'+dbo.F_test(@npid),'')
end

GO
