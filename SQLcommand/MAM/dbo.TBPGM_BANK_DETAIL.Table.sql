USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBPGM_BANK_DETAIL](
	[FSPROG_ID] [char](7) NOT NULL,
	[FNSEQNO] [int] NOT NULL,
	[FSPROG_NAME] [nvarchar](50) NOT NULL,
	[FSPROG_NAME_ENG] [nvarchar](100) NULL,
	[FDBEG_DATE] [date] NOT NULL,
	[FDEND_DATE] [date] NOT NULL,
	[FSBEG_TIME] [char](4) NOT NULL,
	[FSEND_TIME] [char](4) NOT NULL,
	[FSCHANNEL_ID] [varchar](2) NULL,
	[FNREPLAY] [int] NULL,
	[FSWEEK] [char](7) NULL,
	[FSPLAY_TYPE] [char](1) NULL,
	[FNSEG] [int] NULL,
	[FNDUR] [int] NULL,
	[FNSEC] [int] NULL,
	[FSSIGNAL] [nvarchar](50) NULL,
	[FSMEMO] [nvarchar](500) NULL,
	[FSMEMO1] [nvarchar](500) NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
	[FSUPDATED_BY] [varchar](50) NULL,
	[FDUPDATED_DATE] [datetime] NULL,
 CONSTRAINT [ TBPGM_BANK_DETAIL] PRIMARY KEY CLUSTERED 
(
	[FSPROG_ID] ASC,
	[FNSEQNO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目編碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_BANK_DETAIL', @level2type=N'COLUMN',@level2name=N'FSPROG_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'播映序號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_BANK_DETAIL', @level2type=N'COLUMN',@level2name=N'FNSEQNO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_BANK_DETAIL', @level2type=N'COLUMN',@level2name=N'FSPROG_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目英文名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_BANK_DETAIL', @level2type=N'COLUMN',@level2name=N'FSPROG_NAME_ENG'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'起始日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_BANK_DETAIL', @level2type=N'COLUMN',@level2name=N'FDBEG_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'結束日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_BANK_DETAIL', @level2type=N'COLUMN',@level2name=N'FDEND_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'起始時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_BANK_DETAIL', @level2type=N'COLUMN',@level2name=N'FSBEG_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'結束時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_BANK_DETAIL', @level2type=N'COLUMN',@level2name=N'FSEND_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'頻道編碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_BANK_DETAIL', @level2type=N'COLUMN',@level2name=N'FSCHANNEL_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'重播次數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_BANK_DETAIL', @level2type=N'COLUMN',@level2name=N'FNREPLAY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'星期編碼(1100000 代表星期一二可播)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_BANK_DETAIL', @level2type=N'COLUMN',@level2name=N'FSWEEK'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'播出方式(1表示Live)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_BANK_DETAIL', @level2type=N'COLUMN',@level2name=N'FSPLAY_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'預設段數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_BANK_DETAIL', @level2type=N'COLUMN',@level2name=N'FNSEG'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'預設長度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_BANK_DETAIL', @level2type=N'COLUMN',@level2name=N'FNDUR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'備註(2nd Event)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_BANK_DETAIL', @level2type=N'COLUMN',@level2name=N'FSMEMO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'備註' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_BANK_DETAIL', @level2type=N'COLUMN',@level2name=N'FSMEMO1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_BANK_DETAIL', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_BANK_DETAIL', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_BANK_DETAIL', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_BANK_DETAIL', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'描述', @value=N'節目播映資料表(紀錄節目的播出定義資料)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_BANK_DETAIL'
GO
