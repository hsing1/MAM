USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBPROG_D_INSERT_TAPE](
	[FSPROG_ID] [char](7) NOT NULL,
	[FNEPISODE] [int] NOT NULL,
	[FNNO] [int] NOT NULL,
	[FSPROMO_ID] [varchar](11) NOT NULL,
	[FSINSERT_CHANNEL_ID] [varchar](2) NOT NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
 CONSTRAINT [PK_TBPROG_D_INSERT_TAPE] PRIMARY KEY CLUSTERED 
(
	[FSPROG_ID] ASC,
	[FNEPISODE] ASC,
	[FNNO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
