USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2015/07/02>
-- Description:   查詢建議刪除清單
-- 查詢主控檔案清單中的資料不存在未來幾天內的播出運行表上的資料
-- 濾掉短帶中包含RM的資料
-- =============================================
CREATE Proc [dbo].[SP_Q_PROPOSE_DELETE_LIST]
@CHECKDATE	int,
@CHANNELTYPE	varchar(3)
As
IF @CHANNELTYPE='001'
select 
distinct A.FSVIDEO_ID AS FSVIDEOID ,D.FSPGMNAME AS PROGNAME,C.FNEPISODE,B.FNSEG_ID
from 
TBLOG_MASTERCONTROL_FTP A
left join TBLOG_VIDEO_SEG B on A.FSVIDEO_ID=B.FSVIDEO_ID 
left join TBLOG_VIDEO C on B.FSFILE_NO=C.FSFILE_NO 
join TBPROG_M D on C.FSID=D.FSPROG_ID
where CAST(A.FSTIME AS datetime)  >cast(convert(char(11), getdate(), 111) as datetime)-1
and A.FSCHANNEL_ID<>'11' and A.FSCHANNEL_ID<>'49'
and A.FSVIDEO_ID not in(
select distinct D.FSVIDEO_ID
from 
tbpgm_combine_queue A
left join TBZCHANNEL B on A.fschannel_ID=B.FSCHANNEL_ID
left join tblog_video C on A.fsprog_id=C.FSID and A.FNEPISODE=C.FNEPISODE and B.FSCHANNEL_TYPE=C.FSARC_TYPE
left join TBlog_video_SEG D on C.FSFILE_NO=D.FSFILE_NO and A.FNBREAK_NO=D.FNSEG_ID
where A.FDDATE >= cast(convert(char(11), getdate(), 111) as datetime)-1 and A.FDDATE<= getdate()+2  and D.FSVIDEO_ID is not null
)
union
select 
distinct A.FSVIDEO_ID AS FSVIDEOID  ,D.FSPROMO_NAME AS PROGNAME,'0','0'
from 
TBLOG_MASTERCONTROL_FTP A
left join TBLOG_VIDEO_SEG B on A.FSVIDEO_ID=B.FSVIDEO_ID 
left join TBLOG_VIDEO C on B.FSFILE_NO=C.FSFILE_NO 
join TBPGM_PROMO D on C.FSID=D.FSPROMO_ID and C.FNEPISODE=0
where CAST(A.FSTIME AS datetime) >cast(convert(char(11), getdate(), 111) as datetime)-1  
and A.FSCHANNEL_ID<>'11' and A.FSCHANNEL_ID<>'49'
and FSPROMO_NAME NOT LIKE '%RM%'
and A.FSVIDEO_ID not in(
select distinct D.FSVIDEO_ID
from 
TBPGM_ARR_PROMO A
left join TBZCHANNEL B on A.fschannel_ID=B.FSCHANNEL_ID
left join tblog_video C on A.FSPROMO_ID=C.FSID and B.FSCHANNEL_TYPE=C.FSARC_TYPE
left join TBlog_video_SEG D on C.FSFILE_NO=D.FSFILE_NO 
where A.FDDATE >= cast(convert(char(11), getdate(), 111) as datetime)-1 and A.FDDATE< getdate()+2  and D.FSVIDEO_ID is not null)
else IF @CHANNELTYPE='49'
begin
select 
distinct A.FSVIDEO_ID AS FSVIDEOID ,D.FSPGMNAME AS PROGNAME,C.FNEPISODE,B.FNSEG_ID
from 
TBLOG_MASTERCONTROL_FTP A
left join TBLOG_VIDEO_SEG B on '00'+A.FSVIDEO_ID+'01'=B.FSVIDEO_ID
left join TBLOG_VIDEO C on B.FSFILE_NO=C.FSFILE_NO 
join TBPROG_M D on C.FSID=D.FSPROG_ID
where CAST(A.FSTIME AS datetime)  >cast(convert(char(11), getdate(), 111) as datetime)-1
and A.FSCHANNEL_ID='49'
and '00'+A.FSVIDEO_ID+'01' not in(
select distinct D.FSVIDEO_ID
from 
tbpgm_combine_queue A
left join TBZCHANNEL B on A.fschannel_ID=B.FSCHANNEL_ID
left join tblog_video C on A.fsprog_id=C.FSID and A.FNEPISODE=C.FNEPISODE and B.FSCHANNEL_TYPE=C.FSARC_TYPE
left join TBlog_video_SEG D on C.FSFILE_NO=D.FSFILE_NO and A.FNBREAK_NO=D.FNSEG_ID
where A.FDDATE >= cast(convert(char(11), getdate(), 111) as datetime)-1 and A.FDDATE<= getdate()+2  and D.FSVIDEO_ID is not null
)
union
select 
distinct A.FSVIDEO_ID AS FSVIDEOID  ,D.FSPROMO_NAME AS PROGNAME,'0','0'
from 
TBLOG_MASTERCONTROL_FTP A
left join TBLOG_VIDEO_SEG B on '00'+A.FSVIDEO_ID=B.FSVIDEO_ID 
left join TBLOG_VIDEO C on B.FSFILE_NO=C.FSFILE_NO 
join TBPGM_PROMO D on C.FSID=D.FSPROMO_ID and C.FNEPISODE=0
where CAST(A.FSTIME AS datetime) >cast(convert(char(11), getdate(), 111) as datetime)-1  
and A.FSCHANNEL_ID='49'
and FSPROMO_NAME NOT LIKE '%RM%'
and '00'+A.FSVIDEO_ID not in(
select distinct D.FSVIDEO_ID
from 
TBPGM_ARR_PROMO A
left join TBZCHANNEL B on A.fschannel_ID=B.FSCHANNEL_ID
left join tblog_video C on A.FSPROMO_ID=C.FSID and B.FSCHANNEL_TYPE=C.FSARC_TYPE
left join TBlog_video_SEG D on C.FSFILE_NO=D.FSFILE_NO 
where A.FDDATE >= cast(convert(char(11), getdate(), 111) as datetime)-1 and A.FDDATE< getdate()+2  and D.FSVIDEO_ID is not null)
end
else 
begin
select 
distinct A.FSVIDEO_ID AS FSVIDEOID ,D.FSPGMNAME AS PROGNAME,C.FNEPISODE,B.FNSEG_ID
from 
TBLOG_MASTERCONTROL_FTP A
left join TBLOG_VIDEO_SEG B on '00'+A.FSVIDEO_ID+'01'=B.FSVIDEO_ID
left join TBLOG_VIDEO C on B.FSFILE_NO=C.FSFILE_NO 
join TBPROG_M D on C.FSID=D.FSPROG_ID
where CAST(A.FSTIME AS datetime)  >cast(convert(char(11), getdate(), 111) as datetime)-1
and A.FSCHANNEL_ID='11'
and '00'+A.FSVIDEO_ID+'01' not in(
select distinct D.FSVIDEO_ID
from 
tbpgm_combine_queue A
left join TBZCHANNEL B on A.fschannel_ID=B.FSCHANNEL_ID
left join tblog_video C on A.fsprog_id=C.FSID and A.FNEPISODE=C.FNEPISODE and B.FSCHANNEL_TYPE=C.FSARC_TYPE
left join TBlog_video_SEG D on C.FSFILE_NO=D.FSFILE_NO and A.FNBREAK_NO=D.FNSEG_ID
where A.FDDATE >= cast(convert(char(11), getdate(), 111) as datetime)-1 and A.FDDATE<= getdate()+2  and D.FSVIDEO_ID is not null
)
union
select 
distinct A.FSVIDEO_ID AS FSVIDEOID  ,D.FSPROMO_NAME AS PROGNAME,'0','0'
from 
TBLOG_MASTERCONTROL_FTP A
left join TBLOG_VIDEO_SEG B on '00'+A.FSVIDEO_ID=B.FSVIDEO_ID 
left join TBLOG_VIDEO C on B.FSFILE_NO=C.FSFILE_NO 
join TBPGM_PROMO D on C.FSID=D.FSPROMO_ID and C.FNEPISODE=0
where CAST(A.FSTIME AS datetime) >cast(convert(char(11), getdate(), 111) as datetime)-1  
and A.FSCHANNEL_ID='11'
and FSPROMO_NAME NOT LIKE '%RM%'
and '00'+A.FSVIDEO_ID not in(
select distinct D.FSVIDEO_ID
from 
TBPGM_ARR_PROMO A
left join TBZCHANNEL B on A.fschannel_ID=B.FSCHANNEL_ID
left join tblog_video C on A.FSPROMO_ID=C.FSID and B.FSCHANNEL_TYPE=C.FSARC_TYPE
left join TBlog_video_SEG D on C.FSFILE_NO=D.FSFILE_NO 
where A.FDDATE >= cast(convert(char(11), getdate(), 111) as datetime)-1 and A.FDDATE< getdate()+2  and D.FSVIDEO_ID is not null)
end
GO
