USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/06/02>
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[SP_I_TBZCODE_SET]
	@FSCODE_ID			varchar(10),
	@FSCODE_TITLE		varchar(50),
	@FSCODE_NOTE		varchar(200),
	@FSCODE_ISENABLED	bit,
	@FSCREATED_BY		varchar(50)
AS
BEGIN
	IF EXISTS(SELECT * FROM [TBZCODE_SET] WHERE [FSCODE_ID] = @FSCODE_ID)
		BEGIN
			SELECT RESULT = 'ERROR:已存在此代碼編號, 請重新設定'
		END
	ELSE
		BEGIN
			INSERT [TBZCODE_SET]
				([FSCODE_ID],[FSCODE_TITLE],[FSCODE_TBCOL],[FSCODE_NOTE],[FSCODE_ISENABLED],
				[FSCREATED_BY],[FDCREATED_DATE],[FSUPDATED_BY],[FDUPDATED_DATE])
			VALUES
				(@FSCODE_ID, @FSCODE_TITLE, '', @FSCODE_NOTE, @FSCODE_ISENABLED,
				@FSCREATED_BY, GETDATE(), '', '1900/01/01') 
				
			IF (@@ROWCOUNT = 0)	
				BEGIN
					SELECT RESULT = 'ERROR:沒有受影響的資料列, 新增失敗'
				END	
			ELSE
				BEGIN
					SELECT RESULT = ''
				END		
		END
END


GO
