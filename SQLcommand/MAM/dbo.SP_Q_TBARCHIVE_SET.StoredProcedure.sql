USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE Proc [dbo].[SP_Q_TBARCHIVE_SET]
--@FSTYPE char(1), 
--@FSID varchar(11),
--@FNEPISODE smallint


As

select 

a.*,
--ISNULL(b.FSPGMNAME,'') as FSPGMNAME, 
--ISNULL(c.FSPGDENAME,'') as FSPGDNAME

FSPGMNAME		= CASE a.FSTYPE WHEN 'G' THEN ISNULL(b.FSPGMNAME,'') WHEN 'P' THEN PROMO.FSPROMO_NAME  ELSE '' END,
FSPGDNAME = CASE a.FSTYPE WHEN 'G' THEN ISNULL(c.FSPGDNAME,'') WHEN 'P' THEN '' ELSE '' END
from TBARCHIVE_SET as a
left outer join TBPROG_M as b on a.FSID=b.FSPROG_ID
left outer join TBPROG_D as c on a.FSID=c.FSPROG_ID and a.FNEPISODE=c.FNEPISODE

LEFT JOIN TBPGM_PROMO AS PROMO ON (a.FSTYPE = 'P') AND (PROMO.FSPROMO_ID = a.FSID)	




GO
