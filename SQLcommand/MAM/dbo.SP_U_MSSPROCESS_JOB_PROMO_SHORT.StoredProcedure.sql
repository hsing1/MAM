USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/03/21>
-- Description:	主要來源TBPGM_ARR_PROMO資料表
--				<2011/05/02> 依維智說明修改取法
-- =============================================
CREATE PROCEDURE [dbo].[SP_U_MSSPROCESS_JOB_PROMO_SHORT]
	@DISPLAY_DAY	SMALLINT
AS
BEGIN
	/* ===== 以下, 宣告本預存內使用之相關參數, 並給予值 ===== */

		DECLARE @FSTYPE_ID NVARCHAR(2) = '08'
		DECLARE	@SDate DATE, @EDate DATE, @NEW_ID BIGINT
		DECLARE	@FSNO_DATE CHAR(8)						-- Ex: 20130815
		DECLARE @TODAY_PROCESS_ID BIGINT				-- Ex: 201308150000
		DECLARE @TEMP_TABLE TABLE(	[FNMSSPROCESS_ID]	bigint,
									[FSVIDEOID]			varchar(10),
									[FNSTATUS]			smallint,
									[FNTRANSCODE_ID]	bigint,
									[FNTSMJOB_ID]		bigint,
									[FNMSSCOPY_ID]		bigint,
									[FNFORCEDIRECT]		smallint,
									[FDCREATE_TIME]		datetime,
									[FDUPDATE_TIME]		datetime	)
		
		SELECT	@SDate		= GETDATE(),
				@EDate		= DATEADD(day, @DISPLAY_DAY - 1, GETDATE()),	-- 若是取系統日起n天(共n+1天), 請移除 - 1
				@FSNO_DATE	= CONVERT(VARCHAR(8), GETDATE(), 112),			-- 取單號的判斷字串
				@NEW_ID		= 0 
		SET @TODAY_PROCESS_ID = @FSNO_DATE + '0000'
		
		/* ===== 以下, 先把可能要的東西從 TBPGM_ARR_PROMO 取出來 ===== */
		SET NOCOUNT ON;
				
		INSERT
			@TEMP_TABLE
		SELECT DISTINCT
			0, LOG_VS.FSVIDEO_ID,
			0, -1, -1, -1, 0, GETDATE(), GETDATE()
		FROM
			TBPGM_ARR_PROMO AS PROMO
			
			LEFT JOIN TBZCHANNEL AS CHA
			ON (CAST(CHA.FSCHANNEL_ID AS VARCHAR(2)) = CAST(PROMO.FSCHANNEL_ID AS VARCHAR(2)))
			
			LEFT JOIN TBLOG_VIDEO AS LOG_V
			ON (LOG_V.FSID = PROMO.FSPROMO_ID) AND (LOG_V.FSARC_TYPE = CHA.FSCHANNEL_TYPE)
			
			LEFT JOIN TBLOG_VIDEO_SEG AS LOG_VS
			ON (LOG_VS.FSFILE_NO = LOG_V.FSFILE_NO)
			
		WHERE
			(FDDATE BETWEEN @SDate AND @EDate)
			AND	NOT(ISNUll(LOG_VS.FSVIDEO_ID,'') = '')											-- FSVIDEO_ID為NULL的不取
			AND (CHA.FSCHANNEL_TYPE = '001')
			AND	(LOG_V.FSARC_TYPE = '001')
			AND (LOG_V.FCFILE_STATUS  IN ('Y','T'))

		/* ===== 以下, 跑回圈塞ID ===== */

		DECLARE CSR1 CURSOR FOR SELECT FSVIDEOID FROM @TEMP_TABLE
		DECLARE @FSVIDEOID VARCHAR(10)

		OPEN CSR1
			FETCH NEXT FROM CSR1 INTO @FSVIDEOID
			WHILE (@@FETCH_STATUS=0)
				BEGIN   
					/* ===== 針對每一筆資料, 檢查TBPGM_MSSPROCESS該日是否已有此FSVIDEO_ID, 沒有的才要新增進去 ===== */
					IF NOT (EXISTS(	SELECT	FNMSSPROCESS_ID
									FROM	TBPGM_MSSPROCESS
									WHERE	--(LEFT(CAST(FNMSSPROCESS_ID AS VARCHAR(12)),8) = @FSNO_DATE)
											FNMSSPROCESS_ID > @TODAY_PROCESS_ID
											AND	(FSVIDEOID = @FSVIDEOID)
									))
						BEGIN
							--SELECT 'N',@FSNO_DATE,@FSVIDEOID
							/* ===== 以下取法, 暫時寫法, 不是很好, 要改 ===== */
 							IF (@NEW_ID = 0)
 								BEGIN
									EXEC	[dbo].[SP_I_TBNORECORD]
											@FSTYPE			= @FSTYPE_ID,
											@FSNO_DATE		= @FSNO_DATE,
											@FSNOTE			= '排程轉檔2',
											@FSCREATED_BY	= 'SYSTEM'  
											
									SELECT	@NEW_ID = FSNO 
									FROM	TBNORECORD 
									WHERE	(FSNO_DATE	= @FSNO_DATE) AND (FSTYPE = @FSTYPE_ID)
								END
							ELSE
								BEGIN
									SET @NEW_ID = @NEW_ID + 1
								END

							UPDATE	@TEMP_TABLE
							SET		FNMSSPROCESS_ID = @NEW_ID
							WHERE CURRENT OF CSR1
						END
					--ELSE
					--	BEGIN
					--		SELECT 'Y'
					--	END

				FETCH NEXT FROM CSR1 INTO @FSVIDEOID
				END
		Close CSR1

		INSERT TBPGM_MSSPROCESS
		SELECT	[FNMSSPROCESS_ID]
			  ,[FSVIDEOID]
			  ,[FNSTATUS]
			  ,[FNTRANSCODE_ID]
			  ,[FNTSMJOB_ID]
			  ,[FNMSSCOPY_ID]
			  ,[FNFORCEDIRECT]
			  ,[FDCREATE_TIME]
			  ,[FDUPDATE_TIME]
		FROM @TEMP_TABLE
		WHERE (FNMSSPROCESS_ID <> '0')

		UPDATE	TBNORECORD
		SET		FSNO = CAST(@NEW_ID AS VARCHAR(12))
		WHERE	(@NEW_ID <> 0) AND (FSNO_DATE = @FSNO_DATE) AND (FSTYPE = @FSTYPE_ID)
		
END

GO
