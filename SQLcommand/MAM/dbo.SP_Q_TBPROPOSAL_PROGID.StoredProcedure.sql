USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SP_Q_TBPROPOSAL_PROGID]
	-- Add the parameters for the stored procedure here
	 @FSPROG_NAME varchar(50),
     @FSCREATED_BY varchar(50)
    
AS
BEGIN
	select CAST(FSPROG_ID as varchar) as FSPROG_ID  from TBPROPOSAL where FSCREATED_BY=@FSCREATED_BY and FSPROG_NAME=@FSPROG_NAME
END
GO
