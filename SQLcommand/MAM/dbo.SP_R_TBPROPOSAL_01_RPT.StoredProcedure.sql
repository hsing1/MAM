USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/03/15>
-- Description:	For RPT_TBPROPOSAL_01(成案單), 報表呼叫用
-- =============================================
CREATE PROCEDURE [dbo].[SP_R_TBPROPOSAL_01_RPT]
	@QUERY_KEY	VARCHAR(36)
AS
BEGIN
	SELECT
		[QUERY_KEY]
      ,[QUERY_BY]
      ,[QUERY_DATE]
      ,[成案單編號]
      ,[成案種類_名稱]
      ,[頻道別_名稱]
      ,[節目名稱]
      ,[播出名稱]
      ,[外文名稱]
      ,[製作目的_名稱]
      ,[節目編號]
      ,[每集時長]
      ,[集次起]
      ,[集次迄]
      ,[節目內容簡述]
      ,[預訂播出頻道_名稱]
      ,[製作部門_名稱]
      ,[付款部門_名稱]
      ,[節目來源_名稱]
      ,[節目型態_名稱]
      ,[目標觀眾_名稱]
      ,[表現方式_名稱]
      ,[主聲道_名稱]
      ,[副聲道_名稱]
      ,[預訂上檔日期]
      ,[節目分級_名稱]
      ,[前會部門_名稱]
      ,[後會部門_名稱]
      ,[外購類別大類_名稱]
      ,[外購類別細類_名稱]
      ,[節目規格]
      ,[列印人員]
      ,[分集備註]   
	FROM
		RPT_TBPROPOSAL_01
	WHERE
		(QUERY_KEY = @QUERY_KEY)
	ORDER BY
		[成案單編號]
END



GO
