USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Proc [dbo].[SP_U_TBLOG_VIDEO_STT_OK_BY_FILENO]
@FSFILE_NO char(16) ,
@FSFILE_SIZE NVARCHAR(100),
@FSVIDEO_PROPERTY varchar(max)
As

Declare @CHANGE_FILE_NO CHAR(16)=''

SELECT @CHANGE_FILE_NO = ISNULL(FSCHANGE_FILE_NO,'') FROM TBLOG_VIDEO  /*用JOBID去找即將被置換的檔案編號*/ 
WHERE TBLOG_VIDEO.FSFILE_NO = @FSFILE_NO
	 
	IF @CHANGE_FILE_NO <> ''   /*判斷是否有要置換的檔案，進入置換檔案的流程*/
          BEGIN	
		  update MAM.dbo.TBLOG_VIDEO  /*將舊的狀態存入FSENC_MSG欄位*/
				SET FSENC_MSG=FCFILE_STATUS
				WHERE  MAM.dbo.TBLOG_VIDEO.FSFILE_NO=@CHANGE_FILE_NO
											
				UPDATE MAM.dbo.TBLOG_VIDEO  /*入庫影像檔的狀態改成D(被置換)*/
				SET FCFILE_STATUS = 'D'
				WHERE  MAM.dbo.TBLOG_VIDEO.FSFILE_NO=@CHANGE_FILE_NO
				
				UPDATE MAM.dbo.TBLOG_VIDEO_D/*入庫影像檔_D的狀態改成D(被置換)*/
				SET FCFILE_STATUS = 'D'
				WHERE  MAM.dbo.TBLOG_VIDEO_D.FSFILE_NO=@CHANGE_FILE_NO				
          END    
          
UPDATE MAM.dbo.TBLOG_VIDEO
SET FCFILE_STATUS = 'T', FCLOW_RES = (case when FSARC_TYPE='026' then 'N' else 'Y' end),FSFILE_SIZE=@FSFILE_SIZE,FSVIDEO_PROPERTY=@FSVIDEO_PROPERTY
WHERE  MAM.dbo.TBLOG_VIDEO.FSFILE_NO = @FSFILE_NO



GO
