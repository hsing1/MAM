USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/24>
-- Description:   刪除短帶資料(並無法刪除,只能將FSDEL狀態改變)
-- =============================================
CREATE Proc [dbo].[SP_D_TBPGM_PROMO]
@FSPROMO_ID	varchar(12),
@FSDEL char(1) ,
@FSDELUSER varchar(50) 
As

UPDATE MAM.dbo.TBPGM_PROMO
SET FSDEL=@FSDEL,FSDELUSER=@FSDELUSER,FSUPDATED_BY=@FSDELUSER,FDUPDATED_DATE=GETDATE()
WHERE  MAM.dbo.TBPGM_PROMO.FSPROMO_ID=@FSPROMO_ID
--DELETE from TBPGM_PROMO where FSPROMO_ID=@FSPROMO_ID


GO
