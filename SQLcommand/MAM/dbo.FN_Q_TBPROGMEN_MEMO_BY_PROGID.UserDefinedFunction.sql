USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <David.Sin>
-- Create date:   <2010/10/02>
-- Description:   根據節目ID取得得獎人員備註
-- =============================================

CREATE FUNCTION [dbo].[FN_Q_TBPROGMEN_MEMO_BY_PROGID](@FSPROG_ID char(7),@FNEPISODE int)
RETURNS NVARCHAR(max)
AS
BEGIN
	DECLARE @FSMEMO_TOTAL NVARCHAR(max)
	SELECT DISTINCT
		@FSMEMO_TOTAL = ISNULL((SELECT FSMEMO + ',' FROM TBPROGMEN B WHERE A.FSPROG_ID=B.FSPROG_ID AND A.FNEPISODE = B.FNEPISODE FOR XML PATH('')),'') 
	FROM TBPROG_D A
	WHERE A.FSPROG_ID = @FSPROG_ID AND A.FNEPISODE = @FNEPISODE
	--SET @FSMEMO_TOTAL=''
	--DECLARE @FSMEMO NVARCHAR(max)
	
	--DECLARE CURSOR_A CURSOR FOR
	--SELECT FSMEMO FROM TBPROGMEN WHERE FSPROG_ID=@FSPROG_ID AND FNEPISODE=@FNEPISODE
	
	--OPEN CURSOR_A
	--FETCH NEXT FROM CURSOR_A INTO @FSMEMO
	--WHILE @@FETCH_STATUS=0
	--BEGIN
	--	IF @FSMEMO<>''
	--	BEGIN
	--		SET @FSMEMO_TOTAL = @FSMEMO_TOTAL + @FSMEMO + ','
	--	END
	--	FETCH NEXT FROM CURSOR_A INTO @FSMEMO
	--END
	--CLOSE CURSOR_A
	
	RETURN @FSMEMO_TOTAL
END


GO
