USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:        <David.Sin>
-- Create date:   <2010/10/02>
-- Description:   根據節目ID取得得獎人員姓名
-- =============================================

CREATE FUNCTION [dbo].[FN_Q_TBPROGMEN_NAME_BY_PROGID](@FSPROG_ID char(7),@FNEPISODE int)
RETURNS NVARCHAR(max)
AS
BEGIN
	DECLARE @FSNAME_TOTAL NVARCHAR(max)
	SELECT DISTINCT
		@FSNAME_TOTAL = ISNULL((SELECT FSNAME + ',' FROM TBPROGMEN B WHERE A.FSPROG_ID=B.FSPROG_ID AND A.FNEPISODE = B.FNEPISODE FOR XML PATH('')),'') 
	FROM TBPROG_D A
	WHERE A.FSPROG_ID = @FSPROG_ID AND A.FNEPISODE = @FNEPISODE


	--SET @FSNAME_TOTAL=''
	--DECLARE @FSNAME NVARCHAR(max)
	
	--DECLARE CURSOR_A CURSOR FOR
	--SELECT FSNAME FROM TBPROGMEN WHERE FSPROG_ID=@FSPROG_ID AND FNEPISODE=@FNEPISODE
	
	--OPEN CURSOR_A
	--FETCH NEXT FROM CURSOR_A INTO @FSNAME
	--WHILE @@FETCH_STATUS=0
	--BEGIN
	--	IF @FSNAME<>''
	--	BEGIN
	--		SET @FSNAME_TOTAL = @FSNAME_TOTAL + @FSNAME + ','
	--	END
	--	FETCH NEXT FROM CURSOR_A INTO @FSNAME
	--END
	--CLOSE CURSOR_A
	
	RETURN @FSNAME_TOTAL
END




GO
