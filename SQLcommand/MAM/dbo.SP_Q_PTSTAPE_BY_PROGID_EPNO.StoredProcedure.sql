USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<sam.wu>
-- Create date: <2011/6/1>
-- Description:	<透過節目編號+集別，找到館藏資料>
-- =============================================
CREATE Proc [dbo].[SP_Q_PTSTAPE_BY_PROGID_EPNO]
@progid char(7),
@epno smallint

as
declare @str varchar(20) = 'v.@FNEPISODE '
declare @str2 varchar(20) = '@FNEPISODE '
declare @str3 varchar(20) = 'v. @FNEPISODE '

select TLocation,TIndex 
from  
[PTSTape].[PTSTape].[dbo].[tblLLabelProg] LabelProg,
[PTSTape].[PTSTape].[dbo].[tblLTape] tblLTape
               left outer join [PTSTape].[PTSTape].[dbo].tblZSystem as ZSystem on tblLTape.TType=ZSystem.TypeCode
               left outer join [PTSTape].[PTSTape].[dbo].tblTapeType as TapeType on tblLTape.TTapeCode=TapeType.TapeCode
               left outer join [PTSTape].[PTSTape].[dbo].tblLLabel as LLabel on tblLTape.BIB=LLabel.BIB
                              
where 
LabelProg.Bib=tblLTape.bib
and  LabelProg.progid=@progid and LabelProg.epno=@epno
and  (
(tblLTape.TVolume like (REPLACE(@str, '@FNEPISODE', @epno)+'%'))		--v.集別 要過濾
or 
(tblLTape.TVolume like (REPLACE(@str2, '@FNEPISODE', @epno)+'%'))		--集別 要過濾
or
(tblLTape.TVolume like (REPLACE(@str3, '@FNEPISODE', @epno)+'%'))		--v. 集別 要過濾
)

--SP_Q_PTSTAPE_LABEL_BYPROGID_EPISODE '2008783','21'

GO
