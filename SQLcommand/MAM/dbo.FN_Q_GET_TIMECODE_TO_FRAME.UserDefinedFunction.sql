USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<James.Ho>
-- Create date: <2016/01/19>
-- Description:	<TIMECODE �� FRAME>
-- =============================================
CREATE FUNCTION [dbo].[FN_Q_GET_TIMECODE_TO_FRAME](@Time AS VARCHAR(11)) 

RETURNS int

AS

BEGIN

	DECLARE @hh AS CHAR(2),@mm AS CHAR(2),@ss AS CHAR(2),@ff AS CHAR(2),@countNo AS int
	
	--Remove time code character
	SELECT @Time=REPLACE(REPLACE(@Time,':',''),';','')
	SELECT @CountNo=0
	
	--Get Hour
	SELECT @hh=SUBSTRING(@Time,1,2) 
	
	--Get Min
	SELECT @mm=SUBSTRING(@Time,3,2)
	
	--Get Sec
	SELECT @ss=SUBSTRING(@Time,5,2)
	
	--Get Frame
	SELECT @ff=SUBSTRING(@Time,7,2)
	
	IF @hh<>'00'
	     BEGIN
			 SELECT @CountNo=CAST(LEFT(@hh,1) AS int)*1078920
			 SELECT @CountNo=@CountNo+CAST(RIGHT(@hh,1) AS int)*107892
	     END 
	
	IF @mm<>'00'
	     BEGIN
			 SELECT @CountNo=@CountNo+CAST(LEFT(@mm,1) AS int)*17982
			 SELECT @CountNo=@CountNo+CAST(RIGHT(@mm,1) AS int)*1798
	     END 
	
	IF @ss<>'00'
	     BEGIN
			 SELECT @CountNo=@CountNo+CAST(LEFT(@ss,1) AS int)*300
			 SELECT @CountNo=@CountNo+CAST(RIGHT(@ss,1) AS int)*30
	     END 
	
	IF @ff<>'00'
	     BEGIN
			 SELECT @CountNo=@CountNo+CAST(LEFT(@ff,1) AS int)* 10
			 SELECT @CountNo=@CountNo+CAST(RIGHT(@ff,1) AS int)
	     END 
	
	RETURN(@CountNo)

END
GO
