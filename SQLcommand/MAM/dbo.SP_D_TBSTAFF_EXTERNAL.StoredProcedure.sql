USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_D_TBSTAFF_EXTERNAL]
	@FSSTAFFID	varchar(5)

As
--DELETE FROM [PTSProg].[PTSProgram].[dbo].TBSTAFF
--WHERE  FSSTAFFID=@FSSTAFFID

--Dennis.Wen:寫法改嚴謹一點

BEGIN TRY

If EXISTS( SELECT * FROM  [PTSProg].[PTSProgram].[dbo].TBPROGMEN  WHERE FSSTAFFID = @FSSTAFFID )

    BEGIN
          RAISERROR('ERROR:節目相關人員資料已有此工作人員，無法刪除', 16, 1)
    END
    
ELSE

	BEGIN
	
		DELETE	FROM 
				[PTSProg].[PTSProgram].[dbo].TBSTAFF
		WHERE
				(FSSTAFFID = @FSSTAFFID)
	
		IF (@@ROWCOUNT = 0)
			BEGIN
				SELECT RESULT = 'ERROR:沒有受影響的資料列'
			END
		ELSE
			BEGIN
				SELECT RESULT = ''
			END	
    END	
    	
END TRY
BEGIN CATCH
	SELECT RESULT = 'ERROR:' + ERROR_MESSAGE()
END CATCH

GO
