USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_U_TBPROG_M_EXTERNAL]

@FSPROGID char(7) ,
@FSPGMNAME varchar(50) ,
@FSPGMENAME varchar(100) ,
@FSPRDDEPTID varchar(2) ,
@FSCHANNEL char(20) ,
@FNTOTEPISODE smallint ,
@FNLENGTH smallint ,
@FSPROGOBJID varchar(2) ,
@FSPROGSRCID varchar(2) ,
@FSPROGATTRID varchar(2) ,
@FSPROGAUDID varchar(2) ,
@FSPROGTYPEID varchar(2) ,
@FSPROGGRADEID varchar(2) ,
@FSPROGLANGID1 varchar(2) ,
@FSPROGLANGID2 varchar(2) ,
@FSPROGSALEID varchar(2) ,
@FSPROGBUYID varchar(2) ,
@FSPROGBUYDID varchar(2) ,
@FSPRDCENID varchar(5) ,
@FSPRDYYMM varchar(6) ,
@FDSHOWDATE datetime ,
@FSCONTENT text ,
@FSMEMO varchar(200) ,
@FSEWEBPAGE text ,
@FCDERIVE varchar(1) ,
@FSWEBNAME varchar(50) ,
@FSUPDUSER varchar(5) ,
@FSPROGNATIONID varchar(2)
As

UPDATE [PTSProg].[PTSProgram].[dbo].TBPROG_M
SET 
FSPGMNAME=@FSPGMNAME,
FSPGMENAME = case when @FSPGMENAME ='' then null else @FSPGMENAME end,
FSPRDDEPTID = case when @FSPRDDEPTID ='' then null else @FSPRDDEPTID end,
FSCHANNEL = case when @FSCHANNEL ='' then null else MAM.dbo.FN_Q_SET_CODELIST_TO_PURESTRING(@FSCHANNEL,'20') end,
FNTOTEPISODE=@FNTOTEPISODE,
FNLENGTH=@FNLENGTH,
FSPROGOBJID = case when @FSPROGOBJID ='' then null else @FSPROGOBJID end,
FSPROGSRCID=@FSPROGSRCID,
FSPROGATTRID=@FSPROGATTRID,
FSPROGAUDID=@FSPROGAUDID,
FSPROGTYPEID=@FSPROGTYPEID,
FSPROGGRADEID=@FSPROGGRADEID,
FSPROGLANGID1=@FSPROGLANGID1,
FSPROGLANGID2=@FSPROGLANGID2,
FSPROGSALEID = case when @FSPROGSALEID ='' then null else @FSPROGSALEID end,
FSPROGBUYID = case when @FSPROGBUYID ='' then null else @FSPROGBUYID end,
FSPROGBUYDID = case when @FSPROGBUYDID ='' then null else @FSPROGBUYDID end,
FSPRDCENID=@FSPRDCENID,
FSPRDYYMM = case when @FSPRDYYMM ='' then null else @FSPRDYYMM end,
FDSHOWDATE = case when @FDSHOWDATE ='' then null else @FDSHOWDATE end,--若是播出日期無填入，到節目管理系統要為Null值
FSCONTENT = @FSCONTENT,
FSMEMO = case when @FSMEMO ='' then null else @FSMEMO end,
FSEWEBPAGE =@FSEWEBPAGE,
FCDERIVE=@FCDERIVE,
FSWEBNAME=@FSWEBNAME,
FSUPDUSER='01065',
FDUPDDATE=GETDATE(),
FSPROGNATIONID=@FSPROGNATIONID
WHERE  FSPROGID=@FSPROGID

GO
