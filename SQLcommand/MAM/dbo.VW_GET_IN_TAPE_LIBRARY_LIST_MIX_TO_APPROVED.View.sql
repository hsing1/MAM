USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author: Hsing1     
-- Create date: 2017-09-03
-- Description: 取得5天內要播出的已入庫(已審核）的檔案LIST（要搬到 Approved)  
-- =============================================
CREATE VIEW [dbo].[VW_GET_IN_TAPE_LIBRARY_LIST_MIX_TO_APPROVED]

AS
	select distinct A.FSPROG_ID as FSID,A.FNEPISODE,B.FSFILE_NO,B.FSVIDEO_PROG,B.FCFILE_STATUS,B.FSBRO_ID,C.FCSTATUS,C.FSCREATED_BY,B.FSFILE_PATH_H
		from tbpgm_combine_queue A
		left join TBLOG_Video B on A.FSVIDEO_ID=B.FSVIDEO_PROG and B.FCFILE_STATUS not in('D','X','F')
		left join TBLOG_Video_HD_MC C on B.FSFILE_NO=C.FSFILE_NO 
		inner join TBZCHANNEL E on A.FSCHANNEL_ID=E.FSCHANNEL_ID and E.FSCHANNEL_TYPE='002'
		where 
		A.FDDATE>=getdate() and A.FDDATE<=getdate()+5  --已審核節目的 5 天才放進待播區
		and (B.FCFILE_STATUS='T' or B.FCFILE_STATUS='Y')
		and C.FCSTATUS = 'O'
		union
		select distinct A.FSPROMO_ID AS FSID,0,B.FSFILE_NO,B.FSVIDEO_PROG,B.FCFILE_STATUS,B.FSBRO_ID,C.FCSTATUS,C.FSCREATED_BY,B.FSFILE_PATH_H
			from TBPGM_ARR_PROMO A
			left join TBLOG_Video B on A.FSVIDEO_ID=B.FSVIDEO_PROG and B.FCFILE_STATUS not in('D','X','F')
			left join TBLOG_Video_HD_MC C on B.FSFILE_NO=C.FSFILE_NO 
			inner join TBZCHANNEL E on A.FSCHANNEL_ID=E.FSCHANNEL_ID and E.FSCHANNEL_TYPE='002'
			where 
			A.FDDATE>=getdate() and A.FDDATE<=getdate()+5 --已審核的短帶 5 天才放進待播區
			and (B.FCFILE_STATUS='T' or B.FCFILE_STATUS='Y')
			and C.FCSTATUS = 'O'



GO
