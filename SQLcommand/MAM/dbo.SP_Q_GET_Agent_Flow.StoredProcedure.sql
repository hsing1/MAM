USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_Q_GET_Agent_Flow]
@FSUSER_ID varchar(50)
AS

SELECT a.[aid]
      ,a.[memberid]
      ,b.Member_Name as MemberFSID
      ,b.Member_DisplayName as MemberFSNAME
      ,a.[agentid]
      ,c.Member_Name as AgentFSID
      ,c.Member_DisplayName as AgentFSNAME
      ,a.[SDate]
      ,a.[EDate]
      ,a.[Status]
      ,a.[AgreeDate]
      ,a.[SendDate]
      ,a.[ifopen]
  FROM [PTSFlow].[dbo].[Agent_Flow] as a
  left outer join PTSFlow.dbo.OC_Member as b on a.memberid =b.Member_GUID
  left outer join PTSFlow.dbo.OC_Member as c on a.agentid =c.Member_GUID
  where GETDATE() between a.SDate and a.EDate and c.Member_Name=@FSUSER_ID

GO
