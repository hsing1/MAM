USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBLOG_FILE_EXPIRE_DELETE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FSTABLE_NAME] [nvarchar](500) NULL,
	[FSRECORD] [nvarchar](500) NULL,
	[FSUPDATED_BY] [nvarchar](75) NULL,
	[FDUPDATED_DATE] [datetime] NULL
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[TBLOG_FILE_EXPIRE_DELETE] ADD  CONSTRAINT [DF_TBLOG_FILE_EXPIRE_DELETE_FDUPDATED_DATE]  DEFAULT (getdate()) FOR [FDUPDATED_DATE]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'流水編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_FILE_EXPIRE_DELETE', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改的資料表名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_FILE_EXPIRE_DELETE', @level2type=N'COLUMN',@level2name=N'FSTABLE_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改紀錄' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_FILE_EXPIRE_DELETE', @level2type=N'COLUMN',@level2name=N'FSRECORD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'誰修改的' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_FILE_EXPIRE_DELETE', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_FILE_EXPIRE_DELETE', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
