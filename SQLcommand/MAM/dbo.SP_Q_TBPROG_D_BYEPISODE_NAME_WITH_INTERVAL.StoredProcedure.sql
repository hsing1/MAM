USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Proc [dbo].[SP_Q_TBPROG_D_BYEPISODE_NAME_WITH_INTERVAL]
@FSPROG_ID char(7) ,
@FNEPISODE smallint,
@FSPGDNAME NVARCHAR (50),
@StartEpisode nvarchar(50),
@EndEpisode nvarchar(50)

As
SELECT ISNULL(b.FSUSER_ChtName,'') as FSCREATED_BY_NAME, ISNULL(C.FSUSER_ChtName,'') as FSUPDATED_BY_NAME ,
ISNULL(CONVERT(VARCHAR ,a.FDCRTDATE , 120),'') as WANT_FDCREATEDDATE,
ISNULL(CONVERT(VARCHAR ,a.FDUPDDATE , 120),'') as WANT_FDUPDATEDDATE,
ISNULL(d.FSPGMNAME,'')  as FSPROG_ID_NAME,
ISNULL(a.FSPGDNAME,'')  as FNEPISODE_NAME,
a.* FROM TBPROG_D  as a 
               left outer JOIN TBUSERS as b on a.FSCRTUSER=b.FSUSER_ID
               left outer join TBUSERS as c on a.FSUPDUSER=c.FSUSER_ID
               left outer join TBPROG_M as d on a.FSPROG_ID=d.FSPROG_ID -----[MAM].[dbo].[TBPROG_M]
WHERE a.FSDEL <> 'Y' 
AND a.FSPROG_ID=@FSPROG_ID 
--AND @FNEPISODE = case when @FNEPISODE ='' then @FNEPISODE else FNEPISODE end
And (FNEPISODE between case @StartEpisode when '' then 0 else @StartEpisode end AND case @EndEpisode when '' then 9999999 else @EndEpisode end)
AND a.FSPGDNAME like '%' + @FSPGDNAME +'%'
order by FSPROG_ID,FNEPISODE 
GO
