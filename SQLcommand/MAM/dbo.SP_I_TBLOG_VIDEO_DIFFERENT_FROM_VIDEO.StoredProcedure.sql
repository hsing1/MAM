USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_I_TBLOG_VIDEO_DIFFERENT_FROM_VIDEO]
@FSFILE_NO char(16)
AS

INSERT INTO TBLOG_VIDEO_DIFFERENT(MODE,FSFILE_NO)
VALUES(1,@FSFILE_NO)
GO
