USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/03/30>
-- Description:	把影片管理系統傳來的Title與Episode跟MAM中的TBPROG_進行比對完後, 塞入資料表
-- =============================================
CREATE PROC [dbo].[SP_I_TBTAPE_LOOKUP]
	@FSPROG_ID			CHAR(7),
	@FNEPISODE			SMALLINT,
	@FSBIB				VARCHAR(7),
	@FSNOTE				NVARCHAR(200),
	@FSCREATED_BY		VARCHAR(50),
	@FDCREATED_DATE		DATETIME,
	@EXEC_GUID			VARCHAR(36)
AS

	INSERT
		TBTAPE_LOOKUP	([FSPROG_ID]
						  ,[FNEPISODE]
						  ,[FSBIB]
						  ,[FSNOTE]
						  ,[FSCREATED_BY]
						  ,[FDCREATED_DATE]
						  ,[EXEC_GUID])
	VALUES
						(@FSPROG_ID
						,@FNEPISODE		
						,@FSBIB			
						,@FSNOTE			
						,@FSCREATED_BY	
						,@FDCREATED_DATE
						,@EXEC_GUID)
						
	SELECT	COUNT(*)
	FROM	TBTAPE_LOOKUP 
	WHERE	(FSPROG_ID = @FSPROG_ID) AND
			(FNEPISODE = @FNEPISODE) AND
			(EXEC_GUID = @EXEC_GUID)	--為了確定本次有新增進去
GO
