USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Q_REVIEWTREE_GETYEARS]
@StationID nvarchar(50), 
@CatalogID nvarchar(50)


As


IF @CatalogID='G' 
BEGIN 
	SELECT distinct LEFT([FSPROG_ID],4) as YEARS FROM [MAM].[dbo].[TBPROG_M] where FSCHANNEL_ID=@StationID order by YEARS
END 

IF @CatalogID='P' 
Begin 
	SELECT distinct LEFT([FSPROMO_ID],4) as YEARS FROM [MAM].[dbo].[TBPGM_PROMO] where FSMAIN_CHANNEL_ID=@StationID order by YEARS
End 

if @CatalogID='D'
begin
	SELECT distinct LEFT([FSDATA_ID],4) as YEARS FROM [MAM].[dbo].[TBDATA] order by YEARS
end

GO
