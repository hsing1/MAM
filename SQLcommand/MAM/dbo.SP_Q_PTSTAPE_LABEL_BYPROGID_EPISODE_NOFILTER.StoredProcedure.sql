USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<sam.wu>
-- Create date: <2011/6/1>
-- Description:	<透過節目編號+集別，找到館藏資料，館藏資料不作集別的過濾>
-- =============================================
CREATE Proc [dbo].[SP_Q_PTSTAPE_LABEL_BYPROGID_EPISODE_NOFILTER]
@FSPROG_ID char(7),
@FNEPISODE smallint

as
select tblLTape.*,
ZSystem.TypeName as TType_Name ,
TapeType.TapeName as TTapeCode_Name,
LLabel.Title as  TAPE_TITLE,
LLabel.Description as  TAPE_DES,
LLabel.Duration as  TAPE_DURATION,
ISNULL(CONVERT(VARCHAR(20) ,tblLTape.CreateDT , 111),'') as WANT_FDCREATEDDATE,
ISNULL(CONVERT(VARCHAR(20) ,tblLTape.UpdateDT , 111),'') as WANT_FDUPDATEDDATE
from  
[PTSTape].[PTSTape].[dbo].[tblLLabelProg] LabelProg,
[PTSTape].[PTSTape].[dbo].[tblLTape] tblLTape
               left outer join [PTSTape].[PTSTape].[dbo].tblZSystem as ZSystem on tblLTape.TType=ZSystem.TypeCode
               left outer join [PTSTape].[PTSTape].[dbo].tblTapeType as TapeType on tblLTape.TTapeCode=TapeType.TapeCode
               left outer join [PTSTape].[PTSTape].[dbo].tblLLabel as LLabel on tblLTape.BIB=LLabel.BIB
                              
where 
LabelProg.Bib=tblLTape.bib
and  LabelProg.progid=@FSPROG_ID and LabelProg.epno=@FNEPISODE
and  tblLTape.deadmark = '0' --影帶回溯時，館藏資料若是已停用就不要帶到MAM

GO
