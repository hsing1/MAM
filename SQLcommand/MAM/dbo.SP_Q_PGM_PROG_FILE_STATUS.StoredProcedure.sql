USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

-- Author:        <Mike.Lin>

-- Create date:   <2014/09/17>

-- Description:   查詢節目集數檔案狀態資料

-- =============================================

CREATE Proc [dbo].[SP_Q_PGM_PROG_FILE_STATUS]

@FNEPISODE	smallint,
@FSARC_TYPE	char(3),
@FSPGDNAME nvarchar(50)

As
Select A.FSFILE_NO,B.FSPROG_ID FSID,B.FNEPISODE,A.FSARC_TYPE,A.FCLOW_RES,A.FSBEG_TIMECODE,A.FSEND_TIMECODE ,C.FSPGMNAME AS FSPGDNAME
from TBPROG_D B 
left join dbo.TBLOG_VIDEO A on B.FSPROG_ID=A.FSID and B.FNEPISODE=A.FNEPISODE 
and  (A.FCFILE_STATUS='T' or A.FCFILE_STATUS='Y')
and (@FSARC_TYPE = case when @FSARC_TYPE = '' then @FSARC_TYPE else A.FSARC_TYPE end or A.FSARC_TYPE is null)
left join TBPROG_M C on B.FSPROG_ID=C.FSPROG_ID
where 1=1 
and @FNEPISODE = case when @FNEPISODE = '' then @FNEPISODE else B.FNEPISODE end
and C.FSPGMNAME like '%' + @FSPGDNAME + '%'
order by B.FNEPISODE

GO
