USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBPGM_INSERT_TAPE](
	[FSPROG_ID] [char](7) NOT NULL,
	[FNEPISODE] [int] NOT NULL,
	[FSARC_TYPE] [varchar](3) NOT NULL,
	[FCSMOKE] [char](1) NULL,
	[FCALERTS] [char](1) NULL,
	[FSINSERT_TAPE] [varchar](1) NULL,
	[FSINSERT_TAPE_NAME] [varchar](50) NULL,
	[FSINSERT_TAPE_DUR] [int] NULL,
	[FSTITLE] [char](1) NULL,
	[FSTITLE_NAME] [varchar](50) NULL,
	[FSTITLE_DUR] [int] NULL,
	[FCPROG_SIDE_LABEL] [char](1) NULL,
	[FCLIVE_SIDE_LABEL] [char](1) NULL,
	[FCTIME_SIDE_LABEL] [char](1) NULL,
	[FCREPLAY] [char](1) NULL,
	[FCRECORD_PLAY] [char](1) NULL,
	[FCSTEREO] [char](1) NULL,
	[FCLOGO] [char](1) NULL,
	[FSCH1] [char](2) NULL,
	[FSCH2] [char](2) NULL,
	[FSCH3] [char](2) NULL,
	[FSCH4] [char](2) NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
 CONSTRAINT [PK_TBPGM_INSERT_TAPE] PRIMARY KEY CLUSTERED 
(
	[FSPROG_ID] ASC,
	[FNEPISODE] ASC,
	[FSARC_TYPE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目編碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_INSERT_TAPE', @level2type=N'COLUMN',@level2name=N'FSPROG_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目集數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_INSERT_TAPE', @level2type=N'COLUMN',@level2name=N'FNEPISODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'類型(001:SD,002:HD...)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_INSERT_TAPE', @level2type=N'COLUMN',@level2name=N'FSARC_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否需要吸菸卡' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_INSERT_TAPE', @level2type=N'COLUMN',@level2name=N'FCSMOKE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否需要警示卡' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_INSERT_TAPE', @level2type=N'COLUMN',@level2name=N'FCALERTS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'警示卡號碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_INSERT_TAPE', @level2type=N'COLUMN',@level2name=N'FSINSERT_TAPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'警示卡客製版名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_INSERT_TAPE', @level2type=N'COLUMN',@level2name=N'FSINSERT_TAPE_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'警示卡客製版長度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_INSERT_TAPE', @level2type=N'COLUMN',@level2name=N'FSINSERT_TAPE_DUR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否需要檔名總片頭' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_INSERT_TAPE', @level2type=N'COLUMN',@level2name=N'FSTITLE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'檔名總片頭名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_INSERT_TAPE', @level2type=N'COLUMN',@level2name=N'FSTITLE_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'檔名總片頭長度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_INSERT_TAPE', @level2type=N'COLUMN',@level2name=N'FSTITLE_DUR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目側標' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_INSERT_TAPE', @level2type=N'COLUMN',@level2name=N'FCPROG_SIDE_LABEL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Live側標' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_INSERT_TAPE', @level2type=N'COLUMN',@level2name=N'FCLIVE_SIDE_LABEL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'時間側標' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_INSERT_TAPE', @level2type=N'COLUMN',@level2name=N'FCTIME_SIDE_LABEL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'重播' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_INSERT_TAPE', @level2type=N'COLUMN',@level2name=N'FCREPLAY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'錄影轉播' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_INSERT_TAPE', @level2type=N'COLUMN',@level2name=N'FCRECORD_PLAY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'立體聲' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_INSERT_TAPE', @level2type=N'COLUMN',@level2name=N'FCSTEREO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'公視LOGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_INSERT_TAPE', @level2type=N'COLUMN',@level2name=N'FCLOGO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'音軌1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_INSERT_TAPE', @level2type=N'COLUMN',@level2name=N'FSCH1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'音軌2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_INSERT_TAPE', @level2type=N'COLUMN',@level2name=N'FSCH2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'音軌3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_INSERT_TAPE', @level2type=N'COLUMN',@level2name=N'FSCH3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'音軌4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_INSERT_TAPE', @level2type=N'COLUMN',@level2name=N'FSCH4'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_INSERT_TAPE', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_INSERT_TAPE', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'描述', @value=N'主控播出題示表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_INSERT_TAPE'
GO
