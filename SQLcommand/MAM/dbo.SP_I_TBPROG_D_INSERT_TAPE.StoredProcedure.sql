USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_I_TBPROG_D_INSERT_TAPE]

@FSPROG_ID	char(7),
@FNEPISODE	int,
@FNNO	int,
@FSPROMO_ID	varchar(11),
@FSINSERT_CHANNEL_ID	varchar(2),
@FSCREATED_BY	varchar(50)	

As
Insert Into dbo.TBPROG_D_INSERT_TAPE(FSPROG_ID,
FNEPISODE,FNNO,FSPROMO_ID,FSINSERT_CHANNEL_ID,
FSCREATED_BY,FDCREATED_DATE)
Values(@FSPROG_ID,
@FNEPISODE,@FNNO,@FSPROMO_ID,@FSINSERT_CHANNEL_ID,
@FSCREATED_BY,GETDATE())






	
	
GO
