USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   依照ID與集數與播放頻道查詢此節目或短帶的主控編號
-- =============================================
CREATE Proc [dbo].[SP_Q_TBLOG_VIDEOID_MAP_BY_CHANNEL_ID]

@FSID varchar(11),
@FNEPISODE smallint,
@FSCHANNEL_ID varchar(2)

As

SELECT FSVIDEO_ID_PROG 
FROM TBLOG_VIDEOID_MAP A,TBZCHANNEL B
WHERE A.FSARC_TYPE=B.FSCHANNEL_TYPE  and FSID = @FSID AND FNEPISODE = @FNEPISODE
and B.FSCHANNEL_ID=@FSCHANNEL_ID



GO
