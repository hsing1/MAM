USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBPROGLINK](
	[FSPROG_ID] [char](7) NOT NULL,
	[FNEPISODE] [smallint] NOT NULL,
	[FSORGPROG_ID] [char](7) NOT NULL,
	[FNORGEPISODE] [smallint] NOT NULL,
	[FSMEMO] [nvarchar](200) NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
	[FSUPDATED_BY] [varchar](50) NULL,
	[FDUPDATED_DATE] [datetime] NULL,
	[FSCREATED_BY_PGM] [varchar](50) NULL,
 CONSTRAINT [PK_TBPROGLINK] PRIMARY KEY CLUSTERED 
(
	[FSPROG_ID] ASC,
	[FNEPISODE] ASC,
	[FSORGPROG_ID] ASC,
	[FNORGEPISODE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[TBPROGLINK]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROGLINK_TBPROG_M] FOREIGN KEY([FSPROG_ID])
REFERENCES [dbo].[TBPROG_M] ([FSPROG_ID])
GO
ALTER TABLE [dbo].[TBPROGLINK] CHECK CONSTRAINT [FK_TBPROGLINK_TBPROG_M]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目編碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGLINK', @level2type=N'COLUMN',@level2name=N'FSPROG_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'集別' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGLINK', @level2type=N'COLUMN',@level2name=N'FNEPISODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'扣播出次數節目編碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGLINK', @level2type=N'COLUMN',@level2name=N'FSORGPROG_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'扣播出次數集別' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGLINK', @level2type=N'COLUMN',@level2name=N'FNORGEPISODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'備註' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGLINK', @level2type=N'COLUMN',@level2name=N'FSMEMO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGLINK', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGLINK', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGLINK', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGLINK', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
