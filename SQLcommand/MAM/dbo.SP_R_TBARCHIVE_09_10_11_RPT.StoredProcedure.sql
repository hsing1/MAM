USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_R_TBARCHIVE_09_10_11_RPT] --'123'
@FSFILE_NO  char(16)
AS
BEGIN
	SET NOCOUNT ON;
declare  @temp_Table  table
(
ID int,
FSFILE_NO char(16),
節目名稱 nvarchar(50),
集別 int,
分集名 nvarchar(50),
片長_分 int,
片長_秒 int,
音軌1 char(1),
音軌2 char(1),
音軌3 char(1),
音軌4 char(1),
page_no int,
節目編號 char(7),
帶別 nvarchar(10)
)


	select 
	IDENTITY(INT,1,1) as ID,
	v.FSFILE_NO,
	m.FSPGMNAME as '節目名稱',
	v.FNEPISODE as '集別',
	md.FSPGDNAME as '分集名',
	dbo.Caculate_TotalMinute( (select Convert(int, myvalues) 	from dbo.SplitToTable(
    dbo.CaculateDuration(v.fsbeg_TimeCode,v.fsend_TimeCode),':') as stt where stt.sid=1),(select Convert(int, myvalues) from dbo.SplitToTable(
    dbo.CaculateDuration(v.fsbeg_TimeCode,v.fsend_TimeCode),':') as stt where stt.sid=2)) as '片長_分',
	(select Convert(int, myvalues) from dbo.SplitToTable(
    dbo.CaculateDuration(v.fsbeg_TimeCode,v.fsend_TimeCode),':') as stt where stt.sid=3) as '片長_秒',
	SUBSTRING(v.fstrack,1,1) as '音軌1',
	SUBSTRING(v.fstrack,2,1) as '音軌2',
	SUBSTRING(v.fstrack,3,1) as '音軌3',
	SUBSTRING(v.fstrack,4,1) as '音軌4',
	--(select count(*) from TBLOG_VIDEO_SEG where FSFILE_NO='G201500500090008')/2 as 'page_no',
	--ISNULL(seg.FSBEG_TIMECODE,'00:00:00:00') as 'TImecode_起'
	--,ISNULL(seg.FSEND_TIMECODE,'00:00:00:00') as 'Timecode_迄'
	--,dbo.CaculateDuration(ISNULL( seg.FSBEG_TIMECODE,'00:00:00:00'),ISNULL(seg.FSEND_TIMECODE,'00:00:00:00')) as '實長',
	v.FSID as '節目編號',
	ft.FSTYPE_NAME as '帶別'
	into #temp_Table
	 from TBLOG_VIDEO v
	 join tbfile_type ft on ft.FSARC_TYPE=v.FSARC_TYPE
	--left outer join TBLOG_VIDEO_D d on v.FSFILE_NO=d.FSFILE_NO
	left outer join TBLOG_VIDEO_SEG seg on seg.fsfile_no=v.fsfile_no
	left outer join TBPROG_M m on m.FSPROG_ID=v.fsid
	left outer join TBPROG_D md on md.FSPROG_ID=v.fsid and md.FNEPISODE=v.fnepisode
	where v.FSFILE_NO=@FSFILE_NO


	select distinct page_no=(ID-1)/5,節目名稱,
		FSFILE_NO,
集別 ,
分集名,
Len(分集名) as '名稱長度',
片長_分 ,
片長_秒 ,
音軌1 ,
音軌2,
音軌3 ,
音軌4 ,
節目編號,
帶別  from #temp_Table 
END

GO
