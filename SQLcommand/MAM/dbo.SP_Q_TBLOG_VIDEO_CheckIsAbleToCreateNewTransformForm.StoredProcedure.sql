USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Proc [dbo].[SP_Q_TBLOG_VIDEO_CheckIsAbleToCreateNewTransformForm]
@FSID nvarchar(50),
@FNEPISODE nvarchar(50),
@FSARC_TYPE nvarchar(10)

As
  select a.FSTYPE,a.FSID,a.FNEPISODE,a.FSARC_TYPE,a.FCFILE_STATUS,b.FSBRO_ID,b.FCCHECK_STATUS,a.FSCREATED_BY,c.FSUSER_ChtName from mam.dbo.TBLOG_VIDEO as a
  left outer join mam.dbo.TBBROADCAST as b on a.FSBRO_ID=b.FSBRO_ID
  left outer join mam.dbo.TBUSERS as c on c.FSUSER_ID=a.FSCREATED_BY
  where a.FSID=@FSID and a.FNEPISODE=@FNEPISODE and a.FSARC_TYPE=@FSARC_TYPE
  order by a.FDCREATED_DATE
GO
