USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Proc [dbo].[SP_I_TBLOG_DOC_METADATA]

@FSFILE_NO char(16) ,
@FSSUBJECT_ID char(12) ,
@FSTYPE char(1) ,
@FSID varchar(11) ,
@FNEPISODE smallint ,
@FSARC_TYPE varchar(3) ,
@FSFILE_TYPE varchar(5) ,
@FSTITLE nvarchar(200) ,
@FNDIR_ID bigint,
@FSCONTENT nvarchar(MAX),
@FSDESCRIPTION nvarchar(400) ,
@FSFILE_SIZE nvarchar(100) ,
@FCFILE_STATUS char(1) ,
@FSOLD_FILE_NAME nvarchar(100),
@FSCHANGE_FILE_NO varchar(20) ,
@FSFILE_PATH nvarchar(200) ,
@FSCHANNEL_ID char(2) ,
@FSARC_ID char(12) ,
@FSMETA_PATH nvarchar(500),
@FSCREATED_BY varchar(50) 




As
Insert Into TBLOG_DOC(FSFILE_NO,FSSUBJECT_ID,FSTYPE,FSID,FNEPISODE,FSARC_TYPE,FSFILE_TYPE,FSTITLE,FSCONTENT,FSDESCRIPTION,FSFILE_SIZE,FCFILE_STATUS,FSOLD_FILE_NAME,FSCHANGE_FILE_NO,FSFILE_PATH,FNDIR_ID,FSCHANNEL_ID,FSARC_ID,FSMETA_PATH,FSCREATED_BY,FDCREATED_DATE)
Values(@FSFILE_NO,@FSSUBJECT_ID,@FSTYPE,@FSID,@FNEPISODE,@FSARC_TYPE,@FSFILE_TYPE,@FSTITLE,@FSCONTENT,@FSDESCRIPTION,@FSFILE_SIZE,@FCFILE_STATUS,@FSOLD_FILE_NAME,@FSCHANGE_FILE_NO,@FSFILE_PATH,@FNDIR_ID,@FSCHANNEL_ID,@FSARC_ID,@FSMETA_PATH,@FSCREATED_BY,GETDATE())
GO
