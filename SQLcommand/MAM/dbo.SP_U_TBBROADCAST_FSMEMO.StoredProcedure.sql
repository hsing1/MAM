USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Proc [dbo].[SP_U_TBBROADCAST_FSMEMO]
@FSBRO_ID CHAR(12),
@FSMEMO nvarchar(max),
@FSUPDATED_BY nvarchar(100)

As
UPDATE MAM.dbo.TBBROADCAST
SET FSMEMO=@FSMEMO ,FSUPDATED_BY=@FSUPDATED_BY,FDUPDATED_DATE=GETDATE()
WHERE  MAM.dbo.TBBROADCAST.FSBRO_ID=@FSBRO_ID
GO
