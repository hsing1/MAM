USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBUSERS_BOSS_TEMP](
	[DeptID] [nvarchar](100) NULL,
	[UserID] [nvarchar](100) NULL,
	[UserChtName] [nvarchar](100) NULL,
	[Mail] [nvarchar](100) NULL,
	[JobTitle] [nvarchar](100) NULL,
	[IsSupervisor] [nvarchar](100) NULL,
	[IsMainJobTitle] [nvarchar](100) NULL,
	[Memo] [nvarchar](100) NULL
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部門編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS_BOSS_TEMP', @level2type=N'COLUMN',@level2name=N'DeptID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主管編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS_BOSS_TEMP', @level2type=N'COLUMN',@level2name=N'UserID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主管中文名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS_BOSS_TEMP', @level2type=N'COLUMN',@level2name=N'UserChtName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'電子郵件' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS_BOSS_TEMP', @level2type=N'COLUMN',@level2name=N'Mail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'職位抬頭(預設:單位主管)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS_BOSS_TEMP', @level2type=N'COLUMN',@level2name=N'JobTitle'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否為主管(預設Y)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS_BOSS_TEMP', @level2type=N'COLUMN',@level2name=N'IsSupervisor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主職稱(預設N)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS_BOSS_TEMP', @level2type=N'COLUMN',@level2name=N'IsMainJobTitle'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'註解' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS_BOSS_TEMP', @level2type=N'COLUMN',@level2name=N'Memo'
GO
