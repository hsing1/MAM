USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/04/25>
-- Description:	For RPT_TBLOG_MASTERCONTROL_FTP_01, 報表呼叫用
-- =============================================
CREATE PROCEDURE [dbo].[SP_R_TBLOG_MASTERCONTROL_FTP_01_RPT]
	@DATE			NVARCHAR(10),
	@FSCHANNEL_ID	NVARCHAR(2),
	@QUERY_BY		VARCHAR(50) 
AS
BEGIN
	--DECLARE @AVATIME_MIN SMALLINT = 5				--預設為五分鐘有效, 後續可以考慮改讀取設定檔
	--DECLARE @QUERY_KEY UNIQUEIDENTIFIER = NEWID()	--請留意只有在SQL2008以上才可以在宣告時直接給值, by Dennis	
	--DECLARE @TIME_NOW DATETIME = GETDATE()
	--DECLARE @TIME_SESSION DATETIME = '1900/01/01'
	--DECLARE @NODATA NVARCHAR(20) = 'Y'
	
	--SELECT	@TIME_SESSION = ISNULL(FDLAST_ACCESS_TIME,'1900/01/01'),
	--		@NODATA	=  ISNULL(FSUSER_ID,'Y')		--當取不到資料時, @NODATA會為'Y'
	--FROM TBUSER_SESSION
	--WHERE FSSESSION_ID = @FSSESSION_ID
	
	--IF(@NODATA = 'Y')
	--	BEGIN
	--		INSERT TBLOG_ERRORQRY(QRY_TYPE, QRY_ITEM, QRY_BY, QRY_DATE, NOTE)
	--		VALUES ('REPORT', 'RPT_TBLOG_MASTERCONTROL_FTP_01', @QUERY_BY, GETDATE(), 'SESSION_NOTEXIST')
	--		SELECT RESULT='ERROR:SESSION_NOTEXIST'
	--	END
	--ELSE IF(@TIME_NOW > DATEADD(minute, @AVATIME_MIN, @TIME_SESSION))	--若現在時間(@TIME_NOW)已經超過了前一次動作時間(@TIME_SESSION)+允許時間(@AVATIME_MIN)
	--	BEGIN
	--	-------------------	以下, 檢查使用者的查詢權限有無過期, 過期的無效查詢紀錄於TBLOG_ERRORQRY, NOTE為'SESSION_TIMEOUT'
	--		INSERT TBLOG_ERRORQRY(QRY_TYPE, QRY_ITEM, QRY_BY, QRY_DATE, NOTE)
	--		VALUES ('REPORT', 'RPT_TBLOG_MASTERCONTROL_FTP_01', @QUERY_BY, GETDATE(), 'SESSION_TIMEOUT')
	--		SELECT RESULT='ERROR:SESSION_TIMEOUT'
	--	END
	--ELSE
	--	BEGIN
			DECLARE @MAX1 BIGINT, @MAX2 BIGINT
			SELECT @MAX1 = 500000, @MAX2 = 500000

			IF (@DATE <> '')
				BEGIN
					SET @DATE = CONVERT(NVARCHAR(8),CAST(@DATE AS SMALLDATETIME),112)
				END
				
			DECLARE
				@TEMPTABLE TABLE(	FSTIME			VARCHAR(8),
									FSVIDEO_ID		VARCHAR(10),
									FSCHANNEL_ID	VARCHAR(2),
									--FNFILE_SIZE		BIGINT,
									_DATE			VARCHAR(10),
									_CHANNEL		VARCHAR(20),
									_SIZE1			BIGINT,
									_SIZE2			BIGINT,
									_TOTALTODAY1	BIGINT,
									_TOTALTODAY2	BIGINT,
									_TOTALALL1		BIGINT,
									_TOTALALL2		BIGINT)
				
			INSERT
				@TEMPTABLE

			SELECT
				DISTINCT
				FSTIME			= FTP1.FSTIME,
				FSVIDEO_ID		= FTP1.FSVIDEO_ID,
				FSCHANNEL_ID	= FTP1.FSCHANNEL_ID,
				--FNFILE_SIZE		= FTP1.FNFILE_SIZE,
				_DATE			= SUBSTRING(FTP1.FSTIME,1,4) + '/' + SUBSTRING(FTP1.FSTIME,5,2) + '/' + SUBSTRING(FTP1.FSTIME,7,2),
				_CHANNEL		= '('+ISNULL(CHA.FSCHANNEL_NAME, FTP1.FSCHANNEL_ID)+')',
				_SIZE1			= ISNULL(FTP1.FNFILE_SIZE/1000000,0),
				_SIZE2			= ISNULL(FTP2.FNFILE_SIZE/1000000,0),
				0, 0, 0, 0
				
			FROM
				TBLOG_MASTERCONTROL_FTP AS FTP1
				LEFT JOIN TBLOG_MASTERCONTROL_FTP AS FTP2 ON (FTP1.FSTIME = FTP2.FSTIME) AND (FTP1.FSVIDEO_ID = FTP2.FSVIDEO_ID) AND (FTP2.FSFTP_ID = '2')
				LEFT JOIN TBZCHANNEL AS CHA ON (CHA.FSCHANNEL_ID = FTP1.FSCHANNEL_ID)

			WHERE
				(FTP1.FSTIME = @DATE) AND
				(FTP1.FSCHANNEL_ID = @FSCHANNEL_ID) AND
				(FTP1.FSFTP_ID = '1') /*AND
				(ISNULL(FTP1.FNFILE_SIZE,0) = ISNULL(FTP2.FNFILE_SIZE,0))*/
		UNION

			SELECT
				DISTINCT
				FSTIME			= FTP2.FSTIME,
				FSVIDEO_ID		= FTP2.FSVIDEO_ID,
				FSCHANNEL_ID	= FTP2.FSCHANNEL_ID,
				--FNFILE_SIZE		= FTP2.FNFILE_SIZE,
				_DATE			= SUBSTRING(FTP2.FSTIME,1,4) + '/' + SUBSTRING(FTP2.FSTIME,5,2) + '/' + SUBSTRING(FTP2.FSTIME,7,2),
				_CHANNEL		= '('+ISNULL(CHA.FSCHANNEL_NAME, FTP2.FSCHANNEL_ID)+')',
				_SIZE1			= ISNULL(FTP1.FNFILE_SIZE/1000000,0),
				_SIZE2			= ISNULL(FTP2.FNFILE_SIZE/1000000,0),
				0, 0, 0, 0
				
			FROM
				TBLOG_MASTERCONTROL_FTP AS FTP2
				LEFT JOIN TBLOG_MASTERCONTROL_FTP AS FTP1 ON (FTP2.FSTIME = FTP1.FSTIME) AND (FTP2.FSVIDEO_ID = FTP1.FSVIDEO_ID) AND (FTP1.FSFTP_ID = '1')
				LEFT JOIN TBZCHANNEL AS CHA ON (CHA.FSCHANNEL_ID = FTP2.FSCHANNEL_ID)

			WHERE
				(FTP2.FSTIME = @DATE) AND
				(FTP2.FSCHANNEL_ID = @FSCHANNEL_ID) AND
				(FTP2.FSFTP_ID = '2')/* AND
				(ISNULL(FTP2.FNFILE_SIZE,0) <> ISNULL(FTP1.FNFILE_SIZE,0))*/
		 
		-------------------------
				
			UPDATE
				@TEMPTABLE
			SET
				_TOTALTODAY1 = (SELECT	SUM(FNFILE_SIZE/1000000)
								FROM	TBLOG_MASTERCONTROL_FTP 
								WHERE	(FSTIME = @DATE) AND
										(FSCHANNEL_ID = @FSCHANNEL_ID) AND
										(FSFTP_ID = '1')),
				_TOTALTODAY2 = (SELECT	SUM(FNFILE_SIZE/1000000)
								FROM	TBLOG_MASTERCONTROL_FTP 
								WHERE	(FSTIME = @DATE) AND
										(FSCHANNEL_ID = @FSCHANNEL_ID) AND
										(FSFTP_ID = '2')),
				_TOTALALL1	= @MAX1,
				_TOTALALL2	= @MAX2
		  
			SELECT * FROM @TEMPTABLE
		--END
END




GO
