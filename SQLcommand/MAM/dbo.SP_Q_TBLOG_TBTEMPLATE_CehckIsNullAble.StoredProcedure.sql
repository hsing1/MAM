USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE Proc [dbo].[SP_Q_TBLOG_TBTEMPLATE_CehckIsNullAble]
@FSTABLE_TYPE char(1) ,
@FSFILE_NO varchar(16) 

As

Declare @fntemplate_id int 
set @fntemplate_id=0

if @FSTABLE_TYPE = 'V' 
begin 
select @fntemplate_id = n.fntemplate_id_video_d from tblog_video m, TBDIRECTORIES n WHERE m.FNDIR_ID = n.FNDIR_ID 
AND FSFILE_NO = @FSFILE_NO
end 

if @FSTABLE_TYPE = 'A' 
begin 
select @fntemplate_id = n.fntemplate_id_audio from tblog_audio m, TBDIRECTORIES n WHERE m.FNDIR_ID = n.FNDIR_ID 
AND FSFILE_NO = @FSFILE_NO
end 

if @FSTABLE_TYPE = 'P' 
begin 
select @fntemplate_id = n.fntemplate_id_photo from tblog_photo m, TBDIRECTORIES n WHERE m.FNDIR_ID = n.FNDIR_ID 
AND FSFILE_NO = @FSFILE_NO
end 

if @FSTABLE_TYPE = 'D' 
begin 
select @fntemplate_id = n.fntemplate_id_doc from tblog_doc m, TBDIRECTORIES n WHERE m.FNDIR_ID = n.FNDIR_ID 
AND FSFILE_NO = @FSFILE_NO
end 




--select  FNID, FSFIELD, FSFIELD_NAME, FSFIELD_TYPE, FNFIELD_LENGTH, FSDESCRIPTION, FNORDER, FNOBJECT_WIDTH, FCISNULLABLE, FSDEFAULT, 'aaaaa' FSFIELD_VALUE,
--		FSCODE_ID, FSCODE_CNT, FSCODE_CTRL 
select FSFIELD
		
from tbtemplate_Fields 

where fntemplate_id = @fntemplate_id and FCISNULLABLE='N'

ORDER BY FNORDER



GO
