USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/12/26>
-- Description:   查詢區間播出節目表
-- =============================================
Create PROCEDURE [dbo].[SP_R_RPT_TBPGM_COMBINE_QUEUE_ZONE_FILE]
	@QUERY_KEY	VARCHAR(36)
AS
BEGIN
	SELECT [QUERY_KEY]
		  ,[QUERY_KEY_SUBID]
		  ,[QUERY_BY]
		  ,[QUERY_DATE]
      ,[節目編碼]
      ,[節目集數]
      ,[名稱]
      ,[主控編號]
      ,[檔案編號]
      ,[查詢起始日期]
      ,[查詢結束日期]
     FROM
		[MAM].[dbo].[RPT_TBPGM_COMBINE_QUEUE_ZONE_FILE]
	WHERE
		(QUERY_KEY = @QUERY_KEY)
	ORDER BY
		[節目編碼], [節目集數]
END

GO
