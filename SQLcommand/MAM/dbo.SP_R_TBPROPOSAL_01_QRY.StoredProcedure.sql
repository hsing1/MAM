USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/03/15>
-- Description:	For RPT_TBPROPOSAL_01(成案單), 列印前存入暫存資料庫
--				<傳入FSPRO_ID(成案單編號), QUERY_BY(使用者名稱), FSSESSION_ID( )
--				若不合法的查詢(沒有此FSSESSION_ID或是已超過有效使用期限), 回傳錯誤訊息並記錄於TBLOG_ERRORQRY,
--				若為合法的查詢, 有查到資料者回傳@QUERY_KEY, 否則回傳空字串. for成案單報表:RPT_TBPROPOSAL_01>
-- =============================================
CREATE PROCEDURE [dbo].[SP_R_TBPROPOSAL_01_QRY]
	@FSPRO_ID		NVARCHAR(12),	--相關查詢欄位
	@QUERY_BY		VARCHAR(50),	--查詢的使用者
	@FSSESSION_ID	VARCHAR(255)	--確認查詢權限的FSSESSION_ID
AS
BEGIN
	-------------------	以下, 宣告並賦予值給本次查詢的QUERY_KEY, 作為查詢篩選條件 -------------------<< 此段落可延用不需要修改 >>

	DECLARE @AVATIME_MIN SMALLINT = 5				--預設為五分鐘有效, 後續可以考慮改讀取設定檔
	DECLARE @QUERY_KEY UNIQUEIDENTIFIER = NEWID()	--請留意只有在SQL2008以上才可以在宣告時直接給值, by Dennis	
	DECLARE @TIME_NOW DATETIME = GETDATE()
	DECLARE @TIME_SESSION DATETIME = '1900/01/01'
	DECLARE @NODATA NVARCHAR(20) = 'Y'
	
	SELECT	@TIME_SESSION = ISNULL(FDLAST_ACCESS_TIME,'1900/01/01'),
			@NODATA	=  ISNULL(FSUSER_ID,'Y')		--當取不到資料時, @NODATA會為'Y'
	FROM TBUSER_SESSION
	WHERE FSSESSION_ID = @FSSESSION_ID
	
	IF(@NODATA = 'Y')
		BEGIN
			INSERT TBLOG_ERRORQRY(QRY_TYPE, QRY_ITEM, QRY_BY, QRY_DATE, NOTE)
			VALUES ('REPORT', 'RPT_TBPROPOSAL_01', @QUERY_BY, GETDATE(), 'SESSION_NOTEXIST')
			SELECT QUERY_KEY = 'ERROR:SESSION_NOTEXIST'
		END
	ELSE IF(@TIME_NOW > DATEADD(minute, @AVATIME_MIN, @TIME_SESSION))	--若現在時間(@TIME_NOW)已經超過了前一次動作時間(@TIME_SESSION)+允許時間(@AVATIME_MIN)
		BEGIN
		-------------------	以下, 檢查使用者的查詢權限有無過期, 過期的無效查詢紀錄於TBLOG_ERRORQRY, NOTE為'SESSION_TIMEOUT'
			INSERT TBLOG_ERRORQRY(QRY_TYPE, QRY_ITEM, QRY_BY, QRY_DATE, NOTE)
			VALUES ('REPORT', 'RPT_TBPROPOSAL_01', @QUERY_BY, GETDATE(), 'SESSION_TIMEOUT')
			SELECT QUERY_KEY = 'ERROR:SESSION_TIMEOUT'
		END
	ELSE
		BEGIN
		------------------- 以下, 將本次查詢結果存入temp資料表中,前三欄為必要欄位. -------------------<<TODO:以下須依照各資料表修改>>

			SET NOCOUNT ON;	--不顯示影響筆數, by Dennis

			INSERT
				RPT_TBPROPOSAL_01
			SELECT
				QUERY_KEY		= @QUERY_KEY,
				QUERY_BY		= @QUERY_BY,
				QUERY_DATE		= GETDATE(),
				成案單編號		= PRO.FSPRO_ID,										/*成案單編號*/
				成案種類_名稱	= CASE PRO.FCPRO_TYPE WHEN '1' THEN '新製節目' WHEN '2' THEN '續製節目' WHEN '3' THEN '衍生節目' ELSE '' END,	/*成案種類_名稱*/
				頻道別_名稱		= '('+ISNULL(CHA.FSCHANNEL_NAME, PRO.FSCHANNEL_ID)+')',		/*頻道別名稱	代碼:FSCHANNEL_ID*/
				節目名稱		= PRO.FSPROG_NAME,									/*節目名稱*/
				播出名稱		= PRO.FSPLAY_NAME,									/*播出名稱*/
				外文名稱		= PRO.FSNAME_FOR,									/*外文名稱*/
				製作目的_名稱	= ISNULL(OBJ.FSPROGOBJNAME,PRO.FSOBJ_ID),			/*製作目的		代碼:FSOBJ_ID*/
				節目編號		= PRO.FSPROG_ID,									/*節目編號*/
				每集時長		= PRO.FNLENGTH,										/*每集時長		(單位:分鐘)*/
				集次起			= PRO.FNBEG_EPISODE,								/*集次(起)*/
				集次迄			= PRO.FNEND_EPISODE,								/*集次(迄)*/
				節目內容簡述	= PRO.FSCONTENT,									/*節目內容簡述*/
				預訂播出頻道_名稱 = dbo.FN_Q_TBPROPOSAL_GETNAMELISTBYCODE(PRO.FSCHANNEL), /*預訂播出頻道	代碼:FSCHANNEL*/
				製作部門_名稱	= ISNULL(DEPT1.FSDEPTNAME,PRO.FSPRD_DEPT_ID),		/*製作部門		代碼:FSPRD_DEPT_ID*/
				付款部門_名稱	= ISNULL(DEPT2.FSDEPTNAME,PRO.FSPAY_DEPT_ID),		/*付款部門		代碼:FSPAY_DEPT_ID*/
				節目來源_名稱	= ISNULL(SRC.FSPROGSRCNAME,PRO.FSSRC_ID),			/*節目來源		代碼:FSSRC_ID*/
				節目型態_名稱	= ISNULL(ATTR.FSPROGATTRNAME,PRO.FSTYPE),			/*節目型態		代碼:FSTYPE*/
				目標觀眾_名稱	= ISNULL(AUD.FSPROGAUDNAME,PRO.FSAUD_ID),			/*目標觀眾		代碼:FSAUD_ID*/
				表現方式_名稱	= ISNULL(TYP.FSPROGTYPENAME,PRO.FSSHOW_TYPE),		/*表現方式		代碼:FSSHOW_TYPE*/
				主聲道_名稱		= ISNULL(LANG1.FSPROGLANGNAME,PRO.FSLANG_ID_MAIN),	/*主聲道		代碼:FSLANG_ID_MAIN*/
				副聲道_名稱		= ISNULL(LANG2.FSPROGLANGNAME,PRO.FSLANG_ID_SUB),	/*副聲道		代碼:FSLANG_ID_SUB*/
				預訂上檔日期	= CONVERT(NVARCHAR(10),FDSHOW_DATE,111),			/*預訂上檔日期*/
				節目分級_名稱	= ISNULL(GRAD.FSPROGGRADENAME,PRO.FSGRADE_ID),		/*節目分級		代碼:FSGRADE_ID*/
			    前會部門_名稱	= REPLACE(dbo.FN_Q_GETDEPTNAMELISTBYCODE(ISNULL(CAST(DEPT3.FSDEPTNAME AS VARCHAR(1000)),PRO.FSBEF_DEPT_ID)),';99;',';其他;'),/*前會部門		代碼:FSBEF_DEPT_ID 補上部門檔裡沒有的99「其他」欄位，前後會部門專用*/
			    後會部門_名稱	= REPLACE(dbo.FN_Q_GETDEPTNAMELISTBYCODE(ISNULL(CAST(DEPT4.FSDEPTNAME AS VARCHAR(1000)),PRO.FSAFT_DEPT_ID)),';99;',';其他;'),/*後會部門		代碼:FSAFT_DEPT_ID 補上部門檔裡沒有的99「其他」欄位，前後會部門專用*/
				外購類別大類_名稱 = ISNULL(BUY1.FSPROGBUYNAME,PRO.FSBUY_ID),		/*外購類別大類	代碼:FSBUY_ID*/
				外購類別細類_名稱 = ISNULL(BUY2.FSPROGBUYDNAME,PRO.FSBUYD_ID),		/*外購類別細類	代碼:FSBUYD_ID*/
				節目規格        = dbo.FN_Q_TBPROPOSAL_GETNAMELISTBYCODE_PROGSPEC(PRO.FSPROGSPEC), /*節目規格	代碼:FSPROGSPEC*/
				列印人員        = ISNULL(USERS.FSUSER_ChtName,@QUERY_BY),		    /*列印人員*/
				分集備註		= PRO.FSENOTE										/*分集備註*/
				
			FROM TBPROPOSAL AS PRO
				LEFT JOIN TBZCHANNEL AS CHA ON (CHA.FSCHANNEL_ID = PRO.FSCHANNEL_ID)		/*頻道別代碼檔		=> 頻道別名稱*/
				LEFT JOIN TBZPROGOBJ AS OBJ ON (OBJ.FSPROGOBJID = PRO.FSOBJ_ID)				/*製作目的代碼檔	=> 製作目的*/
				LEFT JOIN TBZDEPT AS DEPT1 ON (DEPT1.FSDEPTID = PRO.FSPRD_DEPT_ID)			/*部門檔			=> 製作部門*/
				LEFT JOIN TBZDEPT AS DEPT2 ON (DEPT2.FSDEPTID = PRO.FSPAY_DEPT_ID)			/*部門檔			=> 付款部門*/
				LEFT JOIN TBZPROGSRC AS SRC ON (SRC.FSPROGSRCID = PRO.FSSRC_ID)				/*節目來源代碼檔	=> 節目來源*/
				LEFT JOIN TBZPROGATTR AS ATTR ON (ATTR.FSPROGATTRID = PRO.FSTYPE)			/*內容屬性代碼檔	=> 節目型態*/
				LEFT JOIN TBZPROGAUD AS AUD	ON (AUD.FSPROGAUDID = PRO.FSAUD_ID)				/*目標觀眾代碼檔	=> 目標觀眾*/
				LEFT JOIN TBZPROGTYPE AS TYP ON (TYP.FSPROGTYPEID = PRO.FSSHOW_TYPE)		/*表現方式代碼檔	=> 表現方式*/
				LEFT JOIN TBZPROGLANG AS LANG1 ON (LANG1.FSPROGLANGID = PRO.FSLANG_ID_MAIN)	/*語言代碼檔		=> 主聲道*/
				LEFT JOIN TBZPROGLANG AS LANG2 ON (LANG2.FSPROGLANGID = PRO.FSLANG_ID_SUB)	/*語言代碼檔		=> 副聲道*/
				LEFT JOIN TBZPROGGRADE AS GRAD ON (GRAD.FSPROGGRADEID = PRO.FSGRADE_ID)		/*節目分級代碼檔	=> 節目分級*/
				LEFT JOIN TBZDEPT AS DEPT3 ON (DEPT3.FSDEPTID = PRO.FSBEF_DEPT_ID)			/*部門檔			=> 前會部門*/
				LEFT JOIN TBZDEPT AS DEPT4 ON (DEPT4.FSDEPTID = PRO.FSAFT_DEPT_ID)			/*部門檔			=> 後會部門*/
				LEFT JOIN TBZPROGBUY AS BUY1 ON (BUY1.FSPROGBUYID = PRO.FSBUY_ID)			/*外購類別代碼檔	=> 外購類別大類*/
				LEFT JOIN TBZPROGBUYD AS BUY2 ON (BUY2.FSPROGBUYID = PRO.FSBUY_ID) 
												AND (BUY2.FSPROGBUYDID =PRO.FSBUYD_ID)		/*外購類別細項代碼檔 => 外購類別細類*/													
			 	LEFT JOIN TBUSERS AS USERS ON (USERS.FSUSER_ID = @QUERY_BY)					/*使用者檔		    =>  列印人員*/
			 				
			 WHERE
				(FSPRO_ID = @FSPRO_ID)
				--OR (@FSPRO_ID = '')	--為避免資料浮增, 暫時不開放query全部資料
				
		-------------------	以下, 儲存完畢後檢查有無新增資料, 沒有的話回傳空字串, 否則回傳@QUERY_KEY -------------------<<TODO:以下僅須修改資料表名稱>>

			IF EXISTS (SELECT TOP 1 QUERY_KEY 
						FROM RPT_TBPROPOSAL_01																		----<<TODO:改資料表名稱
						WHERE QUERY_KEY = @QUERY_KEY)
				BEGIN
					SELECT QUERY_KEY = @QUERY_KEY
				END
			ELSE
				BEGIN
					SELECT QUERY_KEY = ''
				END		
		END	
END


GO
