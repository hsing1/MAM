USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_U_TBSTAFF_EXTERNAL]
	@FSSTAFFID	varchar(5) ,
	@FSNAME		varchar(100) ,
	@FSENAME	varchar(100) ,
	@FSTEL		varchar(50) ,
	@FSFAX		varchar(20) ,
	@FSEMAIL	varchar(50) ,
	@FSADDRESS	varchar(150) ,
	@FSWEBSITE	varchar(200) ,
	@FSINTRO	text ,
	@FSEINTRO	text ,
	@FSMEMO		varchar(200)

As
--UPDATE [PTSProg].[PTSProgram].[dbo].TBSTAFF
--SET FSNAME=@FSNAME,FSENAME=@FSENAME,FSTEL=@FSTEL,FSFAX=@FSFAX,FSEMAIL=@FSEMAIL,FSADDRESS=@FSADDRESS,FSWEBSITE=@FSWEBSITE,FSINTRO=@FSINTRO,FSEINTRO=@FSEINTRO,FSMEMO=@FSMEMO,FSUPDUSER='01065',FDUPDDATE=GETDATE()
--WHERE  FSSTAFFID=@FSSTAFFID

--Dennis.Wen:寫法改嚴謹一點

IF EXISTS(	SELECT	*
			FROM	[PTSProg].[PTSProgram].[dbo].TBSTAFF
			WHERE	(FSNAME = @FSNAME) AND (FSSTAFFID <> @FSSTAFFID))
	BEGIN
		SELECT RESULT = 'ERROR:已存在此中文姓名'
	END
ELSE
	BEGIN
		BEGIN TRY
			UPDATE [PTSProg].[PTSProgram].[dbo].TBSTAFF
			SET		FSNAME		= @FSNAME,
					FSENAME		= case when @FSENAME ='' then null else @FSENAME end,
					FSTEL		= case when @FSTEL ='' then null else FSTEL end,
					FSFAX		= case when @FSFAX ='' then null else FSFAX end,
					FSEMAIL		= case when @FSEMAIL ='' then null else FSEMAIL end,					
					FSADDRESS	= case when @FSADDRESS ='' then null else FSADDRESS end,
					FSWEBSITE	= case when @FSWEBSITE ='' then null else FSWEBSITE end,
					FSINTRO		= @FSINTRO,
					FSEINTRO	= @FSEINTRO,
					FSMEMO		= case when @FSMEMO ='' then null else FSMEMO end,					
					FSUPDUSER	= '01065',
					FDUPDDATE	= GETDATE()
			WHERE
					(FSSTAFFID = @FSSTAFFID)
			
			IF (@@ROWCOUNT = 0)
				BEGIN
					SELECT RESULT = 'ERROR:沒有受影響的資料列'
				END
			ELSE
				BEGIN
					SELECT RESULT = ''
				END
		END TRY
		BEGIN CATCH
			SELECT RESULT = 'ERROR:' + ERROR_MESSAGE()
		END CATCH
	END

GO
