USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

-- Author:        <Mike.Lin>

-- Create date:   <2012/10/25>

-- Description:   新增節目與短帶連結資料

-- =============================================


CREATE Proc [dbo].[SP_I_TBPGM_PROG_PROMO_LINK]

@FSPROG_ID	char(7),
@FNSEQNO	int,
@FSPROMO_ID	varchar(12),
@FNNO		int,
@FSCREATED_BY	varchar(50),
@FSUPDATED_BY	varchar(50),
@FSCHANNEL_ID	varchar(2)
As
Insert Into dbo.TBPGM_PROG_PROMO_LINK(FSPROG_ID,FNSEQNO,FSPROMO_ID,FNNO,FSCREATED_BY,
FDCREATED_DATE,FSUPDATED_BY,FDUPDATED_DATE,FSCHANNEL_ID)
Values(@FSPROG_ID,@FNSEQNO,@FSPROMO_ID,@FNNO,@FSCREATED_BY,
GETDATE(),@FSUPDATED_BY,GETDATE(),@FSCHANNEL_ID)
GO
