USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBPGM_SAVE_TAPE_LIST](
	[FSPROG_ID] [char](7) NOT NULL,
	[FNSEQNO] [int] NOT NULL,
	[FDDATE] [date] NOT NULL,
	[FSCHANNEL_ID] [varchar](2) NOT NULL,
	[FSBEG_TIME] [varchar](4) NULL,
	[FSEND_TIME] [varchar](4) NULL,
	[FSPROG_NAME] [nvarchar](50) NULL,
	[FNEPISODE] [int] NULL,
	[FSTAPENO] [varchar](16) NULL,
	[FNSAVE_NO] [int] NOT NULL,
	[FSSAVE_PROG_ID] [char](7) NULL,
	[FNSAVE_EPISODE] [int] NULL,
	[FSSAVE_TAPENO] [varchar](16) NULL,
	[FSSAVE_LENGTH] [varchar](11) NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
 CONSTRAINT [PK_TBPGM_SAVE_TAPE_LIST] PRIMARY KEY CLUSTERED 
(
	[FSPROG_ID] ASC,
	[FNSEQNO] ASC,
	[FDDATE] ASC,
	[FSCHANNEL_ID] ASC,
	[FNSAVE_NO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目編碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_SAVE_TAPE_LIST', @level2type=N'COLUMN',@level2name=N'FSPROG_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'播映序號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_SAVE_TAPE_LIST', @level2type=N'COLUMN',@level2name=N'FNSEQNO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_SAVE_TAPE_LIST', @level2type=N'COLUMN',@level2name=N'FDDATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'頻道' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_SAVE_TAPE_LIST', @level2type=N'COLUMN',@level2name=N'FSCHANNEL_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'起始時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_SAVE_TAPE_LIST', @level2type=N'COLUMN',@level2name=N'FSBEG_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'結束時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_SAVE_TAPE_LIST', @level2type=N'COLUMN',@level2name=N'FSEND_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_SAVE_TAPE_LIST', @level2type=N'COLUMN',@level2name=N'FSPROG_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'集數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_SAVE_TAPE_LIST', @level2type=N'COLUMN',@level2name=N'FNEPISODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'檔案編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_SAVE_TAPE_LIST', @level2type=N'COLUMN',@level2name=N'FSTAPENO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'救命帶清單序號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_SAVE_TAPE_LIST', @level2type=N'COLUMN',@level2name=N'FNSAVE_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'救命帶節目編碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_SAVE_TAPE_LIST', @level2type=N'COLUMN',@level2name=N'FSSAVE_PROG_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'救命帶節目集數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_SAVE_TAPE_LIST', @level2type=N'COLUMN',@level2name=N'FNSAVE_EPISODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'救命帶檔案編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_SAVE_TAPE_LIST', @level2type=N'COLUMN',@level2name=N'FSSAVE_TAPENO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'救命帶長度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_SAVE_TAPE_LIST', @level2type=N'COLUMN',@level2name=N'FSSAVE_LENGTH'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_SAVE_TAPE_LIST', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_SAVE_TAPE_LIST', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'描述', @value=N'救命帶清單表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_SAVE_TAPE_LIST'
GO
