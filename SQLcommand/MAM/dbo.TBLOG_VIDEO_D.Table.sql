USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBLOG_VIDEO_D](
	[FSFILE_NO] [char](16) NOT NULL,
	[FNSEQ_NO] [int] NOT NULL,
	[FSSUBJECT_ID] [char](12) NOT NULL,
	[FSKEYFRAME_NO] [varchar](25) NULL,
	[FSTYPE] [char](1) NOT NULL,
	[FSID] [varchar](11) NOT NULL,
	[FNEPISODE] [smallint] NOT NULL,
	[FSSUPERVISOR] [char](1) NULL,
	[FSTITLE] [nvarchar](100) NULL,
	[FSDESCRIPTION] [nvarchar](200) NULL,
	[FSARC_ID] [char](12) NULL,
	[FCFILE_STATUS] [char](1) NULL,
	[FSCHANGE_FILE_NO] [varchar](20) NULL,
	[FSOLD_FILE_NAME] [nvarchar](100) NULL,
	[FNDIR_ID] [bigint] NULL,
	[FSCHANNEL_ID] [char](2) NULL,
	[FSBEG_TIMECODE] [char](11) NULL,
	[FSEND_TIMECODE] [char](11) NULL,
	[FCFROM] [char](1) NULL,
	[FSTAPE_ID] [varchar](50) NULL,
	[FSTRANS_FROM] [varchar](100) NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
	[FSUPDATED_BY] [varchar](50) NULL,
	[FDUPDATED_DATE] [datetime] NULL,
	[FSATTRIBUTE1] [nvarchar](max) NULL,
	[FSATTRIBUTE2] [nvarchar](max) NULL,
	[FSATTRIBUTE3] [nvarchar](max) NULL,
	[FSATTRIBUTE4] [nvarchar](max) NULL,
	[FSATTRIBUTE5] [nvarchar](max) NULL,
	[FSATTRIBUTE6] [nvarchar](max) NULL,
	[FSATTRIBUTE7] [nvarchar](max) NULL,
	[FSATTRIBUTE8] [nvarchar](max) NULL,
	[FSATTRIBUTE9] [nvarchar](max) NULL,
	[FSATTRIBUTE10] [nvarchar](max) NULL,
	[FSATTRIBUTE11] [nvarchar](max) NULL,
	[FSATTRIBUTE12] [nvarchar](max) NULL,
	[FSATTRIBUTE13] [nvarchar](max) NULL,
	[FSATTRIBUTE14] [nvarchar](max) NULL,
	[FSATTRIBUTE15] [nvarchar](max) NULL,
	[FSATTRIBUTE16] [nvarchar](max) NULL,
	[FSATTRIBUTE17] [nvarchar](max) NULL,
	[FSATTRIBUTE18] [nvarchar](max) NULL,
	[FSATTRIBUTE19] [nvarchar](max) NULL,
	[FSATTRIBUTE20] [nvarchar](max) NULL,
	[FSATTRIBUTE21] [nvarchar](max) NULL,
	[FSATTRIBUTE22] [nvarchar](max) NULL,
	[FSATTRIBUTE23] [nvarchar](max) NULL,
	[FSATTRIBUTE24] [nvarchar](max) NULL,
	[FSATTRIBUTE25] [nvarchar](max) NULL,
	[FSATTRIBUTE26] [nvarchar](max) NULL,
	[FSATTRIBUTE27] [nvarchar](max) NULL,
	[FSATTRIBUTE28] [nvarchar](max) NULL,
	[FSATTRIBUTE29] [nvarchar](max) NULL,
	[FSATTRIBUTE30] [nvarchar](max) NULL,
	[FSATTRIBUTE31] [nvarchar](max) NULL,
	[FSATTRIBUTE32] [nvarchar](max) NULL,
	[FSATTRIBUTE33] [nvarchar](max) NULL,
	[FSATTRIBUTE34] [nvarchar](max) NULL,
	[FSATTRIBUTE35] [nvarchar](max) NULL,
	[FSATTRIBUTE36] [nvarchar](max) NULL,
	[FSATTRIBUTE37] [nvarchar](max) NULL,
	[FSATTRIBUTE38] [nvarchar](max) NULL,
	[FSATTRIBUTE39] [nvarchar](max) NULL,
	[FSATTRIBUTE40] [nvarchar](max) NULL,
	[FSATTRIBUTE41] [nvarchar](max) NULL,
	[FSATTRIBUTE42] [nvarchar](max) NULL,
	[FSATTRIBUTE43] [nvarchar](max) NULL,
	[FSATTRIBUTE44] [nvarchar](max) NULL,
	[FSATTRIBUTE45] [nvarchar](max) NULL,
	[FSATTRIBUTE46] [nvarchar](max) NULL,
	[FSATTRIBUTE47] [nvarchar](max) NULL,
	[FSATTRIBUTE48] [nvarchar](max) NULL,
	[FSATTRIBUTE49] [nvarchar](max) NULL,
	[FSATTRIBUTE50] [nvarchar](max) NULL,
 CONSTRAINT [PK_TBLOG_VIDEO_D_1] PRIMARY KEY CLUSTERED 
(
	[FSFILE_NO] ASC,
	[FNSEQ_NO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
CREATE NONCLUSTERED INDEX [INDEX_FCFILE_STATUS] ON [dbo].[TBLOG_VIDEO_D]
(
	[FCFILE_STATUS] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDEX_FDCREATED_DATE] ON [dbo].[TBLOG_VIDEO_D]
(
	[FDCREATED_DATE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
CREATE NONCLUSTERED INDEX [INDEX_FSID_FNEPISODE] ON [dbo].[TBLOG_VIDEO_D]
(
	[FSID] ASC,
	[FNEPISODE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
CREATE NONCLUSTERED INDEX [INDEX_FSTYPE] ON [dbo].[TBLOG_VIDEO_D]
(
	[FSTYPE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TBLOG_VIDEO_D] ADD  CONSTRAINT [DF_TBLOG_VIDEO_D_FSSUPERVISOR]  DEFAULT ('N') FOR [FSSUPERVISOR]
GO
ALTER TABLE [dbo].[TBLOG_VIDEO_D] ADD  CONSTRAINT [DF_TBLOG_VIDEO_D_FSTITLE]  DEFAULT ('') FOR [FSTITLE]
GO
ALTER TABLE [dbo].[TBLOG_VIDEO_D] ADD  CONSTRAINT [DF_TBLOG_VIDEO_D_FSDESCRIPTION]  DEFAULT ('') FOR [FSDESCRIPTION]
GO
ALTER TABLE [dbo].[TBLOG_VIDEO_D] ADD  CONSTRAINT [DF_TBLOG_VIDEO_D_FSTAPE_ID]  DEFAULT ('') FOR [FSTAPE_ID]
GO
ALTER TABLE [dbo].[TBLOG_VIDEO_D] ADD  CONSTRAINT [DF_TBLOG_VIDEO_D_FSTRANS_FROM]  DEFAULT ('') FOR [FSTRANS_FROM]
GO
ALTER TABLE [dbo].[TBLOG_VIDEO_D] ADD  CONSTRAINT [DF_TBLOG_VIDEO_D_FSUPDATED_BY]  DEFAULT ('') FOR [FSUPDATED_BY]
GO
ALTER TABLE [dbo].[TBLOG_VIDEO_D] ADD  CONSTRAINT [DF_TBLOG_VIDEO_D_FDUPDATED_DATE]  DEFAULT (getdate()) FOR [FDUPDATED_DATE]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'影片檔案編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSFILE_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'序號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FNSEQ_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'活動代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSSUBJECT_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'影片類型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSTYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目或PROMO代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'子集ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FNEPISODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否原成案單位主管簽核' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSSUPERVISOR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'標題/單元' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSTITLE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'畫面描述/詳細資訊' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSDESCRIPTION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'轉出數位檔類型代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSARC_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'檔案狀態' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FCFILE_STATUS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'置換檔案名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSCHANGE_FILE_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'原始檔案名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSOLD_FILE_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'所屬Queue代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FNDIR_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'所屬頻道代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSCHANNEL_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time Code起' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSBEG_TIMECODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time Code迄' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSEND_TIMECODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'動態欄位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSATTRIBUTE1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'動態欄位類別代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSATTRIBUTE2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'動態欄位類別代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSATTRIBUTE3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'動態欄位類別代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSATTRIBUTE4'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'動態欄位類別代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSATTRIBUTE5'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'動態欄位類別代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSATTRIBUTE6'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'動態欄位類別代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSATTRIBUTE7'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'動態欄位類別代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSATTRIBUTE8'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'動態欄位類別代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSATTRIBUTE9'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'動態欄位類別代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSATTRIBUTE10'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自定欄位String1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSATTRIBUTE11'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自定欄位String2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSATTRIBUTE12'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自定欄位String3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSATTRIBUTE13'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自定欄位String4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSATTRIBUTE14'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自定欄位String5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSATTRIBUTE15'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自定欄位Integer1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSATTRIBUTE16'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自定欄位Integer2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSATTRIBUTE17'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自定欄位Integer3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSATTRIBUTE18'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自定欄位Integer4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSATTRIBUTE19'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自定欄位Integer5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBLOG_VIDEO_D', @level2type=N'COLUMN',@level2name=N'FSATTRIBUTE20'
GO
