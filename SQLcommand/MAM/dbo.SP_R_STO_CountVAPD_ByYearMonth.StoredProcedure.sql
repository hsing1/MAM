USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<kyle.Lin>
-- Create date: <2011/11/14>
-- Description:	For RPT_TBARCHIVE_02(入庫單),依年月查詢數量
-- =============================================
CREATE PROCEDURE [dbo].[SP_R_STO_CountVAPD_ByYearMonth]
	@BeginYear nvarchar(4),	
	@BeginMonth		nvarchar(2),	
	@EndYear nvarchar(4),
    @EndMonth nvarchar(2),
    @CREATED_BY varchar(50)	
AS
BEGIN
  --declare @BeginYear nvarchar(4)='2010'
  --declare @BeginMonth nvarchar(2)='09'
  --declare @EndYear nvarchar(4)='2011'
  --declare @EndMonth nvarchar(2)='11'
  declare @BeginDate datetime = @BeginYear+'/'+@BeginMonth+'/01'
  declare @EndDate datetime = @EndYear+'/'+@EndMonth+'/01'
  declare @i int=0
  declare @VCount int =99999
  declare @ACount int=99999
  declare @PCount int=99999
  declare @DCount int=99999
  declare @VPCount int=9999
  declare @QUERY_BY nvarchar(50)=''
  declare @CountTable Table(
  [Year] nvarchar (4),
  [Month] nvarchar (2),
  [VCount] int,
  [ACount] int,
  [PCount] int,
  [DCount] int,
  [VPCount] int,
  [QUERY_BY] nvarchar (50)
  )
  while DATEADD(MONTH,@i,@BeginDate)<=@EndDate
  begin
  set @VCount=
  (select COUNT(*) from [MAM].[dbo].[TBLOG_VIDEO]
	  where ([FDCREATED_DATE] between DATEADD(MONTH,@i,@BeginDate) and DATEADD(MONTH,@i+1,@BeginDate)) and [FCFILE_STATUS]='Y' and [FSTYPE]='G')
  set @ACount=
  (select COUNT(*) from [MAM].[dbo].[TBLOG_AUDIO]
	  where ([FDCREATED_DATE] between DATEADD(MONTH,@i,@BeginDate) and DATEADD(MONTH,@i+1,@BeginDate)) and [FCFILE_STATUS]='Y')
  set @PCount=
	  (select COUNT(*) from [MAM].[dbo].[TBLOG_PHOTO]
	  where ([FDCREATED_DATE] between DATEADD(MONTH,@i,@BeginDate) and DATEADD(MONTH,@i+1,@BeginDate)) and [FCFILE_STATUS]='Y')
  set @DCount=
	  (select COUNT(*) from [MAM].[dbo].[TBLOG_DOC]
	  where ([FDCREATED_DATE] between DATEADD(MONTH,@i,@BeginDate) and DATEADD(MONTH,@i+1,@BeginDate)) and [FCFILE_STATUS]='Y')
  set @VPCount=
  (select COUNT(*) from [MAM].[dbo].[TBLOG_VIDEO]
	  where ([FDCREATED_DATE] between DATEADD(MONTH,@i,@BeginDate) and DATEADD(MONTH,@i+1,@BeginDate)) and [FCFILE_STATUS]='Y' and [FSTYPE]='P')
	  
  set @QUERY_BY = (select FSUSER_ChtName from [MAM].[dbo].[TBUSERS] where [FSUSER_ID]=@CREATED_BY)
  
  insert @CountTable values(CONVERT(nvarchar, DATEPART(YEAR,DATEADD(MONTH,@i,@BeginDate))),DATEPART(MONTH,DATEADD(MONTH,@i,@BeginDate)),@VCount,@ACount,@PCount,@DCount,@VPCount,@QUERY_BY)
  set @i+=1
  
  end
  
  select * From @CountTable
END


GO
