USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fnGET_TOTAL_SECONDs]()
RETURNS	BIGINT
AS
BEGIN 

	DECLARE @fnTOTAL_SECONDs BIGINT
	SET @fnTOTAL_SECONDs = 0
	
	 
	select 
		@fnTOTAL_SECONDs = SUM(
		CONVERT(BIGINT,CONVERT(INT,SUBSTRING([dbo].[CaculateDuration](FSBEG_TIMECODE,FSEND_TIMECODE),1,2)) * 60 *60 *30)+ 
		CONVERT(BIGINT,CONVERT(INT,SUBSTRING([dbo].[CaculateDuration](FSBEG_TIMECODE,FSEND_TIMECODE),4,2)) * 60 *30)+
		CONVERT(BIGINT,CONVERT(INT,SUBSTRING([dbo].[CaculateDuration](FSBEG_TIMECODE,FSEND_TIMECODE),7,2)) * 30) +
		CONVERT(BIGINT,CONVERT(INT,SUBSTRING([dbo].[CaculateDuration](FSBEG_TIMECODE,FSEND_TIMECODE),10,2)))
		)
	from 
		[dbo].[TBLOG_VIDEO]
	WHERE FCFILE_STATUS IN('T','Y') AND
	FDUPDATED_DATE >= '2014-12-01 00:00:00' and
	FDUPDATED_DATE <= '2014-12-31 00:00:00'

	RETURN @fnTOTAL_SECONDs
	--select count(*) from 
	--[dbo].[TBLOG_VIDEO]
	--WHERE FCFILE_STATUS IN('T','Y') AND FSTYPE='G' AND
	--FDCREATED_DATE >= '2014-12-01 00:00:00'
END
GO
