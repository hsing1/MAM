USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_I_RPT_DATE_LST]

@QUERY_KEY	uniqueidentifier,
@QUERY_KEY_SUBID	uniqueidentifier,
@QUERY_BY	varchar(50),
@序號	varchar(8),
@時間類型	varchar(1),
@節目編碼	varchar(7),
@播映序號	int,
@名稱	varchar(50),
@短帶編碼	varchar(11),
@頻道	varchar(50),
@節目集數	int,
@段數	int,
@支數	int,
@播出日期	date,
@播出時間	nvarchar(11),
@長度	nvarchar(11),
@影像編號	varchar(16),
@主控編號	varchar(8),
@資料類型	varchar(1),
@備註	varchar(200),
@FSSIGNAL	varchar(10),
@FSRAN	varchar(1),
@FNLIVE	varchar(1)
As
Insert Into dbo.RPT_DATE_LST
(QUERY_KEY,QUERY_KEY_SUBID,QUERY_BY,
QUERY_DATE,序號,時間類型,節目編碼,
播映序號,名稱,短帶編碼,
頻道,節目集數,段數,支數,播出日期,播出時間,
長度,影像編號,主控編號,資料類型,備註,FSSIGNAL,FSRAN,FNLIVE)
Values(@QUERY_KEY,@QUERY_KEY_SUBID,@QUERY_BY,
GETDATE(),@序號,@時間類型,@節目編碼,
@播映序號,@名稱,@短帶編碼,
@頻道,@節目集數,@段數,@支數,@播出日期,@播出時間,
@長度,@影像編號,@主控編號,@資料類型,@備註,@FSSIGNAL,@FSRAN,@FNLIVE)
GO
