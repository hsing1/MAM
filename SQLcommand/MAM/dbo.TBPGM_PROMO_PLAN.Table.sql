USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBPGM_PROMO_PLAN](
	[FNNO] [int] NOT NULL,
	[FSPROG_ID] [char](7) NOT NULL,
	[FSPROG_NAME] [nvarchar](50) NOT NULL,
	[FCSOURCETYPE] [varchar](2) NULL,
	[FCSOURCE] [varchar](20) NULL,
	[FSPLAY_COUNT_MAX] [int] NOT NULL,
	[FSPLAY_COUNT_MIN] [int] NOT NULL,
	[FNPROMO_TYPEA] [int] NULL,
	[FNPROMO_TYPEB] [int] NULL,
	[FNPROMO_TYPEC] [int] NULL,
	[FNPROMO_TYPED] [int] NULL,
	[FNPROMO_TYPEE] [int] NULL,
	[FSPROMO_BEG_DATE] [date] NOT NULL,
	[FSPROMO_END_DATE] [date] NOT NULL,
	[FSWEEK] [char](7) NULL,
	[FSMEMO] [nvarchar](50) NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
	[FSUPDATED_BY] [varchar](50) NULL,
	[FDUPDATED_DATE] [datetime] NULL,
 CONSTRAINT [ TBPGMPROMO_PLAN] PRIMARY KEY CLUSTERED 
(
	[FNNO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'序號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_PLAN', @level2type=N'COLUMN',@level2name=N'FNNO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目編碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_PLAN', @level2type=N'COLUMN',@level2name=N'FSPROG_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_PLAN', @level2type=N'COLUMN',@level2name=N'FSPROG_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'來源代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_PLAN', @level2type=N'COLUMN',@level2name=N'FCSOURCETYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'來源名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_PLAN', @level2type=N'COLUMN',@level2name=N'FCSOURCE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最多可排次數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_PLAN', @level2type=N'COLUMN',@level2name=N'FSPLAY_COUNT_MAX'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最小可排次數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_PLAN', @level2type=N'COLUMN',@level2name=N'FSPLAY_COUNT_MIN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A時段可排次數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_PLAN', @level2type=N'COLUMN',@level2name=N'FNPROMO_TYPEA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'B時段可排次數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_PLAN', @level2type=N'COLUMN',@level2name=N'FNPROMO_TYPEB'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'C時段可排次數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_PLAN', @level2type=N'COLUMN',@level2name=N'FNPROMO_TYPEC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'D時段可排次數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_PLAN', @level2type=N'COLUMN',@level2name=N'FNPROMO_TYPED'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'E時段可排次數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_PLAN', @level2type=N'COLUMN',@level2name=N'FNPROMO_TYPEE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'起始日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_PLAN', @level2type=N'COLUMN',@level2name=N'FSPROMO_BEG_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'結束日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_PLAN', @level2type=N'COLUMN',@level2name=N'FSPROMO_END_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'星期代碼(1100000表示星期一二可播)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_PLAN', @level2type=N'COLUMN',@level2name=N'FSWEEK'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'備註' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_PLAN', @level2type=N'COLUMN',@level2name=N'FSMEMO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_PLAN', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_PLAN', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_PLAN', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_PLAN', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'描述', @value=N'預約破口資源表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_PLAN'
GO
