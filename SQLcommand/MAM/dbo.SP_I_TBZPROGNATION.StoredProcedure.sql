USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		林冠宇
-- Create date: 2012/12/17
-- Description:	新增國家來源代碼
-- =============================================
create PROCEDURE [dbo].[SP_I_TBZPROGNATION]
      @FSNATION_ID nvarchar(100),
      @FSNATION_NAME nvarchar(100),
      --@FDCREATE_DATE nvarchar(100),
      @FSCREATE_BY nvarchar(100),
      --@FDUPDATE_DATE;
      @FSUPDATE_BY nvarchar(100),
      @FBISENABLE nvarchar(100),
      @FSSORT nvarchar(100)
AS
BEGIN
	insert into MAM.dbo.TBZPROGNATION 
		   (FSNATION_ID,FSNATION_NAME,FDCREATE_DATE,FSCREATE_BY,FDUPDATE_DATE,FSUPDATE_BY,FBISENABLE,FSSORT) 
	values (@FSNATION_ID,@FSNATION_NAME,GETDATE(),@FSCREATE_BY,GETDATE(),@FSUPDATE_BY,@FBISENABLE,@FSSORT)
END

GO
