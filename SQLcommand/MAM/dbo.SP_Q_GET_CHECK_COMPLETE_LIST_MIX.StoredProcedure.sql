USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        
-- Create date: 
-- Description: 取得審核完成要搬移到已審區的檔案LIST
-- Update desc:    <2016/05/26><Mike>加入註解。
-- =============================================
CREATE PROC [dbo].[SP_Q_GET_CHECK_COMPLETE_LIST_MIX]

AS
select distinct A.FSPROG_ID as FSID,A.FNEPISODE,D.FSFILE_NO,D.FSVIDEO_ID,B.FCFILE_STATUS,C.FCCHECK_STATUS,D.FCSTATUS,C.FSBRO_ID
from tbpgm_combine_queue A
left join TBLOG_Video B on A.FSVIDEO_ID=B.FSVIDEO_PROG and B.FCFILE_STATUS not in('D','X','F')
left join tbbroadcast C on B.FSBRO_ID=C.FSBRO_ID and C.FCCHECK_STATUS IN ('G','T','U')
left join TBLOG_Video_HD_MC D on B.FSFILE_NO=D.FSFILE_NO and D.FCSTATUS='O'
inner join TBZCHANNEL E on A.FSCHANNEL_ID=E.FSCHANNEL_ID and E.FSCHANNEL_TYPE='002'
where 
A.FDDATE>=getdate()-1 and A.FDDATE<=getdate()+14
and B.FCFILE_STATUS in ('B','S','R','T','Y')
and C.FCCHECK_STATUS IN ('G','T','U')
and D.FCSTATUS='O'
union
select distinct A.FSPROMO_ID AS FSID,0,D.FSFILE_NO,D.FSVIDEO_ID,B.FCFILE_STATUS,C.FCCHECK_STATUS,D.FCSTATUS,C.FSBRO_ID
from TBPGM_ARR_PROMO A
left join TBLOG_Video B on A.FSVIDEO_ID=B.FSVIDEO_PROG and B.FCFILE_STATUS not in('D','X','F')
left join tbbroadcast C on B.FSBRO_ID=C.FSBRO_ID and C.FCCHECK_STATUS IN ('G','T','U')
left join TBLOG_Video_HD_MC D on B.FSFILE_NO=D.FSFILE_NO and D.FCSTATUS='O'
inner join TBZCHANNEL E on A.FSCHANNEL_ID=E.FSCHANNEL_ID and E.FSCHANNEL_TYPE='002'
where 
A.FDDATE>=getdate()-1 and A.FDDATE<=getdate()+14
and B.FCFILE_STATUS in ('B','S','R','T','Y')
and C.FCCHECK_STATUS IN ('G','T','U')
and D.FCSTATUS='O'

GO
