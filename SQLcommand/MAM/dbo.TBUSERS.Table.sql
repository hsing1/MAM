USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBUSERS](
	[FSUSER_ID] [nvarchar](50) NOT NULL,
	[FSUSER] [nvarchar](50) NULL,
	[FSPASSWD] [varchar](255) NULL,
	[FSUSER_ChtName] [nvarchar](50) NULL,
	[FSUSER_TITLE] [nvarchar](50) NULL,
	[FSDEPT] [nvarchar](50) NULL,
	[FSEMAIL] [nvarchar](50) NULL,
	[FCACTIVE] [nvarchar](1) NULL,
	[FSDESCRIPTION] [nvarchar](max) NULL,
	[FCSECRET] [char](1) NULL,
	[FSCHANNEL_ID] [char](2) NULL,
	[FNUpperDept_ID] [int] NULL,
	[FNDep_ID] [int] NULL,
	[FNGroup_ID] [int] NULL,
	[FNTreeViewDept] [int] NULL,
	[FSUpperDept_ChtName] [nvarchar](60) NULL,
	[FNTreeViewDept_ChtName] [nvarchar](500) NULL,
	[FSDep_ChtName] [nvarchar](60) NULL,
	[FSGroup_ChtName] [nvarchar](60) NULL,
	[FSIs_Main_Title] [nvarchar](1) NULL,
	[FSIs_Supervisor] [nvarchar](1) NULL,
	[FSCREATED_BY] [varchar](50) NULL,
	[FDCREATED_DATE] [datetime] NULL,
	[FSUPDATED_BY] [varchar](50) NULL,
	[FDUPDATED_DATE] [datetime] NULL,
 CONSTRAINT [PK_TBUSERS] PRIMARY KEY CLUSTERED 
(
	[FSUSER_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[TBUSERS] ADD  CONSTRAINT [DF_TBUSERS_FSPASSWD]  DEFAULT ('\\') FOR [FSPASSWD]
GO
ALTER TABLE [dbo].[TBUSERS] ADD  CONSTRAINT [DF_TBUSERS_FNTreeViewDept]  DEFAULT ((9999)) FOR [FNTreeViewDept]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用者的ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS', @level2type=N'COLUMN',@level2name=N'FSUSER_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用者的AD帳號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS', @level2type=N'COLUMN',@level2name=N'FSUSER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用者的密碼"\\"為AD密碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS', @level2type=N'COLUMN',@level2name=N'FSPASSWD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用者中文姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS', @level2type=N'COLUMN',@level2name=N'FSUSER_ChtName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用者的工作抬頭' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS', @level2type=N'COLUMN',@level2name=N'FSUSER_TITLE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用者的部門中文名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS', @level2type=N'COLUMN',@level2name=N'FSDEPT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'電子郵件信箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS', @level2type=N'COLUMN',@level2name=N'FSEMAIL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用者是否作用中' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS', @level2type=N'COLUMN',@level2name=N'FCACTIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用者的描述(目前未使用)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS', @level2type=N'COLUMN',@level2name=N'FSDESCRIPTION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'(目前未使用)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS', @level2type=N'COLUMN',@level2name=N'FCSECRET'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用者所屬頻道別' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS', @level2type=N'COLUMN',@level2name=N'FSCHANNEL_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'第一層部門ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS', @level2type=N'COLUMN',@level2name=N'FNUpperDept_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'第二層部門ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS', @level2type=N'COLUMN',@level2name=N'FNDep_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'第三層部門ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS', @level2type=N'COLUMN',@level2name=N'FNGroup_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最後算出來的部門ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS', @level2type=N'COLUMN',@level2name=N'FNTreeViewDept'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'上層部門中文名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS', @level2type=N'COLUMN',@level2name=N'FSUpperDept_ChtName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'算出來的部門中文名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS', @level2type=N'COLUMN',@level2name=N'FNTreeViewDept_ChtName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'第二層中文名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS', @level2type=N'COLUMN',@level2name=N'FSDep_ChtName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'第三層部門中文名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS', @level2type=N'COLUMN',@level2name=N'FSGroup_ChtName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否為主職稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS', @level2type=N'COLUMN',@level2name=N'FSIs_Main_Title'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否為主管' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS', @level2type=N'COLUMN',@level2name=N'FSIs_Supervisor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立者ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSERS', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
