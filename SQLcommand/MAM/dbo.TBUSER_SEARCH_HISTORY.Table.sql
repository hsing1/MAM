USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBUSER_SEARCH_HISTORY](
	[FNNO] [bigint] NOT NULL,
	[FSUSER_ID] [nvarchar](50) NOT NULL,
	[FDSEARCH_DATE] [varchar](20) NOT NULL,
	[FSKEY_WORD] [nvarchar](50) NOT NULL,
	[FSINDEX] [varchar](max) NOT NULL,
	[FSCREATED_DATE_BEGIN] [varchar](10) NOT NULL,
	[FSCREATED_DATE_END] [varchar](10) NOT NULL,
	[FSCHANNEL] [nvarchar](50) NOT NULL,
	[FSCOLUMN1] [varchar](50) NOT NULL,
	[FSCOLUMN2] [varchar](50) NOT NULL,
	[FSCOLUMN3] [varchar](50) NOT NULL,
	[FSVALUE1] [varchar](50) NOT NULL,
	[FSVALUE2] [varchar](50) NOT NULL,
	[FSVALUE3] [varchar](50) NOT NULL,
	[FSLOGIC1] [varchar](5) NOT NULL,
	[FSLOGIC2] [varchar](5) NOT NULL,
	[FSSEARCH_MODE] [varchar](1) NOT NULL,
	[FSHOMOPHONE] [varchar](1) NOT NULL,
	[FSSYNONYM] [varchar](1) NOT NULL,
	[FNPAGE_SIZE] [int] NOT NULL,
	[FNSTART_POINT] [bigint] NOT NULL,
	[FSGET_COLUMN] [varchar](200) NOT NULL,
	[FSTREE_NODE] [varchar](50) NOT NULL,
	[FSORDERBY] [varchar](50) NULL,
	[FSORDERTYPE] [char](1) NULL,
 CONSTRAINT [PK_TBUSER_SEARCH_HISTORY] PRIMARY KEY CLUSTERED 
(
	[FNNO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
CREATE NONCLUSTERED INDEX [INDEX_FSKEY_WORD] ON [dbo].[TBUSER_SEARCH_HISTORY]
(
	[FSKEY_WORD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'序號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_SEARCH_HISTORY', @level2type=N'COLUMN',@level2name=N'FNNO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_SEARCH_HISTORY', @level2type=N'COLUMN',@level2name=N'FSUSER_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'檢索日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_SEARCH_HISTORY', @level2type=N'COLUMN',@level2name=N'FDSEARCH_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'關鍵字' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_SEARCH_HISTORY', @level2type=N'COLUMN',@level2name=N'FSKEY_WORD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'索引庫' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_SEARCH_HISTORY', @level2type=N'COLUMN',@level2name=N'FSINDEX'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'開始日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_SEARCH_HISTORY', @level2type=N'COLUMN',@level2name=N'FSCREATED_DATE_BEGIN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'結束日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_SEARCH_HISTORY', @level2type=N'COLUMN',@level2name=N'FSCREATED_DATE_END'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'頻道' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_SEARCH_HISTORY', @level2type=N'COLUMN',@level2name=N'FSCHANNEL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'欄位1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_SEARCH_HISTORY', @level2type=N'COLUMN',@level2name=N'FSCOLUMN1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'欄位2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_SEARCH_HISTORY', @level2type=N'COLUMN',@level2name=N'FSCOLUMN2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'欄位3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_SEARCH_HISTORY', @level2type=N'COLUMN',@level2name=N'FSCOLUMN3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'欄位值1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_SEARCH_HISTORY', @level2type=N'COLUMN',@level2name=N'FSVALUE1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'欄位值2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_SEARCH_HISTORY', @level2type=N'COLUMN',@level2name=N'FSVALUE2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'欄位值3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_SEARCH_HISTORY', @level2type=N'COLUMN',@level2name=N'FSVALUE3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'邏輯運算1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_SEARCH_HISTORY', @level2type=N'COLUMN',@level2name=N'FSLOGIC1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'邏輯運算2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_SEARCH_HISTORY', @level2type=N'COLUMN',@level2name=N'FSLOGIC2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'檢索模式' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_SEARCH_HISTORY', @level2type=N'COLUMN',@level2name=N'FSSEARCH_MODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否啟用同音字' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_SEARCH_HISTORY', @level2type=N'COLUMN',@level2name=N'FSHOMOPHONE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否啟用同義詞' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_SEARCH_HISTORY', @level2type=N'COLUMN',@level2name=N'FSSYNONYM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'每頁筆數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_SEARCH_HISTORY', @level2type=N'COLUMN',@level2name=N'FNPAGE_SIZE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'開始筆數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_SEARCH_HISTORY', @level2type=N'COLUMN',@level2name=N'FNSTART_POINT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'取得欄位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_SEARCH_HISTORY', @level2type=N'COLUMN',@level2name=N'FSGET_COLUMN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'後分類節點' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_SEARCH_HISTORY', @level2type=N'COLUMN',@level2name=N'FSTREE_NODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'排序欄位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_SEARCH_HISTORY', @level2type=N'COLUMN',@level2name=N'FSORDERBY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'排序模式' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_SEARCH_HISTORY', @level2type=N'COLUMN',@level2name=N'FSORDERTYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'描述', @value=N'使用者檢索條件記錄' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_SEARCH_HISTORY'
GO
