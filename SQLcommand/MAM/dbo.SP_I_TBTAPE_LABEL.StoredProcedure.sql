USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/03/30>
-- Description:	把影片管理系統比對完後, 有match到的label塞入資料表
-- =============================================
CREATE PROC [dbo].[SP_I_TBTAPE_LABEL]
	@BIB varchar(7), @Title nvarchar(120), @Kind char(1), @Category varchar(40), @LabelType varchar(60),
	@ChtTitle nvarchar(120),@Author NVARCHAR(MAX), @ProdPlace varchar(120), @Producer varchar(120), @ProdDT varchar(120),
	@Actor NVARCHAR(MAX), @Caption nvarchar(120), @Description NVARCHAR(MAX), @IndexWord NVARCHAR(MAX), @EpNo varchar(10),
	@Duration varchar(255), @TVDate varchar(60), @Format varchar(255), @AddUser varchar(60), @ReturnDate varchar(60),
	@AudioME varchar(60), @LangScript varchar(60), @SubTransDub varchar(60), @Memo NVARCHAR(MAX), @AddDate varchar(60),
	@InPrice varchar(10), @UseYear varchar(10), @TVNumber varchar(10), @Suggest1 varchar(20), @Suggest2 varchar(20),
	@Suggest3 varchar(20), @FormerTitle varchar(60), @Volume varchar(120), @ISBN varchar(20), @ISSN varchar(20),
	@LCCN varchar(20), @Dynix int, @LockMark bit, @Hidden bit, @EXEC_GUID VARCHAR(36)
AS

	INSERT
		TBTAPE_LABEL
		([BIB], [Title], [Kind], [Category], [LabelType], 
		[ChtTitle], [Author], [ProdPlace], [Producer], [ProdDT], 
		[Actor], [Caption], [Description], [IndexWord], [EpNo], 
		[Duration], [TVDate], [Format], [AddUser], [ReturnDate], 
		[AudioME], [LangScript], [SubTransDub], [Memo], [AddDate], 
		[InPrice], [UseYear], [TVNumber], [Suggest1], [Suggest2], 
		[Suggest3], [FormerTitle], [Volume], [ISBN], [ISSN], 
		[LCCN], [Dynix], [LockMark], [Hidden], [EXEC_GUID])
	VALUES
		(@BIB, @Title, @Kind, @Category, @LabelType, 
		@ChtTitle, @Author, @ProdPlace, @Producer, @ProdDT, 
		@Actor, @Caption, @Description, @IndexWord, @EpNo, 
		@Duration, @TVDate, @Format, @AddUser, @ReturnDate, 
		@AudioME, @LangScript, @SubTransDub, @Memo, @AddDate, 
		@InPrice, @UseYear, @TVNumber, @Suggest1, @Suggest2, 
		@Suggest3, @FormerTitle, @Volume, @ISBN, @ISSN, 
		@LCCN, @Dynix, @LockMark, @Hidden, @EXEC_GUID)
						
	SELECT	COUNT(*)
	FROM	TBTAPE_LABEL 
	WHERE	(BIB = @BIB) AND
			(EXEC_GUID = @EXEC_GUID)	--為了確定本次有新增進去

GO
