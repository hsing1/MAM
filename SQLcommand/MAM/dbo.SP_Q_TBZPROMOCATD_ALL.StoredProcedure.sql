USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Q_TBZPROMOCATD_ALL]

As
SELECT FSPROMOCATID as ID,FSPROMOCATDID as IDD,[dbo].[FN_Q_CHANGE_STOP_NAME](FBISENABLE, FSPROMOCATDNAME) as NAME,FSSORT FROM MAM.dbo.TBZPROMOCATD 
where FSPROMOCATDNAME <> ''
order by FSPROMOCATID,FSPROMOCATDID,FSSORT
GO
