USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBMACHINE](
	[FSSERVER_NAME] [varchar](255) NOT NULL,
	[FSSERVER_IP] [varchar](255) NOT NULL,
	[FSGROUP] [varchar](50) NULL,
	[FSMAX_CPU] [int] NULL,
	[FSMONITOR_HD] [varchar](128) NULL,
	[FSMAX_HD] [varchar](50) NULL,
	[FSMAX_MEMORY] [int] NULL,
	[FSMAX_LAN] [int] NULL,
	[FSNETWORK_INTERFACE] [varchar](255) NULL,
	[FSMAX_WEB] [int] NULL,
	[FSWEB_NAME] [nvarchar](255) NULL,
	[FCAVAILABLE] [char](1) NULL,
	[FCHAS_NOTIFY] [char](1) NULL,
 CONSTRAINT [PK_TBMachine] PRIMARY KEY CLUSTERED 
(
	[FSSERVER_NAME] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[TBMACHINE] ADD  CONSTRAINT [DF_TBMACHINE_FCAvailable]  DEFAULT ('N') FOR [FCAVAILABLE]
GO
ALTER TABLE [dbo].[TBMACHINE] ADD  CONSTRAINT [DF_TBMACHINE_FCHAS_NOTIFY]  DEFAULT ('N') FOR [FCHAS_NOTIFY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'伺服器名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMACHINE', @level2type=N'COLUMN',@level2name=N'FSSERVER_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'伺服器IP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMACHINE', @level2type=N'COLUMN',@level2name=N'FSSERVER_IP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'伺服器群組名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMACHINE', @level2type=N'COLUMN',@level2name=N'FSGROUP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CPU最大臨界值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMACHINE', @level2type=N'COLUMN',@level2name=N'FSMAX_CPU'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'受監控磁碟代號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMACHINE', @level2type=N'COLUMN',@level2name=N'FSMONITOR_HD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'磁碟最大臨界值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMACHINE', @level2type=N'COLUMN',@level2name=N'FSMAX_HD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'記憶體最大臨界值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMACHINE', @level2type=N'COLUMN',@level2name=N'FSMAX_MEMORY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'網路最大臨界值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMACHINE', @level2type=N'COLUMN',@level2name=N'FSMAX_LAN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'網路卡名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMACHINE', @level2type=N'COLUMN',@level2name=N'FSNETWORK_INTERFACE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最大系統使用者數臨界值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMACHINE', @level2type=N'COLUMN',@level2name=N'FSMAX_WEB'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'監控網站名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMACHINE', @level2type=N'COLUMN',@level2name=N'FSWEB_NAME'
GO
