USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <David.Sin>
-- Create date:   <2010/10/02>
-- Description:   根據調用單編號查詢調用檔案是否為宏觀所有
-- =============================================

CREATE Proc [dbo].[SP_Q_GET_BOOKING_FILE_INTERNATION_BY_BOOKINGNO]
@FSBOOKING_NO varchar(12)
AS

Select A.FSFILE_NO,C.FNDEP_ID,FSPROG_ID
From TBBOOKING_DETAIL A JOIN TBLOG_VIDEO B ON A.FSFILE_NO=B.FSFILE_NO
		JOIN TBPROG_M C ON B.FSID=C.FSPROG_ID
Where A.FSBOOKING_NO=@FSBOOKING_NO
GO
