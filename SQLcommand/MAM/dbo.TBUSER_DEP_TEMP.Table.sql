USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBUSER_DEP_TEMP](
	[ParentDeptID] [nvarchar](100) NULL,
	[DeptID] [nvarchar](100) NULL,
	[DeptChtName] [nvarchar](100) NULL,
	[DeptMemo] [nvarchar](100) NULL
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'父部門ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_DEP_TEMP', @level2type=N'COLUMN',@level2name=N'ParentDeptID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部門ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_DEP_TEMP', @level2type=N'COLUMN',@level2name=N'DeptID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部門中文名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_DEP_TEMP', @level2type=N'COLUMN',@level2name=N'DeptChtName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部門備註' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_DEP_TEMP', @level2type=N'COLUMN',@level2name=N'DeptMemo'
GO
