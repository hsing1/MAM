USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        
-- Create date: 
-- Description: 取得被未審核通過的清單
-- Update desc:    <2013/04/18><Mike>加入註解。
-- =============================================
CREATE Proc [dbo].[SP_Q_GET_NOT_APPROVE_FILE]

As

select * from tblog_video_hd_mc where fcstatus='X' and FDUPDATED_DATE>GETDATE()-30 and  FDUPDATED_DATE<GETDATE()-3

GO
