USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBPGM_QUEUE_MONITER](
	[FSPROG_ID] [char](7) NOT NULL,
	[FNSEQNO] [int] NOT NULL,
	[FDDATE] [date] NOT NULL,
	[FSCHANNEL_ID] [varchar](2) NOT NULL,
	[FNEPISODE] [int] NOT NULL,
	[FCPROG_SIDE_LABEL] [char](1) NULL,
	[FCLIVE_SIDE_LABEL] [char](1) NULL,
	[FCTIME_SIDE_LABEL] [char](1) NULL,
	[FCREPLAY] [char](1) NULL,
	[FCRECORD_PLAY] [char](1) NULL,
	[FCSTEREO] [char](1) NULL,
	[FCLOGO] [char](1) NULL,
	[FSCH1] [char](2) NULL,
	[FSCH2] [char](2) NULL,
	[FSCH3] [char](2) NULL,
	[FSCH4] [char](2) NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
 CONSTRAINT [PK_TBPGM_QUEUE_MONITER] PRIMARY KEY CLUSTERED 
(
	[FSPROG_ID] ASC,
	[FNSEQNO] ASC,
	[FDDATE] ASC,
	[FSCHANNEL_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_QUEUE_MONITER', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_QUEUE_MONITER', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'描述', @value=N'節目表播出提示側標資料(已經不使用)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_QUEUE_MONITER'
GO
