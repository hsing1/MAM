USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBUSER_DEP](
	[FNDEP_ID] [int] NOT NULL,
	[FSDEP] [nvarchar](60) NULL,
	[FNPARENT_DEP_ID] [int] NULL,
	[FSSupervisor_ID] [nvarchar](50) NULL,
	[FSUpDepName_ChtName] [nvarchar](60) NULL,
	[FSDpeName_ChtName] [nvarchar](60) NULL,
	[FSFullName_ChtName] [nvarchar](246) NULL,
	[FNDEP_LEVEL] [int] NULL,
	[FBIsDepExist] [char](1) NULL,
	[FSDEP_MEMO] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'场ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_DEP', @level2type=N'COLUMN',@level2name=N'FNDEP_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'场嘿' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_DEP', @level2type=N'COLUMN',@level2name=N'FSDEP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'场ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_DEP', @level2type=N'COLUMN',@level2name=N'FNPARENT_DEP_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'恨ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_DEP', @level2type=N'COLUMN',@level2name=N'FSSupervisor_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'糷场いゅ嘿' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_DEP', @level2type=N'COLUMN',@level2name=N'FSUpDepName_ChtName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'场いゅ嘿' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_DEP', @level2type=N'COLUMN',@level2name=N'FSDpeName_ChtName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'﹃癬ㄓ场嘿' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_DEP', @level2type=N'COLUMN',@level2name=N'FSFullName_ChtName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'场' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_DEP', @level2type=N'COLUMN',@level2name=N'FNDEP_LEVEL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'场琌' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_DEP', @level2type=N'COLUMN',@level2name=N'FBIsDepExist'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'爹癘逆' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_DEP', @level2type=N'COLUMN',@level2name=N'FSDEP_MEMO'
GO
