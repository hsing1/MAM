USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBZFILE_STATUS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FSFILE_STATUS_ID] [nvarchar](50) NULL,
	[FSFILE_STATUS_NAME] [nvarchar](50) NULL,
	[FDCREATE_DATE] [datetime] NULL,
	[FSCREATE_BY] [nvarchar](50) NULL,
	[FDUPDATE_DATE] [datetime] NULL,
	[FSUPDATE_BY] [nvarchar](50) NULL
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[TBZFILE_STATUS] ADD  CONSTRAINT [DF_TBZFILE_STATUS_FSFILE_STATUS_ID]  DEFAULT ('') FOR [FSFILE_STATUS_ID]
GO
ALTER TABLE [dbo].[TBZFILE_STATUS] ADD  CONSTRAINT [DF_TBZFILE_STATUS_FSFILE_STATUS_NAME]  DEFAULT ('') FOR [FSFILE_STATUS_NAME]
GO
ALTER TABLE [dbo].[TBZFILE_STATUS] ADD  CONSTRAINT [DF_TBZFILE_STATUS_FDCREATE_DATE]  DEFAULT (getdate()) FOR [FDCREATE_DATE]
GO
ALTER TABLE [dbo].[TBZFILE_STATUS] ADD  CONSTRAINT [DF_TBZFILE_STATUS_FSCREATE_BY]  DEFAULT ('') FOR [FSCREATE_BY]
GO
ALTER TABLE [dbo].[TBZFILE_STATUS] ADD  CONSTRAINT [DF_TBZFILE_STATUS_FDUPDATE_DATE]  DEFAULT (getdate()) FOR [FDUPDATE_DATE]
GO
ALTER TABLE [dbo].[TBZFILE_STATUS] ADD  CONSTRAINT [DF_TBZFILE_STATUS_FSUPDATE_BY]  DEFAULT ('') FOR [FSUPDATE_BY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'流水編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZFILE_STATUS', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'檔案代碼ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZFILE_STATUS', @level2type=N'COLUMN',@level2name=N'FSFILE_STATUS_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'代碼狀態中文名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZFILE_STATUS', @level2type=N'COLUMN',@level2name=N'FSFILE_STATUS_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZFILE_STATUS', @level2type=N'COLUMN',@level2name=N'FDCREATE_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立者ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZFILE_STATUS', @level2type=N'COLUMN',@level2name=N'FSCREATE_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZFILE_STATUS', @level2type=N'COLUMN',@level2name=N'FDUPDATE_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZFILE_STATUS', @level2type=N'COLUMN',@level2name=N'FSUPDATE_BY'
GO
