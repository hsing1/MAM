USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Sam.wu>
-- Create date: <2011/05/13>
-- Description:	<根據傳入條件,取回是否為已停用的代碼名稱>
-- =============================================
CREATE FUNCTION [dbo].[FN_Q_CHANGE_STOP_NAME]
(
	@FSSW Bit,
	@FSNAME NVARCHAR(50) 
)
RETURNS	NVARCHAR(60)
AS
BEGIN 
		Declare @return_value NVARCHAR(60) = @FSNAME
		
		 --SET @return_value = (CASE WHEN @FSSW = 'Y' THEN  '已停用-' +@return_value ELSE @return_value END)
  
		RETURN	Replace (Replace((CASE WHEN @FSSW = 0 THEN  @return_value+'(已停用)' ELSE @return_value END), Char(13), ''),Char(10), '')

END


GO
