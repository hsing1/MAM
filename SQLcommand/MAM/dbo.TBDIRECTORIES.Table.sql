USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBDIRECTORIES](
	[FNDIR_ID] [bigint] NOT NULL,
	[FSDIR] [nvarchar](50) NOT NULL,
	[FNPARENT_ID] [bigint] NULL,
	[FCQUEUE] [char](1) NOT NULL,
	[FSDESCIPTION] [nvarchar](50) NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
	[FSUPDATED_BY] [varchar](50) NULL,
	[FDUPDATED_DATE] [datetime] NULL,
	[FNTEMPLATE_ID_SUBJECT] [int] NULL,
	[FNTEMPLATE_ID_PHOTO] [int] NULL,
	[FNTEMPLATE_ID_DOC] [int] NULL,
	[FNTEMPLATE_ID_AUDIO] [int] NULL,
	[FNTEMPLATE_ID_VIDEO_D] [int] NULL,
 CONSTRAINT [ TBDIRECTORIES] PRIMARY KEY CLUSTERED 
(
	[FNDIR_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[TBDIRECTORIES] ADD  CONSTRAINT [DF_TBDIRECTORIES_FNTEMPLATE_ID_SUBJECT]  DEFAULT ((33)) FOR [FNTEMPLATE_ID_SUBJECT]
GO
ALTER TABLE [dbo].[TBDIRECTORIES] ADD  CONSTRAINT [DF_TBDIRECTORIES_FNTEMPLATE_ID_PHOTO]  DEFAULT ((39)) FOR [FNTEMPLATE_ID_PHOTO]
GO
ALTER TABLE [dbo].[TBDIRECTORIES] ADD  CONSTRAINT [DF_TBDIRECTORIES_FNTEMPLATE_ID_DOC]  DEFAULT ((37)) FOR [FNTEMPLATE_ID_DOC]
GO
ALTER TABLE [dbo].[TBDIRECTORIES] ADD  CONSTRAINT [DF_TBDIRECTORIES_FNTEMPLATE_ID_AUDIO]  DEFAULT ((73)) FOR [FNTEMPLATE_ID_AUDIO]
GO
ALTER TABLE [dbo].[TBDIRECTORIES] ADD  CONSTRAINT [DF_TBDIRECTORIES_FNTEMPLATE_ID_VIDEO_D]  DEFAULT ((35)) FOR [FNTEMPLATE_ID_VIDEO_D]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DIRECTORY代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBDIRECTORIES', @level2type=N'COLUMN',@level2name=N'FNDIR_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DIRECTORY名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBDIRECTORIES', @level2type=N'COLUMN',@level2name=N'FSDIR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'上層DIRECTORY代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBDIRECTORIES', @level2type=N'COLUMN',@level2name=N'FNPARENT_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否為QUEUE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBDIRECTORIES', @level2type=N'COLUMN',@level2name=N'FCQUEUE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBDIRECTORIES', @level2type=N'COLUMN',@level2name=N'FSDESCIPTION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBDIRECTORIES', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBDIRECTORIES', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBDIRECTORIES', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBDIRECTORIES', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
