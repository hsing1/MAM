USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   修改短帶託播單預排資料(目前不使用)
-- =============================================
CREATE Proc [dbo].[SP_U_TBPGM_PROMO_SCHEDULED]

@FNPROMO_BOOKING_NO	int,
@FNNO	int,
@FCSTATUS	varchar(1)

As

update TBPGM_PROMO_SCHEDULED set FCSTATUS=@FCSTATUS where
 FNPROMO_BOOKING_NO=@FNPROMO_BOOKING_NO
 and FNNO=@FNNO

   







GO
