USE [MAM]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[StoredProcedure1](@beginTime [nvarchar](4000), @endTime [nvarchar](4000))
RETURNS [nvarchar](4000) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [SqlServerProject1].[StoredProcedures].[StoredProcedure1]
GO
EXEC sys.sp_addextendedproperty @name=N'SqlAssemblyFile', @value=N'StoredProcedure1.cs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'FUNCTION',@level1name=N'StoredProcedure1'
GO
EXEC sys.sp_addextendedproperty @name=N'SqlAssemblyFileLine', @value=N'12' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'FUNCTION',@level1name=N'StoredProcedure1'
GO
