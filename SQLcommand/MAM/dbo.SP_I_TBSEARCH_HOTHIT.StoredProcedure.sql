USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <David.Sin>
-- Create date:   <2010/10/02>
-- Description:   新增熱門點閱
-- =============================================

CREATE Proc [dbo].[SP_I_TBSEARCH_HOTHIT]
@FSSYS_ID varchar(20),
@FSTITLE nvarchar(200),
@FSINDEX_TYPE varchar(100),
@FSUSER_ID nvarchar(50),
@FDDATE varchar(10)
As

Declare @FNNO bigint
Select @FNNO=MAX(FNNO) From TBSEARCH_HOTHIT

If @FNNO is null Set @FNNO=1 Else Set @FNNO=@FNNO+1

Insert Into TBSEARCH_HOTHIT(FNNO,FSSYS_ID,FSTITLE,FSINDEX_TYPE,FSUSER_ID,FDDATE)
Values(@FNNO,@FSSYS_ID,@FSTITLE,@FSINDEX_TYPE,@FSUSER_ID,@FDDATE)

GO
