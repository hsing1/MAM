USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_U_TBUSERS_INFO]
      @FSUSER_ID nvarchar(50),
      @FSUSER nvarchar(50),
      @FSPASSWD nvarchar(255),
      @FSUSER_ChtName nvarchar(50),
      @FSUSER_TITLE nvarchar(50),
      @FSDEPT nvarchar(50),
      @FSEMAIL nvarchar(50),
      @FCACTIVE nvarchar(1),
      @FSDESCRIPTION nvarchar(2000),
      @FCSECRET nvarchar(1),
      --@FSCHANNEL_ID nvarchar(2),
      @FNUpperDept_ID int,
      @FNDep_ID int,
      @FNGroup_ID int,
      @FNTreeViewDept int,
      @FNTreeViewDept_ChtName nvarchar(100),
      --@FSUpperDept_ChtName nvarchar(60),
      --@FSDep_ChtName nvarchar(60),
      --@FSGroup_ChtName nvarchar(60),
      @FSUPDATED_BY nvarchar(50)
AS

if (@FSPASSWD='') 
begin
	UPDATE [MAM].[dbo].[TBUSERS] 
	SET   FSUSER=@FSUSER
		  --,FSPASSWD=@FSPASSWD
		  ,FSUSER_ChtName=@FSUSER_ChtName
		  ,FSUSER_TITLE=@FSUSER_TITLE
		  ,FSDEPT=@FSDEPT
		  ,FSEMAIL=@FSEMAIL
		  ,FCACTIVE=@FCACTIVE
		  ,FSDESCRIPTION=@FSDESCRIPTION
		  ,FCSECRET=@FCSECRET
		  --,FSCHANNEL_ID=@FSCHANNEL_ID
		  ,FNUpperDept_ID=@FNUpperDept_ID
		  ,FNDep_ID=@FNDep_ID
		  ,FNGroup_ID=@FNGroup_ID
		  ,FNTreeViewDept=@FNTreeViewDept
		  ,FNTreeViewDept_ChtName=@FNTreeViewDept_ChtName
		  --,FSUpperDept_ChtName=@FSUpperDept_ChtName
		  --,FSDep_ChtName=@FSDep_ChtName
		  --,FSGroup_ChtName=@FSGroup_ChtName
		  ,FSUPDATED_BY=@FSUPDATED_BY
		  ,FDUPDATED_DATE=GETDATE()

	WHERE FSUSER_ID=@FSUSER_ID
end
else
begin
	UPDATE [MAM].[dbo].[TBUSERS] 
	SET   FSUSER=@FSUSER
		  ,FSPASSWD=@FSPASSWD
		  ,FSUSER_ChtName=@FSUSER_ChtName
		  ,FSUSER_TITLE=@FSUSER_TITLE
		  ,FSDEPT=@FSDEPT
		  ,FSEMAIL=@FSEMAIL
		  ,FCACTIVE=@FCACTIVE
		  ,FSDESCRIPTION=@FSDESCRIPTION
		  ,FCSECRET=@FCSECRET
		  --,FSCHANNEL_ID=@FSCHANNEL_ID
		  ,FNUpperDept_ID=@FNUpperDept_ID
		  ,FNDep_ID=@FNDep_ID
		  ,FNGroup_ID=@FNGroup_ID
		  ,FNTreeViewDept=@FNTreeViewDept
		  ,FNTreeViewDept_ChtName=@FNTreeViewDept_ChtName
		  --,FSUpperDept_ChtName=@FSUpperDept_ChtName
		  --,FSDep_ChtName=@FSDep_ChtName
		  --,FSGroup_ChtName=@FSGroup_ChtName
		  ,FSUPDATED_BY=@FSUPDATED_BY
		  ,FDUPDATED_DATE=GETDATE()

	WHERE FSUSER_ID=@FSUSER_ID
end
GO
