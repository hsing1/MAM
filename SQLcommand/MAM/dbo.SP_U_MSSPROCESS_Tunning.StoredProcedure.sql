USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Jarvis.Yang>
-- Create date:    <2013/06/10>
-- Description:    段落置換後更新TBPGM_MSSPROCESS中該筆資料的值
-- =============================================
create PROCEDURE [dbo].[SP_U_MSSPROCESS_Tunning] 
	@FNSTATUS char(2),
	@FSVIDEOID varchar(10),
    @FNMSSPROCCESSID varchar(50),
    @CurrentDate char(8),
    @flag char
AS
BEGIN

if(@flag='1')
begin
update TBPGM_MSSPROCESS SET FDUPDATE_TIME=GETDATE(),FNSTATUS=cast(@FNSTATUS as smallint)
where FNMSSPROCESS_ID >cast(@CurrentDate+'0000' as bigint) and FSVIDEOID=@FSVIDEOID
end
else
begin
update TBPGM_MSSPROCESS SET FDUPDATE_TIME=GETDATE(),FNSTATUS=cast(@FNSTATUS as smallint)
where FNMSSPROCESS_ID =cast(@FNMSSPROCCESSID as bigint)

end

END



/****** Object:  StoredProcedure [dbo].[SP_Q_TBBROADCAST_Replacement_BYSTATUS_ADVANCE_STT]    Script Date: 06/19/2013 12:07:17 ******/
SET ANSI_NULLS ON

GO
