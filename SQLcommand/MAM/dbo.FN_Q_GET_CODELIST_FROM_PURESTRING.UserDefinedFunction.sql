USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/04/27>
-- Description:	<根據傳入條件,取回最新單號>
-- =============================================
CREATE FUNCTION [dbo].[FN_Q_GET_CODELIST_FROM_PURESTRING]
(
	@INPUT	NVARCHAR(100)
)
RETURNS	NVARCHAR(310)
AS
BEGIN
	--宣告程序內變數
	DECLARE	@idx INT, @char VARCHAR, @RESULT NVARCHAR(310)

	--初始化變數
	SELECT @idx = 1, @RESULT = ''

	--跑迴圈處理每一個設定
	WHILE(@idx <= LEN(@INPUT))
		BEGIN
			SET @char = SUBSTRING(@INPUT,@idx,1)
			
			--有設定1的才串入結果
			IF (@char = '1')
				SET @RESULT = @RESULT +  SUBSTRING(CAST((100 + @idx) AS VARCHAR(3)),2,2) + ';'

			SET @idx = @idx + 1
		END

	RETURN @RESULT
END


GO
