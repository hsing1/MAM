USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBSEARCH_TABLE_FIELD](
	[FSTABLE] [varchar](50) NOT NULL,
	[FNFILED_ID] [int] NOT NULL,
	[FSFIELD_ENAME] [varchar](50) NOT NULL,
	[FSFIELD_CNAME] [varchar](50) NOT NULL,
	[FSFIELD_TYPE] [varchar](15) NOT NULL,
	[FSCONTROL_TYPE] [varchar](15) NOT NULL,
	[FCCOMMON_FIELD] [char](1) NOT NULL,
	[FNORDER_INDEX] [int] NULL,
 CONSTRAINT [PK_TBSEARCH_TABLE_FIELD] PRIMARY KEY CLUSTERED 
(
	[FNFILED_ID] ASC,
	[FSTABLE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'資料表名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSEARCH_TABLE_FIELD', @level2type=N'COLUMN',@level2name=N'FSTABLE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'欄位編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSEARCH_TABLE_FIELD', @level2type=N'COLUMN',@level2name=N'FNFILED_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'欄位英文名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSEARCH_TABLE_FIELD', @level2type=N'COLUMN',@level2name=N'FSFIELD_ENAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'欄位中文名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSEARCH_TABLE_FIELD', @level2type=N'COLUMN',@level2name=N'FSFIELD_CNAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'欄位屬性' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSEARCH_TABLE_FIELD', @level2type=N'COLUMN',@level2name=N'FSFIELD_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'控制項類別' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSEARCH_TABLE_FIELD', @level2type=N'COLUMN',@level2name=N'FSCONTROL_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否為一般欄位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSEARCH_TABLE_FIELD', @level2type=N'COLUMN',@level2name=N'FCCOMMON_FIELD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'排序' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSEARCH_TABLE_FIELD', @level2type=N'COLUMN',@level2name=N'FNORDER_INDEX'
GO
EXEC sys.sp_addextendedproperty @name=N'描述', @value=N'可檢索欄位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSEARCH_TABLE_FIELD'
GO
