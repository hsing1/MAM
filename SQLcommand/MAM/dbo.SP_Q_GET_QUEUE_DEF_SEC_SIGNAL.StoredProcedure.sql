USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2013/07/12>
-- Description:   查詢之前輸入的預設長度與訊號源
-- =============================================
CREATE Proc [dbo].[SP_Q_GET_QUEUE_DEF_SEC_SIGNAL]
@FSPROG_ID	varchar(8),
@FNEPISODE	int,
@FDDATE	Date
 
As
select fddate,FNDEF_MIN,FNDEF_SEC,FSSIGNAL,FSBEG_TIME,FSEND_TIME  from tbpgm_queue where 
 fsprog_id=@FSPROG_ID and fnepisode=@FNEPISODE and fddate < @FDDATE order by fddate desc 

GO
