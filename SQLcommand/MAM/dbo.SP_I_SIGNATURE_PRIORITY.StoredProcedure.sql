USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  Proc [dbo].[SP_I_SIGNATURE_PRIORITY]
@FNDEP_ID nvarchar(100),
@FSSignatureID nvarchar(100)
As

  insert into [MAM].[dbo].[TBUSER_DEP_SIGNATURE_PRIORITY](FNDEP_ID,FSSignatureID)
  values (@FNDEP_ID,@FSSignatureID)

GO
