USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

-- Author:        <Mike.Lin>

-- Create date:   <2012/10/24>

-- Description:   刪除節目與短帶連結資料

-- =============================================

CREATE Proc [dbo].[SP_D_TBPGM_PROG_PROMO_LINK]

@FSPROG_ID	char(7),
@FNSEQNO	int,
@FSCHANNEL_ID	varchar(2)
As
Delete From dbo.TBPGM_PROG_PROMO_LINK 
where 
FSPROG_ID=@FSPROG_ID
and FNSEQNO= @FNSEQNO  
and FSCHANNEL_ID=@FSCHANNEL_ID
GO
