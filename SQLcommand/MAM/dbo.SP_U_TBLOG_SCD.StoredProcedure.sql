USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/05/02>
-- Description:	�s�WTBLOG_SCD
-- =============================================
CREATE PROC [dbo].[SP_U_TBLOG_SCD]
	@FUGUID		varchar(36),
	@FSSTATUS	varchar(10),
	@FSRESULT	nvarchar(200)
AS
	SET NOCOUNT ON;

	BEGIN TRY
		UPDATE
			TBLOG_SCD
		SET
			FUGUID		= @FUGUID,
			FSSTATUS	= @FSSTATUS,
			FDETIME		= GETDATE(),
			FSRESULT	= @FSRESULT
		WHERE
			(FUGUID		= @FUGUID)
			
		SELECT [RESULT] = ''
		
	END TRY
	BEGIN CATCH

		SELECT [RESULT] = 'ERROR:' + CAST(@@ERROR AS NVARCHAR(10))

	END CATCH


GO
