USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE Proc [dbo].[SP_Q_TBLOG_VIDEO_D]
@FSFILE_NO char(16), 
@FNSEQ_NO int


As


IF @FSFILE_NO='' 
BEGIN 
	select * from TBLOG_VIDEO_D  ORDER BY FSFILE_NO, FNSEQ_NO
END 

ELSE IF @FSFILE_NO != '' AND @FNSEQ_NO = -1 
Begin 
	SELECT * FROM TBLOG_VIDEO_D WHERE FSFILE_NO = @FSFILE_NO ORDER BY FNSEQ_NO
End 

ELSE 
BEGIN 
	select * from TBLOG_VIDEO_D WHERE FSFILE_NO = @FSFILE_NO AND FNSEQ_NO = @FNSEQ_NO ORDER BY FNSEQ_NO 
END 




GO
