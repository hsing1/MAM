USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBZPROMOCATD](
	[FSPROMOCATID] [char](2) NOT NULL,
	[FSPROMOCATDID] [char](2) NOT NULL,
	[FSPROMOCATDNAME] [nvarchar](50) NOT NULL,
	[FSSORT] [char](2) NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
	[FSUPDATED_BY] [varchar](50) NULL,
	[FDUPDATED_DATE] [datetime] NULL,
	[FBISENABLE] [bit] NULL,
 CONSTRAINT [PK_TBZPROMOCATD] PRIMARY KEY CLUSTERED 
(
	[FSPROMOCATDID] ASC,
	[FSPROMOCATID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[TBZPROMOCATD] ADD  CONSTRAINT [DF_TBZPROMOCATD_FBISFUNCTION]  DEFAULT ((1)) FOR [FBISENABLE]
GO
ALTER TABLE [dbo].[TBZPROMOCATD]  WITH NOCHECK ADD  CONSTRAINT [FK_TBZPROMOCATD_TBZPROMOCAT] FOREIGN KEY([FSPROMOCATID])
REFERENCES [dbo].[TBZPROMOCAT] ([FSPROMOCATID])
GO
ALTER TABLE [dbo].[TBZPROMOCATD] CHECK CONSTRAINT [FK_TBZPROMOCATD_TBZPROMOCAT]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'大項ID(對應到TBZPROMOCAT)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZPROMOCATD', @level2type=N'COLUMN',@level2name=N'FSPROMOCATID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'宣傳帶類型細項代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZPROMOCATD', @level2type=N'COLUMN',@level2name=N'FSPROMOCATDID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'宣傳帶類型細項名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZPROMOCATD', @level2type=N'COLUMN',@level2name=N'FSPROMOCATDNAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'排序號碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZPROMOCATD', @level2type=N'COLUMN',@level2name=N'FSSORT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZPROMOCATD', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZPROMOCATD', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZPROMOCATD', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZPROMOCATD', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否作用中' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZPROMOCATD', @level2type=N'COLUMN',@level2name=N'FBISENABLE'
GO
