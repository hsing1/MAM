USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBDIRECTORY_GROUP](
	[FNDIR_ID] [bigint] NOT NULL,
	[FSGROUP_ID] [varchar](20) NOT NULL,
	[FCPRIVILEGE] [char](1) NULL,
	[FSCREATED_BY] [varchar](50) NULL,
	[FDCREATED_DATE] [datetime] NULL,
	[FSUPDATED_BY] [varchar](50) NULL,
	[FDUPDATED_DATE] [datetime] NULL,
 CONSTRAINT [ TBDIRECTORY_GROUP] PRIMARY KEY CLUSTERED 
(
	[FNDIR_ID] ASC,
	[FSGROUP_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[TBDIRECTORY_GROUP]  WITH CHECK ADD  CONSTRAINT [FK_TBDIRECTORY_GROUP_TBDIRECTORIES] FOREIGN KEY([FNDIR_ID])
REFERENCES [dbo].[TBDIRECTORIES] ([FNDIR_ID])
GO
ALTER TABLE [dbo].[TBDIRECTORY_GROUP] CHECK CONSTRAINT [FK_TBDIRECTORY_GROUP_TBDIRECTORIES]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Folder代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBDIRECTORY_GROUP', @level2type=N'COLUMN',@level2name=N'FNDIR_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Group代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBDIRECTORY_GROUP', @level2type=N'COLUMN',@level2name=N'FSGROUP_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用權限' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBDIRECTORY_GROUP', @level2type=N'COLUMN',@level2name=N'FCPRIVILEGE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBDIRECTORY_GROUP', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBDIRECTORY_GROUP', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBDIRECTORY_GROUP', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBDIRECTORY_GROUP', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
