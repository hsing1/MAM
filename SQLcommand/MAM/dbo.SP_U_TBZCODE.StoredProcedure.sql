USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/06/02>
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[SP_U_TBZCODE]
	@FSCODE_ID			varchar(10),
	@FSCODE_CODE		varchar(5),
	@FSCODE_NAME		nvarchar(50),
	@FSCODE_ORDER		varchar(2),
	@FSCODE_NOTE		nvarchar(200),
	@FSCODE_ISENABLED	bit,
	@FSUPDATED_BY		varchar(50)
AS
BEGIN
	BEGIN
		UPDATE [TBZCODE]
		SET
			[FSCODE_ID]			= @FSCODE_ID,
			[FSCODE_CODE]		= @FSCODE_CODE,
			[FSCODE_NAME]		= @FSCODE_NAME,
			[FSCODE_ORDER]		= @FSCODE_ORDER,
			[FSCODE_NOTE]		= @FSCODE_NOTE,
			[FSCODE_ISENABLED]	= @FSCODE_ISENABLED,
			
			[FSUPDATED_BY]		= @FSUPDATED_BY,
			[FDUPDATED_DATE]	= GETDATE()
		WHERE
			(FSCODE_ID = @FSCODE_ID) AND  ([FSCODE_CODE] = @FSCODE_CODE)
			
		IF (@@ROWCOUNT = 0)	
			BEGIN
				SELECT RESULT = 'ERROR:沒有受影響的資料列, 修改失敗'
			END	
		ELSE
			BEGIN
				SELECT RESULT = ''
			END	
	END
END


GO
