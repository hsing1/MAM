USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/12/26>
-- Description:   新增區間播出節目報表資料
-- =============================================
Create Proc [dbo].[SP_I_RPT_TBPGM_COMBINE_QUEUE_ZONE_FILE]

@QUERY_KEY	uniqueidentifier,
@QUERY_KEY_SUBID	uniqueidentifier,
@QUERY_BY	varchar(50),
@節目編碼	varchar(7),
@節目集數	int,
@名稱	varchar(50),
@主控編號	varchar(8),
@檔案編號	varchar(16),
@查詢起始日期	date,
@查詢結束日期	date
	
As

Insert Into dbo.RPT_TBPGM_COMBINE_QUEUE_ZONE_FILE
(QUERY_KEY,QUERY_KEY_SUBID,QUERY_BY,QUERY_DATE,
節目編碼,節目集數,名稱,主控編號,
檔案編號,查詢起始日期,查詢結束日期)
Values(@QUERY_KEY,@QUERY_KEY_SUBID,@QUERY_BY,getdate(),
@節目編碼,@節目集數,@名稱,@主控編號,
@檔案編號,@查詢起始日期,@查詢結束日期)
GO
