USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_U_TBLOG_VIDEO_HD_MC] --'0000ASKC','C','PTS_AP'
	@FSVIDEO_ID varchar(10),
	@FCSTATUS varchar(1),
	@FSUPDAYE_BY nvarchar(50)
AS
BEGIN

declare @fsfile_no varchar(16)
declare @bro_ID varchar(12)
declare @CHANGE_FILE_NO varchar(16)

select @fsfile_no=fsfile_no,@bro_ID=FSBRO_ID,@CHANGE_FILE_NO=(case when @FCSTATUS='C' then FSCHANGE_FILE_NO else ''end )from TBLOG_VIDEO where FSVIDEO_PROG=@FSVIDEO_ID and fcfile_status='B'
--select @fsfile_no,@bro_ID,@CHANGE_FILE_NO
--	if(exists(select * from TBLOG_VIDEO_HD_MC where FSFILE_NO=@fsfile_no and FSBRO_ID=@bro_ID and FSVIDEO_ID=@FSVIDEO_ID))
--begin

update TBLOG_VIDEO_HD_MC
set FCSTATUS=@FCSTATUS,FSUPDATED_BY=@FSUPDAYE_BY,FDUPDATED_DATE=GETDATE()
 where 
 FSFILE_NO=@fsfile_no 
 and FSBRO_ID=@bro_ID 
 and FSVIDEO_ID=@FSVIDEO_ID
 and (FCSTATUS=(case when @FCSTATUS='S' then '' else null end)
 or (FCSTATUS=(case when @FCSTATUS='S' then 'E' else null end)
     or FCSTATUS=(case when @FCSTATUS='C' or @FCSTATUS='E' then 'S' else null end)))

	 if(@CHANGE_FILE_NO!='')
	 begin 
	 	UPDATE MAM.dbo.TBLOG_VIDEO  /*入庫影像檔的狀態改成D(被置換)*/
				SET FCFILE_STATUS = 'D'
				WHERE  MAM.dbo.TBLOG_VIDEO.FSFILE_NO=@CHANGE_FILE_NO
				
				UPDATE MAM.dbo.TBLOG_VIDEO_D/*入庫影像檔_D的狀態改成D(被置換)*/
				SET FCFILE_STATUS = 'D'
				WHERE  MAM.dbo.TBLOG_VIDEO_D.FSFILE_NO=@CHANGE_FILE_NO	
	 end

--end
--else
--begin

--insert into TBLOG_VIDEO_HD_MC
--(
--FSBRO_ID,
--FSFILE_NO,
--FSVIDEO_ID,
--FCSTATUS,
--FSCREATED_BY,
--FDCREATED_DATE
--)
--values
--(
--@bro_ID,
--@fsfile_no,
--@FSVIDEO_ID,
--@FCSTATUS,
--@FSUPDAYE_BY,
--GETDATE()
--)
--end
END

GO
