USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Q_TBLOG_DOC_BY_ARCID]

@FSARC_ID char(12),
@FSID varchar(11) ,
@FNEPISODE smallint
As

if(@FSARC_ID<>'')
	begin
		SELECT * FROM MAM.dbo.TBLOG_DOC
		WHERE MAM.dbo.TBLOG_DOC.FSARC_ID=@FSARC_ID
	end
	else
	begin
		select * from MAM.dbo.TBLOG_DOC
		where MAM.dbo.TBLOG_DOC.FSID=@FSID and MAM.dbo.TBLOG_DOC.FNEPISODE=@FNEPISODE and MAM.dbo.TBLOG_DOC.FCFILE_STATUS='Y'
	end
GO
