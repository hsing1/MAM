USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_I_TBTRANSCODE]

@FNTYPE					SMALLINT,
@FNTSMDISPATCH_ID		BIGINT,
@FNPARTIAL_ID			BIGINT,
@FNANYSTREAM_ID			BIGINT,
@FNANYSTREAM_PRIORITY	SMALLINT,
@FSOUTPUT_RESOLUTION	VARCHAR(16),
@FCFULL_RETRIEVE		CHAR(1),
@FCNEED_EXECUTE_PARTIAL	CHAR(1),
@FCNATIVE_FORMAT		CHAR(1),
@FNPROCESS_STATUS		SMALLINT,
@FSPARTIAL_PARAMS		VARCHAR(1024),
@FSANYSTREAM_XML		VARCHAR(2048),
@FSNOTIFY_ADDRESS		VARCHAR(200),
@FSNOTE					NVARCHAR(512)

AS

DECLARE @TRANSCODE_JOBID BIGINT

-- 算出 JobID
IF EXISTS (SELECT * FROM TBTRANSCODE)
	BEGIN
		SELECT @TRANSCODE_JOBID = MAX(FNTRANSCODE_ID)+1 FROM TBTRANSCODE
	END
ELSE
	BEGIN
		SET @TRANSCODE_JOBID = 1
	END

-- 寫入資料表
INSERT INTO TBTRANSCODE(
	  FNTRANSCODE_ID, FNTYPE, FNTSMDISPATCH_ID, FNPARTIAL_ID, FNANYSTREAM_ID, FNANYSTREAM_PRIORITY, FSOUTPUT_RESOLUTION, FCFULL_RETRIEVE, FCNEED_EXECUTE_PARTIAL, FCNATIVE_FORMAT, FNPROCESS_STATUS,FDSTART_TIME,FSPARTIAL_PARAMS, FSANYSTREAM_XML, FSNOTIFY_ADDRESS, FSNOTE)
VALUES(
	@TRANSCODE_JOBID,@FNTYPE,@FNTSMDISPATCH_ID,@FNPARTIAL_ID,@FNANYSTREAM_ID,@FNANYSTREAM_PRIORITY,@FSOUTPUT_RESOLUTION,@FCFULL_RETRIEVE,@FCNEED_EXECUTE_PARTIAL,@FCNATIVE_FORMAT,@FNPROCESS_STATUS,GETDATE(),  @FSPARTIAL_PARAMS,@FSANYSTREAM_XML,@FSNOTIFY_ADDRESS,@FSNOTE)

-- 回傳 JobID
SELECT @TRANSCODE_JOBID AS TRANSCODEID

GO
