USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2015/04/29>
-- Description:   查詢主控播出排帶表
-- =============================================
Create Proc [dbo].[SP_R_RPT_DAY_TAPE_LIST]
	@QUERY_KEY	VARCHAR(36)
As
BEGIN
SELECT 
QUERY_KEY,QUERY_KEY_SUBID,QUERY_BY,QUERY_DATE,
播出日期,頻道,序號,節目名稱,集數,播出時間,FSIN,VIDEO_ID,FSOUT,長度
from RPT_DAY_TAPE_LIST
	WHERE
		(QUERY_KEY = @QUERY_KEY)
	ORDER BY
		序號
END


GO
