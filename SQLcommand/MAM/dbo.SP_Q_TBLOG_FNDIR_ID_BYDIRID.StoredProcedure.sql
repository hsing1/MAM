USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Proc [dbo].[SP_Q_TBLOG_FNDIR_ID_BYDIRID]
@FNDIR_ID Bigint

As
select count(*)as AUDIO_count,''as DOC_count,''as PHOTO_count,''as VIDEO_count,''as VIDEO_D_count FROM TBLOG_AUDIO 
WHERE TBLOG_AUDIO.FNDIR_ID = @FNDIR_ID
union all
select '' as AUDIO_count,count(*)as DOC_count,''as PHOTO_count,''as VIDEO_count,''as VIDEO_D_count FROM TBLOG_DOC 
WHERE TBLOG_DOC.FNDIR_ID = @FNDIR_ID
union all
select '' as AUDIO_count,''as DOC_count,count(*)as PHOTO_count,''as VIDEO_count,''as VIDEO_D_count FROM TBLOG_PHOTO 
WHERE TBLOG_PHOTO.FNDIR_ID = @FNDIR_ID
union all
select '' as AUDIO_count,''as DOC_count,''as PHOTO_count,count(*)as VIDEO_count,''as VIDEO_D_count FROM TBLOG_VIDEO 
WHERE TBLOG_VIDEO.FNDIR_ID = @FNDIR_ID
union all
select '' as AUDIO_count,''as DOC_count,''as PHOTO_count,''as VIDEO_count,count(*)as VIDEO_D_count FROM TBLOG_VIDEO_D 
WHERE TBLOG_VIDEO_D.FNDIR_ID = @FNDIR_ID

GO
