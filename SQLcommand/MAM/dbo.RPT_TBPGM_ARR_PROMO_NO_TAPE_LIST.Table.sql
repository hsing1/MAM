USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RPT_TBPGM_ARR_PROMO_NO_TAPE_LIST](
	[QUERY_KEY] [uniqueidentifier] NOT NULL,
	[QUERY_KEY_SUBID] [uniqueidentifier] NOT NULL,
	[QUERY_BY] [varchar](50) NOT NULL,
	[QUERY_DATE] [datetime] NOT NULL,
	[播出日期] [date] NOT NULL,
	[頻道] [nvarchar](50) NOT NULL,
	[宣傳帶編碼] [nvarchar](50) NOT NULL,
	[宣傳帶名稱] [nvarchar](50) NOT NULL,
	[影像編號] [varchar](16) NULL,
	[主控編號] [varchar](8) NULL,
	[狀態] [varchar](50) NULL,
	[建立者] [varchar](20) NULL,
 CONSTRAINT [PK_RPT_TBPGM_ARR_PROMO_NO_TAPE_LIST] PRIMARY KEY CLUSTERED 
(
	[QUERY_KEY] ASC,
	[QUERY_KEY_SUBID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'報表編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_NO_TAPE_LIST', @level2type=N'COLUMN',@level2name=N'QUERY_KEY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'資料編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_NO_TAPE_LIST', @level2type=N'COLUMN',@level2name=N'QUERY_KEY_SUBID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_NO_TAPE_LIST', @level2type=N'COLUMN',@level2name=N'QUERY_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_NO_TAPE_LIST', @level2type=N'COLUMN',@level2name=N'QUERY_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'播出日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_NO_TAPE_LIST', @level2type=N'COLUMN',@level2name=N'播出日期'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'頻道' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_NO_TAPE_LIST', @level2type=N'COLUMN',@level2name=N'頻道'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'宣傳帶編碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_NO_TAPE_LIST', @level2type=N'COLUMN',@level2name=N'宣傳帶編碼'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'宣傳帶名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_NO_TAPE_LIST', @level2type=N'COLUMN',@level2name=N'宣傳帶名稱'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'影像編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_NO_TAPE_LIST', @level2type=N'COLUMN',@level2name=N'影像編號'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主控編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_NO_TAPE_LIST', @level2type=N'COLUMN',@level2name=N'主控編號'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'狀態' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_NO_TAPE_LIST', @level2type=N'COLUMN',@level2name=N'狀態'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'資料建立者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_NO_TAPE_LIST', @level2type=N'COLUMN',@level2name=N'建立者'
GO
EXEC sys.sp_addextendedproperty @name=N'描述', @value=N'Promo未到帶清單' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_NO_TAPE_LIST'
GO
