USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblEmpInfo](
	[EmpID] [varchar](10) NOT NULL,
	[CName] [varchar](20) NULL,
	[Category1] [varchar](2) NULL,
	[Category1Name] [varchar](20) NULL,
	[NTDomain] [varchar](10) NULL,
	[NTAccount] [varchar](20) NULL,
	[Email1] [varchar](50) NULL,
	[Extension] [varchar](10) NULL,
	[DeptID] [varchar](4) NULL,
	[GroupID] [varchar](4) NULL,
	[UpperDept] [varchar](4) NULL,
	[Station] [varchar](60) NULL,
	[DeptName] [varchar](60) NULL,
	[GroupName] [varchar](60) NULL,
	[OnDuty] [int] NOT NULL,
	[TBSStaff] [int] NOT NULL,
	[PhotoPath] [varchar](85) NULL,
 CONSTRAINT [PK_tblEmpInfo] PRIMARY KEY CLUSTERED 
(
	[EmpID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
