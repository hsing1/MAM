USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   新增短帶預排資料(目前已不使用)
-- =============================================
CREATE Proc [dbo].[SP_I_TBPGM_PROMO_SCHEDULED]

@FNPROMO_BOOKING_NO	int,
@FSPROMO_ID	varchar(12),
@FNNO	int,
@FDDATE	date,
@FSCHANNEL_ID	varchar(2),
@FSTIME	varchar(4),
@FCSTATUS	varchar(1),
@FSCREATED_BY	varchar(50),
@FSUPDATED_BY	varchar(50)


As
insert into TBPGM_PROMO_SCHEDULED(FNPROMO_BOOKING_NO,FSPROMO_ID,FNNO,
FDDATE,FSCHANNEL_ID,FSTIME,FCSTATUS,FSCREATED_BY,FDCREATED_DATE,FSUPDATED_BY,
FDUPDATED_DATE) values(@FNPROMO_BOOKING_NO,@FSPROMO_ID,@FNNO,@FDDATE,
@FSCHANNEL_ID,@FSTIME,@FCSTATUS,@FSCREATED_BY,GETDATE(),@FSUPDATED_BY,GETDATE())



GO
