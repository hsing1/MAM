USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBZPROGNATION](
	[ID] [int] NOT NULL,
	[FSNATION_ID] [nvarchar](50) NULL,
	[FSNATION_NAME] [nvarchar](50) NULL,
	[FDCREATE_DATE] [datetime] NULL,
	[FSCREATE_BY] [nvarchar](50) NULL,
	[FDUPDATE_DATE] [datetime] NULL,
	[FSUPDATE_BY] [nvarchar](50) NULL,
	[FBISENABLE] [bit] NULL,
	[FSSORT] [nvarchar](50) NULL
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'流水編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZPROGNATION', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'國家代碼ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZPROGNATION', @level2type=N'COLUMN',@level2name=N'FSNATION_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'國家名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZPROGNATION', @level2type=N'COLUMN',@level2name=N'FSNATION_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZPROGNATION', @level2type=N'COLUMN',@level2name=N'FDCREATE_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立人員' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZPROGNATION', @level2type=N'COLUMN',@level2name=N'FSCREATE_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZPROGNATION', @level2type=N'COLUMN',@level2name=N'FDUPDATE_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新人員' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZPROGNATION', @level2type=N'COLUMN',@level2name=N'FSUPDATE_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否作用中' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZPROGNATION', @level2type=N'COLUMN',@level2name=N'FBISENABLE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'排序' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZPROGNATION', @level2type=N'COLUMN',@level2name=N'FSSORT'
GO
