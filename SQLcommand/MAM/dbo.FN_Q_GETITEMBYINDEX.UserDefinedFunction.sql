USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/03/14>
-- Description:	<根據傳入之@List代碼串(使用;串),@i索引值,取出第@List中索引值為@i之項目(第一個項目索引值為0>
-- =============================================
CREATE FUNCTION [dbo].[FN_Q_GETITEMBYINDEX]
(
	@List varchar(500) , @i int
)
RETURNS	VARCHAR(20)
AS
BEGIN
	Declare @idx1 int
	Declare @idx2 int
	declare @j int
	Set @idx1=0
	Set @idx2=0  
	Set @j=0

	WHILE (@j<=@i) 
	BEGIN
		IF @idx2 = Len(@List)
			BEGIN
				Set @idx1 = @idx2
			END
		ELSE
			BEGIN
				Set @idx1 = @idx2+1
				Set @idx2 = CHARINDEX(';', SUBSTRING(@List,@idx1+1,Len(@List)-@idx1))+@idx1
				IF SUBSTRING(@List,@idx1,1)=';'
					BEGIN
						Set @idx2= @idx1 
					END
			END
		Set @j = @j+1
	END
 

	IF @idx2= @idx1
		BEGIN
			RETURN''
		END
	ELSE
		BEGIN
			RETURN SUBSTRING(@List,@idx1,@idx2-@idx1) 
		END	

	RETURN''

END

GO
