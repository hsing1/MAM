USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Q_TBPROGMEN]

@FSPROG_ID char(7) ,
@FNEPISODE smallint ,
@FSTITLEID char(2) ,
@FSSTAFFID char(10)
As
SELECT * FROM MAM.dbo.TBPROGMEN
where @FSPROG_ID = case when @FSPROG_ID ='' then @FSPROG_ID else FSPROG_ID end
and @FNEPISODE = case when @FNEPISODE ='' then @FNEPISODE else FNEPISODE end
and @FSTITLEID = case when @FSTITLEID ='' then @FSTITLEID else FSTITLEID end
and @FSSTAFFID = case when @FSSTAFFID ='' then @FSSTAFFID else FSSTAFFID end
order by FSPROG_ID,FNEPISODE
GO
