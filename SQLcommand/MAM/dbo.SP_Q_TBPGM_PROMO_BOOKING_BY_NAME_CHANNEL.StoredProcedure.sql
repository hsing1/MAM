USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create date:   <2015/03/11>
-- Description:   查詢短帶託播單資料
-- =============================================
CREATE Proc [dbo].[SP_Q_TBPGM_PROMO_BOOKING_BY_NAME_CHANNEL]

@FNPROMO_BOOKING_NO	int,
@FSPROMO_ID	varchar(12),
@FCSTATUS	varchar(1),
@FSPROMO_NAME nvarchar(50),
@FSUSER_ChtName nvarchar(50),
@FDBDATE nvarchar(50),
@FDEDATE nvarchar(50),
@FSCHANNEL_ID nvarchar(2)
As

Select DISTINCT A.FNPROMO_BOOKING_NO,A.FSPROMO_ID,A.FCSTATUS,B.FSPROMO_NAME,B.FSDURATION,A.FSMEMO
,A.FSCREATED_BY,C.FSUSER_ChtName,A.FDCREATED_DATE,
FSCHANNEL_NAME=stuff((select distinct ','+ FSCHANNEL_NAME from TBPGM_PROMO_EFFECTIVE_ZONE t,TBZCHANNEL Z where T.FSCHANNEL_ID=Z.FSCHANNEL_ID and t.FnPromo_booking_no=A.FnPromo_booking_no  for xml path('')), 1, 1, '')
From dbo.TBPGM_PROMO_BOOKING A
left join TBPGM_PROMO B on A.FSPROMO_ID=B.FSPROMO_ID 
left join TBUSERS C on A.FSCREATED_BY=C.FSUSER_ID
inner join TBPGM_PROMO_EFFECTIVE_ZONE D on A.FNPROMO_BOOKING_NO=D.FNPROMO_BOOKING_NO
where B.FSPROMO_NAME like '%' + @FSPROMO_NAME + '%'
and C.FSUSER_ChtName like '%' + @FSUSER_ChtName + '%'
and @FSPROMO_ID = case when @FSPROMO_ID = '' then @FSPROMO_ID else A.FSPROMO_ID end
and @FNPROMO_BOOKING_NO = case when @FNPROMO_BOOKING_NO = '' then @FNPROMO_BOOKING_NO else A.FNPROMO_BOOKING_NO end
and @FDBDATE <= case when @FDBDATE = '' then @FDBDATE else CONVERT(date, A.FDCREATED_DATE, 111) end
and @FDEDATE >= case when @FDEDATE = '' then @FDEDATE else CONVERT(date, A.FDCREATED_DATE, 111) end
and @FSCHANNEL_ID = case when @FSCHANNEL_ID = '' then @FSCHANNEL_ID else D.FSCHANNEL_ID end

GO
