USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Q_TBLOG_VIDEO_BY_BROID] --'201507190001'
@FSBRO_ID char(12)

As
SELECT * FROM MAM.dbo.TBLOG_VIDEO
WHERE MAM.dbo.TBLOG_VIDEO.FSBRO_ID=@FSBRO_ID --and FCFILE_STATUS!='X'
order by FCFILE_STATUS desc

GO
