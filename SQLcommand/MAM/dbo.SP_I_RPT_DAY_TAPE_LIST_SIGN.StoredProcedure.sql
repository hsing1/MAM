USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2015/04/29>
-- Description:   主控播出排帶簽收表
-- =============================================
CREATE Proc [dbo].[SP_I_RPT_DAY_TAPE_LIST_SIGN]

@QUERY_KEY	uniqueidentifier,
@QUERY_KEY_SUBID	uniqueidentifier,
@QUERY_BY	varchar(50),
@播出日期	date,
@頻道	nvarchar(50),
@序號	int,
@節目名稱	nvarchar(50),
@集數	int,
@VIDEO_ID	varchar(8),
@長度	varchar(11),
@帶別		varchar(20)

As
Insert Into RPT_DAY_TAPE_LIST_SIGN(
QUERY_KEY,QUERY_KEY_SUBID,QUERY_BY,QUERY_DATE,
播出日期,頻道,序號,節目名稱,集數,VIDEO_ID,長度,帶別
)
Values(@QUERY_KEY,@QUERY_KEY_SUBID,@QUERY_BY,Getdate(),
@播出日期,@頻道,@序號,@節目名稱,@集數,@VIDEO_ID,@長度,@帶別
)
GO
