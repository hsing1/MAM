USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<sam.wu>
-- Create date: <2011/6/1>
-- Description:	<透過節目編號+集別，找到段落資料>
-- =============================================
CREATE Proc [dbo].[SP_Q_PTSTAPE_LABEL_CLIP_BYPROGID_EPISODE]
@FSPROG_ID char(7),
@FNEPISODE smallint

as

declare @str varchar(20) = 'v.@FNEPISODE'
declare @str2 varchar(20) = '@FNEPISODE'

select *,
ISNULL(CONVERT(VARCHAR(20) ,LabelClip.CreateDT , 111),'') as WANT_FDCREATEDDATE,
ISNULL(CONVERT(VARCHAR(20) ,LabelClip.UpdateDT , 111),'') as WANT_FDUPDATEDDATE
--,REPLACE(LabelClip.ClipTitle,REPLACE(@str, '@epno', @FNEPISODE),'') as Compare ,
--REPLACE(LabelClip.ClipTitle,REPLACE(@str, '@epno', @FNEPISODE),'') as Compare2 
from  [PTSTape].[PTSTape].[dbo].[tblLLabelProg] LabelProg,
[PTSTape].[PTSTape].[dbo].[tblLLabelClip] LabelClip

where 
LabelProg.Bib=LabelClip.bib
and  LabelProg.progid=@FSPROG_ID and LabelProg.epno=@FNEPISODE
and  
(
(
LabelClip.ClipTitle like (REPLACE(@str, '@FNEPISODE', @FNEPISODE)+'-%') or LabelClip.ClipTitle = (REPLACE(@str, '@FNEPISODE', @FNEPISODE)) 
or									--影帶管理系統當許多集打在同一筆資料時，就要用v.?來取段落資料，或是沒有v.，直接打集別加段落資料
LabelClip.ClipTitle like (REPLACE(@str2, '@FNEPISODE', @FNEPISODE)+'-%') or LabelClip.ClipTitle = (REPLACE(@str2, '@FNEPISODE', @FNEPISODE))
)
)
--SP_Q_PTSTAPE_LABEL_CLIP_BYPROGID_EPISODE '2008783','1'

GO
