USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBPGM_PROMO_BOOKING_SETTING](
	[FNPROMO_BOOKING_NO] [int] NOT NULL,
	[FSTIME1] [varchar](10) NULL,
	[FSTIME2] [varchar](10) NULL,
	[FSTIME3] [varchar](10) NULL,
	[FSTIME4] [varchar](10) NULL,
	[FSTIME5] [varchar](10) NULL,
	[FSTIME6] [varchar](10) NULL,
	[FSTIME7] [varchar](10) NULL,
	[FSTIME8] [varchar](10) NULL,
	[FSCMNO] [varchar](10) NULL,
 CONSTRAINT [PK_TBPGM_PROMO_BOOKING_SETTING] PRIMARY KEY CLUSTERED 
(
	[FNPROMO_BOOKING_NO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'短帶託播單編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_BOOKING_SETTING', @level2type=N'COLUMN',@level2name=N'FNPROMO_BOOKING_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'時段檔次1次數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_BOOKING_SETTING', @level2type=N'COLUMN',@level2name=N'FSTIME1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'時段檔次2次數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_BOOKING_SETTING', @level2type=N'COLUMN',@level2name=N'FSTIME2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'時段檔次3次數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_BOOKING_SETTING', @level2type=N'COLUMN',@level2name=N'FSTIME3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'時段檔次4次數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_BOOKING_SETTING', @level2type=N'COLUMN',@level2name=N'FSTIME4'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'時段檔次5次數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_BOOKING_SETTING', @level2type=N'COLUMN',@level2name=N'FSTIME5'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'時段檔次6次數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_BOOKING_SETTING', @level2type=N'COLUMN',@level2name=N'FSTIME6'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'時段檔次7次數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_BOOKING_SETTING', @level2type=N'COLUMN',@level2name=N'FSTIME7'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'時段檔次8(其他)次數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_BOOKING_SETTING', @level2type=N'COLUMN',@level2name=N'FSTIME8'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'其他名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_BOOKING_SETTING', @level2type=N'COLUMN',@level2name=N'FSCMNO'
GO
EXEC sys.sp_addextendedproperty @name=N'描述', @value=N'短帶託播單時段檔次資料表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO_BOOKING_SETTING'
GO
