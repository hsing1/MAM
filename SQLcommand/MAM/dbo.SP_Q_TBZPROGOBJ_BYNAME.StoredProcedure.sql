USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Q_TBZPROGOBJ_BYNAME]
@FSPROGOBJNAME NVARCHAR (50)

As
select b.FSUSER_ChtName as FSCREATED_BY_NAME , C.FSUSER_ChtName as FSUPDATED_BY_NAME ,
        ISNULL(CONVERT(VARCHAR(20) ,a.FDCREATED_DATE , 111),'') as CONVERT_FDCREATED_DATE ,
		ISNULL(CONVERT(VARCHAR(20) ,a.FDUPDATED_DATE , 111),'') as CONVERT_FDUPDATED_DATE,
		 a.* FROM  TBUSERS as b INNER JOIN
               TBZPROGOBJ  as a ON b.FSUSER_ID = a.FSCREATED_BY LEFT OUTER JOIN
               TBUSERS AS c  ON a.FSUPDATED_BY = c.FSUSER_ID
where a.FSCREATED_BY  = b.FSUSER_ID 
AND  a.FSPROGOBJNAME like @FSPROGOBJNAME +'%'
GO
