USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   �s�WLouth Key���
-- =============================================
CREATE Proc [dbo].[SP_I_TBPGM_LOUTH_KEY]
@FSCHANNEL_ID	varchar(2),
@FSGROUP	nvarchar(10),
@FSNAME	nvarchar(50),
@FSNO	varchar(4),
@FSMEMO	nvarchar(100),
@FSLAYEL	varchar(1),
@FSRULE	varchar(20)

As
begin
  Insert Into dbo.TBPGM_LOUTH_KEY(FSCHANNEL_ID,FSGROUP,FSNAME,FSNO,FSMEMO,FSLAYEL,FSRULE)
  Values(@FSCHANNEL_ID,@FSGROUP,@FSNAME,@FSNO,@FSMEMO,@FSLAYEL,@FSRULE)
end




GO
