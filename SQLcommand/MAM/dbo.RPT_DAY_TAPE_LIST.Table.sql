USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RPT_DAY_TAPE_LIST](
	[QUERY_KEY] [uniqueidentifier] NULL,
	[QUERY_KEY_SUBID] [uniqueidentifier] NULL,
	[QUERY_BY] [varchar](50) NULL,
	[QUERY_DATE] [date] NULL,
	[播出日期] [date] NULL,
	[頻道] [nvarchar](50) NULL,
	[序號] [int] NULL,
	[節目名稱] [nvarchar](50) NULL,
	[集數] [int] NULL,
	[播出時間] [varchar](11) NULL,
	[FSIN] [varchar](4) NULL,
	[VIDEO_ID] [varchar](8) NULL,
	[FSOUT] [varchar](4) NULL,
	[長度] [varchar](11) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
