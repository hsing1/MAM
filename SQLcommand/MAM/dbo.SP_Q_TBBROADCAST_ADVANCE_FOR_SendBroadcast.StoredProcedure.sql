USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        
-- Create date: 
-- Description: 送帶轉檔檔檔的所有資料_BY進階查詢轉檔(狀態為G-待轉檔以及狀態為I-已轉檔，都要找出來)
-- Update desc:    <2013/04/18><Jarvis.Yang>加入註解。
-- Update: 2015-06-30，查詢時有輸入單號時無視其他搜尋條件

-- =============================================

CREATE Proc [dbo].[SP_Q_TBBROADCAST_ADVANCE_FOR_SendBroadcast] --'','','','','','','','2016-01-01','2016-06-25'
@FCCHECK_STATUS char(1),
@FSBRO_ID CHAR(12) ,
@FSBRO_TYPE CHAR(1) ,
@FSID VARCHAR(11) ,
@FNEPISODE varchar(5),
@FCCHANGE char(1),
@FSCREATED_BY varchar(50) ,
@SDate		datetime,
@EDate		datetime

As
if(@FSBRO_ID!='')
begin
select ISNULL(b.FSUSER_ChtName,'') as FSCREATED_BY_NAME, 
ISNULL(b.FSEMAIL,'') as FSCREATED_BY_EMAIL, 
isnull(C.FSUSER_ChtName,'') as FSUPDATED_BY_NAME ,
'' as FSBRO_BY_NAME ,
'' as FSCHECK_BY_NAME ,
ISNULL(CONVERT(VARCHAR(20) ,a.FDBRO_DATE , 111),'') as WANT_FDBRO_DATE,
ISNULL(CONVERT(VARCHAR(20) ,a.FDCREATED_DATE , 111),'') as WANT_FDCREATEDDATE,
--ISNULL(CONVERT(VARCHAR(20) ,a.FDUPDATED_DATE , 111),'') as WANT_FDUPDATEDATE,
--video.FSARC_TYPE,
FSPROG_ID_NAME		= ((case when video.FSARC_TYPE='001' then '(SD)' else '(HD)' end) +(CASE a.FSBRO_TYPE WHEN 'G' THEN ISNULL(PROG_M.FSPGMNAME,'') WHEN 'P' THEN PROMO.FSPROMO_NAME WHEN 'D' THEN DATA.FSDATA_NAME ELSE '' END)),
FNEPISODE_NAME	    = CASE a.FSBRO_TYPE WHEN 'G' THEN ISNULL(PROG_D.FSPGDNAME,'') WHEN 'P' THEN '' ELSE '' END,
FSCHANNELID		    = CASE a.FSBRO_TYPE WHEN 'G' THEN ISNULL(PROG_M.FSCHANNEL_ID,'') WHEN 'P' THEN PROMO.FSMAIN_CHANNEL_ID   ELSE '' END,
--ISNULL(d.FSPGMNAME,'') as FSPROG_ID_NAME,
--ISNULL(e.FSPGDNAME,'') as FNEPISODE_NAME,
ISNULL(hm.FCSTATUS,'') as 'HD_MC_Status',
--ISNULL(video.FCFILE_STATUS,'') as 'FCFILE_STATUS' ,
--'B' as 'FCFILE_STATUS' ,
--video.FSARC_TYPE as 'Video_ArcType',
a.* FROM TBBROADCAST  as a 
left outer JOIN TBUSERS as b on a.FSCREATED_BY=b.FSUSER_ID
left outer join TBUSERS as c on a.FSUPDATED_BY=c.FSUSER_ID
--left outer join TBUSERS as f on a.FSBRO_BY=f.FSUSER_ID
--left outer join TBUSERS as g on a.FSCHECK_BY=g.FSUSER_ID
--left outer join TBPROG_M as d on a.FSID=d.FSPROG_ID -----[MAM].[dbo].[TBPROG_M]
--left outer join TBPROG_D as e on a.FSID=e.FSPROG_ID and a.FNEPISODE=e.FNEPISODE
LEFT JOIN TBPROG_M AS PROG_M ON (a.FSBRO_TYPE = 'G') AND (PROG_M.FSPROG_ID = a.FSID) 
LEFT JOIN TBPROG_D AS PROG_D ON (a.FSBRO_TYPE = 'G') AND (a.FNEPISODE <> 0) AND (PROG_D.FSPROG_ID = a.FSID) AND (PROG_D.FNEPISODE = a.FNEPISODE)
LEFT JOIN TBPGM_PROMO AS PROMO ON (a.FSBRO_TYPE = 'P') AND (PROMO.FSPROMO_ID = a.FSID)				
LEFT JOIN TBDATA AS DATA ON (a.FSBRO_TYPE = 'D') AND (DATA.FSDATA_ID = a.FSID)	
left outer join TBLOG_VIDEO_HD_MC hm on hm.FSBRO_ID =a.FSBRO_ID
join (select FSBRO_ID,FSARC_TYPE from TBLOG_VIDEO ) video on  video.FSBRO_ID=a.FSBRO_ID
where 
 a.FSBRO_ID =  @FSBRO_ID
 and FCCHECK_STATUS in('N','R','U','E')
--AND (a.FCCHECK_STATUS = 'G' or a.FCCHECK_STATUS = 'I' or a.FCCHECK_STATUS = 'T')	--G:線上簽收,I:轉檔,T:影帶回溯
--AND @FSBRO_TYPE = case when @FSBRO_TYPE ='' then @FSBRO_TYPE else a.FSBRO_TYPE end
--AND @FSID = case when @FSID ='' then @FSID else a.FSID end
--AND @FNEPISODE = case when @FNEPISODE ='' then @FNEPISODE else a.FNEPISODE end
--AND @FSCREATED_BY = case when @FSCREATED_BY ='' then @FSCREATED_BY else a.FSCREATED_BY end

order by a.FDCREATED_DATE desc
end
else
begin
select ISNULL(b.FSUSER_ChtName,'') as FSCREATED_BY_NAME, 
ISNULL(b.FSEMAIL,'') as FSCREATED_BY_EMAIL, 
isnull(C.FSUSER_ChtName,'') as FSUPDATED_BY_NAME ,
'' as FSBRO_BY_NAME ,
'' as FSCHECK_BY_NAME ,
ISNULL(CONVERT(VARCHAR(20) ,a.FDBRO_DATE , 111),'') as WANT_FDBRO_DATE,
ISNULL(CONVERT(VARCHAR(20) ,a.FDCREATED_DATE , 111),'') as WANT_FDCREATEDDATE,
--video.FSARC_TYPE,
FSPROG_ID_NAME		= ((case when video.FSARC_TYPE='001' then '(SD)' else '(HD)' end) +(CASE a.FSBRO_TYPE WHEN 'G' THEN ISNULL(PROG_M.FSPGMNAME,'') WHEN 'P' THEN PROMO.FSPROMO_NAME WHEN 'D' THEN DATA.FSDATA_NAME ELSE '' END)),
FNEPISODE_NAME	    = CASE a.FSBRO_TYPE WHEN 'G' THEN ISNULL(PROG_D.FSPGDNAME,'') WHEN 'P' THEN '' ELSE '' END,
FSCHANNELID		    = CASE a.FSBRO_TYPE WHEN 'G' THEN ISNULL(PROG_M.FSCHANNEL_ID,'') WHEN 'P' THEN PROMO.FSMAIN_CHANNEL_ID   ELSE '' END,
--ISNULL(d.FSPGMNAME,'') as FSPROG_ID_NAME,
--ISNULL(e.FSPGDNAME,'') as FNEPISODE_NAME,
ISNULL(hm.FCSTATUS,'') as 'HD_MC_Status',
--ISNULL(video.FCFILE_STATUS,'') as 'FCFILE_STATUS' ,
--'B' as 'FCFILE_STATUS' ,
a.* FROM TBBROADCAST  as a 
left outer JOIN TBUSERS as b on a.FSCREATED_BY=b.FSUSER_ID
left outer join TBUSERS as c on a.FSUPDATED_BY=c.FSUSER_ID
--left outer join TBUSERS as f on a.FSBRO_BY=f.FSUSER_ID
--left outer join TBUSERS as g on a.FSCHECK_BY=g.FSUSER_ID
--left outer join TBPROG_M as d on a.FSID=d.FSPROG_ID -----[MAM].[dbo].[TBPROG_M]
--left outer join TBPROG_D as e on a.FSID=e.FSPROG_ID and a.FNEPISODE=e.FNEPISODE
LEFT JOIN TBPROG_M AS PROG_M ON (a.FSBRO_TYPE = 'G') AND (PROG_M.FSPROG_ID = a.FSID) 
LEFT JOIN TBPROG_D AS PROG_D ON (a.FSBRO_TYPE = 'G') AND (a.FNEPISODE <> 0) AND (PROG_D.FSPROG_ID = a.FSID) AND (PROG_D.FNEPISODE = a.FNEPISODE)
LEFT JOIN TBPGM_PROMO AS PROMO ON (a.FSBRO_TYPE = 'P') AND (PROMO.FSPROMO_ID = a.FSID)				
LEFT JOIN TBDATA AS DATA ON (a.FSBRO_TYPE = 'D') AND (DATA.FSDATA_ID = a.FSID)	
left outer join TBLOG_VIDEO_HD_MC hm on hm.FSBRO_ID =a.FSBRO_ID
 join (select FSBRO_ID,FSARC_TYPE from TBLOG_VIDEO ) video on video.FSBRO_ID=a.FSBRO_ID
where 
--(a.FCCHECK_STATUS = 'G' or a.FCCHECK_STATUS = 'I' or a.FCCHECK_STATUS = 'T')	--G:線上簽收,I:轉檔,T:影帶回溯
--AND 
a.FDCREATED_DATE >= CONVERT(datetime, @SDate )
AND a.FDCREATED_DATE<= CONVERT(datetime,@EDate) 
--(CONVERT(NVARCHAR(10), a.FDCREATED_DATE, 111) BETWEEN @SDate AND @EDate )
and a.FCCHANGE=case when @FCCHANGE='' then a.FCCHANGE else @FCCHANGE end
AND @FSBRO_ID = case when @FSBRO_ID ='' then @FSBRO_ID else a.FSBRO_ID end
AND @FSBRO_TYPE = case when @FSBRO_TYPE ='' then @FSBRO_TYPE else a.FSBRO_TYPE end
AND @FSID = case when @FSID ='' then @FSID else a.FSID end
AND @FNEPISODE = case when @FNEPISODE ='' then @FNEPISODE else case when @FNEPISODE='0' and @FSBRO_TYPE='G' then @FNEPISODE else convert(varchar,a.FNEPISODE,5) end end
AND @FSCREATED_BY = case when @FSCREATED_BY ='' then @FSCREATED_BY else a.FSCREATED_BY end
AND a.FCCHECK_STATUS= case when @FCCHECK_STATUS='' then a.FCCHECK_STATUS else @FCCHECK_STATUS end 
--and a.FSBRO_ID in(select distinct FSBRO_ID from TBLOG_VIDEO where FSARC_TYPE='002') 
and FCCHECK_STATUS in('N','R','U','E')
order by a.FDCREATED_DATE desc
end
-- and (CONVERT(NVARCHAR(10), a.FDCREATED_DATE, 111) BETWEEN (CONVERT(NVARCHAR(10),DATEADD(month,-1,GETDATE()), 111))AND (CONVERT(NVARCHAR(10),GETDATE(), 111)) ) 
--(CONVERT(NVARCHAR(10),DATEADD(month,-1,GETDATE()), 111))  取一個月前的寫法

GO
