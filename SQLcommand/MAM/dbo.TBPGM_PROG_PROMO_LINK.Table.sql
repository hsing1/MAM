USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBPGM_PROG_PROMO_LINK](
	[FSPROG_ID] [char](7) NOT NULL,
	[FNSEQNO] [int] NOT NULL,
	[FSPROMO_ID] [varchar](12) NOT NULL,
	[FNNO] [int] NOT NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
	[FSUPDATED_BY] [varchar](50) NULL,
	[FDUPDATED_DATE] [datetime] NULL,
	[FSCHANNEL_ID] [varchar](2) NOT NULL,
 CONSTRAINT [PK_TBPGM_PROG_PROMO_LINK] PRIMARY KEY CLUSTERED 
(
	[FSPROG_ID] ASC,
	[FNSEQNO] ASC,
	[FSPROMO_ID] ASC,
	[FNNO] ASC,
	[FSCHANNEL_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目編碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROG_PROMO_LINK', @level2type=N'COLUMN',@level2name=N'FSPROG_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'播映序號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROG_PROMO_LINK', @level2type=N'COLUMN',@level2name=N'FNSEQNO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'短帶編碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROG_PROMO_LINK', @level2type=N'COLUMN',@level2name=N'FSPROMO_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'序號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROG_PROMO_LINK', @level2type=N'COLUMN',@level2name=N'FNNO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROG_PROMO_LINK', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROG_PROMO_LINK', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROG_PROMO_LINK', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROG_PROMO_LINK', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'描述', @value=N'節目與短帶連結表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROG_PROMO_LINK'
GO
