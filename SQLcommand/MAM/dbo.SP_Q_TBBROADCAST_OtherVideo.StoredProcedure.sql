USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[SP_Q_TBBROADCAST_OtherVideo] --'','','','','admin','2015-01-01','2016-12-31'
@FSBRO_ID CHAR(12) ,
@FSBRO_TYPE CHAR(1) ,
@FSID VARCHAR(11) ,
@FNEPISODE varchar(4),
@FSCREATED_BY varchar(50) ,
@SDate		varchar(10),
@EDate		varchar(10)
AS
BEGIN
select ISNULL(b.FSUSER_ChtName,'') as FSCREATED_BY_NAME, 
ISNULL(b.FSEMAIL,'') as FSCREATED_BY_EMAIL, 
isnull(C.FSUSER_ChtName,'') as FSUPDATED_BY_NAME ,
isnull(f.FSUSER_ChtName,'') as FSBRO_BY_NAME ,
isnull(g.FSUSER_ChtName,'') as FSCHECK_BY_NAME ,
ISNULL(CONVERT(VARCHAR(20) ,a.FDBRO_DATE , 111),'') as WANT_FDBRO_DATE,
ISNULL(CONVERT(VARCHAR(20) ,a.FDCREATED_DATE , 111),'') as WANT_FDCREATEDDATE,
FSPROG_ID_NAME		= CASE a.FSBRO_TYPE WHEN 'G' THEN ISNULL(PROG_M.FSPGMNAME,'') WHEN 'P' THEN PROMO.FSPROMO_NAME  ELSE '' END,
FNEPISODE_NAME	    = CASE a.FSBRO_TYPE WHEN 'G' THEN ISNULL(PROG_D.FSPGDNAME,'') WHEN 'P' THEN '' ELSE '' END,
FSCHANNELID		    = CASE a.FSBRO_TYPE WHEN 'G' THEN ISNULL(PROG_M.FSCHANNEL_ID,'') WHEN 'P' THEN PROMO.FSMAIN_CHANNEL_ID   ELSE '' END,
--ISNULL(d.FSPGMNAME,'') as FSPROG_ID_NAME,
--ISNULL(e.FSPGDNAME,'') as FNEPISODE_NAME,
a.* FROM TBBROADCAST  as a 
left outer JOIN TBUSERS as b on a.FSCREATED_BY=b.FSUSER_ID
left outer join TBUSERS as c on a.FSUPDATED_BY=c.FSUSER_ID
left outer join TBUSERS as f on a.FSBRO_BY=f.FSUSER_ID
left outer join TBUSERS as g on a.FSCHECK_BY=g.FSUSER_ID
--left outer join TBPROG_M as d on a.FSID=d.FSPROG_ID -----[MAM].[dbo].[TBPROG_M]
--left outer join TBPROG_D as e on a.FSID=e.FSPROG_ID and a.FNEPISODE=e.FNEPISODE
LEFT JOIN TBPROG_M AS PROG_M ON (a.FSBRO_TYPE = 'G') AND (PROG_M.FSPROG_ID = a.FSID) 
LEFT JOIN TBPROG_D AS PROG_D ON (a.FSBRO_TYPE = 'G') AND (a.FNEPISODE <> 0) AND (PROG_D.FSPROG_ID = a.FSID) AND (PROG_D.FNEPISODE = a.FNEPISODE)
LEFT JOIN TBPGM_PROMO AS PROMO ON (a.FSBRO_TYPE = 'P') AND (PROMO.FSPROMO_ID = a.FSID)				
--left outer join TBLOG_VIDEO v on v.fsbro_id=a.fsbro_id		
where 
cast( a.FDCREATED_DATE as date) BETWEEN cast(@SDate as date) AND cast( @EDate as date) 
AND @FSBRO_ID = case when @FSBRO_ID ='' then @FSBRO_ID else a.FSBRO_ID end
AND @FSBRO_TYPE = case when @FSBRO_TYPE ='' then @FSBRO_TYPE else a.FSBRO_TYPE end
AND @FSID = case when @FSID ='' then @FSID else a.FSID end
AND @FNEPISODE = case when @FNEPISODE ='' then @FNEPISODE else a.FNEPISODE end
AND @FSCREATED_BY = case when @FSCREATED_BY ='' then @FSCREATED_BY else a.FSCREATED_BY end
AND a.FCCHECK_STATUS in('K','J')
 --and a.fsbro_id in(select distinct fsbro_id from TBLOG_VIDEO where FSARC_TYPE='026' )-- v.FSARC_TYPE='026'
order by a.FDCREATED_DATE desc

END

GO
