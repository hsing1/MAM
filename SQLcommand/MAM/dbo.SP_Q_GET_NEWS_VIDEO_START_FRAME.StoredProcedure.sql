USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_Q_GET_NEWS_VIDEO_START_FRAME]
@FSFILE_NO varchar(20),
@FNSEQ_NO int
AS

SELECT ISNULL(FSTIMECODE1,'0') AS FSTIMECODE1 FROM PTS_MAM.PTS_MAM.dbo.VW_SEARCH_NEWS_START_FRAME
WHERE FSFILE_NO=@FSFILE_NO AND FNSEQ_NO=@FNSEQ_NO
GO
