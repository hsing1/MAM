USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBPGM_COMBINE_QUEUE](
	[FNCONBINE_NO] [int] NULL,
	[FCTIMETYPE] [char](1) NULL,
	[FSPROG_ID] [char](7) NOT NULL,
	[FNSEQNO] [int] NOT NULL,
	[FSPROG_NAME] [nvarchar](50) NULL,
	[FDDATE] [date] NOT NULL,
	[FSCHANNEL_ID] [varchar](2) NOT NULL,
	[FNEPISODE] [int] NULL,
	[FNBREAK_NO] [int] NOT NULL,
	[FNSUB_NO] [int] NULL,
	[FSVIDEO_ID] [varchar](8) NULL,
	[FCPROPERTY] [varchar](1) NOT NULL,
	[FSPLAY_TIME] [varchar](11) NOT NULL,
	[FSDUR] [varchar](11) NOT NULL,
	[FNLIVE] [int] NULL,
	[FSPROMO_ID] [varchar](12) NULL,
	[FSMEMO] [nvarchar](500) NOT NULL,
	[FSMEMO1] [nvarchar](100) NULL,
	[FSSIGNAL] [nvarchar](50) NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
	[FSFILE_NO] [varchar](16) NULL,
 CONSTRAINT [ TBPGMCOMBINE_QUEUE] PRIMARY KEY CLUSTERED 
(
	[FSPROG_ID] ASC,
	[FNSEQNO] ASC,
	[FDDATE] ASC,
	[FSCHANNEL_ID] ASC,
	[FNBREAK_NO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
CREATE NONCLUSTERED INDEX [INDEX_FSVIDEO_ID_FDDATE] ON [dbo].[TBPGM_COMBINE_QUEUE]
(
	[FSVIDEO_ID] ASC,
	[FDDATE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'序號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_COMBINE_QUEUE', @level2type=N'COLUMN',@level2name=N'FNCONBINE_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'時間類型(O:固定,A:連續)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_COMBINE_QUEUE', @level2type=N'COLUMN',@level2name=N'FCTIMETYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目編碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_COMBINE_QUEUE', @level2type=N'COLUMN',@level2name=N'FSPROG_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'播映序號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_COMBINE_QUEUE', @level2type=N'COLUMN',@level2name=N'FNSEQNO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_COMBINE_QUEUE', @level2type=N'COLUMN',@level2name=N'FSPROG_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'播出日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_COMBINE_QUEUE', @level2type=N'COLUMN',@level2name=N'FDDATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'頻道' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_COMBINE_QUEUE', @level2type=N'COLUMN',@level2name=N'FSCHANNEL_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目集數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_COMBINE_QUEUE', @level2type=N'COLUMN',@level2name=N'FNEPISODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目段落' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_COMBINE_QUEUE', @level2type=N'COLUMN',@level2name=N'FNBREAK_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_COMBINE_QUEUE', @level2type=N'COLUMN',@level2name=N'FNSUB_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主控編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_COMBINE_QUEUE', @level2type=N'COLUMN',@level2name=N'FSVIDEO_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'類型(G:節目,P:短帶)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_COMBINE_QUEUE', @level2type=N'COLUMN',@level2name=N'FCPROPERTY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'播出時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_COMBINE_QUEUE', @level2type=N'COLUMN',@level2name=N'FSPLAY_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'長度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_COMBINE_QUEUE', @level2type=N'COLUMN',@level2name=N'FSDUR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否為Live(1:Live)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_COMBINE_QUEUE', @level2type=N'COLUMN',@level2name=N'FNLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'短帶編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_COMBINE_QUEUE', @level2type=N'COLUMN',@level2name=N'FSPROMO_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'備註(2nd Event)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_COMBINE_QUEUE', @level2type=N'COLUMN',@level2name=N'FSMEMO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'備註' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_COMBINE_QUEUE', @level2type=N'COLUMN',@level2name=N'FSMEMO1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'訊號源' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_COMBINE_QUEUE', @level2type=N'COLUMN',@level2name=N'FSSIGNAL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_COMBINE_QUEUE', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_COMBINE_QUEUE', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'檔案編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_COMBINE_QUEUE', @level2type=N'COLUMN',@level2name=N'FSFILE_NO'
GO
