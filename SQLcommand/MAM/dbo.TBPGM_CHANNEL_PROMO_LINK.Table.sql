USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBPGM_CHANNEL_PROMO_LINK](
	[FSPROMO_ID] [varchar](12) NOT NULL,
	[FSCHANNEL_ID] [varchar](2) NOT NULL,
	[FNNO] [int] NOT NULL,
	[FCLINK_TYPE] [char](1) NOT NULL,
	[FCTIME_TYPE] [char](1) NOT NULL,
	[FCINSERT_TYPE] [char](1) NOT NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
	[FSUPDATED_BY] [varchar](50) NULL,
	[FDUPDATED_DATE] [datetime] NULL,
 CONSTRAINT [ TBPGMCHANNEL_PROMO_LINK] PRIMARY KEY CLUSTERED 
(
	[FSPROMO_ID] ASC,
	[FSCHANNEL_ID] ASC,
	[FNNO] ASC,
	[FCLINK_TYPE] ASC,
	[FCTIME_TYPE] ASC,
	[FCINSERT_TYPE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'短帶編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_CHANNEL_PROMO_LINK', @level2type=N'COLUMN',@level2name=N'FSPROMO_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'頻道編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_CHANNEL_PROMO_LINK', @level2type=N'COLUMN',@level2name=N'FSCHANNEL_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'序號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_CHANNEL_PROMO_LINK', @level2type=N'COLUMN',@level2name=N'FNNO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'連結類型(節目前,節目中,最後一段)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_CHANNEL_PROMO_LINK', @level2type=N'COLUMN',@level2name=N'FCLINK_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'時間類型(全部,單點,雙點,每三小時)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_CHANNEL_PROMO_LINK', @level2type=N'COLUMN',@level2name=N'FCTIME_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'插入類型(全部,循環)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_CHANNEL_PROMO_LINK', @level2type=N'COLUMN',@level2name=N'FCINSERT_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_CHANNEL_PROMO_LINK', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_CHANNEL_PROMO_LINK', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_CHANNEL_PROMO_LINK', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_CHANNEL_PROMO_LINK', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'描述', @value=N'頻道與短帶連結表(紀錄當編排某個頻道時要預先插入的短帶)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_CHANNEL_PROMO_LINK'
GO
