USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/05/24>
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[SP_U_TBLOG_VIDEO_RESET]
	@FILE_NO	VARCHAR(16),
	@FSGUID		VARCHAR(36)
AS
BEGIN
	UPDATE	TBLOG_VIDEO
	SET		FCKEYFRAME = 'N'
	WHERE	(FSFILE_NO = @FILE_NO)
		
	UPDATE	TBLOG_WORKER
	SET		FSRESULT		= 'Reset',
			FDUPDATED_DATE	= GETDATE()
	WHERE	(CAST(FSGUID AS VARCHAR(36)) = @FSGUID)
	
	SELECT RESULT = @@ROWCOUNT
END





GO
