USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   查詢短帶運行表未到清單報表
-- =============================================
CREATE PROCEDURE [dbo].[SP_R_TBPGM_ARR_PROMO_NO_TAPE_LIST_QRY]
	@QUERY_KEY	VARCHAR(36)
AS
BEGIN
	SELECT
		[QUERY_KEY]
      ,[QUERY_KEY_SUBID]
      ,[QUERY_BY]
      ,[QUERY_DATE]
      ,[播出日期]
      ,[頻道]
      ,[宣傳帶編碼]
      ,[宣傳帶名稱]
      ,[影像編號]
      ,[主控編號]
      ,[狀態]
      ,[建立者]
	FROM
		RPT_TBPGM_ARR_PROMO_NO_TAPE_LIST
	WHERE
		(QUERY_KEY = @QUERY_KEY)
	ORDER BY
		[播出日期]
END
GO
