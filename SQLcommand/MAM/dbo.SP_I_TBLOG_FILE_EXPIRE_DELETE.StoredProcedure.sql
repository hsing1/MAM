USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Proc [dbo].[SP_I_TBLOG_FILE_EXPIRE_DELETE]
@FSTABLE_NAME nvarchar(70) ,
@FSRECORD nvarchar(300),
@FSUPDATED_BY nvarchar(50) 


--///////////////////////////////////////////
--//冠宇用於插入資料至到期日期及ACTION狀態
--///////////////////////////////////////////
As
Insert Into TBLOG_FILE_EXPIRE_DELETE(FSTABLE_NAME,FSRECORD,FSUPDATED_BY)
Values(@FSTABLE_NAME,@FSRECORD,@FSUPDATED_BY)



GO
