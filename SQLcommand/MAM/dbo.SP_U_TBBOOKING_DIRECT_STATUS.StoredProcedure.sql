USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <David.Sin>
-- Create date:   <2010/10/02>
-- Description:   更新可直接調用審核結果
-- =============================================

CREATE PROC [dbo].[SP_U_TBBOOKING_DIRECT_STATUS]
@FSBOOKING_NO varchar(12)
AS

UPDATE TBBOOKING_DETAIL
SET FCCHECK='Y',FSCHECK_ID='mamadmin',
FDCHECK_DATE=CONVERT(varchar(10),GETDATE(),111)
WHERE FSBOOKING_NO=@FSBOOKING_NO
GO
