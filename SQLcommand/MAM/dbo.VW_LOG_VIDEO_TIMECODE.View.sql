USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_LOG_VIDEO_TIMECODE]
AS
SELECT     TOP (100) PERCENT dbo.TBLOG_VIDEO.FSID AS FSPROGID, dbo.TBLOG_VIDEO.FNEPISODE, dbo.TBFILE_TYPE.FSARC_TYPE, dbo.TBFILE_TYPE.FSTYPE_NAME, 
                      dbo.TBLOG_VIDEO.FSBEG_TIMECODE, dbo.TBLOG_VIDEO.FSEND_TIMECODE, dbo.FN_Q_GET_SECONDS_FROM_TIMECODE(dbo.TBLOG_VIDEO.FSBEG_TIMECODE, 
                      dbo.TBLOG_VIDEO.FSEND_TIMECODE) AS SHOW_SECONDS, dbo.TBPROG_M.FSPGMNAME, dbo.TBPROG_M.FSWEBNAME, dbo.TBPROG_D.FSPGDNAME, 
                      dbo.TBPROG_D.FNSHOWEPISODE
FROM         dbo.TBFILE_TYPE INNER JOIN
                      dbo.TBLOG_VIDEO ON dbo.TBFILE_TYPE.FSARC_TYPE = dbo.TBLOG_VIDEO.FSARC_TYPE INNER JOIN
                      dbo.TBPROG_D ON dbo.TBLOG_VIDEO.FNEPISODE = dbo.TBPROG_D.FNEPISODE AND dbo.TBLOG_VIDEO.FSID = dbo.TBPROG_D.FSPROG_ID INNER JOIN
                      dbo.TBPROG_M ON dbo.TBLOG_VIDEO.FSID = dbo.TBPROG_M.FSPROG_ID
WHERE     (dbo.TBFILE_TYPE.FSSHOW_TIMECODE = 'Y') AND (dbo.TBLOG_VIDEO.FCFILE_STATUS = 'T') AND (dbo.TBFILE_TYPE.FBISENABLE = 1) OR
                      (dbo.TBFILE_TYPE.FSSHOW_TIMECODE = 'Y') AND (dbo.TBLOG_VIDEO.FCFILE_STATUS = 'Y') AND (dbo.TBFILE_TYPE.FBISENABLE = 1)
ORDER BY FSPROGID, dbo.TBLOG_VIDEO.FNEPISODE

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "TBFILE_TYPE"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 218
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TBLOG_VIDEO"
            Begin Extent = 
               Top = 6
               Left = 256
               Bottom = 125
               Right = 446
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TBPROG_D"
            Begin Extent = 
               Top = 6
               Left = 484
               Bottom = 125
               Right = 676
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TBPROG_M"
            Begin Extent = 
               Top = 6
               Left = 714
               Bottom = 125
               Right = 906
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_LOG_VIDEO_TIMECODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_LOG_VIDEO_TIMECODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_LOG_VIDEO_TIMECODE'
GO
