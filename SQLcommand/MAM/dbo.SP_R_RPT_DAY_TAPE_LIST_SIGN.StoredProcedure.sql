USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2015/04/29>
-- Description:   查詢主控播出排帶簽表
-- =============================================
CREATE Proc [dbo].[SP_R_RPT_DAY_TAPE_LIST_SIGN]
	@QUERY_KEY	VARCHAR(36)
As
BEGIN
SELECT 
QUERY_KEY,QUERY_KEY_SUBID,QUERY_BY,QUERY_DATE,
播出日期,頻道,序號,節目名稱,集數,VIDEO_ID,長度,帶別
from RPT_DAY_TAPE_LIST_SIGN
	WHERE
		(QUERY_KEY = @QUERY_KEY)
	ORDER BY
		序號
END

GO
