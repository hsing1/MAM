USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        
-- Create date: 
-- Description:   新增節目製作人檔
-- Update desc:    <2013/04/17><Jarvis.Yang>加入註解。
-- =============================================

CREATE Proc [dbo].[SP_I_TBPROG_PRODUCER]

@FSPRO_TYPE char(1) ,
@FSID varchar(11) ,
@FSPRODUCER varchar(50) ,
@FSCREATED_BY varchar(50) ,
@FSUPDATED_BY varchar(50) 

As
begin
--續製的話,三項PK可能都相同,要擋掉
declare @count int
select @count=COUNT(*) from TBPROG_PRODUCER where FSID=@FSID and FSPRODUCER=@FSPRODUCER and FSPRO_TYPE=@FSPRO_TYPE

if(@count=0)
Insert Into TBPROG_PRODUCER(FSPRO_TYPE,FSID,FSPRODUCER,FSCREATED_BY,FDCREATED_DATE,FSUPDATED_BY,FDUPDATED_DATE)
Values(@FSPRO_TYPE,@FSID,@FSPRODUCER,@FSCREATED_BY,GETDATE(),@FSUPDATED_BY,GETDATE())
end














GO
