USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================

-- Author:        <David.Sin>

-- Create date:   <2014/09/24>

-- Description:   查詢匯出節目明細報表

-- =============================================

CREATE PROC [dbo].[SP_Q_RPT_TBPROGRAM_VIDEO_DETAIL]
@FSGUID VARCHAR(50)

AS

SELECT
	[檔案編號],
	[節目名稱],
	[集別],
	[子集名稱],
	[子集內容],
	[TimeCode],
	[檔案類別],
	[附檔名],
	[檔案類型],
	[段落資訊],
	[影片格式],
	[Video格式],
	[Audio格式]
FROM
	[dbo].[RPT_TBPROGRAM_VIDEO_DETAIL]
WHERE
	[FSGUID] = @FSGUID
GO
