USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBPGM_PROMO](
	[FSPROMO_ID] [varchar](11) NOT NULL,
	[FSPROMO_NAME] [nvarchar](50) NOT NULL,
	[FSPROMO_NAME_ENG] [varchar](50) NULL,
	[FSDURATION] [char](11) NULL,
	[FSPROG_ID] [char](7) NULL,
	[FNEPISODE] [smallint] NULL,
	[FSPROMOTYPEID] [char](2) NULL,
	[FSPROGOBJID] [varchar](2) NOT NULL,
	[FSPRDCENID] [varchar](5) NOT NULL,
	[FSPRDYYMM] [varchar](6) NULL,
	[FSACTIONID] [char](2) NULL,
	[FSPROMOCATID] [char](2) NULL,
	[FSPROMOCATDID] [char](2) NULL,
	[FSPROMOVERID] [char](2) NULL,
	[FSMAIN_CHANNEL_ID] [char](2) NULL,
	[FSMEMO] [nvarchar](500) NULL,
	[FSDEL] [char](1) NULL,
	[FSDELUSER] [varchar](50) NULL,
	[FNDEP_ID] [int] NULL,
	[FSWELFARE] [nvarchar](100) NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
	[FSUPDATED_BY] [varchar](50) NULL,
	[FDUPDATED_DATE] [datetime] NULL,
	[FDEXPIRE_DATE] [datetime] NULL,
	[FSEXPIRE_DATE_ACTION] [nvarchar](10) NULL,
 CONSTRAINT [ TBPGMPROMO] PRIMARY KEY CLUSTERED 
(
	[FSPROMO_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[TBPGM_PROMO] ADD  CONSTRAINT [DF_TBPGM_PROMO_FDEXPIRE_DATE]  DEFAULT ('1900-1-1') FOR [FDEXPIRE_DATE]
GO
ALTER TABLE [dbo].[TBPGM_PROMO] ADD  CONSTRAINT [DF_TBPGM_PROMO_FSEXPIRE_DATE_ACTION]  DEFAULT ('Y') FOR [FSEXPIRE_DATE_ACTION]
GO
ALTER TABLE [dbo].[TBPGM_PROMO]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPGM_PROMO_TBPGM_ACTION] FOREIGN KEY([FSACTIONID])
REFERENCES [dbo].[TBPGM_ACTION] ([FSACTIONID])
GO
ALTER TABLE [dbo].[TBPGM_PROMO] CHECK CONSTRAINT [FK_TBPGM_PROMO_TBPGM_ACTION]
GO
ALTER TABLE [dbo].[TBPGM_PROMO]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPGM_PROMO_TBPROMOCATDID] FOREIGN KEY([FSPROMOCATDID], [FSPROMOCATID])
REFERENCES [dbo].[TBZPROMOCATD] ([FSPROMOCATDID], [FSPROMOCATID])
GO
ALTER TABLE [dbo].[TBPGM_PROMO] CHECK CONSTRAINT [FK_TBPGM_PROMO_TBPROMOCATDID]
GO
ALTER TABLE [dbo].[TBPGM_PROMO]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPGM_PROMO_TBPROMOCATID] FOREIGN KEY([FSPROMOCATID])
REFERENCES [dbo].[TBZPROMOCAT] ([FSPROMOCATID])
GO
ALTER TABLE [dbo].[TBPGM_PROMO] CHECK CONSTRAINT [FK_TBPGM_PROMO_TBPROMOCATID]
GO
ALTER TABLE [dbo].[TBPGM_PROMO]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPGM_PROMO_TBZCHANNEL] FOREIGN KEY([FSMAIN_CHANNEL_ID])
REFERENCES [dbo].[TBZCHANNEL] ([FSCHANNEL_ID])
GO
ALTER TABLE [dbo].[TBPGM_PROMO] CHECK CONSTRAINT [FK_TBPGM_PROMO_TBZCHANNEL]
GO
ALTER TABLE [dbo].[TBPGM_PROMO]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPGM_PROMO_TBZPROMOTYPE] FOREIGN KEY([FSPROMOTYPEID])
REFERENCES [dbo].[TBZPROMOTYPE] ([FSPROMOTYPEID])
GO
ALTER TABLE [dbo].[TBPGM_PROMO] CHECK CONSTRAINT [FK_TBPGM_PROMO_TBZPROMOTYPE]
GO
ALTER TABLE [dbo].[TBPGM_PROMO]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPGM_PROMO_TBZPROMOVER] FOREIGN KEY([FSPROMOVERID])
REFERENCES [dbo].[TBZPROMOVER] ([FSPROMOVERID])
GO
ALTER TABLE [dbo].[TBPGM_PROMO] CHECK CONSTRAINT [FK_TBPGM_PROMO_TBZPROMOVER]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'宣傳帶編碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO', @level2type=N'COLUMN',@level2name=N'FSPROMO_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'宣傳帶中文名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO', @level2type=N'COLUMN',@level2name=N'FSPROMO_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'宣傳帶英文名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO', @level2type=N'COLUMN',@level2name=N'FSPROMO_NAME_ENG'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FSDURATION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO', @level2type=N'COLUMN',@level2name=N'FSDURATION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO', @level2type=N'COLUMN',@level2name=N'FSPROG_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'集別' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO', @level2type=N'COLUMN',@level2name=N'FNEPISODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'宣傳帶類別' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO', @level2type=N'COLUMN',@level2name=N'FSPROMOTYPEID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'製作目的代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO', @level2type=N'COLUMN',@level2name=N'FSPROGOBJID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'製作單位代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO', @level2type=N'COLUMN',@level2name=N'FSPRDCENID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'製作年月' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO', @level2type=N'COLUMN',@level2name=N'FSPRDYYMM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'宣傳帶活動代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO', @level2type=N'COLUMN',@level2name=N'FSACTIONID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'宣傳帶類型代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO', @level2type=N'COLUMN',@level2name=N'FSPROMOCATID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'宣傳帶類型細項代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO', @level2type=N'COLUMN',@level2name=N'FSPROMOCATDID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'宣傳帶版本代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO', @level2type=N'COLUMN',@level2name=N'FSPROMOVERID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'頻道別代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO', @level2type=N'COLUMN',@level2name=N'FSMAIN_CHANNEL_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'備註' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO', @level2type=N'COLUMN',@level2name=N'FSMEMO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否刪除' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO', @level2type=N'COLUMN',@level2name=N'FSDEL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'刪除者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO', @level2type=N'COLUMN',@level2name=N'FSDELUSER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'部門ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO', @level2type=N'COLUMN',@level2name=N'FNDEP_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'描述', @value=N'短帶資料表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_PROMO'
GO
