USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_I_TBPROG_D_BATCH_EXTERNAL]

@FSPROGID char(7) ,
@MAXEPISODE smallint ,
@FSPGDNAME nvarchar(50) ,
@FSPGDENAME nvarchar(100) ,
@FNLENGTH smallint ,
@FNTAPELENGTH int ,
@FSPROGSRCID varchar(2) ,
@FSPROGATTRID varchar(2) ,
@FSPROGAUDID varchar(2) ,
@FSPROGTYPEID varchar(2) ,
@FSPROGGRADEID varchar(2) ,
@FSPROGLANGID1 varchar(2) ,
@FSPROGLANGID2 varchar(2) ,
@FSPROGSALEID varchar(2) ,
@FSPROGBUYID varchar(2) ,
@FSPROGBUYDID varchar(2) ,
@FCRACE varchar(1) ,
@FSSHOOTSPEC char(50) ,
@FSPROGSPEC varchar(100) ,
@FSPRDYEAR varchar(4) ,
@FSPROGGRADE varchar(100) ,
@FSCONTENT text ,
@FSMEMO varchar(500) ,
@FNSHOWEPISODE smallint ,
@FSCRTUSER varchar(5) ,
@FSPROGNATIONID varchar(2)				--國家來源ID

As

DECLARE @_i INT
DECLARE @_MAX INT
DECLARE @dbCount INT
 SET @_i = 1
 SET @_MAX = @MAXEPISODE -- 要產生幾筆子集資料
 WHILE (@_i<@_MAX+1)
 BEGIN
 
		--要迴圈的語法
		IF NOT EXISTS(SELECT FSPROGID FROM [PTSProg].[PTSProgram].[dbo].TBPROG_D WHERE (FSPROGID = @FSPROGID AND FNEPISODE=@_i))	--檢查是否已存在此@FSPROG_ID
		BEGIN
			INSERT [PTSProg].[PTSProgram].[dbo].TBPROG_D
			(FSPROGID,FNEPISODE,FSPGDNAME,FSPGDENAME,FNLENGTH,FNTAPELENGTH,FSPROGSRCID,FSPROGATTRID,FSPROGAUDID,FSPROGTYPEID,FSPROGGRADEID,FSPROGLANGID1,FSPROGLANGID2,FSPROGSALEID,FSPROGBUYID,FSPROGBUYDID,FCRACE,FSSHOOTSPEC,FSPROGSPEC,FSPRDYEAR,FSPROGGRADE,FSCONTENT,FSMEMO,FNSHOWEPISODE,FSCRTUSER,FDCRTDATE,FSPROGNATIONID)
			Values(@FSPROGID,
			@_i,
			case when @FSPGDNAME ='' then null else @FSPGDNAME end,
			case when @FSPGDENAME ='' then null else @FSPGDENAME end,
			case when @FNLENGTH ='0' then null else @FNLENGTH end,
			case when @FNTAPELENGTH ='0' then null else @FNTAPELENGTH end,
			@FSPROGSRCID,
			@FSPROGATTRID,
			@FSPROGAUDID,
			@FSPROGTYPEID,
			@FSPROGGRADEID,
			@FSPROGLANGID1,
			@FSPROGLANGID2,
			case when @FSPROGSALEID ='' then null else @FSPROGSALEID end,
			case when @FSPROGBUYID ='' then null else @FSPROGBUYID end,
			case when @FSPROGBUYDID ='' then null else @FSPROGBUYDID end,
			@FCRACE,
			case when @FSSHOOTSPEC ='' then null else MAM.dbo.FN_Q_SET_CODELIST_TO_PURESTRING(@FSSHOOTSPEC,'50') end,
			case when @FSPROGSPEC ='' then null else MAM.dbo.FN_Q_SET_CODELIST_TO_PURESTRING(@FSPROGSPEC,'50') end,			
			case when @FSPRDYEAR ='' then null else @FSPRDYEAR end,
			case when @FSPROGGRADE ='' then null else @FSPROGGRADE end,
			@FSCONTENT,
			case when @FSMEMO ='' then null else @FSMEMO end,
			@_i,
			'01065',
			GETDATE(),
			@FSPROGNATIONID)
		END	
		
		--加1
		Set @_i=@_i+1
END

GO
