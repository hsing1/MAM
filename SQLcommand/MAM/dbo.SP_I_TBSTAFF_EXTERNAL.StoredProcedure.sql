USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_I_TBSTAFF_EXTERNAL]
	@FSSTAFFID	varchar(5) ,
	@FSNAME		varchar(100) ,
	@FSENAME	varchar(100) ,
	@FSTEL		varchar(50) ,
	@FSFAX		varchar(20) ,
	@FSEMAIL	varchar(50) ,
	@FSADDRESS	varchar(150) ,
	@FSWEBSITE	varchar(200) ,
	@FSINTRO	text ,
	@FSEINTRO	text ,
	@FSMEMO		varchar(200) 
As
--Insert Into [PTSProg].[PTSProgram].[dbo].TBSTAFF(FSSTAFFID,FSNAME,FSENAME,FSTEL,FSFAX,FSEMAIL,FSADDRESS,FSWEBSITE,FSINTRO,FSEINTRO,FSMEMO,FSCRTUSER,FDCRTDATE)
--Values(@FSSTAFFID,@FSNAME,@FSENAME,@FSTEL,@FSFAX,@FSEMAIL,@FSADDRESS,@FSWEBSITE,@FSINTRO,@FSEINTRO,@FSMEMO,'01065',GETDATE())

--Dennis.Wen:寫法改嚴謹一點

IF EXISTS(	SELECT	*
			FROM	[PTSProg].[PTSProgram].[dbo].TBSTAFF
			WHERE	(FSNAME = @FSNAME))
	BEGIN
		SELECT RESULT = 'ERROR:已存在此中文姓名'
	END
ELSE
	BEGIN
		BEGIN TRY
			SET @FSSTAFFID = (	SELECT TOP 1 FSSTAFFID
								FROM [PTSProg].[PTSProgram].[dbo].TBSTAFF
								ORDER BY FSSTAFFID DESC )
								
			SET @FSSTAFFID = RIGHT('00000' + CAST((CAST(@FSSTAFFID AS INT) + 1) AS VARCHAR(5)),5)
		
			Insert Into [PTSProg].[PTSProgram].[dbo].TBSTAFF(FSSTAFFID,FSNAME,FSENAME,FSTEL,FSFAX,FSEMAIL,FSADDRESS,FSWEBSITE,FSINTRO,FSEINTRO,FSMEMO,FSCRTUSER,FDCRTDATE)
			Values(
			@FSSTAFFID,
			@FSNAME,
			case when @FSENAME ='' then null else @FSENAME end,
			case when @FSTEL ='' then null else @FSTEL end,
			case when @FSFAX ='' then null else @FSFAX end,
			case when @FSEMAIL ='' then null else @FSEMAIL end,
			case when @FSADDRESS ='' then null else @FSADDRESS end,
			case when @FSWEBSITE ='' then null else @FSWEBSITE end,
			@FSINTRO,
			@FSEINTRO,
			case when @FSMEMO ='' then null else @FSMEMO end,	
			'01065',
			GETDATE())
			
			IF (@@ROWCOUNT = 0)
				BEGIN
					SELECT RESULT = 'ERROR:沒有受影響的資料列'
				END
			ELSE
				BEGIN
					SELECT RESULT = @FSSTAFFID
				END
		END TRY
		BEGIN CATCH
			SELECT RESULT = 'ERROR:' + ERROR_MESSAGE()
		END CATCH
	END

GO
