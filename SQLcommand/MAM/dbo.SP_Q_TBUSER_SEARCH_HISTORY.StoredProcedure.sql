USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <David.Sin>
-- Create date:   <2010/10/02>
-- Description:   查詢使用者檢索條件記錄
-- =============================================

CREATE Proc [dbo].[SP_Q_TBUSER_SEARCH_HISTORY]
@FSUSER_ID varchar(50),
@FDSEARCH_DATE_BEGIN varchar(20),
@FDSEARCH_DATE_END varchar(20)
As
Select TOP 20 FNNO,Convert(varchar(10),FDSEARCH_DATE,111) As FDSEARCH_DATE,
		FSKEY_WORD,FSINDEX,
		Case FSCREATED_DATE_BEGIN
			When '' Then''
			Else FSCREATED_DATE_BEGIN+'~'+FSCREATED_DATE_END
		End As FDDATE,
		FSCHANNEL,FSCOLUMN1,FSVALUE1,FSCOLUMN2,FSVALUE2,FSCOLUMN3,FSVALUE3,FSLOGIC1,FSLOGIC2,
		Case FSSEARCH_MODE
			When '0' Then '一般查詢'
			When '1' Then '模糊查詢'
		End As FSSEARCH_MODE_NAME,FSSEARCH_MODE,
		Case FSHOMOPHONE
			When '0' Then '不啟用'
			When '1' Then '啟用'
		End As FSHOMOPHONE_NAME,FSHOMOPHONE,
		Case FSSYNONYM
			When '0' Then '不啟用'
			When '1' Then '啟用'
		End As FSSYNONYM_NAME,FSSYNONYM,
		FNPAGE_SIZE,
		FNSTART_POINT,
		FSGET_COLUMN,
		FSTREE_NODE,
		ISNULL(FSORDERBY,'') AS FSORDERBY,
		ISNULL(FSORDERTYPE,'') AS FSORDERTYPE
From TBUSER_SEARCH_HISTORY
Where FSUSER_ID=@FSUSER_ID
And Convert(varchar(10),FDSEARCH_DATE,111) Between @FDSEARCH_DATE_BEGIN And @FDSEARCH_DATE_END
Order By FNNO Desc


GO
