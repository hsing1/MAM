USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBNORECORD](
	[FSNO] [varchar](12) NOT NULL,
	[FSTYPE] [char](2) NOT NULL,
	[FSNO_DATE] [char](8) NULL,
	[FSNOTE] [nvarchar](50) NOT NULL,
	[FSCREATED_BY] [varchar](50) NULL,
	[FDCREATED_DATE] [datetime] NULL,
 CONSTRAINT [PK_TBNORECORD] PRIMARY KEY CLUSTERED 
(
	[FSNO] ASC,
	[FSTYPE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'取號記錄' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBNORECORD', @level2type=N'COLUMN',@level2name=N'FSNO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'取號種類(目前都用07)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBNORECORD', @level2type=N'COLUMN',@level2name=N'FSTYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'取號日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBNORECORD', @level2type=N'COLUMN',@level2name=N'FSNO_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'取號註記' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBNORECORD', @level2type=N'COLUMN',@level2name=N'FSNOTE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBNORECORD', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBNORECORD', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
