USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE Proc [dbo].[SP_U_TBPROG_D_ALL_NINE]

@FSPROG_ID char(7) ,
@FSPROGSRCID varchar(2) ,
@FSPROGATTRID varchar(2) ,
@FSPROGAUDID varchar(2) ,
@FSPROGTYPEID varchar(2) ,
@FSPROGLANGID1 varchar(2) ,
@FSPROGLANGID2 varchar(2) ,
@FSPROGBUYID varchar(2) ,
@FSPROGBUYDID varchar(2) ,
@FSPROGSALEID varchar(2) ,
@FSPROGNATIONID varchar(2),
@bolPROGSRCID  char(1) ,
@bolPROGATTRID  char(1) ,
@bolPROGAUDID  char(1) ,
@bolPROGTYPEID  char(1) ,
@bolPROGLANGID1  char(1) ,
@bolPROGLANGID2  char(1) ,
@bolPROGBUYID  char(1) ,
@bolPROGBUYDIDS  char(1) ,
@bolPROGSALEID  char(1) ,
@bolPROGNATIONID char(1),
@FSUPDUSER varchar(5)

As
UPDATE MAM.dbo.TBPROG_D
SET 
FSPROGSRCID = case when @bolPROGSRCID ='Y' then @FSPROGSRCID else FSPROGSRCID end,
FSPROGATTRID = case when @bolPROGATTRID ='Y' then @FSPROGATTRID else FSPROGATTRID end,
FSPROGAUDID = case when @bolPROGAUDID ='Y' then @FSPROGAUDID else FSPROGAUDID end,
FSPROGTYPEID = case when @bolPROGTYPEID ='Y' then @FSPROGTYPEID else FSPROGTYPEID end,
FSPROGLANGID1 = case when @bolPROGLANGID1 ='Y' then @FSPROGLANGID1 else FSPROGLANGID1 end,
FSPROGLANGID2 = case when @bolPROGLANGID2 ='Y' then @FSPROGLANGID2 else FSPROGLANGID2 end,
FSPROGBUYID = case when @bolPROGBUYID ='Y' then @FSPROGBUYID else FSPROGBUYID end,
FSPROGBUYDID = case when @bolPROGBUYDIDS ='Y' then @FSPROGBUYDID else FSPROGBUYDID end,
FSPROGSALEID = case when @bolPROGSALEID ='Y' then @FSPROGSALEID else FSPROGSALEID end,
FSPROGNATIONID=case when @bolPROGNATIONID='Y' then @FSPROGNATIONID else FSPROGNATIONID end,
FSUPDUSER=@FSUPDUSER,
FDUPDDATE=GETDATE()
WHERE  MAM.dbo.TBPROG_D.FSPROG_ID=@FSPROG_ID AND MAM.dbo.TBPROG_D.FSDEL <>'Y'


GO
