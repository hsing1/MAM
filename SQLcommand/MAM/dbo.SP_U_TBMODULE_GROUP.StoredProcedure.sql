USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE Proc [dbo].[SP_U_TBMODULE_GROUP]
@FSMODULE_ID char(7), 
@FSGROUP_ID varchar(20), 
@FCHasPermission varchar(5) 
As
IF @FCHasPermission = 'False' 
Begin 
	DELETE FROM TBMODULE_GROUP WHERE FSMODULE_ID = @FSMODULE_ID AND FSGROUP_ID = @FSGROUP_ID
END 

Else 
Begin 
	Declare @brtn int 
	set @brtn=0
	select @brtn = Count(*)
	from TBMODULE_GROUP
	WHERE FSMODULE_ID = @FSMODULE_ID AND FSGROUP_ID = @FSGROUP_ID 

	if @brtn = 0 
	Begin 
		INSERT INTO TBMODULE_GROUP(FSMODULE_ID, FSGROUP_ID) VALUES 
		(@FSMODULE_ID, @FSGROUP_ID)
	End 
	
End


GO
