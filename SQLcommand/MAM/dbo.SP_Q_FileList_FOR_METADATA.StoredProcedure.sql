USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jarvis.Yang
-- Create date: 2013-07-10
-- Description:	根據頻道、類別、年分 查詢節目(or短帶or資料帶)清單
-- =============================================
create PROCEDURE [dbo].[SP_Q_FileList_FOR_METADATA] 
	 @type char,
 @year char(4),
 @channelName nvarchar(50)
 AS
BEGIN

declare @dirID int

select @dirID=FNDIR_ID from TBDIRECTORIES where FSDIR=@channelName

select @dirID=FNDIR_ID from TBDIRECTORIES where FNPARENT_ID=@dirID and FSDIR=@year

if(@type='P')
begin


select p.FSPROMO_ID as 'ID',d.* from TBDIRECTORIES d ,TBPGM_PROMO p
WHERE d.FSDIR COLLATE Chinese_Taiwan_Stroke_BIN = p.FSPROMO_NAME COLLATE Chinese_Taiwan_Stroke_BIN
and LEFT(p.FSPROMO_ID,4)=@year
and  d.FNPARENT_ID=@dirID
end

else if (@type='G')
begin

select m.FSPROG_ID as 'ID',d.* from TBDIRECTORIES d ,TBPROG_M m
WHERE d.FSDIR COLLATE Chinese_Taiwan_Stroke_BIN = m.FSPGMNAME COLLATE Chinese_Taiwan_Stroke_BIN
and LEFT(m.FSPROG_ID,4)=@year
and  d.FNPARENT_ID=@dirID
end

else if (@type='D')
begin

select da.FSDATA_ID as 'ID', d.* from TBDIRECTORIES d ,TBDATA da
WHERE d.FSDIR COLLATE Chinese_Taiwan_Stroke_BIN = da.FSDATA_NAME COLLATE Chinese_Taiwan_Stroke_BIN
and LEFT(da.FSDATA_ID,4)=@year
and  d.FNPARENT_ID=@dirID
end
END




GO
