USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBTAPE_LOOKUP](
	[FSPROG_ID] [char](7) NOT NULL,
	[FNEPISODE] [smallint] NOT NULL,
	[FSBIB] [varchar](7) NOT NULL,
	[FSNOTE] [nvarchar](200) NOT NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
	[FSUPDATED_BY] [varchar](50) NULL,
	[FDUPDATED_DATE] [datetime] NULL,
	[EXEC_GUID] [varchar](36) NOT NULL,
 CONSTRAINT [PK_TBTAPE_LOOKUP] PRIMARY KEY CLUSTERED 
(
	[FSPROG_ID] ASC,
	[FNEPISODE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
CREATE NONCLUSTERED INDEX [IX_TBTAPE_LOOKUP] ON [dbo].[TBTAPE_LOOKUP]
(
	[FSBIB] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TBTAPE_LOOKUP] ADD  CONSTRAINT [DF_TBTAPE_LOOKUP_FSPROG_ID]  DEFAULT ('') FOR [FSPROG_ID]
GO
ALTER TABLE [dbo].[TBTAPE_LOOKUP] ADD  CONSTRAINT [DF_TBTAPE_LOOKUP_FSBIB]  DEFAULT ('') FOR [FSBIB]
GO
ALTER TABLE [dbo].[TBTAPE_LOOKUP] ADD  CONSTRAINT [DF_TBTAPE_LOOKUP_FSNOTE]  DEFAULT ('') FOR [FSNOTE]
GO
ALTER TABLE [dbo].[TBTAPE_LOOKUP] ADD  CONSTRAINT [DF_TBTAPE_LOOKUP_FSCREATED_BY]  DEFAULT ('') FOR [FSCREATED_BY]
GO
ALTER TABLE [dbo].[TBTAPE_LOOKUP] ADD  CONSTRAINT [DF_TBTAPE_LOOKUP_FDCREATED_DATE]  DEFAULT (getdate()) FOR [FDCREATED_DATE]
GO
ALTER TABLE [dbo].[TBTAPE_LOOKUP] ADD  CONSTRAINT [DF_TBTAPE_LOOKUP_FSUPDATED_BY]  DEFAULT ('') FOR [FSUPDATED_BY]
GO
ALTER TABLE [dbo].[TBTAPE_LOOKUP] ADD  CONSTRAINT [DF_TBTAPE_LOOKUP_FDUPDATED_DATE]  DEFAULT ('1900/01/01') FOR [FDUPDATED_DATE]
GO
ALTER TABLE [dbo].[TBTAPE_LOOKUP] ADD  CONSTRAINT [DF_TBTAPE_LOOKUP_EXEC_GUID]  DEFAULT ('') FOR [EXEC_GUID]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目編碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBTAPE_LOOKUP', @level2type=N'COLUMN',@level2name=N'FSPROG_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'集別' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBTAPE_LOOKUP', @level2type=N'COLUMN',@level2name=N'FNEPISODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'影帶識別碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBTAPE_LOOKUP', @level2type=N'COLUMN',@level2name=N'FSBIB'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'備註' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBTAPE_LOOKUP', @level2type=N'COLUMN',@level2name=N'FSNOTE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBTAPE_LOOKUP', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBTAPE_LOOKUP', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBTAPE_LOOKUP', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBTAPE_LOOKUP', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
