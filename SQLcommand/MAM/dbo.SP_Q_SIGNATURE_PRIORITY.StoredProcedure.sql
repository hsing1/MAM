USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  Proc [dbo].[SP_Q_SIGNATURE_PRIORITY]
As


--Declare @SQL varchar(255) 
--set @SQL = '' 


SELECT a.FNDEP_ID,c.FSFullName_ChtName,a.FSSignatureID,b.FSUSER_ChtName
  FROM [MAM].[dbo].[TBUSER_DEP_SIGNATURE_PRIORITY] as a
  left outer join MAM.dbo.TBUSERS as b on a.FSSignatureID=b.FSUSER_ID
  left outer join MAM.dbo.TBUSER_DEP as c on a.FNDEP_ID=c.FNDEP_ID


--exec (@SQL)

GO
