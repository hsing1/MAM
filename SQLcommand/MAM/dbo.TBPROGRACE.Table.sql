USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBPROGRACE](
	[AUTO] [bigint] IDENTITY(1,1) NOT NULL,
	[FSPROG_ID] [char](7) NOT NULL,
	[FNEPISODE] [smallint] NOT NULL,
	[FSAREA] [nvarchar](50) NULL,
	[FSPROGRACE] [nvarchar](100) NULL,
	[FDRACEDATE] [datetime] NULL,
	[FSPROGSTATUS] [nvarchar](10) NULL,
	[FNPERIOD] [smallint] NULL,
	[FSPROGWIN] [nvarchar](100) NULL,
	[FSPROGNAME] [nvarchar](100) NULL,
	[FSMEMO] [nvarchar](200) NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
	[FSUPDATED_BY] [varchar](50) NULL,
	[FDUPDATED_DATE] [datetime] NULL,
 CONSTRAINT [PK_TBPROGRACE_1] PRIMARY KEY CLUSTERED 
(
	[AUTO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
CREATE NONCLUSTERED INDEX [INDEX_PROG_ID_EPISODE] ON [dbo].[TBPROGRACE]
(
	[FSPROG_ID] ASC,
	[FNEPISODE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自動編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGRACE', @level2type=N'COLUMN',@level2name=N'AUTO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目編碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGRACE', @level2type=N'COLUMN',@level2name=N'FSPROG_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'集別' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGRACE', @level2type=N'COLUMN',@level2name=N'FNEPISODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'參展區域' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGRACE', @level2type=N'COLUMN',@level2name=N'FSAREA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'影展名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGRACE', @level2type=N'COLUMN',@level2name=N'FSPROGRACE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'入圍或得獎日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGRACE', @level2type=N'COLUMN',@level2name=N'FDRACEDATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'參展狀態' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGRACE', @level2type=N'COLUMN',@level2name=N'FSPROGSTATUS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'屆數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGRACE', @level2type=N'COLUMN',@level2name=N'FNPERIOD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'獎項名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGRACE', @level2type=N'COLUMN',@level2name=N'FSPROGWIN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'參展節目名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGRACE', @level2type=N'COLUMN',@level2name=N'FSPROGNAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'備註' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGRACE', @level2type=N'COLUMN',@level2name=N'FSMEMO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGRACE', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGRACE', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGRACE', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROGRACE', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
