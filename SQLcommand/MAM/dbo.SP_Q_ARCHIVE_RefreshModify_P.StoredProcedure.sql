USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Q_ARCHIVE_RefreshModify_P]

@FSPROMO_ID varchar(11) 

As
select 

ISNULL(a.FSPROMO_NAME,'')  as FSPGMNAME,
a.* FROM TBPGM_PROMO  as a  
               
where a.FSPROMO_ID  = @FSPROMO_ID
order by FSPROG_ID,FNEPISODE
GO
