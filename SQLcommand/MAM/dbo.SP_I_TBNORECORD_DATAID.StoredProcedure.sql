USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Proc [dbo].[SP_I_TBNORECORD_DATAID]
@FSYYYYMM CHAR(6),
@FSDATA_NAME nvarchar(50),
@FSCREATED_BY CHAR(5)

As

Declare @FSNO_MAX CHAR(5)=''			  /*取得的最大號*/
Declare @FSDATA_ID CHAR(11)=''           /*要回傳的檔案編號*/

SELECT @FSNO_MAX = ISNULL(FSNO,'') FROM TBNORECORD_DATAID  /*用傳入的參數去找號*/ 
WHERE TBNORECORD_DATAID.FSYYYYMM = @FSYYYYMM 

	IF @FSNO_MAX = ''   /**/ 
          BEGIN
				SET @FSDATA_ID= @FSYYYYMM + '00001'
                INSERT INTO MAM.dbo.TBNORECORD_DATAID(FSYYYYMM,FSNO,FSDATA_NAME,FSCREATED_BY,FDCREATED_DATE)
                VALUES (@FSYYYYMM,'00001',@FSDATA_NAME,@FSCREATED_BY,GETDATE())
          END
          
     ELSE
          BEGIN 
           	   DECLARE @NEW_FSNO_MAX VARCHAR(5) = RIGHT(CAST((100000+CAST(@FSNO_MAX AS int)+1) AS NVARCHAR(6)),5)
               SET @FSDATA_ID= @FSYYYYMM + @NEW_FSNO_MAX             
             				
                INSERT INTO MAM.dbo.TBNORECORD_DATAID(FSYYYYMM,FSNO,FSDATA_NAME,FSCREATED_BY,FDCREATED_DATE)
                VALUES (@FSYYYYMM,@NEW_FSNO_MAX,@FSDATA_NAME,@FSCREATED_BY,GETDATE())
               --UPDATE TBNORECORD_DATAID
               --SET FSNO = @NEW_FSNO_MAX , FSDATA_NAME=@FSDATA_NAME, FSCREATED_BY = @FSCREATED_BY ,FDCREATED_DATE = GETDATE()
               --WHERE TBNORECORD_DATAID.FSYYYYMM = @FSYYYYMM 
          END

 SELECT @FSDATA_ID AS WANT_DATAID  
GO
