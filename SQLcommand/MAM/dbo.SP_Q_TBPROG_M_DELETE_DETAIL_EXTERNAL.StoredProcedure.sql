USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DECLARE	@FSPROG VARCHAR(7)
--SET @FSPROG = '2003004'

CREATE Proc [dbo].[SP_Q_TBPROG_M_DELETE_DETAIL_EXTERNAL]
@FSPROG_ID char(7)

AS
-----------------------------------
/*把設定的資料表清單取出到@tblList變數*/
DECLARE	@tblList TABLE(TableName VARCHAR(50), TableCName VARCHAR(50))
DECLARE	@tblCnt TABLE(TableName VARCHAR(50), DataCount INT, TableCName VARCHAR(50))

INSERT @tblList SELECT TableName, DataName FROM [PTSProg].[PTSProgram].[dbo].[tblProgDependency] WHERE  ProgLink = 1

DECLARE /*@_TableName		VARCHAR(50),
		@_TableCName	VARCHAR(50),*/
		@_SqlTemplate	VARCHAR(1000),
		@_Sql			VARCHAR(1000),
		@_Cnt			INT,
		@_ExistData		BIT

SELECT	/*@_TableName		= '', @_TableCName = '',*/
		@_SqlTemplate	= 'SELECT TableName = ''@_TableName'', DataCount = COUNT(*), TableCName = ''@_TableCName'' FROM [PTSProg].[PTSProgram].[dbo].[@_TableName] WHERE (FSPROGID = ''' + @FSPROG_ID + ''') ',
		@_Sql			= '',
		@_Cnt			= 0 
  
/*判斷是否有列出清單, 沒有列出的預設為避免刪除失敗, 回傳1*/	
DECLARE CSR1 CURSOR FOR SELECT TableName, TableCName FROM @tblList
DECLARE @TableName NVARCHAR(50), @TableCName NVARCHAR(50)

OPEN CSR1
	FETCH NEXT FROM CSR1 INTO @TableName, @TableCName
	WHILE (@@FETCH_STATUS=0)
		BEGIN 
		---------開始處理每一筆資料
			SET @_Sql = REPLACE(@_SqlTemplate,'@_TableName',@TableName) 
			SET @_Sql = REPLACE(@_Sql,'@_TableCName',@TableCName) 			
			INSERT @tblCnt  EXEC(@_Sql)
 
		---------處理完畢每一筆資料
		FETCH NEXT FROM CSR1 INTO @TableName, @TableCName
		END
Close CSR1

DELETE FROM @tblCnt WHERE (DATACOUNT = 0)
SELECT * FROM @tblCnt

GO
