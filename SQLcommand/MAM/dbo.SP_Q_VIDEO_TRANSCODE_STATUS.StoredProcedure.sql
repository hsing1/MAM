USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Q_VIDEO_TRANSCODE_STATUS]

@FSFILE_NO CHAR(16)

AS

SELECT FCLOW_RES FROM TBLOG_VIDEO WHERE FSFILE_NO = @FSFILE_NO

GO
