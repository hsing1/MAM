USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_I_TBARCHIVE]

@FSARC_ID char(12) ,
@FSTYPE char(1) ,
@FSID varchar(11) ,
@FSLESSCLAM nvarchar(500),
@FSSUPERVISOR char(1),
@FNEPISODE smallint ,
@FCCHECK_STATUS char(1) ,
@FSCREATED_BY varchar(50),
@FSCHANNEL_ID char(10) 

As
Insert Into TBARCHIVE(FSARC_ID,FSTYPE,FSID,FSLESSCLAM,FSSUPERVISOR,FNEPISODE,FCCHECK_STATUS,FSCREATED_BY,FDCREATED_DATE,FSCHANNEL_ID)
Values(@FSARC_ID,@FSTYPE,@FSID,@FSLESSCLAM,@FSSUPERVISOR,@FNEPISODE,@FCCHECK_STATUS,@FSCREATED_BY,GETDATE(),@FSCHANNEL_ID)

--update TBLOG_AUDIO
--set FSSUPERVISOR=@FSSUPERVISOR
--where FSARC_ID=@FSARC_ID

--update TBLOG_PHOTO
--set FSSUPERVISOR=@FSSUPERVISOR
--where FSARC_ID=@FSARC_ID

--update TBLOG_DOC
--set FSSUPERVISOR=@FSSUPERVISOR
--where FSARC_ID=@FSARC_ID


--update TBLOG_VIDEO
--set FSSUPERVISOR=@FSSUPERVISOR
--where FSARC_ID=@FSARC_ID

--update TBLOG_VIDEO_D
--set FSSUPERVISOR=@FSSUPERVISOR
--where FSARC_ID=@FSARC_ID


GO
