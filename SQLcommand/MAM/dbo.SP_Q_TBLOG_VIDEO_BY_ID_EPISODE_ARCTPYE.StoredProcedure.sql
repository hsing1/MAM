USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/12/25>
-- Description:   依照ID與集數與資料帶類型資料查詢檔案資料
-- =============================================
CREATE Proc [dbo].[SP_Q_TBLOG_VIDEO_BY_ID_EPISODE_ARCTPYE]
@FSARC_TYPE	char(3),
@FSID	varchar(11),
@FNEPISODE	smallint


As
IF (select COUNT(*) FSFILE_NO from TBLOG_VIDEO 
where FSID=@FSID and FNEPISODE=@FNEPISODE and (FCFILE_STATUS='T' or FCFILE_STATUS='Y')
and FSARC_TYPE=@FSARC_TYPE 
) >= 1 
select FSID,FNEPISODE,FSARC_TYPE,FSFILE_NO,FCLOW_RES 
FROM TBLOG_VIDEO  
where FSID = @FSID 
and FNEPISODE = @FNEPISODE 
and FSARC_TYPE = @FSARC_TYPE
and (FCFILE_STATUS='T' or FCFILE_STATUS='Y')
else
begin
select FSID,FNEPISODE,FSARC_TYPE,FSFILE_NO,FCLOW_RES 
FROM TBLOG_VIDEO  
where FSID = @FSID 
and FNEPISODE = @FNEPISODE 
and FSARC_TYPE = @FSARC_TYPE
and FCFILE_STATUS<>'D' and FCFILE_STATUS<>'X' and FCFILE_STATUS<>'F'

end

GO
