USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBPGM_ARR_PROMO_LOST](
	[FNCREATE_LIST_NO] [int] NOT NULL,
	[FNARR_PROMO_NO] [int] NOT NULL,
	[FSPROG_ID] [char](7) NOT NULL,
	[FNSEQNO] [int] NOT NULL,
	[FDDATE] [date] NOT NULL,
	[FSCHANNEL_ID] [varchar](2) NOT NULL,
	[FNBREAK_NO] [int] NULL,
	[FNSUB_NO] [int] NULL,
	[FSPROMO_ID] [varchar](12) NOT NULL,
	[FSPLAYTIME] [varchar](11) NULL,
	[FSDUR] [varchar](11) NULL,
	[FSSIGNAL] [nvarchar](50) NULL,
	[FSMEMO] [nvarchar](500) NULL,
	[FSMEMO1] [nvarchar](100) NULL,
	[FSCREATED_BY] [varchar](50) NULL,
	[FDCREATED_DATE] [datetime] NULL,
	[FSSTATUS] [varchar](1) NULL,
	[FDINSERT_DATE] [datetime] NULL,
	[FNEPISODE] [int] NULL,
 CONSTRAINT [PK_TBPGM_ARR_PROMO_LOST] PRIMARY KEY CLUSTERED 
(
	[FNCREATE_LIST_NO] ASC,
	[FNARR_PROMO_NO] ASC,
	[FDDATE] ASC,
	[FSCHANNEL_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'遺失清單編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO_LOST', @level2type=N'COLUMN',@level2name=N'FNCREATE_LIST_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'原插入序號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO_LOST', @level2type=N'COLUMN',@level2name=N'FNARR_PROMO_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'原所屬節目編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO_LOST', @level2type=N'COLUMN',@level2name=N'FSPROG_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'原所屬節目排播序號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO_LOST', @level2type=N'COLUMN',@level2name=N'FNSEQNO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'插入日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO_LOST', @level2type=N'COLUMN',@level2name=N'FDDATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'插入頻道' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO_LOST', @level2type=N'COLUMN',@level2name=N'FSCHANNEL_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'原屬節目段落' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO_LOST', @level2type=N'COLUMN',@level2name=N'FNBREAK_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'原屬節目支數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO_LOST', @level2type=N'COLUMN',@level2name=N'FNSUB_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'宣傳帶編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO_LOST', @level2type=N'COLUMN',@level2name=N'FSPROMO_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'播出時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO_LOST', @level2type=N'COLUMN',@level2name=N'FSPLAYTIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'長度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO_LOST', @level2type=N'COLUMN',@level2name=N'FSDUR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'訊號源' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO_LOST', @level2type=N'COLUMN',@level2name=N'FSSIGNAL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'備註(紀錄2nd Event)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO_LOST', @level2type=N'COLUMN',@level2name=N'FSMEMO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'備註' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO_LOST', @level2type=N'COLUMN',@level2name=N'FSMEMO1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO_LOST', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO_LOST', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'狀態(是否已被重新移回架構)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO_LOST', @level2type=N'COLUMN',@level2name=N'FSSTATUS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'重新移入日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO_LOST', @level2type=N'COLUMN',@level2name=N'FDINSERT_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'所屬節目集數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO_LOST', @level2type=N'COLUMN',@level2name=N'FNEPISODE'
GO
EXEC sys.sp_addextendedproperty @name=N'描述', @value=N'遺失清單(紀錄當節目段落異動而找不到對應之已插入的Promo)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_ARR_PROMO_LOST'
GO
