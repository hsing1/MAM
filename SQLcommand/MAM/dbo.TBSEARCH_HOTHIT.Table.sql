USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBSEARCH_HOTHIT](
	[FNNO] [bigint] NOT NULL,
	[FSSYS_ID] [varchar](20) NOT NULL,
	[FSTITLE] [nvarchar](200) NOT NULL,
	[FSINDEX_TYPE] [varchar](100) NOT NULL,
	[FSUSER_ID] [nvarchar](50) NOT NULL,
	[FDDATE] [varchar](10) NOT NULL,
 CONSTRAINT [PK_TBSEARCH_HOTHIT] PRIMARY KEY CLUSTERED 
(
	[FNNO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'序號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSEARCH_HOTHIT', @level2type=N'COLUMN',@level2name=N'FNNO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'檔案編號+多段編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSEARCH_HOTHIT', @level2type=N'COLUMN',@level2name=N'FSSYS_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'標題' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSEARCH_HOTHIT', @level2type=N'COLUMN',@level2name=N'FSTITLE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'索引庫' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSEARCH_HOTHIT', @level2type=N'COLUMN',@level2name=N'FSINDEX_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSEARCH_HOTHIT', @level2type=N'COLUMN',@level2name=N'FSUSER_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'點閱日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSEARCH_HOTHIT', @level2type=N'COLUMN',@level2name=N'FDDATE'
GO
EXEC sys.sp_addextendedproperty @name=N'描述', @value=N'熱門點閱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBSEARCH_HOTHIT'
GO
