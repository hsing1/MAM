USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/24>
-- Description:   刪除短帶託播單有效區間
-- =============================================
CREATE Proc [dbo].[SP_D_TBPGM_PROMO_EFFECTIVE_ZONE]

@FNPROMO_BOOKING_NO	int
As
delete TBPGM_PROMO_EFFECTIVE_ZONE 
where FNPROMO_BOOKING_NO= @FNPROMO_BOOKING_NO
   

GO
