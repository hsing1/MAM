USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   新增救命帶清單資料
-- =============================================
CREATE Proc [dbo].[SP_I_TBPGM_SAVE_TAPE_LIST]

@FSPROG_ID	char(7),
@FNSEQNO	int,
@FDDATE	date,
@FSCHANNEL_ID	varchar(2),
@FSBEG_TIME	varchar(4),
@FSEND_TIME	varchar(4),
@FSPROG_NAME	nvarchar(50),
@FNEPISODE	int,
@FSTAPENO	varchar(16),
@FNSAVE_NO	int,
@FSSAVE_PROG_ID	char(7),
@FNSAVE_EPISODE	int,
@FSSAVE_TAPENO	varchar(16),
@FSSAVE_LENGTH	varchar(11),
@FSCREATED_BY	varchar(50)

As
insert into TBPGM_SAVE_TAPE_LIST(FSPROG_ID,FNSEQNO,FDDATE,FSCHANNEL_ID,FSBEG_TIME,FSEND_TIME,FSPROG_NAME,
FNEPISODE,FSTAPENO,FNSAVE_NO,FSSAVE_PROG_ID,FNSAVE_EPISODE,FSSAVE_TAPENO,
FSSAVE_LENGTH,FSCREATED_BY,FDCREATED_DATE)
 values(@FSPROG_ID,@FNSEQNO,@FDDATE,@FSCHANNEL_ID,@FSBEG_TIME,@FSEND_TIME,@FSPROG_NAME,
@FNEPISODE,@FSTAPENO,@FNSAVE_NO,@FSSAVE_PROG_ID,@FNSAVE_EPISODE,@FSSAVE_TAPENO,
@FSSAVE_LENGTH,@FSCREATED_BY,GETDATE())



GO
