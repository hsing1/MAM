USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_U_RESET_QC_STATUS](@FSFILE_NO varchar(20))
AS
BEGIN
	if ((select FCSTATUS from TBLOG_VIDEO_HD_MC where FSFILE_NO = @FSFILE_NO) = 'O')
		update TBLOG_VIDEO_HD_MC set FCSTATUS = 'N' where FSFILE_NO = @FSFILE_NO
		--print 'Update Status'
	else
	    print 'The FCSTATUS is not O'

	if ((select FCFILE_STATUS from TBLOG_VIDEO where FSFILE_NO = @FSFILE_NO) = 'S')
		update TBLOG_VIDEO set FCFILE_STATUS = 'B' where FSFILE_NO = @FSFILE_NO
		--print 'Update TBLOG_VIDEO'
	else
	    print 'The FSFILE_STATUS is not S'

END
GO
