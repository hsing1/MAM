USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_U_TBUSERS]



AS
--更新部門檔案
--------------------------------
DECLARE @UserDepTempTable TABLE(
	[DeptID]		NVARCHAR(100),
	[UpperDept]	NVARCHAR(100),
	[LevelCode]		NVARCHAR(100),
	[BossID]		NVARCHAR(100),
	[StopDate]		NVARCHAR(100),
	[UUUDeptName]	NVARCHAR(100),
	[UUDeptName]		NVARCHAR(100),
	[UDeptName]	NVARCHAR(100),
	[DeptName]	NVARCHAR(100),
	[FULLName]		NVARCHAR(100),
	[Exist]	NVARCHAR(100)
)

INSERT
	@UserDepTempTable
SELECT
	ISNULL(DeptID,'') as DeptID1,
	ISNULL(UpperDept,'') as UpperDept1,
	ISNULL(LevelCode,	'') as LevelCode1,
	ISNULL(BossID,'') as BossID1,
	ISNULL(StopDate,'') as StopDate1,
	ISNULL(UUUDeptName,'') as UUUDeptName1,
	ISNULL(UUDeptName,	'') as UUDeptName1,
	ISNULL(UDeptName,'') as UDeptName1,
	ISNULL(DeptName,'') as DeptName1,
	ISNULL(FULLName,'') as FULLName1,
	ISNULL(Exist,'') as Exist1 
FROM
	[PTSHRRSC].[PTSHR].[dbo].[tblTBSDept]
where Exist='1'

	
	

--跑回圈處理每一筆資料
	
DECLARE CSR2 CURSOR FOR SELECT DeptID,UpperDept,LevelCode,BossID,StopDate,UUUDeptName,UUDeptName,UDeptName,DeptName,FULLName, Exist
						FROM @UserDepTempTable
DECLARE 
	@DeptID1		NVARCHAR(100),
	@UpperDept1	NVARCHAR(100),
	@LevelCode1	NVARCHAR(100),
	@BossID1	NVARCHAR(100),
	@StopDate1	NVARCHAR(100),
	@UUUDeptName1	NVARCHAR(100),
	@UUDeptName1	NVARCHAR(100),
	@UDeptName1	NVARCHAR(100),
	@DeptName1	NVARCHAR(100),
	@FULLName1		NVARCHAR(100),
	@Exist1		NVARCHAR(100)
 

OPEN CSR2
	FETCH NEXT FROM CSR2 INTO @DeptID1,@UpperDept1,@LevelCode1,@BossID1,@StopDate1,@UUUDeptName1,@UUDeptName1,@UDeptName1,@DeptName1,@FULLName1, @Exist1
	WHILE (@@FETCH_STATUS=0)
		BEGIN 
		---------開始處理每一筆資料
			IF EXISTS(SELECT @DeptID1 FROM [MAM].[dbo].[TBUSER_DEP] AS _DEP WHERE (_DEP.FNDEP_ID = @DeptID1)) --UPDATE
				BEGIN
					UPDATE	[MAM].[dbo].[TBUSER_DEP]
					SET		[FNPARENT_DEP_ID]=@UpperDept1,
							[FSDEP]=@DeptName1,
							[FNDEP_LEVEL]=	@LevelCode1,
							[FSSupervisor_ID]=@BossID1,
							[FSUpDepName_ChtName]=@UDeptName1,	
							[FSDpeName_ChtName]	=@DeptName1,
							[FSFullName_ChtName]=@FULLName1,
							[FBIsDepExist]=@Exist1	
					WHERE	([FNDEP_ID]=@DeptID1)
				END			
			ELSE									--INSERT
				BEGIN
					INSERT	[MAM].[dbo].[TBUSER_DEP]
					(FNDEP_ID,FNPARENT_DEP_ID,FNDEP_LEVEL,FSSupervisor_ID,FSUpDepName_ChtName,FSDpeName_ChtName,FSFullName_ChtName,FBIsDepExist)
					VALUES
					(@DeptID1,@UpperDept1,@LevelCode1,@BossID1,@UDeptName1,@DeptName1,@FULLName1,@Exist1)
				
				END
			
		FETCH NEXT FROM CSR2 INTO @DeptID1,@UpperDept1,@LevelCode1,@BossID1,@StopDate1,@UUUDeptName1,@UUDeptName1,@UDeptName1,@DeptName1,@FULLName1, @Exist1--@DeptID1,@UpperDept1,@LevelCode1,@BossID1,@StopDate1,@UUUDeptName1,@UUDeptName1,@UDeptName1,@DeptName1, @Exist1
		END
Close CSR2
-----轉入部門結束


--更新使用者從公視的TBUSERS

--------------------------------
DECLARE @TempTable TABLE(
	CName		NVARCHAR(100),
	NTAccount	NVARCHAR(100),
	Email1		NVARCHAR(100),
	DeptID		NVARCHAR(100),
	GroupID		NVARCHAR(100),
	UpperDept	NVARCHAR(100),
	Station		NVARCHAR(100),
	DeptName	NVARCHAR(100),
	GroupName	NVARCHAR(100),
	OnDuty		NVARCHAR(100),
	EmpID	NVARCHAR(100),
	ChannelId  NVARCHAR(100)
)

INSERT
	@TempTable
SELECT
	ISNULL(CName,'') as CName,
	ISNULL(NTAccount,'') as NTAccount,
	ISNULL(Email1,	'') as Email1,
	ISNULL(DeptID,'') as DeptID,
	ISNULL(GroupID,'') as GroupID,
	ISNULL(UpperDept,'') as UpperDept,
	ISNULL(Station,	'') as Station,
	ISNULL(DeptName,'') as DeptName,
	ISNULL(GroupName,'') as GroupName,
	ISNULL(OnDuty,'') as OnDuty,
	ISNULL(EmpID,'') as EmpID,
	'' as ChannelId
FROM
	[PTSHRRSC].[PTSHR].[dbo].[tblEmpinfo]
	--where NTAccount='mis51125'
	

--跑回圈處理每一筆資料
	
DECLARE CSR1 CURSOR FOR SELECT CName,NTAccount,Email1,DeptID,GroupID,UpperDept,Station,DeptName,GroupName,OnDuty, EmpID,ChannelId
						FROM @TempTable
DECLARE 
	@CName		NVARCHAR(100),
	@NTAccount	NVARCHAR(100),
	@Email1		NVARCHAR(100),
	@DeptID		NVARCHAR(100),
	@GroupID	NVARCHAR(100),
	@UpperDept	NVARCHAR(100),
	@Station	NVARCHAR(100),
	@DeptName	NVARCHAR(100),
	@GroupName	NVARCHAR(100),
	@OnDuty		NVARCHAR(100),
	@EmpID		NVARCHAR(100),
	@ChannelId  NVARCHAR(100)
 

OPEN CSR1
	FETCH NEXT FROM CSR1 INTO @CName,@NTAccount,@Email1,@DeptID,@GroupID,@UpperDept,@Station,@DeptName,@GroupName,@OnDuty, @EmpID,@ChannelId
	WHILE (@@FETCH_STATUS=0)
		BEGIN 
		---------開始處理每一筆資料
		IF @Station='公共電視台'		---公共電視台'01'
		begin
	    set	@ChannelId='01'
		end
		IF @Station='客家電視台'		---客家電視台'07'
		begin
	    set	@ChannelId='07'
		end
		IF @Station='原住民族電視台'	---原住民族電視台'09'
		begin
	    set	@ChannelId='09'  
		end
		IF @DeptName='國際部'					---宏觀電視台'08'
		begin
	    set	@ChannelId='08'
		end
		
			IF EXISTS(SELECT @EmpID FROM [MAM].[dbo].[TBUSERS] AS _USR WHERE (_USR.FSUSER_ID = @EmpID)) --UPDATE
				BEGIN
					UPDATE	[MAM].[dbo].[TBUSERS]
					SET		FSUSER_ChtName		=	@CName		,
							FSUSER	=	@NTAccount	,
							FSEMAIL		=	@Email1		,
							FNDep_ID		=	@DeptID		,
							FNGroup_ID		=	@GroupID	,	
							FNUpperDept_ID	=	@UpperDept	,
							FSUpperDept_ChtName		=	@Station	,	
							FSDep_ChtName	=	@DeptName	,
							FSGroup_ChtName	=	@GroupName	,
							FCACTIVE		=	@OnDuty	,
							[FSCREATED_BY] = 'sa',
							[FDCREATED_DATE]=GETDATE(),
							[FSUPDATED_BY]='sa',
							[FDUPDATED_DATE]=GETDATE(),
							FSCHANNEL_ID=@ChannelId
					WHERE	(FSUSER_ID = @EmpID)
				END			
			ELSE									--INSERT
				BEGIN
					INSERT	[MAM].[dbo].[TBUSERS]
					(FSUSER_ID,FSUSER_ChtName,FSUSER,FSEMAIL,FNDep_ID,FNGroup_ID,FNUpperDept_ID,FSUpperDept_ChtName,FSDep_ChtName,FSGroup_ChtName, FCACTIVE,[FSCREATED_BY],[FDCREATED_DATE],[FSUPDATED_BY],[FDUPDATED_DATE],FSCHANNEL_ID)
					VALUES
					(@EmpID,@CName,@NTAccount,@Email1,@DeptID,@GroupID,@UpperDept,@Station,@DeptName,@GroupName,@OnDuty,'sa',GETDATE(),'sa',GETDATE(),@ChannelId)
				
				END
			
		FETCH NEXT FROM CSR1 INTO @CName,@NTAccount,@Email1,@DeptID,@GroupID,@UpperDept,@Station,@DeptName,@GroupName,@OnDuty, @EmpID,@ChannelId
		END
Close CSR1








--更新主管註記
--UPDATE [MAM].[dbo].[TBUSERS] 
--SET [FSIs_Supervisor]='Y'
--WHERE [FSUSER_ID] in
--(SELECT [FSSupervisor_ID]
--FROM [MAM].[dbo].[TBUSER_DEP]
--where NOT(ISNULL(LTRIM([FSSupervisor_ID]),'') = ''))
UPDATE [MAM].[dbo].[TBUSERS] 
SET [FSIs_Supervisor]='N'

UPDATE [MAM].[dbo].[TBUSERS] 
SET [FSIs_Main_Title]='Y'






----------------------DBLINK


--EXEC sp_addlinkedserver
--@server = 'PTSHRRSC', --Server Name
--@srvproduct = 'MS SQL', 
--@datasrc = '10.13.220.35\MSSQL2008' , --Server IP
--@provider = 'SQLNCLI'

--EXEC sp_addlinkedsrvlogin
--@rmtsrvname = 'PTSHRRSC' , --Server Name   PTSProgram
--@useself = 'false' ,
--@locallogin = NULL ,
--@rmtuser = 'sa' , --User
--@rmtpassword = 'P@ssw0rd' --Password

GO
