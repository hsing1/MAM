USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/03/21>
-- Description:	<根據傳入條件,取回最新單號>
-- =============================================
CREATE FUNCTION [dbo].[FN_I_TBNORECORD]
(
	@FSTYPE CHAR(2) ,
	@FSNO_DATE CHAR(8) ,
	@FSNOTE NVARCHAR(50) ,
	@FSCREATED_BY VARCHAR(50) 
)
RETURNS	INT
AS
BEGIN
	-- Declare the return variable here
		Declare @return_value INT

	-- Add the T-SQL statements to compute the return value here
		EXEC	@return_value	= [dbo].[SP_I_TBNORECORD]
				@FSTYPE			= @FSTYPE,
				@FSNO_DATE		= @FSNO_DATE,
				@FSNOTE			= @FSNOTE,
				@FSCREATED_BY	= @FSCREATED_BY

	-- Return the result of the function
	RETURN	@return_value
END


GO
