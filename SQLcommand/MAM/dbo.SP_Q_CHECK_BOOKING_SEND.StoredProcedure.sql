USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <David.Sin>
-- Create date:   <2010/10/02>
-- Description:   查詢預借清單是否已送出
-- =============================================

CREATE PROC [dbo].[SP_Q_CHECK_BOOKING_SEND]
@FSPREBOOKING_NO varchar(12)
AS

SELECT FCSEND FROM TBPREBOOKING_MASTER
WHERE FSPREBOOKING_NO=@FSPREBOOKING_NO
GO
