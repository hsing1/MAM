USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   新增播出運行表所使用的Key 報表資料
-- =============================================
CREATE Proc [dbo].[SP_I_RPT_TBPGM_ARR_PROMO_KeyList]
@QUERY_KEY	uniqueidentifier,
@QUERY_KEY_SUBID	uniqueidentifier,
@QUERY_BY	varchar(50),
@頻道	varchar(50),
@播出日期	date,
@序號	varchar(8),
@KeyLayer	varchar(1),
@KeyNumber	int,
@Title	varchar(100),
@ID	varchar(10)
	
As
Insert Into dbo.RPT_TBPGM_ARR_PROMO_KeyList
(QUERY_KEY,QUERY_KEY_SUBID,QUERY_BY,
QUERY_DATE,頻道,播出日期,序號,
KeyLayer,KeyNumber,Title,
ID)
Values(@QUERY_KEY,@QUERY_KEY_SUBID,@QUERY_BY,
GETDATE(),@頻道,@播出日期,@序號,
@KeyLayer,@KeyNumber,@Title,@ID)
GO
