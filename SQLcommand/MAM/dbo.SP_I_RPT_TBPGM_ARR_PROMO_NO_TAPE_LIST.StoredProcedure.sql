USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   新增撥出運行表未到帶清單報表資料
-- =============================================
CREATE Proc [dbo].[SP_I_RPT_TBPGM_ARR_PROMO_NO_TAPE_LIST]

@QUERY_KEY	uniqueidentifier,
@QUERY_KEY_SUBID	uniqueidentifier,
@QUERY_BY	varchar(50),
@播出日期	date,
@頻道	nvarchar(50),
@宣傳帶編碼	nvarchar(50),
@宣傳帶名稱	nvarchar(50),
@影像編號	varchar(16),
@主控編號	varchar(8),
@狀態	varchar(50),
@建立者	varchar(20)

As
Insert Into dbo.RPT_TBPGM_ARR_PROMO_NO_TAPE_LIST(QUERY_KEY,QUERY_KEY_SUBID,QUERY_BY,QUERY_DATE,播出日期,頻道,
宣傳帶編碼,宣傳帶名稱,影像編號,主控編號,狀態,建立者)
Values(@QUERY_KEY,@QUERY_KEY_SUBID,@QUERY_BY,GETDATE(),@播出日期,
@頻道,@宣傳帶編碼,@宣傳帶名稱,@影像編號,@主控編號,@狀態,@建立者)
GO
