USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBPROPOSAL](
	[FSPRO_ID] [varchar](12) NOT NULL,
	[FCPRO_TYPE] [char](1) NOT NULL,
	[FSPROG_ID] [char](7) NOT NULL,
	[FSPROG_NAME] [nvarchar](50) NOT NULL,
	[FSPLAY_NAME] [nvarchar](50) NULL,
	[FSNAME_FOR] [varchar](100) NULL,
	[FSCONTENT] [nvarchar](max) NULL,
	[FNLENGTH] [int] NOT NULL,
	[FNBEG_EPISODE] [smallint] NOT NULL,
	[FNEND_EPISODE] [smallint] NOT NULL,
	[FSCHANNEL] [varchar](100) NULL,
	[FDSHOW_DATE] [date] NULL,
	[FSPRD_DEPT_ID] [varchar](2) NULL,
	[FSPAY_DEPT_ID] [varchar](2) NULL,
	[FSSIGN_DEPT_ID] [varchar](2) NULL,
	[FSBEF_DEPT_ID] [varchar](100) NULL,
	[FSAFT_DEPT_ID] [varchar](100) NULL,
	[FSOBJ_ID] [varchar](2) NOT NULL,
	[FSGRADE_ID] [varchar](2) NULL,
	[FSSRC_ID] [varchar](2) NULL,
	[FSTYPE] [varchar](2) NULL,
	[FSAUD_ID] [varchar](2) NULL,
	[FSSHOW_TYPE] [varchar](2) NOT NULL,
	[FSLANG_ID_MAIN] [varchar](2) NULL,
	[FSLANG_ID_SUB] [varchar](2) NULL,
	[FSBUY_ID] [varchar](2) NULL,
	[FSBUYD_ID] [varchar](2) NULL,
	[FCCHECK_STATUS] [char](1) NOT NULL,
	[FSCHANNEL_ID] [char](2) NULL,
	[FCEMERGENCY] [char](1) NULL,
	[FCCOMPLETE] [char](1) NULL,
	[FDCOMPLETE] [datetime] NULL,
	[FSPRDCENID] [varchar](5) NOT NULL,
	[FSPROGSPEC] [varchar](100) NULL,
	[FCPROGD] [char](1) NULL,
	[FSENOTE] [nvarchar](200) NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
	[FSUPDATED_BY] [varchar](50) NULL,
	[FDUPDATED_DATE] [datetime] NULL,
	[FDEXPIRE_DATE] [datetime] NULL,
	[FSEXPIRE_DATE_ACTION] [nvarchar](10) NULL,
	[FSPROGNATIONID] [nvarchar](50) NULL,
	[FSPRODUCER] [varchar](10) NULL,
 CONSTRAINT [ TBProposal] PRIMARY KEY CLUSTERED 
(
	[FSPRO_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[TBPROPOSAL] ADD  CONSTRAINT [DF_TBPROPOSAL_FDEXPIRE_DATE]  DEFAULT ('1900-1-1') FOR [FDEXPIRE_DATE]
GO
ALTER TABLE [dbo].[TBPROPOSAL] ADD  CONSTRAINT [DF_TBPROPOSAL_FSEXPIRE_DATE_ACTION]  DEFAULT ('') FOR [FSEXPIRE_DATE_ACTION]
GO
ALTER TABLE [dbo].[TBPROPOSAL] ADD  CONSTRAINT [DF_TBPROPOSAL_FSPROGNATIONID]  DEFAULT ('') FOR [FSPROGNATIONID]
GO
ALTER TABLE [dbo].[TBPROPOSAL]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROPOSAL_TBZCHANNEL] FOREIGN KEY([FSCHANNEL_ID])
REFERENCES [dbo].[TBZCHANNEL] ([FSCHANNEL_ID])
GO
ALTER TABLE [dbo].[TBPROPOSAL] CHECK CONSTRAINT [FK_TBPROPOSAL_TBZCHANNEL]
GO
ALTER TABLE [dbo].[TBPROPOSAL]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROPOSAL_TBZDEPT1] FOREIGN KEY([FSPAY_DEPT_ID])
REFERENCES [dbo].[TBZDEPT] ([FSDEPTID])
GO
ALTER TABLE [dbo].[TBPROPOSAL] CHECK CONSTRAINT [FK_TBPROPOSAL_TBZDEPT1]
GO
ALTER TABLE [dbo].[TBPROPOSAL]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROPOSAL_TBZDEPT2] FOREIGN KEY([FSSIGN_DEPT_ID])
REFERENCES [dbo].[TBZDEPT] ([FSDEPTID])
GO
ALTER TABLE [dbo].[TBPROPOSAL] CHECK CONSTRAINT [FK_TBPROPOSAL_TBZDEPT2]
GO
ALTER TABLE [dbo].[TBPROPOSAL]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROPOSAL_TBZDEPT3] FOREIGN KEY([FSPRD_DEPT_ID])
REFERENCES [dbo].[TBZDEPT] ([FSDEPTID])
GO
ALTER TABLE [dbo].[TBPROPOSAL] CHECK CONSTRAINT [FK_TBPROPOSAL_TBZDEPT3]
GO
ALTER TABLE [dbo].[TBPROPOSAL]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROPOSAL_TBZPROGATTR] FOREIGN KEY([FSTYPE])
REFERENCES [dbo].[TBZPROGATTR] ([FSPROGATTRID])
GO
ALTER TABLE [dbo].[TBPROPOSAL] CHECK CONSTRAINT [FK_TBPROPOSAL_TBZPROGATTR]
GO
ALTER TABLE [dbo].[TBPROPOSAL]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROPOSAL_TBZPROGAUD] FOREIGN KEY([FSAUD_ID])
REFERENCES [dbo].[TBZPROGAUD] ([FSPROGAUDID])
GO
ALTER TABLE [dbo].[TBPROPOSAL] CHECK CONSTRAINT [FK_TBPROPOSAL_TBZPROGAUD]
GO
ALTER TABLE [dbo].[TBPROPOSAL]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROPOSAL_TBZPROGBUY] FOREIGN KEY([FSBUY_ID])
REFERENCES [dbo].[TBZPROGBUY] ([FSPROGBUYID])
GO
ALTER TABLE [dbo].[TBPROPOSAL] CHECK CONSTRAINT [FK_TBPROPOSAL_TBZPROGBUY]
GO
ALTER TABLE [dbo].[TBPROPOSAL]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROPOSAL_TBZPROGBUYD] FOREIGN KEY([FSBUY_ID], [FSBUYD_ID])
REFERENCES [dbo].[TBZPROGBUYD] ([FSPROGBUYID], [FSPROGBUYDID])
GO
ALTER TABLE [dbo].[TBPROPOSAL] CHECK CONSTRAINT [FK_TBPROPOSAL_TBZPROGBUYD]
GO
ALTER TABLE [dbo].[TBPROPOSAL]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROPOSAL_TBZPROGGRADE] FOREIGN KEY([FSGRADE_ID])
REFERENCES [dbo].[TBZPROGGRADE] ([FSPROGGRADEID])
GO
ALTER TABLE [dbo].[TBPROPOSAL] CHECK CONSTRAINT [FK_TBPROPOSAL_TBZPROGGRADE]
GO
ALTER TABLE [dbo].[TBPROPOSAL]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROPOSAL_TBZPROGLANG] FOREIGN KEY([FSLANG_ID_MAIN])
REFERENCES [dbo].[TBZPROGLANG] ([FSPROGLANGID])
GO
ALTER TABLE [dbo].[TBPROPOSAL] CHECK CONSTRAINT [FK_TBPROPOSAL_TBZPROGLANG]
GO
ALTER TABLE [dbo].[TBPROPOSAL]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROPOSAL_TBZPROGLANG1] FOREIGN KEY([FSLANG_ID_SUB])
REFERENCES [dbo].[TBZPROGLANG] ([FSPROGLANGID])
GO
ALTER TABLE [dbo].[TBPROPOSAL] CHECK CONSTRAINT [FK_TBPROPOSAL_TBZPROGLANG1]
GO
ALTER TABLE [dbo].[TBPROPOSAL]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROPOSAL_TBZPROGOBJ] FOREIGN KEY([FSOBJ_ID])
REFERENCES [dbo].[TBZPROGOBJ] ([FSPROGOBJID])
GO
ALTER TABLE [dbo].[TBPROPOSAL] CHECK CONSTRAINT [FK_TBPROPOSAL_TBZPROGOBJ]
GO
ALTER TABLE [dbo].[TBPROPOSAL]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROPOSAL_TBZPROGSRC] FOREIGN KEY([FSSRC_ID])
REFERENCES [dbo].[TBZPROGSRC] ([FSPROGSRCID])
GO
ALTER TABLE [dbo].[TBPROPOSAL] CHECK CONSTRAINT [FK_TBPROPOSAL_TBZPROGSRC]
GO
ALTER TABLE [dbo].[TBPROPOSAL]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROPOSAL_TBZPROGTYPE] FOREIGN KEY([FSSHOW_TYPE])
REFERENCES [dbo].[TBZPROGTYPE] ([FSPROGTYPEID])
GO
ALTER TABLE [dbo].[TBPROPOSAL] CHECK CONSTRAINT [FK_TBPROPOSAL_TBZPROGTYPE]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'成案單編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FSPRO_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'成案種類' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FCPRO_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FSPROG_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FSPROG_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'播出名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FSPLAY_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外文名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FSNAME_FOR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目內容簡述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FSCONTENT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'每集時長' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FNLENGTH'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'集次(起)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FNBEG_EPISODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'集次(迄)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FNEND_EPISODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'預訂播出頻道' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FSCHANNEL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'預訂上檔日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FDSHOW_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'製作部門' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FSPRD_DEPT_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'付款部門' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FSPAY_DEPT_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'簽核單位(怪怪的先不放)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FSSIGN_DEPT_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'前會部門' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FSBEF_DEPT_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'後會部門' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FSAFT_DEPT_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'製作目的' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FSOBJ_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目分級' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FSGRADE_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目來源' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FSSRC_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目型態' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FSTYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'目標觀眾' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FSAUD_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'表現方式' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FSSHOW_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主聲道' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FSLANG_ID_MAIN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'副聲道' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FSLANG_ID_SUB'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外購類別大類' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FSBUY_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外購類別細類' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FSBUYD_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'狀態' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FCCHECK_STATUS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'頻道別代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FSCHANNEL_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'緊急成案' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FCEMERGENCY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'資料補齊' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FCCOMPLETE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'資料補齊日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FDCOMPLETE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'製作單位代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FSPRDCENID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目規格' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FSPROGSPEC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'產生子集' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FCPROGD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'分集備註' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FSENOTE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'到期日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FDEXPIRE_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'到期後是否刪除檔案' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FSEXPIRE_DATE_ACTION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'來源國家代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROPOSAL', @level2type=N'COLUMN',@level2name=N'FSPROGNATIONID'
GO
