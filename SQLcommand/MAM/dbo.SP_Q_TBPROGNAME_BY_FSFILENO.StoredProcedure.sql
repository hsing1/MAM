USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[SP_Q_TBPROGNAME_BY_FSFILENO]
@FCTYPE CHAR(1),
@FSFILE_NO VARCHAR(16),
@FSJOB_ID BIGINT = 0
AS

--�v
IF @FCTYPE = '1'
BEGIN
	--SELECT
	--	[FSPGMNAME] + '(' + CONVERT(VARCHAR(10),[TBLOG_VIDEO].[FNEPISODE]) + ')' AS [FSPGMNAME]
	--FROM
	--	[dbo].[TBPROG_M] JOIN [dbo].[TBLOG_VIDEO] ON [TBPROG_M].[FSPROG_ID] = [TBLOG_VIDEO].[FSID]
	--WHERE
	--	[TBLOG_VIDEO].[FSFILE_NO] = @FSFILE_NO

	SELECT
		REPLACE(REPLACE(REPLACE(REPLACE(REPLACE([FSPGMNAME],'*',''),'/',''),'\',''),'?',''),'"','') + '(' + CONVERT(VARCHAR(10),[TBLOG_VIDEO].[FNEPISODE]) + ')' AS [FSPGMNAME],
		--[TBBOOKING_MASTER].[FSUSER_ID],
		[TBUSERS].[FSUSER],
		[TBBOOKING_DETAIL].[FSFILE_NO],
		[TBBOOKING_DETAIL].[FSBOOKING_NO],
		[TBBOOKING_DETAIL].[FSSEQ]
	FROM
		[dbo].[TBBOOKING_DETAIL] JOIN [dbo].[TBBOOKING_MASTER] ON [TBBOOKING_DETAIL].[FSBOOKING_NO] = [TBBOOKING_MASTER].[FSBOOKING_NO]
								JOIN [dbo].[TBLOG_VIDEO] ON [TBBOOKING_DETAIL].[FSFILE_NO] = [TBLOG_VIDEO].[FSFILE_NO]
								JOIN [dbo].[TBPROG_M] ON [TBPROG_M].[FSPROG_ID] = [TBLOG_VIDEO].[FSID]
								JOIN [dbo].[TBUSERS] ON [TBBOOKING_MASTER].[FSUSER_ID] = [TBUSERS].[FSUSER_ID]
	WHERE
		[TBBOOKING_DETAIL].[FSJOB_ID] = @FSJOB_ID AND [TBLOG_VIDEO].[FSTYPE] = 'G'
	UNION
	SELECT
		
		REPLACE(REPLACE(REPLACE(REPLACE(REPLACE([FSDATA_NAME],'*',''),'/',''),'\',''),'?',''),'"','') + '(' + CONVERT(VARCHAR(10),[TBLOG_VIDEO].[FNEPISODE]) + ')' AS [FSPGMNAME],
		--[TBBOOKING_MASTER].[FSUSER_ID],
		[TBUSERS].[FSUSER],
		[TBBOOKING_DETAIL].[FSFILE_NO],
		[TBBOOKING_DETAIL].[FSBOOKING_NO],
		[TBBOOKING_DETAIL].[FSSEQ]
	FROM
		[dbo].[TBBOOKING_DETAIL] JOIN [dbo].[TBBOOKING_MASTER] ON [TBBOOKING_DETAIL].[FSBOOKING_NO] = [TBBOOKING_MASTER].[FSBOOKING_NO]
								JOIN [dbo].[TBLOG_VIDEO] ON [TBBOOKING_DETAIL].[FSFILE_NO] = [TBLOG_VIDEO].[FSFILE_NO]
								JOIN [dbo].[TBDATA] ON [TBDATA].[FSDATA_ID] = [TBLOG_VIDEO].[FSID]
								JOIN [dbo].[TBUSERS] ON [TBBOOKING_MASTER].[FSUSER_ID] = [TBUSERS].[FSUSER_ID]
	WHERE
		[TBBOOKING_DETAIL].[FSJOB_ID] = @FSJOB_ID AND [TBLOG_VIDEO].[FSTYPE] = 'D'
	UNION
	SELECT
		REPLACE(REPLACE(REPLACE(REPLACE(REPLACE([FSPROMO_NAME],'*',''),'/',''),'\',''),'?',''),'"','') + '(' + CONVERT(VARCHAR(10),[TBLOG_VIDEO].[FNEPISODE]) + ')' AS [FSPGMNAME],
		--[TBBOOKING_MASTER].[FSUSER_ID],
		[TBUSERS].[FSUSER],
		[TBBOOKING_DETAIL].[FSFILE_NO],
		[TBBOOKING_DETAIL].[FSBOOKING_NO],
		[TBBOOKING_DETAIL].[FSSEQ]
	FROM
		[dbo].[TBBOOKING_DETAIL] JOIN [dbo].[TBBOOKING_MASTER] ON [TBBOOKING_DETAIL].[FSBOOKING_NO] = [TBBOOKING_MASTER].[FSBOOKING_NO]
								JOIN [dbo].[TBLOG_VIDEO] ON [TBBOOKING_DETAIL].[FSFILE_NO] = [TBLOG_VIDEO].[FSFILE_NO]
								JOIN [dbo].[TBPGM_PROMO] ON [TBPGM_PROMO].[FSPROMO_ID] = [TBLOG_VIDEO].[FSID]
								JOIN [dbo].[TBUSERS] ON [TBBOOKING_MASTER].[FSUSER_ID] = [TBUSERS].[FSUSER_ID]
	WHERE
		[TBBOOKING_DETAIL].[FSJOB_ID] = @FSJOB_ID AND [TBLOG_VIDEO].[FSTYPE] = 'P'
END
--��
IF @FCTYPE = '2'
BEGIN
	SELECT
		REPLACE(REPLACE(REPLACE(REPLACE(REPLACE([FSPGMNAME],'*',''),'/',''),'\',''),'?',''),'"','') + '(' + CONVERT(VARCHAR(10),[TBLOG_AUDIO].[FNEPISODE]) + ')' AS [FSPGMNAME]
	FROM
		[dbo].[TBPROG_M] JOIN [dbo].[TBLOG_AUDIO] ON [TBPROG_M].[FSPROG_ID] = [TBLOG_AUDIO].[FSID]
	WHERE
		[TBLOG_AUDIO].[FSFILE_NO] = @FSFILE_NO
END
--��
IF @FCTYPE = '3'
BEGIN
	SELECT
		REPLACE(REPLACE(REPLACE(REPLACE(REPLACE([FSPGMNAME],'*',''),'/',''),'\',''),'?',''),'"','') + '(' + CONVERT(VARCHAR(10),[TBLOG_PHOTO].[FNEPISODE]) + ')' AS [FSPGMNAME]
	FROM
		[dbo].[TBPROG_M] JOIN [dbo].[TBLOG_PHOTO] ON [TBPROG_M].[FSPROG_ID] = [TBLOG_PHOTO].[FSID]
	WHERE
		[TBLOG_PHOTO].[FSFILE_NO] = @FSFILE_NO
END
--��
IF @FCTYPE = '4'
BEGIN
	SELECT
		REPLACE(REPLACE(REPLACE(REPLACE(REPLACE([FSPGMNAME],'*',''),'/',''),'\',''),'?',''),'"','') + '(' + CONVERT(VARCHAR(10),[TBLOG_DOC].[FNEPISODE]) + ')' AS [FSPGMNAME]
	FROM
		[dbo].[TBPROG_M] JOIN [dbo].[TBLOG_DOC] ON [TBPROG_M].[FSPROG_ID] = [TBLOG_DOC].[FSID]
	WHERE
		[TBLOG_DOC].[FSFILE_NO] = @FSFILE_NO
END



GO
