USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Q_TBLOG_VIDEO_LOW_RES]

@FSID	varchar(11),
@FNEPISODE	smallint,
@FSARC_TYPE	char(3)

As
Select FSFILE_NO,FSID,FNEPISODE,FSARC_TYPE,FCLOW_RES,FSBEG_TIMECODE,FSEND_TIMECODE from dbo.TBLOG_VIDEO
where FSID = @FSID 
and FNEPISODE = @FNEPISODE 
and FSARC_TYPE=@FSARC_TYPE

GO
