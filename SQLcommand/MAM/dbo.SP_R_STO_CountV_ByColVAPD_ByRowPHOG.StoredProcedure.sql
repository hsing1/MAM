USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Kyle.Lin>
-- Create date: <2011/11/14>
-- Description:	For RPT_TBARCHIVE_01(入庫單), 列印前存入暫存資料庫
-- Modify: <Mihsiu.Chiu><2013/03/27><新增欄位[影片總時長(節目)] & 修改抓取入庫(Y)及轉檔成功(T)>
-- =============================================
CREATE ProcEDURE [dbo].[SP_R_STO_CountV_ByColVAPD_ByRowPHOG]
    @CREATED_BY varchar(50)	
AS
BEGIN

  --declare @CREATED_BY varchar(50) ='sa'
  declare @PHOG_V int =99999
  declare @PHOG_A int=99999
  declare @PHOG_P int=99999
  declare @PHOG_D int=99999
  declare @PHOG_VP int=99999
  declare @PHOG_VLEN decimal(8,2)=99999
  declare @QUERY_BY varchar(50)=''
  declare @CountTable Table(
  [頻道別] nvarchar(10),
  [影] int,
  [音] int,
  [圖] int,
  [其他] int,
  [短片(影)] int,
  [影片總時長(節目)] decimal(8,2),
  [QUERY_BY] varchar (50)
  )

  (select @PHOG_V = COUNT(*) 
		,@PHOG_VLEN = ISNULL(
		Convert(decimal(8,2),
		sum(
		((convert(float,substring(FSEND_TIMECODE,10,2))+ --ff
		convert(int,substring(FSEND_TIMECODE,7,2)*30)+		   --ss
		convert(int,substring(FSEND_TIMECODE,4,2)*60*30)+		   --mm
		convert(int,substring(FSEND_TIMECODE,1,2)*60*60*30))	   --hh
		-
		(convert(float,substring(FSBEG_TIMECODE,10,2))+ --ff
		convert(int,substring(FSBEG_TIMECODE,7,2)*30)+		   --ss
		convert(int,substring(FSBEG_TIMECODE,4,2)*60*30)+		   --mm
		convert(int,substring(FSBEG_TIMECODE,1,2)*60*60*30))	   --hh
		)/30)/60/60	),0)
		from [MAM].[dbo].[TBLOG_VIDEO]
	  where (([FCFILE_STATUS]='Y' or[FCFILE_STATUS]='T')  and [FSCHANNEL_ID]='01' and [FSTYPE]='G'))--公視影
	  
  set @PHOG_A=
  (select COUNT(*) from [MAM].[dbo].[TBLOG_AUDIO]
	  where ([FCFILE_STATUS]='Y' and [FSCHANNEL_ID]='01'))--公視音
  set @PHOG_P=
	  (select COUNT(*) from [MAM].[dbo].[TBLOG_PHOTO]
	  where ([FCFILE_STATUS]='Y' and [FSCHANNEL_ID]='01'))--公視圖
  set @PHOG_D=
	  (select COUNT(*) from [MAM].[dbo].[TBLOG_DOC]
	  where ([FCFILE_STATUS]='Y' and [FSCHANNEL_ID]='01'))--公視文
  set @PHOG_VP=
	  (select COUNT(*) from [MAM].[dbo].[TBLOG_VIDEO]
	  where (([FCFILE_STATUS]='Y' or[FCFILE_STATUS]='T') and [FSCHANNEL_ID]='01' and [FSTYPE]='P'))--公視影Promo
  
  set @QUERY_BY = (select FSUSER_ChtName from [MAM].[dbo].[TBUSERS] where [FSUSER_ID]=@CREATED_BY)
  
  insert @CountTable values('公視',@PHOG_V,@PHOG_A,@PHOG_P,@PHOG_D,@PHOG_VP,@PHOG_VLEN,@QUERY_BY)
  
  --set 
  (select @PHOG_V = COUNT(*)
		,@PHOG_VLEN = ISNULL(
		Convert(decimal(8,2),
		sum(
		((convert(float,substring(FSEND_TIMECODE,10,2))+ --ff
		convert(int,substring(FSEND_TIMECODE,7,2)*30)+		   --ss
		convert(int,substring(FSEND_TIMECODE,4,2)*60*30)+		   --mm
		convert(int,substring(FSEND_TIMECODE,1,2)*60*60*30))	   --hh
		-
		(convert(float,substring(FSBEG_TIMECODE,10,2))+ --ff
		convert(int,substring(FSBEG_TIMECODE,7,2)*30)+		   --ss
		convert(int,substring(FSBEG_TIMECODE,4,2)*60*30)+		   --mm
		convert(int,substring(FSBEG_TIMECODE,1,2)*60*60*30))	   --hh
		)/30)/60/60	),0)
	  from [MAM].[dbo].[TBLOG_VIDEO]
	  where (([FCFILE_STATUS]='Y' or[FCFILE_STATUS]='T') and [FSCHANNEL_ID]='07' and [FSTYPE]='G'))--客台影
  set @PHOG_A=
  (select COUNT(*) from [MAM].[dbo].[TBLOG_AUDIO]
	  where ([FCFILE_STATUS]='Y' and [FSCHANNEL_ID]='07'))--客台音
  set @PHOG_P=
	  (select COUNT(*) from [MAM].[dbo].[TBLOG_PHOTO]
	  where ([FCFILE_STATUS]='Y' and [FSCHANNEL_ID]='07'))--客台圖
  set @PHOG_D=
	  (select COUNT(*) from [MAM].[dbo].[TBLOG_DOC]
	  where ([FCFILE_STATUS]='Y' and [FSCHANNEL_ID]='07'))--客台文
    set @PHOG_VP=
	  (select COUNT(*) from [MAM].[dbo].[TBLOG_VIDEO]
	  where (([FCFILE_STATUS]='Y' or[FCFILE_STATUS]='T') and [FSCHANNEL_ID]='07' and [FSTYPE]='P'))--公視文Promo
  insert @CountTable values('客台',@PHOG_V,@PHOG_A,@PHOG_P,@PHOG_D,@PHOG_VP,@PHOG_VLEN,@QUERY_BY)
    
  --set 
  (select @PHOG_V = COUNT(*)  
		,@PHOG_VLEN = ISNULL(
		Convert(decimal(8,2),
		sum(
		((convert(float,substring(FSEND_TIMECODE,10,2))+ --ff
		convert(int,substring(FSEND_TIMECODE,7,2)*30)+		   --ss
		convert(int,substring(FSEND_TIMECODE,4,2)*60*30)+		   --mm
		convert(int,substring(FSEND_TIMECODE,1,2)*60*60*30))	   --hh
		-
		(convert(float,substring(FSBEG_TIMECODE,10,2))+ --ff
		convert(int,substring(FSBEG_TIMECODE,7,2)*30)+		   --ss
		convert(int,substring(FSBEG_TIMECODE,4,2)*60*30)+		   --mm
		convert(int,substring(FSBEG_TIMECODE,1,2)*60*60*30))	   --hh
		)/30)/60/60	),0)
	  from [MAM].[dbo].[TBLOG_VIDEO]
	  where (([FCFILE_STATUS]='Y' or[FCFILE_STATUS]='T') and [FSCHANNEL_ID]='09' and [FSTYPE]='G'))--原台影
  set @PHOG_A=
  (select COUNT(*) from [MAM].[dbo].[TBLOG_AUDIO]
	  where ([FCFILE_STATUS]='Y' and [FSCHANNEL_ID]='09'))--原台音
  set @PHOG_P=
	  (select COUNT(*) from [MAM].[dbo].[TBLOG_PHOTO]
	  where ([FCFILE_STATUS]='Y' and [FSCHANNEL_ID]='09'))--原台圖
  set @PHOG_D=
	  (select COUNT(*) from [MAM].[dbo].[TBLOG_DOC]
	  where ([FCFILE_STATUS]='Y' and [FSCHANNEL_ID]='09'))--原台文
  set @PHOG_VP=
	  (select COUNT(*) from [MAM].[dbo].[TBLOG_VIDEO]
	  where (([FCFILE_STATUS]='Y' or[FCFILE_STATUS]='T') and [FSCHANNEL_ID]='09' and [FSTYPE]='P'))--公視影Promo
  insert @CountTable values('原台',@PHOG_V,@PHOG_A,@PHOG_P,@PHOG_D,@PHOG_VP,@PHOG_VLEN,@QUERY_BY)
	  
  --set 
  (select @PHOG_V = COUNT(*)
		,@PHOG_VLEN = ISNULL(
		Convert(decimal(8,2),
		sum(
		((convert(float,substring(FSEND_TIMECODE,10,2))+ --ff
		convert(int,substring(FSEND_TIMECODE,7,2)*30)+		   --ss
		convert(int,substring(FSEND_TIMECODE,4,2)*60*30)+		   --mm
		convert(int,substring(FSEND_TIMECODE,1,2)*60*60*30))	   --hh
		-
		(convert(float,substring(FSBEG_TIMECODE,10,2))+ --ff
		convert(int,substring(FSBEG_TIMECODE,7,2)*30)+		   --ss
		convert(int,substring(FSBEG_TIMECODE,4,2)*60*30)+		   --mm
		convert(int,substring(FSBEG_TIMECODE,1,2)*60*60*30))	   --hh
		)/30)/60/60	),0)
	  from [MAM].[dbo].[TBLOG_VIDEO]
	  where (([FCFILE_STATUS]='Y' or[FCFILE_STATUS]='T') and [FSCHANNEL_ID]='08' and [FSTYPE]='G'))--宏觀影
  set @PHOG_A=
  (select COUNT(*) from [MAM].[dbo].[TBLOG_AUDIO]
	  where ([FCFILE_STATUS]='Y' and [FSCHANNEL_ID]='08'))--宏觀音
  set @PHOG_P=
	  (select COUNT(*) from [MAM].[dbo].[TBLOG_PHOTO]
	  where ([FCFILE_STATUS]='Y' and [FSCHANNEL_ID]='08'))--宏觀圖
  set @PHOG_D=
	  (select COUNT(*) from [MAM].[dbo].[TBLOG_DOC]
	  where ([FCFILE_STATUS]='Y' and [FSCHANNEL_ID]='08'))--宏觀文
    set @PHOG_VP=
	  (select COUNT(*) from [MAM].[dbo].[TBLOG_VIDEO]
	  where (([FCFILE_STATUS]='Y' or[FCFILE_STATUS]='T') and [FSCHANNEL_ID]='08' and [FSTYPE]='P'))--原台影Promo
  insert @CountTable values('宏觀',@PHOG_V,@PHOG_A,@PHOG_P,@PHOG_D,@PHOG_VP,@PHOG_VLEN,@QUERY_BY)
 
  select * From @CountTable
END



GO
