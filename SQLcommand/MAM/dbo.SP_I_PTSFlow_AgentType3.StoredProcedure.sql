USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Proc [dbo].[SP_I_PTSFlow_AgentType3]

@cslist VARCHAR(8000),--要新增的部門
@vMember_Name nvarchar(50),--使用者的ID(員工編號)
@vAgent_Name nvarchar(50),--代理者的ID(員工編號)
@vSDate nvarchar(100),--代理啟始的時間
@vEDate nvarchar(100)--代理結束的時間
AS
declare @vMember_GUID nvarchar(50)--使用者的GUID
declare @vAgent_GUID nvarchar(50)--代理者的GUID
declare @vaid nvarchar(50)--dbo.Agent_Flow的PK

--set @vMember_Name='sa'
--set @vAgent_Name='admin'
--set @cslist='98,99,100'
--set @vSDate='2012/03/04'
--set @vEDate='2012/03/07'
set @vMember_GUID =(SELECT [Member_GUID] FROM [PTSFlow].[dbo].[OC_Member] where Member_Name=@vMember_Name)
set @vAgent_GUID=(SELECT [Member_GUID] FROM [PTSFlow].[dbo].[OC_Member] where Member_Name=@vAgent_Name)
insert into [PTSFlow].[dbo].Agent_Flow (memberid,agentid,SDate,EDate,Status,AgreeDate,SendDate,ifopen) values(@vMember_GUID,@vAgent_GUID,@vSDate,@vEDate,'1',GETDATE(),GETDATE(),'1')
set @vaid=(select MAX(aid) FROM [PTSFlow].[dbo].[Agent_Flow])

BEGIN 
    DECLARE @spot SMALLINT, @str VARCHAR(8000), @sql VARCHAR(8000) 
 
    WHILE @cslist <> '' 
    BEGIN 
        SET @spot = CHARINDEX(',', @cslist) 
        IF @spot>0 
            BEGIN 
                SET @str = CAST(LEFT(@cslist, @spot-1) AS INT) 
                SET @cslist = RIGHT(@cslist, LEN(@cslist)-@spot) 
            END 
        ELSE 
            BEGIN 
                SET @str = CAST(@cslist AS INT) 
                SET @cslist = '' 
            END 
        SET @sql = 'INSERT INTO [PTSFlow].[dbo].[AgentDetail_Flow] (aid,eflowid) VALUES('+@vaid+','+CONVERT(VARCHAR(100),@str)+')' 
        EXEC(@sql) 
    END 
END
GO
