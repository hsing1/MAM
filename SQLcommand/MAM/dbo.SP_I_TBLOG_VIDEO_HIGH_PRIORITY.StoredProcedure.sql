USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Proc [dbo].[SP_I_TBLOG_VIDEO_HIGH_PRIORITY]

@FSFILE_NO char(16),
@FSVALID_DATE char(8)

As

IF NOT EXISTS(SELECT * FROM TBLOG_VIDEO_HIGH_PRIORITY WHERE FSFILE_NO = @FSFILE_NO AND FSVALID_DATE = @FSVALID_DATE)
BEGIN
	INSERT INTO TBLOG_VIDEO_HIGH_PRIORITY(FSFILE_NO, FSVALID_DATE) VALUES(@FSFILE_NO, @FSVALID_DATE)
END

GO
