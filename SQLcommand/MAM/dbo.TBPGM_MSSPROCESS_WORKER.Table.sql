USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBPGM_MSSPROCESS_WORKER](
	[FSWORKER_NAME] [varchar](50) NOT NULL,
	[FDCREATE_TIME] [datetime] NOT NULL,
	[FDUPDATE_TIME] [datetime] NOT NULL,
	[FCTOKEN_OWNER] [char](1) NOT NULL,
 CONSTRAINT [PK_TBPGM_MSSPROCESS_WORKER] PRIMARY KEY CLUSTERED 
(
	[FSWORKER_NAME] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
