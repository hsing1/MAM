USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/24>
-- Description:   刪除節目表播出提示側標資料(目前已不使用)
-- =============================================
CREATE Proc [dbo].[SP_D_TBPGM_QUEUE_MONITER]


@FSPROG_ID	char(7),
@FNSEQNO	int,
@FDDATE	date,
@FSCHANNEL_ID	varchar(2)

As
delete
from TBPGM_QUEUE_MONITER
where 
 FDDATE=@FDDATE and FSCHANNEL_ID=@FSCHANNEL_ID
 and FSPROG_ID=@FSPROG_ID and FNSEQNO=@FNSEQNO








	
	
GO
