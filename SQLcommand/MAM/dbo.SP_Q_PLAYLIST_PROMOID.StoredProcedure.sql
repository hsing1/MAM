USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   查詢短帶的未來排播狀況
-- =============================================
CREATE Proc [dbo].[SP_Q_PLAYLIST_PROMOID]

@FSPROMO_ID	varchar(11)

As
select A.FDDATE,A.FSCHANNEL_ID,B.FSCHANNEL_NAME,A.FSCREATED_BY,C.FSUSER_ChtName
from TBPGM_ARR_PROMO A
left join TBZCHANNEL B on  A.FSCHANNEL_ID=B.FSCHANNEL_ID
left join TBUSERS C on A.FSCREATED_BY=C.FSUSER_ID
where A.FDDATE >=GETDATE() and A.FSPROMO_ID=@FSPROMO_ID

GO
