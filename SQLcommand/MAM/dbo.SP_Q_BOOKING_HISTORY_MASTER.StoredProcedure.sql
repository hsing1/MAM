USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[SP_Q_BOOKING_HISTORY_MASTER]
@FDBOOKINGDATE_BEGIN varchar(10),
@FDBOOKINGDATE_END varchar(10)

AS

SELECT A.FSBOOKING_NO,dbo.FN_Q_GET_USERNAME_BY_USERID(A.FSUSER_ID) AS FSUSER_ID,A.FDBOOKING_DATE,B.FSREASON,C.FSCHECK_STATUS,A.FSCHECK_ID,
A.FDCHECK_DATE
FROM TBBOOKING_MASTER A,TBZBOOKING_REASON B,TBZBOOKING_CHECK_STATUS C
WHERE A.FSBOOKING_REASON = B.FSNO AND A.FCCHECK_STATUS=C.FSNO AND 
A.FDBOOKING_DATE BETWEEN @FDBOOKINGDATE_BEGIN AND @FDBOOKINGDATE_END


GO
