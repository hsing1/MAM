USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  Proc [dbo].[SP_D_SIGNATURE_PRIORITY]
@FNDEP_ID nvarchar(100),
@FSSignatureID nvarchar(100)
As

  delete [MAM].[dbo].[TBUSER_DEP_SIGNATURE_PRIORITY] 
  where FNDEP_ID=@FNDEP_ID and FSSignatureID=@FSSignatureID

GO
