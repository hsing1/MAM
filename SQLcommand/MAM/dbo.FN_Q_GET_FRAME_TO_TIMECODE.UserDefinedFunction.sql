USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<James.Ho>
-- Create date: <2016/01/19>
-- Description:	<FRAME �� TIMECODE>
-- =============================================
CREATE FUNCTION [dbo].[FN_Q_GET_FRAME_TO_TIMECODE](@Frame AS int) 

RETURNS VARCHAR(15)

AS

BEGIN

    DECLARE @TimeCode VARCHAR(15)
	DECLARE @dd int
	DECLARE @hh int
	DECLARE @mm int
	DECLARE @m1 int
	DECLARE @ss int
	DECLARE @ff int

	SET @hh=@Frame/107892 
	SET @Frame=@Frame-(@hh*107892)

	SET @mm=@Frame/17982
	SET @Frame=@Frame-(@mm*17982)

    IF (@Frame >2)
		BEGIN
			SET @m1=(@Frame-2)/1798
			SET @Frame=@Frame-(@m1 * 1798)
		END
	ELSE
		BEGIN
			SET @m1=0
		END
	
	SET @ss=@Frame/30
	SET @Frame=@Frame-(@ss*30)
	
	SET @TimeCode = REPLICATE('0',2-LEN(@hh)) + RTRIM(CAST(@hh AS CHAR)) + ':' +  CAST(@mm AS VARCHAR(1))+ CAST(@m1 AS VARCHAR(1)) + ':' + REPLICATE('0',2-LEN(@ss)) + RTRIM(CAST(@ss AS CHAR)) + ':' +  REPLICATE('0',2-LEN(@Frame)) + RTRIM(CAST(@Frame AS CHAR)) 
	
	RETURN @TimeCode
END
GO
