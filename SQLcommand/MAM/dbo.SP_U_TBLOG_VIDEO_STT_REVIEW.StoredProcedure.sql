USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_U_TBLOG_VIDEO_STT_REVIEW]
@FSFILE_NO CHAR(16)

As

Declare @CHANGE_FILE_NO CHAR(16)=''

SELECT @CHANGE_FILE_NO = ISNULL(FSCHANGE_FILE_NO,'') FROM TBLOG_VIDEO  /*用檔案編號去找即將被置換的檔案編號*/ 
WHERE TBLOG_VIDEO.FSFILE_NO = @FSFILE_NO 
	 
	IF @CHANGE_FILE_NO <> ''   /*判斷是否有要置換的檔案，進入置換檔案的流程*/
          BEGIN	
		       --先紀錄原始檔案狀態到FSENC_MSG裡
		       update TBLOG_VIDEO set FSENC_MSG=FCFILE_STATUS where FSFILE_NO=@CHANGE_FILE_NO
		  							
				UPDATE MAM.dbo.TBLOG_VIDEO  /*入庫影像檔的狀態改成D(被置換)*/
				SET FCFILE_STATUS = 'D'
				WHERE  MAM.dbo.TBLOG_VIDEO.FSFILE_NO=@CHANGE_FILE_NO
				
				UPDATE MAM.dbo.TBLOG_VIDEO_D/*入庫影像檔_D的狀態改成D(被置換)*/
				SET FCFILE_STATUS = 'D'
				WHERE  MAM.dbo.TBLOG_VIDEO_D.FSFILE_NO=@CHANGE_FILE_NO				
          END             

GO
