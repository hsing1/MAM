USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   依照主控編號取得此編號段落資料(SOM與EOM並依此計算長度)
-- =============================================
CREATE Proc [dbo].[SP_Q_TBLOG_VIDEO_SEG_DURATION_BY_VIDEOID]
@FSVIDEO_ID varchar(10)
As
IF (select COUNT(*) FSVIDEO_ID from TBLOG_VIDEO_SEG A
 left join TBLOG_VIDEO B on A.FSFILE_NO=B.FSFILE_NO
where  B.FCFILE_STATUS in('T','Y')
and A.FSVIDEO_ID=@FSVIDEO_ID
) >= 1 
select A.FSVIDEO_ID,A.FSBEG_TIMECODE,A.FSEND_TIMECODE from TBLOG_VIDEO_SEG A
left join TBLOG_VIDEO B on A.FSFILE_NO=B.FSFILE_NO
where B.FCFILE_STATUS in('T','Y')
and A.FSVIDEO_ID=@FSVIDEO_ID
ELSE
begin
select A.FSVIDEO_ID,A.FSBEG_TIMECODE,A.FSEND_TIMECODE from TBLOG_VIDEO_SEG A
left join TBLOG_VIDEO B on A.FSFILE_NO=B.FSFILE_NO
where B.FCFILE_STATUS not in('D','F','X')
and A.FSVIDEO_ID=@FSVIDEO_ID
end              


              
                  
GO
