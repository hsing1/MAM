USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/04/29>
-- Description:	�s�WTBLOG_SCD
-- =============================================
CREATE PROC [dbo].[SP_I_TBLOG_SCD]
	@FSFILE_ID	varchar(16)/*,
	@FSSTATUS	varchar(10),
	@FDSTIME	smalldatetime,
	@FDETIME	smalldatetime,
	@FSRESULT	nvarchar(200)*/
AS
	SET NOCOUNT ON;

	DECLARE
		@FUGUID UNIQUEIDENTIFIER
		
	SET @FUGUID = NEWID()--'14fc9dbb-9874-4f36-8e64-16c3158ee274'

	BEGIN TRY

		INSERT
			TBLOG_SCD
			(FUGUID, FSFILE_ID)--, FSSTATUS, FDSTIME, FDETIME, FSRESULT)
		VALUES
			(@FUGUID, @FSFILE_ID)--, '', @FDSTIME, @FDETIME, @FSRESULT)
			
		SELECT [RESULT] = @FUGUID
		
	END TRY
	BEGIN CATCH

		SELECT [RESULT] = 'ERROR:' + CAST(@@ERROR AS NVARCHAR(10))

	END CATCH


GO
