USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<kyle.Lin>
-- Create date: <2011/11/14>
-- Description:	For RPT_TBARCHIVE_02(入庫單),依年月查詢數量
-- =============================================
CREATE PROCEDURE [dbo].[SP_R_STO_CountList_QR1505]
	@FSID nvarchar(50),
	@EPISODE nvarchar(2000)
	--@CREATE_BY nvarchar(50)
AS
BEGIN

	--declare @FSID nvarchar(50)='0000001'
	--declare @EPISODE nvarchar(50)  ='1,2,5,7'
	declare @FSNAME nvarchar(200)
	declare @Count_腳本 int
	declare @Count_對白 int
	declare @Count_宣傳照 int
	declare @Count_字幕 int
	declare @Count_SD播出帶 int
	declare @Count_HD播出帶 int
	declare @Count_SD分軌帶 int
	declare @Count_HD分軌帶 int
	declare @Count_SD無字幕剪輯母帶 int
	declare @Count_HD無字幕剪輯母帶 int
	declare @Count_SD宣傳帶 int
	declare @Count_HD宣傳帶 int
	declare @cChannelName nvarchar(50)

	declare @TempTable table(
	[cChannelName] nvarchar(50),
	[FSID] nvarchar(50),
	[FSNAME] nvarchar(200),
	[FNEPISODE] nvarchar(2000),
	[Count_腳本] int,
	[Count_對白] int,
	[Count_宣傳照] int,
	[Count_字幕] int,
	[Count_SD播出帶] int,
	[Count_HD播出帶] int,
	[Count_SD分軌帶] int,
	[Count_HD分軌帶] int,
	[Count_SD無字幕剪輯母帶] int,
	[Count_HD無字幕剪輯母帶] int,
	[Count_SD宣傳帶] int,
	[Count_HD宣傳帶] int
	)



	set @Count_腳本=
	(select COUNT(*) from [MAM].[dbo].[TBLOG_DOC] where [FSARC_TYPE]='021'					--腳本021
	and FSID=@FSID and FNEPISODE IN (select Data from dbo.FN_SPLIT(@EPISODE,',')))

	set @Count_對白=
	(select COUNT(*) from [MAM].[dbo].[TBLOG_DOC] where [FSARC_TYPE]='022'					--對(旁)白022
	and FSID=@FSID and FNEPISODE IN (select Data from dbo.FN_SPLIT(@EPISODE,',')))

	set @Count_宣傳照=
	(select COUNT(*) from [MAM].[dbo].[TBLOG_PHOTO] where [FSARC_TYPE]='008'				--宣傳照008
	and FSID=@FSID and FNEPISODE IN (select Data from dbo.FN_SPLIT(@EPISODE,',')))

	set @Count_字幕=
	(select COUNT(*) from [MAM].[dbo].[TBLOG_DOC] where [FSARC_TYPE]='005'					--字幕005
	and FSID=@FSID and FNEPISODE IN (select Data from dbo.FN_SPLIT(@EPISODE,',')))

	set @Count_SD播出帶=
	(select COUNT(*) from [MAM].[dbo].[TBLOG_VIDEO] where [FSARC_TYPE]='001'					--SD播出帶001
	and FSID=@FSID and FNEPISODE IN (select Data from dbo.FN_SPLIT(@EPISODE,',')))

	set @Count_HD播出帶=
	(select COUNT(*) from [MAM].[dbo].[TBLOG_VIDEO] where [FSARC_TYPE]='002'					--HD播出帶002
	and FSID=@FSID and FNEPISODE IN (select Data from dbo.FN_SPLIT(@EPISODE,',')))

	set @Count_SD分軌帶=
	(select COUNT(*) from [MAM].[dbo].[TBLOG_VIDEO] where [FSARC_TYPE]='011'					--SD分軌帶011
	and FSID=@FSID and FNEPISODE IN (select Data from dbo.FN_SPLIT(@EPISODE,',')))

	set @Count_HD分軌帶=
	(select COUNT(*) from [MAM].[dbo].[TBLOG_VIDEO] where [FSARC_TYPE]='012'					--HD分軌帶012
	and FSID=@FSID and FNEPISODE IN (select Data from dbo.FN_SPLIT(@EPISODE,',')))

	set @Count_SD無字幕剪輯母帶=
	(select COUNT(*) from [MAM].[dbo].[TBLOG_VIDEO] where [FSARC_TYPE]='013'					--SD無字幕剪輯母帶013
	and FSID=@FSID and FNEPISODE IN (select Data from dbo.FN_SPLIT(@EPISODE,',')))

	set @Count_HD無字幕剪輯母帶=
	(select COUNT(*) from [MAM].[dbo].[TBLOG_VIDEO] where [FSARC_TYPE]='014'					--HD無字幕剪輯母帶014
	and FSID=@FSID and FNEPISODE IN (select Data from dbo.FN_SPLIT(@EPISODE,',')))

	set @Count_SD宣傳帶=
	(select COUNT(*) from [MAM].[dbo].[TBLOG_VIDEO] where [FSARC_TYPE]='023'					--SD宣傳帶023
	and FSID=@FSID and FNEPISODE IN (select Data from dbo.FN_SPLIT(@EPISODE,',')))

	set @Count_HD宣傳帶=
	(select COUNT(*) from [MAM].[dbo].[TBLOG_VIDEO] where [FSARC_TYPE]='024'					--HD宣傳帶024
	and FSID=@FSID and FNEPISODE IN (select Data from dbo.FN_SPLIT(@EPISODE,',')))

	set @cChannelName=(SELECT [TBPROG_M].FSCHANNEL_ID FROM [MAM].[dbo].[TBPROG_M]-- left outer join TBZCHANNEL on [TBPROG_M].FSCHANNEL_ID=TBZCHANNEL.FSCHANNEL_ID
	where FSPROG_ID=@FSID)
	if @cChannelName='01'
	set @cChannelName='公視基金會'
	else if @cChannelName='07'
	set @cChannelName='客家電視台'
	else if @cChannelName='08'
	set @cChannelName='台灣宏觀電視'
	else if @cChannelName='09'
	set @cChannelName='原住民族電視台'
	set @FSNAME=(select [FSPGMNAME] from [MAM].[dbo].[TBPROG_M] as a where a.FSPROG_ID=@FSID)

	insert @TempTable values(@cChannelName,@FSID,@FSNAME,@EPISODE,@Count_腳本,@Count_對白,@Count_宣傳照,@Count_字幕,@Count_SD播出帶,@Count_HD播出帶,@Count_SD分軌帶,@Count_HD分軌帶,@Count_SD無字幕剪輯母帶,@Count_HD無字幕剪輯母帶,@Count_SD宣傳帶,@Count_HD宣傳帶)

	select * from @TempTable
END


GO
