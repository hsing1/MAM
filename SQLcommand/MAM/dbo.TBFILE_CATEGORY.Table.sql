USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBFILE_CATEGORY](
	[FNNO] [int] NOT NULL,
	[FSCATEGORY_NAME] [nvarchar](10) NOT NULL,
	[FSCATEGORY_CNAME] [nvarchar](10) NULL,
 CONSTRAINT [PK_TBFILE_CATEGORY] PRIMARY KEY CLUSTERED 
(
	[FNNO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBFILE_CATEGORY', @level2type=N'COLUMN',@level2name=N'FNNO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'類別英文名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBFILE_CATEGORY', @level2type=N'COLUMN',@level2name=N'FSCATEGORY_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'類別中文名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBFILE_CATEGORY', @level2type=N'COLUMN',@level2name=N'FSCATEGORY_CNAME'
GO
EXEC sys.sp_addextendedproperty @name=N'描述', @value=N'檔案類別' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBFILE_CATEGORY'
GO
