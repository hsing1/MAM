USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   查詢短帶編碼與名稱資料
-- =============================================
CREATE Proc [dbo].[SP_Q_TBPGM_PROMO_SIMPLE]

@FSPROMO_NAME	nvarchar(50)

As
select FSPROMO_ID,FSPROMO_NAME 
from TBPGM_PROMO
where 
FSPROMO_NAME like '%' + @FSPROMO_NAME +'%'

GO
