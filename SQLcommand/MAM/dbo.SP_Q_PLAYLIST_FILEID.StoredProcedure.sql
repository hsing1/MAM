USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   依照檔案編號查詢此檔案在未來播出運行表中的排播狀況
-- =============================================
CREATE Proc [dbo].[SP_Q_PLAYLIST_FILEID]

@FSFILE_NO	varchar(16)

As
select 
 B.FDDATE,B.FSCHANNEL_ID,C.FSCHANNEL_NAME,B.FSCREATED_BY,D.FSUSER_ChtName,D.FSEMAIL
from TBLOG_VIDEO A,TBPGM_ARR_PROMO B
left join TBZCHANNEL C on  B.FSCHANNEL_ID=C.FSCHANNEL_ID
left join TBUSERS D on B.FSCREATED_BY=D.FSUSER_ID
where 
A.FSID=B.FSPROMO_ID and 
B.FDDATE >=GETDATE() and 
A.FSFILE_NO=@FSFILE_NO

union
select 
 B.FDDATE,B.FSCHANNEL_ID,C.FSCHANNEL_NAME,B.FSCREATED_BY,D.FSUSER_ChtName,D.FSEMAIL
from TBLOG_VIDEO A,TBPGM_COMBINE_QUEUE B
left join TBZCHANNEL C on  B.FSCHANNEL_ID=C.FSCHANNEL_ID
left join TBUSERS D on B.FSCREATED_BY=D.FSUSER_ID
where 
A.FSID=B.FSPROG_ID and 
A.FNEPISODE=B.FNEPISODE and
B.FDDATE >=GETDATE() and 
A.FSFILE_NO=@FSFILE_NO

GO
