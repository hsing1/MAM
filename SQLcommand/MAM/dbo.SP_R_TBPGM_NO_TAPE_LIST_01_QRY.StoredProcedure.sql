USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   查詢節目未到帶清單報表
-- =============================================

CREATE PROCEDURE [dbo].[SP_R_TBPGM_NO_TAPE_LIST_01_QRY]
	@QUERY_KEY	VARCHAR(36)
AS
BEGIN
	SELECT
		[QUERY_KEY]
      ,[QUERY_KEY_SUBID]
      ,[QUERY_BY]
      ,[QUERY_DATE]
      ,[播出日期]
      ,[頻道]
      ,[節目名稱]
      ,[集數]
      ,[影像編號]
      ,[起始時間]
      ,[結束時間]
      ,[LIVE播出]
      ,[上檔下檔]
	FROM
		RPT_TBPGM_NO_TAPE_LIST_01
	WHERE
		(QUERY_KEY = @QUERY_KEY)
	ORDER BY
		[播出日期], [起始時間], [結束時間]
END






GO
