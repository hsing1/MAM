USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBPGM_LOUTH_KEY](
	[FSCHANNEL_ID] [varchar](2) NOT NULL,
	[FSGROUP] [nvarchar](10) NULL,
	[FSNAME] [nvarchar](50) NULL,
	[FSNO] [varchar](4) NOT NULL,
	[FSMEMO] [nvarchar](100) NULL,
	[FSLAYEL] [varchar](1) NOT NULL,
	[FSRULE] [varchar](20) NULL,
 CONSTRAINT [PK_TBPGM_LOUTH_KEY_1] PRIMARY KEY CLUSTERED 
(
	[FSCHANNEL_ID] ASC,
	[FSNO] ASC,
	[FSLAYEL] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'頻道' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_LOUTH_KEY', @level2type=N'COLUMN',@level2name=N'FSCHANNEL_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'群組' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_LOUTH_KEY', @level2type=N'COLUMN',@level2name=N'FSGROUP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_LOUTH_KEY', @level2type=N'COLUMN',@level2name=N'FSNAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_LOUTH_KEY', @level2type=N'COLUMN',@level2name=N'FSNO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'備註' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_LOUTH_KEY', @level2type=N'COLUMN',@level2name=N'FSMEMO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'階層' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_LOUTH_KEY', @level2type=N'COLUMN',@level2name=N'FSLAYEL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'規則' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_LOUTH_KEY', @level2type=N'COLUMN',@level2name=N'FSRULE'
GO
EXEC sys.sp_addextendedproperty @name=N'描述', @value=N'Louth Key資料表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPGM_LOUTH_KEY'
GO
