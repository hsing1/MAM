USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   查詢活動資料
-- =============================================
CREATE Proc [dbo].[SP_Q_TBPGM_ACTION_CODE]

As
/*活動代碼*/
SELECT 'TBPGM_ACTION' TABLENAME,FSACTIONID as ID,[dbo].[FN_Q_CHANGE_STOP_NAME](FBISENABLE, FSACTIONNAME) as NAME,FSSORT FROM MAM.dbo.TBPGM_ACTION
where FSACTIONNAME<>''
order by TABLENAME,FSSORT
GO
