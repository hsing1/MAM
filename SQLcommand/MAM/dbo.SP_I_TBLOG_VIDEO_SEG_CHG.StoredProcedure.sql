USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Jarvis.Yang>
-- Create date: <2013-06-04>
-- Description:	insert 資料進TBLOG_VIDEO_SEG_CHG(微調TimeCode用)
-- =============================================
create PROCEDURE [dbo].[SP_I_TBLOG_VIDEO_SEG_CHG]
	@FSFILE_NO char(16),
	@FNSEG_NO char(2),
	@FSVIDEO_ID varchar(10),
	@FSBEG_TIMECODE_OLD char(11),
	@FSEND_TIMECODE_OLD char(11),
    @FCLOW_RES_OLD char(1),
	@FSBEG_TIMECODE_NEW char(11),
	@FSEND_TIMECODE_NEW char(11),
	@FSCREATED_BY varchar(50)
AS
BEGIN



declare @FNNO bigint 
select @FNNO=COUNT(*) from TBLOG_VIDEO_SEG_CHG
SET @FNNO=@FNNO+1

	insert into TBLOG_VIDEO_SEG_CHG
	(FNNO,FSFILE_NO,FNSEG_NO,FSVIDEO_ID,FSBEG_TIMECODE_OLD,FSEND_TIMECODE_OLD
,FCLOW_RES_OLD,FSBEG_TIMECODE_NEW,FSEND_TIMECODE_NEW,FSCREATED_BY,FDCREATED_DATE)
 values
(@FNNO,
@FSFILE_NO,@FNSEG_NO,@FSVIDEO_ID,@FSBEG_TIMECODE_OLD,@FSEND_TIMECODE_OLD,@FCLOW_RES_OLD,@FSBEG_TIMECODE_NEW,@FSEND_TIMECODE_NEW,@FSCREATED_BY,GETDATE()
)
END





/****** Object:  StoredProcedure [dbo].[SP_U_TBLOG_VIDEO_SEG_Replacement]    Script Date: 06/18/2013 16:19:30 ******/
SET ANSI_NULLS ON

GO
