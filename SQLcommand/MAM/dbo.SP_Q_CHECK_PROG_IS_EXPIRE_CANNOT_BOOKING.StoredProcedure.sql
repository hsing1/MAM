USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------
--2012/12/12 <David.Sin>新增SP_Q_CHECK_PROG_IS_EXPIRE_CANNOT_BOOKING 
--2015/12/21 <David.Sin>修改增加fsFILE_TYPE長度
--查詢調用的檔案是否已經過期，若有值則已經過期不可調用
------------------------------------------------------------------

CREATE PROC [dbo].[SP_Q_CHECK_PROG_IS_EXPIRE_CANNOT_BOOKING]
@FSFILE_NOs varchar(2000),
@FSFILE_TYPEs varchar(100)

AS
DECLARE @t TABLE(fsFILE_NO varchar(16))
DECLARE @tFSFILE_NOs TABLE(fnID INT,fsFILE_NO varchar(16))
DECLARE @tFSFILE_TYPEs TABLE(fnID INT,fsFILE_TYPE varchar(10))

INSERT INTO @tFSFILE_NOs
SELECT Id,data FROM [dbo].[FN_SPLIT](@FSFILE_NOs,',')

INSERT INTO @tFSFILE_TYPEs
SELECT Id,data FROM [dbo].[FN_SPLIT](@FSFILE_TYPEs,',')

--SELECT fsFILE_NO,fsFILE_TYPE FROM @tFSFILE_NOs AS A JOIN @tFSFILE_TYPEs AS B ON A.fnID = B.fnID


IF EXISTS(SELECT fsFILE_TYPE FROM @tFSFILE_TYPEs WHERE fsFILE_TYPE = 'V')
BEGIN
	INSERT INTO @t
	SELECT 
		[TBLOG_VIDEO].fsFILE_NO 
	FROM 
		[TBLOG_VIDEO] JOIN 
		(SELECT fsFILE_NO,fsFILE_TYPE FROM @tFSFILE_NOs AS A JOIN @tFSFILE_TYPEs AS B ON A.fnID = B.fnID) AS A ON [TBLOG_VIDEO].fsFILE_NO = A.fsFILE_NO
		JOIN 
		[TBPROG_D] ON [TBLOG_VIDEO].[FSID] = [TBPROG_D].[FSPROG_ID] AND [TBLOG_VIDEO].[FNEPISODE] = [TBPROG_D].[FNEPISODE]
	WHERE
		A.fsFILE_TYPE = 'V' AND YEAR([TBPROG_D].[FDEXPIRE_DATE]) > 1950 AND [TBPROG_D].[FDEXPIRE_DATE] < GETDATE()  AND [TBLOG_VIDEO].[FSTYPE] = 'G'


	INSERT INTO @t
	SELECT 
		[TBLOG_VIDEO].fsFILE_NO 
	FROM 
		[TBLOG_VIDEO] JOIN 
		(SELECT fsFILE_NO,fsFILE_TYPE FROM @tFSFILE_NOs AS A JOIN @tFSFILE_TYPEs AS B ON A.fnID = B.fnID) AS A ON [TBLOG_VIDEO].fsFILE_NO = A.fsFILE_NO
		JOIN 
		[TBPGM_PROMO] ON [TBLOG_VIDEO].[FSID] = [TBPGM_PROMO].[FSPROMO_ID] AND [TBLOG_VIDEO].[FNEPISODE] = [TBPGM_PROMO].[FNEPISODE]
	WHERE
		A.fsFILE_TYPE = 'V' AND YEAR([TBPGM_PROMO].[FDEXPIRE_DATE]) > 1950 AND [TBPGM_PROMO].[FDEXPIRE_DATE] < GETDATE()  AND [TBLOG_VIDEO].[FSTYPE] = 'P'
END

IF EXISTS(SELECT fsFILE_TYPE FROM @tFSFILE_TYPEs WHERE fsFILE_TYPE = 'A')
BEGIN

	INSERT INTO @t
	SELECT 
		[TBLOG_AUDIO].fsFILE_NO 
	FROM 
		[TBLOG_AUDIO] JOIN 
		(SELECT fsFILE_NO,fsFILE_TYPE FROM @tFSFILE_NOs AS A JOIN @tFSFILE_TYPEs AS B ON A.fnID = B.fnID) AS A ON [TBLOG_AUDIO].fsFILE_NO = A.fsFILE_NO
		JOIN 
		[TBPROG_D] ON [TBLOG_AUDIO].[FSID] = [TBPROG_D].[FSPROG_ID] AND [TBLOG_AUDIO].[FNEPISODE] = [TBPROG_D].[FNEPISODE]
	WHERE
		A.fsFILE_TYPE = 'A' AND YEAR([TBPROG_D].[FDEXPIRE_DATE]) > 1950 AND [TBPROG_D].[FDEXPIRE_DATE] < GETDATE() AND [TBLOG_AUDIO].[FSTYPE] = 'G'


	INSERT INTO @t
	SELECT 
		[TBLOG_AUDIO].fsFILE_NO 
	FROM 
		[TBLOG_AUDIO] JOIN 
		(SELECT fsFILE_NO,fsFILE_TYPE FROM @tFSFILE_NOs AS A JOIN @tFSFILE_TYPEs AS B ON A.fnID = B.fnID) AS A ON [TBLOG_AUDIO].fsFILE_NO = A.fsFILE_NO
		JOIN 
		[TBPGM_PROMO] ON [TBLOG_AUDIO].[FSID] = [TBPGM_PROMO].[FSPROMO_ID] AND [TBLOG_AUDIO].[FNEPISODE] = [TBPGM_PROMO].[FNEPISODE]
	WHERE
		A.fsFILE_TYPE = 'A' AND YEAR([TBPGM_PROMO].[FDEXPIRE_DATE]) > 1950 AND [TBPGM_PROMO].[FDEXPIRE_DATE] < GETDATE() AND [TBLOG_AUDIO].[FSTYPE] = 'P'
END


IF EXISTS(SELECT fsFILE_TYPE FROM @tFSFILE_TYPEs WHERE fsFILE_TYPE = 'P')
BEGIN

	INSERT INTO @t
	SELECT 
		[TBLOG_PHOTO].fsFILE_NO 
	FROM 
		[TBLOG_PHOTO] JOIN 
		(SELECT fsFILE_NO,fsFILE_TYPE FROM @tFSFILE_NOs AS A JOIN @tFSFILE_TYPEs AS B ON A.fnID = B.fnID) AS A ON [TBLOG_PHOTO].fsFILE_NO = A.fsFILE_NO
		JOIN 
		[TBPROG_D] ON [TBLOG_PHOTO].[FSID] = [TBPROG_D].[FSPROG_ID] AND [TBLOG_PHOTO].[FNEPISODE] = [TBPROG_D].[FNEPISODE]
	WHERE
		A.fsFILE_TYPE = 'P' AND YEAR([TBPROG_D].[FDEXPIRE_DATE]) > 1950 AND [TBPROG_D].[FDEXPIRE_DATE] < GETDATE() AND [TBLOG_PHOTO].[FSTYPE] = 'G'


	INSERT INTO @t
	SELECT 
		[TBLOG_PHOTO].fsFILE_NO 
	FROM 
		[TBLOG_PHOTO] JOIN 
		(SELECT fsFILE_NO,fsFILE_TYPE FROM @tFSFILE_NOs AS A JOIN @tFSFILE_TYPEs AS B ON A.fnID = B.fnID) AS A ON [TBLOG_PHOTO].fsFILE_NO = A.fsFILE_NO
		JOIN 
		[TBPGM_PROMO] ON [TBLOG_PHOTO].[FSID] = [TBPGM_PROMO].[FSPROMO_ID] AND [TBLOG_PHOTO].[FNEPISODE] = [TBPGM_PROMO].[FNEPISODE]
	WHERE
		A.fsFILE_TYPE = 'P' AND YEAR([TBPGM_PROMO].[FDEXPIRE_DATE]) > 1950 AND [TBPGM_PROMO].[FDEXPIRE_DATE] < GETDATE()  AND [TBLOG_PHOTO].[FSTYPE] = 'P'
END


IF EXISTS(SELECT fsFILE_TYPE FROM @tFSFILE_TYPEs WHERE fsFILE_TYPE = 'D')
BEGIN

	INSERT INTO @t
	SELECT 
		[TBLOG_DOC].fsFILE_NO 
	FROM 
		[TBLOG_DOC] JOIN 
		(SELECT fsFILE_NO,fsFILE_TYPE FROM @tFSFILE_NOs AS A JOIN @tFSFILE_TYPEs AS B ON A.fnID = B.fnID) AS A ON [TBLOG_DOC].fsFILE_NO = A.fsFILE_NO
		JOIN 
		[TBPROG_D] ON [TBLOG_DOC].[FSID] = [TBPROG_D].[FSPROG_ID] AND [TBLOG_DOC].[FNEPISODE] = [TBPROG_D].[FNEPISODE]
	WHERE
		A.fsFILE_TYPE = 'D' AND YEAR([TBPROG_D].[FDEXPIRE_DATE]) > 1950 AND [TBPROG_D].[FDEXPIRE_DATE] < GETDATE()  AND [TBLOG_DOC].[FSTYPE] = 'G'


	INSERT INTO @t
	SELECT 
		[TBLOG_DOC].fsFILE_NO 
	FROM 
		[TBLOG_DOC] JOIN 
		(SELECT fsFILE_NO,fsFILE_TYPE FROM @tFSFILE_NOs AS A JOIN @tFSFILE_TYPEs AS B ON A.fnID = B.fnID) AS A ON [TBLOG_DOC].fsFILE_NO = A.fsFILE_NO
		JOIN 
		[TBPGM_PROMO] ON [TBLOG_DOC].[FSID] = [TBPGM_PROMO].[FSPROMO_ID] AND [TBLOG_DOC].[FNEPISODE] = [TBPGM_PROMO].[FNEPISODE]
	WHERE
		A.fsFILE_TYPE = 'D' AND YEAR([TBPGM_PROMO].[FDEXPIRE_DATE]) > 1950 AND [TBPGM_PROMO].[FDEXPIRE_DATE] < GETDATE() AND [TBLOG_DOC].[FSTYPE] = 'P'
END

SELECT ISNULL((SELECT ISNULL(fsFILE_NO + ',','') FROM @t FOR XML PATH('')),'') AS fsFILE_NOs
GO
