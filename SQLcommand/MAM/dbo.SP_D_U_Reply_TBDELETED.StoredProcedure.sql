USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jarvis.Yang
-- Create date: 20130625
-- Description:	抽單發生錯誤時要回復TBDELETED與TBLOG_VIDEO的資料
-- =============================================
create PROCEDURE [dbo].[SP_D_U_Reply_TBDELETED]
	 @file_No char(16),
	 @oriState char 
AS
BEGIN
	declare @count int
select @count=COUNT(*) from TBDELETED where FSFILE_NO=@file_No 

if(@count>0)
begin
delete from TBDELETED where FSFILE_NO=@file_No
update TBLOG_VIDEO SET FCFILE_STATUS=@oriState where FSFILE_NO=@file_No

end

END





GO
