USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   �d�ߵu�a���
-- =============================================
CREATE Proc [dbo].[SP_Q_SP_Q_TBPGM_PROMO_BYPROMOID]
@FSPROMO_ID	varchar(11)

As
--SELECT *FROM MAM.dbo.TBPGM_PROMO
--WHERE TBPGM_PROMO.FSPROMO_ID = @FSPROMO_ID

select ISNULL(b.FSUSER_ChtName,'') as FSCREATED_BY_NAME, ISNULL(C.FSUSER_ChtName,'') as FSUPDATED_BY_NAME , ISNULL(f.FSUSER_ChtName,'') as FSDEL_BY_NAME,
ISNULL(CONVERT(VARCHAR(20) ,a.FDCREATED_DATE , 111),'') as WANT_FDCREATEDDATE,
ISNULL(CONVERT(VARCHAR(20) ,a.FDUPDATED_DATE , 111),'') as WANT_FDUPDATEDDATE,
ISNULL(d.FSPGMNAME,'')  as FSPROG_ID_NAME,
ISNULL(e.FSPGDNAME,'')  as FNEPISODE_NAME,
a.* FROM TBPGM_PROMO  as a  
               left outer JOIN TBUSERS as b on a.FSCREATED_BY=b.FSUSER_ID
               left outer join TBUSERS as c on a.FSUPDATED_BY=c.FSUSER_ID
               left outer join TBUSERS as f on a.FSDELUSER=f.FSUSER_ID               
               left outer join TBPROG_M as d on a.FSPROG_ID=d.FSPROG_ID -----[MAM].[dbo].[TBPROG_M]
               left outer join TBPROG_D as e on a.FSPROG_ID=e.FSPROG_ID and a.FNEPISODE=e.FNEPISODE
where  a.FSPROMO_ID = @FSPROMO_ID
GO
