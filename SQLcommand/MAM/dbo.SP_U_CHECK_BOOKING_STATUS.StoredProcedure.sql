USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <David.Sin>
-- Create date:   <2010/10/02>
-- Description:   更新調用審核結果
-- =============================================

CREATE PROC [dbo].[SP_U_CHECK_BOOKING_STATUS]
@FSBOOKING_NO varchar(12),
@FSSEQ varchar(3),
@FCCHECK char(1),
@FSCHECK_ID varchar(50),
@FDCHECK_DATE varchar(10)
AS

UPDATE TBBOOKING_DETAIL
SET FCCHECK=@FCCHECK,
	FSCHECK_ID=@FSCHECK_ID,
	FDCHECK_DATE=@FDCHECK_DATE
WHERE FSBOOKING_NO=@FSBOOKING_NO AND FSSEQ=@FSSEQ

GO
