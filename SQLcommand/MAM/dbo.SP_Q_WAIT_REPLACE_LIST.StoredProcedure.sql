USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2013/07/11>
-- Description:   �d�߫ݸm���M��
-- =============================================
CREATE Proc [dbo].[SP_Q_WAIT_REPLACE_LIST]
@FDDATE	date,
@FSCHANNEL_ID	varchar(2)


As
select distinct A.FSPROG_ID,A.FNEPISODE,A.FSPROG_NAME,A.FSFILE_NO,A.FSVIDEO_ID,B.FSBRO_ID,C.FCCHECK_STATUS
from TBPGM_COMBINE_QUEUE A
left join TBLOG_VIDEO B on A.FSFILE_NO= B.fschange_file_no and A.FSFILE_NO <>''
left join tbbroadcast C on B.FSBRO_ID=C.FSBRO_ID
where A.fddate=@FDDATE and A.fschannel_ID=@FSCHANNEL_ID
and A.fsfile_no in (select fschange_file_no from tblog_video where fschange_file_no<>'')
and A.fnlive=0
union 
select distinct A.FSPROMO_ID,0,B.FSPROMO_NAME,A.FSFILE_NO,A.FSVIDEO_ID,C.FSBRO_ID,D.FCCHECK_STATUS
from tbpgm_Arr_Promo A
left join TBPGM_PROMO B on A.FSPROMO_ID=B.FSPROMO_ID
left join TBLOG_VIDEO C on A.FSFILE_NO= C.fschange_file_no and A.FSFILE_NO <>''
left join tbbroadcast D on C.FSBRO_ID=D.FSBRO_ID
where A.fddate=@FDDATE and A.fschannel_ID=@FSCHANNEL_ID
and A.fsfile_no in (select fschange_file_no from tblog_video where fschange_file_no<>'')
GO
