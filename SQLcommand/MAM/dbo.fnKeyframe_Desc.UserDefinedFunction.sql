USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <David.Sin>
-- Create date:   <2010/10/02>
-- Description:   根據檔案編號取得Keyframe描述
-- =============================================

CREATE FUNCTION [dbo].[fnKeyframe_Desc](@Video_No varchar(20)) 
RETURNS nvarchar(max)
AS
BEGIN
 --declare @Begin_TimeCode varchar(50) 
 --declare @End_TimeCode varchar(50)
 --declare @Desc nvarchar(max)
 --declare @Desc_T nvarchar(max)
 --set @Desc_T=''
 --SELECT @Begin_TimeCode=@Video_No+'_'+REPLACE(FSBEG_TIMECODE,':','')
 --FROM TBLOG_VIDEO_D Where FSFILE_NO=@Video_No And FNSEQ_NO=@SEQ_NO
 
 --SELECT @End_TimeCode=@Video_No+'_'+REPLACE(FSEND_TIMECODE,':','')
 --FROM TBLOG_VIDEO_D Where FSFILE_NO=@Video_No And FNSEQ_NO=@SEQ_NO
 
 --declare a_cursor cursor for 
 -- select FSDESCRIPTION from TBLOG_VIDEO_KEYFRAME
 -- where FSVIDEO_NO=@Video_No
 --  and isnull(FSDESCRIPTION,'')<>'' And 
 --  FSFILE_NO between @Begin_TimeCode And @End_TimeCode
 -- order by FSFILE_NO  

 -- Open a_cursor
 -- fetch next from a_cursor into @Desc
 -- while @@fetch_status=0   
 --  begin
 --     set @Desc_T=@Desc_T+@Desc
 --     fetch next from a_cursor into @Desc
 --  end 
 -- close a_cursor

 -- return @Desc_T
 --DECLARE @FSDESCRIPTION_TEMP NVARCHAR(MAX)
 DECLARE @FSDESCRIPTION_ALL_TEMP NVARCHAR(MAX)
 
 SELECT  DISTINCT
	@FSDESCRIPTION_ALL_TEMP = 
		 ISNULL((SELECT ISNULL(FSDESCRIPTION,'') FROM TBLOG_VIDEO_KEYFRAME B WHERE A.FSFILE_NO = B.FSVIDEO_NO FOR XML PATH('')) ,'')
	
 FROM TBLOG_VIDEO_D A
 WHERE A.FSFILE_NO=@Video_No
 
 RETURN @FSDESCRIPTION_ALL_TEMP
 
 --SET @FSDESCRIPTION_TEMP=''
 --SET @FSDESCRIPTION_ALL_TEMP=''
 --DECLARE CURSOR_A CURSOR FOR
	--SELECT FSDESCRIPTION FROM TBLOG_VIDEO_KEYFRAME WHERE FSVIDEO_NO=@Video_No
	
 --OPEN CURSOR_A
 --FETCH NEXT FROM CURSOR_A INTO @FSDESCRIPTION_TEMP
 --WHILE @@fetch_status=0
 --BEGIN
	--SET @FSDESCRIPTION_ALL_TEMP = @FSDESCRIPTION_ALL_TEMP + @FSDESCRIPTION_TEMP + ' '
	--FETCH NEXT FROM CURSOR_A INTO @FSDESCRIPTION_TEMP
	
 --END
 --CLOSE CURSOR_A
 
 --RETURN @FSDESCRIPTION_ALL_TEMP
 
 
End 



GO
