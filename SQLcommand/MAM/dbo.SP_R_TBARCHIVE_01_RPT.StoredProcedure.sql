USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/03/25>
-- Description:	For RPT_TBARCHIVE_01(入庫單), 報表呼叫用
-- =============================================
CREATE PROCEDURE [dbo].[SP_R_TBARCHIVE_01_RPT]
	@QUERY_KEY	VARCHAR(36)
AS
BEGIN
	SELECT
		[QUERY_KEY]
      ,[QUERY_KEY_SUBID]
      ,[QUERY_BY]
      ,[QUERY_DATE]
      ,[入庫單號]
      ,[類型_名稱]
      ,[節目_節目名稱]
      ,[節目_集別]
      ,[節目_子集名稱]
      ,[節目_播出頻道]
      ,[節目_總集數]
      ,[節目_每集時長]
      ,[宣傳帶_宣傳帶名稱]
      ,檔案名稱
      ,[標題]
      ,[檔案類型]
      ,[入庫類型]
	FROM
		RPT_TBARCHIVE_01
	WHERE
		(QUERY_KEY = @QUERY_KEY)
	ORDER BY
		[入庫單號], [標題]
END




GO
