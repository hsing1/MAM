USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--USE MAM

CREATE Proc [dbo].[SP_I_TBNORECORD]

@FSTYPE			CHAR(2) ,
@FSNO_DATE		CHAR(8) ,
@FSNOTE			NVARCHAR(50) ,
@FSCREATED_BY	VARCHAR(50)  

As

Declare @temp_FSNO_NOW VARCHAR(12)=''         /*目前資料庫的編號*/  
Declare @temp_FSNO_New VARCHAR(12)=''         /*即將要寫入資料庫的編號(加工過後)*/
Declare @temp_FSNO_LsstDay VARCHAR(12)=''     /*當資料庫最後一筆與取號時的日期不同(資料庫存在最後一筆的編號)*/
set @FSNO_DATE  =CONVERT(varchar(8) , getdate(), 112)   
SELECT @temp_FSNO_NOW = ISNULL(MAX(FSNO),'') FROM MAM.dbo.TBNORECORD  /*用今天的日期去取號*/ 
WHERE MAM.dbo.TBNORECORD.FSNO_DATE = @FSNO_DATE AND MAM.dbo.TBNORECORD.FSTYPE = @FSTYPE  

IF @temp_FSNO_NOW = ''
   BEGIN   
     
     SELECT @temp_FSNO_LsstDay = ISNULL(MAX(FSNO),'') FROM MAM.dbo.TBNORECORD /*若是沒有表示今天尚未取過號，就要找出該類別是否曾經取過號*/ 
     WHERE  MAM.dbo.TBNORECORD.FSTYPE = @FSTYPE
          
     SET @temp_FSNO_New = @FSNO_DATE + '0001'    /*取號原則：當天若是未取號，數值就塞0001*/      
     
      IF @temp_FSNO_LsstDay = ''   /*若是該類別未曾取過號，資料庫即新增*/ 
          BEGIN
                INSERT INTO MAM.dbo.TBNORECORD(FSNO,FSTYPE,FSNO_DATE,FSNOTE,FSCREATED_BY,FDCREATED_DATE)
                VALUES (@temp_FSNO_New,@FSTYPE,@FSNO_DATE,@FSNOTE,@FSCREATED_BY,GETDATE())          
          END
     ELSE
          BEGIN  
                UPDATE MAM.dbo.TBNORECORD    /*不然就更新資料庫，最後一筆資料*/ 
                SET FSNO=@temp_FSNO_New,FSTYPE=@FSTYPE,FSNO_DATE=@FSNO_DATE,FSNOTE=@FSNOTE,FSCREATED_BY=@FSCREATED_BY,FDCREATED_DATE=GETDATE()
                WHERE FSNO=@temp_FSNO_LsstDay  AND MAM.dbo.TBNORECORD.FSTYPE = @FSTYPE                          
          END     
     SELECT @temp_FSNO_New As FSNO 
   END
   
ELSE

   BEGIN
     
     /*取號原則：當天若是已取號，數值就加1，並且將資料更新*/
     SET @temp_FSNO_New =  CONVERT ( varchar(12),CONVERT(bigint, @temp_FSNO_NOW)+1)    
     
     UPDATE MAM.dbo.TBNORECORD  
     SET FSNO=@temp_FSNO_New,FSTYPE=@FSTYPE,FSNO_DATE=@FSNO_DATE,FSNOTE=@FSNOTE,FSCREATED_BY=@FSCREATED_BY,FDCREATED_DATE=GETDATE()
     WHERE FSNO=@temp_FSNO_NOW AND MAM.dbo.TBNORECORD.FSTYPE = @FSTYPE
     SELECT @temp_FSNO_New As FSNO    
      
   END




GO
