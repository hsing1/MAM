USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      
-- Create date: 
-- Description: 對應 Anystream 進行 Pruge 中所使用的預存，會將相關的 ansid 做搬移、轉換
-- Update desc: 
-- =============================================
CREATE PROCEDURE [dbo].[SP_D_TBTRANSCODE_ANSID]

AS

BEGIN

	DECLARE @PURGE_DATE char(10)
	SET @PURGE_DATE = CONVERT(char(10), GETDATE(), 111)

	BEGIN TRANSACTION;

	BEGIN TRY

		-- 備份 TBTRANSCODE 重要欄位資料到 TBTRANSCODE_ANS_BAK
		INSERT INTO TBTRANSCODE_ANS_BAK
			SELECT FNTRANSCODE_ID, FNANYSTREAM_ID, FNPROCESS_STATUS, '', GETDATE() FROM TBTRANSCODE WHERE FNANYSTREAM_ID > 0

		-- 將 TBTRANSCODE_RECV_LOG 的訊息全數搬至 TBTRANSCODE_ANS_RECV_BAK
		INSERT INTO TBTRANSCODE_ANS_RECV_BAK
			SELECT FDTIME, FNJOB_ID, FSCONTENT, @PURGE_DATE FROM TBTRANSCODE_RECV_LOG

		-- PRUGE DATA
		UPDATE TBTRANSCODE SET FNANYSTREAM_ID = -10 WHERE FNANYSTREAM_ID > 0
		DELETE TBTRANSCODE_RECV_LOG

		-- RETURN RESULT
		SELECT 'SUCCESS'

	END TRY
	BEGIN CATCH

		-- RETURN RESULT
		SELECT 'ERROR'

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

	END CATCH;

	IF @@TRANCOUNT > 0
		COMMIT TRANSACTION;

END

GO
