USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        
-- Create date: 
-- Description: 紀錄派送檔案到主控時間
-- Update desc:    <2013/10/25><Mike>
-- =============================================
create Proc [dbo].[SP_I_PGM_FILE_FTP_DATE]
@FSVIDEO_ID varchar(10)

As
if ((select count(FSVIDEO_ID)
from 
TBPGM_FILE_FTP_DATE 
where FSVIDEO_ID=@FSVIDEO_ID)<>1) 
begin
insert into TBPGM_FILE_FTP_DATE(FSVIDEO_ID,FDFTP_DATE) values(@FSVIDEO_ID,getdate())
end
else
begin
update TBPGM_FILE_FTP_DATE set FDFTP_DATE=getdate() where FSVIDEO_ID=@FSVIDEO_ID
end


GO
