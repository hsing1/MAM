USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RPT_TBPGM_ARR_PROMO_01](
	[QUERY_KEY] [uniqueidentifier] NOT NULL,
	[QUERY_KEY_SUBID] [uniqueidentifier] NOT NULL,
	[QUERY_BY] [varchar](50) NOT NULL,
	[QUERY_DATE] [datetime] NOT NULL,
	[頻道] [varchar](50) NOT NULL,
	[播出日期] [date] NOT NULL,
	[序號] [varchar](8) NOT NULL,
	[節目編碼] [varchar](7) NOT NULL,
	[時間類型] [varchar](1) NOT NULL,
	[名稱] [varchar](50) NOT NULL,
	[節目集數] [int] NOT NULL,
	[段數] [int] NOT NULL,
	[支數] [int] NOT NULL,
	[主控編號] [varchar](8) NOT NULL,
	[播出時間] [nvarchar](11) NOT NULL,
	[長度] [nvarchar](11) NOT NULL,
	[播映序號] [int] NOT NULL,
	[短帶編碼] [varchar](11) NOT NULL,
	[影像編號] [varchar](16) NOT NULL,
	[資料類型] [varchar](1) NOT NULL,
	[備註] [varchar](200) NOT NULL,
	[FSSIGNAL] [varchar](10) NULL,
	[FSRAN] [varchar](1) NULL,
	[FNLIVE] [varchar](1) NULL,
 CONSTRAINT [PK_RPT_TBPGM_ARR_PROMO_01] PRIMARY KEY CLUSTERED 
(
	[QUERY_KEY] ASC,
	[QUERY_KEY_SUBID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'報表編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_01', @level2type=N'COLUMN',@level2name=N'QUERY_KEY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'資料編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_01', @level2type=N'COLUMN',@level2name=N'QUERY_KEY_SUBID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_01', @level2type=N'COLUMN',@level2name=N'QUERY_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_01', @level2type=N'COLUMN',@level2name=N'QUERY_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'頻道' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_01', @level2type=N'COLUMN',@level2name=N'頻道'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'播出日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_01', @level2type=N'COLUMN',@level2name=N'播出日期'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'序號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_01', @level2type=N'COLUMN',@level2name=N'序號'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目編碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_01', @level2type=N'COLUMN',@level2name=N'節目編碼'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'時間類型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_01', @level2type=N'COLUMN',@level2name=N'時間類型'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_01', @level2type=N'COLUMN',@level2name=N'名稱'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目集數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_01', @level2type=N'COLUMN',@level2name=N'節目集數'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'段數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_01', @level2type=N'COLUMN',@level2name=N'段數'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_01', @level2type=N'COLUMN',@level2name=N'支數'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主控編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_01', @level2type=N'COLUMN',@level2name=N'主控編號'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'播出時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_01', @level2type=N'COLUMN',@level2name=N'播出時間'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'長度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_01', @level2type=N'COLUMN',@level2name=N'長度'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'播映序號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_01', @level2type=N'COLUMN',@level2name=N'播映序號'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'短帶編碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_01', @level2type=N'COLUMN',@level2name=N'短帶編碼'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'影像編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_01', @level2type=N'COLUMN',@level2name=N'影像編號'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'資料類型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_01', @level2type=N'COLUMN',@level2name=N'資料類型'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'備註' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_01', @level2type=N'COLUMN',@level2name=N'備註'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'訊號源' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_01', @level2type=N'COLUMN',@level2name=N'FSSIGNAL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否為Live,1為Live' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_01', @level2type=N'COLUMN',@level2name=N'FNLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'描述', @value=N'播出運行表報表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_ARR_PROMO_01'
GO
