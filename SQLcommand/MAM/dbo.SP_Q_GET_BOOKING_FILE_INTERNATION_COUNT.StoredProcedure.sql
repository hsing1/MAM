USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <David.Sin>
-- Create date:   <2010/10/02>
-- Description:   查詢調用檔案為宏觀的數量
-- =============================================

CREATE Proc [dbo].[SP_Q_GET_BOOKING_FILE_INTERNATION_COUNT]
@FSUSER_ID varchar(50)
AS

Select COUNT(*) AS FNFILE_DEPT_COUNT 
From TBPREBOOKING A JOIN TBLOG_VIDEO B ON A.FSFILE_NO=B.FSFILE_NO
		JOIN TBPROG_M C ON B.FSID=C.FSPROG_ID
Where A.FSUSER_ID=@FSUSER_ID And C.FNDEP_ID in (57,58,59)
GO
