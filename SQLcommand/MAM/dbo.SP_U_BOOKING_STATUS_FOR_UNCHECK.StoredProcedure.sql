USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <David.Sin>
-- Create date:   <2010/10/02>
-- Description:   更新調用清單為抽單
-- =============================================

CREATE PROC [dbo].[SP_U_BOOKING_STATUS_FOR_UNCHECK]
@FSBOOKING_NO varchar(12)
AS

UPDATE TBBOOKING_MASTER SET FCCHECK_STATUS='X' WHERE FSBOOKING_NO=@FSBOOKING_NO

UPDATE TBBOOKING_DETAIL SET FCCHECK='X' WHERE FSBOOKING_NO=@FSBOOKING_NO
GO
