USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/04/01>
-- Description:	For RPT_TBPGM_SAVE_TAPE_LIST_01(救命帶清單), 報表呼叫用
-- =============================================
Create PROCEDURE [dbo].[SP_R_TBPGM_SAVE_TAPE_LIST_01_QRY]
	@QUERY_KEY	VARCHAR(36)
AS
BEGIN
	SELECT
		[QUERY_KEY]
      ,[QUERY_KEY_SUBID]
      ,[QUERY_BY]
      ,[QUERY_DATE]
      ,[播出日期]
      ,[頻道]
      ,[來源節目名稱]
      ,[來源節目集數]
      ,[起始時間]
      ,[結束時間]
      ,[序號]
      ,[替換節目名稱]
      ,[替換節目集數]
      ,[替換節目檔案編號]
      ,[替換節目檔案長度]
	FROM
		RPT_TBPGM_SAVE_TAPE_LIST_01
	WHERE
		(QUERY_KEY = @QUERY_KEY)
	ORDER BY
		[播出日期], [起始時間], [結束時間], [序號]
END






GO
