USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <David.Sin>
-- Create date:   <2010/10/02>
-- Description:   更新調用明細備註
-- =============================================

CREATE PROC [dbo].[SP_U_TBBOOKING_DETAIL_MEMO]
@FSBOOKING_NO varchar(12),
@FSSEQ varchar(3),
@FSMEMO nvarchar(max)
AS

UPDATE TBBOOKING_DETAIL
SET FSMEMO=@FSMEMO
WHERE FSBOOKING_NO=@FSBOOKING_NO AND FSSEQ=@FSSEQ
GO
