USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_D_MSSPROCESS_UNREG_WORKER]

@FSWORKER_NAME VARCHAR(50)

AS

-- 刪除該台電腦的註冊資訊
DELETE FROM TBPGM_MSSPROCESS_WORKER WHERE FSWORKER_NAME = @FSWORKER_NAME

GO
