USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBARCHIVE_EXCHANGE](
	[FSARC_ID] [char](12) NOT NULL,
	[FSTYPE] [char](1) NOT NULL,
	[FSID] [varchar](11) NOT NULL,
	[FSLESSCLAM] [nvarchar](500) NULL,
	[FSSUPERVISOR] [char](1) NULL,
	[FSCHANNEL_ID] [char](10) NULL,
	[FNEPISODE] [smallint] NOT NULL,
	[FDARC_DATE] [datetime] NULL,
	[FSARC_BY] [varchar](50) NULL,
	[FCCHECK_STATUS] [char](1) NOT NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
	[FSUPDATED_BY] [varchar](50) NULL,
	[FDUPDATED_DATE] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'置換單ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBARCHIVE_EXCHANGE', @level2type=N'COLUMN',@level2name=N'FSARC_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'代碼 G-節目 P-PROMO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBARCHIVE_EXCHANGE', @level2type=N'COLUMN',@level2name=N'FSTYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBARCHIVE_EXCHANGE', @level2type=N'COLUMN',@level2name=N'FSID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'缺檔描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBARCHIVE_EXCHANGE', @level2type=N'COLUMN',@level2name=N'FSLESSCLAM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'調用是否經過主管簽和' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBARCHIVE_EXCHANGE', @level2type=N'COLUMN',@level2name=N'FSSUPERVISOR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'頻道別' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBARCHIVE_EXCHANGE', @level2type=N'COLUMN',@level2name=N'FSCHANNEL_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'子集編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBARCHIVE_EXCHANGE', @level2type=N'COLUMN',@level2name=N'FNEPISODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'入庫置換日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBARCHIVE_EXCHANGE', @level2type=N'COLUMN',@level2name=N'FDARC_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'置換者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBARCHIVE_EXCHANGE', @level2type=N'COLUMN',@level2name=N'FSARC_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'表單狀態' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBARCHIVE_EXCHANGE', @level2type=N'COLUMN',@level2name=N'FCCHECK_STATUS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'由誰起單' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBARCHIVE_EXCHANGE', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'起單日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBARCHIVE_EXCHANGE', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBARCHIVE_EXCHANGE', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBARCHIVE_EXCHANGE', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
