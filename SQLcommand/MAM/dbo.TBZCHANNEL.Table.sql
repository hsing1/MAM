USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBZCHANNEL](
	[FSCHANNEL_ID] [char](2) NOT NULL,
	[FSCHANNEL_NAME] [nvarchar](50) NOT NULL,
	[FSSHOWNAME] [nvarchar](10) NULL,
	[FSCHANNEL_TYPE] [char](3) NULL,
	[FSSORT] [char](10) NULL,
	[FSSHORTNAME] [varchar](10) NULL,
	[FSOLDTABLENAME] [varchar](20) NULL,
	[FSTSM_POOLNAME] [varchar](30) NOT NULL,
	[FNDIR_ID] [bigint] NOT NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
	[FSUPDATED_BY] [varchar](50) NULL,
	[FDUPDATED_DATE] [datetime] NULL,
	[FBISENABLE] [bit] NULL,
	[FCDATA] [char](1) NULL,
 CONSTRAINT [PK_TBZCHANNEL] PRIMARY KEY CLUSTERED 
(
	[FSCHANNEL_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[TBZCHANNEL] ADD  CONSTRAINT [DF_TBZCHANNEL_FNDIR_ID]  DEFAULT ((1)) FOR [FNDIR_ID]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'頻道別代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZCHANNEL', @level2type=N'COLUMN',@level2name=N'FSCHANNEL_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'頻道別名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZCHANNEL', @level2type=N'COLUMN',@level2name=N'FSCHANNEL_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'顯示名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZCHANNEL', @level2type=N'COLUMN',@level2name=N'FSSHOWNAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'排序號碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZCHANNEL', @level2type=N'COLUMN',@level2name=N'FSSORT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'簡寫名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZCHANNEL', @level2type=N'COLUMN',@level2name=N'FSSHORTNAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'原有資料庫名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZCHANNEL', @level2type=N'COLUMN',@level2name=N'FSOLDTABLENAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZCHANNEL', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZCHANNEL', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZCHANNEL', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZCHANNEL', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
