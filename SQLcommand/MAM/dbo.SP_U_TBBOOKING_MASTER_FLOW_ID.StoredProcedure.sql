USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

-- Author:        <David.Sin>

-- Create date:   <2014/09/04>

-- Description:   更新調用單的流程編號

-- =============================================

CREATE Proc [dbo].[SP_U_TBBOOKING_MASTER_FLOW_ID]
@FSBOOKING_NO varchar(12),
@FNFLOW_ID INT
AS

UPDATE
	[dbo].[TBBOOKING_MASTER]
SET
	[FNFLOW_ID] = @FNFLOW_ID
WHERE
	[FSBOOKING_NO] = @FSBOOKING_NO




GO
