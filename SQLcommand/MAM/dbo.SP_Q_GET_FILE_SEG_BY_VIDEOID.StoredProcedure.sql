USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- =============================================

-- Author:        <Mike.Lin>

-- Create date:   <2016/02/24>

-- Description:   以VIDEOID查詢檔案的段落資料

-- 寫入HD頻道的主控Louth DB時使用

-- =============================================

Create Proc [dbo].[SP_Q_GET_FILE_SEG_BY_VIDEOID]
@FSVIDEO_ID	varchar(8)
As
IF (select FSTYPE from tblog_video where FSARC_TYPE='002' 
and FCFILE_STATUS not in ('D','F','X')
and FSVIDEO_PROG=@FSVIDEO_ID)='G'
select A.FSFILE_NO,A.FSVIDEO_PROG,A.FSTYPE,B.FNSEG_ID,A.FNEPISODE,B.FSBEG_TIMECODE,B.FSEND_TIMECODE,C.FSPGMNAME FSNAME
from tblog_video A
left join TBLOG_VIDEO_SEG B on A.FSFILE_NO=B.FSFILE_NO
left join TBPROG_M C on A.FSID=C.FSPROG_ID
where 
A.FSARC_TYPE='002' 
and A.FCFILE_STATUS not in ('D','F','X')
and A.FSVIDEO_PROG=@FSVIDEO_ID
order by B.FNSEG_ID
else
begin
select A.FSFILE_NO,A.FSVIDEO_PROG,A.FSTYPE,B.FNSEG_ID,A.FNEPISODE,B.FSBEG_TIMECODE,B.FSEND_TIMECODE,C.FSPROMO_NAME FSNAME
from tblog_video A
left join TBLOG_VIDEO_SEG B on A.FSFILE_NO=B.FSFILE_NO
left join TBPGM_PROMO C on A.FSID=C.FSPROMO_ID
where 
A.FSARC_TYPE='002' 
and A.FCFILE_STATUS not in ('D','F','X')
and A.FSVIDEO_PROG=@FSVIDEO_ID
order by B.FNSEG_ID
end
GO
