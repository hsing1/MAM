USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   取得預約破口資源最大編號
-- =============================================
CREATE Proc [dbo].[SP_Q_TBPGM_PROMO_PLAN_MAX_FNNO]

As
select MAX(FNNO)+1 MAXFNNO from TBPGM_PROMO_PLAN




GO
