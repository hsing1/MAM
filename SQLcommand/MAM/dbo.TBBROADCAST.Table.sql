USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBBROADCAST](
	[FSBRO_ID] [char](12) NOT NULL,
	[FSBRO_TYPE] [char](1) NOT NULL,
	[FSID] [varchar](11) NOT NULL,
	[FNEPISODE] [smallint] NULL,
	[FDBRO_DATE] [datetime] NULL,
	[FSBRO_BY] [varchar](50) NULL,
	[FSMEMO] [nvarchar](max) NULL,
	[FCCHECK_STATUS] [char](1) NULL,
	[FSCHECK_BY] [varchar](50) NULL,
	[FCCHANGE] [char](1) NULL,
	[FSPRINTID] [char](36) NULL,
	[FSREJECT] [varchar](200) NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
	[FSUPDATED_BY] [varchar](50) NULL,
	[FDUPDATED_DATE] [datetime] NULL,
	[FSSIGNED_BY] [nvarchar](50) NULL,
	[FSSIGNED_BY_NAME] [nvarchar](50) NULL,
	[FDSIGNED_DATE] [datetime] NULL,
	[FSTRAN_BY] [nvarchar](50) NULL,
	[FDTRAN_DATE] [datetime] NULL,
 CONSTRAINT [PK_TBBRO] PRIMARY KEY CLUSTERED 
(
	[FSBRO_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_TBBROADCAST_CREATEDDATE] ON [dbo].[TBBROADCAST]
(
	[FDCREATED_DATE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
CREATE NONCLUSTERED INDEX [IX_TBBROADCAST_PROGID_EPS] ON [dbo].[TBBROADCAST]
(
	[FSID] ASC,
	[FNEPISODE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TBBROADCAST] ADD  CONSTRAINT [DF_TBBROADCAST_FSMEMO]  DEFAULT ('') FOR [FSMEMO]
GO
ALTER TABLE [dbo].[TBBROADCAST] ADD  CONSTRAINT [DF_TBBROADCAST_FSSIGNED_BY]  DEFAULT ('') FOR [FSSIGNED_BY]
GO
ALTER TABLE [dbo].[TBBROADCAST] ADD  CONSTRAINT [DF_TBBROADCAST_FSSIGNED_BY_NAME]  DEFAULT ('') FOR [FSSIGNED_BY_NAME]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送帶轉檔單號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBROADCAST', @level2type=N'COLUMN',@level2name=N'FSBRO_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'類型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBROADCAST', @level2type=N'COLUMN',@level2name=N'FSBRO_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目或短帶編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBROADCAST', @level2type=N'COLUMN',@level2name=N'FSID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'集別' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBROADCAST', @level2type=N'COLUMN',@level2name=N'FNEPISODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送帶轉檔申請日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBROADCAST', @level2type=N'COLUMN',@level2name=N'FDBRO_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'送帶轉檔申請者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBROADCAST', @level2type=N'COLUMN',@level2name=N'FSBRO_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'備註欄位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBROADCAST', @level2type=N'COLUMN',@level2name=N'FSMEMO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'審核狀態' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBROADCAST', @level2type=N'COLUMN',@level2name=N'FCCHECK_STATUS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'審核者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBROADCAST', @level2type=N'COLUMN',@level2name=N'FSCHECK_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'列印編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBROADCAST', @level2type=N'COLUMN',@level2name=N'FSPRINTID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBROADCAST', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBROADCAST', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBROADCAST', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBROADCAST', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'片庫簽核者的ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBROADCAST', @level2type=N'COLUMN',@level2name=N'FSSIGNED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'片庫簽核者的中文姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBROADCAST', @level2type=N'COLUMN',@level2name=N'FSSIGNED_BY_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'片庫簽核者的簽核日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBBROADCAST', @level2type=N'COLUMN',@level2name=N'FDSIGNED_DATE'
GO
