USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   查詢建議刪除清單報表
-- =============================================
CREATE PROCEDURE [dbo].[SP_R_RPT_PROPOSE_DELETE_LIST_01]
	@QUERY_KEY	VARCHAR(36)
AS
BEGIN
	SELECT [QUERY_KEY]
		  ,[QUERY_KEY_SUBID]
		  ,[QUERY_BY]
		  ,[QUERY_DATE]
      ,[FSVIDEOID]
      ,[FSNAME]
      ,[FNEPISODE]
      ,[FNSEG_ID]
	FROM
		[MAM].[dbo].[RPT_PROPOSE_DELETE_LIST_01]
	WHERE
		(QUERY_KEY = @QUERY_KEY)
order by len(FSVIDEOID),FSVIDEOID

END
GO
