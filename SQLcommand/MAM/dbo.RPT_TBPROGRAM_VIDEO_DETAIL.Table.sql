USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RPT_TBPROGRAM_VIDEO_DETAIL](
	[FSGUID] [varchar](100) NOT NULL,
	[FNNO] [bigint] IDENTITY(1,1) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
	[FSCREATED_BY] [varchar](50) NULL,
	[檔案編號] [varchar](16) NULL,
	[節目名稱] [nvarchar](50) NULL,
	[集別] [int] NULL,
	[子集名稱] [nvarchar](50) NULL,
	[子集內容] [nvarchar](max) NULL,
	[TimeCode] [varchar](50) NULL,
	[檔案類別] [nvarchar](10) NULL,
	[附檔名] [varchar](10) NULL,
	[檔案類型] [nvarchar](30) NULL,
	[段落資訊] [nvarchar](max) NULL,
	[影片格式] [nvarchar](max) NULL,
	[Video格式] [nvarchar](max) NULL,
	[Audio格式] [nvarchar](max) NULL,
 CONSTRAINT [PK_TBRPT_PROGRAM_VIDEO_DETAIL] PRIMARY KEY CLUSTERED 
(
	[FSGUID] ASC,
	[FNNO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
