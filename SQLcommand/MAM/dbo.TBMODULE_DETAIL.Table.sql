USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBMODULE_DETAIL](
	[FSMODULE_ID] [char](7) NOT NULL,
	[FSFUNC_NAME] [nvarchar](50) NULL,
	[FSDESCRIPTION] [nvarchar](max) NULL,
	[FSHANDLER] [varchar](255) NULL,
 CONSTRAINT [PK_TBMODULES] PRIMARY KEY CLUSTERED 
(
	[FSMODULE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模組代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMODULE_DETAIL', @level2type=N'COLUMN',@level2name=N'FSMODULE_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模組功能名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMODULE_DETAIL', @level2type=N'COLUMN',@level2name=N'FSFUNC_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模組功能描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMODULE_DETAIL', @level2type=N'COLUMN',@level2name=N'FSDESCRIPTION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'UI Handler' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBMODULE_DETAIL', @level2type=N'COLUMN',@level2name=N'FSHANDLER'
GO
