USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_Q_TBZPROGOBJ_BYID]
@FSPROGOBJID CHAR (2)

As
SELECT *FROM MAM.dbo.TBZPROGOBJ
WHERE MAM.dbo.TBZPROGOBJ.FSPROGOBJID = @FSPROGOBJID
GO
