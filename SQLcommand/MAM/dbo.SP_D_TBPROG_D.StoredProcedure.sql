USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        
-- Create date: 
-- Description:   刪除MAM的節目子集
-- Update desc:    <2013/04/17><Jarvis.Yang>加入註解。
-- Update desc:    <2014/09/12><Jarvis.Yang>取消更新FSUPDUSER。
-- =============================================
CREATE Proc [dbo].[SP_D_TBPROG_D]
@FSPROG_ID CHAR(7),
@FNEPISODE smallint ,
@FSDEL char(1) ,
@FSDELUSER varchar(50) 

As
UPDATE MAM.dbo.TBPROG_D
SET FSDEL=@FSDEL,FSDELUSER=@FSDELUSER,FDUPDDATE=GETDATE()
WHERE  MAM.dbo.TBPROG_D.FSPROG_ID=@FSPROG_ID AND MAM.dbo.TBPROG_D.FNEPISODE=@FNEPISODE
--UPDATE MAM.dbo.TBPROG_D
--SET FSDEL=@FSDEL,FSDELUSER=@FSDELUSER,FSUPDUSER=@FSDELUSER,FDUPDDATE=GETDATE()
--WHERE  MAM.dbo.TBPROG_D.FSPROG_ID=@FSPROG_ID AND MAM.dbo.TBPROG_D.FNEPISODE=@FNEPISODE
GO
