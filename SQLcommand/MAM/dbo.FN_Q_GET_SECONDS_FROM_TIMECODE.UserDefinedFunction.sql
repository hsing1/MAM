USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Sam.Wu>
-- Create date: <2012/02/08>
-- Description:	<根據傳入的段落起迄條件,傳回段落秒數>
-- =============================================
CREATE FUNCTION [dbo].[FN_Q_GET_SECONDS_FROM_TIMECODE]
(
	@BEG_TIMECODE	CHAR(11),
	@END_TIMECODE	CHAR(11)
)
RETURNS	int --RETURNS	int
AS
BEGIN
 DECLARE @BEG_HH int		--開始段落的時
 DECLARE @BEG_MM int		--開始段落的分
 DECLARE @BEG_SS int		--開始段落的秒
 DECLARE @END_HH int 		--節束段落的時
 DECLARE @END_MM int  		--節束段落的分
 DECLARE @END_SS int  		--節束段落的秒 
 DECLARE @BEG_TOTAL int		--開始段落的總秒數 
 DECLARE @END_TOTAL int  	--節束段落的總秒數 
   
 SET @BEG_HH = CONVERT(INT,SUBSTRING(@BEG_TIMECODE,0,3))
 SET @BEG_MM = CONVERT(INT,SUBSTRING(@BEG_TIMECODE,4,2))
 SET @BEG_SS = CONVERT(INT,SUBSTRING(@BEG_TIMECODE,7,2))
 SET @END_HH = CONVERT(INT,SUBSTRING(@END_TIMECODE,0,3))
 SET @END_MM = CONVERT(INT,SUBSTRING(@END_TIMECODE,4,2)) 
 SET @END_SS = CONVERT(INT,SUBSTRING(@END_TIMECODE,7,2)) 
  
 SET @BEG_TOTAL =  @BEG_HH * 3600 + @BEG_MM * 60 + @BEG_SS
 SET @END_TOTAL =  @END_HH * 3600 + @END_MM * 60 + @END_SS	
	RETURN @END_TOTAL - @BEG_TOTAL
	
	--RETURN CONVERT(INT,SUBSTRING(@BEG_TIMECODE,7,2))
	
	
END


GO
