USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBZSHOOTSPEC](
	[FSSHOOTSPECID] [char](2) NOT NULL,
	[FSSHOOTSPECNAME] [nvarchar](50) NOT NULL,
	[FSSORT] [char](2) NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
	[FSUPDATED_BY] [varchar](50) NULL,
	[FDUPDATED_DATE] [datetime] NULL,
	[FBISENABLE] [bit] NULL,
 CONSTRAINT [PK_TBZSHOOTSPEC] PRIMARY KEY CLUSTERED 
(
	[FSSHOOTSPECID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[TBZSHOOTSPEC] ADD  CONSTRAINT [DF_TBZSHOOTSPEC_FBISFUNCTION]  DEFAULT ((1)) FOR [FBISENABLE]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'拍攝規格代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZSHOOTSPEC', @level2type=N'COLUMN',@level2name=N'FSSHOOTSPECID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'拍攝規格名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZSHOOTSPEC', @level2type=N'COLUMN',@level2name=N'FSSHOOTSPECNAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'排序號碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZSHOOTSPEC', @level2type=N'COLUMN',@level2name=N'FSSORT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZSHOOTSPEC', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZSHOOTSPEC', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZSHOOTSPEC', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZSHOOTSPEC', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
