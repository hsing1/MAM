USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_I_TBNORECORD_PROG_ID]
@FSNO_DATE CHAR(8) ,
@FSNOTE NVARCHAR(50) ,
@FSCREATED_BY VARCHAR(50) 

As

Declare @temp_FSNO_NOW VARCHAR(12)=''         /*目前資料庫的編號*/  
Declare @temp_FSNO_New VARCHAR(12)=''         /*即將要寫入資料庫的編號(加工過後)*/
Declare @temp_FSNO_LsstDay VARCHAR(12)=''     /*當資料庫最後一筆與取號時的日期不同(資料庫存在最後一筆的編號)*/

Declare @temp_Now_Record CHAR(3)=''           /*目前資料庫取得的編號(三碼流水號)*/
Declare @temp_Now_Record_YEAR CHAR(4)=''      /*目前資料庫取得的編號(四碼年份)*/

Declare @temp_Now_YEAR CHAR(4)=''      /*資料庫存在最後一筆的年度*/

SELECT @temp_FSNO_NOW = ISNULL(MAX(FSNO),'') FROM MAM.dbo.TBNORECORD_PROGID  /*用今天的日期去取號*/ 
WHERE MAM.dbo.TBNORECORD_PROGID.FSNO_DATE = @FSNO_DATE  

IF @temp_FSNO_NOW = ''

 BEGIN 
	 SELECT @temp_FSNO_LsstDay = ISNULL(MAX(FSNO),''),@temp_Now_YEAR =ISNULL(MAX(FSNO_DATE),'')  FROM MAM.dbo.TBNORECORD_PROGID /*若是沒有表示今年尚未取過號，就要找出該類別是否曾經取過號*/ 
          
     SET  @temp_Now_Record = substring(@temp_FSNO_LsstDay,5,3)
     SET  @temp_Now_Record_YEAR = substring(@temp_FSNO_LsstDay,1,4)
          
     SET  @temp_FSNO_New = RTRIM(@FSNO_DATE) + '001'    /*取號原則：當年若是未取號，就用傳入的年度+0001*/      


      IF @temp_FSNO_LsstDay = ''   /*若是該類別未曾取過號，資料庫即新增*/ 
          BEGIN
                INSERT INTO MAM.dbo.TBNORECORD_PROGID(FSNO,FSNO_DATE,FSNOTE,FSCREATED_BY,FDCREATED_DATE)
                VALUES (@temp_FSNO_New,@FSNO_DATE,@FSNOTE,@FSCREATED_BY,GETDATE())          
          END
     ELSE
          BEGIN  			
			/*丟進來的年大於取得的年，號碼要重新歸零*/
				IF RTRIM(@FSNO_DATE) > @temp_Now_YEAR
					
					/*還要再判斷丟進來的年度是否大於目前用到的號碼年度*/
					  IF RTRIM(@FSNO_DATE) > @temp_Now_Record_YEAR
						BEGIN /*若是大於就歸零*/
							INSERT INTO MAM.dbo.TBNORECORD_PROGID(FSNO,FSNO_DATE,FSNOTE,FSCREATED_BY,FDCREATED_DATE)
                            VALUES (@temp_FSNO_New,@FSNO_DATE,@FSNOTE,@FSCREATED_BY,GETDATE()) 
						END						
					ELSE		
						BEGIN /*不然就繼續用*/
						SET @temp_FSNO_New = CONVERT ( varchar(12),CONVERT(bigint, @temp_FSNO_LsstDay)+1)
							INSERT INTO MAM.dbo.TBNORECORD_PROGID(FSNO,FSNO_DATE,FSNOTE,FSCREATED_BY,FDCREATED_DATE)
                            VALUES (@temp_FSNO_New,@FSNO_DATE,@FSNOTE,@FSCREATED_BY,GETDATE()) 							
						END		
				
				ELSE/*丟進來的年小於取得的年，號碼不需重新歸零*/
					BEGIN
						SET @temp_FSNO_New = CONVERT ( varchar(12),CONVERT(bigint, @temp_FSNO_LsstDay)+1)
						INSERT INTO MAM.dbo.TBNORECORD_PROGID(FSNO,FSNO_DATE,FSNOTE,FSCREATED_BY,FDCREATED_DATE)
                        VALUES (@temp_FSNO_New,@FSNO_DATE,@FSNOTE,@FSCREATED_BY,GETDATE()) 
					END
		   END     
     SELECT @temp_FSNO_New As FSNO 
   END 
   
ELSE   

 BEGIN     
      
      SET  @temp_Now_Record = substring(@temp_FSNO_NOW,5,3)
      SET  @temp_Now_Record_YEAR = substring(@temp_FSNO_NOW,1,4)
      
       /*檢查取得的編號是否已到最大號999*/   
       IF @temp_Now_Record='999'
          BEGIN
            /*若是已到最大號，要將目前的年度取出+1，再將流水後001塞入*/ 
            SET @temp_FSNO_New =  CONVERT ( varchar(4),CONVERT(bigint, @temp_Now_Record_YEAR)+1) +'001' 
			INSERT INTO MAM.dbo.TBNORECORD_PROGID(FSNO,FSNO_DATE,FSNOTE,FSCREATED_BY,FDCREATED_DATE)
            VALUES (@temp_FSNO_New,@FSNO_DATE,@FSNOTE,@FSCREATED_BY,GETDATE())
            
			SELECT @temp_FSNO_New As FSNO
          END
          
       ELSE
          BEGIN          
			SET @temp_FSNO_New =  CONVERT ( varchar(12),CONVERT(bigint, @temp_FSNO_NOW)+1)
			INSERT INTO MAM.dbo.TBNORECORD_PROGID(FSNO,FSNO_DATE,FSNOTE,FSCREATED_BY,FDCREATED_DATE)
            VALUES (@temp_FSNO_New,@FSNO_DATE,@FSNOTE,@FSCREATED_BY,GETDATE())
			SELECT @temp_FSNO_New As FSNO	
		  END	
END  

GO
