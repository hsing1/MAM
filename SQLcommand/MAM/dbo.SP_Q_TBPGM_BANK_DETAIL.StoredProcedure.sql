USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

-- Author:        <Mike.Lin>

-- Create date:   <2013/05/10>

-- Description:   查詢節目播映資料,加入欄位預設秒數與訊號源

-- =============================================

CREATE Proc [dbo].[SP_Q_TBPGM_BANK_DETAIL]

@FSPROG_ID	char(7),
@FNSEQNO	int,
@FSPROG_NAME	nvarchar(50)
As
select A.FSPROG_ID,A.FNSEQNO,A.FSPROG_NAME,A.FSPROG_NAME_ENG,
A.FDBEG_DATE,A.FDEND_DATE,A.FSBEG_TIME,A.FSEND_TIME,A.FSCHANNEL_ID,A.FNREPLAY,
A.FSWEEK,A.FSPLAY_TYPE,A.FNSEG,A.FNDUR,A.FNSEC,A.FSSIGNAL,A.FSMEMO,A.FSMEMO1,A.FSCREATED_BY,A.FDCREATED_DATE,A.FSUPDATED_BY,A.FDUPDATED_DATE,B.FSCHANNEL_NAME
from TBPGM_BANK_DETAIL A,TBZCHANNEL B where 
A.FSCHANNEL_ID=B.FSCHANNEL_ID
and @FSPROG_ID = case when @FSPROG_ID = '' then @FSPROG_ID else FSPROG_ID end
and @FNSEQNO = case when @FNSEQNO = '' then @FNSEQNO else FNSEQNO end
and FSPROG_NAME like '%' + @FSPROG_NAME +'%'


GO
