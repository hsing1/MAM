USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/04/01>
-- Description:	新增或修改MAM端的Clip明細檔
-- =============================================
Create PROC [dbo].[SP_I_TBTAPE_TAPE]
	@TapeID			varchar(14),
	@BIB			varchar(7),
	@TCopyNum		varchar(20),
	@TLangAudio		varchar(20),
	@TCaption		varchar(20),
	@TIndex			varchar(20),
	@TVolume		varchar(60),
	@TLocation		varchar(10),
	@TLocationSub	varchar(10),
	@TFlow			varchar(10),
	@TType			varchar(20),
	@TPrice			varchar(10),
	@TMemo			varchar(255),
	@TReturnNote	varchar(255),
	@TTapeCode		varchar(4),
	@TDynix			int,
	@LibMark		bit,
	@LimitMark		bit,
	@LimitNews		bit,
	@DeadMark		bit,
	@OutMark		bit,
	@FormNo			varchar(12),
	@Creator		varchar(20),
	@CreateDT		datetime,
	@Updator		varchar(20),
	@UpdateDT		datetime,
	@TempMark		bit,
	@EXEC_GUID		varchar(36)
AS

IF EXISTS ( SELECT	TapeID
			FROM	TBTAPE_TAPE
			WHERE	(TapeID = @TapeID) )
	BEGIN
		UPDATE
			TBTAPE_TAPE
		SET
			TapeID			= @TapeID,
			BIB				= @BIB,
			TCopyNum		= @TCopyNum,
			TLangAudio		= @TLangAudio,
			TCaption		= @TCaption,
			TIndex			= @TIndex,
			TVolume			= @TVolume,
			TLocation		= @TLocation,
			TLocationSub	= @TLocationSub,
			TFlow			= @TFlow,
			TType			= @TType,
			TPrice			= @TPrice,
			TMemo			= @TMemo,
			TReturnNote		= @TReturnNote,
			TTapeCode		= @TTapeCode,
			TDynix			= @TDynix,
			LibMark			= @LibMark,
			LimitMark		= @LimitMark,
			LimitNews		= @LimitNews,
			DeadMark		= @DeadMark,
			OutMark			= @OutMark,
			FormNo			= @FormNo,
			Creator			= @Creator,
			CreateDT		= @CreateDT,
			Updator			= @Updator,
			UpdateDT		= @UpdateDT,
			TempMark		= @TempMark,
			EXEC_GUID		= @EXEC_GUID
		WHERE
			(TapeID = @TapeID)
	END
ELSE
	BEGIN
		INSERT
			TBTAPE_TAPE
			(TapeID, BIB, TCopyNum, TLangAudio, TCaption,
			TIndex, TVolume, TLocation, TLocationSub, TFlow,
			TType, TPrice, TMemo, TReturnNote, TTapeCode,
			TDynix, LibMark, LimitMark, LimitNews, DeadMark,
			OutMark, FormNo, Creator, CreateDT, Updator,
			UpdateDT, TempMark, EXEC_GUID) 		
		VALUES
			(@TapeID, @BIB, @TCopyNum, @TLangAudio, @TCaption,
			@TIndex, @TVolume, @TLocation, @TLocationSub, @TFlow,
			@TType, @TPrice, @TMemo, @TReturnNote, @TTapeCode,
			@TDynix, @LibMark, @LimitMark, @LimitNews, @DeadMark,
			@OutMark, @FormNo, @Creator, @CreateDT, @Updator,
			@UpdateDT, @TempMark, @EXEC_GUID) 				
	END


SELECT	COUNT(*)
FROM	TBTAPE_TAPE
WHERE	(TapeID = @TapeID) AND
		(EXEC_GUID	= @EXEC_GUID)
GO
