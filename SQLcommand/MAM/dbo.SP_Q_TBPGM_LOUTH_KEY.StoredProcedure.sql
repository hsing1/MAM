USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   查詢首頻道Louth Key資料
-- =============================================
CREATE Proc [dbo].[SP_Q_TBPGM_LOUTH_KEY]
@FSCHANNEL_ID	varchar(2)	
As
SELECT * FROM TBPGM_LOUTH_KEY where FSCHANNEL_ID=@FSCHANNEL_ID

GO
