USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RPT_PROPOSE_DELETE_LIST_01](
	[QUERY_KEY] [uniqueidentifier] NOT NULL,
	[QUERY_KEY_SUBID] [uniqueidentifier] NOT NULL,
	[QUERY_BY] [varchar](50) NOT NULL,
	[QUERY_DATE] [datetime] NOT NULL,
	[FSVIDEOID] [varchar](10) NOT NULL,
	[FSNAME] [nvarchar](50) NOT NULL,
	[FNEPISODE] [nvarchar](50) NOT NULL,
	[FNSEG_ID] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_RPT_PROPOSE_DELETE_LIST_01] PRIMARY KEY CLUSTERED 
(
	[QUERY_KEY] ASC,
	[QUERY_KEY_SUBID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'報表編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_PROPOSE_DELETE_LIST_01', @level2type=N'COLUMN',@level2name=N'QUERY_KEY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'資料編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_PROPOSE_DELETE_LIST_01', @level2type=N'COLUMN',@level2name=N'QUERY_KEY_SUBID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_PROPOSE_DELETE_LIST_01', @level2type=N'COLUMN',@level2name=N'QUERY_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_PROPOSE_DELETE_LIST_01', @level2type=N'COLUMN',@level2name=N'QUERY_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主控編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_PROPOSE_DELETE_LIST_01', @level2type=N'COLUMN',@level2name=N'FSVIDEOID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_PROPOSE_DELETE_LIST_01', @level2type=N'COLUMN',@level2name=N'FSNAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'集數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_PROPOSE_DELETE_LIST_01', @level2type=N'COLUMN',@level2name=N'FNEPISODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'段落' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_PROPOSE_DELETE_LIST_01', @level2type=N'COLUMN',@level2name=N'FNSEG_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'描述', @value=N'建議刪除清單報表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_PROPOSE_DELETE_LIST_01'
GO
