USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[FN_Q_GET_USERNAME_BY_USERID](@FSUSER_ID varchar(50)) 
RETURNS nvarchar(100)
AS
BEGIN
  DECLARE @FSUSER_NAME nvarchar(100)
  Set @FSUSER_NAME=''
  SELECT @FSUSER_NAME=FSUSER_ChtName FROM TBUSERS WHERE FSUSER_ID=@FSUSER_ID
  return @FSUSER_NAME 
End 
GO
