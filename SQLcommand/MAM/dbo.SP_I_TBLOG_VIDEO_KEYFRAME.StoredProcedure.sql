USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Dennis.Wen>
-- Create date: <2011/04/07>
-- Description:	有搬移成功的圖像檔新增TBLOG_VIDEO_KEYFRAME
-- =============================================
CREATE PROC [dbo].[SP_I_TBLOG_VIDEO_KEYFRAME]
	@FSSUBJECT_ID	char(12),
	@FSVIDEO_NO		varchar(16),
	@FSFILE_NO		varchar(25),
	@FSCREATED_BY	nvarchar(20),
	@FDCREATED_DATE datetime,
	@FSFILE_PATH	varchar(120),
	@FNFILE_SIZE	int,
	@FNDIR_ID		int,
	@FSSERVER_NAME	varchar(20),
	@FSSHARE_FOLDER varchar(100),
	@FSDESCRIPTION	nvarchar(max)
	
AS
	SET NOCOUNT ON; 
	
	DECLARE @RESULT NVARCHAR(26) = ''

	INSERT
		TBLOG_VIDEO_KEYFRAME
		(	FSSUBJECT_ID,
			FSVIDEO_NO,	
			FSFILE_NO,	
			FSCREATED_BY,
			FDCREATED_DATE,
			FSFILE_PATH,
			FNFILE_SIZE,
			FNDIR_ID,	
			FSSERVER_NAME,
			FSSHARE_FOLDER, 
			FSDESCRIPTION	)
	VALUES
		(	@FSSUBJECT_ID,
			@FSVIDEO_NO,	
			@FSFILE_NO,	
			@FSCREATED_BY,
			@FDCREATED_DATE,
			@FSFILE_PATH,
			@FNFILE_SIZE,
			@FNDIR_ID,	
			@FSSERVER_NAME,
			@FSSHARE_FOLDER, 
			@FSDESCRIPTION	)
			
	SET @RESULT = (SELECT FSFILE_NO FROM TBLOG_VIDEO_KEYFRAME WHERE (FSFILE_NO = @FSFILE_NO))
  		
	SELECT RESULT = @RESULT

GO
