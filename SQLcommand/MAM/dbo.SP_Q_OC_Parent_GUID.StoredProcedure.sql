USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_Q_OC_Parent_GUID]

@GUID nvarchar(20)

As

SELECT Parent_GUID
  FROM [PTSFlow].[dbo].[OC_Department]
  where Dept_GUID=@GUID
                  
GO
