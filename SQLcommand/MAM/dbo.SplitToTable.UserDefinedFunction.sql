USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[SplitToTable](@aString   varchar(8000),@pattern   varchar(10))
  returns   @temp   table([Sid] [int] IDENTITY (1,1) NOT NULL ,Myvalues   varchar(100))
  as    
  begin  
          declare @i int  
          set @aString=rtrim(ltrim(@aString))  
          set @i=charindex(@pattern,@aString)  
          while @i>=1  
          begin  
            insert @temp   values(left(@aString,@i-1))  
            set   @aString=substring(@aString,@i+1,len(@aString)-@i)  
            set   @i=charindex(@pattern,@aString)  
          end  
          if @aString<>''    
             insert @temp values(@aString)  
          return    
  end   
GO
