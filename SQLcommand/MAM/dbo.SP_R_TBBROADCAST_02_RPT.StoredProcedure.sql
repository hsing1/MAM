USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Dennis.Wen>
-- ALTER date: <2011/05/17>
-- Description:	For RPT_TBBROADCAST_02(送帶轉檔明細), 報表呼叫用
-- =============================================
CREATE PROCEDURE [dbo].[SP_R_TBBROADCAST_02_RPT] --'D201409000010001','admin','epjrxx34ybtvfx33mlwcn3ed'
	@FSFILE_NO		VARCHAR(16),
	@QUERY_BY		VARCHAR(50),	--查詢的使用者
	@FSSESSION_ID	VARCHAR(255)	--確認查詢權限的FSSESSION_ID
AS
BEGIN
	-------------------	以下, 宣告並賦予值給本次查詢的QUERY_KEY, 作為查詢篩選條件 -------------------<< 此段落可延用不需要修改 >>

	DECLARE @AVATIME_MIN SMALLINT = 5				--預設為五分鐘有效, 後續可以考慮改讀取設定檔
	DECLARE @QUERY_KEY UNIQUEIDENTIFIER = NEWID()	--請留意只有在SQL2008以上才可以在宣告時直接給值, by Dennis	
	DECLARE @TIME_NOW DATETIME = GETDATE()
	DECLARE @TIME_SESSION DATETIME = '1900/01/01'
	DECLARE @NODATA NVARCHAR(20) = 'Y'
	
	SELECT	@TIME_SESSION = ISNULL(FDLAST_ACCESS_TIME,'1900/01/01'),
			@NODATA	=  ISNULL(FSUSER_ID,'Y')		--當取不到資料時, @NODATA會為'Y'
	FROM TBUSER_SESSION
	WHERE FSSESSION_ID = @FSSESSION_ID
	
	IF(@NODATA = 'Y')
		BEGIN
			INSERT TBLOG_ERRORQRY(QRY_TYPE, QRY_ITEM, QRY_BY, QRY_DATE, NOTE)
			VALUES ('REPORT', 'RPT_TBBROADCAST_02', @QUERY_BY, GETDATE(), 'SESSION_NOTEXIST')
			--SELECT QUERY_KEY = 'ERROR:SESSION_NOTEXIST'
			SELECT
				檔案編號		= 'SESSION_NOTEXIST',
				送帶轉檔單編號	= '',
				節目_節目編號 = '', 
				節目_節目名稱	= '您的登入資訊不存在，請重新登入。',
				節目_集別		= '',
				節目_子集名稱	= '',
				節目_播出頻道	= '',
				節目_總集數		= '',
				節目_每集時長	= '', 
				送帶轉檔_標題		= '',
				送帶轉檔_檔案類型	= '',
				送帶轉檔_起迄		= '', 
				段落檔_序號			= '',
				段落檔_TimeCode起	= '',
				段落檔_TimeCode迄	= '', 
				吸煙卡				='',
				警示卡				='',
				制式版版本			='',
				客製版名稱          ='',
				客製版長度          ='',
				警示卡名稱			='',
				警示卡長度			= '',
				檔名總片頭			= '',
				檔名總片頭名稱		= '',
				檔名總片頭長度		= '',
				節目側標			= '',
				Live側標			= '',
				時間側標			= '',
				重播				= '',
				錄影轉播			= '',
				立體聲				= '',
				公視LOGO			= '',
				雙語1				= '',
				雙語2				= '',
				雙語3				= '',
				雙語4				= '',
				QUERY_BY = @QUERY_BY,
				列印人員 = @QUERY_BY
		END
	ELSE IF(@TIME_NOW > DATEADD(minute, @AVATIME_MIN, @TIME_SESSION))	--若現在時間(@TIME_NOW)已經超過了前一次動作時間(@TIME_SESSION)+允許時間(@AVATIME_MIN)
		BEGIN
		-------------------	以下, 檢查使用者的查詢權限有無過期, 過期的無效查詢紀錄於TBLOG_ERRORQRY, NOTE為'SESSION_TIMEOUT'
			INSERT TBLOG_ERRORQRY(QRY_TYPE, QRY_ITEM, QRY_BY, QRY_DATE, NOTE)
			VALUES ('REPORT', 'RPT_TBBROADCAST_02', @QUERY_BY, GETDATE(), 'SESSION_TIMEOUT')
			--SELECT QUERY_KEY = 'ERROR:SESSION_TIMEOUT'
			SELECT
				檔案編號		= 'SESSION_TIMEOUT',
				送帶轉檔單編號	= '', 
				節目_節目編號 = '',
				節目_節目名稱	= '您的登入資訊已過期，請重新登入。',
				節目_集別		= '',
				節目_子集名稱	= '',
				節目_播出頻道	= '',
				節目_總集數		= '',
				節目_每集時長	= '', 				
				送帶轉檔_標題		= '',
				送帶轉檔_檔案類型	= '',
				送帶轉檔_起迄		= '', 
				段落檔_序號			= '',
				段落檔_TimeCode起	= '',
				段落檔_TimeCode迄	= '', 
				吸煙卡				= '',
				警示卡				='',
				制式版版本			='',
				客製版名稱          ='',
				客製版長度          ='',
				警示卡名稱			='',
				警示卡長度			= '',
				檔名總片頭			= '',
				檔名總片頭名稱		= '',
				檔名總片頭長度		= '',
				節目側標			= '',
				Live側標			= '',
				時間側標			= '',
				重播				= '',
				錄影轉播			= '',
				立體聲				= '',
				公視LOGO			= '',
				雙語1				= '',
				雙語2				= '',
				雙語3				= '',
				雙語4				= '',
				
				QUERY_BY = @QUERY_BY,
				列印人員 = @QUERY_BY
		END
	ELSE
		BEGIN 
			SELECT
				檔案編號		= VDO.FSFILE_NO,
				送帶轉檔單編號	= BROD.FSBRO_ID,

				節目_節目編號 = ISNULL(PROG_M.FSPROG_ID,PROG_DATA.FSDATA_ID),
				節目_節目名稱	= ISNULL(PROG_M.FSPGMNAME,PROG_DATA.FSDATA_NAME),
				節目_集別		= ISNULL(BROD.FNEPISODE,''),
				節目_子集名稱	= (CASE WHEN  BROD.FNEPISODE = 0  THEN ISNULL(PROG_D.FSPGDNAME,'')  
																	ELSE  ISNULL(PROG_D.FSPGDNAME,'???') END ),
				節目_播出頻道	= ISNULL(dbo.FN_Q_TBBROADCAST_GETNAMELISTBYCODE(PROG_M.FSCHANNEL), PROG_M.FSCHANNEL),
				節目_總集數		= ISNULL(PROG_M.FNTOTEPISODE,-1),
				節目_每集時長	= ISNULL(PROG_M.FNLENGTH,''),
				
				送帶轉檔_標題		= VDO.FSTITLE,
				送帶轉檔_檔案類型	= FTP.FSTYPE_NAME,
				送帶轉檔_起迄		= VDO.FSBEG_TIMECODE + ' ~ ' + VDO.FSEND_TIMECODE,
				
				音軌設定=VDO.FSTRACK,
				
				段落檔_序號			= SEG.FNSEG_ID,
				VIDEO_ID= case when VDO.FSVIDEO_PROG='' or VDO.FSVIDEO_PROG is null then ''else SUBSTRING(VDO.FSVIDEO_PROG,3,LEN(VDO.FSVIDEO_PROG)-2) end,
				段落檔_TimeCode起	= SEG.FSBEG_TIMECODE,
				段落檔_TimeCode迄	= SEG.FSEND_TIMECODE,
				時長=case when SEG.FSBEG_TIMECODE is null then '' else dbo.CaculateDuration(SEG.FSBEG_TIMECODE,SEG.FSEND_TIMECODE)end,
				
				吸煙卡				= (CASE WHEN ITAPE.FCSMOKE ='Y' THEN '要' ELSE '不要' END),
				警示卡				= (CASE WHEN ITAPE.FCALERTS ='Y' THEN '要' ELSE '不要' END),
				制式版版本			= (CASE WHEN ITAPE.FSINSERT_TAPE <>'7' THEN  (case when ITAPE.FSINSERT_TAPE !='' then '制式版'+ITAPE.FSINSERT_TAPE else '' end)  ELSE case when ITAPE.FSINSERT_TAPE is null then '' else '客製版' end END), --(CASE WHEN ITAPE.FSINSERT_TAPE is not null THEN (CASE WHEN ITAPE.FSINSERT_TAPE <>'7' THEN '制式版'+ITAPE.FSINSERT_TAPE ELSE '客製版' END) ELSE '' END),
				警示卡名稱			= ITAPE.FSINSERT_TAPE_NAME,
				警示卡長度			= ITAPE.FSINSERT_TAPE_DUR,
				檔名總片頭			= (CASE WHEN ITAPE.FSTITLE ='Y' THEN '要' ELSE '不要' END),
				檔名總片頭名稱		= ITAPE.FSTITLE_NAME,
				檔名總片頭長度		= ITAPE.FSTITLE_DUR,
				節目側標			= (CASE WHEN ITAPE.FCPROG_SIDE_LABEL ='Y' THEN '要' ELSE '不要' END),
				Live側標			= (CASE WHEN ITAPE.FCLIVE_SIDE_LABEL ='Y' THEN '要' ELSE '不要' END),
				時間側標			= (CASE WHEN ITAPE.FCTIME_SIDE_LABEL ='Y' THEN '要' ELSE '不要' END),
				重播				= (CASE WHEN ITAPE.FCREPLAY ='Y' THEN '要' ELSE '不要' END),
				錄影轉播			= (CASE WHEN ITAPE.FCRECORD_PLAY ='Y' THEN '要' ELSE '不要' END),
				立體聲				= (CASE WHEN ITAPE.FCSTEREO ='Y' THEN '要' ELSE '不要' END),
				公視LOGO			= (CASE WHEN ITAPE.FCLOGO ='L' THEN ' 左邊' WHEN ITAPE.FCLOGO ='R' THEN '右邊' ELSE '不要' END),
				雙語1				= (select FSPROGLANGNAME  From TBZPROGLANG where FSPROGLANGID = ITAPE.FSCH1),
				雙語2				= (select FSPROGLANGNAME  From TBZPROGLANG where FSPROGLANGID = ITAPE.FSCH2),
				雙語3				= (select FSPROGLANGNAME  From TBZPROGLANG where FSPROGLANGID = ITAPE.FSCH3),
				雙語4				= (select FSPROGLANGNAME  From TBZPROGLANG where FSPROGLANGID = ITAPE.FSCH4),
				
				QUERY_BY = @QUERY_BY,				
				列印人員 = ISNULL(USERS.FSUSER_ChtName,@QUERY_BY)		    /*列印人員*/
				
			FROM
				TBLOG_VIDEO AS VDO
				
				LEFT JOIN TBBROADCAST AS BROD ON (BROD.FSBRO_ID = VDO.FSBRO_ID)
				LEFT JOIN TBPROG_M AS PROG_M ON (BROD.FSBRO_TYPE = 'G') AND (PROG_M.FSPROG_ID = BROD.FSID) 
				LEFT JOIN TBPROG_D AS PROG_D ON (BROD.FSBRO_TYPE = 'G') AND (BROD.FNEPISODE <> 0) AND (PROG_D.FSPROG_ID = BROD.FSID) AND (PROG_D.FNEPISODE = BROD.FNEPISODE)
				LEFT JOIN TBDATA AS PROG_DATA ON PROG_DATA.FSDATA_ID= VDO.FSID
				LEFT JOIN TBFILE_TYPE AS FTP ON (FTP.FSARC_TYPE = VDO.FSARC_TYPE)
				LEFT JOIN TBLOG_VIDEO_SEG AS SEG ON (SEG.FSFILE_NO = VDO.FSFILE_NO)
				LEFT JOIN TBUSERS AS USERS ON (USERS.FSUSER_ID = @QUERY_BY)					/*使用者檔		    =>  列印人員*/
				LEFT JOIN TBPGM_INSERT_TAPE AS ITAPE ON  (BROD.FSBRO_TYPE = 'G') AND (BROD.FNEPISODE <> 0) AND (ITAPE.FSPROG_ID = PROG_M.FSPROG_ID) AND (ITAPE.FNEPISODE = BROD.FNEPISODE)
				AND(ITAPE.FSARC_TYPE=vdo.FSARC_TYPE)
			WHERE
				(VDO.FSFILE_NO = @FSFILE_NO)
				
			ORDER BY
				段落檔_序號
	
		END
END





GO
