USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Jarvis.Yang>
-- Create date: <2013-06-04>
-- Description:	<更新TBLOG_VIDEO_SEG的資料,微調TimeCode用>
-- =============================================
create PROCEDURE [dbo].[SP_U_TBLOG_VIDEO_SEG_Replacement]
	
@FSFILE_NO char(16) ,
@FNSEG_ID char(2) ,
@FSVIDEO_ID varchar(10) ,
@BEGTC_old char(11) ,--舊的TimeCode，拿來比對用
@ENDTC_old char(11) ,
@FSBEG_TIMECODE char(11) ,--新的TimeCode
@FSEND_TIMECODE char(11) ,
@FSUPDATED_BY varchar(50) 
AS
BEGIN



--select * from TBLOG_VIDEO_SEG 
-- where FSFILE_NO=@FSFILE_NO and FSVIDEO_ID=@FSVIDEO_ID and FNSEG_ID=@FNSEG_ID

	UPDATE MAM.dbo.TBLOG_VIDEO_SEG
SET FSBEG_TIMECODE=@FSBEG_TIMECODE,FSEND_TIMECODE=@FSEND_TIMECODE,FCLOW_RES= 'N',FSUPDATED_BY=@FSUPDATED_BY,FDUPDATED_DATE=GETDATE()
WHERE  MAM.dbo.TBLOG_VIDEO_SEG.FSFILE_NO=@FSFILE_NO 
   AND MAM.dbo.TBLOG_VIDEO_SEG.FNSEG_ID=@FNSEG_ID
   AND MAM.dbo.TBLOG_VIDEO_SEG.FSVIDEO_ID=@FSVIDEO_ID
   AND TBLOG_VIDEO_SEG.FSBEG_TIMECODE=@BEGTC_old
   AND TBLOG_VIDEO_SEG.FSEND_TIMECODE=@ENDTC_old
END




/****** Object:  StoredProcedure [dbo].[SP_U_MSSPROCESS_Tunning]    Script Date: 06/18/2013 16:24:01 ******/
SET ANSI_NULLS ON

GO
