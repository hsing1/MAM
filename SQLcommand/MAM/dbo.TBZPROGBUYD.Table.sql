USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBZPROGBUYD](
	[FSPROGBUYID] [varchar](2) NOT NULL,
	[FSPROGBUYDID] [varchar](2) NOT NULL,
	[FSPROGBUYDNAME] [nvarchar](50) NOT NULL,
	[FSSORT] [char](2) NULL,
	[FSCREATED_BY] [varchar](50) NOT NULL,
	[FDCREATED_DATE] [datetime] NOT NULL,
	[FSUPDATED_BY] [varchar](50) NULL,
	[FDUPDATED_DATE] [datetime] NULL,
	[FBISENABLE] [bit] NULL,
 CONSTRAINT [PK_TBZPROGBUYD] PRIMARY KEY CLUSTERED 
(
	[FSPROGBUYID] ASC,
	[FSPROGBUYDID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[TBZPROGBUYD] ADD  CONSTRAINT [DF_TBZPROGBUYD_FBISFUNCTION]  DEFAULT ((1)) FOR [FBISENABLE]
GO
ALTER TABLE [dbo].[TBZPROGBUYD]  WITH NOCHECK ADD  CONSTRAINT [FK_TBZPROGBUYD_TBZPROGBUY] FOREIGN KEY([FSPROGBUYID])
REFERENCES [dbo].[TBZPROGBUY] ([FSPROGBUYID])
GO
ALTER TABLE [dbo].[TBZPROGBUYD] CHECK CONSTRAINT [FK_TBZPROGBUYD_TBZPROGBUY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外購類別代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZPROGBUYD', @level2type=N'COLUMN',@level2name=N'FSPROGBUYID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外購類別細項代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZPROGBUYD', @level2type=N'COLUMN',@level2name=N'FSPROGBUYDID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外購類別細項名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZPROGBUYD', @level2type=N'COLUMN',@level2name=N'FSPROGBUYDNAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'排序號碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZPROGBUYD', @level2type=N'COLUMN',@level2name=N'FSSORT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZPROGBUYD', @level2type=N'COLUMN',@level2name=N'FSCREATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZPROGBUYD', @level2type=N'COLUMN',@level2name=N'FDCREATED_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZPROGBUYD', @level2type=N'COLUMN',@level2name=N'FSUPDATED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBZPROGBUYD', @level2type=N'COLUMN',@level2name=N'FDUPDATED_DATE'
GO
