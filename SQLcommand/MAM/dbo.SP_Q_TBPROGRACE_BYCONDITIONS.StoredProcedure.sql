USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Q_TBPROGRACE_BYCONDITIONS]

@FSPROG_ID char(7) ,
@FNEPISODE smallint , ---原本是smaiiint，因為查詢時，傳入0會把所有都帶出來，所以改成用字串傳
@FSAREA nvarchar(8) ,
@FSAREA_NOT_TAIWAN nvarchar(8) ,
@FSPROGRACE nvarchar(200) ,
@FDRACEDATE datetime ,
@FDRACEDATE_END datetime ,
@FSPROGSTATUS nvarchar(10) ,
@FNPERIOD smallint , ---原本是smaiiint，因為查詢時，傳入0會把所有都帶出來，所以改成用字串傳
@FSPROGWIN nvarchar(200) ,
@FSPROGNAME nvarchar(100) ,
@FSMEMO varchar(200),
@FSPROGSRCID varchar(2),
@FSCHANNEL_ID varchar(2)

As
if @FDRACEDATE ='1900/1/1'
	begin
		select ISNULL(b.FSUSER_ChtName,'') as FSCREATED_BY_NAME, ISNULL(C.FSUSER_ChtName,'') as FSUPDATED_BY_NAME ,
		ISNULL(CONVERT(VARCHAR(20) ,a.FDRACEDATE , 111),'') as WANT_FDRACEDATE,
		ISNULL(CONVERT(VARCHAR ,a.FDCREATED_DATE , 120),'') as WANT_FDCREATEDDATE,
		ISNULL(CONVERT(VARCHAR ,a.FDUPDATED_DATE , 120),'') as WANT_FDUPDATEDDATE,
		ISNULL(d.FSPGMNAME,'')  as FSPROG_ID_NAME,
		ISNULL(e.FSPGDNAME,'')  as FNEPISODE_NAME,
		ISNULL(d.FSPROGSRCID,'')  as FSPROGSRCID,
		ISNULL(d.FSCHANNEL_ID,'')  as FSCHANNEL_ID,
		a.* FROM TBPROGRACE  as a  
		               left outer JOIN TBUSERS as b on a.FSCREATED_BY=b.FSUSER_ID
		               left outer join TBUSERS as c on a.FSUPDATED_BY=c.FSUSER_ID
		               left outer join TBPROG_M as d on a.FSPROG_ID=d.FSPROG_ID -----[MAM].[dbo].[TBPROG_M]
		               left outer join TBPROG_D as e on a.FSPROG_ID=e.FSPROG_ID and a.FNEPISODE=e.FNEPISODE
		where @FSPROG_ID = case when @FSPROG_ID ='' then @FSPROG_ID else a.FSPROG_ID end
		AND a.FSPROGRACE like '%'+@FSPROGRACE +'%'
		AND a.FSPROGWIN like  '%'+@FSPROGWIN +'%'
		AND @FNEPISODE = case when @FNEPISODE ='' then @FNEPISODE else a.FNEPISODE end
		AND @FSAREA = case when @FSAREA ='' then @FSAREA else a.FSAREA end
		AND a.FSAREA <> case when @FSAREA_NOT_TAIWAN ='' then @FSAREA_NOT_TAIWAN else @FSAREA_NOT_TAIWAN end
		AND @FSPROGSTATUS = case when @FSPROGSTATUS ='' then @FSPROGSTATUS else a.FSPROGSTATUS end
		AND @FNPERIOD = case when @FNPERIOD ='' then @FNPERIOD else a.FNPERIOD end
		AND a.FSPROGNAME like '%'+@FSPROGNAME +'%'
		AND a.FSMEMO like @FSMEMO +'%'
		AND @FSPROGSRCID = case when @FSPROGSRCID ='' then @FSPROGSRCID else d.FSPROGSRCID end
		AND @FSCHANNEL_ID = case when @FSCHANNEL_ID ='' then @FSCHANNEL_ID else d.FSCHANNEL_ID end		
		order by FSPROG_ID,FNEPISODE
	end
else
	begin
		select ISNULL(b.FSUSER_ChtName,'') as FSCREATED_BY_NAME, ISNULL(C.FSUSER_ChtName,'') as FSUPDATED_BY_NAME ,
		ISNULL(CONVERT(VARCHAR(20) ,a.FDRACEDATE , 111),'') as WANT_FDRACEDATE,
		ISNULL(CONVERT(VARCHAR ,a.FDCREATED_DATE , 120),'') as WANT_FDCREATEDDATE,
		ISNULL(CONVERT(VARCHAR ,a.FDUPDATED_DATE , 120),'') as WANT_FDUPDATEDDATE,
		ISNULL(d.FSPGMNAME,'')  as FSPROG_ID_NAME,
		ISNULL(e.FSPGDNAME,'')  as FNEPISODE_NAME,
		ISNULL(d.FSPROGSRCID,'')  as FSPROGSRCID,
		ISNULL(d.FSCHANNEL_ID,'')  as FSCHANNEL_ID,
		a.* FROM TBPROGRACE  as a  
		               left outer JOIN TBUSERS as b on a.FSCREATED_BY=b.FSUSER_ID
		               left outer join TBUSERS as c on a.FSUPDATED_BY=c.FSUSER_ID
		               left outer join TBPROG_M as d on a.FSPROG_ID=d.FSPROG_ID -----[MAM].[dbo].[TBPROG_M]
		               left outer join TBPROG_D as e on a.FSPROG_ID=e.FSPROG_ID and a.FNEPISODE=e.FNEPISODE
		where @FSPROG_ID = case when @FSPROG_ID ='' then @FSPROG_ID else a.FSPROG_ID end
		AND a.FSPROGRACE like '%'+@FSPROGRACE +'%'
		AND a.FSPROGWIN like '%'+@FSPROGWIN +'%'
		AND @FNEPISODE = case when @FNEPISODE ='' then @FNEPISODE else a.FNEPISODE end
		AND @FSAREA = case when @FSAREA ='' then @FSAREA else a.FSAREA end
		AND a.FSAREA <> case when @FSAREA_NOT_TAIWAN ='' then @FSAREA_NOT_TAIWAN else @FSAREA_NOT_TAIWAN end
		AND a.FDRACEDATE BETWEEN @FDRACEDATE and @FDRACEDATE_END
		AND @FSPROGSTATUS = case when @FSPROGSTATUS ='' then @FSPROGSTATUS else a.FSPROGSTATUS end
		AND @FNPERIOD = case when @FNPERIOD ='' then @FNPERIOD else a.FNPERIOD end
		AND a.FSPROGNAME like '%'+@FSPROGNAME +'%'
		AND a.FSMEMO like @FSMEMO +'%'
		AND @FSPROGSRCID = case when @FSPROGSRCID ='' then @FSPROGSRCID else d.FSPROGSRCID end
		AND @FSCHANNEL_ID = case when @FSCHANNEL_ID ='' then @FSCHANNEL_ID else d.FSCHANNEL_ID end	
		order by FSPROG_ID,FNEPISODE
	end

/*
如果型態是smallint，用0去查詢的話，會把全部資料都查出來，因此在預存程序裡就要自己動手腳，
把數字型態的欄位轉成是字串型態，這樣就可以查到為0的資料

傳入資料定義：
@FNEPISODE varchar(10) , ---原本是smaiiint，因為查詢時，傳入0會把所有都帶出來，所以改成用字串傳
@FNPERIOD varchar(10) , ---原本是smaiiint，因為查詢時，傳入0會把所有都帶出來，所以改成用字串傳

查詢條件：
AND @FNEPISODE = case when @FNEPISODE ='' then @FNEPISODE else CAST(a.FNEPISODE AS varchar(10))  end
AND @FNPERIOD = case when @FNPERIOD ='' then @FNPERIOD else CAST(a.FNPERIOD AS varchar(10))  end
*/



GO
