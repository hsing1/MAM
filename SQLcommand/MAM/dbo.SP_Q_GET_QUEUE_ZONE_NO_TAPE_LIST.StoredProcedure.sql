USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   查詢片庫調帶資料
-- 查出日期區間內的節目表中尚未有檔案且在源片庫系統中有存在的節目資料
-- =============================================
CREATE Proc [dbo].[SP_Q_GET_QUEUE_ZONE_NO_TAPE_LIST]
@FSCHANNEL_ID varchar(3),
@FDBDATE varchar(10),
@FDEDATE varchar(10)

As


select distinct A.fsprog_id,A.FSPROG_NAME,TIndex,
STUFF
        (
            (
            select CAST(',' AS VARCHAR(MAX))+ CAST ( G.fnepisode AS VARCHAR(5) ) from 
             (select distinct X.fsprog_id,X.fnepisode,X.FSPROG_NAME from tbpgm_queue X 
left join tblog_video Y on X.fsprog_id=Y.Fsid and X.fnepisode=Y.Fnepisode
left join TBZCHANNEL Z on X.FSCHANNEL_ID=Z.FSCHANNEL_ID  and Y.FSARC_TYPE=Z.FSCHANNEL_TYPE 
where X.fddate>=@FDBDATE and X.fddate<=@FDEDATE 
and X.fschannel_id=@FSCHANNEL_ID
EXCEPT 
select distinct X.fsprog_id,X.fnepisode,X.FSPROG_NAME from tbpgm_queue X 
left join tblog_video Y on X.fsprog_id=Y.Fsid and X.fnepisode=Y.Fnepisode
left join TBZCHANNEL Z on X.FSCHANNEL_ID=Z.FSCHANNEL_ID
where X.fddate>=@FDBDATE and X.fddate<=@FDEDATE 
and X.fschannel_id=@FSCHANNEL_ID and Y.FSARC_TYPE=Z.FSCHANNEL_TYPE  and Y.FCFILE_STATUS in ('Y','T')) as G
where G.FSPROG_ID=A.FSPROG_ID 
             FOR XML PATH('')
            )
            ,1
            ,1
            ,''
        ) AS EPISODE
 from 
(select distinct A.fsprog_id,A.fnepisode,A.FSPROG_NAME from tbpgm_queue A 
left join tblog_video B on A.fsprog_id=B.Fsid and A.fnepisode=B.Fnepisode
left join TBZCHANNEL C on A.FSCHANNEL_ID=C.FSCHANNEL_ID and B.FSARC_TYPE=C.FSCHANNEL_TYPE
where A.fddate>=@FDBDATE and fddate<=@FDEDATE 
and A.fschannel_id=@FSCHANNEL_ID 
EXCEPT 
select distinct A.fsprog_id,A.fnepisode,A.FSPROG_NAME from tbpgm_queue A 
left join tblog_video B on A.fsprog_id=B.Fsid and A.fnepisode=B.Fnepisode
left join TBZCHANNEL C on A.FSCHANNEL_ID=C.FSCHANNEL_ID
where A.fddate>=@FDBDATE and fddate<=@FDEDATE 
and A.fschannel_id=@FSCHANNEL_ID and B.FSARC_TYPE=C.FSCHANNEL_TYPE and B.FCFILE_STATUS in ('Y','T')) as A
join [PTSTape].[PTSTape].[dbo].[tblLLabelProg] D
                        on D.ProgID=A.fsprog_id and D.EPNO=A.FNEPISODE
join [PTSTape].[PTSTape].[dbo].[tblLTape] E on D.BIB=E .BIB 
and (E.TVolume like 'v.' + CONVERT(varchar, A.FNEPISODE) + '%'  or E.TVolume like '' +CONVERT(varchar, A.FNEPISODE) + '%' or E.TVolume like 'V. ' +CONVERT(varchar, 
A.FNEPISODE) + '%')

GO
