USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   查詢短帶運行表尚未到帶資料
-- =============================================
CREATE Proc [dbo].[SP_Q_GET_TBPGM_ARR_PROMO_NO_TAPE_LIST]
@FSCHANNEL_ID varchar(3),
@FDDATE varchar(10)

As
select A.fspromo_id,A.FSPROMO_NAME,C.FSBEG_TIMECODE,C.FCFILE_STATUS,D.FSUSER_ChtName,C.FSFILE_NO,C.FSVIDEO_PROG,B.FSCHANNEL_NAME
from TBPGM_PROMO A
left join TBZCHANNEL B on B.FSCHANNEL_ID=@FSCHANNEL_ID
left join TBLOG_VIDEO C on B.FSCHANNEL_TYPE=C.FSARC_TYPE 
and A.FSPROMO_ID=C.FSID and ( C.FCFILE_STATUS='T' or C.FCFILE_STATUS='Y')
left join TBUSERS D on A.FSCREATED_BY=D.FSUSER_ID
where FSPROMO_ID in(
select distinct fspromo_id from TBPGM_ARR_PROMO 
where FDDATE=@FDDATE and FSCHANNEL_ID=@FSCHANNEL_ID)
and ((C.FCFILE_STATUS<>'T' and C.FCFILE_STATUS<>'Y') or C.FCFILE_STATUS is null) 

GO
