USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_U_TBUSERS_DEPT_SUPERVISOR_MARK]



AS
--更新主管註記
UPDATE [MAM].[dbo].[TBUSERS] 
SET [FSIs_Supervisor]='Y'
WHERE [FSUSER_ID] in
(SELECT [FSSupervisor_ID]
FROM [MAM].[dbo].[TBUSER_DEP]
where NOT(ISNULL(LTRIM([FSSupervisor_ID]),'') = ''))


GO
