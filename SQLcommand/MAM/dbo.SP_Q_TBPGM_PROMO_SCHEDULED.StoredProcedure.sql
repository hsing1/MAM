USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Q_TBPGM_PROMO_SCHEDULED]

@FNPROMO_BOOKING_NO	int,
@FDDATE datetime,
@FSCHANNEL_ID	char(2)
As
select A.FNPROMO_BOOKING_NO,A.FSPROMO_ID,A.FNNO,
A.FDDATE,A.FSCHANNEL_ID,A.FSTIME,A.FCSTATUS,B.FSCHANNEL_NAME 
from TBPGM_PROMO_SCHEDULED A,TBZCHANNEL B
where A.FSCHANNEL_ID=B.FSCHANNEL_ID
and @FNPROMO_BOOKING_NO = case when @FNPROMO_BOOKING_NO = '' then @FNPROMO_BOOKING_NO else FNPROMO_BOOKING_NO end
and @FDDATE = case when @FDDATE = '' then @FDDATE else A.FDDATE end
and @FSCHANNEL_ID = case when @FSCHANNEL_ID = '' then @FSCHANNEL_ID else A.FSCHANNEL_ID end

GO
