USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- =============================================

-- Author:        <Mike.Lin>

-- Create date:   <2016/05/26>

-- Description:   查詢短帶長度差異資料

-- 查出短帶運行表的長度與實際短帶長度不同的資,料混播版

-- =============================================

CREATE Proc [dbo].[SP_Q_GET_TBPGM_ARR_PROMO_DUR_DIFF_MIX]
@FDDATE	date,
@FSCHANNEL_ID	varchar(2)
As
 IF (SELECT FSCHANNEL_TYPE FROM TBZCHANNEL WHERE FSCHANNEL_ID=@FSCHANNEL_ID)='001' 
select distinct A.fspromo_id,A.fsdur,D.FSBEG_TIMECODE,D.FSEND_TIMECODE,C.FSARC_TYPE,E.FSPROMO_NAME,D.FSVIDEO_ID,D.FSFILE_NO,A.FSVIDEO_ID AS OLD_VIDEO_ID,A.FSFILE_NO AS OLD_FILE_NO
  from tbpgm_arr_promo A 
  left join TBZCHANNEL B on A.fschannel_id=B.fschannel_id
left join TBLOG_VIDEO C on A.FSPROMO_ID=C.FSID and C.FSARC_TYPE=B.FSCHANNEL_TYPE and C.FCFILE_STATUS in('T','Y')
left join TBLOG_VIDEO_seg D on C.FSFILE_NO=D.FSFILE_NO
left join TBPGM_PROMO E on A.FSPROMO_ID=E.FSPROMO_ID
where A.fddate=@FDDATE and A.fschannel_id=@FSCHANNEL_ID 
else
select distinct A.fspromo_id,A.fsdur,D.FSBEG_TIMECODE,D.FSEND_TIMECODE,C.FSARC_TYPE,E.FSPROMO_NAME,D.FSVIDEO_ID,D.FSFILE_NO,A.FSVIDEO_ID AS OLD_VIDEO_ID,A.FSFILE_NO AS OLD_FILE_NO
  from tbpgm_arr_promo A 
  left join TBZCHANNEL B on A.fschannel_id=B.fschannel_id
left join TBLOG_VIDEO C on A.FSPROMO_ID=C.FSID and C.FCFILE_STATUS not in('D','X','F') and C.FSARC_TYPE in('001','002')
left join TBLOG_VIDEO_seg D on C.FSFILE_NO=D.FSFILE_NO
left join TBPGM_PROMO E on A.FSPROMO_ID=E.FSPROMO_ID
where A.fddate=@FDDATE and A.fschannel_id=@FSCHANNEL_ID  order by A.fspromo_id,C.FSARC_TYPE desc

GO
