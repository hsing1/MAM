USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   查詢首播清單資料(目前應已不使用)
-- =============================================
CREATE Proc [dbo].[SP_Q_TBPGM_FIRST_PLAY_LIST]

@FSCHANNEL_ID	varchar(2),
@FDDATE		date,
@FSVIDEO_ID	varchar(10)

As
select FSCHANNEL_ID,FDDATE,FSVIDEO_ID,FCTYPE,FDCREATEDATE,FNVERSION from
dbo.TBPGM_FIRST_PLAY_LIST where FSCHANNEL_ID=@FSCHANNEL_ID and FDDATE=@FDDATE 
and @FSVIDEO_ID = case when @FSVIDEO_ID = '' then @FSVIDEO_ID else FSVIDEO_ID end






	
	
GO
