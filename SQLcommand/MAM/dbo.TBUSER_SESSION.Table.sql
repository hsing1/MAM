USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBUSER_SESSION](
	[FSSESSION_ID] [varchar](255) NOT NULL,
	[FSUSER_ID] [varchar](20) NOT NULL,
	[FSSRC_IP] [varchar](15) NULL,
	[FDLAST_ACCESS_TIME] [datetime] NOT NULL,
 CONSTRAINT [PK_TBUSER_SESSION] PRIMARY KEY CLUSTERED 
(
	[FSSESSION_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[TBUSER_SESSION] ADD  CONSTRAINT [DF_TBUSER_SESSION_FDLAST_ACCESS_TIME]  DEFAULT (getdate()) FOR [FDLAST_ACCESS_TIME]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'登入階段ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_SESSION', @level2type=N'COLUMN',@level2name=N'FSSESSION_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用者代號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_SESSION', @level2type=N'COLUMN',@level2name=N'FSUSER_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'來源IP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBUSER_SESSION', @level2type=N'COLUMN',@level2name=N'FSSRC_IP'
GO
