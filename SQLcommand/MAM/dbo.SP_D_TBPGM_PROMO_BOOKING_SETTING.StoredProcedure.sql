USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/24>
-- Description:   刪除短帶託播單時段檔次資料
-- =============================================
CREATE Proc [dbo].[SP_D_TBPGM_PROMO_BOOKING_SETTING]

@FNPROMO_BOOKING_NO	int
As
delete TBPGM_PROMO_BOOKING_SETTING 
where FNPROMO_BOOKING_NO= @FNPROMO_BOOKING_NO
   
GO
