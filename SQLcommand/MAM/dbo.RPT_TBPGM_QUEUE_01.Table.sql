USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RPT_TBPGM_QUEUE_01](
	[QUERY_KEY] [uniqueidentifier] NOT NULL,
	[QUERY_KEY_SUBID] [uniqueidentifier] NOT NULL,
	[QUERY_BY] [varchar](50) NOT NULL,
	[QUERY_DATE] [datetime] NOT NULL,
	[播出日期] [date] NOT NULL,
	[頻道] [nvarchar](50) NOT NULL,
	[節目名稱] [nvarchar](50) NOT NULL,
	[集數] [int] NOT NULL,
	[影像編號] [varchar](16) NOT NULL,
	[起始時間] [varchar](4) NOT NULL,
	[結束時間] [varchar](4) NOT NULL,
	[LIVE播出] [varchar](5) NOT NULL,
	[上檔下檔] [varchar](5) NOT NULL,
 CONSTRAINT [PK_RPT_TBPGM_QUEUE_01] PRIMARY KEY CLUSTERED 
(
	[QUERY_KEY] ASC,
	[QUERY_KEY_SUBID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'報表編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_QUEUE_01', @level2type=N'COLUMN',@level2name=N'QUERY_KEY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'資料編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_QUEUE_01', @level2type=N'COLUMN',@level2name=N'QUERY_KEY_SUBID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_QUEUE_01', @level2type=N'COLUMN',@level2name=N'QUERY_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_QUEUE_01', @level2type=N'COLUMN',@level2name=N'QUERY_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'播出日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_QUEUE_01', @level2type=N'COLUMN',@level2name=N'播出日期'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'頻道' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_QUEUE_01', @level2type=N'COLUMN',@level2name=N'頻道'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_QUEUE_01', @level2type=N'COLUMN',@level2name=N'節目名稱'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'集數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_QUEUE_01', @level2type=N'COLUMN',@level2name=N'集數'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'影像編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_QUEUE_01', @level2type=N'COLUMN',@level2name=N'影像編號'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'起始時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_QUEUE_01', @level2type=N'COLUMN',@level2name=N'起始時間'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'結束時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_QUEUE_01', @level2type=N'COLUMN',@level2name=N'結束時間'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'LIVE播出' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_QUEUE_01', @level2type=N'COLUMN',@level2name=N'LIVE播出'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'上檔下檔' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_QUEUE_01', @level2type=N'COLUMN',@level2name=N'上檔下檔'
GO
EXEC sys.sp_addextendedproperty @name=N'描述', @value=N'節目表報表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RPT_TBPGM_QUEUE_01'
GO
