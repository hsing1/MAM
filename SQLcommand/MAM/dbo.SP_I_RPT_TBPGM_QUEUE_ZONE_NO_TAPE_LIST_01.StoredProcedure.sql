USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   新增片庫調帶報表資料
-- =============================================
CREATE Proc [dbo].[SP_I_RPT_TBPGM_QUEUE_ZONE_NO_TAPE_LIST_01]

@QUERY_KEY	uniqueidentifier,
@QUERY_KEY_SUBID	uniqueidentifier,
@QUERY_BY	varchar(50),
@節目代碼	varchar(12),
@分類號	nvarchar(50),
@節目名稱	nvarchar(50),
@子集	varchar(50),
@BDate	date,
@EDate	date,
@頻道	varchar(20)

As
Insert Into dbo.RPT_TBPGM_QUEUE_ZONE_NO_TAPE_LIST_01(
QUERY_KEY,QUERY_KEY_SUBID,QUERY_BY,QUERY_DATE,
節目代碼,分類號,節目名稱,子集,BDate,EDate,頻道
)
Values(@QUERY_KEY,@QUERY_KEY_SUBID,@QUERY_BY,Getdate(),
@節目代碼,@分類號,@節目名稱,@子集,@BDate,@EDate,@頻道)

GO
