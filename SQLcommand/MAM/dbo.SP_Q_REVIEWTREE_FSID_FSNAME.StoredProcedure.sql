USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Q_REVIEWTREE_FSID_FSNAME]
@StationID nvarchar(50), 
@CatalogID nvarchar(50),
@YearID nvarchar(50)

As


IF @CatalogID='G' 
BEGIN 
	SELECT FSPROG_ID as FSID,FSPGMNAME as FSNAME FROM [MAM].[dbo].[TBPROG_M]
	where LEFT(FSPROG_ID,4)=@YearID and FSCHANNEL_ID=@StationID
END 

IF @CatalogID='P' 
Begin 
	SELECT FSPROMO_ID as FSID,FSPROMO_NAME as FSNAME FROM [MAM].[dbo].[TBPGM_PROMO]
	where LEFT(FSPROMO_ID,4)=@YearID and FSMAIN_CHANNEL_ID=@StationID
End 

if @CatalogID='D'
begin
	SELECT FSDATA_ID as FSID,FSDATA_NAME as FSNAME FROM [MAM].[dbo].[TBDATA]
	where LEFT([FSDATA_ID],4)=@YearID
end

GO
