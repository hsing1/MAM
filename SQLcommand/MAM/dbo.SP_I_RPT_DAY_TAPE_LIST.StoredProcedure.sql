USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2015/04/29>
-- Description:   主控播出排帶表
-- =============================================
CREATE Proc [dbo].[SP_I_RPT_DAY_TAPE_LIST]

@QUERY_KEY	uniqueidentifier,
@QUERY_KEY_SUBID	uniqueidentifier,
@QUERY_BY	varchar(50),
@播出日期	date,
@頻道	nvarchar(50),
@序號	int,
@節目名稱	nvarchar(50),
@集數	int,
@播出時間	varchar(11),
@FSIN	varchar(4),
@VIDEO_ID	varchar(8),
@FSOUT	varchar(4),
@長度	varchar(11)

As
Insert Into dbo.RPT_DAY_TAPE_LIST(
QUERY_KEY,QUERY_KEY_SUBID,QUERY_BY,QUERY_DATE,
播出日期,頻道,序號,節目名稱,集數,播出時間,FSIN,VIDEO_ID,FSOUT,長度
)
Values(@QUERY_KEY,@QUERY_KEY_SUBID,@QUERY_BY,Getdate(),
@播出日期,@頻道,@序號,@節目名稱,@集數,@播出時間,@FSIN,@VIDEO_ID,@FSOUT,@長度
)
GO
