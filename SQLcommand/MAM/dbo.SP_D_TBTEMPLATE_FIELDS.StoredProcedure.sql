USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Proc [dbo].[SP_D_TBTEMPLATE_FIELDS]
@FNID int 
As

-------------------------Dennis.Wen: 移除自訂欄位, 把該欄位內容清空-------------------------

SET NOCOUNT ON;

DECLARE	@FSTABLE VARCHAR(50), @FSFIELD VARCHAR(50), @SQL VARCHAR(200)

/*刪除前取得此自訂欄位所在的資料表與欄位名*/
SELECT	@FSTABLE = FSTABLE, @FSFIELD = FSFIELD
FROM	TBTEMPLATE_FIELDS
WHERE	(FNID = @FNID) 

/*串出語法, 把該資料表的該欄位內容全部RESET回NULL值*/
SET @SQL = 'UPDATE ' + @FSTABLE + ' SET ' + @FSFIELD+ ' = NULL'

/*執行語法*/
EXEC(@SQL)

-------------------------Dennis.Wen: 移除自訂欄位, 把該欄位內容清空-------------------------

delete from dbo.TBTEMPLATE_FIELDS 
where FNID = @FNID 

GO
