USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <David.Sin>
-- Create date:   <2010/10/02>
-- Description:   查詢檔案是否來自影帶
-- =============================================

CREATE PROC [dbo].[SP_Q_CHECK_FILE_IS_TAPE]
@FSFILE_NO varchar(16)
AS
SELECT FCFROM FROM TBLOG_VIDEO WHERE FSFILE_NO=@FSFILE_NO

GO
