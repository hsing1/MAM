USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBUSERSBAK](
	[FSUSER_ID] [nvarchar](50) NOT NULL,
	[FSUSER] [nvarchar](50) NULL,
	[FSPASSWD] [varchar](255) NULL,
	[FSUSER_ChtName] [nvarchar](50) NULL,
	[FSUSER_TITLE] [nvarchar](50) NULL,
	[FSDEPT] [nvarchar](50) NULL,
	[FSEMAIL] [nvarchar](50) NULL,
	[FCACTIVE] [nvarchar](1) NULL,
	[FSDESCRIPTION] [nvarchar](max) NULL,
	[FCSECRET] [char](1) NULL,
	[FSCHANNEL_ID] [char](2) NULL,
	[FNUpperDept_ID] [int] NULL,
	[FNDep_ID] [int] NULL,
	[FNGroup_ID] [int] NULL,
	[FSUpperDept_ChtName] [nvarchar](60) NULL,
	[FSDep_ChtName] [nvarchar](60) NULL,
	[FSGroup_ChtName] [nvarchar](60) NULL,
	[FSIs_Main_Title] [nvarchar](1) NULL,
	[FSIs_Supervisor] [nvarchar](1) NULL,
	[FSCREATED_BY] [varchar](50) NULL,
	[FDCREATED_DATE] [datetime] NULL,
	[FSUPDATED_BY] [varchar](50) NULL,
	[FDUPDATED_DATE] [datetime] NULL,
 CONSTRAINT [PK_TBUSERSBAK] PRIMARY KEY CLUSTERED 
(
	[FSUSER_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
