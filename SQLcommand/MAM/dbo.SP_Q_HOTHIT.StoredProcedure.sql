USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <David.Sin>
-- Create date:   <2010/10/02>
-- Description:   �d�߼����I�\
-- =============================================

CREATE Proc [dbo].[SP_Q_HOTHIT]
@FDDATE_BEGIN varchar(10),
@FDDATE_END varchar(10)

As

Select Top 10 FSSYS_ID,FSTITLE,FSINDEX_TYPE,COUNT(FNNO) As FNNO_COUNT
From TBSEARCH_HOTHIT
Where FDDATE Between @FDDATE_BEGIN And @FDDATE_END
Group By FSSYS_ID,FSTITLE,FSINDEX_TYPE
Order By FNNO_COUNT Desc
GO
