USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   查詢節目未到帶清單報表
-- =============================================
CREATE PROCEDURE [dbo].[SP_R_RPT_TBPGM_QUEUE_ZONE_NO_TAPE_LIST_01]
	@QUERY_KEY	VARCHAR(36)
AS
BEGIN
	SELECT
		[QUERY_KEY]
      ,[QUERY_KEY_SUBID]
      ,[QUERY_BY]
      ,[QUERY_DATE]
      ,[節目代碼]
      ,[分類號]
      ,[節目名稱]
      ,[子集]
      ,[BDate]
      ,[EDate]
      ,[頻道]

	FROM
		RPT_TBPGM_QUEUE_ZONE_NO_TAPE_LIST_01
	WHERE
		(QUERY_KEY = @QUERY_KEY)
	ORDER BY
		[節目代碼]
END
GO
