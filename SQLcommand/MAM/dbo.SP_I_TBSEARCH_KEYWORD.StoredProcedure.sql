USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <David.Sin>
-- Create date:   <2010/10/02>
-- Description:   新增熱門關鍵字
-- =============================================

CREATE Proc [dbo].[SP_I_TBSEARCH_KEYWORD]
@FSKEYWORD nvarchar(50),
@FDDATE varchar(10)
As

Declare @FNNO bigint
Select @FNNO=MAX(FNNO) From TBSEARCH_KEYWORD 
If @FNNO is null Set @FNNO=1 Else Set @FNNO=@FNNO+1
	
Insert Into TBSEARCH_KEYWORD(FNNO,FSKEYWORD,FDDATE)Values(@FNNO,@FSKEYWORD,@FDDATE)


GO
