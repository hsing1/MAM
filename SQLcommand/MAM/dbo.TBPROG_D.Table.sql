USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBPROG_D](
	[FSPROG_ID] [char](7) NOT NULL,
	[FNEPISODE] [smallint] NOT NULL,
	[FSPGDNAME] [nvarchar](50) NOT NULL,
	[FSPGDENAME] [nvarchar](100) NULL,
	[FNLENGTH] [smallint] NULL,
	[FNTAPELENGTH] [int] NULL,
	[FSPROGSRCID] [varchar](2) NOT NULL,
	[FSPROGATTRID] [varchar](2) NOT NULL,
	[FSPROGAUDID] [varchar](2) NOT NULL,
	[FSPROGTYPEID] [varchar](2) NOT NULL,
	[FSPROGGRADEID] [varchar](2) NOT NULL,
	[FSPROGLANGID1] [varchar](2) NOT NULL,
	[FSPROGLANGID2] [varchar](2) NOT NULL,
	[FSPROGSALEID] [varchar](2) NOT NULL,
	[FSPROGBUYID] [varchar](2) NULL,
	[FSPROGBUYDID] [varchar](2) NULL,
	[FCRACE] [varchar](1) NULL,
	[FSSHOOTSPEC] [varchar](100) NULL,
	[FSPRDYEAR] [varchar](4) NULL,
	[FSPROGGRADE] [nvarchar](100) NULL,
	[FSCONTENT] [nvarchar](max) NULL,
	[FSMEMO] [nvarchar](500) NULL,
	[FNSHOWEPISODE] [smallint] NULL,
	[FSCHANNEL_ID] [char](2) NULL,
	[FSDEL] [char](1) NULL,
	[FSDELUSER] [varchar](50) NULL,
	[FSNDMEMO] [varchar](500) NULL,
	[FSPROGSPEC] [varchar](100) NULL,
	[FSCR_TYPE] [char](2) NULL,
	[FSCR_NOTE] [nvarchar](500) NULL,
	[FSCRTUSER] [varchar](5) NOT NULL,
	[FDCRTDATE] [datetime] NOT NULL,
	[FSUPDUSER] [varchar](5) NULL,
	[FDUPDDATE] [datetime] NULL,
	[_FDTRANDATE] [datetime] NULL,
	[FSCREATED_BY_PGM] [varchar](50) NULL,
	[FSUPDATED_BY_PGM] [varchar](50) NULL,
	[FDEXPIRE_DATE] [datetime] NULL,
	[FSEXPIRE_DATE_ACTION] [nvarchar](10) NULL,
	[FSPROGNATIONID] [nvarchar](50) NULL,
 CONSTRAINT [ TBPROG_D] PRIMARY KEY CLUSTERED 
(
	[FSPROG_ID] ASC,
	[FNEPISODE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[TBPROG_D] ADD  CONSTRAINT [DF_TBPROG_D_FDEXPIRE_DATE]  DEFAULT ('1900-1-1') FOR [FDEXPIRE_DATE]
GO
ALTER TABLE [dbo].[TBPROG_D] ADD  CONSTRAINT [DF_TBPROG_D_FSEXPIRE_DATE_ACTION]  DEFAULT ('') FOR [FSEXPIRE_DATE_ACTION]
GO
ALTER TABLE [dbo].[TBPROG_D] ADD  CONSTRAINT [DF_TBPROG_D_FSPROGNATIONID]  DEFAULT ('01') FOR [FSPROGNATIONID]
GO
ALTER TABLE [dbo].[TBPROG_D]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROG_D_TBZCHANNEL] FOREIGN KEY([FSCHANNEL_ID])
REFERENCES [dbo].[TBZCHANNEL] ([FSCHANNEL_ID])
GO
ALTER TABLE [dbo].[TBPROG_D] CHECK CONSTRAINT [FK_TBPROG_D_TBZCHANNEL]
GO
ALTER TABLE [dbo].[TBPROG_D]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROG_D_TBZPROGATTR] FOREIGN KEY([FSPROGATTRID])
REFERENCES [dbo].[TBZPROGATTR] ([FSPROGATTRID])
GO
ALTER TABLE [dbo].[TBPROG_D] CHECK CONSTRAINT [FK_TBPROG_D_TBZPROGATTR]
GO
ALTER TABLE [dbo].[TBPROG_D]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROG_D_TBZPROGAUD] FOREIGN KEY([FSPROGAUDID])
REFERENCES [dbo].[TBZPROGAUD] ([FSPROGAUDID])
GO
ALTER TABLE [dbo].[TBPROG_D] CHECK CONSTRAINT [FK_TBPROG_D_TBZPROGAUD]
GO
ALTER TABLE [dbo].[TBPROG_D]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROG_D_TBZPROGBUY] FOREIGN KEY([FSPROGBUYID])
REFERENCES [dbo].[TBZPROGBUY] ([FSPROGBUYID])
GO
ALTER TABLE [dbo].[TBPROG_D] CHECK CONSTRAINT [FK_TBPROG_D_TBZPROGBUY]
GO
ALTER TABLE [dbo].[TBPROG_D]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROG_D_TBZPROGGRADE] FOREIGN KEY([FSPROGGRADEID])
REFERENCES [dbo].[TBZPROGGRADE] ([FSPROGGRADEID])
GO
ALTER TABLE [dbo].[TBPROG_D] CHECK CONSTRAINT [FK_TBPROG_D_TBZPROGGRADE]
GO
ALTER TABLE [dbo].[TBPROG_D]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROG_D_TBZPROGLANG] FOREIGN KEY([FSPROGLANGID1])
REFERENCES [dbo].[TBZPROGLANG] ([FSPROGLANGID])
GO
ALTER TABLE [dbo].[TBPROG_D] CHECK CONSTRAINT [FK_TBPROG_D_TBZPROGLANG]
GO
ALTER TABLE [dbo].[TBPROG_D]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROG_D_TBZPROGLANG1] FOREIGN KEY([FSPROGLANGID2])
REFERENCES [dbo].[TBZPROGLANG] ([FSPROGLANGID])
GO
ALTER TABLE [dbo].[TBPROG_D] CHECK CONSTRAINT [FK_TBPROG_D_TBZPROGLANG1]
GO
ALTER TABLE [dbo].[TBPROG_D]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROG_D_TBZPROGSRC] FOREIGN KEY([FSPROGSRCID])
REFERENCES [dbo].[TBZPROGSRC] ([FSPROGSRCID])
GO
ALTER TABLE [dbo].[TBPROG_D] CHECK CONSTRAINT [FK_TBPROG_D_TBZPROGSRC]
GO
ALTER TABLE [dbo].[TBPROG_D]  WITH NOCHECK ADD  CONSTRAINT [FK_TBPROG_D_TBZPROGTYPE] FOREIGN KEY([FSPROGTYPEID])
REFERENCES [dbo].[TBZPROGTYPE] ([FSPROGTYPEID])
GO
ALTER TABLE [dbo].[TBPROG_D] CHECK CONSTRAINT [FK_TBPROG_D_TBZPROGTYPE]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目編號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D', @level2type=N'COLUMN',@level2name=N'FSPROG_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'集別' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D', @level2type=N'COLUMN',@level2name=N'FNEPISODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'子集名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D', @level2type=N'COLUMN',@level2name=N'FSPGDNAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'子集外文名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D', @level2type=N'COLUMN',@level2name=N'FSPGDENAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'時長' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D', @level2type=N'COLUMN',@level2name=N'FNLENGTH'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'實際帶長' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D', @level2type=N'COLUMN',@level2name=N'FNTAPELENGTH'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目來源代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D', @level2type=N'COLUMN',@level2name=N'FSPROGSRCID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'內容屬性代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D', @level2type=N'COLUMN',@level2name=N'FSPROGATTRID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'目標觀眾代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D', @level2type=N'COLUMN',@level2name=N'FSPROGAUDID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'表現方式代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D', @level2type=N'COLUMN',@level2name=N'FSPROGTYPEID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'節目分級代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D', @level2type=N'COLUMN',@level2name=N'FSPROGGRADEID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主聲道代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D', @level2type=N'COLUMN',@level2name=N'FSPROGLANGID1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'副聲道代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D', @level2type=N'COLUMN',@level2name=N'FSPROGLANGID2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'行銷類別代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D', @level2type=N'COLUMN',@level2name=N'FSPROGSALEID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外購類別代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D', @level2type=N'COLUMN',@level2name=N'FSPROGBUYID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外購類別細項代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D', @level2type=N'COLUMN',@level2name=N'FSPROGBUYDID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'可排檔' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D', @level2type=N'COLUMN',@level2name=N'FCRACE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'拍攝規格代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D', @level2type=N'COLUMN',@level2name=N'FSSHOOTSPEC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'完成年度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D', @level2type=N'COLUMN',@level2name=N'FSPRDYEAR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'非正常分級的說明' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D', @level2type=N'COLUMN',@level2name=N'FSPROGGRADE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'聯絡人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D', @level2type=N'COLUMN',@level2name=N'FSCONTENT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'備註' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D', @level2type=N'COLUMN',@level2name=N'FSMEMO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'播出集別' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D', @level2type=N'COLUMN',@level2name=N'FNSHOWEPISODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'頻道別代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D', @level2type=N'COLUMN',@level2name=N'FSCHANNEL_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'2nd備註' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D', @level2type=N'COLUMN',@level2name=N'FSNDMEMO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'帶子的類型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D', @level2type=N'COLUMN',@level2name=N'FSCR_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D', @level2type=N'COLUMN',@level2name=N'FSCRTUSER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建檔日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D', @level2type=N'COLUMN',@level2name=N'FDCRTDATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D', @level2type=N'COLUMN',@level2name=N'FSUPDUSER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBPROG_D', @level2type=N'COLUMN',@level2name=N'FDUPDDATE'
GO
