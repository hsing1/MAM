USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================

-- Author:        <David.Sin>

-- Create date:   <2014/09/24>

-- Description:   新增匯出節目明細報表

-- =============================================

CREATE PROC [dbo].[SP_I_RPT_TBPROGRAM_VIDEO_DETAIL]
@FSGUID VARCHAR(50),
@FSCREATED_BY VARCHAR(50),
@FSTYPE CHAR(1),
@FSPROG_ID CHAR(7),
@FNBEG_EPISODE INT,
@FNEND_EPISODE INT,
@FSCHANNEL_ID CHAR(2),
@FSSOURCE NVARCHAR(20) 

AS

IF @FSSOURCE = 'MAM-VIDEO' AND @FSTYPE = 'G'
BEGIN
	
	BEGIN TRY

		INSERT INTO [dbo].[RPT_TBPROGRAM_VIDEO_DETAIL]

		SELECT 
			@FSGUID,
			GETDATE(),
			@FSCREATED_BY,
			[TBLOG_VIDEO].[FSFILE_NO],
			[TBPROG_M].[FSPGMNAME],
			[TBPROG_D].[FNEPISODE],
			[TBPROG_D].[FSPGDNAME],
			[TBPROG_D].[FSCONTENT],
			[TBLOG_VIDEO].[FSBEG_TIMECODE] + '~' + [TBLOG_VIDEO].[FSEND_TIMECODE] AS [FSTIMECODE],
			CASE [TBLOG_VIDEO].[FSTYPE]
				WHEN 'G' THEN '節目'
				WHEN 'P' THEN 'PROMO'
				WHEN 'D' THEN '資料帶'
			END AS [FSTYPE],
			[TBLOG_VIDEO].[FSFILE_TYPE_HV],
			[TBFILE_TYPE].[FSTYPE_NAME],
			(SELECT [FSBEG_TIMECODE] + '~' + [FSEND_TIMECODE] + 'vbnewline' 
				FROM [dbo].[TBLOG_VIDEO_SEG] A
				WHERE [TBLOG_VIDEO].[FSFILE_NO] = A.FSFILE_NO FOR XML PATH('')) AS [FSTIMEVODE_SEGMENT],
		
			--CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).query('/Mediainfo/File/track[@type="General"]') AS [FSGENERAL_FORMAT]
			ISNULL(
			'Format:' + A.[Format] + 'vbnewline' + 
			'Commercial_name:' + A.[Commercial_name] + 'vbnewline' + 
			'Format_profile:' + A.[Format_profile] + 'vbnewline' + 
			'File_size:' + A.[File_size] + 'vbnewline' + 
			'Duration:' + A.[Duration] + 'vbnewline' + 
			'Overall_bit_rate:' + A.[Overall_bit_rate] + 'vbnewline' + 
			'Encoded_date:' + A.[Encoded_date] + 'vbnewline' + 
			'Writing_application:' + A.[Writing_application] + 'vbnewline' + 
			'Writing_library:' + A.[Writing_library],'') AS [FSGENERAL_FORMAT],
			ISNULL(
			'ID:' + A.[ID] + 'vbnewline' + 
			'Format:' + A.[Format_V] + 'vbnewline' + 
			'Commercial_name:' + A.[Commercial_name_V] + 'vbnewline' + 
			'Format_version:' + A.[Format_version] + 'vbnewline' + 
			'Format_profile:' + A.[Format_profile_V] + 'vbnewline' + 
			'Format_settings__BVOP:' + A.[Format_settings__BVOP] + 'vbnewline' + 
			'Format_settings__Matrix:' + A.[Format_settings__Matrix] + 'vbnewline' + 
			'Format_settings__GOP:' + A.[Format_settings__GOP] + 'vbnewline' + 
			'Format_Settings_Wrapping:' + A.[Format_Settings_Wrapping] + 'vbnewline' + 
			'Duration:' + A.[Duration_V] + 'vbnewline' + 
			'Bit_rate:' + A.[Bit_rate] + 'vbnewline' + 
			'Maximum_bit_rate:' + A.[Maximum_bit_rate] + 'vbnewline' + 
			'Width:' + A.[Width] + 'vbnewline' + 
			'Height:' + A.[Height] + 'vbnewline' + 
			'Original_height:' + A.[Original_height] + 'vbnewline' + 
			'Display_aspect_ratio:' + A.[Display_aspect_ratio] + 'vbnewline' + 
			'Frame_rate:' + A.[Frame_rate] + 'vbnewline' + 
			'Standard:' + A.[Standard] + 'vbnewline' + 
			'Color_space:' + A.[Color_space] + 'vbnewline' + 
			'Chroma_subsampling:' + A.[Chroma_subsampling] + 'vbnewline' + 
			'Bit_depth:' + A.[Bit_depth] + 'vbnewline' + 
			'Scan_type:' + A.[Scan_type] + 'vbnewline' + 
			'Scan_order:' + A.[Scan_order] + 'vbnewline' + 
			'Compression_mode:' + A.[Compression_mode] + 'vbnewline' + 
			'Bits__Pixel_Frame_:' + A.[Bits__Pixel_Frame_] + 'vbnewline' + 
			'Stream_size:' + A.[Stream_size] + 'vbnewline' + 
			'Delay_SDTI:' + A.[Delay_SDTI] + 'vbnewline' +
			'Color_primaries:' + A.[Color_primaries] + 'vbnewline' +
			'Transfer_characteristics:' + A.[Transfer_characteristics] + 'vbnewline' +
			'Matrix_coefficients:' + A.[Matrix_coefficients],'') AS [FSVIDEO_FORMAT],

			ISNULL(
			'ID:' + A.[ID_A] + 'vbnewline' + 
			'Format:' + A.[Format_A] + 'vbnewline' + 
			'Format_settings__Endianness:' + A.[Format_settings__Endianness] + 'vbnewline' + 
			'Format_Settings_Wrapping:' + A.[Format_Settings_Wrapping_A] + 'vbnewline' + 
			'Duration:' + A.[Duration] + 'vbnewline' + 
			'Bit_rate_mode:' + A.[Bit_rate_mode_A] + 'vbnewline' + 
			'Bit_rate:' + A.[Bit_rate] + 'vbnewline' + 
			'Channel_s_:' + A.[Channel_s_] + 'vbnewline' + 
			'Sampling_rate:' + A.[Sampling_rate] + 'vbnewline' + 
			'Bit_depth:' + A.[Bit_depth_A] + 'vbnewline' + 
			'Stream_size:' + A.[Stream_size_A] + 'vbnewline' + 
			'Delay_SDTI:' + A.[Delay_SDTI_A],'')AS [FSAUDIO_FORMAT]

		FROM [dbo].[TBLOG_VIDEO] JOIN [dbo].[TBPROG_M] ON [TBLOG_VIDEO].[FSID] = [TBPROG_M].[FSPROG_ID]
								JOIN [dbo].[TBPROG_D] ON [TBLOG_VIDEO].[FSID] = [TBPROG_D].[FSPROG_ID] AND [TBLOG_VIDEO].[FNEPISODE] = [TBPROG_D].[FNEPISODE]
								JOIN [dbo].[TBFILE_TYPE] ON [TBLOG_VIDEO].[FSARC_TYPE] = [TBFILE_TYPE].[FSARC_TYPE]
								JOIN [dbo].[TBZCHANNEL] ON [TBLOG_VIDEO].[FSCHANNEL_ID] = [TBZCHANNEL].[FSCHANNEL_ID]
								LEFT JOIN (SELECT FSFILE_NO,
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="General"]/Format)[1]','nvarchar(2000)'),'') AS [Format],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="General"]/Commercial_name)[1]','nvarchar(2000)'),'') AS [Commercial_name],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="General"]/Format_profile)[1]','nvarchar(2000)'),'') AS [Format_profile],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="General"]/File_size)[1]','nvarchar(2000)'),'') AS [File_size],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="General"]/Duration)[1]','nvarchar(2000)'),'') AS [Duration],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="General"]/Overall_bit_rate)[1]','nvarchar(2000)'),'') AS [Overall_bit_rate],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="General"]/Encoded_date)[1]','nvarchar(2000)'),'') AS [Encoded_date],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="General"]/Writing_application)[1]','nvarchar(2000)'),'') AS [Writing_application],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="General"]/Writing_library)[1]','nvarchar(2000)'),'') AS [Writing_library],

												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Video"]/ID)[1]','nvarchar(2000)'),'') AS [ID],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Video"]/Format)[1]','nvarchar(2000)'),'') AS [Format_V],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Video"]/Commercial_name)[1]','nvarchar(2000)'),'') AS [Commercial_name_V],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Video"]/Format_version)[1]','nvarchar(2000)'),'') AS [Format_version],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Video"]/Format_profile)[1]','nvarchar(2000)'),'') AS [Format_profile_V],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Video"]/Format_settings__BVOP)[1]','nvarchar(2000)'),'') AS [Format_settings__BVOP],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Video"]/Format_settings__Matrix)[1]','nvarchar(2000)'),'') AS [Format_settings__Matrix],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Video"]/Format_settings__GOP)[1]','nvarchar(2000)'),'') AS [Format_settings__GOP],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Video"]/Format_Settings_Wrapping)[1]','nvarchar(2000)'),'') AS [Format_Settings_Wrapping],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Video"]/Duration)[1]','nvarchar(2000)'),'') AS [Duration_V],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Video"]/Bit_rate)[1]','nvarchar(2000)'),'') AS [Bit_rate],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Video"]/Maximum_bit_rate)[1]','nvarchar(2000)'),'') AS [Maximum_bit_rate],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Video"]/Width)[1]','nvarchar(2000)'),'') AS [Width],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Video"]/Height)[1]','nvarchar(2000)'),'') AS [Height],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Video"]/Original_height)[1]','nvarchar(2000)'),'') AS [Original_height],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Video"]/Display_aspect_ratio)[1]','nvarchar(2000)'),'') AS [Display_aspect_ratio],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Video"]/Frame_rate)[1]','nvarchar(2000)'),'') AS [Frame_rate],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Video"]/Standard)[1]','nvarchar(2000)'),'') AS [Standard],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Video"]/Color_space)[1]','nvarchar(2000)'),'') AS [Color_space],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Video"]/Chroma_subsampling)[1]','nvarchar(2000)'),'') AS [Chroma_subsampling],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Video"]/Bit_depth)[1]','nvarchar(2000)'),'') AS [Bit_depth],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Video"]/Scan_type)[1]','nvarchar(2000)'),'') AS [Scan_type],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Video"]/Scan_order)[1]','nvarchar(2000)'),'') AS [Scan_order],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Video"]/Compression_mode)[1]','nvarchar(2000)'),'') AS [Compression_mode],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Video"]/Bits__Pixel_Frame_)[1]','nvarchar(2000)'),'') AS [Bits__Pixel_Frame_],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Video"]/Stream_size)[1]','nvarchar(2000)'),'') AS [Stream_size],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Video"]/Delay_SDTI)[1]','nvarchar(2000)'),'') AS [Delay_SDTI],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Video"]/Color_primaries)[1]','nvarchar(2000)'),'') AS [Color_primaries],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Video"]/Transfer_characteristics)[1]','nvarchar(2000)'),'') AS [Transfer_characteristics],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Video"]/Matrix_coefficients)[1]','nvarchar(2000)'),'') AS [Matrix_coefficients],


												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Audio"]/ID)[1]','nvarchar(2000)'),'') AS [ID_A],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Audio"]/Format)[1]','nvarchar(2000)'),'') AS [Format_A],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Audio"]/Format_settings__Endianness)[1]','nvarchar(2000)'),'') AS [Format_settings__Endianness],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Audio"]/Format_Settings_Wrapping)[1]','nvarchar(2000)'),'') AS [Format_Settings_Wrapping_A],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Audio"]/Duration)[1]','nvarchar(2000)'),'') AS [Duration_A],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Audio"]/Bit_rate_mode)[1]','nvarchar(2000)'),'') AS [Bit_rate_mode_A],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Audio"]/Bit_rate)[1]','nvarchar(2000)'),'') AS [Bit_rate_A],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Audio"]/Channel_s_)[1]','nvarchar(2000)'),'') AS [Channel_s_],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Audio"]/Sampling_rate)[1]','nvarchar(2000)'),'') AS [Sampling_rate],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Audio"]/Bit_depth)[1]','nvarchar(2000)'),'') AS [Bit_depth_A],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Audio"]/Stream_size)[1]','nvarchar(2000)'),'') AS [Stream_size_A],
												ISNULL(CONVERT(XML,ISNULL([TBLOG_VIDEO].[FSVIDEO_PROPERTY],'')).value('(/Mediainfo/File/track[@type="Audio"]/Delay_SDTI)[1]','nvarchar(2000)'),'') AS [Delay_SDTI_A]
											FROM
												[TBLOG_VIDEO]
										) AS A ON [TBLOG_VIDEO].[FSFILE_NO] = A.FSFILE_NO
		WHERE
			[TBLOG_VIDEO].[FSTYPE] = 'G' AND
			[TBLOG_VIDEO].[FCFILE_STATUS] IN ('T','Y') AND
			(@FSPROG_ID = '' OR [TBPROG_M].[FSPROG_ID] = @FSPROG_ID) AND
			(@FNBEG_EPISODE = 0 OR [TBPROG_D].[FNEPISODE] >= @FNBEG_EPISODE) AND
			(@FNEND_EPISODE = 0 OR [TBPROG_D].[FNEPISODE] <= @FNEND_EPISODE) AND
			(@FSCHANNEL_ID = '' OR [TBLOG_VIDEO].[FSCHANNEL_ID] = @FSCHANNEL_ID)

		SELECT '' AS [fsRESULT]
	END TRY
	BEGIN CATCH

		SELECT 'ERROR' AS [fsRESULT]

	END CATCH
END						

GO
