USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Mike.Lin>
-- Create date:   <2012/10/25>
-- Description:   �ק�u�a����
-- =============================================
CREATE Proc [dbo].[SP_U_TBPGM_PROMO_DUR]
@FSPROMO_ID	VARCHAR(11),
@FSDURATION	VARCHAR(11)
As
update TBPGM_PROMO set FSDURATION= @FSDURATION
where FSPROMO_ID=@FSPROMO_ID
GO
