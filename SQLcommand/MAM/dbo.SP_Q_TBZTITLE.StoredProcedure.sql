USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Q_TBZTITLE]

@FSTITLEID char(2)
As
SELECT * FROM MAM.dbo.TBZTITLE
WHERE MAM.dbo.TBZTITLE.FSTITLEID=@FSTITLEID
GO
