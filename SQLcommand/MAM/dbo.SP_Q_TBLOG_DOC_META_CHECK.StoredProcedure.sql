USE [MAM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Q_TBLOG_DOC_META_CHECK]

@FSID varchar(11) ,
@FNEPISODE smallint,
@FSMETA_PATH nvarchar(500)

As
SELECT * FROM MAM.dbo.TBLOG_DOC
WHERE MAM.dbo.TBLOG_DOC.FSTYPE='G'
AND MAM.dbo.TBLOG_DOC.FSID=@FSID
AND MAM.dbo.TBLOG_DOC.FNEPISODE=@FNEPISODE
AND MAM.dbo.TBLOG_DOC.FSMETA_PATH=@FSMETA_PATH
AND MAM.dbo.TBLOG_DOC.FCFILE_STATUS <>'D'
GO
