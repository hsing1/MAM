﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.IO;
using System.Web;

namespace PTS_MAM3.Web.WCF
{
    [DataContract]
    public class FileDownload
    {
        [DataMember]
        public string FileName { get; set; }
        [DataMember]
        public byte[] File { get; set; }
    }

    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class WCF100
    {
        [OperationContract]
        public FileDownload Download(string path)
        {

            string aaa = HttpContext.Current.Server.MapPath("~/UploadFolder/");

            FileStream fileStream = null;
            BinaryReader reader = null;
            string filePath;
            byte[] fileBytes;
            if (path.Substring(0, 2) != "\\\\" && path.Substring(0, 2) != "\\")
                path = aaa + path;

            try
            {
                filePath = path;
                //if (File.Exists(filePath))
                //{
                    fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                    reader = new BinaryReader(fileStream);
                    fileBytes = reader.ReadBytes((int)fileStream.Length);
                    fileStream.Close();
                    return new FileDownload() { FileName = "", File = fileBytes };
                //}
                return null;
            }
            catch (Exception)
            {
                return null;
            }
            // 在此新增您的作業實作
        }

        // 在此新增其他作業，並以 [OperationContract] 來標示它們
    }
}
