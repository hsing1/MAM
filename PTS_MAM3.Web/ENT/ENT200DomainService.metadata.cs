﻿
namespace PTS_MAM3.Web.ENT
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.ServiceModel.DomainServices.Hosting;
    using System.ServiceModel.DomainServices.Server;


    // MetadataTypeAttribute 會將 TBARCHIVE_SET_CLASSMetadata 識別為
    // 帶有 TBARCHIVE_SET_CLASS 類別其他中繼資料的類別。
    [MetadataTypeAttribute(typeof(TBARCHIVE_SET_CLASS.TBARCHIVE_SET_CLASSMetadata))]
    public partial class TBARCHIVE_SET_CLASS
    {

        // 這個類別可讓您將自訂屬性 (Attribute) 附加到 TBARCHIVE_SET_CLASS 類別
        // 的 properties。
        //
        // 例如，下列程式碼將 Xyz 屬性標記為
        // 必要的屬性，並指定有效值的格式:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class TBARCHIVE_SET_CLASSMetadata
        {

            // 中繼資料類別本就不應該具現化。
            private TBARCHIVE_SET_CLASSMetadata()
            {
            }

            public int FIID { get; set; }

            public string FSAUDIO { get; set; }

            public string FSCLASSNAME { get; set; }

            public string FSDOC { get; set; }

            public string FSPHOTO { get; set; }

            public string FSVIDEO { get; set; }
        }
    }
}
