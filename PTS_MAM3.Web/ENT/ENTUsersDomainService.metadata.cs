﻿
namespace PTS_MAM3.Web.ENT
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.ServiceModel.DomainServices.Hosting;
    using System.ServiceModel.DomainServices.Server;


    // The MetadataTypeAttribute identifies TBUSER_DEPMetadata as the class
    // that carries additional metadata for the TBUSER_DEP class.
    [MetadataTypeAttribute(typeof(TBUSER_DEP.TBUSER_DEPMetadata))]
    public partial class TBUSER_DEP
    {

        // This class allows you to attach custom attributes to properties
        // of the TBUSER_DEP class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class TBUSER_DEPMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private TBUSER_DEPMetadata()
            {
            }

            public string FBIsDepExist { get; set; }

            public int FNDEP_ID { get; set; }

            public Nullable<int> FNDEP_LEVEL { get; set; }

            public Nullable<int> FNPARENT_DEP_ID { get; set; }

            public Nullable<int> FNUpperDep_ID { get; set; }

            public string FSDEP { get; set; }

            public string FSDEP_MEMO { get; set; }

            public string FSDpeName_ChtName { get; set; }

            public string FSFullName_ChtName { get; set; }

            public string FSSupervisor_ID { get; set; }

            public string FSUpDepName_ChtName { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies TBUSER_GROUPMetadata as the class
    // that carries additional metadata for the TBUSER_GROUP class.
    [MetadataTypeAttribute(typeof(TBUSER_GROUP.TBUSER_GROUPMetadata))]
    public partial class TBUSER_GROUP
    {

        // This class allows you to attach custom attributes to properties
        // of the TBUSER_GROUP class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class TBUSER_GROUPMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private TBUSER_GROUPMetadata()
            {
            }

            public DateTime FDCREATED_DATE { get; set; }

            public Nullable<DateTime> FDUPDATED_DATE { get; set; }

            public string FSCREATED_BY { get; set; }

            public string FSGROUP_ID { get; set; }

            public string FSUPDATED_BY { get; set; }

            public string FSUSER_ID { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies TBUSERSMetadata as the class
    // that carries additional metadata for the TBUSERS class.
    [MetadataTypeAttribute(typeof(TBUSERS.TBUSERSMetadata))]
    public partial class TBUSERS
    {

        // This class allows you to attach custom attributes to properties
        // of the TBUSERS class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class TBUSERSMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private TBUSERSMetadata()
            {
            }

            public string FCACTIVE { get; set; }

            public string FCSECRET { get; set; }

            public DateTime FDCREATED_DATE { get; set; }

            public Nullable<DateTime> FDUPDATED_DATE { get; set; }

            public Nullable<int> FNDep_ID { get; set; }

            public Nullable<int> FNGroup_ID { get; set; }

            public Nullable<int> FNUpperDept_ID { get; set; }

            public string FSCREATED_BY { get; set; }

            public string FSDep_ChtName { get; set; }

            public string FSDEPT { get; set; }

            public string FSDESCRIPTION { get; set; }

            public string FSEMAIL { get; set; }

            public string FSGroup_ChtName { get; set; }

            public string FSIs_Main_Title { get; set; }

            public string FSIs_Supervisor { get; set; }

            public string FSPASSWD { get; set; }

            public string FSUPDATED_BY { get; set; }

            public string FSUpperDept_ChtName { get; set; }

            public string FSUSER { get; set; }

            public string FSUSER_ChtName { get; set; }

            public string FSUSER_ID { get; set; }

            public string FSUSER_TITLE { get; set; }
        }
    }
}
