﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using PTS_MAM3.Web.ENT;
using System.ComponentModel;
using System.Data;
using System.ServiceModel.DomainServices.EntityFramework;
using System.ServiceModel.DomainServices.Hosting;
using System.ServiceModel.DomainServices.Server;

namespace PTS_MAM3.Web.ENT
{
 
        
        
        public class TBJoinClass
        {
            [Key]
            public string FSPROGBUYID { get; set; }
            public string FSPROGBUYNAME { get; set; }
            [Key]
            public string FSPROGBUYDID { get; set; }
            public string FSPROGBUYDNAME { get; set; }
            public string FSSORT { get; set; }
            public string FSCREATED_BY { get; set; }
            public DateTime FDCREATED_DATE { get; set; }
            public string FSUPDATED_BY { get; set; }
            public Nullable<DateTime> FDUPDATED_DATE { get; set; }
            public bool FBISENABLE { get; set; }
            //public string getName(string id)
            //{
            //    string name = ObjectContext.TBZPROGBUYD.Where(s => s.FSPROGBUYDID == id).FirstOrDefault().FSPROGBUYDNAME;

            //    return name;
            //}




        }

     
}