﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PTS_MAM3.Web.ENT
{
    public class TBZPROMOCATDJoinClass
    {
        [Key]
        public string FSPROMOCATID { get; set; }
        public string FSPROMOCATNAME { get; set; }
        [Key]
        public string FSPROMOCATDID { get; set; }
        public string FSPROMOCATDNAME { get; set; }
        public string FSSORT { get; set; }
        public string FSCREATED_BY { get; set; }
        public DateTime FDCREATED_DATE { get; set; }
        public string FSUPDATED_BY { get; set; }
        public Nullable<DateTime> FDUPDATED_DATE { get; set; }
        public bool FBISENABLE { get; set; }
    }
}