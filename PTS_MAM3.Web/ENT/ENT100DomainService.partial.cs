﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//新增的
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.ServiceModel.DomainServices.EntityFramework;
using System.ServiceModel.DomainServices.Hosting;
using System.ServiceModel.DomainServices.Server;

namespace PTS_MAM3.Web.ENT
{
    public partial class ENT100DomainService
    {
        public string getName(string id) 
        {
            string name = ObjectContext.TBZPROGBUYD.Where(s => s.FSPROGBUYDID == id).FirstOrDefault().FSPROGBUYDNAME;
            
            return name;
        }

        #region 代碼檔查詢

        #endregion
        
        [Query]
        public IQueryable<TBZPROMOCATDJoinClass> GetJoinTBZPROMOCATDQuery()
        {
            var dataset = from pv in ObjectContext.TBZPROMOCAT
                          join pp in ObjectContext.TBZPROMOCATD
                          on pv.FSPROMOCATID equals pp.FSPROMOCATID
                          select new TBZPROMOCATDJoinClass()
                          {
                              FSPROMOCATDID = pp.FSPROMOCATDID,
                              FSPROMOCATNAME = pv.FSPROMOCATNAME,
                              FSPROMOCATID = pp.FSPROMOCATID,
                              FSPROMOCATDNAME = pp.FSPROMOCATDNAME,
                              FSSORT = pp.FSSORT,
                              FSCREATED_BY = pp.FSCREATED_BY,
                              FDCREATED_DATE = pp.FDCREATED_DATE,
                              FSUPDATED_BY = pp.FSUPDATED_BY,
                              FDUPDATED_DATE = pp.FDUPDATED_DATE,
                              FBISENABLE=(bool)pp.FBISENABLE
                          };
            return dataset;


        }

        [Query]
        public IQueryable<TBJoinClass> GetJoinTableBUYIDQuery()
        {
            #region 可以用的
            //var data1 = from pp in ObjectContext.TBZPROGBUYD
            //            select new TBJoinClass()
            //              {
            //                  FSPROGBUYID = pp.FSPROGBUYID,
            //                  FSPROGBUYDID = pp.FSPROGBUYDID,
            //                  FSPROGBUYDNAME = pp.FSPROGBUYDNAME,
            //                  FSSORT = pp.FSSORT,
            //                  FSCREATED_BY = pp.FSCREATED_BY,
            //                  FDCREATED_DATE = pp.FDCREATED_DATE,
            //                  FSUPDATED_BY = pp.FSUPDATED_BY,
            //                  FDUPDATED_DATE = pp.FDUPDATED_DATE,
            //              };
            //List<TBJoinClass> TBList = new List<TBJoinClass>();
            //foreach (TBJoinClass elem in data1)
            //{
            //    string name = ObjectContext.TBZPROGBUY.Where(s => s.FSPROGBUYID == elem.FSPROGBUYID).FirstOrDefault().FSPROGBUYNAME;
            //    TBJoinClass TJC = new TBJoinClass();
            //    TJC.FSPROGBUYID = elem.FSPROGBUYID;
            //    TJC.FSPROGBUYNAME = name;
            //    TJC.FSPROGBUYDID = elem.FSPROGBUYDID;
            //    TJC.FSPROGBUYDNAME = elem.FSPROGBUYDNAME;
            //    TJC.FSSORT = elem.FSSORT;
            //    TJC.FSCREATED_BY = elem.FSCREATED_BY;
            //    TJC.FDCREATED_DATE = elem.FDCREATED_DATE;
            //    TJC.FSUPDATED_BY = elem.FSUPDATED_BY;
            //    TJC.FDUPDATED_DATE = elem.FDUPDATED_DATE;
            //    TBList.Add(TJC);

            //}
            //return TBList;  
            #endregion

            var dataset = from pv in ObjectContext.TBZPROGBUY
                          join pp in ObjectContext.TBZPROGBUYD
                          on pv.FSPROGBUYID equals pp.FSPROGBUYID

                          //group pp by pp.FSPROGBUYDID into c
                          //select new TBJoinClass() 
                          //{
                          //    FSPROGBUYDID=c.Key,

                          //};
                          //into g

                          //select g;

                          select new TBJoinClass()
                          {
                              FSPROGBUYID = pp.FSPROGBUYID,
                              FSPROGBUYNAME = pv.FSPROGBUYNAME,
                              FSPROGBUYDID = pp.FSPROGBUYDID,
                              FSPROGBUYDNAME = pp.FSPROGBUYDNAME,
                              FSSORT = pp.FSSORT,
                              FSCREATED_BY = pp.FSCREATED_BY,
                              FDCREATED_DATE = pp.FDCREATED_DATE,
                              FSUPDATED_BY = pp.FSUPDATED_BY,
                              FDUPDATED_DATE = pp.FDUPDATED_DATE,
                          };
            return dataset;
            //var a = dataset.Select(s => s.FSPROGBUYDID).Distinct();
                          //orderby pp.FSSORT
                          //orderby g.Key
                          //select g;
       
        }

        //[Invoke]
        //public List<GetSP_Q_TBARCHIVE_SET_Result> GetTBARCHIVE_SET_SP()
        //{

        //    var dataset = ObjectContext.GetSP_Q_TBARCHIVE_SET();
        //    return dataset.ToList();
        //    //SP_Q_TBARCHIVE_SET_Result sp=new SP_Q_TBARCHIVE_SET_Result();
        //    //var dts = SP_Q_TBARCHIVE_SET_Result.CreateSP_Q_TBARCHIVE_SET_Result;
        //    ////var dataset=from p in SP_Q_TBARCHIVE_SET_Result.CreateSP_Q_TBARCHIVE_SET_Result
        //    ////            select new SP_Q_TBARCHIVE_SET_Result()
        //    ////            {}
        //}
        [Update]
        public void GetJoinTBZPROMOCATDUpdate(TBZPROMOCATDJoinClass TJC)
        {
            TBZPROMOCATD TBZDB = ObjectContext.TBZPROMOCATD.Where(s => (s.FSPROMOCATDID == TJC.FSPROMOCATDID && s.FSPROMOCATID == TJC.FSPROMOCATID)).FirstOrDefault();//.Where(o=>o.FSPROGBUYID==TJC.FSPROGBUYID)
            TBZDB.FSPROMOCATID = TJC.FSPROMOCATID;
            TBZDB.FSCREATED_BY = TJC.FSUPDATED_BY;
            TBZDB.FBISENABLE = TJC.FBISENABLE;
        }

        [Update]
        public void GetJoinTableBUYIDUpdate(TBJoinClass TJC)
        {
            TBZPROGBUYD TBZDB = ObjectContext.TBZPROGBUYD.Where(s => s.FSPROGBUYDID == TJC.FSPROGBUYDID && s.FSPROGBUYID == TJC.FSPROGBUYID).FirstOrDefault();//.Where(o=>o.FSPROGBUYID==TJC.FSPROGBUYID)
            TBZDB.FSPROGBUYID = TJC.FSPROGBUYID;
            TBZDB.FSCREATED_BY = TJC.FSUPDATED_BY;
            TBZDB.FBISENABLE = TJC.FBISENABLE;
        }
        [Insert]
        public void GetJoinTBZPROMOCATDInsert(TBZPROMOCATDJoinClass TJC)
        {
            TBZPROMOCATD TBZDB = new TBZPROMOCATD();
            TBZDB.FSPROMOCATDID = TJC.FSPROMOCATDID;
            TBZDB.FSPROMOCATID = TJC.FSPROMOCATID;
            TBZDB.FSPROMOCATDNAME = TJC.FSPROMOCATDNAME;
            TBZDB.FSSORT = TJC.FSSORT;
            TBZDB.FSCREATED_BY = TJC.FSCREATED_BY;
            TBZDB.FSUPDATED_BY = TJC.FSUPDATED_BY;
            TBZDB.FDCREATED_DATE = TJC.FDCREATED_DATE;
            TBZDB.FDUPDATED_DATE = TJC.FDUPDATED_DATE;
            TBZDB.FBISENABLE = true;
            
            ObjectContext.TBZPROMOCATD.AddObject(TBZDB);
            try
            {
                ObjectContext.SaveChanges();
            }
            catch (Exception ex)
            { string except = ex.ToString(); }
        }

        [Insert]
        public void GetJoinTableBUYIDInsert(TBJoinClass TJC)
        {
            TBZPROGBUYD TBZDB = new TBZPROGBUYD();
            TBZDB.FSPROGBUYDID = TJC.FSPROGBUYDID;
            TBZDB.FSPROGBUYID = TJC.FSPROGBUYID;
            TBZDB.FSPROGBUYDNAME = TJC.FSPROGBUYDNAME;
            TBZDB.FSSORT = TJC.FSSORT;
            TBZDB.FSCREATED_BY = TJC.FSCREATED_BY;
            TBZDB.FSUPDATED_BY = TJC.FSUPDATED_BY;
            TBZDB.FDCREATED_DATE = TJC.FDCREATED_DATE;
            TBZDB.FDUPDATED_DATE = TJC.FDUPDATED_DATE;
            TBZDB.FBISENABLE = TJC.FBISENABLE;
            
            ObjectContext.TBZPROGBUYD.AddObject(TBZDB);
            ObjectContext.SaveChanges();
        }

        [Delete]
        public void GetJoinTableBUYIDDelete(TBZPROMOCATDJoinClass TJC)
        {
            TBZPROMOCATD TBZDB = ObjectContext.TBZPROMOCATD.Where(s => s.FSPROMOCATDID == TJC.FSPROMOCATDID && s.FSPROMOCATID == TJC.FSPROMOCATID).FirstOrDefault();
            ObjectContext.TBZPROMOCATD.DeleteObject(TBZDB);
            ObjectContext.SaveChanges();
        }

        [Delete]
        public void GetJoinTableBUYIDDelete(TBJoinClass TJC)
        {
            TBZPROGBUYD TBZDB = ObjectContext.TBZPROGBUYD.Where(s => s.FSPROGBUYDID == TJC.FSPROGBUYDID && s.FSPROGBUYID == TJC.FSPROGBUYID).FirstOrDefault();
            ObjectContext.TBZPROGBUYD.DeleteObject(TBZDB);
            ObjectContext.SaveChanges();
        }

        //[Delete]
        //public void DelTBFILE_TYPE_Partial(TBFILE_TYPE DelTB)
        //{
        //    using (ENT100Entities E100 = new ENT100Entities())
        //    {
        //        E100.TBFILE_TYPE.DeleteObject(DelTB);
        //        E100.SaveChanges();
        //    }
        //    //ObjectContext.TBFILE_TYPE.DeleteObject(DelTB);
        //    //ObjectContext.SaveChanges();
        //}
    }
}