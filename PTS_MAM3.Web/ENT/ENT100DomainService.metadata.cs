﻿
namespace PTS_MAM3.Web.ENT
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Data.Objects.DataClasses;
    using System.Linq;
    using System.ServiceModel.DomainServices.Hosting;
    using System.ServiceModel.DomainServices.Server;


    // MetadataTypeAttribute 會將 TBFILE_TYPEMetadata 識別為
    // 帶有 TBFILE_TYPE 類別其他中繼資料的類別。
    [MetadataTypeAttribute(typeof(TBFILE_TYPE.TBFILE_TYPEMetadata))]
    public partial class TBFILE_TYPE
    {

        // 這個類別可讓您將自訂屬性 (Attribute) 附加到 TBFILE_TYPE 類別
        // 的 properties。
        //
        // 例如，下列程式碼將 Xyz 屬性標記為
        // 必要的屬性，並指定有效值的格式:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class TBFILE_TYPEMetadata
        {

            // 中繼資料類別本就不應該具現化。
            private TBFILE_TYPEMetadata()
            {
            }

            public Nullable<bool> FBISENABLE { get; set; }

            public DateTime FDCREATED_DATE { get; set; }

            public Nullable<DateTime> FDUPDATED_DATE { get; set; }

            public string FSARC_TYPE { get; set; }

            public string FSCREATED_BY { get; set; }

            public string FSSORT { get; set; }

            public string FSSPEC { get; set; }

            public string FSTAPE_NOTE { get; set; }

            public string FSTYPE { get; set; }

            public string FSTYPE_NAME { get; set; }

            public string FSUPDATED_BY { get; set; }

            public TBFILE_TYPE TBFILE_TYPE1 { get; set; }

            public TBFILE_TYPE TBFILE_TYPE2 { get; set; }

            public EntityCollection<TBLOG_AUDIO> TBLOG_AUDIO { get; set; }
        }
    }

    // MetadataTypeAttribute 會將 TBLOG_AUDIOMetadata 識別為
    // 帶有 TBLOG_AUDIO 類別其他中繼資料的類別。
    [MetadataTypeAttribute(typeof(TBLOG_AUDIO.TBLOG_AUDIOMetadata))]
    public partial class TBLOG_AUDIO
    {

        // 這個類別可讓您將自訂屬性 (Attribute) 附加到 TBLOG_AUDIO 類別
        // 的 properties。
        //
        // 例如，下列程式碼將 Xyz 屬性標記為
        // 必要的屬性，並指定有效值的格式:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class TBLOG_AUDIOMetadata
        {

            // 中繼資料類別本就不應該具現化。
            private TBLOG_AUDIOMetadata()
            {
            }

            public string FCFILE_STATUS { get; set; }

            public DateTime FDCREATED_DATE { get; set; }

            public Nullable<DateTime> FDUPDATED_DATE { get; set; }

            public Nullable<long> FNDIR_ID { get; set; }

            public short FNEPISODE { get; set; }

            public string FSARC_ID { get; set; }

            public string FSARC_TYPE { get; set; }

            public string FSATTRIBUTE1 { get; set; }

            public string FSATTRIBUTE10 { get; set; }

            public string FSATTRIBUTE11 { get; set; }

            public string FSATTRIBUTE12 { get; set; }

            public string FSATTRIBUTE13 { get; set; }

            public string FSATTRIBUTE14 { get; set; }

            public string FSATTRIBUTE15 { get; set; }

            public string FSATTRIBUTE16 { get; set; }

            public string FSATTRIBUTE17 { get; set; }

            public string FSATTRIBUTE18 { get; set; }

            public string FSATTRIBUTE19 { get; set; }

            public string FSATTRIBUTE2 { get; set; }

            public string FSATTRIBUTE20 { get; set; }

            public string FSATTRIBUTE21 { get; set; }

            public string FSATTRIBUTE22 { get; set; }

            public string FSATTRIBUTE23 { get; set; }

            public string FSATTRIBUTE24 { get; set; }

            public string FSATTRIBUTE25 { get; set; }

            public string FSATTRIBUTE26 { get; set; }

            public string FSATTRIBUTE27 { get; set; }

            public string FSATTRIBUTE28 { get; set; }

            public string FSATTRIBUTE29 { get; set; }

            public string FSATTRIBUTE3 { get; set; }

            public string FSATTRIBUTE30 { get; set; }

            public string FSATTRIBUTE31 { get; set; }

            public string FSATTRIBUTE32 { get; set; }

            public string FSATTRIBUTE33 { get; set; }

            public string FSATTRIBUTE34 { get; set; }

            public string FSATTRIBUTE35 { get; set; }

            public string FSATTRIBUTE36 { get; set; }

            public string FSATTRIBUTE37 { get; set; }

            public string FSATTRIBUTE38 { get; set; }

            public string FSATTRIBUTE39 { get; set; }

            public string FSATTRIBUTE4 { get; set; }

            public string FSATTRIBUTE40 { get; set; }

            public string FSATTRIBUTE41 { get; set; }

            public string FSATTRIBUTE42 { get; set; }

            public string FSATTRIBUTE43 { get; set; }

            public string FSATTRIBUTE44 { get; set; }

            public string FSATTRIBUTE45 { get; set; }

            public string FSATTRIBUTE46 { get; set; }

            public string FSATTRIBUTE47 { get; set; }

            public string FSATTRIBUTE48 { get; set; }

            public string FSATTRIBUTE49 { get; set; }

            public string FSATTRIBUTE5 { get; set; }

            public string FSATTRIBUTE50 { get; set; }

            public string FSATTRIBUTE6 { get; set; }

            public string FSATTRIBUTE7 { get; set; }

            public string FSATTRIBUTE8 { get; set; }

            public string FSATTRIBUTE9 { get; set; }

            public string FSCHANGE_FILE_NO { get; set; }

            public string FSCHANNEL_ID { get; set; }

            public string FSCREATED_BY { get; set; }

            public string FSDESCRIPTION { get; set; }

            public string FSFILE_NO { get; set; }

            public string FSFILE_PATH { get; set; }

            public string FSFILE_SIZE { get; set; }

            public string FSFILE_TYPE { get; set; }

            public string FSID { get; set; }

            public string FSOLD_FILE_NAME { get; set; }

            public string FSSUBJECT_ID { get; set; }

            public string FSSUPERVISOR { get; set; }

            public string FSTITLE { get; set; }

            public string FSTYPE { get; set; }

            public string FSUPDATED_BY { get; set; }

            public TBFILE_TYPE TBFILE_TYPE { get; set; }
        }
    }

    // MetadataTypeAttribute 會將 TBPGM_ACTIONMetadata 識別為
    // 帶有 TBPGM_ACTION 類別其他中繼資料的類別。
    [MetadataTypeAttribute(typeof(TBPGM_ACTION.TBPGM_ACTIONMetadata))]
    public partial class TBPGM_ACTION
    {

        // 這個類別可讓您將自訂屬性 (Attribute) 附加到 TBPGM_ACTION 類別
        // 的 properties。
        //
        // 例如，下列程式碼將 Xyz 屬性標記為
        // 必要的屬性，並指定有效值的格式:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class TBPGM_ACTIONMetadata
        {

            // 中繼資料類別本就不應該具現化。
            private TBPGM_ACTIONMetadata()
            {
            }

            public Nullable<bool> FBISENABLE { get; set; }

            public DateTime FDCREATED_DATE { get; set; }

            public Nullable<DateTime> FDUPDATED_DATE { get; set; }

            public string FSACTIONID { get; set; }

            public string FSACTIONNAME { get; set; }

            public string FSCREATED_BY { get; set; }

            public string FSSORT { get; set; }

            public string FSUPDATED_BY { get; set; }
        }
    }

    // MetadataTypeAttribute 會將 TBZBOOKING_REASONMetadata 識別為
    // 帶有 TBZBOOKING_REASON 類別其他中繼資料的類別。
    [MetadataTypeAttribute(typeof(TBZBOOKING_REASON.TBZBOOKING_REASONMetadata))]
    public partial class TBZBOOKING_REASON
    {

        // 這個類別可讓您將自訂屬性 (Attribute) 附加到 TBZBOOKING_REASON 類別
        // 的 properties。
        //
        // 例如，下列程式碼將 Xyz 屬性標記為
        // 必要的屬性，並指定有效值的格式:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class TBZBOOKING_REASONMetadata
        {

            // 中繼資料類別本就不應該具現化。
            private TBZBOOKING_REASONMetadata()
            {
            }

            public Nullable<bool> FBISENABLE { get; set; }

            public DateTime FDCREATED_DATE { get; set; }

            public Nullable<DateTime> FDUPDATED_DATE { get; set; }

            public string FSCREATED_BY { get; set; }

            public string FSNO { get; set; }

            public string FSREASON { get; set; }

            public string FSSORT { get; set; }

            public string FSUPDATED_BY { get; set; }
        }
    }

    // MetadataTypeAttribute 會將 TBZCHANNELMetadata 識別為
    // 帶有 TBZCHANNEL 類別其他中繼資料的類別。
    [MetadataTypeAttribute(typeof(TBZCHANNEL.TBZCHANNELMetadata))]
    public partial class TBZCHANNEL
    {

        // 這個類別可讓您將自訂屬性 (Attribute) 附加到 TBZCHANNEL 類別
        // 的 properties。
        //
        // 例如，下列程式碼將 Xyz 屬性標記為
        // 必要的屬性，並指定有效值的格式:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class TBZCHANNELMetadata
        {

            // 中繼資料類別本就不應該具現化。
            private TBZCHANNELMetadata()
            {
            }

            public Nullable<bool> FBISENABLE { get; set; }

            public DateTime FDCREATED_DATE { get; set; }

            public Nullable<DateTime> FDUPDATED_DATE { get; set; }

            public string FSCHANNEL_ID { get; set; }

            public string FSCHANNEL_NAME { get; set; }

            public string FSCHANNEL_TYPE { get; set; }

            public string FSCREATED_BY { get; set; }

            public string FSOLDTABLENAME { get; set; }

            public string FSSHORTNAME { get; set; }

            public string FSSHOWNAME { get; set; }

            public string FSSORT { get; set; }

            public string FSTSM_POOLNAME { get; set; }

            public string FSUPDATED_BY { get; set; }
        }
    }

    // MetadataTypeAttribute 會將 TBZDEPTMetadata 識別為
    // 帶有 TBZDEPT 類別其他中繼資料的類別。
    [MetadataTypeAttribute(typeof(TBZDEPT.TBZDEPTMetadata))]
    public partial class TBZDEPT
    {

        // 這個類別可讓您將自訂屬性 (Attribute) 附加到 TBZDEPT 類別
        // 的 properties。
        //
        // 例如，下列程式碼將 Xyz 屬性標記為
        // 必要的屬性，並指定有效值的格式:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class TBZDEPTMetadata
        {

            // 中繼資料類別本就不應該具現化。
            private TBZDEPTMetadata()
            {
            }

            public Nullable<bool> FBISENABLE { get; set; }

            public DateTime FDCREATED_DATE { get; set; }

            public Nullable<DateTime> FDUPDATED_DATE { get; set; }

            public string FSCREATED_BY { get; set; }

            public string FSDEPTID { get; set; }

            public string FSDEPTNAME { get; set; }

            public string FSSORT { get; set; }

            public string FSUPDATED_BY { get; set; }
        }
    }

    // MetadataTypeAttribute 會將 TBZPROGATTRMetadata 識別為
    // 帶有 TBZPROGATTR 類別其他中繼資料的類別。
    [MetadataTypeAttribute(typeof(TBZPROGATTR.TBZPROGATTRMetadata))]
    public partial class TBZPROGATTR
    {

        // 這個類別可讓您將自訂屬性 (Attribute) 附加到 TBZPROGATTR 類別
        // 的 properties。
        //
        // 例如，下列程式碼將 Xyz 屬性標記為
        // 必要的屬性，並指定有效值的格式:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class TBZPROGATTRMetadata
        {

            // 中繼資料類別本就不應該具現化。
            private TBZPROGATTRMetadata()
            {
            }

            public Nullable<bool> FBISENABLE { get; set; }

            public DateTime FDCREATED_DATE { get; set; }

            public Nullable<DateTime> FDUPDATED_DATE { get; set; }

            public string FSCREATED_BY { get; set; }

            public string FSPROGATTRID { get; set; }

            public string FSPROGATTRNAME { get; set; }

            public string FSSORT { get; set; }

            public string FSUPDATED_BY { get; set; }
        }
    }

    // MetadataTypeAttribute 會將 TBZPROGAUDMetadata 識別為
    // 帶有 TBZPROGAUD 類別其他中繼資料的類別。
    [MetadataTypeAttribute(typeof(TBZPROGAUD.TBZPROGAUDMetadata))]
    public partial class TBZPROGAUD
    {

        // 這個類別可讓您將自訂屬性 (Attribute) 附加到 TBZPROGAUD 類別
        // 的 properties。
        //
        // 例如，下列程式碼將 Xyz 屬性標記為
        // 必要的屬性，並指定有效值的格式:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class TBZPROGAUDMetadata
        {

            // 中繼資料類別本就不應該具現化。
            private TBZPROGAUDMetadata()
            {
            }

            public Nullable<bool> FBISENABLE { get; set; }

            public DateTime FDCREATED_DATE { get; set; }

            public Nullable<DateTime> FDUPDATED_DATE { get; set; }

            public string FSCREATED_BY { get; set; }

            public string FSPROGAUDID { get; set; }

            public string FSPROGAUDNAME { get; set; }

            public string FSSORT { get; set; }

            public string FSUPDATED_BY { get; set; }
        }
    }

    // MetadataTypeAttribute 會將 TBZPROGBUYMetadata 識別為
    // 帶有 TBZPROGBUY 類別其他中繼資料的類別。
    [MetadataTypeAttribute(typeof(TBZPROGBUY.TBZPROGBUYMetadata))]
    public partial class TBZPROGBUY
    {

        // 這個類別可讓您將自訂屬性 (Attribute) 附加到 TBZPROGBUY 類別
        // 的 properties。
        //
        // 例如，下列程式碼將 Xyz 屬性標記為
        // 必要的屬性，並指定有效值的格式:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class TBZPROGBUYMetadata
        {

            // 中繼資料類別本就不應該具現化。
            private TBZPROGBUYMetadata()
            {
            }

            public Nullable<bool> FBISENABLE { get; set; }

            public DateTime FDCREATED_DATE { get; set; }

            public Nullable<DateTime> FDUPDATED_DATE { get; set; }

            public string FSCREATED_BY { get; set; }

            public string FSPROGBUYID { get; set; }

            public string FSPROGBUYNAME { get; set; }

            public string FSSORT { get; set; }

            public string FSUPDATED_BY { get; set; }

            public EntityCollection<TBZPROGBUYD> TBZPROGBUYD { get; set; }
        }
    }

    // MetadataTypeAttribute 會將 TBZPROGBUYDMetadata 識別為
    // 帶有 TBZPROGBUYD 類別其他中繼資料的類別。
    [MetadataTypeAttribute(typeof(TBZPROGBUYD.TBZPROGBUYDMetadata))]
    public partial class TBZPROGBUYD
    {

        // 這個類別可讓您將自訂屬性 (Attribute) 附加到 TBZPROGBUYD 類別
        // 的 properties。
        //
        // 例如，下列程式碼將 Xyz 屬性標記為
        // 必要的屬性，並指定有效值的格式:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class TBZPROGBUYDMetadata
        {

            // 中繼資料類別本就不應該具現化。
            private TBZPROGBUYDMetadata()
            {
            }

            public Nullable<bool> FBISENABLE { get; set; }

            public DateTime FDCREATED_DATE { get; set; }

            public Nullable<DateTime> FDUPDATED_DATE { get; set; }

            public string FSCREATED_BY { get; set; }

            public string FSPROGBUYDID { get; set; }

            public string FSPROGBUYDNAME { get; set; }

            public string FSPROGBUYID { get; set; }

            public string FSSORT { get; set; }

            public string FSUPDATED_BY { get; set; }

            public TBZPROGBUY TBZPROGBUY { get; set; }
        }
    }

    // MetadataTypeAttribute 會將 TBZPROGGRADEMetadata 識別為
    // 帶有 TBZPROGGRADE 類別其他中繼資料的類別。
    [MetadataTypeAttribute(typeof(TBZPROGGRADE.TBZPROGGRADEMetadata))]
    public partial class TBZPROGGRADE
    {

        // 這個類別可讓您將自訂屬性 (Attribute) 附加到 TBZPROGGRADE 類別
        // 的 properties。
        //
        // 例如，下列程式碼將 Xyz 屬性標記為
        // 必要的屬性，並指定有效值的格式:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class TBZPROGGRADEMetadata
        {

            // 中繼資料類別本就不應該具現化。
            private TBZPROGGRADEMetadata()
            {
            }

            public Nullable<bool> FBISENABLE { get; set; }

            public DateTime FDCREATED_DATE { get; set; }

            public Nullable<DateTime> FDUPDATED_DATE { get; set; }

            public string FSCREATED_BY { get; set; }

            public string FSPROGGRADEID { get; set; }

            public string FSPROGGRADENAME { get; set; }

            public string FSSORT { get; set; }

            public string FSUPDATED_BY { get; set; }
        }
    }

    // MetadataTypeAttribute 會將 TBZPROGLANGMetadata 識別為
    // 帶有 TBZPROGLANG 類別其他中繼資料的類別。
    [MetadataTypeAttribute(typeof(TBZPROGLANG.TBZPROGLANGMetadata))]
    public partial class TBZPROGLANG
    {

        // 這個類別可讓您將自訂屬性 (Attribute) 附加到 TBZPROGLANG 類別
        // 的 properties。
        //
        // 例如，下列程式碼將 Xyz 屬性標記為
        // 必要的屬性，並指定有效值的格式:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class TBZPROGLANGMetadata
        {

            // 中繼資料類別本就不應該具現化。
            private TBZPROGLANGMetadata()
            {
            }

            public Nullable<bool> FBISENABLE { get; set; }

            public DateTime FDCREATED_DATE { get; set; }

            public Nullable<DateTime> FDUPDATED_DATE { get; set; }

            public string FSCREATED_BY { get; set; }

            public string FSPROGLANGID { get; set; }

            public string FSPROGLANGNAME { get; set; }

            public string FSSORT { get; set; }

            public string FSUPDATED_BY { get; set; }
        }
    }

    // MetadataTypeAttribute 會將 TBZPROGOBJMetadata 識別為
    // 帶有 TBZPROGOBJ 類別其他中繼資料的類別。
    [MetadataTypeAttribute(typeof(TBZPROGOBJ.TBZPROGOBJMetadata))]
    public partial class TBZPROGOBJ
    {

        // 這個類別可讓您將自訂屬性 (Attribute) 附加到 TBZPROGOBJ 類別
        // 的 properties。
        //
        // 例如，下列程式碼將 Xyz 屬性標記為
        // 必要的屬性，並指定有效值的格式:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class TBZPROGOBJMetadata
        {

            // 中繼資料類別本就不應該具現化。
            private TBZPROGOBJMetadata()
            {
            }

            public Nullable<bool> FBISENABLE { get; set; }

            public DateTime FDCREATED_DATE { get; set; }

            public Nullable<DateTime> FDUPDATED_DATE { get; set; }

            public string FSCREATED_BY { get; set; }

            public string FSPROGOBJID { get; set; }

            public string FSPROGOBJNAME { get; set; }

            public string FSSORT { get; set; }

            public string FSUPDATED_BY { get; set; }
        }
    }

    // MetadataTypeAttribute 會將 TBZPROGRACE_AREAMetadata 識別為
    // 帶有 TBZPROGRACE_AREA 類別其他中繼資料的類別。
    [MetadataTypeAttribute(typeof(TBZPROGRACE_AREA.TBZPROGRACE_AREAMetadata))]
    public partial class TBZPROGRACE_AREA
    {

        // 這個類別可讓您將自訂屬性 (Attribute) 附加到 TBZPROGRACE_AREA 類別
        // 的 properties。
        //
        // 例如，下列程式碼將 Xyz 屬性標記為
        // 必要的屬性，並指定有效值的格式:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class TBZPROGRACE_AREAMetadata
        {

            // 中繼資料類別本就不應該具現化。
            private TBZPROGRACE_AREAMetadata()
            {
            }

            public Nullable<bool> FBISENABLE { get; set; }

            public Nullable<DateTime> FDCREATED_DATE { get; set; }

            public Nullable<DateTime> FDUPDATED_DATE { get; set; }

            public string FSCREATED_BY { get; set; }

            public string FSID { get; set; }

            public string FSNAME { get; set; }

            public string FSSORT { get; set; }

            public string FSUPDATED_BY { get; set; }
        }
    }

    // MetadataTypeAttribute 會將 TBZPROGSPECMetadata 識別為
    // 帶有 TBZPROGSPEC 類別其他中繼資料的類別。
    [MetadataTypeAttribute(typeof(TBZPROGSPEC.TBZPROGSPECMetadata))]
    public partial class TBZPROGSPEC
    {

        // 這個類別可讓您將自訂屬性 (Attribute) 附加到 TBZPROGSPEC 類別
        // 的 properties。
        //
        // 例如，下列程式碼將 Xyz 屬性標記為
        // 必要的屬性，並指定有效值的格式:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class TBZPROGSPECMetadata
        {

            // 中繼資料類別本就不應該具現化。
            private TBZPROGSPECMetadata()
            {
            }

            public Nullable<bool> FBISENABLE { get; set; }

            public DateTime FDCREATED_DATE { get; set; }

            public Nullable<DateTime> FDUPDATED_DATE { get; set; }

            public string FSCREATED_BY { get; set; }

            public string FSPROGSPECID { get; set; }

            public string FSPROGSPECNAME { get; set; }

            public string FSSORT { get; set; }

            public string FSUPDATED_BY { get; set; }
        }
    }

    // MetadataTypeAttribute 會將 TBZPROGSRCMetadata 識別為
    // 帶有 TBZPROGSRC 類別其他中繼資料的類別。
    [MetadataTypeAttribute(typeof(TBZPROGSRC.TBZPROGSRCMetadata))]
    public partial class TBZPROGSRC
    {

        // 這個類別可讓您將自訂屬性 (Attribute) 附加到 TBZPROGSRC 類別
        // 的 properties。
        //
        // 例如，下列程式碼將 Xyz 屬性標記為
        // 必要的屬性，並指定有效值的格式:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class TBZPROGSRCMetadata
        {

            // 中繼資料類別本就不應該具現化。
            private TBZPROGSRCMetadata()
            {
            }

            public Nullable<bool> FBISENABLE { get; set; }

            public DateTime FDCREATED_DATE { get; set; }

            public Nullable<DateTime> FDUPDATED_DATE { get; set; }

            public string FSCREATED_BY { get; set; }

            public string FSPROGSRCID { get; set; }

            public string FSPROGSRCNAME { get; set; }

            public string FSSORT { get; set; }

            public string FSUPDATED_BY { get; set; }
        }
    }

    // MetadataTypeAttribute 會將 TBZPROGSTATUSMetadata 識別為
    // 帶有 TBZPROGSTATUS 類別其他中繼資料的類別。
    [MetadataTypeAttribute(typeof(TBZPROGSTATUS.TBZPROGSTATUSMetadata))]
    public partial class TBZPROGSTATUS
    {

        // 這個類別可讓您將自訂屬性 (Attribute) 附加到 TBZPROGSTATUS 類別
        // 的 properties。
        //
        // 例如，下列程式碼將 Xyz 屬性標記為
        // 必要的屬性，並指定有效值的格式:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class TBZPROGSTATUSMetadata
        {

            // 中繼資料類別本就不應該具現化。
            private TBZPROGSTATUSMetadata()
            {
            }

            public Nullable<bool> FBISENABLE { get; set; }

            public DateTime FDCREATED_DATE { get; set; }

            public Nullable<DateTime> FDUPDATED_DATE { get; set; }

            public string FSCREATED_BY { get; set; }

            public string FSPROGSTATUSID { get; set; }

            public string FSPROGSTATUSNAME { get; set; }

            public string FSSORT { get; set; }

            public string FSUPDATED_BY { get; set; }
        }
    }

    // MetadataTypeAttribute 會將 TBZPROGTYPEMetadata 識別為
    // 帶有 TBZPROGTYPE 類別其他中繼資料的類別。
    [MetadataTypeAttribute(typeof(TBZPROGTYPE.TBZPROGTYPEMetadata))]
    public partial class TBZPROGTYPE
    {

        // 這個類別可讓您將自訂屬性 (Attribute) 附加到 TBZPROGTYPE 類別
        // 的 properties。
        //
        // 例如，下列程式碼將 Xyz 屬性標記為
        // 必要的屬性，並指定有效值的格式:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class TBZPROGTYPEMetadata
        {

            // 中繼資料類別本就不應該具現化。
            private TBZPROGTYPEMetadata()
            {
            }

            public Nullable<bool> FBISENABLE { get; set; }

            public DateTime FDCREATED_DATE { get; set; }

            public Nullable<DateTime> FDUPDATED_DATE { get; set; }

            public string FSCREATED_BY { get; set; }

            public string FSPROGTYPEID { get; set; }

            public string FSPROGTYPENAME { get; set; }

            public string FSSORT { get; set; }

            public string FSUPDATED_BY { get; set; }
        }
    }

    // MetadataTypeAttribute 會將 TBZPROMOCATMetadata 識別為
    // 帶有 TBZPROMOCAT 類別其他中繼資料的類別。
    [MetadataTypeAttribute(typeof(TBZPROMOCAT.TBZPROMOCATMetadata))]
    public partial class TBZPROMOCAT
    {

        // 這個類別可讓您將自訂屬性 (Attribute) 附加到 TBZPROMOCAT 類別
        // 的 properties。
        //
        // 例如，下列程式碼將 Xyz 屬性標記為
        // 必要的屬性，並指定有效值的格式:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class TBZPROMOCATMetadata
        {

            // 中繼資料類別本就不應該具現化。
            private TBZPROMOCATMetadata()
            {
            }

            public Nullable<bool> FBISENABLE { get; set; }

            public DateTime FDCREATED_DATE { get; set; }

            public Nullable<DateTime> FDUPDATED_DATE { get; set; }

            public string FSCREATED_BY { get; set; }

            public string FSPROMOCATID { get; set; }

            public string FSPROMOCATNAME { get; set; }

            public string FSSORT { get; set; }

            public string FSUPDATED_BY { get; set; }

            public EntityCollection<TBZPROMOCATD> TBZPROMOCATD { get; set; }
        }
    }

    // MetadataTypeAttribute 會將 TBZPROMOCATDMetadata 識別為
    // 帶有 TBZPROMOCATD 類別其他中繼資料的類別。
    [MetadataTypeAttribute(typeof(TBZPROMOCATD.TBZPROMOCATDMetadata))]
    public partial class TBZPROMOCATD
    {

        // 這個類別可讓您將自訂屬性 (Attribute) 附加到 TBZPROMOCATD 類別
        // 的 properties。
        //
        // 例如，下列程式碼將 Xyz 屬性標記為
        // 必要的屬性，並指定有效值的格式:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class TBZPROMOCATDMetadata
        {

            // 中繼資料類別本就不應該具現化。
            private TBZPROMOCATDMetadata()
            {
            }

            public Nullable<bool> FBISENABLE { get; set; }

            public DateTime FDCREATED_DATE { get; set; }

            public Nullable<DateTime> FDUPDATED_DATE { get; set; }

            public string FSCREATED_BY { get; set; }

            public string FSPROMOCATDID { get; set; }

            public string FSPROMOCATDNAME { get; set; }

            public string FSPROMOCATID { get; set; }

            public string FSSORT { get; set; }

            public string FSUPDATED_BY { get; set; }

            public TBZPROMOCAT TBZPROMOCAT { get; set; }
        }
    }

    // MetadataTypeAttribute 會將 TBZPROMOTYPEMetadata 識別為
    // 帶有 TBZPROMOTYPE 類別其他中繼資料的類別。
    [MetadataTypeAttribute(typeof(TBZPROMOTYPE.TBZPROMOTYPEMetadata))]
    public partial class TBZPROMOTYPE
    {

        // 這個類別可讓您將自訂屬性 (Attribute) 附加到 TBZPROMOTYPE 類別
        // 的 properties。
        //
        // 例如，下列程式碼將 Xyz 屬性標記為
        // 必要的屬性，並指定有效值的格式:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class TBZPROMOTYPEMetadata
        {

            // 中繼資料類別本就不應該具現化。
            private TBZPROMOTYPEMetadata()
            {
            }

            public Nullable<bool> FBISENABLE { get; set; }

            public DateTime FDCREATED_DATE { get; set; }

            public Nullable<DateTime> FDUPDATED_DATE { get; set; }

            public string FSCREATED_BY { get; set; }

            public string FSPROMOTYPEID { get; set; }

            public string FSPROMOTYPENAME { get; set; }

            public string FSSORT { get; set; }

            public string FSUPDATED_BY { get; set; }
        }
    }

    // MetadataTypeAttribute 會將 TBZPROMOVERMetadata 識別為
    // 帶有 TBZPROMOVER 類別其他中繼資料的類別。
    [MetadataTypeAttribute(typeof(TBZPROMOVER.TBZPROMOVERMetadata))]
    public partial class TBZPROMOVER
    {

        // 這個類別可讓您將自訂屬性 (Attribute) 附加到 TBZPROMOVER 類別
        // 的 properties。
        //
        // 例如，下列程式碼將 Xyz 屬性標記為
        // 必要的屬性，並指定有效值的格式:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class TBZPROMOVERMetadata
        {

            // 中繼資料類別本就不應該具現化。
            private TBZPROMOVERMetadata()
            {
            }

            public Nullable<bool> FBISENABLE { get; set; }

            public DateTime FDCREATED_DATE { get; set; }

            public Nullable<DateTime> FDUPDATED_DATE { get; set; }

            public string FSCREATED_BY { get; set; }

            public string FSPROMOVERID { get; set; }

            public string FSPROMOVERNAME { get; set; }

            public string FSSORT { get; set; }

            public string FSUPDATED_BY { get; set; }
        }
    }

    // MetadataTypeAttribute 會將 TBZSHOOTSPECMetadata 識別為
    // 帶有 TBZSHOOTSPEC 類別其他中繼資料的類別。
    [MetadataTypeAttribute(typeof(TBZSHOOTSPEC.TBZSHOOTSPECMetadata))]
    public partial class TBZSHOOTSPEC
    {

        // 這個類別可讓您將自訂屬性 (Attribute) 附加到 TBZSHOOTSPEC 類別
        // 的 properties。
        //
        // 例如，下列程式碼將 Xyz 屬性標記為
        // 必要的屬性，並指定有效值的格式:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class TBZSHOOTSPECMetadata
        {

            // 中繼資料類別本就不應該具現化。
            private TBZSHOOTSPECMetadata()
            {
            }

            public Nullable<bool> FBISENABLE { get; set; }

            public DateTime FDCREATED_DATE { get; set; }

            public Nullable<DateTime> FDUPDATED_DATE { get; set; }

            public string FSCREATED_BY { get; set; }

            public string FSSHOOTSPECID { get; set; }

            public string FSSHOOTSPECNAME { get; set; }

            public string FSSORT { get; set; }

            public string FSUPDATED_BY { get; set; }
        }
    }

    // MetadataTypeAttribute 會將 TBZSIGNALMetadata 識別為
    // 帶有 TBZSIGNAL 類別其他中繼資料的類別。
    [MetadataTypeAttribute(typeof(TBZSIGNAL.TBZSIGNALMetadata))]
    public partial class TBZSIGNAL
    {

        // 這個類別可讓您將自訂屬性 (Attribute) 附加到 TBZSIGNAL 類別
        // 的 properties。
        //
        // 例如，下列程式碼將 Xyz 屬性標記為
        // 必要的屬性，並指定有效值的格式:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class TBZSIGNALMetadata
        {

            // 中繼資料類別本就不應該具現化。
            private TBZSIGNALMetadata()
            {
            }

            public Nullable<bool> FBISENABLE { get; set; }

            public DateTime FDCREATED_DATE { get; set; }

            public Nullable<DateTime> FDUPDATED_DATE { get; set; }

            public string FSCREATED_BY { get; set; }

            public string FSSIGNALID { get; set; }

            public string FSSIGNALNAME { get; set; }

            public string FSSORT { get; set; }

            public string FSUPDATED_BY { get; set; }
        }
    }

    // MetadataTypeAttribute 會將 TBZTITLEMetadata 識別為
    // 帶有 TBZTITLE 類別其他中繼資料的類別。
    [MetadataTypeAttribute(typeof(TBZTITLE.TBZTITLEMetadata))]
    public partial class TBZTITLE
    {

        // 這個類別可讓您將自訂屬性 (Attribute) 附加到 TBZTITLE 類別
        // 的 properties。
        //
        // 例如，下列程式碼將 Xyz 屬性標記為
        // 必要的屬性，並指定有效值的格式:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class TBZTITLEMetadata
        {

            // 中繼資料類別本就不應該具現化。
            private TBZTITLEMetadata()
            {
            }

            public Nullable<bool> FBISENABLE { get; set; }

            public DateTime FDCREATED_DATE { get; set; }

            public Nullable<DateTime> FDUPDATED_DATE { get; set; }

            public string FSCREATED_BY { get; set; }

            public string FSSORT { get; set; }

            public string FSTITLEID { get; set; }

            public string FSTITLENAME { get; set; }

            public string FSUPDATED_BY { get; set; }
        }
    }
}
