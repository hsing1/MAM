﻿
namespace PTS_MAM3.Web.ENT
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Data;
    using System.Linq;
    using System.ServiceModel.DomainServices.EntityFramework;
    using System.ServiceModel.DomainServices.Hosting;
    using System.ServiceModel.DomainServices.Server;


    // Implements application logic using the ENTUsersEntities context.
    // TODO: Add your application logic to these methods or in additional methods.
    // TODO: Wire up authentication (Windows/ASP.NET Forms) and uncomment the following to disable anonymous access
    // Also consider adding roles to restrict access as appropriate.
    // [RequiresAuthentication]
    [EnableClientAccess()]
    public class ENTUsersDomainService : LinqToEntitiesDomainService<ENTUsersEntities>
    {

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'TBUSER_DEP' query.
        public IQueryable<TBUSER_DEP> GetTBUSER_DEP()
        {
            return this.ObjectContext.TBUSER_DEP;
        }

        public void InsertTBUSER_DEP(TBUSER_DEP tBUSER_DEP)
        {
            if ((tBUSER_DEP.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBUSER_DEP, EntityState.Added);
            }
            else
            {
                this.ObjectContext.TBUSER_DEP.AddObject(tBUSER_DEP);
            }
        }

        public void UpdateTBUSER_DEP(TBUSER_DEP currentTBUSER_DEP)
        {
            this.ObjectContext.TBUSER_DEP.AttachAsModified(currentTBUSER_DEP, this.ChangeSet.GetOriginal(currentTBUSER_DEP));
        }

        public void DeleteTBUSER_DEP(TBUSER_DEP tBUSER_DEP)
        {
            if ((tBUSER_DEP.EntityState == EntityState.Detached))
            {
                this.ObjectContext.TBUSER_DEP.Attach(tBUSER_DEP);
            }
            this.ObjectContext.TBUSER_DEP.DeleteObject(tBUSER_DEP);
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'TBUSER_GROUP' query.
        public IQueryable<TBUSER_GROUP> GetTBUSER_GROUP()
        {
            return this.ObjectContext.TBUSER_GROUP;
        }

        public void InsertTBUSER_GROUP(TBUSER_GROUP tBUSER_GROUP)
        {
            if ((tBUSER_GROUP.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBUSER_GROUP, EntityState.Added);
            }
            else
            {
                this.ObjectContext.TBUSER_GROUP.AddObject(tBUSER_GROUP);
            }
        }

        public void UpdateTBUSER_GROUP(TBUSER_GROUP currentTBUSER_GROUP)
        {
            this.ObjectContext.TBUSER_GROUP.AttachAsModified(currentTBUSER_GROUP, this.ChangeSet.GetOriginal(currentTBUSER_GROUP));
        }

        public void DeleteTBUSER_GROUP(TBUSER_GROUP tBUSER_GROUP)
        {
            if ((tBUSER_GROUP.EntityState == EntityState.Detached))
            {
                this.ObjectContext.TBUSER_GROUP.Attach(tBUSER_GROUP);
            }
            this.ObjectContext.TBUSER_GROUP.DeleteObject(tBUSER_GROUP);
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'TBUSERS' query.
        public IQueryable<TBUSERS> GetTBUSERS()
        {
            return this.ObjectContext.TBUSERS;
        }

        public void InsertTBUSERS(TBUSERS tBUSERS)
        {
            if ((tBUSERS.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBUSERS, EntityState.Added);
            }
            else
            {
                this.ObjectContext.TBUSERS.AddObject(tBUSERS);
            }
        }

        public void UpdateTBUSERS(TBUSERS currentTBUSERS)
        {
            this.ObjectContext.TBUSERS.AttachAsModified(currentTBUSERS, this.ChangeSet.GetOriginal(currentTBUSERS));
        }

        public void DeleteTBUSERS(TBUSERS tBUSERS)
        {
            if ((tBUSERS.EntityState == EntityState.Detached))
            {
                this.ObjectContext.TBUSERS.Attach(tBUSERS);
            }
            this.ObjectContext.TBUSERS.DeleteObject(tBUSERS);
        }
    }
}


