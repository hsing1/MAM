﻿
namespace PTS_MAM3.Web.ENT
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Data;
    using System.Linq;
    using System.ServiceModel.DomainServices.EntityFramework;
    using System.ServiceModel.DomainServices.Hosting;
    using System.ServiceModel.DomainServices.Server;


    // 使用 ENT200 內容實作應用程式邏輯。
    // TODO: 將應用程式邏輯加入至這些方法或其他方法。
    // TODO: 連接驗證 (Windows/ASP.NET Forms) 並取消下面的註解，以停用匿名存取
    // 視需要也考慮加入要限制存取的角色。
    // [RequiresAuthentication]
    [EnableClientAccess()]
    public class ENT200DomainService : LinqToEntitiesDomainService<ENT200>
    {

        // TODO:
        // 考慮限制查詢方法的結果。如果需要其他輸入，可以將
        // 參數加入至這個中繼資料，或建立其他不同名稱的其他查詢方法。
        // 為支援分頁，您必須將排序加入至 'TBARCHIVE_SET_CLASS' 查詢。
        public IQueryable<TBARCHIVE_SET_CLASS> GetTBARCHIVE_SET_CLASS()
        {
            return this.ObjectContext.TBARCHIVE_SET_CLASS;
        }

        public void InsertTBARCHIVE_SET_CLASS(TBARCHIVE_SET_CLASS tBARCHIVE_SET_CLASS)
        {
            if ((tBARCHIVE_SET_CLASS.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBARCHIVE_SET_CLASS, EntityState.Added);
            }
            else
            {
                this.ObjectContext.TBARCHIVE_SET_CLASS.AddObject(tBARCHIVE_SET_CLASS);
            }
        }

        public void UpdateTBARCHIVE_SET_CLASS(TBARCHIVE_SET_CLASS currentTBARCHIVE_SET_CLASS)
        {
            this.ObjectContext.TBARCHIVE_SET_CLASS.AttachAsModified(currentTBARCHIVE_SET_CLASS, this.ChangeSet.GetOriginal(currentTBARCHIVE_SET_CLASS));
        }

        public void DeleteTBARCHIVE_SET_CLASS(TBARCHIVE_SET_CLASS tBARCHIVE_SET_CLASS)
        {
            if ((tBARCHIVE_SET_CLASS.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBARCHIVE_SET_CLASS, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.TBARCHIVE_SET_CLASS.Attach(tBARCHIVE_SET_CLASS);
                this.ObjectContext.TBARCHIVE_SET_CLASS.DeleteObject(tBARCHIVE_SET_CLASS);
            }
        }
    }
}


