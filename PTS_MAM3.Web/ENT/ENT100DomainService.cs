﻿
namespace PTS_MAM3.Web.ENT
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Data;
    using System.Linq;
    using System.ServiceModel.DomainServices.EntityFramework;
    using System.ServiceModel.DomainServices.Hosting;
    using System.ServiceModel.DomainServices.Server;


    // 使用 ENT100Entities 內容實作應用程式邏輯。
    // TODO: 將應用程式邏輯加入至這些方法或其他方法。
    // TODO: 連接驗證 (Windows/ASP.NET Forms) 並取消下面的註解，以停用匿名存取
    // 視需要也考慮加入要限制存取的角色。
    // [RequiresAuthentication]
    [EnableClientAccess()]
    public partial class ENT100DomainService : LinqToEntitiesDomainService<ENT100Entities>
    {

        // TODO:
        // 考慮限制查詢方法的結果。如果需要其他輸入，可以將
        // 參數加入至這個中繼資料，或建立其他不同名稱的其他查詢方法。
        // 為支援分頁，您必須將排序加入至 'TBARCHIVE_SET' 查詢。
        public IQueryable<TBARCHIVE_SET> GetTBARCHIVE_SET()
        {
            return this.ObjectContext.TBARCHIVE_SET;
        }

        public void InsertTBARCHIVE_SET(TBARCHIVE_SET tBARCHIVE_SET)
        {
            if ((tBARCHIVE_SET.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBARCHIVE_SET, EntityState.Added);
            }
            else
            {
                this.ObjectContext.TBARCHIVE_SET.AddObject(tBARCHIVE_SET);
            }
        }

        public void UpdateTBARCHIVE_SET(TBARCHIVE_SET currentTBARCHIVE_SET)
        {
            this.ObjectContext.TBARCHIVE_SET.AttachAsModified(currentTBARCHIVE_SET, this.ChangeSet.GetOriginal(currentTBARCHIVE_SET));
        }

        public void DeleteTBARCHIVE_SET(TBARCHIVE_SET tBARCHIVE_SET)
        {
            if ((tBARCHIVE_SET.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBARCHIVE_SET, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.TBARCHIVE_SET.Attach(tBARCHIVE_SET);
                this.ObjectContext.TBARCHIVE_SET.DeleteObject(tBARCHIVE_SET);
            }
        }

        // TODO:
        // 考慮限制查詢方法的結果。如果需要其他輸入，可以將
        // 參數加入至這個中繼資料，或建立其他不同名稱的其他查詢方法。
        // 為支援分頁，您必須將排序加入至 'TBFILE_TYPE' 查詢。
        public IQueryable<TBFILE_TYPE> GetTBFILE_TYPE()
        {
            return this.ObjectContext.TBFILE_TYPE;
        }

        public void InsertTBFILE_TYPE(TBFILE_TYPE tBFILE_TYPE)
        {
            if ((tBFILE_TYPE.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBFILE_TYPE, EntityState.Added);
            }
            else
            {
                this.ObjectContext.TBFILE_TYPE.AddObject(tBFILE_TYPE);
            }
        }

        public void UpdateTBFILE_TYPE(TBFILE_TYPE currentTBFILE_TYPE)
        {
            this.ObjectContext.TBFILE_TYPE.AttachAsModified(currentTBFILE_TYPE, this.ChangeSet.GetOriginal(currentTBFILE_TYPE));
        }

        public void DeleteTBFILE_TYPE(TBFILE_TYPE tBFILE_TYPE)
        {
            if ((tBFILE_TYPE.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBFILE_TYPE, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.TBFILE_TYPE.Attach(tBFILE_TYPE);
                this.ObjectContext.TBFILE_TYPE.DeleteObject(tBFILE_TYPE);
            }
        }

        // TODO:
        // 考慮限制查詢方法的結果。如果需要其他輸入，可以將
        // 參數加入至這個中繼資料，或建立其他不同名稱的其他查詢方法。
        // 為支援分頁，您必須將排序加入至 'TBLOG_AUDIO' 查詢。
        public IQueryable<TBLOG_AUDIO> GetTBLOG_AUDIO()
        {
            return this.ObjectContext.TBLOG_AUDIO;
        }

        public void InsertTBLOG_AUDIO(TBLOG_AUDIO tBLOG_AUDIO)
        {
            if ((tBLOG_AUDIO.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBLOG_AUDIO, EntityState.Added);
            }
            else
            {
                this.ObjectContext.TBLOG_AUDIO.AddObject(tBLOG_AUDIO);
            }
        }

        public void UpdateTBLOG_AUDIO(TBLOG_AUDIO currentTBLOG_AUDIO)
        {
            this.ObjectContext.TBLOG_AUDIO.AttachAsModified(currentTBLOG_AUDIO, this.ChangeSet.GetOriginal(currentTBLOG_AUDIO));
        }

        public void DeleteTBLOG_AUDIO(TBLOG_AUDIO tBLOG_AUDIO)
        {
            if ((tBLOG_AUDIO.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBLOG_AUDIO, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.TBLOG_AUDIO.Attach(tBLOG_AUDIO);
                this.ObjectContext.TBLOG_AUDIO.DeleteObject(tBLOG_AUDIO);
            }
        }

        // TODO:
        // 考慮限制查詢方法的結果。如果需要其他輸入，可以將
        // 參數加入至這個中繼資料，或建立其他不同名稱的其他查詢方法。
        // 為支援分頁，您必須將排序加入至 'TBPGM_ACTION' 查詢。
        public IQueryable<TBPGM_ACTION> GetTBPGM_ACTION()
        {
            return this.ObjectContext.TBPGM_ACTION;
        }

        public void InsertTBPGM_ACTION(TBPGM_ACTION tBPGM_ACTION)
        {
            if ((tBPGM_ACTION.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBPGM_ACTION, EntityState.Added);
            }
            else
            {
                this.ObjectContext.TBPGM_ACTION.AddObject(tBPGM_ACTION);
            }
        }

        public void UpdateTBPGM_ACTION(TBPGM_ACTION currentTBPGM_ACTION)
        {
            this.ObjectContext.TBPGM_ACTION.AttachAsModified(currentTBPGM_ACTION, this.ChangeSet.GetOriginal(currentTBPGM_ACTION));
        }

        public void DeleteTBPGM_ACTION(TBPGM_ACTION tBPGM_ACTION)
        {
            if ((tBPGM_ACTION.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBPGM_ACTION, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.TBPGM_ACTION.Attach(tBPGM_ACTION);
                this.ObjectContext.TBPGM_ACTION.DeleteObject(tBPGM_ACTION);
            }
        }

        // TODO:
        // 考慮限制查詢方法的結果。如果需要其他輸入，可以將
        // 參數加入至這個中繼資料，或建立其他不同名稱的其他查詢方法。
        // 為支援分頁，您必須將排序加入至 'TBZBOOKING_REASON' 查詢。
        public IQueryable<TBZBOOKING_REASON> GetTBZBOOKING_REASON()
        {
            return this.ObjectContext.TBZBOOKING_REASON;
        }

        public void InsertTBZBOOKING_REASON(TBZBOOKING_REASON tBZBOOKING_REASON)
        {
            if ((tBZBOOKING_REASON.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZBOOKING_REASON, EntityState.Added);
            }
            else
            {
                this.ObjectContext.TBZBOOKING_REASON.AddObject(tBZBOOKING_REASON);
            }
        }

        public void UpdateTBZBOOKING_REASON(TBZBOOKING_REASON currentTBZBOOKING_REASON)
        {
            this.ObjectContext.TBZBOOKING_REASON.AttachAsModified(currentTBZBOOKING_REASON, this.ChangeSet.GetOriginal(currentTBZBOOKING_REASON));
        }

        public void DeleteTBZBOOKING_REASON(TBZBOOKING_REASON tBZBOOKING_REASON)
        {
            if ((tBZBOOKING_REASON.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZBOOKING_REASON, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.TBZBOOKING_REASON.Attach(tBZBOOKING_REASON);
                this.ObjectContext.TBZBOOKING_REASON.DeleteObject(tBZBOOKING_REASON);
            }
        }

        // TODO:
        // 考慮限制查詢方法的結果。如果需要其他輸入，可以將
        // 參數加入至這個中繼資料，或建立其他不同名稱的其他查詢方法。
        // 為支援分頁，您必須將排序加入至 'TBZCHANNEL' 查詢。
        public IQueryable<TBZCHANNEL> GetTBZCHANNEL()
        {
            return this.ObjectContext.TBZCHANNEL;
        }

        public void InsertTBZCHANNEL(TBZCHANNEL tBZCHANNEL)
        {
            if ((tBZCHANNEL.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZCHANNEL, EntityState.Added);
            }
            else
            {
                this.ObjectContext.TBZCHANNEL.AddObject(tBZCHANNEL);
            }
        }

        public void UpdateTBZCHANNEL(TBZCHANNEL currentTBZCHANNEL)
        {
            this.ObjectContext.TBZCHANNEL.AttachAsModified(currentTBZCHANNEL, this.ChangeSet.GetOriginal(currentTBZCHANNEL));
        }

        public void DeleteTBZCHANNEL(TBZCHANNEL tBZCHANNEL)
        {
            if ((tBZCHANNEL.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZCHANNEL, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.TBZCHANNEL.Attach(tBZCHANNEL);
                this.ObjectContext.TBZCHANNEL.DeleteObject(tBZCHANNEL);
            }
        }

        // TODO:
        // 考慮限制查詢方法的結果。如果需要其他輸入，可以將
        // 參數加入至這個中繼資料，或建立其他不同名稱的其他查詢方法。
        // 為支援分頁，您必須將排序加入至 'TBZDEPT' 查詢。
        public IQueryable<TBZDEPT> GetTBZDEPT()
        {
            return this.ObjectContext.TBZDEPT;
        }

        public void InsertTBZDEPT(TBZDEPT tBZDEPT)
        {
            if ((tBZDEPT.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZDEPT, EntityState.Added);
            }
            else
            {
                this.ObjectContext.TBZDEPT.AddObject(tBZDEPT);
            }
        }

        public void UpdateTBZDEPT(TBZDEPT currentTBZDEPT)
        {
            this.ObjectContext.TBZDEPT.AttachAsModified(currentTBZDEPT, this.ChangeSet.GetOriginal(currentTBZDEPT));
        }

        public void DeleteTBZDEPT(TBZDEPT tBZDEPT)
        {
            if ((tBZDEPT.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZDEPT, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.TBZDEPT.Attach(tBZDEPT);
                this.ObjectContext.TBZDEPT.DeleteObject(tBZDEPT);
            }
        }

        // TODO:
        // 考慮限制查詢方法的結果。如果需要其他輸入，可以將
        // 參數加入至這個中繼資料，或建立其他不同名稱的其他查詢方法。
        // 為支援分頁，您必須將排序加入至 'TBZPROGATTR' 查詢。
        public IQueryable<TBZPROGATTR> GetTBZPROGATTR()
        {
            return this.ObjectContext.TBZPROGATTR;
        }

        public void InsertTBZPROGATTR(TBZPROGATTR tBZPROGATTR)
        {
            if ((tBZPROGATTR.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROGATTR, EntityState.Added);
            }
            else
            {
                this.ObjectContext.TBZPROGATTR.AddObject(tBZPROGATTR);
            }
        }

        public void UpdateTBZPROGATTR(TBZPROGATTR currentTBZPROGATTR)
        {
            this.ObjectContext.TBZPROGATTR.AttachAsModified(currentTBZPROGATTR, this.ChangeSet.GetOriginal(currentTBZPROGATTR));
        }

        public void DeleteTBZPROGATTR(TBZPROGATTR tBZPROGATTR)
        {
            if ((tBZPROGATTR.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROGATTR, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.TBZPROGATTR.Attach(tBZPROGATTR);
                this.ObjectContext.TBZPROGATTR.DeleteObject(tBZPROGATTR);
            }
        }

        // TODO:
        // 考慮限制查詢方法的結果。如果需要其他輸入，可以將
        // 參數加入至這個中繼資料，或建立其他不同名稱的其他查詢方法。
        // 為支援分頁，您必須將排序加入至 'TBZPROGAUD' 查詢。
        public IQueryable<TBZPROGAUD> GetTBZPROGAUD()
        {
            return this.ObjectContext.TBZPROGAUD;
        }

        public void InsertTBZPROGAUD(TBZPROGAUD tBZPROGAUD)
        {
            if ((tBZPROGAUD.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROGAUD, EntityState.Added);
            }
            else
            {
                this.ObjectContext.TBZPROGAUD.AddObject(tBZPROGAUD);
            }
        }

        public void UpdateTBZPROGAUD(TBZPROGAUD currentTBZPROGAUD)
        {
            this.ObjectContext.TBZPROGAUD.AttachAsModified(currentTBZPROGAUD, this.ChangeSet.GetOriginal(currentTBZPROGAUD));
        }

        public void DeleteTBZPROGAUD(TBZPROGAUD tBZPROGAUD)
        {
            if ((tBZPROGAUD.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROGAUD, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.TBZPROGAUD.Attach(tBZPROGAUD);
                this.ObjectContext.TBZPROGAUD.DeleteObject(tBZPROGAUD);
            }
        }

        // TODO:
        // 考慮限制查詢方法的結果。如果需要其他輸入，可以將
        // 參數加入至這個中繼資料，或建立其他不同名稱的其他查詢方法。
        // 為支援分頁，您必須將排序加入至 'TBZPROGBUY' 查詢。
        public IQueryable<TBZPROGBUY> GetTBZPROGBUY()
        {
            return this.ObjectContext.TBZPROGBUY;
        }

        public void InsertTBZPROGBUY(TBZPROGBUY tBZPROGBUY)
        {
            if ((tBZPROGBUY.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROGBUY, EntityState.Added);
            }
            else
            {
                this.ObjectContext.TBZPROGBUY.AddObject(tBZPROGBUY);
            }
        }

        public void UpdateTBZPROGBUY(TBZPROGBUY currentTBZPROGBUY)
        {
            this.ObjectContext.TBZPROGBUY.AttachAsModified(currentTBZPROGBUY, this.ChangeSet.GetOriginal(currentTBZPROGBUY));
        }

        public void DeleteTBZPROGBUY(TBZPROGBUY tBZPROGBUY)
        {
            if ((tBZPROGBUY.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROGBUY, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.TBZPROGBUY.Attach(tBZPROGBUY);
                this.ObjectContext.TBZPROGBUY.DeleteObject(tBZPROGBUY);
            }
        }

        // TODO:
        // 考慮限制查詢方法的結果。如果需要其他輸入，可以將
        // 參數加入至這個中繼資料，或建立其他不同名稱的其他查詢方法。
        // 為支援分頁，您必須將排序加入至 'TBZPROGBUYD' 查詢。
        public IQueryable<TBZPROGBUYD> GetTBZPROGBUYD()
        {
            return this.ObjectContext.TBZPROGBUYD;
        }

        public void InsertTBZPROGBUYD(TBZPROGBUYD tBZPROGBUYD)
        {
            if ((tBZPROGBUYD.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROGBUYD, EntityState.Added);
            }
            else
            {
                this.ObjectContext.TBZPROGBUYD.AddObject(tBZPROGBUYD);
            }
        }

        public void UpdateTBZPROGBUYD(TBZPROGBUYD currentTBZPROGBUYD)
        {
            this.ObjectContext.TBZPROGBUYD.AttachAsModified(currentTBZPROGBUYD, this.ChangeSet.GetOriginal(currentTBZPROGBUYD));
        }

        public void DeleteTBZPROGBUYD(TBZPROGBUYD tBZPROGBUYD)
        {
            if ((tBZPROGBUYD.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROGBUYD, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.TBZPROGBUYD.Attach(tBZPROGBUYD);
                this.ObjectContext.TBZPROGBUYD.DeleteObject(tBZPROGBUYD);
            }
        }

        // TODO:
        // 考慮限制查詢方法的結果。如果需要其他輸入，可以將
        // 參數加入至這個中繼資料，或建立其他不同名稱的其他查詢方法。
        // 為支援分頁，您必須將排序加入至 'TBZPROGGRADE' 查詢。
        public IQueryable<TBZPROGGRADE> GetTBZPROGGRADE()
        {
            return this.ObjectContext.TBZPROGGRADE;
        }

        public void InsertTBZPROGGRADE(TBZPROGGRADE tBZPROGGRADE)
        {
            if ((tBZPROGGRADE.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROGGRADE, EntityState.Added);
            }
            else
            {
                this.ObjectContext.TBZPROGGRADE.AddObject(tBZPROGGRADE);
            }
        }

        public void UpdateTBZPROGGRADE(TBZPROGGRADE currentTBZPROGGRADE)
        {
            this.ObjectContext.TBZPROGGRADE.AttachAsModified(currentTBZPROGGRADE, this.ChangeSet.GetOriginal(currentTBZPROGGRADE));
        }

        public void DeleteTBZPROGGRADE(TBZPROGGRADE tBZPROGGRADE)
        {
            if ((tBZPROGGRADE.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROGGRADE, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.TBZPROGGRADE.Attach(tBZPROGGRADE);
                this.ObjectContext.TBZPROGGRADE.DeleteObject(tBZPROGGRADE);
            }
        }

        // TODO:
        // 考慮限制查詢方法的結果。如果需要其他輸入，可以將
        // 參數加入至這個中繼資料，或建立其他不同名稱的其他查詢方法。
        // 為支援分頁，您必須將排序加入至 'TBZPROGLANG' 查詢。
        public IQueryable<TBZPROGLANG> GetTBZPROGLANG()
        {
            return this.ObjectContext.TBZPROGLANG;
        }

        public void InsertTBZPROGLANG(TBZPROGLANG tBZPROGLANG)
        {
            if ((tBZPROGLANG.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROGLANG, EntityState.Added);
            }
            else
            {
                this.ObjectContext.TBZPROGLANG.AddObject(tBZPROGLANG);
            }
        }

        public void UpdateTBZPROGLANG(TBZPROGLANG currentTBZPROGLANG)
        {
            this.ObjectContext.TBZPROGLANG.AttachAsModified(currentTBZPROGLANG, this.ChangeSet.GetOriginal(currentTBZPROGLANG));
        }

        public void DeleteTBZPROGLANG(TBZPROGLANG tBZPROGLANG)
        {
            if ((tBZPROGLANG.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROGLANG, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.TBZPROGLANG.Attach(tBZPROGLANG);
                this.ObjectContext.TBZPROGLANG.DeleteObject(tBZPROGLANG);
            }
        }

        // TODO:
        // 考慮限制查詢方法的結果。如果需要其他輸入，可以將
        // 參數加入至這個中繼資料，或建立其他不同名稱的其他查詢方法。
        // 為支援分頁，您必須將排序加入至 'TBZPROGOBJ' 查詢。
        public IQueryable<TBZPROGOBJ> GetTBZPROGOBJ()
        {
            return this.ObjectContext.TBZPROGOBJ;
        }

        public void InsertTBZPROGOBJ(TBZPROGOBJ tBZPROGOBJ)
        {
            if ((tBZPROGOBJ.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROGOBJ, EntityState.Added);
            }
            else
            {
                this.ObjectContext.TBZPROGOBJ.AddObject(tBZPROGOBJ);
            }
        }

        public void UpdateTBZPROGOBJ(TBZPROGOBJ currentTBZPROGOBJ)
        {
            this.ObjectContext.TBZPROGOBJ.AttachAsModified(currentTBZPROGOBJ, this.ChangeSet.GetOriginal(currentTBZPROGOBJ));
        }

        public void DeleteTBZPROGOBJ(TBZPROGOBJ tBZPROGOBJ)
        {
            if ((tBZPROGOBJ.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROGOBJ, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.TBZPROGOBJ.Attach(tBZPROGOBJ);
                this.ObjectContext.TBZPROGOBJ.DeleteObject(tBZPROGOBJ);
            }
        }

        // TODO:
        // 考慮限制查詢方法的結果。如果需要其他輸入，可以將
        // 參數加入至這個中繼資料，或建立其他不同名稱的其他查詢方法。
        // 為支援分頁，您必須將排序加入至 'TBZPROGRACE_AREA' 查詢。
        public IQueryable<TBZPROGRACE_AREA> GetTBZPROGRACE_AREA()
        {
            return this.ObjectContext.TBZPROGRACE_AREA;
        }

        public void InsertTBZPROGRACE_AREA(TBZPROGRACE_AREA tBZPROGRACE_AREA)
        {
            if ((tBZPROGRACE_AREA.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROGRACE_AREA, EntityState.Added);
            }
            else
            {
                this.ObjectContext.TBZPROGRACE_AREA.AddObject(tBZPROGRACE_AREA);
            }
        }

        public void UpdateTBZPROGRACE_AREA(TBZPROGRACE_AREA currentTBZPROGRACE_AREA)
        {
            this.ObjectContext.TBZPROGRACE_AREA.AttachAsModified(currentTBZPROGRACE_AREA, this.ChangeSet.GetOriginal(currentTBZPROGRACE_AREA));
        }

        public void DeleteTBZPROGRACE_AREA(TBZPROGRACE_AREA tBZPROGRACE_AREA)
        {
            if ((tBZPROGRACE_AREA.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROGRACE_AREA, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.TBZPROGRACE_AREA.Attach(tBZPROGRACE_AREA);
                this.ObjectContext.TBZPROGRACE_AREA.DeleteObject(tBZPROGRACE_AREA);
            }
        }

        // TODO:
        // 考慮限制查詢方法的結果。如果需要其他輸入，可以將
        // 參數加入至這個中繼資料，或建立其他不同名稱的其他查詢方法。
        // 為支援分頁，您必須將排序加入至 'TBZPROGSPEC' 查詢。
        public IQueryable<TBZPROGSPEC> GetTBZPROGSPEC()
        {
            return this.ObjectContext.TBZPROGSPEC;
        }

        public void InsertTBZPROGSPEC(TBZPROGSPEC tBZPROGSPEC)
        {
            if ((tBZPROGSPEC.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROGSPEC, EntityState.Added);
            }
            else
            {
                this.ObjectContext.TBZPROGSPEC.AddObject(tBZPROGSPEC);
            }
        }

        public void UpdateTBZPROGSPEC(TBZPROGSPEC currentTBZPROGSPEC)
        {
            this.ObjectContext.TBZPROGSPEC.AttachAsModified(currentTBZPROGSPEC, this.ChangeSet.GetOriginal(currentTBZPROGSPEC));
        }

        public void DeleteTBZPROGSPEC(TBZPROGSPEC tBZPROGSPEC)
        {
            if ((tBZPROGSPEC.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROGSPEC, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.TBZPROGSPEC.Attach(tBZPROGSPEC);
                this.ObjectContext.TBZPROGSPEC.DeleteObject(tBZPROGSPEC);
            }
        }

        // TODO:
        // 考慮限制查詢方法的結果。如果需要其他輸入，可以將
        // 參數加入至這個中繼資料，或建立其他不同名稱的其他查詢方法。
        // 為支援分頁，您必須將排序加入至 'TBZPROGSRC' 查詢。
        public IQueryable<TBZPROGSRC> GetTBZPROGSRC()
        {
            return this.ObjectContext.TBZPROGSRC;
        }

        public void InsertTBZPROGSRC(TBZPROGSRC tBZPROGSRC)
        {
            if ((tBZPROGSRC.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROGSRC, EntityState.Added);
            }
            else
            {
                this.ObjectContext.TBZPROGSRC.AddObject(tBZPROGSRC);
            }
        }

        public void UpdateTBZPROGSRC(TBZPROGSRC currentTBZPROGSRC)
        {
            this.ObjectContext.TBZPROGSRC.AttachAsModified(currentTBZPROGSRC, this.ChangeSet.GetOriginal(currentTBZPROGSRC));
        }

        public void DeleteTBZPROGSRC(TBZPROGSRC tBZPROGSRC)
        {
            if ((tBZPROGSRC.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROGSRC, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.TBZPROGSRC.Attach(tBZPROGSRC);
                this.ObjectContext.TBZPROGSRC.DeleteObject(tBZPROGSRC);
            }
        }

        // TODO:
        // 考慮限制查詢方法的結果。如果需要其他輸入，可以將
        // 參數加入至這個中繼資料，或建立其他不同名稱的其他查詢方法。
        // 為支援分頁，您必須將排序加入至 'TBZPROGSTATUS' 查詢。
        public IQueryable<TBZPROGSTATUS> GetTBZPROGSTATUS()
        {
            return this.ObjectContext.TBZPROGSTATUS;
        }

        public void InsertTBZPROGSTATUS(TBZPROGSTATUS tBZPROGSTATUS)
        {
            if ((tBZPROGSTATUS.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROGSTATUS, EntityState.Added);
            }
            else
            {
                this.ObjectContext.TBZPROGSTATUS.AddObject(tBZPROGSTATUS);
            }
        }

        public void UpdateTBZPROGSTATUS(TBZPROGSTATUS currentTBZPROGSTATUS)
        {
            this.ObjectContext.TBZPROGSTATUS.AttachAsModified(currentTBZPROGSTATUS, this.ChangeSet.GetOriginal(currentTBZPROGSTATUS));
        }

        public void DeleteTBZPROGSTATUS(TBZPROGSTATUS tBZPROGSTATUS)
        {
            if ((tBZPROGSTATUS.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROGSTATUS, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.TBZPROGSTATUS.Attach(tBZPROGSTATUS);
                this.ObjectContext.TBZPROGSTATUS.DeleteObject(tBZPROGSTATUS);
            }
        }

        // TODO:
        // 考慮限制查詢方法的結果。如果需要其他輸入，可以將
        // 參數加入至這個中繼資料，或建立其他不同名稱的其他查詢方法。
        // 為支援分頁，您必須將排序加入至 'TBZPROGTYPE' 查詢。
        public IQueryable<TBZPROGTYPE> GetTBZPROGTYPE()
        {
            return this.ObjectContext.TBZPROGTYPE;
        }

        public void InsertTBZPROGTYPE(TBZPROGTYPE tBZPROGTYPE)
        {
            if ((tBZPROGTYPE.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROGTYPE, EntityState.Added);
            }
            else
            {
                this.ObjectContext.TBZPROGTYPE.AddObject(tBZPROGTYPE);
            }
        }

        public void UpdateTBZPROGTYPE(TBZPROGTYPE currentTBZPROGTYPE)
        {
            this.ObjectContext.TBZPROGTYPE.AttachAsModified(currentTBZPROGTYPE, this.ChangeSet.GetOriginal(currentTBZPROGTYPE));
        }

        public void DeleteTBZPROGTYPE(TBZPROGTYPE tBZPROGTYPE)
        {
            if ((tBZPROGTYPE.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROGTYPE, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.TBZPROGTYPE.Attach(tBZPROGTYPE);
                this.ObjectContext.TBZPROGTYPE.DeleteObject(tBZPROGTYPE);
            }
        }

        // TODO:
        // 考慮限制查詢方法的結果。如果需要其他輸入，可以將
        // 參數加入至這個中繼資料，或建立其他不同名稱的其他查詢方法。
        // 為支援分頁，您必須將排序加入至 'TBZPROMOCAT' 查詢。
        public IQueryable<TBZPROMOCAT> GetTBZPROMOCAT()
        {
            return this.ObjectContext.TBZPROMOCAT;
        }

        public void InsertTBZPROMOCAT(TBZPROMOCAT tBZPROMOCAT)
        {
            if ((tBZPROMOCAT.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROMOCAT, EntityState.Added);
            }
            else
            {
                this.ObjectContext.TBZPROMOCAT.AddObject(tBZPROMOCAT);
            }
        }

        public void UpdateTBZPROMOCAT(TBZPROMOCAT currentTBZPROMOCAT)
        {
            this.ObjectContext.TBZPROMOCAT.AttachAsModified(currentTBZPROMOCAT, this.ChangeSet.GetOriginal(currentTBZPROMOCAT));
        }

        public void DeleteTBZPROMOCAT(TBZPROMOCAT tBZPROMOCAT)
        {
            if ((tBZPROMOCAT.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROMOCAT, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.TBZPROMOCAT.Attach(tBZPROMOCAT);
                this.ObjectContext.TBZPROMOCAT.DeleteObject(tBZPROMOCAT);
            }
        }

        // TODO:
        // 考慮限制查詢方法的結果。如果需要其他輸入，可以將
        // 參數加入至這個中繼資料，或建立其他不同名稱的其他查詢方法。
        // 為支援分頁，您必須將排序加入至 'TBZPROMOCATD' 查詢。
        public IQueryable<TBZPROMOCATD> GetTBZPROMOCATD()
        {
            return this.ObjectContext.TBZPROMOCATD;
        }

        public void InsertTBZPROMOCATD(TBZPROMOCATD tBZPROMOCATD)
        {
            if ((tBZPROMOCATD.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROMOCATD, EntityState.Added);
            }
            else
            {
                this.ObjectContext.TBZPROMOCATD.AddObject(tBZPROMOCATD);
            }
        }

        public void UpdateTBZPROMOCATD(TBZPROMOCATD currentTBZPROMOCATD)
        {
            this.ObjectContext.TBZPROMOCATD.AttachAsModified(currentTBZPROMOCATD, this.ChangeSet.GetOriginal(currentTBZPROMOCATD));
        }

        public void DeleteTBZPROMOCATD(TBZPROMOCATD tBZPROMOCATD)
        {
            if ((tBZPROMOCATD.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROMOCATD, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.TBZPROMOCATD.Attach(tBZPROMOCATD);
                this.ObjectContext.TBZPROMOCATD.DeleteObject(tBZPROMOCATD);
            }
        }

        // TODO:
        // 考慮限制查詢方法的結果。如果需要其他輸入，可以將
        // 參數加入至這個中繼資料，或建立其他不同名稱的其他查詢方法。
        // 為支援分頁，您必須將排序加入至 'TBZPROMOTYPE' 查詢。
        public IQueryable<TBZPROMOTYPE> GetTBZPROMOTYPE()
        {
            return this.ObjectContext.TBZPROMOTYPE;
        }

        public void InsertTBZPROMOTYPE(TBZPROMOTYPE tBZPROMOTYPE)
        {
            if ((tBZPROMOTYPE.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROMOTYPE, EntityState.Added);
            }
            else
            {
                this.ObjectContext.TBZPROMOTYPE.AddObject(tBZPROMOTYPE);
            }
        }

        public void UpdateTBZPROMOTYPE(TBZPROMOTYPE currentTBZPROMOTYPE)
        {
            this.ObjectContext.TBZPROMOTYPE.AttachAsModified(currentTBZPROMOTYPE, this.ChangeSet.GetOriginal(currentTBZPROMOTYPE));
        }

        public void DeleteTBZPROMOTYPE(TBZPROMOTYPE tBZPROMOTYPE)
        {
            if ((tBZPROMOTYPE.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROMOTYPE, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.TBZPROMOTYPE.Attach(tBZPROMOTYPE);
                this.ObjectContext.TBZPROMOTYPE.DeleteObject(tBZPROMOTYPE);
            }
        }

        // TODO:
        // 考慮限制查詢方法的結果。如果需要其他輸入，可以將
        // 參數加入至這個中繼資料，或建立其他不同名稱的其他查詢方法。
        // 為支援分頁，您必須將排序加入至 'TBZPROMOVER' 查詢。
        public IQueryable<TBZPROMOVER> GetTBZPROMOVER()
        {
            return this.ObjectContext.TBZPROMOVER;
        }

        public void InsertTBZPROMOVER(TBZPROMOVER tBZPROMOVER)
        {
            if ((tBZPROMOVER.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROMOVER, EntityState.Added);
            }
            else
            {
                this.ObjectContext.TBZPROMOVER.AddObject(tBZPROMOVER);
            }
        }

        public void UpdateTBZPROMOVER(TBZPROMOVER currentTBZPROMOVER)
        {
            this.ObjectContext.TBZPROMOVER.AttachAsModified(currentTBZPROMOVER, this.ChangeSet.GetOriginal(currentTBZPROMOVER));
        }

        public void DeleteTBZPROMOVER(TBZPROMOVER tBZPROMOVER)
        {
            if ((tBZPROMOVER.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZPROMOVER, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.TBZPROMOVER.Attach(tBZPROMOVER);
                this.ObjectContext.TBZPROMOVER.DeleteObject(tBZPROMOVER);
            }
        }

        // TODO:
        // 考慮限制查詢方法的結果。如果需要其他輸入，可以將
        // 參數加入至這個中繼資料，或建立其他不同名稱的其他查詢方法。
        // 為支援分頁，您必須將排序加入至 'TBZSHOOTSPEC' 查詢。
        public IQueryable<TBZSHOOTSPEC> GetTBZSHOOTSPEC()
        {
            return this.ObjectContext.TBZSHOOTSPEC;
        }

        public void InsertTBZSHOOTSPEC(TBZSHOOTSPEC tBZSHOOTSPEC)
        {
            if ((tBZSHOOTSPEC.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZSHOOTSPEC, EntityState.Added);
            }
            else
            {
                this.ObjectContext.TBZSHOOTSPEC.AddObject(tBZSHOOTSPEC);
            }
        }

        public void UpdateTBZSHOOTSPEC(TBZSHOOTSPEC currentTBZSHOOTSPEC)
        {
            this.ObjectContext.TBZSHOOTSPEC.AttachAsModified(currentTBZSHOOTSPEC, this.ChangeSet.GetOriginal(currentTBZSHOOTSPEC));
        }

        public void DeleteTBZSHOOTSPEC(TBZSHOOTSPEC tBZSHOOTSPEC)
        {
            if ((tBZSHOOTSPEC.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZSHOOTSPEC, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.TBZSHOOTSPEC.Attach(tBZSHOOTSPEC);
                this.ObjectContext.TBZSHOOTSPEC.DeleteObject(tBZSHOOTSPEC);
            }
        }

        // TODO:
        // 考慮限制查詢方法的結果。如果需要其他輸入，可以將
        // 參數加入至這個中繼資料，或建立其他不同名稱的其他查詢方法。
        // 為支援分頁，您必須將排序加入至 'TBZSIGNAL' 查詢。
        public IQueryable<TBZSIGNAL> GetTBZSIGNAL()
        {
            return this.ObjectContext.TBZSIGNAL;
        }

        public void InsertTBZSIGNAL(TBZSIGNAL tBZSIGNAL)
        {
            if ((tBZSIGNAL.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZSIGNAL, EntityState.Added);
            }
            else
            {
                this.ObjectContext.TBZSIGNAL.AddObject(tBZSIGNAL);
            }
        }

        public void UpdateTBZSIGNAL(TBZSIGNAL currentTBZSIGNAL)
        {
            this.ObjectContext.TBZSIGNAL.AttachAsModified(currentTBZSIGNAL, this.ChangeSet.GetOriginal(currentTBZSIGNAL));
        }

        public void DeleteTBZSIGNAL(TBZSIGNAL tBZSIGNAL)
        {
            if ((tBZSIGNAL.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZSIGNAL, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.TBZSIGNAL.Attach(tBZSIGNAL);
                this.ObjectContext.TBZSIGNAL.DeleteObject(tBZSIGNAL);
            }
        }

        // TODO:
        // 考慮限制查詢方法的結果。如果需要其他輸入，可以將
        // 參數加入至這個中繼資料，或建立其他不同名稱的其他查詢方法。
        // 為支援分頁，您必須將排序加入至 'TBZTITLE' 查詢。
        public IQueryable<TBZTITLE> GetTBZTITLE()
        {
            return this.ObjectContext.TBZTITLE;
        }

        public void InsertTBZTITLE(TBZTITLE tBZTITLE)
        {
            if ((tBZTITLE.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZTITLE, EntityState.Added);
            }
            else
            {
                this.ObjectContext.TBZTITLE.AddObject(tBZTITLE);
            }
        }

        public void UpdateTBZTITLE(TBZTITLE currentTBZTITLE)
        {
            this.ObjectContext.TBZTITLE.AttachAsModified(currentTBZTITLE, this.ChangeSet.GetOriginal(currentTBZTITLE));
        }

        public void DeleteTBZTITLE(TBZTITLE tBZTITLE)
        {
            if ((tBZTITLE.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tBZTITLE, EntityState.Deleted);
            }
            else
            {
                this.ObjectContext.TBZTITLE.Attach(tBZTITLE);
                this.ObjectContext.TBZTITLE.DeleteObject(tBZTITLE);
            }
        }
    }
}


