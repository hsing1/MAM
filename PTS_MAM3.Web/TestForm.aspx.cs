﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Xealcom.Security;
using System.IO;
using ADSSECURITYLib;
using ActiveDs;



namespace PTS_MAM3.Web
{
    public partial class TestForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            ImpersonateUser iu = new ImpersonateUser();
            iu.Impersonate(@"pts.org.tw", @"mamsystem", @"p@ssw0rd");
            try
            {
                //Directory.GetFiles(@"\\10.13.200.6\MAMDownload");
                Directory.CreateDirectory(@"\\10.13.200.6\MAMDownload\3TMAM\");
                Directory.CreateDirectory(@"\\10.13.200.6\MAMDownload\3TNews\");

                SetPermissions(@"\\10.13.200.6\MAMDownload\3TMAM\", @"3TMAM");
                SetPermissions(@"\\10.13.200.6\MAMDownload\3TNews\", @"3TNews");
                iu.Undo();
            }
            catch (Exception ex)
            {

            }

            
        }

        public void SetPermissions(String vPath, String UserName)
        {
            ADsSecurity objADsSec;
            SecurityDescriptor objSecDes;
            AccessControlList objDAcl;
            AccessControlEntry objAce1;
            AccessControlEntry objAce2;
            Object objSIdHex;
            ADsSID objSId;

            objADsSec = new ADsSecurity();
            objSecDes = (SecurityDescriptor)(objADsSec.GetSecurityDescriptor("FILE://" + vPath));
            objDAcl = (AccessControlList)objSecDes.DiscretionaryAcl;


            objSId = new ADsSID();
            objSId.SetAs((int)ADSSECURITYLib.ADS_SID_FORMAT.ADS_SID_SAM, UserName.ToString());
            objSIdHex = objSId.GetAs((int)ADSSECURITYLib.ADS_SID_FORMAT.ADS_SID_SDDL);

            //Add a new access control entry (ACE) object (objAce) so that the user has Full Control permissions on NTFS file system files.
            objAce1 = new AccessControlEntry();
            objAce1.Trustee = (objSIdHex).ToString();
            objAce1.AccessMask = (int)ActiveDs.ADS_RIGHTS_ENUM.ADS_RIGHT_DELETE;
            objAce1.AceType = (int)ActiveDs.ADS_ACETYPE_ENUM.ADS_ACETYPE_ACCESS_DENIED;
            objAce1.AceFlags = (int)ActiveDs.ADS_ACEFLAG_ENUM.ADS_ACEFLAG_INHERITED_ACE | 1;
            objDAcl.AddAce(objAce1);
            
            //Add a new access control entry object (objAce) so that the user has Full Control permissions on NTFS file system folders.
            objAce2 = new AccessControlEntry();
            objAce2.Trustee = (objSIdHex).ToString();
            objAce2.AccessMask = (int)ActiveDs.ADS_RIGHTS_ENUM.ADS_RIGHT_GENERIC_READ ;
            objAce2.AceType = (int)ActiveDs.ADS_ACETYPE_ENUM.ADS_ACETYPE_ACCESS_ALLOWED;
            objAce2.AceFlags = (int)ActiveDs.ADS_ACEFLAG_ENUM.ADS_ACEFLAG_INHERIT_ACE | 1;
            objDAcl.AddAce(objAce2);


            //delete everyone
            foreach (AccessControlEntry a in objSecDes.DiscretionaryAcl)
            {
                if (a.Trustee == "Everyone")
                {
                    objDAcl.RemoveAce(a);
                }
            }

            objSecDes.DiscretionaryAcl = objDAcl;

            // Set permissions on the NTFS file system folder.
            objADsSec.SetSecurityDescriptor(objSecDes, "FILE://" + vPath);

            


        }


    }
}