﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_CODE
    {
        public string ID { get; set; }
        public string IDD { get; set; }
        public string NAME { get; set; }
        public string TABLENAME { get; set; }
        public string SORT { get; set; }
    }
}