﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PTS_MAM3.Web.Class;

namespace PTS_MAM3.Web
{
    public static class MAMFunctions
    {
        //查詢節目主檔的節目名稱        
        public static string QueryProgName(string strProgID)
        {
            string strProgName = "";
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPROG_ID", strProgID);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_M", sqlParameters, out sqlResult) || sqlResult.Count == 0)
                return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL            

            strProgName = sqlResult[0]["FSPGMNAME"];
            return strProgName;
        }

        //查詢節目子集的子集名稱        
        public static string QueryEpisodeName(string strProgID, string strEpisode)
        {
            string strEpisodeName = "";
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPROG_ID", strProgID);
            sqlParameters.Add("FNEPISODE", strEpisode);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_D", sqlParameters, out sqlResult) || sqlResult.Count == 0)
                return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            strEpisodeName = sqlResult[0]["FSPGDNAME"];
            return strEpisodeName;
        }

        //查詢宣傳帶主檔的宣傳帶名稱        
        public static string QueryPromoName(string strPromoID)
        {
            string strPromoName = "";
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPROMO_ID", strPromoID);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_SP_Q_TBPGM_PROMO_BYPROMOID", sqlParameters, out sqlResult) || sqlResult.Count == 0)
                return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            strPromoName = sqlResult[0]["FSPROMO_NAME"];
            return strPromoName;
        }

        //解析資料庫回來的Dictionary，轉存成Class(各式代碼檔用)透過名稱
        public static List<Class_CODE> Transfer_Class_CODE(string strProcedureName, string strName)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("NNAME", strName);
            List<Dictionary<string, string>> sqlResult;
            List<Class_CODE> returnData = new List<Class_CODE>();

            if (!MAM_PTS_DLL.DbAccess.Do_Query(strProcedureName, sqlParameters, out sqlResult))
                return new List<Class_CODE>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_CODE obj = new Class_CODE();
                // --------------------------------    
                obj.ID = sqlResult[i]["ID"];
                obj.NAME = sqlResult[i]["NAME"];
                obj.TABLENAME = sqlResult[i]["TABLENAME"];
                obj.SORT = sqlResult[i]["FSSORT"];
                // --------------------------------
                returnData.Add(obj);
            }
            return returnData;
        }

        //解析資料庫回來的Dictionary，轉存成Class(各式代碼檔用)
        public static List<Class_CODE> Transfer_Class_CODE(string strProcedureName)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            List<Class_CODE> returnData = new List<Class_CODE>();

            if (!MAM_PTS_DLL.DbAccess.Do_Query(strProcedureName, sqlParameters, out sqlResult))
                return new List<Class_CODE>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_CODE obj = new Class_CODE();
                // --------------------------------    
                obj.ID = sqlResult[i]["ID"];
                obj.NAME = sqlResult[i]["NAME"];
                obj.TABLENAME = sqlResult[i]["TABLENAME"];
                obj.SORT = sqlResult[i]["FSSORT"];
                // --------------------------------
                returnData.Add(obj);
            }
            return returnData;
        }

        //冠宇加的 因為欄位特別
        public static List<Class_CODE_TBFILE_TYPE> Transfer_Class_CODE_TBFILE_TYPE(string strProcedureName)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            List<Class_CODE_TBFILE_TYPE> returnData = new List<Class_CODE_TBFILE_TYPE>();

            if (!MAM_PTS_DLL.DbAccess.Do_Query(strProcedureName, sqlParameters, out sqlResult))
                return new List<Class_CODE_TBFILE_TYPE>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_CODE_TBFILE_TYPE obj = new Class_CODE_TBFILE_TYPE();
                // --------------------------------    
                obj.ID = sqlResult[i]["ID"];
                obj.NAME = sqlResult[i]["NAME"];
                obj.TABLENAME = sqlResult[i]["TABLENAME"];
                obj.SORT = sqlResult[i]["FSSORT"];
                obj.FSTYPE = sqlResult[i]["FSTYPE"];

                if (sqlResult[i]["FSSPEC"] == null)
                    obj.FSSPEC = "";
                else
                    obj.FSSPEC = sqlResult[i]["FSSPEC"];

                if (sqlResult[i]["FSTAPE_NOTE"] == null)
                    obj.FSTAPE_NOTE = "";
                else
                    obj.FSTAPE_NOTE = sqlResult[i]["FSTAPE_NOTE"];
                // --------------------------------
                returnData.Add(obj);
            }
            return returnData;
        }

        //解析資料庫回來的Dictionary，轉存成Class(細項代碼檔用)
        public static List<Class_CODE> Transfer_Class_CODED(string strProcedureName)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            List<Class_CODE> returnData = new List<Class_CODE>();

            if (!MAM_PTS_DLL.DbAccess.Do_Query(strProcedureName, sqlParameters, out sqlResult))
                return new List<Class_CODE>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_CODE obj = new Class_CODE();
                // --------------------------------    
                obj.ID = sqlResult[i]["ID"];
                obj.IDD = sqlResult[i]["IDD"];
                obj.NAME = sqlResult[i]["NAME"];
                obj.SORT = sqlResult[i]["FSSORT"];
                // --------------------------------
                returnData.Add(obj);
            }
            return returnData;
        }

        //NY到是否的轉換
        public static string compareNY(string strWin)
        {
            if (strWin == "Y")
                return "是";
            else
                return "否";
        }

        //節目名稱轉換
        public static string compareProgName(string strProgName)
        {
            if (strProgName == "Replace_FSPROG_NAME")
                return "";
            else
                return strProgName;
        }

        //子集名稱轉換
        public static string compareEpisodeName(string strEPISODEName)
        {
            if (strEPISODEName == "Replace_FNEPISODE_NAME")
                return "";
            else
                return strEPISODEName;
        }

        //數字類轉換
        public static string compareNumber(string strNumber)
        {
            if (strNumber == "0")
                return "";
            else
                return strNumber;
        }

        //日期轉換
        public static string compareDate(string strDate)
        {
            if (strDate == "1900/01/01")
                return "";
            else if (strDate == "1900-01-01 00:00:00")
                return "";
            else
                return strDate;
        }

        //節目種類轉換
        public static string CheckProgType(string strTypeID)
        {
            if (strTypeID == "2")
                return "續製";
            else if (strTypeID == "3")
                return "衍生";
            else if (strTypeID == "1")
                return "新製";
            else
                return "";
        }

        //類型轉換
        public static string CheckType(string strTypeID)
        {
            if (strTypeID == "P")
                return "短帶";
            else if (strTypeID == "G")
                return "節目";
            else if (strTypeID == "D")
                return "資料帶";
            else
                return "";
        }

        //著作權類型轉換
        public static string CheckCopyRightType(string strTypeID)
        {
            if (strTypeID == "01")
                return "全部";
            else if (strTypeID == "02")
                return "部分";
            else if (strTypeID == "03")
                return "無";
            else
                return "";
        }

        //影音圖文檔案狀態名稱轉換
        public static string CheckFileType(string strTypeID)
        {
            switch (strTypeID)
            {
                case "B":
                    return "待轉檔";
                case "O":
                    return "待轉檔";
                case "S":
                    return "轉檔中";
                case "T":
                    return "轉檔完成";
                case "R":
                    return "轉檔失敗";
                case "A":
                    return "待入庫審核";
                case "F":                   //單子不通過、抽單都會導致檔案狀態為 F
                    return "審核不過";
                case "Y":
                    return "入庫";
                case "D":
                    return "刪除";
                case "W":
                    return "待入庫置換審核";
                case "X":
                    return "刪除";
                default:
                    return "";
            }
        }


        //成案單、送帶轉檔單、送帶轉檔置換單狀態名稱轉換
        public static string CheckStatusReport(string strStatusID)
        {
            switch (strStatusID)
            {
                case "N":               //表單初始狀態
                case "W":               //待入庫置換
                case "C":               //待送帶轉檔置換
                    return "審核";
                case "X":               //使用者抽單
                    return "抽單";
                case "F":               //駁回
                    return "不通過";
                case "Y":               //審核完成
                case "I":               //目前轉檔完不修改狀態
                    return "通過";
                case "T":               //影帶回溯
                    return "回溯";
                case "G"://線上簽收後
                case "K":               //其他影片上傳的初始狀態 2016-01-29 新增 by Jarvis
                    return "簽收";
                case "J":               //其他影片上傳的抽單狀態 2016-01-29 新增 by Jarvis
                    return "退單";
                default:
                    return "";
            }
        }

        //成案單、送帶轉檔單、送帶轉檔置換單狀態名稱轉換
        public static string CheckStatus_SendBro(string strStatusID)
        {
            switch (strStatusID.Trim())
            {
                case "N"://表單初始狀態
                    return "未上傳";
                case "R":
                    return "退單";
                case "U":
                    return "標記上傳";
                case "E":
                    return "抽單";
                default:
                    return "";
            }
        }
        //轉檔時轉換檔案大小
        public static string CheckFileSize(string strSize)
        {
            string strKB = "";       //位元組轉換成KB
            string strMB = "";       //KB轉換成MB
            string strGB = "";      //MB轉換成GB

            try
            {
                strKB = Convert.ToString(Convert.ToDecimal(strSize) / 1024);

                if (Convert.ToDecimal(strKB) > 1024)
                {
                    strMB = Convert.ToString(Convert.ToDecimal(strSize) / 1048576);

                    if (Convert.ToDecimal(strMB) > 1024)
                    {
                        strGB = Convert.ToString(Convert.ToDecimal(strSize) / 1073741824);
                        return Math.Round(Convert.ToDouble(strGB), 1).ToString() + "GB"; //取小數點第一位
                    }
                    else
                        return Math.Floor(Convert.ToDouble(strMB)).ToString() + "MB";
                }
                else
                    return Math.Floor(Convert.ToDouble(strKB)).ToString() + "KB";
            }
            catch (Exception ex)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("CheckFileSize", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "MAMFunctions:" + ex.Message);
                return "";
            }
        }

    }
}