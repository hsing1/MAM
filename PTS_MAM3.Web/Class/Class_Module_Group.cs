﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_Module_Group
    {
        public int FSMOD_GRP_ID { get; set; }
        public string FSMODULE_ID { get; set; }
        public string FSGROUP_ID { get; set; } 
    }
}