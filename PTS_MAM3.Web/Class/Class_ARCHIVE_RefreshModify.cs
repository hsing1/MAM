﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_ARCHIVE_RefreshModify:Class_ARCHIVE
    {
        public string FSPROG_ID_NAME { get; set; }
        public string FSEPISODE_NAME { get; set; }
        public string FNTOTEPISODE { get; set; }
        public string FNLENGTH { get; set; }
        
    }
}