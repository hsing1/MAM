﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_Module_Detail
    {
        public string FSMODULE_ID { get; set; }
        public string FSFUNC_NAME { get; set; }
        public string FSDESCRIPTION { get; set; }
        public string FSHANDLER { get; set; }
        public string FCHasPermission { get; set; } 
    }
}