﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_CODE_TBFILE_TYPE:Class_CODE
    {
        public string FSTYPE { get; set; }      //影音圖其他
        public string FSSPEC { get; set; }      //檔案規格
        public string FSTAPE_NOTE { get; set; } //影帶類型對照
    }
}