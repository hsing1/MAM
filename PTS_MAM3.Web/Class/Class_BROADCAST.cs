﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_BROADCAST
    {
        //public string FSFILE_NO { get; set; } //Add 2015/10/02 by Jarvis
        //public string FSVIDEO_PROG { get; set; }
        public string HD_MC_Status { get; set; }
        //public string HD_MC_Status_NAME { get; set; }
       
        //------------------------------------------
        public string FSBRO_ID { get; set; }
        public string FSBRO_TYPE { get; set; }
        public string FSBRO_TYPE_NAME { get; set; }
        public string FSID { get; set; }
        public string FSID_NAME { get; set; }
        public short? FNEPISODE { get; set; }
        public string SHOW_FNEPISODE { get; set; }
        public string FNEPISODE_NAME { get; set; }
        public DateTime FDBRO_DATE { get; set; }
        public string FSBRO_BY { get; set; }
        public string FSBRO_BY_NAME { get; set; }
        public string FSMEMO { get; set; }
        public string FCCHECK_STATUS { get; set; }
        public string FCCHECK_STATUS_NAME { get; set; }
        public string FSCHECK_BY { get; set; }
        public string FSCHECK_BY_NAME { get; set; }
        public string FSNOTE { get; set; }
        public string FSCHANNELID { get; set; }
        public string FCCHANGE { get; set; }
        public string FSPRINTID { get; set; }
        public string FSREJECT { get; set; }  

        public string FSCREATED_BY { get; set; }
        public DateTime FDCREATED_DATE { get; set; }
        public string FSUPDATED_BY { get; set; }
        public DateTime FDUPDATED_DATE { get; set; }

        //新增的欄位 BY 冠宇 為了顯示簽核者的欄位
        public string FSSIGNED_BY { get; set; }
        public string FSSIGNED_BY_NAME { get; set; }
        public string FDSIGNED_DATE { set; get; }

        //新增的欄位 BY David 為了顯示轉檔者的欄位
        public string FSTRAN_BY { get; set; }
        public string FDTRAN_DATE { set; get; }

        public string FSCREATED_BY_NAME { get; set; }
        public string FSCREATED_BY_EMAIL { get; set; }   //建檔者EMAIL
        public string FSUPDATED_BY_NAME { get; set; }

        public string SHOW_FDCREATED_DATE { get; set; }
        public string SHOW_FDUPDATED_DATE { get; set; }
        public string SHOW_FDBRO_DATE { get; set; }
    }
}