﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_DELETED_SEARCH_PARAMETER
    {
        public string _fstype { set; get; }
        public string _fsfile_type_v { set; get; }
        public string _fsfile_type_a { set; get; }
        public string _fsfile_type_p { set; get; }
        public string _fsfile_type_d { set; get; }
        public string _fsprog_id { set; get; }
        public string _fnepisode_beg { set; get; }
        public string _fnepisode_end { set; get; }
        public string _fsfile_no { set; get; }
        public string _fdcreated_date_beg { set; get; }
        public string _fdcreated_date_end { set; get; }
    }
}