﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_DATA
    {
        public string FSDATA_ID { get; set; }
        public string FSDATA_NAME { get; set; }
        public string FSMEMO { get; set; }
        public string FSDEL { get; set; }
        public string FSDELUSER { get; set; }
        public string FSCHANNEL_ID { get; set; }
        public string FSCREATED_BY { get; set; }
        public DateTime FDCREATED_DATE { get; set; }
        public string FSUPDATED_BY { get; set; }
        public DateTime FDUPDATED_DATE { get; set; }
        public int FNDEP_ID { get; set; }

        public string SHOW_FDCREATED_DATE { get; set; }//建檔日期(顯示)
        public string SHOW_FDUPDATED_DATE { get; set; }//修改日期(顯示)
        public string FSCREATED_BY_NAME { get; set; }  //建檔者(顯示)
        public string FSUPDATED_BY_NAME { get; set; }  //修改者(顯示)
        public string FSDEL_BY_NAME { get; set; }      //刪除者(顯示)
    }
}