﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_PROGPRODUCER
    {
        public string FSPRO_TYPE { get; set; }
        public string FSPRO_TYPE_NAME { get; set; }
        public string FSID { get; set; }
        public string FSID_NAME { get; set; }
        public string FSPRODUCER { get; set; }
        public string FSPRODUCER_NAME { get; set; }
        public int FNUpperDept_ID { get; set; }
        public int FNDep_ID { get; set; }
        public int FNGroup_ID { get; set; }
        public string FSPRODUCER_DEPT_NAME { get; set; }
        public string FSCREATED_BY { get; set; }
        public DateTime FDCREATED_DATE { get; set; }
        public string FSUPDATED_BY { get; set; }
        public DateTime FDUPDATED_DATE { get; set; }

        public string FSCREATED_BY_NAME { get; set; }
        public string FSUPDATED_BY_NAME { get; set; }

        public string SHOW_FDCREATED_DATE { get; set; }
        public string SHOW_FDUPDATED_DATE { get; set; }  
    }
}