﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    /// <summary>
    /// strARC_ID:入庫單單號
    /// strARC_TYPE:撈取TBFILE_TYPE在Combox裡面選取的資料
    /// strProgramId:
    /// strFSID:節目代號
    /// strEpisodeId:集數
    /// strType: G:節目帶 P:宣傳帶
    /// strDirectory:TSM儲存路徑(維智)
    /// strFileId:取檔號碼(至軒)
    /// strOFileName:原始檔案名稱
    /// strOExtensionName:原始副檔名
    /// strChannelId:頻道Id
    /// srtFileSize:檔案大小
    /// strUserId:使用者代碼
    /// strUploadFileType
    /// </summary>
    public class ClassUploadFileClass
    {
        public string strARC_ID { get; set; }
        public string strARC_TYPE { get; set; }
        public string strProgramId { get; set; }
        public string strFSID { get; set; }
        public string strEpisodeId { get; set; }
        public string strType { get; set; }
        public string strDirectory { get; set; }
        public string strFileId { get; set; }
        public string FS64FNDIR_ID { get; set; }
        public string strOFileName { get; set; }
        public string strOExtensionName { get; set; }
        public string strChannelId { get; set; }
        public string srtFileSize { get; set; }
        public string strUserId { get; set; }
        public string strUploadFileType { get; set; }
        //置換檔案用
        public string strFSCHANGE_FILE_NO { get; set; }
        //傳給至軒取號要用的
        public string strFSID_NAME { get; set; }
        public string strEpisodeId_Name { get; set; }
        //input metadata
        public string strMetadataFSTITLE { get; set; }
        public string strMetadataFSFSDESCRIPTION { get; set; }
        public void clear()
        {
            strARC_ID = null;
            strARC_TYPE = null;
            strProgramId = null;
            strFSID = null;
            strEpisodeId = null;
            strType = null;
            strDirectory = null;
            strFileId = null;
            strOFileName = null;
            strOExtensionName = null;
            strChannelId = null;
            srtFileSize = null;
            strUserId = null;
            strUploadFileType = null;
            strMetadataFSTITLE = null;
            strMetadataFSFSDESCRIPTION = null;
        }
    }
}