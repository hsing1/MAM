﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    /// <summary>
    /// 用來傳遞VSPD的
    /// </summary>
    public class Class_ARCHIVE_VAPD
    {
        public string FSTableName { get; set; }
        public string FSFile_No { get; set; }           //檔名
        public string FSFileType { get; set; }          //檔案類型--副檔名
        public string FSTitle { get; set; }             //標題
        public string FSFileSize { get; set; }          //檔案大小
        public string FSOldFileName { get; set; }       //原始檔名
        public string FSTSMPath { get; set; }           //TSM路徑
        public string FSSUPERVISOR { get; set; }        //是否須由主管簽核
        public string FSCreatedBy { get; set; }         
        public DateTime FSCreateDate { get; set; }
        public string FSUpdatedBy { get; set; }
        public DateTime FSUpdateDate { get; set; }
        public string FSIsOldUploadedFile { get; set; } //為了判斷是否為以上傳的檔案所新增的欄位
    }
}