﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_TAPE_LABEL_TAPE
    {
        public string FSTAPEID { get; set; }
        public string FSBIB { get; set; }
        public string FSTVOLUME { get; set; }
        public string FSTLOCATION { get; set; }
        public string FSFILETYPE { get; set; }
        public string FSFILETYPE_NAME { get; set; }
        public string FSTTYPE { get; set; }
        public string FSTTYPE_NAME { get; set; }
        public string FSTTAPECODE { get; set; }
        public string FSTTAPECODE_NAME { get; set; }
        public string FSTAPE_TITLE { get; set; }
        public string FSTAPE_DES { get; set; }
        public string FSDURATION { get; set; }

        public string FSCREATED_BY_NAME { get; set; }  //建檔者(顯示)
        public string FSUPDATED_BY_NAME { get; set; }  //修改者(顯示)
        public DateTime FDCREATED_DATE { get; set; }
        public DateTime FDUPDATED_DATE { get; set; }

        public string SHOW_FDCREATED_DATE { get; set; }//建檔日期(顯示)
        public string SHOW_FDUPDATED_DATE { get; set; }//修改日期(顯示)

    }
}