﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_PROPOSAL
    {
        //public Class_PROPOSAL() 
        //{
        //    _FDCREATED_DATE = new DateTime(1900, 1, 1);
        //    _FDUPDATED_DATE = new DateTime(1900, 1, 1);
        //    _FDSHOW_DATE = new DateTime(1900, 1, 1);
        //}
        public string FSPRODUCER { get; set; }//製作人ID 2013/04/27 by Jarvis
        public string FSPRODUSER_NAME { get; set; }//製作人名稱 2013/05/01 by Jarvis
        public string FSPRO_ID { get; set; }
        public string FCPRO_TYPE { get; set; } 
        public string FCPRO_TYPE_NAME { get; set; } 
        public string FSPROG_ID { get; set; }
        public string FSPROG_NAME { get; set; }
        public string FSPLAY_NAME { get; set; }
        public string FSNAME_FOR { get; set; }
        public string FSCONTENT { get; set; }
        public int FNLENGTH { get; set; }
        public int FNBEG_EPISODE { get; set; }
        public int FNEND_EPISODE { get; set; }
        public string FSCHANNEL { get; set; }  
        public DateTime FDSHOW_DATE { get; set; }   
        public string FSPRD_DEPT_ID { get; set; }
        public string FSPAY_DEPT_ID { get; set; }
        public string FSSIGN_DEPT_ID { get; set; }
        public string FSBEF_DEPT_ID { get; set; }
        public string FSAFT_DEPT_ID { get; set; }
        public string FSOBJ_ID { get; set; }
        public string FSGRADE_ID { get; set; }
        public string FSSRC_ID { get; set; }
        public string FSTYPE { get; set; }
        public string FSAUD_ID { get; set; }
        public string FSSHOW_TYPE { get; set; }
        public string FSLANG_ID_MAIN { get; set; }
        public string FSLANG_ID_SUB { get; set; }
        public string FSBUY_ID { get; set; }
        public string FSBUYD_ID { get; set; }
        public string FCCHECK_STATUS { get; set; }
        public string FSCHANNEL_ID { get; set; }

        public string FCEMERGENCY { get; set; }
        public string FCCOMPLETE { get; set; }
        public DateTime FDCOMPLETE { get; set; }
        public string FSPRDCENID { get; set; }           //製作單位代碼
        public string FSPROGSPEC { get; set; }           //節目規格
        public string FCPROGD { get; set; }              //產生子集
        public string FSENOTE { get; set; }              //分集備註

        public string FSCREATED_BY { get; set; }       
        public DateTime FDCREATED_DATE { get; set; }
        public string FSUPDATED_BY { get; set; }
        public DateTime FDUPDATED_DATE { get; set; }

        public string SHOW_FDSHOW_DATE { get; set; }     //上檔日期(顯示)     
        public string EPISODE { get; set; }              //集數起迄
        public string SHOW_FDCREATED_DATE { get; set; }  //建檔日期(顯示)
        public string SHOW_FDUPDATED_DATE { get; set; }  //修改日期(顯示)
        public string FSCREATED_BY_NAME { get; set; }    //建檔者(顯示)
        public string FSUPDATED_BY_NAME { get; set; }    //修改者(顯示)
        public string FSCREATED_BY_EMAIL { get; set; }   //建檔者EMAIL
        public string SHOW_FDCOMPLETE { get; set; }      //資料補齊日期(顯示)
        public string SHOW_FCEMERGENCY { get; set; }     //緊急成案(顯示)
        public string SHOW_FCCHECK_STATUS_NAME { get; set; }//表單狀態名稱

        //代碼名稱
        public string FSPRD_DEPT_ID_NAME { get; set; }   //製作部門代碼名稱
        public string FSPAY_DEPT_ID_NAME { get; set; }   //付款部門代碼名稱
        public string FSBEF_DEPT_ID_NAME { get; set; }   //前會部門代碼名稱
        public string FSAFT_DEPT_ID_NAME { get; set; }   //後會部門代碼名稱
        public string FSOBJ_ID_NAME { get; set; }        //製作目的代碼名稱
        public string FSSRC_ID_NAME { get; set; }        //節目來源代碼名稱
        public string FSTYPE_NAME { get; set; }          //內容屬性代碼名稱
        public string FSAUD_ID_NAME { get; set; }        //目標觀眾代碼名稱
        public string FSSHOW_TYPE_NAME { get; set; }     //表現方式代碼名稱
        public string FSGRADE_ID_NAME { get; set; }      //節目分級代碼名稱
        public string FSLANG_ID_MAIN_NAME { get; set; }  //主聲道代碼名稱
        public string FSLANG_ID_SUB_NAME { get; set; }   //副聲道代碼名稱
        public string FSBUY_ID_NAME { get; set; }        //外購類別代碼名稱
        public string FSBUYD_ID_NAME { get; set; }       //外購類別細項代碼名稱
        public string FSCHANNEL_ID_NAME { get; set; }    //頻道別代碼名稱
        public string FSPROGSPEC_NAME { get; set; }      //節目規格名稱
        public string FSPRDCENID_NAME { get; set; }      //製作單位名稱
        public string FSEXPIRE_DATE_ACTION { get; set; }    //到期後是否刪除 2012/11/22 kyle
        public DateTime FDEXPIRE_DATE { get; set; }       //到期日期
        public string FSPROGNATIONID { get; set; }          //來源國家代碼
        
        public string Origin_FSEXPIRE_DATE_ACTION { get; set; }    //原_到期後是否刪除 2012/11/22 kyle
        public DateTime Origin_FDEXPIRE_DATE { get; set; }          //原_到期日期
    }
}
  //get
  //          {   //程式判斷新增日期是不是初始化的值，若是就傳空，不然就傳修改後的值
  //              if (_FDSHOW_DATE == new DateTime(1900, 1, 1))
  //                  return string.Empty;
  //              else
  //                  return _FDSHOW_DATE.ToString("yyyyMMdd");
  //          }

// set { _FDUPDATED_DATE = value; }