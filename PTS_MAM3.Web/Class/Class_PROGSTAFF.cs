﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_PROGSTAFF
    {
        public string FSSTAFFID { get; set; }
        public string FSNAME { get; set; }
        public string FSENAME { get; set; }
        public string FSTEL { get; set; }
        public string FSFAX { get; set; }
        public string FSEMAIL { get; set; }
        public string FSADDRESS { get; set; }
        public string FSWEBSITE { get; set; }
        public string FSINTRO { get; set; }
        public string FSEINTRO { get; set; }
        public string FSMEMO { get; set; }
    }
}