﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class DFWaitToCheck_Main_Class
    {
        public string DF_CancleForm { get; set; }
        public string DF_hid_SendTo { get; set; }
        private string DF_Form_FormIdField;

        public string DF_Form_FormId
        {
            get { return DF_Form_FormIdField; }
            set { DF_Form_FormIdField = value; }
        }

        private string DF_QueryActivity_ApplynameField;

        public string DF_QueryActivity_Applyname
        {
            get { return DF_QueryActivity_ApplynameField; }
            set { DF_QueryActivity_ApplynameField = value; }
        }
        private string DF_QueryActivity_ActiveidField;

        public string DF_QueryActivity_Activeid
        {
            get { return DF_QueryActivity_ActiveidField; }
            set { DF_QueryActivity_ActiveidField = value; }
        }
        private string DF_QueryActivity_EnterdateField;

        public string DF_QueryActivity_Enterdate
        {
            get { return DF_QueryActivity_EnterdateField; }
            set { DF_QueryActivity_EnterdateField = value; }
        }
        private string DF_QueryActivity_VesionField;

        public string DF_QueryActivity_Vesion
        {
            get { return DF_QueryActivity_VesionField; }
            set { DF_QueryActivity_VesionField = value; }
        }
        private string DF_QueryActivity_StatusField;

        public string DF_QueryActivity_Status
        {
            get { return DF_QueryActivity_StatusField; }
            set { DF_QueryActivity_StatusField = value; }
        }
        private string DF_QueryActivity_DisplaynameField;

        public string DF_QueryActivity_Displayname
        {
            get { return DF_QueryActivity_DisplaynameField; }
            set { DF_QueryActivity_DisplaynameField = value; }
        }
        private string DF_QueryActivity_FinishdateField;

        public string DF_QueryActivity_Finishdate
        {
            get { return DF_QueryActivity_FinishdateField; }
            set { DF_QueryActivity_FinishdateField = value; }
        }
    }
}