﻿//=====================================
//2012/11/16 Daivd.Sin 新增刪除檔案類別
//=====================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_DELETED
    {
        public string _fsfile_no { set; get; }
        public string _fsfile_type { set; get; }
        public string _fscreated_by { set; get; }
        public string _fdcreated_date { set; get; }
        public string _fcstatus { set; get; }
        public string _fddeleted_date { set; get; }
        public string _fsmessage { set; get; }
        public string _fsmemo { set; get; }
    }
}