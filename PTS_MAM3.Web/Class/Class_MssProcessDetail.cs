﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PTS_MAM3.Web.WS.WSPGM;

namespace PTS_MAM3.Web
{
    public class Class_MssProcessDetail : ClassMSSProcessJob
    {
        public string FSFILE_NO { get; set; }//檔案編號
        public string FNSEG_ID { get; set; }//段落編號
        public string FSVIDEO_ID { get; set; }//VIDEO_ID
        public string FCLOW_RES { get; set; }
        public string FSBEG_TIMECODE { get; set; }//舊的TimeCode
        public string FSEND_TIMECODE { get; set; }
        public string FSBEG_TIMECODE_NEW { get; set; }//新的TimeCode
        public string FSEND_TIMECODE_NEW { get; set; }
        public string FSCREATED_BY { get; set; }//微調者id

        public string FSUSER_ChtName { get; set; }//微調者中文姓名

        public string FSPGMNAME { get; set; }//節目名稱
        public string FSPGDNAME { get; set; }//子集名稱
        public string FSID { get; set; }//節目編號
        public string FNEPISODE { get; set; }//集數
        public string FSVIDEO_PROG { get; set; }
        public string FDCREATED_DATE { get; set; }//微調的時間

    }
}