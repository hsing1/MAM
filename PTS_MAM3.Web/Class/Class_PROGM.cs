﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_PROGM
    {
        public string FSPROG_ID { get; set; }
        public string FSPGMNAME { get; set; }
        public string FSPGMENAME { get; set; }
        public string FSPRDDEPTID { get; set; }
        public string FSCHANNEL { get; set; }
        public short FNTOTEPISODE { get; set; }
        public short FNLENGTH { get; set; }
        public string FSPROGOBJID { get; set; }
        public string FSPROGSRCID { get; set; }
        public string FSPROGATTRID { get; set; }
        public string FSPROGAUDID { get; set; }
        public string FSPROGTYPEID { get; set; }
        public string FSPROGGRADEID { get; set; }
        public string FSPROGLANGID1 { get; set; }
        public string FSPROGLANGID2 { get; set; }
        public string FSPROGSALEID;
        public string FSPROGBUYID { get; set; }
        public string FSPROGBUYDID { get; set; }
        public string FSPRDCENID { get; set; }
        public string FSPRDYYMM { get; set; }
        public DateTime FDSHOWDATE { get; set; }
        public string FSCONTENT { get; set; }
        public string FSMEMO { get; set; }
        public string FSEWEBPAGE { get; set; }
        public string FCDERIVE { get; set; }
        public string FSWEBNAME { get; set; }
        public string FSCHANNEL_ID { get; set; }
        public string FSDEL { get; set; }
        public string FSDELUSER { get; set; }
        public string FCOUT_BUY { get; set; }
        public string FSPROGSPEC { get; set; }           //節目規格
        public int FNDEP_ID { get; set; }                //成案單位

        public string FSCRTUSER { get; set; }
        public DateTime FDCRTDATE { get; set; }
        public string FSUPDUSER { get; set; }
        public DateTime FDUPDDATE { get; set; }

        public string SHOW_FCDERIVE { get; set; }
        public string SHOW_FDSHOWDATE { get; set; }
        public string SHOW_FDCRTDATE { get; set; }
        public string SHOW_FDUPDDATE { get; set; }
        public string FSCRTUSER_NAME { get; set; }
        public string FSUPDUSER_NAME { get; set; }

        //代碼名稱
        public string FSPRDDEPTID_NAME { get; set; }     //製作部門代碼名稱
        public string FSPROGOBJID_NAME { get; set; }     //製作目的代碼名稱
        public string FSPROGSRCID_NAME { get; set; }     //節目來源代碼名稱
        public string FSPROGATTRID_NAME { get; set; }    //內容屬性代碼名稱
        public string FSPROGAUDID_NAME { get; set; }     //目標觀眾代碼名稱
        public string FSPROGTYPEID_NAME { get; set; }    //表現方式代碼名稱
        public string FSPROGGRADEID_NAME { get; set; }   //節目分級代碼名稱
        public string FSPROGLANGID1_NAME { get; set; }   //主聲道代碼名稱
        public string FSPROGLANGID2_NAME { get; set; }   //副聲道代碼名稱
        public string FSPROGSALEID_NAME { get; set; }    //行銷類別代碼名稱
        public string FSPROGBUYID_NAME { get; set; }     //外購類別代碼名稱
        public string FSPROGBUYDID_NAME { get; set; }    //外購類別細項代碼名稱
        public string FSPRDCENID_NAME { get; set; }      //製作單位代碼名稱
        public string FSCHANNEL_ID_NAME { get; set; }    //頻道別代碼名稱
        public string FSPROGSPEC_NAME { get; set; }      //節目規格名稱
        public string FNDEP_ID_NAME { get; set; }        //成案單位名稱
        public string FSPROGNATIONID_NAME { get; set; } //來源國家名稱

        public string FSEXPIRE_DATE_ACTION { get; set; }    //到期後是否刪除 2012/11/22 kyle
        public DateTime FDEXPIRE_DATE { get; set; }       //到期日期
        public string Origin_FSEXPIRE_DATE_ACTION { get; set; }    //原_到期後是否刪除 2012/11/22 kyle
        public DateTime Origin_FDEXPIRE_DATE { get; set; }          //原_到期日期
        public string FSPROGNATIONID { get; set; }          //來源國家代碼
    }
}