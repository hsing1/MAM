﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{

    /// <summary>
    /// strARC_ID:入庫單單號
    /// strProgramId:
    /// strFSID:節目代號
    /// strEpisodeId:集數
    /// strType: G:節目帶 P:宣傳帶
    /// strDirectory:TSM儲存路徑(維智)
    /// strFileId:取檔號碼(至軒)
    /// strOFileName:原始檔案名稱
    /// strOExtensionName:原始副檔名
    /// strChannelId:頻道Id
    /// strUserId:使用者代碼
    /// strUploadFileType
    /// </summary>
    public class Class_ARCHIVE_EXCHANGE
    {
        public string FSARC_ID { get; set; }
        public string FSTYPE { get; set; }
        public string FSID { get; set; }
        public string FSLESSCLAM { get; set; }
        public string FSSUPERVISOR { get; set; }
        public string FSCHANNEL_ID { get; set; }
        public short FNEPISODE { get; set; }
        public DateTime FDARC_DATE { get; set; }
        public string FSARC_BY { get; set; }
        public string FCCHECK_STATUS { get; set; }
        public string FSCREATED_BY { get; set; }
        public DateTime FDCREATED_DATE { get; set; }
        public string FSUPDATED_BY { get; set; }
        public DateTime FDUPDATED_DATE { get; set; }
        //由其他表單所串而來的欄位
        public string FSCREATED_BY_NAME { get; set; }
        public string FSUPDATED_BY_NAME { get; set; }
        public string WANT_FDCREATEDDATE { get; set; }
        public string FSPROG_ID_NAME { get; set; }
        public string FNEPISODE_NAME { get; set; }
    }
}