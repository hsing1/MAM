﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_PROMO
    {
        public string FSPROMO_ID { get; set; }
        public string FSPROMO_NAME { get; set; }
        public string FSPROMO_NAME_ENG { get; set; }
        public string FSDURATION { get; set; }
        public string FSPROG_ID { get; set; }
        public string FSPROG_ID_NAME { get; set; }
        public short FNEPISODE { get; set; }
        public string SHOW_FNEPISODE { get; set; }
        public string FNEPISODE_NAME { get; set; }
        public string FSPROMOTYPEID { get; set; }
        public string FSPROGOBJID { get; set; }
        public string FSPRDCENID { get; set; }
        public string FSPRDYYMM { get; set; }
        public string FSACTIONID { get; set; }
        public string FSPROMOCATID { get; set; }
        public string FSPROMOCATDID { get; set; }
        public string FSPROMOVERID { get; set; }
        public string FSMAIN_CHANNEL_ID { get; set; }
        public string FSMEMO { get; set; }
        public string FSDEL { get; set; }
        public string FSDELUSER { get; set; }
        public int FNDEP_ID { get; set; }
        public string FSWELFARE { get; set; }

        public string FSCREATED_BY { get; set; }
        public DateTime FDCREATED_DATE { get; set; }
        public string FSUPDATED_BY { get; set; }
        public DateTime FDUPDATED_DATE { get; set; }

        public string SHOW_FDCREATED_DATE { get; set; }//建檔日期(顯示)
        public string SHOW_FDUPDATED_DATE { get; set; }//修改日期(顯示)
        public string FSCREATED_BY_NAME { get; set; }  //建檔者(顯示)
        public string FSUPDATED_BY_NAME { get; set; }  //修改者(顯示)
        public string FSDEL_BY_NAME { get; set; }      //刪除者(顯示)


        //代碼名稱
        public string FSPRDCEN_NAME { get; set; }     //製作單位代碼名稱
        public string FSPROGOBJ_NAME { get; set; }    //製作目的代碼名稱
        public string FSPROMOCAT_NAME { get; set; }   //宣傳帶類型代碼名稱
        public string FSPROMOCATD_NAME { get; set; }  //宣傳帶類型細項代碼名稱

        public string FSPROMOVER_NAME { get; set; }   //宣傳帶版本名稱
        public string FSPROMOTYPE_NAME { get; set; }  //宣傳帶類別名稱
        public string FSACTION_NAME { get; set; }     //宣傳帶活動名稱

        public string FSCHANNEL_ID_NAME { get; set; } //頻道別代碼名稱
        public string SHOW_FSDURATION { get; set; }   //顯示的播出長度
        public string FNDEP_ID_NAME { get; set; }     //成案單位名稱

        public string FSEXPIRE_DATE_ACTION { get; set; }    //到期後是否刪除 2012/11/22 kyle
        public DateTime FDEXPIRE_DATE { get; set; }       //到期日期
        public string Origin_FSEXPIRE_DATE_ACTION { get; set; }    //原_到期後是否刪除 2012/11/22 kyle
        public DateTime Origin_FDEXPIRE_DATE { get; set; }          //原_到期日期
    }
    
}