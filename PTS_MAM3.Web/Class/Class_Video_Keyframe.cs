﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_Video_Keyframe
    {
        public string FSSUBJECT_ID { get; set; }
        public string FSVIDEO_NO { get; set; }
        public string FSFILE_NO { get; set; }
        public string FSCREATED_BY { get; set; }
        public string FDCREATED_DATE { get; set; }
        public int FNFILE_SIZE { get; set; }
        public int FNDIR_ID { get; set; }
        public string FSSERVER_NAME { get; set; }
        public string FSSHARE_FOLDER { get; set; }
        public string FSDESCRIPTION { get; set; }
        public string path { get; set; }  //檔案路徑
 
    }
}