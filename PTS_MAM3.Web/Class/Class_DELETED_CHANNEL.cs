﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_DELETED_CHANNEL
    {
        public string _fschannel_id { set; get; }
        public string _fschannel_name { set; get; }
        public bool _fbis_checked { set; get; }

    }
}