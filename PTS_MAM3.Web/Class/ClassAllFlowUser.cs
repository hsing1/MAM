﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class ClassAllFlowUser
    {
        public string FSUSER_ID { get; set; }
        public string FSUSER { get; set; }
        public string FSUSER_ChtName { get; set; }
        public string FSEMAIL { get; set; }
        public string FSDep_ChtName { get; set; }
        public string FSGroup_ChtName { get; set; }
    }
}