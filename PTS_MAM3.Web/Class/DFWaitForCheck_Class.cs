﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PTS_MAM3.Web.Class
{
    [DataContract]
    public class DFWaitForCheck_Class
    {
        //是否可以修改表單在GetTTL中
        #region 每個表單的變數值
        [DataMember]
        public string NextXamlName { get; set; }
        [DataMember]
        public string FormInfo { get; set; }
        [DataMember]
        public string FlowResult { get; set; }
        [DataMember]
        public string Function01{get; set;}
        [DataMember]
        public string Function02 { get; set; }
        [DataMember]
        public string Function03 { get; set; }
        [DataMember]
        public string Function04{ get; set; }
        [DataMember]
        public string Function05 { get; set; }
        [DataMember]
        public string FormId { get; set; }
        #endregion
        [DataMember]
        public string FormUrl { get; set; }

        //[Display(Name = "申請者帳號", Description = "申請者的帳號")]
        //[Required(ErrorMessage = "帳號是必須資料")]
        [DataMember]
        public string Applyname { get; set; }

        [DataMember]
        public string ReceiveDate { get; set; }

        [DataMember]
        public string Priority { get; set; }

        [DataMember]
        public string Title { get; set; }


        //用於儲存表單Id的
        //[Display(Name = "在Flow中表單的Id", Description = "在Flow中表單的Id")]
        //[Required(ErrorMessage = "表單Id是必須資料")]

        [DataMember]
        public string Ttlid { get; set; }

        [DataMember]
        public string Version { get; set; }

        [DataMember]
        public string HaveAttach{ get; set; }

        [DataMember]
        public string Remark{ get; set; }
        [DataMember]
        public string Receiveid{ get; set; }


        [DataMember]
        public string Batch{ get; set; }

        [DataMember]
        public string OriginalName{ get; set; }

        [DataMember]
        public string FlowChartUrl{ get; set; }

        [DataMember]
        public string flowid{ get; set; }

        [DataMember]
        public string OverDueDate { get; set; }

        [DataMember]
        public string Addsign{ get; set; }

        [DataMember]
        public string Nodeid{ get; set; }
        [DataMember]
        public string Roleid { get; set; }

    }
}