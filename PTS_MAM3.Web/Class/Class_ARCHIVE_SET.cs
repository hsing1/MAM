﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_ARCHIVE_SET
    {
        public DateTime FDCREATED_DATE { get; set; }

        public Nullable<DateTime> FDUPDATED_DATE { get; set; }

        public string FNAUDIO { get; set; }

        public string FNDOC { get; set; }

        public short FNEPISODE { get; set; }

        public string FNPHOTO { get; set; }

        public string FNVIDEO { get; set; }

        public string FSCREATED_BY { get; set; }

        public string FSID { get; set; }

        public string FSTYPE { get; set; }

        public string FSUPDATED_BY { get; set; }

        public int SET_ID { get; set; }

        public string FSPGMNAME { get; set; }
        public string FSPGDNAME { get; set; }
    }
}