﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_DELETED_PARAMETER
    {
        public string _fschannel_id { set; get; }
        public string _fstype { set; get; }
        public string _fsprog_id { set; get; }
        public string _fnepisode_beg { set; get; }
        public string _fnepisode_end { set; get; }
        public string _fsfile_type { set; get; }
        public string _fsfile_no { set; get; }
        public string _fdtran_date_beg { set; get; }
        public string _fdtran_date_end { set; get; }
        public string _fcfile_status { set; get; }
    }
}