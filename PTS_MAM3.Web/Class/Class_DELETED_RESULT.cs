﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace PTS_MAM3.Web.Class
{
    public class Class_DELETED_RESULT
    {
        public string _fsprog_id { set; get; }
        public string _fsprog_name { set; get; }
        public string _fnepisode { set; get; }
        public string _fschannel { set; get; }
        public string _fsfile_no { set; get; }
        public string _fdtran_date { set; get; }
        public string _fstran_by { set; get; }
        public string _fscreated_by { set; get; }
        public string _fsfile_status { set; get; }
        public string _fsarc_type { set; get; }
        public string _fstbname { set; get; }
        public bool _fbis_checked { set; get; }
        public string _fsview { set; get; }
        //瀏覽按鈕高度
        public int _fnbutton_height { set; get; }
    }
}