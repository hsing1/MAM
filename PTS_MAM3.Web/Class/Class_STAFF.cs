﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_STAFF
    {
        public string FSSTAFFID { get; set; }           //工作人員代碼
        public string FSNAME { get; set; }              //姓名
        public string FSENAME { get; set; }             //外文姓名
        public string FSTEL { get; set; }               //電話
        public string FSFAX { get; set; }               //傳真
        public string FSEMAIL { get; set; }             //E-Mail 地址
        public string FSADDRESS { get; set; }           //地址
        public string FSWEBSITE { get; set; }           //個人網站
        public string FSINTRO { get; set; }             //簡介
        public string FSEINTRO { get; set; }            //外文簡介
        public string FSMEMO { get; set; }              //備註

        public string FSCRTUSER { get; set; }           //建檔人
        public DateTime FDCRTDATE { get; set; }         //建檔日期
        public string FSUPDUSER { get; set; }           //修改人
        public DateTime FDUPDDATE { get; set; }         //修改日期

        public string SHOW_FDCRTDATE { get; set; }      
        public string SHOW_FDUPDDATE { get; set; }
    }
}