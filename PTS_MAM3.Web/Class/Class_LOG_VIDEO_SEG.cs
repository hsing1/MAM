﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_LOG_VIDEO_SEG
    {
        public string FSFILE_NO { get; set; }
        public string FNSEG_ID { get; set; }
        public string FSVIDEO_ID { get; set; }
        public string FSBEG_TIMECODE { get; set; }
        public string FSEND_TIMECODE { get; set; }
        public string FSSUB_TIMECODE { get; set; }
        public string FCLOW_RES { get; set; }
        public int FNBEG_TIMECODE_FRAME { get; set; }  //起始短落Frame數
        public int FNEND_TIMECODE_FRAME { get; set; }  //結束短落Frame數

        public string FSCREATED_BY { get; set; }
        public DateTime FDCREATED_DATE { get; set; }
        public string FSUPDATED_BY { get; set; }
        public DateTime FDUPDATED_DATE { get; set; }

        public string SHOW_FDCREATED_DATE { get; set; }
        public string SHOW_FDUPDATED_DATE { get; set; }    

        public string FSCREATED_BY_NAME { get; set; }
        public string FSUPDATED_BY_NAME { get; set; }
    }
}