﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_PROGLINK
    {
        public string FSPROG_ID { get; set; }
        public string FSPROG_ID_NAME { get; set; }
        public short FNEPISODE { get; set; }
        public string FNEPISODE_NAME { get; set; }

        public string FSORGPROG_ID { get; set; }
        public string FSORGPROG_ID_NAME { get; set; }
        public short FNORGEPISODE { get; set; }
        public string FNORGEPISODE_NAME { get; set; }
        public string FSMEMO { get; set; }

        public string FSCREATED_BY { get; set; }
        public DateTime FDCREATED_DATE { get; set; }
        public string FSUPDATED_BY { get; set; }
        public DateTime FDUPDATED_DATE { get; set; }

        public string FSCREATED_BY_NAME { get; set; }
        public string FSUPDATED_BY_NAME { get; set; }

        public string SHOW_FDCREATED_DATE { get; set; }
        public string SHOW_FNEPISODE { get; set; }
        public string SHOW_ORGFNEPISODE { get; set; }
        public string SHOW_FNORGEPISODE_PLAY { get; set; }        
    }
}