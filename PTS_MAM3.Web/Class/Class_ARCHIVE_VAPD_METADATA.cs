﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_ARCHIVE_VAPD_METADATA:Class_ARCHIVE_VAPD
    {
        public string MTTitle { get; set; }
        public string MTDescription { get; set; }
        public string FCFILE_STATUS { get; set; }
        public string FSTYPE { get; set; }
        public string FSSUPERVISOR { get; set; }
    }
}