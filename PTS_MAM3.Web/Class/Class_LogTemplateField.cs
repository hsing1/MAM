﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class LogTemplateField
    {
        //FNID, FSFIELD, FSFIELD_NAME FSFIELD_TYPE, FNFIELD_LENGTH, FSDESCRIPTION, FNORDER, FNOBJECT_WIDTH, FCISNULLABLE, FSDEFAULT
        public string FNID { get; set; }
        public string FSFIELD { get; set; }
        public string FSFIELD_NAME { get; set; }     
        public string FSFIELD_TYPE { get; set; }
        public string FNFIELD_LENGTH { get; set; }
        public string FSDESCRIPTION { get; set; }
        public string FNORDER { get; set; }
        public string FNOBJECT_WIDTH { get; set; }
        public string FCISNULLABLE { get; set; }
        public string FSDEFAULT { get; set; }
        public string FSFIELD_VALUE { get; set; }

        //(2011-05-31)Dennis.Wen: 列舉功能修改
        public string FSCODE_ID { get; set; }
        public string FSCODE_CNT { get; set; }
        public string FSCODE_CTRL { get; set; }

        public List<Class.LogTemplateField_CODE> CODELIST { get; set; }
    }

    public class LogTemplateField_CODE
    {
        public string FSCODE_ID { get; set; }
        public string FSCODE_CODE { get; set; }
        public string FSCODE_NAME { get; set; }
        public string FSCODE_ENAME { get; set; }
        public string FSCODE_ORDER { get; set; }
        public string FSCODE_SET { get; set; }
        public string FSCODE_NOTE { get; set; }
        public string FSCODE_CHANNEL_ID { get; set; } 
        public Boolean FSCODE_ISENABLED { get; set; }
        
        public string _ISENABLED { get; set; }

        public String FSCREATED_BY { get; set; }
        public DateTime FDCREATED_DATE { get; set; }
        public String FSUPDATED_BY { get; set; }
        public DateTime FDUPDATED_DATE { get; set; }
    }

    public class LogTemplateField_CODESET
    {
        public string FSCODE_ID { get; set; }
        public string FSCODE_TITLE { get; set; }
        public string FSCODE_TBCOL { get; set; }
        public string FSCODE_NOTE { get; set; }
        public Boolean FSCODE_ISENABLED { get; set; }
         
        public int _COUNT { get; set; }
        public string _ISENABLED { get; set; }
      
        public String FSCREATED_BY { get; set; }
        public DateTime FDCREATED_DATE { get; set; }
        public String FSUPDATED_BY { get; set; }
        public DateTime FDUPDATED_DATE { get; set; }
    }
}