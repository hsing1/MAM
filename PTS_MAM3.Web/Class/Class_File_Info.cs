﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_File_Info
    {
        public string FSFile_Name;          //檔案名稱
        public string FSFile_Size;          //檔案大小
        public string FSFile_Extension;     //附檔名
        public string FSFile_LastWriteTime; //最後修改時間
        public string FSFile_Path;          //檔案完整路徑
    }
}