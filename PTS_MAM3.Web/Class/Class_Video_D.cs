﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_Video_D
    {
        public string FSFILE_NO { get; set; }
        public string FNSEQ_NO { get; set; }
        public string FSSUBJECT_ID { get; set; }
        public string FSKEYFRAME_NO { get; set; }
        public string FSTYPE { get; set; }
        public string FSID { get; set; }
        public int FNEPISODE { get; set; }
        public string FSSUPERVISOR { get; set; } 
        public string FSTITLE { get; set; }
        public string FSDESCRIPTION { get; set; }
        public string FCFILE_STATUS { get; set; }
        public string FSCHANGE_FILE_NO { get; set; }
        public string FSOLD_FILE_NAME { get; set; }
        public string FNDIR_ID { get; set; }
        public string FCFROM { get; set; } 
        public string FSCHANNEL_ID { get; set; }
        public string FSBEG_TIMECODE { get; set; }
        public string FSEND_TIMECODE { get; set; }
        public string FSCREATED_BY { get; set; }
        public string FDCREATED_DATE { get; set; }
        public string FSUPDATED_BY { get; set; }
        public string FDUPDATED_DATE { get; set; }
        public string FSATTRIBUTE1 { get; set; }
        public string FSATTRIBUTE2 { get; set; }
        public string FSATTRIBUTE3 { get; set; }
        public string FSATTRIBUTE4 { get; set; }
        public string FSATTRIBUTE5 { get; set; }
        public string FSATTRIBUTE6 { get; set; }
        public string FSATTRIBUTE7 { get; set; }
        public string FSATTRIBUTE8 { get; set; }
        public string FSATTRIBUTE9 { get; set; }
        public string FSATTRIBUTE10 { get; set; }
        public string FSATTRIBUTE11 { get; set; }
        public string FSATTRIBUTE12 { get; set; }
        public string FSATTRIBUTE13 { get; set; }
        public string FSATTRIBUTE14 { get; set; }
        public string FSATTRIBUTE15 { get; set; }
        public string FSATTRIBUTE16 { get; set; }
        public string FSATTRIBUTE17 { get; set; }
        public string FSATTRIBUTE18 { get; set; }
        public string FSATTRIBUTE19 { get; set; }
        public string FSATTRIBUTE20 { get; set; }
        public string FSATTRIBUTE21 { get; set; }
        public string FSATTRIBUTE22 { get; set; }
        public string FSATTRIBUTE23 { get; set; }
        public string FSATTRIBUTE24 { get; set; }
        public string FSATTRIBUTE25 { get; set; }
        public string FSATTRIBUTE26 { get; set; }
        public string FSATTRIBUTE27 { get; set; }
        public string FSATTRIBUTE28 { get; set; }
        public string FSATTRIBUTE29 { get; set; }
        public string FSATTRIBUTE30 { get; set; }
        public string FSATTRIBUTE31 { get; set; }
        public string FSATTRIBUTE32 { get; set; }
        public string FSATTRIBUTE33 { get; set; }
        public string FSATTRIBUTE34 { get; set; }
        public string FSATTRIBUTE35 { get; set; }
        public string FSATTRIBUTE36 { get; set; }
        public string FSATTRIBUTE37 { get; set; }
        public string FSATTRIBUTE38 { get; set; }
        public string FSATTRIBUTE39 { get; set; }
        public string FSATTRIBUTE40 { get; set; }
        public string FSATTRIBUTE41 { get; set; }
        public string FSATTRIBUTE42 { get; set; }
        public string FSATTRIBUTE43 { get; set; }
        public string FSATTRIBUTE44 { get; set; }
        public string FSATTRIBUTE45 { get; set; }
        public string FSATTRIBUTE46 { get; set; }
        public string FSATTRIBUTE47 { get; set; }
        public string FSATTRIBUTE48 { get; set; }
        public string FSATTRIBUTE49 { get; set; }
        public string FSATTRIBUTE50 { get; set; }
    }


  
}