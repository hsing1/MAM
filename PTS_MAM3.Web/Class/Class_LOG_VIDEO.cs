﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_LOG_VIDEO
    {
        public string FSFILE_NO { get; set; }
        public string FSSUBJECT_ID { get; set; }
        public string FSTYPE { get; set; }
        public string FSID { get; set; }
        public short FNEPISODE { get; set; }
        public string SHOW_FNEPISODE { get; set; }
        public string FSPLAY { get; set; }
        public string FSARC_TYPE { get; set; }
        public string FSARC_TYPE_NAME { get; set; }
        public string FSARC_SPEC_NAME { get; set; }
        public string FSFILE_TYPE_HV { get; set; }
        public string FSFILE_TYPE_LV { get; set; }
        public string FSTITLE { get; set; }
        public string FSDESCRIPTION { get; set; }
        public string FSFILE_SIZE { get; set; }
        public string FCFILE_STATUS { get; set; }
        public string FSCHANGE_FILE_NO { get; set; }
        public string FSOLD_FILE_NAME { get; set; }
        public string FSFILE_PATH_H { get; set; }
        public string FSFILE_PATH_L { get; set; }
        public long FNDIR_ID { get; set; }
        public string SHOW_FNDIR_ID { get; set; }
        public string FSCHANNEL_ID { get; set; }
        public string FSBEG_TIMECODE { get; set; }
        public string FSEND_TIMECODE { get; set; }
        public string FSBRO_ID { get; set; }
        public string FSARC_ID { get; set; }

        public string FSSUPERVISOR { get; set; }  //調用通知主管
        public string FSJOB_ID { get; set; }      //轉檔進度代碼(資料庫是BigInt,但這用string)
        public string FCFROM { get; set; }        //資料來源
        public string FCPROGRESS { get; set; }    //轉檔進度
        public string FSVIDEO_PROG { get; set; }  //VIDEO ID
        public string FSTAPE_ID { get; set; }     //影帶編號
        public string FSTRANS_FROM { get; set; }  //轉檔來源

        public string FSCREATED_BY { get; set; }
        public DateTime FDCREATED_DATE { get; set; }
        public string FSUPDATED_BY { get; set; }
        public DateTime FDUPDATED_DATE { get; set; }

        public string SHOW_FDCREATED_DATE { get; set; }
        public string SHOW_FDUPDATED_DATE { get; set; }

        public string FSCREATED_BY_NAME { get; set; }
        public string FSUPDATED_BY_NAME { get; set; }
        public string FCFILE_STATUS_NAME { get; set; }

        public string FSTRACK { get; set; }//2014-09-19 新增音軌設定 by Jarvis
        public string FSVIDEO_PROPERTY { get; set; }//2015-12-04 用來儲存MediaInfo的影片資訊 by Jarvis
        public string FCLOW_RES { get; set; }//暫時沒在用
        public string Brocast_Status { get; set; } //所屬的送帶轉檔單的狀態
        /// <summary>
        /// 為了送帶轉檔單複製所加入的類別
        /// 2012/9/28 By Kyle
        /// </summary>
        public List<Class_LOG_VIDEO_SEG> VideoListSeg { get; set; }
    }
}