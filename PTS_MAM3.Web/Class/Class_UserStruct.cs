﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.DataSource
{
    public class UserStruct
    {
       
        public string FSUSER_ID;
       
        public string FSUSER;
      
        public string FSUSER_ChtName;
      
        public string FSPASSWD;
       
        public string FSDEPT;
     
        public string FSEMAIL;
     
        public string FCACTIVE;
      
        public string FSDESCRIPTION;
       
        public string FCSECRET;
  
        public int? FNDEP_ID;
        
        public int? FNUPPERDEPT_ID;
      
        public int? FNGROUP_ID;

        public int? FNTreeViewDept_ID;
     
        public string FSUPPERDEPT_CHTNAME;
       
        public string FSDEP_CHTNAME;
     
        public string FSGROUP_CHTNAME;

        public string FSTITLEOFDEPT_CHTNAME;

        public string FNTreeViewDept_ChtName;

        public string FSCREATED_BY;
     
        public DateTime FDCREATED_DATE;
      
        public string FSUPDATED_BY;
     
        public DateTime? FDUPDATED_DATE;
       
        public string FSUSER_TITLE;
       
        public string FSCHANNEL_ID;
      
        public string FSSESSION_ID;

        public string FSTITLE_NAME;
    }

}