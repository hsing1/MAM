﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_Dept
    {
        public int FNDEP_ID;
        public string FSDEP;
        public int? FNPARENT_DEP_ID;
        public string FSSupervisor_ID;
        public string FSUpDepName_ChtName;
        public string FSDpeName_ChtName;
        public string FSFullName_ChtName;
        public int? FNDEP_LEVEL;
        public string FBIsDepExist;
        public string FSDEP_MEMO;
    }
}