﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_TBLOG_VIDEO_HD_MC
    {
        public string FSBRO_ID { get; set; }
        public string FSFILE_NO { get; set; }
        public string FSVIDEO_ID { get; set; }
        public string FCSTATUS { get; set; }
        public string FSCREATED_BY { get; set; }
        public string FDCREATED_DATE { get; set; }
        public string FSUPDATED_BY { get; set; }
        public string FDUPDATED_DATE { get; set; }

    }
}