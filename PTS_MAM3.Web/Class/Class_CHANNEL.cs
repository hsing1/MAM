﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_CHANNEL
    {
        public string FSCHANNEL_ID { get; set; }
        public string FSCHANNEL_NAME { get; set; }
        public string FSSHOWNAME { get; set; }
        public string FSCHANNEL_TYPE { get; set; }
        public string FSSORT { get; set; }
        public string FSSHORTNAME { get; set; }
        public string FSOLDTABLENAME { get; set; }
        public string FSTSM_POOLNAME { get; set; }
        public string FSCREATED_BY { get; set; }
        public DateTime FDCREATED_DATE { get; set; }
        public string FSUPDATED_BY { get; set; }
        public DateTime FDUPDATED_DATE { get; set; }

    }
}