﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_ReviewTree
    {
        public string StationID;    //頻道ID 01公視 07客家 08宏觀 09原民
        public string CatalogID;    //目錄 G節目 P短帶 D資料帶 
        public string YearID;       //分年
        public string FSID;         //節目ID
        public string FSNAME;       //中文名稱
        
        public Class_ReviewTree()
        {
            this.StationID = "";
            this.CatalogID = "";
            this.YearID = "";
            this.FSID = "";
            this.FSNAME = "";
        }
    }

}