﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_PROGMEN
    {
        public string AUTO { get; set; }
        public string FSPROG_ID { get; set; }
        public string FSPROG_ID_NAME { get; set; }
        public short FNEPISODE { get; set; }
        public string FNEPISODE_NAME { get; set; }

        public string FSTITLEID { get; set; }
        public string FSTITLENAME { get; set; }
        public string FSEMAIL { get; set; }
        public string FSSTAFFID { get; set; }
        public string FSSTAFFNAME { get; set; }
        public string FSNAME { get; set; }
        public string FSMEMO { get; set; }

        public string FSCREATED_BY { get; set; }
        public DateTime FDCREATED_DATE { get; set; }
        public string FSUPDATED_BY { get; set; }
        public DateTime FDUPDATED_DATE { get; set; }

        public string FSCREATED_BY_NAME { get; set; }
        public string FSUPDATED_BY_NAME { get; set; }

        public string SHOW_FDRACEDATE { get; set; }
        public string SHOW_FDCREATED_DATE { get; set; }
        public string SHOW_FNEPISODE { get; set; }
        public string SHOW_FDUPDATED_DATE { get; set; }
    }
}