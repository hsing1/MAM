﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_TAPE_LABEL_CLIP
    {
        public string FSBIB { get; set; }	
        public string FSTCSTART { get; set; }	
        public string FSTCEND { get; set; }	
        public string FSDURATION { get; set; }	
        public string FSCLIPTITLE { get; set; }
        public string FSVIDEO_ID { get; set; }         //主控播出編碼
        public int FNBEG_TIMECODE_FRAME { get; set; }  //起始短落Frame數
        public int FNEND_TIMECODE_FRAME { get; set; }  //結束短落Frame數

        public string FSCREATED_BY_NAME { get; set; }  //建檔者(顯示)
        public string FSUPDATED_BY_NAME { get; set; }  //修改者(顯示)
        public DateTime FDCREATED_DATE { get; set; }
        public DateTime FDUPDATED_DATE { get; set; }

        public string SHOW_FDCREATED_DATE { get; set; }//建檔日期(顯示)
        public string SHOW_FDUPDATED_DATE { get; set; }//修改日期(顯示)

    }
}