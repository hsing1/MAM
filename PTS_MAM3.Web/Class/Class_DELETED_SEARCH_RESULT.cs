﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_DELETED_SEARCH_RESULT
    {
        public string _fsprog_name { set; get; }
        public string _fnepisode { set; get; }
        public string _fsfile_no { set; get; }
        public string _fdcreated_date { set; get; }
        public string _fscreated_by { set; get; }
        public string _fsmemo { set; get; }
        public string _fsarc_type_name { set; get; }
        public string _fstype { set; get; }
        public string _fsfile_type { set; get; }
    }
}