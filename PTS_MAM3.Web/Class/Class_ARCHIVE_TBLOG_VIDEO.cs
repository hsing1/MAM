﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_ARCHIVE_TBLOG_VIDEO:ClassUploadFileClass
    {
        public string iherFSFILE_NO { get; set; }
        public string iherFSSUBJECT_ID { get; set; }
        public string iherFSPLAY { get; set; }
        public string iherFSFILE_TYPE_HV { get; set; }
        public string iherFSFILE_TYPE_LV { get; set; }
        public string iherFCFILE_STATUS { get; set; }
        public string iherFSFILE_PATH_H { get; set; }
        public string iherFSFILE_PATH_L { get; set; }
        public string iherFSCREATED_BY { get; set; }
        public string iherFDCREATED_DATE { get; set; }
        public string iherFSUPDATED_BY { get; set; }
        public string iherFDUPDATED_DATE { get; set; }
        public string iherforeingFSPGMNAME { get; set; }
        public string iherforeingFSTYPE_NAME { get; set; }
    }
}