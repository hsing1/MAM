﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_PROGOBJ_DEPT
    {
        public string FSOBJ_ID { get; set; }                //製作目的
        public string FSBEF_DEPT_ID { get; set; }           //前會部門
        public string FSAFT_DEPT_ID { get; set; }           //後會部門

        public string FSCREATED_BY { get; set; }
        public DateTime FDCREATED_DATE { get; set; }
        public string FSUPDATED_BY { get; set; }
        public DateTime FDUPDATED_DATE { get; set; }

        public string SHOW_FDCREATED_DATE { get; set; }     //建檔日期(顯示)
        public string SHOW_FDUPDATED_DATE { get; set; }     //修改日期(顯示)
        public string FSCREATED_BY_NAME { get; set; }       //建檔者(顯示)
        public string FSUPDATED_BY_NAME { get; set; }       //修改者(顯示)
        public string SHOW_FSOBJ_ID { get; set; }           //製作目的名稱(顯示)
        public string SHOW_FSBEF_DEPT_ID { get; set; }      //前會部門名稱(顯示)
        public string SHOW_FSAFT_DEPT_ID { get; set; }      //後會部門名稱(顯示)
    }
}