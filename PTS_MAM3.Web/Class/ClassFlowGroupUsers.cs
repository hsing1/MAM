﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class ClassFlowGroupUsers
    {
        public string GroupID { get; set; }
        public string UserId { get; set; }
        public string Member_DisplayName { get; set; }
    }
}