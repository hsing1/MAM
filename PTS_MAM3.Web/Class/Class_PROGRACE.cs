﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.Class
{
    public class Class_PROGRACE
    {
        public string AUTO { get; set; }
        public string FSPROG_ID  { get; set; }
        public string FSPROG_ID_NAME { get; set; }
        public short  FNEPISODE  { get; set; }
        public string FNEPISODE_NAME { get; set; }
        public string FSAREA  { get; set; }
        public string FSAREA_NOT_TAIWAN { get; set; }   //查詢非台灣
        public string FSPROGRACE  { get; set; }       
        public DateTime FDRACEDATE { get; set; }
        public DateTime FDRACEDATE_END { get; set; }   
        public string FSPROGSTATUS  { get; set; }
        public string FSPROGSTATUS_IN { get; set; }     //入圍
        public string FSPROGSTATUS_WIN { get; set; }    //得獎並存
        public string FSPROGSTATUS_NOTWIN { get; set; } //得獎互斥
        public short  FNPERIOD  { get; set; }
        public string FSPROGWIN  { get; set; }
        public string FSPROGNAME { get; set; }          
        public string FSMEMO  { get; set; }
        public string FSPROGSRCID { get; set; }
        public string FSPROGSRC_NAME { get; set; }
        public string FSCHANNEL_ID { get; set; }
        public string FSCHANNEL_ID_NAME { get; set; }    //頻道別代碼名稱
        public string FSCREATED_BY { get; set; }
        public DateTime FDCREATED_DATE { get; set; }
        public string FSUPDATED_BY { get; set; }
        public DateTime FDUPDATED_DATE { get; set; }

        public string FSCREATED_BY_NAME { get; set; }
        public string FSUPDATED_BY_NAME { get; set; }

        public string SHOW_FDRACEDATE { get; set; }
        public string SHOW_FDCREATED_DATE { get; set; }
        public string SHOW_FDUPDATED_DATE { get; set; }

        public string SHOW_FNEPISODE { get; set; }
        public string SHOW_FNPERIOD { get; set; }
        public string FSAREA_NAME { get; set; }
    }
}

