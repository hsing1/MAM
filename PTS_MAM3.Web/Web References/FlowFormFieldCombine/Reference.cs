﻿//------------------------------------------------------------------------------
// <auto-generated>
//     這段程式碼是由工具產生的。
//     執行階段版本:4.0.30319.1
//
//     對這個檔案所做的變更可能會造成錯誤的行為，而且如果重新產生程式碼，
//     變更將會遺失。
// </auto-generated>
//------------------------------------------------------------------------------

// 
// 原始程式碼已由 Microsoft.VSDesigner 自動產生，版本 4.0.30319.1。
// 
#pragma warning disable 1591

namespace PTS_MAM3.Web.FlowFormFieldCombine {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.ComponentModel;
    using System.Xml.Serialization;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="BasicHttpBinding_FlowFormFieldCombine", Namespace="http://tempuri.org/")]
    public partial class FlowFormFieldCombine : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback IsTheAccountExistOperationCompleted;
        
        private System.Threading.SendOrPostCallback DFWaitYourCheckOperationCompleted;
        
        private System.Threading.SendOrPostCallback DFWaitToCheck_MainOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public FlowFormFieldCombine() {
            this.Url = global::PTS_MAM3.Web.Properties.Settings.Default.PTS_MAM3_Web_FlowFormFieldCombine_FlowFormFieldCombine;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event IsTheAccountExistCompletedEventHandler IsTheAccountExistCompleted;
        
        /// <remarks/>
        public event DFWaitYourCheckCompletedEventHandler DFWaitYourCheckCompleted;
        
        /// <remarks/>
        public event DFWaitToCheck_MainCompletedEventHandler DFWaitToCheck_MainCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("urn:FlowFormFieldCombine/IsTheAccountExist", RequestNamespace="", ResponseNamespace="", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string IsTheAccountExist([System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] string userName) {
            object[] results = this.Invoke("IsTheAccountExist", new object[] {
                        userName});
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void IsTheAccountExistAsync(string userName) {
            this.IsTheAccountExistAsync(userName, null);
        }
        
        /// <remarks/>
        public void IsTheAccountExistAsync(string userName, object userState) {
            if ((this.IsTheAccountExistOperationCompleted == null)) {
                this.IsTheAccountExistOperationCompleted = new System.Threading.SendOrPostCallback(this.OnIsTheAccountExistOperationCompleted);
            }
            this.InvokeAsync("IsTheAccountExist", new object[] {
                        userName}, this.IsTheAccountExistOperationCompleted, userState);
        }
        
        private void OnIsTheAccountExistOperationCompleted(object arg) {
            if ((this.IsTheAccountExistCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.IsTheAccountExistCompleted(this, new IsTheAccountExistCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("urn:FlowFormFieldCombine/DFWaitYourCheck", RequestNamespace="", ResponseNamespace="", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string DFWaitYourCheck([System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] string userName) {
            object[] results = this.Invoke("DFWaitYourCheck", new object[] {
                        userName});
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void DFWaitYourCheckAsync(string userName) {
            this.DFWaitYourCheckAsync(userName, null);
        }
        
        /// <remarks/>
        public void DFWaitYourCheckAsync(string userName, object userState) {
            if ((this.DFWaitYourCheckOperationCompleted == null)) {
                this.DFWaitYourCheckOperationCompleted = new System.Threading.SendOrPostCallback(this.OnDFWaitYourCheckOperationCompleted);
            }
            this.InvokeAsync("DFWaitYourCheck", new object[] {
                        userName}, this.DFWaitYourCheckOperationCompleted, userState);
        }
        
        private void OnDFWaitYourCheckOperationCompleted(object arg) {
            if ((this.DFWaitYourCheckCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.DFWaitYourCheckCompleted(this, new DFWaitYourCheckCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("urn:FlowFormFieldCombine/DFWaitToCheck_Main", RequestNamespace="", ResponseNamespace="", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string DFWaitToCheck_Main([System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] string userName, [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] string FlowStatus, [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] string SDate, [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] string EDate, int Flowid, [System.Xml.Serialization.XmlIgnoreAttribute()] bool FlowidSpecified, int ProcessType, [System.Xml.Serialization.XmlIgnoreAttribute()] bool ProcessTypeSpecified) {
            object[] results = this.Invoke("DFWaitToCheck_Main", new object[] {
                        userName,
                        FlowStatus,
                        SDate,
                        EDate,
                        Flowid,
                        FlowidSpecified,
                        ProcessType,
                        ProcessTypeSpecified});
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void DFWaitToCheck_MainAsync(string userName, string FlowStatus, string SDate, string EDate, int Flowid, bool FlowidSpecified, int ProcessType, bool ProcessTypeSpecified) {
            this.DFWaitToCheck_MainAsync(userName, FlowStatus, SDate, EDate, Flowid, FlowidSpecified, ProcessType, ProcessTypeSpecified, null);
        }
        
        /// <remarks/>
        public void DFWaitToCheck_MainAsync(string userName, string FlowStatus, string SDate, string EDate, int Flowid, bool FlowidSpecified, int ProcessType, bool ProcessTypeSpecified, object userState) {
            if ((this.DFWaitToCheck_MainOperationCompleted == null)) {
                this.DFWaitToCheck_MainOperationCompleted = new System.Threading.SendOrPostCallback(this.OnDFWaitToCheck_MainOperationCompleted);
            }
            this.InvokeAsync("DFWaitToCheck_Main", new object[] {
                        userName,
                        FlowStatus,
                        SDate,
                        EDate,
                        Flowid,
                        FlowidSpecified,
                        ProcessType,
                        ProcessTypeSpecified}, this.DFWaitToCheck_MainOperationCompleted, userState);
        }
        
        private void OnDFWaitToCheck_MainOperationCompleted(object arg) {
            if ((this.DFWaitToCheck_MainCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.DFWaitToCheck_MainCompleted(this, new DFWaitToCheck_MainCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    public delegate void IsTheAccountExistCompletedEventHandler(object sender, IsTheAccountExistCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class IsTheAccountExistCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal IsTheAccountExistCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    public delegate void DFWaitYourCheckCompletedEventHandler(object sender, DFWaitYourCheckCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class DFWaitYourCheckCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal DFWaitYourCheckCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    public delegate void DFWaitToCheck_MainCompletedEventHandler(object sender, DFWaitToCheck_MainCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class DFWaitToCheck_MainCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal DFWaitToCheck_MainCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591