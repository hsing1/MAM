﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Text;
using System.Xml;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSPGMCreatePlayList 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]

    public class ArrPromoList
    {
        public string COMBINENO { get; set; }  //序號
        public string FCTIME_TYPE { get; set; }  //時間類型 O:定時 A:順時
        public string FSPROG_ID { get; set; }   //節目編碼,如果為Promo則為所屬節目編碼
        public string FNSEQNO { get; set; } //節目播映序號
        public string FSNAME { get; set; }  //節目或Promo名稱
        public string FSCHANNEL_ID { get; set; } //頻道編碼
        public string FSCHANNEL_NAME { get; set; }  //頻道名稱
        public string FSPLAY_TIME { get; set; } //播出時間
        public string FSDURATION { get; set; }  //長度
        public string FNEPISODE { get; set; }   //節目集數
        public string FNBREAK_NO { get; set; }  //段數
        public string FNSUB_NO { get; set; }    //支數
        public string FSFILE_NO { get; set; }   //節目或Promo檔名
        public string FSVIDEO_ID { get; set; }  //節目段落和宣傳帶主控檔案編號
        public string FSPROMO_ID { get; set; }  //宣傳帶編碼,節目的話為空
        public string FDDATE { get; set; }  //日期
        public string FNLIVE { get; set; }  //是否為Live節目,0為Live,1為一般
        public string FCTYPE { get; set; }  //節目或Promo,G為節目,P為宣傳帶
        public string FSSTATUS { get; set; }    //檔案狀態,呼叫維智的service取得檔案派送到主控檔案的狀態(其實是NASB)
        public string FSSIGNAL { get; set; }    //訊號來源
        public string FSMEMO { get; set; }  //包含second event
        public string FSMEMO1 { get; set; }  //備註
        public string FSPROG_NAME { get; set; }  //節目名稱,宣傳帶則要要加入所屬節目名稱
    }



    public class WSPGMCreatePlayList : System.Web.Services.WebService
    {
        List<ArrPromoList> DatePlayList = new List<ArrPromoList>();

        public static string ReplaceXML(string XMLString)
        {
            XMLString = XMLString.Replace("&", "&amp;");
            XMLString = XMLString.Replace("'", "&apos;");
            XMLString = XMLString.Replace(@"""", "&quot;");
            XMLString = XMLString.Replace(">", "&gt;");
            XMLString = XMLString.Replace("<", "&lt;");
            return XMLString;
        }
        //取得節目段落資料,如有段落資料則取段落資料,如果沒有則產生預設段落
        public DataTable getProgSegment(string FSProgID, string FNEpisode, string FNSEQNO, string FSChannel_ID, string FNDUR, string FNDEF_MIN, string FNDEF_SEC, string FNDEF_SEG)
        {
            //DataTable dtProgSeg = new DataTable();
            //每一個節目,先查詢節目段落看是否有資料
            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection1 = new SqlConnection(sqlConnStr);
            if (connection1.State == System.Data.ConnectionState.Closed)
            {
                connection1.Open();
            }
            StringBuilder sb = new StringBuilder();
            SqlDataAdapter daProgSeg = new SqlDataAdapter("SP_Q_TBPGM_PROG_SEG", connection1);
            DataTable dtProgSeg = new DataTable();
            DataTable dtProgSegDefault = new DataTable();
            SqlParameter paraPROGID = new SqlParameter("@FSPROG_ID", FSProgID);
            daProgSeg.SelectCommand.Parameters.Add(paraPROGID);
            SqlParameter paraEPISODE = new SqlParameter("@FNEPISODE", FNEpisode);
            daProgSeg.SelectCommand.Parameters.Add(paraEPISODE);
            SqlParameter paraChannelID = new SqlParameter("@FSCHANNEL_ID", FSChannel_ID);
            daProgSeg.SelectCommand.Parameters.Add(paraChannelID);
            daProgSeg.SelectCommand.CommandType = CommandType.StoredProcedure;
            daProgSeg.Fill(dtProgSeg);
            if (dtProgSeg.Rows.Count > 0)
            {
                connection1.Close();


                return dtProgSeg;
            }
            else //如果查無資料則採用預設段落 預設段落先給一段,如果他們之後有規則的話
            {

                //StringBuilder sbBankDetail = new StringBuilder();
                //SqlDataAdapter daBankDetail = new SqlDataAdapter("SP_Q_TBPGM_BANK_DETAIL", connection1);
                //DataTable dtBankDetail = new DataTable();
                //SqlParameter paraBankPROGID = new SqlParameter("@FSPROG_ID", FSProgID);
                //daBankDetail.SelectCommand.Parameters.Add(paraBankPROGID);
                //SqlParameter paraBankFNSEQNO = new SqlParameter("@FNSEQNO", FNSEQNO);
                //daBankDetail.SelectCommand.Parameters.Add(paraBankFNSEQNO);
                //SqlParameter paraBankProgName = new SqlParameter("@FSPROG_NAME", "");
                //daBankDetail.SelectCommand.Parameters.Add(paraBankProgName);
                //daBankDetail.SelectCommand.CommandType = CommandType.StoredProcedure;
                //daBankDetail.Fill(dtBankDetail);


                //string BDSeg;
                //string BDDur;
                //if (dtBankDetail.Rows.Count > 0)
                //{
                //    BDSeg = dtBankDetail.Rows[0]["FNSEG"].ToString();
                //    BDDur = dtBankDetail.Rows[0]["FNDUR"].ToString();
                //}
                //else
                //{
                //    BDSeg = "1";
                //    BDDur = ((int)Math.Floor(int.Parse(FNDUR) * 0.8)).ToString();
                //    //BDDur = FNDUR;
                //}

                string BDSeg;
                string BDDur;
                BDSeg = FNDEF_SEG;
                
                if (BDSeg == "0")
                {
                    BDSeg = "1";
                    //BDDur = FNDUR;
                }

                BDDur = FNDUR;
                if (FNDEF_MIN == "0" && FNDEF_SEC == "0")
                {
                    BDDur = ((int)Math.Floor(int.Parse(FNDUR) * 0.8)).ToString();
                }

                dtProgSegDefault.Columns.Add("FSID", typeof(string));
                dtProgSegDefault.Columns.Add("FNEPISODE", typeof(string));
                dtProgSegDefault.Columns.Add("FSBEG_TIMECODE", typeof(string));
                dtProgSegDefault.Columns.Add("FSEND_TIMECODE", typeof(string));
                dtProgSegDefault.Columns.Add("FCLOW_RES", typeof(string));
                dtProgSegDefault.Columns.Add("FNSEG_ID", typeof(string));
                dtProgSegDefault.Columns.Add("FNDUR_ID", typeof(string));
                dtProgSegDefault.Columns.Add("FSVIDEO_ID", typeof(string));
                dtProgSegDefault.Columns.Add("FSFILE_NO", typeof(string));


                StringBuilder sbGetVideoID = new StringBuilder();
                SqlDataAdapter daGetVideoID = new SqlDataAdapter("SP_Q_TBLOG_VIDEOID_MAP_BY_CHANNEL_ID", connection1);
                DataTable dtGetVideoID = new DataTable();
                SqlParameter paraGetVideoIDPROGID = new SqlParameter("@FSID", FSProgID);
                daGetVideoID.SelectCommand.Parameters.Add(paraGetVideoIDPROGID);
                SqlParameter paraGetVideoIDFNSEQNO = new SqlParameter("@FNEPISODE", FNEpisode);
                daGetVideoID.SelectCommand.Parameters.Add(paraGetVideoIDFNSEQNO);
                SqlParameter paraGetVideoIDProgName = new SqlParameter("@FSCHANNEL_ID", FSChannel_ID);
                daGetVideoID.SelectCommand.Parameters.Add(paraGetVideoIDProgName);
                daGetVideoID.SelectCommand.CommandType = CommandType.StoredProcedure;
                daGetVideoID.Fill(dtGetVideoID);
                //int NewSeg = 1;
                string GetVIdeoID = "";
                if (dtGetVideoID.Rows.Count > 0)
                     GetVIdeoID = dtGetVideoID.Rows[0][0].ToString();
                else
                    GetVIdeoID = "";
                
                //if (int.Parse(BDSeg) != 1)
                //{
                    int n = 1;
                    //int allDur =(int)Math.Floor(int.Parse(FNDUR) * 0.8);
                    int allDur = int.Parse(FNDEF_MIN); //剩餘所有節目長度
                    int nowDur;  //目前剩餘節目長度
                    string nowDurString;
                    //BDSeg 表示節目段數
                    //nowDur = (int)(Math.Floor((double)allDur / (double)int.Parse(BDSeg)));  //此為採用平均分配

                    //規則為 當剩餘長度大於剩餘段落*10則除了最後一段外都為10分鐘,不然就採平均
                    if (int.Parse(FNDEF_MIN) > int.Parse(BDSeg) * 10)
                        nowDur = 10;
                    else
                        nowDur = (int)(Math.Floor((double)allDur / (double)int.Parse(BDSeg)));

                    while (n <= int.Parse(BDSeg))
                    {
                        //n表示目前作業為哪段

                        nowDurString = MinToTimecode(nowDur);
                        if (n != int.Parse(BDSeg))
                            dtProgSegDefault.Rows.Add(FSProgID, FNEpisode, "00:00:00:00", nowDurString, "", n.ToString(), FNDUR, GetVIdeoID, "");
                        else
                            dtProgSegDefault.Rows.Add(FSProgID, FNEpisode, "00:00:00:00", MinToTimecode(allDur).Substring(0, 6) + String.Format("{0:00}", int.Parse(FNDEF_SEC)) + MinToTimecode(allDur).Substring(8, 3), "", n.ToString(), FNDUR, GetVIdeoID, "");

                        allDur = allDur - nowDur;
                        //Console.WriteLine("Current value of n is {0}", n);
                        n++;
                    }
                //}
                //else
                //{
                //    dtProgSegDefault.Rows.Add(FSProgID, FNEpisode, "00:00:00:00", MinToTimecode(int.Parse(BDDur)), "", 1, FNDUR, GetVIdeoID, "");
                //}
                    connection1.Close();
                return dtProgSegDefault;
            }
        }

        //取得節目段落資料,如有段落資料則取段落資料,如果沒有則產生預設段落
        public DataTable getProgSegment_MIX(string FSProgID, string FNEpisode, string FNSEQNO, string FSChannel_ID, string FNDUR, string FNDEF_MIN, string FNDEF_SEC, string FNDEF_SEG)
        {
            //DataTable dtProgSeg = new DataTable();
            //每一個節目,先查詢節目段落看是否有資料
            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection1 = new SqlConnection(sqlConnStr);
            if (connection1.State == System.Data.ConnectionState.Closed)
            {
                connection1.Open();
            }
            StringBuilder sb = new StringBuilder();
            SqlDataAdapter daProgSeg = new SqlDataAdapter("SP_Q_TBPGM_PROG_SEG_MIX", connection1);
            DataTable dtProgSeg = new DataTable();
            DataTable dtProgSegDefault = new DataTable();
            SqlParameter paraPROGID = new SqlParameter("@FSPROG_ID", FSProgID);
            daProgSeg.SelectCommand.Parameters.Add(paraPROGID);
            SqlParameter paraEPISODE = new SqlParameter("@FNEPISODE", FNEpisode);
            daProgSeg.SelectCommand.Parameters.Add(paraEPISODE);
            SqlParameter paraChannelID = new SqlParameter("@FSCHANNEL_ID", FSChannel_ID);
            daProgSeg.SelectCommand.Parameters.Add(paraChannelID);
            daProgSeg.SelectCommand.CommandType = CommandType.StoredProcedure;
            daProgSeg.Fill(dtProgSeg);
            if (dtProgSeg.Rows.Count > 0)
            {
                connection1.Close();


                return dtProgSeg;
            }
            else //如果查無資料則採用預設段落 預設段落先給一段,如果他們之後有規則的話
            {

                //StringBuilder sbBankDetail = new StringBuilder();
                //SqlDataAdapter daBankDetail = new SqlDataAdapter("SP_Q_TBPGM_BANK_DETAIL", connection1);
                //DataTable dtBankDetail = new DataTable();
                //SqlParameter paraBankPROGID = new SqlParameter("@FSPROG_ID", FSProgID);
                //daBankDetail.SelectCommand.Parameters.Add(paraBankPROGID);
                //SqlParameter paraBankFNSEQNO = new SqlParameter("@FNSEQNO", FNSEQNO);
                //daBankDetail.SelectCommand.Parameters.Add(paraBankFNSEQNO);
                //SqlParameter paraBankProgName = new SqlParameter("@FSPROG_NAME", "");
                //daBankDetail.SelectCommand.Parameters.Add(paraBankProgName);
                //daBankDetail.SelectCommand.CommandType = CommandType.StoredProcedure;
                //daBankDetail.Fill(dtBankDetail);




                //string BDSeg;
                //string BDDur;
                //if (dtBankDetail.Rows.Count > 0)
                //{
                //    BDSeg = dtBankDetail.Rows[0]["FNSEG"].ToString();
                //    BDDur = dtBankDetail.Rows[0]["FNDUR"].ToString();
                //}
                //else
                //{
                //    BDSeg = "1";
                //    BDDur = ((int)Math.Floor(int.Parse(FNDUR) * 0.8)).ToString();
                //    //BDDur = FNDUR;
                //}

                string BDSeg;
                string BDDur;
                BDSeg = FNDEF_SEG;

                
                if (BDSeg == "0")
                {
                    BDSeg = "1";
                    //BDDur = FNDUR;
                }
                if (FNDEF_MIN == "0" && FNDEF_SEC == "0")
                {
                    BDDur = ((int)Math.Floor(int.Parse(FNDUR) * 0.8)).ToString();
                }

                dtProgSegDefault.Columns.Add("FSID", typeof(string));
                dtProgSegDefault.Columns.Add("FNEPISODE", typeof(string));
                dtProgSegDefault.Columns.Add("FSBEG_TIMECODE", typeof(string));
                dtProgSegDefault.Columns.Add("FSEND_TIMECODE", typeof(string));
                dtProgSegDefault.Columns.Add("FCLOW_RES", typeof(string));
                dtProgSegDefault.Columns.Add("FNSEG_ID", typeof(string));
                dtProgSegDefault.Columns.Add("FNDUR_ID", typeof(string));
                dtProgSegDefault.Columns.Add("FSVIDEO_ID", typeof(string));
                dtProgSegDefault.Columns.Add("FSFILE_NO", typeof(string));


                StringBuilder sbGetVideoID = new StringBuilder();
                SqlDataAdapter daGetVideoID = new SqlDataAdapter("SP_Q_TBLOG_VIDEOID_MAP_BY_CHANNEL_ID", connection1);
                DataTable dtGetVideoID = new DataTable();
                SqlParameter paraGetVideoIDPROGID = new SqlParameter("@FSID", FSProgID);
                daGetVideoID.SelectCommand.Parameters.Add(paraGetVideoIDPROGID);
                SqlParameter paraGetVideoIDFNSEQNO = new SqlParameter("@FNEPISODE", FNEpisode);
                daGetVideoID.SelectCommand.Parameters.Add(paraGetVideoIDFNSEQNO);
                SqlParameter paraGetVideoIDProgName = new SqlParameter("@FSCHANNEL_ID", FSChannel_ID);
                daGetVideoID.SelectCommand.Parameters.Add(paraGetVideoIDProgName);
                daGetVideoID.SelectCommand.CommandType = CommandType.StoredProcedure;
                daGetVideoID.Fill(dtGetVideoID);
                //int NewSeg = 1;
                string GetVIdeoID = "";
                if (dtGetVideoID.Rows.Count > 0)
                    GetVIdeoID = dtGetVideoID.Rows[0][0].ToString();
                else
                    GetVIdeoID = "";

                //if (int.Parse(BDSeg) != 1)
                //{
                int n = 1;
                //int allDur =(int)Math.Floor(int.Parse(FNDUR) * 0.8);
                int allDur = int.Parse(FNDEF_MIN); //剩餘所有節目長度
                int nowDur;  //目前剩餘節目長度
                string nowDurString;
                //BDSeg 表示節目段數
                //nowDur = (int)(Math.Floor((double)allDur / (double)int.Parse(BDSeg)));  //此為採用平均分配

                //規則為 當剩餘長度大於剩餘段落*10則除了最後一段外都為10分鐘,不然就採平均
                if (int.Parse(FNDEF_MIN) > int.Parse(BDSeg) * 10)
                    nowDur = 10;
                else
                    nowDur = (int)(Math.Floor((double)allDur / (double)int.Parse(BDSeg)));

                while (n <= int.Parse(BDSeg))
                {
                    //n表示目前作業為哪段

                    nowDurString = MinToTimecode(nowDur);
                    if (n != int.Parse(BDSeg))
                        dtProgSegDefault.Rows.Add(FSProgID, FNEpisode, "00:00:00:00", nowDurString, "", n.ToString(), FNDUR, GetVIdeoID, "");
                    else
                        dtProgSegDefault.Rows.Add(FSProgID, FNEpisode, "00:00:00:00", MinToTimecode(allDur).Substring(0, 6) + String.Format("{0:00}", int.Parse(FNDEF_SEC)) + MinToTimecode(allDur).Substring(8, 3), "", n.ToString(), FNDUR, GetVIdeoID, "");

                    allDur = allDur - nowDur;
                    //Console.WriteLine("Current value of n is {0}", n);
                    n++;
                }
                //}
                //else
                //{
                //    dtProgSegDefault.Rows.Add(FSProgID, FNEpisode, "00:00:00:00", MinToTimecode(int.Parse(BDDur)), "", 1, FNDUR, GetVIdeoID, "");
                //}
                connection1.Close();
                return dtProgSegDefault;
            }
        }

        //將分鐘轉換為Timecode
        public string MinToTimecode(int InMin)
        {
            //string MinString;
            int HH = (int)(Math.Floor((double)InMin / (double)60));
            int MM = InMin - 60 * HH;
            return String.Format("{0:00}", HH) + ":" + String.Format("{0:00}", MM) + ":00;00";
        }

        //計算長度0600-0800==>120分
        public int CalProgLength(string BTime, string ETime)
        {
            //string MinString;
            return int.Parse(ETime.Substring(0, 2)) * 60 + int.Parse(ETime.Substring(2, 2)) - int.Parse(BTime.Substring(0, 2)) * 60 - int.Parse(BTime.Substring(2, 2));
        }

        public DataTable AAA = new DataTable();
        public DataTable A1A = new DataTable();
        public DataTable A2A = new DataTable();
        public DataTable A3A = new DataTable();
        public DataTable AAC = new DataTable();
        public DataTable A1C = new DataTable();
        public DataTable A2C = new DataTable();
        public DataTable A3C = new DataTable();

        public DataTable BAA = new DataTable();
        public DataTable B1A = new DataTable();
        public DataTable B2A = new DataTable();
        public DataTable B3A = new DataTable();
        public DataTable BAC = new DataTable();
        public DataTable B1C = new DataTable();
        public DataTable B2C = new DataTable();
        public DataTable B3C = new DataTable();

        public DataTable CAA = new DataTable();
        public DataTable C1A = new DataTable();
        public DataTable C2A = new DataTable();
        public DataTable C3A = new DataTable();
        public DataTable CAC = new DataTable();
        public DataTable C1C = new DataTable();
        public DataTable C2C = new DataTable();
        public DataTable C3C = new DataTable();

        //循環類型要用的,目前循環到哪一支
        public int _IAAC = 0;
        public int _IA1C = 0;
        public int _IA2C = 0;
        public int _IA3C = 0;

        public int _IBAC = 0;
        public int _IB1C = 0;
        public int _IB2C = 0;
        public int _IB3C = 0;

        public int _ICAC = 0;
        public int _IC1C = 0;
        public int _IC2C = 0;
        public int _IC3C = 0;

        public void DataTableAddColumn(DataTable InDataTable)
        {
            InDataTable.Columns.Add("FSPROMO_ID", typeof(string));
            InDataTable.Columns.Add("FSCHANNEL_ID", typeof(string));
            InDataTable.Columns.Add("FNNO", typeof(string));
            InDataTable.Columns.Add("FCLINK_TYPE", typeof(string));
            InDataTable.Columns.Add("FCTIME_TYPE", typeof(string));
            InDataTable.Columns.Add("FCINSERT_TYPE", typeof(string));
            InDataTable.Columns.Add("FSPROMO_NAME", typeof(string));
            InDataTable.Columns.Add("FSCHANNEL_NAME", typeof(string));
            InDataTable.Columns.Add("FSDURATION", typeof(string));
        }

        public void Set_DataTable_Channel_Promo_Link(DataTable Alllink)
        {
            DataTableAddColumn(AAA);
            DataTableAddColumn(A1A);
            DataTableAddColumn(A2A);
            DataTableAddColumn(A3A);
            DataTableAddColumn(AAC);
            DataTableAddColumn(A1C);
            DataTableAddColumn(A2C);
            DataTableAddColumn(A3C);
            DataTableAddColumn(BAA);
            DataTableAddColumn(B1A);
            DataTableAddColumn(B2A);
            DataTableAddColumn(B3A);
            DataTableAddColumn(BAC);
            DataTableAddColumn(B1C);
            DataTableAddColumn(B2C);
            DataTableAddColumn(B3C);
            DataTableAddColumn(CAA);
            DataTableAddColumn(C1A);
            DataTableAddColumn(C2A);
            DataTableAddColumn(C3A);
            DataTableAddColumn(CAC);
            DataTableAddColumn(C1C);
            DataTableAddColumn(C2C);
            DataTableAddColumn(C3C);

            //DataRow TDataLow = new DataRow();

            //DataTable DT = new DataTable();

            //DT.Columns.Add("FSPROMO_ID", typeof(string));
            //DT.Columns.Add("FSCHANNEL_ID", typeof(string));
            //DT.Columns.Add("FNNO", typeof(string));
            //DT.Columns.Add("FCLINK_TYPE", typeof(string));
            //DT.Columns.Add("FCTIME_TYPE", typeof(string));
            //DT.Columns.Add("FCINSERT_TYPE", typeof(string));
            //DT.Columns.Add("FSPROMO_NAME", typeof(string));
            //DT.Columns.Add("FSCHANNEL_NAME", typeof(string));
            //DT.Columns.Add("FSDURATION", typeof(string));

            //AAA = DT;
            //A1A = DT;
            //A2A = DT;
            //A3A = DT;
            //AAC = DT;
            //A1C = DT;
            //A2C = DT;
            //A3C = DT;
            //BAA = DT;
            //B1A = DT;
            //B2A = DT;
            //B3A = DT;
            //BAC = DT;
            //B1C = DT;
            //B2C = DT;
            //B3C = DT;
            //CAA = DT;
            //C1A = DT;
            //C2A = DT;
            //C3A = DT;
            //CAC = DT;
            //C1C = DT;
            //C2C = DT;
            //C3C = DT;
            for (int i = 0; i <= Alllink.Rows.Count - 1; i++)
            {
                if (Alllink.Rows[i]["FCLINK_TYPE"].ToString() == "A" && Alllink.Rows[i]["FCTIME_TYPE"].ToString() == "A" && Alllink.Rows[i]["FCINSERT_TYPE"].ToString() == "A")
                    AAA.Rows.Add(Alllink.Rows[i]["FSPROMO_ID"], Alllink.Rows[i]["FSCHANNEL_ID"], Alllink.Rows[i]["FNNO"], Alllink.Rows[i]["FCLINK_TYPE"], Alllink.Rows[i]["FCTIME_TYPE"], Alllink.Rows[i]["FCINSERT_TYPE"], Alllink.Rows[i]["FSPROMO_NAME"], Alllink.Rows[i]["FSCHANNEL_NAME"], Alllink.Rows[i]["FSDURATION"]);
                if (Alllink.Rows[i]["FCLINK_TYPE"].ToString() == "A" && Alllink.Rows[i]["FCTIME_TYPE"].ToString() == "1" && Alllink.Rows[i]["FCINSERT_TYPE"].ToString() == "A")
                    A1A.Rows.Add(Alllink.Rows[i]["FSPROMO_ID"], Alllink.Rows[i]["FSCHANNEL_ID"], Alllink.Rows[i]["FNNO"], Alllink.Rows[i]["FCLINK_TYPE"], Alllink.Rows[i]["FCTIME_TYPE"], Alllink.Rows[i]["FCINSERT_TYPE"], Alllink.Rows[i]["FSPROMO_NAME"], Alllink.Rows[i]["FSCHANNEL_NAME"], Alllink.Rows[i]["FSDURATION"]);
                if (Alllink.Rows[i]["FCLINK_TYPE"].ToString() == "A" && Alllink.Rows[i]["FCTIME_TYPE"].ToString() == "2" && Alllink.Rows[i]["FCINSERT_TYPE"].ToString() == "A")
                    A2A.Rows.Add(Alllink.Rows[i]["FSPROMO_ID"], Alllink.Rows[i]["FSCHANNEL_ID"], Alllink.Rows[i]["FNNO"], Alllink.Rows[i]["FCLINK_TYPE"], Alllink.Rows[i]["FCTIME_TYPE"], Alllink.Rows[i]["FCINSERT_TYPE"], Alllink.Rows[i]["FSPROMO_NAME"], Alllink.Rows[i]["FSCHANNEL_NAME"], Alllink.Rows[i]["FSDURATION"]);
                if (Alllink.Rows[i]["FCLINK_TYPE"].ToString() == "A" && Alllink.Rows[i]["FCTIME_TYPE"].ToString() == "3" && Alllink.Rows[i]["FCINSERT_TYPE"].ToString() == "A")
                    A3A.Rows.Add(Alllink.Rows[i]["FSPROMO_ID"], Alllink.Rows[i]["FSCHANNEL_ID"], Alllink.Rows[i]["FNNO"], Alllink.Rows[i]["FCLINK_TYPE"], Alllink.Rows[i]["FCTIME_TYPE"], Alllink.Rows[i]["FCINSERT_TYPE"], Alllink.Rows[i]["FSPROMO_NAME"], Alllink.Rows[i]["FSCHANNEL_NAME"], Alllink.Rows[i]["FSDURATION"]);
                if (Alllink.Rows[i]["FCLINK_TYPE"].ToString() == "A" && Alllink.Rows[i]["FCTIME_TYPE"].ToString() == "A" && Alllink.Rows[i]["FCINSERT_TYPE"].ToString() == "C")
                    AAC.Rows.Add(Alllink.Rows[i]["FSPROMO_ID"], Alllink.Rows[i]["FSCHANNEL_ID"], Alllink.Rows[i]["FNNO"], Alllink.Rows[i]["FCLINK_TYPE"], Alllink.Rows[i]["FCTIME_TYPE"], Alllink.Rows[i]["FCINSERT_TYPE"], Alllink.Rows[i]["FSPROMO_NAME"], Alllink.Rows[i]["FSCHANNEL_NAME"], Alllink.Rows[i]["FSDURATION"]);
                if (Alllink.Rows[i]["FCLINK_TYPE"].ToString() == "A" && Alllink.Rows[i]["FCTIME_TYPE"].ToString() == "1" && Alllink.Rows[i]["FCINSERT_TYPE"].ToString() == "C")
                    A1C.Rows.Add(Alllink.Rows[i]["FSPROMO_ID"], Alllink.Rows[i]["FSCHANNEL_ID"], Alllink.Rows[i]["FNNO"], Alllink.Rows[i]["FCLINK_TYPE"], Alllink.Rows[i]["FCTIME_TYPE"], Alllink.Rows[i]["FCINSERT_TYPE"], Alllink.Rows[i]["FSPROMO_NAME"], Alllink.Rows[i]["FSCHANNEL_NAME"], Alllink.Rows[i]["FSDURATION"]);
                if (Alllink.Rows[i]["FCLINK_TYPE"].ToString() == "A" && Alllink.Rows[i]["FCTIME_TYPE"].ToString() == "2" && Alllink.Rows[i]["FCINSERT_TYPE"].ToString() == "C")
                    A2C.Rows.Add(Alllink.Rows[i]["FSPROMO_ID"], Alllink.Rows[i]["FSCHANNEL_ID"], Alllink.Rows[i]["FNNO"], Alllink.Rows[i]["FCLINK_TYPE"], Alllink.Rows[i]["FCTIME_TYPE"], Alllink.Rows[i]["FCINSERT_TYPE"], Alllink.Rows[i]["FSPROMO_NAME"], Alllink.Rows[i]["FSCHANNEL_NAME"], Alllink.Rows[i]["FSDURATION"]);
                if (Alllink.Rows[i]["FCLINK_TYPE"].ToString() == "A" && Alllink.Rows[i]["FCTIME_TYPE"].ToString() == "3" && Alllink.Rows[i]["FCINSERT_TYPE"].ToString() == "C")
                    A3C.Rows.Add(Alllink.Rows[i]["FSPROMO_ID"], Alllink.Rows[i]["FSCHANNEL_ID"], Alllink.Rows[i]["FNNO"], Alllink.Rows[i]["FCLINK_TYPE"], Alllink.Rows[i]["FCTIME_TYPE"], Alllink.Rows[i]["FCINSERT_TYPE"], Alllink.Rows[i]["FSPROMO_NAME"], Alllink.Rows[i]["FSCHANNEL_NAME"], Alllink.Rows[i]["FSDURATION"]);

                if (Alllink.Rows[i]["FCLINK_TYPE"].ToString() == "B" && Alllink.Rows[i]["FCTIME_TYPE"].ToString() == "A" && Alllink.Rows[i]["FCINSERT_TYPE"].ToString() == "A")
                    BAA.Rows.Add(Alllink.Rows[i]["FSPROMO_ID"], Alllink.Rows[i]["FSCHANNEL_ID"], Alllink.Rows[i]["FNNO"], Alllink.Rows[i]["FCLINK_TYPE"], Alllink.Rows[i]["FCTIME_TYPE"], Alllink.Rows[i]["FCINSERT_TYPE"], Alllink.Rows[i]["FSPROMO_NAME"], Alllink.Rows[i]["FSCHANNEL_NAME"], Alllink.Rows[i]["FSDURATION"]);
                if (Alllink.Rows[i]["FCLINK_TYPE"].ToString() == "B" && Alllink.Rows[i]["FCTIME_TYPE"].ToString() == "1" && Alllink.Rows[i]["FCINSERT_TYPE"].ToString() == "A")
                    B1A.Rows.Add(Alllink.Rows[i]["FSPROMO_ID"], Alllink.Rows[i]["FSCHANNEL_ID"], Alllink.Rows[i]["FNNO"], Alllink.Rows[i]["FCLINK_TYPE"], Alllink.Rows[i]["FCTIME_TYPE"], Alllink.Rows[i]["FCINSERT_TYPE"], Alllink.Rows[i]["FSPROMO_NAME"], Alllink.Rows[i]["FSCHANNEL_NAME"], Alllink.Rows[i]["FSDURATION"]);
                if (Alllink.Rows[i]["FCLINK_TYPE"].ToString() == "B" && Alllink.Rows[i]["FCTIME_TYPE"].ToString() == "2" && Alllink.Rows[i]["FCINSERT_TYPE"].ToString() == "A")
                    B2A.Rows.Add(Alllink.Rows[i]["FSPROMO_ID"], Alllink.Rows[i]["FSCHANNEL_ID"], Alllink.Rows[i]["FNNO"], Alllink.Rows[i]["FCLINK_TYPE"], Alllink.Rows[i]["FCTIME_TYPE"], Alllink.Rows[i]["FCINSERT_TYPE"], Alllink.Rows[i]["FSPROMO_NAME"], Alllink.Rows[i]["FSCHANNEL_NAME"], Alllink.Rows[i]["FSDURATION"]);
                if (Alllink.Rows[i]["FCLINK_TYPE"].ToString() == "B" && Alllink.Rows[i]["FCTIME_TYPE"].ToString() == "3" && Alllink.Rows[i]["FCINSERT_TYPE"].ToString() == "A")
                    B3A.Rows.Add(Alllink.Rows[i]["FSPROMO_ID"], Alllink.Rows[i]["FSCHANNEL_ID"], Alllink.Rows[i]["FNNO"], Alllink.Rows[i]["FCLINK_TYPE"], Alllink.Rows[i]["FCTIME_TYPE"], Alllink.Rows[i]["FCINSERT_TYPE"], Alllink.Rows[i]["FSPROMO_NAME"], Alllink.Rows[i]["FSCHANNEL_NAME"], Alllink.Rows[i]["FSDURATION"]);
                if (Alllink.Rows[i]["FCLINK_TYPE"].ToString() == "B" && Alllink.Rows[i]["FCTIME_TYPE"].ToString() == "A" && Alllink.Rows[i]["FCINSERT_TYPE"].ToString() == "C")
                    BAC.Rows.Add(Alllink.Rows[i]["FSPROMO_ID"], Alllink.Rows[i]["FSCHANNEL_ID"], Alllink.Rows[i]["FNNO"], Alllink.Rows[i]["FCLINK_TYPE"], Alllink.Rows[i]["FCTIME_TYPE"], Alllink.Rows[i]["FCINSERT_TYPE"], Alllink.Rows[i]["FSPROMO_NAME"], Alllink.Rows[i]["FSCHANNEL_NAME"], Alllink.Rows[i]["FSDURATION"]);
                if (Alllink.Rows[i]["FCLINK_TYPE"].ToString() == "B" && Alllink.Rows[i]["FCTIME_TYPE"].ToString() == "1" && Alllink.Rows[i]["FCINSERT_TYPE"].ToString() == "C")
                    B1C.Rows.Add(Alllink.Rows[i]["FSPROMO_ID"], Alllink.Rows[i]["FSCHANNEL_ID"], Alllink.Rows[i]["FNNO"], Alllink.Rows[i]["FCLINK_TYPE"], Alllink.Rows[i]["FCTIME_TYPE"], Alllink.Rows[i]["FCINSERT_TYPE"], Alllink.Rows[i]["FSPROMO_NAME"], Alllink.Rows[i]["FSCHANNEL_NAME"], Alllink.Rows[i]["FSDURATION"]);
                if (Alllink.Rows[i]["FCLINK_TYPE"].ToString() == "B" && Alllink.Rows[i]["FCTIME_TYPE"].ToString() == "2" && Alllink.Rows[i]["FCINSERT_TYPE"].ToString() == "C")
                    B2C.Rows.Add(Alllink.Rows[i]["FSPROMO_ID"], Alllink.Rows[i]["FSCHANNEL_ID"], Alllink.Rows[i]["FNNO"], Alllink.Rows[i]["FCLINK_TYPE"], Alllink.Rows[i]["FCTIME_TYPE"], Alllink.Rows[i]["FCINSERT_TYPE"], Alllink.Rows[i]["FSPROMO_NAME"], Alllink.Rows[i]["FSCHANNEL_NAME"], Alllink.Rows[i]["FSDURATION"]);
                if (Alllink.Rows[i]["FCLINK_TYPE"].ToString() == "B" && Alllink.Rows[i]["FCTIME_TYPE"].ToString() == "3" && Alllink.Rows[i]["FCINSERT_TYPE"].ToString() == "C")
                    B3C.Rows.Add(Alllink.Rows[i]["FSPROMO_ID"], Alllink.Rows[i]["FSCHANNEL_ID"], Alllink.Rows[i]["FNNO"], Alllink.Rows[i]["FCLINK_TYPE"], Alllink.Rows[i]["FCTIME_TYPE"], Alllink.Rows[i]["FCINSERT_TYPE"], Alllink.Rows[i]["FSPROMO_NAME"], Alllink.Rows[i]["FSCHANNEL_NAME"], Alllink.Rows[i]["FSDURATION"]);

                if (Alllink.Rows[i]["FCLINK_TYPE"].ToString() == "C" && Alllink.Rows[i]["FCTIME_TYPE"].ToString() == "A" && Alllink.Rows[i]["FCINSERT_TYPE"].ToString() == "A")
                    CAA.Rows.Add(Alllink.Rows[i]["FSPROMO_ID"], Alllink.Rows[i]["FSCHANNEL_ID"], Alllink.Rows[i]["FNNO"], Alllink.Rows[i]["FCLINK_TYPE"], Alllink.Rows[i]["FCTIME_TYPE"], Alllink.Rows[i]["FCINSERT_TYPE"], Alllink.Rows[i]["FSPROMO_NAME"], Alllink.Rows[i]["FSCHANNEL_NAME"], Alllink.Rows[i]["FSDURATION"]);
                if (Alllink.Rows[i]["FCLINK_TYPE"].ToString() == "C" && Alllink.Rows[i]["FCTIME_TYPE"].ToString() == "1" && Alllink.Rows[i]["FCINSERT_TYPE"].ToString() == "A")
                    C1A.Rows.Add(Alllink.Rows[i]["FSPROMO_ID"], Alllink.Rows[i]["FSCHANNEL_ID"], Alllink.Rows[i]["FNNO"], Alllink.Rows[i]["FCLINK_TYPE"], Alllink.Rows[i]["FCTIME_TYPE"], Alllink.Rows[i]["FCINSERT_TYPE"], Alllink.Rows[i]["FSPROMO_NAME"], Alllink.Rows[i]["FSCHANNEL_NAME"], Alllink.Rows[i]["FSDURATION"]);
                if (Alllink.Rows[i]["FCLINK_TYPE"].ToString() == "C" && Alllink.Rows[i]["FCTIME_TYPE"].ToString() == "2" && Alllink.Rows[i]["FCINSERT_TYPE"].ToString() == "A")
                    C2A.Rows.Add(Alllink.Rows[i]["FSPROMO_ID"], Alllink.Rows[i]["FSCHANNEL_ID"], Alllink.Rows[i]["FNNO"], Alllink.Rows[i]["FCLINK_TYPE"], Alllink.Rows[i]["FCTIME_TYPE"], Alllink.Rows[i]["FCINSERT_TYPE"], Alllink.Rows[i]["FSPROMO_NAME"], Alllink.Rows[i]["FSCHANNEL_NAME"], Alllink.Rows[i]["FSDURATION"]);
                if (Alllink.Rows[i]["FCLINK_TYPE"].ToString() == "C" && Alllink.Rows[i]["FCTIME_TYPE"].ToString() == "3" && Alllink.Rows[i]["FCINSERT_TYPE"].ToString() == "A")
                    C3A.Rows.Add(Alllink.Rows[i]["FSPROMO_ID"], Alllink.Rows[i]["FSCHANNEL_ID"], Alllink.Rows[i]["FNNO"], Alllink.Rows[i]["FCLINK_TYPE"], Alllink.Rows[i]["FCTIME_TYPE"], Alllink.Rows[i]["FCINSERT_TYPE"], Alllink.Rows[i]["FSPROMO_NAME"], Alllink.Rows[i]["FSCHANNEL_NAME"], Alllink.Rows[i]["FSDURATION"]);
                if (Alllink.Rows[i]["FCLINK_TYPE"].ToString() == "C" && Alllink.Rows[i]["FCTIME_TYPE"].ToString() == "A" && Alllink.Rows[i]["FCINSERT_TYPE"].ToString() == "C")
                    CAC.Rows.Add(Alllink.Rows[i]["FSPROMO_ID"], Alllink.Rows[i]["FSCHANNEL_ID"], Alllink.Rows[i]["FNNO"], Alllink.Rows[i]["FCLINK_TYPE"], Alllink.Rows[i]["FCTIME_TYPE"], Alllink.Rows[i]["FCINSERT_TYPE"], Alllink.Rows[i]["FSPROMO_NAME"], Alllink.Rows[i]["FSCHANNEL_NAME"], Alllink.Rows[i]["FSDURATION"]);
                if (Alllink.Rows[i]["FCLINK_TYPE"].ToString() == "C" && Alllink.Rows[i]["FCTIME_TYPE"].ToString() == "1" && Alllink.Rows[i]["FCINSERT_TYPE"].ToString() == "C")
                    C1C.Rows.Add(Alllink.Rows[i]["FSPROMO_ID"], Alllink.Rows[i]["FSCHANNEL_ID"], Alllink.Rows[i]["FNNO"], Alllink.Rows[i]["FCLINK_TYPE"], Alllink.Rows[i]["FCTIME_TYPE"], Alllink.Rows[i]["FCINSERT_TYPE"], Alllink.Rows[i]["FSPROMO_NAME"], Alllink.Rows[i]["FSCHANNEL_NAME"], Alllink.Rows[i]["FSDURATION"]);
                if (Alllink.Rows[i]["FCLINK_TYPE"].ToString() == "C" && Alllink.Rows[i]["FCTIME_TYPE"].ToString() == "2" && Alllink.Rows[i]["FCINSERT_TYPE"].ToString() == "C")
                    C2C.Rows.Add(Alllink.Rows[i]["FSPROMO_ID"], Alllink.Rows[i]["FSCHANNEL_ID"], Alllink.Rows[i]["FNNO"], Alllink.Rows[i]["FCLINK_TYPE"], Alllink.Rows[i]["FCTIME_TYPE"], Alllink.Rows[i]["FCINSERT_TYPE"], Alllink.Rows[i]["FSPROMO_NAME"], Alllink.Rows[i]["FSCHANNEL_NAME"], Alllink.Rows[i]["FSDURATION"]);
                if (Alllink.Rows[i]["FCLINK_TYPE"].ToString() == "C" && Alllink.Rows[i]["FCTIME_TYPE"].ToString() == "3" && Alllink.Rows[i]["FCINSERT_TYPE"].ToString() == "C")
                    C3C.Rows.Add(Alllink.Rows[i]["FSPROMO_ID"], Alllink.Rows[i]["FSCHANNEL_ID"], Alllink.Rows[i]["FNNO"], Alllink.Rows[i]["FCLINK_TYPE"], Alllink.Rows[i]["FCTIME_TYPE"], Alllink.Rows[i]["FCINSERT_TYPE"], Alllink.Rows[i]["FSPROMO_NAME"], Alllink.Rows[i]["FSCHANNEL_NAME"], Alllink.Rows[i]["FSDURATION"]);

            }
        }

        //插入宣傳帶
        public void addPromoRows(DataTable Arrromo, DataTable TEMPDatatable, int i, int SUBNO, string FSProgID, string FNSEQNO, string FNBreakNO, string FSProg_Name)
        {
            ArrPromoList TempArrPromoList = new ArrPromoList();

            TempArrPromoList.COMBINENO = "";
            TempArrPromoList.FSPROG_ID = FSProgID;
            TempArrPromoList.FNSEQNO = FNSEQNO;
            TempArrPromoList.FSNAME = TEMPDatatable.Rows[i]["FSPROMO_NAME"].ToString();
            TempArrPromoList.FSCHANNEL_ID = "";
            TempArrPromoList.FSCHANNEL_NAME = "";
            TempArrPromoList.FSPLAY_TIME = "";
            TempArrPromoList.FSDURATION = TEMPDatatable.Rows[i]["FSDURATION"].ToString();
            TempArrPromoList.FNEPISODE = "";
            TempArrPromoList.FNBREAK_NO = FNBreakNO;
            TempArrPromoList.FNSUB_NO = SUBNO.ToString();
            TempArrPromoList.FSFILE_NO = "";//TEMPDatatable.Rows[i]["FSFILE_NO"].ToString();
            TempArrPromoList.FSVIDEO_ID = "";//TEMPDatatable.Rows[i]["FSVIDEO_ID"].ToString();
            TempArrPromoList.FSPROMO_ID = TEMPDatatable.Rows[i]["FSPROMO_ID"].ToString();
            TempArrPromoList.FDDATE = "";
            TempArrPromoList.FNLIVE = "0";
            TempArrPromoList.FCTYPE = "P";
            TempArrPromoList.FSSTATUS = "";
            TempArrPromoList.FSSIGNAL = "COMM";
            TempArrPromoList.FSMEMO = "";//TEMPDatatable.Rows[i]["FSMEMO"].ToString();
            TempArrPromoList.FSMEMO1 = "";
            TempArrPromoList.FSPROG_NAME = FSProg_Name;

            PTS_MAM3.Web.DataSource.WSMAMFunctions obj = new PTS_MAM3.Web.DataSource.WSMAMFunctions();
            string ReturnVideoID = "";
            ReturnVideoID = obj.QueryVideoID_PROG(TEMPDatatable.Rows[i]["FSPROMO_ID"].ToString(), "0", _WorkChannelID);
            if (ReturnVideoID == "        ")
            {
               
            }
            TempArrPromoList.FSVIDEO_ID = ReturnVideoID;
            DatePlayList.Add(TempArrPromoList);
        }

        //取得節目段落所要插入的Promo資料
        public void getProgSegmentPromo(string FSProgID, string FSProg_Name, string FNSEQNO, string FNBreakNO, DataTable Prog_Promo_Link, bool LastSeg, bool TimeOne, bool TimeTwo, bool TimeThree)
        {
            DataTable ProgSegPromo = new DataTable();
            ProgSegPromo.Columns.Add("FNARR_PROMO_NO", typeof(string));
            ProgSegPromo.Columns.Add("FSPROG_ID", typeof(string));
            ProgSegPromo.Columns.Add("FNSEQNO", typeof(string));
            ProgSegPromo.Columns.Add("FDDATE", typeof(string));
            ProgSegPromo.Columns.Add("FSCHANNEL_ID", typeof(string));
            ProgSegPromo.Columns.Add("FNBREAK_NO", typeof(string));
            ProgSegPromo.Columns.Add("FNSUB_NO", typeof(string));
            ProgSegPromo.Columns.Add("FSPROMO_ID", typeof(string));
            ProgSegPromo.Columns.Add("FSPLAYTIME", typeof(string));
            ProgSegPromo.Columns.Add("FSDUR", typeof(string));
            ProgSegPromo.Columns.Add("FSSIGNAL", typeof(string));
            ProgSegPromo.Columns.Add("FSMEMO", typeof(string));
            ProgSegPromo.Columns.Add("FSMEMO1", typeof(string));
            ProgSegPromo.Columns.Add("FSCREATED_BY", typeof(string));
            ProgSegPromo.Columns.Add("FDCREATED_DATE", typeof(string));
            ProgSegPromo.Columns.Add("FSCHANNEL_NAME", typeof(string));
            ProgSegPromo.Columns.Add("FSFILE_NO", typeof(string));
            ProgSegPromo.Columns.Add("FSVIDEO_ID", typeof(string));
            ProgSegPromo.Columns.Add("FSPROMO_NAME", typeof(string));


            int SUBNO = 1;
            if (FNBreakNO == "0") //第0段 要插入節目預排宣傳帶(抬頭卡)與"A??"系列的頻道預排宣傳帶
            {
                //除了宏觀外預排宣傳帶的順序都是先頻道預排宣傳帶之後再放節目預排宣傳帶
               if (_WorkChannelID=="08") //宏觀頻道  先放節目預排
               {
                   for (int i = 0; i <= Prog_Promo_Link.Rows.Count - 1; i++)
                   {
                       ArrPromoList TempArrPromoList = new ArrPromoList();
                       TempArrPromoList.COMBINENO = "";
                       TempArrPromoList.FSPROG_ID = FSProgID;
                       TempArrPromoList.FNSEQNO = FNSEQNO;
                       TempArrPromoList.FSNAME = Prog_Promo_Link.Rows[i]["FSPROMO_NAME"].ToString();
                       TempArrPromoList.FSCHANNEL_ID = "";
                       TempArrPromoList.FSCHANNEL_NAME = "";
                       TempArrPromoList.FSPLAY_TIME = "";
                       TempArrPromoList.FSDURATION = Prog_Promo_Link.Rows[i]["FSDURATION"].ToString();
                       TempArrPromoList.FNEPISODE = "";
                       TempArrPromoList.FNBREAK_NO = FNBreakNO;
                       TempArrPromoList.FNSUB_NO = SUBNO.ToString();
                       TempArrPromoList.FSFILE_NO = "";//Prog_Promo_Link.Rows[i]["FSFILE_NO"].ToString();
                       TempArrPromoList.FSVIDEO_ID = "";//Prog_Promo_Link.Rows[i]["FSVIDEO_ID"].ToString();
                       TempArrPromoList.FSPROMO_ID = Prog_Promo_Link.Rows[i]["FSPROMO_ID"].ToString();
                       TempArrPromoList.FDDATE = "";
                       TempArrPromoList.FNLIVE = "0";
                       TempArrPromoList.FCTYPE = "P";
                       TempArrPromoList.FSSTATUS = "";
                       TempArrPromoList.FSSIGNAL = "COMM";
                       TempArrPromoList.FSMEMO = "";// Prog_Promo_Link.Rows[i]["FSMEMO"].ToString();
                       TempArrPromoList.FSMEMO1 = "";// Prog_Promo_Link.Rows[i]["FSMEMO"].ToString();
                       TempArrPromoList.FSPROG_NAME = FSProg_Name;
                       PTS_MAM3.Web.DataSource.WSMAMFunctions obj = new PTS_MAM3.Web.DataSource.WSMAMFunctions();
                       string ReturnVideoID = "";
                       ReturnVideoID = obj.QueryVideoID_PROG(Prog_Promo_Link.Rows[i]["FSPROMO_ID"].ToString(), "0", _WorkChannelID);
                       if (ReturnVideoID == "        ")
                       {

                       }
                       TempArrPromoList.FSVIDEO_ID = ReturnVideoID;
                       DatePlayList.Add(TempArrPromoList);
                       SUBNO = SUBNO + 1;
                   }
               }
 
                //插入"A??"系列的頻道預排宣傳帶
                for (int i = 0; i <= AAA.Rows.Count - 1; i++)
                {
                    addPromoRows(ProgSegPromo, AAA, i, SUBNO, FSProgID, FNSEQNO, FNBreakNO, FSProg_Name);
                    SUBNO = SUBNO + 1;
                }
                for (int i = 0; i <= AAC.Rows.Count - 1; i++)
                {
                    if (_IAAC > AAC.Rows.Count - 1)
                        _IAAC = _IAAC - AAC.Rows.Count;
                    if (i == _IAAC)
                    {
                        addPromoRows(ProgSegPromo, AAC, i, SUBNO, FSProgID, FNSEQNO, FNBreakNO, FSProg_Name);
                        SUBNO = SUBNO + 1;
                        _IAAC++;
                        break;
                    }
                }

                if (TimeOne == true)
                {
                    for (int i = 0; i <= A1A.Rows.Count - 1; i++)
                    {
                        addPromoRows(ProgSegPromo, A1A, i, SUBNO, FSProgID, FNSEQNO, FNBreakNO, FSProg_Name);
                        SUBNO = SUBNO + 1;
                    }
                    for (int i = 0; i <= A1C.Rows.Count - 1; i++)
                    {
                        if (_IA1C > A1C.Rows.Count - 1)
                            _IA1C = _IA1C - A1C.Rows.Count;
                        if (i == _IA1C)
                        {
                            addPromoRows(ProgSegPromo, A1C, i, SUBNO, FSProgID, FNSEQNO, FNBreakNO, FSProg_Name);
                            SUBNO = SUBNO + 1;
                            _IA1C++;
                            break;
                        }
                    }
                }
                if (TimeTwo == true)
                {
                    for (int i = 0; i <= A2A.Rows.Count - 1; i++)
                    {
                        addPromoRows(ProgSegPromo, A2A, i, SUBNO, FSProgID, FNSEQNO, FNBreakNO, FSProg_Name);
                        SUBNO = SUBNO + 1;
                    }
                    for (int i = 0; i <= A2C.Rows.Count - 1; i++)
                    {
                        if (_IA2C > A2C.Rows.Count - 1)
                            _IA2C = _IA2C - A2C.Rows.Count;
                        if (i == _IA2C)
                        {
                            addPromoRows(ProgSegPromo, A2C, i, SUBNO, FSProgID, FNSEQNO, FNBreakNO, FSProg_Name);
                            SUBNO = SUBNO + 1;
                            _IA2C++;
                            break;
                        }
                    }
                }
                if (TimeThree == true)
                {
                    for (int i = 0; i <= A3A.Rows.Count - 1; i++)
                    {
                        addPromoRows(ProgSegPromo, A3A, i, SUBNO, FSProgID, FNSEQNO, FNBreakNO, FSProg_Name);
                        SUBNO = SUBNO + 1;
                    }
                    for (int i = 0; i <= A3C.Rows.Count - 1; i++)
                    {
                        if (_IA3C > A3C.Rows.Count - 1)
                            _IA3C = _IA3C - A3C.Rows.Count;
                        if (i == _IA3C)
                        {
                            addPromoRows(ProgSegPromo, A3C, i, SUBNO, FSProgID, FNSEQNO, FNBreakNO, FSProg_Name);
                            SUBNO = SUBNO + 1;
                            _IA3C++;
                            break;
                        }
                    }
                }

                if (_WorkChannelID != "08") //非宏觀頻道  先放頻道預排,放完再放節目預排
                {
                    for (int i = 0; i <= Prog_Promo_Link.Rows.Count - 1; i++)
                    {
                        ArrPromoList TempArrPromoList = new ArrPromoList();
                        TempArrPromoList.COMBINENO = "";
                        TempArrPromoList.FSPROG_ID = FSProgID;
                        TempArrPromoList.FNSEQNO = FNSEQNO;
                        TempArrPromoList.FSNAME = Prog_Promo_Link.Rows[i]["FSPROMO_NAME"].ToString();
                        TempArrPromoList.FSCHANNEL_ID = "";
                        TempArrPromoList.FSCHANNEL_NAME = "";
                        TempArrPromoList.FSPLAY_TIME = "";
                        TempArrPromoList.FSDURATION = Prog_Promo_Link.Rows[i]["FSDURATION"].ToString();
                        TempArrPromoList.FNEPISODE = "";
                        TempArrPromoList.FNBREAK_NO = FNBreakNO;
                        TempArrPromoList.FNSUB_NO = SUBNO.ToString();
                        TempArrPromoList.FSFILE_NO = "";//Prog_Promo_Link.Rows[i]["FSFILE_NO"].ToString();
                        TempArrPromoList.FSVIDEO_ID = "";//Prog_Promo_Link.Rows[i]["FSVIDEO_ID"].ToString();
                        TempArrPromoList.FSPROMO_ID = Prog_Promo_Link.Rows[i]["FSPROMO_ID"].ToString();
                        TempArrPromoList.FDDATE = "";
                        TempArrPromoList.FNLIVE = "0";
                        TempArrPromoList.FCTYPE = "P";
                        TempArrPromoList.FSSTATUS = "";
                        TempArrPromoList.FSSIGNAL = "COMM";
                        TempArrPromoList.FSMEMO = "";// Prog_Promo_Link.Rows[i]["FSMEMO"].ToString();
                        TempArrPromoList.FSMEMO1 = "";// Prog_Promo_Link.Rows[i]["FSMEMO"].ToString();
                        TempArrPromoList.FSPROG_NAME = FSProg_Name;
                        PTS_MAM3.Web.DataSource.WSMAMFunctions obj = new PTS_MAM3.Web.DataSource.WSMAMFunctions();
                        string ReturnVideoID = "";
                        ReturnVideoID = obj.QueryVideoID_PROG(Prog_Promo_Link.Rows[i]["FSPROMO_ID"].ToString(), "0", _WorkChannelID);
                        if (ReturnVideoID == "        ")
                        {

                        }
                        TempArrPromoList.FSVIDEO_ID = ReturnVideoID;
                        DatePlayList.Add(TempArrPromoList);
                        SUBNO = SUBNO + 1;
                    }
                }
            }
            else // if (FNBreakNO == "0") 
            {
                //不是第0段
                if (LastSeg == false)//不是最後一段, 要插入"B??"系列的頻道預排宣傳帶
                {
                    //插入"B??"系列的頻道預排宣傳帶
                    for (int i = 0; i <= BAA.Rows.Count - 1; i++)
                    {
                        addPromoRows(ProgSegPromo, BAA, i, SUBNO, FSProgID, FNSEQNO, FNBreakNO, FSProg_Name);
                        SUBNO = SUBNO + 1;
                    }
                    for (int i = 0; i <= BAC.Rows.Count - 1; i++)
                    {
                        if (_IBAC > BAC.Rows.Count - 1)
                            _IBAC = _IBAC - BAC.Rows.Count;
                        if (i == _IBAC)
                        {
                            addPromoRows(ProgSegPromo, BAC, i, SUBNO, FSProgID, FNSEQNO, FNBreakNO, FSProg_Name);
                            SUBNO = SUBNO + 1;
                            _IBAC++;
                            break;
                        }
                    }

                    if (TimeOne == true)
                    {
                        for (int i = 0; i <= B1A.Rows.Count - 1; i++)
                        {
                            addPromoRows(ProgSegPromo, B1A, i, SUBNO, FSProgID, FNSEQNO, FNBreakNO, FSProg_Name);
                            SUBNO = SUBNO + 1;
                        }
                        for (int i = 0; i <= B1C.Rows.Count - 1; i++)
                        {
                            if (_IB1C > B1C.Rows.Count - 1)
                                _IB1C = _IB1C - B1C.Rows.Count;
                            if (i == _IB1C)
                            {
                                addPromoRows(ProgSegPromo, B1C, i, SUBNO, FSProgID, FNSEQNO, FNBreakNO, FSProg_Name);
                                SUBNO = SUBNO + 1;
                                _IB1C++;
                                break;
                            }
                        }
                    }
                    if (TimeTwo == true)
                    {
                        for (int i = 0; i <= B2A.Rows.Count - 1; i++)
                        {
                            addPromoRows(ProgSegPromo, B2A, i, SUBNO, FSProgID, FNSEQNO, FNBreakNO, FSProg_Name);
                            SUBNO = SUBNO + 1;
                        }
                        for (int i = 0; i <= B2C.Rows.Count - 1; i++)
                        {
                            if (_IB2C > B2C.Rows.Count - 1)
                                _IB2C = _IB2C - B2C.Rows.Count;
                            if (i == _IB2C)
                            {
                                addPromoRows(ProgSegPromo, B2C, i, SUBNO, FSProgID, FNSEQNO, FNBreakNO, FSProg_Name);
                                SUBNO = SUBNO + 1;
                                _IB2C++;
                                break;
                            }
                        }
                    }
                    if (TimeThree == true)
                    {
                        for (int i = 0; i <= B3A.Rows.Count - 1; i++)
                        {
                            addPromoRows(ProgSegPromo, B3A, i, SUBNO, FSProgID, FNSEQNO, FNBreakNO, FSProg_Name);
                            SUBNO = SUBNO + 1;
                        }
                        for (int i = 0; i <= B3C.Rows.Count - 1; i++)
                        {
                            if (_IB3C > B3C.Rows.Count - 1)
                                _IB3C = _IB3C - B3C.Rows.Count;
                            if (i == _IB3C)
                            {
                                addPromoRows(ProgSegPromo, B3C, i, SUBNO, FSProgID, FNSEQNO, FNBreakNO, FSProg_Name);
                                SUBNO = SUBNO + 1;
                                _IB3C++;
                                break;
                            }
                        }
                    }
                }
                else //最後一段, 要插入"C??"系列的頻道預排宣傳帶
                {
                    //插入"C??"系列的頻道預排宣傳帶
                    for (int i = 0; i <= CAA.Rows.Count - 1; i++) //最後一段的全部時段,全部宣傳帶
                    {
                        addPromoRows(ProgSegPromo, CAA, i, SUBNO, FSProgID, FNSEQNO, FNBreakNO, FSProg_Name);
                        SUBNO = SUBNO + 1;
                    }
                    for (int i = 0; i <= CAC.Rows.Count - 1; i++) //最後一段的全部時段,循環宣傳帶
                    {
                        if (_ICAC > CAC.Rows.Count - 1)
                            _ICAC = _ICAC - CAC.Rows.Count;
                        if (i == _ICAC)
                        {
                            addPromoRows(ProgSegPromo, CAC, i, SUBNO, FSProgID, FNSEQNO, FNBreakNO, FSProg_Name);
                            SUBNO = SUBNO + 1;
                            _ICAC++;
                            break;
                        }
                    }

                    if (TimeOne == true)
                    {
                        for (int i = 0; i <= C1A.Rows.Count - 1; i++)
                        {
                            addPromoRows(ProgSegPromo, C1A, i, SUBNO, FSProgID, FNSEQNO, FNBreakNO, FSProg_Name);
                            SUBNO = SUBNO + 1;
                        }
                        for (int i = 0; i <= C1C.Rows.Count - 1; i++)
                        {
                            if (_IC1C > C1C.Rows.Count - 1)
                                _IC1C = _IC1C - C1C.Rows.Count;
                            if (i == _IC1C)
                            {
                                addPromoRows(ProgSegPromo, C1C, i, SUBNO, FSProgID, FNSEQNO, FNBreakNO, FSProg_Name);
                                SUBNO = SUBNO + 1;
                                _IC1C++;
                                break;
                            }
                        }
                    }
                    if (TimeTwo == true)
                    {
                        for (int i = 0; i <= C2A.Rows.Count - 1; i++)
                        {
                            addPromoRows(ProgSegPromo, C2A, i, SUBNO, FSProgID, FNSEQNO, FNBreakNO, FSProg_Name);
                            SUBNO = SUBNO + 1;
                        }
                        for (int i = 0; i <= C2C.Rows.Count - 1; i++)
                        {
                            if (_IC2C > C2C.Rows.Count - 1)
                                _IC2C = _IC2C - C2C.Rows.Count;
                            if (i == _IC2C)
                            {
                                addPromoRows(ProgSegPromo, C2C, i, SUBNO, FSProgID, FNSEQNO, FNBreakNO, FSProg_Name);
                                SUBNO = SUBNO + 1;
                                _IC2C++;
                                break;
                            }
                        }
                    }
                    if (TimeThree == true)
                    {
                        for (int i = 0; i <= C3A.Rows.Count - 1; i++)
                        {
                            addPromoRows(ProgSegPromo, C3A, i, SUBNO, FSProgID, FNSEQNO, FNBreakNO, FSProg_Name);
                            SUBNO = SUBNO + 1;
                        }
                        for (int i = 0; i <= C3C.Rows.Count - 1; i++)
                        {
                            if (_IC3C > C3C.Rows.Count - 1)
                                _IC3C = _IC3C - C3C.Rows.Count;
                            if (i == _IC3C)
                            {
                                addPromoRows(ProgSegPromo, C3C, i, SUBNO, FSProgID, FNSEQNO, FNBreakNO, FSProg_Name);
                                SUBNO = SUBNO + 1;
                                _IC3C++;
                                break;
                            }
                        }
                    }
                }

            }

            //return ProgSegPromo;
        }


        //取得節目段落資料,如有段落資料則取段落資料,如果沒有則產生預設段落
        [WebMethod()]
        public string getPromoFileNO(string FSPROMO_ID, string FSChannel_ID)
        {
            //DataTable dtProgSeg = new DataTable();
            //每一個節目,先查詢節目段落看是否有資料
            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection1 = new SqlConnection(sqlConnStr);
            if (connection1.State == System.Data.ConnectionState.Closed)
            {
                connection1.Open();
            }
            StringBuilder sb = new StringBuilder();
            SqlDataAdapter daPROMOFILENO = new SqlDataAdapter("SP_Q_GET_PROMO_FILENO", connection1);
            DataTable dtPROMOFILENO = new DataTable();
            SqlParameter paraPROGID = new SqlParameter("@FSPROMO_ID", FSPROMO_ID);
            daPROMOFILENO.SelectCommand.Parameters.Add(paraPROGID);
            SqlParameter paraChannelID = new SqlParameter("@FSCHANNEL_ID", FSChannel_ID);
            daPROMOFILENO.SelectCommand.Parameters.Add(paraChannelID);
            daPROMOFILENO.SelectCommand.CommandType = CommandType.StoredProcedure;
            daPROMOFILENO.Fill(dtPROMOFILENO);
            if (dtPROMOFILENO.Rows.Count > 0)
            {
                connection1.Close();
                return dtPROMOFILENO.Rows[0]["FSFILE_NO"].ToString();
            }
            else 
            {
                return "";
            }
        }

        //取得節目段落所要插入的Promo資料
        public void getArrPromo(string FSProgID, string FSProg_Name, string FNSEQNO, string FNBreakNO,string FNEpisode, DataTable Arr_Promo_List)
        {
            DataTable ProgSegPromo = new DataTable();
            ProgSegPromo.Columns.Add("FNARR_PROMO_NO", typeof(string));
            ProgSegPromo.Columns.Add("FSPROG_ID", typeof(string));
            ProgSegPromo.Columns.Add("FNSEQNO", typeof(string));
            ProgSegPromo.Columns.Add("FDDATE", typeof(string));
            ProgSegPromo.Columns.Add("FSCHANNEL_ID", typeof(string));
            ProgSegPromo.Columns.Add("FNBREAK_NO", typeof(string));
            ProgSegPromo.Columns.Add("FNSUB_NO", typeof(string));
            ProgSegPromo.Columns.Add("FSPROMO_ID", typeof(string));
            ProgSegPromo.Columns.Add("FSPLAYTIME", typeof(string));
            ProgSegPromo.Columns.Add("FSDUR", typeof(string));
            ProgSegPromo.Columns.Add("FSMEMO", typeof(string));
            ProgSegPromo.Columns.Add("FSCREATED_BY", typeof(string));
            ProgSegPromo.Columns.Add("FDCREATED_DATE", typeof(string));
            ProgSegPromo.Columns.Add("FSCHANNEL_NAME", typeof(string));
            ProgSegPromo.Columns.Add("FSFILE_NO", typeof(string));
            ProgSegPromo.Columns.Add("FSVIDEO_ID", typeof(string));
            ProgSegPromo.Columns.Add("FSPROMO_NAME", typeof(string));
            for (int i = 0; i <= Arr_Promo_List.Rows.Count - 1; i++)
            {
                if (Arr_Promo_List.Rows[i]["FSPROG_ID"].ToString() == FSProgID && Arr_Promo_List.Rows[i]["FNSEQNO"].ToString() == FNSEQNO && Arr_Promo_List.Rows[i]["FNBREAK_NO"].ToString() == FNBreakNO)
                {
                    ArrPromoList TempArrPromoList = new ArrPromoList();
                    TempArrPromoList.COMBINENO = "";
                    TempArrPromoList.FSPROG_ID = FSProgID;
                    TempArrPromoList.FNSEQNO = FNSEQNO;
                    TempArrPromoList.FSNAME = Arr_Promo_List.Rows[i]["FSPROMO_NAME"].ToString();
                    TempArrPromoList.FSCHANNEL_ID = "";
                    TempArrPromoList.FSCHANNEL_NAME = "";
                    TempArrPromoList.FSPLAY_TIME = Arr_Promo_List.Rows[i]["FSPLAYTIME"].ToString();
                    TempArrPromoList.FSDURATION = Arr_Promo_List.Rows[i]["FSDUR"].ToString();
                    TempArrPromoList.FNEPISODE = "";
                    TempArrPromoList.FNBREAK_NO = FNBreakNO;
                    TempArrPromoList.FNSUB_NO = Arr_Promo_List.Rows[i]["FNSUB_NO"].ToString();
                    if (Arr_Promo_List.Rows[i]["FSFILE_NO"].ToString()=="")
                        TempArrPromoList.FSFILE_NO = getPromoFileNO(Arr_Promo_List.Rows[i]["FSPROMO_ID"].ToString(), Arr_Promo_List.Rows[i]["FSCHANNEL_ID"].ToString());
                    else
                        TempArrPromoList.FSFILE_NO = Arr_Promo_List.Rows[i]["FSFILE_NO"].ToString();
                    TempArrPromoList.FSVIDEO_ID = Arr_Promo_List.Rows[i]["FSVIDEO_ID"].ToString();
                    TempArrPromoList.FSPROMO_ID = Arr_Promo_List.Rows[i]["FSPROMO_ID"].ToString();
                    TempArrPromoList.FDDATE = "";
                    TempArrPromoList.FNLIVE = "0";
                    TempArrPromoList.FCTYPE = "P";
                    TempArrPromoList.FSSTATUS = "";
                    TempArrPromoList.FSSIGNAL = Arr_Promo_List.Rows[i]["FSSIGNAL"].ToString();
                    TempArrPromoList.FSMEMO = Arr_Promo_List.Rows[i]["FSMEMO"].ToString();
                    TempArrPromoList.FSMEMO1 = Arr_Promo_List.Rows[i]["FSMEMO1"].ToString();
                    TempArrPromoList.FSPROG_NAME = FSProg_Name;
                    TempArrPromoList.FCTIME_TYPE = Arr_Promo_List.Rows[i]["FCTIMETYPE"].ToString();
                    DatePlayList.Add(TempArrPromoList);
                    Arr_Promo_List.Rows[i]["hasInsert"] = "Y";
                    Arr_Promo_List.Rows[i]["FNEPISODE"] = FNEpisode;
                }
            }

        }

        //取得節目段落所要插入的Promo資料,可能會有多筆資料相同,因為HDSD的關係,需比對FNARR_PROMO_NO相同取一筆
        public void getArrPromo_MIX(string FSProgID, string FSProg_Name, string FNSEQNO, string FNBreakNO, string FNEpisode, DataTable Arr_Promo_List)
        {
            DataTable ProgSegPromo = new DataTable();
            ProgSegPromo.Columns.Add("FNARR_PROMO_NO", typeof(string));
            ProgSegPromo.Columns.Add("FSPROG_ID", typeof(string));
            ProgSegPromo.Columns.Add("FNSEQNO", typeof(string));
            ProgSegPromo.Columns.Add("FDDATE", typeof(string));
            ProgSegPromo.Columns.Add("FSCHANNEL_ID", typeof(string));
            ProgSegPromo.Columns.Add("FNBREAK_NO", typeof(string));
            ProgSegPromo.Columns.Add("FNSUB_NO", typeof(string));
            ProgSegPromo.Columns.Add("FSPROMO_ID", typeof(string));
            ProgSegPromo.Columns.Add("FSPLAYTIME", typeof(string));
            ProgSegPromo.Columns.Add("FSDUR", typeof(string));
            ProgSegPromo.Columns.Add("FSMEMO", typeof(string));
            ProgSegPromo.Columns.Add("FSCREATED_BY", typeof(string));
            ProgSegPromo.Columns.Add("FDCREATED_DATE", typeof(string));
            ProgSegPromo.Columns.Add("FSCHANNEL_NAME", typeof(string));
            ProgSegPromo.Columns.Add("FSFILE_NO", typeof(string));
            ProgSegPromo.Columns.Add("FSVIDEO_ID", typeof(string));
            ProgSegPromo.Columns.Add("FSPROMO_NAME", typeof(string));
            string FNARR_PROMO_NO = "";
            for (int i = 0; i <= Arr_Promo_List.Rows.Count - 1; i++)
            {
                if (Arr_Promo_List.Rows[i]["FSPROG_ID"].ToString() == FSProgID && Arr_Promo_List.Rows[i]["FNSEQNO"].ToString() == FNSEQNO && Arr_Promo_List.Rows[i]["FNBREAK_NO"].ToString() == FNBreakNO)
                {
                    if (FNARR_PROMO_NO != Arr_Promo_List.Rows[i]["FNARR_PROMO_NO"].ToString())
                    { //代表有換號需比對下一筆是否相同,判斷HD的資料是否有File_NO
                        int WorkRow = i;
                        if (i + 1 <= Arr_Promo_List.Rows.Count - 1)
                        {
                            if (Arr_Promo_List.Rows[i]["FNARR_PROMO_NO"].ToString() == Arr_Promo_List.Rows[i + 1]["FNARR_PROMO_NO"].ToString())
                            { //有兩筆相同資料
                                if (Arr_Promo_List.Rows[i]["FSARC_TYPE"].ToString() == "002")
                                {
                                    if (Arr_Promo_List.Rows[i]["FSFILE_NO"].ToString() == "" && Arr_Promo_List.Rows[i + 1]["FSFILE_NO"].ToString() != "")
                                    {
                                        WorkRow = i + 1;
                                    }
                                }
                                else
                                {
                                    WorkRow = i + 1;
                                    if (Arr_Promo_List.Rows[i+1]["FSFILE_NO"].ToString() == "" && Arr_Promo_List.Rows[i]["FSFILE_NO"].ToString() != "")
                                    {
                                        WorkRow = i;
                                    }                                
                                }
                               
                            }
                        }


                        ArrPromoList TempArrPromoList = new ArrPromoList();
                        TempArrPromoList.COMBINENO = "";
                        TempArrPromoList.FSPROG_ID = FSProgID;
                        TempArrPromoList.FNSEQNO = FNSEQNO;
                        TempArrPromoList.FSNAME = Arr_Promo_List.Rows[WorkRow]["FSPROMO_NAME"].ToString();
                        TempArrPromoList.FSCHANNEL_ID = "";
                        TempArrPromoList.FSCHANNEL_NAME = "";
                        TempArrPromoList.FSPLAY_TIME = Arr_Promo_List.Rows[WorkRow]["FSPLAYTIME"].ToString();
                        TempArrPromoList.FSDURATION = Arr_Promo_List.Rows[WorkRow]["FSDUR"].ToString();
                        TempArrPromoList.FNEPISODE = "";
                        TempArrPromoList.FNBREAK_NO = FNBreakNO;
                        TempArrPromoList.FNSUB_NO = Arr_Promo_List.Rows[i]["FNSUB_NO"].ToString();
                        if (Arr_Promo_List.Rows[WorkRow]["FSFILE_NO"].ToString() == "")
                            TempArrPromoList.FSFILE_NO = getPromoFileNO(Arr_Promo_List.Rows[i]["FSPROMO_ID"].ToString(), Arr_Promo_List.Rows[i]["FSCHANNEL_ID"].ToString());
                        else
                            TempArrPromoList.FSFILE_NO = Arr_Promo_List.Rows[WorkRow]["FSFILE_NO"].ToString();
                        TempArrPromoList.FSVIDEO_ID = Arr_Promo_List.Rows[WorkRow]["FSVIDEO_ID"].ToString();
                        TempArrPromoList.FSPROMO_ID = Arr_Promo_List.Rows[WorkRow]["FSPROMO_ID"].ToString();
                        TempArrPromoList.FDDATE = "";
                        TempArrPromoList.FNLIVE = "0";
                        TempArrPromoList.FCTYPE = "P";
                        TempArrPromoList.FSSTATUS = "";
                        TempArrPromoList.FSSIGNAL = Arr_Promo_List.Rows[WorkRow]["FSSIGNAL"].ToString();
                        TempArrPromoList.FSMEMO = Arr_Promo_List.Rows[WorkRow]["FSMEMO"].ToString();
                        TempArrPromoList.FSMEMO1 = Arr_Promo_List.Rows[WorkRow]["FSMEMO1"].ToString();
                        TempArrPromoList.FSPROG_NAME = FSProg_Name;
                        TempArrPromoList.FCTIME_TYPE = Arr_Promo_List.Rows[WorkRow]["FCTIMETYPE"].ToString();
                        DatePlayList.Add(TempArrPromoList);
                        Arr_Promo_List.Rows[WorkRow]["hasInsert"] = "Y";
                        Arr_Promo_List.Rows[WorkRow]["FNEPISODE"] = FNEpisode;
                    }
                }
                FNARR_PROMO_NO = Arr_Promo_List.Rows[i]["FNARR_PROMO_NO"].ToString();
            }

        }
        public bool CheckISTimeONE(string InTime)
        {
            bool TRTime;
            int HH = 0;
            int MM = 0;

            HH = Int32.Parse(InTime.Substring(0, 2));
            MM = Int32.Parse(InTime.Substring(2, 2));
            int AllMin = HH * 60 + MM;
            int LostHH;
            LostHH = (int)(Math.Floor((double)AllMin / (double)120));

            int NowMin = AllMin - LostHH * 120;

            if (NowMin >= 56 & NowMin <= 64) //前後4分鐘
                TRTime = true;
            else
                TRTime = false;
            return TRTime;
        }
        public bool CheckISTimeTWO(string InTime)
        {
            bool TRTime;
            int HH = 0;
            int MM = 0;

            HH = Int32.Parse(InTime.Substring(0, 2));
            MM = Int32.Parse(InTime.Substring(2, 2));
            int AllMin = HH * 60 + MM;
            int LostHH;
            LostHH = (int)(Math.Floor((double)AllMin / (double)120));

            int NowMin = AllMin - LostHH * 120;

            if (NowMin >= 116 || NowMin <= 4) //前後4分鐘
                TRTime = true;
            else
                TRTime = false;
            return TRTime;
        }
        public bool CheckISTimeTHREE(string InTime)
        {
            bool TRTime;
            int HH = 0;
            int MM = 0;

            HH = Int32.Parse(InTime.Substring(0, 2));
            MM = Int32.Parse(InTime.Substring(2, 2));
            int AllMin = HH * 60 + MM;
            int LostHH;
            LostHH = (int)(Math.Floor((double)AllMin / (double)180));

            int NowMin = AllMin - LostHH * 180;

            if (NowMin >= 176 || NowMin <= 4) //前後4分鐘
                TRTime = true;
            else
                TRTime = false;
            return TRTime;
        }
        public int CALProgTime(string ThisProgTIme, string NextProgTime)
        {
            int HH = 0;
            int MM = 0;
            HH = Int32.Parse(ThisProgTIme.Substring(0, 2));
            MM = Int32.Parse(ThisProgTIme.Substring(2, 2));

            int HH1 = 0;
            int MM1 = 0;
            HH1 = Int32.Parse(NextProgTime.Substring(0, 2));
            MM1 = Int32.Parse(NextProgTime.Substring(2, 2));
            return HH1 * 60 + MM1 - HH * 60 - MM;
        }

        //插入一個空白段落,當節目開始時
        public void InsertNullProgSeg(string progname)
        {
            ArrPromoList NullPromoList = new ArrPromoList();
            NullPromoList.COMBINENO = "";
            NullPromoList.FSPROG_ID = "";
            NullPromoList.FNSEQNO = "";
            NullPromoList.FSNAME = "-------------------";
            NullPromoList.FSCHANNEL_ID = "";
            NullPromoList.FSCHANNEL_NAME = "";
            NullPromoList.FSPLAY_TIME = "";
            NullPromoList.FSDURATION = "";
            NullPromoList.FNEPISODE = "";
            NullPromoList.FNBREAK_NO = "";
            NullPromoList.FNSUB_NO = "";
            NullPromoList.FSFILE_NO = "";
            NullPromoList.FSVIDEO_ID = "";
            NullPromoList.FSPROMO_ID = "";
            NullPromoList.FDDATE = "";
            NullPromoList.FNLIVE = "";
            NullPromoList.FCTYPE = "";
            NullPromoList.FSSTATUS = "";
            NullPromoList.FSSIGNAL = "";
            NullPromoList.FSMEMO = "";
            NullPromoList.FSMEMO1 = "";
            NullPromoList.FSPROG_NAME = "";
            DatePlayList.Add(NullPromoList);
        }

        string _WorkChannelID = "";
        //產生節目播出運行表,由TBPGM_QUEUE產生的節目表,一一先查詢段落資料再查詢
        [WebMethod()]
        public string Do_Query_Prog_Queue_Seg(string strSQL, string parameters = null)
        {
            //strSQL為查詢Store Procesure Name
            //parameters為XML內容(DB_Function_Sample.txt)

            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            //StringBuilder sb = new StringBuilder();

            SqlDataAdapter daQueue = new SqlDataAdapter("SP_Q_TBPGM_QUEUE", connection);
            DataTable dtQueue = new DataTable();
            
            SqlDataAdapter daCombineQueue = new SqlDataAdapter("SP_Q_TBPGM_COMBINE_QUEUE", connection);
            DataTable dtCombineQueue = new DataTable();

            SqlDataAdapter daArrPromo = new SqlDataAdapter("SP_Q_TBPGM_ARR_PROMO_NEW_VIDEOID", connection);
            DataTable dtArrPromo = new DataTable();

            string ParaChannel = "";
            string ParaDate = "";
            //參數剖析
            #region 剖析參數取得日期及頻道參數
            if (parameters != "")
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(parameters);
                XmlNode xmlNode = xmldoc.FirstChild;
                for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                {
                    if (xmlNode.ChildNodes[i].FirstChild == null || xmlNode.ChildNodes[i].FirstChild.Value == "")
                    {
                        SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, "");
                        SqlParameter para1 = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, "");
                        SqlParameter para2 = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, "");
                        daQueue.SelectCommand.Parameters.Add(para);
                        daArrPromo.SelectCommand.Parameters.Add(para1);
                        daCombineQueue.SelectCommand.Parameters.Add(para2);
                    }
                    else
                    {
                        SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, xmlNode.ChildNodes[i].FirstChild.Value);
                        SqlParameter para1 = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, xmlNode.ChildNodes[i].FirstChild.Value);
                        SqlParameter para2 = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, xmlNode.ChildNodes[i].FirstChild.Value);
                        daQueue.SelectCommand.Parameters.Add(para);
                        daArrPromo.SelectCommand.Parameters.Add(para1);
                        daCombineQueue.SelectCommand.Parameters.Add(para2);
                        if (xmlNode.ChildNodes[i].Name == "FSCHANNEL_ID")
                        {
                            ParaChannel = xmlNode.ChildNodes[i].FirstChild.Value;
                            _WorkChannelID = xmlNode.ChildNodes[i].FirstChild.Value;
                        }
                        if (xmlNode.ChildNodes[i].Name == "FDDATE")
                            ParaDate = xmlNode.ChildNodes[i].FirstChild.Value;

                    }
                }

            }
            #endregion
            #region 取得頻道預排宣傳帶
            SqlDataAdapter da_CHANNEL_PROMO_LINK = new SqlDataAdapter("SP_Q_TBPGM_CHANNEL_PROMO_LINK", connection);
            DataTable dt_CHANNEL_PROMO_LINK = new DataTable();

            //SqlParameter para_CHANNEL_PROMO_LINK = new SqlParameter("@FSCHANNEL_ID", dtQueue.Rows[1]["FSCHANNEL_ID"].ToString());
            SqlParameter para_CHANNEL_PROMO_LINK = new SqlParameter("@FSCHANNEL_ID", ParaChannel);
            da_CHANNEL_PROMO_LINK.SelectCommand.Parameters.Add(para_CHANNEL_PROMO_LINK);
            SqlParameter para_CHANNEL_PROMO_LINK1 = new SqlParameter("@FCLINK_TYPE", "");
            da_CHANNEL_PROMO_LINK.SelectCommand.Parameters.Add(para_CHANNEL_PROMO_LINK1);
            SqlParameter para_CHANNEL_PROMO_LINK2 = new SqlParameter("@FCTIME_TYPE", "");
            da_CHANNEL_PROMO_LINK.SelectCommand.Parameters.Add(para_CHANNEL_PROMO_LINK2);
            SqlParameter para_CHANNEL_PROMO_LINK3 = new SqlParameter("@FCINSERT_TYPE", "");
            da_CHANNEL_PROMO_LINK.SelectCommand.Parameters.Add(para_CHANNEL_PROMO_LINK3);

            #endregion

            int Combineno = 1;
            string NowPlaytime = "";
            string NextPlaytime = "";
            daQueue.SelectCommand.CommandType = CommandType.StoredProcedure;
            daArrPromo.SelectCommand.CommandType = CommandType.StoredProcedure;
            daCombineQueue.SelectCommand.CommandType = CommandType.StoredProcedure;
            da_CHANNEL_PROMO_LINK.SelectCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                //先查詢QUEUE&Arrpromo的資料
                daQueue.Fill(dtQueue);

                daArrPromo.Fill(dtArrPromo);
                daCombineQueue.Fill(dtCombineQueue);
                dtArrPromo.Columns.Add("hasInsert");
                dtArrPromo.Columns.Add("FNEPISODE");


                if (dtArrPromo.Rows.Count > 0)
                { //有資料則不用去查詢預排宣傳帶
                }
                else
                {  //查詢預排資料,以便將資料放入表中
                    da_CHANNEL_PROMO_LINK.Fill(dt_CHANNEL_PROMO_LINK);
                    Set_DataTable_Channel_Promo_Link(dt_CHANNEL_PROMO_LINK);
                }

                if (dtQueue.Rows.Count > 0)
                {

                    if (dtArrPromo.Rows.Count > 0) //曾經插入過宣傳帶資料,就不用再去做預排動作
                    {
                        #region --曾經插入過宣傳帶資料,就不用再去做預排動作
                        for (int i = 0; i <= dtQueue.Rows.Count - 1; i++) //先取得段落
                        {
                            InsertNullProgSeg(dtQueue.Rows[i]["FSPROG_NAME"].ToString());

                            //每一個節目,先查詢節目段落看是否有資料
                            DataTable dtProgSeg = new DataTable();
                            int ProgLength;
                            ProgLength = CalProgLength(dtQueue.Rows[i]["FSBEG_TIME"].ToString(), dtQueue.Rows[i]["FSEND_TIME"].ToString());
                            //取得段數資料,dtProgSeg 如果有查到段落則使用段落m不然建立一個預設段落

                            string TProgID = dtQueue.Rows[i]["FSPROG_ID"].ToString();
                            string TProgName = dtQueue.Rows[i]["FSPROG_NAME"].ToString();
                            string TSEQNO = dtQueue.Rows[i]["FNSEQNO"].ToString();
                            string TBREAKNO = "0";//dtProgSeg.Rows[j]["FNSEG_ID"].ToString();
                            string TEPISODE = dtQueue.Rows[i]["FNEPISODE"].ToString();
                            string TSEG = dtQueue.Rows[i]["FNDEF_SEG"].ToString(); 
                            //取得節目段落
                            dtProgSeg = getProgSegment(dtQueue.Rows[i]["FSPROG_ID"].ToString(), dtQueue.Rows[i]["FNEPISODE"].ToString(), dtQueue.Rows[i]["FNSEQNO"].ToString(), dtQueue.Rows[i]["FSCHANNEL_ID"].ToString(), ProgLength.ToString(), dtQueue.Rows[i]["FNDEF_MIN"].ToString(), dtQueue.Rows[i]["FNDEF_SEC"].ToString(), dtQueue.Rows[i]["FNDEF_SEG"].ToString());
                            //將自節目集數段落資料或預設節目段落節目跟播出運行表的資料做比對,如果有運行表資料的話,將他有修改過的Memo,Memo1的資料抄寫過來

                            for (int j = 0; j <= dtProgSeg.Rows.Count - 1; j++)
                            {
                                if (int.Parse(dtProgSeg.Rows[j]["FNSEG_ID"].ToString()) == 1) //如果是第1段則先插入第0段的宣傳帶
                                {
                                    getArrPromo(TProgID, TProgName, TSEQNO, TBREAKNO,TEPISODE, dtArrPromo);
                                } //第一段節目
                                //#插入節目
                                ArrPromoList TempArrPromoList = new ArrPromoList();

                                TempArrPromoList.COMBINENO = "";
                                if (int.Parse(dtProgSeg.Rows[j]["FNSEG_ID"].ToString()) == 1)
                                    TempArrPromoList.FCTIME_TYPE = "O";
                                else
                                    TempArrPromoList.FCTIME_TYPE = "A";
                                TempArrPromoList.FSPROG_ID = dtQueue.Rows[i]["FSPROG_ID"].ToString();
                                TempArrPromoList.FNSEQNO = dtQueue.Rows[i]["FNSEQNO"].ToString();
                                TempArrPromoList.FSNAME = dtQueue.Rows[i]["FSPROG_NAME"].ToString();
                                TempArrPromoList.FSCHANNEL_ID = dtQueue.Rows[i]["FSCHANNEL_ID"].ToString();
                                TempArrPromoList.FSCHANNEL_NAME = dtQueue.Rows[i]["FSCHANNEL_NAME"].ToString();
                                int IDurationFrame = MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtProgSeg.Rows[j]["FSEND_TIMECODE"].ToString()) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtProgSeg.Rows[j]["FSBEG_TIMECODE"].ToString());

                                if (int.Parse(dtProgSeg.Rows[j]["FNSEG_ID"].ToString()) == 1)
                                {
                                    NowPlaytime = dtQueue.Rows[i]["FSBEG_TIME"].ToString().Substring(0, 2) + ":" + dtQueue.Rows[i]["FSBEG_TIME"].ToString().Substring(2, 2) + ":00;00";
                                }
                                else
                                {
                                    NowPlaytime = NextPlaytime;
                                }
                                NextPlaytime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(NowPlaytime) + IDurationFrame);

                                TempArrPromoList.FSPLAY_TIME = NowPlaytime;
                                //NextPlaytime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(NowPlaytime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtProgSeg.Rows[j - 1]["FSEND_TIMECODE"].ToString()) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtProgSeg.Rows[j - 1]["FSBEG_TIMECODE"].ToString()));

                                TempArrPromoList.FSDURATION = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(IDurationFrame);
                                TempArrPromoList.FNEPISODE = dtQueue.Rows[i]["FNEPISODE"].ToString();
                                TempArrPromoList.FNBREAK_NO = int.Parse(dtProgSeg.Rows[j]["FNSEG_ID"].ToString()).ToString();
                                TempArrPromoList.FNSUB_NO = "0";
                                TempArrPromoList.FSFILE_NO = dtProgSeg.Rows[j]["FSFILE_NO"].ToString();

                                TempArrPromoList.FSVIDEO_ID = dtProgSeg.Rows[j]["FSVIDEO_ID"].ToString();

                                TempArrPromoList.FSPROMO_ID = "";
                                TempArrPromoList.FDDATE = dtQueue.Rows[i]["FDDATE"].ToString();
                                TempArrPromoList.FNLIVE = dtQueue.Rows[i]["FNLIVE"].ToString();
                                TempArrPromoList.FCTYPE = "G";
                                TempArrPromoList.FSSTATUS = "";
                                TempArrPromoList.FSSIGNAL = dtQueue.Rows[i]["FSSIGNAL"].ToString();
                                TempArrPromoList.FSMEMO = dtQueue.Rows[i]["FSMEMO"].ToString();
                                TempArrPromoList.FSMEMO1 = dtQueue.Rows[i]["FSMEMO1"].ToString();
                                TempArrPromoList.FSPROG_NAME = dtQueue.Rows[i]["FSPROG_NAME"].ToString();

                                for (int k = 0; k <= dtCombineQueue.Rows.Count - 1; k++) //比對舊架構的資料將FCTIMETYPE,FSSIGNAL,FSMEMO,FSMEMO1抄寫過來
                                {
                                    if (dtQueue.Rows[i]["FSPROG_ID"].ToString() == dtCombineQueue.Rows[k]["FSPROG_ID"].ToString() &&
                                        dtQueue.Rows[i]["FNSEQNO"].ToString() == dtCombineQueue.Rows[k]["FNSEQNO"].ToString() &&
                                         int.Parse(dtProgSeg.Rows[j]["FNSEG_ID"].ToString()) == int.Parse(dtCombineQueue.Rows[k]["FNBREAK_NO"].ToString()))
                                    {
                                        TempArrPromoList.FSPLAY_TIME = dtCombineQueue.Rows[k]["FSPLAY_TIME"].ToString();
                                        TempArrPromoList.FCTIME_TYPE = dtCombineQueue.Rows[k]["FCTIME_TYPE"].ToString();
                                        TempArrPromoList.FSSIGNAL = dtCombineQueue.Rows[k]["FSSIGNAL"].ToString();
                                        TempArrPromoList.FSMEMO = dtCombineQueue.Rows[k]["FSMEMO"].ToString();
                                        TempArrPromoList.FSMEMO1 = dtCombineQueue.Rows[k]["FSMEMO1"].ToString();

                                    }
                                }
                                DatePlayList.Add(TempArrPromoList);

                                getArrPromo(TProgID, TProgName, TSEQNO, int.Parse(dtProgSeg.Rows[j]["FNSEG_ID"].ToString()).ToString(),TEPISODE, dtArrPromo);

                            }
                        }
                        //加入最後一行空白列,以方便插入
                        InsertNullProgSeg("");
                        #endregion
                    }
                    else  //(dtArrPromo.Rows.Count >0) //當有有插入過宣傳帶時要取ArrPromo的宣傳帶
                    { //當沒有插入過宣傳帶資料時,要取預排宣傳帶
                        #region  --當沒有插入過宣傳帶資料時,要取預排宣傳帶
                        for (int i = 0; i <= dtQueue.Rows.Count - 1; i++)
                        {
                            InsertNullProgSeg(dtQueue.Rows[i]["FSPROG_NAME"].ToString());
                            SqlDataAdapter da_PROG_PROMO_LINK = new SqlDataAdapter("SP_Q_TBPGM_PROG_PROMO_LINK_ALL", connection);
                            DataTable dt_PROG_PROMO_LINK = new DataTable();
                            da_PROG_PROMO_LINK.SelectCommand.CommandType = CommandType.StoredProcedure;
                            SqlParameter para_PROG_PROMO_LINK1 = new SqlParameter("@FSPROG_ID", dtQueue.Rows[i]["FSPROG_ID"].ToString());
                            da_PROG_PROMO_LINK.SelectCommand.Parameters.Add(para_PROG_PROMO_LINK1);
                            SqlParameter para_PROG_PROMO_LINK2 = new SqlParameter("@FNSEQNO", dtQueue.Rows[i]["FNSEQNO"].ToString());
                            da_PROG_PROMO_LINK.SelectCommand.Parameters.Add(para_PROG_PROMO_LINK2);
                            SqlParameter para_PROG_PROMO_LINK3 = new SqlParameter("@FSCHANNEL_ID", ParaChannel);
                            da_PROG_PROMO_LINK.SelectCommand.Parameters.Add(para_PROG_PROMO_LINK3);
                            da_PROG_PROMO_LINK.Fill(dt_PROG_PROMO_LINK);

                            //每一個節目,先查詢節目段落看是否有資料
                            DataTable dtProgSeg = new DataTable();
                            int ProgLength;
                            ProgLength = CalProgLength(dtQueue.Rows[i]["FSBEG_TIME"].ToString(), dtQueue.Rows[i]["FSEND_TIME"].ToString());
                            //取得段數資料,dtProgSeg 如果有查到段落則使用段落,不然建立一個預設段落

                            string TProgID = dtQueue.Rows[i]["FSPROG_ID"].ToString();
                            string TProgName = dtQueue.Rows[i]["FSPROG_NAME"].ToString();
                            string TSEQNO = dtQueue.Rows[i]["FNSEQNO"].ToString();
                            string TBREAKNO = "0";//dtProgSeg.Rows[j]["FNSEG_ID"].ToString();
                            string TEPISODE = dtQueue.Rows[i]["FNEPISODE"].ToString();
                            bool TTimeONE;
                            bool TTimeTWO;
                            bool TTimeTHREE;
                            TTimeONE = CheckISTimeONE(dtQueue.Rows[i]["FSBEG_TIME"].ToString());
                            TTimeTWO = CheckISTimeTWO(dtQueue.Rows[i]["FSBEG_TIME"].ToString());
                            TTimeTHREE = CheckISTimeTHREE(dtQueue.Rows[i]["FSBEG_TIME"].ToString());
                            if (i < dtQueue.Rows.Count - 1) //如果不是最後一筆則要先比對下一筆節目的起始時間,跟這一筆在8分鐘內,則要看下一筆節目的起始時間是否也算是在整點
                            {
                                if (CALProgTime(dtQueue.Rows[i]["FSBEG_TIME"].ToString(), dtQueue.Rows[i + 1]["FSBEG_TIME"].ToString()) < 8)
                                {
                                    if (TTimeONE == true && CheckISTimeONE(dtQueue.Rows[i + 1]["FSBEG_TIME"].ToString()) == true)
                                        TTimeONE = false;
                                    if (TTimeTWO == true && CheckISTimeTWO(dtQueue.Rows[i + 1]["FSBEG_TIME"].ToString()) == true)
                                        TTimeTWO = false;
                                    if (TTimeTHREE == true && CheckISTimeTHREE(dtQueue.Rows[i + 1]["FSBEG_TIME"].ToString()) == true)
                                        TTimeTHREE = false;
                                }
                            }

                            dtProgSeg = getProgSegment(dtQueue.Rows[i]["FSPROG_ID"].ToString(), dtQueue.Rows[i]["FNEPISODE"].ToString(), dtQueue.Rows[i]["FNSEQNO"].ToString(), dtQueue.Rows[i]["FSCHANNEL_ID"].ToString(), ProgLength.ToString(), dtQueue.Rows[i]["FNDEF_MIN"].ToString(), dtQueue.Rows[i]["FNDEF_SEC"].ToString(), dtQueue.Rows[i]["FNDEF_SEG"].ToString());
                            
                            for (int j = 0; j <= dtProgSeg.Rows.Count - 1; j++)
                            {
                                if (int.Parse(dtProgSeg.Rows[j]["FNSEG_ID"].ToString()) == 1) //如果是第1段則先插入第0段的宣傳帶
                                {
                                    bool TLastSeg;
                                    if (j == dtProgSeg.Rows.Count - 1)
                                        TLastSeg = true;
                                    else
                                        TLastSeg = false;

                                    getProgSegmentPromo(TProgID, TProgName, TSEQNO, TBREAKNO, dt_PROG_PROMO_LINK, TLastSeg, TTimeONE, TTimeTWO, TTimeTHREE);
                                } //第一段節目
                                //#插入節目
                                ArrPromoList TempArrPromoList = new ArrPromoList();

                                TempArrPromoList.COMBINENO = "";
                                if (int.Parse(dtProgSeg.Rows[j]["FNSEG_ID"].ToString()) == 1)
                                    TempArrPromoList.FCTIME_TYPE = "O";
                                else
                                    TempArrPromoList.FCTIME_TYPE = "A";
                                TempArrPromoList.FSPROG_ID = dtQueue.Rows[i]["FSPROG_ID"].ToString();
                                TempArrPromoList.FNSEQNO = dtQueue.Rows[i]["FNSEQNO"].ToString();
                                TempArrPromoList.FSNAME = dtQueue.Rows[i]["FSPROG_NAME"].ToString();
                                TempArrPromoList.FSCHANNEL_ID = dtQueue.Rows[i]["FSCHANNEL_ID"].ToString();
                                TempArrPromoList.FSCHANNEL_NAME = dtQueue.Rows[i]["FSCHANNEL_NAME"].ToString();
                                int IDurationFrame = MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtProgSeg.Rows[j]["FSEND_TIMECODE"].ToString()) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtProgSeg.Rows[j]["FSBEG_TIMECODE"].ToString());

                                if (int.Parse(dtProgSeg.Rows[j]["FNSEG_ID"].ToString()) == 1)
                                {
                                    NowPlaytime = dtQueue.Rows[i]["FSBEG_TIME"].ToString().Substring(0, 2) + ":" + dtQueue.Rows[i]["FSBEG_TIME"].ToString().Substring(2, 2) + ":00;00";
                                }
                                else
                                {
                                    NowPlaytime = NextPlaytime;
                                }
                                NextPlaytime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(NowPlaytime) + IDurationFrame);

                                TempArrPromoList.FSPLAY_TIME = NowPlaytime;
                                //NextPlaytime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(NowPlaytime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtProgSeg.Rows[j - 1]["FSEND_TIMECODE"].ToString()) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtProgSeg.Rows[j - 1]["FSBEG_TIMECODE"].ToString()));

                                TempArrPromoList.FSDURATION = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(IDurationFrame);
                                TempArrPromoList.FNEPISODE = dtQueue.Rows[i]["FNEPISODE"].ToString();
                                TempArrPromoList.FNBREAK_NO = int.Parse(dtProgSeg.Rows[j]["FNSEG_ID"].ToString()).ToString();
                                TempArrPromoList.FNSUB_NO = "0";
                                TempArrPromoList.FSFILE_NO = dtProgSeg.Rows[j]["FSFILE_NO"].ToString();
                                TempArrPromoList.FSVIDEO_ID = dtProgSeg.Rows[j]["FSVIDEO_ID"].ToString();
                                TempArrPromoList.FSPROMO_ID = "";
                                TempArrPromoList.FDDATE = dtQueue.Rows[i]["FDDATE"].ToString();
                                TempArrPromoList.FNLIVE = dtQueue.Rows[i]["FNLIVE"].ToString();
                                TempArrPromoList.FCTYPE = "G";
                                TempArrPromoList.FSSTATUS = "";
                                TempArrPromoList.FSSIGNAL = dtQueue.Rows[i]["FSSIGNAL"].ToString();
                                TempArrPromoList.FSMEMO = dtQueue.Rows[i]["FSMEMO"].ToString();
                                TempArrPromoList.FSMEMO1 = dtQueue.Rows[i]["FSMEMO1"].ToString();
                                //TempArrPromoList.FSPROG_NAME = ReplaceXML(dtQueue.Rows[i]["FSPROG_NAME"].ToString());
                                TempArrPromoList.FSPROG_NAME = dtQueue.Rows[i]["FSPROG_NAME"].ToString();

                                for (int k = 0; k <= dtCombineQueue.Rows.Count - 1; k++) //比對舊架構的資料將FCTIMETYPE,FSSIGNAL,FSMEMO,FSMEMO1抄寫過來
                                {
                                    if (dtQueue.Rows[i]["FSPROG_ID"].ToString() == dtCombineQueue.Rows[k]["FSPROG_ID"].ToString() &&
                                        dtQueue.Rows[i]["FNSEQNO"].ToString() == dtCombineQueue.Rows[k]["FNSEQNO"].ToString() &&
                                         int.Parse(dtProgSeg.Rows[j]["FNSEG_ID"].ToString()) == int.Parse(dtCombineQueue.Rows[k]["FNBREAK_NO"].ToString()))
                                    {
                                        TempArrPromoList.FSPLAY_TIME = dtCombineQueue.Rows[k]["FSPLAY_TIME"].ToString();
                                        TempArrPromoList.FCTIME_TYPE = dtCombineQueue.Rows[k]["FCTIME_TYPE"].ToString();
                                        TempArrPromoList.FSSIGNAL = dtCombineQueue.Rows[k]["FSSIGNAL"].ToString();
                                        TempArrPromoList.FSMEMO = dtCombineQueue.Rows[k]["FSMEMO"].ToString();
                                        TempArrPromoList.FSMEMO1 = dtCombineQueue.Rows[k]["FSMEMO1"].ToString();

                                    }
                                }

                                DatePlayList.Add(TempArrPromoList);

                                bool TLastSeg1;
                                if (j == dtProgSeg.Rows.Count - 1)
                                    TLastSeg1 = true;
                                else
                                    TLastSeg1 = false;

                                getProgSegmentPromo(TProgID, TProgName, TSEQNO, int.Parse(dtProgSeg.Rows[j]["FNSEG_ID"].ToString()).ToString(), dt_PROG_PROMO_LINK, TLastSeg1, TTimeONE, TTimeTWO, TTimeTHREE);

                            }

                        }

                        //加入最後一行空白列,以方便插入
                        InsertNullProgSeg("");
                        #endregion
                    }
                    
                    //將DatePlayList的資料寫成XML
                    //DatePlayList
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("<Datas>");

                    Combineno = 1;
                    NowPlaytime = dtQueue.Rows[0]["FSBEG_TIME"].ToString().Substring(0, 2) + ":" + dtQueue.Rows[0]["FSBEG_TIME"].ToString().Substring(2, 2) + ":00;00";
                    NextPlaytime = dtQueue.Rows[0]["FSBEG_TIME"].ToString().Substring(0, 2) + ":" + dtQueue.Rows[0]["FSBEG_TIME"].ToString().Substring(2, 2) + ":00;00";
                    foreach (ArrPromoList TempPromoList in DatePlayList)
                    {
                        sb.AppendLine("<Data>");
                        sb.AppendLine("<COMBINENO>" + Combineno.ToString() + " </COMBINENO>");
                        Combineno = Combineno + 1;
                        if (TempPromoList.FCTIME_TYPE == "" || TempPromoList.FCTIME_TYPE == null)
                            sb.AppendLine("<FCTIME_TYPE>" + "A" + "</FCTIME_TYPE>");
                        else
                            sb.AppendLine("<FCTIME_TYPE>" + TempPromoList.FCTIME_TYPE + "</FCTIME_TYPE>");

                        sb.AppendLine("<FSPROG_ID>" + TempPromoList.FSPROG_ID + "</FSPROG_ID>");
                        sb.AppendLine("<FNSEQNO>" + TempPromoList.FNSEQNO + "</FNSEQNO>");
                        sb.AppendLine("<FSNAME>" + ReplaceXML(TempPromoList.FSNAME) + "</FSNAME>");
                        sb.AppendLine("<FSCHANNEL_ID>" + dtQueue.Rows[0]["FSCHANNEL_ID"].ToString() + "</FSCHANNEL_ID>");
                        sb.AppendLine("<FSCHANNEL_NAME>" + dtQueue.Rows[0]["FSCHANNEL_NAME"].ToString() + "</FSCHANNEL_NAME>");
                        if (TempPromoList.FSPROG_ID != "")
                        {
                            if (TempPromoList.FCTIME_TYPE == "O")
                            {
                                sb.AppendLine("<FSPLAY_TIME>" + TempPromoList.FSPLAY_TIME + "</FSPLAY_TIME>");
                                NowPlaytime = TempPromoList.FSPLAY_TIME;
                                NextPlaytime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TempPromoList.FSPLAY_TIME) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TempPromoList.FSDURATION));
                            }
                            else
                            {
                                sb.AppendLine("<FSPLAY_TIME>" + NextPlaytime + "</FSPLAY_TIME>");
                                NextPlaytime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(NextPlaytime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TempPromoList.FSDURATION));
                            }
                        }
                        else
                        {
                            sb.AppendLine("<FSPLAY_TIME>" + "" + "</FSPLAY_TIME>");
                        }

                        sb.AppendLine("<FSDURATION>" + TempPromoList.FSDURATION + "</FSDURATION>");
                        sb.AppendLine("<FNEPISODE>" + TempPromoList.FNEPISODE + "</FNEPISODE>");
                        sb.AppendLine("<FNBREAK_NO>" + TempPromoList.FNBREAK_NO + "</FNBREAK_NO>");
                        sb.AppendLine("<FNSUB_NO>" + TempPromoList.FNSUB_NO + "</FNSUB_NO>");
                        sb.AppendLine("<FSFILE_NO>" + TempPromoList.FSFILE_NO + "</FSFILE_NO>");
                        sb.AppendLine("<FSVIDEO_ID>" + TempPromoList.FSVIDEO_ID + "</FSVIDEO_ID>");
                        sb.AppendLine("<FSPROMO_ID>" + TempPromoList.FSPROMO_ID + "</FSPROMO_ID>");
                        sb.AppendLine("<FDDATE>" + dtQueue.Rows[0]["FDDATE"].ToString() + "</FDDATE>");
                        sb.AppendLine("<FNLIVE>" + TempPromoList.FNLIVE + "</FNLIVE>");
                        sb.AppendLine("<FCTYPE>" + TempPromoList.FCTYPE + "</FCTYPE>");
                        sb.AppendLine("<FSSTATUS>" + TempPromoList.FSSTATUS + "</FSSTATUS>");
                        sb.AppendLine("<FSSIGNAL>" + TempPromoList.FSSIGNAL + "</FSSIGNAL>");
                        sb.AppendLine("<FSMEMO>" + ReplaceXML(TempPromoList.FSMEMO) + "</FSMEMO>");
                        sb.AppendLine("<FSMEMO1>" + ReplaceXML(TempPromoList.FSMEMO1) + "</FSMEMO1>");
                        sb.AppendLine("<FSPROG_NAME>" + ReplaceXML(TempPromoList.FSPROG_NAME) + "</FSPROG_NAME>");
                        sb.AppendLine("</Data>");

                    }
                    sb.AppendLine("</Datas>");

                    #region 先查詢TBPGM_ARR_PROMO_LOST最大編號 MAX_ARR_PROMO_LOST_ID
                    SqlDataAdapter da_LOST_MAX_ID = new SqlDataAdapter("SP_Q_TBPGM_ARR_PROMO_LOST_MAX_ID", connection);
                    DataTable dt_LOST_MAX_ID = new DataTable();

                    //SqlParameter para_CHANNEL_PROMO_LINK = new SqlParameter("@FSCHANNEL_ID", dtQueue.Rows[1]["FSCHANNEL_ID"].ToString());
                    SqlParameter para_LOST_MAX_ID = new SqlParameter("@FSCHANNEL_ID", ParaChannel);
                    da_LOST_MAX_ID.SelectCommand.Parameters.Add(para_LOST_MAX_ID);
                    SqlParameter para_LOST_MAX_ID1 = new SqlParameter("@FDDATE", ParaDate);
                    da_LOST_MAX_ID.SelectCommand.Parameters.Add(para_LOST_MAX_ID1);

                    da_LOST_MAX_ID.SelectCommand.CommandType = CommandType.StoredProcedure;

                    if (connection.State == System.Data.ConnectionState.Closed)
                    {
                        connection.Open();
                    }


                    //先查詢QUEUE&Arrpromo的資料
                    da_LOST_MAX_ID.Fill(dt_LOST_MAX_ID);
                    int MAX_ARR_PROMO_LOST_ID = 0;
                    for (int L = 0; L <= dt_LOST_MAX_ID.Rows.Count - 1; L++) //先取得段落
                    {
                        if (dt_LOST_MAX_ID.Rows[L][0].ToString() == "")
                            MAX_ARR_PROMO_LOST_ID = 0;
                        else
                            MAX_ARR_PROMO_LOST_ID = int.Parse(dt_LOST_MAX_ID.Rows[L][0].ToString());
                    }
                    MAX_ARR_PROMO_LOST_ID = MAX_ARR_PROMO_LOST_ID + 1;
                    //connection.Close();
                    #endregion

                    #region 將沒有對應到的Promo寫入TBPGM_ARR_PROMO_LOST中
                    int hasInsertNum = 0;
                    int InsErrNum = 0;
                    SqlTransaction tran = null;
                    
                    SqlCommand sqlIns_Arr_Promo_Lost = new SqlCommand("SP_I_TBPGM_ARR_PROMO_LOST", connection);
                    
                    tran = connection.BeginTransaction();
                    sqlIns_Arr_Promo_Lost.Transaction = tran;
                    try
                    {
                        if (connection.State == System.Data.ConnectionState.Closed)
                        {
                            connection.Open();
                        }

                        for (int i = 0; i <= dtArrPromo.Rows.Count - 1; i++) //先取得段落
                        {

                            if (dtArrPromo.Rows[i]["hasInsert"].ToString() != "Y")
                            {
                                sqlIns_Arr_Promo_Lost.Parameters.Clear();
                                SqlParameter para_LOST_FNCREATE_LIST_NO = new SqlParameter("@FNCREATE_LIST_NO", MAX_ARR_PROMO_LOST_ID);
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FNCREATE_LIST_NO);

                                SqlParameter para_LOST_FNARR_PROMO_NO = new SqlParameter("@FNARR_PROMO_NO", dtArrPromo.Rows[i]["FNARR_PROMO_NO"].ToString());
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FNARR_PROMO_NO);

                                SqlParameter para_LOST_FSPROG_ID = new SqlParameter("@FSPROG_ID", dtArrPromo.Rows[i]["FSPROG_ID"].ToString());
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FSPROG_ID);

                                SqlParameter para_LOST_FNSEQNO = new SqlParameter("@FNSEQNO", dtArrPromo.Rows[i]["FNSEQNO"].ToString());
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FNSEQNO);

                                //string[] DateArr = dtArrPromo.Rows[i]["FDDATE"].ToString().Split(' ');
                                SqlParameter para_LOST_FDDATE = new SqlParameter("@FDDATE", dtArrPromo.Rows[i]["FDDATE"].ToString().Split(' ')[0]);
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FDDATE);

                                SqlParameter para_LOST_FSCHANNEL_ID = new SqlParameter("@FSCHANNEL_ID", dtArrPromo.Rows[i]["FSCHANNEL_ID"].ToString());
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FSCHANNEL_ID);

                                SqlParameter para_LOST_FNBREAK_NO = new SqlParameter("@FNBREAK_NO", dtArrPromo.Rows[i]["FNBREAK_NO"].ToString());
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FNBREAK_NO);
                                SqlParameter para_LOST_FNSUB_NO = new SqlParameter("@FNSUB_NO", dtArrPromo.Rows[i]["FNSUB_NO"].ToString());
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FNSUB_NO);
                                SqlParameter para_LOST_FSPROMO_ID = new SqlParameter("@FSPROMO_ID", dtArrPromo.Rows[i]["FSPROMO_ID"].ToString());
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FSPROMO_ID);
                                SqlParameter para_LOST_FSPLAYTIME = new SqlParameter("@FSPLAYTIME", dtArrPromo.Rows[i]["FSPLAYTIME"].ToString());
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FSPLAYTIME);
                                SqlParameter para_LOST_FSDUR = new SqlParameter("@FSDUR", dtArrPromo.Rows[i]["FSDUR"].ToString());
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FSDUR);
                                SqlParameter para_LOST_FSSIGNAL = new SqlParameter("@FSSIGNAL", dtArrPromo.Rows[i]["FSSIGNAL"].ToString());
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FSSIGNAL);
                                SqlParameter para_LOST_FSMEMO = new SqlParameter("@FSMEMO", dtArrPromo.Rows[i]["FSMEMO"].ToString());
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FSMEMO);
                                SqlParameter para_LOST_FSMEMO1 = new SqlParameter("@FSMEMO1", dtArrPromo.Rows[i]["FSMEMO1"].ToString());
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FSMEMO1);
                                SqlParameter para_LOST_FSCREATED_BY = new SqlParameter("@FSCREATED_BY", dtArrPromo.Rows[i]["FSCREATED_BY"].ToString());
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FSCREATED_BY);
                                SqlParameter para_LOST_FDCREATED_DATE = new SqlParameter("@FDCREATED_DATE", dtArrPromo.Rows[i]["FDCREATED_DATE"].ToString().Split(' ')[0]);
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FDCREATED_DATE);
                                SqlParameter para_LOST_FSSTATUS = new SqlParameter("@FSSTATUS", "");
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FSSTATUS);
                                SqlParameter para_LOST_FNEPISODE = new SqlParameter("@FNEPISODE", dtArrPromo.Rows[i]["FNEPISODE"].ToString());
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FNEPISODE); 
                                
                                sqlIns_Arr_Promo_Lost.CommandType = CommandType.StoredProcedure;


                                if (sqlIns_Arr_Promo_Lost.ExecuteNonQuery() == 0)
                                {
                                    InsErrNum = InsErrNum + 1;
                                }
                                else
                                {
                                    //不須知道異動筆數

                                    //tran.Rollback();

                                }

                            }

                            hasInsertNum = hasInsertNum + 1;
                        }

                        if (InsErrNum > 0)
                        {
                            tran.Rollback();
                            return "<Errors>" + "寫入遺失清單失敗" + "</Errors>";
                        }
                        else
                        {
                            tran.Commit();
                        }

                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        return "<Errors>" + "寫入遺失清單失敗" + "</Errors>";
                    }
                    finally
                    {

                        connection.Close();
                        connection.Dispose();
                        sqlIns_Arr_Promo_Lost.Dispose();
                        GC.Collect();

                    }


                    #endregion



                    return sb.ToString();
                }
                else //(dtQueue.Rows.Count < 0) 沒有節目表資料
                {
                    return "<Errors>" + "沒有節目表資料" + "</Errors>";
                }

            }
            catch (Exception ex)
            {
                StringBuilder sbError = new StringBuilder();
                sbError.AppendLine("<Errors>" + ex.Message + "</Errors>");
                return sbError.ToString();
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }
        }


    //產生節目播出運行表,由TBPGM_QUEUE產生的節目表,一一先查詢段落資料再查詢
        [WebMethod()]
        public string Do_Query_Prog_Queue_Seg_MIX(string strSQL, string parameters = null)
        {
            //strSQL為查詢Store Procesure Name
            //parameters為XML內容(DB_Function_Sample.txt)

            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            //StringBuilder sb = new StringBuilder();

            SqlDataAdapter daQueue = new SqlDataAdapter("SP_Q_TBPGM_QUEUE", connection);
            DataTable dtQueue = new DataTable();
            
            SqlDataAdapter daCombineQueue = new SqlDataAdapter("SP_Q_TBPGM_COMBINE_QUEUE", connection);
            DataTable dtCombineQueue = new DataTable();

            //可能查出多筆資料,高低解同時存在,要盡量挑選同一個類型的
            SqlDataAdapter daArrPromo = new SqlDataAdapter("SP_Q_TBPGM_ARR_PROMO_NEW_VIDEOID_MIX", connection);
            DataTable dtArrPromo = new DataTable();

            string ParaChannel = "";
            string ParaDate = "";
            //參數剖析
            #region 剖析參數取得日期及頻道參數
            if (parameters != "")
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(parameters);
                XmlNode xmlNode = xmldoc.FirstChild;
                for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                {
                    if (xmlNode.ChildNodes[i].FirstChild == null || xmlNode.ChildNodes[i].FirstChild.Value == "")
                    {
                        SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, "");
                        SqlParameter para1 = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, "");
                        SqlParameter para2 = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, "");
                        daQueue.SelectCommand.Parameters.Add(para);
                        daArrPromo.SelectCommand.Parameters.Add(para1);
                        daCombineQueue.SelectCommand.Parameters.Add(para2);
                    }
                    else
                    {
                        SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, xmlNode.ChildNodes[i].FirstChild.Value);
                        SqlParameter para1 = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, xmlNode.ChildNodes[i].FirstChild.Value);
                        SqlParameter para2 = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, xmlNode.ChildNodes[i].FirstChild.Value);
                        daQueue.SelectCommand.Parameters.Add(para);
                        daArrPromo.SelectCommand.Parameters.Add(para1);
                        daCombineQueue.SelectCommand.Parameters.Add(para2);
                        if (xmlNode.ChildNodes[i].Name == "FSCHANNEL_ID")
                        {
                            ParaChannel = xmlNode.ChildNodes[i].FirstChild.Value;
                            _WorkChannelID = xmlNode.ChildNodes[i].FirstChild.Value;
                        }
                        if (xmlNode.ChildNodes[i].Name == "FDDATE")
                            ParaDate = xmlNode.ChildNodes[i].FirstChild.Value;

                    }
                }

            }
            #endregion
            #region 取得頻道預排宣傳帶
            SqlDataAdapter da_CHANNEL_PROMO_LINK = new SqlDataAdapter("SP_Q_TBPGM_CHANNEL_PROMO_LINK", connection);
            DataTable dt_CHANNEL_PROMO_LINK = new DataTable();

            //SqlParameter para_CHANNEL_PROMO_LINK = new SqlParameter("@FSCHANNEL_ID", dtQueue.Rows[1]["FSCHANNEL_ID"].ToString());
            SqlParameter para_CHANNEL_PROMO_LINK = new SqlParameter("@FSCHANNEL_ID", ParaChannel);
            da_CHANNEL_PROMO_LINK.SelectCommand.Parameters.Add(para_CHANNEL_PROMO_LINK);
            SqlParameter para_CHANNEL_PROMO_LINK1 = new SqlParameter("@FCLINK_TYPE", "");
            da_CHANNEL_PROMO_LINK.SelectCommand.Parameters.Add(para_CHANNEL_PROMO_LINK1);
            SqlParameter para_CHANNEL_PROMO_LINK2 = new SqlParameter("@FCTIME_TYPE", "");
            da_CHANNEL_PROMO_LINK.SelectCommand.Parameters.Add(para_CHANNEL_PROMO_LINK2);
            SqlParameter para_CHANNEL_PROMO_LINK3 = new SqlParameter("@FCINSERT_TYPE", "");
            da_CHANNEL_PROMO_LINK.SelectCommand.Parameters.Add(para_CHANNEL_PROMO_LINK3);

            #endregion

            int Combineno = 1;
            string NowPlaytime = "";
            string NextPlaytime = "";
            daQueue.SelectCommand.CommandType = CommandType.StoredProcedure;
            daArrPromo.SelectCommand.CommandType = CommandType.StoredProcedure;
            daCombineQueue.SelectCommand.CommandType = CommandType.StoredProcedure;
            da_CHANNEL_PROMO_LINK.SelectCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                //先查詢QUEUE&Arrpromo的資料
                daQueue.Fill(dtQueue);

                daArrPromo.Fill(dtArrPromo);
                daCombineQueue.Fill(dtCombineQueue);
                dtArrPromo.Columns.Add("hasInsert");
                dtArrPromo.Columns.Add("FNEPISODE");


                if (dtArrPromo.Rows.Count > 0)
                { //有資料則不用去查詢預排宣傳帶
                }
                else
                {  //查詢預排資料,以便將資料放入表中
                    da_CHANNEL_PROMO_LINK.Fill(dt_CHANNEL_PROMO_LINK);
                    Set_DataTable_Channel_Promo_Link(dt_CHANNEL_PROMO_LINK);
                }

                if (dtQueue.Rows.Count > 0)
                {

                    if (dtArrPromo.Rows.Count > 0) //曾經插入過宣傳帶資料,就不用再去做預排動作
                    {
                        #region 先將Arr_Promo的資料去除重複HD SD取其一
                        for (int i = dtArrPromo.Rows.Count - 1; i >= 0; i--)
                        {
                            if (i >= 1)
                            {
                                if (dtArrPromo.Rows[i]["FNARR_PROMO_NO"].ToString() == dtArrPromo.Rows[i - 1]["FNARR_PROMO_NO"].ToString())
                                {
                                    if (dtArrPromo.Rows[i]["FSARC_TYPE"].ToString() == "001")
                                    {
                                        if (dtArrPromo.Rows[i - 1]["FSFILE_NO"].ToString() != "")
                                        { //002 的檔案有編號則採用002,刪除001
                                            dtArrPromo.Rows.RemoveAt(i);
                                        }
                                        else if (dtArrPromo.Rows[i]["FSFILE_NO"].ToString() != "")
                                        { //002為空但001不為空則刪除002
                                            dtArrPromo.Rows.RemoveAt(i - 1);
                                        }
                                        else
                                        {  //001 002 皆為空
                                            dtArrPromo.Rows.RemoveAt(i);
                                        }
                                    }
                                    else
                                    {

                                        if (dtArrPromo.Rows[i]["FSFILE_NO"].ToString() != "")
                                        { //002 的檔案有編號則採用002,刪除001
                                            dtArrPromo.Rows.RemoveAt(i - 1);
                                        }
                                        else if (dtArrPromo.Rows[i - 1]["FSFILE_NO"].ToString() != "")
                                        { //002為空但001不為空則刪除002
                                            dtArrPromo.Rows.RemoveAt(i);
                                        }
                                        else
                                        {  //001 002 皆為空
                                            dtArrPromo.Rows.RemoveAt(i - 1);
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                        #region --曾經插入過宣傳帶資料,就不用再去做預排動作
                        for (int i = 0; i <= dtQueue.Rows.Count - 1; i++) //先取得段落
                        {
                            InsertNullProgSeg(dtQueue.Rows[i]["FSPROG_NAME"].ToString());

                            //每一個節目,先查詢節目段落看是否有資料
                            DataTable dtProgSeg = new DataTable();
                            int ProgLength;
                            ProgLength = CalProgLength(dtQueue.Rows[i]["FSBEG_TIME"].ToString(), dtQueue.Rows[i]["FSEND_TIME"].ToString());
                            //取得段數資料,dtProgSeg 如果有查到段落則使用段落m不然建立一個預設段落

                            string TProgID = dtQueue.Rows[i]["FSPROG_ID"].ToString();
                            string TProgName = dtQueue.Rows[i]["FSPROG_NAME"].ToString();
                            string TSEQNO = dtQueue.Rows[i]["FNSEQNO"].ToString();
                            string TBREAKNO = "0";//dtProgSeg.Rows[j]["FNSEG_ID"].ToString();
                            string TEPISODE = dtQueue.Rows[i]["FNEPISODE"].ToString();
                            string TSEG = dtQueue.Rows[i]["FNDEF_SEG"].ToString();

                            //取得節目段落
                            dtProgSeg = getProgSegment_MIX(dtQueue.Rows[i]["FSPROG_ID"].ToString(), dtQueue.Rows[i]["FNEPISODE"].ToString(), dtQueue.Rows[i]["FNSEQNO"].ToString(), dtQueue.Rows[i]["FSCHANNEL_ID"].ToString(), ProgLength.ToString(), dtQueue.Rows[i]["FNDEF_MIN"].ToString(), dtQueue.Rows[i]["FNDEF_SEC"].ToString(), dtQueue.Rows[i]["FNDEF_SEG"].ToString());
                            //將自節目集數段落資料或預設節目段落節目跟播出運行表的資料做比對,如果有運行表資料的話,將他有修改過的Memo,Memo1的資料抄寫過來

                            for (int j = 0; j <= dtProgSeg.Rows.Count - 1; j++)
                            {
                                if (int.Parse(dtProgSeg.Rows[j]["FNSEG_ID"].ToString()) == 1) //如果是第1段則先插入第0段的宣傳帶
                                {
                                    getArrPromo_MIX(TProgID, TProgName, TSEQNO, TBREAKNO, TEPISODE, dtArrPromo);
                                } //第一段節目
                                //#插入節目
                                ArrPromoList TempArrPromoList = new ArrPromoList();

                                TempArrPromoList.COMBINENO = "";
                                if (int.Parse(dtProgSeg.Rows[j]["FNSEG_ID"].ToString()) == 1)
                                    TempArrPromoList.FCTIME_TYPE = "O";
                                else
                                    TempArrPromoList.FCTIME_TYPE = "A";
                                TempArrPromoList.FSPROG_ID = dtQueue.Rows[i]["FSPROG_ID"].ToString();
                                TempArrPromoList.FNSEQNO = dtQueue.Rows[i]["FNSEQNO"].ToString();
                                TempArrPromoList.FSNAME = dtQueue.Rows[i]["FSPROG_NAME"].ToString();
                                TempArrPromoList.FSCHANNEL_ID = dtQueue.Rows[i]["FSCHANNEL_ID"].ToString();
                                TempArrPromoList.FSCHANNEL_NAME = dtQueue.Rows[i]["FSCHANNEL_NAME"].ToString();
                                int IDurationFrame = MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtProgSeg.Rows[j]["FSEND_TIMECODE"].ToString()) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtProgSeg.Rows[j]["FSBEG_TIMECODE"].ToString());

                                if (int.Parse(dtProgSeg.Rows[j]["FNSEG_ID"].ToString()) == 1)
                                {
                                    NowPlaytime = dtQueue.Rows[i]["FSBEG_TIME"].ToString().Substring(0, 2) + ":" + dtQueue.Rows[i]["FSBEG_TIME"].ToString().Substring(2, 2) + ":00;00";
                                }
                                else
                                {
                                    NowPlaytime = NextPlaytime;
                                }
                                NextPlaytime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(NowPlaytime) + IDurationFrame);

                                TempArrPromoList.FSPLAY_TIME = NowPlaytime;
                                //NextPlaytime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(NowPlaytime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtProgSeg.Rows[j - 1]["FSEND_TIMECODE"].ToString()) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtProgSeg.Rows[j - 1]["FSBEG_TIMECODE"].ToString()));

                                TempArrPromoList.FSDURATION = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(IDurationFrame);
                                TempArrPromoList.FNEPISODE = dtQueue.Rows[i]["FNEPISODE"].ToString();
                                TempArrPromoList.FNBREAK_NO = int.Parse(dtProgSeg.Rows[j]["FNSEG_ID"].ToString()).ToString();
                                TempArrPromoList.FNSUB_NO = "0";
                                TempArrPromoList.FSFILE_NO = dtProgSeg.Rows[j]["FSFILE_NO"].ToString();

                                TempArrPromoList.FSVIDEO_ID = dtProgSeg.Rows[j]["FSVIDEO_ID"].ToString();

                                TempArrPromoList.FSPROMO_ID = "";
                                TempArrPromoList.FDDATE = dtQueue.Rows[i]["FDDATE"].ToString();
                                TempArrPromoList.FNLIVE = dtQueue.Rows[i]["FNLIVE"].ToString();
                                TempArrPromoList.FCTYPE = "G";
                                TempArrPromoList.FSSTATUS = "";
                                TempArrPromoList.FSSIGNAL = dtQueue.Rows[i]["FSSIGNAL"].ToString();
                                TempArrPromoList.FSMEMO = dtQueue.Rows[i]["FSMEMO"].ToString();
                                TempArrPromoList.FSMEMO1 = dtQueue.Rows[i]["FSMEMO1"].ToString();
                                TempArrPromoList.FSPROG_NAME = dtQueue.Rows[i]["FSPROG_NAME"].ToString();

                                for (int k = 0; k <= dtCombineQueue.Rows.Count - 1; k++) //比對舊架構的資料將FCTIMETYPE,FSSIGNAL,FSMEMO,FSMEMO1抄寫過來
                                {
                                    if (dtQueue.Rows[i]["FSPROG_ID"].ToString() == dtCombineQueue.Rows[k]["FSPROG_ID"].ToString() &&
                                        dtQueue.Rows[i]["FNSEQNO"].ToString() == dtCombineQueue.Rows[k]["FNSEQNO"].ToString() &&
                                         int.Parse(dtProgSeg.Rows[j]["FNSEG_ID"].ToString()) == int.Parse(dtCombineQueue.Rows[k]["FNBREAK_NO"].ToString()))
                                    {
                                        TempArrPromoList.FSPLAY_TIME = dtCombineQueue.Rows[k]["FSPLAY_TIME"].ToString();
                                        TempArrPromoList.FCTIME_TYPE = dtCombineQueue.Rows[k]["FCTIME_TYPE"].ToString();
                                        TempArrPromoList.FSSIGNAL = dtCombineQueue.Rows[k]["FSSIGNAL"].ToString();
                                        TempArrPromoList.FSMEMO = dtCombineQueue.Rows[k]["FSMEMO"].ToString();
                                        TempArrPromoList.FSMEMO1 = dtCombineQueue.Rows[k]["FSMEMO1"].ToString();

                                    }
                                }
                                DatePlayList.Add(TempArrPromoList);

                                getArrPromo_MIX(TProgID, TProgName, TSEQNO, int.Parse(dtProgSeg.Rows[j]["FNSEG_ID"].ToString()).ToString(), TEPISODE, dtArrPromo);

                            }
                        }
                        //加入最後一行空白列,以方便插入
                        InsertNullProgSeg("");
                        #endregion
                    }
                    else  //(dtArrPromo.Rows.Count >0) //當有有插入過宣傳帶時要取ArrPromo的宣傳帶
                    { //當沒有插入過宣傳帶資料時,要取預排宣傳帶
                        #region  --當沒有插入過宣傳帶資料時,要取預排宣傳帶
                        for (int i = 0; i <= dtQueue.Rows.Count - 1; i++)
                        {
                            InsertNullProgSeg(dtQueue.Rows[i]["FSPROG_NAME"].ToString());
                            SqlDataAdapter da_PROG_PROMO_LINK = new SqlDataAdapter("SP_Q_TBPGM_PROG_PROMO_LINK_ALL", connection);
                            DataTable dt_PROG_PROMO_LINK = new DataTable();
                            da_PROG_PROMO_LINK.SelectCommand.CommandType = CommandType.StoredProcedure;
                            SqlParameter para_PROG_PROMO_LINK1 = new SqlParameter("@FSPROG_ID", dtQueue.Rows[i]["FSPROG_ID"].ToString());
                            da_PROG_PROMO_LINK.SelectCommand.Parameters.Add(para_PROG_PROMO_LINK1);
                            SqlParameter para_PROG_PROMO_LINK2 = new SqlParameter("@FNSEQNO", dtQueue.Rows[i]["FNSEQNO"].ToString());
                            da_PROG_PROMO_LINK.SelectCommand.Parameters.Add(para_PROG_PROMO_LINK2);
                            SqlParameter para_PROG_PROMO_LINK3 = new SqlParameter("@FSCHANNEL_ID", ParaChannel);
                            da_PROG_PROMO_LINK.SelectCommand.Parameters.Add(para_PROG_PROMO_LINK3);
                            da_PROG_PROMO_LINK.Fill(dt_PROG_PROMO_LINK);

                            //每一個節目,先查詢節目段落看是否有資料
                            DataTable dtProgSeg = new DataTable();
                            int ProgLength;
                            ProgLength = CalProgLength(dtQueue.Rows[i]["FSBEG_TIME"].ToString(), dtQueue.Rows[i]["FSEND_TIME"].ToString());
                            //取得段數資料,dtProgSeg 如果有查到段落則使用段落,不然建立一個預設段落

                            string TProgID = dtQueue.Rows[i]["FSPROG_ID"].ToString();
                            string TProgName = dtQueue.Rows[i]["FSPROG_NAME"].ToString();
                            string TSEQNO = dtQueue.Rows[i]["FNSEQNO"].ToString();
                            string TBREAKNO = "0";//dtProgSeg.Rows[j]["FNSEG_ID"].ToString();
                            string TEPISODE = dtQueue.Rows[i]["FNEPISODE"].ToString();
                            bool TTimeONE;
                            bool TTimeTWO;
                            bool TTimeTHREE;
                            TTimeONE = CheckISTimeONE(dtQueue.Rows[i]["FSBEG_TIME"].ToString());
                            TTimeTWO = CheckISTimeTWO(dtQueue.Rows[i]["FSBEG_TIME"].ToString());
                            TTimeTHREE = CheckISTimeTHREE(dtQueue.Rows[i]["FSBEG_TIME"].ToString());
                            if (i < dtQueue.Rows.Count - 1) //如果不是最後一筆則要先比對下一筆節目的起始時間,跟這一筆在8分鐘內,則要看下一筆節目的起始時間是否也算是在整點
                            {
                                if (CALProgTime(dtQueue.Rows[i]["FSBEG_TIME"].ToString(), dtQueue.Rows[i + 1]["FSBEG_TIME"].ToString()) < 8)
                                {
                                    if (TTimeONE == true && CheckISTimeONE(dtQueue.Rows[i + 1]["FSBEG_TIME"].ToString()) == true)
                                        TTimeONE = false;
                                    if (TTimeTWO == true && CheckISTimeTWO(dtQueue.Rows[i + 1]["FSBEG_TIME"].ToString()) == true)
                                        TTimeTWO = false;
                                    if (TTimeTHREE == true && CheckISTimeTHREE(dtQueue.Rows[i + 1]["FSBEG_TIME"].ToString()) == true)
                                        TTimeTHREE = false;
                                }
                            }
                            
                            dtProgSeg = getProgSegment_MIX(dtQueue.Rows[i]["FSPROG_ID"].ToString(), dtQueue.Rows[i]["FNEPISODE"].ToString(), dtQueue.Rows[i]["FNSEQNO"].ToString(), dtQueue.Rows[i]["FSCHANNEL_ID"].ToString(), ProgLength.ToString(), dtQueue.Rows[i]["FNDEF_MIN"].ToString(), dtQueue.Rows[i]["FNDEF_SEC"].ToString(), dtQueue.Rows[i]["FNDEF_SEG"].ToString());
                            
                            for (int j = 0; j <= dtProgSeg.Rows.Count - 1; j++)
                            {
                                if (int.Parse(dtProgSeg.Rows[j]["FNSEG_ID"].ToString()) == 1) //如果是第1段則先插入第0段的宣傳帶
                                {
                                    bool TLastSeg;
                                    if (j == dtProgSeg.Rows.Count - 1)
                                        TLastSeg = true;
                                    else
                                        TLastSeg = false;

                                    getProgSegmentPromo(TProgID, TProgName, TSEQNO, TBREAKNO, dt_PROG_PROMO_LINK, TLastSeg, TTimeONE, TTimeTWO, TTimeTHREE);
                                } //第一段節目
                                //#插入節目
                                ArrPromoList TempArrPromoList = new ArrPromoList();

                                TempArrPromoList.COMBINENO = "";
                                if (int.Parse(dtProgSeg.Rows[j]["FNSEG_ID"].ToString()) == 1)
                                    TempArrPromoList.FCTIME_TYPE = "O";
                                else
                                    TempArrPromoList.FCTIME_TYPE = "A";
                                TempArrPromoList.FSPROG_ID = dtQueue.Rows[i]["FSPROG_ID"].ToString();
                                TempArrPromoList.FNSEQNO = dtQueue.Rows[i]["FNSEQNO"].ToString();
                                TempArrPromoList.FSNAME = dtQueue.Rows[i]["FSPROG_NAME"].ToString();
                                TempArrPromoList.FSCHANNEL_ID = dtQueue.Rows[i]["FSCHANNEL_ID"].ToString();
                                TempArrPromoList.FSCHANNEL_NAME = dtQueue.Rows[i]["FSCHANNEL_NAME"].ToString();
                                int IDurationFrame = MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtProgSeg.Rows[j]["FSEND_TIMECODE"].ToString()) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtProgSeg.Rows[j]["FSBEG_TIMECODE"].ToString());

                                if (int.Parse(dtProgSeg.Rows[j]["FNSEG_ID"].ToString()) == 1)
                                {
                                    NowPlaytime = dtQueue.Rows[i]["FSBEG_TIME"].ToString().Substring(0, 2) + ":" + dtQueue.Rows[i]["FSBEG_TIME"].ToString().Substring(2, 2) + ":00;00";
                                }
                                else
                                {
                                    NowPlaytime = NextPlaytime;
                                }
                                NextPlaytime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(NowPlaytime) + IDurationFrame);

                                TempArrPromoList.FSPLAY_TIME = NowPlaytime;
                                //NextPlaytime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(NowPlaytime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtProgSeg.Rows[j - 1]["FSEND_TIMECODE"].ToString()) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtProgSeg.Rows[j - 1]["FSBEG_TIMECODE"].ToString()));

                                TempArrPromoList.FSDURATION = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(IDurationFrame);
                                TempArrPromoList.FNEPISODE = dtQueue.Rows[i]["FNEPISODE"].ToString();
                                TempArrPromoList.FNBREAK_NO = int.Parse(dtProgSeg.Rows[j]["FNSEG_ID"].ToString()).ToString();
                                TempArrPromoList.FNSUB_NO = "0";
                                TempArrPromoList.FSFILE_NO = dtProgSeg.Rows[j]["FSFILE_NO"].ToString();
                                TempArrPromoList.FSVIDEO_ID = dtProgSeg.Rows[j]["FSVIDEO_ID"].ToString();
                                TempArrPromoList.FSPROMO_ID = "";
                                TempArrPromoList.FDDATE = dtQueue.Rows[i]["FDDATE"].ToString();
                                TempArrPromoList.FNLIVE = dtQueue.Rows[i]["FNLIVE"].ToString();
                                TempArrPromoList.FCTYPE = "G";
                                TempArrPromoList.FSSTATUS = "";
                                TempArrPromoList.FSSIGNAL = dtQueue.Rows[i]["FSSIGNAL"].ToString();
                                TempArrPromoList.FSMEMO = dtQueue.Rows[i]["FSMEMO"].ToString();
                                TempArrPromoList.FSMEMO1 = dtQueue.Rows[i]["FSMEMO1"].ToString();
                                //TempArrPromoList.FSPROG_NAME = ReplaceXML(dtQueue.Rows[i]["FSPROG_NAME"].ToString());
                                TempArrPromoList.FSPROG_NAME = dtQueue.Rows[i]["FSPROG_NAME"].ToString();

                                for (int k = 0; k <= dtCombineQueue.Rows.Count - 1; k++) //比對舊架構的資料將FCTIMETYPE,FSSIGNAL,FSMEMO,FSMEMO1抄寫過來
                                {
                                    if (dtQueue.Rows[i]["FSPROG_ID"].ToString() == dtCombineQueue.Rows[k]["FSPROG_ID"].ToString() &&
                                        dtQueue.Rows[i]["FNSEQNO"].ToString() == dtCombineQueue.Rows[k]["FNSEQNO"].ToString() &&
                                         int.Parse(dtProgSeg.Rows[j]["FNSEG_ID"].ToString()) == int.Parse(dtCombineQueue.Rows[k]["FNBREAK_NO"].ToString()))
                                    {
                                        TempArrPromoList.FSPLAY_TIME = dtCombineQueue.Rows[k]["FSPLAY_TIME"].ToString();
                                        TempArrPromoList.FCTIME_TYPE = dtCombineQueue.Rows[k]["FCTIME_TYPE"].ToString();
                                        TempArrPromoList.FSSIGNAL = dtCombineQueue.Rows[k]["FSSIGNAL"].ToString();
                                        TempArrPromoList.FSMEMO = dtCombineQueue.Rows[k]["FSMEMO"].ToString();
                                        TempArrPromoList.FSMEMO1 = dtCombineQueue.Rows[k]["FSMEMO1"].ToString();

                                    }
                                }

                                DatePlayList.Add(TempArrPromoList);

                                bool TLastSeg1;
                                if (j == dtProgSeg.Rows.Count - 1)
                                    TLastSeg1 = true;
                                else
                                    TLastSeg1 = false;

                                getProgSegmentPromo(TProgID, TProgName, TSEQNO, int.Parse(dtProgSeg.Rows[j]["FNSEG_ID"].ToString()).ToString(), dt_PROG_PROMO_LINK, TLastSeg1, TTimeONE, TTimeTWO, TTimeTHREE);

                            }

                        }

                        //加入最後一行空白列,以方便插入
                        InsertNullProgSeg("");
                        #endregion
                    }
                    
                    //將DatePlayList的資料寫成XML
                    //DatePlayList
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("<Datas>");

                    Combineno = 1;
                    NowPlaytime = dtQueue.Rows[0]["FSBEG_TIME"].ToString().Substring(0, 2) + ":" + dtQueue.Rows[0]["FSBEG_TIME"].ToString().Substring(2, 2) + ":00;00";
                    NextPlaytime = dtQueue.Rows[0]["FSBEG_TIME"].ToString().Substring(0, 2) + ":" + dtQueue.Rows[0]["FSBEG_TIME"].ToString().Substring(2, 2) + ":00;00";
                    foreach (ArrPromoList TempPromoList in DatePlayList)
                    {
                        sb.AppendLine("<Data>");
                        sb.AppendLine("<COMBINENO>" + Combineno.ToString() + " </COMBINENO>");
                        Combineno = Combineno + 1;
                        if (TempPromoList.FCTIME_TYPE == "" || TempPromoList.FCTIME_TYPE == null)
                            sb.AppendLine("<FCTIME_TYPE>" + "A" + "</FCTIME_TYPE>");
                        else
                            sb.AppendLine("<FCTIME_TYPE>" + TempPromoList.FCTIME_TYPE + "</FCTIME_TYPE>");

                        sb.AppendLine("<FSPROG_ID>" + TempPromoList.FSPROG_ID + "</FSPROG_ID>");
                        sb.AppendLine("<FNSEQNO>" + TempPromoList.FNSEQNO + "</FNSEQNO>");
                        sb.AppendLine("<FSNAME>" + ReplaceXML(TempPromoList.FSNAME) + "</FSNAME>");
                        sb.AppendLine("<FSCHANNEL_ID>" + dtQueue.Rows[0]["FSCHANNEL_ID"].ToString() + "</FSCHANNEL_ID>");
                        sb.AppendLine("<FSCHANNEL_NAME>" + dtQueue.Rows[0]["FSCHANNEL_NAME"].ToString() + "</FSCHANNEL_NAME>");
                        if (TempPromoList.FSPROG_ID != "")
                        {
                            if (TempPromoList.FCTIME_TYPE == "O")
                            {
                                sb.AppendLine("<FSPLAY_TIME>" + TempPromoList.FSPLAY_TIME + "</FSPLAY_TIME>");
                                NowPlaytime = TempPromoList.FSPLAY_TIME;
                                NextPlaytime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TempPromoList.FSPLAY_TIME) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TempPromoList.FSDURATION));
                            }
                            else
                            {
                                sb.AppendLine("<FSPLAY_TIME>" + NextPlaytime + "</FSPLAY_TIME>");
                                NextPlaytime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(NextPlaytime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TempPromoList.FSDURATION));
                            }
                        }
                        else
                        {
                            sb.AppendLine("<FSPLAY_TIME>" + "" + "</FSPLAY_TIME>");
                        }

                        sb.AppendLine("<FSDURATION>" + TempPromoList.FSDURATION + "</FSDURATION>");
                        sb.AppendLine("<FNEPISODE>" + TempPromoList.FNEPISODE + "</FNEPISODE>");
                        sb.AppendLine("<FNBREAK_NO>" + TempPromoList.FNBREAK_NO + "</FNBREAK_NO>");
                        sb.AppendLine("<FNSUB_NO>" + TempPromoList.FNSUB_NO + "</FNSUB_NO>");
                        sb.AppendLine("<FSFILE_NO>" + TempPromoList.FSFILE_NO + "</FSFILE_NO>");
                        sb.AppendLine("<FSVIDEO_ID>" + TempPromoList.FSVIDEO_ID + "</FSVIDEO_ID>");
                        sb.AppendLine("<FSPROMO_ID>" + TempPromoList.FSPROMO_ID + "</FSPROMO_ID>");
                        sb.AppendLine("<FDDATE>" + dtQueue.Rows[0]["FDDATE"].ToString() + "</FDDATE>");
                        sb.AppendLine("<FNLIVE>" + TempPromoList.FNLIVE + "</FNLIVE>");
                        sb.AppendLine("<FCTYPE>" + TempPromoList.FCTYPE + "</FCTYPE>");
                        sb.AppendLine("<FSSTATUS>" + TempPromoList.FSSTATUS + "</FSSTATUS>");
                        sb.AppendLine("<FSSIGNAL>" + TempPromoList.FSSIGNAL + "</FSSIGNAL>");
                        sb.AppendLine("<FSMEMO>" + ReplaceXML(TempPromoList.FSMEMO) + "</FSMEMO>");
                        sb.AppendLine("<FSMEMO1>" + ReplaceXML(TempPromoList.FSMEMO1) + "</FSMEMO1>");
                        sb.AppendLine("<FSPROG_NAME>" + ReplaceXML(TempPromoList.FSPROG_NAME) + "</FSPROG_NAME>");
                        sb.AppendLine("</Data>");

                    }
                    sb.AppendLine("</Datas>");

                    #region 先查詢TBPGM_ARR_PROMO_LOST最大編號 MAX_ARR_PROMO_LOST_ID
                    SqlDataAdapter da_LOST_MAX_ID = new SqlDataAdapter("SP_Q_TBPGM_ARR_PROMO_LOST_MAX_ID", connection);
                    DataTable dt_LOST_MAX_ID = new DataTable();

                    //SqlParameter para_CHANNEL_PROMO_LINK = new SqlParameter("@FSCHANNEL_ID", dtQueue.Rows[1]["FSCHANNEL_ID"].ToString());
                    SqlParameter para_LOST_MAX_ID = new SqlParameter("@FSCHANNEL_ID", ParaChannel);
                    da_LOST_MAX_ID.SelectCommand.Parameters.Add(para_LOST_MAX_ID);
                    SqlParameter para_LOST_MAX_ID1 = new SqlParameter("@FDDATE", ParaDate);
                    da_LOST_MAX_ID.SelectCommand.Parameters.Add(para_LOST_MAX_ID1);

                    da_LOST_MAX_ID.SelectCommand.CommandType = CommandType.StoredProcedure;

                    if (connection.State == System.Data.ConnectionState.Closed)
                    {
                        connection.Open();
                    }


                    //先查詢QUEUE&Arrpromo的資料
                    da_LOST_MAX_ID.Fill(dt_LOST_MAX_ID);
                    int MAX_ARR_PROMO_LOST_ID = 0;
                    for (int L = 0; L <= dt_LOST_MAX_ID.Rows.Count - 1; L++) //先取得段落
                    {
                        if (dt_LOST_MAX_ID.Rows[L][0].ToString() == "")
                            MAX_ARR_PROMO_LOST_ID = 0;
                        else
                            MAX_ARR_PROMO_LOST_ID = int.Parse(dt_LOST_MAX_ID.Rows[L][0].ToString());
                    }
                    MAX_ARR_PROMO_LOST_ID = MAX_ARR_PROMO_LOST_ID + 1;
                    //connection.Close();
                    #endregion

                    #region 將沒有對應到的Promo寫入TBPGM_ARR_PROMO_LOST中
                    int hasInsertNum = 0;
                    int InsErrNum = 0;
                    SqlTransaction tran = null;
                    
                    SqlCommand sqlIns_Arr_Promo_Lost = new SqlCommand("SP_I_TBPGM_ARR_PROMO_LOST", connection);
                    
                    tran = connection.BeginTransaction();
                    sqlIns_Arr_Promo_Lost.Transaction = tran;
                    try
                    {
                        if (connection.State == System.Data.ConnectionState.Closed)
                        {
                            connection.Open();
                        }

                        for (int i = 0; i <= dtArrPromo.Rows.Count - 1; i++) //先取得段落
                        {

                            if (dtArrPromo.Rows[i]["hasInsert"].ToString() != "Y")
                            {
                                sqlIns_Arr_Promo_Lost.Parameters.Clear();
                                SqlParameter para_LOST_FNCREATE_LIST_NO = new SqlParameter("@FNCREATE_LIST_NO", MAX_ARR_PROMO_LOST_ID);
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FNCREATE_LIST_NO);

                                SqlParameter para_LOST_FNARR_PROMO_NO = new SqlParameter("@FNARR_PROMO_NO", dtArrPromo.Rows[i]["FNARR_PROMO_NO"].ToString());
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FNARR_PROMO_NO);

                                SqlParameter para_LOST_FSPROG_ID = new SqlParameter("@FSPROG_ID", dtArrPromo.Rows[i]["FSPROG_ID"].ToString());
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FSPROG_ID);

                                SqlParameter para_LOST_FNSEQNO = new SqlParameter("@FNSEQNO", dtArrPromo.Rows[i]["FNSEQNO"].ToString());
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FNSEQNO);

                                //string[] DateArr = dtArrPromo.Rows[i]["FDDATE"].ToString().Split(' ');
                                SqlParameter para_LOST_FDDATE = new SqlParameter("@FDDATE", dtArrPromo.Rows[i]["FDDATE"].ToString().Split(' ')[0]);
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FDDATE);

                                SqlParameter para_LOST_FSCHANNEL_ID = new SqlParameter("@FSCHANNEL_ID", dtArrPromo.Rows[i]["FSCHANNEL_ID"].ToString());
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FSCHANNEL_ID);

                                SqlParameter para_LOST_FNBREAK_NO = new SqlParameter("@FNBREAK_NO", dtArrPromo.Rows[i]["FNBREAK_NO"].ToString());
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FNBREAK_NO);
                                SqlParameter para_LOST_FNSUB_NO = new SqlParameter("@FNSUB_NO", dtArrPromo.Rows[i]["FNSUB_NO"].ToString());
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FNSUB_NO);
                                SqlParameter para_LOST_FSPROMO_ID = new SqlParameter("@FSPROMO_ID", dtArrPromo.Rows[i]["FSPROMO_ID"].ToString());
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FSPROMO_ID);
                                SqlParameter para_LOST_FSPLAYTIME = new SqlParameter("@FSPLAYTIME", dtArrPromo.Rows[i]["FSPLAYTIME"].ToString());
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FSPLAYTIME);
                                SqlParameter para_LOST_FSDUR = new SqlParameter("@FSDUR", dtArrPromo.Rows[i]["FSDUR"].ToString());
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FSDUR);
                                SqlParameter para_LOST_FSSIGNAL = new SqlParameter("@FSSIGNAL", dtArrPromo.Rows[i]["FSSIGNAL"].ToString());
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FSSIGNAL);
                                SqlParameter para_LOST_FSMEMO = new SqlParameter("@FSMEMO", dtArrPromo.Rows[i]["FSMEMO"].ToString());
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FSMEMO);
                                SqlParameter para_LOST_FSMEMO1 = new SqlParameter("@FSMEMO1", dtArrPromo.Rows[i]["FSMEMO1"].ToString());
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FSMEMO1);
                                SqlParameter para_LOST_FSCREATED_BY = new SqlParameter("@FSCREATED_BY", dtArrPromo.Rows[i]["FSCREATED_BY"].ToString());
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FSCREATED_BY);
                                SqlParameter para_LOST_FDCREATED_DATE = new SqlParameter("@FDCREATED_DATE", dtArrPromo.Rows[i]["FDCREATED_DATE"].ToString().Split(' ')[0]);
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FDCREATED_DATE);
                                SqlParameter para_LOST_FSSTATUS = new SqlParameter("@FSSTATUS", "");
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FSSTATUS);
                                SqlParameter para_LOST_FNEPISODE = new SqlParameter("@FNEPISODE", dtArrPromo.Rows[i]["FNEPISODE"].ToString());
                                sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FNEPISODE); 
                                
                                sqlIns_Arr_Promo_Lost.CommandType = CommandType.StoredProcedure;


                                if (sqlIns_Arr_Promo_Lost.ExecuteNonQuery() == 0)
                                {
                                    InsErrNum = InsErrNum + 1;
                                }
                                else
                                {
                                    //不須知道異動筆數

                                    //tran.Rollback();

                                }

                            }

                            hasInsertNum = hasInsertNum + 1;
                        }

                        if (InsErrNum > 0)
                        {
                            tran.Rollback();
                            return "<Errors>" + "寫入遺失清單失敗,失敗筆數:" + InsErrNum.ToString() + "</Errors>";
                        }
                        else
                        {
                            tran.Commit();
                        }

                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        return "<Errors>" + "寫入遺失清單失敗,錯誤訊息:" + ex.Message + "</Errors>";
                    }
                    finally
                    {

                        connection.Close();
                        connection.Dispose();
                        sqlIns_Arr_Promo_Lost.Dispose();
                        GC.Collect();

                    }


                    #endregion



                    return sb.ToString();
                }
                else //(dtQueue.Rows.Count < 0) 沒有節目表資料
                {
                    return "<Errors>" + "沒有節目表資料" + "</Errors>";
                }

            }
            catch (Exception ex)
            {
                StringBuilder sbError = new StringBuilder();
                sbError.AppendLine("<Errors>" + ex.Message + "</Errors>");
                return sbError.ToString();
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }
        }
    }
}
