﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Xml;
using System.Text;
using System.Xml.Linq;
using System.IO;
using PTS_MAM3.Web.Class;
using System.Collections.ObjectModel;
using System.Web.Configuration;
using PTS_MAM3.Web.WS.WSTSM;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSMEDIA_INFO 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSMEDIA_INFO : System.Web.Services.WebService
    {
        /// <summary>
        /// 取回所有的 ArcType
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public string GetArcTypeList()
        {
            // 查詢資料庫
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBFILE_TYPE", new Dictionary<string, string>(), out sqlResult))
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSMEDIA_INFO/GetArcTypeList", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "Got false when SP_Q_TBFILE_TYPE");
                return string.Empty;    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            }

            // 組合
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<Datas>");
            for (int i = 0; i < sqlResult.Count; i++)
            {
                sb.AppendLine("<Data>");
                sb.AppendLine(string.Concat("  <ID>", sqlResult[i]["FSARC_TYPE"], "</ID>"));
                sb.AppendLine(string.Concat("  <NAME>", sqlResult[i]["FSTYPE_NAME"], "</NAME>"));
                sb.AppendLine(string.Concat("  <TYPE>", sqlResult[i]["FSTYPE"], "</TYPE>"));
                sb.AppendLine(string.Concat("  <ENABLE>", sqlResult[i]["FBISENABLE"], "</ENABLE>"));
                sb.AppendLine("</Data>");
            }
            sb.AppendLine("</Datas>");

            return sb.ToString();
        }

        /// <summary>
        /// 輸入progID、子集、型態，回傳唯一的fileNo
        /// </summary>
        /// <param name="progID"></param>
        /// <param name="episodeNo"></param>
        /// <param name="arcType"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetMediaFileNo(string progID, string episodeNo, string arcType)
        {
            // 檢查輸入的參數
            int dummyInt;
            if (string.IsNullOrEmpty(episodeNo) || !Int32.TryParse(episodeNo, out dummyInt) || dummyInt < 1)
                return string.Empty;

            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSID", progID);
            sqlParameters.Add("FNEPISODE", episodeNo);
            sqlParameters.Add("FSARC_TYPE", arcType);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_BY_ID_EPISODE_ARCTPYE", sqlParameters, out sqlResult))
                return string.Empty;    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count == 0)
                return string.Empty;
            else       
                return sqlResult[0]["FSFILE_NO"];
        }

        /// <summary>
        /// 回傳 MPEG-21 格式描述的媒體檔資訊
        /// </summary>
        /// <param name="FSFILE_NO"></param>
        /// <returns></returns>
        [WebMethod]
        public String GetMediaInfo(string FSFILE_NO)
        {
            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            StringBuilder sb = new StringBuilder();


            SqlDataAdapter da = new SqlDataAdapter("SP_Q_WSMEDIAINFO", connection);
            DataTable dt = new DataTable();
            SqlParameter para = new SqlParameter("@FSFILE_NO", FSFILE_NO);
            da.SelectCommand.Parameters.Add(para);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            String strResult = "";

            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                da.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    strResult = dt.Rows[0][0].ToString();

                    ServiceTSM tsm = new ServiceTSM();

                    //取代第一個component的@C1_01
                    String _C1_01 = tsm.GetHttpPathByFileID(FSFILE_NO, ServiceTSM.FileID_MediaType.LowVideo);
                    strResult = strResult.Replace("@C1_01", _C1_01);

                    List<ServiceTSM.KeyframeInfo> _FilesInfo = tsm.GetKeyframeFilesInfo(FSFILE_NO);
                    foreach (ServiceTSM.KeyframeInfo finfo in _FilesInfo)
                    {
                        String _KeyframeHttpPath = finfo.KeyframeHttpPath;
                        String _KeyframeDescription = finfo.KeyframeDescription;
                        String _timecode = "待替換@" + _KeyframeHttpPath.Substring(_KeyframeHttpPath.LastIndexOf("_") + 1, 8);

                        strResult = strResult.Replace(_timecode, _KeyframeHttpPath);
                    }              
                }
                else
                {
                    strResult = "";
                }
                connection.Close();
                connection.Dispose();
                GC.Collect();
                return strResult;
            }
            catch (Exception ex)
            {
                return "";
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }
        }
    }
}
