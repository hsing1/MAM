﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using MAM_PTS_DLL;
using System.Xml;
using System.IO;
using PTS_MAM3.Web.WS.WSTSM;
using PTS_MAM3.Web.WS.WSASC;
using Xealcom.Security;
using ADSSECURITYLib;
using ActiveDs;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSBookingFunction 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSBookingFunction : System.Web.Services.WebService
    {
        private WSSearchService.SearchServiceSoapClient wsSearchService = new WSSearchService.SearchServiceSoapClient();

        /// <summary>
        /// 檢查檔案是否為數位檔
        /// </summary>
        /// <param name="sp_name">SP名稱</param>
        /// <param name="parameter">參數</param>
        /// <returns>回傳檔案來源，若為T，則為影帶，否則為空字串</returns>
        [WebMethod]
        public string Check_File_Is_Tape(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_CHECK_FILE_IS_TAPE", parameter);
            return result;
        }

        /// <summary>
        /// 取得預借清單編號
        /// </summary>
        /// <param name="sp_name">SP名稱</param>
        /// <param name="parameter">參數</param>
        /// <returns>預借清單編號</returns>
        [WebMethod]
        public string GetPreBookingNo(string sp_name, string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query(sp_name, parameter);
            return result;
        }

        /// <summary>
        /// 取得調用清單編號
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>調用清單編號</returns>
        [WebMethod]
        public string GetBookingNo(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_I_TBNORECORD", parameter);
            return result;
        }

        /// <summary>
        /// 取得檔案資訊
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>檔案資訊</returns>
        [WebMethod]
        public string GetFileInformation(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_FILE_INFORMATION", parameter);
            return result;
        }


        /// <summary>
        /// 取得檔案資訊
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>檔案資訊</returns>
        [WebMethod]
        public string GetFileInformation2(string fsUSER_ID, string fsFILE_NOs, string fsFILE_TYPEs, int fnGROUP_ID)
        {
            // 在此新增您的作業實作
            string result = "";

            //重寫多筆新增
            if (!string.IsNullOrEmpty(fsFILE_NOs) && !string.IsNullOrEmpty(fsFILE_TYPEs))
            {
                string[] fsFILE_NO = fsFILE_NOs.Split(',');
                string[] fsFILE_TYPE = fsFILE_TYPEs.Split(',');
                if (fsFILE_NO.Length == fsFILE_TYPE.Length)
                {
                    for (int i = 0; i <= fsFILE_NO.Length - 1; i++)
                    {
                        string fsFILE_LOCATION = GetFileLocation(fsFILE_NO[i], fsFILE_TYPE[i]);
                        string xmlFILE_INFORMATION = GetFileInformation("<Data><FSFILE_NO>" + fsFILE_NO[i] + "</FSFILE_NO><FSVW_NAME>" + fsFILE_TYPE[i] + "</FSVW_NAME></Data>");
                        XmlDocument xDOC = new XmlDocument();
                        xDOC.LoadXml(xmlFILE_INFORMATION);

                        string parameter = "";
                        parameter += "<Data>";
                        parameter += "<FNNO></FNNO>";
                        parameter += "<FSGUID>" + Guid.NewGuid().ToString() + "</FSGUID>";
                        parameter += "<FSUSER_ID>" + fsUSER_ID + "</FSUSER_ID>";
                        parameter += "<FDBOOKING_DATE>" + DateTime.Now.ToString("yyyy/MM/dd") + "</FDBOOKING_DATE>";
                        parameter += "<FSFILE_NO>" + fsFILE_NO[i] + "</FSFILE_NO>";
                        parameter += "<FSSTART_TIMECODE>" + GetFrame(xDOC.FirstChild.ChildNodes[0].ChildNodes[5].FirstChild.Value, xDOC.FirstChild.ChildNodes[0].ChildNodes[2].FirstChild.Value) + "</FSSTART_TIMECODE>";
                        parameter += "<FSBEG_TIMECODE></FSBEG_TIMECODE>";
                        parameter += "<FSEND_TIMECODE></FSEND_TIMECODE>";
                        parameter += "<FSFILE_TYPE>" + xDOC.FirstChild.ChildNodes[0].ChildNodes[4].FirstChild.Value + "</FSFILE_TYPE>";
                        parameter += "<FSFILE_CATEGORY>" + xDOC.FirstChild.ChildNodes[0].ChildNodes[5].FirstChild.Value + "</FSFILE_CATEGORY>";
                        parameter += "<FNLOCATION>" + fsFILE_LOCATION + "</FNLOCATION>";
                        parameter += "<FCOUT_BUY>" + xDOC.FirstChild.ChildNodes[0].ChildNodes[6].FirstChild.Value + "</FCOUT_BUY>";
                        parameter += "<FCBOOKING_TYPE>1</FCBOOKING_TYPE>";
                        if (fnGROUP_ID == 63)
                        {
                            parameter += "<FCSUPERVISOR_CHECK>N</FCSUPERVISOR_CHECK>";
                            parameter += "<FSSUPERVISOR></FSSUPERVISOR>";
                        }
                        else
                        {
                            parameter += "<FCSUPERVISOR_CHECK>" + xDOC.FirstChild.ChildNodes[0].ChildNodes[7].FirstChild.Value + "</FCSUPERVISOR_CHECK>";
                            parameter += "<FSSUPERVISOR>" + xDOC.FirstChild.ChildNodes[0].ChildNodes[8].FirstChild.Value + "</FSSUPERVISOR>";
                        }

                        parameter += "<FCALL></FCALL>";
                        parameter += "</Data>";


                        if (!InsertPreBooking2(parameter, fsUSER_ID))
                        {
                            result += "檔案編號:" + fsFILE_NO[i] + "加入預借清單失敗!\r\n";
                        }
                    }
                }
                else
                {
                    result = "檔案個數與型別個數不符，新增失敗!";
                }
            }
            //result = DbAccess.Do_Query("SP_Q_FILE_INFORMATION", parameter);
            return result;
        }


        private string GetFrame(string category, string timecode)
        {
            if (timecode != "")
            {
                if (category == "1")
                {
                    
                    return timecodetoframe(timecode).ToString();
                }
                else if (category == "5")
                {
                    return timecode;
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
        }

        public static int timecodetoframe(string Timecode)
        {
            int HH = 0;
            int MM = 0;
            int SS = 0;
            int FF = 0;
            int itmp = 0;

            HH = Int32.Parse(Timecode.Substring(0, 2));
            MM = Int32.Parse(Timecode.Substring(3, 2));
            SS = Int32.Parse(Timecode.Substring(6, 2));

            FF = Int32.Parse(Timecode.Substring(9, 2));
            itmp = HH * 107892;
            //itmp = itmp + MM * 1798;
            if (MM >= 50)
            {
                itmp = itmp + (17982 * 5);
                MM = MM - 50;
            }
            else if (MM >= 40)
            {
                itmp = itmp + (17982 * 4);
                MM = MM - 40;
            }
            else if (MM >= 30)
            {
                itmp = itmp + (17982 * 3);
                MM = MM - 30;
            }
            else if (MM >= 20)
            {
                itmp = itmp + (17982 * 2);
                MM = MM - 20;
            }
            else if (MM >= 10)
            {
                itmp = itmp + (17982 * 1);
                MM = MM - 10;
            }
            if (MM > 0)
            {
                itmp = itmp + MM * 1798 + 2;
                itmp = itmp + SS * 30;
                //if (FF >= 2)
                itmp = itmp + FF + 1 - 2;
            }
            else if (MM == 0)
            {
                itmp = itmp + SS * 30;
                itmp = itmp + FF + 1;
            }
            return itmp - 1;

        }

        /// <summary>
        /// 檢查檔案是否已存在預借清單中
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>檔案資訊</returns>
        [WebMethod]
        public string GetFileExistPreBooking(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_CHECK_PREBOOKING_FILE_EXIST", parameter);
            return result;
        }

        /// <summary>
        /// 檢查檔案是否已存在預借清單中
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>檔案資訊</returns>
        [WebMethod]
        public string GetFileExistPreBooking2(string fsUSER_ID, string fsFILE_NOs, string fcPREBOOKING_TYPE)
        {
            // 在此新增您的作業實作
            string result = "";
            List<Dictionary<string, string>> sqlResult;
            Dictionary<string, string> _dic = new Dictionary<string, string>();
            _dic.Add("FSUSER_ID", fsUSER_ID);
            _dic.Add("FSFILE_NOs", fsFILE_NOs);
            _dic.Add("FCBOOKING_TYPE", fcPREBOOKING_TYPE);
            DbAccess.Do_Query("SP_Q_CHECK_PREBOOKING_FILE_EXIST2", _dic, out sqlResult);
            if (sqlResult != null && sqlResult.Count > 0)
            {
                result = (string.IsNullOrEmpty(sqlResult[0]["fsFILE_NOs"]) == true ? string.Empty : sqlResult[0]["fsFILE_NOs"].Substring(0, sqlResult[0]["fsFILE_NOs"].Length - 1));
            }

            return result;
        }


        /// <summary>
        /// 取得檔案位置
        /// </summary>
        /// <param name="fsfile_no">檔案編號</param>
        /// <returns>檔案位置</returns>
        [WebMethod]
        public string GetFileLocation(string fsfile_no, string fsfile_type)
        {
            // 在此新增您的作業實作
            string result = "1";
            try
            {
                if (fsfile_no != null && fsfile_no != "" && fsfile_type == "VW_SEARCH_VIDEO")
                {
                    WS.WSTSM.ServiceTSM obj = new WS.WSTSM.ServiceTSM();

                    WS.WSTSM.ServiceTSM.TSM_File_Status status = obj.GetFileStatusByFileID(fsfile_no, WS.WSTSM.ServiceTSM.FileID_MediaType.HiVideo);

                    switch (status)
                    {
                        case WS.WSTSM.ServiceTSM.TSM_File_Status.NearLine:
                            result = "1";
                            break;
                        case WS.WSTSM.ServiceTSM.TSM_File_Status.TapeOnly:
                            result = "2";
                            break;
                        case WS.WSTSM.ServiceTSM.TSM_File_Status.TapeOffline:
                            result = "3";
                            break;
                        case WS.WSTSM.ServiceTSM.TSM_File_Status.NotAvailable:
                            result = "4";
                            break;
                    }
                    //result = "1";
                }
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }

        }

        
        /// <summary>
        /// 新增預借清單
        /// </summary>
        /// <param name="prelookingList">參數</param>
        /// <returns>成功或失敗</returns>
        [WebMethod]
        public bool InsertPreBooking1(string prelookingList, string user_id)
        {
            bool flag = true;

            if (prelookingList != null && prelookingList != "")
            {

                XmlDocument xDoc = new XmlDocument();
                xDoc.LoadXml(prelookingList);
                XmlNodeList xNodeList = xDoc.GetElementsByTagName("Data");
                if (xNodeList != null && xNodeList.Count > 0)
                {
                    for (int i = 0; i <= xNodeList.Count - 1; i++)
                    {
                        XmlNode xNode = xNodeList[i];
                        string parameter = "<Data>" + xNode.InnerXml + "</Data>";
                        if (!DbAccess.Do_Transaction("SP_I_TBPREBOOKING", parameter, user_id))
                        {
                            flag = false;
                            return flag;
                        }
                    }
                }

            }

            return flag;
        }


        /// <summary>
        /// 新增Asx檔案
        /// </summary>
        /// <param name="guid">guid</param>
        /// <param name="start_time">開始時間</param>
        /// <param name="duration">長度</param>
        /// <param name="url">路徑</param>
        /// <returns>成功或失敗</returns>
        [WebMethod]
        public bool WriteAsxFile(string guid, string start_time, string duration, string url)
        {
            bool flag = true;
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string path = SettingObj.sysConfig_Read("/ServerConfig/AsxFilePath") + @"\" + guid + @".asx";
            string asxString = "<ASX VERSION=\"3.0\">";
            asxString += "<ENTRY>";
            asxString += "<STARTTIME VALUE=\"" + start_time + "\" />";
            asxString += "<DURATION VALUE=\"" + duration + "\" />";
            asxString += "<REF HREF=\"" + url + "\" />";
            asxString += "</ENTRY>";
            asxString += "</ASX>";
            File.WriteAllText(path, asxString, System.Text.Encoding.Default);
            return flag;
        }

        /// <summary>
        /// 取得Asx檔案
        /// </summary>
        /// <param name="guid">guid</param>
        /// <param name="start_time">開始時間</param>
        /// <param name="duration">長度</param>
        /// <param name="url">路徑</param>
        /// <returns>成功或失敗</returns>
        [WebMethod]
        public string GetAsxFile(string guid)
        {
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string path = SettingObj.sysConfig_Read("/ServerConfig/AsxFileHttpPath") + guid + @".asx";
            return path;
        }

        /// <summary>
        /// 新增預借清單
        /// </summary>
        /// <param name="prelookingList">參數</param>
        /// <param name="user_id">使用者名稱</param>
        /// <returns>成功或失敗</returns>
        [WebMethod]
        public bool InsertPreBooking2(string parameter, string user_id)
        {
            bool flag = true;
            flag = DbAccess.Do_Transaction("SP_I_TBPREBOOKING", parameter, user_id);
            return flag;
        }

        /// <summary>
        /// 取得預借清單List
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>預借清單List</returns>
        [WebMethod]
        public string GetPreBookingList(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_PREBOOKING_BY_USERID", parameter);
            return result;
        }

        /// <summary>
        /// 取得檔案路徑
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>檔案路徑</returns>
        [WebMethod]
        public string GetFilePath(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_GET_FILE_PATH", parameter);
            return result;
        }

        /// <summary>
        /// 刪除預借清單
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <param name="user_id">使用者名稱</param>
        /// <returns>成功或失敗</returns>
        [WebMethod]
        public bool DeletePreBooking(string parameter, string user_id)
        {
            // 在此新增您的作業實作
            bool flag = true;
            flag = DbAccess.Do_Transaction("SP_D_TBPREBOOKING", parameter, user_id);
            return flag;
        }


        /// <summary>
        /// 檢查檔案是否已被MarkIn、MarkOut
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>MarkIn、MarkOut檔案個數</returns>
        [WebMethod]
        public string CheckFileMark(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_CHECK_BOOKING_FILE_MARK", parameter);
            return result;
        }

        /// <summary>
        /// 取得轉檔資訊列表
        /// </summary>
        /// <returns>Profile完整路徑與檔名</returns>
        [WebMethod]
        public string GetFileTranscodeInfo()
        {
            // 在此新增您的作業實作
            string[] filePath = null;
            string returnPathName = "";
            SysConfig sysconfig = new SysConfig();
            filePath = Directory.GetFiles(sysconfig.sysConfig_Read("/ServerConfig/ANS_Config/PROFILE_CUSTOM_ENC_DIR"), "*.awp", SearchOption.AllDirectories);

            if (filePath != null && filePath.Count() > 0)
            {
                for (int i = 0; i <= filePath.Count() - 1; i++)
                {
                    returnPathName += filePath[i] + "|" + System.IO.Path.GetFileNameWithoutExtension(filePath[i]) + ";";
                }
            }

            if (returnPathName != "")
            {
                returnPathName = returnPathName.Substring(0, returnPathName.Length - 1);
            }

            return returnPathName;
        }


        /// <summary>
        /// 取得轉檔資訊內容
        /// </summary>
        /// <returns>Profile完整路徑與檔名</returns>
        [WebMethod]
        public string GetFileTranscodeInfoTxt(string fileTranscodeName)
        {
            // 在此新增您的作業實作;
            string content = "";
            SysConfig sysconfig = new SysConfig();
            if (File.Exists(fileTranscodeName + ".txt"))
            {
                content = File.ReadAllText(fileTranscodeName + ".txt");
            }


            return content;
        }

        /// <summary>
        /// 取得Logo資訊列表
        /// </summary>
        /// <returns>Logo完整路徑與檔名</returns>
        [WebMethod]
        public string GetFileLogoInfo()
        {
            // 在此新增您的作業實作
            string[] filePath = null;
            string returnPathName = "";
            SysConfig sysconfig = new SysConfig();
            filePath = Directory.GetFiles(sysconfig.sysConfig_Read("/ServerConfig/ANS_Config/PROFILE_WATERMARK_DIR"), "*.*", SearchOption.AllDirectories);
            //filePath = Directory.GetFiles("D:\\PTS\\數位片庫\\profiles\\watermark", "*.*", SearchOption.AllDirectories);
            if (filePath != null && filePath.Count() > 0)
            {
                for (int i = 0; i <= filePath.Count() - 1; i++)
                {
                    returnPathName += filePath[i] + "|" + System.IO.Path.GetFileNameWithoutExtension(filePath[i]) + ";";
                }
            }

            if (returnPathName != "")
            {
                returnPathName = returnPathName.Substring(0, returnPathName.Length - 1);
            }

            return returnPathName;
        }

        /// <summary>
        /// 取得調用原因列表
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>調用原因列表</returns>
        [WebMethod]
        public string GetBookingReason(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_TBBOOKING_REASON", parameter);
            return result;
        }


        /// <summary>
        /// 新增調用清單
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <param name="user_id">使用者帳號</param>
        /// <returns>成功或失敗</returns>
        [WebMethod]
        public bool InsertBooking(string parameter, string user_id)
        {
            // 在此新增您的作業實作
            bool flag = true;
            flag = DbAccess.Do_Transaction("SP_I_TBBOOKING", parameter, user_id);

            //取得調用清單檔案編號
            string booking_no = "";
            XmlDocument xDoc1 = new XmlDocument();
            xDoc1.LoadXml(parameter);
            XmlNodeList xNodeList1 = xDoc1.GetElementsByTagName("FSBOOKING_NO");
            if (xNodeList1 != null && xNodeList1.Count > 0)
            {
                XmlNode xNode = xNodeList1[0];
                booking_no = xNode.FirstChild.Value;
            }

            //取得調用清單明細
            string p2 = DbAccess.Do_Query("SP_Q_TBBOOKING_DETAIL_BY_BOOKINGNO", "<Data><FSBOOKING_NO>" + booking_no + "</FSBOOKING_NO></Data>");
            XmlDocument xDoc2 = new XmlDocument();
            xDoc2.LoadXml(p2);
            XmlNodeList xNodeList2 = xDoc2.GetElementsByTagName("Data");
            if (xNodeList2 != null && xNodeList2.Count > 0)
            {
                for (int i = 0; i <= xNodeList2.Count - 1; i++)
                {
                    XmlNode xNode = xNodeList2[i];
                    string fsfile_no = (xNode.ChildNodes[2].FirstChild != null ? xNode.ChildNodes[2].FirstChild.Value : "");
                    string fstitle = (xNode.ChildNodes[3].FirstChild != null ? xNode.ChildNodes[3].FirstChild.Value : "");

                    string p3 = DbAccess.Do_Query("SP_Q_GET_PROG_DEPT_SUPERVISOR_EMAIL", "<Data><FSFILE_NO>" + fsfile_no + "</FSFILE_NO></Data>");


                    XmlDocument xDoc3 = new XmlDocument();
                    xDoc3.LoadXml(p3);
                    XmlNodeList xNodeList3 = xDoc3.GetElementsByTagName("Data");
                    if (xNodeList3 != null && xNodeList3.Count > 0)
                    {
                        XmlNode xNode3 = xNodeList3[0];
                        string fsuser_cht_name = (xNode3.ChildNodes[0].FirstChild != null ? xNode3.ChildNodes[0].FirstChild.Value : "");
                        string fsemail = (xNode3.ChildNodes[1].FirstChild != null ? xNode3.ChildNodes[1].FirstChild.Value : "");

                        //取得user_id name
                        string p4 = DbAccess.Do_Query("SP_Q_TBUSERS_CHTNAME_BY_USERID", "<Data><FSUSER_ID>" + user_id + "</FSUSER_ID></Data>");
                        XmlDocument xDoc4 = new XmlDocument();
                        xDoc4.LoadXml(p4);
                        XmlNodeList xNodeList4 = xDoc4.GetElementsByTagName("Data");
                        if (xNodeList4 != null && xNodeList4.Count > 0)
                        {
                            XmlNode xNode4 = xNodeList4[0];
                            string fs_user_name = (xNode4.ChildNodes[0].FirstChild != null ? xNode4.ChildNodes[0].FirstChild.Value : "");
                            //寄信
                            //string html = "";
                            //html += "<html>";
                            //html += "<body>";
                            //html += fsuser_cht_name + " 您好： <br />";
                            //html += "檔案編號：" + fsfile_no + "<br />";
                            //html += "檔案標題：" + fstitle + "<br />";
                            //html += "已由 " + fs_user_name + " 調用!";
                            //html += "</body>";
                            //html += "</html>";
                            //Protocol.SendEmail(fsemail, "調用檔案通知", html);

                        }



                        //string p4 = DbAccess.Do_Query("SP_Q_GET_USER_SUPERVISOR_DATA", "<Data><FSUSER_ID>" + createdBy + "</FSUSER_ID></Data>");

                        //XmlDocument xDoc4 = new XmlDocument();
                        //xDoc4.LoadXml(p4);
                        //XmlNodeList xNodeList4 = xDoc4.GetElementsByTagName("Data");
                        //if (xNodeList4 != null && xNodeList4.Count > 0)
                        //{
                        //    string fsuser_id = (xNodeList4[0].ChildNodes[0].FirstChild != null ? xNodeList4[0].ChildNodes[0].FirstChild.Value : "");
                        //    string fsuser_cht_name = (xNodeList4[0].ChildNodes[1].FirstChild != null ? xNodeList4[0].ChildNodes[1].FirstChild.Value : "");
                        //    string fsemail = (xNodeList4[0].ChildNodes[2].FirstChild != null ? xNodeList4[0].ChildNodes[2].FirstChild.Value : "");

                        //    //寄信
                        //    string html = "";
                        //    html += "<html>";
                        //    html += "<body>";
                        //    html += fsuser_cht_name + " 您好： <br />";
                        //    html += "檔案編號：" + fsfile_no + "<br />";
                        //    html += "檔案標題：" + fstitle + "<br />";
                        //    html += "已由" + user_id + "調用!";
                        //    html += "</body>";
                        //    html += "</html>";
                        //    Protocol.SendEmail(fsemail, "調用檔案通知", html);
                        //}

                    }
                }
            }


            return flag;
        }


        /// <summary>
        /// 新增調用清單2
        /// </summary>
        /// <param name="sp_name">SP名稱</param>
        /// <param name="parameter">參數</param>
        /// <returns>成功或失敗</returns>
        [WebMethod]
        public bool InsertBooking2(string parameter, string user_id)
        {
            // 在此新增您的作業實作
            bool flag = true;
            if (parameter != "")
            {
                XmlDocument xDoc = new XmlDocument();
                xDoc.LoadXml(parameter);
                XmlNodeList xNodeList = xDoc.GetElementsByTagName("Data");
                if (xNodeList != null && xNodeList.Count > 0)
                {
                    for (int i = 0; i <= xNodeList.Count - 1; i++)
                    {
                        XmlNode xNode = xNodeList[i];
                        string parameter1 = "<Data>" + xNode.InnerXml + "</Data>";
                        if (!DbAccess.Do_Transaction("SP_I_TBBOOKING", parameter1, user_id))
                        {
                            flag = false;
                            return flag;
                        }
                    }
                }
            }
            return flag;
        }

        /// <summary>
        /// 記錄流程編號
        /// </summary>
        /// <param name="booking_no">調用單號</param>
        /// <param name="flow_id">流程編號</param>
        /// <param name="user_id">異動者</param>
        /// <returns>成功或失敗</returns>
        [WebMethod]
        public bool UpdateBookingMasterFlowID(string booking_no, int flow_id, string user_id)
        {
            bool flag = true;
            flag = DbAccess.Do_Transaction("SP_U_TBBOOKING_MASTER_FLOW_ID", "<Data><FSBOOKING_NO>" + booking_no + "</FSBOOKING_NO><FNFLOW_ID>" + flow_id.ToString() + "</FNFLOW_ID></Data>", user_id);
            return flag;
        }

        /// <summary>
        /// 取得調用申請單節目查詢
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>調用申請單節目查詢結果</returns>
        [WebMethod]
        public string GetProgramSearch(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_PROG_BOOKING", parameter);
            return result;
        }

        /// <summary>
        /// 取得調用申請單節目查詢詳細資料
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>調用申請單節目查詢結果</returns>
        [WebMethod]
        public string GetProgramSearchDetailData(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_GET_PROGRAM_DETAIL_DATA", parameter);
            return result;
        }


        /// <summary>
        /// 查詢使用者調用歷史資料主檔
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>使用者調用歷史資料主檔查詢結果</returns>
        [WebMethod]
        public string GetUserBookingHistoryMaster(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_USER_BOOKING_HISTORY_MASTER", parameter);
            return result;
        }

        /// <summary>
        /// 查詢所有調用歷史資料主檔
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>所有調用歷史資料主檔查詢結果</returns>
        [WebMethod]
        public string GetBookingHistoryMaster(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_BOOKING_HISTORY_MASTER", parameter);
            return result;
        }


        /// <summary>
        /// 查詢使用者調用歷史資料明細
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>使用者調用歷史資料明細查詢結果</returns>
        [WebMethod]
        public string GetUserBookingHistoryDetail(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_USER_BOOKING_HISTORY_DETAIL", parameter);
            return result;
        }

        /// <summary>
        /// 查詢所有調用歷史資料明細
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>所有調用歷史資料明細查詢結果</returns>
        [WebMethod]
        public string GetBookingHistoryDetail(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_BOOKING_HISTORY_DETAIL", parameter);
            return result;
        }

        /// <summary>
        /// 查詢節目詳細資料
        /// </summary>
        /// <param name="sp_name">SP名稱</param>
        /// <param name="parameter">參數</param>
        /// <returns>節目詳細資料結果</returns>
        [WebMethod]
        public string GetProgramDetailData(string sp_name, string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query(sp_name, parameter);
            return result;
        }


        /// <summary>
        /// 查詢調用清單主檔
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>調用清單主檔</returns>
        [WebMethod]
        public string GetBookingMasterData(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_TBBOOKING_MASTER_BY_BOOKINGNO", parameter);
            return result;
        }

        /// <summary>
        /// 查詢調用清單明細
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>調用清單明細</returns>
        [WebMethod]
        public string GetBookingDetailData(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_TBBOOKING_DETAIL_BY_BOOKINGNO", parameter);
            return result;
        }

        /// <summary>
        /// 取得調用審核備註
        /// </summary>
        [WebMethod]
        public string GetBookingMemo(string parameter)
        {
            string result = "";
            result = DbAccess.Do_Query("SP_Q_TBBOOKING_DETAIL_MEMO", parameter);
            return result;
        }


        /// <summary>
        /// 更新調用審核備註
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>成功或失敗</returns>
        [WebMethod]
        public bool UpdateBookingMemo(string parameter, string user_id)
        {
            bool flag = true;
            flag = DbAccess.Do_Transaction("SP_U_TBBOOKING_DETAIL_MEMO", parameter, user_id);
            return flag;
        }


        /// <summary>
        /// 更新直接調用審核狀態
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>成功或失敗</returns>
        [WebMethod]
        public bool UpdateDirectBookingStatus(string _fsbooking_no, string user_id)
        {
            // 在此新增您的作業實作
            bool flag = true;
            flag = DbAccess.Do_Transaction("SP_U_TBBOOKING_DIRECT_STATUS", "<Data><FSBOOKING_NO>" + _fsbooking_no + "</FSBOOKING_NO></Data>", user_id);
            return flag;
        }

        /// <summary>
        /// 更新直屬主管審核狀態
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>成功或失敗</returns>
        [WebMethod]
        public bool UpdateBookingStatus(string parameter, string user_id)
        {
            // 在此新增您的作業實作
            bool flag = false;

            //記錄參數
            MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/WSBookingFunction/UpdateBookingStatus", Log.TRACKING_LEVEL.INFO, "參數:" + parameter);

            if (parameter != "")
            {
                try
                {
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.LoadXml(parameter);
                    XmlNodeList xNodeList = xDoc.GetElementsByTagName("Data");
                    if (xNodeList != null && xNodeList.Count > 0)
                    {
                        for (int i = 0; i <= xNodeList.Count - 1; i++)
                        {
                            XmlNode xNode = xNodeList[i];
                            string parameter1 = "<Data>" + xNode.InnerXml + "</Data>";
                            if (!DbAccess.Do_Transaction("SP_U_CHECK_BOOKING_STATUS", parameter1, user_id))
                            {
                                flag = false;
                                return flag;
                            }
                        }

                        flag = true;
                    }
                }
                catch (Exception ex)
                {
                    MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/WSBookingFunction/UpdateBookingStatus", Log.TRACKING_LEVEL.ERROR, "錯誤:" + ex.Message);
                }

            }
            return flag;
        }


        /// <summary>
        /// 更新檔案部門主管審核狀態
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>成功或失敗</returns>
        [WebMethod]
        public bool UpdateBookingStatusSupervisor(string parameter, string user_id)
        {
            // 在此新增您的作業實作
            bool flag = false;

            //記錄參數
            MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/WSBookingFunction/UpdateBookingStatusSupervisor", Log.TRACKING_LEVEL.INFO, "參數:" + parameter);

            if (parameter != "")
            {

                try
                {
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.LoadXml(parameter);
                    XmlNodeList xNodeList = xDoc.GetElementsByTagName("Data");
                    if (xNodeList != null && xNodeList.Count > 0)
                    {
                        for (int i = 0; i <= xNodeList.Count - 1; i++)
                        {
                            XmlNode xNode = xNodeList[i];
                            string parameter1 = "<Data>" + xNode.InnerXml + "</Data>";
                            if (!DbAccess.Do_Transaction("SP_U_CHECK_BOOKING_STATUS_SUPERVISOR", parameter1, user_id))
                            {
                                flag = false;
                                return flag;
                            }
                        }

                        flag = true;
                    }
                }
                catch (Exception ex)
                {
                    MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/WSBookingFunction/UpdateBookingStatusSupervisor", Log.TRACKING_LEVEL.ERROR, "錯誤:" + ex.Message);
                }

            }
            return flag;
        }


        /// <summary>
        /// 查詢調用清單外購數量
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>調用清單外購數量</returns>
        [WebMethod]
        public string GetBookingOutBuyCount(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_CHECK_BOOKING_OUT_BUY", parameter);
            return result;
        }

        /// <summary>
        /// 查詢調用清單宏觀數量
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>調用清單宏觀數量</returns>
        [WebMethod]
        public string GetBookingInternationCount(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_GET_BOOKING_FILE_INTERNATION_COUNT", parameter);
            return result;
        }

        /// <summary>
        /// 調用清單是否需要外購審核
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>調用清單是否需要外購審核</returns>
        [WebMethod]
        public string CheckBookingNeedCheckOutBuy(string _fsbooking_no)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_GET_BOOKING_NEED_CHECK_OUTBUY", "<Data><FSBOOKING_NO>" + _fsbooking_no + "</FSBOOKING_NO></Data>");
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(result);
            XmlNodeList xNodeList = xDoc.GetElementsByTagName("Data");
            if (xNodeList != null && xNodeList.Count > 0)
            {
                XmlNode xNode = xNodeList[0];
                result = xNode.ChildNodes[0].FirstChild.Value;
            }
            return result;
        }

        /// <summary>
        /// 調用清單是否有宏觀
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>調用清單是否有宏觀</returns>
        [WebMethod]
        public string CheckBookingNeedCheckInternation(string _fsbooking_no)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_GET_BOOKING_NEED_CHECK_INTERNATION", "<Data><FSBOOKING_NO>" + _fsbooking_no + "</FSBOOKING_NO></Data>");
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(result);
            XmlNodeList xNodeList = xDoc.GetElementsByTagName("Data");
            if (xNodeList != null && xNodeList.Count > 0)
            {
                XmlNode xNode = xNodeList[0];
                result = xNode.ChildNodes[0].FirstChild.Value;
            }
            return result;
        }


        /// <summary>
        /// 查詢調用清單外購明細
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>調用清單外購明細</returns>
        [WebMethod]
        public string GetBookingDetailOutBuyData(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_TBBOOKING_DETAIL_OUTBUY_BY_BOOKINGNO", parameter);
            return result;
        }


        /// <summary>
        /// 更新外購審核狀態
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>成功或失敗</returns>
        [WebMethod]
        public bool UpdateBookingOutBuyStatus(string parameter, string user_id)
        {
            // 在此新增您的作業實作
            bool flag = true;
            if (parameter != "")
            {
                XmlDocument xDoc = new XmlDocument();
                xDoc.LoadXml(parameter);
                XmlNodeList xNodeList = xDoc.GetElementsByTagName("Data");
                if (xNodeList != null && xNodeList.Count > 0)
                {
                    for (int i = 0; i <= xNodeList.Count - 1; i++)
                    {
                        XmlNode xNode = xNodeList[i];
                        string parameter1 = "<Data>" + xNode.InnerXml + "</Data>";
                        if (!DbAccess.Do_Transaction("SP_U_CHECK_BOOKING_OUTBUY_STATUS", parameter1, user_id))
                        {
                            flag = false;
                            return flag;
                        }
                    }
                }
            }

            return flag;
        }

        /// <summary>
        /// 送轉檔
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>成功或失敗</returns>
        [WebMethod]
        public string SendToTranscode(string booking_no, string user_id)
        {
            // 在此新增您的作業實作

            //更新調用清單主檔審核狀態
            DbAccess.Do_Transaction("SP_U_TBBOOKING_MASTER_CHECK_STATUS", "<Data><FSBOOKING_NO>" + booking_no.Trim() + "</FSBOOKING_NO></Data>", user_id);

            bool flag = true;
            //取得已審核並且合核可的
            string checkedResult = DbAccess.Do_Query("SP_Q_GET_CHECKED_BOOKING", "<Data><FSBOOKING_NO>" + booking_no.Trim() + "</FSBOOKING_NO></Data>");
            XmlDocument xDocCheckedResult = new XmlDocument();
            xDocCheckedResult.LoadXml(checkedResult);
            XmlNodeList xNodeListCheckedResult = xDocCheckedResult.GetElementsByTagName("Data");
            if (xNodeListCheckedResult != null && xNodeListCheckedResult.Count > 0)
            {
                for (int i = 0; i <= xNodeListCheckedResult.Count - 1; i++)
                {
                    XmlNode xNode = xNodeListCheckedResult[i];
                    //判斷檔案類型(新聞或節目)
                    string _fsbookingNo = (xNode.ChildNodes[0].FirstChild != null ? xNode.ChildNodes[0].FirstChild.Value : "");
                    string _fsseq = (xNode.ChildNodes[1].FirstChild != null ? xNode.ChildNodes[1].FirstChild.Value : "");
                    string _fsuser_id = (xNode.ChildNodes[2].FirstChild != null ? xNode.ChildNodes[2].FirstChild.Value : "");
                    string _fdbooking_date = (xNode.ChildNodes[3].FirstChild != null ? xNode.ChildNodes[3].FirstChild.Value : "");
                    string _fslogo_info = (xNode.ChildNodes[4].FirstChild != null ? xNode.ChildNodes[4].FirstChild.Value : "");
                    string _fsfile_no = (xNode.ChildNodes[5].FirstChild != null ? xNode.ChildNodes[5].FirstChild.Value : "");
                    string _fsbegin_timecode = (xNode.ChildNodes[6].FirstChild != null ? xNode.ChildNodes[6].FirstChild.Value : "");
                    string _fsend_timecode = (xNode.ChildNodes[7].FirstChild != null ? xNode.ChildNodes[7].FirstChild.Value : "");
                    string _fsfile_category = (xNode.ChildNodes[8].FirstChild != null ? xNode.ChildNodes[8].FirstChild.Value : "");
                    string _fsfile_output_info = (xNode.ChildNodes[9].FirstChild != null ? xNode.ChildNodes[9].FirstChild.Value : "");
                    string _fcoriginal = (xNode.ChildNodes[10].FirstChild != null ? xNode.ChildNodes[10].FirstChild.Value : "N");
                    string _fcall = (xNode.ChildNodes[11].FirstChild != null ? xNode.ChildNodes[11].FirstChild.Value : "N");
                    string _fcinterplay = (xNode.ChildNodes[12].FirstChild != null ? xNode.ChildNodes[12].FirstChild.Value : "N");

                    string sourcePath = "";
                    string destPath = "";
                    string guid = Guid.NewGuid().ToString().Replace("-", "").Replace("_", "");
                    string fsPROGNAME = string.Empty;
                    SysConfig sysconfig = new SysConfig();
                    try
                    {
                        if (_fsfile_category == "1")
                        {
                            //判斷為HD or SD
                            string _fsfile_type = "";
                            string p1 = DbAccess.Do_Query("SP_Q_GET_FILE_TYPE_BY_FILENO", "<Data><FSFILE_NO>" + _fsfile_no + "</FSFILE_NO></Data>");
                            XmlDocument xDoc1 = new XmlDocument();
                            xDoc1.LoadXml(p1);
                            XmlNodeList xNodeList1 = xDoc1.GetElementsByTagName("Data");
                            if (xNodeList1 != null && xNodeList1.Count > 0)
                            {
                                XmlNode xNode1 = xNodeList1[0];
                                _fsfile_type = (xNode1.ChildNodes[0].FirstChild != null ? xNode1.ChildNodes[0].FirstChild.Value : "");
                            }


                            //片庫送至轉檔中心
                            ServiceTranscode Tsobj = new ServiceTranscode();
                            DoIngestXml Insobj = new DoIngestXml();
                            ServiceTSM tsm = new ServiceTSM();
                            sourcePath = tsm.GetSambaFilePathByFileID(_fsfile_no, WS.WSTSM.ServiceTSM.FileID_MediaType.HiVideo);

                            ////2014/10/29 david 增加節目名稱在檔名上
                            ////取得節目名稱
                            //string fsPROGNAME_XML = DbAccess.Do_Query("SP_Q_TBPROGNAME_BY_FSFILENO", "<Data><FCTYPE>1</FCTYPE><FSFILE_NO>" + _fsfile_no + "</FSFILE_NO></Data>");
                            //XmlDocument xDoc_PROGNAME = new XmlDocument();
                            //xDoc_PROGNAME.LoadXml(fsPROGNAME_XML);
                            //XmlNodeList xNodeList_PROGNAME = xDoc_PROGNAME.GetElementsByTagName("Data");
                            //if (xNodeList_PROGNAME != null && xNodeList_PROGNAME.Count > 0)
                            //{
                            //    XmlNode xNode1 = xNodeList_PROGNAME[0];
                            //    fsPROGNAME = (xNode1.ChildNodes[0].FirstChild != null ? xNode1.ChildNodes[0].FirstChild.Value : "");
                            //}

                            destPath = sysconfig.sysConfig_Read("/ServerConfig/BOOKING_RETRIEVE") + _fsuser_id + "\\" + _fsbookingNo + "\\" + System.IO.Path.GetFileNameWithoutExtension(sourcePath) + "_" + _fsseq + System.IO.Path.GetExtension(sourcePath);

                            //呼叫轉檔
                            ImpersonateUser iu = new ImpersonateUser();
                            iu.Impersonate(@"pts.org.tw", @"mamsystem", @"p@ssw0rd");

                            Directory.CreateDirectory(System.IO.Path.GetDirectoryName(destPath));

                            if (!Directory.Exists(sysconfig.sysConfig_Read("/ServerConfig/BOOKING_RETRIEVE") + _fsuser_id))
                            {
                                SetPermissions(sysconfig.sysConfig_Read("/ServerConfig/BOOKING_RETRIEVE") + _fsuser_id, @"pts\" + _fsuser_id);
                                SetPermissions(sysconfig.sysConfig_Read("/ServerConfig/BOOKING_RETRIEVE") + _fsuser_id, @"pts\agility");
                            }

                            iu.Undo();


                            //逐筆塞入參數
                            Insobj.IsFullTranscode = _fcall;            //是否為整段調用
                            Insobj.IsNativeFormat = _fcoriginal;        //是否為原始格式
                            Insobj.CaptureKeyframe = "N";               //關鍵影格  
                            Insobj.CreateLowRes = "N";                 //低解            
                            Insobj.DestinationTo = destPath;          //目的地(TSM路徑_高解)
                            if (_fslogo_info != "")
                            {
                                Insobj.DisplayLogo = "Y";               //顯示LOGO
                            }
                            else
                            {
                                Insobj.DisplayLogo = "N";               //不顯示LOGO
                            }
                            Insobj.EncoderProfliePath = (string.IsNullOrEmpty(_fsfile_output_info) ? "原生格式" : _fsfile_output_info);    //轉檔Profile位置
                            Insobj.StartTimecode = _fsbegin_timecode;              //起始TiemeCode
                            Insobj.EndTimecode = _fsend_timecode;               //結束TiemeCode
                            Insobj.LogoProfilePath = _fslogo_info;
                            Insobj.LowResDir = "";  //低解目錄(TSM路徑_低解)
                            Insobj.Note = "MAM-調用清單編號： " + _fsbookingNo + "，" + _fsuser_id + "調用";          //備註
                            Insobj.PostbackAddress = sysconfig.sysConfig_Read("/ServerConfig/AshxHandler/ANS_BOK");  //結束處理函式
                            Insobj.SourceFrom = sourcePath;             //來源
                            Insobj.TransCodeType = "FILE";
                            Insobj.CreateHightRes = "Y";

                            if (_fsfile_type == "01")
                            {
                                Insobj.SourceFormat = "SD";                //SD來源格式
                                Insobj.DestinationFormat = "SD";            //SD目的格式
                            }
                            else if (_fsfile_type == "02")
                            {
                                Insobj.SourceFormat = "HD";                //HD來源格式
                                Insobj.DestinationFormat = "HD";            //HD目的格式
                            }


                            long JobID = Tsobj.DoTranscode(Insobj);
                            //更新轉檔狀態為"R"
                            DbAccess.Do_Transaction("SP_U_TBBOOKING_DETAIL_FILE_STATUS_BY_BOOKINGNO", "<Data><FSBOOKING_NO>" + _fsbookingNo + "</FSBOOKING_NO><FSSEQ>" + _fsseq + "</FSSEQ><FCFILE_TRANSCODE_STATUS>R</FCFILE_TRANSCODE_STATUS></Data>", user_id);
                            if (JobID == -1)
                            {
                                //更新轉檔狀態為"F"
                                DbAccess.Do_Transaction("SP_U_TBBOOKING_DETAIL_FILE_STATUS_BY_BOOKINGNO", "<Data><FSBOOKING_NO>" + _fsbookingNo + "</FSBOOKING_NO><FSSEQ>" + _fsseq + "</FSSEQ><FCFILE_TRANSCODE_STATUS>F</FCFILE_TRANSCODE_STATUS></Data>", user_id);
                                continue;
                            }
                            else
                            {
                                //更新GUID&JOBID
                                string updateParameter = "";
                                updateParameter += "<Data>";
                                updateParameter += "<FSBOOKING_NO>" + booking_no + "</FSBOOKING_NO>";
                                updateParameter += "<FSSEQ>" + _fsseq + "</FSSEQ>";
                                updateParameter += "<FSGUID>" + guid + "</FSGUID>";
                                updateParameter += "<FSJOB_ID>" + JobID + "</FSJOB_ID>";
                                updateParameter += "<FCFILE_TRANSCODE_STATUS></FCFILE_TRANSCODE_STATUS>";
                                updateParameter += "</Data>";
                                DbAccess.Do_Transaction("SP_U_TBBOOKING_DETAIL_GUID_JOBID", updateParameter, user_id);
                            }

                        }
                        else if (_fsfile_category == "2")
                        {
                            ImpersonateUser iu = new ImpersonateUser();
                            try
                            {

                                iu.Impersonate(@"pts.org.tw", @"mamsystem", @"p@ssw0rd");
                                //更新轉檔狀態為"R"
                                DbAccess.Do_Transaction("SP_U_TBBOOKING_DETAIL_FILE_STATUS_BY_BOOKINGNO", "<Data><FSBOOKING_NO>" + _fsbookingNo + "</FSBOOKING_NO><FSSEQ>" + _fsseq + "</FSSEQ><FCFILE_TRANSCODE_STATUS>R</FCFILE_TRANSCODE_STATUS></Data>", user_id);
                                WS.WSTSM.ServiceTSM tsm = new WS.WSTSM.ServiceTSM();
                                sourcePath = tsm.GetSambaFilePathByFileID(_fsfile_no, WS.WSTSM.ServiceTSM.FileID_MediaType.Audio);

                                //2014/10/29 david 增加節目名稱在檔名上
                                //取得節目名稱
                                string fsPROGNAME_XML = DbAccess.Do_Query("SP_Q_TBPROGNAME_BY_FSFILENO", "<Data><FCTYPE>2</FCTYPE><FSFILE_NO>" + _fsfile_no + "</FSFILE_NO></Data>");
                                XmlDocument xDoc_PROGNAME = new XmlDocument();
                                xDoc_PROGNAME.LoadXml(fsPROGNAME_XML);
                                XmlNodeList xNodeList_PROGNAME = xDoc_PROGNAME.GetElementsByTagName("Data");
                                if (xNodeList_PROGNAME != null && xNodeList_PROGNAME.Count > 0)
                                {
                                    XmlNode xNode1 = xNodeList_PROGNAME[0];
                                    fsPROGNAME = (xNode1.ChildNodes[0].FirstChild != null ? xNode1.ChildNodes[0].FirstChild.Value : "");
                                }


                                destPath = sysconfig.sysConfig_Read("/ServerConfig/BOOKING_RETRIEVE") + _fsuser_id + "\\" + _fsbookingNo + "\\" + fsPROGNAME + "_" + System.IO.Path.GetFileNameWithoutExtension(sourcePath) + "_" + _fsseq + System.IO.Path.GetExtension(sourcePath);

                                Directory.CreateDirectory(System.IO.Path.GetDirectoryName(destPath));
                                if (!Directory.Exists(sysconfig.sysConfig_Read("/ServerConfig/BOOKING_RETRIEVE") + _fsuser_id))
                                {
                                    SetPermissions(sysconfig.sysConfig_Read("/ServerConfig/BOOKING_RETRIEVE") + _fsuser_id, @"pts\" + _fsuser_id);
                                    SetPermissions(sysconfig.sysConfig_Read("/ServerConfig/BOOKING_RETRIEVE") + _fsuser_id, @"pts\agility");
                                }

                                File.Copy(sourcePath, destPath, true);
                                //更新檔案狀態為"Y"
                                DbAccess.Do_Transaction("SP_U_TBBOOKING_DETAIL_FILE_STATUS_BY_BOOKINGNO", "<Data><FSBOOKING_NO>" + _fsbookingNo + "</FSBOOKING_NO><FSSEQ>" + _fsseq + "</FSSEQ><FCFILE_TRANSCODE_STATUS>Y</FCFILE_TRANSCODE_STATUS></Data>", user_id);


                            }
                            catch (Exception ex)
                            {
                                //更新檔案狀態為"F"
                                DbAccess.Do_Transaction("SP_U_TBBOOKING_DETAIL_FILE_STATUS_BY_BOOKINGNO", "<Data><FSBOOKING_NO>" + _fsbookingNo + "</FSBOOKING_NO><FSSEQ>" + _fsseq + "</FSSEQ><FCFILE_TRANSCODE_STATUS>F</FCFILE_TRANSCODE_STATUS></Data>", user_id);
                                MAM_PTS_DLL.Log.AppendTrackingLog("WSBookingFunction.asmx/SendToTranscode", Log.TRACKING_LEVEL.ERROR, ex.Message);
                            }
                            finally
                            {
                                iu.Undo();
                            }

                        }
                        else if (_fsfile_category == "3")
                        {
                            ImpersonateUser iu = new ImpersonateUser();
                            try
                            {
                                //更新轉檔狀態為"R"

                                iu.Impersonate(@"pts.org.tw", @"mamsystem", @"p@ssw0rd");

                                DbAccess.Do_Transaction("SP_U_TBBOOKING_DETAIL_FILE_STATUS_BY_BOOKINGNO", "<Data><FSBOOKING_NO>" + _fsbookingNo + "</FSBOOKING_NO><FSSEQ>" + _fsseq + "</FSSEQ><FCFILE_TRANSCODE_STATUS>R</FCFILE_TRANSCODE_STATUS></Data>", user_id);
                                WS.WSTSM.ServiceTSM tsm = new WS.WSTSM.ServiceTSM();
                                sourcePath = tsm.GetSambaFilePathByFileID(_fsfile_no, WS.WSTSM.ServiceTSM.FileID_MediaType.Photo);

                                //2014/10/29 david 增加節目名稱在檔名上
                                //取得節目名稱
                                string fsPROGNAME_XML = DbAccess.Do_Query("SP_Q_TBPROGNAME_BY_FSFILENO", "<Data><FCTYPE>3</FCTYPE><FSFILE_NO>" + _fsfile_no + "</FSFILE_NO></Data>");
                                XmlDocument xDoc_PROGNAME = new XmlDocument();
                                xDoc_PROGNAME.LoadXml(fsPROGNAME_XML);
                                XmlNodeList xNodeList_PROGNAME = xDoc_PROGNAME.GetElementsByTagName("Data");
                                if (xNodeList_PROGNAME != null && xNodeList_PROGNAME.Count > 0)
                                {
                                    XmlNode xNode1 = xNodeList_PROGNAME[0];
                                    fsPROGNAME = (xNode1.ChildNodes[0].FirstChild != null ? xNode1.ChildNodes[0].FirstChild.Value : "");
                                }

                                destPath = sysconfig.sysConfig_Read("/ServerConfig/BOOKING_RETRIEVE") + _fsuser_id + "\\" + _fsbookingNo + "\\" + fsPROGNAME + "_" + System.IO.Path.GetFileNameWithoutExtension(sourcePath) + "_" + _fsseq + System.IO.Path.GetExtension(sourcePath);

                                Directory.CreateDirectory(System.IO.Path.GetDirectoryName(destPath));
                                if (!Directory.Exists(sysconfig.sysConfig_Read("/ServerConfig/BOOKING_RETRIEVE") + _fsuser_id))
                                {
                                    SetPermissions(sysconfig.sysConfig_Read("/ServerConfig/BOOKING_RETRIEVE") + _fsuser_id, @"pts\" + _fsuser_id);
                                    SetPermissions(sysconfig.sysConfig_Read("/ServerConfig/BOOKING_RETRIEVE") + _fsuser_id, @"pts\agility");
                                }

                                File.Copy(sourcePath, destPath, true);
                                //更新檔案狀態為"Y"
                                DbAccess.Do_Transaction("SP_U_TBBOOKING_DETAIL_FILE_STATUS_BY_BOOKINGNO", "<Data><FSBOOKING_NO>" + _fsbookingNo + "</FSBOOKING_NO><FSSEQ>" + _fsseq + "</FSSEQ><FCFILE_TRANSCODE_STATUS>Y</FCFILE_TRANSCODE_STATUS></Data>", user_id);


                            }
                            catch (Exception ex)
                            {
                                //更新檔案狀態為"F"
                                DbAccess.Do_Transaction("SP_U_TBBOOKING_DETAIL_FILE_STATUS_BY_BOOKINGNO", "<Data><FSBOOKING_NO>" + _fsbookingNo + "</FSBOOKING_NO><FSSEQ>" + _fsseq + "</FSSEQ><FCFILE_TRANSCODE_STATUS>F</FCFILE_TRANSCODE_STATUS></Data>", user_id);
                                MAM_PTS_DLL.Log.AppendTrackingLog("WSBookingFunction.asmx/SendToTranscode", Log.TRACKING_LEVEL.ERROR, ex.Message);
                            }
                            finally
                            {
                                iu.Undo();
                            }
                        }
                        else if (_fsfile_category == "4")
                        {
                            ImpersonateUser iu = new ImpersonateUser();
                            try
                            {
                                //更新轉檔狀態為"R"

                                iu.Impersonate(@"pts.org.tw", @"mamsystem", @"p@ssw0rd");

                                DbAccess.Do_Transaction("SP_U_TBBOOKING_DETAIL_FILE_STATUS_BY_BOOKINGNO", "<Data><FSBOOKING_NO>" + _fsbookingNo + "</FSBOOKING_NO><FSSEQ>" + _fsseq + "</FSSEQ><FCFILE_TRANSCODE_STATUS>R</FCFILE_TRANSCODE_STATUS></Data>", user_id);
                                WS.WSTSM.ServiceTSM tsm = new WS.WSTSM.ServiceTSM();
                                sourcePath = tsm.GetSambaFilePathByFileID(_fsfile_no, WS.WSTSM.ServiceTSM.FileID_MediaType.Doc);

                                //2014/10/29 david 增加節目名稱在檔名上
                                //取得節目名稱
                                string fsPROGNAME_XML = DbAccess.Do_Query("SP_Q_TBPROGNAME_BY_FSFILENO", "<Data><FCTYPE>4</FCTYPE><FSFILE_NO>" + _fsfile_no + "</FSFILE_NO></Data>");
                                XmlDocument xDoc_PROGNAME = new XmlDocument();
                                xDoc_PROGNAME.LoadXml(fsPROGNAME_XML);
                                XmlNodeList xNodeList_PROGNAME = xDoc_PROGNAME.GetElementsByTagName("Data");
                                if (xNodeList_PROGNAME != null && xNodeList_PROGNAME.Count > 0)
                                {
                                    XmlNode xNode1 = xNodeList_PROGNAME[0];
                                    fsPROGNAME = (xNode1.ChildNodes[0].FirstChild != null ? xNode1.ChildNodes[0].FirstChild.Value : "");
                                }

                                destPath = sysconfig.sysConfig_Read("/ServerConfig/BOOKING_RETRIEVE") + _fsuser_id + "\\" + _fsbookingNo + "\\" + fsPROGNAME + "_" + System.IO.Path.GetFileNameWithoutExtension(sourcePath) + "_" + _fsseq + System.IO.Path.GetExtension(sourcePath);

                                Directory.CreateDirectory(System.IO.Path.GetDirectoryName(destPath));
                                if (!Directory.Exists(sysconfig.sysConfig_Read("/ServerConfig/BOOKING_RETRIEVE") + _fsuser_id))
                                {
                                    SetPermissions(sysconfig.sysConfig_Read("/ServerConfig/BOOKING_RETRIEVE") + _fsuser_id, @"pts\" + _fsuser_id);
                                    SetPermissions(sysconfig.sysConfig_Read("/ServerConfig/BOOKING_RETRIEVE") + _fsuser_id, @"pts\agility");
                                }

                                File.Copy(sourcePath, destPath, true);
                                //更新檔案狀態為"Y"
                                DbAccess.Do_Transaction("SP_U_TBBOOKING_DETAIL_FILE_STATUS_BY_BOOKINGNO", "<Data><FSBOOKING_NO>" + _fsbookingNo + "</FSBOOKING_NO><FSSEQ>" + _fsseq + "</FSSEQ><FCFILE_TRANSCODE_STATUS>Y</FCFILE_TRANSCODE_STATUS></Data>", user_id);

                            }
                            catch (Exception ex)
                            {
                                //更新檔案狀態為"F"
                                DbAccess.Do_Transaction("SP_U_TBBOOKING_DETAIL_FILE_STATUS_BY_BOOKINGNO", "<Data><FSBOOKING_NO>" + _fsbookingNo + "</FSBOOKING_NO><FSSEQ>" + _fsseq + "</FSSEQ><FCFILE_TRANSCODE_STATUS>F</FCFILE_TRANSCODE_STATUS></Data>", user_id);
                                MAM_PTS_DLL.Log.AppendTrackingLog("WSBookingFunction.asmx/SendToTranscode", Log.TRACKING_LEVEL.ERROR, ex.Message);
                            }
                            finally
                            {
                                iu.Undo();
                            }

                        }
                        else if (_fsfile_category == "5")
                        {
                            //新聞送到PTS_MAM.TBVIDEO_RETRIVE

                            //取得frame與duration
                            int _beginFrame = TimeCodeCalc.timecodetoframe(_fsbegin_timecode);
                            int _endFrame = TimeCodeCalc.timecodetoframe(_fsend_timecode);
                            string fcall = "N";
                            if (_beginFrame == 0)
                            {
                                string durationResult = DbAccess.Do_Query("SP_Q_GET_NEWS_DURATION", "<Data><FSFILE_NO>" + _fsfile_no + "</FSFILE_NO></Data>");
                                XmlDocument xDocDurationResult = new XmlDocument();
                                xDocDurationResult.LoadXml(durationResult);
                                XmlNodeList xNodeListDurationdResult = xDocDurationResult.GetElementsByTagName("Data");
                                if (xNodeListDurationdResult != null && xNodeListDurationdResult.Count > 0)
                                {
                                    XmlNode xNodeDuration = xNodeListDurationdResult[0];
                                    int _fnduration = Int32.Parse(xNodeDuration.FirstChild.ChildNodes[0].Value);
                                    if (Math.Abs(_endFrame - _fnduration) < 10)
                                    {
                                        fcall = "Y";
                                    }
                                }
                            }

                            //新增至新聞片庫
                            string insertParameter = "";
                            insertParameter += "<Data>";
                            insertParameter += "<FSUSER_ID>" + _fsuser_id + "</FSUSER_ID>";
                            insertParameter += "<FSFILE_NO>" + _fsfile_no + "</FSFILE_NO>";
                            insertParameter += "<FDDATE>" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff") + "</FDDATE>";
                            insertParameter += "<FNSTARTFRAME>" + _beginFrame.ToString() + "</FNSTARTFRAME>";
                            insertParameter += "<FNENDFRAME>" + _endFrame.ToString() + "</FNENDFRAME>";
                            insertParameter += "<FCSTATUS>N</FCSTATUS>";
                            insertParameter += "<FNPER>0</FNPER>";
                            insertParameter += "<FSFILENAME></FSFILENAME>";
                            insertParameter += "<FSMESSAGE>Prepare</FSMESSAGE>";
                            insertParameter += "<FSGUID>" + guid + "</FSGUID>";
                            insertParameter += "<FCALL>" + fcall + "</FCALL>";
                            insertParameter += "<FDLASTACCESS>" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff") + "</FDLASTACCESS>";
                            insertParameter += "<FCINTERPLAY>" + _fcinterplay + "</FCINTERPLAY>";
                            insertParameter += "</Data>";

                            DbAccess.Do_Transaction_News("SP_I_NEWS_TBVIDEO_RETRIVE", insertParameter, user_id);

                            //更新GUID
                            string updateParameter = "";
                            updateParameter += "<Data>";
                            updateParameter += "<FSBOOKING_NO>" + booking_no + "</FSBOOKING_NO>";
                            updateParameter += "<FSSEQ>" + _fsseq + "</FSSEQ>";
                            updateParameter += "<FSGUID>" + guid + "</FSGUID>";
                            updateParameter += "<FSJOB_ID></FSJOB_ID>";
                            updateParameter += "<FCFILE_TRANSCODE_STATUS>A</FCFILE_TRANSCODE_STATUS>";
                            updateParameter += "</Data>";
                            DbAccess.Do_Transaction("SP_U_TBBOOKING_DETAIL_GUID_JOBID", updateParameter, user_id);

                        }

                        //完成調用檔案處理後，寄信給調用者
                        //string user_mail = DbAccess.Do_Query("SP_Q_BOOKING_USER_EMAIL", "<Data><FSBOOKING_NO>" + booking_no + "</FSBOOKING_NO><FSUSER_ID>" + _fsuser_id + "</FSUSER_ID><FSJOB_ID>0</FSJOB_ID></Data>");
                        //XmlDocument xDocUserMail = new XmlDocument();
                        //xDocUserMail.LoadXml(user_mail);
                        //if (xDocUserMail.GetElementsByTagName("Data").Count > 0)
                        //{
                        //    XmlNodeList xNodeListUserMail = xDocUserMail.GetElementsByTagName("Data");
                        //    XmlNode xNodeUserMail = xNodeListUserMail[0];
                        //    if (xNodeUserMail.ChildNodes[0].FirstChild.Value != "")
                        //    {
                        //        string mailContent = "調用檔案轉檔完成!";
                        //        Protocol.SendEmail(xNodeUserMail.ChildNodes[0].FirstChild.Value, "調用檔案轉檔完成!", mailContent);
                        //    }
                        //}

                    }
                    catch (Exception ex)
                    {
                        MAM_PTS_DLL.Log.AppendTrackingLog("WSBookingFunction.asmx/SendToTranscode", Log.TRACKING_LEVEL.ERROR, ex.Message);
                    }


                }
            }

            return booking_no;
        }

        /// <summary>
        /// 取得新聞片庫低解檔案路徑
        /// </summary>
        /// <param name="fsfile_no">檔案編號</param>
        /// <returns>路徑</returns>
        [WebMethod]
        public string GetNewsLVPath(string fsfile_no)
        {
            // 在此新增您的作業實作

            string year = fsfile_no.Substring(0, 4);
            string month = fsfile_no.Substring(4, 2);
            string day = fsfile_no.Substring(6, 2);
            Properties.Settings setting = new Properties.Settings();
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string path = SettingObj.sysConfig_Read("/ServerConfig/NEWSPath") + "/LV/" + year + "/" + month + "/" + day + "/" + fsfile_no + ".mov";
            return path;
        }

        /// <summary>
        /// 取得新聞片庫KeyFrame路徑
        /// </summary>
        /// <param name="fsfile_no">檔案編號</param>
        /// <returns>路徑Class</returns>
        [WebMethod]
        public List<ServiceTSM.KeyframeInfo> GetNewsKeyFramePath(string fsfile_no)
        {
            // 在此新增您的作業實作
            List<ServiceTSM.KeyframeInfo> newsKeyFrameList = new List<ServiceTSM.KeyframeInfo>();
            try
            {
                string result = DbAccess.Do_Query("SP_Q_GET_NEWS_KEYFRAME", "<Data><FSVIDEO_NO>" + fsfile_no + "</FSVIDEO_NO></Data>");
                XmlDocument xDocKeyframeResult = new XmlDocument();
                xDocKeyframeResult.LoadXml(result);
                XmlNodeList xNodeListKeyFrameResult = xDocKeyframeResult.GetElementsByTagName("Data");
                Properties.Settings setting = new Properties.Settings();

                if (xNodeListKeyFrameResult != null && xNodeListKeyFrameResult.Count > 0)
                {
                    MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
                    for (int i = 0; i <= xNodeListKeyFrameResult.Count - 1; i++)
                    {
                        ServiceTSM.KeyframeInfo k = new ServiceTSM.KeyframeInfo();
                        k.KeyframeHttpPath = "";
                        k.KeyframeDescription = "";
                        if (xNodeListKeyFrameResult[i].ChildNodes[0].FirstChild != null)
                        {
                            string path = SettingObj.sysConfig_Read("/ServerConfig/NEWSPath") + "/AirMAM_File/" + fsfile_no.Substring(0, 11) + "/KeyFrame/" + xNodeListKeyFrameResult[i].ChildNodes[0].FirstChild.Value + ".jpg";

                            k.KeyframeHttpPath = path;
                        }

                        k.KeyframeDescription = "";


                        newsKeyFrameList.Add(k);
                    }
                }
                return newsKeyFrameList;
            }
            catch (Exception ex)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSBookingFunction.asmx/GetNewsKeyFramePath", Log.TRACKING_LEVEL.ERROR, ex.Message);
                return null;
            }
        }


        /// <summary>
        /// 取得新聞影片開始Frame
        /// </summary>
        /// <param name="fsfile_no">檔案編號</param>
        /// <returns>路徑</returns>
        [WebMethod]
        public string GetNewsStartFrame(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_GET_NEWS_VIDEO_START_FRAME", parameter);
            return result;
        }

        /// <summary>
        /// 取得片庫影片開始TimeCode
        /// </summary>
        /// <param name="fsfile_no">檔案編號</param>
        /// <returns>路徑</returns>
        [WebMethod]
        public string GetFileStartTimeCode(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_GET_FILE_START_TIMECODE", parameter);
            return result;
        }

        /// <summary>
        /// 查詢影片起迄檔
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>起迄點</returns>
        [WebMethod]
        public string GetVideoTimeCode(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_GETVIDEO_TIMECODE", parameter);
            return result;
        }

        /// <summary>
        /// 調用申請單是否有勾選檔案
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>個數</returns>
        [WebMethod]
        public string GetProgramPreBookingCount(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_GET_PROGRAM_PREBOOKING_COUNT", parameter);
            return result;
        }

        //列印調用清單報告
        [WebMethod]
        public string GetUserBookingReport(string sDate, string eData, string CREATED_BY, string strSessionID)
        {
            String strURL = "";  //要傳回去的URL

            //取列印報表的系統設定檔
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string reportSrv = SettingObj.sysConfig_Read("/ServerConfig/ReportServer");
            //string MainWeb = SettingObj.sysConfig_Read("/ServerConfig/MainWeb");

            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("SDATE", sDate);                 //開始時間
            source.Add("EDATE", eData);                 //結束時間
            source.Add("QUERY_BY", CREATED_BY);          //查詢者
            source.Add("FSSESSION_ID", strSessionID);    //SessionID

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_R_TBBOOKING_01_QRY", source, out sqlResult))
                return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count > 0)
            {
                if (sqlResult[0]["QUERY_KEY"].ToString().StartsWith("ERROR:SESSION_TIMEOUT"))
                    return "錯誤，登入時間過久，請重新登入後才能啟動報表功能！";
                else if (sqlResult[0]["QUERY_KEY"].ToString().StartsWith("ERROR:SESSION_NOTEXIST"))
                    return "錯誤，使用者未正常登入，請重新登入後才能啟動報表功能！";
                else if (sqlResult[0]["QUERY_KEY"].ToString().StartsWith("ERROR"))
                    return "";
                else
                {
                    strURL = reportSrv + "RPT_TBBOOKING_01&QUERY_KEY=" + sqlResult[0]["QUERY_KEY"] + "&rc:parameters=false&rs:Command=Render";
                    return Server.UrlEncode(strURL);
                }
            }
            else
                return "";
        }


        /// <summary>
        /// 匯出檢索結果
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>個數</returns>
        [WebMethod]
        public void ExportSearchResult(string parameter, string user_id)
        {
            // 在此新增您的作業實作
            try
            {

                MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
                string[] fssys_id = parameter.Split(';');
                string url = SettingObj.sysConfig_Read("/ServerConfig/ExportSearchResultRealFilePath") + user_id + @"\SearchExport.csv";
                string url_complete = SettingObj.sysConfig_Read("/ServerConfig/ExportSearchResultRealFilePath") + user_id + @"\SearchExport.complete";

                //string url = "D:\\PTS\\數位片庫\\CSV\\" + user_id + "\\SearchExport.csv";
                //string url_complete = "D:\\PTS\\數位片庫\\CSV\\" + user_id + @"\SearchExport.complete";

                if (!Directory.Exists(System.IO.Path.GetDirectoryName(url)))
                {
                    Directory.CreateDirectory(System.IO.Path.GetDirectoryName(url));
                }

                if (File.Exists(url))
                {
                    File.Delete(url);
                }
                if (File.Exists(url_complete))
                {
                    File.Delete(url_complete);
                }

                //檢索
                string result = "";
                int totalCount = 0;
                Int32.TryParse(wsSearchService.SearchCount(parameter), out totalCount);

                if (totalCount > 0)
                {
                    StreamWriter sw = new StreamWriter(url, false, System.Text.Encoding.UTF8);
                    sw.WriteLine("檔案編號,標題,內容描述,檔案類型,主檔名稱,子集名稱,集數,台別,建檔者,建檔日期,異動者,異動日期");

                    int Page = (totalCount % 50 == 0 ? (totalCount / 50) - 1 : totalCount / 50);



                    for (int c = 0; c <= Page; c++)
                    {

                        if (c > 0)
                        {
                            parameter = parameter.Replace("<StartPoint>" + ((c - 1) * 50).ToString() + "</StartPoint>", "<StartPoint>" + ((c * 50).ToString() + "</StartPoint>"));
                        }

                        result = wsSearchService.Search(parameter);
                        result = result.Replace(',', '，').Replace(Convert.ToChar((byte)0x1F), ' ');
                        //result = result.Replace(',', '，').Replace("'","");
                        if (!String.IsNullOrEmpty(result))
                        {
                            XmlDocument xDocResult = new XmlDocument();
                            xDocResult.LoadXml(result);
                            XmlNodeList xNodeListResult = xDocResult.GetElementsByTagName("Data");
                            if (xNodeListResult.Count > 0)
                            {

                                for (int i = 0; i <= xNodeListResult.Count - 1; i++)
                                {
                                    sw.WriteLine(
                                        //(xNodeListResult.Item(i).ChildNodes[0].FirstChild != null ? xNodeListResult.Item(i).ChildNodes[0].FirstChild.Value : "") + "," +
                                        (xNodeListResult.Item(i).ChildNodes[1].FirstChild != null ? xNodeListResult.Item(i).ChildNodes[1].FirstChild.Value : "") + "," +
                                        (xNodeListResult.Item(i).ChildNodes[2].FirstChild != null ? xNodeListResult.Item(i).ChildNodes[2].FirstChild.Value : "") + "," +
                                        (xNodeListResult.Item(i).ChildNodes[3].FirstChild != null ? xNodeListResult.Item(i).ChildNodes[3].FirstChild.Value : "") + "," +
                                        (xNodeListResult.Item(i).ChildNodes[4].FirstChild != null ? xNodeListResult.Item(i).ChildNodes[4].FirstChild.Value : "") + "," +
                                        (xNodeListResult.Item(i).ChildNodes[5].FirstChild != null ? xNodeListResult.Item(i).ChildNodes[5].FirstChild.Value : "") + "," +
                                        (xNodeListResult.Item(i).ChildNodes[6].FirstChild != null ? xNodeListResult.Item(i).ChildNodes[6].FirstChild.Value : "") + "," +
                                        (xNodeListResult.Item(i).ChildNodes[7].FirstChild != null ? xNodeListResult.Item(i).ChildNodes[7].FirstChild.Value : "") + "," +
                                        (xNodeListResult.Item(i).ChildNodes[8].FirstChild != null ? xNodeListResult.Item(i).ChildNodes[8].FirstChild.Value : "") + "," +
                                        (xNodeListResult.Item(i).ChildNodes[9].FirstChild != null ? xNodeListResult.Item(i).ChildNodes[9].FirstChild.Value : "") + "," +
                                        (xNodeListResult.Item(i).ChildNodes[10].FirstChild != null ? xNodeListResult.Item(i).ChildNodes[10].FirstChild.Value : "") + "," +
                                        (xNodeListResult.Item(i).ChildNodes[11].FirstChild != null ? xNodeListResult.Item(i).ChildNodes[11].FirstChild.Value : "")
                                        );
                                }


                            }
                        }
                    }

                    sw.Close();

                    StreamWriter sw_complete = new StreamWriter(url_complete, false, System.Text.Encoding.UTF8);
                    sw_complete.WriteLine("");
                    sw_complete.Close();
                }
                else
                {
                    StreamWriter sw_complete = new StreamWriter(url_complete, false, System.Text.Encoding.UTF8);
                    sw_complete.WriteLine("");
                    sw_complete.Close();
                }
            }
            catch (Exception ex)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSBookingFunction.asmx/ExportSearchResult", Log.TRACKING_LEVEL.ERROR, ex.Message);
                //return "";
            }

        }

        /// <summary>
        /// 檢查匯出檔案是否已完成
        /// </summary>
        /// <param name="user_id">使用者帳號</param>
        /// <returns>下載路徑</returns>
        [WebMethod]
        public string IsExportComplete(string user_id)
        {
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();

            string url = "";
            //string url_complete = "D:\\PTS\\數位片庫\\CSV\\" + user_id + @"\SearchExport.complete";
            string url_complete = SettingObj.sysConfig_Read("/ServerConfig/ExportSearchResultRealFilePath") + user_id + @"\SearchExport.complete";
            if (File.Exists(url_complete))
            {
                url = SettingObj.sysConfig_Read("/ServerConfig/ExportSearchResult") + user_id + "/SearchExport.csv";
                //url = "http://localhost/PTS_MAM3.Web/Export/" + user_id + "/SearchExport.csv";
            }
            else
            {
                url = "";
            }
            return url;
        }



        /// <summary>
        /// 抽單
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>成功或失敗</returns>
        [WebMethod]
        public bool SetBookingUnCheck(string booking_no, string user_id)
        {
            bool flag = true;
            flag = DbAccess.Do_Transaction("SP_U_BOOKING_STATUS_FOR_UNCHECK", "<Data><FSBOOKING_NO>" + booking_no + "</FSBOOKING_NO></Data>", user_id);
            return flag;
        }

        /// <summary>
        /// 取得調用審核主管
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>主管id</returns>
        [WebMethod]
        public string GetBookingSupervisor(string parameter)
        {
            string result = "";
            result = DbAccess.Do_Query("SP_Q_GET_BOOKING_SUPERVISOR", parameter);
            return result;
        }


        /// <summary>
        /// 取得要修改的調用審核主管
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>主管id</returns>
        [WebMethod]
        public string GetBookingSupervisorUpdate(string parameter)
        {
            string result = "";
            result = DbAccess.Do_Query("SP_Q_GET_BOOKING_SUPERVISOR_UPDATE", parameter);
            return result;
        }

        /// <summary>
        /// 查詢直屬主管審核調用清單主檔
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>調用清單主檔</returns>
        [WebMethod]
        public string GetBookingMasterDataCheckBySelfSupervisor(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_TBBOOKING_MASTER_CHECK_BY_SELF_SUPERVISOR", parameter);
            return result;
        }

        /// <summary>
        /// 查詢直屬主管審核調用清單明細檔
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>調用清單明細</returns>
        [WebMethod]
        public string GetBookingDetailDataCheckBySelfSupervisor(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_TBBOOKING_DETAIL_CHECK_BY_SELF_SUPERVISOR", parameter);
            return result;
        }


        /// <summary>
        /// 查詢檔案主管審核調用清單主檔
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>調用清單主檔</returns>
        [WebMethod]
        public string GetBookingMasterDataCheckByFileSupervisor(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_TBBOOKING_MASTER_CHECK_BY_FILE_SUPERVISOR", parameter);
            return result;
        }

        /// <summary>
        /// 查詢檔案主管審核調用清單明細檔
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>調用清單明細</returns>
        [WebMethod]
        public string GetBookingDetailDataCheckByFileSupervisor(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_TBBOOKING_DETAIL_CHECK_BY_FILE_SUPERVISOR", parameter);
            return result;
        }


        //資料夾套用ADu 權限
        public void SetPermissions(String vPath, String UserName)
        {
            ADsSecurity objADsSec;
            SecurityDescriptor objSecDes;
            AccessControlList objDAcl;
            AccessControlEntry objAce1;
            AccessControlEntry objAce2;
            Object objSIdHex;
            ADsSID objSId;

            objADsSec = new ADsSecurity();
            objSecDes = (SecurityDescriptor)(objADsSec.GetSecurityDescriptor("FILE://" + vPath));
            objDAcl = (AccessControlList)objSecDes.DiscretionaryAcl;


            objSId = new ADsSID();
            objSId.SetAs((int)ADSSECURITYLib.ADS_SID_FORMAT.ADS_SID_SAM, UserName.ToString());
            objSIdHex = objSId.GetAs((int)ADSSECURITYLib.ADS_SID_FORMAT.ADS_SID_SDDL);

            //Add a new access control entry (ACE) object (objAce) so that the user has Full Control permissions on NTFS file system files.
            if (UserName == "pts\\agility")
            {
                objAce1 = new AccessControlEntry();
                objAce1.Trustee = (objSIdHex).ToString();
                objAce1.AccessMask = (int)ActiveDs.ADS_RIGHTS_ENUM.ADS_RIGHT_GENERIC_WRITE;
                objAce1.AceType = (int)ActiveDs.ADS_ACETYPE_ENUM.ADS_ACETYPE_ACCESS_ALLOWED;
                objAce1.AceFlags = (int)ActiveDs.ADS_ACEFLAG_ENUM.ADS_ACEFLAG_INHERITED_ACE | 1;
                objDAcl.AddAce(objAce1);
            }
            else
            {
                objAce1 = new AccessControlEntry();
                objAce1.Trustee = (objSIdHex).ToString();
                objAce1.AccessMask = (int)ActiveDs.ADS_RIGHTS_ENUM.ADS_RIGHT_DELETE;
                objAce1.AceType = (int)ActiveDs.ADS_ACETYPE_ENUM.ADS_ACETYPE_ACCESS_DENIED;
                objAce1.AceFlags = (int)ActiveDs.ADS_ACEFLAG_ENUM.ADS_ACEFLAG_INHERITED_ACE | 1;
                objDAcl.AddAce(objAce1);
            }


            //Add a new access control entry object (objAce) so that the user has Full Control permissions on NTFS file system folders.
            if (UserName == "pts\\agility")
            {
                objAce2 = new AccessControlEntry();
                objAce2.Trustee = (objSIdHex).ToString();
                objAce2.AccessMask = (int)ActiveDs.ADS_RIGHTS_ENUM.ADS_RIGHT_GENERIC_WRITE;
                objAce2.AceType = (int)ActiveDs.ADS_ACETYPE_ENUM.ADS_ACETYPE_ACCESS_ALLOWED;
                objAce2.AceFlags = (int)ActiveDs.ADS_ACEFLAG_ENUM.ADS_ACEFLAG_INHERIT_ACE | 1;
                objDAcl.AddAce(objAce2);
            }
            else
            {
                objAce2 = new AccessControlEntry();
                objAce2.Trustee = (objSIdHex).ToString();
                objAce2.AccessMask = (int)ActiveDs.ADS_RIGHTS_ENUM.ADS_RIGHT_GENERIC_READ;
                objAce2.AceType = (int)ActiveDs.ADS_ACETYPE_ENUM.ADS_ACETYPE_ACCESS_ALLOWED;
                objAce2.AceFlags = (int)ActiveDs.ADS_ACEFLAG_ENUM.ADS_ACEFLAG_INHERIT_ACE | 1;
                objDAcl.AddAce(objAce2);
            }



            //delete everyone
            foreach (AccessControlEntry a in objSecDes.DiscretionaryAcl)
            {
                if (a.Trustee == "Everyone")
                {
                    objDAcl.RemoveAce(a);
                }
            }

            objSecDes.DiscretionaryAcl = objDAcl;

            // Set permissions on the NTFS file system folder.
            objADsSec.SetSecurityDescriptor(objSecDes, "FILE://" + vPath);

        }

        //調用分析
        [WebMethod]
        public string GetBookingAnalysisReport(string sYear, string sMonth, string eYear, string eMonth, string CREATED_BY, string strSessionID)
        {
            String strURL = "";  //要傳回去的URL

            //取列印報表的系統設定檔
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string reportSrv = SettingObj.sysConfig_Read("/ServerConfig/ReportServer");
            //string MainWeb = SettingObj.sysConfig_Read("/ServerConfig/MainWeb");

            //http://10.13.220.103/ReportServer/Pages/ReportViewer.aspx?%2fPTS_MAM3%2fRPT_TBBOOKING_02&QUERY_BY=David.Sin&YY=2011&rc:parameters=false&rs:Command=Render
            strURL = reportSrv + "RPT_TBBOOKING_02&QUERY_BY=" + CREATED_BY + "&SYYMM=" + sYear + "/" + sMonth + "&EYYMM=" + eYear + "/" + eMonth + "&rc:parameters=false&rs:Command=Render";
            return Server.UrlEncode(strURL);

        }

        //調用頻道分析
        [WebMethod]
        public string GetBookingChannelAnalysisReport(string sYear, string sMonth, string eYear, string eMonth, string CREATED_BY, string strSessionID)
        {
            String strURL = "";  //要傳回去的URL

            //取列印報表的系統設定檔
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string reportSrv = SettingObj.sysConfig_Read("/ServerConfig/ReportServer");
            //string MainWeb = SettingObj.sysConfig_Read("/ServerConfig/MainWeb");

            //http://10.13.220.103/ReportServer/Pages/ReportViewer.aspx?%2fPTS_MAM3%2fRPT_TBBOOKING_03&QUERY_BY=David.Sin&YY=2011&rc:parameters=false&rs:Command=Render
            strURL = reportSrv + "RPT_TBBOOKING_03&QUERY_BY=" + CREATED_BY + "&SYYMM=" + sYear + "/" + sMonth + "&EYYMM=" + eYear + "/" + eMonth + "&rc:parameters=false&rs:Command=Render";
            return Server.UrlEncode(strURL);

        }

        //各頻道調用原因分析
        [WebMethod]
        public string GetBookingReasonChannelAnalysisReport(string sDate, string eData, string CREATED_BY, string strSessionID)
        {
            String strURL = "";  //要傳回去的URL
            if (string.IsNullOrEmpty(sDate))
            {
                sDate = DateTime.Now.ToString("yyyy/MM/dd");
            }
            if (string.IsNullOrEmpty(eData))
            {
                eData = DateTime.Now.ToString("yyyy/MM/dd");
            }
            //取列印報表的系統設定檔
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string reportSrv = SettingObj.sysConfig_Read("/ServerConfig/ReportServer");
            //string MainWeb = SettingObj.sysConfig_Read("/ServerConfig/MainWeb");

            //http://10.13.220.103/ReportServer/Pages/ReportViewer.aspx?%2fPTS_MAM3%2fRPT_TBBOOKING_04&QUERY_BY=David.Sin&YY=2011&rc:parameters=false&rs:Command=Render
            strURL = reportSrv + "RPT_TBBOOKING_04&QUERY_BY=" + CREATED_BY + "&SDATE=" + sDate + "&EDATE=" + eData + "&rc:parameters=false&rs:Command=Render";
            return Server.UrlEncode(strURL);

        }

        /// <summary>
        /// 檢查調用時節目是否過期
        /// </summary>
        /// <param name="_fsfile_no">檔案編號</param>
        /// <param name="_fsfile_type">檔案類型(VAPD)</param>
        /// <returns></returns>
        [WebMethod]
        public string CheckProgIsExpire(string _fsfile_nos, string _fsfile_types)
        {
            string _result = "";
            //if (_fsfile_type.ToLower().IndexOf("video") > -1)
            //{
            //    _fsfile_type = "V";
            //}
            //else if (_fsfile_type.ToLower().IndexOf("audio") > -1)
            //{
            //    _fsfile_type = "A";
            //}
            //else if (_fsfile_type.ToLower().IndexOf("photo") > -1)
            //{
            //    _fsfile_type = "P";
            //}
            //else if (_fsfile_type.ToLower().IndexOf("doc") > -1)
            //{
            //    _fsfile_type = "D";
            //}
            //VW_SEARCH_VIDEO

            List<Dictionary<string, string>> sqlResult;
            Dictionary<string, string> _dic = new Dictionary<string, string>();
            _dic.Add("FSFILE_NOs", _fsfile_nos);
            _dic.Add("FSFILE_TYPEs", _fsfile_types.Replace("VW_SEARCH_VIDEO", "V").Replace("VW_SEARCH_AUDIO", "A").Replace("VW_SEARCH_PHOTO", "P").Replace("VW_SEARCH_DOC", "D"));
            DbAccess.Do_Query("SP_Q_CHECK_PROG_IS_EXPIRE_CANNOT_BOOKING", _dic, out sqlResult);
            if (sqlResult != null && sqlResult.Count > 0)
            {
                _result = (string.IsNullOrEmpty(sqlResult[0]["fsFILE_NOs"]) == true ? string.Empty : sqlResult[0]["fsFILE_NOs"].Substring(0, sqlResult[0]["fsFILE_NOs"].Length - 1));
            }

            return _result;
        }

        /// <summary>
        /// 記錄調用審核呼叫FLOW-新增審核
        /// </summary>
        /// <param name="fnPROCESS_ID">流程編號</param>
        /// <param name="fsFLOW_ID">案號</param>
        /// <param name="fsUSER_ID">建立者</param>
        /// <param name="fsCONTENT">內容</param>
        /// <returns></returns>
        [WebMethod]
        public void fnINSERT_NEWFLOW_WITHFIELD_LOG(int fnPROCESS_ID, string fsFLOW_ID, string fsUSER_ID, string fsCONTENT)
        {
            MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/FlowMAM/CallFlow_NewFlowWithField", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "NT;流程編號：" + fnPROCESS_ID + "，申請者：" + fsUSER_ID + "，內容：" + fsCONTENT + "，案號：" + fsFLOW_ID);
        }


        /// <summary>
        /// 記錄調用審核呼叫FLOW-審核
        /// </summary>
        /// <param name="fnPROCESS_ID">流程編號</param>
        /// <param name="fsFLOW_ID">案號</param>
        /// <param name="fsUSER_ID">建立者</param>
        /// <param name="fsCONTENT">內容</param>
        /// <returns></returns>
        [WebMethod]
        public void fnINSERT_FLOW_WITHFIELD_LOG(int fnPROCESS_ID, string fsFLOW_ID, string fsUSER_ID, string fsCONTENT)
        {
            MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/FlowMAM/CallFlow_FlowWithField", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "NT;流程編號：" + fnPROCESS_ID + "，申請者：" + fsUSER_ID + "，內容：" + fsCONTENT + "，案號：" + fsFLOW_ID);
        }



    }


}
