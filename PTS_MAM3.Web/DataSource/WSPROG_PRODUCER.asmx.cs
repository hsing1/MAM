﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using PTS_MAM3.Web.Class;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSPROG_PRODUCER 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSPROG_PRODUCER : System.Web.Services.WebService
    {
        //節目製作人檔的所有資料
        [WebMethod]
        public List<Class_PROGPRODUCER> GetTBPROG_PRODUCER_ALL()
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_PRODUCER_ALL", sqlParameters, out sqlResult))
                return new List<Class_PROGPRODUCER>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PROGPRODUCER(sqlResult);
        }

        //解析資料庫回來的Dictionary，轉存成Class
        private List<Class_PROGPRODUCER> Transfer_PROGPRODUCER(List<Dictionary<string, string>> sqlResult)
        {
            List<Class_PROGPRODUCER> returnData = new List<Class_PROGPRODUCER>();
 
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_PROGPRODUCER obj = new Class_PROGPRODUCER();
                // --------------------------------    
                obj.FSPRO_TYPE = sqlResult[i]["FSPRO_TYPE"];                                    //類型
                obj.FSPRO_TYPE_NAME = MAMFunctions.CheckType(sqlResult[i]["FSPRO_TYPE"]);       //類型(顯示)
                obj.FSID = sqlResult[i]["FSID"];                                                //編碼
                obj.FSID_NAME = sqlResult[i]["FSID_NAME"];                                      //名稱(顯示)
                obj.FSPRODUCER = sqlResult[i]["FSPRODUCER"];                                    //製作人或協調
                obj.FSPRODUCER_NAME = sqlResult[i]["SHOW_FSPRODUCER_NAME"];                     //製作人或協調名字
                obj.FNUpperDept_ID =  Convert.ToInt32(sqlResult[i]["FNUpperDept_ID"]);          //上層組織
                obj.FNDep_ID =  Convert.ToInt32(sqlResult[i]["FNDep_ID"]);                      //組織
                obj.FNGroup_ID =  Convert.ToInt32(sqlResult[i]["FNGroup_ID"]);                  //組別
                obj.FSPRODUCER_DEPT_NAME = QUERY_TBUSERS_BYUSERID(sqlResult[i]["FSPRODUCER"]);  //製作人或協調的單位

                obj.FSCREATED_BY_NAME = sqlResult[i]["FSCREATED_BY_NAME"];                      //建檔者
                obj.FDCREATED_DATE = Convert.ToDateTime(sqlResult[i]["WANT_FDCREATEDDATE"]);    //建檔日期
                obj.SHOW_FDCREATED_DATE = MAMFunctions.compareDate(sqlResult[i]["WANT_FDCREATEDDATE"]); //建檔日期(顯示)
                obj.FSUPDATED_BY_NAME = sqlResult[i]["FSUPDATED_BY_NAME"];                      //修改者
                obj.FDUPDATED_DATE = Convert.ToDateTime(sqlResult[i]["WANT_FDUPDATEDDATE"]);            //修改日期
                obj.SHOW_FDUPDATED_DATE = MAMFunctions.compareDate(sqlResult[i]["WANT_FDUPDATEDDATE"]); //修改日期(顯示)
                // --------------------------------
                returnData.Add(obj);
            }

            return returnData;
        }

        //新增節目製作人檔
        [WebMethod]
        public Boolean INSERT_TBPROG_PRODUCER(Class_PROGPRODUCER obj)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPRO_TYPE", obj.FSPRO_TYPE);
            source.Add("FSID", obj.FSID);
            source.Add("FSPRODUCER", obj.FSPRODUCER);            
            source.Add("FSCREATED_BY", obj.FSCREATED_BY);
            source.Add("FSUPDATED_BY", obj.FSUPDATED_BY);

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBPROG_PRODUCER", source, obj.FSUPDATED_BY);
        }

        //查詢節目製作人檔
        [WebMethod]
        public List<Class_PROGPRODUCER> QUERY_TBPROG_PRODUCER(Class_PROGPRODUCER obj)
        {
            // SQL Parameters
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPRO_TYPE", obj.FSPRO_TYPE);
            sqlParameters.Add("FSID", obj.FSID);
            sqlParameters.Add("FSPRODUCER", obj.FSPRODUCER);       

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_PRODUCER_CONDITIONS", sqlParameters, out sqlResult))
                return new List<Class_PROGPRODUCER>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PROGPRODUCER(sqlResult);
        }

        //查詢節目製作人檔_BY PRODUCER
        [WebMethod]
        public List<Class_PROGPRODUCER> QUERY_TBPROG_PRODUCER_BYPRODUCER(Class_PROGPRODUCER obj)
        {
            // SQL Parameters
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();

            if (obj.FSPRO_TYPE == null)
                sqlParameters.Add("FSPRO_TYPE", "");
            else
                sqlParameters.Add("FSPRO_TYPE", obj.FSPRO_TYPE);

            if (obj.FSID == null)
                sqlParameters.Add("FSID", "");
            else
                sqlParameters.Add("FSID", obj.FSID);

            if (obj.FSPRODUCER == null)
                sqlParameters.Add("FSPRODUCER", "");
            else
                sqlParameters.Add("FSPRODUCER", obj.FSPRODUCER);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_PRODUCER_BYPRODUCER", sqlParameters, out sqlResult))
                return new List<Class_PROGPRODUCER>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PROGPRODUCER(sqlResult);
        }

        //查詢節目製作人檔(要去擋同一類型、同一編碼、同一製作人或協調)
        [WebMethod]
        public string QUERY_TBPROG_PRODUCER_CHECK(Class_PROGPRODUCER obj)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPRO_TYPE", obj.FSPRO_TYPE);
            sqlParameters.Add("FSID", obj.FSID);
            sqlParameters.Add("FSPRODUCER", obj.FSPRODUCER);           

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_PRODUCER_BYCHECK", sqlParameters, out sqlResult))
                return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count > 0)
                return sqlResult[0]["dbCount"];
            else
                return "";
        }

        //刪除節目製作人檔
        [WebMethod]
        public Boolean DELETE_TBPROG_PRODUCER(string strPRO_TYPE, string strID, string strPRODUCER, string strUPDATED_BY)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPRO_TYPE", strPRO_TYPE);
            source.Add("FSID", strID);
            source.Add("FSPRODUCER", strPRODUCER);

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_TBPROG_PRODUCER", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source), strUPDATED_BY);
        }
        
        //查詢使用者單位
        [WebMethod]
        public string QUERY_TBUSERS_BYUSERID(string strUserID)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSUSER_ID", strUserID);
            string strReturn = "";

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBUSER_BYUSERID", sqlParameters, out sqlResult))
                return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count > 0)
            {
                if (sqlResult[0]["FSUpperDept_ChtName"] != null && sqlResult[0]["FSUpperDept_ChtName"] != "" )
                    strReturn = sqlResult[0]["FSUpperDept_ChtName"];

                if (sqlResult[0]["FSDep_ChtName"] != null && sqlResult[0]["FSDep_ChtName"] != "")
                    strReturn = strReturn + "\\" +  sqlResult[0]["FSDep_ChtName"] ;

                if (sqlResult[0]["FSGroup_ChtName"] != null && sqlResult[0]["FSGroup_ChtName"] != "")
                    strReturn = strReturn + "\\" + sqlResult[0]["FSGroup_ChtName"];

                return strReturn;
            }
            else
                return "";
        }

        //查詢全部的使用者檔
        [WebMethod]
        public List<UserStruct> QUERY_TBUSERS_ALL()
        {
            // SQL Parameters
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBUSERS_ALL", sqlParameters, out sqlResult))
                return new List<UserStruct>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            List<UserStruct> returnData = new List<UserStruct>();

            for (int i = 0; i < sqlResult.Count; i++)
            {
                UserStruct obj = new UserStruct();
                // --------------------------------    
                obj.FSUSER_ID = sqlResult[i]["FSUSER_ID"];                              //AD帳號
                obj.FSUSER = sqlResult[i]["FSUSER"];                                    //MAM帳號
                obj.FSUSER_ChtName = sqlResult[i]["FSUSER_ChtName"].Trim() + "-" + sqlResult[i]["FSUSER_ID"].Trim();//顯示名稱
                obj.FSUPPERDEPT_CHTNAME = sqlResult[i]["FSUpperDept_ChtName"];          //頻道名稱
                obj.FSDEP_CHTNAME = sqlResult[i]["FSDep_ChtName"];                      //部門名稱
                obj.FSGROUP_CHTNAME = sqlResult[i]["FSGroup_ChtName"];                  //組名稱   

                //把重複的FSUpperDept_ChtName改成FSDep_ChtName並把多餘的部分註解  by Jarvis 20140109
                //if (sqlResult[i]["FSUpperDept_ChtName"].Trim() != "")
                //    obj.FSTITLE_NAME = sqlResult[i]["FSUpperDept_ChtName"].Trim() + "-" + sqlResult[i]["FSUSER_ID"].Trim();

                //if (sqlResult[i]["FSDep_ChtName"].Trim() != "")
                //    obj.FSTITLE_NAME = sqlResult[i]["FSUpperDept_ChtName"].Trim() + "/" + sqlResult[i]["FSDept_ChtName"].Trim() + "-" + sqlResult[i]["FSUSER_ID"].Trim();

                //if (sqlResult[i]["FSGroup_ChtName"].Trim() != "")
                obj.FSTITLE_NAME = sqlResult[i]["FSUpperDept_ChtName"].Trim() + "/" + sqlResult[i]["FSDep_ChtName"].Trim() + "/" + sqlResult[i]["FSGroup_ChtName"].Trim() + "-" + sqlResult[i]["FSUSER_ID"].Trim();
                // --------------------------------
                returnData.Add(obj);
            }

            return returnData;
        }

    }
}
