﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.Text;
using System.Xml;
using System.Runtime.Serialization.Formatters.Binary; 

namespace PTS_MAM3.Web.DataSource
{
    public class SerializationClass
    {

        public string Serialize<T>(T data)
        {

            //XmlSerializer ser = new XmlSerializer(data.GetType());
            ////StringBuilder sb = new StringBuilder();
            ////StringWriter writer = new StringWriter(sb);

            //byte[] bytes;
            //MemoryStream ws = new MemoryStream();
            //BinaryFormatter sf = new BinaryFormatter();

            ////sf.Serialize(ws, 0); 

            //sf.Serialize(ws, data); 

            //bytes = ws.GetBuffer() ;
            ////ser.Serialize(ws, data); 
            ////ser.Serialize(writer, data);

            //return Convert.ToBase64String(bytes, 0, bytes.Length, Base64FormattingOptions.None); 
            using (var memoryStream = new MemoryStream())
            {


                var serializer = new DataContractSerializer(typeof(T));
                serializer.WriteObject(memoryStream, data);

                memoryStream.Seek(0, SeekOrigin.Begin);

                var reader = new StreamReader(memoryStream);
                string content = reader.ReadToEnd();
                return content;
            }
        }

        public T Deserialize<T>(string xml)
        {
             XmlDocument xdoc = new XmlDocument();   

        try  
        {
            xdoc.LoadXml(xml);   
            XmlNodeReader reader = new XmlNodeReader(xdoc.DocumentElement);   
            XmlSerializer ser = new XmlSerializer(typeof(T));   
            object obj = ser.Deserialize(reader);   
  
            return (T)obj;   
        }   
        catch  
        {   
           return default(T);   
        }   


            //using (var stream = new MemoryStream(System.Text.Encoding.Unicode.GetBytes(xml)))
            //{
            //    var serializer = new DataContractSerializer(typeof(T));
            //    T theObject = (T)serializer.ReadObject(stream);
            //    return theObject;
            //}
        }
    }
}