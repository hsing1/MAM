﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSLogSystem 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSLogSystem : System.Web.Services.WebService
    {

        // 如果有其他層級，可以在這邊增加
        public enum TRACKING_LEVEL { TRACE, DEBUG, INFO, WARNING, ERROR, FATALERROR };

        [WebMethod]
        public bool AppendTrackingLog(string programFile, TRACKING_LEVEL trackingLevel, string trackingMessage)
        {
            return AppendTrackingLog(programFile, trackingLevel.ToString(), trackingMessage);
        }
        

        public bool AppendTrackingLog(string programFile, string trackingLevel, string trackingMessage)
        {
            // SQL Parameters
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROGRAM", programFile);
            source.Add("FSCATALOG", trackingLevel);
            source.Add("FSMESSAGE", trackingMessage);
            // Do Transcation
            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBTRACKINGLOG", source, "system", false);
        }
     
    }
}
