﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Text;
using System.Xml.Linq;
using System.IO;
using System.Data;


namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSLogTemplateField 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSLogTemplateField : System.Web.Services.WebService
    {
        //public List<Class.LogTemplateField> tempList = new List<Class.LogTemplateField>();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fstable_type">VDAP</param>
        /// <param name="fsfile_no">FSFILE_NO</param>
        /// <param name="fnseq_no">(optional </param>
        /// <param name="TemplateFieldList"></param>
        /// <param name="fsuser_id"></param>
        /// <returns></returns>
        [WebMethod]
        public Boolean SetTemplateFields(string fstable_type, string fsfile_no, int fnseq_no, List<Class.LogTemplateField> TemplateFieldList, string fsuser_id)
        {
            Boolean brtn = false;

            foreach (Class.LogTemplateField obj in TemplateFieldList)
            {
                
                Dictionary<string, string> source = new Dictionary<string, string>();

                source.Add("FSTABLE_TYPE", fstable_type);
                source.Add("FSFILE_NO", fsfile_no);
                source.Add("FNSEQ_NO", fnseq_no.ToString());
                source.Add("FSFIELD", obj.FSFIELD);
                source.Add("FSFIELD_VALUE", obj.FSFIELD_VALUE);
                brtn = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBLOG_TEMPLATE", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source), fsuser_id);

            }
    
            return brtn;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fstable_type">V/A/D/P</param>
        /// <param name="fsfile_no">FSFILE_NO</param>
        /// <returns></returns>
        [WebMethod]
        public List<Class.LogTemplateField> GetTemplateFields(string fstable_type, string fsfile_no, string fnseq_no)
        {
            List<Class.LogTemplateField> TemplateFieldList = new List<Class.LogTemplateField>();
            try
            {
                string rtn = "";
                Dictionary<string, string> source = new Dictionary<string, string>();

                source.Add("FSTABLE_TYPE", fstable_type);

                source.Add("FSFILE_NO", fsfile_no);

                rtn = MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_TBTEMPLATE", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source));

                if (!string.IsNullOrEmpty(rtn))
                {
                    byte[] byteArray = Encoding.Unicode.GetBytes(rtn);
                    XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                    var ltox = from s in doc.Elements("Datas").Elements("Data")
                               select new Class.LogTemplateField
                               {
                                   FCISNULLABLE = (string)s.Element("FCISNULLABLE"),
                                   FNFIELD_LENGTH = (string)s.Element("FNFIELD_LENGTH"),
                                   FNID = (string)s.Element("FNID"),
                                   FNOBJECT_WIDTH = (string)s.Element("FNOBJECT_WIDTH"),
                                   FNORDER = (string)s.Element("FNORDER"),                                
                                   FSDESCRIPTION = (string)s.Element("FSDESCRIPTION"),
                                   FSFIELD = (string)s.Element("FSFIELD"),
                                   FSFIELD_NAME=(string)s.Element("FSFIELD_NAME"),
                                   FSFIELD_VALUE=(string)s.Element("FSFIELD_VALUE"),
                                   FSFIELD_TYPE = (string)s.Element("FSFIELD_TYPE"),
                                   FSCODE_ID = (string)s.Element("FSCODE_ID"),
                                   FSCODE_CNT = (string)s.Element("FSCODE_CNT"),
                                   FSCODE_CTRL = (string)s.Element("FSCODE_CTRL"),
                                   CODELIST = (List<Class.LogTemplateField_CODE>)GetCodeList((string)s.Element("FSCODE_ID"))
                               };
                    foreach (Class.LogTemplateField obj in ltox)
                    {

                        source = new Dictionary<string, string>();
                        source.Add("FSTABLE_TYPE", fstable_type);
                        source.Add("FSFILE_NO", fsfile_no); 
                        source.Add("FSFIELD", obj.FSFIELD);
                        source.Add("FNSEQ_NO", fnseq_no); 
                        rtn = MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_FIELD", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source));
                       //TemplateFieldList.Add(obj);

                        //SP_Q_TBLOG_FIELD
                        if (!string.IsNullOrEmpty(rtn))
                        {
                             byteArray = Encoding.Unicode.GetBytes(rtn);
                             doc = XDocument.Load(new MemoryStream(byteArray));
                             var l = from s in doc.Elements("Datas").Elements("Data")
                                     select
                                       (string)s.Element(obj.FSFIELD) ;

                             foreach (string s in l)
                             {
                                 obj.FSFIELD_VALUE = s;
                                 break; 
                             }
                        }

                        TemplateFieldList.Add(obj);
                    }
                }
            }
            catch (Exception)
            {
                TemplateFieldList = null;
            }
            return TemplateFieldList;
        }


        [WebMethod]
        public List<Class.LogTemplateField_CODE> GetCodeList(String CodeID)
        {
            List<Class.LogTemplateField_CODE> CodeList = new List<Class.LogTemplateField_CODE>();

            if (CodeID == "") return CodeList;

            try
            {
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;
                    cmd.CommandText = " SELECT ";
                    cmd.CommandText += "    [FSCODE_ID],[FSCODE_CODE],[FSCODE_NAME] = (CASE WHEN FSCODE_ISENABLED = 0 THEN '(已停用)' + [FSCODE_NAME] ELSE [FSCODE_NAME] END)    ,[FSCODE_ENAME],[FSCODE_ORDER], ";
                    cmd.CommandText += "    [FSCODE_SET],[FSCODE_NOTE],[FSCODE_CHANNEL_ID],[FSCODE_ISENABLED], ";
                    cmd.CommandText += "    [FSCREATED_BY],[FDCREATED_DATE],[FSUPDATED_BY],[FDUPDATED_DATE] ";
                    cmd.CommandText += "FROM ";
                    cmd.CommandText += "    TBZCODE ";
                    cmd.CommandText += "WHERE ";
                    cmd.CommandText += "    FSCODE_ID = '" + CodeID + "' ";
                    cmd.CommandText += "ORDER BY ";
                    cmd.CommandText += "    FSCODE_ISENABLED DESC, FSCODE_ORDER, FSCODE_CODE"; 

                    cn.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                CodeList.Add
                                (
                                    new PTS_MAM3.Web.Class.LogTemplateField_CODE
                                    {
                                        FSCODE_ID = dr["FSCODE_ID"].ToString(),
                                        FSCODE_CODE = dr["FSCODE_CODE"].ToString(),
                                        FSCODE_NAME = dr["FSCODE_NAME"].ToString(),
                                        FSCODE_ENAME = dr["FSCODE_ENAME"].ToString(),
                                        FSCODE_ORDER = dr["FSCODE_ORDER"].ToString(),

                                        FSCODE_SET = dr["FSCODE_SET"].ToString(),
                                        FSCODE_NOTE = dr["FSCODE_NOTE"].ToString(),
                                        FSCODE_CHANNEL_ID = dr["FSCODE_CHANNEL_ID"].ToString(),

                                        FSCODE_ISENABLED = Boolean.Parse(dr["FSCODE_ISENABLED"].ToString()),

                                        FSCREATED_BY = dr["FSCREATED_BY"].ToString(),
                                        FDCREATED_DATE= DateTime.Parse( dr["FDCREATED_DATE"].ToString()),
                                        FSUPDATED_BY = dr["FSUPDATED_BY"].ToString(),
                                        FDUPDATED_DATE = DateTime.Parse(dr["FDUPDATED_DATE"].ToString())
                                    }
                                );
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                return null;
            }

            return CodeList;
        }


        [WebMethod]
        public List<Class.LogTemplateField_CODESET> GetCodeSetList(String _ID, String _TITLE, String _NOTE, String _ISENABLED)
        {
            List<Class.LogTemplateField_CODESET> CodeSetList = new List<Class.LogTemplateField_CODESET>();
             
            try
            {
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;
                    cmd.CommandText = " SELECT " + "\n";
                    cmd.CommandText += "    S.[FSCODE_ID],S.[FSCODE_TITLE],S.[FSCODE_TBCOL],S.[FSCODE_NOTE],S.[FSCODE_ISENABLED], " + "\n";
                    cmd.CommandText += "    S.[FSCREATED_BY],S.[FDCREATED_DATE],S.[FSUPDATED_BY],S.[FDUPDATED_DATE], " + "\n";
                    cmd.CommandText += "    _COUNT = COUNT(C.FSCODE_CODE), " + "\n";
                    cmd.CommandText += "    _ISENABLED = CASE CAST(S.[FSCODE_ISENABLED] AS VARCHAR) WHEN '0' THEN 'N' ELSE 'Y' END " + "\n";
                    cmd.CommandText += "FROM " + "\n";
                    cmd.CommandText += "    TBZCODE_SET AS S ";
                    cmd.CommandText += "    LEFT JOIN TBZCODE AS C ON (C.FSCODE_ID = S.FSCODE_ID)" + "\n";
                    cmd.CommandText += "WHERE " + "\n";
                    cmd.CommandText += "    (S.FSCODE_ID LIKE '%" + _ID + "%') AND " + "\n";
                    cmd.CommandText += "    (S.FSCODE_TITLE LIKE '%" + _TITLE + "%') AND " + "\n";
                    cmd.CommandText += "    ('" + _ISENABLED + "' LIKE '%'+CAST(S.FSCODE_ISENABLED AS VARCHAR)+'%') " + "\n";
                    cmd.CommandText += "GROUP BY S.FSCODE_ID, FSCODE_TITLE, S.FSCODE_TBCOL, S.FSCODE_NOTE, S.[FSCODE_ISENABLED], S.FSCREATED_BY, S.FDCREATED_DATE, S.FSUPDATED_BY, S.FDUPDATED_DATE " + "\n";
                    cmd.CommandText += "ORDER BY " + "\n";
                    cmd.CommandText += "    S.[FSCODE_ID]";

                    cn.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                CodeSetList.Add
                                (
                                    new PTS_MAM3.Web.Class.LogTemplateField_CODESET
                                    {
                                        FSCODE_ID = dr["FSCODE_ID"].ToString(),
                                        FSCODE_TITLE = dr["FSCODE_TITLE"].ToString(),
                                        FSCODE_TBCOL = dr["FSCODE_TBCOL"].ToString(),
                                        FSCODE_NOTE = dr["FSCODE_NOTE"].ToString(),
                                        FSCODE_ISENABLED = Boolean.Parse(dr["FSCODE_ISENABLED"].ToString()),
                                        _COUNT = int.Parse(dr["_COUNT"].ToString()),
                                        _ISENABLED = dr["_ISENABLED"].ToString(),
                                         
                                        FSCREATED_BY = dr["FSCREATED_BY"].ToString(),
                                        FDCREATED_DATE = DateTime.Parse(dr["FDCREATED_DATE"].ToString()),
                                        FSUPDATED_BY = dr["FSUPDATED_BY"].ToString(),
                                        FDUPDATED_DATE = DateTime.Parse(dr["FDUPDATED_DATE"].ToString())
                                    }
                                );
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                return null;
            }

            return CodeSetList;
        }


        [WebMethod]
        public string InsertCodeSet(Class.LogTemplateField_CODESET clCODESET)
        {
            //List<Class.LogTemplateField_CODESET> lstNews = new List<Class.LogTemplateField_CODESET>();
            String sqlConnStr = WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            String strResult = "";  

            try
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_I_TBZCODE_SET", connection);
                DataTable dt = new DataTable();

                da.SelectCommand.Parameters.Add(new SqlParameter("@FSCODE_ID", clCODESET.FSCODE_ID));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSCODE_TITLE", clCODESET.FSCODE_TITLE));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSCODE_NOTE", clCODESET.FSCODE_NOTE));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSCODE_ISENABLED", clCODESET.FSCODE_ISENABLED));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSCREATED_BY", clCODESET.FSCREATED_BY));

                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                da.Fill(dt);

                if (dt.Rows.Count == 0)
                {
                    strResult = "ERROR:資料庫未回應執行結果";
                }
                else
                {
                    strResult = dt.Rows[0].ItemArray[0].ToString();
                }
            }
            catch (Exception ex)
            {
                strResult = "ERROR:" + ex.Message;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }

            return strResult;
        }

        [WebMethod]
        public string UpdateCodeSet(Class.LogTemplateField_CODESET clCODESET)
        {
            //List<Class.LogTemplateField_CODESET> lstNews = new List<Class.LogTemplateField_CODESET>();
            String sqlConnStr = WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            String strResult = "";

            try
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_U_TBZCODE_SET", connection);
                DataTable dt = new DataTable();

                da.SelectCommand.Parameters.Add(new SqlParameter("@FSCODE_ID", clCODESET.FSCODE_ID));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSCODE_TITLE", clCODESET.FSCODE_TITLE));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSCODE_NOTE", clCODESET.FSCODE_NOTE));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSCODE_ISENABLED", clCODESET.FSCODE_ISENABLED));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSUPDATED_BY", clCODESET.FSUPDATED_BY));

                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                da.Fill(dt);

                if (dt.Rows.Count == 0)
                {
                    strResult = "ERROR:資料庫未回應執行結果";
                }
                else
                {
                    strResult = dt.Rows[0].ItemArray[0].ToString();
                }
            }
            catch (Exception ex)
            {
                strResult = "ERROR:" + ex.Message;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }

            return strResult;
        }

        [WebMethod]
        public string DeleteCodeSet(String m_ID)
        {
            //List<Class.LogTemplateField_CODESET> lstNews = new List<Class.LogTemplateField_CODESET>();
            String sqlConnStr = WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            String strResult = "";

            try
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_D_TBZCODE_SET", connection);
                DataTable dt = new DataTable();

                da.SelectCommand.Parameters.Add(new SqlParameter("@FSCODE_ID", m_ID)); 

                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                da.Fill(dt);

                if (dt.Rows.Count == 0)
                {
                    strResult = "ERROR:資料庫未回應執行結果";
                }
                else
                {
                    strResult = dt.Rows[0].ItemArray[0].ToString();
                }
            }
            catch (Exception ex)
            {
                strResult = "ERROR:" + ex.Message;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }

            return strResult;
        }




        [WebMethod]
        public string InsertCode(Class.LogTemplateField_CODE clCODE)
        {
            //List<Class.LogTemplateField_CODESET> lstNews = new List<Class.LogTemplateField_CODESET>();
            String sqlConnStr = WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            String strResult = "";

            try
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_I_TBZCODE", connection);
                DataTable dt = new DataTable();

                da.SelectCommand.Parameters.Add(new SqlParameter("@FSCODE_ID", clCODE.FSCODE_ID));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSCODE_CODE", clCODE.FSCODE_CODE));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSCODE_NAME", clCODE.FSCODE_NAME));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSCODE_ORDER", clCODE.FSCODE_ORDER));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSCODE_NOTE", clCODE.FSCODE_NOTE));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSCODE_ISENABLED", clCODE.FSCODE_ISENABLED));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSCREATED_BY", clCODE.FSCREATED_BY));

                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                da.Fill(dt);

                if (dt.Rows.Count == 0)
                {
                    strResult = "ERROR:資料庫未回應執行結果";
                }
                else
                {
                    strResult = dt.Rows[0].ItemArray[0].ToString();
                }
            }
            catch (Exception ex)
            {
                strResult = "ERROR:" + ex.Message;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }

            return strResult;
        }

        [WebMethod]
        public string UpdateCode(Class.LogTemplateField_CODE clCODE)
        {
            //List<Class.LogTemplateField_CODESET> lstNews = new List<Class.LogTemplateField_CODESET>();
            String sqlConnStr = WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            String strResult = "";

            try
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_U_TBZCODE", connection);
                DataTable dt = new DataTable();

                da.SelectCommand.Parameters.Add(new SqlParameter("@FSCODE_ID", clCODE.FSCODE_ID));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSCODE_CODE", clCODE.FSCODE_CODE));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSCODE_NAME", clCODE.FSCODE_NAME));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSCODE_ORDER", clCODE.FSCODE_ORDER));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSCODE_NOTE", clCODE.FSCODE_NOTE));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSCODE_ISENABLED", clCODE.FSCODE_ISENABLED));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSUPDATED_BY", clCODE.FSUPDATED_BY));


                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                da.Fill(dt);

                if (dt.Rows.Count == 0)
                {
                    strResult = "ERROR:資料庫未回應執行結果";
                }
                else
                {
                    strResult = dt.Rows[0].ItemArray[0].ToString();
                }
            }
            catch (Exception ex)
            {
                strResult = "ERROR:" + ex.Message;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }

            return strResult;
        }

        [WebMethod]
        public string DeleteCode(String m_ID)
        {
            //List<Class.LogTemplateField_CODESET> lstNews = new List<Class.LogTemplateField_CODESET>();
            String sqlConnStr = WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            String strResult = "";

            try
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_D_TBZCODET", connection);
                DataTable dt = new DataTable();

                da.SelectCommand.Parameters.Add(new SqlParameter("@FSCODE_ID", m_ID));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSCODE_CODE", m_ID));

                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                da.Fill(dt);

                if (dt.Rows.Count == 0)
                {
                    strResult = "ERROR:資料庫未回應執行結果";
                }
                else
                {
                    strResult = dt.Rows[0].ItemArray[0].ToString();
                }
            }
            catch (Exception ex)
            {
                strResult = "ERROR:" + ex.Message;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }

            return strResult;
        }
    }
}
