﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;



namespace PTS_MAM3.Web.DataSource
{
  
    [DataContract(Name = "DeptStruct", Namespace = "")]
    public class DeptStruct
    {
        [DataMember]
        public int FNDEP_ID;
        [DataMember]
        public string FSDEP;
        [DataMember]
        public int? FNPARENT_DEP_ID;
        [DataMember]
        public int? FNDEP_LEVEL;
        [DataMember]
        public string FSDEP_MEMO;

      
        [DataMember]
        public string FSSupervisor_ID;
        [DataMember]
        public string FSUpDepName_ChtName;
        [DataMember]
        public string FSDpeName_ChtName;
        [DataMember]
        public string FSFullName_ChtName; 
        [DataMember]
        public string FBIsDepExist; 
    }

}