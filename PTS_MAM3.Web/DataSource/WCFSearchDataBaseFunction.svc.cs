﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using MAM_PTS_DLL;
using System.Xml;
using System.Collections.Generic;


namespace PTS_MAM3.Web.DataSource
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class SearchDataBaseFunction
    {
        /// <summary>
        /// 取得檔案MetaData
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>MetaData</returns>
        [OperationContract]
        public string GetSearchDetailData(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("sp_Q_GET_SEARCH_DETAIL_DATA", parameter);
            return result;
        }

        /// <summary>
        /// 取得影片格式屬性
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>MetaData</returns>
        [OperationContract]
        public string GetVideoProperty(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_GET_VIDEO_PROPERTY", parameter);
            return result;
        }


        /// <summary>
        /// 取得影帶資訊
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>MetaData</returns>
        [OperationContract]
        public string GetTapeData(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_GET_TAPE_DATA", parameter);
            return result;
        }


        /// <summary>
        /// 新增使用者查詢記錄
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <param name="user_id">使用者帳號</param>
        [OperationContract]
        public void InsertSearchHistoryParameter(string parameter,string user_id)
        {
            DbAccess.Do_Transaction("SP_I_TBUSER_SEARCH_HISTORY", parameter, user_id);
        }

        /// <summary>
        /// 新增使用者查詢記錄
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <param name="user_id">使用者帳號</param>
        [OperationContract]
        public void InsertKeyWord(string parameter, string user_id)
        {
            // 在此新增您的作業實作
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(parameter);

            if (xDoc.GetElementsByTagName("FSKEYWORD").Item(0).InnerText != "")
            {

                string[] SplitStr = xDoc.GetElementsByTagName("FSKEYWORD").Item(0).InnerText.Split(':');

                //新增至查詢記錄
                for (int j = 0; j <= SplitStr.Length - 1; j++)
                {
                    xDoc.GetElementsByTagName("FSKEYWORD").Item(0).InnerText = SplitStr[j];
                    parameter = xDoc.InnerXml;
                    DbAccess.Do_Transaction("SP_I_TBSEARCH_KEYWORD", parameter, user_id);
                }
            }
        }

        /// <summary>
        /// 根據索引庫取得檢索欄位
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>檢索欄位名稱</returns>
        [OperationContract]
        public string GetSearchColumnByindex(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_TBSEARCH_TABLE_FIELD_BY_INDEX", parameter);
            return result;
        }

        //
        /// <summary>
        /// 根據類別取得檢索欄位
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>檢索欄位名稱</returns>
        [OperationContract]
        public string GetSearchColumnType(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_TBSEARCH_COLUMN_ENUM", parameter);
            return result;
        }


        /// <summary>
        /// 取得熱門關鍵字
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>熱門關鍵字</returns>
        [OperationContract]
        public string GetHotKeyWord(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_SEARCH_HOT_KEYWORD", parameter);
            return result;
        }

        /// <summary>
        /// 新增熱門點閱
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <param name="user_id">使用者帳號</param>
        [OperationContract]
        public void InsertSearchHotHit(string parameter, string user_id)
        {
            // 在此新增您的作業實作
            DbAccess.Do_Transaction("sp_I_TBSEARCH_HOTHIT", parameter, user_id);
        }


        /// <summary>
        /// 取得熱門點閱
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>熱門點閱</returns>
        [OperationContract]
        public string GetHotHit(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_HOTHIT", parameter);
            return result;
        }

        /// <summary>
        /// 取得查詢記錄
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>查詢記錄</returns>
        [OperationContract]
        public string GetSearchHistory(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_TBUSER_SEARCH_HISTORY", parameter);
            return result;
        }

        /// <summary>
        /// 取得所有同義詞
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>同義詞</returns>
        [OperationContract]
        public string GetSynonym(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_TBSYNONYM", parameter);
            return result;
        }

        /// <summary>
        /// 由編號取得同義詞
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>同義詞</returns>
        [OperationContract]
        public string GetSynonymByNo(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_TBSYNONYM_BY_FNNO", parameter);
            return result;
        }

        /// <summary>
        /// 由字詞取得同義詞
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>同義詞</returns>
        [OperationContract]
        public string GetSynonymByWord(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_SYNONYM_BY_WORD", parameter);
            return result;
        }

        /// <summary>
        /// 檢查同義詞是否存在
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <returns>同義詞</returns>
        [OperationContract]
        public string CheckSynonymExist(string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query("SP_Q_SYNONYM_EXIST", parameter);
            return result;
        }


        /// <summary>
        /// 新增同義詞組
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <param name="user_id">使用者帳號</param>
        [OperationContract]
        public void InsertSynonymList(string parameter, string user_id)
        {
            // 在此新增您的作業實作
            DbAccess.Do_Transaction("SP_I_TBSYNONYM", parameter, user_id);
        }

        /// <summary>
        /// 新增同義詞彙
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <param name="user_id">使用者帳號</param>
        [OperationContract]
        public void InsertSynonym(string parameter, string user_id)
        {
            // 在此新增您的作業實作
            DbAccess.Do_Transaction("SP_I_TBSYNONYM", parameter, user_id);
        }

        /// <summary>
        /// 刪除同義詞組
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <param name="user_id">使用者帳號</param>
        [OperationContract]
        public void DeleteSynonymList(string parameter, string user_id)
        {
            // 在此新增您的作業實作
            DbAccess.Do_Transaction("SP_D_TBSYNONYM", parameter, user_id);
        }

        /// <summary>
        /// 刪除同義詞彙
        /// </summary>
        /// <param name="parameter">參數</param>
        /// <param name="user_id">使用者帳號</param>
        [OperationContract]
        public void DeleteSynonym(string parameter, string user_id)
        {
            // 在此新增您的作業實作
            DbAccess.Do_Transaction("SP_D_TBSYNONYM", parameter, user_id);
        }

        /// <summary>
        /// 取得Keyframe路徑與描述
        /// </summary>
        /// <param name="parameter">參數</param>
        [OperationContract]
        public string GetVideoSegment(string parameter)
        {
            string result = "";
            result = DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_SEG_BY_FSFILENO", parameter);
            return result;
        }

        ///// <summary>
        ///// 取得Keyframe路徑與描述
        ///// </summary>
        ///// <param name="parameter">參數</param>
        //[OperationContract]
        //public List<WS.WSTSM.ServiceTSM.KeyframeInfo> GetKeyFramePathAndDescription(string fsfile_no)
        //{
        //    // 在此新增您的作業實作
        //    WS.WSTSM.ServiceTSM tsm = new WS.WSTSM.ServiceTSM();
        //    List<WS.WSTSM.ServiceTSM.KeyframeInfo> keyframeInfoList = new List<WS.WSTSM.ServiceTSM.KeyframeInfo>();
        //    keyframeInfoList = tsm.GetKeyframeFilesInfo(fsfile_no);

        //    return keyframeInfoList;
        //}

    }
}
