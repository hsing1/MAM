﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Web;
using System.Collections.Generic;
using System.Collections.ObjectModel; 
using System.Data;

namespace PTS_MAM3.Web.DataSource
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ASPNetSession
    {
       
        [OperationContract]
        public List<UserStruct> GetUserSession(string SessionName)
        {

            // Add your operation implementation here
            List<UserStruct> SessionList = new List<UserStruct>();

            if (System.Web.HttpContext.Current.Session[SessionName] != null)
            {
                SessionList.Add((UserStruct)System.Web.HttpContext.Current.Session[SessionName]); 
            }
            else
                SessionList = null; 

            return SessionList; 
        }

        [OperationContract]
        public bool SetSession(string SessionName, Object SessionValue)
        {

            // Add your operation implementation here

            System.Web.HttpContext.Current.Session[SessionName] = SessionValue;

            return true;
        }

        [OperationContract]
        public string GetSession(string SessionName)
        {

            // Add your operation implementation here
            string SessionValue = "";

            if (System.Web.HttpContext.Current.Session[SessionName] != null)
            {
                SessionValue = System.Web.HttpContext.Current.Session[SessionName].ToString();

            }
            else
                SessionValue = null;

            return SessionValue;
        }
    }
}
