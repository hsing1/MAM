﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Text;
using System.Xml.Linq;
using System.IO;
using PTS_MAM3.Web.Class;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSPROGMEN 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSPROGMEN : System.Web.Services.WebService
    {
        List<Class_CODE> m_CodeData = new List<Class_CODE>();       //代碼檔集合
        
        public struct ReturnMsg     //錯誤訊息處理
        {
            public bool bolResult;
            public string strErrorMsg;
        }

        //解析資料庫回來的Dictionary，轉存成Class
        private List<Class_PROGMEN> Transfer_PROGMEN(List<Dictionary<string, string>> sqlResult)
        {
            List<Class_PROGMEN> returnData = new List<Class_PROGMEN>();
            m_CodeData = MAMFunctions.Transfer_Class_CODE("SP_Q_TBPROGMEN_CODE");

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_PROGMEN obj = new Class_PROGMEN();
                // --------------------------------    
                obj.AUTO = sqlResult[i]["AUTO"];                                                        //主Key         
                obj.FSPROG_ID = sqlResult[i]["FSPROG_ID"].Trim();                                       //節目編號
                obj.FSPROG_ID_NAME = sqlResult[i]["FSPROG_ID_NAME"].Trim();                             //節目名稱
                obj.FNEPISODE = Convert.ToInt16(sqlResult[i]["FNEPISODE"]);                             //集別
                obj.SHOW_FNEPISODE = MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]);             //集別(顯示)
                obj.FNEPISODE_NAME = sqlResult[i]["FNEPISODE_NAME"].Trim();                             //子集名稱
                obj.FSTITLEID = sqlResult[i]["FSTITLEID"].Trim();                                       //頭銜代碼
                obj.FSTITLENAME = compareCode_Code(sqlResult[i]["FSTITLEID"].Trim(), "TBZTITLE");       //頭銜代碼名稱
                obj.FSEMAIL = sqlResult[i]["FSEMAIL"].Trim();                                           //E-mail
                obj.FSSTAFFID = sqlResult[i]["FSSTAFFID"].Trim();                                       //工作人員代碼
                obj.FSSTAFFNAME = compareCode_Code(sqlResult[i]["FSSTAFFID"].Trim(), "TBSTAFF");        //工作人員姓名
                obj.FSNAME = sqlResult[i]["FSNAME"].Trim();                                             //公司/姓名               
                obj.FSMEMO = sqlResult[i]["FSMEMO"].Trim();                                             //備註    

                obj.FSCREATED_BY_NAME = sqlResult[i]["FSCREATED_BY_NAME"];                              //建檔者
                obj.FDCREATED_DATE = Convert.ToDateTime(sqlResult[i]["WANT_FDCREATEDDATE"]);            //建檔日期
                obj.SHOW_FDCREATED_DATE = MAMFunctions.compareDate(sqlResult[i]["WANT_FDCREATEDDATE"]); //建檔日期(顯示)
                obj.FSUPDATED_BY_NAME = sqlResult[i]["FSUPDATED_BY_NAME"];                              //修改者
                obj.FDUPDATED_DATE = Convert.ToDateTime(sqlResult[i]["WANT_FDUPDATEDDATE"]);            //修改日期
                obj.SHOW_FDUPDATED_DATE = MAMFunctions.compareDate(sqlResult[i]["WANT_FDUPDATEDDATE"]);//修改日期(顯示)
                // --------------------------------
                returnData.Add(obj);
            }

            return returnData;
        }

        //解析資料庫回來的Dictionary，轉存成Class
        private List<Class_PROGSTAFF> Transfer_PROGSTAFF(List<Dictionary<string, string>> sqlResult)
        {
            List<Class_PROGSTAFF> returnData = new List<Class_PROGSTAFF>();

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_PROGSTAFF obj = new Class_PROGSTAFF();
                // --------------------------------    
                obj.FSSTAFFID = sqlResult[i]["FSSTAFFID"];           //工作人員代碼         
                obj.FSNAME = sqlResult[i]["FSNAME"];                 //姓名
                obj.FSENAME = sqlResult[i]["FSENAME"];               //外文姓名                
                obj.FSTEL = sqlResult[i]["FSTEL"];                   //電話
                obj.FSFAX = sqlResult[i]["FSFAX"];                   //傳真                
                obj.FSEMAIL = sqlResult[i]["FSEMAIL"];               //E-Mail 地址
                obj.FSADDRESS = sqlResult[i]["FSADDRESS"];           //地址
                obj.FSWEBSITE = sqlResult[i]["FSWEBSITE"];           //個人網站
                obj.FSINTRO = sqlResult[i]["FSINTRO"];               //簡介
                obj.FSEINTRO = sqlResult[i]["FSEINTRO"];             //外文簡介                          
                obj.FSMEMO = sqlResult[i]["FSMEMO"];                 //備註 
                // --------------------------------
                returnData.Add(obj);
            }

            return returnData;
        }   

        //相關人員檔的所有代碼檔
        [WebMethod()]
        public List<Class_CODE> fnGetTBPROGMEN_CODE()
        {
            m_CodeData = MAMFunctions.Transfer_Class_CODE("SP_Q_TBPROGMEN_CODE");
            return m_CodeData;
        }

        //相關人員檔的所有資料
        [WebMethod]
        public List<Class_PROGMEN> fnGetTBPROGMEN_ALL()
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROGMEN_ALL", sqlParameters, out sqlResult))
                return new List<Class_PROGMEN>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PROGMEN(sqlResult); 
        }

        //新增相關人員檔
        [WebMethod]
        public ReturnMsg fnINSERT_TBPROMEN(Class.Class_PROGMEN obj)
        {
            ReturnMsg objMsg = new ReturnMsg();

            if (INSERT_TBPROMEN_PGM(obj, out objMsg.strErrorMsg) == true)              //新增節目管理系統的相關人員檔
            {
                if (INSERT_TBPROMEN_MAM(obj, out objMsg.strErrorMsg) == true)          //新增MAM的相關人員檔
                    objMsg.bolResult = true;
                else
                    objMsg.bolResult = false;
            }
            else
                objMsg.bolResult = false;

            return objMsg;   
        }

        //整批新增相關人員檔
        [WebMethod]
        public List<ReturnMsg> fnINSERT_TBPROMEN_ALL(List<Class.Class_PROGMEN> Listobj)
        {
            List<ReturnMsg> ListobjMsg = new List<ReturnMsg>();
            string strMenMsg = "";  //各個子集的內容

            foreach (Class.Class_PROGMEN objMen in Listobj)
            {
                ReturnMsg objMsg = new ReturnMsg();

                strMenMsg = "第「" + objMen.FNEPISODE.ToString() + "」集資料新增失敗，錯誤原因：";

                if (fnQUERY_TBPROGMEN_CHECK(objMen) == "0")
                {
                    if (INSERT_TBPROMEN_PGM(objMen, out objMsg.strErrorMsg) == true)              //新增節目管理系統的相關人員檔
                    {
                        if (INSERT_TBPROMEN_MAM(objMen, out objMsg.strErrorMsg) == true)          //新增MAM的相關人員檔
                            objMsg.bolResult = true;
                        else
                            objMsg.bolResult = false;
                    }
                    else
                        objMsg.bolResult = false;

                    objMsg.strErrorMsg = strMenMsg + objMsg.strErrorMsg;
                }
                else
                {
                    objMsg.bolResult = false;
                    objMsg.strErrorMsg = strMenMsg + "資料庫已有重複資料";
                }

                ListobjMsg.Add(objMsg);
            }

            return ListobjMsg;
        }

        //新增MAM的相關人員檔
        [WebMethod]
        public Boolean INSERT_TBPROMEN_MAM(Class.Class_PROGMEN obj, out string exceptionMsg)
        {
            Boolean bolDB = false;          //寫入節目管理系統資料庫結果
            string strError = "";           //錯誤訊息

            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", obj.FSPROG_ID);

            if (obj.FNEPISODE == 0)
                source.Add("FNEPISODE", "");
            else
                source.Add("FNEPISODE", obj.FNEPISODE.ToString());

            source.Add("FSTITLEID", obj.FSTITLEID);
            source.Add("FSEMAIL", obj.FSEMAIL);
            source.Add("FSSTAFFID", obj.FSSTAFFID);
            source.Add("FSNAME", obj.FSNAME);
            source.Add("FSMEMO", obj.FSMEMO);
            source.Add("FSCREATED_BY", obj.FSCREATED_BY);
            source.Add("FSUPDATED_BY", obj.FSUPDATED_BY);

            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBPROGMEN", source, out strError, obj.FSUPDATED_BY, true);
            exceptionMsg = strError;

            if (bolDB == true)
                return true;
            else
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToMAM-INSERT PROGMEN", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "ERROR:" + strError);
                EMAIL_PROGMEN_Error(obj, strError, "MAM", "INSERT");             //寄送Mail
                return false;
            } 
        }

        //新增節目管理系統的相關人員檔
        [WebMethod]
        public Boolean INSERT_TBPROMEN_PGM(Class.Class_PROGMEN obj, out string exceptionMsg)
        {
            Boolean bolDB = false;          //寫入節目管理系統資料庫結果
            string strError = "";           //錯誤訊息

            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROGID", obj.FSPROG_ID);

            if (obj.FNEPISODE == 0)
                source.Add("FNEPISODE", "");
            else
                source.Add("FNEPISODE", obj.FNEPISODE.ToString());

            source.Add("FSTITLEID", obj.FSTITLEID);
            source.Add("FSEMAIL", obj.FSEMAIL);
            source.Add("FSSTAFFID", obj.FSSTAFFID);
            source.Add("FSNAME", obj.FSNAME);
            source.Add("FSMEMO", obj.FSMEMO);
            source.Add("FSCRTUSER", "MAM");                   //節目管理系統定義由MAM寫入的資料USER皆為 MAM 

            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction_With_DBLink("SP_I_TBPROGMEN_EXTERNAL", source, out strError, obj.FSUPDATED_BY, true);
            exceptionMsg = strError;

            if (bolDB == true)
                return true;
            else
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToPGM-INSERT PROGMEN", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "ERROR:" + strError);
                EMAIL_PROGMEN_Error(obj, strError, "PGM", "INSERT");                   //寄送Mail
                return false;
            }
        }

        //修改相關人員檔
        [WebMethod]
        public ReturnMsg fnUPDATE_TBPROGRACE(Class.Class_PROGMEN obj)
        {
            ReturnMsg objMsg = new ReturnMsg();

            if (UPDATE_TBPROGRACE_PGM(obj, out objMsg.strErrorMsg) == true)              //修改節目管理系統的相關人員檔
            {
                if (UPDATE_TBPROGRACE_MAM(obj, out objMsg.strErrorMsg) == true)          //修改MAM的相關人員檔
                    objMsg.bolResult = true;
                else
                    objMsg.bolResult = false;
            }
            else
                objMsg.bolResult = false;

            return objMsg;   
        }

        //修改MAM的相關人員檔
        [WebMethod]
        public Boolean UPDATE_TBPROGRACE_MAM(Class.Class_PROGMEN obj, out string exceptionMsg)
        {
            Boolean bolDB = false;          //寫入節目管理系統資料庫結果
            string strError = "";           //錯誤訊息

            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("AUTO", obj.AUTO.ToString());
            source.Add("FSPROG_ID", obj.FSPROG_ID);

            if (obj.FNEPISODE == 0)
                source.Add("FNEPISODE", "");
            else
                source.Add("FNEPISODE", obj.FNEPISODE.ToString());

            source.Add("FSTITLEID", obj.FSTITLEID);
            source.Add("FSEMAIL", obj.FSEMAIL);
            source.Add("FSSTAFFID", obj.FSSTAFFID);
            source.Add("FSNAME", obj.FSNAME);
            source.Add("FSMEMO", obj.FSMEMO);
            source.Add("FSUPDATED_BY", obj.FSUPDATED_BY);
            
            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBPROGMEN", source, out strError, obj.FSUPDATED_BY, true);
            exceptionMsg = strError;

            if (bolDB == true)
                return true;
            else
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToMAM-UPDATE PROGMEN", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "ERROR:" + strError);
                EMAIL_PROGMEN_Error(obj, strError, "MAM", "UPDATE");             //寄送Mail
                return false;
            } 
        }

        //修改節目管理系統的相關人員檔
        [WebMethod]
        public Boolean UPDATE_TBPROGRACE_PGM(Class.Class_PROGMEN obj, out string exceptionMsg)
        {
            Boolean bolDB = false;          //寫入節目管理系統資料庫結果
            string strError = "";           //錯誤訊息

            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROGID", obj.FSPROG_ID);

            if (obj.FNEPISODE == 0)
                source.Add("FNEPISODE", "");
            else
                source.Add("FNEPISODE", obj.FNEPISODE.ToString());

            source.Add("FSTITLEID", obj.FSTITLEID);
            source.Add("FSEMAIL", obj.FSEMAIL);
            source.Add("FSSTAFFID", obj.FSSTAFFID);
            source.Add("FSNAME", obj.FSNAME);
            source.Add("FSMEMO", obj.FSMEMO);
            source.Add("FSUPDUSER", "MAM");                   //節目管理系統定義由MAM寫入的資料USER皆為 MAM 

            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction_With_DBLink("SP_U_TBPROGMEN_EXTERNAL", source, out strError, obj.FSUPDATED_BY, true);
            exceptionMsg = strError;

            if (bolDB == true)
                return true;
            else
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToPGM-UPDATE PROGMEN", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "ERROR:" + strError);
                EMAIL_PROGMEN_Error(obj, strError, "PGM", "UPDATE");                   //寄送Mail
                return false;
            }
        }

        //查詢相關人員檔
        [WebMethod]
        public List<Class_PROGMEN> fnQUERY_TBPROGMEN(Class.Class_PROGMEN obj)
        {           
            Dictionary<string, string> source = new Dictionary<string, string>();

            if (obj.FSPROG_ID == null)
                source.Add("FSPROG_ID", "");
            else
                source.Add("FSPROG_ID", obj.FSPROG_ID);            

            if (obj.FNEPISODE == 0)
                source.Add("FNEPISODE", "");
            else
                source.Add("FNEPISODE", obj.FNEPISODE.ToString());

            if (obj.FSTITLEID == null)          //避免沒有填TitleID
                source.Add("FSTITLEID", "");
            else
                source.Add("FSTITLEID", obj.FSTITLEID);

            if (obj.FSEMAIL == null)
                source.Add("FSEMAIL", "");
            else
                source.Add("FSEMAIL", obj.FSEMAIL);

            if (obj.FSSTAFFID == null)
                source.Add("FSSTAFFID", "");
            else
                source.Add("FSSTAFFID", obj.FSSTAFFID);

            if (obj.FSNAME == null)
                source.Add("FSNAME", "");
            else
                source.Add("FSNAME", obj.FSNAME);

            if (obj.FSMEMO == null)
                source.Add("FSMEMO", "");
            else
                source.Add("FSMEMO", obj.FSMEMO);   
                
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROGMEN_BYCONDITIONS", source, out sqlResult))
                return new List<Class_PROGMEN>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PROGMEN(sqlResult); 
        }

        //查詢相關人員檔(要去擋同一集、同頭銜且同名)
        [WebMethod]
        public string fnQUERY_TBPROGMEN_CHECK(Class.Class_PROGMEN obj)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();

            //if (obj.FSSTAFFID == "")    //如果沒有連結工作人員資料，就可以不用檢查，見相同公司/姓名的資料
            //    return "0";

            source.Add("FSPROG_ID", obj.FSPROG_ID);

            if (obj.FNEPISODE == 0)
                source.Add("FNEPISODE", "");
            else
                source.Add("FNEPISODE", obj.FNEPISODE.ToString());

            source.Add("FSTITLEID", obj.FSTITLEID);
            //source.Add("FSSTAFFID", obj.FSSTAFFID);
            source.Add("FSNAME", obj.FSNAME);
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROGMEN_BYCHECK", source, out sqlResult))
                return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count > 0)
                return sqlResult[0]["dbCount"];
            else
                return "";
        }

        //刪除相關人員檔
        [WebMethod]
        public ReturnMsg fnDELETE_TBPROGMEN(Class.Class_PROGMEN obj,string strDelUser)
        {
            ReturnMsg objMsg = new ReturnMsg();

            if (DELETE_TBPROGMEN_PGM(obj, out objMsg.strErrorMsg, strDelUser) == true)              //刪除節目管理系統的相關人員檔
            {
                if (DELETE_TBPROGMEN_MAM(obj, out objMsg.strErrorMsg, strDelUser) == true)          //刪除MAM的相關人員檔
                    objMsg.bolResult = true;
                else
                    objMsg.bolResult = false;
            }
            else
                objMsg.bolResult = false;

            return objMsg; 
        }

        //刪除MAM的相關人員檔
        [WebMethod]
        public Boolean DELETE_TBPROGMEN_MAM(Class.Class_PROGMEN obj, out string exceptionMsg, string strDelUser)
        {
            Boolean bolDB = false;          //寫入節目管理系統資料庫結果
            string strError = "";           //錯誤訊息

            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("AUTO", obj.AUTO.ToString().Trim());

            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_TBPROGMEN", source, out strError, strDelUser, true);
            exceptionMsg = strError;

            if (bolDB == true)
                return true;
            else
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToMAM-DELETE PROGMEN", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "ERROR:" + strError);
                EMAIL_PROGMEN_Error(obj, strError, "MAM", "DELETE");             //寄送Mail
                return false;
            }
        }

        //刪除節目管理統的相關人員檔
        [WebMethod]
        public Boolean DELETE_TBPROGMEN_PGM(Class.Class_PROGMEN obj, out string exceptionMsg, string strDelUser)
        {
            Boolean bolDB = false;          //寫入節目管理系統資料庫結果
            string strError = "";           //錯誤訊息

            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROGID", obj.FSPROG_ID);

            if (obj.FNEPISODE == 0)
                source.Add("FNEPISODE", "");
            else
                source.Add("FNEPISODE", obj.FNEPISODE.ToString());

            source.Add("FSTITLEID", obj.FSTITLEID);

            if (obj.FSEMAIL == null)
                source.Add("FSEMAIL", "");
            else
                source.Add("FSEMAIL", obj.FSEMAIL);

            source.Add("FSSTAFFID", obj.FSSTAFFID);

            if (obj.FSNAME == null)
                source.Add("FSNAME", "");
            else
                source.Add("FSNAME", obj.FSNAME);

            if (obj.FSMEMO == null)
                source.Add("FSMEMO", "");
            else
                source.Add("FSMEMO", obj.FSMEMO); 

            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction_With_DBLink("SP_D_TBPROGMEN_EXTERNAL", source, out strError, strDelUser, true);
            exceptionMsg = strError;

            if (bolDB == true)
                return true;
            else
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToPGM-DELETE PROGMEN", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "ERROR:" + strError);
                EMAIL_PROGMEN_Error(obj, strError, "PGM", "DELETE");                   //寄送Mail
                return false;
            }
        }

        //查詢節目管理系統的工作人員代碼檔，撈出全部的資料
        [WebMethod]
        public List<Class_PROGSTAFF> fnGetTBPROGSTAFF()
        {  
            Dictionary<string, string> source = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROGMEN_ALLSTAFF", source, out sqlResult))
                return new List<Class_PROGSTAFF>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PROGSTAFF(sqlResult); 
        }


        //測試節目管理系統的工作人員代碼檔
        [WebMethod]
        public string fnTestTBPROGSTAFF(Class.Class_PROGSTAFF obj)
        {
            return MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROGMEN_ALLSTAFF", "");
        }
        
        //比對代碼檔
        string compareCode_Code(string strID, string strTable)
        {
            if (strID != "")
                strID = strID.Trim();
            else
                return "";

            for (int i = 0; i < m_CodeData.Count; i++)
            {
                if (m_CodeData[i].ID.ToString().Trim().Equals(strID) && m_CodeData[i].TABLENAME.ToString().Trim().Equals(strTable))
                {
                    return m_CodeData[i].NAME.ToString().Trim();
                }
            }
            return "";
        }  
           
        //寄送EMAIL通知_資料同步時發生問題要通知管理者
        [WebMethod]
        public void EMAIL_PROGMEN_Error(Class.Class_PROGMEN obj, string strError, string strSystem, string strStatus)
        {
            //讀取通知的管理者(暫用)   
            MAM_PTS_DLL.SysConfig objDll = new MAM_PTS_DLL.SysConfig();
            string strErrorMail = objDll.sysConfig_Read("/ServerConfig/SynchronizeError").Trim();
            string strPROG_NAME = MAMFunctions.QueryProgName(obj.FSPROG_ID.Trim());
            string strTitle = "";
            m_CodeData = MAMFunctions.Transfer_Class_CODE("SP_Q_TBPROGMEN_CODE");

            StringBuilder msg = new StringBuilder();
            msg.AppendLine("節目名稱：" + strPROG_NAME);
            msg.AppendLine(" , 節目編號：" + obj.FSPROG_ID.Trim());
            msg.AppendLine(" , 集別：" + obj.FNEPISODE.ToString().Trim());
            msg.AppendLine(" , 頭銜名稱：" + compareCode_Code(obj.FSTITLEID.ToString().Trim(),"TBZTITLE"));
            msg.AppendLine(" , 工作人員姓名：" + compareCode_Code(obj.FSSTAFFID.ToString().Trim(), "TBSTAFF"));

            if (strSystem.Trim() == "MAM")
                msg.AppendLine(" , 同步失敗資料庫：數位片庫");
            else if (strSystem.Trim() == "PGM")
                msg.AppendLine(" , 同步失敗資料庫：節目管理系統");

            if (strStatus.Trim() == "INSERT")
                msg.AppendLine(" , 動作：新增");
            else if (strStatus.Trim() == "UPDATE")
                msg.AppendLine(" , 動作：修改");
            else if (strStatus.Trim() == "DELETE")
                msg.AppendLine(" , 動作：刪除");

            msg.AppendLine(" , 錯誤訊息：" + strError);

            msg.AppendLine(" , 新增者：" + obj.FSCREATED_BY.Trim() + " , 修改者：" + obj.FSUPDATED_BY.Trim());

            strTitle = "節目相關人員資料同步失敗通知-" + strPROG_NAME;

            MAM_PTS_DLL.Protocol.SendEmail(strErrorMail, strTitle, msg.ToString());
        }
    }
}
