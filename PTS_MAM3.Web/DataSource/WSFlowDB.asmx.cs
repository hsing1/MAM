﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSFlowDB 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSFlowDB : System.Web.Services.WebService
    {
        [WebMethod]
        public bool CompareGroupUserExist(string USERID,string GroupID)
        {
            bool returnbool=false;
            List<Class.ClassFlowGroupUsers> compareList = new List<Class.ClassFlowGroupUsers>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("GroupID", GroupID);
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query_Flow("SP_Q_GROUPUSERS", sqlParameters, out sqlResult))
                return false;
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class.ClassFlowGroupUsers obj = new Class.ClassFlowGroupUsers();
                obj.GroupID = sqlResult[i]["Group_GUID"];
                obj.UserId = sqlResult[i]["UserId"];
                obj.Member_DisplayName = sqlResult[i]["Member_DisplayName"];
                compareList.Add(obj);
            }
            if (compareList.Where(s => s.UserId == USERID).Count() > 0)
            { returnbool = true; }
            return returnbool;
        }
    }
}
