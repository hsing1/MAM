﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Data;
using System.Text;
using System.Xml.Linq;
using System.IO;


namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSVideoMetaEdit 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSVideoMetaEdit : System.Web.Services.WebService
    {

        /// <summary>
        /// 刪除影像段落
        /// </summary>
        /// <param name="fsfile_no">檔案編號</param>
        /// <param name="fnseq_no">段落</param>
        /// <param name="fsuser_id">使用者帳號</param>
        /// <returns>true(成功)/false(失敗) </returns>
        [WebMethod]
        public Boolean fnDeleteTBLOG_VIDEO_D(string fsfile_no, int fnseq_no, string fsuser_id)
        {
            Boolean brtn = false;

            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSFILE_NO", fsfile_no);
            source.Add("FNSEQ_NO", fnseq_no.ToString());

            brtn = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_TBLOG_VIDEO_D", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source), fsuser_id);

            return brtn; 
        }




        /// <summary>
        /// 根據Video_D 的Range 取得keyframe list 
        /// </summary>
        /// <param name="fsvideo_no">影像檔紀錄號</param>
        /// <param name="rangeStart">Video D 的Start Time</param>
        /// <param name="rangeEnd">Video D 的End Time</param>
        /// <returns>Keyframe List</returns>
        [WebMethod]
        public ObservableCollection<Class.Class_Video_Keyframe> fnGetTBLOG_VIDEO_KEYFRAME_IN_RANGE(string fsvideo_no, string rangeStart, string rangeEnd)
        {
            string rtn = "";
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSVIDEO_NO", fsvideo_no);
            source.Add("FSRANGE_START", rangeStart);
            source.Add("FSRANGE_END", rangeEnd);

            rtn = MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_KEYFRAME_IN_RANGE", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source));


            ObservableCollection<Class.Class_Video_Keyframe> VIDEO_Keyframe_List = new ObservableCollection<Class.Class_Video_Keyframe>();

            if (!string.IsNullOrEmpty(rtn))
            {
                byte[] byteArray = Encoding.Unicode.GetBytes(rtn);
                XDocument doc = XDocument.Load(new MemoryStream(byteArray));

                WS.WSTSM.ServiceTSM TSMObj = new WS.WSTSM.ServiceTSM();
                string url = TSMObj.GetKeyframeFiles(fsvideo_no).FirstOrDefault();
                //System.IO.FileInfo fin = new System.IO.FileInfo(path);

                if (!string.IsNullOrEmpty(url))
                {
                    string[] sp = { fsvideo_no + "_" };
                    url = url.Split(sp, StringSplitOptions.RemoveEmptyEntries)[0];


                }
                else
                {
                    url = "";
                }





                var ltox = from s in doc.Elements("Datas").Elements("Data")
                           select new Class.Class_Video_Keyframe
                           {
                               //FSBEG_TIMECODE = (string)s.Element("FSBEG_TIMECODE"),
                               FDCREATED_DATE = (string)s.Element("FDCREATED_DATE"),
                               FNDIR_ID = (int)s.Element("FNDIR_ID"),
                               FNFILE_SIZE = (int)s.Element("FNFILE_SIZE"),
                               FSCREATED_BY = (string)s.Element("FSCREATED_BY"),
                               FSDESCRIPTION = (string)s.Element("FSDESCRIPTION"),
                               FSFILE_NO = (string)s.Element("FSFILE_NO"),
                               FSSERVER_NAME = (string)s.Element("FSSERVER_NAME"),
                               FSSHARE_FOLDER = (string)s.Element("FSSHARE_FOLDER"),
                               FSSUBJECT_ID = (string)s.Element("FSSUBJECT_ID"),
                               FSVIDEO_NO = (string)s.Element("FSVIDEO_NO"),

                               path = url + (string)s.Element("FSFILE_NO") + ".jpg" 
                               //path = "http://attach2.mobile01.com/image/news/6da14cfbbce826cff0a4a05e13261a3a.jpg"

                           };
                foreach (Class.Class_Video_Keyframe obj in ltox)
                {
                    VIDEO_Keyframe_List.Add(obj);
                }

            }



            return VIDEO_Keyframe_List; 
            
        }


        [WebMethod]
        public Boolean fnUpdateTBLOG_VIDEO_KEYFRAME(Class.Class_Video_Keyframe videoKeyframeObj, string fsuser_id)
        {
            Boolean brtn = false;
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSFILE_NO", videoKeyframeObj.FSFILE_NO);
            source.Add("FSSUBJECT_ID", videoKeyframeObj.FSSUBJECT_ID);
            source.Add("FNFILE_SIZE", videoKeyframeObj.FNFILE_SIZE.ToString() );
            source.Add("FNDIR_ID", videoKeyframeObj.FNDIR_ID.ToString() );
            source.Add("FSSERVER_NAME", videoKeyframeObj.FSSERVER_NAME);
            source.Add("FSSHARE_FOLDER", videoKeyframeObj.FSSHARE_FOLDER);
            source.Add("FSDESCRIPTION", videoKeyframeObj.FSDESCRIPTION);
            brtn = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBLOG_VIDEO_KEYFRAME", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source), fsuser_id);
            return brtn; 
        }


        [WebMethod]
        public ObservableCollection<Class.Class_Video_Keyframe> fnGetTBLOG_VIDEO_KEYFRAME(string fsvideo_no)
        {
            string rtn = "";
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSVIDEO_NO", fsvideo_no);


            ObservableCollection<Class.Class_Video_Keyframe> VIDEO_Keyframe_List = new ObservableCollection<Class.Class_Video_Keyframe>();
            rtn = MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_KEYFRAME", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source));


            if (!string.IsNullOrEmpty(rtn))
            {
                byte[] byteArray = Encoding.Unicode.GetBytes(rtn);
                XDocument doc = XDocument.Load(new MemoryStream(byteArray));

                WS.WSTSM.ServiceTSM TSMObj = new WS.WSTSM.ServiceTSM();
                string url = TSMObj.GetKeyframeFiles(fsvideo_no).FirstOrDefault();
                //System.IO.FileInfo fin = new System.IO.FileInfo(path);

                if (!string.IsNullOrEmpty(url))
                {
                    string[] sp = { fsvideo_no + "_" };
                    url = url.Split(sp, StringSplitOptions.RemoveEmptyEntries)[0];


                }
                else
                {
                    url = "";
                }

                
          


                var ltox = from s in doc.Elements("Datas").Elements("Data")
                           select new Class.Class_Video_Keyframe
                           {
                               //FSBEG_TIMECODE = (string)s.Element("FSBEG_TIMECODE"),
                               FDCREATED_DATE = (string)s.Element("FDCREATED_DATE"), 
                               FNDIR_ID = (int) s.Element("FNDIR_ID") ,
                               FNFILE_SIZE = (int)s.Element("FNFILE_SIZE"),
                               FSCREATED_BY = (string)s.Element("FSCREATED_BY"),
                               FSDESCRIPTION = (string)s.Element("FSDESCRIPTION"), 
                               FSFILE_NO = (string) s.Element("FSFILE_NO"),
                               FSSERVER_NAME = (string)s.Element("FSSERVER_NAME"),
                               FSSHARE_FOLDER = (string)s.Element("FSSHARE_FOLDER"),
                               FSSUBJECT_ID = (string)s.Element("FSSUBJECT_ID") ,
                               FSVIDEO_NO = (string)s.Element("FSVIDEO_NO") , 
                             
                               path = url + (string)s.Element("FSFILE_NO") + ".jpg" 
                               //path = "http://attach2.mobile01.com/image/news/6da14cfbbce826cff0a4a05e13261a3a.jpg" 
                        
                           };
                foreach (Class.Class_Video_Keyframe obj in ltox)
                {
                    VIDEO_Keyframe_List.Add(obj);
                }

            }



            return VIDEO_Keyframe_List; 
        }




        [WebMethod]
        public Boolean fnUpdateTBLOG_VIDEO_D(Class.Class_Video_D obj, string fsuser_id)
        {
            Boolean brtn = false;
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FNSEQ_NO", obj.FNSEQ_NO); 
            source.Add("FCFILE_STATUS", obj.FCFILE_STATUS);
            source.Add("FNDIR_ID", obj.FNDIR_ID);
            source.Add("FNEPISODE", obj.FNEPISODE.ToString());
            source.Add("FSSUPERVISOR", obj.FSSUPERVISOR.ToString());
            source.Add("FSBEG_TIMECODE", obj.FSBEG_TIMECODE);
            source.Add("FSCHANGE_FILE_NO", obj.FSCHANGE_FILE_NO);
            source.Add("FSCHANNEL_ID", obj.FSCHANNEL_ID);
            source.Add("FSCREATED_BY", obj.FSCREATED_BY);
            source.Add("FSDESCRIPTION", obj.FSDESCRIPTION);
            source.Add("FSEND_TIMECODE", obj.FSEND_TIMECODE);
            source.Add("FSFILE_NO", obj.FSFILE_NO);
            source.Add("FSID", obj.FSID);
            source.Add("FSKEYFRAME_NO", obj.FSKEYFRAME_NO);
            source.Add("FSOLD_FILE_NAME", obj.FSOLD_FILE_NAME);
            source.Add("FSSUBJECT_ID", obj.FSSUBJECT_ID);
            source.Add("FSTITLE", obj.FSTITLE);
            source.Add("FSTYPE", obj.FSTYPE);
            source.Add("FSUPDATED_BY", obj.FSUPDATED_BY);
            source.Add("FCFROM", obj.FCFROM); 
           
            
            brtn = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBLOG_VIDEO_D", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source), fsuser_id);
            return brtn; 

        }




        [WebMethod]
        public Boolean fnInsertTBLOG_VIDEO_D(Class.Class_Video_D obj, string fsuser_id )
        {
            Boolean brtn = false;
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FCFILE_STATUS", obj.FCFILE_STATUS);
            source.Add("FNDIR_ID", obj.FNDIR_ID );
            source.Add("FNEPISODE", obj.FNEPISODE.ToString() );
            source.Add("FSSUPERVISOR", obj.FSSUPERVISOR.ToString()); 
            source.Add("FSBEG_TIMECODE", obj.FSBEG_TIMECODE);
            source.Add("FSCHANGE_FILE_NO", obj.FSCHANGE_FILE_NO);
            source.Add("FSCHANNEL_ID", obj.FSCHANNEL_ID);
            source.Add("FSCREATED_BY", obj.FSCREATED_BY);
            source.Add("FSDESCRIPTION", obj.FSDESCRIPTION);
            source.Add("FSEND_TIMECODE", obj.FSEND_TIMECODE);
            source.Add("FSFILE_NO", obj.FSFILE_NO);
            source.Add("FSID", obj.FSID);
            source.Add("FSKEYFRAME_NO", obj.FSKEYFRAME_NO);
            source.Add("FSOLD_FILE_NAME", obj.FSOLD_FILE_NAME);
            source.Add("FSSUBJECT_ID", obj.FSSUBJECT_ID);
            source.Add("FSTITLE", obj.FSTITLE);
            source.Add("FSTYPE", obj.FSTYPE);
            source.Add("FSUPDATED_BY", obj.FSUPDATED_BY);
            source.Add("FCFROM", obj.FCFROM);
            source.Add("FSTAPE_ID", "");            //影帶編號 
            source.Add("FSTRANS_FROM", "");         //轉檔來源

            brtn = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBLOG_VIDEO_D", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source), fsuser_id);
            return brtn; 

        }


      
        [WebMethod]
        public ObservableCollection<Class.Class_Video_D> fnGetTBLOG_VIDEO_D(string fsfile_no, int fnseq_no)
        {

            string rtn = ""; 
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSFILE_NO", fsfile_no);
            source.Add("FNSEQ_NO", fnseq_no.ToString() );


            ObservableCollection<Class.Class_Video_D> VIDEO_D_List = new ObservableCollection<Class.Class_Video_D>(); 
            rtn = MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_D", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source));


            if (!string.IsNullOrEmpty(rtn))
            {

                //byte[] byteArray = Encoding.Unicode.GetBytes(rtn);
                //XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                XDocument doc = XDocument.Parse(rtn);


                var ltox = from s in doc.Elements("Datas").Elements("Data")
                           select new Class.Class_Video_D
                           {
                               FSBEG_TIMECODE = (string) s.Element("FSBEG_TIMECODE"),
                               FSCHANGE_FILE_NO = (string) s.Element("FSCHANGE_FILE_NO"), 
                               FSCHANNEL_ID = (string) s.Element("FSCHANNEL_ID"), 
                               FSCREATED_BY = (string) s.Element("FSCREATED_BY"), 
                               FSDESCRIPTION = (string) s.Element("FSDESCRIPTION"),
                               FSEND_TIMECODE = (string)s.Element("FSEND_TIMECODE"),
                               FSFILE_NO = (string)s.Element("FSFILE_NO"),
                               FSID = (string) s.Element("FSID"), 
                               FSKEYFRAME_NO = (string) s.Element("FSKEYFRAME_NO") , 
                               FSOLD_FILE_NAME = (string) s.Element("FSOLD_FILE_NAME"),
                               FSSUBJECT_ID = (string)s.Element("FSSUBJECT_ID"), 
                               FSTITLE = (string) s.Element("FSTITLE"), 
                               FSTYPE = (string) s.Element("FSTYPE"),
                               FSUPDATED_BY = (string)s.Element("FSUPDATED_BY"),
                                FCFILE_STATUS = (string) s.Element("FCFILE_STATUS"),
                                FDCREATED_DATE = (string)s.Element("FDCREATED_DATE"),
                                FDUPDATED_DATE = (string)s.Element("FDUPDATED_DATE"),
                                FNDIR_ID = (string)s.Element("FNDIR_ID") ,
                                FNEPISODE = (int)s.Element("FNEPISODE"),
                                FNSEQ_NO = (string)s.Element("FNSEQ_NO"),
                                FCFROM = (string) s.Element("FCFROM"), 
                                FSSUPERVISOR = (string)s.Element("FSSUPERVISOR"),                              
                                FSATTRIBUTE1 = (string)s.Element("FSATTRIBUTE1"),
                                FSATTRIBUTE2 = (string)s.Element("FSATTRIBUTE2"),
                                FSATTRIBUTE3 = (string)s.Element("FSATTRIBUTE3"),
                                FSATTRIBUTE4 = (string)s.Element("FSATTRIBUTE4"),
                                FSATTRIBUTE5 = (string)s.Element("FSATTRIBUTE5"),
                                FSATTRIBUTE6 = (string)s.Element("FSATTRIBUTE6"),
                                FSATTRIBUTE7 = (string)s.Element("FSATTRIBUTE7"),
                                FSATTRIBUTE8 = (string)s.Element("FSATTRIBUTE8"),
                                FSATTRIBUTE9 = (string)s.Element("FSATTRIBUTE9"),
                                FSATTRIBUTE10 = (string)s.Element("FSATTRIBUTE10"),
                                FSATTRIBUTE11 = (string)s.Element("FSATTRIBUTE11"),
                                FSATTRIBUTE12 = (string)s.Element("FSATTRIBUTE12"),
                                FSATTRIBUTE13 = (string)s.Element("FSATTRIBUTE13"),
                                FSATTRIBUTE14 = (string)s.Element("FSATTRIBUTE14"),
                                FSATTRIBUTE15 = (string)s.Element("FSATTRIBUTE15"),
                                FSATTRIBUTE16 = (string)s.Element("FSATTRIBUTE16"),
                                FSATTRIBUTE17 = (string)s.Element("FSATTRIBUTE17"),
                                FSATTRIBUTE18 = (string)s.Element("FSATTRIBUTE18"),
                                FSATTRIBUTE19 = (string)s.Element("FSATTRIBUTE19"),
                                FSATTRIBUTE20 = (string)s.Element("FSATTRIBUTE20"),
                                FSATTRIBUTE21 = (string)s.Element("FSATTRIBUTE21"),
                                FSATTRIBUTE22 = (string)s.Element("FSATTRIBUTE22"),
                                FSATTRIBUTE23 = (string)s.Element("FSATTRIBUTE23"),
                                FSATTRIBUTE24 = (string)s.Element("FSATTRIBUTE24"),
                                FSATTRIBUTE25 = (string)s.Element("FSATTRIBUTE25"),
                                FSATTRIBUTE26 = (string)s.Element("FSATTRIBUTE26"),
                                FSATTRIBUTE27 = (string)s.Element("FSATTRIBUTE27"),
                                FSATTRIBUTE28 = (string)s.Element("FSATTRIBUTE28"),
                                FSATTRIBUTE29 = (string)s.Element("FSATTRIBUTE29"),
                                FSATTRIBUTE30 = (string)s.Element("FSATTRIBUTE30"),
                                FSATTRIBUTE31 = (string)s.Element("FSATTRIBUTE31"),
                                FSATTRIBUTE32 = (string)s.Element("FSATTRIBUTE32"),
                                FSATTRIBUTE33 = (string)s.Element("FSATTRIBUTE33"),
                                FSATTRIBUTE34 = (string)s.Element("FSATTRIBUTE34"),
                                FSATTRIBUTE35 = (string)s.Element("FSATTRIBUTE35"),
                                FSATTRIBUTE36 = (string)s.Element("FSATTRIBUTE36"),
                                FSATTRIBUTE37 = (string)s.Element("FSATTRIBUTE37"),
                                FSATTRIBUTE38 = (string)s.Element("FSATTRIBUTE38"),
                                FSATTRIBUTE39 = (string)s.Element("FSATTRIBUTE39"),
                                FSATTRIBUTE40 = (string)s.Element("FSATTRIBUTE40"),
                                FSATTRIBUTE41 = (string)s.Element("FSATTRIBUTE41"),
                                FSATTRIBUTE42 = (string)s.Element("FSATTRIBUTE42"),
                                FSATTRIBUTE43 = (string)s.Element("FSATTRIBUTE43"),
                                FSATTRIBUTE44 = (string)s.Element("FSATTRIBUTE44"),
                                FSATTRIBUTE45 = (string)s.Element("FSATTRIBUTE45"),
                                FSATTRIBUTE46 = (string)s.Element("FSATTRIBUTE46"),
                                FSATTRIBUTE47 = (string)s.Element("FSATTRIBUTE47"),
                                FSATTRIBUTE48 = (string)s.Element("FSATTRIBUTE48"),
                                FSATTRIBUTE49 = (string)s.Element("FSATTRIBUTE49"),
                                FSATTRIBUTE50 = (string)s.Element("FSATTRIBUTE50")
                           };
                foreach (Class.Class_Video_D obj in ltox)
                {
                    VIDEO_D_List.Add(obj);
                }


                return VIDEO_D_List;  
            }
            else
                return null;



        }

        


    }
}
