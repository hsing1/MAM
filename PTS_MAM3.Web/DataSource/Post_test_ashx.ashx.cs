﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    ///Post_test_ashx 的摘要描述
    /// </summary>
    public class Post_test_ashx : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            HttpRequest req = context.Request;
            StreamReader sr = new StreamReader(req.InputStream);
            string inputData = sr.ReadToEnd();
            sr.Close();

            MAM_PTS_DLL.Log.AppendTrackingLog("HTTP_POST_TEST_LOG", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, inputData);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}