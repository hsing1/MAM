﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Xml;
using System.Text;
using System.Xml.Linq;
using System.IO;
using PTS_MAM3.Web.Class;
using System.Collections.ObjectModel;
using System.Web.Configuration;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSREPORT 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSREPORT : System.Web.Services.WebService
    {

        ObservableCollection<DeptStruct> m_DeptStruct = new ObservableCollection<DeptStruct>();

        //依據單位ID取得單位清單
        [WebMethod()]
        public ObservableCollection<DeptStruct> fnGetDeptList(int FNPARENT_DEP_ID)
        {
            WSUserAdmin ua = new WSUserAdmin();
            m_DeptStruct = ua.GetDeptList(FNPARENT_DEP_ID);
            return m_DeptStruct; 
        }

        ObservableCollection<UserStruct> m_UserStruct = new ObservableCollection<UserStruct>();

        //依據單位ID取得使用者清單
        [WebMethod()]
        public ObservableCollection<UserStruct> fnGetTBUSERS_BY_DEPT_ID(string fsgroup_id)
        {
            WSUserObject uo = new WSUserObject();
            m_UserStruct = uo.fnGetTBUSERS_BY_DEPT_ID(fsgroup_id);
            return m_UserStruct;
        }


        //列印Log統計表
        [WebMethod]
        public string GetRPT100Sting(string _FSUSER_ID, string _FDDATE_S, string _FDDATE_E, 
                                     string _FSSP_NAME, string _QUERY_BY)
        {
            String strURL = "";  //要傳回去的URL

            //取列印報表的系統設定檔
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string reportSrv = SettingObj.sysConfig_Read("/ServerConfig/ReportServer");

            strURL = reportSrv + "RPT_TBTRAN_LOG_01&FSUSER_ID=" + _FSUSER_ID +
                                                  "&FDDATE_S=" + _FDDATE_S +
                                                  "&FDDATE_E=" + _FDDATE_E +
                                                  "&FSSP_NAME=" + _FSSP_NAME +
                                                  "&QUERY_BY=" + _QUERY_BY +
                                 "&rc:parameters=false&rs:Command=Render";
            return Server.UrlEncode(strURL);
        }
        #region 冠宇的報表WS
        [WebMethod]
        public string GetRPT_STO_01(string sDateYear, string sDateMonth, string eDateYear, string eDateMonth,string creater, string reportNumber)
        {
            string strURL = "";
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string reportSrv = SettingObj.sysConfig_Read("/ServerConfig/ReportServer");
            if (reportNumber == "1")//頻道轉檔內容分析
            {
                strURL = reportSrv + "RPT_TBARCHIVE_07&BeginYear=" + sDateYear +
                                                "&BeginMonth=" + sDateMonth +
                                                "&EndYear=" + eDateYear +
                                                "&EndMonth=" + eDateMonth +
                                                "&CREATED_BY=" + creater + "&rc:parameters=false&rs:Command=Render";
            }
            else if (reportNumber == "2")//入庫量分析
            {
                strURL = reportSrv + "RPT_TBARCHIVE_02&BeginYear=" + sDateYear +
                                "&BeginMonth=" + sDateMonth +
                                "&EndYear=" + eDateYear +
                                "&EndMonth=" + eDateMonth +
                                "&CREATED_BY=" + creater + "&rc:parameters=false&rs:Command=Render";
            }
            else if (reportNumber == "3")//頻道入庫分析
            {
                strURL = reportSrv + "RPT_TBARCHIVE_03&BeginYear=" + sDateYear +
                                "&BeginMonth=" + sDateMonth +
                                "&EndYear=" + eDateYear +
                                "&EndMonth=" + eDateMonth +
                                "&CREATED_BY=" + creater + "&rc:parameters=false&rs:Command=Render";
            }
            else if (reportNumber == "4")//頻道轉檔分析
            {
                strURL = reportSrv + "RPT_TBARCHIVE_06&BeginYear=" + sDateYear +
                                "&BeginMonth=" + sDateMonth +
                                "&EndYear=" + eDateYear +
                                "&EndMonth=" + eDateMonth +
                                "&CREATED_BY=" + creater + "&rc:parameters=false&rs:Command=Render";
            }
            else if (reportNumber == "5")//館藏數量統計
            {
                strURL = reportSrv + "RPT_TBARCHIVE_04"+
                                "&CREATED_BY=" + creater + "&rc:parameters=false&rs:Command=Render";
            }
            else if (reportNumber == "6")//影片類型分析
            {
                strURL = reportSrv + "RPT_TBARCHIVE_05" +
                                "&CREATE_BY=" + creater + "&rc:parameters=false&rs:Command=Render";
            }
            return Server.UrlEncode(strURL);
        }
        [WebMethod]
        public string GetRPT_STO_02(string FSID, string FSEPISODE)
        {
            string strURL = "";
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string reportSrv = SettingObj.sysConfig_Read("/ServerConfig/ReportServer");
            strURL = reportSrv + "RPT_TBARCHIVE_08&FSID=" + FSID + "&EPISODE=" + FSEPISODE + "&rc:parameters=false&rs:Command=Render";
            return Server.UrlEncode(strURL);
        }
        #endregion
        //主控FTP容量明細表
        [WebMethod]
        public string GetFTP_01Sting(string _FSCHANNEL_ID, string _DATE, string _QUERY_BY)
        {
            String strURL = "";  //要傳回去的URL

            //取列印報表的系統設定檔
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string reportSrv = SettingObj.sysConfig_Read("/ServerConfig/ReportServer");

            strURL = reportSrv + "RPT_TBLOG_MASTERCONTROL_FTP_01&FSCHANNEL_ID=" + _FSCHANNEL_ID +
                                                  "&DATE=" + _DATE +
                                                  "&QUERY_BY=" + _QUERY_BY +
                                 "&rc:parameters=false&rs:Command=Render";
            return Server.UrlEncode(strURL);
        }


        //主控FTP容量統計表
        [WebMethod]
        public string GetFTP_02Sting(string _DATE, string _QUERY_BY)
        {
            String strURL = "";  //要傳回去的URL

            //取列印報表的系統設定檔
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string reportSrv = SettingObj.sysConfig_Read("/ServerConfig/ReportServer");

            strURL = reportSrv + "RPT_TBLOG_MASTERCONTROL_FTP_02" +
                                                  "&DATE=" + _DATE +
                                                  "&QUERY_BY=" + _QUERY_BY +
                                 "&rc:parameters=false&rs:Command=Render";
            return Server.UrlEncode(strURL);
        }
        
        //查詢部門檔
        [WebMethod]
        public DeptStruct QUERY_TBUSER_DEP_BYID(string strDepID)
        {            
            DeptStruct obj = new DeptStruct();    
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FNDEP_ID", strDepID);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBUSER_DEP_BYID", sqlParameters, out sqlResult))
                return obj;    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count > 0)
            {
                if (sqlResult[0]["FNDEP_ID"] != null && sqlResult[0]["FNDEP_ID"] != "")
                    obj.FNDEP_ID = Convert.ToInt32(sqlResult[0]["FNDEP_ID"]);         //組織
                else
                    obj.FNDEP_ID = 0;

                if (sqlResult[0]["FNPARENT_DEP_ID"] != null && sqlResult[0]["FNPARENT_DEP_ID"] !="" )
                    obj.FNPARENT_DEP_ID = Convert.ToInt32(sqlResult[0]["FNPARENT_DEP_ID"]);//上層組織
                else
                    obj.FNPARENT_DEP_ID = 0;

                if (sqlResult[0]["FNDEP_LEVEL"] != null && sqlResult[0]["FNDEP_LEVEL"] !="" )
                    obj.FNDEP_LEVEL = Convert.ToInt32(sqlResult[0]["FNDEP_LEVEL"]);         //層級
                else
                    obj.FNDEP_LEVEL = 0;
            }
            
            return obj;
        }
          
    }
}
