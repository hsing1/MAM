﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Configuration;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using System.Data.Odbc;
using System.Xml;
using System.Text;
using System.IO;
using System.Net;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WebService1 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {

        //[WebMethod]
        //public string HelloWorld()
        //{
        //    return "Hello World";
        //}

        [WebMethod]
        public string TransProglist(string channelid, string proglistdate)
        {
           String sqlConnStr = "";
           sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
           //SqlConnection myconnection = new SqlConnection(sqlConnStr);
           SqlConnection myconnection1 = new SqlConnection(sqlConnStr);
           SqlConnection myconnection2 = new SqlConnection(sqlConnStr);
           SqlConnection myconnection3 = new SqlConnection(sqlConnStr);
           if (channelid == "N")
            {
            }
            DataTable DT = new DataTable();

            DT.Columns.Add("FSPROGID", typeof(string));
            DT.Columns.Add("FNEPISODE", typeof(string));
            DT.Columns.Add("FDDATE", typeof(string));
            DT.Columns.Add("FSBTIME", typeof(string));
            DT.Columns.Add("FSDURATION", typeof(string));
            DT.Columns.Add("FNREPLAY", typeof(string));
            DT.Columns.Add("FSKSE", typeof(string));
            DT.Columns.Add("FSLIVE", typeof(string));
            DT.Columns.Add("FSDEP", typeof(string));
            DT.Columns.Add("FSETIME", typeof(string));
            DT.Columns.Add("FNSEQNO", typeof(string));
            DT.Columns.Add("FSPROGNAME", typeof(string));
            DT.Columns.Add("PSDIR", typeof(string));
            DT.Columns.Add("PROGDMEMO", typeof(string));
            //DT.Columns.Add("FSMEMO", typeof(string));
            //DT.Columns.Add("FSMEMO1", typeof(string));



            //string MyConString = "DRIVER={MySQL ODBC 3.51 Driver};" +
            //"SERVER=10.1.253.19;" +
            //"DATABASE=programX;" +
            //"UID=PTSMAMUser;" +
            //"PASSWORD=ptsmamtest2011;" +
            //"OPTION=3";

            string MyConString = ConfigurationManager.ConnectionStrings["MYSQLConnStr"].ConnectionString;

//            string MyConString = "DRIVER={MySQL ODBC 3.51 Driver};" +
//"SERVER=10.1.253.19;" +
//"DATABASE=ProgramXTest;" +
//"UID=PTSMAMUser;" +
//"PASSWORD=ptsmamtest2011;" +
//"OPTION=3";
            OdbcConnection MyConnection = new OdbcConnection(MyConString);
            MyConnection.Open();
            string FSPROGID, FNEPISODE, FDDATE, FSBTIME, FSDURATION, FNREPLAY, FSKSE, FSLIVE, FSDEP, FSETIME, FNSEQNO, FSPROGNAME, PSDIR, PROGDMEMO = "";

            string Sqltxt = "select PAENO FSPROGID,PBENO FNEPISODE,PSDAT FDDATE,PSTME FSBTIME,PSMIN FSDURATION,PSKND FNREPLAY,";

//01	PTS	公視	001	01        			/ptstv	00001	2004-05-17 15:25:19.590	NULL	NULL
//02	DIMO	DIMO	001	02        			/ptstv	00001	2004-05-17 15:25:35.763	00001	2006-11-23 09:48:24.563
//03	華衛(澳中印)		001	17        			/ptstv	00001	2004-05-18 10:37:08.857	00001	2006-12-05 16:27:19.643
//04	TigerTech(北美)		001	16        			/ptstv	00001	2004-08-13 10:15:00.717	00001	2006-12-05 16:27:11.897
//05	優娛樂		001	14        			/ptstv	00001	2005-02-21 15:23:43.373	00001	2006-12-05 16:26:40.897
//06	優新聞		001	15        			/ptstv	00001	2005-02-21 15:23:55.967	00001	2006-12-05 16:27:05.380
//07	客家	客家	001	04        			/hakkatv	00001	2006-11-23 09:42:39.953	00001	2007-11-01 14:49:42.763D:\PTS-MAM\PTS_MAM3\PTS_MAM3\PTS_MAM3\PGM\PGM100_04.xaml
//08	宏觀	宏觀	001	06        			/mvtv	00001	2006-12-05 16:26:31.643	00001	2007-11-01 14:49:29.467
//09	原民	原視	001	05        			/titv	00001	2007-01-02 17:20:09.973	00001	2007-11-01 14:49:36.607
//10	DVB-H		001	07        			/ptstv	00001	2007-05-08 15:12:08.760	00001	2007-11-01 14:49:17.747
//11	HD	HIHD	002	03        				00001	2007-11-01 14:50:03.573	NULL	NULL

            try
            {

                SqlCommand mycommand3 = new SqlCommand();
                mycommand3.Connection = myconnection3;
                SqlDataReader myReader3;
                myconnection3.Open(); //連線到正式Mam資料庫
                string Sqltxt1 = "Select FSOLDTABLENAME from TBZCHANNEL where FSCHANNEL_ID='" + channelid + "'";
                mycommand3.CommandText = Sqltxt1;

                myReader3 = mycommand3.ExecuteReader();

                //int i;

                if (myReader3.HasRows == true)
                {
                    while (myReader3.Read())
                    {
                        Sqltxt = Sqltxt + "PSKSE FSKSE,PSKLV FSLIVE,PSDEP FSDEP,PSDIR from " + myReader3.GetString(0) +" where PSDAT='" + proglistdate + "' order by PSTME";
               
                        //myReader3.GetString(0);
                        //DT.Rows[i][4] = myReader1.GetInt16(0).ToString();
                        //DT.Rows[i][11] = myReader1.GetString(1);

                    }
                }
                else
                {
                    //DT.Rows[i][4] = "30";
                    //DT.Rows[i][11] = "節目名稱不存在";
                }
                myconnection3.Close();
            }
            catch (Exception ex)
            {
                return "查詢頻道表有問題,訊息:" + ex.Message;
            }
            finally
            {

            }

            //if (channelid == "01")
            //{
            //    Sqltxt = Sqltxt + "PSKSE FSKSE,PSKLV FSLIVE,PSDEP FSDEP from PSHOW_SKYNET where PSDAT='" + proglistdate + "' order by PSTME";
            //    //Sqltxt = Sqltxt + "PSKSE FSKSE,PSKLV FSLIVE,PSDEP FSDEP from PSHOW_PTS where PSDAT='" + proglistdate + "' order by PSTME";
            //}
            //else if (channelid == "02")
            //{
            //    Sqltxt = Sqltxt + "PSKSE FSKSE,PSKLV FSLIVE,PSDEP FSDEP from PSHOW_DIMO where PSDAT='" + proglistdate + "' order by PSTME";
            //}
            //else if (channelid == "07")
            //{
            //    Sqltxt = Sqltxt + "PSKSE FSKSE,PSKLV FSLIVE,PSDEP FSDEP from PSHOW_HAKKA where PSDAT='" + proglistdate + "' order by PSTME";
            //}
            //else if (channelid == "08")
            //{
            //    Sqltxt = Sqltxt + "PSKSE FSKSE,PSKLV FSLIVE,PSDEP FSDEP from PSHOW_MVTV where PSDAT='" + proglistdate + "' order by PSTME";
            //}
            //else if (channelid == "09")
            //{
            //    Sqltxt = Sqltxt + "PSKSE FSKSE,PSKLV FSLIVE,PSDEP FSDEP from PSHOW_TITV where PSDAT='" + proglistdate + "' order by PSTME";
            //}
            //else if (channelid == "11")
            //{
            //    Sqltxt = Sqltxt + "PSKSE FSKSE,PSKLV FSLIVE,PSDEP FSDEP from PSHOW_HD where PSDAT='" + proglistdate + "' order by PSTME";
            //}

            Sqltxt = Sqltxt + "";
            OdbcCommand cmd = new OdbcCommand(Sqltxt, MyConnection);
            //處理異常：插入重複記錄有異常
            int i = 0;
            try
            {
                OdbcDataAdapter oda = new OdbcDataAdapter(Sqltxt, MyConnection);

                DataTable ds = new DataTable();

                oda.Fill(ds);

                i = 0;
                FSPROGNAME = "";
                FNSEQNO = "";
                for (i = 0; i <= ds.Rows.Count - 1; i++)
                {
                    //string TempStr = "";
                    //TempStr = DT.Rows[i][0].ToString();

                    FSPROGID = ds.Rows[i][0].ToString();
                    FNEPISODE = ds.Rows[i][1].ToString();
                    FDDATE = ds.Rows[i][2].ToString();
                    FSBTIME = ds.Rows[i][3].ToString();
                    FSDURATION = ds.Rows[i][4].ToString();
                    FNREPLAY = ds.Rows[i][5].ToString();
                    FSKSE = ds.Rows[i][6].ToString();
                    FSLIVE = ds.Rows[i][7].ToString();
                    FSDEP = ds.Rows[i][8].ToString();
                    PSDIR = ds.Rows[i][9].ToString(); 
                    FSETIME ="";
                    PROGDMEMO = "";
                    DT.Rows.Add(FSPROGID, FNEPISODE, FDDATE, FSBTIME, FSDURATION, FNREPLAY, FSKSE, FSLIVE, FSDEP, FSETIME, FNSEQNO, FSPROGNAME, PSDIR, PROGDMEMO);
 
                }

                if (ds.Rows.Count==0)
                {
                  return "節目表資料不存在";
                }
                
                //FSPROGNAME = "";
                //FNSEQNO = "";
                //if (myReader.HasRows == true)
                //{
                //    while (myReader.Read())
                //    {
                //        FSPROGID = myReader.GetString(0);
                //        FNEPISODE = myReader.GetSqlInt32(1).ToString();
                //        FDDATE = myReader.GetDateTime(2).ToString();
                //        FSBTIME = myReader.GetString(3);
                //        FSDURATION = myReader.GetSqlInt32(1).ToString();
                //        FNREPLAY = myReader.GetSqlInt32(1).ToString();
                //        FSKSE = myReader.GetSqlInt32(1).ToString();
                //        FSLIVE = myReader.GetSqlInt32(1).ToString();
                //        FSDEP = myReader.GetSqlInt32(1).ToString();
                //        FSETIME = "";

                //        DT.Rows.Add(FSPROGID, FNEPISODE, FDDATE, FSBTIME, FSDURATION, FNREPLAY, FSKSE, FSLIVE, FSDEP, FSETIME, FNSEQNO, FSPROGNAME);
                //    }
                //}
                //else
                //{
                //    return "No Playlist";
                //}
            }
            catch (Exception ex)
            {
                return "自排檔系統取得節目表資料有誤";
            }
            finally
            {
                cmd.Dispose();
            }


  //            PWENO int(11) default '0',	星期編號	value = 0
  //PWCHN varchar(20) default NULL,	頻道????????	value = PROGRAM or null
  //PSENO int(10) unsigned NOT NULL auto_increment,	Unique Key	
  //PRENO int(11) default '0',	分類編號	value = 0
  //PAENO varchar(10) default NULL,	節目編號	FK: table ???
  //PBENO int(11) default '0',	集數編號, 播出集別 or 集別 ??	FK: table ???
  //PSDAT datetime default NULL,	播放日期	date only
  //PSTME varchar(5) default NULL,	播放時間	format = hh:mm
  //PSMIN int(11) default '0',	播放時數	分鐘為單位, value = 0
  //PSKND int(11) default '0',	播放型態	1首播 2重播, value = 0, 1, 2
  //PSKSE int(11) default '0',	上檔/播畢	1上檔 2播畢 value = 0, 1, 2
  //PSKLV int(11) default '0',	LIVE節目	1  LIVE value = 0, 1
  //PSDEP int(11) default '0',	獨立分開	1 分開 value = 0, 1
  //PSRFT int(11) default '0',	統計-第一次上檔	value = 0
  //PSRFY int(11) default '0',	統計-當年首播	value = 0, 1
  //PSRFW int(11) default '0',	統計-當週首播	value = 0
  //PSDIR int(11) default '0',	已通知製作人	value = 0
  //PSRAL float default '0',	????????	value = 0; ???? Only in this table, not in other PSHOW_ tables
  //PSRAT float default '0',	收視率	value = 0
  //PSUSR varchar(20) default NULL,	最後修改人	value = null
  //PSUPD datetime default NULL,	最後修改日	value = null
  //PSLOG text,	LOG檔????????	value = null




            //myconnection.Open();
            //SqlCommand mycommand = new SqlCommand(Sqltxt,myconnection);
            //SqlDataReader myReader;
            //myReader = mycommand.ExecuteReader();
            //FSPROGNAME = "";
            //FNSEQNO = "";
            //if (myReader.HasRows == true)
            //{
            //    while (myReader.Read())
            //    {
            //        FSPROGID = myReader.GetString(0);
            //        FNEPISODE = myReader.GetSqlInt32(1).ToString();
            //        FDDATE = myReader.GetDateTime(2).ToString();
            //        FSBTIME = myReader.GetString(3);
            //        FSDURATION = myReader.GetSqlInt32(1).ToString();
            //        FNREPLAY = myReader.GetSqlInt32(1).ToString();
            //        FSKSE = myReader.GetSqlInt32(1).ToString();
            //        FSLIVE = myReader.GetSqlInt32(1).ToString();
            //        FSDEP = myReader.GetSqlInt32(1).ToString();
            //        FSETIME = "";

            //        DT.Rows.Add(FSPROGID, FNEPISODE, FDDATE, FSBTIME, FSDURATION, FNREPLAY, FSKSE, FSLIVE, FSDEP, FSETIME, FNSEQNO, FSPROGNAME);
            //    }
            //}
            //else
            //{
            //    return "No Playlist";
            //}

            //myReader.Close();
            //myconnection.Close();
            
            try
            {

            SqlCommand mycommand1 = new SqlCommand();
            mycommand1.Connection = myconnection1;
            SqlDataReader myReader1;
            mycommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ProgID", System.Data.SqlDbType.VarChar, 7, "FSPROGID"));
            mycommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Episode", System.Data.SqlDbType.SmallInt, 7, "FNEPISODE"));

            //int i;
            myconnection1.Open(); //連線到正式Mam資料庫
            myconnection2.Open();
            for (i = 0; i <= DT.Rows.Count - 1; i++)
            {
                string TempStr = "";
                TempStr = DT.Rows[i][0].ToString();
                TempStr=TempStr.PadLeft(7,'0');
                DT.Rows[i][0] = TempStr;

                //Sqltxt = "Select FSORGPROG_ID,FNORGEPISODE from TBPROGLINK where FSPROG_ID=@ProgID and FNEPISODE=@Episode";
                //mycommand1.Parameters["@ProgID"].Value = DT.Rows[i][0].ToString();
                //mycommand1.Parameters["@Episode"].Value = DT.Rows[i][1].ToString();
                //mycommand1.CommandText = Sqltxt;

                //myReader1 = mycommand1.ExecuteReader();

                //if (myReader1.HasRows == true)
                //{
                //    while (myReader1.Read())
                //    {
                //        DT.Rows[i][0] =  myReader1.GetString(0);
                //        DT.Rows[i][1] = myReader1.GetInt16(1).ToString();
                //    }
                //}
                //myReader1.Close();

                Sqltxt = "select A.FNLENGTH,A.FSPGDNAME,A.FSPGDENAME,B.FSPGMNAME,A.FSMEMO from TBPROG_D A,TBPROG_M B";
                Sqltxt = Sqltxt + " where A.FSPROG_ID=B.FSPROG_ID and A.FSPROG_ID=@ProgID and A.FnEpisode=@Episode";
                mycommand1.CommandText = Sqltxt;

                //mycommand1.Parameters.AddWithValue("@FSPROGID", DT.Rows[i][0].ToString());
                mycommand1.Parameters["@ProgID"].Value = DT.Rows[i][0].ToString();
                if (DT.Rows[i][1].ToString() == "0")
                {
                    DT.Rows[i][1] = "1";
                }
                mycommand1.Parameters["@Episode"].Value = DT.Rows[i][1].ToString();

                myReader1 = mycommand1.ExecuteReader();

                if (myReader1.HasRows == true)
                {
                    while (myReader1.Read())
                    {
                        DT.Rows[i][4] = myReader1.GetInt16(0).ToString();
                        DT.Rows[i][11] = myReader1.GetString(3);
                        DT.Rows[i][13] = myReader1.GetString(4);
                    }
                }
                else
                {
                    DT.Rows[i][4] = "30";
                    DT.Rows[i][11] = "節目名稱不存在";
                    DT.Rows[i][13] = "";
                }
                myReader1.Close();
                TempStr = DT.Rows[i][3].ToString(); //起始時間
                TempStr = TempStr.Substring(0, 2) + TempStr.Substring(3, 2);
                DT.Rows[i][3] = TempStr;

                //TempStr = DT.Rows[i][9].ToString(); //結束時間
                int alltimemin;
                alltimemin=int.Parse(TempStr.Substring(0, 2)) * 60 + int.Parse(TempStr.Substring(2, 2)) + int.Parse(DT.Rows[i][4].ToString());


                DT.Rows[i][9] = (alltimemin / 60).ToString().PadLeft(2,'0') +  (alltimemin % 60).ToString().PadLeft(2,'0');
             }
            SqlCommand mycom = new SqlCommand();
            mycom.Connection = myconnection1;
            SqlCommand mycom1 = new SqlCommand();
            mycom1.Connection = myconnection2;


            string Sqltxt1 = "DELETE FROM tbpgm_queue where fddate='" + proglistdate + "'";
            Sqltxt1 = Sqltxt1 + " and FSCHANNEL_ID='" + channelid + "' ";
            mycom.CommandText = Sqltxt1;
            mycom.ExecuteNonQuery();
            for (i = 0; i <= DT.Rows.Count - 1; i++)
            {
                //先去查詢TBLOG_VIDEO_MAP看資料是否存在,有的話就不需要新增,不然要新增一筆新的資料,並取得新的Video_ID_PROG號碼
                //呼叫維智寫的函式去見查詢與建立Video_ID_PROG,輸入條件:ID,集數,channel_ID

                PTS_MAM3.Web.DataSource.WSMAMFunctions obj = new PTS_MAM3.Web.DataSource.WSMAMFunctions();
                string ReturnVideoID = "";
                ReturnVideoID=obj.QueryVideoID_PROG(DT.Rows[i][0].ToString(), DT.Rows[i][1].ToString(), channelid);
                if (ReturnVideoID == "       ")
                {
                    return "取得Video ID有誤";
                }

                 //依據查看是否有播映資料存在,如果沒有則要新增一筆並取新的FNSEQNO,有的話則取FNSEQNO
                Sqltxt1 = "select FnSeqno,FSprog_ID,FsProg_Name,FNSEQNO,FNDUR,FNSEC,FSSIGNAL,FSMEMO,FSMEMO1 from tbpgm_bank_Detail where fsprog_id='" + DT.Rows[i][0].ToString() + "'";
                 Sqltxt1 = Sqltxt1 + " and FSBEG_TIME='" + DT.Rows[i][3].ToString() + "' and FSCHANNEL_ID='" + channelid + "'";
                 mycom.CommandText = Sqltxt1;
                 myReader1 = mycom.ExecuteReader();
                 string _BankDetailSeg = "";
                 string _BankDetailDur = "";
                 string _BankDetailSec = "";
                 string _BankDetailSignal = "";

                 string _BankDetailMemo = "";
                 string _BankDetailMemo1 = "";

                 if (myReader1.HasRows == true)
                 {
                     while (myReader1.Read())
                     {
                         if (myReader1.IsDBNull(0) != true)
                         {
                             try
                             { _BankDetailSeg = myReader1.GetInt32(3).ToString(); }
                             catch
                             { _BankDetailSeg = ""; }
                             try
                             { _BankDetailDur = myReader1.GetInt32(4).ToString(); }
                             catch
                             { _BankDetailDur = ""; }
                             try
                             { _BankDetailSec = myReader1.GetInt32(5).ToString(); }
                             catch
                             { _BankDetailSec = ""; }
                             try
                             { _BankDetailSignal = myReader1.GetString(6); }
                             catch
                             { _BankDetailSignal = ""; }
                             try
                             { _BankDetailMemo = myReader1.GetString(7); }
                             catch
                             { _BankDetailMemo = ""; }
                             try
                             { _BankDetailMemo1 = myReader1.GetString(8); }
                             catch
                             { _BankDetailMemo1 = ""; }

                             FNSEQNO = myReader1.GetInt32(0).ToString();
                             FSPROGNAME = myReader1.GetString(2);

                             string Sqltxt2 = "update tbpgm_bank_Detail set fdEND_date='" + proglistdate + "' where fsprog_id='" + DT.Rows[i][0].ToString() + "'";
                             Sqltxt2 = Sqltxt2 + " and Fnseqno=" + FNSEQNO;
                             mycom1.CommandText = Sqltxt2;
                             mycom1.ExecuteNonQuery();
                         }
                     }
                 }
                 else
                 {
                     string Sqltxt2 = "select max(fnseqno) from tbpgm_bank_Detail where fsprog_id='" + DT.Rows[i][0].ToString() + "'";
                     SqlDataReader myReader2;
                     mycom1.CommandText = Sqltxt2;
                     FNSEQNO = "";
                     FSPROGNAME = "";
                     myReader2 = mycom1.ExecuteReader();
                     myReader2.Read();
                     if (myReader2.IsDBNull(0) != true)
                     {
                         FNSEQNO = (myReader2.GetInt32(0) + 1).ToString();
                     }
                     else
                     {
                         FNSEQNO = "1";
                     }
                     //if (myReader2.HasRows == true)
                     //{
                     //    while (myReader2.Read())
                     //    {

                     //    }
                     //}

                     myReader2.Close();
                     Sqltxt1 = "insert into tbpgm_bank_detail(FSPROG_ID, FNSEQNO, FSPROG_NAME, FSPROG_NAME_ENG, FDBEG_DATE, FDEND_DATE,";
                     Sqltxt1 = Sqltxt1 + "FSBEG_TIME, FSEND_TIME, FSCHANNEL_ID, FNREPLAY,FSWEEK,FSPLAY_TYPE,FNSEG,FNDUR,FNSEC,FSSIGNAL,FSCREATED_BY,FDCREATED_DATE) values(";
                     Sqltxt1 = Sqltxt1 + "'" + DT.Rows[i][0].ToString() + "',";
                     Sqltxt1 = Sqltxt1 + FNSEQNO + ",";
                     Sqltxt1 = Sqltxt1 + "'" + DT.Rows[i][11].ToString().Replace("'","''") + "',";
                     Sqltxt1 = Sqltxt1 + "'',";
                     Sqltxt1 = Sqltxt1 + "'" + proglistdate + "',";
                     Sqltxt1 = Sqltxt1 + "'" + proglistdate + "',";
                     Sqltxt1 = Sqltxt1 + "'" + DT.Rows[i][3].ToString() + "',";
                     Sqltxt1 = Sqltxt1 + "'" + DT.Rows[i][9].ToString() + "',";
                     Sqltxt1 = Sqltxt1 + "'" + channelid + "',";
                     Sqltxt1 = Sqltxt1 + DT.Rows[i][5].ToString() + ",";
                     Sqltxt1 = Sqltxt1 + "'" + "1111111" + "',";
                     Sqltxt1 = Sqltxt1 + DT.Rows[i][7].ToString() + ","; //live



                     Sqltxt1 = Sqltxt1 + "0,0,0,'','',Getdate())"; //seg,FUR,userid,date
                     mycom1.CommandText = Sqltxt1;
                     mycom1.ExecuteNonQuery();

                     //DT.Columns.Add("FSPROGID", typeof(string));
                     //DT.Columns.Add("FNEPISODE", typeof(string));
                     //DT.Columns.Add("FDDATE", typeof(string));
                     //DT.Columns.Add("FSBTIME", typeof(string));
                     //DT.Columns.Add("FSDURATION", typeof(string));
                     //DT.Columns.Add("FSTYPE", typeof(string));
                     //DT.Columns.Add("FSKSE", typeof(string));
                     //DT.Columns.Add("FSLIVE", typeof(string));
                     //DT.Columns.Add("FSDEP", typeof(string));
                     //DT.Columns.Add("FSETIME", typeof(string));
                     //DT.Columns.Add("FNSEQNO", typeof(string));
                     //string FSPROGID, FNEPISODE, FDDATE, FSBTIME, FSDURATION, FSTYPE, FSKSE, FSLIVE, FSDEP, FSETIME, FNSEQNO = "";
                 }

                 myReader1.Close();

                 Sqltxt1 = "insert into tbpgm_queue(FSPROG_ID, FNSEQNO, FDDATE, FSCHANNEL_ID, FSBEG_TIME, FSEND_TIME,";
                 Sqltxt1 = Sqltxt1 + "FNREPLAY, FNEPISODE,FSTAPENO, FSPROG_NAME,FNLIVE,FSSIGNAL,FNDEP,FNONOUT, FSMEMO,FSMEMO1,FSCREATED_BY,FDCREATED_DATE,FNDEF_MIN,FNDEF_SEC) values(";
                 Sqltxt1 = Sqltxt1 + "'" + DT.Rows[i][0].ToString() + "',";
                 Sqltxt1 = Sqltxt1 + FNSEQNO + ",";
                 Sqltxt1 = Sqltxt1 + "'" + proglistdate + "',";
                 Sqltxt1 = Sqltxt1 + "'" + channelid + "',";
                 Sqltxt1 = Sqltxt1 + "'" + DT.Rows[i][3].ToString() + "',";
                 Sqltxt1 = Sqltxt1 + "'" + DT.Rows[i][9].ToString() + "',";
                 Sqltxt1 = Sqltxt1 + DT.Rows[i][5].ToString() + ",";
                 Sqltxt1 = Sqltxt1 + DT.Rows[i][1].ToString() + ",";

                 Sqltxt1 = Sqltxt1 + "'',";//TapeNO
                 Sqltxt1 = Sqltxt1 + "'" + DT.Rows[i][11].ToString().Replace("'", "''") + "',";
                 Sqltxt1 = Sqltxt1 + DT.Rows[i][7].ToString() + ",";  //live
                 if (DT.Rows[i][7].ToString() == "0")  //非live
                 {
                     Sqltxt1 = Sqltxt1 + "'PGM'" + ",";  //FSSIGNAL
                 }
                 else
                 {
                     Sqltxt1 = Sqltxt1 + "'" + _BankDetailSignal+ "'" + ",";  //FSSIGNAL
                 }
                 Sqltxt1 = Sqltxt1 + DT.Rows[i][8].ToString() + ",";
                 Sqltxt1 = Sqltxt1 + DT.Rows[i][6].ToString() + ",";//KSE ONOUT
                 if (DT.Rows[i][12].ToString() == "1" || DT.Rows[i][12].ToString() == "3")
                 {
                     if (_BankDetailMemo1.Length >= 7)
                     {
                         if (_BankDetailMemo1.Substring(0, 7) != "MOD不可播出")
                             _BankDetailMemo1 = "MOD不可播出" + _BankDetailMemo1;
                     }
                     else
                         _BankDetailMemo1 = "MOD不可播出" + _BankDetailMemo1;
                 }
                 else
                 {
                     if (_BankDetailMemo1.Length >= 7)
                     {
                         if (_BankDetailMemo1.Substring(0, 7) == "MOD不可播出")
                             _BankDetailMemo1 = _BankDetailMemo1.Substring(7, _BankDetailMemo1.Length - 7);
                     }
                 }

                if (DT.Rows[i][13].ToString()!="")
                {
                    if (_BankDetailMemo1 == "")
                    {
                        _BankDetailMemo1= DT.Rows[i][13].ToString();
                    }
                    else
                    { 
                         _BankDetailMemo1=_BankDetailMemo1+"---"+ DT.Rows[i][13].ToString();
                    }
                }
                 Sqltxt1 = Sqltxt1 + "'" + _BankDetailMemo.Replace("'", "''") + "','" + _BankDetailMemo1.Replace("'", "''") + "','',GetDate()";//MEMO,user,date

                 if (_BankDetailSeg == "")
                 { _BankDetailSeg = "0"; }
                 if (_BankDetailDur == "")
                 { _BankDetailDur = "0"; }
                 if (_BankDetailSec == "")
                 { _BankDetailSec = "0"; }

                 Sqltxt1 = Sqltxt1 + "," + _BankDetailDur + "," + _BankDetailSec + ")";
                 mycommand1.CommandText = Sqltxt1;
                 mycommand1.ExecuteNonQuery();

            }
          
            myconnection1.Close();
            myconnection2.Close();

        //Me.SqlSelectCommand1.CommandText = "SELECT CONVERT(char(20), dbo.Tbtape.FdDateTime, 20) AS fddatet, dbo.Tbkind.FsKind" & _
        //", dbo.Tbusers.FsUserName, dbo.TbLang.FsLang, dbo.Tbtape.FsTypeID, dbo.Tbtape.FsK" & _
        //"indID, dbo.Tbtape.FsSlug, dbo.Tbtape.FsUser, dbo.Tbtape.FdDateTime, dbo.Tbtape.F" & _
        //"sMemo FROM dbo.Tbtape INNER JOIN dbo.Tbkind ON dbo.Tbtape.FsKindID = dbo.Tbkind." & _
        //"FsKindID INNER JOIN dbo.TbLang ON SUBSTRING(dbo.Tbtape.FsTypeID, 7, 1) = dbo.TbL" & _
        //"ang.FsLangID INNER JOIN dbo.Tbusers ON dbo.Tbtape.FsUser = dbo.Tbusers.FsEmpNO W" & _
        //"HERE (dbo.Tbtape.FdDateTime >= @bdate) AND (dbo.Tbtape.FsSlug LIKE @sslugtxt) AN" & _
        //"D (dbo.Tbtape.FsKindID LIKE @skindtxt) AND (SUBSTRING(dbo.Tbtape.FsTypeID, 7, 1)" & _
        //" LIKE @slangtxt) AND (dbo.Tbtape.FdDateTime <= @edate) ORDER BY dbo.Tbtape.FdDat" & _
        //"eTime DESC"
        //Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        //Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@bdate", System.Data.SqlDbType.DateTime, 8, "FdDateTime"))
        //Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@sslugtxt", System.Data.SqlDbType.VarChar, 20, "FsSlug"))
        //Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@skindtxt", System.Data.SqlDbType.VarChar, 1, "FsKindID"))
        //Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@slangtxt", System.Data.SqlDbType.VarChar))
        //Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@edate", System.Data.SqlDbType.DateTime, 8, "FdDateTime"))


//            { CREATE TABLE PSHOW_DIMO (	播放資料	
//  PWENO int(11) default '0',	星期編號	value = 0
//  PWCHN varchar(20) default NULL,	頻道????????	value = PROGRAM or null
//  PSENO int(10) unsigned NOT NULL auto_increment,	Unique Key	
//  PRENO int(11) default '0',	分類編號	value = 0
//  PAENO varchar(10) default NULL,	節目編號	FK: table ???
//  PBENO int(11) default '0',	集數編號, 播出集別 or 集別 ??	FK: table ???
//  PSDAT datetime default NULL,	播放日期	date only
//  PSTME varchar(5) default NULL,	播放時間	format = hh:mm
//  PSMIN int(11) default '0',	播放時數	分鐘為單位, value = 0
//  PSKND int(11) default '0',	播放型態	1首播 2重播, value = 0, 1, 2
//  PSKSE int(11) default '0',	上檔/播畢	1上檔 2播畢 value = 0, 1, 2
//  PSKLV int(11) default '0',	LIVE節目	1  LIVE value = 0, 1
//  PSDEP int(11) default '0',	獨立分開	1 分開 value = 0, 1
//  PSRFT int(11) default '0',	統計-第一次上檔	value = 0
//  PSRFY int(11) default '0',	統計-當年首播	value = 0, 1
//  PSRFW int(11) default '0',	統計-當週首播	value = 0
//  PSDIR int(11) default '0',	已通知製作人	value = 0
//  PSRAL float default '0',	????????	value = 0; ???? Only in this table, not in other PSHOW_ tables
//  PSRAT float default '0',	收視率	value = 0
//  PSUSR varchar(20) default NULL,	最後修改人	value = null
//  PSUPD datetime default NULL,	最後修改日	value = null
//  PSLOG text,	LOG檔????????	value = null
//  PRIMARY KEY  (PSENO)		
//) TYPE=MyISAM AUTO_INCREMENT=43862 ;		}




            //string connectionInfo1 = System.Configuration.ConfigurationManager.AppSettings["ConnectionInfo1"];
          
            //using(SqlConnection myconnection1 = new SqlConnection(connectionInfo1))
            //string connectionInfo = System.Configuration.ConfigurationSettings.AppSettings["ConnectionInfo"];


            //var myconnection =new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings.GetValues(("")));

              //  Dim myConnection As New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("connstr"))
   // Dim myConnection1 As New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("connstr"))
            return "ok";
            }
            catch (Exception ex)
            {
                return "寫入節目表有問題,訊息:" + ex.Message;
            }
            finally
            {
                cmd.Dispose();
            }
        }

        [WebMethod]
        public string MapVideoID(string channelid, string prog_id,string Episode)
        {
            PTS_MAM3.Web.DataSource.WSMAMFunctions obj = new PTS_MAM3.Web.DataSource.WSMAMFunctions();
            string ReturnVideoID = "";
            ReturnVideoID = obj.QueryVideoID_PROG(prog_id, Episode, channelid);
            if (ReturnVideoID == "       ")
            {
                return "取得Video ID有誤";
            }
            else
                return ReturnVideoID;
        }

        [WebMethod]
        public string MapVideoIDList(string InXML)
        {
            //先分析參數 
            StringBuilder sb = new StringBuilder(); 
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(InXML);
            XmlNode xmlNode = xmldoc.FirstChild;
            sb.AppendLine("<Datas>");
            for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
            {
                string prog_id="";
                string Episode="";
                string channelid = "";
              
                for (int j = 0; j <= xmlNode.ChildNodes[i].ChildNodes.Count - 1; j++)
                {
                    if (xmlNode.ChildNodes[i].ChildNodes[j].Name == "prog_id")
                        prog_id = xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value;

                    if (xmlNode.ChildNodes[i].ChildNodes[j].Name == "Episode")
                        Episode = xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].ChildNodes[j].Name == "channelid")
                        channelid = xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value;


                }

                if (prog_id != "" && Episode != "" && channelid != "")
                {
                    PTS_MAM3.Web.DataSource.WSMAMFunctions obj = new PTS_MAM3.Web.DataSource.WSMAMFunctions();
                    string ReturnVideoID = "";
                    ReturnVideoID = obj.QueryVideoID_PROG(prog_id, Episode, channelid);
                    if (ReturnVideoID == "       ")
                    {

                    }
                    else
                    {
                        sb.AppendLine("<Data>");
                        sb.AppendLine("<prog_id>" + prog_id + "</prog_id>");
                        sb.AppendLine("<Episode>" + Episode + "</Episode>");
                        sb.AppendLine("<channelid>" + Episode + "</channelid>");
                        sb.AppendLine("<video_id>" + ReturnVideoID + "</video_id>");
                        sb.AppendLine("</Data>");
                    }
                       
                }
            }
            sb.AppendLine("</Datas>");

            return sb.ToString();

            
        }

        [WebMethod]
        public string ExportModProglist(string channelid)
        {
            bool needftp = false;

            //超過兩碼則前兩碼為頻道ID
            if (channelid.Length > 2 )
            {
                channelid = channelid.Substring(0,2);
                needftp = true;
            }

            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection1 = new SqlConnection(sqlConnStr);
            if (connection1.State == System.Data.ConnectionState.Closed)
            {
                connection1.Open();
            }
            
            SqlDataAdapter daChannel = new SqlDataAdapter("SP_Q_TBPGM_PROG_EPG", connection1);
            DataTable dtChannel = new DataTable();
            SqlParameter paraChannelID = new SqlParameter("@FSCHANNEL_ID", channelid);
            daChannel.SelectCommand.Parameters.Add(paraChannelID);

            daChannel.SelectCommand.CommandType = CommandType.StoredProcedure;
            daChannel.Fill(dtChannel);
            connection1.Close();
            string FS_PROGRAMX_TABLE_NAME;
            string FS_PAREA_CHEID;
            string FS_OUTPUT_FILE_NAME;
            if (dtChannel.Rows.Count == 0)
            {
                return "沒有查到頻道資料";
            }
            else
            { 

                 if (dtChannel.Rows[0]["FS_PROGRAMX_TABLE_NAME"].ToString() == "")
                    FS_PROGRAMX_TABLE_NAME = "";
                else
                     FS_PROGRAMX_TABLE_NAME = dtChannel.Rows[0]["FS_PROGRAMX_TABLE_NAME"].ToString();

                if (dtChannel.Rows[0]["FS_PAREA_CHEID"].ToString() == "")
                    FS_PAREA_CHEID ="";
                else
                    FS_PAREA_CHEID = dtChannel.Rows[0]["FS_PAREA_CHEID"].ToString();

                if (dtChannel.Rows[0]["FS_OUTPUT_FILE_NAME"].ToString() == "")
                    FS_OUTPUT_FILE_NAME ="";
                else
                    FS_OUTPUT_FILE_NAME =  dtChannel.Rows[0]["FS_OUTPUT_FILE_NAME"].ToString();
              
            }
         

            
            //    sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            //SqlConnection myconnection = new SqlConnection(sqlConnStr);
            SqlConnection myconnection1 = new SqlConnection(sqlConnStr);
            SqlConnection myconnection2 = new SqlConnection(sqlConnStr);
           
            
            DataTable DT = new DataTable();

            DT.Columns.Add("FSNAME", typeof(string));
            DT.Columns.Add("FSBDATE", typeof(string));
            DT.Columns.Add("FSEDATE", typeof(string));
            DT.Columns.Add("FSBTIME", typeof(string));
            DT.Columns.Add("FSETIME", typeof(string));
            DT.Columns.Add("FILSNAME", typeof(string));
            DT.Columns.Add("BWEEK", typeof(string));
            DT.Columns.Add("FSPROGGRADEID", typeof(string));
            DT.Columns.Add("FSDUAL_LANG", typeof(string));



            //測試-----------------------------------------------
            //string MyConString = ConfigurationManager.ConnectionStrings["MYSQLConnStr"].ConnectionString;
            //OdbcConnection MyConnection = new OdbcConnection(MyConString);
            //MyConnection.Open();
            //string Sqltxt1="";
            //Sqltxt1 = "SELECT  * FROM programX.PSHOW_PTS_MOD where PSDAT>=sysdate() and PSDAT<= sysdate()+14";
            //OdbcCommand cmd = new OdbcCommand(Sqltxt1, MyConnection);
            //OdbcDataAdapter oda = new OdbcDataAdapter(Sqltxt1, MyConnection);

            //DataTable ds = new DataTable();

            //oda.Fill(ds);
            //string tempname=""; 
            //for (int aa = 0; aa <= 20; aa++)
            //{

            //    tempname = tempname + ds.Columns[aa].ColumnName + ',';
            //}

            //----------------------------------------------------


            myconnection1.Open();
            SqlCommand mycom = new SqlCommand();
            mycom.Connection = myconnection1;
            string FSNAME, FSBDATE, FSEDATE, FSBTIME, FSETIME, FILSNAME, BWEEK, FSPROGGRADEID, FSDUAL_LANG;
            string Sqltxt = "";
            Sqltxt = Sqltxt + "select A.PAENO FSPROGID, A.PSDAT FDDATE, A.PSTME FSBTIME, B.FSWEBNAME,A.PBENO FNEPISODE, B.FNTOTEPISODE, ";
            Sqltxt = Sqltxt + "ISNULL(D.FSPROGGRADENAME, '普') FSPROGGRADENAME, ";
            Sqltxt = Sqltxt + "(CASE WHEN (C.FSPROGLANGID1 = C.FSPROGLANGID2) THEN 'N' ELSE 'Y' END) as FSDUAL_LANG ";
            Sqltxt = Sqltxt + "from OPENQUERY(PTS_PROG_MYSQL, 'SELECT  * FROM programX." + FS_PROGRAMX_TABLE_NAME + " where PSDAT>=sysdate() ";
            Sqltxt = Sqltxt + " and PSDAT<= sysdate()+ interval 45 day') A join TBPROG_M B on A.PAENO = B.FSPROG_ID ";
            Sqltxt = Sqltxt + " left join TBPROG_D C on C.FSPROG_ID = A.PAENO and A.PBENO = C.FNEPISODE";
            Sqltxt = Sqltxt + " left join TBZPROGGRADE D on C.FSPROGGRADEID = D.FSPROGGRADEID";
            Sqltxt = Sqltxt + " where A.PAENO=B.FSPROG_ID order by A.PSDAT,A.PSTME";

            //if (channelid == "51")
            //{
            //    Sqltxt = Sqltxt + "select A.PAENO FSPROGID,A.PSDAT FDDATE,A.PSTME FSBTIME,B.FSWEBNAME,A.PBENO FNEPISODE,B.FNTOTEPISODE ";
            //    Sqltxt = Sqltxt + "from OPENQUERY(PTS_PROG_MYSQL, 'SELECT  * FROM programX.PSHOW_PTS_MOD where PSDAT>=sysdate() ";
            //    Sqltxt = Sqltxt + " and PSDAT<= sysdate()+ interval 14 day') A,TBPROG_M B ";
            //    Sqltxt = Sqltxt + "where A.PAENO=B.FSPROG_ID order by A.PSDAT,A.PSTME";

            //}
            //else if (channelid == "61")
            //{
            //    Sqltxt = Sqltxt + "select A.PAENO FSPROGID,A.PSDAT FDDATE,A.PSTME FSBTIME,B.FSWEBNAME,A.PBENO FNEPISODE,B.FNTOTEPISODE ";
            //    Sqltxt = Sqltxt + "from OPENQUERY(PTS_PROG_MYSQL, 'SELECT  * FROM programX.PSHOW_HD_MOD where PSDAT>=sysdate() ";
            //    Sqltxt = Sqltxt + " and PSDAT<= sysdate()+ interval 14 day') A,TBPROG_M B ";
            //    Sqltxt = Sqltxt + "where A.PAENO=B.FSPROG_ID order by A.PSDAT,A.PSTME"; 
            //}

            mycom.CommandText = Sqltxt;
            SqlDataReader myReader1;
            

            try
            {
                myReader1 = mycom.ExecuteReader();
                if (myReader1.HasRows == true)
                {

                    while (myReader1.Read())
                    {
                        //4=A.PBENO 集數, 5=B.FNTOTEPISODE 總集數 如果超過1集就要串集數
                        if ((myReader1.GetInt16(5).ToString() == "1") || (myReader1.GetInt32(4).ToString() == "0"))
                            FSNAME = myReader1.GetString(3);
                        else
                            FSNAME = myReader1.GetString(3) + "(" + myReader1.GetInt32(4).ToString() + ")";
                        //1=A.PSDAT 播出日期
                        FSBDATE = DateTime.Parse(myReader1.GetDateTime(1).ToString()).ToString("yyyy-MM-dd");
                        FSEDATE = "";// DateTime.Parse(myReader1.GetDateTime(1).ToString()).ToString("yyyy-MM-dd");
                        //2=A.PSTME 播出時間 06:30 -> 0630
                        FSBTIME = myReader1.GetString(2).Substring(0, 2) + myReader1.GetString(2).Substring(3, 2);
                        FSETIME = "";
                        FILSNAME = "";
                        BWEEK = "";
                        FSPROGGRADEID = myReader1.GetString(6).Split('：')[0];
                        FSDUAL_LANG = myReader1.GetString(7);
                        //FSDUAL_LANG = "N";
                        DT.Rows.Add(FSNAME, FSBDATE, FSEDATE, FSBTIME, FSETIME, FILSNAME, BWEEK, FSPROGGRADEID, FSDUAL_LANG);
                    }
                }
                else
                {
                    return "沒有查到節目表資料";
                }
              
            }
            catch (Exception ex)
            {
                return "自排檔系統取得節目表資料有誤";
                //return ex.Message;
            }
            finally
            {
               
            }

            try
            {
                
                myconnection2.Open();

                
                for (int i = 0; i <= DT.Rows.Count - 1; i++)
                {

                    if (i + 1 < DT.Rows.Count)
                    {
                        DT.Rows[i][4] = DT.Rows[i + 1][3]; //FSETIME = next FSBTIME
                        DT.Rows[i][2] = DT.Rows[i + 1][1]; //FSEDATE = next FSBDATE

                        if (int.Parse(DT.Rows[i][3].ToString()) >= 2400)
                        {
                            DT.Rows[i][3] = (int.Parse(DT.Rows[i][3].ToString()) - 2400).ToString("0000");
                            DT.Rows[i][1] = (DateTime.Parse(DT.Rows[i][1].ToString()).AddDays(1).ToString("yyyy-MM-dd"));
                        }
                        if (int.Parse(DT.Rows[i][4].ToString()) >= 2400)
                        {
                            DT.Rows[i][4] = (int.Parse(DT.Rows[i][4].ToString()) - 2400).ToString("0000");
                            DT.Rows[i][2] = (DateTime.Parse(DT.Rows[i][2].ToString()).AddDays(1).ToString("yyyy-MM-dd"));
                        }

                        string NWeek = DateTime.Parse(DT.Rows[i][1].ToString()).DayOfWeek.ToString();
                        if (NWeek == "Sunday")
                            DT.Rows[i][6] = "PRCB0";
                        else if (NWeek == "Monday")
                            DT.Rows[i][6] = "PRCB1";
                        else if (NWeek == "Tuesday")
                            DT.Rows[i][6] = "PRCB2";
                        else if (NWeek == "Wednesday")
                            DT.Rows[i][6] = "PRCB3";
                        else if (NWeek == "Thursday")
                            DT.Rows[i][6] = "PRCB4";
                        else if (NWeek == "Friday")
                            DT.Rows[i][6] = "PRCB5";
                        else if (NWeek == "Saturday")
                            DT.Rows[i][6] = "PRCB6";

                        //                    if (抓取日期為週日)  strSQL += " AND PRCB1 = '1' ";
                        //if (抓取日期為週一)  strSQL += " AND PRCB2 = '1' ";
                        //if (抓取日期為週二)  strSQL += " AND PRCB3 = '1' ";
                        //if (抓取日期為週三)  strSQL += " AND PRCB4 = '1' ";
                        //if (抓取日期為週四)  strSQL += " AND PRCB5 = '1' ";
                        //if (抓取日期為週五)  strSQL += " AND PRCB6 = '1' ";
                        //if (抓取日期為週六)  strSQL += " AND PRCB0 = '1' ";
                        //strSQL += "ORDER BY PRENO DESC "
                        DT.Rows[i][3] = DT.Rows[i][3].ToString().Substring(0, 2) + ":" + DT.Rows[i][3].ToString().Substring(2, 2);
                        DT.Rows[i][4] = DT.Rows[i][4].ToString().Substring(0, 2) + ":" + DT.Rows[i][4].ToString().Substring(2, 2);

                        string Sqltxt2 = "";
                        Sqltxt2 = "select PRNAM from OPENQUERY(PTS_PROG_MYSQL, 'SELECT PRNAM ";
                        Sqltxt2 = Sqltxt2 + "FROM programX.PAREA WHERE CHEID = ''";
                        Sqltxt2 = Sqltxt2 + FS_PAREA_CHEID;
                        //if (channelid == "51")
                        //{
                        //    Sqltxt2 = Sqltxt2 + "PTS_MOD";
                        //}
                        //else if (channelid == "61")
                        //{
                        //    Sqltxt2 = Sqltxt2 + "HD_MOD";
                        //}
                        Sqltxt2 = Sqltxt2 + "'' AND PRSDT <=''" + DT.Rows[i][1].ToString() + "'' and  PREDT >= ''" + DT.Rows[i][2].ToString() + "'' ";
                        Sqltxt2 = Sqltxt2 + "and PRTMS <= ''" + DT.Rows[i][3].ToString() + "'' and  PRTME >= ''" + DT.Rows[i][3].ToString() + "'' ";
                        Sqltxt2 = Sqltxt2 + "and " + DT.Rows[i][6] + "=1 ORDER BY PRENO DESC')";

                        SqlCommand mycom1 = new SqlCommand();
                        mycom1.Connection = myconnection2;
                        mycom1.CommandText = Sqltxt2;
                        SqlDataReader myReader2;
                        myReader2 = mycom1.ExecuteReader();
                        try
                        {
                            
                            if (myReader2.HasRows == true)
                            {
                                myReader2.Read();
                                if (myReader2.IsDBNull(0) != true)
                                    DT.Rows[i][5] = myReader2.GetString(0);
                                else
                                    DT.Rows[i][5] = "";
                            }
                            else
                            {
                                DT.Rows[i][5] = "";
                            }
                            myReader2.Close();
                        }
                        catch (Exception ex)
                        {
                            myReader2.Close();
                        }
                        finally
                        {

                        }
                      
                    }
                }

                //資料查詢完成,開始寫入檔案
                string Exportfilename="";
                string delimiter = "~";
                string[] Header = { "開始日期", "開始時間", "結束日期", "結束時間", "節目名稱", "節目分級", "語言(Y|N)",
                "字幕(Y|N)", "多螢頻道(Y|N)",  "導演/主持人", "演員/來賓", "系列節目簡介", "系列節目註記", "內容分類/版型",
                "英文片名", "系列節目集數", "節目簡介(單集簡介)", "是否推薦(Y|N)", "得獎紀錄",
                "關鍵字註記", "推薦文", "圖片檔名", "社群熱度", "年份" };

                Exportfilename = HttpContext.Current.Server.MapPath("~/UploadFolder/" + FS_OUTPUT_FILE_NAME);
                      //if (channelid == "51")
                      //    Exportfilename=HttpContext.Current.Server.MapPath("~/UploadFolder/13.txt");
                      //else if (channelid == "61")
                      //    Exportfilename=HttpContext.Current.Server.MapPath("~/UploadFolder/14.txt");
                
                if (File.Exists(Exportfilename) == true)
                    File.Delete(Exportfilename);
                FileStream myFile = File.Create(Exportfilename);
                StreamWriter m_streamWriter = new StreamWriter(myFile);
                StringBuilder sb = new StringBuilder();

                for (int i = 0; i < Header.Length; i++)
                {
                    sb.Append(Header[i] + delimiter);
                }
                sb.AppendLine("");

                for (int i = 0; i <= DT.Rows.Count - 1; i++)
                {
                         // DT.Rows.Add(FSNAME, FSBDATE, FSEDATE, FSBTIME, FSETIME, FILSNAME, BWEEK);
                       if (  DT.Rows[i][4].ToString()!="")
                       {
                            sb.Append(DT.Rows[i][1].ToString() + delimiter + DT.Rows[i][3].ToString() + ":00.0" + delimiter);
                            sb.Append(DT.Rows[i][2].ToString() + delimiter + DT.Rows[i][4].ToString() + ":00.0" + delimiter);
                            if (DT.Rows[i][5].ToString() != "")
                                sb.Append("[" + DT.Rows[i][5].ToString() + "]");
                            sb.Append(DT.Rows[i][0].ToString() + delimiter);
                            sb.Append(DT.Rows[i][7] + delimiter); //FSPROGGRADEID 分級
                            sb.Append(DT.Rows[i][8] + delimiter); //FSDUAL_LANG 雙語
                            sb.AppendLine("");
                       }
                   }
                   m_streamWriter.Write(sb.ToString());
                   m_streamWriter.Flush();
                   m_streamWriter.Close();

                   m_streamWriter.Dispose();


                //SessionOptions WinscpOpt = new SessionOptions();
                //WinscpOpt.Protocol=Protocol.Ftp;
                //WinscpOpt.HostName="192.168.100.95";
                //WinscpOpt.UserName="video";
                //WinscpOpt.Password="asfvideo";
                
                //WinSCP.Session session1 = new WinSCP.Session();
                //session1.Open(WinscpOpt);
                //TransferOptions FtpTransferOptions =new TransferOptions();
                //FtpTransferOptions.TransferMode=TransferMode.Binary;
                //TransferOperationResult transferResult; 
                //transferResult = session1.PutFiles(Exportfilename, "", false, FtpTransferOptions);
                //transferResult.Check();
                //if (transferResult.IsSuccess == true) 
                //{
                //    return "ok";
                //}
                //else
                //{ 
                //    return "Ftp失敗";
                //}

               
                   string UploadPath;

                   UploadPath = PTS_MAM3.Web.Properties.Settings.Default.UploadPath;

                   //UploadPath = "http://localhost/UploadFolder/";
                   string ReturnString = "";
                   //return UploadPath + lstFileName;
                   ReturnString = UploadPath + FS_OUTPUT_FILE_NAME;
                
                   //if (channelid == "51")
                   //    ReturnString = UploadPath + "13.txt";
                   //else if (channelid == "61")
                   //    ReturnString = UploadPath + "14.txt";

                   if (needftp == true)
                   {
                       string userName = "modhd";
                       string password = "modhd!@#";
                       ReturnString = UploadPath + FS_OUTPUT_FILE_NAME;
                       //if (channelid == "51")
                       //    ReturnString = UploadPath + "13.txt";
                       //else if (channelid == "61")
                       //    ReturnString = UploadPath + "14.txt";
                       //string uploadUrl = "ftp://10.1.1.17/13.txt";
                       string uploadUrl = "ftp://10.1.1.17/" + FS_OUTPUT_FILE_NAME;

                       string MyFileName = ReturnString;
                       WebClient wc = new WebClient();
                       wc.Credentials = new NetworkCredential(userName, password);
                       byte[] fildData = wc.UploadFile(uploadUrl, MyFileName);
                       return "OK";
                   }
              
                   
                  return ReturnString;
               
              //return "ok";
            }
            catch (Exception ex)
            {
                return "寫入節目表有問題,訊息:" + ex.Message;
            }
            finally
            {
                myReader1.Close();
            }
        }
        

    }
}
