﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using PTS_MAM3.Web.Class;
using System.IO;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSPTS_AP 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSPTS_AP : System.Web.Services.WebService
    {
        /// <summary>
        /// 給PTS_取得需要搬檔的任務列表
        /// </summary>
        /// <param name="beginDate">起始日期(yyyy-MM-dd)，可帶空白</param>
        /// <param name="endDate">結束日期(yyyy-MM-dd)，可帶空白</param>
        /// <returns></returns>
        [WebMethod]
        public List<Class_MoveJob> PTS_Get_MoveJobs(string beginDate, string endDate)
        {
            List<Class_MoveJob> jobs = new List<Class_MoveJob>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FCCHECK_STATUS", "U");
            sqlParameters.Add("FCFILE_STATUS", "B");
            sqlParameters.Add("BegeinDate", beginDate);
            sqlParameters.Add("EndDate", endDate);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_GetMoveJobs", sqlParameters, out sqlResult))
                return new List<Class_MoveJob>();
            else
            {
                foreach (Dictionary<string, string> ss in sqlResult)
                {
                    jobs.Add(new Class_MoveJob() { VIDEO_ID = ss["FSVIDEO_PROG"].Substring(2, 6), UPDATED_DATE = Convert.ToDateTime(ss["FDUPDATED_DATE"]) });
                }


                string logContent = DateTime.Now.ToString() + Environment.NewLine;
                foreach (Class_MoveJob job in jobs)
                {
                    logContent += job.VIDEO_ID + "   " + job.UPDATED_DATE + Environment.NewLine;
                }
                logContent += "----------------------------------------------------" + Environment.NewLine;

                File.AppendAllText(@"D:\LogFiles\PTS_AP_" + DateTime.Now.ToString("yyyyMMdd") + ".txt", logContent);
            }
            return jobs;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="video_id">VIDEO_ID</param>
        /// <param name="status">狀態</param>
        /// <param name="percentage">進度</param>
        /// <param name="resendTimes">重試次數</param>
        /// <returns>0:false ; 1:true ;2:查無資料</returns>
        [WebMethod]
        public int PTS_Status_MoveJob(string video_id, int status, int percentage, int resendTimes)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> resultData;
            sqlParameters.Add("FSVIDEO_ID", "00" + video_id);


            if (MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_By_VideoID", sqlParameters, out resultData))
            {
                if (resultData.Count > 0)
                {
                    if (status == 1)
                    {
                        if (PTS_Start_MoveJob(video_id))
                        {
                            sqlParameters.Clear();
                            sqlParameters.Add("FSVIDEO_ID", "00" + video_id);

                            return Convert.ToInt32(MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBPTSAP_STATUS_LOG", sqlParameters, "PTS_AP"));
                        }
                        else
                            return 0;
                    }
                    else
                    {
                        if ((status == 7 && resendTimes == 2) || status == 88 || status == 99)
                        {
                            if (!PTS_Fail_MoveJob(video_id))
                            {
                                return 0;
                            }
                        }
                        else if (status == 80)
                        {
                            if (!PTS_Finish_MoveJob(video_id))
                            {
                                return 0;
                            }
                        }

                        sqlParameters.Clear();
                        sqlParameters.Add("FSVIDEO_ID", "00" + video_id);
                        sqlParameters.Add("FCSTATUS", status.ToString());
                        sqlParameters.Add("FSPERCENTAGE", percentage.ToString());
                        sqlParameters.Add("FCRESENDTIMES", resendTimes.ToString());

                        if (!MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBPTSAP_STATUS_LOG", sqlParameters, "PTS_AP"))
                        {
                            return 0;
                        };

                        return 1;
                    }
                }
                else
                {
                    return 2;
                }
            }


            return 0;
        }


        /// <summary>
        /// 給PTS_啟動搬檔任務
        /// </summary>
        /// <param name="video_id"></param>
        /// <returns></returns>
        //[WebMethod]
        public bool PTS_Start_MoveJob(string video_id)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            // List<Dictionary<string, string>> resultData;
            //sqlParameters.Add("FSVIDEO_ID", "00" + video_id);


            //if (MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_By_VideoID", sqlParameters, out resultData))
            //{
            //    if (resultData.Count > 0)
            //    {
            //        sqlParameters.Clear();
            sqlParameters.Add("FSVIDEO_ID", "00" + video_id);
            sqlParameters.Add("FCSTATUS", "S");
            sqlParameters.Add("FSUPDAYE_BY", "PTS_AP");

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBLOG_VIDEO_HD_MC", sqlParameters, "PTS_AP");
            //    }
            //    else
            //    {
            //        return "查無資料";
            //    }
            //}
            //return "false";

        }

        /// <summary>
        /// 給PTS_將搬檔任務狀態改為成功
        /// </summary>
        /// <param name="video_id"></param>
        /// <returns></returns>
        //[WebMethod]
        public bool PTS_Finish_MoveJob(string video_id)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            //List<Dictionary<string, string>> resultData;
            //sqlParameters.Add("FSVIDEO_ID", "00" + video_id);


            //if (MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_By_VideoID", sqlParameters, out resultData))
            //{
            //    if (resultData.Count > 0)
            //    {
            //        sqlParameters.Clear();
            sqlParameters.Add("FSVIDEO_ID", "00" + video_id);
            sqlParameters.Add("FCSTATUS", "C");
            sqlParameters.Add("FSUPDAYE_BY", "PTS_AP");

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBLOG_VIDEO_HD_MC", sqlParameters, "PTS_AP");
            //    }
            //    else
            //    {
            //        return "查無資料";
            //    }
            //}
            //return "false";
        }

        /// <summary>
        /// 給PTS_將搬檔任務狀態改為失敗
        /// </summary>
        /// <param name="video_id"></param>
        /// <returns></returns>
        //[WebMethod]
        public bool PTS_Fail_MoveJob(string video_id)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            //List<Dictionary<string, string>> resultData;
            //sqlParameters.Add("FSVIDEO_ID", "00" + video_id);


            //if (MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_By_VideoID", sqlParameters, out resultData))
            //{
            //    if (resultData.Count > 0)
            //    {
            //        sqlParameters.Clear();
            sqlParameters.Add("FSVIDEO_ID", "00" + video_id);
            sqlParameters.Add("FCSTATUS", "E");
            sqlParameters.Add("FSUPDAYE_BY", "PTS_AP");

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBLOG_VIDEO_HD_MC", sqlParameters, "PTS_AP");
            //    }
            //    else
            //    {
            //        return "查無資料";
            //    }
            //}
            //return "false";
        }


    }
}
