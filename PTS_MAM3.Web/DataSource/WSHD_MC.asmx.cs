﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using PTS_MAM3.Web.Class;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSHD_MC 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSHD_MC : System.Web.Services.WebService
    {

        [WebMethod]
        public bool Add_HD_MC(string bro_id,string fsfile_no, string video_id, string user_id)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSBRO_ID", bro_id);
            sqlParameters.Add("FSFILE_NO", fsfile_no);
            sqlParameters.Add("FSVIDEO_ID", video_id);
            sqlParameters.Add("FSCREATED_BY", user_id);


            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBLOG_VIDEO_HD_MC", sqlParameters, user_id);
        }

        /// <summary>
        /// 根據VideoID 更改檔案狀態
        /// </summary>
        /// <param name="video_id"></param>
        /// <param name="status"></param>
        /// <param name="user_id"></param>
        /// <returns></returns>
        [WebMethod]
        public bool Update_HD_MC_STATUS(string video_id, string status, string user_id)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSVIDEO_ID", video_id);
            sqlParameters.Add("FCSTATUS", status);
            sqlParameters.Add("FSUPDAYE_BY", user_id);


            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBLOG_VIDEO_HD_MC", sqlParameters, user_id);
        }

        /// <summary>
        /// 根據送播單單號 或 檔案編號 或 VideoID 取得 TBLOG_VIDEO_HD_MC 中的所有資料
        /// </summary>
        /// <param name="bro_id">送播單單號，可帶空白</param>
        /// <param name="file_no">檔案編號，可帶空白</param>
        /// <param name="video_id">VideoID，可帶空白</param>
        /// <returns></returns>
        [WebMethod]
        public List<Class_TBLOG_VIDEO_HD_MC> Get_HD_MC(string bro_id, string file_no, string video_id)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSVIDEO_ID", video_id);
            sqlParameters.Add("FSBRO_ID", bro_id);
            sqlParameters.Add("FSFILE_NO", file_no);

            List<Dictionary<string, string>> sqlResult;
            if (MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_HD_MC", sqlParameters, out sqlResult))
            {
                List<Class_TBLOG_VIDEO_HD_MC> resultList = new List<Class_TBLOG_VIDEO_HD_MC>();
                foreach (Dictionary<string, string> dic in sqlResult)
                {
                    Class_TBLOG_VIDEO_HD_MC hd_mc = new Class_TBLOG_VIDEO_HD_MC();
                    hd_mc.FSBRO_ID = dic["FSBRO_ID"];
                    hd_mc.FSVIDEO_ID = dic["FSVIDEO_ID"];
                    hd_mc.FSFILE_NO = dic["FSFILE_NO"];
                    hd_mc.FCSTATUS = dic["FCSTATUS"];
                    hd_mc.FSCREATED_BY = dic["FSCREATED_BY"];
                    hd_mc.FSUPDATED_BY = dic["FSUPDATED_BY"];
                    hd_mc.FDCREATED_DATE = dic["FDCREATED_DATE"];
                    hd_mc.FDUPDATED_DATE = dic["FDUPDATED_DATE"];

                    resultList.Add(hd_mc);
                }

                return resultList;

            }

            return new List<Class_TBLOG_VIDEO_HD_MC>();
        }

        /// <summary>
        /// 根據VideoID 刪除 TBLOG_VIDEO_HD_MC中的資料，重新啟動搬檔
        /// </summary>
        /// <param name="video_id"></param>
        /// <returns></returns>
        [WebMethod]
        public bool Delete_HD_MC(string video_id)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSVIDEO_ID", video_id);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_D_TBLOG_VIDEO_HD_MC", sqlParameters, out sqlResult))
                return false;

            return true;
        }

        /// <summary>
        /// 根據送播單單號刪除 TBLOG_VIDEO_HD_MC中的資料，重新啟動搬檔
        /// </summary>
        /// <param name="video_id"></param>
        /// <returns></returns>
        [WebMethod]
        public bool Delete_HD_MC_BY_BROID(string bro_id)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSBROID", bro_id);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_D_TBLOG_VIDEO_HD_MC_BY_BROID", sqlParameters, out sqlResult))
                return false;

            return true;
        }

        /// <summary>
        /// 根據各表單的狀態取得VideoID的列表
        /// </summary>
        /// <param name="broadcast_Status">送播單狀態，可帶空白</param>
        /// <param name="file_Status">檔案狀態，可帶空白</param>
        /// <param name="hd_mc_Status">搬檔與審核狀態，可帶空白</param>
        /// <returns>VideoID的集合</returns>
        [WebMethod]
        public List<string> Get_VideoID_By_Status(string broadcast_Status, string file_Status, string hd_mc_Status)
        {
            List<string> video_ids = new List<string>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FCCHECK_STATUS", broadcast_Status);
            sqlParameters.Add("FCFILE_STATUS", file_Status);
            sqlParameters.Add("FCSTATUS", hd_mc_Status);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_GetMoveJobs", sqlParameters, out sqlResult))
                return new List<string>();
            else
            {
                foreach (Dictionary<string, string> ss in sqlResult)
                {
                    video_ids.Add(ss["FSVIDEO_PROG"]);
                }
            }
            return video_ids;
        }
    }
}
