﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using PTS_MAM3.Web.WS.WSASC;
using PTS_MAM3.Web.WS.WSTSM;
using PTS_MAM3.Web.Class;
using System.Drawing;
using Xealcom.Security;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// 轉檔
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSSTT : System.Web.Services.WebService
    {
        [WebMethod]  //透過檔案編號，檢查JobID及檢查轉檔是否成功
        public Boolean CallIngest_Check(string strFileNO)
        {
            string strCheckJobID = "";  //檢查JobID 
            string strHandler = "";     //搬檔完成要回呼的sahx

            //檢查JobID及檢查轉檔是否成功，若是無JobID或是轉檔失敗回傳False，讓前端繼續選擇VTR或是數位檔案轉檔
            //若是有JobID且轉檔成功，就直接進行搬檔，並告知前端使用者進行搬檔
            strCheckJobID = QUERY_TBLOG_VIDEO_JobID(strFileNO);
            if (strCheckJobID != "")
            {
                //檢查該JobID 的「進度」，判斷是轉檔錯或搬檔錯，若是轉檔完成就是搬檔錯，就要重新搬擋
                //若是轉檔錯誤，則繼續後面的步驟，重新轉檔
                if (GetTranscode_Progress(strCheckJobID) == "完成")
                {
                    MAM_PTS_DLL.SysConfig objConfig = new MAM_PTS_DLL.SysConfig();
                    strHandler = objConfig.sysConfig_Read("/ServerConfig/AshxHandler/ANS_STT");

                    // 發出 POST 更新通知
                    ServiceTranscode objServiceTranscode = new ServiceTranscode();
                    objServiceTranscode.SendTranscodeJobHttpMessage(strHandler, strCheckJobID, WS.WSASC.ServiceTranscode.TRANSCODE_STATUS.FINISH.ToString(), "MetaSAN Function Retry Trigger");
                    return true;
                }
                else
                    return false;   //轉檔失敗  
            }
            else
                return false;       //無JobID就當作轉檔失敗       
        }

        [WebMethod]  //呼叫轉檔
        public string CallIngest(string strFileNO, string strType, string strSource, string strBeg, string strEnd, string strHiPath, string strLowPath, string strNote, string strFileTypeSPEC)
        {
            ServiceTranscode Tsobj = new ServiceTranscode();
            DoIngestXml Insobj = new DoIngestXml();
            ServiceTSM obj = new ServiceTSM();
            string strErrorH = "";  //高解檔案錯誤訊息
            string strErrorL = "";  //低解檔案錯誤訊息

            //轉檔前檢查目錄資料夾是否存在
            if (CheckTranscode_Path("H", strHiPath, out strErrorH) == false || CheckTranscode_Path("L", strLowPath, out strErrorL) == false)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/WSSTT/CallIngest", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, strErrorH + strErrorL);
                return "E：" + strErrorH + strErrorL;
            }

            //轉檔Profile位置           
            MAM_PTS_DLL.SysConfig objConfig = new MAM_PTS_DLL.SysConfig();
            string strHandler = objConfig.sysConfig_Read("/ServerConfig/AshxHandler/ANS_STT");
            //ANS_STT_HD 2015-11-17 HD送播 新的Config 設定新的 HandlerRecevive
            string strProfile = objConfig.sysConfig_Read("/ServerConfig/ANS_Config/PROFILE_MAM_MXF");
            string strProfileHD = objConfig.sysConfig_Read("/ServerConfig/ANS_Config/PROFILE_MAM_HD");
            //要存入的MetaSan空間，例如 T:\IngestTemp\${fileNo}.mxf 或是 T:\IngestTemp\${fileNo}.wmv 
            string strTempPath = objConfig.sysConfig_Read("/ServerConfig/STT_Config/INGEST_TEMP_DIR_UNC");
            //是否要取得關鍵影格(節目、短帶、資料帶都要個別區分)
            string strProg_CaptureKeyFrame = objConfig.sysConfig_Read("/ServerConfig/STT_Config/PROG_CAPTURE_KEYFRAME");
            string strPromo_CaptureKeyFrame = objConfig.sysConfig_Read("/ServerConfig/STT_Config/PROMO_CAPTURE_KEYFRAME");
            string strData_CaptureKeyFrame = objConfig.sysConfig_Read("/ServerConfig/STT_Config/DATA_CAPTURE_KEYFRAME");

            //逐筆塞入轉檔需要的參數

            //關鍵影格(節目、短帶、資料帶各別判斷是否要截取關鍵影格)
            if (strFileNO.StartsWith("G") == true && strProg_CaptureKeyFrame == "N" || strFileNO.StartsWith("P") == true && strPromo_CaptureKeyFrame == "N" || strFileNO.StartsWith("D") == true && strData_CaptureKeyFrame == "N")
                Insobj.CaptureKeyframe = "N";
            else
                Insobj.CaptureKeyframe = "Y";

            Insobj.CreateLowRes = "Y";                 //低解            
            Insobj.CreateHightRes = "Y";// 2015 11 17 HD送播檔不轉高解
            Insobj.DestinationTo = strTempPath + strFileNO.Trim() + Path.GetExtension(strHiPath);//目的地(TSM路徑_高解)，副檔名直接剖析
            //Insobj.DestinationTo = strHiPath ;        //因應網路速度，之後都先搬到MetaSan再到Mamstream，所以要改寫法
            Insobj.DisplayLogo = "N";                     //顯示LOGO    

            Insobj.StartTimecode = strBeg;              //起始TiemeCode
            Insobj.EndTimecode = strEnd;               //結束TiemeCode
            Insobj.LogoProfilePath = string.Empty;

            Insobj.LowResDir = strTempPath;             //低解目錄(TSM路徑_低解)
            //Insobj.LowResDir = System.IO.Path.GetDirectoryName(strLowPath);  //因應網路速度，之後都先搬到MetaSan再到Mamstream，所以要改寫法

            Insobj.Note = strNote;          //備註
            Insobj.PostbackAddress = strHandler;        //轉檔結束時要呼叫的ashx
            Insobj.SourceFrom = strSource;             //來源
            Insobj.TransCodeType = strType;

            if (strFileTypeSPEC == "SD")
            {
                Insobj.EncoderProfliePath = strProfile;    //SD轉檔Profile位置
                Insobj.SourceFormat = "SD";                //SD來源格式
                Insobj.DestinationFormat = "SD";            //SD目的格式
            }
            else if (strFileTypeSPEC == "HD")
            {
                Insobj.EncoderProfliePath = strProfileHD;  //HD轉檔Profile位置
                Insobj.SourceFormat = "HD";                //HD來源格式
                Insobj.DestinationFormat = "HD";            //HD目的格式
            }

            long JobID = Tsobj.DoTranscode(Insobj); ;

            if (JobID < 0)
                return "";
            else
                return JobID.ToString();
        }

        [WebMethod]  //呼叫轉檔
        public string CallIngest_HD(string strFileNO, string strType, string strSource, string strBeg, string strEnd, string strLowPath, string strNote, string strFileTypeSPEC)
        {
            ServiceTranscode Tsobj = new ServiceTranscode();
            DoIngestXml Insobj = new DoIngestXml();
            ServiceTSM obj = new ServiceTSM();
            string strErrorH = "";  //高解檔案錯誤訊息
            string strErrorL = "";  //低解檔案錯誤訊息

            //轉檔前檢查目錄資料夾是否存在
            if (CheckTranscode_Path("L", strLowPath, out strErrorL) == false)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/WSSTT/CallIngest_HD", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, strErrorH + strErrorL);
                return "E：" + strErrorH + strErrorL;
            }

            //轉檔Profile位置           
            MAM_PTS_DLL.SysConfig objConfig = new MAM_PTS_DLL.SysConfig();
            string strHandler = objConfig.sysConfig_Read("/ServerConfig/AshxHandler/ANS_STT_HD");
            //ANS_STT_HD 2015-11-17 HD送播 新的Config 設定新的 HandlerRecevive
            string strProfile = objConfig.sysConfig_Read("/ServerConfig/ANS_Config/PROFILE_MAM_MXF");
            string strProfileHD = objConfig.sysConfig_Read("/ServerConfig/ANS_Config/PROFILE_MAM_HD");
            //要存入的MetaSan空間，例如 T:\IngestTemp\${fileNo}.mxf 或是 T:\IngestTemp\${fileNo}.wmv 
            string strTempPath = objConfig.sysConfig_Read("/ServerConfig/STT_Config/INGEST_TEMP_DIR_UNC");
            //是否要取得關鍵影格(節目、短帶、資料帶都要個別區分)
            string strProg_CaptureKeyFrame = objConfig.sysConfig_Read("/ServerConfig/STT_Config/PROG_CAPTURE_KEYFRAME");
            string strPromo_CaptureKeyFrame = objConfig.sysConfig_Read("/ServerConfig/STT_Config/PROMO_CAPTURE_KEYFRAME");
            string strData_CaptureKeyFrame = objConfig.sysConfig_Read("/ServerConfig/STT_Config/DATA_CAPTURE_KEYFRAME");

            //逐筆塞入轉檔需要的參數

            //關鍵影格(節目、短帶、資料帶各別判斷是否要截取關鍵影格)
            if (strFileNO.StartsWith("G") == true && strProg_CaptureKeyFrame == "N" || strFileNO.StartsWith("P") == true && strPromo_CaptureKeyFrame == "N" || strFileNO.StartsWith("D") == true && strData_CaptureKeyFrame == "N")
                Insobj.CaptureKeyframe = "N";
            else
                Insobj.CaptureKeyframe = "Y";

            Insobj.CreateLowRes = "Y";                 //低解            
            Insobj.CreateHightRes = "N";// 2015-11-17 HD送播檔不轉高解
            Insobj.DestinationTo = strTempPath + strFileNO.Trim() + Path.GetExtension(strLowPath);//目的地(TSM路徑_高解)，副檔名直接剖析
            //Insobj.DestinationTo = strHiPath ;        //因應網路速度，之後都先搬到MetaSan再到Mamstream，所以要改寫法
            Insobj.DisplayLogo = "N";                     //顯示LOGO    

            Insobj.StartTimecode = strBeg;              //起始TiemeCode
            Insobj.EndTimecode = strEnd;               //結束TiemeCode
            Insobj.LogoProfilePath = string.Empty;

            Insobj.LowResDir = strTempPath;             //低解目錄(TSM路徑_低解)
            //Insobj.LowResDir = System.IO.Path.GetDirectoryName(strLowPath);  //因應網路速度，之後都先搬到MetaSan再到Mamstream，所以要改寫法

            Insobj.Note = strNote;          //備註
            Insobj.PostbackAddress = strHandler;        //轉檔結束時要呼叫的ashx
            Insobj.SourceFrom = strSource;             //來源
            Insobj.TransCodeType = strType;

            if (strFileTypeSPEC == "SD")
            {
                Insobj.EncoderProfliePath = strProfile;    //SD轉檔Profile位置
                Insobj.SourceFormat = "SD";                //SD來源格式
                Insobj.DestinationFormat = "SD";            //SD目的格式
            }
            else if (strFileTypeSPEC == "HD")
            {
                Insobj.EncoderProfliePath = strProfileHD;  //HD轉檔Profile位置
                Insobj.SourceFormat = "HD";                //HD來源格式
                Insobj.DestinationFormat = "HD";            //HD目的格式
            }

            long JobID = Tsobj.DoTranscode(Insobj); ;

            if (JobID < 0)
                return "";
            else
                return JobID.ToString();
        }

        [WebMethod]  //呼叫轉檔
        public string CallIngest_SD(string strFileNO, string strType, string strSource, string strBeg, string strEnd, string strLowPath, string strNote, string strFileTypeSPEC)
        {
            ServiceTranscode Tsobj = new ServiceTranscode();
            DoIngestXml Insobj = new DoIngestXml();
            ServiceTSM obj = new ServiceTSM();
            string strErrorH = "";  //高解檔案錯誤訊息
            string strErrorL = "";  //低解檔案錯誤訊息

            //轉檔前檢查目錄資料夾是否存在
            if (CheckTranscode_Path("L", strLowPath, out strErrorH) == false || CheckTranscode_Path("L", strLowPath, out strErrorL) == false)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/WSSTT/CallIngest_SD", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, strErrorH + strErrorL);
                return "E：" + strErrorH + strErrorL;
            }

            //轉檔Profile位置           
            MAM_PTS_DLL.SysConfig objConfig = new MAM_PTS_DLL.SysConfig();
            string strHandler = objConfig.sysConfig_Read("/ServerConfig/AshxHandler/ANS_STT_HD");
            //ANS_STT_HD 2015-11-17 HD送播 新的Config 設定新的 HandlerRecevive
            string strProfile = objConfig.sysConfig_Read("/ServerConfig/ANS_Config/PROFILE_MAM_MXF");
            string strProfileHD = objConfig.sysConfig_Read("/ServerConfig/ANS_Config/PROFILE_MAM_HD");
            //要存入的MetaSan空間，例如 T:\IngestTemp\${fileNo}.mxf 或是 T:\IngestTemp\${fileNo}.wmv 
            string strTempPath = objConfig.sysConfig_Read("/ServerConfig/STT_Config/INGEST_TEMP_DIR_UNC");
            //是否要取得關鍵影格(節目、短帶、資料帶都要個別區分)
            string strProg_CaptureKeyFrame = objConfig.sysConfig_Read("/ServerConfig/STT_Config/PROG_CAPTURE_KEYFRAME");
            string strPromo_CaptureKeyFrame = objConfig.sysConfig_Read("/ServerConfig/STT_Config/PROMO_CAPTURE_KEYFRAME");
            string strData_CaptureKeyFrame = objConfig.sysConfig_Read("/ServerConfig/STT_Config/DATA_CAPTURE_KEYFRAME");

            //逐筆塞入轉檔需要的參數

            //關鍵影格(節目、短帶、資料帶各別判斷是否要截取關鍵影格)
            if (strFileNO.StartsWith("G") == true && strProg_CaptureKeyFrame == "N" || strFileNO.StartsWith("P") == true && strPromo_CaptureKeyFrame == "N" || strFileNO.StartsWith("D") == true && strData_CaptureKeyFrame == "N")
                Insobj.CaptureKeyframe = "N";
            else
                Insobj.CaptureKeyframe = "Y";

            Insobj.CreateLowRes = "Y";                 //低解            
            Insobj.CreateHightRes = "N";// 2016 05 23 SD送播檔不轉高解
            Insobj.DestinationTo = strTempPath + strFileNO.Trim() + Path.GetExtension(strLowPath);//目的地(TSM路徑_高解)，副檔名直接剖析
            //Insobj.DestinationTo = strHiPath ;        //因應網路速度，之後都先搬到MetaSan再到Mamstream，所以要改寫法
            Insobj.DisplayLogo = "N";                     //顯示LOGO    

            Insobj.StartTimecode = strBeg;              //起始TiemeCode
            Insobj.EndTimecode = strEnd;               //結束TiemeCode
            Insobj.LogoProfilePath = string.Empty;

            Insobj.LowResDir = strTempPath;             //低解目錄(TSM路徑_低解)
            //Insobj.LowResDir = System.IO.Path.GetDirectoryName(strLowPath);  //因應網路速度，之後都先搬到MetaSan再到Mamstream，所以要改寫法

            Insobj.Note = strNote;          //備註
            Insobj.PostbackAddress = strHandler;        //轉檔結束時要呼叫的ashx
            Insobj.SourceFrom = strSource;             //來源
            Insobj.TransCodeType = strType;

            if (strFileTypeSPEC == "SD")
            {
                Insobj.EncoderProfliePath = strProfile;    //SD轉檔Profile位置
                Insobj.SourceFormat = "SD";                //SD來源格式
                Insobj.DestinationFormat = "SD";            //SD目的格式
            }
            else if (strFileTypeSPEC == "HD")
            {
                Insobj.EncoderProfliePath = strProfileHD;  //HD轉檔Profile位置
                Insobj.SourceFormat = "HD";                //HD來源格式
                Insobj.DestinationFormat = "HD";            //HD目的格式
            }

            long JobID = Tsobj.DoTranscode(Insobj); ;

            if (JobID < 0)
                return "";
            else
                return JobID.ToString();
        }

        [WebMethod]  //呼叫轉檔
        public string CallIngest_SSD(string strFileNO, string strType, string strSource, string strBeg, string strEnd, string strHiPath, string strLowPath, string strNote, string strFileTypeSPEC)
        {
            ServiceTranscode Tsobj = new ServiceTranscode();
            DoIngestXml Insobj = new DoIngestXml();
            ServiceTSM obj = new ServiceTSM();
            string strErrorH = "";  //高解檔案錯誤訊息
            string strErrorL = "";  //低解檔案錯誤訊息

            //轉檔前檢查目錄資料夾是否存在
            if (CheckTranscode_Path("H", strHiPath, out strErrorH) == false || CheckTranscode_Path("L", strLowPath, out strErrorL) == false)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/WSSTT/CallIngest", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, strErrorH + strErrorL);
                return "E：" + strErrorH + strErrorL;
            }

            //轉檔Profile位置           
            MAM_PTS_DLL.SysConfig objConfig = new MAM_PTS_DLL.SysConfig();
            string strHandler = objConfig.sysConfig_Read("/ServerConfig/AshxHandler/ANS_STT");
            //ANS_STT_HD 2015-11-17 HD送播 新的Config 設定新的 HandlerRecevive
            string strProfile = objConfig.sysConfig_Read("/ServerConfig/ANS_Config/PROFILE_MAM_MXF");
            string strProfileHD = objConfig.sysConfig_Read("/ServerConfig/ANS_Config/PROFILE_MAM_HD");
            //要存入的MetaSan空間，例如 T:\IngestTemp\${fileNo}.mxf 或是 T:\IngestTemp\${fileNo}.wmv 
            string strTempPath = objConfig.sysConfig_Read("/ServerConfig/STT_Config/INGEST_TEMP_DIR_UNC");
            //是否要取得關鍵影格(節目、短帶、資料帶都要個別區分)
            string strProg_CaptureKeyFrame = objConfig.sysConfig_Read("/ServerConfig/STT_Config/PROG_CAPTURE_KEYFRAME");
            string strPromo_CaptureKeyFrame = objConfig.sysConfig_Read("/ServerConfig/STT_Config/PROMO_CAPTURE_KEYFRAME");
            string strData_CaptureKeyFrame = objConfig.sysConfig_Read("/ServerConfig/STT_Config/DATA_CAPTURE_KEYFRAME");

            //逐筆塞入轉檔需要的參數

            //關鍵影格(節目、短帶、資料帶各別判斷是否要截取關鍵影格)
            if (strFileNO.StartsWith("G") == true && strProg_CaptureKeyFrame == "N" || strFileNO.StartsWith("P") == true && strPromo_CaptureKeyFrame == "N" || strFileNO.StartsWith("D") == true && strData_CaptureKeyFrame == "N")
                Insobj.CaptureKeyframe = "N";
            else
                Insobj.CaptureKeyframe = "Y";

            Insobj.CreateLowRes = "Y";                 //低解            
            Insobj.CreateHightRes = "N";
            Insobj.DestinationTo = strTempPath + strFileNO.Trim() + Path.GetExtension(strHiPath);//目的地(TSM路徑_高解)，副檔名直接剖析
            //Insobj.DestinationTo = strHiPath ;        //因應網路速度，之後都先搬到MetaSan再到Mamstream，所以要改寫法
            Insobj.DisplayLogo = "N";                     //顯示LOGO    

            Insobj.StartTimecode = strBeg;              //起始TiemeCode
            Insobj.EndTimecode = strEnd;               //結束TiemeCode
            Insobj.LogoProfilePath = string.Empty;

            Insobj.LowResDir = strTempPath;             //低解目錄(TSM路徑_低解)
            //Insobj.LowResDir = System.IO.Path.GetDirectoryName(strLowPath);  //因應網路速度，之後都先搬到MetaSan再到Mamstream，所以要改寫法

            Insobj.Note = strNote;          //備註
            Insobj.PostbackAddress = strHandler;        //轉檔結束時要呼叫的ashx
            Insobj.SourceFrom = strSource;             //來源
            Insobj.TransCodeType = strType;

            if (strFileTypeSPEC == "SD")
            {
                Insobj.EncoderProfliePath = strProfile;    //SD轉檔Profile位置
                Insobj.SourceFormat = "SD";                //SD來源格式
                Insobj.DestinationFormat = "SD";            //SD目的格式
            }
            else if (strFileTypeSPEC == "HD")
            {
                Insobj.EncoderProfliePath = strProfileHD;  //HD轉檔Profile位置
                Insobj.SourceFormat = "HD";                //HD來源格式
                Insobj.DestinationFormat = "HD";            //HD目的格式
            }

            long JobID = Tsobj.DoTranscode(Insobj); ;

            if (JobID < 0)
                return "";
            else
                return JobID.ToString();
        }


        [WebMethod]    //取得VTR LIST
        public List<ClassVTR> GetVTRList()
        {
            WS.WSASC.ServiceTranscode obj = new WS.WSASC.ServiceTranscode();
            List<ClassVTR> VCRList = obj.GetAvailableVTRList();

            return VCRList;
        }

        [WebMethod]    //取得File LIST
        public List<string> GetFileList()
        {
            //線上簽收位置
            string strSTT_Desc = "";
            MAM_PTS_DLL.SysConfig ob = new MAM_PTS_DLL.SysConfig();
            strSTT_Desc = ob.sysConfig_Read("/ServerConfig/STT_UPLOAD");  //@

            //strSTT_Desc = @"\\10.13.200.2\MasterCtrl\TEMP_UPLOAD\";   //先隨便寫一個位置，只要讀出檔案即可

            if (strSTT_Desc.Trim() == "")
                return new List<string>();

            ImpersonateUser iu = new ImpersonateUser();
            iu.Impersonate(@"pts.org.tw", @"mamsystem", @"p@ssw0rd");

            List<string> FileList = new List<string>(Directory.GetFiles(strSTT_Desc));
            iu.Undo();
            return FileList;
        }

        [WebMethod]    //取得File大小
        public string GetFileSize(string strSize)
        {
            ImpersonateUser iu = new ImpersonateUser();
            iu.Impersonate(@"pts.org.tw", @"mamsystem", @"p@ssw0rd");

            string fileSize = MAMFunctions.CheckFileSize(strSize);
            iu.Undo();
            return fileSize;
        }

        [WebMethod]    //取得File路徑
        public string GetSTTFilePath(string strUpLoadType)
        {
            //線上簽收位置
            string strSTT_Desc = "";
            MAM_PTS_DLL.SysConfig ob = new MAM_PTS_DLL.SysConfig();

            if (strUpLoadType == "Fiber")
                strSTT_Desc = ob.sysConfig_Read("/ServerConfig/STT_UPLOAD_FIBER");
            else if (strUpLoadType == "Network")
                strSTT_Desc = ob.sysConfig_Read("/ServerConfig/STT_UPLOAD");
            else if (strUpLoadType == "Other")
                strSTT_Desc = ob.sysConfig_Read("/ServerConfig/STT_OTHERVIDEO");

            return strSTT_Desc;
        }

        [WebMethod]    //取得送帶轉檔暫存區目錄(最主要的一層)
        public List<string> GetTopDirectories(string strUpLoadType)
        {
            //線上簽收位置
            string strSTT_Desc = "";

            MAM_PTS_DLL.SysConfig ob = new MAM_PTS_DLL.SysConfig();

            if (strUpLoadType == "Fiber")
                strSTT_Desc = ob.sysConfig_Read("/ServerConfig/STT_UPLOAD_FIBER");
            //else
            //    strSTT_Desc = ob.sysConfig_Read("/ServerConfig/STT_UPLOAD");  
            else if (strUpLoadType == "Network")
                strSTT_Desc = ob.sysConfig_Read("/ServerConfig/STT_UPLOAD");
            else if (strUpLoadType == "Other")
                strSTT_Desc = ob.sysConfig_Read("/ServerConfig/STT_OTHERVIDEO");

            if (strSTT_Desc.Trim() == "")
                return new List<string>();

            ImpersonateUser iu = new ImpersonateUser();
            iu.Impersonate(@"pts.org.tw", @"mamsystem", @"p@ssw0rd");

            if (Directory.Exists(strSTT_Desc) == false) //先確認是否有此路徑，若無就回傳空的內容
                return new List<string>();

            List<string> DirectoriesList = new List<string>(Directory.GetDirectories(strSTT_Desc));
            iu.Undo();
            return DirectoriesList;
        }

        [WebMethod]    //取得目錄
        public List<string> GetDirectories(string strPath)
        {
            if (strPath.Trim() == "")
                return new List<string>();

            ImpersonateUser iu = new ImpersonateUser();
            iu.Impersonate(@"pts.org.tw", @"mamsystem", @"p@ssw0rd");

            List<string> DirectoriesList = new List<string>(Directory.GetDirectories(strPath));
            iu.Undo();
            return DirectoriesList;
        }

        [WebMethod]    //取得目錄下的檔案
        public List<Class_File_Info> GetDirectorieFiles(string strPath)
        {
            //if (strPath.Trim() == "")
            //    return new List<string>();
            //List<string> FileList = new List<string>(Directory.GetFiles(strPath));
            //return FileList;

            List<Class_File_Info> Listobj = new List<Class_File_Info>();

            ImpersonateUser iu = new ImpersonateUser();
            iu.Impersonate(@"pts.org.tw", @"mamsystem", @"p@ssw0rd");


            if (Directory.Exists(strPath) == false) //先確認是否有此路徑，若無就回傳空的內容
                return Listobj;
            List<string> FileList = new List<string>(Directory.GetFiles(strPath));

            for (int i = 0; i < FileList.Count; i++)
            {
                //濾掉不認識的副檔名，只保留設定檔裡的
                if (File.Exists(FileList[i]) == true && fnCheckInSupportFileExtension(FileList[i]) == true)
                {
                    Class_File_Info obj = new Class_File_Info();
                    FileInfo EachFile = new FileInfo(FileList[i]);

                    obj.FSFile_Name = EachFile.Name;
                    obj.FSFile_Path = FileList[i];
                    obj.FSFile_Size = "大小:" + MAMFunctions.CheckFileSize(EachFile.Length.ToString());
                    obj.FSFile_Extension = " 類型:" + EachFile.Extension.Substring(1);
                    obj.FSFile_LastWriteTime = EachFile.LastWriteTime.ToString();
                    Listobj.Add(obj);
                }
            }
            iu.Undo();
            return Listobj;
        }

        [WebMethod]    //取得目錄下的檔案
        public List<Class_File_Info> GetDirectorieFiles_Without_Filter(string strPath)
        {
            //if (strPath.Trim() == "")
            //    return new List<string>();
            //List<string> FileList = new List<string>(Directory.GetFiles(strPath));
            //return FileList;

            List<Class_File_Info> Listobj = new List<Class_File_Info>();

            string whitelistingStr = File.ReadAllText(@"D:\WebSite\PTSMAM\Whitelisting.txt");
            
            string[] whitelistingArr = whitelistingStr.Split(new string[] { ";", " " }, StringSplitOptions.RemoveEmptyEntries);

            ImpersonateUser iu = new ImpersonateUser();
            iu.Impersonate(@"pts.org.tw", @"mamsystem", @"p@ssw0rd");

            if (Directory.Exists(strPath) == false) //先確認是否有此路徑，若無就回傳空的內容
                return Listobj;



            List<string> FileList = new List<string>(Directory.GetFiles(strPath));

            for (int i = 0; i < FileList.Count; i++)
            {

                if (whitelistingArr.Contains(System.IO.Path.GetExtension(FileList[i]).ToLower()) && File.Exists(FileList[i]) == true)
                {
                    Class_File_Info obj = new Class_File_Info();
                    FileInfo EachFile = new FileInfo(FileList[i]);

                    obj.FSFile_Name = EachFile.Name;
                    obj.FSFile_Path = FileList[i];
                    obj.FSFile_Size = "大小:" + MAMFunctions.CheckFileSize(EachFile.Length.ToString());
                    obj.FSFile_Extension = " 類型:" + EachFile.Extension.Substring(1);
                    obj.FSFile_LastWriteTime = EachFile.LastWriteTime.ToString();
                    Listobj.Add(obj);
                }
            }

            iu.Undo();
            return Listobj;
        }

        //判斷附檔名格式是否為轉檔中心可以接受的格式
        private bool fnCheckInSupportFileExtension(string FilePath)
        {
            MAM_PTS_DLL.SysConfig ob = new MAM_PTS_DLL.SysConfig();

            List<string> supportExtName = new List<string>(ob.sysConfig_Read("ServerConfig/ANS_Config/SUPPORT_EXT").Split(';'));
            foreach (string extName in supportExtName)
                if (Path.GetExtension(FilePath).ToLower() == extName.ToLower())
                    return true;
            return false;
        }

        [WebMethod]    //取得轉檔進度
        public string GetTranscode_Progress(string strObj)
        {
            if (strObj.Trim() == "")
                return "異常";

            long transcodeID = long.Parse(strObj);

            WS.WSASC.ServiceTranscode obj = new WS.WSASC.ServiceTranscode();
            ServiceAnystream.QueryAnsJobProgress status = obj.QueryTranscodeStatus(transcodeID);

            if (status.isERROR == false && status.isFINISH == true)
                return "完成";
            else if (status.isERROR == false && status.isFINISH == false)
                return status.progress.ToString();
            else
                return "異常";
        }

        [WebMethod]    //轉檔前檢查目錄資料夾是否存在，若是不在要補建
        public Boolean CheckTranscode_Path(string strSW, string strPath, out string strMsg)
        {
            try
            {
                strMsg = "";

                if (!Directory.Exists(Path.GetDirectoryName(strPath)))
                {
                    //如果沒有目錄就建立目錄
                    Directory.CreateDirectory(Path.GetDirectoryName(strPath));

                    if (!Directory.Exists(Path.GetDirectoryName(strPath)))
                    {
                        if (strSW.Trim() == "H")
                        {
                            strMsg = "高解檔案建立目錄失敗，失敗路徑：" + Path.GetDirectoryName(strPath);
                            return false;
                        }
                        else if (strSW.Trim() == "L")
                        {
                            strMsg = "低解檔案建立目錄失敗，失敗路徑：" + Path.GetDirectoryName(strPath);
                            return false;
                        }
                        else
                        {
                            strMsg = "檔案目錄建立失敗，失敗路徑：" + Path.GetDirectoryName(strPath);
                            return false;
                        }
                    }
                    else
                        return true;

                }
                else
                    return true;
            }
            catch
            {
                strMsg = "檔案目錄檢查或建立失敗，失敗路徑：" + Path.GetDirectoryName(strPath);
                return false;
            }
        }

        //檢查轉檔的JobID (轉檔啟動時要檢查)
        public string QUERY_TBLOG_VIDEO_JobID(string strFILE_NO)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSFILE_NO", strFILE_NO);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO", sqlParameters, out sqlResult))
                return "";

            if (sqlResult.Count > 0 && sqlResult[0]["FSJOB_ID"].Trim() != "0")
                return sqlResult[0]["FSJOB_ID"].Trim();
            else
                return "";
        }
    }
}
