﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSAgentFieldDelete 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSAgentFieldDelete : System.Web.Services.WebService
    {

        [WebMethod]
        public string DelAgentField(string TableName,string ID,string UserId)
        {
            bool IsSuccess;
            string returnString="";
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSTABLE_NAME", TableName);
            sqlParameters.Add("FSFILE_NO", ID);
            //IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_TBZAgentField", sqlParameters, UserId);
            IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction_With_DBLink("SP_D_TBZAgentField", sqlParameters, out returnString, UserId, true);
            if (IsSuccess == true)
            { returnString = "true"; }
            else if (IsSuccess == false)
            { return returnString; }
            return returnString;
        }

        [WebMethod]
        public string DelAgentFieldDetail(string TableName, string ID1, string ID2, string UserId)
        {
            bool IsSuccess;
            string returnString = "";
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSTABLE_NAME", TableName);
            sqlParameters.Add("ID1", ID1);
            sqlParameters.Add("ID2", ID2);
            //IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_TBZAgentField", sqlParameters, UserId);
            IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction_With_DBLink("SP_D_TBZAgentFieldDetail", sqlParameters, out returnString, UserId, true);
            if (IsSuccess == true)
            { returnString = "true"; }
            else if (IsSuccess == false)
            { return returnString; }
            return returnString;
        }
    }
}
