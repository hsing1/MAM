﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using PTS_MAM3.Web.Class.TBZ;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSTBZ 用於處理代碼檔的WS
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSTBZ : System.Web.Services.WebService
    {
        /// <summary>
        /// 查詢國家來源代碼檔
        /// </summary>
        /// <returns>List<Class_TBZPROGNAION></returns>
        [WebMethod]
        public List<Class_TBZPROGNAION> Q_TBZPROGNATION()
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult = new List<Dictionary<string, string>>();
            List<Class_TBZPROGNAION> returnResult = new List<Class_TBZPROGNAION>();
            MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBZPROGNATION", sqlParameters, out sqlResult);
            if (sqlResult.Count > 0)
            {
                for (int i = 0; i < sqlResult.Count ; i++)
                {
                    Class_TBZPROGNAION myItem = new Class_TBZPROGNAION();
                    myItem.ID = sqlResult[i]["ID"];
                    myItem.FSNATION_ID = sqlResult[i]["FSNATION_ID"];
                    myItem.FSNATION_NAME = sqlResult[i]["FSNATION_NAME"];
                    myItem.FBISENABLE = sqlResult[i]["FBISENABLE"];
                    myItem.FDCREATE_DATE = sqlResult[i]["FDCREATE_DATE"];
                    myItem.FSCREATE_BY = sqlResult[i]["FSCREATE_BY"];
                    myItem.FDUPDATE_DATE = sqlResult[i]["FDUPDATE_DATE"];
                    myItem.FSUPDATE_BY = sqlResult[i]["FSUPDATE_BY"];
                    myItem.FSSORT = sqlResult[i]["FSSORT"];

                    returnResult.Add(myItem);

                }
            }
            return returnResult;
        }

        /// <summary>
        /// 新增來源國家代碼檔
        /// </summary>
        /// <returns>true:成功 false:失敗</returns>
        [WebMethod]
        public bool I_TBZPROGNATION(Class_TBZPROGNAION iniClass)
        {
            bool rtnResult = false;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSNATION_ID", iniClass.FSNATION_ID);
            sqlParameters.Add("FSNATION_NAME", iniClass.FSNATION_NAME);
            sqlParameters.Add("FBISENABLE", "1");
            sqlParameters.Add("FSCREATE_BY", iniClass.FSCREATE_BY);
            sqlParameters.Add("FSUPDATE_BY", iniClass.FSUPDATE_BY);
            sqlParameters.Add("FSSORT", iniClass.FSSORT);
            if (MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBZPROGNATION", sqlParameters, iniClass.FSCREATE_BY))
            {
                rtnResult = true;
            }
            return rtnResult;
        }

        /// <summary>
        /// 更新國家來源代碼檔
        /// </summary>
        /// <param name="iniClass"></param>
        /// <returns></returns>
        [WebMethod]
        public bool U_TBZPROGNATION(Class_TBZPROGNAION iniClass)
        {
            bool rtnResult = false;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("ID", iniClass.ID);
            sqlParameters.Add("FSNATION_ID", iniClass.FSNATION_ID);
            sqlParameters.Add("FSNATION_NAME", iniClass.FSNATION_NAME);
            sqlParameters.Add("FBISENABLE", iniClass.FBISENABLE);
            sqlParameters.Add("FSUPDATE_BY", iniClass.FSUPDATE_BY);
            sqlParameters.Add("FSSORT", iniClass.FSSORT);
            if (MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBZPROGNATION", sqlParameters, iniClass.FSUPDATE_BY))
            {
                rtnResult = true;
            }
            return rtnResult;
        }

        /// <summary>
        /// 刪除國家來源代碼檔
        /// </summary>
        /// <param name="iniClass"></param>
        /// <returns></returns>
        [WebMethod]
        public bool D_TBZPROGNATION(Class_TBZPROGNAION iniClass,string DeleteBy)
        {
            bool rtnResult = false;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("ID", iniClass.ID);
            if (MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_TBZPROGNATION", sqlParameters, DeleteBy))
            {
                rtnResult = true;
            }
            return rtnResult;
        }
    }
}
