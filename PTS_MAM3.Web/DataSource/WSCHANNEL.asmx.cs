﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.IO;
using System.Data.OleDb;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using System.Collections.ObjectModel;
using System.Web.Configuration;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSCHANNEL 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSCHANNEL : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello Dennis";
        }

        [WebMethod]
        public ObservableCollection<PTS_MAM3.Web.Class.Class_CHANNEL> GetChannelList()
        {
            ObservableCollection<PTS_MAM3.Web.Class.Class_CHANNEL> ChannelList = new ObservableCollection<PTS_MAM3.Web.Class.Class_CHANNEL>();
            try
            {
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;
                    cmd.CommandText = "SELECT [FSCHANNEL_ID], [FSCHANNEL_NAME] ,[FSSHOWNAME] ,[FSCHANNEL_TYPE] ,[FSSORT] ,[FSSHORTNAME] ,[FSOLDTABLENAME] ,[FSTSM_POOLNAME] ,[FSCREATED_BY] ,[FDCREATED_DATE] ,[FSUPDATED_BY] ,[FDUPDATED_DATE] FROM [TBZCHANNEL] ";
                    cmd.CommandText += "ORDER BY FSCHANNEL_ID";

                    cn.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                ChannelList.Add
                                (
                                    new PTS_MAM3.Web.Class.Class_CHANNEL
                                    {
                                        FSCHANNEL_ID= dr["FSCHANNEL_ID"].ToString(),
                                        FSCHANNEL_NAME= dr["FSCHANNEL_NAME"].ToString(),
                                        FSSHOWNAME= dr["FSSHOWNAME"].ToString(),
                                        FSCHANNEL_TYPE= dr["FSCHANNEL_TYPE"].ToString(),
                                        FSSORT= dr["FSSORT"].ToString(),
                                        FSSHORTNAME= dr["FSSHORTNAME"].ToString(),
                                        FSOLDTABLENAME= dr["FSOLDTABLENAME"].ToString(),
                                        FSTSM_POOLNAME= dr["FSTSM_POOLNAME"].ToString(),
                                        FSCREATED_BY= dr["FSCREATED_BY"].ToString(),
                                        //FDCREATED_DATE= DateTime.Parse( dr["FDCREATED_DATE"].ToString()),
                                        FSUPDATED_BY= dr["FSUPDATED_BY"].ToString()//,
                                        //FDUPDATED_DATE = DateTime.Parse(dr["FDUPDATED_DATE"].ToString())
                                    }
                                );
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                return null;
            }

            return ChannelList;
        }


    }
}
