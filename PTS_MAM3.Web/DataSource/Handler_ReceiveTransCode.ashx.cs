﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Xml;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    //接收轉檔中心轉檔完成的XML
    /// </summary>
    public class Handler_ReceiveTransCode : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                long JobID;
                string transcodeJobStatus = string.Empty;
                string strFileConvertSize = string.Empty;
                string strFileNO = string.Empty;
                string strHiPath = string.Empty;
                string strLowPath = string.Empty;
                string strHiType = string.Empty;
                string strLowType = string.Empty;
                string strDestinationHi = string.Empty;
                string strDestinationLo = string.Empty;
                string strType = string.Empty;
                string strVideoID = string.Empty;
                string strARC_TYPE = string.Empty;

               

                HttpRequest req = context.Request;

                // 解析從 ANS 傳來的內容，如果回傳 False 代表 HTTP POST 的內容有誤，無法判斷是哪個 ANSID，也因此不用改狀態
                WS.WSASC.ServiceTranscode objTranscode = new WS.WSASC.ServiceTranscode();
                bool flagParse = objTranscode.ParseTranscodeJobHttpMessage(req.InputStream, out JobID, out transcodeJobStatus);
                objTranscode.Dispose();

                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/Handler_ReceiverTransCode/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "Recv--JobID:" + JobID);

                // 因後續動作時間很長，在這邊先將對方的 Handler 關掉，以避免逾時錯誤的問題
                context.Response.Flush();
                context.Response.Close();

                if (!flagParse)
                {
                    MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/Handler_ReceiverTransCode/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "解析HttpContext內容錯誤 JobID：" + JobID);
                    return;
                }

                // JobID小於零表示資料異常，寫LOG並且跳出
                if (JobID < 0)
                {
                    MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/Handler_ReceiverTransCode/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "解析HttpContext內容 FAIL JobID小於零 JobID：" + JobID);
                    return;
                }

                MAM_PTS_DLL.SysConfig objSysConfig = new MAM_PTS_DLL.SysConfig();
                WSBROADCAST objWSBROADCAST = new WSBROADCAST();
                WS.WSTSM.ServiceTSM objTsm = new WS.WSTSM.ServiceTSM();

                // 檢查由 Trasncdoe 服務回傳來的狀態值
                if (transcodeJobStatus != WS.WSASC.ServiceTranscode.TRANSCODE_STATUS.FINISH.ToString())
                {
                    MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/Handler_ReceiverTransCode/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "轉檔任務錯誤，編號：" + JobID);
                    objWSBROADCAST.UPDATE_TBLOG_VIDEO_STT_FAIL(JobID.ToString(), "System");    // 因為轉檔後的更新是系統，所以給System
                    return;
                }

                // 透過JobID取得檔案編號、高解的路徑、低解的路徑
                if (!objWSBROADCAST.GetTBLOG_VIDEO_PATH_BYJOBID(JobID.ToString(), out strFileNO, out strHiPath, out strLowPath, out strHiType, out strLowType, out strType, out strVideoID, out strARC_TYPE))
                {
                    MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/Handler_ReceiverTransCode/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "執行網路服務(WSBROADCAST.GetTBLOG_VIDEO_PATH_BYJOBID)時發生錯誤，轉檔編號：" + JobID);
                    objWSBROADCAST.UPDATE_TBLOG_VIDEO_STT_FAIL(JobID.ToString(), "System");    // 因為轉檔後的更新是系統，所以給System
                    return;
                }

                // 
                string strMetasanUncRootPath = objSysConfig.sysConfig_Read("/ServerConfig/STT_Config/INGEST_TEMP_DIR_UNC");      // MetaSAN 的 UNC 位置
                string strMetasanLinuxRootPath = objSysConfig.sysConfig_Read("/ServerConfig/STT_Config/INGEST_TEMP_DIR_LINUX");  // MetaSAN 的 LINUX 位置

                // 高解檔在光纖硬碟的 Linux 路徑，範例 sysConfig_Read (/INGEST_TEMP_DIR_LINUX) + Path.GetFileName(PATH_HI)
                string strLINUXPathHi = strMetasanLinuxRootPath + strFileNO + "." + strHiType;

                // 轉換目的檔 Samba 路徑至 Linux 路徑
                strDestinationHi = objTsm.GetLinuxPathBySambaPath(strHiPath);

                // 暫時的Log
                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/Handler_ReceiverTransCode/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "高解的LINUXPath：" + strLINUXPathHi + "目的地：" + strDestinationHi + "JobID：" + JobID);

                // DEBUG
                string tmpMetaSanHiPath = string.Concat(strMetasanUncRootPath, strFileNO, ".", strHiType);
                long fileSize1 = -1;
                if (System.IO.File.Exists(tmpMetaSanHiPath))
                {
                    try
                    {
                        FileInfo objFileInfo = new FileInfo(tmpMetaSanHiPath);
                        fileSize1 = objFileInfo.Length;
                        MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/Handler_ReceiverTransCode/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, string.Format("BeforeMove;tmpMetaSanHiPath=" + tmpMetaSanHiPath + " ;FileNO={0}, Size={1} JobID:{2}", strFileNO, objFileInfo.Length, JobID));
                    }
                    catch
                    {
                        //
                        fileSize1 = -1;
                    }
                }

                // 呼叫 API 將高解檔從 18TB 搬到 GPFS，inputParameters: 1, 2
                // returnValue: true or false
                // the function will auto-log detail error message
                if (!fnMoveFileToGPFS(strLINUXPathHi, strDestinationHi, strHiPath))
                {
                    // 在 fnMoveFileToGPFS 已經做好充足的記錄，這邊不用再添一筆了

                    // 搬檔失敗時，在TBLOG_VIDEO上需標記錯誤
                    objWSBROADCAST.UPDATE_TBLOG_VIDEO_STT_FAIL(JobID.ToString(), "System");
                    return;
                }

                // 直接利用 File.Copy 將低解檔從 18TB 搬到 GPFS
                string sourceLowResFilePathUnc = string.Concat(strMetasanUncRootPath, strFileNO, ".", strLowType);
                if (!fnMoveLowResFile(sourceLowResFilePathUnc, strLowPath))
                {
                    // Log By fnMoveLowResFile(x, x);
                    objWSBROADCAST.UPDATE_TBLOG_VIDEO_STT_FAIL(JobID.ToString(), "System");
                    return;
                }

                // 如果是 SD 的 Promo 時，就檢查看看可不可以搬 MSSVideo
                if (strType == "P" && strARC_TYPE == "001" && objSysConfig.sysConfig_Read("/ServerConfig/STT_Config/PromoIngestMssVideo") == "Y")
                {
                    string sourceDirPath = string.Concat(objSysConfig.sysConfig_Read("/ServerConfig/STT_Config/INGEST_TEMP_DIR_UNC"), strFileNO) + "\\";
                    string destDirPath = objTsm.GetSambaDirPathByVideoID(strVideoID) + "\\";
                    // 不需要攔截錯誤 (因為他不是正式的送帶轉檔流程)
                    fnMoveMssFiles(sourceDirPath, destDirPath, strFileNO);
                }

                // DEBUG AGAIN
                long fileSize2 = -1;
                if (System.IO.File.Exists(tmpMetaSanHiPath))
                {
                    try
                    {
                        FileInfo objFileInfo = new FileInfo(tmpMetaSanHiPath);
                        fileSize2 = objFileInfo.Length;
                        MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/Handler_ReceiverTransCode/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, string.Format("AfterMove;tmpMetaSanHiPath=" + tmpMetaSanHiPath + " ;FileNO={0}, Size={1},JobID:{2}", strFileNO, objFileInfo.Length, JobID));
                    }
                    catch
                    {
                        //
                        fileSize2 = -1;
                    }
                }

                // DEBUG!!
                long fileSize3 = -1;
                if (System.IO.File.Exists(strHiPath))
                {
                    try
                    {
                        FileInfo objFileInfo = new FileInfo(strHiPath);
                        fileSize3 = objFileInfo.Length;
                        MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/Handler_ReceiverTransCode/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, string.Format("AfterMove;strHiPath=" + strHiPath + " ;FileNO={0}, Size={1},JobID:{2}", strFileNO, objFileInfo.Length, JobID));
                    }
                    catch
                    {
                        //
                        fileSize3 = -1;
                    }
                }

                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/Handler_ReceiverTransCode/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "開始檢查檔案大小,fileSize1：" + fileSize1.ToString() + ";fileSize2:" + fileSize2.ToString() + ";fileSize3:" + fileSize3+" JobID:"+JobID);

                if (fileSize1 > 0 && fileSize2 > 0 && fileSize3 > 0)
                {
                    if (fileSize1 != fileSize2)
                    {
                        // T 槽檔案不應持續成長！判為 FAIL
                        MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/Handler_ReceiverTransCode/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "T 槽檔案不應持續成長！判為 FAIL -- JobID：" + JobID);
                        objWSBROADCAST.UPDATE_TBLOG_VIDEO_STT_FAIL(JobID.ToString(), "System");
                        return;
                    }
                    if (fileSize1 != fileSize3)
                    {
                        // 檔案應該要一樣大才對!
                        MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/Handler_ReceiverTransCode/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "T 槽檔案與 GPFS 檔案大小不同！判為 FAIL -- JobID：" + JobID);
                        objWSBROADCAST.UPDATE_TBLOG_VIDEO_STT_FAIL(JobID.ToString(), "System");
                        return;
                    }
                }

                // 查詢高解檔的檔案大小
                if (!string.IsNullOrEmpty(strHiPath.Trim()))
                {
                    strFileConvertSize = fnCheckFileSize(strHiPath); // 檔案從位元組轉換成KB、MB或GB
                    MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/Handler_ReceiverTransCode/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "查詢高解檔的檔案大小,strFileConvertSize：" + strFileConvertSize+" JobID:"+JobID);
                }
                else
                {
                    MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/Handler_ReceiverTransCode/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "查詢高解檔的檔案大小發生錯誤,strHiPath為空白 JobID:"+JobID);

                    return;
                }
                // 更新資料庫-1
                // 因為轉檔後的更新是系統，所以給System
                if (!objWSBROADCAST.UPDATE_TBLOG_VIDEO_STT_OK(JobID.ToString(), strFileConvertSize, fnGetMediaFileProperty(strHiPath), "System"))
                {
                    MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/Handler_ReceiverTransCode/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "執行UPDATE_TBLOG_VIDEO_STT_OK失敗,JobID：" + JobID.ToString() + ";FileNO:" + strFileNO);
                    return;
                }
                // 更新資料庫-2
                if (!objWSBROADCAST.GetTBLOG_VIDEO_FILENO_INSERT_TBLOG_VIDEO_D(JobID.ToString()))
                {
                    MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/Handler_ReceiverTransCode/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "執行GetTBLOG_VIDEO_FILENO_INSERT_TBLOG_VIDEO_D失敗,JobID：" + JobID.ToString() + ";FileNO:" + strFileNO);
                    return;
                }

                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/Handler_ReceiveTransCode/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "已完成檔案編號：" + strFileNO + " 所有轉檔完成處理程序");
            }
            catch (Exception ex)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/Handler_ReceiverTransCode/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "非預期中的Excetption：" + ex.ToString());

            }
        }

        // 低解檔搬檔 (直接利用 File.Copy 的方式)
        private bool fnMoveLowResFile(string sourceFile, string targetFile)
        {
            bool flag = false;
            for (int i = 0; i < 10; i++)
            {
                try
                {
                    if (File.Exists(targetFile))
                        File.Delete(targetFile);

                    File.Copy(sourceFile, targetFile, true);
                    flag = true;
                    break;

                }
                catch (Exception ex)
                {
                    if (i < 9)//9次內睡5秒進行下一次
                    {
                        System.Threading.Thread.Sleep(5000);
                        flag = false;
                        continue;
                    }
                    else//第10次還失敗就放棄，寫log
                    {
                        MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/Handler_ReceiverTransCode/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Format("低解搬檔時發生錯誤：From {0} To {1}，錯誤：{2}", sourceFile, targetFile, ex.ToString()));
                        return false;
                    }
                }
            }

            return flag;
        }

       

        // 利用 plink 呼叫HSM，將高低解檔搬到相對應的位置，回傳值代表是否有正確搬檔
        private bool fnMoveFileToGPFS(string srcLinuxHi, string destLinuxHi, string destSambaHi)
        {
            string stringStdErr = string.Empty;
            string stringStdOut = string.Empty;
            string testFilePath = string.Empty;
            string scriptFilePath = string.Empty;
            string executeCommand = string.Empty;

            //
            MAM_PTS_DLL.Log.AppendTrackingLog("Handler_ReceiverTransCode/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, string.Concat("開始進行高解搬檔：", srcLinuxHi));

            // Read Connection Method from SysConfig
            var objSysConfig = new MAM_PTS_DLL.SysConfig();
            var connectionStrings = default(List<string>);
            try
            {
                connectionStrings = new List<string>(objSysConfig.sysConfig_Read("/ServerConfig/STT_Config/RSH_CONNECTION").Split(';'));

                // Read testFilePath from SysConfig
                testFilePath = objSysConfig.sysConfig_Read("/ServerConfig/STT_Config/METASAN_CHECK_FILE");

                // Read ScriptFilePath froom SysConfig
                scriptFilePath = objSysConfig.sysConfig_Read("/ServerConfig/STT_Config/COMMAND_SCRIPT");

                // Combine executeCommand
                executeCommand = string.Format("{0} {1} {2} {3}", scriptFilePath, testFilePath, srcLinuxHi, destLinuxHi);

                // 如果發生錯誤的話，就輪詢吧
                for (int i = 0; i < connectionStrings.Count; i++)
                {
                    var argu = string.Format("{0} \"{1}\"", connectionStrings[i], executeCommand);
                    var objProcess = new System.Diagnostics.Process();
                    objProcess.StartInfo.FileName = "D:\\0selfsys\\plink.exe";
                    objProcess.StartInfo.Arguments = argu;
                    objProcess.StartInfo.UseShellExecute = false;
                    objProcess.StartInfo.CreateNoWindow = true;
                    objProcess.StartInfo.RedirectStandardInput = true;
                    objProcess.StartInfo.RedirectStandardOutput = true;
                    objProcess.StartInfo.RedirectStandardError = true;

                    objProcess.Start();
                    stringStdOut = objProcess.StandardOutput.ReadToEnd();
                    stringStdErr = objProcess.StandardError.ReadToEnd();
                    objProcess.Close();
                    objProcess.Dispose();

                    if (!string.IsNullOrEmpty(stringStdErr))
                    {
                        // 搬的時候就已知有問題了
                        MAM_PTS_DLL.Log.AppendTrackingLog("Handler_ReceiverTransCode/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Format("高解搬檔失敗：{0}/{1}/{2}", srcLinuxHi, argu, stringStdErr));
                        // 輪詢，再搬
                    }
                    else
                    {
                        // 搬的時候沒看到問題，就檢查檔案是否存在
                        if (File.Exists(destSambaHi))
                        {
                            MAM_PTS_DLL.Log.AppendTrackingLog("Handler_ReceiverTransCode/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, string.Concat("高解搬檔完成：", destSambaHi));
                            return true;
                        }
                        else
                        {
                            MAM_PTS_DLL.Log.AppendTrackingLog("Handler_ReceiverTransCode/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Format("高解搬檔無錯誤訊息，但找不到目標檔案：{0}/{1}", srcLinuxHi, argu));
                            // 輪詢，再搬
                        }
                    }
                }

                // 輪詢完了，通通沒有
            }
            catch (Exception ex)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("Handler_ReceiverTransCode/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Format("高解搬檔遇未預期錯誤：{0}/{1}", srcLinuxHi, ex.Message));
            }

            // 所有 Server 都問過，而且都發生錯誤訊息了，放棄吧
            return false;
        }

        // 搬移主控播出檔 (直接利用 File.Copy 的方式)
        private bool fnMoveMssFiles(string sourceDir, string targetDir, string fsFileNo)
        {
            try
            {
                if (Directory.Exists(sourceDir))
                {
                    // 
                    if (Directory.Exists(targetDir))
                        Directory.Delete(targetDir, true);
                    // 
                    Directory.CreateDirectory(targetDir);
                    // 
                    File.Copy(sourceDir + "header", targetDir + "header");
                    File.Copy(sourceDir + "ft", targetDir + "ft");
                    File.Copy(sourceDir + "std", targetDir + "std");
                    // 
                    long totalFileSize = 0;
                    FileInfo objFileInfo = new FileInfo(targetDir + "header");
                    totalFileSize += objFileInfo.Length;
                    objFileInfo = new FileInfo(targetDir + "ft");
                    totalFileSize += objFileInfo.Length;
                    objFileInfo = new FileInfo(targetDir + "std");
                    totalFileSize += objFileInfo.Length;
                    // 更新資料庫
                    fnUpdateSegmentStatus(fsFileNo, "01", "Y", totalFileSize);
                    // 
                    Directory.Delete(sourceDir, true);
                }
                else
                    throw new Exception("/ServerConfig/STT_Config/PromoIngestMssVideo 值為 Y，但無法找到 Ingest 的 MSSVideo 目錄: " + sourceDir);

                return true;
            }
            catch (Exception ex)
            {
                // 因這邊不會實際影響到正常作業流程，所以僅記錄即可
                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/Handler_ReceiverTransCode/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "搬移主控播出檔時發生錯誤：" + ex.ToString());
                return false;
            }
        }

        // 將 TBLOG_VIDEO_SEG 的某一個段落狀態值做修改
        private bool fnUpdateSegmentStatus(string fileNo, string segNo, string status, long fileSize)
        {
            /*
                ALTER Proc [dbo].[SP_U_VIDEOSEG_STATUS]

                @FSFILE_NO		CHAR(16),
                @FNSEG_ID		CHAR(2),
                @FCLOW_RES		CHAR(1),
                @FNFILE_SIZE	BIGINT
             */
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSFILE_NO", fileNo);
            sqlParameters.Add("FNSEG_ID", segNo);
            sqlParameters.Add("FCLOW_RES", status);
            sqlParameters.Add("FNFILE_SIZE", fileSize.ToString());
            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_VIDEOSEG_STATUS_PROMO", sqlParameters, "system");
        }

        // 從檔案位置找到檔案大小
        private string fnCheckFileSize(string strPath)
        {
            try
            {
                FileInfo f = new FileInfo(strPath);
                string strFileLength = f.Length.ToString();
                string strFileConvertSize = string.Empty;

                if (!string.IsNullOrEmpty(strFileLength.Trim())) //檔案從位元組轉換成KB、MB或GB
                    strFileConvertSize = MAMFunctions.CheckFileSize(strFileLength);

                return strFileConvertSize;
            }
            catch
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("get File Size fail：" + strPath, MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "RECEIVE FINAL DATA");
                return string.Empty;
            }
        }

        // 取得媒體檔案的媒體相關資訊，請注意執行檔要放在 D:\0selfsys\MediaInfoCLI.exe
        private string fnGetMediaFileProperty(string filePath)
        {
            string result = string.Empty;
            MAM_PTS_DLL.SysConfig objSysConfig = new MAM_PTS_DLL.SysConfig();
            System.Diagnostics.Process myProcess = new System.Diagnostics.Process();
            //myProcess.StartInfo.FileName = @"D:\0selfsys\MediaInfoCLI.exe";   //改成呼叫系統設定檔
            myProcess.StartInfo.FileName = objSysConfig.sysConfig_Read("/ServerConfig/TOOLS_CONFIG/Tool_MediaInfo_EXE_Path");
            myProcess.StartInfo.Arguments = string.Concat("--Output=XML ", filePath);
            myProcess.StartInfo.UseShellExecute = false;
            myProcess.StartInfo.CreateNoWindow = true;
            myProcess.StartInfo.RedirectStandardInput = true;
            myProcess.StartInfo.RedirectStandardOutput = true;
            myProcess.StartInfo.RedirectStandardError = true;

            try
            {
                myProcess.Start();
                result = myProcess.StandardOutput.ReadToEnd();
            }
            catch (Exception ex)
            {
                string errMsg = string.Concat("Got Error file = ", filePath, ", errmsg = ", ex.ToString());
                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/Handler_ReceiverTransCode/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
            }
            finally
            {
                myProcess.Close();
                myProcess.Dispose();
            }

            return result;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
