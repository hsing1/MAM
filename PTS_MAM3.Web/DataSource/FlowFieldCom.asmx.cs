﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.IO;
using System.Xml.Linq;
using PTS_MAM3.Web.Class;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// FlowFieldCom 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class FlowFieldCom : System.Web.Services.WebService
    {
        public FlowWS.Flow client = new FlowWS.Flow();
        [WebMethod]
        public List<Class.DFWaitForCheck_Class> DFWaitYourCheck(string userName)
        {
            string backXml = string.Empty;
            string GetTTL = string.Empty;
            string GetVariableValueString = string.Empty;
            try
            {
                GetTTL = client.GetTTL(userName).Replace("&amp;", "");
            }
            catch (Exception ex)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("ERROE kyle ex.tostring find FlowFieldCom.DFWaitYourCheck", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "ex.ToString() = " + ex.ToString());
            }

            List<Class.DFWaitForCheck_Class> WaitForCheckList = new List<Class.DFWaitForCheck_Class>();
            List<Class.Class_Flow_Agent_Flow> Agent_Flow_List = new List<Class_Flow_Agent_Flow>();
            try
            {
                TextReader txr = new StringReader(GetTTL);
                XDocument doc = XDocument.Load(txr);
                var ltox = from sel in doc.Elements("TTLCollection").Elements("TTL")
                           select sel;
                foreach (XElement elem in ltox)
                {
                    Class.DFWaitForCheck_Class WaitForCheckClass = new Class.DFWaitForCheck_Class();
                    WaitForCheckClass.FormUrl = "";//elem.Element("FormUrl").Value;
                    WaitForCheckClass.Applyname = elem.Element("Applyname").Value;
                    WaitForCheckClass.ReceiveDate = elem.Element("ReceiveDate").Value;
                    WaitForCheckClass.Priority = elem.Element("Priority").Value;
                    WaitForCheckClass.Title = elem.Element("Title").Value;
                    WaitForCheckClass.Ttlid = (elem.Element("Ttlid") == null ? "" : elem.Element("Ttlid").Value);
                    WaitForCheckClass.Version = elem.Element("Version").Value;
                    WaitForCheckClass.HaveAttach = elem.Element("HaveAttach").Value;
                    WaitForCheckClass.Remark = elem.Element("Remark").Value;
                    WaitForCheckClass.Receiveid = elem.Element("Receiveid").Value;
                    WaitForCheckClass.Batch = elem.Element("Batch").Value;
                    WaitForCheckClass.OriginalName = elem.Element("OriginalName").Value;
                    WaitForCheckClass.FlowChartUrl = "";//elem.Element("FlowChartUrl").Value;
                    WaitForCheckClass.flowid = elem.Element("flowid").Value;
                    WaitForCheckClass.OverDueDate = elem.Element("OverDueDate").Value;
                    WaitForCheckClass.Addsign = elem.Element("Addsign").Value;
                    WaitForCheckClass.Nodeid = elem.Element("Nodeid").Value;
                    WaitForCheckClass.Roleid = elem.Element("Roleid").Value;
#region 代理人的時候使用/把相關資料載入表單中
                    if (elem.Element("Ttlid") == null || elem.Element("Ttlid").Value == "")
                    { WaitForCheckClass.FormId = "N/A"; }
                    else
                    {
                        GetVariableValueString = client.GetVariableValue(Convert.ToInt32(elem.Element("Ttlid").Value));
                        TextReader txr1 = new StringReader(GetVariableValueString);
                        XDocument doc1 = XDocument.Load(txr1);
                        var ltox1 = from sel1 in doc1.Elements("VariableCollection").Elements("Variable")
                                    where sel1.Element("name").Value == "FormId"
                                    select sel1;
                        foreach (XElement elem1 in ltox1)
                        {
                            WaitForCheckClass.FormId = elem1.Element("value").Value;
                        }
                        var ltox2 = from sel2 in doc1.Elements("VariableCollection").Elements("Variable")
                                    where sel2.Element("name").Value == "FormInfo"
                                    select sel2;
                        foreach (XElement elem1 in ltox2)
                        {
                            WaitForCheckClass.FormInfo = elem1.Element("value").Value;
                        }
                        var ltox3 = from sel3 in doc1.Elements("VariableCollection").Elements("Variable")
                                    where sel3.Element("name").Value == "FlowResult"
                                    select sel3;
                        foreach (XElement elem1 in ltox3)
                        {
                            WaitForCheckClass.FlowResult = elem1.Element("value").Value;
                        }
                        var ltox4 = from sel4 in doc1.Elements("VariableCollection").Elements("Variable")
                                    where sel4.Element("name").Value == "Function01"
                                    select sel4;
                        foreach (XElement elem1 in ltox4)
                        {
                            WaitForCheckClass.Function01 = elem1.Element("value").Value;
                        }
                        var ltox5 = from sel5 in doc1.Elements("VariableCollection").Elements("Variable")
                                    where sel5.Element("name").Value == "NextXamlName"
                                    select sel5;
                        foreach (XElement elem1 in ltox5)
                        {
                            WaitForCheckClass.NextXamlName = elem1.Element("value").Value;
                        }
                    }
#endregion
                    WaitForCheckList.Add(WaitForCheckClass);
                }
                //SP_Q_GET_Agent_Flow
                Dictionary<string, string> sqlParametersAgent = new Dictionary<string, string>();
                List<Dictionary<string, string>> sqlResultAgent;
                sqlParametersAgent.Add("FSUSER_ID", userName);
                if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_GET_Agent_Flow", sqlParametersAgent, out sqlResultAgent))
                    Agent_Flow_List = new List<Class.Class_Flow_Agent_Flow>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
                if (sqlResultAgent.Count > 0)
                {
                    //取得Agent_Flow TABLE裡面的資料
                    for (int i = 0; i < sqlResultAgent.Count; i++)
                    {
                        Class.Class_Flow_Agent_Flow CFIF = new Class.Class_Flow_Agent_Flow();
                        CFIF.aid = sqlResultAgent[i]["aid"];
                        CFIF.memberid = sqlResultAgent[i]["memberid"];
                        CFIF.MemberFSID = sqlResultAgent[i]["MemberFSID"];
                        CFIF.MemberFSNAME = sqlResultAgent[i]["MemberFSNAME"];
                        CFIF.agentid = sqlResultAgent[i]["agentid"];
                        CFIF.AgentFSID = sqlResultAgent[i]["AgentFSID"];
                        CFIF.AgentFSNAME = sqlResultAgent[i]["AgentFSNAME"];
                        CFIF.SDate = sqlResultAgent[i]["SDate"];
                        CFIF.EDate = sqlResultAgent[i]["EDate"];
                        CFIF.Status = sqlResultAgent[i]["Status"];
                        CFIF.AgreeDate = sqlResultAgent[i]["AgreeDate"];
                        CFIF.SendDate = sqlResultAgent[i]["SendDate"];
                        CFIF.ifopen = sqlResultAgent[i]["ifopen"];
                        Agent_Flow_List.Add(CFIF);
                    }
                    foreach (var item in Agent_Flow_List)
                    {
                        List<Class.Class_Flow_AgentDetail_Flow> AgentDetail_Flow_List = new List<Class_Flow_AgentDetail_Flow>();
                        Dictionary<string, string> sqlParametersDetail = new Dictionary<string, string>();
                        List<Dictionary<string, string>> sqlResultDetail;
                        sqlParametersDetail.Add("aid", item.aid);
                        //取得代理的流程編號
                        if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_GET_AgentDetail_Flow", sqlParametersDetail, out sqlResultDetail))
                            AgentDetail_Flow_List = new List<Class.Class_Flow_AgentDetail_Flow>();
                        if (sqlResultDetail.Count > 0)
                        {
                            for (int i = 0; i < sqlResultDetail.Count; i++)
                            {
                                Class.Class_Flow_AgentDetail_Flow CFAD = new Class_Flow_AgentDetail_Flow();
                                CFAD.aid = sqlResultDetail[i]["aid"];
                                CFAD.AgentDetailid = sqlResultDetail[i]["AgentDetailid"];
                                CFAD.eflowid = sqlResultDetail[i]["eflowid"];
                                AgentDetail_Flow_List.Add(CFAD);
                            }
                            if (AgentDetail_Flow_List.FirstOrDefault().eflowid == "0")
                            {
                                string GetDetailTTL = client.GetTTL(item.MemberFSID).Replace("&amp;", "");
                                TextReader txrDetail = new StringReader(GetDetailTTL);
                                XDocument docDetail = XDocument.Load(txrDetail);
                                var ltoxDetail = from selDetail in docDetail.Elements("TTLCollection").Elements("TTL")
                                                 select selDetail;
                                foreach (XElement elem in ltoxDetail)
                                {
                                    Class.DFWaitForCheck_Class WaitForCheckClass = new Class.DFWaitForCheck_Class();
                                    WaitForCheckClass.FormUrl = "";// elem.Element("FormUrl").Value;
                                    WaitForCheckClass.Applyname = elem.Element("Applyname").Value;
                                    WaitForCheckClass.ReceiveDate = elem.Element("ReceiveDate").Value;
                                    WaitForCheckClass.Priority = elem.Element("Priority").Value;
                                    WaitForCheckClass.Title = elem.Element("Title").Value;
                                    WaitForCheckClass.Ttlid = (elem.Element("Ttlid") == null ? "" : elem.Element("Ttlid").Value);
                                    WaitForCheckClass.Version = elem.Element("Version").Value;
                                    WaitForCheckClass.HaveAttach = elem.Element("HaveAttach").Value;
                                    WaitForCheckClass.Remark = elem.Element("Remark").Value;
                                    WaitForCheckClass.Receiveid = elem.Element("Receiveid").Value;
                                    WaitForCheckClass.Batch = elem.Element("Batch").Value;
                                    WaitForCheckClass.OriginalName = elem.Element("OriginalName").Value;
                                    WaitForCheckClass.FlowChartUrl = "";//elem.Element("FlowChartUrl").Value;
                                    WaitForCheckClass.flowid = elem.Element("flowid").Value;
                                    WaitForCheckClass.OverDueDate = elem.Element("OverDueDate").Value;
                                    WaitForCheckClass.Addsign = elem.Element("Addsign").Value;
                                    WaitForCheckClass.Nodeid = elem.Element("Nodeid").Value;
                                    WaitForCheckClass.Roleid = elem.Element("Roleid").Value;
                                    #region 代理人的時候使用/把相關資料載入表單中
                                    if (elem.Element("Ttlid") == null || elem.Element("Ttlid").Value == "")
                                    { WaitForCheckClass.FormId = "N/A"; }
                                    else
                                    {
                                        GetVariableValueString = client.GetVariableValue(Convert.ToInt32(elem.Element("Ttlid").Value));
                                        TextReader txr1 = new StringReader(GetVariableValueString);
                                        XDocument doc1 = XDocument.Load(txr1);
                                        var ltox1 = from sel1 in doc1.Elements("VariableCollection").Elements("Variable")
                                                    where sel1.Element("name").Value == "FormId"
                                                    select sel1;
                                        foreach (XElement elem1 in ltox1)
                                        {
                                            WaitForCheckClass.FormId = elem1.Element("value").Value;
                                        }
                                        var ltox2 = from sel2 in doc1.Elements("VariableCollection").Elements("Variable")
                                                    where sel2.Element("name").Value == "FormInfo"
                                                    select sel2;
                                        foreach (XElement elem1 in ltox2)
                                        {
                                            WaitForCheckClass.FormInfo = "(代理表單-" + item.MemberFSNAME + ") " + elem1.Element("value").Value;
                                        }
                                        var ltox3 = from sel3 in doc1.Elements("VariableCollection").Elements("Variable")
                                                    where sel3.Element("name").Value == "FlowResult"
                                                    select sel3;
                                        foreach (XElement elem1 in ltox3)
                                        {
                                            WaitForCheckClass.FlowResult = elem1.Element("value").Value;
                                        }
                                        var ltox4 = from sel4 in doc1.Elements("VariableCollection").Elements("Variable")
                                                    where sel4.Element("name").Value == "Function01"
                                                    select sel4;
                                        foreach (XElement elem1 in ltox4)
                                        {
                                            WaitForCheckClass.Function01 = elem1.Element("value").Value;
                                        }
                                        var ltox5 = from sel5 in doc1.Elements("VariableCollection").Elements("Variable")
                                                    where sel5.Element("name").Value == "NextXamlName"
                                                    select sel5;
                                        foreach (XElement elem1 in ltox5)
                                        {
                                            WaitForCheckClass.NextXamlName = elem1.Element("value").Value;
                                        }
                                    }
                                    #endregion
                                    DateTime receiveTime = Convert.ToDateTime(elem.Element("ReceiveDate").Value);
                                    DateTime sDate = Convert.ToDateTime(item.SDate);
                                    DateTime eDate = Convert.ToDateTime(item.EDate);
                                    if (receiveTime > sDate && receiveTime < eDate)
                                    {
                                        WaitForCheckList.Add(WaitForCheckClass);   
                                    }
                                }
                            }
                            else
                            {
                                string GetDetailTTL = client.GetTTL(item.MemberFSID).Replace("&amp;", "");
                                TextReader txrDetail = new StringReader(GetDetailTTL);
                                XDocument docDetail = XDocument.Load(txrDetail);
                                var ltoxDetail = from sel in docDetail.Elements("TTLCollection").Elements("TTL")
                                                 select sel;
                                foreach (XElement elem in ltoxDetail)
                                {
                                    Class.DFWaitForCheck_Class WaitForCheckClass = new Class.DFWaitForCheck_Class();
                                    WaitForCheckClass.FormUrl = elem.Element("FormUrl").Value;
                                    WaitForCheckClass.Applyname = elem.Element("Applyname").Value;
                                    WaitForCheckClass.ReceiveDate = elem.Element("ReceiveDate").Value;
                                    WaitForCheckClass.Priority = elem.Element("Priority").Value;
                                    WaitForCheckClass.Title = elem.Element("Title").Value;
                                    WaitForCheckClass.Ttlid = (elem.Element("Ttlid") == null ? "" : elem.Element("Ttlid").Value);
                                    WaitForCheckClass.Version = elem.Element("Version").Value;
                                    WaitForCheckClass.HaveAttach = elem.Element("HaveAttach").Value;
                                    WaitForCheckClass.Remark = elem.Element("Remark").Value;
                                    WaitForCheckClass.Receiveid = elem.Element("Receiveid").Value;
                                    WaitForCheckClass.Batch = elem.Element("Batch").Value;
                                    WaitForCheckClass.OriginalName = elem.Element("OriginalName").Value;
                                    WaitForCheckClass.FlowChartUrl = elem.Element("FlowChartUrl").Value;
                                    WaitForCheckClass.flowid = elem.Element("flowid").Value;
                                    WaitForCheckClass.OverDueDate = elem.Element("OverDueDate").Value;
                                    WaitForCheckClass.Addsign = elem.Element("Addsign").Value;
                                    WaitForCheckClass.Nodeid = elem.Element("Nodeid").Value;
                                    WaitForCheckClass.Roleid = elem.Element("Roleid").Value;
                                    #region 代理人的時候使用/把相關資料載入表單中
                                    if (elem.Element("Ttlid") == null || elem.Element("Ttlid").Value == "")
                                    { WaitForCheckClass.FormId = "N/A"; }
                                    else
                                    {
                                        GetVariableValueString = client.GetVariableValue(Convert.ToInt32(elem.Element("Ttlid").Value));
                                        TextReader txr1 = new StringReader(GetVariableValueString);
                                        XDocument doc1 = XDocument.Load(txr1);
                                        var ltox1 = from sel1 in doc1.Elements("VariableCollection").Elements("Variable")
                                                    where sel1.Element("name").Value == "FormId"
                                                    select sel1;
                                        foreach (XElement elem1 in ltox1)
                                        {
                                            WaitForCheckClass.FormId = elem1.Element("value").Value;
                                        }
                                        var ltox2 = from sel2 in doc1.Elements("VariableCollection").Elements("Variable")
                                                    where sel2.Element("name").Value == "FormInfo"
                                                    select sel2;
                                        foreach (XElement elem1 in ltox2)
                                        {
                                            WaitForCheckClass.FormInfo = "(代理表單-" + item.MemberFSNAME + ") " + elem1.Element("value").Value;
                                        }
                                        var ltox3 = from sel3 in doc1.Elements("VariableCollection").Elements("Variable")
                                                    where sel3.Element("name").Value == "FlowResult"
                                                    select sel3;
                                        foreach (XElement elem1 in ltox3)
                                        {
                                            WaitForCheckClass.FlowResult = elem1.Element("value").Value;
                                        }
                                        var ltox4 = from sel4 in doc1.Elements("VariableCollection").Elements("Variable")
                                                    where sel4.Element("name").Value == "Function01"
                                                    select sel4;
                                        foreach (XElement elem1 in ltox4)
                                        {
                                            WaitForCheckClass.Function01 = elem1.Element("value").Value;
                                        }
                                        var ltox5 = from sel5 in doc1.Elements("VariableCollection").Elements("Variable")
                                                    where sel5.Element("name").Value == "NextXamlName"
                                                    select sel5;
                                        foreach (XElement elem1 in ltox5)
                                        {
                                            WaitForCheckClass.NextXamlName = elem1.Element("value").Value;
                                        }
                                    }
                                    #endregion
                                    DateTime receiveTime = Convert.ToDateTime(elem.Element("ReceiveDate").Value);
                                    DateTime sDate = Convert.ToDateTime(item.SDate);
                                    DateTime eDate = Convert.ToDateTime(item.EDate);
                                    if (receiveTime > sDate && receiveTime < eDate)
                                    {
                                        foreach (var itemDetail in AgentDetail_Flow_List)
                                        {
                                            if (itemDetail.eflowid == WaitForCheckClass.flowid)
                                                WaitForCheckList.Add(WaitForCheckClass);
                                        }
                                    }
                                }
                            }
                        }
                        
                    }
                }
            }
            catch (Exception ex)
            {
                string errMsg = string.Concat("Got error parsing GetTTL value: ", ex.ToString());
                MAM_PTS_DLL.Log.AppendTrackingLog("ERROR FlowFormFieldcombine", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return new List<DFWaitForCheck_Class>();
            }
            return WaitForCheckList;
        }

        [WebMethod]
        public List<Class_FlowIDfromFormID> GetFlowIDfromFormID(string FormID)
        {
            List<Class_FlowIDfromFormID> returnList = new List<Class_FlowIDfromFormID>();
            //string QueryActivity, GetApprovalList;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FormID", FormID);
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_FlowidFromFormid", sqlParameters, out sqlResult))
                return new List<Class_FlowIDfromFormID>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_FlowIDfromFormID CFIF = new Class_FlowIDfromFormID();
                CFIF.ActiveID = sqlResult[i]["F_ActiveID"];
                CFIF.FormId = sqlResult[i]["FormId"];
                CFIF.displayname = sqlResult[i]["F_displayname"];
                returnList.Add(CFIF);
            }
            return returnList;
        }


         /// <summary>
        /// 查詢流程狀況經由表單單號
        /// </summary>
        /// <param name="dataGrid1SelectedId"></param>
        /// <returns></returns>
        [WebMethod]
        public List<ClassGetApprovalList> GetApprovalListDetailByFormID(string dataGrid1SelectedId)
        {
            int ActiveID;
            if ((GetFlowIDfromFormID(dataGrid1SelectedId)).Count() > 0)
            {
                ActiveID = Convert.ToInt32(((Class_FlowIDfromFormID)(GetFlowIDfromFormID(dataGrid1SelectedId).FirstOrDefault())).ActiveID);
                return GetApprovalListDetail(ActiveID);
            }
            return new List<ClassGetApprovalList>();
        }


        /// <summary>
        /// 用來回傳組合欄位用的
        /// </summary>
        /// <param name="dataGrid1SelectedId"></param>
        /// <returns></returns>
        [WebMethod]
        public List<ClassGetApprovalList> GetApprovalListDetail(int dataGrid1SelectedId)
        {
            List<ClassGetApprovalList> returnList = new List<ClassGetApprovalList>();
            List<ClassAllFlowUser> userList=new List<ClassAllFlowUser>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBUSER_ALL", sqlParameters, out sqlResult))
                return new List<ClassGetApprovalList>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            for(int i=0;i<sqlResult.Count;i++)
            {
                ClassAllFlowUser CAFU=new ClassAllFlowUser();
                CAFU.FSUSER_ID=sqlResult[i]["FSUSER_ID"];
                CAFU.FSUSER=sqlResult[i]["FSUSER"];
                CAFU.FSUSER_ChtName=sqlResult[i]["FSUSER_ChtName"];
                CAFU.FSEMAIL=sqlResult[i]["FSEMAIL"];
                CAFU.FSDep_ChtName=sqlResult[i]["FSDep_ChtName"];
                CAFU.FSGroup_ChtName=sqlResult[i]["FSGroup_ChtName"];
                userList.Add(CAFU);
            }
            
            string BackXml2=client.GetApprovalList(dataGrid1SelectedId);
            TextReader txr2 = new StringReader(BackXml2);
            
            XDocument doc2 = XDocument.Load(txr2);
            var ltox2 = from sel2 in doc2.Elements("APLCollection").Elements("APL")
                        select sel2;
            List<ClassGetApprovalList> GetApprovalList = new List<ClassGetApprovalList>();
            foreach (XElement elem2 in ltox2)
            {
                ClassGetApprovalList GetApprovalListObj = new ClassGetApprovalList();
                //var ltoObj=from sel in userList where userList.Where
                ClassAllFlowUser user = new ClassAllFlowUser();
                user = userList.Where(a => a.FSUSER_ID == elem2.Element("MemberName").Value.Trim()).FirstOrDefault();
                
                if (string.IsNullOrEmpty(user.FSUSER_ChtName))
                { GetApprovalListObj.DF_GetApprovalList_MemberNameField_CombineField = "(" + elem2.Element("MemberName").Value.Trim() + ")"; }
                else
                { GetApprovalListObj.DF_GetApprovalList_MemberNameField_CombineField = "(" + elem2.Element("MemberName").Value.Trim() + ")" + user.FSUSER_ChtName; }
                GetApprovalListObj.DF_GetApprovalList_MemberName = elem2.Element("MemberName").Value;
                GetApprovalListObj.DF_GEtApprovalList_MemberDisplayName = elem2.Element("MemberDisplayName").Value;
                GetApprovalListObj.DF_GEtApprovalList_Enterdate = elem2.Element("Enterdate").Value;
                GetApprovalListObj.DF_GEtApprovalList_Finishdate = elem2.Element("Finishdate").Value;
                GetApprovalListObj.DF_GetApprovalList_NodeName = elem2.Element("NodeName").Value;
                GetApprovalListObj.DF_GetApprovalList_Status = elem2.Element("Status").Value;
                GetApprovalListObj.DF_GetApprovalList_FormUrl = elem2.Element("FormUrl").Value;
                GetApprovalListObj.DF_GetApprovalList_TTLid = elem2.Element("TTLid").Value;
                GetApprovalListObj.DF_GetApprovalList_Nodeid = elem2.Element("Nodeid").Value;
                GetApprovalListObj.DF_GetApprovalList_Comment = elem2.Element("Comment").Value;
                GetApprovalListObj.DF_GetApprovalList_CommentPosition = elem2.Element("CommentPosition").Value;
                GetApprovalList.Add(GetApprovalListObj);
            }

            return GetApprovalList;
        }

        [WebMethod]
        public List<DFWaitToCheck_Main_Class> DFWaitToCheck_Main(string userName, string FlowStatus, string SDate, string EDate, int Flowid, int ProcessType)
        {
            List<DFWaitToCheck_Main_Class> returnList = new List<DFWaitToCheck_Main_Class>();
            string QueryActivity,GetApprovalList;
            QueryActivity = client.QueryActivity(userName, FlowStatus, SDate, EDate, Flowid, 0, 0, ProcessType);
            TextReader txr = new StringReader(QueryActivity);
            XDocument doc = XDocument.Load(txr);
            var ltox = from sel in doc.Elements("ActivityCollection").Elements("Activity")
                       select sel;
            foreach (XElement elem in ltox)
            {
                Class.DFWaitToCheck_Main_Class QueryActivityClass = new Class.DFWaitToCheck_Main_Class();
                QueryActivityClass.DF_QueryActivity_Activeid = elem.Element("activeid").Value;
                QueryActivityClass.DF_QueryActivity_Applyname = elem.Element("Applyname").Value;
                QueryActivityClass.DF_QueryActivity_Displayname = elem.Element("displayname").Value;
                QueryActivityClass.DF_QueryActivity_Enterdate = elem.Element("enterDate").Value;
                QueryActivityClass.DF_QueryActivity_Finishdate = elem.Element("finishDate").Value;
                QueryActivityClass.DF_QueryActivity_Status = elem.Element("status").Value;
                QueryActivityClass.DF_QueryActivity_Vesion = elem.Element("version").Value;
                GetApprovalList = client.GetVariableValue(Convert.ToInt32(QueryActivityClass.DF_QueryActivity_Activeid));
                TextReader txr1 = new StringReader(GetApprovalList);
                XDocument doc1 = XDocument.Load(txr1);
                var ltox1 = from sel1 in doc1.Elements("VariableCollection").Elements("Variable")
                            where sel1.Element("name").Value == "FormId"
                            select sel1;
                foreach (XElement elem1 in ltox1)
                { QueryActivityClass.DF_Form_FormId = elem1.Element("value").Value; }
                QueryActivityClass.DF_hid_SendTo = (doc1.Elements("VariableCollection").Elements("Variable").Where(s => s.Element("name").Value == "hid_SendTo").FirstOrDefault()).Element("value").Value;
                if ((doc1.Elements("VariableCollection").Elements("Variable").Where(s => s.Element("name").Value == "CancelForm").FirstOrDefault()) != null)
                {
                    QueryActivityClass.DF_CancleForm = (doc1.Elements("VariableCollection").Elements("Variable").Where(s => s.Element("name").Value == "CancelForm").FirstOrDefault()).Element("value").Value;
                }
                else
                    QueryActivityClass.DF_CancleForm = "1";
                returnList.Add(QueryActivityClass);
            }
            return returnList;
        }

        [WebMethod]
        public string ReturnIPAddress()
        {
            return System.Web.HttpContext.Current.Request.UserHostAddress;
        }

        [WebMethod]
        public List<Class_BROADCAST> GetTBBROADCAST_BYIDEPISODE_BYALLUSER(string strBRO_TYPE, string strID, string strEPISODE, DateTime dtStart, DateTime dtEnd)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSBRO_TYPE", strBRO_TYPE);
            sqlParameters.Add("FSID", strID);
            sqlParameters.Add("FNEPISODE", strEPISODE);
            sqlParameters.Add("FDSTART", dtStart.ToShortDateString());
            sqlParameters.Add("FDEND", dtEnd.ToShortDateString());

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBBROADCAST_BYIDEPISODE_BYALLUSER", sqlParameters, out sqlResult))
                return new List<Class_BROADCAST>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_BRO(sqlResult);
        }

        //解析資料庫回來的Dictionary，轉存成Class
        private List<Class_BROADCAST> Transfer_BRO(List<Dictionary<string, string>> sqlResult)
        {
            List<Class_BROADCAST> returnData = new List<Class_BROADCAST>();

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_BROADCAST obj = new Class_BROADCAST();
                // --------------------------------    
                obj.FSBRO_ID = sqlResult[i]["FSBRO_ID"];                                    //送帶轉檔單號
                obj.FSBRO_TYPE = sqlResult[i]["FSBRO_TYPE"];                                //類型
                obj.FSBRO_TYPE_NAME = MAMFunctions.CheckType(sqlResult[i]["FSBRO_TYPE"]);   //類型名稱
                obj.FSID = sqlResult[i]["FSID"];                                            //編碼
                obj.FSID_NAME = sqlResult[i]["FSPROG_ID_NAME"];                             //節目或宣傳帶名稱
                obj.FNEPISODE = Convert.ToInt16(sqlResult[i]["FNEPISODE"]);                 //集別
                obj.SHOW_FNEPISODE = MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]); //集別(顯示)
                obj.FNEPISODE_NAME = sqlResult[i]["FNEPISODE_NAME"];                        //集別名稱
                obj.FDBRO_DATE = Convert.ToDateTime(sqlResult[i]["WANT_FDBRO_DATE"]);       //送帶轉檔申請日期
                obj.SHOW_FDBRO_DATE = sqlResult[i]["WANT_FDBRO_DATE"];                      //送帶轉檔申請日期(顯示)
                obj.FSBRO_BY = sqlResult[i]["FSBRO_BY"];                                    //送帶轉檔申請者
                obj.FSBRO_BY_NAME = sqlResult[i]["FSBRO_BY_NAME"];                          //送帶轉檔申請者名稱
                obj.FCCHECK_STATUS = sqlResult[i]["FCCHECK_STATUS"];                        //審核狀態
                obj.FCCHECK_STATUS_NAME = MAMFunctions.CheckStatusReport(sqlResult[i]["FCCHECK_STATUS"]);//狀態名稱
                obj.FSCHECK_BY = sqlResult[i]["FSCHECK_BY"];                                //審核者
                obj.FSCHECK_BY_NAME = sqlResult[i]["FSCHECK_BY_NAME"];                      //審核者名稱
                obj.FCCHANGE = sqlResult[i]["FCCHANGE"];                                    //置換

                obj.FSNOTE = ""; //GetTBLOG_VIDEO_BYBROID(sqlResult[i]["FSBRO_ID"]);        //送帶轉檔內容(此方法會需要許多時間，導致資料下載等待很久，因此先不使用)
                obj.FSCHANNELID = sqlResult[i]["FSCHANNELID"];                              //頻道別
                obj.FSPRINTID = sqlResult[i]["FSPRINTID"].Trim();                           //列印編號
                obj.FSREJECT = sqlResult[i]["FSREJECT"].Trim();                             //片庫駁回原因

                obj.FSCREATED_BY = sqlResult[i]["FSCREATED_BY"];                                        //建檔者
                obj.FSCREATED_BY_NAME = sqlResult[i]["FSCREATED_BY_NAME"];                              //建檔者(顯示)
                obj.FSCREATED_BY_EMAIL = sqlResult[i]["FSCREATED_BY_EMAIL"];                            //建檔者EMAIL
                obj.FDCREATED_DATE = Convert.ToDateTime(sqlResult[i]["WANT_FDCREATEDDATE"]);            //建檔日期
                obj.SHOW_FDCREATED_DATE = MAMFunctions.compareDate(sqlResult[i]["WANT_FDCREATEDDATE"]); //建檔日期(顯示)
                obj.FSUPDATED_BY = sqlResult[i]["FSUPDATED_BY"];                                        //修改者
                obj.FSUPDATED_BY_NAME = sqlResult[i]["FSUPDATED_BY_NAME"];                              //修改者(顯示)
                // --------------------------------
                returnData.Add(obj);
            }

            return returnData;
        }
    }
}
