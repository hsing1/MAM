﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DeltaFlowAPI;
using DeltaFlowCenter;
using FlowWebControls;


namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSFLOWDLL 的摘要描述
    /// Flow_Oc_AddNewAccount:                      新增Flow帳號
    /// Flow_Oc_AddNewAccount_and_AddJobTitleRole:  新增帳號及指定部門與職稱
    /// Flow_Oc_DeleteAccount:                      刪除帳號
    /// Flow_Oc_UpdateAccount:                      修改帳號
    /// FlowBackOffice:                             將後台網址字串拼回來
    /// Flow_Oc_AddJobTitleRole:                    設定帳號部門及職稱
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSFLOWDLL : System.Web.Services.WebService
    {
        
        Oc FlowOc = new Oc();
        /// <summary>
        /// 在Flow中新增一個帳號
        /// </summary>
        /// <param name="strName">USER_ID</param>
        /// <param name="strDisplayName">USERchtName</param>
        /// <param name="strEmail">user Email</param>
        /// <returns>true:成功 false:失敗</returns>
        [WebMethod]
        public bool Flow_Oc_AddNewAccount(string strName,string strDisplayName,string strEmail)
        {
            bool returnResult = false;
            returnResult = FlowOc.AddNewAccount(strName, strDisplayName, "", strEmail);
            if (returnResult)
            { returnResult = Flow_Oc_AddJobTitleRole("MAM外部使用者", strName, "員工", true); }
            return returnResult;
        }
        
        /// <summary>
        /// 新增帳號並且將帳號指派部門及職稱
        /// </summary>
        /// <param name="strName">使用者帳號</param>
        /// <param name="strDisplayName">使用者顯示名稱</param>
        /// <param name="strEmail">Email</param>
        /// <param name="strDeptName">部門名稱</param>
        /// <param name="strJobTitle">職稱</param>
        /// <param name="bolIsMainJobTitle">是否為主要職稱</param>
        /// <returns>true:成功 false:失敗</returns>
        [WebMethod]
        public bool Flow_Oc_AddNewAccount_and_AddJobTitleRole(string strName, string strDisplayName, string strEmail, string strDeptName, string strJobTitle, bool bolIsMainJobTitle)
        {
            bool returnResult = false;
            returnResult = Flow_Oc_AddNewAccount(strName, strDisplayName, strEmail);
            if (returnResult)
            { returnResult = Flow_Oc_AddJobTitleRole(strDeptName, strName, strJobTitle, bolIsMainJobTitle); }
            return returnResult;
        }

        /// <summary>
        /// 在Flow中刪除一個帳號
        /// False：
        /// 1.所要刪除的帳號不存在;
        /// 2.該帳號目前還有扮演職稱角色!請改指定這些職稱角色;"
        /// 3.該帳號目前還有扮演職務角色!請改指定這些職務角色;"
        /// 4.該帳號目前還有待辦事項!!請改派該帳號的待辦事項;"
        /// 5.該帳號是deltaflow系統的最後一個系統管理員!所以不允許刪除!
        /// </summary>
        /// <param name="strAccountName">帳號</param>
        /// <returns>true:成功 false:失敗</returns>
        [WebMethod]
        public bool Flow_Oc_DeleteAccount(string strAccountName)
        {
            bool returnResult = false;
            returnResult = FlowOc.DeleteAccount(strAccountName);
            return returnResult;
        }
        /// <summary>
        /// 修改Flow中的使用者帳號
        /// </summary>
        /// <param name="strAccountName">UserId所指定要更新的帳號(不可為空值)</param>
        /// <param name="strDisplayName">UserChtName新的使用者全名(不可為空值)</param>
        /// <param name="strEmail">新的email</param>
        /// <returns>true成功 false失敗</returns>
        [WebMethod]
        public bool Flow_Oc_UpdateAccount(string strAccountName, string strDisplayName,string strEmail)
        {
            bool returnResult = false;
            returnResult = FlowOc.UpdateAccount(strAccountName, strDisplayName, "", strEmail);
            return returnResult;
        }

        /// <summary>
        /// 用來傳組合進入後台的網址
        /// </summary>
        /// <param name="strAccountName"></param>
        /// <returns></returns>
        [WebMethod]
        public string FlowBackOffice(string strAccountName)
        {
            string returnResult = "";
            string strConfirmcode = "";
            FlowWebService.FlowSoapClient clinet = new FlowWebService.FlowSoapClient();
            strConfirmcode = clinet.GetConfirmCodeForBackSite(strAccountName);
            returnResult = @"http://10.13.220.9/deltaflow/mainlogin.aspx?uid=" + strAccountName + @"&confirmcode=" + strConfirmcode;
            returnResult = Server.UrlEncode(returnResult);
            //returnResult = strConfirmcode;
            return returnResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strDeptName">指定新職稱角色所屬的部門名稱(不可為空值)</param>
        /// <param name="strAccountName">扮演此新職稱角色的人員帳號(不可為空值)</param>
        /// <param name="strJobTitle">此新職稱角色職稱類別(不可為空值)</param>
        /// <param name="bolIsMainJobTitle">決定所新增的職稱角色是否為所傳入帳號的主職稱角色</param>
        /// <returns></returns>
        [WebMethod]
        public bool Flow_Oc_AddJobTitleRole(string strDeptName, string strAccountName, string strJobTitle, bool bolIsMainJobTitle)
        {
            bool returnResult=false;
            returnResult = FlowOc.AddJobTitleRole(strDeptName, strAccountName, strJobTitle, bolIsMainJobTitle);
            return returnResult;
        }

        //[WebMethod]
        //public bool AAA(string strDeptName, string strAccountName, string strJobTitle, bool bolIsMainJobTitle)
        //{
        //    bool returnResult = false;
        //    returnResult = FlowOc.AddJobTitleRole("MAM外部使用者", "sd", "員工", true);
        //    return returnResult;
        //}
        [WebMethod]
        public DeltaFlowAPI.DepartCollection Flow_Oc_GetAllDepart()
        {
            //List<DeltaFlowAPI.Depart> returnList=new List<Depart>();
            //returnList= FlowOc.GetAllDepart();
            return FlowOc.GetAllDepart();
        }
    }
}
