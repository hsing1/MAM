﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Text;
using System.Data.SqlClient;
using System.Web.Configuration;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSMAMFunctions 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public partial class WSMAMFunctions : System.Web.Services.WebService
    {
        /// <summary>
        /// 取號記錄檔_檔案編號
        /// </summary>
        /// <param name="strType">類別</param>
        /// <param name="strID">節目編號或宣傳帶編號</param>
        /// <param name="strEpisode">集別</param>
        /// <param name="strCREATED_BY">取號者</param>
        /// <param name="strChannelID">頻道別</param>
        /// <param name="strProgName">節目:節目名稱;宣傳帶:宣傳帶名稱</param>
        /// <param name="strEpisodeName">節目:節目名稱+集別;宣傳帶:宣傳帶名稱</param>
        /// <returns></returns>
        [WebMethod()]
        public static string GetNoRecord_FILEID(string strType, string strID, string strEpisode, string strCREATED_BY ,string strChannelID,string strProgName,string strEpisodeName )
        {
            string strSUBJECT_ID = "";
            short shortCheck;   //純粹為了判斷是否為數字型態之用
            int   intCheck;
            double dCheck;
            string strYYYY = "";//取DIRID用，取該節目的年度
            Dictionary<string, string> source = new Dictionary<string, string>();
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();

            //判斷送進來的節目、集別或宣傳帶號碼是否型態或長度都正確，正確後才去取號
            if (strType == "G")
            {
                if (int.TryParse(strID.Trim(), out intCheck) == false || strID.Trim().Length != 7)
                    return "節目編號錯誤";

                if (short.TryParse(strEpisode.Trim(), out shortCheck) == false || strEpisode.Trim().Length > 4)
                    return "集別錯誤";

                strSUBJECT_ID = strID.Trim() + strEpisode.Trim().PadLeft(4, '0');                
            }
            else if (strType == "P")
            {
                if (double.TryParse(strID.Trim(), out dCheck) == false || strID.Trim().Length != 11)
                    return "宣傳帶編號錯誤";

                strSUBJECT_ID = strID.Trim();
            }
            else if (strType == "D")
            {
                if (double.TryParse(strID.Trim(), out dCheck) == false || strID.Trim().Length != 11)
                    return "資料帶編號錯誤";

                strSUBJECT_ID = strID.Trim();
            }
            else
            {
                return "編號錯誤";
            }
            source.Add("FSTYPE", strType);
            source.Add("FSSUBJECT_ID", strSUBJECT_ID);
            source.Add("FSCREATED_BY", strCREATED_BY);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_I_TBNORECORD_FILEID", source, out resultData))
                return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (resultData.Count > 0)
            {
                MAM_PTS_DLL.DirAndSubjectAccess Subobj = new MAM_PTS_DLL.DirAndSubjectAccess();  //取得DIR_ID   
                int DIR_ID = 0;

                if (strSUBJECT_ID.Trim() != "" && strSUBJECT_ID.Length == 11)
                    strYYYY = strSUBJECT_ID.Substring(0, 4);
                else
                    strYYYY = DateTime.Now.ToString("yyyy");                

                if (strType == "G")
                {
                    DIR_ID = Subobj.ChkAndCreateDir(strChannelID, strProgName, "G" + strSUBJECT_ID, strEpisodeName, strYYYY);
                    return "G" + resultData[0]["WANT_FILEID"] + ";" + Convert.ToString(DIR_ID);
                }
                else if (strType == "P")
                {
                    DIR_ID = Subobj.ChkAndCreateDir(strChannelID, strProgName, "P" + strSUBJECT_ID, strEpisodeName, strYYYY);
                    return "P" + resultData[0]["WANT_FILEID"] + ";" + Convert.ToString(DIR_ID);
                }
                else if (strType == "D")
                {
                    DIR_ID = Subobj.ChkAndCreateDir(strChannelID, strProgName, "D" + strSUBJECT_ID, strEpisodeName, strYYYY);
                    return "D" + resultData[0]["WANT_FILEID"] + ";" + Convert.ToString(DIR_ID);
                }
                else
                    return "";  
            }
            else
                return "";            
        }


        /// <summary>
        /// 取號記錄檔_檔案編號
        /// </summary>
        /// <param name="strType">類別</param>
        /// <param name="strID">節目編號或宣傳帶編號</param>
        /// <param name="strEpisode">集別</param>
        /// <param name="strCREATED_BY">取號者</param>
        /// <param name="strChannelID">頻道別</param>
        /// <param name="strProgName">節目:節目名稱;宣傳帶:宣傳帶名稱</param>
        /// <param name="strEpisodeName">節目:節目名稱+集別;宣傳帶:宣傳帶名稱</param>
        /// <returns></returns>
        [WebMethod()]
        public string GetNoRecord_FILEID_AP(string strType, string strID, string strEpisode, string strCREATED_BY, string strChannelID, string strProgName, string strEpisodeName)
        {
            string strSUBJECT_ID = "";
            short shortCheck;   //純粹為了判斷是否為數字型態之用
            int intCheck;
            double dCheck;
            string strYYYY = "";//取DIRID用，取該節目的年度
            Dictionary<string, string> source = new Dictionary<string, string>();
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();

            //判斷送進來的節目、集別或宣傳帶號碼是否型態或長度都正確，正確後才去取號
            if (strType == "G")
            {
                if (int.TryParse(strID.Trim(), out intCheck) == false || strID.Trim().Length != 7)
                    return "節目編號錯誤";

                if (short.TryParse(strEpisode.Trim(), out shortCheck) == false || strEpisode.Trim().Length > 4)
                    return "集別錯誤";

                strSUBJECT_ID = strID.Trim() + strEpisode.Trim().PadLeft(4, '0');
            }
            else if (strType == "P")
            {
                if (double.TryParse(strID.Trim(), out dCheck) == false || strID.Trim().Length != 11)
                    return "宣傳帶編號錯誤";

                strSUBJECT_ID = strID.Trim();
            }
            else
            {
                return "編號錯誤";
            }
            source.Add("FSTYPE", strType);
            source.Add("FSSUBJECT_ID", strSUBJECT_ID);
            source.Add("FSCREATED_BY", strCREATED_BY);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_I_TBNORECORD_FILEID", source, out resultData))
                return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (resultData.Count > 0)
            {
                MAM_PTS_DLL.DirAndSubjectAccess Subobj = new MAM_PTS_DLL.DirAndSubjectAccess();  //取得DIR_ID   
                int DIR_ID = 0;

                if (strSUBJECT_ID.Trim() != "" && strSUBJECT_ID.Length == 11)
                    strYYYY = strSUBJECT_ID.Substring(0, 4);
                else
                    strYYYY = DateTime.Now.ToString("yyyy");

                if (strType == "G")
                {
                    DIR_ID = Subobj.ChkAndCreateDir(strChannelID, strProgName, "G" + strSUBJECT_ID, strEpisodeName, strYYYY);
                    return "G" + resultData[0]["WANT_FILEID"] + ";" + Convert.ToString(DIR_ID);
                }
                else
                {
                    DIR_ID = Subobj.ChkAndCreateDir(strChannelID, strProgName, "P" + strSUBJECT_ID, strEpisodeName, strYYYY);
                    return "P" + resultData[0]["WANT_FILEID"] + ";" + Convert.ToString(DIR_ID);
                }
            }
            else
                return "";
        }

        /// <summary>
        /// 取號記錄檔
        /// </summary>
        /// <param name="TYPE"></param>
        /// <param name="NO_DATE"></param>
        /// <param name="NOTE"></param>
        /// <param name="CREATED_BY"></param>
        /// <returns></returns>
        [WebMethod()]
        public  string GetNoRecord(string TYPE, string NO_DATE, string NOTE, string CREATED_BY)
        {
            // SQL Parameters
            Dictionary<string, string> source = new Dictionary<string, string>();
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            source.Add("FSTYPE", TYPE);
            source.Add("FSNO_DATE", DateTime.Now.ToString("yyyyMMdd"));
            source.Add("FSNOTE", NOTE);
            source.Add("FSCREATED_BY", CREATED_BY);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_I_TBNORECORD", source, out resultData))
                return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (resultData.Count > 0)
                return resultData[0]["FSNO"];
            else
                return "";
        }

        /// <summary>
        /// 取得目前主機時間
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public DateTime GetServerDateTime()
        {
            return DateTime.Now;
        }

        /// <summary>
        /// MD5 取值
        /// </summary>
        /// <param name="inputStr"></param>
        /// <returns></returns>
        [WebMethod]
        public string makeMD5Str(string inputStr)
        {
            return MakeMD5Str.MAMFunctions.MakeMD5Str(inputStr);
        }

        [WebMethod]
        public List<string> fnGetValue(string SQLstr)
        {
            List<string> rtnList = new List<string>();

            try
            {
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;
                    cmd.CommandText = SQLstr;

                    cn.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                rtnList.Add(dr[0].ToString());
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                return null;
            }

            return rtnList;
        }

        [WebMethod]
        public string fnExecuteSql(string SQLstr)
        {
            string rtn = string.Empty;

            try
            {
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;
                    cmd.CommandText = SQLstr;

                    cn.Open();

                    cmd.ExecuteNonQuery();


                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }

            return rtn;
        }
    }
}
