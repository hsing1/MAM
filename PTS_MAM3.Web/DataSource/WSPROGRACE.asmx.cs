﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml;
using System.Text;
using System.Xml.Linq;
using System.IO;
using PTS_MAM3.Web.Class;
//using Excel = Microsoft.Office.Interop.Excel;
//using System.Reflection;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSPROGRACE 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSPROGRACE : System.Web.Services.WebService
    {
        List<Class_CODE> m_CodeData = new List<Class_CODE>();       //代碼檔集合

        //參展得獎檔的所有代碼檔 //原本參展狀態是用代碼檔，後來改成只有三種：入選、入圍、得獎
        [WebMethod()]
        public List<Class_CODE> fnGetTBPROGRACE_CODE()
        {
            m_CodeData = MAMFunctions.Transfer_Class_CODE("SP_Q_TBPROGRACE_CODE");
            return m_CodeData;
        }

        //參展得獎檔的所有資料
        [WebMethod]
        public List<Class_PROGRACE> fnGetTBPROGRACE_ALL()
        {            
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROGRACE_ALL", sqlParameters, out sqlResult))
                return new List<Class_PROGRACE>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PROGRACE(sqlResult);            
        }

        //解析資料庫回來的Dictionary，轉存成Class
        private List<Class_PROGRACE> Transfer_PROGRACE (List<Dictionary<string, string>> sqlResult)
        {
            int intCheck;                                                                   //純粹為了判斷是否為數字型態之用(integer)
            short shortCheck;                                                               //純粹為了判斷是否為數字型態之用(smallint)
            DateTime dtCheck;                                                               //純粹為了判斷是否為日期型態之用(DateTime) 

            List<Class_PROGRACE> returnData = new List<Class_PROGRACE>();
            m_CodeData = MAMFunctions.Transfer_Class_CODE("SP_Q_TBPROGRACE_CODE");

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_PROGRACE obj = new Class_PROGRACE();
                // --------------------------------    
                obj.AUTO = sqlResult[i]["AUTO"];                                      //主Key         
                obj.FSPROG_ID = sqlResult[i]["FSPROG_ID"];                           //節目編號
                obj.FSPROG_ID_NAME = sqlResult[i]["FSPROG_ID_NAME"];                 //節目名稱

                obj.FNEPISODE = Convert.ToInt16(sqlResult[i]["FNEPISODE"]);          //集別
                obj.SHOW_FNEPISODE = MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]);//集別(顯示)
                obj.FNEPISODE_NAME = sqlResult[i]["FNEPISODE_NAME"];                 //子集名稱
                obj.FSAREA = sqlResult[i]["FSAREA"];                                 //參展區域
                obj.FSAREA_NAME = compareCode_Code(sqlResult[i]["FSAREA"], "TBZPROGRACE_AREA");//參展區域名稱                
                obj.FSPROGRACE = sqlResult[i]["FSPROGRACE"];                         //參展名稱

                if (DateTime.TryParse(sqlResult[i]["WANT_FDRACEDATE"], out dtCheck) == false)
                {
                    obj.FDRACEDATE = Convert.ToDateTime("1900/1/1");
                    obj.SHOW_FDRACEDATE = "";
                }                    
                else
                {
                    obj.FDRACEDATE = Convert.ToDateTime(sqlResult[i]["WANT_FDRACEDATE"]);   //參展日期
                    obj.SHOW_FDRACEDATE = MAMFunctions.compareDate(sqlResult[i]["WANT_FDRACEDATE"]);  //參展日期(顯示)
                }
                   
                obj.FSPROGSTATUS = sqlResult[i]["FSPROGSTATUS"];                            //參展狀態代碼               
                obj.FNPERIOD = Convert.ToInt16(sqlResult[i]["FNPERIOD"]);                   //屆數
                obj.SHOW_FNPERIOD = MAMFunctions.compareNumber((sqlResult[i]["FNPERIOD"])); //屆數(顯示)
                obj.FSPROGWIN = sqlResult[i]["FSPROGWIN"];                                  //得獎名稱      
                obj.FSPROGNAME = sqlResult[i]["FSPROGNAME"];                                //參展節目名稱
                obj.FSMEMO = sqlResult[i]["FSMEMO"];                                        //備註 

                if (sqlResult[i]["FSPROGSRCID"] == null)                                    //節目來源及名稱
                {
                    obj.FSPROGSRCID = "";
                    obj.FSPROGSRC_NAME = "";
                }
                else
                {
                    obj.FSPROGSRCID = sqlResult[i]["FSPROGSRCID"];
                    obj.FSPROGSRC_NAME = compareCode_Code(sqlResult[i]["FSPROGSRCID"], "TBZPROGSRC"); 
                }

                if (sqlResult[i]["FSCHANNEL_ID"] == null)                                    //頻道及頻道名稱
                {
                    obj.FSCHANNEL_ID = "";
                    obj.FSCHANNEL_ID_NAME = "";
                }
                else
                {
                    obj.FSCHANNEL_ID = sqlResult[i]["FSCHANNEL_ID"];
                    obj.FSCHANNEL_ID_NAME = compareCode_Code(sqlResult[i]["FSCHANNEL_ID"], "TBZCHANNEL");
                }
                
                obj.FSCREATED_BY_NAME = sqlResult[i]["FSCREATED_BY_NAME"];                  //建檔者
                obj.FDCREATED_DATE = Convert.ToDateTime(sqlResult[i]["WANT_FDCREATEDDATE"]);//建檔日期
                obj.SHOW_FDCREATED_DATE = MAMFunctions.compareDate(sqlResult[i]["WANT_FDCREATEDDATE"]);  //建檔日期(顯示)
                obj.FSUPDATED_BY_NAME = sqlResult[i]["FSUPDATED_BY_NAME"];                  //修改者
                obj.FDUPDATED_DATE = Convert.ToDateTime(sqlResult[i]["WANT_FDUPDATEDDATE"]);//修改日期
                obj.SHOW_FDUPDATED_DATE = MAMFunctions.compareDate(sqlResult[i]["WANT_FDUPDATEDDATE"]);  //修改日期(顯示)
                // --------------------------------
                returnData.Add(obj);
            }

            return returnData;
        }      

        //參展名稱的所有資料(AutoComplete用)
        [WebMethod]
        public List<string> fnGetRACE()
        {
            List<string> ListRace = new List<string>();               //參展名稱集合
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROGRACE_AUTO_RACE", sqlParameters, out sqlResult))
                return new List<string>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            for (int i = 0; i < sqlResult.Count; i++)
            {
                if (sqlResult[i]["FSPROGRACE"] != null)
                    ListRace.Add(sqlResult[i]["FSPROGRACE"]);
            }
            return ListRace;
        }

        //得獎名稱的所有資料(AutoComplete用)
        [WebMethod]
        public List<string> fnGetWIN()
        {
            List<string> ListWin = new List<string>();               //得獎名稱集合
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROGRACE_AUTO_WIN", sqlParameters, out sqlResult))
                return new List<string>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            for (int i = 0; i < sqlResult.Count; i++)
            {
                if (sqlResult[i]["FSPROGWIN"] != null)
                    ListWin.Add(sqlResult[i]["FSPROGWIN"]);
            }
            return ListWin;
        }
       
        //新增參展得獎檔
        [WebMethod]
        public Boolean fnINSERT_TBPROGRACE(Class_PROGRACE obj)
        {                       
            Dictionary<string, string> source = new Dictionary<string, string>();        
            source.Add("FSPROG_ID", obj.FSPROG_ID);

            if (obj.FNEPISODE==0)
            source.Add("FNEPISODE", "");
            else
            source.Add("FNEPISODE", obj.FNEPISODE.ToString());

            source.Add("FSAREA", obj.FSAREA);
            source.Add("FSPROGRACE", obj.FSPROGRACE);

            if (obj.FDRACEDATE == new DateTime(1900, 1, 1))
                source.Add("FDRACEDATE", "");
            else
                source.Add("FDRACEDATE", obj.FDRACEDATE.ToString("yyyy/MM/dd"));

            source.Add("FSPROGSTATUS", obj.FSPROGSTATUS);

            if (obj.FNPERIOD == 0)
                source.Add("FNPERIOD", "");
            else
                source.Add("FNPERIOD", obj.FNPERIOD.ToString());

            source.Add("FSPROGWIN", obj.FSPROGWIN);
            source.Add("FSPROGNAME", obj.FSPROGNAME);  
            source.Add("FSMEMO", obj.FSMEMO);
            source.Add("FSCREATED_BY", obj.FSCREATED_BY);
            source.Add("FSUPDATED_BY", obj.FSUPDATED_BY);

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBPROGRACE", source ,  obj.FSUPDATED_BY );
        }

        //修改參展得獎檔
        [WebMethod]
        public Boolean fnUPDATE_TBPROGRACE(Class_PROGRACE obj)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("AUTO", obj.AUTO.ToString());
            source.Add("FSPROG_ID", obj.FSPROG_ID);

            if (obj.FNEPISODE == 0)
                source.Add("FNEPISODE", "");
            else
                source.Add("FNEPISODE", obj.FNEPISODE.ToString());

            source.Add("FSAREA", obj.FSAREA);
            source.Add("FSPROGRACE", obj.FSPROGRACE);

            if (obj.FDRACEDATE == new DateTime(1900, 1, 1))
                source.Add("FDRACEDATE", "");
            else
                source.Add("FDRACEDATE", obj.FDRACEDATE.ToString("yyyy/MM/dd"));

            source.Add("FSPROGSTATUS", obj.FSPROGSTATUS);

            if (obj.FNPERIOD == 0)
                source.Add("FNPERIOD", "");
            else
                source.Add("FNPERIOD", obj.FNPERIOD.ToString());

            source.Add("FSPROGWIN", obj.FSPROGWIN);
            source.Add("FSPROGNAME", obj.FSPROGNAME);
            source.Add("FSMEMO", obj.FSMEMO);
            source.Add("FSUPDATED_BY", obj.FSUPDATED_BY);

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBPROGRACE", source, obj.FSUPDATED_BY );
        }

        //查詢參展得獎檔
        [WebMethod]
        public List<Class_PROGRACE> fnQUERY_TBPROGRACE(Class_PROGRACE obj)
        {      
            // SQL Parameters
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();

            if (obj.FSPROG_ID == null)
                sqlParameters.Add("FSPROG_ID", "");
            else
                sqlParameters.Add("FSPROG_ID", obj.FSPROG_ID);

            if (obj.FNEPISODE == 0)
                sqlParameters.Add("FNEPISODE", "");
            else
                sqlParameters.Add("FNEPISODE", obj.FNEPISODE.ToString());

            //要指定區域時，會查詢出該區域，並且要注意FSAREA_NOT_TAIWAN要給空白
            if (obj.FSAREA == null)
                sqlParameters.Add("FSAREA", "");
            else
                sqlParameters.Add("FSAREA", obj.FSAREA);

            //要傳入台灣的代碼，會查詢出非台灣的區域，並且要注意FSAREA要給空白
            if (obj.FSAREA_NOT_TAIWAN == null)
                sqlParameters.Add("FSAREA_NOT_TAIWAN", "");
            else
                sqlParameters.Add("FSAREA_NOT_TAIWAN", obj.FSAREA_NOT_TAIWAN);

            if (obj.FSPROGRACE == null)
                sqlParameters.Add("FSPROGRACE", "");
            else
                sqlParameters.Add("FSPROGRACE", obj.FSPROGRACE);

            if (obj.FDRACEDATE == new DateTime(1900, 1, 1))         //開始日
                sqlParameters.Add("FDRACEDATE", "1900/1/1");
            else if (obj.FDRACEDATE == new DateTime(0001, 1, 1))
                sqlParameters.Add("FDRACEDATE", "");
            else
                sqlParameters.Add("FDRACEDATE", obj.FDRACEDATE.ToString("yyyy/MM/dd"));

            if (obj.FDRACEDATE_END == new DateTime(1900, 1, 1))     //結束日
                sqlParameters.Add("FDRACEDATE_END", DateTime.Now.ToShortDateString());
            else if (obj.FDRACEDATE_END == new DateTime(0001, 1, 1))
                sqlParameters.Add("FDRACEDATE_END", "");
            else
                sqlParameters.Add("FDRACEDATE_END", obj.FDRACEDATE_END.ToString("yyyy/MM/dd"));

            if (obj.FSPROGSTATUS == null)
                sqlParameters.Add("FSPROGSTATUS", "");
            else
                sqlParameters.Add("FSPROGSTATUS", obj.FSPROGSTATUS);

            if (obj.FNPERIOD == 0)
                sqlParameters.Add("FNPERIOD", "");
            else
                sqlParameters.Add("FNPERIOD", obj.FNPERIOD.ToString());

            if (obj.FSPROGWIN == null)
                sqlParameters.Add("FSPROGWIN", "");
            else
                sqlParameters.Add("FSPROGWIN", obj.FSPROGWIN);

            if (obj.FSPROGNAME == null)
                sqlParameters.Add("FSPROGNAME", "");
            else
                sqlParameters.Add("FSPROGNAME", obj.FSPROGNAME);

            if (obj.FSMEMO == null)
                sqlParameters.Add("FSMEMO", "");
            else
                sqlParameters.Add("FSMEMO", obj.FSMEMO);

            if (obj.FSPROGSRCID == null)
                sqlParameters.Add("FSPROGSRCID", "");  
            else
                sqlParameters.Add("FSPROGSRCID", obj.FSPROGSRCID);

            if (obj.FSCHANNEL_ID == null)
                sqlParameters.Add("FSCHANNEL_ID", "");
            else
                sqlParameters.Add("FSCHANNEL_ID", obj.FSCHANNEL_ID);  

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROGRACE_BYCONDITIONS", sqlParameters, out sqlResult))
                return new List<Class_PROGRACE>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PROGRACE(sqlResult);            
        }    

        //查詢參展得獎檔(要去擋同一集、同參展狀態、同參展區域、同屆、同參展名稱、且同得獎名稱)
        [WebMethod]
        public string fnQUERY_TBPROGRACE_CHECK(Class_PROGRACE obj)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPROG_ID", obj.FSPROG_ID);

            if (obj.FNEPISODE == 0)
                sqlParameters.Add("FNEPISODE", "");
            else
                sqlParameters.Add("FNEPISODE", obj.FNEPISODE.ToString());

            sqlParameters.Add("FSAREA", obj.FSAREA);
            sqlParameters.Add("FSPROGRACE", obj.FSPROGRACE);
            sqlParameters.Add("FSPROGSTATUS", obj.FSPROGSTATUS);

            if (obj.FNPERIOD == 0)
                sqlParameters.Add("FNPERIOD", "");
            else
                sqlParameters.Add("FNPERIOD", obj.FNPERIOD.ToString());

            sqlParameters.Add("FSPROGWIN", obj.FSPROGWIN);  

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROGRACE_BYCHECK", sqlParameters, out sqlResult))
                return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if  (sqlResult.Count>0)
                return sqlResult[0]["dbCount"];
            else
                return "";
        }

        //刪除參展得獎檔
        [WebMethod]
        public Boolean fnDELETE_TBPROGRACE(string strAuto , string strUPDATED_BY )
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("AUTO", strAuto);

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_TBPROGRACE", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source), strUPDATED_BY);
        }

        //比對代碼檔   //原本參展狀態是用代碼檔，後來改成只有三種：入選、入圍、得獎
        string compareCode_Code(string strID, string strTable)
        {
            for (int i = 0; i < m_CodeData.Count; i++)
            {
                if (m_CodeData[i].ID.ToString().Trim().Equals(strID) && m_CodeData[i].TABLENAME.ToString().Trim().Equals(strTable))
                {
                    return m_CodeData[i].NAME.ToString().Trim();
                }
            }
            return "";
        }  

        //參展區域的所有代碼檔_透過名稱查詢
        [WebMethod()]
        public List<Class_CODE> GetTBPROGRACE_AREA_CODE_BYNAME(string strName)
        {
            m_CodeData = MAMFunctions.Transfer_Class_CODE("SP_Q_TBZPROGRACE_AREA_BYNAME", strName);
            return m_CodeData;
        }

        //[WebMethod]
        //public string ExportToExcel(string strFile, string strUser, List<Class_PROGRACE> ListobjRace) //寫入Excel
        //{
        //    string strFileName = strFile + "_" + strUser + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls";
        //    string path = HttpContext.Current.Server.MapPath("~/UploadFolder/" + strFileName);
        //    int k = 2;  //要從2開始，因為第一行是標頭
        //    try
        //    {
        //        Excel.Application oXL;
        //        Excel.Workbook oWB;
        //        Excel.Worksheet oSheet;
        //        Excel.Range oRange;

        //        // Start Excel and get Application object.
        //        oXL = new Excel.Application();

        //        // Set some properties
        //        oXL.Visible = true;
        //        oXL.DisplayAlerts = false;

        //        // Get a new workbook.
        //        oWB = oXL.Workbooks.Add(Missing.Value);

        //        // Get the active sheet
        //        oSheet = (Excel.Worksheet)oWB.ActiveSheet;
        //        oSheet.Name = strFile;

        //        oSheet.Cells[1, 1] = "編號";
        //        oSheet.Cells[1, 2] = "節目編號";
        //        oSheet.Cells[1, 3] = "節目名稱";
        //        oSheet.Cells[1, 4] = "子集名稱";
        //        oSheet.Cells[1, 5] = "集別";
        //        oSheet.Cells[1, 6] = "參展狀態";
        //        oSheet.Cells[1, 7] = "參展區域";
        //        oSheet.Cells[1, 8] = "屆數";
        //        oSheet.Cells[1, 9] = "影展名稱";
        //        oSheet.Cells[1, 10] = "入圍或得獎日期";
        //        oSheet.Cells[1, 11] = "獎項名稱";
        //        oSheet.Cells[1, 12] = "參展節目名稱";
        //        oSheet.Cells[1, 13] = "節目來源";
        //        oSheet.Cells[1, 14] = "所屬頻道";

        //        foreach (Class_PROGRACE objRace in ListobjRace)
        //        {
        //            oSheet.Cells[k, 1] = k - 1;

        //            if (objRace.FSPROG_ID.StartsWith("0"))
        //                oSheet.Cells[k, 2] = "'" + objRace.FSPROG_ID;
        //            else
        //                oSheet.Cells[k, 2] =objRace.FSPROG_ID;
                 
        //            oSheet.Cells[k, 3] =objRace.FSPROG_ID_NAME;
        //            oSheet.Cells[k, 4] =objRace.FNEPISODE_NAME;
        //            oSheet.Cells[k, 5] =objRace.FNEPISODE;
        //            oSheet.Cells[k, 6] =objRace.FSPROGSTATUS;
        //            oSheet.Cells[k, 7] =objRace.FSAREA_NAME;

        //            if (objRace.FNPERIOD == 0)
        //                oSheet.Cells[k, 8] = "";
        //            else
        //                oSheet.Cells[k, 8] = objRace.FNPERIOD;
                    
        //            oSheet.Cells[k, 9] =objRace.FSPROGRACE;
        //            oSheet.Cells[k, 10] =objRace.SHOW_FDRACEDATE;
        //            oSheet.Cells[k, 11] =objRace.FSPROGWIN;
        //            oSheet.Cells[k, 12] =objRace.FSPROGNAME;
        //            oSheet.Cells[k, 13] =objRace.FSPROGSRC_NAME;
        //            oSheet.Cells[k, 14] =objRace.FSCHANNEL_ID_NAME;
        //            k++;
        //        }
                
        //        // Save the sheet and close
        //        oSheet = null;
        //        oRange = null;
        //        oWB.SaveAs(path, Excel.XlFileFormat.xlWorkbookNormal,
        //            Missing.Value, Missing.Value, Missing.Value, Missing.Value,
        //            Excel.XlSaveAsAccessMode.xlExclusive,
        //            Missing.Value, Missing.Value, Missing.Value,
        //            Missing.Value, Missing.Value);
        //        oWB.Close(Missing.Value, Missing.Value, Missing.Value);
        //        oWB = null;
        //        oXL.Quit();

        //        // Clean up
        //        // NOTE: When in release mode, this does the trick
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();                

        //        return path;
        //    }
        //    catch (Exception ex)
        //    {
        //        return "Error:" + ex.Message;
        //    }
        //    finally
        //    {
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //    }
        //}

        //[WebMethod]
        //public string ExportToTxt(string strFile, string strUser, List<Class_PROGRACE> ListobjRace) //寫入Excel
        //{
        //    string strFileName = strFile + "_" + strUser + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
        //    string path = HttpContext.Current.Server.MapPath("~/UploadFolder/" + strFileName);
        //    string logDirPath = Path.GetDirectoryName(path);
        //    StringBuilder content = new StringBuilder("");
        //    int intCount = 1;
        //    try
        //    {
        //        if (!Directory.Exists(logDirPath))
        //            Directory.CreateDirectory(logDirPath);

        //        content.Append("編號,");
        //        content.Append("節目編號,");
        //        content.Append("節目名稱,");
        //        content.Append("子集名稱,");
        //        content.Append("集別,");
        //        content.Append("參展狀態,");
        //        content.Append("參展區域,");
        //        content.Append("屆數,");
        //        content.Append("影展名稱,");
        //        content.Append("入圍或得獎日期,");
        //        content.Append("獎項名稱,");
        //        content.Append("參展節目名稱,");
        //        content.Append("節目來源,");
        //        content.Append("所屬頻道,");
        //        content.AppendLine("");

        //        foreach (Class_PROGRACE objRace in ListobjRace)
        //        {                    
        //            content.Append(intCount + ",");

        //            if (objRace.FSPROG_ID.StartsWith("0"))
        //                content.Append("'" + objRace.FSPROG_ID + ",");
        //            else
        //                content.Append(objRace.FSPROG_ID + ",");
                    
        //            content.Append(objRace.FSPROG_ID_NAME.Replace(",", "，") + ",");
        //            content.Append(objRace.FNEPISODE_NAME.Replace(",", "，") + ",");
        //            content.Append(objRace.FNEPISODE + ",");
        //            content.Append(objRace.FSPROGSTATUS.Replace(",", "，") + ",");
        //            content.Append(objRace.FSAREA_NAME.Replace(",", "，") + ",");

        //            if (objRace.FNPERIOD == 0)
        //                content.Append(",");
        //            else
        //                content.Append(objRace.FNPERIOD + ",");
                    
        //            content.Append(objRace.FSPROGRACE.Replace(",", "，").Replace("\n"," ") + ",");
        //            content.Append(objRace.SHOW_FDRACEDATE.Replace(",", "，") + ",");
        //            content.Append(objRace.FSPROGWIN.Replace(",", "，").Replace("\n", " ") + ",");
        //            content.Append(objRace.FSPROGNAME.Replace(",", "，").Replace("\n", " ") + ",");
        //            content.Append(objRace.FSPROGSRC_NAME.Replace(",", "，") + ",");
        //            content.Append(objRace.FSCHANNEL_ID_NAME.Replace(",", "，") + ",");
        //            content.AppendLine("");
        //            intCount++;
        //        }
                
        //        StreamWriter sw = new StreamWriter(path, true, Encoding.UTF8);                
        //        sw.WriteLine(content);
        //        sw.Flush();
        //        sw.Close();
                
        //        return  PTS_MAM3.Web.Properties.Settings.Default.UploadPath+ strFileName;
        //    }
        //    catch (Exception ex)
        //    {
        //        return "Error:" + ex.Message;
        //    }
        //    finally
        //    {
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //    }
        //}

    }
}
