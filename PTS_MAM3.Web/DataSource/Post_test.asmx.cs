﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// Post_test 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class Post_test : System.Web.Services.WebService
    {

       [WebMethod]
       public bool testPost(string pass, string postAddr, string postMsg, int count, int delay)
       {
            if (string.IsNullOrEmpty(pass) || pass != "0987")
                return false;

            for (int i = 0; i < count; i++)
            {
                if (!MAM_PTS_DLL.Protocol.SendHttpPostMessage(postAddr, postMsg))
					MAM_PTS_DLL.Log.AppendTrackingLog("HTTP_POST_TEST", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "Get Error From PostBack Dll");
                System.Threading.Thread.Sleep(delay);
            }
            return true;
       }

    }
}
