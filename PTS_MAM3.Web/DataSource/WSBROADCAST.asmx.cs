﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using PTS_MAM3.Web.Class;
using System.Text;
using PTS_MAM3.Web.WS.WSPGM;
using System.Data.SqlClient;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSBROADCAST 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSBROADCAST : System.Web.Services.WebService
    {
        List<Class_CODE> m_CodeData = new List<Class_CODE>();       //代碼檔集合
        List<Class_CODE_TBFILE_TYPE> m_CodeDataFILE_TYPE = new List<Class_CODE_TBFILE_TYPE>();       //代碼檔集合

        [WebMethod]
        public string TestMethod(string joibID)
        {
            // System.Text.StringBuilder sb = new System.Text.StringBuilder();
            // sb.AppendLine("<TSM_TRANSCODE>");
            // sb.AppendLine(string.Concat("<PROCESS_ID>", joibID, "</PROCESS_ID>"));
            // sb.AppendLine(string.Concat("<PROCESS_RESULT>", "FINISH", "</PROCESS_RESULT>"));
            // sb.AppendLine(string.Concat("<PROCESS_MESSAGE>", "MetaSAN Function Retry Trigger", "</PROCESS_MESSAGE>"));
            // sb.AppendLine("</TSM_TRANSCODE>");
            // if (MAM_PTS_DLL.Protocol.SendHttpPostMessage("http://localhost/PTS_MAM3.Web/DataSource/Handler_ReceiveTransCode_HD.ashx", sb.ToString()))
            //// if (MAM_PTS_DLL.Protocol.SendHttpPostMessage("http://10.13.220.34/DataSource/Handler_ReceiveTransCode_HD.ashx", sb.ToString()))
            // {
            //     return "Success";
            // }
            // else
            // {
            //    return"Fail";
            // }
            return "";
        }
        [WebMethod]
        public bool PrepareSendBroadcastNotify(Class_BROADCAST obj, Class_LOG_VIDEO logVideo)
        {
            if (obj.FCCHECK_STATUS == "N" || obj.FCCHECK_STATUS == "R")
            {
                int temp_Type = 0;
                if (obj.FCCHECK_STATUS == "R")
                    temp_Type = 2;
                else if (obj.FCCHANGE == "Y")
                    temp_Type = 3;
                else if (obj.FCCHECK_STATUS == "N")
                    temp_Type = 1;



                return SendBroadcastNotify(temp_Type, obj.FSBRO_TYPE, obj.FSCREATED_BY, obj.FSBRO_ID, obj.FSID, obj.FSID_NAME, obj.FNEPISODE.ToString(), logVideo);
            }

            return false;
        }

        bool SendBroadcastNotify(int notifyType, string bro_type, string userid, string broId, string fsid, string fsname, string episode, Class_LOG_VIDEO logVideo)
        {
            string[] UserDataArr = QUERY_TBUSERS_BYUSERID(userid);
            string userName = UserDataArr[0];
            string userEmail = UserDataArr[1];

            string Video_ID ="";
            string videoType ="";
            if (logVideo == null)
            {
                string[] logVideoInfo = QUERY_SENDBROADCAST_VIDEOID(broId);

                Video_ID = logVideoInfo[0];
                videoType = logVideoInfo[1] == "001" ? "SD" : "HD";
            }
            else 
            {
                Video_ID = logVideo.FSVIDEO_PROG;
                videoType = logVideo.FSARC_TYPE == "001" ? "SD" : "HD";
            }

            MAM_PTS_DLL.SysConfig sysObj = new MAM_PTS_DLL.SysConfig();
            List<string> SendTo = sysObj.sysConfig_Read("/ServerConfig/ReportEmailList/GroupList/BroadcastEmail").Split(new string[]{";"},StringSplitOptions.RemoveEmptyEntries).ToList();
            string Header = "";
            string Content = "";

            switch (notifyType)
            {
                case 0:
                    break;
                case 1:
                    Header = String.Format("[" + videoType + "檔案交播] 檔案上傳通知-上傳者：{0} {1}_{2}_{3}_{4}.mxf", userName, broId, fsname, episode, Video_ID);
                    Content = CheckPlayList(bro_type, fsid, episode);
                    break;

                case 2:
                    SendTo = new List<string> { userEmail };
                    Header = String.Format("[" + videoType + "檔案交播] 上傳檔案退單通知-審核者:{0}：{1}_{2}_{3}_{4}.mxf", userName, broId, fsname, episode, Video_ID);
                    Content = CheckPlayList(bro_type, fsid, episode);
                    break;

                case 3:
                    Header = String.Format("[" + videoType + "檔案交播] 檔案置換上傳通知-上傳者：{0} {1}_{2}_{3}_{4}.mxf", userName, broId, fsname, episode, Video_ID);
                    Content = CheckPlayList(bro_type, fsid, episode);

                    break;
            }

            if (MAM_PTS_DLL.Protocol.SendEmail(SendTo, Header, Content)
        )
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public string[] QUERY_TBUSERS_BYUSERID(string strUserID)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSUSER_ID", strUserID);
            string[] resultArr = new string[2];

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBUSER_BYUSERID", sqlParameters, out sqlResult))
                return null;    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count > 0)
            {

                resultArr[0] = sqlResult[0]["FSUSER_ChtName"];
                resultArr[1] = sqlResult[0]["FSEMAIL"];

                return resultArr;
            }
            else
                return null;
        }

        public string[] QUERY_SENDBROADCAST_VIDEOID(string broid)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSBRO_ID", broid);

            string[] result = new string[2];
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_SENDBROADCAST_VIDEOID_By_BROID", sqlParameters, out sqlResult))
                return null;    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count > 0)
            {
                result[0] = sqlResult[0]["FSVIDEO_PROG"];
                result[1] = sqlResult[0]["FSARC_TYPE"];
            }

            return result;
        }

        string CheckPlayList(string type, string fsid, string episode)
        {
            List<string> playListState = new List<string>();
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult = null;

            if (type == "G")
            {
                parameters.Add("fsid", fsid);
                parameters.Add("FNEPISODE", episode);

                MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPGM_CombinQueue_IN_14Day", parameters, out sqlResult);
            }
            else if (type == "P")
            {
                parameters.Add("FSPROMO_ID", fsid);

                MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPGM_ARR_PROMO_IN_14Day_BY_PROMOID", parameters, out sqlResult);
            }

            for (int i = 0; i < sqlResult.Count; i++)
            {
                if (playListState.Count == 0)
                {
                    playListState.Add("　　　　播出時間　　　│　頻道　 ");

                }
                string date = sqlResult[i]["Want_Date"];
                string time = sqlResult[i]["FSPLAYTIME"];
                string channel = sqlResult[i]["FSCHANNEL_NAME"];
                playListState.Add(date + "　" + time + "　│　" + channel);

            }

            string finalResult = "";

            foreach (string str in playListState)
            {
                finalResult += str + "<br>";
            }
            return finalResult;
        }

        [WebMethod]
        public bool RecordStartTranscode(Class_LOG_VIDEO brocast, string user_id)
        {
            string strMessage = "";

            System.Reflection.PropertyInfo[] properties = brocast.GetType().GetProperties();
            foreach (System.Reflection.PropertyInfo pi in properties)
            {
                object obj = pi.GetValue(brocast, null);
                if (obj != null)
                    strMessage += pi.Name + ":" + obj.ToString() + Environment.NewLine;
            }

            return MAM_PTS_DLL.Log.AppendTrackingLog("ClickToStartTrancode", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "ClickTranscode:" + strMessage);

        }

        [WebMethod]
        public bool RecordStepIntoTrackinglog(string stepName, string parameters, string user_id)
        {
            return MAM_PTS_DLL.Log.AppendTrackingLog("RecordStepIntoTrackinglog", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "StepName:" + stepName + " ; Parameters:" + parameters + " ; by：" + user_id);
        }

        #region 代碼檔

        //送帶轉檔檔案類型的所有代碼檔
        [WebMethod()]
        public List<Class_CODE> fnGetTBBROADCAST_CODE()
        {
            m_CodeData = MAMFunctions.Transfer_Class_CODE("SP_Q_TBBROADCAST_CODE");
            return m_CodeData;
        }

        //送帶轉檔檔案類型的入庫檔案類型代碼檔
        [WebMethod]
        public List<Class_CODE_TBFILE_TYPE> GetTBBROADCAST_CODE_TBFILE_TYPE()
        {
            List<Class_CODE_TBFILE_TYPE> m_CodeDataFILE_TYPE = new List<Class_CODE_TBFILE_TYPE>();
            m_CodeDataFILE_TYPE = MAMFunctions.Transfer_Class_CODE_TBFILE_TYPE("SP_Q_TBBROADCAST_CODE_TBFILE_TYPE");
            return m_CodeDataFILE_TYPE;
        }

        #endregion

        #region 節目或宣傳帶段落檔( TBLOG_VIDEO_SEG )

        //解析資料庫回來的Dictionary，轉存成Class
        private List<Class_LOG_VIDEO_SEG> Transfer_SEG(List<Dictionary<string, string>> sqlResult)
        {
            List<Class_LOG_VIDEO_SEG> returnData = new List<Class_LOG_VIDEO_SEG>();

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_LOG_VIDEO_SEG obj = new Class_LOG_VIDEO_SEG();
                // --------------------------------    
                obj.FSFILE_NO = sqlResult[i]["FSFILE_NO"];                         //檔案編號

                if (sqlResult[i]["FNSEG_ID"].StartsWith("0"))
                    obj.FNSEG_ID = sqlResult[i]["FNSEG_ID"].Substring(1, 1);       //段落序號(動手腳把第一碼為零的零拿掉)
                else
                    obj.FNSEG_ID = sqlResult[i]["FNSEG_ID"];

                obj.FSVIDEO_ID = sqlResult[i]["FSVIDEO_ID"];                       //主控播出編碼
                obj.FSBEG_TIMECODE = sqlResult[i]["FSBEG_TIMECODE"];               //Time Code起
                obj.FSEND_TIMECODE = sqlResult[i]["FSEND_TIMECODE"];               //Time Code迄
                obj.FSSUB_TIMECODE = MAM_PTS_DLL.TimeCodeCalc.Subtract_timecode(sqlResult[i]["FSBEG_TIMECODE"], sqlResult[i]["FSEND_TIMECODE"]); //Time Code相差
                obj.FCLOW_RES = sqlResult[i]["FCLOW_RES"];                         //段落轉檔狀態
                obj.FNBEG_TIMECODE_FRAME = MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(sqlResult[i]["FSBEG_TIMECODE"]);               //Time Code起,Frame數
                obj.FNEND_TIMECODE_FRAME = MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(sqlResult[i]["FSEND_TIMECODE"]);               //Time Code迄,Frame數

                obj.FSCREATED_BY = sqlResult[i]["FSCREATED_BY"];                                        //建檔者
                obj.FSCREATED_BY_NAME = sqlResult[i]["FSCREATED_BY_NAME"];                              //建檔者
                obj.FDCREATED_DATE = Convert.ToDateTime(sqlResult[i]["WANT_FDCREATEDDATE"]);            //建檔日期
                obj.SHOW_FDCREATED_DATE = MAMFunctions.compareDate(sqlResult[i]["WANT_FDCREATEDDATE"]); //建檔日期(顯示)
                obj.FSUPDATED_BY = sqlResult[i]["FSUPDATED_BY"];                                        //修改者
                obj.FSUPDATED_BY_NAME = sqlResult[i]["FSUPDATED_BY_NAME"];                              //修改者
                // --------------------------------
                returnData.Add(obj);
            }

            return returnData;
        }

        //根據檔案編號尋找所有段落by Jarvis20130605
        [WebMethod]
        public List<Class_LOG_VIDEO_SEG> GetTBLOG_VIDEO_SEG_COUNT(string fileNo)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSFILE_NO", fileNo);
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_SEG_ALL", sqlParameters, out sqlResult))
                return new List<Class_LOG_VIDEO_SEG>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL


            return Transfer_SEG(sqlResult);

        }


        //節目或宣傳帶段落檔的所有資料
        [WebMethod]
        public List<Class_LOG_VIDEO_SEG> fnGetTBLOG_VIDEO_SEG_ALL()
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_SEG", sqlParameters, out sqlResult))
                return new List<Class_LOG_VIDEO_SEG>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_SEG(sqlResult);
        }

        //節目或宣傳帶段落檔的資料_透過檔案編號 FileID
        [WebMethod]
        public List<Class_LOG_VIDEO_SEG> GetTBLOG_VIDEO_SEG_BYFileID(string strFileNO)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSFILE_NO", strFileNO);
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_SEG_BYFILEID", sqlParameters, out sqlResult))
                return new List<Class_LOG_VIDEO_SEG>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_SEG(sqlResult);
        }

        //刪除節目或宣傳帶段落檔的資料_透過檔案編號 FileID
        [WebMethod]
        public Boolean DelTBLOG_VIDEO_SEG_BYFileID(string strFileNO, string strUpdated_BY)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSFILE_NO", strFileNO);

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_TBLOG_VIDEO_SEG_BYFILEID", source, strUpdated_BY);
        }

        //新增節目或短帶段落檔
        [WebMethod]
        public Boolean INSERT_TBLOG_VIDEO_SEG(List<Class_LOG_VIDEO_SEG> Listobj)
        {
            WSMAMFunctions MAMobj = new WSMAMFunctions();  //取得VideoID用建立的object

            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            foreach (Class_LOG_VIDEO_SEG obj in Listobj)
            {
                sqlParameters.Add("FSFILE_NO", obj.FSFILE_NO);
                sqlParameters.Add("FNSEG_ID", obj.FNSEG_ID.Trim().PadLeft(2, '0'));

                //節目的VideoID是10碼，要串段落序號，短帶就直接是取到的VideoID_PROG(8碼)，資料帶先不取VideoID，因為不送主控播
                if (obj.FSVIDEO_ID != "")
                {
                    if (obj.FSFILE_NO.StartsWith("G"))
                    {

                        sqlParameters.Add("FSVIDEO_ID", obj.FSVIDEO_ID.Trim().Substring(0, 8) + obj.FNSEG_ID.Trim().PadLeft(2, '0'));
                    }
                    else if (obj.FSFILE_NO.StartsWith("P"))
                    {
                        sqlParameters.Add("FSVIDEO_ID", obj.FSVIDEO_ID.Trim() != "" ? obj.FSVIDEO_ID.Trim().Substring(0, 8) : "");
                    }
                    else if (obj.FSFILE_NO.StartsWith("D"))
                        sqlParameters.Add("FSVIDEO_ID", "");
                }
                else
                {
                    sqlParameters.Add("FSVIDEO_ID", "");
                }

                sqlParameters.Add("FSBEG_TIMECODE", obj.FSBEG_TIMECODE);
                sqlParameters.Add("FSEND_TIMECODE", obj.FSEND_TIMECODE);
                sqlParameters.Add("FCLOW_RES", "N");  //第一次新增時狀態為：N→未開始轉檔
                sqlParameters.Add("FSCREATED_BY", obj.FSCREATED_BY);
                sqlParameters.Add("FSUPDATED_BY", obj.FSUPDATED_BY);

                if (!MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBLOG_VIDEO_SEG", sqlParameters, obj.FSUPDATED_BY))
                    return false;

                sqlParameters.Clear();
            }

            MAMobj.Dispose();
            return true;
        }

        //查詢TBLOG_VIDEOID_MAP的VideoID，若是已有就取出，不然就新增後才取出
        [WebMethod]
        public string QueryVideoID_PROG_STT(string strID, string strEpisode, string strArc_Type, Boolean bolChange)
        {
            string strReturn = "";
            WSMAMFunctions obj = new WSMAMFunctions();

            if (bolChange == true)
                strReturn = obj.GetNewVideoID();    //置換時，就取新的VideoID
            else
                strReturn = obj.QueryVideoID_PROG_ARC_TYPE(strID, strEpisode, strArc_Type);

            return strReturn;
        }

        [WebMethod]
        public string QueryVideoID_PROG_Once(string strID, string strEpisode, string strArc_Type)
        {
            string strReturn = "";
            WSMAMFunctions obj = new WSMAMFunctions();
            strReturn = obj.QueryVideoID_PROG_ARC_TYPE_Once(strID, strEpisode, strArc_Type);
            //if (bolChange == true)
            //    strReturn = obj.GetNewVideoID();    //置換時，就取新的VideoID
            //else
            //    strReturn = obj.QueryVideoID_PROG_ARC_TYPE(strID, strEpisode, strArc_Type);

            return strReturn;
        }

        // 錯誤的 VideoID，為八個空白字元
        public const string ERROR_VIDEOID = "        ";
        /// <summary>
        /// 直接寫入一筆新的VideoID到TBLOG_VIDEOID_MAP
        /// 216-02-17 by Jarvis
        /// </summary>
        /// <param name="fsID"></param>
        /// <param name="fsEpisode"></param>
        /// <param name="fsARC_TYPE"></param>
        /// <returns></returns>
        [WebMethod]
        public string Insert_VideoID_Map(string fsID, string fsEpisode, string fsARC_TYPE, string video_id)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSID", fsID);
            source.Add("FNEPISODE", fsEpisode);
            source.Add("FSARC_TYPE", fsARC_TYPE);
            source.Add("FSVIDEO_ID_PROG", video_id);
            if (!MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBLOG_VIDEOID_MAP", source, "system"))
            {
                string errMsg = string.Concat("執行 SP_I_TBLOG_VIDEOID_MAP 時發生錯誤回傳，參數：", fsID, ", ", fsEpisode, ", ", fsARC_TYPE, ", ", video_id);
                MAM_PTS_DLL.Log.AppendTrackingLog("WSMAMFunctions_VideoID/QueryVideoID_PROG_ARC_TYPE", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return ERROR_VIDEOID;
            }

            return video_id;
        }

        #endregion

        #region 入庫影像檔 ( TBLOG_VIDEO )

        //解析資料庫回來的Dictionary，轉存成Class
        private List<Class_LOG_VIDEO> Transfer_Log_Video(List<Dictionary<string, string>> sqlResult)
        {
            List<Class_LOG_VIDEO> returnData = new List<Class_LOG_VIDEO>();
            m_CodeDataFILE_TYPE = MAMFunctions.Transfer_Class_CODE_TBFILE_TYPE("SP_Q_TBBROADCAST_CODE_TBFILE_TYPE");

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_LOG_VIDEO obj = new Class_LOG_VIDEO();
                // --------------------------------    
                obj.FSFILE_NO = sqlResult[i]["FSFILE_NO"];                                  //檔案編號         
                obj.FSSUBJECT_ID = sqlResult[i]["FSSUBJECT_ID"];                            //主題代碼
                obj.FSTYPE = sqlResult[i]["FSTYPE"];                                        //資料類型
                obj.FSID = sqlResult[i]["FSID"];                                            //編碼
                obj.FNEPISODE = Convert.ToInt16(sqlResult[i]["FNEPISODE"]);                 //集別
                obj.SHOW_FNEPISODE = MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]); //集別(顯示)

                obj.FSPLAY = sqlResult[i]["FSPLAY"];                                        //主控播出
                obj.FSARC_TYPE = sqlResult[i]["FSARC_TYPE"];                                //入庫檔案類型
                obj.FSARC_TYPE_NAME = compareCode_Code(sqlResult[i]["FSARC_TYPE"], "TBFILE_TYPE"); //入庫檔案類型
                obj.FSARC_SPEC_NAME = compareCode_CodeSDHD(sqlResult[i]["FSARC_TYPE"], "TBFILE_TYPE"); //入庫檔案類型規格;
                obj.FSFILE_TYPE_HV = sqlResult[i]["FSFILE_TYPE_HV"];                        //高解檔案類型
                obj.FSFILE_TYPE_LV = sqlResult[i]["FSFILE_TYPE_LV"];                        //低解檔案類型
                obj.FSTITLE = sqlResult[i]["FSTITLE"];                                      //標題
                obj.FSDESCRIPTION = sqlResult[i]["FSDESCRIPTION"];                          //描述
                obj.FSFILE_SIZE = sqlResult[i]["FSFILE_SIZE"];                               //檔案大小
                obj.FCFILE_STATUS = sqlResult[i]["FCFILE_STATUS"];                          //檔案狀態
                obj.FSCHANGE_FILE_NO = sqlResult[i]["FSCHANGE_FILE_NO"];                    //置換檔案編碼
                obj.FSOLD_FILE_NAME = sqlResult[i]["FSOLD_FILE_NAME"];                      //原始檔名
                obj.FSFILE_PATH_H = sqlResult[i]["FSFILE_PATH_H"];                          //TSM路徑_高解
                obj.FSFILE_PATH_L = sqlResult[i]["FSFILE_PATH_L"];                          //TSM路徑_低解
                obj.FNDIR_ID = Convert.ToInt64(sqlResult[i]["FNDIR_ID"]);                   //所屬Queue代碼
                obj.SHOW_FNDIR_ID = MAMFunctions.compareNumber(sqlResult[i]["FNDIR_ID"]);   //所屬Queue代碼(顯示)
                obj.FSCHANNEL_ID = sqlResult[i]["FSCHANNEL_ID"];                            //頻道別代碼
                obj.FSBEG_TIMECODE = sqlResult[i]["FSBEG_TIMECODE"];                        //Time Code 起
                obj.FSEND_TIMECODE = sqlResult[i]["FSEND_TIMECODE"];                        //Time Code 迄
                obj.FSBRO_ID = sqlResult[i]["FSBRO_ID"];                                    //送帶轉檔單號
                obj.FSARC_ID = sqlResult[i]["FSARC_ID"];                                    //入庫單號             

                obj.FSCREATED_BY_NAME = sqlResult[i]["FSCREATED_BY_NAME"];                              //建檔者姓名
                obj.FSCREATED_BY = sqlResult[i]["FSCREATED_BY"];                                        //建檔者
                obj.FDCREATED_DATE = Convert.ToDateTime(sqlResult[i]["WANT_FDCREATEDDATE"]);            //建檔日期
                obj.SHOW_FDCREATED_DATE = MAMFunctions.compareDate(sqlResult[i]["WANT_FDCREATEDDATE"]); //建檔日期(顯示)
                obj.FSUPDATED_BY_NAME = sqlResult[i]["FSUPDATED_BY_NAME"];                              //修改者姓名
                obj.FSUPDATED_BY = sqlResult[i]["FSUPDATED_BY"]; //修改者
                obj.FSTRACK = sqlResult[i]["FSTRACK"];//2014-09-19 By Jarvis
                obj.FCFILE_STATUS_NAME = MAMFunctions.CheckFileType(sqlResult[i]["FCFILE_STATUS"]);     //檔案狀態名稱

                obj.FSSUPERVISOR = sqlResult[i]["FSSUPERVISOR"];                                        //調用通知主管
                obj.FSJOB_ID = sqlResult[i]["FSJOB_ID"].ToString();                    //任務編號  
                if (obj.FCFILE_STATUS == "R")
                {
                    WS.WSASC.ServiceTranscode sobj = new WS.WSASC.ServiceTranscode();
                    WS.WSASC.ServiceAnystream.QueryAnsJobProgress myStatus = sobj.QueryTranscodeStatus(Convert.ToInt64(obj.FSJOB_ID));
                    if (myStatus.isFINISH == true && myStatus.isERROR == false)
                    { obj.FCFILE_STATUS_NAME = "轉檔成功，搬檔失敗"; }

                }
                obj.FCFROM = sqlResult[i]["FCFROM"];                                                    //資料來源
                obj.FSVIDEO_PROG = sqlResult[i]["FSVIDEO_PROG"].Trim();                                 //VIDEO ID
                obj.Brocast_Status = sqlResult[i].ContainsKey("Brocast_Status") ? sqlResult[i]["Brocast_Status"] : "";
                if (sqlResult[i]["FCFILE_STATUS"].Trim() == "S" & sqlResult[i]["FSJOB_ID"].Trim() != "")//轉檔進度
                {
                    WSSTT objSTT = new WSSTT();
                    obj.FCPROGRESS = objSTT.GetTranscode_Progress(obj.FSJOB_ID.Trim());
                }
                else
                    obj.FCPROGRESS = "";
                // --------------------------------
                obj.VideoListSeg = GetTBLOG_VIDEO_SEG_BYFileID(obj.FSFILE_NO);
                returnData.Add(obj);
            }

            return returnData;
        }

        //新增入庫影像檔
        [WebMethod]
        public Boolean INSERT_LOG_VIDEO(Class_LOG_VIDEO obj)
        {
            WS.WSTSM.ServiceTSM objTSM = new WS.WSTSM.ServiceTSM();   //產生高低解檔案路徑(懷疑第一次取，時間會比較久)

            string strFSFILE_PATH_H = "";  //高解檔案路徑
            string strFSFILE_PATH_L = "";  //低解檔案路徑

            strFSFILE_PATH_H = objTSM.GenerateSambaPathByFileIdInfo(obj.FSFILE_NO, WS.WSTSM.ServiceTSM.FileID_MediaType.HiVideo, obj.FSCHANNEL_ID, obj.FSFILE_TYPE_HV);

            if (obj.FSARC_TYPE == "026")
            {
                obj.FSFILE_TYPE_HV = System.IO.Path.GetExtension(obj.FSOLD_FILE_NAME).Replace(".", "");
                strFSFILE_PATH_H = strFSFILE_PATH_H.Replace("mxf", obj.FSFILE_TYPE_HV);
            }

            strFSFILE_PATH_L = objTSM.GenerateSambaPathByFileIdInfo(obj.FSFILE_NO, WS.WSTSM.ServiceTSM.FileID_MediaType.LowVideo, obj.FSCHANNEL_ID, obj.FSFILE_TYPE_LV);
            //if (obj.FSARC_TYPE_NAME.Contains("SD"))
            //{
            //    strFSFILE_PATH_L = objTSM.GenerateSambaPathByFileIdInfo(obj.FSFILE_NO, WS.WSTSM.ServiceTSM.FileID_MediaType.LowVideo, obj.FSCHANNEL_ID, "wmv");
            //}
            //else if (obj.FSARC_TYPE_NAME.Contains("HD"))
            //{
            //    strFSFILE_PATH_L = objTSM.GenerateSambaPathByFileIdInfo(obj.FSFILE_NO, WS.WSTSM.ServiceTSM.FileID_MediaType.LowVideo, obj.FSCHANNEL_ID, "mp4");
            //}

            //strFSFILE_PATH_L = objTSM.GenerateSambaPathByFileIdInfo(obj.FSFILE_NO, WS.WSTSM.ServiceTSM.FileID_MediaType.LowVideo, obj.FSCHANNEL_ID, "wmv");

            if (strFSFILE_PATH_H == "" || strFSFILE_PATH_L == "")
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("/DataSource/WSBROADCAST/INSERT_LOG_VIDEO", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "strFSFILE_PATH_H=" + strFSFILE_PATH_H + "; strFSFILE_PATH_L=" + strFSFILE_PATH_L);
                return false;
            }
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSFILE_NO", obj.FSFILE_NO);
            source.Add("FSSUBJECT_ID", obj.FSSUBJECT_ID);
            source.Add("FSTYPE", obj.FSTYPE);
            source.Add("FSID", obj.FSID);
            source.Add("FNEPISODE", obj.FNEPISODE.ToString());
            source.Add("FSSUPERVISOR", obj.FSSUPERVISOR.ToString());
            source.Add("FSPLAY", obj.FSPLAY);
            source.Add("FSARC_TYPE", obj.FSARC_TYPE);
            source.Add("FSFILE_TYPE_HV", obj.FSFILE_TYPE_HV);
            source.Add("FSFILE_TYPE_LV", obj.FSFILE_TYPE_LV == null ? "" : obj.FSFILE_TYPE_LV);
            source.Add("FSTITLE", obj.FSTITLE);
            source.Add("FSDESCRIPTION", obj.FSDESCRIPTION);
            source.Add("FSFILE_SIZE", obj.FSFILE_SIZE.ToString());
            source.Add("FCFILE_STATUS", obj.FCFILE_STATUS);
            source.Add("FSCHANGE_FILE_NO", obj.FSCHANGE_FILE_NO);
            source.Add("FSOLD_FILE_NAME", obj.FSOLD_FILE_NAME);


            source.Add("FSFILE_PATH_H", strFSFILE_PATH_H);
            source.Add("FSFILE_PATH_L", strFSFILE_PATH_L);

            source.Add("FNDIR_ID", obj.FNDIR_ID.ToString());
            source.Add("FSCHANNEL_ID", obj.FSCHANNEL_ID);
            source.Add("FSBEG_TIMECODE", obj.FSBEG_TIMECODE);
            source.Add("FSEND_TIMECODE", obj.FSEND_TIMECODE);
            source.Add("FSBRO_ID", obj.FSBRO_ID);
            source.Add("FSARC_ID", obj.FSARC_ID);
            source.Add("FSJOB_ID", "-2");   //預設的JOBID為-2，才不會搞錯
            source.Add("FCFROM", "N");
            source.Add("FSVIDEO_PROG", obj.FSVIDEO_PROG);

            if (obj.FSTAPE_ID == null)      //影帶回溯時，也要帶回影帶管理系統的影帶編號
                source.Add("FSTAPE_ID", "");
            else
                source.Add("FSTAPE_ID", obj.FSTAPE_ID);

            source.Add("FSCREATED_BY", obj.FSCREATED_BY);
            source.Add("FSUPDATED_BY", obj.FSUPDATED_BY);

            source.Add("FCLOW_RES", "N");
            source.Add("FCKEYFRAME", "N");
            source.Add("FNENC_CNT", "0");
            source.Add("FSENC_MSG", "");
            source.Add("FDENC_DATE", "1900/1/1");
            source.Add("FCINGEST_QC", "");
            source.Add("FSINGEST_QC_MSG", "");

            if (obj.FSTRACK == "" || obj.FSTRACK == null)
            {
                obj.FSTRACK = "MMMM";
            }

            source.Add("FSTRACK", obj.FSTRACK);//新增 2014-09-19 by Jarvis

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBLOG_VIDEO", source, obj.FSUPDATED_BY);
        }

        //查詢入庫影像檔資料_透過檔案節目編號及集別
        [WebMethod]
        public List<Class_LOG_VIDEO> GetTBLOG_VIDEO_BYProgID_Episode_BROID(string strType, string strProgID, string strEpisode, string strBroID)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSTYPE", strType);
            sqlParameters.Add("FSID", strProgID);
            sqlParameters.Add("FNEPISODE", strEpisode);
            sqlParameters.Add("FSBRO_ID", strBroID);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_BY_ID_EPISODE_BROID", sqlParameters, out sqlResult))
                return new List<Class_LOG_VIDEO>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_Log_Video(sqlResult);
        }

        [WebMethod]
        public List<Class_LOG_VIDEO> GetTBLOG_VIDEO_BY_Type_EPISODE(string strType, string arc_Type, string strProgID, string strEpisode, string bro_ID)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSTYPE", strType);
            sqlParameters.Add("FSID", strProgID);
            sqlParameters.Add("FNEPISODE", strEpisode);
            sqlParameters.Add("FSARC_TYPE", arc_Type);
            sqlParameters.Add("FSBRO_ID", bro_ID);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_By_Type_EPISODE", sqlParameters, out sqlResult))
                return new List<Class_LOG_VIDEO>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_Log_Video(sqlResult);
        }

        //查詢入庫影像檔資料_透過檔案節目編號及集別
        [WebMethod]
        public List<Class_LOG_VIDEO> GetTBLOG_VIDEO_BYProgID_Episode(string strType, string strProgID, string strEpisode)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSTYPE", strType);
            sqlParameters.Add("FSID", strProgID);
            sqlParameters.Add("FNEPISODE", strEpisode);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_BY_ID_EPISODE", sqlParameters, out sqlResult))
                return new List<Class_LOG_VIDEO>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_Log_Video(sqlResult);
        }

        [WebMethod]
        public List<Class_LOG_VIDEO> GetTBLOG_VIDEO_BYFSFILE_NO(string fileNo)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSFILE_NO", fileNo);


            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_BY_FSFILE_NO", sqlParameters, out sqlResult))
                return new List<Class_LOG_VIDEO>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_Log_Video(sqlResult);

        }

        //修改入庫影像檔
        [WebMethod]
        public Boolean UPDATE_TBLOG_VIDEO_TITLE_DES(string strFILE_NO, string strTitle, string strDescription, string strBegin, string strEnd, string FSSUPERVISOR, string strUpdated_By)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSFILE_NO", strFILE_NO);
            sqlParameters.Add("FSTITLE", strTitle);
            sqlParameters.Add("FSDESCRIPTION", strDescription);
            sqlParameters.Add("FSBEG_TIMECODE", strBegin);
            sqlParameters.Add("FSEND_TIMECODE", strEnd);
            sqlParameters.Add("FSSUPERVISOR", FSSUPERVISOR);
            if (!MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBLOG_VIDEO_TITLE_DES", sqlParameters, strUpdated_By))
                return false;    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return true;
        }

        //修改入庫影像檔_多一個入庫影像類型
        [WebMethod]
        public Boolean UPDATE_TBLOG_VIDEO_TITLE_DES_ARC(string strFILE_NO, string strTitle, string strDescription, string strBegin, string strEnd, string FSSUPERVISOR, string strARC, string strUpdated_By)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSFILE_NO", strFILE_NO);
            sqlParameters.Add("FSTITLE", strTitle);
            sqlParameters.Add("FSDESCRIPTION", strDescription);
            sqlParameters.Add("FSBEG_TIMECODE", strBegin);
            sqlParameters.Add("FSEND_TIMECODE", strEnd);
            sqlParameters.Add("FSSUPERVISOR", FSSUPERVISOR);
            sqlParameters.Add("FSARC_TYPE", strARC);
            if (!MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBLOG_VIDEO_TITLE_DES_ARC", sqlParameters, strUpdated_By))
                return false;    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return true;
        }

        //修改入庫影像檔_更改檔案狀態
        [WebMethod]
        public Boolean UPDATE_TBLOG_VIDEO_FILE_STATUS(string strFILE_NO, string strStatus, string strUpdated_By)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSFILE_NO", strFILE_NO);
            sqlParameters.Add("FCFILE_STATUS", strStatus);
            sqlParameters.Add("FSUPDATED_BY", strUpdated_By);

            if (!MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBLOG_VIDEO_FILE_STATUS", sqlParameters, strUpdated_By))
                return false;    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return true;
        }

        //修改入庫影像檔及轉檔檔_強制失敗時入庫影像檔更改檔案狀態為失敗，轉檔檔也要更改狀態為失敗
        [WebMethod]
        public string UPDATE_TBLOG_VIDEO_TBTRANSCODE_FORCE_FAIL(string strFILE_NO, string strJobID, string strUpdated_By)
        {
            int ansJobID;
            WSSTT objWSSTT = new WSSTT();
            if (objWSSTT.GetTranscode_Progress(strJobID) == "完成")
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/WSBROADCAST/UPDATE_TBLOG_VIDEO_TBTRANSCODE_FORCE_FAIL", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "web services function：Anystream 轉檔已完成，無法強制停止轉檔，檔案編號：" + strFILE_NO + "，任務編號：" + strJobID + "，更新者：" + strUpdated_By);
                return "Anystream 轉檔已完成，無法強制停止轉檔";
            }

            //// 如果在 TSMRecall 或 DoPartial 的時候發生問題，這邊就會把「強制失敗」的需求擋掉，變成沒有辦法停止轉檔動作 2013/01/30 唯智加的
            //WS.WSASC.ServiceTranscode objTranscode = new WS.WSASC.ServiceTranscode();
            //WS.WSASC.ClassTranscodeJob objTranscodeJob = objTranscode.GetOneTranscodeJobInformation(strJobID);
            //if (Int32.Parse(objTranscodeJob.AnsJobID) < 0)
            //{
            //    MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/WSBROADCAST/UPDATE_TBLOG_VIDEO_TBTRANSCODE_FORCE_FAIL", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "web services function：Anystream 轉檔已完成，無法強制停止轉檔，檔案編號：" + strFILE_NO + "，任務編號：" + strJobID + "，更新者：" + strUpdated_By);
            //MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/WSBROADCAST/UPDATE_TBLOG_VIDEO_TBTRANSCODE_FORCE_FAIL", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, string.Format("轉檔狀態清除：{0}, {1}, {2}, {3}", FSJOB_ID, sqlResult[0]["FNTSM_ID"], sqlResult[0]["FNPARTIAM_ID"], sqlResult[0]["FNANYSTREAM_ID"])); //有些問題
            //    return "尚未取得 AnystreamID，無法強制停止轉檔";
            //}



            MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/WSBROADCAST/UPDATE_TBLOG_VIDEO_TBTRANSCODE_FORCE_FAIL", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "web services function：強制轉檔失敗程序開始，檔案編號：" + strFILE_NO + "，任務編號：" + strJobID + "，更新者：" + strUpdated_By);

            List<Dictionary<string, string>> sqlResult;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSFILE_NO", strFILE_NO);
            sqlParameters.Add("FSJOB_ID", strJobID);
            sqlParameters.Add("FCFILE_STATUS", "R");
            sqlParameters.Add("FCLOW_RES", "F");
            sqlParameters.Add("FNPROCESS_STATUS", ((int)(WS.WSASC.ServiceTranscode.TRANSCODE_STATUS.SYSERROR)).ToString());

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_U_TBLOG_VIDEO_TBTRANSCODE_FORCE_FAIL", sqlParameters, out sqlResult))
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/WSBROADCAST/UPDATE_TBLOG_VIDEO_TBTRANSCODE_FORCE_FAIL", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "web services function：強制轉檔失敗，檔案編號：" + strFILE_NO + "，任務編號：" + strJobID + "，更新者：" + strUpdated_By);
                return "無法更新轉檔失敗，資料庫更新異常";    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            }

            if (sqlResult.Count > 0)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/WSBROADCAST/UPDATE_TBLOG_VIDEO_TBTRANSCODE_FORCE_FAIL", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, string.Format("轉檔狀態清除：{0}, {1}, {2}, {3}", strJobID, sqlResult[0]["FNTSM_ID"], sqlResult[0]["FNPARTIAM_ID"], sqlResult[0]["FNANYSTREAM_ID"]));
                if (int.TryParse(sqlResult[0]["FNANYSTREAM_ID"], out ansJobID) == true && ansJobID > 0)
                {

                    WS.WSASC.ServiceAnystream objAnystream = new WS.WSASC.ServiceAnystream();
                    if (objAnystream.DoJobStop(ansJobID) == true)
                    {
                        MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/WSBROADCAST/UPDATE_TBLOG_VIDEO_TBTRANSCODE_FORCE_FAIL", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "web services function：強制轉檔失敗的指令完成，檔案編號：" + strFILE_NO + "，任務編號：" + strJobID + "，anystream 編號：" + ansJobID.ToString() + "，更新者：" + strUpdated_By);
                        return "";
                    }
                    else
                    {
                        MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/WSBROADCAST/UPDATE_TBLOG_VIDEO_TBTRANSCODE_FORCE_FAIL", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "web services function：通知Anystream 轉檔停止指令異常，檔案編號：" + strFILE_NO + "，任務編號：" + strJobID + "，anystream 編號：" + ansJobID + "，更新者：" + strUpdated_By);
                        return "通知Anystream 轉檔停止指令異常";
                    }
                }
                else
                {
                    MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/WSBROADCAST/UPDATE_TBLOG_VIDEO_TBTRANSCODE_FORCE_FAIL", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "web services function：強制轉檔失敗的指令完成，並且無呼叫anystream 停止，檔案編號：" + strFILE_NO + "，任務編號：" + strJobID + "，anystream 編號：" + ansJobID.ToString() + "，更新者：" + strUpdated_By);
                    return "";
                }
            }
            else
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/WSBROADCAST/UPDATE_TBLOG_VIDEO_TBTRANSCODE_FORCE_FAIL", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "web services function：無法更新轉檔失敗，資料庫更新異常，檔案編號：" + strFILE_NO + "，任務編號：" + strJobID + "，更新者：" + strUpdated_By);
                return "無法更新轉檔失敗，資料庫更新異常";
            }
        }



        //讓置換審核流程來呼叫-當置換的送帶轉檔單審核完成後，要將MAP裡的VideoID換掉，以及要把被置換的檔案狀態改成D
        /// <summary>
        /// 用來將送帶轉檔置換審核通過後檔案的狀態將原本的檔案狀態置換成'D'
        /// 2012/07/24 comment by Kyle
        /// </summary>
        /// <param name="strBroID">送帶轉檔置換表單編號</param>
        /// <param name="strUpdated_By">由誰發出的</param>
        /// <returns>True:成功 False:失敗</returns>
        [WebMethod]
        public Boolean UPDATE_TBLOG_VIDEO_REVIEW(string strBroID, string strUpdated_By)
        {
            List<string> ListFileNO = new List<string>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            Dictionary<string, string> sqlParameters2 = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSBRO_ID", strBroID);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_BY_BROID", sqlParameters, out sqlResult))
                return false;    // 發生SQL錯誤時，回傳空的東西，而不是NULL          

            if (sqlResult.Count > 0)
            {
                WSMAMFunctions obj = new WSMAMFunctions();

                for (int i = 0; i < sqlResult.Count; i++)
                {
                    //送帶轉檔單裡每個檔案都要去修改MAP對照檔的VideoID
                    obj.UpdateVideoID_PROG(sqlResult[i]["FSID"], sqlResult[i]["FNEPISODE"], sqlResult[i]["FSARC_TYPE"], sqlResult[i]["FSVIDEO_PROG"]);

                    //送帶轉檔單裡每個檔案都要將要置換的檔案狀態改成D
                    sqlParameters2.Add("FSFILE_NO", sqlResult[i]["FSFILE_NO"]);
                    MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBLOG_VIDEO_STT_REVIEW", sqlParameters2, strUpdated_By);
                    sqlParameters2.Clear();
                }

                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// 更新音軌設定 by Jarvis 20141124
        /// </summary>
        /// <param name="file_no">檔案編號</param>
        /// <param name="newTrack">音軌設定</param>
        /// <returns></returns>
        [WebMethod]
        public Boolean UPDATE_TBLOG_VIDEO_TRACK(string file_no, string newTrack, string strUpdated_By)
        {
            List<string> ListFileNO = new List<string>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();

            sqlParameters.Add("FSFILE_NO", file_no);
            sqlParameters.Add("FSTRACK", newTrack);
            sqlParameters.Add("FSUPDATED_BY", strUpdated_By);
            if (!MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBLOG_VIDEO_FSTRACK", sqlParameters, strUpdated_By))
                return false;    // 發生SQL錯誤時，回傳空的東西，而不是NULL          

            return true;
        }


        /// <summary>
        /// 將被置換的檔案狀態改成D
        /// </summary>
        /// <param name="strFSFILE_NO">要置換別人的FILE_NO</param>
        /// <param name="strUpdated_By">修改者</param>
        /// <returns>true:成功 false:失敗</returns>
        [WebMethod]
        public Boolean UPDATE_TBLOG_VIDEO_AND_D_WHEN_EXCHANGE_FILE(string strFSFILE_NO, string strUpdated_By)
        {
            List<string> ListFileNO = new List<string>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            Dictionary<string, string> sqlParameters2 = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSFILE_NO", strFSFILE_NO);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_BY_FSFILE_NO", sqlParameters, out sqlResult))
                return false;    // 發生SQL錯誤時，回傳空的東西，而不是NULL          

            if (sqlResult.Count > 0)
            {
                WSMAMFunctions obj = new WSMAMFunctions();

                for (int i = 0; i < sqlResult.Count; i++)
                {
                    //送帶轉檔單裡每個檔案都要去修改MAP對照檔的VideoID
                    obj.UpdateVideoID_PROG(sqlResult[i]["FSID"], sqlResult[i]["FNEPISODE"], sqlResult[i]["FSARC_TYPE"], sqlResult[i]["FSVIDEO_PROG"]);

                    //送帶轉檔單裡每個檔案都要將要置換的檔案狀態改成D
                    sqlParameters2.Add("FSFILE_NO", sqlResult[i]["FSFILE_NO"]);
                    MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBLOG_VIDEO_STT_REVIEW", sqlParameters2, strUpdated_By);
                    sqlParameters2.Clear();
                }

                return true;
            }
            else
                return false;
        }

        //修改入庫影像檔_更新JobID(轉檔進度開始)
        [WebMethod]
        public Boolean UPDATE_TBLOG_VIDEO_JobID(string strFILE_NO, string strJobID, string strTransFrom, string strUpdated_By)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSFILE_NO", strFILE_NO);
            sqlParameters.Add("FSJOB_ID", strJobID);
            sqlParameters.Add("FSTRANS_FROM", strTransFrom);
            sqlParameters.Add("FSUPDATED_BY", strUpdated_By);
            if (!MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBLOG_VIDEO_JOBID", sqlParameters, strUpdated_By))
                return false;    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            //201209_001增加將原本被置換的檔案把狀態改為D的function
            bool rtnValue = UPDATE_TBLOG_VIDEO_AND_D_WHEN_EXCHANGE_FILE(strFILE_NO, strUpdated_By);
            return rtnValue;
        }

        //修改入庫影像檔_更新檔案狀態(轉檔完成)
        [WebMethod]
        public Boolean UPDATE_TBLOG_VIDEO_STT_OK(string strJobID, string strFILE_SIZE, string strVideo_Property, string strUpdated_By)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSJOB_ID", strJobID);
            sqlParameters.Add("FSFILE_SIZE", strFILE_SIZE);
            sqlParameters.Add("FSVIDEO_PROPERTY", strVideo_Property);

            //這個預存程序裡會判斷是否有要置換檔案，若是FSCHANGE_FILE_NO不為空，就會將該置換檔案的狀態改成D(被置換)，自身設成T(轉檔完成)，以及TBLOG_VIDEO_D置換檔案的狀態改成D(被置換)
            if (!MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBLOG_VIDEO_STT_OK", sqlParameters, strUpdated_By))
                return false;    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return true;
        }

        //修改入庫影像檔_更新檔案狀態(轉檔完成)
        [WebMethod]
        public Boolean UPDATE_TBLOG_VIDEO_STT_OK_BY_FILENO(string file_no, string strFILE_SIZE, string strVideo_Property, string strUpdated_By)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSFILE_NO", file_no);
            sqlParameters.Add("FSFILE_SIZE", strFILE_SIZE);
            sqlParameters.Add("FSVIDEO_PROPERTY", strVideo_Property);

            //這個預存程序裡會判斷是否有要置換檔案，若是FSCHANGE_FILE_NO不為空，就會將該置換檔案的狀態改成D(被置換)，自身設成T(轉檔完成)，以及TBLOG_VIDEO_D置換檔案的狀態改成D(被置換)
            if (!MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBLOG_VIDEO_STT_OK_BY_FILENO", sqlParameters, strUpdated_By))
                return false;    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return true;
        }

        //修改入庫影像檔_更新檔案狀態(轉檔失敗)
        [WebMethod]
        public Boolean UPDATE_TBLOG_VIDEO_STT_FAIL(string strJobID, string strUpdated_By)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSJOB_ID", strJobID);
            if (!MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBLOG_VIDEO_STT_FAIL", sqlParameters, strUpdated_By))
                return false;    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return true;
        }

        //修改入庫影像檔_更新檔案狀態(轉檔失敗)
        [WebMethod]
        public Boolean UPDATE_TBLOG_VIDEO_STT_FAIL_BY_FILENO(string file_no, string strUpdated_By)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSFILE_NO", file_no);
            if (!MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBLOG_VIDEO_STT_FAIL_BY_FILENO", sqlParameters, strUpdated_By))
                return false;    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return true;
        }

        // 用JOBID取得檔案編號，並且寫入TBLOG_VIDEO_D
        [WebMethod]
        public Boolean GetTBLOG_VIDEO_FILENO_INSERT_TBLOG_VIDEO_D(string strJobID)
        {
            string strFileNO = "";

            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSJOB_ID", strJobID);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_BYJOBID", sqlParameters, out sqlResult))
                return false;    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count == 0)
            {
                string logFileName = string.Concat(@"D:\LogFiles\DB_R-", DateTime.Now.ToString("yyyyMMdd"), ".txt");
                StringBuilder outputLog = new StringBuilder();
                outputLog.AppendLine(string.Format("執行{0}後找不到資料", "SP_Q_TBLOG_VIDEO"));
                outputLog.AppendLine("參數：");
                foreach (string index in sqlParameters.Keys)
                    outputLog.AppendLine(string.Format("{0}->{1}", index, sqlParameters[index]));
                outputLog.AppendLine("錯誤訊息：");
                MAM_PTS_DLL.Log.WriteTxtLog(logFileName, outputLog.ToString(), "ERR01");
            }

            if (sqlResult.Count > 0)
            {
                strFileNO = sqlResult[0]["FSFILE_NO"];

                if (strFileNO.Trim() != "")   //取回的檔案編號不為空，就要去複製段落檔
                {
                    INSERT_LOG_VIDEO_D(strFileNO);
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }

        // 用JOBID取得檔案路徑
        [WebMethod]
        public Boolean GetTBLOG_VIDEO_PATH_BYJOBID(string strJobID, out string strFileNo, out string strHiPath, out string strLowPath, out string strFileType_H, out string strFileType_L, out string strType, out string strVideoID, out string strARC_TYPE)
        {
            strFileNo = "";
            strHiPath = "";
            strLowPath = "";
            strFileType_H = "";
            strFileType_L = "";
            strType = "";
            strVideoID = "";
            strARC_TYPE = "";

            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSJOB_ID", strJobID);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_BYJOBID", sqlParameters, out sqlResult))
                return false;    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count > 0)
            {
                strFileNo = sqlResult[0]["FSFILE_NO"];
                strHiPath = sqlResult[0]["FSFILE_PATH_H"];
                strLowPath = sqlResult[0]["FSFILE_PATH_L"];
                strFileType_H = sqlResult[0]["FSFILE_TYPE_HV"];
                strFileType_L = sqlResult[0]["FSFILE_TYPE_LV"];
                strType = sqlResult[0]["FSTYPE"];
                strVideoID = sqlResult[0]["FSVIDEO_PROG"];
                strARC_TYPE = sqlResult[0]["FSARC_TYPE"];
                return true;
            }
            else
                return false;
        }

        //新增入庫影像檔段落_TBLOG_VIDEO_D
        [WebMethod]
        public Boolean INSERT_LOG_VIDEO_D(string strFileNO)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSFILE_NO", strFileNO);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO", sqlParameters, out sqlResult))
                return false;

            if (sqlResult.Count == 0)
            {
                string logFileName = string.Concat(@"D:\LogFiles\DB_R-", DateTime.Now.ToString("yyyyMMdd"), ".txt");
                StringBuilder outputLog = new StringBuilder();
                outputLog.AppendLine(string.Format("執行{0}後找不到資料", "SP_Q_TBLOG_VIDEO"));
                outputLog.AppendLine("參數：");
                foreach (string index in sqlParameters.Keys)
                    outputLog.AppendLine(string.Format("{0}->{1}", index, sqlParameters[index]));
                outputLog.AppendLine("錯誤訊息：");
                MAM_PTS_DLL.Log.WriteTxtLog(logFileName, outputLog.ToString(), "ERR01");

            }
            //取得TBLOG_VIDEO放到sqlResult，再寫到TBLOG_VIDEO_D                                              
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSFILE_NO", sqlResult[0]["FSFILE_NO"]);
            source.Add("FSSUBJECT_ID", sqlResult[0]["FSSUBJECT_ID"]);
            source.Add("FSKEYFRAME_NO", sqlResult[0]["FSFILE_NO"] + "_00000000");  //FSKEYFRAME_NO => 檔案編號+底線+8個0
            source.Add("FSTYPE", sqlResult[0]["FSTYPE"]);
            source.Add("FSID", sqlResult[0]["FSID"]);
            source.Add("FNEPISODE", sqlResult[0]["FNEPISODE"]);
            source.Add("FSSUPERVISOR", sqlResult[0]["FSSUPERVISOR"]);
            source.Add("FSTITLE", sqlResult[0]["FSTITLE"]);
            source.Add("FSDESCRIPTION", sqlResult[0]["FSDESCRIPTION"]);
            source.Add("FCFILE_STATUS", sqlResult[0]["FCFILE_STATUS"]);
            source.Add("FSCHANGE_FILE_NO", sqlResult[0]["FSCHANGE_FILE_NO"]);
            source.Add("FSOLD_FILE_NAME", sqlResult[0]["FSOLD_FILE_NAME"]);
            source.Add("FNDIR_ID", sqlResult[0]["FNDIR_ID"]);
            source.Add("FSCHANNEL_ID", sqlResult[0]["FSCHANNEL_ID"]);
            source.Add("FSBEG_TIMECODE", sqlResult[0]["FSBEG_TIMECODE"]);
            source.Add("FSEND_TIMECODE", sqlResult[0]["FSEND_TIMECODE"]);
            source.Add("FCFROM", sqlResult[0]["FCFROM"]);
            source.Add("FSTAPE_ID", sqlResult[0]["FSTAPE_ID"]);                     //影帶編號
            source.Add("FSTRANS_FROM", sqlResult[0]["FSTRANS_FROM"]);               //轉檔來源
            source.Add("FSCREATED_BY", sqlResult[0]["FSCREATED_BY"]);
            source.Add("FSUPDATED_BY", sqlResult[0]["FSUPDATED_BY"]);

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBLOG_VIDEO_D", source, "System");  //系統自動轉檔
        }

        //查詢入庫影像檔資料_透過檔案編號
        [WebMethod]
        public List<Class_LOG_VIDEO> GetTBLOG_VIDEO_BYFILNO(string strFileNO)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSFILE_NO", strFileNO);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO", sqlParameters, out sqlResult))
                return new List<Class_LOG_VIDEO>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_Log_Video(sqlResult);
        }

        //用類型、編號取得製作人列表字串
        [WebMethod]
        public string GetPRODUCER(string strPROType, string strID)
        {
            String strPRODUCER = "";
            string strPRODUCERName = "";
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPRO_TYPE", strPROType);
            sqlParameters.Add("FSID", strID);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_PRODUCER", sqlParameters, out sqlResult))
                return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            for (int i = 0; i < sqlResult.Count; i++)
            {
                strPRODUCER = strPRODUCER + sqlResult[i]["FSPRODUCER"] + ",";
            }

            for (int i = 0; i < sqlResult.Count; i++)
            {
                strPRODUCERName = strPRODUCERName + sqlResult[i]["SHOW_FSPRODUCER_NAME"] + "、";
            }

            return strPRODUCER + ";" + strPRODUCERName;
        }

        //用檔案編號集合查詢是否有排播過，因為會決定要跑哪一個流程
        [WebMethod]
        public Boolean GetPROCESS_PLAYLIST(List<string> ListFileNO)
        {
            for (int i = 0; i < ListFileNO.Count; i++)
            {
                Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
                sqlParameters.Add("FSFILE_NO", ListFileNO[i]);

                List<Dictionary<string, string>> sqlResult;
                if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_HAVEEVER_IN_PLAYLIST_FILEID", sqlParameters, out sqlResult))
                    return false;

                if (sqlResult.Count > 0)        //只要找到一筆是已排播的，這筆送帶轉檔置換單要跑部門經理審核的流程
                    return true;
            }

            return false;                       //若是每一筆都沒有排播的記錄，這筆送帶轉檔置換單要跑製作人審核的流程
        }

        //查詢入庫影像檔(避免一筆以上的SD播出帶或是HD播出帶)，傳入入庫影像檔
        [WebMethod]
        public string QUERY_TBLOG_VIDEO_BYProgID_Episode_LIST_CHECK(List<Class_LOG_VIDEO> objLOG_VIDEO)
        {
            string strReturn = "";

            for (int i = 0; i < objLOG_VIDEO.Count; i++)
            {
                if (objLOG_VIDEO[i].FSARC_TYPE.ToString() == "001" || objLOG_VIDEO[i].FSARC_TYPE.ToString() == "002")
                {
                    if (QUERY_TBLOG_VIDEO_BYProgID_Episode_CHECK(objLOG_VIDEO[i].FSTYPE.ToString(), objLOG_VIDEO[i].FSID.ToString(), objLOG_VIDEO[i].FNEPISODE.ToString(), objLOG_VIDEO[i].FSARC_TYPE.ToString(), objLOG_VIDEO[i].FSCHANGE_FILE_NO.ToString().Trim()) == true)
                        strReturn = strReturn + objLOG_VIDEO[i].FSARC_TYPE_NAME + "已存在一筆\n";
                }
            }

            return strReturn;
        }

        //查詢入庫影像檔(避免一筆以上的SD播出帶或是HD播出帶)
        [WebMethod]
        public Boolean QUERY_TBLOG_VIDEO_BYProgID_Episode_CHECK(string strType, string strProgID, string strEpisode, string strArcType, string strChangeFileNo)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSTYPE", strType);
            source.Add("FSID", strProgID);
            source.Add("FNEPISODE", strEpisode);
            source.Add("FSARC_TYPE", strArcType);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_BY_ID_EPISODE_ARCTPYE_CHECK", source, out sqlResult))
                return false;    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count > 0)
            {
                if (strChangeFileNo.Trim() == "")
                    return true;    //沒有置換資料表示不需要檢查，即可確定是有SD播出帶或HD播出帶，因此不能簽收
                else if (sqlResult[0]["FSFILE_NO"] == strChangeFileNo)
                    return false;   //要置換的是系統裡找到的SD播出帶或HD播出帶，因此要能簽收，表示是走置換流程
                else
                    return true;    //要置換的不是系統裡找到的SD播出帶或HD播出帶，因此不能簽收
            }
            else
                return false;
        }

        #endregion
        #region HD 檔案送播
        //解析資料庫回來的Dictionary，轉存成Class
        private List<Class_BROADCAST> Transfer_BRO_HD(List<Dictionary<string, string>> sqlResult)
        {
            List<Class_BROADCAST> returnData = new List<Class_BROADCAST>();

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_BROADCAST obj = new Class_BROADCAST();
                //obj.FSFILE_NO = sqlResult[i]["FSFILE_NO"];
                //obj.FSVIDEO_PROG = sqlResult[i]["FSVIDEO_PROG"];//Add 2015-10-02 by Jarvis
                // --------------------------------    
                obj.FSBRO_ID = sqlResult[i]["FSBRO_ID"];                                    //送帶轉檔單號
                obj.FSBRO_TYPE = sqlResult[i]["FSBRO_TYPE"];                                //類型
                obj.FSBRO_TYPE_NAME = MAMFunctions.CheckType(sqlResult[i]["FSBRO_TYPE"]);   //類型名稱
                obj.FSID = sqlResult[i]["FSID"];                                            //編碼
                obj.FSID_NAME = sqlResult[i]["FSPROG_ID_NAME"];                             //節目或宣傳帶名稱
                obj.FNEPISODE = Convert.ToInt16(sqlResult[i]["FNEPISODE"]);                 //集別
                obj.SHOW_FNEPISODE = MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]); //集別(顯示)
                obj.FNEPISODE_NAME = sqlResult[i]["FNEPISODE_NAME"];                        //集別名稱
                obj.FDBRO_DATE = Convert.ToDateTime(sqlResult[i]["WANT_FDBRO_DATE"]);       //送帶轉檔申請日期
                obj.SHOW_FDBRO_DATE = sqlResult[i]["WANT_FDBRO_DATE"];                      //送帶轉檔申請日期(顯示)
                obj.FSBRO_BY = sqlResult[i]["FSBRO_BY"];                                    //送帶轉檔申請者
                obj.FSBRO_BY_NAME = sqlResult[i]["FSBRO_BY_NAME"];                          //送帶轉檔申請者名稱
                obj.FSMEMO = sqlResult[i]["FSMEMO"];                                        //20120801冠宇新增回傳的FSMEMO欄位
                obj.FCCHECK_STATUS = sqlResult[i]["FCCHECK_STATUS"];                        //審核狀態

                obj.FCCHECK_STATUS_NAME = MAMFunctions.CheckStatus_SendBro(sqlResult[i]["FCCHECK_STATUS"]);//狀態名稱

                obj.HD_MC_Status = sqlResult[i]["HD_MC_Status"];


                obj.FSCHECK_BY = sqlResult[i]["FSCHECK_BY"];                                //審核者
                obj.FSCHECK_BY_NAME = sqlResult[i]["FSCHECK_BY_NAME"];                      //審核者名稱
                obj.FCCHANGE = sqlResult[i]["FCCHANGE"];                                    //置換

                obj.FSSIGNED_BY = sqlResult[i]["FSSIGNED_BY"];
                obj.FSSIGNED_BY_NAME = sqlResult[i]["FSSIGNED_BY_NAME"];
                obj.FDSIGNED_DATE = sqlResult[i]["FDSIGNED_DATE"];

                obj.FSNOTE = ""; //GetTBLOG_VIDEO_BYBROID(sqlResult[i]["FSBRO_ID"]);        //送帶轉檔內容(此方法會需要許多時間，導致資料下載等待很久，因此先不使用)
                obj.FSCHANNELID = sqlResult[i]["FSCHANNELID"];                              //頻道別
                obj.FSPRINTID = sqlResult[i]["FSPRINTID"].Trim();                           //列印編號
                obj.FSREJECT = sqlResult[i]["FSREJECT"].Trim();                             //片庫駁回原因

                obj.FSCREATED_BY = sqlResult[i]["FSCREATED_BY"];                                        //建檔者
                obj.FSCREATED_BY_NAME = sqlResult[i]["FSCREATED_BY_NAME"];                              //建檔者(顯示)
                obj.FSCREATED_BY_EMAIL = sqlResult[i]["FSCREATED_BY_EMAIL"];                            //建檔者EMAIL
                obj.FDCREATED_DATE = Convert.ToDateTime(sqlResult[i]["WANT_FDCREATEDDATE"]);            //建檔日期
                //obj.SHOW_FDCREATED_DATE = MAMFunctions.compareDate(sqlResult[i]["WANT_FDCREATEDDATE"]); //建檔日期(顯示)
                obj.SHOW_FDCREATED_DATE = sqlResult[i]["FDCREATED_DATE"];
                obj.FSUPDATED_BY = sqlResult[i]["FSUPDATED_BY"];                                        //修改者
                obj.FSUPDATED_BY_NAME = sqlResult[i]["FSUPDATED_BY_NAME"];                              //修改者(顯示)
                obj.FDUPDATED_DATE = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                obj.SHOW_FDUPDATED_DATE = sqlResult[i]["FDUPDATED_DATE"];
                obj.FSTRAN_BY = sqlResult[i]["FSTRAN_BY"];
                obj.FDTRAN_DATE = sqlResult[i]["FDTRAN_DATE"];
                // --------------------------------
                returnData.Add(obj);
            }

            return returnData;
        }

        #endregion

        #region 送帶轉檔檔 ( TBBROADCAST )

        //解析資料庫回來的Dictionary，轉存成Class
        private List<Class_BROADCAST> Transfer_BRO(List<Dictionary<string, string>> sqlResult)
        {
            List<Class_BROADCAST> returnData = new List<Class_BROADCAST>();

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_BROADCAST obj = new Class_BROADCAST();
                //obj.FSFILE_NO = sqlResult[i]["FSFILE_NO"];
                //obj.FSVIDEO_PROG = sqlResult[i]["FSVIDEO_PROG"];//Add 2015-10-02 by Jarvis
                // --------------------------------    
                obj.FSBRO_ID = sqlResult[i]["FSBRO_ID"];                                    //送帶轉檔單號
                obj.FSBRO_TYPE = sqlResult[i]["FSBRO_TYPE"];                                //類型
                obj.FSBRO_TYPE_NAME = MAMFunctions.CheckType(sqlResult[i]["FSBRO_TYPE"]);   //類型名稱
                obj.FSID = sqlResult[i]["FSID"];                                            //編碼
                obj.FSID_NAME = sqlResult[i]["FSPROG_ID_NAME"];                             //節目或宣傳帶名稱
                obj.FNEPISODE = Convert.ToInt16(sqlResult[i]["FNEPISODE"]);                 //集別
                obj.SHOW_FNEPISODE = MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]); //集別(顯示)
                obj.FNEPISODE_NAME = sqlResult[i]["FNEPISODE_NAME"];                        //集別名稱
                obj.FDBRO_DATE = Convert.ToDateTime(sqlResult[i]["WANT_FDBRO_DATE"]);       //送帶轉檔申請日期
                obj.SHOW_FDBRO_DATE = sqlResult[i]["WANT_FDBRO_DATE"];                      //送帶轉檔申請日期(顯示)
                obj.FSBRO_BY = sqlResult[i]["FSBRO_BY"];                                    //送帶轉檔申請者
                obj.FSBRO_BY_NAME = sqlResult[i]["FSBRO_BY_NAME"];                          //送帶轉檔申請者名稱
                obj.FSMEMO = sqlResult[i]["FSMEMO"];                                        //20120801冠宇新增回傳的FSMEMO欄位
                obj.FCCHECK_STATUS = sqlResult[i]["FCCHECK_STATUS"];                        //審核狀態
                obj.FCCHECK_STATUS_NAME = MAMFunctions.CheckStatusReport(sqlResult[i]["FCCHECK_STATUS"]);//狀態名稱
                obj.FSCHECK_BY = sqlResult[i]["FSCHECK_BY"];                                //審核者
                obj.FSCHECK_BY_NAME = sqlResult[i]["FSCHECK_BY_NAME"];                      //審核者名稱
                obj.FCCHANGE = sqlResult[i]["FCCHANGE"];                                    //置換

                obj.FSSIGNED_BY = sqlResult[i]["FSSIGNED_BY"];
                obj.FSSIGNED_BY_NAME = sqlResult[i]["FSSIGNED_BY_NAME"];
                obj.FDSIGNED_DATE = sqlResult[i]["FDSIGNED_DATE"];

                obj.FSNOTE = ""; //GetTBLOG_VIDEO_BYBROID(sqlResult[i]["FSBRO_ID"]);        //送帶轉檔內容(此方法會需要許多時間，導致資料下載等待很久，因此先不使用)
                obj.FSCHANNELID = sqlResult[i]["FSCHANNELID"];                              //頻道別
                obj.FSPRINTID = sqlResult[i]["FSPRINTID"].Trim();                           //列印編號
                obj.FSREJECT = sqlResult[i]["FSREJECT"].Trim();                             //片庫駁回原因

                obj.FSCREATED_BY = sqlResult[i]["FSCREATED_BY"];                                        //建檔者
                obj.FSCREATED_BY_NAME = sqlResult[i]["FSCREATED_BY_NAME"];                              //建檔者(顯示)
                obj.FSCREATED_BY_EMAIL = sqlResult[i]["FSCREATED_BY_EMAIL"];                            //建檔者EMAIL
                obj.FDCREATED_DATE = Convert.ToDateTime(sqlResult[i]["WANT_FDCREATEDDATE"]);            //建檔日期
                //obj.SHOW_FDCREATED_DATE = MAMFunctions.compareDate(sqlResult[i]["WANT_FDCREATEDDATE"]); //建檔日期(顯示)
                obj.SHOW_FDCREATED_DATE = sqlResult[i]["FDCREATED_DATE"];
                obj.FSUPDATED_BY = sqlResult[i]["FSUPDATED_BY"];                                        //修改者
                obj.FSUPDATED_BY_NAME = sqlResult[i]["FSUPDATED_BY_NAME"];                              //修改者(顯示)

                obj.FSTRAN_BY = sqlResult[i]["FSTRAN_BY"];
                obj.FDTRAN_DATE = sqlResult[i]["FDTRAN_DATE"];
                // --------------------------------
                returnData.Add(obj);
            }

            return returnData;
        }

        /// <summary>
        /// 20120801冠宇新增用來更新TBBROADCAST FSMEMO欄位的WS
        /// SP_U_TBBROADCAST_FSMEMO
        /// </summary>
        /// <param name="strBRO_ID">要更新的資料ID</param>
        /// <param name="strBRO_FSMEMO">MEMO資料</param>
        /// <param name="strUSER_ID">修改者ID</param>
        /// <returns>true:成功 false:失敗</returns>
        [WebMethod]
        public bool UPDATE_BROADCAST_FSMEMO(string strBRO_ID, string strBRO_FSMEMO, string strUSER_ID)
        {
            bool rtnResult = true;
            Dictionary<string, string> sqlParameter = new Dictionary<string, string>();
            sqlParameter.Add("FSBRO_ID", strBRO_ID);
            sqlParameter.Add("FSMEMO", strBRO_FSMEMO);
            sqlParameter.Add("FSUPDATED_BY", strUSER_ID);
            rtnResult = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBBROADCAST_FSMEMO", sqlParameter, strUSER_ID);
            return rtnResult;
        }

        /// <summary>
        /// 新增送帶轉檔的WS by智軒
        /// 冠宇20120801修改加入FSMEMO欄位
        /// STT100_01.xaml.cs有用到
        /// </summary>
        /// <param name="obj">要傳入新增TBBROADCAST資料表的物件</param>
        /// <returns>true:成功 false:失敗</returns>
        [WebMethod]
        public Boolean INSERT_BROADCAST(Class_BROADCAST obj)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSBRO_ID", obj.FSBRO_ID);
            source.Add("FSBRO_TYPE", obj.FSBRO_TYPE);
            source.Add("FSID", obj.FSID);
            source.Add("FNEPISODE", obj.FNEPISODE.ToString());
            if (string.IsNullOrEmpty(obj.FSMEMO))
            {
                obj.FSMEMO = string.Empty;
            }
            source.Add("FSMEMO", obj.FSMEMO.ToString().Trim()); //冠宇20120801加入的欄位
            if (obj.FDBRO_DATE == new DateTime(1900, 1, 1))
                source.Add("FDBRO_DATE", "");
            else
                source.Add("FDBRO_DATE", obj.FDBRO_DATE.ToString("yyyy/MM/dd"));

            source.Add("FSBRO_BY", obj.FSBRO_BY);

            source.Add("FCCHECK_STATUS", obj.FCCHECK_STATUS);
            source.Add("FSCHECK_BY", obj.FSCHECK_BY);
            source.Add("FCCHANGE", obj.FCCHANGE);

            if (obj.FSPRINTID == null)
                source.Add("FSPRINTID", "");
            else
                source.Add("FSPRINTID", obj.FSPRINTID);

            if (obj.FSREJECT == null)
                source.Add("FSREJECT", "");
            else
                source.Add("FSREJECT", obj.FSREJECT);

            source.Add("FSCREATED_BY", obj.FSCREATED_BY);
            source.Add("FSUPDATED_BY", obj.FSUPDATED_BY);


            if (MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBBROADCAST", source, obj.FSUPDATED_BY))
            {
                PrepareSendBroadcastNotify(obj, null);

                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 不跑Flow新增送帶轉檔單 by Jarvis20130624
        /// </summary>
        /// <param name="obj">要傳入新增TBBROADCAST資料表的物件</param>
        /// <returns>true:成功 false:失敗</returns>
        [WebMethod]
        public Boolean INSERT_BROADCAST_WITHOUT_FLOW(Class_BROADCAST obj)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSBRO_ID", obj.FSBRO_ID);
            source.Add("FSBRO_TYPE", obj.FSBRO_TYPE);
            source.Add("FSID", obj.FSID);
            source.Add("FNEPISODE", obj.FNEPISODE.ToString());
            if (string.IsNullOrEmpty(obj.FSMEMO))
            {
                obj.FSMEMO = string.Empty;
            }
            source.Add("FSMEMO", obj.FSMEMO.ToString().Trim()); //冠宇20120801加入的欄位
            if (obj.FDBRO_DATE == new DateTime(1900, 1, 1))
                source.Add("FDBRO_DATE", "");
            else
                source.Add("FDBRO_DATE", obj.FDBRO_DATE.ToString("yyyy/MM/dd"));

            source.Add("FSBRO_BY", obj.FSBRO_BY);

            source.Add("FCCHECK_STATUS", obj.FCCHECK_STATUS);
            source.Add("FSCHECK_BY", obj.FSCHECK_BY);
            source.Add("FCCHANGE", obj.FCCHANGE);

            if (obj.FSPRINTID == null)
                source.Add("FSPRINTID", "");
            else
                source.Add("FSPRINTID", obj.FSPRINTID);

            if (obj.FSREJECT == null)
                source.Add("FSREJECT", "");
            else
                source.Add("FSREJECT", obj.FSREJECT);

            source.Add("FSCREATED_BY", obj.FSCREATED_BY);
            source.Add("FSUPDATED_BY", obj.FSUPDATED_BY);

            source.Add("FSSIGNED_BY", obj.FSSIGNED_BY);

            if (MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBBROADCAST_WITHOUT_FLOW", source, obj.FSUPDATED_BY))
            {
                PrepareSendBroadcastNotify(obj, null);
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// 用來複製TC時用的 會一次寫入送帶轉檔的所有資料 包括TBBROADCAST、TBLOG_VIDEO、TBLOG_VIDEO_SEG
        /// Kyle 2012/10/04 made
        /// </summary>
        /// <param name="obj">用來寫入TBBROADCAST的資料</param>
        /// <param name="iniVideoList">用來傳遞包含要寫入TBLOG_VIDEO以及TBLOG_VIDEO_SEG的資料</param>
        /// <returns>true:成功 false:失敗</returns>
        [WebMethod]
        public bool INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLIST(Class_BROADCAST obj, List<Class_LOG_VIDEO> iniVideoList)
        {
            bool rtnResult = true;
            #region 寫入資料表TBBROADCAST
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSBRO_ID", obj.FSBRO_ID);
            source.Add("FSBRO_TYPE", obj.FSBRO_TYPE);
            source.Add("FSID", obj.FSID);
            source.Add("FNEPISODE", obj.FNEPISODE.ToString());
            if (string.IsNullOrEmpty(obj.FSMEMO))
            {
                obj.FSMEMO = string.Empty;
            }
            source.Add("FSMEMO", obj.FSMEMO.ToString().Trim()); //冠宇20120801加入的欄位
            if (obj.FDBRO_DATE == new DateTime(1900, 1, 1))
                source.Add("FDBRO_DATE", "");
            else
                source.Add("FDBRO_DATE", obj.FDBRO_DATE.ToString("yyyy/MM/dd"));

            source.Add("FSBRO_BY", obj.FSBRO_BY);

            source.Add("FCCHECK_STATUS", obj.FCCHECK_STATUS);
            source.Add("FSCHECK_BY", obj.FSCHECK_BY);
            source.Add("FCCHANGE", obj.FCCHANGE);

            if (obj.FSPRINTID == null)
                source.Add("FSPRINTID", "");
            else
                source.Add("FSPRINTID", obj.FSPRINTID);

            if (obj.FSREJECT == null)
                source.Add("FSREJECT", "");
            else
                source.Add("FSREJECT", obj.FSREJECT);

            if (obj.FSSIGNED_BY != null)//新增 by Jarvis20130808
            {
                source.Add("FSSIGNED_BY", obj.FSSIGNED_BY);
            }
            else
            {
                source.Add("FSSIGNED_BY", "");
            }

            source.Add("FSCREATED_BY", obj.FSCREATED_BY);
            source.Add("FSUPDATED_BY", obj.FSUPDATED_BY);
            //修改成呼叫SP_I_TBBROADCAST_WITHOUT_FLOW by Jarvis20130808
            rtnResult = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBBROADCAST_WITHOUT_FLOW", source, obj.FSUPDATED_BY);

            //rtnResult = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBBROADCAST", source, obj.FSUPDATED_BY);
            if (rtnResult == false)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("快跟冠宇說:DataSource.WSBROADCAST.INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLIST", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "一次動作送帶轉檔寫入TBBROADCAST資料表發生錯誤");
                return rtnResult;
            }

            PrepareSendBroadcastNotify(obj, iniVideoList[0]);


            #endregion
            #region 寫入資料表TBLOG_VIDEO
            foreach (var item in iniVideoList)
            {
                rtnResult = INSERT_LOG_VIDEO(item);

                if (rtnResult == false)
                {
                    MAM_PTS_DLL.Log.AppendTrackingLog("快跟冠宇說:DataSource.WSBROADCAST.INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLIST", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "一次動作送帶轉檔寫入TBLOG_VIDEO資料表發生錯誤");
                    return rtnResult;
                }

                #region 寫入TBLOG_VIDEO_SEG
                rtnResult = INSERT_TBLOG_VIDEO_SEG(item.VideoListSeg);
                if (rtnResult == false)
                {
                    MAM_PTS_DLL.Log.AppendTrackingLog("快跟冠宇說:DataSource.WSBROADCAST.INSERT_BROADCAST_AT_ONCE_WITH_VIDEOLIST", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "一次動作送帶轉檔寫入TBLOG_VIDEO_SEG資料表發生錯誤");
                    return rtnResult;
                }
                #endregion

            }
            #endregion

            return rtnResult;
        }

        [WebMethod]
        public bool Delete_BROADCAST_AT_ONCE(string bro_id, string user_id)
        {
            bool rtnResult = true;
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSBRO_ID", bro_id);

            rtnResult = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_TBBROADCAST_AND_VIDEO_SEG", source, user_id);

            return rtnResult;
        }

        //修改送帶轉檔檔的列印編號
        [WebMethod]
        public Boolean UPDATE_BROADCAST_PRINTID(string strBRO_ID, string strPrintID, string strUpdated_BY)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSBRO_ID", strBRO_ID.Trim());
            source.Add("FSPRINTID", strPrintID.Trim());

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBBROADCAST_PRINTID", source, strUpdated_BY);
        }

        //送帶轉檔檔檔的所有資料
        [WebMethod]
        public List<Class_BROADCAST> fnGetTBBROADCAST_ALL()
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBBROADCAST_ALL", sqlParameters, out sqlResult))
                return new List<Class_BROADCAST>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_BRO(sqlResult);
        }

        //送帶轉檔檔檔的所有資料_BY新增者
        [WebMethod]
        public List<Class_BROADCAST> fnGetTBBROADCAST_ALL_BYUSERID(string strUSERID, DateTime dtStart, DateTime dtEnd)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSCREATED_BY", strUSERID);
            sqlParameters.Add("FDSTART", dtStart.ToShortDateString());
            sqlParameters.Add("FDEND", dtEnd.ToShortDateString());

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBBROADCAST_ALL_BYUSERID", sqlParameters, out sqlResult))
                return new List<Class_BROADCAST>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_BRO(sqlResult);
        }

        [WebMethod]
        public List<Class_BROADCAST> fnGetTBBROADCAST_BROADCAST(string strUSERID, string isChange, DateTime dtStart, DateTime dtEnd)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSCREATED_BY", strUSERID);
            sqlParameters.Add("FDSTART", dtStart.ToString("yyyy-MM-dd"));
            sqlParameters.Add("FDEND", dtEnd.ToString("yyyy-MM-dd"));
            sqlParameters.Add("FCCHANGE", isChange);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBBROADCAST_BROADCAST", sqlParameters, out sqlResult))
                return new List<Class_BROADCAST>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_BRO_HD(sqlResult);
        }

        [WebMethod]
        public List<Class_BROADCAST> fnGetTBBROADCAST_OtherVideo(string bro_id, string bro_type, string fsid, string episode, string beginDate, string endDate, string strUSERID)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSCREATED_BY", strUSERID);

            sqlParameters.Add("FSBRO_ID", bro_id);
            sqlParameters.Add("FSBRO_TYPE", bro_type);
            sqlParameters.Add("FSID", fsid);
            sqlParameters.Add("FNEPISODE", episode);
            sqlParameters.Add("SDate", beginDate);
            sqlParameters.Add("EDate", endDate);


            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBBROADCAST_OtherVideo", sqlParameters, out sqlResult))
                return new List<Class_BROADCAST>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_BRO(sqlResult);
        }

        //送帶轉檔置換檔的所有資料_BY新增者
        [WebMethod]
        public List<Class_BROADCAST> fnGetTBBROADCAST_ALL_BYUSERID_CHANGE(string strUSERID, DateTime dtStart, DateTime dtEnd)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSCREATED_BY", strUSERID);
            sqlParameters.Add("FDSTART", dtStart.ToShortDateString());
            sqlParameters.Add("FDEND", dtEnd.ToShortDateString());

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBBROADCAST_ALL_BYUSERID_CHANGE", sqlParameters, out sqlResult))
                return new List<Class_BROADCAST>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_BRO(sqlResult);
        }

        //送帶轉檔檔檔的所有資料_BY狀態
        [WebMethod]
        public List<Class_BROADCAST> fnGetTBBROADCAST_ALL_BYSTATUS(string strStatus, string strSDate, string strEDate)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FCCHECK_STATUS", strStatus);
            sqlParameters.Add("SDate", strSDate);
            sqlParameters.Add("EDate", strEDate);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBBROADCAST_ALL_BYSTATUS", sqlParameters, out sqlResult))
                return new List<Class_BROADCAST>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_BRO(sqlResult);
        }

        //送帶轉檔檔檔的所有資料_BY狀態
        [WebMethod]
        public List<Class_BROADCAST> fnGetTBBROADCAST_ALL_BYSTATUS_ADVANCE(string strStatus, string strBRO_ID, string strBRO_TYPE, string strID, string strEPISODE, string strCREATED_BY, DateTime dtStart, DateTime dtEnd)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FCCHECK_STATUS", strStatus);
            sqlParameters.Add("FSBRO_ID", strBRO_ID);
            sqlParameters.Add("FSBRO_TYPE", strBRO_TYPE);
            sqlParameters.Add("FSID", strID);
            sqlParameters.Add("FNEPISODE", strEPISODE);
            sqlParameters.Add("FSCREATED_BY", strCREATED_BY);
            sqlParameters.Add("SDate", dtStart.ToShortDateString());
            sqlParameters.Add("EDate", dtEnd.ToShortDateString());

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBBROADCAST_ALL_BYSTATUS_ADVANCE", sqlParameters, out sqlResult))
                return new List<Class_BROADCAST>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_BRO(sqlResult);
        }

        //送帶轉檔檔檔的所有資料_BY狀態_送帶轉檔查詢專用
        [WebMethod]
        public List<Class_BROADCAST> fnGetTBBROADCAST_ALL_BYSTATUS_ADVANCE_STT(string strStatus, string strBRO_ID, string strBRO_TYPE, string strID, string strEPISODE, string strCREATED_BY, DateTime dtStart, DateTime dtEnd)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FCCHECK_STATUS", strStatus);
            sqlParameters.Add("FSBRO_ID", strBRO_ID);
            sqlParameters.Add("FSBRO_TYPE", strBRO_TYPE);
            sqlParameters.Add("FSID", strID);
            sqlParameters.Add("FNEPISODE", strEPISODE);
            sqlParameters.Add("FSCREATED_BY", strCREATED_BY);
            sqlParameters.Add("SDate", dtStart.ToShortDateString());
            sqlParameters.Add("EDate", dtEnd.ToShortDateString());

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBBROADCAST_ALL_BYSTATUS_ADVANCE_STT", sqlParameters, out sqlResult))
                return new List<Class_BROADCAST>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_BRO(sqlResult);
        }

        //送帶轉檔置換單的所有資料_BY狀態_送帶轉檔置換查詢專用 by Jarvis20130507
        [WebMethod]
        public List<Class_BROADCAST> fnGetTBBROADCAST_Replacement_BYSTATUS_ADVANCE_STT(string strStatus, string strBRO_ID, string strBRO_TYPE, string strID, string strEPISODE, string strCREATED_BY, DateTime dtStart, DateTime dtEnd)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            //sqlParameters.Add("FCCHECK_STATUS", strStatus);
            sqlParameters.Add("FSBRO_ID", strBRO_ID);
            sqlParameters.Add("FSBRO_TYPE", strBRO_TYPE);
            sqlParameters.Add("FSID", strID);
            sqlParameters.Add("FNEPISODE", strEPISODE);
            sqlParameters.Add("FSCREATED_BY", strCREATED_BY);
            sqlParameters.Add("SDate", dtStart.ToShortDateString());
            sqlParameters.Add("EDate", dtEnd.ToShortDateString());

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBBROADCAST_Replacement_BYSTATUS_ADVANCE_STT", sqlParameters, out sqlResult))
                return new List<Class_BROADCAST>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_BRO(sqlResult);
        }

        //尋找所有已抽單的送帶轉檔單單號 by Jarvis20130607
        [WebMethod]
        public List<string> fnGetTBBROADCAST_Detached_ALL()
        {
            List<string> detachList = new List<string>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;


            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBBROADCAST_Detached_ALL", sqlParameters, out sqlResult))
                return detachList;    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            for (int i = 0; i < sqlResult.Count; i++)
            {
                foreach (string key in sqlResult[i].Keys)
                {
                    detachList.Add(sqlResult[i][key]);
                }
            }



            return detachList;


        }

        //根據單號查詢該筆送帶轉檔單目前的狀態 by Jarvis20130523
        [WebMethod]
        public string fnQueryTBBROADCAST_STATUS_BY_BROID(string broID)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSBRO_ID", broID);


            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBBROADCAST_STATUS_BY_BROID", sqlParameters, out sqlResult))
                return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL


            return sqlResult[0]["FCCHECK_STATUS"];
        }

        //送帶轉檔檔檔的所有資料_BY轉檔(狀態為G-待轉檔以及狀態為I-已轉檔，都要找出來)
        [WebMethod]
        public List<Class_BROADCAST> fnGetTBBROADCAST_ALL_BYINGEST(string strSDate, string strEDate)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("SDate", strSDate);
            sqlParameters.Add("EDate", strEDate);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBBROADCAST_ALL_BYINGEST", sqlParameters, out sqlResult))
                return new List<Class_BROADCAST>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_BRO(sqlResult);
        }

        //送帶轉檔檔檔的所有資料_BY進階查詢轉檔(狀態為G-待轉檔以及狀態為I-已轉檔，都要找出來)
        [WebMethod]
        public List<Class_BROADCAST> fnGetTBBROADCAST_ALL_BYINGEST_ADVANCE(string strBRO_ID, string strBRO_TYPE, string strID, string strEPISODE, string strCREATED_BY, DateTime dtStart, DateTime dtEnd)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSBRO_ID", strBRO_ID);
            sqlParameters.Add("FSBRO_TYPE", strBRO_TYPE);
            sqlParameters.Add("FSID", strID);
            sqlParameters.Add("FNEPISODE", strEPISODE);
            sqlParameters.Add("FSCREATED_BY", strCREATED_BY);
            sqlParameters.Add("SDate", dtStart.ToShortDateString());
            sqlParameters.Add("EDate", dtEnd.ToShortDateString());

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBBROADCAST_ALL_BYINGEST_ADVANCE", sqlParameters, out sqlResult))
                return new List<Class_BROADCAST>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_BRO(sqlResult);
        }

        [WebMethod]
        public List<Class_BROADCAST> fnGetTBBROADCAST_ADVANCE_FOR_SendBroadcast(string strStatus, string strBRO_ID, string strBRO_TYPE, string strID, string strEPISODE, string isChange, string strCREATED_BY, DateTime dtStart, DateTime dtEnd)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FCCHECK_STATUS", strStatus);
            sqlParameters.Add("FSBRO_ID", strBRO_ID);
            sqlParameters.Add("FSBRO_TYPE", strBRO_TYPE);
            sqlParameters.Add("FSID", strID);
            sqlParameters.Add("FNEPISODE", strEPISODE);
            sqlParameters.Add("FCCHANGE", isChange);
            sqlParameters.Add("FSCREATED_BY", strCREATED_BY);
            sqlParameters.Add("SDate", dtStart.ToString("yyyy-MM-dd"));
            sqlParameters.Add("EDate", dtEnd.ToString("yyyy-MM-dd"));

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBBROADCAST_ADVANCE_FOR_SendBroadcast", sqlParameters, out sqlResult))
                return new List<Class_BROADCAST>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_BRO_HD(sqlResult);
        }

        //送帶轉檔檔檔的所有資料_BY轉檔(狀態為G-待轉檔以及狀態為I-已轉檔，都要找出來)，並且所有的檔案都不是未轉檔
        [WebMethod]
        public List<Class_BROADCAST> fnGetTBBROADCAST_ALL_BYINGEST_COMPLETE(string strSDate, string strEDate)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("SDate", strSDate);
            sqlParameters.Add("EDate", strEDate);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBBROADCAST_ALL_BYINGEST_COMPLETE", sqlParameters, out sqlResult))
                return new List<Class_BROADCAST>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_BRO(sqlResult);
        }

        //送帶轉檔檔檔的所有資料_BY轉檔(狀態為G-待轉檔以及狀態為I-已轉檔，都要找出來)，並且所有的檔案都是轉檔失敗的
        [WebMethod]
        public List<Class_BROADCAST> fnGetTBBROADCAST_ALL_BYINGEST_FAIL(string strSDate, string strEDate)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("SDate", strSDate);
            sqlParameters.Add("EDate", strEDate);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBBROADCAST_ALL_BYINGEST_FAIL", sqlParameters, out sqlResult))
                return new List<Class_BROADCAST>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_BRO(sqlResult);
        }

        //查詢送帶轉檔檔是否有超過一筆的列印資料
        [WebMethod]
        public Boolean QUERY_TBBROADCAST_PRINT_CHECK(string strPrintID)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPRINTID", strPrintID);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBBROADCAST_PRINTID_CHECK", source, out sqlResult))
                return false;    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count > 0 && sqlResult[0]["dbCount"] != "1")
                return true;
            else
                return false;
        }

        //用送帶轉檔單號取得入庫備註
        [WebMethod]
        public string GetTBLOG_VIDEO_BYBROID(string strBroID)
        {
            string strReturn = "";
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSBRO_ID", strBroID);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_BY_BROID", sqlParameters, out sqlResult))
                return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            m_CodeDataFILE_TYPE = MAMFunctions.Transfer_Class_CODE_TBFILE_TYPE("SP_Q_TBBROADCAST_CODE_TBFILE_TYPE");

            for (int i = 0; i < sqlResult.Count; i++)
            {
                strReturn = strReturn + compareCode_Code(sqlResult[i]["FSARC_TYPE"], "TBFILE_TYPE") + "、"; //入庫檔案類型
            }

            if (strReturn.Trim().Length > 0)
                strReturn = strReturn.Substring(0, strReturn.Trim().Length - 1);

            return strReturn;
        }

        //用送帶轉檔單號取得入庫檔是否有超過一筆
        [WebMethod]
        public Boolean QUERY_TBLOG_VIDEO_COUNT(string strBroID)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSBRO_ID", strBroID);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_BY_BROID", sqlParameters, out sqlResult))
                return false;    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count == 1)
                return true;
            else
                return false;
        }


        //用送帶轉檔單號取得TBLOG_VIDEO檔案資料 by Jarvis20130430
        [WebMethod]
        public List<string> GetTBLOG_VIDEO_FileInfoList_BYBROID(string strBroID)
        {
            List<string> ListFileNO = new List<string>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSBRO_ID", strBroID);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_BY_BROID", sqlParameters, out sqlResult))
                return new List<string>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            //m_CodeDataFILE_TYPE = MAMFunctions.Transfer_Class_CODE_TBFILE_TYPE("SP_Q_TBBROADCAST_CODE_TBFILE_TYPE");

            for (int i = 0; i < sqlResult.Count; i++)
            {
                ListFileNO.Add(sqlResult[i]["FSFILE_NO"] + ";" + MAMFunctions.CheckFileType(sqlResult[i]["FCFILE_STATUS"]));
            }

            return ListFileNO;
        }

        //用送帶轉檔單號取得檔案集合
        [WebMethod]
        public List<string> GetTBLOG_VIDEO_FileNOList_BYBROID(string strBroID)
        {
            List<string> ListFileNO = new List<string>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSBRO_ID", strBroID);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_BY_BROID", sqlParameters, out sqlResult))
                return new List<string>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            //m_CodeDataFILE_TYPE = MAMFunctions.Transfer_Class_CODE_TBFILE_TYPE("SP_Q_TBBROADCAST_CODE_TBFILE_TYPE");

            for (int i = 0; i < sqlResult.Count; i++)
            {
                ListFileNO.Add(sqlResult[i]["FSFILE_NO"]);
            }

            return ListFileNO;
        }

        //用送帶轉檔單號取得檔案編號+置換檔案編號的集合
        [WebMethod]
        public List<string> GetTBLOG_VIDEO_FileNOList_BYBROID_USECHANGEID(string strBroID)
        {
            List<string> ListFileNO = new List<string>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSBRO_ID", strBroID);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_BY_BROID", sqlParameters, out sqlResult))
                return new List<string>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            for (int i = 0; i < sqlResult.Count; i++)
            {
                if (sqlResult[i]["FSCHANGE_FILE_NO"] != "")
                    ListFileNO.Add(sqlResult[i]["FSFILE_NO"] + ";" + sqlResult[i]["FSCHANGE_FILE_NO"]);
            }

            return ListFileNO;
        }

        //刪除入庫影像檔以及入庫影像段落檔
        [WebMethod]
        public Boolean DELETE_TBLOG_VIDEO_SEG_BYFILENO(List<Class_LOG_VIDEO> ListLogVideo)
        {
            foreach (Class_LOG_VIDEO myClass in ListLogVideo)
            {
                Dictionary<string, string> source = new Dictionary<string, string>();
                source.Add("FSFILE_NO", myClass.FSFILE_NO.Trim());

                MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_TBLOG_VIDEO_BYFILENO", source, myClass.FSUPDATED_BY.Trim());
            }
            return true;
        }

        //刪除入庫影像檔以及入庫影像段落檔
        [WebMethod]
        public Boolean DELETE_TBLOG_VIDEO_SEG_BYFILENO_ONE(string strFileNo, string strUpdated_BY)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSFILE_NO", strFileNo.Trim());

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_TBLOG_VIDEO_BYFILENO", source, strUpdated_BY);
        }

        //修改入庫影像段落檔
        [WebMethod]
        public Boolean UPDATE_TBLOG_VIDEO_SEG_BYFILENO(string strFileNo, string strSegID, string strBeg, string strEnd, string strUpdated_BY)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSFILE_NO", strFileNo.Trim());
            source.Add("FNSEG_ID", strSegID.Trim());
            source.Add("FSBEG_TIMECODE", strBeg.Trim());
            source.Add("FSEND_TIMECODE", strEnd.Trim());
            source.Add("FSUPDATED_BY", strUpdated_BY.Trim());

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBLOG_VIDEO_SEG_BYFILEID", source, strUpdated_BY);
        }





        //刪除主控插播帶及主控鏡面
        [WebMethod]
        public Boolean DELETE_TBPROG_D_MONITER_TAPE_BYPROGID_EPISODE(string strProgID, string strEpisode, string strUpdated_BY)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FFSPROG_ID", strProgID);
            source.Add("FNEPISODE", strEpisode);

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_PROG_D_MONITER_TAPE", source, strUpdated_BY);
        }

        //用送帶轉檔單號取得送帶轉檔檔
        [WebMethod]
        public List<Class_BROADCAST> GetTBBROADCAST_BYBROID(string strBroID)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSBRO_ID", strBroID);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBBROADCAST_BYBROID", sqlParameters, out sqlResult))
                return new List<Class_BROADCAST>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_BRO_HD(sqlResult);
        }

        //用送帶轉檔單編號取得製作人列表字串
        [WebMethod]
        public string GetPRODUCER_BYBroID(string strBroID)
        {
            String strPRODUCER = "";
            string strPROType = "";
            string strID = "";
            string strUserID = "";

            List<Class_BROADCAST> ListobjBrocast = new List<Class_BROADCAST>();
            ListobjBrocast = GetTBBROADCAST_BYBROID(strBroID);

            if (ListobjBrocast.Count > 0)
            {
                strPROType = ListobjBrocast[0].FSBRO_TYPE.Trim();
                strID = ListobjBrocast[0].FSID.Trim();
                strUserID = ListobjBrocast[0].FSCREATED_BY.Trim();
            }
            else
                return "";

            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPRO_TYPE", strPROType);
            sqlParameters.Add("FSID", strID);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_PRODUCER", sqlParameters, out sqlResult))
                return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            for (int i = 0; i < sqlResult.Count; i++)
            {
                if (strUserID == sqlResult[i]["FSPRODUCER"])
                    return "me";    //若是提出者就是製作人，就要給流程引擎"me"
                else
                    strPRODUCER = strPRODUCER + sqlResult[i]["FSPRODUCER"] + ",";
            }

            //若是短帶未設定製作人，要再去查詢該短帶是否有綁定節目，若是有綁定節目就要查該節目是否有設定製作人，若有製作人就要給該製作人
            if (strPROType == "P" && sqlResult.Count == 0)
            {
                WSPROMO objWSPROMO = new WSPROMO();
                List<Class_PROMO> Listobj = new List<Class_PROMO>();
                Listobj = objWSPROMO.QUERY_TBPGM_PROMO_BYID(strID);

                if (Listobj.Count > 0)
                {
                    strID = Listobj[0].FSPROG_ID.Trim();
                    strPROType = "G";

                    if (strID != "")
                    {
                        sqlParameters.Clear();
                        sqlParameters.Add("FSPRO_TYPE", strPROType);
                        sqlParameters.Add("FSID", strID);

                        if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_PRODUCER", sqlParameters, out sqlResult))
                            return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL

                        for (int i = 0; i < sqlResult.Count; i++)
                        {
                            if (strUserID == sqlResult[i]["FSPRODUCER"])
                                return "me";    //若是提出者就是製作人，就要給流程引擎"me"
                            else
                                strPRODUCER = strPRODUCER + sqlResult[i]["FSPRODUCER"] + ",";
                        }
                    }
                }
            }

            return strPRODUCER;
        }

        //用類型、編碼、集別、新增者取得送帶轉檔檔
        [WebMethod]
        public List<Class_BROADCAST> GetTBBROADCAST_BYIDEPISODE_BYUSERID(string strBRO_TYPE, string strID, string strEPISODE, string strUserID, DateTime dtStart, DateTime dtEnd)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSBRO_TYPE", strBRO_TYPE);
            sqlParameters.Add("FSID", strID);
            sqlParameters.Add("FNEPISODE", strEPISODE);
            sqlParameters.Add("FSCREATED_BY", strUserID);
            sqlParameters.Add("FDSTART", dtStart.ToShortDateString());
            sqlParameters.Add("FDEND", dtEnd.ToShortDateString());

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBBROADCAST_BYIDEPISODE_BYUSERID", sqlParameters, out sqlResult))
                return new List<Class_BROADCAST>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_BRO(sqlResult);
        }

        //用類型、編碼、集別、新增者取得送帶轉檔置換檔
        [WebMethod]
        public List<Class_BROADCAST> GetTBBROADCAST_BYIDEPISODE_BYUSERID_CHANGE(string strBRO_TYPE, string strID, string strEPISODE, string strUserID, DateTime dtStart, DateTime dtEnd)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSBRO_TYPE", strBRO_TYPE);
            sqlParameters.Add("FSID", strID);
            sqlParameters.Add("FNEPISODE", strEPISODE);
            sqlParameters.Add("FSCREATED_BY", strUserID);
            sqlParameters.Add("FDSTART", dtStart.ToShortDateString());
            sqlParameters.Add("FDEND", dtEnd.ToShortDateString());

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBBROADCAST_BYIDEPISODE_BYUSERID_CHANGE", sqlParameters, out sqlResult))
                return new List<Class_BROADCAST>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_BRO(sqlResult);
        }

        //用類型、編碼、集別取得送帶轉檔置換檔 管理介面專用
        [WebMethod]
        public List<Class_BROADCAST> GetTBBROADCAST_BYIDEPISODE_CHANGE_MANAGEMENT(string strBRO_TYPE, string strID, string strEPISODE, DateTime dtStart, DateTime dtEnd)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSBRO_TYPE", strBRO_TYPE);
            sqlParameters.Add("FSID", strID);
            sqlParameters.Add("FNEPISODE", strEPISODE);
            sqlParameters.Add("FDSTART", dtStart.ToShortDateString());
            sqlParameters.Add("FDEND", dtEnd.ToShortDateString());

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBBROADCAST_BYIDEPISODE_CHANGE_MANAGEMENT", sqlParameters, out sqlResult))
                return new List<Class_BROADCAST>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_BRO(sqlResult);
        }

        /// <summary>
        /// 用建單者取得送帶轉檔置換檔 管理介面專用
        /// </summary>
        /// <param name="strUserID">建單者的ID</param>
        /// <returns></returns>
        [WebMethod]
        public List<Class_BROADCAST> GetTBBROADCAST_BYCREATER_CHANGE_MANAGEMENT(string strUserID)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSCREATED_BY", strUserID);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBBROADCAST_BYCREATER_CHANGE_MANAGEMENT", sqlParameters, out sqlResult))
                return new List<Class_BROADCAST>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_BRO(sqlResult);
        }

        /// <summary>
        /// 用單號取得送帶轉檔單 管理介面專用
        /// </summary>
        /// <param name="FSBRO_ID">送帶轉檔單號</param>
        /// <returns></returns>
        [WebMethod]
        public List<Class_BROADCAST> GetTBBROADCAST_BYFSBROID_CHANGE_MANAGEMENT(string FSBRO_ID)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSBRO_ID", FSBRO_ID);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBBROADCAST_BYFSBROID_CHANGE_MANAGEMENT", sqlParameters, out sqlResult))
                return new List<Class_BROADCAST>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_BRO(sqlResult);
        }

        //用類型、編碼、集別取得送帶轉檔檔
        [WebMethod]
        public List<Class_BROADCAST> GetTBBROADCAST_BYIDEPISODE(string strBRO_TYPE, string strID, string strEPISODE)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSBRO_TYPE", strBRO_TYPE);
            sqlParameters.Add("FSID", strID);
            sqlParameters.Add("FNEPISODE", strEPISODE);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBBROADCAST_BYIDEPISODE", sqlParameters, out sqlResult))
                return new List<Class_BROADCAST>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_BRO(sqlResult);
        }

        //修改送帶轉檔檔的片庫駁回原因
        [WebMethod]
        public Boolean UPDATE_TBBROADCAST_Reject(string strBRO_ID, string strReject, string strUPDATED_BY)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSBRO_ID", strBRO_ID);
            source.Add("FSREJECT", strReject);
            source.Add("FSUPDATED_BY", strUPDATED_BY);

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBBROADCAST_REJECT", source, strUPDATED_BY);
        }

        //核可(Y)/駁回(F)/線上簽收(G) 送帶轉檔檔(預設為N)送帶轉檔置換檔(預設為C)
        /// <summary>
        /// 用於處理送帶轉檔置換單審核通過後改變檔案狀態用
        /// </summary>
        /// <param name="strBRO_ID">單子的ID</param>
        /// <param name="strCHECK_STATUS">單子跑完的結果 C為通過 D為不通過</param>
        /// <param name="strUPDATED_BY">送單者</param>
        /// <returns></returns>
        [WebMethod]
        public Boolean UPDATE_TBBROADCAST_Status(string strBRO_ID, string strCHECK_STATUS, string strUPDATED_BY)
        {
            string strChangeStatus = strCHECK_STATUS.Trim();

            if (strChangeStatus == "C")
                strChangeStatus = "Y";      //置換完成後也要變成Y

            if (strChangeStatus == "D")
                strChangeStatus = "F";      //置換的駁回完成後也要變成F

            Boolean bolStatus = false;
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSBRO_ID", strBRO_ID);
            source.Add("FCCHECK_STATUS", strChangeStatus);
            source.Add("FSUPDATED_BY", strUPDATED_BY);

            bolStatus = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBBROADCAST_STATUS", source, strUPDATED_BY);

            if (bolStatus == false)
                return false;

            if (bolStatus == true && strCHECK_STATUS.Trim() == "Y")
            {
                EMAIL_BROADCAST(strBRO_ID, "Y");         //當送帶轉檔單核可後，EMAIL通知提出成案人員 
                return true;
            }
            else if (bolStatus == true && strCHECK_STATUS.Trim() == "C")
            {
                EMAIL_BROADCAST(strBRO_ID, "C");         //當送帶轉檔置換單核可後，EMAIL通知提出成案人員 
                //UPDATE_TBLOG_VIDEO_REVIEW(strBRO_ID, strUPDATED_BY);//當置換的送帶轉檔單審核完成後，要將MAP裡的VideoID換掉，以及要把被置換的檔案狀態改成D

                List<Class_BROADCAST> TempListBROADCAST = new List<Class_BROADCAST>();
                TempListBROADCAST = GetTBBROADCAST_BYBROID(strBRO_ID);

                if (TempListBROADCAST.Count > 0)
                {
                    GetPlayListOrderAndSendMail(strBRO_ID, TempListBROADCAST[0].FSBRO_TYPE, TempListBROADCAST[0].FSID, TempListBROADCAST[0].FSID_NAME, TempListBROADCAST[0].SHOW_FNEPISODE);             //若是送帶轉檔單為置換時，就要去比對是否有排播過，要去寄EMAIL                    
                }
                return true;
            }
            else if (bolStatus == true && strCHECK_STATUS.Trim() == "F")
            {
                // EMAIL_BROADCAST(strBRO_ID, "F");        //當送帶轉檔單駁回後，EMAIL通知提出成案人員
                UPDATE_TBLOG_VIDEO_Staus_ByBroID(strBRO_ID, "F", strUPDATED_BY);//當送帶轉檔單駁回後，將該送帶轉檔單裡的檔案狀態都改成F(駁回)
                return true;
            }
            else if (bolStatus == true && strCHECK_STATUS.Trim() == "D")
            {
                // EMAIL_BROADCAST(strBRO_ID, "D");        //當送帶轉檔置換單駁回後，EMAIL通知提出成案人員
                UPDATE_TBLOG_VIDEO_Staus_ByBroID(strBRO_ID, "F", strUPDATED_BY);//當送帶轉檔單駁回後，將該送帶轉檔單裡的檔案狀態都改成F(駁回)
                return true;
            }                                                             //2015-12-01 by Jarvis
            else if (bolStatus == true && (strCHECK_STATUS.Trim() == "G" || strCHECK_STATUS.Trim() == "U"))
                return true;
            else if (bolStatus == true && (strCHECK_STATUS.Trim() == "X" || strCHECK_STATUS.Trim() == "E" || strCHECK_STATUS.Trim() == "R")) //送帶轉檔單抽單、送播單抽單、後制退單都要改成X  2015-12-03 by Jarvis
            {
                UPDATE_TBLOG_VIDEO_Staus_ByBroID(strBRO_ID, "X", strUPDATED_BY);//當抽單後，將該送帶轉檔單裡的檔案狀態都改成F(駁回)
                return true;
            }
            else if (bolStatus == true && strCHECK_STATUS.Trim() == "N")
            {
                UPDATE_TBLOG_VIDEO_Staus_ByBroID(strBRO_ID, "B", strUPDATED_BY);//節目單位重新修改完送播單後，回復成初始狀態 2015-12-01 by Jarvis
                return true;
            }
            else if (strCHECK_STATUS.Trim() == "M")
            { 
                //2018.01.18標記上傳將狀態改為M，吳百的程式會撈狀態M的將檔案MOVE到HDUpload狀態改為C
                //反正台員寫的程式就很垃圾，我們的code更垃圾也沒有關係by benwu
                var strConn = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString;

                using (SqlConnection connection = new SqlConnection(strConn))
                {
                    SqlCommand command = new SqlCommand("UPDATE TBLOG_VIDEO_HD_MC SET FCSTATUS = 'M' WHERE FSBRO_ID = @FSBRO_ID", connection);
                    command.Parameters.AddWithValue("@FSBRO_ID", strBRO_ID);
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                }
                 return true;
            }
            else
                return false;
        }

        //更新送帶轉檔單的狀態 by Jarvis20130521
        [WebMethod]
        public Boolean UPDATE_TBBROADCAST_Staus_Directly(string strBRO_ID, string strCHECK_STATUS, string strUPDATED_BY)
        {
            string strChangeStatus = strCHECK_STATUS.Trim();

            Boolean bolStatus = false;
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSBRO_ID", strBRO_ID);
            source.Add("FCCHECK_STATUS", strChangeStatus);
            source.Add("FSUPDATED_BY", strUPDATED_BY);

            bolStatus = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBBROADCAST_STATUS", source, strUPDATED_BY);
            return bolStatus;
        }

        //用送帶轉檔單，查詢這個時間點以後是否有排播，若有就要EMAIL通知
        [WebMethod]
        public void GetPlayListOrderAndSendMail(string strBRO_ID, string strTYPE, string strID, string strID_NAME, string strEpisode)
        {
            List<string> ListFileNO = new List<string>();
            ListFileNO = GetTBLOG_VIDEO_FileNOList_BYBROID_USECHANGEID(strBRO_ID);  //用送帶轉檔單取回檔案編號集合(要被置換的檔案編號)

            for (int i = 0; i < ListFileNO.Count; i++)                  //用每個檔案去查詢
            {
                string strFileNO = "";
                string strChangeFileNO = "";

                char[] delimiterChars = { ';' };
                string[] words = ListFileNO[i].Split(delimiterChars);

                if (words.Length == 2)
                {
                    strFileNO = words[0];
                    strChangeFileNO = words[1];

                    List<Dictionary<string, string>> sqlResult;
                    Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
                    List<Class_returnMail> returnMailsList = new List<Class_returnMail>();          //E-Mail
                    sqlParameters.Add("FSFILE_NO", strChangeFileNO);
                    MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_PLAYLIST_FILEID", sqlParameters, out sqlResult);    //目前的日期以後，排播的詳細記錄(為了要去寄送通知的E-mail)                

                    for (int j = 0; j < sqlResult.Count; j++)   //若是目前的日期以後有排播過，就要寄E-Mail通知
                    {
                        Class_returnMail returnMail = new Class_returnMail();
                        returnMail.FDDATE = sqlResult[j]["FDDATE"];
                        returnMail.FSCHANNEL_ID = sqlResult[j]["FSCHANNEL_ID"];
                        returnMail.FSCHANNEL_NAME = sqlResult[j]["FSCHANNEL_NAME"];
                        returnMail.FSCREATED_BY = sqlResult[j]["FSCREATED_BY"];
                        returnMail.FSEMAIL = sqlResult[j]["FSEMAIL"];
                        returnMail.FSUSER_ChtName = sqlResult[j]["FSUSER_ChtName"];
                        returnMailsList.Add(returnMail);
                    }

                    foreach (Class_returnMail Mail in returnMailsList)
                    {
                        if (Mail.FSEMAIL.ToString() != "")
                        {
                            DateTime dtCheck;
                            StringBuilder msg = new StringBuilder();

                            if (strTYPE.Trim() == "G")
                            {
                                msg.Append("節目名稱：" + strID_NAME);
                                msg.Append(" ， 節目編號：" + strID);

                                if (strEpisode.Trim() != "")
                                    msg.Append(" ， 集別：" + strEpisode);
                            }
                            else if (strTYPE.Trim() == "P")
                            {
                                msg.Append("宣傳帶名稱：" + strID_NAME);
                                msg.Append(" ， 宣傳帶編號：" + strID);
                            }

                            if (DateTime.TryParse(Mail.FDDATE, out dtCheck) == false)
                                msg.Append(" ， 播出日期：" + Mail.FDDATE);
                            else
                                msg.Append(" ， 播出日期：" + Convert.ToDateTime(Mail.FDDATE).ToString("yyyy/MM/dd"));

                            msg.Append(" ， 播出頻道：" + Mail.FSCHANNEL_NAME);
                            msg.Append(" ， 已排播的檔案編號：" + strChangeFileNO + "將被置換");
                            msg.Append(" ， 新的檔案編號：" + strFileNO);
                            MAM_PTS_DLL.Protocol.SendEmail(Mail.FSEMAIL.ToString(), "送帶轉檔檔案置換通知", msg.ToString());//MAIL
                        }
                    }
                }
            }
        }

        public class Class_returnMail   //置換時要去通知使用者的Class
        {
            public string FDDATE;
            public string FSCHANNEL_ID;
            public string FSCHANNEL_NAME;
            public string FSCREATED_BY;
            public string FSUSER_ChtName;
            public string FSEMAIL;
        }

        //寄送EMAIL通知_傳入送帶轉檔單編號、狀態(執行時機為成案單審核通過或不通過後)
        [WebMethod]
        public Boolean EMAIL_BROADCAST(string strBRO_ID, string strSTATUS)
        {
            List<Class_BROADCAST> Listobj = new List<Class_BROADCAST>();
            Class_BROADCAST obj = new Class_BROADCAST();
            string strTitle = "";

            Listobj = GetTBBROADCAST_BYBROID(strBRO_ID);

            if (Listobj.Count > 0)   //透過成案單撈出成案class
                obj = Listobj[0];
            else
                return false;

            string strNAME = obj.FSID_NAME.Trim();
            StringBuilder msg = new StringBuilder();

            if (strSTATUS == "C")
            {
                msg.AppendLine("送帶轉檔置換單編號：" + obj.FSBRO_ID.Trim());
                strTitle = "送帶轉檔置換審核結果通知-";
            }
            else if (strSTATUS == "D")
            {
                msg.AppendLine("送帶轉檔置換單編號：" + obj.FSBRO_ID.Trim());
                strTitle = "送帶轉檔置換審核結果通知-";
            }
            else if (strSTATUS == "Y")
            {
                msg.AppendLine("送帶轉檔單編號：" + obj.FSBRO_ID.Trim());
                strTitle = "送帶轉檔審核結果通知-";
            }
            else if (strSTATUS == "F")
            {
                msg.AppendLine("送帶轉檔單編號：" + obj.FSBRO_ID.Trim());
                strTitle = "送帶轉檔審核結果通知-";
            }

            msg.AppendLine(" , 類型：" + obj.FSBRO_TYPE_NAME.Trim());

            if (obj.FSBRO_TYPE.Trim() == "G")
            {
                msg.AppendLine(" , 節目名稱：" + strNAME);
                msg.AppendLine(" , 節目編號：" + obj.FSID.Trim());

                if (obj.FNEPISODE != 0)
                {
                    msg.AppendLine(" , 集別：" + obj.FNEPISODE);
                }
            }
            else if (obj.FSBRO_TYPE.Trim() == "P")
            {
                msg.AppendLine(" , 宣傳帶名稱：" + strNAME);
                msg.AppendLine(" , 宣傳帶編號：" + obj.FSID.Trim());
            }

            if (strSTATUS.Trim() == "Y")
                msg.AppendLine(" , 審核結果：通過");
            else if (strSTATUS.Trim() == "C")
                msg.AppendLine(" , 審核結果：通過");
            else if (strSTATUS.Trim() == "D")
                msg.AppendLine(" , 審核結果：不通過");
            else if (strSTATUS.Trim() == "F")
                msg.AppendLine(" , 審核結果：不通過");

            if (MAM_PTS_DLL.Protocol.SendEmail(obj.FSCREATED_BY_EMAIL.Trim(), strTitle + strNAME, msg.ToString()))  //MAIL
                return true;
            else
                return false;
        }

        public class Class_returnPlayNote   //播出的訊息驗證
        {
            public Boolean FBSW = false;
            public string FDDATE;
            public string FSPLAYTIME;
            public string FSCHANNEL_NAME;
            public string FSNOTE;
        }

        //用節目編號、節別查詢48小時內是否有排播
        [WebMethod]
        public Class_returnPlayNote GetPROG_PLAY_NOTE(string strProgID, string strEPISODE)
        {
            Class_returnPlayNote PlayNote = new Class_returnPlayNote();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSPROG_ID", strProgID);
            sqlParameters.Add("FNEPISODE", strEPISODE);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPGM_QUEUE_BY_PROGID_48Hour", sqlParameters, out sqlResult))
                return new Class_returnPlayNote();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count > 0)
            {
                PlayNote.FBSW = true;

                if (sqlResult[0]["FDDATE"] != "")
                    PlayNote.FDDATE = sqlResult[0]["FDDATE"];

                if (sqlResult[0]["FSBEG_TIME"] != "")
                    PlayNote.FSPLAYTIME = sqlResult[0]["FSBEG_TIME"];

                if (sqlResult[0]["FSCHANNEL_NAME"] != "")
                    PlayNote.FSCHANNEL_NAME = sqlResult[0]["FSCHANNEL_NAME"];

                if (sqlResult[0]["minusDate"] != "")
                    PlayNote.FSNOTE = sqlResult[0]["minusDate"];

                return PlayNote;
            }
            else
                return new Class_returnPlayNote();
        }

        //用宣傳帶編號查詢48小時內是否有排播
        [WebMethod]
        public Class_returnPlayNote GetPROMO_PLAY_NOTE(string strPromoID)
        {
            Class_returnPlayNote PlayNote = new Class_returnPlayNote();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSPROMO_ID", strPromoID);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPGM_ARR_PROMO_BY_PROMOID_48Hour", sqlParameters, out sqlResult))
                return new Class_returnPlayNote();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count > 0)
            {
                PlayNote.FBSW = true;

                if (sqlResult[0]["FDDATE"] != "")
                    PlayNote.FDDATE = sqlResult[0]["FDDATE"];

                if (sqlResult[0]["FSPLAYTIME"] != "")
                    PlayNote.FSPLAYTIME = sqlResult[0]["FSPLAYTIME"];

                if (sqlResult[0]["FSCHANNEL_NAME"] != "")
                    PlayNote.FSCHANNEL_NAME = sqlResult[0]["FSCHANNEL_NAME"];

                if (sqlResult[0]["minusDate"] != "")
                    PlayNote.FSNOTE = sqlResult[0]["minusDate"];

                return PlayNote;
            }
            else
                return new Class_returnPlayNote();
        }

        //線上簽收時發現為緊急送播，要寫入主控用的資料表-TBLOG_VIDEO_HIGH_PRIORITY
        [WebMethod]
        public Boolean UrgentsTT(List<Class_LOG_VIDEO> ListLogVideo)
        {
            ServicePGM objPGM = new ServicePGM();

            foreach (Class_LOG_VIDEO myClass in ListLogVideo)
            {
                objPGM.InsertHighPriorityVideo(myClass.FSFILE_NO.Trim(), DateTime.Now);
            }

            return true;
        }

        //送帶轉檔單不通過時，該送帶轉檔單裡的檔案狀態要改成F(駁回)，才不會造成無法再置換的問題
        [WebMethod]
        public Boolean UPDATE_TBLOG_VIDEO_Staus_ByBroID(string strBRO_ID, string strSTATUS, string strUPDATED_BY)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSBRO_ID", strBRO_ID);
            sqlParameters.Add("FCFILE_STATUS", strSTATUS);

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBLOG_VIDEO_FILESTATUS_BY_BROID", sqlParameters, strUPDATED_BY);
        }

        #endregion

        #region 節目子集檔 ( TBPROG_D )

        //修改MAM的節目子集檔
        [WebMethod]
        public Boolean UPDATE_TBPROG_D_ND_DURATION(string strProgID, string strEpisode, string strTAPELENGTH, string strNDMEMO, string strUpdated_By)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPROG_ID", strProgID);
            sqlParameters.Add("FNEPISODE", strEpisode);
            sqlParameters.Add("FNTAPELENGTH", strTAPELENGTH);
            sqlParameters.Add("FSNDMEMO", strNDMEMO);

            if (!MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBPROG_D_BRO", sqlParameters, strUpdated_By))
                return false;    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            else
            {
                //原本轉檔時要依據送帶轉檔的段落來更新子集的實際帶長，但是因為使用者反應，目前先關掉此功能
                //更新節目管理系統的節目子集檔實際帶長
                //UPDATE_TBPROG_D_ND_DURATION_PGM(strProgID, strEpisode, strTAPELENGTH, strUpdated_By);
                return true;
            }
        }

        //修改節目管理系統的節目子集檔(先不判斷寫入節目管理系統失敗)
        [WebMethod]
        public Boolean UPDATE_TBPROG_D_ND_DURATION_PGM(string strProgID, string strEpisode, string strTAPELENGTH, string strUpdated_By)
        {
            Boolean bolDB = false;          //寫入節目管理系統資料庫結果
            string strError = "";           //錯誤訊息

            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPROGID", strProgID);
            sqlParameters.Add("FNEPISODE", strEpisode);
            sqlParameters.Add("FNTAPELENGTH", strTAPELENGTH);

            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction_With_DBLink("SP_U_TBPROG_D_BRO_DURATION_EXTERNAL", sqlParameters, out strError, strUpdated_By, true);

            if (bolDB == true)
                return true;
            else
            {
                //寫入節目管理系統錯誤時，要因應的作法(目前只寫進LOG)
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToPGM-UPDATE PROGD DURATION", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "ERROR:" + strError);
                //EMAIL_PROGD_Error(obj, strError, "PGM", "UPDATE");             //寄送Mail
                return false;
            }
        }

        //查詢節目子集檔的2nd資料
        [WebMethod]
        public string Query_TBPROG_D_ND(string strProgID, string strEpisode)
        {
            List<Dictionary<string, string>> sqlResult;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPROG_ID", strProgID);
            sqlParameters.Add("FNEPISODE", strEpisode);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_D", sqlParameters, out sqlResult))
                return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count > 0)
                return sqlResult[0]["FSNDMEMO"];
            else
                return "";
        }

        /// <summary>
        /// 查詢是否已有送帶轉檔單檔案 B:已送單待轉檔(修改原單 TIME CODE 段落) T:已轉檔完成(用置換)
        /// 前端用於STT100_01.xaml.cs
        /// Comment by Kyle 2012/07/27
        /// </summary>
        /// <param name="strProgID">FSID(G:TBPROG_M,節目主檔代號,P:TBPGM_PROMO)</param>
        /// <param name="strEpisode">G:TBPROG_D-->'EPISODE'  P:0(Promo帶都用0帶入)</param>
        /// <param name="strFSARC_TYPE">代碼:如為SD播出帶為001</param>
        /// <returns>若為中文敘述為無法新增送帶轉檔的狀態描述,回傳字串"True"則可新增,回傳字串"Error"則代表查詢SP時發生錯誤</returns>
        [WebMethod]
        public string Query_CheckIsAbleToCreateNewTransformForm(string strProgID, string strEpisode, List<Class_LOG_VIDEO> iniCheckList)
        {
            string rtnResult = "True";
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();

            if (iniCheckList.Where(S => S.FSARC_TYPE == "001").Count() > 0)
            {
                sqlParameters.Add("FSID", strProgID);
                sqlParameters.Add("FNEPISODE", strEpisode);
                sqlParameters.Add("FSARC_TYPE", "001");
                List<Dictionary<string, string>> sqlResult;
                if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_CheckIsAbleToCreateNewTransformForm", sqlParameters, out sqlResult))
                    return "SP Error";    // 發生SQL錯誤時，回傳錯誤字串"Error"，而不是NULL
                for (int i = 0; i < sqlResult.Count; i++)
                {
                    string strFCFILE_STATUS = sqlResult[i]["FCFILE_STATUS"];
                    string strFCCHECK_STATUS = sqlResult[i]["FCCHECK_STATUS"];
                    if (strFCFILE_STATUS == "B" && (strFCCHECK_STATUS == "G" || strFCCHECK_STATUS == "N" || strFCCHECK_STATUS == "Y"))//N:尚未審核 Y:審核通過 G:片庫簽收
                    {
                        rtnResult = "表單編號：" + sqlResult[i]["FSBRO_ID"] + "  (" + sqlResult[i]["FSCREATED_BY"] + ")" + sqlResult[i]["FSUSER_ChtName"] + " 已申請過SD播出帶轉檔,無法再新增一筆送帶轉檔。";
                    }
                    else if (strFCFILE_STATUS == "B" && (strFCCHECK_STATUS == "T"))//FCCHECK_STATUS:T<---由回溯產生了表單不可再新增 
                    {
                        rtnResult = "表單編號：" + sqlResult[i]["FSBRO_ID"] + "  (" + sqlResult[i]["FSCREATED_BY"] + ")" + sqlResult[i]["FSUSER_ChtName"] + " 已申請過SD播出帶回溯轉檔,無法再新增一筆送帶轉檔。";
                    }
                    else if (strFCFILE_STATUS == "T" || strFCFILE_STATUS == "Y")
                    {
                        rtnResult = "播出帶已存在,轉檔完成檔案,請用置換方式取代檔案。";
                    }
                    else if (strFCFILE_STATUS == "R" || strFCFILE_STATUS == "S")    //R:轉檔失敗 S:轉檔中
                        rtnResult = "檔案為轉檔失敗或轉檔中，請重新轉檔，不可新增送帶轉檔單!";
                    else if (strFCFILE_STATUS == "F")    //F:審核失敗
                        rtnResult = "已申請過SD播出帶轉檔,無法再新增一筆送帶轉檔。";
                }
                //return rtnResult;
            }
            if (iniCheckList.Where(O => O.FSARC_TYPE == "002").Count() > 0)
            {
                sqlParameters.Clear();
                sqlParameters.Add("FSID", strProgID);
                sqlParameters.Add("FNEPISODE", strEpisode);
                sqlParameters.Add("FSARC_TYPE", "002");
                List<Dictionary<string, string>> sqlResult;
                if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_CheckIsAbleToCreateNewTransformForm", sqlParameters, out sqlResult))
                    return "SP Error";    // 發生SQL錯誤時，回傳錯誤字串"Error"，而不是NULL
                for (int i = 0; i < sqlResult.Count; i++)
                {
                    string strFCFILE_STATUS = sqlResult[i]["FCFILE_STATUS"];
                    string strFCCHECK_STATUS = sqlResult[i]["FCCHECK_STATUS"];
                    if (strFCFILE_STATUS == "B" && (strFCCHECK_STATUS == "G" || strFCCHECK_STATUS == "N" || strFCCHECK_STATUS == "Y"))//N:尚未審核 Y:審核通過 G:片庫簽收
                    {
                        rtnResult = "(" + sqlResult[i]["FSCREATED_BY"] + ")" + sqlResult[i]["FSUSER_ChtName"] + " 已申請過HD播出帶轉檔,無法再新增一筆送帶轉檔。";
                    }
                    else if (strFCFILE_STATUS == "B" && (strFCCHECK_STATUS == "T"))//FCCHECK_STATUS:T<---由回溯產生了表單不可再新增 
                    {
                        rtnResult = "(" + sqlResult[i]["FSCREATED_BY"] + ")" + sqlResult[i]["FSUSER_ChtName"] + " 已申請過HD播出帶回溯轉檔,無法再新增一筆送帶轉檔。";
                    }
                    else if (strFCFILE_STATUS == "T" || strFCFILE_STATUS == "Y")
                    {
                        rtnResult = "HD播出帶已存在,轉檔完成檔案,請用置換方式取代檔案。";
                    }
                    else if (strFCFILE_STATUS == "R" || strFCFILE_STATUS == "S")    //R:轉檔失敗 S:轉檔中
                        rtnResult = "HD檔案為轉檔失敗或轉檔中，請重新轉檔，不可新增送帶轉檔單!";
                    else if (strFCFILE_STATUS == "F")    //F:審核失敗
                        rtnResult = "已申請過HD播出帶轉檔,無法再新增一筆送帶轉檔。";
                }
                //return rtnResult;
            }
            return rtnResult;
        }

        [WebMethod]
        public string Query_CheckIsAbleToModifyTimeCode()
        {//STY TVLOG_VIDEO FCFileStatus
            string rtnString = "True";
            return rtnString;
        }



        /// <summary>
        /// 查詢節目子集的主控播出提示資料的TBPROG_D_MONITER資料
        /// </summary>
        /// <param name="strProgID"></param>
        /// <param name="strEpisode"></param>
        /// <returns>true:已有提示資料 false:無提示資料</returns>
        [WebMethod]
        public Boolean Query_TBPROG_D_MONITER(string strProgID, string strEpisode)
        {
            List<Dictionary<string, string>> sqlResult;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPROG_ID", strProgID);
            sqlParameters.Add("FNEPISODE", strEpisode);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_D_MONITER_CHECK", sqlParameters, out sqlResult))
                return false;    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count > 0)
                return true;
            else
                return false;
        }

        //查詢節目子集檔的主控播出提示資料
        [WebMethod]
        public Boolean Query_TBPROG_INSERT_TAPE(string strProgID, string strEpisode, List<string> arc_type)
        {
            bool result = false;
            for (int i = 0; i < arc_type.Count; i++)
            {
                List<Dictionary<string, string>> sqlResult;
                Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
                sqlParameters.Add("FSPROG_ID", strProgID);
                sqlParameters.Add("FNEPISODE", strEpisode);
                sqlParameters.Add("FSARC_TYPE", arc_type[i]);

                if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPGM_INSERT_TAPE", sqlParameters, out sqlResult))
                    return false;    // 發生SQL錯誤時，回傳空的東西，而不是NULL

                if (sqlResult.Count > 0)
                    return true; // 一找到就return，不要在等他跑完了 2016-01-04 by Jarvis
            }
            return result;
            //List<Dictionary<string, string>> sqlResult;
            //Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            //sqlParameters.Add("FSPROG_ID", strProgID);
            //sqlParameters.Add("FNEPISODE", strEpisode);

            //if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_D_INSERT_TAPE", sqlParameters, out sqlResult))
            //    return false;    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            //if (sqlResult.Count > 0)
            //    return true;
            //else
            //    return false;
        }

        #endregion



        #region 公用Function


        /// <summary>
        /// 將複製其他節目的 段落資料傳進來處理
        /// 1.取回原本每一筆TBLOG_VIDEO的SEG資料
        /// 2.重新取得FSFILE_NO
        /// </summary>
        /// <param name="strType">G OR P</param>
        /// <param name="strFSID">節目ID</param>
        /// <param name="strEpisode">子集ID</param>
        /// <param name="strUpdated_By">修改者</param>
        /// <param name="strChannelID">頻道別</param>
        /// <param name="strProgName">節目名稱</param>
        /// <param name="strEpisodeName">子集名稱</param>
        /// <param name="BRO_ID">送帶轉檔單號</param>
        /// <param name="iniCLV">所有複製過來的類別</param>
        /// <returns>整理過後的類別</returns>
        [WebMethod]
        public List<Class_LOG_VIDEO> Set_Class_LOG_VIDEO_AND_SEG(string strType, string strFSID, string strEpisode, string strUpdated_By, string strChannelID, string strProgName, string strEpisodeName, string BRO_ID, List<Class_LOG_VIDEO> iniCLV)
        {
            List<Class_LOG_VIDEO> returnCLV = new List<Class_LOG_VIDEO>();
            returnCLV = iniCLV;
            //取得原始SEG資料

            foreach (var item in returnCLV)
            {
                if (item.VideoListSeg.Count == 0)
                {
                    item.VideoListSeg = GetTBLOG_VIDEO_SEG_BYFileID(item.FSFILE_NO);            //**************取回所有SEG從TBLOG_VIDEO_SEG
                }
            }

            foreach (var item in returnCLV)
            {
                #region 更新Class_LOG_VIDEO類別內容
                //接收回來的參數，前者是檔案編號，後者是DIRID
                string strGet = WSMAMFunctions.GetNoRecord_FILEID(strType, strFSID, strEpisode, strUpdated_By, strChannelID, strProgName, strEpisodeName); //*******取新的FILE_NO
                char[] delimiterChars = { ';' };
                string[] words = strGet.Split(delimiterChars);

                if (words.Length == 2)
                {
                    item.FSFILE_NO = words[0];
                    item.FNDIR_ID = Convert.ToInt64(words[1]);
                }
                else if (words.Length == 1)
                {
                    item.FSFILE_NO = words[0];
                    item.FNDIR_ID = 0;
                }
                else
                {
                    item.FSFILE_NO = "";
                    item.FNDIR_ID = 0;
                }
                item.FSSUBJECT_ID = item.FSFILE_NO.Trim().Substring(0, 12);
                item.FSTYPE = strType;
                item.FSID = strFSID;
                item.FNEPISODE = Convert.ToInt16(strEpisode);
                item.FSPLAY = "N";
                item.FSARC_TYPE = item.FSARC_TYPE;  //不變
                item.FSFILE_TYPE_HV = "mxf";        //高解檔名
                if (item.FSARC_TYPE_NAME.Contains("SD"))
                    item.FSFILE_TYPE_LV = "wmv";        //低解檔名
                else if (item.FSARC_TYPE_NAME.Contains("HD"))
                    item.FSFILE_TYPE_LV = "mp4";
                item.FSTITLE = "";
                item.FSDESCRIPTION = "";
                item.FSFILE_SIZE = "";
                item.FCFILE_STATUS = "B";
                item.FCFILE_STATUS_NAME = "複製過來的TC";
                item.FSCHANGE_FILE_NO = "";         //非置換所用的 所以不會有置換檔案名稱

                item.FSOLD_FILE_NAME = "";
                item.FSFILE_PATH_H = "";         //高低解檔案路徑在web services裡取得
                item.FSFILE_PATH_L = "";

                item.FSBRO_ID = BRO_ID;

                item.FSSUPERVISOR = "N";        //複製過來的檔案調用預設為一律不須主管簽核
                item.FSCHANNEL_ID = strChannelID;
                item.FSVIDEO_PROG = QueryVideoID_PROG_STT(item.FSID, item.FNEPISODE.ToString(), item.FSARC_TYPE, false);//取回VIDEO_ID
                item.FSCREATED_BY = strUpdated_By;
                item.FSUPDATED_BY = strUpdated_By;
                #endregion
                if (item.VideoListSeg != null)
                {
                    foreach (var itemSub in item.VideoListSeg)
                    {
                        itemSub.FSFILE_NO = item.FSFILE_NO;
                        itemSub.FSVIDEO_ID = item.FSVIDEO_PROG;
                        itemSub.FCLOW_RES = "N";
                        itemSub.FSCREATED_BY = strUpdated_By;
                        itemSub.FSUPDATED_BY = strUpdated_By;
                    }
                }
            }
            return returnCLV;
        }

        /// <summary>
        /// 將複製其他節目的 段落資料傳進來處理(強制取新的VIDEO_ID版)
        /// 1.取回原本每一筆TBLOG_VIDEO的SEG資料
        /// 2.重新取得FSFILE_NO
        /// </summary>
        /// <param name="strType">G OR P</param>
        /// <param name="strFSID">節目ID</param>
        /// <param name="strEpisode">子集ID</param>
        /// <param name="strUpdated_By">修改者</param>
        /// <param name="strChannelID">頻道別</param>
        /// <param name="strProgName">節目名稱</param>
        /// <param name="strEpisodeName">子集名稱</param>
        /// <param name="BRO_ID">送帶轉檔單號</param>
        /// <param name="iniCLV">所有複製過來的類別</param>
        /// <returns>整理過後的類別</returns>
        [WebMethod]
        public List<Class_LOG_VIDEO> Set_Class_LOG_VIDEO_AND_SEG_NewVideoID(string strType, string strFSID, string strEpisode, string strUpdated_By, string strChannelID, string strProgName, string strEpisodeName, string BRO_ID, List<Class_LOG_VIDEO> iniCLV)
        {
            List<Class_LOG_VIDEO> returnCLV = new List<Class_LOG_VIDEO>();
            returnCLV = iniCLV;
            //取得原始SEG資料

            foreach (var item in returnCLV)
            {
                if (item.VideoListSeg.Count == 0)
                {
                    item.VideoListSeg = GetTBLOG_VIDEO_SEG_BYFileID(item.FSFILE_NO);            //**************取回所有SEG從TBLOG_VIDEO_SEG
                }
            }

            foreach (var item in returnCLV)
            {
                #region 更新Class_LOG_VIDEO類別內容
                //接收回來的參數，前者是檔案編號，後者是DIRID
                string strGet = WSMAMFunctions.GetNoRecord_FILEID(strType, strFSID, strEpisode, strUpdated_By, strChannelID, strProgName, strEpisodeName); //*******取新的FILE_NO
                char[] delimiterChars = { ';' };
                string[] words = strGet.Split(delimiterChars);

                if (words.Length == 2)
                {
                    item.FSFILE_NO = words[0];
                    item.FNDIR_ID = Convert.ToInt64(words[1]);
                }
                else if (words.Length == 1)
                {
                    item.FSFILE_NO = words[0];
                    item.FNDIR_ID = 0;
                }
                else
                {
                    item.FSFILE_NO = "";
                    item.FNDIR_ID = 0;
                }
                item.FSSUBJECT_ID = item.FSFILE_NO.Trim().Substring(0, 12);
                item.FSTYPE = strType;
                item.FSID = strFSID;
                item.FNEPISODE = Convert.ToInt16(strEpisode);
                item.FSPLAY = "N";
                item.FSARC_TYPE = item.FSARC_TYPE;  //不變
                item.FSFILE_TYPE_HV = "mxf";        //高解檔名
                if (item.FSARC_TYPE_NAME.Contains("SD"))
                    item.FSFILE_TYPE_LV = "wmv";        //低解檔名
                else if (item.FSARC_TYPE_NAME.Contains("HD"))
                    item.FSFILE_TYPE_LV = "mp4";
                item.FSTITLE = "";
                item.FSDESCRIPTION = "";
                item.FSFILE_SIZE = "";
                item.FCFILE_STATUS = "B";
                item.FCFILE_STATUS_NAME = "複製過來的TC";
                item.FSCHANGE_FILE_NO = "";         //非置換所用的 所以不會有置換檔案名稱

                item.FSOLD_FILE_NAME = "";
                item.FSFILE_PATH_H = "";         //高低解檔案路徑在web services裡取得
                item.FSFILE_PATH_L = "";

                item.FSBRO_ID = BRO_ID;

                item.FSSUPERVISOR = "N";        //複製過來的檔案調用預設為一律不須主管簽核
                item.FSCHANNEL_ID = strChannelID;
                item.FSVIDEO_PROG = QueryVideoID_PROG_STT(item.FSID, item.FNEPISODE.ToString(), item.FSARC_TYPE, true);//取回VIDEO_ID
                item.FSCREATED_BY = strUpdated_By;
                item.FSUPDATED_BY = strUpdated_By;
                #endregion
                if (item.VideoListSeg != null)
                {
                    foreach (var itemSub in item.VideoListSeg)
                    {
                        itemSub.FSFILE_NO = item.FSFILE_NO;
                        itemSub.FSVIDEO_ID = item.FSVIDEO_PROG;
                        itemSub.FCLOW_RES = "N";
                        itemSub.FSCREATED_BY = strUpdated_By;
                        itemSub.FSUPDATED_BY = strUpdated_By;
                    }
                }
            }
            return returnCLV;
        }

        //取得檔案編號
        [WebMethod()]
        public string fnGetFileID(string strType, string strFSID, string strEpisode, string strUpdated_By, string strChannelID, string strProgName, string strEpisodeName)
        {
            string strFileNO = "";
            strFileNO = WSMAMFunctions.GetNoRecord_FILEID(strType, strFSID, strEpisode, strUpdated_By, strChannelID, strProgName, strEpisodeName);
            return strFileNO;
        }

        //比對代碼檔
        string compareCode_Code(string strID, string strTable)
        {
            for (int i = 0; i < m_CodeDataFILE_TYPE.Count; i++)
            {
                if (m_CodeDataFILE_TYPE[i].ID.ToString().Trim().Equals(strID) && m_CodeDataFILE_TYPE[i].TABLENAME.ToString().Trim().Equals(strTable))
                {
                    return m_CodeDataFILE_TYPE[i].NAME.ToString().Trim();
                }
            }
            return "";
        }

        //比對代碼檔_SD、HD
        string compareCode_CodeSDHD(string strID, string strTable)
        {
            for (int i = 0; i < m_CodeDataFILE_TYPE.Count; i++)
            {
                if (m_CodeDataFILE_TYPE[i].ID.ToString().Trim().Equals(strID) && m_CodeDataFILE_TYPE[i].TABLENAME.ToString().Trim().Equals(strTable))
                {
                    if (m_CodeDataFILE_TYPE[i].FSSPEC.Trim() == "01")
                        return "SD";
                    else if (m_CodeDataFILE_TYPE[i].FSSPEC.Trim() == "02")
                        return "HD";
                    else
                        return "";
                }
            }
            return "";
        }

        //列印送帶轉檔表單
        [WebMethod]
        public string CallREPORT_BRO(string strBroID, string CREATED_BY, string strSessionID)
        {
            String strURL = "";  //要傳回去的URL

            //取列印報表的系統設定檔
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string reportSrv = SettingObj.sysConfig_Read("/ServerConfig/ReportServer");

            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSBRO_ID", strBroID);             //送帶轉檔單編號
            source.Add("QUERY_BY", CREATED_BY);          //查詢者
            source.Add("FSSESSION_ID", strSessionID);    //SessionID

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_R_TBBROADCAST_01_QRY", source, out sqlResult))
                return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL          

            if (sqlResult.Count > 0)
            {
                if (sqlResult[0]["QUERY_KEY"].ToString().StartsWith("ERROR:SESSION_TIMEOUT"))
                    return "錯誤，登入時間過久，請重新登入後才能啟動報表功能！";
                else if (sqlResult[0]["QUERY_KEY"].ToString().StartsWith("ERROR:SESSION_NOTEXIST"))
                    return "錯誤，使用者未正常登入，請重新登入後才能啟動報表功能！";
                else if (sqlResult[0]["QUERY_KEY"].ToString().StartsWith("ERROR"))
                    return "";
                else
                {
                    strURL = reportSrv + "RPT_TBBROADCAST_01&QUERY_KEY=" + sqlResult[0]["QUERY_KEY"] + "&rc:parameters=false&rs:Command=Render";
                    return Server.UrlEncode(strURL);
                }
            }
            else
                return "";
        }

        //列印送帶轉檔明細表單
        [WebMethod]
        public string CallREPORT_BRO_Detail(string strPRINTID, string CREATED_BY, string strSessionID)
        {
            String strURL = "";  //要傳回去的URL

            //取列印報表的系統設定檔
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string reportSrv = SettingObj.sysConfig_Read("/ServerConfig/ReportServer");

            //檔案編號、查詢者、SessionID
            strURL = reportSrv + "RPT_TBBROADCAST_02&FSFILE_NO=" + strPRINTID + "&QUERY_BY=" + CREATED_BY + "&FSSESSION_ID=" + strSessionID + "&rc:parameters=false&rs:Command=Render";
            return Server.UrlEncode(strURL);
        }

        [WebMethod]
        public string CallReport_Archive_Big(string fileno)
        {
            String strURL = "";  //要傳回去的URL
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string reportSrv = SettingObj.sysConfig_Read("/ServerConfig/ReportServer");

            strURL = reportSrv + "RPT_TBARCHIVE_10&FSFILE_NO=" + fileno + "&rc:parameters=false&rs:Command=Render&rs:Format=PDF";

            return Server.UrlEncode(strURL);
        }

        [WebMethod]
        public string CallReport_Archive_Small(string fileno)
        {
            String strURL = "";  //要傳回去的URL
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string reportSrv = SettingObj.sysConfig_Read("/ServerConfig/ReportServer");

            strURL = reportSrv + "RPT_TBARCHIVE_09&FSFILE_NO=" + fileno + "&rc:parameters=false&rs:Command=Render&rs:Format=PDF";

            return Server.UrlEncode(strURL);
        }

        [WebMethod]
        public string CallReport_Archive_Stick(string fileno)
        {
            String strURL = "";  //要傳回去的URL
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string reportSrv = SettingObj.sysConfig_Read("/ServerConfig/ReportServer");

            strURL = reportSrv + "RPT_TBARCHIVE_11&FSFILE_NO=" + fileno + "&rc:parameters=false&rs:Command=Render&rs:Format=PDF";

            return Server.UrlEncode(strURL);
        }

        //列印送帶轉檔表單-同一支帶子有多個短帶
        [WebMethod]
        public string CallREPORT_BRO_PROMO(string strPRINTID, string CREATED_BY, string strSessionID)
        {
            String strURL = "";  //要傳回去的URL

            //取列印報表的系統設定檔
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string reportSrv = SettingObj.sysConfig_Read("/ServerConfig/ReportServer");

            //檔案編號、查詢者、SessionID
            strURL = reportSrv + "RPT_TBBROADCAST_03&FSPRINTID=" + strPRINTID + "&QUERY_BY=" + CREATED_BY + "&FSSESSION_ID=" + strSessionID + "&rc:parameters=false&rs:Command=Render";
            return Server.UrlEncode(strURL);
        }

        //取得檔案上傳區路徑
        [WebMethod]
        public string GET_FILEUPLOAD_PATH()
        {
            //取列印報表的系統設定檔
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string reportSrv = SettingObj.sysConfig_Read("/ServerConfig/STT_UPLOAD");

            return reportSrv;
        }

        //透過帳號查詢使用者所屬的部門主管_因為送帶轉檔置換時要送到這關審核
        [WebMethod]
        public string Query_DEP_SupervisorID_BYUSERID(string strUserID)
        {
            string strDep_ID = "0";          //部門            

            Dictionary<string, string> source = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            Dictionary<string, string> source2 = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult2;
            source.Add("FSUSER_ID", strUserID);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBUSERS_DEPID_PROPOSAL", source, out sqlResult))
                return "";

            if (sqlResult.Count > 0)
            {
                strDep_ID = sqlResult[0]["FNDep_ID"];
                source2.Add("FNDEP_ID", strDep_ID);
                if (strDep_ID != "0")
                {
                    MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBUSER_DEP_SUPERVISOR", source2, out sqlResult2);

                    if (sqlResult2.Count > 0)
                        return sqlResult2[0]["FSSupervisor_ID"].Trim();
                    else
                        return "";
                }
                else
                    return "";
            }
            else
                return "";
        }

        #endregion

        #region 片庫媒材回溯
        /// <summary>
        ///透過片庫媒材上傳程式新增入庫圖片檔
        /// </summary>
        /// <param name="strFSID">節目編號</param>
        /// <param name="strEpisode">集數</param>
        /// <param name="strProgName">節目名稱</param>
        /// <param name="strChannelID">頻道ID</param>
        /// <param name="strFileType">檔案副檔名類型</param>
        /// <param name="strTITLE">標題</param>
        /// <param name="strMETA_EPISODE">媒材集數</param>
        /// <param name="strMETA_MEM_NAME">人名(角色)/團體名稱</param>
        /// <param name="strMETA_ACTIVE">事件/活動</param>
        /// <param name="strMETA_PLACE">地點</param>
        /// <param name="strMETA_OCCUR_TIME">發生時間</param>
        /// <param name="strMETA_FROM">來源</param>
        /// <param name="strMETA_RIGHT">版權</param>
        /// <param name="strMETA_NOTE">備註</param>
        /// <param name="strFileSize">檔案大小</param>
        /// <param name="strOldFileName">原始檔名(包含副檔名)</param>
        /// <param name="strUploadFilePath">要上傳的圖片原始路徑</param>
        /// <param name="CREATED_BY">新增者 修改者</param>
        ///  <param name="strMETA_PATH">圖片完整路徑</param>
        /// <returns></returns>
        [WebMethod]
        public string INSERT_TBLOG_PHOTO_META(string strFSID, string strEpisode, string strProgName, string strChannelID, string strFileType, string strTITLE, string strMETA_EPISODE, string strMETA_MEM_NAME, string strMETA_ACTIVE, string strMETA_PLACE, string strMETA_OCCUR_TIME, string strMETA_FROM, string strMETA_RIGHT, string strMETA_NOTE, string strFileSize, string strOldFileName, string strUploadFilePath, string CREATED_BY, string strMETA_PATH)
        {
            string strGetNo = "";       //取回來的ID
            string strEpisodeName = ""; //子集名稱
            string strFILE_NO = "";     //檔案編號
            string strSUBJECT_ID = "";  //SUBJECT ID
            string strDIR_ID = "";      //DIR ID
            string strFilePath = "";    //檔案路徑
            string strSmallName = "";    //縮圖位置
            bool isGenImgSuccess = false;//產生縮圖      

            //上傳前先檢查是否有重複的檔名，若是有就不上傳
            if (GetTBLOG_PHOTO_META_CHECK(strFSID, strEpisode, strMETA_PATH) == true)
                return "Error:重覆檔案名稱不上傳";

            WSBROADCAST Broobj = new WSBROADCAST();   //取得主要的ID
            strGetNo = Broobj.fnGetFileID("G", strFSID, strEpisode, "admin", strChannelID, strProgName, strEpisodeName);

            //接收回來的參數，前者是檔案編號，後者是DIRID   
            char[] delimiterChars = { ';' };
            string[] words = strGetNo.Split(delimiterChars);

            if (words.Length == 2)
            {
                strFILE_NO = words[0];
                strSUBJECT_ID = words[0].Trim().Substring(0, 12);
                strDIR_ID = words[1];
            }
            else
                return "Error:取得檔案編號異常";   //取得檔案編號異常

            WS.WSTSM.ServiceTSM objTsm = new WS.WSTSM.ServiceTSM();
            strFilePath = objTsm.GenerateSambaPathByFileIdInfo(strFILE_NO, WS.WSTSM.ServiceTSM.FileID_MediaType.Photo, strChannelID, strFileType);

            if (string.IsNullOrEmpty(strFilePath))
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("/DataSource/WSBROADCAST/INSERT_TBLOG_PHOTO_META", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "strFilePath=" + strFilePath);

                return "Error: 取得檔案路徑異常";
            }

            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSFILE_NO", strFILE_NO);
            sqlParameters.Add("FSSUBJECT_ID", strSUBJECT_ID);
            sqlParameters.Add("FSTYPE", "G");
            sqlParameters.Add("FSID", strFSID);
            sqlParameters.Add("FNEPISODE", strEpisode);
            sqlParameters.Add("FSSUPERVISOR", "Y"); //預設片庫轉入的都要審核
            sqlParameters.Add("FSARC_TYPE", "003");
            sqlParameters.Add("FSFILE_TYPE", strFileType);
            sqlParameters.Add("FSTITLE", strTITLE);
            sqlParameters.Add("FSDESCRIPTION", "");
            sqlParameters.Add("FSFILE_SIZE", strFileSize);
            sqlParameters.Add("FNWIDTH", "0");
            sqlParameters.Add("FNHEIGHT", "0");
            sqlParameters.Add("FNDPI", "0");
            sqlParameters.Add("FCFILE_STATUS", "Y");//已審核
            sqlParameters.Add("FSOLD_FILE_NAME", strOldFileName);//原始檔名       
            sqlParameters.Add("FSCHANGE_FILE_NO", "N/A");
            sqlParameters.Add("FSFILE_PATH", strFilePath);
            sqlParameters.Add("FNDIR_ID", strDIR_ID);
            sqlParameters.Add("FSCHANNEL_ID", strChannelID);
            sqlParameters.Add("FSARC_ID", "");
            sqlParameters.Add("FCMETA", "Y");
            sqlParameters.Add("FSMETA_EPISODE", strMETA_EPISODE);
            sqlParameters.Add("FSMETA_MEM_NAME", strMETA_MEM_NAME);
            sqlParameters.Add("FSMETA_ACTIVE", strMETA_ACTIVE);
            sqlParameters.Add("FSMETA_PLACE", strMETA_PLACE);
            sqlParameters.Add("FSMETA_OCCUR_TIME", strMETA_OCCUR_TIME);
            sqlParameters.Add("FSMETA_FROM", strMETA_FROM);
            sqlParameters.Add("FSMETA_RIGHT", strMETA_RIGHT);
            sqlParameters.Add("FSMETA_NOTE", strMETA_NOTE);
            sqlParameters.Add("FSMETA_PATH", strMETA_PATH);
            sqlParameters.Add("FSCREATED_BY", CREATED_BY);
            sqlParameters.Add("FSUPDATED_BY", CREATED_BY);

            strSmallName = getSmallPhoto(strFilePath, strFILE_NO);   //取得預覽圖檔位置

            //產生預覽圖檔
            isGenImgSuccess = MAM_PTS_DLL.PhotoSrv.genThumbnailImg(strUploadFilePath, strSmallName, 320);
            if (isGenImgSuccess == false)
                return "Error:產生預覽圖檔失敗";

            if (MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBLOG_PHOTO_METADATA", sqlParameters, "admin") == true)
                return strFilePath;
            else
                return "Error:入庫圖片檔寫入失敗";
        }

        //查詢入庫圖片檔資料是否重覆上傳_片庫媒材回溯用，有資料或查詢有問題就回傳True，無資料回傳False
        [WebMethod]
        public Boolean GetTBLOG_PHOTO_META_CHECK(string strProgID, string strEpisode, string strMetaPath)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSID", strProgID);
            sqlParameters.Add("FNEPISODE", strEpisode);
            sqlParameters.Add("FSMETA_PATH", strMetaPath);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_PHOTO_META_CHECK", sqlParameters, out sqlResult))
                return true;

            if (sqlResult.Count > 0)
                return true;
            else
                return false;
        }

        //查詢縮圖的位置
        private string getSmallPhoto(string strFilePath, string strFILE_NO)
        {
            return strFilePath.Substring(0, strFilePath.IndexOf(strFILE_NO)) + strFILE_NO + "_M.jpg";
        }

        //查詢使用者單位
        [WebMethod]
        public string QUERY_TBUSERS_BYUSER_LOGIN(string strUser)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSUSER", strUser); ;

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBUSER_BYLOGIN_USER", sqlParameters, out sqlResult))
                return "";

            if (sqlResult.Count > 0)
            {
                if (sqlResult[0]["FSUSER_ID"] != null && sqlResult[0]["FSUSER_ID"] != "")
                    return sqlResult[0]["FSUSER_ID"];
            }

            return "";
        }

        /// <summary>
        /// 透過片庫媒材上傳程式新增入庫文件檔
        /// </summary>
        /// <param name="strFSID">節目編號</param>
        /// <param name="strEpisode">集數</param>
        /// <param name="strProgName">節目名稱</param>
        /// <param name="strChannelID">頻道ID</param>
        /// <param name="strFileType">檔案副檔名類型</param>
        /// <param name="strFileSize">檔案大小</param>
        /// <param name="strOldFileName">檔案大小</param>
        /// <param name="CREATED_BY">新增者 修改者</param>
        /// <param name="strMETA_PATH">完整路徑</param>
        /// <returns></returns>
        [WebMethod]
        public string INSERT_TBLOG_DOC_META(string strFSID, string strEpisode, string strProgName, string strChannelID, string strFileType, string strFileSize, string strOldFileName, string CREATED_BY, string strMETA_PATH)
        {
            string strGetNo = "";       //取回來的ID
            string strEpisodeName = ""; //子集名稱
            string strFILE_NO = "";     //檔案編號
            string strSUBJECT_ID = "";  //SUBJECT ID
            string strDIR_ID = "";      //DIR ID
            string strFilePath = "";    //檔案路徑
            string Doccontet = "";      //解析文字檔的內容
            string eName = "";          //檔案格式
            //上傳前先檢查是否有重複的檔名，若是有就不上傳
            if (GetTBLOG_DOC_META_CHECK(strFSID, strEpisode, strMETA_PATH) == true)
                return "Error:重覆檔案名稱不上傳";

            strGetNo = fnGetFileID("G", strFSID, strEpisode, "admin", strChannelID, strProgName, strEpisodeName);

            //接收回來的參數，前者是檔案編號，後者是DIRID   
            char[] delimiterChars = { ';' };
            string[] words = strGetNo.Split(delimiterChars);

            if (words.Length == 2)
            {
                strFILE_NO = words[0];
                strSUBJECT_ID = words[0].Trim().Substring(0, 12);
                strDIR_ID = words[1];
            }
            else
                return "Error:取得檔案編號異常";   //取得檔案編號異常

            WS.WSTSM.ServiceTSM objTsm = new WS.WSTSM.ServiceTSM();
            strFilePath = objTsm.GenerateSambaPathByFileIdInfo(strFILE_NO, WS.WSTSM.ServiceTSM.FileID_MediaType.Doc, strChannelID, strFileType);
            if (string.IsNullOrEmpty(strFilePath))
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("/DataSource/WSBROADCAST/INSERT_TBLOG_DOC_META", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "strFilePath=" + strFilePath);

                return "Error: 取得檔案路徑異常";
            }
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();

            WSExtDoc ExtDoc = new WSExtDoc();
            sqlParameters.Add("FSFILE_NO", strFILE_NO);
            sqlParameters.Add("FSSUBJECT_ID", strSUBJECT_ID);
            sqlParameters.Add("FSTYPE", "G");
            sqlParameters.Add("FSID", strFSID);
            sqlParameters.Add("FNEPISODE", strEpisode);     //文件上傳都被放在第0集
            sqlParameters.Add("FSARC_TYPE", "025");         //預設都是回溯文件
            sqlParameters.Add("FSFILE_TYPE", strFileType);
            sqlParameters.Add("FSTITLE", strOldFileName);
            sqlParameters.Add("FSDESCRIPTION", "");
            sqlParameters.Add("FSFILE_SIZE", strFileSize);
            sqlParameters.Add("FCFILE_STATUS", "Y");        //待審核
            sqlParameters.Add("FSOLD_FILE_NAME", strOldFileName);//原始檔名
            sqlParameters.Add("FSCHANGE_FILE_NO", "N/A");
            sqlParameters.Add("FSFILE_PATH", strFilePath);
            sqlParameters.Add("FNDIR_ID", strDIR_ID);
            sqlParameters.Add("FSCHANNEL_ID", strChannelID);
            sqlParameters.Add("FSARC_ID", "");
            sqlParameters.Add("FSMETA_PATH", strMETA_PATH);
            sqlParameters.Add("FSCREATED_BY", CREATED_BY);

            eName = strFileType.ToLower();
            if ((eName == "doc") || (eName == "docx") || (eName == "pdf") || (eName == "ppt") || (eName == "pptx") || (eName == "txt") || eName == "xml" || eName == "xls" || eName == "xlsx" || eName == "rtf")
            {
                Doccontet = ExtDoc.ExtDocInfo(strMETA_PATH);// + @"\" + x.strFileId + "." + eName);
                sqlParameters.Add("FSCONTENT", Doccontet);
            }
            else
                sqlParameters.Add("FSCONTENT", ""); //無法解析就不要解析    

            if (MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBLOG_DOC_METADATA", sqlParameters, "admin") == true)
                return strFilePath;
            else
                return "Error:入庫圖片檔寫入失敗";
        }

        //查詢入庫文件檔資料是否重覆上傳_片庫媒材回溯用，有資料或查詢有問題就回傳True，無資料回傳False
        [WebMethod]
        public Boolean GetTBLOG_DOC_META_CHECK(string strProgID, string strEpisode, string strMetaPath)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSID", strProgID);
            sqlParameters.Add("FNEPISODE", strEpisode);
            sqlParameters.Add("FSMETA_PATH", strMetaPath);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_DOC_META_CHECK", sqlParameters, out sqlResult))
                return true;

            if (sqlResult.Count > 0)
                return true;
            else
                return false;
        }

        #endregion
    }
}
