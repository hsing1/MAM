﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSIFilter 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSExtFileInfo : System.Web.Services.WebService
    {
        public class photoExifInfo
        {
            public string fnwidth {get; set ;} // 影像寬度
            public string fnheight { get; set; }  // 影像高度
            public string fsvertical_res { get; set; }  //垂直解析度
            public string fscamera_make { get; set; }  //相機廠牌
            public string fscamera_model { get; set; }  //相機型號
            public string fdcamera_date { get; set; }  // 攝影時間
            public string fniso { get; set; }   //ISO
            public string fsfocal_length { get; set; }    // 焦距
            public string fsexposure_time { get; set; }   // 曝光時間
            public string fsaperture { get; set; }  //光圈
        }

        [WebMethod]
        public photoExifInfo fnGetExifFromPhoto(string sFileName)
        {
            

            photoExifInfo photo = new photoExifInfo();

            

            System.Drawing.Image img;
            


            try
            {
                img = System.Drawing.Imaging.Metafile.FromFile(sFileName);
                photo.fnwidth = img.Width.ToString() ;
                photo.fnheight = img.Height.ToString();
                photo.fsvertical_res = img.VerticalResolution.ToString() ;
                

                Int64 mm;

                if (img.PropertyItems.Length > 0)
                {
                    foreach (System.Drawing.Imaging.PropertyItem pi in img.PropertyItems) {
                        switch (pi.Id)
                        {
                            case 271 :
                                photo.fscamera_make = System.Text.ASCIIEncoding.ASCII.GetString(pi.Value).Replace(((char)0).ToString(), ""); 
                                break ; 
                            case 272 :
                                photo.fscamera_model = System.Text.ASCIIEncoding.ASCII.GetString(pi.Value).Replace(((char)0).ToString(), ""); 
                                break; 
                            case 36867 :
                                photo.fdcamera_date = System.Text.ASCIIEncoding.ASCII.GetString(pi.Value).Replace(((char)0).ToString(), "");
                                break; 
                            case 34855 :
                                photo.fniso = System.Text.ASCIIEncoding.ASCII.GetString(pi.Value).Replace(((char)0).ToString(), "");
                                break; 
                            case 37386 : //焦距
                                // not implement ;
                                break;
                            case 33434: //曝光時間
                                // not implement ;
                                break;
                            case 33437: //光圈
                                // not implement ;
                                break;

                            
                        }
                    }
                }                

            }
            catch (Exception ex)
            {
                return null; 
            }


            return photo; 
        }

        /// <summary>
        /// 剖析文件檔內容
        /// doc,pdf,ppt,pptx,docx,txt,xml,rtf(要試試看，不確定)
        /// </summary>
        /// <param name="sFileName">檔名FULL PATH</param>
        /// <returns>string 內容</returns>
        [WebMethod]
        public string fnExtractTextFromFile(string sFileName)
        {
            string sContent = string.Empty; 
            try
            {
                if (EmadOmar.OfficeToText.IsParseable(sFileName))
                {
                    sContent = EmadOmar.OfficeToText.Parse(sFileName);                    
                }
            }
            catch (Exception ex)
            {
               sContent = string.Empty;
            }
            return sContent; 
        }

    }
}
