﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using PTS_MAM3.Web.Class;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSDATA 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSDATA : System.Web.Services.WebService
    {
        List<Class_DELETE_MSG> m_DeleteMsg = new List<Class_DELETE_MSG>();  //檢查刪除節目主檔訊息集合

        public struct ReturnMsg     //錯誤訊息處理
        {
            public bool bolResult;
            public string strErrorMsg;
        }

        //解析資料庫回來的Dictionary，轉存成Class
        private List<Class_DATA> Transfer_DATA(List<Dictionary<string, string>> sqlResult)
        {
            List<Class_DATA> returnData = new List<Class_DATA>();
       
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_DATA obj = new Class_DATA();
                // --------------------------------    
                obj.FSDATA_ID = sqlResult[i]["FSDATA_ID"];                                     //資料帶編碼
                obj.FSDATA_NAME = sqlResult[i]["FSDATA_NAME"];                                 //資料帶中文名稱                      
                obj.FSMEMO = sqlResult[i]["FSMEMO"];                                           //備註 
                obj.FSDEL = sqlResult[i]["FSDEL"];                                             //刪除註記
                obj.FSDELUSER = sqlResult[i]["FSDELUSER"];                                     //刪除者            
                obj.FSCHANNEL_ID = GET_TBZCHANNEL_DATA();                                      //頻道別
                obj.FNDEP_ID = Convert.ToInt16(sqlResult[i]["FNDEP_ID"]);                      //成案單位

                obj.FSCREATED_BY_NAME = sqlResult[i]["FSCREATED_BY_NAME"];                                //建檔者
                obj.FDCREATED_DATE = Convert.ToDateTime(sqlResult[i]["WANT_FDCREATEDDATE"]);              //建檔日期
                obj.SHOW_FDCREATED_DATE = MAMFunctions.compareDate(sqlResult[i]["WANT_FDCREATEDDATE"]);   //建檔日期(顯示)
                obj.FSUPDATED_BY_NAME = sqlResult[i]["FSUPDATED_BY_NAME"];                                //修改者
                obj.FDUPDATED_DATE = Convert.ToDateTime(sqlResult[i]["WANT_FDUPDATEDDATE"]);              //修改日期
                obj.SHOW_FDUPDATED_DATE = MAMFunctions.compareDate(sqlResult[i]["WANT_FDUPDATEDDATE"]);   //修改日期(顯示)
                obj.FSDEL_BY_NAME = sqlResult[i]["FSDEL_BY_NAME"];                                        //刪除者(顯示)
                // --------------------------------
                returnData.Add(obj);
            }
            return returnData;
        }

        //資料帶資料檔的所有資料
        [WebMethod]
        public List<Class_DATA> GetTBDATA_ALL()
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBDATA_ALL", sqlParameters, out sqlResult))
                return new List<Class_DATA>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_DATA(sqlResult);
        }

        //資料帶資料檔的所有資料_透過資料帶名稱
        [WebMethod]
        public List<Class_DATA> GetTBDATA_BYDATANAME(string strDataName)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSDATA_NAME", strDataName);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBDATA_ALL_BYDATANAME", sqlParameters, out sqlResult))
                return new List<Class_DATA>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_DATA(sqlResult);
        }

        //資料帶資料檔的所有資料_透過資料帶名稱(新增資料帶時的檢查) 回傳false不能新增
        [WebMethod]
        public Boolean GetTBDATA_BYDATANAME_CHECK(string strDataName)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSDATA_NAME", strDataName);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBDATA_ALL_BYDATANAME_CHECK", sqlParameters, out sqlResult))
                return false;    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count > 0)
                return false;
            else
                return true;
        }

        //取資料帶編號
        [WebMethod]
        public string GetNoRecordDATA_ID(string strDataName, string CREATED_BY)
        {
            // SQL Parameters
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSYYYYMM", DateTime.Now.ToString("yyyyMM"));
            source.Add("FSDATA_NAME", strDataName);
            source.Add("FSCREATED_BY", CREATED_BY);

            List<Dictionary<string, string>> sqlResult;

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_I_TBNORECORD_DATAID", source, out sqlResult))
                return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count > 0)
                return sqlResult[0]["WANT_DATAID"];
            else
                return "";
        }

        //透過資料帶名稱查詢資料帶基本資料，修改資料帶用
        [WebMethod()]
        public Boolean QUERY_TBDATA_BYNAME_CHECK(string strDataName, string strDataID)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSDATA_NAME", strDataName.Trim());
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBDATA_ALL_BYDATANAME", sqlParameters, out sqlResult))
                return true;    // 發生SQL錯誤時，回傳True，表示有同樣節目名稱的節目主檔，不給修改

            if (sqlResult.Count == 1)
            {
                //如果查到的事自己這筆，表示可以修改，回傳False
                if (sqlResult[0]["FSDATA_NAME"] == strDataName.Trim())
                {
                    if (sqlResult[0]["FSDATA_ID"] == strDataID.Trim())
                        return false;   //資料帶編號相同，查到的是自己這筆，回傳False，表示可以修改
                    else
                        return true;    //資料帶編號不同，查到的是已經存在的短帶名稱，回傳True，表示不能修改
                }
                else
                    return false;       //沒有這個資料帶名稱，回傳False，表示可以修改
            }
            else if (sqlResult.Count == 0)
                return false;           //沒有這個資料帶名稱，回傳False，表示可以修改
            else
                return true;            //照理說資料帶資料時一定不會超過一筆，若是進到這表是有問題就還是寫回傳True，不給修改               
        }

        //新資料帶資料檔
        [WebMethod]
        public Boolean INSERT_TBDATA(Class.Class_DATA obj)
        {
            //先取得資料帶編號
            string stData_ID = GetNoRecordDATA_ID(obj.FSDATA_NAME, obj.FSCREATED_BY);
            
            if (stData_ID.Trim() == "")
                return false;

            Dictionary<string, string> source = new Dictionary<string, string>();
            WSPROPOSAL objProposal = new WSPROPOSAL();
            source.Add("FSDATA_ID", stData_ID);
            source.Add("FSDATA_NAME", obj.FSDATA_NAME);
            source.Add("FSMEMO", obj.FSMEMO);
            source.Add("FSDEL", "N");
            source.Add("FSDELUSER", "");
            source.Add("FNDEP_ID", objProposal.Query_DEPID_BYUSERID(obj.FSCREATED_BY).ToString());  //查詢所屬部門
            source.Add("FSCREATED_BY", obj.FSCREATED_BY);
            source.Add("FSUPDATED_BY", obj.FSUPDATED_BY);

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBDATA", source, obj.FSUPDATED_BY);
        }

        //修改宣傳帶資料檔
        [WebMethod]
        public Boolean UPDATE_TBDATA(Class.Class_DATA obj)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSDATA_ID",obj.FSDATA_ID);
            source.Add("FSDATA_NAME", obj.FSDATA_NAME);
            source.Add("FSMEMO", obj.FSMEMO); 
            source.Add("FSDEL", obj.FSDEL);
            source.Add("FSDELUSER", obj.FSDELUSER);
            source.Add("FSUPDATED_BY", obj.FSUPDATED_BY);

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBDATA", source, obj.FSUPDATED_BY);
        }

        //修改宣傳帶資料檔
        [WebMethod]
        public Boolean UPDATE_TBDATA_ByDirID(string dir_ID,string newName,string updateUser)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FNDIR_ID", dir_ID);
            source.Add("FSDATA_NAME", newName);
            source.Add("FSUPDATE_BY", updateUser);


            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBDATA_And_TBSubject_BYDIR_ID", source, updateUser);
        }

        //修改資料帶後TBDIRECTORIES要跟著連動
        [WebMethod]
        public Boolean UpdateTBDIRECTORIES(Class_DATA data, string oriName) 
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSDATA_NAME", data.FSDATA_NAME);
            source.Add("FSMEMO", data.FSMEMO);
            source.Add("OriName", oriName);
            source.Add("UpdateUser", data.FSUPDATED_BY);

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBDIRECTORIES", source, data.FSUPDATED_BY);
        }


        //修改資料帶後TBSubject要跟著連動
        [WebMethod]
        public Boolean UpdateTBSubject(Class_DATA data)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSDATA_ID", data.FSDATA_ID);
            source.Add("FSDATA_NAME", data.FSDATA_NAME);
            source.Add("UpdateUser", data.FSUPDATED_BY);

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBSUBJECT", source, data.FSUPDATED_BY);
        }


        //刪除資料帶資料時，要檢查該資料帶是否有其他資料
        [WebMethod]
        public List<Class_DELETE_MSG> DELETE_TBDATA_Check(string strDataID, string strDelUser)
        {
            DELETE_TBDATA_Check_BROCAST(strDataID);            //刪除資料帶資料時，檢查送帶轉檔
            DELETE_TBDATA_Check_ARCHIVE(strDataID);            //刪除資料帶資料時，檢查入庫檔          

            return m_DeleteMsg;
        }

        //刪除資料帶資料時，檢查送帶轉檔
        public void DELETE_TBDATA_Check_BROCAST(string strPromoID)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSBRO_TYPE", "D");
            source.Add("FSID", strPromoID);
            source.Add("FNEPISODE", "0");

            List<Dictionary<string, string>> sqlResult;
            MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBBROADCAST_BYIDEPISODE", source, out sqlResult);

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_DELETE_MSG obj = new Class_DELETE_MSG();
                obj.FSSystem = "送帶轉檔資料";

                if (sqlResult[i]["WANT_FDBRO_DATE"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + "申請日期 - " + sqlResult[i]["WANT_FDBRO_DATE"].ToString().Trim();

                m_DeleteMsg.Add(obj);
            }
        }

        //刪除資料帶資料時，檢查入庫檔
        public void DELETE_TBDATA_Check_ARCHIVE(string strPromoID)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSTYPE", "D");
            source.Add("FSID", strPromoID);
            source.Add("FNEPISODE", "0");

            List<Dictionary<string, string>> sqlResult;
            MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBARCHIVE_BYID_EPISODE", source, out sqlResult);

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_DELETE_MSG obj = new Class_DELETE_MSG();
                obj.FSSystem = "入庫資料";

                if (sqlResult[i]["WANT_FDARC_DATE"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + "申請日期 - " + sqlResult[i]["WANT_FDARC_DATE"].ToString().Trim();

                m_DeleteMsg.Add(obj);
            }
        }        

        //刪除資料帶資料檔
        [WebMethod]
        public ReturnMsg DELETE_TBDATA(string strDATA_ID, string strUPDATED_BY)
        {
            //先作一連串的判斷，確定該資料帶可以刪掉後才刪除(將刪除註記變成Y)
            ReturnMsg objMsg = new ReturnMsg();
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSDATA_ID", strDATA_ID);
            source.Add("FSDEL", "Y");
            source.Add("FSDELUSER", strUPDATED_BY);

            objMsg.bolResult = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_TBDATA", source, strUPDATED_BY);
            objMsg.strErrorMsg = "";

            return objMsg;
        }
      
        //查詢資料帶的ChannelID
        public string GET_TBZCHANNEL_DATA()
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSCHANNEL_NAME", "資料帶");
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBZCHANNEL_ALL_NAME", sqlParameters, out sqlResult))
                return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count > 0)
                return sqlResult[0]["FSCHANNEL_ID"];
            else
                return "";
        }
    }
}
