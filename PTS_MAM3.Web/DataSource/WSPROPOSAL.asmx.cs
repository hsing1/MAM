﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Xml;
using System.Text;
using System.Xml.Linq;
using System.IO;
using PTS_MAM3.Web.Class;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSPROPOSAL 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSPROPOSAL : System.Web.Services.WebService
    {
        List<Class_CODE> m_CodeData = new List<Class_CODE>();       //代碼檔集合
        List<Class_CODE> m_CodeDataBuyD = new List<Class_CODE>();   //代碼檔集合(外購類別細項)        

        //解析資料庫回來的Dictionary，轉存成Class
        private List<Class_PROPOSAL> Transfer_PROPOSAL(List<Dictionary<string, string>> sqlResult)
        {
            List<Class_PROPOSAL> returnData = new List<Class_PROPOSAL>();
            m_CodeData = MAMFunctions.Transfer_Class_CODE("SP_Q_TBPROG_M_CODE");
            m_CodeDataBuyD = MAMFunctions.Transfer_Class_CODED("SP_Q_TBZPROGBUYD_ALL");
            /*
           List< Dictionary<string, string>> producerDictionary = new List<Dictionary<string, string>>();
            foreach (Dictionary<string, string> dic in sqlResult)
            {
                Dictionary<string, string> inputParameters = new Dictionary<string, string>();
                inputParameters.Add("FSUSER_ID", dic["FSPRODUCER"]);
                List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
                if (MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBUSERS_CHTNAME_BY_USERID", inputParameters, out resultData))
                {

                    if (resultData.Count > 0)
                    {
                        Dictionary<string, string> resultDictionary = new Dictionary<string, string>();

                        resultDictionary.Add(dic["FSPRODUCER"], resultData[0]["FSUSER_ChtName"]);

                        producerDictionary.Add(resultDictionary);
                    }
                }
            }*/

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_PROPOSAL obj = new Class_PROPOSAL();
                // --------------------------------    
                obj.FSPRO_ID = sqlResult[i]["FSPRO_ID"];                                    //成案單編號
                obj.FCPRO_TYPE = sqlResult[i]["FCPRO_TYPE"];                                //成案種類
                obj.FCPRO_TYPE_NAME = MAMFunctions.CheckProgType(sqlResult[i]["FCPRO_TYPE"]);//成案種類名稱
                obj.FSPROG_ID = sqlResult[i]["FSPROG_ID"];                                  //節目編號
                obj.FSPROG_NAME = sqlResult[i]["FSPROG_NAME"];                              //節目名稱
                obj.FSPLAY_NAME = sqlResult[i]["FSPLAY_NAME"];                              //播出名稱
                obj.FSNAME_FOR = sqlResult[i]["FSNAME_FOR"];                                //外文名稱
                obj.FSCONTENT = sqlResult[i]["FSCONTENT"];                                  //節目內容簡述
                obj.FNLENGTH = Convert.ToInt32(sqlResult[i]["FNLENGTH"]);                   //每集時長
                obj.FNBEG_EPISODE = Convert.ToInt16(sqlResult[i]["FNBEG_EPISODE"]);         //集次(起)
                obj.FNEND_EPISODE = Convert.ToInt16(sqlResult[i]["FNEND_EPISODE"]);         //集次(迄)
                obj.EPISODE = Convert.ToInt16(sqlResult[i]["FNBEG_EPISODE"]) + "~" + Convert.ToInt16(sqlResult[i]["FNEND_EPISODE"]);  //集數起迄 
                obj.FSCHANNEL = sqlResult[i]["FSCHANNEL"];                                 //預訂播出頻道
                obj.FDSHOW_DATE = Convert.ToDateTime(sqlResult[i]["WANTSHOWDATE"]);        //預訂上檔日期
                obj.SHOW_FDSHOW_DATE = MAMFunctions.compareDate(sqlResult[i]["WANTSHOWDATE"]);//預訂上檔日期(顯示)

                obj.FSPRD_DEPT_ID = sqlResult[i]["FSPRD_DEPT_ID"];                          //製作部門
                obj.FSPRODUCER = sqlResult[i]["FSPRODUCER"];                               //製作人ID
                obj.FSPRODUSER_NAME = QueryProducerName(obj.FSPRODUCER);//producerDictionary[i]["FSUSER_ChtName"];               //製作人姓名
                obj.FSPAY_DEPT_ID = sqlResult[i]["FSPAY_DEPT_ID"];                          //付款部門 
                obj.FSBEF_DEPT_ID = sqlResult[i]["FSBEF_DEPT_ID"];                          //前會部門 
                obj.FSAFT_DEPT_ID = sqlResult[i]["FSAFT_DEPT_ID"];                          //後會部門 
                obj.FSOBJ_ID = sqlResult[i]["FSOBJ_ID"];                                    //製作目的     
                obj.FSGRADE_ID = sqlResult[i]["FSGRADE_ID"];                                //節目分級 
                obj.FSSRC_ID = sqlResult[i]["FSSRC_ID"];                                    //節目來源 
                obj.FSTYPE = sqlResult[i]["FSTYPE"];                                        //節目型態 
                obj.FSAUD_ID = sqlResult[i]["FSAUD_ID"];                                    //目標觀眾     
                obj.FSSHOW_TYPE = sqlResult[i]["FSSHOW_TYPE"];                              //表現方式 
                obj.FSLANG_ID_MAIN = sqlResult[i]["FSLANG_ID_MAIN"];                        //主聲道 
                obj.FSLANG_ID_SUB = sqlResult[i]["FSLANG_ID_SUB"];                          //副聲道 
                obj.FSBUY_ID = sqlResult[i]["FSBUY_ID"];                                    //外購類別大類     
                obj.FSBUYD_ID = sqlResult[i]["FSBUYD_ID"];                                  //外購類別細類 
                obj.FCCHECK_STATUS = sqlResult[i]["FCCHECK_STATUS"];                        //狀態
                obj.FSCHANNEL_ID = sqlResult[i]["FSCHANNEL_ID"];                            //頻道別代碼

                obj.FCEMERGENCY = sqlResult[i]["FCEMERGENCY"];                              //緊急成案
                obj.FCCOMPLETE = sqlResult[i]["FCCOMPLETE"];                                //資料補齊
                obj.SHOW_FCCHECK_STATUS_NAME = MAMFunctions.CheckStatusReport(sqlResult[i]["FCCHECK_STATUS"]);//狀態名稱
                obj.FSPRDCENID = sqlResult[i]["FSPRDCENID"];                                //製作單位代碼
                obj.FSPROGSPEC = sqlResult[i]["FSPROGSPEC"];                                //節目規格
                obj.FCPROGD = sqlResult[i]["FCPROGD"];                                      //產生子集
                obj.FSENOTE = sqlResult[i]["FSENOTE"];                                      //分集備註

                if (sqlResult[i]["WANT_FDCOMPLETE"] == "")
                {
                    obj.FDCOMPLETE = Convert.ToDateTime("1900/1/1");       //資料補齊日期
                    obj.SHOW_FDCOMPLETE = "";//資料補齊日期(顯示)                  
                }
                else
                {
                    obj.FDCOMPLETE = Convert.ToDateTime(sqlResult[i]["WANT_FDCOMPLETE"]);       //資料補齊日期
                    obj.SHOW_FDCOMPLETE = MAMFunctions.compareDate(sqlResult[i]["WANT_FDCOMPLETE"]);//資料補齊日期(顯示)                    
                }

                //緊急成案(顯示)
                if (obj.FCEMERGENCY.Trim() == "Y" && obj.FCCOMPLETE.Trim() == "Y")
                    obj.SHOW_FCEMERGENCY = "補齊";
                else if (obj.FCEMERGENCY.Trim() == "Y" && obj.FCCOMPLETE.Trim() == "N")
                    obj.SHOW_FCEMERGENCY = "待補";
                else
                    obj.SHOW_FCEMERGENCY = "否";

                obj.FSCREATED_BY = sqlResult[i]["FSCREATED_BY"];                            //建檔者
                obj.FSCREATED_BY_NAME = sqlResult[i]["FSCREATED_BY_NAME"];                  //建檔者(顯示)
                obj.FSCREATED_BY_EMAIL = sqlResult[i]["FSCREATED_BY_EMAIL"];                //建檔者EMAIL
                obj.FDCREATED_DATE = Convert.ToDateTime(sqlResult[i]["WANT_FDCREATEDDATE"]);//建檔日期
                obj.SHOW_FDCREATED_DATE = MAMFunctions.compareDate(sqlResult[i]["WANT_FDCREATEDDATE"]);  //建檔日期(顯示)

                obj.FSUPDATED_BY = sqlResult[i]["FSUPDATED_BY"];                             //修改者
                obj.FSUPDATED_BY_NAME = sqlResult[i]["FSUPDATED_BY_NAME"];                   //修改者(顯示)
                obj.FDUPDATED_DATE = Convert.ToDateTime(sqlResult[i]["WANT_FDUPDATEDDATE"]); //修改日期
                obj.SHOW_FDUPDATED_DATE = MAMFunctions.compareDate(sqlResult[i]["WANT_FDUPDATEDDATE"]);//修改日期(顯示)

                obj.FSPRD_DEPT_ID_NAME = compareCode_Code(sqlResult[i]["FSPRD_DEPT_ID"], "TBZDEPT");        //製作部門代碼名稱
                obj.FSPAY_DEPT_ID_NAME = compareCode_Code(sqlResult[i]["FSPAY_DEPT_ID"], "TBZDEPT");        //付款部門代碼名稱
                obj.FSBEF_DEPT_ID_NAME = compareCode_Code(sqlResult[i]["FSBEF_DEPT_ID"], "TBZDEPT");        //前會部門代碼名稱
                obj.FSAFT_DEPT_ID_NAME = compareCode_Code(sqlResult[i]["FSAFT_DEPT_ID"], "TBZDEPT");        //後會部門代碼名稱
                obj.FSOBJ_ID_NAME = compareCode_Code(sqlResult[i]["FSOBJ_ID"], "TBZPROGOBJ");               //製作目的代碼名稱
                obj.FSSRC_ID_NAME = compareCode_Code(sqlResult[i]["FSSRC_ID"], "TBZPROGSRC");               //節目來源代碼名稱
                obj.FSTYPE_NAME = compareCode_Code(sqlResult[i]["FSTYPE"], "TBZPROGATTR");                  //內容屬性代碼名稱
                obj.FSAUD_ID_NAME = compareCode_Code(sqlResult[i]["FSAUD_ID"], "TBZPROGAUD");               //目標觀眾代碼名稱
                obj.FSSHOW_TYPE_NAME = compareCode_Code(sqlResult[i]["FSSHOW_TYPE"], "TBZPROGTYPE");        //表現方式代碼名稱
                obj.FSGRADE_ID_NAME = compareCode_Code(sqlResult[i]["FSGRADE_ID"], "TBZPROGGRADE");         //節目分級代碼名稱
                obj.FSLANG_ID_MAIN_NAME = compareCode_Code(sqlResult[i]["FSLANG_ID_MAIN"], "TBZPROGLANG");  //主聲道代碼名稱
                obj.FSLANG_ID_SUB_NAME = compareCode_Code(sqlResult[i]["FSLANG_ID_SUB"], "TBZPROGLANG");    //副聲道代碼名稱
                obj.FSBUY_ID_NAME = compareCode_Code(sqlResult[i]["FSBUY_ID"], "TBZPROGBUY");               //外購類別代碼名稱
                obj.FSBUYD_ID_NAME = compareCode_CodeBuyD(sqlResult[i]["FSBUY_ID"], sqlResult[i]["FSBUYD_ID"]);//外購類別細項代碼名稱
                obj.FSCHANNEL_ID_NAME = compareCode_Code(sqlResult[i]["FSCHANNEL_ID"], "TBZCHANNEL");       //頻道別代碼名稱 
                obj.FSPROGSPEC_NAME = compareCode_Code_ProgSpec(sqlResult[i]["FSPROGSPEC"], "TBZPROGSPEC"); //節目規格名稱
                obj.FSPRDCENID_NAME = compareCode_Code_ProgSpec(sqlResult[i]["FSPRDCENID"], "TBZPRDCENTER");//製作單位名稱 
                if (sqlResult[i]["FDEXPIRE_DATE"] == "")
                {
                    obj.FDEXPIRE_DATE = Convert.ToDateTime("1900-1-1");
                    obj.Origin_FDEXPIRE_DATE = Convert.ToDateTime("1900-1-1");
                }
                else
                {
                    obj.FDEXPIRE_DATE = Convert.ToDateTime(sqlResult[i]["FDEXPIRE_DATE"]);                      //到期日期
                    obj.Origin_FDEXPIRE_DATE = Convert.ToDateTime(sqlResult[i]["FDEXPIRE_DATE"]);
                }
                obj.FSEXPIRE_DATE_ACTION = sqlResult[i]["FSEXPIRE_DATE_ACTION"];                            //到期時執行的動作
                obj.Origin_FSEXPIRE_DATE_ACTION = sqlResult[i]["FSEXPIRE_DATE_ACTION"];
                obj.FSPROGNATIONID = sqlResult[i]["FSPROGNATIONID"];                                        //來源國家
                // --------------------------------
                returnData.Add(obj);
            }

            return returnData;
        }


        //使用Producer的@FSUSER_ID查詢FSUSER_ChtName
        private string QueryProducerName(string FSUSER_ID)
        {
            string resultStr = "";
            if (FSUSER_ID != "")
            {
                Dictionary<string, string> inputParameters = new Dictionary<string, string>();
                inputParameters.Add("FSUSER_ID", FSUSER_ID);
                List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
                if (MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBUSERS_CHTNAME_BY_USERID", inputParameters, out resultData))
                {
                    if (resultData.Count > 0)
                    {
                        resultStr = resultData[0]["FSUSER_ChtName"];
                    }
                }
            }
            

            return resultStr;
        }

        //比對代碼檔
        string compareCode_Code(string strID, string strTable)
        {
            for (int i = 0; i < m_CodeData.Count; i++)
            {
                if (m_CodeData[i].ID.ToString().Trim().Equals(strID) && m_CodeData[i].TABLENAME.ToString().Trim().Equals(strTable))
                {
                    return m_CodeData[i].NAME.ToString().Trim();
                }
            }
            return "";
        }

        //比對代碼檔_外購類別細項
        string compareCode_CodeBuyD(string strID, string strDID)
        {
            for (int i = 0; i < m_CodeDataBuyD.Count; i++)
            {
                if (m_CodeDataBuyD[i].ID.ToString().Trim().Equals(strID) && m_CodeDataBuyD[i].IDD.ToString().Trim().Equals(strDID))
                {
                    return m_CodeDataBuyD[i].NAME.ToString().Trim();
                }
            }
            return "";
        }
        public string currentNoRecordPROG_ID = "";

        [WebMethod]
        //從TBPROPOSAL查詢節目編號 by Jarvis20130429
        public string QueryNoRecordPROG_ID(string ProgName, string CREATED_BY)
        {
            // SQL Parameters
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_NAME", ProgName);
            source.Add("FSCREATED_BY", CREATED_BY);
            //source.Add("FSPROG_ID", "");
            List<Dictionary<string, string>> sqlResult;

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROPOSAL_PROGID", source, out sqlResult))
                return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count > 0)
                return sqlResult[0]["FSPROG_ID"];
            else
                return "";
        }

        //取節目編號
        [WebMethod]
        public string GetNoRecordPROG_ID(string NOTE, string CREATED_BY)
        {
            // SQL Parameters
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSNO_DATE", DateTime.Now.ToString("yyyy"));

            if (NOTE == null)
                source.Add("FSNOTE", "");
            else
                source.Add("FSNOTE", NOTE);

            source.Add("FSCREATED_BY", CREATED_BY);

            List<Dictionary<string, string>> sqlResult;

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_I_TBNORECORD_PROG_ID", source, out sqlResult))
                return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count > 0)
                return sqlResult[0]["FSNO"];
            else
                return "";
        }

        //成案檔的所有資料
        [WebMethod]
        public List<Class_PROPOSAL> fnGetTBPROPOSAL_ALL()
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROPOSAL_ALL", sqlParameters, out sqlResult))
                return new List<Class_PROPOSAL>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PROPOSAL(sqlResult);
        }

        //成案檔的所有資料_BY新增者
        [WebMethod]
        public List<Class_PROPOSAL> fnGetTBPROPOSAL_ALL_BYUSERID(string strUSERID)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSCREATED_BY", strUSERID);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROPOSAL_ALL_BYUSERID", sqlParameters, out sqlResult))
                return new List<Class_PROPOSAL>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PROPOSAL(sqlResult);
        }

        //成案檔的所有資料_BY查詢區間及緊急成案
        [WebMethod]
        public List<Class_PROPOSAL> fnGetTBPROPOSAL_ALL_BYEMERGENCY(DateTime dtStart, DateTime dtEnd, string strProgID)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FDSTART", dtStart.ToShortDateString());
            sqlParameters.Add("FDEND", dtEnd.ToShortDateString());
            sqlParameters.Add("FSPROG_ID", strProgID.Trim());

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROPOSAL_ALL_BYEMERGENCY_DATE", sqlParameters, out sqlResult))
                return new List<Class_PROPOSAL>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PROPOSAL(sqlResult);
        }

        //新增成案主檔
        [WebMethod]
        public Boolean fnINSERTTBPROPOSAL(Class.Class_PROPOSAL obj)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPRO_ID", obj.FSPRO_ID);
            source.Add("FCPRO_TYPE", obj.FCPRO_TYPE);

            //1:新製節目; 2:續製節目; 3 衍生節目 (續製節目不重新取號)
            if (obj.FCPRO_TYPE == "2")
                source.Add("FSPROG_ID", obj.FSPROG_ID);
            else
                source.Add("FSPROG_ID", GetNoRecordPROG_ID(obj.FSPROG_NAME, obj.FSCREATED_BY));
            //by Jarvis20130429
            source.Add("FSPRODUCER", obj.FSPRODUCER);

            source.Add("FSPROG_NAME", obj.FSPROG_NAME);
            source.Add("FSPLAY_NAME", obj.FSPLAY_NAME);
            source.Add("FSNAME_FOR", obj.FSNAME_FOR);
            source.Add("FSCONTENT", obj.FSCONTENT);
            source.Add("FNLENGTH", obj.FNLENGTH.ToString());
            source.Add("FNBEG_EPISODE", obj.FNBEG_EPISODE.ToString());
            source.Add("FNEND_EPISODE", obj.FNEND_EPISODE.ToString());
            source.Add("FSCHANNEL", obj.FSCHANNEL);
            source.Add("FDSHOW_DATE", obj.FDSHOW_DATE.ToShortDateString());
            source.Add("FSPRD_DEPT_ID", obj.FSPRD_DEPT_ID);
            source.Add("FSPAY_DEPT_ID", obj.FSPAY_DEPT_ID);
            source.Add("FSSIGN_DEPT_ID", obj.FSSIGN_DEPT_ID);
            source.Add("FSBEF_DEPT_ID", obj.FSBEF_DEPT_ID);
            source.Add("FSAFT_DEPT_ID", obj.FSAFT_DEPT_ID);
            source.Add("FSOBJ_ID", obj.FSOBJ_ID);
            source.Add("FSGRADE_ID", obj.FSGRADE_ID);
            source.Add("FSSRC_ID", obj.FSSRC_ID);
            source.Add("FSTYPE", obj.FSTYPE);
            source.Add("FSAUD_ID", obj.FSAUD_ID);
            source.Add("FSSHOW_TYPE", obj.FSSHOW_TYPE);
            source.Add("FSLANG_ID_MAIN", obj.FSLANG_ID_MAIN);
            source.Add("FSLANG_ID_SUB", obj.FSLANG_ID_SUB);
            source.Add("FSBUY_ID", obj.FSBUY_ID);
            source.Add("FSBUYD_ID", obj.FSBUYD_ID);
            source.Add("FCCHECK_STATUS", obj.FCCHECK_STATUS);
            source.Add("FSCHANNEL_ID", obj.FSCHANNEL_ID);
            source.Add("FCEMERGENCY", obj.FCEMERGENCY);         //N-非緊急,Y-緊急成案
            source.Add("FSPRDCENID", obj.FSPRDCENID);
            source.Add("FSPROGSPEC", obj.FSPROGSPEC);
            source.Add("FCPROGD", obj.FCPROGD);
            source.Add("FSENOTE", obj.FSENOTE);

            source.Add("FSCREATED_BY", obj.FSCREATED_BY);
            source.Add("FSUPDATED_BY", obj.FSUPDATED_BY);
            source.Add("FSEXPIRE_DATE_ACTION", obj.FSEXPIRE_DATE_ACTION);           //到日後數位檔是否刪除
            source.Add("FDEXPIRE_DATE", obj.FDEXPIRE_DATE.ToString("yyyy-MM-dd HH:mm:ss"));                  //到期時間
            source.Add("FSPROGNATIONID", obj.FSPROGNATIONID);   //來源國家代碼

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBPROPOSAL", source, obj.FSUPDATED_BY);
        }

        //修改成案主檔
        /// <summary>
        /// 修改成案單用於PRO100_02
        /// </summary>
        /// <param name="obj">成案資料表欄位</param>
        /// <returns>true:成功 false:失敗</returns>
        [WebMethod]
        public Boolean fnUPDATETBPROPOSAL(Class.Class_PROPOSAL obj)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPRODUCER", obj.FSPRODUCER);
            source.Add("FSPRO_ID", obj.FSPRO_ID);
            source.Add("FCPRO_TYPE", obj.FCPRO_TYPE);
            source.Add("FSPROG_NAME", obj.FSPROG_NAME);
            source.Add("FSPLAY_NAME", obj.FSPLAY_NAME);
            source.Add("FSNAME_FOR", obj.FSNAME_FOR);
            source.Add("FSCONTENT", obj.FSCONTENT);
            source.Add("FNLENGTH", obj.FNLENGTH.ToString());
            source.Add("FNBEG_EPISODE", obj.FNBEG_EPISODE.ToString());
            source.Add("FNEND_EPISODE", obj.FNEND_EPISODE.ToString());
            source.Add("FSCHANNEL", obj.FSCHANNEL);
            source.Add("FDSHOW_DATE", obj.FDSHOW_DATE.ToShortDateString());
            source.Add("FSPRD_DEPT_ID", obj.FSPRD_DEPT_ID);
            source.Add("FSPAY_DEPT_ID", obj.FSPAY_DEPT_ID);
            source.Add("FSSIGN_DEPT_ID", obj.FSSIGN_DEPT_ID);
            source.Add("FSBEF_DEPT_ID", obj.FSBEF_DEPT_ID);
            source.Add("FSAFT_DEPT_ID", obj.FSAFT_DEPT_ID);
            source.Add("FSOBJ_ID", obj.FSOBJ_ID);
            source.Add("FSGRADE_ID", obj.FSGRADE_ID);
            source.Add("FSSRC_ID", obj.FSSRC_ID);
            source.Add("FSTYPE", obj.FSTYPE);
            source.Add("FSAUD_ID", obj.FSAUD_ID);
            source.Add("FSSHOW_TYPE", obj.FSSHOW_TYPE);
            source.Add("FSLANG_ID_MAIN", obj.FSLANG_ID_MAIN);
            source.Add("FSLANG_ID_SUB", obj.FSLANG_ID_SUB);
            source.Add("FSBUY_ID", obj.FSBUY_ID);
            source.Add("FSBUYD_ID", obj.FSBUYD_ID);
            source.Add("FCCHECK_STATUS", obj.FCCHECK_STATUS);
            source.Add("FSCHANNEL_ID", obj.FSCHANNEL_ID);
            source.Add("FCEMERGENCY", obj.FCEMERGENCY);         //N-非緊急,Y-緊急成案
            source.Add("FSPRDCENID", obj.FSPRDCENID);
            source.Add("FSPROGSPEC", obj.FSPROGSPEC);
            source.Add("FCPROGD", obj.FCPROGD);
            source.Add("FSENOTE", obj.FSENOTE);

            source.Add("FSEXPIRE_DATE_ACTION", obj.FSEXPIRE_DATE_ACTION);    //到期時動作
            source.Add("FDEXPIRE_DATE", obj.FDEXPIRE_DATE.ToString("yyyy-MM-dd HH:mm:ss"));       //到期日期
            source.Add("FSPROGNATIONID", obj.FSPROGNATIONID);               //來源國家ID

            source.Add("FSCREATED_BY", obj.FSCREATED_BY);
            source.Add("FSUPDATED_BY", obj.FSUPDATED_BY);
            //若有異動到期日期貨到期日動作寫入TBLOG_FILE_EXPIRE_DELETE資料庫
            if (obj.FDEXPIRE_DATE != obj.Origin_FDEXPIRE_DATE || obj.FSEXPIRE_DATE_ACTION != obj.Origin_FSEXPIRE_DATE_ACTION)
            {
                string myFSTABLE_NAME = "TBPROPOSAL：" + obj.FSPRO_ID;
                string myFSRECORD = obj.Origin_FDEXPIRE_DATE.ToString() + " => " + obj.FDEXPIRE_DATE.ToString() + " || " + obj.Origin_FSEXPIRE_DATE_ACTION + " => " + obj.FSEXPIRE_DATE_ACTION;
                INSERT_TBLOG_FILE_EXPIRE_DELETE(myFSTABLE_NAME, myFSRECORD, obj.FSUPDATED_BY);
            }

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBPROPOSAL", source, obj.FSUPDATED_BY);
        }

        public bool INSERT_TBLOG_FILE_EXPIRE_DELETE(string FSTABLE_NAME, string FSRECORD, string FSUPDATED_BY)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSTABLE_NAME", FSTABLE_NAME);
            source.Add("FSRECORD", FSRECORD);
            source.Add("FSUPDATED_BY", FSUPDATED_BY);
            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBLOG_FILE_EXPIRE_DELETE", source, FSUPDATED_BY);
        }

        //修改成案_BY緊急流程
        [WebMethod]
        public Boolean fnUPDATETBPROPOSAL_EMERGENCY(Class.Class_PROPOSAL obj)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPRO_ID", obj.FSPRO_ID);
            source.Add("FCCOMPLETE", obj.FCCOMPLETE);

            //if (obj.FDCOMPLETE == new DateTime(1900, 1, 1))
            //    source.Add("FDCOMPLETE", "");
            //else
            source.Add("FDCOMPLETE", obj.FDCOMPLETE.ToString("yyyy/MM/dd"));

            source.Add("FSUPDATED_BY", obj.FSUPDATED_BY);

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBPROPOSAL_EMERGENCY", source, obj.FSUPDATED_BY);
        }

        //查詢成案主檔
        [WebMethod]
        public List<Class_PROPOSAL> fnQUERYTBPROPOSAL(Class.Class_PROPOSAL obj, DateTime dtStart, DateTime dtEnd, string strStatus)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPRO_ID", obj.FSPRO_ID);
            source.Add("FCPRO_TYPE", obj.FCPRO_TYPE);
            source.Add("FSPROG_NAME", obj.FSPROG_NAME);
            source.Add("FSPLAY_NAME", obj.FSPLAY_NAME);
            source.Add("FSNAME_FOR", obj.FSNAME_FOR);
            source.Add("FSCONTENT", obj.FSCONTENT);

            if (obj.FNLENGTH == 0)
                source.Add("FNLENGTH", "");
            else
                source.Add("FNLENGTH", obj.FNLENGTH.ToString());

            if (obj.FNBEG_EPISODE == 0)
                source.Add("FNBEG_EPISODE", "");
            else
                source.Add("FNBEG_EPISODE", obj.FNBEG_EPISODE.ToString());

            if (obj.FNEND_EPISODE == 0)
                source.Add("FNEND_EPISODE", "");
            else
                source.Add("FNEND_EPISODE", obj.FNEND_EPISODE.ToString());

            source.Add("FSCHANNEL", obj.FSCHANNEL);

            if (obj.FDSHOW_DATE == new DateTime(1900, 1, 1))
                source.Add("FDSHOW_DATE", "");
            else
                source.Add("FDSHOW_DATE", obj.FDSHOW_DATE.ToString("yyyy/MM/dd"));

            source.Add("FSPRODUCER", obj.FSPRODUCER);
            source.Add("FSPRD_DEPT_ID", obj.FSPRD_DEPT_ID);
            source.Add("FSPAY_DEPT_ID", obj.FSPAY_DEPT_ID);
            source.Add("FSSIGN_DEPT_ID", "");
            source.Add("FSBEF_DEPT_ID", obj.FSBEF_DEPT_ID);
            source.Add("FSAFT_DEPT_ID", obj.FSAFT_DEPT_ID);
            source.Add("FSOBJ_ID", obj.FSOBJ_ID);
            source.Add("FSGRADE_ID", obj.FSGRADE_ID);
            source.Add("FSSRC_ID", obj.FSSRC_ID);
            source.Add("FSTYPE", obj.FSTYPE);
            source.Add("FSAUD_ID", obj.FSAUD_ID);
            source.Add("FSSHOW_TYPE", obj.FSSHOW_TYPE);
            source.Add("FSLANG_ID_MAIN", obj.FSLANG_ID_MAIN);
            source.Add("FSLANG_ID_SUB", obj.FSBUY_ID);
            source.Add("FSBUY_ID", obj.FSBUY_ID);
            source.Add("FSBUYD_ID", obj.FSBUYD_ID);
            source.Add("FCCHECK_STATUS", obj.FCCHECK_STATUS);
            source.Add("FSCHANNEL_ID", obj.FSCHANNEL_ID);
            source.Add("FCEMERGENCY", obj.FCEMERGENCY);         //N-非緊急,Y-緊急成案
            source.Add("FCCOMPLETE", obj.FCCOMPLETE);           //N-未補齊,Y-補齊
            source.Add("FSPRDCENID", obj.FSPRDCENID);
            source.Add("FSPROGSPEC", obj.FSPROGSPEC);
            source.Add("FCPROGD", obj.FCPROGD);
            source.Add("FSENOTE", obj.FSENOTE);
            source.Add("FDSTART", dtStart.ToShortDateString());
            source.Add("FDEND", dtEnd.ToShortDateString());
            source.Add("FCSTATUS", strStatus);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROPOSAL_BYCONDITIONS", source, out sqlResult))
                return new List<Class_PROPOSAL>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PROPOSAL(sqlResult);
        }

        //查詢成案主檔_BY新增者
        [WebMethod]
        public List<Class_PROPOSAL> fnQUERYTBPROPOSAL_BYUSERID(Class.Class_PROPOSAL obj, string strCREATED_BY)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSCREATED_BY", strCREATED_BY);
            source.Add("FCPRO_TYPE", obj.FCPRO_TYPE);
            source.Add("FSPROG_NAME", obj.FSPROG_NAME);
            source.Add("FSPLAY_NAME", obj.FSPLAY_NAME);
            source.Add("FSNAME_FOR", obj.FSNAME_FOR);
            source.Add("FSCONTENT", obj.FSCONTENT);

            if (obj.FNLENGTH == 0)
                source.Add("FNLENGTH", "");
            else
                source.Add("FNLENGTH", obj.FNLENGTH.ToString());

            if (obj.FNBEG_EPISODE == 0)
                source.Add("FNBEG_EPISODE", "");
            else
                source.Add("FNBEG_EPISODE", obj.FNBEG_EPISODE.ToString());

            if (obj.FNEND_EPISODE == 0)
                source.Add("FNEND_EPISODE", "");
            else
                source.Add("FNEND_EPISODE", obj.FNEND_EPISODE.ToString());

            source.Add("FSCHANNEL", obj.FSCHANNEL);

            if (obj.FDSHOW_DATE == new DateTime(1900, 1, 1))
                source.Add("FDSHOW_DATE", "");
            else
                source.Add("FDSHOW_DATE", obj.FDSHOW_DATE.ToString("yyyy/MM/dd"));

            source.Add("FSPRD_DEPT_ID", obj.FSPRD_DEPT_ID);
            source.Add("FSPAY_DEPT_ID", obj.FSPAY_DEPT_ID);
            source.Add("FSSIGN_DEPT_ID", "");
            source.Add("FSBEF_DEPT_ID", obj.FSBEF_DEPT_ID);
            source.Add("FSAFT_DEPT_ID", obj.FSAFT_DEPT_ID);
            source.Add("FSOBJ_ID", obj.FSOBJ_ID);
            source.Add("FSGRADE_ID", obj.FSGRADE_ID);
            source.Add("FSSRC_ID", obj.FSSRC_ID);
            source.Add("FSTYPE", obj.FSTYPE);
            source.Add("FSAUD_ID", obj.FSAUD_ID);
            source.Add("FSSHOW_TYPE", obj.FSSHOW_TYPE);
            source.Add("FSLANG_ID_MAIN", obj.FSLANG_ID_MAIN);
            source.Add("FSLANG_ID_SUB", obj.FSBUY_ID);
            source.Add("FSBUY_ID", obj.FSBUY_ID);
            source.Add("FSBUYD_ID", obj.FSBUYD_ID);
            source.Add("FCCHECK_STATUS", obj.FCCHECK_STATUS);
            source.Add("FSCHANNEL_ID", obj.FSCHANNEL_ID);
            source.Add("FCEMERGENCY", obj.FCEMERGENCY);         //N-非緊急,Y-緊急成案
            source.Add("FCCOMPLETE", obj.FCCOMPLETE);           //N-未補齊,Y-補齊
            source.Add("FSPRDCENID", obj.FSPRDCENID);
            source.Add("FSPROGSPEC", obj.FSPROGSPEC);
            source.Add("FCPROGD", obj.FCPROGD);
            source.Add("FSENOTE", obj.FSENOTE);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROPOSAL_BYCONDITIONS_BYUSERID", source, out sqlResult))
                return new List<Class_PROPOSAL>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PROPOSAL(sqlResult);
        }

        //查詢成案主檔透過成案編號
        [WebMethod]
        public List<Class_PROPOSAL> fnQUERYTBPROPOSAL_BYID(string strPRO_ID)
        {
            // SQL Parameters
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPRO_ID", strPRO_ID);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROPOSAL", source, out sqlResult))
                return new List<Class_PROPOSAL>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PROPOSAL(sqlResult);
        }

        //查詢緊急成案透過成案編號
        [WebMethod]
        public Boolean QUERYTBPROPOSAL_BYID_EMERGENCY(string strPRO_ID)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPRO_ID", strPRO_ID);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROPOSAL", source, out sqlResult))
                return false;

            if (sqlResult.Count > 0 && sqlResult[0]["FCEMERGENCY"] == "Y")
                return true;
            else
                return false;
        }

        //查詢成案主檔透過節目編號
        [WebMethod]
        public List<Class_PROPOSAL> QUERYTBPROPOSAL_BYPROGID(string strPROG_ID)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", strPROG_ID);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROPOSAL_BYPROG_ID", source, out sqlResult))
                return new List<Class_PROPOSAL>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PROPOSAL(sqlResult);
        }

        //查詢成案主檔透過節目編號_緊急成案是否已補齊資料,True-已補齊資料可入庫，False-未補齊資料不可入庫
        [WebMethod]
        public Boolean QUERYTBPROPOSAL_BYPROGID_EMERGENCY(string strPROG_ID)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", strPROG_ID);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROPOSAL_BYPROG_ID", source, out sqlResult))
                return true;

            for (int i = 0; i < sqlResult.Count; i++)
            {
                if (sqlResult[i]["FCEMERGENCY"] == "Y" && sqlResult[i]["FCCOMPLETE"] != "Y")
                {
                    return false;
                }
            }

            return true;
        }

        //查詢節目資料檔(要去擋重覆的節目名稱)
        [WebMethod]
        public Boolean QUERY_TBPROGNAME_CHECK(string strName)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPGMNAME", strName);

            List<Dictionary<string, string>> sqlResult;

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_M_BYCHECK", sqlParameters, out sqlResult))  //先判斷節目主檔
                return false;    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count > 0)
            {
                if (sqlResult[0]["dbCount"] == "0")
                    return false;
                else
                    return true;    //筆數不為零，表示有相同節目名稱的節目
            }
            else
                return false;
        }

        //核可/駁回 成案主檔
        [WebMethod]
        public Boolean fnUPDATETBPROPOSAL_Staus(string strPRO_ID, string strCHECK_STATUS, string strUPDATED_BY)
        {
            List<Class_PROPOSAL> Listobj = new List<Class_PROPOSAL>();
            Class_PROPOSAL obj = new Class_PROPOSAL();
            Boolean bolStatus = false;
            // SQL Parameters
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPRO_ID", strPRO_ID);
            source.Add("FCCHECK_STATUS", strCHECK_STATUS);
            source.Add("FSUPDATED_BY", strUPDATED_BY);

            bolStatus = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBPROPOSAL_STATUS", source, strUPDATED_BY);

            if (bolStatus == false)     //若是更新成案單狀態失敗，就要跳出
                return false;

            Listobj = fnQUERYTBPROPOSAL_BYID(strPRO_ID);

            if (Listobj.Count > 0)   //透過成案單撈出成案class
                obj = Listobj[0];
            else
                return false;

            if (bolStatus == true && strCHECK_STATUS != "X")
                EMAIL_PROPOSAL(obj, strCHECK_STATUS.Trim());        //當成案單核可或駁回後，EMAIL通知提出成案人員

            if (bolStatus == true && strCHECK_STATUS.Trim() == "Y") //當成案單核可後，要將成案單檔寫入節目管理系統的節目主檔、MAM的節目主檔
            {
                if (INSERT_TBPROGM_TBPROG(obj) == true)             //新增節目管理系統的節目主檔、MAM的節目主檔
                {
                    //新增節目製作人至TBPROG_PRODUCER by Jarvis2013/06/18
                    WSPROG_PRODUCER clientPRODUCER = new WSPROG_PRODUCER();
                    Class_PROGPRODUCER producerData = new Class_PROGPRODUCER();
                    producerData.FSPRO_TYPE = "G";
                    producerData.FSID = obj.FSPROG_ID;
                    producerData.FSPRODUCER = obj.FSPRODUCER;
                    producerData.FSCREATED_BY = obj.FSCREATED_BY;
                    producerData.FSUPDATED_BY = obj.FSCREATED_BY;
                  bool b=clientPRODUCER.INSERT_TBPROG_PRODUCER(producerData);
                   
                    //新增節目主檔後要檢查是否也要產生子集
                    if (obj.FCPROGD.Trim() == "Y")
                    {
                        WSPROG_M WSPROG_Mobj = new WSPROG_M();
                        Class_PROGM PROGMobj = new Class_PROGM();

                        PROGMobj = WSPROG_Mobj.fnGetProg_M(obj.FSPROG_ID.Trim())[0]; //查詢節目主檔

                        //產生節目子集
                        return (WSPROG_Mobj.INSERT_TBPROGD_TBPROG(PROGMobj, obj.FNBEG_EPISODE.ToString(), obj.FNEND_EPISODE.ToString(), PROGMobj.FSCRTUSER)).bolResult;
                    }
                    else
                        return true;
                }
                else
                    return false;
            }
            else if (bolStatus == true) //駁回或抽單
                return true;
            else
                return false;
        }

        //新增節目管理系統的節目主檔、MAM的節目主檔
        [WebMethod]
        public Boolean INSERT_TBPROGM_TBPROG(Class.Class_PROPOSAL obj)
        {
            ////檢查是否要寫回節目管理系統(暫用)   
            //MAM_PTS_DLL.SysConfig objDll = new MAM_PTS_DLL.SysConfig();
            //string strPGM = objDll.sysConfig_Read("/ServerConfig/ConnectPGM").Trim();
            string strContent = "";             //節目主檔的內容欄位

            if (obj.FCPRO_TYPE == "2")          //1:新製節目; 2:續製節目; 3 衍生節目
            {
                WSPROG_M ProgMobj = new WSPROG_M();
                List<Class_PROGM> ListProgMobj = new List<Class_PROGM>();
                ListProgMobj = ProgMobj.fnGetProg_M(obj.FSPROG_ID);

                if (ListProgMobj.Count > 0)
                    strContent = ListProgMobj[0].FSCONTENT.Trim();

                if (UPDATE_TBPGM(obj, strContent) == true)              //修改節目管理系統的節目主檔
                {
                    if (UPDATE_TBPROGM(obj, strContent) == true)        //修改MAM的節目主檔
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
            else
            {
                if (INSERT_TBPGM(obj) == true)              //寫入節目管理系統的節目主檔
                {
                    if (INSERT_TBPROGM(obj) == true)        //寫入MAM的節目主檔
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
        }

        /// <summary>
        /// 新增節目主檔_傳入成案的class(執行時機為成案單審核通過後)
        /// </summary>
        /// <param name="obj">傳入類別物件</param>
        /// <returns></returns>
        [WebMethod]
        public Boolean INSERT_TBPROGM(Class.Class_PROPOSAL obj)
        {
            string strBeg = "";
            string strEnd = "";
            Boolean bolDB = false;      //寫入節目管理系統資料庫結果
            string strError = "";        //錯誤訊息
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", obj.FSPROG_ID);
            source.Add("FSPGMNAME", obj.FSPROG_NAME);
            source.Add("FSPGMENAME", obj.FSNAME_FOR);
            source.Add("FSPRDDEPTID", obj.FSPRD_DEPT_ID);
            source.Add("FSCHANNEL", obj.FSCHANNEL);

            strBeg = obj.FNBEG_EPISODE.ToString().Trim();
            strEnd = obj.FNEND_EPISODE.ToString().Trim();

            //原本打算要用結束集別減去起始集別，得到總集別，但是目前直接取結束集別設定為總集別
            //if (strBeg == strEnd)
            //    source.Add("FNTOTEPISODE", "1");
            //else
            //    source.Add("FNTOTEPISODE", Convert.ToString(Convert.ToInt16(strEnd) - Convert.ToInt16(strBeg)));

            source.Add("FNTOTEPISODE", strEnd);

            source.Add("FNLENGTH", obj.FNLENGTH.ToString());

            source.Add("FSPROGOBJID", obj.FSOBJ_ID);
            source.Add("FSPROGSRCID", obj.FSSRC_ID);
            source.Add("FSPROGATTRID", obj.FSTYPE);
            source.Add("FSPROGAUDID", obj.FSAUD_ID);
            source.Add("FSPROGTYPEID", obj.FSSHOW_TYPE);
            source.Add("FSPROGGRADEID", obj.FSGRADE_ID);
            source.Add("FSPROGLANGID1", obj.FSLANG_ID_MAIN);
            source.Add("FSPROGLANGID2", obj.FSLANG_ID_SUB);
            source.Add("FSPROGSALEID", "");
            source.Add("FSPROGBUYID", obj.FSBUY_ID);
            source.Add("FSPROGBUYDID", obj.FSBUYD_ID);
            source.Add("FSPRDCENID", obj.FSPRDCENID);
            source.Add("FSPRDYYMM", "");
            source.Add("FDSHOWDATE", obj.FDSHOW_DATE.ToShortDateString());
            source.Add("FSCONTENT", obj.FSCONTENT);
            source.Add("FSMEMO", "");
            source.Add("FSEWEBPAGE", "");

            if (obj.FCPRO_TYPE == "3")         //1:新製節目; 2:續製節目; 3 衍生節目
                source.Add("FCDERIVE", "Y");
            else
                source.Add("FCDERIVE", "N");

            source.Add("FSWEBNAME", obj.FSPLAY_NAME);
            source.Add("FSCHANNEL_ID", obj.FSCHANNEL_ID);
            source.Add("FSDEL", "N");
            source.Add("FSDELUSER", "");
            source.Add("FCOUT_BUY", "N");
            source.Add("FNDEP_ID", Query_DEPID_BYUSERID(obj.FSCREATED_BY).ToString());
            source.Add("FSPROGSPEC", obj.FSPROGSPEC);

            source.Add("FSCRTUSER", obj.FSCREATED_BY);
            source.Add("FSUPDUSER", obj.FSCREATED_BY);

            //加入三個新增的欄位 FSPROGNATIONID、FSEXPIRE_DATE_ACTION、FDEXPIRE_DATE
            source.Add("FSEXPIRE_DATE_ACTION", obj.FSEXPIRE_DATE_ACTION);
            source.Add("FDEXPIRE_DATE", obj.FDEXPIRE_DATE.ToString("yyyy-MM-dd HH:mm:ss"));
            source.Add("FSPROGNATIONID", obj.FSPROGNATIONID);

            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBPROG_M", source, out strError, obj.FSCREATED_BY, true);

            if (bolDB == true)
                return true;
            else
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToMAM-SYSTEM INSERT PROGM", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "ERROR:" + strError);
                EMAIL_PROPOSAL_Error(obj, strError, "MAM", "INSERT");                 //寄送Mail
                DELETE_TBPROGM(obj.FSPROG_ID, obj.FSCREATED_BY);                    //刪除已新增節目管理系統的該筆節目主檔資料
                return false;
            }
        }

        /// <summary>
        /// 新增節目管理系統的節目主檔_傳入成案的class(執行時機為成案單審核通過後)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [WebMethod]
        public Boolean INSERT_TBPGM(Class.Class_PROPOSAL obj)
        {
            string strBeg = "";
            string strEnd = "";
            Boolean bolDB = false;      //寫入節目管理系統資料庫結果
            string strError = "";        //錯誤訊息
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROGID", obj.FSPROG_ID);
            source.Add("FSPGMNAME", obj.FSPROG_NAME);
            source.Add("FSPGMENAME", obj.FSNAME_FOR);
            source.Add("FSPRDDEPTID", obj.FSPRD_DEPT_ID);
            source.Add("FSCHANNEL", obj.FSCHANNEL);

            strBeg = obj.FNBEG_EPISODE.ToString().Trim();
            strEnd = obj.FNEND_EPISODE.ToString().Trim();

            //原本打算要用結束集別減去起始集別，得到總集別，但是目前直接取結束集別設定為總集別
            //if (strBeg == strEnd)
            //    source.Add("FNTOTEPISODE", "1");
            //else
            //    source.Add("FNTOTEPISODE", Convert.ToString(Convert.ToInt16(strEnd) - Convert.ToInt16(strBeg)));

            source.Add("FNTOTEPISODE", strEnd);
            source.Add("FNLENGTH", obj.FNLENGTH.ToString());
            source.Add("FSPROGOBJID", obj.FSOBJ_ID);
            source.Add("FSPROGSRCID", obj.FSSRC_ID);
            source.Add("FSPROGATTRID", obj.FSTYPE);
            source.Add("FSPROGAUDID", obj.FSAUD_ID);
            source.Add("FSPROGTYPEID", obj.FSSHOW_TYPE);
            source.Add("FSPROGGRADEID", obj.FSGRADE_ID);
            source.Add("FSPROGLANGID1", obj.FSLANG_ID_MAIN);
            source.Add("FSPROGLANGID2", obj.FSLANG_ID_SUB);
            source.Add("FSPROGSALEID", "");
            source.Add("FSPROGBUYID", obj.FSBUY_ID);
            source.Add("FSPROGBUYDID", obj.FSBUYD_ID);
            source.Add("FSPRDCENID", obj.FSPRDCENID);
            source.Add("FSPRDYYMM", "");

            if (obj.FDSHOW_DATE.ToShortDateString() == "1900/1/1")
                source.Add("FDSHOWDATE", "");
            else
                source.Add("FDSHOWDATE", obj.FDSHOW_DATE.ToShortDateString());

            source.Add("FSCONTENT", obj.FSCONTENT);
            source.Add("FSMEMO", "");
            source.Add("FSEWEBPAGE", "");

            if (obj.FCPRO_TYPE == "3")         //1:新製節目; 2:續製節目; 3 衍生節目
                source.Add("FCDERIVE", "Y");
            else
                source.Add("FCDERIVE", "N");

            source.Add("FSWEBNAME", obj.FSPLAY_NAME);
            source.Add("FSCRTUSER", "MAM");                  //節目管理系統定義由MAM寫入的資料USER皆為 MAM
            source.Add("FSPROGNATIONID", obj.FSPROGNATIONID);

            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction_With_DBLink("SP_I_TBPROG_M_EXTERNAL", source, out strError, obj.FSCREATED_BY, true);

            if (bolDB == true)
                return true;
            else
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToPGM-SYSTEM INSERT PROGM", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "ERROR:" + strError);
                EMAIL_PROPOSAL_Error(obj, strError, "PGM", "INSERT"); //寄送Mail
                return false;
            }
        }

        //修改節目主檔_傳入成案的class(執行時機為成案單審核通過後，並且節目種類為續製)
        [WebMethod]
        public Boolean UPDATE_TBPROGM(Class.Class_PROPOSAL obj, string strContent)
        {
            Boolean bolDB = false;          //寫入節目管理系統資料庫結果
            string strError = "";           //錯誤訊息
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", obj.FSPROG_ID);
            source.Add("FSPGMNAME", obj.FSPROG_NAME);
            source.Add("FSPGMENAME", obj.FSNAME_FOR);
            source.Add("FSPRDDEPTID", obj.FSPRD_DEPT_ID);
            source.Add("FSCHANNEL", obj.FSCHANNEL);
            source.Add("FNTOTEPISODE", obj.FNEND_EPISODE.ToString().Trim());
            source.Add("FNLENGTH", obj.FNLENGTH.ToString());
            source.Add("FSPROGOBJID", obj.FSOBJ_ID);
            source.Add("FSPROGSRCID", obj.FSSRC_ID);
            source.Add("FSPROGATTRID", obj.FSTYPE);
            source.Add("FSPROGAUDID", obj.FSAUD_ID);
            source.Add("FSPROGTYPEID", obj.FSSHOW_TYPE);
            source.Add("FSPROGGRADEID", obj.FSGRADE_ID);
            source.Add("FSPROGLANGID1", obj.FSLANG_ID_MAIN);
            source.Add("FSPROGLANGID2", obj.FSLANG_ID_SUB);
            source.Add("FSPROGBUYID", obj.FSBUY_ID);
            source.Add("FSPROGBUYDID", obj.FSBUYD_ID);
            source.Add("FSPRDCENID", obj.FSPRDCENID);
            source.Add("FDSHOWDATE", obj.FDSHOW_DATE.ToShortDateString());

            if (strContent.Trim() != "")
                source.Add("FSCONTENT", strContent + "\n" + obj.FSCONTENT);
            else
                source.Add("FSCONTENT", obj.FSCONTENT);

            source.Add("FCDERIVE", "");                      //續製的節目主檔不修改「衍生」欄位，預存程序會忽略掉此欄位
            source.Add("FSWEBNAME", obj.FSPLAY_NAME);
            source.Add("FSPROGSPEC", obj.FSPROGSPEC);
            source.Add("FSUPDUSER", obj.FSCREATED_BY);

            source.Add("FSEXPIRE_DATE_ACTION", obj.FSEXPIRE_DATE_ACTION);    //到期時動作
            source.Add("FDEXPIRE_DATE", obj.FDEXPIRE_DATE.ToString("yyyy-MM-dd HH:mm:ss"));       //到期日期
            source.Add("FSPROGNATIONID", obj.FSPROGNATIONID);               //來源國家ID

            //續製時記錄產生的日期
            string myFSTABLE_NAME = "TBPROPOSAL(續製)：" + obj.FSPRO_ID;
            string myFSRECORD = obj.Origin_FDEXPIRE_DATE.ToString() + " => " + obj.FDEXPIRE_DATE.ToString() + " || " + obj.Origin_FSEXPIRE_DATE_ACTION + " => " + obj.FSEXPIRE_DATE_ACTION;
            INSERT_TBLOG_FILE_EXPIRE_DELETE(myFSTABLE_NAME, myFSRECORD, obj.FSUPDATED_BY);


            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBPROG_M_BYPROPOSAL", source, out strError, obj.FSCREATED_BY, true);

            if (bolDB == true)
                return true;
            else
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToMAM-SYSTEM UPDATE PROGM", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "ERROR:" + strError);
                EMAIL_PROPOSAL_Error(obj, strError, "MAM", "UPDATE");             //寄送Mail
                return false;
            }
        }


        /// <summary>
        /// 修改節目管理系統的節目主檔_傳入成案的class(執行時機為成案單審核通過後，並且節目種類為續製)
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="strContent"></param>
        /// <returns></returns>
        [WebMethod]
        public Boolean UPDATE_TBPGM(Class.Class_PROPOSAL obj, string strContent)
        {
            Boolean bolDB = false;          //寫入節目管理系統資料庫結果
            string strError = "";           //錯誤訊息
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", obj.FSPROG_ID);
            source.Add("FSPGMNAME", obj.FSPROG_NAME);
            source.Add("FSPGMENAME", obj.FSNAME_FOR);
            source.Add("FSPRDDEPTID", obj.FSPRD_DEPT_ID);
            source.Add("FSCHANNEL", obj.FSCHANNEL);
            source.Add("FNTOTEPISODE", obj.FNEND_EPISODE.ToString().Trim());
            source.Add("FNLENGTH", obj.FNLENGTH.ToString());
            source.Add("FSPROGOBJID", obj.FSOBJ_ID);
            source.Add("FSPROGSRCID", obj.FSSRC_ID);
            source.Add("FSPROGATTRID", obj.FSTYPE);
            source.Add("FSPROGAUDID", obj.FSAUD_ID);
            source.Add("FSPROGTYPEID", obj.FSSHOW_TYPE);
            source.Add("FSPROGGRADEID", obj.FSGRADE_ID);
            source.Add("FSPROGLANGID1", obj.FSLANG_ID_MAIN);
            source.Add("FSPROGLANGID2", obj.FSLANG_ID_SUB);
            source.Add("FSPROGBUYID", obj.FSBUY_ID);
            source.Add("FSPROGBUYDID", obj.FSBUYD_ID);
            source.Add("FSPRDCENID", obj.FSPRDCENID);
            source.Add("FDSHOWDATE", obj.FDSHOW_DATE.ToShortDateString());

            if (strContent.Trim() != "")
                source.Add("FSCONTENT", strContent + "\n" + obj.FSCONTENT);
            else
                source.Add("FSCONTENT", obj.FSCONTENT);

            source.Add("FCDERIVE", "");                      //續製的節目主檔不修改「衍生」欄位，預存程序會忽略掉此欄位
            source.Add("FSWEBNAME", obj.FSPLAY_NAME);
            source.Add("FSUPDUSER", "MAM");                  //節目管理系統定義由MAM寫入的資料USER皆為 MAM
            source.Add("FSPROGNATIONID", obj.FSPROGNATIONID);   //傳入國家代碼

            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction_With_DBLink("SP_U_TBPROG_M_BYPROPOSAL_EXTERNAL", source, out strError, obj.FSCREATED_BY, true);

            if (bolDB == true)
                return true;
            else
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToPGM-SYSTEM UPDATE PROGM", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "ERROR:" + strError);
                EMAIL_PROPOSAL_Error(obj, strError, "PGM", "UPDATE");             //寄送Mail
                return false;
            }
        }

        //寄送EMAIL通知_傳入成案的class、狀態(執行時機為成案單審核通過或不通過後)
        [WebMethod]
        public void EMAIL_PROPOSAL(Class.Class_PROPOSAL obj, string strSTATUS)
        {
            string strPROG_NAME = obj.FSPROG_NAME.Trim();

            StringBuilder msg = new StringBuilder();
            msg.AppendLine("成案單編號：" + obj.FSPRO_ID.Trim());

            if (obj.FCPRO_TYPE.Trim() == "1")
                msg.AppendLine(" , 成案種類：新製節目");
            else if (obj.FCPRO_TYPE.Trim() == "2")
                msg.AppendLine(" , 成案種類：續製節目");
            else if (obj.FCPRO_TYPE.Trim() == "3")
                msg.AppendLine(" , 成案種類：衍生節目");

            msg.AppendLine(" , 節目名稱：" + strPROG_NAME);
            msg.AppendLine(" , 節目編號：" + obj.FSPROG_ID.Trim());

            if (strSTATUS.Trim() == "Y")
                msg.AppendLine(" , 審核結果：通過");
            else if (strSTATUS.Trim() == "F")
                msg.AppendLine(" , 審核結果：不通過");

            MAM_PTS_DLL.Protocol.SendEmail(obj.FSCREATED_BY_EMAIL.Trim(), "成案審核結果通知-" + strPROG_NAME, msg.ToString());  //MAIL          
        }

        //寄送EMAIL通知_資料同步時發生問題要通知管理者
        [WebMethod]
        public void EMAIL_PROPOSAL_Error(Class.Class_PROPOSAL obj, string strError, string strSystem, string strStatus)
        {
            //讀取通知的管理者(暫用)   
            MAM_PTS_DLL.SysConfig objDll = new MAM_PTS_DLL.SysConfig();
            string strErrorMail = objDll.sysConfig_Read("/ServerConfig/SynchronizeError").Trim();

            string strPROG_NAME = obj.FSPROG_NAME.Trim();

            StringBuilder msg = new StringBuilder();
            msg.AppendLine("成案單編號：" + obj.FSPRO_ID.Trim());

            if (obj.FCPRO_TYPE.Trim() == "1")
                msg.AppendLine(" , 成案種類：新製節目");
            else if (obj.FCPRO_TYPE.Trim() == "2")
                msg.AppendLine(" , 成案種類：續製節目");
            else if (obj.FCPRO_TYPE.Trim() == "3")
                msg.AppendLine(" , 成案種類：衍生節目");

            msg.AppendLine(" , 節目名稱：" + strPROG_NAME);
            msg.AppendLine(" , 節目編號：" + obj.FSPROG_ID.Trim());

            if (strSystem.Trim() == "MAM")
                msg.AppendLine(" , 同步失敗資料庫：數位片庫");
            else if (strSystem.Trim() == "PGM")
                msg.AppendLine(" , 同步失敗資料庫：節目管理系統");

            if (strSystem.Trim() == "INSERT")
                msg.AppendLine(" , 動作：新增");
            else if (strSystem.Trim() == "UPDATE")
                msg.AppendLine(" , 動作：修改");

            msg.AppendLine(" , 錯誤訊息：" + strError);

            msg.AppendLine(" , 新增者：" + obj.FSCREATED_BY.Trim() + " , 修改者：" + obj.FSUPDATED_BY.Trim());

            MAM_PTS_DLL.Protocol.SendEmail(strErrorMail, "成案資料同步失敗通知-" + strPROG_NAME, msg.ToString());
        }

        //新增節目主檔_傳入成案的class(執行時機為成案單審核通過後)
        [WebMethod]
        public void DELETE_TBPROGM(string strPROGID, string strUser)
        {
            string strError = "";        //錯誤訊息
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROGID", strPROGID);
            MAM_PTS_DLL.DbAccess.Do_Transaction_With_DBLink("SP_D_TBPROG_M_EXTERNAL", source, out strError, strUser, true);
        }

        //列印成案表單
        [WebMethod]
        public string CallREPORT_PRO(string strProID, string CREATED_BY, string strSessionID)
        {
            String strURL = "";  //要傳回去的URL

            //取列印報表的系統設定檔
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string reportSrv = SettingObj.sysConfig_Read("/ServerConfig/ReportServer");

            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPRO_ID", strProID);            //成案單編號
            source.Add("QUERY_BY", CREATED_BY);          //查詢者
            source.Add("FSSESSION_ID", strSessionID);    //SessionID

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_R_TBPROPOSAL_01_QRY", source, out sqlResult))
                return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count > 0)
            {
                if (sqlResult[0]["QUERY_KEY"].ToString().StartsWith("ERROR:SESSION_TIMEOUT"))
                    return "錯誤，登入時間過久，請重新登入後才能啟動報表功能！";
                else if (sqlResult[0]["QUERY_KEY"].ToString().StartsWith("ERROR:SESSION_NOTEXIST"))
                    return "錯誤，使用者未正常登入，請重新登入後才能啟動報表功能！";
                else if (sqlResult[0]["QUERY_KEY"].ToString().StartsWith("ERROR"))
                    return "";
                else
                {
                    strURL = reportSrv + "RPT_TBPROPOSAL_01&QUERY_KEY=" + sqlResult[0]["QUERY_KEY"] + "&rc:parameters=false&rs:Command=Render";
                    return Server.UrlEncode(strURL);
                }
            }
            else
                return "";
        }

        //取得下載說明文件網址
        [WebMethod]
        public string CallREPORT_DOC()
        {
            String strURL = "";  //要傳回去的URL

            strURL = "http://mam.pts.org.tw/MAM系統操作手冊.doc";
            return Server.UrlEncode(strURL);
        }

        //透過帳號查詢使用者所屬的部門_因為成案時要記錄成案部門
        public int Query_DEPID_BYUSERID(string strUserID)
        {
            string strUpperDept_ID = "0";    //頻道
            string strDep_ID = "0";          //部門
            string strGroup_ID = "0";        //組別

            Dictionary<string, string> source = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            source.Add("FSUSER_ID", strUserID);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBUSERS_DEPID_PROPOSAL", source, out sqlResult))
                return 0;

            if (sqlResult.Count > 0)
            {
                strUpperDept_ID = sqlResult[0]["FNUpperDept_ID"];
                strDep_ID = sqlResult[0]["FNDep_ID"];
                strGroup_ID = sqlResult[0]["FNGroup_ID"];

                if (strGroup_ID != "0")
                    return Convert.ToInt32(strGroup_ID);

                if (strDep_ID != "0")
                    return Convert.ToInt32(strDep_ID);

                if (strUpperDept_ID != "0")
                    return Convert.ToInt32(strUpperDept_ID);

                return 0;
            }
            else
                return 0;
        }

        //查詢製作目的與前後會部門對照檔列表
        [WebMethod]
        public List<Class_PROGOBJ_DEPT> QueryTBPROGOBJ_DEPT_LIST()
        {
            List<Class_CODE> m_CodeDataObj = new List<Class_CODE>();       //代碼檔集合
            List<Class_PROGOBJ_DEPT> returnData = new List<Class_PROGOBJ_DEPT>();
            m_CodeData = MAMFunctions.Transfer_Class_CODE("SP_Q_TBPROG_M_CODE");

            for (int i = 0; i < m_CodeData.Count; i++)
            {
                if (m_CodeData[i].TABLENAME == "TBZPROGOBJ" && m_CodeData[i].ID.Trim() != "")
                {
                    //從所有代碼裡找出製作目的代碼
                    m_CodeDataObj.Add(m_CodeData[i]);
                }
            }

            for (int i = 0; i < m_CodeDataObj.Count; i++)
            {
                //再從製作目的代碼找出製作目的與前後會部門對照檔
                returnData.Add(QueryPROGOBJ_DEPT(m_CodeDataObj[i].ID.Trim()));
            }

            return returnData;
        }

        //查詢製作目的與前後會部門對照檔        
        public Class_PROGOBJ_DEPT QueryPROGOBJ_DEPT(string strOBJ_ID)
        {
            Class_PROGOBJ_DEPT obj = new Class_PROGOBJ_DEPT();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSOBJ_ID", strOBJ_ID);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROGOBJ_DEPT_BYID", sqlParameters, out sqlResult) || sqlResult.Count == 0)
            {
                obj.FSOBJ_ID = strOBJ_ID;
                obj.FSBEF_DEPT_ID = "";
                obj.FSAFT_DEPT_ID = "";
                return obj;
            }

            obj.FSOBJ_ID = sqlResult[0]["FSOBJ_ID"];                                    //製作目的
            obj.FSBEF_DEPT_ID = sqlResult[0]["FSBEF_DEPT_ID"];                          //前會部門
            obj.FSAFT_DEPT_ID = sqlResult[0]["FSAFT_DEPT_ID"]; ;                        //後會部門
            obj.FSCREATED_BY = sqlResult[0]["FSCREATED_BY"];                            //建檔者
            obj.FSCREATED_BY_NAME = sqlResult[0]["FSCREATED_BY_NAME"];                  //建檔者(顯示)         
            obj.FDCREATED_DATE = Convert.ToDateTime(sqlResult[0]["WANT_FDCREATEDDATE"]);//建檔日期
            obj.SHOW_FDCREATED_DATE = MAMFunctions.compareDate(sqlResult[0]["WANT_FDCREATEDDATE"]);  //建檔日期(顯示)

            obj.FSUPDATED_BY = sqlResult[0]["FSUPDATED_BY"];                             //修改者
            obj.FSUPDATED_BY_NAME = sqlResult[0]["FSUPDATED_BY_NAME"];                   //修改者(顯示)
            obj.FDUPDATED_DATE = Convert.ToDateTime(sqlResult[0]["WANT_FDUPDATEDDATE"]); //修改日期
            obj.SHOW_FDUPDATED_DATE = MAMFunctions.compareDate(sqlResult[0]["WANT_FDUPDATEDDATE"]);//修改日期(顯示)
            obj.SHOW_FSBEF_DEPT_ID = "";                                                 //前會部門(顯示)-前端處理
            obj.SHOW_FSAFT_DEPT_ID = "";                                                 //後會部門(顯示)-前端處理

            return obj;
        }

        //修改製作目的與前後會部門對照檔
        [WebMethod]
        public Boolean UPDATETBPROGOBJ_DEPT_LIST(List<Class_PROGOBJ_DEPT> Listobj, string strUser)
        {
            for (int i = 0; i < Listobj.Count; i++)
            {
                if (UPDATETBPROGOBJ_DEPT(Listobj[i], strUser) == false)
                    return false;
            }
            return true;
        }

        //修改製作目的與前後會部門對照檔
        public Boolean UPDATETBPROGOBJ_DEPT(Class_PROGOBJ_DEPT obj, string strUser)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSOBJ_ID", obj.FSOBJ_ID);
            source.Add("FSBEF_DEPT_ID", obj.FSBEF_DEPT_ID);
            source.Add("FSAFT_DEPT_ID", obj.FSAFT_DEPT_ID);
            source.Add("FSUPDATED_BY", strUser);

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBPROGOBJ_DEPT", source, strUser);
        }

        //比對代碼檔_節目規格
        string compareCode_Code_ProgSpec(string strID, string strTable)
        {
            string strProgSpec = "";

            for (int i = 0; i < m_CodeData.Count; i++)
            {
                if (strID.IndexOf(m_CodeData[i].ID.ToString().Trim()) > -1 && m_CodeData[i].TABLENAME.ToString().Trim().Equals(strTable))
                {
                    strProgSpec = strProgSpec + m_CodeData[i].NAME.ToString().Trim() + " ";
                }
            }
            return strProgSpec;
        }

    }
}
