﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Text;
using System.Xml.Linq;
using System.IO;
using PTS_MAM3.Web.Class;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSPROGLINK 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSPROGLINK : System.Web.Services.WebService
    {
        public struct ReturnMsg     //錯誤訊息處理
        {
            public bool bolResult;
            public string strErrorMsg;
        }

        //解析資料庫回來的Dictionary，轉存成Class
        private List<Class_PROGLINK> Transfer_PROGLINK(List<Dictionary<string, string>> sqlResult)
        {
            List<Class_PROGLINK> returnData = new List<Class_PROGLINK>();         

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_PROGLINK obj = new Class_PROGLINK();
                // --------------------------------                          
                obj.FSPROG_ID = sqlResult[i]["FSPROG_ID"];                           //節目編號
                obj.FSPROG_ID_NAME = sqlResult[i]["FSPROG_ID_NAME"];                 //節目名稱
                obj.FNEPISODE = Convert.ToInt16(sqlResult[i]["FNEPISODE"]);          //集別
                obj.SHOW_FNEPISODE = MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]);//集別(顯示)
                obj.FNEPISODE_NAME = sqlResult[i]["FNEPISODE_NAME"];                 //子集名稱

                obj.FSORGPROG_ID = sqlResult[i]["FSORGPROG_ID"];                     //扣播出次數節目編號
                obj.FSORGPROG_ID_NAME = sqlResult[i]["FSORGPROG_ID_NAME"];           //扣播出次數節目名稱
                obj.FNORGEPISODE = Convert.ToInt16(sqlResult[i]["FNORGEPISODE"]);    //扣播出次數集別
                obj.SHOW_ORGFNEPISODE = sqlResult[i]["FNORGEPISODE"];                //扣播出次數集別(顯示)
                obj.FNORGEPISODE_NAME = sqlResult[i]["FNORGEPISODE_NAME"];           //扣播出次數子集名稱
                obj.SHOW_FNORGEPISODE_PLAY = sqlResult[i]["FNORGEPISODE_PLAY"];      //扣播出次數播出集別(顯示)
                obj.FSMEMO = sqlResult[i]["FSMEMO"];                                 //備註               

                obj.FSCREATED_BY_NAME = sqlResult[i]["FSCREATED_BY_NAME"];               //建檔者
                obj.FDCREATED_DATE = Convert.ToDateTime(sqlResult[i]["WANT_FDCREATEDDATE"]);  //建檔日期
                obj.SHOW_FDCREATED_DATE = MAMFunctions.compareDate(sqlResult[i]["WANT_FDCREATEDDATE"]);  //建檔日期(顯示)
                obj.FSUPDATED_BY_NAME = sqlResult[i]["FSUPDATED_BY_NAME"];               //修改者
                // --------------------------------
                returnData.Add(obj);
            }

            return returnData;
        } 

        //相關人員檔的所有資料
        [WebMethod]
        public List<Class_PROGLINK> GetTBPROGLINK_ALL()
        {           
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROGLINK_ALL", sqlParameters, out sqlResult))
                return new List<Class_PROGLINK>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PROGLINK(sqlResult);  
        }

        //新增MAM的扣播出次數節目子集檔
        [WebMethod]
        public List<ReturnMsg> INSERT_TBPROLINK_BATCH(List<Class.Class_PROGLINK> Listobj)
        {            
            List<ReturnMsg> ListobjMsg = new  List<ReturnMsg>();

            for (int i = 0; i < Listobj.Count; i++)
            {   
                ListobjMsg.Add(INSERT_TBPROLINK(Listobj[i]));
            }

            return ListobjMsg;
        }
               
        //新增扣播出次數節目子集檔
        [WebMethod]
        public ReturnMsg INSERT_TBPROLINK(Class.Class_PROGLINK obj)
        {
            ReturnMsg objMsg = new ReturnMsg();

            if (INSERT_TBPROLINK_PGM(obj, out objMsg.strErrorMsg) == true)              //新增節目管理系統的扣播出次數節目子集檔
            {
                if (INSERT_TBPROLINK_MAM(obj, out objMsg.strErrorMsg) == true)          //新增MAM的扣播出次數節目子集檔
                    objMsg.bolResult = true;
                else
                    objMsg.bolResult = false;
            }
            else
                objMsg.bolResult = false;

            return objMsg;           
        }

        //新增MAM的扣播出次數節目子集檔
        [WebMethod]
        public Boolean INSERT_TBPROLINK_MAM(Class.Class_PROGLINK obj, out string exceptionMsg)
        {
            Boolean bolDB = false;          //寫入節目管理系統資料庫結果
            string strError = "";           //錯誤訊息

            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", obj.FSPROG_ID);

            if (obj.FNEPISODE.ToString() == "32767")
                source.Add("FNEPISODE", "");
            else
                source.Add("FNEPISODE", obj.FNEPISODE.ToString());

            source.Add("FSORGPROG_ID", obj.FSORGPROG_ID);

            if (obj.FNORGEPISODE.ToString() == "32767")
                source.Add("FNORGEPISODE", "");
            else
                source.Add("FNORGEPISODE", obj.FNORGEPISODE.ToString());

            source.Add("FSMEMO", obj.FSMEMO);
            source.Add("FSCREATED_BY", obj.FSCREATED_BY);
            source.Add("FSUPDATED_BY", obj.FSUPDATED_BY);

            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBPROGLINK", source, out strError, obj.FSUPDATED_BY, true);
            exceptionMsg = strError;

            if (bolDB == true)
                return true;
            else
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToMAM-INSERT PROGLINK", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "ERROR:" + strError);
                EMAIL_PROGLINK_Error(obj, strError, "MAM", "INSERT");             //寄送Mail
                return false;
            }  
        }

        //新增節目管理系統的扣播出次數節目子集檔
        [WebMethod]
        public Boolean INSERT_TBPROLINK_PGM(Class.Class_PROGLINK obj, out string exceptionMsg)
        {
            Boolean bolDB = false;          //寫入節目管理系統資料庫結果
            string strError = "";           //錯誤訊息

            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROGID", obj.FSPROG_ID);

            if (obj.FNEPISODE.ToString() == "32767")
                source.Add("FNEPISODE", "");
            else
                source.Add("FNEPISODE", obj.FNEPISODE.ToString());

            source.Add("FSORGPROGID", obj.FSORGPROG_ID);

            if (obj.FNORGEPISODE.ToString() == "32767")
                source.Add("FNORGEPISODE", "");
            else
                source.Add("FNORGEPISODE", obj.FNORGEPISODE.ToString());

            source.Add("FSCRTUSER", "MAM");                     //節目管理系統定義由MAM寫入的資料USER皆為 MAM 

            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction_With_DBLink("SP_I_TBPROGLINK_EXTERNAL", source, out strError, obj.FSUPDATED_BY, true);
            exceptionMsg = strError;

            if (bolDB == true)
                return true;
            else
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToPGM-INSERT PROGLINK", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "ERROR:" + strError);
                EMAIL_PROGLINK_Error(obj, strError, "PGM", "INSERT");                   //寄送Mail
                return false;
            }
        }

        //修改扣播出次數節目子集檔(因為節目管理系統無修改備註欄位功能，因此修改只有修改MAM，不修改節目管理系統)
        [WebMethod]
        public ReturnMsg UPDATE_TBPROGLINK(Class.Class_PROGLINK obj)
        {
            ReturnMsg objMsg = new ReturnMsg();

            //if (UPDATE_TBPROGLINK_PGM(obj, out objMsg.strErrorMsg) == true)              //修改節目管理系統的扣播出次數節目子集檔
            //{                
            //}
            //else
            //    objMsg.bolResult = false;

            if (UPDATE_TBPROGLINK_MAM(obj, out objMsg.strErrorMsg) == true)          //修改MAM的扣播出次數節目子集檔
                objMsg.bolResult = true;
            else
                objMsg.bolResult = false;

            return objMsg;
        }

        //修改MAM的扣播出次數節目子集檔
        [WebMethod]
        public Boolean UPDATE_TBPROGLINK_MAM(Class.Class_PROGLINK obj, out string exceptionMsg)
        {
            Boolean bolDB = false;          //寫入節目管理系統資料庫結果
            string strError = "";           //錯誤訊息

            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", obj.FSPROG_ID);

            if (obj.FNEPISODE.ToString() == "32767")
                source.Add("FNEPISODE", "");
            else
                source.Add("FNEPISODE", obj.FNEPISODE.ToString());

            source.Add("FSORGPROG_ID", obj.FSORGPROG_ID);

            if (obj.FNORGEPISODE.ToString() == "32767")
                source.Add("FNORGEPISODE", "");
            else
                source.Add("FNORGEPISODE", obj.FNORGEPISODE.ToString());

            source.Add("FSMEMO", obj.FSMEMO);
            source.Add("FSUPDATED_BY", obj.FSUPDATED_BY);

            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBPROGLINK", source, out strError, obj.FSUPDATED_BY, true);
            exceptionMsg = strError;

            if (bolDB == true)
                return true;
            else
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToMAM-UPDATE PROGLINK", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "ERROR:" + strError);
                EMAIL_PROGLINK_Error(obj, strError, "MAM", "UPDATE");             //寄送Mail
                return false;
            } 
        }

        //修改節目管理系統的扣播出次數節目子集檔
        [WebMethod]
        public Boolean UPDATE_TBPROGLINK_PGM(Class.Class_PROGLINK obj, out string exceptionMsg)
        {
            Boolean bolDB = false;          //寫入節目管理系統資料庫結果
            string strError = "";           //錯誤訊息

            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROGID", obj.FSPROG_ID);

            if (obj.FNEPISODE.ToString() == "32767")
                source.Add("FNEPISODE", "");
            else
                source.Add("FNEPISODE", obj.FNEPISODE.ToString());

            source.Add("FSORGPROGID", obj.FSORGPROG_ID);

            if (obj.FNORGEPISODE.ToString() == "32767")
                source.Add("FNORGEPISODE", "");
            else
                source.Add("FNORGEPISODE", obj.FNORGEPISODE.ToString());

            source.Add("FSUPDUSER", "MAM");                   //節目管理系統定義由MAM寫入的資料USER皆為 MAM 

            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction_With_DBLink("SP_U_TBPROGLINK_EXTERNAL", source, out strError, obj.FSUPDATED_BY, true);
            exceptionMsg = strError;

            if (bolDB == true)
                return true;
            else
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToPGM-UPDATE PROGLINK", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "ERROR:" + strError);
                EMAIL_PROGLINK_Error(obj, strError, "PGM", "UPDATE");                   //寄送Mail
                return false;
            }
        }

        //查詢扣播出次數節目子集檔
        [WebMethod]
        public List<Class_PROGLINK> QUERY_TBPROGLINK(Class.Class_PROGLINK obj)
        {            
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", obj.FSPROG_ID);

            if (obj.FNEPISODE.ToString() == "32767")
                source.Add("FNEPISODE", "");
            else
                source.Add("FNEPISODE", obj.FNEPISODE.ToString());

            source.Add("FSORGPROG_ID", obj.FSORGPROG_ID);

            if (obj.FNORGEPISODE.ToString() == "32767")
                source.Add("FNORGEPISODE", "");
            else
                source.Add("FNORGEPISODE", obj.FNORGEPISODE.ToString());

            source.Add("FSMEMO", "");       //查詢不用備註查

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROGLINK_BYCONDITIONS", source, out sqlResult))
                return new List<Class_PROGLINK>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PROGLINK(sqlResult);
        }

        //查詢扣播出次數節目子集檔(避免重複Key)
        [WebMethod]
        public string QUERY_TBPROGLINK_CHECK(Class.Class_PROGLINK obj)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", obj.FSPROG_ID);

            if (obj.FNEPISODE.ToString() == "32767")
                source.Add("FNEPISODE", "");
            else
                source.Add("FNEPISODE", obj.FNEPISODE.ToString());

            source.Add("FSORGPROG_ID", obj.FSORGPROG_ID);

            if (obj.FNORGEPISODE.ToString() == "32767")
                source.Add("FNORGEPISODE", "");
            else
                source.Add("FNORGEPISODE", obj.FNORGEPISODE.ToString());

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROGLINK_BYCHECK", source, out sqlResult))
                return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count > 0)
                return sqlResult[0]["dbCount"];
            else
                return "";
        }

        //查詢某節目是否被設定為衍生節目
        [WebMethod]
        public Boolean QUERY_TBPROGLINK_BYPROGID_BYCHECK_LINK(string strProgID)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", strProgID);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROGLINK_BYPROGID_BYCHECK_LINK", source, out sqlResult))
                return false;    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count > 0 && sqlResult[0]["dbCount"] != "0")
                return true;
            else
                return false;
        }

        //查詢某節目是否被設定為扣播出次數節目
        [WebMethod]
        public Boolean QUERY_TBPROGLINK_BYPROGID_BYCHECK_LINK_PLAY(string strProgID)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", strProgID);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROGLINK_BYPROGID_BYCHECK_LINK_PLAY", source, out sqlResult))
                return false;    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count > 0 && sqlResult[0]["dbCount"] != "0")
                return true;
            else
                return false;
        }

        //刪除扣播出次數節目子集檔
        [WebMethod]
        public ReturnMsg DELETE_TBPROGLINK(Class.Class_PROGLINK obj, string strDelUser)
        {
            ReturnMsg objMsg = new ReturnMsg();

            if (DELETE_TBPROGLINK_PGM(obj, out objMsg.strErrorMsg, strDelUser) == true)              //刪除節目管理系統的扣播出次數節目子集檔
            {
                if (DELETE_TBPROGLINK_MAM(obj, out objMsg.strErrorMsg, strDelUser) == true)          //刪除MAM的扣播出次數節目子集檔
                    objMsg.bolResult = true;
                else
                    objMsg.bolResult = false;
            }
            else
                objMsg.bolResult = false;

            return objMsg; 
        }

        //刪除MAM的扣播出次數節目子集檔
        [WebMethod]
        public Boolean DELETE_TBPROGLINK_MAM(Class.Class_PROGLINK obj, out string exceptionMsg, string strDelUser)
        {
            Boolean bolDB = false;          //寫入節目管理系統資料庫結果
            string strError = "";           //錯誤訊息

            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", obj.FSPROG_ID);
            source.Add("FNEPISODE", obj.FNEPISODE.ToString());
            source.Add("FSORGPROG_ID", obj.FSORGPROG_ID);
            source.Add("FNORGEPISODE", obj.FNORGEPISODE.ToString());

            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_TBPROGLINK", source, out strError, strDelUser, true);
            exceptionMsg = strError;

            if (bolDB == true)
                return true;
            else
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToMAM-DELETE PROGLINK", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "ERROR:" + strError);
                EMAIL_PROGLINK_Error(obj, strError, "MAM", "DELETE");             //寄送Mail
                return false;
            }
        }

        //刪除節目管理系統的扣播出次數節目子集檔
        [WebMethod]
        public Boolean DELETE_TBPROGLINK_PGM(Class.Class_PROGLINK obj, out string exceptionMsg, string strDelUser)
        {
            Boolean bolDB = false;          //寫入節目管理系統資料庫結果
            string strError = "";           //錯誤訊息

            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROGID", obj.FSPROG_ID);
            source.Add("FNEPISODE", obj.FNEPISODE.ToString());
            source.Add("FSORGPROGID", obj.FSORGPROG_ID);
            source.Add("FNORGEPISODE", obj.FNORGEPISODE.ToString());

            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction_With_DBLink("SP_D_TBPROGLINK_EXTERNAL", source, out strError, strDelUser, true);
            exceptionMsg = strError;

            if (bolDB == true)
                return true;
            else
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToPGM-DELETE PROGLINK", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "ERROR:" + strError);
                EMAIL_PROGLINK_Error(obj, strError, "PGM", "DELETE");                   //寄送Mail
                return false;
            }
        }

        //寄送EMAIL通知_資料同步時發生問題要通知管理者
        [WebMethod]
        public void EMAIL_PROGLINK_Error(Class.Class_PROGLINK obj, string strError, string strSystem, string strStatus)
        {
            //讀取通知的管理者(暫用)   
            MAM_PTS_DLL.SysConfig objDll = new MAM_PTS_DLL.SysConfig();
            string strErrorMail = objDll.sysConfig_Read("/ServerConfig/SynchronizeError").Trim();
            string strPROG_NAME = MAMFunctions.QueryProgName(obj.FSPROG_ID.Trim());
            string strTitle = "";

            StringBuilder msg = new StringBuilder();
            msg.AppendLine("衍生節目名稱：" + strPROG_NAME);
            msg.AppendLine(" , 衍生節目編號：" + obj.FSPROG_ID.Trim());
            msg.AppendLine(" , 衍生集別：" + obj.FNEPISODE.ToString().Trim());

            msg.AppendLine(" , 扣播出次數節目名稱：" + MAMFunctions.QueryProgName(obj.FSORGPROG_ID.Trim()));
            msg.AppendLine(" , 扣播出次數節目編號：" + obj.FSORGPROG_ID.Trim());
            msg.AppendLine(" , 扣播出次數集別：" + obj.FNORGEPISODE.ToString().Trim());

            if (strSystem.Trim() == "MAM")
                msg.AppendLine(" , 同步失敗資料庫：數位片庫");
            else if (strSystem.Trim() == "PGM")
                msg.AppendLine(" , 同步失敗資料庫：節目管理系統");

            if (strStatus.Trim() == "INSERT")
                msg.AppendLine(" , 動作：新增");
            else if (strStatus.Trim() == "UPDATE")
                msg.AppendLine(" , 動作：修改");
            else if (strStatus.Trim() == "DELETE")
                msg.AppendLine(" , 動作：刪除");

            msg.AppendLine(" , 錯誤訊息：" + strError);

            msg.AppendLine(" , 新增者：" + obj.FSCREATED_BY.Trim() + " , 修改者：" + obj.FSUPDATED_BY.Trim());

            strTitle = "節目衍生子集資料同步失敗通知-" + strPROG_NAME;

            MAM_PTS_DLL.Protocol.SendEmail(strErrorMail, strTitle, msg.ToString());
        }
    }
}
