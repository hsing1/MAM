﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using PTS_MAM3.Web.Class;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSTAPE 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSTAPE : System.Web.Services.WebService
    {
        List<Class_CODE_TBFILE_TYPE> m_CodeDataFILE_TYPE = new List<Class_CODE_TBFILE_TYPE>();       //代碼檔集合

        //解析資料庫回來的Dictionary，轉存成Class
        private List<Class_TAPE_LABEL_CLIP> Transfer_TAPE_LABEL_CLIP(List<Dictionary<string, string>> sqlResult)
        {
            List<Class_TAPE_LABEL_CLIP> returnData = new List<Class_TAPE_LABEL_CLIP>();

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_TAPE_LABEL_CLIP obj = new Class_TAPE_LABEL_CLIP();
                // --------------------------------    
                obj.FSBIB = sqlResult[i]["BIB"];                                    //影帶管理系統識別
                obj.FSTCSTART = sqlResult[i]["TCStart"];                            //起始點
                obj.FSTCEND = sqlResult[i]["TCEnd"];                                //結束點
                obj.FSDURATION = sqlResult[i]["Duration"];                          //長度
                obj.FSCLIPTITLE = sqlResult[i]["ClipTitle"];                        //單元標題

                obj.FSCREATED_BY_NAME = sqlResult[i]["Creator"];                    //建檔者(顯示)
                obj.FSUPDATED_BY_NAME = sqlResult[i]["Updator"];                    //修改者(顯示)
                obj.FDCREATED_DATE = Convert.ToDateTime(sqlResult[i]["WANT_FDCREATEDDATE"]);            //建檔日期
                obj.SHOW_FDCREATED_DATE = MAMFunctions.compareDate(sqlResult[i]["WANT_FDCREATEDDATE"]); //建檔日期(顯示)
                // --------------------------------
                returnData.Add(obj);
            }

            return returnData;
        }

        //解析資料庫回來的Dictionary，轉存成Class
        private List<Class_TAPE_LABEL_TAPE> Transfer_TAPE_LABEL(List<Dictionary<string, string>> sqlResult, string Episode)
        {
            m_CodeDataFILE_TYPE = MAMFunctions.Transfer_Class_CODE_TBFILE_TYPE("SP_Q_TBBROADCAST_CODE_TBFILE_TYPE");
            List<Class_TAPE_LABEL_TAPE> returnData = new List<Class_TAPE_LABEL_TAPE>();
            string strFSTTYPE = "";
            string strFSTTYPE_NAME = "";

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_TAPE_LABEL_TAPE obj = new Class_TAPE_LABEL_TAPE();
                CheckTAPE_LABEL_TYPE(sqlResult[i]["TVolume"], sqlResult[i]["TTapeCode_Name"], out strFSTTYPE, out strFSTTYPE_NAME);

                // --------------------------------    
                obj.FSTAPEID = sqlResult[i]["TapeID"];                              //影帶編號
                obj.FSBIB = sqlResult[i]["BIB"];                                    //影帶管理系統識別
                obj.FSTVOLUME = sqlResult[i]["TVolume"];                            //部冊號
                obj.FSTLOCATION = sqlResult[i]["TLocation"];                        //放置位置
                obj.FSFILETYPE = strFSTTYPE;                                        //檔案代碼
                obj.FSFILETYPE_NAME = strFSTTYPE_NAME;                              //檔案名稱                
                obj.FSTTYPE = sqlResult[i]["TType"];                                //館藏類型代碼
                obj.FSTTYPE_NAME = sqlResult[i]["TType_Name"];                      //館藏類型名稱
                obj.FSTTAPECODE = sqlResult[i]["TTapeCode"];                        //影帶規格代碼
                obj.FSTTAPECODE_NAME = sqlResult[i]["TTapeCode_Name"];              //影帶規格名稱
                obj.FSTAPE_TITLE = sqlResult[i]["TAPE_TITLE"];                      //影帶標題
                obj.FSTAPE_DES = sqlResult[i]["TAPE_DES"];                          //影帶描述
                obj.FSDURATION = sqlResult[i]["TAPE_DURATION"];                     //影帶長度

                obj.FSCREATED_BY_NAME = sqlResult[i]["Creator"];                    //建檔者(顯示)
                obj.FSUPDATED_BY_NAME = sqlResult[i]["Updator"];                    //修改者(顯示)
                obj.FDCREATED_DATE = Convert.ToDateTime(sqlResult[i]["WANT_FDCREATEDDATE"]);            //建檔日期
                obj.SHOW_FDCREATED_DATE = MAMFunctions.compareDate(sqlResult[i]["WANT_FDCREATEDDATE"]); //建檔日期(顯示)
                // --------------------------------
                returnData.Add(obj);
            }

            return returnData;
        }

        //查詢影帶段落檔
        [WebMethod]
        public List<Class_TAPE_LABEL_CLIP> GetTAPE_CLIP_BYPROGID_EPISODE(string ProgID, string Episode)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSPROG_ID", ProgID);
            sqlParameters.Add("FNEPISODE", Episode);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_PTSTAPE_LABEL_CLIP_BYPROGID_EPISODE", sqlParameters, out sqlResult))
                return new List<Class_TAPE_LABEL_CLIP>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count == 0)
                return GetTAPE_CLIP_BYPROGID_EPISODE_NOFILTER(ProgID, Episode);
            else
                return Transfer_TAPE_LABEL_CLIP(sqlResult);
        }

        //查詢影帶段落檔_不作段落資料集別的過濾
        [WebMethod]
        public List<Class_TAPE_LABEL_CLIP> GetTAPE_CLIP_BYPROGID_EPISODE_NOFILTER(string ProgID, string Episode)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSPROG_ID", ProgID);
            sqlParameters.Add("FNEPISODE", Episode);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_PTSTAPE_LABEL_CLIP_BYPROGID_EPISODE_NOFILTER", sqlParameters, out sqlResult))
                return new List<Class_TAPE_LABEL_CLIP>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_TAPE_LABEL_CLIP(sqlResult);
        }


        //查詢影帶館藏檔
        [WebMethod]
        public List<Class_TAPE_LABEL_TAPE> GetTAPE_LABEL_BYPROGID_EPISODE(string ProgID, string Episode)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSPROG_ID", ProgID);
            sqlParameters.Add("FNEPISODE", Episode);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_PTSTAPE_LABEL_BYPROGID_EPISODE", sqlParameters, out sqlResult))
                return new List<Class_TAPE_LABEL_TAPE>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count == 0)
               return GetTAPE_LABEL_BYPROGID_EPISODE_NOFILTER(ProgID, Episode);
            else
                return Transfer_TAPE_LABEL(sqlResult,Episode);
        }

        //查詢影帶館藏檔_不作館藏資料的過濾
        public List<Class_TAPE_LABEL_TAPE> GetTAPE_LABEL_BYPROGID_EPISODE_NOFILTER(string ProgID, string Episode)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSPROG_ID", ProgID);
            sqlParameters.Add("FNEPISODE", Episode);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_PTSTAPE_LABEL_BYPROGID_EPISODE_NOFILTER", sqlParameters, out sqlResult))
                return new List<Class_TAPE_LABEL_TAPE>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_TAPE_LABEL(sqlResult, Episode);
        }

        //查詢影帶的檔案類型透過影帶的部冊號、影帶規格名稱
        public void CheckTAPE_LABEL_TYPE(string strTvolume,string strTTapeName ,out string strFileType_ID, out string strFileType_Name)
        {
            string strSpec = "";    //影帶規格
            strFileType_ID = "";
            strFileType_Name = "";            

            if (strTvolume.Trim() == "") 
                return;

            if (strTTapeName.Trim() == "")
                return;

            if (strTTapeName.Trim().IndexOf("HD") > -1)     //影帶規格名稱裡有HD的字，表示要比對HD，否則就比對SD
                strSpec = "02";
            else
                strSpec = "01";

            for (int i = 0; i < m_CodeDataFILE_TYPE.Count; i++)
            {
                //用部冊號去比對影的備註欄位
                if (m_CodeDataFILE_TYPE[i].FSTYPE.Trim() == "影" && m_CodeDataFILE_TYPE[i].FSSPEC.Trim() == strSpec)
                {
                    char[] delimiterChars = { ';' };
                    string[] words = m_CodeDataFILE_TYPE[i].FSTAPE_NOTE.Trim().Split(delimiterChars);

                    for (int j = 0; j < words.Length; j++)
                    {
                        if (words[j].Trim() != "" && strTvolume.Trim().IndexOf(words[j].Trim()) >-1)     //EndsWith(原本比對最後面，但發現會有c.mb (CHI/TAI)情形，因此就不能只比對後面)
                        {
                            strFileType_ID = m_CodeDataFILE_TYPE[i].ID;
                            strFileType_Name = m_CodeDataFILE_TYPE[i].NAME;
                            return;
                        }
                    }
                }
            }

            strFileType_ID = "";
            strFileType_Name = "";
        }

        //傳入入庫影像及影帶段落，新增入庫影像檔及段落檔(影帶回溯用)
        [WebMethod]
        public Boolean INSERT_TBLOG_VIDEO_TAPE(List<Class_LOG_VIDEO> ListVIDEOobj, List<Class_TAPE_LABEL_CLIP> ListCLIPobj,string strType, string strFSID, string strEpisode, string strUpdated_By ,string strChannelID,string strProgName,string strEpisodeName,string audioTrack)
        {
            try
            {
                WSMAMFunctions MAMobj = new WSMAMFunctions();   //取得VideoID
                Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
                WSBROADCAST Broobj = new WSBROADCAST();   //透過WSBROADCAST的新增段落function   
                string strFileNo = "";                    //檔案編號

                foreach (Class_LOG_VIDEO VIDEOobj in ListVIDEOobj)
                {
                    strFileNo = Broobj.fnGetFileID(strType, strFSID, strEpisode, strUpdated_By, strChannelID, strProgName, strEpisodeName);

                    //接收回來的參數，前者是檔案編號，後者是DIRID   
                    char[] delimiterChars = { ';' };
                    string[] words = strFileNo.Split(delimiterChars);

                    if (words.Length == 2)
                    {
                        VIDEOobj.FSFILE_NO = words[0];
                        VIDEOobj.FSSUBJECT_ID = words[0].Trim().Substring(0, 12);
                        VIDEOobj.FNDIR_ID = Convert.ToInt32(words[1]);
                        VIDEOobj.FSVIDEO_PROG = MAMobj.QueryVideoID_PROG_ARC_TYPE(strFSID, strEpisode, VIDEOobj.FSARC_TYPE.Trim());
                        VIDEOobj.FSTRACK = audioTrack;
                    }
                    else
                        return false;   //取得檔案編號異常，回傳錯誤               

                    if (Broobj.INSERT_LOG_VIDEO(VIDEOobj) == true)
                        INSERT_TBLOG_VIDEO_SEG_TAPE(ListCLIPobj, VIDEOobj.FSFILE_NO, VIDEOobj.FSVIDEO_PROG, VIDEOobj.FSCREATED_BY);
                    else
                        return false;
                }

                Broobj.Dispose();
            }
            catch (Exception ex) 
            {
            
            }
            return true;
        }

        //傳入影帶段落到節目段落檔(影帶回溯用)
        public Boolean INSERT_TBLOG_VIDEO_SEG_TAPE(List<Class_TAPE_LABEL_CLIP> ListCLIPobj, string strFileNo,string strVideoID , string strCreated_BY)
        {
            WSMAMFunctions MAMobj = new WSMAMFunctions();   //取得VideoID用建立的object
            Int16 i = 1;                                    //段落序號，因為影帶傳入時沒有段落序號，所以自己給

            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            foreach (Class_TAPE_LABEL_CLIP CLIPobj in ListCLIPobj)
            {
                sqlParameters.Add("FSFILE_NO", strFileNo);
                sqlParameters.Add("FNSEG_ID", i.ToString().PadLeft(2, '0'));
                sqlParameters.Add("FSVIDEO_ID", strVideoID.Trim().Substring(0, 8) + i.ToString().PadLeft(2, '0'));  //回溯只有節目，因此videoID要加序號
                sqlParameters.Add("FSBEG_TIMECODE", CLIPobj.FSTCSTART);
                sqlParameters.Add("FSEND_TIMECODE", CLIPobj.FSTCEND);
                sqlParameters.Add("FCLOW_RES", "N");  //第一次新增時狀態為：N→未開始轉檔
                sqlParameters.Add("FSCREATED_BY", strCreated_BY);
                sqlParameters.Add("FSUPDATED_BY", strCreated_BY);

                if (!MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBLOG_VIDEO_SEG", sqlParameters, strCreated_BY))
                    return false;

                sqlParameters.Clear();

                i++;
            }
            MAMobj.Dispose();
            return true;
        }

        //查詢入庫影像檔(避免一筆以上的SD播出帶或是HD播出帶)，傳入入庫影像檔
        [WebMethod]
        public string QUERY_TBLOG_VIDEO_BYProgID_Episode_LIST_CHECK(List<Class_LOG_VIDEO> objLOG_VIDEO)
        {
            string strReturn = "";

            for (int i = 0; i < objLOG_VIDEO.Count; i++)
            {
                if (objLOG_VIDEO[i].FSARC_TYPE.ToString() == "001" || objLOG_VIDEO[i].FSARC_TYPE.ToString() == "002")
                {
                    if (QUERY_TBLOG_VIDEO_BYProgID_Episode_CHECK(objLOG_VIDEO[i].FSTYPE.ToString(), objLOG_VIDEO[i].FSID.ToString(), objLOG_VIDEO[i].FNEPISODE.ToString(), objLOG_VIDEO[i].FSARC_TYPE.ToString(), objLOG_VIDEO[i].FSCHANGE_FILE_NO.ToString().Trim()) == true)
                        strReturn = strReturn + QUERY_ARCTPYE(objLOG_VIDEO[i].FSARC_TYPE.ToString().Trim()) + "已存在一筆\n";
                        
                        //strReturn = strReturn + objLOG_VIDEO[i].FSARC_TYPE_NAME + "已存在一筆\n";
                }
            }

            return strReturn;
        }

        //查詢入庫影像檔(影帶回溯，每一種類型都只能有一筆，用意是提醒片庫人員，避免重複入庫)，傳入入庫影像檔
        [WebMethod]
        public string QUERY_TBLOG_VIDEO_BYProgID_Episode_LIST_CHECK_ALL(List<Class_LOG_VIDEO> objLOG_VIDEO)
        {
            string strReturn = "";

            for (int i = 0; i < objLOG_VIDEO.Count; i++)
            {
                if (QUERY_TBLOG_VIDEO_BYProgID_Episode_CHECK(objLOG_VIDEO[i].FSTYPE.ToString(), objLOG_VIDEO[i].FSID.ToString(), objLOG_VIDEO[i].FNEPISODE.ToString(), objLOG_VIDEO[i].FSARC_TYPE.ToString(), objLOG_VIDEO[i].FSCHANGE_FILE_NO.ToString().Trim()) == true)
                    strReturn = strReturn + "「" + objLOG_VIDEO[i].FSARC_TYPE_NAME + "」已存在一筆\n";
            }

            return strReturn;
        } 

        //查詢入庫影像檔(避免一筆以上的SD播出帶或是HD播出帶)
        [WebMethod]
        public Boolean QUERY_TBLOG_VIDEO_BYProgID_Episode_CHECK(string strType, string strProgID, string strEpisode, string strArcType, string strChangeFileNo)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSTYPE", strType);
            source.Add("FSID", strProgID);
            source.Add("FNEPISODE", strEpisode);
            source.Add("FSARC_TYPE", strArcType);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_BY_ID_EPISODE_ARCTPYE_CHECK", source, out sqlResult))
                return false;    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count > 0)
            {
                if (strChangeFileNo.Trim() == "")
                    return true;    //沒有置換資料表示不需要檢查，即可確定是有SD播出帶或HD播出帶，因此不能簽收
                else if (sqlResult[0]["FSFILE_NO"] == strChangeFileNo)
                    return false;   //要置換的是系統裡找到的SD播出帶或HD播出帶，因此要能簽收，表示是走置換流程
                else
                    return true;    //要置換的不是系統裡找到的SD播出帶或HD播出帶，因此不能簽收
            }
            else
                return false;
        }

        //查詢入庫影像檔(避免一筆以上的SD播出帶或是HD播出帶)，傳入入庫影像檔
        [WebMethod]
        public string QUERY_ARCTPYE(string ARCID)
        {
            List<Class_CODE_TBFILE_TYPE> m_CodeDataFILE_TYPE = new List<Class_CODE_TBFILE_TYPE>();
            m_CodeDataFILE_TYPE = MAMFunctions.Transfer_Class_CODE_TBFILE_TYPE("SP_Q_TBBROADCAST_CODE_TBFILE_TYPE");

            for (int i = 0; i < m_CodeDataFILE_TYPE.Count; i++)
            {
                if (m_CodeDataFILE_TYPE[i].ID.Trim() == ARCID)
                    return m_CodeDataFILE_TYPE[i].NAME.Trim();                
            }
            return "";
        }




    }
}
