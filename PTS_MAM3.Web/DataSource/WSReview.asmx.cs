﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using PTS_MAM3.Web.Class;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSReview 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSReview : System.Web.Services.WebService
    {
        [WebMethod]
        public List<Class_ReviewTree> TreeStructure()
        {
            List<Class_ReviewTree> rtnList = new List<Class_ReviewTree>();
            
            return rtnList;
        }
        [WebMethod]
        public List<Class_ReviewTree> GetCatalog_Years(Class.Class_ReviewTree iniClass)
        {
            List<Class_ReviewTree> rtnList = new List<Class_ReviewTree>();
            List<Dictionary<string, string>> sqlResult;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("StationID", iniClass.StationID);
            sqlParameters.Add("CatalogID", iniClass.CatalogID);
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_REVIEWTREE_GETYEARS", sqlParameters, out sqlResult))
                return new List<Class_ReviewTree>();
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ReviewTree obj = new Class_ReviewTree();
                obj.CatalogID = iniClass.CatalogID;
                obj.FSID = iniClass.FSID;
                obj.FSNAME = iniClass.FSNAME;
                obj.StationID = iniClass.StationID;
                obj.YearID = sqlResult[i]["YEARS"];
                rtnList.Add(obj);
            }
            return rtnList;
        }

        [WebMethod]
        public List<Class_ReviewTree> GetFSID_FSNAME(Class_ReviewTree iniClass)
        {
            List<Class_ReviewTree> rtnList = new List<Class_ReviewTree>();
            List<Dictionary<string, string>> sqlResult;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("StationID", iniClass.StationID);
            sqlParameters.Add("CatalogID", iniClass.CatalogID);
            sqlParameters.Add("YearID", iniClass.YearID);
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_REVIEWTREE_FSID_FSNAME", sqlParameters, out sqlResult))
                return new List<Class_ReviewTree>();
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ReviewTree obj = new Class_ReviewTree();
                obj.CatalogID = iniClass.CatalogID;
                obj.FSID = sqlResult[i]["FSID"];
                obj.FSNAME = sqlResult[i]["FSNAME"];
                obj.StationID = iniClass.StationID;
                obj.YearID = iniClass.YearID;
                rtnList.Add(obj);
            }
            return rtnList;
        }
        [WebMethod]
        public List<Class_ReviewDetailList> GetVAPDCountList(Class_ReviewTree iniClass)
        {
            List<Class_ReviewDetailList> rtnList = new List<Class_ReviewDetailList>();
            List<Dictionary<string, string>> sqlResult;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("CatalogID", iniClass.CatalogID);
            sqlParameters.Add("FSID", iniClass.FSID);
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_REVIEWLIST_VAPDCount", sqlParameters, out sqlResult))
                return new List<Class_ReviewDetailList>();
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ReviewDetailList obj = new Class_ReviewDetailList();
                obj.CatalogID = iniClass.CatalogID;
                obj.FSID = sqlResult[i]["FSID"];
                obj.FSNAME = sqlResult[i]["NAME"];
                obj.FSEPISODEID = sqlResult[i]["EPISODE"];
                obj.CountV = sqlResult[i]["VCount"];
                obj.CountA = sqlResult[i]["ACount"];
                obj.CountP = sqlResult[i]["PCount"];
                obj.CountD = sqlResult[i]["DCount"];
                rtnList.Add(obj);
            }
            return rtnList;
        }

        //修改入庫MetaData後新增的方法,先帶入名稱查詢ID在執行原本的GetVAPDCountList方法 by Jarvis20130709
        [WebMethod]
        public List<Class_ReviewDetailList> GetVAPDCountList_NEW(string CatalogID, string NAME)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("CatalogID", CatalogID);
            sqlParameters.Add("FSPGMNAME", NAME);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_SelecedtFileID", sqlParameters, out sqlResult))
                return new List<Class_ReviewDetailList>();


            if (sqlResult.Count > 0)
            {
                Class_ReviewTree crt = new Class_ReviewTree();
                crt.CatalogID = CatalogID;
                crt.FSID = sqlResult[0]["ID"];
               
                return GetVAPDCountList(crt);
            }
            return new List<Class_ReviewDetailList>();

        }


        // 根據頻道、類型、年分尋找入庫列表 by Jarvis20130710
        [WebMethod]
        public List<Class_ReviewTree> GetFileList_NEW(string CatalogID, string channelName,string year )
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("type", CatalogID);
            sqlParameters.Add("channelName", channelName);
            sqlParameters.Add("year", year);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_FileList_FOR_METADATA", sqlParameters, out sqlResult))
                return new List<Class_ReviewTree>();

            List<Class_ReviewTree> rtnList = new List<Class_ReviewTree>();

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ReviewTree obj = new Class_ReviewTree();
                obj.CatalogID = CatalogID;
                obj.FSID = sqlResult[i]["ID"];
                obj.FSNAME = sqlResult[i]["FSDIR"];
                obj.YearID = year;
                //Todo....
                rtnList.Add(obj);
            }
            return rtnList;

        }
        

        //獲取根據頻道與節目類型取得年分列表 by Jarvis20130709
        [WebMethod]
        public List<Class_ReviewTree> Get_Channel_YEARS(string channelName,string type)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("ChannelName", channelName);
            sqlParameters.Add("type", type);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_MetaDataYears", sqlParameters, out sqlResult))
                return new List<Class_ReviewTree>();

            List<Class_ReviewTree> promoData = new System.Collections.Generic.List<Class_ReviewTree>();
            foreach (Dictionary<string, string> dic in sqlResult)
            {
                Class_ReviewTree crt = new Class_ReviewTree();
                //crt.CatalogID = "P";
                //crt.FSID = dic["FSPROMO_ID"];
                //crt.FSNAME = dic["FSPROMO_NAME"];
                //crt.StationID = dic["FSMAIN_CHANNEL_ID"];
                //crt.YearID = dic["Years"];
                //promoData.Add(crt);

                crt.CatalogID = type;
                //crt.FSID = dic["FSPROMO_ID"];
                //crt.FSNAME = dic["FSPROMO_NAME"];
                //crt.StationID = dic["FSMAIN_CHANNEL_ID"];
                crt.YearID = dic["Years"];
                promoData.Add(crt);
            }
            return promoData;
        }

        [WebMethod]
        public List<Class_GPD_Name> GetGPDNameList(string iniName)
        {
            List<Class_GPD_Name> rtnList = new List<Class_GPD_Name>();
            List<Dictionary<string, string>> sqlResult;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSNAME", iniName);
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_REVIEWLIST_GPD_NAME", sqlParameters, out sqlResult))
                return new List<Class_GPD_Name>();
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_GPD_Name obj = new Class_GPD_Name();
                obj.GPD = sqlResult[i]["GPD"];
                obj.GPD_Name = sqlResult[i]["GPD_Name"];
                obj.FSID = sqlResult[i]["FSID"];
                obj.FSNAME = sqlResult[i]["FSNAME"];
                rtnList.Add(obj);
            }
            return rtnList;
        }

        [WebMethod]
        public List<ClassReviewFile> GetVAPDFILES(Class_ReviewDetailList iniClass, string iniFILETYPE,string categroyID)
        {
            List<ClassReviewFile> rtnList = new List<ClassReviewFile>();
            List<Dictionary<string, string>> sqlResult;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSEPISODE", iniClass.FSEPISODEID);
            sqlParameters.Add("FSID", iniClass.FSID);
            sqlParameters.Add("FILETYPE", iniFILETYPE);
            sqlParameters.Add("FSCATEGORY_ID", categroyID);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_REVIEW_VAPDFILES", sqlParameters, out sqlResult))
                return new List<ClassReviewFile>();
            for (int i = 0; i < sqlResult.Count; i++)
            {
                ClassReviewFile obj = new ClassReviewFile();
                obj.FSFILE_NO = sqlResult[i]["FSFILE_NO"];
                obj.FSFILE_PATH = sqlResult[i]["FSFILE_PATH"];
                obj.FSFILE_TYPE = sqlResult[i]["FSFILE_TYPE"];
                obj.FSOLD_FILE_NAME = sqlResult[i]["FSOLD_FILE_NAME"];
                obj.FSTITLE = sqlResult[i]["FSTITLE"];
                obj.FSTB = iniFILETYPE;
                rtnList.Add(obj);
            }
            return rtnList;
        }
    }
}
