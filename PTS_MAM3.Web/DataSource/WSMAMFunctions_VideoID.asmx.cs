﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Text;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSMAMFunctions_VideoID 的摘要描述
    /// </summary>
    public partial class WSMAMFunctions : System.Web.Services.WebService
    {
        // 錯誤的 VideoID，為八個空白字元
        public const string ERROR_VIDEOID = "        ";

        /// <summary>
        /// 依照查詢的條件，回傳 或 新增+回傳
        /// </summary>
        /// <param name="fsID">ProgID</param>
        /// <param name="fsEpisode">子集數</param>
        /// <param name="fsChannelID">頻道ID</param>
        /// <returns></returns>
        [WebMethod]
        public string QueryVideoID_PROG(string fsID, string fsEpisode, string fsChannel_ID)
        {
            // 取得該頻道的FSCHANNEL_TYPE
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSCHANNEL_ID", fsChannel_ID);
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBZCHANNEL", sqlParameters, out resultData) || resultData.Count == 0)
            {
                string errMsg = string.Concat("無法找到相關的頻道資料，channelID = ", fsChannel_ID);
                MAM_PTS_DLL.Log.AppendTrackingLog("WSMAMFunctions_VideoID/QueryVideoID_PROG", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, errMsg);
                return string.Empty;
            }

            return QueryVideoID_PROG_ARC_TYPE(fsID, fsEpisode, resultData[0]["FSCHANNEL_TYPE"]);
        }

        /// <summary>
        /// 依照查詢的條件，回傳 或 新增+回傳
        /// </summary>
        /// <param name="fsID">ProgID</param>
        /// <param name="fsEpisode">子集數</param>
        /// <param name="fsARC_TYPE">檔案類型</param>
        /// <returns></returns>
        [WebMethod]
        public string QueryVideoID_PROG_ARC_TYPE(string fsID, string fsEpisode, string fsARC_TYPE)
        {
            // 先查詢在 TBLOG_VIDEOID_MAP 中是否已經存在指定條件的VideoID
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSID", fsID);
            source.Add("FNEPISODE", fsEpisode);
            source.Add("FSARC_TYPE", fsARC_TYPE);
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEOID_MAP", source, out resultData))
            {
                string errMsg = string.Concat("執行 SP_Q_TBLOG_VIDEOID_MAP 時發生錯誤回傳，參數：", fsID, ", ", fsEpisode, ", ", fsARC_TYPE);
                MAM_PTS_DLL.Log.AppendTrackingLog("WSMAMFunctions_VideoID/QueryVideoID_PROG_ARC_TYPE", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return ERROR_VIDEOID;
            }

            // 如果沒有查到東西的話，就做新增的動作
            if (resultData.Count == 0)
            {
                // 目前沒有VideoID的存在，需新增

                // 取得新的 VideoID
                string returnVideoID = GetNewVideoID(string.Concat(fsID, ",", fsEpisode, ",", fsARC_TYPE));

                if (!string.IsNullOrEmpty(returnVideoID))
                {
                    // 將取得的VideoID寫入VideoID_MAP資料表
                    source = new Dictionary<string, string>();
                    source.Add("FSID", fsID);
                    source.Add("FNEPISODE", fsEpisode);
                    source.Add("FSARC_TYPE", fsARC_TYPE);
                    source.Add("FSVIDEO_ID_PROG", returnVideoID);
                    if (!MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBLOG_VIDEOID_MAP", source, "system"))
                    {
                        string errMsg = string.Concat("執行 SP_I_TBLOG_VIDEOID_MAP 時發生錯誤回傳，參數：", fsID, ", ", fsEpisode, ", ", fsARC_TYPE, ", ", returnVideoID);
                        MAM_PTS_DLL.Log.AppendTrackingLog("WSMAMFunctions_VideoID/QueryVideoID_PROG_ARC_TYPE", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                        return ERROR_VIDEOID;
                    }

                    // 當新增"成功"後，最後再做一次檢查
                    // 註：因為發生過在申請時沒發生，但新增時同樣新增兩筆，但後來上了 PK 限制，所以發生相同狀況時，只會有一筆被寫入，此時再查一次，可以選出被註冊的那筆
                    source = new Dictionary<string, string>();
                    source.Add("FSID", fsID);
                    source.Add("FNEPISODE", fsEpisode);
                    source.Add("FSARC_TYPE", fsARC_TYPE);
                    resultData = new List<Dictionary<string, string>>();
                    if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEOID_MAP", source, out resultData) || resultData.Count == 0)
                    {
                        string errMsg = string.Concat("第二次執行 SP_Q_TBLOG_VIDEOID_MAP 時發生錯誤回傳，參數：", fsID, ", ", fsEpisode, ", ", fsARC_TYPE);
                        MAM_PTS_DLL.Log.AppendTrackingLog("WSMAMFunctions_VideoID/QueryVideoID_PROG_ARC_TYPE", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                        return ERROR_VIDEOID;
                    }
                    else
                        return resultData[0]["FSVIDEO_ID_PROG"];
                }
                else
                {
                    // 當發生錯誤時，回傳八個字元的空字串
                    return ERROR_VIDEOID;
                }
            }
            else
            {
                // 如果有查到的話，直接回傳就好了，不用後面再查一次
                return resultData[0]["FSVIDEO_ID_PROG"];
            }
        }

        [WebMethod]
        public string QueryVideoID_PROG_ARC_TYPE_Once(string fsID, string fsEpisode, string fsARC_TYPE)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSID", fsID);
            source.Add("FNEPISODE", fsEpisode);
            source.Add("FSARC_TYPE", fsARC_TYPE);
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_NewVideoID", source, out resultData))
            {
                string errMsg = string.Concat("執行 SP_Q_NewVideoID 時發生錯誤回傳，參數：", fsID, ", ", fsEpisode, ", ", fsARC_TYPE);
                MAM_PTS_DLL.Log.AppendTrackingLog("WSMAMFunctions_VideoID/SP_Q_NewVideoID", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return ERROR_VIDEOID;
            }
            return resultData[0]["FSVIDEOID"];
        }


        /// <summary>
        /// 更新 TBLOG_VIDEOID_MAP 裡面的資訊
        /// </summary>
        /// <param name="fsID"></param>
        /// <param name="fsEpisode"></param>
        /// <param name="fsChannel_ID"></param>
        /// <returns></returns>
        [WebMethod]
        public bool UpdateVideoID_PROG(string fsID, string fsEpisode, string fsARC_TYPE, string fsNewVideoID)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSID", fsID);
            source.Add("FNEPISODE", fsEpisode);
            source.Add("FSARC_TYPE", fsARC_TYPE);
            source.Add("FSVIDEO_ID_PROG", fsNewVideoID);
            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBLOG_VIDEOID_MAP", source, "system");
        }

        /// <summary>
        /// 取得新的VIDEOID_PROG，當發生存取異常時，會回傳八位元的空字串
        /// </summary>
        /// <param name="note">申請原因</param>
        /// <returns></returns>
        [WebMethod]
        public string GetNewVideoID(string note = "")
        {
            long longVideoID;
            string stringVideoID;

            // 從資料庫取得新的VideoID，並更新資料
            if (!fnGetNewVideoIDValue(out longVideoID, note))
                return ERROR_VIDEOID;

            // 將int64轉換為string格式
            fnConvertLongToVideoIDFormat(longVideoID, out stringVideoID);

            return stringVideoID;
        }

        // 呼叫預存程序，取得新的VideoID
        private bool fnGetNewVideoIDValue(out long longVideoID, string note)
        {
            longVideoID = 0;

            // SQL Parameters
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSNOTE", note);

            // Receive SQL Result
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            bool isSuccess = MAM_PTS_DLL.DbAccess.Do_Query("SP_I_TBLOGVIDEOID_NEWID", source, out resultData);

            if (!isSuccess)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSMAMFunctions_VideoID/fnGetNewVideoIDValue", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "執行 SP_I_TBLOGVIDEOID_NEWID 發生錯誤");
                return false;
            }
            else if (resultData.Count > 0)
            {
                // 只取第 1 筆結果的 FNVIDEO_ID 欄位值
                if (Int64.TryParse(resultData[0]["FNVIDEO_ID"], out longVideoID))
                    return true;
                else
                {
                    MAM_PTS_DLL.Log.AppendTrackingLog("WSMAMFunctions_VideoID/fnGetNewVideoIDValue", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "執行 SP_I_TBLOGVIDEOID_NEWID 時，回傳的資料無法解析為 INT64 格式");
                    return false;
                }
            }
            else
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSMAMFunctions_VideoID/fnGetNewVideoIDValue", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "執行 SP_I_TBLOGVIDEOID_NEWID 時，回傳的資料筆數量異常");
                return false;
            }
        }

        // 將長整數轉換為私有的36進位字串
        private void fnConvertLongToVideoIDFormat(long nVideoID, out string sVideoID)
        {
            // Sql BigInt Max = 9,223,372,036,854,775,807
            // C# long Max = 9,223,372,036,854,775,807
            // VideoID Max = 2,821,109,907,456

            int index;
            int varPower = 7;
            byte[] arr = new Byte[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            StringBuilder result = new StringBuilder();

            while (varPower >= 0)
            {
                index = 7 - varPower;
                arr[index] = (byte)Math.Floor(nVideoID / Math.Pow(36, varPower));
                nVideoID = nVideoID - (long)(arr[index] * Math.Pow(36, varPower));
                varPower--;
            }

            for (int i = 0; i < 8; i++)
            {
                if (arr[i] < 10)
                    result.Append(arr[i]);
                else
                    result.Append(((char)(arr[i] + 55)).ToString());
            }
            sVideoID = result.ToString();
        }
    }
}
