﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Collections.ObjectModel;
using System.Web.Configuration;

namespace PTS_MAM3.Web.DataSource
{

    public class TBTemplate_FieldsStruct
    {
       
        public int FNTEMPLATE_ID;
      
        public int FNID;
       
        public string FSTABLE;
       
        public string FSFIELD;
       
        public string FSFIELD_NAME;
      
        public string FSFIELD_TYPE;
      
        public int FNFIELD_LENGTH;
     
        public string FSDESCRIPTION;
      
        public int FNORDER;
    
        public int FNOBJECT_WIDTH;
     
        public string FCGRID;
     
        public string FCISNULLABLE;
      
        public string FCLIST;
       
        public string FCLIST_TYPE;
       
        public string FSLIST_NAME;
       
        public string FSDEFAULT;
      
        public string FSCREATED_BY;
       
        public DateTime FDCREATED_DATE;
        
        public string FSUPDATED_BY;
      
        public string FDUPDATED_DATE;

        public string FSCODE_ID;
        public string FSCODE_CNT;
        public string FSCODE_CTRL;
    }


    public class TBTemplateStruct
    {
       
        public int FNTEMPLATE_ID;
        
        public string FSTABLE;
       
        public string FSTEMPLATE;
     
        public string FSDESCRIPTION;
       
        public string FSCREATED_BY;
        
        public DateTime FDCREATED_DATE;
       
        public string FSUPDATED_BY;
        
        public DateTime FDUPDATED_DATE;

        public string FCREAD_ONLY; 
    }


    public class DirectoryGroupStruct
    {
     
        public string FSGROUP_ID;
   
        public string FCPRIVILEGE;
    }




    public class DirectoryStruct
    {
    
        public int FNDIR_ID;
      
        public string FSDIR;
    
        public int FNPARENT_ID;
  
        public string FSPARENT;
    
        public string FCQUEUE;
   
        public string FSDESCIPTION;
    
        public string FSCREATED_BY;
     
        public DateTime FDCREATED_DATE;
     
        public string FSUPDATED_BY;
     
        public string FDUPDATED_DATE;
    
        public int FNTEMPLATE_ID_SUBJECT;
    
        public int FNTEMPLATE_ID_PHOTO;
    
        public int FNTEMPLATE_ID_DOC;
   
        public int FNTEMPLATE_ID_AUDIO;
     
        public int FNTEMPLATE_ID_VIDEO_D;

    }



    /// <summary>
    /// WSDirectoryAdmin 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSDirectoryAdmin : System.Web.Services.WebService
    {
         
        [WebMethod]
        public Boolean fnCopyTBTEMPLATE(TBTemplateStruct t, string fsuser_id)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();

            source.Add("FNTEMPLATE_ID", t.FNTEMPLATE_ID.ToString());
            source.Add("FSCREATED_BY", t.FSCREATED_BY);
            source.Add("FSUPDATED_BY", t.FSUPDATED_BY);
         
            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBTEMPLATE_TBTEMPLATE_FIELDS", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source), fsuser_id);
        }
         
        /// <summary>
        /// 修改Template 名稱與描述
        /// </summary>
        /// <param name="t">TemplateStruct</param>
        /// <returns>True or false</returns>
        [WebMethod]
        public Boolean fnUpdateTBTEMPLATE(TBTemplateStruct t, string fsuser_id)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSTEMPLATE", t.FSTEMPLATE);
            source.Add("FSUPDATED_BY", t.FSUPDATED_BY);
            source.Add("FNTEMPLATE_ID", t.FNTEMPLATE_ID.ToString());
            source.Add("FSDESCRIPTION", t.FSDESCRIPTION);
           
            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBTEMPLATE", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source), fsuser_id);

        }

        /// <summary>
        /// 刪除Template 
        /// </summary>
        /// <param name="t">TemplateStruct</param>
        /// <returns>True or false</returns>
         [WebMethod]
        public Boolean fnDeleteTBTEMPLATE(TBTemplateStruct t, string fsuser_id)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FNTEMPLATE_ID", t.FNTEMPLATE_ID.ToString());
            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_TBTEMPLATE", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source), fsuser_id);
        }
         
        /// <summary>
        /// Delete 自訂欄位
        /// </summary>
        /// <param name="fnid"></param>
        /// <returns>True or false</returns>
         [WebMethod]
        public Boolean fnDeleteTBTEMPLATE_FIELDS(int fnid, string fsuser_id)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FNID", fnid.ToString());
            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_TBTEMPLATE_FIELDS", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source), fsuser_id);
        }

        /// <summary>
        /// 更新修改自訂欄位
        /// </summary>
        /// <param name="tf">自訂欄位資料型態</param>
        /// <returns>True or false</returns>
         [WebMethod]
        public Boolean fnUpdateTBTEMPLATE_FIELDS(TBTemplate_FieldsStruct tf, string fsuser_id)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FNID", tf.FNID.ToString());
            source.Add("FSFIELD", tf.FSFIELD.ToString());
            source.Add("FSFIELD_NAME", tf.FSFIELD_NAME.ToString());
            source.Add("FSFIELD_TYPE", tf.FSFIELD_TYPE.ToString());
            source.Add("FNFIELD_LENGTH", Convert.ToString(tf.FNFIELD_LENGTH));
            source.Add("FNOBJECT_WIDTH", Convert.ToString(tf.FNOBJECT_WIDTH));
            source.Add("FCISNULLABLE", tf.FCISNULLABLE);
            source.Add("FSDEFAULT", tf.FSDEFAULT.ToString());
            source.Add("FSUPDATED_BY", tf.FSUPDATED_BY.ToString());



            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBTEMPLATE_FIELDS", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source), fsuser_id);
        }

         [WebMethod]
         public Boolean fnUpdateTBTEMPLATE_FIELDS2(TBTemplate_FieldsStruct tf, string fsuser_id)
         {
             Dictionary<string, string> source = new Dictionary<string, string>();
             source.Add("FNID", tf.FNID.ToString());
             source.Add("FSFIELD", tf.FSFIELD.ToString());
             source.Add("FSFIELD_NAME", tf.FSFIELD_NAME.ToString());
             source.Add("FSFIELD_TYPE", tf.FSFIELD_TYPE.ToString());
             source.Add("FNFIELD_LENGTH", Convert.ToString(tf.FNFIELD_LENGTH));
             source.Add("FNOBJECT_WIDTH", Convert.ToString(tf.FNOBJECT_WIDTH));
             source.Add("FCISNULLABLE", tf.FCISNULLABLE);
             source.Add("FSDEFAULT", tf.FSDEFAULT.ToString());
             source.Add("FSUPDATED_BY", tf.FSUPDATED_BY.ToString());


             source.Add("FSCODE_ID", tf.FSCODE_ID.ToString());
             source.Add("FSCODE_CNT", tf.FSCODE_CNT.ToString());
             source.Add("FSCODE_CTRL", tf.FSCODE_CTRL.ToString());

             return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBTEMPLATE_FIELDS2", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source), fsuser_id);
         }


        /// <summary>
        /// 新增自訂欄位
        /// </summary>
        /// <param name="tf">自訂欄位資料型態</param>
        /// <returns>True or false</returns>
         [WebMethod]
        public Boolean fnInsertTBTEMPLATE_FIELDS(TBTemplate_FieldsStruct tf, string fsuser_id)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FNTEMPLATE_ID", tf.FNTEMPLATE_ID.ToString());
            source.Add("FCGRID", tf.FCGRID.ToString());
            source.Add("FCISNULLABLE", tf.FCISNULLABLE.ToString());
            source.Add("FCLIST", tf.FCLIST.ToString());
            source.Add("FCLIST_TYPE", tf.FCLIST_TYPE.ToString());
            source.Add("FNFIELD_LENGTH", tf.FNFIELD_LENGTH.ToString());
            source.Add("FNOBJECT_WIDTH", tf.FNOBJECT_WIDTH.ToString());
            source.Add("FNORDER", tf.FNORDER.ToString());
            source.Add("FSCREATED_BY", tf.FSCREATED_BY.ToString());
            source.Add("FSDEFAULT", tf.FSDEFAULT.ToString());
            source.Add("FSDESCRIPTION", tf.FSDESCRIPTION.ToString());
            source.Add("FSFIELD", tf.FSFIELD.ToString());
            source.Add("FSFIELD_NAME", tf.FSFIELD_NAME.ToString());
            source.Add("FSFIELD_TYPE", tf.FSFIELD_TYPE.ToString());
            source.Add("FSLIST_NAME", tf.FSLIST_NAME.ToString());
            source.Add("FSTABLE", tf.FSTABLE.ToString());
            source.Add("FSUPDATED_BY", tf.FSUPDATED_BY.ToString());
            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBTEMPLATE_FIELDS", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source), fsuser_id);
        }

         [WebMethod]
         public Boolean fnInsertTBTEMPLATE_FIELDS2(TBTemplate_FieldsStruct tf, string fsuser_id)
         {
             Dictionary<string, string> source = new Dictionary<string, string>();
             source.Add("FNTEMPLATE_ID", tf.FNTEMPLATE_ID.ToString());
             source.Add("FCGRID", tf.FCGRID.ToString());
             source.Add("FCISNULLABLE", tf.FCISNULLABLE.ToString());
             source.Add("FCLIST", tf.FCLIST.ToString());
             source.Add("FCLIST_TYPE", tf.FCLIST_TYPE.ToString());
             source.Add("FNFIELD_LENGTH", tf.FNFIELD_LENGTH.ToString());
             source.Add("FNOBJECT_WIDTH", tf.FNOBJECT_WIDTH.ToString());
             source.Add("FNORDER", tf.FNORDER.ToString());
             source.Add("FSCREATED_BY", tf.FSCREATED_BY.ToString());
             source.Add("FSDEFAULT", tf.FSDEFAULT.ToString());
             source.Add("FSDESCRIPTION", tf.FSDESCRIPTION.ToString());
             source.Add("FSFIELD", tf.FSFIELD.ToString());
             source.Add("FSFIELD_NAME", tf.FSFIELD_NAME.ToString());
             source.Add("FSFIELD_TYPE", tf.FSFIELD_TYPE.ToString());
             source.Add("FSLIST_NAME", tf.FSLIST_NAME.ToString());
             source.Add("FSTABLE", tf.FSTABLE.ToString());
             source.Add("FSUPDATED_BY", tf.FSUPDATED_BY.ToString());

             source.Add("FSCODE_ID", tf.FSCODE_ID.ToString());
             source.Add("FSCODE_CNT", tf.FSCODE_CNT.ToString());
             source.Add("FSCODE_CTRL", tf.FSCODE_CTRL.ToString());

             return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBTEMPLATE_FIELDS2", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source), fsuser_id);
         }

        /// <summary>
        /// 取得自訂欄位名稱列表
        /// </summary>
        /// <param name="fstable">可使用的表格</param>
        /// <param name="fntemplate_id">自訂欄位Template </param>
        /// <param name="fsfield">欄位名稱</param>
        /// <returns>XML String</returns>
         [WebMethod]
        public string fnGetTBTEMPLATE_FIELDS_FSFIELD(string fstable, int fntemplate_id, string fsfield)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();

            source.Add("FSTABLE", fstable);
            source.Add("FNTEMPLATE_ID", Convert.ToString(fntemplate_id));


            return MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBTEMPLATE_FIELDS_FSFIELD", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source));

        }



         [WebMethod]
        public string InsertDirectGroup(int fnDirId, int childId, string fsuser_id)
        {
            string rtn = string.Empty;
            try
            {
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;
                    cmd.CommandText = "select FSGROUP_ID,FCPRIVILEGE from TBDIRECTORY_GROUP where FNDIR_ID=" + fnDirId;
                    cmd.CommandText += " except select FSGROUP_ID,FCPRIVILEGE from TBDIRECTORY_GROUP where  FNDIR_ID=" + childId;
                    DataSet ds = new DataSet();
                    cn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd.CommandText, cn);
                    da.MissingSchemaAction = MissingSchemaAction.AddWithKey;
                    da.Fill(ds);

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        // cmd.CommandText = "insert TBDIRECTORY_GROUP (FNDIR_ID,FSGROUP_ID,FCPRIVILEGE,FSCREATED_BY,FDCREATED_DATE) values (" & childId & ",'" & ds.Tables[0].Rows[i].Field & "','" & ds.Tables[0].Rows[i].Item[1] & "','" & ViewState("User_ID") & "',getdate())";
                        cmd.ExecuteNonQuery();
                    }

                }
            }
            catch (Exception ex)
            {
                return null;
            }


            return rtn;
        }

         [WebMethod]
        public ObservableCollection<DirectoryGroupStruct> GetReadableDirectoryGroupListById(int fndir_id)
        {
            ObservableCollection<DirectoryGroupStruct> DirectoryGroupList = new ObservableCollection<DirectoryGroupStruct>();
            try
            {
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    if (fndir_id == -1)
                    {
                        cmd.CommandText = "select FSGROUP_ID, FCPRIVILEGE from TBDIRECTORY_GROUP where FCPRIVILEGE='R' ";
                    }
                    else
                    {
                        cmd.CommandText = "select FSGROUP_ID, FCPRIVILEGE from TBDIRECTORY_GROUP where FNDIR_ID=" + fndir_id + " and FCPRIVILEGE='R' ";
                    }

                    cn.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                DirectoryGroupList.Add
                                (
                                    new DirectoryGroupStruct
                                    {
                                        FCPRIVILEGE = dr["FCPRIVILEGE"].ToString(),
                                        FSGROUP_ID = dr["FSGROUP_ID"].ToString()
                                    }
                                );
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                return null;
            }


            return DirectoryGroupList;
        }


         [WebMethod]
        public ObservableCollection<DirectoryGroupStruct> GetOtherDirectoryGroupListById(int fndir_id)
        {
            ObservableCollection<DirectoryGroupStruct> DirectoryGroupList = new ObservableCollection<DirectoryGroupStruct>();
            try
            {
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    if (fndir_id == -1)
                    {
                        cmd.CommandText = "select FSGROUP_ID,FCPRIVILEGE  from tbgroups where FSGROUP_ID not in(select FSGROUP_ID from TBDIRECTORY_GROUP where FCPRIVILEGE='R' )";
                    }
                    else
                    {
                        cmd.CommandText = "select FSGROUP_ID, '' FCPRIVILEGE  from tbgroups where FSGROUP_ID not in(select FSGROUP_ID from TBDIRECTORY_GROUP where FNDIR_ID=" + fndir_id + "  and FCPRIVILEGE='R' )";
                    }

                    cn.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                DirectoryGroupList.Add
                                (
                                    new DirectoryGroupStruct
                                    {
                                        FCPRIVILEGE = dr["FCPRIVILEGE"].ToString(),
                                        FSGROUP_ID = dr["FSGROUP_ID"].ToString()
                                    }
                                );
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                return null;
            }


            return DirectoryGroupList;
        }

         [WebMethod]
        public ObservableCollection<TBTemplate_FieldsStruct> GetTemplateFieldsById(int FNTEMPLATE_ID)
        {
            ObservableCollection<TBTemplate_FieldsStruct> TemplateFieldsList = new ObservableCollection<TBTemplate_FieldsStruct>();
            try
            {
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    cn.Open();

                    cmd.CommandText = "SELECT FNTEMPLATE_ID, FNID, FSTABLE, FSFIELD, ISNULL(FSFIELD_NAME, '') FSFIELD_NAME, ISNULL(FSFIELD_TYPE, '') FSFIELD_TYPE, isNULL(FNFIELD_LENGTH, 0) FNFIELD_LENGTH, ISNULL(FSDESCRIPTION, '') FSDESCRIPTION,  ";
                    cmd.CommandText += "ISNULL(FNORDER, 0) FNORDER, ISNULL(FNOBJECT_WIDTH, 0) FNOBJECT_WIDTH, isNULL(FCGRID, 'N') FCGRID, ISNULL(FCISNULLABLE, 'Y') FCISNULLABLE, ISNULL(FCLIST, '0') FCLIST, ISNULL(FCLIST_TYPE, 'N') FCLIST_TYPE, ISNULL(FSLIST_NAME, 'FIELD') FSLIST_NAME, ";
                    cmd.CommandText += "ISNULL(FSDEFAULT, '') FSDEFAULT, ISNULL(FSCREATED_BY, '') FSCREATED_BY, FDCREATED_DATE,  ISNULL(FSUPDATED_BY, '') FSUPDATED_BY, isNULL(FDUPDATED_DATE , '') FDUPDATED_DATE, FSCODE_ID = ISNULL(FSCODE_ID,''),  FSCODE_CNT = ISNULL(FSCODE_CNT,''), FSCODE_CTRL = ISNULL(FSCODE_CTRL,'') ";
                    cmd.CommandText += "FROM TBTEMPLATE_FIELDS WHERE FNTEMPLATE_ID = " + FNTEMPLATE_ID;

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                TemplateFieldsList.Add
                                (
                                    new TBTemplate_FieldsStruct
                                    {
                                        FCGRID = Convert.ToString(dr["FCGRID"]),
                                        FCISNULLABLE = Convert.ToString(dr["FCISNULLABLE"]),
                                        FCLIST = Convert.ToString(dr["FCLIST"]),
                                        FCLIST_TYPE = Convert.ToString(dr["FCLIST_TYPE"]),
                                        FDCREATED_DATE = Convert.ToDateTime(dr["FDCREATED_DATE"]),
                                        FDUPDATED_DATE = Convert.ToString(dr["FDUPDATED_DATE"]),
                                        FNFIELD_LENGTH = Convert.ToInt32(dr["FNFIELD_LENGTH"]),
                                        FNID = Convert.ToInt32(dr["FNID"]),
                                        FNOBJECT_WIDTH = Convert.ToInt32(dr["FNOBJECT_WIDTH"]),
                                        FNORDER = Convert.ToInt32(dr["FNORDER"]),
                                        FNTEMPLATE_ID = Convert.ToInt32(dr["FNTEMPLATE_ID"]),
                                        FSDESCRIPTION = Convert.ToString(dr["FSDESCRIPTION"]),
                                        FSCREATED_BY = Convert.ToString(dr["FSCREATED_BY"]),
                                        FSDEFAULT = Convert.ToString(dr["FSDEFAULT"]),
                                        FSFIELD = Convert.ToString(dr["FSFIELD"]),
                                        FSFIELD_NAME = Convert.ToString(dr["FSFIELD_NAME"]),
                                        FSFIELD_TYPE = Convert.ToString(dr["FSFIELD_TYPE"]),
                                        FSLIST_NAME = Convert.ToString(dr["FSLIST_NAME"]),
                                        FSTABLE = Convert.ToString(dr["FSTABLE"]),
                                        FSUPDATED_BY = Convert.ToString(dr["FSUPDATED_BY"]),
                                        FSCODE_ID = Convert.ToString(dr["FSCODE_ID"]),
                                        FSCODE_CNT = Convert.ToString(dr["FSCODE_CNT"]),
                                        FSCODE_CTRL = Convert.ToString(dr["FSCODE_CTRL"])

                                    }
                              );
                            }

                        }
                    }





                }


            }
            catch (Exception ex)
            {
                return null;
            }


            return TemplateFieldsList;
        }




        [WebMethod]
        public ObservableCollection<TBTemplateStruct> GetTemplateListByTable(string FSTABLE)
        {
            ObservableCollection<TBTemplateStruct> TemplateList = new ObservableCollection<TBTemplateStruct>();
            try
            {
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    if (FSTABLE == string.Empty)
                    {
                        cmd.CommandText = "SELECT * FROM TBTEMPLATE ";
                    }
                    else
                    {
                        cmd.CommandText = "SELECT * FROM TBTEMPLATE WHERE FSTABLE = '" + FSTABLE.Replace("'", "''") + "' ";
                    }

                    cn.Open();


                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                TemplateList.Add
                                (
                                    new TBTemplateStruct
                                    {
                                        FDCREATED_DATE = Convert.ToDateTime(dr["FDCREATED_DATE"]),
                                        FDUPDATED_DATE = Convert.ToDateTime(dr["FDUPDATED_DATE"]),
                                        FNTEMPLATE_ID = Convert.ToInt32(dr["FNTEMPLATE_ID"]),
                                        FSCREATED_BY = Convert.ToString(dr["FSCREATED_BY"]),
                                        FSDESCRIPTION = Convert.ToString(dr["FSDESCRIPTION"]),
                                        FSTABLE = Convert.ToString(dr["FSTABLE"]),
                                        FSTEMPLATE = Convert.ToString(dr["FSTEMPLATE"]),
                                        FSUPDATED_BY = Convert.ToString(dr["FSUPDATED_BY"]),
                                        FCREAD_ONLY = Convert.ToString(dr["FCREAD_ONLY"])
                                    }
                              );
                            }

                        }

                    }
                }

            }
            catch (Exception ex)
            {
                return null;
            }


            return TemplateList;
        }





        [WebMethod]
        public string UpdateDirectoryItem(DirectoryStruct dirItem)
        {
            string rtn = string.Empty;
            try
            {
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    cmd.CommandText = "UPDATE TBDIRECTORIES SET FSDIR = '" + dirItem.FSDIR.Replace("'", "''") + "', FCQUEUE = '" + dirItem.FCQUEUE.Replace("'", "''") + "', FSDESCIPTION = '" + dirItem.FSDESCIPTION.Replace("'", "''") + "', FSUPDATED_BY = '" + dirItem.FSUPDATED_BY.Replace("'", "''") + "', FDUPDATED_DATE = getdate()" +
                    ",FNTEMPLATE_ID_PHOTO=" + dirItem.FNTEMPLATE_ID_PHOTO + ",FNTEMPLATE_ID_DOC=" + dirItem.FNTEMPLATE_ID_DOC + ",FNTEMPLATE_ID_AUDIO=" + dirItem.FNTEMPLATE_ID_AUDIO + ",FNTEMPLATE_ID_VIDEO_D=" + dirItem.FNTEMPLATE_ID_VIDEO_D;
                    cmd.CommandText += "WHERE FNDIR_ID = " + dirItem.FNDIR_ID;
                    cn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return rtn;
        }


        [WebMethod]
        public string AddDirectoryItem(DirectoryStruct dirItem)
        {
            //取得DIR_ID最大號
            MAM_PTS_DLL.DirAndSubjectAccess Subobj = new MAM_PTS_DLL.DirAndSubjectAccess();  
            dirItem.FNDIR_ID = Subobj.getMaxDirId();
            string rtn = dirItem.FNDIR_ID.ToString();   //取回來的DIR_ID要送回前端
            try
            {
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    cmd.CommandText = "INSERT INTO TBDIRECTORIES(FNDIR_ID,FSDIR,FNPARENT_ID,FCQUEUE,FSDESCIPTION,FSCREATED_BY,FDCREATED_DATE, FSUPDATED_BY,FDUPDATED_DATE,FNTEMPLATE_ID_PHOTO,FNTEMPLATE_ID_DOC,FNTEMPLATE_ID_AUDIO,FNTEMPLATE_ID_VIDEO_D) VALUES ";
                    cmd.CommandText += "(" + dirItem.FNDIR_ID + ",'" + dirItem.FSDIR.Replace("'", "''") + "'," + dirItem.FNPARENT_ID + ", '" + dirItem.FCQUEUE.Replace("'", "''") + "', ";
                    cmd.CommandText += "'" + dirItem.FSDESCIPTION.Replace("'", "''") + "', '" + dirItem.FSCREATED_BY.Replace("'", "''") + "', getDate() , '" + dirItem.FSUPDATED_BY.Replace("'", "''") + "', getdate(),"+ dirItem.FNTEMPLATE_ID_PHOTO + "," + dirItem.FNTEMPLATE_ID_DOC + "," + dirItem.FNTEMPLATE_ID_AUDIO + "," + dirItem.FNTEMPLATE_ID_VIDEO_D ;
                    cmd.CommandText += ") ";
                    cn.Open();
                    cmd.ExecuteNonQuery();

                }


            }
            catch (Exception ex)
            {
                return "E:" + ex.ToString();
            }
            return rtn;
        }




        [WebMethod]
        public ObservableCollection<DirectoryStruct> GetDirListById(int fndir_id)
        {
            ObservableCollection<DirectoryStruct> DirList = new ObservableCollection<DirectoryStruct>();

            try
            {
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;


                    if (fndir_id == -1)
                    {
                        cmd.CommandText = "SELECT FNDIR_ID,FSDIR,ISNULL(FNPARENT_ID, -1) FNPARENT_ID, FCQUEUE, FSDESCIPTION, isNULL(FSCREATED_BY, '') FSCREATED_BY, FDCREATED_DATE, isNULL(FSUPDATED_BY, '') FSUPDATED_BY, isNULL(FDUPDATED_DATE, '') FDUPDATED_DATE, FNTEMPLATE_ID_SUBJECT, FNTEMPLATE_ID_AUDIO, FNTEMPLATE_ID_DOC, FNTEMPLATE_ID_PHOTO, FNTEMPLATE_ID_VIDEO_D FROM TBDIRECTORIES WHERE FNPARENT_ID is null ";
                    }
                    else
                        cmd.CommandText = "SELECT FNDIR_ID,FSDIR,ISNULL(FNPARENT_ID, -1) FNPARENT_ID , FCQUEUE, FSDESCIPTION, isNULL(FSCREATED_BY, '') FSCREATED_BY, FDCREATED_DATE, isNULL(FSUPDATED_BY, '') FSUPDATED_BY, isNULL(FDUPDATED_DATE, '') FDUPDATED_DATE, FNTEMPLATE_ID_SUBJECT, FNTEMPLATE_ID_AUDIO, FNTEMPLATE_ID_DOC, FNTEMPLATE_ID_PHOTO, FNTEMPLATE_ID_VIDEO_D FROM TBDIRECTORIES WHERE FNPARENT_ID = " + fndir_id;
                    cn.Open();
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {

                            PTS_MAM3.Web.DataSource.WSMAMFunctions mamFunctions = new PTS_MAM3.Web.DataSource.WSMAMFunctions();
                            while (dr.Read())
                            {
                                DirList.Add
                                (
                                    new DirectoryStruct
                                    {
                                        FNDIR_ID = Convert.ToInt32(dr["FNDIR_ID"]),
                                        FSDIR = Convert.ToString(dr["FSDIR"]),
                                        FNPARENT_ID = Convert.ToInt32(dr["FNPARENT_ID"]),
                                        FCQUEUE = Convert.ToString(dr["FCQUEUE"]),
                                        FSDESCIPTION = Convert.ToString(dr["FSDESCIPTION"]),
                                        FSCREATED_BY = Convert.ToString(dr["FSCREATED_BY"]),
                                        FDCREATED_DATE = Convert.ToDateTime(dr["FDCREATED_DATE"]),
                                        FSUPDATED_BY = Convert.ToString(dr["FSUPDATED_BY"]),
                                        FDUPDATED_DATE = Convert.ToString(dr["FDUPDATED_DATE"]),
                                        FSPARENT = mamFunctions.fnGetValue("SELECT ISNULL(FSDIR, '') FSDIR FROM TBDIRECTORIES WHERE FNDIR_ID = " + Convert.ToString(dr["FNPARENT_ID"])).FirstOrDefault(),
                                        FNTEMPLATE_ID_SUBJECT = Convert.ToInt32(dr["FNTEMPLATE_ID_SUBJECT"]),
                                        FNTEMPLATE_ID_AUDIO = Convert.ToInt32(dr["FNTEMPLATE_ID_AUDIO"]),
                                        FNTEMPLATE_ID_DOC = Convert.ToInt32(dr["FNTEMPLATE_ID_DOC"]),
                                        FNTEMPLATE_ID_PHOTO = Convert.ToInt32(dr["FNTEMPLATE_ID_PHOTO"]),
                                        FNTEMPLATE_ID_VIDEO_D = Convert.ToInt32(dr["FNTEMPLATE_ID_VIDEO_D"])
                                    }
                                );
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                return null;
            }

            return DirList;
        }

        //刪除節點，但是要先檢查，確定無相關才可以刪除
        [WebMethod]
        public string DELETE_TBDIRECTORIES_CHECK(string strDir_ID,string strUserID)
        {
            string strMsg = "";

            if (CHECK_TBDIRECTORIES_FNPARENT_ID_BYDIRID(strDir_ID) == false )      
                return "E:此節點以下尚有其他節點，無法刪除";           
            else
            {              
              if (CHECK_TBLOG_FNDIR_ID_BYDIRID(strDir_ID) == false)      
                  return "E:此節點以下尚有其他主題，無法刪除";      
              else      
              {      
                  if (DELETE_TBDIRECTORIES_BYDIR_ID(strDir_ID, strUserID, out strMsg) == true)      
                      return "";   //刪除成功      
                  else      
                      return "E:" + strMsg;      
              }                     
            }                
        }

        //檢查該節點底下是否有其他節點，回傳True表示可以刪除
        public Boolean CHECK_TBDIRECTORIES_FNPARENT_ID_BYDIRID(string strDIR_ID)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FNDIR_ID", strDIR_ID);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBDIRECTORIES_BYDIRID", sqlParameters, out sqlResult))
                return false;

            if (sqlResult.Count > 0)
            {
                if (sqlResult[0]["db_count"] == "0") 
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        //檢查該節點底下是否有主題，回傳True表示沒有主題，該節點可以刪除
        public Boolean CHECK_TBLOG_FNDIR_ID_BYDIRID(string strDIR_ID)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FNDIR_ID", strDIR_ID);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_FNDIR_ID_BYDIRID", sqlParameters, out sqlResult))
                return false;

            if (sqlResult.Count > 0)
            {                
                if (sqlResult[0]["AUDIO_count"] == "0" && sqlResult[1]["DOC_count"] == "0" && sqlResult[2]["PHOTO_count"] == "0" && sqlResult[3]["VIDEO_count"] == "0" && sqlResult[4]["VIDEO_D_count"] == "0")
                        return true;
                else
                    return false;
            }
            else
                return false;
        }

        //刪除節點
        [WebMethod]
        public Boolean DELETE_TBDIRECTORIES_BYDIR_ID(string strDIR_ID, string strUserID, out string strError)
        {
            strError = "";

            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FNDIR_ID", strDIR_ID);

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_TBDIRECTORIES_BYDIRID", source, out strError, strUserID, true);            
        }
        

    }
}
