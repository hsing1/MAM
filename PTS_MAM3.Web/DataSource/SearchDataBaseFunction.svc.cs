﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using MAM_PTS_DLL;
using System.Xml;

namespace PTS_MAM3.Web.DataSource
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class SearchDataBaseFunction
    {
        //取得詳細資料
        [OperationContract]
        public string GetSearchDetailData(string sp_name, string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query(sp_name, parameter);
            return result;
        }

        //新增使用者查詢記錄
        [OperationContract]
        public void InsertSearchHistoryParameter(string sp_name, string parameter)
        {
            DbAccess.Do_Transaction(sp_name, parameter);
        }

        //新增使用者查詢記錄
        [OperationContract]
        public void InsertKeyWord(string sp_name, string parameter)
        {
            // 在此新增您的作業實作
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(parameter);

            if (xDoc.GetElementsByTagName("FSKEYWORD").Item(0).InnerText != "")
            {

                string[] SplitStr = xDoc.GetElementsByTagName("FSKEYWORD").Item(0).InnerText.Split(':');

                //新增至查詢記錄
                for (int j = 0; j <= SplitStr.Length - 1; j++)
                {
                    xDoc.GetElementsByTagName("FSKEYWORD").Item(0).InnerText = SplitStr[j];
                    parameter = xDoc.InnerXml;
                    DbAccess.Do_Transaction(sp_name, parameter);
                }
            }
        }

        //取得檢索欄位
        [OperationContract]
        public string GetSearchColumnByindex(string sp_name, string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query(sp_name, parameter);
            return result;
        }

        //取得檢索欄位
        [OperationContract]
        public string GetSearchColumnType(string sp_name, string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query(sp_name, parameter);
            return result;
        }


        //取得熱門關鍵字
        [OperationContract]
        public string GetHotKeyWord(string sp_name, string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query(sp_name, parameter);
            return result;
        }

        //新增熱門點閱
        [OperationContract]
        public void InsertSearchHotHit(string sp_name, string parameter)
        {
            // 在此新增您的作業實作
            DbAccess.Do_Transaction(sp_name, parameter);
        }


        //取得熱門點閱
        [OperationContract]
        public string GetHotHit(string sp_name, string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query(sp_name, parameter);
            return result;
        }

        //取得查詢記錄
        [OperationContract]
        public string GetSearchHistory(string sp_name, string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query(sp_name, parameter);
            return result;
        }

        //取得同義詞
        [OperationContract]
        public string GetSynonym(string sp_name, string parameter)
        {
            // 在此新增您的作業實作
            string result = "";
            result = DbAccess.Do_Query(sp_name, parameter);
            return result;
        }

        //新增同義詞組
        [OperationContract]
        public void InsertSynonymList(string sp_name, string parameter)
        {
            // 在此新增您的作業實作
            DbAccess.Do_Transaction(sp_name, parameter);
        }

        //新增同義詞彙
        [OperationContract]
        public void InsertSynonym(string sp_name, string parameter)
        {
            // 在此新增您的作業實作
            DbAccess.Do_Transaction(sp_name, parameter);
        }

        //刪除同義詞組
        [OperationContract]
        public void DeleteSynonymList(string sp_name, string parameter)
        {
            // 在此新增您的作業實作
            DbAccess.Do_Transaction(sp_name, parameter);
        }

        //刪除同義詞彙
        [OperationContract]
        public void DeleteSynonym(string sp_name, string parameter)
        {
            // 在此新增您的作業實作
            DbAccess.Do_Transaction(sp_name, parameter);
        }
        // 在此新增其他作業，並以 [OperationContract] 來標示它們
    }
}
