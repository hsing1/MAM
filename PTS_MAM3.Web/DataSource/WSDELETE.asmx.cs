﻿//===========================================
//2012/11/16 David.Sin 新增刪除檔案服務
//===========================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using PTS_MAM3.Web.Class;
using MAM_PTS_DLL;
using System.Collections.ObjectModel;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSDELETE 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSDELETE : System.Web.Services.WebService
    {
        /// <summary>
        /// 取得台別
        /// </summary>
        /// <returns>台別List</returns>
        [WebMethod]
        public List<Class_DELETED_CHANNEL> GetChannelList()
        {
            List<Class_DELETED_CHANNEL> _class_deleted_channel_list = new List<Class_DELETED_CHANNEL>();
            Dictionary<string, string> _dic = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            DbAccess.Do_Query("SP_Q_TBZCHANNEL_ALL", _dic, out sqlResult);
            if (sqlResult != null && sqlResult.Count > 0)
            {
                for (int i = 0; i <= sqlResult.Count - 1; i++)
                {
                    if (!string.IsNullOrEmpty(sqlResult[i]["FSSHOWNAME"]) && sqlResult[i]["ID"] != "99")
                    {
                        Class_DELETED_CHANNEL _class_deleted_channel = new Class_DELETED_CHANNEL();
                        _class_deleted_channel._fschannel_id = sqlResult[i]["ID"];
                        _class_deleted_channel._fschannel_name = sqlResult[i]["FSSHOWNAME"];
                        if (i == 1)
                        {
                            _class_deleted_channel._fbis_checked = true;
                        }
                        else
                        {
                            _class_deleted_channel._fbis_checked = false;
                        }
                        _class_deleted_channel_list.Add(_class_deleted_channel);
                    }
                }
            }
            return _class_deleted_channel_list;
        }

        /// <summary>
        /// 取得檔案狀態
        /// </summary>
        /// <returns>檔案狀態List</returns>
        [WebMethod]
        public List<string> GetFileStatusList()
        {
            List<string> _file_status_list = new List<string>();
            _file_status_list.Add("未選擇");
            _file_status_list.Add("D:已被置換");
            _file_status_list.Add("T:轉檔完成");
            _file_status_list.Add("Y:入庫完成");
            _file_status_list.Add("R:轉檔失敗");
            return _file_status_list;
        }

        /// <summary>
        /// 取得節目
        /// </summary>
        /// <param name="_fschannel_id">頻道</param>
        /// <param name="_fstype">類別</param>
        /// <param name="_fsprog_id">節目ID</param>
        /// <param name="_fsprog_name">節目名稱</param>
        /// <returns>節目List</returns>
        [WebMethod]
        public List<Class_DELETED_PROGRAM> GetProgramList(string _fschannel_id, string _fstype, string _fsprog_id, string _fsprog_name)
        {
            List<Class_DELETED_PROGRAM> _class_deleted_program_list = new List<Class_DELETED_PROGRAM>();
            Dictionary<string, string> _dic = new Dictionary<string, string>();
            _dic.Add("FSCHANNEL_ID", _fschannel_id);
            _dic.Add("FSPROG_TYPE", _fstype);
            _dic.Add("FSPROG_ID", _fsprog_id);
            _dic.Add("FSPROG_NAME", _fsprog_name);

            List<Dictionary<string, string>> sqlResult;
            DbAccess.Do_Query("SP_Q_DELETED_PROGRAM", _dic, out sqlResult);

            if (sqlResult != null && sqlResult.Count > 0)
            {
                for (int i = 0; i <= sqlResult.Count - 1; i++)
                {
                    Class_DELETED_PROGRAM _class_deleted_program = new Class_DELETED_PROGRAM();
                    _class_deleted_program._fsprog_id = sqlResult[i]["ID"];
                    _class_deleted_program._fsprog_name = sqlResult[i]["NAME"];
                    _class_deleted_program_list.Add(_class_deleted_program);
                }
            }

            return _class_deleted_program_list;
        }

        /// <summary>
        /// 查詢要刪除的檔案資訊
        /// </summary>
        /// <param name="_class_deleted_parameter">查詢條件</param>
        /// <returns>檔案資訊List</returns>
        [WebMethod]
        public List<Class_DELETED_RESULT> GetDeletedFileList(Class_DELETED_PARAMETER _class_deleted_parameter)
        {
            List<Class_DELETED_RESULT> _class_deleted_result_list = new List<Class_DELETED_RESULT>();
            List<Dictionary<string, string>> sqlResult;
            Dictionary<string, string> _dic = new Dictionary<string, string>();
            _dic.Add("FSCHANNEL_ID", _class_deleted_parameter._fschannel_id);
            _dic.Add("FSTYPE", _class_deleted_parameter._fstype);
            _dic.Add("FSID", _class_deleted_parameter._fsprog_id);
            _dic.Add("FNEPISODE_BEG", _class_deleted_parameter._fnepisode_beg);
            _dic.Add("FNEPISODE_END", _class_deleted_parameter._fnepisode_end);
            _dic.Add("FILE_TYPE", _class_deleted_parameter._fsfile_type);
            _dic.Add("FSFILE_NO", _class_deleted_parameter._fsfile_no);
            _dic.Add("FDTRAN_DATE_BEG", _class_deleted_parameter._fdtran_date_beg);
            _dic.Add("FDTRAN_DATE_END", _class_deleted_parameter._fdtran_date_end);
            _dic.Add("FCFILE_STATUS", _class_deleted_parameter._fcfile_status);
            DbAccess.Do_Query("SP_Q_DELETED_FILE", _dic, out sqlResult);
            if (sqlResult != null && sqlResult.Count > 0)
            {
                for (int i = 0; i <= sqlResult.Count - 1; i++)
                {
                    Class_DELETED_RESULT _class_deleted_result = new Class_DELETED_RESULT();
                    _class_deleted_result._fsprog_id = sqlResult[i]["FSPROG_ID"];
                    _class_deleted_result._fsprog_name = sqlResult[i]["FSPGDNAME"];
                    _class_deleted_result._fnepisode = sqlResult[i]["FNEPISODE"];
                    _class_deleted_result._fschannel = sqlResult[i]["FSCHANNEL"];
                    _class_deleted_result._fsfile_no = sqlResult[i]["FSFILE_NO"];
                    _class_deleted_result._fdtran_date = (string.IsNullOrEmpty(sqlResult[i]["FDTRANSCODE_DATE"]) ? "" : DateTime.Parse(sqlResult[i]["FDTRANSCODE_DATE"]).ToString("yyyy/MM/dd HH:mm:ss"));
                    _class_deleted_result._fstran_by = (string.IsNullOrEmpty(sqlResult[i]["FSTRANSCODE_BY"]) ? "" : "轉檔者        " + sqlResult[i]["FSTRANSCODE_BY"]);
                    _class_deleted_result._fscreated_by = sqlResult[i]["FSCREATED_BY"];
                    _class_deleted_result._fsfile_status = sqlResult[i]["FCFILE_STATUS"];
                    _class_deleted_result._fsarc_type = sqlResult[i]["FSTYPE_NAME"];
                    _class_deleted_result._fstbname = sqlResult[i]["FSTBNAME"];
                    _class_deleted_result._fbis_checked = false;
                    _class_deleted_result._fsview = sqlResult[i]["FSFILE_NO"] + ";" + sqlResult[i]["FSTBNAME"];
                    if (_class_deleted_result._fstbname == "V" || _class_deleted_result._fstbname == "P")
                    {
                        _class_deleted_result._fnbutton_height = 30;
                    }
                    else
                    {
                        _class_deleted_result._fnbutton_height = 0;
                    }
                    _class_deleted_result_list.Add(_class_deleted_result);
                }
            }


            return _class_deleted_result_list;
        }

        /// <summary>
        /// 自動刪除檔案的WS
        /// </summary>
        /// <returns>結果字串</returns>
        [WebMethod]
        public string AutoDeleteFile()
        {
            string rtnString = "自動檔案刪除完成";
            List<Class_DELETED> myAutoDeleteFileList = new List<Class_DELETED>();   //到期且需要刪除的檔案LIST
            List<Class_DELETED> myDeleteFileList = new List<Class_DELETED>();       //到期且需要刪除的檔案且可以被刪除(經過獻倫程式的判定)
            List<string> rtnFailFileNameList = new List<string>();                  //刪除失敗的檔案
            Dictionary<string, string> parameter = new Dictionary<string, string>();
            List<Dictionary<string,string>> sqlResult;
            DbAccess.Do_Query("SP_Q_AUTO_DELETE_FILE", parameter, out sqlResult);
            if (sqlResult!=null && sqlResult.Count>0)
            {
                for (int i = 0; i <= sqlResult.Count - 1; i++)
                {
                    Class_DELETED clsDelete = new Class_DELETED();
                    clsDelete._fsfile_no = sqlResult[i]["Del_FSFILE_NO"];
                    clsDelete._fsfile_type = sqlResult[i]["Del_VAPD"];
                    clsDelete._fdcreated_date = Convert.ToDateTime(sqlResult[i]["Del_FDCREATED_DATE"]).ToString("yyyy-MM-dd HH:mm:ss");
                    //clsDelete._fscreated_by = sqlResult[i]["Del_FSCREATED_BY"];
                    clsDelete._fcstatus = "X";
                    clsDelete._fscreated_by = "admin";
                    clsDelete._fsmemo = "系統自動刪除 註記刪除日期為：" + sqlResult[i]["Del_FDEXPIRE_DATE"];
                    myAutoDeleteFileList.Add(clsDelete);
                }
            }
            //如果有到期且需要刪除的檔案時
            if (myAutoDeleteFileList.Count>0)
            {
                foreach (var item in myAutoDeleteFileList)
                {
                    if (CheckFileCanDelete(item._fsfile_no)=="")
                    {
                        myDeleteFileList.Add(item);
                    }
                }
            }
            //經過獻倫判定需要刪除的檔案 呼叫刪除程式
            if (myDeleteFileList.Count>0)
            {
                rtnFailFileNameList = DeleteFile(myDeleteFileList);
            }
            if (rtnFailFileNameList.Count == 0)
            {
                rtnString = "完成" + myDeleteFileList.Count + "筆刪除記錄。";
            }
            else
            { rtnString = "檔案刪除失敗" + rtnFailFileNameList.Count.ToString() + "筆"; }
            return rtnString;
        }

        /// <summary>
        /// 檢查檔案是否可以刪除
        /// </summary>
        /// <param name="_fsfile_no">檔案編號</param>
        /// <returns>空字串:可以刪除</returns>
        [WebMethod]
        public string CheckFileCanDelete(string _fsfile_no)
        {
            string _result = "";
            List<Dictionary<string, string>> sqlResult;
            Dictionary<string, string> _dic = new Dictionary<string, string>();
            _dic.Add("FSFILE_NO", _fsfile_no);
            if (DbAccess.Do_Query("SP_Q_CHECK_FILE_CAN_DELETED_BY_FSFILE_NO", _dic, out sqlResult))
            {
                if (sqlResult != null && sqlResult.Count > 0)
                {
                    _result = sqlResult[0]["RESULT"];
                }
            }

            return _result;
        }

        /// <summary>
        /// 刪除檔案
        /// </summary>
        /// <param name="_deletedList">刪除Class List</param>
        /// <returns>失敗檔案編號List</returns>
        [WebMethod]
        public List<string> DeleteFile(List<Class_DELETED> _deletedList)
        {
            WSMAMFunctions wsMAMFunction = new WSMAMFunctions();
            List<string> _fsfile_no_delete_error_list = new List<string>();
            List<Dictionary<string, string>> sqlResult;
            if (_deletedList != null && _deletedList.Count > 0)
            {
                foreach (Class_DELETED _class_deleted in _deletedList)
                {
                    //取得新的video id
                    string _fsvideo_id_new = "";
                    if (_class_deleted._fsfile_type == "V")
                    {
                        _fsvideo_id_new = wsMAMFunction.GetNewVideoID(_class_deleted._fsfile_no + ",D");
                    }
                    
                    Dictionary<string, string> _dic = new Dictionary<string, string>();
                    _dic.Add("FSFILE_NO", _class_deleted._fsfile_no);
                    _dic.Add("FSCREATED_BY", _class_deleted._fscreated_by);
                    _dic.Add("FDCREATED_DATE", _class_deleted._fdcreated_date);
                    _dic.Add("FCSTATUS", "N");
                    _dic.Add("FSMEMO", _class_deleted._fsmemo);
                    _dic.Add("FSVIDEO_PROG_NEW", _fsvideo_id_new);
                    DbAccess.Do_Query("SP_I_TBDELETED", _dic, out sqlResult);

                    if (sqlResult != null && sqlResult.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(sqlResult[0]["RESULT"]))
                        {
                            _fsfile_no_delete_error_list.Add(_class_deleted._fsfile_no);
                        }
                    }
                }
            }

            return _fsfile_no_delete_error_list;
        }

        /// <summary>
        /// 刪除檔案(不取新的VideoID)
        /// </summary>
        /// <param name="_deletedList">刪除Class List</param>
        /// <returns>失敗檔案編號List</returns>
        [WebMethod]
        public List<string> DeleteFile_No_NewVideoID(List<Class_DELETED> _deletedList)
        {
            WSMAMFunctions wsMAMFunction = new WSMAMFunctions();
            List<string> _fsfile_no_delete_error_list = new List<string>();
            List<Dictionary<string, string>> sqlResult;
            if (_deletedList != null && _deletedList.Count > 0)
            {
                foreach (Class_DELETED _class_deleted in _deletedList)
                {
                    //取得新的video id
                    //string _fsvideo_id_new = "";
                    //if (_class_deleted._fsfile_type == "V")
                    //{
                    //    _fsvideo_id_new = wsMAMFunction.GetNewVideoID(_class_deleted._fsfile_no + ",D");
                    //}

                    Dictionary<string, string> _dic = new Dictionary<string, string>();
                    _dic.Add("FSFILE_NO", _class_deleted._fsfile_no);
                    _dic.Add("FSCREATED_BY", _class_deleted._fscreated_by);
                    _dic.Add("FDCREATED_DATE", _class_deleted._fdcreated_date);
                    _dic.Add("FCSTATUS", "N");
                    _dic.Add("FSMEMO", _class_deleted._fsmemo);
                    _dic.Add("FSVIDEO_PROG_NEW", "");
                    DbAccess.Do_Query("SP_I_TBDELETED", _dic, out sqlResult);

                    if (sqlResult != null && sqlResult.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(sqlResult[0]["RESULT"]))
                        {
                            _fsfile_no_delete_error_list.Add(_class_deleted._fsfile_no);
                        }
                    }
                }
            }

            return _fsfile_no_delete_error_list;
        }

        /// <summary>
        /// 抽單發生錯誤時回復TBDELETED與TBLOG_VIDEO的資料 by Jarvis20130625
        /// </summary>
        /// <param name="File_No">檔案編號</param>
        /// <param name="OriState">原始檔案狀態</param>
        /// <param name="strUPDATED_BY">更新者</param>
        /// <returns>true=成功;false=失敗</returns>
        [WebMethod]
        public bool Reply_Detached_Error(string File_No,string OriState,string strUPDATED_BY) 
        {
            Dictionary<string,string> source =new Dictionary<string,string>();
            source.Add("file_No", File_No);
            source.Add("oriState", OriState);

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_U_Reply_TBDELETED", source, strUPDATED_BY);
        }

        [WebMethod]
        public bool Reply_Detached_Error_Batch(List<Class_DELETED> delList)
        {
            bool result = false;
            foreach (Class_DELETED del in delList)
            {
                Dictionary<string, string> source = new Dictionary<string, string>();
                source.Add("file_No", del._fsfile_no);
                source.Add("oriState", del._fcstatus);

                result = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_U_Reply_TBDELETED", source, del._fscreated_by);
                
            }
            return result;
        }


        /// <summary>
        /// 查詢已刪除的檔案
        /// </summary>
        /// <param name="_class_deleted_search_parameter">查詢條件</param>
        /// <returns>檔案List</returns>
        [WebMethod]
        public List<Class_DELETED_SEARCH_RESULT> GetAlreadyDeletedFileList(Class_DELETED_SEARCH_PARAMETER _class_deleted_search_parameter)
        {
            List<Class_DELETED_SEARCH_RESULT> _class_deleted_search_result_list = new List<Class_DELETED_SEARCH_RESULT>();
            List<Dictionary<string, string>> sqlResult;
            Dictionary<string, string> _dic = new Dictionary<string, string>();
            _dic.Add("FSFILE_TYPE_V", _class_deleted_search_parameter._fsfile_type_v);
            _dic.Add("FSFILE_TYPE_A", _class_deleted_search_parameter._fsfile_type_a);
            _dic.Add("FSFILE_TYPE_P", _class_deleted_search_parameter._fsfile_type_p);
            _dic.Add("FSFILE_TYPE_D", _class_deleted_search_parameter._fsfile_type_d);
            _dic.Add("FSTYPE", _class_deleted_search_parameter._fstype);
            _dic.Add("FSID", _class_deleted_search_parameter._fsprog_id);
            _dic.Add("FNEPISODE_BEG", _class_deleted_search_parameter._fnepisode_beg);
            _dic.Add("FNEPISODE_END", _class_deleted_search_parameter._fnepisode_end);
            _dic.Add("FSFILE_NO", _class_deleted_search_parameter._fsfile_no);
            _dic.Add("FDCREATED_DATE_BEG", _class_deleted_search_parameter._fdcreated_date_beg);
            _dic.Add("FDCREATED_DATE_END", _class_deleted_search_parameter._fdcreated_date_end);
            DbAccess.Do_Query("SP_Q_TBDELETED_BY_PROGID_EPISODE", _dic, out sqlResult);
            if (sqlResult != null && sqlResult.Count > 0)
            {
                for (int i = 0; i <= sqlResult.Count - 1; i++)
                {
                    Class_DELETED_SEARCH_RESULT _class_deleted_search_result = new Class_DELETED_SEARCH_RESULT();
                    _class_deleted_search_result._fsprog_name = sqlResult[i]["FSPGDNAME"];
                    _class_deleted_search_result._fnepisode = sqlResult[i]["FNEPISODE"];
                    _class_deleted_search_result._fsfile_no = sqlResult[i]["FSFILE_NO"];
                    _class_deleted_search_result._fscreated_by = sqlResult[i]["FSCREATED_BY"];
                    _class_deleted_search_result._fdcreated_date = sqlResult[i]["FDCREATED_DATE"];
                    _class_deleted_search_result._fsmemo = sqlResult[i]["FSMEMO"];
                    _class_deleted_search_result._fsarc_type_name = sqlResult[i]["FSARC_TYPE_NAME"];
                    if (sqlResult[i]["FSFILE_TYPE"] == "V")
                    {
                        _class_deleted_search_result._fsfile_type = "/PTS_MAM3;component/Images/Video.png";
                    }
                    else if (sqlResult[i]["FSFILE_TYPE"] == "A")
                    {
                        _class_deleted_search_result._fsfile_type = "/PTS_MAM3;component/Images/audio.png";
                    }
                    else if (sqlResult[i]["FSFILE_TYPE"] == "P")
                    {
                        _class_deleted_search_result._fsfile_type = "/PTS_MAM3;component/Images/photo.png";
                    }
                    else if (sqlResult[i]["FSFILE_TYPE"] == "D")
                    {
                        _class_deleted_search_result._fsfile_type = "audio";
                    }
                    _class_deleted_search_result._fstype = sqlResult[i]["FSTYPE"];
                    _class_deleted_search_result_list.Add(_class_deleted_search_result);
                }
            }

            return _class_deleted_search_result_list;
        }
    }
}
