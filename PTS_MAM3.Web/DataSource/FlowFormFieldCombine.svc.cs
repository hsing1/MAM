﻿
using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using PTS_MAM3.Web.Class;

namespace PTS_MAM3.Web.DataSource
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class FlowFormFieldCombine
    {
        #region 判斷是否有這個帳號存在於Flow中
        [OperationContract]
        public string IsTheAccountExist(string userName)
        {
            string isExist;
            DeltaFlowAPI.Oc Oc = new DeltaFlowAPI.Oc();
            DeltaFlowAPI.Account myAccount = new DeltaFlowAPI.Account(userName, string.Empty, string.Empty, string.Empty, string.Empty);

            myAccount = Oc.GetAccountByName(userName);
            if (myAccount == null)
            { isExist = "f"; }
            else
            { isExist = "t"; }
            return isExist;
            //return "t";
        }
        #endregion
        #region DFWaitYourCheck(string userName) "DFGetTTL.xaml.cs" 待簽核的表單 組合欄位
        [OperationContract]
        public List<Class.DFWaitForCheck_Class> DFWaitYourCheck(string userName)
        {
            string backXml = "";
            string GetTTL, GetVariableValueString;

            MAM_PTS_DLL.Log.AppendTrackingLog("kyle-test-ttl-START", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "START");
            
            FlowWS.Flow client = new FlowWS.Flow(); 

            //client.GetTTL(userName); 
            GetTTL = client.GetTTL(userName).Replace("&amp;", "");

            MAM_PTS_DLL.Log.AppendTrackingLog("kyle-test-DFWaitYourCheck-1", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "GetTTL = " + GetTTL);
            MAM_PTS_DLL.Log.AppendTrackingLog("kyle-test-DFWaitYourCheck-2", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "client ip =  " + System.Web.HttpContext.Current.Request.UserHostAddress);

            List<Class.DFWaitForCheck_Class> WaitForCheckList = new List<Class.DFWaitForCheck_Class>();
            try
            {
                TextReader txr = new StringReader(GetTTL);
                XDocument doc = XDocument.Load(txr);
                var ltox = from sel in doc.Elements("TTLCollection").Elements("TTL")
                           select sel;
                foreach (XElement elem in ltox)
                {
                    Class.DFWaitForCheck_Class WaitForCheckClass = new Class.DFWaitForCheck_Class();
                    WaitForCheckClass.FormUrl = elem.Element("FormUrl").Value;
                    WaitForCheckClass.Applyname = elem.Element("Applyname").Value;
                    WaitForCheckClass.ReceiveDate = elem.Element("ReceiveDate").Value;
                    WaitForCheckClass.Priority = elem.Element("Priority").Value;
                    WaitForCheckClass.Title = elem.Element("Title").Value;
                    WaitForCheckClass.Ttlid = (elem.Element("Ttlid") == null ? "" : elem.Element("Ttlid").Value);
                    WaitForCheckClass.Version = elem.Element("Version").Value;
                    WaitForCheckClass.HaveAttach = elem.Element("HaveAttach").Value;
                    WaitForCheckClass.Remark = elem.Element("Remark").Value;
                    WaitForCheckClass.Receiveid = elem.Element("Receiveid").Value;
                    WaitForCheckClass.Batch = elem.Element("Batch").Value;
                    WaitForCheckClass.OriginalName = elem.Element("OriginalName").Value;
                    WaitForCheckClass.FlowChartUrl = elem.Element("FlowChartUrl").Value;
                    WaitForCheckClass.flowid = elem.Element("flowid").Value;
                    WaitForCheckClass.OverDueDate = elem.Element("OverDueDate").Value;
                    WaitForCheckClass.Addsign = elem.Element("Addsign").Value;
                    WaitForCheckClass.Nodeid = elem.Element("Nodeid").Value;
                    WaitForCheckClass.Roleid = elem.Element("Roleid").Value;
                    if (elem.Element("Ttlid") == null || elem.Element("Ttlid").Value == "")
                    { WaitForCheckClass.FormId = "N/A"; }
                    else
                    {
                        GetVariableValueString = client.GetVariableValue(Convert.ToInt32(elem.Element("Ttlid").Value));
                        TextReader txr1 = new StringReader(GetVariableValueString);
                        XDocument doc1 = XDocument.Load(txr1);
                        var ltox1 = from sel1 in doc1.Elements("VariableCollection").Elements("Variable")
                                    where sel1.Element("name").Value == "FormId"
                                    select sel1;
                        foreach (XElement elem1 in ltox1)
                        {
                            WaitForCheckClass.FormId = elem1.Element("value").Value;
                        }
                        var ltox2 = from sel2 in doc1.Elements("VariableCollection").Elements("Variable")
                                    where sel2.Element("name").Value == "FormInfo"
                                    select sel2;
                        foreach (XElement elem1 in ltox2)
                        {
                            WaitForCheckClass.FormInfo = elem1.Element("value").Value;
                        }
                        var ltox3 = from sel3 in doc1.Elements("VariableCollection").Elements("Variable")
                                    where sel3.Element("name").Value == "FlowResult"
                                    select sel3;
                        foreach (XElement elem1 in ltox3)
                        {
                            WaitForCheckClass.FlowResult = elem1.Element("value").Value;
                        }
                        var ltox4 = from sel4 in doc1.Elements("VariableCollection").Elements("Variable")
                                    where sel4.Element("name").Value == "Function01"
                                    select sel4;
                        foreach (XElement elem1 in ltox4)
                        {
                            WaitForCheckClass.Function01 = elem1.Element("value").Value;
                        }
                        var ltox5 = from sel5 in doc1.Elements("VariableCollection").Elements("Variable")
                                    where sel5.Element("name").Value == "NextXamlName"
                                    select sel5;
                        foreach (XElement elem1 in ltox5)
                        {
                            WaitForCheckClass.NextXamlName = elem1.Element("value").Value;
                        }
                    }
                    //var ltox2 = from sel1 in doc1.Elements("VariableCollection").Elements("Variable")
                    //            where sel1.Element("name").Value == "Function01"
                    //            select sel1;
                    //foreach (XElement elem1 in ltox1)
                    //{
                    //    WaitForCheckClass.Function01 = elem1.Element("value").Value;
                    //}
                    WaitForCheckList.Add(WaitForCheckClass);
                }
                //backXml += "<DFWaitToCheck_Main>";
                //for (int i = 0; i < WaitForCheckList.Count; i++)
                //{
                //    backXml += "<Record>";
                //    backXml += "<FormId>" + WaitForCheckList[i].FormId + "</FormId>";
                //    backXml += "<FormUrl>" + WaitForCheckList[i].FormUrl + "</FormUrl>";
                //    backXml += "<Applyname>" + WaitForCheckList[i].Applyname + "</Applyname>";
                //    backXml += "<ReceiveDate>" + WaitForCheckList[i].ReceiveDate + "</ReceiveDate>";
                //    backXml += "<Priority>" + WaitForCheckList[i].Priority + "</Priority>";
                //    backXml += "<Title>" + WaitForCheckList[i].Title + "</Title>";
                //    backXml += "<Ttlid>" + WaitForCheckList[i].Ttlid + "</Ttlid>";
                //    backXml += "<Version>" + WaitForCheckList[i].Version + "</Version>";
                //    backXml += "<HaveAttach>" + WaitForCheckList[i].HaveAttach + "</HaveAttach>";
                //    backXml += "<Remark>" + WaitForCheckList[i].Remark + "</Remark>";
                //    backXml += "<Receiveid>" + WaitForCheckList[i].Receiveid + "</Receiveid>";
                //    backXml += "<Batch>" + WaitForCheckList[i].Batch + "</Batch>";
                //    backXml += "<OriginalName>" + WaitForCheckList[i].OriginalName + "</OriginalName>";
                //    backXml += "<FlowChartUrl>" + WaitForCheckList[i].FlowChartUrl + "</FlowChartUrl>";
                //    backXml += "<flowid>" + WaitForCheckList[i].flowid + "</flowid>";
                //    backXml += "<OverDueDate>" + WaitForCheckList[i].OverDueDate + "</OverDueDate>";
                //    backXml += "<Addsign>" + WaitForCheckList[i].Addsign + "</Addsign>";
                //    backXml += "<Nodeid>" + WaitForCheckList[i].Nodeid + "</Nodeid>";
                //    backXml += "<Roleid>" + WaitForCheckList[i].Roleid + "</Roleid>";
                //    //backXml += "<Function01>" + WaitForCheckList[i].Function01 + "</Function01>";
                //    backXml += "</Record>";
                //}
                //backXml += "</DFWaitToCheck_Main>";
            }
            catch (Exception ex)
            {
                string errMsg = string.Concat("Got error parsing GetTTL value _DFWaitYourCheck_: ", ex.ToString());
                MAM_PTS_DLL.Log.AppendTrackingLog("FlowFormFieldcombine", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return new List<DFWaitForCheck_Class>();
            }
            //MAM_PTS_DLL.Log.AppendTrackingLog("End of FlowFormFieldcombine", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
            return WaitForCheckList;
        }
        #endregion


        #region DFWaitToCheck_Main(string userName,string selectedCondiction,string  ) "DFQueryActivity.xaml.cs" 審批中表單 的Master
        [OperationContract]
        public string DFWaitToCheck_Main(string userName,string FlowStatus,string SDate,string EDate,int Flowid,int ProcessType)
        {
            string returnXML,QueryActivity,GetApprovalList;
            //FlowWebService.FlowSoapClient client = new FlowWebService.FlowSoapClient();
            FlowWS.Flow client = new FlowWS.Flow();
           
            QueryActivity = client.QueryActivity(userName, FlowStatus, SDate, EDate, Flowid, 0, 0, ProcessType);
            //GetApprovalList = client.GetApprovalList();
            List<Class.DFWaitToCheck_Main_Class> WaitToCheckList = new List<Class.DFWaitToCheck_Main_Class>();
            TextReader txr = new StringReader(QueryActivity);
            XDocument doc = XDocument.Load(txr);
            var ltox = from sel in doc.Elements("ActivityCollection").Elements("Activity")
                       select sel;
            foreach (XElement elem in ltox)
            {
                Class.DFWaitToCheck_Main_Class QueryActivityClass = new Class.DFWaitToCheck_Main_Class();
                QueryActivityClass.DF_QueryActivity_Activeid = elem.Element("activeid").Value;
                QueryActivityClass.DF_QueryActivity_Applyname = elem.Element("Applyname").Value;
                QueryActivityClass.DF_QueryActivity_Displayname = elem.Element("displayname").Value;
                QueryActivityClass.DF_QueryActivity_Enterdate = elem.Element("enterDate").Value;
                QueryActivityClass.DF_QueryActivity_Finishdate = elem.Element("finishDate").Value;
                QueryActivityClass.DF_QueryActivity_Status = elem.Element("status").Value;
                QueryActivityClass.DF_QueryActivity_Vesion = elem.Element("version").Value;
                GetApprovalList = client.GetVariableValue(Convert.ToInt32(QueryActivityClass.DF_QueryActivity_Activeid));
                TextReader txr1 = new StringReader(GetApprovalList);
                XDocument doc1 = XDocument.Load(txr1);
                var ltox1 = from sel1 in doc1.Elements("VariableCollection").Elements("Variable")
                            where sel1.Element("name").Value == "FormId"
                            select sel1;
                foreach (XElement elem1 in ltox1)
                { QueryActivityClass.DF_Form_FormId = elem1.Element("value").Value; }
                WaitToCheckList.Add(QueryActivityClass);
            }
            returnXML = "<DFWaitToCheck_Main>";
            for (int i = 0; i < WaitToCheckList.Count; i++)
            {
                returnXML += "<Record>";
                returnXML += "<DF_Form_FormId>" + WaitToCheckList[i].DF_Form_FormId + "</DF_Form_FormId>";
                returnXML += "<DF_QueryActivity_Activeid>" + WaitToCheckList[i].DF_QueryActivity_Activeid + "</DF_QueryActivity_Activeid>";
                returnXML += "<DF_QueryActivity_Applyname>" + WaitToCheckList[i].DF_QueryActivity_Applyname + "</DF_QueryActivity_Applyname>";
                returnXML += "<DF_QueryActivity_Displayname>" + WaitToCheckList[i].DF_QueryActivity_Displayname + "</DF_QueryActivity_Displayname>";
                returnXML += "<DF_QueryActivity_Enterdate>" + WaitToCheckList[i].DF_QueryActivity_Enterdate + "</DF_QueryActivity_Enterdate>";
                returnXML += "<DF_QueryActivity_Finishdate>" + WaitToCheckList[i].DF_QueryActivity_Finishdate + "</DF_QueryActivity_Finishdate>";
                returnXML += "<DF_QueryActivity_Status>" + WaitToCheckList[i].DF_QueryActivity_Status + "</DF_QueryActivity_Status>";
                returnXML += "<DF_QueryActivity_Vesion>" + WaitToCheckList[i].DF_QueryActivity_Vesion + "</DF_QueryActivity_Vesion>";
                returnXML += "</Record>";
            }
            returnXML += "</DFWaitToCheck_Main>";
            
            return returnXML;
        }
        #endregion

        #region DFWaitToCheck_Main_PE(string userName,string selectedCondiction,string  ) "DFQueryActivity.xaml.cs" 審批中表使用者要求要可以同時看到批審中 以及已結案的表單2011/04/25
        [OperationContract]
        public string DFWaitToCheck_Main_PE(string userName, string FlowStatus, string SDate, string EDate, int Flowid, int ProcessType)
        {
            string returnXML, QueryActivity, GetApprovalList;
            //FlowWebService.FlowSoapClient client = new FlowWebService.FlowSoapClient();
            FlowWS.Flow client = new FlowWS.Flow();

            QueryActivity = client.QueryActivity(userName, "P", SDate, EDate, Flowid, 0, 0, ProcessType);
            //GetApprovalList = client.GetApprovalList();
            List<Class.DFWaitToCheck_Main_Class> WaitToCheckList = new List<Class.DFWaitToCheck_Main_Class>();
            TextReader txrP = new StringReader(QueryActivity);
            XDocument docP = XDocument.Load(txrP);
            var ltoxP = from sel in docP.Elements("ActivityCollection").Elements("Activity")
                       select sel;
            foreach (XElement elem in ltoxP)
            {
                Class.DFWaitToCheck_Main_Class QueryActivityClass = new Class.DFWaitToCheck_Main_Class();
                QueryActivityClass.DF_QueryActivity_Activeid = elem.Element("activeid").Value;
                QueryActivityClass.DF_QueryActivity_Applyname = elem.Element("Applyname").Value;
                QueryActivityClass.DF_QueryActivity_Displayname = elem.Element("displayname").Value;
                QueryActivityClass.DF_QueryActivity_Enterdate = elem.Element("enterDate").Value;
                QueryActivityClass.DF_QueryActivity_Finishdate = elem.Element("finishDate").Value;
                QueryActivityClass.DF_QueryActivity_Status = elem.Element("status").Value;
                QueryActivityClass.DF_QueryActivity_Vesion = elem.Element("version").Value;
                GetApprovalList = client.GetVariableValue(Convert.ToInt32(QueryActivityClass.DF_QueryActivity_Activeid));
                TextReader txr1 = new StringReader(GetApprovalList);
                XDocument doc1 = XDocument.Load(txr1);
                var ltox1 = from sel1 in doc1.Elements("VariableCollection").Elements("Variable")
                            where sel1.Element("name").Value == "FormId"
                            select sel1;
                foreach (XElement elem1 in ltox1)
                { QueryActivityClass.DF_Form_FormId = elem1.Element("value").Value; }
                WaitToCheckList.Add(QueryActivityClass);
            }

            QueryActivity = client.QueryActivity(userName, "E", SDate, EDate, Flowid, 0, 0, ProcessType);
            TextReader txrE = new StringReader(QueryActivity);
            XDocument docE = XDocument.Load(txrE);
            var ltoxE = from sel in docE.Elements("ActivityCollection").Elements("Activity")
                       select sel;
            foreach (XElement elem in ltoxE)
            {
                Class.DFWaitToCheck_Main_Class QueryActivityClass = new Class.DFWaitToCheck_Main_Class();
                QueryActivityClass.DF_QueryActivity_Activeid = elem.Element("activeid").Value;
                QueryActivityClass.DF_QueryActivity_Applyname = elem.Element("Applyname").Value;
                QueryActivityClass.DF_QueryActivity_Displayname = elem.Element("displayname").Value;
                QueryActivityClass.DF_QueryActivity_Enterdate = elem.Element("enterDate").Value;
                QueryActivityClass.DF_QueryActivity_Finishdate = elem.Element("finishDate").Value;
                QueryActivityClass.DF_QueryActivity_Status = elem.Element("status").Value;
                QueryActivityClass.DF_QueryActivity_Vesion = elem.Element("version").Value;
                GetApprovalList = client.GetVariableValue(Convert.ToInt32(QueryActivityClass.DF_QueryActivity_Activeid));
                TextReader txr1 = new StringReader(GetApprovalList);
                XDocument doc1 = XDocument.Load(txr1);
                var ltox1 = from sel1 in doc1.Elements("VariableCollection").Elements("Variable")
                            where sel1.Element("name").Value == "FormId"
                            select sel1;
                foreach (XElement elem1 in ltox1)
                { QueryActivityClass.DF_Form_FormId = elem1.Element("value").Value; }
                WaitToCheckList.Add(QueryActivityClass);
            }
            //WaitToCheckList.OrderByDescending(R => R.DF_QueryActivity_Activeid);
            returnXML = "<DFWaitToCheck_Main>";
            for (int i = 0; i < WaitToCheckList.Count; i++)
            {
                returnXML += "<Record>";
                returnXML += "<DF_Form_FormId>" + WaitToCheckList[i].DF_Form_FormId + "</DF_Form_FormId>";
                returnXML += "<DF_QueryActivity_Activeid>" + WaitToCheckList[i].DF_QueryActivity_Activeid + "</DF_QueryActivity_Activeid>";
                returnXML += "<DF_QueryActivity_Applyname>" + WaitToCheckList[i].DF_QueryActivity_Applyname + "</DF_QueryActivity_Applyname>";
                returnXML += "<DF_QueryActivity_Displayname>" + WaitToCheckList[i].DF_QueryActivity_Displayname + "</DF_QueryActivity_Displayname>";
                returnXML += "<DF_QueryActivity_Enterdate>" + WaitToCheckList[i].DF_QueryActivity_Enterdate + "</DF_QueryActivity_Enterdate>";
                returnXML += "<DF_QueryActivity_Finishdate>" + WaitToCheckList[i].DF_QueryActivity_Finishdate + "</DF_QueryActivity_Finishdate>";
                returnXML += "<DF_QueryActivity_Status>" + WaitToCheckList[i].DF_QueryActivity_Status + "</DF_QueryActivity_Status>";
                returnXML += "<DF_QueryActivity_Vesion>" + WaitToCheckList[i].DF_QueryActivity_Vesion + "</DF_QueryActivity_Vesion>";
                returnXML += "</Record>";
            }
            returnXML += "</DFWaitToCheck_Main>";

            return returnXML;
        }
        #endregion
        // 在此新增其他作業，並以 [OperationContract] 來標示它們
    }
}
