﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.IO;
using System.Text;
using System.Xml.Linq;
using PTS_MAM3.Web.Class;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSPROG_D 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSPROG_D : System.Web.Services.WebService
    {
        List<Class_CODE> m_CodeData = new List<Class_CODE>();       //代碼檔集合
        List<Class_CODE> m_CodeDataBuyD = new List<Class_CODE>();   //代碼檔集合(外購類別細項) 
        List<Class_DELETE_MSG> m_DeleteMsg = new List<Class_DELETE_MSG>();  //檢查刪除節目主檔訊息集合
        List<Class_CODE> m_CodeDataMen = new List<Class_CODE>();            //代碼檔集合(刪除節目主檔-相關人員)
        List<Class_CODE> m_CodeDataRace = new List<Class_CODE>();           //代碼檔集合(刪除節目主檔-參展人員)

        public struct ReturnMsg     //錯誤訊息處理
        {
            public bool bolResult;
            public string strErrorMsg;
        }

        public struct EpisodeLength     //錯誤訊息處理
        {
            public bool bolResult;
            public string FSTYPE_NAME;
            public int intShow_Seconds;
            public string FSShow;           
        }

        
        /// <summary>
        /// 解析資料庫回來的Dictionary，轉存成Class
        /// </summary>
        /// <param name="sqlResult"></param>
        /// <returns></returns>
        private List<Class_PROGD> Transfer_PROGD(List<Dictionary<string, string>> sqlResult)
        {            
            short shortCheck;                                                               //純粹為了判斷是否為數字型態之用(smallint)
            int intCheck;                                                                   //純粹為了判斷是否為數字型態之用(int)
            DateTime dtCheck;                                                               //純粹為了判斷是否為日期型態之用(DateTime)                                

            List<Class_PROGD> returnData = new List<Class_PROGD>();
            m_CodeData = MAMFunctions.Transfer_Class_CODE("SP_Q_TBPROG_M_CODE");
            m_CodeDataBuyD = MAMFunctions.Transfer_Class_CODED("SP_Q_TBZPROGBUYD_ALL");            

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_PROGD obj = new Class_PROGD();
                // -------------------------------- 
                obj.FSPROG_ID = sqlResult[i]["FSPROG_ID"];                                  //節目編號
                obj.FSPROG_ID_NAME = sqlResult[i]["FSPROG_ID_NAME"];                        //節目名稱                
                obj.FSPGDNAME = sqlResult[i]["FSPGDNAME"];                                  //節目名稱
                obj.FNEPISODE = Convert.ToInt16(sqlResult[i]["FNEPISODE"]);                 //集別  
                obj.SHOW_FNEPISODE = MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]); //集別(顯示)
                obj.FNEPISODE_NAME = sqlResult[i]["FNEPISODE_NAME"];                        //子集名稱

                if (short.TryParse(sqlResult[i]["FNSHOWEPISODE"], out shortCheck) == false)
                    obj.FNSHOWEPISODE = 0;
                else
                    obj.FNSHOWEPISODE = Convert.ToInt16(sqlResult[i]["FNSHOWEPISODE"]);     //播出集別  
                
                obj.SHOW_FNSHOWEPISODE = MAMFunctions.compareNumber(sqlResult[i]["FNSHOWEPISODE"]); //播出集別(顯示)
                obj.FSPGDENAME = sqlResult[i]["FSPGDENAME"];                                //子集外文名稱
                obj.FNLENGTH = Convert.ToInt16(sqlResult[i]["FNLENGTH"]);                   //時長
                obj.SHOW_FNLENGTH = sqlResult[i]["FNLENGTH"];                               //時長(顯示)

                if (int.TryParse(sqlResult[i]["FNTAPELENGTH"], out intCheck) == false)
                    obj.FNTAPELENGTH = 0;
                else
                    obj.FNTAPELENGTH = Convert.ToInt32(sqlResult[i]["FNTAPELENGTH"]);       //實際帶長               
                
                obj.SHOW_FNTAPELENGTH = MAMFunctions.compareNumber(sqlResult[i]["FNTAPELENGTH"]);//實際帶長(顯示)
                obj.FSPROGSRCID = sqlResult[i]["FSPROGSRCID"];                              //節目來源代碼
                obj.FSPROGATTRID = sqlResult[i]["FSPROGATTRID"];                            //內容屬性代碼
                obj.FSPROGAUDID = sqlResult[i]["FSPROGAUDID"];                              //目標觀眾代碼
                obj.FSPROGTYPEID = sqlResult[i]["FSPROGTYPEID"];                            //表現方式代碼
                obj.FSPROGGRADEID = sqlResult[i]["FSPROGGRADEID"];                          //節目分級代碼
                obj.FSPROGLANGID1 = sqlResult[i]["FSPROGLANGID1"];                          //主聲道代碼
                obj.FSPROGLANGID2 = sqlResult[i]["FSPROGLANGID2"];                          //副聲道代碼
                obj.FSPROGSALEID = sqlResult[i]["FSPROGSALEID"];                            //行銷類別代碼
                obj.FSPROGBUYID = sqlResult[i]["FSPROGBUYID"];                              //外購類別代碼
                obj.FSPROGBUYDID = sqlResult[i]["FSPROGBUYDID"];                            //外購類別細項代碼                
                obj.FCRACE = sqlResult[i]["FCRACE"];                                        //可排檔 
                obj.FSSHOOTSPEC = sqlResult[i]["FSSHOOTSPEC"];                              //拍攝規格代碼
                obj.FSPRDYEAR = sqlResult[i]["FSPRDYEAR"];                                  //完成年度
                obj.FSPROGGRADE = sqlResult[i]["FSPROGGRADE"];                              //非正常分級的說明
                obj.FSCONTENT = sqlResult[i]["FSCONTENT"];                                  //內容
                obj.FSMEMO = sqlResult[i]["FSMEMO"];                                        //備註
                obj.FSCHANNEL_ID = sqlResult[i]["FSCHANNEL_ID"];                            //頻道別代碼
                obj.FSDEL = sqlResult[i]["FSDEL"];                                          //刪除註記
                obj.FSDELUSER = sqlResult[i]["FSDELUSER"];                                  //刪除人
                obj.FSNDMEMO = sqlResult[i]["FSNDMEMO"];                                    //2nd備註
                obj.FSPROGSPEC = sqlResult[i]["FSPROGSPEC"];                                //節目規格
                obj.FSCR_TYPE = sqlResult[i]["FSCR_TYPE"];                                  //著作權類型
                obj.FSCR_NOTE = sqlResult[i]["FSCR_NOTE"];                                  //著作權備註
                obj.FSCRTUSER = sqlResult[i]["FSCRTUSER"];                                  //建檔者

                if (DateTime.TryParse(sqlResult[i]["WANT_FDCREATEDDATE"], out dtCheck) == false)
                    obj.FDCRTDATE = Convert.ToDateTime("1900/1/1");
                else
                    obj.FDCRTDATE = Convert.ToDateTime(sqlResult[i]["WANT_FDCREATEDDATE"]); //建檔日期  

                obj.SHOW_FDCRTDATE = MAMFunctions.compareDate(sqlResult[i]["WANT_FDCREATEDDATE"]);//建檔日期(顯示)
                obj.FSUPDUSER = sqlResult[i]["FSUPDUSER"];                                  //修改者

                if (DateTime.TryParse(sqlResult[i]["WANT_FDUPDATEDDATE"], out dtCheck) == false)
                    obj.FDUPDDATE = Convert.ToDateTime("1900/1/1");
                else
                    obj.FDUPDDATE = Convert.ToDateTime(sqlResult[i]["WANT_FDUPDATEDDATE"]); //修改日期

                obj.SHOW_FDUPDDATE = MAMFunctions.compareDate(sqlResult[i]["WANT_FDUPDATEDDATE"]);//修改日期(顯示)

                obj.FSCRTUSER_NAME = sqlResult[i]["FSCREATED_BY_NAME"];                     //建檔者姓名
                obj.FSUPDUSER_NAME = sqlResult[i]["FSUPDATED_BY_NAME"];                     //修改者姓名
                
                obj.FSPROGSRCID_NAME = compareCode_Code(sqlResult[i]["FSPROGSRCID"], "TBZPROGSRC");    //節目來源代碼名稱
                obj.FSPROGATTRID_NAME = compareCode_Code(sqlResult[i]["FSPROGATTRID"], "TBZPROGATTR"); //內容屬性代碼名稱
                obj.FSPROGAUDID_NAME = compareCode_Code(sqlResult[i]["FSPROGAUDID"], "TBZPROGAUD");    //目標觀眾代碼名稱
                obj.FSPROGTYPEID_NAME = compareCode_Code(sqlResult[i]["FSPROGTYPEID"], "TBZPROGTYPE"); //表現方式代碼名稱
                obj.FSPROGGRADEID_NAME = compareCode_Code(sqlResult[i]["FSPROGGRADEID"], "TBZPROGGRADE"); //節目分級代碼名稱
                obj.FSPROGLANGID1_NAME = compareCode_Code(sqlResult[i]["FSPROGLANGID1"], "TBZPROGLANG"); //主聲道代碼名稱
                obj.FSPROGLANGID2_NAME = compareCode_Code(sqlResult[i]["FSPROGLANGID2"], "TBZPROGLANG"); //副聲道代碼名稱
                obj.FSPROGSALEID_NAME = compareCode_Code(sqlResult[i]["FSPROGSALEID"], "TBZPROGSALE");  //行銷類別代碼名稱
                obj.FSPROGBUYID_NAME = compareCode_Code(sqlResult[i]["FSPROGBUYID"], "TBZPROGBUY");     //外購類別代碼名稱
                obj.FSPROGBUYDID_NAME = compareCode_CodeBuyD(sqlResult[i]["FSPROGBUYID"], sqlResult[i]["FSPROGBUYDID"]);    //外購類別細項代碼名稱
                obj.FSCHANNEL_ID_NAME = compareCode_Code(sqlResult[i]["FSCHANNEL_ID"], "TBZCHANNEL"); //頻道別代碼名稱   
                obj.FSCR_TYPE_NAME = MAMFunctions.CheckCopyRightType(sqlResult[i]["FSCR_TYPE"]);      //著作權類型名稱   
                obj.FSPROGSPEC_NAME = compareCode_Code_Spec(sqlResult[i]["FSPROGSPEC"], "TBZPROGSPEC");//節目規格名稱
                obj.FSSHOOTSPEC_NAME = compareCode_Code_Spec(sqlResult[i]["FSSHOOTSPEC"], "TBZSHOOTSPEC");//拍攝規格名稱   

                if (sqlResult[i]["FDEXPIRE_DATE"] == "")
                {
                    obj.FDEXPIRE_DATE = Convert.ToDateTime("1900-1-1");
                    obj.Origin_FDEXPIRE_DATE = Convert.ToDateTime("1900-1-1");
                }
                else
                {
                    obj.FDEXPIRE_DATE = Convert.ToDateTime(sqlResult[i]["FDEXPIRE_DATE"]);                      //到期日期
                    obj.Origin_FDEXPIRE_DATE = Convert.ToDateTime(sqlResult[i]["FDEXPIRE_DATE"]);
                }
                obj.FSEXPIRE_DATE_ACTION = sqlResult[i]["FSEXPIRE_DATE_ACTION"];                            //到期時執行的動作
                obj.Origin_FSEXPIRE_DATE_ACTION = sqlResult[i]["FSEXPIRE_DATE_ACTION"];
                obj.FSPROGNATIONID = sqlResult[i]["FSPROGNATIONID"];                                        //來源國家
                // --------------------------------
                returnData.Add(obj);
            }

            return returnData;
        }

        //透過節目編號、集別查詢節目子集資料
        [WebMethod()]
        public List<Class_PROGD> GetProg_D(string ProgID, string EPISODE)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPROG_ID", ProgID);
            sqlParameters.Add("FNEPISODE", EPISODE);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_D", sqlParameters, out sqlResult))
                return new List<Class_PROGD>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PROGD(sqlResult);  
        }

        //透過節目子集名稱查詢節目子集資料
        [WebMethod()]
        public List<Class_PROGD> QProg_D_BYPGDNAME(string ProgID, string EPISODE, string PGDNAME)
        {           
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPROG_ID", ProgID);
            sqlParameters.Add("FSPGDNAME", PGDNAME);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_D_BYPGDNAME", sqlParameters, out sqlResult))
                return new List<Class_PROGD>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PROGD(sqlResult); 
        }

        //透過節目子集名稱查詢節目子集資料_參展得獎
        [WebMethod()]
        public List<Class_PROGD> QProg_D_BYPGDNAME_RACE(string PGDNAME)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPGDNAME", PGDNAME);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_D_BYPGDNAME_RACE", sqlParameters, out sqlResult))
                return new List<Class_PROGD>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PROGD(sqlResult);
        }

        //透過集別、節目子集名稱查詢節目子集資料
        [WebMethod()]
        public List<Class_PROGD> QProg_D_BYEPISODE_PGDNAME(string ProgID, string EPISODE, string PGDNAME)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPROG_ID", ProgID);
            sqlParameters.Add("FNEPISODE", EPISODE);
            sqlParameters.Add("FSPGDNAME", PGDNAME);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_D_BYEPISODE_NAME", sqlParameters, out sqlResult))
                return new List<Class_PROGD>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PROGD(sqlResult);
        }

        //透過集別、節目子集名稱查詢節目子集資料 包含子集區間
        [WebMethod()]
        public List<Class_PROGD> QProg_D_BYEPISODE_PGDNAME_WITH_INTERVAL(string ProgID, string EPISODE, string PGDNAME,string startEpisode,string endEpisode )
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPROG_ID", ProgID);
            sqlParameters.Add("FNEPISODE", EPISODE);
            sqlParameters.Add("FSPGDNAME", PGDNAME);
            sqlParameters.Add("StartEpisode", startEpisode);
            sqlParameters.Add("EndEpisode", endEpisode);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_D_BYEPISODE_NAME_WITH_INTERVAL", sqlParameters, out sqlResult))
                return new List<Class_PROGD>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PROGD(sqlResult);
        }

        //節目子集的所有資料
        [WebMethod()]
        public List<Class_PROGD> GetTBPROG_D_ALL()
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_D_ALL", sqlParameters, out sqlResult))
                return new List<Class_PROGD>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PROGD(sqlResult);         
        }

        //節目子集的所有資料_BY節目編號
        [WebMethod()]
        public List<Class_PROGD> GetTBPROG_D_BYPROGID_ALL(string ProgID)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPROG_ID", ProgID);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_D_BYPROGID_ALL", sqlParameters, out sqlResult))
                return new List<Class_PROGD>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PROGD(sqlResult);
        }

        //修改節目管理系統的節目子集資料、MAM的節目子集資料
        [WebMethod]
        public ReturnMsg UPDATE_TBPROGD(Class.Class_PROGD obj)
        {
            ////檢查是否要寫回節目管理系統(暫用)   
            //MAM_PTS_DLL.SysConfig objDll = new MAM_PTS_DLL.SysConfig();
            //string strPGM = objDll.sysConfig_Read("/ServerConfig/ConnectPGM").Trim();
            ReturnMsg objMsg = new ReturnMsg();
            string strGrade_Old = "";

            if (UPDATE_TBPGMD(obj, out objMsg.strErrorMsg) == true)             //修改節目管理系統的節目主檔
            {
                if (UPDATE_TBPROGD_MAIN(obj, out objMsg.strErrorMsg) == true)   //修改MAM的節目主檔
                {
                   objMsg.bolResult = true;

                   //節目子集檔異動或刪除時，要去檢查節目分級欄位，將最嚴重的等級修改至節目主檔，異動或刪除完再檢查  
                   if (QUERY_TBPROGD_CHECK_PROGDGRADE_DELETE(obj.FSPROG_ID, out strGrade_Old))    
                   {    
                       //修改節目主檔及節目管理系統的節目主檔的節目分級    
                       UPDATE_TBPROGM_GRADE(obj.FSPROG_ID, strGrade_Old, obj.FSUPDUSER);    
                       UPDATE_TBPGM_GRADE(obj.FSPROG_ID, strGrade_Old, obj.FSUPDUSER);    
                   }    
                }
                else
                    objMsg.bolResult = false;
            }
            else
                objMsg.bolResult = false;

            return objMsg;
        }

        //修改節目子集檔_傳入節目子集的class
        [WebMethod]
        public Boolean UPDATE_TBPROGD_MAIN(Class.Class_PROGD obj, out string exceptionMsg)
        {
            Boolean bolDB = false;          //寫入節目管理系統資料庫結果
            string strError = "";           //錯誤訊息

            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", obj.FSPROG_ID);

            if (obj.FNEPISODE.ToString() == "0")
                source.Add("FNEPISODE", "");
            else
                source.Add("FNEPISODE", obj.FNEPISODE.ToString());

            source.Add("FSPGDNAME", obj.FSPGDNAME);
            source.Add("FSPGDENAME", obj.FSPGDENAME);

            if (obj.FNLENGTH.ToString() == "0")
                source.Add("FNLENGTH", "");
            else
                source.Add("FNLENGTH", obj.FNLENGTH.ToString());

            if (obj.FNTAPELENGTH.ToString() == "0")
                source.Add("FNTAPELENGTH", "");
            else
                source.Add("FNTAPELENGTH", obj.FNTAPELENGTH.ToString());

            source.Add("FSPRDYEAR", obj.FSPRDYEAR);
            source.Add("FSPROGSRCID", obj.FSPROGSRCID);
            source.Add("FSPROGATTRID", obj.FSPROGATTRID);
            source.Add("FSPROGAUDID", obj.FSPROGAUDID);
            source.Add("FSPROGTYPEID", obj.FSPROGTYPEID);
            source.Add("FSPROGGRADEID", obj.FSPROGGRADEID);
            source.Add("FSPROGLANGID1", obj.FSPROGLANGID1);
            source.Add("FSPROGLANGID2", obj.FSPROGLANGID2);
            source.Add("FSPROGSALEID", obj.FSPROGSALEID);
            source.Add("FSPROGBUYID", obj.FSPROGBUYID);
            source.Add("FSPROGBUYDID", obj.FSPROGBUYDID);
            source.Add("FCRACE", obj.FCRACE);
            source.Add("FSSHOOTSPEC", obj.FSSHOOTSPEC);
            source.Add("FSPROGGRADE", obj.FSPROGGRADE);

            if (obj.FNSHOWEPISODE.ToString() == "0")
                source.Add("FNSHOWEPISODE", "");
            else
                source.Add("FNSHOWEPISODE", obj.FNSHOWEPISODE.ToString());

            source.Add("FSCONTENT", obj.FSCONTENT);
            source.Add("FSMEMO", obj.FSMEMO);
            source.Add("FSCHANNEL_ID", obj.FSCHANNEL_ID);
            source.Add("FSDEL", obj.FSDEL);
            source.Add("FSDELUSER", obj.FSDELUSER);
            source.Add("FSNDMEMO", obj.FSNDMEMO);
            source.Add("FSPROGSPEC", obj.FSPROGSPEC);
            source.Add("FSCR_TYPE", obj.FSCR_TYPE);
            source.Add("FSCR_NOTE", obj.FSCR_NOTE);
            source.Add("FSUPDUSER", obj.FSUPDUSER);

            source.Add("FSEXPIRE_DATE_ACTION", obj.FSEXPIRE_DATE_ACTION);
            source.Add("FDEXPIRE_DATE", obj.FDEXPIRE_DATE.ToString("yyyy-MM-dd HH:mm:ss"));
            source.Add("FSPROGNATIONID", obj.FSPROGNATIONID);

            //若有異動到期日期貨到期日動作寫入TBLOG_FILE_EXPIRE_DELETE資料庫
            if (obj.FDEXPIRE_DATE != obj.Origin_FDEXPIRE_DATE || obj.FSEXPIRE_DATE_ACTION != obj.Origin_FSEXPIRE_DATE_ACTION)
            {
                string myFSTABLE_NAME = "TBPROG_D：FSID：" + obj.FSPROG_ID + " Episode：" + obj.FNEPISODE;
                string myFSRECORD = obj.Origin_FDEXPIRE_DATE.ToString() + " => " + obj.FDEXPIRE_DATE.ToString() + " || " + obj.Origin_FSEXPIRE_DATE_ACTION + " => " + obj.FSEXPIRE_DATE_ACTION;
                INSERT_TBLOG_FILE_EXPIRE_DELETE(myFSTABLE_NAME, myFSRECORD, obj.FSUPDUSER);
            }

            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBPROG_D", source, out strError, obj.FSUPDUSER, true);
            exceptionMsg = strError;

            if (bolDB == true)
                return true;
            else
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToMAM-UPDATE PROGD", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "ERROR:" + strError);
                EMAIL_PROGD_Error(obj, strError, "MAM", "UPDATE");             //寄送Mail
                return false;
            }    
        }

        /// <summary>
        /// 呼叫寫入 LOG紀錄
        /// </summary>
        /// <param name="FSTABLE_NAME"></param>
        /// <param name="FSRECORD"></param>
        /// <param name="FSUPDATED_BY"></param>
        /// <returns></returns>
        public bool INSERT_TBLOG_FILE_EXPIRE_DELETE(string FSTABLE_NAME, string FSRECORD, string FSUPDATED_BY)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSTABLE_NAME", FSTABLE_NAME);
            source.Add("FSRECORD", FSRECORD);
            source.Add("FSUPDATED_BY", FSUPDATED_BY);
            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBLOG_FILE_EXPIRE_DELETE", source, FSUPDATED_BY);
        }

        /// <summary>
        /// 修改節目管理系統的節目子集檔_傳入節目子集的class 
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="exceptionMsg"></param>
        /// <returns></returns>
        [WebMethod]
        public Boolean UPDATE_TBPGMD(Class.Class_PROGD obj, out string exceptionMsg)
        {
            Boolean bolDB = false;          //寫入節目管理系統資料庫結果
            string strError = "";           //錯誤訊息

            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROGID", obj.FSPROG_ID);

            if (obj.FNEPISODE.ToString() == "0")
                source.Add("FNEPISODE", "");
            else
                source.Add("FNEPISODE", obj.FNEPISODE.ToString());

            source.Add("FSPGDNAME", obj.FSPGDNAME);
            source.Add("FSPGDENAME", obj.FSPGDENAME);

            if (obj.FNLENGTH.ToString() == "0")
                source.Add("FNLENGTH", "");
            else
                source.Add("FNLENGTH", obj.FNLENGTH.ToString());

            if (obj.FNTAPELENGTH.ToString() == "0")
                source.Add("FNTAPELENGTH", "");
            else
                source.Add("FNTAPELENGTH", obj.FNTAPELENGTH.ToString());
            
            source.Add("FSPROGSRCID", obj.FSPROGSRCID);
            source.Add("FSPROGATTRID", obj.FSPROGATTRID);
            source.Add("FSPROGAUDID", obj.FSPROGAUDID);
            source.Add("FSPROGTYPEID", obj.FSPROGTYPEID);
            source.Add("FSPROGGRADEID", obj.FSPROGGRADEID);
            source.Add("FSPROGLANGID1", obj.FSPROGLANGID1);
            source.Add("FSPROGLANGID2", obj.FSPROGLANGID2);
            source.Add("FSPROGSALEID", obj.FSPROGSALEID);
            source.Add("FSPROGBUYID", obj.FSPROGBUYID);
            source.Add("FSPROGBUYDID", obj.FSPROGBUYDID);
            source.Add("FCRACE", obj.FCRACE);
            source.Add("FSSHOOTSPEC", obj.FSSHOOTSPEC);
            source.Add("FSPROGSPEC", obj.FSPROGSPEC);
            source.Add("FSPRDYEAR", obj.FSPRDYEAR);
            source.Add("FSPROGGRADE", obj.FSPROGGRADE);
            source.Add("FSCONTENT", obj.FSCONTENT);
            source.Add("FSMEMO", obj.FSMEMO);

            if (obj.FNSHOWEPISODE.ToString() == "0")
                source.Add("FNSHOWEPISODE", "");
            else
                source.Add("FNSHOWEPISODE", obj.FNSHOWEPISODE.ToString());
            
            source.Add("FSUPDUSER", obj.FSUPDUSER);
            source.Add("FSPROGNATIONID", obj.FSPROGNATIONID);

            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction_With_DBLink("SP_U_TBPROG_D_EXTERNAL", source, out strError, obj.FSUPDUSER, true);
            exceptionMsg = strError;

            if (bolDB == true)
                return true;
            else
            {
                //寫入節目管理系統錯誤時，要因應的作法(目前只寫進LOG)
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToPGM-UPDATE PROGD", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "ERROR:" + strError);
                EMAIL_PROGD_Error(obj, strError, "PGM", "UPDATE");             //寄送Mail
                return false;
            } 
        }

        //查詢節目子集檔
        [WebMethod]
        public List<Class_PROGD> QUERY_TBPROG_D(Class.Class_PROGD obj)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();

            if (obj.FSPROG_ID == null)
                source.Add("FSPROG_ID", "");
            else
                source.Add("FSPROG_ID", obj.FSPROG_ID);

            if (obj.FNEPISODE.ToString() == "0")
                source.Add("FNEPISODE", "");
            else
                source.Add("FNEPISODE", obj.FNEPISODE.ToString());

            if (obj.FSPGDNAME == null)
                source.Add("FSPGDNAME", "");
            else
                source.Add("FSPGDNAME", obj.FSPGDNAME);

            if (obj.FSPGDENAME == null)
                source.Add("FSPGDENAME", "");
            else
                source.Add("FSPGDENAME", obj.FSPGDENAME);

            if (obj.FNLENGTH.ToString() == "0")
                source.Add("FNLENGTH", "");
            else
                source.Add("FNLENGTH", obj.FNLENGTH.ToString());

            if (obj.FNTAPELENGTH.ToString() == "0")
                source.Add("FNTAPELENGTH", "");
            else
                source.Add("FNTAPELENGTH", obj.FNTAPELENGTH.ToString());

            if (obj.FSPRDYEAR == null)
                source.Add("FSPRDYEAR", "");
            else
                source.Add("FSPRDYEAR", obj.FSPRDYEAR);

            if (obj.FSPROGSRCID == null)
                source.Add("FSPROGSRCID", "");
            else
                source.Add("FSPROGSRCID", obj.FSPROGSRCID);

            if (obj.FSPROGATTRID == null)
                source.Add("FSPROGATTRID", "");
            else
                source.Add("FSPROGATTRID", obj.FSPROGATTRID);

            if (obj.FSPROGAUDID == null)
                source.Add("FSPROGAUDID", "");
            else
                source.Add("FSPROGAUDID", obj.FSPROGAUDID);

            if (obj.FSPROGTYPEID == null)
                source.Add("FSPROGTYPEID", "");
            else
                source.Add("FSPROGTYPEID", obj.FSPROGTYPEID);

            if (obj.FSPROGGRADEID == null)
                source.Add("FSPROGGRADEID", "");
            else
                source.Add("FSPROGGRADEID", obj.FSPROGGRADEID);

            if (obj.FSPROGLANGID1 == null)
                source.Add("FSPROGLANGID1", "");
            else
                source.Add("FSPROGLANGID1", obj.FSPROGLANGID1);

            if (obj.FSPROGLANGID2 == null)
                source.Add("FSPROGLANGID2", "");
            else
                source.Add("FSPROGLANGID2", obj.FSPROGLANGID2);

            if (obj.FSPROGSALEID == null)
                source.Add("FSPROGSALEID", "");
            else
                source.Add("FSPROGSALEID", obj.FSPROGSALEID);

            if (obj.FSPROGBUYID == null)
                source.Add("FSPROGBUYID", "");
            else
                source.Add("FSPROGBUYID", obj.FSPROGBUYID);

            if (obj.FSPROGBUYDID == null)
                source.Add("FSPROGBUYDID", "");
            else
                source.Add("FSPROGBUYDID", obj.FSPROGBUYDID);

            if (obj.FCRACE == null)
                source.Add("FCRACE", "");
            else
                source.Add("FCRACE", obj.FCRACE);

            if (obj.FSSHOOTSPEC == null)
                source.Add("FSSHOOTSPEC", "");
            else
                source.Add("FSSHOOTSPEC", obj.FSSHOOTSPEC);

            if (obj.FSPROGGRADE == null)
                source.Add("FSPROGGRADE", "");
            else
                source.Add("FSPROGGRADE", obj.FSPROGGRADE);
         
            if (obj.FNSHOWEPISODE.ToString() == "0")
                source.Add("FNSHOWEPISODE", "");
            else
                source.Add("FNSHOWEPISODE", obj.FNSHOWEPISODE.ToString());

            if (obj.FSCONTENT == null)
                source.Add("FSCONTENT", "");
            else
                source.Add("FSCONTENT", obj.FSCONTENT);

            if (obj.FSMEMO == null)
                source.Add("FSMEMO", "");
            else
                source.Add("FSMEMO", obj.FSMEMO);

            if (obj.FSCHANNEL_ID == null)
                source.Add("FSCHANNEL_ID", "");
            else
                source.Add("FSCHANNEL_ID", obj.FSCHANNEL_ID);

            if (obj.FSDEL == null)
                source.Add("FSDEL", "");
            else
                source.Add("FSDEL", obj.FSDEL);

            if (obj.FSDELUSER == null)
                source.Add("FSDELUSER", "");
            else
                source.Add("FSDELUSER", obj.FSDELUSER);

            if (obj.FSPROGSPEC == null)
                source.Add("FSPROGSPEC", "");
            else
                source.Add("FSPROGSPEC", obj.FSPROGSPEC);

            if (obj.FSCR_TYPE == null)
                source.Add("FSCR_TYPE", "");
            else
                source.Add("FSCR_TYPE", obj.FSCR_TYPE);

            if (obj.FSCR_NOTE == null)
                source.Add("FSCR_NOTE", "");
            else
                source.Add("FSCR_NOTE", obj.FSCR_NOTE);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_D_BYCONDITIONS", source, out sqlResult))
                return new List<Class_PROGD>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PROGD(sqlResult);
        }

        //查詢節目子集檔的2nd資料
        [WebMethod]
        public string Query_TBPROG_D_ND(string strProgID, string strEpisode)
        {
            List<Dictionary<string, string>> sqlResult;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPROG_ID", strProgID);
            sqlParameters.Add("FNEPISODE", strEpisode);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_D", sqlParameters, out sqlResult))
                return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count > 0)
                return sqlResult[0]["FSNDMEMO"];
            else
                return "";
        }

        //刪除節目子集
        [WebMethod]
        public ReturnMsg DELETE_TBPROGD(Class.Class_PROGD obj, string strDelUser)
        {
            ReturnMsg objMsg = new ReturnMsg();
            string strGrade_Old = "";

            if (DELETE_TBPROGD_PGM(obj, out objMsg.strErrorMsg, strDelUser) == true)              //刪除節目管理系統的節目子集
            {
                if (DELETE_TBPROGD_MAM(obj, out objMsg.strErrorMsg, strDelUser) == true)          //刪除MAM的節目子集
                {
                    objMsg.bolResult = true;

                    //節目子集檔異動或刪除時，要去檢查節目分級欄位，將最嚴重的等級修改至節目主檔，異動或刪除完再檢查  
                    if (QUERY_TBPROGD_CHECK_PROGDGRADE_DELETE(obj.FSPROG_ID,out strGrade_Old))    
                   {
                       //修改節目主檔及節目管理系統的節目主檔的節目分級
                       UPDATE_TBPROGM_GRADE(obj.FSPROG_ID, strGrade_Old, obj.FSUPDUSER);
                       UPDATE_TBPGM_GRADE(obj.FSPROG_ID, strGrade_Old, obj.FSUPDUSER);
                   }
                }                    
                else
                    objMsg.bolResult = false;
            }
            else
                objMsg.bolResult = false;

            return objMsg;
        }

        //刪除MAM的節目子集
        [WebMethod]
        public Boolean DELETE_TBPROGD_MAM(Class.Class_PROGD obj, out string exceptionMsg, string strDelUser)
        {
            Boolean bolDB = false;          //寫入節目管理系統資料庫結果
            string strError = "";           //錯誤訊息

            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", obj.FSPROG_ID);
            source.Add("FNEPISODE", obj.FNEPISODE.ToString());
            source.Add("FSDEL", "Y");
            source.Add("FSDELUSER", strDelUser);

            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_TBPROG_D", source, out strError, strDelUser, true);
            exceptionMsg = strError;

            if (bolDB == true)
                return true;
            else
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToMAM-DELETE PROGD", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "ERROR:" + strError);
                EMAIL_PROGD_Error(obj, strError, "MAM", "DELETE");             //寄送Mail
                return false;
            }
        }

        //刪除節目管理系統的節目子集
        [WebMethod]
        public Boolean DELETE_TBPROGD_PGM(Class.Class_PROGD obj, out string exceptionMsg, string strDelUser)
        {
            Boolean bolDB = false;          //寫入節目管理系統資料庫結果
            string strError = "";           //錯誤訊息

            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROGID", obj.FSPROG_ID);
            source.Add("FNEPISODE", obj.FNEPISODE.ToString());

            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction_With_DBLink("SP_D_TBPROG_D_EXTERNAL", source, out strError, strDelUser, true);
            exceptionMsg = strError;

            if (bolDB == true)
                return true;
            else
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToPGM-DELETE PROGD", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "ERROR:" + strError);
                EMAIL_PROGD_Error(obj, strError, "PGM", "DELETE");                   //寄送Mail
                return false;
            }
        }

        //比對代碼檔
        string compareCode_Code(string strID, string strTable)
        {
            for (int i = 0; i < m_CodeData.Count; i++)
            {
                if (m_CodeData[i].ID.ToString().Trim().Equals(strID) && m_CodeData[i].TABLENAME.ToString().Trim().Equals(strTable))
                {
                    return m_CodeData[i].NAME.ToString().Trim();
                }
            }
            return "";
        }

        //比對代碼檔_外購類別細項
        string compareCode_CodeBuyD(string strID, string strDID)
        {
            for (int i = 0; i < m_CodeDataBuyD.Count; i++)
            {
                if (m_CodeDataBuyD[i].ID.ToString().Trim().Equals(strID) && m_CodeDataBuyD[i].IDD.ToString().Trim().Equals(strDID))
                {
                    return m_CodeDataBuyD[i].NAME.ToString().Trim();
                }
            }
            return "";
        }

        //比對代碼檔_節目規格及拍攝規格
        string compareCode_Code_Spec(string strID, string strTable)
        {
            string strProgSpec = "";  

            for (int i = 0; i < m_CodeData.Count; i++)
            {
                if (strID.IndexOf(m_CodeData[i].ID.ToString().Trim()) > -1 && m_CodeData[i].TABLENAME.ToString().Trim().Equals(strTable))
                {
                    strProgSpec = strProgSpec + m_CodeData[i].NAME.ToString().Trim() + " ";
                }
            }
            return strProgSpec;
        }

        //寄送EMAIL通知_資料同步時發生問題要通知管理者
        [WebMethod]
        public void EMAIL_PROGD_Error(Class.Class_PROGD obj, string strError, string strSystem, string strStatus)
        {
            //讀取通知的管理者(暫用)   
            MAM_PTS_DLL.SysConfig objDll = new MAM_PTS_DLL.SysConfig();
            string strErrorMail = objDll.sysConfig_Read("/ServerConfig/SynchronizeError").Trim();
            string strPROG_NAME = MAMFunctions.QueryProgName(obj.FSPROG_ID.Trim());   
            string strTitle = "";

            StringBuilder msg = new StringBuilder();
            msg.AppendLine("節目名稱：" + strPROG_NAME);            
            msg.AppendLine(" , 節目編號：" + obj.FSPROG_ID.Trim());
            msg.AppendLine(" , 集別：" + obj.FNEPISODE.ToString().Trim());
            msg.AppendLine(" , 節目子集名稱：" + obj.FSPGDNAME.Trim());

            if (strSystem.Trim() == "MAM")
                msg.AppendLine(" , 同步失敗資料庫：數位片庫");
            else if (strSystem.Trim() == "PGM")
                msg.AppendLine(" , 同步失敗資料庫：節目管理系統");

            if (strStatus.Trim() == "INSERT")
                msg.AppendLine(" , 動作：新增");
            else if (strStatus.Trim() == "UPDATE")
                msg.AppendLine(" , 動作：修改");
            else if (strStatus.Trim() == "DELETE")
                msg.AppendLine(" , 動作：刪除");

            msg.AppendLine(" , 錯誤訊息：" + strError);

            msg.AppendLine(" , 新增者：" + obj.FSCRTUSER.Trim() + " , 修改者：" + obj.FSUPDUSER.Trim());

            strTitle = "節目子集資料同步失敗通知-" + strPROG_NAME;

            MAM_PTS_DLL.Protocol.SendEmail(strErrorMail, strTitle, msg.ToString());
        }

        //比對代碼檔(刪除時專用)
        string compareCode_CodeDel(string strID, string strTable, List<Class_CODE> CodeDataDel)
        {
            for (int i = 0; i < CodeDataDel.Count; i++)
            {
                if (CodeDataDel[i].ID.ToString().Trim().Equals(strID.Trim()) && CodeDataDel[i].TABLENAME.ToString().Trim().Equals(strTable.Trim()))
                {
                    return CodeDataDel[i].NAME.ToString().Trim();
                }
            }
            return "";
        }

        //刪除節目資料時，要檢查該節目除了節目子集外是否有其他資料
        [WebMethod]
        public List<Class_DELETE_MSG> DELETE_TBPROGD_Check(Class.Class_PROGD obj, string strDelUser)
        {
            string strProgID = obj.FSPROG_ID.ToString().Trim();
            string strEpisode = obj.FNEPISODE.ToString().Trim();

            //DELETE_TBPROGD_Check_PROGD(strProgID, strEpisode);            //刪除節目子集資料時，檢查節目子集檔(刪除子集不需要考量是否有子集，因此先mark掉)
            DELETE_TBPROGD_Check_PROGMEN(strProgID, strEpisode);            //刪除節目子集資料時，檢查節目相關人員檔
            DELETE_TBPROGD_Check_PROGRACE(strProgID, strEpisode);           //刪除節目子集資料時，檢查參展得獎記錄檔
            DELETE_TBPROGD_Check_PROGLINK(strProgID, strEpisode);           //刪除節目子集資料時，檢查扣播出次數節目子集檔
            DELETE_TBPROGD_Check_BROCAST(strProgID, strEpisode);            //刪除節目子集資料時，檢查送帶轉檔
            DELETE_TBPROGD_Check_ARCHIVE(strProgID, strEpisode);            //刪除節目子集資料時，檢查入庫檔
            DELETE_TBPROGD_Check_QUEUE(strProgID, strEpisode);              //刪除節目子集資料時，檢查排表
            DELETE_TBPROGD_Check_PGM_DB(strProgID, strEpisode);                         //刪除節目子集資料時，檢查排表

            return m_DeleteMsg;
        }

        //刪除節目資料時，檢查節目子集檔
        public void DELETE_TBPROGD_Check_PROGD(string ProgID,string Episode)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", ProgID);
            source.Add("FNEPISODE", Episode);

            List<Dictionary<string, string>> sqlResult;
            MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_D_BYPROGID_EPISODE", source, out sqlResult);

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_DELETE_MSG obj = new Class_DELETE_MSG();
                obj.FSSystem = "節目子集資料";
                obj.FSCheckMsg = "集別 - " + MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]);

                if (sqlResult[i]["FNEPISODE_NAME"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 子集名稱 - " + sqlResult[i]["FNEPISODE_NAME"].ToString().Trim();

                m_DeleteMsg.Add(obj);
            }
        }

        //刪除節目資料時，檢查節目相關人員檔
        public void DELETE_TBPROGD_Check_PROGMEN(string ProgID, string Episode)
        {
            m_CodeDataMen = MAMFunctions.Transfer_Class_CODE("SP_Q_TBPROGMEN_CODE");

            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", ProgID);
            source.Add("FNEPISODE", Episode);
            source.Add("FSTITLEID", "");
            source.Add("FSSTAFFID", "");

            List<Dictionary<string, string>> sqlResult;
            MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROGMEN", source, out sqlResult);

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_DELETE_MSG obj = new Class_DELETE_MSG();
                obj.FSSystem = "節目相關人員資料";

                if (MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]) != "")
                    obj.FSCheckMsg = "集別 - " + MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]);

                obj.FSCheckMsg = obj.FSCheckMsg + " ， 頭銜 - " + compareCode_CodeDel(sqlResult[i]["FSTITLEID"], "TBZTITLE", m_CodeDataMen);       //頭銜名稱

                if (sqlResult[i]["FSNAME"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 公司/姓名 - " + sqlResult[i]["FSNAME"].ToString().Trim();             //公司/姓名名稱

                if (sqlResult[i]["FSSTAFFID"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 工作人員 - " + compareCode_CodeDel(sqlResult[i]["FSSTAFFID"], "TBSTAFF", m_CodeDataMen);//工作人員

                m_DeleteMsg.Add(obj);
            }
        }

        //刪除節目資料時，檢查參展得獎記錄檔
        public void DELETE_TBPROGD_Check_PROGRACE(string ProgID, string Episode)
        {
            m_CodeDataRace = MAMFunctions.Transfer_Class_CODE("SP_Q_TBPROGRACE_CODE");

            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", ProgID);
            source.Add("FNEPISODE", Episode);

            List<Dictionary<string, string>> sqlResult;
            MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROGRACE_BYPROGID_EPISODE", source, out sqlResult);

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_DELETE_MSG obj = new Class_DELETE_MSG();
                obj.FSSystem = "節目參展得獎資料";

                if (MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]) != "")
                    obj.FSCheckMsg = "集別 - " + MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]);

                if (sqlResult[i]["FSPROGSTATUSID"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 參展狀態 - " + compareCode_CodeDel(sqlResult[i]["FSPROGSTATUSID"], "TBZPROGSTATUS", m_CodeDataRace);       //參展狀態

                if (sqlResult[i]["FSAREA"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 參展區域 - " + sqlResult[i]["FSAREA"].ToString().Trim();

                if (sqlResult[i]["FSPROGRACE"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 參展名稱 - " + sqlResult[i]["FSPROGRACE"].ToString().Trim();

                if (sqlResult[i]["FSPROGWIN"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 得獎名稱 - " + sqlResult[i]["FSPROGWIN"].ToString().Trim();

                m_DeleteMsg.Add(obj);
            }
        }

        //刪除節目資料時，檢查扣播出次數節目子集檔
        public void DELETE_TBPROGD_Check_PROGLINK(string ProgID, string Episode)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", ProgID);
            source.Add("FNEPISODE", Episode);

            List<Dictionary<string, string>> sqlResult;
            MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROGLINK_BYPROGID_EPISODE", source, out sqlResult);

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_DELETE_MSG obj = new Class_DELETE_MSG();
                obj.FSSystem = "節目衍生子集資料";

                if (MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]) != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + "節目 - " + sqlResult[i]["FSPROG_ID_NAME"] + " ， 子集名稱 - " + sqlResult[i]["FNEPISODE_NAME"] + " ， 集別 - " + MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]);

                if (MAMFunctions.compareNumber(sqlResult[i]["FNORGEPISODE"]) != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 扣播出次數節目 - " + sqlResult[i]["FSORGPROG_ID_NAME"] + " ， 扣播出次數子集名稱 - " + sqlResult[i]["FNORGEPISODE_NAME"] + " ， 扣播出次數集別 - " + MAMFunctions.compareNumber(sqlResult[i]["FNORGEPISODE"]);

                m_DeleteMsg.Add(obj);
            }
        }

        //刪除節目資料時，檢查送帶轉檔
        public void DELETE_TBPROGD_Check_BROCAST(string ProgID, string Episode)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSBRO_TYPE", "G");
            source.Add("FSID", ProgID);
            source.Add("FNEPISODE", Episode);

            List<Dictionary<string, string>> sqlResult;
            MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBBROADCAST_BYIDEPISODE", source, out sqlResult);

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_DELETE_MSG obj = new Class_DELETE_MSG();
                obj.FSSystem = "送帶轉檔資料";

                if (MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]) != "")
                    obj.FSCheckMsg = "集別 - " + MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]);
                else
                    obj.FSCheckMsg = "集別 - 無";

                if (sqlResult[i]["FNEPISODE_NAME"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 子集名稱 - " + sqlResult[i]["FNEPISODE_NAME"].ToString().Trim();

                if (sqlResult[i]["WANT_FDBRO_DATE"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 申請日期 - " + sqlResult[i]["WANT_FDBRO_DATE"].ToString().Trim();

                m_DeleteMsg.Add(obj);
            }
        }

        //刪除節目資料時，檢查入庫檔
        public void DELETE_TBPROGD_Check_ARCHIVE(string ProgID, string Episode)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSTYPE", "G");
            source.Add("FSID", ProgID);
            source.Add("FNEPISODE", Episode);

            List<Dictionary<string, string>> sqlResult;
            MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBARCHIVE_BYID_EPISODE", source, out sqlResult);

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_DELETE_MSG obj = new Class_DELETE_MSG();
                obj.FSSystem = "入庫資料";

                if (MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]) != "")
                    obj.FSCheckMsg = "集別 - " + MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]);
                else
                    obj.FSCheckMsg = "集別 - 無";

                if (sqlResult[i]["FNEPISODE_NAME"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 子集名稱 - " + sqlResult[i]["FNEPISODE_NAME"].ToString().Trim();

                if (sqlResult[i]["WANT_FDARC_DATE"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 申請日期 - " + sqlResult[i]["WANT_FDARC_DATE"].ToString().Trim();

                m_DeleteMsg.Add(obj);
            }
        }

        //刪除節目資料時，檢查排表
        public void DELETE_TBPROGD_Check_QUEUE(string ProgID, string Episode)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", ProgID);
            source.Add("FNEPISODE", Episode);

            List<Dictionary<string, string>> sqlResult;
            MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPGM_QUEUE_BY_PROGID", source, out sqlResult);

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_DELETE_MSG obj = new Class_DELETE_MSG();
                obj.FSSystem = "排表資料";

                if (MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]) != "")
                    obj.FSCheckMsg = "集別 - " + MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]);
                else
                    obj.FSCheckMsg = "集別 - 無";

                if (sqlResult[i]["Want_Date"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 排播日期 - " + sqlResult[i]["Want_Date"].ToString().Trim();

                if (sqlResult[i]["FSBEG_TIME"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 排播時間 - " + sqlResult[i]["FSBEG_TIME"].ToString().Trim();

                if (sqlResult[i]["FSCHANNEL_NAME"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 排播頻道 - " + sqlResult[i]["FSCHANNEL_NAME"].ToString().Trim();

                m_DeleteMsg.Add(obj);
            }
        }

        //刪除節目資料時，檢查節目管理系統的各個資料表
        public void DELETE_TBPROGD_Check_PGM_DB(string ProgID,string Episode)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", ProgID);
            source.Add("FNEPISODE", Episode);

            List<Dictionary<string, string>> sqlResult;
            MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_D_DELETE_DETAIL_EXTERNAL", source, out sqlResult);

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_DELETE_MSG obj = new Class_DELETE_MSG();
                //TableCName是取中文名稱，若要取英文名稱則是TableName
                obj.FSSystem = "節目管理系統資料-" + sqlResult[i]["TableCName"];
                obj.FSCheckMsg = "資料筆數 - " + sqlResult[i]["DataCount"] + "筆";

                m_DeleteMsg.Add(obj);
            }
        }   

        //節目子集檔異動或刪除時，要去檢查節目分級欄位，將最嚴重的等級修改至節目主檔，異動或刪除完再檢查   
        public Boolean QUERY_TBPROGD_CHECK_PROGDGRADE_DELETE(string strProgID , out string strGrade)
        {           
            List<int> ListGrade = new List<int>();
            strGrade = "";          //要傳回修改的節目分級ID
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", strProgID);

            List<Dictionary<string, string>> sqlResult;
            MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_D_BYPROGID", source, out sqlResult);

            for (int i = 0; i < sqlResult.Count; i++)
            {
                ListGrade.Add(Convert.ToInt32(sqlResult[i]["FSPROGGRADEID"]));
            }

            ListGrade.Sort();

            if (ListGrade.Count == 0)
                return false;    //沒有得比對，就回傳false，不修改主檔

            return Query_TBPROG_M_PROGDGRADE(strProgID, ListGrade[ListGrade.Count - 1].ToString().PadLeft(2, '0'),out strGrade); //查詢節目主檔的節目分級資料，並且比對 
        }

        //查詢節目主檔的節目分級資料，並且比對      
        public Boolean Query_TBPROG_M_PROGDGRADE(string strProgID, string strGradeID, out string strGradeID_old)
        {
            List<Dictionary<string, string>> sqlResult;
            strGradeID_old = "";
            Int32 intGradeID = Convert.ToInt32(strGradeID.Trim());
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPROG_ID", strProgID);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_M", sqlParameters, out sqlResult))
                return false;    // 發生SQL錯誤時，回傳false，表示不要改

            if (sqlResult.Count > 0)
            {
                if (intGradeID == Convert.ToInt32(sqlResult[0]["FSPROGGRADEID"].Trim()))
                    return false;
                else
                {
                    strGradeID_old = strGradeID.Trim();
                    return true;
                }
            }
            else
                return false;    // 發生SQL錯誤時，回傳false，表示不要改
        }

        //修改節目主檔的節目分級
        [WebMethod]
        public void UPDATE_TBPROGM_GRADE(string strProgID, string strGradeID, string strUpdateID)
        {                       
            Dictionary<string, string> source = new Dictionary<string, string>();
            string strError = "";           //錯誤訊息
            source.Add("FSPROG_ID", strProgID);
            source.Add("FSPROGGRADEID", strGradeID);

            MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBPROG_M_GRADE", source, out strError, strUpdateID, true);
        }

        //修改節目管理系統的節目主檔的節目分級  
        public void UPDATE_TBPGM_GRADE(string strProgID, string strGradeID,string strUpdateID)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            string strError = "";           //錯誤訊息
            source.Add("FSPROGID", strProgID);
            source.Add("FSPROGGRADEID", strGradeID);

            MAM_PTS_DLL.DbAccess.Do_Transaction_With_DBLink("SP_U_TBPROG_M_EXTERNAL_GRADE", source, out strError, strUpdateID, true);         
        }

        #region 整批修改節目管理系統的節目子集資料、MAM的節目子集資料


        /// <summary>
        /// 整批修改節目管理系統的節目子集資料、MAM的節目子集資料
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="FNEPISODE_END"></param>
        /// <param name="bolPGDNAME"></param>
        /// <param name="bolPGDENAME"></param>
        /// <param name="bolLENGTH"></param>
        /// <param name="bolPROGGRADEID"></param>
        /// <param name="bolPROGGRADE"></param>
        /// <param name="bolCONTENT"></param>
        /// <param name="bolcbMEMO"></param>
        /// <param name="bolCR_TYPE"></param>
        /// <param name="bolCR_NOTE"></param>
        /// <param name="bolPROGSRCID"></param>
        /// <param name="bolPROGATTRID"></param>
        /// <param name="bolPROGAUDID"></param>
        /// <param name="bolPROGTYPEID"></param>
        /// <param name="bolPROGLANGID1"></param>
        /// <param name="bolPROGLANGID2"></param>
        /// <param name="bolPROGBUYID"></param>
        /// <param name="bolPROGSALEID"></param>
        /// <param name="bolPROGSPEC"></param>
        /// <param name="bolSHOOTSPEC"></param>
        /// <returns></returns>
        [WebMethod]
        public ReturnMsg UPDATE_TBPROGD_ALL(Class.Class_PROGD obj, short FNEPISODE_END, Boolean bolPGDNAME, Boolean bolPGDENAME, Boolean bolLENGTH, Boolean bolPROGGRADEID, Boolean bolPROGGRADE, Boolean bolCONTENT,
        Boolean bolcbMEMO, Boolean bolCR_TYPE, Boolean bolCR_NOTE, Boolean bolPROGSRCID, Boolean bolPROGATTRID, Boolean bolPROGAUDID, Boolean bolPROGTYPEID, Boolean bolPROGLANGID1, Boolean bolPROGLANGID2, Boolean bolPROGBUYID, Boolean bolPROGSALEID, Boolean bolPROGSPEC, Boolean bolSHOOTSPEC, Boolean bolPROGNATIONID, Boolean bolEXPIRE_DATE, Boolean bolEXPIRE_DATE_ACTION, Boolean bolPRDYEAR)
        {  
            ReturnMsg objMsg = new ReturnMsg();
            string strGrade_Old = "";

            if (UPDATE_TBPGMD_ALL(obj, out objMsg.strErrorMsg,FNEPISODE_END,bolPGDNAME,bolPGDENAME,bolLENGTH,bolPROGGRADEID,bolPROGGRADE,bolCONTENT,bolcbMEMO,bolCR_TYPE,bolCR_NOTE,
            bolPROGSRCID, bolPROGATTRID, bolPROGAUDID, bolPROGTYPEID, bolPROGLANGID1, bolPROGLANGID2, bolPROGBUYID, bolPROGSALEID, bolPROGSPEC, bolSHOOTSPEC, bolPROGNATIONID, bolPRDYEAR) == true)             //修改節目管理系統的節目主檔
            {
                if (UPDATE_TBPROGD_MAIN_ALL(obj, out objMsg.strErrorMsg,FNEPISODE_END,bolPGDNAME,bolPGDENAME,bolLENGTH,bolPROGGRADEID,bolPROGGRADE,bolCONTENT,bolcbMEMO,bolCR_TYPE,bolCR_NOTE,
                bolPROGSRCID, bolPROGATTRID, bolPROGAUDID, bolPROGTYPEID, bolPROGLANGID1, bolPROGLANGID2, bolPROGBUYID, bolPROGSALEID, bolPROGSPEC, bolSHOOTSPEC, bolPROGNATIONID, bolEXPIRE_DATE, bolEXPIRE_DATE_ACTION, bolPRDYEAR) == true)   //修改MAM的節目主檔
                {
                    objMsg.bolResult = true;

                    //節目子集檔異動或刪除時，要去檢查節目分級欄位，將最嚴重的等級修改至節目主檔，異動或刪除完再檢查  
                    if (QUERY_TBPROGD_CHECK_PROGDGRADE_DELETE(obj.FSPROG_ID, out strGrade_Old))
                    {
                        //修改節目主檔及節目管理系統的節目主檔的節目分級    
                        UPDATE_TBPROGM_GRADE(obj.FSPROG_ID, strGrade_Old, obj.FSUPDUSER);
                        UPDATE_TBPGM_GRADE(obj.FSPROG_ID, strGrade_Old, obj.FSUPDUSER);
                    }
                }
                else
                    objMsg.bolResult = false;
            }
            else
                objMsg.bolResult = false;

            return objMsg;
        }

        /// <summary>
        /// 修改MAM節目子集檔_傳入節目子集的class
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="exceptionMsg"></param>
        /// <param name="FNEPISODE_END"></param>
        /// <param name="bolPGDNAME"></param>
        /// <param name="bolPGDENAME"></param>
        /// <param name="bolLENGTH"></param>
        /// <param name="bolPROGGRADEID"></param>
        /// <param name="bolPROGGRADE"></param>
        /// <param name="bolCONTENT"></param>
        /// <param name="bolcbMEMO"></param>
        /// <param name="bolCR_TYPE"></param>
        /// <param name="bolCR_NOTE"></param>
        /// <param name="bolPROGSRCID"></param>
        /// <param name="bolPROGATTRID"></param>
        /// <param name="bolPROGAUDID"></param>
        /// <param name="bolPROGTYPEID"></param>
        /// <param name="bolPROGLANGID1"></param>
        /// <param name="bolPROGLANGID2"></param>
        /// <param name="bolPROGBUYID"></param>
        /// <param name="bolPROGSALEID"></param>
        /// <param name="bolPROGSPEC"></param>
        /// <param name="bolSHOOTSPEC"></param>
        /// <returns></returns>
        [WebMethod]
        public Boolean UPDATE_TBPROGD_MAIN_ALL(Class.Class_PROGD obj, out string exceptionMsg, short FNEPISODE_END, Boolean bolPGDNAME, Boolean bolPGDENAME, Boolean bolLENGTH, Boolean bolPROGGRADEID, Boolean bolPROGGRADE, Boolean bolCONTENT,
        Boolean bolcbMEMO, Boolean bolCR_TYPE, Boolean bolCR_NOTE, Boolean bolPROGSRCID, Boolean bolPROGATTRID, Boolean bolPROGAUDID, Boolean bolPROGTYPEID, Boolean bolPROGLANGID1, Boolean bolPROGLANGID2, Boolean bolPROGBUYID, Boolean bolPROGSALEID, Boolean bolPROGSPEC, Boolean bolSHOOTSPEC, Boolean bolPROGNATIONID, Boolean bolEXPIRE_DATE, Boolean bolEXPIRE_DATE_ACTION, Boolean bolPRDYEAR)
        {
            Boolean bolDB = false;          //寫入節目管理系統資料庫結果
            string strError = "";           //錯誤訊息

            Dictionary<string, string> source = new Dictionary<string, string>();

            if (bolPGDNAME == true)
                source.Add("bolPGDNAME", "Y");
            else
                source.Add("bolPGDNAME", "N");

            if (bolPGDENAME == true)
                source.Add("bolPGDENAME", "Y");
            else
                source.Add("bolPGDENAME", "N");

            if ( bolLENGTH == true)
                source.Add("bolLENGTH", "Y");
            else
                source.Add("bolLENGTH", "N");

            if ( bolPROGGRADEID == true)
                source.Add("bolPROGGRADEID", "Y");
            else
                source.Add("bolPROGGRADEID", "N");

            if ( bolPROGGRADE == true)
                source.Add("bolPROGGRADE", "Y");
            else
                source.Add("bolPROGGRADE", "N");

            if ( bolCONTENT == true)
                source.Add("bolCONTENT", "Y");
            else
                source.Add("bolCONTENT", "N");

            if ( bolcbMEMO == true)
                source.Add("bolcbMEMO", "Y");
            else
                source.Add("bolcbMEMO", "N");

            if ( bolCR_TYPE == true)
                source.Add("bolCR_TYPE", "Y");
            else
                source.Add("bolCR_TYPE", "N");

            if ( bolCR_NOTE == true)
                source.Add("bolCR_NOTE", "Y");
            else
                source.Add("bolCR_NOTE", "N");

            if ( bolPROGSRCID == true)
                source.Add("bolPROGSRCID", "Y");
            else
                source.Add("bolPROGSRCID", "N");

            if ( bolPROGATTRID == true)
                source.Add("bolPROGATTRID", "Y");
            else
                source.Add("bolPROGATTRID", "N");

            if ( bolPROGAUDID == true)
                source.Add("bolPROGAUDID", "Y");
            else
                source.Add("bolPROGAUDID", "N");

            if (bolPROGTYPEID  == true)
                source.Add("bolPROGTYPEID", "Y");
            else
                source.Add("bolPROGTYPEID", "N");

            if ( bolPROGLANGID1 == true)
                source.Add("bolPROGLANGID1", "Y");
            else
                source.Add("bolPROGLANGID1", "N");

            if ( bolPROGLANGID2 == true)
                source.Add("bolPROGLANGID2", "Y");
            else
                source.Add("bolPROGLANGID2", "N");

            if ( bolPROGBUYID== true)
                source.Add("bolPROGBUYID", "Y");
            else
                source.Add("bolPROGBUYID", "N");

            if ( bolPROGSALEID == true)
                source.Add("bolPROGSALEID", "Y");
            else
                source.Add("bolPROGSALEID", "N");

            if ( bolPROGSPEC == true)
                source.Add("bolPROGSPEC", "Y");
            else
                source.Add("bolPROGSPEC", "N");

            if (bolSHOOTSPEC  == true)
                source.Add("bolSHOOTSPEC", "Y");
            else
                source.Add("bolSHOOTSPEC", "N");

            if (bolPROGNATIONID == true)
                source.Add("bolPROGNATIONID", "Y");
            else
                source.Add("bolPROGNATIONID", "N");

            if (bolEXPIRE_DATE == true)
                source.Add("bolEXPIRE_DATE", "Y");
            else
                source.Add("bolEXPIRE_DATE", "N");

            if (bolEXPIRE_DATE_ACTION == true)
                source.Add("bolEXPIRE_DATE_ACTION", "Y");
            else
                source.Add("bolEXPIRE_DATE_ACTION", "N");

            if (bolPRDYEAR == true)
                source.Add("bolPRDYEAR", "Y");
            else
                source.Add("bolPRDYEAR", "N");

            source.Add("FSPROG_ID", obj.FSPROG_ID);
            source.Add("FNEPISODE", obj.FNEPISODE.ToString());
            source.Add("FNEPISODE_END", FNEPISODE_END.ToString());

            source.Add("FSPGDNAME", obj.FSPGDNAME);
            source.Add("FSPGDENAME", obj.FSPGDENAME);

            if (obj.FNLENGTH.ToString() == "0")
                source.Add("FNLENGTH", "");
            else
                source.Add("FNLENGTH", obj.FNLENGTH.ToString());

            source.Add("FSCONTENT", obj.FSCONTENT);
            source.Add("FSMEMO", obj.FSMEMO);
            source.Add("FSCR_TYPE", obj.FSCR_TYPE);
            source.Add("FSCR_NOTE", obj.FSCR_NOTE);
            source.Add("FSPROGGRADE", obj.FSPROGGRADE);
            source.Add("FSPROGSRCID", obj.FSPROGSRCID);
            source.Add("FSPROGATTRID", obj.FSPROGATTRID);
            source.Add("FSPROGAUDID", obj.FSPROGAUDID);
            source.Add("FSPROGTYPEID", obj.FSPROGTYPEID);
            source.Add("FSPROGGRADEID", obj.FSPROGGRADEID);
            source.Add("FSPROGLANGID1", obj.FSPROGLANGID1);
            source.Add("FSPROGLANGID2", obj.FSPROGLANGID2);
            source.Add("FSPROGSALEID", obj.FSPROGSALEID);
            source.Add("FSPROGBUYID", obj.FSPROGBUYID);
            source.Add("FSPROGBUYDID", obj.FSPROGBUYDID);      
            source.Add("FSSHOOTSPEC", obj.FSSHOOTSPEC);  
            source.Add("FSPROGSPEC", obj.FSPROGSPEC);
            source.Add("FSUPDUSER", obj.FSUPDUSER);
            source.Add("FSPROGNATIONID", obj.FSPROGNATIONID);
            source.Add("FDEXPIRE_DATE", obj.FDEXPIRE_DATE.ToString("yyyy-MM-dd HH:mm:ss"));
            source.Add("FSEXPIRE_DATE_ACTION", obj.FSEXPIRE_DATE_ACTION);
            source.Add("FSPRDYEAR", obj.FSPRDYEAR);


            //若有異動到期日期貨到期日動作寫入TBLOG_FILE_EXPIRE_DELETE資料庫
            if (obj.FDEXPIRE_DATE != obj.Origin_FDEXPIRE_DATE || obj.FSEXPIRE_DATE_ACTION != obj.Origin_FSEXPIRE_DATE_ACTION)
            {
                string myFSTABLE_NAME = "TBPROG_D：批次修改子集 FSID：" + obj.FSPROG_ID + " Episode：　from " + obj.FNEPISODE + " to " + FNEPISODE_END.ToString();
                string myFSRECORD = obj.Origin_FDEXPIRE_DATE.ToString() + " => " + obj.FDEXPIRE_DATE.ToString() + " || " + obj.Origin_FSEXPIRE_DATE_ACTION + " => " + obj.FSEXPIRE_DATE_ACTION;
                INSERT_TBLOG_FILE_EXPIRE_DELETE(myFSTABLE_NAME, myFSRECORD, obj.FSUPDUSER);
            }


            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBPROG_D_ALL", source, out strError, obj.FSUPDUSER, true);
            exceptionMsg = strError;

            if (bolDB == true)
                return true;
            else
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToMAM-UPDATE PROGD", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "整批修改子集，" + "ERROR:" + strError + "，參數：" + source.ToString());
                EMAIL_PROGD_Error(obj, "整批修改子集，" + strError, "MAM", "UPDATE");             //寄送Mail
                return false;
            }
        }

      
        /// <summary>
        /// 修改節目管理系統的節目子集檔_傳入節目子集的class
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="exceptionMsg"></param>
        /// <param name="FNEPISODE_END"></param>
        /// <param name="bolPGDNAME"></param>
        /// <param name="bolPGDENAME"></param>
        /// <param name="bolLENGTH"></param>
        /// <param name="bolPROGGRADEID"></param>
        /// <param name="bolPROGGRADE"></param>
        /// <param name="bolCONTENT"></param>
        /// <param name="bolcbMEMO"></param>
        /// <param name="bolCR_TYPE"></param>
        /// <param name="bolCR_NOTE"></param>
        /// <param name="bolPROGSRCID"></param>
        /// <param name="bolPROGATTRID"></param>
        /// <param name="bolPROGAUDID"></param>
        /// <param name="bolPROGTYPEID"></param>
        /// <param name="bolPROGLANGID1"></param>
        /// <param name="bolPROGLANGID2"></param>
        /// <param name="bolPROGBUYID"></param>
        /// <param name="bolPROGSALEID"></param>
        /// <param name="bolPROGSPEC"></param>
        /// <param name="bolSHOOTSPEC"></param>
        /// <param name="bolPROGNATIONID"></param>
        /// <returns></returns>
        [WebMethod]
        public Boolean UPDATE_TBPGMD_ALL(Class.Class_PROGD obj, out string exceptionMsg, short FNEPISODE_END, Boolean bolPGDNAME, Boolean bolPGDENAME, Boolean bolLENGTH, Boolean bolPROGGRADEID, Boolean bolPROGGRADE, Boolean bolCONTENT,
        Boolean bolcbMEMO, Boolean bolCR_TYPE, Boolean bolCR_NOTE, Boolean bolPROGSRCID, Boolean bolPROGATTRID, Boolean bolPROGAUDID, Boolean bolPROGTYPEID, Boolean bolPROGLANGID1, Boolean bolPROGLANGID2, Boolean bolPROGBUYID, Boolean bolPROGSALEID, Boolean bolPROGSPEC, Boolean bolSHOOTSPEC, Boolean bolPROGNATIONID, Boolean bolPRDYEAR)
        {
            Boolean bolDB = false;          //寫入節目管理系統資料庫結果
            string strError = "";           //錯誤訊息

            Dictionary<string, string> source = new Dictionary<string, string>();

            if (bolPGDNAME == true)
                source.Add("bolPGDNAME", "Y");
            else
                source.Add("bolPGDNAME", "N");

            if (bolPGDENAME == true)
                source.Add("bolPGDENAME", "Y");
            else
                source.Add("bolPGDENAME", "N");

            if (bolLENGTH == true)
                source.Add("bolLENGTH", "Y");
            else
                source.Add("bolLENGTH", "N");

            if (bolPROGGRADEID == true)
                source.Add("bolPROGGRADEID", "Y");
            else
                source.Add("bolPROGGRADEID", "N");

            if (bolPROGGRADE == true)
                source.Add("bolPROGGRADE", "Y");
            else
                source.Add("bolPROGGRADE", "N");

            if (bolCONTENT == true)
                source.Add("bolCONTENT", "Y");
            else
                source.Add("bolCONTENT", "N");

            if (bolcbMEMO == true)
                source.Add("bolcbMEMO", "Y");
            else
                source.Add("bolcbMEMO", "N");

            if (bolCR_TYPE == true)
                source.Add("bolCR_TYPE", "Y");
            else
                source.Add("bolCR_TYPE", "N");

            if (bolCR_NOTE == true)
                source.Add("bolCR_NOTE", "Y");
            else
                source.Add("bolCR_NOTE", "N");

            if (bolPROGSRCID == true)
                source.Add("bolPROGSRCID", "Y");
            else
                source.Add("bolPROGSRCID", "N");

            if (bolPROGATTRID == true)
                source.Add("bolPROGATTRID", "Y");
            else
                source.Add("bolPROGATTRID", "N");

            if (bolPROGAUDID == true)
                source.Add("bolPROGAUDID", "Y");
            else
                source.Add("bolPROGAUDID", "N");

            if (bolPROGTYPEID == true)
                source.Add("bolPROGTYPEID", "Y");
            else
                source.Add("bolPROGTYPEID", "N");

            if (bolPROGLANGID1 == true)
                source.Add("bolPROGLANGID1", "Y");
            else
                source.Add("bolPROGLANGID1", "N");

            if (bolPROGLANGID2 == true)
                source.Add("bolPROGLANGID2", "Y");
            else
                source.Add("bolPROGLANGID2", "N");

            if (bolPROGBUYID == true)
                source.Add("bolPROGBUYID", "Y");
            else
                source.Add("bolPROGBUYID", "N");

            if (bolPROGSALEID == true)
                source.Add("bolPROGSALEID", "Y");
            else
                source.Add("bolPROGSALEID", "N");

            if (bolPROGSPEC == true)
                source.Add("bolPROGSPEC", "Y");
            else
                source.Add("bolPROGSPEC", "N");

            if (bolSHOOTSPEC == true)
                source.Add("bolSHOOTSPEC", "Y");
            else
                source.Add("bolSHOOTSPEC", "N");

            if (bolPROGNATIONID == true)
                source.Add("bolPROGNATIONID", "Y");
            else
                source.Add("bolPROGNATIONID", "N");

            if (bolPRDYEAR == true)
                source.Add("bolPRDYEAR", "Y");
            else
                source.Add("bolPRDYEAR", "N");

            source.Add("FSPROGID", obj.FSPROG_ID);
            source.Add("FNEPISODE", obj.FNEPISODE.ToString());
            source.Add("FNEPISODE_END", FNEPISODE_END.ToString());

            source.Add("FSPGDNAME", obj.FSPGDNAME);
            source.Add("FSPGDENAME", obj.FSPGDENAME);

            if (obj.FNLENGTH.ToString() == "0")
                source.Add("FNLENGTH", "");
            else
                source.Add("FNLENGTH", obj.FNLENGTH.ToString());

            source.Add("FSPROGGRADE", obj.FSPROGGRADE);
            source.Add("FSCONTENT", obj.FSCONTENT);
            source.Add("FSMEMO", obj.FSMEMO);
            source.Add("FSPROGSRCID", obj.FSPROGSRCID);
            source.Add("FSPROGATTRID", obj.FSPROGATTRID);
            source.Add("FSPROGAUDID", obj.FSPROGAUDID);
            source.Add("FSPROGTYPEID", obj.FSPROGTYPEID);
            source.Add("FSPROGGRADEID", obj.FSPROGGRADEID);
            source.Add("FSPROGLANGID1", obj.FSPROGLANGID1);
            source.Add("FSPROGLANGID2", obj.FSPROGLANGID2);
            source.Add("FSPROGSALEID", obj.FSPROGSALEID);
            source.Add("FSPROGBUYID", obj.FSPROGBUYID);
            source.Add("FSPROGBUYDID", obj.FSPROGBUYDID);

            source.Add("FSSHOOTSPEC", obj.FSSHOOTSPEC);
            source.Add("FSPROGSPEC", obj.FSPROGSPEC);
            source.Add("FSPROGNATIONID", obj.FSPROGNATIONID);
            source.Add("FSPRDYEAR", obj.FSPRDYEAR);

            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction_With_DBLink("SP_U_TBPROG_D_EXTERNAL_ALL", source, out strError, obj.FSUPDUSER, true);
            exceptionMsg = strError;

            if (bolDB == true)
                return true;
            else
            {
                //寫入節目管理系統錯誤時，要因應的作法(目前只寫進LOG)
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToPGM-UPDATE PROGD", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "整批修改子集，" + "ERROR:" + strError + "，參數：" + source.ToString());
                EMAIL_PROGD_Error(obj, "整批修改子集，" + strError, "PGM", "UPDATE");             //寄送Mail
                return false;
            }          
        }

        #endregion

        #region 整批修改節目管理系統的節目子集資料、MAM的節目子集資料(只修改六大選項，從節目主檔來的)

        //整批修改節目管理系統的節目子集資料、MAM的節目子集資料(只修改六大選項，從節目主檔來的)
        [WebMethod]
        public ReturnMsg UPDATE_TBPROGD_ALL_NINE(Class.Class_PROGD obj, Boolean bolPROGSRCID, Boolean bolPROGATTRID, Boolean bolPROGAUDID, Boolean bolPROGTYPEID, Boolean bolPROGLANGID1, Boolean bolPROGLANGID2, Boolean bolPROGBUYID, Boolean bolPROGBUYDIDS, Boolean bolPROGSALEID, Boolean bolPROGNATIONID)
        {
            ReturnMsg objMsg = new ReturnMsg();

            if (UPDATE_TBPGMD_ALL_NINE(obj, out objMsg.strErrorMsg, bolPROGSRCID, bolPROGATTRID, bolPROGAUDID, bolPROGTYPEID, bolPROGLANGID1, bolPROGLANGID2, bolPROGBUYID, bolPROGBUYDIDS, bolPROGSALEID, bolPROGNATIONID) == true)             //修改節目管理系統的節目主檔
            {
                if (UPDATE_TBPROGD_MAIN_ALL_NINE(obj, out objMsg.strErrorMsg, bolPROGSRCID, bolPROGATTRID, bolPROGAUDID, bolPROGTYPEID, bolPROGLANGID1, bolPROGLANGID2, bolPROGBUYID, bolPROGBUYDIDS, bolPROGSALEID, bolPROGNATIONID) == true)   //修改MAM的節目主檔
                    objMsg.bolResult = true; 
                else
                    objMsg.bolResult = false;
            }
            else
                objMsg.bolResult = false;

            return objMsg;
        }

        //修改節目子集檔_傳入節目子集的class(只修改九大選項，從節目主檔來的)
        [WebMethod]
        public Boolean UPDATE_TBPROGD_MAIN_ALL_NINE(Class.Class_PROGD obj, out string exceptionMsg, Boolean bolPROGSRCID, Boolean bolPROGATTRID, Boolean bolPROGAUDID, Boolean bolPROGTYPEID, Boolean bolPROGLANGID1, Boolean bolPROGLANGID2, Boolean bolPROGBUYID, Boolean bolPROGBUYDIDS, Boolean bolPROGSALEID, Boolean bolPROGNATIONID)
        {
            Boolean bolDB = false;          //寫入節目管理系統資料庫結果
            string strError = "";           //錯誤訊息

            Dictionary<string, string> source = new Dictionary<string, string>();

            if (bolPROGSRCID == true)
                source.Add("bolPROGSRCID", "Y");
            else
                source.Add("bolPROGSRCID", "N");

            if (bolPROGATTRID == true)
                source.Add("bolPROGATTRID", "Y");
            else
                source.Add("bolPROGATTRID", "N");

            if (bolPROGAUDID == true)
                source.Add("bolPROGAUDID", "Y");
            else
                source.Add("bolPROGAUDID", "N");

            if (bolPROGTYPEID == true)
                source.Add("bolPROGTYPEID", "Y");
            else
                source.Add("bolPROGTYPEID", "N");

            if (bolPROGLANGID1 == true)
                source.Add("bolPROGLANGID1", "Y");
            else
                source.Add("bolPROGLANGID1", "N");

            if (bolPROGLANGID2 == true)
                source.Add("bolPROGLANGID2", "Y");
            else
                source.Add("bolPROGLANGID2", "N");

            if (bolPROGBUYID == true)
                source.Add("bolPROGBUYID", "Y");
            else
                source.Add("bolPROGBUYID", "N");

            if (bolPROGBUYDIDS == true)
                source.Add("bolPROGBUYDIDS", "Y");
            else
                source.Add("bolPROGBUYDIDS", "N");

            if (bolPROGSALEID == true)
                source.Add("bolPROGSALEID", "Y");
            else
                source.Add("bolPROGSALEID", "N");

            if (bolPROGNATIONID == true)
                source.Add("bolPROGNATIONID", "Y");
            else
                source.Add("bolPROGNATIONID", "N");

                source.Add("FSPROG_ID", obj.FSPROG_ID);
            source.Add("FSPROGSRCID", obj.FSPROGSRCID);
            source.Add("FSPROGATTRID", obj.FSPROGATTRID);
            source.Add("FSPROGAUDID", obj.FSPROGAUDID);
            source.Add("FSPROGTYPEID", obj.FSPROGTYPEID); 
            source.Add("FSPROGLANGID1", obj.FSPROGLANGID1);
            source.Add("FSPROGLANGID2", obj.FSPROGLANGID2);
            source.Add("FSPROGBUYID", obj.FSPROGBUYID); 
            source.Add("FSPROGBUYDID", obj.FSPROGBUYDID);
            source.Add("FSPROGSALEID", obj.FSPROGSALEID);
            source.Add("FSPROGNATIONID", obj.FSPROGNATIONID);

            source.Add("FSUPDUSER", obj.FSUPDUSER);    

            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBPROG_D_ALL_NINE", source, out strError, obj.FSUPDUSER, true);
            exceptionMsg = strError;

            if (bolDB == true)
                return true;
            else
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToMAM-UPDATE PROGD", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "整批修改子集九大選項，" + "ERROR:" + strError + "，參數：" + source.ToString());
                EMAIL_PROGD_Error(obj, "整批修改子集九大選項，" + strError, "MAM", "UPDATE");             //寄送Mail
                return false;
            }
        }

        //修改節目管理系統的節目子集檔_傳入節目子集的class(只修改九大選項，從節目主檔來的)
        [WebMethod]
        public Boolean UPDATE_TBPGMD_ALL_NINE(Class.Class_PROGD obj, out string exceptionMsg, Boolean bolPROGSRCID, Boolean bolPROGATTRID, Boolean bolPROGAUDID, Boolean bolPROGTYPEID, Boolean bolPROGLANGID1, Boolean bolPROGLANGID2, Boolean bolPROGBUYID, Boolean bolPROGBUYDIDS, Boolean bolPROGSALEID,Boolean bolPROGNATIONID)
        {
            Boolean bolDB = false;          //寫入節目管理系統資料庫結果
            string strError = "";           //錯誤訊息

            Dictionary<string, string> source = new Dictionary<string, string>();

            if (bolPROGSRCID == true)
                source.Add("bolPROGSRCID", "Y");
            else
                source.Add("bolPROGSRCID", "N");

            if (bolPROGATTRID == true)
                source.Add("bolPROGATTRID", "Y");
            else
                source.Add("bolPROGATTRID", "N");

            if (bolPROGAUDID == true)
                source.Add("bolPROGAUDID", "Y");
            else
                source.Add("bolPROGAUDID", "N");

            if (bolPROGTYPEID == true)
                source.Add("bolPROGTYPEID", "Y");
            else
                source.Add("bolPROGTYPEID", "N");

            if (bolPROGLANGID1 == true)
                source.Add("bolPROGLANGID1", "Y");
            else
                source.Add("bolPROGLANGID1", "N");

            if (bolPROGLANGID2 == true)
                source.Add("bolPROGLANGID2", "Y");
            else
                source.Add("bolPROGLANGID2", "N");

            if (bolPROGBUYID == true)
                source.Add("bolPROGBUYID", "Y");
            else
                source.Add("bolPROGBUYID", "N");

            if (bolPROGBUYDIDS == true)
                source.Add("bolPROGBUYDIDS", "Y");
            else
                source.Add("bolPROGBUYDIDS", "N");

            if (bolPROGSALEID == true)
                source.Add("bolPROGSALEID", "Y");
            else
                source.Add("bolPROGSALEID", "N");

            if (bolPROGNATIONID == true)
                source.Add("bolPROGNATIONID", "Y");
            else
                source.Add("bolPROGNATIONID", "N");

            source.Add("FSPROGID", obj.FSPROG_ID);       
            source.Add("FSPROGSRCID", obj.FSPROGSRCID);
            source.Add("FSPROGATTRID", obj.FSPROGATTRID);
            source.Add("FSPROGAUDID", obj.FSPROGAUDID);
            source.Add("FSPROGTYPEID", obj.FSPROGTYPEID);     
            source.Add("FSPROGLANGID1", obj.FSPROGLANGID1);
            source.Add("FSPROGLANGID2", obj.FSPROGLANGID2);     
            source.Add("FSPROGBUYID", obj.FSPROGBUYID); 
            source.Add("FSPROGBUYDID", obj.FSPROGBUYDID);
            source.Add("FSPROGSALEID", obj.FSPROGSALEID);
            source.Add("FSPROGNATIONID", obj.FSPROGNATIONID);

            
            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction_With_DBLink("SP_U_TBPROG_D_EXTERNAL_ALL_NINE", source, out strError, obj.FSUPDUSER, true);
            exceptionMsg = strError;

            if (bolDB == true)
                return true;
            else
            {
                //寫入節目管理系統錯誤時，要因應的作法(目前只寫進LOG)
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToPGM-UPDATE PROGD", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "整批修改子集九大選項，" + "ERROR:" + strError + "，參數：" + source.ToString());
                EMAIL_PROGD_Error(obj, "整批修改子集九大選項，" + strError, "PGM", "UPDATE");             //寄送Mail
                return false;
            }
        }

        #endregion

        //查詢view裡的實際帶長
        [WebMethod]
        public List<EpisodeLength> GET_EPISODE_LENGTH(Class.Class_PROGD obj)
        {
            List<EpisodeLength> ListMsg = new List<EpisodeLength>();

            List<Dictionary<string, string>> sqlResult;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPROGID", obj.FSPROG_ID);
            sqlParameters.Add("FNEPISODE", obj.FNEPISODE.ToString());

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_D_LENGTH", sqlParameters, out sqlResult))
                return new List<EpisodeLength>();

            if (sqlResult.Count > 0)
            {
                for (int i = 0; i < sqlResult.Count; i++)
                {
                    int intCheck;  
                    EpisodeLength objLength = new EpisodeLength();
                    objLength.FSTYPE_NAME = sqlResult[i]["FSTYPE_NAME"].Trim();

                    if (int.TryParse(sqlResult[i]["SHOW_SECONDS"].Trim(), out intCheck) == true)
                    {
                        objLength.intShow_Seconds = intCheck;
                        objLength.FSShow = intCheck/60 + "分" + intCheck%60 + "秒";
                    }
                    else
                    {
                        objLength.intShow_Seconds = 0 ;
                        objLength.FSShow = "";
                    }

                    ListMsg.Add(objLength);
                }
            }

            return ListMsg;
        }
    }
}
