﻿using System;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;
using System.IO;
using System.Reflection;


namespace PTS_MAM3.Web.DataSource
{
    public class TBGroupStruct
    {

        public string FSGROUP_ID { get; set; }

        public string FSGROUP { get; set; }

        public string FSDESCRIPTION { get; set; }

        public string FSCREATED_BY { get; set; }

        public DateTime FDCREATED_DATE { get; set; }

        public string FSUPDATED_BY { get; set; }

        public DateTime FDUPDATED_DATE { get; set; }

        public string oFSGROUP_ID { get; set; }
    }


    public class TBUser_GroupStruct
    {

        public string FSUSER_ID { get; set; }

        public string FSGROUP_ID { get; set; }

        public string FSCREATED_BY { get; set; }

        public DateTime FDCREATED_DATE { get; set; }

        public string FSUPDATED_BY { get; set; }

        public DateTime FDUPDATED_DATE { get; set; }

    }


    /// <summary>
    /// WSGroupAdmin 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSGroupAdmin : System.Web.Services.WebService
    {
        [WebMethod]
        public Boolean fnUpdateTBMODULE_GROUP(ObservableCollection<Class.Class_Module_Detail> mod_detail_list, string fsgroup_id, string fsuser_id)
        {
            Boolean brtn = false;



            foreach (Class.Class_Module_Detail obj in mod_detail_list)
            {

                Dictionary<string, string> source = new Dictionary<string, string>();
                source.Add("FSMODULE_ID", obj.FSMODULE_ID);
                source.Add("FSGROUP_ID", fsgroup_id);
                source.Add("FCHasPermission", obj.FCHasPermission);
                brtn = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBMODULE_GROUP", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source), fsuser_id);

            }


            return brtn;
        }

        [WebMethod]
        public ObservableCollection<Class.Class_Module_Detail> fnGetTBMODULE_DETAIL(string FSMOUDULE_CAT_ID, string FSGROUP_ID)
        {
            string rtn = "";
            Dictionary<string, string> source = new Dictionary<string, string>();

            source.Add("FSMOUDULE_CAT_ID", FSMOUDULE_CAT_ID);

            rtn = MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBMODULE_DETAIL", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source));

            ObservableCollection<Class.Class_Module_Detail> ModuleDetailList = new ObservableCollection<Class.Class_Module_Detail>();

            if (!string.IsNullOrEmpty(rtn))
            {
                byte[] byteArray = Encoding.Unicode.GetBytes(rtn);
                XDocument doc = XDocument.Load(new MemoryStream(byteArray));

                var ltox = from s in doc.Elements("Datas").Elements("Data")
                           select new Class.Class_Module_Detail
                           {
                               FSDESCRIPTION = (string)s.Element("FSDESCRIPTION"),
                               FSFUNC_NAME = (string)s.Element("FSFUNC_NAME"),
                               FSHANDLER = (string)s.Element("FSHANDLER"),
                               FSMODULE_ID = (string)s.Element("FSMODULE_ID")
                           };

                WSMAMFunctions MAMFunObj = new WSMAMFunctions();
                foreach (Class.Class_Module_Detail obj in ltox)
                {

                    obj.FCHasPermission = MAMFunObj.fnGetValue("SP_Q_TBMODULE_GROUP_Permission '" + obj.FSMODULE_ID + "', '" + FSGROUP_ID + "' ").FirstOrDefault();
                    ModuleDetailList.Add(obj);

                }

            }

            return ModuleDetailList;

        }


        [WebMethod]
        public ObservableCollection<Class.Class_MODULE_CAT> fnGetTBMODULE_CAT(string FSMOUDULE_CAT_ID)
        {
            string rtn = "";
            Dictionary<string, string> source = new Dictionary<string, string>();

            source.Add("FSMOUDULE_CAT_ID", FSMOUDULE_CAT_ID);

            rtn = MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBMODULE_CAT", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source));

            ObservableCollection<Class.Class_MODULE_CAT> ModuleCatList = new ObservableCollection<Class.Class_MODULE_CAT>();

            if (!string.IsNullOrEmpty(rtn))
            {
                byte[] byteArray = Encoding.Unicode.GetBytes(rtn);
                XDocument doc = XDocument.Load(new MemoryStream(byteArray));

                var ltox = from s in doc.Elements("Datas").Elements("Data")
                           select new Class.Class_MODULE_CAT
                           {
                               FSMOUDULE_CAT_ID = (string)s.Element("FSMOUDULE_CAT_ID"),
                               FSMODULE_CAT_NAME = (string)s.Element("FSMODULE_CAT_NAME")
                           };

                foreach (Class.Class_MODULE_CAT obj in ltox)
                {
                    ModuleCatList.Add(obj);
                }

            }


            return ModuleCatList;
        }


        [WebMethod]
        public Boolean fnDeleteTBGROUPS(TBGroupStruct t, string fsuser_id)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSGROUP_ID", t.FSGROUP_ID);
            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_TBGROUPS", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source), fsuser_id);
        }


        [WebMethod]
        public Boolean fnInsertTBGROUPS(TBGroupStruct t, string fsuser_id)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSGROUP_ID", t.FSGROUP_ID);
            source.Add("FSGROUP", t.FSGROUP);
            source.Add("FSDESCRIPTION", t.FSDESCRIPTION);

            source.Add("FSCREATED_BY", t.FSCREATED_BY);
            source.Add("FSUPDATED_BY", t.FSUPDATED_BY);


            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBGROUPS", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source), fsuser_id);
        }


        [WebMethod]
        public Boolean fnInsertTBUSER_GROUP(TBUser_GroupStruct t, string fsuser_id)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSUSER_ID", t.FSUSER_ID);
            source.Add("FSGROUP_ID", t.FSGROUP_ID);
            source.Add("FSCREATED_BY", t.FSCREATED_BY);
            source.Add("FSUPDATED_BY", t.FSUPDATED_BY);


            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBUSER_GROUP", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source), fsuser_id);
        }

        [WebMethod]
        public Boolean fnInsertTBUSER_GROUP_Batch(List<TBUser_GroupStruct> user_groups, string fsuser_id)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();

            foreach (TBUser_GroupStruct user_group in user_groups)
            {
                source.Clear();
                source.Add("FSUSER_ID", user_group.FSUSER_ID);
                source.Add("FSGROUP_ID", user_group.FSGROUP_ID);
                source.Add("FSCREATED_BY", user_group.FSCREATED_BY);
                source.Add("FSUPDATED_BY", user_group.FSUPDATED_BY);
               if(!MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBUSER_GROUP", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source), fsuser_id))
               {
               return false;
               }
            }

            return true;
        }

        [WebMethod]
        public Boolean fnDeleteTBUSER_GROUP(TBUser_GroupStruct t, string fsuser_id)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSUSER_ID", t.FSUSER_ID);
            source.Add("FSGROUP_ID", t.FSGROUP_ID);

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_TBUSER_GROUP", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source), fsuser_id);
        }

        [WebMethod]
        public Boolean fnDeleteTBUSER_GROUP_Batch(List<TBUser_GroupStruct> user_groups, string fsuser_id)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            foreach (TBUser_GroupStruct user_group in user_groups)
            {
                source.Clear();

                source.Add("FSUSER_ID", user_group.FSUSER_ID);
                source.Add("FSGROUP_ID", user_group.FSGROUP_ID);

                if (!MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_TBUSER_GROUP", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source), fsuser_id)) 
                {
                    return false;
                }
            }
            return true ;
        }


        [WebMethod]
        public string fnGetTBGROUPS(TBGroupStruct t)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSGROUP_ID", t.FSGROUP_ID);
            return MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBGROUPS", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source));
        }

        [WebMethod]
        public List<TBGroupStruct> fnGetTBGROUPS_By_USERID(string user_id)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSUSER_ID", user_id);
            List<Dictionary<string, string>> result;
            List<TBGroupStruct> returnResult = new List<TBGroupStruct>();
            if (MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBGROUPS_BY_USERID", source, out result))
            {

                for (int i = 0; i < result.Count; i++)
                {
                    TBGroupStruct groupStruct = new TBGroupStruct();
                    PropertyInfo[] properties = groupStruct.GetType().GetProperties();
                    foreach (PropertyInfo pi in properties)
                    {
                        try
                        {
                            if (result[i].Keys.Contains(pi.Name))
                            {
                                if (pi.PropertyType == typeof(String))
                                    pi.SetValue(groupStruct, result[i][pi.Name], null);
                                //else if (pi.PropertyType == typeof(DateTime))
                                //    pi.SetValue(groupStruct,Convert.ToDateTime(result[i][pi.Name]), null);

                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    }

                    returnResult.Add(groupStruct);
                }

            }

            return returnResult;
        }

        [WebMethod]
        public Boolean fnUpdateTBGROUPS(TBGroupStruct t, string fsuser_id)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();

            source.Add("FSGROUP_ID", t.FSGROUP_ID.ToString());
            source.Add("FSGROUP", t.FSGROUP);
            source.Add("FSDESCRIPTION", t.FSDESCRIPTION);
            source.Add("FSUPDATED_BY", t.FSUPDATED_BY);
            source.Add("oFSGROUP_ID", t.oFSGROUP_ID);

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBGROUPS", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source), fsuser_id);
        }
    }
}
