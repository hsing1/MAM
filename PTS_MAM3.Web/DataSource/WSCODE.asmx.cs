﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Xml;
using System.Text;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSCODE 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSCODE : System.Web.Services.WebService
    {

         //查詢製作目的代碼檔全部資料
        [WebMethod()]
        public string fnGetTBZPROGOBJ_ALL()
        {
            return MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBZPROGOBJ_ALL", "");
        }

        //查詢製作目的代碼檔BYName
        [WebMethod()]
        public string fnGetTBZPROGOBJ_BYNAME(string NAME)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROGOBJNAME", NAME);
            return MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBZPROGOBJ_BYNAME", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source));
        }

        //查詢製作目的代碼檔BYID
        [WebMethod()]
        public string fnGetTBZPROGOBJ_BYID(string ID)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROGOBJID", ID);
            return MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBZPROGBUYD_BYID", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source));
        }

    }
}
