﻿<%@ WebHandler Language="C#" Class="TakeUploadString" %>

using System;
using System.Web;
using System.IO;
using System.Data.OleDb;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;


public class TakeUploadString : IHttpHandler
{

    public bool ProcessRequest(HttpContext context)
    {
        using (StreamReader sr = new StreamReader(context.Request.InputStream))
        {
            byte[] myFile = Convert.FromBase64String(sr.ReadToEnd());
            using (FileStream fs = new FileStream(HttpContext.Current.Server.MapPath("~/UploadFolder/" + context.Request.QueryString["FileName"].ToString()), FileMode.Create))
            {

                fs.Write(myFile, 0, myFile.Length);
                fs.Flush();
                fs.Close();
            }
        }

        string path = HttpContext.Current.Server.MapPath("~/UploadFolder/" + context.Request.QueryString["FileName"].ToString());
        Boolean aaa = false;

        DataTable MyDt = new DataTable();
        MyDt = ImportExcel(path, aaa);

        String sqlConnStr = "";
        sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
        SqlConnection connection = new SqlConnection(sqlConnStr);
        SqlTransaction tran = null;
        SqlCommand sqlcmd = new SqlCommand("", connection);

        sqlcmd.CommandType = CommandType.StoredProcedure;

        try
        {
            if (connection.State == System.Data.ConnectionState.Closed)
            {
                connection.Open();
            }

            tran = connection.BeginTransaction();
            sqlcmd.Transaction = tran;

            //參數剖析


            //sqlcmd.CommandText = "SP_D_TBPGM_LOUTH_KEY";
            //sqlcmd.Parameters.Clear();
            //if (sqlcmd.ExecuteNonQuery() == 0)
            //{
            //    //tran.Rollback();
            //    //return false;
            //}
            int i = 0;
            for (i = 1; i < MyDt.Rows.Count; i++)
            {
                DataRow TempRow = MyDt.Rows[i];
                sqlcmd.Parameters.Clear();
                sqlcmd.CommandText = "SP_I_TBPGM_LOUTH_KEY";
                SqlParameter para = new SqlParameter("@FSGROUP", TempRow[0].ToString());
                sqlcmd.Parameters.Add(para);
                SqlParameter para1 = new SqlParameter("@FSNAME", TempRow[1].ToString());
                sqlcmd.Parameters.Add(para1);
                SqlParameter para2 = new SqlParameter("@FSNO", TempRow[2].ToString());
                sqlcmd.Parameters.Add(para2);
                SqlParameter para3 = new SqlParameter("@FSMEMO", TempRow[3].ToString());
                sqlcmd.Parameters.Add(para3);
                SqlParameter para4 = new SqlParameter("@FSLAYEL", TempRow[4].ToString());
                sqlcmd.Parameters.Add(para4);
                SqlParameter para5 = new SqlParameter("@FSRULE", TempRow[5].ToString());
                sqlcmd.Parameters.Add(para5);
                if (sqlcmd.ExecuteNonQuery() == 0)
                {
                    tran.Rollback();
                    return false;
                }
                //    sb.AppendLine("<FSGROUP>" + TempRow[0].ToString() + "</FSGROUP>");
                //    sb.AppendLine("<FSNAME>" + TempRow[1].ToString() + "</FSNAME>");
                //    sb.AppendLine("<FSNO>" + TempRow[2].ToString() + "</FSNO>");
                //    sb.AppendLine("<FSMEMO>" + TempRow[3].ToString() + "</FSMEMO>");
                //    sb.AppendLine("<FSLAYEL>" + TempRow[4].ToString() + "</FSLAYEL>");
                //    sb.AppendLine("<FSRULE>" + TempRow[5].ToString() + "</FSRULE>");
            }
            tran.Commit();
            return true;
        }
        catch
        {
            tran.Rollback();
            return false;
        }
        finally
        {
            connection.Close();
            connection.Dispose();

            sqlcmd.Dispose();
            GC.Collect();

        }

        //PTS_MAM3.Web.DataSource..Do_Insert_TBPGM_LOUTH_KEY
        //PTS_MAM3.Web.DataSource.
        //WSPGMSendSQL.SendSQLSoapClient SP_I_TBPGM_LOUTH_KEY = new PTS_MAM3.Web.SendSQL1.SendSQLSoapClient();

        //PTS_MAM3.Web.SendSQL1.SendSQLSoapClient SP_I_TBPGM_LOUTH_KEY = new PTS_MAM3.Web.SendSQL1.SendSQLSoapClient();
        //StringBuilder sb = new StringBuilder();
        //object ReturnXML = "";
        //sb.AppendLine("<Datas>");
        //int i=0;
        //for(i=1;i< MyDt.Rows.Count;i++)
        //{
        //    DataRow TempRow = MyDt.Rows[i];

        //    sb.AppendLine("<Data>");
        //    sb.AppendLine("<FSGROUP>" + TempRow[0].ToString() + "</FSGROUP>");
        //    sb.AppendLine("<FSNAME>" + TempRow[1].ToString() + "</FSNAME>");
        //    sb.AppendLine("<FSNO>" + TempRow[2].ToString() + "</FSNO>");
        //    sb.AppendLine("<FSMEMO>" + TempRow[3].ToString() + "</FSMEMO>");
        //    sb.AppendLine("<FSLAYEL>" + TempRow[4].ToString() + "</FSLAYEL>");
        //    sb.AppendLine("<FSRULE>" + TempRow[5].ToString() + "</FSRULE>");
        //    sb.AppendLine("</Data>");
        //}
        //sb.AppendLine("</Datas>");
        ////sb=sb.Replace("\r\n", "");
        //SP_I_TBPGM_LOUTH_KEY.Do_Insert_TBPGM_LOUTH_KEY("SP_I_TBPGM_LOUTH_KEY", sb.ToString());
    }



    public bool IsReusable
    {
        get
        {
            return false;
        }
    }


    public static System.Data.DataTable ImportExcel(string Path, bool HasFieldName)
    {
        try
        {
            System.Collections.Generic.List<string> sTBList = new System.Collections.Generic.List<string>();
            string strConn;
            /*  strConn = "Provider=Microsoft.Jet.OLEDB.4.0;" +
              "Data Source=" + FName +
              ";Extended Properties=Excel 8.0;";*/
            if (HasFieldName)
                /*如果Excel中的第一列為欄名,則寫成*/
                strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Path + ";Extended Properties=\"Excel 8.0;HDR=YES;IMEX=1;\"";
            else
                strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Path + ";Extended Properties=\"Excel 8.0;HDR=NO;IMEX=1;\"";
            OleDbConnection odc = new OleDbConnection(strConn);
            odc.Open();
            DataTable dt = odc.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    sTBList.Add(dr["TABLE_NAME"].ToString());
                }
            }
            OleDbDataAdapter myCommand = new OleDbDataAdapter("SELECT * FROM [" + sTBList[0] + "]", strConn);
            //OleDbDataAdapter myCommand = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", strConn);
            System.Data.DataTable myDataSet = new System.Data.DataTable();
            myCommand.Fill(myDataSet);
            odc.Close();
            return myDataSet;
        }
        catch (Exception e)
        {
            return null;
        }
    }
}