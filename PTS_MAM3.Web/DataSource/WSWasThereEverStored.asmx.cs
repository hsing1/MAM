﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using PTS_MAM3.Web.Class;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSWasThereEverStored 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSWasThereEverStored : System.Web.Services.WebService
    {

        [WebMethod]
        public List<Class_ARCHIVE> CheckStored_G()
        {
            List<Class_ARCHIVE> returnList=new List<Class_ARCHIVE>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSTYPE", "G");
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBARCHIVE_WasStored", sqlParameters, out sqlResult))
                return new List<Class_ARCHIVE>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ARCHIVE obj = new Class_ARCHIVE();
                

                obj.FSTYPE = sqlResult[i]["FSTYPE"];
                obj.FSID = sqlResult[i]["FSID"];
                obj.FNEPISODE_NAME = sqlResult[i]["FNEPISODE"];

                returnList.Add(obj);
            }
                return returnList;//Transfer_PROGM(sqlResult); 
        }

        [WebMethod]
        public List<Class_ARCHIVE> CheckStored_P()
        {
            List<Class_ARCHIVE> returnList = new List<Class_ARCHIVE>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSTYPE", "P");
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBARCHIVE_WasStored", sqlParameters, out sqlResult))
                return new List<Class_ARCHIVE>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ARCHIVE obj = new Class_ARCHIVE();


                obj.FSTYPE = sqlResult[i]["FSTYPE"];
                obj.FSID = sqlResult[i]["FSID"];
                obj.FNEPISODE_NAME = sqlResult[i]["FNEPISODE"];

                returnList.Add(obj);
            }
            return returnList;//Transfer_PROGM(sqlResult); 
        }
    }
}
