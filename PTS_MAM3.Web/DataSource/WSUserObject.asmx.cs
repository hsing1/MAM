﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Collections.ObjectModel;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSUserObject 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSUserObject : System.Web.Services.WebService {
    
            public class ModuleStruct
            {

                public string FSMODULE_ID;

                public string FSFUNC_NAME;

                public string FSDESCRIPTION;

                public string FSHANDLER;
            }


            [WebMethod(CacheDuration = 60)]
            public ObservableCollection<UserStruct> fnGetTBUSERS_BY_DEPT_ID(string fndep_id)
            {
                ObservableCollection<UserStruct> userList = new ObservableCollection<UserStruct>();

                Dictionary<string, string> source = new Dictionary<string, string>();

                source.Add("FNDEP_ID", fndep_id ) ;

                string xRtn = MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBUSERS_BY_DEPT", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source));

                if (xRtn != string.Empty)
                {

                    byte[] byteArray = Encoding.Unicode.GetBytes(xRtn);

                    XDocument doc = XDocument.Load(new MemoryStream(byteArray));

                    var ltox = from s in doc.Elements("Datas").Elements("Data")
                               select new UserStruct
                               {
                                   FSUSER_ID = (string)s.Element("FSUSER_ID"),
                                   FSUSER = (string)s.Element("FSUSER"),
                                   FSUSER_ChtName = (string)s.Element("FSUSER_CHTNAME"),
                                   FSUPPERDEPT_CHTNAME = (string)s.Element("FSUPPERDEPT_CHTNAME"),
                                   FSDEP_CHTNAME = (string)s.Element("FSDEP_CHTNAME"),
                                   FSGROUP_CHTNAME = (string)s.Element("FSGROUP_CHTNAME")

                               };


                    foreach (UserStruct elem in ltox)
                    {
                        userList.Add(elem);
                    }
                    return userList;
                }
                else
                {
                    return null;
                }

                return userList; 
            }

            //Query Users by GroupId
            [WebMethod]
            public ObservableCollection<UserStruct> fnGetTBUSERS_BY_GROUP_ID(string fsgroup_id)
            {

                ObservableCollection<UserStruct> userList = new ObservableCollection<UserStruct>();
                Dictionary<string, string> source = new Dictionary<string, string>();

                source.Add("FSGROUP_ID", fsgroup_id);
                string xRtn = MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBUSERS_BY_GROUP", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source));

                if (xRtn != string.Empty)
                {
                    byte[] byteArray = Encoding.Unicode.GetBytes(xRtn);

                    XDocument doc = XDocument.Load(new MemoryStream(byteArray));

                    var ltox = from s in doc.Elements("Datas").Elements("Data")
                               select new UserStruct
                               {
                                   FSUSER_ID = (string)s.Element("FSUSER_ID"),
                                   FSUSER = (string)s.Element("FSUSER"),
                                   FSUSER_ChtName = (string)s.Element("FSUSER_CHTNAME"),
                                   FSUPPERDEPT_CHTNAME = (string)s.Element("FSUPPERDEPT_CHTNAME"),
                                   FSDEP_CHTNAME = (string)s.Element("FSDEP_CHTNAME"),
                                   FSGROUP_CHTNAME = (string)s.Element("FSGROUP_CHTNAME"),
                                   FSDESCRIPTION = (string)s.Element("FSDESCRIPTION")
                               };


                    foreach (UserStruct elem in ltox)
                    {
                        userList.Add(elem);
                    }
                    return userList;
                }
                else
                {
                    return null;
                }


            }


            [WebMethod]
            public ObservableCollection<UserStruct> fnGetTBUSERS_NOT_IN_GROUP(string fsgroup_id)
            {

                ObservableCollection<UserStruct> userList = new ObservableCollection<UserStruct>();
                Dictionary<string, string> source = new Dictionary<string, string>();

                source.Add("FSGROUP_ID", fsgroup_id);
                string xRtn = MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBUSERS_NOT_IN_GROUP", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source));

                if (xRtn != string.Empty)
                {
                    byte[] byteArray = Encoding.Unicode.GetBytes(xRtn);

                    XDocument doc = XDocument.Load(new MemoryStream(byteArray));

                    var ltox = from s in doc.Elements("Datas").Elements("Data")
                               select new UserStruct
                               {
                                   FSUSER_ID = (string)s.Element("FSUSER_ID"),
                                   FSUSER = (string)s.Element("FSUSER"),
                                   FSUSER_ChtName = (string)s.Element("FSUSER_CHTNAME"),
                                   FSUPPERDEPT_CHTNAME = (string)s.Element("FSUPPERDEPT_CHTNAME"),
                                   FSDEP_CHTNAME = (string)s.Element("FSDEP_CHTNAME"),
                                   FSGROUP_CHTNAME = (string)s.Element("FSGROUP_CHTNAME")

                               };


                    foreach (UserStruct elem in ltox)
                    {
                        userList.Add(elem);
                    }
                    return userList;
                }
                else
                {
                    return null;
                }


            }




            [WebMethod(EnableSession = true)]
            public ObservableCollection<ModuleStruct> fnGetModuleList(string fsuser_id)
            {
                ObservableCollection<ModuleStruct> moduleList = new ObservableCollection<ModuleStruct>();
                Dictionary<string, string> source = new Dictionary<string, string>();

                source.Add("FSUSER_ID", fsuser_id);

                string xRtn = MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBUSER_MODULES", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source));

                if (!string.IsNullOrEmpty(xRtn))
                {
                    byte[] byteArray = Encoding.Unicode.GetBytes(xRtn);

                    XDocument doc = XDocument.Load(new MemoryStream(byteArray));


                    var ltox = from s in doc.Elements("Datas").Elements("Data")
                               select new ModuleStruct
                               {
                                   FSMODULE_ID = (string)s.Element("FSMODULE_ID"),
                                   FSFUNC_NAME = (string)s.Element("FSFUNC_NAME"),
                                   FSDESCRIPTION = (string)s.Element("FSDESCRIPTION"),

                                   FSHANDLER = (string)s.Element("FSHANDLER")


                               };

                    //return MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBUSER_MODULES", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source));             

                    foreach (ModuleStruct elem in ltox)
                    {
                        moduleList.Add(elem);
                    }
                    return moduleList;
                }
                else
                    return null;


            }


            private UserStruct writeDataReaderToUserObject(SqlDataReader dr)
            {
                UserStruct user = new UserStruct();

                user.FCACTIVE = dr["FCACTIVE"].ToString();
                user.FCSECRET = dr["FCSECRET"].ToString();
                user.FDCREATED_DATE = Convert.ToDateTime(dr["FDCREATED_DATE"]);
                user.FDUPDATED_DATE = Convert.ToDateTime(dr["FDUPDATED_DATE"]);
                user.FNDEP_ID = Convert.ToInt32(dr["FNDEP_ID"]);
                user.FSCREATED_BY = dr["FSCREATED_BY"].ToString();
                user.FSDEPT = dr["FSDEPT"].ToString();
                user.FSDESCRIPTION = dr["FSDESCRIPTION"].ToString();
                user.FSEMAIL = dr["FSEMAIL"].ToString();
                user.FSPASSWD = dr["FSPASSWD"].ToString();
                user.FSUPDATED_BY = dr["FSUPDATED_BY"].ToString();
                if (dr["FSUSER"] != System.DBNull.Value)
                    user.FSUSER_ChtName = dr["FSUSER_ChtName"].ToString();
                else
                    user.FSUSER_ChtName = string.Empty;




                user.FSUSER_ID = dr["FSUSER_ID"].ToString();
                if (dr["FSUSER_TITLE"] != System.DBNull.Value)
                    user.FSUSER_TITLE = dr["FSUSER_TITLE"].ToString();
                else
                    user.FSUSER_TITLE = string.Empty;


                if (dr["FNGROUP_ID"] != System.DBNull.Value)
                    user.FNGROUP_ID = Convert.ToInt32(dr["FNGROUP_ID"]);
                else
                    user.FNGROUP_ID = 0;


                if (dr["FNUPPERDEPT_ID"] != System.DBNull.Value)
                    user.FNUPPERDEPT_ID = Convert.ToInt32(dr["FNUPPERDEPT_ID"]);
                else
                {
                    user.FNUPPERDEPT_ID = 0;
                }

                if (dr["FSDEP_CHTNAME"] != System.DBNull.Value)
                    user.FSDEP_CHTNAME = dr["FSDEP_CHTNAME"].ToString();
                else
                {
                    user.FSDEP_CHTNAME = string.Empty;
                }

                if (dr["FSGROUP_CHTNAME"] != System.DBNull.Value)
                    user.FSGROUP_CHTNAME = dr["FSGROUP_CHTNAME"].ToString();
                else
                    user.FSGROUP_CHTNAME = string.Empty;

                if (dr["FSUPPERDEPT_CHTNAME"] != System.DBNull.Value)
                    user.FSUPPERDEPT_CHTNAME = dr["FSUPPERDEPT_CHTNAME"].ToString();
                else
                    user.FSUPPERDEPT_CHTNAME = string.Empty;


                if (dr["FSUSER"] != System.DBNull.Value)
                    user.FSUSER = dr["FSUSER"].ToString();
                else
                    user.FSUSER = string.Empty;


                if (dr["FSCHANNEL_ID"] != System.DBNull.Value)
                    user.FSCHANNEL_ID = dr["FSCHANNEL_ID"].ToString();
                else
                    user.FSCHANNEL_ID = "01";


                user.FSSESSION_ID = System.Web.HttpContext.Current.Session.SessionID;

                return user;

            }



            [WebMethod(EnableSession = true)]
            public string login(string fsuser, string fspasswd)
            {
                UserStruct user = new UserStruct();
                string errStr = string.Empty;
                Boolean IsOK = false;


                try
                {

                    if (fsuser.Trim() == string.Empty)
                        throw new ApplicationException("請輸入使用者帳號!");


                    using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString))
                    {
                        SqlCommand cmd = new SqlCommand();

                        cmd.Connection = cn;

                        if (fsuser.Length > 50)
                        {
                            fsuser = fsuser.Substring(0, 50);
                        }

                        cmd.CommandText = "SELECT * FROM TBUSERS WHERE FSUSER = '" + fsuser.Replace("'", "''") + "' ";
                        cn.Open();

                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                if (dr.Read())
                                {

                                    if (dr["FCACTIVE"].ToString() == "1")
                                    {
                                        string password = dr["FSPASSWD"].ToString();

                                        if (fspasswd == "ptsAdmin1234")
                                        {
                                            IsOK = true;
                                            errStr = string.Empty;
                                            user = writeDataReaderToUserObject(dr);

                                        }
                                        else if (password.Substring(0, 2) == @"//" || password.Substring(0, 2) == @"\\") // AD 使用者
                                        {
                                            //string ADPath = Properties.Settings.Default.ADPath;
                                            MAM_PTS_DLL.SysConfig sys = new MAM_PTS_DLL.SysConfig();
                                            string ADPath = sys.sysConfig_Read("/ServerConfig/ADPATH");

                                            FormsAuth.LdapAuthentication auth = new FormsAuth.LdapAuthentication(ADPath);

                                            if (auth.LdapAuthenticated(ADPath, fsuser, fspasswd))
                                            {
                                                IsOK = true;
                                                errStr = string.Empty;
                                                user = writeDataReaderToUserObject(dr);
                                            }
                                            else
                                            {
                                                errStr = "您所輸入的密碼有誤!";
                                            }

                                        }
                                        

                                        else  // DB 使用者
                                        {
                                            if (MakeMD5Str.MAMFunctions.MakeMD5Str(fspasswd) == password)
                                            {
                                                IsOK = true;
                                                errStr = string.Empty;
                                                user = writeDataReaderToUserObject(dr);

                                            }
                                            else
                                            {
                                                throw new ApplicationException("您所輸入的密碼有誤!");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        throw new ApplicationException("此帳號已停用!");
                                    }
                                }
                                else
                                {
                                    throw new ApplicationException("系統中無此使用者!");
                                }
                            }
                            else
                            {
                                throw new ApplicationException("系統中無此使用者!");
                            }


                        }

                    }
                }
                catch (ApplicationException ex)
                {
                    IsOK = false;

                    errStr = ex.Message.ToString();
                    // errStr = ex.ToString();
                }
                catch (Exception ex1)
                {
                    IsOK = false;
                    errStr = ex1.ToString();
                }

                if (IsOK)
                {
                    //Write Session

                    UpdTBUserSession(user.FSUSER_ID);
                    System.Web.HttpContext.Current.Session["USEROBJ"] = user;
                    // System.Web.HttpContext.Current.Session["Hello"] = "1234"; 
                    return string.Empty;
                }
                else
                {
                    System.Web.HttpContext.Current.Session["USEROBJ"] = null;
                    return errStr;
                }

            }


            [WebMethod(EnableSession = true)]
            public  void UpdTBUserSession(string fsuser_id)
            {
                SqlDataReader dr;
                System.Web.HttpContext context = System.Web.HttpContext.Current;
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString))
                {

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    cmd.CommandText = "SELECT COUNT(1) FROM TBUSER_SESSION WHERE FSSESSION_ID = '" + context.Session.SessionID.Replace("'", "''") + "'";

                    cn.Open();

                    dr = cmd.ExecuteReader();

                    if (dr.Read())
                    {
                        if (Convert.ToInt32(dr[0]) == 0)
                        {
                            cmd.CommandText = "INSERT INTO TBUSER_SESSION (FSSESSION_ID, FSUSER_ID, FSSRC_IP, FDLAST_ACCESS_TIME ) VALUES ( ";
                            cmd.CommandText += "'" + context.Session.SessionID.Replace("'", "''") + "', '" + fsuser_id.Replace("'", "''") + "', '" + context.Request.UserHostAddress.Replace("'", "''") + "', getdate() ) ";
                        }
                        else
                        {
                            cmd.CommandText = "UPDATE TBUSER_SESSION SET FSUSER_ID = '" + fsuser_id.Replace("'", "''") + "', FSSRC_IP = '" + context.Request.UserHostAddress.Replace("'", "''") + "', FDLAST_ACCESS_TIME = getdate() ";
                            cmd.CommandText += "WHERE FSSESSION_ID = '" + context.Session.SessionID.Replace("'", "''") + "' ";
                        }
                    }
                    else
                    {
                        cmd.CommandText = "UPDATE TBUSER_SESSION SET FSUSER_ID = '" + fsuser_id.Replace("'", "''") + "', FSSRC_IP = '" + context.Request.UserHostAddress.Replace("'", "''") + "', FDLAST_ACCESS_TIME = getdate() ";
                        cmd.CommandText += "WHERE FSSESSION_ID = '" + context.Session.SessionID.Replace("'", "''") + "' ";
                    }
                    dr.Close();


                    cmd.ExecuteNonQuery();


                    cn.Close();
                }

            }
            
            //用在DELTAFLOW LOGIN上 把AD帳號轉換成USER帳號
            [WebMethod]
            public string UserINAD(string AD_ID)
            {
                string rtnString = "NA";
                Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
                sqlParameters.Add(@"AD_FSUSER", AD_ID);
                List<Dictionary<string, string>> sqlResult;
                
                if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBUSER_UserInAD",sqlParameters,out sqlResult))
                { return rtnString; }
                for (int i = 0; i < sqlResult.Count; i++)
                {
                    rtnString = sqlResult[i]["FSUSER_ID"];
                }
                return rtnString;
            }

            //用在DELTAFLOW MyPage\ManagerReasign.aspx.vb 去除重複的部門
            [WebMethod]
            public string EarseRepetDept(string DEPT_ID)
            {
                //Array arrayA = DEPT_ID.Split(',');
                string rtnString = "";
                //Array arrayA = DEPT_ID.Split(',').Distinct().ToArray();
                List<string> ListA = DEPT_ID.Split(',').Distinct().ToList();
                List<string> ListB = new List<string>();
                foreach (string item in ListA)
                {
                    string currentDep = item;
                    string currentParentDep = FindParentGUID(currentDep);
                    string itemStatus = "Add";
                    while(currentParentDep!="0")
                    {
                        int countDeps = ListA.Where(s => s == currentParentDep).Count();
                        if (countDeps>0)
                        {
                            itemStatus = "Del";
                            break;
                        }
                        currentParentDep = FindParentGUID(currentParentDep);
                    }
                    if (itemStatus=="Add")
                    {
                        ListB.Add(item);
                    }
                }

                for (int ArrayCunt = 0; ArrayCunt < ListB.Count; ArrayCunt++)
                {
                    if (ArrayCunt < ListB.Count - 1)
                    {
                        rtnString += ListB[ArrayCunt] + ",";
                    }
                    else
                        rtnString += ListB[ArrayCunt];
                }

                return rtnString;

            }
            //傳回FLOW父部門代號
            [WebMethod]
            public string FindParentGUID(string iniGUID)
            {
                string rtnString = "1";
                Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
                sqlParameters.Add(@"GUID", iniGUID);
                List<Dictionary<string, string>> sqlResult;

                if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_OC_Parent_GUID", sqlParameters, out sqlResult))
                { return rtnString; }
                for (int i = 0; i < sqlResult.Count; i++)
                {
                    rtnString = sqlResult[i]["Parent_GUID"];
                }
                return rtnString;
            }
        } 
}
