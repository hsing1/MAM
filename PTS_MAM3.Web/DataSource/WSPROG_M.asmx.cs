﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Xml;
using System.Text;
using PTS_MAM3.Web.Class;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSPROG_M 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSPROG_M : System.Web.Services.WebService
    {
        List<Class_CODE> m_CodeData = new List<Class_CODE>();               //代碼檔集合
        List<Class_CODE> m_CodeDataBuyD = new List<Class_CODE>();           //代碼檔集合(外購類別細項) 
        List<Class_DELETE_MSG> m_DeleteMsg = new List<Class_DELETE_MSG>();  //檢查刪除節目主檔訊息集合
        List<Class_CODE> m_CodeDataMen = new List<Class_CODE>();            //代碼檔集合(刪除節目主檔-相關人員)
        List<Class_CODE> m_CodeDataRace = new List<Class_CODE>();           //代碼檔集合(刪除節目主檔-參展人員)

        public struct ReturnMsg             //錯誤訊息處理
        {
            public bool bolResult;          //是否錯誤
            public string strErrorMsg;      //錯誤內容
        }

        //解析資料庫回來的Dictionary，轉存成Class
        private List<Class_PROGM> Transfer_PROGM(List<Dictionary<string, string>> sqlResult)
        {
            List<Class_PROGM> returnData = new List<Class_PROGM>();
            m_CodeData = MAMFunctions.Transfer_Class_CODE("SP_Q_TBPROG_M_CODE");
            m_CodeDataBuyD = MAMFunctions.Transfer_Class_CODED("SP_Q_TBZPROGBUYD_ALL");

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_PROGM obj = new Class_PROGM();
                // -------------------------------- 
                obj.FSPROG_ID = sqlResult[i]["FSPROG_ID"];                                  //節目編號   
                obj.FSPGMNAME = sqlResult[i]["FSPGMNAME"];                                  //節目名稱 
                obj.FSPGMENAME = sqlResult[i]["FSPGMENAME"];                                //外文播出名稱
                obj.FSPRDDEPTID = sqlResult[i]["FSPRDDEPTID"];                              //製作部門代碼
                obj.FSCHANNEL = sqlResult[i]["FSCHANNEL"];                                  //可播頻道
                obj.FNTOTEPISODE = Convert.ToInt16(sqlResult[i]["FNTOTEPISODE"]);           //總集數
                obj.FNLENGTH = Convert.ToInt16(sqlResult[i]["FNLENGTH"]);                   //每集時長
                obj.FSPROGOBJID = sqlResult[i]["FSPROGOBJID"];                              //製作目的代碼
                obj.FSPROGSRCID = sqlResult[i]["FSPROGSRCID"];                              //節目來源代碼
                obj.FSPROGATTRID = sqlResult[i]["FSPROGATTRID"];                            //內容屬性代碼
                obj.FSPROGAUDID = sqlResult[i]["FSPROGAUDID"];                              //目標觀眾代碼
                obj.FSPROGTYPEID = sqlResult[i]["FSPROGTYPEID"];                            //表現方式代碼
                obj.FSPROGGRADEID = sqlResult[i]["FSPROGGRADEID"];                          //節目分級代碼
                obj.FSPROGLANGID1 = sqlResult[i]["FSPROGLANGID1"];                          //主聲道代碼
                obj.FSPROGLANGID2 = sqlResult[i]["FSPROGLANGID2"];                          //副聲道代碼
                obj.FSPROGSALEID = sqlResult[i]["FSPROGSALEID"];                            //行銷類別代碼
                obj.FSPROGBUYID = sqlResult[i]["FSPROGBUYID"];                              //外購類別代碼
                obj.FSPROGBUYDID = sqlResult[i]["FSPROGBUYDID"];                            //外購類別細項代碼
                obj.FSPRDCENID = sqlResult[i]["FSPRDCENID"];                                //製作單位代碼
                obj.FSPRDYYMM = sqlResult[i]["FSPRDYYMM"];                                  //製作年月
                obj.FDSHOWDATE = Convert.ToDateTime(sqlResult[i]["WANTSHOWDATE"]);         //預計上檔日期(交片日期)
                obj.SHOW_FDSHOWDATE = MAMFunctions.compareDate(sqlResult[i]["WANTSHOWDATE"]);//預計上檔日期(交片日期)(顯示)
                obj.FSCONTENT = sqlResult[i]["FSCONTENT"];                                  //內容
                obj.FSMEMO = sqlResult[i]["FSMEMO"];                                        //備註
                obj.FSEWEBPAGE = sqlResult[i]["FSEWEBPAGE"];                                //英文網頁資料
                obj.FCDERIVE = sqlResult[i]["FCDERIVE"];                                    //衍生性節目註記(Y/N)
                obj.SHOW_FCDERIVE = MAMFunctions.compareNY(sqlResult[i]["FCDERIVE"]);       //衍生性節目註記(顯示)
                obj.FSWEBNAME = sqlResult[i]["FSWEBNAME"];                                  //網路播出名稱
                obj.FSCHANNEL_ID = sqlResult[i]["FSCHANNEL_ID"];                            //頻道別代碼
                obj.FSDEL = sqlResult[i]["FSDEL"];                                          //刪除註記
                obj.FSDELUSER = sqlResult[i]["FSDELUSER"];                                  //刪除人
                obj.FCOUT_BUY = sqlResult[i]["FCOUT_BUY"];                                  //外購節目
                obj.FSPROGSPEC = sqlResult[i]["FSPROGSPEC"];                                //節目規格
                obj.FSPROGSPEC = sqlResult[i]["FSPROGSPEC"];                                //節目規格
                obj.FNDEP_ID = Convert.ToInt16(sqlResult[i]["FNDEP_ID"]);                   //成案單位

                obj.FSCRTUSER = sqlResult[i]["FSCRTUSER"];                                  //建檔者
                obj.FDCRTDATE = Convert.ToDateTime(sqlResult[i]["WANT_FDCREATEDDATE"]);     //建檔日期
                obj.SHOW_FDCRTDATE = MAMFunctions.compareDate(sqlResult[i]["WANT_FDCREATEDDATE"]);//建檔日期(顯示)
                obj.FSUPDUSER = sqlResult[i]["FSUPDUSER"];                                  //修改者
                obj.FDUPDDATE = Convert.ToDateTime(sqlResult[i]["WANT_FDUPDATEDDATE"]);     //修改日期
                obj.SHOW_FDUPDDATE = MAMFunctions.compareDate(sqlResult[i]["WANT_FDUPDATEDDATE"]);//修改日期(顯示)
                obj.FSCRTUSER_NAME = sqlResult[i]["FSCREATED_BY_NAME"];                     //建檔者姓名
                obj.FSUPDUSER_NAME = sqlResult[i]["FSUPDATED_BY_NAME"];                     //修改者姓名

                obj.FSPRDDEPTID_NAME = compareCode_Code(sqlResult[i]["FSPRDDEPTID"], "TBZDEPT");       //製作部門代碼名稱
                obj.FSPRDCENID_NAME = compareCode_Code(sqlResult[i]["FSPRDCENID"], "TBZPRDCENTER");    //製作單位代碼名稱
                obj.FSPROGOBJID_NAME = compareCode_Code(sqlResult[i]["FSPROGOBJID"], "TBZPROGOBJ");    //製作目的代碼名稱
                obj.FSPROGSRCID_NAME = compareCode_Code(sqlResult[i]["FSPROGSRCID"], "TBZPROGSRC");    //節目來源代碼名稱
                obj.FSPROGATTRID_NAME = compareCode_Code(sqlResult[i]["FSPROGATTRID"], "TBZPROGATTR"); //內容屬性代碼名稱
                obj.FSPROGAUDID_NAME = compareCode_Code(sqlResult[i]["FSPROGAUDID"], "TBZPROGAUD");    //目標觀眾代碼名稱
                obj.FSPROGTYPEID_NAME = compareCode_Code(sqlResult[i]["FSPROGTYPEID"], "TBZPROGTYPE"); //表現方式代碼名稱
                obj.FSPROGGRADEID_NAME = compareCode_Code(sqlResult[i]["FSPROGGRADEID"], "TBZPROGGRADE"); //節目分級代碼名稱
                obj.FSPROGLANGID1_NAME = compareCode_Code(sqlResult[i]["FSPROGLANGID1"], "TBZPROGLANG"); //主聲道代碼名稱
                obj.FSPROGLANGID2_NAME = compareCode_Code(sqlResult[i]["FSPROGLANGID2"], "TBZPROGLANG"); //副聲道代碼名稱
                obj.FSPROGSALEID_NAME = compareCode_Code(sqlResult[i]["FSPROGSALEID"], "TBZPROGSALE");  //行銷類別代碼名稱
                obj.FSPROGBUYID_NAME = compareCode_Code(sqlResult[i]["FSPROGBUYID"], "TBZPROGBUY");     //外購類別代碼名稱
                obj.FSPROGBUYDID_NAME = compareCode_CodeBuyD(sqlResult[i]["FSPROGBUYID"], sqlResult[i]["FSPROGBUYDID"]);    //外購類別細項代碼名稱
                obj.FSCHANNEL_ID_NAME = compareCode_Code(sqlResult[i]["FSCHANNEL_ID"], "TBZCHANNEL"); //頻道別代碼名稱
                obj.FSPROGSPEC_NAME = compareCode_Code_ProgSpec(sqlResult[i]["FSPROGSPEC"], "TBZPROGSPEC");//節目規格名稱   
                obj.FNDEP_ID_NAME = compareCode_Code(sqlResult[i]["FNDEP_ID"], "TBUSER_DEP");              //成案單位名稱
                obj.FSPROGNATIONID_NAME = compareCode_Code(sqlResult[i]["FSPROGNATIONID"], "TBZPROGNATION");              //來源國家名稱

                if (sqlResult[i]["FDEXPIRE_DATE"] == "")
                {
                    obj.FDEXPIRE_DATE = Convert.ToDateTime("1900-1-1");
                    obj.Origin_FDEXPIRE_DATE = Convert.ToDateTime("1900-1-1");
                }
                else
                {
                    obj.FDEXPIRE_DATE = Convert.ToDateTime(sqlResult[i]["FDEXPIRE_DATE"]);                      //到期日期
                    obj.Origin_FDEXPIRE_DATE = Convert.ToDateTime(sqlResult[i]["FDEXPIRE_DATE"]);
                }
                obj.FSEXPIRE_DATE_ACTION = sqlResult[i]["FSEXPIRE_DATE_ACTION"];                            //到期時執行的動作
                obj.Origin_FSEXPIRE_DATE_ACTION = sqlResult[i]["FSEXPIRE_DATE_ACTION"];
                obj.FSPROGNATIONID = sqlResult[i]["FSPROGNATIONID"];                                        //來源國家
                // --------------------------------
                returnData.Add(obj);
            }

            return returnData;
        }

        //透過節目編號查詢節目基本資料
        [WebMethod()]
        public List<Class_PROGM> fnGetProg_M(string ProgMID)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPROG_ID", ProgMID);
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_M", sqlParameters, out sqlResult))
                return new List<Class_PROGM>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PROGM(sqlResult);
        }

        //透過節目編號查詢節目基本資料，判斷是不是衍生子集
        [WebMethod()]
        public string fnGetProg_M_LINK(string ProgMID)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPROG_ID", ProgMID);
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_M", sqlParameters, out sqlResult))
                return "E";    // 發生SQL錯誤時，回傳字串"E"

            if (sqlResult.Count > 0 && sqlResult[0]["FCDERIVE"].Trim() != "")
                return sqlResult[0]["FCDERIVE"].Trim();
            else
                return "E";
        }

        //透過節目名稱查詢節目基本資料
        [WebMethod()]
        public List<Class_PROGM> fnQProg_M_BYPGDNAME(string PGDNAME)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPGMNAME", PGDNAME);
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_M_BYPGMNAME", sqlParameters, out sqlResult))
                return new List<Class_PROGM>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PROGM(sqlResult);
        }

        //透過節目名稱查詢節目基本資料，修改節目資料用
        [WebMethod()]
        public Boolean QUERY_TBPROGM_BYNAME_CHECK(string PGDNAME, string strProgID)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPGMNAME", PGDNAME);
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_M_BYPGMNAME_CHECK", sqlParameters, out sqlResult))
                return true;    // 發生SQL錯誤時，回傳True，表示有同樣節目名稱的節目主檔，不給修改

            if (sqlResult.Count == 1)
            {
                //如果查到的事自己這筆，表示可以修改，回傳False
                if (sqlResult[0]["FSPGMNAME"] == PGDNAME.Trim())
                {
                    if (sqlResult[0]["FSPROG_ID"] == strProgID.Trim())
                        return false;   //節目編號相同，查到的是自己這筆，回傳False，表示可以修改
                    else
                        return true;    //節目編號不同，查到的是已經存在的節目名稱，回傳True，表示不能修改
                }
                else
                    return false;       //沒有這個節目名稱，回傳False，表示可以修改
            }
            else if (sqlResult.Count == 0)
                return false;           //沒有這個節目名稱，回傳False，表示可以修改
            else
                return true;            //照理說修改節目資料時一定不會超過一筆，若是進到這表是有問題就還是寫回傳True，不給修改               
        }

        //透過節目名稱查詢節目基本資料_參展記錄
        [WebMethod()]
        public List<Class_PROGM> fnQProg_M_BYPGDNAME_RACE(string PGDNAME)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPGMNAME", PGDNAME);
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_M_BY_NAME_RACE", sqlParameters, out sqlResult))
                return new List<Class_PROGM>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PROGM(sqlResult);
        }

        //節目資料認定_比對頻道別類型
        [WebMethod()]
        public Boolean fnCheck_ChannelType(string strChannelID, string strstrChannelIDNew)
        {
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            string strChannelType = "";         //頻道類型
            string strChannelTypeNew = "";

            sqlParameters.Add("FSCHANNEL_ID", strChannelID);
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBZCHANNEL", sqlParameters, out resultData) || resultData.Count == 0)
                return false;
            else
                strChannelType = resultData[0]["FSCHANNEL_TYPE"];

            //第二次查詢頻道
            sqlParameters.Clear();
            sqlParameters.Add("FSCHANNEL_ID", strstrChannelIDNew);
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBZCHANNEL", sqlParameters, out resultData) || resultData.Count == 0)
                return false;
            else
                strChannelTypeNew = resultData[0]["FSCHANNEL_TYPE"];

            if (strChannelType == strChannelTypeNew)    //比對頻道別類型
                return true;   //頻道類型相同，可以進行頻道別修改
            else
                return false;  //頻道類型不相同，不可以進行頻道別修改                      
        }

        //節目主檔的所有代碼檔
        [WebMethod()]
        public List<Class_CODE> fnGetTBPROG_M_CODE()
        {
            m_CodeData = MAMFunctions.Transfer_Class_CODE("SP_Q_TBPROG_M_CODE");
            return m_CodeData;
        }

        //節目主檔的所有資料
        [WebMethod()]
        public List<Class_PROGM> fnGetTBPROG_M_ALL()
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_M_ALL", sqlParameters, out sqlResult))
                return new List<Class_PROGM>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PROGM(sqlResult);
        }

        //節目主檔的外購類別細項代碼檔
        [WebMethod()]
        public List<Class_CODE> fnGetTBPROG_M_PROGBUYD_CODE()
        {
            m_CodeData = MAMFunctions.Transfer_Class_CODED("SP_Q_TBZPROGBUYD_ALL");
            return m_CodeData;
        }

        //修改節目主檔的總集數時，要先判斷子集的集數，如果總集數少於子集時要提醒，必且禁止修改，回傳空值表示可以修改節目主檔
        [WebMethod]
        public string CHECK_PROG_M_MAX_EPISODE(string strProgID, short intTOTEPISODE)
        {
            short shortCheck;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            sqlParameters.Add("FSPROG_ID", strProgID);
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_D_BYPROGID_ALL", sqlParameters, out sqlResult))
                return "查詢該節目子集的集數資料異常，無法修改節目基本資料";    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count > 0)
            {
                if (short.TryParse(sqlResult[sqlResult.Count - 1]["FNEPISODE"], out shortCheck) == false)
                    return "查詢該節目子集的集數資料異常，無法修改節目基本資料";
                else
                {
                    if (intTOTEPISODE < shortCheck)
                        return "該節目子集的集數最大集為「" + shortCheck + "集」，修改總集數時務必要大於或等於「" + shortCheck + "集」，否則無法修改節目基本資料";
                    else
                        return "";
                }
            }
            else
                return "";  //查無子集資料，表示不用檢查 
        }


        //修改節目主檔
        [WebMethod]
        public ReturnMsg fnUPDATETBPROGM(Class.Class_PROGM obj)
        {
            ////檢查是否要寫回節目管理系統(暫用)   
            //MAM_PTS_DLL.SysConfig objDll = new MAM_PTS_DLL.SysConfig();
            //string strPGM = objDll.sysConfig_Read("/ServerConfig/ConnectPGM").Trim();
            ReturnMsg objMsg = new ReturnMsg();

            //修改節目主檔的總集數時，要先判斷子集的集數，如果總集數少於子集時要提醒，必且禁止修改
            string strCheckMsg = CHECK_PROG_M_MAX_EPISODE(obj.FSPROG_ID.Trim(), obj.FNTOTEPISODE);

            if (strCheckMsg.Trim() != "")
            {
                objMsg.strErrorMsg = strCheckMsg;
                objMsg.bolResult = false;
                return objMsg;
            }

            if (UPDATE_TBPGM(obj, out objMsg.strErrorMsg) == true)      //修改節目管理系統的節目主檔(目前先註解)
            {
                if (UPDATE_TBPROGM(obj, out objMsg.strErrorMsg) == true)        //修改MAM的節目主檔
                    objMsg.bolResult = true;
                else
                    objMsg.bolResult = false;
            }
            else
                objMsg.bolResult = false;

            return objMsg;
        }

        //修改節目主檔_傳入節目基本資料的的class
        [WebMethod]
        public Boolean UPDATE_TBPROGM(Class.Class_PROGM obj, out string exceptionMsg)
        {
            Boolean bolDB = false;          //寫入節目管理系統資料庫結果
            string strError = "";           //錯誤訊息
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", obj.FSPROG_ID);
            source.Add("FSPGMNAME", obj.FSPGMNAME);
            source.Add("FSPGMENAME", obj.FSPGMENAME);
            source.Add("FSPRDDEPTID", obj.FSPRDDEPTID);
            source.Add("FSCHANNEL", obj.FSCHANNEL);
            source.Add("FNTOTEPISODE", obj.FNTOTEPISODE.ToString());
            source.Add("FNLENGTH", obj.FNLENGTH.ToString());
            source.Add("FSPROGOBJID", obj.FSPROGOBJID);
            source.Add("FSPROGSRCID", obj.FSPROGSRCID);
            source.Add("FSPROGATTRID", obj.FSPROGATTRID);
            source.Add("FSPROGAUDID", obj.FSPROGAUDID);
            source.Add("FSPROGTYPEID", obj.FSPROGTYPEID);
            source.Add("FSPROGGRADEID", obj.FSPROGGRADEID);
            source.Add("FSPROGLANGID1", obj.FSPROGLANGID1);
            source.Add("FSPROGLANGID2", obj.FSPROGLANGID2);
            source.Add("FSPROGSALEID", obj.FSPROGSALEID);
            source.Add("FSPROGBUYID", obj.FSPROGBUYID);
            source.Add("FSPROGBUYDID", obj.FSPROGBUYDID);
            source.Add("FSPRDCENID", obj.FSPRDCENID);
            source.Add("FSPRDYYMM", obj.FSPRDYYMM);
            source.Add("FDSHOWDATE", obj.FDSHOWDATE.ToShortDateString());
            source.Add("FSCONTENT", obj.FSCONTENT);
            source.Add("FSMEMO", obj.FSMEMO);
            source.Add("FSEWEBPAGE", obj.FSEWEBPAGE);
            source.Add("FCDERIVE", obj.FCDERIVE);
            source.Add("FSWEBNAME", obj.FSWEBNAME);
            source.Add("FSCHANNEL_ID", obj.FSCHANNEL_ID);
            source.Add("FSDEL", obj.FSDEL);
            source.Add("FSDELUSER", obj.FSDELUSER);
            source.Add("FCOUT_BUY", obj.FCOUT_BUY);
            source.Add("FSPROGSPEC", obj.FSPROGSPEC);
            source.Add("FSUPDUSER", obj.FSUPDUSER);
            source.Add("FSEXPIRE_DATE_ACTION", obj.FSEXPIRE_DATE_ACTION);
            source.Add("FDEXPIRE_DATE", obj.FDEXPIRE_DATE.ToString("yyyy-MM-dd HH:mm:ss"));
            source.Add("FSPROGNATIONID", obj.FSPROGNATIONID);

            //若有異動到期日期貨到期日動作寫入TBLOG_FILE_EXPIRE_DELETE資料庫
            if (obj.FDEXPIRE_DATE != obj.Origin_FDEXPIRE_DATE || obj.FSEXPIRE_DATE_ACTION != obj.Origin_FSEXPIRE_DATE_ACTION)
            {
                string myFSTABLE_NAME = "TBPROG_M：" + obj.FSPROG_ID;
                string myFSRECORD = obj.Origin_FDEXPIRE_DATE.ToString() + " => " + obj.FDEXPIRE_DATE.ToString() + " || " + obj.Origin_FSEXPIRE_DATE_ACTION + " => " + obj.FSEXPIRE_DATE_ACTION;
                INSERT_TBLOG_FILE_EXPIRE_DELETE(myFSTABLE_NAME, myFSRECORD, obj.FSUPDUSER);
            }

            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBPROG_M", source, out strError, obj.FSUPDUSER, true);
            exceptionMsg = strError;

            if (bolDB == true)
                return true;
            else
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToMAM-UPDATE PROGM", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "ERROR:" + strError);
                EMAIL_PROGM_Error(obj, strError, "MAM", "UPDATE", "M", "");             //寄送Mail
                return false;
            }
        }

        public bool INSERT_TBLOG_FILE_EXPIRE_DELETE(string FSTABLE_NAME, string FSRECORD, string FSUPDATED_BY)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSTABLE_NAME", FSTABLE_NAME);
            source.Add("FSRECORD", FSRECORD);
            source.Add("FSUPDATED_BY", FSUPDATED_BY);
            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBLOG_FILE_EXPIRE_DELETE", source, FSUPDATED_BY);
        }

        //修改節目管理系統的節目主檔_傳入節目基本資料的的class
        [WebMethod]
        public Boolean UPDATE_TBPGM(Class.Class_PROGM obj, out string exceptionMsg)
        {
            Boolean bolDB = false;          //寫入節目管理系統資料庫結果
            string strError = "";           //錯誤訊息

            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROGID", obj.FSPROG_ID);
            source.Add("FSPGMNAME", obj.FSPGMNAME);
            source.Add("FSPGMENAME", obj.FSPGMENAME);
            source.Add("FSPRDDEPTID", obj.FSPRDDEPTID);
            source.Add("FSCHANNEL", obj.FSCHANNEL);
            source.Add("FNTOTEPISODE", obj.FNTOTEPISODE.ToString());
            source.Add("FNLENGTH", obj.FNLENGTH.ToString());
            source.Add("FSPROGOBJID", obj.FSPROGOBJID);
            source.Add("FSPROGSRCID", obj.FSPROGSRCID);
            source.Add("FSPROGATTRID", obj.FSPROGATTRID);
            source.Add("FSPROGAUDID", obj.FSPROGAUDID);
            source.Add("FSPROGTYPEID", obj.FSPROGTYPEID);
            source.Add("FSPROGGRADEID", obj.FSPROGGRADEID);
            source.Add("FSPROGLANGID1", obj.FSPROGLANGID1);
            source.Add("FSPROGLANGID2", obj.FSPROGLANGID2);
            source.Add("FSPROGSALEID", obj.FSPROGSALEID);
            source.Add("FSPROGBUYID", obj.FSPROGBUYID);
            source.Add("FSPROGBUYDID", obj.FSPROGBUYDID);
            source.Add("FSPRDCENID", obj.FSPRDCENID);
            source.Add("FSPRDYYMM", obj.FSPRDYYMM);

            if (obj.FDSHOWDATE.ToShortDateString() == "1900/1/1")
                source.Add("FDSHOWDATE", "");
            else
                source.Add("FDSHOWDATE", obj.FDSHOWDATE.ToShortDateString());

            source.Add("FSCONTENT", obj.FSCONTENT);
            source.Add("FSMEMO", obj.FSMEMO);
            source.Add("FSEWEBPAGE", obj.FSEWEBPAGE);
            source.Add("FCDERIVE", obj.FCDERIVE);
            source.Add("FSWEBNAME", obj.FSWEBNAME);
            source.Add("FSUPDUSER", "MAM");                  //節目管理系統定義由MAM寫入的資料USER皆為 MAM
            source.Add("FSPROGNATIONID", obj.FSPROGNATIONID);

            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction_With_DBLink("SP_U_TBPROG_M_EXTERNAL", source, out strError, obj.FSUPDUSER, true);
            exceptionMsg = strError;

            if (bolDB == true)
                return true;
            else
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToPGM-UPDATE PROGM", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "ERROR:" + strError);
                EMAIL_PROGM_Error(obj, strError, "PGM", "UPDATE", "M", "");                   //寄送Mail
                return false;
            }
        }

        //修改節目主檔_節目資料認定_修改頻道別及成案單位
        [WebMethod]
        public Boolean UPDATE_TBPROGM_IDENTIFIED(Class.Class_PROGM obj, string strOldChannelID)
        {
            Boolean bolDB = false;          //寫入節目管理系統資料庫結果
            string strError = "";           //錯誤訊息
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", obj.FSPROG_ID);
            source.Add("FSCHANNEL_ID", obj.FSCHANNEL_ID);
            source.Add("FNDEP_ID", obj.FNDEP_ID.ToString());

            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBPROG_M_IDENTIFIED", source, out strError, obj.FSUPDUSER, true);

            if (bolDB == true)
            {
                //頻道別不同才要異動Directory，而且如果是影帶回溯就不會有原本的頻道別，所以特別要把無原本頻道別擋掉，因為沒有原本頻道別也不用換Directories
                if (obj.FSCHANNEL_ID.Trim() != strOldChannelID.Trim() && strOldChannelID.Trim() != "")
                    UPDATE_TBDIRECTORIES_PROGM_IDENTIFIED(obj, strOldChannelID);

                return true;
            }
            else
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToMAM-UPDATE PROGM", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "ERROR:" + strError);
                EMAIL_PROGM_Error(obj, strError, "MAM", "UPDATE", "M", "");             //寄送Mail
                return false;
            }
        }

        //修改節目所屬節點_節目資料認定_修改頻道別
        [WebMethod]
        public void UPDATE_TBDIRECTORIES_PROGM_IDENTIFIED(Class.Class_PROGM obj, string strOldChannelID)
        {
            long longDirID;
            MAM_PTS_DLL.DirAndSubjectAccess Subobj = new MAM_PTS_DLL.DirAndSubjectAccess();
            //取得Directory的ID
            longDirID = Subobj.getProgDirId(strOldChannelID, obj.FSPGMNAME.Trim(), obj.FSPROG_ID.Substring(0, 4));

            //異動節點
            Subobj.chgDirAndChannel(obj.FSCHANNEL_ID.Trim(), obj.FSPGMNAME.Trim(), longDirID.ToString(), obj.FSPROG_ID.Substring(0, 4));
        }

        //修改所有子集的頻道別以及影音圖文的頻道別
        [WebMethod]
        public Boolean UPDATE_TBPROGD_TBLOG_CHANNEL(string strType, string strID, string strChannel, string strUpdate_By)
        {
            Boolean bolDB = false;          //寫入節目管理系統資料庫結果
            string strError = "";           //錯誤訊息
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSTYPE", strType);
            source.Add("FSID", strID);
            source.Add("FSCHANNEL_ID", strChannel);

            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBPROGD_TBLOG_CHANNEL", source, out strError, strUpdate_By, true);

            return bolDB;
        }

        //查詢節目資料主檔
        [WebMethod]
        public List<Class_PROGM> fnQUERYTBPROG_M(Class.Class_PROGM obj)
        {
            // SQL Parameters
            Dictionary<string, string> source = new Dictionary<string, string>();

            if (obj.FSPROG_ID == null)                      //節目編號查詢
                source.Add("FSPROG_ID", "");
            else
                source.Add("FSPROG_ID", obj.FSPROG_ID);

            if (obj.FSPGMNAME == null)
                source.Add("FSPGMNAME", "");
            else
                source.Add("FSPGMNAME", obj.FSPGMNAME);

            if (obj.FSPGMENAME == null)
                source.Add("FSPGMENAME", "");
            else
                source.Add("FSPGMENAME", obj.FSPGMENAME);

            if (obj.FSPRDDEPTID == null)
                source.Add("FSPRDDEPTID", "");
            else
                source.Add("FSPRDDEPTID", obj.FSPRDDEPTID);

            if (obj.FSCHANNEL == null)
                source.Add("FSCHANNEL", "");
            else
                source.Add("FSCHANNEL", obj.FSCHANNEL);

            if (obj.FNTOTEPISODE == 0)
                source.Add("FNTOTEPISODE", "");
            else
                source.Add("FNTOTEPISODE", obj.FNTOTEPISODE.ToString());

            if (obj.FNLENGTH == 0)
                source.Add("FNLENGTH", "");
            else
                source.Add("FNLENGTH", obj.FNLENGTH.ToString());

            if (obj.FSPROGOBJID == null)
                source.Add("FSPROGOBJID", "");
            else
                source.Add("FSPROGOBJID", obj.FSPROGOBJID);

            if (obj.FSPROGSRCID == null)
                source.Add("FSPROGSRCID", "");
            else
                source.Add("FSPROGSRCID", obj.FSPROGSRCID);

            if (obj.FSPROGATTRID == null)
                source.Add("FSPROGATTRID", "");
            else
                source.Add("FSPROGATTRID", obj.FSPROGATTRID);

            if (obj.FSPROGAUDID == null)
                source.Add("FSPROGAUDID", "");
            else
                source.Add("FSPROGAUDID", obj.FSPROGAUDID);

            if (obj.FSPROGTYPEID == null)
                source.Add("FSPROGTYPEID", "");
            else
                source.Add("FSPROGTYPEID", obj.FSPROGTYPEID);

            if (obj.FSPROGGRADEID == null)
                source.Add("FSPROGGRADEID", "");
            else
                source.Add("FSPROGGRADEID", obj.FSPROGGRADEID);

            if (obj.FSPROGLANGID1 == null)
                source.Add("FSPROGLANGID1", "");
            else
                source.Add("FSPROGLANGID1", obj.FSPROGLANGID1);

            if (obj.FSPROGLANGID2 == null)
                source.Add("FSPROGLANGID2", "");
            else
                source.Add("FSPROGLANGID2", obj.FSPROGLANGID2);

            if (obj.FSPROGSALEID == null)
                source.Add("FSPROGSALEID", "");
            else
                source.Add("FSPROGSALEID", obj.FSPROGSALEID);

            if (obj.FSPROGBUYID == null)
                source.Add("FSPROGBUYID", "");
            else
                source.Add("FSPROGBUYID", obj.FSPROGBUYID);

            if (obj.FSPROGBUYDID == null)
                source.Add("FSPROGBUYDID", "");
            else
                source.Add("FSPROGBUYDID", obj.FSPROGBUYDID);

            if (obj.FSPRDCENID == null)
                source.Add("FSPRDCENID", "");
            else
                source.Add("FSPRDCENID", obj.FSPRDCENID);

            if (obj.FSPRDYYMM == null)
                source.Add("FSPRDYYMM", "");
            else
                source.Add("FSPRDYYMM", obj.FSPRDYYMM);

            if (obj.FDSHOWDATE == new DateTime(1900, 1, 1))
                source.Add("FDSHOWDATE", "");
            else if (obj.FDSHOWDATE == new DateTime(0001, 1, 1))
                source.Add("FDSHOWDATE", "");
            else
                source.Add("FDSHOWDATE", obj.FDSHOWDATE.ToShortDateString());

            if (obj.FSCONTENT == null)
                source.Add("FSCONTENT", "");
            else
                source.Add("FSCONTENT", obj.FSCONTENT);

            if (obj.FSMEMO == null)
                source.Add("FSMEMO", "");
            else
                source.Add("FSMEMO", obj.FSMEMO);

            if (obj.FSEWEBPAGE == null)
                source.Add("FSEWEBPAGE", "");
            else
                source.Add("FSEWEBPAGE", obj.FSEWEBPAGE);

            if (obj.FCDERIVE == null)
                source.Add("FCDERIVE", "");
            else
                source.Add("FCDERIVE", obj.FCDERIVE);

            if (obj.FSWEBNAME == null)
                source.Add("FSWEBNAME", "");
            else
                source.Add("FSWEBNAME", obj.FSWEBNAME);

            if (obj.FSCHANNEL_ID == null)
                source.Add("FSCHANNEL_ID", "");
            else
                source.Add("FSCHANNEL_ID", obj.FSCHANNEL_ID);

            if (obj.FCOUT_BUY == null)
                source.Add("FCOUT_BUY", "");
            else
                source.Add("FCOUT_BUY", obj.FCOUT_BUY);

            if (obj.FSDEL == null)
                source.Add("FSDEL", "");
            else
                source.Add("FSDEL", obj.FSDEL);

            if (obj.FSDELUSER == null)
                source.Add("FSDELUSER", "");
            else
                source.Add("FSDELUSER", obj.FSDELUSER);

            if (obj.FSPROGSPEC == null)
                source.Add("FSPROGSPEC", "");
            else
                source.Add("FSPROGSPEC", obj.FSPROGSPEC);

            if (obj.FSCRTUSER == null)
                source.Add("FSCRTUSER", "");
            else
                source.Add("FSCRTUSER", obj.FSCRTUSER);

            List<Dictionary<string, string>> sqlResult;

            //MAM_PTS_DLL.Log.AppendTrackingLog("test/DataSource/WSPROG_M", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "查詢節目主檔，節目名稱：" + obj.FSPGMNAME); //trace專用

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_M_BYCONDITIONS", source, out sqlResult))
            {
                //MAM_PTS_DLL.Log.AppendTrackingLog("test/DataSource/WSPROG_M", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "查詢節目主檔發生錯誤，節目名稱：" + obj.FSPGMNAME); //trace專用
                return new List<Class_PROGM>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            }

            //MAM_PTS_DLL.Log.AppendTrackingLog("test/DataSource/WSPROG_M", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "查詢節目主檔正確，節目名稱：" + obj.FSPGMNAME + "，筆數：" + sqlResult.Count()); //trace專用
            return Transfer_PROGM(sqlResult);
        }

        //新增節目管理系統的節目子集資料、MAM的節目子集資料
        /// <summary>
        /// 簽核完成後修改"節目管理系統"的主檔及子集
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="strMin"></param>
        /// <param name="strMax"></param>
        /// <param name="strCreated_By"></param>
        /// <returns></returns>
        [WebMethod]
        public ReturnMsg INSERT_TBPROGD_TBPROG(Class.Class_PROGM obj, string strMin, string strMax, string strCreated_By)
        {
            ////檢查是否要寫回節目管理系統(暫用)   
            //MAM_PTS_DLL.SysConfig objDll = new MAM_PTS_DLL.SysConfig();
            //string strPGM = objDll.sysConfig_Read("/ServerConfig/ConnectPGM").Trim();

            ReturnMsg objMsg = new ReturnMsg();

            if (INSERT_TBPGMD(obj, strMin, strMax, out objMsg.strErrorMsg) == true)      //新增或修改節目管理系統的節目子集
            {
                if (INSERT_TBPROGD(obj, strMin, strMax, out objMsg.strErrorMsg, strCreated_By) == true)        //新增或修改MAM的節目子集
                    objMsg.bolResult = true;
                else
                    objMsg.bolResult = false;
            }
            else
                objMsg.bolResult = false;

            return objMsg;
        }

        //新增節目子集檔_傳入節目主檔的class(執行時機為使用者按下「批次新增子集」的按鈕)
        /// <summary>
        /// 將節目子集新增到MAM內的TABLE TBPROG_D
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="strMin"></param>
        /// <param name="strMax"></param>
        /// <param name="exceptionMsg"></param>
        /// <param name="strCreated_By"></param>
        /// <returns></returns>
        [WebMethod]
        public Boolean INSERT_TBPROGD(Class.Class_PROGM obj, string strMin, string strMax, out string exceptionMsg, string strCreated_By)
        {
            Boolean bolDB = false;          //寫入資料庫結果
            string strError = "";           //錯誤訊息
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", obj.FSPROG_ID);

            if (obj.FNTOTEPISODE.ToString() == "0")
            {
                exceptionMsg = "選擇的節目資料總集數為「0」，無法新增子集資料";
                return false;
            }
            else
            {
                if (strMin != "" && strMax != "")
                {
                    source.Add("MINEPISODE", strMin);                       //指定開始集數、結束集數
                    source.Add("MAXEPISODE", strMax);
                }
                else
                {
                    source.Add("MAXEPISODE", obj.FNTOTEPISODE.ToString());  //從第一集開始新增到最後一集
                }
            }

            //source.Add("FSPGDNAME", obj.FSPGMNAME);
            source.Add("FSPGDNAME", obj.FSWEBNAME);
            source.Add("FSPGDENAME", obj.FSPGMENAME);

            if (obj.FNLENGTH.ToString() == "0")
                source.Add("FNLENGTH", "");
            else
                source.Add("FNLENGTH", obj.FNLENGTH.ToString());

            source.Add("FNTAPELENGTH", "");

            source.Add("FSPRDYEAR", "");
            source.Add("FSPROGSRCID", obj.FSPROGSRCID);
            source.Add("FSPROGATTRID", obj.FSPROGATTRID);
            source.Add("FSPROGAUDID", obj.FSPROGAUDID);
            source.Add("FSPROGTYPEID", obj.FSPROGTYPEID);
            source.Add("FSPROGGRADEID", obj.FSPROGGRADEID);
            source.Add("FSPROGLANGID1", obj.FSPROGLANGID1);
            source.Add("FSPROGLANGID2", obj.FSPROGLANGID2);
            source.Add("FSPROGSALEID", obj.FSPROGSALEID);
            source.Add("FSPROGBUYID", obj.FSPROGBUYID);
            source.Add("FSPROGBUYDID", obj.FSPROGBUYDID);
            source.Add("FCRACE", "Y");  //改成預設為可排檔
            source.Add("FSSHOOTSPEC", "");
            source.Add("FSPROGGRADE", "");
            source.Add("FNSHOWEPISODE", "");
            //source.Add("FSCONTENT", obj.FSCONTENT);
            //source.Add("FSMEMO", obj.FSMEMO);
            source.Add("FSCONTENT", "");//修改 by Jarvis20130629
            source.Add("FSMEMO", "");
            source.Add("FSCHANNEL_ID", obj.FSCHANNEL_ID);
            source.Add("FSDEL", "N");
            source.Add("FSDELUSER", "");
            source.Add("FSNDMEMO", "");
            source.Add("FSPROGSPEC", obj.FSPROGSPEC);
            source.Add("FSCR_TYPE", "");
            source.Add("FSCR_NOTE", "");
            source.Add("FSCRTUSER", strCreated_By);
            source.Add("FSUPDUSER", "");            //新增時修改者為空

            source.Add("FSEXPIRE_DATE_ACTION", obj.FSEXPIRE_DATE_ACTION);    //到期時動作
            source.Add("FDEXPIRE_DATE", obj.FDEXPIRE_DATE.ToString("yyyy-MM-dd HH:mm:ss"));       //到期日期
            source.Add("FSPROGNATIONID", obj.FSPROGNATIONID);               //來源國家ID

            if (strMin != "" && strMax != "")
                bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBPROG_D_BATCH_WANT", source, out strError, strCreated_By, true);
            else
                bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBPROG_D_BATCH", source, out strError, strCreated_By, true);

            exceptionMsg = strError;

            if (bolDB == true)
                return true;
            else
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToMAM-INSERT PROGD", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "ERROR:" + strError);
                EMAIL_PROGM_Error(obj, strError, "MAM", "INSERT", "D", "批次新增集別：" + strMin + "~" + strMax);             //寄送Mail
                return false;
            }
        }

        //新增節目管理系統的節目子集檔_傳入節目主檔的class(執行時機為使用者按下「批次新增子集」的按鈕)
        /// <summary>
        /// 新增到"節目管理系統內"的子集
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="strMin"></param>
        /// <param name="strMax"></param>
        /// <param name="exceptionMsg"></param>
        /// <returns></returns>
        [WebMethod]
        public Boolean INSERT_TBPGMD(Class.Class_PROGM obj, string strMin, string strMax, out string exceptionMsg)
        {
            Boolean bolDB = false;      //寫入節目管理系統資料庫結果
            string strError = "";        //錯誤訊息
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROGID", obj.FSPROG_ID);

            if (obj.FNTOTEPISODE.ToString() == "0")
            {
                exceptionMsg = "選擇的節目資料總集數為「0」，無法新增子集資料";
                return false;
            }
            else
            {
                if (strMin != "" && strMax != "")
                {
                    source.Add("MINEPISODE", strMin);                       //指定開始集數、結束集數
                    source.Add("MAXEPISODE", strMax);
                }
                else
                {
                    source.Add("MAXEPISODE", obj.FNTOTEPISODE.ToString());  //從第一集開始新增到最後一集
                }
            }

            //source.Add("FSPGDNAME", obj.FSPGMNAME);
            source.Add("FSPGDNAME", obj.FSWEBNAME);
            source.Add("FSPGDENAME", obj.FSPGMENAME);
            if (obj.FNLENGTH.ToString() == "0")
                source.Add("FNLENGTH", "");
            else
                source.Add("FNLENGTH", obj.FNLENGTH.ToString());

            source.Add("FNTAPELENGTH", "");
            source.Add("FSPROGSRCID", obj.FSPROGSRCID);
            source.Add("FSPROGATTRID", obj.FSPROGATTRID);
            source.Add("FSPROGAUDID", obj.FSPROGAUDID);
            source.Add("FSPROGTYPEID", obj.FSPROGTYPEID);
            source.Add("FSPROGGRADEID", obj.FSPROGGRADEID);
            source.Add("FSPROGLANGID1", obj.FSPROGLANGID1);
            source.Add("FSPROGLANGID2", obj.FSPROGLANGID2);
            source.Add("FSPROGSALEID", obj.FSPROGSALEID);
            source.Add("FSPROGBUYID", obj.FSPROGBUYID);
            source.Add("FSPROGBUYDID", obj.FSPROGBUYDID);
            source.Add("FCRACE", "Y");      //改成預設為可排檔
            source.Add("FSSHOOTSPEC", "");
            source.Add("FSPROGSPEC", obj.FSPROGSPEC);

            source.Add("FSPRDYEAR", "");//修改 by Jarvis20130629
            source.Add("FSPROGGRADE", "");
            //source.Add("FSCONTENT", obj.FSCONTENT);
            //source.Add("FSMEMO", obj.FSMEMO);
            source.Add("FSCONTENT", "");
            source.Add("FSMEMO", "");
            source.Add("FNSHOWEPISODE", "");
            source.Add("FSCRTUSER", "MAM");                   //節目管理系統定義由MAM寫入的資料USER皆為 MAM     
            source.Add("FSPROGNATIONID", obj.FSPROGNATIONID);               //來源國家ID

            if (strMin != "" && strMax != "")
                bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction_With_DBLink("SP_I_TBPROG_D_BATCH_EXTERNAL_WANT", source, out strError, obj.FSUPDUSER, true);
            else
                bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction_With_DBLink("SP_I_TBPROG_D_BATCH_EXTERNAL", source, out strError, obj.FSUPDUSER, true);

            exceptionMsg = strError;

            if (bolDB == true)
                return true;
            else
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToPGM-SYSTEM INSERT PROGD", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "ERROR:" + strError);
                EMAIL_PROGM_Error(obj, strError, "MAM", "INSERT", "D", "批次新增集別：" + strMin + "~" + strMax);//寄送Mail
                return false;
            }
        }

        //比對代碼檔
        string compareCode_Code(string strID, string strTable)
        {
            for (int i = 0; i < m_CodeData.Count; i++)
            {
                if (m_CodeData[i].ID.ToString().Trim().Equals(strID) && m_CodeData[i].TABLENAME.ToString().Trim().Equals(strTable))
                {
                    return m_CodeData[i].NAME.ToString().Trim();
                }
            }
            return "";
        }

        //比對代碼檔_外購類別細項
        string compareCode_CodeBuyD(string strID, string strDID)
        {
            for (int i = 0; i < m_CodeDataBuyD.Count; i++)
            {
                if (m_CodeDataBuyD[i].ID.ToString().Trim().Equals(strID) && m_CodeDataBuyD[i].IDD.ToString().Trim().Equals(strDID))
                {
                    return m_CodeDataBuyD[i].NAME.ToString().Trim();
                }
            }
            return "";
        }

        //查詢節目資料製作人
        [WebMethod]
        public string QUERYTBPROG_PRODUCER(string strType, string strID)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPRO_TYPE", strType);
            source.Add("FSID", strID);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_PRODUCER", source, out sqlResult))
                return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count > 0)
                return sqlResult[0]["FSPRODUCER"];
            else
                return "";
        }

        //刪除節目主檔
        [WebMethod]
        public ReturnMsg DELETE_TBPROGM(Class.Class_PROGM obj, string strDelUser)
        {
            ReturnMsg objMsg = new ReturnMsg();

            if (DELETE_TBPROGM_PGM(obj, out objMsg.strErrorMsg, strDelUser) == true)              //刪除節目管理系統的節目基本資料檔
            {
                if (DELETE_TBPROGM_MAM(obj, out objMsg.strErrorMsg, strDelUser) == true)          //刪除MAM的節目基本資料檔
                    objMsg.bolResult = true;
                else
                    objMsg.bolResult = false;
            }
            else
                objMsg.bolResult = false;

            return objMsg;
        }

        //刪除MAM的節目主檔       
        public Boolean DELETE_TBPROGM_MAM(Class.Class_PROGM obj, out string exceptionMsg, string strDelUser)
        {
            Boolean bolDB = false;          //寫入節目管理系統資料庫結果
            string strError = "";           //錯誤訊息

            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", obj.FSPROG_ID);
            source.Add("FSDEL", "Y");
            source.Add("FSDELUSER", strDelUser);

            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_TBPROG_M", source, out strError, strDelUser, true);
            exceptionMsg = strError;

            if (bolDB == true)
                return true;
            else
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToMAM-DELETE PROGM", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "ERROR:" + strError);
                EMAIL_PROGM_Error(obj, strError, "MAM", "DELETE", "M", "");             //寄送Mail
                return false;
            }
        }

        //刪除節目管理系統的節目主檔        
        public Boolean DELETE_TBPROGM_PGM(Class.Class_PROGM obj, out string exceptionMsg, string strDelUser)
        {
            Boolean bolDB = false;          //寫入節目管理系統資料庫結果
            string strError = "";           //錯誤訊息

            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROGID", obj.FSPROG_ID);

            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction_With_DBLink("SP_D_TBPROG_M_EXTERNAL", source, out strError, strDelUser, true);
            exceptionMsg = strError;

            if (bolDB == true)
                return true;
            else
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WriteToPGM-DELETE PROGM", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "ERROR:" + strError);
                EMAIL_PROGM_Error(obj, strError, "PGM", "DELETE", "M", "");                   //寄送Mail
                return false;
            }
        }

        //寄送EMAIL通知_資料同步時發生問題要通知管理者        
        public void EMAIL_PROGM_Error(Class.Class_PROGM obj, string strError, string strSystem, string strStatus, string strMD, string strNote)
        {
            //讀取通知的管理者(暫用)   
            MAM_PTS_DLL.SysConfig objDll = new MAM_PTS_DLL.SysConfig();
            string strErrorMail = objDll.sysConfig_Read("/ServerConfig/SynchronizeError").Trim();
            string strPROG_NAME = obj.FSPGMNAME.Trim();
            string strTitle = "";

            StringBuilder msg = new StringBuilder();
            msg.AppendLine("節目名稱：" + strPROG_NAME);
            msg.AppendLine(" , 節目編號：" + obj.FSPROG_ID.Trim());

            if (strSystem.Trim() == "MAM")
                msg.AppendLine(" , 同步失敗資料庫：數位片庫");
            else if (strSystem.Trim() == "PGM")
                msg.AppendLine(" , 同步失敗資料庫：節目管理系統");

            if (strStatus.Trim() == "INSERT")
                msg.AppendLine(" , 動作：新增");
            else if (strStatus.Trim() == "UPDATE")
                msg.AppendLine(" , 動作：修改");
            else if (strStatus.Trim() == "DELETE")
                msg.AppendLine(" , 動作：刪除");

            if (strNote.Trim() != "")
                msg.AppendLine(" ," + strNote.Trim());

            msg.AppendLine(" , 錯誤訊息：" + strError);

            msg.AppendLine(" , 新增者：" + obj.FSCRTUSER.Trim() + " , 修改者：" + obj.FSUPDUSER.Trim());

            if (strMD == "M")
                strTitle = "節目基本資料同步失敗通知-" + strPROG_NAME;
            else if (strMD == "D")
                strTitle = "節目子集資料同步失敗通知-" + strPROG_NAME;

            MAM_PTS_DLL.Protocol.SendEmail(strErrorMail, strTitle, msg.ToString());
        }

        //比對代碼檔(刪除時專用)
        string compareCode_CodeDel(string strID, string strTable, List<Class_CODE> CodeDataDel)
        {
            for (int i = 0; i < CodeDataDel.Count; i++)
            {
                if (CodeDataDel[i].ID.ToString().Trim().Equals(strID.Trim()) && CodeDataDel[i].TABLENAME.ToString().Trim().Equals(strTable.Trim()))
                {
                    return CodeDataDel[i].NAME.ToString().Trim();
                }
            }
            return "";
        }

        //刪除節目資料時，要檢查該節目除了節目主檔外是否有其他資料
        [WebMethod]
        public List<Class_DELETE_MSG> DELETE_TBPROGM_Check(Class.Class_PROGM obj, string strDelUser)
        {
            string strProgID = obj.FSPROG_ID.ToString().Trim();

            DELETE_TBPROGM_Check_PROGD(strProgID);              //刪除節目資料時，檢查節目子集檔
            DELETE_TBPROGM_Check_PROGMEN(strProgID);            //刪除節目資料時，檢查節目相關人員檔
            DELETE_TBPROGM_Check_PROGRACE(strProgID);           //刪除節目資料時，檢查參展得獎記錄檔
            DELETE_TBPROGM_Check_PROGLINK(strProgID);           //刪除節目資料時，檢查扣播出次數節目子集檔
            DELETE_TBPROGM_Check_BROCAST(strProgID);            //刪除節目資料時，檢查送帶轉檔
            DELETE_TBPROGM_Check_ARCHIVE(strProgID);            //刪除節目資料時，檢查入庫檔
            DELETE_TBPROGM_Check_QUEUE(strProgID);              //刪除節目資料時，檢查排表
            DELETE_TBPROGM_Check_PGM_DB(strProgID);             //刪除節目資料時，檢查節目管理系統各個資料表
            return m_DeleteMsg;
        }

        //刪除節目資料時，檢查節目子集檔
        public void DELETE_TBPROGM_Check_PROGD(string ProgID)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", ProgID);

            List<Dictionary<string, string>> sqlResult;
            MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_D_BYPROGID", source, out sqlResult);

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_DELETE_MSG obj = new Class_DELETE_MSG();
                obj.FSSystem = "節目子集資料";
                obj.FSCheckMsg = "集別 - " + MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]);

                if (sqlResult[i]["FNEPISODE_NAME"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 子集名稱 - " + sqlResult[i]["FNEPISODE_NAME"].ToString().Trim();

                m_DeleteMsg.Add(obj);
            }
        }

        //刪除節目資料時，檢查節目相關人員檔
        public void DELETE_TBPROGM_Check_PROGMEN(string ProgID)
        {
            m_CodeDataMen = MAMFunctions.Transfer_Class_CODE("SP_Q_TBPROGMEN_CODE");

            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", ProgID);
            source.Add("FNEPISODE", "");
            source.Add("FSTITLEID", "");
            source.Add("FSSTAFFID", "");

            List<Dictionary<string, string>> sqlResult;
            MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROGMEN", source, out sqlResult);

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_DELETE_MSG obj = new Class_DELETE_MSG();
                obj.FSSystem = "節目相關人員資料";

                if (MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]) != "")
                    obj.FSCheckMsg = "集別 - " + MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]);

                obj.FSCheckMsg = obj.FSCheckMsg + " ， 頭銜 - " + compareCode_CodeDel(sqlResult[i]["FSTITLEID"], "TBZTITLE", m_CodeDataMen);       //頭銜名稱

                if (sqlResult[i]["FSNAME"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 公司/姓名 - " + sqlResult[i]["FSNAME"].ToString().Trim();             //公司/姓名名稱

                if (sqlResult[i]["FSSTAFFID"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 工作人員 - " + compareCode_CodeDel(sqlResult[i]["FSSTAFFID"], "TBSTAFF", m_CodeDataMen);//工作人員

                m_DeleteMsg.Add(obj);
            }
        }

        //刪除節目資料時，檢查參展得獎記錄檔
        public void DELETE_TBPROGM_Check_PROGRACE(string ProgID)
        {
            m_CodeDataRace = MAMFunctions.Transfer_Class_CODE("SP_Q_TBPROGRACE_CODE");

            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", ProgID);

            List<Dictionary<string, string>> sqlResult;
            MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROGRACE_BYPROGID", source, out sqlResult);

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_DELETE_MSG obj = new Class_DELETE_MSG();
                obj.FSSystem = "節目參展得獎資料";

                if (MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]) != "")
                    obj.FSCheckMsg = "集別 - " + MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]);

                if (sqlResult[i]["FSPROGSTATUSID"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 參展狀態 - " + compareCode_CodeDel(sqlResult[i]["FSPROGSTATUSID"], "TBZPROGSTATUS", m_CodeDataRace);       //參展狀態

                if (sqlResult[i]["FSAREA"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 參展區域 - " + sqlResult[i]["FSAREA"].ToString().Trim();

                if (sqlResult[i]["FSPROGRACE"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 參展名稱 - " + sqlResult[i]["FSPROGRACE"].ToString().Trim();

                if (sqlResult[i]["FSPROGWIN"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 得獎名稱 - " + sqlResult[i]["FSPROGWIN"].ToString().Trim();

                m_DeleteMsg.Add(obj);
            }
        }

        //刪除節目資料時，檢查扣播出次數節目子集檔
        public void DELETE_TBPROGM_Check_PROGLINK(string ProgID)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", ProgID);

            List<Dictionary<string, string>> sqlResult;
            MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROGLINK_BYPROGID", source, out sqlResult);

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_DELETE_MSG obj = new Class_DELETE_MSG();
                obj.FSSystem = "節目衍生子集資料";

                if (MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]) != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + "節目 - " + sqlResult[i]["FSPROG_ID_NAME"] + " ， 子集名稱 - " + sqlResult[i]["FNEPISODE_NAME"] + " ， 集別 - " + MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]);

                if (MAMFunctions.compareNumber(sqlResult[i]["FNORGEPISODE"]) != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 扣播出次數節目 - " + sqlResult[i]["FSORGPROG_ID_NAME"] + " ， 扣播出次數子集名稱 - " + sqlResult[i]["FNORGEPISODE_NAME"] + " ， 扣播出次數集別 - " + MAMFunctions.compareNumber(sqlResult[i]["FNORGEPISODE"]);

                m_DeleteMsg.Add(obj);
            }
        }

        //刪除節目資料時，檢查送帶轉檔
        public void DELETE_TBPROGM_Check_BROCAST(string ProgID)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSBRO_TYPE", "G");
            source.Add("FSID", ProgID);

            List<Dictionary<string, string>> sqlResult;
            MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBBROADCAST_BYID", source, out sqlResult);

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_DELETE_MSG obj = new Class_DELETE_MSG();
                obj.FSSystem = "送帶轉檔資料";

                if (MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]) != "")
                    obj.FSCheckMsg = "集別 - " + MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]);
                else
                    obj.FSCheckMsg = "集別 - 無";

                if (sqlResult[i]["FNEPISODE_NAME"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 子集名稱 - " + sqlResult[i]["FNEPISODE_NAME"].ToString().Trim();

                if (sqlResult[i]["WANT_FDBRO_DATE"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 申請日期 - " + sqlResult[i]["WANT_FDBRO_DATE"].ToString().Trim();

                m_DeleteMsg.Add(obj);
            }
        }

        //刪除節目資料時，檢查入庫檔
        public void DELETE_TBPROGM_Check_ARCHIVE(string ProgID)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSTYPE", "G");
            source.Add("FSID", ProgID);

            List<Dictionary<string, string>> sqlResult;
            MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBARCHIVE_BYID", source, out sqlResult);

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_DELETE_MSG obj = new Class_DELETE_MSG();
                obj.FSSystem = "入庫資料";

                if (MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]) != "")
                    obj.FSCheckMsg = "集別 - " + MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]);
                else
                    obj.FSCheckMsg = "集別 - 無";

                if (sqlResult[i]["FNEPISODE_NAME"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 子集名稱 - " + sqlResult[i]["FNEPISODE_NAME"].ToString().Trim();

                if (sqlResult[i]["WANT_FDARC_DATE"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 申請日期 - " + sqlResult[i]["WANT_FDARC_DATE"].ToString().Trim();

                m_DeleteMsg.Add(obj);
            }
        }

        //刪除節目資料時，檢查排表
        public void DELETE_TBPROGM_Check_QUEUE(string ProgID)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", ProgID);
            source.Add("FNEPISODE", "");

            List<Dictionary<string, string>> sqlResult;
            MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPGM_QUEUE_BY_PROGID", source, out sqlResult);

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_DELETE_MSG obj = new Class_DELETE_MSG();
                obj.FSSystem = "排表資料";

                if (MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]) != "")
                    obj.FSCheckMsg = "集別 - " + MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]);
                else
                    obj.FSCheckMsg = "集別 - 無";

                if (sqlResult[i]["Want_Date"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 排播日期 - " + sqlResult[i]["Want_Date"].ToString().Trim();

                if (sqlResult[i]["FSBEG_TIME"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 排播時間 - " + sqlResult[i]["FSBEG_TIME"].ToString().Trim();

                if (sqlResult[i]["FSCHANNEL_NAME"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 排播頻道 - " + sqlResult[i]["FSCHANNEL_NAME"].ToString().Trim();

                m_DeleteMsg.Add(obj);
            }
        }

        //刪除節目資料時，檢查節目管理系統的各個資料表
        public void DELETE_TBPROGM_Check_PGM_DB(string ProgID)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROG_ID", ProgID);

            List<Dictionary<string, string>> sqlResult;
            MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPROG_M_DELETE_DETAIL_EXTERNAL", source, out sqlResult);

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_DELETE_MSG obj = new Class_DELETE_MSG();
                //TableCName是取中文名稱，若要取英文名稱則是TableName
                obj.FSSystem = "節目管理系統資料-" + sqlResult[i]["TableCName"];
                obj.FSCheckMsg = "資料筆數 - " + sqlResult[i]["DataCount"] + "筆";

                m_DeleteMsg.Add(obj);
            }
        }

        // Dennis.Wen: 呼叫WSPROG_MYSQL_IMPORT, 把結果組為字串傳回前端.
        /// <summary>
        /// 呼叫WSPROG_MYSQL_IMPORT, 把結果組為字串傳回前端.
        /// </summary>
        [WebMethod]
        public String chkAndImportFromMySQL(string fsProgId, string pgmName, int startEpisode, int endEpisode, int iforceUpdate, string createdUser)
        {
            WSPROG_MYSQL_IMPORT wsPROG_MYSQL_IMPORT = new WSPROG_MYSQL_IMPORT();
            string strResult = "";
            Boolean blResult;

            blResult = wsPROG_MYSQL_IMPORT.chkAndImportFromMySQL(out strResult, fsProgId, pgmName, startEpisode, endEpisode, iforceUpdate, createdUser);
            if (blResult != true)
                strResult = "ERROR:" + strResult;

            return strResult;
        }

        //製作單位的所有代碼檔_透過名稱查詢
        [WebMethod()]
        public List<Class_CODE> GetTBPGM_PRDCENTER_CODE_BYNAME(string strName)
        {
            m_CodeData = MAMFunctions.Transfer_Class_CODE("SP_Q_TBZPRDCENTER_BYNAME_EXTERNAL", strName);
            return m_CodeData;
        }

        //比對代碼檔_節目規格
        string compareCode_Code_ProgSpec(string strID, string strTable)
        {
            string strProgSpec = "";

            for (int i = 0; i < m_CodeData.Count; i++)
            {
                if (strID.IndexOf(m_CodeData[i].ID.ToString().Trim()) > -1 && m_CodeData[i].TABLENAME.ToString().Trim().Equals(strTable))
                {
                    strProgSpec = strProgSpec + m_CodeData[i].NAME.ToString().Trim() + " ";
                }
            }
            return strProgSpec;
        }

        /// <summary>
        /// 匯出節目影片明細
        /// </summary>
        /// <param name="FSTYPE">G、P、D</param>
        /// <param name="FSPROG_ID">節目編號</param>
        /// <param name="FNBEG_EPISODE">集數-起</param>
        /// <param name="FNEND_EPISODE">集數-迄</param>
        /// <param name="FSCHANNEL_ID">頻道</param>
        /// <param name="FSSOURCE">來源</param>
        /// <param name="FSCREATED_BY">匯出者</param>
        /// <returns></returns>
        [WebMethod]
        public string fnRPT_PROGRAM_VIDEO_DETAIL(string FSTYPE, string FSPROG_ID, string FNBEG_EPISODE, string FNEND_EPISODE, string FSCHANNEL_ID, string FSSOURCE, string FSCREATED_BY)
        {
            string strURL = "";  //要傳回去的URL

            //取列印報表的系統設定檔
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string reportSrv = SettingObj.sysConfig_Read("/ServerConfig/ReportServer");

            Dictionary<string, string> source = new Dictionary<string, string>();
            string fsGUID = Guid.NewGuid().ToString();
            source.Add("FSGUID", fsGUID);
            source.Add("FSCREATED_BY", FSCREATED_BY);
            source.Add("FSTYPE", FSTYPE);
            source.Add("FSPROG_ID", FSPROG_ID);
            source.Add("FNBEG_EPISODE", FNBEG_EPISODE);
            source.Add("FNEND_EPISODE", FNEND_EPISODE);
            source.Add("FSCHANNEL_ID", FSCHANNEL_ID);
            source.Add("FSSOURCE", FSSOURCE);

            List<Dictionary<string, string>> sqlResult;
            MAM_PTS_DLL.DbAccess.Do_Query("SP_I_RPT_TBPROGRAM_VIDEO_DETAIL", source, out sqlResult);
            if (sqlResult.Count > 0 && string.IsNullOrEmpty(sqlResult[0]["fsRESULT"].ToString()))
            {
                strURL = reportSrv + "RPT_PROGRAM_VIDEO_DETAIL&rs:Command=Render&rc:parameters=false&rs:Format=EXCEL&FSGUID=" + fsGUID;
            }


            return strURL;
        }

    }
}
