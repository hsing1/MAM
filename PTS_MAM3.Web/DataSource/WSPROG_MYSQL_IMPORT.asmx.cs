﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Transactions; 

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSPROG_MYSQL_IMPORT 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSPROG_MYSQL_IMPORT : System.Web.Services.WebService
    {
        public struct ReturnMySqlMsg                //訊息處理
        {
            public bool bolResult;                  //是否錯誤
            public string strMsg;                   //回傳訊息
            public string strMySQLWhosInCharge;     //委製協調
            public Int64 intMySQLProgID;            //MySql的節目主檔編號
            public bool bolQuery;                   //詢問

        }

        //不論是哪一種類型都要先檢查MySql的資料主檔裡是否只有一筆相同的節目資料，以及子集資料
        [WebMethod]
        public ReturnMySqlMsg checkProgName_ProgD(string strProgID, string pgmName, int startEpisode, int endEpisode)
        {
            Int64 MySQLProgID;
            string MySQLWhosInCharge = "";
            ReturnMySqlMsg msgobj = new ReturnMySqlMsg();

            //查詢外部網路輸入的資料是否有超過一筆相同的節目資料
            if (checkMySQLProgName(pgmName, out msgobj.strMsg, out MySQLProgID, out MySQLWhosInCharge) == false)
            {
                msgobj.bolResult = false;
                return msgobj;
            }

            //檢查子集是否有資料
            if (checkProgD_DATA(MySQLProgID, strProgID, startEpisode, endEpisode, out msgobj.strMsg) == false)
            {
                msgobj.bolResult = false;
                return msgobj;
            }

            msgobj.bolResult = true;
            msgobj.intMySQLProgID = MySQLProgID;
            return msgobj;             
        }

        //執行工作人員的更新
        [WebMethod]
        public ReturnMySqlMsg DoProgMen(Int64 MySQLProgID, string strProgID, string pgmName, int startEpisode, int endEpisode,string strWhosInCharge, Boolean bolConfirm,string createdUser)
        {            
            ReturnMySqlMsg msgobj = new ReturnMySqlMsg();

            //檢查子集工作人員是否審核通過
            if (checkProgD_DATA_MEM(MySQLProgID, startEpisode, endEpisode, out msgobj.strMsg) == false)
            {
                msgobj.bolResult = false;
                msgobj.bolQuery = false;
                return msgobj;
            }

            //第一次進入，檢查子集工作人員是否已存在確認要刪除重建
            if (bolConfirm == false)
            {
                if (checkProgD_DATA_MEM_DELETE(strProgID, startEpisode, endEpisode, out msgobj.strMsg) == false)
                {
                    msgobj.bolResult = false;
                    msgobj.bolQuery = true;
                    return msgobj;
                }
            }

            //若是已確認進入，或是第一次進入但是沒有要刪除重建的繼續執行
            //刪除子集的工作人員
            if (deleteProgMen_DATA(MySQLProgID, strProgID, startEpisode, endEpisode) == false)
            {
                msgobj.bolResult = false;
                msgobj.bolQuery = false;
                msgobj.strMsg = "刪除子集的工作人員資料異常";
                return msgobj;
            }

            //新增子集工作人員
            if (AddProgMen_DATA(MySQLProgID, strProgID, startEpisode, endEpisode, strWhosInCharge, createdUser) == false)
            {
                msgobj.bolResult = false;
                msgobj.bolQuery = false;
                msgobj.strMsg = "新增子集的工作人員資料異常";
                return msgobj;
            }
          
            msgobj.bolResult = true;
            return msgobj;
        }

        //執行音樂的更新
        [WebMethod]
        public ReturnMySqlMsg DoProgMusic(Int64 MySQLProgID, string strProgID, string pgmName, int startEpisode, int endEpisode, Boolean bolConfirm)
        {
            ReturnMySqlMsg msgobj = new ReturnMySqlMsg();

            //檢查子集音樂是否審核通過
            if (checkProgD_DATA_MUSIC(MySQLProgID, startEpisode, endEpisode, out msgobj.strMsg) == false)
            {
                msgobj.bolResult = false;
                msgobj.bolQuery = false;
                return msgobj;
            }

            //第一次進入，檢查子集音樂是否已存在確認要刪除重建
            if (bolConfirm == false)
            {
                if (checkProgD_DATA_MUSIC_DELETE(strProgID, startEpisode, endEpisode, out msgobj.strMsg) == false)
                {
                    msgobj.bolResult = false;
                    msgobj.bolQuery = true;
                    return msgobj;
                }
            }

            //若是已確認進入，或是第一次進入但是沒有要刪除重建的繼續執行
            //刪除子集的音樂資料
            if (deleteProgMusic_DATA(MySQLProgID, strProgID, startEpisode, endEpisode) == false)
            {
                msgobj.bolResult = false;
                msgobj.bolQuery = false;
                msgobj.strMsg = "刪除子集的音樂資料異常";
                return msgobj;
            }

            //新增子集的音樂資料
            if (AddProgMusic_DATA(MySQLProgID, strProgID, startEpisode, endEpisode) == false)
            {
                msgobj.bolResult = false;
                msgobj.bolQuery = false;
                msgobj.strMsg = "新增子集的音樂資料異常";
                return msgobj;
            }

            msgobj.bolResult = true;
            return msgobj;
        }

        //檢查外部網路輸入的資料是否有超過一筆相同的節目資料
        public Boolean checkMySQLProgName(string pgmName, out string msg, out Int64 MySQLProgID, out string MySQLWhosInCharge)
        {          
            SqlConnection sqlConn = new SqlConnection(MAM_PTS_DLL.DbAccess.sqlConnStr);
            SqlCommand cmd = new SqlCommand("", sqlConn);
            SqlDataReader dr;
            string SQLstr = string.Empty;
            WSMAMFunctions wsobj = new WSMAMFunctions();
            List<string> rtnList;
            string BNENO = "";
            msg = "";
            MySQLProgID = 0;
            string MySQLCompany = "";
            MySQLWhosInCharge = "";

            try
            {
                sqlConn.Open();
                int recNum = 0;     //記錄的筆數             
                SQLstr = "SELECT count(1) recNum FROM OPENQUERY(PTS_PROG_MYSQL, 'SELECT PAENO, PACOM, BNENO FROM CRFILE_PROGA WHERE PANAM = ''" + pgmName.Replace("'", "''''") + "'' ')  ";

                cmd.CommandText = SQLstr;
                dr = cmd.ExecuteReader();
                
                if (dr.Read())
                {
                    recNum = Convert.ToInt32(dr["recNum"]);
                }
                else
                {
                    dr.Close();
                    msg = "查詢外部系統MySQL的CRFILE_PROGA資料表發生錯誤！";
                    return false;
                }        

                if (recNum == 0)
                {
                    dr.Close();
                    sqlConn.Close();
                    msg = "外部系統查無此節目名稱為【" + pgmName + "】的資料";                
                    return false;       //查無資料，無法匯入
                }
                else if (recNum > 1)
                {
                    dr.Close();
                    sqlConn.Close();
                    msg = "外部系統的資料中不只一個節目名稱為【" + pgmName + "】！";
                    return false;       //查詢到相同節目名稱的資料超過一筆，無法匯入
                }
                else if (recNum == 1)
                {
                    dr.Close();
                    //查詢MySQL資料表的節目主檔的ID(MySQLProgID)、公司姓名(MySQLCompany)、委製協調(MySQLWhosInCharge)
                    cmd.CommandText = "SELECT PAENO, ISNULL(PACOM, '') PACOM, ISNULL(BNENO, '')BNENO FROM OPENQUERY(PTS_PROG_MYSQL, 'SELECT PAENO, PACOM, BNENO FROM CRFILE_PROGA WHERE PANAM = ''" + pgmName.Replace("'", "''''") + "'' ')  ";
                    dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        MySQLProgID = Convert.ToInt64(dr["PAENO"]);
                        MySQLCompany = dr["PACOM"].ToString();
                        BNENO = dr["BNENO"].ToString();

                        if (BNENO != string.Empty)
                        {
                            rtnList = wsobj.fnGetValue("SELECT BNNAM FROM OPENQUERY(PTS_PROG_MYSQL, 'SELECT BNNAM FROM CRFILE_SYSMAN WHERE BNENO = ''" + BNENO + "'' ')");
                            if (rtnList.Count > 0)
                            {
                                MySQLWhosInCharge = rtnList[0];
                            }
                        }
                        dr.Close();
                        return true;        //只有一筆節目主檔的資料，可以正常匯入
                    }
                    else
                    {
                        dr.Close();
                        msg = "查詢外部系統MySQL的CRFILE_PROGA資料表發生錯誤！";
                        return false;
                    }                    
                }
                else
                {
                    dr.Close();    
                    msg = "查詢外部系統MySQL的CRFILE_PROGA資料表發生錯誤！";
                    return false;
                }        
            }
            catch (Exception ex)
            {
                msg = "查詢外部系統MySQL的CRFILE_PROGA資料表發生錯誤！";
                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/WSPROG_MYSQL_IMPORT/checkMySQLProgName", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "查詢外部系統MySQL的CRFILE_PROGA資料表發生錯誤！節目名稱：" + pgmName + "  訊息：" + ex.Message);
                return false;
            }
            finally
            {                
                sqlConn.Close();
            }
        }

        //檢查子集是否有資料，才可以匯入
        public Boolean checkProgD_DATA(Int64 MySQLProgID, string strProgID,int startEpisode, int endEpisode, out string msg)
        {           
            WSMAMFunctions wsobj = new WSMAMFunctions();
            string SQLstr = string.Empty;
            List<string> rtnList;
            string strEpsList = string.Empty;   //檢查子集是否存在
            msg = "";

            try
            {
                SQLstr = "SELECT * FROM OPENQUERY(PTS_PROG_MYSQL, 'SELECT PBSET FROM CRFILE_PROGB WHERE PAENO = " + MySQLProgID + " AND PBSET >= " + startEpisode + " AND PBSET <= " + endEpisode + " ORDER BY PBSET') ";
                rtnList = wsobj.fnGetValue(SQLstr);

                if (rtnList.Count == 0)
                {                   
                    msg = "外部網路輸入的資料沒有該節目【第" + startEpisode + "集 - 第" + endEpisode + "集】任何一集的子集資料，無法匯入！";              
                    return false;
                }
                else if (rtnList.Count != (endEpisode - startEpisode + 1))
                {
                    int intEpsNo = 0;
                    for (intEpsNo = startEpisode; intEpsNo <= endEpisode; intEpsNo++)
                    {
                        Boolean b = false;

                        foreach (string s in rtnList)
                        {
                            if (Convert.ToInt32(s) == intEpsNo)
                            {
                                b = true;
                                break;
                            }
                        }

                        if (!b)
                        {
                            if (strEpsList.Trim() == "")
                            {
                                strEpsList = intEpsNo.ToString();
                            }
                            else
                            {
                                strEpsList = strEpsList + ", " + intEpsNo;
                            }
                        }
                    }

                    if (strEpsList.Length > 0)
                    {
                        msg = "外部網路輸入的資料沒有該節目這幾集【第 " + strEpsList + " 集】的子集資料，無法匯入！";                   
                        return false;
                    }
                }
                else
                {
                    strEpsList = "";
                    List<string> rtnList2;

                    foreach (string s in rtnList)
                    {
                        SQLstr = "select * from PTSPROG.PTSProgram.dbo.TBPROG_D  WHERE FSPROGID = '" + strProgID + "' AND FNEPISODE = " + s;
                        rtnList2 = wsobj.fnGetValue(SQLstr);
                        if (rtnList2.Count == 0)
                        {
                            if (strEpsList.Trim() == "")                           
                                strEpsList = s;                           
                            else                           
                                strEpsList = strEpsList + ", " + s;                            
                        }

                    }

                    if (strEpsList.Length > 0)
                    {
                        msg = "請先新增下列子集後再執行整批匯入：" + strEpsList + "。";  
                        return false;
                    }
                }

                return true;    //子集有資料，可以繼續往下作
            }
            catch (Exception ex)
            {
                msg = "查詢外部系統MySQL的CRFILE_PROGB資料表發生錯誤！";
                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/WSPROG_MYSQL_IMPORT/checkProgD_DATA", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR,
                    "查詢外部系統MySQL的CRFILE_PROGB資料表發生錯誤！MySQL節目編號：" + MySQLProgID.ToString() + "，節目編號：" + strProgID + "，開始集數：" + startEpisode.ToString() + "，結束集數：" + endEpisode.ToString() + "  訊息：" + ex.Message);
                return false;
            }           
        }
        
        //檢查子集音樂報表是否審核通
        public Boolean checkProgD_DATA_MUSIC(Int64 MySQLProgID, int startEpisode, int endEpisode, out string msg)
        {
            WSMAMFunctions wsobj = new WSMAMFunctions();
            string SQLstr = string.Empty;
            string strEpsList = string.Empty;
            List<string> rtnList;
            msg = "";
            try
            {
                SQLstr = "SELECT PBSET FROM CRFILE_PROGB WHERE PAENO = " + MySQLProgID + " AND PBSET >= " + startEpisode + " AND PBSET <= " + endEpisode + " AND PBCBB < 1 ORDER BY PBSET";
                SQLstr = "SELECT * FROM openquery(PTS_PROG_MYSQL, '" + SQLstr.Replace("'", "''''") + "') ";
                rtnList = wsobj.fnGetValue(SQLstr);

                if (rtnList.Count > 0)
                {
                    strEpsList = "";
                    foreach (string s in rtnList)
                    {
                        if (strEpsList.Trim() == "")                       
                            strEpsList = s;                       
                        else
                            strEpsList = strEpsList + ", " + s;
                    }
                    msg = "下列子集的音樂報表尚未審核通過：" + strEpsList + "，無法執行整批匯入！";        
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/WSPROG_MYSQL_IMPORT/checkProgD_DATA_MUSIC", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR,
                    "查詢外部系統MySQL的CRFILE_PROGB資料表發生錯誤！MySQL節目編號：" + MySQLProgID.ToString() + "，開始集數：" + startEpisode.ToString() + "，結束集數：" + endEpisode.ToString() + "  訊息：" + ex.Message);
                return false;
            }     
        }

        //檢查子集工作人員是否審核通過
        public Boolean checkProgD_DATA_MEM(Int64 MySQLProgID, int startEpisode, int endEpisode, out string msg)
        {
            WSMAMFunctions wsobj = new WSMAMFunctions();
            string SQLstr = string.Empty;
            string strEpsList = string.Empty;
            List<string> rtnList;
            msg = "";
            try
            {
                SQLstr = "SELECT PBSET FROM CRFILE_PROGB WHERE PAENO = " + MySQLProgID + " AND PBSET >= " + startEpisode + " AND PBSET <= " + endEpisode + " AND PBCBA < 1 ORDER BY PBSET";
                SQLstr = "SELECT * FROM openquery(PTS_PROG_MYSQL, '" + SQLstr.Replace("'", "''''") + "' ) ";
                rtnList = wsobj.fnGetValue(SQLstr);

                if (rtnList.Count > 0)
                {
                    strEpsList = "";
                    foreach (string s in rtnList)
                    {
                        if (strEpsList.Trim() == "")                        
                            strEpsList = s;                       
                        else                      
                            strEpsList = strEpsList + ", " + s;                       
                    }
                                     
                    msg = "下列子集的工作人員報表尚未審核通過：" + strEpsList + "，無法執行整批匯入！";                   
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/WSPROG_MYSQL_IMPORT/checkProgD_DATA_MEM", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR,
                    "查詢外部系統MySQL的CRFILE_PROGB資料表發生錯誤！MySQL節目編號：" + MySQLProgID.ToString() + "，開始集數：" + startEpisode.ToString() + "，結束集數：" + endEpisode.ToString() + "  訊息：" + ex.Message);
                return false;
            }
        }

        //檢查子集工作人員是否已存在確認要刪除重建
        public Boolean checkProgD_DATA_MEM_DELETE(string strProgID, int startEpisode, int endEpisode, out string msg)
        {
            WSMAMFunctions wsobj = new WSMAMFunctions();
            string SQLstr = string.Empty;
            string strEpsList = string.Empty;
            List<string> rtnList;
            msg = "";
            try
            {
                SQLstr = "SELECT DISTINCT FNEPISODE FROM PTSPROG.PTSProgram.dbo.TBPROGMEN WHERE FSPROGID = '" + strProgID + "' AND FNEPISODE >= " + startEpisode + " AND FNEPISODE <= " + endEpisode + " ORDER BY FNEPISODE";
                rtnList = wsobj.fnGetValue(SQLstr);

                if (rtnList.Count > 0)
                {
                    strEpsList = "";
                    foreach (string s in rtnList)
                    {
                        if (strEpsList.Trim() == "")
                        {
                            strEpsList = s;
                        }
                        else
                        {
                            strEpsList = strEpsList + ", " + s;
                        }
                    }

                    msg = "下列子集的工作人員已經建檔：" + strEpsList + "，確定要清除 (所有工作人員將被刪除) 後重新匯入工作人員資料嗎？"; 
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/WSPROG_MYSQL_IMPORT/checkProgD_DATA_MEM_DELETE", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR,
                    "查詢外部系統MySQL的CRFILE_PROGB資料表發生錯誤！節目編號：" + strProgID + "，開始集數：" + startEpisode.ToString() + "，結束集數：" + endEpisode.ToString() + "  訊息：" + ex.Message);
                return false;
            }  
        }

        //檢查子集音樂報表是否已存在確認要刪除重建
        public Boolean checkProgD_DATA_MUSIC_DELETE(string strProgID, int startEpisode, int endEpisode, out string msg)
        {
            WSMAMFunctions wsobj = new WSMAMFunctions();
            string SQLstr = string.Empty;
            string strEpsList = string.Empty;
            List<string> rtnList;
            msg = "";
            try
            {
                SQLstr = "SELECT DISTINCT FNEPISODE FROM PTSPROG.PTSProgram.dbo.TBMUSIC WHERE FSPROGID = '" + strProgID + "' AND FNEPISODE >= " + startEpisode + " AND FNEPISODE <= " + endEpisode + " ORDER BY FNEPISODE";
                rtnList = wsobj.fnGetValue(SQLstr);

                if (rtnList.Count > 0)
                {
                    strEpsList = "";
                    foreach (string s in rtnList)
                    {
                        //strEpsList = strEpsList + ", " + s;

                        if (strEpsList.Trim() == "")
                        {
                            strEpsList = s;
                        }
                        else
                        {
                            strEpsList = strEpsList + ", " + s;
                        }
                    }
                    
                    msg = "下列子集的音樂詞曲著作已經建檔：" + strEpsList + "，確定要清除 (所有音樂詞曲著作將被刪除) 後重新匯入音樂詞曲著作資料嗎？";
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/WSPROG_MYSQL_IMPORT/checkProgD_DATA_MUSIC_DELETE", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR,
                    "查詢外部系統MySQL的CRFILE_PROGB資料表發生錯誤！節目編號：" + strProgID + "，開始集數：" + startEpisode.ToString() + "，結束集數：" + endEpisode.ToString() + "  訊息：" + ex.Message);
                return false;
            }
        }

        //更新節目子集的子集名稱、內容、實際帶長、完成年度、節目規格
        [WebMethod]
        public Boolean updateProgD_DATA(Int64 MySQLProgID, string strProgID, int startEpisode, int endEpisode, string createdUser)
        {
            SqlConnection sqlConn = new SqlConnection(MAM_PTS_DLL.DbAccess.sqlConnStr);
            SqlConnection sqlConn2 = new SqlConnection(MAM_PTS_DLL.DbAccess.sqlConnStr);
            SqlCommand cmd = new SqlCommand("", sqlConn);
            SqlCommand cmd2 = new SqlCommand("", sqlConn2);
            SqlDataReader dr;
            string SQLstr = string.Empty;
            string SQLstrMAM = string.Empty;
            WSMAMFunctions wsobj = new WSMAMFunctions();
            List<string> rtnList;

            try
            {
                sqlConn.Open();
                sqlConn2.Open();
                SQLstr = "SELECT PBSET, PBNAM, PBCON, PBLEN, PBYEAR,PBENO FROM CRFILE_PROGB WHERE PAENO = " + MySQLProgID + " AND PBSET >= " + startEpisode + " AND PBSET <= " + endEpisode + " ORDER BY PBSET";
                SQLstr = "SELECT PBSET, PBNAM, PBCON, PBLEN,PBENO, ISNULL(PBYEAR,'')as PBYEAR FROM openquery(PTS_PROG_MYSQL, '" + SQLstr.Replace("'", "''''") + "' ) ";

                cmd.CommandText = SQLstr;
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    SQLstr = "UPDATE PTSPROG.PTSProgram.dbo.TBPROG_D SET FSPGDNAME = '" + dr["PBNAM"].ToString().Replace("'","''") + "' ";

                    //內容
                    if (Convert.IsDBNull(dr["PBCON"]))
                    {
                        SQLstr = SQLstr + ",  FSCONTENT = NULL";
                    }
                    else
                    {
                        SQLstr = SQLstr + ",  FSCONTENT = '" + dr["PBCON"].ToString().Replace("'", "''")+"'"; //PBNAM改成PBCON
                    }

                    //實際帶長
                    if (Convert.IsDBNull(dr["PBLEN"]))
                    {
                        SQLstr = SQLstr + ",  FNTAPELENGTH = NULL";
                    }
                    else
                    {
                        SQLstr = SQLstr + ",  FNTAPELENGTH = " + dr["PBLEN"].ToString().Trim();
                    }

                    //完成年度
                    if (Convert.IsDBNull(dr["PBCON"]))
                    {
                        SQLstr = SQLstr + ",  FSPRDYEAR = NULL";
                    }
                    else
                    {
                        if (dr["PBYEAR"].ToString().Trim()=="")
                            SQLstr = SQLstr + ",  FSPRDYEAR = NULL";
                        else
                            SQLstr = SQLstr + ",  FSPRDYEAR = '" + dr["PBYEAR"].ToString().Trim() + "'";
                    }

                    //節目規格
                    string strProgSpecPRG = checkProgSpecPGM(MySQLProgID.ToString(), dr["PBENO"].ToString().Trim());

                    if (strProgSpecPRG.Trim() == "")
                    {
                        SQLstr = SQLstr + ",  FSPROGSPEC = NULL";
                    }
                    else
                    {
                        SQLstr = SQLstr + ",  FSPROGSPEC = '" + strProgSpecPRG.Trim() + "'";
                    }

                    SQLstr = SQLstr + ", FSUPDUSER = '01065', FDUPDDATE = Getdate() ";
                    SQLstr = SQLstr + " WHERE FSPROGID = '" + strProgID + "' AND FNEPISODE = " + dr["PBSET"].ToString();

                    //連至MAM系統，更新子集名稱、內容、實際帶長、完成年度、節目規格    

                    string strProgSpecMAM = checkProgSpecMAM(MySQLProgID.ToString(), dr["PBENO"].ToString().Trim());

                    if (Convert.IsDBNull(dr["PBCON"]))
                        SQLstrMAM = "UPDATE MAM.dbo.TBPROG_D SET FSPGDNAME = '" + dr["PBNAM"].ToString().Replace("'", "''") + "',  FSCONTENT = '' , FNTAPELENGTH =" + dr["PBLEN"].ToString().Trim() + ",  FSPRDYEAR ='" + dr["PBYEAR"].ToString().Trim() + "', FSPROGSPEC = '" + strProgSpecMAM + "' , FSUPDUSER = '"+createdUser+"', FDUPDDATE = Getdate()  WHERE FSPROG_ID = '" + strProgID + "' AND FNEPISODE = " + dr["PBSET"].ToString();
                    else
                    {
                        SQLstrMAM = "UPDATE MAM.dbo.TBPROG_D SET FSPGDNAME = '" + dr["PBNAM"].ToString().Replace("'", "''") + "',  FSCONTENT = '" + dr["PBCON"].ToString().Replace("'", "''") + "'  , FNTAPELENGTH =" + dr["PBLEN"].ToString().Trim() + ",  FSPROGSPEC = '" + strProgSpecMAM + "',  FSPRDYEAR ='" + dr["PBYEAR"].ToString().Trim() + "', FSUPDUSER = '" + createdUser + "', FDUPDDATE = Getdate()  WHERE FSPROG_ID = '" + strProgID + "' AND FNEPISODE = " + dr["PBSET"].ToString();
                    }                  

                    cmd2.CommandText = SQLstr;
                    cmd2.ExecuteNonQuery();

                    cmd2.CommandText = SQLstrMAM;
                    cmd2.ExecuteNonQuery();
                }

                dr.Close();
                return true;
            }
            catch (Exception ex)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/WSPROG_MYSQL_IMPORT/updateProgD_DATA", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR,
                    "更新節目子集資料表發生錯誤！MySQL節目編號：" + MySQLProgID.ToString() + "，節目編號：" + strProgID + "，開始集數：" + startEpisode.ToString() + "，結束集數：" + endEpisode.ToString() + "  訊息：" + ex.Message);
                return false;
            }
            finally
            {
                sqlConn.Close();
                sqlConn2.Close();
            }
        }

        //刪除子集的工作人員
        public Boolean deleteProgMen_DATA(Int64 MySQLProgID, string strProgID, int startEpisode, int endEpisode)
        {
            SqlConnection sqlConn = new SqlConnection(MAM_PTS_DLL.DbAccess.sqlConnStr);
            SqlConnection sqlConn2 = new SqlConnection(MAM_PTS_DLL.DbAccess.sqlConnStr);
            SqlCommand cmd = new SqlCommand("", sqlConn);
            SqlCommand cmd2 = new SqlCommand("", sqlConn2);
            SqlDataReader dr;               
            string SQLstr = string.Empty;
            string SQLstrMAM = string.Empty;
            WSMAMFunctions wsobj = new WSMAMFunctions();

            try
            {
                sqlConn.Open();
                sqlConn2.Open();
                SQLstr = "SELECT PBSET, PBNAM, PBCON, PBLEN, PBYEAR,PBENO FROM CRFILE_PROGB WHERE PAENO = " + MySQLProgID + " AND PBSET >= " + startEpisode + " AND PBSET <= " + endEpisode + " ORDER BY PBSET";
                SQLstr = "SELECT PBSET, PBNAM, PBCON, PBLEN,PBENO, ISNULL(PBYEAR,'')as PBYEAR FROM openquery(PTS_PROG_MYSQL, '" + SQLstr.Replace("'", "''''") + "' ) ";

                cmd.CommandText = SQLstr;
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    //刪除節目管理系統
                    SQLstr = "DELETE FROM PTSPROG.PTSProgram.dbo.TBPROGMEN WHERE FSPROGID = '" + strProgID + "' AND FNEPISODE = " + dr["PBSET"].ToString();

                    //刪除MAM系統  
                    SQLstrMAM = "DELETE FROM MAM.dbo.TBPROGMEN WHERE FSPROG_ID = '" + strProgID + "' AND FNEPISODE = " + dr["PBSET"].ToString();

                    cmd2.CommandText = SQLstr;
                    cmd2.ExecuteNonQuery();

                    cmd2.CommandText = SQLstrMAM;
                    cmd2.ExecuteNonQuery();    
                }

                dr.Close();
                return true;
            }
            catch (Exception ex)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/WSPROG_MYSQL_IMPORT/deleteProgMen_DATA", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR,
                    "刪除節目工作人員資料表發生錯誤！MySQL節目編號：" + MySQLProgID.ToString() + "，節目編號：" + strProgID + "，開始集數：" + startEpisode.ToString() + "，結束集數：" + endEpisode.ToString() + "  訊息：" + ex.Message);
                return false;
            }
            finally
            {
                sqlConn.Close();
            }
        }

        //刪除子集的音樂資料
        public Boolean deleteProgMusic_DATA(Int64 MySQLProgID, string strProgID, int startEpisode, int endEpisode)
        {
            SqlConnection sqlConn = new SqlConnection(MAM_PTS_DLL.DbAccess.sqlConnStr);
            SqlConnection sqlConn2 = new SqlConnection(MAM_PTS_DLL.DbAccess.sqlConnStr);
            SqlCommand cmd = new SqlCommand("", sqlConn);
            SqlCommand cmd2 = new SqlCommand("", sqlConn2);
            SqlDataReader dr;
            string SQLstr = string.Empty;
            string SQLstrMAM = string.Empty;
            WSMAMFunctions wsobj = new WSMAMFunctions();

            try
            {
                sqlConn.Open();
                sqlConn2.Open();
                SQLstr = "SELECT PBSET, PBNAM, PBCON, PBLEN, PBYEAR,PBENO FROM CRFILE_PROGB WHERE PAENO = " + MySQLProgID + " AND PBSET >= " + startEpisode + " AND PBSET <= " + endEpisode + " ORDER BY PBSET";
                SQLstr = "SELECT PBSET, PBNAM, PBCON, PBLEN,PBENO, ISNULL(PBYEAR,'')as PBYEAR FROM openquery(PTS_PROG_MYSQL, '" + SQLstr.Replace("'", "''''") + "' ) ";

                cmd.CommandText = SQLstr;
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    //刪除子集的音樂著作
                    SQLstr = "DELETE FROM PTSPROG.PTSProgram.dbo.TBMUSIC WHERE FSPROGID = '" + strProgID + "' AND FNEPISODE = " + dr["PBSET"].ToString();
                    cmd2.CommandText = SQLstr;
                    cmd2.ExecuteNonQuery();  
                }

                dr.Close();
                return true;
            }
            catch (Exception ex)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/WSPROG_MYSQL_IMPORT/deleteProgMusic_DATA", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR,
                    "刪除節目音樂資料表發生錯誤！MySQL節目編號：" + MySQLProgID.ToString() + "，節目編號：" + strProgID + "，開始集數：" + startEpisode.ToString() + "，結束集數：" + endEpisode.ToString() + "  訊息：" + ex.Message);
                return false;
            }
            finally
            {
                sqlConn.Close();
            }
        }

        //新增音樂著作：必須音樂著作審核通過，全部刪除後重新建立
        public Boolean AddProgMusic_DATA(Int64 MySQLProgID, string strProgID, int startEpisode, int endEpisode)
        {
            SqlConnection sqlConn = new SqlConnection(MAM_PTS_DLL.DbAccess.sqlConnStr);
            SqlConnection sqlConn2 = new SqlConnection(MAM_PTS_DLL.DbAccess.sqlConnStr);
            SqlCommand cmd = new SqlCommand("", sqlConn);
            SqlCommand cmd2 = new SqlCommand("", sqlConn2);
            string SQLstr = string.Empty;
            WSMAMFunctions wsobj = new WSMAMFunctions();
            SqlDataReader dr;
            List<string> rtnList;

            try
            {
                sqlConn.Open();
                sqlConn2.Open();
                string strValStr = string.Empty;
                string strSeqNo = string.Empty;

                SQLstr = "SELECT M.*, PBSET FROM CRFILE_MUSIC M INNER JOIN CRFILE_PROGB E ON M.PAENO = E.PAENO AND M.PBENO = E.PBENO WHERE M.PAENO = " + MySQLProgID + " AND PBSET >= " + startEpisode + " AND PBSET <= " + endEpisode + " ORDER BY PBSET";
                SQLstr = "SELECT * FROM OPENQUERY(PTS_PROG_MYSQL, '" + SQLstr.Replace("'", "''''") + "' ) ";
                cmd.CommandText = SQLstr;
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    strSeqNo = "";
                    SQLstr = "SELECT ISNULL(MAX(FNSEQNO)+1,1) FROM PTSPROG.PTSProgram.dbo.TBMUSIC WHERE FSPROGID = '" + strProgID + "' AND FNEPISODE = " + dr["PBSET"].ToString();

                    rtnList = wsobj.fnGetValue(SQLstr);

                    if (rtnList.Count > 0)
                        strSeqNo = rtnList[0];

                    //檢查曲目是否空白
                    strValStr = dr["MSNAM"].ToString().Trim().Replace("\r", "");
                    strValStr = strValStr.Trim().Replace("\n", "");

                    if (strValStr != string.Empty)
                    {                    
                        SQLstr = "INSERT INTO PTSPROG.PTSProgram.dbo.TBMUSIC( FSPROGID, FNEPISODE, FNSEQNO, ";
                        SQLstr = SQLstr + "FSMUSICNAME, FSCOMPOSER, FSSONGWRITING, FSARRANGER, ";
                        SQLstr = SQLstr + "FSSINGER, FSCOMPANY, FSPRODUCER, FSPRODUCTID, FSSOUNDMAKER, FNUSETIMES, FNUSELENGTH, ";
                        SQLstr = SQLstr + "FSMEMO, FCBACKGROUND, FCTITLETHEME, FCENDINGTHEME, FCPERFORMANCE, ";
                        SQLstr = SQLstr + "FCLIVE, FCRECORD, FCMTV, FCFREESHOW, FCPUBSHOW, FCPUBTRANSMIT, ";
                        SQLstr = SQLstr + "FCPERIPHERAL, FCPUBLISH, FCREPRD, FCREAUTH, ";
                        SQLstr = SQLstr + "FSCRTUSER, FDCRTDATE) ";
                        SQLstr = SQLstr + " VALUES ('" + strProgID + "'";
                        SQLstr = SQLstr + ", " + dr["PBSET"].ToString();
                        SQLstr = SQLstr + ",'" + strSeqNo + "'";
                        SQLstr = SQLstr + ",'" + strValStr.Replace("'", "''") + "'";

                        if (Convert.IsDBNull(dr["MSMAA"]))
                            SQLstr = SQLstr + ", NULL";                       
                        else
                            SQLstr = SQLstr + ",'" + dr["MSMAA"].ToString().Replace("'", "''") + "'";                      

                        if (Convert.IsDBNull(dr["MSMAB"]))
                            SQLstr = SQLstr + ", NULL";                     
                        else
                            SQLstr = SQLstr + ",'" + dr["MSMAB"].ToString().Replace("'", "''") + "'";                       

                        if (Convert.IsDBNull(dr["MSMAC"]))
                            SQLstr = SQLstr + ", NULL";                       
                        else
                            SQLstr = SQLstr + ",'" + dr["MSMAC"].ToString().Replace("'", "''") + "'";
                       

                        if (Convert.IsDBNull(dr["MSMAD"]))
                            SQLstr = SQLstr + ", NULL";                       
                        else
                            SQLstr = SQLstr + ",'" + dr["MSMAD"].ToString().Replace("'", "''") + "'";                       

                        if (Convert.IsDBNull(dr["MSMAE"]))                      
                            SQLstr = SQLstr + ", NULL";                       
                        else
                            SQLstr = SQLstr + ",'" + dr["MSMAE"].ToString().Replace("'", "''") + "'";   

                        if (Convert.IsDBNull(dr["MSMAG"]))                      
                            SQLstr = SQLstr + ", NULL";                       
                        else                       
                            SQLstr = SQLstr + ",'" + dr["MSMAG"].ToString().Replace("'", "''") + "'";

                        if (Convert.IsDBNull(dr["MSGNO"]))                     
                            SQLstr = SQLstr + ", NULL";                     
                        else                    
                            SQLstr = SQLstr + ",'" + dr["MSGNO"].ToString().Replace("'", "''") + "'";

                        if (Convert.IsDBNull(dr["MSMAF"]))                      
                            SQLstr = SQLstr + ", NULL";                       
                        else                     
                            SQLstr = SQLstr + ",'" + dr["MSMAF"].ToString().Replace("'", "''") + "'";                       
                        
                        SQLstr = SQLstr + "," + dr["MSTME"].ToString().Replace("'", "''");
                        SQLstr = SQLstr + "," + dr["MSLEN"].ToString().Replace("'", "''");

                        if (Convert.IsDBNull(dr["MSMRK"]))                   
                            SQLstr = SQLstr + ", NULL";
                        else
                            SQLstr = SQLstr + ",'" + dr["MSMRK"].ToString().Replace("'", "''") + "'";

                        SQLstr = SQLstr + (Convert.ToInt32(dr["MSKDA"]) == 1 ? ", 1" : ", 0");
                        SQLstr = SQLstr + (Convert.ToInt32(dr["MSKDB"]) == 1 ? ", 1" : ", 0");
                        SQLstr = SQLstr + (Convert.ToInt32(dr["MSKDC"]) == 1 ? ", 1" : ", 0");
                        SQLstr = SQLstr + (Convert.ToInt32(dr["MSKDD"]) == 1 ? ", 1" : ", 0");
                        SQLstr = SQLstr + (Convert.ToInt32(dr["MSKAA"]) == 1 ? ", 1" : ", 0");
                        SQLstr = SQLstr + (Convert.ToInt32(dr["MSKAB"]) == 1 ? ", 1" : ", 0");
                        SQLstr = SQLstr + (Convert.ToInt32(dr["MSKAC"]) == 1 ? ", 1" : ", 0");
                        SQLstr = SQLstr + (Convert.ToInt32(dr["MSKBA"]) == 1 ? ", 1" : ", 0");
                        SQLstr = SQLstr + (Convert.ToInt32(dr["MSKBB"]) == 1 ? ", 1" : ", 0");
                        SQLstr = SQLstr + (Convert.ToInt32(dr["MSKBC"]) == 1 ? ", 1" : ", 0");
                        SQLstr = SQLstr + (Convert.ToInt32(dr["MSKBD"]) == 1 ? ", 1" : ", 0");
                        SQLstr = SQLstr + (Convert.ToInt32(dr["MSKBE"]) == 1 ? ", 1" : ", 0");
                        SQLstr = SQLstr + (Convert.ToInt32(dr["MSKBF"]) == 1 ? ", 1" : ", 0");

                        if (Convert.ToInt32(dr["MSKBA"]) == 1)
                        {
                            if (Convert.ToInt32(dr["MSKCA"]) == 1)                           
                                SQLstr = SQLstr + ", 'F'";                        
                            else if (Convert.ToInt32(dr["MSKCB"]) == 1)                           
                                SQLstr = SQLstr + ", 'Y'";                          
                            else if (Convert.ToInt32(dr["MSKCC"]) == 1)                         
                                SQLstr = SQLstr + ", 'N'";                          
                            else                           
                                SQLstr = SQLstr + ", ''";  
                        }
                        else                       
                            SQLstr = SQLstr + ", ''";
                     
                        SQLstr = SQLstr + ", '01065',Getdate())";

                        cmd2.CommandText = SQLstr;
                        cmd2.ExecuteNonQuery();
                    }
                }
                dr.Close(); 

                return true;
            }
            catch (Exception ex)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/WSPROG_MYSQL_IMPORT/AddProgMusic_DATA", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR,
                    "新增節目音樂資料表發生錯誤！MySQL節目編號：" + MySQLProgID.ToString() + "，開始集數：" + startEpisode.ToString() + "，結束集數：" + endEpisode.ToString() + "  訊息：" + ex.Message);
                return false;
            }
            finally
            {
                sqlConn.Close();
                sqlConn2.Close();
            }
        }

        //新增子集工作人員：必須工作人員審核通過，全部刪除後重新建立
        public Boolean AddProgMen_DATA(Int64 MySQLProgID, string strProgID, int startEpisode, int endEpisode, string strWhosInCharge, string createdUser)
        {
            SqlConnection sqlConn = new SqlConnection(MAM_PTS_DLL.DbAccess.sqlConnStr);
            SqlConnection sqlConn2 = new SqlConnection(MAM_PTS_DLL.DbAccess.sqlConnStr);
            SqlConnection sqlConn3 = new SqlConnection(MAM_PTS_DLL.DbAccess.sqlConnStr);
            SqlCommand cmd = new SqlCommand("", sqlConn);
            SqlCommand cmd2 = new SqlCommand("", sqlConn2);
            SqlCommand cmd3 = new SqlCommand("", sqlConn3);
            string SQLstr = string.Empty;
            string SQLstrMAM = string.Empty;
            string strValStr = string.Empty;
            WSMAMFunctions wsobj = new WSMAMFunctions();
            SqlDataReader dr;
            SqlDataReader dr2;
            SqlDataReader dr3;
            List<string> rtnList;

            try
            {
                sqlConn.Open();
                sqlConn2.Open();
                sqlConn3.Open();

                SQLstr = "SELECT S.*, PBSET FROM CRFILE_STAFF S INNER JOIN CRFILE_PROGB E ON S.PAENO = E.PAENO";
                SQLstr = SQLstr + " AND S.PBENO = E.PBENO";
                SQLstr = SQLstr + " WHERE S.PAENO = " + MySQLProgID + " AND PBSET >= " + startEpisode;
                SQLstr = SQLstr + " AND PBSET <= " + endEpisode;
                SQLstr = SQLstr + " ORDER BY PBSET";

                SQLstr = "SELECT * FROM OPENQUERY(PTS_PROG_MYSQL, '" + SQLstr.Replace("'", "''''") + "' ) ";
                cmd.CommandText = SQLstr;
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    //檢查公司/姓名是否空白
                    strValStr = dr["STNAM"].ToString().Trim().Replace("\r", "");
                    strValStr = strValStr.Trim().Replace("\n", "");
                    if (strValStr != string.Empty)
                    {
                        //檢查頭銜代碼
                        SQLstr = "SELECT FSTITLEID FROM PTSPROG.PTSProgram.dbo.TBZTITLE ";
                        SQLstr = SQLstr + " WHERE FSTITLEID = '" + dr["STITL"].ToString().Replace("'", "''") + "'";
                        rtnList = wsobj.fnGetValue(SQLstr);

                        if (rtnList.Count > 0)
                        {
                            SQLstr = "INSERT INTO PTSPROG.PTSProgram.dbo.TBPROGMEN(FSPROGID, FNEPISODE, FSTITLEID,";
                            SQLstr = SQLstr + " FSNAME, FSMEMO, FSCRTUSER, FDCRTDATE) ";
                            SQLstr = SQLstr + " VALUES ('" + strProgID + "'";
                            SQLstr = SQLstr + "," + dr["PBSET"].ToString();
                            SQLstr = SQLstr + ",'" + dr["STITL"].ToString() + "'";
                            SQLstr = SQLstr + ",'" + strValStr.Replace("'", "''") + "'";

                            if (Convert.IsDBNull(dr["STMRK"]))
                                SQLstr = SQLstr + ", NULL";                          
                            else
                                SQLstr = SQLstr + ",'" + dr["STMRK"].ToString().Replace("'", "''") + "'";

                            SQLstr = SQLstr + ",'01065',Getdate())";

                            //連結MAM系統，新增相關人員資料
                            SQLstrMAM = "INSERT INTO MAM.dbo.TBPROGMEN(FSPROG_ID, FNEPISODE, FSTITLEID,";
                            SQLstrMAM = SQLstrMAM + "FSEMAIL, FSSTAFFID, FSNAME, FSMEMO, FSCREATED_BY, FDCREATED_DATE, FSUPDATED_BY, FDUPDATED_DATE) ";
                            SQLstrMAM = SQLstrMAM + " VALUES ('" + strProgID + "'";
                            SQLstrMAM = SQLstrMAM + "," + dr["PBSET"].ToString();
                            SQLstrMAM = SQLstrMAM + ",'" + dr["STITL"].ToString() + "','',''";
                            SQLstrMAM = SQLstrMAM + ",'" + strValStr.Replace("'", "''") + "'";

                            if (Convert.IsDBNull(dr["STMRK"]))
                                SQLstrMAM = SQLstrMAM + ", ''";
                            else
                                SQLstrMAM = SQLstrMAM + ",'" + dr["STMRK"].ToString().Replace("'", "''") + "'";

                            SQLstrMAM = SQLstrMAM + ",'" + createdUser + "',Getdate(),'','1900/1/1')";//20130916 'mam'修改成當前登入者

                            cmd2.CommandText = SQLstr;
                            cmd2.ExecuteNonQuery();

                            cmd2.CommandText = SQLstrMAM;
                            cmd2.ExecuteNonQuery();
                        }
                    }
                }
                dr.Close();

                //新增/更改節目工作人員
                SQLstr = "SELECT * FROM CRFILE_STAFF WHERE PAENO = " + MySQLProgID + " AND PBENO = 0";
                SQLstr = "SELECT * FROM OPENQUERY(PTS_PROG_MYSQL, '" + SQLstr.Replace("'", "''''") + "' )";

                cmd.CommandText = SQLstr;
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    //檢查公司/姓名是否空白
                    strValStr = dr["STNAM"].ToString().Replace("\r", "");
                    strValStr = dr["STNAM"].ToString().Replace("\n", "");
                    if (strValStr != "")
                    {
                        //檢查節目工作人員是否已建檔
                        SQLstr = "SELECT FSTITLEID, FSMEMO FROM PTSPROG.PTSProgram.dbo.TBPROGMEN WHERE FSPROGID = '" + strProgID + "' AND FNEPISODE IS NULL ";
                        SQLstr = SQLstr + " AND FSTITLEID = '" + dr["STITL"].ToString().Replace("'", "''''") + "' ";
                        SQLstr = SQLstr + "AND FSNAME = '" + strValStr.Replace("'", "''''") + "' ";
                                                
                        cmd3.CommandText = SQLstr;
                        dr3 = cmd3.ExecuteReader();

                        if (!dr3.Read())
                        {
                            //檢查頭銜代碼
                            SQLstr = "SELECT FSTITLEID FROM PTSPROG.PTSProgram.dbo.TBZTITLE WHERE FSTITLEID = '" + dr["STITL"].ToString() + "'";
                            rtnList = wsobj.fnGetValue(SQLstr);
                            if (rtnList.Count > 0)
                            {
                                //開始新增節目工作人員
                                SQLstr = "INSERT INTO PTSPROG.PTSProgram.dbo.TBPROGMEN(FSPROGID, FNEPISODE, FSTITLEID,";
                                SQLstr = SQLstr + " FSNAME, FSMEMO, FSCRTUSER, FDCRTDATE) ";
                                SQLstr = SQLstr + " VALUES ('" + strProgID + "', NULL ";
                                SQLstr = SQLstr + ",'" + dr["STITL"] + "'";
                                SQLstr = SQLstr + ",'" + strValStr.Replace("'", "''") + "'";

                                if (Convert.IsDBNull(dr["STMRK"]))
                                    SQLstr = SQLstr + ", NULL";
                                else
                                    SQLstr = SQLstr + ",'" + dr["STMRK"].ToString().Replace("'", "''") + "'";

                                SQLstr = SQLstr + ",'01065',Getdate())";

                                //連結MAM系統，新增相關人員資料
                                SQLstrMAM = "INSERT INTO MAM.dbo.TBPROGMEN(FSPROG_ID, FNEPISODE, FSTITLEID,";
                                SQLstrMAM = SQLstrMAM + "FSEMAIL, FSSTAFFID, FSNAME, FSMEMO, FSCREATED_BY, FDCREATED_DATE, FSUPDATED_BY, FDUPDATED_DATE) ";
                                SQLstrMAM = SQLstrMAM + " VALUES ('" + strProgID + "', '0' ";
                                SQLstrMAM = SQLstrMAM + ",'" + dr["STITL"] + "','',''";
                                SQLstrMAM = SQLstrMAM + ",'" + strValStr.Replace("'", "''") + "'";

                                if (Convert.IsDBNull(dr["STMRK"]))
                                    SQLstrMAM = SQLstrMAM + ", ''";
                                else
                                    SQLstrMAM = SQLstrMAM + ",'" + dr["STMRK"].ToString().Replace("'", "''") + "'";

                                SQLstrMAM = SQLstrMAM + ",'" + createdUser + "',Getdate(),'','1900/1/1')";//20130916 'mam'修改成當前登入者

                                cmd2.CommandText = SQLstr;
                                cmd2.ExecuteNonQuery();

                                cmd2.CommandText = SQLstrMAM;
                                cmd2.ExecuteNonQuery();
                            }
                        }

                        else
                        {
                            Boolean bInMod = false;

                            if (!Convert.IsDBNull(dr3["FSMEMO"]) && !Convert.IsDBNull(dr["STMRK"]))
                            {
                                if (dr3["FSMEMO"] != dr["STMRK"])
                                    bInMod = true;
                            }
                            else if (Convert.IsDBNull(dr3["FSMEMO"]) && Convert.IsDBNull(dr["STMRK"]))
                            {
                            }
                            else
                            {
                                bInMod = true;
                            }

                            //If Not IsNull(rs.Fields("FSMEMO")) And Not IsNull(adcImp.Recordset.Fields("STMRK")) Then
                            //If rs.Fields("FSMEMO") <> adcImp.Recordset.Fields("STMRK") Then blnMod = True
                            //ElseIf IsNull(rs.Fields("FSMEMO")) And IsNull(adcImp.Recordset.Fields("STMRK")) Then
                            //Else
                            //    blnMod = True
                            //End If
                            if (bInMod)
                            {
                                SQLstr = "UPDATE PTSPROG.PTSProgram.dbo.TBPROGMEN SET FSNAME = '" + dr["STNAM"].ToString().Replace("'", "''") + "'";
                                if (Convert.IsDBNull(dr["STMRK"]))
                                {
                                    SQLstr = SQLstr + ", FSMEMO = NULL";
                                }
                                else
                                {
                                    SQLstr = SQLstr + ", FSMEMO = '" + dr["STMRK"].ToString().Replace("'", "''") + "'";
                                }

                                SQLstr = SQLstr + ", FSUPDUSER = '01065'";
                                SQLstr = SQLstr + ", FDUPDDATE = Getdate() ";
                                SQLstr = SQLstr + "WHERE FSPROGID = '" + strProgID + "' ";

                                SQLstr = SQLstr + "AND FNEPISODE IS NULL ";
                                SQLstr = SQLstr + "AND FSTITLEID = '" + dr["STITL"].ToString().Replace("'", "''") + "' ";
                                SQLstr = SQLstr + "AND FSNAME = '" + strValStr.Replace("'", "''") + "' ";

                                //連結MAM系統，修改相關人員
                                SQLstrMAM = "UPDATE MAM.dbo.TBPROGMEN SET FSNAME = '" + dr["STNAM"].ToString().Replace("'", "''") + "'";
                                if (Convert.IsDBNull(dr["STMRK"]))
                                    SQLstrMAM = SQLstrMAM + ", FSMEMO = ''";
                                else
                                    SQLstrMAM = SQLstrMAM + ", FSMEMO = '" + dr["STMRK"].ToString().Replace("'", "''") + "'";

                                SQLstrMAM = SQLstrMAM + ", FSUPDATED_BY = '" + createdUser + "'";
                                SQLstrMAM = SQLstrMAM + ", FDUPDATED_DATE = Getdate() ";
                                SQLstrMAM = SQLstrMAM + "WHERE FSPROG_ID = '" + strProgID + "' ";

                                SQLstrMAM = SQLstrMAM + "AND FNEPISODE = 0 ";
                                SQLstrMAM = SQLstrMAM + "AND FSTITLEID = '" + dr["STITL"].ToString().Replace("'", "''") + "' ";
                                SQLstrMAM = SQLstrMAM + "AND FSNAME = '" + strValStr.Replace("'", "''") + "' ";

                                cmd2.CommandText = SQLstr;
                                cmd2.ExecuteNonQuery();

                                cmd2.CommandText = SQLstrMAM;
                                cmd2.ExecuteNonQuery();
                            }
                        }
                        dr3.Close();                      
                    }
                }

                //如果前面的查詢有查到委製協調的話，要在工作人員資料表裡新增該委製協調
                if (strWhosInCharge != string.Empty)
                {
                    // 記錄委製協調
                    SQLstr = "SELECT FSTITLEID, FSMEMO FROM PTSPROG.PTSProgram.dbo.TBPROGMEN ";
                    SQLstr = SQLstr + "WHERE FSPROGID = '" + strProgID + "' ";
                    SQLstr = SQLstr + "AND FNEPISODE IS NULL ";
                    SQLstr = SQLstr + "AND FSTITLEID = '06' ";
                    SQLstr = SQLstr + "AND FSNAME = '" + strWhosInCharge.Replace("'", "''") + "' ";

                    rtnList = wsobj.fnGetValue(SQLstr);

                    if (rtnList.Count == 0)
                    {
                        SQLstr = "INSERT INTO PTSPROG.PTSProgram.dbo.TBPROGMEN(FSPROGID, FNEPISODE, FSTITLEID,";
                        SQLstr = SQLstr + " FSNAME, FSMEMO, FSCRTUSER, FDCRTDATE) ";
                        SQLstr = SQLstr + " VALUES ('" + strProgID + "', NULL ,'06'";
                        SQLstr = SQLstr + ",'" + strWhosInCharge.Replace("'", "''") + "', NULL";
                        SQLstr = SQLstr + ",'01065',Getdate())";

                        //連結MAM系統，新增節目相關人員
                        SQLstrMAM = "INSERT INTO MAM.dbo.TBPROGMEN(FSPROGID, FNEPISODE, FSTITLEID,";
                        SQLstrMAM = SQLstrMAM + " FSEMAIL, FSSTAFFID, FSNAME, FSMEMO, FSCREATED_BY, FDCREATED_DATE, FSUPDATED_BY, FDUPDATED_DATE) ";
                        SQLstrMAM = SQLstrMAM + " VALUES ('" + strProgID + "', '0' ,'06','',''";
                        SQLstrMAM = SQLstrMAM + ",'" + strWhosInCharge.Replace("'", "''") + "', ''";
                        SQLstrMAM = SQLstrMAM + ",'" + createdUser + "',Getdate(),'','1900/1/1')";//20130916 'mam'修改成當前登入者

                        cmd2.CommandText = SQLstr;
                        cmd2.ExecuteNonQuery();

                        cmd2.CommandText = SQLstrMAM;
                        cmd2.ExecuteNonQuery();
                    }
                }

                sqlConn2.Close();   
                              
                return true;
            }
            catch (Exception ex)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/WSPROG_MYSQL_IMPORT/AddProgMen_DATA", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR,
                    "新增節目人員資料表發生錯誤！MySQL節目編號：" + MySQLProgID.ToString() + "，開始集數：" + startEpisode.ToString() + "，結束集數：" + endEpisode.ToString() + "  訊息：" + ex.Message);
                return false;
            }
            finally
            {
                sqlConn.Close();
                sqlConn2.Close();
                sqlConn3.Close();
            }
        }

        /// <summary>
        /// 節目管理系統外部MySQL 
        /// return "true" 並且回傳的errString訊息為空字串表示處理完畢
        /// return "true" 且回傳的errString 不為空, 表示出現需要confirm 的狀態, 前端請pass iforceUpdate + 1 的值以便進入下一個檢查點.
        /// return "false" 表示出現錯誤, 錯誤訊息將帶入errString 中.
        /// </summary>
        /// <param name="errString">回傳的訊息</param>
        /// <param name="fsProgId">欲匯入的節目ID</param>
        /// <param name="pgmName">外部節目管理系統的節目名稱</param>
        /// <param name="startEpisode">外部節目管理系統的節目起始集數</param>
        /// <param name="endEpisode">外部節目管理系統的節目結束集數</param>
        /// <param name="iforceUpdate">0(表示目前要pass 第幾個confirm動作)</param>
        /// <returns>true/false</returns>
        [WebMethod]
        public Boolean chkAndImportFromMySQL(out string errString, string fsProgId, string pgmName, int startEpisode, int endEpisode, int iforceUpdate, string createdUser)
        {
            Boolean brtn = true;
            errString = string.Empty;
            
            string strPrgName = pgmName.Trim(); 
            SqlConnection sqlConn = new SqlConnection( MAM_PTS_DLL.DbAccess.sqlConnStr);
            SqlCommand cmd = new SqlCommand("", sqlConn);

            SqlConnection sqlConn2 = new SqlConnection(MAM_PTS_DLL.DbAccess.sqlConnStr);
            SqlCommand cmd2 = new SqlCommand("", sqlConn2);

            SqlConnection sqlConn3 = new SqlConnection(MAM_PTS_DLL.DbAccess.sqlConnStr);
            SqlCommand cmd3 = new SqlCommand("", sqlConn3);

            SqlDataReader dr, dr2, dr3  ;
            // 記錄製作公司?、委製協調
            long lPrgID;
            string strCompany = string.Empty;
            string strWhosInCharge = string.Empty;
            string BNENO;
            string SQLstr = string.Empty; 
            WSMAMFunctions wsobj = new WSMAMFunctions();
            List<string> rtnList;
            string SQLstrMAM = string.Empty;    //MAM系統的SQL語法

            sqlConn2.Open();

            try
            {
                sqlConn.Open();                         

                cmd.CommandText = "SELECT count(1) recNum FROM OPENQUERY(PTS_PROG_MYSQL, 'SELECT PAENO, PACOM, BNENO FROM CRFILE_PROGA WHERE PANAM = ''" + strPrgName.Replace("'", "''''") + "'' ')  ";

                dr = cmd.ExecuteReader();

                int recNum = 0;
                if (dr.Read())
                {
                    recNum = Convert.ToInt32(dr["recNum"]);                  
                }                

                if (recNum > 1)
                {
                    brtn = false;
                    errString = "外部網路輸入的資料中不只一個節目名稱為【" + strPrgName + "】！";
                    dr.Close();
                    sqlConn.Close();
                    return brtn;
                }

                dr.Close();

                cmd.CommandText = "SELECT PAENO, ISNULL(PACOM, '') PACOM, ISNULL(BNENO, '')BNENO FROM OPENQUERY(PTS_PROG_MYSQL, 'SELECT PAENO, PACOM, BNENO FROM CRFILE_PROGA WHERE PANAM = ''" + strPrgName.Replace("'", "''''") + "'' ')  ";
                
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {       
                    lPrgID =Convert.ToInt64( dr["PAENO"]);  
                    strCompany = dr["PACOM"].ToString();
                    BNENO = dr["BNENO"].ToString();

                    if (BNENO != string.Empty)
                    {
                        rtnList = wsobj.fnGetValue("SELECT BNNAM FROM OPENQUERY(PTS_PROG_MYSQL, 'SELECT BNNAM FROM CRFILE_SYSMAN WHERE BNENO = ''" + BNENO + "'' ')");
                        if (rtnList.Count > 0)
                        {
                            strWhosInCharge = rtnList[0]; 
                        }                                                
                    }

                    //檢查子集是否存在
                    string strEpsList = string.Empty;

                    SQLstr = "SELECT * FROM OPENQUERY(PTS_PROG_MYSQL, 'SELECT PBSET FROM CRFILE_PROGB WHERE PAENO = " + lPrgID + " AND PBSET >= " + startEpisode + " AND PBSET <= " + endEpisode + " ORDER BY PBSET') ";

                    rtnList = wsobj.fnGetValue(SQLstr);

                    if (rtnList.Count == 0)
                    {
                        brtn = false;
                        errString = "外部網路輸入的資料沒有該節目【第" + startEpisode + "集 - 第" + endEpisode + "集】任何一集的子集資料，無法匯入！"; 
                        dr.Close() ; 
                        sqlConn.Close() ; 
                        return brtn ; 
                    }
                    else if (rtnList.Count != (endEpisode - startEpisode + 1) ) 
                    {                        
                        int intEpsNo = 0 ;
                        for(intEpsNo = startEpisode ; intEpsNo <= endEpisode ; intEpsNo++) 
                        {
                            Boolean b = false ; 

                            foreach( string s in rtnList) 
                            {
                                if (Convert.ToInt32(s) == intEpsNo) 
                                {
                                    b = true ; 
                                    break ; 
                                }
                            }   
            
                            if (!b) {
                                //strEpsList = strEpsList + ", " + intEpsNo;

                                if (strEpsList.Trim() == "")
                                {
                                    strEpsList = intEpsNo.ToString() ;
                                }
                                else
                                {
                                    strEpsList = strEpsList + ", " +  intEpsNo;
                                }
                            }
                        }

                        if(strEpsList.Length > 0 ) {
                            brtn = false; 

                            errString = "外部網路輸入的資料沒有該節目這幾集【第 " + strEpsList + " 集】的子集資料，無法匯入！"; 
                            dr.Close() ; 
                            sqlConn.Close() ; 
                            return brtn ; 
                        } 
                    }
                    else 
                    {
                         strEpsList = "";
                         List<string> rtnList2;
                   
                         foreach (string s in rtnList)
                         {
                             SQLstr = "select * from PTSPROG.PTSProgram.dbo.TBPROG_D  WHERE FSPROGID = '" + fsProgId + "' AND FNEPISODE = " + s ;
                             rtnList2 = wsobj.fnGetValue(SQLstr);
                             if (rtnList2.Count == 0)
                             {
                                 //strEpsList = strEpsList + ", " + s;

                                 if (strEpsList.Trim() == "")
                                 {
                                     strEpsList = s;
                                 }
                                 else
                                 {
                                     strEpsList = strEpsList + ", " + s;
                                 }
                             }

                         }

                         if (strEpsList.Length > 0)
                         {
                             brtn = false;

                             errString = "請先新增下列子集後再執行整批匯入：" + strEpsList + "。";
                             dr.Close();
                             sqlConn.Close();
                             return brtn;
                         }
                    }


                    //檢查子集音樂報表是否審核通過
                    SQLstr = "SELECT PBSET FROM CRFILE_PROGB WHERE PAENO = " + lPrgID + " AND PBSET >= " + startEpisode + " AND PBSET <= " + endEpisode + " AND PBCBB < 1 ORDER BY PBSET";

                    SQLstr = "SELECT * FROM openquery(PTS_PROG_MYSQL, '" + SQLstr.Replace("'", "''''") + "') ";

                    rtnList = wsobj.fnGetValue(SQLstr);

                    if (rtnList.Count > 0)
                    {
                        strEpsList = "";
                        foreach (string s in rtnList)
                        {
                            //strEpsList = strEpsList + ", " + s;

                            if (strEpsList.Trim() == "")
                            {
                                strEpsList = s;
                            }
                            else
                            {
                                strEpsList = strEpsList + ", " + s;
                            }
                        }

                        brtn = false;
                        errString = "下列子集的音樂報表尚未審核通過：" + strEpsList + "，無法執行整批匯入！";
                        dr.Close();
                        sqlConn.Close();
                        return brtn;
                    }

                    //檢查子集工作人員是否審核通過
                    SQLstr = "SELECT PBSET FROM CRFILE_PROGB WHERE PAENO = " + lPrgID + " AND PBSET >= " + startEpisode + " AND PBSET <= " + endEpisode + " AND PBCBA < 1 ORDER BY PBSET";

                    SQLstr = "SELECT * FROM openquery(PTS_PROG_MYSQL, '" + SQLstr.Replace("'", "''''") + "' ) ";
                    rtnList = wsobj.fnGetValue(SQLstr);

                    if (rtnList.Count > 0)
                    {
                        strEpsList = "";
                        foreach (string s in rtnList)
                        {
                            if (strEpsList.Trim() == "")
                            {
                                strEpsList = s;
                            }
                            else
                            { 
                                strEpsList = strEpsList + ", " + s;
                            }
                        }

                        brtn = false;
                        errString = "下列子集的工作人員報表尚未審核通過：" + strEpsList + "，無法執行整批匯入！";
                        dr.Close();
                        sqlConn.Close();
                        return brtn;
                    }
                    
                    //檢查子集工作人員是否已存在確認要刪除重建
                    if (iforceUpdate == 0 ) {

                        SQLstr = "SELECT DISTINCT FNEPISODE FROM PTSPROG.PTSProgram.dbo.TBPROGMEN WHERE FSPROGID = '" + fsProgId + "' AND FNEPISODE >= " + startEpisode + " AND FNEPISODE <= " + endEpisode + " ORDER BY FNEPISODE";

                        //SQLstr = "SELECT * FROM openquery(PTS_PROG, '" + SQLstr.Replace("'", "''''") + "' ) ";

                        rtnList = wsobj.fnGetValue(SQLstr); 

                        if (rtnList.Count > 0 ){
                            strEpsList = "";
                            foreach (string s in rtnList)
                            {
                                //strEpsList = strEpsList + ", " + s;

                                if (strEpsList.Trim() == "")
                                {
                                    strEpsList = s;
                                }
                                else
                                {
                                    strEpsList = strEpsList + ", " + s;
                                }
                            }

                            brtn = true;
                            errString = "下列子集的工作人員已經建檔：" + strEpsList + "，確定要清除 (所有工作人員將被刪除) 後重新匯入工作人員資料嗎？" ;
                            dr.Close();
                            sqlConn.Close();
                            return brtn;
                        }

                    }

                    //檢查子集音樂報表是否已存在確認要刪除重建
                    if (iforceUpdate <= 1)
                    {
                        SQLstr = "SELECT DISTINCT FNEPISODE FROM PTSPROG.PTSProgram.dbo.TBMUSIC WHERE FSPROGID = '" + fsProgId + "' AND FNEPISODE >= " + startEpisode + " AND FNEPISODE <= " + endEpisode + " ORDER BY FNEPISODE";
                        //SQLstr = "SELECT * FROM openquery(PTS_PROG, '" + SQLstr.Replace("'", "''''") + "' ) ";

                        rtnList = wsobj.fnGetValue(SQLstr);

                        if (rtnList.Count > 0)
                        {
                            strEpsList = "";
                            foreach (string s in rtnList)
                            {
                                //strEpsList = strEpsList + ", " + s;

                                if (strEpsList.Trim() == "")
                                {
                                    strEpsList = s;
                                }
                                else
                                {
                                    strEpsList = strEpsList + ", " + s;
                                }
                            }

                            brtn = true;

                            errString = "下列子集的音樂詞曲著作已經建檔：" + strEpsList + "，確定要清除 (所有音樂詞曲著作將被刪除) 後重新匯入音樂詞曲著作資料嗎？";

                            dr.Close();
                            sqlConn.Close();
                            return brtn;
                        }
                    }




                }
                else
                {
                    brtn = false;
                    errString = "外部網路輸入的資料中找不到節目名稱為【" + strPrgName + "】的資料！";
                    dr.Close();
                    sqlConn.Close(); 
                    return brtn; 
                }
             
                dr.Close() ;

                //開始異動

                SQLstr = "SELECT PBSET, PBNAM, PBCON, PBLEN, PBYEAR,PBENO FROM CRFILE_PROGB WHERE PAENO = " + lPrgID + " AND PBSET >= " + startEpisode + " AND PBSET <= " + endEpisode + " ORDER BY PBSET";

                SQLstr = "SELECT PBSET, PBNAM, PBCON, PBLEN,PBENO, ISNULL(PBYEAR,'')as PBYEAR FROM openquery(PTS_PROG_MYSQL, '" + SQLstr.Replace("'", "''''") + "' ) ";

                cmd.CommandText = SQLstr;

                dr = cmd.ExecuteReader();

            

                while (dr.Read())
                {                    
                    SQLstr = "UPDATE PTSPROG.PTSProgram.dbo.TBPROG_D SET FSPGDNAME = '" + dr["PBNAM"].ToString() + "' ";
                    
                    //內容
                    if (Convert.IsDBNull(dr["PBCON"]))
                    {
                        SQLstr = SQLstr + ",  FSCONTENT = NULL";
                    }
                    else
                    {
                        SQLstr = SQLstr + ",  FSCONTENT = '" + dr["PBCON"].ToString() + "'"; //PBNAM改成PBCON
                    }

                    //實際帶長
                    if (Convert.IsDBNull(dr["PBLEN"]))
                    {
                        SQLstr = SQLstr + ",  FNTAPELENGTH = NULL";
                    }
                    else
                    {
                        SQLstr = SQLstr + ",  FNTAPELENGTH = " + dr["PBLEN"].ToString().Trim() ;
                    }

                    //完成年度
                    if (Convert.IsDBNull(dr["PBCON"]))
                    {
                        SQLstr = SQLstr + ",  FSPRDYEAR = NULL";
                    }
                    else
                    {
                        SQLstr = SQLstr + ",  FSPRDYEAR = '" + dr["PBYEAR"].ToString().Trim() + "'"; 
                    }

                    //節目規格
                    string strProgSpecPRG = checkProgSpecPGM(lPrgID.ToString(), dr["PBENO"].ToString().Trim());

                    if (strProgSpecPRG.Trim() == "")
                    {
                        SQLstr = SQLstr + ",  FSPROGSPEC = NULL";
                    }
                    else
                    {
                        SQLstr = SQLstr + ",  FSPROGSPEC = '" + strProgSpecPRG.Trim() + "'";
                    }

                    SQLstr = SQLstr + ", FSUPDUSER = '01065', FDUPDDATE = Getdate() "; 
                    SQLstr = SQLstr + " WHERE FSPROGID = '" + fsProgId + "' AND FNEPISODE = " + dr["PBSET"].ToString() ;

                    //連至MAM系統，更新子集名稱、內容、實際帶長、完成年度、節目規格    

                    string strProgSpecMAM = checkProgSpecMAM(lPrgID.ToString(), dr["PBENO"].ToString().Trim());

                     if (Convert.IsDBNull(dr["PBCON"]))
                         SQLstrMAM = "UPDATE MAM.dbo.TBPROG_D SET FSPGDNAME = '" + dr["PBNAM"].ToString().Replace("'", "''") + "',  FSCONTENT = '' , FNTAPELENGTH =" + dr["PBLEN"].ToString().Trim() + ",  FSPRDYEAR ='" + dr["PBYEAR"].ToString().Trim() + "', FSPROGSPEC = '" + strProgSpecMAM + "' , FSUPDUSER = '"+createdUser+"', FDUPDDATE = Getdate()  WHERE FSPROG_ID = '" + fsProgId + "' AND FNEPISODE = " + dr["PBSET"].ToString();
                     else
                     {
                         SQLstrMAM = "UPDATE MAM.dbo.TBPROG_D SET FSPGDNAME = '" + dr["PBNAM"].ToString().Replace("'", "''") + "',  FSCONTENT = '" + dr["PBCON"].ToString().Replace("'", "''") + "'  , FNTAPELENGTH =" + dr["PBLEN"].ToString().Trim() + ",  FSPROGSPEC = '" + strProgSpecMAM + "',  FSPRDYEAR ='" + dr["PBYEAR"].ToString().Trim() + "', FSUPDUSER = '"+createdUser+"', FDUPDDATE = Getdate()  WHERE FSPROG_ID = '" + fsProgId + "' AND FNEPISODE = " + dr["PBSET"].ToString();
                     }

                    cmd2.CommandText = SQLstr; 
                    cmd2.ExecuteNonQuery() ;

                    cmd2.CommandText = SQLstrMAM; 
                    cmd2.ExecuteNonQuery() ;


                    //刪除子集的工作人員
                    SQLstr = "DELETE FROM PTSPROG.PTSProgram.dbo.TBPROGMEN WHERE FSPROGID = '" + fsProgId + "' AND FNEPISODE = " + dr["PBSET"].ToString();

                    //連至MAM系統，刪除子集的工作人員
                    SQLstrMAM = "DELETE FROM MAM.dbo.TBPROGMEN WHERE FSPROG_ID = '" + fsProgId + "' AND FNEPISODE = " + dr["PBSET"].ToString();

                    cmd2.CommandText = SQLstr; 
                    cmd2.ExecuteNonQuery();

                    cmd2.CommandText = SQLstrMAM;
                    cmd2.ExecuteNonQuery();

                    //刪除子集的音樂著作
                    SQLstr = "DELETE FROM PTSPROG.PTSProgram.dbo.TBMUSIC WHERE FSPROGID = '" + fsProgId + "' AND FNEPISODE = " + dr["PBSET"].ToString();
                    cmd2.CommandText = SQLstr;
                    cmd2.ExecuteNonQuery();                    
                }

                dr.Close(); 

                //新增音樂著作：必須音樂著作審核通過，全部刪除後重新建立
                string strValStr = string.Empty;
                string strSeqNo = string.Empty;

                SQLstr = "SELECT M.*, PBSET FROM CRFILE_MUSIC M INNER JOIN CRFILE_PROGB E ON M.PAENO = E.PAENO AND M.PBENO = E.PBENO WHERE M.PAENO = " + lPrgID + " AND PBSET >= " + startEpisode + " AND PBSET <= " + endEpisode + " ORDER BY PBSET";
                SQLstr = "SELECT * FROM OPENQUERY(PTS_PROG_MYSQL, '" + SQLstr.Replace("'", "''''") + "' ) "; 
                cmd.CommandText = SQLstr;
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    strSeqNo = "";
                    SQLstr = "SELECT ISNULL(MAX(FNSEQNO)+1,1) FROM PTSPROG.PTSProgram.dbo.TBMUSIC WHERE FSPROGID = '" + fsProgId + "' AND FNEPISODE = " + dr["PBSET"].ToString();
                    
                    rtnList = wsobj.fnGetValue(SQLstr);

                    if (rtnList.Count > 0)
                        strSeqNo = rtnList[0]; 

                        
                    //檢查曲目是否空白
                    strValStr = dr["MSNAM"].ToString().Trim().Replace("\r", "") ;
                    strValStr = strValStr.Trim().Replace("\n", "");

                    if (strValStr != string.Empty)
                    {
                        //' now insert 

                        SQLstr = "INSERT INTO PTSPROG.PTSProgram.dbo.TBMUSIC( FSPROGID, FNEPISODE, FNSEQNO, " ; 
                        SQLstr = SQLstr  + "FSMUSICNAME, FSCOMPOSER, FSSONGWRITING, FSARRANGER, " ; 
                        SQLstr = SQLstr + "FSSINGER, FSCOMPANY, FSPRODUCER, FSPRODUCTID, FSSOUNDMAKER, FNUSETIMES, FNUSELENGTH, " ; 
                        SQLstr = SQLstr + "FSMEMO, FCBACKGROUND, FCTITLETHEME, FCENDINGTHEME, FCPERFORMANCE, " ; 
                        SQLstr = SQLstr + "FCLIVE, FCRECORD, FCMTV, FCFREESHOW, FCPUBSHOW, FCPUBTRANSMIT, " ; 
                        SQLstr = SQLstr + "FCPERIPHERAL, FCPUBLISH, FCREPRD, FCREAUTH, "; 
                        SQLstr = SQLstr + "FSCRTUSER, FDCRTDATE) " ;
                        SQLstr = SQLstr + " VALUES ('" + fsProgId + "'"; 
                        SQLstr = SQLstr + ", " + dr["PBSET"].ToString() ; 
                        SQLstr = SQLstr + ",'" + strSeqNo + "'" ; 
                        SQLstr = SQLstr + ",'" + strValStr.Replace("'", "''")  + "'" ;

                        if (Convert.IsDBNull(dr["MSMAA"]))
                        {
                            SQLstr = SQLstr + ", NULL";
                        }
                        else
                        {
                            SQLstr = SQLstr + ",'" + dr["MSMAA"].ToString().Replace("'", "''") + "'";
                        }

                        if (Convert.IsDBNull(dr["MSMAB"]))
                        {
                            SQLstr = SQLstr + ", NULL";
                        }
                        else
                        {
                            SQLstr = SQLstr + ",'" + dr["MSMAB"].ToString().Replace("'", "''") + "'";
                        }

                        if (Convert.IsDBNull(dr["MSMAC"]))
                        {
                            SQLstr = SQLstr + ", NULL";
                        }
                        else
                        {
                            SQLstr = SQLstr + ",'" + dr["MSMAC"].ToString().Replace("'", "''") + "'";
                        }

                        if (Convert.IsDBNull(dr["MSMAD"]))
                        {
                            SQLstr = SQLstr + ", NULL";
                        }
                        else
                        {
                            SQLstr = SQLstr + ",'" + dr["MSMAD"].ToString().Replace("'", "''") + "'";
                        }



                        if (Convert.IsDBNull(dr["MSMAE"]))
                        {
                            SQLstr = SQLstr + ", NULL";
                        }
                        else
                        {
                            SQLstr = SQLstr + ",'" + dr["MSMAE"].ToString().Replace("'", "''") + "'";
                        }


                        if (Convert.IsDBNull(dr["MSMAG"]))
                        {
                            SQLstr = SQLstr + ", NULL";
                        }
                        else
                        {
                            SQLstr = SQLstr + ",'" + dr["MSMAG"].ToString().Replace("'", "''") + "'";
                        }



                        if (Convert.IsDBNull(dr["MSGNO"]))
                        {
                            SQLstr = SQLstr + ", NULL";
                        }
                        else
                        {
                            SQLstr = SQLstr + ",'" + dr["MSGNO"].ToString().Replace("'", "''") + "'";
                        }


                        if (Convert.IsDBNull(dr["MSMAF"]))
                        {
                            SQLstr = SQLstr + ", NULL";
                        }
                        else
                        {
                            SQLstr = SQLstr + ",'" + dr["MSMAF"].ToString().Replace("'", "''") + "'";
                        }

                        //if (Convert.IsDBNull(dr["MSMAF"]))
                        //{
                        //    SQLstr = SQLstr + ", NULL";
                        //}
                        //else
                        //{
                        //    SQLstr = SQLstr + ",'" + dr["MSMAF"].ToString() + "'";
                        //}


                        SQLstr = SQLstr + "," + dr["MSTME"].ToString().Replace("'", "''");

                        SQLstr = SQLstr + "," + dr["MSLEN"].ToString().Replace("'", "''");

                        if (Convert.IsDBNull(dr["MSMRK"]))
                        {
                            SQLstr = SQLstr + ", NULL"; 
                        }
                        else
                        {
                            SQLstr = SQLstr + ",'" + dr["MSMRK"].ToString().Replace("'", "''") + "'"; 
                        }


                        SQLstr = SQLstr + (Convert.ToInt32(dr["MSKDA"]) == 1 ? ", 1": ", 0");
                        SQLstr = SQLstr + (Convert.ToInt32(dr["MSKDB"]) == 1 ? ", 1": ", 0");
                        SQLstr = SQLstr + (Convert.ToInt32(dr["MSKDC"]) == 1 ? ", 1": ", 0");
                        SQLstr = SQLstr + (Convert.ToInt32(dr["MSKDD"]) == 1 ? ", 1": ", 0");
                        SQLstr = SQLstr + (Convert.ToInt32(dr["MSKAA"]) == 1 ? ", 1": ", 0");
                        SQLstr = SQLstr + (Convert.ToInt32(dr["MSKAB"]) == 1 ? ", 1": ", 0");
                        SQLstr = SQLstr + (Convert.ToInt32(dr["MSKAC"]) == 1 ? ", 1": ", 0");
                        SQLstr = SQLstr + (Convert.ToInt32(dr["MSKBA"]) == 1 ? ", 1": ", 0");
                        SQLstr = SQLstr + (Convert.ToInt32(dr["MSKBB"]) == 1 ? ", 1": ", 0");
                        SQLstr = SQLstr + (Convert.ToInt32(dr["MSKBC"]) == 1 ? ", 1": ", 0");
                        SQLstr = SQLstr + (Convert.ToInt32(dr["MSKBD"]) == 1 ? ", 1": ", 0");
                        SQLstr = SQLstr + (Convert.ToInt32(dr["MSKBE"]) == 1 ? ", 1": ", 0");
                        SQLstr = SQLstr + (Convert.ToInt32(dr["MSKBF"]) == 1 ? ", 1": ", 0");

                        if (Convert.ToInt32(dr["MSKBA"]) == 1)
                        {
                            if (Convert.ToInt32(dr["MSKCA"]) == 1)
                            {
                                SQLstr = SQLstr + ", 'F'"; 
                            }
                            else if (Convert.ToInt32(dr["MSKCB"]) == 1)
                            {
                                SQLstr = SQLstr + ", 'Y'"; 
                            }
                            else if (Convert.ToInt32(dr["MSKCC"]) == 1)
                            {
                                SQLstr = SQLstr + ", 'N'";
                            }
                            else
                            {
                                SQLstr = SQLstr + ", ''"; 
                            }

                        }
                        else
                        {
                            SQLstr = SQLstr + ", ''"; 
                        }
                        SQLstr = SQLstr + ", '01065',Getdate())";


                        cmd2.CommandText = SQLstr;
                        cmd2.ExecuteNonQuery(); 
                    }
                }
                dr.Close(); 


                // 新增子集工作人員：必須工作人員審核通過，全部刪除後重新建立
                SQLstr = "SELECT S.*, PBSET FROM CRFILE_STAFF S INNER JOIN CRFILE_PROGB E ON S.PAENO = E.PAENO" ; 
                SQLstr = SQLstr + " AND S.PBENO = E.PBENO" ; 
                SQLstr = SQLstr + " WHERE S.PAENO = " + lPrgID + " AND PBSET >= " + startEpisode ; 
                SQLstr = SQLstr + " AND PBSET <= " + endEpisode ;
                SQLstr = SQLstr + " ORDER BY PBSET";

                SQLstr = "SELECT * FROM OPENQUERY(PTS_PROG_MYSQL, '" + SQLstr.Replace("'", "''''")  + "' ) " ; 



                cmd.CommandText = SQLstr;
                dr = cmd.ExecuteReader(); 
                while (dr.Read()) 
                {
                    //檢查公司/姓名是否空白
                    strValStr = dr["STNAM"].ToString().Trim().Replace("\r", "");
                    strValStr = strValStr.Trim().Replace("\n", "");
                    if (strValStr != string.Empty)
                    {
                        //檢查頭銜代碼
                        SQLstr = "SELECT FSTITLEID FROM PTSPROG.PTSProgram.dbo.TBZTITLE ";
                        SQLstr = SQLstr + " WHERE FSTITLEID = '" + dr["STITL"].ToString().Replace("'", "''") + "'";


                        rtnList = wsobj.fnGetValue(SQLstr);

                        if (rtnList.Count > 0)
                        {
                            SQLstr = "INSERT INTO PTSPROG.PTSProgram.dbo.TBPROGMEN(FSPROGID, FNEPISODE, FSTITLEID," ; 
                            SQLstr = SQLstr +  " FSNAME, FSMEMO, FSCRTUSER, FDCRTDATE) " ;
                            SQLstr = SQLstr + " VALUES ('" + fsProgId + "'";
                            SQLstr = SQLstr + "," + dr["PBSET"].ToString() ; 
                            SQLstr = SQLstr + ",'" + dr["STITL"].ToString()  + "'"  ;
                            SQLstr = SQLstr + ",'" + strValStr.Replace("'", "''") + "'"; 

                            if(Convert.IsDBNull(dr["STMRK"]) ) 
                            {
                                SQLstr = SQLstr + ", NULL";
                            }
                            else 
                            {
                                SQLstr = SQLstr + ",'" + dr["STMRK"].ToString().Replace("'", "''") + "'"; 
                            }
                            
                             SQLstr = SQLstr + ",'01065',Getdate())"; 

                            //連結MAM系統，新增相關人員資料
                             SQLstrMAM = "INSERT INTO MAM.dbo.TBPROGMEN(FSPROG_ID, FNEPISODE, FSTITLEID,";
                             SQLstrMAM = SQLstrMAM + "FSEMAIL, FSSTAFFID, FSNAME, FSMEMO, FSCREATED_BY, FDCREATED_DATE, FSUPDATED_BY, FDUPDATED_DATE) ";
                             SQLstrMAM = SQLstrMAM + " VALUES ('" + fsProgId + "'";
                             SQLstrMAM = SQLstrMAM + "," + dr["PBSET"].ToString();
                             SQLstrMAM = SQLstrMAM + ",'" + dr["STITL"].ToString() + "','',''";
                             SQLstrMAM = SQLstrMAM + ",'" + strValStr.Replace("'", "''") + "'";

                             if (Convert.IsDBNull(dr["STMRK"]))
                             {
                                 SQLstrMAM = SQLstrMAM + ", ''";
                             }
                             else
                             {
                                 SQLstrMAM = SQLstrMAM + ",'" + dr["STMRK"].ToString().Replace("'", "''") + "'";
                             }

                             SQLstrMAM = SQLstrMAM + ",'" + createdUser + "',Getdate(),'','1900/1/1')";//SQLstrMAM + ",'mam',Getdate(),'','1900/1/1')"; by Jarvis 20131007

                             cmd2.CommandText = SQLstr;
                             cmd2.ExecuteNonQuery(); 

                             cmd2.CommandText = SQLstrMAM;
                             cmd2.ExecuteNonQuery();                              
                        }

                        //cmd2.CommandText = SQLstr;

                        //dr2 = cmd2.ExecuteReader(); 


                    }
                }
                dr.Close(); 

                //新增/更改節目工作人員
                SQLstr = "SELECT * FROM CRFILE_STAFF WHERE PAENO = " + lPrgID + " AND PBENO = 0";
                SQLstr = "SELECT * FROM OPENQUERY(PTS_PROG_MYSQL, '" + SQLstr.Replace("'", "''''") + "' )";

                cmd.CommandText = SQLstr ;
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    //檢查公司/姓名是否空白
                    strValStr = dr["STNAM"].ToString().Replace("\r", "") ; 
                    strValStr = dr["STNAM"].ToString().Replace("\n", "") ;
                    if (strValStr != "")
                    {
                        //檢查節目工作人員是否已建檔
                        SQLstr = "SELECT FSTITLEID, FSMEMO FROM PTSPROG.PTSProgram.dbo.TBPROGMEN WHERE FSPROGID = '" + fsProgId + "' AND FNEPISODE IS NULL "; 
                        SQLstr = SQLstr + " AND FSTITLEID = '" + dr["STITL"].ToString().Replace("'", "''''") + "' " ;
                        SQLstr = SQLstr + "AND FSNAME = '" + strValStr.Replace("'", "''''") +  "' ";


                        sqlConn3.Open() ; 
                        cmd3.CommandText = SQLstr   ; 

                        dr3 = cmd3.ExecuteReader() ; 


                        

                        if (!dr3.Read() )
                        {
                            //檢查頭銜代碼
                            SQLstr = "SELECT FSTITLEID FROM PTSPROG.PTSProgram.dbo.TBZTITLE WHERE FSTITLEID = '" + dr["STITL"].ToString() + "'" ;
                            rtnList = wsobj.fnGetValue(SQLstr);
                            if (rtnList.Count > 0)
                            {
                                //開始新增節目工作人員
                                SQLstr = "INSERT INTO PTSPROG.PTSProgram.dbo.TBPROGMEN(FSPROGID, FNEPISODE, FSTITLEID,";
                                SQLstr = SQLstr + " FSNAME, FSMEMO, FSCRTUSER, FDCRTDATE) ";
                                SQLstr = SQLstr + " VALUES ('" + fsProgId + "', NULL ";
                                SQLstr = SQLstr + ",'" + dr["STITL"] + "'";
                                SQLstr = SQLstr + ",'" + strValStr.Replace("'", "''") + "'";

                                if (Convert.IsDBNull(dr["STMRK"]))
                                {
                                    SQLstr = SQLstr + ", NULL";
                                }
                                else
                                {
                                    SQLstr = SQLstr + ",'" + dr["STMRK"].ToString().Replace("'", "''") + "'";
                                }

                                SQLstr = SQLstr + ",'01065',Getdate())";

                                //連結MAM系統，新增相關人員資料
                                SQLstrMAM = "INSERT INTO MAM.dbo.TBPROGMEN(FSPROG_ID, FNEPISODE, FSTITLEID,";
                                SQLstrMAM = SQLstrMAM + "FSEMAIL, FSSTAFFID, FSNAME, FSMEMO, FSCREATED_BY, FDCREATED_DATE, FSUPDATED_BY, FDUPDATED_DATE) ";
                                SQLstrMAM = SQLstrMAM + " VALUES ('" + fsProgId + "', '0' ";
                                SQLstrMAM = SQLstrMAM + ",'" + dr["STITL"] + "','',''";
                                SQLstrMAM = SQLstrMAM + ",'" + strValStr.Replace("'", "''") + "'";

                                if (Convert.IsDBNull(dr["STMRK"]))
                                {
                                    SQLstrMAM = SQLstrMAM + ", ''";
                                }
                                else
                                {
                                    SQLstrMAM = SQLstrMAM + ",'" + dr["STMRK"].ToString().Replace("'", "''") + "'";
                                }

                                SQLstrMAM = SQLstrMAM + ",'"+createdUser+"',Getdate(),'','1900/1/1')"; //SQLstrMAM + ",'mam',Getdate(),'','1900/1/1')"; by Jarvis 20131007

                                cmd2.CommandText = SQLstr;
                                cmd2.ExecuteNonQuery();

                                cmd2.CommandText = SQLstrMAM;
                                cmd2.ExecuteNonQuery();
                            }
                            

                        
                        }

                        else
                        {
                            Boolean bInMod = false;

                            if (!Convert.IsDBNull(dr3["FSMEMO"]) && !Convert.IsDBNull(dr["STMRK"]))
                            {
                                if (dr3["FSMEMO"] != dr["STMRK"])
                                    bInMod = true;


                            }
                            else if (Convert.IsDBNull(dr3["FSMEMO"]) && Convert.IsDBNull(dr["STMRK"]))
                            {

                            }
                            else
                            {
                                bInMod = true;
                            }

                            //If Not IsNull(rs.Fields("FSMEMO")) And Not IsNull(adcImp.Recordset.Fields("STMRK")) Then
                            //If rs.Fields("FSMEMO") <> adcImp.Recordset.Fields("STMRK") Then blnMod = True
                            //ElseIf IsNull(rs.Fields("FSMEMO")) And IsNull(adcImp.Recordset.Fields("STMRK")) Then
                            //Else
                            //    blnMod = True
                            //End If
                            if (bInMod)
                            {
                                SQLstr = "UPDATE PTSPROG.PTSProgram.dbo.TBPROGMEN SET FSNAME = '" + dr["STNAM"].ToString().Replace("'", "''") + "'";
                                if (Convert.IsDBNull(dr["STMRK"]))
                                {
                                    SQLstr = SQLstr + ", FSMEMO = NULL";
                                }
                                else
                                {
                                    SQLstr = SQLstr + ", FSMEMO = '" + dr["STMRK"].ToString().Replace("'", "''") + "'";
                                }

                                SQLstr = SQLstr + ", FSUPDUSER = '01065'";
                                SQLstr = SQLstr + ", FDUPDDATE = Getdate() ";
                                SQLstr = SQLstr + "WHERE FSPROGID = '" + fsProgId + "' ";

                                SQLstr = SQLstr + "AND FNEPISODE IS NULL ";
                                SQLstr = SQLstr + "AND FSTITLEID = '" + dr["STITL"].ToString().Replace("'", "''") + "' ";
                                SQLstr = SQLstr + "AND FSNAME = '" + strValStr.Replace("'", "''") + "' ";

                                //連結MAM系統，修改相關人員
                                SQLstrMAM = "UPDATE MAM.dbo.TBPROGMEN SET FSNAME = '" + dr["STNAM"].ToString().Replace("'", "''") + "'";
                                if (Convert.IsDBNull(dr["STMRK"]))
                                {
                                    SQLstrMAM = SQLstrMAM + ", FSMEMO = ''";
                                }
                                else
                                {
                                    SQLstrMAM = SQLstrMAM + ", FSMEMO = '" + dr["STMRK"].ToString().Replace("'", "''") + "'";
                                }

                                SQLstrMAM = SQLstrMAM + ", FSUPDATED_BY = '"+createdUser+"'";
                                SQLstrMAM = SQLstrMAM + ", FDUPDATED_DATE = Getdate() ";
                                SQLstrMAM = SQLstrMAM + "WHERE FSPROG_ID = '" + fsProgId + "' ";

                                SQLstrMAM = SQLstrMAM + "AND FNEPISODE = 0 ";
                                SQLstrMAM = SQLstrMAM + "AND FSTITLEID = '" + dr["STITL"].ToString().Replace("'", "''") + "' ";
                                SQLstrMAM = SQLstrMAM + "AND FSNAME = '" + strValStr.Replace("'", "''") + "' ";

                                cmd2.CommandText = SQLstr;
                                cmd2.ExecuteNonQuery();

                                cmd2.CommandText = SQLstrMAM;
                                cmd2.ExecuteNonQuery();
                            }
                        }
                        dr3.Close();
                        sqlConn3.Close();
                    }

                    
           
                }

                //如果前面的查詢有查到委製協調的話，要在工作人員資料表裡新增該委製協調
                if (strWhosInCharge != string.Empty)
                {
                    // 記錄委製協調
                    SQLstr = "SELECT FSTITLEID, FSMEMO FROM PTSPROG.PTSProgram.dbo.TBPROGMEN "  ;
                    SQLstr = SQLstr + "WHERE FSPROGID = '" + fsProgId + "' "; 
                    SQLstr = SQLstr + "AND FNEPISODE IS NULL " ; 
                    SQLstr = SQLstr + "AND FSTITLEID = '06' " ;
                    SQLstr = SQLstr + "AND FSNAME = '" + strWhosInCharge.Replace("'", "''") + "' ";

                    rtnList = wsobj.fnGetValue(SQLstr);

                    if (rtnList.Count == 0)
                    {
                        SQLstr = "INSERT INTO PTSPROG.PTSProgram.dbo.TBPROGMEN(FSPROGID, FNEPISODE, FSTITLEID,"; 
                         SQLstr = SQLstr + " FSNAME, FSMEMO, FSCRTUSER, FDCRTDATE) " ;
                         SQLstr = SQLstr + " VALUES ('" + fsProgId + "', NULL ,'06'"; 
                         SQLstr = SQLstr +  ",'" + strWhosInCharge.Replace("'", "''")  +  "', NULL" ; 
                         SQLstr = SQLstr + ",'01065',Getdate())" ;

                        //連結MAM系統，新增節目相關人員
                         SQLstrMAM = "INSERT INTO MAM.dbo.TBPROGMEN(FSPROGID, FNEPISODE, FSTITLEID,";
                         SQLstrMAM = SQLstrMAM + " FSEMAIL, FSSTAFFID, FSNAME, FSMEMO, FSCREATED_BY, FDCREATED_DATE, FSUPDATED_BY, FDUPDATED_DATE) ";
                         SQLstrMAM = SQLstrMAM + " VALUES ('" + fsProgId + "', '0' ,'06','',''";
                         SQLstrMAM = SQLstrMAM + ",'" + strWhosInCharge.Replace("'", "''") + "', ''";
                         SQLstrMAM = SQLstrMAM + ",'"+createdUser+"',Getdate(),'','1900/1/1')";

                         cmd2.CommandText = SQLstr;
                        cmd2.ExecuteNonQuery();

                        cmd2.CommandText = SQLstrMAM;
                        cmd2.ExecuteNonQuery(); 
                    }
                }
                
                sqlConn2.Close();                
            }
            catch (Exception ex)
            {
               // tran.Rollback(); 
                brtn = false;
                errString = ex.ToString();
            }
            finally
            {
                sqlConn.Close(); 
            }
           


            return brtn; 
        }

        //查詢節目的規格是否有設定SD、HD、其他(節目管理系統)
        public string checkProgSpecPGM(string strPAENO,string strPBENO)
        {
            System.Text.StringBuilder sbSpec = new System.Text.StringBuilder();             
            SqlConnection sqlConn = new SqlConnection(MAM_PTS_DLL.DbAccess.sqlConnStr);
            SqlCommand cmd = new SqlCommand("", sqlConn);
            SqlDataReader dr;
            string SQLstr = string.Empty;
            Boolean bolUse = false;
            try
            {
                sqlConn.Open();
                sbSpec.Append("00000000000000000000000000000000000000000000000000");
                SQLstr = "SELECT PBSPE FROM CRFILE_specification WHERE PAENO = " + strPAENO + " AND PBENO = " + strPBENO ;
                SQLstr = "SELECT ISNULL(PBSPE,'')as PBSPE FROM openquery(PTS_PROG_MYSQL, '" + SQLstr + "' ) ";

                cmd.CommandText = SQLstr;
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    if (dr["PBSPE"].ToString().Trim().IndexOf("SD") >= 0)
                    {
                        bolUse = true;
                        sbSpec.Replace("0", "1", 0, 1);
                    }
                    else if (dr["PBSPE"].ToString().Trim().IndexOf("HD") >= 0)
                    {
                        bolUse = true;
                        sbSpec.Replace("0", "1", 1, 1);
                    }
                    else if (dr["PBSPE"].ToString().Trim().IndexOf("其他") >= 0)
                    {
                        bolUse = true;
                        sbSpec.Replace("0", "1", 49, 1);
                    }
                }

                if (bolUse == false)
                    return "";

                dr.Close(); 
                sqlConn.Close();
                return sbSpec.ToString();
            }
            catch(Exception ex)
            {
                return "";
            }
            finally
            {               
                sqlConn.Close(); 
            }
        }

        //查詢節目的規格是否有設定SD、HD、其他(MAM系統)
        public string checkProgSpecMAM(string strPAENO, string strPBENO)
        {
            System.Text.StringBuilder sbSpec = new System.Text.StringBuilder();
            SqlConnection sqlConn = new SqlConnection(MAM_PTS_DLL.DbAccess.sqlConnStr);
            SqlCommand cmd = new SqlCommand("", sqlConn);
            SqlDataReader dr;
            string SQLstr = string.Empty;
            Boolean bolUse = false;
            try
            {
                sqlConn.Open();
                SQLstr = "SELECT PBSPE FROM CRFILE_specification WHERE PAENO = " + strPAENO + " AND PBENO = " + strPBENO + "  ORDER BY PBSPE DESC";
                SQLstr = "SELECT ISNULL(PBSPE,'')as PBSPE FROM openquery(PTS_PROG_MYSQL, '" + SQLstr + "' ) ";

                cmd.CommandText = SQLstr;
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    if (dr["PBSPE"].ToString().Trim().IndexOf("SD") >= 0)
                    {
                        bolUse = true;
                        sbSpec.Append("01;");
                    }
                    else if (dr["PBSPE"].ToString().Trim().IndexOf("HD") >= 0)
                    {
                        bolUse = true;
                        sbSpec.Append("02;");
                    }
                    else if (dr["PBSPE"].ToString().Trim().IndexOf("其他") >= 0)
                    {
                        bolUse = true;
                        sbSpec.Append("50;");
                    }
                }

                if (bolUse == false)
                    return "";

                dr.Close();
                sqlConn.Close();
                return sbSpec.ToString();
            }
            catch (Exception ex)
            {
                return "";
            }
            finally
            {
                sqlConn.Close();
            }
        }


    }
}
