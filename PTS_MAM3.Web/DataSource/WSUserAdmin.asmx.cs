﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Collections.ObjectModel;


namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSUserAdmin 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSUserAdmin : System.Web.Services.WebService
    {

        [WebMethod(EnableSession=true)]        
        public string ModifyUser(UserStruct user)
        {
            string rtn = "";
            try
            {
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;



                    cmd.CommandText = "UPDATE TBUSERS SET FSUSER = '" + user.FSUSER.Replace("'", "''") + "', FSUSER_ChtName = '" + user.FSUSER_ChtName.Replace("'", "''") + "', FSDEPT = '" + user.FSDEPT.Replace("'", "''") + "', ";
                    if (user.FSPASSWD.Trim() != string.Empty)
                    {
                        cmd.CommandText += "FSPASSWD = '" + MakeMD5Str.MAMFunctions.MakeMD5Str(user.FSPASSWD) + "', ";
                    }
                    cmd.CommandText += "FSEMAIL = '" + user.FSEMAIL.Replace("'", "''") + "' , FCACTIVE = '" + user.FCACTIVE.Replace("'", "''") + "' , FSDESCRIPTION = '" + user.FSDESCRIPTION.Replace("'", "''") + "', ";
                    cmd.CommandText += "FSUPDATED_BY = '" + ((UserStruct)System.Web.HttpContext.Current.Session["USEROBJ"]).FSUSER.Replace("'", "''") + "', FDUPDATED_DATE = getdate() , FSUSER_TITLE = '" + user.FSUSER_TITLE.Replace("'", "''") + "' WHERE FSUSER_ID = '" + user.FSUSER_ID + "' ";


                    WSFLOWDLL wsflowObj = new WSFLOWDLL();

                    Boolean brtn = wsflowObj.Flow_Oc_UpdateAccount(user.FSUSER_ID, user.FSUSER_ChtName, user.FSEMAIL);



                    if (brtn)
                    {
                        cn.Open();

                        cmd.ExecuteNonQuery();

                        cn.Close();


                    }
                    else
                    {
                        throw new Exception("更新流程內使用者資料表時發生錯誤!"); 
                    }


                    
                }
            }
            catch (Exception ex)
            {
                rtn = ex.ToString();
            }
            return rtn;
        }

       [WebMethod(EnableSession=true)]        
        public string AddUser(UserStruct user)
        {
            string rtn = "";
            try
            {
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString))
                {
                    WSMAMFunctions wobj = new WSMAMFunctions();
                    List<string> ids = wobj.fnGetValue("SELECT MAX(FSUSER_ID)  FROM TBUSERS WHERE FSUSER_ID LIKE 'Z%' ");

                    if (ids[0] == "")
                    {
                        ids.RemoveAt(0);
                        ids.Add("Z0000");
                    }

                    string id = ids[0].Substring(1, 4);
                    id = "Z" + String.Format("{0:0000}", (Convert.ToInt32(id) + 1));


                    user.FSUSER_ID = id;

                    WSFLOWDLL WSFlowObj = new WSFLOWDLL();

                    Boolean brtn = WSFlowObj.Flow_Oc_AddNewAccount(user.FSUSER_ID, user.FSUSER_ChtName, user.FSEMAIL);

                    if (brtn)
                    {
                   

                        if (ids.Count > 0)
                        {
                          

                            SqlCommand cmd = new SqlCommand();

                            cmd.Connection = cn;

                            cmd.CommandText = "INSERT INTO TBUSERS(FSUSER_ID, FSUSER, FSPASSWD, FSUSER_ChtName, FSDEPT, ";
                            cmd.CommandText += "FSEMAIL, FCACTIVE, FSDESCRIPTION, FCSECRET, FNUpperDept_ID, FNDep_ID, ";
                            cmd.CommandText += "FNGroup_ID, FSUpperDept_ChtName, FSDep_ChtName, FSGroup_ChtName, FSIs_Main_Title, ";
                            cmd.CommandText += "FSIs_Supervisor, FSCREATED_BY, FDCREATED_DATE, FSUPDATED_BY, FDUPDATED_DATE, FSUSER_TITLE ) VALUES ";
                            cmd.CommandText += "('" + user.FSUSER_ID.Replace("'", "''") + "', '" + user.FSUSER.Replace("'", "''") + "', '" + MakeMD5Str.MAMFunctions.MakeMD5Str(user.FSPASSWD) + "', ";
                            cmd.CommandText += "'" + user.FSUSER_ChtName.Replace("'", "''") + "', '" + user.FSDEPT.Replace("'", "''") + "', '" + user.FSEMAIL.Replace("'", "''") + "', ";
                            cmd.CommandText += "'" + user.FCACTIVE + "', '" + user.FSDESCRIPTION.Replace("'", "''") + "', ";
                            cmd.CommandText += "'" + user.FCSECRET + "', " + user.FNUPPERDEPT_ID + ", " + user.FNDEP_ID + ", ";
                            cmd.CommandText += user.FNGROUP_ID + ", '" + user.FSUPPERDEPT_CHTNAME.Replace("'", "''") + "', ";
                            cmd.CommandText += "'" + user.FSDEP_CHTNAME.Replace("'", "''") + "', '" + user.FSGROUP_CHTNAME.Replace("'", "''") + "', ";
                            cmd.CommandText += "'Y', 'N', '" + ((UserStruct)System.Web.HttpContext.Current.Session["USEROBJ"]).FSUSER.Replace("'", "''") + "', ";
                            cmd.CommandText += "getdate(), '" + ((UserStruct)System.Web.HttpContext.Current.Session["USEROBJ"]).FSUSER.Replace("'", "''") + "', getdate(), '" + user.FSUSER_TITLE.Replace("'", "''") + "' ) ";
                            cn.Open();

                            cmd.ExecuteNonQuery();

                            cn.Close();
                        }
                        else
                        {
                            throw new Exception("取得使用者ID 時發生錯誤!"); 
                        }
                        

                    }
                    else
                    {
                        throw new Exception("新增流程使用者帳號時發生錯誤!"); 
                    }

                }

            }
            catch (Exception ex)
            {
                rtn = ex.ToString();
            }

            return rtn;
        }


        [WebMethod(EnableSession = true)]        
        public ObservableCollection<UserStruct> GetUserListByQuery(UserStruct user)
        {
            ObservableCollection<UserStruct> UserList = new ObservableCollection<UserStruct>();
            try
            {
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;
                    if (user != null)
                    {
                        string linebuf = "";

                        if (user.FSUSER_ID.Trim().Replace("'", "''") != string.Empty)
                        {
                            if (linebuf.Length > 0)
                                linebuf += " AND ";
                            linebuf += "FSUSER_ID like '%" + user.FSUSER_ID.Trim().Replace("'", "''") + "%' ";
                        }

                        if (user.FSUSER.Trim().Replace("'", "''") != string.Empty)
                        {
                            if (linebuf.Length > 0)
                                linebuf += " AND ";
                            linebuf += "FSUSER LIKE '%" + user.FSUSER.Trim().Replace("'", "''") + "%' ";
                        }

                        if (user.FSUSER_ChtName.Trim().Replace("'", "''") != string.Empty)
                        {
                            if (linebuf.Length > 0)
                                linebuf += " AND ";
                            linebuf += "FSUSER_ChtName LIKE '%" + user.FSUSER_ChtName.Trim().Replace("'", "''") + "%' ";
                        }

                        if (user.FSGROUP_CHTNAME.Trim().Replace("'", "''") != string.Empty)
                        {
                            if (linebuf.Length > 0)
                                linebuf += " AND ";
                            linebuf += "FSGroup_ChtName LIKE '%" + user.FSGROUP_CHTNAME.Trim().Replace("'", "''") + "%' ";
                        }


                        if (user.FSUSER_TITLE.Trim().Replace("'", "''") != string.Empty)
                        {
                            if (linebuf.Length > 0)
                                linebuf += " AND ";
                            linebuf += "FSUSER_TITLE LIKE '%" + user.FSUSER_TITLE.Trim().Replace("'", "''") + "%' ";
                        }

                        if (user.FSDESCRIPTION.Trim().Replace("'", "''") != string.Empty)
                        {
                            if (linebuf.Length > 0)
                                linebuf += " AND ";
                            linebuf += "FSDESCRIPTION LIKE '%" + user.FSDESCRIPTION.Trim().Replace("'", "''") + "%' ";
                        }

                        if (user.FCACTIVE.Trim().Replace("'", "''") != string.Empty)
                        {
                            if (linebuf.Length > 0)
                                linebuf += " AND ";
                            linebuf += "FCACTIVE LIKE '%" + user.FCACTIVE.Trim().Replace("'", "''") + "%' ";
                        }

                        cmd.CommandText = "SELECT FSUSER_ID, FSUSER, ISNULL(FSPASSWD, '')FSPASSWD ,ISNULL(FSDEPT, '') FSDEPT, isNULL(FSEMAIL, '')FSEMAIL , ISNULL(FCACTIVE, 'Y') FCACTIVE, ISNULL(FSDESCRIPTION, '')FSDESCRIPTION ,ISNULL(FCSECRET,1)FCSECRET , ISNULL(FNDEP_ID, 0) FNDEP_ID, isNULL(FSCREATED_BY, '') FSCREATED_BY,ISNULL(FDCREATED_DATE, getdate() ) FDCREATED_DATE,ISNULL(FSUPDATED_BY, '')FSUPDATED_BY ,isNULL(FDUPDATED_DATE, getdate())FDUPDATED_DATE ,ISNULL(FSUSER_TITLE, '')FSUSER_TITLE, ISNULL(FSUSER_CHTNAME, '')FSUSER_CHTNAME,  FNUpperDept_ID, FNGroup_ID, ISNULL(FSUpperDept_ChtName, '') FSUpperDept_ChtName,  isNULL(FSDep_ChtName, '') FSDep_ChtName, isNULL(FSGroup_ChtName, '') FSGroup_ChtName FROM TBUSERS ";


                        cmd.CommandText += "WHERE " + linebuf;
                        cmd.CommandText += "ORDER BY FCACTIVE DESC ";

                        cn.Open();

                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    UserList.Add
                                    (
                                        new UserStruct
                                        {
                                            FSUSER_ID = (string)dr["FSUSER_ID"],
                                            FSUSER = (string)dr["FSUSER"],
                                            FSUSER_ChtName = (string)dr["FSUSER_ChtName"],
                                            FSPASSWD = (string)dr["FSPASSWD"],
                                            FSDEPT = (string)dr["FSDEPT"],
                                            FSEMAIL = (string)dr["FSEMAIL"],
                                            FCACTIVE = (string)dr["FCACTIVE"],
                                            FSDESCRIPTION = (string)dr["FSDESCRIPTION"],
                                            FCSECRET = (string)dr["FCSECRET"],
                                            FNDEP_ID = (int)dr["FNDEP_ID"],
                                            FSCREATED_BY = (string)dr["FSCREATED_BY"],
                                            FDCREATED_DATE = (DateTime)dr["FDCREATED_DATE"],
                                            FSUPDATED_BY = (string)dr["FSUPDATED_BY"],
                                            FDUPDATED_DATE = (DateTime)dr["FDUPDATED_DATE"],
                                            FSUSER_TITLE = (string)dr["FSUSER_TITLE"],
                                            FNUPPERDEPT_ID = (int)dr["FNUpperDept_ID"],
                                            FNGROUP_ID = (int)dr["FNGROUP_ID"],
                                            FSDEP_CHTNAME = (string)dr["FSDEP_CHTNAME"],
                                            FSGROUP_CHTNAME = (string)dr["FSGROUP_CHTNAME"],
                                            FSUPPERDEPT_CHTNAME = (string)dr["FSUPPERDEPT_CHTNAME"]
                                        }
                                    );
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                return null;
            }

            return UserList;

        }

        //GetUserListBySearchCondiction
        [WebMethod(EnableSession = true)]
        public ObservableCollection<UserStruct> GetUserListBySearchCondiction(UserStruct user)
        {
            ObservableCollection<UserStruct> UserList = new ObservableCollection<UserStruct>();
            try
            {
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;
                    if (user != null)
                    {
                        string linebuf = "";

                        if (user.FSUSER_ID.Trim().Replace("'", "''") != string.Empty)
                        {
                            if (linebuf.Length > 0)
                                linebuf += " AND ";
                            linebuf += "FSUSER_ID like '%" + user.FSUSER_ID.Trim().Replace("'", "''") + "%' ";
                        }

                        if (user.FSUSER.Trim().Replace("'", "''") != string.Empty)
                        {
                            if (linebuf.Length > 0)
                                linebuf += " AND ";
                            linebuf += "FSUSER LIKE '%" + user.FSUSER.Trim().Replace("'", "''") + "%' ";
                        }

                        if (user.FSUSER_ChtName.Trim().Replace("'", "''") != string.Empty)
                        {
                            if (linebuf.Length > 0)
                                linebuf += " AND ";
                            linebuf += "FSUSER_ChtName LIKE '%" + user.FSUSER_ChtName.Trim().Replace("'", "''") + "%' ";
                        }

                        if (user.FSGROUP_CHTNAME.Trim().Replace("'", "''") != string.Empty)
                        {
                            if (linebuf.Length > 0)
                                linebuf += " AND ";
                            linebuf += "FSGroup_ChtName LIKE '%" + user.FSGROUP_CHTNAME.Trim().Replace("'", "''") + "%' ";
                        }


                        if (user.FSUSER_TITLE.Trim().Replace("'", "''") != string.Empty)
                        {
                            if (linebuf.Length > 0)
                                linebuf += " AND ";
                            linebuf += "FSUSER_TITLE LIKE '%" + user.FSUSER_TITLE.Trim().Replace("'", "''") + "%' ";
                        }

                        if (user.FSDESCRIPTION.Trim().Replace("'", "''") != string.Empty)
                        {
                            if (linebuf.Length > 0)
                                linebuf += " AND ";
                            linebuf += "FSDESCRIPTION LIKE '%" + user.FSDESCRIPTION.Trim().Replace("'", "''") + "%' ";
                        }

                        if (user.FCACTIVE.Trim().Replace("'", "''") != string.Empty)
                        {
                            if (linebuf.Length > 0)
                                linebuf += " AND ";
                            linebuf += "FCACTIVE LIKE '%" + user.FCACTIVE.Trim().Replace("'", "''") + "%' ";
                        }

                        cmd.CommandText = "SELECT FSUSER_ID, FSUSER, ISNULL(FSPASSWD, '')FSPASSWD ,ISNULL(FSDEPT, '') FSDEPT, isNULL(FSEMAIL, '')FSEMAIL , ISNULL(FCACTIVE, 'Y') FCACTIVE, ISNULL(FSDESCRIPTION, '')FSDESCRIPTION ,ISNULL(FCSECRET,1)FCSECRET , ISNULL(FNDEP_ID, 0) FNDEP_ID, isNULL(FSCREATED_BY, '') FSCREATED_BY,ISNULL(FDCREATED_DATE, getdate() ) FDCREATED_DATE,ISNULL(FSUPDATED_BY, '')FSUPDATED_BY ,isNULL(FDUPDATED_DATE, getdate())FDUPDATED_DATE ,ISNULL(FSUSER_TITLE, '')FSUSER_TITLE, ISNULL(FSUSER_CHTNAME, '')FSUSER_CHTNAME,  FNUpperDept_ID, FNGroup_ID, ISNULL(FSUpperDept_ChtName, '') FSUpperDept_ChtName,  isNULL(FSDep_ChtName, '') FSDep_ChtName, isNULL(FSGroup_ChtName, '') FSGroup_ChtName,FNTreeViewDept,FNTreeViewDept_ChtName FROM TBUSERS ";


                        cmd.CommandText += "WHERE " + linebuf;
                        cmd.CommandText += "ORDER BY FCACTIVE DESC ";

                        cn.Open();

                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    string department=(string)dr["FSUPPERDEPT_CHTNAME"] == "MAM內部帳號" ? (string)dr["FSUPPERDEPT_CHTNAME"] + "(" + (string)dr["FNTreeViewDept_ChtName"] + ")" : (string)dr["FSUPPERDEPT_CHTNAME"] + "／" + (string)dr["FSDEP_CHTNAME"] + "／" + (string)dr["FSGROUP_CHTNAME"];
                                    if(department[department.Length-1]=='／')
                                    {
                                    department=department.Remove(department.Length-1);
                                    }
                                    UserList.Add
                                    (
                                        new UserStruct
                                        {
                                            FSUSER_ID = (string)dr["FSUSER_ID"],
                                            FSUSER = (string)dr["FSUSER"],
                                            FSUSER_ChtName = (string)dr["FSUSER_ChtName"],
                                            FSPASSWD = (string)dr["FSPASSWD"],
                                            FSDEPT = (string)dr["FSDEPT"],
                                            FSEMAIL = (string)dr["FSEMAIL"],
                                            FCACTIVE = (string)dr["FCACTIVE"],
                                            FSDESCRIPTION = (string)dr["FSDESCRIPTION"],
                                            FCSECRET = (string)dr["FCSECRET"],
                                            FNDEP_ID = (int)dr["FNDEP_ID"],
                                            FSCREATED_BY = (string)dr["FSCREATED_BY"],
                                            FDCREATED_DATE = (DateTime)dr["FDCREATED_DATE"],
                                            FSUPDATED_BY = (string)dr["FSUPDATED_BY"],
                                            FDUPDATED_DATE = (DateTime)dr["FDUPDATED_DATE"],
                                            FSUSER_TITLE = (string)dr["FSUSER_TITLE"],
                                            FNUPPERDEPT_ID = (int)dr["FNUpperDept_ID"],
                                            FNGROUP_ID = (int)dr["FNGROUP_ID"],
                                            FSDEP_CHTNAME = (string)dr["FSDEP_CHTNAME"],
                                            FSGROUP_CHTNAME = (string)dr["FSGROUP_CHTNAME"],
                                            FSUPPERDEPT_CHTNAME = (string)dr["FSUPPERDEPT_CHTNAME"],
                                            FSTITLEOFDEPT_CHTNAME = department
                                    //(string)dr["FSUPPERDEPT_CHTNAME"] + "/" + (string)dr["FSDEP_CHTNAME"] + "/" + (string)dr["FSGROUP_CHTNAME"]
                                       //20140902 by Jarvis 
                                        }
                                    );
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                return null;
            }

            return UserList;

        }

        [WebMethod]
        public ObservableCollection<Class.Class_SIGNATURE_PRIORITY> GetSignaturePriority()
        {
            ObservableCollection<Class.Class_SIGNATURE_PRIORITY> rtnCollection = new ObservableCollection<Class.Class_SIGNATURE_PRIORITY>();
            List<Dictionary<string, string>> sqlResult;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_SIGNATURE_PRIORITY", sqlParameters, out sqlResult))
                return new ObservableCollection<Class.Class_SIGNATURE_PRIORITY>();
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class.Class_SIGNATURE_PRIORITY obj = new Class.Class_SIGNATURE_PRIORITY();
                //
                obj.FNDEPID = sqlResult[i]["FNDEP_ID"];
                obj.FSFullName_ChtName = sqlResult[i]["FSFullName_ChtName"];
                obj.FSSignatureID = sqlResult[i]["FSSignatureID"];
                obj.FSUSER_ChtName = sqlResult[i]["FSUSER_ChtName"];
                rtnCollection.Add(obj);
            }
            return rtnCollection;
        }
        [WebMethod]
        public bool InsSignaturePriority(Class.Class_SIGNATURE_PRIORITY SD, string UserId)
        {
            bool IsSuccess = false;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            //
            sqlParameters.Add("FNDEP_ID", SD.FNDEPID);
            sqlParameters.Add("FSSignatureID", SD.FSSignatureID);
            //
            IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_SIGNATURE_PRIORITY", sqlParameters, UserId);
            return IsSuccess;
        }

        [WebMethod]
        public bool DelSignaturePriority(Class.Class_SIGNATURE_PRIORITY SD, string UserId)
        {
            bool IsSuccess = false;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            //
            sqlParameters.Add("FNDEP_ID", SD.FNDEPID);
            sqlParameters.Add("FSSignatureID", SD.FSSignatureID);
            //
            IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_SIGNATURE_PRIORITY", sqlParameters, UserId);
            return IsSuccess;
        }

        /// <summary>
        /// 冠宇修改 用SP的方式去處理後台的資料庫
        /// </summary>
        /// <param name="TreeViewDept_ID">部門代號</param>
        /// <returns></returns>
        [WebMethod]
        public List<UserStruct> GetUserListByTreeViewDept(string TreeViewDept_ID)
        {
            List<UserStruct> rtnList = new List<UserStruct>();

            //
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FNTreeViewID", TreeViewDept_ID);
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBUSER_QueryByTreeViewDept", sqlParameters, out sqlResult))
                return new List<UserStruct>();
            for (int i = 0; i < sqlResult.Count; i++)
            {
                UserStruct obj = new UserStruct();
                //
                obj.FSUSER_ID = sqlResult[i]["FSUSER_ID"];
                obj.FSUSER = sqlResult[i]["FSUSER"];
                obj.FSUSER_ChtName = sqlResult[i]["FSUSER_ChtName"];
                obj.FSPASSWD = sqlResult[i]["FSPASSWD"];
                obj.FSDEPT = sqlResult[i]["FSDEPT"];
                obj.FSEMAIL = sqlResult[i]["FSEMAIL"];
                obj.FCACTIVE = sqlResult[i]["FCACTIVE"];
                obj.FSDESCRIPTION = sqlResult[i]["FSDESCRIPTION"];
                obj.FCSECRET = sqlResult[i]["FCSECRET"];
                obj.FNDEP_ID = Convert.ToInt32(sqlResult[i]["FNDep_ID"]);
                obj.FSCREATED_BY = sqlResult[i]["FSCREATED_BY"];
                obj.FDCREATED_DATE = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                obj.FSUPDATED_BY = sqlResult[i]["FSUPDATED_BY"];
                obj.FDUPDATED_DATE = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                obj.FSUSER_TITLE = sqlResult[i]["FSUSER_TITLE"];
                obj.FNUPPERDEPT_ID = Convert.ToInt32(sqlResult[i]["FNUpperDept_ID"]);
                obj.FNGROUP_ID = Convert.ToInt32(sqlResult[i]["FNGroup_ID"]);
                obj.FSDEP_CHTNAME = sqlResult[i]["FSDep_ChtName"];
                obj.FSGROUP_CHTNAME = sqlResult[i]["FSGroup_ChtName"];
                obj.FSUPPERDEPT_CHTNAME = sqlResult[i]["FSUpperDept_ChtName"];
                obj.FNTreeViewDept_ID = Convert.ToInt32(sqlResult[i]["FNTreeViewDept"]);
                string department = obj.FSUPPERDEPT_CHTNAME == "MAM內部帳號" ? obj.FSUPPERDEPT_CHTNAME + "(" + sqlResult[i]["FNTreeViewDept_ChtName"] + ")" : obj.FSUPPERDEPT_CHTNAME + "／" + obj.FSDEP_CHTNAME + "／" + obj.FSGROUP_CHTNAME;
                if (department[department.Length - 1] == '／')
                {
                    department = department.Remove(department.Length - 1);
                }
                obj.FSTITLEOFDEPT_CHTNAME = department;
                //;// + "/" + sqlResult[i]["FSDep_ChtName"] + "/" + sqlResult[i]["FSGroup_ChtName"];
                //20140902 by Jarvis
                rtnList.Add(obj);
            }
            //
            return rtnList;
        }

        [WebMethod]
        public bool UpdUserInfo(UserStruct userInfo,string UserId)
        {
            //List<UserStruct> rtnList = new List<UserStruct>();

            bool IsSuccess = false;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            //
            sqlParameters.Add("FSUSER_ID", userInfo.FSUSER_ID);
            sqlParameters.Add("FSUSER", userInfo.FSUSER);
            if (userInfo.FSPASSWD == "")
            {
                sqlParameters.Add("FSPASSWD", "");
            }
            else if (userInfo.FSPASSWD == "\\\\")
            {
                sqlParameters.Add("FSPASSWD", "\\\\");
            }
            else
            {
                sqlParameters.Add("FSPASSWD", MakeMD5Str.MAMFunctions.MakeMD5Str(userInfo.FSPASSWD));
            }
            sqlParameters.Add("FSUSER_ChtName", userInfo.FSUSER_ChtName);
            sqlParameters.Add("FSUSER_TITLE", userInfo.FSUSER_TITLE);
            sqlParameters.Add("FSDEPT", userInfo.FSDEPT);
            sqlParameters.Add("FSEMAIL", userInfo.FSEMAIL);
            sqlParameters.Add("FCACTIVE", userInfo.FCACTIVE);
            sqlParameters.Add("FSDESCRIPTION", userInfo.FSDESCRIPTION);
            sqlParameters.Add("FCSECRET", userInfo.FCSECRET);
            //sqlParameters.Add("FSCHANNEL_ID", userInfo.FSCHANNEL_ID);
            sqlParameters.Add("FNUpperDept_ID", userInfo.FNUPPERDEPT_ID.ToString());
            sqlParameters.Add("FNDep_ID", userInfo.FNDEP_ID.ToString());
            sqlParameters.Add("FNGroup_ID", userInfo.FNGROUP_ID.ToString());
            sqlParameters.Add("FNTreeViewDept", userInfo.FNTreeViewDept_ID.ToString());
            sqlParameters.Add("FNTreeViewDept_ChtName", userInfo.FSTITLEOFDEPT_CHTNAME);//利用借來的欄位塞部門中文名稱
            //sqlParameters.Add("FSUpperDept_ChtName", userInfo.FSUPPERDEPT_CHTNAME);
            //sqlParameters.Add("FSDep_ChtName", userInfo.FSDEP_CHTNAME);
            //sqlParameters.Add("FSGroup_ChtName", userInfo.FSGROUP_CHTNAME);
            sqlParameters.Add("FSUPDATED_BY", userInfo.FSUPDATED_BY);
            //

            IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBUSERS_INFO", sqlParameters, UserId);

            return IsSuccess;
        }

        //檢查帳號是否有重複 by Jarvis20130627
        [WebMethod]
        public bool GetUserAccountIsExist(string fsuser)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSUSER", fsuser);
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBUSER_ISEXIST", sqlParameters, out sqlResult))
                return false;

            return sqlResult.Count != 0;
        }

        [WebMethod]
        public bool InsUserMAM(UserStruct userInfo, string UserId)
        {
            //List<UserStruct> rtnList = new List<UserStruct>();

            bool IsSuccess = false;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            //
            sqlParameters.Add("FSUSER_ID", userInfo.FSUSER_ID);
            sqlParameters.Add("FSUSER", userInfo.FSUSER);
            sqlParameters.Add("FSPASSWD",MakeMD5Str.MAMFunctions.MakeMD5Str(userInfo.FSPASSWD));
            sqlParameters.Add("FSUSER_ChtName", userInfo.FSUSER_ChtName);
            sqlParameters.Add("FSUSER_TITLE", userInfo.FSUSER_TITLE);
            sqlParameters.Add("FSEMAIL", userInfo.FSEMAIL);
            sqlParameters.Add("FCACTIVE", userInfo.FCACTIVE);
            sqlParameters.Add("FSDESCRIPTION", userInfo.FSDESCRIPTION);
            sqlParameters.Add("FCSECRET", userInfo.FCSECRET);
            sqlParameters.Add("FNTreeViewDept", userInfo.FNTreeViewDept_ID.ToString());
            sqlParameters.Add("FNTreeViewDept_ChtName", userInfo.FNTreeViewDept_ChtName);
            sqlParameters.Add("FSCREATED_BY", UserId);
            sqlParameters.Add("FSUPDATED_BY", UserId);
            //

            IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBUSER_MAM", sqlParameters, UserId);

            return IsSuccess;
        }

        [WebMethod]
        public bool InsUser_DEP_MAM(Class.Class_Dept DepInfo, string UserId)
        {
            //List<UserStruct> rtnList = new List<UserStruct>();

            bool IsSuccess = false;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            //
            sqlParameters.Add("FSDEP", DepInfo.FSDEP);
            sqlParameters.Add("FNPARENT_DEP_ID", DepInfo.FNPARENT_DEP_ID.ToString());
            sqlParameters.Add("FSSupervisor_ID", DepInfo.FSSupervisor_ID);
            sqlParameters.Add("FSUpDepName_ChtName", DepInfo.FSUpDepName_ChtName);
            sqlParameters.Add("FSDpeName_ChtName", DepInfo.FSDpeName_ChtName);
            sqlParameters.Add("FSFullName_ChtName", DepInfo.FSFullName_ChtName);
            sqlParameters.Add("FNDEP_LEVEL", DepInfo.FNDEP_LEVEL.ToString());
            sqlParameters.Add("FBIsDepExist", DepInfo.FBIsDepExist);
            sqlParameters.Add("FSDEP_MEMO", DepInfo.FSDEP_MEMO);
            
            //

            IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBUSER_DEP_MAM", sqlParameters, UserId);

            return IsSuccess;
        }

        [WebMethod]
        public bool UpdUser_DEP_MAM(ObservableCollection<UserStruct> UserList1, ObservableCollection<UserStruct> UserList2, int DepNo1, int DepNo2, string UpdUserId)
        {
            bool IsSuccess = false;
            foreach (var item in UserList1)
            {
                Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
                sqlParameters.Add("USER_ID", item.FSUSER_ID);
                sqlParameters.Add("UPDATE_BY", UpdUserId);
                sqlParameters.Add("USER_DEP_ID", DepNo1.ToString());
                IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBUSERS_TreeViewDept", sqlParameters, UpdUserId);
                if (IsSuccess == false)
                    return false;
            }
            foreach (var item in UserList2)
            {
                Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
                sqlParameters.Add("USER_ID", item.FSUSER_ID);
                sqlParameters.Add("UPDATE_BY", UpdUserId);
                sqlParameters.Add("USER_DEP_ID", DepNo2.ToString());
                IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBUSERS_TreeViewDept", sqlParameters, UpdUserId);
                if (IsSuccess == false)
                    return false;
            }
            return IsSuccess;
        }

        [WebMethod]
        public int GetUserIdIsExist(string userId)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSUSER_ID", userId);
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBUSER_ID_ISEXIST", sqlParameters, out sqlResult))
                return 99;
            int rtnCount = 99;
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class.Class_Dept obj = new Class.Class_Dept();
                //

                rtnCount = Convert.ToInt32(sqlResult[i]["IDCOUNT"]);
                
            }
            //
            return rtnCount;
        }

        [WebMethod]
        public List<Class.Class_Dept> GetDeptListAll()
        {
            List<Class.Class_Dept> rtnList = new List<Class.Class_Dept>();

            //
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            //sqlParameters.Add("FNTreeViewID", TreeViewDept_ID);
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBUSER_DEP_ALL", sqlParameters, out sqlResult))
                return new List<Class.Class_Dept>();
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class.Class_Dept obj = new Class.Class_Dept();
                //
                obj.FNDEP_ID = Convert.ToInt32(sqlResult[i]["FNDEP_ID"]);
                obj.FSDEP = sqlResult[i]["FSDEP"];
                obj.FNPARENT_DEP_ID = Convert.ToInt32(sqlResult[i]["FNPARENT_DEP_ID"]);
                obj.FSSupervisor_ID = sqlResult[i]["FSSupervisor_ID"];
                obj.FSUpDepName_ChtName = sqlResult[i]["FSUpDepName_ChtName"];
                obj.FSDpeName_ChtName = sqlResult[i]["FSDpeName_ChtName"];
                obj.FSFullName_ChtName = sqlResult[i]["FSFullName_ChtName"];
                obj.FNDEP_LEVEL = Convert.ToInt32(sqlResult[i]["FNDEP_LEVEL"]);
                obj.FBIsDepExist = sqlResult[i]["FBIsDepExist"];
                obj.FSDEP_MEMO = sqlResult[i]["FSDEP_MEMO"];
                rtnList.Add(obj);
            }
            //
            return rtnList;
        }


        [WebMethod(EnableSession = true)]        
        public ObservableCollection<UserStruct> GetUserListByDept(int FNGroup_ID)
        {
            ObservableCollection<UserStruct> UserList = new ObservableCollection<UserStruct>();
            try
            {
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    if (FNGroup_ID == -1)
                    {
                        cmd.CommandText = "SELECT FSUSER_ID, FSUSER, ISNULL(FSPASSWD, '')FSPASSWD ,ISNULL(FSDEPT, '') FSDEPT, isNULL(FSEMAIL, '')FSEMAIL , ISNULL(FCACTIVE, 'Y') FCACTIVE, ISNULL(FSDESCRIPTION, '')FSDESCRIPTION ,ISNULL(FCSECRET,1)FCSECRET , ISNULL(FNDEP_ID, 0) FNDEP_ID, isNULL(FSCREATED_BY, '') FSCREATED_BY,isNULL(FDCREATED_DATE, getdate())FDCREATED_DATE ,ISNULL(FSUPDATED_BY, '')FSUPDATED_BY ,isnull(FDUPDATED_DATE, getdate()) FDUPDATED_DATE,ISNULL(FSUSER_TITLE, '')FSUSER_TITLE, ISNULL(FSUSER_CHTNAME, '')FSUSER_CHTNAME,  FNUpperDept_ID, FNGroup_ID, ISNULL(FSUpperDept_ChtName, '') FSUpperDept_ChtName,  isNULL(FSDep_ChtName, '') FSDep_ChtName, isNULL(FSGroup_ChtName, '') FSGroup_ChtName FROM TBUSERS ORDER BY FCACTIVE DESC  ";
                    }
                    else if (FNGroup_ID == 0)
                    {
                        return null; 
                    }
                    else
                    {
                        cmd.CommandText = "SELECT FSUSER_ID, FSUSER, ISNULL(FSPASSWD, '') FSPASSWD ,ISNULL(FSDEPT, '') FSDEPT, isNULL(FSEMAIL, '')FSEMAIL , ISNULL(FCACTIVE, 'Y') FCACTIVE, ISNULL(FSDESCRIPTION, '')FSDESCRIPTION ,ISNULL(FCSECRET,1)FCSECRET , ISNULL(FNDEP_ID, 0) FNDEP_ID, isNULL(FSCREATED_BY, '') FSCREATED_BY,isNull(FDCREATED_DATE,getdate())FDCREATED_DATE ,ISNULL(FSUPDATED_BY, '')FSUPDATED_BY , isNULL(FDUPDATED_DATE, getdate()) FDUPDATED_DATE , ISNULL(FSUSER_TITLE, '')FSUSER_TITLE , ISNULL(FSUSER_CHTNAME, '')FSUSER_CHTNAME,  FNUpperDept_ID, FNGroup_ID, ISNULL(FSUpperDept_ChtName, '') FSUpperDept_ChtName,  isNULL(FSDep_ChtName, '') FSDep_ChtName, isNULL(FSGroup_ChtName, '') FSGroup_ChtName FROM TBUSERS WHERE FNGroup_ID = " + FNGroup_ID + " ORDER BY FCACTIVE DESC ";
                    }

                    cn.Open();


                
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                UserList.Add
                                (
                                    new UserStruct
                                    {
                                        FSUSER_ID = (string)dr["FSUSER_ID"],
                                        FSUSER = (string)dr["FSUSER"],
                                        FSUSER_ChtName = (string)dr["FSUSER_ChtName"],
                                        FSPASSWD = (string)dr["FSPASSWD"],
                                        FSDEPT = (string)dr["FSDEPT"],
                                        FSEMAIL = (string)dr["FSEMAIL"],
                                        FCACTIVE = (string)dr["FCACTIVE"],
                                        FSDESCRIPTION = (string)dr["FSDESCRIPTION"],
                                        FCSECRET = (string)dr["FCSECRET"],
                                        FNDEP_ID = (int)dr["FNDEP_ID"],
                                        FSCREATED_BY = (string)dr["FSCREATED_BY"],
                                        FDCREATED_DATE = (DateTime)dr["FDCREATED_DATE"],
                                        FSUPDATED_BY = (string)dr["FSUPDATED_BY"],
                                        FDUPDATED_DATE = (DateTime)dr["FDUPDATED_DATE"],
                                        FSUSER_TITLE = (string)dr["FSUSER_TITLE"],
                                        FNUPPERDEPT_ID = (int)dr["FNUpperDept_ID"],
                                        FNGROUP_ID = (int)dr["FNGROUP_ID"],
                                        FSDEP_CHTNAME = (string)dr["FSDEP_CHTNAME"],
                                        FSGROUP_CHTNAME = (string)dr["FSGROUP_CHTNAME"],
                                        FSUPPERDEPT_CHTNAME = (string)dr["FSUPPERDEPT_CHTNAME"]
                                    }
                                );


                            }
                        }
                    }

                   
                   
                }

            }
            catch (Exception ex)
            {

                return null;
            }

            return UserList;
        }


        [WebMethod(EnableSession = true)]        
        public ObservableCollection<DeptStruct> GetDeptList(int FNPARENT_DEP_ID)
        {
            ObservableCollection<DeptStruct> DeptStructList = new ObservableCollection<DeptStruct>();
            try
            {
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;



                    if (FNPARENT_DEP_ID == -1)
                    {
                        cmd.CommandText = "SELECT FNDEP_ID, FSDEP, ISNULL(FNPARENT_DEP_ID, -1) FNPARENT_DEP_ID, ISNULL(FNDEP_LEVEL,0) FNDEP_LEVEL, ISNULL(FSDEP_MEMO, '') FSDEP_MEMO, ISNULL(FSSupervisor_ID, '-1') FSSupervisor_ID, ISNULL(FSUpDepName_ChtName, '') FSUpDepName_ChtName, ISNULL(FSDpeName_ChtName, '') FSDpeName_ChtName, ISNULL(FSFullName_ChtName, '') FSFullName_ChtName, ISNULL(FBIsDepExist, '') FBIsDepExist  FROM TBUSER_DEP WHERE FNPARENT_DEP_ID is null";
                    }
                    else
                    {
                        cmd.CommandText = "SELECT FNDEP_ID, FSDEP, FNPARENT_DEP_ID,  ISNULL(FNDEP_LEVEL,0) FNDEP_LEVEL, ISNULL(FSDEP_MEMO, '') FSDEP_MEMO, ISNULL(FSSupervisor_ID, '-1') FSSupervisor_ID, ISNULL(FSUpDepName_ChtName, '') FSUpDepName_ChtName, ISNULL(FSDpeName_ChtName, '') FSDpeName_ChtName, ISNULL(FSFullName_ChtName, '') FSFullName_ChtName, ISNULL(FBIsDepExist, '') FBIsDepExist FROM TBUSER_DEP WHERE FNPARENT_DEP_ID = " + FNPARENT_DEP_ID;
                    }

                    cn.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                DeptStructList.Add
                                (
                                    new DeptStruct
                                    {
                                        FNDEP_ID = dr.GetInt32(0),
                                        FSDEP = dr.GetString(1),
                                        FNPARENT_DEP_ID = dr.GetInt32(2),
                                        FNDEP_LEVEL = dr.GetInt32(3),
                                        FSDEP_MEMO = dr.GetString(4),

                                        FSSupervisor_ID = dr.GetString(5),
                                        FSUpDepName_ChtName = dr.GetString(6),
                                        FSDpeName_ChtName = dr.GetString(7),
                                        FSFullName_ChtName = dr.GetString(8),
                                        FBIsDepExist = dr.GetString(9)
                                    }
                                );

                            }
                        }
                    }
                    cn.Close();

                }
            }
            catch (Exception ex)
            {

                return null;
            }


            return DeptStructList;
        }

    }
}
