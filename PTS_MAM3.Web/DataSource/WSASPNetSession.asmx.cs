﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSASPNetSession 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSASPNetSession : System.Web.Services.WebService
    {

        [WebMethod(EnableSession=true)]
        public List<UserStruct> GetUserSession(string SessionName)
        {

            // Add your operation implementation here
            List<UserStruct> SessionList = new List<UserStruct>();

            if (System.Web.HttpContext.Current.Session[SessionName] != null)
            {
                SessionList.Add((UserStruct)System.Web.HttpContext.Current.Session[SessionName]);
            }
            else
                SessionList = null;

            return SessionList;
        }


        [WebMethod(EnableSession = true)]
        public bool SetSession(string SessionName, Object SessionValue)
        {

            // Add your operation implementation here

            System.Web.HttpContext.Current.Session[SessionName] = SessionValue;

            return true;
        }

        [WebMethod(EnableSession = true)]
        public string GetSession(string SessionName)
        {

            // Add your operation implementation here
            string SessionValue = "";

            if (System.Web.HttpContext.Current.Session[SessionName] != null)
            {
                SessionValue = System.Web.HttpContext.Current.Session[SessionName].ToString();

            }
            else
                SessionValue = null;

            return SessionValue;
        }

    }
}
