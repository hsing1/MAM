﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.OleDb;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WriteExcel 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WriteExcel : System.Web.Services.WebService
    {

        [WebMethod]
        public string ImportExcel(string ImportFileName,string ChannelID)
        {
            string path = HttpContext.Current.Server.MapPath("~/UploadFolder/" + ImportFileName);
            Boolean aaa = false;

            DataTable MyDt = new DataTable();
            MyDt = ImportExcel(path, aaa);


            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            SqlTransaction tran = null;
            SqlCommand sqlcmd = new SqlCommand("", connection);

            sqlcmd.CommandType = CommandType.StoredProcedure;

            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                tran = connection.BeginTransaction();
                sqlcmd.Transaction = tran;

                //參數剖析
                SqlConnection myconnection1 = new SqlConnection(sqlConnStr);
                SqlDataReader myReader1;
                SqlCommand mycom = new SqlCommand();
                mycom.Connection = myconnection1;
                myconnection1.Open();

                //sqlcmd.CommandText = "SP_D_TBPGM_LOUTH_KEY";
                //sqlcmd.Parameters.Clear(); 
                //SqlParameter paraD = new SqlParameter("@FSCHANNEL_ID", ChannelID);
                //sqlcmd.Parameters.Add(paraD);

                //if (sqlcmd.ExecuteNonQuery() == 0)
                //{
                //    //tran.Rollback();
                //    //return false;
                //}
                int i = 0;
                int ModifyCOunt = 0;
                int InsertCount = 0;
                string ModifyMessage = "";
                for (i = 1; i < MyDt.Rows.Count; i++)
                {
                   
                    DataRow TempRow = MyDt.Rows[i];
                    int level = 0;
                    //當FSLAYEL為空白時設定預設Level資料
                    if (TempRow[4].ToString() == "")
                    {

                        if (TempRow[0].ToString() == "片名")
                            level = 1; //片名 : 1
                        if (TempRow[0].ToString() == "雙語")
                            level = 1; //雙語 : 1
                        if (TempRow[0].ToString() == "結尾")
                            level = 3; //雙語 : 1
                    
                        if (TempRow[0].ToString() == "其他") //其他 :
                        {
                            if (TempRow[1].ToString().IndexOf("LOGO") > -1 && TempRow[1].ToString().IndexOf("短") > -1)
                                level = 3; //  LGOO : 有"短" ==>放第三層
                            else if (TempRow[1].ToString().IndexOf("LOGO") > -1)
                                level = 4; //  LOGO : 4
                            else if (TempRow[1].ToString().IndexOf("時間") > -1)
                                level = 2; //  時間 : 1

                            else if (TempRow[1].ToString().IndexOf("Live") > -1 || TempRow[1].ToString().IndexOf("LIVE") > -1)
                                level = 1;

                            else if (TempRow[1].ToString().IndexOf("立體聲") > -1)
                                level = 1;

                            else if (TempRow[1].ToString().IndexOf("重播") > -1)
                                level = 1;
                            else if (TempRow[1].ToString().IndexOf("錄影轉播") > -1)
                                level = 1;                           
                            else
                                level = 1; 

                        }

                        //SqlParameter para4 = new SqlParameter("@FSLAYEL", level.ToString());
                        //sqlcmd.Parameters.Add(para4);
                    }
                    else
                    {
                        level = int.Parse(TempRow[4].ToString()); 
                        //SqlParameter para4 = new SqlParameter("@FSLAYEL", TempRow[4].ToString());
                        //sqlcmd.Parameters.Add(para4);
                    }

                    //SP_Q_TBPGM_LOUTH_KEY_BY_NO_CHANNEL_LAYEL

                    string ssqltxt1 = "";

                    ssqltxt1 = "SELECT FSCHANNEL_ID,FSGROUP,FSNAME,FSNO,FSMEMO,FSLayel,FSRULE FROM TBPGM_LOUTH_KEY where FSNO='";
                    ssqltxt1 = ssqltxt1 + TempRow[2].ToString() + "' and FSCHANNEL_ID='" + ChannelID + "' and FSLayel='" + level.ToString() + "'";
                    //SqlDataAdapter daQUEUE_INSERT_TAPE = new SqlDataAdapter("SP_Q_TBPGM_LOUTH_KEY_BY_NO_CHANNEL_LAYEL", myconnection1);
                    mycom.CommandText = ssqltxt1;
                    //mycom.CommandText = "SP_Q_TBPGM_LOUTH_KEY_BY_NO_CHANNEL_LAYEL";
                    //mycom.Parameters.Clear();
                    //SqlParameter QPara1 = new SqlParameter("@FSNO", TempRow[2].ToString());
                    //mycom.Parameters.Add(QPara1);
                    //SqlParameter QPara2 = new SqlParameter("@FSCHANNEL_ID",ChannelID);
                    //mycom.Parameters.Add(QPara2);
                    //SqlParameter QPara3 = new SqlParameter("@FSLayel", level.ToString());
                    //mycom.Parameters.Add(QPara3);


                    myReader1 = mycom.ExecuteReader();
                    if (myReader1.HasRows == true)
                    {
                        while (myReader1.Read())
                        {
                            if (myReader1.IsDBNull(0) != true) //代表有資料,要修改,要先檢查資料是不是和目前一樣,如果相同就不用修改
                            {
                                string TestText = "";
                                try
                                { TestText = myReader1.GetString(2); }
                                catch
                                { TestText = ""; }
                                if (TestText != TempRow[1].ToString() && TempRow[1].ToString()!="")
                                {
                                    sqlcmd.Parameters.Clear();
                                    sqlcmd.CommandText = "SP_U_TBPGM_LOUTH_KEY";
                                    SqlParameter paraC = new SqlParameter("@FSCHANNEL_ID", ChannelID);
                                    sqlcmd.Parameters.Add(paraC);

                                    SqlParameter para = new SqlParameter("@FSGROUP", TempRow[0].ToString());
                                    sqlcmd.Parameters.Add(para);
                                    SqlParameter para1 = new SqlParameter("@FSNAME", TempRow[1].ToString());
                                    sqlcmd.Parameters.Add(para1);
                                    SqlParameter para2 = new SqlParameter("@FSNO", TempRow[2].ToString());
                                    sqlcmd.Parameters.Add(para2);
                                    SqlParameter para3 = new SqlParameter("@FSMEMO", TempRow[3].ToString());
                                    sqlcmd.Parameters.Add(para3);
                                    SqlParameter para4 = new SqlParameter("@FSLAYEL", level.ToString());
                                    sqlcmd.Parameters.Add(para4);
                                    SqlParameter para5 = new SqlParameter("@FSRULE", TempRow[5].ToString());
                                    sqlcmd.Parameters.Add(para5);
                                    if (sqlcmd.ExecuteNonQuery() == 0)
                                    {
                                        tran.Rollback();
                                        return "Error";
                                    }
                               

                                    ModifyCOunt = ModifyCOunt + 1;
                                    ModifyMessage = ModifyMessage + "序號:" + TempRow[2].ToString() + "第" + level.ToString() + "層有重複,將" + TestText + "置換為" + TempRow[1].ToString();
                                }
                            }
                            else //代表沒有資料要新增
                            {

                            }
                        }
                    }
                    else  //代表沒有資料要新增
                    {
                        if (TempRow[1].ToString() != "") //如果名稱為空值則不要寫入
                        {
                            sqlcmd.Parameters.Clear();
                            sqlcmd.CommandText = "SP_I_TBPGM_LOUTH_KEY";
                            SqlParameter paraC = new SqlParameter("@FSCHANNEL_ID", ChannelID);
                            sqlcmd.Parameters.Add(paraC);

                            SqlParameter para = new SqlParameter("@FSGROUP", TempRow[0].ToString());
                            sqlcmd.Parameters.Add(para);
                            SqlParameter para1 = new SqlParameter("@FSNAME", TempRow[1].ToString());
                            sqlcmd.Parameters.Add(para1);
                            SqlParameter para2 = new SqlParameter("@FSNO", TempRow[2].ToString());
                            sqlcmd.Parameters.Add(para2);
                            SqlParameter para3 = new SqlParameter("@FSMEMO", TempRow[3].ToString());
                            sqlcmd.Parameters.Add(para3);
                            SqlParameter para4 = new SqlParameter("@FSLAYEL", level.ToString());
                            sqlcmd.Parameters.Add(para4);
                            SqlParameter para5 = new SqlParameter("@FSRULE", TempRow[5].ToString());
                            sqlcmd.Parameters.Add(para5);
                            if (sqlcmd.ExecuteNonQuery() == 0)
                            {
                                tran.Rollback();
                                return "Error";
                            }
                            InsertCount = InsertCount + 1;
                        }
                    }

                    myReader1.Close();
                   
                    //    sb.AppendLine("<FSGROUP>" + TempRow[0].ToString() + "</FSGROUP>");
                    //    sb.AppendLine("<FSNAME>" + TempRow[1].ToString() + "</FSNAME>");
                    //    sb.AppendLine("<FSNO>" + TempRow[2].ToString() + "</FSNO>");
                    //    sb.AppendLine("<FSMEMO>" + TempRow[3].ToString() + "</FSMEMO>");
                    //    sb.AppendLine("<FSLAYEL>" + TempRow[4].ToString() + "</FSLAYEL>");
                    //    sb.AppendLine("<FSRULE>" + TempRow[5].ToString() + "</FSRULE>");
                }
                tran.Commit();
                myconnection1.Close();
                return "匯入完成,總共匯入" + InsertCount.ToString() + "筆資料,修改" + ModifyCOunt.ToString() + "筆資料," + ModifyMessage;
            }
            catch
            {
                tran.Rollback();
                //myconnection1.Close();
                return "匯入失敗";
            }
            finally
            {
                connection.Close();
                connection.Dispose();

                sqlcmd.Dispose();
                GC.Collect();

            }

            
            return "ok";
        }



        public static System.Data.DataTable ImportExcel(string Path, bool HasFieldName)
        {
            try
            {
                System.Collections.Generic.List<string> sTBList = new System.Collections.Generic.List<string>();
                string strConn;
                /*  strConn = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                  "Data Source=" + FName +
                  ";Extended Properties=Excel 8.0;";*/
                if (HasFieldName)
                    /*如果Excel中的第一列為欄名,則寫成*/
                    strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Path + ";Extended Properties=\"Excel 8.0;HDR=YES;IMEX=1;\"";
                else
                    strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Path + ";Extended Properties=\"Excel 8.0;HDR=NO;IMEX=1;\"";
                OleDbConnection odc = new OleDbConnection(strConn);
                odc.Open();
                DataTable dt = odc.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {

                        //if (dr["TABLE_NAME"].ToString()=="Sheet1$")
                        sTBList.Add(dr["TABLE_NAME"].ToString());

                        OleDbDataAdapter myCommand = new OleDbDataAdapter("SELECT * FROM [" + sTBList[0] + "]", strConn);
                        //OleDbDataAdapter myCommand = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", strConn);
                        System.Data.DataTable myDataSet = new System.Data.DataTable();
                        myCommand.Fill(myDataSet);
                        odc.Close();
                        if (myDataSet.Rows.Count >= 2)
                            return myDataSet;
                        else
                            sTBList.Clear();
                    }
                }
                //OleDbDataAdapter myCommand = new OleDbDataAdapter("SELECT * FROM [" + sTBList[0] + "]", strConn);
                ////OleDbDataAdapter myCommand = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", strConn);
                //System.Data.DataTable myDataSet = new System.Data.DataTable();
                //myCommand.Fill(myDataSet);
                //odc.Close();
                //return myDataSet;
                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
