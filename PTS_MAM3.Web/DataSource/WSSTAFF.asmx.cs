﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using PTS_MAM3.Web.Class;
using System.Text;
using System.Xml.Linq;
using System.IO;
using System.Data;
using System.Web.Configuration;
using System.Data.SqlClient;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSSTAFF 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSSTAFF : System.Web.Services.WebService
    {
        public struct ReturnMsg     //錯誤訊息處理
        {
            public bool bolResult;
            public string strErrorMsg;
        }

        //解析資料庫回來的Dictionary，轉存成Class
        private List<Class_STAFF> Transfer_STAFF(List<Dictionary<string, string>> sqlResult)
        {
            List<Class_STAFF> returnData = new List<Class_STAFF>();   

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_STAFF obj = new Class_STAFF();
                // -------------------------------- 
                obj.FSSTAFFID = sqlResult[i]["FSSTAFFID"];                      //工作人員代碼   
                obj.FSNAME = sqlResult[i]["FSNAME"];                            //姓名 
                obj.FSENAME = sqlResult[i]["FSENAME"];                          //外文姓名
                obj.FSTEL = sqlResult[i]["FSTEL"];                              //電話
                obj.FSFAX = sqlResult[i]["FSFAX"];                              //傳真
                obj.FSEMAIL = sqlResult[i]["FSEMAIL"];                          //E-Mail 地址
                obj.FSADDRESS = sqlResult[i]["FSADDRESS"];                      //地址
                obj.FSWEBSITE = sqlResult[i]["FSWEBSITE"];                      //個人網站
                obj.FSINTRO = sqlResult[i]["FSINTRO"];                          //簡介
                obj.FSEINTRO = sqlResult[i]["FSEINTRO"];                        //外文簡介
                obj.FSMEMO = sqlResult[i]["FSMEMO"];                            //備註

                if (sqlResult[i]["FSCRTUSER"] == "01065")                       //建檔者
                    obj.FSCRTUSER = "MAM系統使用者";
                else if (sqlResult[i]["FSCRTUSER"]=="")
                    obj.FSCRTUSER = "";
                else
                    obj.FSCRTUSER = "節目管理系統";
                //obj.FSCRTUSER = sqlResult[i]["FSCRTUSER"];//因為此資料表是放在節目管理系統，因此MAM系統無儲存誰修改
                                                      
                obj.FDCRTDATE = Convert.ToDateTime(sqlResult[i]["WANT_FDCREATEDDATE"]);     //建檔日期
                obj.SHOW_FDCRTDATE = MAMFunctions.compareDate(sqlResult[i]["WANT_FDCREATEDDATE"]);//建檔日期(顯示)

                if (sqlResult[i]["FSUPDUSER"] == "01065")                       //修改者
                    obj.FSUPDUSER = "MAM系統使用者";
                else if (sqlResult[i]["FSUPDUSER"] == "")
                    obj.FSUPDUSER = "";
                else
                    obj.FSUPDUSER = "節目管理系統";
                //obj.FSUPDUSER = sqlResult[i]["FSUPDUSER"];//因為此資料表是放在節目管理系統，因此MAM系統無儲存誰修改
                
                //obj.FDUPDDATE = Convert.ToDateTime(sqlResult[i]["WANT_FDUPDATEDDATE"]);     //修改日期
                obj.SHOW_FDUPDDATE = MAMFunctions.compareDate(sqlResult[i]["WANT_FDUPDATEDDATE"]);//修改日期(顯示)               
                // --------------------------------
                returnData.Add(obj);
            }

            return returnData;
        }

        //工作人員檔的所有資料
        [WebMethod]
        public List<Class_STAFF> GetTBSTAFF_ALL()
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBSTAFF_ALL_EXTERNAL", sqlParameters, out sqlResult))
                return new List<Class_STAFF>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_STAFF(sqlResult);
        }

        //查詢工作人員檔
        [WebMethod]
        public List<Class_STAFF> QUERY_TBSTAFF(Class.Class_STAFF obj)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSSTAFFID", obj.FSSTAFFID);
            source.Add("FSNAME", obj.FSNAME);
            source.Add("FSENAME", obj.FSENAME);
            source.Add("FSTEL", obj.FSTEL);
            source.Add("FSFAX", obj.FSFAX);
            source.Add("FSEMAIL", obj.FSEMAIL);
            source.Add("FSADDRESS", obj.FSADDRESS);
            source.Add("FSWEBSITE", obj.FSWEBSITE);
            source.Add("FSINTRO", obj.FSINTRO);
            source.Add("FSEINTRO", obj.FSEINTRO);
            source.Add("FSMEMO", obj.FSMEMO);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBSTAFF_BYCONDITIONS_EXTERNAL", source, out sqlResult))
                return new List<Class_STAFF>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_STAFF(sqlResult);
        }

        //查詢工作人員檔(避免重複Key)
        [WebMethod]
        public string QUERY_TBSTAFF_CHECK(Class.Class_STAFF obj)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSSTAFFID", obj.FSSTAFFID);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBSTAFF_BYCHECK_EXTERNAL", source, out sqlResult))
                return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count > 0)
                return sqlResult[0]["dbCount"];
            else
                return "";
        }

        //寄送EMAIL通知_資料同步時發生問題要通知管理者
        [WebMethod]
        public void EMAIL_STAFF_Error(Class.Class_STAFF obj, string strError, string strSystem, string strStatus)
        {
            //讀取通知的管理者(暫用)   
            MAM_PTS_DLL.SysConfig objDll = new MAM_PTS_DLL.SysConfig();
            string strErrorMail = objDll.sysConfig_Read("/ServerConfig/SynchronizeError").Trim();
            
            string strTitle = "";

            StringBuilder msg = new StringBuilder();
            msg.AppendLine("工作人員代碼：" + obj.FSSTAFFID.Trim());
            msg.AppendLine(" , 姓名：" + obj.FSNAME.Trim());

            //if (strSystem.Trim() == "MAM")
            //    msg.AppendLine(" , 同步失敗資料庫：數位片庫");
            //else if (strSystem.Trim() == "PGM")
            msg.AppendLine(" , 同步失敗資料庫：節目管理系統");

            if (strStatus.Trim() == "INSERT")
                msg.AppendLine(" , 動作：新增");
            else if (strStatus.Trim() == "UPDATE")
                msg.AppendLine(" , 動作：修改");
            else if (strStatus.Trim() == "DELETE")
                msg.AppendLine(" , 動作：刪除");

            msg.AppendLine(" , 錯誤訊息：" + strError);

            msg.AppendLine(" , 新增者：" + obj.FSCRTUSER.Trim() + " , 修改者：" + obj.FSUPDUSER.Trim());

            strTitle = "節目工作人員資料同步失敗通知-公司/人員 姓名" + obj.FSNAME.Trim();

            MAM_PTS_DLL.Protocol.SendEmail(strErrorMail, strTitle, msg.ToString());
        }

        //Dennis.Wen改過的========================================================================================================

        //新增節目管理系統的工作人員檔
        [WebMethod]
        public String INSERT_TBSTAFF_PGM(Class.Class_STAFF clSTAFF)
        {
            String sqlConnStr = WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            String strResult = "";

            try
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_I_TBSTAFF_EXTERNAL", connection);
                DataTable dt = new DataTable();

                da.SelectCommand.Parameters.Add(new SqlParameter("@FSSTAFFID", clSTAFF.FSSTAFFID));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSNAME", clSTAFF.FSNAME));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSENAME", clSTAFF.FSENAME));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSTEL", clSTAFF.FSTEL));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSFAX", clSTAFF.FSFAX));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSEMAIL", clSTAFF.FSEMAIL));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSADDRESS", clSTAFF.FSADDRESS));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSWEBSITE", clSTAFF.FSWEBSITE));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSINTRO", clSTAFF.FSINTRO));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSEINTRO", clSTAFF.FSEINTRO));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSMEMO", clSTAFF.FSMEMO));
                //da.SelectCommand.Parameters.Add(new SqlParameter("@FSCRTUSER", clSTAFF.FSCRTUSER));
                //da.SelectCommand.Parameters.Add(new SqlParameter("@FDCRTDATE", clSTAFF.FDCRTDATE));
                //da.SelectCommand.Parameters.Add(new SqlParameter("@FSUPDUSER", clSTAFF.FSUPDUSER));
                //da.SelectCommand.Parameters.Add(new SqlParameter("@FDUPDDATE", clSTAFF.FDUPDDATE));

                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                da.Fill(dt);

                if (dt.Rows.Count == 0)
                {
                    strResult = "ERROR:資料庫未回應執行結果";
                }
                else
                {
                    strResult = dt.Rows[0].ItemArray[0].ToString();
                }
            }
            catch (Exception ex)
            {
                strResult = "ERROR:" + ex.Message;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }

            return strResult;
        }

        //修改節目管理系統的節目主檔_傳入節目基本資料的的class
        [WebMethod]
        public String UPDATE_TBSTAFF_PGM(Class.Class_STAFF clSTAFF)
        {
            String sqlConnStr = WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            String strResult = "";

            try
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_U_TBSTAFF_EXTERNAL", connection);
                DataTable dt = new DataTable();

                da.SelectCommand.Parameters.Add(new SqlParameter("@FSSTAFFID", clSTAFF.FSSTAFFID));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSNAME", clSTAFF.FSNAME));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSENAME", clSTAFF.FSENAME));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSTEL", clSTAFF.FSTEL));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSFAX", clSTAFF.FSFAX));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSEMAIL", clSTAFF.FSEMAIL));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSADDRESS", clSTAFF.FSADDRESS));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSWEBSITE", clSTAFF.FSWEBSITE));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSINTRO", clSTAFF.FSINTRO));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSEINTRO", clSTAFF.FSEINTRO));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSMEMO", clSTAFF.FSMEMO));
                //da.SelectCommand.Parameters.Add(new SqlParameter("@FSCRTUSER", clSTAFF.FSCRTUSER));
                //da.SelectCommand.Parameters.Add(new SqlParameter("@FDCRTDATE", clSTAFF.FDCRTDATE));
                //da.SelectCommand.Parameters.Add(new SqlParameter("@FSUPDUSER", clSTAFF.FSUPDUSER));
                //da.SelectCommand.Parameters.Add(new SqlParameter("@FDUPDDATE", clSTAFF.FDUPDDATE));

                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                da.Fill(dt);

                if (dt.Rows.Count == 0)
                {
                    strResult = "ERROR:資料庫未回應執行結果";
                }
                else
                {
                    strResult = dt.Rows[0].ItemArray[0].ToString();
                }
            }
            catch (Exception ex)
            {
                strResult = "ERROR:" + ex.Message;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }

            return strResult;
        }

        //修改節目管理系統的節目主檔_傳入節目基本資料的的class
        [WebMethod]
        public String DELETE_TBSTAFF_PGM(String _FSSTAFFID)
        {
            String sqlConnStr = WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            String strResult = "";

            try
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_D_TBSTAFF_EXTERNAL", connection);
                DataTable dt = new DataTable();

                da.SelectCommand.Parameters.Add(new SqlParameter("@FSSTAFFID", _FSSTAFFID));

                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                da.Fill(dt);

                if (dt.Rows.Count == 0)
                {
                    strResult = "ERROR:資料庫未回應執行結果";
                }
                else
                {
                    strResult = dt.Rows[0].ItemArray[0].ToString();
                }
            }
            catch (Exception ex)
            {
                strResult = "ERROR:" + ex.Message;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }

            return strResult;
        }


        //修改節目管理系統的節目主檔_傳入節目基本資料的的class
        [WebMethod]
        public List<Class.Class_STAFF> QUERY_TBSTAFF_PGM_BYFILTER(Class.Class_STAFF clSTAFF)
        {
            String sqlConnStr = WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            List<Class.Class_STAFF> STAFFList = new List<Class.Class_STAFF>();

            try
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_Q_TBSTAFF_EXTERNAL_BYFILTER", connection);
                DataTable dt = new DataTable();

                da.SelectCommand.Parameters.Add(new SqlParameter("@FSSTAFFID", clSTAFF.FSSTAFFID));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSNAME", clSTAFF.FSNAME));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSENAME", clSTAFF.FSENAME));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSTEL", clSTAFF.FSTEL));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSFAX", clSTAFF.FSFAX));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSEMAIL", clSTAFF.FSEMAIL));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSADDRESS", clSTAFF.FSADDRESS));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSWEBSITE", clSTAFF.FSWEBSITE));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSINTRO", clSTAFF.FSINTRO));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSEINTRO", clSTAFF.FSEINTRO));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSMEMO", clSTAFF.FSMEMO));

                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                da.Fill(dt);

                if (dt.Rows.Count != 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        DateTime _d1,_d2;
                        string strCreated = "";
                        string strUpdated = "";
                        
                        if (dr["FDCRTDATE"].ToString() == "")
                        {
                            _d1 = DateTime.Parse("1900/01/01");
                        }
                        else
                        {
                            _d1 = DateTime.Parse( dr["FDCRTDATE"].ToString());
                        }

                        if (dr["FDUPDDATE"].ToString() == "")
                        {
                            _d2 = DateTime.Parse("1900/01/01");
                        }
                        else
                        {
                            _d2 = DateTime.Parse(dr["FDUPDDATE"].ToString());
                        }
                        
                        if (dr["FSCRTUSER"].ToString().Trim() == "01065")                       //建檔者
                            strCreated = "MAM系統使用者";
                        else if (dr["FSCRTUSER"].ToString().Trim() == "")
                            strCreated = "";
                        else
                            strCreated = "節目管理系統";

                        if (dr["FSUPDUSER"].ToString().Trim() == "01065")                       //修改者
                            strUpdated = "MAM系統使用者";
                        else if (dr["FSUPDUSER"].ToString().Trim() == "")
                            strUpdated = "";
                        else
                            strUpdated = "節目管理系統";

                        STAFFList.Add(
                            new Class.Class_STAFF
                            {
                                FSSTAFFID = dr["FSSTAFFID"].ToString(),
                                FSNAME = dr["FSNAME"].ToString(),
                                FSENAME = dr["FSENAME"].ToString(),
                                FSTEL = dr["FSTEL"].ToString(),
                                FSFAX = dr["FSFAX"].ToString(),
                                FSEMAIL = dr["FSEMAIL"].ToString(),
                                FSADDRESS = dr["FSADDRESS"].ToString(),
                                FSWEBSITE = dr["FSWEBSITE"].ToString(),
                                FSINTRO = dr["FSINTRO"].ToString(),
                                FSEINTRO = dr["FSEINTRO"].ToString(),
                                FSMEMO = dr["FSMEMO"].ToString(),
                                FSCRTUSER = strCreated , //dr["FSCRTUSER"].ToString(),
                                FDCRTDATE = _d1,
                                FSUPDUSER = strUpdated,//dr["FSUPDUSER"].ToString(),
                                FDUPDDATE = _d2,
                                SHOW_FDCRTDATE = dr["WANT_FDCREATEDDATE"].ToString(),
                                SHOW_FDUPDDATE = dr["WANT_FDUPDATEDDATE"].ToString()
                            }
                        );
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }

            return STAFFList;
        }


        //以下是修改前的

        ////新增節目管理系統的工作人員檔
        //[WebMethod]
        //public Boolean INSERT_TBSTAFF_PGM(Class.Class_STAFF obj, out string exceptionMsg)
        //{
        //    Boolean bolDB = false;          //寫入節目管理系統資料庫結果
        //    string strError = "";           //錯誤訊息

        //    Dictionary<string, string> source = new Dictionary<string, string>();
        //    source.Add("FSSTAFFID", obj.FSSTAFFID);
        //    source.Add("FSNAME", obj.FSNAME);
        //    source.Add("FSENAME", obj.FSENAME);
        //    source.Add("FSTEL", obj.FSTEL);
        //    source.Add("FSFAX", obj.FSFAX);
        //    source.Add("FSEMAIL", obj.FSEMAIL);
        //    source.Add("FSADDRESS", obj.FSADDRESS);
        //    source.Add("FSWEBSITE", obj.FSWEBSITE);
        //    source.Add("FSINTRO", obj.FSINTRO);
        //    source.Add("FSEINTRO", obj.FSEINTRO);
        //    source.Add("FSMEMO", obj.FSMEMO);

        //    bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction_With_DBLink("SP_I_TBSTAFF_EXTERNAL", source, out strError, obj.FSCRTUSER, true);
        //    exceptionMsg = strError;

        //    if (bolDB == true)
        //        return true;
        //    else
        //    {
        //        MAM_PTS_DLL.Log.AppendTrackingLog("WriteToPGM-INSERT STAFF", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "ERROR:" + strError);
        //        EMAIL_STAFF_Error(obj, strError, "PGM", "INSERT");                   //寄送Mail
        //        return false;
        //    }
        //}

        ////修改節目管理系統的節目主檔_傳入節目基本資料的的class
        //[WebMethod]
        //public Boolean UPDATE_TBSTAFF_PGM(Class.Class_STAFF obj, out string exceptionMsg)
        //{
        //    Boolean bolDB = false;          //寫入節目管理系統資料庫結果
        //    string strError = "";           //錯誤訊息

        //    Dictionary<string, string> source = new Dictionary<string, string>();
        //    source.Add("FSSTAFFID", obj.FSSTAFFID);
        //    source.Add("FSNAME", obj.FSNAME);
        //    source.Add("FSENAME", obj.FSENAME);
        //    source.Add("FSTEL", obj.FSTEL);
        //    source.Add("FSFAX", obj.FSFAX);
        //    source.Add("FSEMAIL", obj.FSEMAIL);
        //    source.Add("FSADDRESS", obj.FSADDRESS);
        //    source.Add("FSWEBSITE", obj.FSWEBSITE);
        //    source.Add("FSINTRO", obj.FSINTRO);
        //    source.Add("FSEINTRO", obj.FSEINTRO);
        //    source.Add("FSMEMO", obj.FSMEMO);

        //    bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction_With_DBLink("SP_U_TBSTAFF_EXTERNAL", source, out strError, obj.FSUPDUSER, true);
        //    exceptionMsg = strError;

        //    if (bolDB == true)
        //        return true;
        //    else
        //    {
        //        MAM_PTS_DLL.Log.AppendTrackingLog("WriteToPGM-UPDATE STAFF", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "ERROR:" + strError);
        //        EMAIL_STAFF_Error(obj, strError, "PGM", "UPDATE");                   //寄送Mail
        //        return false;
        //    }
        //}

        ////刪除節目管理系統的工作人員檔
        //[WebMethod]
        //public Boolean DELETE_TBSTAFF_PGM(Class.Class_STAFF obj, out string exceptionMsg, string strDelUser)
        //{
        //    Boolean bolDB = false;          //寫入節目管理系統資料庫結果
        //    string strError = "";           //錯誤訊息

        //    Dictionary<string, string> source = new Dictionary<string, string>();
        //    source.Add("FSSTAFFID", obj.FSSTAFFID);

        //    bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction_With_DBLink("SP_D_TBSTAFF_EXTERNAL", source, out strError, strDelUser, true);
        //    exceptionMsg = strError;

        //    if (bolDB == true)
        //        return true;
        //    else
        //    {
        //        MAM_PTS_DLL.Log.AppendTrackingLog("WriteToPGM-DELETE PROGLINK", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "ERROR:" + strError);
        //        EMAIL_STAFF_Error(obj, strError, "PGM", "DELETE");                   //寄送Mail
        //        return false;
        //    }
        //}
    }
}
