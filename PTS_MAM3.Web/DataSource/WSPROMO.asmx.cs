﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using PTS_MAM3.Web.Class;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSPROMO 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSPROMO : System.Web.Services.WebService
    {
        List<Class_CODE> m_CodeData = new List<Class_CODE>();       //代碼檔集合 
        List<Class_CODE> m_CodeCATD = new List<Class_CODE>();       //代碼檔集合(宣傳帶類型細項代碼檔) 
        List<Class_DELETE_MSG> m_DeleteMsg = new List<Class_DELETE_MSG>();  //檢查刪除節目主檔訊息集合

        public struct ReturnMsg     //錯誤訊息處理
        {
            public bool bolResult;
            public string strErrorMsg;
        }

        //解析資料庫回來的Dictionary，轉存成Class
        private List<Class_PROMO> Transfer_PGM_PROMO(List<Dictionary<string, string>> sqlResult)
        {
            List<Class_PROMO> returnData = new List<Class_PROMO>();
            m_CodeData = MAMFunctions.Transfer_Class_CODE("SP_Q_TBPGM_PROMO_CODE");
            m_CodeCATD = MAMFunctions.Transfer_Class_CODED("SP_Q_TBZPROMOCATD_ALL");      

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_PROMO obj = new Class_PROMO();
                // --------------------------------    
                obj.FSPROMO_ID = sqlResult[i]["FSPROMO_ID"];                                   //宣傳帶編碼
                obj.FSPROMO_NAME = sqlResult[i]["FSPROMO_NAME"];                               //宣傳帶中文名稱
                obj.FSPROMO_NAME_ENG = sqlResult[i]["FSPROMO_NAME_ENG"];                       //宣傳帶英文名稱
                obj.FSDURATION = sqlResult[i]["FSDURATION"];                                   //播出長度

                if (sqlResult[i]["FSDURATION"].Trim() == "")                                   //播出長度(顯示)
                    obj.SHOW_FSDURATION = "";
                else
                    obj.SHOW_FSDURATION = Convert.ToString(MAM_PTS_DLL.TimeCodeCalc.timecodetoSecond(sqlResult[i]["FSDURATION"].Trim())); 

                obj.FSPROG_ID = sqlResult[i]["FSPROG_ID"];                                     //節目編號
                obj.FSPROG_ID_NAME = sqlResult[i]["FSPROG_ID_NAME"];                           //節目名稱
                obj.FNEPISODE = Convert.ToInt16(sqlResult[i]["FNEPISODE"]);                    //集別
                obj.SHOW_FNEPISODE = MAMFunctions.compareNumber(sqlResult[i]["FNEPISODE"]);    //集別(顯示)
                obj.FNEPISODE_NAME = sqlResult[i]["FNEPISODE_NAME"];                           //子集名稱
                obj.FSPROMOTYPEID = sqlResult[i]["FSPROMOTYPEID"];                             //宣傳帶類型
                obj.FSPROGOBJID = sqlResult[i]["FSPROGOBJID"];                                 //製作目的代碼
                obj.FSPRDCENID = sqlResult[i]["FSPRDCENID"];                                   //製作單位代碼
                obj.FSPRDYYMM = sqlResult[i]["FSPRDYYMM"];                                     //製作年月                
                obj.FSACTIONID = sqlResult[i]["FSACTIONID"];                                   //宣傳帶活動代碼
                obj.FSPROMOCATID = sqlResult[i]["FSPROMOCATID"];                               //宣傳帶類型代碼
                obj.FSPROMOCATDID = sqlResult[i]["FSPROMOCATDID"];                             //宣傳帶類型細項代碼
                obj.FSPROMOVERID = sqlResult[i]["FSPROMOVERID"];                               //宣傳帶版本代碼
                obj.FSMAIN_CHANNEL_ID = sqlResult[i]["FSMAIN_CHANNEL_ID"];                     //頻道別代碼               
                obj.FSMEMO = sqlResult[i]["FSMEMO"];                                           //備註 
                obj.FSDEL = sqlResult[i]["FSDEL"];                                             //刪除註記
                obj.FSDELUSER = sqlResult[i]["FSDELUSER"];                                     //刪除者
                obj.FNDEP_ID = Convert.ToInt16(sqlResult[i]["FNDEP_ID"]);                      //成案單位
                obj.FSWELFARE = sqlResult[i]["FSWELFARE"];                                     //公益託播編號 
                
                obj.FSCREATED_BY_NAME = sqlResult[i]["FSCREATED_BY_NAME"];                                //建檔者
                obj.FDCREATED_DATE = Convert.ToDateTime(sqlResult[i]["WANT_FDCREATEDDATE"]);              //建檔日期
                obj.SHOW_FDCREATED_DATE = MAMFunctions.compareDate(sqlResult[i]["WANT_FDCREATEDDATE"]);   //建檔日期(顯示)
                obj.FSUPDATED_BY_NAME = sqlResult[i]["FSUPDATED_BY_NAME"];                                //修改者
                obj.FDUPDATED_DATE = Convert.ToDateTime(sqlResult[i]["WANT_FDUPDATEDDATE"]);              //修改日期
                obj.SHOW_FDUPDATED_DATE = MAMFunctions.compareDate(sqlResult[i]["WANT_FDUPDATEDDATE"]);   //修改日期(顯示)
                obj.FSDEL_BY_NAME = sqlResult[i]["FSDEL_BY_NAME"];                                        //刪除者(顯示)

                obj.FSPRDCEN_NAME = compareCode_Code(sqlResult[i]["FSPRDCENID"], "TBZPRDCENTER");         //製作單位代碼名稱
                obj.FSPROGOBJ_NAME = compareCode_Code(sqlResult[i]["FSPROGOBJID"], "TBZPROGOBJ");         //製作目的代碼名稱
                obj.FSPRDCEN_NAME = compareCode_Code(sqlResult[i]["FSPRDCENID"], "TBZPRDCENTER");         //製作單位代碼名稱
                obj.FSPROGOBJ_NAME = compareCode_Code(sqlResult[i]["FSPROGOBJID"], "TBZPROGOBJ");         //製作目的代碼名稱

                obj.FSPROMOVER_NAME = compareCode_Code(sqlResult[i]["FSPROMOVERID"], "TBZPROMOVER");      //宣傳帶版本名稱
                obj.FSPROMOTYPE_NAME = compareCode_Code(sqlResult[i]["FSPROMOTYPEID"], "TBZPROMOTYPE");   //宣傳帶類別名稱
                obj.FSACTION_NAME = compareCode_Code(sqlResult[i]["FSACTIONID"], "TBPGM_ACTION");         //宣傳帶活動名稱
                obj.FSPROMOCAT_NAME = compareCode_Code(sqlResult[i]["FSPROMOCATID"], "TBZPROMOCAT");      //宣傳帶類型代碼名稱
                obj.FSPROMOCATD_NAME = compareCode_CodeCatD(sqlResult[i]["FSPROMOCATID"], sqlResult[i]["FSPROMOCATDID"]);    //宣傳帶類型代碼細項代碼名稱
                obj.FSCHANNEL_ID_NAME = compareCode_Code(sqlResult[i]["FSMAIN_CHANNEL_ID"], "TBZCHANNEL"); //頻道別代碼名稱  
                obj.FNDEP_ID_NAME = compareCode_Code(sqlResult[i]["FNDEP_ID"], "TBUSER_DEP");             //成案單位名稱
                if (sqlResult[i]["FDEXPIRE_DATE"] == "")
                {
                    obj.FDEXPIRE_DATE = Convert.ToDateTime("1900-1-1");
                    obj.Origin_FDEXPIRE_DATE = Convert.ToDateTime("1900-1-1");
                }
                else
                {
                    obj.FDEXPIRE_DATE = Convert.ToDateTime(sqlResult[i]["FDEXPIRE_DATE"]);                      //到期日期
                    obj.Origin_FDEXPIRE_DATE = Convert.ToDateTime(sqlResult[i]["FDEXPIRE_DATE"]);
                }
                obj.FSEXPIRE_DATE_ACTION = sqlResult[i]["FSEXPIRE_DATE_ACTION"];                            //到期時執行的動作
                obj.Origin_FSEXPIRE_DATE_ACTION = sqlResult[i]["FSEXPIRE_DATE_ACTION"];
                // --------------------------------
                returnData.Add(obj);
            }
            return returnData;
        }

        //取宣傳帶編號
        [WebMethod]
        public string GetNoRecordPROMO_ID(string strPromoName,string CREATED_BY)
        {
            // SQL Parameters
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSYYYYMM", DateTime.Now.ToString("yyyyMM"));
            source.Add("FSPROMO_NAME", strPromoName);  
            source.Add("FSCREATED_BY", CREATED_BY);

            List<Dictionary<string, string>> sqlResult;

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_I_TBNORECORD_PROMOID", source, out sqlResult))
                return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count > 0)
                return sqlResult[0]["WANT_PROMOID"];
            else
                return "";
        }

        //宣傳帶資料檔的所有代碼檔
        [WebMethod()]
        public List<Class_CODE> GetTBPGM_PROMO_CODE()
        {
            m_CodeData = MAMFunctions.Transfer_Class_CODE("SP_Q_TBPGM_PROMO_CODE");
            return m_CodeData;
        }

        //宣傳帶類型細項代碼檔
        [WebMethod()]
        public List<Class_CODE> GetTBPGM_PROMO_CATD_CODE()
        {
            m_CodeData = MAMFunctions.Transfer_Class_CODED("SP_Q_TBZPROMOCATD_ALL");
            return m_CodeData;
        }

        //宣傳帶資料檔的所有資料
        [WebMethod]
        public List<Class_PROMO> GetTBPGM_PROMO_ALL()
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPGM_PROMO_ALL", sqlParameters, out sqlResult))
                return new List<Class_PROMO>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PGM_PROMO(sqlResult);
        }

        //
        [WebMethod]
        public List<Class_PROMO> GetTBPGM_PROMO_NOT_DELETED()
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPGM_PROMO_NOT_DELETED", sqlParameters, out sqlResult))
                return new List<Class_PROMO>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PGM_PROMO(sqlResult);
        }

        //宣傳帶資料檔的所有資料_透過宣傳帶名稱
        [WebMethod]
        public List<Class_PROMO> GetTBPGM_PROMO_BYPROMONAME(string strPromoName)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPROMO_NAME", strPromoName);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPGM_PROMO_ALL_BYPROMONAME", sqlParameters, out sqlResult))
                return new List<Class_PROMO>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PGM_PROMO(sqlResult);
        }

        //新增宣傳帶資料檔
        [WebMethod]
        public Boolean INSERT_TBPGM_PROMO(Class.Class_PROMO obj)
        {
            //先取得宣傳帶編號
            string strPromo_ID = GetNoRecordPROMO_ID(obj.FSPROMO_NAME, obj.FSCREATED_BY);

            if (strPromo_ID.Trim() == "")
                return false;

            WSPROPOSAL objProposal = new WSPROPOSAL();

            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROMO_ID", strPromo_ID);
            source.Add("FSPROMO_NAME", obj.FSPROMO_NAME);
            source.Add("FSPROMO_NAME_ENG", obj.FSPROMO_NAME_ENG);

            if (obj.FSDURATION == "0")  //轉成TimeCode
                source.Add("FSDURATION", "");
            else
            {
                if (obj.FSDURATION.ToString() == "")
                    source.Add("FSDURATION", "");
                else
                    source.Add("FSDURATION", MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(Convert.ToInt32(obj.FSDURATION)));
            }

            source.Add("FSPROG_ID", obj.FSPROG_ID);

            if (obj.FNEPISODE == 0)
                source.Add("FNEPISODE", "");
            else
                source.Add("FNEPISODE", obj.FNEPISODE.ToString());

            source.Add("FSPROMOTYPEID", obj.FSPROMOTYPEID);
            source.Add("FSPROGOBJID", obj.FSPROGOBJID);
            source.Add("FSPRDCENID", obj.FSPRDCENID);
            source.Add("FSPRDYYMM", obj.FSPRDYYMM);
            source.Add("FSACTIONID", obj.FSACTIONID);
            source.Add("FSPROMOCATID", obj.FSPROMOCATID);
            source.Add("FSPROMOCATDID", obj.FSPROMOCATDID);
            source.Add("FSPROMOVERID", obj.FSPROMOVERID);
            source.Add("FSMAIN_CHANNEL_ID", obj.FSMAIN_CHANNEL_ID);
            source.Add("FSMEMO", obj.FSMEMO);
            source.Add("FSDEL", "N");
            source.Add("FSDELUSER", "");
            source.Add("FNDEP_ID", objProposal.Query_DEPID_BYUSERID(obj.FSCREATED_BY).ToString());
            source.Add("FSWELFARE", obj.FSWELFARE);
            source.Add("FSCREATED_BY", obj.FSCREATED_BY);
            source.Add("FSUPDATED_BY", obj.FSUPDATED_BY);
            source.Add("FSEXPIRE_DATE_ACTION", obj.FSEXPIRE_DATE_ACTION);
            source.Add("FDEXPIRE_DATE", obj.FDEXPIRE_DATE.ToString("yyyy-MM-dd HH:mm:ss"));

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBPGM_PROMO", source, obj.FSUPDATED_BY);
        }

        //查詢宣傳帶資料檔(要去擋重覆的宣傳帶名稱)
        [WebMethod]
        public string QUERY_TBPROMO_CHECK(string strName)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPROMO_NAME", strName);          

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPGM_PROMO_BYCHECK", sqlParameters, out sqlResult))
                return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            if (sqlResult.Count > 0)
                return sqlResult[0]["dbCount"];
            else
                return "";
        }

        //透過短帶名稱查詢短帶基本資料，修改短帶資料用
        [WebMethod()]
        public Boolean QUERY_TBPROMO_BYNAME_CHECK(string strPromoName, string strPromoID)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPROMO_NAME", strPromoName.Trim());
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPGM_PROMO_BYNAME", sqlParameters, out sqlResult))
                return true;    // 發生SQL錯誤時，回傳True，表示有同樣節目名稱的節目主檔，不給修改

            if (sqlResult.Count == 1)
            {
                //如果查到的事自己這筆，表示可以修改，回傳False
                if (sqlResult[0]["FSPROMO_NAME"] == strPromoName.Trim())
                {
                    if (sqlResult[0]["FSPROMO_ID"] == strPromoID.Trim())
                        return false;   //短帶編號相同，查到的是自己這筆，回傳False，表示可以修改
                    else
                        return true;    //短帶編號不同，查到的是已經存在的短帶名稱，回傳True，表示不能修改
                }
                else
                    return false;       //沒有這個短帶名稱，回傳False，表示可以修改
            }
            else if (sqlResult.Count == 0)
                return false;           //沒有這個短帶名稱，回傳False，表示可以修改
            else
                return true;            //照理說短帶節目資料時一定不會超過一筆，若是進到這表是有問題就還是寫回傳True，不給修改               
        }

        //查詢宣傳帶資料檔_透過宣傳帶名稱(新增短帶資料完就要送帶轉擋)
        [WebMethod]
        public Class_PROMO QUERY_TBPROMO_BYNAME_STT(string strName)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPROMO_NAME", strName);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPGM_PROMO_BYNAME", sqlParameters, out sqlResult))
                return new Class_PROMO();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PGM_PROMO(sqlResult)[0];      
        }

        //修改宣傳帶資料檔
        [WebMethod]
        public Boolean UPDATE_TBPGM_PROMO(Class.Class_PROMO obj)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROMO_ID", obj.FSPROMO_ID);
            source.Add("FSPROMO_NAME", obj.FSPROMO_NAME);
            source.Add("FSPROMO_NAME_ENG", obj.FSPROMO_NAME_ENG);

            if (obj.FSDURATION.ToString() == "0")  //轉成TimeCode
                source.Add("FSDURATION", "");
            else
            {
                if (obj.FSDURATION.ToString() == "")
                    source.Add("FSDURATION", "");
                else
                    source.Add("FSDURATION", MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(Convert.ToInt32(obj.FSDURATION)));
            }

            source.Add("FSPROG_ID", obj.FSPROG_ID);

            if (obj.FNEPISODE == 0)
                source.Add("FNEPISODE", "");
            else
                source.Add("FNEPISODE", obj.FNEPISODE.ToString());

            source.Add("FSPROMOTYPEID", obj.FSPROMOTYPEID);
            source.Add("FSPROGOBJID", obj.FSPROGOBJID);
            source.Add("FSPRDCENID", obj.FSPRDCENID);
            source.Add("FSPRDYYMM", obj.FSPRDYYMM);
            source.Add("FSACTIONID", obj.FSACTIONID);
            source.Add("FSPROMOCATID", obj.FSPROMOCATID);
            source.Add("FSPROMOCATDID", obj.FSPROMOCATDID);
            source.Add("FSPROMOVERID", obj.FSPROMOVERID);
            source.Add("FSMAIN_CHANNEL_ID", obj.FSMAIN_CHANNEL_ID);
            source.Add("FSMEMO", obj.FSMEMO);
            source.Add("FSDEL", obj.FSDEL);
            source.Add("FSDELUSER", obj.FSDELUSER);
            source.Add("FSWELFARE", obj.FSWELFARE);
            source.Add("FSUPDATED_BY", obj.FSUPDATED_BY);
            source.Add("FSEXPIRE_DATE_ACTION", obj.FSEXPIRE_DATE_ACTION);
            source.Add("FDEXPIRE_DATE", obj.FDEXPIRE_DATE.ToString("yyyy-MM-dd HH:mm:ss"));

            //若有異動到期日期貨到期日動作寫入TBLOG_FILE_EXPIRE_DELETE資料庫
            if (obj.FDEXPIRE_DATE != obj.Origin_FDEXPIRE_DATE || obj.FSEXPIRE_DATE_ACTION != obj.Origin_FSEXPIRE_DATE_ACTION)
            {
                string myFSTABLE_NAME = "SP_U_TBPGM_PROMO(PROMOID)：" + obj.FSPROMO_ID;
                string myFSRECORD = obj.Origin_FDEXPIRE_DATE.ToString() + " => " + obj.FDEXPIRE_DATE.ToString() + " || " + obj.Origin_FSEXPIRE_DATE_ACTION + " => " + obj.FSEXPIRE_DATE_ACTION;
                INSERT_TBLOG_FILE_EXPIRE_DELETE(myFSTABLE_NAME, myFSRECORD, obj.FSUPDATED_BY);
            }

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBPGM_PROMO", source, obj.FSUPDATED_BY);
        }

        public bool INSERT_TBLOG_FILE_EXPIRE_DELETE(string FSTABLE_NAME, string FSRECORD, string FSUPDATED_BY)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSTABLE_NAME", FSTABLE_NAME);
            source.Add("FSRECORD", FSRECORD);
            source.Add("FSUPDATED_BY", FSUPDATED_BY);
            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBLOG_FILE_EXPIRE_DELETE", source, FSUPDATED_BY);
        }

        //修改宣傳帶資料檔_宣傳帶資料認定_修改頻道別及成案單位
        [WebMethod]
        public Boolean UPDATE_TBPGM_PROMO_IDENTIFIED(Class.Class_PROMO obj, string strOldChannelID)
        {
            Boolean bolDB = false;          //寫入節目管理系統資料庫結果
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROMO_ID", obj.FSPROMO_ID);           
            source.Add("FSMAIN_CHANNEL_ID", obj.FSMAIN_CHANNEL_ID);
            source.Add("FNDEP_ID", obj.FNDEP_ID.ToString());           

            bolDB = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBPGM_PROMO_IDENTIFIED", source, obj.FSUPDATED_BY);

             if (bolDB == true)
             {
                 //頻道別不同才要異動Directory，而且如果是影帶回溯就不會有原本的頻道別，所以特別要把無原本頻道別擋掉，因為沒有原本頻道別也不用換Directories
                 if (obj.FSMAIN_CHANNEL_ID.Trim() != strOldChannelID.Trim() && strOldChannelID.Trim() != "")
                     UPDATE_TBDIRECTORIES_PROMO_IDENTIFIED(obj, strOldChannelID);
                     
                 return true;
             }                 
             else            
                 return false;                        
        }

        //修改短帶所屬節點_短帶資料認定_修改頻道別
        [WebMethod]
        public void UPDATE_TBDIRECTORIES_PROMO_IDENTIFIED(Class.Class_PROMO obj, string strOldChannelID)
        {
            MAM_PTS_DLL.DirAndSubjectAccess Subobj = new MAM_PTS_DLL.DirAndSubjectAccess();
            //Subobj.getProgDirId()     




        }

        //查詢宣傳帶資料檔
        [WebMethod]
        public List<Class_PROMO> QUERY_TBPGM_PROMO(Class.Class_PROMO obj)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();

            if(obj.FSPROMO_ID==null)
                source.Add("FSPROMO_ID", "");
            else
                source.Add("FSPROMO_ID", obj.FSPROMO_ID);

            if (obj.FSPROMO_NAME == null)
                source.Add("FSPROMO_NAME", "");
            else
                source.Add("FSPROMO_NAME", obj.FSPROMO_NAME);

            if (obj.FSPROMO_NAME_ENG == null)
                source.Add("FSPROMO_NAME_ENG", "");
            else
                source.Add("FSPROMO_NAME_ENG", obj.FSPROMO_NAME_ENG);

            if (obj.FSDURATION == null)
                source.Add("FSDURATION", "");
            else if (obj.FSDURATION.ToString().Trim() == "0")  //轉成TimeCode
                source.Add("FSDURATION", "");
            else if (obj.FSDURATION.ToString().Trim() == "")
                 source.Add("FSDURATION", "");
            else
                source.Add("FSDURATION", MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(Convert.ToInt32(obj.FSDURATION.ToString().Trim())));

            if (obj.FSPROG_ID == null)
                source.Add("FSPROG_ID", "");
            else
                source.Add("FSPROG_ID", obj.FSPROG_ID);

            if (obj.FNEPISODE == 0)
                source.Add("FNEPISODE", ""); 
            else
                source.Add("FNEPISODE", obj.FNEPISODE.ToString());

            if (obj.FSPROMOTYPEID == null)
                source.Add("FSPROMOTYPEID", "");
            else
                source.Add("FSPROMOTYPEID", obj.FSPROMOTYPEID);

            if (obj.FSPROGOBJID == null)
                source.Add("FSPROGOBJID", "");
            else
                source.Add("FSPROGOBJID", obj.FSPROGOBJID);

            if (obj.FSPRDCENID == null)
                source.Add("FSPRDCENID", "");
            else
                source.Add("FSPRDCENID", obj.FSPRDCENID);

            if (obj.FSPRDYYMM == null)
                source.Add("FSPRDYYMM", "");
            else
                source.Add("FSPRDYYMM", obj.FSPRDYYMM);

            if (obj.FSACTIONID == null)
                source.Add("FSACTIONID", "");
            else
                source.Add("FSACTIONID", obj.FSACTIONID);

            if (obj.FSPROMOCATID == null)
                source.Add("FSPROMOCATID", "");
            else
                source.Add("FSPROMOCATID", obj.FSPROMOCATID);

            if (obj.FSPROMOCATDID == null)
                source.Add("FSPROMOCATDID", "");
            else
                source.Add("FSPROMOCATDID", obj.FSPROMOCATDID);

            if (obj.FSPROMOVERID == null)
                source.Add("FSPROMOVERID", "");
            else
                source.Add("FSPROMOVERID", obj.FSPROMOVERID);

            if (obj.FSMAIN_CHANNEL_ID == null)
                source.Add("FSMAIN_CHANNEL_ID", "");
            else
                source.Add("FSMAIN_CHANNEL_ID", obj.FSMAIN_CHANNEL_ID);

            if (obj.FSMEMO == null)
                source.Add("FSMEMO", "");
            else
                source.Add("FSMEMO", obj.FSMEMO);

            if (obj.FSWELFARE == null)
                source.Add("FSWELFARE", "");
            else
                source.Add("FSWELFARE", obj.FSWELFARE);

            if (obj.FSCREATED_BY == null)
                source.Add("FSCREATED_BY", "");
            else
                source.Add("FSCREATED_BY", obj.FSCREATED_BY);

            if (obj.FSDEL == null)
            {
                source.Add("FSDEL", "");
            }
            else 
            {
                source.Add("FSDEL", obj.FSDEL);
            }


            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPGM_PROMO_BYCONDITIONS", source, out sqlResult))
                return new List<Class_PROMO>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PGM_PROMO(sqlResult);
        }

        //刪除宣傳帶資料時，要檢查該宣傳帶是否有其他資料
        [WebMethod]
        public List<Class_DELETE_MSG> DELETE_TBPROMO_Check(string strPromoID, string strDelUser)
        {        
            DELETE_TBPROGD_Check_BROCAST(strPromoID);            //刪除節目子集資料時，檢查送帶轉檔
            DELETE_TBPROGD_Check_ARCHIVE(strPromoID);            //刪除節目子集資料時，檢查入庫檔
            DELETE_TBPROGD_Check_QUEUE(strPromoID);              //刪除節目子集資料時，檢查排表
            DELETE_TBPROGD_Check_Promo_Booking(strPromoID);      //刪除短帶資料時，檢查短帶託播單 by Jarvis 20141125

            return m_DeleteMsg;
        }

        public void DELETE_TBPROGD_Check_Promo_Booking(string strPromoID) 
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROMO_ID", strPromoID);

            List<Dictionary<string, string>> sqlResult;
            MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPGM_PROMO_BOOKING_Check", source, out sqlResult);

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_DELETE_MSG obj = new Class_DELETE_MSG();
                obj.FSSystem = "短帶託播單資料";

                if (sqlResult[i]["FSCHANNEL_NAME"] != null)
                {
                    obj.FSCheckMsg = obj.FSCheckMsg + "頻道- " + sqlResult[i]["FSCHANNEL_NAME"].ToString().Trim()+
                        "; 有效日期：" + sqlResult[i]["FDBEG_DATE"].ToString().Trim().Substring(0,10)+
                        " ~ " + sqlResult[i]["FDEND_DATE"].ToString().Trim().Substring(0,10);
                }
                m_DeleteMsg.Add(obj);
            }
        
        }

        //刪除節目資料時，檢查送帶轉檔
        public void DELETE_TBPROGD_Check_BROCAST(string strPromoID)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSBRO_TYPE", "P");
            source.Add("FSID", strPromoID);
            source.Add("FNEPISODE", "0");

            List<Dictionary<string, string>> sqlResult;
            MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBBROADCAST_BYIDEPISODE", source, out sqlResult);

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_DELETE_MSG obj = new Class_DELETE_MSG();
                obj.FSSystem = "送帶轉檔資料";

                if (sqlResult[i]["WANT_FDBRO_DATE"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + "申請日期 - " + sqlResult[i]["WANT_FDBRO_DATE"].ToString().Trim();

                m_DeleteMsg.Add(obj);
            }
        }

        //刪除節目資料時，檢查入庫檔
        public void DELETE_TBPROGD_Check_ARCHIVE(string strPromoID)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSTYPE", "P");
            source.Add("FSID", strPromoID);
            source.Add("FNEPISODE", "0");

            List<Dictionary<string, string>> sqlResult;
            MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBARCHIVE_BYID_EPISODE", source, out sqlResult);

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_DELETE_MSG obj = new Class_DELETE_MSG();
                obj.FSSystem = "入庫資料"; 

                if (sqlResult[i]["WANT_FDARC_DATE"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + "申請日期 - " + sqlResult[i]["WANT_FDARC_DATE"].ToString().Trim();

                m_DeleteMsg.Add(obj);
            }
        }

        //刪除節目資料時，檢查排表
        public void DELETE_TBPROGD_Check_QUEUE(string strPromoID)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROMO_ID", strPromoID);

            List<Dictionary<string, string>> sqlResult;
            MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPGM_ARR_PROMO_BY_PROMOID", source, out sqlResult);

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_DELETE_MSG obj = new Class_DELETE_MSG();
                obj.FSSystem = "排表資料";

                if (sqlResult[i]["Want_Date"].ToString().Trim() != "")
                    obj.FSCheckMsg = "排播日期 - " + sqlResult[i]["Want_Date"].ToString().Trim();

                if (sqlResult[i]["FSPLAYTIME"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 排播時間 - " + sqlResult[i]["FSPLAYTIME"].ToString().Trim();

                if (sqlResult[i]["FSCHANNEL_NAME"].ToString().Trim() != "")
                    obj.FSCheckMsg = obj.FSCheckMsg + " ， 排播頻道 - " + sqlResult[i]["FSCHANNEL_NAME"].ToString().Trim();

                m_DeleteMsg.Add(obj);
            }
        }

        //刪除宣傳帶資料檔
        [WebMethod]
        public ReturnMsg DELETE_TBPGM_PROMO(string strPROMO_ID, string strUPDATED_BY)
        {
            //先作一連串的判斷，確定該PROMO可以刪掉後才刪除(將刪除註記變成Y)
            ReturnMsg objMsg = new ReturnMsg();
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSPROMO_ID", strPROMO_ID);
            source.Add("FSDEL", "Y");
            source.Add("FSDELUSER", strUPDATED_BY);

            objMsg.bolResult= MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_TBPGM_PROMO", source, strUPDATED_BY);
            objMsg.strErrorMsg = "";

            return objMsg;
        }

        //比對代碼檔
        string compareCode_Code(string strID, string strTable)
        {
            for (int i = 0; i < m_CodeData.Count; i++)
            {
                if (m_CodeData[i].ID.ToString().Trim().Equals(strID) && m_CodeData[i].TABLENAME.ToString().Trim().Equals(strTable))
                {
                    return m_CodeData[i].NAME.ToString().Trim();
                }
            }
            return "";
        }

        //比對代碼檔_宣傳帶類型細項
        string compareCode_CodeCatD(string strID, string strDID)
        {
            for (int i = 0; i < m_CodeCATD.Count; i++)
            {
                if (m_CodeCATD[i].ID.ToString().Trim().Equals(strID) && m_CodeCATD[i].IDD.ToString().Trim().Equals(strDID))
                {
                    return m_CodeCATD[i].NAME.ToString().Trim();
                }
            }
            return "";
        }
                
        //活動的所有代碼檔_透過名稱查詢
        [WebMethod()]
        public List<Class_CODE> GetTBPGM_ACTION_CODE_BYNAME(string strName)
        {
            m_CodeData = MAMFunctions.Transfer_Class_CODE("SP_Q_TBPGM_ACTION_BYNAME",strName);
            return m_CodeData;
        }

        //查詢宣傳帶主檔透過宣傳帶編號        
        [WebMethod()]
        public List<Class_PROMO> QUERY_TBPGM_PROMO_BYID(string strPromoID)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPROMO_ID", strPromoID);

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_SP_Q_TBPGM_PROMO_BYPROMOID", sqlParameters, out sqlResult))
                return new List<Class_PROMO>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            return Transfer_PGM_PROMO(sqlResult);
        }

    }
}
