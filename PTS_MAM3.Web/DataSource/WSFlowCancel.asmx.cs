﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSFlowCancel 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSFlowCancel : System.Web.Services.WebService
    {
        private FlowWS.Flow myFlowWS = new FlowWS.Flow();
        /// <summary>
        /// 入庫單刪除表單動作
        /// </summary>
        /// <param name="FlowId"></param>
        /// <param name="FormId"></param>
        /// <param name="UserId"></param>
        /// <returns>True成功 False失敗</returns>
        [WebMethod]
        public bool STO100Cancel(string FlowId, string FormId, string UserId)
        {
            bool returnbool = false;
            WSARCHIVE myWSARCHIVE = new WSARCHIVE();
            returnbool = myWSARCHIVE.UpdTBARCHIVE_Finished_ForFLOW(FormId, "X", UserId);
            if (returnbool == true)
            { returnbool = myFlowWS.CancelFlow(Convert.ToInt32(FlowId)); }
            return returnbool;
        }

        /// <summary>
        /// 入庫置換單刪除
        /// </summary>
        /// <param name="FlowId"></param>
        /// <param name="FormId"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [WebMethod]
        public bool STO101Cancel(string FlowId, string FormId, string UserId)
        {
            bool returnbool = false;
            WSARCHIVE myWSARCHIVE = new WSARCHIVE();
            returnbool = myWSARCHIVE.UpdEXCHNAGE_Finished_ForFLOW(FormId, "X", UserId);
            #region 在此加入每個流程修改資料表的服務
            if (returnbool == true)
            { returnbool = myFlowWS.CancelFlow(Convert.ToInt32(FlowId)); }
            #endregion
            return returnbool;
        }

        /// <summary>
        /// 託播單審核
        /// </summary>
        /// <param name="FlowId"></param>
        /// <param name="FormId"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [WebMethod]
        public bool PGM110Cancel(string FlowId, string FormId, string UserId)
        {
            bool returnbool = false;
            DataSource.SendSQL myWSARCHIVE = new SendSQL();
            returnbool = myWSARCHIVE.Do_Update_Promo_Booling(FormId, "X");
            #region 在此加入每個流程修改資料表的服務
            if (returnbool == true)
            { returnbool = myFlowWS.CancelFlow(Convert.ToInt32(FlowId)); }
            #endregion
            return returnbool;
        }
        /// <summary>
        /// 成案刪除單據
        /// </summary>
        /// <param name="FlowId"></param>
        /// <param name="FormId"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [WebMethod]
        public bool PRO100Cancel(string FlowId, string FormId, string UserId)
        {
            WSPROPOSAL myWSARCHIVE = new WSPROPOSAL();
            bool returnbool = false;
            
            //先更改資料表中的參數
            returnbool = myWSARCHIVE.fnUPDATETBPROPOSAL_Staus(FormId, "X", UserId);
            
            //確認更改資料表中的參數後 刪除流程單
            if (returnbool == true)
            { returnbool = myFlowWS.CancelFlow(Convert.ToInt32(FlowId)); }

            return returnbool;
        }

        /// <summary>
        /// 送帶轉檔表單
        /// </summary>
        /// <param name="FlowId"></param>
        /// <param name="FormId"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [WebMethod]
        public bool BRO100Cancel(string FlowId, string FormId, string UserId)
        {
            bool returnbool = false;
           
            WSBROADCAST myWSARCHIVE = new WSBROADCAST();
            returnbool = myWSARCHIVE.UPDATE_TBBROADCAST_Status(FormId, "X", UserId);
            //先刪除資料表再刪除流程單
            if (returnbool == true)
            { returnbool = myFlowWS.CancelFlow(Convert.ToInt32(FlowId)); }

            return returnbool;
        }

        /// <summary>
        /// 抽單送帶轉檔表單
        /// </summary>
        /// <param name="FlowId"></param>
        /// <param name="FormId"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [WebMethod]
        public bool BRO100Cancel_2(string FlowId, string FormId, string UserId)
        {
            bool returnbool = false;

            WSBROADCAST myWSARCHIVE = new WSBROADCAST();
            WSDELETE deleteObj = new WSDELETE();
            returnbool = myWSARCHIVE.UPDATE_TBBROADCAST_Staus_Directly(FormId, "X", UserId);
            List<Class.Class_DELETED> deleteList = new List<Class.Class_DELETED>();
            
            //deleteObj.DeleteFile(deleteList);
            //先刪除資料表再刪除流程單
            if (returnbool == true)
            { returnbool = myFlowWS.CancelFlow(Convert.ToInt32(FlowId)); }

            return returnbool;
        }

        /// <summary>
        /// 送帶轉檔置換
        /// </summary>
        /// <param name="FlowId"></param>
        /// <param name="FormId"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [WebMethod]
        public bool BRO200Cancel(string FlowId, string FormId, string UserId)
        {
            bool returnbool = false;
            WSBROADCAST myWSARCHIVE = new WSBROADCAST();
            returnbool = myWSARCHIVE.UPDATE_TBBROADCAST_Status(FormId, "X", UserId);
            #region 在此加入每個流程修改資料表的服務
            if (returnbool == true)
            { returnbool = myFlowWS.CancelFlow(Convert.ToInt32(FlowId)); }
            #endregion
            return returnbool;
        }

        /// <summary>
        /// 調用單刪除
        /// </summary>
        /// <param name="FlowId"></param>
        /// <param name="FormId"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [WebMethod]
        public bool BOK100Cancel(string FlowId, string FormId, string UserId)
        {
            bool returnbool = false;
            WSBookingFunction cancelWookingFunction = new WSBookingFunction();
            returnbool = cancelWookingFunction.SetBookingUnCheck(FormId, UserId);
            if (returnbool == true)
                returnbool = myFlowWS.CancelFlow(Convert.ToInt32(FlowId));
            else
                return false;
            #region 在此加入每個流程修改資料表的服務
            #endregion
            return returnbool;
        }
    }
}
