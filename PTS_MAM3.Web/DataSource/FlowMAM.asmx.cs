﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// FlowMAM 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class FlowMAM : System.Web.Services.WebService
    {
        FlowWebService.FlowSoapClient FlowClinet = new FlowWebService.FlowSoapClient();//流程引擎 
       
        /// <summary>
        /// 呼叫流程起始
        /// </summary>
        /// <param name="strFlowID"></param>
        /// <param name="strUserID"></param>
        /// <param name="strContent"></param>
        /// <returns>流程案號</returns>
        [WebMethod]
        public string CallFlow_NewFlowWithField(int intProcessID, string strUserID, string strContent)
        {
            string strFlowMsg = "";

            try
            {
                
                if (strUserID.Trim() == "" || strContent.Trim() == "")
                    return "";

                for (int i = 0; i < 3; i++) //呼叫流程失敗要重作，至少三次
                {
                    MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/FlowMAM/CallFlow_NewFlowWithField", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "ST;開始呼叫Flow - 流程編號：" + intProcessID + "，申請者：" + strUserID + "，內容：" + strContent);
                    
                    strFlowMsg = FlowClinet.NewFlowWithField(intProcessID, strUserID, strContent);
                    
                    if (strFlowMsg != "")
                    {
                        long flowNo = 0;
                        if (!long.TryParse(strFlowMsg, out flowNo))
                        {
                            CallFlow_SendMail(intProcessID.ToString(), strContent, strUserID, strContent);    //寄信
                            MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/FlowMAM/CallFlow_NewFlowWithField", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "PE;流程編號：" + intProcessID + "，申請者：" + strUserID + "，內容：" + strContent + "，案號：" + strFlowMsg);
                        }
                        else
                        {
                            if (flowNo > 0)
                            {
                                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/FlowMAM/CallFlow_NewFlowWithField", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "NT;流程編號：" + intProcessID + "，申請者：" + strUserID + "，內容：" + strContent + "，案號：" + strFlowMsg);
                                return strFlowMsg;
                            }
                            else
                            {
                                CallFlow_SendMail(intProcessID.ToString(), strContent, strUserID, strContent);    //寄信
                                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/FlowMAM/CallFlow_NewFlowWithField", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "ZE;流程編號：" + intProcessID + "，申請者：" + strUserID + "，內容：" + strContent + "，案號：" + strFlowMsg);
                            }
                        }
                    }
                    else
                    {
                        CallFlow_SendMail(intProcessID.ToString(), strContent, strUserID, strContent);    //寄信
                        MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/FlowMAM/CallFlow_NewFlowWithField", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "ME;流程編號：" + intProcessID + "，申請者：" + strUserID + "，內容：" + strContent);
                    }
                }

                return "";
            }
            catch
            {
                CallFlow_SendMail(intProcessID.ToString(), strContent, strUserID, strContent);    //寄信
                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/FlowMAM/CallFlow_NewFlowWithField", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "E;流程編號：" + intProcessID + "，申請者：" + strUserID + "，內容：" + strContent + "，案號：" + strFlowMsg);
                return "";
            }
        }

        /// <summary>
        /// 呼叫流程審核
        /// </summary>
        /// <param name="strProcessID"></param>
        /// <param name="strContent"></param>
        /// <returns></returns>
        [WebMethod]
        public string CallFlow_WorkWithField(string strProcessID,string strContent)
        {
            string strFlowMsg = "";
            
            try
            {
                

                if (strProcessID.Trim() == "" || strContent.Trim() == "")
                    return "";

                for (int i = 0; i < 3; i++) //呼叫流程失敗要重作，至少三次
                {
                    MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/FlowMAM/CallFlow_WorkWithField", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "ST;流程編號：" + strProcessID + "，內容：" + strContent + "，案號：" + strFlowMsg);

                    strFlowMsg = FlowClinet.WorkWithField(strProcessID, strContent);

                    //0:表示處理失敗 
                    //>0:表示處理成功 
                    if (strFlowMsg != "")
                    {
                        long flowNo = 0;
                        if (!long.TryParse(strFlowMsg, out flowNo))
                        {
                            CallFlow_SendMail(strProcessID, strContent, "", "");    //寄信
                            MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/FlowMAM/CallFlow_WorkWithField", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "PE;流程編號：" + strProcessID + "，內容：" + strContent + "，案號：" + strFlowMsg);
                        }
                        else
                        {
                            if (flowNo > 0)
                            {
                                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/FlowMAM/CallFlow_WorkWithField", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "NT;流程編號：" + strProcessID + "，內容：" + strContent + "，案號：" + strFlowMsg);
                                return strFlowMsg;
                            }
                            else
                            {
                                CallFlow_SendMail(strProcessID, strContent, "", "");    //寄信
                                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/FlowMAM/CallFlow_WorkWithField", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "ZE;流程編號：" + strProcessID + "，內容：" + strContent + "，案號：" + strFlowMsg);
                            }
                        }
                    }
                    else
                    {
                        CallFlow_SendMail(strProcessID, strContent, "", "");    //寄信
                        MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/FlowMAM/CallFlow_WorkWithField", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "ME;流程編號：" + strProcessID + "，內容：" + strContent + "，案號：" + strFlowMsg);
                    }
                }
                return "";             
            }
            catch
            {
                CallFlow_SendMail(strProcessID, strContent, "", "");    //寄信
                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/FlowMAM/CallFlow_WorkWithField", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "E;流程編號：" + strProcessID + "，內容：" + strContent + "，案號：" + strFlowMsg);
                return "";
            }
        }
            
        //流程失敗要寄信
        public Boolean CallFlow_SendMail(string strProcessID, string strContent, string strUserID,string strSW)
        {
            try
            {
                //讀取通知的管理者(暫用)   
                MAM_PTS_DLL.SysConfig objDll = new MAM_PTS_DLL.SysConfig();
                string strErrorMail = objDll.sysConfig_Read("/ServerConfig/SynchronizeError").Trim();
                string strTitle = "";
                string msg = "內容：" + strContent;

                if (strUserID.Trim() != "")
                    msg = msg + "，申請者ID：" + strUserID;

                if (strSW == "Start")
                    strTitle = "流程起始失敗通知-流程編號:" + strProcessID;
                else
                    strTitle = "流程審核失敗通知-流程編號:" + strProcessID;

                MAM_PTS_DLL.Protocol.SendEmail(strErrorMail, strTitle, msg.ToString());
                return true;
            }
            catch
            {                
                return false;
            }
        }

        //查詢流程通知的內容
        [WebMethod]
        public string CallFlow_QUERYMAIL_CONTENT(string strFlowID,string strItemID)
        {
            try
            {
                string strContent = "";

                Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
                sqlParameters.Add("FSFLOW_ID", strFlowID);
                sqlParameters.Add("FSITEM_ID", strItemID);

                List<Dictionary<string, string>> sqlResult;
                if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBFLOW_MAIL_CONTENT", sqlParameters, out sqlResult))
                    return "";   

                if (sqlResult.Count > 0 && sqlResult[0]["FSCONTENT"].Trim() != "")
                {
                    strContent = sqlResult[0]["FSCONTENT"];
                }

                return strContent;
            }
            catch
            {
                return "";
            }
        }

    }
}
