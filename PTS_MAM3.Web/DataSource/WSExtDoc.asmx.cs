﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSExtDoc 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSExtDoc : System.Web.Services.WebService
    {

        [WebMethod]
        public string ExtDocInfo(string FileSource)
        {
            string returnString = "";
            try
            {
                if (EmadOmar.OfficeToText.IsParseable(FileSource))
                    returnString = EmadOmar.OfficeToText.Parse(FileSource);
            }
            catch (Exception ex)
            {
                return returnString;
            }
            return returnString;
        }
    }
}
