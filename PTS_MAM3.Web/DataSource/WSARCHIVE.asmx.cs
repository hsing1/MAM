﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using PTS_MAM3.Web.Class;
using System.Text;
using System.IO;
//using ExifLibrary;
using System.Xml;
using System.Xml.Linq;
using LevDan.Exif;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSARCHIVE 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSARCHIVE : System.Web.Services.WebService
    {
        #region 入庫置換所使用的方法 TBARCHIVE_EXCHANGE_FILE
        [WebMethod]
        public List<Class_ARCHIVE> GetTBARCHIVE_EXCHANEG_FILE_ALL(string USERID, string sDate, string eDate, string FSTYPE, string FSID, string FSEPISODE)
        {
            List<Class_ARCHIVE> returnList = new List<Class_ARCHIVE>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSCREATED_BY", USERID);
            sqlParameters.Add("sDate", sDate);
            sqlParameters.Add("eDate", eDate);
            sqlParameters.Add("FSTYPE", FSTYPE);
            sqlParameters.Add("FSID", FSID);
            if (FSEPISODE == "")
                sqlParameters.Add("FNEPISODE", "0");
            else
                sqlParameters.Add("FNEPISODE", FSEPISODE);
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBARCHIVE_EXCHANGE_LIST", sqlParameters, out sqlResult))
                return new List<Class_ARCHIVE>();
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ARCHIVE obj = new Class_ARCHIVE();
                obj.FSARC_ID = sqlResult[i]["FSARC_ID"];
                obj.FSTYPE = sqlResult[i]["FSTYPE"];
                obj.FSID = sqlResult[i]["FSID"];
                obj.FSLESSCLAM = sqlResult[i]["FSLESSCLAM"];
                obj.FSSUPERVISOR = sqlResult[i]["FSSUPERVISOR"];
                obj.FSCHANNEL_ID = sqlResult[i]["FSCHANNEL_ID"];
                obj.FNEPISODE = Convert.ToInt16(sqlResult[i]["FNEPISODE"]);
                //obj.FDARC_DATE = Convert.ToDateTime(sqlResult[i]["FDARC_DATE"]);
                //obj.FSARC_BY = sqlResult[i]["FSARC_BY"];
                obj.FCCHECK_STATUS = sqlResult[i]["FCCHECK_STATUS"];
                obj.FSCREATED_BY = sqlResult[i]["FSCREATED_BY"];
                obj.FDCREATED_DATE = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                obj.FSUPDATED_BY = sqlResult[i]["FSUPDATED_BY"];
                obj.FDUPDATED_DATE = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                //串來的欄位
                obj.FSCREATED_BY_NAME = sqlResult[i]["FSCREATED_BY_NAME"];
                obj.FSUPDATED_BY_NAME = sqlResult[i]["FSUPDATED_BY_NAME"];
                obj.WANT_FDCREATEDDATE = sqlResult[i]["WANT_FDCREATEDDATE"];
                obj.FSPROG_ID_NAME = sqlResult[i]["FSPROG_ID_NAME"];
                obj.FNEPISODE_NAME = sqlResult[i]["FNEPISODE_NAME"];
                returnList.Add(obj);
                //

                //
            }
            return returnList;
        }

        [WebMethod]
        public List<Class_ARCHIVE> GetTBARCHIVE_EXCHANEG_FILE_ALL_NONCONDICTION(string USERID)
        {
            List<Class_ARCHIVE> returnList = new List<Class_ARCHIVE>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBARCHIVE_EXCHANGE_LIST_NONCONDICTION", sqlParameters, out sqlResult))
                return new List<Class_ARCHIVE>();
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ARCHIVE obj = new Class_ARCHIVE();
                obj.FSARC_ID = sqlResult[i]["FSARC_ID"];
                obj.FSTYPE = sqlResult[i]["FSTYPE"];
                obj.FSID = sqlResult[i]["FSID"];
                obj.FSLESSCLAM = sqlResult[i]["FSLESSCLAM"];
                obj.FSSUPERVISOR = sqlResult[i]["FSSUPERVISOR"];
                obj.FSCHANNEL_ID = sqlResult[i]["FSCHANNEL_ID"];
                obj.FNEPISODE = Convert.ToInt16(sqlResult[i]["FNEPISODE"]);
                //obj.FDARC_DATE = Convert.ToDateTime(sqlResult[i]["FDARC_DATE"]);
                //obj.FSARC_BY = sqlResult[i]["FSARC_BY"];
                obj.FCCHECK_STATUS = sqlResult[i]["FCCHECK_STATUS"];
                obj.FSCREATED_BY = sqlResult[i]["FSCREATED_BY"];
                obj.FDCREATED_DATE = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                obj.FSUPDATED_BY = sqlResult[i]["FSUPDATED_BY"];
                obj.FDUPDATED_DATE = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                //串來的欄位
                obj.FSCREATED_BY_NAME = sqlResult[i]["FSCREATED_BY_NAME"];
                obj.FSUPDATED_BY_NAME = sqlResult[i]["FSUPDATED_BY_NAME"];
                obj.WANT_FDCREATEDDATE = sqlResult[i]["WANT_FDCREATEDDATE"];
                obj.FSPROG_ID_NAME = sqlResult[i]["FSPROG_ID_NAME"];
                obj.FNEPISODE_NAME = sqlResult[i]["FNEPISODE_NAME"];
                returnList.Add(obj);
                //

                //
            }
            return returnList;
        }

        [WebMethod]
        public List<Class_ARCHIVE> GetTBARCHIVE_EXCHANGE_BY_ARCHIVEID(string FSARC_ID)
        {
            List<Class_ARCHIVE> retunList = new List<Class_ARCHIVE>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSARC_ID", FSARC_ID);
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBARCHIVE_EXCHNAGE_BY_ARCHIVEID", sqlParameters, out sqlResult))
                return new List<Class.Class_ARCHIVE>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class.Class_ARCHIVE obj = new Class.Class_ARCHIVE();
                // --------------------------------
                obj.FSARC_ID = sqlResult[i]["FSARC_ID"];
                obj.FSTYPE = sqlResult[i]["FSTYPE"];
                obj.FSID = sqlResult[i]["FSID"];
                obj.FNEPISODE = Convert.ToInt16(sqlResult[i]["FNEPISODE"]);
                //obj.FDARC_DATE = Convert.ToDateTime(sqlResult[i]["FDARC_DATE"]);
                obj.FCCHECK_STATUS = sqlResult[i]["FCCHECK_STATUS"];
                obj.FSCREATED_BY = sqlResult[i]["FSCREATED_BY"];
                obj.FDCREATED_DATE = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                obj.FSUPDATED_BY = sqlResult[i]["FSUPDATED_BY"];
                obj.FDUPDATED_DATE = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                obj.FSCHANNEL_ID = sqlResult[i]["FSCHANNEL_ID"];
                obj.FSSUPERVISOR = sqlResult[i]["FSSUPERVISOR"];
                obj.FSLESSCLAM = sqlResult[i]["FSLESSCLAM"];
                //自己串的欄位
                obj.FSCREATED_BY_NAME = sqlResult[i]["FSCREATED_BY_NAME"];
                obj.FSUPDATED_BY_NAME = sqlResult[i]["FSUPDATED_BY_NAME"];
                obj.WANT_FDCREATEDDATE = sqlResult[i]["WANT_FDCREATEDDATE"];
                obj.FSPROG_ID_NAME = sqlResult[i]["FSPROG_ID_NAME"];
                obj.FNEPISODE_NAME = sqlResult[i]["FNEPISODE_NAME"];

                //obj.FSPROG_ID_NAME = fnQueryProgName(sqlResult[i]["FSPROG_ID"]);
                //obj.FNEPISODE_NAME = fnQueryEpisodeName(sqlResult[i]["FSPROG_ID"], sqlResult[i]["FNEPISODE"]);
                // --------------------------------
                retunList.Add(obj);
            }
            return retunList;
        }

        /// <summary>
        /// 取回TBLOG_VIDEO已入庫資料
        /// </summary>
        /// <param name="FSID">節目ID</param>
        /// <param name="FNEPISODE">節目集數</param>
        /// <returns></returns>
        [WebMethod]
        public List<Class.Class_ARCHIVE_TBLOG_VIDEO> GetTBLOG_VIDEO_EXCHANGE_ALL(string FSID, string FNEPISODE)
        {
            List<Class.Class_ARCHIVE_TBLOG_VIDEO> returnData = new List<Class_ARCHIVE_TBLOG_VIDEO>();
            // 無SQL參數，直接 new 一個空的 Dictionary<string, string>() 即可
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSID", FSID);
            sqlParameters.Add("FNEPISODE", FNEPISODE);
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_EXCHANGE_STT", sqlParameters, out sqlResult))
                return new List<Class.Class_ARCHIVE_TBLOG_VIDEO>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class.Class_ARCHIVE_TBLOG_VIDEO obj = new Class.Class_ARCHIVE_TBLOG_VIDEO();
                // --------------------------------
                obj.iherFSFILE_NO = sqlResult[i]["FSFILE_NO"];
                obj.iherFSSUBJECT_ID = sqlResult[i]["FSSUBJECT_ID"];
                obj.strType = sqlResult[i]["FSTYPE"];
                obj.strFSID = sqlResult[i]["FSID"];
                obj.strEpisodeId = sqlResult[i]["FNEPISODE"];
                obj.iherFSPLAY = sqlResult[i]["FSPLAY"];
                obj.strARC_TYPE = sqlResult[i]["FSARC_TYPE"];
                obj.iherFSFILE_TYPE_HV = sqlResult[i]["FSFILE_TYPE_HV"];
                obj.iherFSFILE_TYPE_LV = sqlResult[i]["FSFILE_TYPE_LV"];
                obj.strMetadataFSTITLE = sqlResult[i]["FSTITLE"];
                obj.strMetadataFSFSDESCRIPTION = sqlResult[i]["FSDESCRIPTION"];
                obj.srtFileSize = sqlResult[i]["FSFILE_SIZE"];
                obj.iherFCFILE_STATUS = sqlResult[i]["FCFILE_STATUS"];
                obj.strOFileName = sqlResult[i]["FSOLD_FILE_NAME"];
                obj.iherFSFILE_PATH_H = sqlResult[i]["FSFILE_PATH_H"];
                obj.iherFSFILE_PATH_L = sqlResult[i]["FSFILE_PATH_L"];
                obj.strARC_ID = sqlResult[i]["FSARC_ID"];
                obj.iherforeingFSPGMNAME = sqlResult[i]["FSPGMNAME"];
                obj.iherforeingFSTYPE_NAME = sqlResult[i]["FSTYPE_NAME"];
                obj.iherFSCREATED_BY = sqlResult[i]["FSCREATED_BY"];
                obj.iherFDCREATED_DATE = sqlResult[i]["FDCREATED_DATE"];
                obj.iherFSUPDATED_BY = sqlResult[i]["FSUPDATED_BY"];
                obj.iherFDUPDATED_DATE = sqlResult[i]["FDUPDATED_DATE"];
                returnData.Add(obj);
            }
            return returnData;
        }

        /// <summary>
        /// 取得METADATA以便修改
        /// </summary>
        /// <param name="VAPDClass"></param>
        /// <returns></returns>
        [WebMethod]
        public List<Class_ARCHIVE_VAPD_METADATA> GetTBLOG_VAPD_EXCHANGE_METADATA(string tableName, string FSTYPE, string FSID, string FNEPISODE)
        {
            List<Class_ARCHIVE_VAPD_METADATA> returnList = new List<Class_ARCHIVE_VAPD_METADATA>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSTABLENAME", tableName);
            sqlParameters.Add("FSTYPE", FSTYPE);
            sqlParameters.Add("FSID", FSID);
            sqlParameters.Add("FNEPISODE", FNEPISODE);
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VAPD_EXCHANGE_METADATA", sqlParameters, out sqlResult))
                return new List<Class_ARCHIVE_VAPD_METADATA>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ARCHIVE_VAPD_METADATA returnClass = new Class_ARCHIVE_VAPD_METADATA();
                returnClass.FSTableName = tableName;
                returnClass.FSFile_No = sqlResult[i]["FSFILE_NO"];
                if (tableName == "TBLOG_VIDEO")
                { returnClass.FSFileType = sqlResult[i]["FSFILE_TYPE_HV"]; }
                else
                { returnClass.FSFileType = sqlResult[i]["FSFILE_TYPE"]; }
                returnClass.FSTitle = sqlResult[i]["FSTITLE"];
                returnClass.FSFileSize = sqlResult[i]["FSFILE_SIZE"];
                returnClass.FSOldFileName = sqlResult[i]["FSOLD_FILE_NAME"];
                if (tableName == "TBLOG_VIDEO")
                { returnClass.FSTSMPath = sqlResult[i]["FSFILE_PATH_H"]; }
                else
                { returnClass.FSTSMPath = sqlResult[i]["FSFILE_PATH"]; }
                returnClass.FSCreatedBy = sqlResult[i]["FSCREATED_BY"];
                returnClass.FSCreateDate = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                returnClass.FSUpdatedBy = sqlResult[i]["FSUPDATED_BY"];
                returnClass.FSUpdateDate = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                returnClass.MTTitle = sqlResult[i]["FSTITLE"];
                returnClass.MTDescription = sqlResult[i]["FSDESCRIPTION"];
                returnList.Add(returnClass);
            }
            return returnList;
        }

        [WebMethod]
        public bool InsTBLOG_ARCHIVE_EXCHANGE_ARECARD(Class_ARCHIVE ARCHIVEClass)
        {
            bool IsSuccess;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSARC_ID", ARCHIVEClass.FSARC_ID);
            sqlParameters.Add("FSTYPE", ARCHIVEClass.FSTYPE);
            sqlParameters.Add("FSID", ARCHIVEClass.FSID);
            sqlParameters.Add("FNEPISODE", ARCHIVEClass.FNEPISODE.ToString().Trim());
            sqlParameters.Add("FCCHECK_STATUS", "W");
            sqlParameters.Add("FSLESSCLAM", ARCHIVEClass.FSLESSCLAM.ToString().Trim());
            sqlParameters.Add("FSSUPERVISOR", ARCHIVEClass.FSSUPERVISOR.ToString().Trim());
            sqlParameters.Add("FSCREATED_BY", ARCHIVEClass.FSCREATED_BY);
            sqlParameters.Add("FSCHANNEL_ID", ARCHIVEClass.FSCHANNEL_ID);
            IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBARCHIVE_EXCHANGE", sqlParameters, ARCHIVEClass.FSCREATED_BY);
            return IsSuccess;
        }

        /// <summary>
        /// STO101_01.xaml.cs 中 private void OKButton_Click(object sender, RoutedEventArgs e) 所使用
        /// </summary>
        /// <param name="FSARC_ID">入庫單號</param>
        /// <param name="FSTYPE">ARCFSTYPE:G節目帶 P宣傳帶</param>
        /// <param name="FSID">FSID:節目ID</param>
        /// <param name="FNEPISODE">ARCFNEPISODE:節目集數</param>
        /// <param name="FCCHECK_STATUS">N:審核中 Y:審核完成</param>
        /// <param name="FSCHANNEL_ID">ARCCHANNEL:節目所屬頻道</param>
        /// <param name="FSUPDATE_BY">修改者ID</param>
        /// <returns></returns>
        [WebMethod]
        public bool UpdTBARCHIVE_EXCHANGE(string FSARC_ID, string FSTYPE, string FSID, string FNEPISODE, string FCCHECK_STATUS, string FSCHANNEL_ID, string FSUPDATE_BY, string FSLESSCLAM, string FSSUPERVISOR)
        {
            bool IsSuccess;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSARC_ID", FSARC_ID);
            sqlParameters.Add("FSTYPE", FSTYPE);
            sqlParameters.Add("FSID", FSID);
            sqlParameters.Add("FNEPISODE", FNEPISODE);
            sqlParameters.Add("FCCHECK_STATUS", FCCHECK_STATUS);
            sqlParameters.Add("FSCHANNEL_ID", FSCHANNEL_ID);
            sqlParameters.Add("FSUPDATED_BY", FSUPDATE_BY);
            sqlParameters.Add("FSLESSCLAM", FSLESSCLAM);
            sqlParameters.Add("FSSUPERVISOR", FSSUPERVISOR);
            IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBARCHIVE_EXCHANGE", sqlParameters, FSUPDATE_BY);
            return IsSuccess;
        }

        /// <summary>
        /// 流程專用的WS
        /// </summary>
        /// <param name="ARCHIVEId"></param>
        /// <param name="FCCHECK_STATUS"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [WebMethod]
        public bool UpdEXCHNAGE_Finished_ForFLOW(string ARCHIVEId, string FCCHECK_STATUS, string userId)
        {
            bool IsSuccess;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSARC_ID", ARCHIVEId);
            sqlParameters.Add("FCCHECK_STATUS", FCCHECK_STATUS);
            sqlParameters.Add("FSUPDATED_BY", userId);
            IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBARCHIVE_EXCHANGE_FINISH", sqlParameters, userId);
            return IsSuccess;
        }
        #endregion

        #region
        /// <summary>
        /// 入庫置換通知排表人員用的EMAIL
        /// </summary>
        /// <param name="ARIC_EXCHANGE_Id"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetPlayListOrderAndSendMail(string ARIC_EXCHANGE_Id)
        {
            string returnResult = "";
            List<Class_returnFILE_NO> FileNoList = new List<Class_returnFILE_NO>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSARC_ID", ARIC_EXCHANGE_Id);
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_BY_ARCID_OnlyFSFILE_NO", sqlParameters, out sqlResult))
                FileNoList = new List<Class_returnFILE_NO>();

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_returnFILE_NO FileNo = new Class_returnFILE_NO();
                FileNo.FILE_NOs = sqlResult[i]["FSFILE_NO"];
                FileNoList.Add(FileNo);
            }

            foreach (Class_returnFILE_NO x in FileNoList)
            {
                Dictionary<string, string> sqlParameters1 = new Dictionary<string, string>();
                List<Class_returnMail> returnMailsList = new List<Class_returnMail>();
                sqlParameters.Add("FSFILE_NO", x.FILE_NOs);
                List<Dictionary<string, string>> sqlResult1;
                if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_PLAYLIST_FILEID", sqlParameters1, out sqlResult1))
                    returnMailsList = new List<Class_returnMail>();
                for (int j = 0; j < sqlResult1.Count; j++)
                {
                    Class_returnMail returnMail = new Class_returnMail();
                    returnMail.FDDATE = sqlResult1[j]["FDDATE"];
                    returnMail.FSCHANNEL_ID = sqlResult1[j]["FSCHANNEL_ID"];
                    returnMail.FSCHANNEL_NAME = sqlResult1[j]["FSCHANNEL_NAME"];
                    returnMail.FSCREATED_BY = sqlResult1[j]["FSCREATED_BY"];
                    returnMail.FSEMAIL = sqlResult1[j]["FSEMAIL"];
                    returnMail.FSUSER_ChtName = sqlResult1[j]["FSUSER_ChtName"];
                    returnMailsList.Add(returnMail);
                }
                foreach (Class_returnMail a in returnMailsList)
                {
                    if (a.FSEMAIL.ToString() != "")
                    {
                        StringBuilder msg = new StringBuilder();
                        msg.Append("播出日期：" + a.FDDATE);
                        msg.Append("播出頻道：" + a.FSCHANNEL_NAME);
                        msg.Append("檔案：" + x.FILE_NOs);
                        MAM_PTS_DLL.Protocol.SendEmail(a.FSEMAIL.ToString(), "檔案置換通知", msg.ToString());//MAIL
                    }
                }
                return returnMailsList.Count().ToString();
            }
            return returnResult;
        }
        /// <summary>
        /// 有排過表的 無論有沒有播過的 都要送給經理的判斷
        /// </summary>
        /// <param name="ARIC_EXCHANGE_Id"></param>
        /// <returns>true:有排表過的 or false:無已排表過</returns>
        [WebMethod]
        public bool GetWasBroadcasted(string ARIC_EXCHANGE_Id)
        {
            bool returnResult = false;
            List<Class_returnFILE_NO> FileNoList = new List<Class_returnFILE_NO>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSARC_ID", ARIC_EXCHANGE_Id);
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_BY_ARCID_OnlyFSFILE_NO", sqlParameters, out sqlResult))
                FileNoList = new List<Class_returnFILE_NO>();

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_returnFILE_NO FileNo = new Class_returnFILE_NO();
                FileNo.FILE_NOs = sqlResult[i]["FSFILE_NO"];
                FileNoList.Add(FileNo);
            }

            foreach (Class_returnFILE_NO x in FileNoList)
            {
                Dictionary<string, string> sqlParameters1 = new Dictionary<string, string>();
                List<Class_returnMail> returnMailsList = new List<Class_returnMail>();
                sqlParameters.Add("FSFILE_NO", x.FILE_NOs);
                List<Dictionary<string, string>> sqlResult1;
                if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_HAVEEVER_IN_PLAYLIST_FILEID", sqlParameters1, out sqlResult1))
                    returnMailsList = new List<Class_returnMail>();
                if (sqlResult1.Count > 0)
                { return returnResult = true; }
                else
                { returnResult = false; }
            }
            return returnResult;
        }
        /// <summary>
        /// Flow中使用是否為PRODUCER
        /// </summary>
        /// <param name="USERID"></param>
        /// <param name="Producers"></param>
        /// <returns></returns>
        [WebMethod]
        public bool IsUserProducer(string USERID, string Producers)
        {
            bool returnResult = false;
            string strProducersLis = Producers.Trim();
            if (Producers != "")
            {
                char[] delimiterChars = { ',' };
                string[] words = strProducersLis.Split(delimiterChars);

                if (words.Length > 0)
                {
                    for (int i = 0; i < words.Length; i++)
                    {
                        if (words[i] == USERID.ToString().Trim())
                        {
                            return true;          //若是提出申請者就是製作人
                        }
                    }
                }
            }
            return returnResult;
        }
        #endregion

        public class Class_returnFILE_NO
        {
            public string FILE_NOs { get; set; }
        }
        public class Class_returnMail
        {
            public string FDDATE;
            public string FSCHANNEL_ID;
            public string FSCHANNEL_NAME;
            public string FSCREATED_BY;
            public string FSUSER_ChtName;
            public string FSEMAIL;
        }

        #region 入庫所使用的方法TBARCHIVE

        //取得檔案編號
        [WebMethod]
        public List<ClassUploadFileClass> GetUploadInfo(List<ClassUploadFileClass> myList1)
        {
            List<ClassUploadFileClass> returnList = new List<ClassUploadFileClass>();
            returnList = myList1;
            foreach (ClassUploadFileClass myClass in returnList)
            {
                //myClass.strFileId = WSMAMFunctions.GetNoRecord_FILEID(G or P, 影帶ID, 集數, 使用者);
                /// <param name="strChannelID">頻道別</param>
                /// <param name="strProgName">節目:節目名稱;宣傳帶:宣傳帶名稱</param>
                /// <param name="strEpisodeName">節目:子集名稱 宣傳帶:宣傳帶名稱</param>
                //myClass.strFileId = 
                if (myClass.strEpisodeId_Name == "")
                    myClass.strEpisodeId_Name = myClass.strFSID_NAME;
                string strGet = WSMAMFunctions.GetNoRecord_FILEID(myClass.strType, myClass.strFSID, myClass.strEpisodeId, myClass.strUserId, myClass.strChannelId, myClass.strFSID_NAME, myClass.strEpisodeId_Name); //為配合長結點，必須給三個參數，至軒先塞空白 3/26
                char[] delimiterChars = { ';' };
                string m_strFileNONew, m_DIRID;
                string[] words = strGet.Split(delimiterChars);

                if (words.Length == 2)
                {
                    m_strFileNONew = words[0];
                    m_DIRID = words[1];
                }
                else if (words.Length == 1)
                {
                    m_strFileNONew = words[0];
                    m_DIRID = "";
                }
                else
                {
                    m_strFileNONew = "";
                    m_DIRID = "";
                }
                myClass.strFileId = m_strFileNONew;
                myClass.FS64FNDIR_ID = m_DIRID;
                WS.WSTSM.ServiceTSM obj = new WS.WSTSM.ServiceTSM();
                //上傳影音圖文的內容
                if (myClass.strUploadFileType == "Audio")
                {
                    myClass.strDirectory = obj.GenerateSambaPathByFileIdInfo(myClass.strFileId, WS.WSTSM.ServiceTSM.FileID_MediaType.Audio, myClass.strChannelId, myClass.strOExtensionName);
                }
                else if (myClass.strUploadFileType == "Doc")
                {
                    myClass.strDirectory = obj.GenerateSambaPathByFileIdInfo(myClass.strFileId, WS.WSTSM.ServiceTSM.FileID_MediaType.Doc, myClass.strChannelId, myClass.strOExtensionName);
                }
                else if (myClass.strUploadFileType == "HiVideo")
                {
                    myClass.strDirectory = obj.GenerateSambaPathByFileIdInfo(myClass.strFileId, WS.WSTSM.ServiceTSM.FileID_MediaType.HiVideo, myClass.strChannelId, myClass.strOExtensionName);
                }
                else if (myClass.strUploadFileType == "LowVideo")
                {
                    myClass.strDirectory = obj.GenerateSambaPathByFileIdInfo(myClass.strFileId, WS.WSTSM.ServiceTSM.FileID_MediaType.LowVideo, myClass.strChannelId, myClass.strOExtensionName);
                }
                else if (myClass.strUploadFileType == "Photo")
                {
                    myClass.strDirectory = obj.GenerateSambaPathByFileIdInfo(myClass.strFileId, WS.WSTSM.ServiceTSM.FileID_MediaType.Photo, myClass.strChannelId, myClass.strOExtensionName);
                    //維智TSM TEST
                }
                else
                    myClass.strDirectory = myClass.strUploadFileType;


                if (myClass.strDirectory == "")
                {
                    MAM_PTS_DLL.Log.AppendTrackingLog("/DataSource/WSARCHIVE/GetUploadInfo", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "myClass.strUploadFileType=" + myClass.strUploadFileType + "myClass.strDirectory=" + myClass.strDirectory);
                }
                //myClass.strDirectory=s`
            }
            //string strFileID=  WSMAMFunctions.GetNoRecord_FILEID("G", "2011001", "1", "00001");
            return returnList;
        }

        /// <summary>
        /// MAG102所使用的
        /// </summary>
        /// <returns>LIST</returns>
        [WebMethod]
        public List<Class_ARCHIVE_SET> GetTBARCHIVE_SET()
        {
            List<Class_ARCHIVE_SET> returnList = new List<Class_ARCHIVE_SET>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBARCHIVE_SET", sqlParameters, out sqlResult))
                return new List<Class_ARCHIVE_SET>();

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ARCHIVE_SET obj = new Class_ARCHIVE_SET();
                obj.SET_ID = Convert.ToInt32(sqlResult[i]["SET_ID"]);
                obj.FDCREATED_DATE = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                obj.FDUPDATED_DATE = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                obj.FNAUDIO = sqlResult[i]["FNAUDIO"];
                obj.FNDOC = sqlResult[i]["FNDOC"];
                obj.FNEPISODE = Convert.ToInt16(sqlResult[i]["FNEPISODE"]);
                obj.FNPHOTO = sqlResult[i]["FNPHOTO"];
                obj.FNVIDEO = sqlResult[i]["FNVIDEO"];
                obj.FSCREATED_BY = sqlResult[i]["FSCREATED_BY"];
                obj.FSID = sqlResult[i]["FSID"];
                obj.FSTYPE = sqlResult[i]["FSTYPE"];
                obj.FSUPDATED_BY = sqlResult[i]["FSUPDATED_BY"];
                obj.FSPGMNAME = sqlResult[i]["FSPGMNAME"];
                obj.FSPGDNAME = sqlResult[i]["FSPGDNAME"];
                returnList.Add(obj);
            }
            return returnList;
        }

        //[SP_Q_TBARCHIVE_SET_SEARCH]
        /// <summary>
        /// MAG102 RefreshDataGridWithCondiction 所使用的
        /// </summary>
        /// <param name="FSTYPE"></param>
        /// <param name="FSID"></param>
        /// <param name="FSEPISODE"></param>
        /// <returns></returns>
        [WebMethod]
        public List<Class_ARCHIVE_SET> GetTBARCHIVE_SET_SEARCH(string FSTYPE, string FSID, string FSEPISODE)
        {
            List<Class_ARCHIVE_SET> returnList = new List<Class_ARCHIVE_SET>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSTYPE", FSTYPE.ToString().Trim());
            sqlParameters.Add("FSID", FSID.ToString().Trim());
            sqlParameters.Add("FSEPISODE", FSEPISODE.ToString().Trim());
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBARCHIVE_SET_SEARCH", sqlParameters, out sqlResult))
                return new List<Class_ARCHIVE_SET>();

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ARCHIVE_SET obj = new Class_ARCHIVE_SET();
                obj.SET_ID = Convert.ToInt32(sqlResult[i]["SET_ID"]);
                obj.FDCREATED_DATE = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                obj.FDUPDATED_DATE = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                obj.FNAUDIO = sqlResult[i]["FNAUDIO"];
                obj.FNDOC = sqlResult[i]["FNDOC"];
                obj.FNEPISODE = Convert.ToInt16(sqlResult[i]["FNEPISODE"]);
                obj.FNPHOTO = sqlResult[i]["FNPHOTO"];
                obj.FNVIDEO = sqlResult[i]["FNVIDEO"];
                obj.FSCREATED_BY = sqlResult[i]["FSCREATED_BY"];
                obj.FSID = sqlResult[i]["FSID"];
                obj.FSTYPE = sqlResult[i]["FSTYPE"];
                obj.FSUPDATED_BY = sqlResult[i]["FSUPDATED_BY"];
                obj.FSPGMNAME = sqlResult[i]["FSPGMNAME"];
                obj.FSPGDNAME = sqlResult[i]["FSPGDNAME"];
                returnList.Add(obj);
            }
            return returnList;
        }

        [WebMethod]
        public List<Class_ARCHIVE_SET> GetRBARCHIVE_SET_BY_TIECondiction(string FSTYPE, string FSID, string FSEPISODE)
        {
            List<Class_ARCHIVE_SET> returnList = new List<Class_ARCHIVE_SET>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSTYPE", FSTYPE);
            sqlParameters.Add("FSID", FSID);
            sqlParameters.Add("FNEPISODE", FSEPISODE);
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBARCHIVE_SET_CONDICTION", sqlParameters, out sqlResult))
                return new List<Class_ARCHIVE_SET>();
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ARCHIVE_SET obj = new Class_ARCHIVE_SET();
                obj.SET_ID = Convert.ToInt32(sqlResult[i]["SET_ID"]);
                obj.FDCREATED_DATE = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                obj.FDUPDATED_DATE = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                obj.FNAUDIO = sqlResult[i]["FNAUDIO"];
                obj.FNDOC = sqlResult[i]["FNDOC"];
                obj.FNEPISODE = Convert.ToInt16(sqlResult[i]["FNEPISODE"]);
                obj.FNPHOTO = sqlResult[i]["FNPHOTO"];
                obj.FNVIDEO = sqlResult[i]["FNVIDEO"];
                obj.FSCREATED_BY = sqlResult[i]["FSCREATED_BY"];
                obj.FSID = sqlResult[i]["FSID"];
                obj.FSTYPE = sqlResult[i]["FSTYPE"];
                obj.FSUPDATED_BY = sqlResult[i]["FSUPDATED_BY"];
                //obj.FSPGMNAME = sqlResult[i]["FSPGMNAME"];
                //obj.FSPGDNAME = sqlResult[i]["FSPGDNAME"];
                returnList.Add(obj);
            }
            return returnList;
        }
        [WebMethod]
        public bool DelRBARCHIVE_SET_Record(string FSTYPE, string FSID, string FSEPISODE, string UserId)
        {
            bool IsSuccess = false;
            List<Class_ARCHIVE_SET> returnList = new List<Class_ARCHIVE_SET>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSTYPE", FSTYPE);
            sqlParameters.Add("FSID", FSID);
            sqlParameters.Add("FNEPISODE", FSEPISODE);
            IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_Q_TBARCHIVE_SET_DELETE", sqlParameters, UserId);

            return IsSuccess;
        }
        //[SP_Q_TBARCHIVE_SET_DELETE]
        //入庫檔的所有資料
        [WebMethod]
        public List<Class.Class_ARCHIVE> GetTBARCHIVE_ALL(string USERID, string sDate, string eDate, string FSTYPE, string FSID, string FSEPISODE)
        {
            List<Class.Class_ARCHIVE> returnData = new List<Class.Class_ARCHIVE>();

            // 無SQL參數，直接 new 一個空的 Dictionary<string, string>() 即可
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSCREATED_BY", USERID);
            sqlParameters.Add("sDate", sDate);
            sqlParameters.Add("eDate", eDate);
            sqlParameters.Add("FSTYPE", FSTYPE);
            sqlParameters.Add("FSID", FSID);
            if (FSEPISODE == "")
                sqlParameters.Add("FNEPISODE", "0");
            else
                sqlParameters.Add("FNEPISODE", FSEPISODE);
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBARCHIVE_ALL", sqlParameters, out sqlResult))
                return new List<Class.Class_ARCHIVE>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class.Class_ARCHIVE obj = new Class.Class_ARCHIVE();
                // --------------------------------
                obj.FSARC_ID = sqlResult[i]["FSARC_ID"];
                obj.FSTYPE = sqlResult[i]["FSTYPE"];
                obj.FSID = sqlResult[i]["FSID"];
                obj.FNEPISODE = Convert.ToInt16(sqlResult[i]["FNEPISODE"]);
                obj.FDARC_DATE = Convert.ToDateTime(sqlResult[i]["FDARC_DATE"]);
                obj.FCCHECK_STATUS = sqlResult[i]["FCCHECK_STATUS"];
                obj.FSCREATED_BY = sqlResult[i]["FSCREATED_BY"];
                obj.FDCREATED_DATE = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                obj.FSUPDATED_BY = sqlResult[i]["FSUPDATED_BY"];
                obj.FDUPDATED_DATE = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                obj.FSCHANNEL_ID = sqlResult[i]["FSCHANNEL_ID"];
                obj.FSSUPERVISOR = sqlResult[i]["FSSUPERVISOR"];
                obj.FSLESSCLAM = sqlResult[i]["FSLESSCLAM"];
                //自己串的欄位
                obj.FSCREATED_BY_NAME = sqlResult[i]["FSCREATED_BY_NAME"];
                obj.FSUPDATED_BY_NAME = sqlResult[i]["FSUPDATED_BY_NAME"];
                obj.WANT_FDCREATEDDATE = sqlResult[i]["WANT_FDCREATEDDATE"];
                obj.FSPROG_ID_NAME = sqlResult[i]["FSPROG_ID_NAME"];
                obj.FNEPISODE_NAME = sqlResult[i]["FNEPISODE_NAME"];
                returnData.Add(obj);
            }


            return returnData;

        }

        //入庫檔的所有資料 片庫用的
        [WebMethod]
        public List<Class.Class_ARCHIVE> GetTBARCHIVE_ALL_NONCONDICTION(string USERID)
        {
            List<Class.Class_ARCHIVE> returnData = new List<Class.Class_ARCHIVE>();

            // 無SQL參數，直接 new 一個空的 Dictionary<string, string>() 即可
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBARCHIVE_ALL_NONCONDICTION", sqlParameters, out sqlResult))
                return new List<Class.Class_ARCHIVE>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class.Class_ARCHIVE obj = new Class.Class_ARCHIVE();
                // --------------------------------
                obj.FSARC_ID = sqlResult[i]["FSARC_ID"];
                obj.FSTYPE = sqlResult[i]["FSTYPE"];
                obj.FSID = sqlResult[i]["FSID"];
                obj.FNEPISODE = Convert.ToInt16(sqlResult[i]["FNEPISODE"]);
                obj.FDARC_DATE = Convert.ToDateTime(sqlResult[i]["FDARC_DATE"]);
                obj.FCCHECK_STATUS = sqlResult[i]["FCCHECK_STATUS"];
                obj.FSCREATED_BY = sqlResult[i]["FSCREATED_BY"];
                obj.FDCREATED_DATE = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                obj.FSUPDATED_BY = sqlResult[i]["FSUPDATED_BY"];
                obj.FDUPDATED_DATE = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                obj.FSCHANNEL_ID = sqlResult[i]["FSCHANNEL_ID"];
                obj.FSSUPERVISOR = sqlResult[i]["FSSUPERVISOR"];
                obj.FSLESSCLAM = sqlResult[i]["FSLESSCLAM"];
                //自己串的欄位
                obj.FSCREATED_BY_NAME = sqlResult[i]["FSCREATED_BY_NAME"];
                obj.FSUPDATED_BY_NAME = sqlResult[i]["FSUPDATED_BY_NAME"];
                obj.WANT_FDCREATEDDATE = sqlResult[i]["WANT_FDCREATEDDATE"];
                obj.FSPROG_ID_NAME = sqlResult[i]["FSPROG_ID_NAME"];
                obj.FNEPISODE_NAME = sqlResult[i]["FNEPISODE_NAME"];

                //obj.FSPROG_ID_NAME = fnQueryProgName(sqlResult[i]["FSPROG_ID"]);
                //obj.FNEPISODE_NAME = fnQueryEpisodeName(sqlResult[i]["FSPROG_ID"], sqlResult[i]["FNEPISODE"]);
                // --------------------------------
                returnData.Add(obj);
            }

            return returnData;

        }

        [WebMethod]
        public List<Class_ARCHIVE> GetTBARCHIVE_BY_ARCHIVEID(string FSARC_ID)
        {
            List<Class_ARCHIVE> retunList = new List<Class_ARCHIVE>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSARC_ID", FSARC_ID);
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBARCHIVE_BY_ARCHIVEID", sqlParameters, out sqlResult))
                return new List<Class.Class_ARCHIVE>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class.Class_ARCHIVE obj = new Class.Class_ARCHIVE();
                // --------------------------------
                obj.FSARC_ID = sqlResult[i]["FSARC_ID"];
                obj.FSTYPE = sqlResult[i]["FSTYPE"];
                obj.FSID = sqlResult[i]["FSID"];
                obj.FNEPISODE = Convert.ToInt16(sqlResult[i]["FNEPISODE"]);
                obj.FDARC_DATE = Convert.ToDateTime(sqlResult[i]["FDARC_DATE"]);
                obj.FCCHECK_STATUS = sqlResult[i]["FCCHECK_STATUS"];
                obj.FSCREATED_BY = sqlResult[i]["FSCREATED_BY"];
                obj.FDCREATED_DATE = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                obj.FSUPDATED_BY = sqlResult[i]["FSUPDATED_BY"];
                obj.FDUPDATED_DATE = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                obj.FSCHANNEL_ID = sqlResult[i]["FSCHANNEL_ID"];
                obj.FSSUPERVISOR = sqlResult[i]["FSSUPERVISOR"];
                obj.FSLESSCLAM = sqlResult[i]["FSLESSCLAM"];
                //自己串的欄位
                obj.FSCREATED_BY_NAME = sqlResult[i]["FSCREATED_BY_NAME"];
                obj.FSUPDATED_BY_NAME = sqlResult[i]["FSUPDATED_BY_NAME"];
                obj.WANT_FDCREATEDDATE = sqlResult[i]["WANT_FDCREATEDDATE"];
                obj.FSPROG_ID_NAME = sqlResult[i]["FSPROG_ID_NAME"];
                obj.FNEPISODE_NAME = sqlResult[i]["FNEPISODE_NAME"];

                //obj.FSPROG_ID_NAME = fnQueryProgName(sqlResult[i]["FSPROG_ID"]);
                //obj.FNEPISODE_NAME = fnQueryEpisodeName(sqlResult[i]["FSPROG_ID"], sqlResult[i]["FNEPISODE"]);
                // --------------------------------
                retunList.Add(obj);
            }
            return retunList;
        }

        /// <summary>
        /// 更新VAPD
        /// </summary>
        /// <param name="ARCID">FSARC_ID</param>
        /// <returns>包含四個資料表的LIST</returns>
        [WebMethod]
        public List<Class_ARCHIVE_VAPD> RefreshVAPD(string ARCID, string FSID, string EPISODEID)
        {
            List<Class_ARCHIVE_VAPD> returnList = new List<Class_ARCHIVE_VAPD>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;


            sqlParameters.Clear();
            sqlParameters.Add("FSARC_ID", ARCID);
            sqlParameters.Add("FSID", "");
            sqlParameters.Add("FNEPISODE", EPISODEID);
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_AUDIO_BY_ARCID", sqlParameters, out sqlResult))
                return new List<Class_ARCHIVE_VAPD>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ARCHIVE_VAPD obj = new Class_ARCHIVE_VAPD();
                obj.FSTableName = "TBLOG_AUDIO";
                obj.FSFile_No = sqlResult[i]["FSFILE_NO"];
                obj.FSFileType = sqlResult[i]["FSFILE_TYPE"];
                obj.FSTitle = sqlResult[i]["FSTITLE"];
                obj.FSFileSize = sqlResult[i]["FSFILE_SIZE"];
                obj.FSOldFileName = sqlResult[i]["FSOLD_FILE_NAME"];
                obj.FSTSMPath = sqlResult[i]["FSFILE_PATH"];
                obj.FSCreatedBy = sqlResult[i]["FSCREATED_BY"];
                obj.FSCreateDate = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                obj.FSUpdatedBy = sqlResult[i]["FSUPDATED_BY"];
                obj.FSUpdateDate = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                obj.FSIsOldUploadedFile = "N";
                returnList.Add(obj);
            }
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_PHOTO_BY_ARCID", sqlParameters, out sqlResult))
                return new List<Class_ARCHIVE_VAPD>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ARCHIVE_VAPD obj = new Class_ARCHIVE_VAPD();
                obj.FSTableName = "TBLOG_PHOTO";
                obj.FSFile_No = sqlResult[i]["FSFILE_NO"];
                obj.FSFileType = sqlResult[i]["FSFILE_TYPE"];
                obj.FSTitle = sqlResult[i]["FSTITLE"];
                obj.FSFileSize = sqlResult[i]["FSFILE_SIZE"];
                obj.FSOldFileName = sqlResult[i]["FSOLD_FILE_NAME"];
                obj.FSTSMPath = sqlResult[i]["FSFILE_PATH"];
                obj.FSCreatedBy = sqlResult[i]["FSCREATED_BY"];
                obj.FSCreateDate = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                obj.FSUpdatedBy = sqlResult[i]["FSUPDATED_BY"];
                obj.FSUpdateDate = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                obj.FSIsOldUploadedFile = "N";
                returnList.Add(obj);
            }
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_DOC_BY_ARCID", sqlParameters, out sqlResult))
                return new List<Class_ARCHIVE_VAPD>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ARCHIVE_VAPD obj = new Class_ARCHIVE_VAPD();
                obj.FSTableName = "TBLOG_DOC";
                obj.FSFile_No = sqlResult[i]["FSFILE_NO"];
                obj.FSFileType = sqlResult[i]["FSFILE_TYPE"];
                obj.FSTitle = sqlResult[i]["FSTITLE"];
                obj.FSFileSize = sqlResult[i]["FSFILE_SIZE"];
                obj.FSOldFileName = sqlResult[i]["FSOLD_FILE_NAME"];
                obj.FSTSMPath = sqlResult[i]["FSFILE_PATH"];
                obj.FSCreatedBy = sqlResult[i]["FSCREATED_BY"];
                obj.FSCreateDate = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                obj.FSUpdatedBy = sqlResult[i]["FSUPDATED_BY"];
                obj.FSUpdateDate = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                obj.FSIsOldUploadedFile = "N";
                returnList.Add(obj);
            }
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_BY_ARCID", sqlParameters, out sqlResult))
                return new List<Class_ARCHIVE_VAPD>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ARCHIVE_VAPD obj = new Class_ARCHIVE_VAPD();
                obj.FSTableName = "TBLOG_VIDEO";
                obj.FSFile_No = sqlResult[i]["FSFILE_NO"];
                obj.FSFileType = "送帶轉檔檔案";
                obj.FSTitle = sqlResult[i]["FSTITLE"];
                obj.FSFileSize = sqlResult[i]["FSFILE_SIZE"];
                obj.FSOldFileName = sqlResult[i]["FSFILE_NO"];//sqlResult[i]["FSOLD_FILE_NAME"];
                obj.FSTSMPath = "NonDelete";
                obj.FSCreatedBy = sqlResult[i]["FSCREATED_BY"];
                obj.FSCreateDate = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                obj.FSUpdatedBy = sqlResult[i]["FSUPDATED_BY"];
                obj.FSUpdateDate = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                obj.FSIsOldUploadedFile = "N";
                returnList.Add(obj);
            }

            #region 已上傳的紀錄先回傳到LIST中
            sqlParameters.Clear();
            sqlParameters.Add("FSARC_ID", "");
            sqlParameters.Add("FSID", FSID);
            sqlParameters.Add("FNEPISODE", EPISODEID);
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_AUDIO_BY_ARCID", sqlParameters, out sqlResult))
                return new List<Class_ARCHIVE_VAPD>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ARCHIVE_VAPD obj = new Class_ARCHIVE_VAPD();
                obj.FSTableName = "TBLOG_AUDIO";
                obj.FSFile_No = sqlResult[i]["FSFILE_NO"];
                obj.FSFileType = sqlResult[i]["FSFILE_TYPE"];
                obj.FSTitle = sqlResult[i]["FSTITLE"];
                obj.FSFileSize = sqlResult[i]["FSFILE_SIZE"];
                obj.FSOldFileName = sqlResult[i]["FSOLD_FILE_NAME"];
                obj.FSTSMPath = sqlResult[i]["FSFILE_PATH"];
                obj.FSCreatedBy = sqlResult[i]["FSCREATED_BY"];
                obj.FSCreateDate = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                obj.FSUpdatedBy = sqlResult[i]["FSUPDATED_BY"];
                obj.FSUpdateDate = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                obj.FSIsOldUploadedFile = "Y";
                obj.FSSUPERVISOR = sqlResult[i]["FSSUPERVISOR"];
                returnList.Add(obj);
            }
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_PHOTO_BY_ARCID", sqlParameters, out sqlResult))
                return new List<Class_ARCHIVE_VAPD>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ARCHIVE_VAPD obj = new Class_ARCHIVE_VAPD();
                obj.FSTableName = "TBLOG_PHOTO";
                obj.FSFile_No = sqlResult[i]["FSFILE_NO"];
                obj.FSFileType = sqlResult[i]["FSFILE_TYPE"];
                obj.FSTitle = sqlResult[i]["FSTITLE"];
                obj.FSFileSize = sqlResult[i]["FSFILE_SIZE"];
                obj.FSOldFileName = sqlResult[i]["FSOLD_FILE_NAME"];
                obj.FSTSMPath = sqlResult[i]["FSFILE_PATH"];
                obj.FSCreatedBy = sqlResult[i]["FSCREATED_BY"];
                obj.FSCreateDate = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                obj.FSUpdatedBy = sqlResult[i]["FSUPDATED_BY"];
                obj.FSUpdateDate = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                obj.FSIsOldUploadedFile = "Y";
                obj.FSSUPERVISOR = sqlResult[i]["FSSUPERVISOR"];
                returnList.Add(obj);
            }
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_DOC_BY_ARCID", sqlParameters, out sqlResult))
                return new List<Class_ARCHIVE_VAPD>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ARCHIVE_VAPD obj = new Class_ARCHIVE_VAPD();
                obj.FSTableName = "TBLOG_DOC";
                obj.FSFile_No = sqlResult[i]["FSFILE_NO"];
                obj.FSFileType = sqlResult[i]["FSFILE_TYPE"];
                obj.FSTitle = sqlResult[i]["FSTITLE"];
                obj.FSFileSize = sqlResult[i]["FSFILE_SIZE"];
                obj.FSOldFileName = sqlResult[i]["FSOLD_FILE_NAME"];
                obj.FSTSMPath = sqlResult[i]["FSFILE_PATH"];
                obj.FSCreatedBy = sqlResult[i]["FSCREATED_BY"];
                obj.FSCreateDate = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                obj.FSUpdatedBy = sqlResult[i]["FSUPDATED_BY"];
                obj.FSUpdateDate = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                obj.FSIsOldUploadedFile = "Y";
                obj.FSSUPERVISOR = sqlResult[i]["FSSUPERVISOR"];
                returnList.Add(obj);
            }
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_BY_ARCID", sqlParameters, out sqlResult))
                return new List<Class_ARCHIVE_VAPD>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ARCHIVE_VAPD obj = new Class_ARCHIVE_VAPD();
                obj.FSTableName = "TBLOG_VIDEO";
                obj.FSFile_No = sqlResult[i]["FSFILE_NO"];
                obj.FSFileType = "送帶轉檔檔案";
                obj.FSTitle = sqlResult[i]["FSTITLE"];
                obj.FSFileSize = sqlResult[i]["FSFILE_SIZE"];
                obj.FSOldFileName = sqlResult[i]["FSFILE_NO"];//sqlResult[i]["FSOLD_FILE_NAME"];
                obj.FSTSMPath = "NonDelete";
                obj.FSCreatedBy = sqlResult[i]["FSCREATED_BY"];
                obj.FSCreateDate = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                obj.FSUpdatedBy = sqlResult[i]["FSUPDATED_BY"];
                obj.FSUpdateDate = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                obj.FSIsOldUploadedFile = "Y";
                obj.FSSUPERVISOR = sqlResult[i]["FSSUPERVISOR"];
                returnList.Add(obj);
            }
            #endregion

            return returnList;
        }

        /// <summary>
        /// 更新VAPD
        /// N:本單號新增的檔案
        /// R:重送的代號
        /// Y:已經核可過
        /// </summary>
        /// <param name="ARCID">FSARC_ID</param>
        /// <returns>包含四個資料表的LIST</returns>
        [WebMethod]
        public List<Class_ARCHIVE_VAPD> RefreshVAPD_RESEND(string ARCID, string ARCID_OLD, string FSID, string EPISODEID)
        {
            List<Class_ARCHIVE_VAPD> returnList = new List<Class_ARCHIVE_VAPD>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult;

            //新增加的檔案
            sqlParameters.Clear();
            sqlParameters.Add("FSARC_ID", ARCID);
            sqlParameters.Add("FSID", "");
            sqlParameters.Add("FNEPISODE", EPISODEID);
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_AUDIO_BY_ARCID", sqlParameters, out sqlResult))
                return new List<Class_ARCHIVE_VAPD>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ARCHIVE_VAPD obj = new Class_ARCHIVE_VAPD();
                obj.FSTableName = "TBLOG_AUDIO";
                obj.FSFile_No = sqlResult[i]["FSFILE_NO"];
                obj.FSFileType = sqlResult[i]["FSFILE_TYPE"];
                obj.FSTitle = sqlResult[i]["FSTITLE"];
                obj.FSFileSize = sqlResult[i]["FSFILE_SIZE"];
                obj.FSOldFileName = sqlResult[i]["FSOLD_FILE_NAME"];
                obj.FSTSMPath = sqlResult[i]["FSFILE_PATH"];
                obj.FSCreatedBy = sqlResult[i]["FSCREATED_BY"];
                obj.FSCreateDate = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                obj.FSUpdatedBy = sqlResult[i]["FSUPDATED_BY"];
                obj.FSUpdateDate = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                obj.FSIsOldUploadedFile = "N";
                returnList.Add(obj);
            }
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_PHOTO_BY_ARCID", sqlParameters, out sqlResult))
                return new List<Class_ARCHIVE_VAPD>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ARCHIVE_VAPD obj = new Class_ARCHIVE_VAPD();
                obj.FSTableName = "TBLOG_PHOTO";
                obj.FSFile_No = sqlResult[i]["FSFILE_NO"];
                obj.FSFileType = sqlResult[i]["FSFILE_TYPE"];
                obj.FSTitle = sqlResult[i]["FSTITLE"];
                obj.FSFileSize = sqlResult[i]["FSFILE_SIZE"];
                obj.FSOldFileName = sqlResult[i]["FSOLD_FILE_NAME"];
                obj.FSTSMPath = sqlResult[i]["FSFILE_PATH"];
                obj.FSCreatedBy = sqlResult[i]["FSCREATED_BY"];
                obj.FSCreateDate = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                obj.FSUpdatedBy = sqlResult[i]["FSUPDATED_BY"];
                obj.FSUpdateDate = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                obj.FSIsOldUploadedFile = "N";
                returnList.Add(obj);
            }
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_DOC_BY_ARCID", sqlParameters, out sqlResult))
                return new List<Class_ARCHIVE_VAPD>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ARCHIVE_VAPD obj = new Class_ARCHIVE_VAPD();
                obj.FSTableName = "TBLOG_DOC";
                obj.FSFile_No = sqlResult[i]["FSFILE_NO"];
                obj.FSFileType = sqlResult[i]["FSFILE_TYPE"];
                obj.FSTitle = sqlResult[i]["FSTITLE"];
                obj.FSFileSize = sqlResult[i]["FSFILE_SIZE"];
                obj.FSOldFileName = sqlResult[i]["FSOLD_FILE_NAME"];
                obj.FSTSMPath = sqlResult[i]["FSFILE_PATH"];
                obj.FSCreatedBy = sqlResult[i]["FSCREATED_BY"];
                obj.FSCreateDate = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                obj.FSUpdatedBy = sqlResult[i]["FSUPDATED_BY"];
                obj.FSUpdateDate = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                obj.FSIsOldUploadedFile = "N";
                returnList.Add(obj);
            }
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_BY_ARCID", sqlParameters, out sqlResult))
                return new List<Class_ARCHIVE_VAPD>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ARCHIVE_VAPD obj = new Class_ARCHIVE_VAPD();
                obj.FSTableName = "TBLOG_VIDEO";
                obj.FSFile_No = sqlResult[i]["FSFILE_NO"];
                obj.FSFileType = "送帶轉檔檔案";
                obj.FSTitle = sqlResult[i]["FSTITLE"];
                obj.FSFileSize = sqlResult[i]["FSFILE_SIZE"];
                obj.FSOldFileName = sqlResult[i]["FSOLD_FILE_NAME"];
                obj.FSTSMPath = "NonDelete";
                obj.FSCreatedBy = sqlResult[i]["FSCREATED_BY"];
                obj.FSCreateDate = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                obj.FSUpdatedBy = sqlResult[i]["FSUPDATED_BY"];
                obj.FSUpdateDate = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                obj.FSIsOldUploadedFile = "N";
                returnList.Add(obj);
            }

            #region "不通過"單子傳回已上傳檔案
            sqlParameters.Clear();
            sqlParameters.Add("FSARC_ID", ARCID_OLD);
            sqlParameters.Add("FSID", "");
            sqlParameters.Add("FNEPISODE", EPISODEID);
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_AUDIO_BY_ARCID", sqlParameters, out sqlResult))
                return new List<Class_ARCHIVE_VAPD>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ARCHIVE_VAPD obj = new Class_ARCHIVE_VAPD();
                obj.FSTableName = "TBLOG_AUDIO";
                obj.FSFile_No = sqlResult[i]["FSFILE_NO"];
                obj.FSFileType = sqlResult[i]["FSFILE_TYPE"];
                obj.FSTitle = sqlResult[i]["FSTITLE"];
                obj.FSFileSize = sqlResult[i]["FSFILE_SIZE"];
                obj.FSOldFileName = sqlResult[i]["FSOLD_FILE_NAME"];
                obj.FSTSMPath = sqlResult[i]["FSFILE_PATH"];
                obj.FSCreatedBy = sqlResult[i]["FSCREATED_BY"];
                obj.FSCreateDate = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                obj.FSUpdatedBy = sqlResult[i]["FSUPDATED_BY"];
                obj.FSUpdateDate = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                obj.FSIsOldUploadedFile = "R";
                returnList.Add(obj);
            }
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_PHOTO_BY_ARCID", sqlParameters, out sqlResult))
                return new List<Class_ARCHIVE_VAPD>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ARCHIVE_VAPD obj = new Class_ARCHIVE_VAPD();
                obj.FSTableName = "TBLOG_PHOTO";
                obj.FSFile_No = sqlResult[i]["FSFILE_NO"];
                obj.FSFileType = sqlResult[i]["FSFILE_TYPE"];
                obj.FSTitle = sqlResult[i]["FSTITLE"];
                obj.FSFileSize = sqlResult[i]["FSFILE_SIZE"];
                obj.FSOldFileName = sqlResult[i]["FSOLD_FILE_NAME"];
                obj.FSTSMPath = sqlResult[i]["FSFILE_PATH"];
                obj.FSCreatedBy = sqlResult[i]["FSCREATED_BY"];
                obj.FSCreateDate = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                obj.FSUpdatedBy = sqlResult[i]["FSUPDATED_BY"];
                obj.FSUpdateDate = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                obj.FSIsOldUploadedFile = "R";
                returnList.Add(obj);
            }
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_DOC_BY_ARCID", sqlParameters, out sqlResult))
                return new List<Class_ARCHIVE_VAPD>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ARCHIVE_VAPD obj = new Class_ARCHIVE_VAPD();
                obj.FSTableName = "TBLOG_DOC";
                obj.FSFile_No = sqlResult[i]["FSFILE_NO"];
                obj.FSFileType = sqlResult[i]["FSFILE_TYPE"];
                obj.FSTitle = sqlResult[i]["FSTITLE"];
                obj.FSFileSize = sqlResult[i]["FSFILE_SIZE"];
                obj.FSOldFileName = sqlResult[i]["FSOLD_FILE_NAME"];
                obj.FSTSMPath = sqlResult[i]["FSFILE_PATH"];
                obj.FSCreatedBy = sqlResult[i]["FSCREATED_BY"];
                obj.FSCreateDate = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                obj.FSUpdatedBy = sqlResult[i]["FSUPDATED_BY"];
                obj.FSUpdateDate = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                obj.FSIsOldUploadedFile = "R";
                returnList.Add(obj);
            }
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_BY_ARCID", sqlParameters, out sqlResult))
                return new List<Class_ARCHIVE_VAPD>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ARCHIVE_VAPD obj = new Class_ARCHIVE_VAPD();
                obj.FSTableName = "TBLOG_VIDEO";
                obj.FSFile_No = sqlResult[i]["FSFILE_NO"];
                obj.FSFileType = "送帶轉檔檔案";
                obj.FSTitle = sqlResult[i]["FSTITLE"];
                obj.FSFileSize = sqlResult[i]["FSFILE_SIZE"];
                obj.FSOldFileName = sqlResult[i]["FSOLD_FILE_NAME"];
                obj.FSTSMPath = "NonDelete";
                obj.FSCreatedBy = sqlResult[i]["FSCREATED_BY"];
                obj.FSCreateDate = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                obj.FSUpdatedBy = sqlResult[i]["FSUPDATED_BY"];
                obj.FSUpdateDate = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                obj.FSIsOldUploadedFile = "R";
                returnList.Add(obj);
            }
            #endregion

            #region 已上傳的紀錄先回傳到LIST中
            sqlParameters.Clear();
            sqlParameters.Add("FSARC_ID", "");
            sqlParameters.Add("FSID", FSID);
            sqlParameters.Add("FNEPISODE", EPISODEID);
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_AUDIO_BY_ARCID", sqlParameters, out sqlResult))
                return new List<Class_ARCHIVE_VAPD>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ARCHIVE_VAPD obj = new Class_ARCHIVE_VAPD();
                obj.FSTableName = "TBLOG_AUDIO";
                obj.FSFile_No = sqlResult[i]["FSFILE_NO"];
                obj.FSFileType = sqlResult[i]["FSFILE_TYPE"];
                obj.FSTitle = sqlResult[i]["FSTITLE"];
                obj.FSFileSize = sqlResult[i]["FSFILE_SIZE"];
                obj.FSOldFileName = sqlResult[i]["FSOLD_FILE_NAME"];
                obj.FSTSMPath = sqlResult[i]["FSFILE_PATH"];
                obj.FSCreatedBy = sqlResult[i]["FSCREATED_BY"];
                obj.FSCreateDate = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                obj.FSUpdatedBy = sqlResult[i]["FSUPDATED_BY"];
                obj.FSUpdateDate = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                obj.FSIsOldUploadedFile = "Y";
                returnList.Add(obj);
            }
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_PHOTO_BY_ARCID", sqlParameters, out sqlResult))
                return new List<Class_ARCHIVE_VAPD>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ARCHIVE_VAPD obj = new Class_ARCHIVE_VAPD();
                obj.FSTableName = "TBLOG_PHOTO";
                obj.FSFile_No = sqlResult[i]["FSFILE_NO"];
                obj.FSFileType = sqlResult[i]["FSFILE_TYPE"];
                obj.FSTitle = sqlResult[i]["FSTITLE"];
                obj.FSFileSize = sqlResult[i]["FSFILE_SIZE"];
                obj.FSOldFileName = sqlResult[i]["FSOLD_FILE_NAME"];
                obj.FSTSMPath = sqlResult[i]["FSFILE_PATH"];
                obj.FSCreatedBy = sqlResult[i]["FSCREATED_BY"];
                obj.FSCreateDate = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                obj.FSUpdatedBy = sqlResult[i]["FSUPDATED_BY"];
                obj.FSUpdateDate = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                obj.FSIsOldUploadedFile = "Y";
                returnList.Add(obj);
            }
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_DOC_BY_ARCID", sqlParameters, out sqlResult))
                return new List<Class_ARCHIVE_VAPD>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ARCHIVE_VAPD obj = new Class_ARCHIVE_VAPD();
                obj.FSTableName = "TBLOG_DOC";
                obj.FSFile_No = sqlResult[i]["FSFILE_NO"];
                obj.FSFileType = sqlResult[i]["FSFILE_TYPE"];
                obj.FSTitle = sqlResult[i]["FSTITLE"];
                obj.FSFileSize = sqlResult[i]["FSFILE_SIZE"];
                obj.FSOldFileName = sqlResult[i]["FSOLD_FILE_NAME"];
                obj.FSTSMPath = sqlResult[i]["FSFILE_PATH"];
                obj.FSCreatedBy = sqlResult[i]["FSCREATED_BY"];
                obj.FSCreateDate = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                obj.FSUpdatedBy = sqlResult[i]["FSUPDATED_BY"];
                obj.FSUpdateDate = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                obj.FSIsOldUploadedFile = "Y";
                returnList.Add(obj);
            }
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_BY_ARCID", sqlParameters, out sqlResult))
                return new List<Class_ARCHIVE_VAPD>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class_ARCHIVE_VAPD obj = new Class_ARCHIVE_VAPD();
                obj.FSTableName = "TBLOG_VIDEO";
                obj.FSFile_No = sqlResult[i]["FSFILE_NO"];
                obj.FSFileType = "送帶轉檔檔案";
                obj.FSTitle = sqlResult[i]["FSTITLE"];
                obj.FSFileSize = sqlResult[i]["FSFILE_SIZE"];
                obj.FSOldFileName = sqlResult[i]["FSOLD_FILE_NAME"];
                obj.FSTSMPath = "NonDelete";
                obj.FSCreatedBy = sqlResult[i]["FSCREATED_BY"];
                obj.FSCreateDate = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                obj.FSUpdatedBy = sqlResult[i]["FSUPDATED_BY"];
                obj.FSUpdateDate = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                obj.FSIsOldUploadedFile = "Y";
                returnList.Add(obj);
            }
            #endregion

            return returnList;
        }


        /// <summary>
        /// 取回TBLOG_VIDEO資料
        /// </summary>
        /// <param name="FSID">節目ID</param>
        /// <param name="FNEPISODE">節目集數</param>
        /// <returns></returns>
        [WebMethod]
        public List<Class.Class_ARCHIVE_TBLOG_VIDEO> GetTBLOG_VIDEO_ALL(string FSID, string FNEPISODE)
        {
            List<Class.Class_ARCHIVE_TBLOG_VIDEO> returnData = new List<Class_ARCHIVE_TBLOG_VIDEO>();
            // 無SQL參數，直接 new 一個空的 Dictionary<string, string>() 即可
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSID", FSID);
            sqlParameters.Add("FNEPISODE", FNEPISODE);
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_STT", sqlParameters, out sqlResult))
                return new List<Class.Class_ARCHIVE_TBLOG_VIDEO>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class.Class_ARCHIVE_TBLOG_VIDEO obj = new Class.Class_ARCHIVE_TBLOG_VIDEO();
                // --------------------------------
                obj.iherFSFILE_NO = sqlResult[i]["FSFILE_NO"];
                obj.iherFSSUBJECT_ID = sqlResult[i]["FSSUBJECT_ID"];
                obj.strType = sqlResult[i]["FSTYPE"];
                obj.strFSID = sqlResult[i]["FSID"];
                obj.strEpisodeId = sqlResult[i]["FNEPISODE"];
                obj.iherFSPLAY = sqlResult[i]["FSPLAY"];
                obj.strARC_TYPE = sqlResult[i]["FSARC_TYPE"];
                obj.iherFSFILE_TYPE_HV = sqlResult[i]["FSFILE_TYPE_HV"];
                obj.iherFSFILE_TYPE_LV = sqlResult[i]["FSFILE_TYPE_LV"];
                obj.strMetadataFSTITLE = sqlResult[i]["FSTITLE"];
                obj.strMetadataFSFSDESCRIPTION = sqlResult[i]["FSDESCRIPTION"];
                obj.srtFileSize = sqlResult[i]["FSFILE_SIZE"];
                obj.iherFCFILE_STATUS = sqlResult[i]["FCFILE_STATUS"];
                obj.strOFileName = sqlResult[i]["FSOLD_FILE_NAME"];
                obj.iherFSFILE_PATH_H = sqlResult[i]["FSFILE_PATH_H"];
                obj.iherFSFILE_PATH_L = sqlResult[i]["FSFILE_PATH_L"];
                obj.strARC_ID = sqlResult[i]["FSARC_ID"];
                obj.iherforeingFSPGMNAME = sqlResult[i]["FSPGMNAME"];
                obj.iherforeingFSTYPE_NAME = sqlResult[i]["FSTYPE_NAME"];
                obj.iherFSCREATED_BY = sqlResult[i]["FSCREATED_BY"];
                obj.iherFDCREATED_DATE = sqlResult[i]["FDCREATED_DATE"];
                obj.iherFSUPDATED_BY = sqlResult[i]["FSUPDATED_BY"];
                obj.iherFDUPDATED_DATE = sqlResult[i]["FDUPDATED_DATE"];
                returnData.Add(obj);
            }
            return returnData;
        }

        /// <summary>
        /// STO100_01 void RefreshModify(WSARCHIVE.Class_ARCHIVE myClass)用的
        /// </summary>
        /// <param name="InClass">Class_ARCHIVE的類別</param>
        /// <returns>Class_ARCHIVE_RefreshModify</returns>
        [WebMethod]
        public Class_ARCHIVE_RefreshModify GetARCHIVE_RefreshModify(Class_ARCHIVE InClass)
        {
            Class_ARCHIVE_RefreshModify returnClass = new Class_ARCHIVE_RefreshModify();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPROG_ID", InClass.FSID);
            sqlParameters.Add("FNEPISODE", InClass.FNEPISODE.ToString().Trim());
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_ARCHIVE_RefreshModify", sqlParameters, out sqlResult))
                return new Class_ARCHIVE_RefreshModify();
            for (int i = 0; i < sqlResult.Count; i++)
            {
                returnClass.FSPROG_ID_NAME = sqlResult[i]["FSPGMNAME"];
                returnClass.FSEPISODE_NAME = sqlResult[i]["FSPGDNAME"];//FSPGDNAME
                returnClass.FNTOTEPISODE = sqlResult[i]["FNTOTEPISODE"];
                //returnClass.FSPROG_ID_NAME = sqlResult[i]["FSPROG_ID_NAME"];
                //returnClass.FSLESSCLAM = sqlResult[i]["FSLESSCLAM"];
                //returnClass.FSSUPERVISOR = sqlResult[i]["FSSUPERVISOR"];
                returnClass.FNLENGTH = sqlResult[i]["FNLENGTH"];
                returnClass.FSCHANNEL_ID = sqlResult[i]["FSCHANNEL"];
            }
            return returnClass;
        }

        [WebMethod]
        public Class_ARCHIVE_RefreshModify GetARCHIVE_RefreshModify_P(Class_ARCHIVE InClass)
        {
            Class_ARCHIVE_RefreshModify returnClass = new Class_ARCHIVE_RefreshModify();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSPROMO_ID", InClass.FSID);
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_ARCHIVE_RefreshModify_P", sqlParameters, out sqlResult))
                return new Class_ARCHIVE_RefreshModify();
            for (int i = 0; i < sqlResult.Count; i++)
            {
                returnClass.FSPROG_ID_NAME = sqlResult[i]["FSPGMNAME"];
                returnClass.FSEPISODE_NAME = "";//FSPGDNAME
                //returnClass.FNTOTEPISODE = sqlResult[i]["FNTOTEPISODE"];
                //returnClass.FSPROG_ID_NAME = sqlResult[i]["FSPROG_ID_NAME"];
                //returnClass.FSLESSCLAM = sqlResult[i]["FSLESSCLAM"];
                //returnClass.FSSUPERVISOR = sqlResult[i]["FSSUPERVISOR"];
                //returnClass.FNLENGTH = sqlResult[i]["FNLENGTH"];
                returnClass.FSCHANNEL_ID = sqlResult[i]["FSMAIN_CHANNEL_ID"];
            }
            return returnClass;
        }

        /// <summary>
        /// 取得METADATA以便修改
        /// </summary>
        /// <param name="VAPDClass"></param>
        /// <returns></returns>
        [WebMethod]
        public Class_ARCHIVE_VAPD_METADATA GetTBLOG_VAPD_METADATA(Class_ARCHIVE_VAPD VAPDClass)
        {
            Class_ARCHIVE_VAPD_METADATA returnClass = new Class_ARCHIVE_VAPD_METADATA();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSTABLENAME", VAPDClass.FSTableName);
            sqlParameters.Add("FSFILE_NO", VAPDClass.FSFile_No);
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VAPD_METADATA", sqlParameters, out sqlResult))
                return new Class_ARCHIVE_VAPD_METADATA();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            for (int i = 0; i < sqlResult.Count; i++)
            {
                returnClass.FSTableName = VAPDClass.FSTableName;
                returnClass.FSFile_No = VAPDClass.FSFile_No;
                returnClass.FSFileType = VAPDClass.FSFileType;
                returnClass.FSTitle = VAPDClass.FSTitle;
                returnClass.FSFileSize = VAPDClass.FSFileSize;
                returnClass.FSOldFileName = VAPDClass.FSOldFileName;
                returnClass.FSTSMPath = VAPDClass.FSTSMPath;
                returnClass.FSCreatedBy = VAPDClass.FSCreatedBy;
                returnClass.FSCreateDate = VAPDClass.FSCreateDate;
                returnClass.FSUpdatedBy = VAPDClass.FSCreatedBy;
                returnClass.FSUpdateDate = VAPDClass.FSUpdateDate;
                returnClass.MTTitle = sqlResult[i]["FSTITLE"];
                returnClass.MTDescription = sqlResult[i]["FSDESCRIPTION"];
                returnClass.FSSUPERVISOR = sqlResult[i]["FSSUPERVISOR"];
            }
            return returnClass;
        }

        [WebMethod]
        public string InsTBARCHIVE_SET(string PGType, string PGM_Id, string Episode, int v_number, int a_number, int p_number, int d_number, string createrId)
        {
            bool IsSuccess;
            string returnString = "";
            //
            Dictionary<string, string> sqlParametersCondiction = new Dictionary<string, string>();
            sqlParametersCondiction.Add("FSTYPE", PGType);
            sqlParametersCondiction.Add("FSID", PGM_Id);
            sqlParametersCondiction.Add("FNEPISODE", Episode);
            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBARCHIVE_SET_CONDICTION_FORADDNEW", sqlParametersCondiction, out sqlResult))
            { }
            if (sqlResult.Count == 0)
            {
                //
                Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
                sqlParameters.Add("FSTYPE", PGType);
                sqlParameters.Add("FSID", PGM_Id);
                sqlParameters.Add("FNEPISODE", Episode);
                sqlParameters.Add("FNVIDEO", v_number.ToString());
                sqlParameters.Add("FNAUDIO", a_number.ToString());
                sqlParameters.Add("FNPHOTO", p_number.ToString());
                sqlParameters.Add("FNDOC", d_number.ToString());
                sqlParameters.Add("FSCREATED_BY", createrId);
                sqlParameters.Add("FSUPDATED_BY", createrId);
                IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBARCHIVE_SET", sqlParameters, createrId);
                if (IsSuccess == true)
                { returnString = "HaveAddaNew"; }
            }
            else
            { returnString = "SameId"; }
            return returnString;
        }

        [WebMethod]
        public bool InsTBLOG_ARCHIVE_ARECARD(Class_ARCHIVE ARCHIVEClass)
        {
            bool IsSuccess;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSARC_ID", ARCHIVEClass.FSARC_ID);
            sqlParameters.Add("FSTYPE", ARCHIVEClass.FSTYPE);
            sqlParameters.Add("FSID", ARCHIVEClass.FSID);
            sqlParameters.Add("FNEPISODE", ARCHIVEClass.FNEPISODE.ToString().Trim());
            sqlParameters.Add("FCCHECK_STATUS", "N");
            sqlParameters.Add("FSLESSCLAM", ARCHIVEClass.FSLESSCLAM.ToString().Trim());
            sqlParameters.Add("FSSUPERVISOR", ARCHIVEClass.FSSUPERVISOR.ToString().Trim());
            sqlParameters.Add("FSCREATED_BY", ARCHIVEClass.FSCREATED_BY);
            sqlParameters.Add("FSCHANNEL_ID", ARCHIVEClass.FSCHANNEL_ID);
            IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBARCHIVE", sqlParameters, ARCHIVEClass.FSCREATED_BY);
            return IsSuccess;
        }
        /// <summary>
        /// 重送TBARCHIVE入庫單時STO700所使用
        /// </summary>
        /// <param name="strARCHIVE_ID"></param>
        /// <param name="strClam"></param>
        /// <param name="strUSERID"></param>
        /// <returns></returns>
        [WebMethod]
        public bool UpdTBLOG_ARCHIVE_ARECARD_RESEND(string strARCHIVE_ID, string strClam, string strUSERID)
        {
            bool IsSuccess;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSARC_ID", strARCHIVE_ID);
            sqlParameters.Add("FCCHECK_STATUS", "N");
            sqlParameters.Add("FSLESSCLAM", strClam.Trim());
            sqlParameters.Add("FSUPDATED_BY", strUSERID);
            IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBARCHIVE_RESEND", sqlParameters, strUSERID);
            return IsSuccess;
        }

        /// <summary>
        /// 將修改的資料寫回資料表中
        /// </summary>
        /// <param name="VAPDClass">Class_ARCHIVE_VAPD_METADATA</param>
        /// <returns>teru:成功 false:失敗</returns>
        [WebMethod]
        public bool UpdTBLOG_VAPD_METADATA(Class_ARCHIVE_VAPD_METADATA VAPDClass)
        {
            bool IsSuccess;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSTABLENAME", VAPDClass.FSTableName);
            sqlParameters.Add("FSFILE_NO", VAPDClass.FSFile_No);
            sqlParameters.Add("FSTITLE", VAPDClass.MTTitle);
            sqlParameters.Add("FSDESCRIPTION", VAPDClass.MTDescription);
            sqlParameters.Add("FSUPDATED_BY", VAPDClass.FSUpdatedBy);
            sqlParameters.Add("FSSUPERVISOR", VAPDClass.FSSUPERVISOR);
            IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBLOG_VAPD_METADATA", sqlParameters, VAPDClass.FSUpdatedBy);
            return IsSuccess;
        }


        #region VIDEO由至軒那邊去寫
        ///// <summary>
        ///// 將資料增到TBLOG_PHOTO資料表，並且搬移檔案
        ///// </summary>
        ///// <param name="InsList"></param>
        ///// <returns></returns>
        //[WebMethod]
        //public string InsTBLOG_Video(List<ClassUploadFileClass> InsList)
        //{
        //    bool IsSuccess = false, IsMovSuccess = false;
        //    Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
        //    foreach (ClassUploadFileClass x in InsList)
        //    {
        //        sqlParameters.Add("FSFILE_NO", x.strFileId);
        //        sqlParameters.Add("FSSUBJECT_ID", x.strFileId.Substring(0, 12));
        //        sqlParameters.Add("FSTYPE", x.strType);
        //        sqlParameters.Add("FSID", x.strFSID);
        //        sqlParameters.Add("FNEPISODE", x.strEpisodeId);
        //        sqlParameters.Add("FSPLAY", "N");//主控播出"N":否
        //        sqlParameters.Add("FSARC_TYPE", x.strARC_TYPE);
        //        sqlParameters.Add("FSFILE_TYPE_LV", x.strOExtensionName);
        //        sqlParameters.Add("FSTITLE", x.strMetadataFSTITLE);
        //        sqlParameters.Add("FSDESCRIPTION", x.strMetadataFSFSDESCRIPTION);
        //        sqlParameters.Add("FSFILE_SIZE", x.srtFileSize);
        //        sqlParameters.Add("FCFILE_STATUS", "A");//待審核
        //        sqlParameters.Add("FSOLD_FILE_NAME", x.strOFileName);//原始檔名
        //        sqlParameters.Add("FSFILE_PATH_L", x.strDirectory);
        //        sqlParameters.Add("FNDIR_ID", "0");
        //        sqlParameters.Add("FSCHANNEL_ID", x.strChannelId);
        //        sqlParameters.Add("FSARC_ID", x.strARC_ID);
        //        sqlParameters.Add("FSCREATED_BY", x.strUserId);
        //        sqlParameters.Add("FSUPDATED_BY", "N/A");
        //        IsMovSuccess = fileMove(x, "LowVideo");
        //        if (IsMovSuccess == false)
        //            return "false";
        //        IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBLOG_PHOTO", sqlParameters, x.strUserId);
        //        sqlParameters.Clear();
        //        if (IsSuccess == false)
        //            return "false";
        //    }
        //    return "true";
        //}
        #endregion

        /// <summary>
        /// STO100_01.xaml.cs 中 private void OKButton_Click(object sender, RoutedEventArgs e) 所使用
        /// </summary>
        /// <param name="FSARC_ID">入庫單號</param>
        /// <param name="FSTYPE">ARCFSTYPE:G節目帶 P宣傳帶</param>
        /// <param name="FSID">FSID:節目ID</param>
        /// <param name="FNEPISODE">ARCFNEPISODE:節目集數</param>
        /// <param name="FCCHECK_STATUS">N:審核中 Y:審核完成</param>
        /// <param name="FSCHANNEL_ID">ARCCHANNEL:節目所屬頻道</param>
        /// <param name="FSUPDATE_BY">修改者ID</param>
        /// <returns></returns>
        [WebMethod]
        public bool UpdTBARCHIVE(string FSARC_ID, string FSTYPE, string FSID, string FNEPISODE, string FCCHECK_STATUS, string FSCHANNEL_ID, string FSUPDATE_BY, string FSLESSCLAM, string FSSUPERVISOR)
        {
            bool IsSuccess;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSARC_ID", FSARC_ID);
            sqlParameters.Add("FSTYPE", FSTYPE);
            sqlParameters.Add("FSID", FSID);
            sqlParameters.Add("FNEPISODE", FNEPISODE);
            sqlParameters.Add("FCCHECK_STATUS", FCCHECK_STATUS);
            sqlParameters.Add("FSCHANNEL_ID", FSCHANNEL_ID);
            sqlParameters.Add("FSUPDATED_BY", FSUPDATE_BY);
            sqlParameters.Add("FSLESSCLAM", FSLESSCLAM);
            sqlParameters.Add("FSSUPERVISOR", FSSUPERVISOR);
            IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBARCHIVE", sqlParameters, FSUPDATE_BY);
            return IsSuccess;
        }

        [WebMethod]
        public bool UpdTBARCHIVE_Finished_ForFLOW(string ARCHIVEId, string FCCHECK_STATUS, string userId)
        {
            bool IsSuccess;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSARC_ID", ARCHIVEId);
            sqlParameters.Add("FCCHECK_STATUS", FCCHECK_STATUS);
            sqlParameters.Add("FSUPDATED_BY", userId);
            IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBARCHIVE_FINISH", sqlParameters, userId);
            return IsSuccess;
        }

        //[WebMethod]
        //public bool UpdTBARCHIVE_SET(string FSTYPE, string FSID, string FNEPISODE, string FNVIDEO, string FNAUDIO, string FNPHOTO, string FNDOC, string FSUPDATED_BY)
        [WebMethod]
        public bool UpdTBARCHIVE_SET_Upd(Class_ARCHIVE_SET myClass)
        {
            bool IsSuccess;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            //sqlParameters.Add("FSTYPE", FSTYPE);
            //sqlParameters.Add("FSID", FSID);
            //sqlParameters.Add("FNEPISODE", FNEPISODE);
            //sqlParameters.Add("FNVIDEO", FNVIDEO);
            //sqlParameters.Add("FNAUDIO", FNAUDIO);
            //sqlParameters.Add("FNPHOTO", FNPHOTO);
            //sqlParameters.Add("FNDOC", FNDOC);
            //sqlParameters.Add("FSUPDATED_BY", FSUPDATED_BY);

            sqlParameters.Add("SET_ID", myClass.SET_ID.ToString().Trim());
            sqlParameters.Add("FNVIDEO", myClass.FNVIDEO);
            sqlParameters.Add("FNAUDIO", myClass.FNAUDIO);
            sqlParameters.Add("FNPHOTO", myClass.FNPHOTO);
            sqlParameters.Add("FNDOC", myClass.FNDOC);
            sqlParameters.Add("FSUPDATED_BY", myClass.FSUPDATED_BY);
            IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBARCHIVE_SET", sqlParameters, myClass.FSUPDATED_BY);
            return IsSuccess;
        }


        /// <summary>
        /// 將FSARC_ID值填入資料表中
        /// </summary>
        /// <param name="InsList">Class_ARCHIVE_TBLOG_VIDEO</param>
        /// <returns>bool treu:成功 false:失敗</returns>
        [WebMethod]
        public bool UpdTBLOG_Video_STT(List<Class_ARCHIVE_TBLOG_VIDEO> InsList)
        {
            bool IsSuccess;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            foreach (Class_ARCHIVE_TBLOG_VIDEO x in InsList)
            {
                sqlParameters.Add("FSFILE_NO", x.iherFSFILE_NO);
                sqlParameters.Add("FSARC_ID", x.strARC_ID);
                sqlParameters.Add("FSTITLE", x.strMetadataFSTITLE);
                sqlParameters.Add("FSDESCRIPTION", x.strMetadataFSFSDESCRIPTION);
                if (x.strFSCHANGE_FILE_NO == null)
                { sqlParameters.Add("FSCHANGE_FILE_NO", ""); }
                else
                { sqlParameters.Add("FSCHANGE_FILE_NO", x.strFSCHANGE_FILE_NO); }
                sqlParameters.Add("FSUPDATED_BY", x.strUserId);
                IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBLOG_VIDEO_STT", sqlParameters, x.strUserId);
                sqlParameters.Clear();
                if (IsSuccess == false)
                    return false;
            }
            return true;
        }
        //很重要的一個路徑變數
        //@"\UploadFolder\";//
        private bool fileMove(ClassUploadFileClass x, string scase)
        {
            string fileFrom = string.Empty;
            string fileFromV = string.Empty;
            string fileDest = x.strDirectory;
            //string fileDest2 = string.Concat(System.IO.Path.GetDirectoryName(fileDest), @"\", System.IO.Path.GetFileNameWithoutExtension(fileDest), "_M", System.IO.Path.GetExtension(fileDest));
            string fileDest2 = string.Concat(System.IO.Path.GetDirectoryName(fileDest), @"\", System.IO.Path.GetFileNameWithoutExtension(fileDest), "_M.jpg");
            string TempDir = Server.MapPath("/") + @"UploadFolder\";
            //MAM_PTS_DLL.SysConfig obj = new MAM_PTS_DLL.SysConfig();
            //string dirPhoto = obj.sysConfig_Read("/ServerConfig/STO_Config/UPLOAD_FOLDER_PHOTO");
            //string dirDoc = obj.sysConfig_Read("/ServerConfig/STO_Config/UPLOAD_FOLDER_DOC");
            //string dirAudio = obj.sysConfig_Read("/ServerConfig/STO_Config/UPLOAD_FOLDER_AUDIO");

            try
            {
                //MAM_PTS_DLL.Log.AppendTrackingLog("WSARchive/fileMove", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "Path = " + fileDest);// + @"\" + x.strFileId + "." + x.strOExtensionName);
                switch (scase)
                {
                    case ("Photo"):
                        fileFrom = string.Concat(TempDir, x.strFileId, ".", x.strOExtensionName);//@"\\10.13.220.2\uploadfolder\Pictures\"
                        //fileFromV = string.Concat(TempDir, x.strFileId, "_M.", x.strOExtensionName);//@"\\10.13.220.2\uploadfolder\Pictures\"
                        fileFromV = string.Concat(TempDir, x.strFileId, "_M.jpg");
                        break;
                    case ("Audio"):
                        fileFrom = string.Concat(TempDir, x.strFileId, ".", x.strOExtensionName);//@"\\10.13.220.2\uploadfolder\Audios\"
                        break;
                    case ("Doc"):
                        fileFrom = string.Concat(TempDir, x.strFileId, ".", x.strOExtensionName);
                        break;
                    case ("LowVideo"):
                        fileFrom = string.Concat(TempDir, x.strFileId, ".", x.strOExtensionName);
                        break;
                }

                string dirPath = System.IO.Path.GetDirectoryName(fileDest);
                if (!System.IO.Directory.Exists(dirPath))
                    System.IO.Directory.CreateDirectory(dirPath);
                System.IO.File.Move(fileFrom, fileDest);        // + @"\" + x.strFileId + "." + x.strOExtensionName);

                if (scase == "Photo")
                    System.IO.File.Move(fileFromV, fileDest2);

                return true;
            }
            catch (Exception ex)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("File Move Problem", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, x.strOFileName + ",ex = " + ex.ToString());
                return false;
            }

        }

        /// <summary>
        /// 將資料增到TBLOG_AUDIO資料表，並且搬移檔案
        /// </summary>
        /// <param name="InsList"></param>
        /// <returns></returns>
        [WebMethod]
        public string InsTBLOG_AUDIO(List<ClassUploadFileClass> InsList)
        {
            bool IsSuccess = false, IsMovSuccess = false;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            foreach (ClassUploadFileClass x in InsList)
            {
                sqlParameters.Add("FSFILE_NO", x.strFileId);
                sqlParameters.Add("FSSUBJECT_ID", x.strFileId.Substring(0, 12));
                sqlParameters.Add("FSTYPE", x.strType);
                sqlParameters.Add("FSID", x.strFSID);
                sqlParameters.Add("FNEPISODE", x.strEpisodeId);
                sqlParameters.Add("FSARC_TYPE", x.strARC_TYPE);
                sqlParameters.Add("FSFILE_TYPE", x.strOExtensionName);
                sqlParameters.Add("FSTITLE", x.strMetadataFSTITLE);
                sqlParameters.Add("FSDESCRIPTION", x.strMetadataFSFSDESCRIPTION);
                sqlParameters.Add("FSFILE_SIZE", x.srtFileSize);
                sqlParameters.Add("FCFILE_STATUS", "A");//待審核
                sqlParameters.Add("FSOLD_FILE_NAME", x.strOFileName);//原始檔名
                if (x.strFSCHANGE_FILE_NO == null)
                { sqlParameters.Add("FSCHANGE_FILE_NO", ""); }
                else
                { sqlParameters.Add("FSCHANGE_FILE_NO", x.strFSCHANGE_FILE_NO.Trim()); }
                sqlParameters.Add("FSFILE_PATH", x.strDirectory);
                sqlParameters.Add("FNDIR_ID", x.FS64FNDIR_ID);
                sqlParameters.Add("FSCHANNEL_ID", x.strChannelId);
                sqlParameters.Add("FSARC_ID", x.strARC_ID);
                sqlParameters.Add("FSCREATED_BY", x.strUserId);
                sqlParameters.Add("FSUPDATED_BY", "N/A");
                IsMovSuccess = fileMove(x, "Audio");
                if (IsMovSuccess == false)
                    return "false";
                IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBLOG_AUDIO", sqlParameters, x.strUserId);
                sqlParameters.Clear();
                if (IsSuccess == false)
                    return "false";
            }
            return "true";
        }

        /// <summary>
        /// 將資料增到TBLOG_DOC資料表，並且搬移檔案
        /// </summary>
        /// <param name="InsList"></param>
        /// <returns></returns>
        [WebMethod]
        public string InsTBLOG_DOC(List<ClassUploadFileClass> InsList)
        {
            bool IsSuccess = false, IsMovSuccess = false;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            foreach (ClassUploadFileClass x in InsList)
            {
                WSExtFileInfo DocInfo = new WSExtFileInfo();
                WSExtDoc ExtDoc = new WSExtDoc();
                sqlParameters.Add("FSFILE_NO", x.strFileId);
                sqlParameters.Add("FSSUBJECT_ID", x.strFileId.Substring(0, 12));
                sqlParameters.Add("FSTYPE", x.strType);
                sqlParameters.Add("FSID", x.strFSID);
                sqlParameters.Add("FNEPISODE", x.strEpisodeId);
                sqlParameters.Add("FSARC_TYPE", x.strARC_TYPE);
                sqlParameters.Add("FSFILE_TYPE", x.strOExtensionName);
                string eName = x.strOExtensionName.ToLower();
                string Doccontet = "";


                sqlParameters.Add("FSTITLE", x.strMetadataFSTITLE);
                sqlParameters.Add("FSDESCRIPTION", x.strMetadataFSFSDESCRIPTION);
                sqlParameters.Add("FSFILE_SIZE", x.srtFileSize);
                sqlParameters.Add("FCFILE_STATUS", "A");//待審核
                sqlParameters.Add("FSOLD_FILE_NAME", x.strOFileName);//原始檔名
                if (x.strFSCHANGE_FILE_NO == null)
                { sqlParameters.Add("FSCHANGE_FILE_NO", "N/A"); }
                else
                { sqlParameters.Add("FSCHANGE_FILE_NO", x.strFSCHANGE_FILE_NO); }//
                sqlParameters.Add("FSFILE_PATH", x.strDirectory);
                sqlParameters.Add("FNDIR_ID", x.FS64FNDIR_ID);
                sqlParameters.Add("FSCHANNEL_ID", x.strChannelId);
                sqlParameters.Add("FSARC_ID", x.strARC_ID);
                sqlParameters.Add("FSCREATED_BY", x.strUserId);
                IsMovSuccess = fileMove(x, "Doc");
                if (IsMovSuccess == false)
                    return "false";
                string strDir = x.strDirectory;//Server.MapPath("/") + @"UploadFolder\" + x.strFileId + "." + x.strOExtensionName;//@"D:HK.doc";//x.strDirectory;//.Replace(@"\\",@"\");

                if ((eName == "doc") || (eName == "docx") || (eName == "pdf") || (eName == "ppt") || (eName == "pptx") || eName == "xml" || eName == "xls" || eName == "xlsx" || eName == "rtf")
                {//|| (eName == "txt")
                    Doccontet = ExtDoc.ExtDocInfo(strDir);// + @"\" + x.strFileId + "." + eName);
                }
                else if (eName == "txt")
                {
                    StreamReader sr = new StreamReader(strDir);
                    string strNoEncode = sr.ReadToEnd();
                    sr.Close();

                    StreamReader sr2 = new StreamReader(strDir, Encoding.Default);
                    string strEncode = sr2.ReadToEnd();
                    sr2.Close();

                    string[] noEcodeArr = strNoEncode.Split(new char[] { '?' }, StringSplitOptions.RemoveEmptyEntries);
                    string[] ecodeArr = strEncode.Split(new char[] { '?' }, StringSplitOptions.RemoveEmptyEntries);
                    if (noEcodeArr.Length < ecodeArr.Length)
                    {
                        Doccontet = strNoEncode;
                    }
                    else
                    {
                        Doccontet = strEncode;
                    }
                    //MAM_PTS_DLL.Log.WriteTxtLog(@"D:\LogFiles\TestArsss.txt", Doccontet + Environment.NewLine +"noEcodeArr.Length"+ noEcodeArr.Length + "ecodeArr.Length" + ecodeArr.Length);

                }
                sqlParameters.Add("FSCONTENT", Doccontet);
                IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBLOG_DOC", sqlParameters, x.strUserId);
                sqlParameters.Clear();
                if (IsSuccess == false)
                    return "false";
            }
            return "true";
        }

        /// <summary>
        /// 將資料增到TBLOG_PHOTO資料表，並且搬移檔案
        /// </summary>
        /// <param name="InsList"></param>
        /// <returns></returns>
        [WebMethod]
        public string InsTBLOG_PHOTO(List<ClassUploadFileClass> InsList)
        {
            bool IsSuccess = false, IsMovSuccess = false;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();

            WSLogSystem logSystem = new WSLogSystem();

            try
            {
                foreach (ClassUploadFileClass x in InsList)
                {
                    sqlParameters.Add("FSFILE_NO", x.strFileId);
                    sqlParameters.Add("FSSUBJECT_ID", x.strFileId.Substring(0, 12));
                    sqlParameters.Add("FSTYPE", x.strType);
                    sqlParameters.Add("FSID", x.strFSID);
                    sqlParameters.Add("FNEPISODE", x.strEpisodeId);
                    sqlParameters.Add("FSARC_TYPE", x.strARC_TYPE);
                    sqlParameters.Add("FSFILE_TYPE", x.strOExtensionName);
                    sqlParameters.Add("FSTITLE", x.strMetadataFSTITLE);
                    sqlParameters.Add("FSDESCRIPTION", x.strMetadataFSFSDESCRIPTION);
                    sqlParameters.Add("FSFILE_SIZE", x.srtFileSize);
                    sqlParameters.Add("FNWIDTH", "0");
                    sqlParameters.Add("FNHEIGHT", "0");
                    sqlParameters.Add("FNDPI", "0");
                    sqlParameters.Add("FCFILE_STATUS", "A");//待審核
                    sqlParameters.Add("FSOLD_FILE_NAME", x.strOFileName);//原始檔名
                    if (x.strFSCHANGE_FILE_NO == null)
                    { sqlParameters.Add("FSCHANGE_FILE_NO", "N/A"); }
                    else
                    { sqlParameters.Add("FSCHANGE_FILE_NO", x.strFSCHANGE_FILE_NO); }//x.strFSCHANGE_FILE_NO
                    sqlParameters.Add("FSFILE_PATH", x.strDirectory);
                    sqlParameters.Add("FNDIR_ID", x.FS64FNDIR_ID);
                    sqlParameters.Add("FSCHANNEL_ID", x.strChannelId);
                    sqlParameters.Add("FSARC_ID", x.strARC_ID);
                    sqlParameters.Add("FSCREATED_BY", x.strUserId);
                    sqlParameters.Add("FSUPDATED_BY", "N/A");

                    string eName = x.strOExtensionName.ToLower();

                    //ExifFile exifFile = ExifFile.Read(Server.MapPath("/") + @"UploadFolder\" + x.strFileId + "." + eName);//@"D:\WebSite\PTSMAM\UploadFolder\G201200400010061.jpg");//+ System.IO.Path.GetFileName(x.strDirectory));
                    logSystem.AppendTrackingLog("InsTBLOG_PHOTO", WSLogSystem.TRACKING_LEVEL.TRACE, "START Create EXIF");
                    //ExifTagCollection _exif = new ExifTagCollection(Server.MapPath("/") + @"UploadFolder\" + x.strFileId + "." + eName);

                    //XDocument xdocument = new XDocument();
                    //List<XElement> elements = new List<XElement>();

                    //foreach (ExifTag item in _exif)
                    //{
                    //    if (item.FieldName == "MakerNote")
                    //        continue;

                    //    elements.Add(new XElement(item.FieldName, item.Value));
                    //}

                    ////foreach (ExifProperty item in exifFile.Properties.Values)
                    ////{
                    ////    elements.Add(new XElement(item.Name, item.Value));
                    ////}

                    //xdocument.AddFirst(new XElement("EXIFS_ROOT", elements.ToArray()));
                    //string exifXmlStr = elements.Count == 0 ? "" : xdocument.ToString();

                    ExifTagCollection _exif = new ExifTagCollection(Server.MapPath("/") + @"UploadFolder\" + x.strFileId + "." + eName, false, true);


                    XDocument xdocument = new XDocument();
                    List<XElement> elements = new List<XElement>();
                    string exifXmlStr = "<EXIFS_ROOT>";

                    foreach (ExifTag item in _exif)
                    {
                        if (item.FieldName != null)
                        {
                            if (item.FieldName == "MakerNote")
                            {
                                continue;
                                //var stringA = Encoding.UTF8.GetString(Encoding.GetEncoding("ASCII").GetBytes(item.Value));
                            }
                            exifXmlStr += Environment.NewLine + "<" + item.FieldName + ">";
                            exifXmlStr += item.Value + "</" + item.FieldName + ">";

                            // elements.Add(new XElement(item.FieldName, item.Value));
                        }
                    }
                    exifXmlStr += "</EXIFS_ROOT>";

                    sqlParameters.Add("FSEXIF", exifXmlStr);
                    logSystem.AppendTrackingLog("InsTBLOG_PHOTO", WSLogSystem.TRACKING_LEVEL.TRACE, "END Create EXIF! Value=" + exifXmlStr);

                    //產生預覽圖檔
                    logSystem.AppendTrackingLog("InsTBLOG_PHOTO", WSLogSystem.TRACKING_LEVEL.TRACE, "START Generate Thumbnail Imge!");
                    bool isGenImgSuccess = false;
                    //isGenImgSuccess = MAM_PTS_DLL.PhotoSrv.genThumbnailImg(Server.MapPath("/") + @"UploadFolder\" + x.strFileId + "." + eName, Server.MapPath("/") + @"UploadFolder\" + x.strFileId + "_M." + eName, 320);
                    isGenImgSuccess = MAM_PTS_DLL.PhotoSrv.genThumbnailImg(Server.MapPath("/") + @"UploadFolder\" + x.strFileId + "." + eName, Server.MapPath("/") + @"UploadFolder\" + x.strFileId + "_M.jpg", 320);

                    logSystem.AppendTrackingLog("InsTBLOG_PHOTO", WSLogSystem.TRACKING_LEVEL.TRACE, "END Generate Thumbnail Imge! Value=" + isGenImgSuccess.ToString());
                    if (isGenImgSuccess == false)
                        return "false";

                    logSystem.AppendTrackingLog("InsTBLOG_PHOTO", WSLogSystem.TRACKING_LEVEL.TRACE, "START Move Thumbnail Imge! Value=" + x.strDirectory);

                    IsMovSuccess = fileMove(x, "Photo");


                    logSystem.AppendTrackingLog("InsTBLOG_PHOTO", WSLogSystem.TRACKING_LEVEL.TRACE, "END Move Thumbnail Imge! Value=" + IsMovSuccess.ToString());

                    if (IsMovSuccess == false)
                        return "false";
                    logSystem.AppendTrackingLog("InsTBLOG_PHOTO", WSLogSystem.TRACKING_LEVEL.TRACE, "START Insert into TBLOG_PHOTO!");
                    IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBLOG_PHOTO", sqlParameters, x.strUserId);

                    logSystem.AppendTrackingLog("InsTBLOG_PHOTO", WSLogSystem.TRACKING_LEVEL.TRACE, "END Insert into TBLOG_PHOTO!Value=" + IsSuccess.ToString());

                    sqlParameters.Clear();
                    if (IsSuccess == false)
                        return "false";
                }
            }
            catch (Exception ex)
            {
                logSystem.AppendTrackingLog("InsTBLOG_PHOTO", WSLogSystem.TRACKING_LEVEL.ERROR, ex.Message);
                return "false";
            }
            return "true";
        }

        /// <summary>
        /// 刪除TBLOG_AUDIO裡面的資料維護用 PS:不刪實體檔案
        /// </summary>
        /// <param name="FSFILENO">類別Class_ARCHIVE_VAPD的實體</param>
        /// <param name="UserId">USERId</param>
        /// <returns>true:成功 false:失敗</returns>
        [WebMethod]
        public bool DelTBLOG_VIDEO(Class_ARCHIVE_VAPD VAPD, string UserId)
        {
            bool IsSuccess;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSFILE_NO", VAPD.FSFile_No);
            IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_TBLOG_VIDEO_BY_FSFILE_NO", sqlParameters, UserId);
            return IsSuccess;
        }

        /// <summary>
        /// 刪除TBLOG_AUDIO裡面的資料維護用
        /// </summary>
        /// <param name="FSFILENO">類別Class_ARCHIVE_VAPD的實體</param>
        /// <param name="UserId">USERId</param>
        /// <returns>true:成功 false:失敗</returns>
        [WebMethod]
        public bool DelTBLOG_AUDIO(Class_ARCHIVE_VAPD VAPD, string UserId)
        {
            bool IsSuccess;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSFILE_NO", VAPD.FSFile_No);
            try
            {
                System.IO.File.Delete(VAPD.FSTSMPath);// + @"\" + VAPD.FSFile_No + "." + VAPD.FSFileType);
            }
            catch (Exception ex)
            { return IsSuccess = false; }
            IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_TBLOG_AUDIO_BY_FSFILE_NO", sqlParameters, UserId);
            return IsSuccess;
        }

        /// <summary>
        /// 刪除TBLOG_PHOTO裡面的資料維護用
        /// </summary>
        /// <param name="FSFILENO">類別Class_ARCHIVE_VAPD的實體</param>
        /// <param name="UserId">USERId</param>
        /// <returns>true:成功 false:失敗</returns>
        [WebMethod]
        public bool DelTBLOG_PHOTO(Class_ARCHIVE_VAPD VAPD, string UserId)
        {
            bool IsSuccess;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSFILE_NO", VAPD.FSFile_No);
            try
            { System.IO.File.Delete(VAPD.FSTSMPath); }// + @"\" + VAPD.FSFile_No + "." + VAPD.FSFileType); }
            catch (Exception ex)
            { return IsSuccess = false; }
            IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_TBLOG_PHOTO_BY_FSFILE_NO", sqlParameters, UserId);
            return IsSuccess;
        }

        /// <summary>
        /// 刪除TBLOG_DOC裡面的資料維護用
        /// </summary>
        /// <param name="VAPD">類別Class_ARCHIVE_VAPD的實體</param>
        /// <param name="UserId">USERId</param>
        /// <returns>true:成功 false:失敗</returns>
        [WebMethod]
        public bool DelTBLOG_DOC(Class_ARCHIVE_VAPD VAPD, string UserId)
        {
            bool IsSuccess;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSFILE_NO", VAPD.FSFile_No);
            try
            {
                System.IO.File.Delete(VAPD.FSTSMPath);// + @"\" + VAPD.FSFile_No + "." + VAPD.FSFileType);
            }
            catch (Exception ex)
            { return IsSuccess = false; }
            IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_TBLOG_DOC_BY_FSFILE_NO", sqlParameters, UserId);
            return IsSuccess;
        }

        [WebMethod]
        public bool DelTBARCHIVE_SET(Class_ARCHIVE_SET set, string UserId)
        {
            bool IsSuccess;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("SET_ID", set.SET_ID.ToString());
            IsSuccess = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_TBARCHIVE_SET", sqlParameters, UserId);
            return IsSuccess;
        }

        [WebMethod]
        public List<Class_CODE> GetTBARCHIVE_CODE()
        {
            List<Class_CODE> m_CodeData = new List<Class_CODE>();
            m_CodeData = MAMFunctions.Transfer_Class_CODE("SP_Q_TBARCHIVE_CODE");
            return m_CodeData;
        }

        [WebMethod]
        public List<Class_CODE_TBFILE_TYPE> GetTBARCHIVE_CODE_TBFILE_TYPE()
        {
            List<Class_CODE_TBFILE_TYPE> m_CodeData = new List<Class_CODE_TBFILE_TYPE>();
            m_CodeData = MAMFunctions.Transfer_Class_CODE_TBFILE_TYPE("SP_Q_TBARCHIVE_CODE_TBFILE_TYPE");
            return m_CodeData;
        }

        //用類型、編碼、集別取得入庫單檔
        [WebMethod]
        public List<Class_ARCHIVE> GetTBARCHIVE_BYIDEPISODE(string strBRO_TYPE, string strID, string strEPISODE, string strTBNAME, string USERID)
        {
            List<Class.Class_ARCHIVE> returnData = new List<Class.Class_ARCHIVE>();
            string strReturn = "";
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult = new List<Dictionary<string, string>>();
            sqlParameters.Add("FSTYPE", strBRO_TYPE);
            sqlParameters.Add("FSID", strID);
            sqlParameters.Add("FNEPISODE", strEPISODE);
            sqlParameters.Add("FSCREATED_BY_NAME", USERID);
            //List<Class_ARCHIVE> sqlResult = new List<Class_ARCHIVE>();
            if (strTBNAME == "TBARCHIVE")
            {
                if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBARCHIVE_BYIDEPISODE", sqlParameters, out sqlResult))
                    return new List<Class_ARCHIVE>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            }
            else if (strTBNAME == "TBARCHIVE_EXCHANGE")
            {
                if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBARCHIVE_EXCHANGE_BYIDEPISODE", sqlParameters, out sqlResult))
                    return new List<Class_ARCHIVE>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            }

            for (int i = 0; i < sqlResult.Count; i++)
            {
                Class.Class_ARCHIVE obj = new Class.Class_ARCHIVE();
                // --------------------------------
                obj.FSARC_ID = sqlResult[i]["FSARC_ID"];
                obj.FSTYPE = sqlResult[i]["FSTYPE"];
                obj.FSID = sqlResult[i]["FSID"];
                obj.FNEPISODE = Convert.ToInt16(sqlResult[i]["FNEPISODE"]);
                //obj.FDARC_DATE = Convert.ToDateTime(sqlResult[i]["FDARC_DATE"]);
                obj.FCCHECK_STATUS = sqlResult[i]["FCCHECK_STATUS"];
                obj.FSCREATED_BY = sqlResult[i]["FSCREATED_BY"];
                obj.FDCREATED_DATE = Convert.ToDateTime(sqlResult[i]["FDCREATED_DATE"]);
                obj.FSUPDATED_BY = sqlResult[i]["FSUPDATED_BY"];
                obj.FDUPDATED_DATE = Convert.ToDateTime(sqlResult[i]["FDUPDATED_DATE"]);
                obj.FSCHANNEL_ID = sqlResult[i]["FSCHANNEL_ID"];
                obj.FSSUPERVISOR = sqlResult[i]["FSSUPERVISOR"];
                obj.FSLESSCLAM = sqlResult[i]["FSLESSCLAM"];
                //自己串的欄位
                obj.FSCREATED_BY_NAME = sqlResult[i]["FSCREATED_BY_NAME"];
                obj.FSUPDATED_BY_NAME = sqlResult[i]["FSUPDATED_BY_NAME"];
                obj.WANT_FDCREATEDDATE = sqlResult[i]["WANT_FDCREATEDDATE"];
                obj.FSPROG_ID_NAME = sqlResult[i]["FSPROG_ID_NAME"];
                obj.FNEPISODE_NAME = sqlResult[i]["FNEPISODE_NAME"];

                //obj.FSPROG_ID_NAME = fnQueryProgName(sqlResult[i]["FSPROG_ID"]);
                //obj.FNEPISODE_NAME = fnQueryEpisodeName(sqlResult[i]["FSPROG_ID"], sqlResult[i]["FNEPISODE"]);
                // --------------------------------
                returnData.Add(obj);
            }

            return returnData;
        }

        #endregion

        [WebMethod]
        public string UploadFolderPATH()
        {
            //System.Windows.Forms.SystemInformation.ComputerName
            //System.Net.IPAddress ip4 = new System.Net.IPAddress();
            //ip4.Address
            //return @"\\10.13.220.2\UploadFolder\";//Server.MapPath("/") + @"UploadFolder\";
            return Server.MapPath("/") + @"UploadFolder\";
        }

        [WebMethod]
        public string PreViewDocContent(string iniFileNo)
        {
            string rtnString = "";
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult = new List<Dictionary<string, string>>();
            sqlParameters.Add("FSFILE_NO", iniFileNo);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_DOC", sqlParameters, out sqlResult))
                return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            for (int i = 0; i < sqlResult.Count; i++)
            {
                rtnString = sqlResult[i]["FSCONTENT"];
            }
            return rtnString;
        }

        [WebMethod]
        public string fnGetMediaFileProperty(string filePath)
        {
            string result = string.Empty;
            MAM_PTS_DLL.SysConfig objSysConfig = new MAM_PTS_DLL.SysConfig();
            System.Diagnostics.Process myProcess = new System.Diagnostics.Process();
            //myProcess.StartInfo.FileName = @"D:\0selfsys\MediaInfoCLI.exe";   //改成呼叫系統設定檔
            myProcess.StartInfo.FileName = objSysConfig.sysConfig_Read("/ServerConfig/TOOLS_CONFIG/Tool_MediaInfo_EXE_Path");
            myProcess.StartInfo.Arguments = string.Concat("--Output=XML ", filePath);
            myProcess.StartInfo.UseShellExecute = false;
            myProcess.StartInfo.CreateNoWindow = true;
            myProcess.StartInfo.RedirectStandardInput = true;
            myProcess.StartInfo.RedirectStandardOutput = true;
            myProcess.StartInfo.RedirectStandardError = true;

            try
            {
                myProcess.Start();
                result = myProcess.StandardOutput.ReadToEnd();
            }
            catch (Exception ex)
            {
                string errMsg = string.Concat("Got Error file = ", filePath, ", errmsg = ", ex.ToString());
                MAM_PTS_DLL.Log.AppendTrackingLog("DataSource/Handler_ReceiverTransCode/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
            }
            finally
            {
                myProcess.Close();
                myProcess.Dispose();
            }

            return result;
        }

        #region 報表相關



        //列印成案表單
        [WebMethod]
        public string CallREPORT_STO(string strBroID, string CREATED_BY, string strSessionID)
        {
            String strURL = "";  //要傳回去的URL

            //取列印報表的系統設定檔
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string reportSrv = SettingObj.sysConfig_Read("/ServerConfig/ReportServer");

            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSARC_ID", strBroID);             //送帶轉檔單編號
            source.Add("QUERY_BY", CREATED_BY);          //查詢者
            source.Add("FSSESSION_ID", strSessionID);    //SessionID

            List<Dictionary<string, string>> sqlResult;
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_R_TBARCHIVE_01_QRY", source, out sqlResult))
                return "";    // 發生SQL錯誤時，回傳空的東西，而不是NULL          

            if (sqlResult.Count > 0)
            {
                if (sqlResult[0]["QUERY_KEY"].ToString().StartsWith("ERROR:SESSION_TIMEOUT"))
                    return "錯誤，登入時間過久，請重新登入後才能啟動報表功能！";
                else if (sqlResult[0]["QUERY_KEY"].ToString().StartsWith("ERROR:SESSION_NOTEXIST"))
                    return "錯誤，使用者未正常登入，請重新登入後才能啟動報表功能！";
                else if (sqlResult[0]["QUERY_KEY"].ToString().StartsWith("ERROR"))
                    return "";
                else
                {
                    strURL = reportSrv + "RPT_TBARCHIVE_01&QUERY_KEY=" + sqlResult[0]["QUERY_KEY"] + "&rc:parameters=false&rs:Command=Render";
                    return Server.UrlEncode(strURL);
                }
            }
            else
                return "";
        }
        #endregion

    }
}
