﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Xml;
using System.Text;
using System.Xml.Linq;
using System.IO;
using System.Collections.Specialized;
using System.Collections;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Checksums;
using System.Net;
using Xealcom.Security;


namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// SendSQL 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class SendSQL : System.Web.Services.WebService
    {

        public class ArrPromoList
        {
            public string COMBINENO { get; set; }  //序號
            public string FCTIME_TYPE { get; set; }  //時間類型 O:定時 A:順時
            public string FSPROG_ID { get; set; }   //節目編碼,如果為Promo則為所屬節目編碼
            public string FNSEQNO { get; set; } //節目播映序號
            public string FSNAME { get; set; }  //節目或Promo名稱
            public string FSCHANNEL_ID { get; set; } //頻道編碼
            public string FSCHANNEL_NAME { get; set; }  //頻道名稱
            public string FSPLAY_TIME { get; set; } //播出時間
            public string FSDURATION { get; set; }  //長度
            public string FNEPISODE { get; set; }   //節目集數
            public string FNBREAK_NO { get; set; }  //段數
            public string FNSUB_NO { get; set; }    //支數
            public string FSFILE_NO { get; set; }   //節目或Promo檔名
            public string FSVIDEO_ID { get; set; }  //節目段落和宣傳帶主控檔案編號
            public string FSPROMO_ID { get; set; }  //宣傳帶編碼,節目的話為空
            public string FDDATE { get; set; }  //日期
            public string FNLIVE { get; set; }  //是否為Live節目,1為Live,0為一般
            public string FCTYPE { get; set; }  //節目或Promo,G為節目,P為宣傳帶
            public string FSSTATUS { get; set; }    //檔案狀態,呼叫維智的service取得檔案派送到主控檔案的狀態(其實是NASB)
            public string FSMEMO { get; set; }  //備註,包含second event
            public string FSPROG_NAME { get; set; }  //節目名稱,宣傳帶則要要加入所屬節目名稱
            public string FSSIGNAL { get; set; }  //訊號來源,如為Live節目則要將勳號來源帶入VIDEOID中
        }

        public class ArrPromoListAddSOM
        {
            public string COMBINENO { get; set; }  //序號
            public string FCTIME_TYPE { get; set; }  //時間類型 O:定時 A:順時
            public string FSPROG_ID { get; set; }   //節目編碼,如果為Promo則為所屬節目編碼
            public string FNSEQNO { get; set; } //節目播映序號
            public string FSNAME { get; set; }  //節目或Promo名稱
            public string FSCHANNEL_ID { get; set; } //頻道編碼
            public string FSCHANNEL_NAME { get; set; }  //頻道名稱
            public string FSPLAY_TIME { get; set; } //播出時間
            public string FSDURATION { get; set; }  //長度
            public string FNEPISODE { get; set; }   //節目集數
            public string FNBREAK_NO { get; set; }  //段數
            public string FNSUB_NO { get; set; }    //支數
            public string FSFILE_NO { get; set; }   //節目或Promo檔名
            public string FSVIDEO_ID { get; set; }  //節目段落和宣傳帶主控檔案編號
            public string FSPROMO_ID { get; set; }  //宣傳帶編碼,節目的話為空
            public string FDDATE { get; set; }  //日期
            public string FNLIVE { get; set; }  //是否為Live節目,1為Live,0為一般
            public string FCTYPE { get; set; }  //節目或Promo,G為節目,P為宣傳帶
            public string FSSTATUS { get; set; }    //檔案狀態,呼叫維智的service取得檔案派送到主控檔案的狀態(其實是NASB)
            public string FSMEMO { get; set; }  //備註,包含second event
            public string FSPROG_NAME { get; set; }  //節目名稱,宣傳帶則要要加入所屬節目名稱
            public string FSSIGNAL { get; set; }  //訊號來源,如為Live節目則要將勳號來源帶入VIDEOID中
            public string FSSOM { get; set; }  //起始位置
            public string FBREALTIMECODE { get; set; }  //是否採用預設段落長度
        }

        public static string ReplaceXML(string XMLString)
        {
            XMLString = XMLString.Replace("&", "&amp;");
            XMLString = XMLString.Replace("'", "&apos;");
            XMLString = XMLString.Replace(@"""", "&quot;");
            XMLString = XMLString.Replace(">", "&gt;");
            XMLString = XMLString.Replace("<", "&lt;");
            return XMLString;
        }

        public static string DTRowToString(object INDataRow)
        {
            string ReturnStr;
            try
            {
                ReturnStr = INDataRow.ToString();
                return ReturnStr;

            }
            catch (Exception ex)
            {
                return "";
            }
        }
        //查詢
        [WebMethod()]
        public string Do_Query(string strSQL, string parameters = null)
        {
            //strSQL為查詢Store Procesure Name
            //parameters為XML內容(DB_Function_Sample.txt)

            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            StringBuilder sb = new StringBuilder();

            SqlDataAdapter da = new SqlDataAdapter(strSQL, connection);
            DataTable dt = new DataTable();

            //參數剖析
            if (parameters != "")
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(parameters);
                XmlNode xmlNode = xmldoc.FirstChild;
                for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                {
                    if (xmlNode.ChildNodes[i].FirstChild == null || xmlNode.ChildNodes[i].FirstChild.Value == "")
                    {
                        SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, "");
                        da.SelectCommand.Parameters.Add(para);
                    }
                    else
                    {
                        SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, xmlNode.ChildNodes[i].FirstChild.Value);
                        da.SelectCommand.Parameters.Add(para);
                    }
                    //SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, xmlNode.ChildNodes[i].InnerXml);
                    //da.SelectCommand.Parameters.Add(para);
                }
            }

            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                da.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    sb.AppendLine("<Datas>");
                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        sb.AppendLine("<Data>");
                        for (int j = 0; j <= dt.Columns.Count - 1; j++)
                        {
                            if (Convert.IsDBNull(dt.Rows[i][j]))
                            {
                                sb.AppendLine("<" + dt.Columns[j].ColumnName + "></" + dt.Columns[j].ColumnName + ">");
                            }
                            else
                            {
                                sb.AppendLine("<" + dt.Columns[j].ColumnName + ">" + ReplaceXML(dt.Rows[i][j].ToString()) + "</" + dt.Columns[j].ColumnName + ">");
                            }
                        }
                        sb.AppendLine("</Data>");
                    }
                    sb.AppendLine("</Datas>");
                }
                else
                {
                    return sb.ToString();
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                StringBuilder sbError = new StringBuilder();
                sbError.AppendLine("<Errors>" + ex.Message + "</Errors>");
                return sbError.ToString();
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }
        }


        //public Boolean UrlExists(string URL)
        //{
        //    Boolean exists=false;
        //    WebRequest req=WebRequest.Create(URL);
        //    HttpWebResponse response;
        //    try
        //    {
        //        req=WebRequest.Create(URL);
        //       // (HttpWebResponse)req.GetResponse
        //        response= DirectCast(req.GetResponse(), HttpWebResponse);
        //        exists = true;
        //    }
        //     catch (WebException e)
        //    {
        //    }
           
        //    return exists;
        //}
       
        //查詢
        [WebMethod()]
        public string Do_Get_HD_PlayList_File_Status(string strSQL, string parameters = null)
        {
            //strSQL為查詢Store Procesure Name
            //parameters為XML內容(DB_Function_Sample.txt)
            //string PendingPath = "D:\\Test\\PTS\\PendingZone\\" ; //待審區路徑
            //string CompletePath = "D:\\Test\\PTS\\CompleteZone\\";  //帶播區路徑

            //查詢設定值
            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            //SqlConnection connection1 = new SqlConnection(sqlConnStr);
            //if (connection1.State == System.Data.ConnectionState.Closed)
            //{
            //    connection1.Open();
            //}

            //SqlDataAdapter daPGM_Setting = new SqlDataAdapter("SP_Q_TBPGM_SETTING", connection1);
            //DataTable dtPGM_Setting = new DataTable();


            //daPGM_Setting.SelectCommand.CommandType = CommandType.StoredProcedure;
            //daPGM_Setting.Fill(dtPGM_Setting);
            //connection1.Close();
            //string PendingPath = ""; //待審區路徑
            //string CompletePath = "";  //帶播區路徑
            ////string PendingPath = "D:\\Test\\PTS\\PendingZone\\"; //待審區路徑
            ////string CompletePath = "D:\\Test\\PTS\\CompleteZone\\";  //帶播區路徑
            //if (dtPGM_Setting.Rows.Count > 0)
            //{
            //    for (int i = 0; i <= dtPGM_Setting.Rows.Count - 1; i++)
            //    {
            //        if (dtPGM_Setting.Rows[i]["FSTYPE"].ToString().ToUpper() == "PENDINGPATH")
            //            PendingPath = dtPGM_Setting.Rows[i]["FSNAME"].ToString();
            //        if (dtPGM_Setting.Rows[i]["FSTYPE"].ToString().ToUpper() == "COMPLETEPATH")
            //            CompletePath = dtPGM_Setting.Rows[i]["FSNAME"].ToString();
            //    }



            //}
            MAM_PTS_DLL.SysConfig obj = new MAM_PTS_DLL.SysConfig();
            string PendingPath = obj.sysConfig_Read("/ServerConfig/PGM_Config/PendingPath");

            string CompletePath = obj.sysConfig_Read("/ServerConfig/PGM_Config/CompletePath");

            SqlConnection connection = new SqlConnection(sqlConnStr);
            StringBuilder sb = new StringBuilder();

            SqlDataAdapter da = new SqlDataAdapter(strSQL, connection);
            DataTable dt = new DataTable();

            //參數剖析
            if (parameters != "")
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(parameters);
                XmlNode xmlNode = xmldoc.FirstChild;
                for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                {
                    if (xmlNode.ChildNodes[i].FirstChild == null || xmlNode.ChildNodes[i].FirstChild.Value == "")
                    {
                        SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, "");
                        da.SelectCommand.Parameters.Add(para);
                    }
                    else
                    {
                        SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, xmlNode.ChildNodes[i].FirstChild.Value);
                        da.SelectCommand.Parameters.Add(para);
                    }
                    //SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, xmlNode.ChildNodes[i].InnerXml);
                    //da.SelectCommand.Parameters.Add(para);
                }
            }

            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                da.Fill(dt);
               
                if (dt.Rows.Count > 0)
                {
                    ImpersonateUser iu = new ImpersonateUser();
                    iu.Impersonate(@"pts.org.tw", @"mamsystem", @"p@ssw0rd");
                    sb.AppendLine("<Datas>");
                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        //A.FSPROG_ID,A.FNEPISODE,A.FSPROG_NAME,A.FNBREAK_NO,A.FCPROPERTY,A.fsplay_time,A.FSDUR,A.FNLIVE,A.FSFILE_NO,A.FSVIDEO_ID,A.FSMEMO,G.FCCHECK_STATUS,C.FCFILE_STATUS,D.FCSTATUS,F.FSBEG_TIMECODE,F.FSEND_TIMECODE 
                        string videoID = "";
                        string RealFileStatus = "";

                        sb.AppendLine("<Data>");
                        for (int j = 0; j <= dt.Columns.Count - 1; j++)
                        {
                            if (dt.Columns[j].ColumnName == "FSVIDEO_ID")
                            {
                                videoID = dt.Rows[i][j].ToString();
                                //呼叫轉檔
                              
                                
                                if (File.Exists(CompletePath + videoID.Substring(2, 6) + ".mxf") == true)
                                    RealFileStatus = "檔案已到待播區";
                                else if (File.Exists(PendingPath + videoID.Substring(2, 6) + ".mxf") == true)
                                    RealFileStatus = "檔案已到待審區";


                                
                               
                            }
                            if (Convert.IsDBNull(dt.Rows[i][j]))
                            {
                                sb.AppendLine("<" + dt.Columns[j].ColumnName + "></" + dt.Columns[j].ColumnName + ">");
                            }
                            else
                            {
                                sb.AppendLine("<" + dt.Columns[j].ColumnName + ">" + ReplaceXML(dt.Rows[i][j].ToString()) + "</" + dt.Columns[j].ColumnName + ">");
                            }
                        }
                        sb.AppendLine("<Real_File_Status>" + RealFileStatus + "</Real_File_Status>");
                        sb.AppendLine("</Data>");
                    }


                    sb.AppendLine("</Datas>");
                    //iu.Undo();
                    iu.Undo();
                }
                else
                {
                    return sb.ToString();
                }
              
                return sb.ToString();
            }
            catch (Exception ex)
            {
                StringBuilder sbError = new StringBuilder();
                sbError.AppendLine("<Errors>" + ex.Message + "</Errors>");
                return sbError.ToString();
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }
        }

        //傳入節目編碼,日期,時間起迄,決定是要新增或修改播映資料
        [WebMethod()]
        public string Do_Bank_Deail_Modify(string strSQL, string parameters = null)
        {
            string FSPROGID = "";
            string FNSEG = "";
            string FDDATE = "";
            string FNEPISODE = "";
            string FSCHANNEL_ID = "";
            string FSBTIME = "";
            string FSDURATION = "";
            string FNREPLAY = "";
            string FSPLAY_TYPE = "";
            string FNDUR = "";
            string FSETIME = "";
            string FNSEQNO = "";
            string FSPROGNAME = "";
            string FSMEMO = "";
            string FSMEMO1 = "";
            string FSSIGNAL = "";
            string FNDEF_MIN = "";
            string FNDEF_SEC = "";


            if (parameters != "")
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(parameters);
                XmlNode xmlNode = xmldoc.FirstChild;
                for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                {
                    if (xmlNode.ChildNodes[i].Name == "FSPROG_ID")
                        FSPROGID = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "FNEPISODE")
                        FNEPISODE = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "FDDATE")
                        FDDATE = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "FSBTIME")
                        FSBTIME = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "FSETIME")
                        FSETIME = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "FSPROGNAME")
                        FSPROGNAME = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "FNDUR")
                        FNDUR = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "FNSEG")
                        FNSEG = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "FSPLAY_TYPE")
                        FSPLAY_TYPE = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "FSCHANNEL_ID")
                        FSCHANNEL_ID = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "FSMEMO")
                    {
                        if (xmlNode.ChildNodes[i].FirstChild == null)
                            FSMEMO = "";
                        else
                            FSMEMO = xmlNode.ChildNodes[i].FirstChild.Value;
                    }
                    if (xmlNode.ChildNodes[i].Name == "FSMEMO1")
                    {
                        if (xmlNode.ChildNodes[i].FirstChild == null)
                            FSMEMO1 = "";
                        else
                            FSMEMO1 = xmlNode.ChildNodes[i].FirstChild.Value;
                    }
                    if (xmlNode.ChildNodes[i].Name == "FSSIGNAL")
                        FSSIGNAL = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "FNDEF_MIN")
                        FNDEF_MIN = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "FNDEF_SEC")
                        FNDEF_SEC = xmlNode.ChildNodes[i].FirstChild.Value;

                }
            }

            PTS_MAM3.Web.DataSource.WSMAMFunctions obj = new PTS_MAM3.Web.DataSource.WSMAMFunctions();
            string ReturnVideoID = "";
            ReturnVideoID = obj.QueryVideoID_PROG(FSPROGID, FNEPISODE, FSCHANNEL_ID);
            if (ReturnVideoID == "        ")
            {
                return "";
            }

            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            //SqlConnection myconnection = new SqlConnection(sqlConnStr);
            SqlConnection myconnection1 = new SqlConnection(sqlConnStr);
            SqlConnection myconnection2 = new SqlConnection(sqlConnStr);
            SqlCommand mycommand1 = new SqlCommand();
            mycommand1.Connection = myconnection1;
            SqlDataReader myReader1;
            SqlCommand mycom = new SqlCommand();
            mycom.Connection = myconnection1;
            SqlCommand mycom1 = new SqlCommand();
            mycom1.Connection = myconnection2;
            myconnection1.Open();
            myconnection2.Open();
            string Sqltxt1;
            Sqltxt1 = "select FnSeqno,FSprog_ID,FsProg_Name from tbpgm_bank_Detail where fsprog_id='" + FSPROGID + "'";
            Sqltxt1 = Sqltxt1 + " and FSBEG_TIME='" + FSBTIME + "' and FSCHANNEL_ID='" + FSCHANNEL_ID + "'";
            mycom.CommandText = Sqltxt1;
            myReader1 = mycom.ExecuteReader();
            if (myReader1.HasRows == true)
            {
                myReader1.Read();
                //while (myReader1.Read())
                //{
                if (myReader1.IsDBNull(0) != true)
                {
                    FNSEQNO = myReader1.GetInt32(0).ToString();
                    //FSPROGNAME = myReader1.GetString(2);

                    string Sqltxt2 = "update tbpgm_bank_Detail set fdEND_date='" + FDDATE + "', FSPROG_NAME='" + FSPROGNAME + "' where fsprog_id='" + FSPROGID + "'";
                    Sqltxt2 = Sqltxt2 + " and Fnseqno=" + FNSEQNO;
                    mycom1.CommandText = Sqltxt2;
                    mycom1.ExecuteNonQuery();
                }
                else
                {
                    FNSEQNO = "1";
                }
                //}
            }
            else
            {
                string Sqltxt2 = "select max(fnseqno) from tbpgm_bank_Detail where fsprog_id='" + FSPROGID + "'";
                SqlDataReader myReader2;
                mycom1.CommandText = Sqltxt2;
                FNSEQNO = "";
                //FSPROGNAME = "";
                myReader2 = mycom1.ExecuteReader();
                myReader2.Read();
                if (myReader2.IsDBNull(0) != true)
                {
                    FNSEQNO = (myReader2.GetInt32(0) + 1).ToString();
                }
                else
                {
                    FNSEQNO = "1";
                }
                myReader2.Close();
                Sqltxt1 = "insert into tbpgm_bank_detail(FSPROG_ID, FNSEQNO, FSPROG_NAME, FSPROG_NAME_ENG, FDBEG_DATE, FDEND_DATE,";
                Sqltxt1 = Sqltxt1 + "FSBEG_TIME, FSEND_TIME, FSCHANNEL_ID, FNREPLAY,FSWEEK,FSPLAY_TYPE,FNSEG,FNDUR,FSCREATED_BY,FDCREATED_DATE) values(";
                Sqltxt1 = Sqltxt1 + "'" + FSPROGID + "',";
                Sqltxt1 = Sqltxt1 + FNSEQNO + ",";
                Sqltxt1 = Sqltxt1 + "'" + FSPROGNAME + "',";
                Sqltxt1 = Sqltxt1 + "'',";
                Sqltxt1 = Sqltxt1 + "'" + FDDATE + "',";
                Sqltxt1 = Sqltxt1 + "'" + FDDATE + "',";
                Sqltxt1 = Sqltxt1 + "'" + FSBTIME + "',";
                Sqltxt1 = Sqltxt1 + "'" + FSETIME + "',";
                Sqltxt1 = Sqltxt1 + "'" + FSCHANNEL_ID + "',";
                Sqltxt1 = Sqltxt1 + "0" + ",";
                Sqltxt1 = Sqltxt1 + "'" + "1111111" + "',";
                Sqltxt1 = Sqltxt1 + FSPLAY_TYPE + ","; //live
                Sqltxt1 = Sqltxt1 + FNSEG + "," + FNDUR + ",'',Getdate())"; //seg,FUR,userid,date
                mycom1.CommandText = Sqltxt1;
                mycom1.ExecuteNonQuery();

            }

            myReader1.Close();

            //寫入TBPGM_QUEUE
            Sqltxt1 = "select FnSeqno,FSprog_ID,FsProg_Name from TBPGM_QUEUE where fsprog_id='" + FSPROGID + "'";
            Sqltxt1 = Sqltxt1 + " and FNSEQNO=" + FNSEQNO + " and FSCHANNEL_ID='" + FSCHANNEL_ID + "' and fddate='" + FDDATE + "'";
            mycom.CommandText = Sqltxt1;
            myReader1 = mycom.ExecuteReader();
            if (myReader1.HasRows == false)
            {  // 開始寫入TBPGM_QUEUE


                string Sqltxt2 = "insert into tbpgm_queue(FSPROG_ID,FNSEQNO,FDDATE,FSCHANNEL_ID,FSBEG_TIME,FSEND_TIME,";
                Sqltxt2 = Sqltxt2 + " FNREPLAY,FNEPISODE,FSTAPENO,FSPROG_NAME,FNLIVE,FSSIGNAL,FNDEP,FNONOUT,FSMEMO,FSMEMO1,";
                Sqltxt2 = Sqltxt2 + " FSCREATED_BY,FDCREATED_DATE,FNDEF_MIN,FNDEF_SEC) ";
                Sqltxt2 = Sqltxt2 + " values ('" + FSPROGID + "'," + FNSEQNO + ",'" + FDDATE + "','" + FSCHANNEL_ID + "','";
                Sqltxt2 = Sqltxt2 + FSBTIME + "','" + FSETIME + "',0," + FNEPISODE + ",'','" + FSPROGNAME + "',";
                Sqltxt2 = Sqltxt2 + FSPLAY_TYPE + ",'" + FSSIGNAL + "',0,0,'" + FSMEMO + "','" + FSMEMO1 + "','',getdate(),'" + FNDEF_MIN + "','" + FNDEF_SEC + "')";
                mycom1.CommandText = Sqltxt2;
                mycom1.ExecuteNonQuery();
            }

            myReader1.Close();
            return FNSEQNO;

        }

        //查詢節目表未到帶清單
        [WebMethod()]
        public string Do_Query_TBLOG_VIDEO_BY_ID_EPISODE(string strSQL, string parameters = null)
        {
            //strSQL為查詢Store Procesure Name
            //parameters為XML內容(DB_Function_Sample.txt)

            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;

            StringBuilder sb = new StringBuilder();
            DataTable dt = new DataTable();
            //SqlCommand sqlcmd = new SqlCommand(strSQL, connection);
            sb.AppendLine("<Datas>");
            //開始分析傳入的XML參數
            if (parameters != "")
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(parameters);
                XmlNode xmlNode = xmldoc.FirstChild;

                //逐一將參數分析出來
                //sqlcmd.CommandText = "SP_Q_TBLOG_VIDEO_BY_ID_EPISODE";
                for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                {
                    SqlConnection connection = new SqlConnection(sqlConnStr);
                    SqlDataAdapter da = new SqlDataAdapter(strSQL, connection);

                    //sqlcmd.Parameters.Clear();
                    string _FSID = "";
                    string _Episode = "";
                    string _FSARC_TYPE = "";
                    da.SelectCommand.Parameters.Clear();
                    for (int j = 0; j <= xmlNode.ChildNodes[i].ChildNodes.Count - 1; j++)
                    {
                        if (xmlNode.ChildNodes[i].ChildNodes[j].FirstChild == null || xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value == "")
                        {
                            SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, "");
                            da.SelectCommand.Parameters.Add(para);
                        }
                        else
                        {
                            SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value);
                            da.SelectCommand.Parameters.Add(para);
                        }
                        if (xmlNode.ChildNodes[i].ChildNodes[j].Name == "FSID")
                            _FSID = xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value;
                        if (xmlNode.ChildNodes[i].ChildNodes[j].Name == "FNEPISODE")
                            _Episode = xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value;
                        if (xmlNode.ChildNodes[i].ChildNodes[j].Name == "FSARC_TYPE")
                            _FSARC_TYPE = xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value;

                        //SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value);
                        //da.SelectCommand.Parameters.Add(para);
                    }
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;

                    //依照傳入的參數FSID,FNEPISODE,FSARC_TYPE 去呼叫SP_Q_TBLOG_VIDEO_BY_ID_EPISODE_ARCTPYE
                    try
                    {
                        if (connection.State == System.Data.ConnectionState.Closed)
                        {
                            connection.Open();
                        }
                        dt.Clear();
                        da.Fill(dt);

                        //查詢完畢,將查出資料帶到dt中,如果有資料則代表TBLOG_VIDEO中已有此資料,代表已經做過送帶轉檔,但並未檢視是否為轉檔完成
                        if (dt.Rows.Count > 0)
                        {
                            //欄位包含:FSID,FNEPISODE,FSARC_TYPE,FSFILE_NO,FCLOW_RES 
                            for (int m = 0; m <= dt.Rows.Count - 1; m++)
                            {
                                sb.AppendLine("<Data>");
                                for (int n = 0; n <= dt.Columns.Count - 1; n++)
                                {
                                    if (Convert.IsDBNull(dt.Rows[m][n]))
                                    {
                                        sb.AppendLine("<" + dt.Columns[n].ColumnName + "></" + dt.Columns[n].ColumnName + ">");
                                    }
                                    else
                                    {
                                        sb.AppendLine("<" + dt.Columns[n].ColumnName + ">" + ReplaceXML(dt.Rows[m][n].ToString()) + "</" + dt.Columns[n].ColumnName + ">");
                                    }
                                }
                                sb.AppendLine("</Data>");
                            }

                        }
                        else  //如果都沒有資料則代表TBLOG_VIDEO中沒有此節目資料,則需要做查詢片庫,看此節目是否在片庫
                        {
                            SqlConnection connection1 = new SqlConnection(sqlConnStr);
                            SqlDataAdapter da1 = new SqlDataAdapter("SP_Q_PTSTAPE_BY_PROGID_EPNO", connection1);
                            da1.SelectCommand.Parameters.Clear();
                            SqlParameter para = new SqlParameter("@progid", _FSID);
                            da1.SelectCommand.Parameters.Add(para);
                            SqlParameter para1 = new SqlParameter("@epno", _Episode);
                            da1.SelectCommand.Parameters.Add(para1);
                            da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                            DataTable dt1 = new DataTable();
                            dt1.Clear();
                            da1.Fill(dt1);

                            sb.AppendLine("<Data>");
                            sb.AppendLine("<FSID>" + _FSID + "</FSID>");
                            sb.AppendLine("<FNEPISODE>" + _Episode + "</FNEPISODE>");

                            sb.AppendLine("<FSARC_TYPE>" + _FSARC_TYPE + "</FSARC_TYPE>");
                            sb.AppendLine("<FSFILE_NO>" + "" + "</FSFILE_NO>");

                            if (dt1.Rows.Count > 0)
                            {
                                sb.AppendLine("<FCLOW_RES>" + "存放位置:" + ReplaceXML(dt1.Rows[0][0].ToString()) + "</FCLOW_RES>");
                            }
                            else
                            {
                                sb.AppendLine("<FCLOW_RES>" + "" + "</FCLOW_RES>");
                            }


                            sb.AppendLine("</Data>");
                            connection1.Close();
                            connection1.Dispose();
                        }

                    }
                    catch (Exception ex)
                    {
                        StringBuilder sbError = new StringBuilder();
                        sbError.AppendLine("<Errors>" + ex.Message + "</Errors>");
                        return sbError.ToString();
                    }
                    finally
                    {
                        connection.Close();
                        connection.Dispose();
                        GC.Collect();
                    }
                }
            }
            sb.AppendLine("</Datas>");
            //參數剖析

            return sb.ToString();

        }

        //新增
        [WebMethod()]
        public bool Do_Insert(string strSQL, string parameters)
        {
            //strSQL為查詢Store Procesure Name

            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            SqlTransaction tran = null;
            SqlCommand sqlcmd = new SqlCommand(strSQL, connection);


            //參數剖析
            if (parameters != "")
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(parameters);
                XmlNode xmlNode = xmldoc.FirstChild;
                for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                {
                    if (xmlNode.ChildNodes[i].FirstChild == null || xmlNode.ChildNodes[i].FirstChild.Value == "")
                    {
                        SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, "");
                        sqlcmd.Parameters.Add(para);
                    }
                    else
                    {
                        SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, xmlNode.ChildNodes[i].FirstChild.Value);
                        sqlcmd.Parameters.Add(para);
                    }

                    //SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, xmlNode.ChildNodes[i].FirstChild.Value);
                    //sqlcmd.Parameters.Add(para);
                }
            }

            sqlcmd.CommandType = CommandType.StoredProcedure;

            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                tran = connection.BeginTransaction();
                sqlcmd.Transaction = tran;
                if (sqlcmd.ExecuteNonQuery() > 0)
                {
                    tran.Commit();
                    return true;
                }
                else
                {
                    //不須知道異動筆數
                    tran.Commit();
                    //tran.Rollback();
                    return true;
                }

            }
            catch (Exception ex)
            {
                tran.Rollback();
                return false;
            }
            finally
            {

                connection.Close();
                connection.Dispose();
                sqlcmd.Dispose();
                GC.Collect();

            }
        }


        [WebMethod()]
        public string Do_Insert_TBPGM_PROMO_PLAN(string getMaxNO, string getMaxNOparameters, string strSQL, string parameters)
        {
            //strSQL為查詢Store Procesure Name
            string maxnoXML;
            maxnoXML = Do_Query(getMaxNO, getMaxNOparameters);
            string maxNO = "";
            byte[] byteArray = Encoding.Unicode.GetBytes(maxnoXML);
            //StringBuilder output = new StringBuilder();
            // Create an XmlReader
            XDocument doc = XDocument.Load(new MemoryStream(byteArray));
            var ltox = from str in doc.Elements("Datas").Elements("Data")
                       select str;
            foreach (XElement elem in ltox)
            {
                maxNO = elem.Element("MAXFNNO").Value.ToString();
            }


            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            SqlTransaction tran = null;
            SqlCommand sqlcmd = new SqlCommand(strSQL, connection);


            //參數剖析
            if (parameters != "")
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(parameters);
                XmlNode xmlNode = xmldoc.FirstChild;

                SqlParameter para1 = new SqlParameter("@FNNO", maxNO);
                sqlcmd.Parameters.Add(para1);

                for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                {
                    if (xmlNode.ChildNodes[i].FirstChild == null || xmlNode.ChildNodes[i].FirstChild.Value == "")
                    {
                        SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, "");
                        sqlcmd.Parameters.Add(para);
                    }
                    else
                    {
                        SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, xmlNode.ChildNodes[i].FirstChild.Value);
                        sqlcmd.Parameters.Add(para);
                    }

                    //SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, xmlNode.ChildNodes[i].FirstChild.Value);
                    //sqlcmd.Parameters.Add(para);
                }
            }

            sqlcmd.CommandType = CommandType.StoredProcedure;

            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                tran = connection.BeginTransaction();
                sqlcmd.Transaction = tran;
                if (sqlcmd.ExecuteNonQuery() > 0)
                {
                    tran.Commit();
                    return maxNO;
                }
                else
                {
                    //不須知道異動筆數
                    tran.Commit();
                    //tran.Rollback();
                    return maxNO;
                }

            }
            catch (Exception ex)
            {
                tran.Rollback();
                return "Error";
            }
            finally
            {

                connection.Close();
                connection.Dispose();
                sqlcmd.Dispose();
                GC.Collect();

            }
        }

        //新增
        [WebMethod()]
        public bool Do_Insert_TBPGM_LOUTH_KEY(string strSQL, string parameters)
        {

            //strSQL為查詢Store Procesure Name
            strSQL = "SP_I_TBPGM_LOUTH_KEY";
            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            SqlTransaction tran = null;
            SqlCommand sqlcmd = new SqlCommand(strSQL, connection);

            sqlcmd.CommandType = CommandType.StoredProcedure;

            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                tran = connection.BeginTransaction();
                sqlcmd.Transaction = tran;

                //參數剖析


                //sqlcmd.CommandText = "SP_D_TBPGM_LOUTH_KEY";
                //    sqlcmd.Parameters.Clear();
                //    if (sqlcmd.ExecuteNonQuery() == 0)
                //    {
                //        //tran.Rollback();
                //        //return false;
                //    }

                if (parameters != "")
                {

                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(parameters);
                    XmlNode xmlNode = xmldoc.FirstChild;
                    sqlcmd.CommandText = "SP_I_TBPGM_LOUTH_KEY";

                    for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                    {
                        sqlcmd.Parameters.Clear();

                        for (int j = 0; j <= xmlNode.ChildNodes[i].ChildNodes.Count - 1; j++)
                        {
                            if (xmlNode.ChildNodes[i].ChildNodes[j].FirstChild == null || xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value == "")
                            {
                                SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, "");
                                sqlcmd.Parameters.Add(para);
                            }
                            else
                            {
                                SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value);
                                sqlcmd.Parameters.Add(para);
                            }

                        }
                        if (sqlcmd.ExecuteNonQuery() == 0)
                        {
                            tran.Rollback();
                            return false;
                        }
                    }
                }
                tran.Commit();
                return true;
            }
            catch
            {
                //ex.Message
                tran.Rollback();
                return false;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                sqlcmd.Dispose();
                GC.Collect();
            }
        }

        //修改托播單狀態
        [WebMethod()]
        public bool Do_Update_Promo_Booling(string FNPROMO_BOOKING_NO, string FCSTATUS)
        {
            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            SqlTransaction tran = null;
            SqlCommand sqlcmd = new SqlCommand("SP_U_TBPGM_PROMO_BOOKING", connection);
            sqlcmd.CommandType = CommandType.StoredProcedure;

            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                tran = connection.BeginTransaction();
                sqlcmd.Transaction = tran;

                //參數剖析

                sqlcmd.CommandText = "SP_U_TBPGM_PROMO_BOOKING";
                sqlcmd.Parameters.Clear();
                SqlParameter para = new SqlParameter("@FNPROMO_BOOKING_NO", FNPROMO_BOOKING_NO);
                sqlcmd.Parameters.Add(para);
                SqlParameter para1 = new SqlParameter("@FCSTATUS", FCSTATUS);
                sqlcmd.Parameters.Add(para1);
                if (sqlcmd.ExecuteNonQuery() == 0)
                {
                    tran.Rollback();
                    return false;
                }
                tran.Commit();
                return true;
            }
            catch
            {
                //ex.Message
                tran.Rollback();
                return false;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                sqlcmd.Dispose();
                GC.Collect();
            }
        }

        //新增
        [WebMethod()]
        public bool Do_Insert_List(string DelStoreProcesure, string DelPara, string InsStoreProcesure, string InsPara)
        {
            //strSQL為查詢Store Procesure Name

            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            SqlTransaction tran = null;
            SqlCommand sqlcmd = new SqlCommand(DelStoreProcesure, connection);

            sqlcmd.CommandType = CommandType.StoredProcedure;

            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                tran = connection.BeginTransaction();
                sqlcmd.Transaction = tran;

                //參數剖析
                if (DelStoreProcesure != "")
                {

                    sqlcmd.CommandText = DelStoreProcesure;
                    sqlcmd.Parameters.Clear();

                    if (DelPara != "")
                    {
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(DelPara);
                        XmlNode xmlNode = xmldoc.FirstChild;
                        sqlcmd.CommandText = DelStoreProcesure;
                        sqlcmd.Parameters.Clear();
                        for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                        {
                            if (xmlNode.ChildNodes[i].FirstChild == null || xmlNode.ChildNodes[i].FirstChild.Value == "")
                            {
                                SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, "");
                                sqlcmd.Parameters.Add(para);
                            }
                            else
                            {
                                SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, xmlNode.ChildNodes[i].FirstChild.Value);
                                sqlcmd.Parameters.Add(para);
                            }
                        }
                        if (sqlcmd.ExecuteNonQuery() == 0)
                        {
                            //tran.Rollback();
                            //return false;
                        }
                    }
                }

                if (InsPara != "")
                {

                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(InsPara);
                    XmlNode xmlNode = xmldoc.FirstChild;
                    sqlcmd.CommandText = InsStoreProcesure;

                    for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                    {
                        sqlcmd.Parameters.Clear();
                        for (int j = 0; j <= xmlNode.ChildNodes[i].ChildNodes.Count - 1; j++)
                        {
                            if (xmlNode.ChildNodes[i].ChildNodes[j].FirstChild == null || xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value == "")
                            {
                                SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, "");
                                sqlcmd.Parameters.Add(para);
                            }
                            else
                            {
                                SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value);
                                sqlcmd.Parameters.Add(para);
                            }
                        }
                        if (sqlcmd.ExecuteNonQuery() == 0)
                        {
                            tran.Rollback();
                            return false;
                        }
                    }
                }
                tran.Commit();
                return true;
            }
            catch
            {
                //ex.Message
                tran.Rollback();
                return false;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                sqlcmd.Dispose();
                GC.Collect();
            }
        }

        //新增託播單傳回託播單號
        [WebMethod()]
        public string Do_Insert_Promo_Booking(string strSQL, string parameters, string PromoBookingparameters, string PromoParameter, string PromoZoneParameter, string PromoSCHEDULEDParameter)
        {

            //strSQL為查詢Store Procesure Name
            strSQL = "SP_Q_TBPGM_PROMO_BOOKING_GET_MAX_ID";
            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            SqlTransaction tran = null;
            SqlCommand sqlcmd = new SqlCommand(strSQL, connection);

            SqlDataAdapter da = new SqlDataAdapter(strSQL, connection);

            DataTable dt = new DataTable();
            sqlcmd.CommandType = CommandType.StoredProcedure;

            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                sqlcmd.CommandText = "SP_Q_TBPGM_PROMO_BOOKING_GET_MAX_ID";
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.Fill(dt);

                string MAXBookingID;
                MAXBookingID = dt.Rows[0][0].ToString();

                tran = connection.BeginTransaction();
                sqlcmd.Transaction = tran;

                //參數剖析
                if (PromoBookingparameters != "")
                {

                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(PromoBookingparameters);
                    XmlNode xmlNode = xmldoc.FirstChild;
                    sqlcmd.CommandText = "SP_I_TBPGM_PROMO_BOOKING";
                    SqlParameter para1 = new SqlParameter("@FNPROMO_BOOKING_NO", MAXBookingID);
                    sqlcmd.Parameters.Clear();
                    sqlcmd.Parameters.Add(para1);
                    for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                    {
                        if (xmlNode.ChildNodes[i].FirstChild == null || xmlNode.ChildNodes[i].FirstChild.Value == "")
                        {
                            SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, "");
                            sqlcmd.Parameters.Add(para);
                        }
                        else
                        {
                            SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, xmlNode.ChildNodes[i].FirstChild.Value);
                            sqlcmd.Parameters.Add(para);
                        }

                        //SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, xmlNode.ChildNodes[i].FirstChild.Value);
                        //sqlcmd.Parameters.Add(para);
                    }
                    if (sqlcmd.ExecuteNonQuery() == 0)
                    {
                        tran.Rollback();
                        return "";
                    }
                }

                //20120420 BY Mike
                if (PromoSCHEDULEDParameter != "") //建立 SP_I_TBPGM_PROMO_BOOKING_SETTING
                {

                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(PromoSCHEDULEDParameter);
                    XmlNode xmlNode = xmldoc.FirstChild;
                    sqlcmd.CommandText = "SP_I_TBPGM_PROMO_BOOKING_SETTING";
                    SqlParameter para1 = new SqlParameter("@FNPROMO_BOOKING_NO", MAXBookingID);
                    sqlcmd.Parameters.Clear();
                    sqlcmd.Parameters.Add(para1);
                    for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                    {
                        if (xmlNode.ChildNodes[i].FirstChild == null || xmlNode.ChildNodes[i].FirstChild.Value == "")
                        {
                            SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, "");
                            sqlcmd.Parameters.Add(para);
                        }
                        else
                        {
                            SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, xmlNode.ChildNodes[i].FirstChild.Value);
                            sqlcmd.Parameters.Add(para);
                        }

                    }
                    if (sqlcmd.ExecuteNonQuery() == 0)
                    {
                        tran.Rollback();
                        return "";
                    }
                }


                //(string strSQL, string parameters, string PromoParameter, string PromoZoneParameter, string PromoSCHEDULEDParameter)
                //參數剖析
                if (PromoParameter != "") //只有一筆
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(PromoParameter);
                    XmlNode xmlNode = xmldoc.FirstChild;
                    sqlcmd.CommandText = "SP_I_TBPGM_PROMO";
                    //sqlcmd.CommandText = "SP_I_TBPGM_PROMO_BOOKING";
                    //sqlcmd.CommandText = "SP_I_TBPGM_PROMO_EFFECTIVE_ZONE";
                    //sqlcmd.CommandText = "SP_I_TBPGM_PROMO_SCHEDULED";
                    sqlcmd.Parameters.Clear();
                    for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                    {
                        if (xmlNode.ChildNodes[i].FirstChild == null || xmlNode.ChildNodes[i].FirstChild.Value == "")
                        {
                            SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, "");
                            sqlcmd.Parameters.Add(para);
                        }
                        else
                        {
                            SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, xmlNode.ChildNodes[i].FirstChild.Value);
                            sqlcmd.Parameters.Add(para);
                        }

                        //SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, xmlNode.ChildNodes[i].FirstChild.Value);
                        //sqlcmd.Parameters.Add(para);
                    }
                    if (sqlcmd.ExecuteNonQuery() == 0)
                    {
                        tran.Rollback();
                        return "";
                    }

                }

                if (PromoZoneParameter != "")
                {

                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(PromoZoneParameter);
                    XmlNode xmlNode = xmldoc.FirstChild;
                    sqlcmd.CommandText = "SP_I_TBPGM_PROMO_EFFECTIVE_ZONE";
                    for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                    {
                        sqlcmd.Parameters.Clear();

                        SqlParameter para1 = new SqlParameter("@FNPROMO_BOOKING_NO", MAXBookingID);
                        sqlcmd.Parameters.Add(para1);

                        for (int j = 0; j <= xmlNode.ChildNodes[i].ChildNodes.Count - 1; j++)
                        {
                            if (xmlNode.ChildNodes[i].ChildNodes[j].FirstChild == null || xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value == "")
                            {
                                SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, "");
                                sqlcmd.Parameters.Add(para);
                            }
                            else
                            {
                                SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value);
                                sqlcmd.Parameters.Add(para);
                            }

                            //SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value);
                            //sqlcmd.Parameters.Add(para);
                        }
                        if (sqlcmd.ExecuteNonQuery() == 0)
                        {
                            tran.Rollback();
                            return "";
                        }
                    }
                }
                //20120420 BY Mike
                //if (PromoSCHEDULEDParameter != "")
                //{
                //    XmlDocument xmldoc = new XmlDocument();
                //    xmldoc.LoadXml(PromoSCHEDULEDParameter);
                //    XmlNode xmlNode = xmldoc.FirstChild;
                //    sqlcmd.CommandText = "SP_I_TBPGM_PROMO_SCHEDULED";
                //    for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                //    {
                //        sqlcmd.Parameters.Clear();

                //        SqlParameter para1 = new SqlParameter("@FNPROMO_BOOKING_NO", MAXBookingID);
                //        sqlcmd.Parameters.Add(para1);

                //        for (int j = 0; j <= xmlNode.ChildNodes[i].ChildNodes.Count - 1; j++)
                //        {
                //            if (xmlNode.ChildNodes[i].ChildNodes[j].FirstChild == null || xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value == "")
                //            {
                //                SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, "");
                //                sqlcmd.Parameters.Add(para);
                //            }
                //            else
                //            {
                //                SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value);
                //                sqlcmd.Parameters.Add(para);
                //            }

                //            //SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value);
                //            //sqlcmd.Parameters.Add(para);
                //        }
                //        if (sqlcmd.ExecuteNonQuery() == 0)
                //        {
                //            tran.Rollback();
                //            return "";
                //        }
                //    }
                //}

                tran.Commit();
                return MAXBookingID;


            }
            catch (Exception ex)
            {
                //ex.Message
                tran.Rollback();
                return "";
            }
            finally
            {

                connection.Close();
                connection.Dispose();
                sqlcmd.Dispose();
                GC.Collect();

            }
        }

        //修改託播單
        [WebMethod()]
        public bool Do_Agree_Promo_Booking(string BookingID, string PromoZoneParameter, string PromoSCHEDULEDParameter, string FSMEMO)
        {

            //strSQL為查詢Store Procesure Name
            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            SqlTransaction tran = null;
            SqlCommand sqlcmd = new SqlCommand("", connection);
            sqlcmd.CommandType = CommandType.StoredProcedure;

            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                tran = connection.BeginTransaction();
                sqlcmd.Transaction = tran;

                //參數剖析
                if (BookingID != "")
                {
                    sqlcmd.CommandText = "SP_U_TBPGM_PROMO_BOOKING";
                    SqlParameter para1 = new SqlParameter("@FNPROMO_BOOKING_NO", BookingID);
                    sqlcmd.Parameters.Clear();
                    sqlcmd.Parameters.Add(para1);
                    SqlParameter para2 = new SqlParameter("@FCSTATUS", "R");
                    //sqlcmd.Parameters.Clear();
                    sqlcmd.Parameters.Add(para2);

                    if (sqlcmd.ExecuteNonQuery() == 0)
                    {
                        tran.Rollback();
                        return false;
                    }

                    sqlcmd.CommandText = "SP_U_TBPGM_PROMO_BOOKING_MEMO";
                    SqlParameter paraM1 = new SqlParameter("@FNPROMO_BOOKING_NO", BookingID);
                    sqlcmd.Parameters.Clear();
                    sqlcmd.Parameters.Add(paraM1);
                    SqlParameter paraM2 = new SqlParameter("@FSMEMO", FSMEMO);
                    //sqlcmd.Parameters.Clear();
                    sqlcmd.Parameters.Add(paraM2);

                    if (sqlcmd.ExecuteNonQuery() == 0)
                    {
                        tran.Rollback();
                        return false;
                    }



                    sqlcmd.CommandText = "SP_D_TBPGM_PROMO_EFFECTIVE_ZONE";
                    SqlParameter para3 = new SqlParameter("@FNPROMO_BOOKING_NO", BookingID);
                    sqlcmd.Parameters.Clear();
                    sqlcmd.Parameters.Add(para3);

                    if (sqlcmd.ExecuteNonQuery() == 0)
                    {
                        //tran.Rollback();
                        //return false;
                    }

                }



                if (PromoZoneParameter != "")
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(PromoZoneParameter);
                    XmlNode xmlNode = xmldoc.FirstChild;
                    sqlcmd.CommandText = "SP_I_TBPGM_PROMO_EFFECTIVE_ZONE";
                    for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                    {
                        sqlcmd.Parameters.Clear();

                        SqlParameter para1 = new SqlParameter("@FNPROMO_BOOKING_NO", BookingID);
                        sqlcmd.Parameters.Add(para1);

                        for (int j = 0; j <= xmlNode.ChildNodes[i].ChildNodes.Count - 1; j++)
                        {
                            if (xmlNode.ChildNodes[i].ChildNodes[j].FirstChild == null || xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value == "")
                            {
                                SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, "");
                                sqlcmd.Parameters.Add(para);
                            }
                            else
                            {
                                SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value);
                                sqlcmd.Parameters.Add(para);
                            }

                            //SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value);
                            //sqlcmd.Parameters.Add(para);
                        }
                        if (sqlcmd.ExecuteNonQuery() == 0)
                        {
                            tran.Rollback();
                            return false;
                        }
                    }
                }

                if (PromoSCHEDULEDParameter != "")
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(PromoSCHEDULEDParameter);
                    XmlNode xmlNode = xmldoc.FirstChild;
                    sqlcmd.CommandText = "SP_U_TBPGM_PROMO_BOOKING_SETTING";

                    sqlcmd.Parameters.Clear();

                    SqlParameter para1 = new SqlParameter("@FNPROMO_BOOKING_NO", BookingID);
                    sqlcmd.Parameters.Add(para1);

                    for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                    {
                        if (xmlNode.ChildNodes[i].FirstChild == null || xmlNode.ChildNodes[i].FirstChild.Value == "")
                        {
                            SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, "");
                            sqlcmd.Parameters.Add(para);
                        }
                        else
                        {
                            SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, xmlNode.ChildNodes[i].FirstChild.Value);
                            sqlcmd.Parameters.Add(para);
                        }

                    }
                    if (sqlcmd.ExecuteNonQuery() == 0)
                    {
                        tran.Rollback();
                        return false;
                    }
                }


                tran.Commit();
                return true;


            }
            catch (Exception ex)
            {
                //ex.Message
                tran.Rollback();
                return false;
            }
            finally
            {

                connection.Close();
                connection.Dispose();
                sqlcmd.Dispose();
                GC.Collect();

            }
        }

        //刪除宣傳帶託播單
        [WebMethod()]
        public bool Do_Delete_Promo_Booking(string FNPROMO_BOOKING_NO)
        {

            //strSQL為查詢Store Procesure Name
            string strSQL;
            strSQL = "SP_D_TBPGM_QUEUE";
            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            SqlTransaction tran = null;
            SqlCommand sqlcmd = new SqlCommand(strSQL, connection);

            sqlcmd.CommandType = CommandType.StoredProcedure;

            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                tran = connection.BeginTransaction();
                sqlcmd.Transaction = tran;


                sqlcmd.CommandText = "SP_D_TBPGM_PROMO_BOOKING";
                sqlcmd.Parameters.Clear();
                SqlParameter para1 = new SqlParameter("@FNPROMO_BOOKING_NO", FNPROMO_BOOKING_NO);
                sqlcmd.Parameters.Add(para1);

                if (sqlcmd.ExecuteNonQuery() == 0)
                {
                    //tran.Rollback();
                    //return false;
                }
                sqlcmd.CommandText = "SP_D_TBPGM_PROMO_BOOKING_SETTING";
                sqlcmd.Parameters.Clear();
                SqlParameter para2 = new SqlParameter("@FNPROMO_BOOKING_NO", FNPROMO_BOOKING_NO);
                sqlcmd.Parameters.Add(para2);

                if (sqlcmd.ExecuteNonQuery() == 0)
                {
                    //tran.Rollback();
                    //return false;
                }
                sqlcmd.CommandText = "SP_D_TBPGM_PROMO_EFFECTIVE_ZONE";
                sqlcmd.Parameters.Clear();
                SqlParameter para3 = new SqlParameter("@FNPROMO_BOOKING_NO", FNPROMO_BOOKING_NO);
                sqlcmd.Parameters.Add(para3);

                if (sqlcmd.ExecuteNonQuery() == 0)
                {
                    //tran.Rollback();
                    //return false;
                }

                tran.Commit();
                return true;
            }
            catch
            {
                //ex.Message
                tran.Rollback();
                return false;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                sqlcmd.Dispose();
                GC.Collect();
            }
        }

        //新增節目表
        [WebMethod()]
        public bool Do_Insert_TBPGMQUEUE(string strSQL, string fddate, string fschannel_id, string parameters)
        {

            //strSQL為查詢Store Procesure Name
            strSQL = "SP_D_TBPGM_QUEUE";
            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            SqlTransaction tran = null;
            SqlCommand sqlcmd = new SqlCommand(strSQL, connection);

            sqlcmd.CommandType = CommandType.StoredProcedure;

            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                tran = connection.BeginTransaction();
                sqlcmd.Transaction = tran;

                //參數剖析
                if (fddate != "")
                {

                    sqlcmd.CommandText = "SP_D_TBPGM_QUEUE";
                    sqlcmd.Parameters.Clear();
                    SqlParameter para1 = new SqlParameter("@FDDATE", fddate);
                    sqlcmd.Parameters.Add(para1);
                    SqlParameter para2 = new SqlParameter("@FSCHANNEL_ID", fschannel_id);
                    sqlcmd.Parameters.Add(para2);
                    if (sqlcmd.ExecuteNonQuery() == 0)
                    {
                        //tran.Rollback();
                        //return false;
                    }
                }


                if (parameters != "")
                {

                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(parameters);
                    XmlNode xmlNode = xmldoc.FirstChild;
                    sqlcmd.CommandText = "SP_I_TBPGM_QUEUE";

                    for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                    {
                        sqlcmd.Parameters.Clear();

                        for (int j = 0; j <= xmlNode.ChildNodes[i].ChildNodes.Count - 1; j++)
                        {
                            if (xmlNode.ChildNodes[i].ChildNodes[j].FirstChild == null || xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value == "")
                            {
                                SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, "");
                                sqlcmd.Parameters.Add(para);
                            }
                            else
                            {
                                SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value);
                                sqlcmd.Parameters.Add(para);
                            }

                            //SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value);
                            //sqlcmd.Parameters.Add(para);
                        }
                        if (sqlcmd.ExecuteNonQuery() == 0)
                        {
                            tran.Rollback();
                            return false;
                        }
                    }
                }
                tran.Commit();
                return true;
            }
            catch
            {
                //ex.Message
                tran.Rollback();
                return false;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                sqlcmd.Dispose();
                GC.Collect();
            }
        }

        //新增播出運行表
        [WebMethod()]
        public bool Do_Insert_TBPGM_Combine_queue(string strSQL, string fddate, string fschannel_id, string CombineParameters, string ArrPromoParameters)
        {
            //strSQL為查詢Store Procesure Name
            strSQL = "SP_D_TBPGM_ARR_PROMO";
            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            SqlTransaction tran = null;
            SqlCommand sqlcmd = new SqlCommand(strSQL, connection);

            sqlcmd.CommandType = CommandType.StoredProcedure;

            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                tran = connection.BeginTransaction();
                sqlcmd.Transaction = tran;

                //參數剖析
                if (fddate != "")
                {
                    sqlcmd.CommandText = "SP_D_TBPGM_ARR_PROMO";
                    sqlcmd.Parameters.Clear();
                    SqlParameter para1 = new SqlParameter("@FDDATE", fddate);
                    sqlcmd.Parameters.Add(para1);
                    SqlParameter para2 = new SqlParameter("@FSCHANNEL_ID", fschannel_id);
                    sqlcmd.Parameters.Add(para2);
                    if (sqlcmd.ExecuteNonQuery() == 0)
                    {
                        //tran.Rollback();
                        //return false;
                    }
                }

                if (fddate != "")
                {
                    sqlcmd.CommandText = "SP_D_TBPGM_COMBINE_QUEUE";
                    sqlcmd.Parameters.Clear();
                    SqlParameter para1 = new SqlParameter("@FDDATE", fddate);
                    sqlcmd.Parameters.Add(para1);
                    SqlParameter para2 = new SqlParameter("@FSCHANNEL_ID", fschannel_id);
                    sqlcmd.Parameters.Add(para2);
                    if (sqlcmd.ExecuteNonQuery() == 0)
                    {
                    }
                }

                if (CombineParameters != "")
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(CombineParameters);
                    XmlNode xmlNode = xmldoc.FirstChild;
                    sqlcmd.CommandText = "SP_I_TBPGM_COMBINE_QUEUE";
                    for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                    {
                        sqlcmd.Parameters.Clear();
                        for (int j = 0; j <= xmlNode.ChildNodes[i].ChildNodes.Count - 1; j++)
                        {
                            if (xmlNode.ChildNodes[i].ChildNodes[j].FirstChild == null || xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value == "")
                            {
                                SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, "");
                                sqlcmd.Parameters.Add(para);
                            }
                            else
                            {
                                SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value);
                                sqlcmd.Parameters.Add(para);
                            }

                            //SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value);
                            //sqlcmd.Parameters.Add(para);
                        }
                        if (sqlcmd.ExecuteNonQuery() == 0)
                        {
                            tran.Rollback();
                            return false;
                        }
                    }
                }
                if (ArrPromoParameters != "")
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(ArrPromoParameters);
                    XmlNode xmlNode = xmldoc.FirstChild;
                    sqlcmd.CommandText = "SP_I_TBPGM_ARR_PROMO";
                    for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                    {
                        sqlcmd.Parameters.Clear();
                        for (int j = 0; j <= xmlNode.ChildNodes[i].ChildNodes.Count - 1; j++)
                        {
                            if (xmlNode.ChildNodes[i].ChildNodes[j].FirstChild == null || xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value == "")
                            {
                                SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, "");
                                sqlcmd.Parameters.Add(para);
                            }
                            else
                            {
                                SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value);
                                sqlcmd.Parameters.Add(para);
                            }

                            //SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value);
                            //sqlcmd.Parameters.Add(para);
                        }
                        if (sqlcmd.ExecuteNonQuery() == 0)
                        {
                            tran.Rollback();
                            return false;
                        }
                    }
                }
                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                //ex.Message;
                //ex.Message
                tran.Rollback();
                return false;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                sqlcmd.Dispose();
                GC.Collect();
            }
        }

        //修改ARR_PROMO 與TBPGMPROMO的長度資料
        [WebMethod()]
        public bool Do_Update_ARR_PROMO_DUR(string strSQL, string fddate, string fschannel_id)
        {
            //strSQL為查詢Store Procesure Name
            //strSQL = "SP_U_TBPGM_ARR_PROMO_DUR";
            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            SqlTransaction tran = null;
            SqlCommand sqlcmd = new SqlCommand(strSQL, connection);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            SqlCommand sqlcmd1 = new SqlCommand(strSQL, connection);
            sqlcmd1.CommandType = CommandType.StoredProcedure;
            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                tran = connection.BeginTransaction();
                sqlcmd.Transaction = tran;
                sqlcmd1.Transaction = tran;
                if (strSQL != "")
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(strSQL);
                    XmlNode xmlNode = xmldoc.FirstChild;
                    sqlcmd.CommandText = "SP_U_TBPGM_PROMO_DUR";
                    sqlcmd1.CommandText = "SP_U_TBPGM_ARR_PROMO_DUR";
                    for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                    {
                        sqlcmd.Parameters.Clear();
                        sqlcmd1.Parameters.Clear();
                        for (int j = 0; j <= xmlNode.ChildNodes[i].ChildNodes.Count - 1; j++)
                        {
                            if (xmlNode.ChildNodes[i].ChildNodes[j].FirstChild == null || xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value == "")
                            {
                                SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, "");
                                sqlcmd.Parameters.Add(para);
                                SqlParameter para1 = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, "");
                                sqlcmd1.Parameters.Add(para1);
                            }
                            else
                            {
                                SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value);
                                sqlcmd.Parameters.Add(para);
                                SqlParameter para1 = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value);
                                sqlcmd1.Parameters.Add(para1);
                            }

                            //SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value);
                            //sqlcmd.Parameters.Add(para);82
                        }
                        SqlParameter para2 = new SqlParameter("@FDDATE", fddate);
                        sqlcmd1.Parameters.Add(para2);
                        SqlParameter para3 = new SqlParameter("@FSCHANNEL_ID", fschannel_id);
                        sqlcmd1.Parameters.Add(para3);
                        if (sqlcmd.ExecuteNonQuery() == 0)
                        {
                            tran.Rollback();
                            return false;
                        }
                        if (sqlcmd1.ExecuteNonQuery() == 0)
                        {
                            tran.Rollback();
                            return false;
                        }
                    }
                }
                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                //ex.Message;
                //ex.Message
                tran.Rollback();
                return false;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                sqlcmd.Dispose();
                GC.Collect();
            }
        }

        //查詢片庫需求調帶清單)
        [WebMethod()]
        public string GET_Queue_Zone_NO_Tape_List(string Bdate, string Edate, string fschannel_id, string UserCHtName, string Channel_name)
        {
            //strSQL為查詢Store Procesure Name
            //parameters為XML內容(DB_Function_Sample.txt)

            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection1 = new SqlConnection(sqlConnStr);
            StringBuilder sb = new StringBuilder();


            SqlTransaction tran = null;
            SqlCommand sqlcmd = new SqlCommand("SP_Q_GET_QUEUE_ZONE_NO_TAPE_LIST", connection1);
            sqlcmd.CommandType = CommandType.StoredProcedure;

            try
            {
                if (connection1.State == System.Data.ConnectionState.Closed)
                {
                    connection1.Open();
                }

            }
            catch
            {
                return "Error";
            }

            SqlConnection connection = new SqlConnection(sqlConnStr);
            Guid g;
            g = Guid.NewGuid();
            //參數剖析
            SqlDataAdapter daQUEUE_ZONE_NO_TAPE_LIST = new SqlDataAdapter("SP_Q_GET_QUEUE_ZONE_NO_TAPE_LIST", connection);
            DataTable dtQUEUE_ZONE_NO_TAPE_LIST = new DataTable();
            SqlParameter paraQIT1 = new SqlParameter("@FSCHANNEL_ID", fschannel_id);
            SqlParameter paraQIT2 = new SqlParameter("@FDBDATE", Bdate);
            SqlParameter paraQIT3 = new SqlParameter("@FDEDATE", Edate);

            daQUEUE_ZONE_NO_TAPE_LIST.SelectCommand.Parameters.Add(paraQIT1);
            daQUEUE_ZONE_NO_TAPE_LIST.SelectCommand.Parameters.Add(paraQIT2);
            daQUEUE_ZONE_NO_TAPE_LIST.SelectCommand.Parameters.Add(paraQIT3);
            daQUEUE_ZONE_NO_TAPE_LIST.SelectCommand.CommandType = CommandType.StoredProcedure;
            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                //先查詢QUEUE&Arrpromo的資料
                daQUEUE_ZONE_NO_TAPE_LIST.Fill(dtQUEUE_ZONE_NO_TAPE_LIST);

                if (dtQUEUE_ZONE_NO_TAPE_LIST.Rows.Count > 0) //有片庫需求調帶清單資料
                {

                    sqlcmd.CommandText = "SP_I_RPT_TBPGM_QUEUE_ZONE_NO_TAPE_LIST_01"; //將資料寫入片庫需求調帶清單報表

                    tran = connection1.BeginTransaction();
                    sqlcmd.Transaction = tran;
                    #region 逐一將資料寫入報表 RPT_TBPGM_QUEUE_ZONE_NO_TAPE_LIST_01
                    for (int i = 0; i <= dtQUEUE_ZONE_NO_TAPE_LIST.Rows.Count - 1; i++)
                    {
                        sqlcmd.Parameters.Clear();
                        SqlParameter para11 = new SqlParameter("@QUERY_KEY", g.ToString());
                        sqlcmd.Parameters.Add(para11);
                        Guid g1;
                        g1 = Guid.NewGuid();
                        SqlParameter para12 = new SqlParameter("@QUERY_KEY_SUBID", g1.ToString());
                        sqlcmd.Parameters.Add(para12);

                        SqlParameter para13 = new SqlParameter("@QUERY_BY", UserCHtName);
                        sqlcmd.Parameters.Add(para13);

                        SqlParameter para14 = new SqlParameter("@節目代碼", dtQUEUE_ZONE_NO_TAPE_LIST.Rows[i]["FSPROG_ID"].ToString());
                        sqlcmd.Parameters.Add(para14);

                        SqlParameter para15 = new SqlParameter("@分類號", dtQUEUE_ZONE_NO_TAPE_LIST.Rows[i]["TIndex"].ToString());
                        sqlcmd.Parameters.Add(para15);

                        SqlParameter para16 = new SqlParameter("@節目名稱", dtQUEUE_ZONE_NO_TAPE_LIST.Rows[i]["FSPROG_NAME"].ToString());
                        sqlcmd.Parameters.Add(para16);

                        SqlParameter para17 = new SqlParameter("@子集", dtQUEUE_ZONE_NO_TAPE_LIST.Rows[i]["EPISODE"].ToString());
                        sqlcmd.Parameters.Add(para17);

                        SqlParameter para18 = new SqlParameter("@BDate", Bdate);
                        sqlcmd.Parameters.Add(para18);

                        SqlParameter para19 = new SqlParameter("@EDate", Edate);
                        sqlcmd.Parameters.Add(para19);

                        SqlParameter para20 = new SqlParameter("@頻道", Channel_name);
                        sqlcmd.Parameters.Add(para20);


                        if (sqlcmd.ExecuteNonQuery() == 0)
                        {
                            tran.Rollback();
                            return "";
                        }

                    }
                    #endregion

                    tran.Commit();
                    connection.Close();
                    connection.Dispose();
                    connection1.Close();
                    connection1.Dispose();
                    GC.Collect();
                }
                else
                {
                    return "Error:沒有查到資料";
                }

            }
            catch (Exception ex)
            {
                return "Error:訊息:" + ex.Message;
            }

            String strURL = "";  //要傳回去的URL

            //取列印報表的系統設定檔
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string reportSrv = SettingObj.sysConfig_Read("/ServerConfig/ReportServer");

            strURL = reportSrv + "RPT_TBPGM_QUEUE_ZONE_NO_TAPE_LIST_01" + "&QUERY_KEY=" + g.ToString() + "&rc:parameters=false&rs:Command=Render";
            //strURL _URL = "http://172.20.141.92/ReportServer/Pages/ReportViewer.aspx?%2fPTS_MAM3%2fRPT_TBPROPOSAL_01&QUERY_KEY=A3AE2DED-3451-4AF0-898D-04D1417D01AA&rc:parameters=false&rs:Command=Render";
            return Server.UrlEncode(strURL);


            return "";
        }


        //查詢播出提示報表(新版)
        [WebMethod()]
        public string Do_Query_Queue_Insert_Tape(string fddate, string fschannel_id, string parameters, string UserCHtName)
        {
            //依照Parameter的資料逐一查詢節目播映資料的播出題示,如果沒有則去查詢節目的播出題示
            //strSQL為查詢Store Procesure Name
            //parameters為XML內容(DB_Function_Sample.txt)

            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection1 = new SqlConnection(sqlConnStr);
            StringBuilder sb = new StringBuilder();


            SqlTransaction tran = null;
            SqlCommand sqlcmd = new SqlCommand("SP_I_RPT_TBPGM_QUEUE_INSERT_TAPE_01", connection1);
            sqlcmd.CommandType = CommandType.StoredProcedure;

            try
            {
                if (connection1.State == System.Data.ConnectionState.Closed)
                {
                    connection1.Open();
                }

            }
            catch
            {
                return "Error";
            }

            SqlConnection connection = new SqlConnection(sqlConnStr);
            Guid g;
            g = Guid.NewGuid();
            //參數剖析
            SqlDataAdapter daQUEUE_INSERT_TAPE = new SqlDataAdapter("SP_Q_TBPGM_QUEUE_PLAY_MEMO", connection);
            DataTable dtQUEUE_INSERT_TAPE = new DataTable();
            SqlParameter paraQIT1 = new SqlParameter("@FDDATE", fddate);
            SqlParameter paraQIT2 = new SqlParameter("@FSCHANNEL_ID", fschannel_id);

            daQUEUE_INSERT_TAPE.SelectCommand.Parameters.Add(paraQIT1);
            daQUEUE_INSERT_TAPE.SelectCommand.Parameters.Add(paraQIT2);
            daQUEUE_INSERT_TAPE.SelectCommand.CommandType = CommandType.StoredProcedure;
            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                //先查詢QUEUE&Arrpromo的資料
                daQUEUE_INSERT_TAPE.Fill(dtQUEUE_INSERT_TAPE);

                if (dtQUEUE_INSERT_TAPE.Rows.Count > 0) //有播出題是資料
                {

                    sqlcmd.CommandText = "SP_I_RPT_TBPGM_QUEUE_INSERT_TAPE_01"; //將資料寫入報表

                    tran = connection1.BeginTransaction();
                    sqlcmd.Transaction = tran;
                    #region 逐一將資料寫入報表 RPT_TBPGM_QUEUE_INSERT_TAPE_01
                    for (int i = 0; i <= dtQUEUE_INSERT_TAPE.Rows.Count - 1; i++)
                    {
                        string _TempSide_Label = "";
                        string _Insert_Tape = "";
                        if (dtQUEUE_INSERT_TAPE.Rows[i]["FCPROG_SIDE_LABEL"].ToString() == "Y")
                            _TempSide_Label = _TempSide_Label + "節目側標,";
                        if (dtQUEUE_INSERT_TAPE.Rows[i]["FCLIVE_SIDE_LABEL"].ToString() == "Y")
                            _TempSide_Label = _TempSide_Label + "LIVE側標,";
                        if (dtQUEUE_INSERT_TAPE.Rows[i]["FCTIME_SIDE_LABEL"].ToString() == "Y")
                            _TempSide_Label = _TempSide_Label + "時間側標,";
                        if (dtQUEUE_INSERT_TAPE.Rows[i]["FCREPLAY"].ToString() == "Y")
                            _TempSide_Label = _TempSide_Label + "重播,";
                        if (dtQUEUE_INSERT_TAPE.Rows[i]["FCRECORD_PLAY"].ToString() == "Y")
                            _TempSide_Label = _TempSide_Label + "錄影轉播,";
                        if (dtQUEUE_INSERT_TAPE.Rows[i]["FCSTEREO"].ToString() == "Y")
                            _TempSide_Label = _TempSide_Label + "立體聲,";
                        if (dtQUEUE_INSERT_TAPE.Rows[i]["FCLOGO"].ToString() == "L")
                            _TempSide_Label = _TempSide_Label + "公視LOGO(左),";
                        if (dtQUEUE_INSERT_TAPE.Rows[i]["FCLOGO"].ToString() == "R")
                            _TempSide_Label = _TempSide_Label + "公視LOGO(右),";
                        if (dtQUEUE_INSERT_TAPE.Rows[i]["FSCH1"].ToString() != "")
                            _TempSide_Label = _TempSide_Label + " 音軌1:" + dtQUEUE_INSERT_TAPE.Rows[i]["FSCHNAME1"].ToString();
                        if (dtQUEUE_INSERT_TAPE.Rows[i]["FSCH2"].ToString() != "")
                            _TempSide_Label = _TempSide_Label + " 音軌2:" + dtQUEUE_INSERT_TAPE.Rows[i]["FSCHNAME2"].ToString();
                        if (dtQUEUE_INSERT_TAPE.Rows[i]["FSCH3"].ToString() != "")
                            _TempSide_Label = _TempSide_Label + " 音軌3:" + dtQUEUE_INSERT_TAPE.Rows[i]["FSCHNAME3"].ToString();
                        if (dtQUEUE_INSERT_TAPE.Rows[i]["FSCH4"].ToString() != "")
                            _TempSide_Label = _TempSide_Label + " 音軌4:" + dtQUEUE_INSERT_TAPE.Rows[i]["FSCHNAME4"].ToString();

                        if (dtQUEUE_INSERT_TAPE.Rows[i]["FCSMOKE"].ToString() == "Y")
                            _Insert_Tape = _Insert_Tape + "吸菸卡,";
                        if (dtQUEUE_INSERT_TAPE.Rows[i]["FCALERTS"].ToString() == "Y")
                            _Insert_Tape = _Insert_Tape + "警示卡:";
                        if (dtQUEUE_INSERT_TAPE.Rows[i]["FSINSERT_TAPE"].ToString() == "1")
                            _Insert_Tape = _Insert_Tape + "制式版1 ";
                        if (dtQUEUE_INSERT_TAPE.Rows[i]["FSINSERT_TAPE"].ToString() == "2")
                            _Insert_Tape = _Insert_Tape + "制式版2 ";
                        if (dtQUEUE_INSERT_TAPE.Rows[i]["FSINSERT_TAPE"].ToString() == "3")
                            _Insert_Tape = _Insert_Tape + "制式版3 ";
                        if (dtQUEUE_INSERT_TAPE.Rows[i]["FSINSERT_TAPE"].ToString() == "4")
                            _Insert_Tape = _Insert_Tape + "制式版4 ";
                        if (dtQUEUE_INSERT_TAPE.Rows[i]["FSINSERT_TAPE"].ToString() == "5")
                            _Insert_Tape = _Insert_Tape + "制式版5 ";
                        if (dtQUEUE_INSERT_TAPE.Rows[i]["FSINSERT_TAPE"].ToString() == "6")
                            _Insert_Tape = _Insert_Tape + "制式版6 ";
                        if (dtQUEUE_INSERT_TAPE.Rows[i]["FSINSERT_TAPE"].ToString() == "7")
                        {
                            _Insert_Tape = _Insert_Tape + "客製版:" + dtQUEUE_INSERT_TAPE.Rows[i]["FSINSERT_TAPE_NAME"].ToString() + " 長度:" + dtQUEUE_INSERT_TAPE.Rows[i]["FSINSERT_TAPE_DUR"].ToString() + "秒 ";

                        }
                        if (dtQUEUE_INSERT_TAPE.Rows[i]["FSTITLE"].ToString() == "Y")
                            _Insert_Tape = _Insert_Tape + "檔名總片頭:" + dtQUEUE_INSERT_TAPE.Rows[i]["FSTITLE_NAME"].ToString() + " 長度:" + dtQUEUE_INSERT_TAPE.Rows[i]["FSTITLE_DUR"].ToString() + "秒 ";


                        sqlcmd.Parameters.Clear();
                        SqlParameter para11 = new SqlParameter("@QUERY_KEY", g.ToString());
                        sqlcmd.Parameters.Add(para11);
                        Guid g1;
                        g1 = Guid.NewGuid();
                        SqlParameter para12 = new SqlParameter("@QUERY_KEY_SUBID", g1.ToString());
                        sqlcmd.Parameters.Add(para12);

                        SqlParameter para13 = new SqlParameter("@QUERY_BY", UserCHtName);
                        sqlcmd.Parameters.Add(para13);

                        SqlParameter para14 = new SqlParameter("@頻道", dtQUEUE_INSERT_TAPE.Rows[i]["FSCHANNEL_NAME"].ToString());
                        sqlcmd.Parameters.Add(para14);




                        SqlParameter para15 = new SqlParameter("@播出日期", fddate);
                        sqlcmd.Parameters.Add(para15);

                        SqlParameter para16 = new SqlParameter("@節目名稱", dtQUEUE_INSERT_TAPE.Rows[i]["FSPROG_NAME"].ToString());
                        sqlcmd.Parameters.Add(para16);

                        SqlParameter para17 = new SqlParameter("@集數", dtQUEUE_INSERT_TAPE.Rows[i]["FNEPISODE"].ToString());
                        sqlcmd.Parameters.Add(para17);

                        SqlParameter para18 = new SqlParameter("@主控插播帶", _Insert_Tape);
                        sqlcmd.Parameters.Add(para18);

                        SqlParameter para19 = new SqlParameter("@主控鏡面", _TempSide_Label);
                        sqlcmd.Parameters.Add(para19);



                        //sqlcmd.Connection = myconnection1;

                        if (sqlcmd.ExecuteNonQuery() == 0)
                        {
                            tran.Rollback();
                            return "";
                        }

                    }
                    #endregion

                    tran.Commit();
                    connection.Close();
                    connection.Dispose();
                    connection1.Close();
                    connection1.Dispose();
                    GC.Collect();
                }
                else
                {
                    return "Error:沒有查到資料";
                }

            }
            catch (Exception ex)
            {
                return "Error:訊息:" + ex.Message;
            }

            String strURL = "";  //要傳回去的URL

            //取列印報表的系統設定檔
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string reportSrv = SettingObj.sysConfig_Read("/ServerConfig/ReportServer");

            strURL = reportSrv + "RPT_TBPGM_QUEUE_INSERT_TAPE_01" + "&QUERY_KEY=" + g.ToString() + "&rc:parameters=false&rs:Command=Render";
            //strURL _URL = "http://172.20.141.92/ReportServer/Pages/ReportViewer.aspx?%2fPTS_MAM3%2fRPT_TBPROPOSAL_01&QUERY_KEY=A3AE2DED-3451-4AF0-898D-04D1417D01AA&rc:parameters=false&rs:Command=Render";
            return Server.UrlEncode(strURL);


            return "";
        }

        //查詢播出提示報表
        [WebMethod()]
        public string Do_Query_Play_Memo(string fddate, string fschannel_id, string parameters, string UserCHtName)
        {
            //依照Parameter的資料逐一查詢節目播映資料的播出題示,如果沒有則去查詢節目的播出題示
            //strSQL為查詢Store Procesure Name
            //parameters為XML內容(DB_Function_Sample.txt)

            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection1 = new SqlConnection(sqlConnStr);
            StringBuilder sb = new StringBuilder();


            SqlTransaction tran = null;
            SqlCommand sqlcmd = new SqlCommand("SP_I_RPT_TBPGM_PLAY_MEMO_01", connection1);
            sqlcmd.CommandType = CommandType.StoredProcedure;

            try
            {
                if (connection1.State == System.Data.ConnectionState.Closed)
                {
                    connection1.Open();
                }

            }
            catch
            {
                return "Error";
            }

            SqlConnection connection = new SqlConnection(sqlConnStr);
            Guid g;
            g = Guid.NewGuid();
            //參數剖析
            if (parameters != "")
            {

                try
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(parameters);
                    XmlNode xmlNode = xmldoc.FirstChild;

                    for (int k = 0; k <= xmlNode.ChildNodes.Count - 1; k++)
                    {
                        string _TempProgID = "";
                        string _TempChannelID = "";
                        string _TempDate = "";
                        string _TempSeqno = "";
                        string _TempEpisode = "";
                        string _TempChannel_Name = "";
                        string _TempSide_Label = "";
                        string _CH1 = "";
                        string _CH2 = "";
                        string _CH3 = "";
                        string _CH4 = "";
                        string _TempProg_Name = "";
                        for (int j = 0; j <= xmlNode.ChildNodes[k].ChildNodes.Count - 1; j++)
                        {
                            if (xmlNode.ChildNodes[k].ChildNodes[j].Name == "FSPROG_ID")
                                _TempProgID = xmlNode.ChildNodes[k].ChildNodes[j].FirstChild.Value;
                            if (xmlNode.ChildNodes[k].ChildNodes[j].Name == "FNSEQNO")
                                _TempSeqno = xmlNode.ChildNodes[k].ChildNodes[j].FirstChild.Value;
                            if (xmlNode.ChildNodes[k].ChildNodes[j].Name == "FSCHANNEL_ID")
                                _TempChannelID = xmlNode.ChildNodes[k].ChildNodes[j].FirstChild.Value;
                            if (xmlNode.ChildNodes[k].ChildNodes[j].Name == "FDDATE")
                                _TempDate = xmlNode.ChildNodes[k].ChildNodes[j].FirstChild.Value;
                            if (xmlNode.ChildNodes[k].ChildNodes[j].Name == "FNEPISODE")
                                _TempEpisode = xmlNode.ChildNodes[k].ChildNodes[j].FirstChild.Value;
                            if (xmlNode.ChildNodes[k].ChildNodes[j].Name == "FSCHANNEL_NAME")
                                _TempChannel_Name = xmlNode.ChildNodes[k].ChildNodes[j].FirstChild.Value;
                            if (xmlNode.ChildNodes[k].ChildNodes[j].Name == "FSPROG_ANME")
                                _TempProg_Name = xmlNode.ChildNodes[k].ChildNodes[j].FirstChild.Value;
                        }
                        SqlDataAdapter daQUEUE_INSERT_TAPE = new SqlDataAdapter("SP_Q_TBPGM_QUEUE_INSERT_TAPE", connection);
                        DataTable dtQUEUE_INSERT_TAPE = new DataTable();
                        SqlParameter paraQIT1 = new SqlParameter("@FDDATE", _TempDate);
                        SqlParameter paraQIT2 = new SqlParameter("@FSPROG_ID", _TempProgID);
                        SqlParameter paraQIT3 = new SqlParameter("@FNSEQNO", _TempSeqno);
                        SqlParameter paraQIT4 = new SqlParameter("@FSCHANNEL_ID", _TempChannelID);

                        SqlDataAdapter daQUEUE_MONITER = new SqlDataAdapter("SP_Q_TBPGM_QUEUE_MONITER", connection);
                        DataTable dtQUEUE_MONITER = new DataTable();
                        SqlParameter paraQM1 = new SqlParameter("@FDDATE", _TempDate);
                        SqlParameter paraQM2 = new SqlParameter("@FSPROG_ID", _TempProgID);
                        SqlParameter paraQM3 = new SqlParameter("@FNSEQNO", _TempSeqno);
                        SqlParameter paraQM4 = new SqlParameter("@FSCHANNEL_ID", _TempChannelID);

                        daQUEUE_INSERT_TAPE.SelectCommand.Parameters.Add(paraQIT1);
                        daQUEUE_INSERT_TAPE.SelectCommand.Parameters.Add(paraQIT2);
                        daQUEUE_INSERT_TAPE.SelectCommand.Parameters.Add(paraQIT3);
                        daQUEUE_INSERT_TAPE.SelectCommand.Parameters.Add(paraQIT4);
                        daQUEUE_INSERT_TAPE.SelectCommand.CommandType = CommandType.StoredProcedure;
                        daQUEUE_MONITER.SelectCommand.Parameters.Add(paraQM1);
                        daQUEUE_MONITER.SelectCommand.Parameters.Add(paraQM2);
                        daQUEUE_MONITER.SelectCommand.Parameters.Add(paraQM3);
                        daQUEUE_MONITER.SelectCommand.Parameters.Add(paraQM4);
                        daQUEUE_MONITER.SelectCommand.CommandType = CommandType.StoredProcedure;

                        if (connection.State == System.Data.ConnectionState.Closed)
                        {
                            connection.Open();
                        }

                        //先查詢QUEUE&Arrpromo的資料
                        daQUEUE_INSERT_TAPE.Fill(dtQUEUE_INSERT_TAPE);
                        daQUEUE_MONITER.Fill(dtQUEUE_MONITER);
                        if (dtQUEUE_MONITER.Rows.Count > 0) //有播映資料的播出提示資料
                        {

                            sqlcmd.CommandText = "SP_I_RPT_TBPGM_PLAY_MEMO_01";

                            tran = connection1.BeginTransaction();
                            sqlcmd.Transaction = tran;
                            if (dtQUEUE_MONITER.Rows.Count > 0) //有節目集數資料的播出提示資料
                            {
                                _TempSide_Label = "";
                                int i;
                                for (i = 0; i <= dtQUEUE_MONITER.Rows.Count - 1; i++)
                                {

                                    if (dtQUEUE_MONITER.Rows[i]["FCPROG_SIDE_LABEL"].ToString() == "Y")
                                        _TempSide_Label = _TempSide_Label + "節目側標,";
                                    if (dtQUEUE_MONITER.Rows[i]["FCLIVE_SIDE_LABEL"].ToString() == "Y")
                                        _TempSide_Label = _TempSide_Label + "LIVE側標,";
                                    if (dtQUEUE_MONITER.Rows[i]["FCTIME_SIDE_LABEL"].ToString() == "Y")
                                        _TempSide_Label = _TempSide_Label + "時間側標,";
                                    if (dtQUEUE_MONITER.Rows[i]["FCREPLAY"].ToString() == "Y")
                                        _TempSide_Label = _TempSide_Label + "重播,";
                                    if (dtQUEUE_MONITER.Rows[i]["FCRECORD_PLAY"].ToString() == "Y")
                                        _TempSide_Label = _TempSide_Label + "錄影轉播,";
                                    if (dtQUEUE_MONITER.Rows[i]["FCSTEREO"].ToString() == "Y")
                                        _TempSide_Label = _TempSide_Label + "立體聲,";
                                    if (dtQUEUE_MONITER.Rows[i]["FCLOGO"].ToString() == "Y")
                                        _TempSide_Label = _TempSide_Label + "公視LOGO,";

                                    _CH1 = dtQUEUE_MONITER.Rows[i]["FSCH1"].ToString();
                                    _CH2 = dtQUEUE_MONITER.Rows[i]["FSCH2"].ToString();
                                    _CH3 = dtQUEUE_MONITER.Rows[i]["FSCH3"].ToString();
                                    _CH4 = dtQUEUE_MONITER.Rows[i]["FSCH4"].ToString();

                                }

                                if (dtQUEUE_INSERT_TAPE.Rows.Count > 0)
                                {
                                    for (i = 0; i <= dtQUEUE_INSERT_TAPE.Rows.Count - 1; i++)
                                    {
                                        sqlcmd.Parameters.Clear();
                                        SqlParameter para11 = new SqlParameter("@QUERY_KEY", g.ToString());
                                        sqlcmd.Parameters.Add(para11);
                                        Guid g1;
                                        g1 = Guid.NewGuid();
                                        SqlParameter para12 = new SqlParameter("@QUERY_KEY_SUBID", g1.ToString());
                                        sqlcmd.Parameters.Add(para12);

                                        SqlParameter para13 = new SqlParameter("@QUERY_BY", UserCHtName);
                                        sqlcmd.Parameters.Add(para13);

                                        SqlParameter para14 = new SqlParameter("@頻道名稱", _TempChannel_Name);
                                        sqlcmd.Parameters.Add(para14);

                                        SqlParameter para15 = new SqlParameter("@播出日期", _TempDate);
                                        sqlcmd.Parameters.Add(para15);

                                        SqlParameter para16 = new SqlParameter("@節目編碼", _TempProgID);
                                        sqlcmd.Parameters.Add(para16);

                                        SqlParameter para17 = new SqlParameter("@播映序號", _TempSeqno);
                                        sqlcmd.Parameters.Add(para17);

                                        SqlParameter para18 = new SqlParameter("@節目名稱", _TempProg_Name);
                                        sqlcmd.Parameters.Add(para18);

                                        SqlParameter para19 = new SqlParameter("@短帶名稱", dtQUEUE_INSERT_TAPE.Rows[i]["FSPROMO_NAME"]);
                                        sqlcmd.Parameters.Add(para19);

                                        SqlParameter para20 = new SqlParameter("@主控鏡面", _TempSide_Label);
                                        sqlcmd.Parameters.Add(para20);

                                        SqlParameter para21 = new SqlParameter("@CH1", _CH1);
                                        sqlcmd.Parameters.Add(para21);

                                        SqlParameter para22 = new SqlParameter("@CH2", _CH2);
                                        sqlcmd.Parameters.Add(para22);

                                        SqlParameter para23 = new SqlParameter("@CH3", _CH3);
                                        sqlcmd.Parameters.Add(para23);

                                        SqlParameter para24 = new SqlParameter("@CH4", _CH4);
                                        sqlcmd.Parameters.Add(para24);

                                        //sqlcmd.Connection = myconnection1;

                                        if (sqlcmd.ExecuteNonQuery() == 0)
                                        {
                                            tran.Rollback();
                                            return "";
                                        }
                                    }
                                }


                            }
                            else  //如果沒有dtPROG_D_INSERT_TAPE 資料
                            {
                                sqlcmd.Parameters.Clear();
                                SqlParameter para11 = new SqlParameter("@QUERY_KEY", g.ToString());
                                sqlcmd.Parameters.Add(para11);
                                Guid g1;
                                g1 = Guid.NewGuid();
                                SqlParameter para12 = new SqlParameter("@QUERY_KEY_SUBID", g1.ToString());
                                sqlcmd.Parameters.Add(para12);

                                SqlParameter para13 = new SqlParameter("@QUERY_BY", UserCHtName);
                                sqlcmd.Parameters.Add(para13);

                                SqlParameter para14 = new SqlParameter("@頻道名稱", _TempChannel_Name);
                                sqlcmd.Parameters.Add(para14);

                                SqlParameter para15 = new SqlParameter("@播出日期", _TempDate);
                                sqlcmd.Parameters.Add(para15);

                                SqlParameter para16 = new SqlParameter("@節目編碼", _TempProgID);
                                sqlcmd.Parameters.Add(para16);

                                SqlParameter para17 = new SqlParameter("@播映序號", _TempSeqno);
                                sqlcmd.Parameters.Add(para17);

                                SqlParameter para18 = new SqlParameter("@節目名稱", _TempProg_Name);
                                sqlcmd.Parameters.Add(para18);

                                SqlParameter para19 = new SqlParameter("@短帶名稱", "");
                                sqlcmd.Parameters.Add(para19);

                                SqlParameter para20 = new SqlParameter("@主控鏡面", _TempSide_Label);
                                sqlcmd.Parameters.Add(para20);

                                SqlParameter para21 = new SqlParameter("@CH1", _CH1);
                                sqlcmd.Parameters.Add(para21);

                                SqlParameter para22 = new SqlParameter("@CH2", _CH2);
                                sqlcmd.Parameters.Add(para22);

                                SqlParameter para23 = new SqlParameter("@CH3", _CH3);
                                sqlcmd.Parameters.Add(para23);

                                SqlParameter para24 = new SqlParameter("@CH4", _CH4);
                                sqlcmd.Parameters.Add(para24);


                                if (sqlcmd.ExecuteNonQuery() == 0)
                                {
                                    tran.Rollback();
                                    return "";
                                }
                            }
                            tran.Commit();
                            connection.Close();
                            connection.Dispose();
                            GC.Collect();

                        }
                        else  //去查詢是否有節目集數的播出提示資料
                        {
                            SqlDataAdapter daPROG_D_INSERT_TAPE = new SqlDataAdapter("SP_Q_TBPROG_D_INSERT_TAPE", connection);
                            DataTable dtPROG_D_INSERT_TAPE = new DataTable();
                            SqlParameter paraDIT1 = new SqlParameter("@FNEPISODE", _TempEpisode);
                            SqlParameter paraDIT2 = new SqlParameter("@FSPROG_ID", _TempProgID);


                            SqlDataAdapter daPROG_D_MONITER = new SqlDataAdapter("SP_Q_TBPROG_D_MONITER", connection);
                            DataTable dtPROG_D_MONITER = new DataTable();
                            SqlParameter paraDM1 = new SqlParameter("@FNEPISODE", _TempEpisode);
                            SqlParameter paraDM2 = new SqlParameter("@FSPROG_ID", _TempProgID);
                            daPROG_D_INSERT_TAPE.SelectCommand.CommandType = CommandType.StoredProcedure;
                            daPROG_D_MONITER.SelectCommand.CommandType = CommandType.StoredProcedure;
                            daPROG_D_INSERT_TAPE.Fill(dtPROG_D_INSERT_TAPE);
                            daPROG_D_MONITER.Fill(dtPROG_D_MONITER);

                            sqlcmd.CommandText = "SP_I_RPT_TBPGM_PLAY_MEMO_01";

                            tran = connection.BeginTransaction();
                            sqlcmd.Transaction = tran;
                            if (dtPROG_D_MONITER.Rows.Count > 0) //有節目集數資料的播出提示資料
                            {
                                _TempSide_Label = "";
                                int i;
                                for (i = 0; i <= dtPROG_D_MONITER.Rows.Count - 1; i++)
                                {
                                    if (dtPROG_D_MONITER.Rows[i][2].ToString() == "Y")
                                        _TempSide_Label = _TempSide_Label + "節目側標,";
                                    if (dtPROG_D_MONITER.Rows[i][3].ToString() == "Y")
                                        _TempSide_Label = _TempSide_Label + "LIVE側標,";
                                    if (dtPROG_D_MONITER.Rows[i][4].ToString() == "Y")
                                        _TempSide_Label = _TempSide_Label + "時間側標,";
                                    if (dtPROG_D_MONITER.Rows[i][5].ToString() == "Y")
                                        _TempSide_Label = _TempSide_Label + "重播,";
                                    if (dtPROG_D_MONITER.Rows[i][6].ToString() == "Y")
                                        _TempSide_Label = _TempSide_Label + "錄影轉播,";
                                    if (dtPROG_D_MONITER.Rows[i][7].ToString() == "Y")
                                        _TempSide_Label = _TempSide_Label + "立體聲,";
                                    if (dtPROG_D_MONITER.Rows[i][8].ToString() == "Y")
                                        _TempSide_Label = _TempSide_Label + "公視LOGO,";

                                    _CH1 = dtPROG_D_MONITER.Rows[i][9].ToString();
                                    _CH2 = dtPROG_D_MONITER.Rows[i][10].ToString();
                                    _CH3 = dtPROG_D_MONITER.Rows[i][11].ToString();
                                    _CH4 = dtPROG_D_MONITER.Rows[i][12].ToString();

                                }

                                if (dtPROG_D_INSERT_TAPE.Rows.Count > 0)
                                {
                                    for (i = 0; i <= dtPROG_D_INSERT_TAPE.Rows.Count - 1; i++)
                                    {
                                        sqlcmd.Parameters.Clear();
                                        SqlParameter para11 = new SqlParameter("@QUERY_KEY", g.ToString());
                                        sqlcmd.Parameters.Add(para11);
                                        Guid g1;
                                        g1 = Guid.NewGuid();
                                        SqlParameter para12 = new SqlParameter("@QUERY_KEY_SUBID", g1.ToString());
                                        sqlcmd.Parameters.Add(para12);

                                        SqlParameter para13 = new SqlParameter("@QUERY_BY", UserCHtName);
                                        sqlcmd.Parameters.Add(para13);

                                        SqlParameter para14 = new SqlParameter("@頻道名稱", _TempChannel_Name);
                                        sqlcmd.Parameters.Add(para14);

                                        SqlParameter para15 = new SqlParameter("@播出日期", _TempDate);
                                        sqlcmd.Parameters.Add(para15);

                                        SqlParameter para16 = new SqlParameter("@節目編碼", _TempProgID);
                                        sqlcmd.Parameters.Add(para16);

                                        SqlParameter para17 = new SqlParameter("@播映序號", _TempSeqno);
                                        sqlcmd.Parameters.Add(para17);

                                        SqlParameter para18 = new SqlParameter("@節目名稱", _TempProg_Name);
                                        sqlcmd.Parameters.Add(para18);

                                        SqlParameter para19 = new SqlParameter("@短帶名稱", dtPROG_D_INSERT_TAPE.Rows[i]["FSPROMO_NAME"]);
                                        sqlcmd.Parameters.Add(para19);

                                        SqlParameter para20 = new SqlParameter("@主控鏡面", _TempSide_Label);
                                        sqlcmd.Parameters.Add(para20);

                                        SqlParameter para21 = new SqlParameter("@CH1", _CH1);
                                        sqlcmd.Parameters.Add(para21);

                                        SqlParameter para22 = new SqlParameter("@CH2", _CH2);
                                        sqlcmd.Parameters.Add(para22);

                                        SqlParameter para23 = new SqlParameter("@CH3", _CH3);
                                        sqlcmd.Parameters.Add(para23);

                                        SqlParameter para24 = new SqlParameter("@CH4", _CH4);
                                        sqlcmd.Parameters.Add(para24);


                                        if (sqlcmd.ExecuteNonQuery() == 0)
                                        {
                                            tran.Rollback();
                                            return "";
                                        }
                                    }
                                }


                            }
                            else  //如果沒有dtPROG_D_INSERT_TAPE 資料
                            {
                                sqlcmd.Parameters.Clear();
                                SqlParameter para11 = new SqlParameter("@QUERY_KEY", g.ToString());
                                sqlcmd.Parameters.Add(para11);
                                Guid g1;
                                g1 = Guid.NewGuid();
                                SqlParameter para12 = new SqlParameter("@QUERY_KEY_SUBID", g1.ToString());
                                sqlcmd.Parameters.Add(para12);

                                SqlParameter para13 = new SqlParameter("@QUERY_BY", UserCHtName);
                                sqlcmd.Parameters.Add(para13);

                                SqlParameter para14 = new SqlParameter("@頻道名稱", _TempChannel_Name);
                                sqlcmd.Parameters.Add(para14);

                                SqlParameter para15 = new SqlParameter("@播出日期", _TempDate);
                                sqlcmd.Parameters.Add(para15);

                                SqlParameter para16 = new SqlParameter("@節目編碼", _TempProgID);
                                sqlcmd.Parameters.Add(para16);

                                SqlParameter para17 = new SqlParameter("@播映序號", _TempSeqno);
                                sqlcmd.Parameters.Add(para17);

                                SqlParameter para18 = new SqlParameter("@節目名稱", _TempProg_Name);
                                sqlcmd.Parameters.Add(para18);

                                SqlParameter para19 = new SqlParameter("@短帶名稱", "");
                                sqlcmd.Parameters.Add(para19);

                                SqlParameter para20 = new SqlParameter("@主控鏡面", _TempSide_Label);
                                sqlcmd.Parameters.Add(para20);

                                SqlParameter para21 = new SqlParameter("@CH1", _CH1);
                                sqlcmd.Parameters.Add(para21);

                                SqlParameter para22 = new SqlParameter("@CH2", _CH2);
                                sqlcmd.Parameters.Add(para22);

                                SqlParameter para23 = new SqlParameter("@CH3", _CH3);
                                sqlcmd.Parameters.Add(para23);

                                SqlParameter para24 = new SqlParameter("@CH4", _CH4);
                                sqlcmd.Parameters.Add(para24);


                                if (sqlcmd.ExecuteNonQuery() == 0)
                                {
                                    tran.Rollback();
                                    return "";
                                }
                            }
                            tran.Commit();
                            connection.Close();
                            connection.Dispose();
                            GC.Collect();
                        }
                    }
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
            }
            String strURL = "";  //要傳回去的URL

            //取列印報表的系統設定檔
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string reportSrv = SettingObj.sysConfig_Read("/ServerConfig/ReportServer");

            strURL = reportSrv + "RPT_TBPGM_PLAY_MEMO_01" + "&QUERY_KEY=" + g.ToString() + "&rc:parameters=false&rs:Command=Render";
            //strURL _URL = "http://172.20.141.92/ReportServer/Pages/ReportViewer.aspx?%2fPTS_MAM3%2fRPT_TBPROPOSAL_01&QUERY_KEY=A3AE2DED-3451-4AF0-898D-04D1417D01AA&rc:parameters=false&rs:Command=Render";
            return Server.UrlEncode(strURL);


            return "";
        }

        //取得報表URL
        [WebMethod]
        public string GETReportURL(string REPORTNAME, string QUERY_KEY)
        {
            String strURL = "";  //要傳回去的URL

            //取列印報表的系統設定檔
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string reportSrv = SettingObj.sysConfig_Read("/ServerConfig/ReportServer");
            strURL = reportSrv + REPORTNAME + "&QUERY_KEY=" + QUERY_KEY + "&rc:parameters=false&rs:Command=Render";
            //strURL _URL = "http://172.20.141.92/ReportServer/Pages/ReportViewer.aspx?%2fPTS_MAM3%2fRPT_TBPROPOSAL_01&QUERY_KEY=A3AE2DED-3451-4AF0-898D-04D1417D01AA&rc:parameters=false&rs:Command=Render";
            return Server.UrlEncode(strURL);
        }

        //取得救命帶清單
        [WebMethod]
        public string PrintSaveTapeList(string PrintDate, string PrintChannel, string PrintUserName)
        {
            //要先查詢救命帶清單寫入救命帶清單列印表

            //strSQL為查詢Store Procesure Name
            //parameters為XML內容(DB_Function_Sample.txt)

            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            StringBuilder sb = new StringBuilder();

            SqlDataAdapter da = new SqlDataAdapter("SP_Q_TBPGM_SAVE_TAPE_LIST", connection);
            DataTable dt = new DataTable();
            SqlParameter para = new SqlParameter("@FDDATE", PrintDate);
            da.SelectCommand.Parameters.Add(para);
            SqlParameter para1 = new SqlParameter("@FSCHANNEL_ID", PrintChannel);
            da.SelectCommand.Parameters.Add(para1);
            SqlParameter para2 = new SqlParameter("@FSPROG_ID", "");
            da.SelectCommand.Parameters.Add(para2);
            SqlParameter para3 = new SqlParameter("@FNSEQNO", "");
            da.SelectCommand.Parameters.Add(para3);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlTransaction tran = null;
            SqlCommand sqlcmd = new SqlCommand("SP_I_RPT_TBPGM_SAVE_TAPE_LIST_01", connection);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            Guid g;
            g = Guid.NewGuid();
            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                da.Fill(dt);

                tran = connection.BeginTransaction();
                sqlcmd.Transaction = tran;

                if (dt.Rows.Count > 0)
                {
                    sqlcmd.CommandText = "SP_I_RPT_TBPGM_SAVE_TAPE_LIST_01";
                    //逐筆寫入救命帶列印清單

                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        sqlcmd.Parameters.Clear();
                        SqlParameter para11 = new SqlParameter("@QUERY_KEY", g.ToString());
                        sqlcmd.Parameters.Add(para11);
                        Guid g1;
                        g1 = Guid.NewGuid();
                        SqlParameter para12 = new SqlParameter("@QUERY_KEY_SUBID", g1.ToString());
                        sqlcmd.Parameters.Add(para12);

                        SqlParameter para13 = new SqlParameter("@QUERY_BY", PrintUserName);
                        sqlcmd.Parameters.Add(para13);

                        SqlParameter para14 = new SqlParameter("@播出日期", dt.Rows[i]["FDDATE"]);
                        sqlcmd.Parameters.Add(para14);

                        SqlParameter para15 = new SqlParameter("@頻道", dt.Rows[i]["FSCHANNEL_NAME"]);
                        sqlcmd.Parameters.Add(para15);

                        SqlParameter para16 = new SqlParameter("@來源節目名稱", dt.Rows[i]["FSPROG_NAME"]);
                        sqlcmd.Parameters.Add(para16);

                        SqlParameter para17 = new SqlParameter("@來源節目集數", dt.Rows[i]["FNEPISODE"]);
                        sqlcmd.Parameters.Add(para17);

                        SqlParameter para18 = new SqlParameter("@起始時間", dt.Rows[i]["FSBEG_TIME"]);
                        sqlcmd.Parameters.Add(para18);

                        SqlParameter para19 = new SqlParameter("@結束時間", dt.Rows[i]["FSEND_TIME"]);
                        sqlcmd.Parameters.Add(para19);

                        SqlParameter para20 = new SqlParameter("@序號", dt.Rows[i]["FNSAVE_NO"]);
                        sqlcmd.Parameters.Add(para20);

                        SqlParameter para21 = new SqlParameter("@替換節目名稱", DTRowToString(dt.Rows[i]["FSPGDNAME"]));
                        sqlcmd.Parameters.Add(para21);



                        SqlParameter para22 = new SqlParameter("@替換節目集數", DTRowToString(dt.Rows[i]["FNSAVE_EPISODE"]));
                        sqlcmd.Parameters.Add(para22);

                        SqlParameter para23 = new SqlParameter("@替換節目檔案編號", DTRowToString(dt.Rows[i]["FSSAVE_TAPENO"]));
                        sqlcmd.Parameters.Add(para23);

                        SqlParameter para24 = new SqlParameter("@替換節目檔案長度", DTRowToString(dt.Rows[i]["FSSAVE_LENGTH"]));
                        sqlcmd.Parameters.Add(para24);


                        if (sqlcmd.ExecuteNonQuery() == 0)
                        {
                            tran.Rollback();
                            return "";
                        }
                    }
                    tran.Commit();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                StringBuilder sbError = new StringBuilder();
                return ex.Message.ToString();
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }

            String strURL = "";  //要傳回去的URL

            //取列印報表的系統設定檔
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string reportSrv = SettingObj.sysConfig_Read("/ServerConfig/ReportServer");

            strURL = reportSrv + "RPT_TBPGM_SAVE_TAPE_LIST_01" + "&QUERY_KEY=" + g.ToString() + "&rc:parameters=false&rs:Command=Render";
            //strURL _URL = "http://172.20.141.92/ReportServer/Pages/ReportViewer.aspx?%2fPTS_MAM3%2fRPT_TBPROPOSAL_01&QUERY_KEY=A3AE2DED-3451-4AF0-898D-04D1417D01AA&rc:parameters=false&rs:Command=Render";
            return Server.UrlEncode(strURL);
        }

        //取得建議刪除清單
        [WebMethod]
        public string Print_Propose_Delete_List(string PrintUserName,string ChannelType)
        {
            //要先查詢救命帶清單寫入救命帶清單列印表

            //strSQL為查詢Store Procesure Name
            //parameters為XML內容(DB_Function_Sample.txt)

            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            StringBuilder sb = new StringBuilder();

            SqlDataAdapter da = new SqlDataAdapter("SP_Q_PROPOSE_DELETE_LIST", connection);
            DataTable dt = new DataTable();
            SqlParameter para = new SqlParameter("@CHECKDATE", "7");
            da.SelectCommand.Parameters.Add(para);

            SqlParameter para1 = new SqlParameter("@CHANNELTYPE", ChannelType);
            da.SelectCommand.Parameters.Add(para1);

            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlTransaction tran = null;
            SqlCommand sqlcmd = new SqlCommand("SP_I_RPT_PROPOSE_DELETE_LIST", connection);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            Guid g;
            g = Guid.NewGuid();
            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                da.Fill(dt);

                tran = connection.BeginTransaction();
                sqlcmd.Transaction = tran;

                if (dt.Rows.Count > 0)
                {
                    sqlcmd.CommandText = "SP_I_RPT_PROPOSE_DELETE_LIST_01";
                    //逐筆寫入救命帶列印清單

                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                    {

                        sqlcmd.Parameters.Clear();
                        SqlParameter para11 = new SqlParameter("@QUERY_KEY", g.ToString());
                        sqlcmd.Parameters.Add(para11);
                        Guid g1;
                        g1 = Guid.NewGuid();
                        SqlParameter para12 = new SqlParameter("@QUERY_KEY_SUBID", g1.ToString());
                        sqlcmd.Parameters.Add(para12);

                        SqlParameter para13 = new SqlParameter("@QUERY_BY", PrintUserName);
                        sqlcmd.Parameters.Add(para13);

                        SqlParameter para14 = new SqlParameter("@FSVIDEOID", dt.Rows[i]["FSVIDEOID"]);
                        sqlcmd.Parameters.Add(para14);

                        SqlParameter para15 = new SqlParameter("@FSNAME", dt.Rows[i]["PROGNAME"]);
                        sqlcmd.Parameters.Add(para15);

                        SqlParameter para16 = new SqlParameter("@FNEPISODE", dt.Rows[i]["FNEPISODE"]);
                        sqlcmd.Parameters.Add(para16);

                        SqlParameter para17 = new SqlParameter("@FNSEG_ID", dt.Rows[i]["FNSEG_ID"]);
                        sqlcmd.Parameters.Add(para17);

                        if (sqlcmd.ExecuteNonQuery() == 0)
                        {
                            tran.Rollback();
                            return "";
                        }
                    }
                    tran.Commit();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                StringBuilder sbError = new StringBuilder();
                return "ERROR:" + ex.Message.ToString();
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }

            String strURL = "";  //要傳回去的URL

            //取列印報表的系統設定檔
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string reportSrv = SettingObj.sysConfig_Read("/ServerConfig/ReportServer");

            strURL = reportSrv + "RPT_PROPOSE_DELETE_LIST" + "&QUERY_KEY=" + g.ToString() + "&rc:parameters=false&rs:Command=Render";
            //strURL _URL = "http://172.20.141.92/ReportServer/Pages/ReportViewer.aspx?%2fPTS_MAM3%2fRPT_TBPROPOSAL_01&QUERY_KEY=A3AE2DED-3451-4AF0-898D-04D1417D01AA&rc:parameters=false&rs:Command=Render";
            return Server.UrlEncode(strURL);
        }

        //查詢節目運行表
        [WebMethod()]
        public string Do_Query_Prog_Table(string strSQL, string parameters = null)
        {
            //strSQL為查詢Store Procesure Name
            //parameters為XML內容(DB_Function_Sample.txt)

            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            StringBuilder sb = new StringBuilder();

            SqlDataAdapter daCombine = new SqlDataAdapter("SP_Q_TBPGM_COMBINE_QUEUE", connection);
            SqlDataAdapter daArrPromo = new SqlDataAdapter("SP_Q_TBPGM_ARR_PROMO", connection);
            DataTable dtCombine = new DataTable();
            DataTable dtArrPromo = new DataTable();

            //參數剖析
            if (parameters != "")
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(parameters);
                XmlNode xmlNode = xmldoc.FirstChild;
                for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                {
                    if (xmlNode.ChildNodes[i].FirstChild == null || xmlNode.ChildNodes[i].FirstChild.Value == "")
                    {
                        SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, "");
                        daCombine.SelectCommand.Parameters.Add(para);
                        daArrPromo.SelectCommand.Parameters.Add(para);
                    }
                    else
                    {
                        SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, xmlNode.ChildNodes[i].FirstChild.Value);
                        daCombine.SelectCommand.Parameters.Add(para);
                        daArrPromo.SelectCommand.Parameters.Add(para);
                    }
                    //SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].Name, xmlNode.ChildNodes[i].InnerXml);
                    //da.SelectCommand.Parameters.Add(para);
                }

            }

            daCombine.SelectCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                daCombine.Fill(dtCombine);
                daArrPromo.Fill(dtArrPromo);

                if (dtCombine.Rows.Count > 0)
                {
                    sb.AppendLine("<Datas>");
                    for (int i = 0; i <= dtCombine.Rows.Count - 1; i++)
                    {
                        sb.AppendLine("<Data>");
                        for (int j = 0; j <= dtCombine.Columns.Count - 1; j++)
                        {

                            if (Convert.IsDBNull(dtCombine.Rows[i][j]))
                            {
                                sb.AppendLine("<" + dtCombine.Columns[j].ColumnName + "></" + dtCombine.Columns[j].ColumnName + ">");
                            }
                            else
                            {
                                sb.AppendLine("<" + dtCombine.Columns[j].ColumnName + ">" + ReplaceXML(dtCombine.Rows[i][j].ToString()) + "</" + dtCombine.Columns[j].ColumnName + ">");
                            }
                        }
                        sb.AppendLine("<FSSTATUS>C</FSSTATUS>"); //此資料為節目段落

                        sb.AppendLine("</Data>");
                        //依照此則節目段落一一加入插入的宣傳帶

                        for (int k = 0; k <= dtArrPromo.Rows.Count - 1; k++)
                        {
                            if (dtCombine.Rows[i]["FSPROG_ID"] == dtArrPromo.Rows[k]["FSPROG_ID"] && dtCombine.Rows[i]["FNSEQNO"] == dtArrPromo.Rows[k]["FNSEQNO"] && dtCombine.Rows[i]["FNBREAK_NO"] == dtArrPromo.Rows[k]["FNBREAK_NO"])
                            {
                                sb.AppendLine("<Data>");
                                for (int l = 0; l <= dtArrPromo.Columns.Count - 1; l++)
                                {

                                    if (Convert.IsDBNull(dtArrPromo.Rows[k][l]))
                                    {
                                        sb.AppendLine("<" + dtArrPromo.Columns[l].ColumnName + "></" + dtArrPromo.Columns[l].ColumnName + ">");
                                    }
                                    else
                                    {
                                        sb.AppendLine("<" + dtArrPromo.Columns[l].ColumnName + ">" + ReplaceXML(dtArrPromo.Rows[k][l].ToString()) + "</" + dtArrPromo.Columns[l].ColumnName + ">");
                                    }
                                }
                                sb.AppendLine("<FSSTATUS>M</FSSTATUS>"); //此資料為宣傳帶段落
                                sb.AppendLine("</Data>");
                            }
                        }
                    }
                    sb.AppendLine("</Datas>");
                }
                else
                {
                    return sb.ToString();
                }
                return sb.ToString();

            }
            catch (Exception ex)
            {
                StringBuilder sbError = new StringBuilder();
                sbError.AppendLine("<Errors>" + ex.Message + "</Errors>");
                return sbError.ToString();
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }
        }


        //取得宣傳帶未到帶清單
        [WebMethod()]
        public string Get_Arr_Promo_NO_Tape_List(string PrintDate, string PrintChannel, string PrintUserName)
        {
            //strSQL為查詢Store Procesure Name
            //parameters為XML內容(DB_Function_Sample.txt)

            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            StringBuilder sb = new StringBuilder();

            SqlDataAdapter daArr_Promo_NO_Tape_List = new SqlDataAdapter("SP_Q_GET_TBPGM_ARR_PROMO_NO_TAPE_LIST", connection);
            DataTable dtArr_Promo_NO_Tape_List = new DataTable();
            SqlParameter para = new SqlParameter("@FSCHANNEL_ID", PrintChannel);
            SqlParameter para1 = new SqlParameter("@FDDATE", PrintDate);

            daArr_Promo_NO_Tape_List.SelectCommand.Parameters.Add(para);
            daArr_Promo_NO_Tape_List.SelectCommand.Parameters.Add(para1);
            daArr_Promo_NO_Tape_List.SelectCommand.CommandType = CommandType.StoredProcedure;
            if (connection.State == System.Data.ConnectionState.Closed)
            {
                connection.Open();
            }

            daArr_Promo_NO_Tape_List.Fill(dtArr_Promo_NO_Tape_List);
            if (dtArr_Promo_NO_Tape_List.Rows.Count == 0)
            {
                return "沒有未到帶清單";
            }

            SqlTransaction tran = null;
            SqlCommand sqlcmd = new SqlCommand("SP_I_RPT_TBPGM_ARR_PROMO_NO_TAPE_LIST", connection);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            Guid g;
            g = Guid.NewGuid();
            try
            {

                tran = connection.BeginTransaction();
                sqlcmd.Transaction = tran;

                if (dtArr_Promo_NO_Tape_List.Rows.Count > 0)
                {
                    sqlcmd.CommandText = "SP_I_RPT_TBPGM_ARR_PROMO_NO_TAPE_LIST";
                    //逐筆寫入未到帶列印清單

                    for (int i = 0; i <= dtArr_Promo_NO_Tape_List.Rows.Count - 1; i++)
                    {
                        sqlcmd.Parameters.Clear();
                        SqlParameter para11 = new SqlParameter("@QUERY_KEY", g.ToString());
                        sqlcmd.Parameters.Add(para11);
                        Guid g1;
                        g1 = Guid.NewGuid();
                        SqlParameter para12 = new SqlParameter("@QUERY_KEY_SUBID", g1.ToString());
                        sqlcmd.Parameters.Add(para12);

                        SqlParameter para13 = new SqlParameter("@QUERY_BY", PrintUserName);
                        sqlcmd.Parameters.Add(para13);

                        SqlParameter para14 = new SqlParameter("@播出日期", PrintDate);
                        sqlcmd.Parameters.Add(para14);

                        SqlParameter para15 = new SqlParameter("@頻道", dtArr_Promo_NO_Tape_List.Rows[i]["FSCHANNEL_NAME"]);
                        sqlcmd.Parameters.Add(para15);

                        SqlParameter para16 = new SqlParameter("@宣傳帶編碼", dtArr_Promo_NO_Tape_List.Rows[i]["fspromo_id"]);
                        sqlcmd.Parameters.Add(para16);

                        SqlParameter para17 = new SqlParameter("@宣傳帶名稱", dtArr_Promo_NO_Tape_List.Rows[i]["FSPROMO_NAME"]);
                        sqlcmd.Parameters.Add(para17);

                        SqlParameter para18 = new SqlParameter("@影像編號", dtArr_Promo_NO_Tape_List.Rows[i]["FSFILE_NO"]);
                        sqlcmd.Parameters.Add(para18);

                        SqlParameter para19 = new SqlParameter("@主控編號", dtArr_Promo_NO_Tape_List.Rows[i]["FSVIDEO_PROG"]);
                        sqlcmd.Parameters.Add(para19);
                        string Filestatus = "";
                        if (dtArr_Promo_NO_Tape_List.Rows[i]["FCFILE_STATUS"] == "")
                            Filestatus = "尚未到帶";
                        else if (dtArr_Promo_NO_Tape_List.Rows[i]["FCFILE_STATUS"] == "R")
                            Filestatus = "轉檔中";
                        else if (dtArr_Promo_NO_Tape_List.Rows[i]["FCFILE_STATUS"] == "F")
                            Filestatus = "轉檔失敗";
                        else if (dtArr_Promo_NO_Tape_List.Rows[i]["FCFILE_STATUS"] == "B")
                            Filestatus = "尚未轉檔";

                        SqlParameter para20 = new SqlParameter("@狀態", Filestatus);
                        sqlcmd.Parameters.Add(para20);

                        SqlParameter para21 = new SqlParameter("@建立者", DTRowToString(dtArr_Promo_NO_Tape_List.Rows[i]["FSUSER_ChtName"]));
                        sqlcmd.Parameters.Add(para21);

                        if (sqlcmd.ExecuteNonQuery() == 0)
                        {
                            tran.Rollback();
                            return "";
                        }
                    }
                    tran.Commit();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                StringBuilder sbError = new StringBuilder();
                return ex.Message.ToString();
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }

            String strURL = "";  //要傳回去的URL

            //取列印報表的系統設定檔
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string reportSrv = SettingObj.sysConfig_Read("/ServerConfig/ReportServer");

            strURL = reportSrv + "RPT_TBPGM_ARR_PROMO_NO_TAPE_01" + "&QUERY_KEY=" + g.ToString() + "&rc:parameters=false&rs:Command=Render";
            //strURL _URL = "http://172.20.141.92/ReportServer/Pages/ReportViewer.aspx?%2fPTS_MAM3%2fRPT_TBPROPOSAL_01&QUERY_KEY=A3AE2DED-3451-4AF0-898D-04D1417D01AA&rc:parameters=false&rs:Command=Render";
            return Server.UrlEncode(strURL);
        }

        //取得區間節目播出資訊
        [WebMethod()]
        public string Get_Combine_queue_Prog_List(string InDate, string OutDate, string PrintUserName)
        {
            //strSQL為查詢Store Procesure Name
            //parameters為XML內容(DB_Function_Sample.txt)

            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            StringBuilder sb = new StringBuilder();

            SqlDataAdapter daCombine_Queue_Prog_File = new SqlDataAdapter("SP_Q_GET_PLAYLIST_ZONE_PLAY", connection);
            DataTable dtCombine_Queue_Prog_File = new DataTable();
            SqlParameter para = new SqlParameter("@FDBDATE", InDate);
            SqlParameter para1 = new SqlParameter("@FDEDATE", OutDate);


            daCombine_Queue_Prog_File.SelectCommand.Parameters.Add(para);
            daCombine_Queue_Prog_File.SelectCommand.Parameters.Add(para1);
            daCombine_Queue_Prog_File.SelectCommand.CommandType = CommandType.StoredProcedure;
            if (connection.State == System.Data.ConnectionState.Closed)
            {
                connection.Open();
            }

            daCombine_Queue_Prog_File.Fill(dtCombine_Queue_Prog_File);
            if (dtCombine_Queue_Prog_File.Rows.Count == 0)
            {
                return "沒有節目資料";
            }

            SqlTransaction tran = null;
            SqlCommand sqlcmd = new SqlCommand("SP_I_RPT_TBPGM_COMBINE_QUEUE_ZONE_FILE", connection);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            Guid g;
            g = Guid.NewGuid();
            try
            {

                tran = connection.BeginTransaction();
                sqlcmd.Transaction = tran;

                if (dtCombine_Queue_Prog_File.Rows.Count > 0)
                {
                    sqlcmd.CommandText = "SP_I_RPT_TBPGM_COMBINE_QUEUE_ZONE_FILE";
                    //逐筆寫入未到帶列印清單

                    for (int i = 0; i <= dtCombine_Queue_Prog_File.Rows.Count - 1; i++)
                    {

                        sqlcmd.Parameters.Clear();
                        SqlParameter para11 = new SqlParameter("@QUERY_KEY", g.ToString());
                        sqlcmd.Parameters.Add(para11);
                        Guid g1;
                        g1 = Guid.NewGuid();
                        SqlParameter para12 = new SqlParameter("@QUERY_KEY_SUBID", g1.ToString());
                        sqlcmd.Parameters.Add(para12);

                        SqlParameter para13 = new SqlParameter("@QUERY_BY", PrintUserName);
                        sqlcmd.Parameters.Add(para13);

                        SqlParameter para14 = new SqlParameter("@節目編碼", dtCombine_Queue_Prog_File.Rows[i]["FSPROG_ID"]);
                        sqlcmd.Parameters.Add(para14);

                        SqlParameter para15 = new SqlParameter("@節目集數", dtCombine_Queue_Prog_File.Rows[i]["FNEPISODE"]);
                        sqlcmd.Parameters.Add(para15);

                        SqlParameter para16 = new SqlParameter("@名稱", dtCombine_Queue_Prog_File.Rows[i]["FSPROG_NAME"]);
                        sqlcmd.Parameters.Add(para16);

                        SqlParameter para17 = new SqlParameter("@主控編號", dtCombine_Queue_Prog_File.Rows[i]["FSVIDEO_ID"]);
                        sqlcmd.Parameters.Add(para17);

                        SqlParameter para18 = new SqlParameter("@檔案編號", dtCombine_Queue_Prog_File.Rows[i]["FSFILE_NO"]);
                        sqlcmd.Parameters.Add(para18);

                        SqlParameter para19 = new SqlParameter("@查詢起始日期", InDate);
                        sqlcmd.Parameters.Add(para19);


                        SqlParameter para20 = new SqlParameter("@查詢結束日期", OutDate);
                        sqlcmd.Parameters.Add(para20);


                        if (sqlcmd.ExecuteNonQuery() == 0)
                        {
                            tran.Rollback();
                            return "";
                        }
                    }
                    tran.Commit();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                StringBuilder sbError = new StringBuilder();
                return ex.Message.ToString();
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }

            String strURL = "";  //要傳回去的URL

            //取列印報表的系統設定檔
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string reportSrv = SettingObj.sysConfig_Read("/ServerConfig/ReportServer");

            strURL = reportSrv + "RPT_TBPGM_COMBINE_QUEUE_ZONE_FILE" + "&QUERY_KEY=" + g.ToString() + "&rc:parameters=false&rs:Command=Render";
            //strURL _URL = "http://172.20.141.92/ReportServer/Pages/ReportViewer.aspx?%2fPTS_MAM3%2fRPT_TBPROPOSAL_01&QUERY_KEY=A3AE2DED-3451-4AF0-898D-04D1417D01AA&rc:parameters=false&rs:Command=Render";
            return Server.UrlEncode(strURL);
        }

        //取得宣傳帶未到帶清單
        [WebMethod()]
        public string Get_Key_List(string KeyList, string _Channel, string _date, string _username, string _ChannelName)
        {
            //1.先分析傳入的XML,只有一個欄位FSMEMO
            //將其寫入List中

            List<string> MemoList = new List<string>();

            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(KeyList);
            XmlNode xmlNode = xmldoc.FirstChild;


            for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
            {
                MemoList.Add(xmlNode.ChildNodes[i].FirstChild.Value.ToString());
            }
            //分析MemoList將其加入分拆出+,並且加入UniKeyName中
            List<string> UniKeyName = new List<string>();

            for (int i = 0; i <= MemoList.Count - 1; i++)
            {
                string[] LouthKeys = MemoList[i].Split(new Char[] { '+' });
                foreach (string LouthKey in LouthKeys)
                {
                    string[] LouthKeyType = LouthKey.Split(new Char[] { '#' });
                    if (LouthKeyType[0] != "")
                    {
                        if (LouthKeyType[0].IndexOf("片名") > -1)
                        {
                            if (LouthKeyType[0].IndexOf("(整段)") > -1)
                                LouthKeyType[0] = LouthKeyType[0].Substring(3, (LouthKeyType[0].Length) - 7);
                            else if (LouthKeyType[0].IndexOf("(全程避片頭)") > -1)
                                LouthKeyType[0] = LouthKeyType[0].Substring(3, (LouthKeyType[0].Length) - 10);
                            else
                                LouthKeyType[0] = LouthKeyType[0].Substring(3, (LouthKeyType[0].Length) - 3);
                        }
                        //要檢查是否已經加入UniKeyName清單
                        bool needAddKey = true;
                        for (int j = 0; j <= UniKeyName.Count - 1; j++)
                        {
                            if (UniKeyName[j] == LouthKeyType[0])
                            {
                                needAddKey = false;
                            }
                        }
                        if (needAddKey == true)
                        {
                            UniKeyName.Add(LouthKeyType[0]);
                        }
                    }
                }
            }

            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            StringBuilder sb = new StringBuilder();

            if (connection.State == System.Data.ConnectionState.Closed)
            {
                connection.Open();
            }
            string Errmessage = "";
            SqlTransaction tran = null;
            SqlCommand sqlcmd = new SqlCommand("SP_I_RPT_TBPGM_ARR_PROMO_KeyList", connection);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            Guid g;
            g = Guid.NewGuid();
            try
            {
                tran = connection.BeginTransaction();
                sqlcmd.Transaction = tran;

                if (UniKeyName.Count > 0)
                {
                    sqlcmd.CommandText = "SP_I_RPT_TBPGM_ARR_PROMO_KeyList";
                    //逐筆寫入未到帶列印清單
                    //逐筆依照UniKeyName資料查出Louth的資料並且寫入報表
                    for (int i = 0; i <= UniKeyName.Count - 1; i++)
                    {
                        LouthTableData NewLouth = new LouthTableData();
                        NewLouth = GetLouthData(UniKeyName[i], _Channel);
                        if (NewLouth.FSNO == "")
                            Errmessage = Errmessage + UniKeyName[i] + "不存在     ";
                        else
                        {
                            sqlcmd.Parameters.Clear();
                            SqlParameter para11 = new SqlParameter("@QUERY_KEY", g.ToString());
                            sqlcmd.Parameters.Add(para11);
                            Guid g1;
                            g1 = Guid.NewGuid();
                            SqlParameter para12 = new SqlParameter("@QUERY_KEY_SUBID", g1.ToString());
                            sqlcmd.Parameters.Add(para12);

                            SqlParameter para13 = new SqlParameter("@QUERY_BY", _username);
                            sqlcmd.Parameters.Add(para13);

                            SqlParameter para14 = new SqlParameter("@播出日期", _date);
                            sqlcmd.Parameters.Add(para14);

                            SqlParameter para15 = new SqlParameter("@頻道", _ChannelName);
                            sqlcmd.Parameters.Add(para15);

                            SqlParameter para16 = new SqlParameter("@序號", (i + 1).ToString());
                            sqlcmd.Parameters.Add(para16);

                            SqlParameter para17 = new SqlParameter("@KeyLayer", NewLouth.FSLayel);
                            sqlcmd.Parameters.Add(para17);

                            string FSNO = NewLouth.FSNO.Substring(1, NewLouth.FSNO.Length - 1);
                            int FNNO = int.Parse(FSNO);
                            SqlParameter para18 = new SqlParameter("@KeyNumber", FNNO.ToString());
                            sqlcmd.Parameters.Add(para18);

                            SqlParameter para19 = new SqlParameter("@Title", NewLouth.FSNAME);
                            sqlcmd.Parameters.Add(para19);
                            string Filestatus = "";
                            if (NewLouth.FSNAME.IndexOf("時間") > 0 || NewLouth.FSNAME.IndexOf("重播") > 0 || NewLouth.FSNAME.IndexOf("直播") > 0)
                                Filestatus = "sTRANKEY";
                            else
                                Filestatus = "sKEYER";
                            SqlParameter para20 = new SqlParameter("@ID", Filestatus);
                            sqlcmd.Parameters.Add(para20);

                            if (sqlcmd.ExecuteNonQuery() == 0)
                            {
                                tran.Rollback();
                                return "";
                            }
                        }
                    }

                    tran.Commit();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                StringBuilder sbError = new StringBuilder();
                return ex.Message.ToString();
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }

            String strURL = "";  //要傳回去的URL
            if (Errmessage == "")
            {
                //取列印報表的系統設定檔
                MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
                string reportSrv = SettingObj.sysConfig_Read("/ServerConfig/ReportServer");

                strURL = reportSrv + "RPT_TBPGM_ARR_PROMO_KEY_LIST" + "&QUERY_KEY=" + g.ToString() + "&rc:parameters=false&rs:Command=Render";
                //strURL _URL = "http://172.20.141.92/ReportServer/Pages/ReportViewer.aspx?%2fPTS_MAM3%2fRPT_TBPROPOSAL_01&QUERY_KEY=A3AE2DED-3451-4AF0-898D-04D1417D01AA&rc:parameters=false&rs:Command=Render";
                return Server.UrlEncode(strURL);
            }
            else
                return "ERROR:" + Errmessage;
        }

        //派送檔案
        //20150904 by Mike
        [WebMethod()]
        public String FtpVideo(string Video_ID)
        {
            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            SqlCommand sqlcmd = new SqlCommand("SP_I_PGM_FILE_FTP_DATE", connection);
            try
            {


                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }
                PTS_MAM3.Web.WS.WSPGM.ServiceNasFileMoving obj = new WS.WSPGM.ServiceNasFileMoving();
                obj.RegisterNasFileMovingService(Video_ID);
                ///開始將檔案的派送日期寫入
                SqlParameter para = new SqlParameter("@FSVIDEO_ID", Video_ID);
                sqlcmd.Parameters.Clear();
                sqlcmd.Parameters.Add(para);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.ExecuteNonQuery();
                return "作業完成";
            }
            catch (Exception ex)
            {
                StringBuilder sbError = new StringBuilder();
                sbError.AppendLine("作業錯誤:" + ex.Message);
                
                return sbError.ToString();
            }
            finally
            {
               
                connection.Close();
                connection.Dispose();
                sqlcmd.Dispose();
                GC.Collect();
                GC.Collect();
                
            }
        }

        //派送檔案
        [WebMethod()]
        public String SendMovingFile(string Video_ID_List_XML)
        {
            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            SqlTransaction tran = null;
            SqlCommand sqlcmd = new SqlCommand("SP_I_PGM_FILE_FTP_DATE", connection);
            try
            {
                if (Video_ID_List_XML != "")
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(Video_ID_List_XML);
                    XmlNode xmlNode = xmldoc.FirstChild;


                    if (connection.State == System.Data.ConnectionState.Closed)
                    {
                        connection.Open();
                    }

                    tran = connection.BeginTransaction();
                    sqlcmd.Transaction = tran;

                    for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                    {
                        for (int j = 0; j <= xmlNode.ChildNodes[i].ChildNodes.Count - 1; j++)
                        {
                            PTS_MAM3.Web.WS.WSPGM.ServiceNasFileMoving obj = new WS.WSPGM.ServiceNasFileMoving();
                            obj.RegisterNasFileMovingService(xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value);

                            ///開始將檔案的派送日期寫入
                            SqlParameter para = new SqlParameter("@FSVIDEO_ID", xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value);
                            sqlcmd.Parameters.Clear();
                            sqlcmd.Parameters.Add(para);
                            sqlcmd.CommandType = CommandType.StoredProcedure;
                            sqlcmd.ExecuteNonQuery();
                        }

                    }
                }
                return "作業完成";
            }
            catch (Exception ex)
            {
                StringBuilder sbError = new StringBuilder();
                sbError.AppendLine("作業錯誤:" + ex.Message);
                tran.Rollback();
                return sbError.ToString();
            }
            finally
            {
                tran.Commit();
                connection.Close();
                connection.Dispose();
                sqlcmd.Dispose();
                GC.Collect();
                GC.Collect();
            }

        }

        [WebMethod()]
        //取得HD檔案的狀態
        public string getHD_VideoID_Status(string VideoID)
        {
            //DataTable dtProgSeg = new DataTable();
            //每一個節目,先查詢節目段落看是否有資料
            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection1 = new SqlConnection(sqlConnStr);
            if (connection1.State == System.Data.ConnectionState.Closed)
            {
                connection1.Open();
            }
            StringBuilder sb = new StringBuilder();
            SqlDataAdapter daVideoIDStatus = new SqlDataAdapter("SP_Q_GET_HD_VIDEO_STATUS", connection1);
            DataTable dtVideoIDStatus = new DataTable();
            SqlParameter paraVIDEOID = new SqlParameter("@FSVIDEO_ID", VideoID);
            daVideoIDStatus.SelectCommand.Parameters.Add(paraVIDEOID);

            daVideoIDStatus.SelectCommand.CommandType = CommandType.StoredProcedure;
            daVideoIDStatus.Fill(dtVideoIDStatus);
            connection1.Close();
            string ReturnString="";
            if (dtVideoIDStatus.Rows.Count > 0)
            {
                if (dtVideoIDStatus.Rows[0]["FCCHECK_STATUS"].ToString() == "")
                    ReturnString = "_";
                else
                    ReturnString = dtVideoIDStatus.Rows[0]["FCCHECK_STATUS"].ToString();

                if (dtVideoIDStatus.Rows[0]["FCFILE_STATUS"].ToString() == "")
                    ReturnString = ReturnString + "_";
                else
                    ReturnString = ReturnString + dtVideoIDStatus.Rows[0]["FCFILE_STATUS"].ToString();

                if (dtVideoIDStatus.Rows[0]["FCSTATUS"].ToString() == "")
                    ReturnString = ReturnString + "_";
                else
                    ReturnString = ReturnString + dtVideoIDStatus.Rows[0]["FCSTATUS"].ToString();
                return ReturnString;
                //if (dtVideoIDStatus.Rows[0]["FCSTATUS"].ToString() == "S") //TBLOG_Video_HD_MC 
                //    return "PTS-AP上傳中";
                //else if (dtVideoIDStatus.Rows[0]["FCSTATUS"].ToString() == "C") //TBLOG_Video_HD_MC 
                //    return "PTS-AP上傳完成";
                //else if (dtVideoIDStatus.Rows[0]["FCSTATUS"].ToString() == "E") //TBLOG_Video_HD_MC 
                //    return "PTS-AP上傳錯誤";
                //else if (dtVideoIDStatus.Rows[0]["FCSTATUS"].ToString() == "N") //TBLOG_Video_HD_MC 
                //    return "尚未審核";
                //else if (dtVideoIDStatus.Rows[0]["FCSTATUS"].ToString() == "X") //TBLOG_Video_HD_MC 
                //    return "審核不通過";
                //else if (dtVideoIDStatus.Rows[0]["FCSTATUS"].ToString() == "O") //TBLOG_Video_HD_MC 
                //{
                //    if (dtVideoIDStatus.Rows[0]["FCFILE_STATUS"].ToString() == "B") //TBLOG_Video
                //        return "審核通過";
                //    else if (dtVideoIDStatus.Rows[0]["FCFILE_STATUS"].ToString() == "S") //TBLOG_Video
                //        return "低解轉檔中";
                //    else if (dtVideoIDStatus.Rows[0]["FCFILE_STATUS"].ToString() == "R") //TBLOG_Video
                //        return "低解轉檔失敗";
                //    else if ((dtVideoIDStatus.Rows[0]["FCFILE_STATUS"].ToString() == "T") || (dtVideoIDStatus.Rows[0]["FCFILE_STATUS"].ToString() == "Y"))//TBLOG_Video
                //        return "HD低解轉檔完成";
                //    else
                //        return "審核通過但檔案狀態未知:" + dtVideoIDStatus.Rows[0]["FCFILE_STATUS"].ToString();
                //}
                //else
                //{


                //    if (dtVideoIDStatus.Rows[0]["FCCHECK_STATUS"].ToString() == "N")  //tbbroadcast
                //        return "新增HD送播單";
                //    else if (dtVideoIDStatus.Rows[0]["FCCHECK_STATUS"].ToString() == "U")  //tbbroadcast
                //        return "USER勾選已上傳";
                //    else if (dtVideoIDStatus.Rows[0]["FCCHECK_STATUS"].ToString() == "R")  //tbbroadcast
                //        return "後製退單";
                //    else if (dtVideoIDStatus.Rows[0]["FCCHECK_STATUS"].ToString() == "E")  //tbbroadcast
                //        return "節目單位抽單";
                //    else if (dtVideoIDStatus.Rows[0]["FCCHECK_STATUS"].ToString() == "X")  //tbbroadcast
                //        return "送帶轉檔抽單";
                //    else if ((dtVideoIDStatus.Rows[0]["FCCHECK_STATUS"].ToString() == "G") || (dtVideoIDStatus.Rows[0]["FCCHECK_STATUS"].ToString() == "T"))  //TBLOG_Video
                //    {
                //        if (dtVideoIDStatus.Rows[0]["FCFILE_STATUS"].ToString() == "B") //TBLOG_Video
                //            return "新增送帶轉檔/回溯單";
                //        else if (dtVideoIDStatus.Rows[0]["FCFILE_STATUS"].ToString() == "S") //TBLOG_Video
                //            return "送帶轉檔/回溯轉檔中";
                //        else if (dtVideoIDStatus.Rows[0]["FCFILE_STATUS"].ToString() == "R") //TBLOG_Video
                //            return "送帶轉檔/回溯轉檔失敗";
                //        else if ((dtVideoIDStatus.Rows[0]["FCFILE_STATUS"].ToString() == "T") || (dtVideoIDStatus.Rows[0]["FCFILE_STATUS"].ToString() == "Y"))//TBLOG_Video
                //            return "送帶轉檔/回溯轉檔完成";
                //        else
                //            return "送帶轉檔通過但檔案狀態未知:" + dtVideoIDStatus.Rows[0]["FCFILE_STATUS"].ToString();
                //    }
                //    else
                //    {
                //        return "檔案狀態未知:" + dtVideoIDStatus.Rows[0]["FCFILE_STATUS"].ToString();
                //    }
                //}
               
                
            }
            else //如果查無資料
            {
                return "___"; //表示查無資料
                //return "檔案資料不存在"; //表示查無資料
            }
        }

        //取得HD檔案的VideoID檔案狀態
        [WebMethod()]
        public string GetHD_List_VideoID_Status( List<ArrPromoList> InList)
        {
            //AlwaysSendXML=True 表示強制派送功能
            if (InList != null)
            {
                PGM_ARR_PROMOAddSOM.Clear();
                foreach (ArrPromoList TemArrPromoList in InList)
                {
                    PGM_ARR_PROMOAddSOM.Add(AddSOMToArrPromoList(TemArrPromoList));
                }
                try
                {
                    //要先建立一個獨立清單,不包含重複資料
                    #region 要先建立一個獨立清單,不包含重複資料
                    List<ArrPromoListAddSOM> UniqueList = new List<ArrPromoListAddSOM>();
                    foreach (ArrPromoListAddSOM TemArrPromoList in PGM_ARR_PROMOAddSOM)
                    {
                        //先檢查是否有加入過清單
                        bool NeedAdd = false;
                        foreach (ArrPromoListAddSOM TempUniqueList in UniqueList)
                        {

                            if (TemArrPromoList.FSVIDEO_ID == TempUniqueList.FSVIDEO_ID)
                            {
                                NeedAdd = true;
                            }
                        }
                        if (NeedAdd == false) //沒被加入過清單
                        {
                            UniqueList.Add(TemArrPromoList);
                        }
                    }
                    #endregion

                    //查詢設定值
                    //String sqlConnStr = "";
                    //sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
                    //SqlConnection connection1 = new SqlConnection(sqlConnStr);
                    //if (connection1.State == System.Data.ConnectionState.Closed)
                    //{
                    //    connection1.Open();
                    //}

                    //SqlDataAdapter daPGM_Setting = new SqlDataAdapter("SP_Q_TBPGM_SETTING", connection1);
                    //DataTable dtPGM_Setting = new DataTable();


                    //daPGM_Setting.SelectCommand.CommandType = CommandType.StoredProcedure;
                    //daPGM_Setting.Fill(dtPGM_Setting);
                    //connection1.Close();
                    //string PendingPath = ""; //待審區路徑
                    //string CompletePath = "";  //帶播區路徑
                    ////string PendingPath = "D:\\Test\\PTS\\PendingZone\\"; //待審區路徑
                    ////string CompletePath = "D:\\Test\\PTS\\CompleteZone\\";  //帶播區路徑

                    MAM_PTS_DLL.SysConfig obj = new MAM_PTS_DLL.SysConfig();
                    string PendingPath = obj.sysConfig_Read("/ServerConfig/PGM_Config/PendingPath");

                    string CompletePath = obj.sysConfig_Read("/ServerConfig/PGM_Config/CompletePath");
                    //if (dtPGM_Setting.Rows.Count > 0)
                    //{
                    //    for (int i = 0; i <= dtPGM_Setting.Rows.Count - 1; i++)
                    //    {
                    //        if (dtPGM_Setting.Rows[i]["FSTYPE"].ToString().ToUpper() == "PENDINGPATH")
                    //            PendingPath = dtPGM_Setting.Rows[i]["FSNAME"].ToString();
                    //        if (dtPGM_Setting.Rows[i]["FSTYPE"].ToString().ToUpper() == "COMPLETEPATH")
                    //            CompletePath = dtPGM_Setting.Rows[i]["FSNAME"].ToString();     
                    //    }

                

                    //}



                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("<Datas>");
                    #region 逐一查詢狀態並寫回狀態
                    ImpersonateUser iu = new ImpersonateUser();
                    iu.Impersonate(@"pts.org.tw", @"mamsystem", @"p@ssw0rd");
                    foreach (ArrPromoListAddSOM TemArrPromoList in UniqueList)
                    {
                        //20120328 By Mike測試隱藏
                        //if (TemArrPromoList.FSFILE_NO != "" && TemArrPromoList.FBREALTIMECODE != "F")
                        //{
                        string video_Status = "";
                        if (File.Exists(CompletePath + TemArrPromoList.FSVIDEO_ID.Substring(2, 6) + ".mxf") == true)
                            video_Status = "檔案已到待播區";
                        else if (File.Exists(PendingPath + TemArrPromoList.FSVIDEO_ID.Substring(2, 6) + ".mxf") == true)
                            video_Status = "檔案已到待審區";
                        else
                            video_Status = getHD_VideoID_Status(TemArrPromoList.FSVIDEO_ID);

                        //if (File.Exists(HttpContext.Current.Server.MapPath("~/HDFILEPATH/Approved/" + TemArrPromoList.FSVIDEO_ID.Substring(2, 6) + ".mxf")) == true)
                        //    video_Status = "檔案已到待播區";
                        //else if (File.Exists(HttpContext.Current.Server.MapPath("~/HDFILEPATH/Review/" + TemArrPromoList.FSVIDEO_ID.Substring(2, 6) + ".mxf")) == true)
                        //    video_Status = "檔案已到待審區";
                        //else
                        //    video_Status = getHD_VideoID_Status(TemArrPromoList.FSVIDEO_ID);

                            //if (System.IO.File.Exists(CompletePath + TemArrPromoList.FSVIDEO_ID.Substring(2,6) + ".mxf") == true)
                            //    video_Status = "檔案已到待播區";
                            //else if (System.IO.File.Exists(PendingPath + TemArrPromoList.FSVIDEO_ID.Substring(2, 6) + ".mxf") == true)
                            //    video_Status = "檔案已到待審區";
                            //else
                            //    video_Status = getHD_VideoID_Status(TemArrPromoList.FSVIDEO_ID);
                            sb.AppendLine("<Data>");
                            sb.AppendLine("<FSVIDEO_ID>" + TemArrPromoList.FSVIDEO_ID + "</FSVIDEO_ID>");

                            sb.AppendLine("<FSSTATUS>" + video_Status + "</FSSTATUS>");
                            sb.AppendLine("</Data>");

                        //}
                        //else 
                        //{
                        //    sb.AppendLine("<Data>");
                        //    sb.AppendLine("<FSVIDEO_ID>" + TemArrPromoList.FSVIDEO_ID + "</FSVIDEO_ID>");

                        //    sb.AppendLine("<FSSTATUS>" + "尚未有資料" + "</FSSTATUS>");
                        //    sb.AppendLine("</Data>");
                        //}
                    }
                    #endregion
                    iu.Undo();
                    sb.AppendLine("</Datas>");

                    return sb.ToString();
                }
                catch
                {
                    return "Error:" + "取得檔案狀態失敗";
                }

              
            }
            else
                return "Error:" + "沒有要取得狀態的資料";

            

        }

        //派送檔案並取得VideoID檔案狀態
        [WebMethod()]
        public string GetVideoISStatus(string Video_ID_List_XML, List<ArrPromoList> InList, Boolean AlwaysSendXML)
        {
            //AlwaysSendXML=True 表示強制派送功能
            if (InList != null)
            {


                PGM_ARR_PROMOAddSOM.Clear();
                foreach (ArrPromoList TemArrPromoList in InList)
                {
                    PGM_ARR_PROMOAddSOM.Add(AddSOMToArrPromoList(TemArrPromoList));
                }
                string TempChannelID = "";
                string TempDate = "";
                #region 開始建立檔案,給維智將資料寫入Louth DB使用

                    string DateString = "";
                    DateString = DateString + DateTime.Now.Year + String.Format("{0:00}", DateTime.Now.Month) + String.Format("{0:00}", DateTime.Now.Day) + "-" + String.Format("{0:00}", DateTime.Now.Hour) + String.Format("{0:00}", DateTime.Now.Minute) + String.Format("{0:00}", DateTime.Now.Second);

                    string InsDateString = "";

                    InsDateString = InsDateString + DateTime.Now.Year + "-" + String.Format("{0:00}", DateTime.Now.Month) + "-" + String.Format("{0:00}", DateTime.Now.Day) + " " + String.Format("{0:00}", DateTime.Now.Hour) + ":" + String.Format("{0:00}", DateTime.Now.Minute) + ":" + String.Format("{0:00}", DateTime.Now.Second) + "." + String.Format("{0:000}", DateTime.Now.Millisecond);

                    //開啟建立檔案,給維智將資料寫入Louth DB使用

                    //RealPath @"\\10.13.200.1\MasterCtrl\MSG\LOUTH\InputXML\"
                    //string TestPath = @"\\10.13.220.42\Project\Test\";
                    //string TestPath = @"D:\TempFile\";
                    string TestPath = @"\\10.13.200.1\MasterCtrl\MSG\LOUTH\InputXML\";
                    if (File.Exists(TestPath + DateString + ".xml") == true)
                        File.Delete(TestPath + DateString + ".xml");
                    StringBuilder sb = new StringBuilder();
                    //FileStream myFile = File.Open(@"C:\0Selfsys\1234.lst", FileMode.Open, FileAccess.ReadWrite);
                    FileStream myFile = File.Create(TestPath + DateString + ".xml");
                    //FileStream fs = new FileStream(@"c:\sw.txt", FileMode.Create, FileAccess.Write);
                    //StreamWriter m_streamWriter = new StreamWriter(fs);
                    //// Write to the file using StreamWriter class
                    StreamWriter m_streamWriter = new StreamWriter(myFile);
                    try
                    {
                    //BinaryWriter myWriter = new BinaryWriter(myFile);
                    //int WorkRow = 0;
                    sb.AppendLine("<Datas>");
                    int TempI = 1;
                    //要先建立一個獨立清單,不包含重複資料
                    #region 要先建立一個獨立清單,不包含重複資料
                    List<ArrPromoListAddSOM> UniqueList = new List<ArrPromoListAddSOM>();
                    foreach (ArrPromoListAddSOM TemArrPromoList in PGM_ARR_PROMOAddSOM)
                    {
                        if (TemArrPromoList.FDDATE != "" && TemArrPromoList.FDDATE != null)
                        {
                            TempChannelID = TemArrPromoList.FSCHANNEL_ID;
                            TempDate = TemArrPromoList.FDDATE;
                        }
                        //先檢查是否有加入過清單
                        bool NeedAdd = false;
                        foreach (ArrPromoListAddSOM TempUniqueList in UniqueList)
                        {
                            if (TemArrPromoList.FCTYPE == "G") //節目則要取段落,才表示唯一
                            {
                                if (TemArrPromoList.FSVIDEO_ID == TempUniqueList.FSVIDEO_ID && TemArrPromoList.FNBREAK_NO == TempUniqueList.FNBREAK_NO)
                                {
                                    NeedAdd = true;
                                }
                            }
                            else //Promo則不需要取段落表示唯一
                            {
                                if (TemArrPromoList.FSVIDEO_ID == TempUniqueList.FSVIDEO_ID)
                                {
                                    NeedAdd = true;
                                }
                            }
                        }
                        if (NeedAdd == false) //沒被加入過清單
                        {
                            UniqueList.Add(TemArrPromoList);
                        }
                    }
                    #endregion


                    #region 逐一寫入文字檔案


                    foreach (ArrPromoListAddSOM TemArrPromoList in UniqueList)
                    {
                        //20120328 By Mike測試隱藏
                        if (TemArrPromoList.FSFILE_NO != "" && TemArrPromoList.FBREALTIMECODE != "F")
                        {


                            if (TemArrPromoList.FCTYPE == "P") //Promo
                            {
                                PTS_MAM3.Web.WS.WSPGM.ServicePGM obj = new WS.WSPGM.ServicePGM();
                                WS.WSPGM.ServicePGM.MSS_FILE_STATUS status = obj.GetFileStatus_MSS(TemArrPromoList.FSVIDEO_ID);
                                //if (status.ToString() == "FTP1ERR_FTP2NOTSTART" || status.ToString() == "FTP1ERR_FTP2NOSTART" ||  status.ToString() == "MSSCOPY_OK" || status.ToString() == "MSSPROCESS_OK" || status.ToString() == "SYSERROR")

                                if (status.ToString() == "MSSCOPY_ERR" || status.ToString() == "MSSPROCESS_OK" || status.ToString() == "SYSERROR" || AlwaysSendXML == true)
                                {
                                    //要先查詢否與實際檔案長度(TBLOG_VIDEO_SEG.FSVIDEO_ID FSBEG_TIMECODE,FSEND_TIMECODE 相減 相同
                                    //#region 檢查 Promo 實際長度,如果相同在寫入.不用比對了,因為之前AddSOMToArrPromoList比對過
                                    //int PromoLength;
                                    //PromoLength = getPromoSegmentLength(TemArrPromoList.FSVIDEO_ID, "Dur","");
                                    //#endregion
                                    ////比對 Promo 實際長度,如果相同再寫
                                    //if (PromoLength == MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION))
                                    //{
                                        sb.AppendLine("<Data>");
                                        sb.AppendLine("<TableName>ASDB</TableName>");
                                        sb.AppendLine("<Type>s</Type>");

                                        sb.AppendLine("<Identifier>" + TemArrPromoList.FSVIDEO_ID + "</Identifier>");
                                        //sb.AppendLine("<Identifier>" + String.Format("{0:00000000}", TempI) + "</Identifier>");

                                        sb.AppendLine("<Title>" + ReplaceXML(GetSubString(TemArrPromoList.FSNAME, 16)) + "</Title>");
                                        TempI = TempI + 1;
                                        sb.AppendLine("<Operator>" + "MAM" + "</Operator>");

                                        sb.AppendLine("<StartOfMessage>" + TimecodeToLouthTimecode(TemArrPromoList.FSSOM) + "</StartOfMessage>");
                                        sb.AppendLine("<Duration>" + TimecodeToLouthTimecode(TemArrPromoList.FSDURATION) + "</Duration>");
                                        sb.AppendLine("<LabelType>" + "0" + "</LabelType>");
                                        sb.AppendLine("<VideoFormat>" + "4" + "</VideoFormat>");
                                        sb.AppendLine("<AudioFormat>" + "1" + "</AudioFormat>");
                                        sb.AppendLine("<VideoQuality>" + "0" + "</VideoQuality>");
                                        sb.AppendLine("<MadeDateTime>" + InsDateString + "</MadeDateTime>");
                                        sb.AppendLine("<AirDateTime>" + InsDateString + "</AirDateTime>");
                                        sb.AppendLine("<PurgeDateTime>" + InsDateString + "</PurgeDateTime>");
                                        sb.AppendLine("<UserString>" + GetPromoWELFARE(TemArrPromoList.FSPROMO_ID) + "</UserString>");
                                        sb.AppendLine("</Data>");
                                        //20150904 by Mike


                                        FtpVideo(TemArrPromoList.FSVIDEO_ID);
                                    //}
                                    //else //有些宣傳帶的實際檔案長度與運行表上的不同,中止作業
                                    //{
                                    //    sb.AppendLine("</Datas>");
                                    //    m_streamWriter.Write(sb.ToString());
                                    //    m_streamWriter.Flush();
                                    //    m_streamWriter.Close();
                                    //    m_streamWriter.Dispose();
                                    //    myFile.Close();
                                    //    myFile.Dispose();
                                    //    if (File.Exists(TestPath + DateString + ".xml") == true)
                                    //        File.Delete(TestPath + DateString + ".xml");

                                    //    return "Error:短帶:" + TemArrPromoList.FSNAME + "的長度與實際長度不符合,請執行比對段落資料修正長度";
                                    //}
                                }
                            }
                            else //Prog
                            {
                                PTS_MAM3.Web.WS.WSPGM.ServicePGM obj = new WS.WSPGM.ServicePGM();
                                WS.WSPGM.ServicePGM.MSS_FILE_STATUS status = obj.GetFileStatus_MSS(TemArrPromoList.FSVIDEO_ID + int.Parse(TemArrPromoList.FNBREAK_NO).ToString("00"));
                                //if (status.ToString() == "FTP1ERR_FTP2NOTSTART" || status.ToString() == "FTP1ERR_FTP2NOSTART" || status.ToString() == "MSSCOPY_OK" || status.ToString() == "MSSPROCESS_OK" || status.ToString() == "SYSERROR")

                                if (status.ToString() == "MSSCOPY_ERR" || status.ToString() == "MSSPROCESS_OK" || status.ToString() == "SYSERROR" || AlwaysSendXML == true)
                                {
                                    if (TemArrPromoList.FNBREAK_NO == "1")  //第一段要先加入ASDB,每段要加ASSEG
                                    {
                                        int dur = 0;
                                        foreach (ArrPromoListAddSOM AAA in PGM_ARR_PROMOAddSOM)
                                        {
                                            if (AAA.FSPROG_ID == TemArrPromoList.FSPROG_ID && AAA.FCTYPE == "G" && AAA.FNSEQNO == TemArrPromoList.FNSEQNO)
                                            {
                                                dur = dur + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(AAA.FSDURATION);
                                            }
                                        }

                                        sb.AppendLine("<Data>");
                                        sb.AppendLine("<TableName>ASDB</TableName>");
                                        sb.AppendLine("<Type>m</Type>");


                                        sb.AppendLine("<Identifier>" + TemArrPromoList.FSVIDEO_ID + "</Identifier>");
                                        //sb.AppendLine("<Identifier>" + String.Format("{0:00000000}", TempI) + "</Identifier>");



                                        //節目 Title 要加入集數
                                        if (TemArrPromoList.FNEPISODE != "" && TemArrPromoList.FNEPISODE != null)
                                            sb.AppendLine("<Title>" + ReplaceXML(GetSubString(TemArrPromoList.FSNAME, 16 - (TemArrPromoList.FNEPISODE.Length + 1)) + "#" + TemArrPromoList.FNEPISODE) + "</Title>");
                                        else
                                            sb.AppendLine("<Title>" + ReplaceXML(GetSubString(TemArrPromoList.FSNAME, 16)) + "</Title>");


                                        TempI = TempI + 1;
                                        sb.AppendLine("<Operator>" + "MAM" + "</Operator>");

                                        sb.AppendLine("<StartOfMessage>" + TimecodeToLouthTimecode(TemArrPromoList.FSSOM) + "</StartOfMessage>");

                                        //sb.AppendLine("<StartOfMessage>" + "0" + "</StartOfMessage>");
                                        sb.AppendLine("<Duration>" + TimecodeToLouthTimecode(MAM_PTS_DLL.TimeCodeCalc.frame2timecode(dur)) + "</Duration>");
                                        sb.AppendLine("<LabelType>" + "0" + "</LabelType>");
                                        sb.AppendLine("<VideoFormat>" + "4" + "</VideoFormat>");
                                        sb.AppendLine("<AudioFormat>" + "1" + "</AudioFormat>");
                                        sb.AppendLine("<VideoQuality>" + "0" + "</VideoQuality>");
                                        sb.AppendLine("<MadeDateTime>" + InsDateString + "</MadeDateTime>");
                                        sb.AppendLine("<AirDateTime>" + InsDateString + "</AirDateTime>");
                                        sb.AppendLine("<PurgeDateTime>" + InsDateString + "</PurgeDateTime>");
                                        sb.AppendLine("<UserString>" + "" + "</UserString>");
                                        sb.AppendLine("</Data>");

                                    }
                                    sb.AppendLine("<Data>");
                                    sb.AppendLine("<TableName>ASSEG</TableName>");
                                    sb.AppendLine("<Type>m</Type>");
                                    sb.AppendLine("<SegNum>" + TemArrPromoList.FNBREAK_NO + "</SegNum>");
                                    sb.AppendLine("<Identifier>" + TemArrPromoList.FSVIDEO_ID + "</Identifier>");
                                    //sb.AppendLine("<Identifier>" + String.Format("{0:00000000}", TempI) + "</Identifier>");

                                    //節目 Title 要加入集數
                                    if (TemArrPromoList.FNEPISODE != "" && TemArrPromoList.FNEPISODE != null)
                                        sb.AppendLine("<Title>" + ReplaceXML(GetSubString(TemArrPromoList.FSNAME, 16 - (TemArrPromoList.FNEPISODE.Length + 1)) + "#" + TemArrPromoList.FNEPISODE) + "</Title>");
                                    else
                                        sb.AppendLine("<Title>" + ReplaceXML(GetSubString(TemArrPromoList.FSNAME, 16)) + "</Title>");


                                    TempI = TempI + 1;
                                    sb.AppendLine("<StartOfMessage>" + TimecodeToLouthTimecode(TemArrPromoList.FSSOM) + "</StartOfMessage>");
                                    sb.AppendLine("<Duration>" + TimecodeToLouthTimecode(TemArrPromoList.FSDURATION) + "</Duration>");
                                    sb.AppendLine("<NoteDateTime>" + InsDateString + "</NoteDateTime>");
                                    sb.AppendLine("</Data>");
                                    //20150904 by Mike
                                    FtpVideo(TemArrPromoList.FSVIDEO_ID + int.Parse(TemArrPromoList.FNBREAK_NO).ToString("00"));
                                }
                            }
                        }
                    }
                    #endregion



                    sb.AppendLine("</Datas>");
                    m_streamWriter.Write(sb.ToString());
                    m_streamWriter.Flush();
                    m_streamWriter.Close();
                    m_streamWriter.Dispose();
                    myFile.Close();
                    myFile.Dispose();
                    if (File.Exists(TestPath + DateString + ".xml.pgmok") == true)
                        File.Delete(TestPath + DateString + ".xml.pgmok");
                    //FileStream myFile = File.Open(@"C:\0Selfsys\1234.lst", FileMode.Open, FileAccess.ReadWrite);
                    FileStream myFile1 = File.Create(TestPath + DateString + ".xml.pgmok");
                    StreamWriter m_streamWriter1 = new StreamWriter(myFile1);
                    m_streamWriter1.Write("");
                    m_streamWriter1.Flush();
                    m_streamWriter1.Close();
                    m_streamWriter1.Dispose();
                    myFile1.Close();
                    myFile1.Dispose();
                }
                catch
                {
                    sb.AppendLine("</Datas>");
                    m_streamWriter.Write(sb.ToString());
                    m_streamWriter.Flush();
                    m_streamWriter.Close();
                    m_streamWriter.Dispose();
                    myFile.Close();
                    myFile.Dispose();
                    if (File.Exists(TestPath + DateString + ".xml.pgmok") == true)
                        File.Delete(TestPath + DateString + ".xml.pgmok");
                    //FileStream myFile = File.Open(@"C:\0Selfsys\1234.lst", FileMode.Open, FileAccess.ReadWrite);
                    FileStream myFile1 = File.Create(TestPath + DateString + ".xml.pgmok");
                    StreamWriter m_streamWriter1 = new StreamWriter(myFile1);
                    m_streamWriter1.Write("");
                    m_streamWriter1.Flush();
                    m_streamWriter1.Close();
                    m_streamWriter1.Dispose();
                    myFile1.Close();
                    myFile1.Dispose();
                }

                #endregion
            }

            //return "Error";
            try
            {
                StringBuilder sb = new StringBuilder();
                if (Video_ID_List_XML != "")
                {
                    sb.AppendLine("<Datas>");
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(Video_ID_List_XML);
                    XmlNode xmlNode = xmldoc.FirstChild;
                    //ImpersonateUser iu = new ImpersonateUser();
                    //iu.Impersonate(@"pts.org.tw", @"mamsystem", @"p@ssw0rd");
                    for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                    {

                        for (int j = 0; j <= xmlNode.ChildNodes[i].ChildNodes.Count - 1; j++)
                        {
                            string InVideoID = xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value;
                            PTS_MAM3.Web.WS.WSPGM.ServicePGM obj = new WS.WSPGM.ServicePGM();
                            WS.WSPGM.ServicePGM.MSS_FILE_STATUS status = obj.GetFileStatus_MSS(InVideoID);
                            sb.AppendLine("<Data>");
                            sb.AppendLine("<VideoID>" + InVideoID + "</VideoID>");
                            sb.AppendLine("<Status>" + status.ToString() + "</Status>");
                            sb.AppendLine("</Data>");
                            switch (status)
                            {
                                //case WS.WSPGM.ServicePGM.MSS_FILE_STATUS.
                            }
                        }

                    }
                    //iu.Undo();
                    sb.AppendLine("</Datas>");
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                StringBuilder sbError = new StringBuilder();
                sbError.AppendLine("作業錯誤:" + ex.Message);
                return sbError.ToString();
            }
            finally
            {
                GC.Collect();
            }

        }


        //取得VideoID檔案狀態
        [WebMethod()]
        public string GetVideoIDStatus(string Video_ID_List_XML)
        {
            //逐一取得VideoID的檔案狀態並回傳XML
            try
            {
                StringBuilder sb = new StringBuilder();
                if (Video_ID_List_XML != "")
                {
                    sb.AppendLine("<Datas>");
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(Video_ID_List_XML);
                    XmlNode xmlNode = xmldoc.FirstChild;
                    for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                    {

                        for (int j = 0; j <= xmlNode.ChildNodes[i].ChildNodes.Count - 1; j++)
                        {
                            string InVideoID = xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value;
                            PTS_MAM3.Web.WS.WSPGM.ServicePGM obj = new WS.WSPGM.ServicePGM();
                            WS.WSPGM.ServicePGM.MSS_FILE_STATUS status = obj.GetFileStatus_MSS(InVideoID);
                            sb.AppendLine("<Data>");
                            sb.AppendLine("<VideoID>" + InVideoID + "</VideoID>");
                            sb.AppendLine("<Status>" + status.ToString() + "</Status>");
                            sb.AppendLine("</Data>");
                            switch (status)
                            {
                                //case WS.WSPGM.ServicePGM.MSS_FILE_STATUS.
                            }
                        }

                    }
                    sb.AppendLine("</Datas>");
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                StringBuilder sbError = new StringBuilder();
                sbError.AppendLine("作業錯誤:" + ex.Message);
                return sbError.ToString();
            }
            finally
            {
                GC.Collect();
            }

        }


        //派送檔案
        [WebMethod()]
        public String SendFileAndWriteDBToMC(string Video_ID_List_XML, List<ArrPromoList> InList)
        {
            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            SqlTransaction tran = null;
            SqlCommand sqlcmd = new SqlCommand("SP_I_PGM_FILE_FTP_DATE", connection);
            try
            {
                if (Video_ID_List_XML != "")
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(Video_ID_List_XML);
                    XmlNode xmlNode = xmldoc.FirstChild;


                    if (connection.State == System.Data.ConnectionState.Closed)
                    {
                        connection.Open();
                    }

                    tran = connection.BeginTransaction();
                    sqlcmd.Transaction = tran;

                    for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                    {
                        for (int j = 0; j <= xmlNode.ChildNodes[i].ChildNodes.Count - 1; j++)
                        {
                            PTS_MAM3.Web.WS.WSPGM.ServiceNasFileMoving obj = new WS.WSPGM.ServiceNasFileMoving();
                            obj.RegisterNasFileMovingService(xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value);

                            ///開始將檔案的派送日期寫入
                            SqlParameter para = new SqlParameter("@FSVIDEO_ID", xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value);
                            sqlcmd.Parameters.Clear();
                            sqlcmd.Parameters.Add(para);
                            sqlcmd.CommandType = CommandType.StoredProcedure;
                            sqlcmd.ExecuteNonQuery();
                        }

                    }
                }
                return "作業完成";
            }
            catch (Exception ex)
            {
                StringBuilder sbError = new StringBuilder();
                sbError.AppendLine("作業錯誤:" + ex.Message);
                tran.Rollback();
                return sbError.ToString();
            }
            finally
            {
                tran.Commit();
                connection.Close();
                connection.Dispose();
                sqlcmd.Dispose();
                GC.Collect();
                GC.Collect();
            }

        }
        //查詢宣傳帶可排播時間
        [WebMethod()]
        public string Do_Query_Promo_Plan_Set(string strSQL, string parameters = null)
        {
            //strSQL為查詢Store Procesure Name
            //parameters為XML內容(DB_Function_Sample.txt)
            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;

            StringBuilder sb = new StringBuilder();
            DataTable dt = new DataTable();
            //SqlCommand sqlcmd = new SqlCommand(strSQL, connection);
            sb.AppendLine("<Datas>");

            if (parameters != "")
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(parameters);
                XmlNode xmlNode = xmldoc.FirstChild;
                //sqlcmd.CommandText = "SP_Q_TBLOG_VIDEO_BY_ID_EPISODE";
                for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                {
                    SqlConnection connection = new SqlConnection(sqlConnStr);
                    SqlDataAdapter da = new SqlDataAdapter(strSQL, connection);
                    //sqlcmd.Parameters.Clear();

                    da.SelectCommand.Parameters.Clear();
                    for (int j = 0; j <= xmlNode.ChildNodes[i].ChildNodes.Count - 1; j++)
                    {
                        if (xmlNode.ChildNodes[i].ChildNodes[j].FirstChild == null || xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value == "")
                        {
                            SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, "");
                            da.SelectCommand.Parameters.Add(para);
                        }
                        else
                        {
                            SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value);
                            da.SelectCommand.Parameters.Add(para);
                        }

                        //SqlParameter para = new SqlParameter("@" + xmlNode.ChildNodes[i].ChildNodes[j].Name, xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value);
                        //da.SelectCommand.Parameters.Add(para);
                    }
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;

                    try
                    {
                        if (connection.State == System.Data.ConnectionState.Closed)
                        {
                            connection.Open();
                        }
                        dt.Clear();
                        da.Fill(dt);

                        if (dt.Rows.Count > 0)
                        {
                            for (int m = 0; m <= dt.Rows.Count - 1; m++)
                            {
                                sb.AppendLine("<Data>");
                                for (int n = 0; n <= dt.Columns.Count - 1; n++)
                                {
                                    if (Convert.IsDBNull(dt.Rows[m][n]))
                                    {
                                        sb.AppendLine("<" + dt.Columns[n].ColumnName + "></" + dt.Columns[n].ColumnName + ">");
                                    }
                                    else
                                    {
                                        sb.AppendLine("<" + dt.Columns[n].ColumnName + ">" + dt.Rows[m][n].ToString() + "</" + dt.Columns[n].ColumnName + ">");
                                    }
                                }
                                sb.AppendLine("</Data>");
                            }
                        }
                        else
                        {
                            ////                            sb.AppendLine("<Data>");
                            ////                            A.FSPROMO_ID,A.FSPROMO_NAME,B.FSPROG_ID,B.FSPROG_NAME,
                            ////B.FNPROMO_TYPEA,B.FNPROMO_TYPEB,B.FNPROMO_TYPEC,B.FNPROMO_TYPED,B.FNPROMO_TYPEE,
                            ////B.FSPLAY_COUNT_MAX,B.FSPLAY_COUNT_MIN
                            //                            sb.AppendLine("<FSPROMO_ID>"++"</FSPROMO_ID>");
                            //                            for (int n = 0; n <= dt.Columns.Count - 1; n++)
                            //                            {
                            //                                if (Convert.IsDBNull(dt.Rows[m][n]))
                            //                                {
                            //                                    sb.AppendLine("<" + dt.Columns[n].ColumnName + "></" + dt.Columns[n].ColumnName + ">");
                            //                                }
                            //                                else
                            //                                {
                            //                                    sb.AppendLine("<" + dt.Columns[n].ColumnName + ">" + dt.Rows[m][n].ToString() + "</" + dt.Columns[n].ColumnName + ">");
                            //                                }
                            //                            }
                            //                            sb.AppendLine("</Data>");
                        }

                    }
                    catch (Exception ex)
                    {
                        StringBuilder sbError = new StringBuilder();
                        sbError.AppendLine("<Errors>" + ex.Message + "</Errors>");
                        return sbError.ToString();
                    }
                    finally
                    {
                        connection.Close();
                        connection.Dispose();
                        GC.Collect();
                    }
                }
            }
            sb.AppendLine("</Datas>");
            //參數剖析
            return sb.ToString();
        }

        public string AddProgListRow(DataTable DTCombine, DataTable DTArrPromo, int DTCombine_Row, int DTArrPromo_Row, string INDataType)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<Data>");
            if (INDataType == "G")
            {
                sb.AppendLine("<COMBINENO>" + DTCombine.Rows[DTCombine_Row]["FNCONBINE_NO"].ToString() + "</COMBINENO>");
                sb.AppendLine("<FCTIME_TYPE>" + DTCombine.Rows[DTCombine_Row]["FCTIME_TYPE"].ToString() + "</FCTIME_TYPE>");
                sb.AppendLine("<FSPROG_ID>" + DTCombine.Rows[DTCombine_Row]["FSPROG_ID"].ToString() + "</FSPROG_ID>");
                sb.AppendLine("<FNSEQNO>" + DTCombine.Rows[DTCombine_Row]["FNSEQNO"].ToString() + "</FNSEQNO>");
                sb.AppendLine("<FSNAME>" + ReplaceXML(DTCombine.Rows[DTCombine_Row]["FSPROG_NAME"].ToString()) + "</FSNAME>");
                sb.AppendLine("<FSCHANNEL_ID>" + DTCombine.Rows[DTCombine_Row]["FSCHANNEL_ID"].ToString() + "</FSCHANNEL_ID>");
                sb.AppendLine("<FSCHANNEL_NAME>" + DTCombine.Rows[DTCombine_Row]["FSCHANNEL_NAME"].ToString() + "</FSCHANNEL_NAME>");
                sb.AppendLine("<FSPLAY_TIME>" + DTCombine.Rows[DTCombine_Row]["FSPLAY_TIME"].ToString() + "</FSPLAY_TIME>");
                sb.AppendLine("<FSDURATION>" + DTCombine.Rows[DTCombine_Row]["FSDUR"].ToString() + "</FSDURATION>");
                sb.AppendLine("<FNEPISODE>" + DTCombine.Rows[DTCombine_Row]["FNEPISODE"].ToString() + "</FNEPISODE>");
                sb.AppendLine("<FNBREAK_NO>" + DTCombine.Rows[DTCombine_Row]["FNBREAK_NO"].ToString() + "</FNBREAK_NO>");
                sb.AppendLine("<FNSUB_NO>" + DTCombine.Rows[DTCombine_Row]["FNSUB_NO"].ToString() + "</FNSUB_NO>");
                sb.AppendLine("<FSFILE_NO>" + DTCombine.Rows[DTCombine_Row]["FSFILE_NO"].ToString() + "</FSFILE_NO>");
                sb.AppendLine("<FSVIDEO_ID>" + DTCombine.Rows[DTCombine_Row]["FSVIDEO_ID"].ToString() + "</FSVIDEO_ID>");
                sb.AppendLine("<FSPROMO_ID>" + "" + "</FSPROMO_ID>");
                sb.AppendLine("<FDDATE>" + DTCombine.Rows[DTCombine_Row]["FDDATE"].ToString() + "</FDDATE>");
                sb.AppendLine("<FNLIVE>" + DTCombine.Rows[DTCombine_Row]["FNLIVE"].ToString() + "</FNLIVE>");
                sb.AppendLine("<FSSIGNAL>" + DTCombine.Rows[DTCombine_Row]["FSSIGNAL"].ToString() + "</FSSIGNAL>");
                sb.AppendLine("<FCTYPE>" + "G" + "</FCTYPE>");
                sb.AppendLine("<FSSTATUS>" + "" + "</FSSTATUS>");
                sb.AppendLine("<FSMEMO>" + ReplaceXML(DTCombine.Rows[DTCombine_Row]["FSMEMO"].ToString()) + "</FSMEMO>");
                sb.AppendLine("<FSMEMO1>" + ReplaceXML(DTCombine.Rows[DTCombine_Row]["FSMEMO1"].ToString()) + "</FSMEMO1>");
                sb.AppendLine("<FSPROG_NAME>" + ReplaceXML(DTCombine.Rows[DTCombine_Row]["FSPROG_NAME"].ToString()) + "</FSPROG_NAME>");
            }
            else if (INDataType == "P")
            {
                sb.AppendLine("<COMBINENO>" + DTArrPromo.Rows[DTArrPromo_Row]["FNARR_PROMO_NO"].ToString() + "</COMBINENO>");
                sb.AppendLine("<FCTIME_TYPE>" + DTArrPromo.Rows[DTArrPromo_Row]["FCTIMETYPE"].ToString() + "</FCTIME_TYPE>");
                sb.AppendLine("<FSPROG_ID>" + DTArrPromo.Rows[DTArrPromo_Row]["FSPROG_ID"].ToString() + "</FSPROG_ID>");
                sb.AppendLine("<FNSEQNO>" + DTArrPromo.Rows[DTArrPromo_Row]["FNSEQNO"].ToString() + "</FNSEQNO>");
                sb.AppendLine("<FSNAME>" + ReplaceXML(DTArrPromo.Rows[DTArrPromo_Row]["FSPROMO_NAME"].ToString()) + "</FSNAME>");
                sb.AppendLine("<FSCHANNEL_ID>" + DTArrPromo.Rows[DTArrPromo_Row]["FSCHANNEL_ID"].ToString() + "</FSCHANNEL_ID>");
                sb.AppendLine("<FSCHANNEL_NAME>" + DTArrPromo.Rows[DTArrPromo_Row]["FSCHANNEL_NAME"].ToString() + "</FSCHANNEL_NAME>");
                sb.AppendLine("<FSPLAY_TIME>" + DTArrPromo.Rows[DTArrPromo_Row]["FSPLAYTIME"].ToString() + "</FSPLAY_TIME>");
                sb.AppendLine("<FSDURATION>" + DTArrPromo.Rows[DTArrPromo_Row]["FSDUR"].ToString() + "</FSDURATION>");
                sb.AppendLine("<FNEPISODE>" + DTCombine.Rows[DTCombine_Row]["FNEPISODE"].ToString() + "</FNEPISODE>");
                sb.AppendLine("<FNBREAK_NO>" + DTArrPromo.Rows[DTArrPromo_Row]["FNBREAK_NO"].ToString() + "</FNBREAK_NO>");
                sb.AppendLine("<FNSUB_NO>" + DTArrPromo.Rows[DTArrPromo_Row]["FNSUB_NO"].ToString() + "</FNSUB_NO>");
                sb.AppendLine("<FSFILE_NO>" + DTArrPromo.Rows[DTArrPromo_Row]["FSFILE_NO"].ToString() + "</FSFILE_NO>");
                sb.AppendLine("<FSVIDEO_ID>" + DTArrPromo.Rows[DTArrPromo_Row]["FSVIDEO_ID"].ToString() + "</FSVIDEO_ID>");
                sb.AppendLine("<FSPROMO_ID>" + DTArrPromo.Rows[DTArrPromo_Row]["FSPROMO_ID"].ToString() + "</FSPROMO_ID>");
                sb.AppendLine("<FDDATE>" + DTArrPromo.Rows[DTArrPromo_Row]["FDDATE"].ToString() + "</FDDATE>");
                sb.AppendLine("<FSSIGNAL>" + DTArrPromo.Rows[DTArrPromo_Row]["FSSIGNAL"].ToString() + "</FSSIGNAL>");
                sb.AppendLine("<FNLIVE>" + "" + "</FNLIVE>");
                sb.AppendLine("<FCTYPE>" + "P" + "</FCTYPE>");
                sb.AppendLine("<FSSTATUS>" + "" + "</FSSTATUS>");
                sb.AppendLine("<FSMEMO>" + ReplaceXML(DTArrPromo.Rows[DTArrPromo_Row]["FSMEMO"].ToString()) + "</FSMEMO>");
                sb.AppendLine("<FSMEMO1>" + ReplaceXML(DTArrPromo.Rows[DTArrPromo_Row]["FSMEMO"].ToString()) + "</FSMEMO1>");
                sb.AppendLine("<FSPROG_NAME>" + ReplaceXML(DTCombine.Rows[DTCombine_Row]["FSPROG_NAME"].ToString()) + "</FSPROG_NAME>");
            }
            else if (INDataType == "")
            {
                sb.AppendLine("<COMBINENO>" + "" + "</COMBINENO>");
                sb.AppendLine("<FCTIME_TYPE>" + "" + "</FCTIME_TYPE>");
                sb.AppendLine("<FSPROG_ID>" + "" + "</FSPROG_ID>");
                sb.AppendLine("<FNSEQNO>" + "" + "</FNSEQNO>");
                //sb.AppendLine("<FSNAME>" + "開始[" + DTCombine.Rows[DTCombine_Row]["FSPROG_NAME"].ToString() + "]</FSNAME>");
                sb.AppendLine("<FSNAME>-------------------</FSNAME>");
                sb.AppendLine("<FSCHANNEL_ID>" + "" + "</FSCHANNEL_ID>");
                sb.AppendLine("<FSCHANNEL_NAME>" + "" + "</FSCHANNEL_NAME>");
                sb.AppendLine("<FSPLAY_TIME>" + "" + "</FSPLAY_TIME>");
                sb.AppendLine("<FSDURATION>" + "" + "</FSDURATION>");
                sb.AppendLine("<FNEPISODE>" + "" + "</FNEPISODE>");
                sb.AppendLine("<FNBREAK_NO>" + "" + "</FNBREAK_NO>");
                sb.AppendLine("<FNSUB_NO>" + "" + "</FNSUB_NO>");
                sb.AppendLine("<FSFILE_NO>" + "" + "</FSFILE_NO>");
                sb.AppendLine("<FSVIDEO_ID>" + "" + "</FSVIDEO_ID>");
                sb.AppendLine("<FDDATE>" + "" + "</FDDATE>");
                sb.AppendLine("<FSPROMO_ID>" + "" + "</FSPROMO_ID>");
                sb.AppendLine("<FNLIVE>" + "" + "</FNLIVE>");
                sb.AppendLine("<FCTYPE>" + "" + "</FCTYPE>");
                sb.AppendLine("<FSSIGNAL>" + "" + "</FSSIGNAL>");
                sb.AppendLine("<FSSTATUS>" + "" + "</FSSTATUS>");
                sb.AppendLine("<FSMEMO>" + "" + "</FSMEMO>");
                sb.AppendLine("<FSMEMO1>" + "" + "</FSMEMO1>");
                sb.AppendLine("<FSPROG_NAME>" + "" + "</FSPROG_NAME>");
            }
            sb.AppendLine("</Data>");
            return sb.ToString();
        }

        //查詢播出運行表
        [WebMethod()]
        public string Do_Query_Combin_Arr_Promo(string fddate, string fschannel_id)
        {
            //strSQL為查詢Store Procesure Name
            //parameters為XML內容(DB_Function_Sample.txt)
            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);

            SqlDataAdapter daCombineQueue = new SqlDataAdapter("SP_Q_TBPGM_COMBINE_QUEUE", connection);
            DataTable dtCombineQueue = new DataTable();

            SqlDataAdapter daArrPromo = new SqlDataAdapter("SP_Q_TBPGM_ARR_PROMO", connection);
            DataTable dtArrPromo = new DataTable();

            SqlParameter paraD1 = new SqlParameter("@FDDATE", fddate);
            SqlParameter paraD2 = new SqlParameter("@FDDATE", fddate);
            SqlParameter paraC1 = new SqlParameter("@FSCHANNEL_ID", fschannel_id);
            SqlParameter paraC2 = new SqlParameter("@FSCHANNEL_ID", fschannel_id);

            daCombineQueue.SelectCommand.Parameters.Add(paraD1);
            daCombineQueue.SelectCommand.Parameters.Add(paraC1);
            daArrPromo.SelectCommand.Parameters.Add(paraD2);
            daArrPromo.SelectCommand.Parameters.Add(paraC2);

            daCombineQueue.SelectCommand.CommandType = CommandType.StoredProcedure;
            daArrPromo.SelectCommand.CommandType = CommandType.StoredProcedure;
            StringBuilder sb = new StringBuilder();
            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                //先查詢QUEUE&Arrpromo的資料
                daCombineQueue.Fill(dtCombineQueue);
                daArrPromo.Fill(dtArrPromo);

                if (dtCombineQueue.Rows.Count > 0)
                {
                    sb.AppendLine("<Datas>");
                    for (int i = 0; i <= dtCombineQueue.Rows.Count - 1; i++)
                    {
                        if (dtCombineQueue.Rows[i]["FNBREAK_NO"].ToString() == "1")
                        { //要插入第0段宣傳帶
                            sb.Append(AddProgListRow(dtCombineQueue, dtArrPromo, i, 0, ""));
                            for (int j = 0; j <= dtArrPromo.Rows.Count - 1; j++)
                            {
                                if (dtCombineQueue.Rows[i]["FSPROG_ID"].ToString() == dtArrPromo.Rows[j]["FSPROG_ID"].ToString() && dtCombineQueue.Rows[i]["FNSEQNO"].ToString() == dtArrPromo.Rows[j]["FNSEQNO"].ToString() && dtArrPromo.Rows[j]["FNBREAK_NO"].ToString() == "0")
                                {
                                    sb.Append(AddProgListRow(dtCombineQueue, dtArrPromo, i, j, "P"));
                                }
                            }
                        }
                        sb.Append(AddProgListRow(dtCombineQueue, dtArrPromo, i, 0, "G"));
                        for (int j = 0; j <= dtArrPromo.Rows.Count - 1; j++)
                        {
                            if (dtCombineQueue.Rows[i]["FSPROG_ID"].ToString() == dtArrPromo.Rows[j]["FSPROG_ID"].ToString() && dtCombineQueue.Rows[i]["FNSEQNO"].ToString() == dtArrPromo.Rows[j]["FNSEQNO"].ToString() && dtCombineQueue.Rows[i]["FNBREAK_NO"].ToString() == dtArrPromo.Rows[j]["FNBREAK_NO"].ToString())
                            {
                                sb.Append(AddProgListRow(dtCombineQueue, dtArrPromo, i, j, "P"));
                            }
                        }
                    }
                    sb.Append(AddProgListRow(dtCombineQueue, dtArrPromo, 0, 0, ""));
                    sb.AppendLine("</Datas>");
                }
                else //(dtCombineQueue.Rows.Count < 0) 沒有節目表資料
                {
                    return "<Errors>" + "沒有節目表資料" + "</Errors>";
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                StringBuilder sbError = new StringBuilder();
                sbError.AppendLine("<Errors>" + ex.Message + "</Errors>");
                return sbError.ToString();
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }
        }

        private static string toHex(string arg)
        {
            StringBuilder sb = new StringBuilder();
            char[] inp = arg.ToCharArray();

            for (int i = 0; i < inp.Length; i++)
            {
                sb.Append(((int)inp[i]).ToString("X"));
            }
            return sb.ToString();
        }

        public void FileSaveHex()
        {
            string filename = @"c:\1.txt";
            string strWriteFile = "AABBCCDD";//假設為指向某串口來的最終16進制顯示的數據字符串
            byte[] data = new byte[2];//2個16進制數表示一個字節
            //int i = 0;
            //foreach (Match m in Regex.Matches(strWriteFile, "(?i)[a-f0-9]{2}"))
            //{
            //    data[i++] = byte.Parse(m.Value, System.Globalization.NumberStyles.HexNumber);
            //}
            data[0] = byte.Parse("A", System.Globalization.NumberStyles.HexNumber);
            data[1] = byte.Parse("A", System.Globalization.NumberStyles.HexNumber);
            using (FileStream fs = File.OpenWrite(filename))
            {
                fs.Seek(0, SeekOrigin.End);
                fs.Write(data, 0, data.Length);
            }
        }

        string Hex(int i)
        {
            return i.ToString("x");
        }

        public static int BinWrite(string SaveFileName, byte[] InData)
        {
            try
            {
                //開啟建立檔案
                FileStream myFile = File.Open(SaveFileName, FileMode.Open, FileAccess.ReadWrite);
                BinaryWriter myWriter = new BinaryWriter(myFile);
                myWriter.Write(InData);
                myWriter.Close();
                myFile.Close();
                return 1;
            }
            catch (InvalidCastException e)
            {
                return -1;
            }

        }

        public static byte[] StrToByteArray(string str, int Length)
        {
            byte[] progname = new byte[Length];
            if (str == "" || str == null)
            {
                for (int i = 0; i < Length; i++)
                {
                    progname[i] = (byte)32;
                }
                return progname;
            }

            Encoding e1 = Encoding.GetEncoding(950);
            byte[] b1 = e1.GetBytes(str);
            //return b1;


            //System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            //return encoding.GetBytes(str);


            for (int i = 0; i < Length; i++)
            {
                if (i < b1.Count())
                    progname[i] = b1[i];
                else
                    progname[i] = (byte)32;
            }

            //System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            //progname = encoding.GetBytes(str);
            //if (progname.Count() < Length)
            //{ 

            //    int TempI=progname.Count()+1;
            //    while (TempI<= Length)
            //    {
            //        progname[TempI]=(byte)32;
            //         TempI++;
            //    }
            //}

            //progname=encoding.GetBytes(str.Substring(0,8));
            return progname;
        }

        public int INT10To16(string INTime)
        {
            int tempInt;
            try
            {
                int.TryParse(INTime, out tempInt);
                tempInt = int.Parse(INTime.Substring(0, 1)) * 16 + int.Parse(INTime.Substring(1, 1));
            }
            catch (InvalidCastException e)
            {
                tempInt = 0;
            }
           
         

            
            return tempInt;
        }

        public byte[] WriteProgByte(ArrPromoList TemArrPromoList, string ChannelType)
        {
            byte[] ProgData = new byte[104];
            ProgData[0] = (byte)0;
            for (int i = 1; i < 9; i++)
            {
                ProgData[i] = (byte)255; //Reconcile Key 	2 	8 	ASCII 	FFH 
            }
            for (int i = 9; i < 12; i++)
            {
                ProgData[i] = (byte)0;
                //ProgData[0]=(byte)0; //Effect1		10 	1 	BINARY 	00H 
                //sw1.Write((char)0); //Effect2 		11 	1 	BINARY 	00H
                //sw1.Write((char)0);  //Effect3 		12 	1	BINARY 	00H 
            }
            ProgData[12] = (byte)INT10To16(TemArrPromoList.FSPLAY_TIME.Substring(9, 2));
            ProgData[13] = (byte)INT10To16(TemArrPromoList.FSPLAY_TIME.Substring(6, 2));
            ProgData[14] = (byte)INT10To16(TemArrPromoList.FSPLAY_TIME.Substring(3, 2));
            ProgData[15] = (byte)INT10To16(TemArrPromoList.FSPLAY_TIME.Substring(0, 2));
            //ProgData[12]=StrToByteArray(TemArrPromoList.FSPLAY_TIME.Substring(9, 2))[0]; //On Air Frame 	13 	1 	BCD 	FFH
            //ProgData[13]=StrToByteArray(TemArrPromoList.FSPLAY_TIME.Substring(6, 2))[0]; //On Air Second 	14 	1	BCD		FFH
            //ProgData[14]=StrToByteArray(TemArrPromoList.FSPLAY_TIME.Substring(9, 2))[0]; //On Air Minute 	15 	1 	BCD 	FFH
            //ProgData[15]=StrToByteArray(TemArrPromoList.FSPLAY_TIME.Substring(9, 2))[0]; //On Air Hour 	16 	1 	BCD 	FFH

            //                    ID 		17 	8 	ASCII 	FFH if empty, 20H if used (Material ID)
            //TITLE		25 	16 	ASCII 	20H
            byte[] TempArray = new byte[16];
            if (TemArrPromoList.FNLIVE == "1") //表示為Live
                TempArray = StrToByteArray(TemArrPromoList.FSSIGNAL, 8);
            else
                TempArray = StrToByteArray(TemArrPromoList.FSVIDEO_ID, 8);
            for (int i = 16; i < 24; i++)
            {
                ProgData[i] = TempArray[i - 16];
            }
            TempArray = StrToByteArray(TemArrPromoList.FSNAME, 16);
            for (int i = 24; i < 40; i++)
            {
                ProgData[i] = TempArray[i - 24];
            }
            //SOM & 長度
            for (int i = 40; i < 44; i++)
            {
                ProgData[i] = (byte)0; //SOM Frame 	41 	1 	BCD 	FFH
                //                        SOM Frame 	41 	1 	BCD 	FFH
                //SOM Second 	42 	1 	BCD 	FFH
                //SOM Minute 	43 	1 	BCD 	FFH
                //SOM Hour 	44 	1 	BCD 	FFH
                //DUR Frame 	45 	1 	BCD 	FFH
                //DUR Second 	46 	1 	BCD 	FFH
                //DUR Minute 	47 	1 	BCD 	FFH
                //DUR Hour 	48 	1 	BCD 	FFH
            }
            ProgData[44] = (byte)INT10To16(TemArrPromoList.FSDURATION.Substring(9, 2));//DUR Frame 	45 	1 	BCD 	FFH
            ProgData[45] = (byte)INT10To16(TemArrPromoList.FSDURATION.Substring(6, 2));//DUR Second 	46 	1 	BCD 	FFH
            ProgData[46] = (byte)INT10To16(TemArrPromoList.FSDURATION.Substring(3, 2));//DUR Minute 	47 	1 	BCD 	FFH
            ProgData[47] = (byte)INT10To16(TemArrPromoList.FSDURATION.Substring(0, 2)); //DUR Hour 	48 	1 	BCD 	FFH
            ProgData[48] = (byte)0; // Channel 		49 	1	BINARY 	00H 
            ProgData[49] = (byte)0; // Qualifier4 	50 	1 	BINARY 	00H (Fill With Null) 
            if (TemArrPromoList.FCTYPE == "G")
            { //節目段落,當段落為節目時要加
             
                    //if (ChannelType == "002")
                    //    ProgData[50] = (byte)int.Parse(TemArrPromoList.FNBREAK_NO);
                    //else
                        ProgData[50] = (byte)INT10To16(string.Format("{0:00}", int.Parse(TemArrPromoList.FNBREAK_NO)));//Segment Number 51 	1 	BCD 	FFH Program Segment Number
            
            }
            else
            {
                ProgData[50] = (byte)255; //Segment Number 51 	1 	BCD 	FFH Program Segment Number
            }

            //Device Maj. 	52 	1 	BINARY 	00H
            //Device Min. 	53 	1 	BINARY 	00H
            //Bin High 		54 	1 	BINARY 	00H
            //Bin Low 		55 	1 	BINARY 	00H
            //Qualifier1		56 	1 	BINARY 	00H (Fill With Null) 
            //Qualifier2		57 	1 	BINARY 	00H (Fill With Null)
            //Qualifier3		58 	1	BINARY 	00H (Fill With Null)
            for (int i = 51; i < 58; i++)
            {
                ProgData[i] = (byte)0;
            }
            ProgData[58] = (byte)255;  //DateToAir 	59 	2 	WORD 	FFH (Julian Type for Asrun)
            ProgData[59] = (byte)255;
            //Event COntrol
            //ProgData[60] = (byte)7; //Event Control	61 	2 	BINARY
            //ProgData[61] = (byte)2;
            ProgData[60] = (byte)0; //Event Control	61 	2 	BINARY
            ProgData[61] = (byte)0;
            //-----------------
            ProgData[62] = (byte)0; //Event Status 	63 	2 	BINARY 	00H (Used by system)
            ProgData[63] = (byte)0;

            //CompileID 		65 		8 		ASCII 		FFH (Fill With Null)
            //CompileSOM 		73 		4 		BCD		FFH (Fill With Null) 
            //BoxAID 		77 		8 		ASCII		FFH (Fill With Null)
            //BoxASOM 		85 		4 		BCD 		FFH (Fill With Null)
            //BoxBID 		89 		8 		ASCII		FFH (Fill With Null)
            //BoxBSOM 		97 		4 		BCD 		FFH (Fill With Null)
            for (int i = 64; i < 100; i++)
            {
                ProgData[i] = (byte)255;
            }
            //Reserved 		101 		1 		BINARY 	00H (Fill With ZERO Always)
            //Backup Device Maj. 	102		1 		BINARY 	00H
            //Backup Device Min. 	103 		1 		BINARY 	00H
            //Extended EventControl 	104		1		BINARY 	00H
            for (int i = 100; i < 104; i++)
            {
                ProgData[i] = (byte)0;
            }
            return ProgData;
        }

        public byte[] WriteKeyerByte(ArrPromoList TemArrPromoList, string PlayTime, string Dur, string VideoID, string Title)
        {
            byte[] ProgData = new byte[104];
            ProgData[0] = (byte)131;
            for (int i = 1; i < 9; i++)
            {
                ProgData[i] = (byte)255; //Reconcile Key 	2 	8 	ASCII 	FFH 
            }
            for (int i = 9; i < 12; i++)
            {
                ProgData[i] = (byte)0;
                //ProgData[0]=(byte)0; //Effect1		10 	1 	BINARY 	00H 
                //sw1.Write((char)0); //Effect2 		11 	1 	BINARY 	00H
                //sw1.Write((char)0);  //Effect3 		12 	1	BINARY 	00H 
            }
            ProgData[12] = (byte)INT10To16(PlayTime.Substring(9, 2));
            ProgData[13] = (byte)INT10To16(PlayTime.Substring(6, 2));
            ProgData[14] = (byte)INT10To16(PlayTime.Substring(3, 2));
            ProgData[15] = (byte)INT10To16(PlayTime.Substring(0, 2));
            //ProgData[12]=StrToByteArray(TemArrPromoList.FSPLAY_TIME.Substring(9, 2))[0]; //On Air Frame 	13 	1 	BCD 	FFH
            //ProgData[13]=StrToByteArray(TemArrPromoList.FSPLAY_TIME.Substring(6, 2))[0]; //On Air Second 	14 	1	BCD		FFH
            //ProgData[14]=StrToByteArray(TemArrPromoList.FSPLAY_TIME.Substring(9, 2))[0]; //On Air Minute 	15 	1 	BCD 	FFH
            //ProgData[15]=StrToByteArray(TemArrPromoList.FSPLAY_TIME.Substring(9, 2))[0]; //On Air Hour 	16 	1 	BCD 	FFH

            //                    ID 		17 	8 	ASCII 	FFH if empty, 20H if used (Material ID)
            //TITLE		25 	16 	ASCII 	20H
            byte[] TempArray = new byte[16];
            TempArray = StrToByteArray(VideoID, 8);
            for (int i = 16; i < 24; i++)
            {
                ProgData[i] = TempArray[i - 16];
            }
            TempArray = StrToByteArray(Title, 16);
            for (int i = 24; i < 40; i++)
            {
                ProgData[i] = TempArray[i - 24];
            }
            //SOM & 長度
            for (int i = 40; i < 44; i++)
            {
                ProgData[i] = (byte)0; //SOM Frame 	41 	1 	BCD 	FFH
                //                        SOM Frame 	41 	1 	BCD 	FFH
                //SOM Second 	42 	1 	BCD 	FFH
                //SOM Minute 	43 	1 	BCD 	FFH
                //SOM Hour 	44 	1 	BCD 	FFH
                //DUR Frame 	45 	1 	BCD 	FFH
                //DUR Second 	46 	1 	BCD 	FFH
                //DUR Minute 	47 	1 	BCD 	FFH
                //DUR Hour 	48 	1 	BCD 	FFH
            }
            ProgData[44] = (byte)INT10To16(Dur.Substring(9, 2));//DUR Frame 	45 	1 	BCD 	FFH
            ProgData[45] = (byte)INT10To16(Dur.Substring(6, 2));//DUR Second 	46 	1 	BCD 	FFH
            ProgData[46] = (byte)INT10To16(Dur.Substring(3, 2));//DUR Minute 	47 	1 	BCD 	FFH
            ProgData[47] = (byte)INT10To16(Dur.Substring(0, 2)); //DUR Hour 	48 	1 	BCD 	FFH
            ProgData[48] = (byte)0; // Channel 		49 	1	BINARY 	00H 
            ProgData[49] = (byte)0; // Qualifier4 	50 	1 	BINARY 	00H (Fill With Null) 
            ProgData[50] = (byte)255; //Segment Number 51 	1 	BCD 	FFH Program Segment Number
            //Device Maj. 	52 	1 	BINARY 	00H
            //Device Min. 	53 	1 	BINARY 	00H
            //Bin High 		54 	1 	BINARY 	00H
            //Bin Low 		55 	1 	BINARY 	00H
            //Qualifier1		56 	1 	BINARY 	00H (Fill With Null) 
            //Qualifier2		57 	1 	BINARY 	00H (Fill With Null)
            //Qualifier3		58 	1	BINARY 	00H (Fill With Null)
            for (int i = 51; i < 58; i++)
            {
                ProgData[i] = (byte)0;
            }

            ProgData[58] = (byte)255;  //DateToAir 	59 	2 	WORD 	FFH (Julian Type for Asrun)
            ProgData[59] = (byte)255;
            //Event Control
            ProgData[60] = (byte)7; //Event Control	61 	2 	BINARY
            ProgData[61] = (byte)2;
            //-----------------
            ProgData[62] = (byte)0; //Event Status 	63 	2 	BINARY 	00H (Used by system)
            ProgData[63] = (byte)0;

            //CompileID 		65 		8 		ASCII 		FFH (Fill With Null)
            //CompileSOM 		73 		4 		BCD		FFH (Fill With Null) 
            //BoxAID 		77 		8 		ASCII		FFH (Fill With Null)
            //BoxASOM 		85 		4 		BCD 		FFH (Fill With Null)
            //BoxBID 		89 		8 		ASCII		FFH (Fill With Null)
            //BoxBSOM 		97 		4 		BCD 		FFH (Fill With Null)
            for (int i = 64; i < 100; i++)
            {
                ProgData[i] = (byte)255;
            }
            //Reserved 		101 		1 		BINARY 	00H (Fill With ZERO Always)
            //Backup Device Maj. 	102		1 		BINARY 	00H
            //Backup Device Min. 	103 		1 		BINARY 	00H
            //Extended EventControl 	104		1		BINARY 	00H
            for (int i = 100; i < 104; i++)
            {
                ProgData[i] = (byte)0;
            }
            return ProgData;
        }

        public byte[] WriteLouthByteArray(ArrPromoListAddSOM TemArrPromoList, string striType, string VideoID, string Title, string PlayTime, string Dur, string SOM, string EventControl, string QF1, string QF2, string QF3, string QF4, string EF1, string EF2, string EF3, string ChannelType)
        {
            byte[] ProgData = new byte[104];
            ProgData[0] = (byte)int.Parse(striType);
            for (int i = 1; i < 9; i++)
            {
                ProgData[i] = (byte)255; //Reconcile Key 	2 	8 	ASCII 	FFH 
            }

            if (EF1 != "")
                ProgData[9] = (byte)int.Parse(EF1);
            else
                ProgData[9] = (byte)0;

            if (EF2 != "")
                ProgData[10] = (byte)int.Parse(EF2);
            else
                ProgData[10] = (byte)0;

            if (EF3 != "")
                ProgData[11] = (byte)int.Parse(EF3);
            else
                ProgData[11] = (byte)0;

            if (PlayTime == "")
            {
                ProgData[12] = (byte)0;
                ProgData[13] = (byte)0;
                ProgData[14] = (byte)0;
                ProgData[15] = (byte)0;
            }
            else
            {
                ProgData[12] = (byte)INT10To16(PlayTime.Substring(9, 2));
                ProgData[13] = (byte)INT10To16(PlayTime.Substring(6, 2));
                ProgData[14] = (byte)INT10To16(PlayTime.Substring(3, 2));
                ProgData[15] = (byte)INT10To16(PlayTime.Substring(0, 2));
            }
            //                    ID 		17 	8 	ASCII 	FFH if empty, 20H if used (Material ID)
            //TITLE		25 	16 	ASCII 	20H
            byte[] TempArray = new byte[16];
            if (TemArrPromoList.FNLIVE == "1") //表示為Live,則將訊號來源寫入VideoID位置
                TempArray = StrToByteArray(TemArrPromoList.FSSIGNAL, 8);
            else
            {
                if (TemArrPromoList.FSSIGNAL == "" || TemArrPromoList.FSSIGNAL == "COMM" || TemArrPromoList.FSSIGNAL == "PGM")
                    TempArray = StrToByteArray(VideoID, 8);
                else
                    TempArray = StrToByteArray(TemArrPromoList.FSSIGNAL, 8);
            }
            for (int i = 16; i < 24; i++)
            {
                ProgData[i] = TempArray[i - 16];
            }

            if (TemArrPromoList.FCTYPE == "G")
            {
                if (TemArrPromoList.FNEPISODE != "" && TemArrPromoList.FNEPISODE != null)
                    TempArray = StrToByteArray(GetSubString(Title, 16 - (TemArrPromoList.FNEPISODE.Length + 1)) + "#" + TemArrPromoList.FNEPISODE, 16);
                else
                    TempArray = StrToByteArray(GetSubString(Title, 16), 16);
            }
            else
                TempArray = StrToByteArray(GetSubString(Title, 16), 16);
            for (int i = 24; i < 40; i++)
            {
                ProgData[i] = TempArray[i - 24];
            }
            //SOM & 長度

            if (SOM != "")
            {
                ProgData[40] = (byte)INT10To16(SOM.Substring(9, 2));//DUR Frame 	45 	1 	BCD 	FFH
                ProgData[41] = (byte)INT10To16(SOM.Substring(6, 2));//DUR Second 	46 	1 	BCD 	FFH
                ProgData[42] = (byte)INT10To16(SOM.Substring(3, 2));//DUR Minute 	47 	1 	BCD 	FFH
                ProgData[43] = (byte)INT10To16(SOM.Substring(0, 2)); //DUR Hour 	48 	1 	BCD 	FFH
            }
            else
            {
                ProgData[40] = (byte)0; //SOM Frame 	41 	1 	BCD 	FFH
                ProgData[41] = (byte)0; //SOM Frame 	41 	1 	BCD 	FFH
                ProgData[42] = (byte)0; //SOM Frame 	41 	1 	BCD 	FFH
                ProgData[43] = (byte)0; //SOM Frame 	41 	1 	BCD 	FFH
            }

            if (Dur == "")
            {
                ProgData[44] = (byte)0;
                ProgData[45] = (byte)0;
                ProgData[46] = (byte)0;
                ProgData[47] = (byte)0;
            }
            else
            {
                ProgData[44] = (byte)INT10To16(Dur.Substring(9, 2));//DUR Frame 	45 	1 	BCD 	FFH
                ProgData[45] = (byte)INT10To16(Dur.Substring(6, 2));//DUR Second 	46 	1 	BCD 	FFH
                ProgData[46] = (byte)INT10To16(Dur.Substring(3, 2));//DUR Minute 	47 	1 	BCD 	FFH
                ProgData[47] = (byte)INT10To16(Dur.Substring(0, 2)); //DUR Hour 	48 	1 	BCD 	FFH
            }
            ProgData[48] = (byte)0; // Channel 		49 	1	BINARY 	00H 

            if (QF4 != "")
                ProgData[49] = (byte)int.Parse(QF4);
            else
                ProgData[49] = (byte)0;
            //ProgData[49] = (byte)0; // Qualifier4 	50 	1 	BINARY 	00H (Fill With Null) 

            if (TemArrPromoList.FCTYPE == "G" && striType == "0")
            {
                if (TemArrPromoList.FNBREAK_NO != "" && TemArrPromoList.FNBREAK_NO != null)
                {
                    //if (ChannelType == "002")
                    //    ProgData[50] = (byte)int.Parse(TemArrPromoList.FNBREAK_NO);
                    //else
                    if (TemArrPromoList.FSPROG_ID == "2004090" || TemArrPromoList.FSPROG_ID == "2005459" || (ChannelType == "002" && TemArrPromoList.FNLIVE == "1"))
                        ProgData[50] = (byte)255;
                    else
                        ProgData[50] = (byte)INT10To16(string.Format("{0:00}", int.Parse(TemArrPromoList.FNBREAK_NO)));
                }
                else
                    ProgData[50] = (byte)255;
            }
            else
                ProgData[50] = (byte)255; //Segment Number 51 	1 	BCD 	FFH Program Segment Number

            //Device Maj. 	52 	1 	BINARY 	00H
            //Device Min. 	53 	1 	BINARY 	00H
            //Bin High 		54 	1 	BINARY 	00H
            //Bin Low 		55 	1 	BINARY 	00H
            //Qualifier1		56 	1 	BINARY 	00H (Fill With Null) 
            //Qualifier2		57 	1 	BINARY 	00H (Fill With Null)
            //Qualifier3		58 	1	BINARY 	00H (Fill With Null)
            for (int i = 51; i < 55; i++)
            {
                ProgData[i] = (byte)0;
            }

            if (QF1 != "")
                ProgData[55] = (byte)int.Parse(QF1);
            else
                ProgData[55] = (byte)0;

            if (QF2 != "")
                ProgData[56] = (byte)int.Parse(QF2);
            else
                ProgData[56] = (byte)0;

            if (QF3 != "")
                ProgData[57] = (byte)int.Parse(QF3);
            else
                ProgData[57] = (byte)0;
            ProgData[58] = (byte)255;  //DateToAir 	59 	2 	WORD 	FFH (Julian Type for Asrun)
            ProgData[59] = (byte)255;

            //            Const CTL_A = &H7 'A=>Autoplay(01)+Autothread(02)+Autoswitch(04)
            //Const CTL_AO = &H17  'AO=A+Hardtime(10)
            //Const CTL_AUN = &H247    'ANU=A+Automarktime(200)+uppercounter(40)
            //Const CTL_AN = &H207 'AN=A+Automarktime(200)
            //Const CTL_AUNO = &H257  'AUNO=AUN+Hardtime(10)
            //Const CTL_ANO = &H217  'ANO=AN+Hardtime(10)
            //Event Control
            if (EventControl == "")
            {
                ProgData[60] = (byte)0; //Event Control	61 	2 	BINARY
                ProgData[61] = (byte)0;
            }
            else if (EventControl == "AO")
            {
                ProgData[60] = (byte)23; //Event Control	61 	2 	BINARY
                ProgData[61] = (byte)0;
            }
            else if (EventControl == "A")
            {
                ProgData[60] = (byte)7; //Event Control	61 	2 	BINARY
                ProgData[61] = (byte)0;
            }
            else if (EventControl == "AUN")
            {
                ProgData[60] = (byte)71; //Event Control	61 	2 	BINARY
                ProgData[61] = (byte)2;
            }
            else if (EventControl == "AN")
            {
                ProgData[60] = (byte)7; //Event Control	61 	2 	BINARY
                ProgData[61] = (byte)2;
            }
            else if (EventControl == "AUNO")
            {
                ProgData[60] = (byte)87; //Event Control	61 	2 	BINARY
                ProgData[61] = (byte)2;
            }
            else if (EventControl == "ANO")
            {
                ProgData[60] = (byte)23; //Event Control	61 	2 	BINARY
                ProgData[61] = (byte)2;
            }
            //ProgData[60] = (byte)7; //Event Control	61 	2 	BINARY
            //ProgData[61] = (byte)2;
            //-----------------
            ProgData[62] = (byte)0; //Event Status 	63 	2 	BINARY 	00H (Used by system)
            ProgData[63] = (byte)0;



            //CompileID 		65 		8 		ASCII 		FFH (Fill With Null)
            //CompileSOM 		73 		4 		BCD		FFH (Fill With Null) 
            //BoxAID 		77 		8 		ASCII		FFH (Fill With Null)
            //BoxASOM 		85 		4 		BCD 		FFH (Fill With Null)
            //BoxBID 		89 		8 		ASCII		FFH (Fill With Null)
            //BoxBSOM 		97 		4 		BCD 		FFH (Fill With Null)
            for (int i = 64; i < 100; i++)
            {
                ProgData[i] = (byte)255;
            }


            //Reserved 		101 		1 		BINARY 	00H (Fill With ZERO Always)
            //Backup Device Maj. 	102		1 		BINARY 	00H
            //Backup Device Min. 	103 		1 		BINARY 	00H
            //Extended EventControl 	104		1		BINARY 	00H
            if (ChannelType == "002")
            {
                if (TemArrPromoList.FNLIVE == "1")
                    ProgData[100] = (byte)2;
                else if (TemArrPromoList.FCTYPE == "G")
                    ProgData[100] = (byte)1;
                else
                    ProgData[100] = (byte)0;
            }
            else
                ProgData[100] = (byte)0;

            for (int i = 101; i < 104; i++)
            {
                ProgData[i] = (byte)0;
            }
            return ProgData;
        }

        public byte[] WritKeyByteArray(ArrPromoListAddSOM TemArrPromoList, string striType, string VideoID, string Title, string PlayTime, string Dur, string SOM, string EventControl, string QF1, string QF2, string QF3, string QF4, string EF1, string EF2, string EF3, string ChannelType,string LouthType)
        {
            byte[] ProgData = new byte[104];
            ProgData[0] = (byte)int.Parse(striType);
            for (int i = 1; i < 9; i++)
            {
                ProgData[i] = (byte)255; //Reconcile Key 	2 	8 	ASCII 	FFH 
            }

            if (EF1 != "")
                ProgData[9] = (byte)int.Parse(EF1);
            else
                ProgData[9] = (byte)0;

            if (EF2 != "")
                ProgData[10] = (byte)int.Parse(EF2);
            else
                ProgData[10] = (byte)0;

            if (EF3 != "")
                ProgData[11] = (byte)int.Parse(EF3);
            else
                ProgData[11] = (byte)0;

            if (PlayTime == "")
            {
                ProgData[12] = (byte)255;
                ProgData[13] = (byte)255;
                ProgData[14] = (byte)255;
                ProgData[15] = (byte)255;
            }
            else
            {
                ProgData[12] = (byte)INT10To16(PlayTime.Substring(9, 2));
                ProgData[13] = (byte)INT10To16(PlayTime.Substring(6, 2));
                ProgData[14] = (byte)INT10To16(PlayTime.Substring(3, 2));
                ProgData[15] = (byte)INT10To16(PlayTime.Substring(0, 2));
            }
            //                    ID 		17 	8 	ASCII 	FFH if empty, 20H if used (Material ID)
            //TITLE		25 	16 	ASCII 	20H
            byte[] TempArray = new byte[16];

            TempArray = StrToByteArray(VideoID, 8);

            for (int i = 16; i < 24; i++)
            {
                ProgData[i] = TempArray[i - 16];
            }
            //LouthType 代表要輸出哪一種的Louth的檔案格式
            if (LouthType == "HD")
            {
                string TempTitle = "";
                TempTitle = "L=" + string.Format("{0:000}", int.Parse(QF1));
                //TempArray = StrToByteArray(GetSubString(Title, 16), 16);
                TempArray = StrToByteArray(TempTitle, 16);
                for (int i = 24; i < 40; i++)
                {
                    ProgData[i] = TempArray[i - 24];
                }
            }
            else if (LouthType == "SD")
            {              
                TempArray = StrToByteArray(GetSubString(Title, 16), 16);
                for (int i = 24; i < 40; i++)
                {
                    ProgData[i] = TempArray[i - 24];
                }
            }
            else if (LouthType == "E")
            {
                TempArray = StrToByteArray(GetSubString(Title, 16), 16);
                for (int i = 24; i < 40; i++)
                {
                    ProgData[i] = TempArray[i - 24];
                }
            }


            //SOM & 長度

            if (SOM != "")
            {
                ProgData[40] = (byte)INT10To16(SOM.Substring(9, 2));//DUR Frame 	45 	1 	BCD 	FFH
                ProgData[41] = (byte)INT10To16(SOM.Substring(6, 2));//DUR Second 	46 	1 	BCD 	FFH
                ProgData[42] = (byte)INT10To16(SOM.Substring(3, 2));//DUR Minute 	47 	1 	BCD 	FFH
                ProgData[43] = (byte)INT10To16(SOM.Substring(0, 2)); //DUR Hour 	48 	1 	BCD 	FFH
            }
            else
            {
                ProgData[40] = (byte)255; //SOM Frame 	41 	1 	BCD 	FFH
                ProgData[41] = (byte)255; //SOM Frame 	41 	1 	BCD 	FFH
                ProgData[42] = (byte)255; //SOM Frame 	41 	1 	BCD 	FFH
                ProgData[43] = (byte)255; //SOM Frame 	41 	1 	BCD 	FFH
            }

            if (Dur == "")
            {
                ProgData[44] = (byte)255;
                ProgData[45] = (byte)255;
                ProgData[46] = (byte)255;
                ProgData[47] = (byte)255;
            }
            else
            {
                ProgData[44] = (byte)INT10To16(Dur.Substring(9, 2));//DUR Frame 	45 	1 	BCD 	FFH
                ProgData[45] = (byte)INT10To16(Dur.Substring(6, 2));//DUR Second 	46 	1 	BCD 	FFH
                ProgData[46] = (byte)INT10To16(Dur.Substring(3, 2));//DUR Minute 	47 	1 	BCD 	FFH
                ProgData[47] = (byte)INT10To16(Dur.Substring(0, 2)); //DUR Hour 	48 	1 	BCD 	FFH
            }
            ProgData[48] = (byte)0; // Channel 		49 	1	BINARY 	00H 
           
            //LouthType 代表要輸出哪一種的Louth的檔案格式
            if (LouthType == "HD")
            {
                if (QF4 != "")
                    ProgData[49] = (byte)int.Parse(QF4);
                else
                    ProgData[49] = (byte)0;
            }
            else if (LouthType == "SD")
            {
                if (QF4 != "")
                    ProgData[49] = (byte)int.Parse(QF4);
                else
                    ProgData[49] = (byte)0;
            }
            else if (LouthType == "E")
            {
                if (QF4 != "")
                {
                    if (QF4 == "7")
                        ProgData[49] = (byte)int.Parse("1");
                    if (QF4 == "6")
                        ProgData[49] = (byte)int.Parse("2");
                    if (QF4 == "4")
                        ProgData[49] = (byte)int.Parse("3");
                    if (QF4 == "3")
                        ProgData[49] = (byte)int.Parse("4");
                }
                else
                    ProgData[49] = (byte)0;
            }
          
            //ProgData[49] = (byte)0; // Qualifier4 	50 	1 	BINARY 	00H (Fill With Null) 

            if (TemArrPromoList.FCTYPE == "G" && striType == "0")
            {
                //if (ChannelType == "002")
                //    ProgData[50] = (byte)int.Parse(TemArrPromoList.FNBREAK_NO);
                //else

                if (TemArrPromoList.FSPROG_ID == "2004090" || TemArrPromoList.FSPROG_ID == "2005459" || (ChannelType == "002" && TemArrPromoList.FNLIVE == "1"))
                    ProgData[50] = (byte)255;

                else
                    ProgData[50] = (byte)INT10To16(string.Format("{0:00}", int.Parse(TemArrPromoList.FNBREAK_NO)));
            }
            else
                ProgData[50] = (byte)255; //Segment Number 51 	1 	BCD 	FFH Program Segment Number

            //Device Maj. 	52 	1 	BINARY 	00H
            //Device Min. 	53 	1 	BINARY 	00H
            //Bin High 		54 	1 	BINARY 	00H
            //Bin Low 		55 	1 	BINARY 	00H
            //Qualifier1		56 	1 	BINARY 	00H (Fill With Null) 
            //Qualifier2		57 	1 	BINARY 	00H (Fill With Null)
            //Qualifier3		58 	1	BINARY 	00H (Fill With Null)
            for (int i = 51; i < 55; i++)
            {
                ProgData[i] = (byte)0;
            }

            //LouthType 代表要輸出哪一種的Louth的檔案格式
            if (LouthType == "HD")
            {
                if (QF4 != "")
                    ProgData[55] = (byte)int.Parse(QF4);
                else
                    ProgData[55] = (byte)1;
            }
            else if (LouthType == "SD")
            {
                if (QF1 != "")
                    ProgData[55] = (byte)int.Parse(QF1);
                else
                    ProgData[55] = (byte)0;
            }
            else if (LouthType == "E")
            {
                if (QF1 != "")
                    ProgData[55] = (byte)int.Parse(QF1);
                else
                    ProgData[55] = (byte)0;
            }

           
           

            if (QF2 != "")
                ProgData[56] = (byte)int.Parse(QF2);
            else
                ProgData[56] = (byte)0;

            if (QF3 != "")
                ProgData[57] = (byte)int.Parse(QF3);
            else
                ProgData[57] = (byte)0;


            ProgData[58] = (byte)255;  //DateToAir 	59 	2 	WORD 	FFH (Julian Type for Asrun)
            ProgData[59] = (byte)255;
            //ProgData[58] = (byte)125;  //DateToAir 	59 	2 	WORD 	FFH (Julian Type for Asrun)
            //ProgData[59] = (byte)156;

            //            Const CTL_A = &H7 'A=>Autoplay(01)+Autothread(02)+Autoswitch(04)
            //Const CTL_AO = &H17  'AO=A+Hardtime(10)
            //Const CTL_AUN = &H247    'ANU=A+Automarktime(200)+uppercounter(40)
            //Const CTL_AN = &H207 'AN=A+Automarktime(200)
            //Const CTL_AUNO = &H257  'AUNO=AUN+Hardtime(10)
            //Const CTL_ANO = &H217  'ANO=AN+Hardtime(10)
            //Event Control
            if (EventControl == "")
            {
                ProgData[60] = (byte)0; //Event Control	61 	2 	BINARY
                ProgData[61] = (byte)0;
            }
            else if (EventControl == "AO")
            {
                ProgData[60] = (byte)23; //Event Control	61 	2 	BINARY
                ProgData[61] = (byte)0;
            }
            else if (EventControl == "A")
            {
                ProgData[60] = (byte)7; //Event Control	61 	2 	BINARY
                ProgData[61] = (byte)0;
            }
            else if (EventControl == "AUN")
            {
                ProgData[60] = (byte)71; //Event Control	61 	2 	BINARY
                ProgData[61] = (byte)2;
            }
            else if (EventControl == "AN")
            {
                ProgData[60] = (byte)7; //Event Control	61 	2 	BINARY
                ProgData[61] = (byte)2;
            }
            else if (EventControl == "AUNO")
            {
                ProgData[60] = (byte)87; //Event Control	61 	2 	BINARY
                ProgData[61] = (byte)2;
            }
            else if (EventControl == "ANO")
            {
                ProgData[60] = (byte)23; //Event Control	61 	2 	BINARY
                ProgData[61] = (byte)2;
            }
            //ProgData[60] = (byte)7; //Event Control	61 	2 	BINARY
            //ProgData[61] = (byte)2;
            //-----------------
            ProgData[62] = (byte)0; //Event Status 	63 	2 	BINARY 	00H (Used by system)
            ProgData[63] = (byte)0;



            //CompileID 		65 		8 		ASCII 		FFH (Fill With Null)
            //CompileSOM 		73 		4 		BCD		FFH (Fill With Null) 
            //BoxAID 		77 		8 		ASCII		FFH (Fill With Null)
            //BoxASOM 		85 		4 		BCD 		FFH (Fill With Null)
            //BoxBID 		89 		8 		ASCII		FFH (Fill With Null)
            //BoxBSOM 		97 		4 		BCD 		FFH (Fill With Null)
            for (int i = 64; i < 100; i++)
            {
                ProgData[i] = (byte)255;
            }


            //Reserved 		101 		1 		BINARY 	00H (Fill With ZERO Always)
            //Backup Device Maj. 	102		1 		BINARY 	00H
            //Backup Device Min. 	103 		1 		BINARY 	00H
            //Extended EventControl 	104		1		BINARY 	00H
            for (int i = 100; i < 104; i++)
            {
                ProgData[i] = (byte)0;
            }
            return ProgData;
        }

        public byte[] WritKeyByteArrayOut(ArrPromoListAddSOM TemArrPromoList, string striType, string VideoID, string Title, string PlayTime, string Dur, string SOM, string EventControl, string QF1, string QF2, string QF3, string QF4, string EF1, string EF2, string EF3, string ChannelType)
        {
            byte[] ProgData = new byte[104];
            ProgData[0] = (byte)int.Parse(striType);
            for (int i = 1; i < 9; i++)
            {
                ProgData[i] = (byte)255; //Reconcile Key 	2 	8 	ASCII 	FFH 
            }

            if (EF1 != "")
                ProgData[9] = (byte)int.Parse(EF1);
            else
                ProgData[9] = (byte)0;

            if (EF2 != "")
                ProgData[10] = (byte)int.Parse(EF2);
            else
                ProgData[10] = (byte)0;

            if (EF3 != "")
                ProgData[11] = (byte)int.Parse(EF3);
            else
                ProgData[11] = (byte)0;

            if (PlayTime == "")
            {
                ProgData[12] = (byte)255;
                ProgData[13] = (byte)255;
                ProgData[14] = (byte)255;
                ProgData[15] = (byte)255;
            }
            else
            {
                ProgData[12] = (byte)INT10To16(PlayTime.Substring(9, 2));
                ProgData[13] = (byte)INT10To16(PlayTime.Substring(6, 2));
                ProgData[14] = (byte)INT10To16(PlayTime.Substring(3, 2));
                ProgData[15] = (byte)INT10To16(PlayTime.Substring(0, 2));
            }
            //                    ID 		17 	8 	ASCII 	FFH if empty, 20H if used (Material ID)
            //TITLE		25 	16 	ASCII 	20H
            byte[] TempArray = new byte[16];

            //TempArray = StrToByteArray(VideoID, 8);
            TempArray = StrToByteArray("50" + QF1, 8);

            for (int i = 16; i < 24; i++)
            {
                ProgData[i] = TempArray[i - 16];
            }

            TempArray = StrToByteArray(GetSubString(Title, 16), 16);

            for (int i = 24; i < 40; i++)
            {
                ProgData[i] = TempArray[i - 24];
            }
            //SOM & 長度

            if (SOM != "")
            {
                ProgData[40] = (byte)INT10To16(SOM.Substring(9, 2));//DUR Frame 	45 	1 	BCD 	FFH
                ProgData[41] = (byte)INT10To16(SOM.Substring(6, 2));//DUR Second 	46 	1 	BCD 	FFH
                ProgData[42] = (byte)INT10To16(SOM.Substring(3, 2));//DUR Minute 	47 	1 	BCD 	FFH
                ProgData[43] = (byte)INT10To16(SOM.Substring(0, 2)); //DUR Hour 	48 	1 	BCD 	FFH
            }
            else
            {
                ProgData[40] = (byte)255; //SOM Frame 	41 	1 	BCD 	FFH
                ProgData[41] = (byte)255; //SOM Frame 	41 	1 	BCD 	FFH
                ProgData[42] = (byte)255; //SOM Frame 	41 	1 	BCD 	FFH
                ProgData[43] = (byte)255; //SOM Frame 	41 	1 	BCD 	FFH
            }

            if (Dur == "")
            {
                ProgData[44] = (byte)255;
                ProgData[45] = (byte)255;
                ProgData[46] = (byte)255;
                ProgData[47] = (byte)255;
            }
            else
            {
                ProgData[44] = (byte)INT10To16(Dur.Substring(9, 2));//DUR Frame 	45 	1 	BCD 	FFH
                ProgData[45] = (byte)INT10To16(Dur.Substring(6, 2));//DUR Second 	46 	1 	BCD 	FFH
                ProgData[46] = (byte)INT10To16(Dur.Substring(3, 2));//DUR Minute 	47 	1 	BCD 	FFH
                ProgData[47] = (byte)INT10To16(Dur.Substring(0, 2)); //DUR Hour 	48 	1 	BCD 	FFH
            }
            ProgData[48] = (byte)0; // Channel 		49 	1	BINARY 	00H 

            if (QF4 != "")
                ProgData[49] = (byte)int.Parse(QF4);
            else
                ProgData[49] = (byte)0;
            //ProgData[49] = (byte)0; // Qualifier4 	50 	1 	BINARY 	00H (Fill With Null) 

            if (TemArrPromoList.FCTYPE == "G" && striType == "0")
            {
                //if (ChannelType == "002")
                //    ProgData[50] = (byte)int.Parse(TemArrPromoList.FNBREAK_NO);
                //else
                    ProgData[50] = (byte)INT10To16(string.Format("{0:00}", int.Parse(TemArrPromoList.FNBREAK_NO)));
            }
            else
                ProgData[50] = (byte)255; //Segment Number 51 	1 	BCD 	FFH Program Segment Number

            //Device Maj. 	52 	1 	BINARY 	00H
            //Device Min. 	53 	1 	BINARY 	00H
            //Bin High 		54 	1 	BINARY 	00H
            //Bin Low 		55 	1 	BINARY 	00H
            //Qualifier1		56 	1 	BINARY 	00H (Fill With Null) 
            //Qualifier2		57 	1 	BINARY 	00H (Fill With Null)
            //Qualifier3		58 	1	BINARY 	00H (Fill With Null)
            for (int i = 51; i < 55; i++)
            {
                ProgData[i] = (byte)0;
            }

            if (QF1 != "")
                ProgData[55] = (byte)int.Parse(QF1);
            else
                ProgData[55] = (byte)0;

            if (QF2 != "")
                ProgData[56] = (byte)int.Parse(QF2);
            else
                ProgData[56] = (byte)0;

            if (QF3 != "")
                ProgData[57] = (byte)int.Parse(QF3);
            else
                ProgData[57] = (byte)0;


            ProgData[58] = (byte)255;  //DateToAir 	59 	2 	WORD 	FFH (Julian Type for Asrun)
            ProgData[59] = (byte)255;
            //ProgData[58] = (byte)125;  //DateToAir 	59 	2 	WORD 	FFH (Julian Type for Asrun)
            //ProgData[59] = (byte)156;

            //            Const CTL_A = &H7 'A=>Autoplay(01)+Autothread(02)+Autoswitch(04)
            //Const CTL_AO = &H17  'AO=A+Hardtime(10)
            //Const CTL_AUN = &H247    'ANU=A+Automarktime(200)+uppercounter(40)
            //Const CTL_AN = &H207 'AN=A+Automarktime(200)
            //Const CTL_AUNO = &H257  'AUNO=AUN+Hardtime(10)
            //Const CTL_ANO = &H217  'ANO=AN+Hardtime(10)
            //Event Control
            if (EventControl == "")
            {
                ProgData[60] = (byte)0; //Event Control	61 	2 	BINARY
                ProgData[61] = (byte)0;
            }
            else if (EventControl == "AO")
            {
                ProgData[60] = (byte)23; //Event Control	61 	2 	BINARY
                ProgData[61] = (byte)0;
            }
            else if (EventControl == "A")
            {
                ProgData[60] = (byte)7; //Event Control	61 	2 	BINARY
                ProgData[61] = (byte)0;
            }
            else if (EventControl == "AUN")
            {
                ProgData[60] = (byte)71; //Event Control	61 	2 	BINARY
                ProgData[61] = (byte)2;
            }
            else if (EventControl == "AN")
            {
                ProgData[60] = (byte)7; //Event Control	61 	2 	BINARY
                ProgData[61] = (byte)2;
            }
            else if (EventControl == "AUNO")
            {
                ProgData[60] = (byte)87; //Event Control	61 	2 	BINARY
                ProgData[61] = (byte)2;
            }
            else if (EventControl == "ANO")
            {
                ProgData[60] = (byte)23; //Event Control	61 	2 	BINARY
                ProgData[61] = (byte)2;
            }
            //ProgData[60] = (byte)7; //Event Control	61 	2 	BINARY
            //ProgData[61] = (byte)2;
            //-----------------
            ProgData[62] = (byte)0; //Event Status 	63 	2 	BINARY 	00H (Used by system)
            ProgData[63] = (byte)0;



            //CompileID 		65 		8 		ASCII 		FFH (Fill With Null)
            //CompileSOM 		73 		4 		BCD		FFH (Fill With Null) 
            //BoxAID 		77 		8 		ASCII		FFH (Fill With Null)
            //BoxASOM 		85 		4 		BCD 		FFH (Fill With Null)
            //BoxBID 		89 		8 		ASCII		FFH (Fill With Null)
            //BoxBSOM 		97 		4 		BCD 		FFH (Fill With Null)
            for (int i = 64; i < 100; i++)
            {
                ProgData[i] = (byte)255;
            }


            //Reserved 		101 		1 		BINARY 	00H (Fill With ZERO Always)
            //Backup Device Maj. 	102		1 		BINARY 	00H
            //Backup Device Min. 	103 		1 		BINARY 	00H
            //Extended EventControl 	104		1		BINARY 	00H
            for (int i = 100; i < 104; i++)
            {
                ProgData[i] = (byte)0;
            }
            return ProgData;
        }

        public string WriteTXTProgList(string VideoID, string PlayTime, string Dur, string SOM, string FCTYPE, string FCPLAYTYPE, string FSPROG_NAME, string FNLIVE, string FSSIGNAL)
        {
            string ReString = "";
            if (FNLIVE == "1")
                ReString = ReString + FSSIGNAL + "\t";
            else
                ReString = ReString + "/fs0/clip.dir/" + VideoID + ".mxf" + "\t";
            

            if (int.Parse(PlayTime.Substring(0, 2)) > 24)
                ReString = ReString + string.Format("{0:00}", int.Parse(PlayTime.Substring(0, 2)) - 24) + PlayTime.Substring(2, 9) + "\t";
            else
                ReString = ReString + PlayTime + "\t";

            string Etime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(Dur));

            if (int.Parse(Etime.Substring(0, 2)) > 24)
                ReString = ReString + string.Format("{0:00}", int.Parse(Etime.Substring(0, 2)) - 24) + Etime.Substring(2, 9) + "\t";
            else
                ReString = ReString + Etime + "\t";

            ReString = ReString + Dur + "\t";
            ReString = ReString + "\t\t";
            ReString = ReString + FCPLAYTYPE + "\t";
            //if (FCTYPE == "G")
            //    ReString = ReString + "hard" + "\t";
            //else if (FNLIVE=="1")
            //    ReString = ReString + "manual" + "\t";
            //else
            //    ReString = ReString + "auto" + "\t";

            if (FCTYPE == "G")
                ReString = ReString + "SOM:" + SOM + " - " + FSPROG_NAME + "\t";
            else
                ReString = ReString + FSPROG_NAME + "\t";
            ReString = ReString + "0" + "\t";
            if (FCTYPE == "G")
                ReString = ReString + SOM;
           
            return ReString;
        }

        public string WriteTXTProgOnly(string VideoID, string PlayTime, string Dur, string SOM, string FCTYPE, string FCPLAYTYPE, string FSPROG_NAME,string FNLIVE,string FSSIGNAL)
        {
            string ReString = "";
            if (FNLIVE=="1")
                ReString = ReString + FSSIGNAL + "\t";
            else
              ReString = ReString + "/fs0/clip.dir/" + VideoID + ".mxf" + "\t";
            ReString = ReString + "00:00:00:00" + "\t";

            ReString = ReString + "00:00:00:00" + "\t";

            ReString = ReString + Dur + "\t";
            ReString = ReString + "\t\t";
            ReString = ReString + FCPLAYTYPE + "\t";
            //if (FCTYPE == "G")
            //    ReString = ReString + "hard" + "\t";
            //else if (FNLIVE=="1")
            //    ReString = ReString + "manual" + "\t";
            //else
            //    ReString = ReString + "auto" + "\t";

            if (FCTYPE == "G")
                ReString = ReString + "SOM:" + SOM + " - " + FSPROG_NAME + "\t";
            else
                ReString = ReString + FSPROG_NAME + "\t";
            ReString = ReString + "0" + "\t";
            ReString = ReString + SOM;
            return ReString;
        }

        public string WriteTXTProgOnlyList(ArrPromoListAddSOM TemArrPromoList, string striType, string VideoID, string Title, string PlayTime, string Dur, string SOM, string EventControl, string QF1, string QF2, string QF3, string QF4, string EF1, string EF2, string EF3, string ChannelType)
        {
            string ReString = "";
            ReString = ReString + "/fs0/clip.dir/" + VideoID + ".mxf" + "\t";
            ReString = ReString + "00:00:00:00" + "\t";

            ReString = ReString + "00:00:00:00" + "\t";

            ReString = ReString + Dur + "\t";
            ReString = ReString + "\t\t";
            ReString = ReString + "auto" + "\t";
            ReString = ReString + "SOM:" + SOM + " - " + TemArrPromoList.FSPROG_NAME + "\t";
            ReString = ReString + "0" + "\t";
            ReString = ReString + SOM;
            return ReString;
        }

        public class LouthRecord
        {
            public Byte Type { get; set; }             //1  Type           1  1  BINARY   00
            public Byte[] Reconcile { get; set; }      //2  Reconcile Key  2  8  ASCII FF
            public Byte Effect1 { get; set; }          //3  Effect1        10 1  BCD   00
            public Byte Effect2 { get; set; }          //4  Effect2        11 1  BCD   00
            public Byte Effect3 { get; set; }          //5  Effect3        12 1  BCD   00
            public Byte AirFrame { get; set; }         //6  AirFrame       13 1  BCD   FF
            public Byte AirSecond { get; set; }        //7  AirSecond      14 1  BCD   FF
            public Byte AirMinute { get; set; }        //8  AirMinute      15 1  BCD   FF
            public Byte AirHour { get; set; }          //9  AirHour        16 1  BCD   FF
            public string ID { get; set; }             //10  ID            17 8  string 空白為FF,不滿為20
            public string Title { get; set; }          //11  Title         25 16  string 20
            public Byte SOMFrame { get; set; }         //12  SOMFrame      41 1  BCD   FF
            public Byte SOMSecond { get; set; }        //13  SOMSecond      42 1  BCD   FF
            public Byte SOMMinute { get; set; }        //14  SOMMinute      43 1  BCD   FF
            public Byte SOMHour { get; set; }          //15  SOMHour        44 1  BCD   FF
            public Byte DURFrame { get; set; }         //16  DURFrame       45 1  BCD   FF
            public Byte DURSecond { get; set; }        //17  DURSecond      46 1  BCD   FF
            public Byte DURMinute { get; set; }        //18  DURMinute      47 1  BCD   FF
            public Byte DURHour { get; set; }          //19  DURHour        48 1  BCD   FF
            public Byte Channel { get; set; }          //20  Channel        49 1  BINARY   00
            public Byte Qualifier4 { get; set; }       //21  Qualifier4     50 1  BINARY   00
            public Byte SegmentNumber { get; set; }    //22  SegmentNumber  51 1  BINARY   FF
            public Byte DeviceMaj { get; set; }        //23  DeviceMaj      52 1  BINARY   00
            public Byte DeviceMin { get; set; }        //24  DeviceMin      53 1  BINARY   00
            public Byte BinHigh { get; set; }          //25  BinHigh        54 1  BINARY   00
            public Byte BinLow { get; set; }           //26  BinLow         55 1  BINARY   00
            public Byte Qualifier1 { get; set; }       //27  Qualifier1     56 1  BINARY   00
            public Byte Qualifier2 { get; set; }       //28  Qualifier2     57 1  BINARY   00
            public Byte Qualifier3 { get; set; }       //29  Qualifier3     58 1  BINARY   00
            public Byte[] DateToAir { get; set; }      //30  DateToAir      59 2  BINARY   00
            public Byte[] EventControl { get; set; }   //31  EventControl   61 2  BINARY   00
            public Byte[] EventStatus { get; set; }    //32  EventStatus    63 2  BINARY   00
            public Byte[] CompileID { get; set; }      //33  CompileID      65 8  BINARY   00
            public Byte[] CompileSOM { get; set; }     //34  CompileSOM     73 4  BINARY   00
            public Byte[] BoxAID { get; set; }         //35  BoxAID         77 8  BINARY   00
            public Byte[] BoxASOM { get; set; }        //36  BoxASOM        85 4  BINARY   00
            public Byte[] BoxBID { get; set; }         //37  BoxBID         89 8  BINARY   00
            public Byte[] BoxBSOM { get; set; }        //38  BoxBSOM        97 4  BINARY   00
            public Byte Reserved { get; set; }         //39  Reserved       101 1  BINARY   00
            public Byte BackupDeviceMaj { get; set; }  //40  BackupDeviceMaj     102 1  BINARY   00
            public Byte BackupDeviceMin { get; set; }  //41  BackupDeviceMin     103 1  BINARY   00
            public Byte ExtendedEventControl { get; set; }    //42  ExtendedEventControl     104 1  BINARY   00

        }

        public class LouthTableData
        {
            public string FSGROUP { get; set; }
            public string FSNAME { get; set; }
            public string FSNO { get; set; }
            public string FSMEMO { get; set; }
            public string FSLayel { get; set; }
            public string FSRULE { get; set; }
        }

        public class FilingTableData
        {
            public string FSPROG_ID { get; set; }
            public string FNEPISODE { get; set; }
            public string FSFILE_NO { get; set; }
            public string fsprog_name { get; set; }
            public string FSVIDEO_ID { get; set; }
            public string FNSEG_ID { get; set; }
            public string FSBEG_TIMECODE { get; set; }
            public string FSEND_TIMECODE { get; set; }
            public string FSDURATION { get; set; }
        }

        string[] TimeStatus1;
        public class TimeStatus
        {
            public string SStartTime { get; set; }
            public string SInteval { get; set; }
            public string SDur { get; set; }
            public string SSec { get; set; }
        }

        public class SEndPointStatus
        {
            public string SEndPoint { get; set; }
            public string SDur { get; set; }
        }

        public TimeStatus Trans(string Instring)
        {
            TimeStatus RTimeStatus = new TimeStatus();
            TimeStatus1 = Instring.Split(new Char[] { ',' });
            if (Instring.IndexOf(",") > -1)//代表有 間隔,長度
            {
                RTimeStatus.SStartTime = TimeStatus1[0];
                RTimeStatus.SInteval = TimeStatus1[1];
                RTimeStatus.SDur = TimeStatus1[2];

                try
                {
                    RTimeStatus.SSec = TimeStatus1[3];
                }
                catch
                {
                    RTimeStatus.SSec = "0";
                }
            }
            else
            {
                RTimeStatus.SStartTime = TimeStatus1[0];
                RTimeStatus.SInteval = "";
                RTimeStatus.SDur = "";
                RTimeStatus.SSec = "0";
            }
            return RTimeStatus;
        }

        public SEndPointStatus Trans1(string Instring)
        {
            SEndPointStatus REndPointStatus = new SEndPointStatus();
            TimeStatus1 = Instring.Split(new Char[] { ',' });
            if (Instring.IndexOf(",") > -1)//代表有 最後播放時間,長度
            {
                REndPointStatus.SEndPoint = TimeStatus1[0];
                REndPointStatus.SDur = TimeStatus1[1];
            }
            else
            {
                REndPointStatus.SEndPoint = TimeStatus1[0];
                REndPointStatus.SDur = "";
            }
            return REndPointStatus;
        }

        public LouthTableData GetLouthData(string LouthName, string ChannelID)
        {
            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            StringBuilder sb = new StringBuilder();


            SqlDataAdapter da = new SqlDataAdapter("SP_Q_TBPGM_LOUTH_KEY_BY_NAME", connection);
            DataTable dt = new DataTable();
            SqlParameter para = new SqlParameter("@FSNAME", LouthName);
            SqlParameter para1 = new SqlParameter("@FSCHANNEL_ID", ChannelID);

            da.SelectCommand.Parameters.Add(para);
            da.SelectCommand.Parameters.Add(para1);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            LouthTableData ReTurnLouth = new LouthTableData();
            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                da.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    if (Convert.IsDBNull(dt.Rows[0]["FSGROUP"]))
                        ReTurnLouth.FSGROUP = "";
                    else
                        ReTurnLouth.FSGROUP = dt.Rows[0]["FSGROUP"].ToString();
                    if (Convert.IsDBNull(dt.Rows[0]["FSNAME"]))
                        ReTurnLouth.FSNAME = "";
                    else
                        ReTurnLouth.FSNAME = dt.Rows[0]["FSNAME"].ToString();
                    if (Convert.IsDBNull(dt.Rows[0]["FSNO"]))
                        ReTurnLouth.FSNO = "";
                    else
                        ReTurnLouth.FSNO = dt.Rows[0]["FSNO"].ToString();
                    if (Convert.IsDBNull(dt.Rows[0]["FSLayel"]))
                        ReTurnLouth.FSLayel = "";
                    else
                        ReTurnLouth.FSLayel = dt.Rows[0]["FSLayel"].ToString();
                    if (Convert.IsDBNull(dt.Rows[0]["FSMEMO"]))
                        ReTurnLouth.FSMEMO = "";
                    else
                        ReTurnLouth.FSMEMO = dt.Rows[0]["FSMEMO"].ToString();
                    if (Convert.IsDBNull(dt.Rows[0]["FSRULE"]))
                        ReTurnLouth.FSRULE = "";
                    else
                        ReTurnLouth.FSRULE = dt.Rows[0]["FSRULE"].ToString();
                }
                else
                {
                    ReTurnLouth.FSGROUP = "";
                    ReTurnLouth.FSNAME = "";
                    ReTurnLouth.FSNO = "";
                    ReTurnLouth.FSLayel = "";
                    ReTurnLouth.FSMEMO = "";
                    ReTurnLouth.FSRULE = "";
                }
                connection.Close();
                connection.Dispose();
                GC.Collect();
                return ReTurnLouth;
            }
            catch (Exception ex)
            {
                return ReTurnLouth;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }
        }

        public int TimecodeToLouthTimecode(string InTime)
        {
            int RTime;
            RTime = int.Parse(InTime.Substring(0, 1)) * 16 * 16 * 16 * 16 * 16 * 16 * 16;
            RTime = RTime + int.Parse(InTime.Substring(1, 1)) * 16 * 16 * 16 * 16 * 16 * 16;
            RTime = RTime + int.Parse(InTime.Substring(3, 1)) * 16 * 16 * 16 * 16 * 16;
            RTime = RTime + int.Parse(InTime.Substring(4, 1)) * 16 * 16 * 16 * 16;
            RTime = RTime + int.Parse(InTime.Substring(6, 1)) * 16 * 16 * 16;
            RTime = RTime + int.Parse(InTime.Substring(7, 1)) * 16 * 16;
            RTime = RTime + int.Parse(InTime.Substring(9, 1)) * 16;
            RTime = RTime + int.Parse(InTime.Substring(10, 1));
            return RTime;
        }
        //轉換LST File

        //檢查首播日期,如果沒有或者比PlayDate大,則將PlayDate寫入
        public bool Check_First_Play(string VideoID, string PlayDate, string channel_ID)
        {

            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            StringBuilder sb = new StringBuilder();

            SqlDataAdapter da = new SqlDataAdapter("SP_Q_TBLOG_VIDEO_SEG_BY_VIDEOID", connection);
            DataTable dt = new DataTable();

            SqlParameter para = new SqlParameter("@FSVIDEO_ID", VideoID);
            da.SelectCommand.Parameters.Add(para);

            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                da.Fill(dt);

                if (dt.Rows.Count > 0)　//如果等於零表示還沒有段落資料,則不用作業,理論上不會有此情況,因為TBLOG_VIDEO有VIDEOID,就會建立TBLOG_VIDEO_SEG資料
                {
                    //                     SELECT 
                    //ISNULL(CONVERT(VARCHAR(20) ,FDDISPLAY_TIME , 111),'') as FDDISPLAY_TIME,FSDISPLAY_CHANNEL_ID,GETDATE() sysdate
                    // FROM TBLOG_VIDEO_SEG 
                    if (dt.Rows[0][0].ToString() == "" || (DateTime.Parse(PlayDate) < DateTime.Parse(dt.Rows[0][0].ToString()) && DateTime.Parse(dt.Rows[0][2].ToString()) < DateTime.Parse(PlayDate)))
                    { //當首播日期為空值或
                        //首播日期>播出日期　而且　播出日期　>系統日期
                        //此時必須修改首播日期與頻道

                        SqlConnection connection1 = new SqlConnection(sqlConnStr);
                        SqlTransaction tran = null;
                        SqlCommand sqlcmd = new SqlCommand("SP_U_TBLOG_VIDEO_SEG_BY_VIDEO_ID", connection1);

                        SqlParameter paraVideoID = new SqlParameter("@FSVIDEO_ID", VideoID);
                        sqlcmd.Parameters.Add(paraVideoID);
                        SqlParameter paraDisPlayTime = new SqlParameter("@FDDISPLAY_TIME", PlayDate);
                        sqlcmd.Parameters.Add(paraDisPlayTime);
                        SqlParameter paraChannelID = new SqlParameter("@FSDISPLAY_CHANNEL_ID", channel_ID);
                        sqlcmd.Parameters.Add(paraChannelID);
                        sqlcmd.CommandType = CommandType.StoredProcedure;

                        try
                        {
                            if (connection1.State == System.Data.ConnectionState.Closed)
                            {
                                connection1.Open();
                            }

                            tran = connection1.BeginTransaction();
                            sqlcmd.Transaction = tran;
                            if (sqlcmd.ExecuteNonQuery() > 0)
                            {
                                tran.Commit();
                                //return true;
                            }
                            else
                            {
                                //不須知道異動筆數
                                tran.Commit();
                                //tran.Rollback();
                                //return true;
                            }

                        }
                        catch (Exception ex)
                        {
                            tran.Rollback();
                            return false;
                        }
                        finally
                        {
                            connection1.Close();
                            connection1.Dispose();
                            sqlcmd.Dispose();
                            GC.Collect();

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }
            return true;
        }


        //清除節目表首播紀錄
        public bool Clear_First_Play(string PlayDate, string channel_ID)
        {
            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;

            SqlConnection connection1 = new SqlConnection(sqlConnStr);
            SqlTransaction tran = null;
            SqlCommand sqlcmd = new SqlCommand("SP_U_TBLOG_VIDEO_SEG_CLEAR_FIRST_PLAY_LIST", connection1);

            SqlParameter paraDisPlayTime = new SqlParameter("@FDDISPLAY_TIME", PlayDate);
            sqlcmd.Parameters.Add(paraDisPlayTime);
            SqlParameter paraChannelID = new SqlParameter("@FSDISPLAY_CHANNEL_ID", channel_ID);
            sqlcmd.Parameters.Add(paraChannelID);
            sqlcmd.CommandType = CommandType.StoredProcedure;

            try
            {
                if (connection1.State == System.Data.ConnectionState.Closed)
                {
                    connection1.Open();
                }

                tran = connection1.BeginTransaction();
                sqlcmd.Transaction = tran;
                if (sqlcmd.ExecuteNonQuery() > 0)
                {
                    tran.Commit();
                    //return true;
                }
                else
                {
                    //不須知道異動筆數
                    tran.Commit();
                    //tran.Rollback();
                    //return true;
                }

            }
            catch (Exception ex)
            {
                tran.Rollback();
                return false;
            }
            finally
            {
                connection1.Close();
                connection1.Dispose();
                sqlcmd.Dispose();
                GC.Collect();

            }

            return true;
        }

        ///20131028 改為取得首派清單
        //產生首播清單檔案
        public bool Create_First_Play_List(string WriteFileName, string PlayDate, string channel_ID, string ListType, string ChannelType)
        {
            if (File.Exists(HttpContext.Current.Server.MapPath("~/UploadFolder/" + WriteFileName)) == true)
                File.Delete(HttpContext.Current.Server.MapPath("~/UploadFolder/" + WriteFileName));
            FileStream myFile = File.Create(HttpContext.Current.Server.MapPath("~/UploadFolder/" + WriteFileName));
            BinaryWriter myWriter = new BinaryWriter(myFile);

            //先查詢首播清單

            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            StringBuilder sb = new StringBuilder();

            ///SqlDataAdapter da = new SqlDataAdapter("SP_Q_FIRST_PLAY_LIST", connection);
            //SqlDataAdapter da = new SqlDataAdapter("SP_Q_GET_First_Post_List", connection);
            SqlDataAdapter da = new SqlDataAdapter("SP_Q_MAIN_CONTROL_CHECK_LIST", connection);


            DataTable dt = new DataTable();

            SqlParameter para = new SqlParameter("@FDDISPLAY_TIME", PlayDate);
            da.SelectCommand.Parameters.Add(para);
            SqlParameter para1 = new SqlParameter("@FSDISPLAY_CHANNEL_ID", channel_ID);
            da.SelectCommand.Parameters.Add(para1);
            string ParaNum = "";
            if (ListType == "G")
                ParaNum = "10";
            else
                ParaNum = "8";

            //SqlParameter para2 = new SqlParameter("@LISTTYPE", ParaNum);
            //da.SelectCommand.Parameters.Add(para2); 
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                da.Fill(dt);

                if (dt.Rows.Count > 0)　//如果等於零表示沒有首播清單
                {

                    //                     select A.FSTITLE,CONVERT(VARCHAR(20) ,B.FDDISPLAY_TIME , 111) as FDDISPLAY_TIME ,B.FSVIDEO_ID
                    //,B.FNSEG_ID,B.FSBEG_TIMECODE,FSEND_TIMECODE
                    //from 
                    //TBLOG_VIDEO A,
                    //tblog_video_seg B
                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        //dt.Rows[i][0].ToString()= A.FSTITLE
                        //dt.Rows[i][1].ToString()= FDDISPLAY_TIME
                        //dt.Rows[i][2].ToString()= FSVIDEO_ID
                        //dt.Rows[i][3].ToString()= FNSEG_ID
                        //dt.Rows[i][4].ToString()= FSBEG_TIMECODE
                        //dt.Rows[i][5].ToString()= FSEND_TIMECODE

                        ArrPromoListAddSOM TemArrPromoList = new ArrPromoListAddSOM();
                        TemArrPromoList.FSVIDEO_ID = dt.Rows[i][2].ToString();
                        if (dt.Rows[i][2].ToString().Length == 8)
                        {
                            TemArrPromoList.FCTYPE = "P";  //Promo
                            TemArrPromoList.FSSIGNAL = "COMM";
                        }
                        else
                        {
                            TemArrPromoList.FCTYPE = "G"; //節目
                            TemArrPromoList.FSSIGNAL = "PGM";
                        }

                        TemArrPromoList.FNBREAK_NO = dt.Rows[i][3].ToString();
                        TemArrPromoList.FSNAME = dt.Rows[i][0].ToString();
                        if ((dt.Rows[i][4].ToString() == "") || (dt.Rows[i][5].ToString() == ""))
                        {
                            TemArrPromoList.FSDURATION = "";
                        }
                        else
                        {

                            TemArrPromoList.FSDURATION = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dt.Rows[i][5].ToString()) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dt.Rows[i][4].ToString()));
                        }
                        TemArrPromoList.FSSOM = dt.Rows[i][4].ToString();
                        string striType = "0";
                        string EventCOntrol = "";
                        string QF1 = "0";
                        string QF2 = "0";
                        string QF3 = "0";
                        string QF4 = "0";
                        string EF1 = "0";
                        string EF2 = "0";
                        string EF3 = "0";
                        string VideoID = TemArrPromoList.FSVIDEO_ID;
                        string Title = TemArrPromoList.FSNAME;
                        string PlayTime = "00:00:00:00";
                        string Dur = TemArrPromoList.FSDURATION;
                        string SOM = TemArrPromoList.FSSOM;

                        //HD頻道只放六碼
                        if (ChannelType == "002")
                        {
                            if (VideoID.Length >= 8)
                                VideoID = VideoID.Substring(2, 6);
                        }

                        TemArrPromoList.FNEPISODE = dt.Rows[i][6].ToString();
                        if ((TemArrPromoList.FCTYPE == "G") || (TemArrPromoList.FNBREAK_NO == "1"))
                            EventCOntrol = "AN";
                        else
                            EventCOntrol = "A";
                        if ((TemArrPromoList.FCTYPE == "G") && (TemArrPromoList.FNLIVE == "1")) //Live節目
                        {
                            if (TemArrPromoList.FCTIME_TYPE == "O")
                                EventCOntrol = "AUNO";
                            else
                                EventCOntrol = "AUN";

                        }

                        if (TemArrPromoList.FCTYPE == "G")  //,節目時 EF=FADE-FADE ,FAST  轉場效果
                        {
                            EF1 = "3";
                            EF2 = "0";
                            EF3 = "2";
                        }
                        //myWriter.Write(WriteProgByte(TemArrPromoList));
                        //myWriter.Write(WriteLouthByteArray(TemArrPromoList,string striType,string VideoID,string Title, string PlayTime,string Dur,string SOM,string EventControl,string QF1,string QF2,string QF3,string QF4,string EF1,string EF2,string EF3));
                        //寫入表中的資料,節目或宣傳帶
                        if (TemArrPromoList.FCTYPE == "G") //首播清單只要有節目的即可,不要加入短帶
                            myWriter.Write(WriteLouthByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3, ChannelType));

                        // sb.AppendLine("<" + dt.Columns[j].ColumnName + ">" + ReplaceXML(dt.Rows[i][j].ToString()) + "</" + dt.Columns[j].ColumnName + ">");
                    }

                }
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }


            myFile.Close();
            myFile.Dispose();
            return true;
        }

        //只比對段落長度還不夠,還需要比對目前架構和如果'重載仔入後的架構的相異處,並將相異的地方列出
        //1.目前架構有但重新載入後消失
        //2.重新載入架構有但目前並沒有
        //3.Promo目前長度與重新載入後長度不同

        //比對節目架構與播出運行表的異常如有不同的資料則寫入此Class中
        public class CheckDiffList
        {
            public string NowProgID { get; set; }  //目前Combine中之ProgID,or目前Arr_Promo中的PromoID
            public string NowProgName { get; set; }  //目前Combine中之ProgName,or目前Arr_Promo中的PromoName
            public string NowProgEpisode { get; set; }  //目前Combine中之ProgEpisode
            public string NowProgSeg { get; set; }  //目前Combine中之ProgSeg
            public string NowProgDur { get; set; }  //目前Combine中之ProgDur
            public string NowProgVIdeoID { get; set; }  //目前Combine中之VideoID

            public string QueueProgID { get; set; }  //目前節目表中之ProgID
            public string QueueProgName { get; set; }  //目前節目表中之ProgName
            public string QueueProgEpisode { get; set; }  //目前節目表中之ProgEpisode
            public string QueueProgSeg { get; set; }  //目前節目表中之ProgSeg
            public string QueueProgDur { get; set; }  //目前節目表中之ProgDur
            public string QueueProgVIdeoID { get; set; }  //目前節目表中之VideoID
            public string FSSTATUS { get; set; }  //不同的狀態
        }

        //取得節目段落資料,如有段落資料則取段落資料,如果沒有則產生預設段落
        public DataTable getProgSegment(string FSProgID, string FNEpisode, string FNSEQNO, string FSChannel_ID, string FNDUR, string FNDEF_MIN, string FNDEF_SEC)
        {
            //DataTable dtProgSeg = new DataTable();
            //每一個節目,先查詢節目段落看是否有資料
            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection1 = new SqlConnection(sqlConnStr);
            if (connection1.State == System.Data.ConnectionState.Closed)
            {
                connection1.Open();
            }
            StringBuilder sb = new StringBuilder();
            SqlDataAdapter daProgSeg = new SqlDataAdapter("SP_Q_TBPGM_PROG_SEG", connection1);
            DataTable dtProgSeg = new DataTable();
            DataTable dtProgSegDefault = new DataTable();
            SqlParameter paraPROGID = new SqlParameter("@FSPROG_ID", FSProgID);
            daProgSeg.SelectCommand.Parameters.Add(paraPROGID);
            SqlParameter paraEPISODE = new SqlParameter("@FNEPISODE", FNEpisode);
            daProgSeg.SelectCommand.Parameters.Add(paraEPISODE);
            SqlParameter paraChannelID = new SqlParameter("@FSCHANNEL_ID", FSChannel_ID);
            daProgSeg.SelectCommand.Parameters.Add(paraChannelID);
            daProgSeg.SelectCommand.CommandType = CommandType.StoredProcedure;
            daProgSeg.Fill(dtProgSeg);
            if (dtProgSeg.Rows.Count > 0)
            {
                return dtProgSeg;
            }
            else //如果查無資料則採用預設段落 預設段落先給一段,如果他們之後有規則的話
            {

                StringBuilder sbBankDetail = new StringBuilder();
                SqlDataAdapter daBankDetail = new SqlDataAdapter("SP_Q_TBPGM_BANK_DETAIL", connection1);
                DataTable dtBankDetail = new DataTable();
                SqlParameter paraBankPROGID = new SqlParameter("@FSPROG_ID", FSProgID);
                daBankDetail.SelectCommand.Parameters.Add(paraBankPROGID);
                SqlParameter paraBankFNSEQNO = new SqlParameter("@FNSEQNO", FNSEQNO);
                daBankDetail.SelectCommand.Parameters.Add(paraBankFNSEQNO);
                SqlParameter paraBankProgName = new SqlParameter("@FSPROG_NAME", "");
                daBankDetail.SelectCommand.Parameters.Add(paraBankProgName);
                daBankDetail.SelectCommand.CommandType = CommandType.StoredProcedure;
                daBankDetail.Fill(dtBankDetail);

                string BDSeg;
                string BDDur;
                if (dtBankDetail.Rows.Count > 0)
                {
                    BDSeg = dtBankDetail.Rows[0]["FNSEG"].ToString();
                    BDDur = dtBankDetail.Rows[0]["FNDUR"].ToString();
                }
                else
                {
                    BDSeg = "1";
                    BDDur = ((int)Math.Floor(int.Parse(FNDUR) * 0.8)).ToString();
                    //BDDur = FNDUR;
                }
                if (BDSeg == "0")
                {
                    BDSeg = "1";
                    //BDDur = FNDUR;
                }
                if (FNDEF_MIN == "0" && FNDEF_SEC == "0")
                {
                    BDDur = ((int)Math.Floor(int.Parse(FNDUR) * 0.8)).ToString();
                }
                dtProgSegDefault.Columns.Add("FSID", typeof(string));
                dtProgSegDefault.Columns.Add("FNEPISODE", typeof(string));
                dtProgSegDefault.Columns.Add("FSBEG_TIMECODE", typeof(string));
                dtProgSegDefault.Columns.Add("FSEND_TIMECODE", typeof(string));
                dtProgSegDefault.Columns.Add("FCLOW_RES", typeof(string));
                dtProgSegDefault.Columns.Add("FNSEG_ID", typeof(string));
                dtProgSegDefault.Columns.Add("FNDUR_ID", typeof(string));
                dtProgSegDefault.Columns.Add("FSVIDEO_ID", typeof(string));
                dtProgSegDefault.Columns.Add("FSFILE_NO", typeof(string));


                //int NewSeg = 1;

                //if (int.Parse(BDSeg) != 1)
                //{
                int n = 1;
                //int allDur =(int)Math.Floor(int.Parse(FNDUR) * 0.8);
                int allDur = int.Parse(FNDEF_MIN); //剩餘所有節目長度
                int nowDur;  //目前剩餘節目長度
                string nowDurString;
                //BDSeg 表示節目段數
                //nowDur = (int)(Math.Floor((double)allDur / (double)int.Parse(BDSeg)));  //此為採用平均分配

                //規則為 當剩餘長度大於剩餘段落*10則除了最後一段外都為10分鐘,不然就採平均
                if (int.Parse(FNDEF_MIN) > int.Parse(BDSeg) * 10)
                    nowDur = 10;
                else
                    nowDur = (int)(Math.Floor((double)allDur / (double)int.Parse(BDSeg)));

                while (n <= int.Parse(BDSeg))
                {
                    //n表示目前作業為哪段

                    nowDurString = MinToTimecode(nowDur);
                    if (n != int.Parse(BDSeg))
                        dtProgSegDefault.Rows.Add(FSProgID, FNEpisode, "00:00:00:00", nowDurString, "", n.ToString(), FNDUR, "", "");
                    else
                        dtProgSegDefault.Rows.Add(FSProgID, FNEpisode, "00:00:00:00", MinToTimecode(allDur).Substring(0, 6) + String.Format("{0:00}", int.Parse(FNDEF_SEC)) + MinToTimecode(allDur).Substring(8, 3), "", n.ToString(), FNDUR, "", "");

                    allDur = allDur - nowDur;
                    //Console.WriteLine("Current value of n is {0}", n);
                    n++;
                }
                //}
                //else
                //{
                //    dtProgSegDefault.Rows.Add(FSProgID, FNEpisode, "00:00:00:00", MinToTimecode(int.Parse(BDDur)), "", 1, FNDUR, "", "");
                //}
                return dtProgSegDefault;
            }
        }


        //取得節目段落資料,如有段落資料則取段落資料,如果沒有則產生預設段落
        public DataTable getProgSegment_MIX(string FSProgID, string FNEpisode, string FNSEQNO, string FSChannel_ID, string FNDUR, string FNDEF_MIN, string FNDEF_SEC)
        {
            //DataTable dtProgSeg = new DataTable();
            //每一個節目,先查詢節目段落看是否有資料
            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection1 = new SqlConnection(sqlConnStr);
            if (connection1.State == System.Data.ConnectionState.Closed)
            {
                connection1.Open();
            }
            StringBuilder sb = new StringBuilder();
            SqlDataAdapter daProgSeg = new SqlDataAdapter("SP_Q_TBPGM_PROG_SEG_MIX", connection1);
            DataTable dtProgSeg = new DataTable();
            DataTable dtProgSegDefault = new DataTable();
            SqlParameter paraPROGID = new SqlParameter("@FSPROG_ID", FSProgID);
            daProgSeg.SelectCommand.Parameters.Add(paraPROGID);
            SqlParameter paraEPISODE = new SqlParameter("@FNEPISODE", FNEpisode);
            daProgSeg.SelectCommand.Parameters.Add(paraEPISODE);
            SqlParameter paraChannelID = new SqlParameter("@FSCHANNEL_ID", FSChannel_ID);
            daProgSeg.SelectCommand.Parameters.Add(paraChannelID);
            daProgSeg.SelectCommand.CommandType = CommandType.StoredProcedure;
            daProgSeg.Fill(dtProgSeg);
            if (dtProgSeg.Rows.Count > 0)
            {
                connection1.Close();


                return dtProgSeg;
            }
            else //如果查無資料則採用預設段落 預設段落先給一段,如果他們之後有規則的話
            {

                StringBuilder sbBankDetail = new StringBuilder();
                SqlDataAdapter daBankDetail = new SqlDataAdapter("SP_Q_TBPGM_BANK_DETAIL", connection1);
                DataTable dtBankDetail = new DataTable();
                SqlParameter paraBankPROGID = new SqlParameter("@FSPROG_ID", FSProgID);
                daBankDetail.SelectCommand.Parameters.Add(paraBankPROGID);
                SqlParameter paraBankFNSEQNO = new SqlParameter("@FNSEQNO", FNSEQNO);
                daBankDetail.SelectCommand.Parameters.Add(paraBankFNSEQNO);
                SqlParameter paraBankProgName = new SqlParameter("@FSPROG_NAME", "");
                daBankDetail.SelectCommand.Parameters.Add(paraBankProgName);
                daBankDetail.SelectCommand.CommandType = CommandType.StoredProcedure;
                daBankDetail.Fill(dtBankDetail);




                string BDSeg;
                string BDDur;
                if (dtBankDetail.Rows.Count > 0)
                {
                    BDSeg = dtBankDetail.Rows[0]["FNSEG"].ToString();
                    BDDur = dtBankDetail.Rows[0]["FNDUR"].ToString();
                }
                else
                {
                    BDSeg = "1";
                    BDDur = ((int)Math.Floor(int.Parse(FNDUR) * 0.8)).ToString();
                    //BDDur = FNDUR;
                }
                if (BDSeg == "0")
                {
                    BDSeg = "1";
                    //BDDur = FNDUR;
                }
                if (FNDEF_MIN == "0" && FNDEF_SEC == "0")
                {
                    BDDur = ((int)Math.Floor(int.Parse(FNDUR) * 0.8)).ToString();
                }

                dtProgSegDefault.Columns.Add("FSID", typeof(string));
                dtProgSegDefault.Columns.Add("FNEPISODE", typeof(string));
                dtProgSegDefault.Columns.Add("FSBEG_TIMECODE", typeof(string));
                dtProgSegDefault.Columns.Add("FSEND_TIMECODE", typeof(string));
                dtProgSegDefault.Columns.Add("FCLOW_RES", typeof(string));
                dtProgSegDefault.Columns.Add("FNSEG_ID", typeof(string));
                dtProgSegDefault.Columns.Add("FNDUR_ID", typeof(string));
                dtProgSegDefault.Columns.Add("FSVIDEO_ID", typeof(string));
                dtProgSegDefault.Columns.Add("FSFILE_NO", typeof(string));


                StringBuilder sbGetVideoID = new StringBuilder();
                SqlDataAdapter daGetVideoID = new SqlDataAdapter("SP_Q_TBLOG_VIDEOID_MAP_BY_CHANNEL_ID", connection1);
                DataTable dtGetVideoID = new DataTable();
                SqlParameter paraGetVideoIDPROGID = new SqlParameter("@FSID", FSProgID);
                daGetVideoID.SelectCommand.Parameters.Add(paraGetVideoIDPROGID);
                SqlParameter paraGetVideoIDFNSEQNO = new SqlParameter("@FNEPISODE", FNEpisode);
                daGetVideoID.SelectCommand.Parameters.Add(paraGetVideoIDFNSEQNO);
                SqlParameter paraGetVideoIDProgName = new SqlParameter("@FSCHANNEL_ID", FSChannel_ID);
                daGetVideoID.SelectCommand.Parameters.Add(paraGetVideoIDProgName);
                daGetVideoID.SelectCommand.CommandType = CommandType.StoredProcedure;
                daGetVideoID.Fill(dtGetVideoID);
                //int NewSeg = 1;
                string GetVIdeoID = "";
                if (dtGetVideoID.Rows.Count > 0)
                    GetVIdeoID = dtGetVideoID.Rows[0][0].ToString();
                else
                    GetVIdeoID = "";

                //if (int.Parse(BDSeg) != 1)
                //{
                int n = 1;
                //int allDur =(int)Math.Floor(int.Parse(FNDUR) * 0.8);
                int allDur = int.Parse(FNDEF_MIN); //剩餘所有節目長度
                int nowDur;  //目前剩餘節目長度
                string nowDurString;
                //BDSeg 表示節目段數
                //nowDur = (int)(Math.Floor((double)allDur / (double)int.Parse(BDSeg)));  //此為採用平均分配

                //規則為 當剩餘長度大於剩餘段落*10則除了最後一段外都為10分鐘,不然就採平均
                if (int.Parse(FNDEF_MIN) > int.Parse(BDSeg) * 10)
                    nowDur = 10;
                else
                    nowDur = (int)(Math.Floor((double)allDur / (double)int.Parse(BDSeg)));

                while (n <= int.Parse(BDSeg))
                {
                    //n表示目前作業為哪段

                    nowDurString = MinToTimecode(nowDur);
                    if (n != int.Parse(BDSeg))
                        dtProgSegDefault.Rows.Add(FSProgID, FNEpisode, "00:00:00:00", nowDurString, "", n.ToString(), FNDUR, GetVIdeoID, "");
                    else
                        dtProgSegDefault.Rows.Add(FSProgID, FNEpisode, "00:00:00:00", MinToTimecode(allDur).Substring(0, 6) + String.Format("{0:00}", int.Parse(FNDEF_SEC)) + MinToTimecode(allDur).Substring(8, 3), "", n.ToString(), FNDUR, GetVIdeoID, "");

                    allDur = allDur - nowDur;
                    //Console.WriteLine("Current value of n is {0}", n);
                    n++;
                }
                //}
                //else
                //{
                //    dtProgSegDefault.Rows.Add(FSProgID, FNEpisode, "00:00:00:00", MinToTimecode(int.Parse(BDDur)), "", 1, FNDUR, GetVIdeoID, "");
                //}
                connection1.Close();
                return dtProgSegDefault;
            }
        }
        //將分鐘轉換為Timecode
        public string MinToTimecode(int InMin)
        {
            //string MinString;
            int HH = (int)(Math.Floor((double)InMin / (double)60));
            int MM = InMin - 60 * HH;
            return String.Format("{0:00}", HH) + ":" + String.Format("{0:00}", MM) + ":00;00";
        }

        public int CalProgLength(string BTime, string ETime)
        {
            //string MinString;
            return int.Parse(ETime.Substring(0, 2)) * 60 + int.Parse(ETime.Substring(2, 2)) - int.Parse(BTime.Substring(0, 2)) * 60 - int.Parse(BTime.Substring(2, 2));
        }

        //取得節目預定連結之主控編碼(VideoID)
        public string getMapVideoID(string FSID, string FNEPISODE, string FSARC_TYPE)
        {

            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection1 = new SqlConnection(sqlConnStr);
            if (connection1.State == System.Data.ConnectionState.Closed)
            {
                connection1.Open();
            }
            StringBuilder sb = new StringBuilder();
            SqlDataAdapter daProgSeg = new SqlDataAdapter("SP_Q_TBLOG_VIDEOID_MAP", connection1);
            DataTable dtProgSeg = new DataTable();

            SqlParameter paraPROGID = new SqlParameter("@FSID", FSID);
            daProgSeg.SelectCommand.Parameters.Add(paraPROGID);
            SqlParameter paraEPISODE = new SqlParameter("@FNEPISODE", FNEPISODE);
            daProgSeg.SelectCommand.Parameters.Add(paraEPISODE);
            SqlParameter paraChannelID = new SqlParameter("@FSARC_TYPE", FSARC_TYPE);
            daProgSeg.SelectCommand.Parameters.Add(paraChannelID);
            daProgSeg.SelectCommand.CommandType = CommandType.StoredProcedure;
            daProgSeg.Fill(dtProgSeg);
            if (dtProgSeg.Rows.Count > 0)
            {
                return dtProgSeg.Rows[0][0].ToString();
            }
            else //如果查無資料則採用預設段落 預設段落先給一段,如果他們之後有規則的話
            {
                return "";
            }
        }


        [WebMethod()]
        public string CheckDiffProgList(string _ChannelID, string _Date, string _Arc_Type)
        {
            //先查詢TbPGM_Combine_Queue,與TBPGM_QUEUE

            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);

            SqlDataAdapter daCombineQueue = new SqlDataAdapter("SP_Q_TBPGM_COMBINE_QUEUE", connection);
            DataTable dtCombineQueue = new DataTable();

            SqlDataAdapter daQueue = new SqlDataAdapter("SP_Q_TBPGM_QUEUE", connection);
            DataTable dtQueue = new DataTable();

            SqlDataAdapter daArr_Promo = new SqlDataAdapter("SP_Q_GET_TBPGM_ARR_PROMO_DUR_DIFF", connection);
            DataTable dtArr_Promo = new DataTable();

            SqlParameter paraD1 = new SqlParameter("@FDDATE", _Date);
            SqlParameter paraD2 = new SqlParameter("@FDDATE", _Date);
            SqlParameter paraD3 = new SqlParameter("@FDDATE", _Date);
            SqlParameter paraC1 = new SqlParameter("@FSCHANNEL_ID", _ChannelID);
            SqlParameter paraC2 = new SqlParameter("@FSCHANNEL_ID", _ChannelID);
            SqlParameter paraC3 = new SqlParameter("@FSCHANNEL_ID", _ChannelID);

            daCombineQueue.SelectCommand.Parameters.Add(paraD1);
            daCombineQueue.SelectCommand.Parameters.Add(paraC1);
            daQueue.SelectCommand.Parameters.Add(paraD2);
            daQueue.SelectCommand.Parameters.Add(paraC2);

            daArr_Promo.SelectCommand.Parameters.Add(paraD3);
            daArr_Promo.SelectCommand.Parameters.Add(paraC3);

            daCombineQueue.SelectCommand.CommandType = CommandType.StoredProcedure;
            daQueue.SelectCommand.CommandType = CommandType.StoredProcedure;
            daArr_Promo.SelectCommand.CommandType = CommandType.StoredProcedure;
            StringBuilder sb = new StringBuilder();
            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                //先查詢CombineQUEUE&Queue的資料
                daCombineQueue.Fill(dtCombineQueue);
                daQueue.Fill(dtQueue);
                daArr_Promo.Fill(dtArr_Promo);

                DataTable dtReBindCombine_queue = new DataTable();

                dtReBindCombine_queue.Columns.Add("FSID", typeof(string));
                dtReBindCombine_queue.Columns.Add("FNSEQNO", typeof(string));
                dtReBindCombine_queue.Columns.Add("FSPROGNAME", typeof(string));
                dtReBindCombine_queue.Columns.Add("FNEPISODE", typeof(string));
                dtReBindCombine_queue.Columns.Add("FSBEG_TIMECODE", typeof(string));
                dtReBindCombine_queue.Columns.Add("FSEND_TIMECODE", typeof(string));
                dtReBindCombine_queue.Columns.Add("FCLOW_RES", typeof(string));
                dtReBindCombine_queue.Columns.Add("FNSEG_ID", typeof(string));
                dtReBindCombine_queue.Columns.Add("FNDUR_ID", typeof(string));
                dtReBindCombine_queue.Columns.Add("FSVIDEO_ID", typeof(string));
                dtReBindCombine_queue.Columns.Add("FSFILE_NO", typeof(string));

                for (int i = 0; i <= dtQueue.Rows.Count - 1; i++)
                {
                    //取得節目段落
                    string sVIdeoID = getMapVideoID(dtQueue.Rows[i]["FSPROG_ID"].ToString(), dtQueue.Rows[i]["FNEPISODE"].ToString(), _Arc_Type);
                    int ProgLength;
                    ProgLength = CalProgLength(dtQueue.Rows[i]["FSBEG_TIME"].ToString(), dtQueue.Rows[i]["FSEND_TIME"].ToString());
                    DataTable TempTable = new DataTable();

                    TempTable = getProgSegment(dtQueue.Rows[i]["FSPROG_ID"].ToString(), dtQueue.Rows[i]["FNEPISODE"].ToString(), dtQueue.Rows[i]["FNSEQNO"].ToString(), dtQueue.Rows[i]["FSCHANNEL_ID"].ToString(), ProgLength.ToString(), dtQueue.Rows[i]["FNDEF_MIN"].ToString(), dtQueue.Rows[i]["FNDEF_SEC"].ToString());
                    for (int j = 0; j <= TempTable.Rows.Count - 1; j++)
                    {
                        string SegDur = "";
                        SegDur = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TempTable.Rows[j]["FSEND_TIMECODE"].ToString()) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TempTable.Rows[j]["FSBEG_TIMECODE"].ToString()));
                        dtReBindCombine_queue.Rows.Add(TempTable.Rows[j]["FSID"].ToString(), dtQueue.Rows[i]["FNSEQNO"].ToString(), dtQueue.Rows[i]["FSPROG_NAME"].ToString(), TempTable.Rows[j]["FNEPISODE"].ToString(), TempTable.Rows[j]["FSBEG_TIMECODE"].ToString(), TempTable.Rows[j]["FSEND_TIMECODE"].ToString(), TempTable.Rows[j]["FCLOW_RES"].ToString(), TempTable.Rows[j]["FNSEG_ID"].ToString(), SegDur, sVIdeoID, TempTable.Rows[j]["FSFILE_NO"].ToString());
                    }
                }
                int allcount = 0;
                for (int i = 0; i <= dtReBindCombine_queue.Rows.Count - 1; i++)
                {
                    allcount = dtReBindCombine_queue.Rows.Count;
                }

                //先比對CombineQueu的資料和dtReBindCombine_queue的資料相異性,
                //取得Combinequeue中有而dtReBindCombine_queue中沒有的資料,與Combinequeue中資料長度與ID和dtReBindCombine_queue中不同
                //再比對dtReBindCombine_queue和CombineQueu,取得dtReBindCombine_queue中有而Combinequeue中沒有的資料
                sb.AppendLine("<Datas>");
                if (dtCombineQueue.Rows.Count > 0)
                {
                    for (int i = 0; i <= dtCombineQueue.Rows.Count - 1; i++)
                    {
                        Boolean _Flag = false;
                        for (int j = 0; j <= dtReBindCombine_queue.Rows.Count - 1; j++)
                        {
                            if (dtCombineQueue.Rows[i]["FSPROG_ID"].ToString() == dtReBindCombine_queue.Rows[j]["FSID"].ToString()
                                && dtCombineQueue.Rows[i]["FNSEQNO"].ToString() == dtReBindCombine_queue.Rows[j]["FNSEQNO"].ToString()
                                && int.Parse(dtCombineQueue.Rows[i]["FNBREAK_NO"].ToString()) == int.Parse(dtReBindCombine_queue.Rows[j]["FNSEG_ID"].ToString()))
                            {
                                _Flag = true;// 代表目前新舊都有此筆資料,則要比對此則的VideoID和EPisode和長度,如有任何不同就寫入"不同清單"中


                                if (dtCombineQueue.Rows[i]["FSVIDEO_ID"].ToString() != dtReBindCombine_queue.Rows[j]["FSVIDEO_ID"].ToString()
                                   || dtCombineQueue.Rows[i]["FNEPISODE"].ToString() != dtReBindCombine_queue.Rows[j]["FNEPISODE"].ToString()
                                   || dtCombineQueue.Rows[i]["FSDUR"].ToString() != dtReBindCombine_queue.Rows[j]["FNDUR_ID"].ToString()
                                    || dtCombineQueue.Rows[i]["FSFILE_NO"].ToString() != dtReBindCombine_queue.Rows[j]["FSFILE_NO"].ToString())
                                {
                                    sb.AppendLine("<Data>");
                                    sb.AppendLine("<NowProgID>" + dtReBindCombine_queue.Rows[j]["FSID"].ToString() + "</NowProgID>");
                                    sb.AppendLine("<NowProgName>" + ReplaceXML(dtReBindCombine_queue.Rows[j]["FSPROGNAME"].ToString()) + "</NowProgName>");
                                    sb.AppendLine("<NowProgEpisode>" + dtReBindCombine_queue.Rows[j]["FNEPISODE"].ToString() + "</NowProgEpisode>");
                                    sb.AppendLine("<NowProgSeg>" + dtReBindCombine_queue.Rows[j]["FNSEG_ID"].ToString() + "</NowProgSeg>");
                                    sb.AppendLine("<NowProgDur>" + dtReBindCombine_queue.Rows[j]["FNDUR_ID"].ToString() + "</NowProgDur>");
                                    sb.AppendLine("<NowProgVIdeoID>" + dtReBindCombine_queue.Rows[j]["FSVIDEO_ID"].ToString() + "</NowProgVIdeoID>");
                                    sb.AppendLine("<NowProgFileNO>" + dtReBindCombine_queue.Rows[j]["FSFILE_NO"].ToString() + "</NowProgFileNO>");

                                    sb.AppendLine("<QueueProgID>" + dtCombineQueue.Rows[i]["FSPROG_ID"].ToString() + "</QueueProgID>");
                                    sb.AppendLine("<QueueProgName>" + ReplaceXML(dtCombineQueue.Rows[i]["FSPROG_NAME"].ToString()) + "</QueueProgName>");
                                    sb.AppendLine("<QueueProgEpisode>" + dtCombineQueue.Rows[i]["FNEPISODE"].ToString() + "</QueueProgEpisode>");
                                    sb.AppendLine("<QueueProgSeg>" + dtCombineQueue.Rows[i]["FNBREAK_NO"].ToString() + "</QueueProgSeg>");
                                    sb.AppendLine("<QueueProgDur>" + dtCombineQueue.Rows[i]["FSDUR"].ToString() + "</QueueProgDur>");

                                    sb.AppendLine("<QueueProgVIdeoID>" + dtCombineQueue.Rows[i]["FSVIDEO_ID"].ToString() + "</QueueProgVIdeoID>");
                                    sb.AppendLine("<QueueProgFileNO>" + dtCombineQueue.Rows[i]["FSFILE_NO"].ToString() + "</QueueProgFileNO>");

                                    string TEmpStr = "";
                                    if (dtCombineQueue.Rows[i]["FSFILE_NO"].ToString() != dtReBindCombine_queue.Rows[j]["FSFILE_NO"].ToString())
                                        TEmpStr = TEmpStr + "檔案編號不同";
                                    if (dtCombineQueue.Rows[i]["FSVIDEO_ID"].ToString() != dtReBindCombine_queue.Rows[j]["FSVIDEO_ID"].ToString())
                                        TEmpStr = TEmpStr + "主控編號不同";
                                    if (dtCombineQueue.Rows[i]["FNEPISODE"].ToString() != dtReBindCombine_queue.Rows[j]["FNEPISODE"].ToString())
                                        TEmpStr = TEmpStr + "集數不同";
                                    if (dtCombineQueue.Rows[i]["FSDUR"].ToString() != dtReBindCombine_queue.Rows[j]["FNDUR_ID"].ToString())
                                        TEmpStr = TEmpStr + "長度不同";
                                    sb.AppendLine("<FSSTATUS>" + TEmpStr + "</FSSTATUS>");
                                    sb.AppendLine("</Data>");
                                }
                            }


                        }
                        if (_Flag == false)//表示此資料在dtCombineQueue中有,但重新載入後會消失,也要寫入"不同清單"中
                        {
                            sb.AppendLine("<Data>");
                            sb.AppendLine("<NowProgID>" + "" + "</NowProgID>");
                            sb.AppendLine("<NowProgName>" + "" + "</NowProgName>");
                            sb.AppendLine("<NowProgEpisode>" + "" + "</NowProgEpisode>");
                            sb.AppendLine("<NowProgSeg>" + "" + "</NowProgSeg>");
                            sb.AppendLine("<NowProgDur>" + "" + "</NowProgDur>");
                            sb.AppendLine("<NowProgVIdeoID>" + "" + "</NowProgVIdeoID>");
                            sb.AppendLine("<NowProgFileNO>" + "" + "</NowProgFileNO>");

                            sb.AppendLine("<QueueProgID>" + dtCombineQueue.Rows[i]["FSPROG_ID"].ToString() + "</QueueProgID>");
                            sb.AppendLine("<QueueProgName>" + ReplaceXML(dtCombineQueue.Rows[i]["FSPROG_NAME"].ToString()) + "</QueueProgName>");
                            sb.AppendLine("<QueueProgEpisode>" + dtCombineQueue.Rows[i]["FNEPISODE"].ToString() + "</QueueProgEpisode>");
                            sb.AppendLine("<QueueProgSeg>" + dtCombineQueue.Rows[i]["FNBREAK_NO"].ToString() + "</QueueProgSeg>");
                            sb.AppendLine("<QueueProgDur>" + dtCombineQueue.Rows[i]["FSDUR"].ToString() + "</QueueProgDur>");

                            sb.AppendLine("<QueueProgVIdeoID>" + dtCombineQueue.Rows[i]["FSVIDEO_ID"].ToString() + "</QueueProgVIdeoID>");
                            sb.AppendLine("<QueueProgFileNO>" + dtCombineQueue.Rows[i]["FSFILE_NO"].ToString() + "</QueueProgFileNO>");

                            sb.AppendLine("<FSSTATUS>" + "重新載入後將會被移除" + "</FSSTATUS>");

                            sb.AppendLine("</Data>");
                        }


                    }



                    #region 再比對dtReBindCombine_queue和CombineQueu,取得dtReBindCombine_queue中有而Combinequeue中沒有的資料
                    for (int i = 0; i <= dtReBindCombine_queue.Rows.Count - 1; i++)
                    {
                        Boolean _Flag = false;
                        for (int j = 0; j <= dtCombineQueue.Rows.Count - 1; j++)
                        {
                            if (dtReBindCombine_queue.Rows[i]["FSID"].ToString() == dtCombineQueue.Rows[j]["FSPROG_ID"].ToString()
                                && dtReBindCombine_queue.Rows[i]["FNSEQNO"].ToString() == dtCombineQueue.Rows[j]["FNSEQNO"].ToString()
                                && int.Parse(dtReBindCombine_queue.Rows[i]["FNSEG_ID"].ToString()) == int.Parse(dtCombineQueue.Rows[j]["FNBREAK_NO"].ToString()))
                            {
                                _Flag = true;// 代表目前新舊都有此筆資料
                            }

                        }
                        if (_Flag == false)//表示此資料在dtReBindCombine_queue(重新載入)中有,但dtCombineQueue中沒有,代表節目有變動,要寫入"不同清單"中
                        {
                            sb.AppendLine("<Data>");
                            sb.AppendLine("<NowProgID>" + dtReBindCombine_queue.Rows[i]["FSID"].ToString() + "</NowProgID>");
                            sb.AppendLine("<NowProgName>" + ReplaceXML(dtReBindCombine_queue.Rows[i]["FSPROGNAME"].ToString()) + "</NowProgName>");
                            sb.AppendLine("<NowProgEpisode>" + dtReBindCombine_queue.Rows[i]["FNEPISODE"].ToString() + "</NowProgEpisode>");

                            sb.AppendLine("<NowProgSeg>" + dtReBindCombine_queue.Rows[i]["FNSEG_ID"].ToString() + "</NowProgSeg>");
                            sb.AppendLine("<NowProgDur>" + dtReBindCombine_queue.Rows[i]["FNDUR_ID"].ToString() + "</NowProgDur>");
                            sb.AppendLine("<NowProgVIdeoID>" + dtReBindCombine_queue.Rows[i]["FSVIDEO_ID"].ToString() + "</NowProgVIdeoID>");
                            sb.AppendLine("<NowProgFileNO>" + dtReBindCombine_queue.Rows[i]["FSFILE_NO"].ToString() + "</NowProgFileNO>");

                            sb.AppendLine("<QueueProgID>" + "" + "</QueueProgID>");
                            sb.AppendLine("<QueueProgName>" + "" + "</QueueProgName>");
                            sb.AppendLine("<QueueProgEpisode>" + "" + "</QueueProgEpisode>");
                            sb.AppendLine("<QueueProgSeg>" + "" + "</QueueProgSeg>");
                            sb.AppendLine("<QueueProgDur>" + "" + "</QueueProgDur>");

                            sb.AppendLine("<QueueProgVIdeoID>" + "" + "</QueueProgVIdeoID>");
                            sb.AppendLine("<QueueProgFileNO>" + "" + "</QueueProgFileNO>");

                            sb.AppendLine("<FSSTATUS>" + "新產生之節目" + "</FSSTATUS>");

                            sb.AppendLine("</Data>");
                        }
                    }
                    #endregion

                    #region 再檢查Arr_Promo的長度與TBLOG_VIDEO_SEG是否相同
                    for (int i = 0; i <= dtArr_Promo.Rows.Count - 1; i++)
                    {
                        //MAM_PTS_DLL.TimeCodeCalc.timecodetoframe
                        //select distinct A.fspromo_id,A.fsdur,D.FSBEG_TIMECODE,D.FSEND_TIMECODE,C.FSARC_TYPE,E.FSPROMO_NAME
                        //當長度不符合時將資料寫入
                        if (dtArr_Promo.Rows[i]["FSEND_TIMECODE"].ToString() != "" && dtArr_Promo.Rows[i]["FSEND_TIMECODE"].ToString() != null)
                        {
                            //                           if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtArr_Promo.Rows[i]["fsdur"].ToString()) != MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtArr_Promo.Rows[i]["FSEND_TIMECODE"].ToString()) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtArr_Promo.Rows[i]["FSBEG_TIMECODE"].ToString()))
                            //if (dtArr_Promo.Rows[i]["fsdur"].ToString() != MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtArr_Promo.Rows[i]["FSEND_TIMECODE"].ToString()) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtArr_Promo.Rows[i]["FSBEG_TIMECODE"].ToString())))

                            string NowDur = dtArr_Promo.Rows[i]["fsdur"].ToString();
                            string RealDur = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtArr_Promo.Rows[i]["FSEND_TIMECODE"].ToString()) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtArr_Promo.Rows[i]["FSBEG_TIMECODE"].ToString()));
                            if (NowDur.Substring(0, 2) != RealDur.Substring(0, 2) || NowDur.Substring(3, 2) != RealDur.Substring(3, 2) ||
                                NowDur.Substring(6, 2) != RealDur.Substring(6, 2) || NowDur.Substring(9, 2) != RealDur.Substring(9, 2)
                                || dtArr_Promo.Rows[i]["FSVIDEO_ID"].ToString() != dtArr_Promo.Rows[i]["OLD_VIDEO_ID"].ToString()
                                || dtArr_Promo.Rows[i]["FSFILE_NO"].ToString() != dtArr_Promo.Rows[i]["OLD_FILE_NO"].ToString())
                            {
                                sb.AppendLine("<Data>");
                                sb.AppendLine("<NowProgID>" + dtArr_Promo.Rows[i]["fspromo_id"].ToString() + "</NowProgID>");
                                sb.AppendLine("<NowProgName>" + ReplaceXML(dtArr_Promo.Rows[i]["FSPROMO_NAME"].ToString()) + "</NowProgName>");
                                sb.AppendLine("<NowProgEpisode>" + dtArr_Promo.Rows[i]["FSBEG_TIMECODE"].ToString() + "</NowProgEpisode>");

                                sb.AppendLine("<NowProgSeg>" + dtArr_Promo.Rows[i]["FSEND_TIMECODE"].ToString() + "</NowProgSeg>");
                                sb.AppendLine("<NowProgDur>" + dtArr_Promo.Rows[i]["fsdur"].ToString() + "</NowProgDur>");
                                sb.AppendLine("<NowProgVIdeoID>" + dtArr_Promo.Rows[i]["FSVIDEO_ID"].ToString() + "</NowProgVIdeoID>");
                                sb.AppendLine("<NowProgFileNO>" + dtArr_Promo.Rows[i]["FSFILE_NO"].ToString() + "</NowProgFileNO>");

                                sb.AppendLine("<QueueProgID>" + "PROMO" + "</QueueProgID>");
                                sb.AppendLine("<QueueProgName>" + "" + "</QueueProgName>");
                                sb.AppendLine("<QueueProgEpisode>" + "" + "</QueueProgEpisode>");
                                sb.AppendLine("<QueueProgSeg>" + "" + "</QueueProgSeg>");
                                sb.AppendLine("<QueueProgDur>" + "" + "</QueueProgDur>");

                                sb.AppendLine("<QueueProgVIdeoID>" + dtArr_Promo.Rows[i]["OLD_VIDEO_ID"].ToString() + "</QueueProgVIdeoID>");
                                sb.AppendLine("<QueueProgFileNO>" + dtArr_Promo.Rows[i]["OLD_FILE_NO"].ToString() + "</QueueProgFileNO>");
                                string TEmpStr = "";
                                if (NowDur.Substring(0, 2) != RealDur.Substring(0, 2) || NowDur.Substring(3, 2) != RealDur.Substring(3, 2) ||
                                    NowDur.Substring(6, 2) != RealDur.Substring(6, 2) || NowDur.Substring(9, 2) != RealDur.Substring(9, 2))
                                    TEmpStr = TEmpStr + "Promo長度不同";
                                if (dtArr_Promo.Rows[i]["FSVIDEO_ID"].ToString() != dtArr_Promo.Rows[i]["OLD_VIDEO_ID"].ToString())
                                    TEmpStr = TEmpStr + " 主控編號不同";
                                if (dtArr_Promo.Rows[i]["FSFILE_NO"].ToString() != dtArr_Promo.Rows[i]["OLD_FILE_NO"].ToString())
                                    TEmpStr = TEmpStr + " 檔案編號不同";

                                sb.AppendLine("<FSSTATUS>" + TEmpStr + "</FSSTATUS>");

                                sb.AppendLine("</Data>");
                            }
                        }
                        else
                        {
                            sb.AppendLine("<Data>");
                            sb.AppendLine("<NowProgID>" + dtArr_Promo.Rows[i]["fspromo_id"].ToString() + "</NowProgID>");
                            sb.AppendLine("<NowProgName>" + ReplaceXML(dtArr_Promo.Rows[i]["FSPROMO_NAME"].ToString()) + "</NowProgName>");
                            sb.AppendLine("<NowProgEpisode>" + dtArr_Promo.Rows[i]["FSBEG_TIMECODE"].ToString() + "</NowProgEpisode>");

                            sb.AppendLine("<NowProgSeg>" + dtArr_Promo.Rows[i]["FSEND_TIMECODE"].ToString() + "</NowProgSeg>");
                            sb.AppendLine("<NowProgDur>" + dtArr_Promo.Rows[i]["fsdur"].ToString() + "</NowProgDur>");
                            sb.AppendLine("<NowProgVIdeoID>" + dtArr_Promo.Rows[i]["FSVIDEO_ID"].ToString() + "</NowProgVIdeoID>");
                            sb.AppendLine("<NowProgFileNO>" + dtArr_Promo.Rows[i]["FSFILE_NO"].ToString() + "</NowProgFileNO>");

                            sb.AppendLine("<QueueProgID>" + "PROMO" + "</QueueProgID>");
                            sb.AppendLine("<QueueProgName>" + "" + "</QueueProgName>");
                            sb.AppendLine("<QueueProgEpisode>" + "" + "</QueueProgEpisode>");
                            sb.AppendLine("<QueueProgSeg>" + "" + "</QueueProgSeg>");
                            sb.AppendLine("<QueueProgDur>" + "" + "</QueueProgDur>");

                            sb.AppendLine("<QueueProgVIdeoID>" + dtArr_Promo.Rows[i]["OLD_VIDEO_ID"].ToString() + "</QueueProgVIdeoID>");
                            sb.AppendLine("<QueueProgFileNO>" + dtArr_Promo.Rows[i]["OLD_FILE_NO"].ToString() + "</QueueProgFileNO>");

                            sb.AppendLine("<FSSTATUS>" + "還未做送帶轉檔" + "</FSSTATUS>");

                            sb.AppendLine("</Data>");
                        }
                    }
                    #endregion
                }
                else //(dtCombineQueue.Rows.Count < 0) 沒有節目表資料
                {
                    return "<Errors>" + "目前沒有節目段落" + "</Errors>";
                }
                sb.AppendLine("</Datas>");
                return sb.ToString();
            }
            catch (Exception ex)
            {
                StringBuilder sbError = new StringBuilder();
                sbError.AppendLine("<Errors>" + ex.Message + "</Errors>");
                return sbError.ToString();
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }

            //檢查節目表中每個節目跟Promo的段落資料是否和資料庫中已經轉檔完成的段落相同
            //不檢查Live節目
            string ReturnString = "";
            //foreach (ArrPromoList TemArrPromoList in InList)
            //{
            //    if (TemArrPromoList.FNLIVE != "1") //表示非為Live
            //    {
            //        if (TemArrPromoList.FCTYPE == "G") //節目
            //        {

            //            //                        A.FSID,A.FNEPISODE,B.FSBEG_TIMECODE,B.FSEND_TIMECODE,
            //            //B.FCLOW_RES,B.FNSEG_ID,B.FSVIDEO_ID,A.FSFILE_NO
            //            //from TBLOG_VIDEO A,TBLOG_VIDEO_SEG B,TBZCHANNEL C
            //            //where
            //            //A.FSFILE_NO=B.FSFILE_NO and A.FSARC_TYPE=C.FSCHANNEL_TYPE
            //            //and A.FSID=@FSPROG_ID and A.FNEPISODE=@FNEPISODE and (A.FCFILE_STATUS='T' or A.FCFILE_STATUS='Y')
            //            //and C.FSCHANNEL_ID=@FSCHANNEL_ID order by B.FNSEG_ID

            //            String sqlConnStr = "";
            //            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            //            SqlConnection connection = new SqlConnection(sqlConnStr);
            //            StringBuilder sb = new StringBuilder();

            //            SqlDataAdapter da = new SqlDataAdapter("SP_Q_TBPGM_PROG_REAL_SEG", connection);
            //            DataTable dt = new DataTable();


            //            SqlParameter para = new SqlParameter("@FSPROG_ID", TemArrPromoList.FSPROG_ID);
            //            SqlParameter para1 = new SqlParameter("@FNEPISODE", TemArrPromoList.FNEPISODE);
            //            SqlParameter para2 = new SqlParameter("@FSCHANNEL_ID", TemArrPromoList.FSCHANNEL_ID);
            //            SqlParameter para3 = new SqlParameter("@FNSEG_ID", String.Format("{0:00}", TemArrPromoList.FNBREAK_NO));

            //            da.SelectCommand.Parameters.Add(para);
            //            da.SelectCommand.Parameters.Add(para1);
            //            da.SelectCommand.Parameters.Add(para2);
            //            da.SelectCommand.Parameters.Add(para3);
            //            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            //            try
            //            {
            //                if (connection.State == System.Data.ConnectionState.Closed)
            //                {
            //                    connection.Open();
            //                }

            //                da.Fill(dt);

            //                if (dt.Rows.Count > 0)
            //                {
            //                    //當長度不同
            //                    if ((MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dt.Rows[0]["FSEND_TIMECODE"].ToString()) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dt.Rows[0]["FSBEG_TIMECODE"].ToString())) != MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION)
            //                        || dt.Rows[0]["FSVIDEO_ID"].ToString() != TemArrPromoList.FSVIDEO_ID
            //                        || dt.Rows[0]["FSFILE_NO"].ToString() != TemArrPromoList.FSFILE_NO)
            //                    {
            //                        ReturnString = ReturnString + "節目:" + TemArrPromoList.FSPROG_NAME + " 集數:" + TemArrPromoList.FNEPISODE + " :第" + TemArrPromoList.FNBREAK_NO + "段與實際轉檔資料段落長度不符" + Convert.ToChar(10) + Convert.ToChar(13);
            //                    }

            //                }
            //                else
            //                {
            //                    ReturnString = ReturnString + "節目:" + TemArrPromoList.FSPROG_NAME + " 集數:" + TemArrPromoList.FNEPISODE + " :第" + TemArrPromoList.FNBREAK_NO + "段與實際轉檔資料段落長度不符" + Convert.ToChar(10) + Convert.ToChar(13);

            //                }
            //                connection.Close();
            //                connection.Dispose();
            //                GC.Collect();

            //            }
            //            catch (Exception ex)
            //            {

            //            }
            //            finally
            //            {
            //                connection.Close();
            //                connection.Dispose();
            //                GC.Collect();
            //            }
            //        }
            //        else //Promo類型
            //        {
            //            String sqlConnStr = "";
            //            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            //            SqlConnection connection = new SqlConnection(sqlConnStr);
            //            StringBuilder sb = new StringBuilder();

            //            SqlDataAdapter da = new SqlDataAdapter("SP_Q_TBPGM_PROG_REAL_SEG", connection);
            //            DataTable dt = new DataTable();


            //            SqlParameter para = new SqlParameter("@FSPROG_ID", TemArrPromoList.FSPROG_ID);
            //            SqlParameter para1 = new SqlParameter("@FNEPISODE", "0");
            //            SqlParameter para2 = new SqlParameter("@FSCHANNEL_ID", TemArrPromoList.FSCHANNEL_ID);
            //            SqlParameter para3 = new SqlParameter("@FNSEG_ID", "1");

            //            da.SelectCommand.Parameters.Add(para);
            //            da.SelectCommand.Parameters.Add(para1);
            //            da.SelectCommand.Parameters.Add(para2);
            //            da.SelectCommand.Parameters.Add(para3);
            //            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            //            try
            //            {
            //                if (connection.State == System.Data.ConnectionState.Closed)
            //                {
            //                    connection.Open();
            //                }

            //                da.Fill(dt);

            //                if (dt.Rows.Count > 0)
            //                {
            //                    //當長度不同
            //                    if ((MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dt.Rows[0]["FSEND_TIMECODE"].ToString()) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dt.Rows[0]["FSBEG_TIMECODE"].ToString())) != MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION)
            //                        || dt.Rows[0]["FSVIDEO_ID"].ToString() != TemArrPromoList.FSVIDEO_ID
            //                        || dt.Rows[0]["FSFILE_NO"].ToString() != TemArrPromoList.FSFILE_NO)
            //                    {
            //                        ReturnString = ReturnString + "Promo:" + TemArrPromoList.FSPROG_NAME + "與實際轉檔資料不符" + Convert.ToChar(10) + Convert.ToChar(13);
            //                    }

            //                }
            //                else
            //                {
            //                    ReturnString = ReturnString + "Promo:" + TemArrPromoList.FSPROG_NAME + "與實際轉檔資料不符" + Convert.ToChar(10) + Convert.ToChar(13);
            //                }
            //                connection.Close();
            //                connection.Dispose();
            //                GC.Collect();

            //            }
            //            catch (Exception ex)
            //            {

            //            }
            //            finally
            //            {
            //                connection.Close();
            //                connection.Dispose();
            //                GC.Collect();
            //            }
            //        }
            //    }

            //    //SP_Q_TBPGM_PROG_REAL_SEG

            //}
            if (ReturnString == "")
                return "ok";
            else
                return ReturnString;
        }

        [WebMethod()]
        public string CheckDiffProgList_MIX(string _ChannelID, string _Date, string _Arc_Type)
        {
            //先查詢TbPGM_Combine_Queue,與TBPGM_QUEUE

            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);

            SqlDataAdapter daCombineQueue = new SqlDataAdapter("SP_Q_TBPGM_COMBINE_QUEUE", connection);
            DataTable dtCombineQueue = new DataTable();

            SqlDataAdapter daQueue = new SqlDataAdapter("SP_Q_TBPGM_QUEUE", connection);
            DataTable dtQueue = new DataTable();

            SqlDataAdapter daArr_Promo = new SqlDataAdapter("SP_Q_GET_TBPGM_ARR_PROMO_DUR_DIFF_MIX", connection);
            DataTable dtArr_Promo = new DataTable();

            SqlParameter paraD1 = new SqlParameter("@FDDATE", _Date);
            SqlParameter paraD2 = new SqlParameter("@FDDATE", _Date);
            SqlParameter paraD3 = new SqlParameter("@FDDATE", _Date);
            SqlParameter paraC1 = new SqlParameter("@FSCHANNEL_ID", _ChannelID);
            SqlParameter paraC2 = new SqlParameter("@FSCHANNEL_ID", _ChannelID);
            SqlParameter paraC3 = new SqlParameter("@FSCHANNEL_ID", _ChannelID);

            daCombineQueue.SelectCommand.Parameters.Add(paraD1);
            daCombineQueue.SelectCommand.Parameters.Add(paraC1);
            daQueue.SelectCommand.Parameters.Add(paraD2);
            daQueue.SelectCommand.Parameters.Add(paraC2);

            daArr_Promo.SelectCommand.Parameters.Add(paraD3);
            daArr_Promo.SelectCommand.Parameters.Add(paraC3);

            daCombineQueue.SelectCommand.CommandType = CommandType.StoredProcedure;
            daQueue.SelectCommand.CommandType = CommandType.StoredProcedure;
            daArr_Promo.SelectCommand.CommandType = CommandType.StoredProcedure;
            StringBuilder sb = new StringBuilder();
            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                //先查詢CombineQUEUE&Queue的資料
                daCombineQueue.Fill(dtCombineQueue);
                daQueue.Fill(dtQueue);
                daArr_Promo.Fill(dtArr_Promo);

                DataTable dtReBindCombine_queue = new DataTable();

                dtReBindCombine_queue.Columns.Add("FSID", typeof(string));
                dtReBindCombine_queue.Columns.Add("FNSEQNO", typeof(string));
                dtReBindCombine_queue.Columns.Add("FSPROGNAME", typeof(string));
                dtReBindCombine_queue.Columns.Add("FNEPISODE", typeof(string));
                dtReBindCombine_queue.Columns.Add("FSBEG_TIMECODE", typeof(string));
                dtReBindCombine_queue.Columns.Add("FSEND_TIMECODE", typeof(string));
                dtReBindCombine_queue.Columns.Add("FCLOW_RES", typeof(string));
                dtReBindCombine_queue.Columns.Add("FNSEG_ID", typeof(string));
                dtReBindCombine_queue.Columns.Add("FNDUR_ID", typeof(string));
                dtReBindCombine_queue.Columns.Add("FSVIDEO_ID", typeof(string));
                dtReBindCombine_queue.Columns.Add("FSFILE_NO", typeof(string));

                for (int i = 0; i <= dtQueue.Rows.Count - 1; i++)
                {
                    //取得節目段落
                    //string sVIdeoID = getMapVideoID(dtQueue.Rows[i]["FSPROG_ID"].ToString(), dtQueue.Rows[i]["FNEPISODE"].ToString(), _Arc_Type);
                    int ProgLength;
                    ProgLength = CalProgLength(dtQueue.Rows[i]["FSBEG_TIME"].ToString(), dtQueue.Rows[i]["FSEND_TIME"].ToString());
                    DataTable TempTable = new DataTable();

                    TempTable = getProgSegment_MIX(dtQueue.Rows[i]["FSPROG_ID"].ToString(), dtQueue.Rows[i]["FNEPISODE"].ToString(), dtQueue.Rows[i]["FNSEQNO"].ToString(), dtQueue.Rows[i]["FSCHANNEL_ID"].ToString(), ProgLength.ToString(), dtQueue.Rows[i]["FNDEF_MIN"].ToString(), dtQueue.Rows[i]["FNDEF_SEC"].ToString());
                    for (int j = 0; j <= TempTable.Rows.Count - 1; j++)
                    {
                        string SegDur = "";
                        SegDur = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TempTable.Rows[j]["FSEND_TIMECODE"].ToString()) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TempTable.Rows[j]["FSBEG_TIMECODE"].ToString()));
                        dtReBindCombine_queue.Rows.Add(TempTable.Rows[j]["FSID"].ToString(), dtQueue.Rows[i]["FNSEQNO"].ToString(), dtQueue.Rows[i]["FSPROG_NAME"].ToString(), TempTable.Rows[j]["FNEPISODE"].ToString(), TempTable.Rows[j]["FSBEG_TIMECODE"].ToString(), TempTable.Rows[j]["FSEND_TIMECODE"].ToString(), TempTable.Rows[j]["FCLOW_RES"].ToString(), TempTable.Rows[j]["FNSEG_ID"].ToString(), SegDur, TempTable.Rows[j]["FSVIDEO_ID"].ToString(), TempTable.Rows[j]["FSFILE_NO"].ToString());
                    }
                }
                int allcount = 0;
                for (int i = 0; i <= dtReBindCombine_queue.Rows.Count - 1; i++)
                {
                    allcount = dtReBindCombine_queue.Rows.Count;
                }

                //先比對CombineQueu的資料和dtReBindCombine_queue的資料相異性,
                //取得Combinequeue中有而dtReBindCombine_queue中沒有的資料,與Combinequeue中資料長度與ID和dtReBindCombine_queue中不同
                //再比對dtReBindCombine_queue和CombineQueu,取得dtReBindCombine_queue中有而Combinequeue中沒有的資料
                sb.AppendLine("<Datas>");
                if (dtCombineQueue.Rows.Count > 0)
                {
                    for (int i = 0; i <= dtCombineQueue.Rows.Count - 1; i++)
                    {
                        Boolean _Flag = false;
                        for (int j = 0; j <= dtReBindCombine_queue.Rows.Count - 1; j++)
                        {
                            if (dtCombineQueue.Rows[i]["FSPROG_ID"].ToString() == dtReBindCombine_queue.Rows[j]["FSID"].ToString()
                                && dtCombineQueue.Rows[i]["FNSEQNO"].ToString() == dtReBindCombine_queue.Rows[j]["FNSEQNO"].ToString()
                                && int.Parse(dtCombineQueue.Rows[i]["FNBREAK_NO"].ToString()) == int.Parse(dtReBindCombine_queue.Rows[j]["FNSEG_ID"].ToString()))
                            {
                                _Flag = true;// 代表目前新舊都有此筆資料,則要比對此則的VideoID和EPisode和長度,如有任何不同就寫入"不同清單"中


                                if (dtCombineQueue.Rows[i]["FSVIDEO_ID"].ToString() != dtReBindCombine_queue.Rows[j]["FSVIDEO_ID"].ToString()
                                   || dtCombineQueue.Rows[i]["FNEPISODE"].ToString() != dtReBindCombine_queue.Rows[j]["FNEPISODE"].ToString()
                                   || dtCombineQueue.Rows[i]["FSDUR"].ToString() != dtReBindCombine_queue.Rows[j]["FNDUR_ID"].ToString()
                                    || dtCombineQueue.Rows[i]["FSFILE_NO"].ToString() != dtReBindCombine_queue.Rows[j]["FSFILE_NO"].ToString())
                                {
                                    sb.AppendLine("<Data>");
                                    sb.AppendLine("<NowProgID>" + dtReBindCombine_queue.Rows[j]["FSID"].ToString() + "</NowProgID>");
                                    sb.AppendLine("<NowProgName>" + ReplaceXML(dtReBindCombine_queue.Rows[j]["FSPROGNAME"].ToString()) + "</NowProgName>");
                                    sb.AppendLine("<NowProgEpisode>" + dtReBindCombine_queue.Rows[j]["FNEPISODE"].ToString() + "</NowProgEpisode>");
                                    sb.AppendLine("<NowProgSeg>" + dtReBindCombine_queue.Rows[j]["FNSEG_ID"].ToString() + "</NowProgSeg>");
                                    sb.AppendLine("<NowProgDur>" + dtReBindCombine_queue.Rows[j]["FNDUR_ID"].ToString() + "</NowProgDur>");
                                    sb.AppendLine("<NowProgVIdeoID>" + dtReBindCombine_queue.Rows[j]["FSVIDEO_ID"].ToString() + "</NowProgVIdeoID>");
                                    sb.AppendLine("<NowProgFileNO>" + dtReBindCombine_queue.Rows[j]["FSFILE_NO"].ToString() + "</NowProgFileNO>");

                                    sb.AppendLine("<QueueProgID>" + dtCombineQueue.Rows[i]["FSPROG_ID"].ToString() + "</QueueProgID>");
                                    sb.AppendLine("<QueueProgName>" + ReplaceXML(dtCombineQueue.Rows[i]["FSPROG_NAME"].ToString()) + "</QueueProgName>");
                                    sb.AppendLine("<QueueProgEpisode>" + dtCombineQueue.Rows[i]["FNEPISODE"].ToString() + "</QueueProgEpisode>");
                                    sb.AppendLine("<QueueProgSeg>" + dtCombineQueue.Rows[i]["FNBREAK_NO"].ToString() + "</QueueProgSeg>");
                                    sb.AppendLine("<QueueProgDur>" + dtCombineQueue.Rows[i]["FSDUR"].ToString() + "</QueueProgDur>");

                                    sb.AppendLine("<QueueProgVIdeoID>" + dtCombineQueue.Rows[i]["FSVIDEO_ID"].ToString() + "</QueueProgVIdeoID>");
                                    sb.AppendLine("<QueueProgFileNO>" + dtCombineQueue.Rows[i]["FSFILE_NO"].ToString() + "</QueueProgFileNO>");

                                    string TEmpStr = "";
                                    if (dtCombineQueue.Rows[i]["FSFILE_NO"].ToString() != dtReBindCombine_queue.Rows[j]["FSFILE_NO"].ToString())
                                        TEmpStr = TEmpStr + "檔案編號不同";
                                    if (dtCombineQueue.Rows[i]["FSVIDEO_ID"].ToString() != dtReBindCombine_queue.Rows[j]["FSVIDEO_ID"].ToString())
                                        TEmpStr = TEmpStr + "主控編號不同";
                                    if (dtCombineQueue.Rows[i]["FNEPISODE"].ToString() != dtReBindCombine_queue.Rows[j]["FNEPISODE"].ToString())
                                        TEmpStr = TEmpStr + "集數不同";
                                    if (dtCombineQueue.Rows[i]["FSDUR"].ToString() != dtReBindCombine_queue.Rows[j]["FNDUR_ID"].ToString())
                                        TEmpStr = TEmpStr + "長度不同";
                                    sb.AppendLine("<FSSTATUS>" + TEmpStr + "</FSSTATUS>");
                                    sb.AppendLine("</Data>");
                                }
                            }


                        }
                        if (_Flag == false)//表示此資料在dtCombineQueue中有,但重新載入後會消失,也要寫入"不同清單"中
                        {
                            sb.AppendLine("<Data>");
                            sb.AppendLine("<NowProgID>" + "" + "</NowProgID>");
                            sb.AppendLine("<NowProgName>" + "" + "</NowProgName>");
                            sb.AppendLine("<NowProgEpisode>" + "" + "</NowProgEpisode>");
                            sb.AppendLine("<NowProgSeg>" + "" + "</NowProgSeg>");
                            sb.AppendLine("<NowProgDur>" + "" + "</NowProgDur>");
                            sb.AppendLine("<NowProgVIdeoID>" + "" + "</NowProgVIdeoID>");
                            sb.AppendLine("<NowProgFileNO>" + "" + "</NowProgFileNO>");

                            sb.AppendLine("<QueueProgID>" + dtCombineQueue.Rows[i]["FSPROG_ID"].ToString() + "</QueueProgID>");
                            sb.AppendLine("<QueueProgName>" + ReplaceXML(dtCombineQueue.Rows[i]["FSPROG_NAME"].ToString()) + "</QueueProgName>");
                            sb.AppendLine("<QueueProgEpisode>" + dtCombineQueue.Rows[i]["FNEPISODE"].ToString() + "</QueueProgEpisode>");
                            sb.AppendLine("<QueueProgSeg>" + dtCombineQueue.Rows[i]["FNBREAK_NO"].ToString() + "</QueueProgSeg>");
                            sb.AppendLine("<QueueProgDur>" + dtCombineQueue.Rows[i]["FSDUR"].ToString() + "</QueueProgDur>");

                            sb.AppendLine("<QueueProgVIdeoID>" + dtCombineQueue.Rows[i]["FSVIDEO_ID"].ToString() + "</QueueProgVIdeoID>");
                            sb.AppendLine("<QueueProgFileNO>" + dtCombineQueue.Rows[i]["FSFILE_NO"].ToString() + "</QueueProgFileNO>");

                            sb.AppendLine("<FSSTATUS>" + "重新載入後將會被移除" + "</FSSTATUS>");

                            sb.AppendLine("</Data>");
                        }


                    }



                    #region 再比對dtReBindCombine_queue和CombineQueu,取得dtReBindCombine_queue中有而Combinequeue中沒有的資料
                    for (int i = 0; i <= dtReBindCombine_queue.Rows.Count - 1; i++)
                    {
                        Boolean _Flag = false;
                        for (int j = 0; j <= dtCombineQueue.Rows.Count - 1; j++)
                        {
                            if (dtReBindCombine_queue.Rows[i]["FSID"].ToString() == dtCombineQueue.Rows[j]["FSPROG_ID"].ToString()
                                && dtReBindCombine_queue.Rows[i]["FNSEQNO"].ToString() == dtCombineQueue.Rows[j]["FNSEQNO"].ToString()
                                && int.Parse(dtReBindCombine_queue.Rows[i]["FNSEG_ID"].ToString()) == int.Parse(dtCombineQueue.Rows[j]["FNBREAK_NO"].ToString()))
                            {
                                _Flag = true;// 代表目前新舊都有此筆資料
                            }

                        }
                        if (_Flag == false)//表示此資料在dtReBindCombine_queue(重新載入)中有,但dtCombineQueue中沒有,代表節目有變動,要寫入"不同清單"中
                        {
                            sb.AppendLine("<Data>");
                            sb.AppendLine("<NowProgID>" + dtReBindCombine_queue.Rows[i]["FSID"].ToString() + "</NowProgID>");
                            sb.AppendLine("<NowProgName>" + ReplaceXML(dtReBindCombine_queue.Rows[i]["FSPROGNAME"].ToString()) + "</NowProgName>");
                            sb.AppendLine("<NowProgEpisode>" + dtReBindCombine_queue.Rows[i]["FNEPISODE"].ToString() + "</NowProgEpisode>");

                            sb.AppendLine("<NowProgSeg>" + dtReBindCombine_queue.Rows[i]["FNSEG_ID"].ToString() + "</NowProgSeg>");
                            sb.AppendLine("<NowProgDur>" + dtReBindCombine_queue.Rows[i]["FNDUR_ID"].ToString() + "</NowProgDur>");
                            sb.AppendLine("<NowProgVIdeoID>" + dtReBindCombine_queue.Rows[i]["FSVIDEO_ID"].ToString() + "</NowProgVIdeoID>");
                            sb.AppendLine("<NowProgFileNO>" + dtReBindCombine_queue.Rows[i]["FSFILE_NO"].ToString() + "</NowProgFileNO>");

                            sb.AppendLine("<QueueProgID>" + "" + "</QueueProgID>");
                            sb.AppendLine("<QueueProgName>" + "" + "</QueueProgName>");
                            sb.AppendLine("<QueueProgEpisode>" + "" + "</QueueProgEpisode>");
                            sb.AppendLine("<QueueProgSeg>" + "" + "</QueueProgSeg>");
                            sb.AppendLine("<QueueProgDur>" + "" + "</QueueProgDur>");

                            sb.AppendLine("<QueueProgVIdeoID>" + "" + "</QueueProgVIdeoID>");
                            sb.AppendLine("<QueueProgFileNO>" + "" + "</QueueProgFileNO>");

                            sb.AppendLine("<FSSTATUS>" + "新產生之節目" + "</FSSTATUS>");

                            sb.AppendLine("</Data>");
                        }
                    }
                    #endregion

                    #region 先將Arr_Promo的資料去除重複HD SD取其一
                    for (int i = dtArr_Promo.Rows.Count - 1; i >= 0; i--)
                    {
                        if (i >= 1)
                        {
                            if (dtArr_Promo.Rows[i]["FSPROMO_ID"].ToString() == dtArr_Promo.Rows[i-1]["FSPROMO_ID"].ToString())
                            {
                                if (dtArr_Promo.Rows[i]["FSARC_TYPE"].ToString() == "001")
                                {
                                    if (dtArr_Promo.Rows[i - 1]["FSFILE_NO"].ToString() != "")
                                    { //002 的檔案有編號則採用002,刪除001
                                        dtArr_Promo.Rows.RemoveAt(i);
                                    }
                                    else if (dtArr_Promo.Rows[i]["FSFILE_NO"].ToString() != "")
                                    { //002為空但001不為空則刪除002
                                        dtArr_Promo.Rows.RemoveAt(i - 1);
                                    }
                                    else
                                    {  //001 002 皆為空
                                        dtArr_Promo.Rows.RemoveAt(i);
                                    }
                                }
                                else
                                {

                                    if (dtArr_Promo.Rows[i]["FSFILE_NO"].ToString() != "")
                                    { //002 的檔案有編號則採用002,刪除001
                                        dtArr_Promo.Rows.RemoveAt(i-1);
                                    }
                                    else if (dtArr_Promo.Rows[i-1]["FSFILE_NO"].ToString() != "")
                                    { //002為空但001不為空則刪除002
                                        dtArr_Promo.Rows.RemoveAt(i);
                                    }
                                    else
                                    {  //001 002 皆為空
                                        dtArr_Promo.Rows.RemoveAt(i-1);
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                    #region 再檢查Arr_Promo的長度與TBLOG_VIDEO_SEG是否相同
                    for (int i = 0; i <= dtArr_Promo.Rows.Count - 1; i++)
                    {
                        //MAM_PTS_DLL.TimeCodeCalc.timecodetoframe
                        //select distinct A.fspromo_id,A.fsdur,D.FSBEG_TIMECODE,D.FSEND_TIMECODE,C.FSARC_TYPE,E.FSPROMO_NAME
                        //當長度不符合時將資料寫入
                        if (dtArr_Promo.Rows[i]["FSEND_TIMECODE"].ToString() != "" && dtArr_Promo.Rows[i]["FSEND_TIMECODE"].ToString() != null)
                        {
                            //                           if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtArr_Promo.Rows[i]["fsdur"].ToString()) != MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtArr_Promo.Rows[i]["FSEND_TIMECODE"].ToString()) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtArr_Promo.Rows[i]["FSBEG_TIMECODE"].ToString()))
                            //if (dtArr_Promo.Rows[i]["fsdur"].ToString() != MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtArr_Promo.Rows[i]["FSEND_TIMECODE"].ToString()) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtArr_Promo.Rows[i]["FSBEG_TIMECODE"].ToString())))

                            string NowDur = dtArr_Promo.Rows[i]["fsdur"].ToString();
                            string RealDur = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtArr_Promo.Rows[i]["FSEND_TIMECODE"].ToString()) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtArr_Promo.Rows[i]["FSBEG_TIMECODE"].ToString()));
                            if (NowDur.Substring(0, 2) != RealDur.Substring(0, 2) || NowDur.Substring(3, 2) != RealDur.Substring(3, 2) ||
                                NowDur.Substring(6, 2) != RealDur.Substring(6, 2) || NowDur.Substring(9, 2) != RealDur.Substring(9, 2)
                                || dtArr_Promo.Rows[i]["FSVIDEO_ID"].ToString() != dtArr_Promo.Rows[i]["OLD_VIDEO_ID"].ToString()
                                || dtArr_Promo.Rows[i]["FSFILE_NO"].ToString() != dtArr_Promo.Rows[i]["OLD_FILE_NO"].ToString())
                            {
                                sb.AppendLine("<Data>");
                                sb.AppendLine("<NowProgID>" + dtArr_Promo.Rows[i]["fspromo_id"].ToString() + "</NowProgID>");
                                sb.AppendLine("<NowProgName>" + ReplaceXML(dtArr_Promo.Rows[i]["FSPROMO_NAME"].ToString()) + "</NowProgName>");
                                sb.AppendLine("<NowProgEpisode>" + dtArr_Promo.Rows[i]["FSBEG_TIMECODE"].ToString() + "</NowProgEpisode>");

                                sb.AppendLine("<NowProgSeg>" + dtArr_Promo.Rows[i]["FSEND_TIMECODE"].ToString() + "</NowProgSeg>");
                                sb.AppendLine("<NowProgDur>" + dtArr_Promo.Rows[i]["fsdur"].ToString() + "</NowProgDur>");
                                sb.AppendLine("<NowProgVIdeoID>" + dtArr_Promo.Rows[i]["FSVIDEO_ID"].ToString() + "</NowProgVIdeoID>");
                                sb.AppendLine("<NowProgFileNO>" + dtArr_Promo.Rows[i]["FSFILE_NO"].ToString() + "</NowProgFileNO>");

                                sb.AppendLine("<QueueProgID>" + "PROMO" + "</QueueProgID>");
                                sb.AppendLine("<QueueProgName>" + "" + "</QueueProgName>");
                                sb.AppendLine("<QueueProgEpisode>" + "" + "</QueueProgEpisode>");
                                sb.AppendLine("<QueueProgSeg>" + "" + "</QueueProgSeg>");
                                sb.AppendLine("<QueueProgDur>" + "" + "</QueueProgDur>");

                                sb.AppendLine("<QueueProgVIdeoID>" + dtArr_Promo.Rows[i]["OLD_VIDEO_ID"].ToString() + "</QueueProgVIdeoID>");
                                sb.AppendLine("<QueueProgFileNO>" + dtArr_Promo.Rows[i]["OLD_FILE_NO"].ToString() + "</QueueProgFileNO>");
                                string TEmpStr = "";
                                if (NowDur.Substring(0, 2) != RealDur.Substring(0, 2) || NowDur.Substring(3, 2) != RealDur.Substring(3, 2) ||
                                    NowDur.Substring(6, 2) != RealDur.Substring(6, 2) || NowDur.Substring(9, 2) != RealDur.Substring(9, 2))
                                    TEmpStr = TEmpStr + "Promo長度不同";
                                if (dtArr_Promo.Rows[i]["FSVIDEO_ID"].ToString() != dtArr_Promo.Rows[i]["OLD_VIDEO_ID"].ToString())
                                    TEmpStr = TEmpStr + " 主控編號不同";
                                if (dtArr_Promo.Rows[i]["FSFILE_NO"].ToString() != dtArr_Promo.Rows[i]["OLD_FILE_NO"].ToString())
                                    TEmpStr = TEmpStr + " 檔案編號不同";

                                sb.AppendLine("<FSSTATUS>" + TEmpStr + "</FSSTATUS>");

                                sb.AppendLine("</Data>");
                            }
                        }
                        else
                        {
                            sb.AppendLine("<Data>");
                            sb.AppendLine("<NowProgID>" + dtArr_Promo.Rows[i]["fspromo_id"].ToString() + "</NowProgID>");
                            sb.AppendLine("<NowProgName>" + ReplaceXML(dtArr_Promo.Rows[i]["FSPROMO_NAME"].ToString()) + "</NowProgName>");
                            sb.AppendLine("<NowProgEpisode>" + dtArr_Promo.Rows[i]["FSBEG_TIMECODE"].ToString() + "</NowProgEpisode>");

                            sb.AppendLine("<NowProgSeg>" + dtArr_Promo.Rows[i]["FSEND_TIMECODE"].ToString() + "</NowProgSeg>");
                            sb.AppendLine("<NowProgDur>" + dtArr_Promo.Rows[i]["fsdur"].ToString() + "</NowProgDur>");
                            sb.AppendLine("<NowProgVIdeoID>" + dtArr_Promo.Rows[i]["FSVIDEO_ID"].ToString() + "</NowProgVIdeoID>");
                            sb.AppendLine("<NowProgFileNO>" + dtArr_Promo.Rows[i]["FSFILE_NO"].ToString() + "</NowProgFileNO>");

                            sb.AppendLine("<QueueProgID>" + "PROMO" + "</QueueProgID>");
                            sb.AppendLine("<QueueProgName>" + "" + "</QueueProgName>");
                            sb.AppendLine("<QueueProgEpisode>" + "" + "</QueueProgEpisode>");
                            sb.AppendLine("<QueueProgSeg>" + "" + "</QueueProgSeg>");
                            sb.AppendLine("<QueueProgDur>" + "" + "</QueueProgDur>");

                            sb.AppendLine("<QueueProgVIdeoID>" + dtArr_Promo.Rows[i]["OLD_VIDEO_ID"].ToString() + "</QueueProgVIdeoID>");
                            sb.AppendLine("<QueueProgFileNO>" + dtArr_Promo.Rows[i]["OLD_FILE_NO"].ToString() + "</QueueProgFileNO>");

                            sb.AppendLine("<FSSTATUS>" + "還未做送帶轉檔" + "</FSSTATUS>");

                            sb.AppendLine("</Data>");
                        }
                    }
                    #endregion
                }
                else //(dtCombineQueue.Rows.Count < 0) 沒有節目表資料
                {
                    return "<Errors>" + "目前沒有節目段落" + "</Errors>";
                }
                sb.AppendLine("</Datas>");
                return sb.ToString();
            }
            catch (Exception ex)
            {
                StringBuilder sbError = new StringBuilder();
                sbError.AppendLine("<Errors>" + ex.Message + "</Errors>");
                return sbError.ToString();
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }

           
            string ReturnString = "";
           
            if (ReturnString == "")
                return "ok";
            else
                return ReturnString;
        }


        [WebMethod()]
        public string CheckArrListFileSeg(List<ArrPromoList> InList)
        {
            //檢查節目表中每個節目跟Promo的段落資料是否和資料庫中已經轉檔完成的段落相同
            //不檢查Live節目
            string ReturnString = "";
            foreach (ArrPromoList TemArrPromoList in InList)
            {
                if (TemArrPromoList.FNLIVE != "1") //表示非為Live
                {
                    if (TemArrPromoList.FCTYPE == "G") //節目
                    {

                        //                        A.FSID,A.FNEPISODE,B.FSBEG_TIMECODE,B.FSEND_TIMECODE,
                        //B.FCLOW_RES,B.FNSEG_ID,B.FSVIDEO_ID,A.FSFILE_NO
                        //from TBLOG_VIDEO A,TBLOG_VIDEO_SEG B,TBZCHANNEL C
                        //where
                        //A.FSFILE_NO=B.FSFILE_NO and A.FSARC_TYPE=C.FSCHANNEL_TYPE
                        //and A.FSID=@FSPROG_ID and A.FNEPISODE=@FNEPISODE and (A.FCFILE_STATUS='T' or A.FCFILE_STATUS='Y')
                        //and C.FSCHANNEL_ID=@FSCHANNEL_ID order by B.FNSEG_ID

                        String sqlConnStr = "";
                        sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
                        SqlConnection connection = new SqlConnection(sqlConnStr);
                        StringBuilder sb = new StringBuilder();

                        SqlDataAdapter da = new SqlDataAdapter("SP_Q_TBPGM_PROG_REAL_SEG", connection);
                        DataTable dt = new DataTable();


                        SqlParameter para = new SqlParameter("@FSPROG_ID", TemArrPromoList.FSPROG_ID);
                        SqlParameter para1 = new SqlParameter("@FNEPISODE", TemArrPromoList.FNEPISODE);
                        SqlParameter para2 = new SqlParameter("@FSCHANNEL_ID", TemArrPromoList.FSCHANNEL_ID);
                        SqlParameter para3 = new SqlParameter("@FNSEG_ID", String.Format("{0:00}", TemArrPromoList.FNBREAK_NO));

                        da.SelectCommand.Parameters.Add(para);
                        da.SelectCommand.Parameters.Add(para1);
                        da.SelectCommand.Parameters.Add(para2);
                        da.SelectCommand.Parameters.Add(para3);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        try
                        {
                            if (connection.State == System.Data.ConnectionState.Closed)
                            {
                                connection.Open();
                            }

                            da.Fill(dt);

                            if (dt.Rows.Count > 0)
                            {
                                //當長度不同
                                if ((MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dt.Rows[0]["FSEND_TIMECODE"].ToString()) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dt.Rows[0]["FSBEG_TIMECODE"].ToString())) != MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION)
                                    || dt.Rows[0]["FSVIDEO_ID"].ToString() != TemArrPromoList.FSVIDEO_ID
                                    || dt.Rows[0]["FSFILE_NO"].ToString() != TemArrPromoList.FSFILE_NO)
                                {
                                    ReturnString = ReturnString + "節目:" + TemArrPromoList.FSPROG_NAME + " 集數:" + TemArrPromoList.FNEPISODE + " :第" + TemArrPromoList.FNBREAK_NO + "段與實際轉檔資料段落長度不符" + Convert.ToChar(10) + Convert.ToChar(13);
                                }

                            }
                            else
                            {
                                ReturnString = ReturnString + "節目:" + TemArrPromoList.FSPROG_NAME + " 集數:" + TemArrPromoList.FNEPISODE + " :第" + TemArrPromoList.FNBREAK_NO + "段與實際轉檔資料段落長度不符" + Convert.ToChar(10) + Convert.ToChar(13);

                            }
                            connection.Close();
                            connection.Dispose();
                            GC.Collect();

                        }
                        catch (Exception ex)
                        {

                        }
                        finally
                        {
                            connection.Close();
                            connection.Dispose();
                            GC.Collect();
                        }
                    }
                    else //Promo類型
                    {
                        String sqlConnStr = "";
                        sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
                        SqlConnection connection = new SqlConnection(sqlConnStr);
                        StringBuilder sb = new StringBuilder();

                        SqlDataAdapter da = new SqlDataAdapter("SP_Q_TBPGM_PROG_REAL_SEG", connection);
                        DataTable dt = new DataTable();


                        SqlParameter para = new SqlParameter("@FSPROG_ID", TemArrPromoList.FSPROG_ID);
                        SqlParameter para1 = new SqlParameter("@FNEPISODE", "0");
                        SqlParameter para2 = new SqlParameter("@FSCHANNEL_ID", TemArrPromoList.FSCHANNEL_ID);
                        SqlParameter para3 = new SqlParameter("@FNSEG_ID", "1");

                        da.SelectCommand.Parameters.Add(para);
                        da.SelectCommand.Parameters.Add(para1);
                        da.SelectCommand.Parameters.Add(para2);
                        da.SelectCommand.Parameters.Add(para3);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        try
                        {
                            if (connection.State == System.Data.ConnectionState.Closed)
                            {
                                connection.Open();
                            }

                            da.Fill(dt);

                            if (dt.Rows.Count > 0)
                            {
                                //當長度不同
                                if ((MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dt.Rows[0]["FSEND_TIMECODE"].ToString()) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dt.Rows[0]["FSBEG_TIMECODE"].ToString())) != MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION)
                                    || dt.Rows[0]["FSVIDEO_ID"].ToString() != TemArrPromoList.FSVIDEO_ID
                                    || dt.Rows[0]["FSFILE_NO"].ToString() != TemArrPromoList.FSFILE_NO)
                                {
                                    ReturnString = ReturnString + "Promo:" + TemArrPromoList.FSPROG_NAME + "與實際轉檔資料不符" + Convert.ToChar(10) + Convert.ToChar(13);
                                }

                            }
                            else
                            {
                                ReturnString = ReturnString + "Promo:" + TemArrPromoList.FSPROG_NAME + "與實際轉檔資料不符" + Convert.ToChar(10) + Convert.ToChar(13);
                            }
                            connection.Close();
                            connection.Dispose();
                            GC.Collect();

                        }
                        catch (Exception ex)
                        {

                        }
                        finally
                        {
                            connection.Close();
                            connection.Dispose();
                            GC.Collect();
                        }
                    }
                }

                //SP_Q_TBPGM_PROG_REAL_SEG

            }
            if (ReturnString == "")
                return "ok";
            else
                return ReturnString;
        }

        [WebMethod()]
        //取得Promo段落長度資料,並計算長度將其轉為Frame數,如果沒有則傳回0 
        //(於是要寫入主控DB,之後都不能修改 ,所以此查詢需要檔案狀態為T or Y 才會取得)
        //GetDataType 看回傳值類型為何,Dur 表示回傳長度,SOM表示回傳起始時間
        public int getPromoSegmentLength(string VideoID, string GetDataType, string indur)
        {
            //DataTable dtProgSeg = new DataTable();
            //每一個節目,先查詢節目段落看是否有資料
            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection1 = new SqlConnection(sqlConnStr);
            if (connection1.State == System.Data.ConnectionState.Closed)
            {
                connection1.Open();
            }
            StringBuilder sb = new StringBuilder();
            SqlDataAdapter daPromoSeg = new SqlDataAdapter("SP_Q_TBLOG_VIDEO_SEG_DURATION_BY_VIDEOID", connection1);
            DataTable dtPromoSeg = new DataTable();
            SqlParameter paraVIDEOID = new SqlParameter("@FSVIDEO_ID", VideoID);
            daPromoSeg.SelectCommand.Parameters.Add(paraVIDEOID);

            daPromoSeg.SelectCommand.CommandType = CommandType.StoredProcedure;
            daPromoSeg.Fill(dtPromoSeg);
            connection1.Close();
            if (dtPromoSeg.Rows.Count > 0)
            {
                int SegDur;
                SegDur = MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtPromoSeg.Rows[0]["FSEND_TIMECODE"].ToString()) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtPromoSeg.Rows[0]["FSBEG_TIMECODE"].ToString());
                if (indur !="") //如果沒有帶入長度
                {
                    if (SegDur!= MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(indur)) //如果比對長度不相同則為假段落長度
                        return -1;
                
                }

                if (GetDataType == "Dur")
                    return SegDur;
                else  //SOM
                    return MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(dtPromoSeg.Rows[0]["FSBEG_TIMECODE"].ToString());
            }
            else //如果查無資料
            {
                return -1; //表示查無資料
            }
        }


        List<ArrPromoListAddSOM> PGM_ARR_PROMOAddSOM = new List<ArrPromoListAddSOM>();

        [WebMethod()]
        public ArrPromoListAddSOM AddSOMToArrPromoList(ArrPromoList inArrPromoList)
        {

            ArrPromoListAddSOM RArrPromoList = new ArrPromoListAddSOM();
            RArrPromoList.COMBINENO = inArrPromoList.COMBINENO;
            RArrPromoList.FCTIME_TYPE = inArrPromoList.FCTIME_TYPE;
            RArrPromoList.FSPROG_ID = inArrPromoList.FSPROG_ID;
            RArrPromoList.FNSEQNO = inArrPromoList.FNSEQNO;
            RArrPromoList.FSNAME = inArrPromoList.FSNAME;
            RArrPromoList.FSCHANNEL_ID = inArrPromoList.FSCHANNEL_ID;
            RArrPromoList.FSCHANNEL_NAME = inArrPromoList.FSCHANNEL_NAME;
            RArrPromoList.FSPLAY_TIME = inArrPromoList.FSPLAY_TIME;
            RArrPromoList.FSDURATION = inArrPromoList.FSDURATION;
            RArrPromoList.FNEPISODE = inArrPromoList.FNEPISODE;
            if (inArrPromoList.FNBREAK_NO!="")
                RArrPromoList.FNBREAK_NO = int.Parse(inArrPromoList.FNBREAK_NO).ToString();
            else
                RArrPromoList.FNBREAK_NO = inArrPromoList.FNBREAK_NO;
            RArrPromoList.FNSUB_NO = inArrPromoList.FNSUB_NO;
            RArrPromoList.FSFILE_NO = inArrPromoList.FSFILE_NO;
            RArrPromoList.FSVIDEO_ID = inArrPromoList.FSVIDEO_ID;
            RArrPromoList.FSPROMO_ID = inArrPromoList.FSPROMO_ID;
            RArrPromoList.FDDATE = inArrPromoList.FDDATE;
            RArrPromoList.FNLIVE = inArrPromoList.FNLIVE;
            RArrPromoList.FCTYPE = inArrPromoList.FCTYPE;
            RArrPromoList.FSSTATUS = inArrPromoList.FSSTATUS;
            RArrPromoList.FSSIGNAL = inArrPromoList.FSSIGNAL;
            RArrPromoList.FSMEMO = inArrPromoList.FSMEMO;
            RArrPromoList.FSPROG_NAME = inArrPromoList.FSPROG_NAME;
            RArrPromoList.FBREALTIMECODE = "T";
            if (inArrPromoList.FSVIDEO_ID != "")
            {
                int ReturnDur;
                if (inArrPromoList.FCTYPE == "G")
                    ReturnDur = getPromoSegmentLength(inArrPromoList.FSVIDEO_ID + int.Parse(inArrPromoList.FNBREAK_NO).ToString("00"), "SOM", inArrPromoList.FSDURATION);
                else
                    ReturnDur = getPromoSegmentLength(inArrPromoList.FSVIDEO_ID, "SOM", inArrPromoList.FSDURATION);

                if (ReturnDur == -1)
                {
                    RArrPromoList.FSSOM = "00:00:00:00";
                    RArrPromoList.FBREALTIMECODE = "F";
                }
                else
                {
                    RArrPromoList.FSSOM = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(ReturnDur);
                    RArrPromoList.FBREALTIMECODE = "T";
                }
                //if (inArrPromoList.FCTYPE == "G") //節目的話要加入段落
                //    RArrPromoList.FSSOM = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(int.Parse(getPromoSegmentLength(inArrPromoList.FSVIDEO_ID + int.Parse(inArrPromoList.FNBREAK_NO).ToString("00"), "SOM").ToString()));
                //else
                //    RArrPromoList.FSSOM = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(int.Parse(getPromoSegmentLength(inArrPromoList.FSVIDEO_ID, "SOM").ToString()));
            }
            else
            {
                RArrPromoList.FSSOM = "00:00:00:00";
                RArrPromoList.FBREALTIMECODE = "F";
            }

            return RArrPromoList;
        }


        [WebMethod()]
        public string TransLst(List<ArrPromoList> InList)
        {

            PGM_ARR_PROMOAddSOM.Clear();
            foreach (ArrPromoList TemArrPromoList in InList)
            {
                PGM_ARR_PROMOAddSOM.Add(AddSOMToArrPromoList(TemArrPromoList));
            }

            string TempChannelID = "";
            string TempDate = "";
            string TempChannelType = "";
            List<ArrPromoListAddSOM> UniqueList = new List<ArrPromoListAddSOM>();
            #region 開始建立檔案,給維智將資料寫入Louth DB使用
            try
            {
                //    string DateString = "";
                //    DateString = DateString + DateTime.Now.Year + String.Format("{0:00}", DateTime.Now.Month) + String.Format("{0:00}", DateTime.Now.Day) + "-" + String.Format("{0:00}", DateTime.Now.Hour) + String.Format("{0:00}", DateTime.Now.Minute) + String.Format("{0:00}", DateTime.Now.Second);

                //    string InsDateString = "";

                //    InsDateString = InsDateString + DateTime.Now.Year + "-" + String.Format("{0:00}", DateTime.Now.Month) + "-" + String.Format("{0:00}", DateTime.Now.Day) + " " + String.Format("{0:00}", DateTime.Now.Hour) + ":" + String.Format("{0:00}", DateTime.Now.Minute) + ":" + String.Format("{0:00}", DateTime.Now.Second) + "." + String.Format("{0:000}", DateTime.Now.Millisecond);

                //    //開啟建立檔案,給維智將資料寫入Louth DB使用

                //    //RealPath @"\\10.13.200.1\MasterCtrl\MSG\LOUTH\InputXML\"
                //    //string TestPath = @"\\10.13.220.42\Project\Test\";
                //    //string TestPath = @"D:\Tmp\";
                //    string TestPath = @"\\10.13.200.1\MasterCtrl\MSG\LOUTH\InputXML\";
                //    if (File.Exists(TestPath + DateString + ".xml") == true)
                //        File.Delete(TestPath + DateString + ".xml");
                //    StringBuilder sb = new StringBuilder();
                //    //FileStream myFile = File.Open(@"C:\0Selfsys\1234.lst", FileMode.Open, FileAccess.ReadWrite);
                //    FileStream myFile = File.Create(TestPath + DateString + ".xml");
                //    //FileStream fs = new FileStream(@"c:\sw.txt", FileMode.Create, FileAccess.Write);
                //    //StreamWriter m_streamWriter = new StreamWriter(fs);
                //    //// Write to the file using StreamWriter class
                //    StreamWriter m_streamWriter = new StreamWriter(myFile);

                //    //BinaryWriter myWriter = new BinaryWriter(myFile);
                //    //int WorkRow = 0;
                //    sb.AppendLine("<Datas>");
                int TempI = 1;
                //要先建立一個獨立清單,不包含重複資料
                #region 要先建立一個獨立清單,不包含重複資料

                foreach (ArrPromoListAddSOM TemArrPromoList in PGM_ARR_PROMOAddSOM)
                {
                    if (TemArrPromoList.FDDATE != "" && TemArrPromoList.FDDATE != null)
                    {
                        TempChannelID = TemArrPromoList.FSCHANNEL_ID;
                        TempDate = TemArrPromoList.FDDATE;
                        ///開始取得頻道類型,看是否為HD頻道
                        #region 開始取得頻道類型,看是否為HD頻道
                        String sqlConnStr = "";
                        sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
                        SqlConnection connection = new SqlConnection(sqlConnStr);
                        StringBuilder sb = new StringBuilder();

                        SqlDataAdapter da = new SqlDataAdapter("SP_Q_TBZCHANNEL", connection);
                        DataTable dt = new DataTable();
                        SqlParameter para1 = new SqlParameter("@FSCHANNEL_ID", TempChannelID);

                        da.SelectCommand.Parameters.Add(para1);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;


                        try
                        {
                            if (connection.State == System.Data.ConnectionState.Closed)
                            {
                                connection.Open();
                            }

                            da.Fill(dt);

                            if (dt.Rows.Count > 0)
                            {
                                if (Convert.IsDBNull(dt.Rows[0]["FSCHANNEL_TYPE"]))
                                    TempChannelType = "";
                                else
                                    TempChannelType = dt.Rows[0]["FSCHANNEL_TYPE"].ToString();
                            }

                            connection.Close();
                            connection.Dispose();
                        }

                        catch (Exception ex)
                        {
                            StringBuilder sbError = new StringBuilder();
                            sbError.AppendLine("<Errors>" + ex.Message + "</Errors>");
                            return sbError.ToString();
                        }

                        #endregion
                    }
                    //先檢查是否有加入過清單
                    bool NeedAdd = false;
                    foreach (ArrPromoListAddSOM TempUniqueList in UniqueList)
                    {
                        if (TemArrPromoList.FCTYPE == "G") //節目則要取段落,才表示唯一
                        {
                            if (TemArrPromoList.FSVIDEO_ID == TempUniqueList.FSVIDEO_ID && TemArrPromoList.FNBREAK_NO == TempUniqueList.FNBREAK_NO)
                            {
                                NeedAdd = true;
                            }
                        }
                        else //Promo則不需要取段落表示唯一
                        {
                            if (TemArrPromoList.FSVIDEO_ID == TempUniqueList.FSVIDEO_ID)
                            {
                                NeedAdd = true;
                            }
                        }
                    }
                    if (NeedAdd == false) //沒被加入過清單
                    {
                        UniqueList.Add(TemArrPromoList);
                    }
                }
                #endregion


                #region 逐一寫入文字檔案


                //    foreach (ArrPromoListAddSOM TemArrPromoList in UniqueList)
                //    {
                //        //20120328 By Mike測試隱藏
                //        if (TemArrPromoList.FSFILE_NO != "" && TemArrPromoList.FBREALTIMECODE != "F")
                //        {
                //            if (TemArrPromoList.FCTYPE == "P") //Promo
                //            {
                //                //要先查詢否與實際檔案長度(TBLOG_VIDEO_SEG.FSVIDEO_ID FSBEG_TIMECODE,FSEND_TIMECODE 相減 相同
                //                #region 檢查 Promo 實際長度,如果相同在寫入
                //                int PromoLength;
                //                PromoLength = getPromoSegmentLength(TemArrPromoList.FSVIDEO_ID, "Dur");
                //                #endregion
                //                //比對 Promo 實際長度,如果相同再寫
                //                if (PromoLength == MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION))
                //                {
                //                    sb.AppendLine("<Data>");
                //                    sb.AppendLine("<TableName>ASDB</TableName>");
                //                    sb.AppendLine("<Type>s</Type>");

                //                    sb.AppendLine("<Identifier>" + TemArrPromoList.FSVIDEO_ID + "</Identifier>");
                //                    //sb.AppendLine("<Identifier>" + String.Format("{0:00000000}", TempI) + "</Identifier>");

                //                    sb.AppendLine("<Title>" + ReplaceXML(GetSubString(TemArrPromoList.FSNAME, 16)) + "</Title>");
                //                    TempI = TempI + 1;
                //                    sb.AppendLine("<Operator>" + "MAM" + "</Operator>");

                //                    sb.AppendLine("<StartOfMessage>" + TimecodeToLouthTimecode(TemArrPromoList.FSSOM) + "</StartOfMessage>");
                //                    sb.AppendLine("<Duration>" + TimecodeToLouthTimecode(TemArrPromoList.FSDURATION) + "</Duration>");
                //                    sb.AppendLine("<LabelType>" + "0" + "</LabelType>");
                //                    sb.AppendLine("<VideoFormat>" + "4" + "</VideoFormat>");
                //                    sb.AppendLine("<AudioFormat>" + "1" + "</AudioFormat>");
                //                    sb.AppendLine("<VideoQuality>" + "0" + "</VideoQuality>");
                //                    sb.AppendLine("<MadeDateTime>" + InsDateString + "</MadeDateTime>");
                //                    sb.AppendLine("<AirDateTime>" + InsDateString + "</AirDateTime>");
                //                    sb.AppendLine("<PurgeDateTime>" + InsDateString + "</PurgeDateTime>");
                //                    sb.AppendLine("<UserString>" + GetPromoWELFARE(TemArrPromoList.FSPROMO_ID) + "</UserString>");
                //                    sb.AppendLine("</Data>");
                //                }
                //            }
                //            else //Prog
                //            {
                //                if (TemArrPromoList.FNBREAK_NO == "1")  //第一段要先加入ASDB,每段要加ASSEG
                //                {
                //                    int dur = 0;
                //                    foreach (ArrPromoListAddSOM AAA in PGM_ARR_PROMOAddSOM)
                //                    {
                //                        if (AAA.FSPROG_ID == TemArrPromoList.FSPROG_ID && AAA.FCTYPE == "G" && AAA.FNSEQNO == TemArrPromoList.FNSEQNO)
                //                        {
                //                            dur = dur + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(AAA.FSDURATION);
                //                        }
                //                    }

                //                    sb.AppendLine("<Data>");
                //                    sb.AppendLine("<TableName>ASDB</TableName>");
                //                    sb.AppendLine("<Type>m</Type>");


                //                    sb.AppendLine("<Identifier>" + TemArrPromoList.FSVIDEO_ID + "</Identifier>");
                //                    //sb.AppendLine("<Identifier>" + String.Format("{0:00000000}", TempI) + "</Identifier>");



                //                    //節目 Title 要加入集數
                //                    if (TemArrPromoList.FNEPISODE != "" && TemArrPromoList.FNEPISODE != null)
                //                        sb.AppendLine("<Title>" + ReplaceXML(GetSubString(TemArrPromoList.FSNAME, 16 - (TemArrPromoList.FNEPISODE.Length + 1)) + "#" + TemArrPromoList.FNEPISODE) + "</Title>");
                //                    else
                //                        sb.AppendLine("<Title>" + ReplaceXML(GetSubString(TemArrPromoList.FSNAME, 16)) + "</Title>");


                //                    TempI = TempI + 1;
                //                    sb.AppendLine("<Operator>" + "MAM" + "</Operator>");

                //                    sb.AppendLine("<StartOfMessage>" + TimecodeToLouthTimecode(TemArrPromoList.FSSOM) + "</StartOfMessage>");

                //                    //sb.AppendLine("<StartOfMessage>" + "0" + "</StartOfMessage>");
                //                    sb.AppendLine("<Duration>" + TimecodeToLouthTimecode(MAM_PTS_DLL.TimeCodeCalc.frame2timecode(dur)) + "</Duration>");
                //                    sb.AppendLine("<LabelType>" + "0" + "</LabelType>");
                //                    sb.AppendLine("<VideoFormat>" + "4" + "</VideoFormat>");
                //                    sb.AppendLine("<AudioFormat>" + "1" + "</AudioFormat>");
                //                    sb.AppendLine("<VideoQuality>" + "0" + "</VideoQuality>");
                //                    sb.AppendLine("<MadeDateTime>" + InsDateString + "</MadeDateTime>");
                //                    sb.AppendLine("<AirDateTime>" + InsDateString + "</AirDateTime>");
                //                    sb.AppendLine("<PurgeDateTime>" + InsDateString + "</PurgeDateTime>");
                //                    sb.AppendLine("<UserString>" + "" + "</UserString>");
                //                    sb.AppendLine("</Data>");

                //                }
                //                sb.AppendLine("<Data>");
                //                sb.AppendLine("<TableName>ASSEG</TableName>");
                //                sb.AppendLine("<Type>m</Type>");
                //                sb.AppendLine("<SegNum>" + TemArrPromoList.FNBREAK_NO + "</SegNum>");
                //                sb.AppendLine("<Identifier>" + TemArrPromoList.FSVIDEO_ID + "</Identifier>");
                //                //sb.AppendLine("<Identifier>" + String.Format("{0:00000000}", TempI) + "</Identifier>");

                //                //節目 Title 要加入集數
                //                if (TemArrPromoList.FNEPISODE != "" && TemArrPromoList.FNEPISODE != null)
                //                    sb.AppendLine("<Title>" + ReplaceXML(GetSubString(TemArrPromoList.FSNAME, 16 - (TemArrPromoList.FNEPISODE.Length + 1)) + "#" + TemArrPromoList.FNEPISODE) + "</Title>");
                //                else
                //                    sb.AppendLine("<Title>" + ReplaceXML(GetSubString(TemArrPromoList.FSNAME, 16)) + "</Title>");


                //                TempI = TempI + 1;
                //                sb.AppendLine("<StartOfMessage>" + TimecodeToLouthTimecode(TemArrPromoList.FSSOM) + "</StartOfMessage>");
                //                sb.AppendLine("<Duration>" + TimecodeToLouthTimecode(TemArrPromoList.FSDURATION) + "</Duration>");
                //                sb.AppendLine("<NoteDateTime>" + InsDateString + "</NoteDateTime>");
                //                sb.AppendLine("</Data>");

                //            }
                //        }
                //    }
                #endregion



                //    sb.AppendLine("</Datas>");
                //    m_streamWriter.Write(sb.ToString());
                //    m_streamWriter.Flush();
                //    m_streamWriter.Close();
                //    m_streamWriter.Dispose();
                //    myFile.Close();
                //    myFile.Dispose();
                //    if (File.Exists(TestPath + DateString + ".xml.pgmok") == true)
                //        File.Delete(TestPath + DateString + ".xml.pgmok");
                //    //FileStream myFile = File.Open(@"C:\0Selfsys\1234.lst", FileMode.Open, FileAccess.ReadWrite);
                //    FileStream myFile1 = File.Create(TestPath + DateString + ".xml.pgmok");
                //    StreamWriter m_streamWriter1 = new StreamWriter(myFile1);
                //    m_streamWriter1.Write("");
                //    m_streamWriter1.Flush();
                //    m_streamWriter1.Close();
                //    m_streamWriter1.Dispose();
                //    myFile1.Close();
                //    myFile1.Dispose();
                bool NeedClearFirstPlayList = true;

                //先清除原有的首播清單紀錄再逐一檢查是否為首播,如果為首播則修改TBLOG_VIDEO_SEG首播日期與首播頻道
                //為了防止今天有些節目排了後又拿掉(所以沒有播出),要先將今日的首播清單TBLOG_Video_Seg.FDDISPLAY_TIME & FSDISPLAY_Channel
                #region 先清除原有的首播清單紀錄再逐一檢查是否為首播,如果為首播則修改TBLOG_VIDEO_SEG首播日期與首播頻道

                foreach (ArrPromoListAddSOM TemArrPromoList in UniqueList)
                {
                    if (NeedClearFirstPlayList == true) //只有第一筆需要清除首播清單
                    {
                        if (Clear_First_Play(TemArrPromoList.FDDATE, TemArrPromoList.FSCHANNEL_ID) == true)
                        {
                        }
                        NeedClearFirstPlayList = false;
                    }
                    if (TemArrPromoList.FSVIDEO_ID != "")
                    {
                        //  select FDDISPLAY_TIME from tblog_video_seg where FSVIDEO_ID='TemArrPromoList.FSVIDEO_ID + int.Parse(TemArrPromoList.FNBREAK_NO).ToString("00")'
                        if (TemArrPromoList.FCTYPE == "P") //Promo
                        {
                            if (Check_First_Play(TemArrPromoList.FSVIDEO_ID, TemArrPromoList.FDDATE, TemArrPromoList.FSCHANNEL_ID) == true)
                            {

                            }

                        }
                        else //Prog
                        {
                            if (Check_First_Play(TemArrPromoList.FSVIDEO_ID + int.Parse(TemArrPromoList.FNBREAK_NO).ToString("00"), TemArrPromoList.FDDATE, TemArrPromoList.FSCHANNEL_ID) == true)
                            {

                            }
                        }

                    }
                }

                #endregion

            }
            catch
            {
            }

            #endregion

            bool ISLive = false;

            try
            {
                //開啟建立檔案
                string FileChannelDate = "";
                FileChannelDate = TempChannelID + String.Format("{0:00}", Convert.ToDateTime(TempDate).Year) + String.Format("{0:00}", Convert.ToDateTime(TempDate).Month) + String.Format("{0:00}", Convert.ToDateTime(TempDate).Day);
                string lstFileName = "";
                lstFileName = FileChannelDate + ".lst";
                string lstFileName1 = "";
                lstFileName1 = FileChannelDate+"(louth)" + ".lst";

                string DateString = String.Format("{0:00}", Convert.ToDateTime(TempDate).Year) + String.Format("{0:00}", Convert.ToDateTime(TempDate).Month) + String.Format("{0:00}", Convert.ToDateTime(TempDate).Day);
                string CombinelstFileName = ""; //Program -Only

                string PlayListTXT = DateString + "_All";
                string ProgOnlyTXT = DateString + "_prog";

                int PlayListFileNumber = 0; //第幾個檔案,給HD TXT 使用,超過196筆資料要換一個檔名
                int PlayListRowNumber = 1;  //第幾筆資料,給HD TXT 使用,超過196筆資料要換一個檔名

                int ProgOnlyFileNumber = 0; //第幾個檔案,給HD TXT 使用,超過196筆資料要換一個檔名
                int ProgOnlyRowNumber = 1;  //第幾筆資料,給HD TXT 使用,超過196筆資料要換一個檔名


                string FirstPlayListFileName = ""; //首播清單 
                if (TempChannelID == "01") //公視主頻
                {
                    CombinelstFileName = "P" + DateString + "_prog.lst";
                    FirstPlayListFileName = "FP" + DateString + ".lst";
                }
                else if (TempChannelID == "02") //DImo
                {
                    CombinelstFileName = "D" + DateString + "_prog.lst";
                    FirstPlayListFileName = "FD" + DateString + ".lst";
                }
                else if (TempChannelID == "07") //客家
                {
                    CombinelstFileName = "H" + DateString + "_prog.lst";
                    FirstPlayListFileName = "FH" + DateString + ".lst";
                }
                else if (TempChannelID == "08") //宏觀
                {
                    CombinelstFileName = "M" + DateString + "_prog.lst";
                    FirstPlayListFileName = "FM" + DateString + ".lst";
                }
                else if (TempChannelID == "09") //原民
                {
                    CombinelstFileName = "I" + DateString + "_prog.lst";
                    FirstPlayListFileName = "FI" + DateString + ".lst";
                }
                else if (TempChannelID == "11") //HD頻道
                {
                    CombinelstFileName = "N" + DateString + "_prog.lst";
                    FirstPlayListFileName = "FN" + DateString + ".lst";
                    PlayListTXT = "O" + "N" + DateString + "_All";
                    ProgOnlyTXT = "O" + "N" + DateString + "_prog";
                }
                else if (TempChannelID == "51") //主頻MOD
                {
                    CombinelstFileName = "O" + DateString + "_prog.lst";
                    FirstPlayListFileName = "FO" + DateString + ".lst";
                }
                else if (TempChannelID == "61") //HD MOD
                {
                    CombinelstFileName = "I" + DateString + "_prog.lst";
                    FirstPlayListFileName = "FI" + DateString + ".lst";
                    PlayListTXT = "O" + "A" + DateString + "_All";
                    ProgOnlyTXT = "O" + "A" + DateString + "_prog";
                }
                else if (TempChannelID == "12") //PTS1
                {
                    CombinelstFileName = "B" + DateString + "_prog.lst";
                    FirstPlayListFileName = "FB" + DateString + ".lst";
                    PlayListTXT = "O" + "B" + DateString + "_All";
                    ProgOnlyTXT = "O" + "B" + DateString + "_prog";
                }
                else if (TempChannelID == "13") //PTS2
                {
                    CombinelstFileName = "C" + DateString + "_prog.lst";
                    FirstPlayListFileName = "FC" + DateString + ".lst";
                    PlayListTXT = "O" + "C" + DateString + "_All";
                    ProgOnlyTXT = "O" + "C" + DateString + "_prog";
                }
                else if (TempChannelID == "14") //PTS3
                {
                    CombinelstFileName = "E" + DateString + "_prog.lst";
                    FirstPlayListFileName = "FE" + DateString + ".lst";
                    PlayListTXT = "O" + "E" + DateString + "_All";
                    ProgOnlyTXT = "O" + "E" + DateString + "_prog";
                }
                else if (TempChannelID == "62") //PTS1 MOD
                {
                    CombinelstFileName = "K" + DateString + "_prog.lst";
                    FirstPlayListFileName = "FK" + DateString + ".lst";
                    PlayListTXT = "O" + "K" + DateString + "_All";
                    ProgOnlyTXT = "O" + "K" + DateString + "_prog";
                }
                else if (TempChannelID == "64") //PTS3 MOD
                {
                    CombinelstFileName = "R" + DateString + "_prog.lst";
                    FirstPlayListFileName = "FR" + DateString + ".lst";
                    PlayListTXT = "O" + "R" + DateString + "_All";
                    ProgOnlyTXT = "O" + "R" + DateString + "_prog";
                }
                else
                {
                    CombinelstFileName = "ProOnly" + DateString + "_prog.lst";
                    FirstPlayListFileName = "FirPlay" + DateString + ".lst";
                }

                //if (File.Exists(HttpContext.Current.Server.MapPath("~/UploadForder/" + "1234.lst")) == true)
                //    File.Delete(HttpContext.Current.Server.MapPath("~/UploadForder/" + "1234.lst"));
                ////FileStream myFile = File.Open(@"C:\0Selfsys\1234.lst", FileMode.Open, FileAccess.ReadWrite);
                //FileStream myFile = File.Create(HttpContext.Current.Server.MapPath("~/UploadForder/" + "1234.lst"));
                if (File.Exists(HttpContext.Current.Server.MapPath("~/UploadFolder/" + lstFileName)) == true)
                    File.Delete(HttpContext.Current.Server.MapPath("~/UploadFolder/" + lstFileName));
                if (File.Exists(HttpContext.Current.Server.MapPath("~/UploadFolder/" + CombinelstFileName)) == true)
                    File.Delete(HttpContext.Current.Server.MapPath("~/UploadFolder/" + CombinelstFileName));                //FileStream myFile = File.Open(@"C:\0Selfsys\1234.lst", FileMode.Open, FileAccess.ReadWrite);
                if (File.Exists(HttpContext.Current.Server.MapPath("~/UploadFolder/" + FirstPlayListFileName)) == true)
                    File.Delete(HttpContext.Current.Server.MapPath("~/UploadFolder/" + FirstPlayListFileName));

                if (File.Exists(HttpContext.Current.Server.MapPath("~/UploadFolder/" + lstFileName1)) == true)
                    File.Delete(HttpContext.Current.Server.MapPath("~/UploadFolder/" + lstFileName1));
                FileStream myFile = File.Create(HttpContext.Current.Server.MapPath("~/UploadFolder/" + lstFileName));
                BinaryWriter myWriter = new BinaryWriter(myFile);

                FileStream myFile1 = File.Create(HttpContext.Current.Server.MapPath("~/UploadFolder/" + lstFileName1));
                BinaryWriter myWriter1 = new BinaryWriter(myFile1);

                FileStream myCombineFile = File.Create(HttpContext.Current.Server.MapPath("~/UploadFolder/" + CombinelstFileName));
                BinaryWriter myCombineWriter = new BinaryWriter(myCombineFile);

                FileStream myPlayListTXTFile = File.Create(HttpContext.Current.Server.MapPath("~/UploadFolder/" + PlayListTXT + ".txt"));
                TextWriter myPlayListTXTWrite = new StreamWriter(myPlayListTXTFile);

                FileStream myProgOnlyTXTFile = File.Create(HttpContext.Current.Server.MapPath("~/UploadFolder/" + ProgOnlyTXT + ".txt"));
                TextWriter myProgOnlyTXTWrite = new StreamWriter(myProgOnlyTXTFile);



                #region 開始寫入TXT 節目清單
                //if (TempChannelID == "11" || TempChannelID == "61")
                if (TempChannelType == "002")
                {
                    foreach (ArrPromoListAddSOM TempUniqueList in UniqueList)
                    {
                        if (TempUniqueList.FCTYPE == "G")
                        {
                            ProgOnlyRowNumber = ProgOnlyRowNumber + 1;
                            if (ProgOnlyRowNumber > 196)
                            {
                                myProgOnlyTXTWrite.Flush();
                                myProgOnlyTXTWrite.Close();
                                myProgOnlyTXTWrite.Dispose();
                                myProgOnlyTXTFile.Close();
                                myProgOnlyTXTFile.Dispose();
                                //myProgOnlyTXTFile = null;
                                ProgOnlyRowNumber = 1;
                                ProgOnlyFileNumber = ProgOnlyFileNumber + 1;
                                myProgOnlyTXTFile = File.Create(HttpContext.Current.Server.MapPath("~/UploadFolder/" + ProgOnlyTXT + "-" + ProgOnlyFileNumber.ToString() + ".txt"));
                                myProgOnlyTXTWrite = new StreamWriter(myProgOnlyTXTFile);
                            }
                            if (TempUniqueList.FNLIVE == "1")
                            { }
                            if (ProgOnlyRowNumber == 2)
                            {
                                myProgOnlyTXTWrite.WriteLine(WriteTXTProgOnly(TempUniqueList.FSVIDEO_ID.Substring(2, 6), TempUniqueList.FSPLAY_TIME, TempUniqueList.FSDURATION, TempUniqueList.FSSOM, TempUniqueList.FCTYPE, "auto", TempUniqueList.FSPROG_NAME, TempUniqueList.FNLIVE, TempUniqueList.FSSIGNAL));
                                //myProgOnlyTXTWrite.WriteLine(WriteTXTProgOnly(TempUniqueList.FSVIDEO_ID.Substring(2,6), TempUniqueList.FSPLAY_TIME, TempUniqueList.FSDURATION, TempUniqueList.FSSOM, TempUniqueList.FCTYPE, "hard", TempUniqueList.FSPROG_NAME));
                            }
                            else
                            {
                                myProgOnlyTXTWrite.WriteLine(WriteTXTProgOnly(TempUniqueList.FSVIDEO_ID.Substring(2, 6), TempUniqueList.FSPLAY_TIME, TempUniqueList.FSDURATION, TempUniqueList.FSSOM, TempUniqueList.FCTYPE, "auto", TempUniqueList.FSPROG_NAME, TempUniqueList.FNLIVE, TempUniqueList.FSSIGNAL));
                            }

                        }
                    }

                }

                #endregion






                int WorkRow = 0;
                bool beFirst = false; //是否為第一段
                bool bLast = false;  //是否為最後一段
                string PriorDataType = "";
                foreach (ArrPromoListAddSOM TemArrPromoList in PGM_ARR_PROMOAddSOM)
                {
                    string striType = "0";
                    string EventCOntrol = "";
                    string QF1 = "0";
                    string QF2 = "0";
                    string QF3 = "0";
                    string QF4 = "0";
                    string EF1 = "0";
                    string EF2 = "0";
                    string EF3 = "0";
                    string VideoID = TemArrPromoList.FSVIDEO_ID;
                    string Title = TemArrPromoList.FSNAME;
                    string PlayTime = TemArrPromoList.FSPLAY_TIME;
                    string Dur = TemArrPromoList.FSDURATION;
                    string SOM = TemArrPromoList.FSSOM;
                    //HD頻道只放六碼
                    if (TempChannelType == "002")
                    {
                        if (TemArrPromoList.FSVIDEO_ID.Length == 8)
                            VideoID = TemArrPromoList.FSVIDEO_ID.Substring(2, 6);
                    }

                    if (TemArrPromoList.FCTIME_TYPE == "O")
                        EventCOntrol = "AO";
                    //else if ((TemArrPromoList.FCTYPE=="G") || (TemArrPromoList.FNSUB_NO=="1"))
                    else if ((TemArrPromoList.FCTYPE == "G") || (TemArrPromoList.FCTYPE == "P" && TemArrPromoList.FNBREAK_NO != "0" && TemArrPromoList.FNSUB_NO == "1"))

                        EventCOntrol = "AN";
                    //else if ((TemArrPromoList.FNBREAK_NO=="1" && TemArrPromoList.FCTYPE=="G") || (TemArrPromoList.FNSUB_NO=="1"))
                    else
                        EventCOntrol = "A";

                    //如果前一筆資料為節目,此筆資料為短帶,則要設定為 AN
                    if (PriorDataType == "G" && TemArrPromoList.FCTYPE == "P")
                    {
                        if (TemArrPromoList.FCTIME_TYPE == "O")
                            EventCOntrol = "AO";
                        else
                            EventCOntrol = "AN";
                        //EventCOntrol = "AN";
                    }
                    //用完後將此筆資料的類型設定進PriorDataType,當下一筆時可以使用
                    PriorDataType = TemArrPromoList.FCTYPE;

                    if ((TemArrPromoList.FCTYPE == "G") && (TemArrPromoList.FNLIVE == "1")) //Live節目
                    {
                        if (TemArrPromoList.FCTIME_TYPE == "O")
                            EventCOntrol = "AUNO";
                        else
                            EventCOntrol = "AUN";

                    }

                    if ((TemArrPromoList.FCTYPE == "P") && (TemArrPromoList.FSSIGNAL != "PGM") && (TemArrPromoList.FSSIGNAL != "COMM") && (TemArrPromoList.FSSIGNAL != "")) //Live PROMO
                    {
                        if (TemArrPromoList.FCTIME_TYPE == "O")
                            EventCOntrol = "AUNO";
                        else
                            EventCOntrol = "AUN";

                    }
                    if (TemArrPromoList.FNLIVE == "1")  //如果為Live則要將訊號源寫入VIdeoID中
                    {
                        VideoID = TemArrPromoList.FSSIGNAL;
                    }
                    if (TemArrPromoList.FCTYPE == "G")  //,節目時 EF=FADE-FADE ,FAST  轉場效果
                    {
                        EF1 = "3";
                        EF2 = "0";
                        EF3 = "2";
                    }
                    //myWriter.Write(WriteProgByte(TemArrPromoList));
                    //myWriter.Write(WriteLouthByteArray(TemArrPromoList,string striType,string VideoID,string Title, string PlayTime,string Dur,string SOM,string EventControl,string QF1,string QF2,string QF3,string QF4,string EF1,string EF2,string EF3));
                    //寫入表中的資料,節目或宣傳帶

                    //if (TemArrPromoList.FSMEMO.IndexOf("時間") > -1)
                    //    EF2 = "1";

                    if (TemArrPromoList.FSMEMO.ToUpper().IndexOf("LOGO") > -1) //Logo那節目要+Lose
                        EF2 = "1";
                    if (TemArrPromoList.FSPROG_ID == "2004090" && TemArrPromoList.FCTYPE == "G") //訊號測試節目要+Lose
                        EF2 = "1";
                    myWriter.Write(WriteLouthByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3, TempChannelType));
                    if (TempChannelType=="002")
                        myWriter1.Write(WriteLouthByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3, "001"));

                    if (TemArrPromoList.FCTYPE == "G")
                        if (TemArrPromoList.FNLIVE != "1") //不要Live資料,Live資料不要寫入 Program Only清單
                            myCombineWriter.Write(WriteLouthByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3, TempChannelType));

                    if (TemArrPromoList.FCTYPE == "G")
                    {
                        beFirst = false; //是否為第一段
                        bLast = false;  //是否為最後一段
                        if (WorkRow < PGM_ARR_PROMOAddSOM.Count - 1) //檢查此資料狀態,是否為第一段 和 是否為最後一段 和 是否只有一段
                        {
                            if (TemArrPromoList.FNBREAK_NO == "1")
                                beFirst = true;
                            int NextProgRow = WorkRow + 1;
                            while (NextProgRow < PGM_ARR_PROMOAddSOM.Count - 1 && PGM_ARR_PROMOAddSOM[NextProgRow].FCTYPE == "P")
                                NextProgRow = NextProgRow + 1;

                            if (PGM_ARR_PROMOAddSOM[NextProgRow].FCTYPE == "G")
                            {
                                if (PGM_ARR_PROMOAddSOM[NextProgRow].FSPROG_ID != TemArrPromoList.FSPROG_ID || PGM_ARR_PROMOAddSOM[NextProgRow].FNSEQNO != TemArrPromoList.FNSEQNO)
                                    bLast = true; //表示最後一段
                            }
                            else
                            {
                                bLast = true; //表示最後一段
                            }
                        }
                    }

                    #region 開始寫入TXT播出運行表
                    //if (TempChannelID == "11" || TempChannelID == "61")
                    if (TempChannelType == "002")
                    {
                        PlayListRowNumber = PlayListRowNumber + 1;
                        if (PlayListRowNumber > 196)
                        {

                            myPlayListTXTWrite.Flush();
                            myPlayListTXTWrite.Close();
                            myPlayListTXTWrite.Dispose();
                            myPlayListTXTFile.Close();
                            myPlayListTXTFile.Dispose();
                            //myPlayListTXTFile = null;
                            PlayListRowNumber = 1;
                            PlayListFileNumber = PlayListFileNumber + 1;
                            myPlayListTXTFile = File.Create(HttpContext.Current.Server.MapPath("~/UploadFolder/" + PlayListTXT + "-" + PlayListFileNumber.ToString() + ".txt"));
                            myPlayListTXTWrite = new StreamWriter(myPlayListTXTFile);
                        }

                        //if (FCTYPE == "G")
                        //    ReString = ReString + "hard" + "\t";
                        //else if (FNLIVE=="1")
                        //    ReString = ReString + "manual" + "\t";
                        //else
                        //    ReString = ReString + "auto" + "\t";
                        
                        string TempPlayType="";
                        if (PlayListRowNumber == 2)
                             TempPlayType="hard";
                        else  if (ISLive == true)  //上一筆資料為Live
                            TempPlayType="manual";
                        else  if (TemArrPromoList.FCTYPE == "G") 
                             TempPlayType="hard";
                        else
                             TempPlayType="auto";
                        myPlayListTXTWrite.WriteLine(WriteTXTProgList(VideoID, PlayTime, Dur, SOM, TemArrPromoList.FCTYPE, TempPlayType, TemArrPromoList.FSNAME, TemArrPromoList.FNLIVE, TemArrPromoList.FSSIGNAL));
                        //if (ISLive == true) 
                        //{
                        //    if (TemArrPromoList.FCTYPE == "G")

                        //    else
                        //           myPlayListTXTWrite.WriteLine(WriteTXTProgList(VideoID, PlayTime, Dur, SOM, TemArrPromoList.FCTYPE, "hard", TemArrPromoList.FSNAME));
                        //}
                        //else
                        //{
                        //    if (TemArrPromoList.FCTYPE == "G") 
                        //}
                        //    myPlayListTXTWrite.WriteLine(WriteTXTProgList(VideoID, PlayTime, Dur, SOM, TemArrPromoList.FCTYPE, "manual", TemArrPromoList.FSPROG_NAME));
                        //else if (TemArrPromoList.FCTYPE == "G")
                        //    myPlayListTXTWrite.WriteLine(WriteTXTProgList(VideoID, PlayTime, Dur, SOM, TemArrPromoList.FCTYPE, "hard", TemArrPromoList.FSPROG_NAME));
                        //else
                        //    myPlayListTXTWrite.WriteLine(WriteTXTProgList(VideoID, PlayTime, Dur, SOM, TemArrPromoList.FCTYPE, "auto", TemArrPromoList.FSPROG_NAME));

                        if (TemArrPromoList.FNLIVE == "1")
                            ISLive = true;
                        else
                            ISLive = false;

                        //ProgOnlyRowNumber = ProgOnlyRowNumber + 1;
                        //if (ProgOnlyRowNumber > 196)
                        //{
                        //    myProgOnlyTXTWrite.Flush();
                        //    myProgOnlyTXTWrite.Close();
                        //    myProgOnlyTXTWrite.Dispose();
                        //    myProgOnlyTXTFile.Close();
                        //    myProgOnlyTXTFile.Dispose();
                        //    //myProgOnlyTXTFile = null;
                        //    ProgOnlyRowNumber = 1;
                        //    ProgOnlyFileNumber = ProgOnlyFileNumber + 1;
                        //    myProgOnlyTXTFile = File.Create(HttpContext.Current.Server.MapPath("~/UploadFolder/" + ProgOnlyTXT + "-" + ProgOnlyFileNumber.ToString() + ".txt"));
                        //    myProgOnlyTXTWrite = new StreamWriter(myProgOnlyTXTFile);
                        //}
                        //myProgOnlyTXTWrite.WriteLine(WriteTXTProgList(VideoID, PlayTime, Dur, SOM, TemArrPromoList.FCTYPE, "auto", TemArrPromoList.FSPROG_NAME));
                    }

                    #endregion

                    WorkRow = WorkRow + 1;
                    //開始分析Memo的Louth Key
                    #region 開始分析Memo的Louth Key
                    if (TemArrPromoList.FSMEMO != "")
                    {
                        string[] LouthKeys = TemArrPromoList.FSMEMO.Split(new Char[] { '+' });

                        foreach (string LouthKey in LouthKeys)  //每一個+ 號表示一個Key
                        {
                            if (LouthKey.Trim() != "")
                            {
                                striType = "0";
                                EventCOntrol = "";
                                QF1 = "0";
                                QF2 = "0";
                                QF3 = "0";
                                QF4 = "0";
                                EF1 = "0";
                                EF2 = "0";
                                EF3 = "0";
                                VideoID = TemArrPromoList.FSVIDEO_ID; //VideoID必須放為sKeyer Or sTRANKEY
                                QF2 = "1";
                                // 時間 ,重播,直播  為 sTRANKEY 其他為 sKEYER

                                //VideoID = "sKEYER";
                                string[] LouthKeyType = LouthKey.Split(new Char[] { '#' }); //將LouthKey 依照#分開,如果有 #代表有輸入時間 Ex:中/英#00:02:00,10,30
                                if (LouthKeyType[0] != "") //如果有設定LouthKey Ex:中/英#00:02:00,10,30
                                {
                                    #region 查詢Louth的資料 並將其加入 NewLouth
                                    LouthTableData NewLouth = new LouthTableData();

                                    if (LouthKeyType[0].IndexOf("片名") == 0)
                                    {
                                        //NewLouth = GetLouthData("片名-" + TemArrPromoList.FSNAME, TempChannelID);
                                        NewLouth = GetLouthData(LouthKeyType[0], TempChannelID);
                                        if (LouthKeyType[0].IndexOf("(整段)") > -1)
                                            NewLouth = GetLouthData(LouthKeyType[0].Substring(3, (LouthKeyType[0].Length) - 7), TempChannelID);
                                        else if (LouthKeyType[0].IndexOf("(全程避片頭)") > -1)
                                            NewLouth = GetLouthData(LouthKeyType[0].Substring(3, (LouthKeyType[0].Length) - 10), TempChannelID);
                                        else
                                            NewLouth = GetLouthData(LouthKeyType[0].Substring(3, (LouthKeyType[0].Length) - 3), TempChannelID);
                                    }
                                    else
                                        NewLouth = GetLouthData(LouthKeyType[0], TempChannelID); //取得Louth Table的資料
                                    if (NewLouth.FSNO != "")
                                    {
                                        QF4 = NewLouth.FSLayel;
                                        QF1 = NewLouth.FSNO.Substring(1, NewLouth.FSNO.Length - 1);

                                        if (LouthKeyType[0].ToUpper().IndexOf("LOGO") == 0)  //LOGO才要 變QUAL:EH
                                        {
                                            EF2 = "1";  //EH Hold
                                        }
                                        if (LouthKeyType[0].IndexOf("時間") == 0 || LouthKeyType[0].IndexOf("重播") == 0 || LouthKeyType[0].IndexOf("直播") == 0)
                                        {
                                            VideoID = "sTRANKEY";
                                            SOM = "";
                                            Dur = "";
                                            PlayTime = "";

                                        }
                                        else
                                        {
                                            VideoID = "sKEYER";
                                            SOM = "";
                                            PlayTime = TemArrPromoList.FSPLAY_TIME;
                                            Dur = TemArrPromoList.FSDURATION;
                                        }
                                        Title = NewLouth.FSNAME;
                                    #endregion
                                        bool NeedLoop = false;    //是否要做循環
                                        TimeStatus RTimeStatus = new TimeStatus();
                                        SEndPointStatus REndPointStatus = new SEndPointStatus();
                                        //設定播出規則,沒有的話則採用預設規則  並將資料加入RTimeStatus 只有雙語 片名 立體聲 數位同步播出有 值
                                        //接著跟繼續收看
                                        #region 設定播出規則,沒有的話則採用預設規則  並將資料加入RTimeStatus
                                        if ((LouthKeyType[0].IndexOf("/") > -1) || (LouthKeyType[0].IndexOf("片名") == 0) || (LouthKeyType[0].IndexOf("立體聲") == 0) || (LouthKeyType[0].IndexOf("數位") == 0) || (LouthKeyType[0].IndexOf("口述影像版") > -1)) //代表為片名 //有斜線代表為雙語
                                        {
                                            if (LouthKey.IndexOf("#") > -1)  //#00:02:00,10,30 //表示有設定起始時間,間隔,長度
                                            {
                                                RTimeStatus = Trans(LouthKeyType[1]);
                                                if (RTimeStatus.SStartTime.Length == 8)
                                                    RTimeStatus.SStartTime = RTimeStatus.SStartTime + ":00";
                                                if (RTimeStatus.SStartTime == "") //如為空值則採用預設值
                                                    RTimeStatus.SStartTime = "00:01:00:00";
                                                if (RTimeStatus.SInteval == "") //如為空值則採用預設值
                                                    RTimeStatus.SInteval = "10";
                                                if (RTimeStatus.SDur == "") //如為空值則採用預設值
                                                    RTimeStatus.SDur = "30";
                                                NeedLoop = true;
                                            }
                                            else
                                            {
                                                if (LouthKeyType[0].IndexOf("/") > -1) //雙語
                                                {
                                                    RTimeStatus.SStartTime = "00:00:20:00";  //1分開始上30秒,間隔10分鐘
                                                    RTimeStatus.SInteval = "10";
                                                    RTimeStatus.SDur = "90";
                                                }
                                                else if (LouthKeyType[0].IndexOf("片名") == 0)  //片名
                                                {
                                                    RTimeStatus.SStartTime = "00:02:00:00";  //2分開始上8分鐘秒,間隔10分鐘
                                                    RTimeStatus.SInteval = "10";
                                                    RTimeStatus.SDur = "480";
                                                }
                                                else if (LouthKeyType[0].IndexOf("立體聲") == 0)  //立體聲
                                                {
                                                    RTimeStatus.SStartTime = "00:02:00:00";  //2分開始上30秒,間隔10分鐘
                                                    RTimeStatus.SInteval = "10";
                                                    RTimeStatus.SDur = "60";
                                                }
                                                else if (LouthKeyType[0].IndexOf("數位") == 0)  //數位
                                                {
                                                    RTimeStatus.SStartTime = "00:00:05:00";  //5秒開始上1分鐘,間隔30分鐘
                                                    RTimeStatus.SInteval = "30";
                                                    RTimeStatus.SDur = "60";
                                                }
                                                else if (LouthKeyType[0].IndexOf("口述影像版") > -1)  //口述影像版
                                                {
                                                    RTimeStatus.SStartTime = "00:00:00:00";  //5秒開始上1分鐘,間隔30分鐘
                                                    RTimeStatus.SInteval = "30";
                                                    RTimeStatus.SDur = "60";
                                                }
                                                else
                                                {
                                                    RTimeStatus.SStartTime = "00:01:00:00";
                                                    RTimeStatus.SInteval = "10";
                                                    RTimeStatus.SDur = "30";
                                                }
                                                RTimeStatus.SSec = "0";
                                                NeedLoop = true;
                                            }
                                        }
                                        #endregion

                                        #region 接著,下週,明天,下次 將資料寫入REndPointInsert 表示最後幾秒要或不要
                                        if ((LouthKeyType[0].IndexOf("接著") > -1) || (LouthKeyType[0].IndexOf("下週") > -1) || (LouthKeyType[0].IndexOf("明天") > -1) || (LouthKeyType[0].IndexOf("下次") > -1))
                                        {
                                            striType = "131";
                                            //只要有最後幾秒要播出和播出長度
                                            string LastTimeInsert = "00:00:00:00"; //最後多少時間要插入Key
                                            if (LouthKey.IndexOf("#") > -1)  //#00:02:00,10,30 //表示有設定最後幾秒開始與長度
                                            {
                                                REndPointStatus = Trans1(LouthKeyType[1]);
                                                if (REndPointStatus.SEndPoint == "") //如為空值則採用預設值
                                                {
                                                    if (LouthKeyType[0].IndexOf("接著") == 0)
                                                    {
                                                        REndPointStatus.SEndPoint = "90";
                                                        REndPointStatus.SDur = "20";
                                                    }
                                                    else if ((LouthKeyType[0].IndexOf("下週") == 0) || (LouthKeyType[0].IndexOf("明天") == 0) || (LouthKeyType[0].IndexOf("下次") == 0))
                                                    {
                                                        REndPointStatus.SEndPoint = "40";
                                                        REndPointStatus.SDur = "20";
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (LouthKeyType[0].IndexOf("接著") == 0)
                                                {
                                                    REndPointStatus.SEndPoint = "90";
                                                    REndPointStatus.SDur = "20";
                                                }
                                                else if ((LouthKeyType[0].IndexOf("下週") == 0) || (LouthKeyType[0].IndexOf("明天") == 0) || (LouthKeyType[0].IndexOf("下次") == 0))
                                                {
                                                    REndPointStatus.SEndPoint = "40";
                                                    REndPointStatus.SDur = "20";
                                                }
                                            }

                                            //int IStartTimeFrame = 0;
                                            //IStartTimeFrame = MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - int.Parse(REndPointStatus.SEndPoint) * 30;
                                            //if (IStartTimeFrame > 0)
                                            //    PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(IStartTimeFrame);
                                            //else
                                            //    PlayTime = "00:00:00;00";

                                            //string Dur1;
                                            //長度為起始時間加五秒及最後幾分鐘不加,所以為長度減掉最後幾分鐘再減五秒

                                            //Dur1 = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(REndPointStatus.SDur));
                                            //myWriter.Write(WriteLouthByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur1, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3));

                                        }
                                        #endregion

                                        //開始將資料寫入LouthList

                                        #region 片名, 雙語 ,立體聲,數位同步播出 除了片名整段外都是有循環
                                        if ((LouthKeyType[0].IndexOf("/") > -1) || (LouthKeyType[0].IndexOf("片名") == 0) || (LouthKeyType[0].IndexOf("立體聲") == 0) || (LouthKeyType[0].IndexOf("數位") == 0) || (LouthKeyType[0].IndexOf("口述影像版") > -1)) //代表為片名 //有斜線代表為雙語
                                        {
                                            string LastTimeNoInsert = "00:02:00:00"; //最後多少時間不要插入Key

                                            //抓取資料完成開始加入LouthKey
                                            if ((LouthKeyType[0].IndexOf("整段") > -1) || (LouthKeyType[0].IndexOf("(全程避片頭)") > -1) || (LouthKeyType[0].IndexOf("口述影像版") > -1)) //整段
                                            {
                                                if (LouthKeyType[0].IndexOf("整段") > -1)
                                                {
                                                    VideoID = "sTRANKEY";
                                                    striType = "132";
                                                }
                                                else
                                                {
                                                    striType = "131";
                                                    VideoID = "sKEYER";
                                                }
                                                PlayTime = "";
                                                Dur = "";
                                                LastTimeNoInsert = "00:00:00:00";
                                                NeedLoop = false;
                                            }
                                            else //如果不為整段
                                            {
                                                NeedLoop = true;
                                                striType = "131";
                                                VideoID = "sKEYER";
                                                
                                                if (LouthKey.IndexOf("#") > -1)  //#00:02:00,10,30 //表示有設定起始時間,間隔,長度
                                                { }
                                                else
                                                {
                                                    if ((TemArrPromoList.FSCHANNEL_ID == "01") || (TemArrPromoList.FSCHANNEL_ID == "02") || (TemArrPromoList.FSCHANNEL_ID == "51"))
                                                        LastTimeNoInsert = "00:02:00:00";
                                                    else
                                                        LastTimeNoInsert = "00:00:30:00";
                                                }
                                                
                                            }

                                            EventCOntrol = "";

                                            #region 加入Louth Ley
                                            if (NeedLoop == true) //加入多筆louth key Loop
                                            {
                                                string ThisPlayTime = RTimeStatus.SStartTime;
                                                if (bLast == false) //不是最後一段只要檢查Key起始時間+長度不要大於節目時長即可
                                                {
                                                    while (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(ThisPlayTime) + int.Parse(RTimeStatus.SDur) * 30 < MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION))
                                                    {
                                                        PlayTime = ThisPlayTime;
                                                        //ThisPlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(ThisPlayTime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe("00:" + String.Format("{0:00}", RTimeStatus.SInteval) + ":00:00") + int.Parse(RTimeStatus.SSec) * 30);
                                                        ThisPlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(ThisPlayTime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe("00:" + String.Format("{0:00}", int.Parse(RTimeStatus.SInteval)) + ":00:00"));
                                                        string Dur1;
                                                        Dur1 = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(RTimeStatus.SDur));
                                                        if (TempChannelType != "002")
                                                        {
                                                            myWriter.Write(WritKeyByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur1, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3, TempChannelType,"SD"));
                                                        }
                                                        else  //當HD頻道與Layer=4時要多送一組結束訊號
                                                        {
                                                            //HD頻道要寫兩種LST檔
                                                            myWriter1.Write(WritKeyByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur1, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3, "001", "HD"));
                                                            //HD格式的VideoID為 L1 100
                                                            if (LouthKeyType[0].ToUpper().IndexOf("LOGO") == 0)
                                                            {
                                                                PlayTime = "";
                                                                Dur1 = "";
                                                            }
                                                            if (QF4 == "3")
                                                            {
                                                                //須給上下架時間,長度都為兩秒,放在第四層的應當都為整段的Key,所以起始時間為0.
                                                                if (QF1.Substring(0, 1) == "0")
                                                                    VideoID = "ST " + QF1.Substring(1, 2);
                                                                else
                                                                    VideoID = "ST " + QF1;
                                                                if (PlayTime == "")
                                                                    PlayTime = "00:00:00:00";
                                                                if (Dur1 == "")
                                                                    Dur1 = TemArrPromoList.FSDURATION;
                                                                if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) < 15 * 30)
                                                                    PlayTime = "00:00:15:00";
                                                                string RealDur = Dur1;
                                                                Dur1 = "00:00:02:00";
                                                                myWriter.Write(WritKeyByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur1, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3, TempChannelType, "E"));
                                                                VideoID = "ST 5" + QF1;
                                                                PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(RealDur));
                                                                if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) + 15 * 30 >= MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION))
                                                                    PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - 15 * 30);
                                                                Dur1 = "00:00:02:00";
                                                                myWriter.Write(WritKeyByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur1, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3, TempChannelType, "E"));
                                                            }
                                                            else
                                                            {
                                                                if (QF4 == "7")
                                                                    VideoID = "L1" + " " + QF1;
                                                                if (QF4 == "6")
                                                                    VideoID = "L2" + " " + QF1;
                                                                if (QF4 == "4")
                                                                    VideoID = "L3" + " " + QF1;
                                                                if (QF4 == "3")
                                                                    VideoID = "L4" + " " + QF1;
                                                                //VideoID = "L" + QF4 + " " + QF1;
                                                                myWriter.Write(WritKeyByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur1, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3, TempChannelType, "E"));
                                                            }

                                                        }

                                                    }
                                                }
                                                else //最後一段要檢查Key起始時間+3分鐘不要大於節目時長即可
                                                {
                                                    while (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(ThisPlayTime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(LastTimeNoInsert) + int.Parse(RTimeStatus.SDur) * 30 < MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION))
                                                    {
                                                        PlayTime = ThisPlayTime;
                                                        //ThisPlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(ThisPlayTime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe("00:" + String.Format("{0:00}", RTimeStatus.SInteval) + ":00:00") + int.Parse(RTimeStatus.SSec) * 30);
                                                        ThisPlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(ThisPlayTime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe("00:" + String.Format("{0:00}", int.Parse(RTimeStatus.SInteval)) + ":00:00"));
                                                        string Dur1;
                                                        Dur1 = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(RTimeStatus.SDur));
                                                        if (TempChannelType != "002")
                                                            myWriter.Write(WritKeyByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur1, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3, TempChannelType, "SD"));
                                                        else  //當HD頻道與Layer=4時要多送一組結束訊號
                                                        {
                                                            //HD頻道要寫兩種LST檔
                                                            myWriter1.Write(WritKeyByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur1, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3, "001", "HD"));
                                                          
                                                            //HD格式的VideoID為 L1 100
                                                            if (LouthKeyType[0].ToUpper().IndexOf("LOGO") == 0)
                                                            {
                                                                PlayTime = "";
                                                                Dur1 = "";
                                                            }
                                                            if (QF4 == "3")
                                                            {

                                                                if (QF1.Substring(0, 1) == "0")
                                                                    VideoID = "ST " + QF1.Substring(1, 2);
                                                                else
                                                                    VideoID = "ST " + QF1;
                                                                if (PlayTime == "")
                                                                    PlayTime = "00:00:00:00";
                                                                if (Dur1 == "")
                                                                    Dur1 = TemArrPromoList.FSDURATION;
                                                                if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) < 15 * 30)
                                                                    PlayTime = "00:00:15:00";
                                                                string RealDur = Dur1;
                                                                Dur1 = "00:00:02:00";
                                                                myWriter.Write(WritKeyByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur1, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3, TempChannelType, "E"));
                                                                VideoID = "ST 5" + QF1;
                                                                PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(RealDur));
                                                                if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) + 15 * 30 + +MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(LastTimeNoInsert) >= MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION))
                                                                    PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - 15 * 30 - +MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(LastTimeNoInsert));
                                                        
                                                                Dur1 = "00:00:02:00";

                                                                myWriter.Write(WritKeyByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur1, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3, TempChannelType, "E"));
                                                            }
                                                            else
                                                            {
                                                                if (QF4 == "7")
                                                                    VideoID = "L1" + " " + QF1;
                                                                if (QF4 == "6")
                                                                    VideoID = "L2" + " " + QF1;
                                                                if (QF4 == "4")
                                                                    VideoID = "L3" + " " + QF1;
                                                                if (QF4 == "3")
                                                                    VideoID = "L4" + " " + QF1;
                                                                //VideoID = "L" + QF4 + " " + QF1;
                                                                myWriter.Write(WritKeyByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur1, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3, TempChannelType, "E"));
                                                            }

                                                        }
                                                    }
                                                }
                                            }
                                            else //if (NeedLoop == false) 不重複
                                            {
                                                if (TemArrPromoList.FSDURATION != "00:00:00:00")
                                                {
                                                    string Dur1;
                                                    if (LouthKeyType[0].IndexOf("(全程避片頭)") > -1) //從20秒開始到結束,最末段前兩分鐘不秀
                                                    {
                                                        PlayTime = "00:00:20:00";
                                                        if (bLast == false) //不是最後一段
                                                        {
                                                            if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) > 20 * 30)
                                                                Dur1 = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - 20 * 30);
                                                            else
                                                                Dur1 = "00:00:00:00";
                                                        }
                                                        else
                                                        {
                                                            if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) > 140 * 30)
                                                                Dur1 = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - 140 * 30);
                                                            else
                                                                Dur1 = "00:00:00:00";
                                                        }
                                                    }
                                                    else if (LouthKeyType[0].IndexOf("口述影像版") > -1)
                                                    {
                                                        VideoID = "sKEYER";
                                                        PlayTime = "00:00:00:00";
                                                        if (bLast == false) //不是最後一段
                                                        {
                                                            VideoID = "sTRANKEY";
                                                            striType = "132";
                                                            Dur1 = "";
                                                            PlayTime = "";
                                                            //Dur1 = TemArrPromoList.FSDURATION;
                                                        }
                                                        else
                                                        {
                                                            if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) > 120 * 30)
                                                                Dur1 = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - 120 * 30);
                                                            else
                                                                Dur1 = "00:00:00:00";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        PlayTime = RTimeStatus.SStartTime;
                                                        Dur1 = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(RTimeStatus.SDur));
                                                    }
                                                    if (VideoID == "sTRANKEY")
                                                    {
                                                        PlayTime = "";
                                                        Dur1 = "";
                                                    }
                                                    if (TempChannelType != "002")
                                                        myWriter.Write(WritKeyByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur1, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3, TempChannelType, "SD"));
                                                    else  //當HD頻道與Layer=4時要多送一組結束訊號
                                                    {
                                                        //HD頻道要寫兩種LST檔
                                                        myWriter1.Write(WritKeyByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur1, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3, "001", "HD"));
                                                          
                                                        //HD格式的VideoID為 L1 100
                                                        if (LouthKeyType[0].ToUpper().IndexOf("LOGO") == 0)
                                                        {
                                                            PlayTime = "";
                                                            Dur1 = "";
                                                        }
                                                        if (QF4 == "3")
                                                        {

                                                            if (QF1.Substring(0, 1) == "0")
                                                                VideoID = "ST " + QF1.Substring(1, 2);
                                                            else
                                                                VideoID = "ST " + QF1;
                                                            if (PlayTime == "")
                                                                PlayTime = "00:00:00:00";
                                                            if (Dur1 == "")
                                                                Dur1 = TemArrPromoList.FSDURATION;
                                                            if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) < 15 * 30)
                                                                PlayTime = "00:00:15:00";
                                                            string RealDur = Dur1;
                                                            Dur1 = "00:00:02:00";
                                                            myWriter.Write(WritKeyByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur1, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3, TempChannelType, "E"));
                                                            VideoID = "ST 5" + QF1;
                                                            PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(RealDur));
                                                            if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) + 15 * 30 >= MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION))
                                                                PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - 15 * 30);
                                                            Dur1 = "00:00:02:00";
                                                            myWriter.Write(WritKeyByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur1, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3, TempChannelType, "E"));
                                                        }
                                                        else
                                                        {
                                                            if (QF4 == "7")
                                                                VideoID = "L1" + " " + QF1;
                                                            if (QF4 == "6")
                                                                VideoID = "L2" + " " + QF1;
                                                            if (QF4 == "4")
                                                                VideoID = "L3" + " " + QF1;
                                                            if (QF4 == "3")
                                                                VideoID = "L4" + " " + QF1;
                                                            //VideoID = "L" + QF4 + " " + QF1;
                                                            myWriter.Write(WritKeyByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur1, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3, TempChannelType, "E"));
                                                        }
                                                    }
                                                }
                                            } //if (NeedLoop == false) 不重複
                                            #endregion

                                        }
                                        #endregion

                                        #region 重播, 直播 ,時間,Live,錄影轉播,Logo,都是整段
                                        if ((LouthKeyType[0].IndexOf("重播") == 0) || (LouthKeyType[0].IndexOf("直播") == 0) || (LouthKeyType[0].IndexOf("時間") == 0) || (LouthKeyType[0].ToUpper().IndexOf("LIVE") == 0) || (LouthKeyType[0].ToUpper().IndexOf("LOGO") == 0) || (LouthKeyType[0].IndexOf("錄影轉播") == 0)) //代表為片名 //有斜線代表為雙語
                                        {
                                            VideoID = "sTRANKEY";
                                            striType = "132";
                                            PlayTime = "";
                                            Dur = "";
                                            //if (LouthKeyType[0].IndexOf("時間") > -1)
                                            //{
                                            //    EF2 = "0";  //在節目時如果為1 則會顯示LOSE
                                            //    EF3 = "2";

                                            //    //VideoID = "sKEYER";
                                            //    //striType = "131";
                                            //    //PlayTime = "00:01:00:00";
                                            //    //Dur = "00:00:30:00";
                                            //    //SOM = "00:00:00:00";
                                            //    QF2 = "0"; 
                                            //    QF3 = "16";
                                            //}
                                            if (LouthKeyType[0].ToUpper().IndexOf("LOGO") == 0)
                                            {
                                                EF2 = "0";  //在節目時如果為1 則會顯示LOSE
                                                EF3 = "2";
                                                QF2 = "1"; ////EH Hold
                                                QF3 = "16";
                                                striType = "131";
                                                PlayTime = "00:00:00:00";
                                                Dur = "00:00:10:00";
                                                VideoID = "sKEYER";
                                            }
                                            //PlayTime = RTimeStatus.SStartTime;
                                            string Dur1;
                                            //Dur1 = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(RTimeStatus.SDur));
                                            Dur1 = Dur;
                                            if (TempChannelType != "002")
                                                myWriter.Write(WritKeyByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur1, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3, TempChannelType, "SD"));
                                            else  //當HD頻道與Layer=4時要多送一組結束訊號
                                            {
                                                //HD頻道要寫兩種LST檔
                                                myWriter1.Write(WritKeyByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur1, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3, "001", "HD"));
                                                          
                                                //HD格式的VideoID為 L1 100

                                                if (LouthKeyType[0].ToUpper().IndexOf("LOGO") == 0)
                                                {
                                                    PlayTime = "";
                                                    Dur1 = "";
                                                }
                                                if (QF4 == "3")
                                                {

                                                    if (QF1.Substring(0, 1) == "0")
                                                        VideoID = "ST " + QF1.Substring(1, 2);
                                                    else
                                                        VideoID = "ST " + QF1;
                                                    if (PlayTime == "")
                                                        PlayTime = "00:00:00:00";
                                                    if (Dur1 == "")
                                                        Dur1 = TemArrPromoList.FSDURATION;
                                                    if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) < 15 * 30)
                                                        PlayTime = "00:00:15:00";

                                                    if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime)+ 2*30 < MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(Dur1) )
                                                    {
                                                        string RealDur = Dur1;
                                                        Dur1 = "00:00:02:00";
                                                        myWriter.Write(WritKeyByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur1, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3, TempChannelType, "E"));
                                                        VideoID = "ST 5" + QF1;
                                                        PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(RealDur));
                                                        if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) + 15 * 30 >= MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION))
                                                            PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - 15 * 30);
                                                        
                                                        Dur1 = "00:00:02:00";
                                                        myWriter.Write(WritKeyByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur1, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3, TempChannelType, "E"));
                                                    }
                                                }
                                                else
                                                {
                                                    if (QF4 == "7")
                                                        VideoID = "L1" + " " + QF1;
                                                    if (QF4 == "6")
                                                        VideoID = "L2" + " " + QF1;
                                                    if (QF4 == "4")
                                                        VideoID = "L3" + " " + QF1;
                                                    if (QF4 == "3")
                                                        VideoID = "L4" + " " + QF1;
                                                    //VideoID = "L" + QF4 + " " + QF1;
                                                    myWriter.Write(WritKeyByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur1, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3, TempChannelType, "E"));
                                                }

                                            }

                                        }
                                        #endregion

                                        #region 接著,下週,明天,下次
                                        if ((LouthKeyType[0].IndexOf("接著") == 0) || (LouthKeyType[0].IndexOf("下週") == 0) || (LouthKeyType[0].IndexOf("明天") == 0) || (LouthKeyType[0].IndexOf("下次") == 0))
                                        {
                                            striType = "131";
                                            //只要有最後幾秒要播出和播出長度
                                            string LastTimeInsert = "00:00:00:00"; //最後多少時間要插入Key
                                            EventCOntrol = "";
                                            Title = NewLouth.FSNAME;
                                            VideoID = "sKEYER";

                                            int IStartTimeFrame = 0;
                                            if ( MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) < int.Parse(REndPointStatus.SEndPoint) * 30)
                                                IStartTimeFrame = 0;
                                            else
                                                IStartTimeFrame = MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - int.Parse(REndPointStatus.SEndPoint) * 30;
                                            if (IStartTimeFrame > 0)
                                                PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(IStartTimeFrame);
                                            else
                                                PlayTime = "00:00:00;00";

                                            string Dur1;
                                            //長度為起始時間加五秒及最後幾分鐘不加,所以為長度減掉最後幾分鐘再減五秒

                                            Dur1 = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(REndPointStatus.SDur));
                                            if (TempChannelType != "002")
                                                myWriter.Write(WritKeyByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur1, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3, TempChannelType, "SD"));
                                            else  //當HD頻道與Layer=4時要多送一組結束訊號
                                            {
                                                //HD頻道要寫兩種LST檔
                                                myWriter1.Write(WritKeyByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur1, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3, "001", "HD"));
                                                          
                                                //HD格式的VideoID為 L1 100
                                                if (LouthKeyType[0].ToUpper().IndexOf("LOGO") == 0)
                                                {
                                                    PlayTime = "";
                                                    Dur1 = "";
                                                }
                                                if (QF4 == "3")
                                                {

                                                    if (QF1.Substring(0, 1) == "0")
                                                        VideoID = "ST " + QF1.Substring(1, 2);
                                                    else
                                                        VideoID = "ST " + QF1;
                                                    if (PlayTime == "")
                                                        PlayTime = "00:00:00:00";
                                                    if (Dur1 == "")
                                                        Dur1 = TemArrPromoList.FSDURATION;
                                                    if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) < 15 * 30)
                                                        PlayTime = "00:00:15:00";
                                                    string RealDur = Dur1;
                                                    Dur1 = "00:00:02:00";
                                                    myWriter.Write(WritKeyByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur1, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3, TempChannelType, "E"));
                                                    VideoID = "ST 5" + QF1;
                                                    PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(RealDur));
                                                    if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) + 15 * 30 >= MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION))
                                                    {
                                                        if ( MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) > 15 * 30)
                                                           PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - 15 * 30);
                                                    }
                                                        
                                                        
                                                    Dur1 = "00:00:02:00";
                                                    myWriter.Write(WritKeyByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur1, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3, TempChannelType, "E"));
                                                }
                                                else
                                                {
                                                    if (QF4 == "7")
                                                        VideoID = "L1" + " " + QF1;
                                                    if (QF4 == "6")
                                                        VideoID = "L2" + " " + QF1;
                                                    if (QF4 == "4")
                                                        VideoID = "L3" + " " + QF1;
                                                    if (QF4 == "3")
                                                        VideoID = "L4" + " " + QF1;
                                                    //VideoID = "L" + QF4 + " " + QF1;
                                                    myWriter.Write(WritKeyByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur1, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3, TempChannelType, "E"));
                                                }
                                                //}
                                            }
                                        }  //if ((LouthKeyType[0].IndexOf(" 接著,下週,明天,下次") > -1)
                                        #endregion
                                    }
                                }
                            }
                        } //  foreach (string LouthKey in LouthKeys)                     
                    } //if (TemArrPromoList.FSMEMO != "")
                    #endregion
                    //} //if FCPLAY_Type="G"
                }
                myFile1.Close();
                myFile1.Dispose();
                myFile.Close();
                myFile.Dispose();

                myCombineFile.Close();
                myCombineFile.Dispose();

                //Create_First_Play_List("G" + lstFileName,TempDate,TempChannelID,"G");
                //Create_First_Play_List("P" + lstFileName, TempDate, TempChannelID, "P");
                //開始查詢首播清單並寫入檔案 FirstPlayListFileName

                //Create_First_Play_List(FirstPlayListFileName, TempDate, TempChannelID, "P");
                Create_First_Play_List(FirstPlayListFileName, TempDate, TempChannelID, "G", TempChannelType);

                //List<string> names = new List<string>();

                //names.Add(HttpContext.Current.Server.MapPath("~/UploadFolder/" + lstFileName));
                //names.Add(HttpContext.Current.Server.MapPath("~/UploadFolder/" + "G" + lstFileName));
                //names.Add(HttpContext.Current.Server.MapPath("~/UploadFolder/" +  "P" + lstFileName));

                //if (File.Exists(HttpContext.Current.Server.MapPath("~/UploadFolder/" + lstFileName + ".Zip")) == true)
                //    File.Delete(HttpContext.Current.Server.MapPath("~/UploadFolder/" + lstFileName + ".Zip"));
                //FileStream myFile1 = File.Create(HttpContext.Current.Server.MapPath("~/UploadFolder/" + lstFileName +".Zip"));
                //BinaryWriter myWriter1 = new BinaryWriter(myFile1);
                //myFile1.Close();
                //myFile1.Dispose();


                //myPlayListTXTWrite.WriteLine("csharp.net-informations.com");


                myPlayListTXTWrite.Flush();
                myPlayListTXTWrite.Close();
                myPlayListTXTWrite.Dispose();
                myPlayListTXTFile.Close();
                myPlayListTXTFile.Dispose();
                myPlayListTXTFile = null;



                //myProgOnlyTXTWrite.WriteLine("csharp.net-informations.com");

                myProgOnlyTXTWrite.Flush();
                myProgOnlyTXTWrite.Close();
                myProgOnlyTXTWrite.Dispose();
                myProgOnlyTXTFile.Close();
                myProgOnlyTXTFile.Dispose();
                myProgOnlyTXTFile = null;

                //20151116 by Mike
                //產生FilingList清單--------------------------------


                #region 查詢資料庫取得Filing清單(每日播出檔案減昨日播出)放置於FilingList

                    String sqlConnStr = "";
                    sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
                    SqlConnection connection = new SqlConnection(sqlConnStr);
                    StringBuilder sb = new StringBuilder();

                    SqlDataAdapter da = new SqlDataAdapter("SP_Q_FILING_LIST", connection);
                    DataTable dt = new DataTable();
                    SqlParameter para = new SqlParameter("@FDDATE", TempDate);
                    da.SelectCommand.Parameters.Add(para);
                    SqlParameter para1 = new SqlParameter("@FSCHANNEL_ID", TempChannelID);
                    da.SelectCommand.Parameters.Add(para1);

                    da.SelectCommand.CommandType = CommandType.StoredProcedure;

                    if (connection.State == System.Data.ConnectionState.Closed)
                    {
                        connection.Open();
                    }
                    da.Fill(dt);
                    connection.Close();

                    List<FilingTableData> FilingList = new List<FilingTableData>();



                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            FilingTableData FilingData = new FilingTableData();
                            if (Convert.IsDBNull(dt.Rows[i]["FSPROG_ID"]))
                                FilingData.FSPROG_ID = "";
                            else
                                FilingData.FSPROG_ID = dt.Rows[i]["FSPROG_ID"].ToString();

                            if (Convert.IsDBNull(dt.Rows[i]["FNEPISODE"]))
                                FilingData.FNEPISODE = "";
                            else
                                FilingData.FNEPISODE = dt.Rows[i]["FNEPISODE"].ToString();


                            if (Convert.IsDBNull(dt.Rows[i]["FSFILE_NO"]))
                                FilingData.FSFILE_NO = "";
                            else
                                FilingData.FSFILE_NO = dt.Rows[i]["FSFILE_NO"].ToString();

                            if (Convert.IsDBNull(dt.Rows[i]["fsprog_name"]))
                                FilingData.fsprog_name = "";
                            else
                                FilingData.fsprog_name = dt.Rows[i]["fsprog_name"].ToString();
                            if (Convert.IsDBNull(dt.Rows[i]["FSVIDEO_ID"]))
                                FilingData.FSVIDEO_ID = "";
                            else
                                FilingData.FSVIDEO_ID = dt.Rows[i]["FSVIDEO_ID"].ToString();
                            if (Convert.IsDBNull(dt.Rows[i]["FNSEG_ID"]))
                                FilingData.FNSEG_ID = "";
                            else
                                FilingData.FNSEG_ID = dt.Rows[i]["FNSEG_ID"].ToString();
                            if (Convert.IsDBNull(dt.Rows[i]["FSBEG_TIMECODE"]))
                                FilingData.FSBEG_TIMECODE = "";
                            else
                                FilingData.FSBEG_TIMECODE = dt.Rows[i]["FSBEG_TIMECODE"].ToString();
                            if (Convert.IsDBNull(dt.Rows[i]["FSEND_TIMECODE"]))
                                FilingData.FSEND_TIMECODE = "";
                            else
                                FilingData.FSEND_TIMECODE = dt.Rows[i]["FSEND_TIMECODE"].ToString();

                            FilingData.FSDURATION = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(FilingData.FSEND_TIMECODE) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(FilingData.FSBEG_TIMECODE));
                            FilingList.Add(FilingData);
                        }
                    }
                    else
                    {
                        //return "";
                    }
                #endregion
                #region 開啟建立檔案
                //開啟建立檔案

                string ChannelAliasName = TransChannelIDToAliasName(TempChannelID);

                string FilingListTXT = "OF" + ChannelAliasName + DateString + ".txt";
                string M_FilingListTXT = "OF" + ChannelAliasName + DateString + "_M" + ".txt";

                //if (File.Exists(HttpContext.Current.Server.MapPath("~/UploadForder/" + "1234.lst")) == true)
                //    File.Delete(HttpContext.Current.Server.MapPath("~/UploadForder/" + "1234.lst"));
                ////FileStream myFile = File.Open(@"C:\0Selfsys\1234.lst", FileMode.Open, FileAccess.ReadWrite);
                //FileStream myFile = File.Create(HttpContext.Current.Server.MapPath("~/UploadForder/" + "1234.lst"));
                if (File.Exists(HttpContext.Current.Server.MapPath("~/UploadFolder/" + FilingListTXT)) == true)
                    File.Delete(HttpContext.Current.Server.MapPath("~/UploadFolder/" + FilingListTXT));
                if (File.Exists(HttpContext.Current.Server.MapPath("~/UploadFolder/" + M_FilingListTXT)) == true)
                    File.Delete(HttpContext.Current.Server.MapPath("~/UploadFolder/" + M_FilingListTXT));                //FileStream myFile = File.Open(@"C:\0Selfsys\1234.lst", FileMode.Open, FileAccess.ReadWrite);

                FileStream myFilingListFile = File.Create(HttpContext.Current.Server.MapPath("~/UploadFolder/" + FilingListTXT));
                TextWriter myFilingListWriter = new StreamWriter(myFilingListFile);

                FileStream myM_FilingListFile = File.Create(HttpContext.Current.Server.MapPath("~/UploadFolder/" + M_FilingListTXT));
                TextWriter myM_FilingListWrite = new StreamWriter(myM_FilingListFile);
                #endregion

                #region 開始寫入FilingListTXT 節目清單

                foreach (FilingTableData TempFilingData in FilingList)
                {
                    myFilingListWriter.WriteLine(WriteTXTProgOnly(TempFilingData.FSVIDEO_ID.Substring(2, 6), "00:00:00;00", TempFilingData.FSDURATION, TempFilingData.FSBEG_TIMECODE, "G", "auto", TempFilingData.fsprog_name, "0", ""));
                }

                #endregion


                #region 開始寫入TXT播出運行表
                bool FirstDate = true;
                foreach (FilingTableData TempFilingData in FilingList)
                {
                    string EndBeginTimeCode = "";//段落結束時間減一分鐘
                    if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TempFilingData.FSEND_TIMECODE) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe("00:01:00;00") >= 0)
                        EndBeginTimeCode = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TempFilingData.FSEND_TIMECODE) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe("00:01:00;00"));
                    else
                        EndBeginTimeCode = "00:00:00;00";
                    if (FirstDate == true)
                    {
                        myM_FilingListWrite.WriteLine(WriteTXTProgOnly(TempFilingData.FSVIDEO_ID.Substring(2, 6), "00:00:00;00", "00:00:59;28", TempFilingData.FSBEG_TIMECODE, "G", "manual", TempFilingData.fsprog_name, "0", ""));
                        myM_FilingListWrite.WriteLine(WriteTXTProgOnly(TempFilingData.FSVIDEO_ID.Substring(2, 6), "00:00:00;00", "00:00:59;28", EndBeginTimeCode, "G", "auto", TempFilingData.fsprog_name, "0", ""));
                        FirstDate = false;
                    }
                    else
                    {
                        myM_FilingListWrite.WriteLine(WriteTXTProgOnly(TempFilingData.FSVIDEO_ID.Substring(2, 6), "00:00:00;00", "00:00:59;28", TempFilingData.FSBEG_TIMECODE, "G", "auto", TempFilingData.fsprog_name, "0", ""));
                        myM_FilingListWrite.WriteLine(WriteTXTProgOnly(TempFilingData.FSVIDEO_ID.Substring(2, 6), "00:00:00;00", "00:00:59;28", EndBeginTimeCode, "G", "auto", TempFilingData.fsprog_name, "0", ""));
                    }
                }
                #endregion


                myFilingListWriter.Flush();
                myFilingListWriter.Close();
                myFilingListWriter.Dispose();
                myFilingListFile.Close();
                myFilingListFile.Dispose();
                myFilingListFile = null;


                myM_FilingListWrite.Flush();
                myM_FilingListWrite.Close();
                myM_FilingListWrite.Dispose();
                myM_FilingListFile.Close();
                myM_FilingListFile.Dispose();
                myM_FilingListFile = null;

                   
                //---------------------------------------------------------------------------------------


                string ZipFIleName = "";
                ZipFIleName = FileChannelDate + ".Zip";




                #region 開始將檔案壓縮成Zip
                Crc32 crc = new Crc32();
                if (File.Exists(HttpContext.Current.Server.MapPath("~/UploadFolder/" + ZipFIleName)) == true)
                    File.Delete(HttpContext.Current.Server.MapPath("~/UploadFolder/" + ZipFIleName));
                ZipOutputStream s = new ZipOutputStream(File.Create(HttpContext.Current.Server.MapPath("~/UploadFolder/" + ZipFIleName)));

                s.SetLevel(9); // 0 - store only to 9 - means best compression

                AddZipFileClass BBB = new AddZipFileClass();
                BBB = AddZipFile(HttpContext.Current.Server.MapPath("~/UploadFolder/" + lstFileName), lstFileName);
                s.PutNextEntry(BBB.entry);
                s.Write(BBB.buffer, 0, BBB.buffer.Length);

                AddZipFileClass CCC = new AddZipFileClass();
                CCC = AddZipFile(HttpContext.Current.Server.MapPath("~/UploadFolder/" + CombinelstFileName), CombinelstFileName);
                s.PutNextEntry(CCC.entry);
                s.Write(CCC.buffer, 0, CCC.buffer.Length);

                AddZipFileClass DDD = new AddZipFileClass();
                DDD = AddZipFile(HttpContext.Current.Server.MapPath("~/UploadFolder/" + FirstPlayListFileName), FirstPlayListFileName);
                s.PutNextEntry(DDD.entry);
                s.Write(DDD.buffer, 0, DDD.buffer.Length);

                AddZipFileClass GGG = new AddZipFileClass();
                GGG = AddZipFile(HttpContext.Current.Server.MapPath("~/UploadFolder/" + FilingListTXT), FilingListTXT);
                s.PutNextEntry(GGG.entry);
                s.Write(GGG.buffer, 0, GGG.buffer.Length);

                AddZipFileClass FFF = new AddZipFileClass();
                FFF = AddZipFile(HttpContext.Current.Server.MapPath("~/UploadFolder/" + M_FilingListTXT), M_FilingListTXT);
                s.PutNextEntry(FFF.entry);
                s.Write(FFF.buffer, 0, FFF.buffer.Length);


                //if (TempChannelID == "11" || TempChannelID == "61")
                if (TempChannelType=="002")
                {

                    for (int i = 0; i <= PlayListFileNumber; i++)
                    {

                        AddZipFileClass EEE = new AddZipFileClass();
                        if (i == 0)
                            EEE = AddZipFile(HttpContext.Current.Server.MapPath("~/UploadFolder/" + PlayListTXT + ".txt"), PlayListTXT + ".txt");
                        else
                            EEE = AddZipFile(HttpContext.Current.Server.MapPath("~/UploadFolder/" + PlayListTXT + "-" + i.ToString() + ".txt"), PlayListTXT + "-" + i.ToString() + ".txt");
                        s.PutNextEntry(EEE.entry);
                        s.Write(EEE.buffer, 0, EEE.buffer.Length);
                    }
                    for (int i = 0; i <= ProgOnlyFileNumber; i++)
                    {
                        AddZipFileClass EEE = new AddZipFileClass();
                        if (i == 0)
                            EEE = AddZipFile(HttpContext.Current.Server.MapPath("~/UploadFolder/" + ProgOnlyTXT + ".txt"), ProgOnlyTXT + ".txt");
                        else
                            EEE = AddZipFile(HttpContext.Current.Server.MapPath("~/UploadFolder/" + ProgOnlyTXT + "-" + i.ToString() + ".txt"), ProgOnlyTXT + "-" + i.ToString() + ".txt");
                        s.PutNextEntry(EEE.entry);
                        s.Write(EEE.buffer, 0, EEE.buffer.Length);
                    }

                    AddZipFileClass HHH = new AddZipFileClass();
                    HHH = AddZipFile(HttpContext.Current.Server.MapPath("~/UploadFolder/" + lstFileName1), lstFileName1);
                    s.PutNextEntry(HHH.entry);
                    s.Write(HHH.buffer, 0, HHH.buffer.Length);

                }

                s.Finish();
                s.Close();

                #endregion

                string UploadPath;

                UploadPath = PTS_MAM3.Web.Properties.Settings.Default.UploadPath;

                //return UploadPath + lstFileName;
                return UploadPath + ZipFIleName;

                //return "http://" + HttpContext.Current.Request.Url.Host + "/UploadFolder/" + lstFileName;
                //return HttpContext.Current.Request.ApplicationPath;
                //return HttpContext.Current.Server.MapPath("~/UploadFolder/" + lstFileName);
                //return "http://localhost/PTS_MAM3.Web/UploadFolder/" + lstFileName;

            }
            catch (InvalidCastException e)
            {
                return "";
            }


        }



        public static string[] GetZipContents(System.IO.Stream zipStream)
        {


            List<string> names = new List<string>();
            BinaryReader reader = new BinaryReader(zipStream);
            while (reader.ReadUInt32() == 0x04034b50)
            {

                // Skip the portions of the header we don't care about
                reader.BaseStream.Seek(14, SeekOrigin.Current);
                uint compressedSize = reader.ReadUInt32();
                uint uncompressedSize = reader.ReadUInt32();
                int nameLength = reader.ReadUInt16();
                int extraLength = reader.ReadUInt16();
                byte[] nameBytes = reader.ReadBytes(nameLength);
                names.Add(Encoding.UTF8.GetString(nameBytes, 0, nameLength));
                reader.BaseStream.Seek(extraLength + compressedSize, SeekOrigin.Current);

            }
            // Move the stream back to the begining
            zipStream.Seek(0, SeekOrigin.Begin);
            return names.ToArray();

        }

        //轉換LST File
        [WebMethod()]
        public string TransLst11(List<ArrPromoList> InList)
        {

            //FileStream fs = new FileStream(@"c:\sw.txt", FileMode.Create, FileAccess.Write);
            //StreamWriter m_streamWriter = new StreamWriter(fs);
            //// Write to the file using StreamWriter class
            //m_streamWriter.BaseStream.Seek(0, SeekOrigin.End);
            //for (int i = 128; i < 255; i++)
            //{
            //    m_streamWriter.Write((char)i);
            //}


            //m_streamWriter.Flush();
            //m_streamWriter.Close();


            //return "ok";
            //FileSaveHex();
            //System.IO.FileStream fs = new FileStream();
            //System.IO.StreamWriter sw1 = new System.IO.StreamWriter(fs);

            // using(BinaryWriter binWriter =
            //    new BinaryWriter(File.Open(@"C:\BIn.txt", FileMode.Create)))
            //{
            //    binWriter.Write("");

            //}
            string TempStr = "";
            byte[] aaa = new byte[256];
            for (int i = 1; i <= 255; i++)
            {
                TempStr = TempStr + (char)i;
                aaa[i] = (byte)i; //hex.Substring(x, 2); //0x255;
            }
            TempStr = TempStr + "新聞";


            try
            {
                //開啟建立檔案
                FileStream myFile = File.Open(@"C:\0Selfsys\1234.txt", FileMode.Open, FileAccess.ReadWrite);
                BinaryWriter myWriter = new BinaryWriter(myFile);
                myWriter.Write(aaa);
                myWriter.Write(aaa);
                myWriter.Close();
                myFile.Close();

            }
            catch (InvalidCastException e)
            {

            }


            //BinWrite(@"C:\0Selfsys\1234.txt", aaa);

            MemoryStream TempMe = new MemoryStream();
            //FileStream FileS=new FileStream(@"c:\big5.txt",

            StreamWriter big5 = new StreamWriter(@"c:\big5.txt", false, Encoding.GetEncoding("big5"));

            for (int i = 1; i <= 255; i++)
            {
                big5.Write((char)i);
            }
            big5.Write("新聞");
            big5.Flush();
            big5.Close();

            StreamWriter UTF7 = new StreamWriter(@"c:\UTF7.txt", false, Encoding.UTF7);
            for (int i = 1; i <= 255; i++)
            {
                UTF7.Write((char)i);
            }
            UTF7.Write("新聞");
            UTF7.Flush();
            UTF7.Close();

            StreamWriter UTF8 = new StreamWriter(@"c:\UTF8.txt", false, Encoding.UTF8);
            for (int i = 1; i <= 255; i++)
            {
                UTF8.Write((char)i);
            }
            UTF8.Write("新聞");
            UTF8.Flush();
            UTF8.Close();

            StreamWriter UTF32 = new StreamWriter(@"c:\UTF32.txt", false, Encoding.UTF32);
            for (int i = 1; i <= 255; i++)
            {
                UTF32.Write((char)i);
            }
            UTF32.Write("新聞");
            UTF32.Flush();
            UTF32.Close();

            StreamWriter Default = new StreamWriter(@"c:\Default.txt", false, Encoding.Default);
            for (int i = 1; i <= 255; i++)
            {
                Default.Write((char)i);
            }
            Default.Write("新聞");
            Default.Flush();
            Default.Close();
            StreamWriter ASCII = new StreamWriter(@"c:\ASCII.txt", false, Encoding.ASCII);
            for (int i = 1; i <= 255; i++)
            {
                ASCII.Write((char)i);
            }
            ASCII.Write("新聞");
            ASCII.Flush();
            ASCII.Close();


            StreamWriter Unicode = new StreamWriter(@"c:\Unicode.txt", false, Encoding.Unicode);
            for (int i = 1; i <= 255; i++)
            {
                Unicode.Write((char)i);
            }
            Unicode.Write("新聞");
            Unicode.Flush();
            Unicode.Close();

            StreamWriter dian = new StreamWriter(@"c:\dian.txt", false, System.Text.Encoding.BigEndianUnicode);
            for (int i = 1; i <= 255; i++)
            {
                dian.Write((char)i);
            }
            dian.Write("新聞");
            dian.Flush();
            dian.Close();

            return "";
            //StreamReader sr = new StreamReader(@"c:\temp\1.txt",
            //Encoding.GetEncoding("big5"));
            //StreamWriter sw = new StreamWriter(@"c:\sw.txt",false, Encoding.GetEncoding("big5")); ;
            //StreamWriter sw = new StreamWriter(@"c:\sw.txt", false, Encoding.GetEncoding(""));
            StreamWriter sw = new StreamWriter(@"c:\sw.txt", false, Encoding.GetEncoding("big5"));
            //BinaryWriter TWB =  new BinaryWriter(File.Open(@"c:\TWB.txt", FileMode.Create));
            //TextWriter tw = new StreamWriter("date.txt");
            //sw.Write(0xFF);
            //sw.Write(0x255);
            //sw.Write(IntToHex(255));

            //sw.Write(Convert.ToInt32("FF", 16));

            //sw.Write(toHex("FF"));
            for (int i = 128; i < 255; i++)
            {
                //sw.Write(Convert.ToChar(i));
                //sw.Write(System.Convert.ToInt32(i));
                //sw.Write(i.ToString("X"));

                byte aaa1 = new byte();
                aaa1 = 255;
                //sw.Write(Convert.ToString(aaa, 16));
                //sw.Write(aaa);
                //sw.Write(Convert.ToString(i, 16));
                sw.Write((char)i);
                //sw.Write((char)255);
                //TWB.Write((char)i);
                //tw.Write((char)i);
            }
            //tw.Close();
            //    TWB.Close();
            sw.Write("新聞");
            //sw.Write((char)1);
            sw.Flush();
            sw.Close();
            //FileStream fs = new FileStream(@"c:\mcb.txt", FileMode.OpenOrCreate, FileAccess.Write);
            //StreamWriter m_streamWriter = new StreamWriter(fs);
            //// Write to the file using StreamWriter class
            //m_streamWriter.BaseStream.Seek(0, SeekOrigin.End);
            //for (int i = 1; i < 255; i++)
            //{
            //    m_streamWriter.Write((char)i);
            //}

            ////m_streamWriter.Write(" File Write Operation Starts : ");
            ////m_streamWriter.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
            ////m_streamWriter.WriteLine(" First Line : Data is first line \n");
            ////m_streamWriter.WriteLine(" This is next line in the text file. \n ");
            //m_streamWriter.Flush();
            //m_streamWriter.Close();
            foreach (ArrPromoList TemArrPromoList in InList)
            {
                //sw1.Write((char)0);  //將數字轉換為ASCII

                //sw1.Write((char)0); //        Type 		1 	1	BINARY
                //for (int i = 0; i < 8; i++)
                //{
                //    sw1.Write((char)255); //Reconcile Key 	2 	8 	ASCII 	FFH 
                //}

                //sw1.Write((char)0); //Effect1		10 	1 	BINARY 	00H 
                //sw1.Write((char)0); //Effect2 		11 	1 	BINARY 	00H
                //sw1.Write((char)0);  //Effect3 		12 	1	BINARY 	00H 
                //sw1.Write((char)int.Parse(TemArrPromoList.FSPLAY_TIME.Substring(9, 2))); //On Air Frame 	13 	1 	BCD 	FFH
                //sw1.Write((char)int.Parse(TemArrPromoList.FSPLAY_TIME.Substring(6, 2))); //On Air Second 	14 	1	BCD		FFH
                //sw1.Write((char)int.Parse(TemArrPromoList.FSPLAY_TIME.Substring(3, 2))); //On Air Minute 	15 	1 	BCD 	FFH
                //sw1.Write((char)int.Parse(TemArrPromoList.FSPLAY_TIME.Substring(0, 2))); //On Air Hour 	16 	1 	BCD 	FFH
                //for (int i = 0; i < 8; i++)
                //{
                //    sw1.Write(TemArrPromoList.FSVIDEO_ID.Substring(i, 1));  //ID 		17 	8 	ASCII 	FFH if empty, 20H if used (Material ID)
                //}
                //if (TemArrPromoList.FSVIDEO_ID.Length<=8)
                //    sw1.Write((char)int.Parse(TemArrPromoList.FSVIDEO_ID)); //ID 		17 	8 	ASCII 	FFH if empty, 20H if used (Material ID)
                //else
                //    sw1.Write((char)int.Parse(TemArrPromoList.FSVIDEO_ID.Substring(0,8)));

                //TITLE		25 	16 	ASCII 	20H
                //SOM Frame 	41 	1 	BCD 	FFH
                //SOM Second 	42 	1 	BCD 	FFH
                //SOM Minute 	43 	1 	BCD 	FFH
                //SOM Hour 	44 	1 	BCD 	FFH
                //DUR Frame 	45 	1 	BCD 	FFH
                //DUR Second 	46 	1 	BCD 	FFH
                //DUR Minute 	47 	1 	BCD 	FFH
                //DUR Hour 	48 	1 	BCD 	FFH
                //Channel 		49 	1	BINARY 	00H 
                //Qualifier4 	50 	1 	BINARY 	00H (Fill With Null) 
                //Segment Number 51 	1 	BCD 	FFH Program Segment Number
                //Device Maj. 	52 	1 	BINARY 	00H
                //Device Min. 	53 	1 	BINARY 	00H
                //Bin High 		54 	1 	BINARY 	00H
                //Bin Low 		55 	1 	BINARY 	00H
                //Qualifier1		56 	1 	BINARY 	00H (Fill With Null) 
                //Qualifier2		57 	1 	BINARY 	00H (Fill With Null)
                //Qualifier3		58 	1	BINARY 	00H (Fill With Null)
                //DateToAir 	59 	2 	WORD 	FFH (Julian Type for Asrun)
                //Event Control	61 	2 	BINARY
                //Event Status 	63 	2 	BINARY 	00H (Used by system)
                //CompileID 		65 		8 		ASCII 		FFH (Fill With Null)
                //CompileSOM 		73 		4 		BCD		FFH (Fill With Null) 
                //BoxAID 		77 		8 		ASCII		FFH (Fill With Null)
                //BoxASOM 		85 		4 		BCD 		FFH (Fill With Null)
                //BoxBID 		89 		8 		ASCII		FFH (Fill With Null)
                //BoxBSOM 		97 		4 		BCD 		FFH (Fill With Null)
                //Reserved 		101 		1 		BINARY 	00H (Fill With ZERO Always)
                //Backup Device Maj. 	102		1 		BINARY 	00H
                //Backup Device Min. 	103 		1 		BINARY 	00H
                //Extended EventControl 	104		1		BINARY 	00H

                //if (TemArrPromoList.FCTYPE == "G") //節目
                //{
                //    sbCombine.AppendLine("<Data>");
                //    sbCombine.AppendLine("<FNCONBINE_NO>" + TemArrPromoList.COMBINENO + "</FNCONBINE_NO>");
                //    sbCombine.AppendLine("<FCTIMETYPE>" + TemArrPromoList.FCTIME_TYPE + "</FCTIMETYPE>");
                //    sbCombine.AppendLine("<FSPROG_ID>" + TemArrPromoList.FSPROG_ID + "</FSPROG_ID>");
                //    sbCombine.AppendLine("<FNSEQNO>" + TemArrPromoList.FNSEQNO + "</FNSEQNO>");
                //    sbCombine.AppendLine("<FDDATE>" + _Date + "</FDDATE>");
                //    sbCombine.AppendLine("<FSCHANNEL_ID>" + _ChannelID + "</FSCHANNEL_ID>");
                //    sbCombine.AppendLine("<FNEPISODE>" + TemArrPromoList.FNEPISODE + "</FNEPISODE>");
                //    sbCombine.AppendLine("<FNBREAK_NO>" + TemArrPromoList.FNBREAK_NO + "</FNBREAK_NO>");
                //    sbCombine.AppendLine("<FNSUB_NO>" + TemArrPromoList.FNSUB_NO + "</FNSUB_NO>");
                //    sbCombine.AppendLine("<FCPROPERTY>" + TemArrPromoList.FCTYPE + "</FCPROPERTY>");
                //    sbCombine.AppendLine("<FSPLAY_TIME>" + TemArrPromoList.FSPLAY_TIME + "</FSPLAY_TIME>");
                //    sbCombine.AppendLine("<FSDUR>" + TemArrPromoList.FSDURATION + "</FSDUR>");
                //    sbCombine.AppendLine("<FNLIVE>" + "" + "</FNLIVE>");
                //    sbCombine.AppendLine("<FSPROMO_ID>" + "" + "</FSPROMO_ID>");
                //    sbCombine.AppendLine("<FSMEMO>" + TemArrPromoList.FSMEMO + "</FSMEMO>");
                //    sbCombine.AppendLine("<FSCREATED_BY>" + UserClass.userData.FSUSER_ChtName + "</FSCREATED_BY>");
                //    sbCombine.AppendLine("<FSPROG_NAME>" + TemArrPromoList.FSPROG_NAME + "</FSPROG_NAME>");
                //    sbCombine.AppendLine("<FSVIDEO_ID>" + TemArrPromoList.FSVIDEO_ID + "</FSVIDEO_ID>");
                //    sbCombine.AppendLine("</Data>");
                //}
                //else if (TemArrPromoList.FCTYPE == "P")  //宣傳帶
                //{
                //    sbArrPromo.AppendLine("<Data>");
                //    sbArrPromo.AppendLine("<FNARR_PROMO_NO>" + TemArrPromoList.COMBINENO + "</FNARR_PROMO_NO>");
                //    sbArrPromo.AppendLine("<FSPROG_ID>" + TemArrPromoList.FSPROG_ID + "</FSPROG_ID>");
                //    sbArrPromo.AppendLine("<FNSEQNO>" + TemArrPromoList.FNSEQNO + "</FNSEQNO>");
                //    sbArrPromo.AppendLine("<FDDATE>" + _Date + "</FDDATE>");
                //    sbArrPromo.AppendLine("<FSCHANNEL_ID>" + _ChannelID + "</FSCHANNEL_ID>");
                //    sbArrPromo.AppendLine("<FNBREAK_NO>" + TemArrPromoList.FNBREAK_NO + "</FNBREAK_NO>");
                //    sbArrPromo.AppendLine("<FNSUB_NO>" + TemArrPromoList.FNSUB_NO + "</FNSUB_NO>");
                //    sbArrPromo.AppendLine("<FSPROMO_ID>" + TemArrPromoList.FSPROMO_ID + "</FSPROMO_ID>");
                //    sbArrPromo.AppendLine("<FSPLAYTIME>" + TemArrPromoList.FSPLAY_TIME + "</FSPLAYTIME>");
                //    sbArrPromo.AppendLine("<FSDUR>" + TemArrPromoList.FSDURATION + "</FSDUR>");
                //    sbArrPromo.AppendLine("<FSMEMO>" + TemArrPromoList.FSMEMO + "</FSMEMO>");
                //    sbArrPromo.AppendLine("<FSCREATED_BY>" + UserClass.userData.FSUSER_ChtName + "</FSCREATED_BY>");

                //    sbArrPromo.AppendLine("</Data>");
                //}
            }
            //sw1.Flush();
            //sw1.Close();
            return "<Errors>" + "沒有節目表資料" + "</Errors>";
        }


        //將LST File節目名稱轉換編碼

        public class LSTProgName
        {
            public byte[] Name { get; set; }  //節目名稱
        }

        [WebMethod()]
        public string TransLstProgName(List<LSTProgName> aaa)
        {
            //ProgName aaa = new ProgName();
            string ReturnString = "";

            ReturnString = ReturnString + "<Datas>";
            foreach (LSTProgName TTT in aaa)
            {
                ReturnString = ReturnString + "<Data><ProgName>";
                ReturnString = ReturnString + System.Text.Encoding.GetEncoding(950).GetString(TTT.Name);
                ReturnString = ReturnString + "</ProgName></Data>";
            }
            ReturnString = ReturnString + "</Datas>";
            //參數剖析
            //string NewProgName;
            //NewProgName = System.Text.Encoding.GetEncoding(950).GetString(parameters);

            //ReturnString = NewProgName;
            return ReturnString;
        }

        public string TransASXTimecode(string InTimecode)
        {
            string ReturnString;
            ReturnString = InTimecode.Substring(0, 8) + "." + (Int32.Parse(InTimecode.Substring(InTimecode.Length - 2, 2)) * 0.033366700033667).ToString();
            return ReturnString;
        }

        [WebMethod()]
        public string GetPromoWELFARE(string PromoID)
        {
            //ProgName aaa = new ProgName();


            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            StringBuilder sb = new StringBuilder();

            SqlDataAdapter da = new SqlDataAdapter("SP_Q_GET_TBPGM_PROMO_WELFARE", connection);
            DataTable dt = new DataTable();

            SqlParameter para = new SqlParameter("@FSPROMO_ID", PromoID);
            da.SelectCommand.Parameters.Add(para);

            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                da.Fill(dt);
            }
            catch (Exception ex)
            {
                StringBuilder sbError = new StringBuilder();
                sbError.AppendLine("<Errors>" + ex.Message + "</Errors>");
                return "";
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }
            string ReturnString = "";
            ReturnString = dt.Rows[0][0].ToString();

            return ReturnString;

        }


        [WebMethod()]
        public string GetLVFilePath(string fileno, int segno)
        {
            //ProgName aaa = new ProgName();
            string ReturnString = "";

            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            StringBuilder sb = new StringBuilder();

            SqlDataAdapter da = new SqlDataAdapter("SP_Q_TBLOG_VIDEO_SEG_ALL", connection);
            DataTable dt = new DataTable();

            SqlParameter para = new SqlParameter("@FSFILE_NO", fileno);
            da.SelectCommand.Parameters.Add(para);
            //SqlParameter para1 = new SqlParameter("@FNSEG_ID", String.Format("{0:00}", segno));
            //da.SelectCommand.Parameters.Add(para1);
            //參數剖析

            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            string First_Seg_Start_Time = "";
            string start_time = "";
            string duration = "";
            string end_Time = "";
            string url = "";

            Guid g;
            g = Guid.NewGuid();
            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                da.Fill(dt);
                //WSBookingFunction
                //WriteAsxFile(g, start_time, duration, url);
                string TempBegTime = "";
                string TempEndTime = "";
                string TempSeg = "";

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        TempBegTime = "";
                        TempEndTime = "";
                        TempSeg = "";
                        for (int j = 0; j <= dt.Columns.Count - 1; j++)
                        {
                            if (dt.Columns[j].ColumnName == "FSBEG_TIMECODE")
                                TempBegTime = dt.Rows[i][j].ToString();
                            if (dt.Columns[j].ColumnName == "FSEND_TIMECODE")
                                TempEndTime = dt.Rows[i][j].ToString();
                            if (dt.Columns[j].ColumnName == "FNSEG_ID")
                                TempSeg = dt.Rows[i][j].ToString();
                        }
                        if (TempSeg == "01")
                            First_Seg_Start_Time = TempBegTime;

                        if (TempSeg == String.Format("{0:00}", segno))
                        {
                            start_time = TempBegTime;
                            end_Time = TempEndTime;
                        }
                    }
                    start_time = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(start_time) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(First_Seg_Start_Time));
                    end_Time = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(end_Time) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(First_Seg_Start_Time));
                    duration = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(end_Time) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(start_time));
                    duration = TransASXTimecode(duration);
                    //WSBookingFunction
                    PTS_MAM3.Web.DataSource.WSBookingFunction sss = new PTS_MAM3.Web.DataSource.WSBookingFunction();
                    PTS_MAM3.Web.WS.WSTSM.ServiceTSM obj = new WS.WSTSM.ServiceTSM();

                    url = obj.GetHttpPathByFileID(fileno, WS.WSTSM.ServiceTSM.FileID_MediaType.LowVideo);
                    sss.WriteAsxFile(g.ToString(), start_time, duration, url);
                    ReturnString = sss.GetAsxFile(g.ToString());
                    return ReturnString;
                }
                else
                {
                    return sb.ToString();
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                StringBuilder sbError = new StringBuilder();
                sbError.AppendLine("<Errors>" + ex.Message + "</Errors>");
                return sbError.ToString();
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }


            //ReturnString = ReturnString + "<Datas>";
            //foreach (LSTProgName TTT in aaa)
            //{
            //    ReturnString = ReturnString + "<Data><ProgName>";
            //    ReturnString = ReturnString + System.Text.Encoding.GetEncoding(950).GetString(TTT.Name);
            //    ReturnString = ReturnString + "</ProgName></Data>";
            //}
            //ReturnString = ReturnString + "</Datas>";
            ////參數剖析
            ////string NewProgName;
            ////NewProgName = System.Text.Encoding.GetEncoding(950).GetString(parameters);

            ////ReturnString = NewProgName;
            return ReturnString;
        }


        //取得輸入字串前幾個字元byte數,中文字算2byte
        public string GetSubString(string str, int getStrLength)
        {
            int StrLength = 0;

            //string tempStr = "";
            int StrSubLength = 0;
            foreach (char c in str)
            {
                if ((int)c >= 33 && (int)c <= 126)
                {
                    if ((StrLength + 1) > getStrLength)
                        return str.Substring(0, StrSubLength);
                    StrLength = StrLength + 1;
                    StrSubLength = StrSubLength + 1;
                }
                else
                {
                    int aa = (int)c;
                    //MessageBox.Show(aa.ToString());
                    if ((StrLength + 2) > getStrLength)
                        return str.Substring(0, StrSubLength);
                    StrLength = StrLength + 2;
                    StrSubLength = StrSubLength + 1;
                }
            }
            return str.Substring(0, StrSubLength);
        }


        public class AddZipFileClass
        {
            public ZipEntry entry { get; set; }
            public byte[] buffer { get; set; }  //時間類型 O:定時 A:順時

        }

        public AddZipFileClass AddZipFile(string ZipFileName, string ZipName)
        {
            Crc32 crc = new Crc32();

            FileStream fs = File.OpenRead(ZipFileName);
            byte[] buffer = new byte[fs.Length];
            fs.Read(buffer, 0, buffer.Length);
            ZipEntry entry = new ZipEntry(ZipEntry.CleanName(ZipName));
            entry.DateTime = DateTime.Now;
            entry.Comment = "test file";
            entry.ZipFileIndex = 1;
            entry.Size = fs.Length;
            fs.Close();
            crc.Reset();
            crc.Update(buffer);
            entry.Crc = crc.Value;
            AddZipFileClass AAA = new AddZipFileClass();
            AAA.entry = entry;
            AAA.buffer = buffer;
            return AAA;

        }

        public class RPT_DATE_LST
        {
            public string QUERY_KEY { get; set; }
            public string QUERY_KEY_SUBID { get; set; }
            public string QUERY_BY { get; set; }
            public string NO { get; set; }
            public string TIMETYPE { get; set; }
            public string PROGID { get; set; }
            public string SEQNO { get; set; }
            public string PROGNAME { get; set; }
            public string PROMOID { get; set; }
            public string CHANNELNAME { get; set; }
            public string PROGEPISODE { get; set; }
            public string PROGSEG { get; set; }
            public string SUBNO { get; set; }
            public string PLAYDATE { get; set; }
            public string PLAYTIME { get; set; }
            public string DUR { get; set; }
            public string FILENO { get; set; }
            public string VIDEOID { get; set; }
            public string DATATYPE { get; set; }
            public string MEMO { get; set; }
            public string FSSIGNAL { get; set; }
            public string FSRAN { get; set; }
            public string FNLIVE { get; set; }
            public string FSSOM { get; set; }
        }

        //寫入資料到主控播出運行表報表,包含節目表與Louth LIST 分筆資料
        public bool INSERT_RPT_DATE_LST(RPT_DATE_LST TMP_RPT_DATE_LST)
        {
            Guid g1;
            g1 = Guid.NewGuid();

            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;

            SqlConnection connection1 = new SqlConnection(sqlConnStr);
            SqlTransaction tran = null;
            SqlCommand sqlcmd = new SqlCommand("SP_I_RPT_DATE_LST", connection1);

            SqlParameter QUERY_KEY = new SqlParameter("@QUERY_KEY", TMP_RPT_DATE_LST.QUERY_KEY);
            sqlcmd.Parameters.Add(QUERY_KEY);
            SqlParameter QUERY_KEY_SUBID = new SqlParameter("@QUERY_KEY_SUBID", g1.ToString());
            sqlcmd.Parameters.Add(QUERY_KEY_SUBID);
            SqlParameter QUERY_BY = new SqlParameter("@QUERY_BY", TMP_RPT_DATE_LST.QUERY_BY);
            sqlcmd.Parameters.Add(QUERY_BY);
            SqlParameter NO = new SqlParameter("@序號", TMP_RPT_DATE_LST.NO);
            sqlcmd.Parameters.Add(NO);
            SqlParameter TIMETYPE = new SqlParameter("@時間類型", TMP_RPT_DATE_LST.TIMETYPE);
            sqlcmd.Parameters.Add(TIMETYPE);
            SqlParameter PROGID = new SqlParameter("@節目編碼", TMP_RPT_DATE_LST.PROGID);
            sqlcmd.Parameters.Add(PROGID);
            SqlParameter SEQNO = new SqlParameter("@播映序號", TMP_RPT_DATE_LST.SEQNO);
            sqlcmd.Parameters.Add(SEQNO);

            if (TMP_RPT_DATE_LST.DATATYPE == "G")
            {
                //節目 Title 要加入集數
                string AddEpisodeName = "";
                if (TMP_RPT_DATE_LST.PROGEPISODE != "" && TMP_RPT_DATE_LST.PROGEPISODE != null)
                    AddEpisodeName = GetSubString(TMP_RPT_DATE_LST.PROGNAME, 16 - (TMP_RPT_DATE_LST.PROGEPISODE.Length + 1)) + "#" + TMP_RPT_DATE_LST.PROGEPISODE;
                else
                    AddEpisodeName = GetSubString(TMP_RPT_DATE_LST.PROGNAME, 16);
                SqlParameter PROGNAME = new SqlParameter("@名稱", AddEpisodeName);
                sqlcmd.Parameters.Add(PROGNAME);
            }
            else
            {
                SqlParameter PROGNAME = new SqlParameter("@名稱", TMP_RPT_DATE_LST.PROGNAME);
                sqlcmd.Parameters.Add(PROGNAME);
            }
            SqlParameter PROMOID = new SqlParameter("@短帶編碼", TMP_RPT_DATE_LST.PROMOID);
            sqlcmd.Parameters.Add(PROMOID);
            SqlParameter CHANNELNAME = new SqlParameter("@頻道", TMP_RPT_DATE_LST.CHANNELNAME);
            sqlcmd.Parameters.Add(CHANNELNAME);
            SqlParameter PROGEPISODE = new SqlParameter("@節目集數", TMP_RPT_DATE_LST.PROGEPISODE);
            sqlcmd.Parameters.Add(PROGEPISODE);
            SqlParameter PROGSEG = new SqlParameter("@段數", TMP_RPT_DATE_LST.PROGSEG);
            sqlcmd.Parameters.Add(PROGSEG);
            SqlParameter SUBNO = new SqlParameter("@支數", TMP_RPT_DATE_LST.SUBNO);
            sqlcmd.Parameters.Add(SUBNO);
            SqlParameter PLAYDATE = new SqlParameter("@播出日期", TMP_RPT_DATE_LST.PLAYDATE);
            sqlcmd.Parameters.Add(PLAYDATE);
            SqlParameter PLAYTIME = new SqlParameter("@播出時間", TMP_RPT_DATE_LST.PLAYTIME);
            sqlcmd.Parameters.Add(PLAYTIME);
            SqlParameter DUR = new SqlParameter("@長度", TMP_RPT_DATE_LST.DUR);
            sqlcmd.Parameters.Add(DUR);
            SqlParameter FILENO = new SqlParameter("@影像編號", TMP_RPT_DATE_LST.FILENO);
            sqlcmd.Parameters.Add(FILENO);
            SqlParameter VIDEOID = new SqlParameter("@主控編號", TMP_RPT_DATE_LST.VIDEOID);
            sqlcmd.Parameters.Add(VIDEOID);
            SqlParameter DATATYPE = new SqlParameter("@資料類型", TMP_RPT_DATE_LST.DATATYPE);
            sqlcmd.Parameters.Add(DATATYPE);
            SqlParameter MEMO = new SqlParameter("@備註", TMP_RPT_DATE_LST.MEMO);
            sqlcmd.Parameters.Add(MEMO);
            SqlParameter FSSIGNAL = new SqlParameter("@FSSIGNAL", TMP_RPT_DATE_LST.FSSIGNAL);
            sqlcmd.Parameters.Add(FSSIGNAL);
            SqlParameter FSRAN = new SqlParameter("@FSRAN", TMP_RPT_DATE_LST.FSRAN);
            sqlcmd.Parameters.Add(FSRAN);
            SqlParameter FNLIVE = new SqlParameter("@FNLIVE", TMP_RPT_DATE_LST.FNLIVE);
            sqlcmd.Parameters.Add(FNLIVE);

            sqlcmd.CommandType = CommandType.StoredProcedure;

            try
            {
                if (connection1.State == System.Data.ConnectionState.Closed)
                {
                    connection1.Open();
                }

                tran = connection1.BeginTransaction();
                sqlcmd.Transaction = tran;
                if (sqlcmd.ExecuteNonQuery() > 0)
                {
                    tran.Commit();
                    //return true;
                }
                else
                {
                    //不須知道異動筆數
                    tran.Commit();
                    //tran.Rollback();
                    //return true;
                }

            }
            catch (Exception ex)
            {
                tran.Rollback();
                return false;
            }
            finally
            {
                connection1.Close();
                connection1.Dispose();
                sqlcmd.Dispose();
                GC.Collect();

            }

            return true;
        }

        //寫入資料到主控播出運行表報表,包含節目表與Louth LIST 分筆資料
        public bool INSERT_RPT_HD_DATE_LST(RPT_DATE_LST TMP_RPT_DATE_LST)
        {
            Guid g1;
            g1 = Guid.NewGuid();

            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;

            SqlConnection connection1 = new SqlConnection(sqlConnStr);
            SqlTransaction tran = null;
            SqlCommand sqlcmd = new SqlCommand("SP_I_RPT_HD_DATE_LST", connection1);

            SqlParameter QUERY_KEY = new SqlParameter("@QUERY_KEY", TMP_RPT_DATE_LST.QUERY_KEY);
            sqlcmd.Parameters.Add(QUERY_KEY);
            SqlParameter QUERY_KEY_SUBID = new SqlParameter("@QUERY_KEY_SUBID", g1.ToString());
            sqlcmd.Parameters.Add(QUERY_KEY_SUBID);
            SqlParameter QUERY_BY = new SqlParameter("@QUERY_BY", TMP_RPT_DATE_LST.QUERY_BY);
            sqlcmd.Parameters.Add(QUERY_BY);
            SqlParameter NO = new SqlParameter("@序號", TMP_RPT_DATE_LST.NO);
            sqlcmd.Parameters.Add(NO);
            SqlParameter TIMETYPE = new SqlParameter("@時間類型", TMP_RPT_DATE_LST.TIMETYPE);
            sqlcmd.Parameters.Add(TIMETYPE);
            SqlParameter PROGID = new SqlParameter("@節目編碼", TMP_RPT_DATE_LST.PROGID);
            sqlcmd.Parameters.Add(PROGID);
            SqlParameter SEQNO = new SqlParameter("@播映序號", TMP_RPT_DATE_LST.SEQNO);
            sqlcmd.Parameters.Add(SEQNO);

            if (TMP_RPT_DATE_LST.DATATYPE == "G")
            {
                //節目 Title 要加入集數
                string AddEpisodeName = "";
                if (TMP_RPT_DATE_LST.PROGEPISODE != "" && TMP_RPT_DATE_LST.PROGEPISODE != null)
                    AddEpisodeName = GetSubString(TMP_RPT_DATE_LST.PROGNAME, 16 - (TMP_RPT_DATE_LST.PROGEPISODE.Length + 1)) + "#" + TMP_RPT_DATE_LST.PROGEPISODE;
                else
                    AddEpisodeName = GetSubString(TMP_RPT_DATE_LST.PROGNAME, 16);
                SqlParameter PROGNAME = new SqlParameter("@名稱", AddEpisodeName);
                sqlcmd.Parameters.Add(PROGNAME);
            }
            else
            {
                SqlParameter PROGNAME = new SqlParameter("@名稱", TMP_RPT_DATE_LST.PROGNAME);
                sqlcmd.Parameters.Add(PROGNAME);
            }
            SqlParameter PROMOID = new SqlParameter("@短帶編碼", TMP_RPT_DATE_LST.PROMOID);
            sqlcmd.Parameters.Add(PROMOID);
            SqlParameter CHANNELNAME = new SqlParameter("@頻道", TMP_RPT_DATE_LST.CHANNELNAME);
            sqlcmd.Parameters.Add(CHANNELNAME);
            SqlParameter PROGEPISODE = new SqlParameter("@節目集數", TMP_RPT_DATE_LST.PROGEPISODE);
            sqlcmd.Parameters.Add(PROGEPISODE);
            SqlParameter PROGSEG = new SqlParameter("@段數", TMP_RPT_DATE_LST.PROGSEG);
            sqlcmd.Parameters.Add(PROGSEG);
            SqlParameter SUBNO = new SqlParameter("@支數", TMP_RPT_DATE_LST.SUBNO);
            sqlcmd.Parameters.Add(SUBNO);
            SqlParameter PLAYDATE = new SqlParameter("@播出日期", TMP_RPT_DATE_LST.PLAYDATE);
            sqlcmd.Parameters.Add(PLAYDATE);
            SqlParameter PLAYTIME = new SqlParameter("@播出時間", TMP_RPT_DATE_LST.PLAYTIME);
            sqlcmd.Parameters.Add(PLAYTIME);
            SqlParameter DUR = new SqlParameter("@長度", TMP_RPT_DATE_LST.DUR);
            sqlcmd.Parameters.Add(DUR);
            SqlParameter FILENO = new SqlParameter("@影像編號", TMP_RPT_DATE_LST.FILENO);
            sqlcmd.Parameters.Add(FILENO);
            SqlParameter VIDEOID = new SqlParameter("@主控編號", TMP_RPT_DATE_LST.VIDEOID);
            sqlcmd.Parameters.Add(VIDEOID);
            SqlParameter DATATYPE = new SqlParameter("@資料類型", TMP_RPT_DATE_LST.DATATYPE);
            sqlcmd.Parameters.Add(DATATYPE);
            SqlParameter MEMO = new SqlParameter("@備註", TMP_RPT_DATE_LST.MEMO);
            sqlcmd.Parameters.Add(MEMO);
            SqlParameter FSSIGNAL = new SqlParameter("@FSSIGNAL", TMP_RPT_DATE_LST.FSSIGNAL);
            sqlcmd.Parameters.Add(FSSIGNAL);
            SqlParameter FSRAN = new SqlParameter("@FSRAN", TMP_RPT_DATE_LST.FSRAN);
            sqlcmd.Parameters.Add(FSRAN);
            SqlParameter FNLIVE = new SqlParameter("@FNLIVE", TMP_RPT_DATE_LST.FNLIVE);
            sqlcmd.Parameters.Add(FNLIVE);
            SqlParameter FSSOM = new SqlParameter("@FSSOM", TMP_RPT_DATE_LST.FSSOM);
            sqlcmd.Parameters.Add(FSSOM);

            sqlcmd.CommandType = CommandType.StoredProcedure;

            try
            {
                if (connection1.State == System.Data.ConnectionState.Closed)
                {
                    connection1.Open();
                }

                tran = connection1.BeginTransaction();
                sqlcmd.Transaction = tran;
                if (sqlcmd.ExecuteNonQuery() > 0)
                {
                    tran.Commit();
                    //return true;
                }
                else
                {
                    //不須知道異動筆數
                    tran.Commit();
                    //tran.Rollback();
                    //return true;
                }

            }
            catch (Exception ex)
            {
                tran.Rollback();
                return false;
            }
            finally
            {
                connection1.Close();
                connection1.Dispose();
                sqlcmd.Dispose();
                GC.Collect();

            }

            return true;
        }
        [WebMethod()]
        public string PrintLst(List<ArrPromoList> InList, string USERNAME)
        {

            PGM_ARR_PROMOAddSOM.Clear();
            foreach (ArrPromoList TemArrPromoList in InList)
            {
                PGM_ARR_PROMOAddSOM.Add(AddSOMToArrPromoList(TemArrPromoList));
            }

            string TempChannelID = "";
            string TempDate = "";

            //INSERT_RPT_DATE_LST
            Guid g;
            g = Guid.NewGuid();
            try
            {
                int WorkRow = 0;
                bool beFirst = false; //是否為第一段
                bool bLast = false;  //是否為最後一段
                string PriorDataType = "";
                //逐一分析每筆資料的FSMEMO並將其寫入報表中
                int RowNO = 1;
                foreach (ArrPromoListAddSOM TemArrPromoList in PGM_ARR_PROMOAddSOM)
                {
                    TempChannelID = TemArrPromoList.FSCHANNEL_ID;
                    TempDate = TemArrPromoList.FDDATE;
                    RPT_DATE_LST TMP_RPT_DATE_LST = new RPT_DATE_LST();
                    TMP_RPT_DATE_LST.QUERY_KEY = g.ToString();

                    TMP_RPT_DATE_LST.QUERY_KEY_SUBID = "";
                    TMP_RPT_DATE_LST.QUERY_BY = USERNAME;

                    TMP_RPT_DATE_LST.NO = RowNO.ToString();
                    RowNO = RowNO + 1;

                    TMP_RPT_DATE_LST.VIDEOID = TemArrPromoList.FSVIDEO_ID;
                    TMP_RPT_DATE_LST.PROGNAME = TemArrPromoList.FSNAME;
                    if (int.Parse(TemArrPromoList.FSPLAY_TIME.Substring(0, 2)) >= 24)
                        TMP_RPT_DATE_LST.PLAYTIME = (int.Parse(TemArrPromoList.FSPLAY_TIME.Substring(0, 2)) - 24).ToString() + TemArrPromoList.FSPLAY_TIME.Substring(2, 9);
                    else
                        TMP_RPT_DATE_LST.PLAYTIME = TemArrPromoList.FSPLAY_TIME;
                    TMP_RPT_DATE_LST.DUR = TemArrPromoList.FSDURATION;
                    TMP_RPT_DATE_LST.PROGID = TemArrPromoList.FSPROG_ID;
                    TMP_RPT_DATE_LST.SEQNO = TemArrPromoList.FNSEQNO;
                    TMP_RPT_DATE_LST.DATATYPE = TemArrPromoList.FCTYPE;
                    TMP_RPT_DATE_LST.PROMOID = TemArrPromoList.FSPROMO_ID;
                    TMP_RPT_DATE_LST.FSSIGNAL = TemArrPromoList.FSSIGNAL;
                    TMP_RPT_DATE_LST.PLAYDATE = TemArrPromoList.FDDATE;
                    TMP_RPT_DATE_LST.CHANNELNAME = TemArrPromoList.FSCHANNEL_NAME;
                    TMP_RPT_DATE_LST.SUBNO = TemArrPromoList.FNSUB_NO;
                    TMP_RPT_DATE_LST.PLAYDATE = TemArrPromoList.FDDATE;
                    TMP_RPT_DATE_LST.PROGSEG = TemArrPromoList.FNBREAK_NO;
                    TMP_RPT_DATE_LST.PROGEPISODE = TemArrPromoList.FNEPISODE;
                    TMP_RPT_DATE_LST.FSRAN = "";
                    TMP_RPT_DATE_LST.FNLIVE = TemArrPromoList.FNLIVE;
                    TMP_RPT_DATE_LST.FILENO = TemArrPromoList.FSFILE_NO;
                    //string SOM = TemArrPromoList.FSSOM;

                    if (TemArrPromoList.FCTIME_TYPE == "O")
                        TMP_RPT_DATE_LST.TIMETYPE = "AO";
                    //else if ((TemArrPromoList.FCTYPE=="G") || (TemArrPromoList.FNSUB_NO=="1"))
                    else if ((TemArrPromoList.FCTYPE == "G") || (TemArrPromoList.FCTYPE == "P" && TemArrPromoList.FNBREAK_NO != "0" && TemArrPromoList.FNSUB_NO == "1"))

                        TMP_RPT_DATE_LST.TIMETYPE = "AN";
                    //else if ((TemArrPromoList.FNBREAK_NO=="1" && TemArrPromoList.FCTYPE=="G") || (TemArrPromoList.FNSUB_NO=="1"))
                    else
                        TMP_RPT_DATE_LST.TIMETYPE = "A";

                    //如果前一筆資料為節目,此筆資料為短帶,則要設定為 AN
                    if (PriorDataType == "G" && TemArrPromoList.FCTYPE == "P")
                    {
                        TMP_RPT_DATE_LST.TIMETYPE = "AN";
                    }
                    //用完後將此筆資料的類型設定進PriorDataType,當下一筆時可以使用
                    PriorDataType = TemArrPromoList.FCTYPE;

                    if ((TemArrPromoList.FCTYPE == "G") && (TemArrPromoList.FNLIVE == "1")) //Live節目
                    {
                        if (TemArrPromoList.FCTIME_TYPE == "O")
                            TMP_RPT_DATE_LST.TIMETYPE = "AUNO";
                        else
                            TMP_RPT_DATE_LST.TIMETYPE = "AUN";
                    }

                    if ((TemArrPromoList.FCTYPE == "P") && (TemArrPromoList.FSSIGNAL != "PGM") && (TemArrPromoList.FSSIGNAL != "COMM") && (TemArrPromoList.FSSIGNAL != "")) //Live PROMO
                    {
                        if (TemArrPromoList.FCTIME_TYPE == "O")
                            TMP_RPT_DATE_LST.TIMETYPE = "AUNO";
                        else
                            TMP_RPT_DATE_LST.TIMETYPE = "AUN";
                    }
                    if (TemArrPromoList.FNLIVE == "1")  //如果為Live則要將訊號源寫入VIdeoID中
                    {
                        TMP_RPT_DATE_LST.VIDEOID = TemArrPromoList.FSSIGNAL;
                    }
                    TMP_RPT_DATE_LST.TIMETYPE = TemArrPromoList.FCTIME_TYPE;
                    TMP_RPT_DATE_LST.MEMO = "";
                    //TMP_RPT_DATE_LST.MEMO = TemArrPromoList.FSMEMO;
                    if (TMP_RPT_DATE_LST.DATATYPE != "G")
                        TMP_RPT_DATE_LST.PROGSEG = "";
                    if (TemArrPromoList.FSMEMO.ToUpper().IndexOf("LOGO") > -1) //Logo那節目要+Lose
                        TMP_RPT_DATE_LST.MEMO = TMP_RPT_DATE_LST.MEMO + "(LOSE)";
                    //if (TemArrPromoList.FSMEMO.IndexOf("LOGO") > -1) //Logo那節目要+Lose
                    //    TMP_RPT_DATE_LST.MEMO = TMP_RPT_DATE_LST.MEMO+ "+LOSE";
                    //if (TemArrPromoList.FSPROG_ID == "2004090" && TemArrPromoList.FCTYPE == "G") //訊號測試節目要+Lose
                    //    TMP_RPT_DATE_LST.MEMO = TMP_RPT_DATE_LST.MEMO + "+LOSE";

                    INSERT_RPT_DATE_LST(TMP_RPT_DATE_LST);

                    if (TemArrPromoList.FCTYPE == "G")
                    {
                        beFirst = false; //是否為第一段
                        bLast = false;  //是否為最後一段
                        if (WorkRow < PGM_ARR_PROMOAddSOM.Count - 1) //檢查此資料狀態,是否為第一段 和 是否為最後一段 和 是否只有一段
                        {
                            if (TemArrPromoList.FNBREAK_NO == "1")
                                beFirst = true;
                            int NextProgRow = WorkRow + 1;
                            while (NextProgRow < PGM_ARR_PROMOAddSOM.Count - 1 && PGM_ARR_PROMOAddSOM[NextProgRow].FCTYPE == "P")
                                NextProgRow = NextProgRow + 1;

                            if (PGM_ARR_PROMOAddSOM[NextProgRow].FCTYPE == "G")
                            {
                                if (PGM_ARR_PROMOAddSOM[NextProgRow].FSPROG_ID != TemArrPromoList.FSPROG_ID || PGM_ARR_PROMOAddSOM[NextProgRow].FNSEQNO != TemArrPromoList.FNSEQNO)
                                    bLast = true; //表示最後一段
                            }
                            else
                            {
                                bLast = true; //表示最後一段
                            }
                        }
                    }
                    WorkRow = WorkRow + 1;
                    //開始分析Memo的Louth Key
                    if (TemArrPromoList.FSMEMO != "")
                    {
                        string[] LouthKeys = TemArrPromoList.FSMEMO.Split(new Char[] { '+' });

                        foreach (string LouthKey in LouthKeys)  //每一個+ 號表示一個Key
                        {
                            if (LouthKey.Trim() != "")
                            {

                                TMP_RPT_DATE_LST.PROGSEG = "";

                                TMP_RPT_DATE_LST.NO = "";
                                TMP_RPT_DATE_LST.TIMETYPE = "";
                                TMP_RPT_DATE_LST.MEMO = "";
                                // 時間 ,重播,直播  為 sTRANKEY 其他為 sKEYER

                                //VideoID = "sKEYER";
                                string[] LouthKeyType = LouthKey.Split(new Char[] { '#' }); //將LouthKey 依照#分開,如果有 #代表有輸入時間 Ex:中/英#00:02:00,10,30
                                if (LouthKeyType[0] != "") //如果有設定LouthKey Ex:中/英#00:02:00,10,30
                                {
                                    #region 查詢Louth的資料 並將其加入 NewLouth
                                    LouthTableData NewLouth = new LouthTableData();

                                    if (LouthKeyType[0].IndexOf("片名") == 0)
                                    {
                                        //NewLouth = GetLouthData("片名-" + TemArrPromoList.FSNAME, TempChannelID);

                                        NewLouth = GetLouthData(LouthKeyType[0], TempChannelID);
                                        if (LouthKeyType[0].IndexOf("(整段)") > -1)
                                            NewLouth = GetLouthData(LouthKeyType[0].Substring(3, (LouthKeyType[0].Length) - 7), TempChannelID);
                                        else if (LouthKeyType[0].IndexOf("(全程避片頭)") > -1)
                                            NewLouth = GetLouthData(LouthKeyType[0].Substring(3, (LouthKeyType[0].Length) - 10), TempChannelID);
                                        else
                                            NewLouth = GetLouthData(LouthKeyType[0].Substring(3, (LouthKeyType[0].Length) - 3), TempChannelID);
                                    }
                                    else
                                        NewLouth = GetLouthData(LouthKeyType[0], TempChannelID); //取得Louth Table的資料
                                    if (NewLouth.FSNO != "")
                                    {
                                        //QF4 = NewLouth.FSLayel;
                                        TMP_RPT_DATE_LST.VIDEOID = NewLouth.FSNO.Substring(1, NewLouth.FSNO.Length - 1);
                                        TMP_RPT_DATE_LST.DATATYPE = "K";
                                        TMP_RPT_DATE_LST.FSSIGNAL = NewLouth.FSLayel;
                                        // QF1 = NewLouth.FSNO.Substring(1, NewLouth.FSNO.Length - 1);

                                        if (LouthKeyType[0].IndexOf("時間") == 0 || LouthKeyType[0].IndexOf("重播") == 0 || LouthKeyType[0].IndexOf("直播") == 0)
                                        {

                                            TMP_RPT_DATE_LST.MEMO = "sTRANKEY";
                                            TMP_RPT_DATE_LST.DUR = "";
                                            TMP_RPT_DATE_LST.PLAYTIME = "";


                                        }
                                        else
                                        {
                                            if (LouthKeyType[0].ToUpper().IndexOf("LOGO") == 0)
                                                TMP_RPT_DATE_LST.MEMO = "sKEYER(EH)";
                                            else
                                                TMP_RPT_DATE_LST.MEMO = "sKEYER";
                                            //SOM = "";
                                            TMP_RPT_DATE_LST.PLAYTIME = "00:00:00:00";
                                            TMP_RPT_DATE_LST.DUR = TemArrPromoList.FSDURATION;
                                        }
                                        TMP_RPT_DATE_LST.PROGNAME = NewLouth.FSNAME;
                                    #endregion
                                        bool NeedLoop = false;    //是否要做循環
                                        TimeStatus RTimeStatus = new TimeStatus();
                                        SEndPointStatus REndPointStatus = new SEndPointStatus();
                                        //設定播出規則,沒有的話則採用預設規則  並將資料加入RTimeStatus 只有雙語 片名 立體聲 數位同步播出有 值
                                        //接著跟繼續收看
                                        #region 設定播出規則,沒有的話則採用預設規則  並將資料加入RTimeStatus
                                        if ((LouthKeyType[0].IndexOf("/") > -1) || (LouthKeyType[0].IndexOf("片名") == 0) || (LouthKeyType[0].IndexOf("立體聲") == 0) || (LouthKeyType[0].IndexOf("數位") == 0) || (LouthKeyType[0].IndexOf("口述影像版") > -1)) //代表為片名 //有斜線代表為雙語
                                        {
                                            if (LouthKey.IndexOf("#") > -1)  //#00:02:00,10,30 //表示有設定起始時間,間隔,長度
                                            {
                                                RTimeStatus = Trans(LouthKeyType[1]);
                                                if (RTimeStatus.SStartTime.Length == 8)
                                                    RTimeStatus.SStartTime = RTimeStatus.SStartTime + ":00";
                                                if (RTimeStatus.SStartTime == "") //如為空值則採用預設值
                                                    RTimeStatus.SStartTime = "00:01:00:00";
                                                if (RTimeStatus.SInteval == "") //如為空值則採用預設值
                                                    RTimeStatus.SInteval = "10";
                                                if (RTimeStatus.SDur == "") //如為空值則採用預設值
                                                    RTimeStatus.SDur = "30";
                                                NeedLoop = true;
                                            }
                                            else
                                            {
                                                if (LouthKeyType[0].IndexOf("口述影像版") > -1)  //口述影像版
                                                {
                                                    RTimeStatus.SStartTime = "00:00:00:00";  //5秒開始上1分鐘,間隔30分鐘
                                                    RTimeStatus.SInteval = "30";
                                                    RTimeStatus.SDur = "60";
                                                }
                                                else if (LouthKeyType[0].IndexOf("/") > -1) //雙語
                                                {
                                                    RTimeStatus.SStartTime = "00:00:20:00";  //1分開始上30秒,間隔10分鐘
                                                    RTimeStatus.SInteval = "10";
                                                    RTimeStatus.SDur = "90";
                                                }
                                                else if (LouthKeyType[0].IndexOf("(全程避片頭)") > -1) //全程避片頭
                                                {
                                                    RTimeStatus.SStartTime = "00:00:20:00";  //1分開始上30秒,間隔10分鐘
                                                    RTimeStatus.SInteval = "10";
                                                    RTimeStatus.SDur = "480";
                                                }
                                                else if (LouthKeyType[0].IndexOf("片名") == 0)  //片名
                                                {
                                                    RTimeStatus.SStartTime = "00:02:00:00";  //2分開始上8分鐘秒,間隔10分鐘
                                                    RTimeStatus.SInteval = "10";
                                                    RTimeStatus.SDur = "480";
                                                }
                                                else if (LouthKeyType[0].IndexOf("立體聲") == 0)  //立體聲
                                                {
                                                    RTimeStatus.SStartTime = "00:02:00:00";  //2分開始上30秒,間隔10分鐘
                                                    RTimeStatus.SInteval = "10";
                                                    RTimeStatus.SDur = "60";
                                                }
                                                else if (LouthKeyType[0].IndexOf("數位") == 0)  //數位
                                                {
                                                    RTimeStatus.SStartTime = "00:00:05:00";  //5秒開始上1分鐘,間隔30分鐘
                                                    RTimeStatus.SInteval = "30";
                                                    RTimeStatus.SDur = "60";
                                                }
                                                else
                                                {
                                                    RTimeStatus.SStartTime = "00:01:00:00";
                                                    RTimeStatus.SInteval = "10";
                                                    RTimeStatus.SDur = "30";
                                                }
                                                RTimeStatus.SSec = "0";
                                                NeedLoop = true;

                                            }
                                        }
                                        #endregion

                                        #region 接著,下週,明天,下次 將資料寫入REndPointInsert 表示最後幾秒要或不要
                                        if ((LouthKeyType[0].IndexOf("接著") > -1) || (LouthKeyType[0].IndexOf("下週") > -1) || (LouthKeyType[0].IndexOf("明天") > -1) || (LouthKeyType[0].IndexOf("下次") > -1))
                                        {

                                            //只要有最後幾秒要播出和播出長度
                                            string LastTimeInsert = "00:00:00:00"; //最後多少時間要插入Key
                                            if (LouthKey.IndexOf("#") > -1)  //#00:02:00,10,30 //表示有設定最後幾秒開始與長度
                                            {
                                                REndPointStatus = Trans1(LouthKeyType[1]);
                                                if (REndPointStatus.SEndPoint == "") //如為空值則採用預設值
                                                {
                                                    if (LouthKeyType[0].IndexOf("接著") == 0)
                                                    {
                                                        REndPointStatus.SEndPoint = "90";
                                                        REndPointStatus.SDur = "20";
                                                    }
                                                    else if ((LouthKeyType[0].IndexOf("下週") == 0) || (LouthKeyType[0].IndexOf("明天") == 0) || (LouthKeyType[0].IndexOf("下次") == 0))
                                                    {
                                                        REndPointStatus.SEndPoint = "40";
                                                        REndPointStatus.SDur = "20";
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (LouthKeyType[0].IndexOf("接著") == 0)
                                                {
                                                    REndPointStatus.SEndPoint = "90";
                                                    REndPointStatus.SDur = "20";
                                                }
                                                else if ((LouthKeyType[0].IndexOf("下週") == 0) || (LouthKeyType[0].IndexOf("明天") == 0) || (LouthKeyType[0].IndexOf("下次") == 0))
                                                {
                                                    REndPointStatus.SEndPoint = "40";
                                                    REndPointStatus.SDur = "20";
                                                }
                                            }

                                            //int IStartTimeFrame = 0;
                                            //IStartTimeFrame = MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - int.Parse(REndPointStatus.SEndPoint) * 30;
                                            //if (IStartTimeFrame > 0)
                                            //    PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(IStartTimeFrame);
                                            //else
                                            //    PlayTime = "00:00:00;00";

                                            //string Dur1;
                                            //長度為起始時間加五秒及最後幾分鐘不加,所以為長度減掉最後幾分鐘再減五秒

                                            //Dur1 = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(REndPointStatus.SDur));
                                            //myWriter.Write(WriteLouthByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur1, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3));

                                        }
                                        #endregion

                                        //開始將資料寫入LouthList

                                        #region 片名, 雙語 ,立體聲,數位同步播出 除了片名整段外都是有循環
                                        if ((LouthKeyType[0].IndexOf("/") > -1) || (LouthKeyType[0].IndexOf("片名") == 0) || (LouthKeyType[0].IndexOf("立體聲") == 0) || (LouthKeyType[0].IndexOf("數位") == 0) || (LouthKeyType[0].IndexOf("口述影像版") > -1)) //代表為片名 //有斜線代表為雙語
                                        {
                                            string LastTimeNoInsert = "00:02:00:00"; //最後多少時間不要插入Key
                                            //抓取資料完成開始加入LouthKey
                                            if ((LouthKeyType[0].IndexOf("整段") > -1) || (LouthKeyType[0].IndexOf("(全程避片頭)") > -1) || (LouthKeyType[0].IndexOf("口述影像版") > -1)) //整段
                                            {
                                                if (LouthKeyType[0].IndexOf("整段") > -1)
                                                {
                                                    TMP_RPT_DATE_LST.MEMO = "sTRANKEY";

                                                    TMP_RPT_DATE_LST.PLAYTIME = "";
                                                    TMP_RPT_DATE_LST.DUR = "";
                                                    LastTimeNoInsert = "00:00:00:00";
                                                    NeedLoop = false;
                                                }
                                                else if (LouthKeyType[0].IndexOf("(全程避片頭)") > -1)
                                                {
                                                    TMP_RPT_DATE_LST.MEMO = "sKEYER";

                                                    TMP_RPT_DATE_LST.PLAYTIME = "00:00:20:00";

                                                    if (bLast == false) //不是最後一段
                                                    {
                                                        if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) > 20 * 30)
                                                            TMP_RPT_DATE_LST.DUR = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - 20 * 30);
                                                        else
                                                            TMP_RPT_DATE_LST.DUR = "00:00:00:00";
                                                    }
                                                    else
                                                    {
                                                        if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) > 140 * 30)
                                                            TMP_RPT_DATE_LST.DUR = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - 140 * 30);
                                                        else
                                                            TMP_RPT_DATE_LST.DUR = "00:00:00:00";
                                                    }

                                                    LastTimeNoInsert = "00:00:00:00";
                                                    NeedLoop = false;
                                                }
                                                else if (LouthKeyType[0].IndexOf("口述影像版") > -1)
                                                {
                                                    TMP_RPT_DATE_LST.MEMO = "sKEYER";

                                                    TMP_RPT_DATE_LST.PLAYTIME = "00:00:00:00";
                                                    if (bLast == false) //不是最後一段
                                                    {
                                                        TMP_RPT_DATE_LST.MEMO = "sTRANKEY";
                                                        TMP_RPT_DATE_LST.PLAYTIME = "";
                                                        TMP_RPT_DATE_LST.DUR = "";
                                                        //TMP_RPT_DATE_LST.DUR = TemArrPromoList.FSDURATION;
                                                    }
                                                    else
                                                    {
                                                        if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) > 120 * 30)
                                                            TMP_RPT_DATE_LST.DUR = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - 120 * 30);
                                                        else
                                                            TMP_RPT_DATE_LST.DUR = "00:00:00:00";
                                                    }

                                                    LastTimeNoInsert = "00:00:00:00";
                                                    NeedLoop = false;
                                                }
                                                else
                                                {

                                                    TMP_RPT_DATE_LST.MEMO = "sKEYER";

                                                    TMP_RPT_DATE_LST.PLAYTIME = "";
                                                    TMP_RPT_DATE_LST.DUR = "";
                                                    LastTimeNoInsert = "00:00:00:00";
                                                    NeedLoop = false;
                                                }


                                            }
                                            else //如果不為整段
                                            {
                                                NeedLoop = true;

                                                TMP_RPT_DATE_LST.MEMO = "sKEYER";
                                                if (LouthKey.IndexOf("#") > -1)  //#00:02:00,10,30 //表示有設定起始時間,間隔,長度
                                                { }
                                                else
                                                {
                                                    if ((TemArrPromoList.FSCHANNEL_ID == "01") || (TemArrPromoList.FSCHANNEL_ID == "02") || (TemArrPromoList.FSCHANNEL_ID == "51"))
                                                        LastTimeNoInsert = "00:02:00:00";
                                                    else
                                                        LastTimeNoInsert = "00:00:30:00";
                                                }
                                               
                                            }



                                            #region 加入Louth Ley
                                            if (NeedLoop == true) //加入多筆louth key Loop
                                            {
                                                string ThisPlayTime = RTimeStatus.SStartTime;
                                                if (bLast == false) //不是最後一段只要檢查Key起始時間+長度不要大於節目時長即可
                                                {
                                                    while (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(ThisPlayTime) + int.Parse(RTimeStatus.SDur) * 30 < MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION))
                                                    {
                                                        TMP_RPT_DATE_LST.PLAYTIME = ThisPlayTime;
                                                        ThisPlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(ThisPlayTime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe("00:" + String.Format("{0:00}", int.Parse(RTimeStatus.SInteval)) + ":00:00") + int.Parse(RTimeStatus.SSec) * 30);
                                                        // string Dur1;
                                                        TMP_RPT_DATE_LST.DUR = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(RTimeStatus.SDur));
                                                        TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                        RowNO = RowNO + 1;
                                                        INSERT_RPT_DATE_LST(TMP_RPT_DATE_LST);

                                                    }
                                                }
                                                else //最後一段要檢查Key起始時間+3分鐘不要大於節目時長即可
                                                {
                                                    while (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(ThisPlayTime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(LastTimeNoInsert) + int.Parse(RTimeStatus.SDur) * 30 < MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION))
                                                    {
                                                        TMP_RPT_DATE_LST.PLAYTIME = ThisPlayTime;
                                                        ThisPlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(ThisPlayTime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe("00:" + String.Format("{0:00}", int.Parse(RTimeStatus.SInteval)) + ":00:00") + int.Parse(RTimeStatus.SSec) * 30);
                                                        string Dur1;
                                                        Dur1 = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(RTimeStatus.SDur));
                                                        TMP_RPT_DATE_LST.DUR = Dur1;
                                                        TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                        RowNO = RowNO + 1;
                                                        INSERT_RPT_DATE_LST(TMP_RPT_DATE_LST);
                                                    }
                                                }
                                            }
                                            else
                                            {



                                                TMP_RPT_DATE_LST.PLAYTIME = RTimeStatus.SStartTime;
                                                string Dur1;
                                                Dur1 = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(RTimeStatus.SDur));

                                                if (TMP_RPT_DATE_LST.MEMO == "sTRANKEY")
                                                {
                                                    TMP_RPT_DATE_LST.PLAYTIME = "";
                                                    TMP_RPT_DATE_LST.DUR = "";
                                                }
                                                TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                RowNO = RowNO + 1;
                                                INSERT_RPT_DATE_LST(TMP_RPT_DATE_LST);
                                            }
                                            #endregion

                                        }
                                        #endregion

                                        
                                        #region 重播, 直播 ,時間,Live,錄影轉播,Logo,都是整段
                                        if ((LouthKeyType[0].IndexOf("重播") == 0) || (LouthKeyType[0].IndexOf("直播") == 0) || (LouthKeyType[0].IndexOf("時間") == 0) || (LouthKeyType[0].ToUpper().IndexOf("LIVE") == 0) || (LouthKeyType[0].ToUpper().IndexOf("LOGO") == 0) || (LouthKeyType[0].IndexOf("錄影轉播") == 0)) //代表為片名 //有斜線代表為雙語
                                        {
                                            TMP_RPT_DATE_LST.MEMO = "sTRANKEY";
                                            //striType = "132";
                                            TMP_RPT_DATE_LST.PLAYTIME = "";
                                            TMP_RPT_DATE_LST.DUR = "";
                                            //if (LouthKeyType[0].IndexOf("時間") > -1)
                                            //{
                                            //    EF2 = "0";  //在節目時如果為1 則會顯示LOSE
                                            //    EF3 = "2";

                                            //    //VideoID = "sKEYER";
                                            //    //striType = "131";
                                            //    //PlayTime = "00:01:00:00";
                                            //    //Dur = "00:00:30:00";
                                            //    //SOM = "00:00:00:00";
                                            //    QF2 = "0"; 
                                            //    QF3 = "16";
                                            //}
                                            if (LouthKeyType[0].ToUpper().IndexOf("LOGO") == 0)
                                            {

                                                TMP_RPT_DATE_LST.PLAYTIME = "00:00:00:00";
                                                TMP_RPT_DATE_LST.DUR = "00:00:10:00";
                                                TMP_RPT_DATE_LST.MEMO = "sKEYER(EH)";
                                            }
                                            //PlayTime = RTimeStatus.SStartTime;
                                            string Dur1;
                                            //Dur1 = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(RTimeStatus.SDur));

                                            //TMP_RPT_DATE_LST.DUR = TemArrPromoList.FSDURATION;
                                            TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                            RowNO = RowNO + 1;
                                            INSERT_RPT_DATE_LST(TMP_RPT_DATE_LST);

                                        }
                                        #endregion

                                        #region 接著,下週,明天,下次
                                        if ((LouthKeyType[0].IndexOf("接著") == 0) || (LouthKeyType[0].IndexOf("下週") == 0) || (LouthKeyType[0].IndexOf("明天") == 0) || (LouthKeyType[0].IndexOf("下次") == 0))
                                        {
                                            //striType = "131";
                                            //只要有最後幾秒要播出和播出長度
                                            string LastTimeInsert = "00:00:00:00"; //最後多少時間要插入Key
                                            //EventCOntrol = "";
                                            TMP_RPT_DATE_LST.PROGNAME = NewLouth.FSNAME;
                                            TMP_RPT_DATE_LST.MEMO = "sKEYER";

                                            int IStartTimeFrame = 0;
                                            IStartTimeFrame = MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - int.Parse(REndPointStatus.SEndPoint) * 30;
                                            if (IStartTimeFrame > 0)
                                                TMP_RPT_DATE_LST.PLAYTIME = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(IStartTimeFrame);
                                            else
                                                TMP_RPT_DATE_LST.PLAYTIME = "00:00:00;00";

                                            string Dur1;
                                            //長度為起始時間加五秒及最後幾分鐘不加,所以為長度減掉最後幾分鐘再減五秒

                                            Dur1 = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(REndPointStatus.SDur));
                                            TMP_RPT_DATE_LST.DUR = Dur1;
                                            TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                            RowNO = RowNO + 1;
                                            INSERT_RPT_DATE_LST(TMP_RPT_DATE_LST);
                                            //}

                                        }  //if ((LouthKeyType[0].IndexOf(" 接著,下週,明天,下次") > -1)
                                        #endregion
                                    }
                                }
                            }
                        } //  foreach (string LouthKey in LouthKeys)                     
                    } //if (TemArrPromoList.FSMEMO != "")

                    //} //if FCPLAY_Type="G"
                }



            }
            catch (InvalidCastException e)
            {
                return "";
            }

            return g.ToString();
        }


        [WebMethod()]
        public string PrintHDLst(List<ArrPromoList> InList, string USERNAME)
        {

            PGM_ARR_PROMOAddSOM.Clear();
            foreach (ArrPromoList TemArrPromoList in InList)
            {
                PGM_ARR_PROMOAddSOM.Add(AddSOMToArrPromoList(TemArrPromoList));
            }

            string TempChannelID = "";
            string TempDate = "";

            //INSERT_RPT_DATE_LST
            Guid g;
            g = Guid.NewGuid();
            try
            {
                int WorkRow = 0;
                bool beFirst = false; //是否為第一段
                bool bLast = false;  //是否為最後一段
                string PriorDataType = "";
                //逐一分析每筆資料的FSMEMO並將其寫入報表中
                int RowNO = 1;
                foreach (ArrPromoListAddSOM TemArrPromoList in PGM_ARR_PROMOAddSOM)
                {
                    TempChannelID = TemArrPromoList.FSCHANNEL_ID;
                    TempDate = TemArrPromoList.FDDATE;
                    RPT_DATE_LST TMP_RPT_DATE_LST = new RPT_DATE_LST();
                    TMP_RPT_DATE_LST.QUERY_KEY = g.ToString();

                    TMP_RPT_DATE_LST.QUERY_KEY_SUBID = "";
                    TMP_RPT_DATE_LST.QUERY_BY = USERNAME;

                    TMP_RPT_DATE_LST.NO = RowNO.ToString();
                    RowNO = RowNO + 1;

                    TMP_RPT_DATE_LST.VIDEOID = TemArrPromoList.FSVIDEO_ID;
                    TMP_RPT_DATE_LST.PROGNAME = TemArrPromoList.FSNAME;
                    if (int.Parse(TemArrPromoList.FSPLAY_TIME.Substring(0, 2)) >= 24)
                        TMP_RPT_DATE_LST.PLAYTIME = (int.Parse(TemArrPromoList.FSPLAY_TIME.Substring(0, 2)) - 24).ToString() + TemArrPromoList.FSPLAY_TIME.Substring(2, 9);
                    else
                        TMP_RPT_DATE_LST.PLAYTIME = TemArrPromoList.FSPLAY_TIME;
                    TMP_RPT_DATE_LST.DUR = TemArrPromoList.FSDURATION;
                    TMP_RPT_DATE_LST.PROGID = TemArrPromoList.FSPROG_ID;
                    TMP_RPT_DATE_LST.SEQNO = TemArrPromoList.FNSEQNO;
                    TMP_RPT_DATE_LST.DATATYPE = TemArrPromoList.FCTYPE;
                    TMP_RPT_DATE_LST.PROMOID = TemArrPromoList.FSPROMO_ID;
                    TMP_RPT_DATE_LST.FSSIGNAL = TemArrPromoList.FSSIGNAL;
                    TMP_RPT_DATE_LST.PLAYDATE = TemArrPromoList.FDDATE;
                    TMP_RPT_DATE_LST.CHANNELNAME = TemArrPromoList.FSCHANNEL_NAME;
                    TMP_RPT_DATE_LST.SUBNO = TemArrPromoList.FNSUB_NO;
                    TMP_RPT_DATE_LST.PLAYDATE = TemArrPromoList.FDDATE;
                    TMP_RPT_DATE_LST.PROGSEG = TemArrPromoList.FNBREAK_NO;
                    TMP_RPT_DATE_LST.PROGEPISODE = TemArrPromoList.FNEPISODE;
                    TMP_RPT_DATE_LST.FSRAN = "";
                    TMP_RPT_DATE_LST.FNLIVE = TemArrPromoList.FNLIVE;
                    TMP_RPT_DATE_LST.FILENO = TemArrPromoList.FSFILE_NO;
                    //string SOM = TemArrPromoList.FSSOM;

                    if (TemArrPromoList.FCTIME_TYPE == "O")
                        TMP_RPT_DATE_LST.TIMETYPE = "AO";
                    //else if ((TemArrPromoList.FCTYPE=="G") || (TemArrPromoList.FNSUB_NO=="1"))
                    else if ((TemArrPromoList.FCTYPE == "G") || (TemArrPromoList.FCTYPE == "P" && TemArrPromoList.FNBREAK_NO != "0" && TemArrPromoList.FNSUB_NO == "1"))

                        TMP_RPT_DATE_LST.TIMETYPE = "AN";
                    //else if ((TemArrPromoList.FNBREAK_NO=="1" && TemArrPromoList.FCTYPE=="G") || (TemArrPromoList.FNSUB_NO=="1"))
                    else
                        TMP_RPT_DATE_LST.TIMETYPE = "A";

                    //如果前一筆資料為節目,此筆資料為短帶,則要設定為 AN
                    if (PriorDataType == "G" && TemArrPromoList.FCTYPE == "P")
                    {
                        TMP_RPT_DATE_LST.TIMETYPE = "AN";
                    }
                    //用完後將此筆資料的類型設定進PriorDataType,當下一筆時可以使用
                    PriorDataType = TemArrPromoList.FCTYPE;

                    if ((TemArrPromoList.FCTYPE == "G") && (TemArrPromoList.FNLIVE == "1")) //Live節目
                    {
                        if (TemArrPromoList.FCTIME_TYPE == "O")
                            TMP_RPT_DATE_LST.TIMETYPE = "AUNO";
                        else
                            TMP_RPT_DATE_LST.TIMETYPE = "AUN";
                    }

                    if ((TemArrPromoList.FCTYPE == "P") && (TemArrPromoList.FSSIGNAL != "PGM") && (TemArrPromoList.FSSIGNAL != "COMM") && (TemArrPromoList.FSSIGNAL != "")) //Live PROMO
                    {
                        if (TemArrPromoList.FCTIME_TYPE == "O")
                            TMP_RPT_DATE_LST.TIMETYPE = "AUNO";
                        else
                            TMP_RPT_DATE_LST.TIMETYPE = "AUN";
                    }
                    if (TemArrPromoList.FNLIVE == "1")  //如果為Live則要將訊號源寫入VIdeoID中
                    {
                        TMP_RPT_DATE_LST.VIDEOID = TemArrPromoList.FSSIGNAL;
                    }
                    TMP_RPT_DATE_LST.TIMETYPE = TemArrPromoList.FCTIME_TYPE;
                    TMP_RPT_DATE_LST.MEMO = "";
                    //TMP_RPT_DATE_LST.MEMO = TemArrPromoList.FSMEMO;
                    if (TMP_RPT_DATE_LST.DATATYPE != "G")
                        TMP_RPT_DATE_LST.PROGSEG = "";
                    if (TemArrPromoList.FSMEMO.ToUpper().IndexOf("LOGO") > -1) //Logo那節目要+Lose
                        TMP_RPT_DATE_LST.MEMO = TMP_RPT_DATE_LST.MEMO + "(LOSE)";
                    //if (TemArrPromoList.FSMEMO.IndexOf("LOGO") > -1) //Logo那節目要+Lose
                    //    TMP_RPT_DATE_LST.MEMO = TMP_RPT_DATE_LST.MEMO+ "+LOSE";
                    //if (TemArrPromoList.FSPROG_ID == "2004090" && TemArrPromoList.FCTYPE == "G") //訊號測試節目要+Lose
                    //    TMP_RPT_DATE_LST.MEMO = TMP_RPT_DATE_LST.MEMO + "+LOSE";
                    TMP_RPT_DATE_LST.FSSOM = TemArrPromoList.FSSOM;
                    INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);

                    if (TemArrPromoList.FCTYPE == "G")
                    {
                        beFirst = false; //是否為第一段
                        bLast = false;  //是否為最後一段
                        if (WorkRow < PGM_ARR_PROMOAddSOM.Count - 1) //檢查此資料狀態,是否為第一段 和 是否為最後一段 和 是否只有一段
                        {
                            if (TemArrPromoList.FNBREAK_NO == "1")
                                beFirst = true;
                            int NextProgRow = WorkRow + 1;
                            while (NextProgRow < PGM_ARR_PROMOAddSOM.Count - 1 && PGM_ARR_PROMOAddSOM[NextProgRow].FCTYPE == "P")
                                NextProgRow = NextProgRow + 1;

                            if (PGM_ARR_PROMOAddSOM[NextProgRow].FCTYPE == "G")
                            {
                                if (PGM_ARR_PROMOAddSOM[NextProgRow].FSPROG_ID != TemArrPromoList.FSPROG_ID || PGM_ARR_PROMOAddSOM[NextProgRow].FNSEQNO != TemArrPromoList.FNSEQNO)
                                    bLast = true; //表示最後一段
                            }
                            else
                            {
                                bLast = true; //表示最後一段
                            }
                        }
                    }
                    WorkRow = WorkRow + 1;
                    //開始分析Memo的Louth Key
                    if (TemArrPromoList.FSMEMO != "")
                    {
                        string[] LouthKeys = TemArrPromoList.FSMEMO.Split(new Char[] { '+' });

                        foreach (string LouthKey in LouthKeys)  //每一個+ 號表示一個Key
                        {
                            if (LouthKey.Trim() != "")
                            {

                                TMP_RPT_DATE_LST.PROGSEG = "";

                                TMP_RPT_DATE_LST.NO = "";
                                TMP_RPT_DATE_LST.TIMETYPE = "";
                                TMP_RPT_DATE_LST.MEMO = "";
                                // 時間 ,重播,直播  為 sTRANKEY 其他為 sKEYER

                                //VideoID = "sKEYER";
                                string[] LouthKeyType = LouthKey.Split(new Char[] { '#' }); //將LouthKey 依照#分開,如果有 #代表有輸入時間 Ex:中/英#00:02:00,10,30
                                if (LouthKeyType[0] != "") //如果有設定LouthKey Ex:中/英#00:02:00,10,30
                                {
                                    #region 查詢Louth的資料 並將其加入 NewLouth
                                    LouthTableData NewLouth = new LouthTableData();

                                    if (LouthKeyType[0].IndexOf("片名") == 0)
                                    {
                                        //NewLouth = GetLouthData("片名-" + TemArrPromoList.FSNAME, TempChannelID);

                                        NewLouth = GetLouthData(LouthKeyType[0], TempChannelID);
                                        if (LouthKeyType[0].IndexOf("(整段)") > -1)
                                            NewLouth = GetLouthData(LouthKeyType[0].Substring(3, (LouthKeyType[0].Length) - 7), TempChannelID);
                                        else if (LouthKeyType[0].IndexOf("(全程避片頭)") > -1)
                                            NewLouth = GetLouthData(LouthKeyType[0].Substring(3, (LouthKeyType[0].Length) - 10), TempChannelID);
                                        else
                                            NewLouth = GetLouthData(LouthKeyType[0].Substring(3, (LouthKeyType[0].Length) - 3), TempChannelID);
                                    }
                                    else
                                        NewLouth = GetLouthData(LouthKeyType[0], TempChannelID); //取得Louth Table的資料
                                    if (NewLouth.FSNO != "")
                                    {
                                        //QF4 = NewLouth.FSLayel;
                                        TMP_RPT_DATE_LST.VIDEOID = NewLouth.FSNO.Substring(1, NewLouth.FSNO.Length - 1);
                                        TMP_RPT_DATE_LST.DATATYPE = "K";
                                        TMP_RPT_DATE_LST.FSSIGNAL = NewLouth.FSLayel;
                                        // QF1 = NewLouth.FSNO.Substring(1, NewLouth.FSNO.Length - 1);

                                        if (LouthKeyType[0].IndexOf("時間") == 0 || LouthKeyType[0].IndexOf("重播") == 0 || LouthKeyType[0].IndexOf("直播") == 0)
                                        {

                                            TMP_RPT_DATE_LST.MEMO = "sTRANKEY";
                                            TMP_RPT_DATE_LST.DUR = "";
                                            TMP_RPT_DATE_LST.PLAYTIME = "";


                                        }
                                        else
                                        {
                                            if (LouthKeyType[0].ToUpper().IndexOf("LOGO") == 0)
                                                TMP_RPT_DATE_LST.MEMO = "sKEYER(EH)";
                                            else
                                                TMP_RPT_DATE_LST.MEMO = "sKEYER";
                                            //SOM = "";
                                            TMP_RPT_DATE_LST.PLAYTIME = "00:00:00:00";
                                            TMP_RPT_DATE_LST.DUR = TemArrPromoList.FSDURATION;
                                        }
                                        TMP_RPT_DATE_LST.PROGNAME = NewLouth.FSNAME;
                                    #endregion
                                        bool NeedLoop = false;    //是否要做循環
                                        TimeStatus RTimeStatus = new TimeStatus();
                                        SEndPointStatus REndPointStatus = new SEndPointStatus();
                                        //設定播出規則,沒有的話則採用預設規則  並將資料加入RTimeStatus 只有雙語 片名 立體聲 數位同步播出有 值
                                        //接著跟繼續收看
                                        #region 設定播出規則,沒有的話則採用預設規則  並將資料加入RTimeStatus
                                        if ((LouthKeyType[0].IndexOf("/") > -1) || (LouthKeyType[0].IndexOf("片名") == 0) || (LouthKeyType[0].IndexOf("立體聲") == 0) || (LouthKeyType[0].IndexOf("數位") == 0) || (LouthKeyType[0].IndexOf("口述影像版") > -1)) //代表為片名 //有斜線代表為雙語
                                        {
                                            if (LouthKey.IndexOf("#") > -1)  //#00:02:00,10,30 //表示有設定起始時間,間隔,長度
                                            {
                                                RTimeStatus = Trans(LouthKeyType[1]);
                                                if (RTimeStatus.SStartTime.Length == 8)
                                                    RTimeStatus.SStartTime = RTimeStatus.SStartTime + ":00";
                                                if (RTimeStatus.SStartTime == "") //如為空值則採用預設值
                                                    RTimeStatus.SStartTime = "00:01:00:00";
                                                if (RTimeStatus.SInteval == "") //如為空值則採用預設值
                                                    RTimeStatus.SInteval = "10";
                                                if (RTimeStatus.SDur == "") //如為空值則採用預設值
                                                    RTimeStatus.SDur = "30";
                                                NeedLoop = true;
                                            }
                                            else
                                            {
                                                if (LouthKeyType[0].IndexOf("口述影像版") > -1)  //口述影像版
                                                {
                                                    RTimeStatus.SStartTime = "00:00:00:00";  //5秒開始上1分鐘,間隔30分鐘
                                                    RTimeStatus.SInteval = "30";
                                                    RTimeStatus.SDur = "60";
                                                }
                                                else if (LouthKeyType[0].IndexOf("/") > -1) //雙語
                                                {
                                                    RTimeStatus.SStartTime = "00:00:20:00";  //1分開始上30秒,間隔10分鐘
                                                    RTimeStatus.SInteval = "10";
                                                    RTimeStatus.SDur = "90";
                                                }
                                                else if (LouthKeyType[0].IndexOf("(全程避片頭)") > -1) //全程避片頭
                                                {
                                                    RTimeStatus.SStartTime = "00:00:20:00";  //1分開始上30秒,間隔10分鐘
                                                    RTimeStatus.SInteval = "10";
                                                    RTimeStatus.SDur = "480";
                                                }
                                                else if (LouthKeyType[0].IndexOf("片名") == 0)  //片名
                                                {
                                                    RTimeStatus.SStartTime = "00:02:00:00";  //2分開始上8分鐘秒,間隔10分鐘
                                                    RTimeStatus.SInteval = "10";
                                                    RTimeStatus.SDur = "480";
                                                }
                                                else if (LouthKeyType[0].IndexOf("整段") > -1)
                                                {
                                                    RTimeStatus.SStartTime = "00:00:00:00";  //2分開始上8分鐘秒,間隔10分鐘
                                                    RTimeStatus.SInteval = "0";
                                                    RTimeStatus.SDur = "0";
                                                }
                                                else if (LouthKeyType[0].IndexOf("立體聲") == 0)  //立體聲
                                                {
                                                    RTimeStatus.SStartTime = "00:02:00:00";  //2分開始上30秒,間隔10分鐘
                                                    RTimeStatus.SInteval = "10";
                                                    RTimeStatus.SDur = "60";
                                                }
                                                else if (LouthKeyType[0].IndexOf("數位") == 0)  //數位
                                                {
                                                    RTimeStatus.SStartTime = "00:00:05:00";  //5秒開始上1分鐘,間隔30分鐘
                                                    RTimeStatus.SInteval = "30";
                                                    RTimeStatus.SDur = "60";
                                                }
                                                else
                                                {
                                                    RTimeStatus.SStartTime = "00:01:00:00";
                                                    RTimeStatus.SInteval = "10";
                                                    RTimeStatus.SDur = "30";
                                                }
                                                RTimeStatus.SSec = "0";
                                                NeedLoop = true;

                                            }
                                        }
                                        #endregion

                                        #region 接著,下週,明天,下次 將資料寫入REndPointInsert 表示最後幾秒要或不要
                                        if ((LouthKeyType[0].IndexOf("接著") > -1) || (LouthKeyType[0].IndexOf("下週") > -1) || (LouthKeyType[0].IndexOf("明天") > -1) || (LouthKeyType[0].IndexOf("下次") > -1))
                                        {

                                            //只要有最後幾秒要播出和播出長度
                                            string LastTimeInsert = "00:00:00:00"; //最後多少時間要插入Key
                                            if (LouthKey.IndexOf("#") > -1)  //#00:02:00,10,30 //表示有設定最後幾秒開始與長度
                                            {
                                                REndPointStatus = Trans1(LouthKeyType[1]);
                                                if (REndPointStatus.SEndPoint == "") //如為空值則採用預設值
                                                {
                                                    if (LouthKeyType[0].IndexOf("接著") == 0)
                                                    {
                                                        REndPointStatus.SEndPoint = "90";
                                                        REndPointStatus.SDur = "20";
                                                    }
                                                    else if ((LouthKeyType[0].IndexOf("下週") == 0) || (LouthKeyType[0].IndexOf("明天") == 0) || (LouthKeyType[0].IndexOf("下次") == 0))
                                                    {
                                                        REndPointStatus.SEndPoint = "40";
                                                        REndPointStatus.SDur = "20";
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (LouthKeyType[0].IndexOf("接著") == 0)
                                                {
                                                    REndPointStatus.SEndPoint = "90";
                                                    REndPointStatus.SDur = "20";
                                                }
                                                else if ((LouthKeyType[0].IndexOf("下週") == 0) || (LouthKeyType[0].IndexOf("明天") == 0) || (LouthKeyType[0].IndexOf("下次") == 0))
                                                {
                                                    REndPointStatus.SEndPoint = "40";
                                                    REndPointStatus.SDur = "20";
                                                }
                                            }

                                            //int IStartTimeFrame = 0;
                                            //IStartTimeFrame = MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - int.Parse(REndPointStatus.SEndPoint) * 30;
                                            //if (IStartTimeFrame > 0)
                                            //    PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(IStartTimeFrame);
                                            //else
                                            //    PlayTime = "00:00:00;00";

                                            //string Dur1;
                                            //長度為起始時間加五秒及最後幾分鐘不加,所以為長度減掉最後幾分鐘再減五秒

                                            //Dur1 = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(REndPointStatus.SDur));
                                            //myWriter.Write(WriteLouthByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur1, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3));

                                        }
                                        #endregion

                                        //開始將資料寫入LouthList

                                        #region 片名, 雙語 ,立體聲,數位同步播出 除了片名整段外都是有循環
                                        if ((LouthKeyType[0].IndexOf("/") > -1) || (LouthKeyType[0].IndexOf("片名") == 0) || (LouthKeyType[0].IndexOf("立體聲") == 0) || (LouthKeyType[0].IndexOf("數位") == 0) || (LouthKeyType[0].IndexOf("口述影像版") > -1)) //代表為片名 //有斜線代表為雙語
                                        {
                                            string LastTimeNoInsert = "00:02:00:00"; //最後多少時間不要插入Key
                                            //抓取資料完成開始加入LouthKey
                                            if ((LouthKeyType[0].IndexOf("整段") > -1) || (LouthKeyType[0].IndexOf("(全程避片頭)") > -1) || (LouthKeyType[0].IndexOf("口述影像版") > -1)) //整段
                                            {
                                                if (LouthKeyType[0].IndexOf("整段") > -1)
                                                {
                                                    TMP_RPT_DATE_LST.MEMO = "sTRANKEY";

                                                    TMP_RPT_DATE_LST.PLAYTIME = "00:00:00:00";
                                                    TMP_RPT_DATE_LST.DUR = TemArrPromoList.FSDURATION;
                                                    LastTimeNoInsert = "00:00:00:00";
                                                    NeedLoop = false;
                                                }
                                                else if (LouthKeyType[0].IndexOf("(全程避片頭)") > -1)
                                                {
                                                    TMP_RPT_DATE_LST.MEMO = "sKEYER";

                                                    TMP_RPT_DATE_LST.PLAYTIME = "00:00:20:00";

                                                    if (bLast == false) //不是最後一段
                                                    {
                                                        if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) > 20 * 30)
                                                            TMP_RPT_DATE_LST.DUR = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - 20 * 30);
                                                        else
                                                            TMP_RPT_DATE_LST.DUR = "00:00:00:00";
                                                    }
                                                    else
                                                    {
                                                        if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) > 140 * 30)
                                                            TMP_RPT_DATE_LST.DUR = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - 140 * 30);
                                                        else
                                                            TMP_RPT_DATE_LST.DUR = "00:00:00:00";
                                                    }

                                                    LastTimeNoInsert = "00:00:00:00";
                                                    NeedLoop = false;
                                                }
                                                else if (LouthKeyType[0].IndexOf("口述影像版") > -1)
                                                {
                                                    TMP_RPT_DATE_LST.MEMO = "sKEYER";

                                                    TMP_RPT_DATE_LST.PLAYTIME = "00:00:00:00";
                                                    if (bLast == false) //不是最後一段
                                                    {
                                                        TMP_RPT_DATE_LST.MEMO = "sTRANKEY";
                                                        TMP_RPT_DATE_LST.PLAYTIME = "";
                                                        TMP_RPT_DATE_LST.DUR = "";
                                                        //TMP_RPT_DATE_LST.DUR = TemArrPromoList.FSDURATION;
                                                    }
                                                    else
                                                    {
                                                        if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) > 120 * 30)
                                                            TMP_RPT_DATE_LST.DUR = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - 120 * 30);
                                                        else
                                                            TMP_RPT_DATE_LST.DUR = "00:00:00:00";
                                                    }

                                                    LastTimeNoInsert = "00:00:00:00";
                                                    NeedLoop = false;
                                                }
                                                else
                                                {

                                                    TMP_RPT_DATE_LST.MEMO = "sKEYER";

                                                    TMP_RPT_DATE_LST.PLAYTIME = "";
                                                    TMP_RPT_DATE_LST.DUR = "";
                                                    LastTimeNoInsert = "00:00:00:00";
                                                    NeedLoop = false;
                                                }


                                            }
                                            else //如果不為整段
                                            {
                                                NeedLoop = true;

                                                TMP_RPT_DATE_LST.MEMO = "sKEYER";
                                                if (TemArrPromoList.FSCHANNEL_ID == "02") //Dimo頻道
                                                {
                                                    if (LouthKey.IndexOf("#") > -1)  //#00:02:00,10,30 //表示有設定起始時間,間隔,長度
                                                    { }
                                                    else
                                                    {
                                                        if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) >= MAM_PTS_DLL.TimeCodeCalc.timecodetoframe("00:05:00:00")) //如果長度大於5分鐘
                                                        {
                                                            TMP_RPT_DATE_LST.PLAYTIME = "00:00:59:28";
                                                            TMP_RPT_DATE_LST.DUR = "00:02:00:00";
                                                            LastTimeNoInsert = "00:00:00:00";
                                                            NeedLoop = false;
                                                        }
                                                        else //如果長度小於5分鐘
                                                        {
                                                            TMP_RPT_DATE_LST.PLAYTIME = "00:00:10:00";
                                                            TMP_RPT_DATE_LST.DUR = "00:00:20:00";
                                                            LastTimeNoInsert = "00:00:00:00";
                                                            NeedLoop = false;
                                                        }

                                                    }
                                                }
                                                else //如果不為Dimo
                                                {
                                                    if (LouthKey.IndexOf("#") > -1)  //#00:02:00,10,30 //表示有設定起始時間,間隔,長度
                                                    { }
                                                    else
                                                    {
                                                        if ((TemArrPromoList.FSCHANNEL_ID == "01") || (TemArrPromoList.FSCHANNEL_ID == "51"))
                                                            LastTimeNoInsert = "00:02:00:00";
                                                        else
                                                            LastTimeNoInsert = "00:00:30:00";
                                                    }
                                                }
                                            }



                                            #region 加入Louth Ley
                                            if (NeedLoop == true) //加入多筆louth key Loop
                                            {
                                                string ThisPlayTime = RTimeStatus.SStartTime;
                                                string PlayTime;
                                                if (bLast == false) //不是最後一段只要檢查Key起始時間+長度不要大於節目時長即可
                                                {
                                                    string QF4 = TMP_RPT_DATE_LST.FSSIGNAL;
                                                    string QF1 = TMP_RPT_DATE_LST.VIDEOID;
                                                    TMP_RPT_DATE_LST.VIDEOID = "L" + TMP_RPT_DATE_LST.FSSIGNAL + " " + TMP_RPT_DATE_LST.VIDEOID;
                                                    while (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(ThisPlayTime) + int.Parse(RTimeStatus.SDur) * 30 < MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION))
                                                    {
                                                        //HD格式的VideoID為 L1 100

                                                            //HD格式的VideoID為 L1 100
                                                            PlayTime = ThisPlayTime;
                                                            ThisPlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(ThisPlayTime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe("00:" + String.Format("{0:00}", int.Parse(RTimeStatus.SInteval)) + ":00:00"));
                                                            string Dur1;
                                                            Dur1 = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(RTimeStatus.SDur));

                                                            //HD格式的VideoID為 L1 100
                                                            if (LouthKeyType[0].ToUpper().IndexOf("LOGO") == 0)
                                                            {
                                                                PlayTime = "";
                                                                Dur1 = "";
                                                            }

                                                            string VideoID;
                                                            if (QF4 == "3")
                                                            {
                                                                //須給上下架時間,長度都為兩秒,放在第四層的應當都為整段的Key,所以起始時間為0.
                                                                if (QF1.Substring(0, 1) == "0")
                                                                    VideoID = "ST " + QF1.Substring(1, 2);
                                                                else
                                                                    VideoID = "ST " + QF1;
                                                                if (PlayTime == "")
                                                                    PlayTime = "00:00:00:00";
                                                                if (Dur1 == "")
                                                                    Dur1 = TemArrPromoList.FSDURATION;
                                                                if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) < 15 * 30)
                                                                    PlayTime = "00:00:15:00";
                                                                string RealDur = Dur1;
                                                                Dur1 = "00:00:02:00";

                                                                TMP_RPT_DATE_LST.PLAYTIME = PlayTime;
                                                                TMP_RPT_DATE_LST.DUR = "00:00:02:00";
                                                                TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                                TMP_RPT_DATE_LST.FSSOM = PlayTime;
                                                                TMP_RPT_DATE_LST.VIDEOID = VideoID;
                                                                INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);
                                                                RowNO = RowNO + 1;
                                                                VideoID = "ST 5" + QF1;
                                                                TMP_RPT_DATE_LST.VIDEOID = VideoID;

                                                                PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) + int.Parse(RTimeStatus.SDur) * 30);//MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(RealDur));
                                                                if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime)+ 15 *30 > MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION))
                                                                    PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - 15 * 30);
                                                                Dur1 = "00:00:02:00";
                                                                TMP_RPT_DATE_LST.PLAYTIME = PlayTime;
                                                                TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                                TMP_RPT_DATE_LST.FSSOM = PlayTime;
                                                                INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);
                                                                RowNO = RowNO + 1;
                                                            }
                                                            else
                                                            {


                                                                TMP_RPT_DATE_LST.PLAYTIME = PlayTime;
                                                                //ThisPlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(ThisPlayTime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe("00:" + String.Format("{0:00}", int.Parse(RTimeStatus.SInteval)) + ":00:00") + int.Parse(RTimeStatus.SSec) * 30);
                                                                // string Dur1;
                                                                TMP_RPT_DATE_LST.DUR = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(RTimeStatus.SDur));
                                                                TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                                TMP_RPT_DATE_LST.FSSOM = TMP_RPT_DATE_LST.PLAYTIME; // ThisPlayTime;
                                                                TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                                //TMP_RPT_DATE_LST.VIDEOID = "L" + TMP_RPT_DATE_LST.FSSIGNAL + " " + TMP_RPT_DATE_LST.VIDEOID;
                                                                RowNO = RowNO + 1;
                                                                INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);

                                                            }



                                                        }
                                          
                                                }
                                                else //最後一段要檢查Key起始時間+3分鐘不要大於節目時長即可 //if (bLast == true) 如果是最後一段 
                                                {
                                                    string QF4 = TMP_RPT_DATE_LST.FSSIGNAL;
                                                    string QF1 = TMP_RPT_DATE_LST.VIDEOID;
                                                    TMP_RPT_DATE_LST.VIDEOID = "L" + TMP_RPT_DATE_LST.FSSIGNAL + " " + TMP_RPT_DATE_LST.VIDEOID;
                                                    while (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(ThisPlayTime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(LastTimeNoInsert) + int.Parse(RTimeStatus.SDur) * 30 < MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION))
                                                    {
                                                       

                                                        //HD格式的VideoID為 L1 100
                                                        PlayTime = ThisPlayTime;
                                                        ThisPlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(ThisPlayTime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe("00:" + String.Format("{0:00}", int.Parse(RTimeStatus.SInteval)) + ":00:00"));
                                                        string Dur1;
                                                        Dur1 = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(RTimeStatus.SDur));

                                                        //HD格式的VideoID為 L1 100
                                                        if (LouthKeyType[0].ToUpper().IndexOf("LOGO") == 0)
                                                        {
                                                            PlayTime = "";
                                                            Dur1 = "";
                                                        }

                                                        string VideoID;
                                                        if (QF4 == "3")
                                                        {

                                                            //須給上下架時間,長度都為兩秒,放在第四層的應當都為整段的Key,所以起始時間為0.
                                                            if (QF1.Substring(0, 1) == "0")
                                                                VideoID = "ST " + QF1.Substring(1, 2);
                                                            else
                                                                VideoID = "ST " + QF1;
                                                            if (PlayTime == "")
                                                                PlayTime = "00:00:00:00";
                                                            if (Dur1 == "")
                                                                Dur1 = TemArrPromoList.FSDURATION;
                                                            if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) < 15 * 30)
                                                                PlayTime = "00:00:15:00";
                                                            string RealDur = Dur1;
                                                            Dur1 = "00:00:02:00";

                                                            TMP_RPT_DATE_LST.PLAYTIME = PlayTime;
                                                            TMP_RPT_DATE_LST.DUR = "00:00:02:00";
                                                            TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                            TMP_RPT_DATE_LST.FSSOM = PlayTime;
                                                            TMP_RPT_DATE_LST.VIDEOID = VideoID;

                                                            INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);
                                                            RowNO = RowNO + 1;

                                                            VideoID = "ST 5" + QF1;
                                                            TMP_RPT_DATE_LST.VIDEOID = VideoID;
                                                            PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(RealDur));
                                                            if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) + 15 * 30 + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(LastTimeNoInsert) > MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION))
                                                                PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - 15 * 30 - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(LastTimeNoInsert));
                                                               
                                                            Dur1 = "00:00:02:00";
                                                            TMP_RPT_DATE_LST.PLAYTIME = PlayTime;
                                                            TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                            TMP_RPT_DATE_LST.FSSOM = PlayTime;
                                                            INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);
                                                            RowNO = RowNO + 1;
                                                        }
                                                        else
                                                        {

                                                            TMP_RPT_DATE_LST.PLAYTIME = PlayTime;
                                                            //ThisPlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(ThisPlayTime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe("00:" + String.Format("{0:00}", int.Parse(RTimeStatus.SInteval)) + ":00:00") + int.Parse(RTimeStatus.SSec) * 30);
                                                            // string Dur1;
                                                            TMP_RPT_DATE_LST.DUR = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(RTimeStatus.SDur));
                                                            TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                            TMP_RPT_DATE_LST.FSSOM = TMP_RPT_DATE_LST.PLAYTIME; //ThisPlayTime;
                                                            TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                            //TMP_RPT_DATE_LST.VIDEOID = "L" + TMP_RPT_DATE_LST.FSSIGNAL + " " + TMP_RPT_DATE_LST.VIDEOID;
                                                            RowNO = RowNO + 1;
                                                            INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);

                                                        }
  
                                                    }
                                                }
                                            }
                                            else //if (NeedLoop == true)
                                            {
                                                string ThisPlayTime = RTimeStatus.SStartTime;
                                                if (TMP_RPT_DATE_LST.FSSIGNAL == "3")
                                                {
                                                    string QF1 = TMP_RPT_DATE_LST.VIDEOID;
                                                    //須給上下架時間,長度都為兩秒,放在第四層的應當都為整段的Key,所以起始時間為0.
                                                    if (QF1.Substring(0, 1) == "0")
                                                        TMP_RPT_DATE_LST.VIDEOID = "ST " + QF1.Substring(1, 2);
                                                    else
                                                        TMP_RPT_DATE_LST.VIDEOID = "ST " + QF1;
                                                    string PlayTime = ThisPlayTime;
                                                    string RealDur = TMP_RPT_DATE_LST.DUR;
                                                    string Dur1 = "00:00:02:00";

                                                    if (RealDur == "")
                                                        RealDur = TemArrPromoList.FSDURATION;
                                                    TMP_RPT_DATE_LST.PLAYTIME = PlayTime;
                                                    TMP_RPT_DATE_LST.DUR = Dur1;
                                                    TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                    TMP_RPT_DATE_LST.FSSOM = PlayTime;
                                                   
                                                    RowNO = RowNO + 1;
                                                    INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);
                                                    TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                    PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(RealDur));
                                                    if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) + 15 * 30 > MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION))
                                                        PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - 15 * 30);
                                                    TMP_RPT_DATE_LST.PLAYTIME = PlayTime;
                                                    TMP_RPT_DATE_LST.VIDEOID = "ST 5" + QF1;
                                                    TMP_RPT_DATE_LST.FSSOM = PlayTime;
                                                    RowNO = RowNO + 1;
                                                    INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);
                                                }
                                                else
                                                {

                                                    TMP_RPT_DATE_LST.PLAYTIME = RTimeStatus.SStartTime;
                                                    string Dur1;
                                                    Dur1 = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(RTimeStatus.SDur));

                                                    if (TMP_RPT_DATE_LST.MEMO == "sTRANKEY")
                                                    {
                                                        TMP_RPT_DATE_LST.PLAYTIME = "";
                                                        TMP_RPT_DATE_LST.DUR = "";
                                                    }
                                                    TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                    TMP_RPT_DATE_LST.FSSOM = RTimeStatus.SStartTime;
                                                    TMP_RPT_DATE_LST.DUR = Dur1;
                                                    TMP_RPT_DATE_LST.VIDEOID = "L" + TMP_RPT_DATE_LST.FSSIGNAL + " " + TMP_RPT_DATE_LST.VIDEOID;
                                                    RowNO = RowNO + 1;
                                                    INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);
                                                }
                                            }
                                            #endregion

                                        }
                                        #endregion

                                        #region 重播, 直播 ,時間,Live,錄影轉播,Logo,都是整段
                                        if ((LouthKeyType[0].IndexOf("重播") == 0) || (LouthKeyType[0].IndexOf("直播") == 0) || (LouthKeyType[0].IndexOf("時間") == 0) || (LouthKeyType[0].ToUpper().IndexOf("LIVE") == 0) || (LouthKeyType[0].ToUpper().IndexOf("LOGO") == 0) || (LouthKeyType[0].IndexOf("錄影轉播") == 0)) //代表為片名 //有斜線代表為雙語
                                        {
                                            string QF4 = TMP_RPT_DATE_LST.FSSIGNAL;
                                            string QF1 = TMP_RPT_DATE_LST.VIDEOID;
                                            string Memo = "sTRANKEY";
                                            string PlayTime="";
                                            string Dur="";
                                            string VideoID = "";
                                           
                                            
                                            if (LouthKeyType[0].ToUpper().IndexOf("LOGO") == 0)
                                            {
                                                
                                                PlayTime = "00:00:00:00";
                                                Dur = "00:00:10:00";
                                                Memo = "sKEYER";
                                            }
                                            //PlayTime = RTimeStatus.SStartTime;
                                            string Dur1;
                                            //Dur1 = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(RTimeStatus.SDur));
                                            Dur1 = Dur;
                                           
                                            //HD格式的VideoID為 L1 100
                                            if (LouthKeyType[0].ToUpper().IndexOf("LOGO") == 0)
                                            {
                                                PlayTime = "";
                                                Dur1 = "";
                                            }
                                            if (QF4 == "3")
                                            {

                                                if (QF1.Substring(0, 1) == "0")
                                                    VideoID = "ST " + QF1.Substring(1, 2);
                                                else
                                                    VideoID = "ST " + QF1;
                                                if (PlayTime == "")
                                                    PlayTime = "00:00:00:00";
                                                if (Dur1 == "")
                                                    Dur1 = TemArrPromoList.FSDURATION;
                                                if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) < 15 * 30)
                                                    PlayTime = "00:00:15:00";
                                                string RealDur = Dur1;
                                                Dur1 = "00:00:02:00";
                                                TMP_RPT_DATE_LST.MEMO = Memo;
                                                TMP_RPT_DATE_LST.PLAYTIME = PlayTime;
                                                TMP_RPT_DATE_LST.DUR = Dur1;
                                                TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                TMP_RPT_DATE_LST.FSSOM = PlayTime;
                                                TMP_RPT_DATE_LST.VIDEOID = VideoID;
                                                RowNO = RowNO + 1;
                                                INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);
                                                TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode( MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(RealDur));
                                                if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) + 15 * 30 > MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION))
                                                    PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - 15 * 30);
                                                TMP_RPT_DATE_LST.PLAYTIME = PlayTime;
                                                TMP_RPT_DATE_LST.VIDEOID = "ST 5" + QF1;
                                                TMP_RPT_DATE_LST.FSSOM = PlayTime;
                                                RowNO = RowNO + 1;
                                                INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);


             
                                            }
                                            else
                                            {
                                                TMP_RPT_DATE_LST.MEMO = "sTRANKEY";
                                                TMP_RPT_DATE_LST.PLAYTIME = "";
                                                TMP_RPT_DATE_LST.DUR = "";
                                                TMP_RPT_DATE_LST.FSSOM = "00:00:00:00";
                                                if (LouthKeyType[0].ToUpper().IndexOf("LOGO") == 0)
                                                {

                                                    TMP_RPT_DATE_LST.PLAYTIME = "00:00:00:00";
                                                    TMP_RPT_DATE_LST.DUR = "00:00:10:00";
                                                    TMP_RPT_DATE_LST.MEMO = "sKEYER(EH)";
                                                }
                                                //PlayTime = RTimeStatus.SStartTime;
                                                
                                                //Dur1 = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(RTimeStatus.SDur));

                                                //TMP_RPT_DATE_LST.DUR = TemArrPromoList.FSDURATION;
                                                TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                TMP_RPT_DATE_LST.VIDEOID = "L" + TMP_RPT_DATE_LST.FSSIGNAL + " " + TMP_RPT_DATE_LST.VIDEOID;
                                                RowNO = RowNO + 1;
                                                INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);
                                            }

                                         

                                           
                                            
                                        }
                                        #endregion

                                        #region 接著,下週,明天,下次
                                        if ((LouthKeyType[0].IndexOf("接著") == 0) || (LouthKeyType[0].IndexOf("下週") == 0) || (LouthKeyType[0].IndexOf("明天") == 0) || (LouthKeyType[0].IndexOf("下次") == 0))
                                        {
                                            string QF4 = TMP_RPT_DATE_LST.FSSIGNAL;
                                            string QF1 = TMP_RPT_DATE_LST.VIDEOID;
                                            string Memo="";
                                            string PlayTime = "";
                                            string Dur = "";
                                            string VideoID = "";

                                          
                                            //只要有最後幾秒要播出和播出長度
                                            string LastTimeInsert = "00:00:00:00"; //最後多少時間要插入Key
                                           
                                            Memo = "sKEYER";

                                            int IStartTimeFrame = 0;
                                            IStartTimeFrame = MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - int.Parse(REndPointStatus.SEndPoint) * 30;
                                            if (IStartTimeFrame > 0)
                                                PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(IStartTimeFrame);
                                            else
                                                PlayTime = "00:00:00;00";

                                            string Dur1;
                                            //長度為起始時間加五秒及最後幾分鐘不加,所以為長度減掉最後幾分鐘再減五秒

                                            Dur1 = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(REndPointStatus.SDur));

                                            //HD格式的VideoID為 L1 100
                                            if (LouthKeyType[0].ToUpper().IndexOf("LOGO") == 0)
                                            {
                                                PlayTime = "";
                                                Dur1 = "";
                                            }
                                            if (QF4 == "3")
                                            {

                                                if (QF1.Substring(0, 1) == "0")
                                                    VideoID = "ST " + QF1.Substring(1, 2);
                                                else
                                                    VideoID = "ST " + QF1;
                                                if (PlayTime == "")
                                                    PlayTime = "00:00:00:00";
                                                if (Dur1 == "")
                                                    Dur1 = TemArrPromoList.FSDURATION;
                                                if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) < 15 * 30)
                                                    PlayTime = "00:00:15:00";
                                                string RealDur = Dur1;
                                                   
                                                TMP_RPT_DATE_LST.MEMO = Memo;
                                                TMP_RPT_DATE_LST.PLAYTIME = PlayTime;
                                                TMP_RPT_DATE_LST.DUR = Dur1;
                                                TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                TMP_RPT_DATE_LST.FSSOM = PlayTime;
                                                TMP_RPT_DATE_LST.VIDEOID = VideoID;
                                                RowNO = RowNO + 1;
                                                INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);
                                                TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(RealDur));
                                                if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) + 15 * 30 > MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION))
                                                    PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - 15 * 30);
                                                TMP_RPT_DATE_LST.PLAYTIME = PlayTime;
                                                TMP_RPT_DATE_LST.VIDEOID = "ST 5" + QF1;
                                                TMP_RPT_DATE_LST.FSSOM = PlayTime;
                                                RowNO = RowNO + 1;
                                                INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);

                                            }
                                            else
                                            {
                                                //striType = "131";
                                                //只要有最後幾秒要播出和播出長度
                                                LastTimeInsert = "00:00:00:00"; //最後多少時間要插入Key
                                                //EventCOntrol = "";
                                                TMP_RPT_DATE_LST.PROGNAME = NewLouth.FSNAME;
                                                TMP_RPT_DATE_LST.MEMO = "sKEYER";

                                                IStartTimeFrame = 0;
                                                IStartTimeFrame = MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - int.Parse(REndPointStatus.SEndPoint) * 30;
                                                if (IStartTimeFrame > 0)
                                                    TMP_RPT_DATE_LST.PLAYTIME = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(IStartTimeFrame);
                                                else
                                                    TMP_RPT_DATE_LST.PLAYTIME = "00:00:00;00";

                                               
                                                //長度為起始時間加五秒及最後幾分鐘不加,所以為長度減掉最後幾分鐘再減五秒

                                                Dur1 = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(REndPointStatus.SDur));
                                                TMP_RPT_DATE_LST.DUR = Dur1;
                                                TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                TMP_RPT_DATE_LST.FSSOM = TMP_RPT_DATE_LST.PLAYTIME;
                                                TMP_RPT_DATE_LST.VIDEOID = "L" + TMP_RPT_DATE_LST.FSSIGNAL + " " + TMP_RPT_DATE_LST.VIDEOID;
                                                RowNO = RowNO + 1;
                                                INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);

                                            }
                                            //}
                                       
                                          
                                            
                                            //}

                                        }  //if ((LouthKeyType[0].IndexOf(" 接著,下週,明天,下次") > -1)
                                        #endregion
                                    }
                                }
                            }
                        } //  foreach (string LouthKey in LouthKeys)                     
                    } //if (TemArrPromoList.FSMEMO != "")

                    //} //if FCPLAY_Type="G"
                }



            }
            catch (InvalidCastException e)
            {
                return "";
            }

            return g.ToString();
        }


        [WebMethod()]
        public string GetProgSeqno(string DescChannel, String QueueDate, string BTime, string ETime, string ProgID, string ProgName, String PlayType, string SEG, string Dur, string SEC)
        {
            String sqlConnStr = "";
            string FNSEQNO = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            //SqlConnection myconnection = new SqlConnection(sqlConnStr);
            SqlConnection myconnection1 = new SqlConnection(sqlConnStr);
            SqlConnection myconnection2 = new SqlConnection(sqlConnStr);
            SqlCommand mycommand1 = new SqlCommand();
            mycommand1.Connection = myconnection1;
            SqlDataReader myReader1;
            SqlCommand mycom = new SqlCommand();
            mycom.Connection = myconnection1;
            SqlCommand mycom1 = new SqlCommand();
            mycom1.Connection = myconnection2;
            myconnection1.Open();
            myconnection2.Open();
            string Sqltxt1;
            Sqltxt1 = "select FnSeqno,FSprog_ID,FsProg_Name from tbpgm_bank_Detail where fsprog_id='" + ProgID + "'";
            Sqltxt1 = Sqltxt1 + " and FSBEG_TIME='" + BTime + "' and FSCHANNEL_ID='" + DescChannel + "'";
            mycom.CommandText = Sqltxt1;
            myReader1 = mycom.ExecuteReader();
            if (myReader1.HasRows == true)
            {
                myReader1.Read();
                //while (myReader1.Read())
                //{
                if (myReader1.IsDBNull(0) != true)
                {
                    FNSEQNO = myReader1.GetInt32(0).ToString();
                    //FSPROGNAME = myReader1.GetString(2);

                    string Sqltxt2 = "update tbpgm_bank_Detail set fdEND_date='" + QueueDate;
                    Sqltxt2 = Sqltxt2 + "', FSPROG_NAME='" + ProgName + "',FNSEG=" + SEG;
                    Sqltxt2 = Sqltxt2 + ",FNDUR=" + Dur + ",FNSEC=" + SEC + " where fsprog_id='" + ProgID + "'";
                    Sqltxt2 = Sqltxt2 + " and Fnseqno=" + FNSEQNO;
                    mycom1.CommandText = Sqltxt2;
                    mycom1.ExecuteNonQuery();
                }
                else
                {
                    FNSEQNO = "1";
                }
                //}
            }
            else
            {
                string Sqltxt2 = "select max(fnseqno) from tbpgm_bank_Detail where fsprog_id='" + ProgID + "'";
                SqlDataReader myReader2;
                mycom1.CommandText = Sqltxt2;
                FNSEQNO = "";
                //FSPROGNAME = "";
                myReader2 = mycom1.ExecuteReader();
                myReader2.Read();
                if (myReader2.IsDBNull(0) != true)
                {
                    FNSEQNO = (myReader2.GetInt32(0) + 1).ToString();
                }
                else
                {
                    FNSEQNO = "1";
                }
                myReader2.Close();
                Sqltxt1 = "insert into tbpgm_bank_detail(FSPROG_ID, FNSEQNO, FSPROG_NAME, FSPROG_NAME_ENG, FDBEG_DATE, FDEND_DATE,";
                Sqltxt1 = Sqltxt1 + "FSBEG_TIME, FSEND_TIME, FSCHANNEL_ID, FNREPLAY,FSWEEK,FSPLAY_TYPE,FNSEG,FNDUR,FNSEC,FSCREATED_BY,FDCREATED_DATE) values(";
                Sqltxt1 = Sqltxt1 + "'" + ProgID + "',";
                Sqltxt1 = Sqltxt1 + FNSEQNO + ",";
                Sqltxt1 = Sqltxt1 + "'" + ProgName + "',";
                Sqltxt1 = Sqltxt1 + "'',";
                Sqltxt1 = Sqltxt1 + "'" + QueueDate + "',";
                Sqltxt1 = Sqltxt1 + "'" + QueueDate + "',";
                Sqltxt1 = Sqltxt1 + "'" + BTime + "',";
                Sqltxt1 = Sqltxt1 + "'" + ETime + "',";
                Sqltxt1 = Sqltxt1 + "'" + DescChannel + "',";
                Sqltxt1 = Sqltxt1 + "0" + ",";
                Sqltxt1 = Sqltxt1 + "'" + "1111111" + "',";
                Sqltxt1 = Sqltxt1 + PlayType + ","; //live
                Sqltxt1 = Sqltxt1 + SEG + "," + Dur + "," + SEC + ",'',Getdate())"; //seg,FUR,userid,date
                mycom1.CommandText = Sqltxt1;
                mycom1.ExecuteNonQuery();

            }

            myReader1.Close();

            return FNSEQNO;

        }

        //拷貝節目表到MOD
        [WebMethod()]
        public string Do_Copy_Play_List(string fddate, string fschannel_id)
        {
            //strSQL為查詢Store Procesure Name
            //parameters為XML內容(DB_Function_Sample.txt)
            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);

            SqlDataAdapter daQueue = new SqlDataAdapter("SP_Q_TBPGM_QUEUE", connection);
            DataTable dtQueue = new DataTable();

            SqlDataAdapter daCombineQueue = new SqlDataAdapter("SP_Q_TBPGM_COMBINE_QUEUE", connection);
            DataTable dtCombineQueue = new DataTable();

            SqlDataAdapter daArrPromo = new SqlDataAdapter("SP_Q_TBPGM_ARR_PROMO", connection);
            DataTable dtArrPromo = new DataTable();

            string Desc_Channel_ID;
            Desc_Channel_ID = (int.Parse(fschannel_id) + 50).ToString();
            SqlParameter paraD = new SqlParameter("@FDDATE", fddate);
            SqlParameter paraC = new SqlParameter("@FSCHANNEL_ID", fschannel_id);

            SqlParameter paraD1 = new SqlParameter("@FDDATE", fddate);
            SqlParameter paraD2 = new SqlParameter("@FDDATE", fddate);
            SqlParameter paraC1 = new SqlParameter("@FSCHANNEL_ID", fschannel_id);
            SqlParameter paraC2 = new SqlParameter("@FSCHANNEL_ID", fschannel_id);

            daQueue.SelectCommand.Parameters.Add(paraD);
            daQueue.SelectCommand.Parameters.Add(paraC);

            daCombineQueue.SelectCommand.Parameters.Add(paraD1);
            daCombineQueue.SelectCommand.Parameters.Add(paraC1);
            daArrPromo.SelectCommand.Parameters.Add(paraD2);
            daArrPromo.SelectCommand.Parameters.Add(paraC2);

            daQueue.SelectCommand.CommandType = CommandType.StoredProcedure;
            daCombineQueue.SelectCommand.CommandType = CommandType.StoredProcedure;
            daArrPromo.SelectCommand.CommandType = CommandType.StoredProcedure;
            StringBuilder sb = new StringBuilder();
            SqlTransaction tran = null;
            SqlConnection myconnection1 = new SqlConnection(sqlConnStr);

            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                    myconnection1.Open();
                }

                //先查詢QUEUE&Arrpromo的資料
                daQueue.Fill(dtQueue);
                daCombineQueue.Fill(dtCombineQueue);
                daArrPromo.Fill(dtArrPromo);





                SqlCommand mycom1 = new SqlCommand();
                mycom1.Connection = myconnection1;
                tran = myconnection1.BeginTransaction();
                mycom1.Transaction = tran;

                mycom1.CommandType = CommandType.StoredProcedure;

                #region 需要先在託播單加入新頻道的有效區間
                //先建立Promo清單
                string WeekCond = "";
                if ((Convert.ToDateTime(fddate)).DayOfWeek.ToString() == "Monday")
                    WeekCond = "1______";
                else if ((Convert.ToDateTime(fddate)).DayOfWeek.ToString() == "Tuesday")
                    WeekCond = "_1_____";
                else if ((Convert.ToDateTime(fddate)).DayOfWeek.ToString() == "Wednesday")
                    WeekCond = "__1____";
                else if ((Convert.ToDateTime(fddate)).DayOfWeek.ToString() == "Thursday")
                    WeekCond = "___1___";
                else if ((Convert.ToDateTime(fddate)).DayOfWeek.ToString() == "Friday")
                    WeekCond = "____1__";
                else if ((Convert.ToDateTime(fddate)).DayOfWeek.ToString() == "Saturday")
                    WeekCond = "_____1_";
                else if ((Convert.ToDateTime(fddate)).DayOfWeek.ToString() == "Sunday")
                    WeekCond = "______1";
                List<string> UniquePromoList = new List<string>();
                for (int i = 0; i <= dtArrPromo.Rows.Count - 1; i++)
                {
                    //先檢查是否有加入過清單
                    bool NeedAdd = true;
                    foreach (string PromoID in UniquePromoList)
                    {
                        if (PromoID == dtArrPromo.Rows[i]["FSPROMO_ID"].ToString())
                        {
                            NeedAdd = false;
                        }
                    }
                    if (NeedAdd == true)
                    {
                        UniquePromoList.Add(dtArrPromo.Rows[i]["FSPROMO_ID"].ToString());
                    }
                }
                mycom1.CommandType = CommandType.Text;
                //開始將逐一檢查清單內的有效區間,如果在目的頻道沒有有效區間則複製來源頻道的有效區間
                foreach (string PromoID in UniquePromoList)
                {

                    string Sqltxt1;

                    Sqltxt1 = "select FNPROMO_BOOKING_NO,FSPROMO_ID,FNNO,FSCHANNEL_ID from TBPGM_PROMO_EFFECTIVE_ZONE where ";
                    Sqltxt1 = Sqltxt1 + "fspromo_id='" + PromoID + "' and ";
                    Sqltxt1 = Sqltxt1 + " fdbeg_date<='" + fddate + "' and fdend_date>='" + fddate + "' ";
                    Sqltxt1 = Sqltxt1 + " and fsweek like '" + WeekCond + "'";

                    mycom1.CommandText = Sqltxt1;
                    SqlDataReader myReader;
                    myReader = mycom1.ExecuteReader();

                    //SqlDataAdapter daPromo_Zone = new SqlDataAdapter(Sqltxt1, connection);
                    //DataTable dtPromo_Zone = new DataTable();
                    //daPromo_Zone.Fill(dtPromo_Zone);
                    bool hasZone = false;
                    string promo_booking_ID = "";
                    string SourceNO = "";
                    if (myReader.HasRows == true)
                    {
                        while (myReader.Read())
                        {
                            //while (myReader1.Read())
                            //{
                            if (myReader.IsDBNull(0) != true)
                            {
                                if (myReader.GetString(3) == Desc_Channel_ID)
                                { //目的頻道有設定區間 
                                    hasZone = true;
                                }
                                if (myReader.GetString(3) == fschannel_id)
                                { //目的頻道有設定區間 
                                    promo_booking_ID = myReader.GetInt32(0).ToString();
                                    SourceNO = myReader.GetInt32(2).ToString();
                                }

                            }
                        }
                    }

                    myReader.Close();
                    //for (int i = 0; i <= dtPromo_Zone.Rows.Count - 1; i++)
                    //{
                    //    if (dtPromo_Zone.Rows[i]["FSCHANNEL_ID"].ToString() == Desc_Channel_ID)
                    //    { //目的頻道有設定區間 
                    //        hasZone = true;
                    //    }
                    //    if (dtPromo_Zone.Rows[i]["FSCHANNEL_ID"].ToString() == fschannel_id)
                    //    { //目的頻道有設定區間 
                    //        promo_booking_ID = dtPromo_Zone.Rows[i]["FNPROMO_BOOKING_NO"].ToString();
                    //        SourceNO= dtPromo_Zone.Rows[i]["FNNO"].ToString();
                    //    }
                    //}
                    if (hasZone == false)
                    { //沒有頻道時需要複製託播單有效區間
                        if (promo_booking_ID != "")
                        {
                            Sqltxt1 = "select max(fnno) from TBPGM_PROMO_EFFECTIVE_ZONE where ";
                            Sqltxt1 = Sqltxt1 + "FNPROMO_BOOKING_NO=" + promo_booking_ID;
                            mycom1.CommandText = Sqltxt1;
                            //mycom1. aExecuteNonQuery();
                            int DestNO = 0;
                            SqlDataReader myReader1;
                            myReader1 = mycom1.ExecuteReader();
                            if (myReader1.HasRows == true)
                            {
                                myReader1.Read();
                                //while (myReader1.Read())
                                //{
                                if (myReader1.IsDBNull(0) != true)
                                {
                                    DestNO = int.Parse(myReader1.GetInt32(0).ToString()) + 1;
                                    //FSPROGNAME = myReader1.GetString(2);

                                }
                            }

                            myReader1.Close();
                            Sqltxt1 = "insert into TBPGM_PROMO_EFFECTIVE_ZONE(FNPROMO_BOOKING_NO,FSPROMO_ID,FNNO,FDBEG_DATE,FDEND_DATE,";
                            Sqltxt1 = Sqltxt1 + "FSBEG_TIME,FSEND_TIME,FCDATE_TIME_TYPE,FSCHANNEL_ID,FSWEEK,FSCREATED_BY,FDCREATED_DATE,FSUPDATED_BY,FDUPDATED_DATE) ";
                            Sqltxt1 = Sqltxt1 + " select FNPROMO_BOOKING_NO,FSPROMO_ID," + DestNO + ",FDBEG_DATE,FDEND_DATE,";
                            Sqltxt1 = Sqltxt1 + "FSBEG_TIME,FSEND_TIME,FCDATE_TIME_TYPE," + Desc_Channel_ID + ",FSWEEK,FSCREATED_BY,FDCREATED_DATE,FSUPDATED_BY,FDUPDATED_DATE from  ";
                            Sqltxt1 = Sqltxt1 + "TBPGM_PROMO_EFFECTIVE_ZONE where  FNPROMO_BOOKING_NO='" + promo_booking_ID + "' and FNNO=" + SourceNO;

                            mycom1.CommandText = Sqltxt1;
                            mycom1.ExecuteNonQuery();
                        }
                    }

                }
                mycom1.CommandType = CommandType.StoredProcedure;
                #endregion
                mycom1.CommandText = "SP_D_TBPGM_QUEUE";
                mycom1.Parameters.Clear();
                //先刪除目的地的QUEUE,Combinequeue,Arrpromo節目表
                SqlParameter paraD11 = new SqlParameter("@FDDATE", fddate);
                mycom1.Parameters.Add(paraD11);
                SqlParameter paraC11 = new SqlParameter("@FSCHANNEL_ID", Desc_Channel_ID);
                mycom1.Parameters.Add(paraC11);
                if (mycom1.ExecuteNonQuery() == 0)
                {

                }



                mycom1.CommandText = "SP_D_TBPGM_COMBINE_QUEUE";
                mycom1.Parameters.Clear();
                SqlParameter paraD12 = new SqlParameter("@FDDATE", fddate);
                SqlParameter paraC12 = new SqlParameter("@FSCHANNEL_ID", Desc_Channel_ID);
                mycom1.Parameters.Add(paraD12);
                mycom1.Parameters.Add(paraC12);
                mycom1.ExecuteNonQuery();
                mycom1.CommandText = "SP_D_TBPGM_ARR_PROMO";
                mycom1.Parameters.Clear();
                SqlParameter paraD13 = new SqlParameter("@FDDATE", fddate);
                SqlParameter paraC13 = new SqlParameter("@FSCHANNEL_ID", Desc_Channel_ID);
                mycom1.Parameters.Add(paraD13);
                mycom1.Parameters.Add(paraC13);
                mycom1.ExecuteNonQuery();
                mycom1.CommandType = CommandType.Text;
                if (dtQueue.Rows.Count > 0)
                {
                    for (int i = 0; i <= dtQueue.Rows.Count - 1; i++)
                    {
                        string newSeqno = "";
                        newSeqno = GetProgSeqno(Desc_Channel_ID, fddate, dtQueue.Rows[i]["FSBEG_TIME"].ToString(), dtQueue.Rows[i]["FSEND_TIME"].ToString(), dtQueue.Rows[i]["FSPROG_ID"].ToString(), dtQueue.Rows[i]["FSPROG_NAME"].ToString(), dtQueue.Rows[i]["FNLIVE"].ToString(), dtQueue.Rows[i]["FNSEG"].ToString(), dtQueue.Rows[i]["FNDUR"].ToString(), dtQueue.Rows[i]["FNDEF_SEC"].ToString());
                        string Sqltxt1;
                        ///拷貝TBPGM_QUEUE
                        ///FNCONBINE_NO,FCTIMETYPE,FSPROG_ID,FNSEQNO,FSPROG_NAME,FDDATE,FSCHANNEL_ID,
                        ///FNEPISODE,FNBREAK_NO,FNSUB_NO,FSVIDEO_ID,FCPROPERTY,FSPLAY_TIME,FSDUR,FNLIVE,
                        ///FSPROMO_ID,FSMEMO,FSMEMO1,FSSIGNAL,FSCREATED_BY,FDCREATED_DATE,FSFILE_NO
                        string newProgName = dtQueue.Rows[i]["FSPROG_NAME"].ToString();
                        if (dtQueue.Rows[i]["FSMEMO1"].ToString().Length >= 7)
                        {
                            if (dtQueue.Rows[i]["FSMEMO1"].ToString().Substring(0, 7) == "MOD不可播出")
                            {
                                newProgName = "(MOD不可播出)" + dtQueue.Rows[i]["FSPROG_NAME"].ToString();
                            }
                        }
                        Sqltxt1 = "insert into TBPGM_QUEUE(FSPROG_ID,FNSEQNO,FDDATE,FSCHANNEL_ID,FSBEG_TIME,FSEND_TIME,FNREPLAY,FNEPISODE,FSTAPENO,FSPROG_NAME,FNLIVE,FSSIGNAL,FNDEP,FNONOUT,";
                        Sqltxt1 = Sqltxt1 + "FSMEMO,FSMEMO1,FSCREATED_BY,FDCREATED_DATE,FNDEF_MIN,FNDEF_SEC) ";
                        Sqltxt1 = Sqltxt1 + " select FSPROG_ID," + newSeqno + ",FDDATE,'" + Desc_Channel_ID + "',FSBEG_TIME,FSEND_TIME,FNREPLAY,FNEPISODE,FSTAPENO,'" + newProgName + "',FNLIVE,FSSIGNAL,FNDEP,FNONOUT,";
                        Sqltxt1 = Sqltxt1 + "FSMEMO,FSMEMO1,FSCREATED_BY,FDCREATED_DATE,FNDEF_MIN,FNDEF_SEC from  ";
                        Sqltxt1 = Sqltxt1 + "TBPGM_QUEUE where  FDDATE='" + fddate + "' and FSCHANNEL_ID='" + fschannel_id;
                        Sqltxt1 = Sqltxt1 + "' and FSPROG_ID='" + dtQueue.Rows[i]["FSPROG_ID"].ToString() + "' and fnseqno=" + dtQueue.Rows[i]["FNSEQNO"].ToString();
                        mycom1.CommandText = Sqltxt1;
                        mycom1.ExecuteNonQuery();

                        ///拷貝TBPGM_COMBINE_QUEUE
                        ///FNCONBINE_NO,FCTIMETYPE,FSPROG_ID,FNSEQNO,FSPROG_NAME,FDDATE,FSCHANNEL_ID,FNEPISODE,
                        ///FNBREAK_NO,FNSUB_NO,FSVIDEO_ID,FCPROPERTY,FSPLAY_TIME,FSDUR,FNLIVE,FSPROMO_ID,
                        ///FSMEMO,FSMEMO1,FSSIGNAL,FSCREATED_BY,FDCREATED_DATE,FSFILE_NO

                        Sqltxt1 = "insert into TBPGM_COMBINE_QUEUE(FNCONBINE_NO,FCTIMETYPE,FSPROG_ID,FNSEQNO,FSPROG_NAME,FDDATE,FSCHANNEL_ID,FNEPISODE,";
                        Sqltxt1 = Sqltxt1 + "FNBREAK_NO,FNSUB_NO,FSVIDEO_ID,FCPROPERTY,FSPLAY_TIME,FSDUR,FNLIVE,FSPROMO_ID, ";
                        Sqltxt1 = Sqltxt1 + "FSMEMO,FSMEMO1,FSSIGNAL,FSCREATED_BY,FDCREATED_DATE,FSFILE_NO) ";

                        Sqltxt1 = Sqltxt1 + " select FNCONBINE_NO,FCTIMETYPE,FSPROG_ID," + newSeqno + ",'" + newProgName + "',FDDATE,'" + Desc_Channel_ID + "',FNEPISODE,";
                        Sqltxt1 = Sqltxt1 + "FNBREAK_NO,FNSUB_NO,FSVIDEO_ID,FCPROPERTY,FSPLAY_TIME,FSDUR,FNLIVE,FSPROMO_ID, ";
                        Sqltxt1 = Sqltxt1 + "FSMEMO,FSMEMO1,FSSIGNAL,FSCREATED_BY,FDCREATED_DATE,FSFILE_NO ";
                        Sqltxt1 = Sqltxt1 + "from TBPGM_COMBINE_QUEUE ";
                        Sqltxt1 = Sqltxt1 + "where  FDDATE='" + fddate + "' and FSCHANNEL_ID='" + fschannel_id;
                        Sqltxt1 = Sqltxt1 + "' and FSPROG_ID='" + dtQueue.Rows[i]["FSPROG_ID"].ToString() + "' and fnseqno=" + dtQueue.Rows[i]["FNSEQNO"].ToString();

                        mycom1.CommandText = Sqltxt1;
                        mycom1.ExecuteNonQuery();

                        ///拷貝TBPGM_ARR_PROMO
                        ///FNARR_PROMO_NO,FSPROG_ID,FNSEQNO,FDDATE,FSCHANNEL_ID,
                        ///FNBREAK_NO,FNSUB_NO,FSPROMO_ID,FSPLAYTIME,FSDUR,
                        ///FSSIGNAL,FSMEMO,FSMEMO1,FSCREATED_BY,FDCREATED_DATE,
                        ///FCTIMETYPE,FSVIDEO_ID,FSFILE_NO



                        Sqltxt1 = "insert into TBPGM_ARR_PROMO(FNARR_PROMO_NO,FSPROG_ID,FNSEQNO,FDDATE,FSCHANNEL_ID,";
                        Sqltxt1 = Sqltxt1 + "FNBREAK_NO,FNSUB_NO,FSPROMO_ID,FSPLAYTIME,FSDUR, ";
                        Sqltxt1 = Sqltxt1 + "FSSIGNAL,FSMEMO,FSMEMO1,FSCREATED_BY,FDCREATED_DATE, ";
                        Sqltxt1 = Sqltxt1 + "FCTIMETYPE,FSVIDEO_ID,FSFILE_NO) ";


                        Sqltxt1 = Sqltxt1 + " select FNARR_PROMO_NO,FSPROG_ID," + newSeqno + ",FDDATE,'" + Desc_Channel_ID + "',";
                        Sqltxt1 = Sqltxt1 + "FNBREAK_NO,FNSUB_NO,FSPROMO_ID,FSPLAYTIME,FSDUR, ";
                        Sqltxt1 = Sqltxt1 + "FSSIGNAL,FSMEMO,FSMEMO1,FSCREATED_BY,FDCREATED_DATE, ";
                        Sqltxt1 = Sqltxt1 + "FCTIMETYPE,FSVIDEO_ID,FSFILE_NO ";
                        Sqltxt1 = Sqltxt1 + "from TBPGM_ARR_PROMO ";
                        Sqltxt1 = Sqltxt1 + "where  FDDATE='" + fddate + "' and FSCHANNEL_ID='" + fschannel_id;
                        Sqltxt1 = Sqltxt1 + "' and FSPROG_ID='" + dtQueue.Rows[i]["FSPROG_ID"].ToString() + "' and fnseqno=" + dtQueue.Rows[i]["FNSEQNO"].ToString();
                        mycom1.CommandText = Sqltxt1;
                        mycom1.ExecuteNonQuery();
                    }
                }
                else //(dtCombineQueue.Rows.Count < 0) 沒有節目表資料
                {
                    //return "<Errors>" + "沒有節目表資料" + "</Errors>";
                    return "沒有節目表資料,無法複製";
                }
                tran.Commit();
                myconnection1.Close();
                return "";
            }
            catch (Exception ex)
            {
                tran.Rollback();
                StringBuilder sbError = new StringBuilder();
                //sbError.AppendLine("<Errors>" + ex.Message + "</Errors>");
                sbError.AppendLine(ex.Message);
                return sbError.ToString();
            }
            finally
            {
                connection.Close();
                myconnection1.Close();
                myconnection1.Dispose();
                connection.Dispose();
                GC.Collect();
            }
        }


        //拷貝節目表SD頻道到HD頻道
        [WebMethod()]
        public string Do_Copy_Play_List_SD_TO_HD( string Source_Channel_ID,string Desc_Channel_ID,string FDDATE)
        {
            //strSQL為查詢Store Procesure Name
            //parameters為XML內容(DB_Function_Sample.txt)
            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);

            SqlDataAdapter daQueue = new SqlDataAdapter("SP_Q_TBPGM_QUEUE_SD_TO_HD", connection);
            DataTable dtQueue = new DataTable();

            SqlDataAdapter daCombineQueue = new SqlDataAdapter("SP_Q_TBPGM_COMBINE_QUEUE_SD_TO_HD", connection);
            DataTable dtCombineQueue = new DataTable();

            SqlDataAdapter daArrPromo = new SqlDataAdapter("SP_Q_TBPGM_ARR_PROMO_SD_TO_HD", connection);
            DataTable dtArrPromo = new DataTable();


            SqlParameter paraD = new SqlParameter("@FDDATE", FDDATE);
            SqlParameter paraC = new SqlParameter("@FSCHANNEL_ID", Source_Channel_ID);

            SqlParameter paraD1 = new SqlParameter("@FDDATE", FDDATE);
            SqlParameter paraD2 = new SqlParameter("@FDDATE", FDDATE);
            SqlParameter paraC1 = new SqlParameter("@FSCHANNEL_ID", Source_Channel_ID);
            SqlParameter paraC2 = new SqlParameter("@FSCHANNEL_ID", Source_Channel_ID);

            daQueue.SelectCommand.Parameters.Add(paraD);
            daQueue.SelectCommand.Parameters.Add(paraC);

            daCombineQueue.SelectCommand.Parameters.Add(paraD1);
            daCombineQueue.SelectCommand.Parameters.Add(paraC1);
            daArrPromo.SelectCommand.Parameters.Add(paraD2);
            daArrPromo.SelectCommand.Parameters.Add(paraC2);

            daQueue.SelectCommand.CommandType = CommandType.StoredProcedure;
            daCombineQueue.SelectCommand.CommandType = CommandType.StoredProcedure;
            daArrPromo.SelectCommand.CommandType = CommandType.StoredProcedure;
            StringBuilder sb = new StringBuilder();
            SqlTransaction tran = null;
            SqlConnection myconnection1 = new SqlConnection(sqlConnStr);

            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                    myconnection1.Open();
                }

                //先查詢QUEUE&Arrpromo的資料
                daQueue.Fill(dtQueue);
                daCombineQueue.Fill(dtCombineQueue);
                daArrPromo.Fill(dtArrPromo);





                SqlCommand mycom1 = new SqlCommand();
                mycom1.Connection = myconnection1;
                tran = myconnection1.BeginTransaction();
                mycom1.Transaction = tran;

                mycom1.CommandType = CommandType.StoredProcedure;

                #region 需要先在託播單加入新頻道的有效區間
                //先建立Promo清單
                string WeekCond = "";
                if ((Convert.ToDateTime(FDDATE)).DayOfWeek.ToString() == "Monday")
                    WeekCond = "1______";
                else if ((Convert.ToDateTime(FDDATE)).DayOfWeek.ToString() == "Tuesday")
                    WeekCond = "_1_____";
                else if ((Convert.ToDateTime(FDDATE)).DayOfWeek.ToString() == "Wednesday")
                    WeekCond = "__1____";
                else if ((Convert.ToDateTime(FDDATE)).DayOfWeek.ToString() == "Thursday")
                    WeekCond = "___1___";
                else if ((Convert.ToDateTime(FDDATE)).DayOfWeek.ToString() == "Friday")
                    WeekCond = "____1__";
                else if ((Convert.ToDateTime(FDDATE)).DayOfWeek.ToString() == "Saturday")
                    WeekCond = "_____1_";
                else if ((Convert.ToDateTime(FDDATE)).DayOfWeek.ToString() == "Sunday")
                    WeekCond = "______1";
                List<string> UniquePromoList = new List<string>();
                for (int i = 0; i <= dtArrPromo.Rows.Count - 1; i++)
                {
                    //先檢查是否有加入過清單
                    bool NeedAdd = true;
                    foreach (string PromoID in UniquePromoList)
                    {
                        if (PromoID == dtArrPromo.Rows[i]["FSPROMO_ID"].ToString())
                        {
                            NeedAdd = false;
                        }
                    }
                    if (NeedAdd == true)
                    {
                        UniquePromoList.Add(dtArrPromo.Rows[i]["FSPROMO_ID"].ToString());
                    }
                }
                mycom1.CommandType = CommandType.Text;
                //開始將逐一檢查清單內的有效區間,如果在目的頻道沒有有效區間則複製來源頻道的有效區間
                foreach (string PromoID in UniquePromoList)
                {
                    //因為新的頻道不一定建立過VideoID取得宣傳帶的VideoID,如果沒有則將其寫入Map中
                    string newVideo_id = "";
                    PTS_MAM3.Web.DataSource.WSMAMFunctions obj = new PTS_MAM3.Web.DataSource.WSMAMFunctions();
                    newVideo_id = obj.QueryVideoID_PROG(PromoID, "0", Desc_Channel_ID);

                    string Sqltxt1;

                    Sqltxt1 = "select FNPROMO_BOOKING_NO,FSPROMO_ID,FNNO,FSCHANNEL_ID from TBPGM_PROMO_EFFECTIVE_ZONE where ";
                    Sqltxt1 = Sqltxt1 + "fspromo_id='" + PromoID + "' and ";
                    Sqltxt1 = Sqltxt1 + " fdbeg_date<='" + FDDATE + "' and fdend_date>='" + FDDATE + "' ";
                    Sqltxt1 = Sqltxt1 + " and fsweek like '" + WeekCond + "'";

                    mycom1.CommandText = Sqltxt1;
                    SqlDataReader myReader;
                    myReader = mycom1.ExecuteReader();

                    //SqlDataAdapter daPromo_Zone = new SqlDataAdapter(Sqltxt1, connection);
                    //DataTable dtPromo_Zone = new DataTable();
                    //daPromo_Zone.Fill(dtPromo_Zone);
                    bool hasZone = false;
                    string promo_booking_ID = "";
                    string SourceNO = "";
                    if (myReader.HasRows == true)
                    {
                        while (myReader.Read())
                        {
                            //while (myReader1.Read())
                            //{
                            if (myReader.IsDBNull(0) != true)
                            {
                                if (myReader.GetString(3) == Desc_Channel_ID)
                                { //目的頻道有設定區間 
                                    hasZone = true;
                                }
                                if (myReader.GetString(3) == Source_Channel_ID)
                                { //目的頻道有設定區間 
                                    promo_booking_ID = myReader.GetInt32(0).ToString();
                                    SourceNO = myReader.GetInt32(2).ToString();
                                }

                            }
                        }
                    }

                    myReader.Close();
                    //for (int i = 0; i <= dtPromo_Zone.Rows.Count - 1; i++)
                    //{
                    //    if (dtPromo_Zone.Rows[i]["FSCHANNEL_ID"].ToString() == Desc_Channel_ID)
                    //    { //目的頻道有設定區間 
                    //        hasZone = true;
                    //    }
                    //    if (dtPromo_Zone.Rows[i]["FSCHANNEL_ID"].ToString() == fschannel_id)
                    //    { //目的頻道有設定區間 
                    //        promo_booking_ID = dtPromo_Zone.Rows[i]["FNPROMO_BOOKING_NO"].ToString();
                    //        SourceNO= dtPromo_Zone.Rows[i]["FNNO"].ToString();
                    //    }
                    //}
                    if (hasZone == false)
                    { //沒有頻道時需要複製託播單有效區間
                        if (promo_booking_ID != "")
                        {
                            Sqltxt1 = "select max(fnno) from TBPGM_PROMO_EFFECTIVE_ZONE where ";
                            Sqltxt1 = Sqltxt1 + "FNPROMO_BOOKING_NO=" + promo_booking_ID;
                            mycom1.CommandText = Sqltxt1;
                            //mycom1. aExecuteNonQuery();
                            int DestNO = 0;
                            SqlDataReader myReader1;
                            myReader1 = mycom1.ExecuteReader();
                            if (myReader1.HasRows == true)
                            {
                                myReader1.Read();
                                //while (myReader1.Read())
                                //{
                                if (myReader1.IsDBNull(0) != true)
                                {
                                    DestNO = int.Parse(myReader1.GetInt32(0).ToString()) + 1;
                                    //FSPROGNAME = myReader1.GetString(2);

                                }
                            }

                            myReader1.Close();
                            Sqltxt1 = "insert into TBPGM_PROMO_EFFECTIVE_ZONE(FNPROMO_BOOKING_NO,FSPROMO_ID,FNNO,FDBEG_DATE,FDEND_DATE,";
                            Sqltxt1 = Sqltxt1 + "FSBEG_TIME,FSEND_TIME,FCDATE_TIME_TYPE,FSCHANNEL_ID,FSWEEK,FSCREATED_BY,FDCREATED_DATE,FSUPDATED_BY,FDUPDATED_DATE) ";
                            Sqltxt1 = Sqltxt1 + " select FNPROMO_BOOKING_NO,FSPROMO_ID," + DestNO + ",FDBEG_DATE,FDEND_DATE,";
                            Sqltxt1 = Sqltxt1 + "FSBEG_TIME,FSEND_TIME,FCDATE_TIME_TYPE," + Desc_Channel_ID + ",FSWEEK,FSCREATED_BY,FDCREATED_DATE,FSUPDATED_BY,FDUPDATED_DATE from  ";
                            Sqltxt1 = Sqltxt1 + "TBPGM_PROMO_EFFECTIVE_ZONE where  FNPROMO_BOOKING_NO='" + promo_booking_ID + "' and FNNO=" + SourceNO;

                            mycom1.CommandText = Sqltxt1;
                            mycom1.ExecuteNonQuery();
                        }
                    }

                }
                mycom1.CommandType = CommandType.StoredProcedure;
                #endregion
                mycom1.CommandText = "SP_D_TBPGM_QUEUE";
                mycom1.Parameters.Clear();
                //先刪除目的地的QUEUE,Combinequeue,Arrpromo節目表
                SqlParameter paraD11 = new SqlParameter("@FDDATE", FDDATE);
                mycom1.Parameters.Add(paraD11);
                SqlParameter paraC11 = new SqlParameter("@FSCHANNEL_ID", Desc_Channel_ID);
                mycom1.Parameters.Add(paraC11);
                if (mycom1.ExecuteNonQuery() == 0)
                {

                }



                mycom1.CommandText = "SP_D_TBPGM_COMBINE_QUEUE";
                mycom1.Parameters.Clear();
                SqlParameter paraD12 = new SqlParameter("@FDDATE", FDDATE);
                SqlParameter paraC12 = new SqlParameter("@FSCHANNEL_ID", Desc_Channel_ID);
                mycom1.Parameters.Add(paraD12);
                mycom1.Parameters.Add(paraC12);
                mycom1.ExecuteNonQuery();
                mycom1.CommandText = "SP_D_TBPGM_ARR_PROMO";
                mycom1.Parameters.Clear();
                SqlParameter paraD13 = new SqlParameter("@FDDATE", FDDATE);
                SqlParameter paraC13 = new SqlParameter("@FSCHANNEL_ID", Desc_Channel_ID);
                mycom1.Parameters.Add(paraD13);
                mycom1.Parameters.Add(paraC13);
                mycom1.ExecuteNonQuery();
                mycom1.CommandType = CommandType.Text;
                if (dtQueue.Rows.Count > 0)
                {
                    for (int i = 0; i <= dtQueue.Rows.Count - 1; i++)
                    {
                        string newSeqno = "";
                        newSeqno = GetProgSeqno(Desc_Channel_ID, FDDATE, dtQueue.Rows[i]["FSBEG_TIME"].ToString(), dtQueue.Rows[i]["FSEND_TIME"].ToString(), dtQueue.Rows[i]["FSPROG_ID"].ToString(), dtQueue.Rows[i]["FSPROG_NAME"].ToString(), dtQueue.Rows[i]["FNLIVE"].ToString(), dtQueue.Rows[i]["FNSEG"].ToString(), dtQueue.Rows[i]["FNDUR"].ToString(), dtQueue.Rows[i]["FNDEF_SEC"].ToString());
                        string newVideo_id = "";
                        PTS_MAM3.Web.DataSource.WSMAMFunctions obj = new PTS_MAM3.Web.DataSource.WSMAMFunctions();
                        newVideo_id = obj.QueryVideoID_PROG(dtQueue.Rows[i]["FSPROG_ID"].ToString(), dtQueue.Rows[i]["FNEPISODE"].ToString(), Desc_Channel_ID);

                        
                        string Sqltxt1;

                        string newProgName = dtQueue.Rows[i]["FSPROG_NAME"].ToString();
                        Sqltxt1 = "insert into TBPGM_QUEUE(FSPROG_ID,FNSEQNO,FDDATE,FSCHANNEL_ID,FSBEG_TIME,FSEND_TIME,FNREPLAY,FNEPISODE,FSTAPENO,FSPROG_NAME,FNLIVE,FSSIGNAL,FNDEP,FNONOUT,";
                        Sqltxt1 = Sqltxt1 + "FSMEMO,FSMEMO1,FSCREATED_BY,FDCREATED_DATE,FNDEF_MIN,FNDEF_SEC) ";
                        Sqltxt1 = Sqltxt1 + " select A.FSPROG_ID," + newSeqno + ",A.FDDATE,'" + Desc_Channel_ID + "',A.FSBEG_TIME,A.FSEND_TIME,A.FNREPLAY,A.FNEPISODE,D.FSFILE_NO FSTAPENO,'" + newProgName + "',A.FNLIVE,A.FSSIGNAL,A.FNDEP,A.FNONOUT,";
                        Sqltxt1 = Sqltxt1 + "A.FSMEMO,A.FSMEMO1,A.FSCREATED_BY,A.FDCREATED_DATE,A.FNDEF_MIN,FNDEF_SEC from  ";
                        Sqltxt1 = Sqltxt1 + "TBPGM_QUEUE A ";

                        Sqltxt1 = Sqltxt1 + "Left Join  (SELECT FSID, FSFILE_NO,FCFILE_STATUS,FNEPISODE ";
                        Sqltxt1 = Sqltxt1 + "FROM ( SELECT FSID,FNEPISODE,FSARC_TYPE,FCFILE_STATUS,FSFILE_NO, ROW_NUMBER() OVER(PARTITION BY FSID ORDER BY FCFILE_STATUS DESC) Corr ";
                        Sqltxt1 = Sqltxt1 + "FROM TBLOG_VIDEO where FSARC_TYPE='002' and FCFILE_STATUS not in ('D','F','X')) A where Corr = 1) D on D.fsid=A.FSPROG_ID and D.FNEPISODE=A.FNEPISODE  ";

                        Sqltxt1 = Sqltxt1 + "where  A.FDDATE='" + FDDATE + "' and A.FSCHANNEL_ID='" + Source_Channel_ID;

                        Sqltxt1 = Sqltxt1 + "' and A.FSPROG_ID='" + dtQueue.Rows[i]["FSPROG_ID"].ToString() + "' and A.fnseqno=" + dtQueue.Rows[i]["FNSEQNO"].ToString();
                        mycom1.CommandText = Sqltxt1;
                        mycom1.ExecuteNonQuery();

                        ///拷貝TBPGM_COMBINE_QUEUE
                        ///FNCONBINE_NO,FCTIMETYPE,FSPROG_ID,FNSEQNO,FSPROG_NAME,FDDATE,FSCHANNEL_ID,FNEPISODE,
                        ///FNBREAK_NO,FNSUB_NO,FSVIDEO_ID,FCPROPERTY,FSPLAY_TIME,FSDUR,FNLIVE,FSPROMO_ID,
                        ///FSMEMO,FSMEMO1,FSSIGNAL,FSCREATED_BY,FDCREATED_DATE,FSFILE_NO

                        Sqltxt1 = "insert into TBPGM_COMBINE_QUEUE(FNCONBINE_NO,FCTIMETYPE,FSPROG_ID,FNSEQNO,FSPROG_NAME,FDDATE,FSCHANNEL_ID,FNEPISODE,";
                        Sqltxt1 = Sqltxt1 + "FNBREAK_NO,FNSUB_NO,FSVIDEO_ID,FCPROPERTY,FSPLAY_TIME,FSDUR,FNLIVE,FSPROMO_ID, ";
                        Sqltxt1 = Sqltxt1 + "FSMEMO,FSMEMO1,FSSIGNAL,FSCREATED_BY,FDCREATED_DATE,FSFILE_NO) ";

                        Sqltxt1 = Sqltxt1 + " select A.FNCONBINE_NO,A.FCTIMETYPE,A.FSPROG_ID," + newSeqno + ",'" + newProgName + "',A.FDDATE,'" + Desc_Channel_ID + "',A.FNEPISODE,";
                        Sqltxt1 = Sqltxt1 + "A.FNBREAK_NO,A.FNSUB_NO,C.FSVIDEO_ID_PROG FSVIDEO_ID,A.FCPROPERTY,A.FSPLAY_TIME,A.FSDUR,A.FNLIVE,A.FSPROMO_ID, ";
                        Sqltxt1 = Sqltxt1 + "A.FSMEMO,A.FSMEMO1,A.FSSIGNAL,A.FSCREATED_BY,A.FDCREATED_DATE,D.FSFILE_NO ";
                        Sqltxt1 = Sqltxt1 + "from TBPGM_COMBINE_QUEUE A ";

                        Sqltxt1 = Sqltxt1 + "left join TBLOG_VIDEOID_MAP C on A.FSPROG_ID=C.FSID and A.FNEPISODE=C.FNEPISODE and C.FSARC_TYPE='002'  ";

                        Sqltxt1 = Sqltxt1 + "Left Join  (SELECT FSID, FSFILE_NO,FCFILE_STATUS,FNEPISODE ";
                        Sqltxt1 = Sqltxt1 + "FROM ( SELECT FSID,FNEPISODE,FSARC_TYPE,FCFILE_STATUS,FSFILE_NO, ROW_NUMBER() OVER(PARTITION BY FSID ORDER BY FCFILE_STATUS DESC) Corr ";
                        Sqltxt1 = Sqltxt1 + "FROM TBLOG_VIDEO where FSARC_TYPE='002' and FCFILE_STATUS not in ('D','F','X')) A where Corr = 1) D on D.fsid=A.FSPROG_ID and D.FNEPISODE=A.FNEPISODE  ";

                        Sqltxt1 = Sqltxt1 + "where  A.FDDATE='" + FDDATE + "' and A.FSCHANNEL_ID='" + Source_Channel_ID;
                        Sqltxt1 = Sqltxt1 + "' and A.FSPROG_ID='" + dtQueue.Rows[i]["FSPROG_ID"].ToString() + "' and A.fnseqno=" + dtQueue.Rows[i]["FNSEQNO"].ToString();

                        mycom1.CommandText = Sqltxt1;
                        mycom1.ExecuteNonQuery();

                        ///拷貝TBPGM_ARR_PROMO
                        ///FNARR_PROMO_NO,FSPROG_ID,FNSEQNO,FDDATE,FSCHANNEL_ID,
                        ///FNBREAK_NO,FNSUB_NO,FSPROMO_ID,FSPLAYTIME,FSDUR,
                        ///FSSIGNAL,FSMEMO,FSMEMO1,FSCREATED_BY,FDCREATED_DATE,
                        ///FCTIMETYPE,FSVIDEO_ID,FSFILE_NO



                        Sqltxt1 = "insert into TBPGM_ARR_PROMO(FNARR_PROMO_NO,FSPROG_ID,FNSEQNO,FDDATE,FSCHANNEL_ID,";
                        Sqltxt1 = Sqltxt1 + "FNBREAK_NO,FNSUB_NO,FSPROMO_ID,FSPLAYTIME,FSDUR, ";
                        Sqltxt1 = Sqltxt1 + "FSSIGNAL,FSMEMO,FSMEMO1,FSCREATED_BY,FDCREATED_DATE, ";
                        Sqltxt1 = Sqltxt1 + "FCTIMETYPE,FSVIDEO_ID,FSFILE_NO) ";

                        Sqltxt1 = Sqltxt1 + " select A.FNARR_PROMO_NO,A.FSPROG_ID," + newSeqno + ",A.FDDATE,'" + Desc_Channel_ID + "',";
                        Sqltxt1 = Sqltxt1 + "A.FNBREAK_NO,A.FNSUB_NO,A.FSPROMO_ID,A.FSPLAYTIME,FSDUR, ";
                        Sqltxt1 = Sqltxt1 + "A.FSSIGNAL,A.FSMEMO,A.FSMEMO1,A.FSCREATED_BY,A.FDCREATED_DATE, ";
                        Sqltxt1 = Sqltxt1 + "A.FCTIMETYPE,C.FSVIDEO_ID_PROG FSVIDEO_ID,D.FSFILE_NO ";
                        Sqltxt1 = Sqltxt1 + "from TBPGM_ARR_PROMO A ";
                        Sqltxt1 = Sqltxt1 + "left join TBLOG_VIDEOID_MAP C on A.FSPROMO_ID=C.FSID and C.FNEPISODE=0 and C.FSARC_TYPE='002'  ";
                        //Sqltxt1 = Sqltxt1 + "left join TBLOG_VIDEO D on A.FSPROMO_ID=D.FSID and D.FNEPISODE=0 and D.FSARC_TYPE='002' and D.FCFILE_STATUS not in ('D','F','X') ";
                        Sqltxt1 = Sqltxt1 + "Left Join  (SELECT FSID, FSFILE_NO,FCFILE_STATUS,FNEPISODE ";
                        Sqltxt1 = Sqltxt1 + "FROM ( SELECT FSID,FNEPISODE,FSARC_TYPE,FCFILE_STATUS,FSFILE_NO, ROW_NUMBER() OVER(PARTITION BY FSID ORDER BY FCFILE_STATUS DESC) Corr ";
                        Sqltxt1 = Sqltxt1 + "FROM TBLOG_VIDEO where FSARC_TYPE='002' and FCFILE_STATUS not in ('D','F','X')) A where Corr = 1) D on D.fsid=A.FSPROMO_ID and D.FNEPISODE=0  ";

                        Sqltxt1 = Sqltxt1 + "where  A.FDDATE='" + FDDATE + "' and A.FSCHANNEL_ID='" + Source_Channel_ID;
                        Sqltxt1 = Sqltxt1 + "' and A.FSPROG_ID='" + dtQueue.Rows[i]["FSPROG_ID"].ToString() + "' and A.fnseqno=" + dtQueue.Rows[i]["FNSEQNO"].ToString();
                        mycom1.CommandText = Sqltxt1;
                        mycom1.ExecuteNonQuery();
                    }
                }
                else //(dtCombineQueue.Rows.Count < 0) 沒有節目表資料
                {
                    //return "<Errors>" + "沒有節目表資料" + "</Errors>";
                    return "沒有節目表資料,無法複製";
                }
                tran.Commit();
                myconnection1.Close();
                return "";
            }
            catch (Exception ex)
            {
                tran.Rollback();
                StringBuilder sbError = new StringBuilder();
                //sbError.AppendLine("<Errors>" + ex.Message + "</Errors>");
                sbError.AppendLine(ex.Message);
                return sbError.ToString();
            }
            finally
            {
                connection.Close();
                myconnection1.Close();
                myconnection1.Dispose();
                connection.Dispose();
                GC.Collect();
            }
        }

        [WebMethod()]
        public string Delete_Queue_Prog(string DelChannel, String DelDate, string DelProgID, string DelSeqno, string DelEpisode)
        {
            String sqlConnStr = "";
            string FNSEQNO = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            //SqlConnection myconnection = new SqlConnection(sqlConnStr);
            SqlConnection myconnection1 = new SqlConnection(sqlConnStr);
            SqlConnection myconnection2 = new SqlConnection(sqlConnStr);
            SqlCommand mycommand1 = new SqlCommand();
            mycommand1.Connection = myconnection1;
            SqlDataReader myReader1;
            SqlCommand mycom = new SqlCommand();
            mycom.Connection = myconnection2;

            if (myconnection1.State == System.Data.ConnectionState.Closed)
            {
                myconnection1.Open();
            }
            if (myconnection2.State == System.Data.ConnectionState.Closed)
            {
                myconnection2.Open();
            }
            #region 先查詢TBPGM_ARR_PROMO_LOST最大編號 MAX_ARR_PROMO_LOST_ID
            SqlDataAdapter da_LOST_MAX_ID = new SqlDataAdapter("SP_Q_TBPGM_ARR_PROMO_LOST_MAX_ID", myconnection1);
            DataTable dt_LOST_MAX_ID = new DataTable();

            //SqlParameter para_CHANNEL_PROMO_LINK = new SqlParameter("@FSCHANNEL_ID", dtQueue.Rows[1]["FSCHANNEL_ID"].ToString());
            SqlParameter para_LOST_MAX_ID = new SqlParameter("@FSCHANNEL_ID", DelChannel);
            da_LOST_MAX_ID.SelectCommand.Parameters.Add(para_LOST_MAX_ID);
            SqlParameter para_LOST_MAX_ID1 = new SqlParameter("@FDDATE", DelDate);
            da_LOST_MAX_ID.SelectCommand.Parameters.Add(para_LOST_MAX_ID1);

            da_LOST_MAX_ID.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter daArrPromo = new SqlDataAdapter("SP_Q_TBPGM_ARR_PROMO_NEW_VIDEOID", myconnection1);
            DataTable dtArrPromo = new DataTable();
            SqlParameter para1 = new SqlParameter("@FSCHANNEL_ID", DelChannel);
            daArrPromo.SelectCommand.Parameters.Add(para1);
            SqlParameter para2 = new SqlParameter("@FDDATE", DelDate);
            daArrPromo.SelectCommand.Parameters.Add(para2);
            daArrPromo.SelectCommand.CommandType = CommandType.StoredProcedure;
            daArrPromo.Fill(dtArrPromo);
            //先查詢QUEUE&Arrpromo的資料
            da_LOST_MAX_ID.Fill(dt_LOST_MAX_ID);
            int MAX_ARR_PROMO_LOST_ID = 0;
            for (int L = 0; L <= dt_LOST_MAX_ID.Rows.Count - 1; L++) //先取得段落
            {
                if (dt_LOST_MAX_ID.Rows[L][0].ToString() == "")
                    MAX_ARR_PROMO_LOST_ID = 0;
                else
                    MAX_ARR_PROMO_LOST_ID = int.Parse(dt_LOST_MAX_ID.Rows[L][0].ToString());
            }
            MAX_ARR_PROMO_LOST_ID = MAX_ARR_PROMO_LOST_ID + 1;
            //connection.Close();
            #endregion

            #region 將沒有對應到的Promo寫入TBPGM_ARR_PROMO_LOST中
            int hasInsertNum = 0;
            int InsErrNum = 0;
            SqlTransaction tran = null;

            SqlCommand sqlIns_Arr_Promo_Lost = new SqlCommand("SP_I_TBPGM_ARR_PROMO_LOST", myconnection1);

            tran = myconnection1.BeginTransaction();
            sqlIns_Arr_Promo_Lost.Transaction = tran;
            try
            {
                if (myconnection1.State == System.Data.ConnectionState.Closed)
                {
                    myconnection1.Open();
                }


                for (int i = 0; i <= dtArrPromo.Rows.Count - 1; i++) //先取得段落
                {

                    if (dtArrPromo.Rows[i]["FSPROG_ID"].ToString() == DelProgID && dtArrPromo.Rows[i]["FNSEQNO"].ToString() == DelSeqno)
                    {
                        sqlIns_Arr_Promo_Lost.Parameters.Clear();
                        SqlParameter para_LOST_FNCREATE_LIST_NO = new SqlParameter("@FNCREATE_LIST_NO", MAX_ARR_PROMO_LOST_ID);
                        sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FNCREATE_LIST_NO);

                        SqlParameter para_LOST_FNARR_PROMO_NO = new SqlParameter("@FNARR_PROMO_NO", dtArrPromo.Rows[i]["FNARR_PROMO_NO"].ToString());
                        sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FNARR_PROMO_NO);

                        SqlParameter para_LOST_FSPROG_ID = new SqlParameter("@FSPROG_ID", dtArrPromo.Rows[i]["FSPROG_ID"].ToString());
                        sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FSPROG_ID);

                        SqlParameter para_LOST_FNSEQNO = new SqlParameter("@FNSEQNO", dtArrPromo.Rows[i]["FNSEQNO"].ToString());
                        sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FNSEQNO);

                        //string[] DateArr = dtArrPromo.Rows[i]["FDDATE"].ToString().Split(' ');
                        SqlParameter para_LOST_FDDATE = new SqlParameter("@FDDATE", dtArrPromo.Rows[i]["FDDATE"].ToString().Split(' ')[0]);
                        sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FDDATE);

                        SqlParameter para_LOST_FSCHANNEL_ID = new SqlParameter("@FSCHANNEL_ID", dtArrPromo.Rows[i]["FSCHANNEL_ID"].ToString());
                        sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FSCHANNEL_ID);

                        SqlParameter para_LOST_FNBREAK_NO = new SqlParameter("@FNBREAK_NO", dtArrPromo.Rows[i]["FNBREAK_NO"].ToString());
                        sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FNBREAK_NO);
                        SqlParameter para_LOST_FNSUB_NO = new SqlParameter("@FNSUB_NO", dtArrPromo.Rows[i]["FNSUB_NO"].ToString());
                        sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FNSUB_NO);
                        SqlParameter para_LOST_FSPROMO_ID = new SqlParameter("@FSPROMO_ID", dtArrPromo.Rows[i]["FSPROMO_ID"].ToString());
                        sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FSPROMO_ID);
                        SqlParameter para_LOST_FSPLAYTIME = new SqlParameter("@FSPLAYTIME", dtArrPromo.Rows[i]["FSPLAYTIME"].ToString());
                        sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FSPLAYTIME);
                        SqlParameter para_LOST_FSDUR = new SqlParameter("@FSDUR", dtArrPromo.Rows[i]["FSDUR"].ToString());
                        sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FSDUR);
                        SqlParameter para_LOST_FSSIGNAL = new SqlParameter("@FSSIGNAL", dtArrPromo.Rows[i]["FSSIGNAL"].ToString());
                        sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FSSIGNAL);
                        SqlParameter para_LOST_FSMEMO = new SqlParameter("@FSMEMO", dtArrPromo.Rows[i]["FSMEMO"].ToString());
                        sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FSMEMO);
                        SqlParameter para_LOST_FSMEMO1 = new SqlParameter("@FSMEMO1", dtArrPromo.Rows[i]["FSMEMO1"].ToString());
                        sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FSMEMO1);
                        SqlParameter para_LOST_FSCREATED_BY = new SqlParameter("@FSCREATED_BY", dtArrPromo.Rows[i]["FSCREATED_BY"].ToString());
                        sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FSCREATED_BY);
                        SqlParameter para_LOST_FDCREATED_DATE = new SqlParameter("@FDCREATED_DATE", dtArrPromo.Rows[i]["FDCREATED_DATE"].ToString().Split(' ')[0]);
                        sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FDCREATED_DATE);
                        SqlParameter para_LOST_FSSTATUS = new SqlParameter("@FSSTATUS", "");
                        sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FSSTATUS);
                        SqlParameter para_LOST_FNEPISODE = new SqlParameter("@FNEPISODE", DelEpisode);
                        sqlIns_Arr_Promo_Lost.Parameters.Add(para_LOST_FNEPISODE);

                        sqlIns_Arr_Promo_Lost.CommandType = CommandType.StoredProcedure;


                        if (sqlIns_Arr_Promo_Lost.ExecuteNonQuery() == 0)
                        {
                            InsErrNum = InsErrNum + 1;
                        }
                        else
                        {
                            //不須知道異動筆數

                            //tran.Rollback();

                        }

                    }

                    hasInsertNum = hasInsertNum + 1;
                }

                string Sqltxt1;
                Sqltxt1 = "delete from tbpgm_queue where FSPROG_ID='" + DelProgID + "' and FNSEQNO=" + DelSeqno;
                Sqltxt1 = Sqltxt1 + " and FDDATE='" + DelDate + "' and FSCHANNEL_ID='" + DelChannel + "'";
                mycom.CommandText = Sqltxt1;
                mycom.ExecuteNonQuery();

                if (InsErrNum > 0)
                {
                    tran.Rollback();
                    return "<Errors>" + "寫入遺失清單失敗" + "</Errors>";
                }
                else
                {
                    tran.Commit();
                }
                return "OK";
            }
            catch (Exception ex)
            {
                tran.Rollback();
                return "Error:" + ex.Message;
            }
            finally
            {

                myconnection1.Close();
                myconnection1.Dispose();
                myconnection2.Close();
                myconnection2.Dispose();
                sqlIns_Arr_Promo_Lost.Dispose();
                GC.Collect();

            }


            #endregion

        }


        //置換節目表的節目,1.先新增或修改新節目的播映資料,2.在TBPGM_QUEUE增加新節目資料3.刪除TBPGM_QUEUE的舊資料
        [WebMethod()]
        public string ReplaceQueueProg(string strSQL, string parameters = null)
        {
            string FSPROGID = "";
            string FNSEG = "";
            string FDDATE = "";
            string FNEPISODE = "";
            string FSCHANNEL_ID = "";
            string FSBTIME = "";
            string FSDURATION = "";
            string FNREPLAY = "";
            string FSPLAY_TYPE = "";
            string FNDUR = "";
            string FSETIME = "";
            string FNSEQNO = "";
            string FSPROGNAME = "";
            string FSMEMO = "";
            string FSMEMO1 = "";
            string FSSIGNAL = "";
            string FNDEF_MIN = "";
            string FNDEF_SEC = "";

            string SFSPROGID = "";
            string SFNSEQNO = "";

            if (parameters != "")
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(parameters);
                XmlNode xmlNode = xmldoc.FirstChild;
                for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                {
                    if (xmlNode.ChildNodes[i].Name == "FSPROG_ID")
                        FSPROGID = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "FNEPISODE")
                        FNEPISODE = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "FDDATE")
                        FDDATE = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "FSBTIME")
                        FSBTIME = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "FSETIME")
                        FSETIME = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "FSPROGNAME")
                        FSPROGNAME = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "FNDUR")
                        FNDUR = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "FNSEG")
                        FNSEG = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "FSPLAY_TYPE")
                        FSPLAY_TYPE = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "FSCHANNEL_ID")
                        FSCHANNEL_ID = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "FSMEMO")
                    {
                        if (xmlNode.ChildNodes[i].FirstChild == null)
                            FSMEMO = "";
                        else
                            FSMEMO = xmlNode.ChildNodes[i].FirstChild.Value;
                    }
                    if (xmlNode.ChildNodes[i].Name == "FSMEMO1")
                    {
                        if (xmlNode.ChildNodes[i].FirstChild == null)
                            FSMEMO1 = "";
                        else
                            FSMEMO1 = xmlNode.ChildNodes[i].FirstChild.Value;
                    }
                    if (xmlNode.ChildNodes[i].Name == "FSSIGNAL")
                        FSSIGNAL = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "FNDEF_MIN")
                        FNDEF_MIN = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "FNDEF_SEC")
                        FNDEF_SEC = xmlNode.ChildNodes[i].FirstChild.Value;

                    if (xmlNode.ChildNodes[i].Name == "SFSPROGID")
                        SFSPROGID = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "SFNSEQNO")
                        SFNSEQNO = xmlNode.ChildNodes[i].FirstChild.Value;

                }
            }

            PTS_MAM3.Web.DataSource.WSMAMFunctions obj = new PTS_MAM3.Web.DataSource.WSMAMFunctions();
            string ReturnVideoID = "";
            ReturnVideoID = obj.QueryVideoID_PROG(FSPROGID, FNEPISODE, FSCHANNEL_ID);
            if (ReturnVideoID == "        ")
            {
                return "";
            }

            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            //SqlConnection myconnection = new SqlConnection(sqlConnStr);
            SqlConnection myconnection1 = new SqlConnection(sqlConnStr);
            SqlConnection myconnection2 = new SqlConnection(sqlConnStr);
            SqlCommand mycommand1 = new SqlCommand();
            mycommand1.Connection = myconnection1;
            SqlDataReader myReader1;
            SqlCommand mycom = new SqlCommand();
            mycom.Connection = myconnection1;
            SqlCommand mycom1 = new SqlCommand();
            mycom1.Connection = myconnection2;
            myconnection1.Open();
            myconnection2.Open();
            string Sqltxt1;
            Sqltxt1 = "select FnSeqno,FSprog_ID,FsProg_Name from tbpgm_bank_Detail where fsprog_id='" + FSPROGID + "'";
            Sqltxt1 = Sqltxt1 + " and FSBEG_TIME='" + FSBTIME + "' and FSCHANNEL_ID='" + FSCHANNEL_ID + "'";
            mycom.CommandText = Sqltxt1;
            myReader1 = mycom.ExecuteReader();
            if (myReader1.HasRows == true)
            {
                myReader1.Read();
                //while (myReader1.Read())
                //{
                if (myReader1.IsDBNull(0) != true)
                {
                    FNSEQNO = myReader1.GetInt32(0).ToString();
                    //FSPROGNAME = myReader1.GetString(2);

                    string Sqltxt2 = "update tbpgm_bank_Detail set fdEND_date='" + FDDATE + "', FSPROG_NAME='" + FSPROGNAME + "' where fsprog_id='" + FSPROGID + "'";
                    Sqltxt2 = Sqltxt2 + " and Fnseqno=" + FNSEQNO;
                    mycom1.CommandText = Sqltxt2;
                    mycom1.ExecuteNonQuery();
                }
                else
                {
                    FNSEQNO = "1";
                }
                //}
            }
            else
            {
                string Sqltxt2 = "select max(fnseqno) from tbpgm_bank_Detail where fsprog_id='" + FSPROGID + "'";
                SqlDataReader myReader2;
                mycom1.CommandText = Sqltxt2;
                FNSEQNO = "";
                //FSPROGNAME = "";
                myReader2 = mycom1.ExecuteReader();
                myReader2.Read();
                if (myReader2.IsDBNull(0) != true)
                {
                    FNSEQNO = (myReader2.GetInt32(0) + 1).ToString();
                }
                else
                {
                    FNSEQNO = "1";
                }
                myReader2.Close();
                Sqltxt1 = "insert into tbpgm_bank_detail(FSPROG_ID, FNSEQNO, FSPROG_NAME, FSPROG_NAME_ENG, FDBEG_DATE, FDEND_DATE,";
                Sqltxt1 = Sqltxt1 + "FSBEG_TIME, FSEND_TIME, FSCHANNEL_ID, FNREPLAY,FSWEEK,FSPLAY_TYPE,FNSEG,FNDUR,FSCREATED_BY,FDCREATED_DATE) values(";
                Sqltxt1 = Sqltxt1 + "'" + FSPROGID + "',";
                Sqltxt1 = Sqltxt1 + FNSEQNO + ",";
                Sqltxt1 = Sqltxt1 + "'" + FSPROGNAME + "',";
                Sqltxt1 = Sqltxt1 + "'',";
                Sqltxt1 = Sqltxt1 + "'" + FDDATE + "',";
                Sqltxt1 = Sqltxt1 + "'" + FDDATE + "',";
                Sqltxt1 = Sqltxt1 + "'" + FSBTIME + "',";
                Sqltxt1 = Sqltxt1 + "'" + FSETIME + "',";
                Sqltxt1 = Sqltxt1 + "'" + FSCHANNEL_ID + "',";
                Sqltxt1 = Sqltxt1 + "0" + ",";
                Sqltxt1 = Sqltxt1 + "'" + "1111111" + "',";
                Sqltxt1 = Sqltxt1 + FSPLAY_TYPE + ","; //live
                Sqltxt1 = Sqltxt1 + FNSEG + "," + FNDUR + ",'',Getdate())"; //seg,FUR,userid,date
                mycom1.CommandText = Sqltxt1;
                mycom1.ExecuteNonQuery();

            }

            myReader1.Close();

            //刪除舊的Queue資料

            Sqltxt1 = "delete from tbpgm_queue where fsprog_id='" + SFSPROGID + "'";
            Sqltxt1 = Sqltxt1 + " and FNSEQNO=" + SFNSEQNO + " and FSCHANNEL_ID='" + FSCHANNEL_ID + "' and fddate='" + FDDATE + "'";
            mycom1.CommandText = Sqltxt1;
            mycom1.ExecuteNonQuery();
            //myReader1.Close();

            //寫入TBPGM_QUEUE
            Sqltxt1 = "select FnSeqno,FSprog_ID,FsProg_Name from TBPGM_QUEUE where fsprog_id='" + FSPROGID + "'";
            Sqltxt1 = Sqltxt1 + " and FNSEQNO=" + FNSEQNO + " and FSCHANNEL_ID='" + FSCHANNEL_ID + "' and fddate='" + FDDATE + "'";
            mycom.CommandText = Sqltxt1;
            myReader1 = mycom.ExecuteReader();
            if (myReader1.HasRows == false)
            {  // 開始寫入TBPGM_QUEUE
                string Sqltxt2 = "";
                Sqltxt2 = "insert into tbpgm_queue(FSPROG_ID,FNSEQNO,FDDATE,FSCHANNEL_ID,FSBEG_TIME,FSEND_TIME,";
                Sqltxt2 = Sqltxt2 + " FNREPLAY,FNEPISODE,FSTAPENO,FSPROG_NAME,FNLIVE,FSSIGNAL,FNDEP,FNONOUT,FSMEMO,FSMEMO1,";
                Sqltxt2 = Sqltxt2 + " FSCREATED_BY,FDCREATED_DATE,FNDEF_MIN,FNDEF_SEC) ";
                Sqltxt2 = Sqltxt2 + " values ('" + FSPROGID + "'," + FNSEQNO + ",'" + FDDATE + "','" + FSCHANNEL_ID + "','";
                Sqltxt2 = Sqltxt2 + FSBTIME + "','" + FSETIME + "',0," + FNEPISODE + ",'','" + FSPROGNAME + "',";
                Sqltxt2 = Sqltxt2 + FSPLAY_TYPE + ",'" + FSSIGNAL + "',0,0,'" + FSMEMO + "','" + FSMEMO1 + "','',getdate(),'" + FNDEF_MIN + "','" + FNDEF_SEC + "')";
                mycom1.CommandText = Sqltxt2;
                mycom1.ExecuteNonQuery();
            }
            else
            {
                //return "Error:已有目的節目資料,不可重複置換節目";
            }
            myReader1.Close();

            //查詢目的節目是否已在合併架構中
            if (FSPROGID != SFSPROGID && FNSEQNO != SFNSEQNO)
            {
                Sqltxt1 = "select FnSeqno,FSprog_ID,FsProg_Name from TBPGM_COMBINE_QUEUE where fsprog_id='" + FSPROGID + "'";
                Sqltxt1 = Sqltxt1 + " and FNSEQNO=" + FNSEQNO + " and FSCHANNEL_ID='" + FSCHANNEL_ID + "' and fddate='" + FDDATE + "'";

                mycom.CommandText = Sqltxt1;
                myReader1 = mycom.ExecuteReader();
                if (myReader1.HasRows == true)
                {  // 開始寫入TBPGM_QUEUE
                    return "Error:已有目的節目資料,不可重複置換節目";
                }
            }
            //回傳FNSEQNO,VIDEOID


            StringBuilder sb = new StringBuilder();
            DataTable dt = new DataTable();
            //SqlCommand sqlcmd = new SqlCommand(strSQL, connection);
            sb.AppendLine("<Datas>");
            sb.AppendLine("<Data>");
            sb.AppendLine("<FNSEQNO>" + FNSEQNO + "</FNSEQNO>");
            sb.AppendLine("<FSVIDEO_ID>" + ReturnVideoID + "</FSVIDEO_ID>");

            sb.AppendLine("</Data>");
            sb.AppendLine("</Datas>");
            //參數剖析
            return sb.ToString();



        }


        //取得節目段落資料,如有段落資料則取段落資料,如果沒有則產生預設段落
        public DataTable getProgSegment1(string FSProgID, string FNEpisode, string FSChannel_ID, string FNDUR, string FNDEF_MIN, string FNDEF_SEC, string DEFSEG)
        {
            //DataTable dtProgSeg = new DataTable();
            //每一個節目,先查詢節目段落看是否有資料
            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection1 = new SqlConnection(sqlConnStr);
            if (connection1.State == System.Data.ConnectionState.Closed)
            {
                connection1.Open();
            }
            StringBuilder sb = new StringBuilder();
            SqlDataAdapter daProgSeg = new SqlDataAdapter("SP_Q_TBPGM_PROG_SEG", connection1);
            DataTable dtProgSeg = new DataTable();
            DataTable dtProgSegDefault = new DataTable();
            SqlParameter paraPROGID = new SqlParameter("@FSPROG_ID", FSProgID);
            daProgSeg.SelectCommand.Parameters.Add(paraPROGID);
            SqlParameter paraEPISODE = new SqlParameter("@FNEPISODE", FNEpisode);
            daProgSeg.SelectCommand.Parameters.Add(paraEPISODE);
            SqlParameter paraChannelID = new SqlParameter("@FSCHANNEL_ID", FSChannel_ID);
            daProgSeg.SelectCommand.Parameters.Add(paraChannelID);
            daProgSeg.SelectCommand.CommandType = CommandType.StoredProcedure;
            daProgSeg.Fill(dtProgSeg);
            if (dtProgSeg.Rows.Count > 0)
            {
                return dtProgSeg;
            }
            else //如果查無資料則採用預設段落 預設段落先給一段,如果他們之後有規則的話
            {



                string BDSeg;
                string BDDur;

                BDSeg = DEFSEG;
                if (BDSeg == "0")
                    BDSeg = "1";
                BDDur = ((int)Math.Floor(int.Parse(FNDUR) * 0.8)).ToString();

                if (FNDEF_MIN == "0" && FNDEF_SEC == "0")
                {
                    BDDur = ((int)Math.Floor(int.Parse(FNDUR) * 0.8)).ToString();
                    FNDEF_MIN = BDDur;
                }
                else
                {
                    BDDur = FNDEF_MIN;
                }
                dtProgSegDefault.Columns.Add("FSID", typeof(string));
                dtProgSegDefault.Columns.Add("FNEPISODE", typeof(string));
                dtProgSegDefault.Columns.Add("FSBEG_TIMECODE", typeof(string));
                dtProgSegDefault.Columns.Add("FSEND_TIMECODE", typeof(string));
                dtProgSegDefault.Columns.Add("FCLOW_RES", typeof(string));
                dtProgSegDefault.Columns.Add("FNSEG_ID", typeof(string));
                dtProgSegDefault.Columns.Add("FSVIDEO_ID", typeof(string));
                dtProgSegDefault.Columns.Add("FSFILE_NO", typeof(string));


                //int NewSeg = 1;

                //if (int.Parse(BDSeg) != 1)
                //{
                int n = 1;
                //int allDur =(int)Math.Floor(int.Parse(FNDUR) * 0.8);
                int allDur = int.Parse(FNDEF_MIN); //剩餘所有節目長度
                int nowDur;  //目前剩餘節目長度
                string nowDurString;
                //BDSeg 表示節目段數
                //nowDur = (int)(Math.Floor((double)allDur / (double)int.Parse(BDSeg)));  //此為採用平均分配

                //規則為 當剩餘長度大於剩餘段落*10則除了最後一段外都為10分鐘,不然就採平均
                if (int.Parse(FNDEF_MIN) > int.Parse(BDSeg) * 10)
                    nowDur = 10;
                else
                    nowDur = (int)(Math.Floor((double)allDur / (double)int.Parse(BDSeg)));

                while (n <= int.Parse(BDSeg))
                {
                    //n表示目前作業為哪段

                    nowDurString = MinToTimecode(nowDur);
                    if (n != int.Parse(BDSeg))
                        dtProgSegDefault.Rows.Add(FSProgID, FNEpisode, "00:00:00:00", nowDurString, "", n.ToString(), "", "");
                    else
                        dtProgSegDefault.Rows.Add(FSProgID, FNEpisode, "00:00:00:00", MinToTimecode(allDur).Substring(0, 6) + String.Format("{0:00}", int.Parse(FNDEF_SEC)) + MinToTimecode(allDur).Substring(8, 3), "", n.ToString(), "", "");

                    allDur = allDur - nowDur;
                    //Console.WriteLine("Current value of n is {0}", n);
                    n++;
                }
                //}
                //else
                //{
                //    dtProgSegDefault.Rows.Add(FSProgID, FNEpisode, "00:00:00:00", MinToTimecode(int.Parse(BDDur)), "", 1, FNDUR, "", "");
                //}
                return dtProgSegDefault;
            }
        }


        //取得節目集數的段落與長度
        [WebMethod()]
        public string GetProgDef(string strSQL, string parameters = null)
        {
            string FSProgID = "";
            string FNEpisode = "";
            string FSChannel_ID = "";

            string FNDUR = "";
            string FNDEF_MIN = "";
            string FNDEF_SEC = "";
            string FNSEQNO = "";
            string DEFSEG = "";

            if (parameters != "")
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(parameters);
                XmlNode xmlNode = xmldoc.FirstChild;
                for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                {
                    if (xmlNode.ChildNodes[i].Name == "FSProgID")
                        FSProgID = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "FNEpisode")
                        FNEpisode = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "FNSEQNO")
                        FNSEQNO = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "FSChannel_ID")
                        FSChannel_ID = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "FNDUR")
                        FNDUR = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "FNDEF_MIN")
                        FNDEF_MIN = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "FNDEF_SEC")
                        FNDEF_SEC = xmlNode.ChildNodes[i].FirstChild.Value;
                    if (xmlNode.ChildNodes[i].Name == "DEFSEG")
                        DEFSEG = xmlNode.ChildNodes[i].FirstChild.Value;
                }
            }

            //public DataTable getProgSegment(string FSProgID, string FNEpisode, string FNSEQNO, string FSChannel_ID, string FNDUR, string FNDEF_MIN, string FNDEF_SEC)
            //public DataTable getProgSegment(string FSProgID, string FNEpisode, string FNSEQNO, string FSChannel_ID, string FNDUR, string FNDEF_MIN, string FNDEF_SEC)
            DataTable TempTable = new DataTable();

            TempTable = getProgSegment1(FSProgID, FNEpisode, FSChannel_ID, FNDUR, FNDEF_MIN, FNDEF_SEC, DEFSEG);

            //回傳段落資料
            StringBuilder sb = new StringBuilder();
            DataTable dt = new DataTable();
            //SqlCommand sqlcmd = new SqlCommand(strSQL, connection);
            sb.AppendLine("<Datas>");
            for (int j = 0; j <= TempTable.Rows.Count - 1; j++)
            {
                sb.AppendLine("<Data>");
                sb.AppendLine("<FSID>" + FSProgID + "</FSID>");
                sb.AppendLine("<FNEPISODE>" + FNEpisode + "</FNEPISODE>");
                sb.AppendLine("<FSBEG_TIMECODE>" + TempTable.Rows[j]["FSBEG_TIMECODE"].ToString() + "</FSBEG_TIMECODE>");
                sb.AppendLine("<FSEND_TIMECODE>" + TempTable.Rows[j]["FSEND_TIMECODE"].ToString() + "</FSEND_TIMECODE>");
                sb.AppendLine("<FCLOW_RES>" + TempTable.Rows[j]["FCLOW_RES"].ToString() + "</FCLOW_RES>");
                string SegDur = "";
                SegDur = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TempTable.Rows[j]["FSEND_TIMECODE"].ToString()) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TempTable.Rows[j]["FSBEG_TIMECODE"].ToString()));
                sb.AppendLine("<FNSEG_ID>" + TempTable.Rows[j]["FNSEG_ID"].ToString() + "</FNSEG_ID>");
                sb.AppendLine("<FNDUR_ID>" + SegDur + "</FNDUR_ID>");
                sb.AppendLine("<FSVIDEO_ID>" + TempTable.Rows[j]["FSVIDEO_ID"].ToString() + "</FSVIDEO_ID>");
                sb.AppendLine("<FSFILE_NO>" + TempTable.Rows[j]["FSFILE_NO"].ToString() + "</FSFILE_NO>");
                sb.AppendLine("</Data>");

            }

            sb.AppendLine("</Datas>");
            //參數剖析
            return sb.ToString();



        }


        //檢查短帶區間
        [WebMethod()]
        public string Do_Check_Promo_Zone(string strSQL, string parameters = null)
        {

            //參數剖析
            StringBuilder sb = new StringBuilder();
            if (parameters != "")
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(parameters);
                XmlNode xmlNode = xmldoc.FirstChild;
                String sqlConnStr = "";
                sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
                SqlConnection myconnection1 = new SqlConnection(sqlConnStr);
                myconnection1.Open();
                try
                {
                    for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                    {
                        string FSPROMO_ID = "";
                        string FSPROMO_NAME = "";
                        string FSPLAY_TIME = "";
                        string FSCHANNEL_ID = "";
                        string FDDATE = "";
                        for (int j = 0; j <= xmlNode.ChildNodes[i].ChildNodes.Count - 1; j++)
                        {
                            if (xmlNode.ChildNodes[i].ChildNodes[j].FirstChild == null || xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value == "")
                            {
                            }
                            else
                            {
                                if (xmlNode.ChildNodes[i].ChildNodes[j].Name == "FSPROMO_ID")
                                    FSPROMO_ID = xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value;
                                if (xmlNode.ChildNodes[i].ChildNodes[j].Name == "FSPROMO_NAME")
                                    FSPROMO_NAME = xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value;
                                if (xmlNode.ChildNodes[i].ChildNodes[j].Name == "FSPLAY_TIME")
                                    FSPLAY_TIME = xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value;
                                if (xmlNode.ChildNodes[i].ChildNodes[j].Name == "FSCHANNEL_ID")
                                    FSCHANNEL_ID = xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value;
                                if (xmlNode.ChildNodes[i].ChildNodes[j].Name == "FDDATE")
                                    FDDATE = xmlNode.ChildNodes[i].ChildNodes[j].FirstChild.Value;
                            }
                        }
                        string WeekCond = "";
                        if ((Convert.ToDateTime(FDDATE)).DayOfWeek.ToString() == "Monday")
                            WeekCond = "1______";
                        else if ((Convert.ToDateTime(FDDATE)).DayOfWeek.ToString() == "Tuesday")
                            WeekCond = "_1_____";
                        else if ((Convert.ToDateTime(FDDATE)).DayOfWeek.ToString() == "Wednesday")
                            WeekCond = "__1____";
                        else if ((Convert.ToDateTime(FDDATE)).DayOfWeek.ToString() == "Thursday")
                            WeekCond = "___1___";
                        else if ((Convert.ToDateTime(FDDATE)).DayOfWeek.ToString() == "Friday")
                            WeekCond = "____1__";
                        else if ((Convert.ToDateTime(FDDATE)).DayOfWeek.ToString() == "Saturday")
                            WeekCond = "_____1_";
                        else if ((Convert.ToDateTime(FDDATE)).DayOfWeek.ToString() == "Sunday")
                            WeekCond = "______1";

                        //開始查詢短帶是否在有效區間
                        string Sqltxt1;


                        Sqltxt1 = "select FDBEG_DATE,FDEND_DATE,FSBEG_TIME,FSEND_TIME,FCDATE_TIME_TYPE  ";
                        Sqltxt1 = Sqltxt1 + " from TBPGM_PROMO_EFFECTIVE_ZONE where ";
                        Sqltxt1 = Sqltxt1 + "fspromo_id='" + FSPROMO_ID + "' and ";
                        Sqltxt1 = Sqltxt1 + " fdbeg_date<='" + FDDATE + "' and fdend_date>='" + FDDATE + "' ";
                        Sqltxt1 = Sqltxt1 + " and FSCHANNEL_ID='" + FSCHANNEL_ID + "' ";

                        Sqltxt1 = Sqltxt1 + " and fsweek like '" + WeekCond + "'";

                        SqlCommand mycom1 = new SqlCommand();
                        mycom1.Connection = myconnection1;
                        mycom1.CommandText = Sqltxt1;
                        SqlDataReader myReader;
                        myReader = mycom1.ExecuteReader();

                        //SqlDataAdapter daPromo_Zone = new SqlDataAdapter(Sqltxt1, connection);
                        //DataTable dtPromo_Zone = new DataTable();
                        //daPromo_Zone.Fill(dtPromo_Zone);
                        bool hasZone = false;

                        if (myReader.HasRows == true)
                        {
                            while (myReader.Read())
                            {
                                string FDBEG_DATE, FDEND_DATE, FSBEG_TIME, FSEND_TIME, FCDATE_TIME_TYPE;
                                if (myReader.IsDBNull(0) != true)
                                    FDBEG_DATE = myReader.GetDateTime(0).ToString();
                                else
                                    FDBEG_DATE = "";

                                if (myReader.IsDBNull(1) != true)
                                    FDEND_DATE = myReader.GetDateTime(1).ToString();
                                else
                                    FDEND_DATE = "";
                                if (myReader.IsDBNull(2) != true)
                                    FSBEG_TIME = myReader.GetString(2);
                                else
                                    FSBEG_TIME = "";
                                if (myReader.IsDBNull(3) != true)
                                    FSEND_TIME = myReader.GetString(3);
                                else
                                    FSEND_TIME = "";
                                if (myReader.IsDBNull(4) != true)
                                    FCDATE_TIME_TYPE = myReader.GetString(4);
                                else
                                    FCDATE_TIME_TYPE = "";

                                if (FSBEG_TIME != "" && FSEND_TIME != "")
                                {
                                    if (FCDATE_TIME_TYPE == "1") //日期有效區間
                                    {
                                        if (FDDATE == FDBEG_DATE && FDDATE == FDEND_DATE)
                                        {
                                            if (int.Parse(FSPLAY_TIME.Substring(0, 2) + FSPLAY_TIME.Substring(3, 2)) >= int.Parse(FSBEG_TIME) && int.Parse(FSPLAY_TIME.Substring(0, 2) + FSPLAY_TIME.Substring(3, 2)) <= int.Parse(FSEND_TIME))
                                            {
                                                hasZone = true;
                                            }
                                        }
                                        else if (FDDATE == FDBEG_DATE)
                                        {
                                            if (int.Parse(FSPLAY_TIME.Substring(0, 2) + FSPLAY_TIME.Substring(3, 2)) >= int.Parse(FSBEG_TIME))
                                            {
                                                hasZone = true;
                                            }
                                        }
                                        else if (FDDATE == FDEND_DATE)
                                        {
                                            if (int.Parse(FSPLAY_TIME.Substring(0, 2) + FSPLAY_TIME.Substring(3, 2)) <= int.Parse(FSEND_TIME))
                                            {
                                                hasZone = true;
                                            }
                                        }
                                        else
                                            hasZone = true;
                                    }
                                    else if (FCDATE_TIME_TYPE == "2")  //時間有效區間
                                    {
                                        if (int.Parse(FSPLAY_TIME.Substring(0, 2) + FSPLAY_TIME.Substring(3, 2)) >= int.Parse(FSBEG_TIME) && int.Parse(FSPLAY_TIME.Substring(0, 2) + FSPLAY_TIME.Substring(3, 2)) <= int.Parse(FSEND_TIME))
                                        {
                                            hasZone = true;
                                        }
                                    }
                                    else
                                    {
                                        hasZone = true;
                                    }
                                }
                                else
                                    hasZone = true;

                            }
                        }

                        myReader.Close();
                        if (hasZone == false)
                            sb.AppendLine("短帶:[" + FSPROMO_NAME + "]播出時間:[" + FSPLAY_TIME + "]不在有效區間");
                    } //  for (int i = 0; i <= xmlNode.ChildNodes.Count - 1; i++)
                    return sb.ToString();
                }
                catch (Exception ex)
                {

                    StringBuilder sbError = new StringBuilder();
                    //sbError.AppendLine("<Errors>" + ex.Message + "</Errors>");
                    sbError.AppendLine(ex.Message);
                    return sbError.ToString();
                }
                finally
                {

                    myconnection1.Close();
                    myconnection1.Dispose();

                    GC.Collect();
                }

            }

            return sb.ToString();

        }

        //取得置換節目在TBPGM_QUEUE的詳細資料
        [WebMethod()]
        public string Get_QUEUE_Prog(string FSPROG_ID, string FNSEQNO, string FDDATE, string FSCHANNEL_ID)
        {

            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            //SqlConnection myconnection = new SqlConnection(sqlConnStr);
            SqlConnection myconnection1 = new SqlConnection(sqlConnStr);
            SqlDataReader myReader1;
            SqlCommand mycom = new SqlCommand();
            mycom.Connection = myconnection1;
            myconnection1.Open();
            string Sqltxt1;

            Sqltxt1 = "Select A.FSPROG_ID,A.FNSEQNO,A.FDDATE,A.FSCHANNEL_ID,A.FSBEG_TIME,A.FSEND_TIME, ";
            Sqltxt1 = Sqltxt1 + "A.FNREPLAY,A.FNEPISODE,A.FSTAPENO,A.FSPROG_NAME,A.FNLIVE,A.FSSIGNAL,A.FNDEP,A.FNONOUT,";
            Sqltxt1 = Sqltxt1 + "A.FSMEMO,A.FSMEMO1,B.FSCHANNEL_NAME,C.FNSEG,C.FNDUR,A.FNDEF_MIN,A.FNDEF_SEC ";
            Sqltxt1 = Sqltxt1 + "from TBPGM_QUEUE A,TBZCHANNEL B,TBPGM_BANK_DETAIL C ";
            Sqltxt1 = Sqltxt1 + "where A.FSCHANNEL_ID=B.FSCHANNEL_ID and A.FSPROG_ID=C.FSPROG_ID and A.FNSEQNO=C.FNSEQNO ";
            Sqltxt1 = Sqltxt1 + "and A.fsprog_id='" + FSPROG_ID + "'";
            Sqltxt1 = Sqltxt1 + " and A.FNSEQNO=" + FNSEQNO + " and A.FSCHANNEL_ID='" + FSCHANNEL_ID + "' and A.fddate='" + FDDATE + "'";
            mycom.CommandText = Sqltxt1;
            myReader1 = mycom.ExecuteReader();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<Datas>");
            if (myReader1.HasRows == true)
            {
                myReader1.Read();
                //while (myReader1.Read())
                //{
                sb.AppendLine("<Data>");

                sb.AppendLine("<FSPROG_ID>" + FSPROG_ID + "</FSPROG_ID>");
                sb.AppendLine("<FNSEQNO>" + FNSEQNO + "</FNSEQNO>");
                sb.AppendLine("<FDDATE>" + FDDATE + "</FDDATE>");
                sb.AppendLine("<FSCHANNEL_ID>" + FSCHANNEL_ID + "</FSCHANNEL_ID>");

                sb.AppendLine("<FSBEG_TIME>" + myReader1.GetString(4) + "</FSBEG_TIME>");
                sb.AppendLine("<FSEND_TIME>" + myReader1.GetString(5) + "</FSEND_TIME>");
                sb.AppendLine("<FNREPLAY>" + myReader1.GetInt32(6).ToString() + "</FNREPLAY>");
                sb.AppendLine("<FNEPISODE>" + myReader1.GetInt32(7).ToString() + "</FNEPISODE>");
                sb.AppendLine("<FSTAPENO>" + myReader1.GetString(8) + "</FSTAPENO>");
                sb.AppendLine("<FSPROG_NAME>" + myReader1.GetString(9) + "</FSPROG_NAME>");
                sb.AppendLine("<FNLIVE>" + myReader1.GetInt32(10).ToString() + "</FNLIVE>");
                if (myReader1.IsDBNull(11) == false)
                    sb.AppendLine("<FSSIGNAL>" + myReader1.GetString(11) + "</FSSIGNAL>");
                else
                    sb.AppendLine("<FSSIGNAL>" + "" + "</FSSIGNAL>");

                if (myReader1.IsDBNull(12) == false)
                    sb.AppendLine("<FNDEP>" + myReader1.GetInt32(12).ToString() + "</FNDEP>");
                else
                    sb.AppendLine("<FNDEP>" + "" + "</FNDEP>");

                if (myReader1.IsDBNull(13) == false)
                    sb.AppendLine("<FNONOUT>" + myReader1.GetInt32(13).ToString() + "</FNONOUT>");
                else
                    sb.AppendLine("<FNONOUT>" + "" + "</FNONOUT>");

                if (myReader1.IsDBNull(14) == false)
                    sb.AppendLine("<FSMEMO>" + myReader1.GetString(14) + "</FSMEMO>");
                else
                    sb.AppendLine("<FSMEMO>" + "" + "</FSMEMO>");
                if (myReader1.IsDBNull(15) == false)
                    sb.AppendLine("<FSMEMO1>" + myReader1.GetString(15) + "</FSMEMO1>");
                else
                    sb.AppendLine("<FSMEMO1>" + "" + "</FSMEMO1>");
                if (myReader1.IsDBNull(16) == false)
                    sb.AppendLine("<FSCHANNEL_NAME>" + myReader1.GetString(16) + "</FSCHANNEL_NAME>");
                else
                    sb.AppendLine("<FSCHANNEL_NAME>" + "" + "</FSCHANNEL_NAME>");
                if (myReader1.IsDBNull(17) == false)
                    sb.AppendLine("<FNSEG>" + myReader1.GetInt32(17).ToString() + "</FNSEG>");
                else
                    sb.AppendLine("<FNSEG>" + "" + "</FNSEG>");

                if (myReader1.IsDBNull(18) == false)
                    sb.AppendLine("<FNDUR>" + myReader1.GetInt32(18).ToString() + "</FNDUR>");
                else
                    sb.AppendLine("<FNDUR>" + "" + "</FNDUR>");
                if (myReader1.IsDBNull(19) == false)
                    sb.AppendLine("<FNDEF_MIN>" + myReader1.GetInt32(19).ToString() + "</FNDEF_MIN>");
                else
                    sb.AppendLine("<FNDEF_MIN>" + "" + "</FNDEF_MIN>");
                if (myReader1.IsDBNull(20) == false)
                    sb.AppendLine("<FNDEF_SEC>" + myReader1.GetInt32(20).ToString() + "</FNDEF_SEC>");
                else
                    sb.AppendLine("<FNDEF_SEC>" + "" + "</FNDEF_SEC>");

                sb.AppendLine("</Data>");

            }
            //回傳段落資料


            sb.AppendLine("</Datas>");
            //參數剖析
            return sb.ToString();



        }



        [WebMethod()]
        public string ExportFile(List<ArrPromoList> InList)
        {
            PGM_ARR_PROMOAddSOM.Clear();
            foreach (ArrPromoList TemArrPromoList in InList)
            {
                PGM_ARR_PROMOAddSOM.Add(AddSOMToArrPromoList(TemArrPromoList));
            }
            string TempChannelID = "";
            string TempDate = "";
            string TempChannelType = "";
            List<ArrPromoListAddSOM> UniqueList = new List<ArrPromoListAddSOM>();
            #region 開始建立一個獨立清單
            try
            {
                int TempI = 1;
                //要先建立一個獨立清單,不包含重複資料
                #region 要先建立一個獨立清單,不包含重複資料

                foreach (ArrPromoListAddSOM TemArrPromoList in PGM_ARR_PROMOAddSOM)
                {
                    if (TemArrPromoList.FDDATE != "" && TemArrPromoList.FDDATE != null)
                    {
                        TempChannelID = TemArrPromoList.FSCHANNEL_ID;
                        TempDate = TemArrPromoList.FDDATE;
                        ///開始取得頻道類型,看是否為HD頻道
                        #region 開始取得頻道類型,看是否為HD頻道
                        String sqlConnStr = "";
                        sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
                        SqlConnection connection = new SqlConnection(sqlConnStr);
                        StringBuilder sb = new StringBuilder();

                        SqlDataAdapter da = new SqlDataAdapter("SP_Q_TBZCHANNEL", connection);
                        DataTable dt = new DataTable();
                        SqlParameter para1 = new SqlParameter("@FSCHANNEL_ID", TempChannelID);

                        da.SelectCommand.Parameters.Add(para1);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;


                        try
                        {
                            if (connection.State == System.Data.ConnectionState.Closed)
                            {
                                connection.Open();
                            }

                            da.Fill(dt);

                            if (dt.Rows.Count > 0)
                            {
                                if (Convert.IsDBNull(dt.Rows[0]["FSCHANNEL_TYPE"]))
                                    TempChannelType = "";
                                else
                                    TempChannelType = dt.Rows[0]["FSCHANNEL_TYPE"].ToString();
                            }

                            connection.Close();
                            connection.Dispose();
                        }

                        catch (Exception ex)
                        {
                            StringBuilder sbError = new StringBuilder();
                            sbError.AppendLine("<Errors>" + ex.Message + "</Errors>");
                            return sbError.ToString();
                        }

                        #endregion
                    }
                    //先檢查是否有加入過清單
                    bool NeedAdd = false;
                    foreach (ArrPromoListAddSOM TempUniqueList in UniqueList)
                    {
                        if (TemArrPromoList.FCTYPE == "G") //節目則要取段落,才表示唯一
                        {
                            if (TemArrPromoList.FSVIDEO_ID == TempUniqueList.FSVIDEO_ID && TemArrPromoList.FNBREAK_NO == TempUniqueList.FNBREAK_NO)
                            {
                                NeedAdd = true;
                            }
                        }
                        else //Promo則不需要取段落表示唯一
                        {
                            if (TemArrPromoList.FSVIDEO_ID == TempUniqueList.FSVIDEO_ID)
                            {
                                NeedAdd = true;
                            }
                        }
                    }
                    if (NeedAdd == false) //沒被加入過清單
                    {
                        UniqueList.Add(TemArrPromoList);
                    }
                }
                #endregion
            }
            catch
            {
            }

            #endregion

            bool ISLive = false;

            try
            {
                //開啟建立檔案
                string FileChannelDate = "";
                FileChannelDate = TempChannelID + String.Format("{0:00}", Convert.ToDateTime(TempDate).Year) + String.Format("{0:00}", Convert.ToDateTime(TempDate).Month) + String.Format("{0:00}", Convert.ToDateTime(TempDate).Day);
                string lstFileName = "";
                lstFileName = FileChannelDate + ".lst";

                string DateString = String.Format("{0:00}", Convert.ToDateTime(TempDate).Year) + String.Format("{0:00}", Convert.ToDateTime(TempDate).Month) + String.Format("{0:00}", Convert.ToDateTime(TempDate).Day);
                string CombinelstFileName = ""; //Program -Only

                string PlayListTXT = DateString + "_All";
                string ProgOnlyTXT = DateString + "_prog";

                int PlayListFileNumber = 0; //第幾個檔案,給HD TXT 使用,超過196筆資料要換一個檔名
                int PlayListRowNumber = 1;  //第幾筆資料,給HD TXT 使用,超過196筆資料要換一個檔名

                int ProgOnlyFileNumber = 0; //第幾個檔案,給HD TXT 使用,超過196筆資料要換一個檔名
                int ProgOnlyRowNumber = 1;  //第幾筆資料,給HD TXT 使用,超過196筆資料要換一個檔名


                string FirstPlayListFileName = ""; //首播清單 
                if (TempChannelID == "01") //公視主頻
                {
                    CombinelstFileName = "P" + DateString + ".lst";
                    FirstPlayListFileName = "FP" + DateString + ".lst";
                }
                else if (TempChannelID == "02") //DImo
                {
                    CombinelstFileName = "D" + DateString + ".lst";
                    FirstPlayListFileName = "FD" + DateString + ".lst";
                }
                else if (TempChannelID == "07") //客家
                {
                    CombinelstFileName = "H" + DateString + ".lst";
                    FirstPlayListFileName = "FH" + DateString + ".lst";
                }
                else if (TempChannelID == "08") //宏觀
                {
                    CombinelstFileName = "M" + DateString + ".lst";
                    FirstPlayListFileName = "FM" + DateString + ".lst";
                }
                else if (TempChannelID == "09") //原民
                {
                    CombinelstFileName = "I" + DateString + ".lst";
                    FirstPlayListFileName = "FI" + DateString + ".lst";
                }
                else if (TempChannelID == "11") //HD頻道
                {
                    CombinelstFileName = "N" + DateString + ".lst";
                    FirstPlayListFileName = "FN" + DateString + ".lst";
                    PlayListTXT = "O" + "N" + DateString + "_All";
                    ProgOnlyTXT = "O" + "N" + DateString + "_prog";
                }
                else if (TempChannelID == "51") //主頻MOD
                {
                    CombinelstFileName = "O" + DateString + ".lst";
                    FirstPlayListFileName = "FO" + DateString + ".lst";
                }
                else if (TempChannelID == "61") //HD MOD
                {
                    CombinelstFileName = "I" + DateString + ".lst";
                    FirstPlayListFileName = "FI" + DateString + ".lst";
                    PlayListTXT = "O" + "A" + DateString + "_All";
                    ProgOnlyTXT = "O" + "A" + DateString + "_prog";
                }
                else
                {
                    CombinelstFileName = "ProOnly" + DateString + ".lst";
                    FirstPlayListFileName = "FirPlay" + DateString + ".lst";
                }

                //if (File.Exists(HttpContext.Current.Server.MapPath("~/UploadForder/" + "1234.lst")) == true)
                //    File.Delete(HttpContext.Current.Server.MapPath("~/UploadForder/" + "1234.lst"));
                ////FileStream myFile = File.Open(@"C:\0Selfsys\1234.lst", FileMode.Open, FileAccess.ReadWrite);
                //FileStream myFile = File.Create(HttpContext.Current.Server.MapPath("~/UploadForder/" + "1234.lst"));
                if (File.Exists(HttpContext.Current.Server.MapPath("~/UploadFolder/" + lstFileName)) == true)
                    File.Delete(HttpContext.Current.Server.MapPath("~/UploadFolder/" + lstFileName));
                if (File.Exists(HttpContext.Current.Server.MapPath("~/UploadFolder/" + CombinelstFileName)) == true)
                    File.Delete(HttpContext.Current.Server.MapPath("~/UploadFolder/" + CombinelstFileName));                //FileStream myFile = File.Open(@"C:\0Selfsys\1234.lst", FileMode.Open, FileAccess.ReadWrite);
                if (File.Exists(HttpContext.Current.Server.MapPath("~/UploadFolder/" + FirstPlayListFileName)) == true)
                    File.Delete(HttpContext.Current.Server.MapPath("~/UploadFolder/" + FirstPlayListFileName));
                FileStream myFile = File.Create(HttpContext.Current.Server.MapPath("~/UploadFolder/" + lstFileName));
                BinaryWriter myWriter = new BinaryWriter(myFile);
                FileStream myCombineFile = File.Create(HttpContext.Current.Server.MapPath("~/UploadFolder/" + CombinelstFileName));
                BinaryWriter myCombineWriter = new BinaryWriter(myCombineFile);

                FileStream myPlayListTXTFile = File.Create(HttpContext.Current.Server.MapPath("~/UploadFolder/" + PlayListTXT + ".txt"));
                TextWriter myPlayListTXTWrite = new StreamWriter(myPlayListTXTFile);

                FileStream myProgOnlyTXTFile = File.Create(HttpContext.Current.Server.MapPath("~/UploadFolder/" + ProgOnlyTXT + ".txt"));
                TextWriter myProgOnlyTXTWrite = new StreamWriter(myProgOnlyTXTFile);



                #region 開始寫入TXT 節目清單
                if (TempChannelID == "11" || TempChannelID == "61")
                {
                    foreach (ArrPromoListAddSOM TempUniqueList in UniqueList)
                    {
                        if (TempUniqueList.FCTYPE == "G")
                        {
                            ProgOnlyRowNumber = ProgOnlyRowNumber + 1;
                            if (ProgOnlyRowNumber > 196)
                            {
                                myProgOnlyTXTWrite.Flush();
                                myProgOnlyTXTWrite.Close();
                                myProgOnlyTXTWrite.Dispose();
                                myProgOnlyTXTFile.Close();
                                myProgOnlyTXTFile.Dispose();
                                //myProgOnlyTXTFile = null;
                                ProgOnlyRowNumber = 1;
                                ProgOnlyFileNumber = ProgOnlyFileNumber + 1;
                                myProgOnlyTXTFile = File.Create(HttpContext.Current.Server.MapPath("~/UploadFolder/" + ProgOnlyTXT + "-" + ProgOnlyFileNumber.ToString() + ".txt"));
                                myProgOnlyTXTWrite = new StreamWriter(myProgOnlyTXTFile);
                            }
                            if (ProgOnlyRowNumber == 2)
                            {
                                myProgOnlyTXTWrite.WriteLine(WriteTXTProgOnly(TempUniqueList.FSVIDEO_ID.Substring(2, 6), TempUniqueList.FSPLAY_TIME, TempUniqueList.FSDURATION, TempUniqueList.FSSOM, TempUniqueList.FCTYPE, "hard", TempUniqueList.FSPROG_NAME, TempUniqueList.FNLIVE, TempUniqueList.FSSIGNAL));
                            }
                            else
                            {
                                myProgOnlyTXTWrite.WriteLine(WriteTXTProgOnly(TempUniqueList.FSVIDEO_ID.Substring(2, 6), TempUniqueList.FSPLAY_TIME, TempUniqueList.FSDURATION, TempUniqueList.FSSOM, TempUniqueList.FCTYPE, "auto", TempUniqueList.FSPROG_NAME, TempUniqueList.FNLIVE, TempUniqueList.FSSIGNAL));
                            }

                        }
                    }

                }

                #endregion






                int WorkRow = 0;
                bool beFirst = false; //是否為第一段
                bool bLast = false;  //是否為最後一段
                string PriorDataType = "";
                foreach (ArrPromoListAddSOM TemArrPromoList in PGM_ARR_PROMOAddSOM)
                {
                    string striType = "0";
                    string EventCOntrol = "";
                    string QF1 = "0";
                    string QF2 = "0";
                    string QF3 = "0";
                    string QF4 = "0";
                    string EF1 = "0";
                    string EF2 = "0";
                    string EF3 = "0";
                    string VideoID = TemArrPromoList.FSVIDEO_ID;
                    string Title = TemArrPromoList.FSNAME;
                    string PlayTime = TemArrPromoList.FSPLAY_TIME;
                    string Dur = TemArrPromoList.FSDURATION;
                    string SOM = TemArrPromoList.FSSOM;
                    //HD頻道只放六碼
                    if (TempChannelType == "002")
                    {
                        if (TemArrPromoList.FSVIDEO_ID.Length == 8)
                            VideoID = TemArrPromoList.FSVIDEO_ID.Substring(2, 6);
                    }

                    if (TemArrPromoList.FCTIME_TYPE == "O")
                        EventCOntrol = "AO";
                    //else if ((TemArrPromoList.FCTYPE=="G") || (TemArrPromoList.FNSUB_NO=="1"))
                    else if ((TemArrPromoList.FCTYPE == "G") || (TemArrPromoList.FCTYPE == "P" && TemArrPromoList.FNBREAK_NO != "0" && TemArrPromoList.FNSUB_NO == "1"))

                        EventCOntrol = "AN";
                    //else if ((TemArrPromoList.FNBREAK_NO=="1" && TemArrPromoList.FCTYPE=="G") || (TemArrPromoList.FNSUB_NO=="1"))
                    else
                        EventCOntrol = "A";

                    //如果前一筆資料為節目,此筆資料為短帶,則要設定為 AN
                    if (PriorDataType == "G" && TemArrPromoList.FCTYPE == "P")
                    {
                        if (TemArrPromoList.FCTIME_TYPE == "O")
                            EventCOntrol = "AO";
                        else
                            EventCOntrol = "AN";
                        //EventCOntrol = "AN";
                    }
                    //用完後將此筆資料的類型設定進PriorDataType,當下一筆時可以使用
                    PriorDataType = TemArrPromoList.FCTYPE;

                    if ((TemArrPromoList.FCTYPE == "G") && (TemArrPromoList.FNLIVE == "1")) //Live節目
                    {
                        if (TemArrPromoList.FCTIME_TYPE == "O")
                            EventCOntrol = "AUNO";
                        else
                            EventCOntrol = "AUN";

                    }

                    if ((TemArrPromoList.FCTYPE == "P") && (TemArrPromoList.FSSIGNAL != "PGM") && (TemArrPromoList.FSSIGNAL != "COMM") && (TemArrPromoList.FSSIGNAL != "")) //Live PROMO
                    {
                        if (TemArrPromoList.FCTIME_TYPE == "O")
                            EventCOntrol = "AUNO";
                        else
                            EventCOntrol = "AUN";

                    }
                    if (TemArrPromoList.FNLIVE == "1")  //如果為Live則要將訊號源寫入VIdeoID中
                    {
                        VideoID = TemArrPromoList.FSSIGNAL;
                    }
                    if (TemArrPromoList.FCTYPE == "G")  //,節目時 EF=FADE-FADE ,FAST  轉場效果
                    {
                        EF1 = "3";
                        EF2 = "0";
                        EF3 = "2";
                    }
                    //myWriter.Write(WriteProgByte(TemArrPromoList));
                    //myWriter.Write(WriteLouthByteArray(TemArrPromoList,string striType,string VideoID,string Title, string PlayTime,string Dur,string SOM,string EventControl,string QF1,string QF2,string QF3,string QF4,string EF1,string EF2,string EF3));
                    //寫入表中的資料,節目或宣傳帶

                    //if (TemArrPromoList.FSMEMO.IndexOf("時間") > -1)
                    //    EF2 = "1";

                    if (TemArrPromoList.FSMEMO.IndexOf("LOGO") > -1) //Logo那節目要+Lose
                        EF2 = "1";
                    if (TemArrPromoList.FSPROG_ID == "2004090" && TemArrPromoList.FCTYPE == "G") //訊號測試節目要+Lose
                        EF2 = "1";
                    myWriter.Write(WriteLouthByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3, TempChannelType));
                    if (TemArrPromoList.FCTYPE == "G")
                        if (TemArrPromoList.FNLIVE != "1") //不要Live資料,Live資料不要寫入 Program Only清單
                            myCombineWriter.Write(WriteLouthByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3, TempChannelType));

                    if (TemArrPromoList.FCTYPE == "G")
                    {
                        beFirst = false; //是否為第一段
                        bLast = false;  //是否為最後一段
                        if (WorkRow < PGM_ARR_PROMOAddSOM.Count - 1) //檢查此資料狀態,是否為第一段 和 是否為最後一段 和 是否只有一段
                        {
                            if (TemArrPromoList.FNBREAK_NO == "1")
                                beFirst = true;
                            int NextProgRow = WorkRow + 1;
                            while (NextProgRow < PGM_ARR_PROMOAddSOM.Count - 1 && PGM_ARR_PROMOAddSOM[NextProgRow].FCTYPE == "P")
                                NextProgRow = NextProgRow + 1;

                            if (PGM_ARR_PROMOAddSOM[NextProgRow].FCTYPE == "G")
                            {
                                if (PGM_ARR_PROMOAddSOM[NextProgRow].FSPROG_ID != TemArrPromoList.FSPROG_ID || PGM_ARR_PROMOAddSOM[NextProgRow].FNSEQNO != TemArrPromoList.FNSEQNO)
                                    bLast = true; //表示最後一段
                            }
                            else
                            {
                                bLast = true; //表示最後一段
                            }
                        }
                    }

                    #region 開始寫入TXT播出運行表
                    if (TempChannelID == "11" || TempChannelID == "61")
                    {
                        PlayListRowNumber = PlayListRowNumber + 1;
                        if (PlayListRowNumber > 196)
                        {

                            myPlayListTXTWrite.Flush();
                            myPlayListTXTWrite.Close();
                            myPlayListTXTWrite.Dispose();
                            myPlayListTXTFile.Close();
                            myPlayListTXTFile.Dispose();
                            //myPlayListTXTFile = null;
                            PlayListRowNumber = 1;
                            PlayListFileNumber = PlayListFileNumber + 1;
                            myPlayListTXTFile = File.Create(HttpContext.Current.Server.MapPath("~/UploadFolder/" + PlayListTXT + "-" + PlayListFileNumber.ToString() + ".txt"));
                            myPlayListTXTWrite = new StreamWriter(myPlayListTXTFile);
                        }

                        //if (FCTYPE == "G")
                        //    ReString = ReString + "hard" + "\t";
                        //else if (FNLIVE=="1")
                        //    ReString = ReString + "manual" + "\t";
                        //else
                        //    ReString = ReString + "auto" + "\t";
                        if (ISLive == true) //上一筆資料為Live
                            myPlayListTXTWrite.WriteLine(WriteTXTProgList(VideoID, PlayTime, Dur, SOM, TemArrPromoList.FCTYPE, "manual", TemArrPromoList.FSPROG_NAME, TemArrPromoList.FNLIVE, TemArrPromoList.FSSIGNAL));
                        else if (TemArrPromoList.FCTYPE == "G")
                            myPlayListTXTWrite.WriteLine(WriteTXTProgList(VideoID, PlayTime, Dur, SOM, TemArrPromoList.FCTYPE, "hard", TemArrPromoList.FSPROG_NAME, TemArrPromoList.FNLIVE, TemArrPromoList.FSSIGNAL));
                        else
                            myPlayListTXTWrite.WriteLine(WriteTXTProgList(VideoID, PlayTime, Dur, SOM, TemArrPromoList.FCTYPE, "auto", TemArrPromoList.FSPROG_NAME, TemArrPromoList.FNLIVE, TemArrPromoList.FSSIGNAL));

                        if (TemArrPromoList.FNLIVE == "1")
                            ISLive = true;
                        else
                            ISLive = false;

                        //ProgOnlyRowNumber = ProgOnlyRowNumber + 1;
                        //if (ProgOnlyRowNumber > 196)
                        //{
                        //    myProgOnlyTXTWrite.Flush();
                        //    myProgOnlyTXTWrite.Close();
                        //    myProgOnlyTXTWrite.Dispose();
                        //    myProgOnlyTXTFile.Close();
                        //    myProgOnlyTXTFile.Dispose();
                        //    //myProgOnlyTXTFile = null;
                        //    ProgOnlyRowNumber = 1;
                        //    ProgOnlyFileNumber = ProgOnlyFileNumber + 1;
                        //    myProgOnlyTXTFile = File.Create(HttpContext.Current.Server.MapPath("~/UploadFolder/" + ProgOnlyTXT + "-" + ProgOnlyFileNumber.ToString() + ".txt"));
                        //    myProgOnlyTXTWrite = new StreamWriter(myProgOnlyTXTFile);
                        //}
                        //myProgOnlyTXTWrite.WriteLine(WriteTXTProgList(VideoID, PlayTime, Dur, SOM, TemArrPromoList.FCTYPE, "auto", TemArrPromoList.FSPROG_NAME));
                    }

                    #endregion

                    WorkRow = WorkRow + 1;
                    //開始分析Memo的Louth Key

                    //} //if FCPLAY_Type="G"
                }
                myFile.Close();
                myFile.Dispose();

                myCombineFile.Close();
                myCombineFile.Dispose();

                //Create_First_Play_List("G" + lstFileName,TempDate,TempChannelID,"G");
                //Create_First_Play_List("P" + lstFileName, TempDate, TempChannelID, "P");
                //開始查詢首播清單並寫入檔案 FirstPlayListFileName

                //Create_First_Play_List(FirstPlayListFileName, TempDate, TempChannelID, "P");
                Create_First_Play_List(FirstPlayListFileName, TempDate, TempChannelID, "G", TempChannelType);

                //List<string> names = new List<string>();

                //names.Add(HttpContext.Current.Server.MapPath("~/UploadFolder/" + lstFileName));
                //names.Add(HttpContext.Current.Server.MapPath("~/UploadFolder/" + "G" + lstFileName));
                //names.Add(HttpContext.Current.Server.MapPath("~/UploadFolder/" +  "P" + lstFileName));

                //if (File.Exists(HttpContext.Current.Server.MapPath("~/UploadFolder/" + lstFileName + ".Zip")) == true)
                //    File.Delete(HttpContext.Current.Server.MapPath("~/UploadFolder/" + lstFileName + ".Zip"));
                //FileStream myFile1 = File.Create(HttpContext.Current.Server.MapPath("~/UploadFolder/" + lstFileName +".Zip"));
                //BinaryWriter myWriter1 = new BinaryWriter(myFile1);
                //myFile1.Close();
                //myFile1.Dispose();


                //myPlayListTXTWrite.WriteLine("csharp.net-informations.com");


                myPlayListTXTWrite.Flush();
                myPlayListTXTWrite.Close();
                myPlayListTXTWrite.Dispose();
                myPlayListTXTFile.Close();
                myPlayListTXTFile.Dispose();
                myPlayListTXTFile = null;



                //myProgOnlyTXTWrite.WriteLine("csharp.net-informations.com");

                myProgOnlyTXTWrite.Flush();
                myProgOnlyTXTWrite.Close();
                myProgOnlyTXTWrite.Dispose();
                myProgOnlyTXTFile.Close();
                myProgOnlyTXTFile.Dispose();
                myProgOnlyTXTFile = null;

                string ZipFIleName = "";
                ZipFIleName = FileChannelDate + ".Zip";




                #region 開始將檔案壓縮成Zip
                Crc32 crc = new Crc32();
                if (File.Exists(HttpContext.Current.Server.MapPath("~/UploadFolder/" + ZipFIleName)) == true)
                    File.Delete(HttpContext.Current.Server.MapPath("~/UploadFolder/" + ZipFIleName));
                ZipOutputStream s = new ZipOutputStream(File.Create(HttpContext.Current.Server.MapPath("~/UploadFolder/" + ZipFIleName)));

                s.SetLevel(9); // 0 - store only to 9 - means best compression

                AddZipFileClass BBB = new AddZipFileClass();
                BBB = AddZipFile(HttpContext.Current.Server.MapPath("~/UploadFolder/" + lstFileName), lstFileName);
                s.PutNextEntry(BBB.entry);
                s.Write(BBB.buffer, 0, BBB.buffer.Length);

                AddZipFileClass CCC = new AddZipFileClass();
                CCC = AddZipFile(HttpContext.Current.Server.MapPath("~/UploadFolder/" + CombinelstFileName), CombinelstFileName);
                s.PutNextEntry(CCC.entry);
                s.Write(CCC.buffer, 0, CCC.buffer.Length);

                AddZipFileClass DDD = new AddZipFileClass();
                DDD = AddZipFile(HttpContext.Current.Server.MapPath("~/UploadFolder/" + FirstPlayListFileName), FirstPlayListFileName);
                s.PutNextEntry(DDD.entry);
                s.Write(DDD.buffer, 0, DDD.buffer.Length);

                if (TempChannelID == "11" || TempChannelID == "61")
                {

                    for (int i = 0; i <= PlayListFileNumber; i++)
                    {

                        AddZipFileClass EEE = new AddZipFileClass();
                        if (i == 0)
                            EEE = AddZipFile(HttpContext.Current.Server.MapPath("~/UploadFolder/" + PlayListTXT + ".txt"), PlayListTXT + ".txt");
                        else
                            EEE = AddZipFile(HttpContext.Current.Server.MapPath("~/UploadFolder/" + PlayListTXT + "-" + i.ToString() + ".txt"), PlayListTXT + "-" + i.ToString() + ".txt");
                        s.PutNextEntry(EEE.entry);
                        s.Write(EEE.buffer, 0, EEE.buffer.Length);
                    }
                    for (int i = 0; i <= ProgOnlyFileNumber; i++)
                    {
                        AddZipFileClass EEE = new AddZipFileClass();
                        if (i == 0)
                            EEE = AddZipFile(HttpContext.Current.Server.MapPath("~/UploadFolder/" + ProgOnlyTXT + ".txt"), ProgOnlyTXT + ".txt");
                        else
                            EEE = AddZipFile(HttpContext.Current.Server.MapPath("~/UploadFolder/" + ProgOnlyTXT + "-" + i.ToString() + ".txt"), ProgOnlyTXT + "-" + i.ToString() + ".txt");
                        s.PutNextEntry(EEE.entry);
                        s.Write(EEE.buffer, 0, EEE.buffer.Length);
                    }
                }

                s.Finish();
                s.Close();

                #endregion

                string UploadPath;

                UploadPath = PTS_MAM3.Web.Properties.Settings.Default.UploadPath;

                //return UploadPath + lstFileName;
                return UploadPath + ZipFIleName;

                //return "http://" + HttpContext.Current.Request.Url.Host + "/UploadFolder/" + lstFileName;
                //return HttpContext.Current.Request.ApplicationPath;
                //return HttpContext.Current.Server.MapPath("~/UploadFolder/" + lstFileName);
                //return "http://localhost/PTS_MAM3.Web/UploadFolder/" + lstFileName;

            }
            catch (InvalidCastException e)
            {
                return "";
            }


        }

        //轉換頻道ID到代碼
        [WebMethod()]
        public string TransChannelIDToAliasName(string FSCHANNEL_ID)
        {
            string ChannelAliasName = "";
            if (FSCHANNEL_ID == "01") //公視主頻
                ChannelAliasName = "P";
            else if (FSCHANNEL_ID == "02") //DImo
                ChannelAliasName = "D";
            else if (FSCHANNEL_ID == "07") //客家
                ChannelAliasName = "H";
            else if (FSCHANNEL_ID == "08") //宏觀
                ChannelAliasName = "M";
            else if (FSCHANNEL_ID == "09") //原民
                ChannelAliasName = "I";
            else if (FSCHANNEL_ID == "11") //HD頻道
                ChannelAliasName = "N";
            else if (FSCHANNEL_ID == "51") //主頻MOD
                ChannelAliasName = "O";
            else if (FSCHANNEL_ID == "61") //HD MOD
                ChannelAliasName = "A";
            return ChannelAliasName;
        }

        //轉出主控Filing表
        [WebMethod()]
        public string ExportFilingList(string FDDATE, string FSCHANNEL_ID)
        {
            #region 查詢資料庫取得Filing清單(每日播出檔案減昨日播出)放置於FilingList
            try
            {
                  String sqlConnStr = "";
                sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
                SqlConnection connection = new SqlConnection(sqlConnStr);
                StringBuilder sb = new StringBuilder();

                SqlDataAdapter da = new SqlDataAdapter("SP_Q_FILING_LIST", connection);
                DataTable dt = new DataTable();
                SqlParameter para = new SqlParameter("@FDDATE", FDDATE);
                da.SelectCommand.Parameters.Add(para);
                SqlParameter para1 = new SqlParameter("@FSCHANNEL_ID", FSCHANNEL_ID);
                da.SelectCommand.Parameters.Add(para1);

                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }
                da.Fill(dt);
                connection.Close();

                List<FilingTableData> FilingList=new  List<FilingTableData>();

              

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        FilingTableData FilingData = new FilingTableData();
                        if (Convert.IsDBNull(dt.Rows[i]["FSPROG_ID"]))
                            FilingData.FSPROG_ID = "";
                        else
                            FilingData.FSPROG_ID = dt.Rows[i]["FSPROG_ID"].ToString();

                        if (Convert.IsDBNull(dt.Rows[i]["FNEPISODE"]))
                            FilingData.FNEPISODE = "";
                        else
                            FilingData.FNEPISODE = dt.Rows[i]["FNEPISODE"].ToString();


                        if (Convert.IsDBNull(dt.Rows[i]["FSFILE_NO"]))
                            FilingData.FSFILE_NO = "";
                        else
                            FilingData.FSFILE_NO = dt.Rows[i]["FSFILE_NO"].ToString();

                        if (Convert.IsDBNull(dt.Rows[i]["fsprog_name"]))
                            FilingData.fsprog_name = "";
                        else
                            FilingData.fsprog_name = dt.Rows[i]["fsprog_name"].ToString();
                        if (Convert.IsDBNull(dt.Rows[i]["FSVIDEO_ID"]))
                            FilingData.FSVIDEO_ID = "";
                        else
                            FilingData.FSVIDEO_ID = dt.Rows[i]["FSVIDEO_ID"].ToString();
                        if (Convert.IsDBNull(dt.Rows[i]["FNSEG_ID"]))
                            FilingData.FNSEG_ID = "";
                        else
                            FilingData.FNSEG_ID = dt.Rows[i]["FNSEG_ID"].ToString();
                        if (Convert.IsDBNull(dt.Rows[i]["FSBEG_TIMECODE"]))
                            FilingData.FSBEG_TIMECODE = "";
                        else
                            FilingData.FSBEG_TIMECODE = dt.Rows[i]["FSBEG_TIMECODE"].ToString();
                        if (Convert.IsDBNull(dt.Rows[i]["FSEND_TIMECODE"]))
                            FilingData.FSEND_TIMECODE = "";
                        else
                            FilingData.FSEND_TIMECODE = dt.Rows[i]["FSEND_TIMECODE"].ToString();

                        FilingData.FSDURATION = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(FilingData.FSEND_TIMECODE) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(FilingData.FSBEG_TIMECODE));
                        FilingList.Add(FilingData);
                    }
                }
                else
                {
                    return "";
                }
            #endregion

                //開啟建立檔案
                string DateString = String.Format("{0:00}", Convert.ToDateTime(FDDATE).Year) + String.Format("{0:00}", Convert.ToDateTime(FDDATE).Month) + String.Format("{0:00}", Convert.ToDateTime(FDDATE).Day);
                


                string ChannelAliasName = TransChannelIDToAliasName(FSCHANNEL_ID);

                string FilingListTXT = "OF" + ChannelAliasName + DateString + ".txt";
                string M_FilingListTXT = "OF" + ChannelAliasName + DateString + "_M" + ".txt"; 

                //if (File.Exists(HttpContext.Current.Server.MapPath("~/UploadForder/" + "1234.lst")) == true)
                //    File.Delete(HttpContext.Current.Server.MapPath("~/UploadForder/" + "1234.lst"));
                ////FileStream myFile = File.Open(@"C:\0Selfsys\1234.lst", FileMode.Open, FileAccess.ReadWrite);
                //FileStream myFile = File.Create(HttpContext.Current.Server.MapPath("~/UploadForder/" + "1234.lst"));
                if (File.Exists(HttpContext.Current.Server.MapPath("~/UploadFolder/" + FilingListTXT)) == true)
                    File.Delete(HttpContext.Current.Server.MapPath("~/UploadFolder/" + FilingListTXT));
                if (File.Exists(HttpContext.Current.Server.MapPath("~/UploadFolder/" + M_FilingListTXT)) == true)
                    File.Delete(HttpContext.Current.Server.MapPath("~/UploadFolder/" + M_FilingListTXT));                //FileStream myFile = File.Open(@"C:\0Selfsys\1234.lst", FileMode.Open, FileAccess.ReadWrite);

                FileStream myFilingListFile = File.Create(HttpContext.Current.Server.MapPath("~/UploadFolder/" + FilingListTXT));
                TextWriter myFilingListWriter = new StreamWriter(myFilingListFile);

                FileStream myM_FilingListFile = File.Create(HttpContext.Current.Server.MapPath("~/UploadFolder/" + M_FilingListTXT));
                TextWriter myM_FilingListWrite = new StreamWriter(myM_FilingListFile);


                #region 開始寫入FilingListTXT 節目清單

                foreach (FilingTableData TempFilingData in FilingList)
                {
                    myFilingListWriter.WriteLine(WriteTXTProgOnly(TempFilingData.FSVIDEO_ID.Substring(2, 6), "00:00:00;00", TempFilingData.FSDURATION, TempFilingData.FSBEG_TIMECODE, "G", "auto", TempFilingData.fsprog_name, "0",""));
                }

                #endregion


                #region 開始寫入TXT播出運行表
                bool FirstDate = true;
                foreach (FilingTableData TempFilingData in FilingList)
                {
                    string EndBeginTimeCode="";//段落結束時間減一分鐘
                    if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TempFilingData.FSEND_TIMECODE) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe("00:01:00;00") >=0) 
                        EndBeginTimeCode= MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TempFilingData.FSEND_TIMECODE) - MAM_PTS_DLL.TimeCodeCalc.timecodetoframe("00:01:00;00"));
                    else
                        EndBeginTimeCode="00:00:00;00";
                    if (FirstDate == true)
                    {
                        myM_FilingListWrite.WriteLine(WriteTXTProgOnly(TempFilingData.FSVIDEO_ID.Substring(2, 6), "00:00:00;00", "00:00:59;28", TempFilingData.FSBEG_TIMECODE, "G", "manual", TempFilingData.fsprog_name, "0", ""));
                        myM_FilingListWrite.WriteLine(WriteTXTProgOnly(TempFilingData.FSVIDEO_ID.Substring(2, 6), "00:00:00;00", "00:00:59;28", EndBeginTimeCode, "G", "auto", TempFilingData.fsprog_name, "0", ""));
                        FirstDate = false;
                    }
                    else
                    {
                        myM_FilingListWrite.WriteLine(WriteTXTProgOnly(TempFilingData.FSVIDEO_ID.Substring(2, 6), "00:00:00;00", "00:00:59;28", TempFilingData.FSBEG_TIMECODE, "G", "auto", TempFilingData.fsprog_name, "0", ""));
                        myM_FilingListWrite.WriteLine(WriteTXTProgOnly(TempFilingData.FSVIDEO_ID.Substring(2, 6), "00:00:00;00", "00:00:59;28", EndBeginTimeCode, "G", "auto", TempFilingData.fsprog_name, "0", ""));
                    }
                }
                #endregion


                myFilingListWriter.Flush();
                myFilingListWriter.Close();
                myFilingListWriter.Dispose();
                myFilingListFile.Close();
                myFilingListFile.Dispose();
                myFilingListFile = null;


                myM_FilingListWrite.Flush();
                myM_FilingListWrite.Close();
                myM_FilingListWrite.Dispose();
                myM_FilingListFile.Close();
                myM_FilingListFile.Dispose();
                myM_FilingListFile = null;

                string ZipFIleName = "";
                ZipFIleName = "OF" + ChannelAliasName + DateString + ".Zip";


                #region 開始將檔案壓縮成Zip
                Crc32 crc = new Crc32();
                if (File.Exists(HttpContext.Current.Server.MapPath("~/UploadFolder/" + ZipFIleName)) == true)
                    File.Delete(HttpContext.Current.Server.MapPath("~/UploadFolder/" + ZipFIleName));
                ZipOutputStream s = new ZipOutputStream(File.Create(HttpContext.Current.Server.MapPath("~/UploadFolder/" + ZipFIleName)));

                s.SetLevel(9); // 0 - store only to 9 - means best compression

                AddZipFileClass BBB = new AddZipFileClass();
                BBB = AddZipFile(HttpContext.Current.Server.MapPath("~/UploadFolder/" + FilingListTXT), FilingListTXT);
                s.PutNextEntry(BBB.entry);
                s.Write(BBB.buffer, 0, BBB.buffer.Length);

                AddZipFileClass CCC = new AddZipFileClass();
                CCC = AddZipFile(HttpContext.Current.Server.MapPath("~/UploadFolder/" + M_FilingListTXT), M_FilingListTXT);
                s.PutNextEntry(CCC.entry);
                s.Write(CCC.buffer, 0, CCC.buffer.Length);

             
                s.Finish();
                s.Close();

                #endregion

                string UploadPath;

                UploadPath = PTS_MAM3.Web.Properties.Settings.Default.UploadPath;

                //return UploadPath + lstFileName;
                return UploadPath + ZipFIleName;

                //return "http://" + HttpContext.Current.Request.Url.Host + "/UploadFolder/" + lstFileName;
                //return HttpContext.Current.Request.ApplicationPath;
                //return HttpContext.Current.Server.MapPath("~/UploadFolder/" + lstFileName);
                //return "http://localhost/PTS_MAM3.Web/UploadFolder/" + lstFileName;

            }
            catch (InvalidCastException e)
            {
                return "";
            }


        }


        public class FilingPrintTableData
        {
            public string FSPROG_ID { get; set; }
            public string FNSEQNO { get; set; }
            public string FNEPISODE { get; set; }
            public string FSPROG_NAME { get; set; }
            public string FSVIDEO_ID { get; set; }
            public string FNBREAK_NO { get; set; }
            public string FSPLAY_TIME { get; set; }
            public string FDDATE { get; set; }
            public string FSDUR { get; set; }
            public string FSCHANNEL_NAME { get; set; }
        }


        //列印主控Filing表
        [WebMethod()]
        public string PrintFilingList(string FDDATE, string FSCHANNEL_ID, string PrintUserName)
        {
            #region 查詢資料庫取得Filing清單(每日播出檔案減昨日播出)放置於FilingList
            try
            {
                String sqlConnStr1 = "";
                sqlConnStr1 = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
                SqlConnection connection1 = new SqlConnection(sqlConnStr1);
                StringBuilder sb1 = new StringBuilder();

                SqlDataAdapter da = new SqlDataAdapter("SP_Q_PRINT_FILING_LIST", connection1);
                DataTable dt = new DataTable();
                SqlParameter para = new SqlParameter("@FDDATE", FDDATE);
                da.SelectCommand.Parameters.Add(para);
                SqlParameter para1 = new SqlParameter("@FSCHANNEL_ID", FSCHANNEL_ID);
                da.SelectCommand.Parameters.Add(para1);

                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                if (connection1.State == System.Data.ConnectionState.Closed)
                {
                    connection1.Open();
                }
                da.Fill(dt);
                connection1.Close();

                List<FilingPrintTableData> FilingPrintList = new List<FilingPrintTableData>();




                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        FilingPrintTableData FilingPrintData = new FilingPrintTableData();

                        if (Convert.IsDBNull(dt.Rows[i]["FSPROG_ID"]))
                            FilingPrintData.FSPROG_ID = "";
                        else
                            FilingPrintData.FSPROG_ID = dt.Rows[i]["FSPROG_ID"].ToString();

                        if (Convert.IsDBNull(dt.Rows[i]["FNSEQNO"]))
                            FilingPrintData.FNSEQNO = "";
                        else
                            FilingPrintData.FNSEQNO = dt.Rows[i]["FNSEQNO"].ToString();

                        if (Convert.IsDBNull(dt.Rows[i]["FNEPISODE"]))
                            FilingPrintData.FNEPISODE = "";
                        else
                            FilingPrintData.FNEPISODE = dt.Rows[i]["FNEPISODE"].ToString();


                        if (Convert.IsDBNull(dt.Rows[i]["FNBREAK_NO"]))
                            FilingPrintData.FNBREAK_NO = "";
                        else
                            FilingPrintData.FNBREAK_NO = dt.Rows[i]["FNBREAK_NO"].ToString();

                        if (Convert.IsDBNull(dt.Rows[i]["FSPROG_NAME"]))
                            FilingPrintData.FSPROG_NAME = "";
                        else
                            FilingPrintData.FSPROG_NAME = dt.Rows[i]["FSPROG_NAME"].ToString();
                        if (Convert.IsDBNull(dt.Rows[i]["FSVIDEO_ID"]))
                            FilingPrintData.FSVIDEO_ID = "";
                        else
                            FilingPrintData.FSVIDEO_ID = dt.Rows[i]["FSVIDEO_ID"].ToString();
                        if (Convert.IsDBNull(dt.Rows[i]["FSPLAY_TIME"]))
                            FilingPrintData.FSPLAY_TIME = "";
                        else
                            FilingPrintData.FSPLAY_TIME = dt.Rows[i]["FSPLAY_TIME"].ToString();
                        if (Convert.IsDBNull(dt.Rows[i]["FDDATE"]))
                            FilingPrintData.FDDATE = "";
                        else
                            FilingPrintData.FDDATE = FDDATE;
                        if (Convert.IsDBNull(dt.Rows[i]["FSDUR"]))
                            FilingPrintData.FSDUR = "";
                        else
                            FilingPrintData.FSDUR = dt.Rows[i]["FSDUR"].ToString();
                        if (Convert.IsDBNull(dt.Rows[i]["FSCHANNEL_NAME"]))
                            FilingPrintData.FSCHANNEL_NAME = "";
                        else
                            FilingPrintData.FSCHANNEL_NAME = dt.Rows[i]["FSCHANNEL_NAME"].ToString();

                        bool NeedAdd = false;
                        foreach (FilingPrintTableData TempFilingPrintData in FilingPrintList)
                        {
                            if (TempFilingPrintData.FSVIDEO_ID == FilingPrintData.FSVIDEO_ID && TempFilingPrintData.FNBREAK_NO == FilingPrintData.FNBREAK_NO)
                            {
                                NeedAdd = true;
                            }
                        }
                        if (NeedAdd == false)
                        {
                            FilingPrintList.Add(FilingPrintData);
                        }
                    }
                }
                else
                {
                    return "沒有資料";
                }
            
            #endregion
           


            //開始寫入報表
            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            StringBuilder sb = new StringBuilder();

            SqlTransaction tran = null;
            SqlCommand sqlcmd = new SqlCommand("SP_I_RPT_DAY_FILING_LIST", connection);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            Guid g;
            g = Guid.NewGuid();
            if (connection.State == System.Data.ConnectionState.Closed)
            {
                connection.Open();
            }

                sqlcmd.CommandText = "SP_I_RPT_DAY_FILING_LIST";
                if (FilingPrintList.Count > 0)
                {

                    int NOWRow=1;
                    tran = connection.BeginTransaction();
                    sqlcmd.Transaction = tran;
                    foreach (FilingPrintTableData NowFilingPrintData in FilingPrintList)
                    {
                        sqlcmd.Parameters.Clear();
                        SqlParameter para11 = new SqlParameter("@QUERY_KEY", g.ToString());
                        sqlcmd.Parameters.Add(para11);
                        Guid g1;
                        g1 = Guid.NewGuid();
                        SqlParameter para12 = new SqlParameter("@QUERY_KEY_SUBID", g1.ToString());
                        sqlcmd.Parameters.Add(para12);

                        SqlParameter para13 = new SqlParameter("@QUERY_BY", PrintUserName);
                        sqlcmd.Parameters.Add(para13);

                        SqlParameter para14 = new SqlParameter("@播出日期", NowFilingPrintData.FDDATE);
                        sqlcmd.Parameters.Add(para14);

                        SqlParameter para15 = new SqlParameter("@頻道", NowFilingPrintData.FSCHANNEL_NAME);
                        sqlcmd.Parameters.Add(para15);

                        SqlParameter para16 = new SqlParameter("@FNNO", NOWRow);
                        sqlcmd.Parameters.Add(para16);

                        SqlParameter para17 = new SqlParameter("@FSPLAY_TIME", NowFilingPrintData.FSPLAY_TIME);
                        sqlcmd.Parameters.Add(para17);

                        SqlParameter para18= new SqlParameter("@FSIN", "");
                        sqlcmd.Parameters.Add(para18);
                        
                        SqlParameter para19 = new SqlParameter("@VIDEO_ID", NowFilingPrintData.FSVIDEO_ID);
                        sqlcmd.Parameters.Add(para19);
                        
                        SqlParameter para20 = new SqlParameter("@FSOUT", "");
                        sqlcmd.Parameters.Add(para20);

                        SqlParameter para21 = new SqlParameter("@節目名稱", NowFilingPrintData.FSPROG_NAME);
                        sqlcmd.Parameters.Add(para21);

                        SqlParameter para22 = new SqlParameter("@集數", NowFilingPrintData.FNEPISODE);
                        sqlcmd.Parameters.Add(para22);

                        SqlParameter para23 = new SqlParameter("@長度", NowFilingPrintData.FSDUR);
                        sqlcmd.Parameters.Add(para23);

                        SqlParameter para24 = new SqlParameter("@FNBREAK_NO", NowFilingPrintData.FNBREAK_NO);
                        sqlcmd.Parameters.Add(para24);

                        SqlParameter para25 = new SqlParameter("@FSMEMO", "");
                        sqlcmd.Parameters.Add(para25);

                        NOWRow=NOWRow+1;
                        if (sqlcmd.ExecuteNonQuery() == 0)
                        {
                            tran.Rollback();
                            return "";
                        }
                    }
                    tran.Commit();
                }
                else
                {
                    return "";
                }
                connection.Close();
                connection.Dispose();
            

            String strURL = "";  //要傳回去的URL

            //取列印報表的系統設定檔
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string reportSrv = SettingObj.sysConfig_Read("/ServerConfig/ReportServer");

            strURL = reportSrv + "RPT_DAY_FILING_LIST" + "&QUERY_KEY=" + g.ToString() + "&rc:parameters=false&rs:Command=Render";
            //strURL _URL = "http://172.20.141.92/ReportServer/Pages/ReportViewer.aspx?%2fPTS_MAM3%2fRPT_TBPROPOSAL_01&QUERY_KEY=A3AE2DED-3451-4AF0-898D-04D1417D01AA&rc:parameters=false&rs:Command=Render";
            return Server.UrlEncode(strURL);
            }
            catch (Exception ex)
            {
                StringBuilder sbError = new StringBuilder();
                return ex.Message.ToString();
            }
            finally
            {

                GC.Collect();
            }

        }

        public class HDMCTapeList
        {
            public string FDDATE { get; set; }
            public string FSCHANNEL { get; set; }
            public string FNSEQNO { get; set; }
            public string FSPROG_ID { get; set; }
            public string FNEPISODE { get; set; }
            public string FNBREAK_NO { get; set; }
            public string FNSEQ { get; set; }
            public string FSPLAYTIME { get; set; }
            public string FSIN { get; set; }
            public string FSVIDEO_ID { get; set; }
            public string FSOUT { get; set; }
            public string FSPROG_NAME { get; set; }
            public string FSDURATION { get; set; }
            
        }
        //轉出檔案排表並可列印 全日長帶依播出順序由1開始排列，當日有重播則在OUT欄填入下一次播出順序，重播那則需在IN欄填入前一次播出順序
        [WebMethod()]
        public string PrintTapeList(List<ArrPromoList> InList,string PrintUserName)
        {
            List<HDMCTapeList> TapeList = new List<HDMCTapeList>();
            string TempChannelID = "";
            string TempDate = "";
            string TempChannelType = "";
            string TempChannel_Name = "";
            int NowRow = 1;
            #region 要建立資料
            foreach (ArrPromoList TempINList in InList)
            {
                //第一筆先查詢資料取得頻道名稱
                if (TempChannelID == "")
                {
                    TempChannelID = TempINList.FSCHANNEL_ID;
                    TempDate = TempINList.FDDATE;
                    #region 開始取得頻道類型,看是否為HD頻道
                    String sqlConnStr1 = "";
                    sqlConnStr1 = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
                    SqlConnection connection1 = new SqlConnection(sqlConnStr1);
                    StringBuilder sb1 = new StringBuilder();

                    SqlDataAdapter da = new SqlDataAdapter("SP_Q_TBZCHANNEL", connection1);
                    DataTable dt = new DataTable();
                    SqlParameter para1 = new SqlParameter("@FSCHANNEL_ID", TempChannelID);

                    da.SelectCommand.Parameters.Add(para1);
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;


                    try
                    {
                        if (connection1.State == System.Data.ConnectionState.Closed)
                        {
                            connection1.Open();
                        }

                        da.Fill(dt);

                        if (dt.Rows.Count > 0)
                        {
                            if (Convert.IsDBNull(dt.Rows[0]["FSCHANNEL_TYPE"]))
                                TempChannelType = "";
                            else
                                TempChannelType = dt.Rows[0]["FSCHANNEL_TYPE"].ToString();

                            if (Convert.IsDBNull(dt.Rows[0]["FSCHANNEL_NAME"]))
                                TempChannel_Name = "";
                            else
                                TempChannel_Name = dt.Rows[0]["FSCHANNEL_NAME"].ToString();
                        }

                        connection1.Close();
                        connection1.Dispose();
                    }

                    catch (Exception ex)
                    {
                        StringBuilder sbError = new StringBuilder();
                        sbError.AppendLine("<Errors>" + ex.Message + "</Errors>");
                        return sbError.ToString();
                    }

                    #endregion
                }

                //只加入每個節目第一段
                if (TempINList.FCTYPE == "G" && TempINList.FNBREAK_NO == "1")
                {
                    string ThisIN = "";
                    foreach (HDMCTapeList NowTapeListDate in TapeList)
                    {
                        if (NowTapeListDate.FSPROG_ID == TempINList.FSPROG_ID  && NowTapeListDate.FNEPISODE == TempINList.FNEPISODE && NowTapeListDate.FNBREAK_NO == TempINList.FNBREAK_NO )
                        {
                            if (NowTapeListDate.FSOUT != "")
                            {
                                NowTapeListDate.FSOUT = NowRow.ToString();
                                ThisIN = NowRow.ToString();
                            }
                           
                        }
                    }
                    //計算長度
                    int TempDur = 0;
                    foreach (ArrPromoList NowTapeListDate in InList)
                    {
                        if (NowTapeListDate.FSPROG_ID == TempINList.FSPROG_ID && NowTapeListDate.FNSEQNO == TempINList.FNSEQNO && NowTapeListDate.FCTYPE == "G")
                        {
                            TempDur = TempDur + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(NowTapeListDate.FSDURATION);
                        }
                    }

                    string TempSDur = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(TempDur);

                    HDMCTapeList TempTapeListData = new HDMCTapeList();
                    TempTapeListData.FDDATE = TempDate;
                    TempTapeListData.FSCHANNEL = TempChannel_Name;
                    TempTapeListData.FNSEQNO = TempINList.FNSEQNO;
                    TempTapeListData.FNSEQ = NowRow.ToString();
                    TempTapeListData.FSIN = ThisIN;
                    TempTapeListData.FSOUT = "";
                    TempTapeListData.FSPLAYTIME = TempINList.FSPLAY_TIME;
                    TempTapeListData.FSPROG_NAME = TempINList.FSPROG_NAME;
                    TempTapeListData.FSVIDEO_ID = TempINList.FSVIDEO_ID;
                    TempTapeListData.FSPROG_ID = TempINList.FSPROG_ID;
                    TempTapeListData.FNEPISODE = TempINList.FNEPISODE;
                    TempTapeListData.FNBREAK_NO = TempINList.FNBREAK_NO;
                    TempTapeListData.FSDURATION = String.Format("{0:000}", int.Parse(TempSDur.Substring(0, 2)) * 60 + int.Parse(TempSDur.Substring(3, 2))) + TempSDur.Substring(5, 6);
                    TapeList.Add(TempTapeListData);
                    NowRow = NowRow + 1;
                }
            }
            #endregion
           


           //開始寫入報表
            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            StringBuilder sb = new StringBuilder();

            SqlTransaction tran = null;
            SqlCommand sqlcmd = new SqlCommand("SP_I_RPT_DAY_TAPE_LIST", connection);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            Guid g;
            g = Guid.NewGuid();
            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                sqlcmd.CommandText = "SP_I_RPT_DAY_TAPE_LIST";
                if (TapeList.Count > 0)
                {
                    tran = connection.BeginTransaction();
                    sqlcmd.Transaction = tran;
                    foreach (HDMCTapeList NowTapeListDate in TapeList)
                    {
                        sqlcmd.Parameters.Clear();
                        SqlParameter para11 = new SqlParameter("@QUERY_KEY", g.ToString());
                        sqlcmd.Parameters.Add(para11);
                        Guid g1;
                        g1 = Guid.NewGuid();
                        SqlParameter para12 = new SqlParameter("@QUERY_KEY_SUBID", g1.ToString());
                        sqlcmd.Parameters.Add(para12);

                        SqlParameter para13 = new SqlParameter("@QUERY_BY", PrintUserName);
                        sqlcmd.Parameters.Add(para13);

                        SqlParameter para22 = new SqlParameter("@播出日期", NowTapeListDate.FDDATE);
                        sqlcmd.Parameters.Add(para22);

                        SqlParameter para23 = new SqlParameter("@頻道", NowTapeListDate.FSCHANNEL);
                        sqlcmd.Parameters.Add(para23);

                        SqlParameter para14 = new SqlParameter("@序號", NowTapeListDate.FNSEQ);
                        sqlcmd.Parameters.Add(para14);

                        SqlParameter para19 = new SqlParameter("@節目名稱", NowTapeListDate.FSPROG_NAME);
                        sqlcmd.Parameters.Add(para19);

                        SqlParameter para21 = new SqlParameter("@集數", NowTapeListDate.FNEPISODE);
                        sqlcmd.Parameters.Add(para21);

                        SqlParameter para15 = new SqlParameter("@播出時間", NowTapeListDate.FSPLAYTIME);
                        sqlcmd.Parameters.Add(para15);

                        SqlParameter para16 = new SqlParameter("@FSIN", NowTapeListDate.FSIN);
                        sqlcmd.Parameters.Add(para16);

                        SqlParameter para17 = new SqlParameter("@VIDEO_ID", NowTapeListDate.FSVIDEO_ID);
                        sqlcmd.Parameters.Add(para17);

                        SqlParameter para18 = new SqlParameter("@FSOUT", NowTapeListDate.FSOUT);
                        sqlcmd.Parameters.Add(para18);

                        SqlParameter para20 = new SqlParameter("@長度", NowTapeListDate.FSDURATION);
                        sqlcmd.Parameters.Add(para20);


                        if (sqlcmd.ExecuteNonQuery() == 0)
                        {
                            tran.Rollback();
                            return "";
                        }
                    }
                    tran.Commit();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                StringBuilder sbError = new StringBuilder();
                return ex.Message.ToString();
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }

            String strURL = "";  //要傳回去的URL

            //取列印報表的系統設定檔
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string reportSrv = SettingObj.sysConfig_Read("/ServerConfig/ReportServer");

            strURL = reportSrv + "RPT_DAY_TAPE_LIST" + "&QUERY_KEY=" + g.ToString() + "&rc:parameters=false&rs:Command=Render";
            //strURL _URL = "http://172.20.141.92/ReportServer/Pages/ReportViewer.aspx?%2fPTS_MAM3%2fRPT_TBPROPOSAL_01&QUERY_KEY=A3AE2DED-3451-4AF0-898D-04D1417D01AA&rc:parameters=false&rs:Command=Render";
            return Server.UrlEncode(strURL);


        }

        public class HDMCTapeListSign
        {
            public string FDDATE { get; set; }
            public string FSCHANNEL { get; set; }
            public string FNSEQNO { get; set; }
            public string FSPROG_ID { get; set; }
            public string FNEPISODE { get; set; }
            public string FNBREAK_NO { get; set; }
            public string FNSEQ { get; set; }
            public string FSVIDEO_ID { get; set; }
            public string FSPROG_NAME { get; set; }
            public string FSDURATION { get; set; }

        }
        //轉出檔案排簽收表並可列印 全日長帶依播出順序由1開始排列
        [WebMethod()]
        public string PrintTapeListSign(List<ArrPromoList> InList, string PrintUserName)
        {
            List<HDMCTapeListSign> TapeList = new List<HDMCTapeListSign>();
            string TempChannelID = "";
            string TempDate = "";
            string TempChannelType = "";
            string TempChannel_Name = "";
            int NowRow = 1;
            #region 要建立資料
            foreach (ArrPromoList TempINList in InList)
            {
                //第一筆先查詢資料取得頻道名稱
                if (TempChannelID == "")
                {
                    TempChannelID = TempINList.FSCHANNEL_ID;
                    TempDate = TempINList.FDDATE;
                    #region 開始取得頻道類型,看是否為HD頻道
                    String sqlConnStr1 = "";
                    sqlConnStr1 = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
                    SqlConnection connection1 = new SqlConnection(sqlConnStr1);
                    StringBuilder sb1 = new StringBuilder();

                    SqlDataAdapter da = new SqlDataAdapter("SP_Q_TBZCHANNEL", connection1);
                    DataTable dt = new DataTable();
                    SqlParameter para1 = new SqlParameter("@FSCHANNEL_ID", TempChannelID);

                    da.SelectCommand.Parameters.Add(para1);
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;


                    try
                    {
                        if (connection1.State == System.Data.ConnectionState.Closed)
                        {
                            connection1.Open();
                        }

                        da.Fill(dt);

                        if (dt.Rows.Count > 0)
                        {
                            if (Convert.IsDBNull(dt.Rows[0]["FSCHANNEL_TYPE"]))
                                TempChannelType = "";
                            else
                                TempChannelType = dt.Rows[0]["FSCHANNEL_TYPE"].ToString();

                            if (Convert.IsDBNull(dt.Rows[0]["FSCHANNEL_NAME"]))
                                TempChannel_Name = "";
                            else
                                TempChannel_Name = dt.Rows[0]["FSCHANNEL_NAME"].ToString();
                        }

                        connection1.Close();
                        connection1.Dispose();
                    }

                    catch (Exception ex)
                    {
                        StringBuilder sbError = new StringBuilder();
                        sbError.AppendLine("<Errors>" + ex.Message + "</Errors>");
                        return sbError.ToString();
                    }

                    #endregion
                }

                //只加入每個節目第一段
                //不加入收播 與訊號測試 與 Live節目
                if (TempINList.FCTYPE == "G" && TempINList.FNBREAK_NO == "1" && TempINList.FSPROG_ID != "2004090" && TempINList.FSPROG_ID != "2005459" && TempINList.FNLIVE != "1")
                {
                    //先檢查是否有加入過清單
                    bool NeedAdd = false;
                     foreach (HDMCTapeListSign NowTapeListDate in TapeList)
                    {
                        if (NowTapeListDate.FSPROG_ID == TempINList.FSPROG_ID && NowTapeListDate.FNEPISODE == TempINList.FNEPISODE)
                        {
                             NeedAdd = true;
                        }
                    }
                    if (NeedAdd == false) //沒被加入過清單
                    {

                        //計算長度
                        int TempDur = 0;
                        foreach (ArrPromoList NowTapeListDate in InList)
                        {
                            if (NowTapeListDate.FSPROG_ID == TempINList.FSPROG_ID && NowTapeListDate.FNSEQNO == TempINList.FNSEQNO && NowTapeListDate.FCTYPE == "G")
                            {
                                TempDur = TempDur + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(NowTapeListDate.FSDURATION);
                            }
                        }

                        string TempSDur = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(TempDur);

                        HDMCTapeListSign TempTapeListData = new HDMCTapeListSign();
                        TempTapeListData.FDDATE = TempDate;
                        TempTapeListData.FSCHANNEL = TempChannel_Name;
                        TempTapeListData.FNSEQNO = TempINList.FNSEQNO;
                        TempTapeListData.FNSEQ = NowRow.ToString();
                        TempTapeListData.FSPROG_NAME = TempINList.FSPROG_NAME;
                        TempTapeListData.FSVIDEO_ID = TempINList.FSVIDEO_ID;
                        TempTapeListData.FSPROG_ID = TempINList.FSPROG_ID;
                        TempTapeListData.FNEPISODE = TempINList.FNEPISODE;
                        TempTapeListData.FNBREAK_NO = TempINList.FNBREAK_NO;
                        TempTapeListData.FSDURATION = String.Format("{0:000}", int.Parse(TempSDur.Substring(0, 2)) * 60 + int.Parse(TempSDur.Substring(3, 2))) + TempSDur.Substring(5, 6);
                        TapeList.Add(TempTapeListData);
                        NowRow = NowRow + 1;
                    }

                  
                }
            }
            #endregion



            //開始寫入報表
            String sqlConnStr = "";
            sqlConnStr = ConfigurationManager.ConnectionStrings["SqlString"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);
            StringBuilder sb = new StringBuilder();

            SqlTransaction tran = null;
            SqlCommand sqlcmd = new SqlCommand("SP_I_RPT_DAY_TAPE_LIST_SIGN", connection);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            Guid g;
            g = Guid.NewGuid();
            try
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }
                sqlcmd.CommandText = "SP_I_RPT_DAY_TAPE_LIST_SIGN";
                if (TapeList.Count > 0)
                {
                    tran = connection.BeginTransaction();
                    sqlcmd.Transaction = tran;
                    foreach (HDMCTapeListSign NowTapeListDate in TapeList)
                    {
                        sqlcmd.Parameters.Clear();
                        SqlParameter para11 = new SqlParameter("@QUERY_KEY", g.ToString());
                        sqlcmd.Parameters.Add(para11);
                        Guid g1;
                        g1 = Guid.NewGuid();
                        SqlParameter para12 = new SqlParameter("@QUERY_KEY_SUBID", g1.ToString());
                        sqlcmd.Parameters.Add(para12);

                        SqlParameter para13 = new SqlParameter("@QUERY_BY", PrintUserName);
                        sqlcmd.Parameters.Add(para13);

                        SqlParameter para22 = new SqlParameter("@播出日期", NowTapeListDate.FDDATE);
                        sqlcmd.Parameters.Add(para22);

                        SqlParameter para23 = new SqlParameter("@頻道", NowTapeListDate.FSCHANNEL);
                        sqlcmd.Parameters.Add(para23);

                        SqlParameter para14 = new SqlParameter("@序號", NowTapeListDate.FNSEQ);
                        sqlcmd.Parameters.Add(para14);

                        SqlParameter para19 = new SqlParameter("@節目名稱", NowTapeListDate.FSPROG_NAME);
                        sqlcmd.Parameters.Add(para19);

                        SqlParameter para21 = new SqlParameter("@集數", NowTapeListDate.FNEPISODE);
                        sqlcmd.Parameters.Add(para21);

                        SqlParameter para17 = new SqlParameter("@VIDEO_ID", NowTapeListDate.FSVIDEO_ID);
                        sqlcmd.Parameters.Add(para17);

                        SqlParameter para20 = new SqlParameter("@長度", NowTapeListDate.FSDURATION);
                        sqlcmd.Parameters.Add(para20);

                        SqlParameter para25 = new SqlParameter("@帶別", "");
                        sqlcmd.Parameters.Add(para25);

                        if (sqlcmd.ExecuteNonQuery() == 0)
                        {
                            tran.Rollback();
                            return "";
                        }
                    }
                    tran.Commit();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                StringBuilder sbError = new StringBuilder();
                return ex.Message.ToString();
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }

            String strURL = "";  //要傳回去的URL

            //取列印報表的系統設定檔
            MAM_PTS_DLL.SysConfig SettingObj = new MAM_PTS_DLL.SysConfig();
            string reportSrv = SettingObj.sysConfig_Read("/ServerConfig/ReportServer");

            strURL = reportSrv + "RPT_DAY_TAPE_LIST_SIGN" + "&QUERY_KEY=" + g.ToString() + "&rc:parameters=false&rs:Command=Render";
            //strURL _URL = "http://172.20.141.92/ReportServer/Pages/ReportViewer.aspx?%2fPTS_MAM3%2fRPT_TBPROPOSAL_01&QUERY_KEY=A3AE2DED-3451-4AF0-898D-04D1417D01AA&rc:parameters=false&rs:Command=Render";
            return Server.UrlEncode(strURL);


        }


        //測試轉出HD頻道LST格式運行表
        [WebMethod()]
        public string PrintHDLstTest(List<ArrPromoList> InList, string USERNAME)
        {
            //將帶入的節目表增加SOM欄位
            PGM_ARR_PROMOAddSOM.Clear();
            foreach (ArrPromoList TemArrPromoList in InList)
            {
                PGM_ARR_PROMOAddSOM.Add(AddSOMToArrPromoList(TemArrPromoList));
            }

            string TempChannelID = "";
            string TempDate = "";

            //INSERT_RPT_DATE_LST
            Guid g;
            g = Guid.NewGuid();
            try
            {
                int WorkRow = 0;
                bool beFirst = false; //是否為第一段
                bool bLast = false;  //是否為最後一段
                string PriorDataType = "";
                //逐一分析每筆資料的FSMEMO並將其寫入報表中
                int RowNO = 1;


                foreach (ArrPromoListAddSOM TemArrPromoList in PGM_ARR_PROMOAddSOM)
                {
                    #region 開始逐一分析運行表資料做成TMP_RPT_DATE_LST加入報表
                    TempChannelID = TemArrPromoList.FSCHANNEL_ID;
                    TempDate = TemArrPromoList.FDDATE;
                    RPT_DATE_LST TMP_RPT_DATE_LST = new RPT_DATE_LST();
                    TMP_RPT_DATE_LST.QUERY_KEY = g.ToString();

                    TMP_RPT_DATE_LST.QUERY_KEY_SUBID = "";
                    TMP_RPT_DATE_LST.QUERY_BY = USERNAME;

                    TMP_RPT_DATE_LST.NO = RowNO.ToString();
                    RowNO = RowNO + 1;

                    TMP_RPT_DATE_LST.VIDEOID = TemArrPromoList.FSVIDEO_ID;
                    TMP_RPT_DATE_LST.PROGNAME = TemArrPromoList.FSNAME;
                    if (int.Parse(TemArrPromoList.FSPLAY_TIME.Substring(0, 2)) >= 24)
                        TMP_RPT_DATE_LST.PLAYTIME = (int.Parse(TemArrPromoList.FSPLAY_TIME.Substring(0, 2)) - 24).ToString() + TemArrPromoList.FSPLAY_TIME.Substring(2, 9);
                    else
                        TMP_RPT_DATE_LST.PLAYTIME = TemArrPromoList.FSPLAY_TIME;
                    TMP_RPT_DATE_LST.DUR = TemArrPromoList.FSDURATION;
                    TMP_RPT_DATE_LST.PROGID = TemArrPromoList.FSPROG_ID;
                    TMP_RPT_DATE_LST.SEQNO = TemArrPromoList.FNSEQNO;
                    TMP_RPT_DATE_LST.DATATYPE = TemArrPromoList.FCTYPE;
                    TMP_RPT_DATE_LST.PROMOID = TemArrPromoList.FSPROMO_ID;
                    TMP_RPT_DATE_LST.FSSIGNAL = TemArrPromoList.FSSIGNAL;
                    TMP_RPT_DATE_LST.PLAYDATE = TemArrPromoList.FDDATE;
                    TMP_RPT_DATE_LST.CHANNELNAME = TemArrPromoList.FSCHANNEL_NAME;
                    TMP_RPT_DATE_LST.SUBNO = TemArrPromoList.FNSUB_NO;
                    TMP_RPT_DATE_LST.PLAYDATE = TemArrPromoList.FDDATE;
                    TMP_RPT_DATE_LST.PROGSEG = TemArrPromoList.FNBREAK_NO;
                    TMP_RPT_DATE_LST.PROGEPISODE = TemArrPromoList.FNEPISODE;
                    TMP_RPT_DATE_LST.FSRAN = "";
                    TMP_RPT_DATE_LST.FNLIVE = TemArrPromoList.FNLIVE;
                    TMP_RPT_DATE_LST.FILENO = TemArrPromoList.FSFILE_NO;
                    //string SOM = TemArrPromoList.FSSOM;

                    if (TemArrPromoList.FCTIME_TYPE == "O")
                        TMP_RPT_DATE_LST.TIMETYPE = "AO";
                    //else if ((TemArrPromoList.FCTYPE=="G") || (TemArrPromoList.FNSUB_NO=="1"))
                    else if ((TemArrPromoList.FCTYPE == "G") || (TemArrPromoList.FCTYPE == "P" && TemArrPromoList.FNBREAK_NO != "0" && TemArrPromoList.FNSUB_NO == "1"))

                        TMP_RPT_DATE_LST.TIMETYPE = "AN";
                    //else if ((TemArrPromoList.FNBREAK_NO=="1" && TemArrPromoList.FCTYPE=="G") || (TemArrPromoList.FNSUB_NO=="1"))
                    else
                        TMP_RPT_DATE_LST.TIMETYPE = "A";

                    //如果前一筆資料為節目,此筆資料為短帶,則要設定為 AN
                    if (PriorDataType == "G" && TemArrPromoList.FCTYPE == "P")
                    {
                        TMP_RPT_DATE_LST.TIMETYPE = "AN";
                    }
                    //用完後將此筆資料的類型設定進PriorDataType,當下一筆時可以使用
                    PriorDataType = TemArrPromoList.FCTYPE;

                    if ((TemArrPromoList.FCTYPE == "G") && (TemArrPromoList.FNLIVE == "1")) //Live節目
                    {
                        if (TemArrPromoList.FCTIME_TYPE == "O")
                            TMP_RPT_DATE_LST.TIMETYPE = "AUNO";
                        else
                            TMP_RPT_DATE_LST.TIMETYPE = "AUN";
                    }

                    if ((TemArrPromoList.FCTYPE == "P") && (TemArrPromoList.FSSIGNAL != "PGM") && (TemArrPromoList.FSSIGNAL != "COMM") && (TemArrPromoList.FSSIGNAL != "")) //Live PROMO
                    {
                        if (TemArrPromoList.FCTIME_TYPE == "O")
                            TMP_RPT_DATE_LST.TIMETYPE = "AUNO";
                        else
                            TMP_RPT_DATE_LST.TIMETYPE = "AUN";
                    }
                    if (TemArrPromoList.FNLIVE == "1")  //如果為Live則要將訊號源寫入VIdeoID中
                    {
                        TMP_RPT_DATE_LST.VIDEOID = TemArrPromoList.FSSIGNAL;
                    }
                    TMP_RPT_DATE_LST.TIMETYPE = TemArrPromoList.FCTIME_TYPE;
                    TMP_RPT_DATE_LST.MEMO = "";
                    //TMP_RPT_DATE_LST.MEMO = TemArrPromoList.FSMEMO;
                    if (TMP_RPT_DATE_LST.DATATYPE != "G")
                        TMP_RPT_DATE_LST.PROGSEG = "";
                    if (TemArrPromoList.FSMEMO.ToUpper().IndexOf("LOGO") > -1) //Logo那節目要+Lose
                        TMP_RPT_DATE_LST.MEMO = TMP_RPT_DATE_LST.MEMO + "(LOSE)";
                    //if (TemArrPromoList.FSMEMO.IndexOf("LOGO") > -1) //Logo那節目要+Lose
                    //    TMP_RPT_DATE_LST.MEMO = TMP_RPT_DATE_LST.MEMO+ "+LOSE";
                    //if (TemArrPromoList.FSPROG_ID == "2004090" && TemArrPromoList.FCTYPE == "G") //訊號測試節目要+Lose
                    //    TMP_RPT_DATE_LST.MEMO = TMP_RPT_DATE_LST.MEMO + "+LOSE";
                    TMP_RPT_DATE_LST.FSSOM = TemArrPromoList.FSSOM;
                    INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);
                #endregion

                    #region 開始判斷是否為節目最後一段
                    if (TemArrPromoList.FCTYPE == "G")
                    {
                        beFirst = false; //是否為第一段
                        bLast = false;  //是否為最後一段
                        if (WorkRow < PGM_ARR_PROMOAddSOM.Count - 1) //檢查此資料狀態,是否為第一段 和 是否為最後一段 和 是否只有一段
                        {
                            if (TemArrPromoList.FNBREAK_NO == "1")
                                beFirst = true;
                            int NextProgRow = WorkRow + 1;
                            while (NextProgRow < PGM_ARR_PROMOAddSOM.Count - 1 && PGM_ARR_PROMOAddSOM[NextProgRow].FCTYPE == "P")
                                NextProgRow = NextProgRow + 1;

                            if (PGM_ARR_PROMOAddSOM[NextProgRow].FCTYPE == "G")
                            {
                                if (PGM_ARR_PROMOAddSOM[NextProgRow].FSPROG_ID != TemArrPromoList.FSPROG_ID || PGM_ARR_PROMOAddSOM[NextProgRow].FNSEQNO != TemArrPromoList.FNSEQNO)
                                    bLast = true; //表示最後一段
                            }
                            else
                            {
                                bLast = true; //表示最後一段
                            }
                        }
                    }
                    #endregion

                    WorkRow = WorkRow + 1;
                    //開始分析Memo的Louth Key
                    #region 開始分析Memo的Louth Key
                    if (TemArrPromoList.FSMEMO != "")
                    {
                        string[] LouthKeys = TemArrPromoList.FSMEMO.Split(new Char[] { '+' });

                        #region 開始逐一處理Key
                        foreach (string LouthKey in LouthKeys)  //每一個+ 號表示一個Key
                        {
                            if (LouthKey.Trim() != "")
                            {

                                TMP_RPT_DATE_LST.PROGSEG = "";

                                TMP_RPT_DATE_LST.NO = "";
                                TMP_RPT_DATE_LST.TIMETYPE = "";
                                TMP_RPT_DATE_LST.MEMO = "";
                                // 時間 ,重播,直播  為 sTRANKEY 其他為 sKEYER

                                //VideoID = "sKEYER";
                                string[] LouthKeyType = LouthKey.Split(new Char[] { '#' }); //將LouthKey 依照#分開,如果有 #代表有輸入時間 Ex:中/英#00:02:00,10,30
                                if (LouthKeyType[0] != "") //如果有設定LouthKey Ex:中/英#00:02:00,10,30
                                {
                                    #region 查詢Louth的資料 並將其加入 NewLouth
                                    LouthTableData NewLouth = new LouthTableData();

            //                         public string FSGROUP { get; set; }
            //public string FSNAME { get; set; }
            //public string FSNO { get; set; }
            //public string FSMEMO { get; set; }
            //public string FSLayel { get; set; }
            //public string FSRULE { get; set; }

                                    if (LouthKeyType[0].IndexOf("片名") == 0)
                                    {
                                        //NewLouth = GetLouthData("片名-" + TemArrPromoList.FSNAME, TempChannelID);

                                        NewLouth = GetLouthData(LouthKeyType[0], TempChannelID);
                                        if (LouthKeyType[0].IndexOf("(整段)") > -1)
                                            NewLouth = GetLouthData(LouthKeyType[0].Substring(3, (LouthKeyType[0].Length) - 7), TempChannelID);
                                        else if (LouthKeyType[0].IndexOf("(全程避片頭)") > -1)
                                            NewLouth = GetLouthData(LouthKeyType[0].Substring(3, (LouthKeyType[0].Length) - 10), TempChannelID);
                                        else
                                            NewLouth = GetLouthData(LouthKeyType[0].Substring(3, (LouthKeyType[0].Length) - 3), TempChannelID);
                                    }
                                    else
                                        NewLouth = GetLouthData(LouthKeyType[0], TempChannelID); //取得Louth Table的資料

                                    #endregion
                                    #region 只有在有查到資料的情況下才做事
                                    if (NewLouth.FSNO != "")
                                    {
                                        //QF4 = NewLouth.FSLayel;
                                        TMP_RPT_DATE_LST.VIDEOID = NewLouth.FSNO.Substring(1, NewLouth.FSNO.Length - 1);
                                        TMP_RPT_DATE_LST.DATATYPE = "K";
                                        TMP_RPT_DATE_LST.FSSIGNAL = NewLouth.FSLayel;
                                        // QF1 = NewLouth.FSNO.Substring(1, NewLouth.FSNO.Length - 1);

                                        if (LouthKeyType[0].IndexOf("時間") == 0 || LouthKeyType[0].IndexOf("重播") == 0 || LouthKeyType[0].IndexOf("直播") == 0)
                                        {

                                            TMP_RPT_DATE_LST.MEMO = "sTRANKEY";
                                            TMP_RPT_DATE_LST.DUR = "";
                                            TMP_RPT_DATE_LST.PLAYTIME = "";


                                        }
                                        else
                                        {
                                            if (LouthKeyType[0].ToUpper().IndexOf("LOGO") == 0)
                                                TMP_RPT_DATE_LST.MEMO = "sKEYER(EH)";
                                            else
                                                TMP_RPT_DATE_LST.MEMO = "sKEYER";
                                            //SOM = "";
                                            TMP_RPT_DATE_LST.PLAYTIME = "00:00:00:00";
                                            TMP_RPT_DATE_LST.DUR = TemArrPromoList.FSDURATION;
                                        }
                                        TMP_RPT_DATE_LST.PROGNAME = NewLouth.FSNAME;
  
                                        bool NeedLoop = false;    //是否要做循環
                                        TimeStatus RTimeStatus = new TimeStatus();
                                        SEndPointStatus REndPointStatus = new SEndPointStatus();
                                        //設定播出規則,沒有的話則採用預設規則  並將資料加入RTimeStatus 只有雙語 片名 立體聲 數位同步播出有 值
                                        //接著跟繼續收看
                                        #region 設定播出規則,沒有的話則採用預設規則  並將資料加入RTimeStatus
                                        if ((LouthKeyType[0].IndexOf("/") > -1) || (LouthKeyType[0].IndexOf("片名") == 0) || (LouthKeyType[0].IndexOf("立體聲") == 0) || (LouthKeyType[0].IndexOf("數位") == 0) || (LouthKeyType[0].IndexOf("口述影像版") > -1)) //代表為片名 //有斜線代表為雙語
                                        {
                                            if (LouthKey.IndexOf("#") > -1)  //#00:02:00,10,30 //表示有設定起始時間,間隔,長度
                                            {
                                                RTimeStatus = Trans(LouthKeyType[1]);
                                                if (RTimeStatus.SStartTime.Length == 8)
                                                    RTimeStatus.SStartTime = RTimeStatus.SStartTime + ":00";
                                                if (RTimeStatus.SStartTime == "") //如為空值則採用預設值
                                                    RTimeStatus.SStartTime = "00:01:00:00";
                                                if (RTimeStatus.SInteval == "") //如為空值則採用預設值
                                                    RTimeStatus.SInteval = "10";
                                                if (RTimeStatus.SDur == "") //如為空值則採用預設值
                                                    RTimeStatus.SDur = "30";
                                                NeedLoop = true;
                                            }
                                            else
                                            {
                                                if (LouthKeyType[0].IndexOf("口述影像版") > -1)  //口述影像版,全程,但節目最末段兩分鐘不上
                                                {
                                                    RTimeStatus.SStartTime = "00:00:00:00";  //5秒開始上1分鐘,間隔30分鐘
                                                    RTimeStatus.SInteval = "30";
                                                    RTimeStatus.SDur = "60";
                                                }
                                                else if (LouthKeyType[0].IndexOf("/") > -1) //雙語
                                                {
                                                    RTimeStatus.SStartTime = "00:00:20:00";  //1分開始上30秒,間隔10分鐘
                                                    RTimeStatus.SInteval = "10";
                                                    RTimeStatus.SDur = "90";
                                                }
                                                else if (LouthKeyType[0].IndexOf("(全程避片頭)") > -1) //全程避片頭
                                                {
                                                    RTimeStatus.SStartTime = "00:00:20:00";  //1分開始上30秒,間隔10分鐘
                                                    RTimeStatus.SInteval = "10";
                                                    RTimeStatus.SDur = "480";
                                                }
                                                else if (LouthKeyType[0].IndexOf("片名") == 0)  //片名
                                                {
                                                    RTimeStatus.SStartTime = "00:02:00:00";  //2分開始上8分鐘秒,間隔10分鐘
                                                    RTimeStatus.SInteval = "10";
                                                    RTimeStatus.SDur = "480";
                                                }
                                                else if (LouthKeyType[0].IndexOf("立體聲") == 0)  //立體聲
                                                {
                                                    RTimeStatus.SStartTime = "00:02:00:00";  //2分開始上30秒,間隔10分鐘
                                                    RTimeStatus.SInteval = "10";
                                                    RTimeStatus.SDur = "60";
                                                }
                                                else if (LouthKeyType[0].IndexOf("數位") == 0)  //數位
                                                {
                                                    RTimeStatus.SStartTime = "00:00:05:00";  //5秒開始上1分鐘,間隔30分鐘
                                                    RTimeStatus.SInteval = "30";
                                                    RTimeStatus.SDur = "60";
                                                }
                                                else
                                                {
                                                    RTimeStatus.SStartTime = "00:01:00:00";
                                                    RTimeStatus.SInteval = "10";
                                                    RTimeStatus.SDur = "30";
                                                }
                                                RTimeStatus.SSec = "0";
                                                NeedLoop = true;

                                            }
                                        }
                                        #endregion

                                        #region 接著,下週,明天,下次 將資料寫入REndPointInsert 表示最後幾秒要或不要
                                        if ((LouthKeyType[0].IndexOf("接著") > -1) || (LouthKeyType[0].IndexOf("下週") > -1) || (LouthKeyType[0].IndexOf("明天") > -1) || (LouthKeyType[0].IndexOf("下次") > -1))
                                        {

                                            //只要有最後幾秒要播出和播出長度
                                            string LastTimeInsert = "00:00:00:00"; //最後多少時間要插入Key
                                            if (LouthKey.IndexOf("#") > -1)  //#00:02:00,10,30 //表示有設定最後幾秒開始與長度
                                            {
                                                REndPointStatus = Trans1(LouthKeyType[1]);
                                                if (REndPointStatus.SEndPoint == "") //如為空值則採用預設值
                                                {
                                                    if (LouthKeyType[0].IndexOf("接著") == 0)
                                                    {
                                                        REndPointStatus.SEndPoint = "90";
                                                        REndPointStatus.SDur = "20";
                                                    }
                                                    else if ((LouthKeyType[0].IndexOf("下週") == 0) || (LouthKeyType[0].IndexOf("明天") == 0) || (LouthKeyType[0].IndexOf("下次") == 0))
                                                    {
                                                        REndPointStatus.SEndPoint = "40";
                                                        REndPointStatus.SDur = "20";
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (LouthKeyType[0].IndexOf("接著") == 0)
                                                {
                                                    REndPointStatus.SEndPoint = "90";
                                                    REndPointStatus.SDur = "20";
                                                }
                                                else if ((LouthKeyType[0].IndexOf("下週") == 0) || (LouthKeyType[0].IndexOf("明天") == 0) || (LouthKeyType[0].IndexOf("下次") == 0))
                                                {
                                                    REndPointStatus.SEndPoint = "40";
                                                    REndPointStatus.SDur = "20";
                                                }
                                            }

                                            //int IStartTimeFrame = 0;
                                            //IStartTimeFrame = MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - int.Parse(REndPointStatus.SEndPoint) * 30;
                                            //if (IStartTimeFrame > 0)
                                            //    PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(IStartTimeFrame);
                                            //else
                                            //    PlayTime = "00:00:00;00";

                                            //string Dur1;
                                            //長度為起始時間加五秒及最後幾分鐘不加,所以為長度減掉最後幾分鐘再減五秒

                                            //Dur1 = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(REndPointStatus.SDur));
                                            //myWriter.Write(WriteLouthByteArray(TemArrPromoList, striType, VideoID, Title, PlayTime, Dur1, SOM, EventCOntrol, QF1, QF2, QF3, QF4, EF1, EF2, EF3));

                                        }
                                        #endregion

                                        //開始將資料寫入LouthList

                                        #region 片名, 雙語 ,立體聲,數位同步播出 除了片名整段外都是有循環
                                        if ((LouthKeyType[0].IndexOf("/") > -1) || (LouthKeyType[0].IndexOf("片名") == 0) || (LouthKeyType[0].IndexOf("立體聲") == 0) || (LouthKeyType[0].IndexOf("數位") == 0) || (LouthKeyType[0].IndexOf("口述影像版") > -1)) //代表為片名 //有斜線代表為雙語
                                        {
                                            string LastTimeNoInsert = "00:02:00:00"; //最後多少時間不要插入Key
                                            //抓取資料完成開始加入LouthKey
                                            if ((LouthKeyType[0].IndexOf("整段") > -1) || (LouthKeyType[0].IndexOf("(全程避片頭)") > -1) || (LouthKeyType[0].IndexOf("口述影像版") > -1)) //整段
                                            {
                                                if (LouthKeyType[0].IndexOf("整段") > -1)
                                                {
                                                    TMP_RPT_DATE_LST.MEMO = "sTRANKEY";

                                                    TMP_RPT_DATE_LST.PLAYTIME = "";
                                                    TMP_RPT_DATE_LST.DUR = "";
                                                    LastTimeNoInsert = "00:00:00:00";
                                                    NeedLoop = false;
                                                }
                                                else if (LouthKeyType[0].IndexOf("(全程避片頭)") > -1)
                                                {
                                                    TMP_RPT_DATE_LST.MEMO = "sKEYER";

                                                    TMP_RPT_DATE_LST.PLAYTIME = "00:00:20:00";

                                                    if (bLast == false) //不是最後一段
                                                    {
                                                        if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) > 20 * 30)
                                                            TMP_RPT_DATE_LST.DUR = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - 20 * 30);
                                                        else
                                                            TMP_RPT_DATE_LST.DUR = "00:00:00:00";
                                                    }
                                                    else
                                                    {
                                                        if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) > 140 * 30)
                                                            TMP_RPT_DATE_LST.DUR = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - 140 * 30);
                                                        else
                                                            TMP_RPT_DATE_LST.DUR = "00:00:00:00";
                                                    }

                                                    LastTimeNoInsert = "00:00:00:00";
                                                    NeedLoop = false;
                                                }
                                                else if (LouthKeyType[0].IndexOf("口述影像版") > -1)
                                                {
                                                    TMP_RPT_DATE_LST.MEMO = "sKEYER";

                                                    TMP_RPT_DATE_LST.PLAYTIME = "00:00:00:00";
                                                    if (bLast == false) //不是最後一段
                                                    {
                                                        TMP_RPT_DATE_LST.MEMO = "sTRANKEY";
                                                        TMP_RPT_DATE_LST.PLAYTIME = "";
                                                        TMP_RPT_DATE_LST.DUR = "";
                                                        //TMP_RPT_DATE_LST.DUR = TemArrPromoList.FSDURATION;
                                                    }
                                                    else
                                                    {
                                                        if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) > 120 * 30)
                                                            TMP_RPT_DATE_LST.DUR = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - 120 * 30);
                                                        else
                                                            TMP_RPT_DATE_LST.DUR = "00:00:00:00";
                                                    }

                                                    LastTimeNoInsert = "00:00:00:00";
                                                    NeedLoop = false;
                                                }
                                                else
                                                {

                                                    TMP_RPT_DATE_LST.MEMO = "sKEYER";

                                                    TMP_RPT_DATE_LST.PLAYTIME = "";
                                                    TMP_RPT_DATE_LST.DUR = "";
                                                    LastTimeNoInsert = "00:00:00:00";
                                                    NeedLoop = false;
                                                }


                                            }
                                            else //如果不為整段
                                            {
                                                NeedLoop = true;

                                                TMP_RPT_DATE_LST.MEMO = "sKEYER";
                                                if (TemArrPromoList.FSCHANNEL_ID == "02") //Dimo頻道
                                                {
                                                    if (LouthKey.IndexOf("#") > -1)  //#00:02:00,10,30 //表示有設定起始時間,間隔,長度
                                                    { }
                                                    else
                                                    {
                                                        if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) >= MAM_PTS_DLL.TimeCodeCalc.timecodetoframe("00:05:00:00")) //如果長度大於5分鐘
                                                        {
                                                            TMP_RPT_DATE_LST.PLAYTIME = "00:00:59:28";
                                                            TMP_RPT_DATE_LST.DUR = "00:02:00:00";
                                                            LastTimeNoInsert = "00:00:00:00";
                                                            NeedLoop = false;
                                                        }
                                                        else //如果長度小於5分鐘
                                                        {
                                                            TMP_RPT_DATE_LST.PLAYTIME = "00:00:10:00";
                                                            TMP_RPT_DATE_LST.DUR = "00:00:20:00";
                                                            LastTimeNoInsert = "00:00:00:00";
                                                            NeedLoop = false;
                                                        }

                                                    }
                                                }
                                                else //如果不為Dimo
                                                {
                                                    if (LouthKey.IndexOf("#") > -1)  //#00:02:00,10,30 //表示有設定起始時間,間隔,長度
                                                    { }
                                                    else
                                                    {
                                                        if ((TemArrPromoList.FSCHANNEL_ID == "01") || (TemArrPromoList.FSCHANNEL_ID == "51"))
                                                            LastTimeNoInsert = "00:02:00:00";
                                                        else
                                                            LastTimeNoInsert = "00:00:30:00";
                                                    }
                                                }
                                            }



                                            #region 加入Louth Ley
                                            if (NeedLoop == true) //加入多筆louth key Loop
                                            {
                                                string ThisPlayTime = RTimeStatus.SStartTime;
                                                string PlayTime;
                                                if (bLast == false) //不是最後一段只要檢查Key起始時間+長度不要大於節目時長即可
                                                {
                                                    string QF4 = TMP_RPT_DATE_LST.FSSIGNAL;
                                                    string QF1 = TMP_RPT_DATE_LST.VIDEOID;
                                                    while (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(ThisPlayTime) + int.Parse(RTimeStatus.SDur) * 30 < MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION))
                                                    {
                                                        //HD格式的VideoID為 L1 100

                                                        //HD格式的VideoID為 L1 100
                                                        PlayTime = ThisPlayTime;
                                                        ThisPlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(ThisPlayTime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe("00:" + String.Format("{0:00}", int.Parse(RTimeStatus.SInteval)) + ":00:00"));
                                                        string Dur1;
                                                        Dur1 = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(RTimeStatus.SDur));

                                                        //HD格式的VideoID為 L1 100
                                                        if (LouthKeyType[0].ToUpper().IndexOf("LOGO") == 0)
                                                        {
                                                            PlayTime = "";
                                                            Dur1 = "";
                                                        }

                                                        string VideoID;
                                                        if (QF4 == "4")
                                                        {
                                                            //須給上下架時間,長度都為兩秒,放在第四層的應當都為整段的Key,所以起始時間為0.
                                                            if (QF1.Substring(0, 1) == "0")
                                                                VideoID = "ST " + QF1.Substring(1, 2);
                                                            else
                                                                VideoID = "ST " + QF1;
                                                            if (PlayTime == "")
                                                                PlayTime = "00:00:00:00";
                                                            if (Dur1 == "")
                                                                Dur1 = TemArrPromoList.FSDURATION;
                                                            if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) < 15 * 30)
                                                                PlayTime = "00:00:15:00";
                                                            string RealDur = Dur1;
                                                            Dur1 = "00:00:02:00";

                                                            TMP_RPT_DATE_LST.PLAYTIME = PlayTime;
                                                            TMP_RPT_DATE_LST.DUR = "00:00:02:00";
                                                            TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                            TMP_RPT_DATE_LST.FSSOM = PlayTime;
                                                            TMP_RPT_DATE_LST.VIDEOID = VideoID;
                                                            INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);
                                                            RowNO = RowNO + 1;
                                                            VideoID = "ST 5" + QF1;
                                                            TMP_RPT_DATE_LST.VIDEOID = VideoID;
                                                            PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(RealDur));
                                                            Dur1 = "00:00:02:00";
                                                            TMP_RPT_DATE_LST.PLAYTIME = PlayTime;
                                                            TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                            TMP_RPT_DATE_LST.FSSOM = PlayTime;
                                                            INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);
                                                            RowNO = RowNO + 1;
                                                        }
                                                        else
                                                        {

                                                            TMP_RPT_DATE_LST.PLAYTIME = ThisPlayTime;
                                                            ThisPlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(ThisPlayTime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe("00:" + String.Format("{0:00}", int.Parse(RTimeStatus.SInteval)) + ":00:00") + int.Parse(RTimeStatus.SSec) * 30);
                                                            // string Dur1;
                                                            TMP_RPT_DATE_LST.DUR = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(RTimeStatus.SDur));
                                                            TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                            TMP_RPT_DATE_LST.FSSOM = ThisPlayTime;
                                                            TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                            TMP_RPT_DATE_LST.VIDEOID = "L" + TMP_RPT_DATE_LST.FSSIGNAL + " " + TMP_RPT_DATE_LST.VIDEOID;
                                                            RowNO = RowNO + 1;
                                                            INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);

                                                        }



                                                    }

                                                }
                                                else //最後一段要檢查Key起始時間+3分鐘不要大於節目時長即可 //if (bLast == true) 如果是最後一段 
                                                {
                                                    string QF4 = TMP_RPT_DATE_LST.FSSIGNAL;
                                                    string QF1 = TMP_RPT_DATE_LST.VIDEOID;
                                                    while (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(ThisPlayTime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(LastTimeNoInsert) < MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION))
                                                    {


                                                        //HD格式的VideoID為 L1 100
                                                        PlayTime = ThisPlayTime;
                                                        ThisPlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(ThisPlayTime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe("00:" + String.Format("{0:00}", int.Parse(RTimeStatus.SInteval)) + ":00:00"));
                                                        string Dur1;
                                                        Dur1 = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(RTimeStatus.SDur));

                                                        //HD格式的VideoID為 L1 100
                                                        if (LouthKeyType[0].ToUpper().IndexOf("LOGO") == 0)
                                                        {
                                                            PlayTime = "";
                                                            Dur1 = "";
                                                        }

                                                        string VideoID;
                                                        if (QF4 == "4")
                                                        {

                                                            //須給上下架時間,長度都為兩秒,放在第四層的應當都為整段的Key,所以起始時間為0.
                                                            if (QF1.Substring(0, 1) == "0")
                                                                VideoID = "ST " + QF1.Substring(1, 2);
                                                            else
                                                                VideoID = "ST " + QF1;
                                                            if (PlayTime == "")
                                                                PlayTime = "00:00:00:00";
                                                            if (Dur1 == "")
                                                                Dur1 = TemArrPromoList.FSDURATION;
                                                            if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) < 15 * 30)
                                                                PlayTime = "00:00:15:00";
                                                            string RealDur = Dur1;
                                                            Dur1 = "00:00:02:00";

                                                            TMP_RPT_DATE_LST.PLAYTIME = PlayTime;
                                                            TMP_RPT_DATE_LST.DUR = "00:00:02:00";
                                                            TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                            TMP_RPT_DATE_LST.FSSOM = PlayTime;
                                                            TMP_RPT_DATE_LST.VIDEOID = VideoID;

                                                            INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);
                                                            RowNO = RowNO + 1;

                                                            VideoID = "ST 5" + QF1;
                                                            TMP_RPT_DATE_LST.VIDEOID = VideoID;
                                                            PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(RealDur));
                                                            Dur1 = "00:00:02:00";
                                                            TMP_RPT_DATE_LST.PLAYTIME = PlayTime;
                                                            TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                            TMP_RPT_DATE_LST.FSSOM = PlayTime;
                                                            INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);
                                                            RowNO = RowNO + 1;
                                                        }
                                                        else
                                                        {

                                                            TMP_RPT_DATE_LST.PLAYTIME = ThisPlayTime;
                                                            ThisPlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(ThisPlayTime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe("00:" + String.Format("{0:00}", int.Parse(RTimeStatus.SInteval)) + ":00:00") + int.Parse(RTimeStatus.SSec) * 30);
                                                            // string Dur1;
                                                            TMP_RPT_DATE_LST.DUR = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(RTimeStatus.SDur));
                                                            TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                            TMP_RPT_DATE_LST.FSSOM = ThisPlayTime;
                                                            TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                            TMP_RPT_DATE_LST.VIDEOID = "L" + TMP_RPT_DATE_LST.FSSIGNAL + " " + TMP_RPT_DATE_LST.VIDEOID;
                                                            RowNO = RowNO + 1;
                                                            INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);

                                                        }

                                                    }
                                                }
                                            }
                                            else
                                            {
                                                string ThisPlayTime = RTimeStatus.SStartTime;
                                                if (TMP_RPT_DATE_LST.FSSIGNAL == "4")
                                                {
                                                    string QF1 = TMP_RPT_DATE_LST.VIDEOID;
                                                    //須給上下架時間,長度都為兩秒,放在第四層的應當都為整段的Key,所以起始時間為0.
                                                    if (QF1.Substring(0, 1) == "0")
                                                        TMP_RPT_DATE_LST.VIDEOID = "ST " + QF1.Substring(1, 2);
                                                    else
                                                        TMP_RPT_DATE_LST.VIDEOID = "ST " + QF1;
                                                    string PlayTime = ThisPlayTime;
                                                    string RealDur = TMP_RPT_DATE_LST.DUR;
                                                    string Dur1 = "00:00:02:00";

                                                    if (RealDur == "")
                                                        RealDur = TemArrPromoList.FSDURATION;
                                                    TMP_RPT_DATE_LST.PLAYTIME = PlayTime;
                                                    TMP_RPT_DATE_LST.DUR = Dur1;
                                                    TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                    TMP_RPT_DATE_LST.FSSOM = PlayTime;

                                                    RowNO = RowNO + 1;
                                                    INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);
                                                    TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                    PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(RealDur));
                                                    TMP_RPT_DATE_LST.VIDEOID = "ST 5" + QF1;
                                                    TMP_RPT_DATE_LST.FSSOM = PlayTime;
                                                    RowNO = RowNO + 1;
                                                    INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);
                                                }
                                                else
                                                {

                                                    TMP_RPT_DATE_LST.PLAYTIME = RTimeStatus.SStartTime;
                                                    string Dur1;
                                                    Dur1 = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(RTimeStatus.SDur));

                                                    if (TMP_RPT_DATE_LST.MEMO == "sTRANKEY")
                                                    {
                                                        TMP_RPT_DATE_LST.PLAYTIME = "";
                                                        TMP_RPT_DATE_LST.DUR = "";
                                                    }
                                                    TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                    TMP_RPT_DATE_LST.FSSOM = RTimeStatus.SStartTime;
                                                    TMP_RPT_DATE_LST.DUR = Dur1;
                                                    TMP_RPT_DATE_LST.VIDEOID = "L" + TMP_RPT_DATE_LST.FSSIGNAL + " " + TMP_RPT_DATE_LST.VIDEOID;
                                                    RowNO = RowNO + 1;
                                                    INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);
                                                }
                                            }
                                            #endregion

                                        }
                                        #endregion

                                        #region 重播, 直播 ,時間,Live,錄影轉播,Logo,都是整段
                                        if ((LouthKeyType[0].IndexOf("重播") == 0) || (LouthKeyType[0].IndexOf("直播") == 0) || (LouthKeyType[0].IndexOf("時間") == 0) || (LouthKeyType[0].ToUpper().IndexOf("LIVE") == 0) || (LouthKeyType[0].ToUpper().IndexOf("LOGO") == 0) || (LouthKeyType[0].IndexOf("錄影轉播") == 0)) //代表為片名 //有斜線代表為雙語
                                        {
                                            string QF4 = TMP_RPT_DATE_LST.FSSIGNAL;
                                            string QF1 = TMP_RPT_DATE_LST.VIDEOID;
                                            string Memo = "sTRANKEY";
                                            string PlayTime = "";
                                            string Dur = "";
                                            string VideoID = "";


                                            if (LouthKeyType[0].ToUpper().IndexOf("LOGO") == 0)
                                            {

                                                PlayTime = "00:00:00:00";
                                                Dur = "00:00:10:00";
                                                Memo = "sKEYER";
                                            }
                                            //PlayTime = RTimeStatus.SStartTime;
                                            string Dur1;
                                            //Dur1 = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(RTimeStatus.SDur));
                                            Dur1 = Dur;

                                            //HD格式的VideoID為 L1 100
                                            if (LouthKeyType[0].ToUpper().IndexOf("LOGO") == 0)
                                            {
                                                PlayTime = "";
                                                Dur1 = "";
                                            }
                                            if (QF4 == "4")
                                            {

                                                if (QF1.Substring(0, 1) == "0")
                                                    VideoID = "ST " + QF1.Substring(1, 2);
                                                else
                                                    VideoID = "ST " + QF1;
                                                if (PlayTime == "")
                                                    PlayTime = "00:00:00:00";
                                                if (Dur1 == "")
                                                    Dur1 = TemArrPromoList.FSDURATION;
                                                if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) < 15 * 30)
                                                    PlayTime = "00:00:15:00";
                                                string RealDur = Dur1;
                                                Dur1 = "00:00:02:00";
                                                TMP_RPT_DATE_LST.MEMO = Memo;
                                                TMP_RPT_DATE_LST.PLAYTIME = PlayTime;
                                                TMP_RPT_DATE_LST.DUR = Dur1;
                                                TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                TMP_RPT_DATE_LST.FSSOM = PlayTime;
                                                TMP_RPT_DATE_LST.VIDEOID = VideoID;
                                                RowNO = RowNO + 1;
                                                INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);
                                                TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(RealDur));
                                                TMP_RPT_DATE_LST.VIDEOID = "ST 5" + QF1;
                                                TMP_RPT_DATE_LST.FSSOM = PlayTime;
                                                RowNO = RowNO + 1;
                                                INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);



                                            }
                                            else
                                            {
                                                TMP_RPT_DATE_LST.MEMO = "sTRANKEY";
                                                TMP_RPT_DATE_LST.PLAYTIME = "";
                                                TMP_RPT_DATE_LST.DUR = "";
                                                TMP_RPT_DATE_LST.FSSOM = "00:00:00:00";
                                                if (LouthKeyType[0].ToUpper().IndexOf("LOGO") == 0)
                                                {

                                                    TMP_RPT_DATE_LST.PLAYTIME = "00:00:00:00";
                                                    TMP_RPT_DATE_LST.DUR = "00:00:10:00";
                                                    TMP_RPT_DATE_LST.MEMO = "sKEYER(EH)";
                                                }
                                                //PlayTime = RTimeStatus.SStartTime;

                                                //Dur1 = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(RTimeStatus.SDur));

                                                //TMP_RPT_DATE_LST.DUR = TemArrPromoList.FSDURATION;
                                                TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                TMP_RPT_DATE_LST.VIDEOID = "L" + TMP_RPT_DATE_LST.FSSIGNAL + " " + TMP_RPT_DATE_LST.VIDEOID;
                                                RowNO = RowNO + 1;
                                                INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);
                                            }





                                        }
                                        #endregion

                                        #region 接著,下週,明天,下次
                                        if ((LouthKeyType[0].IndexOf("接著") == 0) || (LouthKeyType[0].IndexOf("下週") == 0) || (LouthKeyType[0].IndexOf("明天") == 0) || (LouthKeyType[0].IndexOf("下次") == 0))
                                        {
                                            string QF4 = TMP_RPT_DATE_LST.FSSIGNAL;
                                            string QF1 = TMP_RPT_DATE_LST.VIDEOID;
                                            string Memo = "";
                                            string PlayTime = "";
                                            string Dur = "";
                                            string VideoID = "";


                                            //只要有最後幾秒要播出和播出長度
                                            string LastTimeInsert = "00:00:00:00"; //最後多少時間要插入Key

                                            Memo = "sKEYER";

                                            int IStartTimeFrame = 0;
                                            IStartTimeFrame = MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - int.Parse(REndPointStatus.SEndPoint) * 30;
                                            if (IStartTimeFrame > 0)
                                                PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(IStartTimeFrame);
                                            else
                                                PlayTime = "00:00:00;00";

                                            string Dur1;
                                            //長度為起始時間加五秒及最後幾分鐘不加,所以為長度減掉最後幾分鐘再減五秒

                                            Dur1 = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(REndPointStatus.SDur));

                                            //HD格式的VideoID為 L1 100
                                            if (LouthKeyType[0].ToUpper().IndexOf("LOGO") == 0)
                                            {
                                                PlayTime = "";
                                                Dur1 = "";
                                            }
                                            if (QF4 == "4")
                                            {

                                                if (QF1.Substring(0, 1) == "0")
                                                    VideoID = "ST " + QF1.Substring(1, 2);
                                                else
                                                    VideoID = "ST " + QF1;
                                                if (PlayTime == "")
                                                    PlayTime = "00:00:00:00";
                                                if (Dur1 == "")
                                                    Dur1 = TemArrPromoList.FSDURATION;
                                                if (MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) < 15 * 30)
                                                    PlayTime = "00:00:15:00";
                                                string RealDur = Dur1;

                                                TMP_RPT_DATE_LST.MEMO = Memo;
                                                TMP_RPT_DATE_LST.PLAYTIME = PlayTime;
                                                TMP_RPT_DATE_LST.DUR = Dur1;
                                                TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                TMP_RPT_DATE_LST.FSSOM = PlayTime;
                                                TMP_RPT_DATE_LST.VIDEOID = VideoID;
                                                RowNO = RowNO + 1;
                                                INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);
                                                TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                PlayTime = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(PlayTime) + MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(RealDur));

                                                TMP_RPT_DATE_LST.VIDEOID = "ST 5" + QF1;
                                                TMP_RPT_DATE_LST.FSSOM = PlayTime;
                                                RowNO = RowNO + 1;
                                                INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);

                                            }
                                            else
                                            {
                                                //striType = "131";
                                                //只要有最後幾秒要播出和播出長度
                                                LastTimeInsert = "00:00:00:00"; //最後多少時間要插入Key
                                                //EventCOntrol = "";
                                                TMP_RPT_DATE_LST.PROGNAME = NewLouth.FSNAME;
                                                TMP_RPT_DATE_LST.MEMO = "sKEYER";

                                                IStartTimeFrame = 0;
                                                IStartTimeFrame = MAM_PTS_DLL.TimeCodeCalc.timecodetoframe(TemArrPromoList.FSDURATION) - int.Parse(REndPointStatus.SEndPoint) * 30;
                                                if (IStartTimeFrame > 0)
                                                    TMP_RPT_DATE_LST.PLAYTIME = MAM_PTS_DLL.TimeCodeCalc.frame2timecode(IStartTimeFrame);
                                                else
                                                    TMP_RPT_DATE_LST.PLAYTIME = "00:00:00;00";


                                                //長度為起始時間加五秒及最後幾分鐘不加,所以為長度減掉最後幾分鐘再減五秒

                                                Dur1 = MAM_PTS_DLL.TimeCodeCalc.Secondstotimecode(int.Parse(REndPointStatus.SDur));
                                                TMP_RPT_DATE_LST.DUR = Dur1;
                                                TMP_RPT_DATE_LST.NO = RowNO.ToString();
                                                TMP_RPT_DATE_LST.FSSOM = TMP_RPT_DATE_LST.PLAYTIME;
                                                TMP_RPT_DATE_LST.VIDEOID = "L" + TMP_RPT_DATE_LST.FSSIGNAL + " " + TMP_RPT_DATE_LST.VIDEOID;
                                                RowNO = RowNO + 1;
                                                INSERT_RPT_HD_DATE_LST(TMP_RPT_DATE_LST);

                                            }
                                            //}



                                            //}

                                        }  //if ((LouthKeyType[0].IndexOf(" 接著,下週,明天,下次") > -1)
                                        #endregion
                                    }
                                    #endregion
                                }
                            }
                        } //  foreach (string LouthKey in LouthKeys)     
                        #endregion
                    } //if (TemArrPromoList.FSMEMO != "")
                    #endregion
                    //} //if FCPLAY_Type="G"
                }



            }
            catch (InvalidCastException e)
            {
                return "";
            }

            return g.ToString();
        }

    }
}
