﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// WSLogTemplateField_CheckIsNullable 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSLogTemplateField_CheckIsNullable : System.Web.Services.WebService
    {

        [WebMethod]
        public List<CheckFileList> TemplateField_CheckIsNullAble(List<CheckFileList> myFileList)
        {
            List<CheckFileList> returnFileInfo = new List<CheckFileList>();
            returnFileInfo.Clear();
            try
            {
                foreach(CheckFileList CFL in myFileList)
                {
                    bool HaveNullField = false;
                    string rtn = "";
                    string rtn1 = "";
                    List<string> NotNullFieldList = new List<string>();
                    Dictionary<string, string> source = new Dictionary<string, string>();
                    source.Add("FSTABLE_TYPE",CFL.FSTableType);
                    source.Add("FSFILE_NO", CFL.FSFileNo);
                    rtn = MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_TBTEMPLATE_CehckIsNullAble", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source));
                    byte[] byteArray = Encoding.Unicode.GetBytes(rtn);
                    XDocument doc = XDocument.Load(new MemoryStream(byteArray));
                    var ltox = from s in doc.Elements("Datas").Elements("Data")
                               select s;
                    foreach (XElement elem in ltox)
                    {
                        NotNullFieldList.Add(elem.Element("FSFIELD").Value);
                    }

                    rtn1 = MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_TBTEMPLATE_CehckIsNullAble_VAPDTable", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(source));
                    byte[] byteArray1 = Encoding.Unicode.GetBytes(rtn1);
                    XDocument doc1 = XDocument.Load(new MemoryStream(byteArray1));
                    var ltox1 = from s1 in doc1.Elements("Datas").Elements("Data")
                               select s1;
                    foreach (XElement elem1 in ltox1)
                    {
                        foreach (string FieldName in NotNullFieldList)
                        {
                            string a = elem1.Element(FieldName).Value;
                            if (a == "")//判定是否有任何欄位是NULL的
                            { HaveNullField = true; }
                        }
                    }
                    if (HaveNullField)
                    { returnFileInfo.Add(CFL); }
                    //SP_Q_TBLOG_TBTEMPLATE_CehckIsNullAble_VAPDTable
                }
            }
            catch (Exception ex)
            { }
            return returnFileInfo;
        }
        public class CheckFileList
        {
            public string FSTableType;//V-A-P-D
            public string FSFileNo;
            public string FSFileName;
        }
    }
}
