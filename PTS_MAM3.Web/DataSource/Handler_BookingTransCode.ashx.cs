﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Xml;
using MAM_PTS_DLL;
using ADSSECURITYLib;
using Xealcom.Security;
namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    ///Handler_BookingTransCode 的摘要描述
    /// </summary>
    public class Handler_BookingTransCode : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            long JobID;
            string ansJobStatus = "";
            WSBROADCAST obj = new WSBROADCAST();

            MAM_PTS_DLL.Log.AppendTrackingLog("Handler_BookingTransCode/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "RECEIVE FINAL DATA");

            HttpRequest req = context.Request;

            // 解析從 ANS 傳來的內容
            // 如果 return false ，代表 HTTP POST 的內容有誤，無法判斷是哪個 ANSID，也因此不用改狀態
            WS.WSASC.ServiceTranscode objTranscode = new WS.WSASC.ServiceTranscode();
            bool flagParse = objTranscode.ParseTranscodeJobHttpMessage(req.InputStream, out JobID, out ansJobStatus);
            objTranscode.Dispose();

            // William: 2012-04-16
            context.Response.Flush();
            context.Response.Close();

            if (!flagParse)
                return; // 由 ParseTranscodeJobHttpMessage 來寫LOG

            //取得要發送通知的內容
            string bookingInfo = DbAccess.Do_Query("SP_Q_TBBOOKING_TRAN_COMPLETE_EMAIL_CONTENT", "<Data><FSJOB_ID>" + JobID + "</FSJOB_ID></Data>");
            string bookingNo = "";
            string progName = "";
            string episode = "";
            string subProgName = "";
            string beg_timecode = "";
            string end_timecode = "";

            if (!string.IsNullOrEmpty(bookingInfo))
            {
                XmlDocument xBookingInfo = new XmlDocument();
                xBookingInfo.LoadXml(bookingInfo);
                if (xBookingInfo.GetElementsByTagName("Data").Count > 0)
                {
                    XmlNodeList xNodeListBookingInfo = xBookingInfo.GetElementsByTagName("Data");
                    XmlNode xNodeBookingInfo = xNodeListBookingInfo[0];
                    if (xNodeBookingInfo.ChildNodes[0].FirstChild != null)
                    {
                        bookingNo = xNodeBookingInfo.ChildNodes[0].FirstChild.Value;
                    }

                    if (xNodeBookingInfo.ChildNodes[2].FirstChild != null)
                    {
                        beg_timecode = xNodeBookingInfo.ChildNodes[2].FirstChild.Value;
                    }

                    if (xNodeBookingInfo.ChildNodes[3].FirstChild != null)
                    {
                        end_timecode = xNodeBookingInfo.ChildNodes[3].FirstChild.Value;
                    }

                    if (xNodeBookingInfo.ChildNodes[4].FirstChild != null)
                    {
                        progName = xNodeBookingInfo.ChildNodes[4].FirstChild.Value;
                    }

                    if (xNodeBookingInfo.ChildNodes[5].FirstChild != null)
                    {
                        subProgName = xNodeBookingInfo.ChildNodes[5].FirstChild.Value;
                    }

                    if (xNodeBookingInfo.ChildNodes[6].FirstChild != null)
                    {
                        episode = xNodeBookingInfo.ChildNodes[6].FirstChild.Value;
                    }
                }
            }
            


            if (ansJobStatus == "FINISH") //因為轉檔後的更新是系統，所以給System 
            {
                //成功要做的事情
                //更新轉檔狀態為"Y"
                DbAccess.Do_Transaction("SP_U_TBBOOKING_DETAIL_FILE_STATUS_BY_JOBID", "<Data><FSJOB_ID>" + JobID + "</FSJOB_ID><FCFILE_TRANSCODE_STATUS>Y</FCFILE_TRANSCODE_STATUS></Data>", "admin");
                
                //2014/11/26 david 增加節目名稱在檔名上
                try
                {
                    SysConfig sysconfig = new SysConfig();
                    ImpersonateUser iu = new ImpersonateUser();
                    iu.Impersonate(@"pts.org.tw", @"mamsystem", @"p@ssw0rd");
                    string fsPROGNAME = string.Empty;
                    
                    //取得節目名稱
                    string fsPROGNAME_XML = DbAccess.Do_Query("SP_Q_TBPROGNAME_BY_FSFILENO", "<Data><FCTYPE>1</FCTYPE><FSFILE_NO>0</FSFILE_NO><FSJOB_ID>" + JobID + "</FSJOB_ID></Data>");
                    
                    XmlDocument xDoc_PROGNAME = new XmlDocument();
                    xDoc_PROGNAME.LoadXml(fsPROGNAME_XML);
                    XmlNodeList xNodeList_PROGNAME = xDoc_PROGNAME.GetElementsByTagName("Data");
                    if (xNodeList_PROGNAME != null && xNodeList_PROGNAME.Count > 0)
                    {
                        XmlNode xNode1 = xNodeList_PROGNAME[0];
                        fsPROGNAME = (xNode1.ChildNodes[0].FirstChild != null ? xNode1.ChildNodes[0].FirstChild.Value : "");
                        fsPROGNAME = fsPROGNAME.Replace('\\', '\0').Replace('/', '\0').Replace(':', '\0').Replace('*', '\0').Replace('?', '\0').Replace('「', '\0').Replace('<', '\0').Replace('>', '\0').Replace('|', '\0');
                        string[] fsFILEs = Directory.GetFiles(sysconfig.sysConfig_Read("/ServerConfig/BOOKING_RETRIEVE") + xNode1.ChildNodes[1].FirstChild.Value + "\\" + xNode1.ChildNodes[3].FirstChild.Value);
                        if (fsFILEs != null && fsFILEs.Length > 0)
                        {
                            foreach (string fsFILE in fsFILEs)
                            {
                                string fsFILE_NAME = Path.GetFileNameWithoutExtension(fsFILE);
                                string fsEXT = Path.GetExtension(fsFILE);
                                if (fsFILE_NAME == xNode1.ChildNodes[2].FirstChild.Value + "_" + xNode1.ChildNodes[4].FirstChild.Value)
                                {
                                    string destPathNew = sysconfig.sysConfig_Read("/ServerConfig/BOOKING_RETRIEVE") + xNode1.ChildNodes[1].FirstChild.Value + "\\" + xNode1.ChildNodes[3].FirstChild.Value + "\\" + fsPROGNAME + "_" + fsFILE_NAME + fsEXT;
                                    File.Move(fsFILE, destPathNew);
                                }
                            }
                        }
                        
                    }

                    iu.Undo();
                }
                catch (Exception ex)
                {
                    MAM_PTS_DLL.Log.AppendTrackingLog("Handler_BookingTransCode.ashx", Log.TRACKING_LEVEL.ERROR, ex.Message);
                }

                //完成調用檔案處理後，寄信給調用者
                string user_mail = DbAccess.Do_Query("SP_Q_BOOKING_USER_EMAIL", "<Data><FSBOOKING_NO></FSBOOKING_NO><FSUSER_ID></FSUSER_ID><FSJOB_ID>" + JobID + "</FSJOB_ID></Data>");
                XmlDocument xDocUserMail = new XmlDocument();
                xDocUserMail.LoadXml(user_mail);
                if (xDocUserMail.GetElementsByTagName("Data").Count > 0)
                {
                    XmlNodeList xNodeListUserMail = xDocUserMail.GetElementsByTagName("Data");
                    XmlNode xNodeUserMail = xNodeListUserMail[0];
                    if (xNodeUserMail.ChildNodes[0].FirstChild.Value != "")
                    {
                        string mailTitle = "單號 " + bookingNo + " 節目名稱 " + progName + " 集數 " + episode + "  調用檔案轉檔完成";
                        string mailContent = "調用檔案<br />";
                        mailContent += "單號:" + bookingNo + "<br />";
                        mailContent += "節目名稱:" + progName + "<br />";
                        mailContent += "子集名稱:" + subProgName + "<br />";
                        mailContent += "集數:" + episode + "<br />";
                        mailContent += "段落Timecode起迄:" + beg_timecode +"~"+end_timecode + "<br />";
                        mailContent += "已經轉檔完成";
                        Protocol.SendEmail(xNodeUserMail.ChildNodes[0].FirstChild.Value, mailTitle, mailContent);
                    }
                }
            }
            else
            {
                //失敗要做的事情
                //更新轉檔狀態為"F"
                DbAccess.Do_Transaction("SP_U_TBBOOKING_DETAIL_FILE_STATUS_BY_JOBID", "<Data><FSJOB_ID>" + JobID + "</FSJOB_ID><FCFILE_TRANSCODE_STATUS>F</FCFILE_TRANSCODE_STATUS></Data>", "admin");

                //調用失敗，寄信給調用者
                string user_mail = DbAccess.Do_Query("SP_Q_BOOKING_USER_EMAIL", "<Data><FSBOOKING_NO></FSBOOKING_NO><FSUSER_ID></FSUSER_ID><FSJOB_ID>" + JobID + "</FSJOB_ID></Data>");
                XmlDocument xDocUserMail = new XmlDocument();
                xDocUserMail.LoadXml(user_mail);
                if (xDocUserMail.GetElementsByTagName("Data").Count > 0)
                {
                    XmlNodeList xNodeListUserMail = xDocUserMail.GetElementsByTagName("Data");
                    XmlNode xNodeUserMail = xNodeListUserMail[0];
                    if (xNodeUserMail.ChildNodes[0].FirstChild.Value != "")
                    {
                        string mailTitle = "單號 " + bookingNo + " 節目名稱 " + progName + " 集數 " + episode + "  調用檔案轉檔失敗成";
                        string mailContent = "調用檔案<br />";
                        mailContent += "單號:" + bookingNo + "<br />";
                        mailContent += "節目名稱:" + progName + "<br />";
                        mailContent += "子集名稱:" + subProgName + "<br />";
                        mailContent += "集數:" + episode + "<br />";
                        mailContent += "段落Timecode起迄:" + beg_timecode + "~" + end_timecode + "<br />";
                        mailContent += "轉檔失敗";
                        Protocol.SendEmail(xNodeUserMail.ChildNodes[0].FirstChild.Value, mailTitle, mailContent);
                    }
                }
            }

            
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}