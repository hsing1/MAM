﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Collections.ObjectModel;

using System.Data.SqlClient;
using System.Web.Configuration;

namespace PTS_MAM3.Web.DataSource
{
    public class ServerStruct
    {

        public string FSSERVER_NAME;

        public string FSSERVER_IP;

        public string FSGROUP;

        public int FSMAX_CPU;

        public string FSMONITOR_HD;

        public string FSMAX_HD;

        public int FSMAX_MEMORY;

        public int FSMAX_LAN;

        public string FSNETWORK_INTERFACE;

        public int FSMAX_WEB;

        public string FSWEB_NAME;


        public string FCAVAILABLE; 


    }


    public class ServerDetailStruct
    {

        public string FSSERVER_NAME;

        public string FSCPU;

        public string FSMEMORY;

        public string FSHD_FREEPERCENT;

        public string FSHD_FREEMB;

        public string FSSEND_NETWORK;

        public string FSREAD_NETWORK;

        public string FSWEB_USERS;

        public string FDUPDATE_TIME;

    }


    public class HDWebUserStruct
    {

        public string FSHD_FREEPERCENT;

        public string FSHD_FREEMB;

        public string FSWEB_USERS;

        public string FDUPDATE_TIME;
    }



    /// <summary>
    /// WSServerMonitor1 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSServerMonitor : System.Web.Services.WebService
    {

        [WebMethod]      
        public List<HDWebUserStruct> GetHDWebUser(string fsserver_name)
        {
            List<HDWebUserStruct> sysObjList = new List<HDWebUserStruct>();
            try
            {
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    if (fsserver_name.Trim() != string.Empty)
                    {

                        cn.Open();

                        cmd.CommandText = "SELECT m.FSMONITOR_HD, n.FSHD_FREEPERCENT,n.FSHD_FREEMB,n.FSWEB_USERS , n.FDUPDATE_TIME FROM TBMACHINE m, TBMACHINE_DETAIL n WHERE m.FSSERVER_NAME = n.FSSERVER_NAME AND m.FSSERVER_NAME = '" + fsserver_name + "' ";


                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    int drv = dr["FSMONITOR_HD"].ToString().Split(',').Length;
                                    string line1 = "";
                                    string line2 = "";

                                    for (int i = 0; i < drv; i++)
                                    {
                                        if (line1.Length > 0)
                                            line1 += ",";
                                        if (line2.Length > 0)
                                            line2 += ",";
                                        line1 += dr["FSMONITOR_HD"].ToString().Split(',')[i] + "{" + Convert.ToDouble(dr["FSHD_FREEPERCENT"].ToString().Split(',')[i]) + "%}";
                                        line2 += dr["FSMONITOR_HD"].ToString().Split(',')[i] + "{" + Convert.ToInt32(dr["FSHD_FREEMB"].ToString().Split(',')[i]) / 1024 + "GB}";
                                    }

                                    sysObjList.Add
                                    (
                                        new HDWebUserStruct
                                        {
                                            FSHD_FREEPERCENT = line1,
                                            FSHD_FREEMB = line2,
                                            FSWEB_USERS = dr["FSWEB_USERS"].ToString(),
                                            FDUPDATE_TIME = dr["FDUPDATE_TIME"].ToString()
                                        }
                                    );

                                }

                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                return null;
            }


            return sysObjList;
        }





        [WebMethod]
        public ServerDetailStruct GetServerDetailByName(string fsserver_name)
        {
            ServerDetailStruct sysObj = new ServerDetailStruct();
            try
            {
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    if (fsserver_name.Trim() != string.Empty)
                    {
                        cmd.CommandText = "SELECT  top 1 * FROM TBMACHINE_DETAIL WHERE FSSERVER_NAME = '" + fsserver_name + "' ORDER BY FDUPDATE_TIME DESC ";
                    }
                    //else
                    // cmd.CommandText = "SELECT * FROM TBMACHINE_DETAIL ";

                    cn.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            if (dr.Read())
                            {
                                sysObj =
                                new ServerDetailStruct
                                {
                                    FDUPDATE_TIME = dr["FDUPDATE_TIME"].ToString(),

                                    FSCPU = dr["FSCPU"].ToString(),
                                    FSHD_FREEMB = dr["FSHD_FREEMB"].ToString(),
                                    FSHD_FREEPERCENT = dr["FSHD_FREEPERCENT"].ToString(),
                                    FSMEMORY = dr["FSMEMORY"].ToString(),
                                    FSREAD_NETWORK = dr["FSREAD_NETWORK"].ToString(),
                                    FSSEND_NETWORK = dr["FSSEND_NETWORK"].ToString(),
                                    FSSERVER_NAME = dr["FSSERVER_NAME"].ToString(),
                                    FSWEB_USERS = dr["FSWEB_USERS"].ToString()
                                };
                            }
                        }
                        else
                            return null;

                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            return sysObj;
        }

        [WebMethod]
        public List<ServerStruct> GetServerListByGroup(string fsgroup)
        {

            List<ServerStruct> ServerList = new List<ServerStruct>();

            try
            {
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    if (fsgroup.Trim() != string.Empty)
                    {
                        cmd.CommandText = "SELECT * FROM TBMACHINE WHERE FSGROUP = '" + fsgroup + "' ";
                    }
                    else
                        cmd.CommandText = "SELECT * FROM TBMACHINE ";

                    cn.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                ServerList.Add
                                (
                                    new ServerStruct
                                    {
                                        FSGROUP = dr["FSGROUP"].ToString(),
                                        FSMAX_CPU = Convert.ToInt32(dr["FSMAX_CPU"]),
                                        FSMAX_HD = dr["FSMAX_HD"].ToString(),
                                        FSMAX_LAN = Convert.ToInt32(dr["FSMAX_LAN"]),
                                        FSMAX_MEMORY = Convert.ToInt32(dr["FSMAX_MEMORY"]),
                                        FSMAX_WEB = Convert.ToInt32(dr["FSMAX_WEB"]),
                                        FSMONITOR_HD = dr["FSMONITOR_HD"].ToString(),
                                        FSNETWORK_INTERFACE = dr["FSNETWORK_INTERFACE"].ToString(),
                                        FSSERVER_IP = dr["FSSERVER_IP"].ToString(),
                                        FSSERVER_NAME = dr["FSSERVER_NAME"].ToString(),
                                        FSWEB_NAME = dr["FSWEB_NAME"].ToString(),
                                        FCAVAILABLE = dr["FCAVAILABLE"].ToString() 
                                    }
                                );
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                return null;
            }



            return ServerList;
        }
    }
}
