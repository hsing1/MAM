﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Deltaflow_ClientAPI;
using PTS_MAM3.Web.Class;

namespace PTS_MAM3.Web.DataSource
{
    /// <summary>
    /// 用於 新增、修改、查詢 代理人的WebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class FlowAgent : System.Web.Services.WebService
    {
        Deltaflow_ClientAPI.Agent agentType3Insert = new Agent();
        /// <summary>
        /// 刪除代理人在FLOW中是代理人3的模式
        /// 關係到的資料表在DeltaFlow是
        /// [PTSFlow].[dbo].[Agent_Flow](master資料表)
        /// [PTSFlow].[dbo].[AgentDetail_Flow](detail資料表)
        /// </summary>
        /// <param name="aid">代理模式3的PK</param>
        /// <param name="UserID">提出刪除人的ID</param>
        /// <returns>True:成功 False:失敗</returns>
        [WebMethod]
        public bool DelFlowAgent(string aid,string UserID)
        {
            bool rtnResult=false;
            Dictionary<string, string> sqlParametersAgent = new Dictionary<string, string>();
            sqlParametersAgent.Add("aid", aid);
            rtnResult = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_PTSFlow_AgentType3", sqlParametersAgent, UserID);
            return rtnResult;
        }
        /// <summary>
        /// 新增代理人在FLOW中是代理人3的模式
        /// 關係到的資料表在DeltaFlow是
        /// [PTSFlow].[dbo].[Agent_Flow](master資料表)
        /// [PTSFlow].[dbo].[AgentDetail_Flow](detail資料表)
        /// </summary>
        /// <param name="strHandleName">請求者的ID(PTS中員編)</param>
        /// <param name="strAgentName">代理人的ID(PTS中是員編)</param>
        /// <param name="strSdate">代理啟始時間</param>
        /// <param name="strEdate">代理結束時間</param>
        /// <param name="strEFlowid">要代理的表單編號(以逗號隔開ex:1,3,4)</param>
        /// <returns>成功:True 失敗:False</returns>
        [WebMethod]
        public bool InsFlowAgent(string strHandleName, string strAgentName, string strSdate, string strEdate, string strEFlowid)
        {
            bool rtnResult = false;
            //agentType3Insert.InsertAgentForType3(strHandleName, strAgentName, strSdate, strEdate, strEFlowid);
            //SP_I_PTSFlow_AgentType3
            Dictionary<string, string> sqlParametersAgent = new Dictionary<string, string>();
            sqlParametersAgent.Add("vMember_Name", strHandleName);
            sqlParametersAgent.Add("vAgent_Name", strAgentName);
            sqlParametersAgent.Add("vSDate", strSdate);
            sqlParametersAgent.Add("vEDate", strEdate);
            sqlParametersAgent.Add("cslist", strEFlowid);
            rtnResult = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_PTSFlow_AgentType3", sqlParametersAgent, strHandleName);
            return rtnResult;
        }

        /// <summary>
        /// 取得個人已設定代理人的ID列表
        /// </summary>
        /// <param name="userName">代理人ID</param>
        /// <returns>代理人列表</returns>
        [WebMethod]
        public List<Class_Flow_Agent_Flow> GetFlowAgentList(string userName)
        {
            List<Class.Class_Flow_Agent_Flow> Agent_Flow_List = new List<Class_Flow_Agent_Flow>();
            Dictionary<string, string> sqlParametersAgent = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResultAgent;
            sqlParametersAgent.Add("FSUSER_ID", userName);
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_GET_Agent_Flow_ByMemberid", sqlParametersAgent, out sqlResultAgent))
                Agent_Flow_List = new List<Class.Class_Flow_Agent_Flow>();    // 發生SQL錯誤時，回傳空的東西，而不是NULL
            if (sqlResultAgent.Count > 0)
            {
                //取得Agent_Flow TABLE裡面的資料
                for (int i = 0; i < sqlResultAgent.Count; i++)
                {
                    Class.Class_Flow_Agent_Flow CFIF = new Class.Class_Flow_Agent_Flow();
                    CFIF.aid = sqlResultAgent[i]["aid"];
                    CFIF.memberid = sqlResultAgent[i]["memberid"];
                    CFIF.MemberFSID = sqlResultAgent[i]["MemberFSID"];
                    CFIF.MemberFSNAME = sqlResultAgent[i]["MemberFSNAME"];
                    CFIF.agentid = sqlResultAgent[i]["agentid"];
                    CFIF.AgentFSID = sqlResultAgent[i]["AgentFSID"];
                    CFIF.AgentFSNAME = sqlResultAgent[i]["AgentFSNAME"];
                    CFIF.SDate = sqlResultAgent[i]["SDate"];
                    CFIF.EDate = sqlResultAgent[i]["EDate"];
                    CFIF.Status = sqlResultAgent[i]["Status"];
                    CFIF.AgreeDate = sqlResultAgent[i]["AgreeDate"];
                    CFIF.SendDate = sqlResultAgent[i]["SendDate"];
                    CFIF.ifopen = sqlResultAgent[i]["ifopen"];
                    Agent_Flow_List.Add(CFIF);
                }
            }
            foreach (var item in Agent_Flow_List)
            {
                item.ifopen = GetAgentDetailFlow(item.aid);
            }
            return Agent_Flow_List;
        }
        /// <summary>
        /// 內部使用的Function
        /// </summary>
        /// <param name="aid">傳入Agent_Flow的PK</param>
        /// <returns>串起來的表單中文描述</returns>
        public string GetAgentDetailFlow(string aid)
        {
            string rtnString = "";
            Dictionary<string, string> sqlParameter = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResultAgent;
            sqlParameter.Add("aid", aid);
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_GET_AgentDetail_Flow", sqlParameter, out sqlResultAgent))
                return "";   // 發生SQL錯誤時，回傳空字串
            if (sqlResultAgent.Count > 0)
            {
                //取得AgentDetail_Flow TABLE裡面的資料
                for (int i = 0; i < sqlResultAgent.Count; i++)
                {
                    if (sqlResultAgent[i]["eflowid"] == "0")
                        rtnString = "  全部";
                    if (sqlResultAgent[i]["eflowid"] == "3")
                        rtnString += "  成案表單";
                    if (sqlResultAgent[i]["eflowid"] == "6")
                        rtnString += "  送帶轉檔表單";
                    if (sqlResultAgent[i]["eflowid"] == "7")
                        rtnString += "  入庫表單";
                    if (sqlResultAgent[i]["eflowid"] == "8")
                        rtnString += "  調用表單";
                    if (sqlResultAgent[i]["eflowid"] == "9")
                        rtnString += "  託播單審核表單";
                    if (sqlResultAgent[i]["eflowid"] == "10")
                        rtnString += "  入庫置換表單";
                    if (sqlResultAgent[i]["eflowid"] == "11")
                        rtnString += "  送帶轉檔置換表單";
                }
            }
            return rtnString;
        }
    }
}
