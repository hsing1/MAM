﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Xml;
using System.Xml.Linq;
using System.Text.RegularExpressions;

namespace MAMWebService
{
    /// <summary>
    ///wsUserData 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    //[ScriptMethod(ResponseFormat = ResponseFormat.Xml)]

    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下列一行。
    // [System.Web.Script.Services.ScriptService]
    public class wsUserData : System.Web.Services.WebService
    {
        //public static string sqlConnStr = @"server='10.13.210.33';uid='mam_admin';pwd='ptsP@ssw0rd';database='MAM'";
        public static string sqlConnStr = @"server='10.13.210.33';uid='EZUser';pwd='ezflow';database='MAM'";
        public static string str = @"select u.FSUSER_ID from TBUSERS as u inner join TBUSER_GROUP as g on u.FSUSER_ID = g.FSUSER_ID where u.FSUSER_ID = @FSUSER_ID"; //查詢有MAM 權限者
        public static string str2 = @"select FSUSER_ID from TBUSER_GROUP where FSGROUP_ID = 'Admins'";

        [WebMethod]
        public string GetExists(string userId)
        {
            ResClass res = null;
            List<ResClass> sqlResult = new List<ResClass>();
            SqlConnection conn = new SqlConnection(sqlConnStr);
            DataTable dt = new DataTable(), dt2 = null;
            StringBuilder adminStr = null;
            XDocument xml = null;
            string pattern = @"^\d{5}$", tmpStr = null;
            Regex regex = new Regex(pattern);

            SqlDataAdapter da = new SqlDataAdapter(str, conn);
            SqlDataAdapter da2 = new SqlDataAdapter(str2, conn);
            da.SelectCommand.Parameters.AddWithValue("FSUSER_ID", userId);

            try
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();

                da.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    res= new ResClass("RetID", "0");
                    sqlResult.Add(res);
                    res= new ResClass("ErrMsg", "MAM active user");
                    sqlResult.Add(res);
                    res= new ResClass("SysName", "MAM");
                    sqlResult.Add(res);

                } else
                {
                    res= new ResClass("RetID", "1");
                    sqlResult.Add(res);
                    res= new ResClass("ErrMsg", "Not MAM user");
                    sqlResult.Add(res);
                    res= new ResClass("SysName", "MAM");
                    sqlResult.Add(res);
                }
                //get administrator
                dt2 = new DataTable();
                da2.Fill(dt2);

                adminStr = new StringBuilder();
                if (dt2.Rows.Count > 0) {
                    tmpStr = dt2.Rows[0][0].ToString();
                    if (regex.IsMatch(tmpStr))
                        adminStr.Append(tmpStr);
                }


                for (int i = 1; i < dt2.Rows.Count; i++) {
                    tmpStr = dt2.Rows[i][0].ToString();
                    if (regex.IsMatch(tmpStr)) {
                        adminStr.Append(" ");
                        adminStr.Append(tmpStr);
                    }
                }
                res= new ResClass("AdmEmpIDs", adminStr.ToString());
                sqlResult.Add(res);
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
                try
                {
                    conn.Close();
                    conn.Dispose();
                }
                catch
                {

                }
            }

            XmlWriterSettings setting = new XmlWriterSettings();
            setting.Indent = true;
            setting.OmitXmlDeclaration = true;

            StringBuilder sb = new StringBuilder();
            XmlWriter writer = XmlWriter.Create(sb, setting);

            writer.WriteStartDocument();
            writer.WriteStartElement("root");
            for (int i = 0; i < sqlResult.Count; i++)
            {
                writer.WriteStartElement(sqlResult[i].key);
                writer.WriteString(sqlResult[i].value);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Flush();

            XmlDocument xdoc = new XmlDocument();
            xdoc.LoadXml(sb.ToString());

            return sb.ToString();
        } //end GetExists

        [WebMethod]
        public List<ResClass> GetExists_C(string userId)
        {
            ResClass res = null;
            List<ResClass> sqlResult = new List<ResClass>();
            SqlConnection conn = new SqlConnection(sqlConnStr);
            DataTable dt = new DataTable(), dt2 = null;
            StringBuilder adminStr = null;
            XDocument xml = null;
            string pattern = @"^\d{5}$", tmpStr = null;
            Regex regex = new Regex(pattern);

            SqlDataAdapter da = new SqlDataAdapter(str, conn);
            SqlDataAdapter da2 = new SqlDataAdapter(str2, conn);
            da.SelectCommand.Parameters.AddWithValue("FSUSER_ID", userId);

            try
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();

                da.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    res= new ResClass("RetID", "0");
                    sqlResult.Add(res);
                    res= new ResClass("ErrMsg", "MAM active user");
                    sqlResult.Add(res);
                    res= new ResClass("SysName", "MAM");
                    sqlResult.Add(res);

                } else
                {
                    res= new ResClass("RetID", "1");
                    sqlResult.Add(res);
                    res= new ResClass("ErrMsg", "Not MAM user");
                    sqlResult.Add(res);
                    res= new ResClass("SysName", "MAM");
                    sqlResult.Add(res);
                }
                //get administrator
                dt2 = new DataTable();
                da2.Fill(dt2);

                adminStr = new StringBuilder();
                if (dt2.Rows.Count > 0)
                    tmpStr = dt2.Rows[0][0].ToString();
                    if (regex.IsMatch(tmpStr))
                        adminStr.Append(tmpStr);

                for (int i = 1; i < dt2.Rows.Count; i++) {
                    tmpStr = dt2.Rows[i][0].ToString();
                    if (regex.IsMatch(tmpStr))
                    {
                        adminStr.Append(" ");
                        adminStr.Append(tmpStr);
                    }
                }
                res= new ResClass("AdmEmpIDs", adminStr.ToString());
                sqlResult.Add(res);
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
                try
                {
                    conn.Close();
                    conn.Dispose();
                }
                catch
                {

                }
            }

            return sqlResult;
        } //end GetExists_C

        //[WebMethod]
        public XDocument GetExists_old(string userId)
        {
            XDocument xml = new XDocument();
            ResClass res = null;
            List<ResClass> sqlResult = new List<ResClass>();
            SqlConnection conn = new SqlConnection(sqlConnStr);
            DataTable dt = new DataTable();
            //string str = @"select COUNT (*) as IDCOUNT from  dbo.TBUSERS where dbo.TBUSERS.FSUSER_ID = @FSUSER_ID or dbo.TBUSERS.FSUSER = @FSUSER_ID";
            string str = @"select COUNT (*) as IDCOUNT from  dbo.TBUSERS where dbo.TBUSERS.FSUSER_ID = @FSUSER_ID and FCACTIVE = 1";
            SqlDataAdapter da = new SqlDataAdapter(str, conn);
            da.SelectCommand.Parameters.AddWithValue("FSUSER_ID", userId);

            try
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();

                da.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    res= new ResClass("RedId", "0");
                    sqlResult.Add(res);
                    res= new ResClass("ErrMsg", "MAM active user");
                    sqlResult.Add(res);
                    res= new ResClass("SysName", "MAM");
                    sqlResult.Add(res);
                    res= new ResClass("EmpId", userId);
                    sqlResult.Add(res);
                } else
                {
                    res= new ResClass("RedId", "1");
                    sqlResult.Add(res);
                    res= new ResClass("ErrMsg", "Not MAM user");
                    sqlResult.Add(res);
                    res= new ResClass("SysName", "MAM");
                    sqlResult.Add(res);
                    res= new ResClass("EmpId", userId);
                    sqlResult.Add(res);
                }
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
                try
                {
                    conn.Close();
                    conn.Dispose();
                }
                catch
                {

                }
            }

            XNamespace ns = "http://www.w3.org/2001/XMLSchema";
            XDeclaration decl = new XDeclaration("1.0", "utf-8", "yes");
            XElement document = new XElement(ns + "ArrayOfResClass",
                new XElement(ns + "ResClass",
                    new XElement(ns + "key", sqlResult[0].key),
                    new XElement(ns + "value", sqlResult[0].value)),
                new XElement(ns + "ResClass",
                    new XElement(ns + "key", sqlResult[1].key),
                    new XElement(ns + "value", sqlResult[1].value)),
                new XElement(ns + "ResClass",
                    new XElement(ns + "key", sqlResult[2].key),
                    new XElement(ns + "value", sqlResult[2].value)),
                new XElement(ns + "ResClass",
                    new XElement(ns + "key", sqlResult[3].key,
                    new XElement(ns + "value", sqlResult[3].value))
                )
            );

            xml.Add(document);
            xml.Add(decl);

            /*
            document = new XElement("books",
                new XElement("book",
                    new XElement("title", "Sams Teach Yourself Visual C# 2010 in 24 Hours"),
		            new XElement("isbn - 10", "0 - 672 - 33101 - 2"),
                    new XElement("author", "Dorman"),
                    new XElement("price", new XAttribute("currency", "US"), 39.99M),
                    new XElement("publisher",
                        new XElement("name", "Sams Publishing"),
                        new XElement("state", "IN"))));
            */
            return xml;
        }

        public XElement GetExists_error(string userId)
        {
            ResClass res = null;
            List<ResClass> sqlResult = new List<ResClass>();
            SqlConnection conn = new SqlConnection(sqlConnStr);
            DataTable dt = new DataTable();
            //string str = @"select COUNT (*) as IDCOUNT from  dbo.TBUSERS where dbo.TBUSERS.FSUSER_ID = @FSUSER_ID or dbo.TBUSERS.FSUSER = @FSUSER_ID";
            string str = @"select COUNT (*) as IDCOUNT from  dbo.TBUSERS where dbo.TBUSERS.FSUSER_ID = @FSUSER_ID and FCACTIVE = 1";
            SqlDataAdapter da = new SqlDataAdapter(str, conn);
            da.SelectCommand.Parameters.AddWithValue("FSUSER_ID", userId);

            try
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();

                da.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    res= new ResClass("RedId", "0");
                    sqlResult.Add(res);
                    res= new ResClass("ErrMsg", "MAM active user");
                    sqlResult.Add(res);
                    res= new ResClass("SysName", "MAM");
                    sqlResult.Add(res);
                    res= new ResClass("EmpId", userId);
                    sqlResult.Add(res);
                } else
                {
                    res= new ResClass("RedId", "1");
                    sqlResult.Add(res);
                    res= new ResClass("ErrMsg", "Not MAM user");
                    sqlResult.Add(res);
                    res= new ResClass("SysName", "MAM");
                    sqlResult.Add(res);
                    res= new ResClass("EmpId", userId);
                    sqlResult.Add(res);
                }
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
                try
                {
                    conn.Close();
                    conn.Dispose();
                }
                catch
                {

                }
            }

            XNamespace ns = "http://tempuri.org/";
            XElement document = new XElement(ns + "ArrayOfResClass",
                new XElement(ns + "ResClass",
                    new XElement(ns + "key", sqlResult[0].key),
                    new XElement(ns + "value", sqlResult[0].value)),
                new XElement(ns + "ResClass",
                    new XElement(ns + "key", sqlResult[1].key),
                    new XElement(ns + "value", sqlResult[1].value)),
                new XElement(ns + "ResClass",
                    new XElement(ns + "key", sqlResult[2].key),
                    new XElement(ns + "value", sqlResult[2].value)),
                new XElement(ns + "ResClass",
                    new XElement(ns + "key", sqlResult[3].key,
                    new XElement(ns + "value", sqlResult[3].value))
                )
            );

            /*
            document = new XElement("books",
                new XElement("book",
                    new XElement("title", "Sams Teach Yourself Visual C# 2010 in 24 Hours"),
		            new XElement("isbn - 10", "0 - 672 - 33101 - 2"),
                    new XElement("author", "Dorman"),
                    new XElement("price", new XAttribute("currency", "US"), 39.99M),
                    new XElement("publisher",
                        new XElement("name", "Sams Publishing"),
                        new XElement("state", "IN"))));
            */
            return document;
        } //end GetExists 

        //[WebMethod]
        public List<ResClass> GetExists_Class2(string userId)
        {
            ResClass res = null;
            List<ResClass> sqlResult = new List<ResClass>();
            SqlConnection conn = new SqlConnection(sqlConnStr);
            DataTable dt = new DataTable(), dt2 = null;
            //string str = @"select COUNT (*) as IDCOUNT from  dbo.TBUSERS where dbo.TBUSERS.FSUSER_ID = @FSUSER_ID or dbo.TBUSERS.FSUSER = @FSUSER_ID";
            string str = @"select COUNT (*) as IDCOUNT from  dbo.TBUSERS where dbo.TBUSERS.FSUSER_ID = @FSUSER_ID and FCACTIVE = 1";
            string str2 = @"select FSUSER_ID from TBUSER_GROUP where FSGROUP_ID = 'Admins'";
            string adminStr = null;

            SqlDataAdapter da = new SqlDataAdapter(str, conn);
            SqlDataAdapter da2 = new SqlDataAdapter(str2, conn);
            da.SelectCommand.Parameters.AddWithValue("FSUSER_ID", userId);

            try
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();

                da.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    res= new ResClass("RedId", "0");
                    sqlResult.Add(res);
                    res= new ResClass("ErrMsg", "MAM active user");
                    sqlResult.Add(res);
                    res= new ResClass("SysName", "MAM");
                    sqlResult.Add(res);
                    res= new ResClass("EmpId", userId);
                    sqlResult.Add(res);

                    //get administrator
                    dt2 = new DataTable();
                    da2.Fill(dt2);

                    adminStr = dt2.Rows[0][0].ToString();
                } else
                {
                    res= new ResClass("RedId", "1");
                    sqlResult.Add(res);
                    res= new ResClass("ErrMsg", "Not MAM user");
                    sqlResult.Add(res);
                    res= new ResClass("SysName", "MAM");
                    sqlResult.Add(res);
                    res= new ResClass("EmpId", userId);
                    sqlResult.Add(res);
                }
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
                try
                {
                    conn.Close();
                    conn.Dispose();
                }
                catch
                {

                }
            }
            return sqlResult;
        } //end GetExists 

        //[WebMethod]
        public List<ResClass> GetExists_Class(string userId)
        {
            string sqlConnStr = @"server='10.13.210.33';uid='mam_admin';pwd='ptsP@ssw0rd';database='MAM'";
            ResClass res = null;
            List<ResClass> sqlResult = new List<ResClass>();
            SqlConnection conn = new SqlConnection(sqlConnStr);
            DataTable dt = new DataTable();
            //string str = @"select COUNT (*) as IDCOUNT from  dbo.TBUSERS where dbo.TBUSERS.FSUSER_ID = @FSUSER_ID or dbo.TBUSERS.FSUSER = @FSUSER_ID";
            string str = @"select COUNT (*) as IDCOUNT from  dbo.TBUSERS where dbo.TBUSERS.FSUSER_ID = @FSUSER_ID and FCACTIVE = 1";
            string str2 = @"select FSUSER_ID from TBUSER_GROUP where FSGROUP_ID = 'Admins'";

            SqlDataAdapter da = new SqlDataAdapter(str, conn);
            da.SelectCommand.Parameters.AddWithValue("FSUSER_ID", userId);

            try
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();

                da.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    res= new ResClass("RedId", "0");
                    sqlResult.Add(res);
                    res= new ResClass("ErrMsg", "MAM active user");
                    sqlResult.Add(res);
                    res= new ResClass("SysName", "MAM");
                    sqlResult.Add(res);
                    res= new ResClass("EmpId", userId);
                    sqlResult.Add(res);
                } else
                {
                    res= new ResClass("RedId", "1");
                    sqlResult.Add(res);
                    res= new ResClass("ErrMsg", "Not MAM user");
                    sqlResult.Add(res);
                    res= new ResClass("SysName", "MAM");
                    sqlResult.Add(res);
                    res= new ResClass("EmpId", userId);
                    sqlResult.Add(res);
                }
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
                try
                {
                    conn.Close();
                    conn.Dispose();
                }
                catch
                {

                }
            }
            return sqlResult;
        } //end GetExists 

        //[WebMethod]
        public List<Dictionary<string, string>> GetExists1(string userId)
        {
            string sqlConnStr = @"server='10.13.210.33';uid='mam_admin';pwd='ptsP@ssw0rd';database='MAM'";
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>(), tmpDict = null;
            sqlParameters.Add("FSUSER_ID", userId);
            List<Dictionary<string, string>> sqlResult = new List<Dictionary<string, string>>();
            SqlConnection conn = new SqlConnection(sqlConnStr);
            //SqlDataAdapter da = new SqlDataAdapter("SP_Q_TBUSER_ID_ISEXIST", conn);
            DataTable dt = new DataTable();
            sqlParameters.Add("FUSER_ID", userId);
            StringBuilder sb = new StringBuilder();
            string str = @"select COUNT (*) as IDCOUNT from  dbo.TBUSERS where dbo.TBUSERS.FSUSER_ID = @FSUSER_ID or dbo.TBUSERS.FSUSER = @FSUSER_ID";
            SqlDataAdapter da = new SqlDataAdapter(str, conn);
            da.SelectCommand.Parameters.AddWithValue("FSUSER_ID", userId);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();

                da.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        tmpDict = new Dictionary<string, string>();
                        for (int j = 0; j <= dt.Columns.Count - 1; j++)
                        {
                            if (Convert.IsDBNull(dt.Rows[i][j]))
                                tmpDict.Add(dt.Columns[j].ColumnName, string.Empty);
                            else
                                tmpDict.Add(dt.Columns[j].ColumnName, dt.Rows[i][j].ToString());
                        }
                        sqlResult.Add(tmpDict);

                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
                try
                {
                    conn.Close();
                    conn.Dispose();
                }
                catch
                {

                }
            }
            return sqlResult;
        } //end GetExists 
    }
}
