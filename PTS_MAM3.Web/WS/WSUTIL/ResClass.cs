﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MAMWebService
{
    public class ResClass
    {
        public string key;
        public string value;

        public ResClass() { }

        public ResClass(string key, string value)
        {
            this.key = key;
            this.value = value;
        }
    }
}