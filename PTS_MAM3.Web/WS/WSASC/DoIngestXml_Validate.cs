﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.WS.WSASC
{
    public partial class DoIngestXml
    {
        private bool fnValidate_TranscodeType()
        {
            // 限制：只能是 TAPE 或 FILE
            if (!string.IsNullOrEmpty(TransCodeType) && (TransCodeType == "TAPE" || TransCodeType == "FILE"))
                return true;
            // 
            ErrorMessage = "Error: /JobIngest/TransCodeType can only be 'TAPE' or 'FILE'.";
            return false;
        }

        private bool fnValidate_IsPartialTranscode()
        {
            // 
            if (string.IsNullOrEmpty(this.IsFullTranscode))
                this.IsFullTranscode = "Y";
            // 限制：只能是 'Y' or 'N'
            if (this.IsFullTranscode == "Y" || this.IsFullTranscode == "N")
                return true;
            // 
            ErrorMessage = "Error: /JobIngest/IsFullTranscode can only be 'Y' or 'N'.";
            return false;
        }

        private bool fnValidate_IsNativeFormat()
        {
            // 
            if (string.IsNullOrEmpty(this.IsNativeFormat))
                this.IsNativeFormat = "N";
            // 限制：只能是 'Y' or 'N'
            if (this.IsNativeFormat == "Y" || this.IsNativeFormat == "N")
                return true;
            // 
            ErrorMessage = "Error: /JobIngest/IsNativeFormat can only be 'Y' or 'N'.";
            return false;
        }

        private bool fnValidate_EncoderProfilePath()
        {
            // 目前的限制比較寬鬆
            if (!string.IsNullOrEmpty(EncoderProfliePath))
                return true;
            //
            ErrorMessage = "Error: /JobIngest/EncoderProfilePath value is invalid.";
            return false;
        }

        private bool fnValidate_SourceFrom()
        {
            // Source From 可能會是VCR機器編號或是檔案路徑
            // 目前不去檢查內容
            if (!string.IsNullOrEmpty(SourceFrom))
                return true;
            // 
            ErrorMessage = "Error: /JobIngest/SourceFrom value is invalid.";
            return false;
        }

        private bool fnValidate_DestinationTo()
        {
            // 目前不去檢查內容
            if (string.IsNullOrEmpty(DestinationTo))
            {
                ErrorMessage = "Error: /JobIngest/DestinationTo value is invalid.";
                return false;
            }
            // 
            // 不是很好的寫法，如果尾巴有帶 \ ，就移掉
            if (DestinationTo.EndsWith(@"\"))
                DestinationTo = DestinationTo.Substring(0, DestinationTo.Length - 1);
            // 細部拆解，因為Anystream後面需要使用
            DestinationToDir = string.Concat(System.IO.Path.GetDirectoryName(DestinationTo), "\\");
            DestinationToFile = System.IO.Path.GetFileNameWithoutExtension(DestinationTo);
            return true;
        }

        private bool fnValidate_SourceFormat()
        {
            if (string.IsNullOrEmpty(SourceFormat))
                SourceFormat = "SD";

            if (SourceFormat == "SD" || SourceFormat == "HD")
                return true;
            else
                return false;
        }

        private bool fnValidate_DestinationFormat()
        {
            if (string.IsNullOrEmpty(DestinationFormat))
                DestinationFormat = "SD";

            if (DestinationFormat == "SD" || DestinationFormat == "HD")
                return true;
            else
                return false;
        }

        private bool fnValidate_StartTimecode()
        {
            // 
            if (string.IsNullOrEmpty(StartTimecode) || !MAM_PTS_DLL.CheckFormat.CheckFormat_TimeCode(StartTimecode))
            {
                ErrorMessage = "Error: /JobIngest/StartTimecode value is invalid.";
                return false;
            }
            // 
            return true;
        }

        private bool fnValidate_EndTimecode()
        {
            // 
            if (string.IsNullOrEmpty(EndTimecode) || !MAM_PTS_DLL.CheckFormat.CheckFormat_TimeCode(EndTimecode))
            {
                ErrorMessage = "Error: /JobIngest/EndTimecode value is invalid.";
                return false;
            }
            //
            return true;
        }

        private bool fnValidate_PostbackAddress()
        {
            // 檢查是否符合HTTP POST格式
            if (!string.IsNullOrEmpty(PostbackAddress) && !MAM_PTS_DLL.CheckFormat.CheckFormat_HttpAddress(PostbackAddress))
            {
                ErrorMessage = "Error: /JobIngest/PostbackAddress value is invalid.";
                return false;
            }
            //
            return true;
        }

        private bool fnValidate_CreateLowRes()
        {
            if (string.IsNullOrEmpty(CreateLowRes) || (CreateLowRes != "Y" && CreateLowRes != "N"))
            {
                ErrorMessage = "Error: /JobIngest/CreateLowRes can only be 'Y' or 'N'.";
                return false;
            }
            //
            if (CreateLowRes == "N")
                return true;
            // 這邊寫的比較簡略
            if (string.IsNullOrEmpty(LowResDir))
            {
                // 選 Y，但沒填東西，錯誤動作
                ErrorMessage = "Error: /JobIngest/LowResDir must have value when /JobIngest/CreateLowRes = 'Y'.";
                return false;
            }
            //
            return true;
        }

        private bool fnValidate_CaptureKeyframe()
        {
            if (string.IsNullOrEmpty(CaptureKeyframe) || (CaptureKeyframe != "Y" && CaptureKeyframe != "N"))
            {
                ErrorMessage = "Error: /JobIngest/CaptureKeyframe can only be 'Y' or 'N'.";
                return false;
            }
            //
            return true;
        }

        private bool fnValidate_DisplayLogo()
        {
            if (string.IsNullOrEmpty(DisplayLogo) || (DisplayLogo != "Y" && DisplayLogo != "N"))
            {
                ErrorMessage = "Error: /JobIngest/DisplayLogo can only be 'Y' or 'N'.";
                return false;
            }
            //
            if (DisplayLogo == "N")
                return true;
            // 這邊寫的比較簡略
            if (string.IsNullOrEmpty(LogoProfilePath))
            {
                // 選 Y，但沒填東西，錯誤動作
                ErrorMessage = "Error: /JobIngest/LogoProfilePath must have value when /JobIngest/DisplayLogo = 'Y'.";
                return false;
            }
            //
            return true;
        }

        private bool fnValidate_Note()
        {
            if (string.IsNullOrEmpty(this.Note))
                this.Note = string.Empty;
            return true;
        }
    }
}