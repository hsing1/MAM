﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Xml;
using System.Collections;
using System.Collections.ObjectModel;
using System.Configuration;

namespace PTS_MAM3.Web.WS.WSASC
{
    /// <summary>
    /// WSTranscode 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public partial class ServiceTranscode : System.Web.Services.WebService
    {
        // 這個檔案是做轉檔的入口
        // 因為要考慮到如果來源是 HSM 的情況
        // 所以算是整合 TSM 跟 Anystream 的入口

        public const long ERRORID = -1;                         // 當發生錯誤時的ID
        public const string SYSCODE = "ASC";
        public const int ANYSTREAM_DEFAULT_PRIORITY = 20;       // 預設的轉檔任務重要性

        // 轉檔任務的狀態列表
        public enum TRANSCODE_STATUS { INITIAL, TSMPROCESS, PARPROCESS, ANSPROCESS, FINISH, SOURCE_NOT_EXIST, TSMERROR, PARERROR, ANSERROR, SYSERROR };
        // 轉檔任務的項目列表，為與前版(未儲存該項資訊)相容，所以 0 代表舊版資料(不可顯示)
        public enum TRANSCODE_TYPE { NOT_AVAILABLE, TAPE, FILE };

        /// <summary>
        /// 進行轉檔任務 (來源檔在 TSM 的 Offline 亦可)
        /// </summary>
        /// <param name="transcodeXml">由台員自訂的轉檔XML內容</param>
        /// <returns></returns>
        [WebMethod]
        public long DoTranscode(string transcodeXml)
        {
            string fsPROGRAM = MAM_PTS_DLL.ConvertFormat.Combine_TraceLogFSProgram(SYSCODE, System.Reflection.MethodBase.GetCurrentMethod().Name);

            // 檢查輸入的XML格式是否正確 (檢查 XML 的重要節點)
            DoIngestXml objXml = new DoIngestXml();
            if (!objXml.ImportXML(transcodeXml))
            {
                string errorMsg = string.Concat("無法正確解析輸入的XML：", objXml.ErrorMessage, Environment.NewLine, "原始內容：", objXml.InputXmlContent);
                MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, errorMsg);
                return ERRORID;
            }

            return DoTranscode(objXml);
        }

        /// <summary>
        /// 進行轉檔任務 (來源檔在 TSM 的 Offline 亦可)
        /// </summary>
        /// <param name="objXml">DoIngestXml的物件，內含轉檔資訊的屬性</param>
        /// <returns></returns>
        public long DoTranscode(DoIngestXml objXml)
        {
            string fsPROGRAM = MAM_PTS_DLL.ConvertFormat.Combine_TraceLogFSProgram(SYSCODE, System.Reflection.MethodBase.GetCurrentMethod().Name);

            string ansXml = string.Empty;                       // 要傳給 Anystream 的 XML 內容
            string partialParams = string.Empty;                // 要傳給 MXF_Partial 的參數列

            // 檢查輸入的物件是否正確
            if (!objXml.ExportXML(out partialParams, out ansXml))
            {
                string errorMsg = string.Concat("無法正確使用輸入的物件：", objXml.ErrorMessage);
                MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.WARNING, errorMsg);
                return ERRORID;
            }

            long transcodeID = ERRORID; // 預設值
            long tsmjobID = ERRORID;    // 預設值
            TRANSCODE_STATUS status = TRANSCODE_STATUS.INITIAL; // 任務初始狀態

            // 不論 FILE 或 TAPE，通通先丟到 TSMRecall 做處理
            WSTSM.ServiceTSM objTSM = new WSTSM.ServiceTSM();
            if (!fnRegisterTranscoderTsmRequest(objXml.SourceFrom, ref tsmjobID))
            {
                // Tricky：這樣寫很偷懶，因為其實還是該讓使用者註冊成功的，但會讓後面的 ReDo 非常難寫，所以 ... 就醬子吧，畢竟這邊發生的機率小到一個不行
                MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "註冊 TSMRecall 服務時發生錯誤，轉檔的請求失敗");
                return ERRORID;
            }
            else
                status = TRANSCODE_STATUS.TSMPROCESS;

            // 資料庫更新(理論上，這邊的狀態只會有 TSMPROCESS)
            // 這樣子的流程，要小心：
            // 當 APP_TSM_RECALL 先撈到 TSM 的工作，然後還比轉檔註冊的速度還快！於是會在 TrackingLog 看到一筆無效的 TSMJob，但我想這機會很低
            int tempTranscodeType;
            if (objXml.TransCodeType == "TAPE")
                tempTranscodeType = (int)TRANSCODE_TYPE.TAPE;
            else
                tempTranscodeType = (int)TRANSCODE_TYPE.FILE;
            if (!fnInsertTranscodeData(out transcodeID, tempTranscodeType, tsmjobID, ERRORID, ERRORID, objXml, ((int)status).ToString(), partialParams, ansXml))
            {
                transcodeID = ERRORID;
                MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "加入TRASNCODE資料表(SP_I_TBTRANSCODE)時發生錯誤");
            }

            // 如果是影帶擷取，就把 VTR 鎖起來
            if (objXml.TransCodeType == "TAPE")
                if (!fnLockVTR(ansXml))
                    MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "VTR 上鎖失敗，但轉檔作業持續進行，XML = " + ansXml);

            return transcodeID;
        }

        /// <summary>
        /// 讓指定的Transcode任務重新啟動
        /// </summary>
        /// <param name="transcodeID"></param>
        /// <returns></returns>
        [WebMethod]
        public bool RedoTranscode(long transcodeID)
        {
            string fsPROGRAM = MAM_PTS_DLL.ConvertFormat.Combine_TraceLogFSProgram(SYSCODE, System.Reflection.MethodBase.GetCurrentMethod().Name);

            TRANSCODE_TYPE transcodeType = TRANSCODE_TYPE.NOT_AVAILABLE;
            string origTsmJobID = string.Empty;
            string origAnsJobID = string.Empty;
            string origParJobID = string.Empty;
            string ansXml = string.Empty;       // 不會變，所以不用分orig或new

            MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "START RedoTranscode, ID = " + transcodeID);

            // 讀出原有的 TRANSCODE 任務資訊
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FNTRANSCODE_ID", transcodeID.ToString());
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBTRANSCODE_INFO_FNTRANSCODE_ID", source, out resultData) || resultData.Count == 0)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.WARNING, "查詢不到轉檔任務的資訊：" + transcodeID);
                return false;
            }

            // 解析原有的任務資訊
            transcodeType = (TRANSCODE_TYPE)Int32.Parse(resultData[0]["FNTYPE"]);
            origTsmJobID = resultData[0]["FNTSMDISPATCH_ID"];
            origParJobID = resultData[0]["FNPARTIAL_ID"];
            origAnsJobID = resultData[0]["FNANYSTREAM_ID"];
            ansXml = resultData[0]["FSANYSTREAM_XML"];

            // 記錄一下
            string logMsg = string.Format("轉檔任務{0}重置，原 SubID ={1}/{2}/{3}", transcodeID, origTsmJobID, origParJobID, origAnsJobID);
            MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, logMsg);

            // 配合新修改的 Transcode 流程，每個 Transcode Job 內必含一個 TSMRecall JobID，所以直接 Retry TSMRecallJob 就可以把後面的整串流程都重置了 :p
            WSTSM.ServiceTSM objTSM = new WSTSM.ServiceTSM();
            if (!objTSM.RetryRecallService(Int64.Parse(origTsmJobID)))
            {
                MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "重置 TSMRecallJob 失敗，ID = " + origTsmJobID);
                return false;
            }

            // 如果是影帶擷取，就把 VTR 鎖起來
            if (transcodeType == TRANSCODE_TYPE.TAPE)
                if (!fnLockVTR(ansXml))
                    MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "VTR 上鎖失敗，XML = " + ansXml);

            // 資料庫更新
            if (!UpdateTranscodeStatus(transcodeID, ERRORID, ERRORID, ERRORID, TRANSCODE_STATUS.TSMPROCESS))
            {
                MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "更新資料庫時發生錯誤");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 查詢轉檔任務的「轉檔狀態」，如果在 TSM 處理中時，進度會是 0
        /// </summary>
        /// <param name="trnascodeID"></param>
        /// <returns></returns>
        [WebMethod]
        public ServiceAnystream.QueryAnsJobProgress QueryTranscodeStatus(long transcodeID)
        {
            // 預設傳入的值
            ServiceAnystream.QueryAnsJobProgress returnData = new ServiceAnystream.QueryAnsJobProgress();
            returnData.ansJobID = -1;
            returnData.isERROR = true;
            returnData.progress = "0";

            // 讀取資料庫，取回該任務的 transcodeID 資訊
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FNTRANSCODE_ID", transcodeID.ToString());
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBTRANSCODE_INFO_FNTRANSCODE_ID", source, out resultData) || resultData.Count == 0)
                return returnData;

            // 檢查回傳的 ansID 是否可以轉為 int
            int ansID;
            if (!Int32.TryParse(resultData[0]["FNANYSTREAM_ID"], out ansID))
                return returnData;

            // 如果還沒開始進行轉檔作業時，就回傳 0%
            if (ansID == -1)
            {
                returnData.isERROR = false;
                return returnData;
            }

            // 如果已經進行轉檔作業時，就查詢 Anystream
            ServiceAnystream objANS = new ServiceAnystream();
            return objANS.DoIngestQueryProcess(ansID);
        }

        /// <summary>
        /// 查詢區間內的轉檔進度(多筆)
        /// </summary>
        /// <param name="dateStart"></param>
        /// <param name="dateEnd"></param>
        /// <returns></returns>
        [WebMethod]
        public List<ClassTranscodeJob> GetTranscodeJobsInformation(string dateStart, string dateEnd)
        {
            // 查詢資料表
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("TIME_START", dateStart);
            sqlParameters.Add("TIME_END", dateEnd);
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBTRANSCODE_JOBS", sqlParameters, out resultData) || resultData.Count == 0)
                return new List<ClassTranscodeJob>();

            // 解析內容
            string prevTranscodeID = string.Empty;
            List<ClassTranscodeJob> result = new List<ClassTranscodeJob>();
            for (int i = 0; i < resultData.Count; i++)
            {
                // 如果與前一筆加入的 TranscodeID 相同的話，就不重複加
                if (prevTranscodeID == resultData[i]["FNTRANSCODE_ID"])
                    continue;
                else
                    prevTranscodeID = resultData[i]["FNTRANSCODE_ID"];

                // 加入新的項目
                ClassTranscodeJob obj = new ClassTranscodeJob();
                obj.TranscodeJobID = resultData[i]["FNTRANSCODE_ID"];
                obj.TsmJobID = resultData[i]["FNTSMDISPATCH_ID"];
                obj.PartialJobID = resultData[i]["FNPARTIAL_ID"];
                obj.AnsJobID = resultData[i]["FNANYSTREAM_ID"];
                obj.AnsJobPriority = resultData[i]["FNANYSTREAM_PRIORITY"];
                obj.AnsXml = resultData[i]["FSANYSTREAM_XML"];
                obj.AnsErrorMessage = resultData[i]["FSCONTENT"];
                obj.dbStatus = resultData[i]["FNPROCESS_STATUS"];
                obj.RequestTime = resultData[i]["FDSTART_TIME"];
                obj.Note = resultData[i]["FSNOTE"];
                obj.ParseTranscodeJobStatus();
                result.Add(obj);
            }
            return result;
        }

        /// <summary>
        /// 查詢單筆的轉檔進度，如果找不到相關資料就會回傳null
        /// </summary>
        /// <param name="transcodeID">要查詢的TranscodeID</param>
        /// <returns></returns>
        [WebMethod]
        public ClassTranscodeJob GetOneTranscodeJobInformation(string transcodeID)
        {
            string fsPROGRAM = MAM_PTS_DLL.ConvertFormat.Combine_TraceLogFSProgram(SYSCODE, System.Reflection.MethodBase.GetCurrentMethod().Name);

            long tmpJobID;
            if (!Int64.TryParse(transcodeID, out tmpJobID) || tmpJobID < 1)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, string.Concat("無效的查詢參數：", transcodeID));
                return null;
            }

            // 查詢資料表
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FNTRANSCODE_ID", transcodeID);
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBTRANSCODE_ONEJOB", sqlParameters, out resultData) || resultData.Count == 0)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, string.Concat("SP_Q_TBTRANSCODE_ONEJOB Error：", transcodeID));
                return null;
            }

            // 解析內容，只取第一筆
            ClassTranscodeJob obj = new ClassTranscodeJob();
            obj.TranscodeJobID = transcodeID;
            obj.TsmJobID = resultData[0]["FNTSMDISPATCH_ID"];
            obj.PartialJobID = resultData[0]["FNPARTIAL_ID"];
            obj.AnsJobID = resultData[0]["FNANYSTREAM_ID"];
            obj.AnsJobPriority = resultData[0]["FNANYSTREAM_PRIORITY"];
            obj.AnsErrorMessage = resultData[0]["FSCONTENT"];
            obj.dbStatus = resultData[0]["FNPROCESS_STATUS"];
            obj.RequestTime = resultData[0]["FDSTART_TIME"];
            obj.Note = resultData[0]["FSNOTE"];
            obj.AnsXml = resultData[0]["FSANYSTREAM_XML"];
            obj.ParseTranscodeJobStatus();
            return obj;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transcodeID"></param>
        /// <param name="jobPriority"></param>
        /// <returns></returns>
        [WebMethod]
        public bool ChangeTranscodeJobPriority(long transcodeID, int jobPriority)
        {
            // 檢查 transcodeID
            if (transcodeID < 0)
            {
                string errMsg = string.Format("輸入的格式錯誤，transcodeID = {0}", transcodeID);
                MAM_PTS_DLL.Log.AppendTrackingLog("WSASC/WSTranscode/ChangeTranscodeJobPriority", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, errMsg);
                return false;
            }

            // 檢查 jobPriority
            if (jobPriority < 1 || jobPriority > 100)
            {
                string errMsg = string.Format("輸入的格式錯誤，jobPriority = {0}", jobPriority);
                MAM_PTS_DLL.Log.AppendTrackingLog("WSASC/WSTranscode/ChangeTranscodeJobPriority", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, errMsg);
                return false;
            }

            // 由 transcodeJobID 取得所屬的 ansJobID
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FNTRANSCODE_ID", transcodeID.ToString());
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBTRANSCODE_INFO_FNTRANSCODE_ID", sqlParameters, out resultData) || resultData.Count == 0)
            {
                string errMsg = string.Format("執行 SP_Q_TBTRANSCODE_INFO_FNTRANSCODE_ID 發生錯誤回傳，FNTRANSCODE_ID = {0}", transcodeID);
                MAM_PTS_DLL.Log.AppendTrackingLog("WSASC/WSTranscode/ChangeTranscodeJobPriority", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return false;
            }

            int ansJobID;
            if (!Int32.TryParse(resultData[0]["FNANYSTREAM_ID"], out ansJobID))
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSASC/WSTranscode/ChangeTranscodeJobPriority", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "無法正常解析資料庫的 FNANYSTREAM_ID 回傳");
                return false;
            }

            // 如果 ansJobID 大於 0 (已經有轉檔中的)，就呼叫 ANS 功能，修改優先權
            if (ansJobID > 0)
            {
                // 如果 ansJobID 正在運作，但更新發生常敗，還是不會回寫資料庫
                ServiceAnystream objAns = new ServiceAnystream();
                if (!objAns.DoChangePriority(ansJobID, jobPriority))
                {
                    string errMsg = string.Format("呼叫 ANYSTREAM 執行優先權變更時發生錯誤回傳，ansJobID = {0}, jobPriority = {1}", ansJobID, jobPriority);
                    MAM_PTS_DLL.Log.AppendTrackingLog("WSASC/WSTranscode/ChangeTranscodeJobPriority", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                    return false;
                }
            }
            else
            {
                // ansJobID 小於 0，代表還沒開始轉檔，先寫入資料庫也是 ok 的
            }

            // 利用 transcode 去更新 Update Store Procedure , 更新 transcodeID -> Priority
            sqlParameters.Clear();
            sqlParameters.Add("FNTRANSCODE_ID", transcodeID.ToString());
            sqlParameters.Add("FNANYSTREAM_PRIORITY", jobPriority.ToString());
            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBTRANSCODE_PRIORITY", sqlParameters, "system");
        }

        /// <summary>
        /// DoTranscode執行結束 (成功+失敗) 後的HTTP POST回傳函式(內容會是XML，由這個函數組合)
        /// </summary>
        /// <param name="notifyAddr">要通知的位址</param>
        /// <param name="transcodeID">任務ID</param>
        /// <param name="jobStatus">任務結束狀態</param>
        /// <param name="jobMessage">任務訊息(如果是完成，就會是空字串)</param>
        /// <returns></returns>
        public bool SendTranscodeJobHttpMessage(string notifyAddr, string transcodeID, string jobStatus, string jobMessage)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<TSM_TRANSCODE>");
            sb.AppendLine(string.Concat("<PROCESS_ID>", transcodeID ,"</PROCESS_ID>"));
            sb.AppendLine(string.Concat("<PROCESS_RESULT>", jobStatus, "</PROCESS_RESULT>"));
            sb.AppendLine(string.Concat("<PROCESS_MESSAGE>", jobMessage, "</PROCESS_MESSAGE>"));
            sb.AppendLine("</TSM_TRANSCODE>");
            return MAM_PTS_DLL.Protocol.SendHttpPostMessage(notifyAddr, sb.ToString());
        }

        /// <summary>
        /// 解析由 SendTranscodeJobHttpMessage 組合出來的 HTTP POST 訊息，但如果訊息格式有誤會回傳 false
        /// </summary>
        /// <returns></returns>
        public bool ParseTranscodeJobHttpMessage(Stream inputStream, out long jobID, out string jobStatus)
        {
            string fsPROGRAM = MAM_PTS_DLL.ConvertFormat.Combine_TraceLogFSProgram("TransCode", System.Reflection.MethodBase.GetCurrentMethod().Name);

            jobID = -1;
            jobStatus = string.Empty;

            // 讀入資料
            string inputData = string.Empty;
            try
            {
                StreamReader sr = new StreamReader(inputStream);
                inputData = sr.ReadToEnd();
                sr.Close();
            }
            catch (Exception ex)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.FATALERROR, string.Concat("讀入POST資料流時發生錯誤：", ex.ToString()));
                return false;
            }

            // 記錄
            //string logMsg = string.Concat("由Handler_AnsNotifyReceiver.ashx傳來的內容：", inputData);
            //MAM_PTS_DLL.Log.AppendTrackingLog("WSASC/ParseTranscodeJobHttpMessage", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, logMsg);

            // 解析回傳的XML內容
            XmlDocument xmlDoc = new XmlDocument();
            XmlNode objXmlNode;
            try
            {
                xmlDoc.LoadXml(inputData);
            }
            catch
            {
                MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Concat("Receive unexpected http-post content from TranscodeService: ", inputData));
                return false;
            }

            // 檢查XML中的節點
            objXmlNode = xmlDoc.SelectSingleNode("/TSM_TRANSCODE/PROCESS_RESULT");
            if (objXmlNode == null)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.DEBUG, string.Concat("Can't fine node /job-status/status in http-post content: ", inputData));
                return false;
            }
            jobStatus = objXmlNode.InnerText;

            objXmlNode = xmlDoc.SelectSingleNode("/TSM_TRANSCODE/PROCESS_ID");
            if (objXmlNode == null)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.DEBUG, string.Concat("Can't fine node /job-status/id in http-post content: ", inputData));
                return false;
            }

            MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "Parse: transcodeID = " + objXmlNode.InnerText + " , jobStatus = " + jobStatus);
            return long.TryParse(objXmlNode.InnerText, out jobID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transcodeID"></param>
        /// <param name="actionDesc"></param>
        /// <param name="caller"></param>
        /// <returns></returns>
        public bool InsertTranscodeMgmtLogData(long transcodeID, string actionDesc, string caller)
        {
            Dictionary<string, string> sqlParamenters = new Dictionary<string, string>();
            sqlParamenters.Add("FNTRANSCODE_ID", transcodeID.ToString());
            sqlParamenters.Add("FSACTION", actionDesc);
            sqlParamenters.Add("FSCALLER", caller);
            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TRANSCODE_MGMT_LOG", sqlParamenters, "system");
        }

        /// <summary>
        /// 更新 Transcode 的內容 (IDs, status)
        /// </summary>
        /// <param name="tsmID"></param>
        /// <param name="ansID"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public bool UpdateTranscodeStatus(long transcodeID, long tsmID, long parID, long ansID, TRANSCODE_STATUS status)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FNTRANSCODE_ID", transcodeID.ToString());
            sqlParameters.Add("FNTSMDISPATCH_ID", tsmID.ToString());
            sqlParameters.Add("FNPARTIAL_ID", parID.ToString());
            sqlParameters.Add("FNANYSTREAM_ID", ansID.ToString());
            sqlParameters.Add("FNPROCESS_STATUS", ((int)status).ToString());
            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBTRANSCODE_JOBID", sqlParameters, "system");
        }

        /// <summary>
        /// 從轉檔的XML中，找出要執行影帶頡取的VTR編號
        /// </summary>
        /// <param name="xmlData">Anystream的轉檔XML</param>
        /// <param name="VTRName"></param>
        /// <returns></returns>
        public bool FindTranscodeVTR(string xmlData, out string VTRName)
        {
            VTRName = string.Empty;

            // 將送給 Anystream 的 Script 載入XML 物件，理論上這邊是不會錯的
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                xmlDoc.LoadXml(xmlData);
            }
            catch (Exception ex)
            {
                // 理論上這邊是不會發生的!
                string errMsg = string.Concat("Load Ans Xml Error: ", ex.ToString(), Environment.NewLine, xmlData);
                MAM_PTS_DLL.Log.AppendTrackingLog("WSASC/WSTranscode/fnFindAndLockVTR", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return false;
            }

            // 檢查 XPath，看是否有使用 VTR 做轉檔
            XmlNode objNode = xmlDoc.SelectSingleNode("/planner-submit/capture-station");
            if (objNode != null)
            {
                VTRName = objNode.InnerText;
                // 理論上不應該會是空字串
                if (string.IsNullOrEmpty(VTRName))
                    return false;
            }
            else
            {
                // 數位轉檔不用上鎖、或是查到VTR名稱(非空字串)的也都會在這裡
            }

            return true;
        }



        // 將轉檔資訊寫入資料庫
        private bool fnInsertTranscodeData(out long transcodeID, int transcodeType, long tsmJobID, long partialJobID, long ansJobID, DoIngestXml objDoIngestXml, string jobStatus, string partialString, string ansXml)
        {
            transcodeID = ERRORID;

            // 2012-07-26：依照不同的使用方式，去設定預設的轉檔優先權值
            int priority;
            if (objDoIngestXml.Note.StartsWith("MSS-"))
            {
                // 主控格式
                priority = 4;
            }
            else if (objDoIngestXml.Note.StartsWith("節目名稱") || objDoIngestXml.Note.StartsWith("短帶名稱"))
            {
                // 送帶轉檔
                priority = 2;
            }
            else if (objDoIngestXml.Note.StartsWith("MAM-調用"))
            {
                // 借調
                priority = ANYSTREAM_DEFAULT_PRIORITY;
            }
            else
            {
                // 其他
                priority = ANYSTREAM_DEFAULT_PRIORITY;
            }

            // 準備資料庫參數
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FNTYPE", transcodeType.ToString());
            sqlParameters.Add("FNTSMDISPATCH_ID", tsmJobID.ToString());
            sqlParameters.Add("FNPARTIAL_ID", partialJobID.ToString());
            sqlParameters.Add("FNANYSTREAM_ID", ansJobID.ToString());
            sqlParameters.Add("FNANYSTREAM_PRIORITY", priority.ToString());
            sqlParameters.Add("FSOUTPUT_RESOLUTION", objDoIngestXml.DestinationFormat);
            sqlParameters.Add("FCFULL_RETRIEVE", objDoIngestXml.IsFullTranscode);
            if (objDoIngestXml.NeedPartialProcess)
                sqlParameters.Add("FCNEED_EXECUTE_PARTIAL", "Y");
            else
                sqlParameters.Add("FCNEED_EXECUTE_PARTIAL", "N");
            sqlParameters.Add("FCNATIVE_FORMAT", objDoIngestXml.IsNativeFormat);
            sqlParameters.Add("FNPROCESS_STATUS", jobStatus);
            sqlParameters.Add("FSPARTIAL_PARAMS", partialString);
            sqlParameters.Add("FSANYSTREAM_XML", ansXml);
            sqlParameters.Add("FSNOTIFY_ADDRESS", objDoIngestXml.PostbackAddress);
            sqlParameters.Add("FSNOTE", objDoIngestXml.Note);

            // 寫入資料庫，並取得回傳
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_I_TBTRANSCODE", sqlParameters, out resultData) || resultData.Count == 0)
                return false;

            // 解析JobID
            transcodeID = Int64.Parse(resultData[0]["TRANSCODEID"]);
            return true;
        }

        // 將 VTR 上鎖
        private bool fnLockVTR(string ansXml)
        {
            string fsPROGRAM = MAM_PTS_DLL.ConvertFormat.Combine_TraceLogFSProgram(SYSCODE, System.Reflection.MethodBase.GetCurrentMethod().Name);

            // 針對使用的 VTR (如果有用到的話) 做上鎖的動作，如果上鎖動作發生錯誤 (DBError) 也不去影響
            string VTRName = string.Empty;
            if (FindTranscodeVTR(ansXml, out VTRName))
            {
                if (!string.IsNullOrEmpty(VTRName))
                    if (!LockVTR(VTRName))
                        MAM_PTS_DLL.Log.AppendTrackingLog("WSASC/WSTranscode/fnSendAnsIngestXml", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "Got Error when Try To Lock VTR, VTR name = " + VTRName);
            }
            else
                MAM_PTS_DLL.Log.AppendTrackingLog("WSASC/WSTranscode/fnSendAnsIngestXml", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "Got Error when Try To Find VTR, XML = " + ansXml);

            return true;
        }

        // 向 TSMRecall 的需求申請入口做註冊的動作
        private bool fnRegisterTranscoderTsmRequest(string filePath, ref long tsmjobID)
        {
            string fsPROGRAM = MAM_PTS_DLL.ConvertFormat.Combine_TraceLogFSProgram(SYSCODE, System.Reflection.MethodBase.GetCurrentMethod().Name);

            WSTSM.ServiceTSM objTSM = new WSTSM.ServiceTSM();
            try
            {
                MAM_PTS_DLL.SysConfig objSysconfig = new MAM_PTS_DLL.SysConfig();
                string postbackAddr = objSysconfig.sysConfig_Read("/ServerConfig/AshxHandler/TSM_ANS");

                if (!objTSM.RegisterRecallServiceBySmbPath(filePath, postbackAddr, out tsmjobID))
                    throw new Exception("註冊 TSMJob 的動作發生錯誤");
            }
            catch (Exception ex)
            {
                string errorMsg = string.Concat("執行 fnRegisterTranscoderTsmRequest() 時發生錯誤：", ex.ToString(), Environment.NewLine, "傳送內容：", filePath);
                MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errorMsg);
                return false;
            }

            return true;
        }

        #region TBLOG_VIDEO_KEYFRAME
        /// <summary>
        /// 依照起迄日期查詢TBLOG_WORKER資料(用預存取回資料)
        /// </summary>
        [WebMethod]
        public List<clLOG_WORKER> GetTBLOG_WORKER_ByDates(DateTime m_SDate, DateTime m_EDate, String m_WORKER_NAME, String m_RESULT)
        {
            List<clLOG_WORKER> lstLOG_WORKER = new List<clLOG_WORKER>();
            String sqlConnStr = MAM_PTS_DLL.DbAccess.sqlConnStr;//ConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);

            try
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_Q_TBLOG_WORKER_BYDATES", connection);
                DataTable dt = new DataTable();

                da.SelectCommand.Parameters.Add(new SqlParameter("@SDate", m_SDate));
                da.SelectCommand.Parameters.Add(new SqlParameter("@EDate", m_EDate));
                da.SelectCommand.Parameters.Add(new SqlParameter("@WORKER_NAME", m_WORKER_NAME));
                da.SelectCommand.Parameters.Add(new SqlParameter("@RESULT", m_RESULT));

                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                da.Fill(dt);

                foreach (DataRow dr in dt.Rows)
                {
                    String _ETime = "", _Proc_Time = "";
                    int _Time = 0, _dd = 0, _HH = 0, _mm = 0;

                    if (DateTime.Parse(dr["FDETIME"].ToString()).ToString("yyyy-MM-dd") != DateTime.Parse("1900-01-01").ToString("yyyy-MM-dd"))
                    {
                        _ETime = DateTime.Parse(dr["FDETIME"].ToString()).ToString("yyyy-MM-dd HH:mm");
                    }


                    if ((dr["Proc_Time"].ToString() == "") || (int.TryParse(dr["Proc_Time"].ToString(), out _Time) == false))
                    {
                        _Proc_Time = "無法計算";
                    }
                    else if (_Time < 0)
                    {
                        _Proc_Time = "結束時間錯誤";
                    }
                    else if (dr["Proc_Time"].ToString() != "")
                    {
                        _Time = int.Parse(dr["Proc_Time"].ToString());
                        _HH = _Time / 60;
                        _mm = _Time % 60;

                        if (_HH == 0)
                        {
                            _Proc_Time = _mm.ToString() + "分";
                        }
                        else
                        {
                            _dd = _HH / 24;
                            _HH = _HH % 24;

                            if (_dd == 0)
                            {
                                _Proc_Time = _HH.ToString() + "時" + _mm.ToString() + "分";
                            }
                            else
                            {
                                _Proc_Time = _dd.ToString() + "日" + _HH.ToString() + "時" + _mm.ToString() + "分";
                            }                           
                        } 
                    }

                    lstLOG_WORKER.Add
                    (
                        new clLOG_WORKER
                        {
                            FSGUID = dr["FSGUID"].ToString(),
                            FSWORKER_NAME = dr["FSWORKER_NAME"].ToString(),
                            FDSTIME = DateTime.Parse(dr["FDSTIME"].ToString()).ToString("yyyy-MM-dd HH:mm"),
                            FDETIME = _ETime,
                            FSRESULT = dr["FSRESULT"].ToString(),
                            FSNOTE = dr["FSNOTE"].ToString(),
                            FSCREATED_BY = dr["FSCREATED_BY"].ToString(),
                            FDCREATED_DATE = DateTime.Parse(dr["FDCREATED_DATE"].ToString()),
                            FSUPDATED_BY = dr["FSUPDATED_BY"].ToString(),
                            FDUPDATED_DATE = DateTime.Parse(dr["FDUPDATED_DATE"].ToString()),
                            Proc_Time = _Proc_Time,
                            File_ID = dr["File_ID"].ToString()
                        }
                    );
                }
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }

            return lstLOG_WORKER;
        }

        [WebMethod]
        public clLOG_WORKER GetTBLOG_WORKER_ByFSGUID(String m_FSGUID)
        {
            clLOG_WORKER  clLOG_WORKER = new clLOG_WORKER();
            String sqlConnStr = MAM_PTS_DLL.DbAccess.sqlConnStr;//ConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr);

            try
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_Q_TBLOG_WORKER_BYFSGUID", connection);
                DataTable dt = new DataTable();

                da.SelectCommand.Parameters.Add(new SqlParameter("@FSGUID", m_FSGUID)); 

                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                da.Fill(dt);

                foreach (DataRow dr in dt.Rows)
                {
                    String _ETime = "", _Proc_Time = "";
                    int _Time = 0, _dd = 0, _HH = 0, _mm = 0;

                    if (DateTime.Parse(dr["FDETIME"].ToString()).ToString("yyyy-MM-dd") != DateTime.Parse("1900-01-01").ToString("yyyy-MM-dd"))
                    {
                        _ETime = DateTime.Parse(dr["FDETIME"].ToString()).ToString("yyyy-MM-dd HH:mm");
                    }

                    if (dr["Proc_Time"].ToString() != "")
                    {
                        _Time = int.Parse(dr["Proc_Time"].ToString());
                        _HH = _Time / 60;
                        _mm = _Time % 60;

                        if (_HH == 0)
                        {
                            _Proc_Time = _mm.ToString() + "分";
                        }
                        else
                        {
                            _dd = _HH / 24;
                            _HH = _HH % 24;

                            if (_dd == 0)
                            {
                                _Proc_Time = _HH.ToString() + "時" + _mm.ToString() + "分";
                            }
                            else
                            {
                                _Proc_Time = _dd.ToString() + "日" + _HH.ToString() + "時" + _mm.ToString() + "分";
                            }    
                        }
                    }

                    clLOG_WORKER =
                        new clLOG_WORKER
                        {
                            FSGUID = dr["FSGUID"].ToString(),
                            FSWORKER_NAME = dr["FSWORKER_NAME"].ToString(),
                            FDSTIME = DateTime.Parse(dr["FDSTIME"].ToString()).ToString("yyyy-MM-dd HH:mm"),
                            FDETIME = _ETime,
                            FSRESULT = dr["FSRESULT"].ToString(),
                            FSNOTE = dr["FSNOTE"].ToString(),
                            FSCREATED_BY = dr["FSCREATED_BY"].ToString(),
                            FDCREATED_DATE = DateTime.Parse(dr["FDCREATED_DATE"].ToString()),
                            FSUPDATED_BY = dr["FSUPDATED_BY"].ToString(),
                            FDUPDATED_DATE = DateTime.Parse(dr["FDUPDATED_DATE"].ToString()),
                            Proc_Time = _Proc_Time,
                            File_ID = dr["File_ID"].ToString()
                        };
                }
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            }

            return clLOG_WORKER;
        }

        /// <summary>
        /// 依照輸入值新增News資料(用預存新增資料的範例)
        /// </summary>
        [WebMethod]
        public int TBLOG_VIDEO_RESET(String FILE_NO, String FSGUID)
        {
            String sqlConnStr = MAM_PTS_DLL.DbAccess.sqlConnStr; //ConfigurationManager.ConnectionStrings["MAMSQLConnStr"].ConnectionString;
            SqlConnection connection = new SqlConnection(sqlConnStr); 

            try
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_U_TBLOG_VIDEO_RESET", connection);
                DataTable dt = new DataTable();

                da.SelectCommand.Parameters.Add(new SqlParameter("@FILE_NO", FILE_NO));
                da.SelectCommand.Parameters.Add(new SqlParameter("@FSGUID", FSGUID)); 

                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }

                da.Fill(dt);

                if (dt.Rows.Count == 0)
                {
                    return -1;
                }
                else
                {
                    return int.Parse(dt.Rows[0].ItemArray[0].ToString());
                }
            }
            catch (Exception ex)
            {
                return -1;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
            } 
        }
    
    }

    public class clLOG_WORKER
    {
        public String FSGUID { get; set; }
        public String FSWORKER_NAME { get; set; }
        public String FDSTIME { get; set; }
        public String FDETIME { get; set; }
        public String FSRESULT { get; set; }
        public String FSNOTE { get; set; }
        public String FSCREATED_BY { get; set; }
        public DateTime FDCREATED_DATE { get; set; }
        public String FSUPDATED_BY { get; set; }
        public DateTime FDUPDATED_DATE { get; set; }

        public String Proc_Time { get; set; }
        public String File_ID { get; set; }
    }
#endregion
}
