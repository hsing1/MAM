﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace PTS_MAM3.Web.WS.WSASC
{
    /// <summary>
    /// WSTranscode_VTR 的摘要描述
    /// </summary>
    public partial class ServiceTranscode : System.Web.Services.WebService
    {
        /// <summary>
        /// 新增轉檔中心的 VTR
        /// </summary>
        /// <param name="VTRName">VTR的編號</param>
        /// <param name="isEnable">是否啟用該台VTR：Y或N</param>
        /// <param name="isLock">是否鎖定該台VTR：Y或N</param>
        /// <returns></returns>
        [WebMethod]
        public bool AddVTR(string VTRName, char isEnable, char isLock, string VTRComment)
        {
            // 對輸入參數做簡單的檢查
            if ((isEnable != 'Y' && isEnable != 'N') || (isLock != 'Y' && isLock != 'N'))
                return false;

            // 寫入資料表
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSVTR_NAME", VTRName);
            sqlParameters.Add("FCENABLE", isEnable.ToString());
            sqlParameters.Add("FCLOCK", isLock.ToString());
            sqlParameters.Add("FSNOTE", VTRComment);
            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBVTR", sqlParameters, "system");
        }

        /// <summary>
        /// 針對單一 VTR 做上鎖的動作 (可顯示，但在解鎖前無法再次使用)
        /// </summary>
        /// <param name="VTRName"></param>
        /// <returns></returns>
        public bool LockVTR(string VTRName)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSVTR_NAME", VTRName);
            sqlParameters.Add("LOCK", "Y");
            // 
            if (!MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBVTR_LOCK", sqlParameters, "system"))
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSASC/WSTranscode_VTR/LockVTR", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "VTR Name = " + VTRName);
                return false;
            }
            return true;;
        }

        /// <summary>
        /// 針對單一 VTR 做解的動作 (與 LockVTR 是相反操作)
        /// </summary>
        /// <param name="VTRName"></param>
        /// <returns></returns>
        public bool UnlockVTR(string VTRName)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSVTR_NAME", VTRName);
            sqlParameters.Add("LOCK", "N");
            // 
            if (!MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBVTR_LOCK", sqlParameters, "system"))
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSASC/WSTranscode_VTR/UnlockVTR", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "VTR Name = " + VTRName);
                return false;
            }
            return true; ;
        }

        /// <summary>
        /// 變更 VTR 的狀態
        /// </summary>
        /// <param name="VTRName">VTR的編號</param>
        /// <param name="isEnable">是否啟用該台VTR：Y或N</param>
        /// <param name="isLock">是否鎖定該台VTR：Y或N</param>
        /// <returns></returns>
        [WebMethod]
        public bool ChangeVTRStatus(string VTRName, char isEnable, char isLock, string VTRComment)
        {
            // 對輸入參數做簡單的檢查
            if ((isEnable != 'Y' && isEnable != 'N') || (isLock != 'Y' && isLock != 'N'))
                return false;

            // 寫入資料表
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSVTR_NAME", VTRName);
            sqlParameters.Add("FCENABLE", isEnable.ToString());
            sqlParameters.Add("FCLOCK", isLock.ToString());
            sqlParameters.Add("FSNOTE", VTRComment);
            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBVTR_STATUS", sqlParameters, "system");
        }

        /// <summary>
        /// 刪除 VTR，不過這動作不太好，不建議使用
        /// </summary>
        /// <param name="VTRName"></param>
        /// <returns></returns>
        [WebMethod]
        public bool DeleteVTR(string VTRName)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSVTR_NAME", VTRName);
            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_TBVTR", sqlParameters, "system");
        }

        /// <summary>
        /// 取得目前資料表中為 ENABLE 且未被上鎖的 VTR 機器編號清單
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public List<ClassVTR> GetAvailableVTRList()
        {
            List<ClassVTR> allVTR = GetAllVTRList();

            if (allVTR == null)
                return new List<ClassVTR>();

            List<ClassVTR> result = new List<ClassVTR>();
            foreach (ClassVTR obj in allVTR)
                if (obj.isEnable == "Y" && obj.isLock == "N")
                    result.Add(obj);

            return result;
        }

        /// <summary>
        /// 取得資料表中所有的 VTR 列表及其詳細資訊
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public List<ClassVTR> GetAllVTRList()
        {
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBVTR_ALL", new Dictionary<string, string>(), out resultData) || resultData.Count == 0)
                return new List<ClassVTR>();

            List<ClassVTR> result = new List<ClassVTR>();
            for (int i = 0; i < resultData.Count; i++)
            {
                ClassVTR obj = new ClassVTR();
                obj.Name = resultData[i]["FSVTR_NAME"];
                obj.isEnable = resultData[i]["FCENABLE"];
                obj.isLock = resultData[i]["FCLOCK"];
                obj.UpdateTime = resultData[i]["FDUPDATE_TIME"];
                obj.Comment = resultData[i]["FSNOTE"];
                result.Add(obj);
            }

            return result;
        }
    }
}
