﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Xml;

namespace PTS_MAM3.Web.WS.WSASC
{
    /// <summary>
    ///Handler_TsmNotifyReceiver 的摘要描述
    /// </summary>
    public class TsmNotifyReceiver : IHttpHandler
    {
        const string SYSCODE = "ASC";

        /// <summary>
        /// 處理傳入的HTTP Request
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            string fsPROGRAM = MAM_PTS_DLL.ConvertFormat.Combine_TraceLogFSProgram(SYSCODE, System.Reflection.MethodBase.GetCurrentMethod().Name);

            HttpRequest req = context.Request;

            ServiceTranscode objTranscodeService = new ServiceTranscode();
            long transcodeJobID;
            ServiceTranscode.TRANSCODE_STATUS transcodeJobStatus = ServiceTranscode.TRANSCODE_STATUS.PARPROCESS;

            long tsmJobID;
            WSTSM.TSMRecallService.TsmRecallJobResult tsmJobStatus;
            string tsmJobMessage;

            // 解析從 TSM Dispatch 傳回來的內容
            // 但如果連 Parse Http Post 都出問題了，所以沒辦法去解析內容，更別提對「目標」發訊息了

            // 解析 HTTP POST 的內容
            WSTSM.TSMRecallService objTSMService = new WSTSM.TSMRecallService();
            bool flagParse = objTSMService.ParseTsmPostMessage(req.InputStream, out tsmJobID, out tsmJobStatus, out tsmJobMessage);
            objTSMService.Dispose();

            // William: 2012-04-16
            context.Response.Flush();
            context.Response.Close();

            if (!flagParse)
                return;     // 已經由 ParseTsmPostMessage 做好記錄了，不需要重複

            bool needPartial, isFullRetrieve;
            string partialParams, notifyAddress;
            // 連接資料庫，用 tsmJobID 做 PK，找出該筆轉檔任務的相關資訊 (查詢要傳給ANS的內容)
            // 同上，如果抓不出該任務的內容，也沒辦法發訊息
            if (!fnGetTranscodeInformation(tsmJobID, out transcodeJobID, out needPartial, out isFullRetrieve, out partialParams, out notifyAddress))
                return;     // 已經由 fnGetTranscodeInformation 做好記錄了，不需要重複

            // 如果 TSMRecall 發生錯誤時，就回存結果狀態，並中止程序
            if (tsmJobStatus == WSTSM.TSMRecallService.TsmRecallJobResult.FILE_NOT_EXIST)
            {
                transcodeJobStatus = ServiceTranscode.TRANSCODE_STATUS.SOURCE_NOT_EXIST;
                objTranscodeService.UpdateTranscodeStatus(transcodeJobID, tsmJobID, -1, -1, transcodeJobStatus);
                objTranscodeService.SendTranscodeJobHttpMessage(notifyAddress, transcodeJobID.ToString(), transcodeJobStatus.ToString(), "Error: SOURCE_NOT_EXIST");
                return;
            }
            else if (tsmJobStatus == WSTSM.TSMRecallService.TsmRecallJobResult.SYSTEM_ERROR)
            {
                transcodeJobStatus = ServiceTranscode.TRANSCODE_STATUS.SYSERROR;
                objTranscodeService.UpdateTranscodeStatus(transcodeJobID, tsmJobID, -1, -1, transcodeJobStatus);
                objTranscodeService.SendTranscodeJobHttpMessage(notifyAddress, transcodeJobID.ToString(), transcodeJobStatus.ToString(), "Error: SYSERROR");
                return;
            }

            // 解析 Partial 要用的參數
            string paramSource = string.Empty;
            string paramTarget = string.Empty;
            string paramInPoint = string.Empty;
            string paramOutPoint = string.Empty;
            if (needPartial)
            {
                string[] tmpPartialParams = partialParams.Split('?');
                if (tmpPartialParams.Length != 4)
                {
                    MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "資料庫中的 Partial 參數錯誤：" + partialParams);

                    transcodeJobStatus = ServiceTranscode.TRANSCODE_STATUS.SYSERROR;
                    objTranscodeService.UpdateTranscodeStatus(transcodeJobID, tsmJobID, -1, -1, transcodeJobStatus);
                    objTranscodeService.SendTranscodeJobHttpMessage(notifyAddress, transcodeJobID.ToString(), transcodeJobStatus.ToString(), "Error: SYSERROR");

                    return;
                }

                paramSource = tmpPartialParams[0];
                paramTarget = tmpPartialParams[1];

                if (!isFullRetrieve)
                {
                    paramInPoint = tmpPartialParams[2];
                    paramOutPoint = tmpPartialParams[3];
                }
            }

            // 註冊 MXF_Partial 的任務 (不論是否要真正的執行 Partial，就都給他 ... 註冊進去就對了，只是註冊的資訊略有不同 :p)
            WSASC_PartialService objWSASC_PartialService = new WSASC_PartialService();
            long partialJobID = objWSASC_PartialService.RegisterPartialService(paramSource, paramTarget, paramInPoint, paramOutPoint);
            if (partialJobID < 0)
            {
                string errMsg = string.Format("註冊 Partial 服務時發生錯誤：{0}/{1}/{2}/{3}", paramSource, paramTarget, paramInPoint, paramOutPoint);
                MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);

                transcodeJobStatus = ServiceTranscode.TRANSCODE_STATUS.SYSERROR;
                objTranscodeService.UpdateTranscodeStatus(transcodeJobID, tsmJobID, -1, -1, transcodeJobStatus);
                objTranscodeService.SendTranscodeJobHttpMessage(notifyAddress, transcodeJobID.ToString(), transcodeJobStatus.ToString(), "Error: SYSERROR");

                return;
            }

            // 最後的最後，就將結果回寫吧 :D
            // 這邊，也要考慮一下，會不會有 APP_PARTIAL 的動作快於 DB_UPDATE 的動作，有的話也太可怕了
            objTranscodeService.UpdateTranscodeStatus(transcodeJobID, tsmJobID, partialJobID, -1, transcodeJobStatus);

            // 打完收工
        }

        // 利用 TSMRecallJobID 找出相關的轉檔資訊
        private bool fnGetTranscodeInformation(long tsmJobID, out long transcodeJobID, out bool needPartial, out bool isFullRetrieve, out string partialParams, out string notifyAddress)
        {
            transcodeJobID = -1;
            needPartial = false;
            isFullRetrieve = false;
            partialParams = string.Empty;
            notifyAddress = string.Empty;

            // SQL參數
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FNTSMDISPATCH_ID", tsmJobID.ToString());

            // 讀取資料庫回傳的內容
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBTRANSCODE_INFO_FNTSMDISPATCH_ID", source, out resultData) || resultData.Count == 0)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSASC/Handler_TsmNotifyReceiver/fnGetTranscodeInformation", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "找不到相關的轉檔資訊，tsmJobID = " + tsmJobID);
                return false;
            }

            // 理論上只會有一筆，所以只取第1筆就好
            transcodeJobID = Int64.Parse(resultData[0]["FNTRANSCODE_ID"]);
            if (resultData[0]["FCNEED_EXECUTE_PARTIAL"] == "Y")
                needPartial = true;
            if (resultData[0]["FCFULL_RETRIEVE"] == "Y")
                isFullRetrieve = true;
            partialParams = resultData[0]["FSPARTIAL_PARAMS"];
            notifyAddress = resultData[0]["FSNOTIFY_ADDRESS"];

            return true;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}