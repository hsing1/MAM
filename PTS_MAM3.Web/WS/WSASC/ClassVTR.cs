﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.WS.WSASC
{
    public class ClassVTR
    {
        // 在Anystream中定義的VTR名稱
        public string Name;
        // 是否啟用(如果不啟用的話，就沒辦法在前端頁面中被看到)
        public string isEnable;
        // 是否上鎖(上鎖的VTR沒辦法被拿來做影帶擷取)
        public string isLock;
        // 最後更新資料的時間，純參考用
        public string UpdateTime;
        // 說明欄位，由使用者自行註記其內容，例：SD、HD、Digital BetaCAM
        public string Comment;
    }
}