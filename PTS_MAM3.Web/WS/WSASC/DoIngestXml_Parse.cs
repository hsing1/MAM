﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace PTS_MAM3.Web.WS.WSASC
{
    public partial class DoIngestXml
    {
        private bool fnParse_JobIngest_TransCodeType(XmlNode node)
        {
            // 必填欄位
            if (node == null)
            {
                ErrorMessage = "Parse Xml Error: Can't find /JobIngest/TransCodeType";
                return false;
            }
            //
            TransCodeType = node.InnerText.ToUpper();
            //
            return fnValidate_TranscodeType();
        }

        private bool fnParse_JobIngest_IsPartialTranscode(XmlNode node)
        {
            // 如果沒有這個節點，預設值為'Y'
            if (node == null)
                this.IsFullTranscode = "Y";
            else
                this.IsFullTranscode = node.InnerText;

            return fnValidate_IsPartialTranscode();
        }

        private bool fnParse_JobIngest_IsNativeFormat(XmlNode node)
        {
            // 如果沒有這個節點，預設值為'N'
            if (node == null)
                this.IsNativeFormat = "N";
            else
                this.IsNativeFormat = node.InnerText;

            return fnValidate_IsNativeFormat();
        }

        private bool fnParse_JobIngest_EncoderProfliePath(XmlNode node)
        {
            // 必填欄位
            if (node == null)
            {
                ErrorMessage = "Parse Xml Error: Can't find /JobIngest/EncoderProfliePath";
                return false;
            }
            EncoderProfliePath = node.InnerText;
            //
            return fnValidate_EncoderProfilePath();
        }

        private bool fnParse_JobIngest_SourceFrom(XmlNode node)
        {
            // 必填欄位
            if (node == null)
            {
                ErrorMessage = "Parse Xml Error: Can't find /JobIngest/SourceFrom";
                return false;
            }
            SourceFrom = node.InnerText;
            // 
            return fnValidate_SourceFrom();
        }

        private bool fnParse_JobIngest_DestinationTo(XmlNode node)
        {
            // 必填欄位
            if (node == null)
            {
                ErrorMessage = "Parse Xml Error: Can't find /JobIngest/DestinationTo";
                return false;
            }
            DestinationTo = node.InnerText;
            // 
            return fnValidate_DestinationTo();
        }

        private bool fnParse_JobIngest_SourceFormat(XmlNode node)
        {
            // 如果沒有這個節點，預設值為SD
            if (node == null)
                SourceFormat = "SD";
            else
                SourceFormat = node.InnerText;

            return fnValidate_SourceFrom();
        }

        private bool fnParse_JobIngest_DestinationFormat(XmlNode node)
        {
            // 如果沒有這個節點，預設值為SD
            if (node == null)
                DestinationFormat = "SD";
            else
                DestinationFormat = node.InnerText;

            return fnValidate_DestinationFormat();
        }

        private bool fnParse_JobIngest_StartTimecode(XmlNode node)
        {
            // 必填欄位
            if (node == null)
            {
                ErrorMessage = "Parse Xml Error: Can't find /JobIngest/StartTimecode";
                return false;
            }
            StartTimecode = node.InnerText;
            // 
            return fnValidate_StartTimecode();
        }

        private bool fnParse_JobIngest_EndTimecode(XmlNode node)
        {
            // 必填欄位
            if (node == null)
            {
                ErrorMessage = "Parse Xml Error: Can't find /JobIngest/EndTimecode";
                return false;
            }
            EndTimecode = node.InnerText;
            // 
            return fnValidate_EndTimecode();
        }

        private bool fnParse_JobIngest_PostbackAddress(XmlNode node)
        {
            if (node == null)
                PostbackAddress = string.Empty;
            else
                PostbackAddress = node.InnerText;
            return fnValidate_PostbackAddress();
        }

        private bool fnParse_JobIngest_CreateLowRes(XmlNode node1, XmlNode node2)
        {
            CreateLowRes = "N"; // 預設

            if (node1 == null || node1.InnerText.ToUpper() != "Y")
                return fnValidate_CreateLowRes();

            // 檢查LowResDir
            if (node2 == null)
            {
                ErrorMessage = "Parse Xml Error: /JobIngest/LowResDir must have value when /JobIngest/CreateLowRes = 'Y'.";
                return false;
            }
            LowResDir = node2.InnerText;
            CreateLowRes = "Y";

            //
            return fnValidate_CreateLowRes();
        }

        private bool fnParse_JobIngest_CaptureKeyframe(XmlNode node)
        {
            if (node == null)
                CaptureKeyframe = "N"; // 未填，則預設為N
            else
            {
                if (node.InnerText.ToUpper() == "Y")
                    CaptureKeyframe = "Y";
                else
                    CaptureKeyframe = "N";
            }
            return fnValidate_CaptureKeyframe();
        }

        private bool fnParse_JobIngest_DisplayLogo(XmlNode node1, XmlNode node2)
        {
            this.DisplayLogo = "N"; // 預設值

            if (node1 == null || node1.InnerText.ToUpper() != "Y")
                return fnValidate_DisplayLogo();

            // 檢查LowResDir
            if (node2 == null)
            {
                ErrorMessage = "Parse Xml Error: /JobIngest/DisplayLogo must have value when /JobIngest/LogoProfilePath = 'Y'.";
                return false;
            }
            LogoProfilePath = node2.InnerText;
            DisplayLogo = "Y";

            //
            return fnValidate_CreateLowRes();
        }

        private bool fnParse_JobIngest_Note(XmlNode node)
        {
            if (node == null)
                this.Note = string.Empty;
            else
                this.Note = node.InnerText;
            return fnValidate_Note();
        }
    }
}
