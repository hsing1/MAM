﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Text;
using System.IO;
using System.Xml;
using System.Data.SqlClient;
using System.Xml.Linq;
using PTS_MAM3.Web.WebReferenceAnystream;
using System.Collections;

namespace PTS_MAM3.Web.WS.WSASC
{
    /// <summary>
    /// 直接連接 ANYSTREAM 系統的 WEB SERVICE
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class ServiceAnystream : System.Web.Services.WebService
    {
        public const string SYSCODE = "ASC";

        public struct QueryAnsJobProgress
        {
            public int ansJobID;    // 查詢的 ansID
            public bool isERROR;    // 當 ERROR 時為 TRUE，請注意：在發生錯誤時依然可能會有進度
            public bool isFINISH;   // 
            public string progress; // 轉檔的進度
        }

        // 在 profile ROOT 底下，必定要有 pp , ms , mxf , watermark , pinnacle 等資料夾
        // 這是測試時得到的結果，不然 Anystream 沒辦法正確的判斷他們的身份

        /// <summary>
        /// 傳送轉檔指令給Anystream
        /// </summary>
        /// <param name="xmlData"></param>
        /// <returns></returns>
        [WebMethod]
        public int DoIngest(string xmlData)
        {
            string fsPROGRAM = MAM_PTS_DLL.ConvertFormat.Combine_TraceLogFSProgram(SYSCODE, System.Reflection.MethodBase.GetCurrentMethod().Name);

            JobSubmitStatus[] objJobSubmitStatus = null;
            try
            {
                AgilitySoapAPISoapClient objANS = new AgilitySoapAPISoapClient();
                objJobSubmitStatus = objANS.Submit(xmlData, string.Empty);
                objANS.Close();
            }
            catch (Exception ex)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Concat("Got error when sending command to Anystream: ", ex.ToString(), Environment.NewLine, xmlData));
                return -1;
            }
            finally
            {
                GC.Collect();
            }

            // 取出回傳的 JobID，直接回傳
            return objJobSubmitStatus[0].JobID;
        }

        /// <summary>
        /// 查詢轉檔的進度值，會回傳簡易的資料
        /// </summary>
        /// <param name="ansJobID"></param>
        /// <returns></returns>
        [WebMethod]
        public QueryAnsJobProgress DoIngestQueryProcess(int ansJobID)
        {
            string fsPROGRAM = MAM_PTS_DLL.ConvertFormat.Combine_TraceLogFSProgram(SYSCODE, System.Reflection.MethodBase.GetCurrentMethod().Name);

            QueryAnsJobProgress returnData = new QueryAnsJobProgress();
            returnData.ansJobID = ansJobID; // 
            returnData.isERROR = true;      // 先設定為錯誤
            returnData.progress = "0";      // 進度設定為 0

            // 建立 SOAP 物件，並做查詢
            AgilitySoapAPISoapClient objAPI = null;
            JobDetail objJobDetail = null;
            try
            {
                objAPI = new AgilitySoapAPISoapClient();
                objJobDetail = objAPI.GetJobDetail(ansJobID);
            }
            catch (Exception ex)
            {
                string errMsg = string.Concat("Got error when execute DoIngestQueryProcess: ", ex.ToString(), Environment.NewLine, "Input: ", ansJobID.ToString());
                MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return returnData;
            }
            finally
            {
                if (objAPI != null)
                    objAPI.Close();
                GC.Collect();
            }
            //
            /*
                // Pending – job is waiting to be scheduled
                // Running – job is running
                // Completed – job has completed successfully
                // Failed – job has failed
             */
            //MAM_PTS_DLL.Log.AppendTrackingLog("william-temp-test", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "State = " + objJobDetail.State.ToString());
            //MAM_PTS_DLL.Log.AppendTrackingLog("william-temp-test", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "Status = " + objJobDetail.Status.ToString());
            if (objJobDetail.Status.ToLower() != "failed")
                returnData.isERROR = false;
            if (objJobDetail.Status.ToLower() == "pending" || objJobDetail.Status.ToLower() == "running")
                returnData.isFINISH = false;
            else
                returnData.isFINISH = true;
            returnData.progress = fnCalculateJobProgress(objJobDetail);
            // 
            return returnData;
        }

        /// <summary>
        /// 查詢轉檔的進度狀態(較詳細的，回傳XML)
        /// </summary>
        /// <param name="ansJobID"></param>
        /// <returns></returns>
        [WebMethod]
        public string DoIngestQuery(int ansJobID)
        {
            string fsPROGRAM = MAM_PTS_DLL.ConvertFormat.Combine_TraceLogFSProgram(SYSCODE, System.Reflection.MethodBase.GetCurrentMethod().Name);

            // 建立 SOAP 物件，並做查詢
            AgilitySoapAPISoapClient objAPI = null;
            JobDetail objJobDetail = null;
            try
            {
                objAPI = new AgilitySoapAPISoapClient();
                objJobDetail = objAPI.GetJobDetail(ansJobID);
            }
            catch (Exception ex)
            {
                string errMsg = string.Concat("Got error when execute DoIngestQuery: ", ex.ToString(), Environment.NewLine, "Input: ", ansJobID.ToString());
                MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return string.Empty;
            }
            finally
            {
                if (objAPI != null)
                    objAPI.Close();
                GC.Collect();
            }

            // 這邊是很詳盡的解析，用不到
            //fnParseJobDetail(objJobDetail);

            return fnCreateJobQueryReturnXml(ansJobID.ToString(), objJobDetail.Status, fnCalculateJobProgress(objJobDetail).ToString(), objJobDetail.CompletionReason);
        }
        
        /// <summary>
        /// ANYSTREAM排程管理 - RETRY
        /// </summary>
        /// <param name="ansJobID"></param>
        /// <returns></returns>
        [WebMethod]
        public bool DoJobRetry(int ansJobID)
        {
            string fsPROGRAM = MAM_PTS_DLL.ConvertFormat.Combine_TraceLogFSProgram(SYSCODE, System.Reflection.MethodBase.GetCurrentMethod().Name);

            if (!fnValidateJobID(ansJobID))
                return false;

            AgilitySoapAPISoapClient objAPI = null;
            try
            {
                objAPI = new AgilitySoapAPISoapClient();
                objAPI.RescheduleJob(ansJobID);
            }
            catch (Exception ex)
            {
                string errMsg = string.Concat("Got error when execute DoJobRetry: ", ex.ToString(), Environment.NewLine, "Input: ", ansJobID.ToString());
                MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return false;
            }
            finally
            {
                if (objAPI != null)
                    objAPI.Close();
                GC.Collect();
            }

            // 做 Retry 這個動作很特別，一定要再做記錄
            fnInsertAnsManagementRecord(string.Format("DoJobRetry({0})", ansJobID), HttpContext.Current.Request.UserHostAddress);

            //return fnCreateJobManageReturnXml(ansJobID.ToString(), "Success");
            return true;
        }

        /// <summary>
        /// ANYSTREAM排程管理 - STOP
        /// </summary>
        /// <param name="ansJobID"></param>
        /// <returns></returns>
        [WebMethod]
        public bool DoJobStop(int ansJobID)
        {
            string fsPROGRAM = MAM_PTS_DLL.ConvertFormat.Combine_TraceLogFSProgram(SYSCODE, System.Reflection.MethodBase.GetCurrentMethod().Name);

            if (!fnValidateJobID(ansJobID))
                return false;

            AgilitySoapAPISoapClient objAPI = null;
            try
            {
                objAPI = new AgilitySoapAPISoapClient();
                objAPI.StopJob(ansJobID);
            }
            catch (Exception ex)
            {
                string errMsg = string.Concat("Got error when execute DoJobStop: ", ex.ToString(), Environment.NewLine, "Input: ", ansJobID.ToString());
                MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return false;
            }
            finally
            {
                if (objAPI != null)
                    objAPI.Close();
                GC.Collect();
            }

            // 做 Stop 這個動作很特別，一定要再做記錄
            fnInsertAnsManagementRecord(string.Format("DoJobStop({0})", ansJobID), HttpContext.Current.Request.UserHostAddress);

            //return fnCreateJobManageReturnXml(jobID.ToString(), "Success");
            return true;
        }

        /// <summary>
        /// ANYSTREAM排程管理 - DELETE
        /// </summary>
        /// <param name="ansJobID"></param>
        /// <returns></returns>
        [WebMethod]
        public bool DoJobDelete(int ansJobID)
        {
            string fsPROGRAM = MAM_PTS_DLL.ConvertFormat.Combine_TraceLogFSProgram(SYSCODE, System.Reflection.MethodBase.GetCurrentMethod().Name);

            if (!fnValidateJobID(ansJobID))
                return false;

            AgilitySoapAPISoapClient objAPI = null;
            try
            {
                objAPI = new AgilitySoapAPISoapClient();
                objAPI.DeleteJob(ansJobID);
            }
            catch (Exception ex)
            {
                string errMsg = string.Concat("Got error when execute DoJobRetry: ", ex.ToString(), Environment.NewLine, "Input: ", ansJobID.ToString());
                MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return false;
            }
            finally
            {
                if (objAPI != null)
                    objAPI.Close();
                GC.Collect();
            }

            // 做 Delete 這個動作很特別，一定要再做記錄
            fnInsertAnsManagementRecord(string.Format("DoJobDelete({0})", ansJobID), HttpContext.Current.Request.UserHostAddress);

            //return fnCreateJobManageReturnXml(jobID.ToString(), "Success");
            return true;
        }

        /// <summary>
        /// ANYSTREAM排程管理 - 修改優先權
        /// </summary>
        /// <param name="ansJobID"></param>
        /// <param name="jobPriority"></param>
        /// <returns></returns>
        [WebMethod]
        public bool DoChangePriority(int ansJobID, int jobPriority)
        {
            string fsPROGRAM = MAM_PTS_DLL.ConvertFormat.Combine_TraceLogFSProgram(SYSCODE, System.Reflection.MethodBase.GetCurrentMethod().Name);

            // 檢查看看輸入的 ansJobID 是否有符合格式 (just an integer)
            if (!fnValidateJobID(ansJobID))
                return false;

            // 檢查 jobPriority 的值是否合法
            // ANS: Displays the job priority that was defined when the watch was set up.  
            //      Priority can be 1 - 100, with 1 being the highest priority. 
            if (jobPriority < 1 || jobPriority > 100)
                return false;

            AgilitySoapAPISoapClient objAPI = null;
            try
            {
                objAPI = new AgilitySoapAPISoapClient();
                objAPI.SetJobPriority(ansJobID, jobPriority);
            }
            catch (Exception ex)
            {
                string errMsg = string.Concat("Got error when execute DoChangePriority: ", ex.ToString(), Environment.NewLine, "Input: ", ansJobID.ToString(), ", parameter = " + jobPriority.ToString());
                MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return false;
            }
            finally
            {
                if (objAPI != null)
                    objAPI.Close();
                GC.Collect();
            }

            // 這個動作很特別，一定要再做記錄
            fnInsertAnsManagementRecord(string.Format("DoChangePriority({0})", ansJobID), HttpContext.Current.Request.UserHostAddress);

            return true;
        }

        // 要回傳用戶端的Xml內容(JobQuery)
        private string fnCreateJobQueryReturnXml(string JobID, string JobStatus, string JobProgress, string JobCompletionReason)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"<?xml version=""1.0"" encoding=""utf-8"" ?>");
            sb.Append(@"<JobQueryReturn>");
            sb.Append(@"  <JobID>" + JobID + "</JobID>");
            sb.Append(@"  <JobStatus>" + JobStatus + "</JobStatus>");
            sb.Append(@"  <JobProgress>" + JobProgress + "</JobProgress>");
            sb.Append(@"  <JobCompletionReason>" + JobCompletionReason + "</JobCompletionReason>");
            sb.Append(@"</JobQueryReturn>");
            return sb.ToString();
        }

        // 驗證是否為合法的JobID格式
        private bool fnValidateJobID(int ansJobID)
        {
            if (ansJobID < 0)
                return false;
            return true;
        }

        // 計算Job的進度
        private string fnCalculateJobProgress(JobDetail obj)
        {
            int totalComplete = 0;

            // 轉正後再相加
            //Task[] objTaskArr = obj.TaskList;
            foreach (Task objTask in obj.TaskList)
            {
                //MAM_PTS_DLL.Log.AppendTrackingLog("williamtemptest-progress", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, string.Concat(objTask.Id.ToString(), " = " , objTask.PercentComplete));
                if (objTask.PercentComplete < 0)
                    return "影帶擷取中";
                else
                    totalComplete += Math.Abs(objTask.PercentComplete);
            }

            // 除以項目數，以求平均
            return ((int)(totalComplete / obj.TaskList.Length)).ToString();
        }

        private bool fnInsertAnsManagementRecord(string actionDesc, string caller)
        {
            bool execResult;
            WSASC.ServiceTranscode obj = new WSASC.ServiceTranscode();
            execResult = obj.InsertTranscodeMgmtLogData(-1, actionDesc, caller);
            obj.Dispose();
            return execResult;
        }

        // 沒在用
        //[WebMethod]
        //public string DoGetNoteList()
        //{
        //    AgilitySoapAPISoapClient objAPI = new AgilitySoapAPISoapClient();

        //    StringBuilder sb = new StringBuilder();

        //    //NodeInfo[] NodeInfoArr = objAPI.GetNodeInfoList();
        //    foreach (NodeInfo objNodeInfo in objAPI.GetNodeInfoList())
        //    {
        //        sb.Append("Host Name: " + objNodeInfo.Host.ToString() + Environment.NewLine);
        //        //sb.Append("Node ID: " + objNodeInfo.Id.ToString() + Environment.NewLine);
        //        sb.Append("Node ID: " + objNodeInfo.Id.ToString() + "<br>");
        //        // tricky: 雖然 objNodeInfo.WorkerStatusList 也是列出 WorkerStatus 的集合，但就是沒內容
        //        NodeInfo obj = objAPI.GetNodeDetailByID(Int32.Parse(objNodeInfo.Id));
        //        foreach (WorkerStatus objWorkerStatus in obj.WorkerStatusList)
        //        //foreach (WorkerStatus objWorkerStatus in objNodeInfo.WorkerStatusList)
        //        {
        //            if (objWorkerStatus.Type == "capturePrefilter")
        //            {
        //                sb.Append("--- " + objWorkerStatus.Id.ToString());
        //                sb.Append("(" + objWorkerStatus.Status + ")");
        //            }
        //        }
        //    }

        //    return sb.ToString();
        //}

        // 要回傳用戶端的Xml內容(JobIngest)
        //private string fnCreateJobIngestReturnXml(string JobID, string msg)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append(@"<?xml version=""1.0"" encoding=""utf-8"" ?>");
        //    sb.Append(@"<JobIngestReturn>");
        //    sb.Append(@"  <JobID>" + JobID + "</JobID>");
        //    sb.Append(@"  <Message>" + msg + "</Message>");
        //    sb.Append(@"</JobIngestReturn>");
        //    return sb.ToString();
        //}

        // 要回傳用戶端的Xml內容(JobManage，包括Retry、Stop、Delete)
        //private string fnCreateJobManageReturnXml(string JobID, string msg)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append(@"<?xml version=""1.0"" encoding=""utf-8"" ?>");
        //    sb.Append(@"<JobManageReturn>");
        //    sb.Append(@"  <JobID>" + JobID + "</JobID>");
        //    sb.Append(@"  <Message>" + msg + "</Message>");
        //    sb.Append(@"</JobManageReturn>");
        //    return sb.ToString();
        //}

        /*
        //  完整的Tape Ingest內容：
        //  Note：需要先做好create folder的動作，不然會轉檔時會失敗
        //<planner-submit>
        //    <meta-title>william tape test</meta-title>
        //    <job-name>MAM Subprofile</job-name>
        //    <add-subprofile>\\10.13.231.201\Media\WilliamTestProfile\profiles\pp\williamTest_pp0.pp.awp</add-subprofile>
        //    <add-subprofile>\\10.13.231.201\Media\WilliamTestProfile\profiles\mxf\williamTest_enc_IMX50.mxf.awp</add-subprofile>
        //    <add-subprofile>\\10.13.231.201\Media\WilliamTestProfile\profiles\ms\williamTest_enc_wmv.ms.awp</add-subprofile>
        //    <live-job>1</live-job>
        //    <capture-station>LCS03-CH1</capture-station>
        //    <start-trigger-type>TIMECODE</start-trigger-type>
        //    <stop-trigger-type>TIMECODE</stop-trigger-type>
        //    <start-trigger-timecode>01;05;00;02</start-trigger-timecode>
        //    <stop-trigger-timecode>01;05;30;02</stop-trigger-timecode>
        //    <ms-output-dir>\\10.13.231.100\agility\WilliamTestOutput\ms\</ms-output-dir>
        //    <mxf-output-dir>\\10.13.231.100\agility\WilliamTestOutput\mxf\</mxf-output-dir>
        //    <output-name>$(basename)-$(time).$(extension)</output-name>
        //    <output-basename>williamTestTape</output-basename>
        //    <video-format>525 59.94Hz</video-format>
        //</planner-submit>
        
        //  完整功能版的File Trasncode 
        //<planner-submit>
        //    <meta-title>william tape test</meta-title>
        //    <job-name>My Testing Subprofile</job-name>
        //    <add-subprofile>\\10.13.231.201\Media\WilliamTestProfile\profiles\pp\williamTest_pp1.pp.awp</add-subprofile>
        //    <add-subprofile>\\10.13.231.201\Media\WilliamTestProfile\profiles\ms\williamTest_enc_wmv.ms.awp</add-subprofile>
        //    <add-subprofile>\\10.13.231.201\Media\WilliamTestProfile\profiles\watermark\williamTest.watermark.awp</add-subprofile>
        //    <add-subprofile>\\10.13.231.201\Media\WilliamTestProfile\profiles\not\williamTest.not.awp</add-subprofile>
        //    <source-name>\\10.13.231.100\agility\WilliamTestInput\20110114-0003_TC.mxf</source-name>
        //    <thumb-time-single>00:00:10.000</thumb-time-single>
        //    <source-inpoint>01;02;10;00</source-inpoint>
        //    <source-outpoint>01;02;50;00</source-outpoint>
        //    <ms-output-dir>\\10.13.231.100\agility\WilliamTestOutput\ms\</ms-output-dir>
        //    <output-name>$(basename)-$(time).$(extension)</output-name>
        //    <output-basename>williamTestFile</output-basename>
        //</planner-submit>

        //  主控播出SD格式的轉檔作業
        //<planner-submit>
        //    <meta-title>william tape test</meta-title>
        //    <job-name>My Testing Subprofile</job-name>
        //    <add-subprofile>\\10.13.231.201\Media\WilliamTestProfile\profiles\pp\williamTest_pp1.pp.awp</add-subprofile>
        //    <add-subprofile>\\10.13.231.201\Media\WilliamTestProfile\profiles\pinnacle\williamTest_enc.pinnacle.awp</add-subprofile>
        //    <add-subprofile>\\10.13.231.201\Media\WilliamTestProfile\profiles\watermark\williamTest.watermark.awp</add-subprofile>
        //    <source-name>\\10.13.231.100\agility\WilliamTestInput\20110114-0003_TC.mxf</source-name>
        //    <thumb-time-single>00:00:10.000</thumb-time-single>
        //    <source-inpoint>01;02;10;00</source-inpoint>
        //    <source-outpoint>01;02;30;00</source-outpoint>
        //    <pinnacle-output-dir>\\nas5100n\agility\WilliamTestOutput\pinnacle</pinnacle-output-dir>
        //    <output-name>$(basename)</output-name>
        //    <output-basename>A00000003</output-basename>
        //</planner-submit>
        */

        #region outdated functions
        //[Obsolete("這隻程式已經過期了 =_=")]
        //[WebMethod]
        // Status：
        // [Trace] 正常回傳JobID
        // [Debug] 不正確的輸入(Invalid Input)
        // [Debug] 無法建立目的地目錄
        // [Debug] 目的地檔案已經存在
        // [Error] 傳送指令到AnyStream失敗
        // [Error] 更新資料庫內容失敗
        //public string DoIngest(string xmlData)
        //{
        //    string fsPROGRAM = MAM_PTS_DLL.ConvertFormat.Combine_TraceLogFSProgram(SYSCODE, System.Reflection.MethodBase.GetCurrentMethod().Name);

        //    Dictionary<string, string> resultDict;

        //    // 將Xml資料讀入Dictionary，如果內容有誤，會回傳false
        //    if (!fnParseDoIngestXml(xmlData, out resultDict))
        //    {
        //        MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.DEBUG, resultDict["ErrMsg"]);
        //        return fnCreateJobIngestReturnXml(string.Empty, resultDict["ErrMsg"]);
        //    }

        //    // 建立目的路徑目錄，因為Anystream不會自己建
        //    try
        //    {
        //        Directory.CreateDirectory(resultDict["OutputDir"]);
        //    }
        //    catch (Exception ex)
        //    {
        //        MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.DEBUG, string.Concat("Can't Create Destination Directory: ", resultDict["OutputDir"], Environment.NewLine, "ErrorMsg: ", ex.ToString(), Environment.NewLine, xmlData));
        //        return fnCreateJobIngestReturnXml(string.Empty, "Error: Can't create destination directory. Job request terminated.");
        //    }

        //    string strScript = string.Empty;

        //    // 呼叫Anystream轉檔
        //    AgilitySoapAPISoapClient objAPI = new AgilitySoapAPISoapClient();
        //    JobSubmitStatus[] objJobSubmitStatus = null;
        //    try
        //    {
        //        strScript = fnCreateJobIngestXml(ref resultDict);
        //        objJobSubmitStatus = objAPI.Submit(strScript, string.Empty);
        //    }
        //    catch (Exception ex)
        //    {
        //        MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.DEBUG, string.Concat("Got error when sending command to Anystream: ", ex.ToString(), Environment.NewLine, xmlData));
        //        return fnCreateJobIngestReturnXml(string.Empty, "Got error when sending command to Anystream.");
        //    }

        //    // 在 JobSubmit 後，嘗試刪除掉使用的 temp profile 檔
        //    //if (!string.IsNullOrEmpty(result["LogoProfilePath"]))
        //    //    File.Delete(result["LogoProfilePath"]);

        //    //WriteTmpLog("info", strScript);

        //    string jobID = objJobSubmitStatus[0].JobID.ToString();

        //    if (!fnDbInsertTranscode(jobID, resultDict))
        //    {
        //        MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Concat("Got error when inserting transcode data to DataBase", Environment.NewLine, xmlData));
        //        return fnCreateJobIngestReturnXml(jobID, "Got error when inserting transcode data to DataBase.");
        //    }

        //    return fnCreateJobIngestReturnXml(jobID, "Success");
        //}

        // 舊版了
        // 要送給Anystream的Xml內容
        //private string fnCreateJobIngestXml(ref Dictionary<string, string> inputData)
        //{
        //    StringBuilder sb = new StringBuilder();

        //    // 組合要輸出的內容
        //    sb.Append("<planner-submit>");

        //    sb.Append(@"  <job-name>MAM custom profile</job-name>");
        //    if (inputData["TransCodeType"] == "tape")
        //    {
        //        sb.Append(@"  <meta-title>MAM Tape Ingest</meta-title>");
        //        sb.Append(@"  <add-subprofile>" + dirProfileRoot + @"pp\pp_tape.pp.awp</add-subprofile>");
        //    }
        //    else
        //    {
        //        sb.Append(@"  <meta-title>MAM File Transcode</meta-title>");
        //        sb.Append(@"  <add-subprofile>" + dirProfileRoot + @"pp\pp_file.pp.awp</add-subprofile>");
        //    }

        //    switch (inputData["TrancodeFormat"])
        //    {
        //        case "imx50":
        //            sb.Append(@"  <add-subprofile>" + dirProfileRoot + @"mxf\enc_IMX50.mxf.awp</add-subprofile>");
        //            break;
        //        case "wmv":
        //            sb.Append(@"  <add-subprofile>" + dirProfileRoot + @"ms\enc_wmv.ms.awp</add-subprofile>");
        //            break;
        //        case "mediastream":
        //            sb.Append(@"  <add-subprofile>" + dirProfileRoot + @"pinnacle\enc_pinnacle.pinnacle.awp</add-subprofile>");
        //            break;
        //        case "hd":
        //            break;
        //    }

        //    // WMV轉檔 與 產生低解 會相衝突，不可以同時使用
        //    if (inputData["TrancodeFormat"] != "wmv" && inputData["CreateLowRes"] == "y")
        //        sb.Append(@"  <add-subprofile>" + dirProfileRoot + @"ms\enc_wmv.ms.awp</add-subprofile>");

        //    if (inputData["TransCodeType"] == "tape")
        //    {
        //        sb.Append(@"  <live-job>1</live-job>");
        //        sb.Append(@"  <capture-station>" + inputData["SourceFrom"] + "</capture-station>");
        //        sb.Append(@"  <start-trigger-type>TIMECODE</start-trigger-type>");
        //        sb.Append(@"  <stop-trigger-type>TIMECODE</stop-trigger-type>");
        //        sb.Append(@"  <start-trigger-timecode>" + inputData["StartTimecode"] + "</start-trigger-timecode>");
        //        sb.Append(@"  <stop-trigger-timecode>" + inputData["EndTimecode"] + "</stop-trigger-timecode>");
        //        sb.Append(@"  <video-format>525 59.94Hz</video-format>");
        //    }
        //    else
        //    {
        //        sb.Append(string.Format(@"  <source-name>{0}</source-name>", inputData["SourceFrom"]));
        //        sb.Append(string.Format(@"  <thumb-time-single>00:00:10.000</thumb-time-single>"));
        //        sb.Append(string.Format(@"  <source-inpoint>{0}</source-inpoint>", inputData["StartTimecode"]));
        //        sb.Append(string.Format(@"  <source-outpoint>{0}</source-outpoint>", inputData["EndTimecode"]));
        //    }

        //    sb.Append(string.Format(@"  <output-name>$(basename).$(extension)</output-name>"));
        //    sb.Append(string.Format(@"  <output-basename>{0}</output-basename>", inputData["OutputName"]));

        //    // 組合 output-dir 的部份
        //    sb.Append(string.Format(@"  <mxf-output-dir>{0}</mxf-output-dir>", inputData["OutputDir"]));
        //    sb.Append(string.Format(@"  <pinnacle-output-dir>{0}</pinnacle-output-dir>", inputData["OutputDir"]));
        //    if (inputData["CreateLowRes"] == "y")
        //        sb.Append(string.Format(@"  <ms-output-dir>{0}</ms-output-dir>", inputData["LowResDir"]));
        //    else
        //        sb.Append(string.Format(@"  <ms-output-dir>{0}</ms-output-dir>", inputData["OutputDir"]));

        //    // 組合 LOGO 的部份
        //    // 寫法1：最簡單的寫法
        //    if (inputData["LogoDisplay"] == "y")
        //        sb.Append(string.Format(@"  <add-subprofile>" + dirProfileRoot + @"watermark\watermark{0}_{1}.watermark.awp</add-subprofile>", inputData["LogoSelection"], inputData["LogoLocation"]));
        //    //    sb.Append(string.Format(@"  <add-subprofile>\\10.13.231.201\Media\WilliamTestProfile\profiles\watermark\000000.watermark.awp</add-subprofile>"));
        //    /*
        //       寫法2：較有彈性的寫法
        //     */
        //    // Begin
        //    /*
        //    if (inputData["LogoDisplay"] == "y")
        //    {
        //        string outputTemplatePath = dirProfileRoot + @"watermark\WatermarkTemplate.watermark.awp";
        //        string outputDestPath = dirProfileRoot + @"watermark\" + inputData["Ts"] + ".watermark.awp";

        //        // 讀入範本檔
        //        StreamReader sr = new StreamReader(outputTemplatePath, Encoding.Default);
        //        string templatecontent = sr.ReadToEnd();
        //        sr.Close();
        //        // 字串替換
        //        templatecontent = templatecontent.Replace("<!-- LogoSelection_Dir -->", Path.GetDirectoryName(inputData["LogoSelection"]));
        //        templatecontent = templatecontent.Replace("<!-- LogoSelection_File -->", Path.GetFileName(inputData["LogoSelection"]));
        //        if (inputData["LogoLocation"] == "1" || inputData["LogoLocation"] == "3")
        //            templatecontent = templatecontent.Replace("<!-- LogoStyle_X -->", "Left");
        //        else
        //            templatecontent = templatecontent.Replace("<!-- LogoStyle_X -->", "Right");
        //        if (inputData["LogoLocation"] == "1" || inputData["LogoLocation"] == "2")
        //            templatecontent = templatecontent.Replace("<!-- LogoStyle_Y -->", "Top");
        //        else
        //            templatecontent = templatecontent.Replace("<!-- LogoStyle_Y -->", "Bottom");
        //        // 寫到新的檔案
        //        StreamWriter sw = File.CreateText(outputDestPath);
        //        sw.Write(templatecontent);
        //        sw.Flush();
        //        sw.Close();
        //        // 加入profile參考
        //        sb.Append(string.Format(@"  <add-subprofile>{0}</add-subprofile>", outputDestPath));
        //        // 
        //        inputData["LogoProfilePath"] = outputDestPath;
        //    } else {
        //        inputData["LogoProfilePath"] = string.Empty;
        //    }
        //    */
        //    // End

        //    // 組合 HTTP通知 的部份
        //    sb.Append(@"  <add-subprofile>" + dirProfileRoot + @"not\williamTest.not.awp</add-subprofile>");

        //    sb.Append(@"</planner-submit>");

        //    return sb.ToString();
        //}

        // 這隻程式隨著舊的 DoIngest 一起過期了
        //分析IngestInputXml有用到的欄位：
        // Ts：自己建立的一個UID
        // TransCodeType：file或tape，系統會自動轉小寫
        // TrancodeFormat：imx50或wmv或mediastream或hd，系統會自動轉小寫
        // SourceFrom：無限制，但必填
        // DestinationTo：無限制，但必填
        // OutputDir：由DestinationTo拆出來的，後面有加 \
        // OutputName：由DestinationTo拆出來的，不包含副檔名(為了配合MediaStream的格式)
        // StartTimecode：無限制，但必填，其實應該要檢查格式的
        // EndTimecode：無限制，但必填，其實應該要檢查格式的
        // PostbackPath：選填，如果沒有輸入的話會自動填入空字串
        // CreateLowRes：y或n(預設)
        // LowResDir：當CreateLowRes為y時，才會讀取，無限制，如果沒有輸入的話會自動填入空字串
        // CaptureKeyframe：y或n(預設)
        // KeyframeDir：當CaptureKeyframe為y時，才會讀取，無限制，如果沒有輸入的話會自動填入空字串
        // DisplayLogo：y或n(預設)
        // LogoSelection：當DisplayLogo為y時，才會讀取，預設為1
        // LogoLocation：當DisplayLogo為y時，才會讀取，預設為1
        // Note：選填，無限制，如果沒有輸入的話會自動填入空字串
        //private bool fnParseDoIngestXml(string inputXmlData, out Dictionary<string, string> outputData)
        //{
        //    outputData = new Dictionary<string, string>();
        //    string tmpString = string.Empty;

        //    // 得到的結果應該會像是 2010-01-14-10-12-25-999，其精確度到毫秒
        //    outputData.Add("Ts", string.Format("{0:yyyy-MM-dd-HH-mm-ss}-{1}", System.DateTime.Now, System.DateTime.Now.Millisecond.ToString()));

        //    XmlDocument xmlDoc = new XmlDocument();
        //    XmlNode objXmlNode;
        //    try
        //    {
        //        xmlDoc.LoadXml(inputXmlData);
        //    }
        //    catch (Exception ex)
        //    {
        //        outputData.Add("ErrMsg", string.Concat("Load Xml Error: ", ex.ToString(), Environment.NewLine, inputXmlData));
        //        return false;
        //    }

        //    // 開始解析XML內容

        //    // 必填欄位
        //    objXmlNode = xmlDoc.SelectSingleNode("/JobIngest/TransCodeType");
        //    if (objXmlNode == null)
        //    {
        //        outputData.Add("ErrMsg", string.Concat("Parse Xml Error: Can't find /JobIngest/TransCodeType", Environment.NewLine, inputXmlData));
        //        return false;
        //    }
        //    // 限制：TransCodeType 只能是 Tape 或 File
        //    tmpString = objXmlNode.InnerText.ToLower();
        //    if (tmpString == "tape" || tmpString == "file")
        //        outputData.Add("TransCodeType", tmpString);
        //    else
        //    {
        //        outputData.Add("ErrMsg", string.Concat("Parse Xml Error: /JobIngest/TransCodeType can't be '", objXmlNode.InnerText, "'", Environment.NewLine, inputXmlData));
        //        return false;
        //    }

        //    objXmlNode = xmlDoc.SelectSingleNode("/JobIngest/TrancodeFormat");
        //    if (objXmlNode == null)
        //    {
        //        outputData.Add("ErrMsg", string.Concat("Parse Xml Error: Can't find /JobIngest/TrancodeFormat", Environment.NewLine, inputXmlData));
        //        return false;
        //    }
        //    // 限制：只能是 IMX50 WMV MediaStream HD
        //    tmpString = objXmlNode.InnerText.ToLower();
        //    if (tmpString == "imx50" || tmpString == "wmv" || tmpString == "mediastream" || tmpString == "hd")
        //        outputData.Add("TrancodeFormat", tmpString);
        //    else
        //    {
        //        outputData.Add("ErrMsg", string.Concat("Parse Xml Error: /JobIngest/TrancodeFormat can't be '", tmpString, "'", Environment.NewLine, inputXmlData));
        //        return false;
        //    }

        //    // 必填欄位
        //    objXmlNode = xmlDoc.SelectSingleNode("/JobIngest/SourceFrom");
        //    if (objXmlNode == null)
        //    {
        //        outputData.Add("ErrMsg", string.Concat("Parse Xml Error: Can't find /JobIngest/SourceFrom", Environment.NewLine, inputXmlData));
        //        return false;
        //    }
        //    tmpString = objXmlNode.InnerText;
        //    // Source From 可能會是VCR機器編號或是檔案路徑
        //    if (outputData["TransCodeType"] == "file" && !File.Exists(tmpString))
        //    {
        //        outputData.Add("ErrMsg", string.Concat("Parse Xml Error: Can't find source file (", tmpString, ") at /JobIngest/SourceFrom", Environment.NewLine, inputXmlData));
        //        return false;
        //    }
        //    outputData.Add("SourceFrom", tmpString);

        //    // 必填欄位
        //    objXmlNode = xmlDoc.SelectSingleNode("/JobIngest/DestinationTo");
        //    // 另外將結果拆成兩個資料
        //    if (objXmlNode == null)
        //    {
        //        outputData.Add("ErrMsg", string.Concat("Parse Xml Error: Can't find /JobIngest/DestinationTo", Environment.NewLine, inputXmlData));
        //        return false;
        //    }
        //    // 不是很好的寫法
        //    if (objXmlNode.InnerText.EndsWith(@"\"))
        //        outputData.Add("DestinationTo", objXmlNode.InnerText.Substring(0, objXmlNode.InnerText.Length - 1));
        //    else
        //        outputData.Add("DestinationTo", objXmlNode.InnerText);
        //    // 
        //    outputData.Add("OutputDir", string.Concat(Path.GetDirectoryName(objXmlNode.InnerText), @"\"));
        //    outputData.Add("OutputName", Path.GetFileNameWithoutExtension(objXmlNode.InnerText));

        //    objXmlNode = xmlDoc.SelectSingleNode("/JobIngest/StartTimecode");
        //    if (objXmlNode == null)
        //    {
        //        outputData.Add("ErrMsg", string.Concat("Parse Xml Error: Can't find /JobIngest/StartTimecode", Environment.NewLine, inputXmlData));
        //        return false;
        //    }
        //    if (!MAM_PTS_DLL.CheckFormat.CheckFormat_TimeCode(objXmlNode.InnerText))
        //    {
        //        outputData.Add("ErrMsg", string.Concat("Parse Xml Error: TimeCode (" , objXmlNode.InnerText , ") is invalid at /JobIngest/StartTimecode", Environment.NewLine, inputXmlData));
        //        return false;
        //    }
        //    outputData.Add("StartTimecode", objXmlNode.InnerText);

        //    objXmlNode = xmlDoc.SelectSingleNode("/JobIngest/EndTimecode");
        //    if (objXmlNode == null)
        //    {
        //        outputData.Add("ErrMsg", string.Concat("Parse Xml Error: Can't find /JobIngest/EndTimecode", Environment.NewLine, inputXmlData));
        //        return false;
        //    }
        //    if (!MAM_PTS_DLL.CheckFormat.CheckFormat_TimeCode(objXmlNode.InnerText))
        //    {
        //        outputData.Add("ErrMsg", string.Concat("Parse Xml Error: TimeCode (", objXmlNode.InnerText, ") is invalid at /JobIngest/EndTimecode", Environment.NewLine, inputXmlData));
        //        return false;
        //    }
        //    outputData.Add("EndTimecode", objXmlNode.InnerText);

        //    objXmlNode = xmlDoc.SelectSingleNode("/JobIngest/PostbackPath");
        //    if (objXmlNode != null)
        //    {
        //        if (!MAM_PTS_DLL.CheckFormat.CheckFormat_HttpAddress(objXmlNode.InnerText))
        //        {
        //            outputData.Add("ErrMsg", string.Concat("Parse Xml Error: Postback address (", objXmlNode.InnerText, ") is invalid at /JobIngest/PostbackPath", Environment.NewLine, inputXmlData));
        //            return false;
        //        }
        //        outputData.Add("PostbackPath", objXmlNode.InnerText);
        //    }
        //    else
        //        outputData.Add("PostbackPath", string.Empty);

        //    outputData.Add("CreateLowRes", "n");
        //    outputData.Add("LowResDir", string.Empty);
        //    objXmlNode = xmlDoc.SelectSingleNode("/JobIngest/CreateLowRes");
        //    if (objXmlNode != null && objXmlNode.InnerText.ToLower() == "y")
        //    {
        //        objXmlNode = xmlDoc.SelectSingleNode("/JobIngest/LowResDir");
        //        if (objXmlNode != null)
        //        {
        //            outputData["CreateLowRes"] = "y";
        //            outputData["LowResDir"] = objXmlNode.InnerText;
        //        }
        //    }

        //    outputData.Add("CaptureKeyframe", "n");
        //    outputData.Add("KeyframeDir", string.Empty);
        //    objXmlNode = xmlDoc.SelectSingleNode("/JobIngest/CaptureKeyframe");
        //    if (objXmlNode != null && objXmlNode.InnerText.ToLower() == "y")
        //    {
        //        objXmlNode = xmlDoc.SelectSingleNode("/JobIngest/KeyframeDir");
        //        if (objXmlNode != null)
        //        {
        //            outputData["CaptureKeyframe"] = "y";
        //            outputData["KeyframeDir"] = objXmlNode.InnerText;
        //        }
        //    }

        //    outputData.Add("LogoDisplay", "n");
        //    objXmlNode = xmlDoc.SelectSingleNode("/JobIngest/DisplayLogo");
        //    if (objXmlNode != null && objXmlNode.InnerText.ToLower() == "y")
        //    {
        //        outputData["LogoDisplay"] = "y";
        //        outputData["LogoSelection"] = "1";
        //        outputData["LogoLocation"] = "1";

        //        // LOGO檔的選擇
        //        objXmlNode = xmlDoc.SelectSingleNode("/JobIngest/LogoSelection");
        //        if (objXmlNode != null)
        //            outputData["LogoSelection"] = objXmlNode.InnerText;
        //        // LOGO實際呈現的位置
        //        objXmlNode = xmlDoc.SelectSingleNode("/JobIngest/LogoLocation");
        //        if (objXmlNode != null)
        //            outputData["LogoLocation"] = objXmlNode.InnerText;
        //    }

        //    objXmlNode = xmlDoc.SelectSingleNode("/JobIngest/Note");
        //    outputData["Note"] = objXmlNode != null ? objXmlNode.InnerText : string.Empty;

        //    return true;
        //}

        // 這函式目前沒在用
        //private void fnParseJobDetail(JobDetail obj)
        //{
        //    // new – job is new since last status request
        //    // update – job status has changed since last status request
        //    // deleted – job has completed/failed and will be reaped from memory within 60 minutes
        //    //sb.Append("Main State = " + objJobDetail.State + Environment.NewLine);

        //    // Pending – job is waiting to be scheduled
        //    // Running – job is running
        //    // Completed – job has completed successfully
        //    // Failed – job has failed
        //    //sb.Append("Main Status = " + objJobDetail.Status + Environment.NewLine);

        //    // NotComplete – job has not started or is running
        //    // Succeeded – job completed successfully
        //    // Failed – job failed
        //    // UserStopped – job stopped at request of operator
        //    // SystemStopped – job stopped by system
        //    //sb.Append("Main Complete Reason = " + objJobDetail.CompletionReason + Environment.NewLine);

        //    //sb.Append("Main UserData = " + objJobDetail.UserData + Environment.NewLine);

        //    Task[] objTaskArr = obj.TaskList;
        //    foreach (Task objTask in objTaskArr)
        //    {
        //        //sb.Append(" TaskID = " + objTask.Id + Environment.NewLine);

        //        //    Pending – task is waiting to be scheduled
        //        //    Dependent – task is dependent on 1 or more other task start/complete events
        //        //    Provisioned – request to start task sent to LCS
        //        //    Running – task is executing
        //        //    StopRequest – request to stop task sent to LCS
        //        //    Completed – task completed successfully
        //        //    Failed – task failed
        //        //sb.Append(" Task status: " + objTask.Status + Environment.NewLine);

        //        //sb.Append(" Task progress: " + objTask.PercentComplete.ToString() + Environment.NewLine);
        //        //sb.Append(" Task Elpsed: " + objTask.ElapsedSeconds.ToString() + Environment.NewLine);
        //        //sb.Append(" Task FailMNsg: " + objTask.FailMessage.ToString() + Environment.NewLine);
        //        //sb.Append(" Task StopTime: " + objTask.StopTime.ToString() + Environment.NewLine);
        //        //sb.Append(" Task UserData: " + objTask.UserData.ToString() + Environment.NewLine);
        //    }
        //}

        // 這隻程式隨著舊的 DoIngest 一起過期了
        //private bool fnDbInsertTranscode(string jobID, Dictionary<string, string> result)
        //{
        //    Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
        //    sqlParameters.Add("FSJOBID", jobID);
        //    sqlParameters.Add("FSTYPE", result["TransCodeType"]);
        //    sqlParameters.Add("FSFORMAT", result["TrancodeFormat"]);
        //    sqlParameters.Add("FSSOURCEPATH", result["SourceFrom"]);
        //    sqlParameters.Add("FSDESTINATIONPATH", result["DestinationTo"]);
        //    sqlParameters.Add("FSCALLBACKPATH", result["PostbackPath"]);
        //    sqlParameters.Add("FSSTATUS", "s");    // Start
        //    sqlParameters.Add("FCNEEDLOGO", result["DisplayLogo"]);
        //    sqlParameters.Add("FCNEEDRES", result["CreateLowRes"]);
        //    sqlParameters.Add("FSRES_DIR", result["LowResDir"]);
        //    sqlParameters.Add("FCNEEDKEYFRAME", result["CaptureKeyframe"]);
        //    sqlParameters.Add("FSKEYFRAME_DIR", result["KeyframeDir"]);
        //    sqlParameters.Add("FSNOTE", result["Note"]);
        //    return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBTRANSCODE", MAM_PTS_DLL.DbAccess.CreateDoTranscationXml(sqlParameters));
        //}

        // Media Analyze 的方法是失效的，沒有購買 License
        //[WebMethod]
        //public void fnDoMediaAnalyze(string fname)
        //{
        //    AgilitySoapAPISoapClient objAPI = new AgilitySoapAPISoapClient();
        //    //obj.AnalyzeMediaAsync(fname);
        //    objAPI.AnalyzeMedia(fname);
        //}
        #endregion
    }
}
