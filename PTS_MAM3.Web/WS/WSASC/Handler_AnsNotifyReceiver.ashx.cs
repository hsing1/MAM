﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using System.Xml.Linq;
using System.Xml;
using System.Data.SqlClient;
using System.Net;

namespace PTS_MAM3.Web.WS.WSASC
{
    /// <summary>
    ///Handler1 的摘要描述
    /// </summary>
    public class AnsNotifyReceiver : IHttpHandler
    {
        const string SYSCODE = "ASC";

        /// <summary>
        /// 處理Anystream傳入的HTTP POST訊息
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            string fsPROGRAM = MAM_PTS_DLL.ConvertFormat.Combine_TraceLogFSProgram(SYSCODE, System.Reflection.MethodBase.GetCurrentMethod().Name);

            HttpRequest req = context.Request;

            // 準備變數
            ServiceTranscode objTranscodeService = new ServiceTranscode();
            ServiceTranscode.TRANSCODE_STATUS transcodeJobStatus = ServiceTranscode.TRANSCODE_STATUS.FINISH;

            // 解析從 ANS 傳來的內容
            // 如果 return false ，代表 HTTP POST 的內容有誤，無法判斷是哪個 ANSID，也因此不用改狀態
            long anystreamJobID;
            string anystreamJobStatus;
            bool flagParse = fnParseAnystreamReturnPost(req.InputStream, out anystreamJobID, out anystreamJobStatus);

            // William: 2012-04-16
            context.Response.Flush();
            context.Response.Close();

            if (!flagParse)
                return; // 由 fnParseAnystreamReturnPost 來寫LOG

            // 很不好的寫法！可是目前只能醬子寫了
            if (anystreamJobStatus == "Completed")
                transcodeJobStatus = ServiceTranscode.TRANSCODE_STATUS.FINISH;
            else
                transcodeJobStatus = ServiceTranscode.TRANSCODE_STATUS.ANSERROR;

            // 利用 AnystreamJobID 當 PK，找出所有需要的相關資訊
            // 當找不到相關資訊時，自然也無法正確回寫狀態
            long transcodeJobID, tsmJobID, partialJobID;
            string anystreamXML, notifyAddr;
            ServiceTranscode.TRANSCODE_TYPE transcodeType;
            if (fnGetTranscodeInformation(anystreamJobID, out transcodeJobID, out tsmJobID, out partialJobID, out transcodeType, out anystreamXML, out notifyAddr))
            {
                // 如果可以從 TBTRANSCODE 找出對應的資訊，就先做 DB_UPDATE 再做 SEND_POST 的動作
                objTranscodeService.UpdateTranscodeStatus(transcodeJobID, tsmJobID, partialJobID, anystreamJobID, transcodeJobStatus);
                objTranscodeService.SendTranscodeJobHttpMessage(notifyAddr, transcodeJobID.ToString(), transcodeJobStatus.ToString(), "FINISH");
            }
            else
            {
                // 找不到對應的 AnsID，就記錄下來，但還要繼續後面的(解鎖)動作
                string errMsg = string.Format("Can't Get TransocdeData by AnystreamJobID: {0}", anystreamJobID);
                MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
            }

            // 解鎖 (如果有用到的話)
            string VTRName = string.Empty;
            if (transcodeType == ServiceTranscode.TRANSCODE_TYPE.NOT_AVAILABLE || transcodeType == ServiceTranscode.TRANSCODE_TYPE.TAPE)
            {
                if (!objTranscodeService.FindTranscodeVTR(anystreamXML, out VTRName))
                    MAM_PTS_DLL.Log.AppendTrackingLog("WSASC/Handler_AnsNotifyReceiver/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "Got Error when FindTranscodeVTR, ansID = " + anystreamJobID);

                if (!string.IsNullOrEmpty(VTRName))
                    if (!objTranscodeService.UnlockVTR(VTRName))
                        MAM_PTS_DLL.Log.AppendTrackingLog("WSASC/Handler_AnsNotifyReceiver/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "Got Error when UnlockVTR, VTR = " + VTRName + ", ansID = " + anystreamJobID);
            }
        }

        // 解析 Anystream 的 HTTP POST 回傳
        private bool fnParseAnystreamReturnPost(Stream inputStream, out long ansID, out string jobStatus)
        {
            string fsPROGRAM = MAM_PTS_DLL.ConvertFormat.Combine_TraceLogFSProgram(SYSCODE, System.Reflection.MethodBase.GetCurrentMethod().Name);

            jobStatus = string.Empty;
            ansID = -1;

            // 讀入資料
            string inputData = string.Empty;
            try
            {
                StreamReader sr = new StreamReader(inputStream);
                inputData = sr.ReadToEnd();
                sr.Close();
            }
            catch (Exception ex)
            {
                string errMsg = string.Concat("讀入HTTP回傳的資料流錯誤：錯誤訊息->", ex.ToString());
                MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return false;
            }

            // 解析回傳的XML內容
            XmlDocument xmlDoc = new XmlDocument();
            XmlNode objXmlNode;
            try
            {
                xmlDoc.LoadXml(inputData);
            }
            catch
            {
                MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Concat("Receive unexpected http-post content: ", inputData));
                return false;
            }

            objXmlNode = xmlDoc.SelectSingleNode("/job-status/status");
            if (objXmlNode == null)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.DEBUG, string.Concat("Can't fine node /job-status/status in http-post content: ", inputData));
                return false;
            }
            jobStatus = objXmlNode.InnerText;

            objXmlNode = xmlDoc.SelectSingleNode("/job-status/id");
            if (objXmlNode == null)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.DEBUG, string.Concat("Can't fine node /job-status/id in http-post content: ", inputData));
                return false;
            }
            if (!Int64.TryParse(objXmlNode.InnerText, out ansID))
            {
                MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.DEBUG, string.Concat("Can't invalid AnsJobID = ", objXmlNode.InnerText));
                return false;
            }

            // 將輸入的資料寫入資料表 (如果失敗了，不會影響到正常流程)
            fnInsertAnystreamReturnXml(ansID, inputData);
            // 原本是用 StoreProcedure，但因為字數太長，所以只能自己寫入
            //Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            //sqlParameters.Add("FNJOB_ID", ansID.ToString());
            //sqlParameters.Add("FSCONTENT", inputData);
            //MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TRANSCODE_RECV_LOG", sqlParameters, "system");

            return true;
        }

        // 利用 AnystreamJobID 找出相關的轉檔資訊
        private bool fnGetTranscodeInformation(long anystreamJobID, out long transcodeJobID, out long tsmJobID, out long partialJobID, out ServiceTranscode.TRANSCODE_TYPE transcodeType, out string anystreamXML, out string notifyAddress)
        {
            transcodeJobID = -1;
            tsmJobID = -1;
            partialJobID = -1;
            transcodeType = ServiceTranscode.TRANSCODE_TYPE.NOT_AVAILABLE;
            anystreamXML = string.Empty;
            notifyAddress = string.Empty;

            // SQL參數
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FNANYSTREAM_ID", anystreamJobID.ToString());

            // 讀取資料庫回傳的內容
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBTRANSCODE_INFO_FNANYSTREAM_ID", source, out resultData) || resultData.Count == 0)
                return false;

            // 理論上只會有一筆，所以只取第1筆就好
            transcodeJobID = Int64.Parse(resultData[0]["FNTRANSCODE_ID"]);
            tsmJobID = Int64.Parse(resultData[0]["FNTSMDISPATCH_ID"]);
            partialJobID = Int64.Parse(resultData[0]["FNPARTIAL_ID"]);
            transcodeType = (ServiceTranscode.TRANSCODE_TYPE)Int32.Parse(resultData[0]["FNTYPE"]);
            anystreamXML = resultData[0]["FSANYSTREAM_XML"];
            notifyAddress = resultData[0]["FSNOTIFY_ADDRESS"];

            return true;
        }

        /// <summary>
        /// 這邊要另外寫一個 SQL Insert 的方法，因為傳入的資料太大，超過 4000 字，Store Procedure 會自動截斷
        /// </summary>
        /// <param name="jobID"></param>
        /// <param name="xmlData"></param>
        /// <returns></returns>
        private bool fnInsertAnystreamReturnXml(long jobID, string xmlData)
        {
            int effectedCount = 0;

            try
            {
                SqlConnection connObj = new SqlConnection(MAM_PTS_DLL.DbAccess.sqlConnStr);
                SqlCommand cmdObj = new SqlCommand(string.Empty, connObj);
                connObj.Open();
                cmdObj.CommandText = "INSERT INTO TBTRANSCODE_RECV_LOG(FDTIME,FNJOB_ID,FSCONTENT) VALUES(@FDTIME,@FNJOB_ID,@FSCONTENT)";
                cmdObj.Parameters.AddWithValue("@FDTIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                cmdObj.Parameters.AddWithValue("@FNJOB_ID", jobID.ToString());
                cmdObj.Parameters.AddWithValue("@FSCONTENT", xmlData.ToString());
                effectedCount = cmdObj.ExecuteNonQuery();
                connObj.Close();
                connObj.Dispose();
            }
            catch (Exception ex)
            {
                string errMsg = string.Concat("Got Error when InsertDB, jobID = ", jobID, ", errMsg = ", ex.ToString());
                MAM_PTS_DLL.Log.AppendTrackingLog("WSASC/fnInsertAnystreamReturnXml", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return false;
            }

            if (effectedCount == 1)
                return true;
            else
                return false;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        #region SomeData
        /*  之前取得的 HTTP POST 回傳詳細內容
    //Sample01
    //<job-status state="new">
    //<id>205</id>
    //<status>Completed</status>
    //<author></author>
    //<title>william tape test</title>
    //<copyright></copyright>
    //<description></description>
    //<keywords></keywords>
    //<rating>none</rating>
    //<priority>2</priority>
    //<submit-time>2011-01-11 19:21:07</submit-time>
    //<complete-time>2011-01-11 20:25:41</complete-time>
    //<user-data-job><submit-machine>ECS1</submit-machine><job-profile>PTS WMV JOB2</job-profile><source-clip></source-clip><start-timecode>01;02;45;00</start-timecode><stop-timecode>01;02;59;00</stop-timecode><source-inpoint>00:00:00.000</source-inpoint><source-outpoint>00:00:00.000</source-outpoint><description></description><keywords></keywords><audience-rating>none</audience-rating><basename>william tape test</basename></user-data-job><task-status><id>773</id><jobid>205</jobid><user-data-task><display-name>LCS03-CH1</display-name><task-profile>Preprocessor Default</task-profile></user-data-task><status>Succeeded</status><type>capturePrefilter</type><percent>100</percent><elapsed-seconds>13</elapsed-seconds><start-time>2011-01-11 20:25:17</start-time><stop-time>2011-01-11 20:25:40</stop-time><fail-token></fail-token><fail-message></fail-message><node-host>LCS03</node-host><accounting><worker-type>prefilter both</worker-type><input-type>tape</input-type><start-timecode>01;02;45;00</start-timecode><stop-timecode>01;02;59;00</stop-timecode><source-duration>14.014</source-duration><duration>14.014</duration><source-width>720</source-width><source-height>480</source-height><source-fps>29.97</source-fps><source-sample-rate>48000</source-sample-rate><source-num-audio-channels>2</source-num-audio-channels></accounting></task-status><task-status><id>774</id><jobid>205</jobid><user-data-task><task-profile>PTS WMV2</task-profile><display-name>microsoft</display-name><clip-location>\\10.13.231.100\agility\WilliamTestOutput\HelloWorld2.wmv</clip-location></user-data-task><status>Succeeded</status><type>microsoft</type><percent>100</percent><elapsed-seconds>23</elapsed-seconds><start-time>2011-01-11 20:25:17</start-time><stop-time>2011-01-11 20:25:41</stop-time><fail-token>MS_CLIP_PROPERTY MS_WARNING</fail-token><fail-message>Clip author not specified in job
    //Microsoft worker warning: The audio stream terminated prematurely: expected (14.014) secs., actual (14.013) secs</fail-message><node-host>LCS03</node-host><accounting><worker-type>wm both</worker-type><max-fps>29.97</max-fps><max-width>320</max-width><max-height>240</max-height><duration>14.013</duration><max-video-bitrate>1500000</max-video-bitrate><max-audio-bitrate>128000</max-audio-bitrate><video-codec>Windows Media Video V9</video-codec><audio-codec>Windows Media Audio V9</audio-codec><output-file-name>\\10.13.231.100\agility\WilliamTestOutput\HelloWorld2.wmv</output-file-name><output-file-size>2953027</output-file-size><user-data></user-data></accounting></task-status><live-worker></live-worker><reserved-duration>0</reserved-duration><completion-reason>NotComplete</completion-reason><timed-job-id>0</timed-job-id></job-status>

    //  Sample02
    //    <job-status state="deleted">
    //    <id>205</id>
    //    <status>failed</status>
    //    <author></author>
    //    <title>william tape test</title>
    //    <copyright></copyright>
    //    <description></description>
    //    <keywords></keywords>
    //    <rating>none</rating>
    //    <priority>2</priority>
    //    <submit-time>2011-01-11 19:21:07</submit-time>
    //    <complete-time>2011-01-11 20:35:29</complete-time><user-data-job><submit-machine>ECS1</submit-machine><job-profile>PTS WMV JOB2</job-profile><source-clip></source-clip><start-timecode>01;02;45;00</start-timecode><stop-timecode>01;02;59;00</stop-timecode><source-inpoint>00:00:00.000</source-inpoint><source-outpoint>00:00:00.000</source-outpoint><description></description><keywords></keywords><audience-rating>none</audience-rating><basename>william tape test</basename></user-data-job><task-status><id>773</id><jobid>205</jobid><user-data-task><display-name>LCS03-CH1</display-name><task-profile>Preprocessor Default</task-profile></user-data-task><status>UserStopped</status><type>capturePrefilter</type><percent>0</percent><elapsed-seconds>0</elapsed-seconds><start-time>2011-01-11 20:35:13</start-time><stop-time>2011-01-11 20:35:24</stop-time><fail-token>ECS_TASKKILL</fail-token><fail-message>[ECS_TASKKILL] ECS killed task 911. Reason = UserStopped.</fail-message><node-host>LCS03</node-host><accounting></accounting></task-status><task-status><id>774</id><jobid>205</jobid><user-data-task><task-profile>PTS WMV2</task-profile><display-name>microsoft</display-name><clip-location>\\10.13.231.100\agility\WilliamTestOutput\HelloWorld2.wmv</clip-location></user-data-task><status>ConditionNotMet</status><type>microsoft</type><percent>0</percent><elapsed-seconds>0</elapsed-seconds><start-time>2011-01-11 20:35:14</start-time><stop-time>2011-01-11 20:35:29</stop-time><fail-token>MS_CLIP_PROPERTY ECS_TASKKILL ECS_TASKKILL</fail-token><fail-message>Clip author not specified in job
    //    [ECS_TASKKILL] ECS killed task 912. Reason = UserStopped.
    //    [ECS_TASKKILL] ECS killed task 912. Reason = ConditionNotMet.</fail-message><node-host>LCS03</node-host><accounting></accounting></task-status><live-worker></live-worker><reserved-duration>0</reserved-duration><completion-reason>NotComplete</completion-reason><timed-job-id>0</timed-job-id></job-status>

    //Sample03
    //<job-status state="new">
    //<id>206</id>
    //<status>failed</status>
    //<author></author>
    //<title>william tape test</title>
    //<copyright></copyright>
    //<description></description>
    //<keywords></keywords>
    //<rating>none</rating>
    //<priority>2</priority>
    //<submit-time>2011-01-11 20:37:21</submit-time>
    //<complete-time>2011-01-11 20:37:28</complete-time><user-data-job><submit-machine>ECS1</submit-machine><job-profile>PTS WMV JOB2</job-profile><source-clip></source-clip><start-timecode>01;02;45;00</start-timecode><stop-timecode>01;02;59;00</stop-timecode><source-inpoint>00:00:00.000</source-inpoint><source-outpoint>00:00:00.000</source-outpoint><description></description><keywords></keywords><audience-rating>none</audience-rating><basename>william tape test</basename></user-data-job><task-status><id>776</id><jobid>206</jobid><user-data-task><display-name>LCS03-CH1</display-name><task-profile>Preprocessor Default</task-profile></user-data-task><status>UserStopped</status><type>capturePrefilter</type><percent>0</percent><elapsed-seconds>0</elapsed-seconds><start-time>2011-01-11 20:37:21</start-time><stop-time>2011-01-11 20:37:28</stop-time><fail-token>ECS_TASKKILL</fail-token><fail-message>[ECS_TASKKILL] ECS killed task 914. Reason = UserStopped.</fail-message><node-host>LCS03</node-host><accounting></accounting></task-status><task-status><id>777</id><jobid>206</jobid><user-data-task><task-profile>PTS WMV2</task-profile><display-name>microsoft</display-name><clip-location>\\10.13.231.100\agility\WilliamTestOutput\HelloWorld2.wmv</clip-location></user-data-task><status>ConditionNotMet</status><type>microsoft</type><percent>-1</percent><elapsed-seconds>5</elapsed-seconds><start-time>2011-01-11 20:37:21</start-time><stop-time>2011-01-11 20:37:28</stop-time><fail-token>MS_CLIP_PROPERTY ECS_TASKKILL ECS_TASKKILL</fail-token><fail-message>Clip author not specified in job
    //[ECS_TASKKILL] ECS killed task 915. Reason = UserStopped.
    //[ECS_TASKKILL] ECS killed task 915. Reason = ConditionNotMet.</fail-message><node-host>LCS03</node-host><accounting></accounting></task-status><live-worker></live-worker><reserved-duration>0</reserved-duration><completion-reason>NotComplete</completion-reason><timed-job-id>0</timed-job-id></job-status>
 */

        /*  解析HTTP POST的內容
            //sw.WriteLine(context.Request.ContentType.ToString());
            // text/plain; charset=UTF-8

            //sw.WriteLine(req.ContentLength.ToString());
            // 3083

            //sw.WriteLine(context.Request.HttpMethod);
            // POST

            //sw.WriteLine(context.Request.QueryString);
            // ""

            //sw.WriteLine(context.Request.RequestType);
            // POST

            //sw.WriteLine(context.Request.Headers);
            // Content-Length=3083&Content-Type=text%2fplain%3b+charset%3dUTF-8&Accept=image%2fgif%2c+image%2fx-xbitmap%2c+image%2fjpeg%2c+image%2fpjpeg%2c+*%2f*&Host=10.13.231.99%3a80

            //sw.WriteLine(req.TotalBytes.ToString());
            // 3083
        */
        #endregion
    }
}
