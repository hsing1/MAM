﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.WS.WSASC
{
    /// <summary>
    /// Handler_ParNotifyReceiver 的摘要描述
    /// </summary>
    public class Handler_ParNotifyReceiver : IHttpHandler
    {
        const string SYSCODE = "ASC";

        /// <summary>
        /// 處理傳入的HTTP Request
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            string fsPROGRAM = MAM_PTS_DLL.ConvertFormat.Combine_TraceLogFSProgram(SYSCODE, System.Reflection.MethodBase.GetCurrentMethod().Name);

            HttpRequest req = context.Request;

            // 解析 HTTP POST 訊息，並找出 PartialID
            // 但如果連 Parse Http Post 都出問題了，所以沒辦法去解析內容，更別提對「目標」發訊息了

            // 如果無法解析，ParseTsmPostMessage會自動記錄錯誤的內容
            long partialJobID;
            WSASC_PartialService.TrasncodePartialJobStatus partialJobStatus;
            string partialJobMessage;

            WSASC_PartialService objWSASC_PartialService = new WSASC_PartialService();
            bool flagParse = objWSASC_PartialService.ParsePartialPostMessage(req.InputStream, out partialJobID, out partialJobStatus, out partialJobMessage);
            objWSASC_PartialService.Dispose();

            // William: 2012-04-16
            context.Response.Flush();
            context.Response.Close();

            if (!flagParse)
                return;     // 由 ParsePartialPostMessage 做記錄

            // 利用 PartialID 找出所有相關的轉檔資訊
            // 用 tsmJobID 做 PK 連接資料庫，找出該筆轉檔任務的相關資訊 (查詢要傳給ANS的內容)
            // 同上，如果抓不出該任務的內容，也沒辦法發訊息
            long transcodeJobID, tsmJobID;
            ServiceTranscode.TRANSCODE_STATUS transcodeJobStatus = ServiceTranscode.TRANSCODE_STATUS.ANSPROCESS;
            ServiceTranscode.TRANSCODE_TYPE transcodeJobType;
            string resolution, anystreamXML, notifyAddress;
            bool isNativeFormat, isFullRetrieve;
            string priority;
            if (!fnGetTranscodeInformation(partialJobID, out transcodeJobID, out tsmJobID, out transcodeJobType, out resolution, out isNativeFormat, out isFullRetrieve, out anystreamXML, out notifyAddress, out priority))
            {
                string errMsg = string.Format("fnGetTranscodeInformation() PartialJobID = {0}", partialJobID);
                MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return;
            }

            MAM_PTS_DLL.SysConfig objSysConfig = new MAM_PTS_DLL.SysConfig();
            string sysConfig_SupportPartialHD = objSysConfig.sysConfig_Read("/ServerConfig/TRANSCODE_Config/SupportPartialHD");
            bool supportPartialHD = false;
            if (sysConfig_SupportPartialHD == "Y")
                supportPartialHD = true;

            ServiceTranscode objTranscodeService = new ServiceTranscode();

            //MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, string.Format("{0}/{1}/{2}/{3}/{4}/{5}/{6}", transcodeJobID, tsmJobID, partialJobID, transcodeJobType, isNativeFormat, isFullRetrieve, notifyAddress));

            // 這邊，因為沒辦法幫 Anystream 偽造一個 JobID，所以只好做的比較複雜
            // 如果 isNative = 'Y' 且 isFull = 'Y' 的時候，可以直接中止程序
            // 如果 isNative = 'Y' 且 Type = 'SD' 的時候，可以直接中止程序
            // 如果 isNative = 'Y' 且 Type = 'HD' 且 sysConfig.Read('SupportPartialHD') = 'Y' ，可以直接中止程序
            if ((transcodeJobType == ServiceTranscode.TRANSCODE_TYPE.FILE && isNativeFormat && isFullRetrieve) ||
                (transcodeJobType == ServiceTranscode.TRANSCODE_TYPE.FILE && isNativeFormat && resolution == "SD") ||
                (transcodeJobType == ServiceTranscode.TRANSCODE_TYPE.FILE && isNativeFormat && resolution == "HD" && supportPartialHD))
            {
                // 這邊可以中止程序了：更新資料庫 + HTTP 通知(通知註冊於資料表的位址)
                if (partialJobStatus == WSASC_PartialService.TrasncodePartialJobStatus.FINISH)
                {
                    transcodeJobStatus = ServiceTranscode.TRANSCODE_STATUS.FINISH;
                    objTranscodeService.UpdateTranscodeStatus(transcodeJobID, tsmJobID, partialJobID, -1, transcodeJobStatus);
                    objTranscodeService.SendTranscodeJobHttpMessage(notifyAddress, transcodeJobID.ToString(), transcodeJobStatus.ToString(), "FINISH");
                }
                else
                {
                    transcodeJobStatus = ServiceTranscode.TRANSCODE_STATUS.PARERROR;
                    objTranscodeService.UpdateTranscodeStatus(transcodeJobID, tsmJobID, partialJobID, -1, transcodeJobStatus);
                    objTranscodeService.SendTranscodeJobHttpMessage(notifyAddress, transcodeJobID.ToString(), transcodeJobStatus.ToString(), "Error: " + partialJobStatus.ToString());
                }
                return;
            }

            // 其餘的，發送轉檔到 Anystream 吧
            int anystreamJobID;
            if (!fnDoTranscode(anystreamXML, out anystreamJobID))
            {
                transcodeJobStatus = ServiceTranscode.TRANSCODE_STATUS.ANSERROR;
                objTranscodeService.UpdateTranscodeStatus(transcodeJobID, tsmJobID, partialJobID, -1, transcodeJobStatus);
                objTranscodeService.SendTranscodeJobHttpMessage(notifyAddress, transcodeJobID.ToString(), transcodeJobStatus.ToString(), "Error: Can't register anystream job.");
                return;
            }

            // 記錄 Anystream 回傳的 JobID
            objTranscodeService.UpdateTranscodeStatus(transcodeJobID, tsmJobID, partialJobID, anystreamJobID, transcodeJobStatus);

            // 依照資料庫的內容去更新優先權，就算失敗了，也不會有太大的影響，所以放在 UpdateStatus 的後面
            System.Threading.Thread.Sleep(5000);
            //MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Concat("TEMP ChangePriority: ", anystreamJobID, " -> ", priority));
            ServiceAnystream objServiceAnystream = new ServiceAnystream();
            int intPriority;
            if (Int32.TryParse(priority, out intPriority))
                objServiceAnystream.DoChangePriority(anystreamJobID, intPriority);

            // 打完收工
        }

        // 利用 PartialID 找出相關的轉檔資訊
        private bool fnGetTranscodeInformation(
            long partialJobID, out long transcodeJobID, out long tsmJobID, out ServiceTranscode.TRANSCODE_TYPE transcodeJobType, out string resolution, out bool isNativeFormat, out bool isFullRetrieve, out string anystreamXML, out string notifyAddress, out string priority)
        {
            transcodeJobID = -1;
            tsmJobID = -1;
            transcodeJobType = ServiceTranscode.TRANSCODE_TYPE.NOT_AVAILABLE;
            resolution = string.Empty;
            isNativeFormat = false;
            isFullRetrieve = false;
            anystreamXML = string.Empty;
            notifyAddress = string.Empty;
            priority = string.Empty;

            // SQL參數
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FNPARTIAL_ID", partialJobID.ToString());

            // 讀取資料庫回傳的內容
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBTRANSCODE_INFO_FNPARTIAL_ID", source, out resultData) || resultData.Count == 0)
                return false;

            // 理論上只會有一筆，所以只取第1筆就好
            transcodeJobID = Int64.Parse(resultData[0]["FNTRANSCODE_ID"]);
            tsmJobID = Int64.Parse(resultData[0]["FNTSMDISPATCH_ID"]);
            transcodeJobType = (ServiceTranscode.TRANSCODE_TYPE)Int32.Parse(resultData[0]["FNTYPE"]);
            resolution = resultData[0]["FSOUTPUT_RESOLUTION"];
            if (resultData[0]["FCNATIVE_FORMAT"] == "Y")
                isNativeFormat = true;
            if (resultData[0]["FCFULL_RETRIEVE"] == "Y")
                isFullRetrieve = true;
            anystreamXML = resultData[0]["FSANYSTREAM_XML"];
            notifyAddress = resultData[0]["FSNOTIFY_ADDRESS"];
            if (string.IsNullOrEmpty(resultData[0]["FNANYSTREAM_PRIORITY"]))
                priority = "20";
            else
                priority = resultData[0]["FNANYSTREAM_PRIORITY"];

            return true;
        }

        // 向 Anystream 註冊轉檔任務
        private bool fnDoTranscode(string anystreamXml, out int anystreamJobID)
        {
            string fsPROGRAM = MAM_PTS_DLL.ConvertFormat.Combine_TraceLogFSProgram(SYSCODE, System.Reflection.MethodBase.GetCurrentMethod().Name);

            anystreamJobID = -1;
            try
            {
                ServiceAnystream obj = new ServiceAnystream();
                anystreamJobID = obj.DoIngest(anystreamXml);
                if (anystreamJobID == -1)
                    throw new Exception("執行DoIngest時，取得回傳ID為-1，請確認是否為ANYSTREAM的問題");
                return true;
            }
            catch (Exception ex)
            {
                string errorMsg = string.Concat("執行DoIngst時發生問題：", ex.ToString(), Environment.NewLine, "原始內容：", anystreamXml);
                MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errorMsg);
                return false;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}