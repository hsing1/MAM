﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.WS.WSASC
{
    public class ClassTranscodeJob
    {
        public string TranscodeJobID;
        public string TranscodeJobStatus;
        public string TsmJobID;
        public string TsmJobStatus;
        public string PartialJobID;
        public string PartialJobStatus;
        public string AnsJobID;
        public string AnsJobPriority;
        public string AnsJobStatus;
        public string AnsXml;
        public string AnsErrorMessage;
        public string RequestTime;
        public string Note;

        public string dbStatus;

        public ServiceTranscode.TRANSCODE_STATUS TranscodeStatus;

        // 依據不同的狀態，去改變 Status 及 ID 的說明
        public void ParseTranscodeJobStatus()
        {
            switch ((ServiceTranscode.TRANSCODE_STATUS)Int32.Parse(dbStatus))
            {
                case ServiceTranscode.TRANSCODE_STATUS.INITIAL:
                    TranscodeJobStatus = "檢查狀態";
                    TsmJobID = string.Empty;
                    PartialJobID = string.Empty;
                    TsmJobStatus = string.Empty;
                    PartialJobStatus = string.Empty;
                    AnsJobID = string.Empty;
                    AnsJobStatus = string.Empty;
                    TranscodeStatus = ServiceTranscode.TRANSCODE_STATUS.INITIAL;
                    break;

                case ServiceTranscode.TRANSCODE_STATUS.TSMERROR:
                    TranscodeJobStatus = "TSM處理錯誤";
                    if (TsmJobID == "-1")
                        TsmJobStatus = "註冊TSM服務時發生錯誤";
                    else
                        TsmJobStatus = "TSM執行時發生錯誤";
                    PartialJobID = string.Empty;
                    PartialJobStatus = string.Empty;
                    AnsJobID = string.Empty;
                    AnsJobStatus = string.Empty;
                    TranscodeStatus = ServiceTranscode.TRANSCODE_STATUS.TSMERROR;
                    break;

                case ServiceTranscode.TRANSCODE_STATUS.TSMPROCESS:
                    TranscodeJobStatus = "TSM處理中";
                    PartialJobID = string.Empty;
                    PartialJobStatus = string.Empty;
                    AnsJobID = string.Empty;
                    AnsJobStatus = string.Empty;
                    TranscodeStatus = ServiceTranscode.TRANSCODE_STATUS.TSMPROCESS;
                    break;

                case ServiceTranscode.TRANSCODE_STATUS.PARERROR:
                    TranscodeJobStatus = "PARTIAL處理錯誤";
                    if (PartialJobID == "-1")
                        PartialJobStatus = "註冊PARTIAL服務時發生錯誤";
                    else
                        PartialJobStatus = "PARTIAL執行時發生錯誤";
                    AnsJobID = string.Empty;
                    AnsJobStatus = string.Empty;
                    TranscodeStatus = ServiceTranscode.TRANSCODE_STATUS.PARERROR;
                    break;

                case ServiceTranscode.TRANSCODE_STATUS.PARPROCESS:
                    TranscodeJobStatus = "TSM處理中";
                    AnsJobID = string.Empty;
                    AnsJobStatus = string.Empty;
                    TranscodeStatus = ServiceTranscode.TRANSCODE_STATUS.PARPROCESS;
                    break;

                case ServiceTranscode.TRANSCODE_STATUS.ANSERROR:
                    if (TsmJobID == "-1")
                    {
                        TsmJobID = string.Empty;
                        TsmJobStatus = "無需使用TSM服務";
                    }
                    else
                        TsmJobStatus = "完成";    // 因為在 ANSERROR 的狀態，要不就是不用做TSM、要不就是TSM完成

                    if (PartialJobID == "-1")
                    {
                        PartialJobID = string.Empty;
                        PartialJobStatus = "無需使用PARTIAL服務";
                    }

                    if (AnsJobID == "-1")
                        AnsJobStatus = "註冊Anystream服務時發生錯誤";
                    else
                        AnsJobStatus = "Anystream處理錯誤";

                    TranscodeJobStatus = "ANS處理錯誤";
                    TranscodeStatus = ServiceTranscode.TRANSCODE_STATUS.ANSERROR;
                    break;

                case ServiceTranscode.TRANSCODE_STATUS.ANSPROCESS:
                    if (TsmJobID == "-1")
                    {
                        TsmJobID = string.Empty;
                        TsmJobStatus = "無需使用TSM服務";
                    }
                    else
                        TsmJobStatus = "完成";    // 因為在 ANSERROR 的狀態，要不就是不用做TSM、要不就是TSM完成

                    if (PartialJobID == "-1")
                    {
                        PartialJobID = string.Empty;
                        PartialJobStatus = "無需使用PARTIAL服務";
                    }

                    TranscodeJobStatus = "ANS處理中";
                    TranscodeStatus = ServiceTranscode.TRANSCODE_STATUS.ANSPROCESS;
                    break;

                case ServiceTranscode.TRANSCODE_STATUS.SYSERROR:
                    // 
                    if (TsmJobID == "-1")
                    {
                        TsmJobStatus = "註冊TSM服務時發生錯誤";
                    }
                    else if (TsmJobID == "-10")
                    {
                        TsmJobID = string.Empty;
                        TsmJobStatus = "無需使用TSM服務";
                    }
                    else
                    {
                        // 因為在 ANSERROR 的狀態，要不就是不用做TSM、要不就是TSM完成
                        TsmJobStatus = "完成";
                    }
                    // 
                    // .................
                    // 
                    if (AnsJobID == "-1")
                        AnsJobStatus = "註冊Anystream服務時發生錯誤";
                    else
                        AnsJobStatus = "發生錯誤";
                    // 
                    TranscodeJobStatus = "系統錯誤";
                    TranscodeStatus = ServiceTranscode.TRANSCODE_STATUS.SYSERROR;
                    break;

                case ServiceTranscode.TRANSCODE_STATUS.FINISH:
                    // 
                    if (TsmJobID == "-1")
                    {
                        TsmJobID = string.Empty;
                        TsmJobStatus = string.Empty;
                    }
                    else
                        TsmJobStatus = "完成";
                    // 
                    if (PartialJobID == "-1")
                    {
                        PartialJobID = string.Empty;
                        PartialJobID = string.Empty;
                    }
                    else
                        PartialJobStatus = "完成";
                    // 
                    AnsJobStatus = "完成";
                    // 
                    TranscodeJobStatus = "完成";
                    TranscodeStatus = ServiceTranscode.TRANSCODE_STATUS.FINISH;
                    break;

                default:
                    TranscodeJobStatus = string.Concat("未確定的狀態(", dbStatus, ")");
                    TsmJobStatus = string.Empty;
                    AnsJobStatus = string.Empty;
                    TranscodeStatus = ServiceTranscode.TRANSCODE_STATUS.FINISH;
                    break;
            }
        }
    }
}