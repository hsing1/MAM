﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Text;
using System.IO;
using System.Xml;

namespace PTS_MAM3.Web.WS.WSASC
{
    /// <summary>
    /// WSASC_PartialService 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSASC_PartialService : System.Web.Services.WebService
    {
        public enum TrasncodePartialJobStatus { INITIAL, PROCESS, FINISH, SOURCE_NOT_EXIST, ERROR };

        // APP_Partial專用的 ASHX 位址
        private static string TranscodePartialHandler = string.Empty;

        /// <summary>
        /// 註冊 Partial 用的資訊，目前只針對 MAM 的轉檔任務有效
        /// </summary>
        /// <param name="sourcePath">如果為空白，就不會進行 Partial 的處理動作，直接完成</param>
        /// <param name="targetPath">如果為空白，就不會進行 Partial 的處理動作，直接完成</param>
        /// <param name="inPoint">如果為空白，就會進行完整的檔案複製動作</param>
        /// <param name="outPoint">如果為空白，就會進行完整的檔案複製動作</param>
        /// <returns></returns>
        [WebMethod]
        public long RegisterPartialService(string sourcePath, string targetPath, string inPoint, string outPoint)
        {
            TrasncodePartialJobStatus jobStatus = TrasncodePartialJobStatus.INITIAL;

            // 做些簡單的檢查，但 ... 放一個空白字串的後門 =_=
            if (!string.IsNullOrEmpty(sourcePath) && !MAM_PTS_DLL.CheckFormat.CheckFormat_UncPath(sourcePath))
                return -1;
            if (!string.IsNullOrEmpty(targetPath) && !MAM_PTS_DLL.CheckFormat.CheckFormat_UncPath(targetPath))
                return -1;

            // 捷徑，William: 2012-04-16 無捷徑!
            //if (string.IsNullOrEmpty(sourcePath) || string.IsNullOrEmpty(targetPath))
            //    jobStatus = TrasncodePartialJobStatus.FINISH;

            // 將 Partial 參數註冊到資料表中
            long partialJobID;
            if (!insertPartialJobData(out partialJobID, sourcePath, targetPath, inPoint, outPoint, (int)jobStatus))
                return -1;

            // 捷徑，William: 2012-04-16 無捷徑!
            //if (jobStatus == TrasncodePartialJobStatus.FINISH)
            //    SendPartialPostMessage(partialJobID.ToString(), jobStatus, "Finish: No Need To Execute.");

            return partialJobID;
        }

        /// <summary>
        /// 組合並發送 Partial 服務的 HTTP POST
        /// </summary>
        /// <param name="jobID"></param>
        /// <param name="jobStatus"></param>
        /// <param name="jobMsg"></param>
        /// <returns></returns>
        [WebMethod]
        public bool SendPartialPostMessage(string jobID, TrasncodePartialJobStatus jobStatus, string jobMsg)
        {
            if (string.IsNullOrEmpty(TranscodePartialHandler))
                UpdatePartialHandler();

            return MAM_PTS_DLL.Protocol.SendHttpPostMessage(TranscodePartialHandler, CreatePartialPostMessage(jobID, jobStatus, jobStatus.ToString()));
        }

        // 盡量讓前端沒辦法知道XML結構
        public string CreatePartialPostMessage(string jobID, TrasncodePartialJobStatus jobStatus, string jobMsg)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<TRANSCODE_PARTIAL>");
            sb.AppendLine(string.Format("<PROCESS_ID>{0}</PROCESS_ID>", jobID));
            sb.AppendLine(string.Format("<PROCESS_RESULT>{0}</PROCESS_RESULT>", jobStatus.ToString()));
            sb.AppendLine(string.Format("<PROCESS_MESSAGE>{0}</PROCESS_MESSAGE>", jobMsg));
            sb.AppendLine("</TRANSCODE_PARTIAL>");
            return sb.ToString();
        }

        // 只有主機端會用到，所以暫時不成為 WebMethod
        public bool ParsePartialPostMessage(Stream inputStream, out long jobID, out TrasncodePartialJobStatus jobStatus, out string jobMsg)
        {
            // 預設值
            jobID = -1;
            jobStatus = TrasncodePartialJobStatus.ERROR;
            jobMsg = string.Empty;

            // 將 Stream 的內容讀出
            string inputData = string.Empty;
            try
            {
                StreamReader sr = new StreamReader(inputStream);
                inputData = sr.ReadToEnd();
                sr.Close();
            }
            catch (Exception ex)
            {
                string errMsg = string.Concat("讀入HTTP回傳的資料流發生錯誤：錯誤訊息->", ex.ToString());
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM_RecallService/ParsePartialPostMessage", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return false;
            }

            // 解析 xml 內容
            XmlDocument xdoc = new XmlDocument();
            XmlNode node = null;
            try
            {
                xdoc.LoadXml(inputData);

                //<TRANSCODE_PARTIAL>
                //    <PROCESS_ID>  </PROCESS_ID>
                //    <PROCESS_RESULT>  </PROCESS_RESULT>
                //    <PROCESS_MESSAGE>  </PROCESS_MESSAGE>
                //</TRANSCODE_PARTIAL>

                // 解析節點內容
                node = xdoc.SelectSingleNode("/TRANSCODE_PARTIAL/PROCESS_ID");
                if (node == null)
                    throw new Exception("Can't find node /TRANSCODE_PARTIAL/PROCESS_ID");
                if (!Int64.TryParse(node.InnerText, out jobID))
                    return false;

                // 
                node = xdoc.SelectSingleNode("/TRANSCODE_PARTIAL/PROCESS_RESULT");
                if (node == null)
                    throw new Exception("Can't find node /TRANSCODE_PARTIAL/PROCESS_RESULT");
                if (!Enum.IsDefined(typeof(TrasncodePartialJobStatus), node.InnerText))
                    throw new Exception(string.Format("Can't conver {0} to Enum TsmRecallJobResult", node.InnerText));
                jobStatus = (TrasncodePartialJobStatus)Enum.Parse(typeof(TrasncodePartialJobStatus), node.InnerText);

                // 
                node = xdoc.SelectSingleNode("/TRANSCODE_PARTIAL/PROCESS_MESSAGE");
                if (node == null)
                    throw new Exception("Can't find node /TRANSCODE_PARTIAL/PROCESS_MESSAGE");
                jobMsg = node.InnerText;
            }
            catch (Exception ex)
            {
                string errMsg = string.Concat("解析HTTP回傳的XML錯誤：原始資料->", inputData, "錯誤訊息->", ex.ToString());
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM_RecallService/ParsePartialPostMessage", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return false;
            }

            return true;
        }

        // 當很不幸的，如果 ashx 位址真的要換，但又不能重開 web service 時 ... 只好用這招了
        [WebMethod]
        public bool UpdatePartialHandler()
        {
            // 從設定檔中讀出資訊
            MAM_PTS_DLL.SysConfig objSysConfig = new MAM_PTS_DLL.SysConfig();
            TranscodePartialHandler = objSysConfig.sysConfig_Read("/ServerConfig/AshxHandler/PAR_ANS");
            // 檢查讀出的結果
            if (string.IsNullOrEmpty(TranscodePartialHandler))
                return false;
            // 回傳
            return true;
        }

        // 無用，請不要呼叫
        [WebMethod]
        public TrasncodePartialJobStatus GetPartialJobStatusList()
        {
            return TrasncodePartialJobStatus.ERROR;
        }

        private bool insertPartialJobData(out long jobID, string sourcePath, string targetPath, string inPoint, string outPoint, int jobStatus)
        {
            jobID = -1;

            /*
                @FCPARTIAL				CHAR(1),
                @FSSOURCE_PATH			VARCHAR(512),
                @FSDESTINATION_PATH		VARCHAR(512),
                @FSIN_POINT				VARCHAR(11),
                @FSOUT_POINT			VARCHAR(11),
                @FNSTATUS				SMALLINT
             */

            List<Dictionary<string, string>> resultData = null;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            if (string.IsNullOrEmpty(inPoint) || string.IsNullOrEmpty(outPoint))
                sqlParameters.Add("FCPARTIAL", "N");
            else
                sqlParameters.Add("FCPARTIAL", "Y");
            sqlParameters.Add("FSSOURCE_PATH", sourcePath);
            sqlParameters.Add("FSDESTINATION_PATH", targetPath);
            sqlParameters.Add("FSIN_POINT", inPoint);
            sqlParameters.Add("FSOUT_POINT", outPoint);
            sqlParameters.Add("FNSTATUS", jobStatus.ToString());

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_I_TBTRANSCODE_PARTIAL", sqlParameters, out resultData) || resultData.Count == 0)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSASC/WSASC_PartialService/insertPartialJobData", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "SP_I_TBTRANSCODE_PARTIAL Error");
                return false;
            }
            else
                jobID = Int64.Parse(resultData[0]["FNPARTIAL_ID"]);

            return true;
        }
    }
}
