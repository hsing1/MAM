﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Text;

namespace PTS_MAM3.Web.WS.WSASC
{
    public partial class DoIngestXml
    {
        // 轉檔的類型：FILE或TAPE
        public string TransCodeType { get; set; }
        // 是否為Partial轉檔(由使用者判斷)
        public string IsFullTranscode { get; set; }
        // 是否為原生格式轉檔(由使用者判斷)
        public string IsNativeFormat { get; set; }
        // 來源檔位置：可以是路徑或VTR名稱
        public string SourceFrom { get; set; }
        // 暫存的目標位置(當同時需要Partial及Anystream時才用的到)
        public string TempDestinationTo { get; set; }
        // 目的檔位置：必需是一個路徑(需含檔名)
        public string DestinationTo { get; set; }
        // 來源的解析度格式(SD/HD)
        public string SourceFormat { get; set; }
        // 目標的解析度格式(SD/HD)
        public string DestinationFormat { get; set; }
        // 轉檔用的設定檔完整路徑
        public string EncoderProfliePath { get; set; }
        // 起始時間碼
        public string StartTimecode { get; set; }
        // 結束時間碼
        public string EndTimecode { get; set; }
        // 轉檔動作結束時要通知的位址
        public string PostbackAddress { get; set; }     // Tricky: 這個是最終要丟給使用者的
        // 是否要產生低解檔
        public string CreateLowRes { get; set; }
        // 低解檔的產生路徑
        public string LowResDir { get; set; }
        // 是否要產生關鍵影格
        public string CaptureKeyframe { get; set; }
        // 是否要壓印LOGO
        public string DisplayLogo { get; set; }
        // LOGO的設定檔完整路徑
        public string LogoProfilePath { get; set; }
        // 針對這個轉檔的說明
        public string Note { get; set; }

        public bool NeedPartialProcess { get; set; }
        public bool NeedAnystreamProcess { get; set; }

        public string ErrorMessage { get; set; }
        public string InputXmlContent { get; set; }

        //2015/11/17 David.Sin HD送播檔不轉高解
        public string CreateHightRes { set; get; }
        //2015/11/17 David.Sin HD送播檔不轉高解

        // 要送出的轉檔項目及名稱(僅對於Anystream後台的顯示有差異)
        private static string displayJobProfileName;
        private static string displayTitle_Tape;
        private static string displayTitle_File;

        //2015/11/17 David.Sin HD送播檔不轉高解
        private static string displayTitle_HD;

        // 目的檔的目錄位置
        private string DestinationToDir { get; set; }
        // 目的檔的檔案名稱
        private string DestinationToFile { get; set; }

        // ======================================================
        public DoIngestXml()
        {
            MAM_PTS_DLL.SysConfig objSysConfig = new MAM_PTS_DLL.SysConfig();

            // 初始化一些變數
            if (string.IsNullOrEmpty(displayJobProfileName))
                displayJobProfileName = objSysConfig.sysConfig_Read("/ServerConfig/ANS_Config/DISPLAY_JOBNAME");
            if (string.IsNullOrEmpty(displayJobProfileName))
                displayJobProfileName = "D_MAM_JOB";        // 確保不會有任何錯誤
            if (string.IsNullOrEmpty(displayTitle_Tape))
                displayTitle_Tape = objSysConfig.sysConfig_Read("/ServerConfig/ANS_Config/DISPLAY_TITLE_TAPE");
            if (string.IsNullOrEmpty(displayTitle_Tape))
                displayTitle_Tape = "D_MAM_TAPE";           // 確保不會有任何錯誤
            if (string.IsNullOrEmpty(displayTitle_File))
                displayTitle_File = objSysConfig.sysConfig_Read("/ServerConfig/ANS_Config/DISPLAY_TITLE_FILE");
            if (string.IsNullOrEmpty(displayTitle_File))
                displayTitle_File = "D_MAM_FILE";           // 確保不會有任何錯誤
            if (string.IsNullOrEmpty(displayTitle_HD))
                displayTitle_HD = objSysConfig.sysConfig_Read("/ServerConfig/ANS_Config/DISPLAY_TITLE_HD");
            if (string.IsNullOrEmpty(displayTitle_HD))
                displayTitle_HD = "D_MAM_HD";           // 確保不會有任何錯誤
        }

        // ======================================================
        public bool ImportXML(string xmlData)
        {
            // 檢查是否為完整的XML格式
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                xmlDoc.LoadXml(xmlData);
            }
            catch (Exception ex)
            {
                this.ErrorMessage = string.Concat("Load Xml Error: ", ex.ToString(), Environment.NewLine, xmlData);
                return false;
            }

            // 寫入自己的屬性，其他的private函式就可以用了
            this.InputXmlContent = xmlData;

            // 解析所有的 Xml 內容，碰到錯馬上回傳
            return fnParseXmlData(xmlDoc);
        }

        // 匯出轉檔用的Xml字串 (包含 Partial 及 Anystream 所需要用到的參數)
        public bool ExportXML(out string rtnPartialString, out string rtnAnsXml)
        {
            rtnPartialString = string.Empty;
            rtnAnsXml = string.Empty;

            // 檢查物件屬性是否足以執行Export的動作
            if (!fnValidateAllAttributes())
                return false;

            // 依照現有邏輯判斷，是否需要執行 MXF_Partial 的流程 (this.NeedPartialProcess)
            bool needTempSpace = false;
            fnCheckNeedPartial(ref needTempSpace);

            // 將參數組合為 PartialMXF 要用的參數字串
            rtnPartialString = fnCombineExportPartialString(needTempSpace);

            // 將參數組合為 Anystream 用的 XML
            rtnAnsXml = fnCombineExportAnsXml(needTempSpace);

            return true;
        }

        // ======================================================
        // 依照現有邏輯判斷是否需要執行 MXF_Partial 的流程
        private void fnCheckNeedPartial(ref bool needTempSpace)
        {
            // 從這邊判斷到底需要經過哪些程序!
            // [輸出格式]SD/HD, [User]Partial/Full, [User]NativeFormat/Non-NativeFormat, 設定值1(/ServerConfig/TRANSCODE_Config/PartialNonNativeFormat), 設定值2(/ServerConfig/TRANSCODE_Config/SupportPartialHD)
            // 0. 當為影帶擷取時，強制不准使用 Partial 的相關程式
            // 1. SD + Full + NativeFormat         → NeedPartialProcess = true     (Tricky：會幫忙做搬檔動作)
            // 2. HD + Full + NativeFormat         → NeedPartialProcess = true     (Tricky：會幫忙做搬檔動作)
            // 3. SD + Full + Non-NativeFormat     → NeedPartialProcess = false    (直接送轉了)
            // 4. HD + Full + Non-NativeFormat     → NeedPartialProcess = false    (直接送轉了)
            // 5. SD + Partial + NativeFormat      → NeedPartialProcess = true     (但不用送轉)
            // 6. HD + Partial + NativeFormat      → 參考設定值2，決定 NeedPartialProcess 的結果
            // 7. SD + Partial + Non-NativeFormat  → 參考設定值1，決定 NeedPartialProcess 的結果
            // 8. HD + Partial + Non-NativeFormat  → 參考設定值2+1，決定 NeedPartialProcess 的結果

            // 先取出 config 的值
            bool config1 = false;           // PartialNonNativeFormat
            bool config2 = false;           // SupportPartialHD
            string config3 = string.Empty;  // TranscodeSharingDir

            // 未來考慮要不要做成 Cache，以減少 I/O 次數
            MAM_PTS_DLL.SysConfig objSysConfig = new MAM_PTS_DLL.SysConfig();
            if (objSysConfig.sysConfig_Read("/ServerConfig/TRANSCODE_Config/PartialNonNativeFormat") == "Y")
                config1 = true;
            if (objSysConfig.sysConfig_Read("/ServerConfig/TRANSCODE_Config/SupportPartialHD") == "Y")
                config2 = true;

            // 計算出暫存檔名(不論有沒有用到都先算出來)
            this.TempDestinationTo = string.Concat(objSysConfig.sysConfig_Read("/ServerConfig/TRANSCODE_Config/TranscodeSharingDir"), Guid.NewGuid(), System.IO.Path.GetExtension(this.SourceFrom));

            // 檢測邏輯
            if (this.TransCodeType == "TAPE")                                                                        // 0
                NeedPartialProcess = false;
            else if (this.IsFullTranscode == "Y" && this.IsNativeFormat == "Y")                                      // 1 & 2
                NeedPartialProcess = true;
            else if (this.IsFullTranscode == "Y" && this.IsNativeFormat == "N")                                      // 3 & 4
                NeedPartialProcess = false;
            else if (this.DestinationFormat == "SD" && this.IsFullTranscode == "N" && this.IsNativeFormat == "Y")    // 5
                NeedPartialProcess = true;
            else if (this.DestinationFormat == "HD" && this.IsFullTranscode == "N" && this.IsNativeFormat == "Y")    // 6
            {
                if (config2)
                {
                    NeedPartialProcess = true;
                    needTempSpace = true;
                }
                else
                    NeedPartialProcess = false;
            }
            else if (this.DestinationFormat == "SD" && this.IsFullTranscode == "N" && this.IsNativeFormat == "N")    // 7
            {
                if (config1)
                {
                    NeedPartialProcess = true;
                    needTempSpace = true;
                }
                else
                    NeedPartialProcess = false;
            }
            else if (this.DestinationFormat == "HD" && this.IsFullTranscode == "N" && this.IsNativeFormat == "N")    // 8
            {
                if (config2 && config1)
                {
                    NeedPartialProcess = true;
                    needTempSpace = true;
                }
                else
                    NeedPartialProcess = false;
            }
            else
            {
                string errMsg = string.Format("參數異常：{0}/{1}/{2}/{3}", this.DestinationFormat, this.IsFullTranscode, this.IsNativeFormat, config1, config2);
                MAM_PTS_DLL.Log.AppendTrackingLog("WS/WSASC/DoIngestXml/fnCheckNeedPartial", MAM_PTS_DLL.Log.TRACKING_LEVEL.WARNING, errMsg);
                NeedPartialProcess = false;     // 照理來說，不應該到這邊的=_=
            }
        }

        // 將參數組合為 PartialMXF 要用的參數字串
        private string fnCombineExportPartialString(bool needTempSpace)
        {
            // 如果不需 Partial 轉檔流程的話，也不用這些東西了
            if (!this.NeedPartialProcess)
                return string.Empty;

            // 預設的分隔符號為 '?'
            // 目前設定的字串內容為：SourceFrom ? DestinationTo ? StartTimecode ? EndTimecode
            StringBuilder sb = new StringBuilder();
            sb.Append(this.SourceFrom);
            sb.Append('?');
            if (needTempSpace)
                sb.Append(this.TempDestinationTo);
            else
                sb.Append(this.DestinationTo);
            sb.Append('?');
            sb.Append(this.StartTimecode.Replace(';', ':'));    // InPoint 與 OutPoint 的符號要改用 ':'
            sb.Append('?');
            sb.Append(this.EndTimecode.Replace(';', ':'));      // InPoint 與 OutPoint 的符號要改用 ':'
            return sb.ToString();
        }

        // 將參數組合為 Anystream 用的 XML
        private string fnCombineExportAnsXml(bool needTempSpace)
        {
            MAM_PTS_DLL.SysConfig objSysConfig = new MAM_PTS_DLL.SysConfig();
            string profile_pp_filepath = string.Empty;

            StringBuilder sb = new StringBuilder();

            // 組合要輸出的內容
            sb.AppendLine("<planner-submit>");

            // 轉檔項目的名稱，寫死也無所謂
            sb.AppendLine(string.Format("  <job-name>{0}</job-name>", displayJobProfileName));


            // 選擇要使用的 pp profile：當 HD->SD 時，要注意 pp 要改成 Crop 版本的
            if (TransCodeType == "TAPE")
            {
                sb.AppendLine(string.Format("  <meta-title>{0}</meta-title>", displayTitle_Tape));
                if (SourceFormat == "HD" && DestinationFormat == "SD")
                    profile_pp_filepath = objSysConfig.sysConfig_Read("/ServerConfig/ANS_Config/PROFILE_PP_TAPE_Crop");
                else
                    profile_pp_filepath = objSysConfig.sysConfig_Read("/ServerConfig/ANS_Config/PROFILE_PP_TAPE");
            }
            else
            {
                sb.AppendLine(string.Format("  <meta-title>{0}</meta-title>", displayTitle_File));
                if (SourceFormat == "HD" && DestinationFormat == "SD")
                    profile_pp_filepath = objSysConfig.sysConfig_Read("/ServerConfig/ANS_Config/PROFILE_PP_FILE_Crop");
                else
                    profile_pp_filepath = objSysConfig.sysConfig_Read("/ServerConfig/ANS_Config/PROFILE_PP_FILE");
            }
            sb.AppendLine(string.Concat("  <add-subprofile>", profile_pp_filepath, "</add-subprofile>"));

            //2015/11/17 David.Sin HD送播檔不轉高解
            if (string.IsNullOrEmpty(CreateHightRes) || CreateHightRes == "Y")
            {
                // 從前端來的原生格式，是沒有 ProfilePath 的
                // 但因為 HD 的原生格式，仍需要做轉檔 ... 
                // So 只好在這邊多做一點邏輯判斷了
                if (this.EncoderProfliePath == "NativeFormat" || this.EncoderProfliePath == "原生格式")
                {
                    if (this.SourceFormat == "HD")
                        this.EncoderProfliePath = objSysConfig.sysConfig_Read("/ServerConfig/ANS_Config/PROFILE_MAM_HD");
                    else
                        this.EncoderProfliePath = objSysConfig.sysConfig_Read("/ServerConfig/ANS_Config/PROFILE_MAM_MXF");
                }
                sb.AppendLine(@"  <add-subprofile>" + EncoderProfliePath + @"</add-subprofile>");
            }

            // 低解的部份(PartI)
            // 注意：WMV轉檔 與 產生低解 會相衝突，不可以同時使用
            //       -->但目前入庫時不會用WMV (但會用低解), 調用時不會用低解 (但有可能轉出WMV)，所以理論上是不會衝突的
            if (CreateLowRes == "Y")
            {
                // SD、HD的低解需用到不同的轉檔格式
                if (SourceFormat == "HD")
                    sb.AppendLine(string.Concat("  <add-subprofile>", objSysConfig.sysConfig_Read("/ServerConfig/ANS_Config/PROFILE_MAM_MS_HD"), "</add-subprofile>"));
                else
                    sb.AppendLine(string.Concat("  <add-subprofile>", objSysConfig.sysConfig_Read("/ServerConfig/ANS_Config/PROFILE_MAM_MS_SD"), "</add-subprofile>"));
            }

            // Tricky：在 MAM 中是使用 00:00:00;00 代表 drop-frame 的時間格式
            //         但在 ANS 中好像不吃這種格式，所以只好偷偷的轉換一下囉
            string startTC = StartTimecode.Replace(':', ';');
            string endTC = EndTimecode.Replace(':', ';');

            if (this.TransCodeType == "TAPE")
            {
                sb.AppendLine(@"  <live-job>1</live-job>");
                sb.AppendLine(@"  <capture-station>" + this.SourceFrom + "</capture-station>");
                sb.AppendLine(@"  <start-trigger-type>TIMECODE</start-trigger-type>");
                sb.AppendLine(@"  <stop-trigger-type>TIMECODE</stop-trigger-type>");
                sb.AppendLine(@"  <start-trigger-timecode>" + startTC + "</start-trigger-timecode>");
                sb.AppendLine(@"  <stop-trigger-timecode>" + endTC + "</stop-trigger-timecode>");
                if (SourceFormat == "HD")
                    sb.AppendLine(@"  <video-format>1080i 59.94Hz</video-format>");
                else
                    sb.AppendLine(@"  <video-format>525 59.94Hz</video-format>");
            }
            else
            {
                if (needTempSpace)
                    sb.AppendLine(string.Format(@"  <source-name>{0}</source-name>", this.TempDestinationTo));
                else
                    sb.AppendLine(string.Format(@"  <source-name>{0}</source-name>", this.SourceFrom));
                sb.AppendLine(string.Format(@"  <source-inpoint>{0}</source-inpoint>", startTC));
                sb.AppendLine(string.Format(@"  <source-outpoint>{0}</source-outpoint>", endTC));
            }
            // 這邊跟 HeadFrame 有關係
            sb.AppendLine(string.Format(@"  <thumb-time-single>00:00:00.000</thumb-time-single>"));

            sb.AppendLine(string.Format(@"  <output-name>$(basename).$(extension)</output-name>"));
            sb.AppendLine(string.Format(@"  <output-basename>{0}</output-basename>", this.DestinationToFile));

            // 依照 encoder profile 的不同，組出 output-dir 的部份
            // EncoderProfliePath：\\10.13.231.12\MAMProfiles\MAM\ms\enc_wmv.ms.awp，則取出 ms
            string[] encoderProfilePathPartial = EncoderProfliePath.Split('.');
            string outputDirShortName = encoderProfilePathPartial[encoderProfilePathPartial.Length - 2];
            sb.AppendLine(string.Format(@"  <{0}-output-dir>{1}</{0}-output-dir>", outputDirShortName, this.DestinationToDir));


            // 低解的部份(PartII)
            if (this.CreateLowRes == "Y")
                if (SourceFormat == "HD")
                    sb.AppendLine(string.Format(@"  <h264-output-dir>{0}</h264-output-dir>", this.LowResDir));
                else
                    sb.AppendLine(string.Format(@"  <ms-output-dir>{0}</ms-output-dir>", this.LowResDir));

            // 特別寫死的邏輯：當為送帶轉檔的SD Promo時，就自動加轉 MSSVideo 格式
            // -> 檢查設定檔，看這功能有沒有開啟
            if (objSysConfig.sysConfig_Read("/ServerConfig/STT_Config/PromoIngestMssVideo") == "Y")
            {
                // -> 檢查 HTTP Notify 的位置，與送帶轉檔的是否相符
                if (this.PostbackAddress == objSysConfig.sysConfig_Read("/ServerConfig/AshxHandler/ANS_STT"))
                {
                    // -> 如果是 SD 的 'P'romo，就加上 MSSVideo 的轉檔命令 (使用入庫的 MSSVideo 的 Profile)
                    if (this.DestinationFormat == "SD" && System.IO.Path.GetFileName(this.DestinationTo).StartsWith("P"))
                    {
                        sb.AppendLine(string.Concat("  <add-subprofile>", objSysConfig.sysConfig_Read("/ServerConfig/ANS_Config/PROFILE_MAM_MSS"), "</add-subprofile>"));
                        sb.AppendLine(string.Concat("  <pinnacle-output-dir>", objSysConfig.sysConfig_Read("/ServerConfig/STT_Config/INGEST_TEMP_DIR_UNC"), "</pinnacle-output-dir>"));
                        // TempDebug
                        string debugMsg = string.Concat("AutoCreate MssVideo: ", this.DestinationTo);
                        MAM_PTS_DLL.Log.AppendTrackingLog("TempDebug01", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, debugMsg);
                    }
                }
            }

            // 組合 Keyframe 的部份
            if (this.CaptureKeyframe == "Y")
            {
                sb.AppendLine(string.Concat("  <add-subprofile>", objSysConfig.sysConfig_Read("/ServerConfig/ANS_Config/PROFILE_MAM_KEYFRAME"), "</add-subprofile>"));
                sb.AppendLine(string.Concat("  <thumb-output-dir>", objSysConfig.sysConfig_Read("/ServerConfig/ANS_Config/DIR_KEYFRAME"), "</thumb-output-dir>"));
            }

            // 組合 LOGO 的部份
            if (this.DisplayLogo == "Y")
                sb.AppendLine(string.Concat("  <add-subprofile>", LogoProfilePath, "</add-subprofile>"));

            // 組合 HTTP通知 的部份
            sb.AppendLine(string.Concat("  <add-subprofile>", objSysConfig.sysConfig_Read("/ServerConfig/ANS_Config/PROFILE_MAM_NOT"), "</add-subprofile>"));

            // 結尾
            sb.AppendLine(@"</planner-submit>");

            return sb.ToString();
        }

        // 檢查必填的節點是否都存在
        private bool fnParseXmlData(XmlDocument xmlDoc)
        {
            bool parseResult = false;

            // 解析 /JobIngest/TransCodeType
            parseResult = fnParse_JobIngest_TransCodeType(xmlDoc.SelectSingleNode("/JobIngest/TransCodeType"));
            if (!parseResult)
                return false;

            // 解析 /JobIngest/IsFullTranscode
            parseResult = fnParse_JobIngest_IsPartialTranscode(xmlDoc.SelectSingleNode("/JobIngest/IsFullTranscode"));
            if (!parseResult)
                return false;

            // 解析 /JobIngest/IsNativeFormat
            parseResult = fnParse_JobIngest_IsNativeFormat(xmlDoc.SelectSingleNode("/JobIngest/IsNativeFormat"));
            if (!parseResult)
                return false;

            // 解析 /JobIngest/EncoderProfliePath
            parseResult = fnParse_JobIngest_EncoderProfliePath(xmlDoc.SelectSingleNode("/JobIngest/EncoderProfliePath"));
            if (!parseResult)
                return false;

            // 解析 /JobIngest/SourceFrom
            parseResult = fnParse_JobIngest_SourceFrom(xmlDoc.SelectSingleNode("/JobIngest/SourceFrom"));
            if (!parseResult)
                return false;

            // 解析 /JobIngest/DestinationTo
            parseResult = fnParse_JobIngest_DestinationTo(xmlDoc.SelectSingleNode("/JobIngest/DestinationTo"));
            if (!parseResult)
                return false;

            // 解析 /JobIngest/SourceFormat
            parseResult = fnParse_JobIngest_SourceFormat(xmlDoc.SelectSingleNode("/JobIngest/SourceFormat"));
            if (!parseResult)
                return false;

            // 解析 /JobIngest/DestinationFormat
            parseResult = fnParse_JobIngest_DestinationFormat(xmlDoc.SelectSingleNode("/JobIngest/DestinationFormat"));
            if (!parseResult)
                return false;

            // 解析 /JobIngest/StartTimecode
            parseResult = fnParse_JobIngest_StartTimecode(xmlDoc.SelectSingleNode("/JobIngest/StartTimecode"));
            if (!parseResult)
                return false;

            // 解析 /JobIngest/EndTimecode
            parseResult = fnParse_JobIngest_EndTimecode(xmlDoc.SelectSingleNode("/JobIngest/EndTimecode"));
            if (!parseResult)
                return false;

            // 解析 /JobIngest/PostbackAddress
            parseResult = fnParse_JobIngest_PostbackAddress(xmlDoc.SelectSingleNode("/JobIngest/PostbackAddress"));
            if (!parseResult)
                return false;

            // 解析 /JobIngest/CreateLowRes 及 /JobIngest/LowResDir
            parseResult = fnParse_JobIngest_CreateLowRes(xmlDoc.SelectSingleNode("/JobIngest/CreateLowRes"), xmlDoc.SelectSingleNode("/JobIngest/LowResDir"));
            if (!parseResult)
                return false;

            // 解析 /JobIngest/CaptureKeyframe
            parseResult = fnParse_JobIngest_CaptureKeyframe(xmlDoc.SelectSingleNode("/JobIngest/CaptureKeyframe"));
            if (!parseResult)
                return false;

            // 解析 /JobIngest/DisplayLogo 及 /JobIngest/LogoProfilePath
            parseResult = fnParse_JobIngest_DisplayLogo(xmlDoc.SelectSingleNode("/JobIngest/DisplayLogo"), xmlDoc.SelectSingleNode("/JobIngest/LogoProfilePath"));
            if (!parseResult)
                return false;

            // 解析 /JobIngest/Note
            parseResult = fnParse_JobIngest_Note(xmlDoc.SelectSingleNode("/JobIngest/Note"));
            if (!parseResult)
                return false;

            // 全都解析完了
            return true;
        }

        // 檢查所有屬性內容是否正確
        private bool fnValidateAllAttributes()
        {
            if (!fnValidate_TranscodeType())
                return false;
            if (!fnValidate_IsPartialTranscode())
                return false;
            if (!fnValidate_IsNativeFormat())
                return false;
            if (!fnValidate_EncoderProfilePath())
                return false;
            if (!fnValidate_SourceFrom())
                return false;
            if (!fnValidate_DestinationTo())
                return false;
            if (!fnValidate_SourceFormat())
                return false;
            if (!fnValidate_DestinationFormat())
                return false;
            if (!fnValidate_StartTimecode())
                return false;
            if (!fnValidate_EndTimecode())
                return false;
            if (!fnValidate_PostbackAddress())
                return false;
            if (!fnValidate_CreateLowRes())
                return false;
            if (!fnValidate_CaptureKeyframe())
                return false;
            if (!fnValidate_Note())
                return false;
            return true;
        }
    }
}
