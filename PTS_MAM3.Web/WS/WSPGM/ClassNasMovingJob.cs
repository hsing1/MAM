﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.WS.WSPGM
{
    public class ClassNasMovingJob
    {
        public string RequestTime;
        public string VideoID;
        public string DirFrom;
        public string DirDest;
        public string WorkerName;
        public string StartProcTime;
        public string FinishProcTime;
        public string Status;

        public string mssCopyID;

        // default constructior , but can do nothing
        public ClassNasMovingJob()
        {
            RequestTime = string.Empty;
            VideoID = string.Empty;
            DirFrom = string.Empty;
            DirDest = string.Empty;
            WorkerName = string.Empty;
            StartProcTime = string.Empty;
            FinishProcTime = string.Empty;
            Status = string.Empty;
            mssCopyID = string.Empty;
        }

        // Constructor
        public ClassNasMovingJob(string requestTime, string videoID, string dirFrom, string dirDest, string workerName, string startProcTime, string finishProcTime, string copyID, string enumStatus)
        {
            // 將傳入的參數寫到屬性中
            RequestTime = requestTime;
            VideoID = videoID;
            DirFrom = dirFrom;
            DirDest = dirDest;
            WorkerName = workerName;
            StartProcTime = startProcTime;
            FinishProcTime = finishProcTime;
            mssCopyID = copyID;
            // 檢查時間，如果小於 1901 年的資料，就會變成「尚未開始」
            fnCheckDataTime();
            // 屬性的值，從列舉值轉為比較容易懂的中文 :D
            fnParseStatus(enumStatus);
        }

        private void fnCheckDataTime()
        {
            // Request Time 不可能會是 1900-01-01

            // 開始時間、結束時間有可能會是 null or MinValue，就顯示為 "尚未開始"
            if (StartProcTime.StartsWith("1900"))
                StartProcTime = string.Empty;
            if (FinishProcTime.StartsWith("1900"))
                FinishProcTime = string.Empty;
        }

        private void fnParseStatus(string status)
        {
            int iStatus = Int32.Parse(status);
            ServiceMSSCopy.MSSCOPY_STATUS x = (ServiceMSSCopy.MSSCOPY_STATUS)iStatus;
            switch (x)
            {
                case ServiceMSSCopy.MSSCOPY_STATUS.INITIAL:
                    Status = "尚未開始處理";
                    break;
                case ServiceMSSCopy.MSSCOPY_STATUS.SYSERROR:
                    Status = "處理時發生網路中斷";
                    break;
                case ServiceMSSCopy.MSSCOPY_STATUS.COPYING:
                    Status = "檔案複製中";
                    break;
                case ServiceMSSCopy.MSSCOPY_STATUS.COPY_FAIL:
                    Status = "複製檔案時發生錯誤";
                    break;
                case ServiceMSSCopy.MSSCOPY_STATUS.FINISH:
                    Status = "完成";
                    break;
                case ServiceMSSCopy.MSSCOPY_STATUS.SOURCE_NOTEXIST:
                    Status = "來源檔不存在";
                    break;
                default:
                    Status = "處理中";
                    MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM/ClassNasMovingJob/fnParseStatus", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "Status Unknown : Value = " + x.ToString() + ", String = " + Status);
                    break;
            }
        }
    }
}
