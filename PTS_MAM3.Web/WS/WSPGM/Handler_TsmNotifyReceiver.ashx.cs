﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.WS.WSPGM
{
    /// <summary>
    ///Handler_TsmNotifyReceiver 的摘要描述
    /// </summary>
    public class Handler_TsmNotifyReceiver : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            // 
            MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM/TsmNotifyReceiver/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "START");

            // 
            HttpRequest req = context.Request;

            // 
            long tsmJobID;
            WSTSM.TSMRecallService.TsmRecallJobResult tsmJobStatus;
            string tsmJobMessage;

            // 如果 return false ，代表 HTTP POST 的內容有誤
            // ansJobStatus：TSMERROR, ANSERROR, FINISH, SYSERROR, SOURCE_NOT_EXIST
            WSTSM.TSMRecallService objTSM = new WSTSM.TSMRecallService();
            if (!objTSM.ParseTsmPostMessage(req.InputStream, out tsmJobID, out tsmJobStatus, out tsmJobMessage))
                return;

            // 理論上 TsmJobID < 0 的情況不會跑到這來，但為了安全起見還是擋一下，避免後面在做資料庫更新時會有誤動作
            if (tsmJobID < 0)
                return;

            // 更新資料表的參數
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FNTSMJOB_ID", tsmJobID.ToString());
            // 依照不同 TSM 任務狀況，去調整 MSSPROCESS 的狀態
            if (tsmJobStatus == WSTSM.TSMRecallService.TsmRecallJobResult.FINISH)
                sqlParameters.Add("FNSTATUS", (((int)ServiceMSSProcess.MSSPROCESS_STATUS.INITIAL).ToString()));
            else if (tsmJobStatus == WSTSM.TSMRecallService.TsmRecallJobResult.FILE_NOT_EXIST ||
                    tsmJobStatus == WSTSM.TSMRecallService.TsmRecallJobResult.SYSTEM_ERROR)
                sqlParameters.Add("FNSTATUS", (((int)ServiceMSSProcess.MSSPROCESS_STATUS.ERR_TSMJOB).ToString()));
            else
                sqlParameters.Add("FNSTATUS", (((int)ServiceMSSProcess.MSSPROCESS_STATUS.ERR_SYSTEM).ToString()));
            if (!MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_MSSPROCESS_UPDATE_TSMJOB_STATUS", sqlParameters, "system"))
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM/Handler_TsmNotifyReceiver/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "Got Error When SP_U_MSSPROCESS_UPDATE_TSMJOB_STATUS: tsmJobID = " + tsmJobID);
                return;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}