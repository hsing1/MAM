﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace PTS_MAM3.Web.WS.WSPGM
{
    /// <summary>
    /// 由 MSSProcess 接收 MSS Copy 服務處理的結果
    /// </summary>
    public class MSSCopyHandler : IHttpHandler
    {
        /// <summary>
        /// 原本是設計一整個 asmx +　ashx　的流程來處理　ｍｓｓｃｏｐｙ　的機制
        /// 但發現在限於同時執行的執行緒以及網站的流量負擔的問題下
        /// 所以我把這整個機制拆為：ａｓｍｘ服務註冊的入口處；然後ａｓｈｘ的處理機制交給ｗｉｎｆｏｒｍ程式做處理
        /// 這樣也可以讓ｗｉｎｆｏｒｍ程式佈在其他負載比較輕的主機上，不會限於ｉｉｓ
        /// 
        /// 後來，這個　ｈａｎｄｌｅｒ　原本要用做　ｍｓｓｃｏｐｙ　的　ｃａｌｌｂａｃｋ　ｗｅｂ　ｍｅｔｈｏｄ
        /// 但因為　ｍｓｓｃｏｐｙ　的呼叫來源太單純
        /// 所以直接以　ｗｅｂ　ｍｅｔｈｏｄ　的方式處理掉了，用不到　ｈｔｔｐ　ｐｏｓｔ +　ａｓｈｘ來做傳接
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
        //    //// 服務重啟起始時，把資料庫未完成的都改成F，避免在強制重啟服務後，有一些檔案永遠是P
        //    //if (!initialFlag)
        //    //{
        //    //    initialFlag = true;
        //    //    ServiceMSSCopy.SetAllDispatchingRecordsError();
        //    //}

        //    //// 檢查目前的執行佇列數量是否超過定義好的最大值
        //    //if (isDispatchThreadFull())
        //    //    return;
            
        //    //// 找出要處理的VideoID
        //    //string processVideoID = fnGetDispatchingVideoID();

        //    //// 重複檢查，如果正確就加入執行佇列
        //    //if (!queueInProcess.Contains(processVideoID))
        //    //    queueInProcess.Add(processVideoID);
        //    //else
        //    //{
        //    //    // 錯誤：不應該Pop-Up相同的VideoID出來
        //    //    MAM_PTS_DLL.Log.AppendTrackingLog("SrvPGM-ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "Conflict Error: got the same VideoID to process, return.");
        //    //    // 觸發下一次的動作
        //    //    ServiceMSSCopy.TriggerDispatcher();
        //    //    return;
        //    //}
            
        //    //// 開始進行複製動作
        //    //bool tmpFlag = fnMssDirectoryCopy(processVideoID);

        //    //// 更新資料庫
        //    //fnUpdateDispatchingStatus(processVideoID, tmpFlag);

        //    //// Remove from JobQueue
        //    //queueInProcess.Remove(processVideoID);

        //    //// 觸發下一次的動作
        //    //ServiceMSSCopy.TriggerDispatcher();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}