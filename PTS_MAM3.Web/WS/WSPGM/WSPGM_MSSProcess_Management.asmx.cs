﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace PTS_MAM3.Web.WS.WSPGM
{
    /// <summary>
    /// 用來管理 MSSProcess App 的服務窗口，可以置於 ESB 上供調用
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class WSPGM_MSSProcess_Management : System.Web.Services.WebService
    {
        /// <summary>
        /// 查詢某一天的 MSSProcess 資料
        /// </summary>
        /// <param name="dayFrom">欲查詢的日期</param>
        /// <returns></returns>
        [WebMethod]
        public List<ClassMSSProcessJob> GetOneDayMssProcessJob(string dayFrom)
        {
            // 檢查輸入的參數
            DateTime inputDate;
            if (!DateTime.TryParse(dayFrom, out inputDate))
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM/MSSProcess_Management/GetOneDayMssProcessJob", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, string.Concat("輸入的查詢參數有誤：", dayFrom));
                return new List<ClassMSSProcessJob>();
            }

            // 查詢資料庫
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("QUERY_DATE", inputDate.ToString("yyyyMMdd"));


            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_MSSPROCESS_GETONEDAYJOBS", sqlParameters, out resultData) || resultData.Count == 0)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM/MSSProcess_Management/GetOneDayMssProcessJob", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Concat("SP_Q_MSSPROCESS_GETONEDAYJOBS Error：", dayFrom));
                return new List<ClassMSSProcessJob>();
            }

            // 解析回傳的資料
            List<ClassMSSProcessJob> resultList = new List<ClassMSSProcessJob>();
            for (int i = 0; i < resultData.Count; i++)
            {
                ClassMSSProcessJob obj = new ClassMSSProcessJob();
                obj.MSSProcessJobID = resultData[i]["FNMSSPROCESS_ID"];
                obj.MSSProcessJobStatus = resultData[i]["FNSTATUS"];
                obj.VideoID = resultData[i]["FSVIDEOID"];
                obj.TranscodeJobID = resultData[i]["FNTRANSCODE_ID"];
                obj.TsmJobID = resultData[i]["FNTSMJOB_ID"];
                obj.MSSCopyJobID = resultData[i]["FNMSSCOPY_ID"];
                // 2012/07/06：修改顯示的內容
                obj.Note = string.Format("【{0}】{1}{2}",
                    resultData[i]["FSFILE_NO"],
                    resultData[i]["FSPGMNAME"],
                    (!resultData[i]["FNEPISODE"].Equals("0")) ? string.Concat("（集數：", resultData[i]["FNEPISODE"], "）") : string.Empty);

                obj.ConvertAttributes();
                resultList.Add(obj);
            }
            return resultList;
        }

      
        /// <summary>
        /// 多條件查詢取得段落置換紀錄清單 by Jarvis20130604
        /// </summary>
        /// <param name="FSTYPE">影片類型</param>
        /// <param name="FSPROGID">節目編號</param>
        /// <param name="FNEPISOD">子集集數</param>
        /// <param name="SDATE">開始時間</param>
        /// <param name="EDATE">結束時間</param>
        /// <param name="FSCREATED_BY">更新者</param>
        /// <returns></returns>
        [WebMethod]
        public List<Class_MssProcessDetail> Get_LOG_VIDEO_SEG_CHG_LIST_ADVANCE
            (string FSTYPE, string FSPROGID, string FNEPISOD, string SDATE, string EDATE, string FSCREATED_BY)
        {
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSTYPE", FSTYPE);
            sqlParameters.Add("FSPROGID", FSPROGID);
            sqlParameters.Add("FNEPISOD", FNEPISOD);
            sqlParameters.Add("SDATE", SDATE);
            sqlParameters.Add("EDATE", string.Format(EDATE, "yyyy-mm-dd"));
            sqlParameters.Add("FSCREATED_BY", FSCREATED_BY);


            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_LOG_VIDEO_SEG_CHG", sqlParameters, out resultData))
            {
                //MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM/MSSProcess_Management/GetOneDayMssProcessJob", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, string.Concat("SP_Q_MSSPROCESS_GETONEDAYJOBS Error：", deta));
                return new List<Class_MssProcessDetail>();
            }


            List<Class_MssProcessDetail> returnList = new List<Class_MssProcessDetail>();

            for (int i = 0; i < resultData.Count; i++)
            {
                Class_MssProcessDetail cmp = new Class_MssProcessDetail();

                cmp.FSFILE_NO = resultData[i]["FSFILE_NO"];
                cmp.FSVIDEO_ID = resultData[i]["FSVIDEO_ID"];
                cmp.FSPGMNAME = resultData[i]["FSPGMNAME"];
                cmp.FSPGDNAME = resultData[i]["FSPGDNAME"];
                cmp.FNEPISODE = resultData[i]["FNEPISODE"];
                cmp.FSBEG_TIMECODE = resultData[i]["FSBEG_TIMECODE_OLD"];
                cmp.FSEND_TIMECODE = resultData[i]["FSEND_TIMECODE_OLD"];
                cmp.FSBEG_TIMECODE_NEW = resultData[i]["FSBEG_TIMECODE_NEW"];
                cmp.FSEND_TIMECODE_NEW = resultData[i]["FSEND_TIMECODE_NEW"];
                cmp.FSCREATED_BY = resultData[i]["FSCREATED_BY"];
                cmp.FSUSER_ChtName = resultData[i]["FSUSER_ChtName"];
  
                cmp.FDCREATED_DATE = resultData[i]["FDCREATED_DATE"];
                returnList.Add(cmp);
            }

            return returnList;
        }

       
        /// <summary>
        /// 新增段落置換紀錄  by Jarvis20130604
        /// </summary>
        /// <param name="detail">Class_MssProcessDetail物件，其中的屬性為要傳遞的參數</param>
        /// <param name="FSCREATED_BY">更新者</param>
        /// <returns></returns>
        [WebMethod]
        public Boolean INSERT_LOG_VIDEO_SEG_CHG(Class_MssProcessDetail detail, string FSCREATED_BY)
        {
            Dictionary<string, string> source = new Dictionary<string, string>();
            source.Add("FSFILE_NO", detail.FSFILE_NO);
            source.Add("FNSEG_NO", detail.FNSEG_ID);
            source.Add("FSVIDEO_ID", detail.FSVIDEO_ID);
            source.Add("FSBEG_TIMECODE_OLD", detail.FSBEG_TIMECODE);
            source.Add("FSEND_TIMECODE_OLD", detail.FSEND_TIMECODE);
            source.Add("FCLOW_RES_OLD", detail.FCLOW_RES);
            source.Add("FSBEG_TIMECODE_NEW", detail.FSBEG_TIMECODE_NEW);
            source.Add("FSEND_TIMECODE_NEW", detail.FSEND_TIMECODE_NEW);
            source.Add("FSCREATED_BY", FSCREATED_BY);

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBLOG_VIDEO_SEG_CHG", source, FSCREATED_BY);
        }

      
        /// <summary>
        /// 更新TBLOG_VIDEO_SEG中的段落資料 by Jarvis20130604
        /// </summary>
        /// <param name="detail">Class_MssProcessDetail物件其中的屬性為要傳遞的參數</param>
        /// <param name="strUpdated_BY">更新者</param>
        /// <returns>bool</returns>
        [WebMethod]
        public Boolean UPDATE_TBLOG_VIDEO_SEG_Replacement(Class_MssProcessDetail detail, string strUpdated_BY)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSFILE_NO", detail.FSFILE_NO.Trim());
            sqlParameters.Add("FNSEG_ID", detail.FNSEG_ID.Trim());
            sqlParameters.Add("FSVIDEO_ID", detail.FSVIDEO_ID.Trim());
            sqlParameters.Add("FSBEG_TIMECODE", detail.FSBEG_TIMECODE_NEW.Trim());
            sqlParameters.Add("FSEND_TIMECODE", detail.FSEND_TIMECODE_NEW.Trim());
            sqlParameters.Add("BEGTC_old", detail.FSBEG_TIMECODE.Trim());
            sqlParameters.Add("ENDTC_old", detail.FSEND_TIMECODE.Trim());
            sqlParameters.Add("FSUPDATED_BY", strUpdated_BY.Trim());

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TBLOG_VIDEO_SEG_Replacement", sqlParameters, strUpdated_BY); ;
        }

         
        /// <summary>
        /// 抓取單筆資料的詳細資料 by Jarvis20130603
        /// </summary>
        /// <param name="videoID">該段落的videoID</param>
        /// <returns>Class_MssProcessDetail的集合</returns>
        [WebMethod]
        public List<Class_MssProcessDetail> GetMssProcessDetail(string videoID)
        {
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("VIDEO_ID", videoID);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_MSSProcessDetailData", sqlParameters, out resultData) || resultData.Count == 0)
            {
                //MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM/MSSProcess_Management/GetOneDayMssProcessJob", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, string.Concat("SP_Q_MSSPROCESS_GETONEDAYJOBS Error：", dayFrom));
                return new List<Class_MssProcessDetail>();
            }

            //解析資料
            List<Class_MssProcessDetail> resultList = new List<Class_MssProcessDetail>();
            for (int i = 0; i < resultData.Count; i++)
            {
                Class_MssProcessDetail obj = new Class_MssProcessDetail();

                obj.FSFILE_NO = resultData[i]["FSFILE_NO"];
                obj.FNSEG_ID = resultData[i]["FNSEG_ID"];
                obj.FSVIDEO_ID = resultData[i]["FSVIDEO_ID"];
                obj.FCLOW_RES = resultData[i]["FCLOW_RES"];
                obj.FSBEG_TIMECODE = resultData[i]["FSBEG_TIMECODE"];
                obj.FSEND_TIMECODE = resultData[i]["FSEND_TIMECODE"];
                obj.FSPGDNAME = resultData[i]["FSPGDNAME"];
                obj.FSPGMNAME = resultData[i]["FSPGMNAME"];
                obj.FSID = resultData[i]["FSID"];
                obj.FNEPISODE = resultData[i]["FNEPISODE"];
                obj.FSVIDEO_PROG = resultData[i]["FSVIDEO_PROG"];
                obj.FSCREATED_BY = resultData[i]["FSCREATED_BY"];

                resultList.Add(obj);
            }
            return resultList;

        }


        /// <summary>
        /// 段落置換後更新TBPGM_MSSPROCCESS中的狀態
        ///  by Jarvis20130611
        /// </summary>
        /// <param name="jobID">MSSPROCCESSID</param>
        /// <param name="videoID">該段落的videoID</param>
        /// <param name="UPDATE_BY">更新者</param>
        /// <returns>bool</returns>
        [WebMethod]
        public Boolean Update_MSSPROCCESS_Tunning(string jobID, string videoID, string UPDATE_BY)
        {
            string toDay = DateTime.Now.Year.ToString() + String.Format("{0:00}", DateTime.Now.Month) + String.Format("{0:00}", DateTime.Now.Day);
            string dataDate = jobID.Substring(0, 8);

            int flag = 1;
            int FNSTATUS = ((int)WS.WSPGM.ServiceMSSProcess.MSSPROCESS_STATUS.INITIAL);

            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FNSTATUS", FNSTATUS.ToString());
            sqlParameters.Add("FSVIDEOID", videoID);
            sqlParameters.Add("FNMSSPROCCESSID", jobID);
            sqlParameters.Add("CurrentDate", toDay);
            //sqlParameters.Add("DataDate", dataDate);
            sqlParameters.Add("Flag", flag.ToString());

            Boolean result1 = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_MSSPROCESS_Tunning", sqlParameters, UPDATE_BY);
            Boolean result2 = false;
            if (dataDate != toDay)
            {
                FNSTATUS = ((int)WS.WSPGM.ServiceMSSProcess.MSSPROCESS_STATUS.TIMECODE_CHANGED);
                flag = 0;
                sqlParameters.Clear();
                sqlParameters.Add("FNSTATUS", FNSTATUS.ToString());
                sqlParameters.Add("FSVIDEOID", videoID);
                sqlParameters.Add("FNMSSPROCCESSID", jobID);
                sqlParameters.Add("CurrentDate", toDay);
                sqlParameters.Add("Flag", flag.ToString());
                result2 = MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_MSSPROCESS_Tunning", sqlParameters, UPDATE_BY);
               
                return result1 && result2;
            }


            return result1;
        }

        /// <summary>
        /// 重新啟動 MSSProcess 的單項任務
        /// </summary>
        /// <param name="jobID">要重啟的任務編號</param>
        /// <param name="forceTranscode">是否強制重新轉檔</param>
        /// <param name="forceFileCopy">是否強制自動派送至NAS_B</param>
        /// <returns></returns>
        [WebMethod]
        public bool RetryMssProcessJob(string jobID, bool forceTranscode, bool forceFileCopy)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            List<Dictionary<string, string>> sqlResult = new List<Dictionary<string, string>>();

            // 先做個記錄
            MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM/MSSProcess_Management/RetryMssProcessJob", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, string.Format("START: {0}, {1}, {2}", jobID, forceTranscode, forceFileCopy));

            string videoID = null;
            // 如果有要求強制重轉或是重新派送的話，就清除 NAS 的資料，由 GPFS 重派
            // 如果沒要求的話，僅重置狀態，然後重新檢查狀態
            if (forceTranscode || forceFileCopy)
            {
                // 利用 JobID 取得 VideoID，然後再處理資料
                sqlParameters.Add("FNMSSPROCESS_ID", jobID.ToString());
                if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_MSSPROCESS_VIDEOID", sqlParameters, out sqlResult) || sqlResult.Count == 0 || string.IsNullOrEmpty(sqlResult[0]["FSVIDEOID"]))
                {
                    MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM/MSSProcess_Management/RetryMssProcessJob", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Concat("查詢 VideoID 資訊時發生錯誤，任務編號 = ", jobID));
                    return false;
                }
                videoID = sqlResult[0]["FSVIDEOID"];

                ServicePGM srPGM = new ServicePGM();
                if (!srPGM.DeleteNasFile(videoID))
                {
                    MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM/MSSProcess_Management/RetryMssProcessJob", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "進行 NAS 刪檔時發生錯誤，中止重置程序");
                    return false;
                }

                // 2012-0-26：當被要求重轉或重派時，提高該任務的優先權
                string tmpFilePath = string.Concat("\\\\10.13.200.1\\MasterCtrl\\MSG\\FTP\\FTP_HighPriority\\", videoID, ".high");
                try
                {
                    if (!System.IO.File.Exists(tmpFilePath))
                    {
                        System.IO.FileStream fileObject = System.IO.File.Create(tmpFilePath);
                        fileObject.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM/MSSProcess_Management/RetryMssProcessJob", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Concat("建立優先權註記檔時錯誤：檔名 - ", tmpFilePath, ", 錯誤 - ", ex.ToString()));
                }

                sqlParameters.Clear();
            }

            //if (forceTranscode)
            //{
            // 理論上這邊要去刪除 GPFS 的目錄是為上策，但如果沒有，也會由 Anystream 重蓋
            // 最好的狀態是先去 Stop Anystream Job，然後再重轉!
            //}

            // 理論上來說，這邊要做很好的檢查機制
            // -> 如果 TRANSCODE 的 JOBID 非 -1 時，要先能停掉之前 TRANSCODE 的動作 (以及 FCLOW_RES 的內容)
            // -> 如果 MSSCOPY 有相關動作時，也要先能停掉

            // 不過這邊先做簡單的 RESET 吧

            // 如果 FCLOW_RES = R 的話 (同上，正在轉檔中)
            // 再做 INITIAL 的話 ... 應該會讓整個流程空轉 , NEED TO THINK

            // 注意：過期的 ID 雖然會改變狀態，但不會被執行！
            sqlParameters.Add("FNMSSPROCESS_ID", jobID);
            // 是否強制重新轉檔：會清除 TBPGM_MSSPROCESS 的內容，及重置 TBLOG_VIDEO_SEG 的狀態
            sqlParameters.Add("FNFORCETRANSCODE", (forceTranscode) ? "1" : "0");
            // 是否強制自動派送：影響 fnFORCEDIRECT 欄位
            sqlParameters.Add("FNFORCEDIRECT", (forceFileCopy) ? "1" : "0");

            // 寫入資料表
            if (!MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_MSSPROCESS_RETRY", sqlParameters, "system"))
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM/MSSProcess_Management/RetryMssProcessJob", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "執行SP_U_MSSPROCESS_RETRY時發生異常回傳");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 從 MSSProcess 資料表中找出最後一次轉檔的相關資料
        /// </summary>
        /// <param name="videoID"></param>
        /// <returns></returns>
        [WebMethod]
        public WSASC.ClassTranscodeJob GetLastestAnsJob(string videoID)
        {
            // 檢查 videoID 格式
            if (!MAM_PTS_DLL.CheckFormat.CheckFormat_VideoID(videoID))
                return null;

            // 查詢目標的 TranscodeID
            string transcodeID = getLastestTranscodeID(videoID);
            if (string.IsNullOrEmpty(transcodeID))
                return null;

            // 回傳
            WSASC.ServiceTranscode wsASC = new WSASC.ServiceTranscode();
            return wsASC.GetOneTranscodeJobInformation(transcodeID);
        }

        // 取得最後一筆有進行轉檔作業的轉檔ID
        private string getLastestTranscodeID(string videoID)
        {
            // 查詢資料表
            List<Dictionary<string, string>> sqlData;
            Dictionary<string, string> sqlParam = new Dictionary<string, string>();
            sqlParam.Add("FSVIDEO_ID", videoID);
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_MSSPROCESS_VALID_TRANSCODEID", sqlParam, out sqlData))
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM/MSSProcess_Management/getLastestTranscodeID", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "執行SP_Q_MSSPROCESS_VALID_TRANSCODEID時發生異常回傳");
                return string.Empty;
            }
            return (sqlData.Count == 0) ? string.Empty : sqlData[0]["FNTRANSCODE_ID"];
        }
    }
}