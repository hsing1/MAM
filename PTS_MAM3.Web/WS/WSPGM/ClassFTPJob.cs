﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace PTS_MAM3.Web.WS.WSPGM
{
    public class ClassFTPJob
    {
        public string VideoID;
        public string Ftp1Status;
        public string Ftp2Status;
        public string Ftp1ErrMsg;
        public string Ftp2ErrMsg;
        public string Ftp1StartExecTime;
        public string Ftp2StartExecTime;
        public string Ftp1FinishExecTime;
        public string Ftp2FinishExecTime;
        public string Ftp1FileSizeNAS;
        public string Ftp2FileSizeNAS;
        public string Ftp1FileSizeFTP;
        public string Ftp2FileSizeFTP;
        public string Note;

        public ClassFTPJob()
        {
            Ftp1Status = string.Empty;
            Ftp2Status = string.Empty;
            Ftp1ErrMsg = string.Empty;
            Ftp2ErrMsg = string.Empty;
            Ftp1StartExecTime = string.Empty;
            Ftp2StartExecTime = string.Empty;
            Ftp1FinishExecTime = string.Empty;
            Ftp2FinishExecTime = string.Empty;
            Ftp1FileSizeNAS = string.Empty;
            Ftp2FileSizeNAS = string.Empty;
            Ftp1FileSizeFTP = string.Empty;
            Ftp2FileSizeFTP = string.Empty;
            Note = string.Empty;
        }

        /// <summary>
        /// 讀取並解析輸入的檔名，拆解出來
        /// </summary>
        /// <param name="filePath"></param>
        public static bool ReadSymbolFile(string filePath, out string startTime, out string finishTime, out string fileSizeNas, out string fileSizeFtp, out string procMessage)
        {
            startTime = "讀取檔案時發生錯誤";
            finishTime = "讀取檔案時發生錯誤";
            fileSizeNas = "讀取檔案時發生錯誤";
            fileSizeFtp = "讀取檔案時發生錯誤";
            procMessage = "讀取檔案時發生錯誤";

            if (!File.Exists(filePath))
                return false;

            // 讀取相關檔案，並找出失敗的原因
            string lineData = string.Empty;
            try
            {
                StreamReader sr = new StreamReader(filePath);
                lineData = sr.ReadLine();
                sr.Close();
            }
            catch (Exception ex)
            {
                procMessage = "讀取錯誤訊息時發生IO錯誤";
                string errMsg = string.Concat("Got error when reading text file: ", filePath, ", msg = ", ex.ToString());
                MAM_PTS_DLL.Log.AppendTrackingLog("ClassFTPJob/ReadSymbolFile", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return false;
            }

            // 內容格式錯誤
            if (string.IsNullOrEmpty(lineData))
                return false;

            // 解析 lineData
            string[] parsedData = lineData.Split(';');
            if (parsedData.Length != 5)
            {
                string errMsg = string.Concat("解析到非正確格式的資料：fileName = ", filePath, ", content = ", lineData);
                MAM_PTS_DLL.Log.AppendTrackingLog("ClassFTPJob/ReadSymbolFile", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return false;
            }

            startTime = parsedData[0];
            finishTime = parsedData[1];
            fileSizeFtp = parsedData[2];
            fileSizeNas = parsedData[3];
            procMessage = parsedData[4];

            return true;
        }
    }
}
