﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.WS.WSPGM
{
    public class ClassMSSProcessJob
    {
        public string MSSProcessJobID;
        public string MSSProcessJobStatus;
        public string MSSProcessJobStatusC;
        public string VideoID;
        public string TranscodeJobID;
        public string TsmJobID;
        public string MSSCopyJobID;
        public string Note;

        public void ConvertAttributes()
        {
            // 將任務狀態轉為文字描述
            switch ((ServiceMSSProcess.MSSPROCESS_STATUS)(Int32.Parse(this.MSSProcessJobStatus)))
            {
                case ServiceMSSProcess.MSSPROCESS_STATUS.INITIAL:
                    this.MSSProcessJobStatusC = "等待處理中";
                    break;
                case ServiceMSSProcess.MSSPROCESS_STATUS.WAIT_IMX50:
                    this.MSSProcessJobStatusC = "IMX50高解檔不存在";
                    break;
                case ServiceMSSProcess.MSSPROCESS_STATUS.PROC_TRANS:
                    this.MSSProcessJobStatusC = "轉主控播出檔中";
                    break;
                case ServiceMSSProcess.MSSPROCESS_STATUS.PROC_TSMJOB:
                    this.MSSProcessJobStatusC = "將主控播出檔從磁帶取回中";
                    break;
                case ServiceMSSProcess.MSSPROCESS_STATUS.PROC_MSSCOPY:
                    this.MSSProcessJobStatusC = "將主控播出檔複製至NAS中";
                    break;
                case ServiceMSSProcess.MSSPROCESS_STATUS.ERR_TRANS:
                    this.MSSProcessJobStatusC = "執行轉主控播出檔時發生錯誤";
                    break;
                case ServiceMSSProcess.MSSPROCESS_STATUS.ERR_TSMJOB:
                    this.MSSProcessJobStatusC = "執行將主控播出檔從磁帶取回中時發生錯誤";
                    break;
                case ServiceMSSProcess.MSSPROCESS_STATUS.ERR_MSSCOPY:
                    this.MSSProcessJobStatusC = "執行將主控播出檔複製至NAS中時發生錯誤";
                    break;
                case ServiceMSSProcess.MSSPROCESS_STATUS.ERR_SYSTEM:
                    this.MSSProcessJobStatusC = "發生系統IO錯誤";
                    break;
                case ServiceMSSProcess.MSSPROCESS_STATUS.FINISH:
                    this.MSSProcessJobStatusC = "完成";
                    break;
                case ServiceMSSProcess.MSSPROCESS_STATUS.TIMECODE_CHANGED:
                    this.MSSProcessJobStatusC = "Timecode微調完成";
                    break;
                default:
                    this.MSSProcessJobStatusC = string.Empty;
                    MAM_PTS_DLL.Log.AppendTrackingLog("ClassMSSProcessJob", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Concat("Unknown Status: ", this.MSSProcessJobStatus));
                    break;
            }

            // 
            this.MSSProcessJobStatus = ((ServiceMSSProcess.MSSPROCESS_STATUS)(Int32.Parse(this.MSSProcessJobStatus))).ToString();
        }
    }
}