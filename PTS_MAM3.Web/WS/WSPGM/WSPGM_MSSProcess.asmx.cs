﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace PTS_MAM3.Web.WS.WSPGM
{
    /// <summary>
    /// WSPGM_MSSProcess 的摘要描述
    /// 這部份是供給後面的 WINFORM 使用，不需要 PUBLISH 到 ESB 上面
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class ServiceMSSProcess : System.Web.Services.WebService
    {
        /*
         2. 在 WebService 中，加入 WS_強制重轉，此時會有幾個動作：
            2-0. 要求使用者輸入 VideoID , 配合當天的日期 , 要自動找出該 VidedoID 所屬的 MSSProcessID
            2-1. 刪除 TSM 中的 MSS 檔案
            2-2. 刪除 NAS_A 及 NAS_B 的 MSS 檔案 , 及其 Token 檔 (.msscopy) <- 是否要刪掉 FTPERR 的記錄?
            2-3. 檢查上述檔案是否還存在
            2-4. 將該任務的 isApproved 欄位改寫為 1 , 並將任務狀態也改為 1 (initial)
                以及將 TB_LOG_VIDEO_SEG 的轉檔狀態改為 N (因為要重轉)
         */
        
        // 供 MSSPROCESS 的應用程式使用
        // 請維持 INITIAL 為 0 (且在第一順位)，且錯誤狀態必定要在 FINISH 狀態之後
        public enum MSSPROCESS_STATUS { INITIAL, WAIT_IMX50, PROC_TRANS, PROC_TSMJOB, PROC_MSSCOPY, FINISH, ERR_TRANS, ERR_TSMJOB, ERR_MSSCOPY, ERR_SYSTEM, TIMECODE_CHANGED };

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public string GetMssEncoderProfilePath()
        {
            MAM_PTS_DLL.SysConfig objSysconfig = new MAM_PTS_DLL.SysConfig();
            return objSysconfig.sysConfig_Read("/ServerConfig/ANS_Config/PROFILE_MAM_MSS");
        }

        /// <summary>
        /// used by application
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public string GetTranscodePostBackReceiverAddr()
        {
            MAM_PTS_DLL.SysConfig objSysconfig = new MAM_PTS_DLL.SysConfig();
            return objSysconfig.sysConfig_Read("/ServerConfig/AshxHandler/ANS_MSS");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public string GetTsmPostBackReceiverAddr()
        {
            MAM_PTS_DLL.SysConfig objSysconfig = new MAM_PTS_DLL.SysConfig();
            return objSysconfig.sysConfig_Read("/ServerConfig/AshxHandler/TSM_MSS");
        }

        [Obsolete("Don't Use", true)]
        [WebMethod]
        public MSSPROCESS_STATUS GetMSSPROCESS_STATUS()
        {
            return MSSPROCESS_STATUS.INITIAL;
        }
    }
}
