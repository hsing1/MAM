﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Xml;

namespace PTS_MAM3.Web.WS.WSPGM
{
    /// <summary>
    ///Handler_TranscodeNotifyReceiver 的摘要描述
    /// </summary>
    public class Handler_TranscodeNotifyReceiver : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            long transcodeJobID;
            string transcodeJobStatus = string.Empty;

            // 
            MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM/TranscodeNotifyReceiver/ProcessRequest", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, "START");

            // 
            HttpRequest req = context.Request;

            // 解析從 Transcode 傳來的內容
            // 如果 return false ，代表 HTTP POST 的內容有誤，無法判斷是哪個 ANSID，也因此不用改狀態
            // ansJobStatus：TSMERROR, ANSERROR, FINISH, SYSERROR, SOURCE_NOT_EXIST
            WSASC.ServiceTranscode objTranscode = new WSASC.ServiceTranscode();
            if (!objTranscode.ParseTranscodeJobHttpMessage(req.InputStream, out transcodeJobID, out transcodeJobStatus))
                return;

            long totalLength = 0;
            string filePath = string.Empty;

            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FNTRANSCODE_ID", transcodeJobID.ToString());
            if (transcodeJobStatus == "FINISH")
            {
                // 如果讀不到 VIDEOID 的話，就讓 LENGTH 為 0
                if (MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_MSSPROCESS_VIDEOID_TRANSCODEID", sqlParameters, out resultData))
                {
                    if (resultData.Count > 0 && !string.IsNullOrEmpty(resultData[0]["FSVIDEOID"]))
                    {
                        try
                        {
                            MAM_PTS_DLL.SysConfig objSysConfig = new MAM_PTS_DLL.SysConfig();
                            string shareRoot = objSysConfig.sysConfig_Read("/ServerConfig/TSM_Config/SambaRoot");
                            if (string.IsNullOrEmpty(shareRoot))
                                throw new Exception("Got empty value from objSysConfig.sysConfig_Read('/ServerConfig/TSM_Config/SambaRoot')");

                            filePath = string.Concat(@"\\", shareRoot, @"\mssvideo\", resultData[0]["FSVIDEOID"], @"\header");
                            FileInfo fileInfoObj = new FileInfo(filePath);
                            totalLength += fileInfoObj.Length;

                            filePath = string.Concat(@"\\", shareRoot, @"\mssvideo\", resultData[0]["FSVIDEOID"], @"\ft");
                            fileInfoObj = new FileInfo(filePath);
                            totalLength += fileInfoObj.Length;

                            filePath = string.Concat(@"\\", shareRoot, @"\mssvideo\", resultData[0]["FSVIDEOID"], @"\std");
                            fileInfoObj = new FileInfo(filePath);
                            totalLength += fileInfoObj.Length;
                        }
                        catch (Exception ex)
                        {
                            string errMsg = string.Concat("Got Error when read filesize (", filePath, ") in TSM, msg = ", ex.ToString());
                            MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM/Handler_TranscodeNotifyReceiver/", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                        }
                    }
                    else
                    {
                        // 疑似由 MSSProcess 發出的轉檔，但是又在 MSSProcessJob 資料表中找不到，先留個記錄吧
                        string errMsg = string.Concat("找不到相對應的TranscodeID = ", transcodeJobID.ToString());
                        MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM/Handler_TranscodeNotifyReceiver/", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                        return;
                    }
                }
            }

            // 開始準備要新的參數
            if (transcodeJobStatus == "FINISH")
            {
                // 當轉檔的動作是「完成」的時候，只需要去更新 MSSProcess 的任務到 INITIAL
                sqlParameters.Add("FNSTATUS", ((int)ServiceMSSProcess.MSSPROCESS_STATUS.INITIAL).ToString());
                sqlParameters.Add("FCLOW_RES", "Y");
                sqlParameters.Add("FNFILE_SIZE", totalLength.ToString());
            }
            else
            {
                sqlParameters.Add("FNSTATUS", ((int)ServiceMSSProcess.MSSPROCESS_STATUS.ERR_TRANS).ToString());
                sqlParameters.Add("FCLOW_RES", "F");
                sqlParameters.Add("FNFILE_SIZE", "0");
            }

            // 同時更新 TBPGM_MSSPROCESS & TBLOG_VIDEO_SEG 資料表內容
            if (!MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_MSSPROCESS_UPDATE_TRANSCODE_INFO", sqlParameters, "system"))
                MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM/Handler_TranscodeNotifyReceiver/", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "Get Error when Update Transcode Status: " + transcodeJobID);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}