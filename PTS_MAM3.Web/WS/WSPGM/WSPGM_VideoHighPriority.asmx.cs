﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace PTS_MAM3.Web.WS.WSPGM
{
    /// <summary>
    /// WSPGM_VideoHighPriority 的摘要描述
    /// </summary>
    public partial class ServicePGM : System.Web.Services.WebService
    {
        [WebMethod]
        public bool InsertHighPriorityVideo(string fsFileNo, DateTime fsValidDate)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSFILE_NO", fsFileNo);
            sqlParameters.Add("FSVALID_DATE", fsValidDate.ToString("yyyyMMdd"));
            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBLOG_VIDEO_HIGH_PRIORITY", sqlParameters, "system");
        }

        [WebMethod]
        public bool DeleteHighPriorityVideo(string fsFileNo)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSFILE_NO", fsFileNo);
            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_D_TBLOG_VIDEO_HIGH_PRIORITY", sqlParameters, "system");
        }

        [WebMethod]
        public bool QueryIsHighPriorityVideoID(DateTime queryDate, string VideoID)
        {
            List<Dictionary<string, string>> sqlData = new List<Dictionary<string, string>>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSVALID_DATE", queryDate.ToString("yyyyMMdd"));
            sqlParameters.Add("FSVIDEO_ID", VideoID);

            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_HIGH_PRIORITY_SINGLE", sqlParameters, out sqlData))
            {
                string errMsg = string.Concat("Got Error: ", queryDate.ToString(), ", ", VideoID);
                MAM_PTS_DLL.Log.AppendTrackingLog("QueryIsHighPriorityVideoID", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return false;       // 這邊比較尷尬，因為明明是錯的，但又回false
            }
            else
            {
                // 只需要取第一筆的資料即可(其實也只有一筆啦)
                if (sqlData[0]["RESULT"] == "Y")
                    return true;
                else
                    return false;
            }
        }

        [WebMethod]
        public List<string> QueryHighPriorityVideoIDs(DateTime queryDate)
        {
            List<Dictionary<string, string>> sqlData = new List<Dictionary<string, string>>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSVALID_DATE", queryDate.ToString("yyyyMMdd"));
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBLOG_VIDEO_HIGH_PRIORITY_DAY", sqlParameters, out sqlData) || sqlData.Count == 0)
                return new List<string>();
            else
            {
                // 將 FSVIDEO_ID 欄位內的值倒出來
                List<string> result = new List<string>();
                foreach (Dictionary<string, string> data in sqlData)
                    result.Add(data["FSVIDEO_ID"]);
                return result;
            }
        }
    }
}
