﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace PTS_MAM3.Web.WS.WSPGM
{
    public struct LouthJobDetail
    {
        public string VideoID;
        public string errorLevel;
        public string Title;
        public string Message;
    }

    public class ClassLouthJob
    {
        // overview
        public string XmlFileName;
        public string XmlFilePath;
        public string XmlFileStatus;
        // detail
        public string XmlProcessErrMsg;
        //public List<ClassLouthJobDetail> XmlProcessErrors;
        public List<LouthJobDetail> XmlProcessErrors;

        public ClassLouthJob()
        {
            //XmlProcessErrors = new List<ClassLouthJobDetail>();
            XmlProcessErrors = new List<LouthJobDetail>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lineText"></param>
        public void ParseLineErrorText(string lineText)
        {
            // 目前預定的格式是：
            // errorLevel;VideoID;Title;Message
            // 要注意的部份是 Title 有可能會有 ; 
            List<string> parsedResult = new List<string>(lineText.Split(';'));
            // 如果不符合格式就不寫進來
            if (parsedResult.Count < 4)
                return;
            // 
            //ClassLouthJobDetail obj = new ClassLouthJobDetail();
            LouthJobDetail obj = new LouthJobDetail();
            obj.errorLevel = parsedResult[0];
            obj.VideoID = parsedResult[1];
            obj.Message = parsedResult[parsedResult.Count - 1];
            // Trikcy: need to check
            StringBuilder sb = new StringBuilder(parsedResult[2]);
            for (int i = 3; i < parsedResult.Count - 1; i++)
            {
                sb.Append(";");
                sb.Append(parsedResult[i]);
            }
            obj.Title = sb.ToString();
            // 加入
            XmlProcessErrors.Add(obj);
        }
    }
}
