﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace PTS_MAM3.Web.WS.WSPGM
{
    /// <summary>
    /// WSPGM_MSSCopy 的摘要描述
    /// 這部份僅供給後面的 WINFORM 程式使用，請不要 PUBLISH 到 ESB
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class ServiceMSSCopy : System.Web.Services.WebService
    {
        // 主要供 MSSCOPY 的 WINFORM 程式使用
        public enum MSSCOPY_STATUS { INITIAL, COPYING, FINISH, COPY_FAIL, SOURCE_NOTEXIST, SYSERROR };

        private const string SYSCODE = "WSPGM";

        /// <summary>
        /// 因為這個服務支援了 NAS_FILE_MOVING 及 MSS_PROCESS，所以只好要求填入 dirFrom 及 dirTo
        /// 假如在同日有相同的「要求」被要求，就會回傳已申請的 JobID，並重置其作業狀態
        /// </summary>
        /// <param name="videoID">參考用</param>
        /// <param name="dirFrom">不含VideoID目錄名稱的來源檔的路徑</param>
        /// <param name="dirTo">不含VideoID目錄名稱的目的檔的路徑</param>
        /// <param name="deleteSource">Y或N</param>
        /// <returns></returns>
        [WebMethod]
        public long RegisterMSSCopyService(string videoID, string dirFrom, string dirTo, string deleteSource)
        {
            string fsPROGRAM = MAM_PTS_DLL.ConvertFormat.Combine_TraceLogFSProgram(SYSCODE, System.Reflection.MethodBase.GetCurrentMethod().Name);

            string logMsg = string.Concat("START: ", videoID, ", dirFrom = ", dirFrom, ", dirTo = ", dirTo);
            MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_MSSCopy/RegisterMSSCopyService", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, logMsg);

            long MSSCopyID = -1;

            // 找出當日的第一個 MSSCopyID (因為我不太會寫SQL，所以放這處理=_=)
            long startMSSCopyID = Int64.Parse(System.DateTime.Now.ToString("yyMMdd")) * 10000;

            // 確認寫入的資料，尾巴是帶 \ ，然後將路徑與VideoID做合併
            if (!dirFrom.EndsWith("\\"))
                dirFrom = string.Concat(dirFrom, "\\", videoID, "\\");
            else
                dirFrom = string.Concat(dirFrom, videoID, "\\");
            if (!dirTo.EndsWith("\\"))
                dirTo = string.Concat(dirTo, "\\", videoID, "\\");
            else
                dirTo = string.Concat(dirTo, videoID, "\\");

            // 準備要寫入資料表的參數
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("START_MSSCOPYID", startMSSCopyID.ToString());
            sqlParameters.Add("FSVIDEO_ID", videoID);
            sqlParameters.Add("FSDIRFROM", dirFrom);
            sqlParameters.Add("FSDIRTO", dirTo);
            if (deleteSource == "Y")
                sqlParameters.Add("FCDELETE_SOURCE", "Y");
            else
                sqlParameters.Add("FCDELETE_SOURCE", "N");
            sqlParameters.Add("STATUS_INITIAL_VALUE", ((int)MSSCOPY_STATUS.INITIAL).ToString());

            // 寫入資料表
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_I_MSSCOPY", sqlParameters, out resultData) || resultData.Count == 0)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog(fsPROGRAM, MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Concat("新增資料時發生錯誤，VideoID = ", videoID));
                return -1;
            }

            // 解析資料庫的回傳值
            if (!Int64.TryParse(resultData[0]["FNMSSCOPY_ID"], out MSSCopyID))
                return -1;
            else
                return MSSCopyID;
        }

        /// <summary>
        /// 變更 MSSCopy 任務的優先權
        /// </summary>
        /// <param name="msscopyID"></param>
        /// <param name="priority"></param>
        /// <returns></returns>
        [WebMethod]
        public bool ChangeMSSCopyServicePriority(long msscopyID, int priority)
        {
            string logMsg = string.Format("START：msscopyID = {0}, priority = {1}", msscopyID, priority);
            MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM/MSSCopy/ChangeMSSCopyServicePriority", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, logMsg);

            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FNMSSCOPY_ID", msscopyID.ToString());
            sqlParameters.Add("FNPRIORITY", priority.ToString());

            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_MSSCOPY_PRIORITY", sqlParameters, "system");
        }

        /// <summary>
        /// 當 MSSCopy 的背景應用程式執行完畢後，會呼叫的服務
        /// 這算是 private 的 WebMethod，請不要放上 ESB
        /// </summary>
        /// <param name="msscopyID"></param>
        /// <param name="isSuccess">執行MSSCOPY的動作是否有成功</param>
        /// <returns></returns>
        [WebMethod]
        public bool MSSCopyServiceCallbackMethod(string msscopyID, bool isSuccess)
        {
            MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM/MSSCopy/MSSCopyServiceCallbackMethod", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Concat("[這邊不應該再出現了] START：", msscopyID));

            // 2013-09-09：將這段邏輯移到 APP_MSSCOPY 直接執行，但因為怕有漏鉤的，所以這段 CODE 依然放在 WEBSERVICE 上跑，待確認無誤，就可以整個移除

            // 針對 MSSPROCESS 的資料表做更新，如果是從 PGM 來的 REQUEST，就不會被做到
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FNMSSCOPY_ID", msscopyID);
            if (isSuccess)
                sqlParameters.Add("FNSTATUS", ((int)ServiceMSSProcess.MSSPROCESS_STATUS.FINISH).ToString());
            else
                sqlParameters.Add("FNSTATUS", ((int)ServiceMSSProcess.MSSPROCESS_STATUS.ERR_MSSCOPY).ToString());

            if (!MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_MSSPROCESS_MSSCOPY_STATUS", sqlParameters, "system"))
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM/MSSCopy/MSSCopyServiceCallbackMethod", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Concat("[這邊不應該再出現了] SP_U_MSSPROCESS_MSSCOPY_STATUS Error：", msscopyID));
                return false;
            }
            return true;
        }

        /// <summary>
        /// Dummy，只是要讓 WinForm 可以存取到這邊的列舉值內容
        /// </summary>
        /// <returns></returns>
        [Obsolete("Don't Use", true)]
        [WebMethod]
        public MSSCOPY_STATUS GetMSSProcessStatus()
        {
            return MSSCOPY_STATUS.INITIAL;
        }
    }
}
