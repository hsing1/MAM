﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace PTS_MAM3.Web.WS.WSPGM
{
    /// <summary>
    /// WSPGM_NasFileMoving 的摘要描述
    /// 這部份是提供給排表系統使用，有需要的話要 PUBLISH 到 ESB 上面
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class ServiceNasFileMoving : System.Web.Services.WebService
    {
        public enum NAS_STORAGE_QUERY_ITEMS { NAS_SERVER, NAS_TRANSCODE, NAS_SEND, NAS_MSG };

        public string NAS_STORAGE;
        public string NAS_STORAGE_TRANSCODE;
        public string NAS_STORAGE_SEND;
        public string NAS_STORAGE_MSG;

        public ServiceNasFileMoving()
        {
            // 讀取基本資訊，原本應該要做Refresh的啦，不過有點懶，所以寫在Constructor中
            MAM_PTS_DLL.SysConfig obj = new MAM_PTS_DLL.SysConfig();
            NAS_STORAGE = obj.sysConfig_Read("/ServerConfig/NAS/MASTERCONTROL_NAS_ROOT");
            NAS_STORAGE_TRANSCODE = obj.sysConfig_Read("/ServerConfig/NAS/MASTERCONTROL_NAS_TRANSCODE");
            NAS_STORAGE_SEND = obj.sysConfig_Read("/ServerConfig/NAS/MASTERCONTROL_NAS_SEND");
            NAS_STORAGE_MSG = obj.sysConfig_Read("/ServerConfig/NAS/MASTERCONTROL_NAS_MSG");
        }

        /// <summary>
        /// 供排表系統註冊 NAS 上的播出媒體檔服務
        /// </summary>
        /// <param name="videoID"></param>
        /// <returns></returns>
        [WebMethod]
        public bool RegisterNasFileMovingService(string videoID)
        {
            ServiceMSSCopy objSR = new ServiceMSSCopy();
            long mssCopyID = -1;
            string errMsg;

            // 為了讓排表流程順暢，在這邊會刪除 ftp 的註記檔(如果有存在的話)，當按派送時，就會重新檢查整個 ftp 的內容
            // 如果刪除時發生錯誤，不會讓程序中止，只是會有記錄供查詢
            string deleteFilePath = string.Concat(GetNasStoragePath(NAS_STORAGE_QUERY_ITEMS.NAS_SEND), videoID, ".ftpok1");
            if (!MAM_PTS_DLL.FileOperation.DeleteFile(deleteFilePath, out errMsg))
                MAM_PTS_DLL.Log.AppendTrackingLog("RegisterNasFileMovingService", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Format("刪檔失敗：{0}, {1}", deleteFilePath, errMsg));
            deleteFilePath = string.Concat(GetNasStoragePath(NAS_STORAGE_QUERY_ITEMS.NAS_SEND), videoID, ".ftpok2");
            if (!MAM_PTS_DLL.FileOperation.DeleteFile(deleteFilePath, out errMsg))
                MAM_PTS_DLL.Log.AppendTrackingLog("RegisterNasFileMovingService", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Format("刪檔失敗：{0}, {1}", deleteFilePath, errMsg));
            deleteFilePath = string.Concat(GetNasStoragePath(NAS_STORAGE_QUERY_ITEMS.NAS_SEND), videoID, ".ftperr1");
            if (!MAM_PTS_DLL.FileOperation.DeleteFile(deleteFilePath, out errMsg))
                MAM_PTS_DLL.Log.AppendTrackingLog("RegisterNasFileMovingService", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Format("刪檔失敗：{0}, {1}", deleteFilePath, errMsg));
            deleteFilePath = string.Concat(GetNasStoragePath(NAS_STORAGE_QUERY_ITEMS.NAS_SEND), videoID, ".ftperr2");
            if (!MAM_PTS_DLL.FileOperation.DeleteFile(deleteFilePath, out errMsg))
                MAM_PTS_DLL.Log.AppendTrackingLog("RegisterNasFileMovingService", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Format("刪檔失敗：{0}, {1}", deleteFilePath, errMsg));
            deleteFilePath = string.Concat(GetNasStoragePath(NAS_STORAGE_QUERY_ITEMS.NAS_SEND), videoID, ".ftpexist1");
            if (!MAM_PTS_DLL.FileOperation.DeleteFile(deleteFilePath, out errMsg))
                MAM_PTS_DLL.Log.AppendTrackingLog("RegisterNasFileMovingService", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Format("刪檔失敗：{0}, {1}", deleteFilePath, errMsg));
            deleteFilePath = string.Concat(GetNasStoragePath(NAS_STORAGE_QUERY_ITEMS.NAS_SEND), videoID, ".ftpexist2");
            if (!MAM_PTS_DLL.FileOperation.DeleteFile(deleteFilePath, out errMsg))
                MAM_PTS_DLL.Log.AppendTrackingLog("RegisterNasFileMovingService", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Format("刪檔失敗：{0}, {1}", deleteFilePath, errMsg));

            // 這邊才是原本的重點：註冊 MSSCopy 的服務
            try
            {
                // 檢查：如果來源路徑不存在時，就不給註冊服務
                if (!System.IO.Directory.Exists(string.Concat(GetNasStoragePath(NAS_STORAGE_QUERY_ITEMS.NAS_TRANSCODE), videoID)))
                    return false;

                // 將 NAS_A 的檔案複製到 NAS_B，並且不刪除 NAS_A 上的原始檔
                mssCopyID = objSR.RegisterMSSCopyService(videoID, GetNasStoragePath(NAS_STORAGE_QUERY_ITEMS.NAS_TRANSCODE), GetNasStoragePath(NAS_STORAGE_QUERY_ITEMS.NAS_SEND), "N");

                // 如果收到的回傳值為 -1 ，代表在進行註冊時的動作失敗，通常就是資料庫的寫入發生問題
                if (mssCopyID == -1)
                    throw new Exception(string.Concat("進行 RegisterMSSCopyService() 時得到錯誤的回傳，VideoID = ", videoID));

                // 更新到 NAS_MOVING 的資料表
                if (!fnInsertNasFileMovingTable(mssCopyID))
                    throw new Exception(string.Concat("進行 fnUpdateNasFileMovingTable() 時得到錯誤的回傳，VideoID = ", videoID));

                return true;
            }
            catch (Exception ex)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("RegisterNasFileMovingService", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, ex.ToString());
                return false;
            }
            finally
            {
                objSR.Dispose();
                GC.Collect();
            }
        }

        /// <summary>
        /// 供前端 Silverlight 呼叫，重置 NasMoving 服務的程式
        /// </summary>
        /// <param name="objNasMovingJob"></param>
        /// <returns></returns>
        [WebMethod]
        public bool RetryNasFileMovingService(ClassNasMovingJob objNasMovingJob)
        {
            // 刪除原有的檔案，盡量讓後面資料庫的內容與實際的一致
            try
            {
                // 刪除 token 及 目標檔
                System.IO.File.Delete(string.Concat(objNasMovingJob.DirDest, "header"));
                System.IO.File.Delete(string.Concat(objNasMovingJob.DirDest, "ft"));
                System.IO.File.Delete(string.Concat(objNasMovingJob.DirDest, "std"));
                // 理論上是要刪除 TokenFile，要記得與 msscopy 的 TokenFile Ext Name 同步
                System.IO.File.Delete(string.Concat(objNasMovingJob.DirDest.Substring(0, objNasMovingJob.DirDest.Length - 1), ".finish"));
            }
            catch (Exception ex)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_NasFileMoving/RetryNasFileMovingService", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "JobID = " + objNasMovingJob.mssCopyID + ", " + ex.ToString());
            }
            // 更新資料庫內容並回傳結果
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FNMSSCOPY_ID", objNasMovingJob.mssCopyID);
            sqlParameters.Add("STATUS_INITIAL_VALUE", ((int)ServiceMSSCopy.MSSCOPY_STATUS.INITIAL).ToString());
            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_MSSCOPY_INITIALSTATUS", sqlParameters, "system");
        }

        /// <summary>
        /// 供前端 Silverlight 查詢日期範圍區間內的工作狀態
        /// </summary>
        /// <param name="dateStart"></param>
        /// <param name="dateEnd"></param>
        /// <returns></returns>
        [WebMethod]
        public List<ClassNasMovingJob> GetNasFileMovingJobInformation(string dateStart, string dateEnd)
        {
            // 抓出範圍區間的資料
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("TIME_START", dateStart);
            sqlParameters.Add("TIME_END", dateEnd);
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBPGM_NASMOVING_JOBS", sqlParameters, out resultData) || resultData.Count == 0)
                return new List<ClassNasMovingJob>();

            // 解析內容
            List<ClassNasMovingJob> result = new List<ClassNasMovingJob>();
            for (int i = 0; i < resultData.Count; i++)
            {
                ClassNasMovingJob obj = new ClassNasMovingJob(
                    resultData[i]["FDREQUEST_TIME"], 
                    resultData[i]["FSVIDEO_ID"], 
                    resultData[i]["FSDIRFROM"], 
                    resultData[i]["FSDIRTO"], 
                    resultData[i]["FSPROC_WORKER"], 
                    resultData[i]["FDSTART_TIME"], 
                    resultData[i]["FDFINISH_TIME"], 
                    resultData[i]["FNMSSCOPY_ID"], 
                    resultData[i]["FNSTATUS"]);
                result.Add(obj);
            }

            // 
            return result;
        }

        /// <summary>
        /// 取回NAS上目錄的路徑
        /// 範例：\\10.10.10.10\Transcode\
        /// 範例：\\10.10.10.10\Transcode\A0000001\ (最後面會帶\)
        /// </summary>
        /// <param name="type">NAS_STORAGE_QUERY_ITEMS的例舉值字串內容</param>
        /// <param name="videoID">VideoID</param>
        /// <returns></returns>
        [WebMethod]
        public string GetNasStoragePath(string type, string videoID = "")
        {
            // 檢查輸入的內容是否存在於 enum 裡
            if (!Enum.IsDefined(typeof(NAS_STORAGE_QUERY_ITEMS), type))
                return string.Empty;
            // 傳給後面的多型做處理
            return GetNasStoragePath((NAS_STORAGE_QUERY_ITEMS)Enum.Parse(typeof(NAS_STORAGE_QUERY_ITEMS), type), videoID);
        }

        /// <summary>
        /// 取回NAS上目錄的路徑
        /// 範例：\\10.10.10.10\Transcode\
        /// 範例：\\10.10.10.10\Transcode\A0000001\ (最後面會帶\)
        /// </summary>
        /// <param name="type">NAS_STORAGE_QUERY_ITEMS的列舉值</param>
        /// <param name="videoID">VideoID</param>
        /// <returns></returns>
        public string GetNasStoragePath(NAS_STORAGE_QUERY_ITEMS type, string videoID = "")
        {
            // 有需要的話，在 VideoID 後面加上 \ 符號，方便後面的運算
            if (!string.IsNullOrEmpty(videoID))
                videoID = string.Concat(videoID, "\\");

            switch (type)
            {
                // 查詢NAS_Server的IP位址，所以不用加 VideoID (如果有加會自動忽略)
                case NAS_STORAGE_QUERY_ITEMS.NAS_SERVER:
                    return NAS_STORAGE;

                case NAS_STORAGE_QUERY_ITEMS.NAS_TRANSCODE:
                    return string.Concat(NAS_STORAGE_TRANSCODE, videoID);

                case NAS_STORAGE_QUERY_ITEMS.NAS_SEND:
                    return string.Concat(NAS_STORAGE_SEND, videoID);

                case NAS_STORAGE_QUERY_ITEMS.NAS_MSG:
                    return string.Concat(NAS_STORAGE_MSG, videoID);
            }

            // 不會到這裡
            return NAS_STORAGE;
        }

        /// <summary>
        /// 觸發主控內部的FTP程式重新抓取當日的FTP清單
        /// </summary>
        [WebMethod]
        public bool RenewFtpList()
        {
            MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_NasFileMoving/RenewFtpList", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "START");
            MAM_PTS_DLL.SysConfig obj = new MAM_PTS_DLL.SysConfig();

            string dirPath = obj.sysConfig_Read("/ServerConfig/NAS/MASTERCONTROL_NAS_FTPLIST");

            // 刪除註記檔
            if (!string.IsNullOrEmpty(dirPath) && System.IO.Directory.Exists(dirPath))
            {
                string filePath = string.Empty;
                try
                {
                    filePath = string.Concat(dirPath, DateTime.Now.ToString("yyyyMMdd"), ".ftplist1");
                    if (System.IO.File.Exists(filePath))
                        System.IO.File.Delete(filePath);
                    filePath = string.Concat(dirPath, DateTime.Now.ToString("yyyyMMdd"), ".ftplist1.ok");
                    if (System.IO.File.Exists(filePath))
                        System.IO.File.Delete(filePath);
                    filePath = string.Concat(dirPath, DateTime.Now.ToString("yyyyMMdd"), ".ftplist1.finish");
                    if (System.IO.File.Exists(filePath))
                        System.IO.File.Delete(filePath);
                    // 這樣寫比較不好，如果刪一半斷了，後面就做不到了
                    filePath = string.Concat(dirPath, DateTime.Now.ToString("yyyyMMdd"), ".ftplist2");
                    if (System.IO.File.Exists(filePath))
                        System.IO.File.Delete(filePath);
                    filePath = string.Concat(dirPath, DateTime.Now.ToString("yyyyMMdd"), ".ftplist2.ok");
                    if (System.IO.File.Exists(filePath))
                        System.IO.File.Delete(filePath);
                    filePath = string.Concat(dirPath, DateTime.Now.ToString("yyyyMMdd"), ".ftplist2.finish");
                    if (System.IO.File.Exists(filePath))
                        System.IO.File.Delete(filePath);
                }
                catch (Exception ex)
                {
                    string errMsg = string.Concat("Error: Can't delete file '", filePath, "', Msg = ", ex.ToString());
                    MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_NasFileMoving/RenewFtpList", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                    return false;
                }
            }

            return true;
        }

        private bool fnInsertNasFileMovingTable(long msscopyID)
        {
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FNMSSCOPY_ID", msscopyID.ToString());
            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_I_TBPGM_NASMOVING", sqlParameters, "system");
        }
    }
}
