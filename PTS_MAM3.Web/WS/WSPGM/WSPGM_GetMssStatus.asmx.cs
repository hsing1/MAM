﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace PTS_MAM3.Web.WS.WSPGM
{
    /// <summary>
    ///Service1 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public partial class ServicePGM : System.Web.Services.WebService
    {
        /// <summary>
        /// 主控播出檔的狀態列表
        /// </summary>
        public enum MSS_FILE_STATUS
        {
            FTP1OK_FTP2OK, FTP1OK_FTP2EXIST, FTP1OK_FTP2ERR, FTP1OK_FTP2PROC, FTP1OK_FTP2NOTSTART,
            FTP1EXIST_FTP2OK, FTP1EXIST_FTP2EXIST, FTP1EXIST_FTP2PROC, FTP1EXIST_FTP2ERR, FTP1EXIST_FTP2NOSTART,
            FTP1PROC_FTP2OK, FTP1PROC_FTP2EXIST, FTP1PROC_FTP2PROC, FTP1PROC_FTP2ERR, FTP1PROC_FTP2NOTSTART,
            FTP1ERR_FTP2OK, FTP1ERR_FTP2EXIST, FTP1ERR_FTP2PROC, FTP1ERR_FTP2ERR, FTP1ERR_FTP2NOTSTART,
            FTP1NOTSTART_FTP2OK, FTP1NOTSTART_FTP2EXIST, FTP1NOTSTART_FTP2PROC, FTP1NOTSTART_FTP2ERR,
            MSSCOPY_OK, MSSCOPY_PROC, MSSCOPY_ERR,
            MSSPROCESS_OK, MSSPROCESS_PROC, MSSPROCESS_ERR,
            IMX50_OK, IMX50_PROC, IMX50_ERR, IMX50_NOTEXIST,
            SYSERROR
        };

        /// <summary>
        /// 供排表系統檢查 MSS 格式媒體檔的狀態，詳細邏輯請參考 '' PGM_MSS檔案查詢狀態列表 - 更新.xlsx ''
        /// </summary>
        /// <param name="videoID"></param>
        /// <returns></returns>
        [WebMethod]
        public MSS_FILE_STATUS GetFileStatus_MSS(string videoID)
        {
            ServiceNasFileMoving obj = new ServiceNasFileMoving();

            string FTP1_SYMBOL_OK = string.Concat(obj.NAS_STORAGE_SEND, videoID, ".ftpok1");
            string FTP2_SYMBOL_OK = string.Concat(obj.NAS_STORAGE_SEND, videoID, ".ftpok2");
            string FTP1_SYMBOL_EXIST = string.Concat(obj.NAS_STORAGE_SEND, videoID, ".ftpexist1");
            string FTP2_SYMBOL_EXIST = string.Concat(obj.NAS_STORAGE_SEND, videoID, ".ftpexist2");
            string FTP1_SYMBOL_ERR = string.Concat(obj.NAS_STORAGE_SEND, videoID, ".ftperr1");
            string FTP2_SYMBOL_ERR = string.Concat(obj.NAS_STORAGE_SEND, videoID, ".ftperr2");
            string FTP1_SYMBOL_PROC = string.Concat(obj.NAS_STORAGE_SEND, videoID, ".ftpproc1");
            string FTP2_SYMBOL_PROC = string.Concat(obj.NAS_STORAGE_SEND, videoID, ".ftpproc2");

            // 檢查 FTP SYMBOL 是否存在 → 用於判斷 NOSTART 的狀態
            bool EXIST_FTP1_SYMBOL = false;
            bool EXIST_FTP2_SYMBOL = false;

            bool EXIST_FTP1_SYMBOL_OK = File.Exists(FTP1_SYMBOL_OK);
            bool EXIST_FTP2_SYMBOL_OK = File.Exists(FTP2_SYMBOL_OK);

            if (EXIST_FTP1_SYMBOL_OK && EXIST_FTP2_SYMBOL_OK)
                return MSS_FILE_STATUS.FTP1OK_FTP2OK;       // 1

            bool EXIST_FTP2_SYMBOL_EXIST = File.Exists(FTP2_SYMBOL_EXIST);

            if (EXIST_FTP1_SYMBOL_OK && EXIST_FTP2_SYMBOL_EXIST)
                return MSS_FILE_STATUS.FTP1OK_FTP2EXIST;    // 2

            bool EXIST_FTP2_SYMBOL_PROC = File.Exists(FTP2_SYMBOL_PROC);

            if (EXIST_FTP1_SYMBOL_OK && EXIST_FTP2_SYMBOL_PROC)
                return MSS_FILE_STATUS.FTP1OK_FTP2PROC;     // 3

            bool EXIST_FTP2_SYMBOL_ERR = File.Exists(FTP2_SYMBOL_ERR);

            if (EXIST_FTP1_SYMBOL_OK && EXIST_FTP2_SYMBOL_ERR)
                return MSS_FILE_STATUS.FTP1OK_FTP2ERR;      // 4

            if (EXIST_FTP2_SYMBOL_OK || EXIST_FTP2_SYMBOL_EXIST || EXIST_FTP2_SYMBOL_PROC || EXIST_FTP2_SYMBOL_ERR)
                EXIST_FTP2_SYMBOL = true;

            if (EXIST_FTP1_SYMBOL_OK && !EXIST_FTP2_SYMBOL)
                return MSS_FILE_STATUS.FTP1OK_FTP2NOTSTART; // 5

            bool EXIST_FTP1_SYMBOL_EXIST = File.Exists(FTP1_SYMBOL_EXIST);

            if (EXIST_FTP1_SYMBOL_EXIST && EXIST_FTP2_SYMBOL_OK)
                return MSS_FILE_STATUS.FTP1EXIST_FTP2OK;      // 6

            if (EXIST_FTP1_SYMBOL_EXIST && EXIST_FTP2_SYMBOL_EXIST)
                return MSS_FILE_STATUS.FTP1EXIST_FTP2EXIST; // 7

            if (EXIST_FTP1_SYMBOL_EXIST && EXIST_FTP2_SYMBOL_PROC)
                return MSS_FILE_STATUS.FTP1EXIST_FTP2PROC;    // 8

            if (EXIST_FTP1_SYMBOL_EXIST && EXIST_FTP2_SYMBOL_ERR)
                return MSS_FILE_STATUS.FTP1EXIST_FTP2ERR;   // 9

            if (EXIST_FTP1_SYMBOL_EXIST && !EXIST_FTP2_SYMBOL)
                return MSS_FILE_STATUS.FTP1EXIST_FTP2NOSTART;   // 10

            bool EXIST_FTP1_SYMBOL_PROC = File.Exists(FTP1_SYMBOL_PROC);

            if (EXIST_FTP1_SYMBOL_PROC && EXIST_FTP2_SYMBOL_OK)
                return MSS_FILE_STATUS.FTP1PROC_FTP2OK;     // 11

            if (EXIST_FTP1_SYMBOL_PROC && EXIST_FTP2_SYMBOL_EXIST)
                return MSS_FILE_STATUS.FTP1PROC_FTP2EXIST;  // 12

            if (EXIST_FTP1_SYMBOL_PROC && EXIST_FTP2_SYMBOL_PROC)
                return MSS_FILE_STATUS.FTP1PROC_FTP2PROC;   // 13

            if (EXIST_FTP1_SYMBOL_PROC && EXIST_FTP2_SYMBOL_ERR)
                return MSS_FILE_STATUS.FTP1PROC_FTP2ERR;    // 14

            if (EXIST_FTP1_SYMBOL_PROC && !EXIST_FTP2_SYMBOL)
                return MSS_FILE_STATUS.FTP1PROC_FTP2NOTSTART;   // 15

            bool EXIST_FTP1_SYMBOL_ERR = File.Exists(FTP1_SYMBOL_ERR);

            if (EXIST_FTP1_SYMBOL_ERR && EXIST_FTP2_SYMBOL_OK)
                return MSS_FILE_STATUS.FTP1ERR_FTP2OK;      // 16

            if (EXIST_FTP1_SYMBOL_ERR && EXIST_FTP2_SYMBOL_EXIST)
                return MSS_FILE_STATUS.FTP1ERR_FTP2EXIST;   // 17

            if (EXIST_FTP1_SYMBOL_ERR && EXIST_FTP2_SYMBOL_PROC)
                return MSS_FILE_STATUS.FTP1ERR_FTP2PROC;    // 18

            if (EXIST_FTP1_SYMBOL_ERR && EXIST_FTP2_SYMBOL_ERR)
                return MSS_FILE_STATUS.FTP1ERR_FTP2ERR;     // 19

            if (EXIST_FTP1_SYMBOL_ERR && !EXIST_FTP2_SYMBOL)
                return MSS_FILE_STATUS.FTP1ERR_FTP2NOTSTART;    // 20

            if (EXIST_FTP1_SYMBOL_OK || EXIST_FTP1_SYMBOL_EXIST || EXIST_FTP1_SYMBOL_PROC || EXIST_FTP1_SYMBOL_ERR)
                EXIST_FTP1_SYMBOL = true;

            if (!EXIST_FTP1_SYMBOL && EXIST_FTP2_SYMBOL_OK)
                return MSS_FILE_STATUS.FTP1NOTSTART_FTP2OK;     // 21

            if (!EXIST_FTP1_SYMBOL && EXIST_FTP2_SYMBOL_EXIST)
                return MSS_FILE_STATUS.FTP1NOTSTART_FTP2EXIST;  // 22

            if (!EXIST_FTP1_SYMBOL && EXIST_FTP2_SYMBOL_PROC)
                return MSS_FILE_STATUS.FTP1NOTSTART_FTP2PROC;   // 23

            if (!EXIST_FTP1_SYMBOL && EXIST_FTP2_SYMBOL_ERR)
                return MSS_FILE_STATUS.FTP1NOTSTART_FTP2ERR;    // 24

            string MSSCOPY_SYMBOL_OK = string.Concat(obj.NAS_STORAGE_SEND, videoID, ".msscopyok");
            string MSSCOPY_SYMBOL_PROC = string.Concat(obj.NAS_STORAGE_SEND, videoID, ".msscopyproc");
            string MSSCOPY_SYMBOL_ERR = string.Concat(obj.NAS_STORAGE_SEND, videoID, ".msscopyerr");

            bool EXIST_MSSCOPY_SYMBOL_OK = File.Exists(MSSCOPY_SYMBOL_OK);
            if (EXIST_MSSCOPY_SYMBOL_OK)
                return MSS_FILE_STATUS.MSSCOPY_OK;              // 26

            bool EXIST_MSSCOPY_SYMBOL_PROC = File.Exists(MSSCOPY_SYMBOL_PROC);
            if (EXIST_MSSCOPY_SYMBOL_PROC)
                return MSS_FILE_STATUS.MSSCOPY_PROC;            // 27

            bool EXIST_MSSCOPY_SYMBOL_ERR = File.Exists(MSSCOPY_SYMBOL_ERR);
            if (EXIST_MSSCOPY_SYMBOL_ERR)
                return MSS_FILE_STATUS.MSSCOPY_ERR;             // 28

            string MSSPROCESS_SYMBOL_OK = string.Concat(obj.NAS_STORAGE_TRANSCODE, videoID, ".msscopyok");
            bool EXIST_MSSPROCESS_SYMBOL_OK = File.Exists(MSSPROCESS_SYMBOL_OK);
            if (EXIST_MSSPROCESS_SYMBOL_OK)
                return MSS_FILE_STATUS.MSSPROCESS_OK;           // 30

            // Tricky: 這邊要先檢查 IMX50 的狀態，才能確保 MSSPROCESS 剩下來的狀態是正確的

            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSVIDEO_ID", videoID);
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_MSSPROCESS_JOBDETAIL", sqlParameters, out resultData))
                return MSS_FILE_STATUS.SYSERROR;
            if (resultData.Count == 0)
                return MSS_FILE_STATUS.IMX50_NOTEXIST;          // 無法從 TBLOG_VIDEO_SEG 關聯到 TBLOG_VIDEO，有可能是沒有 T 或 Y 的資料

            switch (resultData[0]["FCLOW_RES_IMX50"])
            {
                case "Y":
                    // 還需要往後去判斷
                    break;
                case "R":
                    return MSS_FILE_STATUS.IMX50_PROC;          // 32
                case "N":
                    return MSS_FILE_STATUS.IMX50_NOTEXIST;      // 31，雖然有 T 或 Y 的資料，但是還沒有進行高解轉檔
                case "F":
                    return MSS_FILE_STATUS.IMX50_ERR;           // 33
                default:
                    // 不應出現這種狀態
                    return MSS_FILE_STATUS.SYSERROR;            // 
            }

            resultData.Clear();
            resultData = new List<Dictionary<string, string>>();
            sqlParameters.Clear();
            sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSVIDEO_ID", videoID);
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_MSSPROCESS_JOBSTATUS_VIDEOID", sqlParameters, out resultData))
                return MSS_FILE_STATUS.SYSERROR;                // 

            // 如果找不到處理資訊，或是今日沒有處理項目
            if (resultData.Count == 0 || (resultData[0]["FNMSSPROCESS_ID"].Substring(0, 8) != DateTime.Now.ToString("yyyyMMdd")))
                return MSS_FILE_STATUS.IMX50_OK;                // 37
            ServiceMSSProcess.MSSPROCESS_STATUS status = (ServiceMSSProcess.MSSPROCESS_STATUS)(Int32.Parse(resultData[0]["FNSTATUS"]));
            if (status == ServiceMSSProcess.MSSPROCESS_STATUS.ERR_MSSCOPY || status == ServiceMSSProcess.MSSPROCESS_STATUS.ERR_SYSTEM ||
                status == ServiceMSSProcess.MSSPROCESS_STATUS.ERR_TRANS || status == ServiceMSSProcess.MSSPROCESS_STATUS.ERR_TSMJOB)
                return MSS_FILE_STATUS.MSSPROCESS_ERR;          // 35
            if (status == ServiceMSSProcess.MSSPROCESS_STATUS.INITIAL || status == ServiceMSSProcess.MSSPROCESS_STATUS.PROC_MSSCOPY ||
                status == ServiceMSSProcess.MSSPROCESS_STATUS.PROC_TRANS || status == ServiceMSSProcess.MSSPROCESS_STATUS.PROC_TSMJOB ||
                status == ServiceMSSProcess.MSSPROCESS_STATUS.WAIT_IMX50)
                return MSS_FILE_STATUS.MSSPROCESS_PROC;         // 34

            return MSS_FILE_STATUS.SYSERROR;                    // 
        }

        /// <summary>
        /// 取得在NAS中(含Transcode及Send目錄)的檔案狀態
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public List<ClassNasFileObject> GetNasFileList()
        {
            MAM_PTS_DLL.SysConfig obj = new MAM_PTS_DLL.SysConfig();
            string NasTranscode = obj.sysConfig_Read("/ServerConfig/NAS/MASTERCONTROL_NAS_TRANSCODE");
            string NasSend = obj.sysConfig_Read("/ServerConfig/NAS/MASTERCONTROL_NAS_SEND");

            // 取得 NAS 內的目錄及檔案列表
            List<string> NasTranscodeDirs;
            List<string> NasTranscodeFiles;
            List<string> NasSendDirs;
            List<string> NasSendFiles;
            MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_GetMssStatus/GetNasFileList", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "Start: " + DateTime.Now.ToString());
            try
            {
                NasTranscodeDirs = new List<string>(Directory.GetDirectories(NasTranscode));
                NasTranscodeFiles = new List<string>(Directory.GetFiles(NasTranscode));
                NasSendDirs = new List<string>(Directory.GetDirectories(NasSend));
                NasSendFiles = new List<string>(Directory.GetFiles(NasSend));
            }
            catch (Exception ex)
            {
                string errMsg = string.Concat("Can't get NAS content: ", ex.ToString());
                MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_GetMssStatus/GetNasFileList", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return new List<ClassNasFileObject>();
            }
            MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_GetMssStatus/GetNasFileList", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "End: " + DateTime.Now.ToString());

            // 以下會開始分析擷取到的內容
            List<ClassNasFileObject> resultList = new List<ClassNasFileObject>();
            // 記錄已經存在於 Transcode 的 VideoID
            List<string> dummyList = new List<string>();

            // 先檢查 Transcode
            foreach (string dirPath in NasTranscodeDirs)
            {
                // Tricky: 當 dirPath 尾端沒有 \ 時，用 GetFileName 可以取得最後一層的目錄名稱
                string videoID = Path.GetFileName(dirPath);
                // 成功的SYMBOL檔
                string symbolFile = string.Concat(dirPath, ".msscopyok");
                // 
                if (NasTranscodeFiles.Contains(symbolFile))
                {
                    ClassNasFileObject objNasFile = new ClassNasFileObject();
                    objNasFile.VideoID = videoID;
                    objNasFile.InTranscodeDir = "Y";
                    objNasFile.Note = string.Empty;
                    dummyList.Add(videoID);
                    resultList.Add(objNasFile);
                }
            }

            // 再檢查 Send 目錄
            foreach (string dirPath in NasSendDirs)
            {
                // Tricky: 當 dirPath 尾端沒有 \ 時，用 GetFileName 可以取得最後一層的目錄名稱
                string videoID = Path.GetFileName(dirPath);
                // 成功的SYMBOL檔
                string symbolFile = string.Concat(dirPath, ".msscopyok");
                if (NasSendFiles.Contains(symbolFile))
                {
                    if (!dummyList.Contains(videoID))
                    {
                        // 如果不存在於 Transcode 就再建立一個
                        ClassNasFileObject objNasFile = new ClassNasFileObject();
                        objNasFile.VideoID = videoID;
                        objNasFile.InSendDir = "Y";
                        objNasFile.Note = string.Empty;
                        resultList.Add(objNasFile);
                    }
                    else
                    {
                        // 如果已存在，就從剛做出來的 List 再處理
                        foreach (ClassNasFileObject fileObj in resultList)
                        {
                            if (fileObj.VideoID == videoID)
                            {
                                fileObj.InSendDir = "Y";
                                break;
                            }
                        }
                    }
                }
            }

            MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_GetMssStatus/GetNasFileList", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "Last: " + DateTime.Now.ToString());

            // ---------------------------------------------------------------------
            // 在這邊因為連線數實在太多，會讓前端介面 Timeout，所以我直接跟 SQL 連線
            // ---------------------------------------------------------------------

            System.Data.SqlClient.SqlConnection objSqlConnection = new System.Data.SqlClient.SqlConnection(MAM_PTS_DLL.DbAccess.sqlConnStr);
            System.Data.SqlClient.SqlCommand objSqlCommand = null;
            System.Data.SqlClient.SqlDataReader objSqlDataReader = null;
            try
            {
                objSqlConnection.Open();
            }
            catch (Exception ex)
            {
                // 如果資料庫連線發生錯誤，就不回傳詳細資訊了
                MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_GetMssStatus/GetNasFileList", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, string.Concat("與 SQL 的連線發生錯誤：", ex.ToString()));
                // 發生錯誤時，就將 Connection 的資訊釋放掉
                try
                {
                    if (objSqlConnection != null)
                        objSqlConnection.Dispose();
                }
                catch
                {
                    //
                }
                return resultList;
            }

            // 最後再針對每一個 VideoID 做詢查的處理
            foreach (ClassNasFileObject tmpObj in resultList)
            {
                try
                {
                    objSqlCommand = new System.Data.SqlClient.SqlCommand(string.Empty, objSqlConnection);
                    objSqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    // 只能用這種方法去判斷長短帶，有點不好，這個方式與 MSSPROCESS 的相同
                    if (tmpObj.VideoID.Length == 8)
                        objSqlCommand.CommandText = "SP_Q_MSSPROCESS_JOBNAME2";
                    else
                        objSqlCommand.CommandText = "SP_Q_MSSPROCESS_JOBNAME";
                    objSqlCommand.Parameters.AddWithValue("@FSVIDEO_ID", tmpObj.VideoID);
                    objSqlDataReader = objSqlCommand.ExecuteReader();
                    if (objSqlDataReader.HasRows)
                    {
                        objSqlDataReader.Read();
                        tmpObj.Note = string.Format("【{0}】{1}，(集數：{2}，段落：{3})", objSqlDataReader["FSFILE_NO"], objSqlDataReader["NAME"], objSqlDataReader["FNEPISODE"], objSqlDataReader["FNSEG_ID"]);
                    }
                }
                catch (Exception ex)
                {
                    // 如果查詢資料庫時發生錯誤，就該筆不回傳詳細資訊
                    MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_GetMssStatus/GetNasFileList", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, string.Concat("查詢 SQL時發生錯誤：", ex.ToString()));
                    continue;
                }
                finally
                {
                    try
                    {
                        if (objSqlDataReader != null)
                            objSqlDataReader.Dispose();
                        if (objSqlCommand != null)
                            objSqlCommand.Dispose();
                    }
                    catch
                    {
                        //
                    }
                    GC.Collect();
                }
               
                // 如果發生資料庫錯誤，就回傳空白
            }

            // 釋放 SqlConnection 的資源
            try
            {
                if (objSqlConnection != null)
                    objSqlConnection.Dispose();
            }
            catch
            {
                //
            }

            return resultList;
        }

        /// <summary>
        /// 刪除在 NAS 中的檔案 (會一次性的刪除NAS_A及NAS_B目錄內的資料及SYMBOL)
        /// </summary>
        /// <param name="videoID">要刪除的VideoID</param>
        /// <returns></returns>
        [WebMethod]
        public bool DeleteNasFile(string videoID)
        {
            MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_GetMssStatus/DeleteNasFile", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, string.Concat("START：", videoID));

            // 檢查 VideoID 的格式是否符合，如果不符合，就直接 Return 掉
            if (!MAM_PTS_DLL.CheckFormat.CheckFormat_VideoID(videoID))
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_GetMssStatus/DeleteNasFile", MAM_PTS_DLL.Log.TRACKING_LEVEL.WARNING, string.Concat("無效的參數：", videoID));
                return false;
            }

            // 取得 NAS Storage 的相關資訊
            MAM_PTS_DLL.SysConfig obj = new MAM_PTS_DLL.SysConfig();
            string NasTranscode = obj.sysConfig_Read("/ServerConfig/NAS/MASTERCONTROL_NAS_TRANSCODE");
            string NasSend = obj.sysConfig_Read("/ServerConfig/NAS/MASTERCONTROL_NAS_SEND");

            // 開始刪除在 Transcode 及 Send 的相關檔案
            try
            {
                // 刪除在 Transcode 中的目錄 (如果有的話)
                string deleteDir = string.Concat(NasTranscode, videoID);
                if (Directory.Exists(deleteDir))
                    Directory.Delete(deleteDir, true);
                // 刪除在 Transcode 中的檔案 (如果有的話)
                string deleteFile = string.Concat(deleteDir, ".msscopyok");
                if (File.Exists(deleteFile))
                    File.Delete(deleteFile);
                deleteFile = string.Concat(deleteDir, ".msscopyerr");
                if (File.Exists(deleteFile))
                    File.Delete(deleteFile);

                // 刪除在 Send 中的目錄 (如果有的話)
                deleteDir = string.Concat(NasSend, videoID);
                if (Directory.Exists(deleteDir))
                    Directory.Delete(deleteDir, true);
                // 刪除在 Send 中的檔案 (如果有的話)
                deleteFile = string.Concat(deleteDir, ".msscopyok");
                if (File.Exists(deleteFile))
                    File.Delete(deleteFile);
                deleteFile = string.Concat(deleteDir, ".msscopyerr");
                if (File.Exists(deleteFile))
                    File.Delete(deleteFile);
                deleteFile = string.Concat(deleteDir, ".ftpok1");
                if (File.Exists(deleteFile))
                    File.Delete(deleteFile);
                deleteFile = string.Concat(deleteDir, ".ftpok2");
                if (File.Exists(deleteFile))
                    File.Delete(deleteFile);
                deleteFile = string.Concat(deleteDir, ".ftpexist1");
                if (File.Exists(deleteFile))
                    File.Delete(deleteFile);
                deleteFile = string.Concat(deleteDir, ".ftpexist2");
                if (File.Exists(deleteFile))
                    File.Delete(deleteFile);
                deleteFile = string.Concat(deleteDir, ".ftperr1");
                if (File.Exists(deleteFile))
                    File.Delete(deleteFile);
                deleteFile = string.Concat(deleteDir, ".ftperr2");
                if (File.Exists(deleteFile))
                    File.Delete(deleteFile);
                // 較危險
                deleteFile = string.Concat(deleteDir, ".ftpproc1");
                if (File.Exists(deleteFile))
                    File.Delete(deleteFile);
                // 較危險
                deleteFile = string.Concat(deleteDir, ".ftpproc2");
                if (File.Exists(deleteFile))
                    File.Delete(deleteFile);
            }
            catch (Exception ex)
            {
                string errMsg = string.Concat("刪檔錯誤：", ex.ToString());
                MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_GetMssStatus/DeleteNasFile", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return false;
            }

            return true;
        }

        /// <summary>
        /// 取得並轉譯所有 FTP 派送任務的狀態
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public List<ClassFTPJob> GetMasterControlFtpStatus()
        {
            MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_GetMssStatus/GetMasterControlFtpStatus", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "START");

            // 取得 NAS Storage 的相關資訊
            MAM_PTS_DLL.SysConfig obj = new MAM_PTS_DLL.SysConfig();
            string NasSend = obj.sysConfig_Read("/ServerConfig/NAS/MASTERCONTROL_NAS_SEND");

            // 取得 NAS 內 Send 目錄及檔案列表
            List<string> NasSendDirs;
            List<string> NasSendFiles;
            MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_GetMssStatus/GetMasterControlFtpStatus", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "Start: " + DateTime.Now.ToString());
            try
            {
                NasSendDirs = new List<string>(Directory.GetDirectories(NasSend));
                NasSendFiles = new List<string>(Directory.GetFiles(NasSend));
            }
            catch (Exception ex)
            {
                string errMsg = string.Concat("Can't get NAS content: ", ex.ToString());
                MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_GetMssStatus/GetMasterControlFtpStatus", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return new List<ClassFTPJob>();
            }

            // 以下會開始分析擷取到的內容
            List<ClassFTPJob> resultList = new List<ClassFTPJob>();
            foreach (string dirPath in NasSendDirs)
            {
                ClassFTPJob objFTPJob = new ClassFTPJob();
                // Tricky: 當目錄名稱的後面沒有加 \ 時，用 GetFileName 讀取可以讀到目錄名稱
                objFTPJob.VideoID = Path.GetFileName(dirPath);
                // 檢查 SYMBOL FILE，看成功與否
                if (File.Exists(string.Concat(dirPath, ".ftpok1")))
                    objFTPJob.Ftp1Status = "傳檔完成";
                else if (File.Exists(string.Concat(dirPath, ".ftpexist1")))
                    objFTPJob.Ftp1Status = "檔案已存在";
                else if (File.Exists(string.Concat(dirPath, ".ftperr1")))
                    objFTPJob.Ftp1Status = "派送失敗";
                else if (File.Exists(string.Concat(dirPath, ".ftpproc1")))
                    objFTPJob.Ftp1Status = "處理中";
                else
                    objFTPJob.Ftp1Status = "尚未處理";
                if (File.Exists(string.Concat(dirPath, ".ftpok2")))
                    objFTPJob.Ftp2Status = "傳檔完成";
                else if (File.Exists(string.Concat(dirPath, ".ftpexist2")))
                    objFTPJob.Ftp2Status = "檔案已存在";
                else if (File.Exists(string.Concat(dirPath, ".ftperr2")))
                    objFTPJob.Ftp2Status = "派送失敗";
                else if (File.Exists(string.Concat(dirPath, ".ftpproc2")))
                    objFTPJob.Ftp2Status = "處理中";
                else
                    objFTPJob.Ftp2Status = "尚未處理";
                //
                resultList.Add(objFTPJob);
            }
            MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_GetMssStatus/GetMasterControlFtpStatus", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "End: " + DateTime.Now.ToString());

            // ---------------------------------------------------------------------
            // 光處理 I/O 的部份就已經快 Timeout 了，所以這邊先不串資料表
            // ---------------------------------------------------------------------

            //System.Data.SqlClient.SqlConnection objSqlConnection = new System.Data.SqlClient.SqlConnection(MAM_PTS_DLL.DbAccess.sqlConnStr);
            //System.Data.SqlClient.SqlCommand objSqlCommand = null;
            //System.Data.SqlClient.SqlDataReader objSqlDataReader = null;
            //try
            //{
            //    objSqlConnection.Open();
            //}
            //catch (Exception ex)
            //{
            //    // 如果資料庫連線發生錯誤，就不回傳詳細資訊了
            //    MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_GetMssStatus/GetNasFileList", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, string.Concat("與 SQL 的連線發生錯誤：", ex.ToString()));
            //    // 發生錯誤時，就將 Connection 的資訊釋放掉
            //    try
            //    {
            //        if (objSqlConnection != null)
            //            objSqlConnection.Dispose();
            //    }
            //    catch
            //    {
            //        //
            //    }
            //    return resultList;
            //}

            // 最後再針對每一個 VideoID 做詢查的處理
            //foreach (ClassFTPJob tmpObj in resultList)
            //{
            //    try
            //    {
            //        objSqlCommand = new System.Data.SqlClient.SqlCommand(string.Empty, objSqlConnection);
            //        objSqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            //        // 只能用這種方法去判斷長短帶，有點不好，這個方式與 MSSPROCESS 的相同
            //        if (tmpObj.VideoID.Length == 8)
            //            objSqlCommand.CommandText = "SP_Q_MSSPROCESS_JOBNAME2";
            //        else
            //            objSqlCommand.CommandText = "SP_Q_MSSPROCESS_JOBNAME";
            //        objSqlCommand.Parameters.AddWithValue("@FSVIDEO_ID", tmpObj.VideoID);
            //        objSqlDataReader = objSqlCommand.ExecuteReader();
            //        if (objSqlDataReader.HasRows)
            //        {
            //            objSqlDataReader.Read();
            //            tmpObj.Note = string.Format("【{0}】{1}，(集數：{2}，段落：{3})", objSqlDataReader["FSFILE_NO"], objSqlDataReader["NAME"], objSqlDataReader["FNEPISODE"], objSqlDataReader["FNSEG_ID"]);
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        // 如果查詢資料庫時發生錯誤，就該筆不回傳詳細資訊
            //        MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_GetMssStatus/GetNasFileList", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, string.Concat("查詢 SQL時發生錯誤：", ex.ToString()));
            //        continue;
            //    }
            //    finally
            //    {
            //        try
            //        {
            //            if (objSqlDataReader != null)
            //                objSqlDataReader.Dispose();
            //            if (objSqlCommand != null)
            //                objSqlCommand.Dispose();
            //        }
            //        catch
            //        {
            //            //
            //        }
            //        GC.Collect();
            //    }

            //    // 如果發生資料庫錯誤，就回傳空白
            //}

            // 釋放 SqlConnection 的資源
            //try
            //{
            //    if (objSqlConnection != null)
            //        objSqlConnection.Dispose();
            //}
            //catch
            //{
            //    //
            //}

            return resultList;
        }

        /// <summary>
        /// 取得單筆 FTP 派送任務的詳細資訊
        /// </summary>
        /// <param name="videoID"></param>
        /// <returns></returns>
        [WebMethod]
        public ClassFTPJob GetMasterControlFtpDetailStatus(string videoID)
        {
            MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_GetMssStatus/GetMasterControlFtpDetailStatus", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, string.Concat("START：", videoID));

            // 檢查 VideoID 的格式是否符合，如果不符合，就直接 Return 掉
            if (!MAM_PTS_DLL.CheckFormat.CheckFormat_VideoID(videoID))
                return null;

            // 要回傳的物件
            ClassFTPJob obj = new ClassFTPJob();
            obj.VideoID = videoID;
            obj.Note = string.Empty;

            // 取得 NAS Storage 的相關資訊
            MAM_PTS_DLL.SysConfig objSysconfig = new MAM_PTS_DLL.SysConfig();
            string NasSend = objSysconfig.sysConfig_Read("/ServerConfig/NAS/MASTERCONTROL_NAS_SEND");

            // 檢查目錄是否存在
            string dirName = string.Concat(NasSend, videoID);
            if (!Directory.Exists(dirName))
            {
                obj.Ftp1Status = "找不到相關檔案";
                obj.Ftp2Status = "找不到相關檔案";
                return obj;
            }

            // 檢查 SYMBOL 檔以判斷狀態 - ftp1
            if (File.Exists(string.Concat(dirName, ".ftpok1")))
            {
                obj.Ftp1Status = "傳檔完成";
                ClassFTPJob.ReadSymbolFile(string.Concat(dirName, ".ftpok1"), out obj.Ftp1StartExecTime, out obj.Ftp1FinishExecTime, out obj.Ftp1FileSizeNAS, out obj.Ftp1FileSizeFTP, out obj.Ftp1ErrMsg);
            }
            else if (File.Exists(string.Concat(dirName, ".ftpexist1")))
            {
                obj.Ftp1Status = "檔案已存在";
                ClassFTPJob.ReadSymbolFile(string.Concat(dirName, ".ftpexist1"), out obj.Ftp1StartExecTime, out obj.Ftp1FinishExecTime, out obj.Ftp1FileSizeNAS, out obj.Ftp1FileSizeFTP, out obj.Ftp1ErrMsg);
            }
            else if (File.Exists(string.Concat(dirName, ".ftperr1")))
            {
                obj.Ftp1Status = "發生錯誤";
                ClassFTPJob.ReadSymbolFile(string.Concat(dirName, ".ftperr1"), out obj.Ftp1StartExecTime, out obj.Ftp1FinishExecTime, out obj.Ftp1FileSizeNAS, out obj.Ftp1FileSizeFTP, out obj.Ftp1ErrMsg);
            }
            else if (File.Exists(string.Concat(dirName, ".ftpproc1")))
                obj.Ftp1Status = "處理中";
            else
                obj.Ftp1Status = "尚未處理";

            // 檢查 SYMBOL 檔以判斷狀態 - ftp2
            if (File.Exists(string.Concat(dirName, ".ftpok2")))
            {
                obj.Ftp2Status = "傳檔完成";
                ClassFTPJob.ReadSymbolFile(string.Concat(dirName, ".ftpok2"), out obj.Ftp2StartExecTime, out obj.Ftp2FinishExecTime, out obj.Ftp2FileSizeNAS, out obj.Ftp2FileSizeFTP, out obj.Ftp2ErrMsg);
            }
            else if (File.Exists(string.Concat(dirName, ".ftpexist2")))
            {
                obj.Ftp2Status = "檔案已存在";
                ClassFTPJob.ReadSymbolFile(string.Concat(dirName, ".ftpexist2"), out obj.Ftp2StartExecTime, out obj.Ftp2FinishExecTime, out obj.Ftp2FileSizeNAS, out obj.Ftp2FileSizeFTP, out obj.Ftp2ErrMsg);
            }
            else if (File.Exists(string.Concat(dirName, ".ftperr2")))
            {
                obj.Ftp2Status = "發生錯誤";
                ClassFTPJob.ReadSymbolFile(string.Concat(dirName, ".ftperr2"), out obj.Ftp2StartExecTime, out obj.Ftp2FinishExecTime, out obj.Ftp2FileSizeNAS, out obj.Ftp2FileSizeFTP, out obj.Ftp2ErrMsg);
            }
            else if (File.Exists(string.Concat(dirName, ".ftpproc2")))
                obj.Ftp2Status = "處理中";
            else
                obj.Ftp2Status = "尚未處理";

            // 查詢詳細資料
            string spName = (obj.VideoID.Length == 8) ? "SP_Q_MSSPROCESS_JOBNAME2" : "SP_Q_MSSPROCESS_JOBNAME";
            List<Dictionary<string, string>> sqlData;
            Dictionary<string, string> sqlParam = new Dictionary<string,string>();
            sqlParam.Add("FSVIDEO_ID", obj.VideoID);
            if (MAM_PTS_DLL.DbAccess.Do_Query(spName, sqlParam, out sqlData) && sqlData.Count > 0)
                obj.Note = string.Format("【{0}】{1}，(集數：{2}，段落：{3})", sqlData[0]["FSFILE_NO"], sqlData[0]["NAME"], sqlData[0]["FNEPISODE"], sqlData[0]["FNSEG_ID"]);

            return obj;
        }

        /// <summary>
        /// 重置單一 FTP 派送的任務
        /// </summary>
        /// <param name="videoID"></param>
        /// <param name="ftpNo"></param>
        /// <returns></returns>
        [WebMethod]
        public bool ResetMasterControlFtpJob(string videoID, string ftpNo)
        {
            MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_GetMssStatus/ResetMasterControlFtpJob", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "START");

            // 檢查 VideoID 的格式是否符合，如果不符合，就直接 Return 掉
            if (!MAM_PTS_DLL.CheckFormat.CheckFormat_VideoID(videoID))
                return false;
            // 檢查 FTPNo 是否正確，只能是 1 或 2
            if (ftpNo != "1" && ftpNo != "2")
                return false;

            // 取得 NAS Storage 的相關資訊
            MAM_PTS_DLL.SysConfig objSysconfig = new MAM_PTS_DLL.SysConfig();
            string NasSend = objSysconfig.sysConfig_Read("/ServerConfig/NAS/MASTERCONTROL_NAS_SEND");
            string fileName = string.Empty;
            try
            {
                fileName = string.Concat(NasSend, videoID, ".ftpok", ftpNo);
                if (File.Exists(fileName))
                    File.Delete(fileName);
                fileName = string.Concat(NasSend, videoID, ".ftpexist", ftpNo);
                if (File.Exists(fileName))
                    File.Delete(fileName);
                fileName = string.Concat(NasSend, videoID, ".ftpproc", ftpNo);
                if (File.Exists(fileName))
                    File.Delete(fileName);
                fileName = string.Concat(NasSend, videoID, ".ftperr", ftpNo);
                if (File.Exists(fileName))
                    File.Delete(fileName);
            }
            catch (Exception ex)
            {
                string errMsg = string.Concat("Got error when delete file: ", fileName, ", msg = ", ex.ToString());
                MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_GetMssStatus/ResetMasterControlFtpJob", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return false;
            }
            return true;
        }

        /// <summary>
        /// 重置所有 \\10.13.200.1\MasterCtrl\SEND 中，記錄為錯誤的派送工作項目
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public bool ResetMasterControlErrorFtpJob()
        {
            bool isComplete = true;

            MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_GetMssStatus/ResetMasterControlErrorFtpJob", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "START");

            // 取得 NAS Storage 的相關資訊
            MAM_PTS_DLL.SysConfig objSysconfig = new MAM_PTS_DLL.SysConfig();
            string NasSend = objSysconfig.sysConfig_Read("/ServerConfig/NAS/MASTERCONTROL_NAS_SEND");
            string fileName = string.Empty;

            // 取得標註為錯誤的清單
            string errMsg;
            List<string> deleteFileList = MAM_PTS_DLL.FileOperation.GetDirectoryFileList(NasSend, "*.ftperr*", false, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_GetMssStatus/ResetMasterControlErrorFtpJob", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Format("查詢檔案目錄清單時發生錯誤：目錄={0}, 錯誤訊息={1}", NasSend, errMsg));
                return false;
            }

            // 將清單中的檔案做刪除
            foreach (string deleteFile in deleteFileList)
            {
                // 有錯的話僅記錄
                if (!MAM_PTS_DLL.FileOperation.DeleteFile(deleteFile, out errMsg))
                {
                    MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_GetMssStatus/ResetMasterControlErrorFtpJob", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Format("刪檔錯誤：檔案路徑={0}, 錯誤訊息={1}", deleteFile, errMsg));
                    isComplete = false;
                }
            }

            // 如果刪檔的過程中，有任何一個檔沒有刪除，判為錯誤，但也不會將那些已刪除的檔案還原回來，如果有需要的話再加強邏輯吧
            return isComplete;
        }

        /// <summary>
        /// 取得 \\10.13.200.1\MasterCtrl\MSG\LOUTH\InputXML 中的檔案清單，並將狀態回傳於前端
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public List<ClassLouthJob> GetMasterControlLouthStatus()
        {
            MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_GetMssStatus/GetMasterControlLouthStatus", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "START");

            // 取得 NAS Storage 的相關資訊
            MAM_PTS_DLL.SysConfig objSysconfig = new MAM_PTS_DLL.SysConfig();
            string NasLouthXML = objSysconfig.sysConfig_Read("/ServerConfig/NAS/MASTERCONTROL_NAS_LOUTH_XML");
            List<string> fileList = new List<string>();
            try
            {
                fileList = new List<string>(Directory.GetFiles(NasLouthXML));
            }
            catch (Exception ex)
            {
                string errMsg = string.Concat("Got error when GetFiles, NasSend = ", NasLouthXML, "msg = ", ex.ToString());
                MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_GetMssStatus/GetMasterControlLouthStatus", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "START");
                return new List<ClassLouthJob>();
            }

            // 解析取得的資訊
            List<ClassLouthJob> resultList = new List<ClassLouthJob>();
            foreach (string fileName in fileList)
            {
                if (Path.GetExtension(fileName) == ".xml")
                {
                    // 要有 .pgmok 才可以繼續檢查
                    if (File.Exists(string.Concat(fileName, ".pgmok")))
                    {
                        ClassLouthJob obj = new ClassLouthJob();
                        // 設定物件屬性
                        obj.XmlFilePath = fileName;
                        obj.XmlFileName = Path.GetFileName(fileName);
                        // 設定物件狀態
                        if (File.Exists(string.Concat(fileName, ".dbok")))
                            obj.XmlFileStatus = "完成";
                        else if (File.Exists(string.Concat(fileName, ".dberr")))
                            obj.XmlFileStatus = "失敗";
                        else
                            obj.XmlFileStatus = "處理中";
                        resultList.Add(obj);
                    }
                }
            }

            return resultList;
        }

        /// <summary>
        /// 查詢 \\10.13.200.1\MasterCtrl\MSG\LOUTH\InputXML 中的註記檔內容，裡面有時候會記錄錯誤訊息或成功時間
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        [WebMethod]
        public ClassLouthJob GetMasterControlLouthDetailStatus(string fileName)
        {
            MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_GetMssStatus/GetMasterControlLouthDetailStatus", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "START, fileName = " + fileName);

            // 取得 NAS Storage 的相關資訊
            MAM_PTS_DLL.SysConfig objSysconfig = new MAM_PTS_DLL.SysConfig();
            string NasLouthXML = objSysconfig.sysConfig_Read("/ServerConfig/NAS/MASTERCONTROL_NAS_LOUTH_XML");

            string filePath = string.Concat(NasLouthXML, fileName);

            // 檢查是否可以讀取到傳入的 filePath (由排表系統產出的XML檔)
            if (!File.Exists(filePath))
                return null;

            // 
            ClassLouthJob obj = new ClassLouthJob();
            obj.XmlFileName = fileName;
            obj.XmlFilePath = filePath;
            if (File.Exists(string.Concat(filePath, ".dbok")))
            {
                obj.XmlFileStatus = "完成";
            }
            else if (File.Exists(string.Concat(filePath, ".dberr")))
            {
                obj.XmlFileStatus = "失敗";
                // -----
                // 讀取錯誤檔的內容，並解析，然後寫入自己的屬性
                string errSymbolFile = string.Concat(filePath, ".dberr");
                try
                {
                    StreamReader sr = File.OpenText(errSymbolFile);
                    string textline = null;
                    while ((textline = sr.ReadLine()) != null)
                    {
                        if (!string.IsNullOrEmpty(textline))
                            obj.ParseLineErrorText(textline);
                        else
                            break;
                    }
                    sr.Close();
                }
                catch (Exception ex)
                {
                    string errMsg = string.Concat("Got error when read text file: ", errSymbolFile, ", msg = ", ex.ToString());
                    MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_GetMssStatus/GetMasterControlLouthDetailStatus", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                    return null;
                }
                // -----
            }
            else
            {
                obj.XmlFileStatus = "處理中";
            }

            return obj;
        }

        /// <summary>
        /// 刪除 \\10.13.200.1\MasterCtrl\MSG\LOUTH\InputXML 中的 XML 檔
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        [WebMethod]
        public bool DeleteMasterControlLouthJob(string fileName)
        {
            MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_GetMssStatus/DeleteMasterControlLouthJob", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "START");

            // 取得 NAS Storage 的相關資訊
            MAM_PTS_DLL.SysConfig objSysconfig = new MAM_PTS_DLL.SysConfig();
            string NasLouthXML = objSysconfig.sysConfig_Read("/ServerConfig/NAS/MASTERCONTROL_NAS_LOUTH_XML");

            string deleteFileName = string.Empty;
            try
            {
                // 刪除主體檔
                deleteFileName = string.Concat(NasLouthXML, fileName);
                if (File.Exists(deleteFileName))
                    File.Delete(deleteFileName);
                // 刪除 SYMBOL 檔
                deleteFileName = string.Concat(NasLouthXML, fileName, ".pgmok");
                if (File.Exists(deleteFileName))
                    File.Delete(deleteFileName);
                deleteFileName = string.Concat(NasLouthXML, fileName, ".dbok");
                if (File.Exists(deleteFileName))
                    File.Delete(deleteFileName);
                deleteFileName = string.Concat(NasLouthXML, fileName, ".dberr");
                if (File.Exists(deleteFileName))
                    File.Delete(deleteFileName);
            }
            catch (Exception ex)
            {
                string errMsg = string.Concat("Got error when delete file: ", deleteFileName, ", msg = ", ex.ToString());
                MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_GetMssStatus/DeleteMasterControlLouthJob", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return false;
            }
 
            return true;
        }

        /// <summary>
        /// 刪除 \\10.13.200.1\MasterCtrl\MSG\LOUTH\InputXML 中的註記檔，讓程式可以重更新 Louth 資料庫的內容
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        [WebMethod]
        public bool ResetMasterControlLouthJob(string fileName)
        {
            MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_GetMssStatus/ResetMasterControlLouthJob", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, "START");

            // 取得 NAS Storage 的相關資訊
            MAM_PTS_DLL.SysConfig objSysconfig = new MAM_PTS_DLL.SysConfig();
            string NasLouthXML = objSysconfig.sysConfig_Read("/ServerConfig/NAS/MASTERCONTROL_NAS_LOUTH_XML");

            string deleteFileName = string.Empty;
            try
            {
                // 刪除 SYMBOL 檔
                deleteFileName = string.Concat(NasLouthXML, fileName, ".dbok");
                if (File.Exists(deleteFileName))
                    File.Delete(deleteFileName);
                deleteFileName = string.Concat(NasLouthXML, fileName, ".dberr");
                if (File.Exists(deleteFileName))
                    File.Delete(deleteFileName);
            }
            catch (Exception ex)
            {
                string errMsg = string.Concat("Got error when delete file: ", deleteFileName, ", msg = ", ex.ToString());
                MAM_PTS_DLL.Log.AppendTrackingLog("WSPGM_GetMssStatus/ResetMasterControlLouthJob", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return false;
            }

            return true;
        }
    }
}
