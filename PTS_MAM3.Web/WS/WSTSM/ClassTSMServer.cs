﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.WS.WSTSM
{
    public class ClassTSMServer
    {
        public string SERVER_IP;
        public string SERVER_NAME;
        public bool SERVER_FUNC_RSH;
        public bool SERVER_FUNC_SAMBA;
        public bool SERVER_FUNC_DB2;
        public string SERVER_PARA_DB2_CONNSTR;
        public string SERVER_PARAD_B2_TBNAME;
        public string SERVER_PARA_RSH_ARGU;
        public string SERVER_PARA_ADMC_ARGU;

        public bool AddServerAttribute(string attr, string value)
        {
            switch (attr)
            {
                case "IP":
                    SERVER_IP = value;
                    break;
                case "NAME":
                    SERVER_NAME = value;
                    break;
                case "RSH":
                    if (value == "Y")
                        SERVER_FUNC_RSH = true;
                    else
                        SERVER_FUNC_RSH = false;
                    break;
                case "SAMBA":
                    if (value == "Y")
                        SERVER_FUNC_SAMBA = true;
                    else
                        SERVER_FUNC_SAMBA = false;
                    break;
                case "DB2":
                    if (value == "Y")
                        SERVER_FUNC_DB2 = true;
                    else
                        SERVER_FUNC_DB2 = false;
                    break;
                case "DB2_CONNSTR":
                    SERVER_PARA_DB2_CONNSTR = value;
                    break;
                case "DB2_TBNAME":
                    SERVER_PARAD_B2_TBNAME = value;
                    break;
                case "RSH_ARGU":
                    SERVER_PARA_RSH_ARGU = value;
                    break;
                case "ADMC_ARGU":
                    SERVER_PARA_ADMC_ARGU = value;
                    break;
                default:
                    return false;
            }
            return true;
        }
    }
}