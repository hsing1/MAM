﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Text;

namespace PTS_MAM3.Web.WS.WSTSM
{
    public partial class ServiceTSM
    {
        /*
         * 註解說明：
         *      TsmCommand 是指直接用 command 做查詢
         *      TsmQuery 是指透過 dsmadmc 下指令做查詢
         *      TsmScript 是指用 dsmadmc 呼叫 Script 做查詢
         *      
         *      每一個指令，都有 Combine (組合指令) 及 Parse (解析該執行該指令後的結果)
         */

        #region TsmCommand_dsmls
        string fnCombine_TsmCmd_dsmls(string tsmpath)
        {
            // 目前已確認在FTV內的5.x版及PTS內的6.x版都是可以刪前八行的內容
            // 第九行是結果，或是錯誤內容
            // (True) 105406468    105406468       102944   r      0106htv4.MPG
            // (True) 182629376     10485760        10240   m (p)  2.msi
            // (False) ANS9126E dsmls: cannot determine whether space management is active or inactive for /HVideo/HVideo/PTSTEST/3.msi due to error: No such file or directory.

            // 範例：dsmls /ptstv/0001.mpeg | sed '1,8d'
            return string.Format(@"dsmls {0} | sed '1,8d'", tsmpath);
        }

        // 回傳的List的順序：ActualSize -> ResidentSize -> FileStatus -> FileName
        bool fnParse_TsmCmd_dsmls(string returnString, out List<string> parseResult)
        {
            parseResult = new List<string>();

            string pattern_dsmls = @"(?<sizeActual>\d+)\s+(?<sizeResident>\d+)\s+(?<sizeBlock>\d+)\s+(?<fileStatus>[rmp]{1})\s+(?<filePath>.+$)";

            // 檢查是否有相符的格式
            Match dataMatch = Regex.Match(returnString, pattern_dsmls);

            if (!dataMatch.Success)
                return false;

            // filePath 的拆解(先不做，反正還用不到)
            // In PTS：
            // (True) 105406468    105406468       102944   r      0106htv4.MPG
            // In FTV：
            // (True) 182629376     10485760        10240   m (p)  2.msi
            string[] filePath = dataMatch.Groups["filePath"].Value.Split(' ');

            parseResult.Add(dataMatch.Groups["sizeActual"].Value);
            parseResult.Add(dataMatch.Groups["sizeResident"].Value);
            parseResult.Add(dataMatch.Groups["fileStatus"].Value);
            parseResult.Add(filePath[filePath.Length - 1]);

            return true;
        }
        #endregion

        #region TsmQuery_查詢檔案在哪捲磁帶
        string fnCombineTsmCommand_queryTapeNumber(string tsmPath)
        {
            string stgpool_name, filespace_name, file_name;

            //
            fnParseTsmPath(tsmPath, out stgpool_name, out filespace_name, out file_name);

            // 最安全的寫法：只會找出在HSM中(不含BAK)的唯一路徑的唯一檔案
            // 範例：dsmadmc -id=admin -pa=admin -dataonly=yes "select volume_name from tsmdb1.contents where type='SpMg' and filespace_name='/hakkatv' and file_name='/123.MPG' and volume_name in (select volume_name from volumes where stgpool_name='HAKKATVHSM')"
            return string.Format(@"{0} ""select volume_name from tsmdb1.contents where type='SpMg' and filespace_name='{1}' and file_name='{2}' and volume_name in (select volume_name from volumes where stgpool_name='{3}')""",
                HsmAdmcArguments, filespace_name, file_name, stgpool_name);
        }

        // 回傳值為在TSM_3350中的磁帶編號
        // 注意：有可能會有磁帶跨捲(多捲)的問題，所以要用List；如果沒有符點的內容就回傳空的List
        List<string> fnParseTsmCommand_queryTapeNumber(List<string> datas)
        {
            // 成功的範例：
            //  PTS521L4
            // 失敗的範例
            //  ANR2034E SELECT: No match found using this criteria.
            //  ANS8001I Return code 11.
            List<string> result = new List<string>();
            foreach (string line in datas)
                if (line.Contains("No match found using this criteria"))
                    return null;
                else if (!string.IsNullOrEmpty(line.Trim()))
                    result.Add(line.Trim());
            return result;
        }
        #endregion

        #region TsmScript_磁帶上架_TSM3310
        string fnCombineTsm3310Checkin()
        {
            // 範例：dsmadmc -id=ptsmam -pa=ptsmam -dataonly=yes "RUN PTS_CHECKIN_TS3310"
            return string.Format(@"{0} ""RUN PTS_CHECKIN_TS3310""", HsmAdmcArguments);
        }

        bool fnParseTsm3310Checkin(List<string> datas)
        {
            // 正常的情況：
            // ANR1462I RUN: Command script PTS_CHECKIN_TS3310 completed successfully.
            return datas != null && datas.Count > 0 && datas[datas.Count - 1].Contains("successfully");
        }
        #endregion

        #region TsmScript_磁帶下架_TSM3310
        string fnCombineTsm3310CheckoutTapes(string tapeNo)
        {
            // 範例：dsmadmc -id=ptsmam -pa=ptsmam -dataonly=yes "RUN PTS_DRM_MOVEOUT 磁帶名稱"
            return string.Format(@"{0} ""RUN PTS_DRM_MOVEOUT {1}""", HsmAdmcArguments, tapeNo);
        }

        bool fnParseTsm3310CheckoutTapes(List<string> datas)
        {
            // 正常的情況：
            // ANR0609I MOVE DRMEDIA started as process 1131.
            // ANR1462I RUN: Command script PTS_DRM_MOVEOUT completed successfully.
            // 下架不存在的磁帶：
            // [[還沒試過]]
            foreach (var line in datas)
                if (line.Contains("PTS_DRM_MOVEOUT") && line.Contains("completed successfully"))
                    return true;
            // 特殊的例外狀況，先記錄
            MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/CHECKOUT_TS3310", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Concat("TS3310下架時未捕捉到正確資訊：", MAM_PTS_DLL.ConvertFormat.ConvertListStringToString(datas)));
            return false;
        }
        #endregion

        #region TsmScript_磁帶上架_TSM3500
        string fnCombineTsm3500CheckinTapes()
        {
            // 範例：dsmadmc -id=admin -pa=admin -dataonly=yes "RUN PTS_CHECKIN_TS3500"
            return string.Format(@"{0} ""RUN PTS_CHECKIN_TS3500""", HsmAdmcArguments);
        }

        bool fnParseTsm3500CheckinTapes(List<string> datas)
        {
            // 理論上只會有一行回傳，但不曉得在錯誤時會是啥情況，所以我還是用 List<string> 來接

            // 成功範例：
            // ANR1462I RUN: Command script PTS_CHECKIN_TS3500 completed successfully.
            if (datas[0].Contains("Command script PTS_CHECKIN_TS3500 completed successfully"))
                return true;
            else
                return false;
        }
        #endregion

        #region TsmScript_磁帶下架_TSM3500
        string fnCombineTsm3500CheckoutTapes(string tapeNo)
        {
            // 範例：dsmadmc -id=ptsmam -pa=ptsmam -dataonly=yes "RUN PTS_CHECKOUT_TS3500 磁帶名稱"
            return string.Format(@"{0} ""RUN PTS_CHECKOUT_TS3500 {1}""", HsmAdmcArguments, tapeNo);
        }

        bool fnParseTsm3500CheckoutTapes(List<string> datas)
        {
            // 正常的情況：
            // ANR1462I RUN: Command script PTS_CHECKOUT_TS3500 completed successfully.
            // 下架不存在的磁帶：
            // ANR8433E CHECKOUT LIBVOLUME: Volume PTS520L4 is not present in library TS3500.
            // ANR1463E RUN: Command script PTS_CHECKOUT_TS3500 completed in error.
            // ANS8001I Return code 4.
            if (datas[0].Contains("Command script PTS_CHECKOUT_TS3500 completed successfully"))
                return true;
            if (datas[0].Contains("is not present in library TS3500"))
                return false;
            // 特殊的例外狀況，先記錄
            MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/CHECKOUT_TS3500", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Concat("不確定的回傳：", MAM_PTS_DLL.ConvertFormat.ConvertListStringToString(datas)));
            return false;
        }
        #endregion

        #region TsmScript_查詢磁帶櫃中的磁帶列表_TSM3310
        string fnCombine_TsmAdmc_GetTapeList_TSM3310()
        {
            // 範例：dsmadmc -id=admin -pa=admin -dataonly=yes -comma "RUN PTS_Q_LIBV_TS3310"
            return string.Format(@"{0} -comma ""RUN PTS_Q_LIBV_TS3310""", HsmAdmcArguments);
        }

        bool fnParse_TsmAdmc_GetTapeList_TSM3310(List<String> datas)
        {
            // 回傳範例：
            //E00705L4,DATA,FULL,1312.84,2014-09-08-13.10,2014-09-11-11.15,FILEBAKCP,READWRITE
            //E00709L4,DATA,FULL,612.66,2014-09-10-16.43,2014-08-08-11.27,FILEBAKCP,READWRITE
            //E00732L4,DATA,FILLING,307.81,2014-09-11-11.15,2014-09-12-11.33,FILEBAKCP,READWRITE
            //B00160L4,Scratch,,,,,,
            //B00170L4,Scratch,,,,,,
            //ANR1462I RUN: Command script PTS_Q_LIBV_TS3310 completed successfully.

            if (datas == null || datas.Count == 0)
                return false;

            // 先檢查最後一行的內容是不是 success
            if (!datas[datas.Count - 1].Contains("Command script PTS_Q_LIBV_TS3310 completed successfully"))
                return false;

            // 清除原有的TapeList
            ClassTapeInfo.ClearTS3310Cache();

            // 將每一個 Tape 都加進去
            for (int i = 0; i < datas.Count; i++)
                ClassTapeInfo.AddTS3310Cache(datas[i]);
                //fnAddTapeListMemberInTsm3310(datas[i]);

            // 排序
            ClassTapeInfo.SortTS3310Cache(true);

            return true;
        }
        #endregion

        #region TsmScript_查詢磁帶櫃中的磁帶列表_TSM3500
        string fnCombine_TsmAdmc_GetTapeList_TSM3500()
        {
            // 範例：dsmadmc -id=admin -pa=admin -dataonly=yes -comma "RUN PTS_Q_LIBV_TS3500"
            return string.Format(@"{0} -comma ""RUN PTS_Q_LIBV_TS3500""", HsmAdmcArguments);
        }

        bool fnParse_TsmAdmc_GetTapeList_TSM3500(List<String> datas)
        {
            // 回傳範例：
            //PTS527L4,DATA,FILLING,40.62,2011-01-26-19.12,2011-01-26-20.06,FILEBAK,READWRITE
            //PTS532L4,DATA,FILLING,3.12,2011-02-10-17.49,2011-02-10-18.14,FILEBAK,READWRITE
            //PTS534L4,DATA,FILLING,0.00,2011-02-17-19.46,2011-02-17-19.46,MSSVIDEOHSM,UNAVAILABLE
            //PTS530L4,DATA,FILLING,0.00,2011-02-18-13.50,2011-02-14-10.12,PTSTVHSM,UNAVAILABLE
            //PTS520L4,DATA,FILLING,0.00,2011-02-18-19.15,2011-02-19-19.07,PTSTVHSM,READWRITE
            //PTS522L4,Scratch,,,,,,
            //PTS523L4,Scratch,,,,,,
            //PTS531L4,Scratch,,,,,,
            //ANR1462I RUN: Command script PTS_Q_LIBV_TS3500 completed successfully.

            if (datas == null || datas.Count == 0)
                return false;

            // 先檢查最後一行的內容是不是 success
            if (!datas[datas.Count - 1].Contains("Command script PTS_Q_LIBV_TS3500 completed successfully"))
                return false;

            // 清除原有的TapeList
            ClassTapeInfo.ClearTS3500Cache();
            //fnClearTapeListInTsm3500();

            // 將每一個 Tape 都加進去
            for (int i = 0; i < datas.Count; i++)
                //ClassTapeInfo.AddCache(new ClassTapeInfo(-1, datas[i]));
                //fnAddTapeListMemberInTsm3500(datas[i]);
                ClassTapeInfo.AddTS3500Cache(datas[i]);

            // 排序
            ClassTapeInfo.SortTS3500Cache(true);

            return true;
        }
        #endregion

        #region TsmScript_查詢目前TSM的執行緒
        private string fnCombineQueryProcess()
        {
            // 範例： dsmadmc -id=admin -pa=admin -dataonly=yes -comma "RUN PTS_Q_PRO"
            return string.Format(@"{0} -comma ""RUN PTS_Q_PRO""", HsmAdmcArguments);
        }

        private bool fnParseQueryProcess(List<string> datas)
        {
            // ANR0944E QUERY PROCESS: No active processes found.
            // ANR1462I RUN: Command script PTS_Q_PRO completed successfully.

            foreach (string line in datas)
                if (line.Contains("No active processes found"))
                    return true;
            return false;
        }
        #endregion
    }
}