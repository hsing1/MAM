﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.IO;
using System.Text;

namespace PTS_MAM3.Web.WS.WSTSM
{
    /// <summary>
    /// WebService1 的摘要描述
    /// </summary>
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public partial class ServiceTSM : System.Web.Services.WebService
    {
        const string omeon_ext_name = ".mxf";

        public struct KeyframeInfo
        {
            public string KeyframeHttpPath;
            public string KeyframeDescription;
        }



        /// <summary>
        /// 由 Samba Path 的 ServerName 檢查查詢的路徑是不是屬於 HSM 上的檔案 (大小寫不會影響判斷)
        /// </summary>
        /// <param name="smbFilePath">要查詢的Samba路徑</param>
        /// <returns></returns>
        public bool isFileInTSM(string smbFilePath)
        {
            foreach (string serverName in TSMSambaServerNames)
                if (smbFilePath.ToLower().StartsWith(serverName.ToLower()))
                    return true;
            return false;
        }

        /// <summary>
        /// 供送帶轉檔、入庫 "製造" 完整路徑
        /// </summary>
        /// <param name="fileID">FileID</param>
        /// <param name="episodeNo">子集數(請輸入數字，程式會自動補零)</param>
        /// <param name="type">類型</param>
        /// <param name="channelID">頻道代碼(ex: 01代表PTS)</param>
        /// <param name="extName">副檔名(不帶.，ex: jpg)</param>
        /// <returns>檔案的Samba路徑</returns>
        [WebMethod]
        public string GenerateSambaPathByFileIdInfo(string fileID, FileID_MediaType type, string channelID, string extName)
        {
            string strReturnSambaPath = string.Empty;

            // 檢查輸入的參數
            if (!MAM_PTS_DLL.CheckFormat.CheckFormat_FileID(fileID))
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("/WSTSM/GenerateSambaPathByFileIdInfo", MAM_PTS_DLL.Log.TRACKING_LEVEL.WARNING, string.Concat("非法的 fileID 格式：", fileID));
                return strReturnSambaPath;
            }

            // 檢查設定值是否完整
            if (string.IsNullOrEmpty(TSMSambaSharePrefix))
            {
                // 強制重新取得 TSM 的設定
                ForceUpdateTSMServerInfo();

                // 再次檢查
                if (string.IsNullOrEmpty(TSMSambaSharePrefix))
                {
                    MAM_PTS_DLL.Log.AppendTrackingLog("/WSTSM/GenerateSambaPathByFileIdInfo", MAM_PTS_DLL.Log.TRACKING_LEVEL.FATALERROR, "取得 TSMSambaSharePrefix 設定值時發生錯誤");
                    return strReturnSambaPath;
                }
            }

            string strYYYY = fileID.Substring(1, 4);

            // 這邊命名的不太好，對於節目是 progID，對於宣傳帶是 promoID
            string strProgID = string.Empty;
            // 用 FileID 字首判斷是節目或是宣傳帶，截取不同的長度
            if (fileID.StartsWith("G"))
                strProgID = fileID.Substring(1, 7);
            else if (fileID.StartsWith("P") || fileID.StartsWith("D"))
                strProgID = fileID.Substring(1, 11);

            // 用 channelID 對應到 PoolName
            string channelShareName = fnGetPoolName(channelID);
            if (string.IsNullOrEmpty(channelShareName))
                return strReturnSambaPath;

            // 這部份的定義，要參考文件 - TSM架講討論
            switch (type)
            {
                case FileID_MediaType.HiVideo:
                    strReturnSambaPath = string.Format(@"\\{0}\{1}\HVideo\{2}\{3}\{4}.{5}",
                        TSMSambaSharePrefix, channelShareName, strYYYY, strProgID, fileID, extName);
                    break;
                case FileID_MediaType.LowVideo:
                    strReturnSambaPath = string.Format(@"\\{0}\Media\Lv\{1}\{2}\{3}\{4}.{5}",
                        TSMSambaSharePrefix, channelShareName, strYYYY, strProgID, fileID, extName);
                    break;
                case FileID_MediaType.Audio:
                    strReturnSambaPath = string.Format(@"\\{0}\Media\Audio\{1}\{2}\{3}\{4}.{5}",
                        TSMSambaSharePrefix, channelShareName, strYYYY, strProgID, fileID, extName);
                    break;
                case FileID_MediaType.Doc:
                    strReturnSambaPath = string.Format(@"\\{0}\Media\Doc\{1}\{2}\{3}\{4}.{5}",
                        TSMSambaSharePrefix, channelShareName, strYYYY, strProgID, fileID, extName);
                    break;
                case FileID_MediaType.Photo:
                    strReturnSambaPath = string.Format(@"\\{0}\Media\Photo\{1}\{2}\{3}\{4}.{5}",
                        TSMSambaSharePrefix, channelShareName, strYYYY, strProgID, fileID, extName);
                    break;
                default:
                    break;
            }

            // 直接嚐試建立目錄，但如果失敗了也不去影響執行的結果
            string createdDirPath = Path.GetDirectoryName(strReturnSambaPath);
            try
            {
                if (!Directory.Exists(createdDirPath))
                    Directory.CreateDirectory(createdDirPath);
            }
            catch (Exception ex)
            {
                // 記錄錯誤，但依然回傳
                MAM_PTS_DLL.Log.AppendTrackingLog("/WSTSM/GenerateSambaPathByFileIdInfo", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Format("建立目的路徑發生錯誤：路徑 = {0}, 錯誤 = {1}", createdDirPath, ex.Message));
            }

            return strReturnSambaPath;
        }

        /// <summary>
        /// 用FILEID+TYPE查詢TSM上的SAMBA檔案路徑
        /// 最後更新：2011-02-15，因為在資料庫內多開了低解檔路徑，可以簡化程式 
        /// </summary>
        /// <param name="fileID">FileID</param>
        /// <param name="type">檔案型別(FileID_MediaType)</param>
        /// <returns>該檔案的UNC路徑</returns>
        [WebMethod]
        public string GetSambaFilePathByFileID(string fileID, FileID_MediaType type)
        {
            string spName;
            // 依不同類別組出不同的SQL Query String
            switch (type)
            {
                case FileID_MediaType.HiVideo:
                    spName = "SP_Q_TBLOGVIDEO_HI_FILEPATH";
                    break;
                case FileID_MediaType.LowVideo:
                    spName = "SP_Q_TBLOGVIDEO_LO_FILEPATH";
                    break;
                case FileID_MediaType.Audio:
                    spName = "SP_Q_TBLOGAUDIO_FILEPATH";
                    break;
                case FileID_MediaType.Doc:
                    spName = "SP_Q_TBLOGDOC_FILEPATH";
                    break;
                case FileID_MediaType.Photo:
                    spName = "SP_Q_TBLOGPHOTO_FILEPATH";
                    break;
                default:
                    MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM_GetFileInfo/GetSambaFilePathByFileID", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, string.Concat("Unexpected Query Type, input value = ", type));
                    return string.Empty;
            }

            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSFILE_NO", fileID);
            MAM_PTS_DLL.DbAccess.Do_Query(spName, sqlParameters, out resultData);

            if (resultData.Count > 0)
            {
                // 因為在 TBLOG_VIDEO 裡面的欄位 (高解、低解) 命名有差別，要拉出來做
                if (type == FileID_MediaType.HiVideo)
                    return resultData[0]["FSFILE_PATH_H"];
                else if (type == FileID_MediaType.LowVideo)
                    return resultData[0]["FSFILE_PATH_L"];
                else
                    return resultData[0]["FSFILE_PATH"];
            }
            else
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM_GetFileInfo/GetSambaFilePathByFileID", MAM_PTS_DLL.Log.TRACKING_LEVEL.INFO, string.Concat("Can't find FileNo information: fileID = ", fileID));
                return string.Empty;
            }
        }

        /// <summary>
        /// 用VIDEOID查詢TSM上的SAMBA目錄路徑(Pinnacle格式用)
        /// 回傳範例：\\10.13.210.12\mssvideo\00000001
        /// </summary>
        /// <param name="videoID"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetSambaDirPathByVideoID(string videoID)
        {
            // 前置檢查，避免是空值
            if (string.IsNullOrEmpty(TSMSambaSharePrefix))
                ForceUpdateTSMServerInfo();

            // 
            if (!string.IsNullOrEmpty(TSMSambaSharePrefix) && MAM_PTS_DLL.CheckFormat.CheckFormat_VideoID(videoID))
                return string.Concat(@"\\", TSMSambaSharePrefix, @"\mssvideo\", videoID);
            else
                return string.Empty;
        }

        /// <summary>
        /// 用VIDEOID查詢TSM上的SAMBA目錄路徑(Omneon格式用)
        /// 回傳範例：\\10.13.210.12\omneon\00000
        /// </summary>
        /// <param name="videoID"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetSambaDirPathByVideoID_O(string videoID)
        {
            // 前置檢查，避免是空值
            if (string.IsNullOrEmpty(TSMSambaSharePrefix))
                ForceUpdateTSMServerInfo();

            // 因為希望讓 Omneon 格式的主控檔案可以稍微做個分層，所以把前五碼的值做為一個資料夾
            // 這樣每一個 Omneon 的子資料夾，最多只會含 1000 個 VideoID 的檔案 (在沒有多段的情況下，如果通通都是段落，我想最多是五千個吧)
            if (!string.IsNullOrEmpty(TSMSambaSharePrefix) && MAM_PTS_DLL.CheckFormat.CheckFormat_VideoID(videoID))
                return string.Concat(@"\\", TSMSambaSharePrefix, @"\omneon\", videoID.Substring(0, 5));
            else
                return string.Empty;
        }

        /// <summary>
        /// 用VIDEOID查詢TSM上的SAMBA檔案路徑(Omneon格式用)
        /// 回傳範例：\\10.13.210.12\omneon\00000\00000123.mxf
        /// </summary>
        /// <param name="videoID"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetSambaFilePathByVideoID_O(string videoID)
        {
            return string.Concat(GetSambaDirPathByVideoID_O(videoID), "\\", videoID, omeon_ext_name);
        }

        /// <summary>
        /// Convert from UNC Path to Linux Path (File in HSM is working only)
        /// </summary>
        /// <param name="smbPath"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetLinuxPathBySambaPath(string smbPath)
        {
            //
            if (!MAM_PTS_DLL.CheckFormat.CheckFormat_UncPath(smbPath))
                return string.Empty;

            //
            foreach (string prefixPath in TSMSambaServerNames)
                if (smbPath.StartsWith(prefixPath))
                    return "/" + smbPath.Replace(prefixPath, "").Replace('\\', '/');

            // if file is not in HSM, we shouldn't give it an answer
            return string.Empty;
        }

        /// <summary>
        /// 取得 Media 檔案的分享路徑，支援類型：低解、音、圖、文
        /// </summary>
        /// <param name="fileID"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetHttpPathByFileID(string fileID, FileID_MediaType type)
        {
            //
            if (type == FileID_MediaType.HiVideo)
                return string.Empty;

            //
            string smbPath = GetSambaFilePathByFileID(fileID, type);
            if (string.IsNullOrEmpty(smbPath))
                return string.Empty;

            //
            return fnConvertMediaUncPathToHttpPath(smbPath);
        }

        /// <summary>
        /// 組合出 Keyframe 放置的目錄路徑，回傳的是 UNC 路徑
        /// 這部份應該只有「建立Keyframe的程式」才需要用，其他需要查的應直接查詢資料表
        /// -->因為新增了「修改節目所屬頻道」的功能，所以這邊的邏輯也要調整，會先查詢資料庫內的相關資料，沒有的話才用動態邏輯去組
        /// </summary>
        /// <param name="fileNo"></param>
        /// <param name="channelID"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetKeyframeDirPath(string fileNo, string channelID)
        {
            //
            if (!MAM_PTS_DLL.CheckFormat.CheckFormat_FileID(fileNo))
                return string.Empty;

            // 先查詢資料庫是否已有相關資料
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSFILE_NO", fileNo);
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_KEYFRAME_GETKEYFRAMEINFO", sqlParameters, out resultData))
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM_GetFileInfo/GetKeyframeDirPath", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Concat("查詢KEYFRAME資料時發生SQL錯誤，fileNo = ", fileNo));
                return string.Empty;
            }

            // 檢查資料庫的回傳結果，如果已有資料，就用資料的結果做回傳
            if (resultData.Count > 0)
            {
                string filePath = resultData[0]["FSFILE_PATH"];
                if (!string.IsNullOrEmpty(filePath))
                {
                    // 回傳資料裡面的東西
                    return string.Concat(Path.GetDirectoryName(filePath), "\\");
                }
                else
                {
                    // 碰到舊版資料，記錄，但不做限制
                    MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM_GetFileInfo/GetKeyframeDirPath", MAM_PTS_DLL.Log.TRACKING_LEVEL.WARNING, string.Concat("KEYFRAME資料表中有資料但無路徑，需用動態邏輯，fileNo = ", fileNo));
                }
            }

            // 以下是採動態邏輯
            string strYYYY = fileNo.Substring(1, 4);

            // 這邊命名的不太好，對於節目是 progID，對於宣傳帶是 promoID
            string strProgID = string.Empty;
            // 用 FileID 字首判斷是節目或是宣傳帶，截取不同的長度
            if (fileNo.StartsWith("G"))
                strProgID = fileNo.Substring(1, 7);
            else if (fileNo.StartsWith("P") || fileNo.StartsWith("D"))
                strProgID = fileNo.Substring(1, 11);

            // 利用 channelID 找出 poolName
            string channelShareName = fnGetPoolName(channelID);
            if (string.IsNullOrEmpty(channelShareName))
                return string.Empty;

            return string.Format(@"\\{0}\Media\Keyframe\{1}\{2}\{3}\{4}\", TSMSambaSharePrefix, channelShareName, strYYYY, strProgID, fileNo);
        }

        /// <summary>
        /// 查詢 Headframe 的 UNC 路徑
        /// </summary>
        /// <param name="fileID"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetHeadframePath(string fileID)
        {
            // 因為不想讓 Keyframe 位置的邏輯四處散落，所以在這邊採比較耗損資源的做法：
            // 將 Keyframe 的列表抓回來，然後再做修改，就變成了 Headframe 的位置
            List<string> keyframeList = new List<string>(GetKeyframeFiles(fileID));
            if (keyframeList.Count > 0)
            {
                // 原始的檔案資料：keyframe扣掉後面多出來的(_00002929.jpg)，然後再把.jpg加回來
                // headframe→\\10.13.210.12\Media\Keyframe\ptstv\2011\2011103\G201110300020001\G201110300020001.jpg
                // keyframe→\\10.13.210.12\Media\Keyframe\ptstv\2011\2011103\G201110300020001\G201110300020001_00002929.jpg
                return string.Concat(keyframeList[0].Substring(0, keyframeList[0].Length - 13), ".jpg");
            }
            else
                return string.Empty;
        }

        /// <summary>
        /// 取得輸入的FileID所屬的關鍵影格檔案列表(HTTP分享位置)
        /// </summary>
        /// <param name="fileID"></param>
        /// <returns></returns>
        [WebMethod]
        public List<string> GetKeyframeFiles(string fileID)
        {
            List<KeyframeInfo> keyframeInfoResult = GetKeyframeFilesInfo(fileID);
            List<string> result = new List<string>();
            // 
            if (keyframeInfoResult.Count > 0)
                foreach (KeyframeInfo obj in keyframeInfoResult)
                    result.Add(obj.KeyframeHttpPath);
            // 
            return result;
        }

        /// <summary>
        /// 取得輸入的FileID所屬的關鍵影格資訊(HTTP分享位置及關鍵影格描述)
        /// </summary>
        /// <param name="fileNO"></param>
        /// <returns></returns>
        [WebMethod]
        public List<KeyframeInfo> GetKeyframeFilesInfo(string fileNO)
        {
            List<KeyframeInfo> keyframeInfoList = new List<KeyframeInfo>();

            // 查詢資料庫
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSFILE_NO", fileNO);
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_KEYFRAME_GETKEYFRAMEINFO", sqlParameters, out resultData))
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM_GetFileInfo/GetKeyframeFilesInfo", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Concat("查詢SP_Q_KEYFRAME_GETKEYFRAMEINFO時生錯誤，FSFILE_NO = ", fileNO));
                return keyframeInfoList;
            }
            if (resultData.Count == 0)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM_GetFileInfo/GetKeyframeFilesInfo", MAM_PTS_DLL.Log.TRACKING_LEVEL.WARNING, string.Concat("查詢SP_Q_KEYFRAME_GETKEYFRAMEINFO時，找不到相關資料，FSFILE_NO = ", fileNO));
                return keyframeInfoList;
            }

            // 有資料，轉換成HTTP路徑
            if (!string.IsNullOrEmpty(resultData[0]["FSFILE_PATH"]))
            {
                foreach (Dictionary<string, string> oneRecord in resultData)
                {
                    string filePath = oneRecord["FSFILE_PATH"];
                    if (!string.IsNullOrEmpty(filePath))
                    {
                        KeyframeInfo obj = new KeyframeInfo();
                        obj.KeyframeHttpPath = fnConvertMediaUncPathToHttpPath(filePath);
                        obj.KeyframeDescription = oneRecord["FSDESCRIPTION"];
                        keyframeInfoList.Add(obj);
                    }
                    else
                    {
                        MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM_GetFileInfo/GetKeyframeFilesInfo", MAM_PTS_DLL.Log.TRACKING_LEVEL.WARNING, string.Concat("部份KEYFRAME資料未含實體檔案路徑，FSFILE_NO = ", fileNO));
                    }
                }
                return keyframeInfoList;
            }

            MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM_GetFileInfo/GetKeyframeFilesInfo", MAM_PTS_DLL.Log.TRACKING_LEVEL.WARNING, string.Concat("KEYFRAME資料未含實體檔案路徑，FSFILE_NO = ", fileNO));
            return keyframeInfoList;
        }

        /// <summary>
        /// 用FILEID+TYPE查詢TSM上的檔案狀態
        /// </summary>
        /// <param name="fileID"></param>
        /// <param name="type">檔案型別(FileID_MediaType)</param>
        /// <returns></returns>
        [WebMethod]
        public TSM_File_Status GetFileStatusByFileID(string fileID, FileID_MediaType type)
        {
            string smbPath = GetSambaFilePathByFileID(fileID, type);

            // 先檢查實體檔案是否存在
            if (!string.IsNullOrEmpty(smbPath) && File.Exists(smbPath))
            {
                // 如果是這音、圖、文其中之一，直接為NearLine
                if (type == FileID_MediaType.LowVideo || type == FileID_MediaType.Audio || type == FileID_MediaType.Doc || type == FileID_MediaType.Photo)
                    return TSM_File_Status.NearLine;
                else
                    return GetFileStatusBySambaPath(smbPath);
            }
            else
                return TSM_File_Status.NotAvailable;
        }

        /// <summary>
        /// 用VIDEOID查詢TSM上的檔案狀態(Pinnacle格式使用)
        /// Tricky：只檢查std檔（其餘的都必為NearLine），所以在查詢的時候是目錄 + std 檔名
        /// </summary>
        /// <param name="videoID"></param>
        /// <returns></returns>
        [WebMethod]
        public TSM_File_Status GetFileStatusByVideoID(string videoID)
        {
            // 先組合出檔案位置
            string smbFilePath = string.Concat(GetSambaDirPathByVideoID(videoID), "\\std");

            // 檔案如果不存在的話，就判失敗
            if (File.Exists(smbFilePath))
                return GetFileStatusByTsmPath(GetLinuxPathBySambaPath(smbFilePath));
            MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/GetFileStatusByVideoID", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Concat("查詢的主控格式檔案不存在：", smbFilePath));
            return TSM_File_Status.NotAvailable;
        }

        /// <summary>
        /// 用VIDEOID查詢TSM上的檔案狀態(Omneon格式使用)
        /// </summary>
        /// <param name="videoID"></param>
        /// <returns></returns>
        [WebMethod]
        public TSM_File_Status GetFileStatusByVideoID_O(string videoID)
        {
            // 先組合出檔案位置
            string smbFilePath = GetSambaFilePathByVideoID_O(videoID);

            // 檔案如果不存在的話，就判失敗
            if (File.Exists(smbFilePath))
                return GetFileStatusByTsmPath(GetLinuxPathBySambaPath(smbFilePath));
            return TSM_File_Status.NotAvailable;
        }

        /// <summary>
        /// 用SAMBA路徑查詢TSM檔案狀態
        /// </summary>
        /// <param name="smbPath"></param>
        /// <returns></returns>
        [WebMethod]
        public TSM_File_Status GetFileStatusBySambaPath(string smbPath)
        {
            // 有重複檢查的嫌疑，不過為了後面運作快一點，還是檢查吧
            if (File.Exists(smbPath))
                return GetFileStatusByTsmPath(GetLinuxPathBySambaPath(smbPath));
            else
                return TSM_File_Status.NotAvailable;
        }

        /// <summary>
        /// 用TSM路徑取得TSM檔案狀態
        /// </summary>
        /// <param name="tsmPath"></param>
        /// <returns></returns>
        [WebMethod]
        // 注意：要回傳的內容只會是列舉值TSM_File_Status裡面的東西
        public TSM_File_Status GetFileStatusByTsmPath(string tsmPath)
        {
            // 檢查字串格式是否正確
            if (!MAM_PTS_DLL.CheckFormat.CheckFormat_TsmPath(tsmPath))
                return TSM_File_Status.NotAvailable;

            // 用 dsmls 指令查詢(確認：這邊的速度很快，一秒內)
            DSMLS_RETURN_VALUE resultDSMLS = fnGetFileStatusByTsmls(tsmPath);

            // 處理 dsmls 的結果
            switch (resultDSMLS)
            {
                case DSMLS_RETURN_VALUE.p:
                case DSMLS_RETURN_VALUE.r:
                    return TSM_File_Status.NearLine;
                case DSMLS_RETURN_VALUE.error:
                    return TSM_File_Status.SysError;
                case DSMLS_RETURN_VALUE.FileNotExist:
                    return TSM_File_Status.NotAvailable;
                // m 不處理，留給後面繼續進行
            }

            // 檢查檔案是在哪"幾"捲磁帶中(確認：這邊的速度比較慢，四秒以上)
            List<string> volumeNames;
            bool execFlag = GetVolumeNameByTsmPath(tsmPath, out volumeNames);

            if (!execFlag)
                return TSM_File_Status.SysError;

            // 解析回傳結果，判定是否在磁帶櫃內
            switch (fnGetTapeStatus(volumeNames))
            {
                case TSM_Tape_Status.OnLine:
                    return TSM_File_Status.TapeOnly;
                case TSM_Tape_Status.OffLine:
                    return TSM_File_Status.TapeOffline;
            }

            return TSM_File_Status.SysError;
        }

        /// <summary>
        /// 將 UNC 路徑轉為 HTTP 路徑，這邊只針對 Media 的部份有效
        /// </summary>
        /// <param name="uncPath"></param>
        /// <returns></returns>
        string fnConvertMediaUncPathToHttpPath(string uncPath)
        {
            // 
            foreach (string prefix in iisAddressMapping.Keys)
                if (uncPath.StartsWith(prefix))
                    return uncPath.Replace(prefix, iisAddressMapping[prefix]).Replace('\\', '/');

            // 如果 iisAddressMapping 找不到的話，視為錯誤
            return string.Empty;
        }

        /// <summary>
        /// 利用 ChannelID 搜尋對應的 PoolName，如果發生錯誤或查不到會回傳空白值
        /// </summary>
        /// <param name="channelID"></param>
        /// <returns></returns>
        string fnGetPoolName(string channelID)
        {
            string poolName = string.Empty;

            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSCHANNEL_ID", channelID);
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBZCHANNEL_TSMPOOLNAME", sqlParameters, out resultData) || resultData.Count == 0 || string.IsNullOrEmpty(resultData[0]["FSTSM_POOLNAME"]))
            {
                // 因為這邊在測試期太常發生，所以在這邊還是要做好記錄的動作
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/GetFileInfo/fnGetPoolName", MAM_PTS_DLL.Log.TRACKING_LEVEL.FATALERROR, string.Concat("SP_Q_TBZCHANNEL_TSMPOOLNAME無法找到相對應的PoolName值，FSCHANNEL_ID = ", channelID));
                return poolName;
            }

            // 將最前面的 / 消掉 (理論上資料庫的設定沒錯的話，是不會跑到 else)
            if (resultData[0]["FSTSM_POOLNAME"].StartsWith("/"))
                poolName = resultData[0]["FSTSM_POOLNAME"].Substring(1);
            else
                poolName = resultData[0]["FSTSM_POOLNAME"];

            return poolName;
        }
    }
}