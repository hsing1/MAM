﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.WS.WSTSM
{
    public class ClassTape
    {
        public string SerialNo;         // 磁帶名稱
        public string UseType;          // 磁帶類型，DATA、TSMDBBAK、Scratch
        public string SpaceStatus;      // 空間使用狀態，FILLING
        public string UseSpace;         // 磁帶已存放資料量(GB)
        public string LastReadTime;     // 最後讀取時間，Format：2011-01-26-17.48
        public string LastWriteTime;    // 最後寫入時間，Format：2011-01-26-17.48
        public string ContainerPool;    // 所屬儲存池，FILEBAK、MSSVIDEOHSM、PTSTVHSM
        public string RW_Status;        // 讀寫狀態，READWRITE

        // 無用，只是給前端 binding 用的
        public bool needCheckout { get; set; }

        public ClassTape()
        {
            this.needCheckout = false;
        }

        public ClassTape(string number)
        {
            SerialNo = number;
            UseType = string.Empty;
            SpaceStatus = string.Empty;
            UseSpace = string.Empty;
            LastReadTime = string.Empty;
            LastWriteTime = string.Empty;
            ContainerPool = string.Empty;
            RW_Status = string.Empty;
        }

        public void SetAllAttributes(string[] attrs)
        {
            UseType = attrs[1];
            SpaceStatus = attrs[2];
            UseSpace = attrs[3];
            LastReadTime = attrs[4];
            LastWriteTime = attrs[5];
            ContainerPool = attrs[6];
            RW_Status = attrs[7];
        }

        public string GenerateObjectXml()
        {
            return string.Empty;
        }
    }
}
