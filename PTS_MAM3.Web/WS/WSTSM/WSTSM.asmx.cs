﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using MAM_PTS_DLL;

namespace PTS_MAM3.Web.WS.WSTSM
{
    /// <summary>
    /// ServiceTSM 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public partial class ServiceTSM : System.Web.Services.WebService
    {
        /*
         General:
         因為在 TSM 系統中，Linux 的 PTS 是個 /dev 底下的關鍵字
         所以我們在設定 TSM 的 Pool 時把 pts 去掉了，改變為 ptstv
         實
         -> ptstv , hakkatv , mvtv , titv , dimotv , mssvideo , ptsnews (新聞片庫用)
         
         在資料表 TBZCHANNEL 中，記錄了頻道名稱及 POOL_NAME 的對應
         而且我們強制要求，POOL_NAME 與 SHARE_NAME 一定要相同
         這樣才能做出比較有彈性的寫法，而不是用 MAPPING
         
         UNC 路徑 與 實體路徑的對應：
         \\10.13.210.12\ptstv\HVideo    <->  /ptstv/HVideo
         \\10.13.210.12\hakkatv\HVideo  <->  /hakkatv/HVideo
         \\10.13.210.12\mvtv\HVideo     <->  /mvtv/HVideo
         */
        internal const string SYSCODE = "TSM";

        public enum FileID_MediaType { HiVideo, LowVideo, Audio, Photo, Doc };
        public enum TSM_File_Status { NearLine, TapeOnly, TapeOffline, NotAvailable, SysError };
        public enum TSM_Tape_Status { OnLine, OffLine, SysError };

        enum DSMLS_RETURN_VALUE { r, p, m, error, FileNotExist };

        static List<ClassTSMServer> TSMServerObjects = new List<ClassTSMServer>();
        static List<ClassTSMServer> TSMServerList_CommandServer = new List<ClassTSMServer>();   // 記錄可提供 Samba 服務的主機的物件
        static List<string> TSMSambaServerNames = new List<string>();                           // 記錄可提供 Samba 服務的主機的 HostName + IP，跟 TSMServerList_SambaServer 有點像，只是同時記錄兩種資訊，用來讓程式判斷指定的路徑是否在 TSM 中
        static string HsmAdmcArguments = string.Empty;

        // 把這個變數的值，當做是 server name 來使用，目前的值應為：mamstream\MAMDFS or HSM1\ptstest
        static string TSMSambaSharePrefix = string.Empty;
        // 
        static string TSMDB2DSN = string.Empty;
        // 
        static Dictionary<string, string> iisAddressMapping = new Dictionary<string, string>();

        /// <summary>
        /// Constructor：每當有WebMethod被呼叫時，這邊都會被觸發到
        /// </summary>
        public ServiceTSM()
        {
            // 更新TSMSERVER的資訊
            fnUpdateTSMServerInfo();
        }

        /// <summary>
        /// 測試用
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        [WebMethod]
        public string TempWebTest(string x)
        {
            ForceUpdateTSMServerInfo();

            return string.Format("{0}_____{1}_____{2}_______{3}______{4}", 
                TSMDB2DSN, TSMSambaSharePrefix, HsmAdmcArguments, TSMSambaServerNames[0], TSMServerList_CommandServer[0].SERVER_NAME);

            // convert UNC to http address, if we can't find the prefix of the UNC address , we return string.empty

            //foreach (string prefix in iisAddressMapping.Keys)
            //    if (x.StartsWith(prefix))
            //        return x.Replace(prefix, iisAddressMapping[prefix]).Replace('\\', '/');

            //var result = new List<string>();
            //var cmdResult = fnSendCommandToTsmServer("/opt/tivoli/tsm/client/ba/bin/qio3310.sh", out result);
            //if (!cmdResult)
            //    return "Exec Error";

            //return MAM_PTS_DLL.ConvertFormat.ConvertListStringToString(result);
        }

        #region Aboud_Recall_Service
        /// <summary>
        /// 將指定的檔案寫到資料表中，再由後端的應用程式做Online+Recall的動作
        /// </summary>
        /// <param name="filePath">要處理的程式(Samba位址)</param>
        /// <param name="notifyAddr">完成後要通知的位址</param>
        /// <param name="jobID">回傳的JobID</param>
        /// <returns></returns>
        [WebMethod]
        public bool RegisterRecallServiceBySmbPath(string smbPath, string notifyAddr, out long jobID)
        {
            jobID = -1;

            // 如果可以轉為正確的 Linux 路徑，就會成功的被後續的程序處理掉
            if (MAM_PTS_DLL.CheckFormat.CheckFormat_UncPath(smbPath))
                return RegisterRecallServiceByTsmPath(GetLinuxPathBySambaPath(smbPath), notifyAddr, out jobID);

            // Tricky：如果不是正常的路徑，照樣往後拋，但會在後續的程序被做一些特別的處理
            return RegisterRecallServiceByTsmPath(smbPath, notifyAddr, out jobID);
        }

        /// <summary>
        /// 將指定的檔案寫到資料表中，再由後端的應用程式做Online+Recall的動作
        /// </summary>
        /// <param name="tsmRealPath">要處理的程式(Samba位址)</param>
        /// <param name="notifyAddr">完成後要通知的位址</param>
        /// <param name="jobID"></param>
        /// <returns></returns>
        [WebMethod]
        public bool RegisterRecallServiceByTsmPath(string tsmRealPath, string notifyAddr, out long jobID)
        {
            jobID = -1;

            // 將要處理的事情寫入資料庫，並取回JobID
            return fnRegisterRecallFile(tsmRealPath, TSMRecallService.TsmRecallStatus.INITIAL, notifyAddr, out jobID);
        }

        /// <summary>
        /// 將所傳入的 TSMJOBID 任務重啟 (設為INITIIAL)
        /// 呼叫這個服務時，千萬要小心，因為如果是 TSM 正在 Recall 中，會導致很麻煩的處理事件
        /// 我會在後端應用程式做 kill process 的動作 .. so 盡量不要這樣子做 :D
        /// </summary>
        /// <param name="TsmJobID"></param>
        /// <returns></returns>
        [WebMethod]
        public bool RetryRecallService(long TsmJobID)
        {
            // 先查詢該任務的內容()
            List<Dictionary<string, string>> sqlResult = null;
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FNTSMJOB_ID", TsmJobID.ToString());
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TSMJOB_ONEJOB", sqlParameters, out sqlResult) || sqlResult.Count == 0)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSASC/WSTSM/RegisterRecallServiceByTsmPath", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "SP_Q_TSMJOB_ONEJOB 失敗：" + TsmJobID);
                return false;
            }

            // 檢查內容，看來源是不是為「無效路徑」
            if (!MAM_PTS_DLL.CheckFormat.CheckFormat_TsmPath(sqlResult[0]["FSSOURCE_PATH"]))
            {
                // 檢查是否需要(可以)發送 HTTP POST
                if (!MAM_PTS_DLL.CheckFormat.CheckFormat_HttpAddress(sqlResult[0]["FSNOTIFY_ADDR"]))
                    return true;

                // 發送 HTTP POST
                TSMRecallService objTSMRecallService = new TSMRecallService();
                string postMsg = objTSMRecallService.CreateTsmPostMessage(sqlResult[0]["FNTSMJOB_ID"], TSMRecallService.TsmRecallJobResult.FINISH, "No Need Process, Path = '" + sqlResult[0]["FSSOURCE_PATH"] + "'.");
                return MAM_PTS_DLL.Protocol.SendHttpPostMessage(sqlResult[0]["FSNOTIFY_ADDR"], postMsg);

                // 回傳，這邊請密切關注，會不會有 ASHX 快於回傳的問題
                //return true;
            }

            // 這邊是有效路徑，僅需修改資料庫的狀態值，讓後端的程式做處理
            sqlParameters.Clear();
            sqlParameters.Add("FNTSMJOB_ID", TsmJobID.ToString());
            sqlParameters.Add("FNSTATUS", ((int)TSMRecallService.TsmRecallStatus.INITIAL).ToString());
            return MAM_PTS_DLL.DbAccess.Do_Transaction("SP_U_TSMJOB_JOBSTATUS_WS", sqlParameters, "system");
        }

        /// <summary>
        /// 回傳當日的 TSM Recall Job 資訊
        /// </summary>
        /// <param name="queryDate"></param>
        /// <returns></returns>
        [WebMethod]
        public List<ClassTSMRecallJob> GetTSMRecallJobInformation(string queryDate)
        {
            //
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FDQUERY_DATE", queryDate);
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TSMJOB_ONEDAY", sqlParameters, out resultData) || resultData.Count == 0)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/GetTSMRecallJobInformation", Log.TRACKING_LEVEL.TRACE, "Got false or Count = 0, input = " + queryDate);
                return new List<ClassTSMRecallJob>();
            }

            // 
            List<ClassTSMRecallJob> resultList = new List<ClassTSMRecallJob>();
            for (int i = 0; i < resultData.Count; i++)
            {
                ClassTSMRecallJob obj = new ClassTSMRecallJob();
                obj.JobID = resultData[i]["FNTSMJOB_ID"];
                obj.ProcessFilePath = resultData[i]["FSSOURCE_PATH"];
                obj.JobDbStatus = resultData[i]["FNSTATUS"];
                obj.RequestTime = resultData[i]["FDREGISTER_TIME"];
                obj.JobResult = resultData[i]["FSRESULT"];
                obj.SetAttributes();
                resultList.Add(obj);
            }
            //
            return resultList;
        }
        #endregion

        #region About_TS3500
        /// <summary>
        /// 磁帶上架的整合式流程，建議在使用前要執行 PreCheck_CheckinTapes_TSM3500 做檢查，避免有衝突
        /// </summary>
        /// <param name="userID">操作者的userID，以供記錄</param>
        /// <returns></returns>
        [WebMethod]
        public bool CheckinTapes_TSM3500(string userID)
        {
            // 上鎖
            if (!fnWriteTSMOperation("3500", userID))
                return false;
            // 指令
            return fnCheckinTapes_TSM3500();
        }

        /// <summary>
        /// 磁帶下架的整合式流程，建議在使用前要執行 PreCheck_CheckoutTapes_TSM3500 做檢查，避免有衝突
        /// </summary>
        /// <param name="volumes">要進行checkout的磁帶編號</param>
        /// <param name="userID">操作者的userID，以供記錄</param>
        /// <returns></returns>
        [WebMethod]
        public bool CheckoutTapes_TSM3500(List<string> volumes, string userID)
        {
            // 上鎖
            if (!fnWriteTSMOperation("3500", userID))
                return false;
            // 記錄
            MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/CheckoutTapes_TSM3500", Log.TRACKING_LEVEL.INFO, string.Format("使用者{0}要從TS3500退出磁帶：{1}", userID, MAM_PTS_DLL.ConvertFormat.ConvertListStringToString(volumes)));
            // 退出磁帶
            foreach (var volume in volumes)
                if (!fnCheckoutTapes_TSM3500(volume))
                    MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/CheckoutTapes_TSM3500", Log.TRACKING_LEVEL.ERROR, string.Concat("從 TS3500 退出磁帶發生錯誤：", volume));
            return true;
        }

        /// <summary>
        /// 取得 TS3500 I/O Port 內磁帶的數量
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public int GetTs3500IOCount()
        {
            var cmdResult = default(List<string>);
            var exeResult = fnSendCommandToTsmServer("/opt/tivoli/tsm/client/ba/bin/qio3500.sh", out cmdResult);
            if (!exeResult)
                return -1;
            var count = default(int);
            if (!Int32.TryParse(MAM_PTS_DLL.ConvertFormat.ConvertListStringToString(cmdResult), out count))
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/GetTs3500IOCount", Log.TRACKING_LEVEL.ERROR, string.Concat("查詢TS3500的IO磁帶數量：", cmdResult));
                return -1;
            }
            return count;
        }
        #endregion

        #region About_TS3310
        /// <summary>
        /// 磁帶上架的整合式流程，建議在使用前要執行 PreCheck_CheckinTapes_TSM3310 做檢查，避免有衝突
        /// </summary>
        /// <param name="userID">操作者的userID，以供記錄</param>
        /// <returns></returns>
        [WebMethod]
        public bool CheckinTapes_TSM3310(string userID)
        {
            List<string> returnData;
            MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/CheckinTapes_TSM3310", Log.TRACKING_LEVEL.INFO, string.Concat("執行TS3310簽入動作：", userID));
            // 發送3310的Checkin指令
            if (!fnSendCommandToTsmServer(fnCombineTsm3310Checkin(), out returnData))
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/CheckinTapes_TSM3310", Log.TRACKING_LEVEL.ERROR, "TS3310簽入錯誤");
                return false;
            }
            // 解析TSM的回傳
            return fnParseTsm3310Checkin(returnData);
        }

        /// <summary>
        /// TS3310 的磁帶下架
        /// </summary>
        /// <param name="volumes">要進行checkout的磁帶編號</param>
        /// <param name="userID">操作者的userID，以供記錄</param>
        /// <returns></returns>
        [WebMethod]
        public bool CheckoutTapes_TSM3310(List<string> volumes, string userID)
        {
            // 上鎖
            if (!fnWriteTSMOperation("3310", userID))
                return false;
            // 記錄
            MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/CheckoutTapes_TSM3310", Log.TRACKING_LEVEL.INFO, string.Format("使用者{0}要從TS3310退出磁帶：{1}", userID, MAM_PTS_DLL.ConvertFormat.ConvertListStringToString(volumes)));
            // 退出磁帶
            var errCount = 0;
            foreach (var volume in volumes)
            {
                if (!fnCheckoutTapes_TSM3310(volume))
                {
                    errCount++;
                    MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/CheckoutTapes_TSM3310", Log.TRACKING_LEVEL.ERROR, string.Format("從TS3310退出磁帶({0})發生錯誤.", volume));
                }
            }
            return errCount == 0;
        }

        /// <summary>
        /// 取得 TS3310 I/O Port 內磁帶的數量
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public int GetTs3310IOCount()
        {
            var cmdResult = default(List<string>);
            var exeResult = fnSendCommandToTsmServer("/opt/tivoli/tsm/client/ba/bin/qio3310.sh", out cmdResult);
            if (!exeResult)
                return -1;
            var count = default(int);
            if (!Int32.TryParse(MAM_PTS_DLL.ConvertFormat.ConvertListStringToString(cmdResult), out count))
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/GetTs3310IOCount", Log.TRACKING_LEVEL.ERROR, string.Concat("查詢TS3310的IO磁帶數量：", cmdResult));
                return -1;
            }
            return count;
        }
        #endregion

        /// <summary>
        /// 強制更新 TSM 主機資訊
        /// </summary>
        [WebMethod]
        public void ForceUpdateTSMServerInfo()
        {
            // Initial Timer
            lastUpdateTSMServerInfo = DateTime.MinValue;

            // ReDo
            fnUpdateTSMServerInfo();
        }
    }
}