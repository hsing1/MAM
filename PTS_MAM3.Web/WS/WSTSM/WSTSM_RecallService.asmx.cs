﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml;
using System.Text;
using System.IO;

namespace PTS_MAM3.Web.WS.WSTSM
{
    /// <summary>
    /// WSTSM_RecallService 的摘要描述，這部份是給後端應用程式使用，請不要放到 ESB 上面
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class TSMRecallService : System.Web.Services.WebService
    {
        public enum TsmRecallStatus { INITIAL, WAIT_CHECKIN, RECALLING, SUCCESS, NOT_EXIST, RECALL_ERROR, SYSERROR };
        public enum TsmRecallJobResult { SYSTEM_ERROR, FILE_NOT_EXIST, FINISH };

        /// <summary>
        /// 組合並發送 TSMRecallJob 所要發出的 HTTP POST 資訊
        /// </summary>
        /// <param name="postAddr"></param>
        /// <param name="jobID"></param>
        /// <param name="jobStatus"></param>
        /// <param name="jobMsg"></param>
        /// <returns></returns>
        [WebMethod]
        public bool SendTsmPostMessage(string postAddr, string jobID, TsmRecallJobResult jobStatus, string jobMsg)
        {
            return MAM_PTS_DLL.Protocol.SendHttpPostMessage(postAddr, CreateTsmPostMessage(jobID, jobStatus, jobStatus.ToString()));
        }

        /// <summary>
        /// 組合 TSMRecallJob 所要發出的 HTTP POST 資訊
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        [WebMethod]
        public string CreateTsmPostMessage(string id, TsmRecallJobResult status, string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<TSMRECALL>");
            sb.AppendLine(string.Format("<PROCESS_ID>{0}</PROCESS_ID>", id));
            sb.AppendLine(string.Format("<PROCESS_RESULT>{0}</PROCESS_RESULT>", status.ToString()));
            sb.AppendLine(string.Format("<PROCESS_MESSAGE>{0}</PROCESS_MESSAGE>", msg));
            sb.AppendLine("</TSMRECALL>");
            return sb.ToString();
        }

        /// <summary>
        /// 解析由 TSMRecallJob 所發出的 Http Post 資訊
        /// </summary>
        /// <param name="postMessage"></param>
        /// <param name="resultStatus"></param>
        /// <returns></returns>
        public bool ParseTsmPostMessage(Stream inputStream, out long TsmRecallJobID, out TsmRecallJobResult TsmRecallJobStatus, out string TsmRecallJobMsg)
        {
            // 預設值
            TsmRecallJobID = -1;
            TsmRecallJobStatus = TsmRecallJobResult.SYSTEM_ERROR;
            TsmRecallJobMsg = string.Empty;

            // 將 Stream 的內容讀出
            string inputData = string.Empty;
            try
            {
                StreamReader sr = new StreamReader(inputStream);
                inputData = sr.ReadToEnd();
                sr.Close();
            }
            catch (Exception ex)
            {
                string errMsg = string.Concat("讀入HTTP回傳的資料流錯誤：錯誤訊息->", ex.ToString());
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM_RecallService/ParseTsmPostMessage", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return false;
            }

            // 解析 xml 內容
            XmlDocument xdoc = new XmlDocument();
            XmlNode node = null;
            try
            {
                xdoc.LoadXml(inputData);

                //<TSMRECALL>
                //    <PROCESS_ID>  </PROCESS_ID>
                //    <PROCESS_RESULT>  </PROCESS_RESULT>
                //    <PROCESS_MESSAGE>  </PROCESS_MESSAGE>
                //</TSMRECALL>

                // 
                node = xdoc.SelectSingleNode("/TSMRECALL/PROCESS_ID");
                if (node == null)
                    throw new Exception("Can't find node /TSMRECALL/PROCESS_ID");
                if (!Int64.TryParse(node.InnerText, out TsmRecallJobID))
                    return false;

                // 
                node = xdoc.SelectSingleNode("/TSMRECALL/PROCESS_RESULT");
                if (node == null)
                    throw new Exception("Can't find node /TSMRECALL/PROCESS_RESULT");
                if (!Enum.IsDefined(typeof(TsmRecallJobResult), node.InnerText))
                    throw new Exception(string.Format("Can't conver {0} to Enum TsmRecallJobResult", node.InnerText));
                TsmRecallJobStatus = (TsmRecallJobResult)Enum.Parse(typeof(TsmRecallJobResult), node.InnerText);

                // 
                node = xdoc.SelectSingleNode("/TSMRECALL/PROCESS_MESSAGE");
                if (node == null)
                    throw new Exception("Can't find node /TSMRECALL/PROCESS_MESSAGE");
                TsmRecallJobMsg = node.InnerText;
            }
            catch (Exception ex)
            {
                string errMsg = string.Concat("解析HTTP回傳的XML錯誤：原始資料->", inputData, ", 錯誤訊息->", ex.ToString());
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM_RecallService/ParseTsmPostMessage", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return false;
            }

            return true;
        }

        /// <summary>
        /// 為了發佈 enum 而設立的 WebMethod，請不要使用
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public TsmRecallStatus GetTsmRecallStatusList()
        {
            return TsmRecallStatus.SYSERROR;
        }

        /// <summary>
        /// 為了發佈 enum 而設立的 WebMethod，請不要使用
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public TsmRecallJobResult GetTsmRecallJobResultList()
        {
            return TsmRecallJobResult.SYSTEM_ERROR;
        }
    }
}
