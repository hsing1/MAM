﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Diagnostics;
using System.Xml;
using System.Collections;

namespace PTS_MAM3.Web.WS.WSTSM
{
    public partial class ServiceTSM
    {
        // 更新磁帶清單的最短間隔(秒)
        const long durationUpdateTapeList = 30;
        // 更新TSM主機清單的最短間隔(分)
        const double durationUpdateTSMServerInfo = 10;
        // 進行 Checkin 或 Checkout 時要 freeze 的時間(秒)
        const int TSM_LIBRARY_OPERATING_TIME = 300;

        //
        static DateTime InitialTimeValue = DateTime.MinValue;
        static DateTime lastUpdateTapeList_TS3310 = InitialTimeValue;       // 預設一個日期，做為初始用
        static DateTime lastUpdateTapeList_TS3500 = InitialTimeValue;       // 預設一個日期，做為初始用
        static DateTime lastUpdateTSMServerInfo = InitialTimeValue;         // 預設一個日期，做為初始用
        static object lockUpdateTSMServerInfo = new object();

        /// <summary>
        /// 讀取SYSCONFIG內容，解析目前TSMSERVER的資訊
        /// </summary>
        /// <returns></returns>
        void fnUpdateTSMServerInfo()
        {
            // 間隔時間太短，不需要重新載入
            if (DateTime.Now.Subtract(lastUpdateTSMServerInfo).TotalMinutes > durationUpdateTSMServerInfo)
                lastUpdateTSMServerInfo = DateTime.Now;
            else
                return;

            lock (lockUpdateTSMServerInfo)
            {
                // 取得 DB2 DSN 的名稱
                fnGetTsmDB2QueryDSN();
                // 取得 TSM 分享的前綴字串
                fnGetSambaSharePrefix();
                // 先建立所有的TSMServer清單
                fnCreateNewServerObjectList();
                // 建立可用SAMBA TSMServer清單
                fnCreateNewSambaServerList();
                // 建立可用的Command TSMServer清單
                fnCreateNewCommandServerList();
                // Create IIS Address Mapping
                fnCreateIisAddressMapping();
            }
        }

        /// <summary>
        /// 從 SYSCONFIG 中取得 DB2 DSN 的名稱
        /// </summary>
        void fnGetTsmDB2QueryDSN()
        {
            string tmpString = string.Empty;

            MAM_PTS_DLL.SysConfig obj = new MAM_PTS_DLL.SysConfig();
            tmpString = obj.sysConfig_Read("/ServerConfig/TSM_Config/DB2_DSN");
            if (!string.IsNullOrEmpty(tmpString))
                TSMDB2DSN = tmpString;
            else
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/fnServiceTSM/fnGetTsmDB2QueryDSN", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "obj.sysConfig_Read('/ServerConfig/TSM_Config/DB2_DSN') 取得空字串");
        }

        /// <summary>
        /// 從 SYSCONFIG 中取得 TSM 分享的前綴字串
        /// </summary>
        void fnGetSambaSharePrefix()
        {
            string tmpString = string.Empty;

            MAM_PTS_DLL.SysConfig obj = new MAM_PTS_DLL.SysConfig();
            tmpString = obj.sysConfig_Read("/ServerConfig/TSM_Config/SambaRoot");
            if (!string.IsNullOrEmpty(tmpString))
                TSMSambaSharePrefix = tmpString;
            else
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/fnServiceTSM/fnGetSambaSharePrefix", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "obj.sysConfig_Read('/ServerConfig/TSM_Config/SambaRoot') 取得空字串");
        }

        /// <summary>
        /// 從 SYSCONFIG 中取得 SERVER 的所有靜態資訊
        /// </summary>
        void fnCreateNewServerObjectList()
        {
            // 清除之前的記錄
            TSMServerObjects.Clear();

            // 開始建立新的記錄
            MAM_PTS_DLL.SysConfig obj = new MAM_PTS_DLL.SysConfig();
            XmlNodeList x = obj.sysConfig_Read_From_NodeList("/ServerConfig/TSM");
            XmlNode root = x[0];
            IEnumerator ienum = root.GetEnumerator();
            XmlNode tsmserver, serverAttributes;
            while (ienum.MoveNext())
            {
                tsmserver = (XmlNode)ienum.Current;
                IEnumerator ienumSub = tsmserver.GetEnumerator();
                ClassTSMServer objTsmServer = new ClassTSMServer();
                while (ienumSub.MoveNext())
                {
                    serverAttributes = (XmlNode)ienumSub.Current;
                    objTsmServer.AddServerAttribute(serverAttributes.Name, serverAttributes.InnerText);
                }
                TSMServerObjects.Add(objTsmServer);
            }

            // 將 HsmAdmcArguments 讀入
            string tmpString = obj.sysConfig_Read("/ServerConfig/TSM_Config/ADMC_ARGU");
            if (!string.IsNullOrEmpty(tmpString))
                HsmAdmcArguments = tmpString;
            else
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/fnServiceTSM/fnCreateNewServerObjectList", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "obj.sysConfig_Read('/ServerConfig/TSM_Config/ADMC_ARGU') 取得空字串");
        }

        /// <summary>
        /// 將 SERVER LIST 中，把可以負責 SAMBA 的 SERVER 名稱挑出來
        /// </summary>
        void fnCreateNewSambaServerList()
        {
            // 清除之前的記錄
            TSMSambaServerNames.Clear();

            // 讀入 Samba Server 清單
            MAM_PTS_DLL.SysConfig objSysConfig = new MAM_PTS_DLL.SysConfig();
            string ServerAliasNames = objSysConfig.sysConfig_Read("/ServerConfig/TSM_Config/SambaServerAliasName");
            if (string.IsNullOrEmpty(ServerAliasNames))
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/fnServiceTSM/fnCreateNewSambaServerList", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "obj.sysConfig_Read('/ServerConfig/TSM_Config/SambaServerAliasName') 取得空字串");
            else
                TSMSambaServerNames.AddRange(ServerAliasNames.Split(';'));
        }

        /// <summary>
        /// 將 SERVER LIST 中，把可以處理 COMMAND 的 SERVER 名稱挑出來
        /// 如果有找到對應的 SERVER，就順便把 COMMAND STRING 做更新
        /// </summary>
        void fnCreateNewCommandServerList()
        {
            // 清除之前的記錄
            TSMServerList_CommandServer.Clear();
            // 開始建立新的記錄，將可以執行 TSMCommand 的主機IP 加入
            foreach (ClassTSMServer obj in TSMServerObjects)
                if (obj.SERVER_FUNC_RSH)
                    TSMServerList_CommandServer.Add(obj);
        }

        /// <summary>
        /// 
        /// </summary>
        void fnCreateIisAddressMapping()
        {
            // read data from SQL
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBMEDIA_MAPPING", sqlParameters, out resultData) || resultData.Count == 0)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/fnServiceTSM/fnCreateIisAddressMapping", MAM_PTS_DLL.Log.TRACKING_LEVEL.FATALERROR, "SP_Q_TBMEDIA_MAPPING Error or Result Count = 0");
                return;
            }

            // append datas
            iisAddressMapping.Clear();
            foreach (Dictionary<string, string> record in resultData)
                iisAddressMapping[record["FSSOURCE_PATH"]] = record["FSHTTP_ADDRESS"];

            // finish
        }

        /// <summary>
        /// 解析TSM的路徑，拆成 pool_name、filespace_name、file_name
        /// 用途：為了配合 TSM 的 查詢 SQL 的需要，把 TSM 的路徑拆解出來
        /// 範例：/ptstv/abcde/0001.mpg -> nodeName = /ptstv ; fileName = /abcde/0001.mpg
        /// 注意：stgpool_name全大寫；filespace_name全小寫
        /// </summary>
        /// <param name="tsmRealPath"></param>
        /// <param name="stgpool_name"></param>
        /// <param name="filespace_name"></param>
        /// <param name="file_name"></param>
        /// <returns></returns>
        bool fnParseTsmPath(string tsmRealPath, out string stgpool_name, out string filespace_name, out string file_name)
        {
            stgpool_name = string.Empty;
            filespace_name = string.Empty;
            file_name = string.Empty;

            // 檢查 RealPath 的格式是否有誤
            if (!tsmRealPath.StartsWith("/"))
                return false;

            int index = tsmRealPath.IndexOf('/', 1);
            if (index == -1)
                return false;

            // 這邊的寫法不太好，Index實際上應該要是Length才好
            filespace_name = tsmRealPath.Substring(0, index);
            file_name = tsmRealPath.Substring(index);
            stgpool_name = string.Concat(tsmRealPath.Substring(1, index - 1), "hsm").ToUpper();

            //string debugInfo = string.Format("Input = {0}, stgpool_name = {1}, filespace_name = {2}, file_name = {3}", tsmRealPath, stgpool_name, filespace_name, file_name);
            //MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/fnServiceTSM/fnParseTsmPath", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, debugInfo);

            return true;
        }

        /// <summary>
        /// [新版，從記憶體中查] 檢查磁帶是否有在磁帶櫃中
        /// </summary>
        /// <param name="volumeName"></param>
        /// <returns></returns>
        TSM_Tape_Status fnGetTapeStatus(string volumeName)
        {
            return fnGetTapeStatus(new List<string>() { volumeName });
        }

        /// <summary>
        /// [新版，從記憶體中查] 檢查磁帶是否有在磁帶櫃中
        /// 輸入的是多個 volume，取最「嚴重」的那一個為回傳
        /// </summary>
        /// <param name="volumeNames"></param>
        /// <returns></returns>
        TSM_Tape_Status fnGetTapeStatus(List<string> volumeNames)
        {
            // 更新磁帶清單
            if (!fnUpdateTapeList_TS3500())
                return TSM_Tape_Status.SysError;

            // 檢查清單資訊是否正常
            if (ClassTapeInfo.GetTS3500Cache().Count == 0)//.CacheTapeList_TS3500.Count == 0)
            //if (cacheTapeList_TSm3500.Count == 0)
            //if (cacheTapeList_Tsm3500.Count == 0)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/fnServiceTSM/fnGetTapeStatus", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "查詢不到磁帶清單");
                return TSM_Tape_Status.SysError;
            }

            // 以 Offline 為優先回傳
            foreach (string volumeName in volumeNames)
            //{
                if (!ClassTapeInfo.GetTS3500Cache().Contains(new ClassTapeInfo(volumeName)))//.CacheTapeList_TS3500.Contains(new ClassTapeInfo(volumeName)))
                //if (!cacheTapeList_TSm3500.Contains(new ClassTapeInfo(volumeName)))
                //if (!cacheTapeList_Tsm3500.ContainsKey(volumeName))
                    return TSM_Tape_Status.OffLine;
            //}

            return TSM_Tape_Status.OnLine;
        }

        /// <summary>
        /// 用 dsmls 指令檢查檔案狀態 (r、p、m)
        /// </summary>
        /// <param name="tsmRealPath">要查詢狀態的TSM檔案</param>
        /// <returns></returns>
        DSMLS_RETURN_VALUE fnGetFileStatusByTsmls(string tsmRealPath)
        {
            // 將dsmls指令送到HSM主機，並取回傳
            List<string> tmpResultList;
            if (!fnSendCommandToTsmServer(fnCombine_TsmCmd_dsmls(tsmRealPath), out tmpResultList))
                return DSMLS_RETURN_VALUE.error;

            // 檢查 dsmls 指令的回傳內容（因為我在指令裡，已經 Sed 掉不需理會的資料，所以理論上只會有一行才對，不論是有檔或無檔）
            if (tmpResultList.Count != 1)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/fnServiceTSM/fnGetFileStatusByTsmls", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Concat("例外：dsmls的回傳值行數異常，回傳內容：", MAM_PTS_DLL.ConvertFormat.ConvertListStringToString(tmpResultList)));
                return DSMLS_RETURN_VALUE.error;
            }

            // 特殊狀況，可以直接回傳
            if (tmpResultList[0].Contains("No such file or directory"))
                return DSMLS_RETURN_VALUE.FileNotExist;

            // 解析回傳
            List<string> parsedResult = new List<string>();
            if (!fnParse_TsmCmd_dsmls(tmpResultList[0], out parsedResult) || parsedResult.Count < 3)
            {
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/fnServiceTSM/fnGetFileStatusByTsmls", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Concat("例外：無法正確解析 dsmls 的回傳值，內容：", tmpResultList[0]));
                return DSMLS_RETURN_VALUE.error;
            }

            // parsedResult 的妙用：
            //parsedResult[0]     // Actual Size
            //parsedResult[1]);   // Resident Size
            //parsedResult[2]);   // File Status
            //parsedResult[3]);   // File Path

            // NearLine的狀態，把 p 也當做是NearLine
            if (parsedResult[2] == "r" || parsedResult[2] == "p")
            {
                // 當 Actual Size == Resident Size 時，才算是完全的 NearLine
                if (parsedResult[0] == parsedResult[1])
                {
                    if (parsedResult[2] == "r")
                        return DSMLS_RETURN_VALUE.r;
                    if (parsedResult[2] == "p")
                        return DSMLS_RETURN_VALUE.p;

                    // 理論上不會到這邊才對
                    MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/fnServiceTSM/fnGetFileStatusByTsmls", MAM_PTS_DLL.Log.TRACKING_LEVEL.FATALERROR, string.Concat("無法辨視的dsmls檔案狀態：", parsedResult[2]));
                    return DSMLS_RETURN_VALUE.error;
                }
                else
                {
                    // 理論上不會有這個例外才對
                    MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/fnServiceTSM/fnGetFileStatusByTsmls", MAM_PTS_DLL.Log.TRACKING_LEVEL.FATALERROR, string.Format("TSM檔案狀態為：{0}，但ActualSize({1})與ResidentSize({2})大小不同", parsedResult[2], parsedResult[0], parsedResult[1]));
                    return DSMLS_RETURN_VALUE.error;
                }
            }

            return DSMLS_RETURN_VALUE.m;
        }

        /// <summary>
        /// 在 TSM 執行 checkin 或 checkout 動作時做記錄，也可以讓執行完後有 cooling time
        /// </summary>
        /// <param name="machine"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        bool fnWriteTSMOperation(string machine, string userID)
        {
            return MAM_PTS_DLL.DbAccess.Do_Transaction(
                "SP_I_TBTSM_SYSOP",
                new Dictionary<string, string>() { { "FSMACHINE", machine }, { "FSUSER_ID", userID }, },
                userID);
        }

        /// <summary>
        /// 註冊要做 TSMRecall 的檔案，並回寫 TSMRecall 的 JobID
        /// </summary>
        /// <param name="tsmPath">要執行ONLINE的TSM路徑</param>
        /// <param name="notifyAddr">結束時要通知的位置</param>
        /// <returns></returns>
        bool fnRegisterRecallFile(string tsmPath, TSMRecallService.TsmRecallStatus status, string notifyAddr, out long jobID)
        {
            string fsPROGRAM = MAM_PTS_DLL.ConvertFormat.Combine_TraceLogFSProgram(SYSCODE, System.Reflection.MethodBase.GetCurrentMethod().Name);

            jobID = -1;

            // 將要註冊的資料寫入 TBTSB_JOB 資料表
            List<Dictionary<string, string>> resultData = new List<Dictionary<string, string>>();
            Dictionary<string, string> sqlParameters = new Dictionary<string, string>();
            sqlParameters.Add("FSSOURCE_PATH", tsmPath);
            sqlParameters.Add("FNSTATUS", ((int)status).ToString());
            sqlParameters.Add("FSNOTIFY_ADDR", notifyAddr);
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_I_TBTSM_JOB", sqlParameters, out resultData) || resultData.Count == 0)
            {
                string errMsg = string.Format("SP_I_TBTSM_JOB Error：{0}/{1}/{2}/", tsmPath, (int)status, notifyAddr);
                MAM_PTS_DLL.Log.AppendTrackingLog("WSASC/fnServiceTSM", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);
                return false;
            }

            // 檢查回傳的值是否為正確的 INT64 格式
            if (Int64.TryParse(resultData[0]["FNTSMJOB_ID"], out jobID))
                return true;
            else
                return false;
        }

        #region About_TS3310_SubFunctions
        /// <summary>
        /// 磁帶下架(非同步) - TS3310
        /// 用途：將指定的磁帶退到IO槽中
        /// </summary>
        /// <param name="volumeName">指定要退到IO槽的磁帶編號</param>
        /// <returns></returns>
        bool fnCheckoutTapes_TSM3310(string volumeName)
        {
            List<string> returnData;
            // 發送指令
            if (!fnSendCommandToTsmServer(fnCombineTsm3310CheckoutTapes(volumeName), out returnData))
                return false;
            // 解析TSM的回傳
            if (!fnParseTsm3310CheckoutTapes(returnData))
                return false;
            // 因為上下架為非同步的動作，所以不需要針對記憶體馬上做更新
            return true;
        }

        /// <summary>
        /// 更新記憶體中的磁帶清單(TS3310)
        /// 因為查詢磁帶櫃中的磁帶編號是一件需要浪費DB效能，且不會常有變化的事，所以我把磁帶清單寫在記憶體中，以減少查詢的次數
        /// </summary>
        /// <returns></returns>
        bool fnUpdateTapeList_TS3310()
        {
            // 間隔時間太短，不需要重新載入
            if (((TimeSpan)(DateTime.Now - lastUpdateTapeList_TS3310)).TotalSeconds < durationUpdateTapeList)
                return true;
            lastUpdateTapeList_TS3310 = DateTime.Now;

            // 把指令送給主機
            List<string> result;
            bool execResult = fnSendCommandToTsmServer(fnCombine_TsmAdmc_GetTapeList_TSM3310(), out result);
            if (!execResult)
            {
                // 失敗就把更新時間重置，讓下次使用時可以再更新
                lastUpdateTapeList_TS3310 = InitialTimeValue;
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/fnUpdateTapeList_TS3310", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Format("發送查詢磁帶清單指令時發生錯誤：指令 = {0}, 回傳訊息 = {1}", fnCombine_TsmAdmc_GetTapeList_TSM3310(), MAM_PTS_DLL.ConvertFormat.ConvertListStringToString(result)));
                return false;
            }

            // 解析
            execResult = fnParse_TsmAdmc_GetTapeList_TSM3310(result);
            if (!execResult)
            {
                // 失敗就把更新時間重置，讓下次使用時可以再更新
                lastUpdateTapeList_TS3310 = InitialTimeValue;
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/fnUpdateTapeList_TS3310", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "解析查詢磁帶清單指令的回傳值時發生錯誤");
                return false;
            }

            // 記錄(其實也可以不要)
            MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/fnUpdateTapeList_TS3310", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, string.Concat("磁帶清單原始內容：", MAM_PTS_DLL.ConvertFormat.ConvertListStringToString(result)));
            return true;
        }
        #endregion

        #region About_TS3500_SubFunctions
        /// <summary>
        /// 磁帶上架(非同步) - TS3500 
        /// 用途：將IO槽內的磁帶置於架中
        /// </summary>
        /// <returns></returns>
        bool fnCheckinTapes_TSM3500()
        {
            var returnData = default(List<string>);
            // 發送指令
            if (!fnSendCommandToTsmServer(fnCombineTsm3500CheckinTapes(), out returnData))
                return false;
            // 解析TSM的回傳
            if (!fnParseTsm3500CheckinTapes(returnData))
                return false;
            // 因為上下架為非同步的動作，所以就不需強制刷新記憶體中的內容
            // 未來如果有需求的話，就變成要調整「更新記憶體」時間
            return true;
        }

        /// <summary>
        /// 磁帶下架(非同步) - TS3500
        /// 用途：將指定的磁帶退到IO槽中
        /// </summary>
        /// <param name="volumeName">指定要退到IO槽的磁帶編號</param>
        /// <returns></returns>
        bool fnCheckoutTapes_TSM3500(string volumeName)
        {
            List<string> returnData;
            // 發送指令
            if (!fnSendCommandToTsmServer(fnCombineTsm3500CheckoutTapes(volumeName), out returnData))
                return false;
            // 解析TSM的回傳
            if (!fnParseTsm3500CheckoutTapes(returnData))
                return false;
            // 因為上下架為非同步的動作，所以不需要針對記憶體馬上做更新
            return true;
        }

        /// <summary>
        /// 更新記憶體中的磁帶清單(TS3500)
        /// 因為查詢磁帶櫃中的磁帶編號是一件需要浪費DB效能，且不會常有變化的事，所以我把磁帶清單寫在記憶體中，以減少查詢的次數
        /// </summary>
        /// <returns></returns>
        bool fnUpdateTapeList_TS3500()
        {
            // 間隔時間太短，不需要重新載入
            if (((TimeSpan)(DateTime.Now - lastUpdateTapeList_TS3500)).TotalSeconds < durationUpdateTapeList)
                return true;
            lastUpdateTapeList_TS3500 = DateTime.Now;

            // 把指令送給主機
            List<string> result;
            bool execResult = fnSendCommandToTsmServer(fnCombine_TsmAdmc_GetTapeList_TSM3500(), out result);
            if (!execResult)
            {
                // 失敗就把更新時間重置，讓下次使用時可以再更新
                lastUpdateTapeList_TS3500 = InitialTimeValue;
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/fnUpdateTapeList_TS3500", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Format("發送查詢磁帶清單指令時發生錯誤：指令 = {0}, 回傳訊息 = {1}", fnCombine_TsmAdmc_GetTapeList_TSM3500(), MAM_PTS_DLL.ConvertFormat.ConvertListStringToString(result)));
                return false;
            }

            // 解析
            execResult = fnParse_TsmAdmc_GetTapeList_TSM3500(result);
            if (!execResult)
            {
                // 失敗就把更新時間重置，讓下次使用時可以再更新
                lastUpdateTapeList_TS3500 = InitialTimeValue;
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/fnUpdateTapeList_TS3500", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "解析查詢磁帶清單指令的回傳值時發生錯誤");
                return false;
            }

            // 記錄(其實也可以不要)
            MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/fnUpdateTapeList_TS3500", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, string.Concat("磁帶清單原始內容：", MAM_PTS_DLL.ConvertFormat.ConvertListStringToString(result)));
            return true;
        }
        #endregion

        /// <summary>
        /// 把 TSM 指令送到主機，並將結果回寫至 buffer 變數
        /// </summary>
        /// <param name="strCommand"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        bool fnSendCommandToTsmServer(string strCommand, out List<string> result)
        {
            result = new List<string>();

            string stderrMsg = string.Empty;
            string errMsg = string.Empty;

            // 目前已知 plink 碰到含 " 的字元時會發生錯誤，所以先做好置換的動作
            strCommand = strCommand.Replace(@"""", @"\""");

            // 如果發生錯誤的話，就輪詢吧
            for (int i = 0; i < TSMServerList_CommandServer.Count; i++)
            {
                Process p = new Process();
                p.StartInfo.FileName = @"D:\0selfsys\plink.exe";
                // Update：201102222，因為用plink沒辦法載到家目錄的設定檔，所以只好用 . /root/.bash_profile 的方式來強制讀入
                p.StartInfo.Arguments = string.Format(@"{0} "". /root/.bash_profile"";""{1}""", TSMServerList_CommandServer[i].SERVER_PARA_RSH_ARGU, strCommand);
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.CreateNoWindow = true;
                p.StartInfo.RedirectStandardInput = true;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.RedirectStandardError = true;

                try
                {
                    p.Start();
                    while (!p.StandardOutput.EndOfStream)
                        result.Add(p.StandardOutput.ReadLine());
                    stderrMsg = p.StandardError.ReadToEnd();

                    // 在這邊將一些資訊處理掉
                    if (!string.IsNullOrEmpty(stderrMsg))
                    {
                        // 這個是特例，當查詢的檔案不存在時，將 stderr 的訊息轉為 stdout 的訊息供前面查詢
                        if (stderrMsg.Contains("No such file or directory"))
                        {
                            result.Clear();
                            result.Add(stderrMsg);
                        }
                        else
                        {
                            errMsg = string.Format("執行 plink 指令的結果碰到錯誤訊息：arug = {0}, msg = {1}", p.StartInfo.Arguments, stderrMsg);
                        }
                    }
                }
                catch (Exception ex)
                {
                    // 這邊是執行 plink 出的 Exception (不過理論上是不常出現的)
                    errMsg = string.Format("執行 plink 指令時碰到例外：argu = {0}, msg = {1}", p.StartInfo.Arguments, ex.Message);
                }
                finally
                {
                    try
                    {
                        p.Close();
                    }
                    catch
                    {
                        //
                    }
                    p.Dispose();
                }

                // 這邊的 errMsg 是包括了執行時的 stderr 錯誤 (有可能是連線錯誤) 及 plink 的錯誤

                // 如果錯誤訊息為空白，就可以回傳了
                if (string.IsNullOrEmpty(errMsg))
                    return true;

                // 將要回傳的內容重新初始化
                result = new List<string>();

                // 記錄錯誤
                string hostName = "N/A";
                try
                {
                    hostName = System.Net.Dns.GetHostName();
                }
                catch
                {
                    //
                }
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/fnServiceTSM/fnSendCommandToTsmServer", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Format("[{0}] {1}", hostName, errMsg));
            }

            // 所有 Server 都問過，而且都發生錯誤訊息了，放棄吧
            MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/fnServiceTSM/fnSendCommandToTsmServer", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, string.Concat("執行命令錯誤(已重試)：", strCommand));
            return false;
        }
    }
}