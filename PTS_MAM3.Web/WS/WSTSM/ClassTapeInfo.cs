﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.WS.WSTSM
{
    /// <summary>
    /// 
    /// </summary>
    public class ClassTapeInfo : IComparable
    {
        public int TapeNoCount;         // 磁帶數量(僅提供流水號供UI檢視)
        public string SerialNo;         // 磁帶名稱，也用做PK
        public string UseType;          // 磁帶類型，DATA、TSMDBBAK、Scratch
        public string SpaceStatus;      // 空間使用狀態，FILLING
        public double UseSpace;         // 磁帶已存放資料量(GB)
        public string LastReadTime;     // 最後讀取時間，Format：2011-01-26-17.48
        public string LastWriteTime;    // 最後寫入時間，Format：2011-01-26-17.48
        public string ContainerPool;    // 所屬儲存池，FILEBAK、MSSVIDEOHSM、PTSTVHSM
        public string RW_Status;        // 讀寫狀態，READWRITE

        // 快取
        static List<ClassTapeInfo> Cache_TS3500 = new List<ClassTapeInfo>();
        static List<ClassTapeInfo> Cache_TS3310 = new List<ClassTapeInfo>();
        static object CacheLock_TS3500 = new object();
        static object CacheLock_TS3310 = new object();

        /// <summary>
        /// 
        /// </summary>
        public ClassTapeInfo()
        {
            // 保留給 Web Service，不然會有錯誤
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serial"></param>
        internal ClassTapeInfo(string serial)
        {
            TapeNoCount = -1;
            SerialNo = serial;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="number"></param>
        internal ClassTapeInfo(string[] attrs, int tapeCount = -1)
        {
            TapeNoCount = tapeCount;
            SerialNo = attrs[0];
            UseType = attrs[1];
            SpaceStatus = attrs[2];
            //UseSpace = attrs[3];
            LastReadTime = attrs[4];
            LastWriteTime = attrs[5];
            ContainerPool = attrs[6];
            RW_Status = attrs[7];

            // UseSpace 以 Double 的方式呈現
            double tmpDouble;
            if (Double.TryParse(attrs[3], out tmpDouble))
                UseSpace = tmpDouble;
            else
                UseSpace = 0.0;
        }

        #region About_Ts3310_Cache
        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="attrs"></param>
        /// <returns></returns>
        internal static bool AddTS3310Cache(string attrs)
        {
            // 範例：
            //E00705L4,DATA,FULL,1312.84,2014-09-08-13.10,2014-09-11-11.15,FILEBAKCP,READWRITE
            //B00160L4,Scratch,,,,,,
            string[] dataArr = attrs.Split(',');
            
            // 檢查格式是否正確
            if (dataArr.Length != 8)
                return false;

            //
            var newTape = new ClassTapeInfo(dataArr);
            lock (CacheLock_TS3310)
            {
                if (!Cache_TS3310.Contains(newTape))
                {
                    Cache_TS3310.Add(newTape);
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// 清除
        /// </summary>
        internal static void ClearTS3310Cache()
        {
            lock (CacheLock_TS3310)
            {
                Cache_TS3310.Clear();
            }
        }

        /// <summary>
        /// 排序
        /// </summary>
        /// <param name="resetCountValue">是否要將 TapeNoCount 一併重置</param>
        internal static void SortTS3310Cache(bool resetCountValue = true)
        {
            lock (CacheLock_TS3310)
            {
                Cache_TS3310.Sort();
                if (resetCountValue)
                    for (int i = 0; i < Cache_TS3310.Count; i++)
                        Cache_TS3310[i].TapeNoCount = i + 1;
            }
        }

        /// <summary>
        /// 取得
        /// </summary>
        /// <returns></returns>
        internal static List<ClassTapeInfo> GetTS3310Cache()
        {
            lock (CacheLock_TS3310)
            {
                return Cache_TS3310;
            }
        }
        #endregion

        #region About_Ts3500_Cache
        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="attrs"></param>
        /// <returns></returns>
        internal static bool AddTS3500Cache(string attrs)
        {
            // 範例：
            //E00705L4,DATA,FULL,1312.84,2014-09-08-13.10,2014-09-11-11.15,FILEBAKCP,READWRITE
            //B00160L4,Scratch,,,,,,
            string[] dataArr = attrs.Split(',');

            // 檢查格式是否正確
            if (dataArr.Length != 8)
                return false;

            //
            var newTape = new ClassTapeInfo(dataArr);
            lock (CacheLock_TS3500)
            {
                if (!Cache_TS3500.Contains(newTape))
                {
                    Cache_TS3500.Add(newTape);
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// 清除
        /// </summary>
        internal static void ClearTS3500Cache()
        {
            lock (CacheLock_TS3500)
            {
                Cache_TS3500.Clear();
            }
        }

        /// <summary>
        /// 排序
        /// </summary>
        /// <param name="resetCountValue">是否要將 TapeNoCount 一併重置</param>
        internal static void SortTS3500Cache(bool resetCountValue = true)
        {
            lock (CacheLock_TS3500)
            {
                Cache_TS3500.Sort();
                if (resetCountValue)
                    for (int i = 0; i < Cache_TS3500.Count; i++)
                        Cache_TS3500[i].TapeNoCount = i + 1;
            }
        }

        /// <summary>
        /// 取得
        /// </summary>
        /// <returns></returns>
        internal static List<ClassTapeInfo> GetTS3500Cache()
        {
            lock (CacheLock_TS3500)
            {
                return Cache_TS3500;
            }
        }
        #endregion

        public int CompareTo(object c)
        {
            // 目前是用 SerialNo 做排序
            if (!(c is ClassTapeInfo))
                return -1;
            return ((ClassTapeInfo)c).SerialNo.CompareTo(SerialNo) * -1;
        }

        public override int GetHashCode()
        {
            return SerialNo.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (!(obj is ClassTapeInfo))
                return false;
            return ((ClassTapeInfo)obj).SerialNo.Equals(SerialNo);
        }

        public override string ToString()
        {
            return SerialNo;
        }
    }
}