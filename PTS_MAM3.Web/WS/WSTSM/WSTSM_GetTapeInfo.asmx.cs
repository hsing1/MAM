﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.Odbc;

namespace PTS_MAM3.Web.WS.WSTSM
{
    /// <summary>
    /// 查詢與磁帶相關的WebService
    /// </summary>
    public partial class ServiceTSM : System.Web.Services.WebService
    {
        static List<string> cacheIndex_TsmPathToVolumeNames = new List<string>();
        static Dictionary<string, List<string>> cache_TsmPathToVolumeNames = new Dictionary<string, List<string>>(); // Key：TSM Path；Value：Volume Names

        /// <summary>
        /// 輸入TSM路徑，回傳是在哪"幾"捲磁帶中
        /// </summary>
        /// <param name="tsmPath"></param>
        /// <param name="volumeNames"></param>
        /// <returns></returns>
        [WebMethod]
        public bool GetVolumeNameByTsmPath(string tsmPath, out List<string> volumeNames)
        {
            // 已知測試：
            // obj.Open();                          需兩秒
            // objReader = objCmd.ExecuteReader();  需一秒
            // obj.Close();                         需一秒

            // 初始化變數
            volumeNames = new List<string>();

            // 檢查TSM路徑格式是否正確
            if (!MAM_PTS_DLL.CheckFormat.CheckFormat_TsmPath(tsmPath))
                return false;

            // 檢查 Cache 中是否有相關資料，有的話就直接回傳
            if (cache_TsmPathToVolumeNames.ContainsKey(tsmPath))
            {
                volumeNames = cache_TsmPathToVolumeNames[tsmPath];
                return true;
            }

            // DB2 的 Query 語法加到設定檔中
            MAM_PTS_DLL.SysConfig objSysConfig = new MAM_PTS_DLL.SysConfig();
            string sqlQueryTemplate = objSysConfig.sysConfig_Read("/ServerConfig/TSM_Config/DB2_QUERY");

            // 檢查 DSN 的值是否正確
            if (string.IsNullOrEmpty(TSMDB2DSN))
                fnUpdateTSMServerInfo();

            // ===========================================================================================
            // 這邊是第一版的使用方式，不過要一直 Query ADMC 的結果，所以效能不太好，也不建議
            //List<string> commandReturn;

            //if (!fnSendCommandToTsmServer(fnCombineTsmCommand_queryTapeNumber(tsmPath), out commandReturn))
            //    return false;

            //if (commandReturn.Count > 0)
            //    MAM_PTS_DLL.Log.AppendTrackingLog("william-test", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, commandReturn[0]);

            //// 可能會有多捲的情況，所以用 List 存放結果
            //volumeNames = fnParseTsmCommand_queryTapeNumber(commandReturn);
            //return true;
            // ===========================================================================================

            // 2011-03-28：改用 DB2 的方式查詢
            string logMsg = string.Empty;
            string stgpool_name = string.Empty;
            string filespace_name = string.Empty;
            string file_name = string.Empty;
            string connString = string.Concat("DSN=", TSMDB2DSN);
            
            // 將輸入的 TSM 路徑，拆解為資料庫所需要欄位值：
            // 例：/ptstv/0001.mxf → filespace_name = /ptstv；file_name = /0001.mxf
            fnParseTsmPath(tsmPath, out stgpool_name, out filespace_name, out file_name);

            // 第一版的 DB2 Query，但發現，因為有 HSMCOPY 的帶子，所以這個結果會把 3310 的東西也撈進來，Error
            //string sqlStr = string.Format("SELECT VOLUME_NAME FROM CONTENTS WHERE FILESPACE_NAME = '{0}' AND FILE_NAME = '{1}'", filespace_name, file_name);
            // 第二版的 DB2 Query，加上了 STGPOOL 的限制條件
            //string sqlStr = string.Format("SELECT VOLUME_NAME FROM CONTENTS WHERE STGPOOL > 0 AND FILESPACE_NAME = '{0}' AND FILE_NAME = '{1}'", filespace_name, file_name);
            // 第三版的 DB2 Query，把 STGPOOL 改為字串，如果是備份帶，尾巴是 CP (要瀘掉)
            //string sqlStr = string.Format("SELECT VOLUME_NAME FROM CONTENTS WHERE FILESPACE_NAME = '{0}' AND FILE_NAME = '{1}' AND STGPOOL NOT LIKE '%CP'", filespace_name, file_name);
            // 第四版的 DB2 Query，直接藏在設定檔中
            string sqlQueryString = string.Format(sqlQueryTemplate, filespace_name, file_name);

            logMsg = string.Format("DB2查詢：{0}", sqlQueryString);
            MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM_GetTapeInfo/GetVolumeNameByTsmPath", MAM_PTS_DLL.Log.TRACKING_LEVEL.TRACE, logMsg);

            // 資料庫連線
            OdbcConnection objConn = null;
            OdbcCommand objCmd = null;
            OdbcDataReader objReader = null;
            try
            {
                objConn = new OdbcConnection(connString);
                objConn.Open(); // 這動作耗費 1 ~ 2 秒

                objCmd = new OdbcCommand(sqlQueryString, objConn);
                objReader = objCmd.ExecuteReader(); // 這動作耗費 1 秒左右

                if (objReader.HasRows)
                {
                    while (objReader.Read())
                        volumeNames.Add(objReader.GetString(0));
                }
            }
            catch (Exception ex)
            {
                logMsg = string.Concat("查詢DB2時碰到例外狀況：", ex.ToString());
                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/GetVolumeNameByTsmPath", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, logMsg);
                return false;
            }
            finally
            {
                try
                {
                    if (objReader != null && !objReader.IsClosed)
                        objReader.Close();
                }
                catch
                {
                    //
                }

                try
                {
                    if (objConn != null && objConn.State != System.Data.ConnectionState.Closed)
                        objConn.Close();      // 這個動作約耗費 1 ~ 2 秒
                }
                catch
                {
                    //
                }

                GC.Collect();
            }

            // 寫到 Cache 裡面，下次查到相同資料時，速度 * n 倍!!!
            cache_TsmPathToVolumeNames.Add(tsmPath, volumeNames);

            // 如果 Cache 內的東西過大時，就 ... 刪掉一筆吧
            if (cacheIndex_TsmPathToVolumeNames.Count > 500)
            {
                cache_TsmPathToVolumeNames.Remove(cacheIndex_TsmPathToVolumeNames[0]);
                cacheIndex_TsmPathToVolumeNames.RemoveAt(0);
            }

            return true;
        }

        /// <summary>
        /// 檢查輸入的磁帶編號是否有存在磁帶櫃中
        /// </summary>
        /// <param name="volumeName">欲查詢的磁帶編號</param>
        /// <returns></returns>
        [WebMethod]
        public TSM_Tape_Status CheckIfTapeOnline(string volumeName)
        {
            return fnGetTapeStatus(volumeName);
        }

        /// <summary>
        /// 從快取記憶體查詢TSM3500磁帶櫃中的所有磁帶及其詳細資訊(非即時)
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public List<ClassTapeInfo> GetTapesDetail_TSM3310()
        {
            // 先更新磁帶清單
            fnUpdateTapeList_TS3310();
            // 回傳
            return ClassTapeInfo.GetTS3310Cache();
        }

        /// <summary>
        /// 從快取記憶體查詢TSM3500磁帶櫃中的所有磁帶及其詳細資訊(非即時)
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public List<ClassTapeInfo> GetTapesDetail_TSM3500()
        {
            // 先更新磁帶清單
            fnUpdateTapeList_TS3500();
            // 回傳
            return ClassTapeInfo.GetTS3500Cache();
        }

        /// <summary>
        /// 回傳TSM3500需要進行人工上架的磁帶編號
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public List<ClassTapeNeedCheckin> GetVolumeNeedOnline()
        {
            // 查詢資料庫
            var sqlData = default(List<Dictionary<string, string>>);
            if (!MAM_PTS_DLL.DbAccess.Do_Query("SP_Q_TBTSM_NEEDCHECKIN", new Dictionary<string, string>(), out sqlData) || sqlData.Count == 0)
                return new List<ClassTapeNeedCheckin>();

            // 轉換物件
            var data = new List<ClassTapeNeedCheckin>();
            for (int i = 0; i < sqlData.Count; i++)
                data.Add(new ClassTapeNeedCheckin() { TapeName = sqlData[i]["FSVOLUME_NO"], RequestTime = sqlData[i]["FDREQUEST_TIME"] });
            return data;
        }
    }
}

///// <summary>
//        /// 建立"TSM3310建議下架清單"的第一步：建立TSM資料庫的備份
//        /// </summary>
//        /// <returns></returns>
//        [WebMethod]
//        public bool CreateTSM3310RecommandCheckoutList()
//        {
//            List<string> returnData;
//            // 發送指令
//            if (!fnSendCommandToTsmServer(fnCombineTsm3310DatabaseBackup(), out returnData))
//                return false;
//            // 解析TSM的回傳，並回傳
//            return fnParseTsm3310DatabaseBackup(returnData);
//        }

//        /// <summary>
//        /// 取得 TSM3310 的建議上架清單
//        /// </summary>
//        /// <returns></returns>
//        [WebMethod]
//        public List<ClassTape> GetTSM3310RecommandCheckinList()
//        {
//            List<string> tsmReturnData;
//            List<ClassTape> serviceReturnResult = new List<ClassTape>();

//            string errMsg = string.Empty;

//            // 發送指令
//            if (!fnSendCommandToTsmServer(fnCombineTsm3310CheckinTapeList(), out tsmReturnData))
//                return null;

//            // 解析TSM的回傳，並回傳
//            if (!fnParseTsm3310CheckinTapes(tsmReturnData, out serviceReturnResult))
//            {
//                if (tsmReturnData.Count > 0)
//                    errMsg = string.Concat("Got Error Parsing: ", tsmReturnData[0]);
//                else
//                    errMsg = string.Concat("Got Nothing from Return Data.");
//                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/GetTSM3310RecommandCheckinList", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);

//                return null;
//            }

//            return serviceReturnResult;
//        }

//        /// <summary>
//        /// 取得 TSM3310 的建議下架清單，如果還沒有產生備份清單，就會回傳null
//        /// </summary>
//        /// <returns></returns>
//        [WebMethod]
//        public List<ClassTape> GetTSM3310RecommandCheckoutList()
//        {
//            List<String> tsmReturnData = new List<string>();
//            List<ClassTape> serviceReturnResult = new List<ClassTape>();

//            string errMsg = string.Empty;

//            bool isComplete = false;

//            // 因為這邊的回傳訊息種類不足
//            // 所以"發生錯誤"及沒完成是同一種回傳(null)
//            // 這邊可能會造成後面流程上的問題!?

//            // 查詢資料庫備份是否完成
//            if (!fnCheckBackupDbListComplete_TSM3310(out isComplete) || !isComplete)
//                return null;

//            // 發送指令
//            if (!fnSendCommandToTsmServer(fnCombineTsm3310CheckoutTapes(), out tsmReturnData))
//                return null;

//            // 解析TSM的回傳，並回傳
//            if (!fnParseTsm3310CheckoutTapes(tsmReturnData, out serviceReturnResult))
//            {
//                if (tsmReturnData.Count > 0)
//                    errMsg = string.Concat("Got Error Parsing: ", tsmReturnData[0]);
//                else
//                    errMsg = string.Concat("Got Nothing from Return Data.");
//                MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/GetTSM3310RecommandCheckoutList", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, errMsg);

//                return null;
//            }

//            return serviceReturnResult;
//        }