﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTS_MAM3.Web.WS.WSTSM
{
    public class ClassTSMRecallJob
    {
        public string JobID;
        public string JobDbStatus;      // 存在資料表內的值
        public string JobStatus;        // 轉換過後的值
        public string JobResult;
        public string RequestTime;
        public string ProcessFilePath;
        public string NotifyAddress;

        public void SetAttributes()
        {
            TSMRecallService.TsmRecallStatus status = (TSMRecallService.TsmRecallStatus)(Int32.Parse(JobDbStatus));
            switch (status)
            {
                case TSMRecallService.TsmRecallStatus.INITIAL:
                    JobStatus = "檢查檔案狀態中";
                    break;
                case TSMRecallService.TsmRecallStatus.NOT_EXIST:
                    JobStatus = "檔案不存在";
                    break;
                case TSMRecallService.TsmRecallStatus.RECALL_ERROR:
                    JobStatus = "檔案取回失敗";
                    break;
                case TSMRecallService.TsmRecallStatus.RECALLING:
                    JobStatus = "檔案取回中";
                    break;
                case TSMRecallService.TsmRecallStatus.SUCCESS:
                    // Tricky：原本應該要新增一個 NoNeed 的任務狀態，但因為這個狀態太過複雜，會導致 SP 及 App 的修正
                    //         所以我把這個狀態與 Finish 混用，僅利用 Check File Path (看是不是 Linux Path) 來判斷
                    if (MAM_PTS_DLL.CheckFormat.CheckFormat_TsmPath(ProcessFilePath))
                        JobStatus = "任務完成";
                    else
                        JobStatus = "無需處理";
                    break;
                case TSMRecallService.TsmRecallStatus.SYSERROR:
                    JobStatus = "系統錯誤";
                    break;
                case TSMRecallService.TsmRecallStatus.WAIT_CHECKIN:
                    JobStatus = "等待磁帶上架";
                    break;
                default:
                    JobStatus = "檢查檔案狀態中";      // Tricky!
                    MAM_PTS_DLL.Log.AppendTrackingLog("WSTSM/ClassTSMRecallJob/SetAttributes", MAM_PTS_DLL.Log.TRACKING_LEVEL.ERROR, "Got Unknown Status = " + status);
                    break;
            }
        }
    }
}
